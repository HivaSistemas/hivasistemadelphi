unit BuscarCNPJ;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TFormBuscarCNPJ = class(TForm)
    eCNPJ: TEdit;
    sb1: TButton;
    lb1: TLabel;
    procedure sb1Click(Sender: TObject);
    procedure eCNPJKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormBuscarCNPJ: TFormBuscarCNPJ;

implementation

{$R *.dfm}

procedure TFormBuscarCNPJ.eCNPJKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    sb1.Click;
end;

procedure TFormBuscarCNPJ.sb1Click(Sender: TObject);
begin
  eCNPJ.Text := Trim(eCNPJ.Text);
  if eCNPJ.Text = '' then begin
    ShowMessage('Nenhum CNPJ foi informado!');
    eCNPJ.SetFocus;
    Abort;
  end;

  if not(Length(eCNPJ.Text) in[14,18]) then begin
    ShowMessage('CNPJ inv�lido!');
    eCNPJ.SetFocus;
    Abort;
  end;

  ModalResult := mrOk;
end;

end.
