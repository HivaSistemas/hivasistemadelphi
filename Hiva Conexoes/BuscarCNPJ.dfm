object FormBuscarCNPJ: TFormBuscarCNPJ
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'CNPJ'
  ClientHeight = 75
  ClientWidth = 193
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lb1: TLabel
    Left = 8
    Top = 8
    Width = 57
    Height = 13
    Caption = 'CNPJ Matriz'
  end
  object eCNPJ: TEdit
    Left = 8
    Top = 22
    Width = 177
    Height = 21
    MaxLength = 18
    TabOrder = 0
    OnKeyDown = eCNPJKeyDown
  end
  object sb1: TButton
    Left = 57
    Top = 46
    Width = 75
    Height = 25
    Caption = 'Confirmar'
    TabOrder = 1
    OnClick = sb1Click
  end
end
