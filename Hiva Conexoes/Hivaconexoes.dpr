program Hivaconexoes;

uses
  Vcl.Dialogs,
  Vcl.Forms,
  Principal in 'Principal.pas' {FormPrincipal},
  _Criptografia in '..\Orientado a Objetos\Repositório\_Criptografia.pas',
  _BibliotecaGenerica in '..\Orientado a Objetos\Repositório\_BibliotecaGenerica.pas',
  BuscarCNPJ in 'BuscarCNPJ.pas' {FormBuscarCNPJ};

{$R *.res}

begin
  FBaixandoArquivos := (Pos('VER_NOVOS_ARQUIVOS', ParamStr(1)) > 0) or ((Pos('BAIXAR_NOVA_COMPILACAO', ParamStr(1)) > 0));

  if ParamStr(1) = 'VER_NOVOS_ARQUIVOS' then begin
    FormPrincipal := TFormPrincipal.Create(nil);
    FormPrincipal.sbConectar.Click;
    FormPrincipal.checarNovosArquivos();

    Halt;
  end
  else if Pos('BAIXAR_NOVA_COMPILACAO', ParamStr(1)) > 0 then begin
    FormPrincipal := TFormPrincipal.Create(nil);
    FormPrincipal.sbConectar.Click;
    FormPrincipal.baixarNovosArquivos( _BibliotecaGenerica.RetornaNumeros(ParamStr(1)) );

    Halt;
  end;

  Application.Initialize;

  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFormPrincipal, FormPrincipal);

  Application.Run;
end.
