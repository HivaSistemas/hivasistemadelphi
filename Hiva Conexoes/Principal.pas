unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  Data.DB, MemDS, System.IniFiles, DBAccess, Ora, OraCall, _Criptografia, System.Types, System.StrUtils, _BibliotecaGenerica, Vcl.Grids, GridLuka,
  Vcl.Menus, System.Math, OraClasses, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase, IdFTP,
  Vcl.ComCtrls, system.Zip, BuscarCNPJ, Vcl.Buttons;

type
  TFormPrincipal = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    eUsuario: TEdit;
    eSenha: TEdit;
    eServico: TEdit;
    sbConectar: TButton;
    eServidor: TEdit;
    Conexao: TOraSession;
    Sql: TOraQuery;
    ePorta: TEdit;
    sgArquivos: TGridLuka;
    pmOpcoes: TPopupMenu;
    StoredProc: TOraStoredProc;
    miInserirNovo: TSpeedButton;
    miDeletararquivoselecionado: TSpeedButton;
    miSalvarArquivoSelecionado: TSpeedButton;
    procedure sbConectarClick(Sender: TObject);
    procedure miInserirNovoClick(Sender: TObject);
    procedure miDeletararquivoselecionadoClick(Sender: TObject);
    procedure miSalvarArquivoSelecionadoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);

    procedure BuscarDados();
    procedure baixarArquivoIni(pCNPJMatriz: string);
  private
    FCaminhoAplicacao: string;
  public
    procedure checarNovosArquivos;
    procedure baixarNovosArquivos(pVersao: string);
    function subirNovosArquivos(pNomeArquivo: string): Boolean;
  end;

var
  FormPrincipal: TFormPrincipal;
  FBaixandoArquivos: Boolean;

implementation

{$R *.dfm}

const
  coArquivo          = 0;
  coDataHoraInsercao = 1;
  coDataHoraArquivo  = 2;

  coBinario         = 0;

procedure TFormPrincipal.baixarArquivoIni(pCNPJMatriz: string);
var
  vFtp: TIdFTP;
  vNomeArquivo: string;
begin

  vNomeArquivo := 'ponto.ini/' + pCNPJMatriz + '/sigo.ini';
  vFtp := TIdFTP.Create(nil);

  try
    try
      vFtp.Username := 'altisUpdate';
      vFtp.Password := 'altisUpdate';
      vFtp.Host     := 'altissistemas.ddns.net';

      vFtp.Connect;

      vFtp.Get(vNomeArquivo, FCaminhoAplicacao + 'sigo.ini', False);
    except
      on e: Exception do begin
        ShowMessage('Falha ao buscar arquivo ini! ' + sLineBreak + e.Message);
        Halt;
      end;
    end;
  finally
     vFtp.Disconnect;
     vFtp.Free;
  end;
end;

procedure TFormPrincipal.baixarNovosArquivos(pVersao: string);
var
  vFtp: TIdFTP;
  vHouveErro: Boolean;

  vNomeArquivoZip: string;

  vZip: TZipFile;
begin
  vHouveErro := False;
  vZip := TZipFile.Create;
  vFtp := TIdFTP.Create(nil);

  vNomeArquivoZip := 'HIVA' + pVersao + '.zip';

  try
    try
      DeleteFile(FCaminhoAplicacao + vNomeArquivoZip);

      vFtp.Username := 'altisUpdate';
      vFtp.Password := 'altisUpdate';
      vFtp.Host     := 'altissistemas.ddns.net';

      vFtp.Connect;

      vFtp.Get(vNomeArquivoZip, FCaminhoAplicacao + vNomeArquivoZip, False);
    except
      on e: Exception do begin
        ShowMessage('Falha ao atualizar compilac�o! ' + sLineBreak + e.Message);
        vHouveErro := True;
      end;
    end;
  finally
     vFtp.Disconnect;
     vFtp.Free;
  end;

  if vHouveErro then
    Exit;

  _BibliotecaGenerica.KillTask('HIVA.exe');

  Sleep(1000);

  DeleteFile(FCaminhoAplicacao +  'HIVA.exe');
  vZip.ExtractZipFile(FCaminhoAplicacao + vNomeArquivoZip, '');

  Sleep(1000);

  if not subirNovosArquivos(FCaminhoAplicacao + 'HIVA.exe') then
    Exit;

  WinExec(PAnsiChar(Ansistring(FCaminhoAplicacao + 'HIVA.exe')), SW_NORMAL);

  vZip.Free;
end;

procedure TFormPrincipal.BuscarDados;
var
  vLinha: Integer;
begin
  sgArquivos.ClearGrid();

  Sql.SQL.Clear;
  Sql.SQL.Add('select ');
  Sql.SQL.Add('  NOME, ');
  Sql.SQL.Add('  DATA_HORA_INSERCAO, ');
  Sql.SQL.Add('  DATA_HORA_ARQUIVO, ');
  Sql.SQL.Add('  ARQUIVO');
  Sql.SQL.Add('from ');
  Sql.SQL.Add('  UP_FILES ');

  Sql.ExecSQL;
  Sql.First;

  vLinha := 1;
  while not Sql.Eof do begin
    sgArquivos.Cells[coArquivo, vLinha]          := Sql.Fields[0].AsString;
    sgArquivos.Cells[coDataHoraInsercao, vLinha] := _BibliotecaGenerica.DHFormat(Sql.Fields[1].AsDateTime);
    sgArquivos.Cells[coDataHoraArquivo, vLinha]  := _BibliotecaGenerica.DHFormat(Sql.Fields[2].AsDateTime);

    if Sql.Fields[2].Value <> null then
      sgArquivos.Objects[coBinario, vLinha]      := Sql.CreateBlobStream(Sql.Fields[3], bmRead);

    Sql.Next;
    Inc(vLinha);
  end;
  sgArquivos.RowCount := IfThen(vLinha > 1, vLinha, 2);
end;

procedure TFormPrincipal.checarNovosArquivos;
var
  i: Integer;
  vBaixar: Boolean;
  vDataHoraArquivo: TDateTime;

  vStream: TStream;
  vArquivo: TFileStream;
begin
  if sgArquivos.Cells[coArquivo, 1] = '' then begin
    WinExec(PAnsiChar(Ansistring(FCaminhoAplicacao + 'HIVA.exe')), SW_NORMAL);
    Exit;
  end;

  for i := sgArquivos.FixedRows to sgArquivos.RowCount -1 do begin
    if not FileExists(FCaminhoAplicacao + sgArquivos.Cells[coArquivo, i]) then
      vBaixar := True
    else begin
      vDataHoraArquivo := FileDateToDateTime(FileAge(FCaminhoAplicacao + sgArquivos.Cells[coArquivo, i]));
      vBaixar := _BibliotecaGenerica.DHFormat(vDataHoraArquivo) <> sgArquivos.Cells[coDataHoraArquivo, i];
    end;

    if vBaixar then begin
      try
        vStream := TStream(sgArquivos.Objects[coBinario, i]);
        vArquivo := TFileStream.Create(FCaminhoAplicacao + sgArquivos.Cells[coArquivo, i], fmCreate);
        vArquivo.CopyFrom(vStream, vStream.Position);
        vArquivo.Free;
        vStream.Free;
      except
        on e: Exception do begin
          ShowMessage('Falha ao salvar arquivo ' + sgArquivos.Cells[coArquivo, i] + '!' + #13 + e.Message);
          Continue;
        end;
      end;
    end;
  end;

  WinExec(PAnsiChar( Ansistring(FCaminhoAplicacao + 'HIVA.exe') ), SW_NORMAL);
end;

procedure TFormPrincipal.FormCreate(Sender: TObject);
const
  coUsuario = 0;
  coSenha   = 1;
  coServer  = 2;
  coPorta   = 3;
  coServico = 4;

var
  vServer: string;
  vIni: TIniFile;
  vAuxiliar: TStringDynArray;
begin
  FCaminhoAplicacao := IncludeTrailingPathDelimiter(ExtractFilePath(Application.ExeName));

  if not FileExists('sigo.ini') then begin
    if FBaixandoArquivos then begin
      ShowMessage(
        'N�o foi encontrado o arquivo de conex�o com o banco de dados, ser� necess�rio informar o CNPJ da empresa MATRIZ para que possa ser feita a conex�o!!!'
      );

      FormBuscarCNPJ := TFormBuscarCNPJ.Create(Self);
      if FormBuscarCNPJ.ShowModal <> mrOk then begin
        ShowMessage('Rotina cancelada pelo usu�rio!');
        Halt;
      end;

      baixarArquivoIni(FormBuscarCNPJ.eCNPJ.Text);
    end
    else begin
      ShowMessage('Arquivo de inicializa��o "sigo.ini" n�o encontrado!');
      Exit;
    end;
  end;

  vIni := TIniFile.Create(IncludeTrailingPathDelimiter( ExtractFileDir(Application.ExeName) ) + 'sigo.ini');

  vServer := vIni.ReadString('SERVIDOR', 'server', '');
  vServer := TCriptografia.Create(Application).Descriptografar(vServer, False);

  vAuxiliar := SplitString(vServer, '|');

  eUsuario.Text := vAuxiliar[coUsuario];
  eSenha.Text := vAuxiliar[coSenha];
  eServidor.Text := vAuxiliar[coServer];
  ePorta.Text := vAuxiliar[coPorta];
  eServico.Text := vAuxiliar[coServico];
end;

procedure TFormPrincipal.miDeletararquivoselecionadoClick(Sender: TObject);
begin
  if sgArquivos.Cells[coArquivo, sgArquivos.Row] = '' then
    Exit;

  Conexao.StartTransaction;

  Sql.SQL.Clear;
  Sql.SQL.Add('delete from UP_FILES ');
  Sql.SQL.Add('where NOME = :P1 ');
  Sql.ParamByName('P1').AsString := sgArquivos.Cells[coArquivo, sgArquivos.Row];
  Sql.ExecSQL;

  Conexao.Commit;

  BuscarDados();
end;

procedure TFormPrincipal.miInserirNovoClick(Sender: TObject);
var
  vBusca: TOpenDialog;
begin
  vBusca := TOpenDialog.Create(nil);
  vBusca.InitialDir := '';
  try
    if vBusca.Execute then
      subirNovosArquivos(vBusca.FileName)
  finally
    vBusca.Free;
    BuscarDados();
  end;
end;

procedure TFormPrincipal.miSalvarArquivoSelecionadoClick(Sender: TObject);
var
  vSalva: TSaveDialog;
  vStream: TStream;
  vArquivo: TFileStream;
begin
  if sgArquivos.Cells[coArquivo, sgArquivos.Row] = '' then
    Exit;

  vSalva := TSaveDialog.Create(nil);
  try
    vSalva.FileName := sgArquivos.Cells[coArquivo, sgArquivos.Row];
    if vSalva.Execute() then begin
      vStream := TStream(sgArquivos.Objects[coBinario, sgArquivos.Row]);

      vArquivo := TFileStream.Create(vSalva.FileName, fmCreate);
      vArquivo.CopyFrom(vStream, vStream.Position);
      vArquivo.Free;
      vStream.Free;
    end;
  except
    on e: Exception do begin
      ShowMessage('Falha ao salvar arquivo! ' + e.Message);
    end;
  end;
  vSalva.Free;
end;

procedure TFormPrincipal.sbConectarClick(Sender: TObject);
begin
  if sbConectar.Caption = 'Desconectar' then begin
    Conexao.Disconnect;

    sbConectar.Caption := 'Conectar';

    miInserirNovo.Enabled := False;
    miDeletararquivoselecionado.Enabled := False;
    miSalvarArquivoSelecionado.Enabled := False;
    sgArquivos.ClearGrid();
    sgArquivos.Enabled := False;

    Exit;
  end;

  try
    //Conexao.Options.Direct := False;

    //Conexao.Server   := eServidor.Text + ':' + ePorta.Text + '/' + eServico.Text;
    Conexao.Username := eUsuario.Text;
    Conexao.Password := eSenha.Text;
    Conexao.ConnectString := eUsuario.Text + '/' + eSenha.Text + '@' + eServidor.Text + ':' + ePorta.Text + ':' + eServico.Text;

    Conexao.Connect;
  except
    on e: Exception do begin
      ShowMessage('Falha ao conectar-se no servidor! ' + e.Message);
      Exit;
    end;
  end;

  if Conexao.Connected and not FBaixandoArquivos then
    ShowMessage('Conectado!!!');

  sbConectar.Caption := 'Desconectar';
  sgArquivos.Enabled := True;
  BuscarDados();

  miInserirNovo.Enabled := True;
  miDeletararquivoselecionado.Enabled := True;
  miSalvarArquivoSelecionado.Enabled := True;
end;

function TFormPrincipal.subirNovosArquivos(pNomeArquivo: string): Boolean;
var
  vDataHoraArquivo: TDateTime;
begin
  Result := False;
  try
    Sql.SQL.Clear;

    pNomeArquivo := ExtractFileName(pNomeArquivo);
    vDataHoraArquivo := FileDateToDateTime(FileAge(pNomeArquivo));

    Conexao.StartTransaction;

    StoredProc.Prepare;
    StoredProc.Params[0].AsString := pNomeArquivo;
    StoredProc.Params[1].AsDateTime := vDataHoraArquivo;
    StoredProc.ExecProc;

    Sql.SQL.Add('update UP_FILES set');
    Sql.SQL.Add('  ARQUIVO = :P2 ');
    Sql.SQL.Add('where NOME = :P1 ');

    Sql.Options.TemporaryLobUpdate := True;
    Sql.Prepare;

    Sql.ParamByName('P1').AsString := LowerCase( pNomeArquivo );

    Sql.ParamByName('P2').ParamType := ptInput;
    Sql.ParamByName('P2').AsOraBlob.OCISvcCtx := Conexao.OCISvcCtx;
    Sql.ParamByName('P2').AsOraBlob.CreateTemporary(ltBlob);
    Sql.ParamByName('P2').AsOraBlob.LoadFromFile(IncludeTrailingPathDelimiter(ExtractFileDir(Application.ExeName)) + pNomeArquivo);
    Sql.ParamByName('P2').AsOraBlob.WriteLob;

    Sql.Execute;

    Conexao.Commit;

    Result := True;
  except
    on e: Exception do
      ShowMessage(e.Message);
  end;
end;

end.
