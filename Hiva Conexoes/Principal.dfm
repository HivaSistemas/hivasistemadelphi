object FormPrincipal: TFormPrincipal
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Hivaconexoes'
  ClientHeight = 298
  ClientWidth = 760
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 6
    Top = 5
    Width = 36
    Height = 13
    Caption = 'Usu'#225'rio'
  end
  object Label2: TLabel
    Left = 109
    Top = 5
    Width = 30
    Height = 13
    Caption = 'Senha'
  end
  object Label3: TLabel
    Left = 356
    Top = 5
    Width = 26
    Height = 13
    Caption = 'Porta'
  end
  object Label4: TLabel
    Left = 420
    Top = 5
    Width = 35
    Height = 13
    Caption = 'Servi'#231'o'
  end
  object Label6: TLabel
    Left = 199
    Top = 5
    Width = 40
    Height = 13
    Caption = 'Servidor'
  end
  object miInserirNovo: TSpeedButton
    Left = 0
    Top = 88
    Width = 142
    Height = 40
    BiDiMode = bdLeftToRight
    Caption = 'Inserir arquivo'
    Flat = True
    NumGlyphs = 2
    ParentBiDiMode = False
    OnClick = miInserirNovoClick
  end
  object miDeletararquivoselecionado: TSpeedButton
    Left = 0
    Top = 134
    Width = 142
    Height = 40
    BiDiMode = bdLeftToRight
    Caption = 'Deletar arquivo selecionado'
    Flat = True
    NumGlyphs = 2
    ParentBiDiMode = False
    OnClick = miDeletararquivoselecionadoClick
  end
  object miSalvarArquivoSelecionado: TSpeedButton
    Left = 0
    Top = 184
    Width = 142
    Height = 40
    BiDiMode = bdLeftToRight
    Caption = 'Salvar arquivo selecionado'
    Flat = True
    NumGlyphs = 2
    ParentBiDiMode = False
    OnClick = miSalvarArquivoSelecionadoClick
  end
  object eUsuario: TEdit
    Left = 6
    Top = 20
    Width = 97
    Height = 21
    TabOrder = 0
  end
  object eSenha: TEdit
    Left = 109
    Top = 20
    Width = 84
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
  end
  object eServico: TEdit
    Left = 420
    Top = 20
    Width = 68
    Height = 21
    MaxLength = 30
    TabOrder = 4
    Text = 'XE'
  end
  object sbConectar: TButton
    Left = 493
    Top = 20
    Width = 259
    Height = 21
    Caption = 'Conectar'
    TabOrder = 5
    OnClick = sbConectarClick
  end
  object eServidor: TEdit
    Left = 199
    Top = 20
    Width = 151
    Height = 21
    TabOrder = 2
  end
  object ePorta: TEdit
    Left = 356
    Top = 20
    Width = 61
    Height = 21
    NumbersOnly = True
    TabOrder = 3
  end
  object sgArquivos: TGridLuka
    Left = 145
    Top = 46
    Width = 607
    Height = 250
    Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
    ColCount = 3
    DefaultRowHeight = 19
    DrawingStyle = gdsClassic
    Enabled = False
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    PopupMenu = pmOpcoes
    TabOrder = 6
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Arquivo'
      'Data/hora inser'#231#227'o'
      'Data/hora arquivo')
    Grid3D = False
    RealColCount = 3
    AtivarPopUpSelecao = False
    ColWidths = (
      201
      156
      227)
  end
  object Conexao: TOraSession
    Options.StatementCacheSize = 50
    Options.Direct = True
    PoolingOptions.MaxPoolSize = 1000
    LoginPrompt = False
    Left = 272
    Top = 200
  end
  object Sql: TOraQuery
    Session = Conexao
    AutoCommit = False
    Options.TemporaryLobUpdate = True
    Left = 464
    Top = 224
  end
  object pmOpcoes: TPopupMenu
    Left = 336
    Top = 168
  end
  object StoredProc: TOraStoredProc
    StoredProcName = 'VERIFICAR_UP_FILES'
    Session = Conexao
    Left = 576
    Top = 200
  end
end
