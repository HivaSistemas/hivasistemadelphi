unit Api.Base;

interface

uses
  System.Classes,
  System.JSON;

type
  TApi= class(TComponent)
  private
    FURL: string;
    FAuthorization: string;
  public
    function Post(const pJson: string): TJSonValue;
    function Get: TJSonValue;
    property URL: string read FURL write FURL;
    property Authorization: string read FAuthorization write FAuthorization;
  end;

implementation

uses
  System.SysUtils,
  System.NetConsts,
  System.Net.HttpClient,
  System.Net.URLClient;

function TApi.Get: TJSonValue;
var
  HttpClient: THTTPClient;
  Response: IHTTPResponse;
begin
  HttpClient := THTTPClient.Create;
  try
    HttpClient.ContentType := 'application/json';
    HttpClient.AcceptCharSet := 'utf-8';
    HttpClient.CustomHeaders['Authorization'] := 'Bearer ' + Authorization;
    Response := HttpClient.Get(FURL, nil, nil);

    Result := TJSonObject.ParseJSONValue(Response.ContentAsString());
  finally
    HttpClient.Free;
  end;
end;

function TApi.Post(const pJson: string): TJSonValue;
var
  HttpClient: THTTPClient;
  ST: TStream;
  Response: IHTTPResponse;
begin
  HttpClient := THTTPClient.Create;
  try
    HttpClient.ContentType := 'application/json';
    if not Authorization.IsEmpty then
      HttpClient.CustomHeaders['Authorization'] := 'Bearer ' + Authorization;
    HttpClient.AcceptCharSet := 'utf-8';
    ST := TStringStream.Create(pJson);
    try
      Response := HttpClient.Post(FURL, ST, nil, nil);

      Result := TJSonObject.ParseJSONValue(Response.ContentAsString());
    finally
      ST.Free;
    end;
  finally
    HttpClient.Free;
  end;
end;

end.
