unit Hiva.Cliente;

interface

uses
  Pkg.Json.DTO, System.Generics.Collections, REST.Json.Types;

{$M+}

type
  TClienteHiva = class
  private
    FClientType: string;
    FCompanyId: string;
    FCompanyName: string;
    FCpfCnpj: string;
    FRegisterId: Integer;
    FStateRegistration: string;
    FTradeName: string;
    FCellNumber: string;
  published
    property ClientType: string read FClientType write FClientType;
    property CompanyId: string read FCompanyId write FCompanyId;
    property CompanyName: string read FCompanyName write FCompanyName;
    property CpfCnpj: string read FCpfCnpj write FCpfCnpj;
    property RegisterId: Integer read FRegisterId write FRegisterId;
    property StateRegistration: string read FStateRegistration write FStateRegistration;
    property TradeName: string read FTradeName write FTradeName;
    property CellNumber: string read FCellNumber write FCellNumber;
  end;

  TListaClienteHiva = class(TJsonDTO)
  private
    [JSONName('clients'), JSONMarshalled(False)]
    FClientArray: TArray<TClienteHiva>;
    [GenericListReflect]
    [JSONName('clients')]
    FCliente: TObjectList<TClienteHiva>;
    function GeTClienteHiva: TObjectList<TClienteHiva>;
  protected
    function GetAsJson: string; override;
  published
    property Cliente: TObjectList<TClienteHiva> read GeTClienteHiva;
  public
    destructor Destroy; override;
  end;

implementation

{ TListaClienteHiva }

destructor TListaClienteHiva.Destroy;
begin
  GeTClienteHiva.Free;
  inherited;
end;

function TListaClienteHiva.GeTClienteHiva: TObjectList<TClienteHiva>;
begin
  Result := ObjectList<TClienteHiva>(FCliente, FClientArray);
end;

function TListaClienteHiva.GetAsJson: string;
begin
  RefreshArray<TClienteHiva>(FCliente, FClientArray);
  Result := inherited;
end;

end.
