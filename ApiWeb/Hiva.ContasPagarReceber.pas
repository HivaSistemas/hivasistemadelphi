unit Hiva.ContasPagarReceber;

interface

uses
  Pkg.Json.DTO, System.Generics.Collections, REST.Json.Types;

{$M+}

type
  TContasPagarReceberHiva= class(TJsonDTO)
  private
    FTotalPay: Double;
    FTotalReceivable: Double;
  published
    property TotalPay: Double read FTotalPay write FTotalPay;
    property TotalReceivable: Double read FTotalReceivable write FTotalReceivable;
  end;

implementation

end.
