unit Hiva.Token;

interface

uses
  Pkg.Json.DTO, System.Generics.Collections, REST.Json.Types;

{$M+}

type
  TTokenHiva= class(TJsonDTO)
  private
    FToken: string;
  published
    property Token: string read FToken write FToken;
  end;

implementation

end.
