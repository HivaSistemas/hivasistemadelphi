unit Hiva.Usuario;

interface

uses
  Pkg.Json.DTO, System.Generics.Collections, REST.Json.Types;

{$M+}

type
  TUsuarioHiva = class
  private
    FRegisterId: integer;
    FCompanyId: string;
    FEmail: string;
    FName: string;
    FPassword: string;
    FUserName: string;
  published
    property RegisterId: integer read FRegisterId write FRegisterId;
    property CompanyId: string read FCompanyId write FCompanyId;
    property Email: string read FEmail write FEmail;
    property Name: string read FName write FName;
    property Password: string read FPassword write FPassword;
    property UserName: string read FUserName write FUserName;
  end;

  TListaUsuarioHiva = class(TJsonDTO)
  private
    [JSONName('users'), JSONMarshalled(False)]
    FListArray: TArray<TUsuarioHiva>;
    [GenericListReflect]
    [JsonName('users')]
    FUsuario: TObjectList<TUsuarioHiva>;
    function GetList: TObjectList<TUsuarioHiva>;
  protected
    function GetAsJson: string; override;
  published
    property Usuario: TObjectList<TUsuarioHiva> read GetList;
  public
    destructor Destroy; override;
  end;


implementation

{ TListaUsuarioHiva }

destructor TListaUsuarioHiva.Destroy;
begin
  GetList.Free;
  inherited;
end;

function TListaUsuarioHiva.GetAsJson: string;
begin
  RefreshArray<TUsuarioHiva>(FUsuario, FListArray);
  Result := inherited;
end;

function TListaUsuarioHiva.GetList: TObjectList<TUsuarioHiva>;
begin
  Result := ObjectList<TUsuarioHiva>(FUsuario, FListArray);
end;

end.
