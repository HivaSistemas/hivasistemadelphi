unit Hiva.TipoCondicao;

interface

uses
  Pkg.Json.DTO, System.Generics.Collections, REST.Json.Types;

{$M+}

type
  TTipoCondicaoHiva = class
  private
    FCompanyId: string;
    FConditionId: integer;
    FName: string;
    FIsPromotion: boolean;
    FIndexAddition: double;
  published
    property CompanyId: string read FCompanyId write FCompanyId;
    property ConditionId: integer read FConditionId write FConditionId;
    property Name: string read FName write FName;
    property IsPromotion: boolean read FIsPromotion write FIsPromotion;
    property IndexAddition: double read FIndexAddition write FIndexAddition;

  end;

  TListaTipoCondicaoHiva = class(TJsonDTO)
  private
    [JSONName('conditions'), JSONMarshalled(False)]
    FListArray: TArray<TTipoCondicaoHiva>;
    [GenericListReflect]
    [JsonName('conditions')]
    FTipoCondicao: TObjectList<TTipoCondicaoHiva>;
    function GetList: TObjectList<TTipoCondicaoHiva>;
  protected
    function GetAsJson: string; override;
  published
    property TipoCondicao: TObjectList<TTipoCondicaoHiva> read GetList;
  public
    destructor Destroy; override;
  end;


implementation

{ TListaUsuarioHiva }

destructor TListaTipoCondicaoHiva.Destroy;
begin
  GetList.Free;
  inherited;
end;

function TListaTipoCondicaoHiva.GetAsJson: string;
begin
  RefreshArray<TTipoCondicaoHiva>(FTipoCondicao, FListArray);
  Result := inherited;
end;

function TListaTipoCondicaoHiva.GetList: TObjectList<TTipoCondicaoHiva>;
begin
  Result := ObjectList<TTipoCondicaoHiva>(FTipoCondicao, FListArray);
end;

end.
