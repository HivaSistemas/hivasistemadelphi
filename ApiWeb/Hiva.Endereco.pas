unit Hiva.Endereco;

interface

uses
  Pkg.Json.DTO, System.Generics.Collections, REST.Json.Types;

{$M+}

type
  TEnderecoHiva = class
  private
    FZipCode: string;
    FStreet: string;
    FComplement: string;
    FNumber: string;
    FDistrict: string;
    FReference: string;
    FCity: string;
    FState: string;
    FRegisterId: Integer;
    FStateRegistration: string;
  published
    property ZipCode: string read FZipCode write FZipCode;
    property Street: string read FStreet write FStreet;
    property Complement: string read FComplement write FComplement;
    property Number: string read FNumber write FNumber;
    property District: string read FDistrict write FDistrict;
    property Reference: string read FReference write FReference;
    property City: string read FCity write FCity;
    property State: string read FState write FState;
    property RegisterId: Integer read FRegisterId write FRegisterId;
    property StateRegistration: string read FStateRegistration write FStateRegistration;
  end;

  TListaEnderecoHiva = class(TJsonDTO)
  private
    [JSONName('addresses'), JSONMarshalled(False)]
    FEnderecoArray: TArray<TEnderecoHiva>;
    [GenericListReflect]
    [JSONName('addresses')]
    FEndereco: TObjectList<TEnderecoHiva>;
    function GeTEnderecoHiva: TObjectList<TEnderecoHiva>;
  protected
    function GetAsJson: string; override;
  published
    property Endereco: TObjectList<TEnderecoHiva> read GeTEnderecoHiva;
  public
    destructor Destroy; override;
  end;

implementation

{ TListaEnderecoHiva }

destructor TListaEnderecoHiva.Destroy;
begin
  GeTEnderecoHiva.Free;
  inherited;
end;

function TListaEnderecoHiva.GeTEnderecoHiva: TObjectList<TEnderecoHiva>;
begin
  Result := ObjectList<TEnderecoHiva>(FEndereco, FEnderecoArray);
end;

function TListaEnderecoHiva.GetAsJson: string;
begin
  RefreshArray<TEnderecoHiva>(FEndereco, FEnderecoArray);
  Result := inherited;
end;

end.
