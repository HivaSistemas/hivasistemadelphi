unit ctrlHivaApi;

interface

uses
  Api.Base, Pkg.Json.DTO, System.JSON, REST.Json, System.SysUtils,
  Hiva.Token,  Hiva.Retorno, Hiva.Retorno.Cliente, Hiva.Retorno.Pedido;

type
  THivaApi = class
    function Exportar(pListaObj: TJsonDTO): TRetornoHiva;
    function ImportarCliente: TListaRetornoClienteHiva;
    function ImportarPedido: TListaRetornoPedidoHiva;
    function Login(pUser : TJsonDTO) : TTokenHiva;

  private
    FApi: TApi;
    FUrlApi: String;
    FChaveAPi: String;
  published
    Property UrlApi: String read FUrlApi write FUrlApi;
    property ChaveAPi : String read FChaveAPi write FChaveAPi;
 public
   Constructor Create;
   Destructor Destroy;
  end;
implementation

{ TSeniorApi }


constructor THivaApi.Create;
begin
  FApi := TApi.Create(nil);
end;

destructor THivaApi.Destroy;
begin
  FApi.Free;
end;

function THivaApi.Exportar(pListaObj: TJsonDTO): TRetornoHiva;
var
  JSonValue : TJSonValue;
  vObjJson: string;
begin

  FApi.URL := FUrlApi;
  FApi.Authorization := FChaveAPi;

  vObjJson := TJson.ObjectToJsonString(pListaObj);
  vObjJson := vObjJson.Replace('\/','/');
  JSonValue := FApi.Post(vObjJson);
  result := TJson.JsonToObject<TRetornoHiva>(JSonValue.ToJSON);
end;


function THivaApi.Login(pUser: TJsonDTO): TTokenHiva;
var
  JSonValue : TJSonValue;
  vObjJson: string;
begin
  FApi.URL := FUrlApi;
  vObjJson := TJson.ObjectToJsonString(pUser);
  JSonValue := FApi.Post(vObjJson);

  result := TJson.JsonToObject<TTokenHiva>(JSonValue.ToJSON);

end;

function THivaApi.ImportarCliente: TListaRetornoClienteHiva;
var
  JSonValue : TJSonValue;
begin

  FApi.URL := FUrlApi;
  FApi.Authorization := FChaveAPi;

  JSonValue := FApi.Get;
  result := TJson.JsonToObject<TListaRetornoClienteHiva>(JSonValue.ToJSON);
end;

function THivaApi.ImportarPedido: TListaRetornoPedidoHiva;
var
  JSonValue : TJSonValue;
begin

  FApi.URL := FUrlApi;
  FApi.Authorization := FChaveAPi;

  JSonValue := FApi.Get;

  result := TJson.JsonToObject<TListaRetornoPedidoHiva>(JSonValue.ToJSON);


end;

end.
