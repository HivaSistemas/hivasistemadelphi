unit Hiva.Retorno;

interface

uses
  Pkg.Json.DTO, System.Generics.Collections, REST.Json.Types;

{$M+}

type
  TRetornoHiva = class(TJsonDTO)
  private
    FMessage: string;
    FSuccess: Boolean;
  published
    property Message: string read FMessage write FMessage;
    property Success: Boolean read FSuccess write FSuccess;
  end;

implementation

end.

