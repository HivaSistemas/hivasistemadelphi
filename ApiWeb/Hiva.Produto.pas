unit Hiva.Produto;

interface

uses
  Pkg.Json.DTO, System.Generics.Collections, REST.Json.Types;

{$M+}

type
  TProdutoHiva = class
  private
    FCode: integer;
    FName: string;
    FMark: string;
    FUnd: string;
    FPrice: Double;
    FCompanyId: string;
    FPromotionPrice: Double;
  published
    property Code: integer read FCode write FCode;
    property Name: string read FName write FName;
    property Mark: string read FMark write FMark;
    property Und: string read FUnd write FUnd;
    property Price: Double read FPrice write FPrice;
    property CompanyId: string read FCompanyId write FCompanyId;
    property PromotionPrice: Double read FPromotionPrice write FPromotionPrice;
  end;

  TListaProdutoHiva = class(TJsonDTO)
  private
    [JSONName('products'), JSONMarshalled(False)]
    FClientArray: TArray<TProdutoHiva>;
    [GenericListReflect]
    [JSONName('products')]
    FProduto: TObjectList<TProdutoHiva>;
    function GeTProdutoHiva: TObjectList<TProdutoHiva>;
  protected
    function GetAsJson: string; override;
  published
    property Produto: TObjectList<TProdutoHiva> read GeTProdutoHiva;
  public
    destructor Destroy; override;
  end;

implementation

{ TListaProdutoHiva }

destructor TListaProdutoHiva.Destroy;
begin
  GeTProdutoHiva.Free;
  inherited;
end;

function TListaProdutoHiva.GeTProdutoHiva: TObjectList<TProdutoHiva>;
begin
  Result := ObjectList<TProdutoHiva>(FProduto, FClientArray);
end;

function TListaProdutoHiva.GetAsJson: string;
begin
  RefreshArray<TProdutoHiva>(FProduto, FClientArray);
  Result := inherited;
end;

end.
