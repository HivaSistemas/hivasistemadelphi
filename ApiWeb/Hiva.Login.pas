unit Hiva.Login;

interface

uses
  Pkg.Json.DTO, System.Generics.Collections, REST.Json.Types;

{$M+}

type
  TLoginHiva= class(TJsonDTO)
  private
    FName: string;
    FPassword: string;
  published
    property Name: string read FName write FName;
    property Password: string read FPassword write FPassword;
  end;

implementation

end.
