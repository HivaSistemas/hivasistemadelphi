unit Hiva.Retorno.Pedido;

interface

uses
  Pkg.Json.DTO, System.Generics.Collections, REST.Json.Types;

{$M+}

type
  TAddress = class;
  TAddressType = class;
  TClient = class;
  TCondition = class;
  TItems = class;
  TProduct = class;

  TProduct = class
  private
    FCode: Integer;
  published
    property Code: Integer read FCode write FCode;
  end;

  TItems = class
  private
    FPrice: Double;
    FProduct: TProduct;
    FQuantity: Integer;
    FTotal: Double;
  published
    property Price: Double read FPrice write FPrice;
    property Product: TProduct read FProduct;
    property Quantity: Integer read FQuantity write FQuantity;
    property Total: Double read FTotal write FTotal;
  public
    constructor Create;
    destructor Destroy; override;
  end;

  TAddressType = class
  private
    FDescription: string;
  published
    property Description: string read FDescription write FDescription;
  end;

  TAddress = class
  private
    FAddressType: TAddressType;
    FCity: string;
    FComplement: string;
    FDistrict: string;
    FNumber: string;
    FReference: string;
    FState: string;
    FStreet: string;
    [JSONName('zip_code')]
    FZipCode: string;
    FStateRegistration: string;
  published
    property AddressType: TAddressType read FAddressType;
    property City: string read FCity write FCity;
    property Complement: string read FComplement write FComplement;
    property District: string read FDistrict write FDistrict;
    property Number: string read FNumber write FNumber;
    property Reference: string read FReference write FReference;
    property State: string read FState write FState;
    property Street: string read FStreet write FStreet;
    property ZipCode: string read FZipCode write FZipCode;
    property StateRegistration: string read FStateRegistration write FStateRegistration;
  public
    constructor Create;
    destructor Destroy; override;
  end;

  TClient = class(TJsonDTO)
  private
    [JSONName('address'), JSONMarshalled(False)]
    FAddressArray: TArray<TAddress>;
    [GenericListReflect]
    FAddress: TObjectList<TAddress>;
    [JSONName('cell_number')]
    FCellNumber: string;
    [JSONName('client_type')]
    FClientType: string;
    [JSONName('company_id')]
    FCompanyId: string;
    [JSONName('company_name')]
    FCompanyName: string;
    [JSONName('cpf_cnpj')]
    FCpfCnpj: string;
    FId: string;
    [JSONName('register_id')]
    FRegisterId: Integer;
    [JSONName('state_registration')]
    FStateRegistration: string;
    [JSONName('trade_name')]
    FTradeName: string;
    function GetAddress: TObjectList<TAddress>;
  protected
    function GetAsJson: string; override;
  published
    property Address: TObjectList<TAddress> read GetAddress;
    property CellNumber: string read FCellNumber write FCellNumber;
    property ClientType: string read FClientType write FClientType;
    property CompanyId: string read FCompanyId write FCompanyId;
    property CompanyName: string read FCompanyName write FCompanyName;
    property CpfCnpj: string read FCpfCnpj write FCpfCnpj;
    property Id: string read FId write FId;
    property RegisterId: Integer read FRegisterId write FRegisterId;
    property StateRegistration: string read FStateRegistration write FStateRegistration;
    property TradeName: string read FTradeName write FTradeName;
  public
    destructor Destroy; override;
  end;

  TCondition = class
  private
    [JSONName('condition_id')]
    FConditionId: Integer;
    [JSONName('index_addition')]
    FIndexAddition: Double;
  published
    property ConditionId: Integer read FConditionId write FConditionId;
    property IndexAddition: Double read FIndexAddition write FIndexAddition;
  end;

  TOrders = class(TJsonDTO)
  private
    FAddress: TAddress;
    FClient: TClient;
    [JSONName('client_Id')]
    FClientId: string;
    FCondition: TCondition;
    [JSONName('condition_Id')]
    FConditionId: string;
    FDifference: Integer;
    FDiscount: Integer;
    [SuppressZero, JSONName('estimated_delivery')]
    FEstimatedDelivery: TDateTime;
    FFreight: Integer;
    [JSONName('items'), JSONMarshalled(False)]
    FItemsArray: TArray<TItems>;
    [GenericListReflect]
    FItems: TObjectList<TItems>;
    FMoney: Double;
    FObservation: string;
    [JSONName('other_expenses')]
    FOtherExpenses: Integer;
    [JSONName('pix_transfer')]
    FPixTransfer: Integer;
    FTotal: Double;
    [JSONName('user_Id')]
    FUserId: string;
    function GetItems: TObjectList<TItems>;
  protected
    function GetAsJson: string; override;
  published
    property Address: TAddress read FAddress;
    property Client: TClient read FClient;
    property ClientId: string read FClientId write FClientId;
    property Condition: TCondition read FCondition;
    property ConditionId: string read FConditionId write FConditionId;
    property Difference: Integer read FDifference write FDifference;
    property Discount: Integer read FDiscount write FDiscount;
    property EstimatedDelivery: TDateTime read FEstimatedDelivery write FEstimatedDelivery;
    property Freight: Integer read FFreight write FFreight;
    property Items: TObjectList<TItems> read GetItems;
    property Money: Double read FMoney write FMoney;
    property Observation: string read FObservation write FObservation;
    property OtherExpenses: Integer read FOtherExpenses write FOtherExpenses;
    property PixTransfer: Integer read FPixTransfer write FPixTransfer;
    property Total: Double read FTotal write FTotal;
    property UserId: string read FUserId write FUserId;
  public
    constructor Create; override;
    destructor Destroy; override;
  end;

  TListaRetornoPedidoHiva = class(TJsonDTO)
  private
    [JSONName('orders'), JSONMarshalled(False)]
    FOrdersArray: TArray<TOrders>;
    [GenericListReflect]
    FOrders: TObjectList<TOrders>;
    function GetOrders: TObjectList<TOrders>;
  protected
    function GetAsJson: string; override;
  published
    property Orders: TObjectList<TOrders> read GetOrders;
  public
    destructor Destroy; override;
  end;

implementation

{ TItems }

constructor TItems.Create;
begin
  inherited;
  FProduct := TProduct.Create;
end;

destructor TItems.Destroy;
begin
  FProduct.Free;
  inherited;
end;

{ TAddress }

constructor TAddress.Create;
begin
  inherited;
  FAddressType := TAddressType.Create;
end;

destructor TAddress.Destroy;
begin
  FAddressType.Free;
  inherited;
end;

{ TClient }

destructor TClient.Destroy;
begin
  GetAddress.Free;
  inherited;
end;

function TClient.GetAddress: TObjectList<TAddress>;
begin
  Result := ObjectList<TAddress>(FAddress, FAddressArray);
end;

function TClient.GetAsJson: string;
begin
  RefreshArray<TAddress>(FAddress, FAddressArray);
  Result := inherited;
end;

{ TOrders }

constructor TOrders.Create;
begin
  inherited;
  FAddress := TAddress.Create;
  FCondition := TCondition.Create;
  FClient := TClient.Create;
end;

destructor TOrders.Destroy;
begin
  FAddress.Free;
  FCondition.Free;
  FClient.Free;
  GetItems.Free;
  inherited;
end;

function TOrders.GetItems: TObjectList<TItems>;
begin
  Result := ObjectList<TItems>(FItems, FItemsArray);
end;

function TOrders.GetAsJson: string;
begin
  RefreshArray<TItems>(FItems, FItemsArray);
  Result := inherited;
end;

{ TListaRetornoPedidoHiva }

destructor TListaRetornoPedidoHiva.Destroy;
begin
  GetOrders.Free;
  inherited;
end;

function TListaRetornoPedidoHiva.GetOrders: TObjectList<TOrders>;
begin
  Result := ObjectList<TOrders>(FOrders, FOrdersArray);
end;

function TListaRetornoPedidoHiva.GetAsJson: string;
begin
  RefreshArray<TOrders>(FOrders, FOrdersArray);
  Result := inherited;
end;

end.


