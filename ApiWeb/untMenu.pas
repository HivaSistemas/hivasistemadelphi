unit untMenu;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,  Hiva.Token,  VarGlobal, Vcl.Themes,
  Vcl.Imaging.pngimage, Vcl.ExtCtrls, System.StrUtils, Data.DB, DBAccess, Ora, Hiva.Endereco,
  _Conexao, _Criptografia, System.Types, System.IniFiles, _RecordsEspeciais, _Clientes,
  _RecordsCadastros,  _ClientesFiliais, _RecordsOrcamentosVendas, System.Generics.Collections,
  Hiva.Retorno.Pedido, Horse;

  const cUrlWeb = 'https://hivamobile-nu.vercel.app/api/';

type
  TfrmAPiWeb = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    ImageIcone: TImage;
    LabelNome: TLabel;
    mmLog: TMemo;
    btnIntegrar: TButton;
    Timer: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure btnIntegrarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TimerTimer(Sender: TObject);
  private
    { Private declarations }
    FCriptografia: TCriptografia;
    FConexaoBanco: TConexao;
    objToken : TTokenHiva;
    vQryParametros : TOraQuery;
    procedure AddLog(pMensagem : string; pSalvar: Boolean = False);
    procedure IniciarConexaoBanco;
    procedure CarregarEstilo;
    procedure Login;
    procedure exportarUsuario;
    procedure exportarCliente;
    procedure exportarTipoCondicao;
    function  exportarEndereco(pCadastroId: integer): TListaEnderecoHiva;
    procedure exportarContasPagarReceber;
    procedure exportarProduto;
    function  getItens(pListaItens: TObjectList<TItems> ): TArray<RecOrcamentoItens>;
    function  getCliente(pCliente: TClient): Integer;
    procedure importarPedido;
  public
    { Public declarations }
  end;

var
  frmAPiWeb: TfrmAPiWeb;

implementation

{$R *.dfm}

uses ctrlHivaApi, Hiva.Login, Hiva.Usuario, Hiva.Retorno, Hiva.Cliente,
  Hiva.TipoCondicao, Hiva.Produto, Hiva.Retorno.Cliente, _Orcamentos,
  Hiva.ContasPagarReceber;

procedure TfrmAPiWeb.AddLog(pMensagem: string; pSalvar: Boolean);

  procedure GetLog();
  begin
    if not Assigned(_Log) then
    begin
      _Log := TStringList.Create;
    end;

    if not FileExists(_LocalEXE + _LogName + '_' + FormatDateTime('dd-MM-yyyy', Date) + '.txt') then
    begin
      _Log.Clear;
      _Log.SaveToFile(_LocalEXE + _LogName + '_' + FormatDateTime('dd-MM-yyyy', Date) + '.txt');
    end;

    _Log.LoadFromFile(_LocalEXE + _LogName + '_' + FormatDateTime('dd-MM-yyyy', Date) + '.txt');
  end;

begin

  if not _GerarLog then
  begin
    Exit;
  end;

  if _LogName = '' then
  begin
    raise Exception.Create('Favor informar o nome do log!');
  end;
  if pSalvar then
  begin
    try
      GetLog;
      _Log.Add('---['+ DateTimeToStr(Now)+']---' + pMensagem);
      _Log.Add(''.PadLeft(200,'-'));

      _Log.SaveToFile(_LocalEXE + _LogName + '_' + FormatDateTime('dd-MM-yyyy', Date) + '.txt');
    except
      //N�o exibir erro
    end;
  //
  end;
  if mmLog.Lines.Count > 1000 then
    mmLog.Clear;

  mmLog.Lines.Add('---['+ DateTimeToStr(Now)+']---' + pMensagem);
  mmLog.Lines.Add(''.PadLeft(165,'-'));
end;

procedure TfrmAPiWeb.btnIntegrarClick(Sender: TObject);
var
  vQry : TOraQuery;
begin
  try
    Timer.Enabled := False;
    try
      Login;
      exportarUsuario;
      exportarCliente;
      exportarTipoCondicao;
      exportarProduto;
      exportarContasPagarReceber;
      importarPedido;
    except
      on e: Exception do begin
        AddLog('Erro: '+ e.Message);
        exit;
      end;
    end;

    vQry := TOraQuery.Create(nil);
    vQry.Session := FConexaoBanco;

    try
      FConexaoBanco.IniciarTransacao;

      vQry.SQL.Add('update PARAMETROS set DATA_ULTIMA_INTEGRACAO_WEB = SYSDATE');
      vQry.ExecSQL;

      FConexaoBanco.FinalizarTransacao;
    except
      on e: Exception do begin
        FConexaoBanco.VoltarTransacao;
      end;
    end;
    vQry.Free;
  finally
    Timer.Enabled := True;
  end;
end;

procedure TfrmAPiWeb.CarregarEstilo;
var
  vEstilo: String;
  ResStream: TResourceStream;
  Style: TObject;
begin
  try
    vEstilo := 'SapphireKamri';
    vEstilo := ReplaceStr(vEstilo, ' ', '');
    if not FileExists(_LocalExe + vEstilo + '.vsf') then
    begin
      ResStream := TResourceStream.Create(HInstance, vEstilo, RT_RCDATA);
      try
        ResStream.Position := 0;
        ResStream.SaveToFile(_LocalExe + vEstilo + '.vsf');
      finally
        ResStream.Free;
      end;
    end;

    if FileExists(_LocalExe + vEstilo + '.vsf') then
    begin
      Style := TStyleManager.LoadFromFile(_LocalExe + vEstilo + '.vsf');
      TStyleManager.SetStyle(Style);
    end;
  except
    AddLog('Houve um problema ao setar o estilo da aplica��o. O sistema ser� aberto com o estilo padr�o.', True);

  end;
end;

procedure TfrmAPiWeb.exportarCliente;
var
  ctrlHivaApi : THivaApi;
  objListaCliente : TListaClienteHiva;
  objListaEndereco : TListaEnderecoHiva;
  objListaEnderecoTemp : TListaEnderecoHiva;
  objCliente : TClienteHiva;
  objEndereco : TEnderecoHiva;
  objRetorno : TRetornoHiva;
  qryCliente : TOraQuery;
  vCnt: Integer;

begin

  objListaCliente := TListaClienteHiva.Create;
  objListaEndereco := TListaEnderecoHiva.Create;
  objRetorno   := TRetornoHiva.Create;
  ctrlHivaApi := THivaApi.Create;
  qryCliente := TOraQuery.Create(nil);
  ctrlHivaApi.ChaveAPi := objToken.Token;

  try
    qryCliente.Session := FConexaoBanco;
    qryCliente.Close;
    qryCliente.SQL.Clear;
    qryCliente.SQL.Add('select cad.cadastro_id, ');
    qryCliente.SQL.Add('       cad.cpf_cnpj, ');
    qryCliente.SQL.Add('       cad.razao_social, ');
    qryCliente.SQL.Add('       cad.nome_fantasia, ');
    qryCliente.SQL.Add('       cad.inscricao_estadual, ');
    qryCliente.SQL.Add('       cad.cep, ');
    qryCliente.SQL.Add('       cad.logradouro, ');
    qryCliente.SQL.Add('       cad.complemento, ');
    qryCliente.SQL.Add('       cad.numero, ');
    qryCliente.SQL.Add('       cad.ponto_referencia, ');
    qryCliente.SQL.Add('       cad.telefone_celular, ');
    qryCliente.SQL.Add('       bar.nome               as bairro, ');
    qryCliente.SQL.Add('       cid.nome               as cidade, ');
    qryCliente.SQL.Add('       cid.estado_id          as estado ');
    qryCliente.SQL.Add('  from cadastros cad, clientes cli, bairros bar, cidades cid ');
    qryCliente.SQL.Add(' where cad.cadastro_id = cli.cadastro_id ');
    qryCliente.SQL.Add('   and cli.data_hora_alteracao > to_date('+vQryParametros.FieldByName('DATA_ULTIMA_INTEGRACAO_WEB').AsString.QuotedString+' ,''dd/mm/yyyy hh24:mi:ss'' ) ');
    qryCliente.SQL.Add('   and cad.bairro_id = bar.bairro_id ');
    qryCliente.SQL.Add('   and bar.cidade_id = cid.cidade_id ');
    qryCliente.SQL.Add('   and cad.ativo = ''S'' ');
    qryCliente.SQL.Add('   and cli.exportar_web = ''S'' ');

    qryCliente.Open;

    if qryCliente.IsEmpty then
      exit;

    qryCliente.First;
    while not qryCliente.Eof do
    begin
      objCliente := TClienteHiva.Create;
      objCliente.CompanyName       := qryCliente.FieldByName('razao_social').AsString;
      objCliente.CpfCnpj           := qryCliente.FieldByName('cpf_cnpj').AsString;
      objCliente.RegisterId        := qryCliente.FieldByName('cadastro_id').AsInteger;
      objCliente.StateRegistration := qryCliente.FieldByName('inscricao_estadual').AsString;
      objCliente.TradeName         := qryCliente.FieldByName('nome_fantasia').AsString;
      //objCliente.CellNumber      := qryCliente.FieldByName('telefone_celular').AsString;
      objCliente.CompanyId         := '1';
      objCliente.ClientType        := 'NC';

      objListaCliente.Cliente.Add(objCliente);

      objEndereco := TEnderecoHiva.Create;
      objEndereco.RegisterId        := qryCliente.FieldByName('cadastro_id').AsInteger;
      objEndereco.ZipCode           := qryCliente.FieldByName('cep').AsString;
      objEndereco.Street            := qryCliente.FieldByName('logradouro').AsString;
      objEndereco.Complement        := qryCliente.FieldByName('complemento').AsString;
      objEndereco.Number            := qryCliente.FieldByName('numero').AsString;
      objEndereco.District          := qryCliente.FieldByName('bairro').AsString;
      objEndereco.Reference         := qryCliente.FieldByName('ponto_referencia').AsString;
      objEndereco.City              := qryCliente.FieldByName('cidade').AsString;
      objEndereco.State             := qryCliente.FieldByName('estado').AsString;
      objEndereco.StateRegistration := qryCliente.FieldByName('inscricao_estadual').AsString;

      objListaEndereco.Endereco.Add(objEndereco);
      objListaEnderecoTemp := exportarEndereco(qryCliente.FieldByName('cadastro_id').AsInteger);
      for vCnt := 0 to objListaEnderecoTemp.Endereco.Count - 1 do
      begin
        objListaEndereco.Endereco.Add(objListaEnderecoTemp.Endereco.Items[vCnt]);
      end;


      qryCliente.Next;
    end;

    ctrlHivaApi.UrlApi   := cUrlWeb + 'client/create-all';
    objRetorno := ctrlHivaApi.Exportar(objListaCliente);
    if objRetorno.Message.Contains('login') then
    begin
      objToken.Token := '';
      Login;
    end;

    AddLog('Cliente: ' + objRetorno.Message);

    ctrlHivaApi.UrlApi   := cUrlWeb + 'address/create-all';
    objRetorno := ctrlHivaApi.Exportar(objListaEndereco);
    AddLog('Cliente Endere�o: ' + objRetorno.Message);



  finally
    ctrlHivaApi.Free;
    qryCliente.Free;
    objRetorno.Free;
    objListaCliente.Free;
    //objListaEnderecoTemp.Free;
    objListaEndereco.Free;
  end;
end;

procedure TfrmAPiWeb.exportarContasPagarReceber;
var
  ctrlHivaApi : THivaApi;
  objContaPagarReceber : TContasPagarReceberHiva;
  objRetorno : TRetornoHiva;
  qryContas: TOraQuery;

begin

  objRetorno   := TRetornoHiva.Create;
  ctrlHivaApi := THivaApi.Create;
  qryContas := TOraQuery.Create(self);
  ctrlHivaApi.ChaveAPi := objToken.Token;
  ctrlHivaApi.UrlApi   := cUrlWeb + 'financial/create';

  try
    objContaPagarReceber := TContasPagarReceberHiva.Create;
    qryContas.Session := FConexaoBanco;
    //contas a receber
    qryContas.Close;
    qryContas.SQL.Clear;
    qryContas.SQL.Add('select sum(r.valor_documento) as total ');
    qryContas.SQL.Add('  from contas_receber r ');
    qryContas.SQL.Add(' where trunc(r.data_vencimento) = trunc(sysdate) ');
    qryContas.SQL.Add('   and r.status = ''A'' ');
    qryContas.Open;

    objContaPagarReceber.TotalReceivable   := qryContas.FieldByName('total').AsFloat;
    //contas a pagar
    qryContas.Close;
    qryContas.SQL.Clear;
    qryContas.SQL.Add('select sum(r.valor_documento) as total ');
    qryContas.SQL.Add('  from contas_pagar r ');
    qryContas.SQL.Add(' where trunc(r.data_vencimento) = trunc(sysdate) ');
    qryContas.SQL.Add('   and r.status = ''A'' ');
    qryContas.Open;

    objContaPagarReceber.TotalPay   := qryContas.FieldByName('total').AsFloat;

    objRetorno := ctrlHivaApi.Exportar(objContaPagarReceber);
    if objRetorno.Message.Contains('login') then
    begin
      objToken.Token := '';
      Login;
    end;

    AddLog('Contas pagar e receber: ' + objRetorno.Message);


  finally
    ctrlHivaApi.Free;
    qryContas.Free;
    objRetorno.Free;
    objContaPagarReceber.Free;
  end;
end;

function TfrmAPiWeb.exportarEndereco(pCadastroId: integer) : TListaEnderecoHiva;
var
  objEndereco : TEnderecoHiva;
  qryEndereco: TOraQuery;

begin

  result := TListaEnderecoHiva.Create;
  qryEndereco := TOraQuery.Create(nil);

  try
    qryEndereco.Session := FConexaoBanco;
    qryEndereco.Close;
    qryEndereco.SQL.Clear;
    qryEndereco.SQL.Add('select e.cep, ');
    qryEndereco.SQL.Add('       e.logradouro, ');
    qryEndereco.SQL.Add('       e.complemento, ');
    qryEndereco.SQL.Add('       e.numero, ');
    qryEndereco.SQL.Add('       b.nome               as bairro, ');
    qryEndereco.SQL.Add('       e.ponto_referencia, ');
    qryEndereco.SQL.Add('       c.nome               as cidade, ');
    qryEndereco.SQL.Add('       c.estado_id          as estado, ');
    qryEndereco.SQL.Add('       e.inscricao_estadual, ');
    qryEndereco.SQL.Add('       e.cadastro_id, ');
    qryEndereco.SQL.Add('       tipo_endereco ');
    qryEndereco.SQL.Add('  from diversos_enderecos e, bairros b, cidades c ');
    qryEndereco.SQL.Add(' where e.bairro_id = b.bairro_id(+) ');
    qryEndereco.SQL.Add('   and b.cidade_id = c.cidade_id(+) ');
    qryEndereco.SQL.Add('   and e.cadastro_id = '+ pCadastroId.ToString );

    qryEndereco.Open;

    if qryEndereco.IsEmpty then
      exit;

    qryEndereco.First;
    while not qryEndereco.Eof do
    begin

      objEndereco := TEnderecoHiva.Create;
      objEndereco.RegisterId        := qryEndereco.FieldByName('cadastro_id').AsInteger;
      objEndereco.ZipCode           := qryEndereco.FieldByName('cep').AsString;
      objEndereco.Street            := qryEndereco.FieldByName('logradouro').AsString;
      objEndereco.Complement        := qryEndereco.FieldByName('complemento').AsString;
      objEndereco.Number            := qryEndereco.FieldByName('numero').AsString;
      objEndereco.District          := qryEndereco.FieldByName('bairro').AsString;
      objEndereco.Reference         := qryEndereco.FieldByName('ponto_referencia').AsString;
      objEndereco.City              := qryEndereco.FieldByName('cidade').AsString;
      objEndereco.State             := qryEndereco.FieldByName('estado').AsString;
      objEndereco.StateRegistration := qryEndereco.FieldByName('inscricao_estadual').AsString;

      result.Endereco.Add(objEndereco);

      qryEndereco.Next;
    end;

  finally
    qryEndereco.Free;
  end;
end;

procedure TfrmAPiWeb.exportarProduto;
var
  ctrlHivaApi : THivaApi;
  objListaProduto : TListaProdutoHiva;
  objProduto : TProdutoHiva;
  objRetorno : TRetornoHiva;
  qryProduto : TOraQuery;

begin

  objListaProduto := TListaProdutoHiva.Create;
  objRetorno   := TRetornoHiva.Create;
  ctrlHivaApi := THivaApi.Create;
  qryProduto := TOraQuery.Create(self);
  ctrlHivaApi.ChaveAPi := objToken.Token;
  ctrlHivaApi.UrlApi   := cUrlWeb + 'product/create';

  try
    qryProduto.Session := FConexaoBanco;
    qryProduto.Close;
    qryProduto.SQL.Clear;
    qryProduto.SQL.Add('select p.produto_id, ');
    qryProduto.SQL.Add('       p.nome, ');
    qryProduto.SQL.Add('       p.unidade_venda, ');
    qryProduto.SQL.Add('       pp.preco_varejo, ');
    qryProduto.SQL.Add('       pro.preco_promocional, ');
    qryProduto.SQL.Add('       m.nome as marca, ');
    qryProduto.SQL.Add('       est.empresa_id ');
    qryProduto.SQL.Add('  from produtos           p, ');
    qryProduto.SQL.Add('       estoques           est, ');
    qryProduto.SQL.Add('       precos_produtos    pp, ');
    qryProduto.SQL.Add('       produtos_promocoes pro, ');
    qryProduto.SQL.Add('       marcas             m ');
    qryProduto.SQL.Add(' where p.produto_id = est.produto_id ');
    qryProduto.SQL.Add('   and est.produto_id = pp.produto_id ');
    qryProduto.SQL.Add('   and est.empresa_id = pp.empresa_id ');
    qryProduto.SQL.Add('   and est.produto_id = pro.produto_id(+) ');
    qryProduto.SQL.Add('   and est.empresa_id = pro.empresa_id(+) ');
    qryProduto.SQL.Add('   and p.marca_id = m.marca_id ');
    qryProduto.SQL.Add('   and p.ativo = ''S'' ');
    qryProduto.SQL.Add('   and p.exportar_web = ''S'' ');
    qryProduto.SQL.Add('   and est.empresa_id = ''1'' ');
    qryProduto.SQL.Add('   and p.data_hora_alteracao > to_date('+vQryParametros.FieldByName('DATA_ULTIMA_INTEGRACAO_WEB').AsString.QuotedString+' ,''dd/mm/yyyy hh24:mi:ss'' ) ');

    qryProduto.Open;

    if qryProduto.IsEmpty then
      exit;

    qryProduto.First;
    while not qryProduto.Eof do
    begin
      objProduto := TProdutoHiva.Create;
      objProduto.CompanyId      := '1';
      objProduto.Code           := qryProduto.FieldByName('produto_id').AsInteger;
      objProduto.Name           := qryProduto.FieldByName('nome').AsString;
      objProduto.Mark           := qryProduto.FieldByName('marca').AsString;
      objProduto.Und            := qryProduto.FieldByName('unidade_venda').AsString;
      objProduto.Price          := qryProduto.FieldByName('preco_varejo').AsFloat;
      objProduto.PromotionPrice := qryProduto.FieldByName('preco_promocional').AsFloat;

      objListaProduto.Produto.Add(objProduto);

      qryProduto.Next;
    end;

    objRetorno := ctrlHivaApi.Exportar(objListaProduto);
    if objRetorno.Message.Contains('login') then
    begin
      objToken.Token := '';
      Login;
    end;

    AddLog('Produtos: ' + objRetorno.Message);


  finally
    ctrlHivaApi.Free;
    qryProduto.Free;
    objRetorno.Free;
    objListaProduto.Free;
  end;
end;

procedure TfrmAPiWeb.exportarTipoCondicao;
var
  ctrlHivaApi : THivaApi;
  objListaCondicao : TListaTipoCondicaoHiva;
  objCondicao : TTipoCondicaoHiva;
  objRetorno : TRetornoHiva;
  qryCondicao : TOraQuery;

begin

  objListaCondicao := TListaTipoCondicaoHiva.Create;
  objRetorno   := TRetornoHiva.Create;
  ctrlHivaApi := THivaApi.Create;
  qryCondicao := TOraQuery.Create(self);
  ctrlHivaApi.ChaveAPi := objToken.Token;
  ctrlHivaApi.UrlApi   := cUrlWeb + 'payment-condition/create';

  try
    qryCondicao.Session := FConexaoBanco;
    qryCondicao.Close;
    qryCondicao.SQL.Clear;
    qryCondicao.SQL.Add('select ');
    qryCondicao.SQL.Add('  condicao_id, ');
    qryCondicao.SQL.Add('  indice_acrescimo, ');
    qryCondicao.SQL.Add('  nome, ');
    qryCondicao.SQL.Add('  permitir_preco_promocional ');
    qryCondicao.SQL.Add('from ');
    qryCondicao.SQL.Add('  condicoes_pagamento ');
    qryCondicao.SQL.Add('  where ativo = ''S'' ');
    qryCondicao.SQL.Add('    and exportar_web = ''S'' ');
    qryCondicao.SQL.Add('    and data_hora_alteracao > to_date('+vQryParametros.FieldByName('DATA_ULTIMA_INTEGRACAO_WEB').AsString.QuotedString+' ,''dd/mm/yyyy hh24:mi:ss'' ) ');

    qryCondicao.Open;

    if qryCondicao.IsEmpty then
      exit;

    qryCondicao.First;
    while not qryCondicao.Eof do
    begin
      objCondicao := TTipoCondicaoHiva.Create;
      objCondicao.CompanyId     := '1';
      objCondicao.ConditionId   := qryCondicao.FieldByName('condicao_id').AsInteger;
      objCondicao.Name          := qryCondicao.FieldByName('nome').AsString;
      objCondicao.IsPromotion   := qryCondicao.FieldByName('permitir_preco_promocional').AsBoolean;
      objCondicao.IndexAddition := qryCondicao.FieldByName('indice_acrescimo').AsFloat;

      objListaCondicao.TipoCondicao.Add(objCondicao);
      qryCondicao.Next;
    end;

    objRetorno := ctrlHivaApi.Exportar(objListaCondicao);
    if objRetorno.Message.Contains('login') then
    begin
      objToken.Token := '';
      Login;
    end;

    AddLog('Tipo de Condi��o: ' + objRetorno.Message);


  finally
    ctrlHivaApi.Free;
    qryCondicao.Free;
    objRetorno.Free;
    objListaCondicao.Free;
  end;
end;

procedure TfrmAPiWeb.exportarUsuario;
var
  ctrlHivaApi : THivaApi;
  objListaUser : TListaUsuarioHiva;
  objUser : TUsuarioHiva;
  objRetorno : TRetornoHiva;
  qryUsuario : TOraQuery;

begin

  objListaUser := TListaUsuarioHiva.Create;
  objRetorno   := TRetornoHiva.Create;
  ctrlHivaApi := THivaApi.Create;
  qryUsuario := TOraQuery.Create(self);
  ctrlHivaApi.ChaveAPi := objToken.Token;
  ctrlHivaApi.UrlApi   := cUrlWeb + 'auth/create';

  try
    qryUsuario.Session := FConexaoBanco;
    qryUsuario.Close;
    qryUsuario.SQL.Clear;
    qryUsuario.SQL.Add('select ');
    qryUsuario.SQL.Add(' f.funcionario_id, ');
    qryUsuario.SQL.Add(' f.nome,');
    qryUsuario.SQL.Add(' f.apelido, ');
    qryUsuario.SQL.Add(' f.senha_sistema, ');
    qryUsuario.SQL.Add(' f.e_mail ');
    qryUsuario.SQL.Add(' from funcionarios f ');
    qryUsuario.SQL.Add(' where f.acesso_hiva_mobile = ''S'' ');
    qryUsuario.SQL.Add('   and f.data_hora_alteracao > to_date('+vQryParametros.FieldByName('DATA_ULTIMA_INTEGRACAO_WEB').AsString.QuotedString+' ,''dd/mm/yyyy hh24:mi:ss'' ) ');

    qryUsuario.Open;

    if qryUsuario.IsEmpty then
      exit;

    qryUsuario.First;
    while not qryUsuario.Eof do
    begin
      objUser := TUsuarioHiva.Create;
      objUser.RegisterId    := qryUsuario.FieldByName('funcionario_id').AsInteger;
      objUser.UserName      := qryUsuario.FieldByName('apelido').AsString;
      objUser.Name          := qryUsuario.FieldByName('nome').AsString;
      objUser.Password      := qryUsuario.FieldByName('senha_sistema').AsString;
      objUser.CompanyId     := '1';
      objUser.Email         := IfThen(qryUsuario.FieldByName('e_mail').IsNull,qryUsuario.RecNo.ToString,qryUsuario.FieldByName('e_mail').AsString);

      objListaUser.Usuario.Add(objUser);
      qryUsuario.Next;
    end;

    objRetorno := ctrlHivaApi.Exportar(objListaUser);
    if objRetorno.Message.Contains('login') then
    begin
      objToken.Token := '';
      Login;
    end;

    AddLog('Usu�rios: ' + objRetorno.Message);


  finally
    ctrlHivaApi.Free;
    qryUsuario.Free;
    objRetorno.Free;
    objListaUser.Free;
  end;
end;

procedure TfrmAPiWeb.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FConexaoBanco.Close;
end;

procedure TfrmAPiWeb.FormCreate(Sender: TObject);
var
  vIntervalo : integer;
begin
  CarregarEstilo;
  IniciarConexaoBanco;
  objToken := TTokenHiva.Create;

  vQryParametros := TOraQuery.Create(nil);
  vQryParametros.Session := FConexaoBanco;
  vQryParametros.Close;
  vQryParametros.SQL.Add('SELECT INTERVALO_INTEGRACAO_WEB,');
  vQryParametros.SQL.Add('       DATA_ULTIMA_INTEGRACAO_WEB, ');
  vQryParametros.SQL.Add('       TOKEN_INTEGRACAO_WEB, ');
  vQryParametros.SQL.Add('       NVL(PORTA_INTEGRACAO_WEB,9191) AS PORTA_INTEGRACAO_WEB ');
  vQryParametros.SQL.Add(' FROM PARAMETROS');
  vQryParametros.Open;

  vIntervalo := vQryParametros.FieldByName('INTERVALO_INTEGRACAO_WEB').AsInteger;
  objToken.Token := vQryParametros.FieldByName('TOKEN_INTEGRACAO_WEB').AsString;

  if vIntervalo <= 0 then
    vIntervalo := 60;


  Timer.Interval := (1000 * 60) * vIntervalo;
  Timer.Enabled := True;

  THorse.Post ('getpedido',
    procedure(Req: THorseRequest; Res: THorseResponse; Next: TProc)
    begin
      try

        importarPedido;

      except on E: Exception do
        AddLog('Erro WebHook: '+ e.Message, True);
      end;
    end);

  THorse.Listen(vQryParametros.FieldByName('PORTA_INTEGRACAO_WEB').AsInteger);


end;

function TfrmAPiWeb.getCliente(pCliente: TClient): Integer;
var
  vRetBanco: RecRetornoBD;
  FCadastro: RecCadastros;
  vClientes: TArray<RecClientes>;
begin
  result := 0;

  vClientes := _Clientes.BuscarClientes(FConexaoBanco, 3, [pCliente.CpfCnpj.Replace('.','').Replace('-','').Replace('/','')], False);

  if Assigned(vClientes) then
  begin
    result := vClientes[0].cadastro_id;
    exit;
  end;

  try
    FCadastro := RecCadastros.Create;
    FCadastro.cadastro_id := 0;
    FCadastro.nome_fantasia := pCliente.TradeName;
    FCadastro.razao_social := pCliente.CompanyName;
    FCadastro.tipo_pessoa := 'J';
    FCadastro.cpf_cnpj := pCliente.CpfCnpj;
    FCadastro.numero := '0';
    FCadastro.e_cliente := 'S';
    FCadastro.e_fornecedor := 'N';
    FCadastro.EMotorista := 'N';
    FCadastro.ETransportadora := 'N';
    FCadastro.EProfissional := 'N';
    if pCliente.Address.Count > 0 then
    begin

      FCadastro.logradouro := pCliente.Address[0].Street;
      FCadastro.complemento := pCliente.Address[0].Complement;
      FCadastro.numero := pCliente.Address[0].Number;
      FCadastro.ponto_referencia := pCliente.Address[0].Reference;
      FCadastro.bairro_id :=  10992;
//      if FrEndereco.FrBairro.EstaVazio then
//        FCadastro.bairro_id := Sessao.getEmpresaLogada.BairroId
//      else
//        FCadastro.bairro_id := FrEndereco.getBairroId;

      FCadastro.cep := pCliente.Address[0].ZipCode;
    end;
//      FCadastro.TelefonePrincipal  := eTelefone.Text;
    FCadastro.TelefoneCelular    := pCliente.CellNumber;
//      FCadastro.TelefoneFax        := eFax.Text;
//      FCadastro.e_mail             := eEmail.Text;
//      FCadastro.estado_civil       := cbEstadoCivil.GetValor;
//      FCadastro.sexo               := rgSexo.GetValor;
//      FCadastro.data_nascimento    := eDataNascimento.AsData;
    FCadastro.inscricao_estadual := pCliente.StateRegistration;
//      FCadastro.cnae               := eCNAE.Text;
//      FCadastro.rg                 := eRG.Text;
//      FCadastro.orgao_expedidor_rg := eOrgaoExpedidor.Text;
//      FCadastro.Observacoes        := eObservacoes.Lines.Text;
    FCadastro.ativo              := 'S';


    vRetBanco :=
      _Clientes.AtualizarCliente(
        FConexaoBanco,
        False,
        0,
        FCadastro,
        nil,//          FTelefones,
        nil,//          FEnderecos,
        'NC',//          cbTipoCliente.GetValor,
        '',//          eCodigoSPC.Text,
        now,//          eDataPesquisaSPC.AsData,
        '',//          eLocalRestricaoSPC.Text,
        now,//          eDataPesquisaSerasa.AsData,
        '',//          eLocalRestricaoSerasa.Text,
        '',//          eAtendenteSPC.Text,
        'N',//          _Biblioteca.ToChar(ckRestricaoSPC),
        now,//          eDataRestricaoSPC.AsData,
        '',//          eAtendenteSerasa.Text,
        'N',//          _Biblioteca.ToChar(ckRestricaoSerasa),
        now,//          eDataRestricaoSerasa.AsData,
        0,//          vCondicaoPadraoId,
        0,//          vVendedorPadraoId,
        'N',//          _Biblioteca.ToChar(ckEmitirSomenteNFE),
        'N',//          _Biblioteca.ToChar(ckExigirModeloNotaFiscal),
        [],//          FrCondicoesPagtoRestritas.GetArrayOfInteger,
        1,//        eLimiteCredito.AsCurr,
        0,//        eLimiteCreditoMensal.AsCurr,
        now,//          eDataAprovacaoLimiteCredito.AsData,
        'N',//          ToChar(ckNaoGerarComissao),
        'S',//          _Biblioteca.ToChar(ckAtivo),
        'N',//          ToChar(ckExigirNotaFaturamento),
        'V',//          cbTipoPreco.GetValor,
        now,//          eDataValidadeLimiteCredito.AsData,
        0,//          eQtdeDiasVencimentoAcumulado.AsInt,
        'S',//          ToChar(ckIgnorarRedirRegraNotaFisc.Checked),
        0,//          vIndiceDescontoVendaId,
        0,//          vListaNegraId,
        '',//          eObservacoesListaNegra.Text,
        'S',//          ckIgnorarBloqueiosVendas.CheckedStr,
        0,//          vGrupoClienteId,
        '',//          eObservacoesVenda.Lines.Text,
        '',//          eUsuarioBanco.lines.Text,
        '',//          eSenhaBanco.lines.Text,
        '',//          eIpServidor.lines.Text,
        '',//          eServicoOracle.lines.Text,
        '',//          eQtdEstacoes.lines.Text,
        '',//          eResponsavelEmp.lines.Text,
        '',//          eValorAdesao.lines.Text,
        '',//          eValorMens.lines.Text,
        now,//          eDataAdesao.AsData,
        '',//          eDataVenc.lines.Text,
        '',//          ePorta.lines.Text,
        'S',//          ToChar(ckCalcularIndiceDeducao),
        '',//          Sessao.getCriptografia.Criptografar(eDataValidade.Text,False),
        [],//          FrArquivos.Arquivos,
        0,//          Sessao.getUsuarioLogado.cadastro_id,
        0,//vRecClienteFilial,//          vRecClienteFilial,
        'SN',//          vRegimeEmpresa,
        'N',//          toChar(ckBloquearVendas)
        'S'
      );

    if vRetBanco.TeveErro then begin
      _log.Add(vRetBanco.MensagemErro);
      Abort;
    end;

    result := vRetBanco.AsInt;


    AddLog('clientes: ' +pCliente.ToString);

  finally
    FCadastro.Free;
  end;
end;

function TfrmAPiWeb.getItens(pListaItens: TObjectList<TItems>): TArray<RecOrcamentoItens>;
var
  vItem: TItems;
  vIndice: Double;
  i: integer;
begin
  Result := nil;
//  vIndice := getIndiceCondicaoPagamento;

  i := 0;
  for vItem in pListaItens do
  begin

    SetLength(Result, Length(Result) + 1);

    Result[i].produto_id                  := vItem.Product.Code; //SFormatInt(sgProdutos.Cells[coProdutoId, i]);
//    Result[i - 1].nome                        := sgProdutos.Cells[coNomeProduto, i];
//    Result[i - 1].nome_marca                  := sgProdutos.Cells[coMarca, i];
    Result[i].item_id                     := i + 1;
    Result[i].quantidade                  := vItem.Quantity; //_Biblioteca.SFormatDouble(sgProdutos.Cells[coQuantidade, i]);
//    Result[i - 1].unidade_venda               := sgProdutos.Cells[coUndVenda, i];
//    Result[i - 1].PrecoManual                 := _Biblioteca.SFormatDouble(sgProdutos.Cells[coPrecoManual, i]);
    Result[i].preco_unitario              := vItem.Price; //_Biblioteca.SFormatDouble(sgProdutos.Cells[coPrecoUnitario, i]);
    Result[i].PrecoBase               := vItem.Price; //_Biblioteca.SFormatDouble(sgProdutos.Cells[coPrecoBaseUtilizado, i]);
//
//    Result[i - 1].valor_total_outras_despesas := _Biblioteca.SFormatDouble(sgProdutos.Cells[coValorTotalOutDespProp, i]);
//    Result[i - 1].valor_total_desconto        := _Biblioteca.SFormatDouble(sgProdutos.Cells[coValorTotalDescProp, i]);
//    Result[i - 1].ValorTotalFrete             := _Biblioteca.SFormatDouble(sgProdutos.Cells[coValorTotalFreteProp, i]);
//    Result[i - 1].ValorDescUnitPrecoManual    := _Biblioteca.SFormatDouble(sgProdutos.Cells[coValorDescontoPrecoMan, i]);
//
    Result[i].valor_total                 := vItem.Total; //_Biblioteca.SFormatDouble(sgProdutos.Cells[coTotalProduto, i]);
//    Result[i - 1].Estoque                     := _Biblioteca.SFormatCurr(sgProdutos.Cells[coEstoque, i]);
//    Result[i - 1].QuantidadeRetirarAto        := IIfDbl(cbTipoEntrega.GetValor = 'RA', Result[i - 1].quantidade);
//    Result[i - 1].multiplo_venda              := SFormatDouble(sgProdutos.Cells[coMultiploVenda, i]);
    Result[i].TipoPrecoUtilizado          := 'MAN'; //sgProdutos.Cells[coTipoPrecoUtilizar, i];
    Result[i].TipoControleEstoque         := 'N'; //sgProdutos.Cells[coTipoControleEstoq, i];
//    Result[i - 1].PercDescontoPrecoManual     := SFormatDouble(sgProdutos.Cells[coPercDescPrecoManual, i]);

//    Result[i - 1].PrecoVarejoBasePrecoManual :=
//      IIfDbl(
//        Result[i - 1].TipoPrecoUtilizado = 'MAN',
//        Arredondar(_Biblioteca.SFormatDouble(sgProdutos.Cells[coPrecoBaseUtilizado, i]) * vIndice, 2),
//        Result[i - 1].preco_unitario
//      );
    inc(i);
  end;
end;

procedure TfrmAPiWeb.importarPedido;
var
  vRetBanco: RecRetornoBD;
  ctrlHivaApi : THivaApi;
  objListaPedido : TListaRetornoPedidoHiva;
  vCnt: Integer;
begin

  ctrlHivaApi := THivaApi.Create;
  ctrlHivaApi.ChaveAPi := objToken.Token;
  ctrlHivaApi.UrlApi   := cUrlWeb + 'order/find-export';

  try
    objListaPedido := ctrlHivaApi.ImportarPedido;
//    FConexaoBanco.IniciarTransacao;
    for vCnt := 0 to objListaPedido.Orders.Count - 1 do
    begin


      vRetBanco :=
        _Orcamentos.AtualizarOrcamento(
          FConexaoBanco,
          0,
          '',//eNomeConsumidorFinal.Text,
          '',//eTelefoneConsumidorFinal.Text,
          1,//Sessao.getEmpresaLogada.EmpresaId,
          getCliente(objListaPedido.Orders[vCnt].Client),// FrCliente.getCliente.cadastro_id,
          objListaPedido.Orders[vCnt].Condition.ConditionId,// FrCondicoesPagamento.getDados.condicao_id,
          1,//FrVendedor.getVendedor.funcionario_id,
          objListaPedido.Orders[vCnt].Condition.IndexAddition, //FrCondicoesPagamento.getDados.indice_acrescimo,
          objListaPedido.Orders[vCnt].Total - (objListaPedido.Orders[vCnt].Discount
                                              + objListaPedido.Orders[vCnt].Freight
                                              + objListaPedido.Orders[vCnt].OtherExpenses),// SFormatDouble(stValorTotalProdutos.Caption),
          0,//SFormatDouble(stValorTotalPromocao.Caption),
          objListaPedido.Orders[vCnt].Total,// FrFechamento.eValorTotalASerPago.AsCurr,
          objListaPedido.Orders[vCnt].OtherExpenses,// FrFechamento.eValorOutrasDespesas.AsCurr,
          objListaPedido.Orders[vCnt].Discount,// FrFechamento.eValorDesconto.AsCurr,
          objListaPedido.Orders[vCnt].Money, //FrFechamento.eValorDinheiro.AsCurr,
          0,// FrFechamento.eValorCheque.AsCurr,
          0,//FrFechamento.eValorCartaoDebito.AsCurr,
          0,//FrFechamento.eValorCartaoCredito.AsCurr,
          0,//FrFechamento.eValorCobranca.AsCurr,
          0,//FrFechamento.eValorAcumulativo.AsCurr,
          0,//FrFechamento.eValorFinanceira.AsCurr,
          0,//FrFechamento.eValorCredito.AsCurr,
          objListaPedido.Orders[vCnt].Freight, //eValorFrete.AsDouble,
          0, //SFormatDouble(stTotalFreteProdutos.Caption),
          0, // TURNO_ID
          'OE', //vStatusOrcamento,
          1, //eOverPrice.AsDouble,
          'EN', //cbTipoEntrega.GetValor,
          objListaPedido.Orders[vCnt].Observation, //eObservacoesCaixa.Lines.Text,
          '', //eObservacoesTitulo.Lines.Text,
          '', //eObservacoesExpedicao.Lines.Text,
          '', //eObservacoesNotaFiscalEletronica.Lines.Text,
          objListaPedido.Orders[vCnt].EstimatedDelivery,// eDataEntrega.AsData + eHoraEntrega.AsHora,
          objListaPedido.Orders[vCnt].EstimatedDelivery,//eHoraEntrega.AsHora,
          'W',
          'C', //Self.tipoAnaliseCusto,
          objListaPedido.Orders[vCnt].Address.Street, //FrEnderecoEntrega.getLogradouro,
          objListaPedido.Orders[vCnt].Address.Complement, //FrEnderecoEntrega.getComplemento,
          objListaPedido.Orders[vCnt].Address.Number, //FrEnderecoEntrega.getNumero,
          objListaPedido.Orders[vCnt].Address.Reference, //FrEnderecoEntrega.getPontoReferencia,
          10992,//vBairroId,
          0, //vIndiceDescontoVendaId,
          objListaPedido.Orders[vCnt].Address.ZipCode, //FrEnderecoEntrega.getCep,
          getItens(objListaPedido.Orders[vCnt].Items),
          [], //vCreditosIds(),
          0, //vProfissionalId,
          [], //FrArquivos.Arquivos,
          objListaPedido.Orders[vCnt].Address.StateRegistration, //FrEnderecoEntrega.getInscricaoEstadual,
          'N', //ckReceberNaEntrega.CheckedStr,
          '', //Sessao.getParametros.DefinirLocalManual,
          '', //edtCPFConsumidorFinal.Text,
          objListaPedido.Orders[vCnt].PixTransfer, //FrFechamento.eValorPix.AsCurr,
          1,//Sessao.getUsuarioLogado.cadastro_id,
          nil,
          'N'//_Biblioteca.IIfStr(cbTipoEntrega.GetValor = 'EN', ckAguardandoContatoCliente.CheckedStr, 'N')
        );

      if vRetBanco.TeveErro then begin
        _log.Add(vRetBanco.MensagemErro);
        Abort;
      end;
    end;

//    FConexaoBanco.FinalizarTransacao;


    AddLog('pedidos: ' +objListaPedido.ToString);


  finally
    ctrlHivaApi.Free;
    objListaPedido.Free;
  end;
end;

procedure TfrmAPiWeb.IniciarConexaoBanco;
const
  coUsuario = 0;
  coSenha   = 1;
  coServer  = 2;
  coPorta   = 3;
  coServico = 4;
  coDireto  = 5;

var
  bolDireto :Boolean;
  vServidor: string;
  vAuxiliar: TStringDynArray;
  vIni: TIniFile;
begin
  if not FileExists(_LocalExe + 'sigo.ini') then begin
    Showmessage('Arquivo de inicializa��o "sigo.ini" n�o encontrado!');
    Halt;
  end;

  vIni := TIniFile.Create(_LocalExe + 'sigo.ini');


  FCriptografia := TCriptografia.Create(Application);
  vServidor := FCriptografia.Descriptografar(vIni.ReadString('SERVIDOR', 'server', ''), False);

  vAuxiliar := SplitString(vServidor, '|');

  try
    FConexaoBanco := _Conexao.TConexao.Create(Application);

    try
      bolDireto :=  vAuxiliar[coDireto] = 'S';
    except
      bolDireto := False;
    end;

    FConexaoBanco.Conectar(
      vAuxiliar[coUsuario],
      vAuxiliar[coSenha],
      vAuxiliar[coServer],
      StrToInt(vAuxiliar[coPorta]),
      vAuxiliar[coServico],
      bolDireto
    );
    // Tempor�rio at� o usu�rio logar completamente no PC
    FConexaoBanco.IniciarSessao(1, 1, 'ApiWeb');

  except
    on e: Exception do begin
      Showmessage('Falha ao instanciar objeto de conex�o com o oracle!' + Chr(13) + Chr(10) + e.Message);
      Halt;
    end;
  end;
  vIni.Free;


end;

procedure TfrmAPiWeb.Login;
var
  ctrlHivaApi : THivaApi;
  objLogin : TLoginHiva;
  vQry : TOraQuery;
begin

  if not objToken.Token.IsEmpty then
    exit;

  vQry := TOraQuery.Create(nil);
  vQry.Session := FConexaoBanco;
  objLogin := TLoginHiva.Create;
  ctrlHivaApi := THivaApi.Create;
  ctrlHivaApi.UrlApi   := cUrlWeb + 'auth/token';

  try

    objLogin.Name := 'hiva';
    objLogin.Password := 'hiv@123';
    objToken := ctrlHivaApi.Login(objLogin);

    try
      FConexaoBanco.IniciarTransacao;

      vQry.SQL.Add('update PARAMETROS set TOKEN_INTEGRACAO_WEB = :P1');
      vQry.ParamByName('P1').AsString := objToken.Token;
      vQry.ExecSQL;

      FConexaoBanco.FinalizarTransacao;
    except
      on e: Exception do begin
        FConexaoBanco.VoltarTransacao;
      end;
    end;

  finally
    vQry.Free;
    ctrlHivaApi.Free;
    objLogin.Free;
  end;
end;

procedure TfrmAPiWeb.TimerTimer(Sender: TObject);
begin
  btnIntegrarClick(self);
end;

end.
