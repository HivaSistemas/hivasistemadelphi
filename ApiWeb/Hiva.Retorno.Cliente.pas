unit Hiva.Retorno.Cliente;

interface

uses
  Pkg.Json.DTO, System.Generics.Collections, REST.Json.Types;

{$M+}

type
  TAddress = class;
  TAddressType = class;

  TAddressType = class
  private
    FDescription: string;
  published
    property Description: string read FDescription write FDescription;
  end;

  TAddress = class
  private
    FAddressType: TAddressType;
    FCity: string;
    FComplement: string;
    FDistrict: string;
    FNumber: string;
    FReference: string;
    FState: string;
    FStreet: string;
    [JSONName('zip_code')]
    FZipCode: string;
  published
    property AddressType: TAddressType read FAddressType;
    property City: string read FCity write FCity;
    property Complement: string read FComplement write FComplement;
    property District: string read FDistrict write FDistrict;
    property Number: string read FNumber write FNumber;
    property Reference: string read FReference write FReference;
    property State: string read FState write FState;
    property Street: string read FStreet write FStreet;
    property ZipCode: string read FZipCode write FZipCode;
  public
    constructor Create;
    destructor Destroy; override;
  end;

  TClients = class(TJsonDTO)
  private
    [JSONName('address'), JSONMarshalled(False)]
    FAddressArray: TArray<TAddress>;
    [GenericListReflect]
    FAddress: TObjectList<TAddress>;
    [JSONName('cell_number')]
    FCellNumber: string;
    [JSONName('client_type')]
    FClientType: string;
    [JSONName('company_id')]
    FCompanyId: string;
    [JSONName('company_name')]
    FCompanyName: string;
    [JSONName('cpf_cnpj')]
    FCpfCnpj: string;
    FId: string;
    [JSONName('register_id')]
    FRegisterId: string;
    [JSONName('state_registration')]
    FStateRegistration: string;
    [JSONName('trade_name')]
    FTradeName: string;
    function GetAddress: TObjectList<TAddress>;
  protected
    function GetAsJson: string; override;
  published
    property Address: TObjectList<TAddress> read GetAddress;
    property CellNumber: string read FCellNumber write FCellNumber;
    property ClientType: string read FClientType write FClientType;
    property CompanyId: string read FCompanyId write FCompanyId;
    property CompanyName: string read FCompanyName write FCompanyName;
    property CpfCnpj: string read FCpfCnpj write FCpfCnpj;
    property Id: string read FId write FId;
    property RegisterId: string read FRegisterId write FRegisterId;
    property StateRegistration: string read FStateRegistration write FStateRegistration;
    property TradeName: string read FTradeName write FTradeName;
  public
    destructor Destroy; override;
  end;

  TListaRetornoClienteHiva = class(TJsonDTO)
  private
    [JSONName('clients'), JSONMarshalled(False)]
    FClientsArray: TArray<TClients>;
    [GenericListReflect]
    FClients: TObjectList<TClients>;
    function GetClients: TObjectList<TClients>;
  protected
    function GetAsJson: string; override;
  published
    property Clients: TObjectList<TClients> read GetClients;
  public
    destructor Destroy; override;
  end;

implementation

{ TAddress }

constructor TAddress.Create;
begin
  inherited;
  FAddressType := TAddressType.Create;
end;

destructor TAddress.Destroy;
begin
  FAddressType.Free;
  inherited;
end;

{ TClients }

destructor TClients.Destroy;
begin
  GetAddress.Free;
  inherited;
end;

function TClients.GetAddress: TObjectList<TAddress>;
begin
  Result := ObjectList<TAddress>(FAddress, FAddressArray);
end;

function TClients.GetAsJson: string;
begin
  RefreshArray<TAddress>(FAddress, FAddressArray);
  Result := inherited;
end;

{ TListaRetornoClienteHiva }

destructor TListaRetornoClienteHiva.Destroy;
begin
  GetClients.Free;
  inherited;
end;

function TListaRetornoClienteHiva.GetClients: TObjectList<TClients>;
begin
  Result := ObjectList<TClients>(FClients, FClientsArray);
end;

function TListaRetornoClienteHiva.GetAsJson: string;
begin
  RefreshArray<TClients>(FClients, FClientsArray);
  Result := inherited;
end;

end.
