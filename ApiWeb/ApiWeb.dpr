program ApiWeb;

uses
  Vcl.Forms,
  System.SysUtils,
  untMenu in 'untMenu.pas' {frmAPiWeb},
  ctrlHivaApi in 'ctrlHivaApi.pas',
  Api.Base in 'Api.Base.pas',
  Hiva.Login in 'Hiva.Login.pas',
  Hiva.Token in 'Hiva.Token.pas',
  Hiva.Usuario in 'Hiva.Usuario.pas',
  Hiva.Retorno in 'Hiva.Retorno.pas',
  VarGlobal in 'VarGlobal.pas',
  Vcl.Themes,
  Vcl.Styles,
  Hiva.Cliente in 'Hiva.Cliente.pas',
  Hiva.TipoCondicao in 'Hiva.TipoCondicao.pas',
  Hiva.Endereco in 'Hiva.Endereco.pas',
  Hiva.Produto in 'Hiva.Produto.pas',
  Hiva.Retorno.Cliente in 'Hiva.Retorno.Cliente.pas',
  Pkg.Json.DTO in '..\Repositório\Pkg.Json.DTO.pas',
  _Conexao in '..\Repositório\_Conexao.pas',
  _Criptografia in '..\Repositório\_Criptografia.pas',
  _Clientes in '..\Piloto\Banco\_Clientes.pas',
  _FrameHerancaPrincipal in '..\Repositório\_FrameHerancaPrincipal.pas' {FrameHerancaPrincipal: TFrame},
  _HerancaBarra in '..\Repositório\_HerancaBarra.pas',
  _HerancaFinalizar in '..\Repositório\_HerancaFinalizar.pas',
  AlterarSenhaUsuario in '..\Piloto\AlterarSenhaUsuario.pas',
  EmpresasLogin in '..\Piloto\EmpresasLogin.pas',
  _HerancaPrincipal in '..\Repositório\_HerancaPrincipal.pas' {FormHerancaPrincipal},
  Perguntar in '..\Piloto\Perguntar.pas' {FormPerguntar},
  Exclamar in '..\Piloto\Exclamar.pas' {FormExclamar},
  Informacao in '..\Piloto\Informacao.pas' {FormInformacao},
  Login in '..\Piloto\Login.pas' {FormLogin},
  Hiva.Retorno.Pedido in 'Hiva.Retorno.Pedido.pas',
  _Orcamentos in '..\Piloto\Banco\_Orcamentos.pas' {$R *.res},
  Hiva.ContasPagarReceber in 'Hiva.ContasPagarReceber.pas';

{$R *.res}

var
  vCnt : Integer;
begin
  _LogName := 'ApiWeb';

  for vCnt := 1 to 2 do
  begin
    if Trim(ParamStr(vCnt)) = '' then
      continue;

    if (Trim(ParamStr(vCnt)) = '-LOG')  then
      _GerarLog := True
  end;

  _LocalExe := IncludeTrailingPathDelimiter(ExtractFilePath(Application.ExeName));

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmAPiWeb, frmAPiWeb);
  Application.Run;
end.
