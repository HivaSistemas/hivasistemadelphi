unit _ImpressoraNaoFiscal;

interface

uses
  System.StrUtils, System.Math, System.Classes, System.SysUtils, _Biblioteca;

type
  TImpressoraNaoFiscal = class
  protected
    F: TextFile;
    FModeloImpressora: (tmElginI9);
  public
    constructor Create(pModeloImpressora: string; pCaminhoImpressora: string);

    function Imprimir(pTexto: string): Boolean;
    function ImprimirSemSaltar(pTexto: string): Boolean;
    function ImprimirQrCode(pTexto: string): Boolean;
    function SaltarLinha: Boolean;
    function CortarPapel: Boolean;
  end;

implementation

{ TImpressoraNaoFiscal }

const
  GS = #29;
  ESC = #27;

constructor TImpressoraNaoFiscal.Create(pModeloImpressora: string; pCaminhoImpressora: string);
var
  vBuffer: string;
begin
  if UpperCase(pModeloImpressora) = 'ELGINI9' then
    FModeloImpressora := tmElginI9;

  AssignFile(F, pCaminhoImpressora);

  vBuffer := ESC + 'a' + #0;

  ImprimirSemSaltar(string(vBuffer));
end;

function TImpressoraNaoFiscal.CortarPapel: Boolean;
var
  vBuffer: AnsiString;
begin
  vBuffer := GS + 'V' + #1;
  Result := ImprimirSemSaltar(string(vBuffer));
end;

function TImpressoraNaoFiscal.Imprimir(pTexto: string): Boolean;
begin
  Result := True;
  try
    Rewrite(F);
    WriteLn(F, AnsiString(pTexto));
    CloseFile(F);
  except
    on e: Exception do begin
      Exclamar(e.Message);
      Result := False;
    end;
  end;
end;

function IntToLEStr(AInteger: Integer; BytesStr: Integer = 2): AnsiString;
var
  AHexStr: String;
  LenHex, P, DecVal: Integer;
begin
  LenHex  := BytesStr * 2 ;
  AHexStr := IntToHex(AInteger,LenHex);
  Result  := '' ;

  P := 1;
  while P < LenHex do begin
    DecVal := StrToInt('$' + Copy(AHexStr,P,2)) ;
    Result := AnsiChar( DecVal ) + Result;
    P := P + 2;
  end;
end;

function TImpressoraNaoFiscal.ImprimirQrCode(pTexto: string): Boolean;
var
  vBuffer: AnsiString;
  vTexto: AnsiString;
begin
  vTexto := AnsiString(pTexto);

  // Alinhamento centralizado
  vBuffer := ESC + 'a' + #1;

  ImprimirSemSaltar(string(vBuffer));
    
  vBuffer :=  
    GS + '(k' + #4 + #0 + '1A2' + #0 +                           // Tipo
    GS + '(k' + #3 + #0 + '1C' + #4 +                            // Largura Modulo
    GS + '(k' + #3 + #0 + '1E' + #0 +                            // Error Level
    GS + '(k' + IntToLEStr(Length(vTexto)+3) + '1P0' + vTexto +  // Codifica
    GS + '(k' + #3 + #0 +'1Q0';                                  // Imprime

  Result := Imprimir(string(vBuffer));
  if not Result then
    Exit;

  // Voltando o alinhamento a esquerda
  vBuffer := ESC + 'a' + #0;

  Result := ImprimirSemSaltar(string(vBuffer));
end;

function TImpressoraNaoFiscal.ImprimirSemSaltar(pTexto: string): Boolean;
begin
  Result := True;
  try
    Rewrite(F);
    Write(F, AnsiString(pTexto));
    CloseFile(F);
  except
    on e: Exception do begin
      Exclamar(e.Message);
      Result := False;
    end;
  end;
end;

function TImpressoraNaoFiscal.SaltarLinha: Boolean;
begin
  Result := ImprimirSemSaltar(#10);
end;

end.
