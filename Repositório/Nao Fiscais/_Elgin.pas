unit _Elgin;

interface

uses
  Forms, SysUtils, Windows, System.StrUtils, _ImpressoraNaoFiscal, _Biblioteca;

type
  TElgin = class(TImpressoraNaoFiscal)
  private
    FImpressao: Pointer;

    FPrinterCreator: function(printer: PPointer; model: string): Integer; cdecl;
    FPortOpenW: function(printer: Pointer; portSetting: string): Integer; cdecl;
    FPortClose: function(printer: Pointer): Integer; cdecl;
    FCutPaper: function(printer: Pointer; cutMode: Integer; distance: Integer): Integer; cdecl;
  public
    constructor Create(pModeloImpressora: string);

    function PrtCutPaper(printer: Pointer; cutMode: Integer; distance: Integer): Integer;
    function PrtPrintTextSA(printer: Pointer; data: AnsiString): Integer;
    function PrtSetTextBold(printer: Pointer; bold: Integer): Integer;
    function PrtSetTextFont(printer: Pointer; font: Integer): Integer;
    function PrtPrintSymbolW(printer: Pointer; symbolType: Integer; symbolData: string; errLevel: Integer; width: Integer; height: Integer; alignment: Integer): Integer;
    function PrtPrintBarCodeW(printer: Pointer; bcType: Integer; data: string; width: Integer; height: Integer; alignment: Integer; hriPosition: Integer): Integer;
    function PrtOpenDrawer(printer: Pointer; pin_mode: Integer; onTime: Integer; offTime: Integer): Integer;

    function AbrirPorta(velocidade_porta: Integer; porta: string): Boolean; override;
    function FecharPorta: Boolean; override;
    function guilhotina(modo: Integer; distancia: Integer): Boolean; override;
    function ImprimirTextoSimples(data: string): Boolean; override;
    function DefinirFonteNegrito(negrito: Integer): Boolean; override;
    function DefinirFonte(fonte: Integer): Boolean; override;
    function ImprimirQrCode(qr_code: string): Boolean; override;
    function ImprimirCodigoBarras(tipo_codigo: Integer; codigo_barras: string; largura: Integer; altura: Integer; alinhamento: Integer; posicao: Integer): Boolean; override;
    function AbrirGaveta: Boolean; override;
    function TeveErro(pRetorno: Integer): Boolean; override;
  end;

implementation

const
  coDLLNaoEncontrada = 1010;

function TElgin.TeveErro(pRetorno: Integer): Boolean;
begin
  Result := (pRetorno <> 0);

  if pRetorno = -1 then
    Exclamar('Erro retornado pela impressora Elgin, par�metro inv�lido.')
  else if pRetorno = -2 then
    Exclamar('Erro retornado pela impressora Elgin, computador n�o possui mem�ria suficiente.')
  else if pRetorno = -3 then
    Exclamar('Erro retornado pela impressora Elgin, fun��o n�o suportada para este modelo.')
  else if pRetorno = -6 then
    Exclamar('Erro retornado pela impressora Elgin, o identificador � inv�lido.')
  else if pRetorno = -8 then
    Exclamar('Erro retornado pela impressora Elgin, nome do modelo � inv�lido.')
  else if pRetorno = -309 then
    Exclamar('Erro retornado pela impressora Elgin, n�o � possivel abrir a porta de comunica��o.')
  else if pRetorno = -311 then
    Exclamar('Erro retornado pela impressora Elgin, falha ao abrir porta de comunica��o.')
  else if pRetorno = -321 then
    Exclamar('Erro retornado pela impressora Elgin, falha ao transmitir dados.')
  else if pRetorno = -322 then
    Exclamar('Erro retornado pela impressora Elgin, tempo limite para transmiss�o de dados atingido.')
  else if pRetorno = coDLLNaoEncontrada then
    Exclamar('N�o foi encontrado a dll utilizada pela impressora Elgin.');
end;


function TElgin.PrtCutPaper(printer: Pointer; cutMode: Integer; distance: Integer): Integer;
type
  TCutPaper = function(printer: Pointer; cutMode: Integer; distance: Integer): Integer; cdecl;
var
  CutPaper: TCutPaper;
begin
  Result := coDLLNaoEncontrada;

  CutPaper := GetProcAddress(FDLLHandle, 'PrtCutPaper');

  if Assigned(CutPaper) then
    Result := CutPaper(FImpressao, cutMode, distance);
end;

function TElgin.PrtPrintTextSA(printer: Pointer; data: AnsiString): Integer;
type
  TPrintTextSA = function(printer: Pointer; data: AnsiString): Integer; cdecl;
var
  PrintTextSA: TPrintTextSA;
begin
  Result := coDLLNaoEncontrada;

  PrintTextSA := GetProcAddress(FDLLHandle, 'PrtPrintTextSA');

  if Assigned(PrintTextSA) then
    Result := PrintTextSA(FImpressao, data);
end;

function TElgin.PrtSetTextBold(printer: Pointer; bold: Integer): Integer;
type
  TSetTextBold = function(printer: Pointer; bold: Integer): Integer; cdecl;
var
  SetTextBold: TSetTextBold;
begin
  Result := coDLLNaoEncontrada;

  SetTextBold := GetProcAddress(FDLLHandle, 'PrtSetTextBold');

  if Assigned(SetTextBold) then
    Result := SetTextBold(FImpressao, bold);
end;

function TElgin.PrtSetTextFont(printer: Pointer; font: Integer): Integer;
type
  TSetTextFont = function(printer: Pointer; font: Integer): Integer; cdecl;
var
  SetTextFont: TSetTextFont;
begin
  Result := coDLLNaoEncontrada;

  SetTextFont := GetProcAddress(FDLLHandle, 'PrtSetTextFont');

  if Assigned(SetTextFont) then
    Result := SetTextFont(FImpressao, font);
end;

function TElgin.PrtPrintSymbolW(printer: Pointer; symbolType: Integer; symbolData: string; errLevel: Integer; width: Integer; height: Integer; alignment: Integer): Integer;
type
  TPrintSymbolW = function(printer: Pointer; symbolType: Integer; symbolData: string; errLevel: Integer; width: Integer; height: Integer; alignment: Integer): Integer; cdecl;
var
  PrintSymbolW: TPrintSymbolW;
begin
  Result := coDLLNaoEncontrada;

  PrintSymbolW := GetProcAddress(FDLLHandle, 'PrtPrintSymbolW');

  if Assigned(PrintSymbolW) then
    Result := PrintSymbolW(FImpressao, 104, symbolData, 48, 3, 0, 1);
end;

function TElgin.PrtPrintBarCodeW(printer: Pointer; bcType: Integer; data: string; width: Integer; height: Integer; alignment: Integer; hriPosition: Integer): Integer;
type
  TPrintBarCodeW = function(printer: Pointer; bcType: Integer; data: string; width: Integer; height: Integer; alignment: Integer; hriPosition: Integer): Integer; cdecl;
var
  PrintBarCodeW: TPrintBarCodeW;
begin
  Result := coDLLNaoEncontrada;

  PrintBarCodeW := GetProcAddress(FDLLHandle, 'PrtPrintBarCodeW');

  if Assigned(PrintBarCodeW) then
    Result := PrintBarCodeW(FImpressao, bcType, data, width, height, alignment, hriPosition);
end;

function TElgin.PrtOpenDrawer(printer: Pointer; pin_mode: Integer; onTime: Integer; offTime: Integer): Integer;
type
  TPrintOpenDrawer = function(printer: Pointer; pin_mode: Integer; onTime: Integer; offTime: Integer): Integer; cdecl;
var
  PrintOpenDrawer: TPrintOpenDrawer;
begin
  Result := coDLLNaoEncontrada;

  PrintOpenDrawer := GetProcAddress(FDLLHandle, 'PrtOpenDrawer');

  if Assigned(PrintOpenDrawer) then
    Result := PrintOpenDrawer(FImpressao, pin_mode, onTime, offTime);
end;

constructor TElgin.Create(pModeloImpressora: string);
begin
  inherited;

  FDLLHandle := LoadLibrary('HprtPrinter.dll');
end;

function TElgin.AbrirPorta(velocidade_porta: Integer; porta: string): Boolean;
var
  vRetorno: Integer;
begin
  Result := False;
  FImpressao := nil;
  vRetorno := coDLLNaoEncontrada;

  FPrinterCreator := GetProcAddress(FDLLHandle, 'PrtPrinterCreatorW');

  if not Assigned(FPrinterCreator) then
    Exit;

  vRetorno := FPrinterCreator(@FImpressao, 'i9');
  Result := not TeveErro(vRetorno);
  if not Result then
    Exit;

  FPortOpenW := GetProcAddress(FDLLHandle, 'PrtPortOpenW');
  if not Assigned(FPortOpenW) then
    Exit;

  vRetorno := FPortOpenW(FImpressao, IfThen(porta = 'USB', porta, porta + ',BAUDRATE_' + IntToStr(velocidade_porta)));
  Result := not TeveErro(vRetorno);
end;

function TElgin.FecharPorta: Boolean;
begin
  Result := False;

  if FImpressao <> nil then begin
    FPortClose := GetProcAddress(FDLLHandle, 'PrtPortClose');
    if not Assigned(FPortClose) then
      Exit;
  end;

  Result := not TeveErro(FPortClose(FImpressao));
end;

function TElgin.guilhotina(modo: Integer; distancia: Integer): Boolean;
var
  ret_int: Integer;
begin
  CutPaper := GetProcAddress(FDLLHandle, 'PrtCutPaper');

  if Assigned(CutPaper) then
    Result := CutPaper(FImpressao, cutMode, distance);

  ret_int := PrtCutPaper(FImpressao, modo, distancia);
  Result := not TeveErro(ret_int);
end;

function TElgin.ImprimirTextoSimples(data: string): Boolean;
var
  ret_int: Integer;
begin
  ret_int := PrtPrintTextSA(FImpressao, AnsiString(data));
  Result := not TeveErro(ret_int);
end;

function TElgin.DefinirFonteNegrito(negrito: Integer): Boolean;
var
  ret_int: Integer;
begin
  ret_int := PrtSetTextBold(FImpressao, negrito);
  Result := not TeveErro(ret_int);
end;

function TElgin.DefinirFonte(fonte: Integer): Boolean;
var
  ret_int: Integer;
begin
  ret_int := PrtSetTextFont(FImpressao, fonte);
  Result := not TeveErro(ret_int);
end;

function TElgin.ImprimirQrCode(qr_code: string): Boolean;
var
  ret_int: Integer;
begin
  ret_int := PrtPrintSymbolW(FImpressao, 104, qr_code, 48, 3, 0, 1);
  Result := not TeveErro(ret_int);
end;

function TElgin.ImprimirCodigoBarras(tipo_codigo: Integer; codigo_barras: string; largura: Integer; altura: Integer; alinhamento: Integer; posicao: Integer): Boolean;
var
  ret_int: Integer;
begin
  ret_int := PrtPrintBarCodeW(FImpressao, tipo_codigo, codigo_barras, largura, altura, alinhamento, posicao);
  Result := not TeveErro(ret_int);
end;

function TElgin.AbrirGaveta: Boolean;
var
  ret_int: Integer;
begin
  ret_int := PrtOpenDrawer(FImpressao, 0, 3, 6);
  Result := not TeveErro(ret_int);
end;

end.
