unit _SessaoGenerica;

interface

uses
  System.Classes, _RecordsCadastros, _Autorizacoes, _Conexao, _OperacoesBancoDados;

type
  TSessaoGenerica = class(TObject)
  private
    FConexao: TConexao;
    FEGerenteSistema: Boolean;
    FAutorizacoesRotinas: TArray<RecAutorizacoes>;
  public
    constructor Create(pConexao: TConexao; const pUsuarioId: Integer);

    function EGerenteSistema: Boolean;
    function AutorizadoRotina(const pNomeAutorizacao: string): Boolean;

    function getColunaStr(const pColuna: string; const pTabela: string; const pWhere: string; const pParametros: array of Variant): string;
    function getColunaInt(const pColuna: string; const pTabela: string; const pWhere: string; const pParametros: array of Variant): Integer;
  end;

var
  SessaoGenerica: TSessaoGenerica;

implementation

{ TSessaoGenerica }

constructor TSessaoGenerica.Create(pConexao: TConexao; const pUsuarioId: Integer);
begin
  FConexao := pConexao;
  FAutorizacoesRotinas := _Autorizacoes.BuscarAutorizacoes(FConexao, 0, [pUsuarioId, 'R']);

  FEGerenteSistema := getColunaInt('USUARIO_GERENTE_SISTEMA_ID', 'PARAMETROS', '', []) = pUsuarioId;
end;

function TSessaoGenerica.EGerenteSistema: Boolean;
begin
  Result := FEGerenteSistema;
end;

function TSessaoGenerica.getColunaInt(const pColuna, pTabela, pWhere: string; const pParametros: array of Variant): Integer;
var
  vSql: TConsulta;
begin
  Result := 0;
  vSql := TConsulta.Create(FConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add(  pColuna);
  vSql.SQL.Add('from ');
  vSql.SQL.Add(  pTabela);

  if pWhere <> '' then begin
    vSql.SQL.Add('where ');  
    vSql.SQL.Add(  pWhere);
  end;

  if vSql.Pesquisar(pParametros) then
    Result := vSql.GetInt(0);

  vSql.Active := False;
  vSql.Free;
end;

function TSessaoGenerica.getColunaStr(const pColuna, pTabela, pWhere: string; const pParametros: array of Variant): string;
var
  vSql: TConsulta;
begin
  Result := '';
  vSql := TConsulta.Create(FConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add(  pColuna);
  vSql.SQL.Add('from ');
  vSql.SQL.Add(  pTabela);
  vSql.SQL.Add('where ');
  vSql.SQL.Add(  pWhere);

  if vSql.Pesquisar(pParametros) then
    Result := vSql.GetString(0);

  vSql.Active := False;
  vSql.Free;
end;

function TSessaoGenerica.AutorizadoRotina(const pNomeAutorizacao: string): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Low(FAutorizacoesRotinas) to High(FAutorizacoesRotinas) do begin
    if pNomeAutorizacao = FAutorizacoesRotinas[i].id then begin
      Result := True;
      Break;
    end;
  end;
end;

end.
