unit _Conexao;

interface

uses
  Vcl.Forms, Data.DB, DBAccess, Ora, System.SysUtils, System.Classes, _BibliotecaGenerica, _Logs, Vcl.Dialogs, System.UITypes;

type
  TConexao = class(TOraSession)
  private
    FIPServidor: string;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Conectar(pUserName: string; pPassWord: string; pServer: string; pPorta: Integer; pServico: string; pDireto: Boolean);
    procedure IniciarTransacao;
    procedure FinalizarTransacao;
    procedure VoltarTransacao;
    procedure IniciarSessao(pEmpresaId: Integer; pFuncionarioID: Integer; pNomeComputador: string);
    function EmTransacao: Boolean;
    procedure SetRotina(pRotina: string);
    function getIPServidor: string;
  end;

implementation

uses
  _OperacoesBancoDados;

{ Conexao }

procedure TConexao.Conectar(pUserName: string; pPassWord: string; pServer: string; pPorta: Integer; pServico: string; pDireto: Boolean);
begin
  Self.Options.Direct := pDireto;
  FIPServidor := pServer;

  if pDireto then begin
    Server   := pServer + ':' + IntToStr(pPorta) + ':' + pServico;
    Password := pPassWord;
    ConnectString := pUserName + '/' + pPassWord + '@' + pServer + ':' + IntToStr(pPorta) + ':' + pServico;
  end
  else begin
    Server   := pServer + ':' + IntToStr(pPorta) + '/' + pServico;
    Password := pPassWord;
    ConnectString := pUserName + '/' + pPassWord + '@' + pServer + ':' + IntToStr(pPorta) + '/' + pServico;
  end;

  Connect;
end;

constructor TConexao.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Options.Direct := True;
  ConnectPrompt := False;
  LoginPrompt := False;
end;

function TConexao.EmTransacao: Boolean;
begin
  Result := Self.InTransaction;
end;

procedure TConexao.FinalizarTransacao;
begin
  Commit;
end;

function TConexao.getIPServidor: string;
begin
  Result := FIPServidor;
end;

procedure TConexao.IniciarTransacao;
begin
  if InTransaction then begin
    if _BibliotecaGenerica.Perguntar('Existe uma transa��o ativa, deseja efetuar roolback?') then
      VoltarTransacao;
  end;
  StartTransaction;
end;

procedure TConexao.SetRotina(pRotina: string);
var
  vSql: TExecucao;
begin
  vSql := TExecucao.Create(Self);
  vSql.Sql.Add('begin ');
  vSql.Sql.Add('  SESSAO.ROTINA := substr(:P1, 1, 30); ');
  vSql.Sql.Add('end; ');

  vSql.Executar([pRotina]);
end;

procedure TConexao.IniciarSessao(pEmpresaId: Integer; pFuncionarioID: Integer; pNomeComputador: string);
var
  vProc: TProcedimentoBanco;
begin
  vProc := TProcedimentoBanco.Create(Self, 'INICIAR_SESSAO');
  try
    try
      vProc.Params[0].AsInteger := pEmpresaId;
      vProc.Params[1].AsInteger := pFuncionarioID;
      {$IFDEF SIGO_VAREJO}
        vProc.Params[2].AsString  := 'SIGO_VAREJO';
      {$ELSE}
        vProc.Params[2].AsString  := 'SIGO_PDV';
      {$ENDIF}
      vProc.Params[3].AsString  := pNomeComputador;

      vProc.Executar;
    except
      on e: Exception do begin
        if Pos('Falha ao validar objetos no banco de dados', e.Message) > 0 then
          ShowMessage(e.Message)
        else
          raise;
      end;
    end;
  finally
    vProc.Free;
  end;
end;

procedure TConexao.VoltarTransacao;
begin
  Rollback;
end;

end.
