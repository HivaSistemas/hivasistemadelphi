unit _Criptografia;

interface

uses
  Classes, LbCipher, LbClass, LbAsym, LbRSA;

type
  TCriptografia = class(TLbRSA)
  private
    procedure Inicializa(const pSimples: Boolean);
  public
    function Criptografar(const pTexto: string): string; overload;
    function Descriptografar(const pTexto: string): string; overload;

    function Criptografar(const pTexto: string; const pSimples: Boolean): string; overload;
    function Descriptografar(const pTexto: string; const pSimples: Boolean): string; overload;

    function getCriptografiaSenhaPadrao: string;
  end;

implementation

{ TCriptografia }

resourcestring
  // 123456
  coSenhaPadrao = 'djSXaD8sI5eUIIfBcT85hNVPHIW3K9IC7Gzx6ypTd4U=';

  coExpPublica   = 'CF04';
  coChavePublica = '65E43F9043D48C80C42AF6614A5838A251F6BAEB2A3B5D7A9FCD7BE711ECFE78';

  coExpPrivada   = 'FF2F02445EBE11E3B689A38E3D310214672A81E7618F2528BD80146C5B686D2C';
  coChavePrivada = '65E43F9043D48C80C42AF6614A5838A251F6BAEB2A3B5D7A9FCD7BE711ECFE78';

  coExpPublicaSimples   = '1F42';
  coChavePublicaSimples = 'F341FB6FE4FFE4D3016E1C42DAE38D97';

  coExpPrivadaSimples   = '57BC98A7561B67399AD37975C8903704';
  coChavePrivadaSimples = 'F341FB6FE4FFE4D3016E1C42DAE38D97';

function TCriptografia.Criptografar(const pTexto: string): string;
begin
  Result := Criptografar(pTexto, True);
end;

function TCriptografia.Descriptografar(const pTexto: string): string;
begin
  Result := Descriptografar(pTexto, True);
end;

function TCriptografia.Criptografar(const pTexto: string; const pSimples: Boolean): string;
begin
  Inicializa(pSimples);
  Result := string( EncryptString(AnsiString(pTexto)) );
end;

function TCriptografia.Descriptografar(const pTexto: string; const pSimples: Boolean): string;
begin
  Inicializa(pSimples);
  Result := string( DecryptString( AnsiString(pTexto)) );
end;

function TCriptografia.getCriptografiaSenhaPadrao: string;
begin
  Result := coSenhaPadrao;
end;

procedure TCriptografia.Inicializa(const pSimples: Boolean);
begin
  if pSimples then begin
    KeySize := aks128;

    PublicKey.ExponentAsString := coExpPublicaSimples;
    PublicKey.ModulusAsString  := coChavePublicaSimples;

    PrivateKey.ExponentAsString := coExpPrivadaSimples;
    PrivateKey.ModulusAsString  := coChavePrivadaSimples;
  end
  else begin
    KeySize := aks256;

    PublicKey.ExponentAsString := coExpPublica;
    PublicKey.ModulusAsString  := coChavePublica;

    PrivateKey.ExponentAsString := coExpPrivada;
    PrivateKey.ModulusAsString  := coChavePrivada;
  end;
end;

end.
