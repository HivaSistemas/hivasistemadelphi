unit MemoAltis;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.StdCtrls, Vcl.Graphics;

type
  TMemoAltis = class(TMemo)
  private
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
  public
    constructor Create(AOwner: TComponent); override;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Altis', [TMemoAltis]);
end;

{ TMemoAltis }

procedure TMemoAltis.CMEnter(var Message: TCMEnter);
begin
  inherited;

  if Self.Color = clWindow then
    Self.Color := $0080FFFF;
end;

procedure TMemoAltis.CMExit(var Message: TCMExit);
begin
  inherited;

  if Self.Color = $0080FFFF then
    Self.Color := clWindow;
end;

constructor TMemoAltis.Create(AOwner: TComponent);
begin
  inherited;
  Self.Color := clWindow;
  CharCase := ecUpperCase;
end;

end.
