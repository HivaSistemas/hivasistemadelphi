unit EditLuka;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.StdCtrls, Vcl.Graphics, _Biblioteca, Winapi.Windows, Vcl.Buttons, System.StrUtils;

type
  TTipoCampo = (tcTexto, tcNumerico);

  TEditLuka = class(TEdit)
  private
    FIdInformacao: Integer;
    FTipoMovimentoInformacao: string;
    FApenasPositivo: Boolean;
    FTipoEdit: TTipoCampo;
    FCasasDecimais: Integer;
    FPadraoEstoque: Boolean;
    FValorMaximo: Currency;
    FNaoAceitarEspaco: Boolean;

    procedure SetTipoCampo(const pValor: TTipoCampo);
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure SetCasasDecimais(const pCasasDecimais: Integer); overload;
    procedure SetCasasDecimais(const pCasasDecimais: Integer; const pValorAtual: Double); overload;
    procedure SetApenasPositivo(const pValor: Boolean);

    function  GetInteger: Integer;
    procedure SetInteger(const pValor: Integer);

    function  GetCurr: Currency;
    procedure SetCurrency(const pValor: Currency);

    function  GetDouble: Double;
    procedure SetDouble(const pValor: Double);
  protected
    procedure KeyPress(var Key: Char); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure Click; override;
  public
    constructor Create(AOwner: TComponent); override;
    procedure SetInformacao(const pId: Integer; const pDescricao: string = ''); overload;
    procedure SetInformacao(const pId: string; const pDescricao: string = ''); overload;
    procedure SetInformacao(const pId: Integer; const pDescricao: string; const pTipoMovimentoInformacao: string); overload;
    procedure Clear; override;
    procedure SetFocus; override;

    function Trim: string;
  published
    property TipoCampo: TTipoCampo read FTipoEdit write SetTipoCampo;
    property ApenasPositivo: Boolean read FApenasPositivo write SetApenasPositivo;
    property AsInt: Integer read GetInteger write SetInteger;
    property AsDouble: Double read GetDouble write SetDouble;
    property AsCurr: Currency read GetCurr write SetCurrency;
    property CasasDecimais: Integer read FCasasDecimais write SetCasasDecimais;
    property IdInformacao: Integer read FIdInformacao;
    property TipoMovimentoInformacao: string read FTipoMovimentoInformacao;
    property PadraoEstoque: Boolean read FPadraoEstoque write FPadraoEstoque default False;
    property ValorMaximo: Currency read FValorMaximo write FValorMaximo;
    property NaoAceitarEspaco: Boolean read FNaoAceitarEspaco write FNaoAceitarEspaco;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('LD Sistemas', [TEditLuka]);
end;

{ TEditLuka }

constructor TEditLuka.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Self.Color := clWindow;
  FApenasPositivo := False;
  CharCase := ecUpperCase;
end;

function TEditLuka.GetCurr: Currency;
var
  vValor: Currency;
begin
  vValor := SFormatDouble(Text);
  Result := vValor;
end;

function TEditLuka.GetDouble: Double;
begin
  Result := SFormatDouble(Text);
end;

function TEditLuka.GetInteger: Integer;
begin
  Result := SFormatInt(Text);
end;

procedure TEditLuka.KeyDown(var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_RETURN) and (FCasasDecimais > 0) then
    SetCasasDecimais(FCasasDecimais, SFormatDouble(Text));

  inherited;
end;

procedure TEditLuka.KeyPress(var Key: Char);
begin
  if (FTipoEdit = tcNumerico) and (Key <> #8) then begin
    try
      StrToInt(Key);
    except
      if not((Key = ',') and (FCasasDecimais > 0) and (Pos(',', Text) = 0)) then
        Key := #0;
    end;
  end
  else if Key = ' ' then begin
    if FNaoAceitarEspaco then
      Key := #0;
  end;

  inherited KeyPress(Key);
end;

procedure TEditLuka.SetApenasPositivo(const pValor: Boolean);
begin
  FApenasPositivo := pValor;

  if FApenasPositivo then
    SetInteger(SFormatInt(Text));
end;

procedure TEditLuka.SetCasasDecimais(const pCasasDecimais: Integer; const pValorAtual: Double);
begin
  if TipoCampo = tcNumerico then begin
    FCasasDecimais := pCasasDecimais;
    case FCasasDecimais of
      0: Text := FormatFloat('#,###', pValorAtual);
      1: Text := FormatFloat('#,##0.0', pValorAtual);
      2: Text := FormatFloat('#,##0.00', pValorAtual);
      3: Text := FormatFloat('#,##0.000', pValorAtual);
      4: Text := FormatFloat('#,##0.0000', pValorAtual);
      5: Text := FormatFloat('#,##0.00000', pValorAtual);
      6: Text := FormatFloat('#,##0.000000', pValorAtual);
    else
      Text := '';
      CasasDecimais := 0;
    end;

    if (FValorMaximo > 0) and (pValorAtual > FValorMaximo) then
      Text := '';
  end;
//  else begin
//    Text := '';
//    FCasasDecimais := 0;
//  end;
end;

procedure TEditLuka.SetCurrency(const pValor: Currency);
begin
  SetCasasDecimais(FCasasDecimais, pValor);
end;

procedure TEditLuka.SetCasasDecimais(const pCasasDecimais: Integer);
begin
  if pCasasDecimais <> FCasasDecimais then
    SetCasasDecimais(pCasasDecimais, 0);
end;

procedure TEditLuka.SetDouble(const pValor: Double);
begin
  SetCasasDecimais(FCasasDecimais, pValor);
end;

procedure TEditLuka.SetFocus;
begin
  inherited;
  SelectAll;
end;

procedure TEditLuka.SetInformacao(const pId: Integer; const pDescricao: string = '');
begin
  if pId = 0 then
    Exit;

  FIdInformacao := pId;
  Text := NFormat(pId) + IfThen(pDescricao <> '', ' - ' + pDescricao);
end;

procedure TEditLuka.SetInformacao(const pId, pDescricao: string);
begin
  if pId = '' then
    Exit;

  Text := pId + IfThen(pDescricao <> '', ' - ' + pDescricao);
end;

procedure TEditLuka.SetInformacao(const pId: Integer; const pDescricao: string; const pTipoMovimentoInformacao: string);
begin
  SetInformacao(pId, pDescricao);
  FTipoMovimentoInformacao := pTipoMovimentoInformacao;
end;

procedure TEditLuka.SetInteger(const pValor: Integer);
begin
  if (csLoading in ComponentState) then
    Exit;

  if FApenasPositivo and (pValor <= 0) then
    Text := ''
  else
    Text := IntToStr(pValor);
end;

procedure TEditLuka.SetTipoCampo(const pValor: TTipoCampo);
begin
  if FTipoEdit = pValor then
    Exit;

  FTipoEdit := pValor;
  if pValor = tcTexto then begin
    Alignment := taLeftJustify;
    CasasDecimais := 0;
  end
  else
    Alignment := taRightJustify;
end;

function TEditLuka.Trim: string;
begin
  Text := System.SysUtils.Trim(Text);
  Result := Text;
end;

procedure TEditLuka.Clear;
begin
  inherited;
  if (TipoCampo = tcNumerico) and (FCasasDecimais > 0) then
    SetCasasDecimais(FCasasDecimais, SFormatDouble(Text));

  FIdInformacao := 0;
end;

procedure TEditLuka.Click;
begin
  inherited;

  if TipoCampo = tcNumerico then
    SelectAll;
end;

procedure TEditLuka.CMEnter(var Message: TCMEnter);
begin
  inherited;

  if Self.Color = clWindow then
    Self.Color := $0080FFFF;

  if TipoCampo = tcNumerico then
    SelectAll;
end;

procedure TEditLuka.CMExit(var Message: TCMExit);
begin
  inherited;

  if TipoCampo = tcNumerico then
    SetCasasDecimais(FCasasDecimais, SFormatDouble(Text));

  if Self.Color = $0080FFFF then
    Self.Color := clWindow;
end;

end.
