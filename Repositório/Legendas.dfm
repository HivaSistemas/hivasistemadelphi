inherited FormLegendas: TFormLegendas
  Caption = 'Legendas'
  ClientHeight = 180
  ClientWidth = 329
  FormStyle = fsStayOnTop
  ExplicitWidth = 335
  ExplicitHeight = 209
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 143
    Width = 329
    ExplicitTop = 143
    ExplicitWidth = 329
  end
  object cbLegendas: TCategoryButtons
    Left = 0
    Top = 2
    Width = 321
    Height = 135
    Align = alCustom
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsNone
    ButtonFlow = cbfVertical
    ButtonHeight = 28
    ButtonWidth = 28
    Categories = <
      item
        Caption = 'Em promo'#231#227'o'
        Color = clMaroon
        Collapsed = False
        GradientColor = clMaroon
        Items = <>
      end
      item
        Caption = 'Kit'
        Color = clGreen
        Collapsed = False
        GradientColor = clGreen
        Items = <>
      end
      item
        Caption = 'Fisico > Real'
        Color = clYellow
        Collapsed = False
        Items = <>
      end
      item
        Caption = 'Fisico < Real'
        Color = clSilver
        Collapsed = False
        Items = <>
      end>
    Enabled = False
    RegularButtonColor = clWhite
    SelectedButtonColor = 15132390
    TabOrder = 1
  end
end
