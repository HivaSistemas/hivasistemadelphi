unit GridLuka;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Forms, Grids, Windows, StdCtrls,
  Dialogs, DateUtils, Gradiente, System.Math, System.UITypes, Vcl.Menus;

type
  TColunaGrid = class
  public
    posicao: integer;
    nome: string;
    constructor Create(posicao: integer);
  end;

  TGridColorEvent = procedure(
  	Sender: TObject;
    ARow, ACol: Longint;
    AState: TGridDrawState;
    ABrush: TBrush;
    AFont: TFont
  ) of object;

  TGridAlignEvent = procedure(
  	Sender: TObject;
  	ARow, ACol: Longint;
    var AAlignment: TAlignment
  ) of object;

  TGridPictureEvent = procedure(
  	Sender: TObject;
    ARow, ACol: LongInt;
    var APicture: TPicture
  ) of object;         

  TGridMaxLengthEvent = procedure(
  	Sender: TObject;
    ARow: LongInt;
    ACol: LongInt;
    var AMaxLength: Longint
  ) of object;

  TGridArrumarGrid = procedure(
    Sender: TObject;
    ARow: LongInt;
    ACol: LongInt;
    var TextCell: string
  ) of object;


  TGridLuka = class(TStringGrid)

  type
    Comparador = function(Grid: TGridLuka; Col, Row1, Row2: Integer; tipo_dados: string): Double;

  private
    { Private declarations }
    pesquisa: TFindDialog;
    expanding_cols_local: Boolean;

    FIncrementExpandCol: Integer;
    FIncrementExpandRow: Integer;
    FCorLinhaFoco: TColor;
    FCorFundoFoco: TColor;
    FCorLinhaDesfoque: TColor;
    FCorFundoDesfoque: Tcolor;
    FCorSuperiorCabecalho: TColor;
    FCorInferiorCabecalho: TColor;
    FCorSeparadorLinhas: TColor;

    FAlign: TAlignment;
    FGrid3D : Boolean;
    FHCol: TStrings;
    FHRow: TStrings;

    FPictureCell: TPicture;

    FOnGetCellColor: TGridColorEvent;
    FOnGetAlignment: TGridAlignEvent;
    FOnGetMaxLength: TGridMaxLengthEvent;
    FOnGetCellPicture: TGridPictureEvent;
    FOnClearGrid: TNotifyEvent;
    FOnArrumarGrid: TGridArrumarGrid;
    FOnResize: TNotifyEvent;

    FOrdenarOnClick: Boolean;
    FOrdenarCharDetalhe: string;
    
    FRealColCount: Integer;
    FOcultarColunas: Boolean;

    // Armazena a largura de todas as colunas do grid no momento em que � carregado na mem�ria.
    ValorColunaOculta: Integer;
    LargurasColunas: array of Integer;

    // Linhas a serem ajustadas
    LinhasReduzidas: array of Integer;
    LinhasAumentadas: array of Integer;

    // Vari�veis para controle de redimensionamento de colunas fixas
    inicio_edicao_coluna_fixa: Integer;
    coluna_fixa_editando: Integer;

    gradiente: TGradiente;

    colunas: TList;
    colunas_ocultas: array of Integer;
    grid_coord: TGridCoord;

    FOrdenando: Boolean;
    FColunaCharDetalhe: Integer;
    FUltimaColunaOrdenada: Integer;
    FCorFonteColunaFoco: TColor;
    FCorColunaFoco: TColor;
    FIndicador: Boolean;
    FPrimeiraColunaVisivel: Integer;
    FCorFonteLinhaFoco: TColor;
    FAtivarPopUpSelecao: Boolean;

    FPopUp: TPopupMenu;

    procedure SetIncExpansaoCol(x: Integer);
    procedure SetIncExpansaoRow(x: Integer);
    procedure SetCorLinhaFoco(x: TColor);
    procedure SetCorFundoFoco(x: TColor);
    procedure SetCorLinhaDesfoque(x: TColor);
    procedure SetCorFundoDesfoque(x: TColor);
    procedure SetCorSuperiorCabecalho(x: TColor);
    procedure SetCorInferiorCabecalho(x: TColor);
    procedure SetCorSeparadorLinhas(x: TColor);
    procedure SetRealColCount(x: Integer);
    function  GetRealColCount: Integer;
    function GetOcultarColunas: Boolean;
    procedure SetOcultarColunas(x: Boolean);

    function  GetAlign: TAlignment;
    procedure SetAlign(const Value: TAlignment);
    procedure SetGrid3D(Value: Boolean);
    procedure InitHCol;
    procedure InitHRow;
    procedure SetHCol(Value: TStrings);
    procedure SetHRow(Value: TStrings);
    procedure SetCells(ACol, ARow: Integer; const Value: string);

    function ColunaFixaMouse(X: Integer): Integer;
    procedure PesquisaOnFind(Sender: TObject);
    procedure GridQuickSort(L, R: Integer; SCompare: Comparador; Col: Integer; tipo_dados: string);
// Procedimentos para ordenacao das colunas
    procedure OrdenarValores(const coluna: Integer; SCompare: Comparador; const tipo_dados: string);
    function DataOk(s:string):boolean;
    procedure SetCorColunaFoco(x: TColor);
    procedure SetCorFonteColunaFoco(x: TColor);
    procedure SetCorFonteLinhaFoco(const Value: TColor);
    procedure setAtivarPopUpSelecao(pValor: Boolean);
  protected
    { Protected declarations }
    const
     c_tamanho_maximo_auto_fit:cardinal = 400;

    function  SelectCell(ACol, ARow: Longint): Boolean; override;
    procedure DrawCell(ACol, ARow : Longint; ARect : TRect; AState : TGridDrawState); override;
    procedure DrawCellPicture(ARect: TRect; APicture: TPicture);
    procedure FixedCellClick(ACol, ARow: Longint); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure ColumnMoved(FromIndex, ToIndex: Integer); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure SetEditText(ACol, ARow: Longint; const Value: string); override;
    procedure DblClick(); override;
    procedure Resize(); override;
  public
    { Public declarations }

    procedure Loaded; override;
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure DeleteRow(FRow:Integer; colunas:Integer = -1); reintroduce;
    procedure DeleteCol(FCol: Integer);
    procedure MoveTo(ACol, ARow: longint);
    procedure Deselect;
    Procedure ClearGrid(NewRowCount:Integer = 0);
    Function  IsRowEmpty(FRow:Integer):Boolean;
    function  Aadd(Vetor:array of string):Integer;
    function  FindRow(Coluna: Integer; S:String): Integer;
    Function  LocalizarColuna(pLinha: Integer; pTexto: string): Integer;
    function  TrocaRow(FRow1, FRow2: Integer; colunas: Integer = -1): Boolean;
    function  Localizar(colunas:array of Integer;valores:array of string):Integer;
    procedure ClearRow(linha:Integer; colunas:Integer = -1);
    property  Cells write SetCells;

    // procedimentos utilizados na mesclagem de c�lulas.
    procedure MergeCells(
      Cell: array of Integer;
      CellInicial: array of Integer;
      CellFinal: array of Integer;
      AAlignment: TAlignment;
      Rect: TRect
    );
    function CellIN(Cell: array of Integer; CellInicial: array of Integer; CellFinal: array of Integer): Boolean;
    // ............................................................................................ //

    // procedimento para mostrar/ocultar colunas e linhas do grid (com efeito).
    procedure OcultarColunas(Colunas: array of Integer);
    procedure MostrarColunas(Colunas: array of Integer);
    procedure ExpandCols(Colunas: array of Integer);
    procedure ExpandRows(Linhas: array of Integer);
    function  ExpandingCols: Boolean;

    // procedimentos de ajuste de linhas.
    procedure AjustarLinhas;
    procedure AddLinhaReduzida(Linha: Integer);
    procedure AddLinhaAumentada(Linha: Integer);
    procedure AjustarData(coluna: Integer; linha: Integer);
    procedure AddNaPrimeiraLinha(qtde_coluna: Integer);

    // procedimento para editar com o combo
    //procedure EditarCombo(Itens: array of string; Valores: array of string; ColunaValor: Integer);

    // Este m�todo serve para chamar uma tela e o usu�rio informar quais telas ele quer ocultar ou mostrar.
    procedure AddListaColunasOcultas(_Colunas: array of Integer);

    // Este m�todo serve para retornar a largura original de uma coluna do grid,
    // j� que este tamanho pode ser alterado pelo usu�rio a qualquer momento.
    function  LarguraOriginalColuna(Coluna: Integer): Integer;
    procedure AtualizarValoresLargurasColunas;
    procedure AutoFit(Coluna: Integer; Valor_Maximo: Integer);

    // Objeto de controle das colunas do grid
    procedure addColuna(coluna: TColunaGrid); overload;
    procedure addColuna(var coluna: TColunaGrid; posicao: Integer; nome: string = ''); overload;
    function getColuna(indice: Integer): TColunaGrid;
    procedure MoveColuna(posicao_atual: Integer; nova_posicao: Integer);

    procedure Arrumar;
    procedure SetTextArrumarGrid(ACol: LongInt; ARow: LongInt; NewText: string; var DestTextCell: string);

    procedure SetLinhasGridPorTamanhoVetor( pTamanho: Integer );
  published
    { Published declarations }
    property IncrementExpandCol: Integer read FIncrementExpandCol write SetIncExpansaoCol;
    property IncrementExpandRow: Integer read FIncrementExpandRow write SetIncExpansaoRow;
    property CorLinhaFoco: TColor read FCorLinhaFoco write SetCorLinhaFoco;
    property CorFundoFoco: TColor read FCorFundoFoco write SetCorFundoFoco;
    property CorLinhaDesfoque: TColor read FCorLinhaDesfoque write SetCorLinhaDesfoque;
    property CorFundoDesfoque: TColor read FCorFundoDesfoque write SetCorFundoDesfoque;
    property CorSuperiorCabecalho: TColor read FCorSuperiorCabecalho write SetCorSuperiorCabecalho;
    property CorInferiorCabecalho: TColor read FCorInferiorCabecalho write SetCorInferiorCabecalho;
    property CorSeparadorLinhas: TColor read FCorSeparadorLinhas write SetCorSeparadorLinhas;
    property CorColunaFoco: TColor read FCorColunaFoco write SetCorColunaFoco default $00FFDD97;
    property CorFonteColunaFoco: TColor read FCorFonteColunaFoco write SetCorFonteColunaFoco default clWhite ;
    property CorFonteLinhaFoco: TColor read fCorFonteLinhaFoco write SetCorFonteLinhaFoco default clWindowText;
    property OcultarColuna: Boolean read GetOcultarColunas write SetOcultarColunas default False;

    property HCol: TStrings read FHCol write SetHCol;
    property HRow: TStrings read FHRow write SetHRow;
    property OnGetCellColor: TGridColorEvent read FOnGetCellColor write FOnGetCellColor;
    property OnGetAlignment: TGridAlignEvent read FOnGetAlignment write FOnGetAlignment;
    property OnGetCellPicture: TGridPictureEvent read FOnGetCellPicture write FOnGetCellPicture;
    property OnClearGrid: TNotifyEvent read FOnClearGrid write FOnClearGrid;
    property OnArrumarGrid: TGridArrumarGrid read FOnArrumarGrid write FOnArrumarGrid;
    property Grid3D: Boolean read FGrid3D write SetGrid3D;
    property Alignment: TAlignment read GetAlign write SetAlign default taLeftJustify;
    property RealColCount: Integer read GetRealColCount write SetRealColCount;
    property OnGetMaxLength: TGridMaxLengthEvent read FOnGetMaxLength write FOnGetMaxLength;
    property OnResize: TNotifyEvent read FOnResize write FOnResize;
    property OrdenarOnClick: Boolean read FOrdenarOnClick write FOrdenarOnClick default False;
    property OrdenarCharDetalhe: string read FOrdenarCharDetalhe write FOrdenarCharDetalhe;
    property Indicador: Boolean read FIndicador write FIndicador default False;
    property AtivarPopUpSelecao: Boolean read FAtivarPopUpSelecao write setAtivarPopUpSelecao;
  end;

procedure Register;

implementation

uses
	Types, _BibliotecaGenerica;

procedure Register;
begin
  RegisterComponents('LD Sistemas', [TGridLuka]);
end;

function GridCompare(Grid: TGridLuka; Col, Row1, Row2: Integer; tipo_dados: string): Double;
begin
  { ********* Tipo de dados ************
    D = Data
    N = Numerico
    S = string
  }

  if tipo_dados = 'D' then
    Result := StrToDate(Grid.Cells[Col, Row1]) - StrToDate(Grid.Cells[Col, Row2])
  else if tipo_dados = 'N' then
    Result := SFormatDouble(Grid.Cells[Col, Row1]) - SFormatDouble(Grid.Cells[Col, Row2])
  else
    Result := AnsiCompareText(Grid.Cells[Col, Row1], Grid.Cells[Col, Row2]);
end;

constructor TGridLuka.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  ColCount := 5;
  RowCount := 2;
  FixedCols := 0;
  DefaultRowHeight := 19;
  DrawingStyle := gdsThemed;

  colunas := TList.Create;
  colunas_ocultas := nil;

  inicio_edicao_coluna_fixa := -1;
  coluna_fixa_editando := -1;
  FIndicador := False;
  FOrdenando := False;
  FColunaCharDetalhe := -1;
  FUltimaColunaOrdenada := -1;

  FHCol := TStringList.Create;
  FHRow := TStringList.Create;

  FAlign := taLeftJustify;
  FixedColor := $00EAEAEA;
  FCorLinhaFoco := $000096DB;
  FCorFundoFoco := $00FFF5E1;
  FCorLinhaDesfoque := $00E2E2E2;
  FCorFundoDesfoque := $00F9F9F9;
  FCorSuperiorCabecalho := clWhite;
  FCorInferiorCabecalho := $00CECECE;
  FCorSeparadorLinhas := $00B7B7B7;
  FCorFonteColunaFoco := clWhite;
  fCorFonteLinhaFoco := clWindowText;
  FCorColunaFoco := clMenuHighlight;

  LinhasReduzidas := nil;
  LinhasAumentadas := nil;

  Self.Options := Self.Options - [goVertLine];
  Self.Options := Self.Options + [goColSizing];

  gradiente := TGradiente.Create(Self.Canvas);

  expanding_cols_local := False;

  pesquisa := TFindDialog.Create(Self);
  pesquisa.OnFind := PesquisaOnFind;
  pesquisa.Options := [frDown, frDisableWholeWord];
end;

destructor TGridLuka.Destroy;
var
  i: Integer;
  c: TColunaGrid;
begin
  gradiente.Free;

  for i := colunas.Count - 1 downto 0 do begin
    c := TColunaGrid(colunas[i]);
    c.Free;
    colunas[i] := nil;
    colunas.Delete(i);
  end;

  colunas.Free;

  pesquisa.Free;

  inherited;
end;

procedure TGridLuka.MoveColuna(posicao_atual, nova_posicao: Integer);
var
  i: Integer;
begin
  colunas.Move(posicao_atual, nova_posicao);
  for i := 0 to colunas.Count - 1 do
    TColunaGrid(colunas[i]).posicao := i;
end;

procedure TGridLuka.MoveTo(ACol, ARow: longint);
begin
  MoveColRow(ACol, ARow, True, True);
end;

procedure TGridLuka.OcultarColunas(Colunas: array of Integer);
var
  i: Integer;
begin
  for i := low(Colunas) to high(Colunas) do
    Self.ColWidths[Colunas[i]] := ValorColunaOculta;
end;

procedure TGridLuka.OrdenarValores(
  const coluna: Integer;
  SCompare: Comparador;
  const tipo_dados: string
);
var
  i: Integer;
  linha_final: Integer;
  linha_inicial: Integer;
begin
  linha_final := 0;
  linha_inicial := 0;

  for i := FixedRows to RowCount - 1 do begin
    if (Cells[FColunaCharDetalhe, i] = OrdenarCharDetalhe) and (linha_inicial = 0) then
      linha_inicial := i;

    if ((Cells[FColunaCharDetalhe, i + 1] <> OrdenarCharDetalhe) or (i = RowCount - 1)) and (linha_inicial > 0) then
      linha_final := i;

    if (linha_inicial > 0) and (linha_final > 0) then begin
      if linha_inicial <> linha_final then begin
        if not(Assigned(SCompare)) then
          GridQuickSort(linha_inicial, linha_final, @GridCompare, coluna, tipo_dados)
        else
          GridQuickSort(linha_inicial, linha_final, @SCompare, coluna, tipo_dados);
      end;
      linha_final := 0;
      linha_inicial := 0;
    end;
  end;
  FUltimaColunaOrdenada := IfThen(FUltimaColunaOrdenada = coluna, -1, coluna);
end;

procedure TGridLuka.PesquisaOnFind(Sender: TObject);
var
  ci: Integer;
  cf: Integer;

  linha: Integer;
  coluna: Integer;

  lf: Integer;
  sinal: Integer;

  tf: string;
  tc: string;
begin

  if frDown in pesquisa.Options then begin
    lf := Self.RowCount - 1;
    sinal := 1;
  end
  else begin
    lf := Self.FixedRows;
    sinal := -1;
  end;

  linha := Self.Row;
  if linha = lf then
    Exit;

  // SE FOR SELE��O DE LINHA INTEIRA, PROCURAR EM TODAS AS COLUNAS.
  // CASO CONTR�RIO, SOMENTE NA COLUNA SELECIONADA.
  if goRowSelect in Self.Options then begin
    ci := Self.FixedCols;
    cf := Self.ColCount - 1;
  end
  else begin
    ci := Self.Col;
    cf := Self.Col;
  end;


  while True do begin
    Inc(linha, sinal);

    for coluna := ci to cf do begin
      tf := pesquisa.FindText;
      tc := Self.Cells[coluna, linha];

      if not (frMatchCase in pesquisa.Options) then begin
        tf := UpperCase(tf);
        tc := UpperCase(tc);
      end;

      if Pos(tf, tc) > 0 then begin
        Self.Row := linha;
        Exit;
      end;
    end;

    if linha = lf then begin
      ShowMessage('Texto n�o localizado.');
      Exit;
    end;
  end;

end;

procedure TGridLuka.Resize;
begin
  if Assigned(FOnResize) then
    FOnResize(Self);
  inherited;
end;

procedure TGridLuka.addColuna(coluna: TColunaGrid);
begin
  colunas.Add(coluna);
end;

procedure TGridLuka.addColuna(var coluna: TColunaGrid; posicao: Integer; nome: string = '');
begin
  coluna := TColunaGrid.Create(posicao);
  coluna.nome := nome;
  colunas.Add(coluna);

  if (posicao + 1) > GetRealColCount then
    SetRealColCount(posicao + 1);
end;

procedure TGridLuka.AddLinhaAumentada(Linha: Integer);
begin
  SetLength(LinhasAumentadas, Length(LinhasAumentadas) + 1);
  LinhasAumentadas[High(LinhasAumentadas)] := Linha;
end;

procedure TGridLuka.AddLinhaReduzida(Linha: Integer);
begin
  SetLength(LinhasReduzidas, Length(LinhasReduzidas) + 1);
  LinhasReduzidas[High(LinhasReduzidas)] := Linha;
end;

procedure TGridLuka.AjustarLinhas;
var
  i: Integer;
begin
  try
    for i := Low(LinhasReduzidas) to High(LinhasReduzidas) do
    	RowHeights[LinhasReduzidas[i]] := 10;

    for i := Low(LinhasAumentadas) to High(LinhasAumentadas) do
    	RowHeights[LinhasAumentadas[i]] := 26;
  except
  	// Faz nada.
  end;
end;

procedure TGridLuka.Arrumar;
var
  Texto: string;
begin
  if Self.EditorMode and Assigned(FOnArrumarGrid) then begin
    Texto := Self.Cells[Self.Col, Self.Row];
    FOnArrumarGrid(Self, Self.Row, Self.Col, Texto);

    inherited SetEditText(Self.Col, Self.Row, Texto);
  end;
end;

procedure TGridLuka.MostrarColunas(Colunas: array of Integer);
var
  i: Integer;
begin
  for i := low(Colunas) to high(Colunas) do
    Self.ColWidths[Colunas[i]] := LargurasColunas[Colunas[i]];
end;

procedure TGridLuka.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Shift = [ssCtrl, ssLeft] then // se for para ordenar ja sai aqui para n�o dar um efeito feio na hora da ordena��o
    Exit;

  //Capturando as coordendas de c�lula no grid de acordo com a posi��o relativa do mouse
  grid_coord := MouseCoord(X, Y);
  if FixedCols > 0 then begin
    inicio_edicao_coluna_fixa := -1;
    coluna_fixa_editando := ColunaFixaMouse(X);

    if coluna_fixa_editando > -1 then begin
      Cursor := crHSplit;
      inicio_edicao_coluna_fixa := x;
    end;

    if inicio_edicao_coluna_fixa = -1 then
      inherited;
  end
  else
    inherited;
end;

procedure TGridLuka.AddListaColunasOcultas(_Colunas: array of Integer);
var
  i: Integer;
begin
  colunas_ocultas := nil;

  SetLength(colunas_ocultas, Length(_Colunas));

  for i := Low(_Colunas) to High(_Colunas) do
  	colunas_ocultas[i] := _Colunas[i];
end;

procedure TGridLuka.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
  if FixedCols > 0 then begin
    if (inicio_edicao_coluna_fixa > -1) or ( ColunaFixaMouse(X) > -1 ) then
      Cursor := crHSplit
    else begin
      Cursor := crDefault;
      inherited;
    end;
  end
  else
    inherited;
end;

procedure TGridLuka.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  novo_tamanho: Integer;
begin

  if inicio_edicao_coluna_fixa > -1 then begin
    novo_tamanho := ColWidths[coluna_fixa_editando] + (X - inicio_edicao_coluna_fixa);

    inicio_edicao_coluna_fixa := -1;

    if novo_tamanho < 5 then
      Exit;

    ColWidths[coluna_fixa_editando] := novo_tamanho;
  end
  else
    inherited;
end;

procedure TGridLuka.InitHCol;
var
  vColuna: Integer;
  vLinha: Integer;
begin
  if FixedRows = 0 Then
     vLinha := 0
  else
     vLinha := FixedRows - 1;

  for vColuna := 0 to pred(ColCount) do begin
    Cells[vColuna, 0] := '';

    if vColuna < FHCol.Count then
      Cells[vColuna, vLinha] := FHCol[vColuna]
    else
      Cells[vColuna, vLinha] := '';
  end;
end;

procedure TGridLuka.InitHRow;
var
  vLinha: Integer;
begin
  for vLinha := 0 to RowCount -2 do begin
    // Se for uma linha fixa e j� houver dados
    if (vLinha + 1 <= FixedRows) and (Cells[0, vLinha + 1] <> '') then
      Continue;

    if vLinha < FHRow.Count then
      Cells[0, vLinha + 1] := FHRow[vLinha]
    else
      Cells[0, vLinha + 1] := '';
  end;
end;

function TGridLuka.LarguraOriginalColuna(Coluna: Integer): Integer;
begin
	Result := LargurasColunas[Coluna];
end;

procedure TGridLuka.Loaded;
begin
  inherited Loaded;

  initHCol;
  initHRow;

  if Assigned(OnDrawCell) then begin
    ValorColunaOculta := 0;

  	Self.DefaultDrawing := False;
    Self.Options := Self.Options - [goFixedVertLine, goFixedHorzLine];
  end
  else
  	ValorColunaOculta := -1;

  AtualizarValoresLargurasColunas;

  if PopupMenu <> nil then
    Hint := 'Click com o bot�o direito do mouse para mais op��es.';
end;

procedure TGridLuka.SetHCol(Value: TStrings);
begin
  FHCol.Assign(Value);
  InitHCol;
  Refresh;
end;

procedure TGridLuka.SetHRow(Value: TStrings);
begin
  FHRow.Assign(Value);
  InitHRow;
  Refresh;
end;

function TGridLuka.GetAlign: TAlignment;
begin
  Result := FAlign;
end;

function TGridLuka.getColuna(indice: Integer): TColunaGrid;
begin
  Result := TColunaGrid(colunas[indice]);
end;

function TGridLuka.SelectCell(ACol, ARow: Integer): Boolean;
var
  sinal: Integer;
  ultimo_indice: Integer;
var
  i: Integer;
begin
  Result := inherited SelectCell(ACol, ARow);

  if not Result then
  	Exit;

 // se o grid estiver com a op��o RowSelect, a coluna selecionada sempre ser� a primeira.
  // (pode ser que esta coluna esteja oculta). Neste caso, permitir selecionar assim mesmo.
  if not (goRowSelect in Self.Options) then begin
  	// Verificando se a coluna n�o est� invis�vel. Se estiver, pular.
    if Self.ColWidths[ACol] < 1 then begin

      if Self.Col < ACol then begin
        sinal := 1;
        ultimo_indice := Self.ColCount -1;
      end
      else begin
        sinal := -1;
        ultimo_indice := 0 + Self.FixedCols;
      end;

      while (Self.ColWidths[ACol] < 1) and (ACol <> ultimo_indice) do
        ACol := ACol + sinal;

      if Self.ColWidths[ACol] > 0 then
        Self.Col := ACol;

      Result := False;
    end;
    if FIndicador then begin
      for I := FixedCols to ColCount  do begin
        InvalidateCell(i,ARow);
        InvalidateCell(i,Row);
      end;
    end;
  end;

  // Verificando se a linha n�o est� invis�vel. Se estiver, pular.
  if Self.RowHeights[ARow] < 1 then begin

  	if Self.Row < ARow then begin
    	sinal := 1;
      ultimo_indice := Self.RowCount - 1;
    end
    else begin
    	sinal := -1;
      ultimo_indice := 0 + Self.FixedRows;
    end;

  	while (Self.RowHeights[ARow] < 1) and (ARow <> ultimo_indice) do
    	ARow := ARow + sinal;

    if Self.RowHeights[ARow] > 0 then Self.Row := ARow;
    Result := False;
  end;
end;

procedure TGridLuka.SetAlign(const Value: TAlignment);
begin
  if FAlign <> Value then
  begin
    FAlign := Value;
    Invalidate;
  end;
end;

procedure TGridLuka.setAtivarPopUpSelecao(pValor: Boolean);

  function AddMenu(pCaption: string): TMenuItem;
  var
    vMenuItem: TMenuItem;
  begin
    vMenuItem := TMenuItem.Create(FPopUp);
    vMenuItem.Caption := pCaption;

    Result := vMenuItem;
  end;

begin
  FAtivarPopUpSelecao := pValor;
  if FAtivarPopUpSelecao then begin
    FPopUp := TPopupMenu.Create(Self);
    FPopUp.Name := 'pm' + Self.Name;
    Self.PopupMenu := FPopUp;

    FPopUp.Items.Add( AddMenu('Marcar selecionado ( Espa�o )') );
    FPopUp.Items.Add( AddMenu('Desmarcar selecionado ( Espa�o )') );
    FPopUp.Items.Add( AddMenu('-') );
    FPopUp.Items.Add( AddMenu('Marcar todos ( CTRL + Espa�o )') );
    FPopUp.Items.Add( AddMenu('Desmarcar todos ( CTRL + Espa�o )') );
    FPopUp.Items.Add( AddMenu('-') );
  end
  else if Assigned(FPopUp) then
    FPopUp.Free;
end;

procedure TGridLuka.DrawCell(ACol, ARow : Longint; ARect : TRect; AState : TGridDrawState);

  procedure DrawCellText;
  var
    Text: array[0..255] of Char;
    AlignValue : TAlignment;
    FontHeight: Integer;
    Rect : TRect;
  const
    Alignments: array[TAlignment] of Word = (DT_LEFT, DT_RIGHT, DT_CENTER);
  begin
    {determine text alignment}
    AlignValue := Alignment;

    if Assigned(FOnGetAlignment) then
      FOnGetAlignment(Self, ARow, ACol, AlignValue);

    {using of Brush for background color}
    Rect := ARect;
    Canvas.FillRect(Rect);

    {centering text in cell}
    FontHeight := Canvas.TextHeight('W');
    with Rect do begin
      Top := ((Bottom + Top) - FontHeight) shr 1;
      Bottom := Top + FontHeight;
      Dec(Right, 2);
      Inc(Left, 2);
    end;

    {drawing of text}
    StrPCopy(Text, Cells[ACol, ARow]);
    DrawText(Canvas.Handle, Text, StrLen(Text), Rect, (DT_EXPANDTABS or DT_VCENTER) or Alignments[AlignValue]);
  end;

begin
    // Se n�o for a pintura padr�o, ajustando a fonte e fundo padr�es.
  if not DefaultDrawing then begin
    Self.Canvas.Font := Self.Font;
    Self.Canvas.Brush.Color := Self.Color;
  end;

  if (ARow mod 2 = 0) and (ARow <> Row) then
    Canvas.Brush.Color := $00EEEEEE
  else
    Canvas.Brush.Color := clWhite;

  if Assigned(FOnGetCellColor) then
    FOnGetCellColor(Self, ARow, ACol, AState, Canvas.Brush, Canvas.Font) ;

  if FIndicador then begin
    if not(Canvas.Font.Color = Canvas.Brush.Color) and (ARow = Row) and (not (ACol < FixedCols)) and (not(ARow < FixedRows)) then  begin
      if (((ACol<>Col) and (ARow = Row)) or (goRowSelect in Self.Options))   then begin
        Canvas.Pen.Color := FCorLinhaFoco;
        if
          (Canvas.Brush.Color = clWhite)  or
          (Canvas.Brush.Color = Self.Color)
        then
          Canvas.Brush.Color :=  FCorFundoFoco;

        Canvas.FillRect(ARect);
      end
      else begin
        Canvas.Pen.Color := FCorLinhaFoco;
        Canvas.Font.Color := FCorFonteColunaFoco;
        Canvas.Brush.Color := FCorColunaFoco;

        Canvas.FillRect(ARect);
      end;
    end
    else begin
      Canvas.Pen.Color := FCorSeparadorLinhas;
      Canvas.FillRect(ARect);
    end;
  end;

  {text draw with alignment}
  if DefaultDrawing then begin
  	if RowHeights[ARow] >= DefaultRowHeight then
    	DrawCellText;

    {3D look cells}
    if FGrid3D and ([goHorzLine, goVertLine] * Options = [goHorzLine, goVertLine]) then begin
      with ARect do begin
        Canvas.Pen.Color := clHighLightText;
        Canvas.PolyLine([Point(Left, Bottom - 1), Point(Left, Top), Point(Right, Top)]);
      end;
    end;
  end
  else
    inherited DrawCell(ACol, ARow, ARect, AState);

  if Assigned(FOnGetCellPicture) then begin
    FPictureCell := nil;
    FOnGetCellPicture(Self, ARow, ACol, FPictureCell);
    DrawCellPicture(ARect, FPictureCell);
  end;

  if
    FIndicador and
    (ARow = Row) and
    (not (ACol < FixedCols)) and
    (not(ARow < FixedRows))
  then begin

    if CellRect(FixedCols, Row).Right > 0 then
      FPrimeiraColunaVisivel := FixedCols
    else if (CellRect(ACol-1, Row).Right = 0) and (CellRect(ACol, Row).Right > 0) then
      FPrimeiraColunaVisivel := ACol;

    Canvas.MoveTo(CellRect(FPrimeiraColunaVisivel, Row).Left, ARect.Top + 1);
    Canvas.LineTo(ARect.Right - 1    , ARect.Top + 1 );
    Canvas.MoveTo(CellRect(FPrimeiraColunaVisivel, Row).Left, ARect.Bottom - 2 );
    Canvas.LineTo(ARect.Right -1    , ARect.Bottom - 2);
  end;
end;

procedure TGridLuka.DrawCellPicture(ARect: TRect; APicture: TPicture);
var
  x: Integer;
begin
  if APicture <> nil then begin
    x := ARect.Left + (ARect.Right - ARect.Left) div 2 - APicture.Width div 2;
    if x >= ARect.Left then
    	ARect.Left := x;

    x := ARect.Top + (ARect.Bottom - ARect.Top) div 2 - APicture.Height div 2;
    if x >= ARect.Top then
    	ARect.Top := x;

    ARect.Right := ARect.Left + APicture.Width;
    ARect.Bottom := ARect.Top + APicture.Height;

    Self.Canvas.StretchDraw(ARect, APicture.Graphic);
  end;
end;

procedure TGridLuka.SetGrid3D(Value : Boolean);
begin
  if FGrid3D <> Value then begin
    FGrid3D := Value;
    Invalidate;
  end;
end;

procedure TGridLuka.SetCells(ACol, ARow: Integer; const Value: string);
begin
  if Value <> inherited Cells[ACol, ARow] then begin
    inherited Cells[ACol, ARow] := Value;

    if not DefaultDrawing then begin
      if ARow < FixedRows then
        InvalidateRow(ARow);
    end;
  end;
end;

procedure TGridLuka.Deselect;
var
  SRect: TGridRect;
begin
  with SRect do begin
    Top := 0;
    Left := ColCount;
    Bottom := 0;
    Right := Left;
  end;

  Selection := SRect;
end;

function TGridLuka.IsRowEmpty(FRow:Integer): Boolean;
var
  C: Integer;
begin
  Result := True;

  For C := 0 To ColCount-1 do begin
    if Trim(Cells[C,FRow]) <> '' Then begin
      Result := False;
      Break;
    end;
  end;
end;

procedure TGridLuka.KeyDown(var Key: Word; Shift: TShiftState);
var
	col_aux: Integer;
begin
  inherited;

  if key = VK_END then begin
    col_aux := Self.ColCount -1;
    while True do begin
      if Self.ColWidths[col_aux] = 0 then begin
        if col_aux = Self.FixedCols then
          Break
        else
      	  Dec(col_aux);
      end
      else
      	Break;
    end;

    Self.Col := col_aux;
  end
  else if key = VK_HOME then begin
    col_aux := Self.FixedCols;

    while True do begin
      if Self.ColWidths[col_aux] = 0 then begin
        if col_aux = Self.FixedCols then
           Break
        else
      	   Inc(col_aux)
      end
      else
      	Break;
    end;

    Self.Col := col_aux;
  end
  else if (Shift = [ssCtrl]) and (Key = Ord('F')) then
    pesquisa.Execute(Self.Handle)
  else if Shift = [ssCtrl] then begin
//    if key = Ord('C') then
///    	Clipboard.AsText := Self.Cells[Self.Col, Self.Row];
  end;
end;

procedure TGridLuka.ClearGrid(NewRowCount:Integer = 0);
var
  i: Integer;
begin

  for i := FixedRows to RowCount-1 do
    Rows[i].Clear;

  if NewRowCount = 0 then
    RowCount := FixedRows + 1
  else
    RowCount := NewRowCount;

  LinhasReduzidas := nil;
  LinhasAumentadas := nil;

  initHRow;

  if Assigned(FOnClearGrid) then
    FOnClearGrid(Self);
End;

function TGridLuka.Aadd(Vetor: array of string):Integer;
var
  c:Integer;
begin
  if not isRowEmpty(RowCount-1) Then
    RowCount:=RowCount+1;

  Result:=RowCount-1;

  for C:=Low(Vetor) to High(Vetor) do
    Cells[c,Result]:=Vetor[c]
End;

Function TGridLuka.FindRow(Coluna:Integer;S:String):Integer;
Begin
  Result := Cols[coluna].IndexOf(S);
End;

Function TGridLuka.LocalizarColuna(pLinha: Integer; pTexto: string): Integer;
var
  i: Integer;
Begin
  Result := -1;
  for i := 0 to ColCount -1 do begin
    if Cells[i, pLinha] = pTexto then begin
      Result := i;
      Break;
    end;
  end;
End;

procedure TGridLuka.FixedCellClick(ACol, ARow: Integer);
var
  i: Integer;
  j: Integer;
  vTipoDados: string;
  vTemDados: Boolean;

  function ContemLetras(Value: string): Boolean;
  var
    i: Integer;
  begin
    Result := False;

    if Trim(Value) = '' then
      Exit;

    for i := 0 to Length(Value) do begin
      if CharInSet(Value[i], ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '�']) then begin
        Result := True;
        Break;
      end;
    end;
  end;

begin
  inherited;
  if OrdenarOnClick then begin // Ordenacao dos valores do grid do menor para o maior
    if FOrdenando then
      Exit;

    if FOrdenarCharDetalhe <> '' then begin
      FColunaCharDetalhe := -1;
      for i := FixedRows to RowCount do begin
        for j := 0 to RealColCount do begin
          if Cells[j, i] = OrdenarCharDetalhe then begin
            FColunaCharDetalhe := j;
            Break;
          end;
        end;

        if (FColunaCharDetalhe = -1) or (Trim(Cells[ACol, i]) = '') then
          Continue;

        Break;
      end;

      if FColunaCharDetalhe = -1 then
        Exit;
    end
    else begin
      vTemDados := False;
      for i := FixedRows to RowCount do begin
        if Cells[ACol, i] <> '' then begin
          vTemDados := True;
          Break;
        end;
      end;

      if not vTemDados then
        Exit;
    end;

    FOrdenando := True;

    vTipoDados := '';
    for i := FixedRows to RowCount do begin
      if OrdenarCharDetalhe <> '' then begin
        if Cells[FColunaCharDetalhe, i] <> OrdenarCharDetalhe then
          Continue;
      end;

      if ContemLetras(Cells[ACol, i]) then begin
        vTipoDados := 'S';
        Break;
      end
      else if DataOk(Cells[ACol, i]) then begin
        vTipoDados := 'D';
        Break;
      end
      else if Abs(SFormatDouble(Cells[ACol, i])) > 0 then begin //Verifica se em algum lugar tem algum valor, se n�o tiver nem precisa ordenar mesmo.
        vTipoDados := 'N';
        Break;
      end;
    end;

//    if RowCount > 800 then begin
    //  f := TFormMensagemOrdenacaoGrid.Create(Self.Parent);
//      f.Show;

  //    Application.ProcessMessages;
  //  end;

    if OrdenarCharDetalhe <> '' then
      OrdenarValores(ACol, nil, vTipoDados)
    else begin
      GridQuickSort(FixedRows, RowCount - 1, @GridCompare, ACol, vTipoDados);
      FUltimaColunaOrdenada := IfThen(FUltimaColunaOrdenada = ACol, -1, ACol);
    end;

//    if f <> nil then
//      FreeAndNil(f);

    FOrdenando := False;
    Exit;
  end;
end;

Procedure TGridLuka.DeleteRow(FRow:Integer; colunas:Integer = -1);
Var
  I:Integer;
begin
  if not EditorMode then begin
    if RowCount - FixedRows > 1 then begin
      for i := FRow to RowCount - 2 do
        TrocaRow(i, i + 1, colunas);

      Rows[RowCount-1].Clear;
      RowCount := RowCount-1;
    end
    else
      Rows[FRow].Clear;
  end;
end;

function TGridLuka.DataOk(s: string): boolean;
begin
  try
    StrToDate(s);
    Result:=True;
  except
    Result:=False;
  end;
end;

procedure TGridLuka.DblClick;
begin
  if (grid_coord.Y <= FixedRows - 1) and (goColSizing in Self.Options) then
    AutoFit(grid_coord.X, c_tamanho_maximo_auto_fit)
  else
    inherited;
end;

procedure TGridLuka.DeleteCol(FCol: Integer);
var
  i: Integer;
begin
  if not EditorMode then begin
    if ColCount - FixedCols > 1 then begin
      for i := FCol to ColCount - 2 do
        Cols[i] := Cols[i + 1];

      Cols[ColCount - 1].Clear;
      ColCount := ColCount-1;
    end
    else
      Cols[FCol].Clear;
  end;
end;

procedure TGridLuka.SetTextArrumarGrid(ACol: LongInt; ARow: LongInt; NewText: string; var DestTextCell: string);
begin
  DestTextCell := NewText;
  if NewText <> Self.Cells[ACol, ARow] then
    Self.Cells[ACol, ARow] := NewText;
end;

Function TGridLuka.TrocaRow(FRow1,FRow2:Integer;Colunas:Integer = -1):Boolean;
var
  i:Integer;
begin
  if Colunas = -1 then
    Colunas := RealColCount;

  if (FRow1<>FRow2) and
     (FRow1>=FixedRows) and
     (FRow2>=FixedRows) and
     (FRow2<=RowCount-1) then begin
    for i := 0 to Colunas - 1 do
      Cols[i].Exchange(FRow1,FRow2);
    Result := true;
  end
  else
    Result := false;
end;

function TGridLuka.Localizar(colunas:array of Integer;valores:array of string):Integer;
var
  i,j:Integer;
  Existe:Boolean;
begin
  Result:=-1;
  for i:=FixedRows to RowCount - 1 do begin

    Existe:=True;
    for j:=Low(colunas) to high(colunas) do
      Existe:=Existe and (Cells[colunas[j],i] = valores[j]);

    if Existe then begin
      Result:=i;
      Break;
    end;

  end;
end;


procedure TGridLuka.ClearRow(linha, colunas: Integer);
var
  i:Integer;
begin
  if colunas = -1 then
    colunas := ColCount - 1;

  for i := 0 to colunas do
    Cells[i,Linha] := '';
end;

procedure TGridLuka.ColumnMoved(FromIndex, ToIndex: Integer);
begin
  inherited;
  MoveColuna(FromIndex, ToIndex);
  AtualizarValoresLargurasColunas;
end;

function TGridLuka.ColunaFixaMouse(X: Integer): Integer;
var
  i: Integer;
  tamanho_anterior: Integer;
begin
  Result := -1;

  tamanho_anterior := 0;
  for i := 0 to FixedCols - 1 do begin
    if (x >= tamanho_anterior + ColWidths[i] - 2) and (x <= tamanho_anterior + ColWidths[i] + 2) then begin
      Result := i;
      break;
    end;

    tamanho_anterior := tamanho_anterior + ColWidths[i];
  end;
end;

{procedure TGridLuka.ProcedimentoComboChange(Sender: TObject);
begin
	Self.Cells[Self.Col, Self.Row] := Combo.Text;
  Self.Cells[Self.ColunaValorCombo, Self.Row] := Combo.GetValor;
end;}


procedure TGridLuka.MergeCells(
  Cell: array of Integer;
  CellInicial: array of Integer;
  CellFinal: array of Integer;
  AAlignment: TAlignment;
  Rect: TRect
);
var
  i: Integer;
  x: Integer;
  foco: Boolean;
  texto: string;

  procedure PintarCabecalho;
  begin
    gradiente.TopToBottom(Rect, [FCorSuperiorCabecalho, FCorInferiorCabecalho]);

    if FCorSuperiorCabecalho <> FCorInferiorCabecalho then begin
      Canvas.Pen.Color := RGB(
        Trunc(GetRValue(FCorSuperiorCabecalho) * 0.94),
        Trunc(GetGValue(FCorSuperiorCabecalho) * 0.94),
        Trunc(GetBValue(FCorSuperiorCabecalho) * 0.94)
      );

//      Canvas.MoveTo(Rect.Left, Rect.Top + 1);
//      Canvas.LineTo(Rect.Left, Rect.Bottom - 1);
//      Canvas.LineTo(Rect.Right - 1, Rect.Bottom - 1);

      Canvas.MoveTo(Rect.Right - 1, Rect.Top);
      Canvas.Pen.Color := RGB(
        Trunc(GetRValue(FCorInferiorCabecalho) * 1.04),
        Trunc(GetGValue(FCorInferiorCabecalho) * 1.04),
        Trunc(GetBValue(FCorInferiorCabecalho) * 1.04)
      );
      Canvas.LineTo(Rect.Right - 1, Rect.Bottom - 1);
    end;


    // se ficar com a margem esquerda fora do limite do ret�ngulo, reajustar para o limite.
    if x < Rect.Left + 2 then
      x := Rect.Left + 2;

    SetBkMode(Self.Canvas.Handle, TRANSPARENT);
    Canvas.TextOut(x + 1, Rect.Top + ((Rect.Bottom - Rect.Top) div 2 - Canvas.TextHeight('Lj') div 2), texto);


    Canvas.Pen.Color := FCorSeparadorLinhas;
    Canvas.MoveTo(Rect.Left, Rect.Bottom);
    Canvas.LineTo(Rect.Right, Rect.Bottom);
  end;

begin
  with TGridLuka(Self) do begin
    if not CellIn(Cell, CellInicial, CellFinal) then
      Exit;

    texto := Cells[CellInicial[1], CellInicial[0]];

    // ajustando a �rea de pintura ..............
    for i := Cell[0] - 1 downto CellInicial[0] do
      Rect.Top := Rect.Top - RowHeights[i];

    for i := Cell[1] - 1 downto CellInicial[1] do
      Rect.Left := Rect.Left - ColWidths[i];

    for i := Cell[1] + 1 to CellFinal[1] do
      Rect.Right := Rect.Right + ColWidths[i];

    for i := Cell[0] + 1 to CellFinal[0] do
      Rect.Bottom := Rect.Bottom + RowHeights[i];
    // ..........................................


    foco :=
    	(Cell[0] = Row) and
      (
      	((Col >= CellInicial[1]) and (Col <= CellFinal[1])) or
        (goRowSelect in Options)
      );

    if foco and not FIndicador then begin
      if Focused and (Canvas.Brush.Color = Self.Color) or (Canvas.Brush.Color = clWhite) then   begin
      	Canvas.Brush.Color := FCorFundoFoco;
        if Canvas.Font.Color = Self.Font.Color then
          Canvas.Font.Color := fCorFonteLinhaFoco;

      end
      else if (Canvas.Brush.Color = clWhite) or (Canvas.Brush.Color = Self.Color) then begin
        Canvas.Brush.Color := CorFundoDesfoque;
        Canvas.Font.Color := Self.Font.Color;
      end;
    end;

    case AAlignment of
      taLeftJustify:
        x := Rect.Left + 3;
      taCenter :
        x := Rect.Left + ((Rect.Right - Rect.Left) div 2 - Canvas.TextWidth(texto) div 2);
      taRightJustify:
        x := Rect.Right - Canvas.TextWidth(texto) - 3;
    end;


    if
      ((FixedRows > 0) and (Cell[0] < FixedRows)) or
      ((FixedCols > 0) and (Cell[1] < FixedCols))
    then
      PintarCabecalho
    else begin
      if (Rect.Bottom - Rect.Top) < DefaultRowHeight then // se a altura da �rea de pintura for menor que a altura padr�o das linhas.
        texto := '';

      Canvas.TextRect(
        Rect,
        x,
        Rect.Top + ((Rect.Bottom - Rect.Top) div 2 - Canvas.TextHeight('Lj') div 2),
        texto
      );
    end;

    if foco  and not FIndicador then begin
      if not (goHorzLine in Options) then
        Rect.Bottom := Rect.Bottom + 1;

    	if Focused then begin
      	Canvas.Pen.Color := FCorLinhaFoco;
        if Canvas.Font.Color = Self.Font.Color then
          Canvas.Font.Color := fCorFonteLinhaFoco;
      end
      else begin
      	Canvas.Pen.Color := FCorLinhaDesfoque;
        Canvas.Font.Color := Self.Font.Color;
      end;

      if goRowSelect in Options then begin
      	if VisibleColCount < ColCount then
      		Rect.Right := Width - 2
        else
        	Rect.Right := CellRect(VisibleColCount - 1, Row).Right - 1;

        Canvas.MoveTo(1, Rect.Top + 1);
      end
      else begin
      	Rect.Right := Rect.Right - 1;
        Canvas.MoveTo(Rect.Left + 1, Rect.Top + 1);
      end;

      Canvas.LineTo(Rect.Right - 1, Rect.Top + 1);
      Canvas.LineTo(Rect.Right - 1, Rect.Bottom - 2);
      Canvas.LineTo(CellRect(Col, Row).Left + 1, Rect.Bottom - 2);
      Canvas.LineTo(CellRect(Col, Row).Left + 1, Rect.Top + 1);
    end;
  end;
end;

procedure TGridLuka.AtualizarValoresLargurasColunas;
var
	i: Integer;
begin

	LargurasColunas := nil;
  SetLength(LargurasColunas, ColCount);

  for i := 0 to ColCount - 1 do begin
  	if ColWidths[i] < 1 then
    	LargurasColunas[i] := DefaultColWidth
    else
    	LargurasColunas[i] := ColWidths[i];
  end;
end;

function TGridLuka.CellIN(Cell: array of Integer; CellInicial: array of Integer; CellFinal: array of Integer): Boolean;
begin
  Result :=
    ((Cell[0] >= CellInicial[0]) and (Cell[0] <= CellFinal[0])) and
    ((Cell[1] >= CellInicial[1]) and (Cell[1] <= CellFinal[1]));
end;

procedure TGridLuka.ExpandCols(Colunas: array of Integer);
var
  i: Integer;
  lf: Integer;
  TemCol: Boolean;
  Mostrar: Boolean;

  function Tamanho: Integer;
  var
    tam: Integer;
    max: Integer;
  begin
    Result := 0;

    tam := Self.ColWidths[Colunas[i]];
    max := LargurasColunas[Colunas[i]];

    if Mostrar then begin
      if tam <> max then begin
        if max - tam < FIncrementExpandCol then
          Result := max - tam
        else
          Result := FIncrementExpandCol;
      end;
    end
    else begin
      if tam > ValorColunaOculta then begin
        if tam < FIncrementExpandCol then
          Result := - 1
        else
          Result := -FIncrementExpandCol;
      end;
    end;

    TemCol := TemCol or (Result <> 0);
  end;
begin
  if Length(Colunas) = 0 then
    Exit;

  expanding_cols_local := True;

  if high(Colunas) < 4 then lf := high(Colunas)
  else lf := 4;

  TemCol := true;
  Mostrar := Self.ColWidths[Colunas[0]] < 1;

  for i := 5 to high(Colunas) do begin
    if Mostrar then
    	Self.ColWidths[Colunas[i]] := LargurasColunas[Colunas[i]]
    else
    	Self.ColWidths[Colunas[i]] := ValorColunaOculta;
  end;

  while TemCol do begin
    TemCol := false;
    Application.ProcessMessages;

    for i := low(Colunas) to lf do
      Self.ColWidths[Colunas[i]] := Self.ColWidths[Colunas[i]] + Tamanho;
  end;

  expanding_cols_local := False;

end;

procedure TGridLuka.ExpandRows(Linhas: array of Integer);
var
  i: Integer;
  lf: Integer;
  TemRow: Boolean;
  Mostrar: Boolean;

  function Tamanho: Integer;
  var
    tam: Integer;
  begin
    Result := 0;

    tam := Self.RowHeights[Linhas[i]];

    if Mostrar then begin
      if tam <> Self.DefaultRowHeight then begin
        if Self.DefaultRowHeight - tam < FIncrementExpandRow then
          Result := Self.DefaultRowHeight - tam
        else
          Result := FIncrementExpandRow;
      end;
    end
    else begin
      if tam > -1 then begin
        if tam < FIncrementExpandRow then
          Result := -(tam + 1)
        else
          Result := -FIncrementExpandRow;
      end;
    end;

    TemRow := TemRow or (Result <> 0);
  end;
begin

  if Length(Linhas) = 0 then
  	Exit;

  if high(Linhas) < 4 then lf := high(Linhas)
  else lf := 4;

  TemRow := true;
  Mostrar := Self.RowHeights[Linhas[0]] < 1;

  for i := 5 to high(Linhas) do begin
  	if Mostrar then
    	Self.RowHeights[Linhas[i]] := DefaultRowHeight
    else
    	Self.RowHeights[Linhas[i]] := -1;
  end;

  while TemRow do begin
    TemRow := False;
    Application.ProcessMessages;

    for i := low(Linhas) to lf do
      Self.RowHeights[Linhas[i]] := Self.RowHeights[Linhas[i]] + Tamanho;
  end;
end;

procedure TGridLuka.SetIncExpansaoCol(x: Integer);
begin
  FIncrementExpandCol := x;
end;

procedure TGridLuka.SetRealColCount(x: Integer);
begin
  if x < 0 then
    FRealColCount := 0
  else
    FRealColCount := x;
end;

function TGridLuka.GetRealColCount: Integer;
begin
  if FRealColCount < ColCount then
    Result := ColCount
  else
    Result := FRealColCount;
end;

procedure TGridLuka.GridQuickSort(L, R: Integer; SCompare: Comparador; Col: Integer; tipo_dados: string);
var
  i: Integer;
  j: Integer;
  c: Integer;
  m: Integer;
begin
  repeat
    i := l;
    j := r;
    m := (l + r) shr 1;
    repeat
      if FUltimaColunaOrdenada = col then begin
        while SCompare(Self, Col, i, m, tipo_dados) > 0 do
          Inc(i);

        while SCompare(Self, Col, j, m, tipo_dados) < 0 do
          Dec(j);
      end
      else begin
        while SCompare(Self, Col, i, m, tipo_dados) < 0 do
          Inc(i);

        while SCompare(Self, Col, j, m, tipo_dados) > 0 do
          Dec(j);
      end;

      if i <= j then begin
        for c := 0 to RealColCount do
          Cols[c].Exchange(i, j);

        if i = m then
          m := j
        else if j = m then
          m := i;

        Inc(i);
        Dec(j);
      end;
    until i > j;

    if l < j then
      GridQuickSort(l, j, SCompare, Col, tipo_dados);

    l := i;
  until i >= r;
end;

procedure TGridLuka.SetOcultarColunas(x: Boolean);
begin
	FOcultarColunas := x;
end;

function TGridLuka.GetOcultarColunas: Boolean;
begin
  Result := FOcultarColunas;
end;

procedure TGridLuka.SetIncExpansaoRow(x: Integer);
begin
	FIncrementExpandRow := x;
end;

procedure TGridLuka.SetLinhasGridPorTamanhoVetor(pTamanho: Integer);
begin
  RowCount := IfThen(pTamanho > 1, pTamanho + 1, 2);
end;

procedure TGridLuka.SetCorColunaFoco(x: TColor);
begin
  FCorColunaFoco := x;
end;

procedure TGridLuka.SetCorFonteColunaFoco(x: TColor);
begin
  FCorFonteColunaFoco := x;
end;

procedure TGridLuka.SetCorFonteLinhaFoco(const Value: TColor);
begin
  fCorFonteLinhaFoco := Value;
end;

procedure TGridLuka.SetCorFundoDesfoque(x: TColor);
begin
  FCorFundoDesfoque := x;
end;

procedure TGridLuka.SetCorFundoFoco(x: TColor);
begin
  FCorFundoFoco := x;
end;

procedure TGridLuka.SetCorInferiorCabecalho(x: TColor);
begin
	FCorInferiorCabecalho := x;
end;

procedure TGridLuka.SetCorLinhaDesfoque(x: TColor);
begin
  FCorLinhaDesfoque := x;
end;

procedure TGridLuka.SetCorLinhaFoco(x: TColor);
begin
  FCorLinhaFoco := x;
end;

procedure TGridLuka.SetCorSeparadorLinhas(x: TColor);
begin
  FCorSeparadorLinhas := x;
end;

procedure TGridLuka.SetCorSuperiorCabecalho(x: TColor);
begin
	FCorSuperiorCabecalho := x;
end;

procedure TGridLuka.SetEditText(ACol, ARow: Integer; const Value: string);
var
  Texto: string;
	max_length: Longint;
begin

  if Assigned(FOnGetMaxLength) then begin
    if Self.InplaceEditor <> nil then begin
      max_length := 0;
      FOnGetMaxLength(Self, ARow, ACol, max_length);
      TEdit(InplaceEditor).MaxLength := max_length;
    end;
  end;

  Texto := Value;
  if not Self.EditorMode or (Self.Cells[ACol, ARow] = Value) then begin
    if Assigned(FOnArrumarGrid) then
      FOnArrumarGrid(Self, ARow, ACol, Texto);
  end;

  inherited SetEditText(ACol, ARow, Texto);
end;

procedure TGridLuka.AjustarData(coluna: Integer; linha: Integer);
var
  Dia: Integer;
  Mes: Integer;
  Ano: Integer;
  Data: string;
begin
  Data := 	Self.Cells[coluna, linha];

  Dia := StrToIntDef(Copy(Data, 1, 2), 0);
  Mes := StrToIntDef(Copy(Data, 4, 2), 0);
  Ano := StrToIntDef(Copy(Data, 7, 4), 0);

  if Dia > 0 then begin
    if Mes = 0 then
      Mes := StrToIntDef(FormatDateTime('MM', Now), 0);

    if Ano = 0 then
      Ano := StrToIntDef(FormatDateTime('YYYY', Now), 0);
  end;

  if Ano < 999 then begin
    if (Ano > 50) and (Ano < 100) then
      Ano := Ano + 1900
    else
      Ano := Ano + 2000;
  end;

  Data := FormatFloat('00', Dia) + '/' + FormatFloat('00', Mes) + '/' + FormatFloat('0000', Ano);

  try
    StrToDate(Data);
    Self.Cells[coluna, linha] := Data;
  except
    Self.Cells[coluna, linha] := '';
  end;
end;


procedure TGridLuka.AutoFit(Coluna: Integer; Valor_Maximo: Integer);
var
	i: Integer;
  j: Integer;
  ci: Integer;
  cf: Integer;
  lt: Integer;
  maior_largura: Integer;
begin

	if Coluna = -1 then begin
  	ci := 0;
    cf := ColCount - 1;
  end
  else begin
  	ci := Coluna;
    cf := Coluna;
  end;

  for i := ci to cf do begin

    maior_largura := 0;
    for j := 0 to RowCount - 1 do begin
    	lt := Canvas.TextWidth(Cells[i, j]);
    	if lt > maior_largura then
      	maior_largura := lt;
    end;

    if maior_largura = 0 then
    	Exit;

    if (Valor_Maximo > 0) and (maior_largura > Valor_Maximo) then
    	maior_largura := Valor_Maximo - 10;

    ColWidths[i] := maior_largura + 10;
  end;

end;

procedure TGridLuka.AddNaPrimeiraLinha(qtde_coluna: Integer);
var
  i: Integer;
begin
  if not Self.IsRowEmpty(1)  then
    Self.RowCount := Self.RowCount + 1
  else
    Exit;

  for i := Self.RowCount - 1 downto 1 do
    TrocaRow( i, i -1, qtde_coluna );
end;

function TGridLuka.ExpandingCols: Boolean;
begin
  Result := expanding_cols_local;
end;

{ TColunaGrid }

constructor TColunaGrid.Create(posicao: Integer);
begin
  Self.posicao := posicao;
end;

end.
