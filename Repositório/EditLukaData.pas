unit EditLukaData;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.StdCtrls, Vcl.Mask, Winapi.Windows, System.DateUtils, Vcl.Graphics;

type
  TTipoData = (tdData, tdDataHora);

  TEditLukaData = class(TMaskEdit)
  private
    FTipoCampo: TTipoData;

    procedure SetTipoCampo(const pValor: TTipoData);
    procedure FormatarData;
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure setData(pData: TDateTime);
    function  getData: TDateTime;
    function  getDataOk: Boolean;
    function  getDataHoraOk: Boolean;
    procedure setDataHora(pData: TDateTime);
    function  getDataHora: TDateTime;
  protected
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure Click; override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property TipoData: TTipoData read FTipoCampo write SetTipoCampo default tdData;
    property DataOk: Boolean read getDataOk;
    property DataHoraOk: Boolean read getDataHoraOk;
    property AsData: TDateTime read getData write setData;
    property AsDataHora: TDateTime read getDataHora write setDataHora;
  end;

procedure Register;

implementation

uses
  _Biblioteca;

procedure Register;
begin
  RegisterComponents('LD Sistemas', [TEditLukaData]);
end;

{ TEditLukaData }

procedure TEditLukaData.Click;
begin
  inherited;
  if not Self.DataOk then
    Self.SelStart := 0;
end;

procedure TEditLukaData.CMEnter(var Message: TCMEnter);
begin
  if Self.Color = clWindow then
    Self.Color := $0080FFFF;
end;

procedure TEditLukaData.CMExit(var Message: TCMExit);
begin
  inherited;

  FormatarData;

  if Self.Color = $0080FFFF then
    Self.Color := clWindow;
end;

constructor TEditLukaData.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  Hint := 'Pressione "A" para definir o dia atual, "I" para o primeiro dia do m�s atual e "F" para o �ltimo dia do m�s atual.';
end;

function TEditLukaData.getDataOk: Boolean;
begin
  Result := _Biblioteca.DataOk(Text);
end;

procedure TEditLukaData.FormatarData;
var
  vDia: Integer;
  vMes: Integer;
  vAno: Integer;
  vHora: Integer;
  vMinutos: Integer;
  vSegundos: Integer;


  vData: string;
begin
  vData := Text;

  vDia := StrToIntDef(Copy(vData, 1, 2), 0);
  vMes := StrToIntDef(Copy(vData, 4, 2), 0);
  vAno := StrToIntDef(Copy(vData, 7, 4), 0);

  vHora := StrToIntDef(Copy(vData, 12, 2), -1);
  vMinutos := StrToIntDef(Copy(vData, 15, 2), -1);
  vSegundos := StrToIntDef(Copy(vData, 18, 2), -1);

  if vDia > 0 then begin
    if vMes = 0 then
      vMes := StrToIntDef(FormatDateTime('MM', Now), 0);

    if vAno = 0 then
      vAno := StrToIntDef(FormatDateTime('YYYY', Now), 0);
  end;

  if vAno < 999 then begin
    if (vAno > 50) and (vAno < 100) then
      vAno := vAno + 1900
    else
      vAno := vAno + 2000;
  end;

  if (vHora = -1) or (vHora > 24)  then begin
    vHora := StrToIntDef(FormatDateTime('HH', Now), 0);

    if (vMinutos = -1) or (vMinutos > 60)  then
      vMinutos := StrToIntDef(FormatDateTime('MI', Now), 0);

    if (vSegundos = -1) or (vSegundos > 60) then
      vSegundos := StrToIntDef(FormatDateTime('SS', Now), 0);
  end
  else begin
    if (vMinutos = -1) or (vMinutos > 60)  then
      vMinutos := StrToIntDef(FormatDateTime('MI', Now), 0);

    if (vSegundos = -1) or (vSegundos > 60) then
      vSegundos := StrToIntDef(FormatDateTime('SS', Now), 0);
  end;

  vData := FormatFloat('00', vDia) + '/' + FormatFloat('00', vMes) + '/' + FormatFloat('0000', vAno);

  if TipoData = tdDataHora then
    vData := vData + ' ' + FormatFloat('00', vHora) + ':' + FormatFloat('00', vMinutos) + ':' + FormatFloat('00', vSegundos);

  try
    if TipoData = tdData then
      StrToDate(vData)
    else
      StrToDateTime(vData);

    Text := vData;
  except
    Text := '';
  end;
end;

function TEditLukaData.getData: TDateTime;
begin
  if getDataOk then
    Result := StrToDate(Text)
  else
    Result := 0;
end;

function TEditLukaData.getDataHora: TDateTime;
begin
  if getDataHoraOk then
    Result := StrToDateTime(Text)
  else
    Result := 0;
end;

function TEditLukaData.getDataHoraOk: Boolean;
begin
  Result := _Biblioteca.DataHoraOk(Text);
end;

procedure TEditLukaData.KeyDown(var Key: Word; Shift: TShiftState);
begin
  if ReadOnly then
    Exit;

  if Key = VK_RETURN then
    FormatarData
  else if Key = Ord('I') then
    Text := FormatDateTime('01/mm/yyyy' + IIfStr(TipoData = tdDataHora, ' 12:00:00'), Now)
  else if Key = Ord('A') then
    Text := FormatDateTime('dd/mm/yyyy' + IIfStr(TipoData = tdDataHora, ' hh:mm:ss'), Now)
  else if Key = Ord('F') then
    Text := FormatDateTime('dd/mm/yyyy' + IIfStr(TipoData = tdDataHora, ' 12:00:00'), EndOfTheMonth(Now));

  inherited KeyDown(Key, Shift);
end;

procedure TEditLukaData.setData(pData: TDateTime);
begin
  if YearOf(pData) = 1899 then
    Text := ''
  else
    Text := FormatDateTime('dd/mm/yyyy', pData);
end;

procedure TEditLukaData.setDataHora(pData: TDateTime);
begin
  if YearOf(pData) = 1899 then
    Text := ''
  else
    Text := FormatDateTime('dd/mm/yyyy hh:mm:ss', pData);
end;

procedure TEditLukaData.SetTipoCampo(const pValor: TTipoData);
begin
  if FTipoCampo = pValor then
    Exit;

  FTipoCampo := pValor;
  if pValor = tdData then
    EditMask := '99/99/9999;1;_'
  else
    EditMask := '99/99/9999 99:99:99;1;_';
end;

end.
