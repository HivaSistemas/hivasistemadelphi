unit _Sessao;

interface

uses
  System.SysUtils, Vcl.Forms, Winapi.Windows, System.IniFiles, System.StrUtils, System.Types, System.Classes,
  _Conexao, _OperacoesBancoDados, _Biblioteca, _RecordsEspeciais, _RecordsCadastros, _Criptografia, _RecordsCaixa,
  Login, _Funcionarios, AlterarSenhaUsuario {$IFDEF SIGO_VAREJO}, EmpresasLogin {$ENDIF}, _Parametros,
  _Autorizacoes, _ParametrosEmpresa, _TurnosCaixas, _Empresas, _Logs, _CalculosSistema, ComboBoxLuka, _Produtos,
  _SeriesNotasFiscais, System.DateUtils, _ImpressorasUsuarios, _ImpressorasEstacoes, Vcl.Graphics, _ParametrosEstacoes,
  Vcl.ExtCtrls;

type
  TSistema = (tsSigoVarejo, tsSigoPDV);

  TOpcoesImpressoras = (
    toNFe,
    toNFCe,
    toComprovantePagamento,
    toComprovanteEntrega,
    toComprovanteDevolucao,
    toComprovantePagtoFinanceiro,
    toListaSeparacao,
    toOrcamento,
    toCartazPromocional,
    toComprovanteCaixa
  );

  RecImpressora = record
    CaminhoImpressora: string;
    TipoImpressora: string;
    AbrirPreview: Boolean;

    function getIndex: Integer;
    function validaImpresssora: Boolean;
  end;

  RecParametrosEstacao = record
    Server: string;
    CaminhoXMLs: string;

    VersaoSistema: string;
    VersaoBandoDados: string;
    VersaoExecutavel: string;
  end;

  {$M+}
  TSessao = class(TObject)
  private
    FSistema: TSistema;
    FVersaoSistema: string;
    FCaminhoExeSistema: string;
    FEmpresas: TArray<RecEmpresas>;

    FTimer: TTimer;
    FFinalizandoPorTempoInativo: Boolean;

    FConexaoBanco: TConexao;
    FCriptografia: TCriptografia;
    FTurnoCaixaAberto: RecTurnosCaixas;
    FEmpresaLogadaSistema: RecEmpresas;
    FUsuarioLogadoSistema: RecFuncionarios;
    FParametrosEstacao: RecParametrosEstacao;
    FAutorizacoesTelas: TArray<RecAutorizacoes>;
    FAutorizacoesRotinas: TArray<RecAutorizacoes>;
    FParametrosEstacoes: RecParametrosEstacoes;

    FLogs: TLogs;
    FParametros: RecParametros;
    FParametrosEmpresaLogada: RecParametrosEmpresa;

    FImpresssorasUsuario: TArray<_ImpressorasUsuarios.RecImpressorasUsuarios>;
    FImpresssorasEstacao: TArray<_ImpressorasEstacoes.RecImpressorasEstacoes>;
    FVersao: String;
  public
    procedure Iniciar;
    procedure Finalizar;

    procedure AtualizarTurnoCaixaAberto;
    procedure AbrirTela(pClasse: TFormClass; pSomenteUsuarioAltis: Boolean = False; pQuantidadeTelas: Integer = 1);
    procedure AbortarSeHouveErro(vRetorno: RecRetornoBD);

    function getVersaoSistema: string;
    function getVersaoSistemaResumido: string;
    function getVersaoSistemaInt: Integer;
    function getConexaoBanco: TConexao;
    function AtualizarParametros: RecRetornoBD;
    function AtualizarParametrosEmpresasBanco: RecRetornoBD;

    function getParametros: RecParametros;
    function getEmpresaLogada: RecEmpresas;
    function getEmpresas: TArray<RecEmpresas>;
    function getEmpresa(pCNPJ: string): RecEmpresas; overload;
    function getEmpresa(pEmpresaId: Integer): RecEmpresas; overload;
    function getCriptografia: TCriptografia;
    function getUsuarioLogado: RecFuncionarios;
    function getTurnoCaixaAberto: RecTurnosCaixas;
    function getParametrosEmpresa: RecParametrosEmpresa;
    function getParametrosEstacao: RecParametrosEstacoes;
    function getParametrosEstacao2: RecParametrosEstacao;

    function AutorizadoRotina( pAutorizacao: string ): Boolean; overload;
    function AutorizadoRotina( pAutorizacao: string; pUsuarioId: Integer ): Boolean; overload;
    function AutorizadoTela(pAutorizacao: string): Boolean;

    function getData: TDateTime;
    function getDataHora: TDateTime;
    function getNextSeq(const pNomeSequence: string): Integer;

    function getValorColuna(const pColuna: string; const pTabela: string; const pWhere: string; const pParametros: array of Variant): Integer;
    function getValorColunaStr(const pColuna: string; const pTabela: string; const pWhere: string; const pParametros: array of Variant): string;

    function getCalculosSistema: RecCalculosSistema;
    function getImpressora( pOpcoesImpressora: TOpcoesImpressoras ): RecImpressora;

    procedure SetComboBoxPisCofins(pComboBox: TComboBoxLuka);
    procedure SetComboBoxTipoEntregas(pComboBox: TComboBoxLuka);
    procedure SetComboBoxSeriesNotas(pComboBox: TComboBoxLuka);

    function getCorLucro(pPercentualLucro: Double): Integer;
    function getCaminhoPastaPreEntradas: string;
    function getCaminhoPastaTemporaria: string;
    function getCaminhoPastaSistema: string;

    procedure setTimerSistemaOcioso(pTimer: TTimer);
    procedure setTimerSistemaOciosoHouveMovimento;

    function TipoCobrancaNaoPermiteAdiantamento(pCobrancaId: Integer): Boolean;
    function TipoCobrancaNaoPermiteAlteracaoValor(pCobrancaId: Integer): Boolean;
    function EProdutoKit(pTipoControleEstoque: string): Boolean;
  published
    property Logs: TLogs read FLogs write FLogs;
    property FinalizandoPorTempoInativo: Boolean read FFinalizandoPorTempoInativo write FFinalizandoPorTempoInativo default False;
  end;

var
  Sessao: TSessao;

implementation

{ TSessao }

uses
  _ExecutaComandoUpdateDB, _Mensagens;

procedure TSessao.Iniciar;
var
bolDireto :Boolean;

  procedure IniciarParametrosEstacao2;
  var
    vIni: TIniFile;
  begin
    FLogs.AddLog('Iniciando os par�metros da esta��o.');

    FCaminhoExeSistema := IncludeTrailingPathDelimiter( ExtractFileDir(Application.ExeName) );
    if not FileExists(FCaminhoExeSistema + 'sigo.ini') then begin
      Exclamar('Arquivo de inicializa��o "sigo.ini" n�o encontrado!');
      Halt;
    end;

    vIni := TIniFile.Create(FCaminhoExeSistema + 'sigo.ini');

    FParametrosEstacao.Server                 := vIni.ReadString('SERVIDOR', 'server', '');
    FParametrosEstacao.CaminhoXMLs            := IncludeTrailingPathDelimiter(FCaminhoExeSistema + 'NFe');

    vIni.Free;
  end;

  procedure IniciarConexaoBanco;
  const
    coUsuario = 0;
    coSenha   = 1;
    coServer  = 2;
    coPorta   = 3;
    coServico = 4;
    coDireto  = 5;

  var
    vServidor: string;
    vAuxiliar: TStringDynArray;
  begin
    FLogs.AddLog('Iniciando a conex�o com o banco de dados.');

    FCriptografia := TCriptografia.Create(Application);
    vServidor := FCriptografia.Descriptografar(FParametrosEstacao.Server, False);

    vAuxiliar := SplitString(vServidor, '|');

    try
      FConexaoBanco := _Conexao.TConexao.Create(Application);
      try
        bolDireto :=  vAuxiliar[coDireto] = 'S';
      except
        bolDireto := False;
      end;

      FConexaoBanco.Conectar(
        vAuxiliar[coUsuario],
        vAuxiliar[coSenha],
        vAuxiliar[coServer],
        StrToInt(vAuxiliar[coPorta]),
        vAuxiliar[coServico],
        bolDireto
      );
    except
      on e: Exception do begin
        FLogs.AddLog('Falha ao instanciar objeto de conex�o com o oracle.');
        FLogs.AddLog(e.Message);
        Exclamar('Falha ao instanciar objeto de conex�o com o oracle!' + Chr(13) + Chr(10) + e.Message);
        Halt;
      end;
    end;
  end;

  procedure IniciarParametros;
  begin
    FLogs.AddLog('Buscando os par�metros.');

    try
      FParametros              := _Parametros.BuscarParametros(FConexaoBanco);
    except
      on e: Exception do begin
        Exclamar('Falha ao iniciar a sess�o!' + chr(13) + chr(10) + e.Message);
        Halt;
      end;
    end;
  end;

  procedure UsuarioLogin;
  var
    vFuncionarioId: Integer;
  begin
    FLogs.AddLog('Iniciando o login do usu�rio.');

    vFuncionarioId := Login.Logar(0, FEmpresaLogadaSistema);
    if vFuncionarioId = 0 then
      Halt;

    FUsuarioLogadoSistema := _Funcionarios.BuscarFuncionarios(FConexaoBanco, 0, [vFuncionarioId], True, False, False, False, False)[0];
    if FUsuarioLogadoSistema.senha_sistema = FCriptografia.getCriptografiaSenhaPadrao then begin
      Informar('A senha que acabou de ser digitada � a senha padr�o, a senha dever� ser alterada.');
      if AlterarSenhaUsuario.AtualizarSenha(False).RetTela = trCancelado then
        Halt;
    end
    else if DaysBetween(FUsuarioLogadoSistema.DataUltimaSenha, Sessao.getData) > FParametros.QuantidadeDiasValidadeSenha then begin
      Informar('A senha que acabou de ser digitada est� vencida, a senha dever� ser alterada.');
      if AlterarSenhaUsuario.AtualizarSenha(False).RetTela = trCancelado then
        Halt;
    end;
  end;

  procedure EmpresaLogin;
  begin
    FLogs.AddLog('Iniciando o de empresas.');

    {$IFDEF SIGO_VAREJO}
      FEmpresaLogadaSistema := EmpresasLogin.BuscarEmpresaLogin;
      if FEmpresaLogadaSistema = nil then begin
        Exclamar('Falha ao buscar as empresas cadastradas!');
        Halt;
      end;
    {$ELSE}
      FEmpresaLogadaSistema := _Empresas.BuscarEmpresas(FConexaoBanco, 0, [1])[0];
    {$ENDIF}
  end;

  procedure IniciarSessao;
  var
    vParametrosEstacoes: TArray<RecParametrosEstacoes>;
  begin
    FLogs.AddLog('Parametrizando a sess�o.');

    try
      FLogs.AddLog('Buscando par�metros');
      FParametros              := _Parametros.BuscarParametros(FConexaoBanco);

      FLogs.AddLog('Buscando autoriza��es telas');
      FAutorizacoesTelas       := _Autorizacoes.BuscarAutorizacoes(FConexaoBanco, 0, [FUsuarioLogadoSistema.funcionario_id, 'T']);

      FLogs.AddLog('Buscando autoriza��es rotinas');
      FAutorizacoesRotinas     := _Autorizacoes.BuscarAutorizacoes(FConexaoBanco, 0, [FUsuarioLogadoSistema.funcionario_id, 'R']);

      FLogs.AddLog('Buscando par�metros empresa logada');
      FParametrosEmpresaLogada := _ParametrosEmpresa.BuscarParametrosEmpresas(FConexaoBanco, 0, [FEmpresaLogadaSistema.EmpresaId])[0];

      FLogs.AddLog('Buscando empresas');
      FEmpresas                := _Empresas.BuscarEmpresas(FConexaoBanco, 4, []);

      FLogs.AddLog('Buscando os parametros da esta��o');
      vParametrosEstacoes := _ParametrosEstacoes.BuscarParametros(FConexaoBanco, 0, [NomeComputador]);
      if vParametrosEstacoes <> nil then
        FParametrosEstacoes := vParametrosEstacoes[0];

      FConexaoBanco.IniciarSessao(FEmpresaLogadaSistema.EmpresaId, FUsuarioLogadoSistema.funcionario_id, _Biblioteca.NomeComputador);
    except
      on e: Exception do begin
        Exclamar('Falha ao iniciar a sess�o!' + chr(13) + chr(10) + e.Message);
        Halt;
      end;
    end;
  end;

  procedure BuscarVersaoSistema;
  type
    PFFI = ^vs_FixedFileInfo;
  var
    vCaminhoAplicacao: String;
    F: PFFI;
    Handle: DWORD;
    Len: LongInt;
    Data: PChar;
    Buffer: Pointer;
    vTamanho: DWORD;
    vAplicacao: PChar;
  begin
    FLogs.AddLog('Buscando a vers�o do sistema.');

    vCaminhoAplicacao := Application.ExeName;
    vAplicacao := StrAlloc(Length(vCaminhoAplicacao) + 1);
    StrPcopy(vAplicacao, vCaminhoAplicacao);
    Len := GetFileVersionInfoSize(vAplicacao, Handle);
    FVersaoSistema := '';
    if Len > 0 then begin
      Data := StrAlloc(Len + 1);
      if GetFileVersionInfo(vAplicacao, Handle, Len, Data) then begin
        VerQueryValue(Data, '\', Buffer, vTamanho);
        F := PFFI(Buffer);
        FVersaoSistema :=
          Format(
            //'%d.%d.%d.%d',   //pegar versao do delph
            _Versao, // - MUDAR A COMPILACAO MANUAL e aqui que ajusta a versao
            [
              HiWord(F^.dwFileVersionMs),
              LoWord(F^.dwFileVersionMs),
              HiWord(F^.dwFileVersionLs),
              Loword(F^.dwFileVersionLs)
            ]
          );
      end;
      StrDispose(Data);
    end;
    StrDispose(vAplicacao);
  end;

  procedure ValidarVersaoSistema;
  var
    vRetBanco: RecRetornoBD;
    vCompilacaoBanco: string;
    vCompilacaoExecutavel: string;
    vCompilacaoBancoTercNivel: string;
    vCompilacaoExecutavelTercNivel: string;

    function StringDeTrasParaFrente(pString: string): string;
    var
      i: Integer;
      vString: TStringList;
    begin
      Result := '';
      vString := TStringList.Create;

      vString.Delimiter := '.';
      vString.StrictDelimiter := True;
      vString.DelimitedText := pString;

      for i := vString.Count -1 downto 0 do
        Result := Result + vString[i] + '.';

      Delete(Result, Length(Result), 1);

      vString.Free;
    end;

    function StringAteTerceiroPonto(pString: string): string;
    var
      inputString, outputString: string;
      dotCount, i: Integer;
    begin
      inputString := pString;
      dotCount := 0;
      outputString := '';

      for i := 1 to Length(inputString) do
      begin
        if inputString[i] = '.' then
          Inc(dotCount);

        if dotCount < 3 then
          outputString := outputString + inputString[i]
        else
          Break;
      end;

      Result := outputString;
    end;

  begin
    vCompilacaoExecutavel := FVersaoSistema;
    vCompilacaoBanco      := _Parametros.getCompilacaoSistemaBanco(getConexaoBanco);

    if (vCompilacaoBanco <> '') and (Copy(vCompilacaoExecutavel, 1, 2) <> Copy(vCompilacaoBanco, 1, 2)) then begin
      Exclamar(
        'A vers�o do execut�vel esta desatualizada!' + chr(13) +
        'Vers�o atual: ' + vCompilacaoBanco + Chr(13) +
        'Vers�o exe..: ' + vCompilacaoExecutavel
      );
      Halt;
    end;

    vCompilacaoBancoTercNivel := StringAteTerceiroPonto(vCompilacaoBanco);
    vCompilacaoExecutavelTercNivel := StringAteTerceiroPonto(vCompilacaoExecutavel);

    if
      (SFormatInt(StringDeTrasParaFrente(vCompilacaoExecutavelTercNivel)) < SFormatInt(StringDeTrasParaFrente(vCompilacaoBancoTercNivel)))
    then begin
      Exclamar(
        'A compila��o do execut�vel esta desatualizada!' + chr(13) +
        'Vers�o atual: ' + vCompilacaoBanco + Chr(13) +
        'Vers�o exe..: ' + vCompilacaoExecutavel
      );
      Halt;
    end;

    if
      (vCompilacaoExecutavel < vCompilacaoBanco) and
      (SFormatInt(StringDeTrasParaFrente(vCompilacaoExecutavel)) < SFormatInt(StringDeTrasParaFrente(vCompilacaoBanco)))
    then begin
      Exclamar(
        'A compila��o do execut�vel esta desatualizada!' + chr(13) +
        'Vers�o atual: ' + vCompilacaoBanco + Chr(13) +
        'Vers�o exe..: ' + vCompilacaoExecutavel
      );
      Halt;
    end
    else begin
      vRetBanco := _Parametros.atualizarCompilacaoSistemaBanco(getConexaoBanco, vCompilacaoExecutavel);
      if vRetBanco.TeveErro then begin
        Exclamar(vRetBanco.MensagemErro);
        Halt;
      end;
    end;
  end;

begin
  {$IFDEF SIGO_VAREJO}
    FSistema := tsSigoVarejo;
    FLogs := TLogs.Create(nil, ExtractFileDir(Application.ExeName), 'sigov');
  {$ELSE}
    FSistema := tsSigoPDV;
    FLogs := TLogs.Create(nil, ExtractFileDir(Application.ExeName), 'sigop');
  {$ENDIF}

  if StrToDate(_Biblioteca.coDataValidadeVersao) < Now then begin
    _Biblioteca.Exclamar('Validade da vers�o expirada!');
    Halt;
  end;

  FLogs.AddLog('Iniciando o sistema.');

  BuscarVersaoSistema;
  IniciarParametrosEstacao2;
  IniciarConexaoBanco;
  ValidarVersaoSistema;

  // Tempor�rio at� o usu�rio logar completamente no PC
  FConexaoBanco.IniciarSessao(1, 1, _Biblioteca.NomeComputador);

  _ExecutaComandoUpdateDB.ExecutarScripts;

  IniciarParametros;
  UsuarioLogin;
//  EmpresaLogin;
  IniciarSessao;
  AtualizarTurnoCaixaAberto;

  Application.Title := ' ' + getVersaoSistema;
end;

procedure TSessao.SetComboBoxPisCofins(pComboBox: TComboBoxLuka);
var
  i: Integer;
  vCst: TArray<RecCstPisCofins>;
begin
  pComboBox.Items.Clear;
  pComboBox.Valores.Clear;

  vCst := _Produtos.BuscarCSTsPisCofins(FConexaoBanco);
  for i := Low(vCst) to High(vCst) do begin
    pComboBox.Items.Add(vCst[i].Cst + ' - ' + vCst[i].Descricao);
    pComboBox.Valores.Add(vCst[i].Cst);
  end;
end;

procedure TSessao.SetComboBoxTipoEntregas(pComboBox: TComboBoxLuka);

  procedure Add(pUtiliza: string; pDescricao: string; pValor: string);
  begin
    if pUtiliza = 'N' then
      Exit;

    pComboBox.Items.Add(pDescricao);
    pComboBox.Valores.Add(pValor);
  end;

begin
  pComboBox.Items.Clear;
  pComboBox.Valores.Clear;

  Add(FParametrosEmpresaLogada.TrabalhaRetirarAto, 'Retirar no ato', 'RA');
  Add(FParametrosEmpresaLogada.TrabalhaRetirar, 'A retirar', 'RE');
  Add(FParametrosEmpresaLogada.TrabalhaEntregar, 'A entregar', 'EN');
  Add(FParametrosEmpresaLogada.TrabalhaSemPrevisao, 'Sem previs�o', 'SP');
  Add(FParametrosEmpresaLogada.TrabalhaSemPrevisaoEntregar, 'Sem previs�o - Entregar', 'SE');
end;

procedure TSessao.setTimerSistemaOcioso(pTimer: TTimer);
begin
  FTimer := pTimer;
  FTimer.Interval := getParametros.QtdeSegTravarAltisOcioso * 1000;
end;

procedure TSessao.setTimerSistemaOciosoHouveMovimento;
begin
  if FTimer = nil then
    Exit;

  FTimer.Enabled := False;
end;

function TSessao.TipoCobrancaNaoPermiteAdiantamento(pCobrancaId: Integer): Boolean;
begin
  Result :=
    Em(pCobrancaId, [
      Sessao.getParametros.TipoCobRecebEntregaId,
      Sessao.getParametros.TipoCobAcumulativoId,
      Sessao.getParametros.TipoCobAdiantamentoAcuId,
      Sessao.getParametros.TipoCobAdiantamentoFinId
    ]);
end;

function TSessao.TipoCobrancaNaoPermiteAlteracaoValor(pCobrancaId: Integer): Boolean;
begin
  Result :=
    Em(pCobrancaId, [
      Sessao.getParametros.TipoCobrancaGeracaoCredId,
      Sessao.getParametros.TipoCobAdiantamentoAcuId,
      Sessao.getParametros.TipoCobAdiantamentoFinId
    ]);
end;

procedure TSessao.SetComboBoxSeriesNotas(pComboBox: TComboBoxLuka);
var
  i: Integer;
  vSeries: TArray<string>;
begin
  pComboBox.Items.Clear;
  pComboBox.Valores.Clear;

  vSeries := _SeriesNotasFiscais.BuscarSeriesNotas(FConexaoBanco, 0, []);
  for i := Low(vSeries) to High(vSeries) do begin
    pComboBox.Items.Add(vSeries[i]);
    pComboBox.Valores.Add(vSeries[i]);
  end;
end;

procedure TSessao.Finalizar;
begin
  if Screen.FormCount > 3 then begin
    Exclamar('Feche todas as janelas antes de finalizar a sess�o!');
    Abort;
  end;

  FreeAndNil(FParametrosEmpresaLogada);
  FreeAndNil(FEmpresaLogadaSistema);
  FreeAndNil(FUsuarioLogadoSistema);

  if FTurnoCaixaAberto <> nil then
    FreeAndNil(FTurnoCaixaAberto);

  FreeAndNil(FConexaoBanco);
  FCriptografia.Free;
  FLogs.Free;
end;

procedure TSessao.AbrirTela(pClasse: TFormClass; pSomenteUsuarioAltis: Boolean = False; pQuantidadeTelas: Integer = 1);
var
  i: Integer;
  vForm: TForm;
  vQtdeTelas: Integer;
  vAutorizado: Boolean;
begin
  vQtdeTelas := 0;

  if pSomenteUsuarioAltis and (FUsuarioLogadoSistema.funcionario_id <> 1) then begin
    Exclamar('Somente o usu�rio Hiva pode acessar esta tela!');
    Exit;
  end;

  if
    (LowerCase(pClasse.ClassName) <> 'tformcentralmensagensinternas') and
    (FUsuarioLogadoSistema.funcionario_id <> 1) and
    (FUsuarioLogadoSistema.CargoId <> coGerenteId)
  then begin
    vAutorizado := False;
    for i := Low(FAutorizacoesTelas) to High(FAutorizacoesTelas) do begin
      if (FAutorizacoesTelas[i].tipo <> 'T') or (LowerCase(FAutorizacoesTelas[i].id) <> LowerCase(pClasse.ClassName)) then
        Continue;

      vAutorizado := True;
      Break;
    end;

    if not vAutorizado then begin
      Exclamar('Voc� n�o possui permiss�es necess�rias para acessar esta tela!');
      Exit;
    end;
  end;

  vForm := nil;
  for i := 0 to Screen.FormCount - 1 do begin
    if Screen.Forms[i].ClassName = pClasse.ClassName then begin
      vForm := Screen.Forms[i];
      Inc(vQtdeTelas, 1);

      // Se n�o for para abrir v�rias telas j� quebrar o la�o
      if pQuantidadeTelas = 1 then
        Break;
    end;
  end;

  if LowerCase(pClasse.ClassName) <> 'tformcentralmensagensinternas' then begin
    if _Mensagens.ExisteMensagensNaoLidas(getConexaoBanco, FUsuarioLogadoSistema.funcionario_id) then begin
      Exclamar('Existem mensagens n�o lidas, verifique suas mensagens antes de continuar!');
      Exit;
    end;
  end;

  if ( vForm = nil ) or ( vQtdeTelas < pQuantidadeTelas ) then
    vForm := pClasse.Create(Application);

  vForm.Show;
end;

procedure TSessao.AbortarSeHouveErro(vRetorno: RecRetornoBD);
begin
  if vRetorno.TeveErro then begin
    if FConexaoBanco.InTransaction then
      FConexaoBanco.VoltarTransacao;

    Exclamar(vRetorno.MensagemErro);
    Abort;
  end;
end;

procedure TSessao.AtualizarTurnoCaixaAberto;
var
  vTurnos: TArray<RecTurnosCaixas>;
begin
  FLogs.AddLog('Iniciando turno.');

  if FTurnoCaixaAberto <> nil then
    FreeAndNil(FTurnoCaixaAberto);

  vTurnos := _TurnosCaixas.BuscarTurnosCaixas( FConexaoBanco, 2, [FUsuarioLogadoSistema.funcionario_id, FEmpresaLogadaSistema.EmpresaId] );
  if vTurnos = nil then
    Exit;

  FTurnoCaixaAberto := vTurnos[0];
end;

function TSessao.AutorizadoRotina(pAutorizacao: string): Boolean;
var
  i: Integer;
begin
  if getUsuarioLogado.CargoId = 1 then begin
    Result := True;
    Exit;
  end;

  Result := False;
  for i := Low(FAutorizacoesRotinas) to High(FAutorizacoesRotinas) do begin
    if FAutorizacoesRotinas[i].id <> pAutorizacao then
      Continue;

    Result := True;
    Break;
  end;
end;


function TSessao.AutorizadoRotina(pAutorizacao: string; pUsuarioId: Integer): Boolean;
var
  FAutorizacoesRotinasUsuario: TArray<RecAutorizacoes>;
  i: Integer;
begin
  if pUsuarioId = 1 then begin
    Result := True;
    Exit;
  end;

  Result := False;
  FAutorizacoesRotinasUsuario     := _Autorizacoes.BuscarAutorizacoes(FConexaoBanco, 0, [pUsuarioId, 'R']);

  if FAutorizacoesRotinasUsuario = nil then
    Exit;

  for i := Low(FAutorizacoesRotinasUsuario) to High(FAutorizacoesRotinasUsuario) do begin
    if FAutorizacoesRotinasUsuario[i].id <> pAutorizacao then
      Continue;

    Result := True;
    Break;
  end;
end;

function TSessao.AutorizadoTela(pAutorizacao: string): Boolean;
var
  i: Integer;
begin
  if getUsuarioLogado.CargoId = 1 then begin
    Result := True;
    Exit;
  end;

  Result := False;
  for i := Low(FAutorizacoesTelas) to High(FAutorizacoesTelas) do begin
    if FAutorizacoesTelas[i].id <> pAutorizacao then
      Continue;

    Result := True;
    Break;
  end;
end;

function TSessao.EProdutoKit(pTipoControleEstoque: string): Boolean;
begin
Result := Em(pTipoControleEstoque, ['K', 'A']);
end;

function TSessao.AtualizarParametros: RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.TeveErro := False;
  vExec := TExecucao.Create(FConexaoBanco);
  try
    vExec.SQL.Add('begin ');
    vExec.SQL.Add('  select * into SESSAO.PARAMETROS_GERAIS from PARAMETROS; ');
    vExec.SQL.Add('end; ');
    vExec.ExecSQL;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vExec.Free;
  FParametros := _Parametros.BuscarParametros(FConexaoBanco);
end;

function TSessao.AtualizarParametrosEmpresasBanco: RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.TeveErro := False;
  vExec := TExecucao.Create(FConexaoBanco);
  try
    vExec.SQL.Add('begin ');
    vExec.SQL.Add('  select * into SESSAO.PARAMETROS_EMPRESA_LOGADA from PARAMETROS_EMPRESA where EMPRESA_ID = :P1; ');
    vExec.SQL.Add('end; ');
    vExec.Executar([FEmpresaLogadaSistema.EmpresaId]);
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  vExec.Free;
  FParametrosEmpresaLogada := _ParametrosEmpresa.BuscarParametrosEmpresas(FConexaoBanco, 0, [FEmpresaLogadaSistema.EmpresaId])[0];
end;

function TSessao.getVersaoSistema: string;
begin
  {$IFDEF SIGO_VAREJO}
    Result := 'Hiva' + FVersaoSistema;
  {$ELSE}
    Result := 'Hiva ' + FVersaoSistema;
  {$ENDIF}
end;

function TSessao.getVersaoSistemaResumido: string;
begin
  Result := Copy(FVersaoSistema, 1, Pos('.', FVersaoSistema) -1);
end;

function TSessao.getVersaoSistemaInt: Integer;
begin
  Result := StrToInt(_Biblioteca.RetornaNumeros(FVersaoSistema));
end;

function TSessao.getData: TDateTime;
var
  vPesq: TConsulta;
begin
  vPesq := TConsulta.Create(FConexaoBanco);
  vPesq.Add('select trunc(sysdate) from dual');
  vPesq.Pesquisar;

  Result := vPesq.Fields[0].AsDateTime;

  vPesq.Free;
end;

function TSessao.getDataHora: TDateTime;
var
  vPesq: TConsulta;
begin
  vPesq := TConsulta.Create(FConexaoBanco);
  vPesq.Add('select sysdate from dual');
  vPesq.Pesquisar;

  Result := vPesq.Fields[0].AsDateTime;

  vPesq.Free;
end;

function TSessao.getNextSeq(const pNomeSequence: string): Integer;
var
  vSeq: TSequencia;
begin
  vSeq := TSequencia.Create(FConexaoBanco, pNomeSequence);
  Result := vSeq.GetProximaSequencia;
  vSeq.Free;
end;

function TSessao.getValorColuna(const pColuna: string; const pTabela: string; const pWhere: string; const pParametros: array of Variant): Integer;
var
  vSql: TConsulta;
begin
  Result := 0;
  vSql := TConsulta.Create(FConexaoBanco);

  vSql.SQL.Add('select ');
  vSql.SQL.Add(  pColuna);
  vSql.SQL.Add('from ');
  vSql.SQL.Add(  pTabela);
  vSql.SQL.Add('where ');
  vSql.SQL.Add(  pWhere);

  if vSql.Pesquisar(pParametros) then
    Result := vSql.GetInt(0);

  vSql.Active := False;
  vSql.Free;
end;

function TSessao.getValorColunaStr(const pColuna: string; const pTabela: string; const pWhere: string; const pParametros: array of Variant): string;
var
  vSql: TConsulta;
begin
  Result := '';
  vSql := TConsulta.Create(FConexaoBanco);

  vSql.SQL.Add('select ');
  vSql.SQL.Add(  pColuna);
  vSql.SQL.Add('from ');
  vSql.SQL.Add(  pTabela);
  vSql.SQL.Add('where ');
  vSql.SQL.Add(  pWhere);

  if vSql.Pesquisar(pParametros) then
    Result := vSql.GetString(0);

  vSql.Active := False;
  vSql.Free;
end;

function TSessao.getCalculosSistema: RecCalculosSistema;
var
  vCalculos: RecCalculosSistema;
begin
  Result := vCalculos;
end;

function TSessao.getCaminhoPastaPreEntradas: string;
begin
  if not DirectoryExists(FCaminhoExeSistema + 'Sistema') then
    MkDir(FCaminhoExeSistema + 'Sistema');

  if not DirectoryExists(FCaminhoExeSistema + 'Sistema\PreEntradas') then
    MkDir(FCaminhoExeSistema + 'Sistema\PreEntradas');

  Result := FCaminhoExeSistema + 'Sistema\PreEntradas';
end;

function TSessao.getCaminhoPastaSistema: string;
begin
  Result := FCaminhoExeSistema;
end;

function TSessao.getCaminhoPastaTemporaria: string;
begin
  if not DirectoryExists(FCaminhoExeSistema + 'Sistema\Temp') then
    MkDir(FCaminhoExeSistema + 'Sistema\Temp');

  Result := FCaminhoExeSistema + 'Sistema\Temp';
end;

function TSessao.getConexaoBanco: TConexao;
begin
  Result := FConexaoBanco;
end;

function TSessao.getCorLucro(pPercentualLucro: Double): Integer;
begin
  if pPercentualLucro > 60 then
    Result := Sessao.getParametrosEmpresa.CorLucratividade1
  else if pPercentualLucro > 30 then
    Result := Sessao.getParametrosEmpresa.CorLucratividade2
  else if pPercentualLucro > 10 then
    Result := Sessao.getParametrosEmpresa.CorLucratividade3
  else if pPercentualLucro > 0 then
    Result := Sessao.getParametrosEmpresa.CorLucratividade4
  else
    Result := clRed;
end;

function TSessao.getCriptografia: TCriptografia;
begin
  Result := FCriptografia;
end;

function TSessao.getEmpresa(pCNPJ: string): RecEmpresas;
var
  i: Integer;
begin
  Result := nil;
  for i := Low(FEmpresas) to High(FEmpresas) do begin
    if RetornaNumeros(pCNPJ) <> RetornaNumeros(FEmpresas[i].Cnpj) then
      Continue;

    Result := FEmpresas[i];
    Break;
  end;
end;

function TSessao.getEmpresa(pEmpresaId: Integer): RecEmpresas;
var
  i: Integer;
begin
  Result := nil;
  for i := Low(FEmpresas) to High(FEmpresas) do begin
    if pEmpresaId <> FEmpresas[i].EmpresaId then
      Continue;

    Result := FEmpresas[i];
    Break;
  end;
end;


function TSessao.getEmpresaLogada: RecEmpresas;
begin
  Result := FEmpresaLogadaSistema;
end;

function TSessao.getEmpresas: TArray<RecEmpresas>;
begin
  Result := FEmpresas;
end;

function TSessao.getImpressora(pOpcoesImpressora: TOpcoesImpressoras): RecImpressora;
begin
  if FImpresssorasUsuario = nil then
    FImpresssorasUsuario := _ImpressorasUsuarios.BuscarImpressoras(getConexaoBanco, 0, [getEmpresaLogada.EmpresaId, getUsuarioLogado.funcionario_id]);

  if FImpresssorasUsuario <> nil then begin
    case pOpcoesImpressora of
      toNFe: begin
        Result.TipoImpressora := FImpresssorasUsuario[0].TipoImpressoraNFe;
        Result.CaminhoImpressora := FImpresssorasUsuario[0].ImpressoraNFe;
        Result.AbrirPreview := FImpresssorasUsuario[0].AbrirPreviewNFe = 'S';
      end;

      toNFCe: begin
        Result.TipoImpressora := FImpresssorasUsuario[0].TipoImpressoraNFCe;
        Result.CaminhoImpressora := FImpresssorasUsuario[0].ImpressoraNFCe;
        Result.AbrirPreview := FImpresssorasUsuario[0].AbrirPreviewNFCe = 'S';
      end;

      toComprovantePagamento: begin
        Result.TipoImpressora := FImpresssorasUsuario[0].TipoImpressoraCompPagamento;
        Result.CaminhoImpressora := FImpresssorasUsuario[0].ImpressoraCompPagamento;
        Result.AbrirPreview := FImpresssorasUsuario[0].AbrirPreviewCompPagamento = 'S';
      end;

      toComprovanteEntrega: begin
        Result.TipoImpressora := FImpresssorasUsuario[0].TipoImpressoraComproEntrega;
        Result.CaminhoImpressora := FImpresssorasUsuario[0].ImpressoraComproEntrega;
        Result.AbrirPreview := FImpresssorasUsuario[0].AbrirPreviewComproEntrega = 'S';
      end;

      toListaSeparacao: begin
        Result.TipoImpressora := FImpresssorasUsuario[0].TipoImpressoraListaSeparac;
        Result.CaminhoImpressora := FImpresssorasUsuario[0].ImpressoraListaSeparac;
        Result.AbrirPreview := FImpresssorasUsuario[0].AbrirPreviewListaSeparacao = 'S';
      end;

      toOrcamento: begin
        Result.TipoImpressora := FImpresssorasUsuario[0].TipoImpressoraOrcamento;
        Result.CaminhoImpressora := FImpresssorasUsuario[0].ImpressoraOrcamento;
        Result.AbrirPreview := FImpresssorasUsuario[0].AbrirPreviewOrcamento = 'S';
      end;

      toCartazPromocional: begin
        Result.TipoImpressora := FImpresssorasUsuario[0].TipoImpressoraCtzPromo;
        Result.CaminhoImpressora := FImpresssorasUsuario[0].ImpressoraCtzPromo;
        Result.AbrirPreview := FImpresssorasUsuario[0].AbrirPrevCtzPromo = 'S';
      end;

      toComprovantePagtoFinanceiro: begin
        Result.TipoImpressora := FImpresssorasUsuario[0].TipoImpCompPagtoFinanceiro;
        Result.CaminhoImpressora := FImpresssorasUsuario[0].ImpCompPagtoFinanceiro;
        Result.AbrirPreview := FImpresssorasUsuario[0].AbrirPrevCompPagtoFinanc = 'S';
      end;

      toComprovanteDevolucao: begin
        Result.TipoImpressora := FImpresssorasUsuario[0].TipoImpressoraCompDevolucao;
        Result.CaminhoImpressora := FImpresssorasUsuario[0].ImpressoraCompDevolucao;
        Result.AbrirPreview := FImpresssorasUsuario[0].AbrirPrevCompDevolucao = 'S';
      end;

      toComprovanteCaixa: begin
        Result.TipoImpressora := FImpresssorasUsuario[0].TipoImpressoraCompCaixa;
        Result.CaminhoImpressora := FImpresssorasUsuario[0].ImpressoraCompCaixa;
        Result.AbrirPreview := FImpresssorasUsuario[0].AbrirPrevCompCaixa = 'S';
      end;
    end;

    // Saindo se houver configura��o por usu�rio
    if Result.CaminhoImpressora <> '' then
      Exit;
  end;

  if FImpresssorasEstacao = nil then
    FImpresssorasEstacao := _ImpressorasEstacoes.BuscarImpressoras(getConexaoBanco, 0, [_Biblioteca.NomeComputador]);

  if FImpresssorasEstacao <> nil then begin
    case pOpcoesImpressora of
      toNFe: begin
        Result.TipoImpressora := FImpresssorasEstacao[0].TipoImpressoraNFe;
        Result.CaminhoImpressora := FImpresssorasEstacao[0].ImpressoraNFe;
        Result.AbrirPreview := FImpresssorasEstacao[0].AbrirPreviewNFe = 'S';
      end;

      toNFCe: begin
        Result.TipoImpressora := FImpresssorasEstacao[0].TipoImpressoraNFCe;
        Result.CaminhoImpressora := FImpresssorasEstacao[0].ImpressoraNFCe;
        Result.AbrirPreview := FImpresssorasEstacao[0].AbrirPreviewNFCe = 'S';
      end;

      toComprovantePagamento: begin
        Result.TipoImpressora := FImpresssorasEstacao[0].TipoImpressoraCompPagamento;
        Result.CaminhoImpressora := FImpresssorasEstacao[0].ImpressoraCompPagamento;
        Result.AbrirPreview := FImpresssorasEstacao[0].AbrirPreviewCompPagamento = 'S';
      end;

      toComprovanteEntrega: begin
        Result.TipoImpressora := FImpresssorasEstacao[0].TipoImpressoraComproEntrega;
        Result.CaminhoImpressora := FImpresssorasEstacao[0].ImpressoraComproEntrega;
        Result.AbrirPreview := FImpresssorasEstacao[0].AbrirPreviewComproEntrega = 'S';
      end;

      toListaSeparacao: begin
        Result.TipoImpressora := FImpresssorasEstacao[0].TipoImpressoraListaSeparac;
        Result.CaminhoImpressora := FImpresssorasEstacao[0].ImpressoraListaSeparac;
        Result.AbrirPreview := FImpresssorasEstacao[0].AbrirPreviewListaSeparacao = 'S';
      end;

      toOrcamento: begin
        Result.TipoImpressora := FImpresssorasEstacao[0].TipoImpressoraOrcamento;
        Result.CaminhoImpressora := FImpresssorasEstacao[0].ImpressoraOrcamento;
        Result.AbrirPreview := FImpresssorasEstacao[0].AbrirPreviewOrcamento = 'S';
      end;

      toCartazPromocional: begin
        Result.TipoImpressora := FImpresssorasEstacao[0].TipoImpressoraCtzPromo;
        Result.CaminhoImpressora := FImpresssorasEstacao[0].ImpressoraCtzPromo;
        Result.AbrirPreview := FImpresssorasEstacao[0].AbrirPrevCtzPromo = 'S';
      end;

      toComprovantePagtoFinanceiro: begin
        Result.TipoImpressora := FImpresssorasEstacao[0].TipoImpCompPagtoFinanceiro;
        Result.CaminhoImpressora := FImpresssorasEstacao[0].ImpCompPagtoFinanceiro;
        Result.AbrirPreview := FImpresssorasEstacao[0].AbrirPrevCompPagtoFinanc = 'S';
      end;

      toComprovanteDevolucao: begin
        Result.TipoImpressora := FImpresssorasEstacao[0].TipoImpressoraCompDevolucao;
        Result.CaminhoImpressora := FImpresssorasEstacao[0].ImpressoraCompDevolucao;
        Result.AbrirPreview := FImpresssorasEstacao[0].AbrirPrevCompDevolucao = 'S';
      end;

      toComprovanteCaixa: begin
        Result.TipoImpressora := FImpresssorasEstacao[0].TipoImpressoraCompCaixa;
        Result.CaminhoImpressora := FImpresssorasEstacao[0].ImpressoraCompCaixa;
        Result.AbrirPreview := FImpresssorasEstacao[0].AbrirPrevCompCaixa = 'S';
      end;
    end;
  end;
end;

function TSessao.getParametros: RecParametros;
begin
  Result := FParametros;
end;

function TSessao.getParametrosEmpresa: RecParametrosEmpresa;
begin
  Result := FParametrosEmpresaLogada;
end;

function TSessao.getParametrosEstacao: RecParametrosEstacoes;
begin
  Result := FParametrosEstacoes;
end;

function TSessao.getParametrosEstacao2: RecParametrosEstacao;
begin
  Result := FParametrosEstacao;
end;

function TSessao.getTurnoCaixaAberto: RecTurnosCaixas;
begin
  AtualizarTurnoCaixaAberto;
  Result := FTurnoCaixaAberto;
end;

function TSessao.getUsuarioLogado: RecFuncionarios;
begin
  Result := FUsuarioLogadoSistema;
end;

{ RecImpressora }

function RecImpressora.getIndex: Integer;
begin
  Result := _Biblioteca.GetIndexImpressora( CaminhoImpressora );
end;

function RecImpressora.validaImpresssora: Boolean;
begin
  Result := (CaminhoImpressora <> '') or (TipoImpressora = 'G');
  if not Result then
    _Biblioteca.ImpressoraNaoConfigurada;
end;

end.

