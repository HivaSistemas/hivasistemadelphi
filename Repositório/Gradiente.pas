unit Gradiente;

interface

uses
  Windows, SysUtils, Graphics, Types, Math;

type
  TOrientacaoVerticalHorizontal = (goVertical, goHorizontal);

  TGradiente = class
  private
    FCanvas: TCanvas;

    procedure ComumVerticalHorizontal(
      orientacao: TOrientacaoVerticalHorizontal;
      p_cursor_inicial: Integer;
      p_cursor_final: Integer;
      p_linha_inicial: Integer;
      p_linha_final: Integer;
      Cores: array of TColor
    );

  public
    constructor Create(Canvas: TCanvas);

    // Efeitos
    procedure TopToBottom(Rect: TRect; Cores: array of TColor);
    procedure LeftToRight(Rect: TRect; Cores: array of TColor);
    procedure LeftTopToRightBottom(Rect: TRect; Cores: array of TColor);
    procedure LeftBottomToRightTop(Rect: TRect; Cores: array of TColor);
  end;

implementation

{ TGradiente }

procedure TGradiente.ComumVerticalHorizontal(
  orientacao: TOrientacaoVerticalHorizontal;
  p_cursor_inicial: Integer;
  p_cursor_final: Integer;
  p_linha_inicial: Integer;
  p_linha_final: Integer;
  Cores: array of TColor
);
var
  i: Integer;

  t: Integer;                   // controla a transi��o corrente
  tt: Integer;                  // tamanho da transi��o
  pt: Integer;                  // posi��o na transi��o

  // estas vari�veis s�o utilizadas somente para melhorar a performance.
  ri: Integer;
  gi: Integer;
  bi: Integer;

  rd: Integer;
  gd: Integer;
  bd: Integer;

  procedure AjustarTransicao;
  begin
    Inc(t);
    pt := -1;

    ri := GetRValue(Cores[t - 1]);
    gi := GetGValue(Cores[t - 1]);
    bi := GetBValue(Cores[t - 1]);

    rd := GetRValue(Cores[t]) - ri;
    gd := GetGValue(Cores[t]) - gi;
    bd := GetBValue(Cores[t]) - bi;
  end;

begin

  t := 0;
  AjustarTransicao;
  pt := 0;

  tt := (p_cursor_final - p_cursor_inicial) div High(Cores);

  for i := p_cursor_inicial to p_cursor_final do begin

    FCanvas.Pen.Color := RGB(
      ri + Trunc(rd / tt * pt),
      gi + Trunc(gd / tt * pt),
      bi + Trunc(bd / tt * pt)
    );

    if orientacao = goVertical then begin
      FCanvas.MoveTo(p_linha_inicial, i);
      FCanvas.LineTo(p_linha_final, i);
    end
    else begin // goHorizontal
      FCanvas.MoveTo(i, p_linha_inicial);
      FCanvas.LineTo(i, p_linha_final);
    end;

    if pt = tt then
      AjustarTransicao;

    Inc(pt);
  end;

end;

constructor TGradiente.Create(Canvas: TCanvas);
begin
  inherited Create;
  FCanvas := Canvas;
end;

procedure TGradiente.LeftBottomToRightTop(Rect: TRect; Cores: array of TColor);
begin

end;

procedure TGradiente.LeftTopToRightBottom(Rect: TRect; Cores: array of TColor);
begin

end;

procedure TGradiente.LeftToRight(Rect: TRect; Cores: array of TColor);
begin
  ComumVerticalHorizontal(
    goHorizontal,
    Rect.Left,
    Rect.Right,
    Rect.Top,
    Rect.Bottom,
    Cores
  );
end;

procedure TGradiente.TopToBottom(Rect: TRect; Cores: array of TColor);
begin
  ComumVerticalHorizontal(
    goVertical,
    Rect.Top,
    Rect.Bottom,
    Rect.Left,
    Rect.Right,
    Cores
  );
end;

end.
