unit _ImpressaoEtiquetas;

interface

uses
  System.Classes;

type
  RecImpressaoEtiqueta = record
    NomeFantasiaEmpresa: string;
    ProdutoId: Integer;
    NomeProduto: string;
    CodigoOriginal: string;
    CodigoBarras: string;
    UnidadeVenda: string;
    NomeMarca: string;
    NomeCondicao: string;
    PrecoVarejo: Double;
    PrecoVarejo2: Double;
    QtdEtiqueta: Integer;
    QtdVezesCondicao2: Integer;
    Caracteristicas:  string;
  end;

  TEtiqueta = class
  private
    FCaminhoImpressao: string;
  public
    constructor Create(
      pModeloEtiqueta: Integer;
      pCaminhoImpressao: string;
      pDadosEtiquetas: TArray<RecImpressaoEtiqueta>;
      pTipoPrecoImprimir: string
    );
    destructor Destroy; override;
    function Imprimir: Boolean;
  protected
    FModeloEtiqueta: Integer;
    FArquivoImpressao: TStrings;
    FDadosEtiquetas: TArray<RecImpressaoEtiqueta>;
    FTipoPcoImprimir: string;

    procedure MontarEtiquetas; virtual; abstract;
  end;

const
  coModeloEtiqueta01 = 0;
  coModeloEtiqueta02 = 1;
  coModeloEtiqueta03 = 2;
  coModeloEtiqueta04 = 3;
  coModeloEtiqueta05 = 4;
  coModeloEtiqueta06 = 5;
  coModeloEtiqueta07 = 6;
  coModeloEtiqueta08 = 7;

implementation

{ TEtiqueta }

constructor TEtiqueta.Create(
  pModeloEtiqueta: Integer;
  pCaminhoImpressao: string;
  pDadosEtiquetas: TArray<RecImpressaoEtiqueta>;
  pTipoPrecoImprimir: string
);
begin
  FArquivoImpressao := TStringList.Create;
  FModeloEtiqueta := pModeloEtiqueta;
  FCaminhoImpressao := pCaminhoImpressao;
  FDadosEtiquetas := pDadosEtiquetas;
  FTipoPcoImprimir := pTipoPrecoImprimir;
  MontarEtiquetas;
end;

destructor TEtiqueta.Destroy;
begin
  FArquivoImpressao.Free;
end;

function TEtiqueta.Imprimir: Boolean;
begin
  try
    FArquivoImpressao.SaveToFile(FCaminhoImpressao);
    Result := True;
  except
    Result := False;
  end;
end;

end.
