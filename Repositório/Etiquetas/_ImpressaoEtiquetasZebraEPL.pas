unit _ImpressaoEtiquetasZebraEPL;

interface

uses
  _ImpressaoEtiquetas, _Biblioteca, System.SysUtils;

type
  TEtiquetaZebraEPL2 = class(TEtiqueta)
  private
    FTipoPrecoImprimir: string;
    procedure MontarEtiquetas01;
    procedure MontarEtiquetas02;
    procedure MontarEtiquetas03;
    procedure MontarEtiquetas04;
    procedure MontarEtiquetas05;
    procedure MontarEtiquetas06;
    procedure MontarEtiquetas08;
  protected
    procedure MontarEtiquetas; override;
  public
    property TipoPrecoImprimir : string read FTipoPrecoImprimir write FTipoPrecoImprimir;
  end;

implementation

{ TEtiqueta }

procedure TEtiquetaZebraEPL2.MontarEtiquetas01;
var
  i: Integer;
begin
  FArquivoImpressao.Clear;

  for i := Low(FDadosEtiquetas) to High(FDadosEtiquetas) do begin
    FArquivoImpressao.Add('I8,A,001');
    FArquivoImpressao.Add('Q400,024');
    FArquivoImpressao.Add('q831');
    FArquivoImpressao.Add('rN');
    FArquivoImpressao.Add('S4');
    FArquivoImpressao.Add('D7');
    FArquivoImpressao.Add('ZT');
    FArquivoImpressao.Add('JF');
    FArquivoImpressao.Add('OD');
    FArquivoImpressao.Add('R16,0');
    FArquivoImpressao.Add('f100');
    FArquivoImpressao.Add('N');
    FArquivoImpressao.Add('A780,371,2,4,1,1,N,"' + Copy(FDadosEtiquetas[i].NomeProduto, 1, 45) + '"');
    FArquivoImpressao.Add('A780,349,2,4,1,1,N,"' + Copy(FDadosEtiquetas[i].NomeProduto, 46, Length(FDadosEtiquetas[i].NomeProduto))+ '"');
    if FTipoPrecoImprimir <> 'N' then
      FArquivoImpressao.Add('A341,179,2,4,2,4,N,"' + CPad(NFormat(FDadosEtiquetas[i].PrecoVarejo), 8)  + '"');

    FArquivoImpressao.Add('A780,301,2,4,1,1,N,"Ref: ' + FDadosEtiquetas[i].CodigoOriginal + '"');
    FArquivoImpressao.Add('A377,176,2,4,1,1,N,"R$"');
    FArquivoImpressao.Add('A780,273,2,4,1,1,N,"C�digo Hiva: ' + NFormat(FDadosEtiquetas[i].ProdutoId) + '"');
    FArquivoImpressao.Add('A580,348,2,4,1,1,N,"' + LPad(FDadosEtiquetas[i].NomeMarca, 34) + '"');
    FArquivoImpressao.Add('A85,112,2,4,1,1,N,"' + LPad(FDadosEtiquetas[i].UnidadeVenda, 3) + '"');
    FArquivoImpressao.Add('A788,56,2,4,1,1,N,"'+ LPad(FDadosEtiquetas[i].NomeCondicao, 47) + '"');
    FArquivoImpressao.Add('B780,181,2,1,3,9,61,B,"' + FDadosEtiquetas[i].CodigoBarras + '"');
    FArquivoImpressao.Add('P' + IntToStr(FDadosEtiquetas[i].QtdEtiqueta));
  end;
end;

procedure TEtiquetaZebraEPL2.MontarEtiquetas02;
var
  i, vCnt: Integer;
begin
  FArquivoImpressao.Clear;

  for i := Low(FDadosEtiquetas) to High(FDadosEtiquetas) do
  begin
    for vCnt := 1 to FDadosEtiquetas[I].QtdEtiqueta do
    begin

      FArquivoImpressao.Add('I8,A,001');
      FArquivoImpressao.Add('Q184,024');
      FArquivoImpressao.Add('q831');
      FArquivoImpressao.Add('rN');
      FArquivoImpressao.Add('S3');
      FArquivoImpressao.Add('D10');
      FArquivoImpressao.Add('ZT');
      FArquivoImpressao.Add('JF');
      FArquivoImpressao.Add('O');
      FArquivoImpressao.Add('R0,0');
      FArquivoImpressao.Add('f100');
      FArquivoImpressao.Add('N');


      FArquivoImpressao.Add('A815,154,2,1,1,1,N,"'+ Copy(FDadosEtiquetas[i].NomeProduto, 1, 22) +'"');
      FArquivoImpressao.Add('A815,136,2,2,1,1,N,"Cod.: ' + NFormat(FDadosEtiquetas[i].ProdutoId) + '"');
      FArquivoImpressao.Add('A815,111,2,2,1,1,N,"Ref.: ' + FDadosEtiquetas[i].CodigoOriginal + '"');
      FArquivoImpressao.Add('B815,94,2,1,2,6,45,N,"' + FDadosEtiquetas[i].CodigoBarras + '"');

      if FTipoPrecoImprimir <> 'N' then
        FArquivoImpressao.Add('A725,26,2,3,1,1,N,"R$' + CPad(NFormat(FDadosEtiquetas[i].PrecoVarejo), 8)  + '"');

      FArquivoImpressao.Add('A535,154,2,1,1,1,N,"'+ Copy(FDadosEtiquetas[i].NomeProduto, 1, 22) +'"');
      FArquivoImpressao.Add('A535,136,2,2,1,1,N,"Cod.: ' + NFormat(FDadosEtiquetas[i].ProdutoId) + '"');
      FArquivoImpressao.Add('A535,111,2,2,1,1,N,"Ref.: ' + FDadosEtiquetas[i].CodigoOriginal + '"');
      FArquivoImpressao.Add('B535,94,2,1,2,6,45,N,"' + FDadosEtiquetas[i].CodigoBarras + '"');
      if FTipoPrecoImprimir <> 'N' then
        FArquivoImpressao.Add('A425,26,2,3,1,1,N,"R$' + CPad(NFormat(FDadosEtiquetas[i].PrecoVarejo), 8)  + '"');

      FArquivoImpressao.Add('A251,154,2,1,1,1,N,"'+ Copy(FDadosEtiquetas[i].NomeProduto, 1, 22) +'"');
      FArquivoImpressao.Add('A251,136,2,2,1,1,N,"Cod.: ' + NFormat(FDadosEtiquetas[i].ProdutoId) + '"');
      FArquivoImpressao.Add('A251,111,2,2,1,1,N,"Ref.: ' + FDadosEtiquetas[i].CodigoOriginal + '"');
      FArquivoImpressao.Add('B251,94,2,1,2,6,45,N,"' + FDadosEtiquetas[i].CodigoBarras + '"');
      if FTipoPrecoImprimir <> 'N' then
        FArquivoImpressao.Add('A151,26,2,3,1,1,N,"R$' + CPad(NFormat(FDadosEtiquetas[i].PrecoVarejo), 8)  + '"');

      FArquivoImpressao.Add('P1');

    end;
  end;
end;

procedure TEtiquetaZebraEPL2.MontarEtiquetas03;
var
  i: Integer;
begin
  FArquivoImpressao.Clear;

  for i := Low(FDadosEtiquetas) to High(FDadosEtiquetas) do begin

    FArquivoImpressao.Add('');
    FArquivoImpressao.Add('^XA');
    FArquivoImpressao.Add('^PR2,2,2');
    FArquivoImpressao.Add('^CI27');
    FArquivoImpressao.Add('^PW614');
    FArquivoImpressao.Add('^LH10,15^FWN');
    FArquivoImpressao.Add('^CFO,28,18');

    //Quantidade de etiquetas
    FArquivoImpressao.Add('^PQ' + IntToStr(FDadosEtiquetas[i].QtdEtiqueta) );

    //Nome fantasia da empresa
    FArquivoImpressao.Add('^FO50,15^AE,18,10^FD' + FDadosEtiquetas[i].NomeFantasiaEmpresa + '^FS');

    //Nome produto
    if FDadosEtiquetas[i].NomeProduto.Length < 41 then
      // width (largura) da fonte maior [25]
      FArquivoImpressao.Add('^FO50,45^A0,40,25^FD' + FDadosEtiquetas[i].NomeProduto  + '^FS')
    else
      // width (largura) da fonte menor [18]
      FArquivoImpressao.Add('^FO50,45^A0,40,18^FD' + FDadosEtiquetas[i].NomeProduto  + '^FS');

    //Marca
    FArquivoImpressao.Add('^FO50,95^AD,18,10^FD' + FDadosEtiquetas[i].NomeMarca + '^FS');

    //Codigo produto + und venda
    FArquivoImpressao.Add('^FO50,112^AD,18,10^FD' + 'C�d.Prod.: ' + NFormat(FDadosEtiquetas[i].ProdutoId) +
      ' Und: ' + FDadosEtiquetas[i].UnidadeVenda  + '^FS');

    //Pre�o
    if FTipoPrecoImprimir <> 'N' then
      FArquivoImpressao.Add('^FO400,80^AS,65,33^FD' + 'R$ ' + CPad(NFormat(FDadosEtiquetas[i].PrecoVarejo), 8) + '^FS');

    //Codigo barras
    FArquivoImpressao.Add('^FO50,170^BCN,30,Y,N,N^,30,25^FD' + FDadosEtiquetas[i].CodigoBarras + '^FS');

    //Fim da etiqueta - ATEN��O, deve ser o �ltimo codigo enviado
    FArquivoImpressao.Add('^XZ');
  end;
end;

procedure TEtiquetaZebraEPL2.MontarEtiquetas04;
var
  i: Integer;
begin
  FArquivoImpressao.Clear;

  for i := Low(FDadosEtiquetas) to High(FDadosEtiquetas) do begin

    FArquivoImpressao.Add('');
    FArquivoImpressao.Add('^XA');
    FArquivoImpressao.Add('^PR2,2,2');
    FArquivoImpressao.Add('^CI27');
    FArquivoImpressao.Add('^PW614');
    FArquivoImpressao.Add('^LH10,15^FWN');
    FArquivoImpressao.Add('^CFD,28,18');

    //Quantidade de etiquetas
    FArquivoImpressao.Add('^PQ' + IntToStr(FDadosEtiquetas[i].QtdEtiqueta) );

    //Nome fantasia da empresa
    FArquivoImpressao.Add('^FO50,20^AO,18,20^FD' + FDadosEtiquetas[i].NomeFantasiaEmpresa + '^FS');

    //Nome produto
    if FDadosEtiquetas[i].NomeProduto.Length < 70 then
      // width (largura) da fonte maior [20]
      FArquivoImpressao.Add('^FO50,50^A0,45,20^FD' + FDadosEtiquetas[i].NomeProduto  + '^FS') ;

    //Marca
    FArquivoImpressao.Add('^FO50,101^AD,18,12^FD' + 'Marca: ' + FDadosEtiquetas[i].NomeMarca + '^FS');

    //Codigo produto + und venda
    FArquivoImpressao.Add('^FO50,118^AD,18,12^FD' + 'C�d.Prod.: ' + NFormat(FDadosEtiquetas[i].ProdutoId) +
      ' Und: ' + FDadosEtiquetas[i].UnidadeVenda  + '^FS');

    //Pre�o
    if FTipoPrecoImprimir <> 'N' then
      FArquivoImpressao.Add('^FO50,135^AS,65,60^FD' + 'R$ ' + CPad(NFormat(FDadosEtiquetas[i].PrecoVarejo), 6) + '^FS');

    //Nome da condicao de pagamento
    FArquivoImpressao.Add('^FO50,205^AS,50,Y,N,N^AR,30,25^FD' + FDadosEtiquetas[i].NomeCondicao + '^FS');

    //Fim da etiqueta - ATEN��O, deve ser o �ltimo codigo enviado
    FArquivoImpressao.Add('^XZ');
  end;
end;

procedure TEtiquetaZebraEPL2.MontarEtiquetas05;
var
  i, vCnt: Integer;
begin
  FArquivoImpressao.Clear;

  for i := Low(FDadosEtiquetas) to High(FDadosEtiquetas) do
  begin
    for vCnt := 1 to FDadosEtiquetas[I].QtdEtiqueta do
    begin

      FArquivoImpressao.Add('I8,A,001');
      FArquivoImpressao.Add('Q240,024');
      FArquivoImpressao.Add('q831');
      FArquivoImpressao.Add('rN');
      FArquivoImpressao.Add('S3');
      FArquivoImpressao.Add('D20');
      FArquivoImpressao.Add('ZT');
      FArquivoImpressao.Add('JF');
      FArquivoImpressao.Add('O');
      FArquivoImpressao.Add('R0,0');
      FArquivoImpressao.Add('f100');
      FArquivoImpressao.Add('N');

      //1-coluna = posi��o vertical / 2-coluna pois��o orizontal  / 3-coluna define se a descri��o do produto vai
      //sair na vertical ou na orizontal  / 4-coluna define o tamanho da fonte

      //1 etiqueta
      FArquivoImpressao.Add('A800,220,2,2,1,1,N,"'+ Copy(FDadosEtiquetas[i].NomeProduto, 1, 32) +'"');
      FArquivoImpressao.Add('A800,200,2,2,1,1,N,"'+ Copy(FDadosEtiquetas[i].NomeProduto, 33, 60) +'"');
      FArquivoImpressao.Add('A800,165,2,2,1,1,N,"Marca.: ' + (FDadosEtiquetas[i].NomeMarca) + '"');
      FArquivoImpressao.Add('A800,145,2,2,1,1,N,"Cod:' + NFormat(FDadosEtiquetas[i].ProdutoId) +
      ' Und:' + FDadosEtiquetas[i].UnidadeVenda  +   '"');
      if FTipoPrecoImprimir <> 'N' then
        FArquivoImpressao.Add('A800,100,2,3,2,1,N,"R$' + CPad(NFormat(FDadosEtiquetas[i].PrecoVarejo), 10)  + '"');
      FArquivoImpressao.Add('A810,70,2,2,1,1,N," ' + Copy(FDadosEtiquetas[i].NomeCondicao, 1, 32) + '"');
      FArquivoImpressao.Add('A810,50,2,2,1,1,N," ' + Copy(FDadosEtiquetas[i].NomeCondicao, 33, 60) + '"');

      //2 etiqueta
      FArquivoImpressao.Add('A382,220,2,2,1,1,N,"'+ Copy(FDadosEtiquetas[i].NomeProduto, 1, 32) +'"');
      FArquivoImpressao.Add('A382,200,2,2,1,1,N,"'+ Copy(FDadosEtiquetas[i].NomeProduto, 33, 60) +'"');
      FArquivoImpressao.Add('A382,165,2,2,1,1,N,"Marca.: ' + (FDadosEtiquetas[i].NomeMarca) + '"');
      FArquivoImpressao.Add('A382,145,2,2,1,1,N,"Cod:' + NFormat(FDadosEtiquetas[i].ProdutoId) +
      ' Und:' + FDadosEtiquetas[i].UnidadeVenda  +   '"');
      if FTipoPrecoImprimir <> 'N' then
        FArquivoImpressao.Add('A382,100,2,3,2,1,N,"R$' + CPad(NFormat(FDadosEtiquetas[i].PrecoVarejo), 10)  + '"');
      FArquivoImpressao.Add('A392,70,2,2,1,1,N," ' + Copy(FDadosEtiquetas[i].NomeCondicao, 1, 32) + '"');
      FArquivoImpressao.Add('A392,50,2,2,1,1,N," ' + Copy(FDadosEtiquetas[i].NomeCondicao, 33, 60) + '"');

      FArquivoImpressao.Add('P1');
    end;
    end;
  end;

procedure TEtiquetaZebraEPL2.MontarEtiquetas06;
var
  i, vCnt: Integer;
begin
  FArquivoImpressao.Clear;

  for i := Low(FDadosEtiquetas) to High(FDadosEtiquetas) do
  begin
    for vCnt := 1 to FDadosEtiquetas[I].QtdEtiqueta do
    begin

      FArquivoImpressao.Add('I8,A,001');
      FArquivoImpressao.Add('Q1200,024');
      FArquivoImpressao.Add('q831');
      FArquivoImpressao.Add('rN');
      FArquivoImpressao.Add('S3');
      FArquivoImpressao.Add('D20');
      FArquivoImpressao.Add('ZT');
      FArquivoImpressao.Add('JF');
      FArquivoImpressao.Add('O');
      FArquivoImpressao.Add('R0,0');
      FArquivoImpressao.Add('f100');
      FArquivoImpressao.Add('N');

      //1-coluna = posi��o vertical / 2-coluna pois��o orizontal  / 3-coluna define se a descri��o do produto vai
      //sair na vertical ou na orizontal  / 4-coluna define o tamanho da fonte

      //Cabecalho
      FArquivoImpressao.Add('A800,1180,2,3,1,1,N,"Cod.:' + NFormat(FDadosEtiquetas[i].ProdutoId) +'"');
      FArquivoImpressao.Add('A800,1160,2,3,1,1,N,"'+ Copy(FDadosEtiquetas[i].NomeProduto, 1, 48) +'"');
      FArquivoImpressao.Add('A800,1140,2,3,1,1,N,"'+ Copy(FDadosEtiquetas[i].NomeProduto, 49, 60) +'"');
      //Referencia
      FArquivoImpressao.Add('A800,1100,2,3,1,1,N,"Refer�ncia do produto:'+'"');
      //Linha -1
      FArquivoImpressao.Add('A800,1080,2,2,1,1,N,"'+ Copy(FDadosEtiquetas[i].Caracteristicas, 1, 55) +'"');
      //Linha -2
      FArquivoImpressao.Add('A800,1060,2,2,1,1,N,"'+ Copy(FDadosEtiquetas[i].Caracteristicas, 56, 110) +'"');
      //Linha -3
      FArquivoImpressao.Add('A800,1040,2,2,1,1,N,"'+ Copy(FDadosEtiquetas[i].Caracteristicas, 111, 165) +'"');
      //Linha -4
      FArquivoImpressao.Add('A800,1020,2,2,1,1,N,"'+ Copy(FDadosEtiquetas[i].Caracteristicas, 166, 220) +'"');
     //Linha -5
      FArquivoImpressao.Add('A800,1000,2,2,1,1,N,"'+ Copy(FDadosEtiquetas[i].Caracteristicas, 221, 275) +'"');
//      //Linha -6
//      FArquivoImpressao.Add('A800,980,2,2,1,1,N,"'+ Copy(FDadosEtiquetas[i].Caracteristicas, 325, 389) +'"');
//      //Linha -7
//      FArquivoImpressao.Add('A800,960,2,2,1,1,N,"'+ Copy(FDadosEtiquetas[i].Caracteristicas, 390, 454) +'"');
//      //Linha -8
//      FArquivoImpressao.Add('A800,940,2,2,1,1,N,"'+ Copy(FDadosEtiquetas[i].Caracteristicas, 455, 519) +'"');
//      //Linha -9
//      FArquivoImpressao.Add('A800,920,2,2,1,1,N,"'+ Copy(FDadosEtiquetas[i].Caracteristicas, 520, 584) +'"');
//      //Linha -10
//      FArquivoImpressao.Add('A800,900,2,2,1,1,N,"'+ Copy(FDadosEtiquetas[i].Caracteristicas, 585, 649) +'"');
//      //Linha -11
//      FArquivoImpressao.Add('A800,880,2,2,1,1,N,"'+ Copy(FDadosEtiquetas[i].Caracteristicas, 650, 714) +'"');
//      //Linha -12
//      FArquivoImpressao.Add('A800,860,2,2,1,1,N,"'+ Copy(FDadosEtiquetas[i].Caracteristicas, 715, 779) +'"');
//      //Linha -13
//      FArquivoImpressao.Add('A800,840,2,2,1,1,N,"'+ Copy(FDadosEtiquetas[i].Caracteristicas, 780, 843) +'"');
//      //Linha -14
//      FArquivoImpressao.Add('A800,820,2,2,1,1,N,"'+ Copy(FDadosEtiquetas[i].Caracteristicas, 844, 908) +'"');
//      //Linha -15
//      FArquivoImpressao.Add('A800,800,2,2,1,1,N,"'+ Copy(FDadosEtiquetas[i].Caracteristicas, 909, 973) +'"');
//      //Linha -16
//      FArquivoImpressao.Add('A800,780,2,2,1,1,N,"'+ Copy(FDadosEtiquetas[i].Caracteristicas, 974, 1038) +'"');
//      //Linha -17
//      FArquivoImpressao.Add('A800,760,2,2,1,1,N,"'+ Copy(FDadosEtiquetas[i].Caracteristicas, 1039, 1103) +'"');
//      //Linha -18
//      FArquivoImpressao.Add('A800,740,2,2,1,1,N,"'+ Copy(FDadosEtiquetas[i].Caracteristicas, 1104, 1168) +'"');


      FArquivoImpressao.Add('P1');
    end;
  end;
end;

procedure TEtiquetaZebraEPL2.MontarEtiquetas08;
var
  i: Integer;
  valorPrazo: Double;
begin
  FArquivoImpressao.Clear;

  for i := Low(FDadosEtiquetas) to High(FDadosEtiquetas) do begin

    FArquivoImpressao.Add('');
    FArquivoImpressao.Add('^XA');
    FArquivoImpressao.Add('^PR2,2,2');
    FArquivoImpressao.Add('^CI27');
    FArquivoImpressao.Add('^PW614');
    FArquivoImpressao.Add('^LH10,15^FWN');
    FArquivoImpressao.Add('^CFO,28,18');

    //Quantidade de etiquetas
    FArquivoImpressao.Add('^PQ' + IntToStr(FDadosEtiquetas[i].QtdEtiqueta) );

    //Nome produto
    if FDadosEtiquetas[i].NomeProduto.Length < 41 then
      // width (largura) da fonte maior [25]
      FArquivoImpressao.Add('^FO50,20^A0,40,25^FD' + FDadosEtiquetas[i].NomeProduto  + '^FS')
    else
      // width (largura) da fonte menor [18]
      FArquivoImpressao.Add('^FO50,20^A0,40,18^FD' + FDadosEtiquetas[i].NomeProduto  + '^FS');

    //Marca
    FArquivoImpressao.Add('^FO50,60^AD,18,10^FD' + FDadosEtiquetas[i].NomeMarca + '^FS');

    //Codigo produto + und venda
    FArquivoImpressao.Add('^FO50,85^AD,18,10^FD' + 'C�d.Prod.: ' + NFormat(FDadosEtiquetas[i].ProdutoId) +
      ' Und: ' + FDadosEtiquetas[i].UnidadeVenda  + '^FS');

    //Qtde vezes condi��o
    FArquivoImpressao.Add('^FO300,105^A0,15,20^FD' + '( ' + IntToStr(FDadosEtiquetas[i].QtdVezesCondicao2) + 'X )' + '^FS');

    //Calcular pre�o a prazo
    valorPrazo := FDadosEtiquetas[i].PrecoVarejo2 / FDadosEtiquetas[i].QtdVezesCondicao2;

    //Pre�o a prazo
    FArquivoImpressao.Add('^FO385,85^AS,60,20^FD' + 'R$ ' + CPad(NFormat(valorPrazo), 8) + '^FS');

    //Total a prazo
    FArquivoImpressao.Add('^FO240,170^A0,15,20^FDTOTAL A PRAZO: ^FS');

    //Pre�o total a prazo
    FArquivoImpressao.Add('^FO50,160^A0,30,25^FD' + LPad('R$ ' + NFormat(FDadosEtiquetas[i].PrecoVarejo2), 60) + '^FS');

    //Codigo barras
    FArquivoImpressao.Add('^FO50,105^BY1,3,80^BCN,80,Y,N,N^FD' + FDadosEtiquetas[i].CodigoBarras + '^FS');

    //Retangulo com bordas pretas finas
    FArquivoImpressao.Add('^FO50,202^GB480,35,0^FS');

    //Nome condi��o a vista
    FArquivoImpressao.Add('^FO110,210^A0,18,25^FD' + FDadosEtiquetas[i].NomeCondicao + ':^FS');

    //Pre�o a vista
    FArquivoImpressao.Add('^FO50,210^A0,18,25^FD' + LPad('R$ ' + NFormat(FDadosEtiquetas[i].PrecoVarejo), 60) + '^FS');

    //Fim da etiqueta - ATEN��O, deve ser o �ltimo codigo enviado
    FArquivoImpressao.Add('^XZ');
  end;
end;

procedure TEtiquetaZebraEPL2.MontarEtiquetas;
begin
  inherited;
  FTipoPrecoImprimir := FTipoPcoImprimir;
  case FModeloEtiqueta of
    coModeloEtiqueta01 : MontarEtiquetas01;
    coModeloEtiqueta02 : MontarEtiquetas02;
    coModeloEtiqueta03 : MontarEtiquetas03;
    coModeloEtiqueta04 : MontarEtiquetas04;
    coModeloEtiqueta05 : MontarEtiquetas05;
    coModeloEtiqueta06 : MontarEtiquetas06;
    coModeloEtiqueta08 : MontarEtiquetas08;
  end;

end;
//  if FModeloEtiqueta = coModeloEtiqueta01 then
//    MontarEtiquetas01;


end.
