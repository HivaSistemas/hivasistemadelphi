unit _ImagensEtiquetas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Imaging.pngimage, _ImpressaoEtiquetas,
  Vcl.Imaging.jpeg;

type
  TFormImagensEtiquetas = class(TForm)
    im0: TImage;
    im1: TImage;
    im2: TImage;
    Im3: TImage;
    Im4: TImage;
    im5: TImage;
    im8: TImage;
  end;

function BuscarImagemEtiqueta(pOwner: TComponent; pModeloId: Integer): TPicture;

implementation

{$R *.dfm}

function BuscarImagemEtiqueta(pOwner: TComponent; pModeloId: Integer): TPicture;
var
  vForm: TFormImagensEtiquetas;
begin
  Result := nil;
  vForm := TFormImagensEtiquetas.Create(pOwner);
  case pModeloId of
    coModeloEtiqueta01: Result := vForm.im0.Picture;
    coModeloEtiqueta02: Result := vForm.im1.Picture;
    coModeloEtiqueta03: Result := vForm.im2.Picture;
    coModeloEtiqueta04: Result := vForm.im3.Picture;
    coModeloEtiqueta05: Result := vForm.im4.Picture;
    coModeloEtiqueta07: Result := vForm.im5.Picture;
    coModeloEtiqueta08: Result := vForm.im8.Picture;
  end;
//  if pModeloId = coModeloEtiqueta01 then
//    Result := vForm.im0.Picture;
//  if pModeloId = coModeloEtiqueta02 then
//    Result := vForm.im1.Picture;

end;

end.
