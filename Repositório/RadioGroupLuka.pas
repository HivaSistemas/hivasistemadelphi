unit RadioGroupLuka;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.StdCtrls, Vcl.ExtCtrls, Winapi.Windows, Vcl.Forms, _BibliotecaGenerica;

type
  TRadioGroupLuka = class(TRadioGroup)
  private
    FValores: TStringList;

    procedure SetValores(const pValue: TStringList);
  protected
    { Protected declarations }
  public
    constructor Create(AOwner: TComponent); override;

    function GetValor: string;

    procedure SetIndicePorValor(pValor: string);
    procedure SetFocus; override;

    destructor Destroy; override;
  published
    property Valores: TStringList read FValores write SetValores;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('LD Sistemas', [TRadioGroupLuka]);
end;

{ TRadioGroupLuka }

constructor TRadioGroupLuka.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FValores := TStringList.Create;
end;

destructor TRadioGroupLuka.Destroy;
begin
  FreeAndNil(FValores);
  inherited Destroy;
end;

function TRadioGroupLuka.GetValor: string;
begin
  if ItemIndex = -1 then
    Result := ''
  else
    Result := FValores[ItemIndex];
end;

procedure TRadioGroupLuka.SetFocus;
begin
  inherited;
  if ItemIndex > -1 then
    SetarFoco(Buttons[ItemIndex])
  else
    SetarFoco(Buttons[0]);
end;

procedure TRadioGroupLuka.SetIndicePorValor(pValor: string);
begin
  ItemIndex := FValores.IndexOf(pValor);

  if (ItemIndex > -1) and (Buttons[ItemIndex].CanFocus) then
    SetarFoco(Buttons[ItemIndex]);
end;

procedure TRadioGroupLuka.SetValores(const pValue: TStringList);
begin
  FValores.Assign(pValue);
end;

end.
