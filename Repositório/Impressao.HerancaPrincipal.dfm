inherited FormHerancaImpressoesGraficas: TFormHerancaImpressoesGraficas
  Caption = 'Heran'#231'a das impress'#245'es gr'#225'ficas'
  ClientHeight = 569
  ClientWidth = 992
  Scaled = False
  ExplicitWidth = 998
  ExplicitHeight = 598
  PixelsPerInch = 96
  TextHeight = 14
  object qrImpressaoNaoFiscal: TQuickRep
    Left = 15
    Top = 24
    Width = 574
    Height = 337
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE')
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Options = [FirstPageHeader, LastPageFooter]
    Page.Columns = 1
    Page.Orientation = poPortrait
    Page.PaperSize = Custom
    Page.Continuous = False
    Page.Values = (
      0.000000000000000000
      445.822916666666700000
      10.000000000000000000
      759.354166666666800000
      20.000000000000000000
      10.000000000000000000
      0.000000000000000000)
    PrinterSettings.Copies = 1
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 1
    PrinterSettings.LastPage = 1
    PrinterSettings.UseStandardprinter = False
    PrinterSettings.UseCustomBinCode = True
    PrinterSettings.CustomBinCode = 15
    PrinterSettings.ExtendedDuplex = 1
    PrinterSettings.UseCustomPaperCode = False
    PrinterSettings.CustomPaperCode = 262
    PrinterSettings.PrintMetaFile = False
    PrinterSettings.MemoryLimit = 1000000
    PrinterSettings.PrintQuality = 203
    PrinterSettings.Collate = 0
    PrinterSettings.ColorOption = 1
    PrintIfEmpty = True
    SnapToGrid = True
    Units = MM
    Zoom = 200
    PrevFormStyle = fsNormal
    PreviewInitialState = wsMaximized
    PrevInitialZoom = qrZoomToFit
    PreviewDefaultSaveType = stPDF
    PreviewLeft = 0
    PreviewTop = 0
    object qrBandTitulo: TQRBand
      Left = 15
      Top = 8
      Width = 551
      Height = 150
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        198.437500000000000000
        728.927083333333300000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbTitle
      object qrEmpresa: TQRLabel
        Left = 63
        Top = 0
        Width = 432
        Height = 32
        Size.Values = (
          41.892361111111110000
          83.784722222222220000
          0.000000000000000000
          571.059027777777800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrEmpresa'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrCnpjEmpresa: TQRLabel
        Left = 63
        Top = -3
        Width = 485
        Height = 25
        Size.Values = (
          33.072916666666670000
          83.784722222222220000
          -4.409722222222222000
          641.614583333333200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'CNPJ - Empresa'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrEndereco: TQRLabel
        Left = 64
        Top = 16
        Width = 484
        Height = 26
        Size.Values = (
          34.395833333333340000
          84.666666666666680000
          21.166666666666670000
          640.291666666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Endereco'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrCidadeUf: TQRLabel
        Left = 64
        Top = 36
        Width = 210
        Height = 26
        Size.Values = (
          34.395833333333340000
          84.666666666666680000
          47.625000000000000000
          277.812500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cidade - UF'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrTipoDocumento: TQRLabel
        Left = 1
        Top = 66
        Width = 544
        Height = 25
        Size.Values = (
          33.072916666666670000
          1.322916666666667000
          87.312500000000000000
          719.666666666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Heran'#231'a'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrSeparador2: TQRShape
        Left = 3
        Top = 120
        Width = 548
        Height = 2
        Size.Values = (
          2.645833333333333000
          3.968750000000000000
          158.750000000000000000
          724.958333333333200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 15395562
        Shape = qrsRectangle
        VertAdjust = 0
      end
    end
    object qrBandItens: TQRBand
      Left = 15
      Top = 158
      Width = 551
      Height = 28
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        37.041666666666670000
        728.927083333333300000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbDetail
    end
    object qrBandFinal: TQRBand
      Left = 15
      Top = 186
      Width = 551
      Height = 141
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        186.531250000000000000
        728.927083333333300000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbSummary
      object qrSeparador8: TQRShape
        Left = 3
        Top = 0
        Width = 548
        Height = 2
        Size.Values = (
          2.204861111111111000
          4.409722222222222000
          0.000000000000000000
          725.399305555555700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 15395562
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrLabelSistema: TQRLabel
        Left = 439
        Top = 131
        Width = 112
        Height = 9
        Size.Values = (
          11.906250000000000000
          580.760416666666800000
          173.302083333333300000
          148.166666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Hiva Sistemas'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = 4
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -3
      end
    end
  end
end
