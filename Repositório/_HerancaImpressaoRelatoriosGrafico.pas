unit _HerancaImpressaoRelatoriosGrafico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, QuickRpt, QRCtrls, Vcl.ExtCtrls, _Sessao, _Biblioteca,
  qrpctrls, QRExport, QRPDFFilt, QRWebFilt;

type
  TFormHerancaImpressaoRelatoriosGrafico = class(TForm)
    qrRelatorioA4: TQuickRep;
    qrbndCabecalho: TQRBand;
    qrVersaoSistema: TQRLabel;
    qrEmitidoEm: TQRLabel;
    qr13: TQRLabel;
    qrbndRodape: TQRBand;
    qrUsuarioImpressao: TQRLabel;
    qr1: TQRLabel;
    qrFiltrosUtilizados: TQRPMemo;
    QRSysData1: TQRSysData;
    qr2: TQRLabel;
    qrQtdePaginas: TQRLabel;
    QRPDFFilter1: TQRPDFFilter;
    QRExcelFilter1: TQRExcelFilter;
    QRTextFilter1: TQRTextFilter;
    QRCSVFilter1: TQRCSVFilter;
    QRHTMLFilter: TQRHTMLFilter;
    procedure QRSysData1Print(sender: TObject; var Value: string);
  public
    constructor Create(AOwner: TComponent; pFiltros: TArray<string>); reintroduce;
  protected
    procedure Imprimir(pQtdeVias: Integer = 1);
    procedure Preview;

    procedure setFiltros(pFiltros: TArray<string>);
  end;

implementation

{$R *.dfm}

{ TFormHerancaImpressaoRelatoriosGrafico }

constructor TFormHerancaImpressaoRelatoriosGrafico.Create(AOwner: TComponent; pFiltros: TArray<string>);
begin
  inherited Create(AOwner);

  qrVersaoSistema.Caption := Sessao.getVersaoSistema;
  qrFiltrosUtilizados.Lines.Clear;
  qrUsuarioImpressao.Caption := 'Emitido por ' + Sessao.getUsuarioLogado.nome;
  qrEmitidoEm.Caption        := 'Emitido em ' + DHFormat(Sessao.getDataHora);

  setFiltros(pFiltros);
end;

procedure TFormHerancaImpressaoRelatoriosGrafico.Imprimir(pQtdeVias: Integer);
begin

end;

procedure TFormHerancaImpressaoRelatoriosGrafico.Preview;
begin
  qrRelatorioA4.Prepare;
  qrQtdePaginas.Caption := '/' + LPad(NFormat(qrRelatorioA4.QRPrinter.PageCount), 3, '0');

  qrRelatorioA4.PreviewModal;
end;

procedure TFormHerancaImpressaoRelatoriosGrafico.QRSysData1Print(Sender: TObject; var Value: string);
begin
  Value := LPad(Value, 3, '0');
end;

procedure TFormHerancaImpressaoRelatoriosGrafico.setFiltros(pFiltros: TArray<string>);
var
  i: Integer;
begin
  qrFiltrosUtilizados.Lines.Clear;

  if pFiltros = nil then begin
    qr1.Enabled := False;
    Exit;
  end;

  for i := Low(pFiltros) to High(pFiltros) do
    qrFiltrosUtilizados.Lines.Add(pFiltros[i]);
end;

end.
