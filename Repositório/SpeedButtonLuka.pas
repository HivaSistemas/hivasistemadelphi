unit SpeedButtonLuka;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.Buttons, _Imagens, _SessaoGenerica, _Biblioteca;

type
  TSpeedButtonLuka = class(TSpeedButton)
  private
    FAutorizacaoRotina: string;
    FTagImagem: Integer;
    FPedirCertificacao: Boolean;
    FPermitirAutOutroUsuario: Boolean;

    procedure SetTagImagem(const pValue: Integer);
  protected
    { Protected declarations }
  public
    constructor Create(AOwner: TComponent); override;
    procedure Click; override;
  published
    property AutorizacaoRotina: string read FAutorizacaoRotina write FAutorizacaoRotina;
    property TagImagem: Integer read FTagImagem write SetTagImagem;
    property PedirCertificacao: Boolean read FPedirCertificacao write FPedirCertificacao;
    property PermitirAutOutroUsuario: Boolean read FPermitirAutOutroUsuario write FPermitirAutOutroUsuario;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('LD Sistemas', [TSpeedButtonLuka]);
end;

{ TSpeedButtonLuka }

procedure TSpeedButtonLuka.Click;

  procedure VerificarAutorizacaoRotina;
  begin
    if AutorizacaoRotina = '' then begin
      if not PedirCertificacao then
        Exit;
    end;

    if _SessaoGenerica.SessaoGenerica.EGerenteSistema then
      Exit;

    if _SessaoGenerica.SessaoGenerica.AutorizadoRotina(AutorizacaoRotina) then begin
      if PedirCertificacao then begin
        // Colocar uma tela de certifica��o
      end;
    end
    else begin
      if PermitirAutOutroUsuario then begin


      end
      else begin
        _Biblioteca.Exclamar('Voc� n�o possui permiss�o para realizar est� rotina!');
        Abort;
      end;
    end;
  end;

begin
  VerificarAutorizacaoRotina;
  inherited Click;
end;

constructor TSpeedButtonLuka.Create(AOwner: TComponent);
begin
  inherited;
  Height := 43;
  Width := 117;
  Flat := True;
end;

procedure TSpeedButtonLuka.SetTagImagem(const pValue: Integer);
begin
  FTagImagem := pValue;
  //{$RUNONLY ON}
  Glyph := _Imagens.getGlyph(Self, TagImagem);
//  NumGlyphs := 1;
//  {$RUNONLY OFF}
end;

end.
