unit EditCpfCnpjLuka;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.StdCtrls, Vcl.Mask, Vcl.Graphics, _Biblioteca;

type
  TCPF_CNPJ = set of (tccCPF, tccCNPJ);

  TEditCPF_CNPJ_Luka = class(TMaskEdit)
  private
    FTipo: TCPF_CNPJ;
    procedure SetTipo(pTipo: TCPF_CNPJ);
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
  public
    function getCPF_CNPJOk: Boolean;
  published
    property Tipo: TCPF_CNPJ read FTipo write SetTipo;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('LD Sistemas', [TEditCPF_CNPJ_Luka]);
end;

{ TEditCPF_CNPJ_Luka }

procedure TEditCPF_CNPJ_Luka.CMEnter(var Message: TCMEnter);
begin
  if Self.Color = clWindow then
    Self.Color := $0080FFFF;
end;

procedure TEditCPF_CNPJ_Luka.CMExit(var Message: TCMExit);
begin
inherited;
  if Self.Color = $0080FFFF then
    Self.Color := clWindow;

  if not getCPF_CNPJOk then
    Text := '';
end;

function TEditCPF_CNPJ_Luka.getCPF_CNPJOk: Boolean;

begin
  if (FTipo = [tccCNPJ]) and ( Length(RetornaNumeros(Text)) <> 14 ) then
    Result := False
  else if (Ftipo = [tccCPF]) and ( Length(RetornaNumeros(Text)) <> 11) then
    Result := False
  else
    Result := _Biblioteca.getCNPJ_CPFOk(Text);
end;

procedure TEditCPF_CNPJ_Luka.SetTipo(pTipo: TCPF_CNPJ);
begin
  FTipo := pTipo;
  if FTipo = [] then
    EditMask := '';
  if FTipo = [tccCPF] then
    EditMask := '999.999.999-99';
  if FTipo = [tccCNPJ] then
    EditMask := '99.999.999/9999-99';
end;

end.
