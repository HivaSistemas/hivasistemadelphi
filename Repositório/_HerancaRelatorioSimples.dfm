inherited FormHerancaRelatorioSimples: TFormHerancaRelatorioSimples
  Caption = 'FormHerancaRelatorioSimples'
  PixelsPerInch = 96
  TextHeight = 13
  inherited qrRelatorioNF: TQuickRep
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Page.Values = (
      0.000000000000000000
      509.322916666666800000
      10.000000000000000000
      750.093750000000000000
      10.000000000000000000
      0.000000000000000000
      0.000000000000000000)
    inherited qrBandTitulo: TQRBand
      Left = 6
      Top = 6
      Width = 448
      Height = 120
      Size.Values = (
        198.437500000000000000
        740.833333333333300000)
      ExplicitLeft = 6
      ExplicitTop = 6
      ExplicitWidth = 448
      ExplicitHeight = 120
      inherited qrlNFNomeEmpresa: TQRLabel
        Size.Values = (
          34.726562500000000000
          6.614583333333332000
          16.536458333333330000
          744.140625000000000000)
        FontSize = 6
      end
      inherited qrNFCnpj: TQRLabel
        Left = 51
        Top = -3
        Width = 388
        Size.Values = (
          33.072916666666670000
          83.784722222222220000
          -4.409722222222222000
          641.614583333333200000)
        FontSize = 7
        ExplicitLeft = 51
        ExplicitTop = -3
        ExplicitWidth = 388
      end
      inherited qrNFEndereco: TQRLabel
        Left = 51
        Top = 13
        Width = 387
        Size.Values = (
          34.395833333333340000
          84.666666666666680000
          21.166666666666670000
          640.291666666666800000)
        FontSize = 7
        ExplicitLeft = 51
        ExplicitTop = 13
        ExplicitWidth = 387
      end
      inherited qrNFCidadeUf: TQRLabel
        Left = 51
        Top = 29
        Width = 168
        Height = 21
        Size.Values = (
          34.395833333333340000
          84.666666666666680000
          47.625000000000000000
          277.812500000000000000)
        FontSize = 7
        ExplicitLeft = 51
        ExplicitTop = 29
        ExplicitWidth = 168
        ExplicitHeight = 21
      end
      inherited qrNFTipoDocumento: TQRLabel
        Top = 53
        Size.Values = (
          33.072916666666670000
          1.322916666666667000
          87.312500000000000000
          719.666666666666800000)
        FontSize = 7
        ExplicitTop = 53
      end
      inherited qrSeparador2: TQRShape
        Top = 96
        Size.Values = (
          2.645833333333333000
          3.968750000000000000
          158.750000000000000000
          724.958333333333200000)
        ExplicitTop = 96
      end
      inherited qrNFTitulo: TQRLabel
        Top = 74
        Size.Values = (
          33.072916666666670000
          1.322916666666667000
          121.708333333333300000
          719.666666666666800000)
        FontSize = 7
        ExplicitTop = 74
      end
      inherited qrlNFEmitidoEm: TQRLabel
        Size.Values = (
          23.151041666666670000
          370.416666666666700000
          3.307291666666667000
          357.187500000000000000)
        FontSize = -6
      end
      inherited qrlNFTelefoneEmpresa: TQRLabel
        Size.Values = (
          42.994791666666670000
          6.614583333333332000
          167.018229166666700000
          744.140625000000000000)
        Caption = 'Telefone: (62) 3000-0000'
        FontSize = 7
      end
    end
    inherited PageFooterBand1: TQRBand
      Left = 6
      Top = 126
      Width = 448
      Size.Values = (
        24.804687500000000000
        740.833333333333300000)
      ExplicitLeft = 6
      ExplicitTop = 126
      ExplicitWidth = 448
      inherited qrlNFSistema: TQRLabel
        Size.Values = (
          23.151041666666670000
          582.083333333333400000
          0.000000000000000000
          138.906250000000000000)
        Caption = 'Hiva 3.0.0.1'
        FontSize = -6
      end
    end
  end
  inherited qrRelatorioA4: TQuickRep
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    inherited qrCabecalho: TQRBand
      Size.Values = (
        227.541666666666700000
        1899.708333333333000000)
      inherited qrCaptionEndereco: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          44.979166666666670000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrEmpresa: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          7.937500000000000000
          568.854166666666700000)
        FontSize = 7
      end
      inherited qrEndereco: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          44.979166666666670000
          1513.416666666667000000)
        FontSize = 7
      end
      inherited qrCaptionEmpresa: TQRLabel [3]
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          7.937500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrEmitidoEm: TQRLabel [4]
        Size.Values = (
          29.104166666666670000
          1529.291666666667000000
          0.000000000000000000
          370.416666666666700000)
        FontSize = 5
      end
      inherited qr1: TQRLabel [5]
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          82.020833333333320000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrBairro: TQRLabel [6]
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          82.020833333333320000
          568.854166666666700000)
        FontSize = 7
      end
      inherited qr3: TQRLabel [7]
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          82.020833333333320000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrCidadeUf: TQRLabel [8]
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          82.020833333333320000
          568.854166666666700000)
        FontSize = 7
      end
      inherited qr5: TQRLabel [9]
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          7.937500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrCNPJ: TQRLabel [10]
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          7.937500000000000000
          248.708333333333300000)
        Caption = '00.000.000/0001-00'
        FontSize = 7
      end
      inherited qr7: TQRLabel [11]
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrTelefone: TQRLabel [12]
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          119.062500000000000000
          187.854166666666700000)
        Caption = '(62) 3000-0000'
        FontSize = 7
      end
      inherited qrFax: TQRLabel [13]
        Size.Values = (
          34.395833333333330000
          767.291666666666800000
          119.062500000000000000
          187.854166666666700000)
        Caption = '(62) 3000-0000'
        FontSize = 7
      end
      inherited qr10: TQRLabel [14]
        Size.Values = (
          34.395833333333330000
          603.250000000000000000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qr11: TQRLabel [15]
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrEmail: TQRLabel [16]
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          119.062500000000000000
          769.937500000000000000)
        Caption = 'hivasolcuoes@gmail.com'
        FontSize = 7
      end
      inherited qr13: TQRLabel [17]
        Size.Values = (
          52.916666666666670000
          711.729166666666800000
          164.041666666666700000
          529.166666666666700000)
        FontSize = 12
      end
      object QRShape1: TQRShape [18]
        Left = 0
        Top = 80
        Width = 718
        Height = 11
        Enabled = False
        Size.Values = (
          29.104166666666670000
          0.000000000000000000
          211.666666666666700000
          1899.708333333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      inherited qrLogoEmpresa: TQRImage [19]
        Size.Values = (
          211.666666666666700000
          13.229166666666670000
          2.645833333333333000
          211.666666666666700000)
      end
    end
    object qrTitulo: TQRBand [1]
      Left = 38
      Top = 124
      Width = 718
      Height = 34
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        89.958333333333330000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbTitle
      object QRShape2: TQRShape
        Left = 0
        Top = 27
        Width = 718
        Height = 11
        Size.Values = (
          29.104166666666670000
          0.000000000000000000
          71.437500000000000000
          1899.708333333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
    end
    object qrBandaCabecalhoColunas: TQRBand [2]
      Left = 38
      Top = 158
      Width = 718
      Height = 20
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        52.916666666666670000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbColumnHeader
    end
    object qrBandaDetalhes: TQRBand [3]
      Left = 38
      Top = 178
      Width = 718
      Height = 19
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        50.270833333333330000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbDetail
    end
    inherited qrbndRodape: TQRBand
      Top = 197
      Height = 44
      Size.Values = (
        116.416666666666700000
        1899.708333333333000000)
      ExplicitTop = 197
      ExplicitHeight = 44
      inherited qrUsuarioImpressao: TQRLabel
        Size.Values = (
          31.750000000000000000
          7.937500000000000000
          74.083333333333340000
          354.541666666666700000)
        FontSize = 7
      end
      inherited qrSistema: TQRLabel
        Size.Values = (
          31.750000000000000000
          1529.291666666667000000
          74.083333333333340000
          370.416666666666700000)
        Caption = 'Hiva 3.0.0.1'
        FontSize = 7
      end
    end
  end
end
