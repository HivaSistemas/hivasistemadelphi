unit StaticTextLuka;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.StdCtrls, _BibliotecaGenerica, Vcl.Graphics, Winapi.Messages;

type
  TTipoCampo = (tcTexto, tcNumerico, tcSimNao);

  TStaticTextLuka = class(TStaticText)
  private
    FValorAtual: Double;
    FValorInt: Integer;
    FCasasDecimais: Integer;
    FTipoCampo: TTipoCampo;

    procedure SetCasasDecimais(const pCasasDecimais: Integer); overload;
    procedure SetCasasDecimais(const pCasasDecimais: Integer; const pValorAtual: Double); overload;
    procedure CMTextChanged(var Message: TMessage); message CM_TEXTCHANGED;
    procedure setDouble(pValor: Double);
    procedure setCurrency(pValor: Currency);
    procedure setInt(pValor: Integer);

    function getValorTotalCurrency: Currency;
    function getValorInt: Integer;
  protected
    { Protected declarations }
  public
    constructor Create(AOwner: TComponent); override;
    procedure Somar(pValor: Double); overload;
    procedure SomarInt(pValor: Integer); overload;
    procedure SetTipoCampo(const pValor: TTipoCampo);
    procedure Clear;
  published
    property AsInt: Integer read getValorInt write setInt;
    property AsCurr: Currency read getValorTotalCurrency write setCurrency;
    property AsDouble: Double read FValorAtual write setDouble;
    property TipoCampo: TTipoCampo read FTipoCampo write SetTipoCampo;
    property CasasDecimais: Integer read FCasasDecimais write SetCasasDecimais;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('NoviTec', [TStaticTextLuka]);
end;

{ TStaticTextLuka }

procedure TStaticTextLuka.Clear;
begin
  inherited;
  Caption := '';
  FValorAtual := 0;
  FValorInt := 0;
end;

procedure TStaticTextLuka.CMTextChanged(var Message: TMessage);
begin
  inherited;
  if TipoCampo = tcNumerico then begin
    FValorAtual := SFormatDouble(Caption);
    FValorInt   := SFormatInt(Caption);
  end
  else if TipoCampo = tcSimNao then begin
    if Caption = 'S' then
      Caption := 'Sim'
    else if Caption = 'N' then
      Caption := 'N�o';

    Font.Color := IIf(Caption = 'Sim', clBlue, clRed);
  end;
end;

constructor TStaticTextLuka.Create(AOwner: TComponent);
begin
  inherited;
  Color       := _BibliotecaGenerica.getCorVerdePadraoAltis();
  BevelInner  := bvNone;
  Alignment   := taCenter;
  Transparent := False;

  AutoSize := False;

  Font.Color := clWhite;
  Font.Style := [fsBold];

  FValorAtual := 0;
  FValorInt   := 0;
end;

function TStaticTextLuka.getValorInt: Integer;
begin
  Result := FValorInt;
end;

function TStaticTextLuka.getValorTotalCurrency: Currency;
begin
  Result := FValorAtual;
end;

procedure TStaticTextLuka.SetCasasDecimais(const pCasasDecimais: Integer; const pValorAtual: Double);
begin
  if FTipoCampo = tcNumerico then begin
    FCasasDecimais := pCasasDecimais;
    case FCasasDecimais of
      0: Text := FormatFloat('#,###', pValorAtual);
      1: Text := FormatFloat('#,##0.0', pValorAtual);
      2: Text := FormatFloat('#,##0.00', pValorAtual);
      3: Text := FormatFloat('#,##0.000', pValorAtual);
      4: Text := FormatFloat('#,##0.0000', pValorAtual);
      5: Text := FormatFloat('#,##0.00000', pValorAtual);
      6: Text := FormatFloat('#,##0.000000', pValorAtual);
    else
      Text := '';
      CasasDecimais := 0;
    end;
  end;
//  else begin
//    Text := '';
//    FCasasDecimais := 0;
//  end;
end;

procedure TStaticTextLuka.setCurrency(pValor: Currency);
begin
  FValorAtual := pValor;
  SetCasasDecimais(FCasasDecimais, FValorAtual);
end;

procedure TStaticTextLuka.setDouble(pValor: Double);
begin
  FValorAtual := pValor;
  SetCasasDecimais(FCasasDecimais, FValorAtual);
end;

procedure TStaticTextLuka.setInt(pValor: Integer);
begin
  FValorInt := pValor;
  SetCasasDecimais(FCasasDecimais, FValorInt);
end;

procedure TStaticTextLuka.SetCasasDecimais(const pCasasDecimais: Integer);
begin
  if pCasasDecimais <> FCasasDecimais then
    SetCasasDecimais(pCasasDecimais, 0);
end;

procedure TStaticTextLuka.SetTipoCampo(const pValor: TTipoCampo);
begin
  if FTipoCampo = pValor then
    Exit;

  FTipoCampo := pValor;
  if pValor = tcTexto then begin
    Alignment := taLeftJustify;
    CasasDecimais := 0;
  end
  else if pValor = tcSimNao then begin
    Color     := clWhite;
    BevelKind := bkFlat;
    Alignment := taCenter;
    Height    := 17;
  end
  else
    Alignment := taCenter;
end;

procedure TStaticTextLuka.SomarInt(pValor: Integer);
begin
  FValorInt := FValorInt + pValor;
  SetCasasDecimais(0, FValorInt);
end;

procedure TStaticTextLuka.Somar(pValor: Double);
begin
  FValorAtual := FValorAtual + pValor;
  SetCasasDecimais(FCasasDecimais, FValorAtual);
end;

end.
