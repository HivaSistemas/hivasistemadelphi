unit _HerancaPesquisas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPrincipal, Vcl.Grids, GridLuka, _RecordsEspeciais,
  Vcl.StdCtrls, EditLuka, ComboBoxLuka, _Biblioteca, System.StrUtils, _Imagens,
  Vcl.ExtCtrls;

type
  TFormHerancaPesquisas = class(TFormHerancaPrincipal)
    sgPesquisa: TGridLuka;
    eQuantidadeDigitosPesquisa: TEdit;
    pnFiltro1: TPanel;
    lblPesquisa: TLabel;
    lblOpcoesPesquisa: TLabel;
    eValorPesquisa: TEditLuka;
    cbOpcoesPesquisa: TComboBoxLuka;
    procedure FormShow(Sender: TObject);
    procedure sgPesquisaDblClick(Sender: TObject);
    procedure eValorPesquisaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgPesquisaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgPesquisaGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure sgPesquisaEnter(Sender: TObject);
  public
    destructor Destroy; override;
  protected
    FDados: TArray<TObject>;
    FAuxiliares: TArray<Variant>;

    procedure ValidarQtdeMinimaDigitos;
    procedure BuscarRegistros; virtual;
  end;

function Pesquisar(pForm: TFormClass; pFiltros: TArray<RecFiltros>; pAuxiliares: TArray<Variant> = nil): TObject;
function PesquisarVarios(pForm: TFormClass; pFiltros: TArray<RecFiltros>): TArray<TObject>;

const
  coSelecionado = 0;
  coCodigo      = 1;

implementation

{$R *.dfm}

{ TFormHerancaPesquisas }

function Pesquisar(pForm: TFormClass; pFiltros: TArray<RecFiltros>; pAuxiliares: TArray<Variant> = nil): TObject;
var
  t: TFormHerancaPesquisas;
begin
  Result := nil;

  t := TFormHerancaPesquisas(pForm.Create(Application));
  t.sgPesquisa.OcultarColunas([coSelecionado]);
  t.cbOpcoesPesquisa.Filtros := pFiltros;
  t.FAuxiliares := pAuxiliares;

  if t.ShowModal = mrOk then
    Result := t.FDados[t.sgPesquisa.Row - 1];

  t.Free;
end;

function PesquisarVarios(pForm: TFormClass; pFiltros: TArray<RecFiltros>): TArray<TObject>;
var
  i: Integer;
  t: TFormHerancaPesquisas;
begin
  Result := nil;

  t := TFormHerancaPesquisas(pForm.Create(Application));
  t.cbOpcoesPesquisa.Filtros := pFiltros;

  if t.ShowModal = mrOk then begin
    for i := t.sgPesquisa.FixedRows to t.sgPesquisa.RowCount - 1 do begin
      if t.sgPesquisa.Cells[coSelecionado, i] = charNaoSelecionado then
        Continue;

      SetLength(Result, Length(Result) + 1);
      Result[High(Result)] := t.FDados[i - 1]
    end;
  end;

  t.Free;
end;

procedure TFormHerancaPesquisas.BuscarRegistros;
begin
  inherited;
  FDados := nil;
  sgPesquisa.ClearGrid;
  ValidarQtdeMinimaDigitos;
end;

destructor TFormHerancaPesquisas.Destroy;
begin
  FDados := nil;
  inherited;
end;

procedure TFormHerancaPesquisas.eValorPesquisaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    SetarFoco(sgPesquisa);
end;

procedure TFormHerancaPesquisas.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_ESCAPE then
    Close;
end;

procedure TFormHerancaPesquisas.FormShow(Sender: TObject);
begin
  inherited;
  FDados := nil;
  SetarFoco(eValorPesquisa);

  if sgPesquisa.ColWidths[coSelecionado] < 1 then
    sgPesquisa.Col := coCodigo;
end;

procedure TFormHerancaPesquisas.sgPesquisaDblClick(Sender: TObject);
begin
  inherited;
  if sgPesquisa.Cells[coCodigo, sgPesquisa.Row] = '' then begin
    Exclamar('Nenhum registro foi selecionado!');
    Exit;
  end;

  sgPesquisa.Cells[coSelecionado, sgPesquisa.Row] := charSelecionado;
  ModalResult := mrOk;
end;

procedure TFormHerancaPesquisas.sgPesquisaEnter(Sender: TObject);
begin
  inherited;
  BuscarRegistros;
end;

procedure TFormHerancaPesquisas.sgPesquisaGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if (ACol <> coSelecionado) or (sgPesquisa.Cells[ACol, ARow] = '') then
   Exit;

  if sgPesquisa.Cells[ACol, ARow] = charNaoSelecionado then
    APicture := _Imagens.FormImagens.im1.Picture
  else
    if sgPesquisa.Cells[ACol, ARow] = charSelecionado then
      APicture := _Imagens.FormImagens.im2.Picture;
//    else
//      APicture := _Imagens.FormImagens.im3.Picture;    criar imagem para isso
end;

procedure TFormHerancaPesquisas.sgPesquisaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then begin
    if sgPesquisa.Cells[coCodigo, sgPesquisa.Row] = '' then begin
      Exclamar('Nenhum registro foi selecionado!');
      Exit;
    end;
    sgPesquisa.Cells[coSelecionado, sgPesquisa.Row] := charSelecionado;
    ModalResult := mrOk;
  end
  else
    MarcarDesmarcarTodos(sgPesquisa, coSelecionado, Key, Shift);
end;

procedure TFormHerancaPesquisas.ValidarQtdeMinimaDigitos;
begin
  try
    StrToInt(eQuantidadeDigitosPesquisa.Text);
  except
    _Biblioteca.Exclamar('N�o foi informada a quantidade m�nima de d�gitos para a pesquisa!');
    SetarFoco(eValorPesquisa);
    Abort;
  end;

  if Length(Trim(eValorPesquisa.Text)) < StrToInt(eQuantidadeDigitosPesquisa.Text) then begin
    _Biblioteca.Exclamar('Digite ao menos ' + eQuantidadeDigitosPesquisa.Text + ' letras para a pesquisa!');
    SetarFoco(eValorPesquisa);
    Abort;
  end;
end;

end.
