unit _ColetarColunasGrid;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, ExtCtrls, StdCtrls, CheckLst, Grids;

type

  TRetornoColunaGrid = record
  	indice: Integer;
    nome: string;
    mostrar: Boolean;
  end;

  TArrayOfTRetornoColunaGrid = array of TRetornoColunaGrid;

  TFormColetarColunasGrid = class(TForm)
    ShBorda: TShape;
    hsBordaPainel: TShape;
    Panel1: TPanel;
    sbOk: TSpeedButton;
    sbCancelar: TSpeedButton;
    sbMarcarDesmarcar: TSpeedButton;
    clbColunas: TCheckListBox;
    procedure sbOkClick(Sender: TObject);
    procedure sbCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbMarcarDesmarcarClick(Sender: TObject);
  private
    { Private declarations }
  public
		{ Public declarations }
  end;

  TColetarColunasGrid = Class(TComponent)
  private
    { Private declarations }
    dados: TArrayOfTRetornoColunaGrid;

  public
		{ Public declarations }
    Constructor Create; reintroduce;
    procedure AddColuna(_caption: string; indice: Integer; marcado: Boolean);
    function Executar(_top: Integer; _left: Integer): TArrayOfTRetornoColunaGrid;
  end;

implementation


{$R *.dfm}

{ ColetarColunasGrid }

constructor TColetarColunasGrid.Create;
begin
	dados := nil;
end;


function TColetarColunasGrid.Executar(_top: Integer; _left: Integer): TArrayOfTRetornoColunaGrid;
var
	i: Integer;
  retorno_indice: Integer;
  formulario: TFormColetarColunasGrid;

  function GetIndiceColunaGrid(nome: string): Integer;
  var
  	j: Integer;
  begin
  	Result := -1;
    if dados = nil then
    	Exit;

    for j := Low(dados) to High(dados) do begin
      if dados[j].nome = nome then
      	Result := dados[j].indice;
    end;
  end;

begin
  Result := nil;
	formulario := TFormColetarColunasGrid.Create(Application);
  with formulario do begin
  	top := _top;
    left := _left;
    for i := Low(dados) to High(dados) do begin
      clbColunas.Items.Add(dados[i].nome);
      clbColunas.Checked[i] := dados[i].mostrar;
    end;


    if ShowModal = mrOk then begin
      SetLength(Result, length(dados));
      for i := 0 to clbColunas.Count -1 do begin
        retorno_indice := GetIndiceColunaGrid(clbColunas.Items[i]);
        if retorno_indice <> -1 then begin
          Result[i].indice := retorno_indice;
          Result[i].mostrar := clbColunas.Checked[i];
          Result[i].nome := clbColunas.Items[i];
        end;
      end;
    end;
  end;
  formulario.Free;
end;

procedure TColetarColunasGrid.AddColuna(_caption: string; indice: Integer; marcado: Boolean);
var
	posic: Integer;
begin
	if (_caption = '') or (indice = -1) then
  	Exit;

  SetLength(dados, Length(dados) + 1);
  posic := High(dados);

  dados[posic].nome := _caption;
  dados[posic].indice := indice;
  dados[posic].mostrar := marcado;
end;

procedure TFormColetarColunasGrid.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if key = VK_RETURN then
  	sbOk.Click
  else if key = VK_ESCAPE then
  	ModalResult := mrCancel;
end;

procedure TFormColetarColunasGrid.FormShow(Sender: TObject);
begin
  SetForegroundWindow(Self.WindowHandle);
end;

procedure TFormColetarColunasGrid.sbOkClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TFormColetarColunasGrid.sbCancelarClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TFormColetarColunasGrid.sbMarcarDesmarcarClick(Sender: TObject);
var
  i: Integer;
  todos_marcados: boolean;
begin
  inherited;
  todos_marcados := true;

  for i := 0 to clbColunas.Count - 1 do
    todos_marcados := todos_marcados and clbColunas.Checked[i];

  for i := 0 to clbColunas.Count - 1 do
    clbColunas.Checked[i] := not todos_marcados;
end;

end.
