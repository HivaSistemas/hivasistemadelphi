unit _Logs;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TLogs = class(TComponent)
  private
    FPath: string;
    FArquivo: TStringList;
  public
    constructor Create(AOwner: TComponent; const pDiretorioSistema: string; const pNomeLogs: string); reintroduce;

    procedure AddLog(pLog: string);

    destructor Destroy; override;
  end;

implementation

{ TLogs }

constructor TLogs.Create(AOwner: TComponent; const pDiretorioSistema: string; const pNomeLogs: string);
begin
  inherited Create(AOwner);
  FPath := pDiretorioSistema + '\Sistema';
  if not DirectoryExists(FPath) then
    MkDir(FPath);

  FPath := FPath + '\Logs';
  if not DirectoryExists(FPath) then
    MkDir(FPath);

  FPath := FPath + '\' + pNomeLogs + ' ' + FormatDateTime('dd-mm-yyyy', Now) + '.log';

  try
    FArquivo := TStringList.Create;
    if FileExists(FPath) then
      FArquivo.LoadFromFile(FPath);
  except
    // N�o fazer nada
  end;
end;

destructor TLogs.Destroy;
begin
  FArquivo.Free;
  inherited;
end;

procedure TLogs.AddLog(pLog: string);
begin
  if FArquivo = nil then
    Exit;

  try
    FArquivo.Append( FormatDateTime('dd/mm/yyyy hh:nn:ss', Now) + '...: ' + pLog );
    FArquivo.SaveToFile(FPath);
  except
    // nada
  end;
end;

end.
