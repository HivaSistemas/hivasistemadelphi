unit Impressao.HerancaPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPrincipal, QRCtrls, QuickRpt,
  Vcl.ExtCtrls;

type
  TFormHerancaImpressoesGraficas = class(TFormHerancaPrincipal)
    qrImpressaoNaoFiscal: TQuickRep;
    qrBandTitulo: TQRBand;
    qrEmpresa: TQRLabel;
    qrCnpjEmpresa: TQRLabel;
    qrEndereco: TQRLabel;
    qrCidadeUf: TQRLabel;
    qrTipoDocumento: TQRLabel;
    qrSeparador2: TQRShape;
    qrBandItens: TQRBand;
    qrBandFinal: TQRBand;
    qrSeparador8: TQRShape;
    qrLabelSistema: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

end.
