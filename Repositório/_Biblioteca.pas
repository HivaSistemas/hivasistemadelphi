unit _Biblioteca;

interface

uses
  Vcl.Forms, Vcl.Controls, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Dialogs, System.DateUtils, System.SysUtils, _FrameHerancaPrincipal,
  System.StrUtils, Vcl.ComCtrls, GroupBoxLuka, GridLuka, Vcl.Mask, Winapi.Windows, Winapi.WinSock, System.Math, Winapi.WinInet,
  Vcl.Graphics, System.Classes, _RecordsOrcamentosVendas, _RecordsFinanceiros, EditLukaData, System.Types, System.UITypes,
  System.MaskUtils, ComboBoxLuka, Vcl.Printers, StaticTextLuka, System.Variants, Vcl.Menus, QRCtrls, qrpctrls, QuickRpt,
  System.Win.ComObj;

type
  TRetTelaFinalizar = (trOk, trCancelado);

  TRetornoTelaFinalizar = record
    RetTela: TRetTelaFinalizar;
    RetInt: Integer;

    procedure Iniciar;
    function BuscaCancelada: Boolean;
    function Ok(const pResultModal: TModalResult; pMostrarMensagemSeCancelado: Boolean = False): Boolean;
  end;

  TRetornoTelaFinalizar<T> = record
  public
    Dados: t;
    RetTela: TRetTelaFinalizar;

    procedure Iniciar;
    function BuscaCancelada: Boolean;
    function Ok(const pResultModal: TModalResult; pMostrarMensagemSeCancelado: Boolean = False): Boolean;
    function OkPesquisaAvancada(const pResultModal: TModalResult; pMostrarMensagemSeCancelado: Boolean; pGrid: TGridLuka; var pDados: TArray<t>): Boolean;
  end;

  TEventoObjeto = procedure(Sender: TObject) of object;

// ------------ Procedimentos --------------------
procedure NenhumRegistro;
procedure RegistroExcluidoSucesso;
procedure NaoAutorizadoRotina;
procedure NenhumRegistroSelecionado;
procedure RotinaSucesso;
procedure RotinaCanceladaUsuario;
procedure NaoPermitidoConsumidorFinal;
procedure ImpressoraNaoConfigurada;
procedure RotinaNaoPermitidaDepositoFechado;
procedure Exclamar(pMensagem: string);
procedure ImpressaoNaoDisponivel;
procedure Informar(pMensagem: string);
procedure InformarAlteracaoRegistro(pId: Integer);
procedure Avisar(pTituloAviso: string; pAviso: string);
procedure MensagemPadraoNaoEmissaoNFe;
procedure SetarFoco(pObjeto: TWinControl); overload;
procedure SetarFoco(pObjeto: TWinControl; Key: Word); overload;
procedure LimparCampos(pObjetos: array of TControl);

procedure WhereOuAnd(var pSqlAtual: string; pNovoComando: string);
procedure WhereOuOr(var pSqlAtual: string; pNovoComando: string);
procedure VerAnd(var pSqlAtual: string; pNovoComando: string);
procedure AddOrSeNecessario(var pSqlAtual: string; pNovoComando: string);
procedure AddVirgulaSeNecessario(var pSqlAtual: string; pNovoComando: string);

procedure AddNoVetorSemRepetir(var pVetor: TArray<Integer>; pValor: Integer); overload;
procedure AddNoVetorSemRepetir(var pVetor: TArray<string>; pValor: string); overload;
procedure AddNoVetorSemRepetir(var pVetor: TArray<TDateTime>; pValor: TDateTime); overload;
procedure SomenteLeitura(pObjetos: array of TComponent; pHabilitar: Boolean);
procedure Habilitar(pObjetos: array of TControl; pHabilitar: Boolean; pLimpar: Boolean = True);
procedure Visibilidade(pObjetos: TArray<TControl>; pVisivel: Boolean);
procedure VisibilidadeTMenu(pObjetos: TArray<TMenuItem>; pVisivel: Boolean);
procedure Index(pObjetos: array of TControl; pIndex: Integer);

procedure Destruir(var pObjeto: TObject); overload;
procedure Destruir(var pObjeto: TArray<TObject>); overload;

//procedure MarcarDesmarcarTodos(var pObjeto: TGridLuka; Key: Word; Shift: TShiftState; pObjeto1: array of integer; pObjeto2: array of string); overload;
procedure MarcarDesmarcarTodos(var pGrid: TGridLuka; pColunaSelecao: Integer; Key: Word; Shift: TShiftState); overload;
// ---------------- Fun��es ----------------------

function getMenorValor(pValores: TArray<Double>; pApenasMaiorQueZero: Boolean = False): Double;
function EMatrizFilial(pCNPJ1: string; pCNPJ2: string): Boolean;
function FormatarCpfCnpf(pCpfCnpj: string): string;
function FormatarCNPJ(pCnpj: string): string;
function FormatarCEP(pCep: string): string;
function FormatarChaveNFe(pChave: string): string;
function MesAno(pData: TDateTime): string;
function FormatarData(pTexto: string): string;
function GetIndexImpressora(pImpressora: string): Integer;
function getPrimeiroDiaMes: string;
function getDataAtual: string;
function getUltimoDiaMes: string;
function getNumeroPorExtenso(pValor: Integer; pMasculino: Boolean = True): string;
function getIntFloat(pValor: Double): Integer;
function getDinheiroPorExtenso(pValor:Double; pQtdCasasAposVirgula: Integer = 2): string;
function EMultiplo(pValor: Integer; pMultiplo: Integer): Boolean;
function getCNPJ_CPFOk(pCpfCnpj: string): Boolean;
function ToSelecao(pValor: string): string;
function SelecaoToChar(pValor: string): string;
function salvarStringEmAquivo(pString: string; pCaminhoNomeArquivo: string): Boolean;
function carregarAquivoEmString(pCaminhoNomeArquivo: string): string;
function RetornaNomeBanco(pCodigo: Integer): String;


function GetTexto(var pRtext:TRichEdit): string;
procedure SetTexto(var pRtexto: TRichEdit;pTexto: string);

function getValorMulta(
  pValorBase: Double;
  pPercentualMulta: Double;
  pQtdeDiasAtraso: Integer
): Double;

function getValorJuros(
  pValorBase: Double;
  pPercentualJurosMensal: Double;
  pQtdeDiasAtraso: Integer
): Double;

function getTextoPerfeito(pTexto: string; pTamanhoMaximo: Integer): TArray<string>; overload;
function getIdsSelecionados( pGrid: TGridLuka; pColunaSelecao: Integer; pColunaId: Integer; pLinhaInicial: Integer = 1 ): TArray<Integer>;

function NomeComputador: string;
function TemConexaoInternet: Boolean;
function Espacos(pQtde: Word): string;
function EnderecoIPComputador: string;
function DataOk(pData: string): Boolean;
function DataHoraOk(pDataHora: string): Boolean;
function getCasasDecimaisEstoque: Integer;
function MesDoAno(pData: TDateTime): string;
function SimNao(const pValor: string): string;
function DiaDaSemana(pData: TDateTime): string;
function Perguntar(pPergunta: string; pCaption: string = 'Pergunta'): Boolean;
function ToChar(pValor: Boolean): string; overload;
function ToChar(pValor: TCheckBox): string; overload;
function ToChar(pValor: string): string; overload;
function ToData(pValor: string): TDateTime;
function nvl(pValor1: TDateTime; pValor2: Variant): Variant; overload;
function nvl(pValor1: string; pValor2: string): string; overload;
function zvl2(pValores: TArray<Double>): Double;
function ToDataHora(pData: TDateTime; pHora: TDateTime): TDateTime; overload;
function ToDataHora(pData: string): TDateTime; overload;
function TratarErro(pMensagem: string): string;
function ToDateOracle(pData: TEditLukaData): string; overload;
function ToDateOracle(pData: TDateTime): string; overload;
function ToDateBetweenOracle(pDataInicial: TEditLukaData; pDataFinal: TEditLukaData): string; overload;
function ToDateBetweenOracle(pDataInicial: TDateTime; pDataFinal: TDateTime): string; overload;
function AzulVermelho(pSimNao: string): Integer;
function VermelhoAzul(pSimNao: string): Integer;
function RetornaMascaraPai(pTexto: string): string;
function RetornaNumeros(pTexto: string): string; overload;
function RetornaTexto(pTexto: string): string;
function TemTextoENumero(pTexto: string): Boolean;
function RemoverCaracteresEspeciais(pTexto: string): string;
function RetornaNumeros(pTexto: AnsiString): AnsiString; overload;
function RemoveCaracteres(pTexto: string; pChars: TSysCharSet): string;
function Decode(pBase: Variant; pValores: array of Variant): Variant;
function FormatarValorECF(pValor: Double; pDecimais: Byte): AnsiString;
function Arredondar(pValor: Double; pCasasDecimais: Integer): Double;
function RemoverCaracter(const pValor: string; const pCaracter: string): string;
function BlockInput(fBlockInput: Boolean): DWORD; stdcall; external 'user32.dll';
function Em(pComparacao: string; pComparados: array of string): Boolean; overload;
function Em(pComparacao: array of string; pComparados: array of string): Boolean; overload;
function Em(pComparacao: Integer; pComparados: array of Integer): Boolean; overload;
function Em(pComparacao: TDateTime; pComparados: array of TDateTime): Boolean; overload;
function ValidarMultiplo(const pQuantidade: Double; const pMultiplo: Double; permitirZero: Boolean = True; exibirMensagem: Boolean = True): Boolean;
function RetornaValorDeAcordoMultiplo(const pQuantidade: Double; const pMultiplo: Double): Double;
function RPad(pTexto: string; pTamanho: Integer; pCaracter: Char = ' '): string;
function LPad(pTexto: string; pTamanho: Integer; pCaracter: Char = ' '): string;
function CPad(pTexto: string; pTamanho: Integer; pCaracter: Char = ' '): string;

// Fun�oes de convers�o
function NFormat(const pValor: Integer): string; overload;
function NFormatOracle(const pValor: Double): string; overload;
function NFormatN(const pValor: Integer): string; overload;

function NFormatEstoque(const pValor: Double): string; overload;
function NFormatNEstoque(const pValor: Double): string; overload;
function NFormatNEstoqueCasasDecimais(const pValor: Double; pCasasDecimais: Integer = 3): string;

function NFormat(const pValor: Double; pDecimais: Integer = 2): string; overload;
function NFormatN(const pValor: Double; pDecimais: Integer = 2): string; overload;

function DFormat(const pValor: TDateTime): string;
function DFormatN(const pValor: TDateTime): string;

function HFormat(const pValor: TDateTime): string;
function HFormatN(const pValor: TDateTime): string;

function DHFormat(const pValor: TDateTime): string;
function DHFormatN(const pValor: TDateTime): string;

function SFormatInt(const pValor: string): Integer;
function SFormatDouble(const pValor: string): Double;
function SFormatCurr(pValor: string): Currency;

function FiltroInInt(pColuna: string; pFiltros: TArray<Integer>): string;
function FiltroInStr(pColuna: string; pFiltros: TArray<string>): string;
function FiltroInData(pColuna: string; pFiltros: TArray<TDateTime>): string;

function RetirarAcento(c: Char): Char;
function RetirarAcentos(pTexto: string): string;
function LimparTexto(pTexto: string): string;
function SetLinhasGridPorTamanhoVetor(pTamanhoVetor: Integer): Integer;
procedure SetarColunaGridSimNao(pFonte: TFont; pSimNao: string);

function IIf(pCondicao: Boolean; pVerdadeiro: Variant; pFalso: Variant): Variant;
function IIfStr(pCondicao: Boolean; pVerdadeiro: string; pFalso: string = ''): string;
function IIfDbl(pCondicao: Boolean; pVerdadeiro: Double; pFalso: Double = 0): Double;
function IIfInt(pCondicao: Boolean; pVerdadeiro: Integer; pFalso: Integer = 0): Integer;
function IIfData(pCondicao: Boolean; pVerdadeiro: TDateTime; pFalso: TDateTime = 0): TDateTime;

function getCorVerdePadraoAltis: TColor;
function DefinirAlinhamentoGrid(pColuna: Integer; pDireita: TArray<Integer>): TAlignment; overload;
function DefinirAlinhamentoGrid(pColuna: Integer; pDireita: TArray<Integer>; pEsquerda: TArray<Integer>; pCentro: TArray<Integer>): TAlignment; overload;

function getInformacao(pMovimentoId: Integer; pMovimento: string): string; overload;
function getInformacao(pMovimentoId: string; pMovimento: string): string; overload;

function getNomeCaminhoArquivoSalvar(pExtensaoArquivo: string; pDescricaoArquivo: string): string;
function getNomeCaminhoArquivoCarregar(pExtensaoArquivo: string; pDescricaoArquivo: string): string;

procedure GridToExcel(pGrid: TGridLuka); overload;
procedure GridToExcel(pGrid: TGridLuka; pColunas: array of integer); overload;

function Inverter(Caminho : String) : String;

var
  _Versao : String;
const
  co_vermelho_prioridade = $006A6AFF;

  coNumeros: array[0..9] of Integer = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
  coNenhumRegistroEncontrado  = 'Nenhum registro encontrado!';
  coNovoRegistroSucessoCodigo = 'Novo registro inserido com sucesso.' + Chr(13) + Chr(10) + 'C�digo: ';
  coAlteracaoRegistroSucesso  = 'Altera��o de registro realizada com sucesso.';

  cor_abertura     = clBlue;
  cor_suprimento   = clMenuHighlight;
  cor_saida_abert  = $00000082;
  cor_sangria      = clRed;
  cor_entrada_sang = $00FFDD97;
  cor_fechamento   = clOlive;
  cor_recebimento  = $000096DB;
  cor_receb_acumu  = clNavy;
  cor_saida_supri  = $000080FF;
  cor_canc_venda   = clRed;
  cor_ent_fec_cai  = $00FF6F6F;
  cor_saida_bx_tit = $000000D5;
  cor_ent_bx_tit   = $00E87400;
  corEdicaoGrid    = clGreen;

  (* Controle de or�amentos / vendas *)
  co_orcamento_em_aberto    = 'OE';
  co_orcamento_bloqueado    = 'OB';
  coPedidoReceberEntrega    = 'VE';
  coOrcamentoRecebido       = 'RE';
  co_orcamento_cancelado    = 'CA';

  (* Selecionado/N�o selecionado/N�o permite selecao *)
  charSelecionado    = '     ';
  charNaoSelecionado = '          ';
  charNaoPermiteSelecionar = '---';

  (* Cores dos grids *)
  coCorFonteEdicao1 = clBlue;
  coCorCelulaEdicao1 = $00FFF5E1;
  coCorFonteEdicao2 = clGreen;
  coCorCelulaEdicao2 = $00ECFFEC;
  coCorFonteEdicao3 = $000080FF;
  coCorCelulaEdicao3 = $00DFEFFF;
  coCorFonteEdicao4 = clPurple;
  coCorCelulaEdicao4 = $00C184FF;
  coCorFonteEdicaoPadrao = clBlack;
  coCorCelulaEdicaoPadrao = clWindow;
  coCorFonteEdicao5 = clRed;
  coCorCelulaEdicao5 = $FDAE9C;

  coGerenteId     = 1;
  coVendedorId    = 2;
  coCaixaId       = 3;
  coMotoristaId   = 4;

  cbNaoFiltrar = 'NaoFiltrar';

  coDataValidadeVersao = '01/12/2025';

implementation

uses
  Perguntar, Exclamar, Informacao;

procedure NenhumRegistro;
begin
  _Biblioteca.Exclamar('Nenhum registro encontrado!');
end;

procedure RegistroExcluidoSucesso;
begin
  _Biblioteca.Exclamar('Registro exclu�do com sucesso!');
end;

procedure NaoAutorizadoRotina;
begin
  _Biblioteca.Exclamar('Voc� n�o possui autoriza��o para acessar esta rotina!');
end;

procedure NenhumRegistroSelecionado;
begin
  _Biblioteca.Exclamar('Nenhum registro foi selecionado!');
end;

procedure RotinaSucesso;
begin
  Informar('Rotina executada com sucesso.');
end;

procedure RotinaCanceladaUsuario;
begin
  _Biblioteca.Exclamar('Rotina cancelada pelo usu�rio!');
end;

procedure NaoPermitidoConsumidorFinal;
begin
  _Biblioteca.Exclamar('N�o � permitido o uso de consumidor final nesta rotina!');
end;

procedure ImpressoraNaoConfigurada;
begin
  _Biblioteca.Exclamar('Impressora n�o configurada para impress�o deste documento!');
end;

procedure RotinaNaoPermitidaDepositoFechado;
begin
  _Biblioteca.Exclamar('Rotina n�o permitida para empresa cadastrada com dep�sito fechado!');
end;

procedure Exclamar(pMensagem: string);
begin
  CriarExclamacao('Aten��o!', pMensagem);
end;

procedure ImpressaoNaoDisponivel;
begin
  _Biblioteca.Exclamar('Impress�o n�o dispon�vel para estas informa��es!');
end;

procedure Informar(pMensagem: string);
begin
  Informacao.CriarInformacao('Informa��o', pMensagem);
end;

procedure InformarAlteracaoRegistro(pId: Integer);
begin
  if pId > 0 then
    _Biblioteca.Informar(coNovoRegistroSucessoCodigo + NFormat(pId))
  else
    _Biblioteca.Informar(coAlteracaoRegistroSucesso);
end;

procedure Avisar(pTituloAviso: string; pAviso: string);
begin
  // Implementar
  ShowMessage(pAviso);
end;

procedure MensagemPadraoNaoEmissaoNFe;
begin
  _Biblioteca.Exclamar('Falha ao emitir a NFe, "Favor entrar em contato com o suporte"!');
end;

procedure LimparCampos(pObjetos: array of TControl);
var
  i: Integer;
begin
  for i := Low(pObjetos) to High(pObjetos) do begin
    if pObjetos[i] is TCustomEdit then
      TCustomEdit(pObjetos[i]).Clear
    else if pObjetos[i] is TComboBoxEx then
      TComboBoxEx(pObjetos[i]).Text := ''
    else if pObjetos[i] is TCustomListControl then
      TCustomListControl(pObjetos[i]).ItemIndex := -1
    else if pObjetos[i] is TRadioGroup then
      TRadioGroup(pObjetos[i]).ItemIndex := -1
    else if pObjetos[i] is TFrameHerancaPrincipal then
      TFrameHerancaPrincipal(pObjetos[i]).Clear
    else if pObjetos[i] is TGridLuka then
      TGridLuka(pObjetos[i]).ClearGrid
    else if pObjetos[i] is TCheckBox then
      TCheckBox(pObjetos[i]).Checked := False
    else if pObjetos[i] is TPageControl then
      TPageControl(pObjetos[i]).ActivePageIndex := 0
    else if pObjetos[i] is TStaticTextLuka then
      TStaticTextLuka(pObjetos[i]).Clear
    else if pObjetos[i] is TStaticText then
      TStaticText(pObjetos[i]).Caption := ''
    else if pObjetos[i] is TImage then
      TImage(pObjetos[i]).Picture := nil
    else if pObjetos[i] is TFrameHerancaPrincipal then
      TFrameHerancaPrincipal(pObjetos[i]).Clear;
  end;
end;

procedure WhereOuAnd(var pSqlAtual: string; pNovoComando: string);
begin
  if pNovoComando = '' then
    Exit;

  if Pos('where', pSqlAtual) = 0 then
    pSqlAtual := ' where ' + pNovoComando + ' '
  else
    pSqlAtual := pSqlAtual + ' and ' + pNovoComando + ' ';
end;

procedure WhereOuOr(var pSqlAtual: string; pNovoComando: string);
begin
  if pNovoComando = '' then
    Exit;

  if Pos('where', pSqlAtual) = 0 then
    pSqlAtual := ' where ' + pNovoComando + ' '
  else
    pSqlAtual := pSqlAtual + ' or ' + pNovoComando + ' ';
end;

procedure VerAnd(var pSqlAtual: string; pNovoComando: string);
begin
  if Trim(pSqlAtual) = '' then
    pSqlAtual := ' ' + pNovoComando + ' '
  else
    pSqlAtual := pSqlAtual + ' and ' + pNovoComando + ' ';
end;

procedure AddOrSeNecessario(var pSqlAtual: string; pNovoComando: string);
begin
  if Length(pSqlAtual) = 0 then
    pSqlAtual := ' ' + pNovoComando + ' '
  else
    pSqlAtual := pSqlAtual + ' or ' + pNovoComando + ' ';
end;

procedure AddVirgulaSeNecessario(var pSqlAtual: string; pNovoComando: string);
begin
  if Length(pSqlAtual) = 0 then
    pSqlAtual := ' ' + pNovoComando
  else
    pSqlAtual := pSqlAtual + ', ' + pNovoComando;
end;

procedure AddOrSeVazio(var pSqlAtual: string; pNovoComando: string);
begin
  if Pos('or', pSqlAtual) = 0 then
    pSqlAtual := ' ' + pNovoComando
  else
    pSqlAtual := pSqlAtual + ' and ' + pNovoComando;
end;

procedure AddNoVetorSemRepetir(var pVetor: TArray<Integer>; pValor: Integer);
begin
  if Em(pValor, pVetor) then
    Exit;

  SetLength(pVetor, Length(pVetor) + 1);
  pVetor[High(pVetor)] := pValor;
end;

procedure AddNoVetorSemRepetir(var pVetor: TArray<string>; pValor: string);
begin
  if Em(pValor, pVetor) then
    Exit;

  SetLength(pVetor, Length(pVetor) + 1);
  pVetor[High(pVetor)] := pValor;
end;

procedure AddNoVetorSemRepetir(var pVetor: TArray<TDateTime>; pValor: TDateTime); overload;
begin
  if Em(pValor, pVetor) then
    Exit;

  SetLength(pVetor, Length(pVetor) + 1);
  pVetor[High(pVetor)] := pValor;
end;

procedure SomenteLeitura(pObjetos: array of TComponent; pHabilitar: Boolean);
var
  i: Integer;
  j: Integer;
begin
  for i := Low(pObjetos) to High(pObjetos) do begin
    if pObjetos[i] is TCustomEdit then
      TCustomEdit(pObjetos[i]).ReadOnly := pHabilitar
    else if pObjetos[i] is TCustomMaskEdit then
      TCustomMaskEdit(pObjetos[i]).ReadOnly := pHabilitar
    else if pObjetos[i] is TFrameHerancaPrincipal then
      TFrameHerancaPrincipal(pObjetos[i]).SomenteLeitura(pHabilitar)
    else if pObjetos[i] is TPageControl then begin
      for j := 0 to TPageControl(pObjetos[i]).ControlCount -1 do
        SomenteLeitura([TPageControl(pObjetos[i]).Controls[j]], pHabilitar);
    end
    else if pObjetos[i] is TComboBox then
      TComboBox(pObjetos[i]).Enabled := False;
  end;
end;

procedure Habilitar(pObjetos: array of TControl; pHabilitar: Boolean; pLimpar: Boolean = True);
var
  i: Integer;
  x: Integer;
begin
  if pLimpar then
    LimparCampos(pObjetos);

  for i := Low(pObjetos) to High(pObjetos) do begin
    if not(pObjetos[i] is TFrameHerancaPrincipal) then begin
      if pObjetos[i] is TPageControl then begin
        for x := 0 to TPageControl(pObjetos[i]).ControlCount - 1 do
          TControl(TPageControl(pObjetos[i]).Controls[x]).Enabled := pHabilitar;

        TPageControl(pObjetos[i]).Enabled := pHabilitar;
      end
      else if pObjetos[i] is TGroupBoxLuka then begin
        for x := 0 to TGroupBoxLuka(pObjetos[i]).ControlCount - 1 do begin
          TControl(TGroupBoxLuka(pObjetos[i]).Controls[x]).Enabled := pHabilitar;
          if TControl(TGroupBoxLuka(pObjetos[i]).Controls[x]) is TCheckBox then
            TCheckBox(TControl(TGroupBoxLuka(pObjetos[i]).Controls[x])).Checked := False;

          TGroupBoxLuka(pObjetos[i]).Enabled := pHabilitar;
        end;
      end
      else
        pObjetos[i].Enabled := pHabilitar;
    end
    else if (pObjetos[i] is TFrameHerancaPrincipal) then
      TFrameHerancaPrincipal(pObjetos[i]).Modo(pHabilitar, pLimpar);
  end;
end;

procedure Visibilidade(pObjetos: TArray<TControl>; pVisivel: Boolean);
var
  i: Integer;
  x: Integer;
begin
  for i := Low(pObjetos) to High(pObjetos) do begin
    if not(pObjetos[i] is TFrameHerancaPrincipal) then begin
      if pObjetos[i] is TPageControl then begin
        for x := 0 to TPageControl(pObjetos[i]).ControlCount - 1 do
          TControl(TPageControl(pObjetos[i]).Controls[x]).Visible := pVisivel;
      end
      else if pObjetos[i] is TGroupBoxLuka then begin
        for x := 0 to TGroupBoxLuka(pObjetos[i]).ControlCount - 1 do
          TControl(TGroupBoxLuka(pObjetos[i]).Controls[x]).Visible := pVisivel;
      end
      else if pObjetos[i] is TQRLabel  then
        pObjetos[i].Enabled := pVisivel
      else if pObjetos[i] is TQRShape  then
        pObjetos[i].Enabled := pVisivel
      else
        pObjetos[i].Visible := pVisivel;
    end;
  end;
end;

procedure VisibilidadeTMenu(pObjetos: TArray<TMenuItem>; pVisivel: Boolean);
var
  i: Integer;
begin
  for i := Low(pObjetos) to High(pObjetos) do
    pObjetos[i].Visible := pVisivel;
end;

procedure Index(pObjetos: array of TControl; pIndex: Integer);
var
  i: Integer;
begin
  for i := Low(pObjetos) to High(pObjetos) do begin
    if pObjetos[i] is TComboBoxLuka then
      TComboBoxLuka(pObjetos[i]).ItemIndex := pIndex;
  end;
end;

procedure SetarFoco(pObjeto: TWinControl);
var
  vPai: TWinControl;
begin
  vPai := nil;
  while (vPai = nil) or (vPai.HasParent) do begin
    if not Assigned(vPai) then
      vPai := pObjeto.Parent
    else
      vPai := vPai.Parent;

    if not(vPai is TTabSheet) then
      Continue;

    if not TTabSheet(vPai).TabVisible then
      Exit;

    TPageControl(vPai.Parent).ActivePage := TTabSheet(vPai);
  end;

  try
    if pObjeto.CanFocus then
      pObjeto.SetFocus;
  except

  end;
end;

procedure SetarFoco(pObjeto: TWinControl; Key: Word);
begin
  if Key = VK_RETURN then
    SetarFoco(pObjeto);
end;

procedure Destruir(var pObjeto: TObject);
begin
  try
    if pObjeto <> nil then
      FreeAndNil(pObjeto);
  except

  end;
end;

procedure Destruir(var pObjeto: TArray<TObject>);
var
  i: Integer;
begin
  for i := Low(pObjeto) to High(pObjeto) do begin
    try
      FreeAndNil(pObjeto[i]);
    except

    end;
  end;

  try
    pObjeto := nil;
  except

  end;
end;

procedure MarcarDesmarcarTodos(var pGrid: TGridLuka; pColunaSelecao: Integer; Key: Word; Shift: TShiftState); overload;

  procedure MarcaDesmarcaGridTodos;
  var
    i: Integer;
    vMarcar: Boolean;
  begin
    vMarcar := False;
    for i := 1 to pGrid.RowCount -1 do begin
      if pGrid.Cells[pColunaSelecao, pGrid.Row] = charNaoPermiteSelecionar then
        Continue;

      if not Em(pGrid.Cells[pColunaSelecao, pGrid.Row], [charSelecionado, charNaoSelecionado]) then
        Continue;

      vMarcar := pGrid.Cells[pColunaSelecao, pGrid.Row] = charNaoSelecionado;
      Break;
    end;

    for i := 1 to pGrid.RowCount -1 do begin
      if pGrid.Cells[pColunaSelecao, i] = charNaoPermiteSelecionar then
        Continue;

      if not Em(pGrid.Cells[pColunaSelecao, i], [charSelecionado, charNaoSelecionado]) then
        Continue;

      pGrid.Cells[pColunaSelecao, i] := IIfStr(vMarcar, charSelecionado, charNaoSelecionado);
    end;
  end;

begin
  if pGrid.Cells[pColunaSelecao, pGrid.Row] = '' then
    Exit;

  if not Em(pGrid.Cells[pColunaSelecao, pGrid.Row], [charSelecionado, charNaoSelecionado]) then
    Exit;

  if (Shift = [ssCtrl]) and (Key = VK_SPACE) then
    MarcaDesmarcaGridTodos
  else if Key = VK_SPACE then
    pGrid.Cells[pColunaSelecao,pGrid.Row] := IIfStr(pGrid.Cells[pColunaSelecao, pGrid.Row] = charNaoSelecionado, charSelecionado, charNaoSelecionado);
end;

function NomeComputador: string;
var
  flag: dWord;
begin
  flag := MAX_COMPUTERNAME_LENGTH + 1;
  SetLength(Result, flag);
  GetComputerName( PChar(Result), flag );
  Result := string( PChar(Result) );
  Result := Trim( Result );
end;

function TemConexaoInternet: Boolean;
begin
  Result := InternetCheckConnection('http://www.google.com.br/', 1, 0);
end;

function Espacos(pQtde: Word): string;
var
  i: Word;
begin
  Result := '';
  for i := 1 to pQtde do
    Result := Result + ' ';
end;

function EnderecoIPComputador: string;
var
  lHostEnt: PHostEnt;
  lHostName: array[0..128] of char;
  lIPAddress: PAnsiChar;
begin
  GetHostName(@lHostName, 128);
  lHostEnt := GetHostByName(@lHostName);
  lIpAddress := iNet_ntoa(PInAddr(lHostEnt^.h_addr_list^)^);
  Result := String(lIpAddress);
end;

function DataOk(pData: string): Boolean;
begin
  try
    Result := True;
    StrToDate(pData);
  except
    Result := False;
  end;
end;

function DataHoraOk(pDataHora: string): Boolean;
begin
  try
    Result := True;
    StrToDateTime(pDataHora);
  except
    Result := False;
  end;
end;

function getCasasDecimaisEstoque: Integer;
begin
  // Mudar pelo novo par�metro
  Result := 3;
end;

function MesDoAno(pData: TDateTime): string;
begin
  Result := '';
  case MonthOfTheYear(pData) of
     1: Result := '/01/';
     2: Result := '/02/';
     3: Result := '03/';
     4: Result := '/04/';
     5: Result := '/05/';
     6: Result := '/06/';
     7: Result := '/07/';
     8: Result := '/08/';
     9: Result := '/09/';
    10: Result := '/10/';
    11: Result := '/11/';
    12: Result := '/12/';
  end;
end;

function SimNao(const pValor: string): string;
begin
  Result := IfThen(pValor = 'S', 'Sim', 'N�o');
end;

function DiaDaSemana(pData: TDateTime): string;
begin
  Result := '';
  case DayOfTheWeek(pData) of
    1: Result := 'Segunda-Feira';
    2: Result := 'Ter�a-Feira';
    3: Result := 'Quarta-Feira';
    4: Result := 'Quinta-Feira';
    5: Result := 'Sexta-Feira';
    6: Result := 'S�bado';
    7: Result := 'Domingo';
  end;
end;

function Perguntar(pPergunta: string; pCaption: string): Boolean;
var
  vRetTela: TRetornoTelaFinalizar;
begin
  vRetTela := CriarPergunta(pCaption, pPergunta);
  Result := vRetTela.RetTela = trOk;
end;

function ToChar(pValor: Boolean): string;
begin
  Result := IfThen(pValor, 'S', 'N');
end;

function ToChar(pValor: TCheckBox): string; overload;
begin
  Result := ToChar(pValor.Checked);
end;

function ToChar(pValor: string): string; overload;
begin
  Result := Copy(pValor, 1, 1);
end;

function ToData(pValor: string): TDateTime;
var
  vData: string;
begin
  // Removendo a hora de houver
  vData := Copy(pValor, 1, 10);

  Result := StrToDateDef(vData, 0);
end;

function ToDataHora(pData: TDateTime; pHora: TDateTime): TDateTime; overload;
begin
  Result := pData + pHora;
end;

function ToDataHora(pData: string): TDateTime; overload;
begin
  Result := StrToDateTimeDef(pData, 0);
end;

function nvl(pValor1: TDateTime; pValor2: Variant): Variant; overload;
begin
  if pValor1 > 0 then
    Result := pValor1
  else
    Result := pValor2;
end;

function nvl(pValor1: string; pValor2: string): string; overload;
begin
  if pValor1 <> '' then
    Result := pValor1
  else
    Result := pValor2;
end;

function zvl2(pValores: TArray<Double>): Double;
var
  i: Integer;
begin
  Result := 0;
  for i := Low(pValores) to High(pValores) do begin
    Result := pValores[i];
    if NFormatN(Result) <> '' then
      Break;
  end;
end;

function TratarErro(pMensagem: string): string;
begin
  if Pos('ORA-20000', pMensagem) = 0 then
    Result := pMensagem
  else begin
    Result := Copy(pMensagem, Pos('ORA-20000', pMensagem) + 11, Length(pMensagem));
    Result := Copy(Result, 1, Pos('ORA-', Result) - 1);
  end;
end;

function ToDateOracle(pData: TEditLukaData): string; overload;
begin
  Result := ToDateOracle(pData.AsData);
end;

function ToDateOracle(pData: TDateTime): string;
begin
  Result := 'to_date(''' + FormatDateTime('DD/MM/YYYY', pData) + ''', ''DD/MM/YYYY'') ';
end;

function ToDateBetweenOracle(pDataInicial: TEditLukaData; pDataFinal: TEditLukaData): string; overload;
begin
  Result := ToDateBetweenOracle(pDataInicial.AsData, pDataFinal.AsData);
end;

function ToDateBetweenOracle(pDataInicial: TDateTime; pDataFinal: TDateTime): string;
begin
  Result := ' between ' + ToDateOracle(pDataInicial) + ' and ' + ToDateOracle(pDataFinal);
end;

function AzulVermelho(pSimNao: string): Integer;
begin
  Result := IfThen(pSimNao = 'Sim', clBlue, clRed);
end;

function VermelhoAzul(pSimNao: string): Integer;
begin
  Result := IfThen(pSimNao = 'Sim', clRed, clBlue);
end;

function RetornaMascaraPai(pTexto: string): string;
var
  i: Integer;
  x: TStringDynArray;
begin
  Result := '';
  x := SplitString(pTexto, '.');
  for i := Low(x) to High(x) do begin
    if i = High(x) then
      Break;

    Result := Result + IfThen(i in[1,2], '.') + LPad(x[i], 3, '0');
  end;
end;

function RetornaNumeros(pTexto: string): string;
var
  i: Integer;
begin
  Result := '';
  for i := 1 to Length(pTexto) do begin
    if CharInSet(pTexto[i], ['0'..'9']) then
      Result := Result + pTexto[i];
  end;
end;

function RetornaTexto(pTexto: string): string;
var
  i: Integer;
begin
  Result := '';
  for i := 1 to Length(pTexto) do begin
    if CharInSet(pTexto[i], ['A'..'Z']) then
      Result := Result + pTexto[i];
  end;
end;

function TemTextoENumero(pTexto: string): Boolean;
begin
  Result := (RetornaTexto(pTexto) <> '') and (RetornaNumeros(pTexto) <> '');
end;

function RemoverCaracteresEspeciais(pTexto: string): string;
var
  i: Integer;

const
  co_caracteres_especiais: array[1..47] of string[1] = (
    '�','�','�','�','�',
    '�','�','�','�','�',
    '�', '�',
    '�','�','�','�',
    '�','�','�','�',
    '�','�','�','�',
    '�','�','�','�',
    '�','�','�','�','�',
    '�','�','�','�','�',
    '�','�','�','�',
    '�','�','�','�',
    '&'
  );

  co_subst_carac_especiais: array[1..47] of string[1] = (
    'A','A','A','A','A',
    'a','a','a','a','a',
    'C', 'c',
    'E','E','E','E',
    'e','e','e','e',
    'I','I','I','I',
    'i','i','i','i',
    'O','O','O','O','O',
    'o','o','o','o','o',
    'U','U','U','U',
    'u','u','u','u',
    'e'
  );

begin
  Result := '';
  pTexto := Trim(pTexto);

  if pTexto = '' then
    Exit;

  for i := Low(co_caracteres_especiais) to High(co_caracteres_especiais) do
     pTexto := StringReplace(pTexto, string(co_caracteres_especiais[i]), string(co_subst_carac_especiais[i]), [rfreplaceall]);

  pTexto := RemoveCaracteres(pTexto, [chr(9)]);
  Result := Trim(pTexto);
end;

function RetornaNumeros(pTexto: AnsiString): AnsiString;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to Length(pTexto) do begin
    if CharInSet(pTexto[i], ['0'..'9']) then
      Result := Result + pTexto[i];
  end;
end;

function RemoveCaracteres(pTexto: string; pChars: TSysCharSet): string;
var
  i : Integer;
begin
  Result := '';
  for i := Length(pTexto) downto 1 do begin
    if CharInSet(pTexto[i], pChars) then
      Delete(pTexto, i, 1);
  end;

  Result := pTexto;
end;

function Decode(pBase: Variant; pValores: array of Variant): Variant;
var
  i: Integer;
  EPar: Boolean;
  MetadeItens: Integer;
begin
  EPar := (High(pValores) + 1) mod 2 = 0;
  if EPar then
    Result := ''
  else
    Result := pValores[High(pValores)];

  MetadeItens := (High(pValores) - 1) div 2;

  for i := 0 to MetadeItens do begin
    if pBase = pValores[i * 2] then begin
      Result := pValores[i * 2 + 1];
      Break;
    end;
  end;
end;

function FormatarValorECF(pValor: Double; pDecimais: Byte): AnsiString;
begin
  Result := '';
  case pDecimais of
    2: Result := AnsiString( FormatFloat('#0.00', pValor) );
    3: Result := AnsiString( FormatFloat('#0.000', pValor) );
  end;
end;

function RemoverCaracter(const pValor: string; const pCaracter: string): string;
begin
  Result := StringReplace(pValor, pCaracter, '', [rfReplaceAll]);
end;

function Arredondar(pValor: Double; pCasasDecimais: Integer): Double;
var
  fator, fracao: Extended;
begin
  {Eleva o Valor 10 ao expoente mandado na variavel casasDecimais}
  fator := IntPower(10, pCasasDecimais);
  { Multiplica o valor pelo fator e faz a conversao de string e depois para float novamente para evitar arredondamentos. }
  pValor := StrToFloat(FloatToStr(pValor * fator));
  {Pega a Parte Inteira do Numero}
  Result := Int(pValor);
  {Pega a Parte Fracionaria}
  fracao := Frac(pValor);

  {Faz a regra de arredondamento}
  if fracao >= 0.5 then
    Result := Result + 1
  else if fracao <= -0.5 then
    Result := Result - 1;

  {O valor Final inteiro divide por 100 para transformar em decimal novamente.}
  Result := Result / fator;
end;

function Em(pComparacao: string; pComparados: array of string): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Low(pComparados) to High(pComparados) do begin
    if pComparacao = pComparados[i] then begin
      Result := True;
      Break;
    end;
  end;
end;

function Em(pComparacao: array of string; pComparados: array of string): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Low(pComparacao) to High(pComparacao) do begin
    if Em(pComparacao[i], pComparados) then begin
      Result := True;
      Break;
    end;
  end;
end;

function Em(pComparacao: Integer; pComparados: array of Integer): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Low(pComparados) to High(pComparados) do begin
    if pComparacao = pComparados[i] then begin
      Result := True;
      Break;
    end;
  end;
end;

function Em(pComparacao: TDateTime; pComparados: array of TDateTime): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Low(pComparados) to High(pComparados) do begin
    if pComparacao = pComparados[i] then begin
      Result := True;
      Break;
    end;
  end;
end;

function ValidarMultiplo(const pQuantidade: Double; const pMultiplo: Double; permitirZero: Boolean = True; exibirMensagem: Boolean = True): Boolean;
var
  vResultado: Double;
  vSugestao: Double;

  vQuantidade: Currency;
  vMultiplo: Currency;
begin
  Result := True;

  if (pQuantidade = 0) or (pMultiplo = 0) then begin
    if not permitirZero then
      Result := False;

    Exit;
  end;

  vQuantidade := pQuantidade;
  vMultiplo := pMultiplo;

  if vMultiplo > vQuantidade then begin

    if exibirMensagem then begin
      _Biblioteca.Exclamar(
        'A quantidade informada � menor que seu m�lt�plo!' + Chr(13) + chr(10) +
        'Quantidade informada: ' + NFormat(pQuantidade, 4) + Chr(13) + chr(10) +
        'M�ltiplo: ' + NFormat(pMultiplo, 4)
      );
    end;

    Result := False;
    Exit;
  end;

  vResultado := pQuantidade / pMultiplo;
  try
    StrToInt( CurrToStr(vResultado) );
  except
    Result := False
  end;

  if Result then
    Exit;

  if exibirMensagem then begin
    vSugestao := (Int(vResultado) + 1) * pMultiplo;
    _Biblioteca.Exclamar(
      'A quantidade definida n�o se ajusta ao m�ltiplo do produto!' + Chr(13) + Chr(10) +
      'Quantidade sugerida: ' + NFormat(vSugestao, 4)
    );
  end;
end;

function RetornaValorDeAcordoMultiplo(const pQuantidade: Double; const pMultiplo: Double): Double;
var
  vResultado: Double;
  vSugestao: Double;
begin
  Result := pQuantidade;
  if (pQuantidade = 0) or (pMultiplo = 0) then
    Exit;

  vResultado := pQuantidade / pMultiplo;
  try
    vSugestao := StrToInt( CurrToStr(vResultado) );
  except
    vSugestao := 0;
  end;

  if vSugestao > 0 then
    Exit;

  vSugestao := (Int(vResultado) + 1) * pMultiplo;
  Result := vSugestao;
end;

function RPad(pTexto: string; pTamanho: Integer; pCaracter: Char = ' '): string;
var
  i: Integer;
begin
  Result := Copy(pTexto, 1, pTamanho);
  for i := 1 to pTamanho - Length(pTexto) do
    Result := Result + pCaracter;
end;

function LPad(pTexto: string; pTamanho: Integer; pCaracter: Char = ' '): string;
var
  i: Integer;
begin
  Result := Copy(pTexto, 1, pTamanho);
  for i := 1 to pTamanho - Length(pTexto) do
    Result := pCaracter + Result;
end;

function CPad(pTexto: string; pTamanho: Integer; pCaracter: Char = ' '): string;
var
  i: Integer;
begin
  Result := Copy(pTexto, 1, pTamanho);
  for i := 1 to pTamanho - Length(pTexto) do begin
    if i mod 2 = 1 then
      Result := Result + pCaracter
    else
      Result := pCaracter + Result;
  end;
end;

function BuscarVersaoSistema: string;
type
  PFFI = ^vs_FixedFileInfo;
var
  vCaminhoAplicacao: String;
  F: PFFI;
  Handle: DWORD;
  Len: LongInt;
  Data: PChar;
  Buffer: Pointer;
  vTamanho: DWORD;
  vAplicacao: PChar;
begin
  vCaminhoAplicacao := Application.ExeName;
  vAplicacao := StrAlloc(Length(vCaminhoAplicacao) + 1);
  StrPcopy(vAplicacao, vCaminhoAplicacao);
  Len := GetFileVersionInfoSize(vAplicacao, Handle);
  Result := '';
  if Len > 0 then begin
    Data := StrAlloc(Len + 1);
    if GetFileVersionInfo(vAplicacao, Handle, Len, Data) then begin
      VerQueryValue(Data, '\', Buffer, vTamanho);
      F := PFFI(Buffer);
      Result :=
        Format(
          //'%d.%d.%d.%d',
          _Versao, // - MUDAR A COMPILACAO MANUAL
          [
            HiWord(F^.dwFileVersionMs),
            LoWord(F^.dwFileVersionMs),
            HiWord(F^.dwFileVersionLs),
            Loword(F^.dwFileVersionLs)
          ]
        );
    end;
    StrDispose(Data);
  end;
  StrDispose(vAplicacao);
end;

function NFormat(const pValor: Double; pDecimais: Integer = 2): string;
begin
  case pDecimais of
    0: Result := FormatFloat('#,##0', pValor);
    1: Result := FormatFloat('#,##0.0', pValor);
    2: Result := FormatFloat('#,##0.00', pValor);
    3: Result := FormatFloat('#,##0.000', pValor);
    4: Result := FormatFloat('#,##0.0000', pValor);
    5: Result := FormatFloat('#,##0.00000', pValor);
    6: Result := FormatFloat('#,##0.000000', pValor);
    7: Result := FormatFloat('#,##0.0000000', pValor);
    8: Result := FormatFloat('#,##0.00000000', pValor);
    9: Result := FormatFloat('#,##0.000000000', pValor);
    10: Result := FormatFloat('#,##0.0000000000', pValor);
  end;
end;

function NFormatN(const pValor: Double; pDecimais: Integer = 2): string;
begin
  if pValor = 0 then
    Result := ''
  else
    Result := NFormat(pValor, pDecimais);
end;

function NFormatEstoque(const pValor: Double): string;
begin
  Result := NFormat(pValor, getCasasDecimaisEstoque);
end;

function NFormatNEstoque(const pValor: Double): string;
begin
  Result := NFormatN(pValor, getCasasDecimaisEstoque);
end;

function NFormatNEstoqueCasasDecimais(const pValor: Double; pCasasDecimais: Integer = 3): string;
begin
  Result := NFormatN(pValor, pCasasDecimais);
end;

function DFormat(const pValor: TDateTime): string;
begin
  Result := FormatDateTime('dd/mm/yyyy', pValor);
end;

function DFormatN(const pValor: TDateTime): string;
begin
  if pValor = 0 then
    Result := ''
  else
    Result := DFormat(pValor);
end;

function HFormat(const pValor: TDateTime): string;
begin
  Result := FormatDateTime('hh:nn:ss', pValor);
end;

function HFormatN(const pValor: TDateTime): string;
begin
  if pValor = 0 then
    Result := ''
  else
    Result := HFormat(pValor);
end;

function DHFormat(const pValor: TDateTime): string;
begin
  Result := DFormat(pValor) + ' ' + HFormat(pValor);
end;

function DHFormatN(const pValor: TDateTime): string;
begin
  if pValor = 0 then
    Result := ''
  else
    Result := DFormat(pValor) + ' ' + HFormat(pValor);
end;

function SFormatInt(const pValor: string): Integer;
var
  vValor: string;
begin
  vValor := StringReplace(pValor, '.', '', [rfReplaceAll]);
  Result := StrToIntDef(vValor, 0);
end;

function SFormatDouble(const pValor: string): Double;
var
  vValor: string;
begin
  vValor := StringReplace(pValor, '.', '', [rfReplaceAll]);
  Result := StrToFloatDef(vValor, 0);
end;

function NFormat(const pValor: Integer): string;
begin
  Result := FormatFloat('#,##0', pValor);
end;

function NFormatOracle(const pValor: Double): string; overload;
var
  vFormat: TFormatSettings;
begin
  vFormat.DecimalSeparator := '.';
  Result := FormatFloat('###.00', pValor, vFormat);
end;

function NFormatN(const pValor: Integer): string;
begin
  if pValor = 0 then
    Result := ''
  else
    Result := IntToStr(pValor);
end;

function SFormatCurr(pValor: string): Currency;
begin
  pValor := StringReplace(pValor, '.', '', [rfReplaceAll]);
  Result := StrToFloatDef(pValor, 0);
end;

function FiltroInInt(pColuna: string; pFiltros: TArray<Integer>): string;
var
	i: integer;
begin
  if Length(pFiltros) = 1 then begin
    Result := ' ' + pColuna + ' = ' + IntToStr(pFiltros[Low(pFiltros)]) + ' ';
    Exit;
  end;

  Result := ' (' + pColuna + ' in (';
  for i := Low(pFiltros) to High(pFiltros) do begin
  	if i mod 1000 = 0 then begin
    	if i > 0 then begin
    		Delete(Result, Length(Result), 1);
      	Result := Result + ') or ' + pColuna + ' in (';
      end;
    end;

    if i mod 30 = 0 then
      Result := Result + #13;

    Result := Result + IntToStr(pFiltros[i]) + ',';
  end;
  Delete(Result, Length(Result), 1);
  Result := Result + ')) ';
end;

function FiltroInStr(pColuna: string; pFiltros: TArray<string>): string;
var
	i: integer;
begin
  Result := ' (' + pColuna + ' in (';

  for i := Low(pFiltros) to High(pFiltros) do begin

  	if i mod 1000 = 0 then begin
    	if i > 0 then begin
    		Delete(Result, Length(Result), 1);
      	Result := Result + ') or ' + pColuna + ' in (';
      end;
    end;

    if i mod 30 = 0 then
      Result := Result + #13;

    Result := Result + '''' + pFiltros[i] + ''',';
  end;
  Delete(Result, Length(Result), 1);
  Result := Result + ')) ';
end;

function FiltroInData(pColuna: string; pFiltros: TArray<TDateTime>): string;
var
	i: integer;
begin
  Result := ' (' + pColuna + ' in (';

  for i := Low(pFiltros) to High(pFiltros) do begin

  	if i mod 1000 = 0 then begin
    	if i > 0 then begin
    		Delete(Result, Length(Result), 1);
      	Result := Result + ') or ' + pColuna + ' in (';
      end;
    end;

    if i mod 30 = 0 then
      Result := Result + #13;

    Result := Result + ToDateOracle(pFiltros[i]) + ',';
  end;
  Delete(Result, Length(Result), 1);
  Result := Result + ')) ';
end;

function RetirarAcento(C: Char) : Char;
begin
  case AnsiUpperCase(C)[1] of
    '�','�','�','�','�': Result := IfThen(IsCharUpper(C),'A','a')[1];
    '�','�','�','�': Result := IfThen(IsCharUpper(C),'E','e')[1];
    '�','�','�','�': Result := IfThen(IsCharUpper(C),'I','i')[1];
    '�','�','�','�','�': Result := IfThen(IsCharUpper(C),'O','o')[1];
    '�','�','�','�': Result := IfThen(IsCharUpper(C),'U','u')[1];
    '�': Result := IfThen(IsCharUpper(C),'C','c')[1];
    '�': Result := 'o';
    '�': Result := 'a';
  else
    Result := C;
  end;
end;

function RetirarAcentos(pTexto: string): string;
var
  i: Integer;
begin
  Result := '';
  for i := 1 to Length(pTexto) do
    Result := Result + RetirarAcento(pTexto[i]);
end;

function LimparTexto(pTexto: string): string;
var
  i: integer;
begin
  Result := '';
  pTexto := RetirarAcentos(pTexto);

  for i := 1 to Length(pTexto) do begin
    if
      CharInSet(
        pTexto[i], [
          '0'..'9', 'A'..'Z', 'a'..'z', '!', '@', '#', '$', '%', '&', '(', ')',
          '*', '/' , '-', '_', '+', '-', '=', '.', ',', '<', '>', ':', ';', '?',
          '[', ']', '{', ' '
        ]
      )
    then
      Result := Result + pTexto[i];
  end;
end;

function IIf(pCondicao: Boolean; pVerdadeiro: Variant; pFalso: Variant): Variant;
begin
  if pCondicao then
    Result := pVerdadeiro
  else
    Result := pFalso;
end;

function IIfStr(pCondicao: Boolean; pVerdadeiro: string; pFalso: string): string;
begin
  if pCondicao then
    Result := pVerdadeiro
  else
    Result := pFalso;
end;

function IIfDbl(pCondicao: Boolean; pVerdadeiro: Double; pFalso: Double): Double;
begin
  if pCondicao then
    Result := pVerdadeiro
  else
    Result := pFalso;
end;

function IIfInt(pCondicao: Boolean; pVerdadeiro: Integer; pFalso: Integer): Integer;
begin
  if pCondicao then
    Result := pVerdadeiro
  else
    Result := pFalso;
end;

function IIfData(pCondicao: Boolean; pVerdadeiro: TDateTime; pFalso: TDateTime = 0): TDateTime;
begin
  if pCondicao then
    Result := pVerdadeiro
  else
    Result := pFalso;
end;

{ TRetornoTelaFinalizar }

function TRetornoTelaFinalizar.BuscaCancelada: Boolean;
begin
  Result := RetTela <> trOk;
end;

procedure TRetornoTelaFinalizar.Iniciar;
begin
  RetTela := trCancelado;
end;

function TRetornoTelaFinalizar.Ok(const pResultModal: TModalResult; pMostrarMensagemSeCancelado: Boolean = False): Boolean;
begin
  Result := (pResultModal = mrOk);

  // Faz a convers�o para o tipo que utilizo ao inv�s do Delphi
  case pResultModal of
    mrOk: RetTela := trOk;
    mrCancel: RetTela := trCancelado;
  end;

  if pMostrarMensagemSeCancelado and (RetTela = trCancelado) then
    RotinaCanceladaUsuario;
end;

{ TRetornoTelaFinalizar<T> }

function TRetornoTelaFinalizar<T>.BuscaCancelada: Boolean;
begin
  Result := RetTela = trCancelado;
end;

procedure TRetornoTelaFinalizar<T>.Iniciar;
begin
  RetTela := trCancelado;
end;

function TRetornoTelaFinalizar<T>.Ok(const pResultModal: TModalResult; pMostrarMensagemSeCancelado: Boolean = False): Boolean;
begin
  Result := (pResultModal = mrOk);

  // Faz a convers�o para o tipo que utilizo ao inv�s do Delphi
  case pResultModal of
    mrOk: RetTela := trOk;
    mrCancel: RetTela := trCancelado;
  end;

  if pMostrarMensagemSeCancelado and (RetTela = trCancelado) then
    RotinaCanceladaUsuario;
end;

function TRetornoTelaFinalizar<T>.OkPesquisaAvancada(
  const pResultModal: TModalResult;
  pMostrarMensagemSeCancelado: Boolean;
  pGrid: TGridLuka;
  var pDados: TArray<t>
): Boolean;
begin
  if Ok(pResultModal, pMostrarMensagemSeCancelado) then
    Dados := pDados[pGrid.Row -1];
end;

function SetLinhasGridPorTamanhoVetor(pTamanhoVetor: Integer): Integer;
begin
  Result := IfThen(pTamanhoVetor > 1, pTamanhoVetor + 1, 2);
end;

procedure SetarColunaGridSimNao(pFonte: TFont; pSimNao: string);
begin
  pFonte.Style := [fsBold];
  pFonte.Color := _Biblioteca.AzulVermelho(pSimNao);
end;

function getCorVerdePadraoAltis: TColor;
begin
  Result := RGB(219, 150, 0);
end;

function DefinirAlinhamentoGrid(pColuna: Integer; pDireita: TArray<Integer>): TAlignment; overload;
begin
  Result := DefinirAlinhamentoGrid(pColuna, pDireita, nil, nil);
end;

function DefinirAlinhamentoGrid(pColuna: Integer; pDireita: TArray<Integer>; pEsquerda: TArray<Integer>; pCentro: TArray<Integer>): TAlignment; overload;
begin
  if Em(pColuna, pDireita) then
    Result := taRightJustify
  else if Em(pColuna, pCentro) then
    Result := taCenter
  else
    Result := taLeftJustify;
end;

function getInformacao(pMovimentoId: Integer; pMovimento: string): string;
begin
  Result := '';
  if pMovimentoId > 0 then
    Result := NFormat(pMovimentoId) + ' - ' + pMovimento;
end;

function getInformacao(pMovimentoId: string; pMovimento: string): string;
begin
  Result := '';
  if pMovimentoId <> '' then
    Result := pMovimentoId + ' - ' + pMovimento;
end;

function getMenorValor(pValores: TArray<Double>; pApenasMaiorQueZero: Boolean = False): Double;
var
  i: Integer;
  vMenorValor: Double;
begin
  if pValores = nil then begin
    Result := 0;
    Exit;
  end;

  vMenorValor := 9999999999999;
  for i := Low(pValores) to High(pValores) do begin
    if pApenasMaiorQueZero and (NFormatN(pValores[i]) = '') then
      Continue;

    if pValores[i] < vMenorValor then
      vMenorValor := pValores[i];
  end;

  if vMenorValor = 9999999999999 then
    vMenorValor := 0;

  Result := vMenorValor;
end;

function EMatrizFilial(pCNPJ1: string; pCNPJ2: string): Boolean;
begin
  Result := Copy(RetornaNumeros(pCNPJ1), 8) = Copy(RetornaNumeros(pCNPJ2), 8);
end;

function FormatarCpfCnpf(pCpfCnpj: string): string;
var
  i: Integer;
begin
  Result := '';
  for i := 1 to length(pCpfCnpj) do begin
    if Length(pCpfCnpj) = 11 then begin
      if i in[4,7] then
        Result := Result + '.'
      else if i = 10 then
        Result := Result + '-';
    end
    else begin
      if i in[3,6] then
        Result := Result + '.'
      else if i = 9 then
        Result := Result + '/'
      else if i = 13 then
        Result := Result + '-';
    end;

    Result := Result + pCpfCnpj[i];
  end;
end;

function FormatarCNPJ(pCnpj: string): string;
var
  i: Integer;
begin
  Result := '';
  for i := 1 to length(pCNPJ) do begin
    if i in[3,6] then
      Result := Result + '.'
    else if i = 9 then
      Result := Result + '/'
    else if i = 13 then
      Result := Result + '-';

    Result := Result + pCnpj[i];
  end;
end;

function FormatarCEP(pCep: string): string;
var
  i: Integer;
begin
  Result := '';
  for i := 1 to length(pCep) do begin
    if i = 6 then
      Result := Result + '-';

    Result := Result + pCep[i];
  end;
end;

function FormatarChaveNFe(pChave: string): string;
var
  i: Integer;
begin
  Result := '';
  pChave := RetornaNumeros(pChave);
  for i := 1 to length(pChave) do begin
    if i in[5, 9, 13, 17, 21, 25, 29, 33, 37, 41] then
      Result := Result + '.';

    Result := Result + pChave[i];
  end;
end;

function MesAno(pData: TDateTime): string;
begin
  Result := FormatDateTime('MM/YYYY', pData);
end;

function FormatarData(pTexto: string): string;
var
  vDia: Integer;
  vMes: Integer;
  vAno: Integer;
  vData: string;
begin
  vData := pTexto;

  vDia := StrToIntDef(Copy(vData, 1, 2), 0);
  vMes := StrToIntDef(Copy(vData, 4, 2), 0);
  vAno := StrToIntDef(Copy(vData, 7, 4), 0);

  if vDia > 0 then begin
    if vMes = 0 then
      vMes := StrToIntDef(FormatDateTime('MM', Now), 0);

    if vAno = 0 then
      vAno := StrToIntDef(FormatDateTime('YYYY', Now), 0);
  end;

  if vAno < 999 then begin
    if (vAno > 50) and (vAno < 100) then
      vAno := vAno + 1900
    else
      vAno := vAno + 2000;
  end;

  vData := FormatFloat('00', vDia) + '/' + FormatFloat('00', vMes) + '/' + FormatFloat('0000', vAno);

  try
    StrToDate(vData);
    Result := vData;
  except
    Result := '';
  end;
end;

function GetIndexImpressora(pImpressora: string): Integer;
begin
  Result := Vcl.Printers.Printer.Printers.IndexOf(pImpressora);
end;

function getPrimeiroDiaMes: string;
begin
  Result := FormatDateTime('01/mm/yyyy', Now);
end;

function getDataAtual: string;
begin
  Result := FormatDateTime('dd/mm/yyyy', Now);
end;

function getUltimoDiaMes: string;
begin
  Result := FormatDateTime('dd/mm/yyyy', EndOfTheMonth(Now));
end;

function getNumeroPorExtenso(pValor: Integer; pMasculino: Boolean = True): string;
const
  veMaxPos: Byte = 5;
  mStr: array[0..2] of string[5] = ('trilh', 'bilh', 'milh');
var
	i: Byte;
  ValorStr, s: String;
  ValorHum: Boolean;
  N: Variant;
  Extenso: string;

	function ProcessaUnidade(n1: Byte): string;
  const
  	UnidStrM: array[0..9] of string = ('um', 'um', 'dois', 'tr�s', 'quatro',
    	'cinco', 'seis', 'sete', 'oito', 'nove');
  	UnidStrF: array[0..9] of string = ('um', 'uma', 'duas', 'tr�s', 'quatro',
    	'cinco', 'seis', 'sete', 'oito', 'nove');
  begin
  	if (n1 = 1) and ValorHum then begin
    	if pMasculino then
	    	Result := ' ' + UnidStrM[0]
      else
	    	Result := ' ' + UnidStrF[0];
    end else begin
    	if pMasculino then
	    	Result := ' ' + UnidStrM[n1]
      else
	    	Result := ' ' + UnidStrF[n1];
    end;
  end;

	function ProcessaDezena(n1, n2: Byte): string;
  const
  	DezStrDec: array[1..9] of string = ('onze', 'doze', 'treze', 'quatorze',
			'quinze', 'dezesseis', 'dezessete', 'dezoito', 'dezenove');
  	DezStrInt: array[1..9] of string = ('dez', 'vinte', 'trinta', 'quarenta',
			'cinq�enta', 'sessenta', 'setenta', 'oitenta', 'noventa');
  var Extenso: string;
  begin
  	Extenso := '';
  	if (n1 = 1) and (n2 > 0) then
    	Extenso := ' ' + DezStrDec[StrToInt(IntToStr(n1) + IntToStr(n2)) - 10]
    else begin
    	if n1 > 0 then
      	Extenso := ' ' + DezStrInt[n1] + iif(n2 > 0, ' e', '');
      if n2 > 0 then
      	Extenso := Extenso + ProcessaUnidade(n2);
    end;
    Result := Extenso;
  end;

	function ProcessaCentena(n1, n2, n3: Byte): string;
  const
  	CentStrM: array[0..9] of string = ('cento', 'cem', 'duzentos', 'trezentos',
			'quatrocentos', 'quinhentos', 'seiscentos', 'setecentos',
      'oitocentos', 'novecentos');
  	CentStrF: array[0..9] of string = ('cento', 'cem', 'duzentas', 'trezentos',
			'quatrocentas', 'quinhentas', 'seiscentas', 'setecentas',
      'oitocentas', 'novecentas');
  var Extenso: string;
  begin
  	Extenso := '';
  	if n1 > 0 then begin
    	if (n1 = 1) and ((n2 > 0) or (n3 > 0)) then begin
      	if pMasculino then
	      	Extenso := ' ' + CentStrM[0]
        else
	      	Extenso := ' ' + CentStrF[0];
      end
      else begin
      	if pMasculino then
	      	Extenso := ' ' + CentStrM[n1]
        else
	      	Extenso := ' ' + CentStrF[n1]
      end;
      if (n2 > 0) or (n3 > 0) then Extenso := Extenso + ' e';
    end;
    Extenso := Extenso + ProcessaDezena(n2, n3);
    Result := Extenso;
  end;

begin
	Extenso := '';
  ValorHum := False;
  pValor := Abs(pValor);
	if pValor > 0 then begin
		ValorStr := IntToStr(pValor);
		for i := Length(ValorStr) to ((veMaxPos * 3) - 1) do
    	ValorStr := '0' + ValorStr;
		N := VarArrayCreate([0, veMaxPos - 1], varOleStr);
    for i := 0 to (veMaxPos - 1) do begin
    	N[i] := Copy(ValorStr, (i * 3) + 1, 3);
	  	if not ValorHum then ValorHum := StrToInt(N[i]) = 1;
     	s := N[i];
      if StrToInt(s) > 0 then begin
      	if (i = (veMaxPos - 1)) and (Length(Extenso) > 0) then
        	Extenso := Extenso + ' e';
      	Extenso := Extenso +
        	ProcessaCentena(StrToInt(s[1]), StrToInt(s[2]), StrToInt(s[3]));
        case i of
        	0..2: Extenso := Extenso + ' ' + string(mStr[i]) + iif(StrToInt(s) = 1, '�o', '�es') + ',';
          3: Extenso := Extenso + ' mil';
        end;
      end;
    end;
  end;
  Result := TrimLeft(Extenso);
end;

function getIntFloat(pValor: Double): Integer;
var
  vValorString: string;
begin
  vValorString := FloatToStr(pValor);
  if Pos(',', vValorString) = 0 then
    Result := SFormatInt( vValorString )
  else
    Result := SFormatInt( Copy(vValorString, 1, pos(',', vValorString) - 1) );
end;

function getDinheiroPorExtenso(pValor:Double; pQtdCasasAposVirgula: Integer = 2): string;
var
  vTexto: string;
  vInteiro: Integer;
  vDecimais: Integer;
begin
  vInteiro  := GetIntFloat(pValor);
  pValor    := decode(pQtdCasasAposVirgula,[2, 100, 3, 1000, 4, 10000]) * (pValor - vInteiro);
  vDecimais := StrToInt( FormatFloat('#0',pValor) );

  Result := '';
  if vInteiro > 0 then
    Result := Result + getNumeroPorExtenso(vInteiro) + ' rea' + IIfStr(vInteiro = 1, 'l', 'is');

  if vDecimais > 0 then begin
    if vInteiro > 0 then
      Result := Result + ' e ';
    if vDecimais = 1 then
      vTexto := ''
    else
      vTexto := 's';

    Result := Result + getNumeroPorExtenso(vDecimais) + ' ' +
      Decode(pQtdCasasAposVirgula,[2,'centavo' + vTexto,3,'mil�simo' + vTexto + ' de real',4,'d�cimo' + vTexto + ' de mil�simo de real']);
  end;
end;

function EMultiplo(pValor: Integer; pMultiplo: Integer): Boolean;
begin
  Result := (pValor mod pMultiplo) = 0;
end;

function getCNPJ_CPFOk(pCpfCnpj: string): Boolean;
var
  vNumeros: string;
  vDigito1: string;
  vDigito2: string;

  s: Integer;
  vCont: Integer;
  vDigito: Integer;
  vSoma: Integer;

  function ValidarSequencia: Boolean;
  var
    i: Integer;
    j: Integer;
    s: string;
    vPontoInicial: Integer;
  begin
    Result := True;
    s := vNumeros;
    vPontoInicial := 0;
    if Length(vNumeros) = 11 then
      vPontoInicial := 1;

    for i := vPontoInicial to 9 do begin
      for j := 1 to Length(s) do begin
        if s[j] <> IntToStr(i) then
          Break
        else begin
          if ( s[j] = IntTostr(i) ) and ( j = Length(s) ) then begin
            Result := False;
            Exit;
          end;
        end;
      end;
    end;
  end;

begin
  vNumeros := _Biblioteca.RetornaNumeros(pCpfCnpj);

  if not ValidarSequencia then begin
    Result := false;
    Exit;
  end;

  case Length(vNumeros) of
    11: begin
      vCont :=1;
      vSoma := 0;
      for S := 9 downto 1 do begin
        vCont := vCont + 1;
        vSoma := vSoma + StrToInt(Copy(vNumeros, s, 1)) * vCont;
      end;
      vSoma := vSoma * 10;
      vDigito1 := IntToStr(vSoma mod 11);

      if SFormatInt(vDigito1) >= 10 then
        vDigito1 := '0';

      vCont := 1;
      vSoma := 0;
      for S := 10 downto 1 do begin
        vCont := vCont + 1;
        vSoma := vSoma + SFormatInt(Copy(vNumeros, s, 1)) * vCont;
      end;
      vSoma := vSoma * 10;
      vDigito2 := IntToStr(vSoma mod 11);
      if SFormatDouble(vDigito2) >= 10 then
        vDigito2 := '0';
    end;

    14: begin
      vSoma :=
        5 * SFormatInt(vNumeros[ 1]) + 4 * SFormatInt(vNumeros[ 2]) + 3 * SFormatInt(vNumeros[3]) +
        2 * SFormatInt(vNumeros[ 4]) + 9 * SFormatInt(vNumeros[ 5]) + 8 * SFormatInt(vNumeros[6]) +
        7 * SFormatInt(vNumeros[ 7]) + 6 * SFormatInt(vNumeros[ 8]) + 5 * SFormatInt(vNumeros[9]) +
        4 * SFormatInt(vNumeros[10]) + 3 * SFormatInt(vNumeros[11]) + 2 * SFormatInt(vNumeros[12]);

      vDigito := vSoma mod 11;
      if vDigito > 1 then
        vDigito := 11 - vDigito
      else
        vDigito := 0;

      vDigito1 := NFormat(vDigito);

      vSoma :=
        6 * SFormatInt(vNumeros[ 1]) + 5 * SFormatInt(vNumeros[ 2]) + 4 * SFormatInt(vNumeros[3]) +
        3 * SFormatInt(vNumeros[ 4]) + 2 * SFormatInt(vNumeros[ 5]) + 9 * SFormatInt(vNumeros[6]) +
        8 * SFormatInt(vNumeros[ 7]) + 7 * SFormatInt(vNumeros[ 8]) + 6 * SFormatInt(vNumeros[9]) +
        5 * SFormatInt(vNumeros[10]) + 4 * SFormatInt(vNumeros[11]) + 3 * SFormatInt(vNumeros[12]) + 2 * SFormatInt(vDigito1);

      vDigito := vSoma mod 11;
      if vDigito > 1 then
        vDigito := 11 - vDigito
      else
        vDigito := 0;

      vDigito2 := NFormat( vDigito );
    end;
  else
    vDigito2 := '999';
  end;
  Result := (Copy(VNumeros, Length(VNumeros)-1, 2) = vDigito1 + vDigito2);
end;

function ToSelecao(pValor: string): string;
begin
  Result := IIfStr(pValor = 'S', charSelecionado, charNaoSelecionado);
end;

function SelecaoToChar(pValor: string): string;
begin
  Result := IIfStr(pValor = charSelecionado, 'S', 'N');
end;

function salvarStringEmAquivo(pString: string; pCaminhoNomeArquivo: string): Boolean;
var
  vArquivo: TStringList;
begin
  Result := False;
  vArquivo := TStringList.Create;

  try
    vArquivo.Add(pString);
    vArquivo.SaveToFile(pCaminhoNomeArquivo);

    Result := True;
  except
    on e: exception do
      _Biblioteca.Exclamar('N�o foi poss�vel salvar o arquivo ' + pCaminhoNomeArquivo + '! ' + e.Message);
  end;

  vArquivo.Free;
end;

function carregarAquivoEmString(pCaminhoNomeArquivo: string): string;
var
  i: Integer;
  vArquivo: TStringList;
begin
  Result := '';
  vArquivo := TStringList.Create;

  try
    vArquivo.LoadFromFile(pCaminhoNomeArquivo);

    for i := 0 to vArquivo.Count -1 do
      Result := Result + vArquivo.Strings[i];
  except
    on e: exception do
      _Biblioteca.Exclamar('N�o foi poss�vel carregar o arquivo ' + pCaminhoNomeArquivo + '! ' + e.Message);
  end;

  vArquivo.Free;
end;

function RetornaNomeBanco(pCodigo: Integer): String;
begin
  case pCodigo of
    246: Result := 'Banco ABC Brasil S.A.';
    25 : Result := 'Banco Alfa S.A.';
    641: Result := 'Banco Alvorada S.A.';
    29 : Result := 'Banco Banerj S.A.';
    0  : Result := 'Banco Bankpar S.A.';
    740: Result := 'Banco Barclays S.A.';
    107: Result := 'Banco BBM S.A.';
    31 : Result := 'Banco Beg S.A.';
    739: Result := 'Banco BGN S.A.';
    96 : Result := 'Banco BM&FBOVESPA de Servi�os de Liquida��o e Cust�dia S.A';
    318: Result := 'Banco BMG S.A.';
    752: Result := 'Banco BNP Paribas Brasil S.A.';
    248: Result := 'Banco Boavista Interatl�ntico S.A.';
    218: Result := 'Banco Bonsucesso S.A.';
    65 : Result := 'Banco Bracce S.A.';
    36 : Result := 'Banco Bradesco BBI S.A.';
    204: Result := 'Banco Bradesco Cart�es S.A.';
    394: Result := 'Banco Bradesco Financiamentos S.A.';
    237: Result := 'Banco Bradesco S.A.';
    225: Result := 'Banco Brascan S.A.';
    208: Result := 'Banco BTG Pactual S.A.';
    44 : Result := 'Banco BVA S.A.';
    263: Result := 'Banco Cacique S.A.';
    473: Result := 'Banco Caixa Geral - Brasil S.A.';
    40 : Result := 'Banco Cargill S.A.';
    233: Result := 'Banco Cifra S.A.';
    745: Result := 'Banco Citibank S.A.';
    215: Result := 'Banco Comercial e de Investimento Sudameris S.A.';
    95 : Result := 'Banco Confidence de C�mbio S.A.';
    756: Result := 'Banco Cooperativo do Brasil S.A. - BANCOOB';
    748: Result := 'Banco Cooperativo Sicredi S.A.';
    222: Result := 'Banco Credit Agricole Brasil S.A.';
    505: Result := 'Banco Credit Suisse (Brasil) S.A.';
    229: Result := 'Banco Cruzeiro do Sul S.A.';
    3  : Result := 'Banco da Amaz�nia S.A.';
    707: Result := 'Banco Daycoval S.A.';
    24 : Result := 'Banco de Pernambuco S.A. - BANDEPE';
    456: Result := 'Banco de Tokyo-Mitsubishi UFJ Brasil S.A.';
    214: Result := 'Banco Dibens S.A.';
    1  : Result := 'Banco do Brasil S.A.';
    47 : Result := 'Banco do Estado de Sergipe S.A.';
    37 : Result := 'Banco do Estado do Par� S.A.';
    41 : Result := 'Banco do Estado do Rio Grande do Sul S.A.';
    4  : Result := 'Banco do Nordeste do Brasil S.A.';
    265: Result := 'Banco Fator S.A.';
    224: Result := 'Banco Fibra S.A.';
    626: Result := 'Banco Ficsa S.A.';
    612: Result := 'Banco Guanabara S.A.';
    63 : Result := 'Banco Ibi S.A. Banco M�ltiplo';
    604: Result := 'Banco Industrial do Brasil S.A.';
    320: Result := 'Banco Industrial e Comercial S.A.';
    653: Result := 'Banco Indusval S.A.';
    249: Result := 'Banco Investcred Unibanco S.A.';
    184: Result := 'Banco Ita� BBA S.A.';
    479: Result := 'Banco Ita�Bank S.A';
    376: Result := 'Banco J. P. Morgan S.A.';
    74 : Result := 'Banco J. Safra S.A.';
    217: Result := 'Banco John Deere S.A.';
    600: Result := 'Banco Luso Brasileiro S.A.';
    389: Result := 'Banco Mercantil do Brasil S.A.';
    746: Result := 'Banco Modal S.A.';
    45 : Result := 'Banco Opportunity S.A.';
    79 : Result := 'Banco Original do Agroneg�cio S.A.';
    623: Result := 'Banco Panamericano S.A.';
    611: Result := 'Banco Paulista S.A.';
    643: Result := 'Banco Pine S.A.';
    638: Result := 'Banco Prosper S.A.';
    747: Result := 'Banco Rabobank International Brasil S.A.';
    356: Result := 'Banco Real S.A.';
    633: Result := 'Banco Rendimento S.A.';
    72 : Result := 'Banco Rural Mais S.A.';
    453: Result := 'Banco Rural S.A.';
    422: Result := 'Banco Safra S.A.';
    33 : Result := 'Banco Santander (Brasil) S.A.';
    749: Result := 'Banco Simples S.A.';
    366: Result := 'Banco Soci�t� G�n�rale Brasil S.A.';
    637: Result := 'Banco Sofisa S.A.';
    12 : Result := 'Banco Standard de Investimentos S.A.';
    464: Result := 'Banco Sumitomo Mitsui Brasileiro S.A.';
    634: Result := 'Banco Tri�ngulo S.A.';
    655: Result := 'Banco Votorantim S.A.';
    610: Result := 'Banco VR S.A.';
    119: Result := 'Banco Western Union do Brasil S.A.';
    370: Result := 'Banco WestLB do Brasil S.A.';
    21 : Result := 'BANESTES S.A. Banco do Estado do Esp�rito Santo';
    719: Result := 'Banif-Banco Internacional do Funchal (Brasil)S.A.';
    755: Result := 'Bank of America Merrill Lynch Banco M�ltiplo S.A.';
    73 : Result := 'BB Banco Popular do Brasil S.A.';
    250: Result := 'BCV - Banco de Cr�dito e Varejo S.A.';
    78 : Result := 'BES Investimento do Brasil S.A.-Banco de Investimento';
    69 : Result := 'BPN Brasil Banco M�ltiplo S.A.';
    125: Result := 'Brasil Plural S.A. - Banco M�ltiplo';
    70 : Result := 'BRB - Banco de Bras�lia S.A.';
    104: Result := 'Caixa Econ�mica Federal';
    477: Result := 'Citibank S.A.';
    487: Result := 'Deutsche Bank S.A. - Banco Alem�o';
    64 : Result := 'Goldman Sachs do Brasil Banco M�ltiplo S.A.';
    62 : Result := 'Hipercard Banco M�ltiplo S.A.';
    399: Result := 'HSBC Bank Brasil S.A. - Banco M�ltiplo';
    492: Result := 'ING Bank N.V.';
    652: Result := 'Ita� Unibanco Holding S.A.';
    341: Result := 'Ita� Unibanco S.A.';
    488: Result := 'JPMorgan Chase Bank';
    751: Result := 'Scotiabank Brasil S.A. Banco M�ltiplo';
    409: Result := 'UNIBANCO - Uni�o de Bancos Brasileiros S.A.';
    230: Result := 'Unicard Banco M�ltiplo S.A.';
    85 : Result := 'Banco Cooperativa Central de Cr�dito - Ailos';
  else
    Result := '';
  end;
end;

function GetTexto(var pRtext:TRichEdit): string;
var
  s: TStringStream;
begin

	Result := '';
  if Trim(pRtext.Text) = '' then
  	Exit;

  s := nil;
  try
    s := TStringStream.Create('');
    pRtext.Lines.SaveToStream(s);
    Result := s.DataString;
    // Retirando a termina��o nula da string resultante.
    Delete(Result, Length(Result), 1);
  finally
    s.Free;
  end;
end;

procedure SetTexto(var pRtexto: TRichEdit;pTexto: string);
var
  s: TStringStream;
begin

	pRtexto.Clear;
  if pTexto = '' then
  	Exit;

  s := nil;
  try
    s := TStringStream.Create(pTexto);
    pRtexto.Lines.LoadFromStream(s);
  finally
    s.Free;
  end;
end;

function getValorMulta(
  pValorBase: Double;
  pPercentualMulta: Double;
  pQtdeDiasAtraso: Integer
): Double;
begin
  Result := 0;

  if pQtdeDiasAtraso <= 0 then
    Exit;

  Result := Arredondar((pPercentualMulta * 0.01) * pValorBase, 2);
end;

function getValorJuros(
  pValorBase: Double;
  pPercentualJurosMensal: Double;
  pQtdeDiasAtraso: Integer
): Double;
begin
  Result := 0;

  if pQtdeDiasAtraso <= 0 then
    Exit;

  Result := Arredondar((pPercentualJurosMensal / 30 * pQtdeDiasAtraso * 0.01) * pValorBase, 2);
end;

function getTextoPerfeito(pTexto: string; pTamanhoMaximo: Integer): TArray<string>;
var
	vPosicEspaco: Integer;
  vPosic: Integer;
	vTextoLinha: string;
  vPosicaoQuebraLinha: Integer;

  procedure AdicionarTexto;
  begin
    if vTextoLinha = '' then
      Exit;

    SetLength(Result, Length(Result) + 1);
    Result[High(Result)] := vTextoLinha;
    vTextoLinha := '';
  end;

begin
  Result := nil;

  if pTamanhoMaximo <= 0 then
  	raise Exception.Create('Par�metro inv�lido na fun��o QuebrarStringPerfeita (tamanho)!');

  pTexto := Trim(pTexto);

  if pTexto = '' then
  	Exit;

  vPosicaoQuebraLinha := Pos(sLineBreak, pTexto);

  // Se otexto n�o possuir nenhuma quebra e
  // Se o pTexto couber todo no espa�o, j� adicionar sem nenhum tratamento.
  if (Length(pTexto) <= pTamanhoMaximo) and (vPosicaoQuebraLinha = 0) then begin
  	vTextoLinha := pTexto;
    pTexto := '';
  	AdicionarTexto;
    Exit;
  end;

  vTextoLinha := '';
  while True do begin
    if pTexto = '' then
      Exit;

    // Se existe quebra de linha e o tamanho do pTexto quebrando a linha for menor ou igual ao limite linha
    // J� adicionar o pTexto ao resultado
    vPosicaoQuebraLinha := Pos(sLineBreak, pTexto);
    if (vPosicaoQuebraLinha > 0) and (vPosicaoQuebraLinha <= pTamanhoMaximo) then begin
      vTextoLinha := vTextoLinha + Copy(pTexto, 1, vPosicaoQuebraLinha);
      pTexto := Copy(pTexto, vPosicaoQuebraLinha + Length(sLineBreak), Length(pTexto));

      AdicionarTexto;
      Continue;
    end;

    // Se n�o existe quebra de linha ou
    // Se o tamanho do pTexto com a quebra de linha for maior do que o pTamanhoMaximo
    // Continuar quebrando no espa�o ou no pr�prio limite de linha.
    while True do begin
      vPosic := Pos(' ', pTexto);

      // Se n�o possuir espa�os ou
      // Se for o final do pTexto ou mesmo quebrando a palavra ela ainda n�o couber no pTamanhoMaximo especificado...
      if (vPosic = 0) or ((vTextoLinha = '') and (vPosic > pTamanhoMaximo)) then begin
        vPosic := Length(pTexto);
        if vPosic > pTamanhoMaximo then
          vPosic := pTamanhoMaximo;
      end;

      // Este teste serve para n�o considerar o espa�o no final da string no teste de pTamanhoMaximo do pTexto que ser� adicionado.
      vPosicEspaco := 0;
      if pTexto[vPosic] = ' ' then
        vPosicEspaco := 1;

      if Length(vTextoLinha) + vPosic - vPosicEspaco <= pTamanhoMaximo then begin
        vTextoLinha := vTextoLinha + Copy(pTexto, 1, vPosic);
        pTexto := Copy(pTexto, vPosic + 1, Length(pTexto));
      end
      else begin
        AdicionarTexto;
        Break;
      end;

      if pTexto = '' then begin
        AdicionarTexto; // Adicionando pTexto pendente.
        Break;
      end;
    end;
  end;
end;

function getIdsSelecionados( pGrid: TGridLuka; pColunaSelecao: Integer; pColunaId: Integer; pLinhaInicial: Integer = 1 ): TArray<Integer>;
var
  i: Integer;
begin
  Result := nil;
  for i := pLinhaInicial to pGrid.RowCount -1 do begin
    if pGrid.Cells[pColunaSelecao, i] <> charSelecionado then
      Continue;

    AddNoVetorSemRepetir(Result, SFormatInt(pGrid.Cells[pColunaId, i]));
  end;
end;

function getNomeCaminhoArquivoSalvar(pExtensaoArquivo: string; pDescricaoArquivo: string): string;
var
  vDialog: TSaveDialog;
begin
  Result := '';
  vDialog := TSaveDialog.Create(nil);

  vDialog.Title       := 'Selecione o local para salvar o arquivo';
  vDialog.InitialDir  := GetCurrentDir;
  vDialog.DefaultExt  := pExtensaoArquivo;
  vDialog.Filter      := pDescricaoArquivo + '|*.' + pExtensaoArquivo;

  if vDialog.Execute then
    Result := vDialog.FileName;

  vDialog.Free;
end;

function getNomeCaminhoArquivoCarregar(pExtensaoArquivo: string; pDescricaoArquivo: string): string;
var
  vDialog: TOpenDialog;
begin
  Result := '';
  vDialog := TOpenDialog.Create(nil);

  vDialog.Title       := 'Selecione o local para carregar o arquivo';
  vDialog.InitialDir  := GetCurrentDir;
  vDialog.DefaultExt  := pExtensaoArquivo;
  vDialog.Filter      := pDescricaoArquivo + '|*.' + pExtensaoArquivo;

  if vDialog.Execute then
    Result := vDialog.FileName;

  vDialog.Free;
end;

procedure GridToExcel(pGrid: TGridLuka; pColunas: array of integer);
var
  xls, wb, Range: OLEVariant;
  arrData: Variant;
  RowCount, ColCount, i, j: Integer;
  colunas: Integer;
  removerPontoColuna: Boolean;
begin
  {create variant array where we'll copy our data}
  RowCount := pGrid.RowCount;
  ColCount := pGrid.ColCount;
  arrData := VarArrayCreate([1, RowCount, 1, ColCount], varVariant);
  {fill array}
  for i := 1 to RowCount do
    for j := 1 to ColCount do begin
      removerPontoColuna := False;
      for colunas := 0 to Length(pColunas) - 1 do begin
        if j = pColunas[colunas] + 1 then begin
          removerPontoColuna := True;
          Break;
        end;
      end;

      if removerPontoColuna then
        arrData[i, j] := RetornaNumeros(pGrid.Cells[j-1, i-1])
      else
        arrData[i, j] := pGrid.Cells[j-1, i-1];
    end;
  {initialize an instance of Excel}
  xls := CreateOLEObject('Excel.Application');
  {create workbook}
  wb := xls.Workbooks.Add;
  {retrieve a range where data must be placed}
  Range := wb.WorkSheets[1].Range[wb.WorkSheets[1].Cells[1, 1],
                                  wb.WorkSheets[1].Cells[RowCount, ColCount]];
  {copy data from allocated variant array}
  Range.Value := arrData;
  {show Excel with our data}
  xls.Visible := True;
end;

procedure GridToExcel(pGrid: TGridLuka);
var
  xls, wb, Range: OLEVariant;
  arrData: Variant;
  RowCount, ColCount, i, j: Integer;
begin
  {create variant array where we'll copy our data}
  RowCount := pGrid.RowCount;
  ColCount := pGrid.ColCount;
  arrData := VarArrayCreate([1, RowCount, 1, ColCount], varVariant);
  {fill array}
  for i := 1 to RowCount do
    for j := 1 to ColCount do
      arrData[i, j] := pGrid.Cells[j-1, i-1];
  {initialize an instance of Excel}
  xls := CreateOLEObject('Excel.Application');
  {create workbook}
  wb := xls.Workbooks.Add;
  {retrieve a range where data must be placed}
  Range := wb.WorkSheets[1].Range[wb.WorkSheets[1].Cells[1, 1],
                                  wb.WorkSheets[1].Cells[RowCount, ColCount]];
  {copy data from allocated variant array}
  Range.Value := arrData;
  {show Excel with our data}
  xls.Visible := True;
end;

function Inverter(Caminho : String) : String;
{Inverte a ordem dos nomes do caminho}
var
  N,
  I,
  J       : Integer;
  Str,
  Montar,
  Aux     : String;
  Vetor   : Array [1..20] of String;
begin
  {Nessa parte � separado as palavras do caminho e atribui a um vetor}
  I := 1;
  for N := 1 to length(caminho) do begin
    Str := Copy(Caminho,N,1);
    if (Str <> '|') then
      Montar := Montar + Str
    else begin
      Vetor[i] := Trim(Montar);
      Montar   := '';
      inc(I);
    end;
  end;
  Vetor[I] := Montar;

  {Nessa parte � invertido os valores do vetor}
  if ((I mod 2) <> 0) then
    J := Trunc(I/2)
  else
    J := Trunc(I/2) - 1;

  for n := 0 to j do begin
    Aux          := Vetor[N + 1];
    Vetor[N + 1] := Vetor[I - N];
    Vetor[I - N] := Aux;
  end;
  Caminho := '';

  {Nessa parte � concatenado novamente}
  for N := 1 to i do begin
    Caminho := Caminho + Trim(Vetor[N]) + IfThen(N <> I,
                                              ' | ',
                                              '');
  end;
  Result := Caminho;
end;
end.

