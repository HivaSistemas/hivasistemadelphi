unit _HerancaCadastro;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaBarra, Vcl.ExtCtrls, _Biblioteca, _Sessao,
  Vcl.Buttons, Vcl.StdCtrls, EditLuka, _HerancaPrincipal, EditLukaData;

type
  TFormHerancaCadastro = class(TFormHerancaBarra)
    sbGravar: TSpeedButton;
    sbDesfazer: TSpeedButton;
    sbExcluir: TSpeedButton;
    sbPesquisar: TSpeedButton;
    sbLogs: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure sbDesfazerClick(Sender: TObject);
    procedure sbExcluirClick(Sender: TObject);
    procedure sbGravarClick(Sender: TObject);
    procedure sbPesquisarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sbLogsClick(Sender: TObject);
  protected
    FEditandoHeranca: Boolean;

    procedure BuscarRegistro; virtual;
    procedure ExcluirRegistro; virtual;
    procedure GravarRegistro(Sender: TObject); virtual; abstract;
    procedure Modo(pEditando: Boolean); virtual;
    procedure PesquisarRegistro; virtual;
    procedure ExibirLogs; virtual;

    function AbrirModal: Integer;
  end;

implementation

{$R *.dfm}

{ TFormHerancaCadastro }

function TFormHerancaCadastro.AbrirModal: Integer;
begin
  Self.FormStyle := fsNormal;
  Self.Visible   := False;
  Result := Self.ShowModal;
end;

procedure TFormHerancaCadastro.BuscarRegistro;
begin
  inherited;
  sbExcluir.Enabled := True;
  Modo(True);
end;

procedure TFormHerancaCadastro.ExcluirRegistro;
begin
  inherited;

end;

procedure TFormHerancaCadastro.ExibirLogs;
begin
  // nada
end;

procedure TFormHerancaCadastro.FormClose(Sender: TObject; var Action: TCloseAction);
begin

  if (not Sessao.FinalizandoPorTempoInativo) and FEditandoHeranca then begin
    if not _Biblioteca.Perguntar('Deseja realmente sair?') then
      Abort;
  end;

  inherited;
end;

procedure TFormHerancaCadastro.FormCreate(Sender: TObject);
begin
  inherited;
  Modo(False);
end;

procedure TFormHerancaCadastro.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_F2: begin
      if sbGravar.Enabled and sbGravar.Visible then
        sbGravar.Click;
    end;

    VK_F3: begin
      if sbDesfazer.Enabled and sbDesfazer.Visible then
        sbDesfazer.Click;
    end;

    VK_F4: begin
      if sbExcluir.Enabled and sbExcluir.Visible then
        sbExcluir.Click;
    end;

    VK_F5:  begin
      if sbPesquisar.Enabled and sbPesquisar.Visible then
        sbPesquisar.Click;
    end;
  end;
end;

procedure TFormHerancaCadastro.Modo(pEditando: Boolean);
var
  vEditCadastro: TEditLukaData;
begin
  inherited;
  _Biblioteca.Habilitar([sbGravar, sbDesfazer, sbLogs], pEditando);
  sbPesquisar.Enabled := not pEditando;

  FEditandoHeranca := pEditando;

  if not pEditando then begin
    sbExcluir.Enabled := False;

    if FindComponent('eDataCadastro') = nil then
      Exit;

    vEditCadastro := TEditLukaData(FindComponent('eDataCadastro'));
    if vEditCadastro <> nil then
      vEditCadastro.Clear;
  end;
end;

procedure TFormHerancaCadastro.PesquisarRegistro;
begin
  inherited;
  Modo(True);
  sbExcluir.Enabled := True;
end;

procedure TFormHerancaCadastro.sbDesfazerClick(Sender: TObject);
begin
  inherited;

  if (not Sessao.FinalizandoPorTempoInativo) and (not _Biblioteca.Perguntar('Deseja realmente sair?')) then
    Exit;

  Modo(False);
end;

procedure TFormHerancaCadastro.sbExcluirClick(Sender: TObject);
begin
  inherited;
  if _Biblioteca.Perguntar('Deseja deletar este registro?') then begin
    ExcluirRegistro;

    _Biblioteca.Informar('Registro exclu�do com sucesso');
    Modo(False);
  end;
end;

procedure TFormHerancaCadastro.sbGravarClick(Sender: TObject);
begin
  inherited;
  VerificarRegistro(Sender);
  GravarRegistro(Sender);
  Modo(False);
end;

procedure TFormHerancaCadastro.sbLogsClick(Sender: TObject);
begin
  inherited;
  ExibirLogs;
end;

procedure TFormHerancaCadastro.sbPesquisarClick(Sender: TObject);
begin
  inherited;
  PesquisarRegistro;
end;

end.
