inherited FormHerancaCadastroCodigo: TFormHerancaCadastroCodigo
  Caption = 'Heran'#231'a Cadastro C'#243'digo'
  ClientWidth = 610
  ExplicitWidth = 616
  PixelsPerInch = 96
  TextHeight = 14
  object Label1: TLabel [0]
    Left = 126
    Top = 5
    Width = 37
    Height = 14
    Caption = 'C'#243'digo'
  end
  inherited pnOpcoes: TPanel
    inherited sbDesfazer: TSpeedButton
      Top = 94
      ExplicitTop = 94
    end
    inherited sbExcluir: TSpeedButton
      Top = 150
      ExplicitTop = 150
    end
    inherited sbPesquisar: TSpeedButton
      Top = 46
      ExplicitTop = 46
    end
  end
  object eID: TEditLuka
    Left = 126
    Top = 19
    Width = 80
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 1
    OnKeyDown = eIDKeyDown
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object ckAtivo: TCheckBoxLuka
    Left = 563
    Top = 5
    Width = 46
    Height = 17
    Caption = 'Ativo'
    TabOrder = 2
    CheckedStr = 'N'
  end
end
