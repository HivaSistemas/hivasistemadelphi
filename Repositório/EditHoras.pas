unit EditHoras;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.StdCtrls, Vcl.Mask, Vcl.Graphics, Winapi.Windows,
  _Biblioteca;

type
  TEditHoras = class(TMaskEdit)
  private
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure SetHora(const pValor: TDateTime);
    function  GetHora: TDateTime;
  protected
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property AsHora: TDateTime read GetHora write SetHora;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('NoviTec', [TEditHoras]);
end;

{ TEditHoras }

procedure TEditHoras.CMEnter(var Message: TCMEnter);
begin
  if Self.Color = clWindow then
    Self.Color := $0080FFFF;
end;

procedure TEditHoras.CMExit(var Message: TCMExit);
begin
  if Self.Color = $0080FFFF then
    Self.Color := clWindow;
end;

constructor TEditHoras.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  EditMask := '!90:00;1;_';
end;

function TEditHoras.GetHora: TDateTime;
begin
  try
    Result := StrToTime(Text);
  except
    Result := 0;
  end;
end;

procedure TEditHoras.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  try
    StrToTime(Text);
  except
    Text := '';
  end;
end;

procedure TEditHoras.SetHora(const pValor: TDateTime);
begin
  Text := HFormatN(pValor);
end;

end.
