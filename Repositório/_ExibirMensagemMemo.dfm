inherited FormExibirMensagemMemo: TFormExibirMensagemMemo
  Caption = 'T'#237'tulo da tela'
  ClientHeight = 244
  ClientWidth = 584
  Visible = False
  OnShow = FormShow
  ExplicitWidth = 590
  ExplicitHeight = 273
  PixelsPerInch = 96
  TextHeight = 14
  object meMensagem: TMemo
    Left = 0
    Top = 0
    Width = 584
    Height = 209
    Align = alClient
    BevelInner = bvNone
    BevelOuter = bvNone
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 0
  end
  object pnOpcoes: TPanel
    Left = 0
    Top = 209
    Width = 584
    Height = 35
    Align = alBottom
    TabOrder = 1
    object sbOk: TSpeedButton
      Left = 234
      Top = 1
      Width = 113
      Height = 33
      Caption = 'Ok'
      Visible = False
      OnClick = sbOkClick
    end
    object lbBotaoPressionar: TLabel
      Left = 170
      Top = 8
      Width = 253
      Height = 19
      Caption = 'Para continuar pressione <F1>'
      Font.Charset = ANSI_CHARSET
      Font.Color = clSilver
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
  end
  object tiBotaoPressionar: TTimer
    Enabled = False
    OnTimer = tiBotaoPressionarTimer
    Left = 528
    Top = 64
  end
end
