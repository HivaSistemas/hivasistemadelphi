unit _OperacoesBancoDados;

interface

uses
  Vcl.Forms, _Conexao, Data.DB, DBAccess, Ora, System.Classes, System.SysUtils, System.Variants, _RecordsEspeciais, OraClasses, _BibliotecaGenerica, _Logs, Vcl.Dialogs,
  DAScript, OraScript, OraErrHand;

type
  TProcedimentoBanco = class(TOraStoredProc)
  private
    FLogs: TLogs;
  public
    constructor Create(pConexao: TConexao; pProcedimento: string); reintroduce;
    procedure Executar; overload;
    procedure Executar(pParametros: TArray<Variant>); overload;
  end;

  // Usada apenas para selects
  TConsulta = class(TOraQuery)
  private
    FQuantidadeRegistros: Integer;
    FLogs: TLogs;
  public
    constructor Create(pConexao: TComponent); override;
    function GetQuantidadeRegistros: Integer;
    function Pesquisar: Boolean; overload;
    function Pesquisar(pFiltros: array of Variant): Boolean; overload;
    function GetInt(pColuna: string): Integer; overload;
    function GetInt(pColuna: Integer): Integer; overload;
    function GetString(pColuna: string): string; overload;
    function GetString(pColuna: Integer): string; overload;
    function GetDouble(pColuna: string): Double; overload;
    function GetDouble(pColuna: Integer): Double; overload;
    function GetData(pColuna: string): TDateTime; overload;
    function GetData(pColuna: Integer): TDateTime; overload;
    function getFoto(pColuna: string): TMemoryStream; overload;
    function getFoto(pColuna: Integer): TMemoryStream; overload;
    procedure Add(pSql: string);
    procedure Limpar;

    destructor Destroy; override;
  end;

  {$M+}
  TSequencia = class
  private
    FLogs: TLogs;
    FConsulta: TConsulta;
    FSequenciaId: Integer;
  public
    constructor Create(pConexao: TConexao; pNomeSequencia: string);
    function GetProximaSequencia: Integer;
    destructor Destroy; override;
  published
      property SequenciaAtual: Integer read FSequenciaId;
  end;
  {$M-}

  // Usada para insert e update em tabelas
  TExecucao = class(TOraQuery)
  private
    FLogs: TLogs;
  public
    constructor Create(pConexao: TComponent); override;
    procedure Limpar;
    function Executar: Boolean; overload;
    function Executar(pParametros: array of Variant): Boolean; overload;
    procedure CarregarBinario(pNomeParametro: string; pBinario: TMemoryStream); overload;
    procedure CarregarBinario(pNomeParametro: string; pCaminhoArquivo: string); overload;
    procedure CarregarClob(pNomeParametro: string; pClob: TMemoryStream);
    procedure Add(pSql: string);
    procedure Clear;
    destructor Destroy; override;
  end;

  // Usado para atualiza��es da estrutra do banco de dados
  TScript = class(TOraScript)
  private
    FOracleHandle: TOraErrorHandler;

    procedure OraErrorHandlerError(Sender: TObject; E: Exception; ErrorCode: Integer; const ConstraintName: string; var Msg: string);
  public
    constructor Create(pConexao: TComponent); override;
    procedure Limpar;
    procedure Executar;
    procedure Add(pSql: string);
    destructor Destroy; override;
  end;

  TCampo = class
    nome: string;
    valor: Variant;
    read_only: Boolean;
  end;

  TCampos = class
  private
    FDados: TList;
    function GetIndiceColuna(pColuna: string): Integer;
  public
    constructor Create;
    destructor Destroy; override;
    function SetColuna(pColuna: string; pReadOnly: Boolean): Integer;

    procedure SetValor(pColuna: string; pValor: Variant); overload;
    procedure SetValor(pColuna: Integer; pValor: Variant); overload;

    function GetValorInteger(pColuna: string): Integer; overload;
    function GetValorInteger(pColuna: Integer): Integer; overload;

    function GetValorInt64(pColuna: string): Int64; overload;
    function GetValorInt64(pColuna: Integer): Int64; overload;

    function GetValorString(pColuna: string): string; overload;
    function GetValorString(pColuna: Integer): string; overload;

    function GetValorData(pColuna: string): TDateTime; overload;
    function GetValorData(pColuna: Integer): TDateTime; overload;

    function GetValorDouble(pColuna: string): Double; overload;
    function GetValorDouble(pColuna: Integer): Double; overload;
  end;

  TOperacoes = class
  private
    FTabela: string;
    FChaves: TCampos;
    FCampos: TCampos;
    FExecucao: TExecucao;
    FFiltrosString: string;
    FFiltros: TArray<RecFiltros>;
    FQuantidadeRegistros: Integer;

    FLogs: TLogs;

    procedure PreencherCampos;
  public
    constructor Create(pConexao: TConexao; pTabela: string);
    destructor Destroy; override;

    procedure Inserir;
    function Atualizar: Boolean;
    procedure ProximoRegistro;

    procedure setInt(const pColuna: string; const pValor: Integer; const pChave: Boolean = False); overload;
    procedure setIntN(const pColuna: string; const pValor: Integer; const pChave: Boolean = False); overload; // Aceita Null
    procedure setInt(const pColuna: Integer; const pValor: Integer; const pChave: Boolean = False); overload;
    procedure setIntN(const pColuna: Integer; const pValor: Integer; const pChave: Boolean = False); overload; // Aceita Null

    procedure setDouble(const pColuna: string; const pValor: Double; const pChave: Boolean = False); overload;
    procedure setDoubleN(const pColuna: string; const pValor: Double; const pChave: Boolean = False); overload; // Aceita Null
    procedure setDouble(const pColuna: Integer; const pValor: Double; const pChave: Boolean = False); overload;
    procedure setDoubleN(const pColuna: Integer; const pValor: Double; const pChave: Boolean = False); overload; // Aceita Null

    procedure setString(const pColuna: string; const pValor: string; const pChave: Boolean = False); overload;
    procedure setStringN(const pColuna: string; const pValor: string; const pChave: Boolean = False); overload; // Aceita Null
    procedure setString(const pColuna: Integer; const pValor: string; const pChave: Boolean = False); overload;
    procedure setStringN(const pColuna: Integer; const pValor: string; const pChave: Boolean = False); overload; // Aceita Null

    procedure setData(const pColuna: string; const pValor: TDateTime; const pChave: Boolean = False); overload;
    procedure setDataN(const pColuna: string; const pValor: TDateTime; const pChave: Boolean = False); overload; // Aceita Null
    procedure setData(const pColuna: Integer; const pValor: TDateTime; const pChave: Boolean = False); overload;
    procedure setDataN(const pColuna: Integer; const pValor: TDateTime; const pChave: Boolean = False); overload; // Aceita Null
  protected
    FSql: string;

    function Excluir: Boolean;
    function GetQuantidadeRegistros: Integer;

    procedure SetFiltros(pFiltros: string); overload;
    procedure SetFiltros(pFiltros: TArray<RecFiltros>); overload;

    function PesquisarComando(pComando: string): Boolean;
    function Pesquisar(pIndice: Integer; pParametros: array of Variant): Boolean; overload;
    function Pesquisar(pIndice: Integer; pParametros: array of Variant; pFiltrosAuxiliar: string): Boolean; overload;

    function AddColuna(const pColuna: string; const pChave: Boolean = False): Integer;
    function AddColunaSL(const pColuna: string; const pChave: Boolean = False): Integer; // Somente Leitura esta coluna

    // Fun��es para obter os valores das colunas
    function getInt(const pColuna: string; const pChave: Boolean = False): Integer; overload;
    function getInt(const pColuna: Integer; const pChave: Boolean = False): Integer; overload;

    function getString(const pColuna: string; const pChave: Boolean = False): string; overload;
    function getString(const pColuna: Integer; const pChave: Boolean = False): string; overload;

    function getDouble(const pColuna: string; const pChave: Boolean = False): Double; overload;
    function getDouble(const pColuna: Integer; const pChave: Boolean = False): Double; overload;

    function getData(const pColuna: string; const pChave: Boolean = False): TDateTime; overload;
    function getData(const pColuna: Integer; const pChave: Boolean = False): TDateTime; overload;
  end;

function NovoRecFiltro(
  pDescricaoFiltro: string;
  pFiltroPadrao: Boolean;
  pIndice: Integer;
  pSQL: string;
  pOrderBy: string = '';
  pGroupBy: string = '';
  pPesquisaAvancada: Boolean = False
): RecFiltros; overload;

function NovoRecFiltro(
  pDescricaoFiltro: string;
  pFiltroPadrao: Boolean;
  pIndice: Integer;
  pSQL: string;
  pPesquisaAvancada: Boolean
): RecFiltros; overload;

implementation

constructor TProcedimentoBanco.Create(pConexao: TConexao; pProcedimento: string);
begin
  inherited Create(pConexao);
  Session := pConexao;

  FLogs := TLogs.Create(Self, ExtractFileDir(Application.ExeName), 'sqlproc');

  StoredProcName := pProcedimento;
  Prepare;
end;

{ FConsulta }

procedure TConsulta.Add(pSql: string);
begin
  SQL.Add(pSql);
end;

constructor TConsulta.Create(pConexao: TComponent);
begin
  inherited Create(pConexao);
  Session := TOraSession(pConexao);
  DisableControls;
  Active := False;
  SQL.Clear;

  FLogs := TLogs.Create(nil, ExtractFileDir(Application.ExeName), 'sqlcons');
end;

function TConsulta.GetQuantidadeRegistros: Integer;
begin
  Result := FQuantidadeRegistros;
end;

function TConsulta.Pesquisar: Boolean;
begin
  Result := Pesquisar([]);
end;

function TConsulta.Pesquisar(pFiltros: array of Variant): Boolean;
var
  i: Integer;
begin
  Active := False;

  for i := Low(pFiltros) to High(pFiltros) do
    Params[i].Value := pFiltros[i];

  try
    Active := True;
    Result := not IsEmpty;
  except
    on e: Exception do begin
      FLogs.AddLog(e.Message);
      FLogs.AddLog(SQL.Text);
      raise;
    end;
  end;

  if not Result then
    Exit;

  FQuantidadeRegistros := 0;
  First;

  while not Eof do begin
    Inc(FQuantidadeRegistros);
    Next;
  end;

  First;
end;

function TConsulta.GetInt(pColuna: Integer): Integer;
begin
  Result := Fields[pColuna].AsInteger;
end;

function TConsulta.GetInt(pColuna: string): Integer;
begin
  Result := Fields.FieldByName(pColuna).AsInteger;
end;

function TConsulta.GetString(pColuna: Integer): string;
begin
  Result := Fields[pColuna].AsString;
end;

procedure TConsulta.Limpar;
begin
  sql.Clear;
end;

function TConsulta.GetString(pColuna: string): string;
begin
  Result := Fields.FieldByName(pColuna).AsString;
end;

function TConsulta.GetDouble(pColuna: Integer): Double;
begin
  Result := Fields[pColuna].AsFloat;
end;

function TConsulta.GetDouble(pColuna: string): Double;
begin
  Result := Fields.FieldByName(pColuna).AsFloat;
end;

function TConsulta.GetData(pColuna: Integer): TDateTime;
begin
  Result := Fields[pColuna].AsDateTime;
end;

function TConsulta.GetData(pColuna: string): TDateTime;
begin
  Result := Fields.FieldByName(pColuna).AsDateTime;
end;

function TConsulta.getFoto(pColuna: string): TMemoryStream;
begin
  Result := getFoto(FieldByName(pColuna).Index);
end;

function TConsulta.getFoto(pColuna: Integer): TMemoryStream;
var
  s: TStream;
begin
  Result := nil;
  if Fields.Fields[pColuna].Value = null then
    Exit;

  Result := TMemoryStream.Create;
  s := CreateBlobStream(Fields[pColuna], bmRead);
  Result.LoadFromStream(s);
  s.Free;
end;

destructor TConsulta.Destroy;
begin
  inherited;
  Active := False;
  FLogs.Free;
end;

constructor TSequencia.Create(pConexao: TConexao; pNomeSequencia: string);
begin
  inherited Create;

  FLogs := TLogs.Create(nil, ExtractFileDir(Application.ExeName), 'sqlseq');
  FConsulta := TConsulta.Create(pConexao);
  FSequenciaId := -1;

  try
    FConsulta.SQL.Add('select ' + pNomeSequencia + '.nextval from DUAL');

    FConsulta.UnPrepare;
    FConsulta.Prepare;
  except
    on e: Exception do begin
      FLogs.AddLog(e.Message);
      raise;
    end;
  end;
end;

function TSequencia.GetProximaSequencia: Integer;
begin
  try
    FConsulta.Pesquisar;

    FSequenciaId := FConsulta.Fields.Fields[0].AsInteger;
    Result := FSequenciaId;

    FConsulta.Active := False;
  except
    on e: Exception do begin
      FLogs.AddLog(e.Message);
      raise;
    end;
  end;
end;

destructor TSequencia.Destroy;
begin
  inherited;
  FConsulta.Free;
  FLogs.Free;
end;

{ Execucao }

constructor TExecucao.Create(pConexao: TComponent);
begin
  inherited Create(pConexao);
  Session := TOraSession(pConexao);
  FLogs := TLogs.Create(nil, ExtractFileDir(Application.ExeName), 'sqlexec');
end;

destructor TExecucao.Destroy;
begin
  FLogs.Free;
  inherited;
end;

function TExecucao.Executar: Boolean;
var
  i: Integer;
begin
  try
     ExecSQL;
    Result := True;
  except
    on e: Exception do begin
      FLogs.AddLog(e.Message + #13 + SQL.Text + #13);
      for i := 0 to ParamCount -1 do
        FLogs.AddLog(Params[i].Value);

      raise;
    end;
  end;
end;

function TExecucao.Executar(pParametros: array of Variant): Boolean;
var
  i: Integer;
begin
  for i := Low(pParametros) to High(pParametros) do begin
    if pParametros[i] = null then
      ParamByName('P' + IntToStr(i + 1)).AsString := ''
    else begin
      if Length(pParametros[i]) <= 4000 then
        ParamByName('P' + IntToStr(i + 1)).Value := pParametros[i]
      else begin
        ParamByName('P' + IntToStr(i + 1)).ParamType := ptInput;
        ParamByName('P' + IntToStr(i + 1)).AsOraClob.OCISvcCtx := Session.OCISvcCtx;
        ParamByName('P' + IntToStr(i + 1)).AsOraClob.CreateTemporary(ltBlob);
        ParamByName('P' + IntToStr(i + 1)).AsOraClob.AsString := pParametros[i];
        ParamByName('P' + IntToStr(i + 1)).AsOraClob.WriteLob;
      end;
    end;
  end;

  Result := Executar;
end;

procedure TExecucao.Limpar;
begin
  SQL.Clear;
end;

procedure TExecucao.Add(pSql: string);
begin
  SQL.Add(pSql);
end;

procedure TExecucao.CarregarBinario(pNomeParametro: string; pBinario: TMemoryStream);
begin
  ParamByName(pNomeParametro).ParamType := ptInput;
  ParamByName(pNomeParametro).AsOraBlob.OCISvcCtx := Session.OCISvcCtx;
  ParamByName(pNomeParametro).AsOraBlob.CreateTemporary(ltBlob);
  ParamByName(pNomeParametro).AsOraBlob.LoadFromStream(pBinario);
  ParamByName(pNomeParametro).AsOraBlob.WriteLob;
  pBinario.Free;
end;

procedure TExecucao.CarregarBinario(pNomeParametro: string; pCaminhoArquivo: string);
begin
  ParamByName(pNomeParametro).ParamType := ptInput;
  ParamByName(pNomeParametro).AsOraBlob.OCISvcCtx := Session.OCISvcCtx;
  ParamByName(pNomeParametro).AsOraBlob.CreateTemporary(ltBlob);
  ParamByName(pNomeParametro).AsOraBlob.LoadFromFile(pCaminhoArquivo);
  ParamByName(pNomeParametro).AsOraBlob.WriteLob;
end;

procedure TExecucao.CarregarClob(pNomeParametro: string; pClob: TMemoryStream);
begin
  ParamByName(pNomeParametro).ParamType := ptInput;
  ParamByName(pNomeParametro).AsOraClob.OCISvcCtx := Session.OCISvcCtx;
  ParamByName(pNomeParametro).AsOraClob.CreateTemporary(ltClob);
  ParamByName(pNomeParametro).AsOraClob.LoadFromStream(pClob);
  ParamByName(pNomeParametro).AsOraClob.WriteLob;
  pClob.Free;
end;

procedure TExecucao.Clear;
begin
  SQL.Clear;
end;

{ TCampos }

constructor TCampos.Create;
begin
  inherited;
  FDados := TList.Create;
end;

destructor TCampos.Destroy;
var
  i: Integer;
begin
  for i := 0 to FDados.Count -1 do
    TCampo(FDados[i]).Free;

  FDados.Free;
  inherited;
end;

function TCampos.GetIndiceColuna(pColuna: string): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if pColuna <> TCampo(FDados[i]).nome then
      Continue;

    Result := i;
    Break;
  end;
end;

function TCampos.GetValorInteger(pColuna: string): Integer;
begin
  if VarIsNull(TCampo(FDados[GetIndiceColuna(pColuna)]).valor) then
    Result := 0
  else
    Result := Integer(TCampo(FDados[GetIndiceColuna(pColuna)]).valor);
end;

function TCampos.GetValorInteger(pColuna: Integer): Integer;
begin
  if VarIsNull(TCampo(FDados[pColuna]).valor) then
    Result := 0
  else
    Result := Integer(TCampo(FDados[pColuna]).valor);
end;

function TCampos.GetValorInt64(pColuna: string): Int64;
begin
  if VarIsNull(TCampo(FDados[GetIndiceColuna(pColuna)]).valor) then
    Result := 0
  else
    Result := Int64(TCampo(FDados[GetIndiceColuna(pColuna)]).valor);
end;

function TCampos.GetValorInt64(pColuna: Integer): Int64;
begin
  if VarIsNull(TCampo(FDados[pColuna]).valor) then
    Result := 0
  else
    Result := Int64(TCampo(FDados[pColuna]).valor);
end;

function TCampos.GetValorString(pColuna: string): string;
begin
  if VarIsNull(TCampo(FDados[GetIndiceColuna(pColuna)]).valor) then
    Result := ''
  else
    Result := string(TCampo(FDados[GetIndiceColuna(pColuna)]).valor);
end;

function TCampos.GetValorString(pColuna: Integer): string;
begin
  if VarIsNull(TCampo(FDados[pColuna]).valor) then
    Result := ''
  else
    Result := string(TCampo(FDados[pColuna]).valor);
end;

function TCampos.GetValorData(pColuna: string): TDateTime;
begin
  if VarIsNull(TCampo(FDados[GetIndiceColuna(pColuna)]).valor) then
    Result := 0
  else
    Result := TCampo(FDados[GetIndiceColuna(pColuna)]).valor;
end;

function TCampos.GetValorData(pColuna: Integer): TDateTime;
begin
  if VarIsNull(TCampo(FDados[pColuna]).valor) then
    Result := 0
  else
    Result := TCampo(FDados[pColuna]).valor;
end;

function TCampos.GetValorDouble(pColuna: string): Double;
begin
  if VarIsNull(TCampo(FDados[GetIndiceColuna(pColuna)]).valor) then
    Result := 0
  else
    Result := Double(TCampo(FDados[GetIndiceColuna(pColuna)]).valor);
end;

function TCampos.GetValorDouble(pColuna: Integer): Double;
begin
  if VarIsNull(TCampo(FDados[pColuna]).valor) then
    Result := 0
  else  
    Result := Double(TCampo(FDados[pColuna]).valor);
end;

function TCampos.SetColuna(pColuna: string; pReadOnly: Boolean): Integer;
var
  x: TCampo;
begin
  x := TCampo.Create;

  x.valor := null;
  x.nome := pColuna;
  x.read_only := pReadOnly;

  Result := FDados.Add(x);
end;

procedure TCampos.SetValor(pColuna: string; pValor: Variant);
var
  i: Integer;
  vEncontrou: Boolean;
begin
  vEncontrou := False;
  for i := 0 to FDados.Count - 1 do begin
    if pColuna <> TCampo(FDados[i]).nome then
      Continue;

    vEncontrou := True;
    TCampo(FDados[i]).valor := pValor;
    Break;
  end;

  if not vEncontrou then begin
    ShowMessage('Campo n�o encontrado!!! ' + pColuna);
    Abort;
  end;
end;

procedure TCampos.SetValor(pColuna: Integer; pValor: Variant);
begin
  TCampo(FDados[pColuna]).valor := pValor;
end;

{ Operacoes }

constructor TOperacoes.Create(pConexao: TConexao; pTabela: string);
begin
  FSql := '';
  FTabela := pTabela;
  FFiltrosString := '';
  FChaves := TCampos.Create;
  FCampos := TCampos.Create;
  FQuantidadeRegistros := 0;
  FExecucao := TExecucao.Create(pConexao);
  FLogs := TLogs.Create(nil, ExtractFileDir(Application.ExeName), 'sqlinsatu');
end;

destructor TOperacoes.Destroy;
begin
  FLogs.Free;
  FChaves.Free;
  FCampos.Free;
  FExecucao.Free;

  inherited;
end;

procedure TOperacoes.PreencherCampos;
var
  i: Integer;
begin
  for i := 0 to FChaves.FDados.Count - 1 do
    FChaves.SetValor(TCampo(FChaves.FDados[i]).nome, FExecucao.FieldByName(TCampo(FChaves.FDados[i]).nome).AsVariant);

  for i := 0 to FCampos.FDados.Count - 1 do
    FCampos.SetValor(TCampo(FCampos.FDados[i]).nome, FExecucao.FieldByName(TCampo(FCampos.FDados[i]).nome).AsVariant);
end;

procedure TOperacoes.Inserir;
var
  i: Integer;
  vExec: TOraQuery;
  vParametros: array of Variant;
begin
  try
    vExec := TOraQuery.Create(nil);

    FSql := 'insert into ' + FTabela + '(';

    for i := 0 to FChaves.FDados.Count - 1 do
      FSql := FSql + TCampo(FChaves.FDados[i]).nome + ', ';

    for i := 0 to FCampos.FDados.Count - 1 do begin
      if TCampo(FCampos.FDados[i]).read_only then
        Continue;

      FSql := FSql + TCampo(FCampos.FDados[i]).nome + ', ';
    end;

    Delete(FSql, Length(FSql) - 1, 2);

    FSql := FSql + ')values(';

    for i := 0 to FChaves.FDados.Count - 1 do
      FSql := FSql + ':P' + IntToStr(i + 1) + ', ';

    for i := 0 to FCampos.FDados.Count - 1 do begin
      if TCampo(FCampos.FDados[i]).read_only then
        Continue;

      FSql := FSql + ':P' + IntToStr(FChaves.FDados.Count + i + 1) + ', ';
    end;

    Delete(FSql, Length(FSql) - 1, 2);

    FSql := FSql + ')';

    for i := 0 to FChaves.FDados.Count - 1 do begin
      SetLength(vParametros, Length(vParametros) + 1);
      vParametros[High(vParametros)] := TCampo(FChaves.FDados[i]).valor;
    end;

    for i := 0 to FCampos.FDados.Count - 1 do begin
      if TCampo(FCampos.FDados[i]).read_only then
        Continue;

      SetLength(vParametros, Length(vParametros) + 1);
      vParametros[High(vParametros)] := TCampo(FCampos.FDados[i]).valor;
    end;

    vExec.SQL.Add(FSql);

    for i := Low(vParametros) to High(vParametros) do
      vExec.Params[i].Value := vParametros[i];

    vExec.UnPrepare;
    vExec.Prepare;

    try
      vExec.ExecSQL;
    except
      on e: Exception do begin
        FLogs.AddLog(e.Message);
        FLogs.AddLog('SQL = ' + FSql);
        FLogs.AddLog('Par�metros utilizados');
        for i := Low(vParametros) to High(vParametros) do begin
          if VarIsNull(vParametros[i]) then
            FLogs.AddLog('Null')
          else
            FLogs.AddLog(vParametros[i]);
        end;

        raise;
      end;
    end;
  finally
    FreeAndNil(vExec);
  end;
end;

function TOperacoes.Excluir: Boolean;
var
  i: Integer;
  execucao: TOraQuery;
  parametros: array of Variant;
begin
  try
    Result := False;
    execucao := TOraQuery.Create(nil);
    FSql := 'delete from ' + FTabela + ' where ';

    SetLength(parametros, FChaves.FDados.Count);
    for i := 0 to FChaves.FDados.Count - 1 do begin
      FSql := FSql + '  ' + TCampo(FChaves.FDados[i]).nome + ' = :P' + IntToStr(i + 1) + ' and ';
      parametros[i] := TCampo(FChaves.FDados[i]).valor;
    end;

    Delete(FSql, Length(FSql) - 4, 5);
    execucao.SQL.Add(FSql);

    for i := Low(parametros) to High(parametros) do
      execucao.Params[i].Value := parametros[i];

    execucao.UnPrepare;
    execucao.Prepare;

    execucao.ExecSQL;
    Result := True;
  finally
    FreeAndNil(execucao);
  end;
end;

function TOperacoes.Pesquisar(pIndice: Integer; pParametros: array of Variant): Boolean;
var
  i: Integer;
  vParametro: string;
begin
  FExecucao.Clear;
  FExecucao.Add(FSql);
  FExecucao.Add(FFiltros[pIndice].sql);
  FExecucao.Add(FFiltrosString);
  FExecucao.Add(FFiltros[pIndice].GroupBy);
  FExecucao.Add(FFiltros[pIndice].OrderBy);

  try
    FExecucao.UnPrepare;
    FExecucao.Prepare;

    for i := Low(pParametros) to High(pParametros) do begin
      if not FFiltros[pIndice].PesquisaAvancada then
        FExecucao.Params[i].Value := pParametros[i]
      else begin
        if VarIsType(pParametros[i], 258) then begin
          vParametro := StringReplace(pParametros[i], ' ', '%', [rfReplaceAll]);
          FExecucao.Params[i].AsString := vParametro;
        end
        else
          FExecucao.Params[i].Value := pParametros[i];
      end;
    end;

    FExecucao.Active := True;
  except
    on e: Exception do begin
      FLogs.AddLog('Classe = ' + Self.ClassName);
      FLogs.AddLog(e.Message);
      FLogs.AddLog('SQL: ' + FSql);
      FLogs.AddLog('Filtro: ' + FFiltros[pIndice].sql);
      FLogs.AddLog('Filtro aux: ' + FFiltrosString);
      raise;
    end;
  end;

  Result := not FExecucao.IsEmpty;

  if not Result then
    Exit;

  FExecucao.First;

  while not FExecucao.Eof do begin
    Inc(FQuantidadeRegistros);
    FExecucao.Next;
  end;

  FExecucao.First;
  PreencherCampos;
end;

function TOperacoes.Pesquisar(pIndice: Integer; pParametros: array of Variant; pFiltrosAuxiliar: string): Boolean;
begin
  SetFiltros(pFiltrosAuxiliar);
  Result := Pesquisar(pIndice, pParametros);
end;

function TOperacoes.PesquisarComando(pComando: string): Boolean;
begin
  FExecucao.SQL.Clear;
  FExecucao.SQL.Add(FSql);
  FExecucao.SQL.Add(pComando);

  try
    FExecucao.UnPrepare;
    FExecucao.Prepare;

    FExecucao.Active := True;
  except
    on e: Exception do begin
      FLogs.AddLog('Classe = ' + Self.ClassName + '. Proc = PesquisarComando');
      FLogs.AddLog(e.Message);
      FLogs.AddLog('SQL: ' + FSql);
      FLogs.AddLog('Comando: ' + pComando);
      raise;
    end;
  end;

  Result := not FExecucao.IsEmpty;

  if not Result then
    Exit;

  FExecucao.First;

  while not FExecucao.Eof do begin
    Inc(FQuantidadeRegistros);
    FExecucao.Next;
  end;

  FExecucao.First;
  PreencherCampos;
end;

procedure TOperacoes.ProximoRegistro;
begin
  FExecucao.Next;
  PreencherCampos;
end;

function TOperacoes.Atualizar: Boolean;
var
  i: Integer;
  vExec: TOraQuery;
  vParametros: array of Variant;

  vTemCamposAtualizar: Boolean;
begin
  vTemCamposAtualizar := False;
  for i := 0 to FCampos.FDados.Count - 1 do begin
    if TCampo(FCampos.FDados[i]).read_only then
      Continue;

    vTemCamposAtualizar := True;
    Break;
  end;

  if not vTemCamposAtualizar then begin
    Result := True;
    Exit;
  end;

  try
    Result := False;
    vParametros := nil;
    vExec := TOraQuery.Create(nil);
    FSql := 'update ' + FTabela + ' set ' + chr(13) + Chr(10);

    for i := 0 to FCampos.FDados.Count - 1 do begin
      if TCampo(FCampos.FDados[i]).read_only then
        Continue;

      FSql := FSql + '  ' + TCampo(FCampos.FDados[i]).nome + ' = :P' + IntToStr(i + 1) + ', ';

      SetLength(vParametros, Length(vParametros) + 1);
      vParametros[High(vParametros)] := TCampo(FCampos.FDados[i]).valor;
    end;

    Delete(FSql, Length(FSql) - 1, 2);

    if FChaves.FDados.Count > 0 then begin
      FSql := FSql + ' where ' + Chr(13) + Chr(10);

      for i := 0 to FChaves.FDados.Count - 1 do begin
        FSql := FSql + '  ' + TCampo(FChaves.FDados[i]).nome + ' = :P' + IntToStr(FCampos.FDados.Count + i + 1) + ' and ';
        SetLength(vParametros, Length(vParametros) + 1);
        vParametros[High(vParametros)] := TCampo(FChaves.FDados[i]).valor;
      end;

      Delete(FSql, Length(FSql) - 4, 5);
    end;

    vExec.SQL.Add(FSql);

    for i := Low(vParametros) to High(vParametros) do
      vExec.Params[i].Value := vParametros[i];

    vExec.UnPrepare;
    vExec.Prepare;

    try
      vExec.ExecSQL;
    except
      on e: Exception do begin
        FLogs.AddLog(e.Message);
        FLogs.AddLog('SQL = ' + FSql);
        FLogs.AddLog('Par�metros utilizados');
        for i := Low(vParametros) to High(vParametros) do begin
          if VarIsNull(vParametros[i]) then
            FLogs.AddLog('Null')
          else
            FLogs.AddLog(vParametros[i]);
        end;

        raise;
      end;
    end;

    Result := True;
  finally
    FreeAndNil(vExec);
  end;
end;

function TOperacoes.GetQuantidadeRegistros: Integer;
begin
  Result := FQuantidadeRegistros;
end;

procedure TOperacoes.SetFiltros(pFiltros: string);
begin
  FFiltrosString := FFiltrosString + pFiltros;
end;

procedure TOperacoes.SetFiltros(pFiltros: TArray<RecFiltros>);
begin
  FFiltros := pFiltros;
end;

procedure TOperacoes.setInt(const pColuna: string; const pValor: Integer; const pChave: Boolean);
begin
  if pChave then
    FChaves.SetValor(pColuna, pValor)
  else
    FCampos.SetValor(pColuna, pValor);
end;

procedure TOperacoes.setInt(const pColuna: Integer; const pValor: Integer; const pChave: Boolean);
begin
  if pChave then
    FChaves.SetValor(pColuna, pValor)
  else
    FCampos.SetValor(pColuna, pValor);
end;

procedure TOperacoes.setIntN(const pColuna: string; const pValor: Integer; const pChave: Boolean);
begin
  if pChave then begin
    if pValor = 0 then
      FChaves.SetValor(pColuna, null)
    else
      FChaves.SetValor(pColuna, pValor)
  end
  else begin
    if pValor = 0 then
      FCampos.SetValor(pColuna, null)
    else
      FCampos.SetValor(pColuna, pValor)
  end;
end;

procedure TOperacoes.setIntN(const pColuna: Integer; const pValor: Integer; const pChave: Boolean);
begin
  if pChave then begin
    if pValor = 0 then
      FChaves.SetValor(pColuna, null)
    else
      FChaves.SetValor(pColuna, pValor)
  end
  else begin
    if pValor = 0 then
      FCampos.SetValor(pColuna, null)
    else
      FCampos.SetValor(pColuna, pValor)
  end;
end;

procedure TOperacoes.setData(const pColuna: Integer; const pValor: TDateTime; const pChave: Boolean);
begin
  if pChave then
    FChaves.SetValor(pColuna, pValor)
  else
    FCampos.SetValor(pColuna, pValor);
end;

procedure TOperacoes.setData(const pColuna: string; const pValor: TDateTime; const pChave: Boolean);
begin
  if pChave then
    FChaves.SetValor(pColuna, pValor)
  else
    FCampos.SetValor(pColuna, pValor);
end;

procedure TOperacoes.setDataN(const pColuna: string; const pValor: TDateTime; const pChave: Boolean);
begin
  if pChave then begin
    if pValor = 0 then
      FChaves.SetValor(pColuna, null)
    else
      FChaves.SetValor(pColuna, pValor)
  end
  else begin
    if pValor = 0 then
      FCampos.SetValor(pColuna, null)
    else
      FCampos.SetValor(pColuna, pValor)
  end;
end;

procedure TOperacoes.setDataN(const pColuna: Integer; const pValor: TDateTime; const pChave: Boolean);
begin
  if pChave then begin
    if pValor = 0 then
      FChaves.SetValor(pColuna, null)
    else
      FChaves.SetValor(pColuna, pValor)
  end
  else begin
    if pValor = 0 then
      FCampos.SetValor(pColuna, null)
    else
      FCampos.SetValor(pColuna, pValor)
  end;
end;

procedure TOperacoes.setDouble(const pColuna: string; const pValor: Double; const pChave: Boolean);
begin
  if pChave then
    FChaves.SetValor(pColuna, pValor)
  else
    FCampos.SetValor(pColuna, pValor);
end;

procedure TOperacoes.setDouble(const pColuna: Integer; const pValor: Double; const pChave: Boolean);
begin
  if pChave then
    FChaves.SetValor(pColuna, pValor)
  else
    FCampos.SetValor(pColuna, pValor);
end;

procedure TOperacoes.setDoubleN(const pColuna: string; const pValor: Double; const pChave: Boolean);
begin
  if pChave then begin
    if pValor = 0 then
      FChaves.SetValor(pColuna, null)
    else
      FChaves.SetValor(pColuna, pValor)
  end
  else begin
    if pValor = 0 then
      FCampos.SetValor(pColuna, null)
    else
      FCampos.SetValor(pColuna, pValor)
  end;
end;

procedure TOperacoes.setDoubleN(const pColuna: Integer; const pValor: Double; const pChave: Boolean);
begin
  if pChave then begin
    if pValor = 0 then
      FChaves.SetValor(pColuna, null)
    else
      FChaves.SetValor(pColuna, pValor)
  end
  else begin
    if pValor = 0 then
      FCampos.SetValor(pColuna, null)
    else
      FCampos.SetValor(pColuna, pValor)
  end;
end;

procedure TOperacoes.setString(const pColuna, pValor: string; const pChave: Boolean);
begin
  if pChave then
    FChaves.SetValor(pColuna, pValor)
  else
    FCampos.SetValor(pColuna, pValor);
end;

procedure TOperacoes.setString(const pColuna: Integer; const pValor: string; const pChave: Boolean);
begin
  if pChave then
    FChaves.SetValor(pColuna, pValor)
  else
    FCampos.SetValor(pColuna, pValor);
end;

procedure TOperacoes.setStringN(const pColuna: Integer; const pValor: string; const pChave: Boolean);
begin
  if pChave then begin
    if pValor = '' then
      FChaves.SetValor(pColuna, null)
    else
      FChaves.SetValor(pColuna, pValor)
  end
  else begin
    if pValor = '' then
      FCampos.SetValor(pColuna, null)
    else
      FCampos.SetValor(pColuna, pValor)
  end;
end;

procedure TOperacoes.setStringN(const pColuna, pValor: string; const pChave: Boolean);
begin
  if pChave then begin
    if pValor = '' then
      FChaves.SetValor(pColuna, null)
    else
      FChaves.SetValor(pColuna, pValor)
  end
  else begin
    if pValor = '' then
      FCampos.SetValor(pColuna, null)
    else
      FCampos.SetValor(pColuna, pValor)
  end;
end;

function TOperacoes.AddColuna(const pColuna: string; const pChave: Boolean = False): Integer;
begin
  if pChave then
    Result := FChaves.SetColuna(pColuna, False)
  else
    Result := FCampos.SetColuna(pColuna, False);
end;

function TOperacoes.AddColunaSL(const pColuna: string; const pChave: Boolean = False): Integer; // Somente Leitura esta coluna
begin
  if pChave then
    Result := FChaves.SetColuna(pColuna, True)
  else
    Result := FCampos.SetColuna(pColuna, True);
end;

function TOperacoes.getInt(const pColuna: string; const pChave: Boolean = False): Integer;
begin
  if pChave then
    Result := FChaves.GetValorInteger(pColuna)
  else
    Result := FCampos.GetValorInteger(pColuna);
end;

function TOperacoes.getInt(const pColuna: Integer; const pChave: Boolean = False): Integer;
begin
  if pChave then
    Result := FChaves.GetValorInteger(pColuna)
  else
    Result := FCampos.GetValorInteger(pColuna);
end;

function TOperacoes.getString(const pColuna: string; const pChave: Boolean = False): string;
begin
  if pChave then
    Result := FChaves.GetValorString(pColuna)
  else
    Result := FCampos.GetValorString(pColuna);
end;

function TOperacoes.getString(const pColuna: Integer; const pChave: Boolean = False): string;
begin
  if pChave then
    Result := FChaves.GetValorString(pColuna)
  else
    Result := FCampos.GetValorString(pColuna);
end;

function TOperacoes.getDouble(const pColuna: string; const pChave: Boolean = False): Double;
begin
  if pChave then
    Result := FChaves.GetValorDouble(pColuna)
  else
    Result := FCampos.GetValorDouble(pColuna);
end;

function TOperacoes.getDouble(const pColuna: Integer; const pChave: Boolean = False): Double;
begin
  if pChave then
    Result := FChaves.GetValorDouble(pColuna)
  else
    Result := FCampos.GetValorDouble(pColuna);
end;

function TOperacoes.getData(const pColuna: string; const pChave: Boolean = False): TDateTime;
begin
  if pChave then
    Result := FChaves.GetValorData(pColuna)
  else
    Result := FCampos.GetValorData(pColuna);
end;

function TOperacoes.getData(const pColuna: Integer; const pChave: Boolean = False): TDateTime;
begin
  if pChave then
    Result := FChaves.GetValorData(pColuna)
  else
    Result := FCampos.GetValorData(pColuna);
end;

function NovoRecFiltro(
  pDescricaoFiltro: string;
  pFiltroPadrao: Boolean;
  pIndice: Integer;
  pSQL: string;
  pOrderBy: string = '';
  pGroupBy: string = '';
  pPesquisaAvancada: Boolean = False
): RecFiltros;
begin
  Result.Sql              := pSQL;
  Result.OrderBy          := pOrderBy;
  Result.GroupBy          := pGroupBy;
  Result.Indice           := pIndice;
  Result.FiltroPadrao     := pFiltroPadrao;
  Result.DescricaoFiltro  := pDescricaoFiltro;
  Result.PesquisaAvancada := pPesquisaAvancada;
end;

function NovoRecFiltro(
  pDescricaoFiltro: string;
  pFiltroPadrao: Boolean;
  pIndice: Integer;
  pSQL: string;
  pPesquisaAvancada: Boolean
): RecFiltros;
begin
  Result :=
    NovoRecFiltro(
      pDescricaoFiltro,
      pFiltroPadrao,
      pIndice,
      pSQL,
      '',
      '',
      pPesquisaAvancada
    );
end;

procedure TProcedimentoBanco.Executar;
begin
  try
    ExecProc;
  except
    on e: Exception do begin
      FLogs.AddLog(e.Message);
      raise;
    end;
  end;
end;

procedure TProcedimentoBanco.Executar(pParametros: TArray<Variant>);
var
  i: Integer;
begin
  for i := Low(pParametros) to High(pParametros) do
    Params[i].Value := pParametros[i];

  Executar;
end;

{ TScript }

procedure TScript.Add(pSql: string);
begin
  SQL.Add(pSql)
end;

constructor TScript.Create(pConexao: TComponent);
begin
  inherited Create(pConexao);
  Session               := TOraSession(pConexao);
  FOracleHandle         := TOraErrorHandler.Create(pConexao);
  FOracleHandle.Session := TOraSession(pConexao);
  FOracleHandle.OnError := OraErrorHandlerError;
end;

destructor TScript.Destroy;
begin
  FOracleHandle.Free;
  inherited;
end;

procedure TScript.Executar;
begin
  Execute;
end;

procedure TScript.Limpar;
begin
  SQL.Clear;
end;

procedure TScript.OraErrorHandlerError(Sender: TObject; E: Exception; ErrorCode: Integer; const ConstraintName: string; var Msg: string);
begin
  raise Exception.Create(e.Message);
end;

end.
