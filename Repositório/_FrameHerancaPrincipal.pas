unit _FrameHerancaPrincipal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs;

type
  TFrameHerancaPrincipal = class(TFrame)
    procedure ProximoCampo(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Numeros(Sender: TObject; var Key: Char);
    procedure NumerosTraco(Sender: TObject; var Key: Char);
    procedure NumerosPonto(Sender: TObject; var Key: Char);
    procedure NumerosPontoNegativo(Sender: TObject; var Key: Char);
    procedure NumerosPontoSemMascara(Sender: TObject; var Key: Char);
    procedure TextoSemEspacoSemTracos(Sender: TObject; var Key: Char);
    procedure LimparEspaco(Sender: TObject; var Key: Char);
  protected
    FEditandoLocal: Boolean;
    FSomenteLeitura: Boolean;
  public
    constructor Create(AOwner: TComponent); override;

    procedure Clear; virtual; abstract;
    procedure SomenteLeitura(pValue: Boolean); virtual;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); virtual;

    function EmEdicao: Boolean;
    function EstaVazio: Boolean; virtual;
  end;

implementation

uses
  _Biblioteca;

{$R *.dfm}

{ TFrameHerancaPrincipal }

constructor TFrameHerancaPrincipal.Create(AOwner: TComponent);
begin
  inherited;
  FEditandoLocal := True;
  FSomenteLeitura := False;
end;

function TFrameHerancaPrincipal.EmEdicao: Boolean;
begin
  Result := FEditandoLocal;
end;

function TFrameHerancaPrincipal.EstaVazio: boolean;
begin
  Result := False;
end;

procedure TFrameHerancaPrincipal.LimparEspaco(Sender: TObject; var Key: Char);
begin
  if Key = ' ' then
    Key := #0;

  Key := upcase(Key);
end;

procedure TFrameHerancaPrincipal.Modo(pEditando, pLimpar: Boolean);
begin
  Enabled := True;
  TabStop := pEditando;
  FEditandoLocal := pEditando;
end;

procedure TFrameHerancaPrincipal.ProximoCampo(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    SelectNext(TWinControl(Sender), True, True);
end;

procedure TFrameHerancaPrincipal.SomenteLeitura(pValue: Boolean);
begin
  FSomenteLeitura := pValue;
end;

procedure TFrameHerancaPrincipal.Numeros(Sender: TObject; var Key: Char);
begin
  if not CharInSet(Key, [#13,#8]) then begin
    if _Biblioteca.RetornaNumeros(Key) = '' then
      Key := #0;
  end;
end;

procedure TFrameHerancaPrincipal.NumerosPonto(Sender: TObject; var Key: Char);
begin
  if CharInSet(Key, [',','.']) then
    Key := ','
  else if not CharInSet(Key, [#13,#8]) then begin
    if _Biblioteca.RetornaNumeros(Key) = '' then
      Key := #0;
  end;
end;

procedure TFrameHerancaPrincipal.NumerosPontoNegativo(Sender: TObject; var Key: Char);
begin
  if CharInSet(Key, [',','.']) then
    Key := ','
  else if not CharInSet(Key, [#13,#8, #45]) then begin
    if _Biblioteca.RetornaNumeros(Key) = '' then
      Key := #0;
  end;
end;

procedure TFrameHerancaPrincipal.NumerosPontoSemMascara(Sender: TObject; var Key: Char);
begin
  if not CharInSet(Key, ['0'..'9', #46, #48, #57, #8]) then
    Key := #0;
end;

procedure TFrameHerancaPrincipal.NumerosTraco(Sender: TObject; var Key: Char);
begin
  if CharInSet(Key, ['.','-',',']) then
    Key := '-'
  else if not CharInSet(Key, [#13,#8]) then begin
    if _Biblioteca.RetornaNumeros(Key) = '' then
      Key := #0;
  end;
end;

procedure TFrameHerancaPrincipal.TextoSemEspacoSemTracos(Sender: TObject; var Key: Char);
begin
  Key := upcase(Key);

  if Key = ' ' then
    Key := #0
  else if Key = '-' then
    Key := #0;
end;

end.
