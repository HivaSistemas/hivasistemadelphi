unit _HerancaPesquisasAvancadas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPrincipal, Vcl.Grids, GridLuka, _Biblioteca,
  _RecordsEspeciais;

type
  TFormHerancaPesquisasAvancadas = class(TFormHerancaPrincipal)
    sgPesquisa: TGridLuka;
    procedure sgPesquisaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgPesquisaEnter(Sender: TObject);
    procedure ObjetoPesquisaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgPesquisaDblClick(Sender: TObject);
  private
    FSqlAvancado: string;
  protected
    procedure addFiltro( pSql: string; pValor: Integer; pFecharParentesis: Boolean = False ); overload;
    procedure addFiltro( pSql: string; pValor: string ); overload;

    procedure BuscarRegistros; virtual;
    procedure VerificarRegistro; virtual;
    function getSqlAvancado: string;
  end;

implementation

{$R *.dfm}

{ TFormHerancaPesquisasAvancadas }

procedure TFormHerancaPesquisasAvancadas.addFiltro(pSql, pValor: string);
begin
  if pValor <> '' then
    _Biblioteca.VerAnd(FSqlAvancado, pSql + pValor);
end;

procedure TFormHerancaPesquisasAvancadas.BuscarRegistros;
begin
  inherited;
  sgPesquisa.ClearGrid;
  VerificarRegistro;
end;

function TFormHerancaPesquisasAvancadas.getSqlAvancado: string;
begin
  Result := FSqlAvancado;
end;

procedure TFormHerancaPesquisasAvancadas.ObjetoPesquisaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    BuscarRegistros;
end;

procedure TFormHerancaPesquisasAvancadas.addFiltro( pSql: string; pValor: Integer; pFecharParentesis: Boolean = False );
begin
  if pValor > -1 then
    _Biblioteca.VerAnd(FSqlAvancado, pSql + IntToStr(pValor) + IIfStr(pFecharParentesis, ')') );
end;

procedure TFormHerancaPesquisasAvancadas.sgPesquisaDblClick(Sender: TObject);
begin
  inherited;
  if sgPesquisa.Cells[0, sgPesquisa.Row] = '' then begin
    Exclamar('Nenhum registro foi selecionado!');
    Exit;
  end;

  ModalResult := mrOk;
end;

procedure TFormHerancaPesquisasAvancadas.sgPesquisaEnter(Sender: TObject);
begin
  inherited;
  BuscarRegistros;
end;

procedure TFormHerancaPesquisasAvancadas.sgPesquisaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    sgPesquisaDblClick(Sender);
end;

procedure TFormHerancaPesquisasAvancadas.VerificarRegistro;
begin
  // N�o faz nada
end;

end.
