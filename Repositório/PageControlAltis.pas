unit PageControlAltis;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.ComCtrls, _BibliotecaGenerica;

type
  TPageControlAltis = class(TPageControl)
  private
    FAbasCarregadas: TArray<Integer>;

    function getCarregouAba: Boolean;
    procedure setCarregouAba(pValor: Boolean = True);
  public
    constructor Create(AOwner: TComponent); override;

    procedure Limpar;
  published
    property CarregouAba: Boolean read getCarregouAba write setCarregouAba;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Altis', [TPageControlAltis]);
end;

{ TPageControlAltis }

constructor TPageControlAltis.Create(AOwner: TComponent);
begin
  inherited;
  FAbasCarregadas := nil;
end;

function TPageControlAltis.getCarregouAba: Boolean;
begin
  Result := _BibliotecaGenerica.Em( Self.TabIndex, FAbasCarregadas );
end;

procedure TPageControlAltis.Limpar;
begin
  FAbasCarregadas := nil;
end;

procedure TPageControlAltis.setCarregouAba(pValor: Boolean);
begin
  _BibliotecaGenerica.AddNoVetorSemRepetir(FAbasCarregadas, Self.TabIndex);
end;

end.
