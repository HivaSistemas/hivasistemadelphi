unit _Notificacoes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls,
  Vcl.Imaging.GIFImg, Vcl.ExtCtrls;

type
  TFormNotificacoes = class(TForm)
    meNotificacoes: TRichEdit;
  private
    { Private declarations }
  public
    procedure SetCorNotificacao(pCor: TColor);
    procedure Notificar(pNotificacao: string);
  end;

implementation

{$R *.dfm}

{ TFormNotificacoes }

procedure TFormNotificacoes.Notificar(pNotificacao: string);
begin
  meNotificacoes.Lines.Add('--> ' + pNotificacao);
  meNotificacoes.Refresh;
  meNotificacoes.Repaint;
end;

procedure TFormNotificacoes.SetCorNotificacao(pCor: TColor);
begin
  meNotificacoes.Font.Color := pCor;
end;

end.
