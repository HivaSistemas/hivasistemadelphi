inherited FormHerancaRelatoriosPageControl: TFormHerancaRelatoriosPageControl
  Caption = 'Heran'#231'a de Relat'#243'rios com PageControl'
  ClientHeight = 547
  ClientWidth = 1014
  ExplicitWidth = 1020
  ExplicitHeight = 576
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 547
    ExplicitHeight = 547
    inherited sbCarregar: TSpeedButton
      Left = 3
      ExplicitLeft = 3
    end
    inherited sbImprimir: TSpeedButton
      Left = 3
      Top = 101
      ParentBiDiMode = False
      Visible = True
      ExplicitLeft = 3
      ExplicitTop = 101
    end
    inherited ckOmitirFiltros: TCheckBoxLuka
      Top = 176
      ExplicitTop = 176
    end
  end
  object pcDados: TPageControl
    Left = 122
    Top = 0
    Width = 892
    Height = 547
    ActivePage = tsFiltros
    Align = alClient
    TabOrder = 1
    object tsFiltros: TTabSheet
      Caption = 'Filtros'
    end
    object tsResultado: TTabSheet
      Caption = 'Resultado'
      ImageIndex = 2
    end
  end
end
