unit _RecordsEspeciais;

interface

uses
  System.SysUtils;

type
  RecRetornoBD = record
    TeveErro: Boolean;
    MensagemErro: string;
    MensagemErroCompleta: string;

    (* Guardando retornos necessários do banco de dados *)
    AsString: string;
    AsInt: Integer;
    AsInt2: Integer;

    AsArrayString: TArray<string>;
    AsArrayInt: TArray<Integer>;

    procedure Iniciar;
    procedure AddInt(pValor: Integer);
    procedure AddString(pValor: string);
    procedure TratarErro(pException: Exception);
  end;

  RecFiltros = record
    DescricaoFiltro: string;
    FiltroPadrao: Boolean;
    Indice: Byte;
    Sql: string;
    OrderBy: string;
    GroupBy: string;
    PesquisaAvancada: Boolean;
  end;

  RecFiltrosPesquisas = record
    Coluna: string;
    Valor: Variant;
  end;

  {$M+}
  RecRetornoWS = class(TObject)
  private
    FCodigo: Integer;
    FMensagemErro: string;
    FTeveErro: Boolean;
    FAsString: string;
    FAsInt: Integer;
  public
    constructor Create;
    procedure TratarErro(pExcecao: Exception);
//  private
//    procedure setCodigo(pValor: Integer);
//    procedure setMensagemErro(pValor: string);
//    procedure setTeveErro(pValor: string);
//    procedure setAsString(pValor: string);
  published
    property Codigo: Integer read FCodigo write FCodigo;
    property MensagemErro: string read FMensagemErro write FMensagemErro;
    property TeveErro: Boolean read FTeveErro write FTeveErro;
    property AsString: string read FAsString write FAsString;
    property AsInt: Integer read FAsInt write FAsInt;
  end;

implementation

{ RecRespostaBanco }

procedure RecRetornoBD.AddInt(pValor: Integer);
begin
  SetLength(AsArrayInt, Length(AsArrayInt) + 1);
  AsArrayInt[High(AsArrayInt)] := pValor;
end;

procedure RecRetornoBD.AddString(pValor: string);
begin
  SetLength(AsArrayString, Length(AsArrayString) + 1);
  AsArrayString[High(AsArrayString)] := pValor;
end;

procedure RecRetornoBD.Iniciar;
begin
  AsInt  := -1;
  AsInt2 := -1;
  AsString := '';
  TeveErro := False;
  AsArrayInt := nil;
  AsArrayString := nil;
end;

procedure RecRetornoBD.TratarErro(pException: Exception);
begin
  TeveErro             := True;
  MensagemErro         := pException.Message;
  MensagemErroCompleta := pException.Message;
end;

{ RecRetornoWebService }

constructor RecRetornoWS.Create;
begin
  TeveErro := False;
end;

procedure RecRetornoWS.TratarErro(pExcecao: Exception);
begin
  TeveErro := True;
  MensagemErro := pExcecao.Message;
end;

end.
