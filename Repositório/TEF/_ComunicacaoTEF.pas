unit _ComunicacaoTEF;

interface

uses
  System.SysUtils, System.Classes, System.StrUtils, Winapi.ShellAPI, Vcl.Forms, Winapi.Windows, _Biblioteca, _Logs;

{$M+}
type
  RecRespostaTEF = record
    CobrancaId: Integer;

    Aprovado: Boolean;
    TipoRecebimento: string;

    Nsu: string;
    CodigoAutorizacao: string;
    NumeroCartao: string;
    Finalizacao: string;

    Mensagem: string;

    Rede: string;
    Bandeira: string;

    DataOperacao: TDateTime;
    HoraOperacao: TDateTime;

    Valor: Double;
    QtdeParcelas: Integer;
    QtdeRegistros29: Integer;

    Comprovante1Via: TArray<string>;
    Comprovante2Via: TArray<string>;

    procedure Iniciar;
  end;

  TTEFDiscado = class(TObject)
  private
    FLogs: TLogs;
    FDiretorioRequisicao: string;
    FDiretorioResposta: string;
    FResposta: RecRespostaTEF;

    FTextoProcurarBandeira1: string;
    FPosicaoInicialProcBand1: Integer;
    FQtdeCaracteresCopiarBand1: Integer;

    FTextoProcurarBandeira2: string;
    FPosicaoInicialProcBand2: Integer;
    FQtdeCaracteresCopiarBand2: Integer;

    FTextoProcurarRede1: string;
    FPosicaoInicialProcRede1: Integer;
    FQtdeCaracteresCopiarRede1: Integer;

    FTextoProcurarRede2: string;
    FPosicaoInicialProcRede2: Integer;
    FQtdeCaracteresCopiarRede2: Integer;

    FTextoProcurarNumeroCartao1: string;
    FPosicIniProcNumeroCartao1: Integer;
    FQtdeCaracCopiarNrCartao1: Integer;

    FTextoProcurarNumeroCartao2: string;
    FPosicIniProcNumeroCartao2: Integer;
    FQtdeCaracCopiarNrCartao2: Integer;

    FTextoProcurarNsu1: string;
    FPosicaoIniProcurarNsu1: Integer;
    FQtdeCaracCopiarNsu1: Integer;

    FTextoProcurarNsu2: string;
    FPosicaoIniProcurarNsu2: Integer;
    FQtdeCaracCopiarNsu2: Integer;

    FTextoProcurarCodAutoriz1: string;
    FPosicaoIniProCodAutoriz1: Integer;
    FQtdeCaracCopCodAutoriz1: Integer;

    FTextoProcurarCodAutoriz2: string;
    FPosicaoIniProCodAutoriz2: Integer;
    FQtdeCaracCopCodAutoriz2: Integer;

    function getId001: string;
    procedure ProcessarOperacao( pOperacao: string; pId: string );
    procedure VerificarPastas;
    procedure ApagarBackupArquivosTEF;    

    procedure ApagarArquivosRespostaTEF;
    procedure ApagarArquivosRequisicaoTEF;    
    procedure ApagarArquivosTEF;    
  public
    constructor Create(
      AOwner: TComponent;
      pRequisicao: string;
      pResposta: string;

      pTextoProcurarBandeira1: string;
      pPosicaoInicialProcBand1: Integer;
      pQtdeCaracteresCopiarBand1: Integer;

      pTextoProcurarBandeira2: string;
      pPosicaoInicialProcBand2: Integer;
      pQtdeCaracteresCopiarBand2: Integer;

      pTextoProcurarRede1: string;
      pPosicaoInicialProcRede1: Integer;
      pQtdeCaracteresCopiarRede1: Integer;

      pTextoProcurarRede2: string;
      pPosicaoInicialProcRede2: Integer;
      pQtdeCaracteresCopiarRede2: Integer;

      pTextoProcurarNumeroCartao1: string;
      pPosicIniProcNumeroCartao1: Integer;
      pQtdeCaracCopiarNrCartao1: Integer;

      pTextoProcurarNumeroCartao2: string;
      pPosicIniProcNumeroCartao2: Integer;
      pQtdeCaracCopiarNrCartao2: Integer;

      pTextoProcurarNsu1: string;
      pPosicaoIniProcurarNsu1: Integer;
      pQtdeCaracCopiarNsu1: Integer;

      pTextoProcurarNsu2: string;
      pPosicaoIniProcurarNsu2: Integer;
      pQtdeCaracCopiarNsu2: Integer;

      pTextoProcurarCodAutoriz1: string;
      pPosicaoIniProCodAutoriz1: Integer;
      pQtdeCaracCopCodAutoriz1: Integer;

      pTextoProcurarCodAutoriz2: string;
      pPosicaoIniProCodAutoriz2: Integer;
      pQtdeCaracCopCodAutoriz2: Integer
    );

    function VerificarTEFAtivo(const pLimparArquivos: Boolean): Boolean;
    procedure AtivarTEF;

    procedure OperacaoADM;

    procedure EfetuarRecebimento(
      const pNumeroCupom: integer;
      const pValorOperacao: double;
      const pRede: string;
      const pParcelas: Integer
    );

    procedure EfetuarCNC(
      const pRede: string;
      const pNSU: string;
      const pFinalizacao: string;
      const pValorOperacao: Double;
      const pDataOperacao: TDateTime;
      const pHoraOperacao: TDateTime
    );

    procedure EfetuarNCN(
      const pRede: string;
      const pNSU: string;
      const pFinalizacao: string;
      const pValorOperacao: Double
    );

    procedure EnviarConfirmacao(const pNumeroCupom: Integer);

    procedure FazerBackupArquivosTEF;
  published
    property Resposta: RecRespostaTEF read FResposta;
  end;

implementation

const
  // Pastas do TEF
  coPastaAltis     = 'c:\Altis\';
  coPastaBackupTEF = 'c:\Altis\TEF\';

  // Nome dos arquivos do TEF
  coArquivoTemp     = 'IntPos.tmp';
  coArquivoReq 		  = 'IntPos.001';
  coArquivoResp		  = 'IntPos.001';
  coArquivoAtivo    = 'Ativo.001';
  coArquivoRespTemp = 'IntPos.Tmp';
  coArquivoStatus   = 'IntPos.Sts';

  // Mensagem padr�o que o TEF n�o est� ativo
  cMensagemGerenciadorInativo = 'O gerenciador padr�o n�o est� ativo!';

{ TTEFDiscado }

function FormatarValorTEF(const pValor: Double): string;
var
  i: Integer;
  s: string;
begin
  Result := '';
  s := FormatFloat('#0.00', pValor);
  for i := 1 to Length(s) do begin
    if CharInSet(s[i], ['0'..'9']) then
      Result := Result + s[i];
  end;
end;

procedure TTEFDiscado.ApagarArquivosRespostaTEF;
begin
  if FileExists(FDiretorioResposta + coArquivoRespTemp) then
    DeleteFile(PWideChar(FDiretorioResposta + coArquivoRespTemp));

  if FileExists(FDiretorioResposta + coArquivoResp) then
    DeleteFile(PWideChar(FDiretorioResposta + coArquivoResp));

  if FileExists(FDiretorioResposta + coArquivoStatus) then
    DeleteFile(PWideChar(FDiretorioResposta + coArquivoStatus));

  if FileExists(FDiretorioResposta + coArquivoAtivo) then
    DeleteFile(PWideChar(FDiretorioResposta + coArquivoAtivo));
end;

procedure TTEFDiscado.ApagarArquivosRequisicaoTEF;
begin
  if FileExists(FDiretorioRequisicao + coArquivoTemp) then
    DeleteFile(PWideChar(FDiretorioRequisicao + coArquivoTemp));

  if FileExists(FDiretorioRequisicao + coArquivoReq) then
    DeleteFile(PWideChar(FDiretorioRequisicao + coArquivoReq));
end;

procedure TTEFDiscado.ApagarArquivosTEF;
begin
  ApagarArquivosRespostaTEF;
  ApagarArquivosRequisicaoTEF;
end;

procedure TTEFDiscado.FazerBackupArquivosTEF;
var
  i: Integer;
  vArquivos: array[0..4] of string;
begin
  VerificarPastas;

  vArquivos[0] := coArquivoTemp;
  vArquivos[1] := coArquivoReq;
  vArquivos[2] := coArquivoResp;
  vArquivos[3] := coArquivoRespTemp;
  vArquivos[4] := coArquivoStatus;

  for i := Low(vArquivos) to High(vArquivos) do begin
    if FileExists(FDiretorioResposta + vArquivos[i]) then
      CopyFile(PWideChar(FDiretorioResposta + vArquivos[i]), PWideChar(coPastaBackupTEF + 'Resp\' + vArquivos[i]), True);
  end;
end;

procedure TTEFDiscado.ApagarBackupArquivosTEF;
var
  i: Integer;
  vArquivos: array[0..4] of string;
begin
  VerificarPastas;

  vArquivos[0] := coArquivoTemp;
  vArquivos[1] := coArquivoReq;
  vArquivos[2] := coArquivoResp;
  vArquivos[3] := coArquivoRespTemp;
  vArquivos[4] := coArquivoStatus;

  for i := Low(vArquivos) to High(vArquivos) do begin
    if FileExists(coPastaBackupTEF + 'Req' + vArquivos[i]) then
      DeleteFile( PWideChar(coPastaBackupTEF + 'Req' + vArquivos[i]) );

    if FileExists(coPastaBackupTEF + 'Resp' + vArquivos[i]) then
      DeleteFile( PWideChar(coPastaBackupTEF + 'Resp' + vArquivos[i]) );
  end;
end;

function getLinhaTEF(var pArquivo: TStrings; pRegistro: string): Integer;
var
  i: Integer;
begin
  Result := -1;

  if pArquivo = nil then
    Exit;

  for i := 0 to pArquivo.Count - 1 do begin
    if Copy(pArquivo[i], 1, Length(pRegistro)) <> pRegistro then
      Continue;

    Result := i;
    Break;
  end;
end;

procedure TTEFDiscado.EfetuarCNC(
  const pRede,
  pNSU,
  pFinalizacao: string;
  const pValorOperacao: Double;
  const pDataOperacao,
  pHoraOperacao: TDateTime
);
var
  vId: string;
  vArq: TStrings;
begin
  ApagarArquivosTEF;

  vId := getId001;

  vArq := TStringList.Create;

  // Gerando requisi��o do cart�o
  vArq.Add('000-000 = CNC');
  vArq.Add('001-000 = ' + vId);
  vArq.Add('003-000 = ' + FormatarValorTEF(pValorOperacao));
  vArq.Add('010-000 = ' + pRede);
  vArq.Add('012-000 = ' + pNSU);
  vArq.Add('022-000 = ' + FormatDateTime('ddmmyyyy', pDataOperacao));
  vArq.Add('023-000 = ' + FormatDateTime('hhmmnn', pHoraOperacao));
  vArq.Add('999-999 = 0');

  vArq.SaveToFile(FDiretorioRequisicao + coArquivoTemp);
  RenameFile(FDiretorioRequisicao + coArquivoTemp, FDiretorioRequisicao + coArquivoReq);

  vArq.Clear;
  vArq.Free;

  // Processando o TEF
  ProcessarOperacao('CNC', vId);
end;

procedure TTEFDiscado.EfetuarRecebimento(
  const pNumeroCupom: integer;
  const pValorOperacao: double;
  const pRede: string;
  const pParcelas: Integer
);
var
  vId: string;
  vArquivo: TStrings;
begin
  ApagarArquivosTEF;

  vId := getId001;

  vArquivo := TStringList.Create;

  vArquivo.Add('000-000 = CRT');
  vArquivo.Add('001-000 = ' + vId);

  if pNumeroCupom > 0 then
    vArquivo.Add('002-000 = ' + IntToStr(pNumeroCupom));

  vArquivo.Add('003-000 = ' + FormatarValorTEF(pValorOperacao));

  if Trim(pRede) <> '' then
    vArquivo.Add('010-000 = ' + pRede);

  if pParcelas = 0 then begin
    vArquivo.Add('731-000 = 2');
    vArquivo.Add('732-000 = 1');
  end
  else begin
    vArquivo.Add('018-000 = ' + IntToStr(pParcelas));
    vArquivo.Add('731-000 = 1');
    vArquivo.Add('732-000 = ' + IfThen(pParcelas = 1, '1', '3'));
  end;

  // Utilizado para homologa��o TEF
  // vArquivo.Add('777-777 = TESTE');

  vArquivo.Add('999-999 = 0');

  vArquivo.SaveToFile(FDiretorioRequisicao + coArquivoTemp);

  RenameFile(FDiretorioRequisicao + coArquivoTemp, FDiretorioRequisicao + coArquivoReq);

  vArquivo.Free;

  Sleep(2000);
  ProcessarOperacao('CRT', vId);
end;

procedure TTEFDiscado.EfetuarNCN(
  const pRede: string;
  const pNSU: string;
  const pFinalizacao: string;
  const pValorOperacao: Double
);
var
  vId: string;
  vArquivo: TStrings;
begin
  ApagarArquivosTEF;

  vId := getId001;

  vArquivo := TStringList.Create;

  // Gerando requisi��o do cart�o
  vArquivo.Add('000-000 = NCN');
  vArquivo.Add('001-000 = ' + vId);
  vArquivo.Add('003-000 = ' + FormatarValorTEF(pValorOperacao));
  vArquivo.Add('010-000 = ' + pRede);
  vArquivo.Add('012-000 = ' + pNSU);
  vArquivo.Add('027-000 = ' + pFinalizacao);
  vArquivo.Add('999-999 = 0');

  vArquivo.SaveToFile(FDiretorioRequisicao + coArquivoTemp);
  RenameFile(FDiretorioRequisicao + coArquivoTemp, FDiretorioRequisicao + coArquivoReq);

  vArquivo.Clear;
  vArquivo.Free;

  FResposta.Aprovado := True;
end;

procedure TTEFDiscado.AtivarTEF;
begin
  ShellExecute(Application.Handle, 'open', 'c:\tef_dial\tef_dial.exe', '', '', SW_SHOWNORMAL);
  Sleep(5000);
end;

constructor TTEFDiscado.Create(
  AOwner: TComponent;
  pRequisicao: string;
  pResposta: string;
  pTextoProcurarBandeira1: string;
  pPosicaoInicialProcBand1: Integer;
  pQtdeCaracteresCopiarBand1: Integer;

  pTextoProcurarBandeira2: string;
  pPosicaoInicialProcBand2: Integer;
  pQtdeCaracteresCopiarBand2: Integer;

  pTextoProcurarRede1: string;
  pPosicaoInicialProcRede1: Integer;
  pQtdeCaracteresCopiarRede1: Integer;

  pTextoProcurarRede2: string;
  pPosicaoInicialProcRede2: Integer;
  pQtdeCaracteresCopiarRede2: Integer;

  pTextoProcurarNumeroCartao1: string;
  pPosicIniProcNumeroCartao1: Integer;
  pQtdeCaracCopiarNrCartao1: Integer;

  pTextoProcurarNumeroCartao2: string;
  pPosicIniProcNumeroCartao2: Integer;
  pQtdeCaracCopiarNrCartao2: Integer;

  pTextoProcurarNsu1: string;
  pPosicaoIniProcurarNsu1: Integer;
  pQtdeCaracCopiarNsu1: Integer;

  pTextoProcurarNsu2: string;
  pPosicaoIniProcurarNsu2: Integer;
  pQtdeCaracCopiarNsu2: Integer;

  pTextoProcurarCodAutoriz1: string;
  pPosicaoIniProCodAutoriz1: Integer;
  pQtdeCaracCopCodAutoriz1: Integer;

  pTextoProcurarCodAutoriz2: string;
  pPosicaoIniProCodAutoriz2: Integer;
  pQtdeCaracCopCodAutoriz2: Integer
);
begin
  FLogs := TLogs.Create(AOwner, ExtractFileDir(Application.ExeName), 'TEF');
  FLogs.AddLog('Criando objeto de logs');

  FLogs.AddLog('Definindo caminhos de requisi��o e resposta');
  FDiretorioRequisicao := pRequisicao;
  FDiretorioResposta   := pResposta;

  FTextoProcurarBandeira1    := pTextoProcurarBandeira1;
  FPosicaoInicialProcBand1   := pPosicaoInicialProcBand1;
  FQtdeCaracteresCopiarBand1 := pQtdeCaracteresCopiarBand1;

  FTextoProcurarBandeira2    := pTextoProcurarBandeira2;
  FPosicaoInicialProcBand2   := pPosicaoInicialProcBand2;
  FQtdeCaracteresCopiarBand2 := pQtdeCaracteresCopiarBand2;

  FTextoProcurarRede1         := pTextoProcurarRede1;
  FPosicaoInicialProcRede1    := pPosicaoInicialProcRede1;
  FQtdeCaracteresCopiarRede1  := pQtdeCaracteresCopiarRede1;

  FTextoProcurarRede2         := pTextoProcurarRede2;
  FPosicaoInicialProcRede2    := pPosicaoInicialProcRede2;
  FQtdeCaracteresCopiarRede2  := pQtdeCaracteresCopiarRede2;

  FTextoProcurarNumeroCartao1 := pTextoProcurarNumeroCartao1;
  FPosicIniProcNumeroCartao1  := pPosicIniProcNumeroCartao1;
  FQtdeCaracCopiarNrCartao1   := pQtdeCaracCopiarNrCartao1;

  FTextoProcurarNumeroCartao2 := pTextoProcurarNumeroCartao2;
  FPosicIniProcNumeroCartao2  := pPosicIniProcNumeroCartao2;
  FQtdeCaracCopiarNrCartao2   := pQtdeCaracCopiarNrCartao2;

  FTextoProcurarNsu1      := pTextoProcurarNsu1;
  FPosicaoIniProcurarNsu1 := pPosicaoIniProcurarNsu1;
  FQtdeCaracCopiarNsu1    := pQtdeCaracCopiarNsu1;

  FTextoProcurarNsu2      := pTextoProcurarNsu2;
  FPosicaoIniProcurarNsu2 := pPosicaoIniProcurarNsu2;
  FQtdeCaracCopiarNsu2    := pQtdeCaracCopiarNsu2;

  FTextoProcurarCodAutoriz1 := pTextoProcurarCodAutoriz1;
  FPosicaoIniProCodAutoriz1 := pPosicaoIniProCodAutoriz1;
  FQtdeCaracCopCodAutoriz1  := pQtdeCaracCopCodAutoriz1;

  FTextoProcurarCodAutoriz2 := pTextoProcurarCodAutoriz2;
  FPosicaoIniProCodAutoriz2 := pPosicaoIniProCodAutoriz2;
  FQtdeCaracCopCodAutoriz2  := pQtdeCaracCopCodAutoriz2;

  VerificarPastas;
end;

procedure TTEFDiscado.EnviarConfirmacao(const pNumeroCupom: Integer);
var
  vArquivo: TStrings;
  vPosicao: Integer;
  vId: string;
begin
  FResposta.Iniciar;
  vId := getId001;

  try
    vArquivo := TStringList.Create;

    // Gerando confirma��o
    vArquivo.Add('000-000 = CNF');
    vArquivo.Add('001-000 = ' + vId);

    if pNumeroCupom > 0 then
      vArquivo.Add('002-000 = ' + IntToStr(pNumeroCupom));

    vArquivo.Add('010-000 = ' + FResposta.Rede);
    vArquivo.Add('012-000 = ' + FResposta.Nsu);
    vArquivo.Add('027-000 = ' + FResposta.Finalizacao);
    vArquivo.Add('999-999 = 0');

    vArquivo.SaveToFile(FDiretorioRequisicao + coArquivoTemp);

    RenameFile(FDiretorioRequisicao + coArquivoTemp, FDiretorioRequisicao + coArquivoReq);
    vArquivo.Clear;

    while True do begin
      if FileExists(FDiretorioResposta + coArquivoStatus) then begin
        Sleep(1000);

        vArquivo.LoadFromFile(FDiretorioResposta + coArquivoStatus);

         vPosicao := getLinhaTEF(vArquivo, '000');
        if vArquivo[vPosicao] <> '000-000 = CNF' then begin
          FResposta.Mensagem := 'O retorno n�o pertence � confirma��o da opera��o!';
          Exit;
        end;

        vArquivo.Clear;
        Break;
      end;
    end;
    FResposta.Aprovado := True;
    ApagarBackupArquivosTEF;
  finally
    ApagarArquivosTEF;
    vArquivo.Free;
  end;
end;

function TTEFDiscado.getId001: string;
begin
  Randomize;
  Result := IntToStr(Random(999999));
end;

procedure TTEFDiscado.OperacaoADM;
var
  vId: string;
  vArquivo: TStrings;
begin
  vId := getId001;
  ApagarArquivosTEF;
  vArquivo := TStringList.Create;

  // Gerando requisi��o das fun��es administrativas
  vArquivo.Add('000-000 = ADM');
  vArquivo.Add('001-000 = ' + vId);
  vArquivo.Add('999-999 = 0');

  vArquivo.SaveToFile(FDiretorioRequisicao + coArquivoTemp);

  RenameFile(FDiretorioRequisicao + coArquivoTemp, FDiretorioRequisicao + coArquivoReq);

  vArquivo.Clear;
  vArquivo.Free;

  Sleep(1000);
  ProcessarOperacao('ADM', vId);
end;

procedure TTEFDiscado.ProcessarOperacao( pOperacao: string; pId: string );
var
  i: Integer;
  k: Integer;
  vPosicao: Integer;
  vArquivo: TStrings;
  vLinhaTemp: string;
  vNumeroCartao: string;

  vContador: Integer;

  (*
    Neste procedimento precisamos buscar:
    1 - Bandeira do cart�o que foi passado
    2 - Rede do cart�o que foi passado
    3 - NSU
    4 - Numero do c�rt�o
    5 - C�digo de autoriza��o
  *)
  procedure BuscarDadosVinculoAutomatico(
    var pVariavel: string;
    pTextoProcurar: string;
    pPosicaoInicial: Integer;
    pQtdeCaracteresCopiar: Integer;
    pApenasNumeros: Boolean = False
  );
  var
    vLinha: Integer;

    function getLinhaTEFPos(var pArquivo: TStrings; pRegistro: string): Integer;
    var
      i: Integer;
    begin
      Result := -1;

      if pArquivo = nil then
        Exit;

      for i := 0 to pArquivo.Count - 1 do begin
        if Pos(pRegistro, pArquivo[i]) = 0 then
          Continue;

        Result := i;
        Break;
      end;
    end;

  begin
    // Se j� est� preenchido n�o fazer nada
    if pVariavel <> '' then
      Exit;

    vLinha := getLinhaTEFPos(vArquivo, pTextoProcurar);
    if vLinha > 0 then begin
      pVariavel := Trim(Copy(vArquivo[vLinha], pPosicaoInicial, pQtdeCaracteresCopiar));
      if pApenasNumeros then
        pVariavel := _Biblioteca.RetornaNumeros(pVariavel);
    end;
  end;

begin
  vContador := 0;
  FResposta.Iniciar;

  vArquivo := TStringList.Create;
  while True do begin
    if not FileExists(FDiretorioResposta + coArquivoResp) then begin
      Inc(vContador);
      if vContador > 90 then begin
        FResposta.Mensagem := 'Tempo excedido.';
        Exit;
      end;

      Sleep(1500);
      Continue;
    end;

    FazerBackupArquivosTEF;

    vArquivo.LoadFromFile(FDiretorioResposta + coArquivoResp);
    vPosicao := getLinhaTEF(vArquivo, '001-000');

    if vPosicao = -1 then
      Continue;

    try
      FLogs.AddLog('');
      FLogs.AddLog(LPad('', 48, '*'));
      FLogs.AddLog(vArquivo.Text);
      FLogs.AddLog(LPad('', 48, '*'));
      FLogs.AddLog('');
    except
      // N�o parar se ocorrer qualquer problema
    end;

    if vArquivo[vPosicao] <> '001-000 = ' + pID then begin
      DeleteFile( PWideChar(FDiretorioResposta + coArquivoResp) );
      Continue;
    end;

    vPosicao := getLinhaTEF(vArquivo, '000');
    if vPosicao = -1 then begin
      FResposta.Mensagem := 'O status de tipo de movimento (000) n�o foi encontrado!';
      ApagarArquivosTEF;
      Exit;
    end;

    if vArquivo[vPosicao] <> '000-000 = ' + pOperacao then begin
      FResposta.Mensagem := 'O retorno n�o pertence � requisi��o de cart�o!';
      ApagarArquivosTEF;
      Exit;
    end;

    vPosicao := getLinhaTEF(vArquivo, '003-000');
    if vPosicao > -1 then
      FResposta.Valor := Arredondar( SFormatDouble(Copy(vArquivo[vPosicao], 11, Length(vArquivo[vPosicao]))) * 0.01, 2);

    vPosicao := getLinhaTEF(vArquivo, '030');
    if vPosicao > -1 then
      FResposta.Mensagem := Copy(vArquivo[vPosicao], 11, Length(vArquivo[vPosicao]));

    vPosicao := getLinhaTEF(vArquivo, '018');
    if vPosicao = -1 then
      FResposta.QtdeParcelas := 1
    else begin
      FResposta.QtdeParcelas := StrToInt( Copy(vArquivo[vPosicao], 11, Length(vArquivo[vPosicao])) );
      if FResposta.QtdeParcelas = 0 then
        FResposta.QtdeParcelas := 1;
    end;

    vPosicao := getLinhaTEF(vArquivo, '013'); // C�digo de autoriza��o da transa��o
    if vPosicao > -1 then
      FResposta.CodigoAutorizacao := Copy(vArquivo[vPosicao], 11, Length(vArquivo[vPosicao]));

    vPosicao := getLinhaTEF(vArquivo, '028');
    if vPosicao = -1 then
      FResposta.QtdeRegistros29 := 0
    else
      FResposta.QtdeRegistros29 := StrToInt( Copy(vArquivo[vPosicao], 11, Length(vArquivo[vPosicao])) );

    vPosicao := getLinhaTEF(vArquivo, '009');
    if vPosicao = -1 then begin
      FResposta.Mensagem := 'O status de aprovado (009) n�o foi encontrado!';
      ApagarArquivosTEF;
      Exit;
    end;
    FResposta.Aprovado := (vArquivo[vPosicao] = '009-000 = 0');

    if FResposta.QtdeRegistros29 = 0 then begin
      ApagarArquivosTEF;
      Exit;
    end;

    vPosicao := getLinhaTEF(vArquivo, '010');
    if vPosicao = -1 then begin
      FResposta.Mensagem := 'O status de rede (010) n�o foi encontrado!';
      ApagarArquivosTEF;
      Exit;
    end;
    FResposta.Rede := Copy(vArquivo[vPosicao], 11, Length(vArquivo[vPosicao]));

    vPosicao := getLinhaTEF(vArquivo, '022');
    if vPosicao <> -1 then begin
      vLinhaTemp := Copy(vArquivo[vPosicao], 11, Length(vArquivo[vPosicao]));
      vLinhaTemp := Copy(vLinhaTemp, 1, 2) + '/' + Copy(vLinhaTemp, 3, 2) + '/' + Copy(vLinhaTemp, 5, 4);
      FResposta.DataOperacao := StrToDate(vLinhaTemp);
    end;

    vPosicao := getLinhaTEF(vArquivo, '023');
    if vPosicao <> -1 then begin
      vLinhaTemp := Copy(vArquivo[vPosicao], 11, Length(vArquivo[vPosicao]));
      vLinhaTemp := Copy(vLinhaTemp, 1, 2) + ':' + Copy(vLinhaTemp, 3, 2) + ':' + Copy(vLinhaTemp, 5, 4);
      FResposta.HoraOperacao := StrToTime(vLinhaTemp);
    end;

    vPosicao := getLinhaTEF(vArquivo, '012'); // NSU
    if vPosicao = -1 then begin
      if pOperacao = 'ADM' then
        FResposta.Nsu := ''
      else begin
        FResposta.Mensagem := 'O status de NSU (012) n�o foi encontrado!';
        ApagarArquivosTEF;
        Exit;
      end;
    end
    else
      FResposta.Nsu := Copy(vArquivo[vPosicao], 11, Length(vArquivo[vPosicao]));

    vPosicao := getLinhaTEF(vArquivo, '027');
    if vPosicao = -1 then begin
      if pOperacao = 'ADM' then
        FResposta.Finalizacao := ''
      else begin
        FResposta.Mensagem := 'O status de finaliza��o (027) n�o foi encontrado!';
        ApagarArquivosTEF;
        Exit;
      end;
    end
    else
      FResposta.Finalizacao := Copy(vArquivo[vPosicao], 11, Length(vArquivo[vPosicao]));

    for i := 0 to vArquivo.Count - 1 do begin
      if Copy(vArquivo[i], 1, 4) = '713-' then begin
        SetLength(FResposta.Comprovante1Via, Length(FResposta.Comprovante1Via) + 1);
        FResposta.Comprovante1Via[Length(FResposta.Comprovante1Via) - 1] := _Biblioteca.RemoverCaracter(Copy(vArquivo[i], 11, Length(vArquivo[i])), '"');
      end;
    end;

    for i := 0 to vArquivo.Count - 1 do begin
      if Copy(vArquivo[i], 1, 4) = '715-' then begin
        SetLength(FResposta.Comprovante2Via, Length(FResposta.Comprovante2Via) + 1);
        FResposta.Comprovante2Via[Length(FResposta.Comprovante2Via) - 1] := _Biblioteca.RemoverCaracter(Copy(vArquivo[i], 11, Length(vArquivo[i])), '"');
      end;
    end;

    if FResposta.Comprovante1Via = nil then begin
      for i := 0 to vArquivo.Count - 1 do begin
        if Copy(vArquivo[i], 1, 4) = '029-' then begin
          SetLength(FResposta.Comprovante1Via, Length(FResposta.Comprovante1Via) + 1);
          FResposta.Comprovante1Via[Length(FResposta.Comprovante1Via) - 1] := _Biblioteca.RemoverCaracter(Copy(vArquivo[i], 11, Length(vArquivo[i])), '"');
        end;
      end;
    end;

    // Bandeira
    BuscarDadosVinculoAutomatico(FResposta.Bandeira, FTextoProcurarBandeira1, FPosicaoInicialProcBand1, FQtdeCaracteresCopiarBand1);
    BuscarDadosVinculoAutomatico(FResposta.Bandeira, FTextoProcurarBandeira2, FPosicaoInicialProcBand2, FQtdeCaracteresCopiarBand2);

    // Rede
    BuscarDadosVinculoAutomatico(FResposta.Rede, FTextoProcurarRede1, FPosicaoInicialProcRede1, FQtdeCaracteresCopiarRede1);
    BuscarDadosVinculoAutomatico(FResposta.Rede, FTextoProcurarRede2, FPosicaoInicialProcRede2, FQtdeCaracteresCopiarRede2);

    // Numero do cart�o
    BuscarDadosVinculoAutomatico(FResposta.NumeroCartao, FTextoProcurarNumeroCartao1, FPosicIniProcNumeroCartao1, FQtdeCaracCopiarNrCartao1, True);
    BuscarDadosVinculoAutomatico(FResposta.NumeroCartao, FTextoProcurarNumeroCartao2, FPosicIniProcNumeroCartao2, FQtdeCaracCopiarNrCartao2, True);

    // C�digo de autoriza��o
    BuscarDadosVinculoAutomatico(FResposta.CodigoAutorizacao, FTextoProcurarCodAutoriz1, FPosicaoIniProCodAutoriz1, FQtdeCaracCopCodAutoriz1, True);
    BuscarDadosVinculoAutomatico(FResposta.CodigoAutorizacao, FTextoProcurarCodAutoriz2, FPosicaoIniProCodAutoriz2, FQtdeCaracCopCodAutoriz2, True);

    vArquivo.Clear;
    Break;
  end;
  vArquivo.Free;
end;

procedure TTEFDiscado.VerificarPastas;
begin
  FLogs.AddLog('Verificando as pastas necess�rias');

  if not DirectoryExists(coPastaAltis) then
    MkDir(coPastaAltis);

  if not DirectoryExists(coPastaBackupTEF) then
    MkDir(coPastaBackupTEF);

  if not DirectoryExists(coPastaBackupTEF + 'Req') then
    MkDir(coPastaBackupTEF + 'Req');

  if not DirectoryExists(coPastaBackupTEF + 'Resp') then
    MkDir(coPastaBackupTEF + 'Resp');
end;

function TTEFDiscado.VerificarTEFAtivo(const pLimparArquivos: Boolean): Boolean;
var
  vTempo: TDateTime;
  vArquivo: TStrings;
begin
  FLogs.AddLog('Function "VerificarTEFAtivo"');

  Result := False;
  vArquivo := TStringList.Create;

  if pLimparArquivos then
    ApagarArquivosRespostaTEF;

  vArquivo.Add('000-000 = ATV');
  vArquivo.Add('001-000 = ' + getId001);
  vArquivo.Add('999-999 = 0');

  vArquivo.SaveToFile(FDiretorioRequisicao + coArquivoRespTemp);
  RenameFile(FDiretorioRequisicao + coArquivoRespTemp, FDiretorioRequisicao + coArquivoReq);

  vArquivo.Clear;
  vArquivo.Free;

  vTempo := Now;
  while True do begin
    if FileExists(FDiretorioResposta + coArquivoStatus) then begin
      Result := True;
      Sleep(1000);
      Break;
    end;

    if StrToInt(FormatDateTime('ss', Now - vTempo)) > 7 then
      Break;
  end;

  if pLimparArquivos then
    ApagarArquivosRespostaTEF;

  FLogs.AddLog('Finalizou function "VerificarTEFAtivo"');
end;

{ RecRespostaTEF }

procedure RecRespostaTEF.Iniciar;
begin
  Valor := 0;
  Mensagem := '';
  QtdeParcelas := 0;
  QtdeRegistros29 := 0;

  DataOperacao := 0;
  HoraOperacao := 0;
  NumeroCartao := '';

  Comprovante1Via := nil;
  Comprovante2Via := nil;

  Aprovado := False;
  Mensagem := 'Mensagem n�o identificada.';
end;

end.
