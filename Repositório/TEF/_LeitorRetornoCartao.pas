unit _LeitorRetornoCartao;

interface

uses
  Classes, _Biblioteca, System.SysUtils, System.Types, System.StrUtils, Vcl.Dialogs, Vcl.Forms, _BibliotecaGenerica;

type
  RecComprovanteVendas = record
    CodigoRO: string;
    NumeroCartao: string;
    DataVenda: TDateTime;
    Valor: Currency;
    Parcela: Integer;
    QtdeParcelas: Integer;
    CodigoAutorizacao: string;
    Nsu: string;
    PercRetencao: Currency;
    Bandeira: string;
    TipoCartao: string;
    ValorRetencao: Currency;
    Conta: string;

    Altis: record
      ReceberId: Integer;
      ClienteId: Integer;
      NomeCliente: string;
      ValorDocumento: Currency;
      ValorRetencao: Currency;
      ValorLiquido: Currency;
      Status: string;
    end;
  end;

  RecResumoOperacoes = record
    CodigoRO: string;
    NumeroEstabelecimento: string;
    StatusPagto: string;
    ValorBruto: Currency;
    ValorRetencao: Currency;
    ValorLiquido: Currency;
    DataPagamento: TDateTime;
    QtdeCVs: Integer;
    ContaCorrente: string;
    DigitoConta: string;
    TipoCartao: string;

    Altis: record
      QtdeIdentificados: Integer;
      ValorBruto: Currency;
      ValorRetencao: Currency;
      ValorLiquido: Currency;
    end;
    ComprovantesVenda: TArray<RecComprovanteVendas>;
  end;

  TLeitorRetornoCartao = class(TObject)
  private
    FArquivo: TStringList;

    function getValorFormatado(pFormatarValor: string): Currency;
    function getBandeira(pBandeira: string): string;
    function getStatusAnalitico(pStatus: string): string;
  public
    constructor Create(pCaminhoArquivo: string);
    destructor Destroy; override;

    function getRetornoCielo: TArray<RecResumoOperacoes>;
  end;

implementation

{ TLeitorRetornoCartao }

constructor TLeitorRetornoCartao.Create(pCaminhoArquivo: string);
begin
  FArquivo := TStringList.Create;
  FArquivo.LoadFromFile(pCaminhoArquivo);
end;

destructor TLeitorRetornoCartao.Destroy;
begin
  FArquivo.Free;
  inherited;
end;

function TLeitorRetornoCartao.getBandeira(pBandeira: string): string;
begin
  Result :=
    _Biblioteca.Decode(
      pBandeira, [
      '001', 'Visa',
      '002', 'Mastercard',
      '006', 'Sorocred',
      '007', 'ELO',
      '009', 'Diners',
      '011', 'Agiplan',
      '015', 'Banescard',
      '023', 'Cabal',
      '029', 'Credsystem',
      '035', 'Esplanada',
      '064', 'Credz',
      'Desconhecido']
    );
end;

function TLeitorRetornoCartao.getValorFormatado(pFormatarValor: string): Currency;
begin
  Result := SFormatCurr(Copy(pFormatarValor, 1, Length(pFormatarValor) - 2) + ',' + Copy(pFormatarValor, Length(pFormatarValor) - 1, 2));
end;

function TLeitorRetornoCartao.getStatusAnalitico(pStatus: string): string;
begin
  Result :=
    _Biblioteca.Decode(
      pStatus, [
      '00', 'Agendado',
      '01', 'Pago',
      '02', 'Enviado p/ banco',
      '03', 'A confirmar']
    );
end;

function TLeitorRetornoCartao.getRetornoCielo: TArray<RecResumoOperacoes>;
var
  i: Integer;
  j: Integer;

  vLinha: string;
  vQuebra: TStringDynArray;

  vPosicRO: Integer;
  vPosicCV: Integer;
  vQtdeDigitosCartao: Integer;

  vPercRetencao: Currency;
  vBandeira: string;
  vTipoCartao: string;
  vConta: string;

  procedure GerarErro(pMensagemErro: string);
  begin
    raise Exception.Create( pMensagemErro );
  end;

begin
  Result := nil;
  try
    if FArquivo.Count < 3 then begin
      vLinha := FArquivo[0];
      vQuebra := SplitString(vLinha, ',');
      GerarErro('N�o houve movimenta��o no arquivo com data de processamento ' + Copy(vLinha, 18, 2)  + '/' + Copy(vLinha, 16, 2) + '/' + Copy(vLinha, 12, 4) + '!');
    end;

    for i := 0 to FArquivo.Count - 1 do begin
      if Length(FArquivo[i]) <> 250 then begin
        GerarErro(
          'O arquivo n�o est� no formato correto! A linha ' +
          _Biblioteca.NFormat(i + 1, 0) + ' deveria ter 250 posi��es mas est� com ' +
          _Biblioteca.NFormat(Length(FArquivo[i]), 0) + ' posi��es.'
        );
      end;
    end;

    if Copy(FArquivo[0], 48, 2) <> '04' then
      GerarErro('O arquivo que est� sendo processado n�o � um arquivo de pagamento!');

    vPosicRO      := -1;
    vPercRetencao := 0;
    vBandeira     := '';

    for i := 0 to FArquivo.Count -1 do begin
      vLinha := FArquivo[i];

      if not _Biblioteca.Em(Copy(FArquivo[i], 1, 1), ['1', '2']) then
        Continue;

      // Se for RO
      if Copy(FArquivo[i], 1, 1) = '1' then begin

        // Agrupando os resumo de opera��es se tiver o mesmo c�digo
        vPosicRO := -1;
        for j := Low(Result) to High(Result) do begin
          if Copy(vLinha, 12, 7) = Result[j].CodigoRO then begin
            vPosicRO := j;
            Break;
          end;
        end;

        if vPosicRO = -1 then begin
          SetLength(Result, Length(Result) + 1);
          vPosicRO := High(Result);

          Result[vPosicRO].ComprovantesVenda := nil;
        end;

        Result[vPosicRO].CodigoRO      := Copy(vLinha, 12, 7);
        Result[vPosicRO].StatusPagto   := getStatusAnalitico(Copy(vLinha, 123, 2));
        Result[vPosicRO].ValorBruto    := Result[vPosicRO].ValorBruto + getValorFormatado(Copy(vLinha, 45, 13));
        Result[vPosicRO].ValorRetencao := Result[vPosicRO].ValorRetencao + getValorFormatado(Copy(vLinha, 59, 13));
        Result[vPosicRO].ValorLiquido  := Result[vPosicRO].ValorLiquido + getValorFormatado(Copy(vLinha, 87, 13));
        Result[vPosicRO].DataPagamento := FormatarDataAnoMesDia(Copy(vLinha, 32, 6));
        Result[vPosicRO].QtdeCVs       := Result[vPosicRO].QtdeCVs + SFormatInt(Copy(vLinha, 125, 6));
        Result[vPosicRO].TipoCartao    := IIfStr(Copy(Result[vPosicRO].CodigoRO, 1, 1) = '5', 'D', 'C');

        // Formatando a conta onde entrou os valores
        Result[vPosicRO].ContaCorrente := Trim(Copy(vLinha, 109, 14));
        Result[vPosicRO].DigitoConta   := Trim(Copy(Result[vPosicRO].ContaCorrente, Length(Result[vPosicRO].ContaCorrente), 1));
        Result[vPosicRO].ContaCorrente := IntToStr(SFormatInt(Copy(Result[vPosicRO].ContaCorrente, 1, Length(Result[vPosicRO].ContaCorrente) - 1)));

        Result[vPosicRO].NumeroEstabelecimento := Copy(vLinha, 2, 10);

        vPercRetencao := getValorFormatado(Copy(vLinha, 210, 4));
        vBandeira     := getBandeira(Copy(vLinha, 185, 3));
        vTipoCartao   := IIfStr(Result[vPosicRO].TipoCartao = 'D', 'D�bito', 'Cr�dito');
        vConta        := Result[vPosicRO].ContaCorrente;
      end
      // Sen�o CV
      else begin
        SetLength(Result[vPosicRO].ComprovantesVenda, Length(Result[vPosicRO].ComprovantesVenda) + 1);
        vPosicCV := High(Result[vPosicRO].ComprovantesVenda);

        Result[vPosicRO].ComprovantesVenda[vPosicCV].CodigoRO          := Copy(vLinha, 12, 7);
        Result[vPosicRO].ComprovantesVenda[vPosicCV].DataVenda         := FormatarDataAnoMesDia(Copy(vLinha, 38, 8));
        Result[vPosicRO].ComprovantesVenda[vPosicCV].Valor             := getValorFormatado(Copy(vLinha, 47, 13));
        Result[vPosicRO].ComprovantesVenda[vPosicCV].Parcela           := zvl(SFormatInt(Copy(vLinha, 60, 2)), 1);
        Result[vPosicRO].ComprovantesVenda[vPosicCV].QtdeParcelas      := zvl(SFormatInt(Copy(vLinha, 62, 2)), 1);
        Result[vPosicRO].ComprovantesVenda[vPosicCV].Nsu               := Copy(vLinha, 93, 6);
        Result[vPosicRO].ComprovantesVenda[vPosicCV].CodigoAutorizacao := Copy(vLinha, 67, 6);
        Result[vPosicRO].ComprovantesVenda[vPosicCV].Conta             := vConta;

        Result[vPosicRO].ComprovantesVenda[vPosicCV].PercRetencao  := vPercRetencao;
        Result[vPosicRO].ComprovantesVenda[vPosicCV].Bandeira      := vBandeira;
        Result[vPosicRO].ComprovantesVenda[vPosicCV].TipoCartao    := vTipoCartao;
        Result[vPosicRO].ComprovantesVenda[vPosicCV].ValorRetencao := _Biblioteca.Arredondar(Result[vPosicRO].ComprovantesVenda[vPosicCV].Valor * vPercRetencao * 0.01, 2);

        vQtdeDigitosCartao := SFormatInt(Copy(vLinha, 112, 2));
        Result[vPosicRO].ComprovantesVenda[vPosicCV].NumeroCartao := Trim(Copy(vLinha, 19, 19));
        Result[vPosicRO].ComprovantesVenda[vPosicCV].NumeroCartao := Copy(Result[vPosicRO].ComprovantesVenda[vPosicCV].NumeroCartao, 1, vQtdeDigitosCartao);

        // Trazendo apenas os 4 ultimos digitos
        vQtdeDigitosCartao := Length(Result[vPosicRO].ComprovantesVenda[vPosicCV].NumeroCartao) - 3;
        Result[vPosicRO].ComprovantesVenda[vPosicCV].NumeroCartao := Copy(Result[vPosicRO].ComprovantesVenda[vPosicCV].NumeroCartao, vQtdeDigitosCartao, 4);

      end;  
    end;
  finally

  end;
end;


end.
