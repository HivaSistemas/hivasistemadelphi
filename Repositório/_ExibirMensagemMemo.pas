unit _ExibirMensagemMemo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPrincipal, Vcl.StdCtrls, _Biblioteca,
  Vcl.Buttons, Vcl.ExtCtrls;

type
  TFormExibirMensagemMemo = class(TFormHerancaPrincipal)
    meMensagem: TMemo;
    pnOpcoes: TPanel;
    sbOk: TSpeedButton;
    lbBotaoPressionar: TLabel;
    tiBotaoPressionar: TTimer;
    procedure sbOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure tiBotaoPressionarTimer(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
  private
    FTecla: Word;
    FAguardarTecla: Boolean;
  public
    { Public declarations }
  end;

procedure Mensagem(const pTituloTela: string; pMensagem: string); overload;
function Mensagem(const pTituloTela: string; pMensagem: string; pMensagemMemo: array of string; const pAguardarTecla: Boolean = False): Boolean; overload;

implementation

{$R *.dfm}

procedure Mensagem(const pTituloTela: string; pMensagem: string); overload;
var
  vForm: TFormExibirMensagemMemo;
begin
  vForm := TFormExibirMensagemMemo.Create(Application);

  vForm.Caption := pTituloTela;
  vForm.meMensagem.Lines.Clear;
  vForm.meMensagem.Lines.Add(pMensagem);

  vForm.ShowModal;

  vForm.Free;
end;

function Mensagem(const pTituloTela: string; pMensagem: string; pMensagemMemo: array of string; const pAguardarTecla: Boolean = False): Boolean;
var
  i: Integer;
  vForm: TFormExibirMensagemMemo;
begin
  vForm := TFormExibirMensagemMemo.Create(Application);

  vForm.Caption := pTituloTela;
  vForm.FAguardarTecla := pAguardarTecla;
  vForm.meMensagem.Lines.Clear;

  if pMensagem <> '' then begin
    vForm.meMensagem.Lines.Add(pMensagem);
    vForm.meMensagem.Lines.Add('');
  end;

  for i := Low(pMensagemMemo) to High(pMensagemMemo) do
    vForm.meMensagem.Lines.Add(pMensagemMemo[i]);

  Result := (vForm.ShowModal = mrOk);

  vForm.Free;
end;

procedure TFormExibirMensagemMemo.FormCreate(Sender: TObject);
begin
  inherited;
  lbBotaoPressionar.Font.Color := _Biblioteca.getCorVerdePadraoAltis;
end;

procedure TFormExibirMensagemMemo.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if not FAguardarTecla then
    Exit;

  if Key = FTecla then
    ModalResult := mrOk;
end;

procedure TFormExibirMensagemMemo.FormShow(Sender: TObject);
var
  vTecla: Integer;
begin
  inherited;
  sbOk.Visible := not FAguardarTecla;
  if sbOk.Visible then
    Exit;

  Randomize;
  vTecla := Random(12);

  while vTecla = 0 do
    vTecla := Random(12);

  case vTecla of
    1: begin
      FTecla := VK_F1;
      lbBotaoPressionar.Caption := 'Para continuar pressione <F1>';
    end;

    2: begin
      FTecla := VK_F2;
      lbBotaoPressionar.Caption := 'Para continuar pressione <F2>';
    end;

    3: begin
      FTecla := VK_F3;
      lbBotaoPressionar.Caption := 'Para continuar pressione <F3>';
    end;

    4: begin
      FTecla := VK_F4;
      lbBotaoPressionar.Caption := 'Para continuar pressione <F4>';
    end;

    5: begin
      FTecla := VK_F5;
      lbBotaoPressionar.Caption := 'Para continuar pressione <F5>';
    end;

    6: begin
      FTecla := VK_F6;
      lbBotaoPressionar.Caption := 'Para continuar pressione <F6>';
    end;

    7: begin
      FTecla := VK_F7;
      lbBotaoPressionar.Caption := 'Para continuar pressione <F7>';
    end;

    8: begin
      FTecla := VK_F8;
      lbBotaoPressionar.Caption := 'Para continuar pressione <F8>';
    end;

    9: begin
      FTecla := VK_F9;
      lbBotaoPressionar.Caption := 'Para continuar pressione <F9>';
    end;

    10: begin
      FTecla := VK_F10;
      lbBotaoPressionar.Caption := 'Para continuar pressione <F10>';
    end;

    11: begin
      FTecla := VK_F11;
      lbBotaoPressionar.Caption := 'Para continuar pressione <F11>';
    end;

    12: begin
      FTecla := VK_F12;
      lbBotaoPressionar.Caption := 'Para continuar pressione <F12>';
    end;
  end;

  tiBotaoPressionar.Enabled := True;
end;

procedure TFormExibirMensagemMemo.sbOkClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrOk;
end;

procedure TFormExibirMensagemMemo.tiBotaoPressionarTimer(Sender: TObject);
begin
  inherited;
  lbBotaoPressionar.Visible := not lbBotaoPressionar.Visible;
end;

end.
