unit _HerancaFinalizar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaBarra, Vcl.Buttons, Vcl.ExtCtrls, System.Math,
  _Biblioteca;

type
  TFormHerancaFinalizar = class(TFormHerancaBarra)
    sbFinalizar: TSpeedButton;
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbFinalizarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    procedure CentralizarBotaoFinalizar;
  protected
    procedure Finalizar(Sender: TObject); virtual;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure ReadOnlyTodosObjetos(pValor: Boolean); virtual;
  public
    procedure abrirModal;
    procedure abrirModalnformacoes;
  end;

implementation

{$R *.dfm}

{ TFormHerancaFinalizar }

procedure TFormHerancaFinalizar.abrirModal;
begin
  try
    Self.ShowModal;
  finally
    Self.Free;
  end;
end;

procedure TFormHerancaFinalizar.abrirModalnformacoes;
begin
  ReadOnlyTodosObjetos(True);
  abrirModal;
end;

procedure TFormHerancaFinalizar.CentralizarBotaoFinalizar;
begin
  sbFinalizar.Left := Ceil((Self.Width - sbFinalizar.Width) / 2);
end;

procedure TFormHerancaFinalizar.Finalizar(Sender: TObject);
begin
  inherited;

end;

procedure TFormHerancaFinalizar.FormCreate(Sender: TObject);
begin
  inherited;
  CentralizarBotaoFinalizar;
end;

procedure TFormHerancaFinalizar.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_F9 then
    sbFinalizarClick(Sender);
end;

procedure TFormHerancaFinalizar.FormResize(Sender: TObject);
begin
  inherited;
  CentralizarBotaoFinalizar;
end;

procedure TFormHerancaFinalizar.ReadOnlyTodosObjetos(pValor: Boolean);
var
  i: Integer;
  vObjetos: array of TComponent;
begin
  SetLength(vObjetos, Self.ComponentCount);
  for i := 0 to Self.ComponentCount -1 do
    vObjetos[i] := Self.Components[i];

  _Biblioteca.SomenteLeitura(vObjetos, pValor);
end;

procedure TFormHerancaFinalizar.sbFinalizarClick(Sender: TObject);
begin
  inherited;
  VerificarRegistro(Sender);
  Finalizar(Sender);

  if fsModal in FormState then
    ModalResult := mrOk
  else
    Self.Close;
end;

procedure TFormHerancaFinalizar.VerificarRegistro(Sender: TObject);
begin
  inherited;
  // Preenchendo espa�o
end;

end.
