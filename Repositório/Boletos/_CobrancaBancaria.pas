unit _CobrancaBancaria;

interface

type
 RecDadosBoletoBancario = record
    ReceberId: Integer;
    Carteira: string;
    Agencia: string;
    DigitoAgencia: string;
    ContaCorrente: string;
    DigitoContaCorrente: string;
    NossoNumero: string;
    DigitoVerificadorNossoNumero: string;
    DigitoVerificadorAgenciaBeneficiario: string;
    Convenio: string;
    DataVencimento: TDateTime;
    Beneficiario: string;
    CnpjBeneficiario: string;
    DataEmissao: TDateTime;
    ValorDocumento: Double;
    Documento: string;
    Logradouro: string;
    Cep: string;
    CpfCnpj: string;
    NomeRazaoSocial: string;
    Endereco: string;
    Instrucao: string;
    Cidade: string;
    Estado: string;
    Pagador: string;
    CodigoBarras: string;
    LinhaDigitavel: string;
    TextoImpressao: string;
    NumeroNota: string;
    PercentualMulta: Double;
    NomeResumidoEmpresa: string;
    PosicaoConvenio: Integer;
    PostoCedente: string;
    Cliente: string;
    AgenciaCodigoBeneficiario: string;
    AgenciaBeneficiario: string;
    EnderecoBeneficiario: string;
    ClienteId: Integer;
    Observacao: string;
    EmpresaId: Integer;
  end;

  RecBoletoGerarRemessa = record
    ReceberId: Integer;
    Documento: string;
    Parcela: Integer;
    Carteira: string;
    Agencia: string;
    Conta: string;
    DigitoConta: string;
    NossoNumero: string;
    DataVencimento: TDateTime;
    ValorDocumento: Double;
    DataEmissao: TDateTime;
    ValorDiaAtraso: Double;
    ValorMulta: Double;
    PercentualMulta: Double;
    PercentualJurosMensal: Double;
    CpfCnpj: string;
    RazaoSocial: string;
    Logradouro: string;
    Complemento: string;
    NomeBairro: string;
    NomeCidade: string;
    EstadoId: string;
    Cep: string;
  end;

  RecBoletoRetornoRemessa = record
    ValorDocumento: Double;
    DataVencimento: TDateTime;
    DataPagamento: TDateTime;
    NossoNumero: string;
    ReceberId: Integer;
    RegistroEmDuplicidade: string;
    MotivoRejeicao: string;

    Retorno: record
      TipoId: Integer;
      DescricaoTipo: string;
      Baixar: Boolean;
      DataOcorrencia: TDateTime;
      BancoCobrador: Integer;
      ValorPago: Double;
      Juros: double;
      Desconto: double;
      DataCredito: TDateTime;
      ValorTarifa: double;
      NumeroDocumento: string;
      Multa: double;
      AgenciaArquivo: string;
      ContaArquivo: string;
      DigitoContaArquivo: string;
    end;
  end;

  ICobrancaBancaria = interface
    function GerarArquivoRemessa(
      pNomeArquivo: string;
      pCodigoEmpresa: string;
      pCNPJEmpresa: string;
      pNomeEmpresa: string;
      pNumeroRemessa: Integer;
      pDataArquivo: TDateTime;
      pHoraArquivo: string;
      pCarteira: string;
      pCEPEmpresa: string;
      pRegistros: TArray<RecBoletoGerarRemessa>;
      pPercentualMulta: Double
    ): Boolean;

    function DigitoVerifNossoNumero(
      pCarteira: string;
      pNossoNumero: string;
      pChave: string;
      pTipoDigito: string
    ): string;

    function getRetorno(
      pCodigoEmpresa: string;
      pArquivo: string
    ): TArray<RecBoletoRetornoRemessa>;
  end;

implementation

end.
