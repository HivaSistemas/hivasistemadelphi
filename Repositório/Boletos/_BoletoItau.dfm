inherited FormBoletoItau: TFormBoletoItau
  Caption = 'FormBoletoItau'
  ClientHeight = 778
  ClientWidth = 1018
  Scaled = False
  ExplicitWidth = 1024
  ExplicitHeight = 806
  PixelsPerInch = 96
  TextHeight = 14
  object QrBoletaItau: TQuickRep
    Left = 89
    Top = -304
    Width = 794
    Height = 1123
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE')
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Options = [FirstPageHeader, LastPageFooter]
    Page.Columns = 2
    Page.Orientation = poPortrait
    Page.PaperSize = A4
    Page.Continuous = False
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    PrinterSettings.Copies = 1
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.UseStandardprinter = False
    PrinterSettings.UseCustomBinCode = False
    PrinterSettings.CustomBinCode = 0
    PrinterSettings.ExtendedDuplex = 0
    PrinterSettings.UseCustomPaperCode = False
    PrinterSettings.CustomPaperCode = 0
    PrinterSettings.PrintMetaFile = False
    PrinterSettings.PrintQuality = 0
    PrinterSettings.Collate = 0
    PrinterSettings.ColorOption = 0
    PrintIfEmpty = True
    SnapToGrid = True
    Units = MM
    Zoom = 100
    PrevFormStyle = fsNormal
    PreviewInitialState = wsMaximized
    PrevInitialZoom = qrZoomToFit
    PreviewDefaultSaveType = stQRP
    PreviewLeft = 0
    PreviewTop = 0
    object QrBrBoletaItau: TQRBand
      Left = 38
      Top = 38
      Width = 718
      Height = 1051
      Frame.Style = psClear
      AlignToBottom = False
      Color = clWindow
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        2780.770833333333000000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbTitle
      object QRShape99: TQRShape
        Left = 8
        Top = 289
        Width = 561
        Height = 28
        Size.Values = (
          74.083333333333330000
          21.166666666666670000
          764.645833333333300000
          1484.312500000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape102: TQRShape
        Left = 436
        Top = 289
        Width = 133
        Height = 28
        Size.Values = (
          74.083333333333330000
          1153.583333333333000000
          764.645833333333300000
          351.895833333333300000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape103: TQRShape
        Left = 568
        Top = 289
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333320000
          1502.833333333333000000
          764.645833333333200000
          362.479166666666700000)
        XLColumn = 0
        Brush.Color = 14671839
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape100: TQRShape
        Left = 8
        Top = 316
        Width = 561
        Height = 28
        Size.Values = (
          74.083333333333320000
          21.166666666666670000
          836.083333333333200000
          1484.312500000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape109: TQRShape
        Left = 436
        Top = 316
        Width = 133
        Height = 28
        Size.Values = (
          74.083333333333330000
          1153.583333333333000000
          836.083333333333300000
          351.895833333333300000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape104: TQRShape
        Left = 568
        Top = 316
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333320000
          1502.833333333333000000
          836.083333333333200000
          362.479166666666700000)
        XLColumn = 0
        Brush.Color = 14671839
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape101: TQRShape
        Left = 8
        Top = 343
        Width = 561
        Height = 55
        Size.Values = (
          145.520833333333300000
          21.166666666666670000
          907.520833333333200000
          1484.312500000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape105: TQRShape
        Left = 568
        Top = 343
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333320000
          1502.833333333333000000
          907.520833333333200000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape106: TQRShape
        Left = 568
        Top = 370
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333320000
          1502.833333333333000000
          978.958333333333200000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape24: TQRShape
        Left = 8
        Top = 564
        Width = 697
        Height = 11
        Frame.Style = psDot
        Size.Values = (
          29.104166666666670000
          21.166666666666670000
          1492.250000000000000000
          1844.145833333333000000)
        XLColumn = 0
        Pen.Style = psDot
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape27: TQRShape
        Left = 8
        Top = 437
        Width = 429
        Height = 40
        Size.Values = (
          105.833333333333300000
          21.166666666666670000
          1156.229166666667000000
          1135.062500000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape88: TQRShape
        Left = 436
        Top = 437
        Width = 133
        Height = 40
        Size.Values = (
          105.833333333333300000
          1153.583333333333000000
          1156.229166666667000000
          351.895833333333300000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape89: TQRShape
        Left = 568
        Top = 437
        Width = 137
        Height = 40
        Size.Values = (
          105.833333333333300000
          1502.833333333333000000
          1156.229166666667000000
          362.479166666666700000)
        XLColumn = 0
        Brush.Color = 14671839
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape28: TQRShape
        Left = 8
        Top = 476
        Width = 561
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          1259.416666666667000000
          1484.312500000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape90: TQRShape
        Left = 568
        Top = 476
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1259.416666666667000000
          362.479166666666700000)
        XLColumn = 0
        Brush.Color = 14671839
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape29: TQRShape
        Left = 8
        Top = 503
        Width = 561
        Height = 58
        Size.Values = (
          153.458333333333300000
          21.166666666666670000
          1330.854166666667000000
          1484.312500000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape91: TQRShape
        Left = 568
        Top = 503
        Width = 137
        Height = 30
        Size.Values = (
          79.375000000000000000
          1502.833333333333000000
          1330.854166666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape92: TQRShape
        Left = 568
        Top = 532
        Width = 137
        Height = 29
        Size.Values = (
          76.729166666666670000
          1502.833333333333000000
          1407.583333333333000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape2: TQRShape
        Left = 8
        Top = 600
        Width = 561
        Height = 36
        Size.Values = (
          95.250000000000000000
          21.166666666666670000
          1587.500000000000000000
          1484.312500000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape3: TQRShape
        Left = 568
        Top = 600
        Width = 137
        Height = 36
        Size.Values = (
          95.250000000000000000
          1502.833333333333000000
          1587.500000000000000000
          362.479166666666700000)
        XLColumn = 0
        Brush.Color = 14671839
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape4: TQRShape
        Left = 8
        Top = 635
        Width = 561
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          1680.104166666667000000
          1484.312500000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape1: TQRShape
        Left = 568
        Top = 635
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1680.104166666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape5: TQRShape
        Left = 8
        Top = 662
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          1751.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape6: TQRShape
        Left = 120
        Top = 662
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          317.500000000000000000
          1751.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape8: TQRShape
        Left = 232
        Top = 662
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          613.833333333333400000
          1751.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape7: TQRShape
        Left = 344
        Top = 662
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          910.166666666666600000
          1751.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape9: TQRShape
        Left = 456
        Top = 662
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          1206.500000000000000000
          1751.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape10: TQRShape
        Left = 568
        Top = 662
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1751.541666666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape11: TQRShape
        Left = 8
        Top = 689
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          1822.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape12: TQRShape
        Left = 120
        Top = 689
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          317.500000000000000000
          1822.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape13: TQRShape
        Left = 232
        Top = 689
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          613.833333333333400000
          1822.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape14: TQRShape
        Left = 344
        Top = 689
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          910.166666666666600000
          1822.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape15: TQRShape
        Left = 456
        Top = 689
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          1206.500000000000000000
          1822.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape16: TQRShape
        Left = 568
        Top = 689
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1822.979166666667000000
          362.479166666666700000)
        XLColumn = 0
        Brush.Color = 14671839
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape17: TQRShape
        Left = 8
        Top = 716
        Width = 561
        Height = 136
        Size.Values = (
          359.833333333333400000
          21.166666666666670000
          1894.416666666667000000
          1484.312500000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape18: TQRShape
        Left = 568
        Top = 716
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1894.416666666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape19: TQRShape
        Left = 568
        Top = 743
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1965.854166666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape20: TQRShape
        Left = 568
        Top = 770
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          2037.291666666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape21: TQRShape
        Left = 568
        Top = 797
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          2108.729166666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape22: TQRShape
        Left = 568
        Top = 824
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          2180.166666666667000000
          362.479166666666700000)
        XLColumn = 0
        Brush.Color = 14671839
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape23: TQRShape
        Left = 8
        Top = 851
        Width = 697
        Height = 74
        Size.Values = (
          195.791666666666700000
          21.166666666666670000
          2251.604166666667000000
          1844.145833333333000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qr1: TQRLabel
        Left = 12
        Top = 602
        Width = 177
        Height = 13
        Size.Values = (
          34.395833333333330000
          31.750000000000000000
          1592.791666666667000000
          468.312500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Local de Pagamento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr2: TQRLabel
        Left = 12
        Top = 613
        Width = 221
        Height = 12
        Size.Values = (
          31.750000000000000000
          31.750000000000000000
          1621.895833333333000000
          584.729166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'At'#233' o vencimento, preferencialmente no Ita'#250' '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr3: TQRLabel
        Left = 571
        Top = 603
        Width = 130
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          1595.437500000000000000
          343.958333333333400000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vencimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrVencimento: TQRLabel
        Left = 571
        Top = 619
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333340000
          1510.770833333333000000
          1637.770833333333000000
          343.958333333333400000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrVencimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr4: TQRLabel
        Left = 12
        Top = 637
        Width = 193
        Height = 13
        Size.Values = (
          34.395833333333340000
          31.750000000000000000
          1685.395833333333000000
          510.645833333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Benefici'#225'rio'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr5: TQRLabel
        Left = 571
        Top = 637
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333340000
          1510.770833333333000000
          1685.395833333333000000
          343.958333333333400000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ag'#234'ncia/C'#243'd.Benefici'#225'rio'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QRShape25: TQRShape
        Left = 8
        Top = 1036
        Width = 697
        Height = 30
        Frame.Style = psDot
        Size.Values = (
          79.375000000000000000
          21.166666666666670000
          2741.083333333333000000
          1844.145833333333000000)
        XLColumn = 0
        Pen.Style = psDot
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object qr6: TQRLabel
        Left = 480
        Top = 926
        Width = 105
        Height = 14
        Size.Values = (
          37.041666666666670000
          1270.000000000000000000
          2450.041666666667000000
          277.812500000000000000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Autentica'#231#227'o Mec'#226'nica'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr7: TQRLabel
        Left = 600
        Top = 926
        Width = 105
        Height = 14
        Size.Values = (
          37.041666666666670000
          1587.500000000000000000
          2450.041666666667000000
          277.812500000000000000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ficha de Compensa'#231#227'o'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr8: TQRLabel
        Left = 12
        Top = 664
        Width = 101
        Height = 13
        Size.Values = (
          34.395833333333340000
          31.750000000000000000
          1756.833333333333000000
          267.229166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Data Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr9: TQRLabel
        Left = 125
        Top = 664
        Width = 106
        Height = 13
        Size.Values = (
          34.395833333333330000
          330.729166666666700000
          1756.833333333333000000
          280.458333333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nr. Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr10: TQRLabel
        Left = 236
        Top = 664
        Width = 107
        Height = 13
        Size.Values = (
          34.395833333333330000
          624.416666666666700000
          1756.833333333333000000
          283.104166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Esp'#233'cie'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr11: TQRLabel
        Left = 460
        Top = 664
        Width = 107
        Height = 13
        Size.Values = (
          34.395833333333340000
          1217.083333333333000000
          1756.833333333333000000
          283.104166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Data Processamento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr12: TQRLabel
        Left = 348
        Top = 664
        Width = 107
        Height = 13
        Size.Values = (
          34.395833333333330000
          920.750000000000000000
          1756.833333333333000000
          283.104166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Aceite'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr13: TQRLabel
        Left = 12
        Top = 691
        Width = 101
        Height = 13
        Size.Values = (
          34.395833333333330000
          31.750000000000000000
          1828.270833333333000000
          267.229166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Uso do Banco'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr14: TQRLabel
        Left = 125
        Top = 691
        Width = 106
        Height = 13
        Size.Values = (
          34.395833333333330000
          330.729166666666700000
          1828.270833333333000000
          280.458333333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Carteira'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr15: TQRLabel
        Left = 236
        Top = 691
        Width = 107
        Height = 13
        Size.Values = (
          34.395833333333330000
          624.416666666666700000
          1828.270833333333000000
          283.104166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Moeda'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr16: TQRLabel
        Left = 348
        Top = 691
        Width = 107
        Height = 13
        Size.Values = (
          34.395833333333330000
          920.750000000000000000
          1828.270833333333000000
          283.104166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Quantidade'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr17: TQRLabel
        Left = 460
        Top = 691
        Width = 107
        Height = 13
        Size.Values = (
          34.395833333333330000
          1217.083333333333000000
          1828.270833333333000000
          283.104166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrInstrucoes: TQRLabel
        Left = 12
        Top = 718
        Width = 551
        Height = 15
        Size.Values = (
          39.687500000000000000
          31.750000000000000000
          1899.708333333333000000
          1457.854166666667000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 
          'Instru'#231#245'es(Todas as informa'#231#245'es deste bloqueto s'#227'o de exclusiva ' +
          'responsabilidade do benefici'#225'rio)'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr18: TQRLabel
        Left = 12
        Top = 853
        Width = 101
        Height = 13
        Size.Values = (
          34.395833333333340000
          31.750000000000000000
          2256.895833333333000000
          267.229166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Pagador'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr19: TQRLabel
        Left = 12
        Top = 908
        Width = 93
        Height = 14
        Size.Values = (
          37.041666666666670000
          31.750000000000000000
          2402.416666666667000000
          246.062500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Sacador/Avalista:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr20: TQRLabel
        Left = 571
        Top = 664
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333340000
          1510.770833333333000000
          1756.833333333333000000
          343.958333333333400000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nosso N'#250'mero'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr21: TQRLabel
        Left = 571
        Top = 691
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333330000
          1510.770833333333000000
          1828.270833333333000000
          343.958333333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor do Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr22: TQRLabel
        Left = 571
        Top = 718
        Width = 130
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          1899.708333333333000000
          343.958333333333400000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(-) Desconto/Abatimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr23: TQRLabel
        Left = 571
        Top = 745
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333340000
          1510.770833333333000000
          1971.145833333334000000
          343.958333333333400000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(-) Outras Dedu'#231#245'es'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr24: TQRLabel
        Left = 571
        Top = 772
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333340000
          1510.770833333333000000
          2042.583333333333000000
          343.958333333333400000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(+) Mora/Multa'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr25: TQRLabel
        Left = 571
        Top = 799
        Width = 130
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          2114.020833333333000000
          343.958333333333400000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(+) Outros'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr26: TQRLabel
        Left = 571
        Top = 826
        Width = 130
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          2185.458333333333000000
          343.958333333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(=) Valor Cobrado'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QRShape26: TQRShape
        Left = 8
        Top = 404
        Width = 697
        Height = 11
        Frame.Style = psDot
        Size.Values = (
          29.104166666666670000
          21.166666666666670000
          1068.916666666667000000
          1844.145833333333000000)
        XLColumn = 0
        Pen.Style = psDot
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object qr27: TQRLabel
        Left = 12
        Top = 505
        Width = 193
        Height = 14
        Size.Values = (
          37.041666666666670000
          31.750000000000000000
          1336.145833333333000000
          510.645833333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Demonstrativo'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr28: TQRLabel
        Left = 12
        Top = 478
        Width = 471
        Height = 13
        Size.Values = (
          34.395833333333340000
          31.750000000000000000
          1264.708333333333000000
          1246.187500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Pagador'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr29: TQRLabel
        Left = 12
        Top = 439
        Width = 193
        Height = 12
        Size.Values = (
          31.750000000000000000
          31.750000000000000000
          1161.520833333333000000
          510.645833333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Benefici'#225'rio'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr30: TQRLabel
        Left = 438
        Top = 439
        Width = 129
        Height = 13
        Size.Values = (
          34.395833333333330000
          1158.875000000000000000
          1161.520833333333000000
          341.312500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ag'#234'ncia/C'#243'd.Benefici'#225'rio'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr31: TQRLabel
        Left = 571
        Top = 439
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333330000
          1510.770833333333000000
          1161.520833333333000000
          343.958333333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vencimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr32: TQRLabel
        Left = 571
        Top = 478
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333330000
          1510.770833333333000000
          1264.708333333333000000
          343.958333333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor do Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr33: TQRLabel
        Left = 571
        Top = 505
        Width = 130
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          1336.145833333333000000
          343.958333333333400000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nosso N'#250'mero'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr34: TQRLabel
        Left = 571
        Top = 534
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333330000
          1510.770833333333000000
          1412.875000000000000000
          343.958333333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nr. Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr35: TQRLabel
        Left = 584
        Top = 421
        Width = 120
        Height = 15
        Size.Values = (
          39.687500000000000000
          1545.166666666667000000
          1113.895833333333000000
          317.500000000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Recibo do Pagador'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 9
      end
      object QRShape94: TQRShape
        Left = 215
        Top = 576
        Width = 17
        Height = 23
        Frame.Style = psDot
        Size.Values = (
          60.854166666666680000
          568.854166666666700000
          1524.000000000000000000
          44.979166666666670000)
        XLColumn = 0
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qr36: TQRLabel
        Left = 226
        Top = 578
        Width = 50
        Height = 20
        Size.Values = (
          52.916666666666660000
          597.958333333333400000
          1529.291666666667000000
          132.291666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '341-7'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 12
      end
      object QRShape97: TQRShape
        Left = 272
        Top = 576
        Width = 9
        Height = 23
        Frame.Style = psDot
        Size.Values = (
          60.854166666666680000
          719.666666666666800000
          1524.000000000000000000
          23.812500000000000000)
        XLColumn = 0
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qrBeneficiario: TQRLabel
        Left = 12
        Top = 647
        Width = 357
        Height = 14
        Size.Values = (
          37.041666666666670000
          31.750000000000000000
          1711.854166666667000000
          944.562500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrBeneficiario'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrDataDocumento: TQRLabel
        Left = 12
        Top = 675
        Width = 101
        Height = 13
        Size.Values = (
          34.395833333333340000
          31.750000000000000000
          1785.937500000000000000
          267.229166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' qrDocumento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrNrDocumento: TQRLabel
        Left = 125
        Top = 675
        Width = 106
        Height = 13
        Size.Values = (
          34.395833333333330000
          330.729166666666700000
          1785.937500000000000000
          280.458333333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNrDocumento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrEspecie: TQRLabel
        Left = 236
        Top = 675
        Width = 107
        Height = 13
        Size.Values = (
          34.395833333333330000
          624.416666666666700000
          1785.937500000000000000
          283.104166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'DM'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrBeneficiario1: TQRLabel
        Left = 12
        Top = 450
        Width = 277
        Height = 13
        Size.Values = (
          34.395833333333330000
          31.750000000000000000
          1190.625000000000000000
          732.895833333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrBeneficiario'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr37: TQRLabel
        Left = 236
        Top = 702
        Width = 107
        Height = 13
        Size.Values = (
          34.395833333333330000
          624.416666666666700000
          1857.375000000000000000
          283.104166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'R$'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrCarteira: TQRLabel
        Left = 125
        Top = 702
        Width = 106
        Height = 13
        Size.Values = (
          34.395833333333330000
          330.729166666666700000
          1857.375000000000000000
          280.458333333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'RAP'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrAceite: TQRLabel
        Left = 348
        Top = 675
        Width = 107
        Height = 13
        Size.Values = (
          34.395833333333330000
          920.750000000000000000
          1785.937500000000000000
          283.104166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrAceite'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrDataProcessamento: TQRLabel
        Left = 460
        Top = 675
        Width = 107
        Height = 13
        Size.Values = (
          34.395833333333340000
          1217.083333333333000000
          1785.937500000000000000
          283.104166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrDataProcessamento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrValor: TQRLabel
        Left = 460
        Top = 702
        Width = 107
        Height = 13
        Size.Values = (
          34.395833333333330000
          1217.083333333333000000
          1857.375000000000000000
          283.104166666666700000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Valor'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrPagador1: TQRLabel
        Left = 12
        Top = 490
        Width = 555
        Height = 12
        Size.Values = (
          31.750000000000000000
          31.750000000000000000
          1296.458333333333000000
          1468.437500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrPagador1'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrPagador: TQRLabel
        Left = 108
        Top = 865
        Width = 293
        Height = 14
        Size.Values = (
          37.041666666666670000
          285.750000000000000000
          2288.645833333333000000
          775.229166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrPagador'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrValorDocumento: TQRLabel
        Left = 571
        Top = 702
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333330000
          1510.770833333333000000
          1857.375000000000000000
          343.958333333333300000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Valor'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrLogradouro: TQRLabel
        Left = 108
        Top = 879
        Width = 565
        Height = 14
        Size.Values = (
          37.041666666666670000
          285.750000000000000000
          2325.687500000000000000
          1494.895833333333000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrLogradouro'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrCep: TQRLabel
        Left = 108
        Top = 895
        Width = 71
        Height = 14
        Size.Values = (
          37.041666666666670000
          285.750000000000000000
          2368.020833333333000000
          187.854166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrCep'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrCidade: TQRLabel
        Left = 234
        Top = 895
        Width = 439
        Height = 14
        Size.Values = (
          37.041666666666670000
          619.125000000000000000
          2368.020833333333000000
          1161.520833333333000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrCidade'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrcpfcnpj: TQRLabel
        Left = 458
        Top = 865
        Width = 103
        Height = 14
        Size.Values = (
          37.041666666666670000
          1211.791666666667000000
          2288.645833333333000000
          272.520833333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrCpfCnpj'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrAgenciaCodigoBeneficiario: TQRLabel
        Left = 571
        Top = 648
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333330000
          1510.770833333333000000
          1714.500000000000000000
          343.958333333333300000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '600253 / 05619560'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrValorDocumento1: TQRLabel
        Left = 571
        Top = 489
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333340000
          1510.770833333333000000
          1293.812500000000000000
          343.958333333333400000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Valor'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrVencimento1: TQRLabel
        Left = 571
        Top = 456
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333330000
          1510.770833333333000000
          1206.500000000000000000
          343.958333333333300000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrVencimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrNrDocumento1: TQRLabel
        Left = 571
        Top = 545
        Width = 130
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          1441.979166666667000000
          343.958333333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNrDocumento1'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrAgenciaCodigoBeneficiario1: TQRLabel
        Left = 438
        Top = 456
        Width = 129
        Height = 13
        Size.Values = (
          34.395833333333330000
          1158.875000000000000000
          1206.500000000000000000
          341.312500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '600253 / 05619560'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QRShape93: TQRShape
        Left = 8
        Top = 23
        Width = 697
        Height = 218
        Size.Values = (
          576.791666666666800000
          21.166666666666670000
          60.854166666666680000
          1844.145833333333000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrTexto: TQRLabel
        Left = 12
        Top = 28
        Width = 685
        Height = 205
        Size.Values = (
          542.395833333333300000
          31.750000000000000000
          74.083333333333320000
          1812.395833333333000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrTexto'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object qrLinhaDigitavel: TQRLabel
        Left = 288
        Top = 582
        Width = 417
        Height = 17
        Size.Values = (
          44.979166666666670000
          762.000000000000000000
          1539.875000000000000000
          1103.312500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrLinhaDigitavel'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object qrCodigoBarra: TQRBarcode
        Left = 12
        Top = 939
        Width = 389
        Height = 50
        Size.Values = (
          132.291666666666700000
          31.750000000000000000
          2484.437500000000000000
          1030.000000000000000000)
        XLColumn = 0
        AutoSize = True
        Picture.Data = {
          07544269746D61707AAC0000424D7AAC00000000000036000000280000006900
          000069000000010020000000000044AC00000000000000000000000000000000
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF0000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
          FFFF00000000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF00FFFFFFFF00000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF0000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
          FFFF00000000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF00FFFFFFFF00000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF0000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
          FFFF00000000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF00FFFFFFFF00000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF0000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
          FFFF00000000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF00FFFFFFFF00000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF0000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
          FFFF00000000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF00FFFFFFFF00000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF0000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
          FFFF00000000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF00FFFFFFFF00000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF0000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
          FFFF00000000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF}
        BarHeight = 50
        BarTop = 1
        BarLeft = 1
        BarModul = 1
        BarRatio = 2.000000000000000000
        BarTyp = bcCode_2_5_interleaved
        BarText = '00000000000000000000000000000000000000000000'
      end
      object qrNossoNumero: TQRLabel
        Left = 571
        Top = 675
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333330000
          1510.770833333333000000
          1785.937500000000000000
          343.958333333333300000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '600253 / 05619560'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrNossoNumero1: TQRLabel
        Left = 571
        Top = 516
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333340000
          1510.770833333333000000
          1365.250000000000000000
          343.958333333333400000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Valor'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrInstrucao: TQRLabel
        Left = 12
        Top = 735
        Width = 551
        Height = 66
        Size.Values = (
          174.625000000000000000
          31.750000000000000000
          1944.687500000000000000
          1457.854166666667000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrInstrucao'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object qr38: TQRLabel
        Left = 12
        Top = 622
        Width = 221
        Height = 12
        Size.Values = (
          31.750000000000000000
          31.750000000000000000
          1645.708333333333000000
          584.729166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ap'#243's o vencimento, somente no Ita'#250' '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr39: TQRLabel
        Left = 10
        Top = 576
        Width = 138
        Height = 23
        Size.Values = (
          60.854166666666670000
          26.458333333333330000
          1524.000000000000000000
          365.125000000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = 'BANCO ITAU S.A'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 12
      end
      object qr40: TQRLabel
        Left = 10
        Top = 413
        Width = 138
        Height = 23
        Size.Values = (
          60.854166666666670000
          26.458333333333330000
          1092.729166666667000000
          365.125000000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = 'BANCO ITAU S.A'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 12
      end
      object QRShape95: TQRShape
        Left = 215
        Top = 412
        Width = 17
        Height = 23
        Frame.Style = psDot
        Size.Values = (
          60.854166666666670000
          568.854166666666700000
          1090.083333333333000000
          44.979166666666670000)
        XLColumn = 0
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qr41: TQRLabel
        Left = 226
        Top = 414
        Width = 50
        Height = 20
        Size.Values = (
          52.916666666666670000
          597.958333333333300000
          1095.375000000000000000
          132.291666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '341-7'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 12
      end
      object QRShape96: TQRShape
        Left = 272
        Top = 412
        Width = 9
        Height = 23
        Frame.Style = psDot
        Size.Values = (
          60.854166666666670000
          719.666666666666700000
          1090.083333333333000000
          23.812500000000000000)
        XLColumn = 0
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qrNumero_Nota: TQRLabel
        Left = 12
        Top = 833
        Width = 551
        Height = 14
        Size.Values = (
          37.041666666666670000
          31.750000000000000000
          2203.979166666667000000
          1457.854166666667000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNumero_Nota'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRShape98: TQRShape
        Left = 8
        Top = 248
        Width = 697
        Height = 14
        Frame.Style = psDot
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          656.166666666666800000
          1844.145833333333000000)
        XLColumn = 0
        Pen.Style = psDot
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object qr42: TQRLabel
        Left = 319
        Top = 345
        Width = 193
        Height = 14
        Size.Values = (
          37.041666666666670000
          844.020833333333300000
          912.812500000000000000
          510.645833333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Assinatura do cliente por extenso'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr43: TQRLabel
        Left = 12
        Top = 318
        Width = 409
        Height = 13
        Size.Values = (
          34.395833333333340000
          31.750000000000000000
          841.375000000000000000
          1082.145833333333000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Pagador'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr44: TQRLabel
        Left = 12
        Top = 291
        Width = 193
        Height = 12
        Size.Values = (
          31.750000000000000000
          31.750000000000000000
          769.937500000000000000
          510.645833333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Benefici'#225'rio'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr45: TQRLabel
        Left = 438
        Top = 291
        Width = 129
        Height = 13
        Size.Values = (
          34.395833333333330000
          1158.875000000000000000
          769.937500000000000000
          341.312500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ag'#234'ncia/C'#243'd.Benefici'#225'rio'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr46: TQRLabel
        Left = 571
        Top = 291
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333330000
          1510.770833333333000000
          769.937500000000000000
          343.958333333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vencimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr47: TQRLabel
        Left = 571
        Top = 318
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333330000
          1510.770833333333000000
          841.375000000000000000
          343.958333333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor do Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr48: TQRLabel
        Left = 571
        Top = 345
        Width = 130
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          912.812500000000000000
          343.958333333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nosso N'#250'mero'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr49: TQRLabel
        Left = 571
        Top = 372
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333330000
          1510.770833333333000000
          984.250000000000000000
          343.958333333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nr. Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr50: TQRLabel
        Left = 566
        Top = 273
        Width = 138
        Height = 15
        Size.Values = (
          39.687500000000000000
          1497.541666666667000000
          722.312500000000000000
          365.125000000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Recibo do Benefici'#225'rio'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 9
      end
      object qrBeneficiario2: TQRLabel
        Left = 12
        Top = 302
        Width = 389
        Height = 13
        Size.Values = (
          34.395833333333340000
          31.750000000000000000
          799.041666666666800000
          1029.229166666667000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrBeneficiario'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrPagador2: TQRLabel
        Left = 12
        Top = 329
        Width = 421
        Height = 13
        Size.Values = (
          34.395833333333330000
          31.750000000000000000
          870.479166666666700000
          1113.895833333333000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrPagador2'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrValorDocumento2: TQRLabel
        Left = 571
        Top = 329
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333330000
          1510.770833333333000000
          870.479166666666800000
          343.958333333333300000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Valor'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrVencimento2: TQRLabel
        Left = 571
        Top = 302
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333330000
          1510.770833333333000000
          799.041666666666800000
          343.958333333333300000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrVencimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrNrDocumento2: TQRLabel
        Left = 571
        Top = 383
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333330000
          1510.770833333333000000
          1013.354166666667000000
          343.958333333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNrDocumento1'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrAgenciaCodigoBeneficiario2: TQRLabel
        Left = 438
        Top = 302
        Width = 129
        Height = 13
        Size.Values = (
          34.395833333333330000
          1158.875000000000000000
          799.041666666666700000
          341.312500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '600253 / 05619560'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrNossoNumero2: TQRLabel
        Left = 571
        Top = 356
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333330000
          1510.770833333333000000
          941.916666666666800000
          343.958333333333300000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Valor'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr51: TQRLabel
        Left = 10
        Top = 265
        Width = 138
        Height = 23
        Size.Values = (
          60.854166666666670000
          26.458333333333330000
          701.145833333333300000
          365.125000000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Caption = 'BANCO ITAU S.A'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 12
      end
      object QRShape107: TQRShape
        Left = 215
        Top = 264
        Width = 17
        Height = 23
        Frame.Style = psDot
        Size.Values = (
          60.854166666666680000
          568.854166666666700000
          698.500000000000000000
          44.979166666666670000)
        XLColumn = 0
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qr52: TQRLabel
        Left = 226
        Top = 266
        Width = 50
        Height = 20
        Size.Values = (
          52.916666666666670000
          597.958333333333200000
          703.791666666666800000
          132.291666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '341-7'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 12
      end
      object QRShape108: TQRShape
        Left = 272
        Top = 264
        Width = 9
        Height = 23
        Frame.Style = psDot
        Size.Values = (
          60.854166666666680000
          719.666666666666800000
          698.500000000000000000
          23.812500000000000000)
        XLColumn = 0
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qrNumero_Nota1: TQRLabel
        Left = 12
        Top = 545
        Width = 551
        Height = 14
        Size.Values = (
          37.041666666666670000
          31.750000000000000000
          1441.979166666667000000
          1457.854166666667000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNumero_Nota1'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object qrPedido: TQRLabel
        Left = 12
        Top = 819
        Width = 551
        Height = 14
        Size.Values = (
          37.041666666666670000
          31.750000000000000000
          2166.937500000000000000
          1457.854166666667000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrPedido'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object qrPedido1: TQRLabel
        Left = 12
        Top = 532
        Width = 551
        Height = 14
        Size.Values = (
          37.041666666666670000
          31.750000000000000000
          1407.583333333333000000
          1457.854166666667000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrPedido1'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object qrDocumento: TQRLabel
        Left = 12
        Top = 804
        Width = 551
        Height = 14
        Size.Values = (
          37.041666666666670000
          31.750000000000000000
          2127.250000000000000000
          1457.854166666667000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrDocumento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object qrDocumento1: TQRLabel
        Left = 12
        Top = 518
        Width = 551
        Height = 14
        Size.Values = (
          37.041666666666670000
          31.750000000000000000
          1370.541666666667000000
          1457.854166666667000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrDocumento1'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object qrlDataEmissao: TQRLabel
        Left = 438
        Top = 318
        Width = 120
        Height = 13
        Size.Values = (
          34.395833333333330000
          1158.875000000000000000
          841.375000000000000000
          317.500000000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Data de Emiss'#227'o'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrDataEmissaoBeneficiario2: TQRLabel
        Left = 438
        Top = 330
        Width = 120
        Height = 12
        Size.Values = (
          31.750000000000000000
          1158.875000000000000000
          873.125000000000000000
          317.500000000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '99/99/9999'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrNomeFantasiaEmpresa: TQRLabel
        Left = 64
        Top = 383
        Width = 105
        Height = 13
        Size.Values = (
          34.395833333333330000
          169.333333333333300000
          1013.354166666667000000
          277.812500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'qrNomeFantasiaEmpresa'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr53: TQRLabel
        Left = 12
        Top = 383
        Width = 49
        Height = 13
        Size.Values = (
          34.395833333333330000
          31.750000000000000000
          1013.354166666667000000
          129.645833333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Empresa:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrDocumento2: TQRLabel
        Left = 12
        Top = 345
        Width = 305
        Height = 13
        Size.Values = (
          34.395833333333330000
          31.750000000000000000
          912.812500000000000000
          806.979166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrDocumento2'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrPedido2: TQRLabel
        Left = 12
        Top = 357
        Width = 305
        Height = 13
        Size.Values = (
          34.395833333333330000
          31.750000000000000000
          944.562500000000000000
          806.979166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrPedido2'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrNumero_Nota2: TQRLabel
        Left = 12
        Top = 370
        Width = 305
        Height = 13
        Size.Values = (
          34.395833333333330000
          31.750000000000000000
          978.958333333333300000
          806.979166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNumero_Nota2'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrCpfCnpjBeneficiario: TQRLabel
        Left = 376
        Top = 647
        Width = 191
        Height = 13
        Size.Values = (
          34.395833333333330000
          994.833333333333300000
          1711.854166666667000000
          505.354166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrCpfCnpjBeneficiario'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrEnderecoBeneficiario: TQRLabel
        Left = 12
        Top = 463
        Width = 423
        Height = 13
        Size.Values = (
          34.395833333333330000
          31.750000000000000000
          1225.020833333333000000
          1119.187500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrEnderecoBeneficiario'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qrCpfCnpjBeneficiario1: TQRLabel
        Left = 290
        Top = 450
        Width = 145
        Height = 12
        Size.Values = (
          31.750000000000000000
          767.291666666666700000
          1190.625000000000000000
          383.645833333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrCpfCnpjBeneficiario1'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr54: TQRLabel
        Left = 404
        Top = 865
        Width = 53
        Height = 13
        Size.Values = (
          34.395833333333330000
          1068.916666666667000000
          2288.645833333333000000
          140.229166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cnpj/Cpf:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
    end
    object QRBand1: TQRBand
      Left = 38
      Top = 1089
      Width = 718
      Height = 1051
      Frame.Style = psClear
      AlignToBottom = False
      Color = clWindow
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        2780.770833333333000000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbTitle
      object qr55: TQRLabel
        Left = 16
        Top = 177
        Width = 681
        Height = 17
        Size.Values = (
          44.979166666666670000
          42.333333333333340000
          468.312500000000100000
          1801.812500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 
          'Garanta a felicidade da sua empresa e de seu cliente utilizando ' +
          'o ADM - Sistema de Gest'#227'o Completo para sua Empresa.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRImage2: TQRImage
        Left = 8
        Top = 392
        Width = 145
        Height = 25
        Size.Values = (
          66.145833333333340000
          21.166666666666670000
          1037.166666666667000000
          383.645833333333400000)
        XLColumn = 0
        Picture.Data = {
          0A544A504547496D6167655E180000FFD8FFE000104A46494600010100000100
          010000FFDB004300080606070605080707070909080A0C140D0C0B0B0C191213
          0F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F2739
          3D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232
          3232323232323232323232323232323232323232323232323232323232323232
          32323232323232323232323232FFC2001108041702CE03012200021101031101
          FFC4001A000100030101010000000000000000000000020506040307FFC40014
          010100000000000000000000000000000000FFDA000C03010002100310000001
          DFBE6FA5346CCFA9A167E45F315606954B406E5931AC646E8B463B4677327AC0
          0000000000000000000000000000000000000000000000000000033B2D00ACF0
          BA14DD5DE29BCEF4524AE452CAE0574FB853CED4717AF4000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000FFFC4002010000203000104030000000000000000000304010205001113
          14601523D0FFDA0008010100010502F5A4EB4A24DE85D431DD6E0D2F1CD6F25C
          29ACCC3989DD72BC2E8106EC364F198E877C4DDC7995D264C270C626424C5DBA
          48EA179D3F8C9E448C25F49AE4D6004CAA92252A4DAD9F11CBA1D4B0A0EA8DF3
          E26233220A5CEEE5E50FBFE3A074167083C32743882AD17295193CDD2EE588B5
          6EC8072107E6A47FFFC400141101000000000000000000000000000000B0FFDA
          0008010301013F0139AFFFC400141101000000000000000000000000000000B0
          FFDA0008010201013F0139AFFFC4003110000201030204030606030000000000
          00010203000411122113314151226181142432607191052342A1B1C1D0D1F0FF
          DA0008010100063F02F96ACE7E018C897C5719C0C67FE1BD3926378D08D48AA7
          5007CF90ABB58521D36C0312F9E58CD4315AA46247884C78BC80F4A782110092
          155E297CE093DAA59C2E9D50BEDF7AFC2E3B768C078793677217AD2C6A61910C
          A1084C92B9DB73CB9F4ABD930B981982FA0A0D25B7B466CB5685EF9AB62B7313
          B367C5206258F60399AB230C51EBB8D790C79115722E151648E40A74F23BA9FE
          E9A6C28889FCBEF8EE69E5BF859B32E62B8CF8547407B54B37555DBEBD2A5B44
          78D8695901439DF186FDFE4A16ED712B5B83911EC3EF53271E458666D6C831F1
          55D36A6F795D2DE5B62A2686678A58E3E187D8E57CC53491DCCB1BB8024231E2
          C7F06BD917223D1A2AD744CE8D6E30AD8076C6299B8F2683371B46DF154D8B89
          5239BE345C73A8E58E778CAC7C2D80DC7AD40B04AF1707233B1C83CF9D5B6967
          F77D5A73E75346C5B12B063E98FF005533A16C4A7515E80D32CB732340C73C2C
          0EF9E744BCF2B299049A7A6DD3E951CFA995D015F0F507BD2C6646908FD4DCCF
          F8D49FFFC4002310010100020202020300030000000000000111002131415161
          71816091A1D0E1F0FFDA0008010100013F21FC6BA74A3DAD18D887006F9F3700
          687B0791781E4EF1D7510A56A03BD3E319C3DA56EA1C96E742E029D93D7B71AF
          0DCF4807FA63B6D434B61A751D7BFAC1804713A1E130E15A54B18C52EF2FCA2D
          026DA2B4F1AAEF03518DC2487B217D59BC8808109D9BE3B9F5EF28EDDFD513E8
          62891C9E434FC8F5D60DB02CB57738B3E5706F02372F87F5335A5C0EC367C0D7
          BFC2B867CC0EEC4152E6A3E0DDD56A59A358BEA070E82D3F7911C44207484C7E
          065B512F1F61FAC6D40A8E766DF9DDCD493B5082A2730CD1441306D5ACA9EB05
          E0BC30C4B66A9CF9C997BAA62DE8E112C00150351CA9FEB37202DE37DEEBDE1D
          946E69075FAE6E0639BD94F9CBEC63E89A59FF005C43B0D647A676DFD1DED14D
          268F53CA3B3DE1CAF8BDFF001A927FFFDA000C03010002000300000010E34F30
          D3CD38D3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF1CF
          1CB0CF1C30CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CFFFC400141101000000000000000000
          000000000000B0FFDA0008010301013F1039AFFFC40014110100000000000000
          0000000000000000B0FFDA0008010201013F1039AFFFC4001F10010101000301
          000301010000000000000111210031415160618171D0FFDA0008010100013F10
          FC69EC1EA80C92CC0C8BD02F11204DFD23EEAC72E8A70C501ABD580D4150108D
          A22108B383252ED843DB8A255529D82A18FA684CAB82B1514954ECA47291875C
          70D34F1DFAD2083E97A853A0B4E87F49658056A8EB8BC83F652AB64CF9CA7E2D
          95140345AF943BC6DAF46840DC1AF934609ECDCA42046942A1530DE16B96BA1A
          1683916E6CB03BAD655F5059419020DBDF0B0148AC200420A91170AD5F59385E
          0878B1FDF380E7BADA7DB4350A33ACC3F09321EF6CD8572B29F4908184166A88
          1A3C4727719C2A104180573DC4EDDE2A44726488938B49AFB00BE996AB9DBD57
          261335CEB4531963A65293257AE20325914081583215C723CA5D053E80201404
          F5DE1E9A2B0820920307FA5338AAAF8EFC02159B3CE25DF6F4510002210AF481
          38E00477F4EA7493F778B15D1D00147496B7B78451FA6707D8F66AF44813821A
          0E01438FBE40110870EECB62649E84047AA7478615CA083283180E1A9CD578D8
          6DABFD876C0AAEFF00CD49FFD9}
      end
      object QRShape30: TQRShape
        Left = 8
        Top = 416
        Width = 561
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          1100.666666666667000000
          1484.312500000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape31: TQRShape
        Left = 568
        Top = 416
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1100.666666666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qr56: TQRLabel
        Left = 8
        Top = 417
        Width = 177
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1103.312500000000000000
          468.312500000000100000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Local de Pagamento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr57: TQRLabel
        Left = 8
        Top = 429
        Width = 249
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1135.062500000000000000
          658.812500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Pag'#225'vel em qualquer banco at'#233' o vencimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr58: TQRLabel
        Left = 570
        Top = 417
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1103.312500000000000000
          354.541666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vencimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr59: TQRLabel
        Left = 568
        Top = 429
        Width = 137
        Height = 14
        Size.Values = (
          37.041666666666670000
          1502.833333333333000000
          1135.062500000000000000
          362.479166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qr59'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QRShape32: TQRShape
        Left = 568
        Top = 443
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1172.104166666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape33: TQRShape
        Left = 8
        Top = 443
        Width = 561
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          1172.104166666667000000
          1484.312500000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qr60: TQRLabel
        Left = 8
        Top = 444
        Width = 193
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1174.750000000000000000
          510.645833333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Cedente'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr61: TQRLabel
        Left = 570
        Top = 444
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1174.750000000000000000
          354.541666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ag'#234'ncia/C'#243'd.Cedente'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QRShape34: TQRShape
        Left = 8
        Top = 470
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          1243.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape35: TQRShape
        Left = 120
        Top = 470
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          317.500000000000000000
          1243.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape36: TQRShape
        Left = 344
        Top = 470
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          910.166666666666600000
          1243.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape37: TQRShape
        Left = 232
        Top = 470
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          613.833333333333400000
          1243.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape38: TQRShape
        Left = 456
        Top = 470
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          1206.500000000000000000
          1243.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape39: TQRShape
        Left = 568
        Top = 470
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1243.541666666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape40: TQRShape
        Left = 8
        Top = 497
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          1314.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape41: TQRShape
        Left = 120
        Top = 497
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          317.500000000000000000
          1314.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape42: TQRShape
        Left = 232
        Top = 497
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          613.833333333333400000
          1314.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape43: TQRShape
        Left = 344
        Top = 497
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          910.166666666666600000
          1314.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape44: TQRShape
        Left = 456
        Top = 497
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          1206.500000000000000000
          1314.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape45: TQRShape
        Left = 568
        Top = 497
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1314.979166666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape46: TQRShape
        Left = 8
        Top = 524
        Width = 561
        Height = 136
        Size.Values = (
          359.833333333333400000
          21.166666666666670000
          1386.416666666667000000
          1484.312500000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape47: TQRShape
        Left = 568
        Top = 524
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1386.416666666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape48: TQRShape
        Left = 568
        Top = 551
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1457.854166666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape49: TQRShape
        Left = 568
        Top = 578
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1529.291666666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape50: TQRShape
        Left = 568
        Top = 605
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1600.729166666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape51: TQRShape
        Left = 568
        Top = 632
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1672.166666666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape52: TQRShape
        Left = 8
        Top = 659
        Width = 697
        Height = 74
        Size.Values = (
          195.791666666666700000
          21.166666666666670000
          1743.604166666667000000
          1844.145833333333000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape53: TQRShape
        Left = 8
        Top = 376
        Width = 697
        Height = 25
        Frame.Style = psDot
        Size.Values = (
          66.145833333333340000
          21.166666666666670000
          994.833333333333400000
          1844.145833333333000000)
        XLColumn = 0
        Pen.Style = psDot
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape54: TQRShape
        Left = 8
        Top = 792
        Width = 697
        Height = 25
        Frame.Style = psDot
        Size.Values = (
          66.145833333333340000
          21.166666666666670000
          2095.500000000000000000
          1844.145833333333000000)
        XLColumn = 0
        Pen.Style = psDot
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object qr62: TQRLabel
        Left = 600
        Top = 734
        Width = 105
        Height = 14
        Size.Values = (
          37.041666666666670000
          1587.500000000000000000
          1942.041666666667000000
          277.812500000000000000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Autentica'#231#227'o Mec'#226'nica'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr63: TQRLabel
        Left = 600
        Top = 750
        Width = 105
        Height = 14
        Size.Values = (
          37.041666666666670000
          1587.500000000000000000
          1984.375000000000000000
          277.812500000000000000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ficha de Compensa'#231#227'o'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr64: TQRLabel
        Left = 8
        Top = 471
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1246.187500000000000000
          298.979166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Data Docto'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr65: TQRLabel
        Left = 120
        Top = 471
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          317.500000000000000000
          1246.187500000000000000
          298.979166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Nr. Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr66: TQRLabel
        Left = 232
        Top = 471
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          613.833333333333400000
          1246.187500000000000000
          298.979166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Esp'#233'cie'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr67: TQRLabel
        Left = 456
        Top = 471
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          1206.500000000000000000
          1246.187500000000000000
          298.979166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Data Proc.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr68: TQRLabel
        Left = 344
        Top = 471
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          910.166666666666600000
          1246.187500000000000000
          298.979166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Aceite'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr69: TQRLabel
        Left = 8
        Top = 498
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1317.625000000000000000
          298.979166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Uso do Banco'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr70: TQRLabel
        Left = 120
        Top = 498
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          317.500000000000000000
          1317.625000000000000000
          298.979166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Carteira'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr71: TQRLabel
        Left = 232
        Top = 498
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          613.833333333333400000
          1317.625000000000000000
          298.979166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Moeda'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr72: TQRLabel
        Left = 344
        Top = 498
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          910.166666666666600000
          1317.625000000000000000
          298.979166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Quantidade'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr73: TQRLabel
        Left = 456
        Top = 498
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          1206.500000000000000000
          1317.625000000000000000
          298.979166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Valor'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr74: TQRLabel
        Left = 8
        Top = 532
        Width = 193
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1407.583333333333000000
          510.645833333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Instru'#231#245'es'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr75: TQRLabel
        Left = 8
        Top = 660
        Width = 193
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1746.250000000000000000
          510.645833333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Sacado'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr76: TQRLabel
        Left = 8
        Top = 716
        Width = 193
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1894.416666666667000000
          510.645833333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Sacador/Avalista:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr77: TQRLabel
        Left = 570
        Top = 471
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1246.187500000000000000
          354.541666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nosso N'#250'mero'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr78: TQRLabel
        Left = 570
        Top = 498
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1317.625000000000000000
          354.541666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor do Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr79: TQRLabel
        Left = 570
        Top = 525
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1389.062500000000000000
          354.541666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(-) Desconto/Abatimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr80: TQRLabel
        Left = 570
        Top = 552
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1460.500000000000000000
          354.541666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(-) Outras Dedu'#231#245'es'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr81: TQRLabel
        Left = 570
        Top = 579
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1531.937500000000000000
          354.541666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(+) Mora/Multa'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr82: TQRLabel
        Left = 570
        Top = 606
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1603.375000000000000000
          354.541666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(+) Outros'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr83: TQRLabel
        Left = 570
        Top = 633
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1674.812500000000000000
          354.541666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(=) Valor Cobrado'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QRShape55: TQRShape
        Left = 8
        Top = 208
        Width = 697
        Height = 25
        Frame.Style = psDot
        Size.Values = (
          66.145833333333340000
          21.166666666666670000
          550.333333333333400000
          1844.145833333333000000)
        XLColumn = 0
        Pen.Style = psDot
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape56: TQRShape
        Left = 8
        Top = 265
        Width = 561
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          701.145833333333400000
          1484.312500000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRImage3: TQRImage
        Left = 8
        Top = 240
        Width = 145
        Height = 25
        Size.Values = (
          66.145833333333340000
          21.166666666666670000
          635.000000000000000000
          383.645833333333400000)
        XLColumn = 0
        Picture.Data = {
          0A544A504547496D6167655E180000FFD8FFE000104A46494600010100000100
          010000FFDB004300080606070605080707070909080A0C140D0C0B0B0C191213
          0F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F2739
          3D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232
          3232323232323232323232323232323232323232323232323232323232323232
          32323232323232323232323232FFC2001108041702CE03012200021101031101
          FFC4001A000100030101010000000000000000000000020506040307FFC40014
          010100000000000000000000000000000000FFDA000C03010002100310000001
          DFBE6FA5346CCFA9A167E45F315606954B406E5931AC646E8B463B4677327AC0
          0000000000000000000000000000000000000000000000000000033B2D00ACF0
          BA14DD5DE29BCEF4524AE452CAE0574FB853CED4717AF4000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000FFFC4002010000203000104030000000000000000000304010205001113
          14601523D0FFDA0008010100010502F5A4EB4A24DE85D431DD6E0D2F1CD6F25C
          29ACCC3989DD72BC2E8106EC364F198E877C4DDC7995D264C270C626424C5DBA
          48EA179D3F8C9E448C25F49AE4D6004CAA92252A4DAD9F11CBA1D4B0A0EA8DF3
          E26233220A5CEEE5E50FBFE3A074167083C32743882AD17295193CDD2EE588B5
          6EC8072107E6A47FFFC400141101000000000000000000000000000000B0FFDA
          0008010301013F0139AFFFC400141101000000000000000000000000000000B0
          FFDA0008010201013F0139AFFFC4003110000201030204030606030000000000
          00010203000411122113314151226181142432607191052342A1B1C1D0D1F0FF
          DA0008010100063F02F96ACE7E018C897C5719C0C67FE1BD3926378D08D48AA7
          5007CF90ABB58521D36C0312F9E58CD4315AA46247884C78BC80F4A782110092
          155E297CE093DAA59C2E9D50BEDF7AFC2E3B768C078793677217AD2C6A61910C
          A1084C92B9DB73CB9F4ABD930B981982FA0A0D25B7B466CB5685EF9AB62B7313
          B367C5206258F60399AB230C51EBB8D790C79115722E151648E40A74F23BA9FE
          E9A6C28889FCBEF8EE69E5BF859B32E62B8CF8547407B54B37555DBEBD2A5B44
          78D8695901439DF186FDFE4A16ED712B5B83911EC3EF53271E458666D6C831F1
          55D36A6F795D2DE5B62A2686678A58E3E187D8E57CC53491DCCB1BB8024231E2
          C7F06BD917223D1A2AD744CE8D6E30AD8076C6299B8F2683371B46DF154D8B89
          5239BE345C73A8E58E778CAC7C2D80DC7AD40B04AF1707233B1C83CF9D5B6967
          F77D5A73E75346C5B12B063E98FF005533A16C4A7515E80D32CB732340C73C2C
          0EF9E744BCF2B299049A7A6DD3E951CFA995D015F0F507BD2C6646908FD4DCCF
          F8D49FFFC4002310010100020202020300030000000000000111002131415161
          71816091A1D0E1F0FFDA0008010100013F21FC6BA74A3DAD18D887006F9F3700
          687B0791781E4EF1D7510A56A03BD3E319C3DA56EA1C96E742E029D93D7B71AF
          0DCF4807FA63B6D434B61A751D7BFAC1804713A1E130E15A54B18C52EF2FCA2D
          026DA2B4F1AAEF03518DC2487B217D59BC8808109D9BE3B9F5EF28EDDFD513E8
          62891C9E434FC8F5D60DB02CB57738B3E5706F02372F87F5335A5C0EC367C0D7
          BFC2B867CC0EEC4152E6A3E0DDD56A59A358BEA070E82D3F7911C44207484C7E
          065B512F1F61FAC6D40A8E766DF9DDCD493B5082A2730CD1441306D5ACA9EB05
          E0BC30C4B66A9CF9C997BAA62DE8E112C00150351CA9FEB37202DE37DEEBDE1D
          946E69075FAE6E0639BD94F9CBEC63E89A59FF005C43B0D647A676DFD1DED14D
          268F53CA3B3DE1CAF8BDFF001A927FFFDA000C03010002000300000010E34F30
          D3CD38D3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF1CF
          1CB0CF1C30CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CFFFC400141101000000000000000000
          000000000000B0FFDA0008010301013F1039AFFFC40014110100000000000000
          0000000000000000B0FFDA0008010201013F1039AFFFC4001F10010101000301
          000301010000000000000111210031415160618171D0FFDA0008010100013F10
          FC69EC1EA80C92CC0C8BD02F11204DFD23EEAC72E8A70C501ABD580D4150108D
          A22108B383252ED843DB8A255529D82A18FA684CAB82B1514954ECA47291875C
          70D34F1DFAD2083E97A853A0B4E87F49658056A8EB8BC83F652AB64CF9CA7E2D
          95140345AF943BC6DAF46840DC1AF934609ECDCA42046942A1530DE16B96BA1A
          1683916E6CB03BAD655F5059419020DBDF0B0148AC200420A91170AD5F59385E
          0878B1FDF380E7BADA7DB4350A33ACC3F09321EF6CD8572B29F4908184166A88
          1A3C4727719C2A104180573DC4EDDE2A44726488938B49AFB00BE996AB9DBD57
          261335CEB4531963A65293257AE20325914081583215C723CA5D053E80201404
          F5DE1E9A2B0820920307FA5338AAAF8EFC02159B3CE25DF6F4510002210AF481
          38E00477F4EA7493F778B15D1D00147496B7B78451FA6707D8F66AF44813821A
          0E01438FBE40110870EECB62649E84047AA7478615CA083283180E1A9CD578D8
          6DABFD876C0AAEFF00CD49FFD9}
      end
      object QRShape57: TQRShape
        Left = 8
        Top = 292
        Width = 561
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          772.583333333333400000
          1484.312500000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape58: TQRShape
        Left = 8
        Top = 319
        Width = 561
        Height = 40
        Size.Values = (
          105.833333333333300000
          21.166666666666670000
          844.020833333333500000
          1484.312500000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qr84: TQRLabel
        Left = 8
        Top = 266
        Width = 193
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          703.791666666666800000
          510.645833333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Cedente'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
    end
    object QRBand2: TQRBand
      Left = 38
      Top = 2140
      Width = 718
      Height = 1051
      Frame.Style = psClear
      AlignToBottom = False
      Color = clWindow
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        2780.770833333333000000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbTitle
      object qr85: TQRLabel
        Left = 16
        Top = 177
        Width = 681
        Height = 17
        Size.Values = (
          44.979166666666670000
          42.333333333333340000
          468.312500000000100000
          1801.812500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 
          'Garanta a felicidade da sua empresa e de seu cliente utilizando ' +
          'o ADM - Sistema de Gest'#227'o Completo para sua Empresa.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 8
      end
      object QRImage4: TQRImage
        Left = 8
        Top = 392
        Width = 145
        Height = 25
        Size.Values = (
          66.145833333333340000
          21.166666666666670000
          1037.166666666667000000
          383.645833333333400000)
        XLColumn = 0
        Picture.Data = {
          0A544A504547496D6167655E180000FFD8FFE000104A46494600010100000100
          010000FFDB004300080606070605080707070909080A0C140D0C0B0B0C191213
          0F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F2739
          3D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232
          3232323232323232323232323232323232323232323232323232323232323232
          32323232323232323232323232FFC2001108041702CE03012200021101031101
          FFC4001A000100030101010000000000000000000000020506040307FFC40014
          010100000000000000000000000000000000FFDA000C03010002100310000001
          DFBE6FA5346CCFA9A167E45F315606954B406E5931AC646E8B463B4677327AC0
          0000000000000000000000000000000000000000000000000000033B2D00ACF0
          BA14DD5DE29BCEF4524AE452CAE0574FB853CED4717AF4000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000FFFC4002010000203000104030000000000000000000304010205001113
          14601523D0FFDA0008010100010502F5A4EB4A24DE85D431DD6E0D2F1CD6F25C
          29ACCC3989DD72BC2E8106EC364F198E877C4DDC7995D264C270C626424C5DBA
          48EA179D3F8C9E448C25F49AE4D6004CAA92252A4DAD9F11CBA1D4B0A0EA8DF3
          E26233220A5CEEE5E50FBFE3A074167083C32743882AD17295193CDD2EE588B5
          6EC8072107E6A47FFFC400141101000000000000000000000000000000B0FFDA
          0008010301013F0139AFFFC400141101000000000000000000000000000000B0
          FFDA0008010201013F0139AFFFC4003110000201030204030606030000000000
          00010203000411122113314151226181142432607191052342A1B1C1D0D1F0FF
          DA0008010100063F02F96ACE7E018C897C5719C0C67FE1BD3926378D08D48AA7
          5007CF90ABB58521D36C0312F9E58CD4315AA46247884C78BC80F4A782110092
          155E297CE093DAA59C2E9D50BEDF7AFC2E3B768C078793677217AD2C6A61910C
          A1084C92B9DB73CB9F4ABD930B981982FA0A0D25B7B466CB5685EF9AB62B7313
          B367C5206258F60399AB230C51EBB8D790C79115722E151648E40A74F23BA9FE
          E9A6C28889FCBEF8EE69E5BF859B32E62B8CF8547407B54B37555DBEBD2A5B44
          78D8695901439DF186FDFE4A16ED712B5B83911EC3EF53271E458666D6C831F1
          55D36A6F795D2DE5B62A2686678A58E3E187D8E57CC53491DCCB1BB8024231E2
          C7F06BD917223D1A2AD744CE8D6E30AD8076C6299B8F2683371B46DF154D8B89
          5239BE345C73A8E58E778CAC7C2D80DC7AD40B04AF1707233B1C83CF9D5B6967
          F77D5A73E75346C5B12B063E98FF005533A16C4A7515E80D32CB732340C73C2C
          0EF9E744BCF2B299049A7A6DD3E951CFA995D015F0F507BD2C6646908FD4DCCF
          F8D49FFFC4002310010100020202020300030000000000000111002131415161
          71816091A1D0E1F0FFDA0008010100013F21FC6BA74A3DAD18D887006F9F3700
          687B0791781E4EF1D7510A56A03BD3E319C3DA56EA1C96E742E029D93D7B71AF
          0DCF4807FA63B6D434B61A751D7BFAC1804713A1E130E15A54B18C52EF2FCA2D
          026DA2B4F1AAEF03518DC2487B217D59BC8808109D9BE3B9F5EF28EDDFD513E8
          62891C9E434FC8F5D60DB02CB57738B3E5706F02372F87F5335A5C0EC367C0D7
          BFC2B867CC0EEC4152E6A3E0DDD56A59A358BEA070E82D3F7911C44207484C7E
          065B512F1F61FAC6D40A8E766DF9DDCD493B5082A2730CD1441306D5ACA9EB05
          E0BC30C4B66A9CF9C997BAA62DE8E112C00150351CA9FEB37202DE37DEEBDE1D
          946E69075FAE6E0639BD94F9CBEC63E89A59FF005C43B0D647A676DFD1DED14D
          268F53CA3B3DE1CAF8BDFF001A927FFFDA000C03010002000300000010E34F30
          D3CD38D3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF1CF
          1CB0CF1C30CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CFFFC400141101000000000000000000
          000000000000B0FFDA0008010301013F1039AFFFC40014110100000000000000
          0000000000000000B0FFDA0008010201013F1039AFFFC4001F10010101000301
          000301010000000000000111210031415160618171D0FFDA0008010100013F10
          FC69EC1EA80C92CC0C8BD02F11204DFD23EEAC72E8A70C501ABD580D4150108D
          A22108B383252ED843DB8A255529D82A18FA684CAB82B1514954ECA47291875C
          70D34F1DFAD2083E97A853A0B4E87F49658056A8EB8BC83F652AB64CF9CA7E2D
          95140345AF943BC6DAF46840DC1AF934609ECDCA42046942A1530DE16B96BA1A
          1683916E6CB03BAD655F5059419020DBDF0B0148AC200420A91170AD5F59385E
          0878B1FDF380E7BADA7DB4350A33ACC3F09321EF6CD8572B29F4908184166A88
          1A3C4727719C2A104180573DC4EDDE2A44726488938B49AFB00BE996AB9DBD57
          261335CEB4531963A65293257AE20325914081583215C723CA5D053E80201404
          F5DE1E9A2B0820920307FA5338AAAF8EFC02159B3CE25DF6F4510002210AF481
          38E00477F4EA7493F778B15D1D00147496B7B78451FA6707D8F66AF44813821A
          0E01438FBE40110870EECB62649E84047AA7478615CA083283180E1A9CD578D8
          6DABFD876C0AAEFF00CD49FFD9}
      end
      object QRShape59: TQRShape
        Left = 8
        Top = 416
        Width = 561
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          1100.666666666667000000
          1484.312500000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape60: TQRShape
        Left = 568
        Top = 416
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1100.666666666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qr86: TQRLabel
        Left = 8
        Top = 417
        Width = 177
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1103.312500000000000000
          468.312500000000100000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Local de Pagamento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr87: TQRLabel
        Left = 8
        Top = 429
        Width = 249
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1135.062500000000000000
          658.812500000000000000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Pag'#225'vel em qualquer banco at'#233' o vencimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr88: TQRLabel
        Left = 570
        Top = 417
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1103.312500000000000000
          354.541666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vencimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr89: TQRLabel
        Left = 568
        Top = 429
        Width = 137
        Height = 14
        Size.Values = (
          37.041666666666670000
          1502.833333333333000000
          1135.062500000000000000
          362.479166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qr89'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QRShape61: TQRShape
        Left = 568
        Top = 443
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1172.104166666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape62: TQRShape
        Left = 8
        Top = 443
        Width = 561
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          1172.104166666667000000
          1484.312500000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qr90: TQRLabel
        Left = 8
        Top = 444
        Width = 193
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1174.750000000000000000
          510.645833333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Cedente'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr91: TQRLabel
        Left = 570
        Top = 444
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1174.750000000000000000
          354.541666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ag'#234'ncia/C'#243'd.Cedente'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QRShape63: TQRShape
        Left = 8
        Top = 470
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          1243.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape64: TQRShape
        Left = 120
        Top = 470
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          317.500000000000000000
          1243.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape65: TQRShape
        Left = 344
        Top = 470
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          910.166666666666600000
          1243.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape66: TQRShape
        Left = 232
        Top = 470
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          613.833333333333400000
          1243.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape67: TQRShape
        Left = 456
        Top = 470
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          1206.500000000000000000
          1243.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape68: TQRShape
        Left = 568
        Top = 470
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1243.541666666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape69: TQRShape
        Left = 8
        Top = 497
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          1314.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape70: TQRShape
        Left = 120
        Top = 497
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          317.500000000000000000
          1314.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape71: TQRShape
        Left = 232
        Top = 497
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          613.833333333333400000
          1314.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape72: TQRShape
        Left = 344
        Top = 497
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          910.166666666666600000
          1314.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape73: TQRShape
        Left = 456
        Top = 497
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          1206.500000000000000000
          1314.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape74: TQRShape
        Left = 568
        Top = 497
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1314.979166666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape75: TQRShape
        Left = 8
        Top = 524
        Width = 561
        Height = 136
        Size.Values = (
          359.833333333333400000
          21.166666666666670000
          1386.416666666667000000
          1484.312500000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape76: TQRShape
        Left = 568
        Top = 524
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1386.416666666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape77: TQRShape
        Left = 568
        Top = 551
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1457.854166666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape78: TQRShape
        Left = 568
        Top = 578
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1529.291666666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape79: TQRShape
        Left = 568
        Top = 605
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1600.729166666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape80: TQRShape
        Left = 568
        Top = 632
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1672.166666666667000000
          362.479166666666700000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape81: TQRShape
        Left = 8
        Top = 659
        Width = 697
        Height = 74
        Size.Values = (
          195.791666666666700000
          21.166666666666670000
          1743.604166666667000000
          1844.145833333333000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape82: TQRShape
        Left = 8
        Top = 376
        Width = 697
        Height = 25
        Frame.Style = psDot
        Size.Values = (
          66.145833333333340000
          21.166666666666670000
          994.833333333333400000
          1844.145833333333000000)
        XLColumn = 0
        Pen.Style = psDot
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape83: TQRShape
        Left = 8
        Top = 792
        Width = 697
        Height = 25
        Frame.Style = psDot
        Size.Values = (
          66.145833333333340000
          21.166666666666670000
          2095.500000000000000000
          1844.145833333333000000)
        XLColumn = 0
        Pen.Style = psDot
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object qr92: TQRLabel
        Left = 600
        Top = 734
        Width = 105
        Height = 14
        Size.Values = (
          37.041666666666670000
          1587.500000000000000000
          1942.041666666667000000
          277.812500000000000000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Autentica'#231#227'o Mec'#226'nica'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr93: TQRLabel
        Left = 600
        Top = 750
        Width = 105
        Height = 14
        Size.Values = (
          37.041666666666670000
          1587.500000000000000000
          1984.375000000000000000
          277.812500000000000000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ficha de Compensa'#231#227'o'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr94: TQRLabel
        Left = 8
        Top = 471
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1246.187500000000000000
          298.979166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Data Docto'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr95: TQRLabel
        Left = 120
        Top = 471
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          317.500000000000000000
          1246.187500000000000000
          298.979166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Nr. Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr96: TQRLabel
        Left = 232
        Top = 471
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          613.833333333333400000
          1246.187500000000000000
          298.979166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Esp'#233'cie'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr97: TQRLabel
        Left = 456
        Top = 471
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          1206.500000000000000000
          1246.187500000000000000
          298.979166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Data Proc.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr98: TQRLabel
        Left = 344
        Top = 471
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          910.166666666666600000
          1246.187500000000000000
          298.979166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Aceite'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr99: TQRLabel
        Left = 8
        Top = 498
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1317.625000000000000000
          298.979166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Uso do Banco'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr100: TQRLabel
        Left = 120
        Top = 498
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          317.500000000000000000
          1317.625000000000000000
          298.979166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Carteira'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr101: TQRLabel
        Left = 232
        Top = 498
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          613.833333333333400000
          1317.625000000000000000
          298.979166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Moeda'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr102: TQRLabel
        Left = 344
        Top = 498
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          910.166666666666600000
          1317.625000000000000000
          298.979166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Quantidade'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr103: TQRLabel
        Left = 456
        Top = 498
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          1206.500000000000000000
          1317.625000000000000000
          298.979166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Valor'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr104: TQRLabel
        Left = 8
        Top = 532
        Width = 193
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1407.583333333333000000
          510.645833333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Instru'#231#245'es'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr105: TQRLabel
        Left = 8
        Top = 660
        Width = 193
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1746.250000000000000000
          510.645833333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Sacado'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr106: TQRLabel
        Left = 8
        Top = 716
        Width = 193
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1894.416666666667000000
          510.645833333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Sacador/Avalista:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr107: TQRLabel
        Left = 570
        Top = 471
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1246.187500000000000000
          354.541666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nosso N'#250'mero'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr108: TQRLabel
        Left = 570
        Top = 498
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1317.625000000000000000
          354.541666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor do Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr109: TQRLabel
        Left = 570
        Top = 525
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1389.062500000000000000
          354.541666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(-) Desconto/Abatimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr110: TQRLabel
        Left = 570
        Top = 552
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1460.500000000000000000
          354.541666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(-) Outras Dedu'#231#245'es'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr111: TQRLabel
        Left = 570
        Top = 579
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1531.937500000000000000
          354.541666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(+) Mora/Multa'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr112: TQRLabel
        Left = 570
        Top = 606
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1603.375000000000000000
          354.541666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(+) Outros'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object qr113: TQRLabel
        Left = 570
        Top = 633
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1674.812500000000000000
          354.541666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(=) Valor Cobrado'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
      object QRShape84: TQRShape
        Left = 8
        Top = 208
        Width = 697
        Height = 25
        Frame.Style = psDot
        Size.Values = (
          66.145833333333340000
          21.166666666666670000
          550.333333333333400000
          1844.145833333333000000)
        XLColumn = 0
        Pen.Style = psDot
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape85: TQRShape
        Left = 8
        Top = 265
        Width = 561
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          701.145833333333400000
          1484.312500000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRImage5: TQRImage
        Left = 8
        Top = 240
        Width = 145
        Height = 25
        Size.Values = (
          66.145833333333340000
          21.166666666666670000
          635.000000000000000000
          383.645833333333400000)
        XLColumn = 0
        Picture.Data = {
          0A544A504547496D6167655E180000FFD8FFE000104A46494600010100000100
          010000FFDB004300080606070605080707070909080A0C140D0C0B0B0C191213
          0F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F2739
          3D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232
          3232323232323232323232323232323232323232323232323232323232323232
          32323232323232323232323232FFC2001108041702CE03012200021101031101
          FFC4001A000100030101010000000000000000000000020506040307FFC40014
          010100000000000000000000000000000000FFDA000C03010002100310000001
          DFBE6FA5346CCFA9A167E45F315606954B406E5931AC646E8B463B4677327AC0
          0000000000000000000000000000000000000000000000000000033B2D00ACF0
          BA14DD5DE29BCEF4524AE452CAE0574FB853CED4717AF4000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000FFFC4002010000203000104030000000000000000000304010205001113
          14601523D0FFDA0008010100010502F5A4EB4A24DE85D431DD6E0D2F1CD6F25C
          29ACCC3989DD72BC2E8106EC364F198E877C4DDC7995D264C270C626424C5DBA
          48EA179D3F8C9E448C25F49AE4D6004CAA92252A4DAD9F11CBA1D4B0A0EA8DF3
          E26233220A5CEEE5E50FBFE3A074167083C32743882AD17295193CDD2EE588B5
          6EC8072107E6A47FFFC400141101000000000000000000000000000000B0FFDA
          0008010301013F0139AFFFC400141101000000000000000000000000000000B0
          FFDA0008010201013F0139AFFFC4003110000201030204030606030000000000
          00010203000411122113314151226181142432607191052342A1B1C1D0D1F0FF
          DA0008010100063F02F96ACE7E018C897C5719C0C67FE1BD3926378D08D48AA7
          5007CF90ABB58521D36C0312F9E58CD4315AA46247884C78BC80F4A782110092
          155E297CE093DAA59C2E9D50BEDF7AFC2E3B768C078793677217AD2C6A61910C
          A1084C92B9DB73CB9F4ABD930B981982FA0A0D25B7B466CB5685EF9AB62B7313
          B367C5206258F60399AB230C51EBB8D790C79115722E151648E40A74F23BA9FE
          E9A6C28889FCBEF8EE69E5BF859B32E62B8CF8547407B54B37555DBEBD2A5B44
          78D8695901439DF186FDFE4A16ED712B5B83911EC3EF53271E458666D6C831F1
          55D36A6F795D2DE5B62A2686678A58E3E187D8E57CC53491DCCB1BB8024231E2
          C7F06BD917223D1A2AD744CE8D6E30AD8076C6299B8F2683371B46DF154D8B89
          5239BE345C73A8E58E778CAC7C2D80DC7AD40B04AF1707233B1C83CF9D5B6967
          F77D5A73E75346C5B12B063E98FF005533A16C4A7515E80D32CB732340C73C2C
          0EF9E744BCF2B299049A7A6DD3E951CFA995D015F0F507BD2C6646908FD4DCCF
          F8D49FFFC4002310010100020202020300030000000000000111002131415161
          71816091A1D0E1F0FFDA0008010100013F21FC6BA74A3DAD18D887006F9F3700
          687B0791781E4EF1D7510A56A03BD3E319C3DA56EA1C96E742E029D93D7B71AF
          0DCF4807FA63B6D434B61A751D7BFAC1804713A1E130E15A54B18C52EF2FCA2D
          026DA2B4F1AAEF03518DC2487B217D59BC8808109D9BE3B9F5EF28EDDFD513E8
          62891C9E434FC8F5D60DB02CB57738B3E5706F02372F87F5335A5C0EC367C0D7
          BFC2B867CC0EEC4152E6A3E0DDD56A59A358BEA070E82D3F7911C44207484C7E
          065B512F1F61FAC6D40A8E766DF9DDCD493B5082A2730CD1441306D5ACA9EB05
          E0BC30C4B66A9CF9C997BAA62DE8E112C00150351CA9FEB37202DE37DEEBDE1D
          946E69075FAE6E0639BD94F9CBEC63E89A59FF005C43B0D647A676DFD1DED14D
          268F53CA3B3DE1CAF8BDFF001A927FFFDA000C03010002000300000010E34F30
          D3CD38D3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF1CF
          1CB0CF1C30CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CFFFC400141101000000000000000000
          000000000000B0FFDA0008010301013F1039AFFFC40014110100000000000000
          0000000000000000B0FFDA0008010201013F1039AFFFC4001F10010101000301
          000301010000000000000111210031415160618171D0FFDA0008010100013F10
          FC69EC1EA80C92CC0C8BD02F11204DFD23EEAC72E8A70C501ABD580D4150108D
          A22108B383252ED843DB8A255529D82A18FA684CAB82B1514954ECA47291875C
          70D34F1DFAD2083E97A853A0B4E87F49658056A8EB8BC83F652AB64CF9CA7E2D
          95140345AF943BC6DAF46840DC1AF934609ECDCA42046942A1530DE16B96BA1A
          1683916E6CB03BAD655F5059419020DBDF0B0148AC200420A91170AD5F59385E
          0878B1FDF380E7BADA7DB4350A33ACC3F09321EF6CD8572B29F4908184166A88
          1A3C4727719C2A104180573DC4EDDE2A44726488938B49AFB00BE996AB9DBD57
          261335CEB4531963A65293257AE20325914081583215C723CA5D053E80201404
          F5DE1E9A2B0820920307FA5338AAAF8EFC02159B3CE25DF6F4510002210AF481
          38E00477F4EA7493F778B15D1D00147496B7B78451FA6707D8F66AF44813821A
          0E01438FBE40110870EECB62649E84047AA7478615CA083283180E1A9CD578D8
          6DABFD876C0AAEFF00CD49FFD9}
      end
      object QRShape86: TQRShape
        Left = 8
        Top = 292
        Width = 561
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          772.583333333333400000
          1484.312500000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape87: TQRShape
        Left = 8
        Top = 319
        Width = 561
        Height = 40
        Size.Values = (
          105.833333333333300000
          21.166666666666670000
          844.020833333333500000
          1484.312500000000000000)
        XLColumn = 0
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qr114: TQRLabel
        Left = 8
        Top = 266
        Width = 193
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          703.791666666666800000
          510.645833333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Cedente'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 7
      end
    end
  end
end
