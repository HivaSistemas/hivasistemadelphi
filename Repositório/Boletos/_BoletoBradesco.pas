unit _BoletoBradesco;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPrincipal, Vcl.Imaging.jpeg, _CobrancaBancaria,
  QRCtrls, QuickRpt, Vcl.ExtCtrls, QRBarcodeComum, QRWebFilt, QRPDFFilt, _Biblioteca, _Sessao,
  QRExport;

type
  TFormBoletoBradesco = class(TFormHerancaPrincipal)
    qrBoletaBradesco: TQuickRep;
    qrbnd1: TQRBand;
    qrl1: TQRLabel;
    QRShape209: TQRShape;
    QRShape208: TQRShape;
    QRShape121: TQRShape;
    QRShape4: TQRShape;
    QRShape125: TQRShape;
    QRShape127: TQRShape;
    QRShape5: TQRShape;
    QRShape126: TQRShape;
    QRShape1: TQRShape;
    QRShape193: TQRShape;
    QRShape199: TQRShape;
    QRShape122: TQRShape;
    QRShape207: TQRShape;
    QRShape212: TQRShape;
    QRShape211: TQRShape;
    QRShape213: TQRShape;
    QRShape214: TQRShape;
    QRShape216: TQRShape;
    QRShape215: TQRShape;
    QRShape217: TQRShape;
    QRShape218: TQRShape;
    QRShape219: TQRShape;
    QRShape3: TQRShape;
    QRShape220: TQRShape;
    QRShape221: TQRShape;
    QRShape222: TQRShape;
    QRShape223: TQRShape;
    QRShape224: TQRShape;
    QRShape225: TQRShape;
    QRShape226: TQRShape;
    QRShape227: TQRShape;
    QRShape228: TQRShape;
    QRShape229: TQRShape;
    QRShape230: TQRShape;
    QRShape231: TQRShape;
    QRShape95: TQRShape;
    QRShape98: TQRShape;
    QRShape100: TQRShape;
    QRShape99: TQRShape;
    QRShape101: TQRShape;
    QRShape102: TQRShape;
    QRShape104: TQRShape;
    QRShape103: TQRShape;
    QRShape105: TQRShape;
    QRShape106: TQRShape;
    QRShape107: TQRShape;
    QRShape2: TQRShape;
    QRShape108: TQRShape;
    QRShape109: TQRShape;
    QRShape110: TQRShape;
    QRShape111: TQRShape;
    QRShape112: TQRShape;
    QRShape113: TQRShape;
    QRShape114: TQRShape;
    QRShape115: TQRShape;
    QRShape116: TQRShape;
    QRShape117: TQRShape;
    QRShape118: TQRShape;
    QRShape119: TQRShape;
    qrl2: TQRLabel;
    qrl3: TQRLabel;
    qrlVencimentoBra: TQRLabel;
    qrl4: TQRLabel;
    qrl5: TQRLabel;
    qrl6: TQRLabel;
    qrl7: TQRLabel;
    qrl8: TQRLabel;
    qrl9: TQRLabel;
    qrl10: TQRLabel;
    qrl11: TQRLabel;
    qrl12: TQRLabel;
    qrl13: TQRLabel;
    qrl14: TQRLabel;
    qrl15: TQRLabel;
    qrl16: TQRLabel;
    qrlInstrucaoBra: TQRLabel;
    qrl17: TQRLabel;
    qrl18: TQRLabel;
    qrl19: TQRLabel;
    qrl20: TQRLabel;
    qrl21: TQRLabel;
    qrl22: TQRLabel;
    qrl23: TQRLabel;
    qrl24: TQRLabel;
    qrlBeneficiarioBra: TQRLabel;
    qrlDataEmissaoBra: TQRLabel;
    qrlNumeroDocumentoBra: TQRLabel;
    qrlEspecieDocumentoBra: TQRLabel;
    qrl25: TQRLabel;
    qrlCarteiraBra: TQRLabel;
    qrlAceiteBra: TQRLabel;
    qrlDataProcessamentoBra: TQRLabel;
    qrlPagadorBra: TQRLabel;
    qrlValorDocumentoBra: TQRLabel;
    qrlAgenciaBeneficiarioBra: TQRLabel;
    qrl26: TQRLabel;
    qrlNossoNumeroBra: TQRLabel;
    qrl27: TQRLabel;
    qrl28: TQRLabel;
    QRShape123: TQRShape;
    qrCodigoBarraBra: TQRBarcode;
    QRShape124: TQRShape;
    qrl29: TQRLabel;
    qrlVencimentoBra3: TQRLabel;
    qrl30: TQRLabel;
    qrl31: TQRLabel;
    qrl32: TQRLabel;
    qrl33: TQRLabel;
    qrlBeneficiarioBra3: TQRLabel;
    qrlValorDocumentoBra3: TQRLabel;
    qrlAgenciaBeneficiarioBra3: TQRLabel;
    qrl34: TQRLabel;
    qrlNossoNumeroBra3: TQRLabel;
    qrl35: TQRLabel;
    QRShape132: TQRShape;
    QRShape133: TQRShape;
    qrlLinhaDigitavelBRA: TQRLabel;
    qrlNome_Banco: TQRLabel;
    qrl36: TQRLabel;
    qrl37: TQRLabel;
    qrlVencimentoBra2: TQRLabel;
    qrl38: TQRLabel;
    qrl39: TQRLabel;
    qrl40: TQRLabel;
    qrl41: TQRLabel;
    qrl42: TQRLabel;
    qrl43: TQRLabel;
    qrl44: TQRLabel;
    qrl45: TQRLabel;
    qrl46: TQRLabel;
    qrl47: TQRLabel;
    qrl48: TQRLabel;
    qrl49: TQRLabel;
    qrl50: TQRLabel;
    qrl51: TQRLabel;
    qrl52: TQRLabel;
    qrl53: TQRLabel;
    qrl54: TQRLabel;
    qrl55: TQRLabel;
    qrl56: TQRLabel;
    qrl57: TQRLabel;
    qrl58: TQRLabel;
    QRShape232: TQRShape;
    QRShape233: TQRShape;
    qrlBeneficiarioBra2: TQRLabel;
    qrlDataEmissaoBra2: TQRLabel;
    qrlNumeroDocumentoBra2: TQRLabel;
    qrlEspecieDocumentoBra2: TQRLabel;
    qrl59: TQRLabel;
    qrlCarteiraBra2: TQRLabel;
    qrlAceite: TQRLabel;
    qrlDataProcessamentoBra2: TQRLabel;
    qrlPagadorBra2: TQRLabel;
    qrlValorDocumentoBra2: TQRLabel;
    qrlAgenciaBeneficiarioBra2: TQRLabel;
    qrl60: TQRLabel;
    qrl61: TQRLabel;
    qrlNossoNumeroBra2: TQRLabel;
    qrl62: TQRLabel;
    qrl63: TQRLabel;
    qrl64: TQRLabel;
    QRShape236: TQRShape;
    qrlCpfCnpjBra: TQRLabel;
    qrlLogradouroBra: TQRLabel;
    qrlCepBra: TQRLabel;
    qrlCidadeBra: TQRLabel;
    qrlCpfCnpjBra2: TQRLabel;
    qrlLogradouroBra2: TQRLabel;
    qrlCepBra2: TQRLabel;
    qrlCidadeBra2: TQRLabel;
    QRShape235: TQRShape;
    qrl65: TQRLabel;
    qrl66: TQRLabel;
    qrlNumero_Nota2: TQRLabel;
    qrlNumero_Nota: TQRLabel;
    qrl67: TQRLabel;
    qrl68: TQRLabel;
    qrlPagadorBra3: TQRLabel;
    qrlNumeroDocumentoBra3: TQRLabel;
    qrl69: TQRLabel;
    qrlPedido: TQRLabel;
    qrlPedido2: TQRLabel;
    qrlDocumento: TQRLabel;
    qrlDocumento2: TQRLabel;
    qrlDataEmissao: TQRLabel;
    qrlDataEmissaoBra3: TQRLabel;
    qrlDocumento3: TQRLabel;
    qrlPedido3: TQRLabel;
    qrlNumero_Nota3: TQRLabel;
    qrl70: TQRLabel;
    qrlNomeFantasiaEmpresa: TQRLabel;
    QRShape93: TQRShape;
    qrLogo: TQRImage;
    qrTexto: TQRMemo;
    qrlCip2: TQRLabel;
    qrlCip: TQRLabel;
    qrlEnderecoBeneficiario: TQRLabel;
    qrl71: TQRLabel;
    qrlCNPJBeneficiarioBra: TQRLabel;
    qrlCNPJBeneficiarioBra2: TQRLabel;
    qrl72: TQRLabel;
    qrl73: TQRLabel;
    qrlUsoBanco2: TQRLabel;
    qrlUsoBanco: TQRLabel;
    qrlSacadorAvalista2: TQRLabel;
    qrlLbCpfCpfDaycoval2: TQRLabel;
    qrlCpfCnpjSacAva2: TQRLabel;
    qrlCpfCnpjSacAva: TQRLabel;
    qrlLbCpfCpfDaycoval: TQRLabel;
    qrlSacadorAvalista: TQRLabel;
    qrlObservacao2: TQRLabel;
    qrlObservacao: TQRLabel;
    qrlInstrucaoBra2: TQRLabel;
    qrbnd2: TQRBand;
    qrl74: TQRLabel;
    QRImage8: TQRImage;
    QRShape135: TQRShape;
    QRShape136: TQRShape;
    qrl75: TQRLabel;
    qrl76: TQRLabel;
    qrl77: TQRLabel;
    qrl78: TQRLabel;
    QRShape137: TQRShape;
    QRShape138: TQRShape;
    qrl79: TQRLabel;
    qrl80: TQRLabel;
    QRShape139: TQRShape;
    QRShape140: TQRShape;
    QRShape141: TQRShape;
    QRShape142: TQRShape;
    QRShape143: TQRShape;
    QRShape144: TQRShape;
    QRShape145: TQRShape;
    QRShape146: TQRShape;
    QRShape147: TQRShape;
    QRShape148: TQRShape;
    QRShape149: TQRShape;
    QRShape150: TQRShape;
    QRShape151: TQRShape;
    QRShape152: TQRShape;
    QRShape153: TQRShape;
    QRShape154: TQRShape;
    QRShape155: TQRShape;
    QRShape156: TQRShape;
    QRShape157: TQRShape;
    QRShape158: TQRShape;
    QRShape159: TQRShape;
    qrl81: TQRLabel;
    qrl82: TQRLabel;
    qrl83: TQRLabel;
    qrl84: TQRLabel;
    qrl85: TQRLabel;
    qrl86: TQRLabel;
    qrl87: TQRLabel;
    qrl88: TQRLabel;
    qrl89: TQRLabel;
    qrl90: TQRLabel;
    qrl91: TQRLabel;
    qrl92: TQRLabel;
    qrl93: TQRLabel;
    qrl94: TQRLabel;
    qrl95: TQRLabel;
    qrl96: TQRLabel;
    qrl97: TQRLabel;
    qrl98: TQRLabel;
    qrl99: TQRLabel;
    qrl100: TQRLabel;
    qrl101: TQRLabel;
    qrl102: TQRLabel;
    QRShape160: TQRShape;
    QRShape161: TQRShape;
    QRImage9: TQRImage;
    QRShape162: TQRShape;
    QRShape163: TQRShape;
    qrl103: TQRLabel;
    qrbnd3: TQRBand;
    qrl104: TQRLabel;
    QRImage10: TQRImage;
    QRShape164: TQRShape;
    QRShape165: TQRShape;
    qrl105: TQRLabel;
    qrl106: TQRLabel;
    qrl107: TQRLabel;
    qrl108: TQRLabel;
    QRShape166: TQRShape;
    QRShape167: TQRShape;
    qrl109: TQRLabel;
    qrl110: TQRLabel;
    QRShape168: TQRShape;
    QRShape169: TQRShape;
    QRShape170: TQRShape;
    QRShape171: TQRShape;
    QRShape172: TQRShape;
    QRShape173: TQRShape;
    QRShape174: TQRShape;
    QRShape175: TQRShape;
    QRShape176: TQRShape;
    QRShape177: TQRShape;
    QRShape178: TQRShape;
    QRShape179: TQRShape;
    QRShape180: TQRShape;
    QRShape181: TQRShape;
    QRShape182: TQRShape;
    QRShape183: TQRShape;
    QRShape184: TQRShape;
    QRShape185: TQRShape;
    QRShape186: TQRShape;
    QRShape187: TQRShape;
    QRShape188: TQRShape;
    qrl111: TQRLabel;
    qrl112: TQRLabel;
    qrl113: TQRLabel;
    qrl114: TQRLabel;
    qrl115: TQRLabel;
    qrl116: TQRLabel;
    qrl117: TQRLabel;
    qrl118: TQRLabel;
    qrl119: TQRLabel;
    qrl120: TQRLabel;
    qrl121: TQRLabel;
    qrl122: TQRLabel;
    qrl123: TQRLabel;
    qrl124: TQRLabel;
    qrl125: TQRLabel;
    qrl126: TQRLabel;
    qrl127: TQRLabel;
    qrl128: TQRLabel;
    qrl129: TQRLabel;
    qrl130: TQRLabel;
    qrl131: TQRLabel;
    qrl132: TQRLabel;
    QRShape189: TQRShape;
    QRShape190: TQRShape;
    QRImage11: TQRImage;
    QRShape191: TQRShape;
    QRShape192: TQRShape;
    qrl133: TQRLabel;
    QRExcelFilter2: TQRExcelFilter;
    QRPDFFilter2: TQRPDFFilter;
    QRHTMLFilter2: TQRHTMLFilter;
    qrEnderecoBeneficiario2: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure Emitir(pBoletos: TArray<RecDadosBoletoBancario>);

implementation

{$R *.dfm}

procedure Emitir(pBoletos: TArray<RecDadosBoletoBancario>);
var
  i: Integer;
  vImagem: TJPEGImage;
  vForm: TFormBoletoBradesco;
begin
  if pBoletos = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
	end;

  vForm := TFormBoletoBradesco.Create(Application);

  if Sessao.getParametrosEmpresa.LogoEmpresa <> nil then begin
    try
      Sessao.getParametrosEmpresa.LogoEmpresa.Position := 0;
      vImagem := TJPEGImage.Create;
      vImagem.LoadFromStream(Sessao.getParametrosEmpresa.LogoEmpresa);
      vForm.qrLogo.Picture.Graphic := vImagem;
    except

    end;
  end;

  try
    for i := Low(pBoletos) to High(pBoletos) do begin
      vForm.qrLogo.Enabled := False;
      vForm.qrTexto.Left := 12;
      vForm.qrTexto.Width := 693;
      vForm.qrTexto.Lines.Add(pBoletos[i].TextoImpressao);

      vForm.qrlNumero_Nota3.Caption := pBoletos[i].NumeroNota;
      vForm.qrlDocumento3.Caption := pBoletos[i].documento;
//      vForm.qrlPedido3.Caption := pBoletos[i].nr_ped_acu;
      vForm.qrlNomeFantasiaEmpresa.Caption    := pBoletos[i].NomeResumidoEmpresa;
      vForm.qrlVencimentoBra.Caption          := DFormat(pBoletos[i].DataVencimento);
      vForm.qrlCNPJBeneficiarioBra.Caption    := 'CNPJ/CPF: ' + pBoletos[i].CnpjBeneficiario;
      vForm.qrlAgenciaBeneficiarioBra.Caption := pBoletos[i].agencia + '-' + pBoletos[i].DigitoAgencia + ' / ' + pBoletos[i].ContaCorrente + '-' + pBoletos[i].DigitoContaCorrente;
      vForm.qrlDataEmissaoBra.Caption         := DFormat(pBoletos[i].DataEmissao);
      vForm.qrlNumeroDocumentoBra.Caption     := NFormat(pBoletos[i].ReceberId);
      vForm.qrlDataProcessamentoBra.Caption   := DFormat(pBoletos[i].DataEmissao);

      vForm.qrlNossoNumeroBra.Caption :=
        pBoletos[i].Carteira + ' / ' +
        pBoletos[i].NossoNumero + '-' +
        pBoletos[i].DigitoVerificadorNossoNumero;

      vForm.qrlValorDocumentoBra.Caption := NFormat(pBoletos[i].ValorDocumento);
      vForm.qrlLogradouroBra.Caption     := pBoletos[i].Logradouro;
      vForm.qrlCepBra.Caption            := pBoletos[i].Cep;
      vForm.qrlCidadeBra.Caption         := pBoletos[i].Cidade + '/' + pBoletos[i].Estado;
      vForm.qrlcpfcnpjBra.Caption        := pBoletos[i].CpfCnpj;
      vForm.qrlInstrucaoBra.Caption      := pBoletos[i].Instrucao;
      vForm.qrlNumero_Nota.Caption       := pBoletos[i].NumeroNota;
      vForm.qrlDocumento.Caption         := pBoletos[i].Documento;
//      vForm.qrlPedido.Caption            := pBoletos[i].nr_ped_acu;
      vForm.qrlPagadorBra.Caption        := pBoletos[i].Pagador;

      vForm.qrlObservacao.Caption := '';
//      if pBoletos[i].imprimir_observacao_boleto = 'S' then
//        qrObservacao.Caption := pBoletos[i].observacao;


      vForm.qrlUsoBanco.Caption := '';
      vForm.qrlCip.Caption := '000';
      vForm.qrlBeneficiarioBra.Caption := pBoletos[i].Beneficiario;

      vForm.qrlSacadorAvalista.Caption := '';
      vForm.qrlCpfCnpjSacAva.Caption := '';
      vForm.qrlLbCpfCpfDaycoval.Caption := '';

      vForm.qrlCarteiraBra.Caption := pBoletos[i].Carteira;

      vForm.qrlLinhaDigitavelBra.Caption      := pBoletos[i].LinhaDigitavel;
      vForm.qrCodigoBarraBra.BarText          := pBoletos[i].CodigoBarras;

      vForm.qrlVencimentoBra2.Caption         := DFormat(pBoletos[i].DataVencimento);
      vForm.qrlBeneficiarioBra2.Caption       := pBoletos[i].Beneficiario;
      vForm.qrlCNPJBeneficiarioBra2.Caption   := 'CNPJ/CPF: ' + pBoletos[i].CnpjBeneficiario;
      vForm.qrlEnderecoBeneficiario.Caption   := pBoletos[i].EnderecoBeneficiario;
      vForm.qrEnderecoBeneficiario2.Caption   := pBoletos[i].EnderecoBeneficiario;
      vForm.qrlAgenciaBeneficiarioBra2.Caption := pBoletos[i].Agencia + '-' + pBoletos[i].DigitoAgencia + ' / ' + pBoletos[i].ContaCorrente + '-' + pBoletos[i].DigitoContaCorrente;
      vForm.qrlDataEmissaoBra2.Caption        := DFormat(pBoletos[i].DataEmissao);
      vForm.qrlNumeroDocumentoBra2.Caption    := NFormat(pBoletos[i].ReceberId, 0);
      vForm.qrlDataProcessamentoBra2.Caption  := DFormat(pBoletos[i].DataEmissao);

      vForm.qrlNossoNumeroBra2.Caption :=
        pBoletos[i].Carteira + ' / ' +
        pBoletos[i].NossoNumero + '-' +
        pBoletos[i].DigitoVerificadorNossoNumero;

      vForm.qrlValorDocumentoBra2.Caption := NFormat(pBoletos[i].ValorDocumento);
      vForm.qrlLogradouroBra2.Caption     := pBoletos[i].Logradouro;
      vForm.qrlCepBra2.Caption            := pBoletos[i].Cep;
      vForm.qrlCidadeBra2.Caption         := pBoletos[i].Cidade + '/' + pBoletos[i].Estado;
      vForm.qrlcpfcnpjBra2.Caption        := pBoletos[i].CpfCnpj;
      vForm.qrlInstrucaoBra2.Caption      := pBoletos[i].Instrucao;
      vForm.qrlNumero_Nota2.Caption       := pBoletos[i].NumeroNota;
//      vForm.qrlPedido2.Caption            := pBoletos[i].nr_ped_acu;
      vForm.qrlDocumento2.Caption         := pBoletos[i].documento;
      vForm.qrlPagadorBra2.Caption        := pBoletos[i].cliente;

      vForm.qrlObservacao2.Caption := '';
//      if pBoletos[i].imprimir_observacao_boleto = 'S' then
//        vForm.qrlObservacao2.Caption := pBoletos[i].observacao;

      vForm.qrlUsoBanco2.Caption := '';
      vForm.qrlCip2.Caption := '000';
      vForm.qrlBeneficiarioBra2.Caption := pBoletos[i].Beneficiario;

      vForm.qrlSacadorAvalista2.Caption := '';
      vForm.qrlCpfCnpjSacAva2.Caption := '';
      vForm.qrlLbCpfCpfDaycoval2.Caption := '';

      vForm.qrlCarteiraBra2.Caption := pBoletos[i].Carteira;

       vForm.qrlVencimentoBra3.Caption := DFormat(pBoletos[i].DataVencimento);
      vForm.qrlBeneficiarioBra3.Caption :=  pBoletos[i].Beneficiario;

      vForm.qrlAgenciaBeneficiarioBra3.Caption := pBoletos[i].agencia + '-' + pBoletos[i].DigitoAgencia + ' / ' + pBoletos[i].ContaCorrente + '-' + pBoletos[i].DigitoContaCorrente;
      vForm.qrlNumeroDocumentoBra3.Caption := NFormat(pBoletos[i].ReceberId);

      vForm.qrlNossoNumeroBra3.Caption :=
        pBoletos[i].Carteira + ' / ' +
        pBoletos[i].NossoNumero + '-' +
        pBoletos[i].DigitoVerificadorNossoNumero;

      vForm.qrlValorDocumentoBra3.Caption := NFormat(pBoletos[i].ValorDocumento);
      vForm.qrlPagadorBra3.Caption := pBoletos[i].cliente;
      vForm.qrlDataEmissaoBra3.Caption := DFormat(pBoletos[i].DataEmissao);

//      if Trim(arquivo) <> '' then begin
//        vForm.qrBoletaBradesco.Prepare;
//        vForm.qrBoletaBradesco.ExportToFilter(TQRPDFDocumentFilter.Create(arquivo + IntToStr(pBoletos[i].financeiro_id) + '.pdf'));
//      end;

      vForm.qrBoletaBradesco.Preview;

//      if (Trim(arquivo) = '') and (not impressao_preview) then begin
//        qrBoletaBradesco.PrinterSettings.PrinterIndex := Printer.Printers.IndexOf(Ambiente.BuscarImpressora(imBOLETA));
//        qrBoletaBradesco.Print;
//      end;
    end;
  finally
    vForm.Free;
  end;
end;

end.
