unit _CobrancaBradesco;

interface

uses
  _CobrancaBancaria, _Biblioteca, System.SysUtils, System.Classes;

type
  TCobrancaBradesco = class(TInterfacedObject, ICobrancaBancaria)
    function GerarArquivoRemessa(
      pNomeArquivo: string;
      pCodigoEmpresa: string;
      pCNPJEmpresa: string;
      pNomeEmpresa: string;
      pNumeroRemessa: Integer;
      pDataArquivo: TDateTime;
      pHoraArquivo: string;
      pCarteira: string;
      pCEPEmpresa: string;
      pRegistros: TArray<RecBoletoGerarRemessa>;
      pPercentualMulta: Double
    ): Boolean;

    function DigitoVerifNossoNumero(
      pCarteira: string;
      pNossoNumero: string;
      pChave: string;
      pTipoDigito: string
    ): string;

    function getRetorno(
      pCodigoEmpresa: string;
      pArquivo: string
    ): TArray<RecBoletoRetornoRemessa>;
  end;

implementation

{ TCobrancaBradesco }

function TCobrancaBradesco.DigitoVerifNossoNumero(
  pCarteira: string;
  pNossoNumero: string;
  pChave: string;
  pTipoDigito: string
): string;

  function DigitoBradesco: String;
  var
    i: integer;
    vMultiplicador: integer;
    vSoma: integer;
    vDigito: integer;
    vCodigo: string;
  begin
    vMultiplicador := 7;
    vSoma := 0;
    vCodigo := LPad(pCarteira, 2, '0') + LPad(pNossoNumero, 11, '0');

    for i := Length(vCodigo) downto 1 do begin
      Inc(vMultiplicador);
      if vMultiplicador = 8 then
        vMultiplicador := 2;

      vSoma := vSoma + ( SFormatInt(vCodigo[i]) * vMultiplicador );
    end;

    vDigito := 11 - (vSoma mod 11);
    if vDigito = 11 then
      Result := '0'
    else if vDigito = 10 then
      Result := 'P'
    else
      Result := IntToStr(vDigito);
  end;

begin
  Result := DigitoBradesco;
end;

function TCobrancaBradesco.GerarArquivoRemessa(
  pNomeArquivo: string;
  pCodigoEmpresa: string;
  pCNPJEmpresa: string;
  pNomeEmpresa: string;
  pNumeroRemessa: Integer;
  pDataArquivo: TDateTime;
  pHoraArquivo: string;
  pCarteira: string;
  pCEPEmpresa: string;
  pRegistros: TArray<RecBoletoGerarRemessa>;
  pPercentualMulta: Double
): Boolean;
var
  vArquivo: TStrings;
  vTexto: string;
  i: Integer;
  vIdRegistro: string;
  vQtdeRegistros: Integer;
begin
  Result := False;

  if pRegistros = nil then begin
    _Biblioteca.Exclamar('Nenhum registro foi informado!');
    Exit;
  end;

  vArquivo := TStringList.Create;

  vQtdeRegistros := 1;

  // Registro header
  vTexto :=
    // Identifica��o do registro
    '0' +
    // Identifica��o do registro de remessa
    '1' +
    // Literal remessa
    'REMESSA' +
    // C�digo do servi�o
    '01' +
    // Literal servi�o
    RPad('COBRANCA', 15) +
    // C�digo da empresa
    LPad(pCodigoEmpresa, 20, '0') +
    // Nome da empresa
    RPad(pNomeEmpresa, 30) +
    // N�mero do bradesco na c�mara de compensa��o
    '237' +
    // Nome do banco por extenso
    RPad('BRADESCO', 15) +
    // Data da grava��o do arquivo
    FormatDateTime('ddmmyy', pDataArquivo) +
    // Branco
    Espacos(8) +
    // Identifica��o do sistema
    'MX' +
    // N�mero sequencial do arquivo
    FormatFloat('0000000', pNumeroRemessa) +
    // Branco
    Espacos(277) +
    // N�mero sequencial do registro de um em um
    FormatFloat('000000', vQtdeRegistros);

  Inc(vQtdeRegistros);

  vArquivo.Add( UpperCase(RetirarAcentos(vTexto)) );

  for i := low(pRegistros) to high(pRegistros) do begin
    vIdRegistro := RetornaNumeros(pRegistros[i].CpfCnpj);

    vTexto :=
      // Identifica��o do registro
      '1' +
      // Ag�ncia de d�bito
      '00000' +
      // D�gito da ag�ncia de d�bito
      '0' +
      // Raz�o da conta corrente
      '00000' +
      // Conta corrente
      '0000000' +
      // D�gito da conta corrente
      '0' +
      // Identifica��o da empresa benefici�rio no banco
      '0' +
      LPad(RetornaNumeros(pRegistros[i].Carteira), 3, '0') +
      LPad(RetornaNumeros(pRegistros[i].Agencia), 5, '0') +
      LPad(RetornaNumeros(pRegistros[i].Conta), 7, '0') +
      LPad(RetornaNumeros(pRegistros[i].DigitoConta), 1, '0') +
      // N�mero de controle do participante
      RPad(IntToStr(pRegistros[i].ReceberId), 25) +
      // C�digo do banco a ser debitado na c�mara de compensa��o
      '000' +
      // Campo de Multa
      IIfStr(pPercentualMulta > 0 , '2', '0') +
      // Percentual de multa
      LPad(RetornaNumeros(NFormat(pPercentualMulta)), 4, '0') +
      // Identifica��o do t�tulo no banco (nosso n�mero)
      LPad(pRegistros[i].NossoNumero, 11, '0') +
      // D�gito verificador do nosso n�mero
      DigitoVerifNossoNumero(pRegistros[i].carteira, pRegistros[i].NossoNumero, '', '') +
      // Desconto bonifica��o por dia
      '0000000000' +
      // Condi��o de emiss�o da papeleta de cobran�a
      IIfStr(pRegistros[i].NossoNumero = '', '1', '2') +
      // Identifica��o se emite papeleta para d�bito autom�tico
      Espacos(1) +
      // Identifica��o da opera��o do banco
      Espacos(10) +
      // Identifica rateio do cr�dito
      Espacos(1) +
      // Endere�amento para aviso de d�bito automatico de conta corrente
      Espacos(1) +
      // Brancos
      Espacos(2) +
      // Identifica��o ocorr�ncia
      '01' + //iif(pRegistros[i].baixa_id = 0,'01','02') +
      // N�mero do documento
      RPad(IntToStr(pRegistros[i].ReceberId), 10) +
      // Data de vencimento
      FormatDateTime('ddmmyy', pRegistros[i].DataVencimento) +
      // Valor do documento
      LPad(RetornaNumeros(NFormat(pRegistros[i].ValorDocumento)), 13, '0') +
      // Banco encarregado da cobran�a
      '000' +
      // Ag�ncia deposit�ria
      '00000' +
      // Esp�cie do t�tulo
      '01' +
      // Identifica��o
        'N' +
      // Data de emiss�o do t�tulo
      FormatDateTime('ddmmyy', pRegistros[i].DataEmissao) +
      // Primeira instru��o
      '00' + //IIfStr(pRegistros[i].dias_protesto > 0, '06', '00') +
      // Segunda instru��o
      '00' + //IIfStr(pRegistros[i].dias_protesto > 0, FormatFloat('00', pRegistros[i].dias_protesto), '00') +
      // Valor a ser cobrado por dia de atraso
      LPad(RetornaNumeros(NFormat(pRegistros[i].ValorDiaAtraso)), 13, '0') +
      // Data limite para concess�o de desconto
      '000000' +
      // Valor do desconto
      '0000000000000' +
      // Valor do IOF
      '0000000000000' +
      // Valor do abatimento a ser concedido ou cancelado
      '0000000000000' +
      // Identifica��o do tipo de inscri��o do sacado
      IIfStr(length(vIdRegistro) = 14, '02', '01') +
      // N�mero de inscri��o do sacado
      LPad(vIdRegistro, 14, '0') +
      // Nome do sacado
      RPad(pRegistros[i].RazaoSocial, 40) +
      // Endere�o sacado
      RPad(pRegistros[i].logradouro + ' ' + pRegistros[i].complemento + ' ' + pRegistros[i].NomeBairro + ' - ' + pRegistros[i].NomeCidade + '/' + pRegistros[i].EstadoId , 40) +
      // Primeira mensagem
      Espacos(12) +
      // CEP
      LPad(RetornaNumeros(pRegistros[i].cep), 8, '0') +
      // Sacador/avalista ou segunda mensagem
      Espacos(60) +
      // N�mero sequencial do registro
      FormatFloat('000000', vQtdeRegistros);

    Inc(vQtdeRegistros);
    vArquivo.Add( UpperCase(RetirarAcentos(vTexto)) );
  end;

  // Registro trailler
  vTexto :=
    // Identifica��o do registro
    '9' +
    // Branco
    Espacos(393) +
    // N�mero sequencial do registro
    FormatFloat('000000', vQtdeRegistros);

  vArquivo.Add( UpperCase(RetirarAcentos(vTexto)) );

  // Gerando o arquivo vTexto
  try
    vArquivo.SaveToFile(pNomeArquivo);
  except
    Informar('Erro ao gerar o arquivo!');
    Exit;
  end;

  // Limpando a mem�ria
  vArquivo.Clear;
  vArquivo.Free;

  Result := True;
end;

function TCobrancaBradesco.getRetorno(
  pCodigoEmpresa: string;
  pArquivo: string
): TArray<RecBoletoRetornoRemessa>;
var
  i: integer;
  s: string;
  vArquivo: TStrings;
  vPosic: Integer;
  vDataCredito: TDateTime;

  procedure LimparArquivo(pMensagem: string);
  begin
    if pMensagem <> '' then
      _Biblioteca.Exclamar(pMensagem);

    vArquivo.Clear;
    vArquivo.Free;
  end;

  function BuscarMotivoAnalitico(id: Integer): string;
  begin
    case id of
      2: Result := 'Entrada confirmada';
      3: Result := 'Entrada rejeitada';
      6: Result := 'Liquida��o normal';
      9: Result := 'Baixado automaticamente via arquivo';
      10: Result := 'Baixado conforme instru��es da ag�ncia';
      11: Result := 'Em ser - arquivo de t�tulos pendentes';
      12: Result := 'Abatimento concedido';
      13: Result := 'Abatimento cancelado';
      14: Result := 'Vencimento alterado';
      15: Result := 'Liquida��o em cart�rio';
      16: Result := 'T�tulo pago em cheque';
      17: Result := 'Liquida��o ap�s baixa ou t�tulo n�o registrado';
      18: Result := 'Acerto de deposit�ria';
      19: Result := 'Confirma��o receb. inst. de protesto';
      20: Result := 'Confirma��o recebimento instru��o de susta��o de protesto';
      21: Result := 'Acerto do controle do participante';
      22: Result := 'T�tulo com pagamento cancelado';
      23: Result := 'Entrada do t�tulo em cart�rio';
      24: Result := 'Entrada rejeitada por CEP irregular';
      27: Result := 'Baixa rejeitada';
      28: Result := 'D�bito de tarifas/custas';
      30: Result := 'Altera��o de outros dados rejeitados';
      32: Result := 'Instru��o rejeitada';
      33: Result := 'Confirma��o pedido altera��o outros dados';
      34: Result := 'Retirado de cart�rio e manuten��o carteira';
      35: Result := 'Desagendamento do d�bito autom�tico';
      68: Result := 'Acerto dos dados do rateio de cr�dito';
      69: Result := 'Cancelamento dos dados do rateio';
    else
      Result := 'N�o identificado';
    end;
  end;

begin
  Result := nil;
  vDataCredito := 0;

  vArquivo := TStringList.Create;

  // Tentando carregar o arquivo
  try
    vArquivo.LoadFromFile(pArquivo);
  except
    LimparArquivo('O arquivo "' + pArquivo + '" n�o pode ser carregado!');
    Exit;
  end;

  // Verificando se o arquivo tem pelo menos 1 linha
  if vArquivo.Count < 3 then begin
    LimparArquivo('O arquivo "' + pArquivo + '" n�o est� no formato correto!');
    Exit;
  end;

  // Fazendoa  verifica��o da quantidade de posi��es nas linhas
  for i := 0 to vArquivo.Count - 1 do begin
    if Length(vArquivo[i]) <> 400 then begin
      LimparArquivo(
        'O arquivo "' + pArquivo + '" n�o est� no formato correto! A linha ' +
        NFormat(i + 1) + ' deveria ter 400 posi��es mas est� com ' +
        NFormat(Length(vArquivo[i])) + ' posi��es.'
      );
      Exit;
    end;
  end;

  if SFormatInt(Copy(vArquivo[0], 27, 20)) = 0 then begin
    LimparArquivo('O c�digo da empresa est� inv�lido no arquivo!');
    Exit;
  end;

  if SFormatInt(Copy(vArquivo[0], 27, 20)) <> SFormatInt(pCodigoEmpresa) then begin
    LimparArquivo('O c�digo da empresa do arquivo n�o � o mesmo informado no sistema!');
    Exit;
  end;

  try
    // Registros
    vPosic := 0;
    for i := 0 to vArquivo.Count - 2 do begin
      s := vArquivo[i];

      // Buscando data de cr�dito no header
      if Copy(s, 1, 1) = '0' then
        vDataCredito := StrToDate( Copy(s, 380, 2) + '/' + Copy(s, 382, 2) + '/' + Copy(s, 384, 2) );

      // Pegando somente registro de transa��o
      if Copy(s, 1, 1) <> '1' then
        Continue;

      SetLength(Result, vPosic + 1);

      Result[vPosic].NossoNumero := copy(s, 71, 11);

      try
        Result[vPosic].DataVencimento := StrToDate( Copy(s, 147, 2) + '/' + Copy(s, 149, 2) + '/' + Copy(s, 151, 2) );
      except
        Result[vPosic].DataVencimento := 0;
      end;

      Result[vPosic].ValorDocumento         := SFormatDouble( Copy(s, 153, 13) ) * 0.01;
      Result[vPosic].Retorno.TipoId         := SFormatInt(copy(s, 109, 2));
      Result[vPosic].Retorno.DescricaoTipo  := BuscarMotivoAnalitico(Result[vPosic].Retorno.TipoId);
      Result[vPosic].Retorno.Baixar         := Result[vPosic].Retorno.TipoId in [6, 15, 17];
      Result[vPosic].Retorno.DataOcorrencia := StrToDate( Copy(s, 111, 2) + '/' + Copy(s, 113, 2) + '/' + Copy(s, 115, 2) );
      Result[vPosic].Retorno.BancoCobrador  := SFormatInt(Copy(s, 166, 3));
      Result[vPosic].Retorno.ValorPago      := SFormatDouble( Copy(s, 254, 13) ) * 0.01;
      Result[vPosic].Retorno.Juros          := SFormatDouble(Copy(s, 202, 13)) * 0.01;
      Result[vPosic].Retorno.Multa          := SFormatDouble(Copy(s, 267, 13)) * 0.01;
      Result[vPosic].Retorno.Desconto       := SFormatDouble( Copy(s, 241, 13) ) * 0.01;
      Result[vPosic].Retorno.AgenciaArquivo := Copy(s, 26, 4);
      Result[vPosic].Retorno.ContaArquivo   := Copy(s, 30, 7);

      if Result[vPosic].Retorno.TipoId in [8, 28] then
        Result[vPosic].Retorno.ValorTarifa := SFormatDouble( Copy(s, 189, 13) ) * 0.01
      else
        Result[vPosic].Retorno.ValorTarifa := SFormatDouble( Copy(s, 176, 13) ) * 0.01;

      try
        if Result[vPosic].Retorno.TipoId in [2, 28] then
          Result[vPosic].Retorno.DataCredito := vDataCredito
        else
          Result[vPosic].Retorno.DataCredito := StrToDate( Copy(s, 296, 2) + '/' + Copy(s, 298, 2) + '/' + Copy(s, 300, 2) );
      except
        Result[vPosic].Retorno.DataCredito := 0;
      end;

      Inc(vPosic);
    end;
	except
    on e: Exception do begin
      Result := nil;
      Exclamar('Erro ao ler o arquivo! ' + e.Message);
    end;
  end;

  LimparArquivo('');
end;

end.
