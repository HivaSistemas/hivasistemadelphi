unit _CobrancaSicoob;

interface

uses
  _CobrancaBancaria, Classes, _Biblioteca, SysUtils, Windows;

type
  TCobrancaSicoob = class(TInterfacedObject, ICobrancaBancaria)
    function GerarArquivoRemessa(
      pNomeArquivo: string;
      pCodigoEmpresa: string;
      pCNPJEmpresa: string;
      pNomeEmpresa: string;
      pNumeroRemessa: Integer;
      pDataArquivo: TDateTime;
      pHoraArquivo: string;
      pCarteira: string;
      pCEPEmpresa: string;
      pRegistros: TArray<RecBoletoGerarRemessa>;
      pPercentualMulta: Double
    ): Boolean;

    function DigitoVerifNossoNumero(
      pCarteira: string;
      pNossoNumero: string;
      pChave: string;
      pTipoDigito: string
    ): string;

    function getRetorno(
      pCodigoEmpresa: string;
      pArquivo: string
    ): TArray<RecBoletoRetornoRemessa>;
  end;

implementation

{ TCobrancaSicoob }

function TCobrancaSicoob.DigitoVerifNossoNumero(
  pCarteira: string;
  pNossoNumero: string;
  pChave: string;
  pTipoDigito: string
): string;
//var
//  i: integer;
//  multiplicador: integer;
//  soma: integer;
//  digito: integer;
//  codigo: string;
begin

//  soma := 0;
//  multiplicador := 3;
//  codigo := agencia +conta+nosso_numero;
//  for i := Length(codigo) downto 1 do begin
//    soma := soma + ( ValorInt(codigo[i]) * multiplicador );
//    case  multiplicador of
//      1 : multiplicador := 3;
//      3 : multiplicador := 7;
//      7 : multiplicador := 9;
//      9 : multiplicador := 1;
//    end;
//  end;
//
//
//  digito := 11 - (soma mod 11);
//  if digito > 9 then
//    Result := '0'
//  else
//    Result := IntToStr(digito);
end;

function TCobrancaSicoob.GerarArquivoRemessa(
  pNomeArquivo: string;
  pCodigoEmpresa: string;
  pCNPJEmpresa: string;
  pNomeEmpresa: string;
  pNumeroRemessa: Integer;
  pDataArquivo: TDateTime;
  pHoraArquivo: string;
  pCarteira: string;
  pCEPEmpresa: string;
  pRegistros: TArray<RecBoletoGerarRemessa>;
  pPercentualMulta: Double
): Boolean;
//var
//  arq: TStrings;
//  texto: string;
//  num_reg: integer;
//  id_reg: string;
//
//  //ARQUIVO REMESSA CRIADO PARA QUE USA COBRAN�A COM REGISTRO 400 posi��es
//  function GerarArquivoRemessaCNAB400: Boolean;
//  var
//    i:Integer;
//  begin
//    num_reg := 1;
//    // Registro header
//    texto :=
//      //Identifica��o do registro header
//        '0' +
//      //Identifica��o do arquivo remessa
//        '1' +
//      //Literal remessa
//        'REMESSA' +
//      // C�digo do servi�o de cobran�a
//        '01' +
//      //Literal cobran�a
//        'COBRAN�A' +
//      //Brancos
//        Espacos(7) +
//      //C�digo do cooperativa
//        Padl(agencia + digito_agencia, 5, '0') +
//      //C�digo do beneficiario
//        Padl(codigo_empresa, 9, '0') +
//      //Brancos
//        '000000' +
//      //nome do beneficiario
//        Padr(nome_empresa, 30, ' ') +
//      //N�mero do banco e Literal BANCOOBCED
//        Padr('756BANCOOBCED', 18, ' ') +
//      //Data de grava��o do arquivo
//        FormatDateTime('ddmmyy', data_arquivo) +
//      //N�mero daremessa
//        FormatFloat('0000000', nr_remessa) +
//      //Filler
//        Espacos(287) +
//      //N�mero sequencial do registro
//        FormatFloat('000000', num_reg);
//
//    arq.Add( UpperCase(Self.RetirarAcentos(texto)) );
//
//    for i := low(registros) to high(registros) do begin
//      inc(num_reg);
//
//      // Registro detalhe
//      texto :=
//        //Identifica��o do registro detalhe
//          '1' +
//        //Tipo de Inscri��o do beneficiario
//          '02' +
//        //CGC do beneficiario
//          Padl(Numeros(cnpj_empresa), 14, '0') +
//        //C�digo do cooperativa
//          Padl(agencia + digito_agencia, 5, '0') +
//        //Conta corrente + d�gito
//          Padl(conta + digito_conta, 9, '0') +
//        //N�mero do Conv�nio de Cobran�a do beneficiario
//          '000000' +
//        //Filler
//          Espacos(25) +
//        //Nosso n�mero
//          Padl(registros[i].nosso_numero, 11, '0') + DigitoVerifNossoNumero(agencia, Padl(codigo_empresa ,10,'0'), '', Padl(registros[i].nosso_numero, 7, '0'), '', '') +
//        //N�mero da parcela
//          padl(registros[i].parcela,2,'0') +
//        // Grupo do valor
//          '00' +
//        //Complemento do registro
//          Espacos(3) +
//        //Indicativo de Mensagem ou Benefici�rio/Avalista
//          ' ' +
//        //Prefixo do t�tulo
//          Espacos(3) +
//        // Varia��o da carteira
//          '000' +
//        // Conta cau��o
//          '0' +
//        //N�mero do Contrato Garantia + Dv
//          '000000' +
//        //Numero do border�
//          '000000' +
//        //Complemento do registro
//          Espacos(4) +
//        //tipo emiss�o
//          '2'+
//        //C�digo da carteira
//          IIfStr(tipo_cobranca = 'S','02','01')+
//        //C�digo da ocorr�ncia:
//          iif(registros[i].baixa_id = 0,'01','02') +
//        //Seu n�mero
//          Padr(registros[i].numero_documento, 10, ' ') +
//        //Data vencimento
//          FormatDateTime('ddmmyy', registros[i].data_vencimento) +
//        //Valor t�tulo
//           Padl(Numeros(NPadrao(registros[i].valor_documento)), 13, '0') +
//        //N�mero banco
//          '756' +
//        //C�digo do cooperativa
//          Padl(agencia + digito_agencia, 5, '0') +
//        //Esp�cie do T�tulo
//          '18' +
//        //aceite do titulo
//          IIfStr(aceite = 'N', '0','1') +
//        //data emiss�o
//          FormatDateTime('ddmmyy', registros[i].data_emissao) +
//        //instru��o de protesto autom�tico
//          '00' +
//        //n�mero de dias para protesto autom�tico
//          '00' +
//        //Taxa de mora m�s
//          Padl(Numeros(NPadrao(registros[i].percentual_juros,4)), 6, '0') +
//        //Taxa de multa
//          Padl(Numeros(NPadrao(registros[i].percentual_multa,4)), 6, '0') +
//        //Tipo distribui��o
//          '2' +
//        //Data primeiro desconto
//          '000000' +
//        //valor primeiro desconto
//          Padl('0', 13, '0') +
//        //C�digo / valor moeda
//          '9000000000000' +
//        //Valor do abatimento
//          Padl('0', 13, '0') +
//        //Tipo de pessoa do pagador PF ou PJ
//          IIfStr(length(Numeros(registros[i].cpf_cnpj)) = 14, '02', '01') +
//        //CIC/CGC do pagador
//          Padl(Numeros(registros[i].cpf_cnpj), 14, '0') +
//        //Nome do pagador
//          Padr(registros[i].nome_razao_social, 40) +
//        //endere�o do pagador
//          Padr(registros[i].logradouro + ' ' + registros[i].complemento, 37) +
//        //bairro do pagador
//          Padr(registros[i].bairro, 15) +
//        //CEP do pagador
//          Padl(Numeros(registros[i].cep), 8, '0') +
//        //cidade do pagador
//          Padr(registros[i].cidade, 15) +
//        //UF do pagador
//          Padr(registros[i].estado, 2) +
//        //Observa��es
//          Espacos(40) +
//        //n�mero de dias para protesto autom�tico
//          Padl(NPadrao(registros[i].dias_protesto, 0), 2,'0') +
//        //Filler
//          Espacos(1) +
//        //N�mero sequencial do registro
//          FormatFloat('000000', num_reg);
//
//      arq.Add( UpperCase(Self.RetirarAcentos(texto)) );
//    end;
//
//    inc(num_reg);
//    // Registro trailler DE ARQUIVO
//    texto :=
//      //Identifica��o do registro trailer
//        '9' +
//      //Filler
//        Espacos(393) +
//      //N�mero sequencial do registro
//        FormatFloat('000000', num_reg);
//
//
//    arq.Add( UpperCase(Self.RetirarAcentos(texto)) );
//
//    Result := true;
//  end;
//
//  //ARQUIVO REMESSA CRIADO PARA QUE USA COBRAN�A COM REGISTRO 240posi��es
//  function GerarArquivoRemessaCNAB240: Boolean;
//  var
//    i:Integer;
//  begin
//    // Registro header
//    texto :=
//      //C�digo do Banco na compensa��o
//      '756' +
//      //Lote de servi�o N 004 0000 1
//      '0000' +
//      //Tipo de registro N 001 0 2
//      '0' +
//      //Reservado (uso Banco)
//      Espacos(9) +
//      //Tipo de inscri��o da empresa
//      '2' +
//      //N� de inscri��o da empresa
//      Padl(Numeros(cnpj_empresa), 14, '0') +
//      //C�digo do conv�nio
//      Espacos(20) +
//      // ag�ncia
//      Padl(agencia, 5, '0') +
//      // DV agencia
//      Padl(digito_agencia, 1, '0') +
//      //Conta corrente
//      Padl(conta, 12, '0') +
//      // DV conta
//      Padl(digito_conta, 1, '0') +
//      // Digito verificador da ag/conta
//      Espacos(1) +
//      //Nome da empresa
//      Padr(nome_empresa, 30) +
//      //Nome do Banco
//      Padr('SICOOB', 30) +
//      //Reservado (uso Banco)
//      Espacos(10) +
//      //C�digo remessa
//      '1' +
//      //Data de gera��o do arquivo
//      FormatDateTime('ddmmyyyy', data_arquivo) +
//      //Hora da gera��o
//      hora_arquivo +
//      //N� seq�encial do arquivo
//      FormatFloat('000000', nr_remessa) +
//      //N� da vers�o do layout do arquivo
//      '087' +
//      // Densidade
//      '00000' +
//      // Reservado (uso Banco)
//      Espacos(20) +
//      // Reservado para a empresa
//      Espacos(20) +
//      // Uso exclusivo FEBRABAN/ CNAB
//      Espacos(29);
//
//    arq.Add(UpperCase(RetirarAcentos(texto)) );
//
//    // REGISTRO HEADER DO LOTE REMESSA
//    texto :=
//      //C�digo do Banco na compensa��o
//      '756' +
//      //Numero do lote remessa
//      '0001' +
//      //Tipo de registro
//      '1' +
//      //Tipo de opera��o
//      'R' +
//      //Tipo de servi�o
//      '01' +
//      //Reservado (uso Banco)
//      Espacos(2) +
//      //N� da vers�o do layout do lote
//      '045' +
//      //Reservado (uso Banco)
//      Espacos(1) +
//      //Tipo de inscri��o da empresa 1 = CPF, 2 = CNPJ
//      '2' +
//      //N� de inscri��o da empresa
//      Padl(Numeros(cnpj_empresa), 15, '0') +
//      //Reservado (uso Banco)
//      Espacos(20) +
//      // ag�ncia
//      Padl(agencia, 5, '0') +
//      // DV agencia
//      Padl(digito_agencia, 1, '0') +
//      //Conta corrente
//      Padl(conta, 12, '0') +
//      // DV conta
//      Padl(digito_conta, 1, '0') +
//      // Digito verificador da ag/conta
//      Espacos(1) +
//      //Nome do beneficiario A 030
//      Padr(nome_empresa, 30) +
//      //Mensagem 1
//      Espacos(40) +
//      //Mensagem 2
//      Espacos(40) +
//      //N�mero remessa/retorno N 008 10
//      FormatFloat('00000000', nr_remessa) +
//      //Data da grava��o remessa/retorno N 008 DDMMAAAA
//      FormatDateTime('ddmmyyyy', data_arquivo) +
//      // Data do cr�dito
//      Padl('0', 8, '0') +
//      //Reservado (uso Banco) A 041 Brancos
//      Espacos(33);
//    arq.Add( UpperCase(RetirarAcentos(texto)) );
//    num_reg := 1;
//
//    //Detalhe
//    for I := Low(registros) to High(registros) do begin
//      id_reg := Numeros(registros[i].cpf_cnpj);
//
//      //REGISTRO DETALHE - SEGMENTO P
//      texto :=
//        //C�digo do Banco na compensa��o
//        '756' +
//        //Numero do lote remessa
//        '0001' +
//        //Tipo de registro
//        '3' +
//        //N� seq�encial do registro de lote
//        FormatFloat('00000', num_reg) +
//        //C�d. Segmento do registro detalhe
//        'P' +
//        //Reservado (uso Banco)
//        Espacos(1) +
//        //C�digo de movimento remessa 16 - 17
//        IIfStr(registros[i].baixa_id = 0, '01', '02') +
//        //Ag�ncia do beneficiario
//        Padl(agencia, 5, '0') +
//        //D�gito da Ag�ncia do beneficiario
//        Padl(digito_agencia, 1, '0') +
//        //N�mero da conta corrente
//        Padl(conta, 12, '0') +
//        //D�gito verificador da conta
//        Padl(digito_conta, 1, '0') +
//        // Digito verificador da ag/conta
//        Espacos(1) +
//        //Nosso n�mero (10 posi��es)
//        Padl(Padl(registros[i].nosso_numero, 7, '0') + DigitoVerifNossoNumero(agencia, Padl(codigo_empresa ,10,'0'), '', Padl(registros[i].nosso_numero, 7, '0'), '', ''), 10, '0') +
//        //N�mero da parcela (2 posi��es)
//        padl(registros[i].parcela,2, '0') +
//        // Modalidade  com registro 01 sem registro 02 (2 posi��es)
//        IIfStr(tipo_cobranca = 'S','02','01')+
//        // Tipo Formulario (1 posi��es)
//        '4' +
//        // Em branco (5 posi��es)
//        Espacos(5) +
//        // Codigo da carteira
//        Padl(carteira, 1, '0') +
//        // Cadastramento
//        '0' +
//        // Documento
//        Espacos(1) +
//        // Emissao boleto
//        '2' +
//        // distribui��o boleto
//        '2' +
//        // Numero documento
//        Padr(IntToStr(registros[i].financeiro_id), 15) +
//        //Data de vencimento do t�tulo
//        FormatDateTime('ddmmyyyy', registros[i].data_vencimento) +
//        //Valor nominal do t�tulo
//        Numeros(FormatFloat('0000000000000.00', registros[i].valor_documento)) +
//        //Ag�ncia encarregada da cobran�a N 004 19
//        '00000' +
//        // Digito verificador da agencia
//        '0' +
//        //Esp�cie do t�tulo
//        '02' +
//        //Identif. de t�tulo Aceito/N�o Aceito
//        Padl(IIfStr(aceite = 'S', 'A','N') , 1, 'N') +
//        //Data da emiss�o do t�tulo
//        FormatDateTime('ddmmyyyy', registros[i].data_emissao) +
//        //C�digo do juros de mora
//        IIfStr(registros[i].valor_por_dia_atraso > 0, '1', '0') +
//        //Data do juros de mora
//        IIfStr(registros[i].valor_por_dia_atraso > 0, FormatDateTime('ddmmyyyy', registros[i].data_vencimento) , '00000000') +
//        //Valor da mora/dia ou Taxa mensal
//        Numeros(FormatFloat('0000000000000.00', registros[i].valor_por_dia_atraso)) +
//        //C�digo do desconto
//        '0' +
//        //Data de desconto
//        Padl('0', 8, '0') +
//        //Valor ou Percentual do desconto concedido
//        Padl('0', 15, '0') +
//        //Valor do IOF a ser recolhido
//        Padl('0', 15, '0') +
//        //Valor do abatimento
//        Padl('0', 15, '0') +
//        //Identifica��o do t�tulo na empresa
//        Padr(IntToStr(registros[i].financeiro_id), 25) +
//        //C�digo para protesto
//        '1' +
//        //N�mero de dias para protesto
//        FormatFloat('00', registros[i].dias_protesto) +
//        //C�digo para Baixa/Devolu��o
//        '0' +
//        //N�mero de dias para Baixa/Devolu��o
//        '000' +
//        //C�digo da moeda
//        '09' +
//        // Numero do contrato
//        Padl('0', 10, '0') +
//        // CNAb
//        Espacos(1);
//
//      Inc(num_reg);
//      arq.Add(UpperCase(RetirarAcentos(texto)) );
//
//      //REGISTRO DETALHE - SEGMENTO Q
//      texto :=
//        //C�digo do Banco na compensa��o
//        '756' +
//        //Numero do lote remessa
//        '0001' +
//        //Tipo de registro
//        '3' +
//        //N� seq�encial do registro no lote
//        FormatFloat('00000', num_reg) +
//        //C�d. segmento do registro detalhe
//        'Q' +
//        //Reservado (uso Banco)
//        Espacos(1) +
//        // Codigo de movimento remessa
//        '01' +
//        //Tipo de inscri��o do pagador
//        IIfStr(length(id_reg) = 14, '2', '1') +
//        //N�mero de inscri��o do pagador
//        Padl(id_reg, 15, '0') +
//        //Nome pagador
//        Padr(registros[i].nome_razao_social, 40) +
//        //Endere�o pagador
//        Padr(registros[i].logradouro + ' ' + registros[i].complemento, 40) +
//        //Bairro pagador
//        Padr(registros[i].bairro, 15) +
//        //Cep pagador
//        Padl(Numeros(IIfStr(registros[i].cep = '',cep_empresa, registros[i].cep)), 5, '0') +
//        //Sufixo do Cep do pagador
//        Padl(Numeros(Copy(IIfStr(registros[i].cep = '',cep_empresa, registros[i].cep),7,3)), 3, '0') +
//        //Cidade do pagador
//        Padr(registros[i].cidade, 15) +
//        //Unidade da federa��o do pagador
//        Padr(registros[i].estado, 2) +
//        //Tipo de inscri��o pagador/avalista
//        Padr('0',1) +
//        //N� de inscri��o pagador/avalista
//        Padr('0',15,'0') +
//        //Nome do pagador/avalista
//        Espacos(40) +
//        // C�digo banco correspondente na compensa��o
//        '000' +
//        //nosso numero no banco correspondente
//        Espacos(20) +
//        //Reservado (uso Banco)
//        Espacos(8);
//
//      Inc(num_reg);
//      arq.Add( UpperCase(RetirarAcentos(texto)) );
//
//      //REGISTRO DETALHE - SEGMENTO R (Opcional)
//      texto :=
//        //C�digo do Banco na compensa��o
//        '756' +
//        //Numero do lote remessa
//        '0001' +
//        //Tipo de registro
//        '3' +
//        //N� seq�encial do registro no lote
//        FormatFloat('00000', num_reg) +
//        //C�d. segmento do registro detalhe
//        'R' +
//        //Reservado (uso Banco)
//        Espacos(1) +
//        //C�digo de movimento remessa
//        '01' +
//        //C�digo do desconto 2
//        '0' +
//        //Data de desconto 2
//        Padl('0', 8, '0') +
//        //Valor ou Percentual do desconto 2
//        Padl('0', 15, '0') +
//        //C�digo do desconto 3
//        '0' +
//        //Data de desconto 3
//        Padl('0', 8, '0') +
//        //Valor ou Percentual do desconto 3
//        Padl('0', 15, '0') +
//        // C�digo da multa
//        IIfStr(((NPadrao(valor_multa_padrao_boleta) = NPadrao(0)) and (NPadrao(perc_multa_) = NPadrao(0))), '0', IIfStr(valor_multa_padrao_boleta > 0, '1', '2')) +
//        //Data da Multa
//        IIfStr((valor_multa_padrao_boleta > 0) or (perc_multa_ > 0), FormatDateTime('ddmmyyyy', registros[i].data_vencimento) ,  '00000000') +
//        //Valor da Multa
//        Numeros(FormatFloat('0000000000000.00', IIfDbl(valor_multa_padrao_boleta > 0, valor_multa_padrao_boleta, perc_multa_))) +
//        //Reservado (uso Banco)
//        Espacos(10) +
//        // Mensagem 3
//        Espacos(40) +
//        //Mensagem 4
//        Espacos(40) +
//        // Uso Exclusivo FEBRABAN/CNAB: Brancos
//        Espacos(20) +
//        //C�d. Ocor. do Pagador:
//        '00000000' +
//        //C�d. do Banco na Conta do D�bito:
//        '000' +
//        // C�digo da Ag�ncia do D�bito
//        '00000' +
//        // D�gito Verificador da Ag�ncia
//        Espacos(1) +
//        // Conta Corrente para D�bito:
//        '000000000000' +
//        //D�gito Verificador da Conta
//        Espacos(1) +
//        //D�gito Verificador Ag/Conta
//        Espacos(1) +
//        // Aviso para D�bito Autom�tico
//        '0' +
//        // Uso Exclusivo FEBRABAN/CNAB
//        Espacos(9);
//      Inc(num_reg);
//      arq.Add( UpperCase(RetirarAcentos(texto)) );
//
//      //REGISTRO DETALHE - SEGMENTO S (Opcional)
//      texto :=
//        //C�digo do Banco na compensa��o
//        '756' +
//        //Numero do lote remessa
//        '0001' +
//        //Tipo de registro
//        '3' +
//        //N� seq�encial do registro no lote
//        FormatFloat('00000', num_reg) +
//        //C�d. segmento do registro detalhe
//        'S' +
//        //Reservado (uso Banco)
//        Espacos(1) +
//        //C�digo de movimento remessa
//        '01' +
//        // tipo impressao
//        '3' +
//        // Informa��o 5
//        Espacos(40) +
//        // Informa��o 6
//        Espacos(40) +
//        // Informa��o 7
//        Espacos(40) +
//        // Informa��o 8
//        Espacos(40) +
//        // Informa��o 9
//        Espacos(40) +
//        // CNAB
//        Espacos(22);
//
//      Inc(num_reg);
//      arq.Add( UpperCase(RetirarAcentos(texto)) );
//    end;
//
//    // TRAILER DE LOTE REMESSA
//    texto :=
//      //C�digo do Banco na compensa��o
//      '756' +
//      //Numero do lote remessa
//      '0001' +
//      //Tipo de registro
//      '5' +
//      //Reservado (uso Banco)
//      Espacos(9) +
//      //Quantidade de registros do lote
//      FormatFloat('000000', num_reg + 1) +
//      // totaliza��o cobran�a simples
//      Padl('0',  23, '0') +
//      // totaliza��o cobran�a vinculada
//      Padl('0',  23, '0') +
//      // totaliza��o cobran�a caucionada
//      Padl('0',  23, '0') +
//      // totaliza��o cobran�a descontada
//      Padl('0',  23, '0') +
//      // Numero do aviso
//      Espacos(8) +
//      // CNAB
//      Espacos(117);
//
//    arq.Add( UpperCase(RetirarAcentos(texto)) );
//
//    // TRAILER DE ARQUIVO REMESSA
//    texto :=
//      //C�digo do Banco na compensa��o
//      '756' +
//      //Numero do lote remessa
//      '9999' +
//      //Tipo de registro
//      '9' +
//      //Reservado (uso Banco)
//      Espacos(9) +
//      //Quantidade de lotes do arquivo
//      '000001'+
//      //Quantidade de registros do arquivo
//      FormatFloat('000000', num_reg + 3) +
//      // Quantidade de contas por conc. (lotes)
//      '000000' +
//      //Reservado (uso Banco)
//      Espacos(205);
//
//    arq.Add( UpperCase(RetirarAcentos(texto)) );
//    Result := True;
//  end;
//

begin
  Result := False;

//  if registros = nil then begin
//    Informar('Nenhum registro foi informado!');
//    Exit;
//  end;
//
//  arq := TStringList.Create;
//
//  case qtd_posicoes_layout_cnab of
//    400:  GerarArquivoRemessaCNAB400;
//    240:  GerarArquivoRemessaCNAB240;
//  end;
//
//  // Gerando o arquivo texto
//  try
//    arq.SaveToFile(nome_arquivo);
//  except
//    Result := False;
//    Informar('Erro ao gerar o arquivo!');
//    Exit;
//  end;
//
//  // Limpando a mem�ria
//  arq.Clear;
//  arq.Free;
//
//  Result := true;
end;

function TCobrancaSicoob.getRetorno(
  pCodigoEmpresa: string;
  pArquivo: string
): TArray<RecBoletoRetornoRemessa>;
//var
//  arq: TStrings;
//  s: string;
//  posic: integer;
//
//  procedure LimparArquivo(mensagem: string);
//  begin
//    if mensagem <> '' then
//      Informar(mensagem);
//    arq.Clear;
//    arq.Free;
//  end;
//
//  function BuscarMotivoAnalitico(id: integer): string;
//  begin
//    case id of
//      2: Result := 'Confirma��o entrada t�tulo';
//      3: Result := 'Comando recusado';
//      4: Result := 'Transferencia de carteira - entrada';
//      5: Result := 'Liquida��o sem registro';
//      6: Result := 'Liquida��o normal';
//      9: Result := 'Baixa de t�tulo';
//      10: Result := 'Baixa solicitada';
//      11: Result := 'T�tulos em ser';
//      12: Result := 'Abatimento concedido';
//      13: Result := 'Abatimento cancelado';
//      14: Result := 'Altera��o de vencimento';
//      15: Result := 'Liquida��o em cart�rio';
//      19: Result := 'Confirma��o instru��o protesto';
//      20: Result := 'D�bito em conta';
//      21: Result := 'Altera��o de nome do pagador';
//      22: Result := 'Altera��o de endere�o pagador';
//      23: Result := 'Encaminhado a protesto';
//      24: Result := 'Sustar protesto';
//      25: Result := 'Dispensar juros';
//      26: Result := 'Instru��o rejeitada';
//      27: Result := 'Confirma��o alterao dados';
//      28: Result := 'Manuteno t�tulo vencido';
//      30: Result := 'Altera��o dados rejeitada';
//      96: Result := 'Despesas de protesto';
//      97: Result := 'Despesas de susta��o de protesto';
//      98: Result := 'Despesas de custas antecipadas';
//      else Result := 'N�o identificado';
//    end;
//  end;
//
//  procedure ProcessarRetorno400;
//  var
//    i:Integer;
//  begin
//    // Verificando se todas as linha t�m 400 posi��es
//    for i := 0 to arq.Count - 1 do begin
//      if Length(arq[i]) <> 400 then begin
//        LimparArquivo(
//        'O arquivo "' + arquivo + '" n�o est� no formato correto! A linha ' +
//        NPadrao(i + 1, 0) + ' deveria ter 400 posi��es mas est� com ' +
//        NPadrao(Length(arq[i]), 0) + ' posi��es.'
//        );
//        Exit;
//      end;
//    end;
//
//    try
//      // Registros
//      posic := 0;
//      for i := 0 to arq.Count - 2 do begin
//        s := arq[i];
//
//        // Pegando somente registro de transa��o
//        if Copy(s, 1, 1) <> '1' then
//          Continue;
//
//        SetLength(Result, posic + 1);
//
//        Result[posic].nosso_numero := copy(s, 63, 11);
//
//        try
//          Result[posic].data_vencimento := StrToDate( Copy(s, 147, 2) + '/' + Copy(s, 149, 2) + '/' + Copy(s, 151, 2) );
//        except
//          Result[posic].data_vencimento := 0;
//        end;
//
//        Result[posic].valor_documento := Valor( Copy(s, 153, 13) ) * 0.01;
//
//
//        Result[posic].ret_tipo_id := ValorInt(copy(s, 109, 2));
//        Result[posic].ret_nome_tipo := BuscarMotivoAnalitico(Result[posic].ret_tipo_id);
//        Result[posic].ret_baixar := Result[posic].ret_tipo_id in [5, 6, 15, 17, 9];
//
//        Result[posic].ret_data_ocorrencia := StrToDate( Copy(s, 111, 2) + '/' + Copy(s, 113, 2) + '/' + Copy(s, 115, 2) );
//
//        Result[posic].ret_banco_cobrador := ValorInt(Copy(s, 166, 3));
//        Result[posic].ret_valor_pago := Valor( Copy(s, 254, 13) ) * 0.01;
//        Result[posic].ret_juros := Valor( Copy(s, 267, 13)) * 0.01;
//        Result[posic].ret_multa := Valor( Copy(s, 280, 13)) * 0.01;
//        Result[posic].ret_desconto := Valor( Copy(s, 241, 13) ) * 0.01;
//        Result[posic].ret_valor_tarifa := Valor( Copy(s, 182, 7) ) * 0.01;
//        try
//          Result[posic].ret_data_credito := StrToDate( Copy(s, 176, 2) + '/' + Copy(s, 178, 2) + '/' + Copy(s, 180, 2) );
//        except
//          Result[posic].ret_data_credito := Result[posic].ret_data_ocorrencia;
//        end;
//        Inc(posic);
//      end;
//    except on e: Exception do
//      begin
//        Result := nil;
//        Informar('Erro ao ler o arquivo! ' + e.Message);
//      end;
//    end;
//
//    LimparArquivo('');
//  end;
//
//  procedure ProcessarRetorno240;
//  var
//    codigo_segmento: string;
//    i: Integer;
//  begin
//    try
//      // Registros
//      posic := 0;
//      for i := 2 to arq.Count - 3 do begin
//        s := arq[i];
//
//        codigo_segmento := Copy(s, 14, 1);
//
//        if codigo_segmento = 'T' then begin
//          Inc(posic);
//          SetLength(Result, posic);
//        end;
//
//        if codigo_segmento = 'T' then begin
//
//          Result[posic - 1].nosso_numero := copy(s, 38, 20);
//          Result[posic - 1].nosso_numero := copy(Result[posic - 1].nosso_numero, 1, 9);
//
//          try
//            Result[posic - 1].data_vencimento := StrToDate( Copy(s, 74, 2) + '/' + Copy(s, 76, 2) + '/' + Copy(s, 78, 4) );
//          except
//            Result[posic - 1].data_vencimento := 0;
//          end;
//
//          Result[posic - 1].valor_documento := Valor( Copy(s, 82, 15) ) * 0.01;
//          Result[posic - 1].ret_valor_tarifa := Valor( Copy(s, 199, 15) ) * 0.01;
//
//          Result[posic - 1].ret_tipo_id := ValorInt(copy(s, 16, 2));
//          Result[posic - 1].ret_nome_tipo := BuscarMotivoAnalitico(Result[posic - 1].ret_tipo_id);
//          Result[posic - 1].ret_baixar := Result[posic - 1].ret_tipo_id in [06,09,17];
//        end
//        else if codigo_segmento = 'U' then begin
//          Result[posic - 1].ret_data_ocorrencia := StrToDate( Copy(s, 138, 2) + '/' + Copy(s, 140, 2) + '/' + Copy(s, 142, 4) );
//          Result[posic - 1].ret_banco_cobrador := 0;
//          Result[posic - 1].ret_valor_pago := Valor( Copy(s, 78, 15) ) * 0.01;
//          Result[posic - 1].ret_multa := Valor( Copy(s, 18, 15) ) * 0.01;
//          Result[posic - 1].ret_desconto := Valor( Copy(s, 33, 15) ) * 0.01;
//          try
//            Result[posic - 1].ret_data_credito := StrToDate( Copy(s, 146, 2) + '/' + Copy(s, 148, 2) + '/' + Copy(s, 150, 4) );
//          except
//            Result[posic - 1].ret_data_credito := 0;
//          end;
//
//          if Result[posic - 1].ret_data_credito = 0 then
//            Result[posic - 1].ret_data_credito := Result[posic - 1].ret_data_ocorrencia;
//        end;
//      end;
//    except on e: Exception do
//      begin
//        Result := nil;
//        Informar('Erro ao ler o arquivo! ' + e.Message);
//      end;
//    end;
//  end;

begin

  Result := nil;

//  arq := TStringList.Create;
//
//  // Tentando carregar o arquivo
//  try
//    arq.LoadFromFile(arquivo);
//  except
//    LimparArquivo('O arquivo "' + arquivo + '" n�o pode ser carregado!');
//    Exit;
//  end;
//
//  // Verificando se o arquivo tem pelo menos 1 linha
//
//  if ((arq.Count < 3) and (Length(arq[0]) = 240)) or
//     ((arq.Count < 2) and (Length(arq[0]) = 400)) then begin
//    LimparArquivo('O arquivo "' + arquivo + '" n�o est� no formato correto!');
//    Exit;
//  end;
//
//  if Length(arq[0]) <> qtd_posicoes_layout_cnab then begin
//    LimparArquivo(
//      'O arquivo "' + arquivo + '" n�o est� no formato que a conta est� parametrizada! O arquivo ' +
//      ' � de '+ IntToStr(Length(arq[0])) + ' posi��es, e a conta esta parametrizada para ' + IntToStr(qtd_posicoes_layout_cnab) + ' posi��es.');
//    Exit;
//  end;
//
//  case qtd_posicoes_layout_cnab of
//    240 : ProcessarRetorno240;
//    400 : ProcessarRetorno400;
//  end;

end;

//function TCobrancaSicoob.RetirarAcentos(texto: string): string;
//var
//  i: integer;
//
//  function RetirarAcento(C: Char) : Char;
//  begin
//    case AnsiUpperCase(C)[1] of
//      '�','�','�','�','�': Result := IIfStr(IsCharUpper(C),'A','a')[1];
//      '�','�','�','�': Result := IIfStr(IsCharUpper(C),'E','e')[1];
//      '�','�','�','�': Result := IIfStr(IsCharUpper(C),'I','i')[1];
//      '�','�','�','�','�': Result := IIfStr(IsCharUpper(C),'O','o')[1];
//      '�','�','�','�': Result := IIfStr(IsCharUpper(C),'U','u')[1];
//      '�': Result := 'o';
//      '�': Result := 'a';
//    else
//      Result := C;
//    end;
//  end;
//begin
//  Result := '';
//  for i := 1 to Length(texto) do
//    Result := Result + RetirarAcento(texto[i]);
//
//end;

end.
