unit CheckBoxLuka;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.StdCtrls, _BibliotecaGenerica;

type
  TCheckBoxLuka = class(TCheckBox)
  private
    FValor: string;

    function  GetCheckedStr: string;
    procedure SetCheckedStr(const pValor: string);
    procedure SetValor(const pValor: string);
  published
    property CheckedStr: string read GetCheckedStr write SetCheckedStr;
    property Valor: string read FValor write SetValor;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('LD Sistemas', [TCheckBoxLuka]);
end;

{ TCheckBoxLuka }

function TCheckBoxLuka.GetCheckedStr: string;
begin
  Result := _BibliotecaGenerica.ToChar(Checked);
end;


procedure TCheckBoxLuka.SetCheckedStr(const pValor: string);
begin
  Checked := Em(pValor, ['S', 'Sim', 'SIM']);
end;

procedure TCheckBoxLuka.SetValor(const pValor: string);
begin
  FValor := pValor;
end;

end.
