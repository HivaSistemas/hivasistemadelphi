unit _ECF;  // Trocar para _ObjetoECF ou _ECF, verificar com o Diego.

interface

uses
  System.Classes, System.SysUtils, _Biblioteca;

type
  RecRetornoImpressora = record
    Erro: Boolean;
    ECF: Integer;
    COO: Integer;
    NumeroFabrica: string;
  end;

//  REcRetReducao = record
//    buscou_dados: Boolean;
//    data_movimento: TDateTime;
//  end;

  TFormaPagamento = (tfDinheiro, tfCheque, tfCartao, tfCartaoPOS, tfCobranca, tfCredito);

  IECF = Interface
  ['{AA63B8BE-7373-4B2E-9133-8FF3C7EDA891}']
    function IniciarComuns(
      pPorta: Integer;
      pNumeroECF: Integer;
      pNumeroFabrica: string;
      pGerarLogs: Boolean;
      pNomeDinheiro: string;
      pNomeCheque: string;
      pNomeCartao: string;
      pNomeCartaoPOS: string;
      pNomeCobranca: string;
      pNomeCredito: string;
      pPercentualIcms1: Double;
      pPercentualIcms2: Double;
      pPercentualIcms3: Double;
      pPercentualcms4: Double;
      pPercentualIcms5: Double;
      pPercentualIcms6: Double;
      pPercentualIcms7: Double;
      pPercentualIcms8: Double;
      pIndiceTotalizadorNaoFiscal: Integer;
      pDecimaisPrecoVenda: Integer;
      pDecimaisQuantidade: Integer
    ): Boolean;

    function IniciarComunicacao(pPorta: Integer): Boolean;
    function ValidarECF: Boolean;

    function AbrirCupom(const pCPF_CNPJ: string; const pNome: string; const pEndereco: string): Boolean;

    function AdicionarItemCupom(
      pCodigo: string;
      pDescricao: string;
      pMarca: string;
      pQuantidade: Double;
      pPrecoUnitario: Double;
      pTipoTributacao: string;
      pPercentualICMS: Double;
      pValorDesconto: Double;
      pUnidade: string
    ): Boolean;

   function CancelarItemCupom(pNumeroItem: Integer): Boolean;

   function IniciarFechamentoCupom(pAcrescimo: Double; pDesconto: Double): Boolean;

    function TotalizarCupom(
      const pValorDinheiro: Double;
      const pValorCartao: Double;
      const pValorCobranca: Double;
      const pValorCheque: Double;
      const pValorCredito: Double;
      const pValorPOS: Double
    ): Boolean;

    function FinalizarCupom(pMensagem: array of string): Boolean;

    function AbrirComprovanteNaoFiscalVinculado(
      const pCondicao: string;
      const pValor: Double;
      const pNumeroCupom: Integer;
      const pNumeroParcelas: Integer
    ): Boolean;

    function ImprimirTextoNaoFiscal(pTexto: string): Boolean;
    function AcionarGuilhotina: Boolean;

    function CancelarUltimoCupom: Boolean;

    function AbrirRelatorioGerencial: Boolean;
    function ImprimirTextoGerencial(texto: string): Boolean;
    function FecharRelatorioGerencial: Boolean;
    function FecharComprovanteNaoFiscalVinculado(mostrar_msg: Boolean): Boolean;

    (* ---------- Rotinas para recebimento n�o fiscal ---------- *)
    function AbrirRecebimentoNaoFiscal(cpf: string; nome: string; endereco: string): Boolean;
    function EfetuarRecebimentoNaoFiscal(valorRecebimento: Double): Boolean;
    function SubTotalizarRecebimento: Boolean;
    function TotalizarRecebimento: Boolean;
    function EfetuarFormaPagamento(forma_pagamento: TFormaPagamento; valor: Double): Boolean;
    function FecharRecebimentoNaoFiscal(mensagem: string): Boolean;
    function CancelarRecebimentoNaoFiscal(cpf: string; nome: string; endereco: string): Boolean;
    (* ---------- ----------------------------------- ---------- *)

    function AbrirGaveta: Boolean;
    function CancelarCupomAberto: Boolean;
    function EmitirReducaoZ: Boolean;
    function EmitirLeituraX: Boolean;
    function RetornoImpressora: RecRetornoImpressora;
    function EmitirLeituraMemoriaFiscal(data_inicial, data_final: TDateTime): Boolean;
    function ImprimirConfiguracoes: Boolean;
    function NomearTotalizadorNaoSujeitoIcms(indice: Integer; totalizador: string): Boolean;
    function EfetuarSuprimento(valor_suprimento: Double): Boolean;
    function AdicionarAliquotaICMS(const pAliquota: Double): Boolean;
    function ProgramarFormaPagamento(forma_pagamento: string; vinculada: Boolean): Boolean;
  //  function BuscarDadosUltimaReducao: TRetReducao;

    function AtualizarInformacoesECF: Boolean;
    function FinalizarComunicacao: Boolean;
    function SemComunicacao: Boolean;
  end;

  TLog = class(TStringList)
  public
    procedure AddLog(log: string);

  end;

  {$M+}
  TECF = class(TInterfacedObject, IECF)
  protected
    FPorta: Integer;
    FHandle: NativeInt;

    FCOO: Integer;

    FSemComunicacao: Boolean;
    FQtdeDecimaisProduto: Integer;
    FQtdeDecimaisPrecoVenda: Integer;
    FIndiceTotalizadorNaoFiscal: Integer;

    FAdicionarMarcaDescricao: Boolean;

    FIndiceICMS: record
      percentual: Double;
      tipo_icms: string;
    end;

    function IniciarComunicacao(pPorta: Integer): Boolean; virtual; abstract;
    function getNomeFormaPagamento(pFormaPagamento: TFormaPagamento): AnsiString;
  private
    FLog: TLog;
    FGerarLogs: Boolean;

    FNumeroECF: Integer;
    FNumeroFabrica: string;

    FPercIcms1: Double;
    FPercIcms2: Double;
    FPercIcms3: Double;
    FPercIcms4: Double;
    FPercIcms5: Double;
    FPercIcms6: Double;
    FPercIcms7: Double;
    FPercIcms8: Double;

    FNomeDinheiro: AnsiString;
    FNomeCheque: AnsiString;
    FNomeCartao: AnsiString;
    FNomeCartaoPOS: AnsiString;
    FNomeCobranca: AnsiString;
    FNomeCredito: AnsiString;

    function ValidarECF: Boolean;
  public
    function IniciarComuns(
      pPorta: Integer;
      pNumeroECF: Integer;
      pNumeroFabrica: string;
      pGerarLogs: Boolean;
      pNomeDinheiro: string;
      pNomeCheque: string;
      pNomeCartao: string;
      pNomeCartaoPOS: string;
      pNomeCobranca: string;
      pNomeCredito: string;
      pPercentualIcms1: Double;
      pPercentualIcms2: Double;
      pPercentualIcms3: Double;
      pPercentualcms4: Double;
      pPercentualIcms5: Double;
      pPercentualIcms6: Double;
      pPercentualIcms7: Double;
      pPercentualIcms8: Double;
      pIndiceTotalizadorNaoFiscal: Integer;
      pDecimaisPrecoVenda: Integer;
      pDecimaisQuantidade: Integer
    ): Boolean; virtual;

    function AbrirCupom(const pCPF_CNPJ: string; const pNome: string; const pEndereco: string): Boolean; virtual;

    function AdicionarItemCupom(
      pCodigo: string;
      pDescricao: string;
      pMarca: string;
      pQuantidade: Double;
      pPrecoUnitario: Double;
      pTipoTributacao: string;
      pPercentualICMS: Double;
      pValorDesconto: Double;
      pUnidade: string
    ): Boolean; virtual;

    function CancelarItemCupom(pNumeroItem: Integer): Boolean; virtual;

    function IniciarFechamentoCupom(pAcrescimo: Double; pDesconto: Double): Boolean; virtual;

    function TotalizarCupom(
      const pValorDinheiro: Double;
      const pValorCartao: Double;
      const pValorCobranca: Double;
      const pValorCheque: Double;
      const pValorCredito: Double;
      const pValorPOS: Double
    ): Boolean; virtual;

    function FinalizarCupom(pMensagem: array of string): Boolean; virtual;

    function AbrirComprovanteNaoFiscalVinculado(
      const pCondicao: string;
      const pValor: Double;
      const pNumeroCupom: Integer;
      const pNumeroParcelas: Integer
    ): Boolean; virtual;

    function ImprimirTextoNaoFiscal(texto: string): Boolean; virtual;
    function AcionarGuilhotina: Boolean; virtual;
    function CancelarUltimoCupom: Boolean; virtual;
    function AbrirRelatorioGerencial: Boolean; virtual;
    function ImprimirTextoGerencial(texto: string): Boolean; virtual;
    function FecharRelatorioGerencial: Boolean; virtual;
    function FecharComprovanteNaoFiscalVinculado(mostrar_msg: Boolean): Boolean; virtual;

    function AbrirRecebimentoNaoFiscal(cpf: string; nome: string; endereco: string): Boolean; virtual;
    function EfetuarRecebimentoNaoFiscal(valorRecebimento: Double): Boolean; virtual;
    function SubTotalizarRecebimento: Boolean; virtual;
    function TotalizarRecebimento: Boolean; virtual;
    function EfetuarFormaPagamento(forma_pagamento: TFormaPagamento; valor: Double): Boolean; virtual;
    function FecharRecebimentoNaoFiscal(mensagem: string): Boolean; virtual;
    function CancelarRecebimentoNaoFiscal(cpf: string; nome: string; endereco: string): Boolean; virtual;

    function AbrirGaveta: Boolean; virtual;
    function CancelarCupomAberto: Boolean; virtual;
    function EmitirReducaoZ: Boolean; virtual;
    function EmitirLeituraX: Boolean; virtual;
    function RetornoImpressora: RecRetornoImpressora; virtual;
    function EmitirLeituraMemoriaFiscal(data_inicial, data_final: TDateTime): Boolean; virtual;
    function ImprimirConfiguracoes: Boolean; virtual;
    function NomearTotalizadorNaoSujeitoIcms(indice: Integer; totalizador: string): Boolean; virtual;
    function EfetuarSuprimento(valor_suprimento: Double): Boolean; virtual;
    function AdicionarAliquotaICMS(const pAliquota: Double): Boolean; virtual;
    function ProgramarFormaPagamento(forma_pagamento: string; vinculada: Boolean): Boolean; virtual;
//    function BuscarDadosUltimaReducao: TRetReducao; virtual;

    function FinalizarComunicacao: Boolean; virtual;
    function AtualizarInformacoesECF: Boolean; virtual;
    function SemComunicacao: Boolean; virtual;

    destructor Destroy; override;
  published
    property AdicionarMarcaDescricao: Boolean read FAdicionarMarcaDescricao write FAdicionarMarcaDescricao;
    property PercentualICMS1: Double read FPercIcms1;
    property PercentualICMS2: Double read FPercIcms2;
    property PercentualICMS3: Double read FPercIcms3;
    property PercentualICMS4: Double read FPercIcms4;
    property PercentualICMS5: Double read FPercIcms5;
    property PercentualICMS6: Double read FPercIcms6;
    property PercentualICMS7: Double read FPercIcms7;
    property PercentualICMS8: Double read FPercIcms8;

    property NomeDinheiro: AnsiString read FNomeDinheiro;
    property NomeCheque: AnsiString read FNomeCheque;
    property NomeCartao: AnsiString read FNomeCartao;
    property NomeCartaoPOS: AnsiString read FNomeCartaoPOS;
    property NomeCobranca: AnsiString read FNomeCobranca;
    property NomeCredito: AnsiString read FNomeCredito;

    property COO: Integer read FCOO;
  end;

implementation

{ TLog }

procedure TLog.AddLog(log: string);
begin

end;

{ TECF }

function TECF.IniciarComuns(
  pPorta: Integer;
  pNumeroECF: Integer;
  pNumeroFabrica: string;
  pGerarLogs: Boolean;
  pNomeDinheiro: string;
  pNomeCheque: string;
  pNomeCartao: string;
  pNomeCartaoPOS: string;
  pNomeCobranca: string;
  pNomeCredito: string;
  pPercentualIcms1: Double;
  pPercentualIcms2: Double;
  pPercentualIcms3: Double;
  pPercentualcms4: Double;
  pPercentualIcms5: Double;
  pPercentualIcms6: Double;
  pPercentualIcms7: Double;
  pPercentualIcms8: Double;
  pIndiceTotalizadorNaoFiscal: Integer;
  pDecimaisPrecoVenda: Integer;
  pDecimaisQuantidade: Integer
): Boolean;
begin
  inherited;
  FSemComunicacao := True;
  FNumeroECF := pNumeroECF;
  FNumeroFabrica := pNumeroFabrica;

  FPercIcms1 := pPercentualIcms1;
  FPercIcms2 := pPercentualIcms2;
  FPercIcms3 := pPercentualIcms3;
  FPercIcms4 := pPercentualcms4;
  FPercIcms5 := pPercentualIcms5;
  FPercIcms6 := pPercentualIcms6;
  FPercIcms7 := pPercentualIcms7;
  FPercIcms8 := pPercentualIcms8;

  FNomeDinheiro  := AnsiString( pNomeDinheiro );
  FNomeCheque    := AnsiString( pNomeCheque );
  FNomeCartao    := AnsiString( pNomeCartao );
  FNomeCartaoPOS := AnsiString( pNomeCartaoPOS );
  FNomeCobranca  := AnsiString( pNomeCobranca );
  FNomeCredito   := AnsiString( pNomeCredito );

  FQtdeDecimaisProduto        := pDecimaisQuantidade;
  FQtdeDecimaisPrecoVenda     := pDecimaisPrecoVenda;
  FIndiceTotalizadorNaoFiscal := pIndiceTotalizadorNaoFiscal;

  FGerarLogs := pGerarLogs;
//  if FGerarLogs then begin
//    FLog := TLog.Create;
//
//  end;

  Result := IniciarComunicacao(pPorta);
  if not Result then
    Exit;

  Result := ValidarECF;
end;

function TECF.ValidarECF: Boolean;
var
  vRet: RecRetornoImpressora;
begin
  Result := False;
  FSemComunicacao := True;

  vRet := Self.RetornoImpressora;
  if vRet.Erro then begin
    Exclamar('Falha ao buscar informa��es da impressora fiscal!');
    Exit;
  end;

  if FNumeroFabrica <> vRet.NumeroFabrica then begin
    Exclamar('O numero de f�brica do cadastro(' + FNumeroFabrica + ') � diferente do numero de fabrica da impressora fiscal conectada(' + vRet.NumeroFabrica + ')!');
    FSemComunicacao := True;
    Exit;
  end;

  if FNumeroECF <> vRet.ECF then begin
    Exclamar('O numero de loja do cadastro(' + IntToStr(FNumeroECF) + ') � diferente do numero de loja da impressora fiscal conectada(' + IntToStr(vRet.ECF) + ')!');
    FSemComunicacao := True;
    Exit;
  end;

  Result := True;
  FSemComunicacao := False;
end;

function TECF.getNomeFormaPagamento(pFormaPagamento: TFormaPagamento): AnsiString;
begin
  Result := '';
  case pFormaPagamento of
    tfDinheiro:  Result := NomeDinheiro;
    tfCheque:    Result := NomeCheque;
    tfCartao:    Result := NomeCartao;
    tfCartaoPOS: Result := NomeCartaoPOS;
    tfCobranca:  Result := NomeCobranca;
    tfCredito:   Result := NomeCredito;
  end;
end;

function TECF.AbrirCupom(const pCPF_CNPJ: string; const pNome: string; const pEndereco: string): Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.AdicionarItemCupom(
  pCodigo: string;
  pDescricao: string;
  pMarca: string;
  pQuantidade: Double;
  pPrecoUnitario: Double;
  pTipoTributacao: string;
  pPercentualICMS: Double;
  pValorDesconto: Double;
  pUnidade: string
): Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.CancelarItemCupom(pNumeroItem: Integer): Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.IniciarFechamentoCupom(pAcrescimo, pDesconto: Double): Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.TotalizarCupom(
  const pValorDinheiro: Double;
  const pValorCartao: Double;
  const pValorCobranca: Double;
  const pValorCheque: Double;
  const pValorCredito: Double;
  const pValorPOS: Double
): Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.FinalizarCupom(pMensagem: array of string): Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.AbrirComprovanteNaoFiscalVinculado(
  const pCondicao: string;
  const pValor: Double;
  const pNumeroCupom: Integer;
  const pNumeroParcelas: Integer
): Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.ImprimirTextoNaoFiscal(texto: string): Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.AcionarGuilhotina: Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.CancelarUltimoCupom: Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.AbrirRelatorioGerencial: Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.ImprimirTextoGerencial(texto: string): Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.FecharRelatorioGerencial: Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.FecharComprovanteNaoFiscalVinculado(mostrar_msg: Boolean): Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.AbrirRecebimentoNaoFiscal(cpf: string; nome: string; endereco: string): Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.EfetuarRecebimentoNaoFiscal(valorRecebimento: Double): Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.SubTotalizarRecebimento: Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.TotalizarRecebimento: Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.EfetuarFormaPagamento(forma_pagamento: TFormaPagamento; valor: Double): Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.FecharRecebimentoNaoFiscal(mensagem: string): Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.CancelarRecebimentoNaoFiscal(cpf: string; nome: string; endereco: string): Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.AbrirGaveta: Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.CancelarCupomAberto: Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.EmitirReducaoZ: Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.EmitirLeituraX: Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.RetornoImpressora: RecRetornoImpressora;
begin
  inherited;
  Result.Erro := False;
  Result.ECF  := 0;
  Result.COO  := 0;
  Result.NumeroFabrica := '';
end;

function TECF.EmitirLeituraMemoriaFiscal(data_inicial, data_final: TDateTime): Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.ImprimirConfiguracoes: Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.NomearTotalizadorNaoSujeitoIcms(indice: Integer; totalizador: string): Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.EfetuarSuprimento(valor_suprimento: Double): Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.AdicionarAliquotaICMS(const pAliquota: Double): Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.ProgramarFormaPagamento(forma_pagamento: string; vinculada: Boolean): Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

//function TECF.BuscarDadosUltimaReducao: TRetReducao;
//begin
//  inherited;
//  Result.buscou_dados := False;
//  // Apenas passar aqui para gerar os logs
//end;

function TECF.FinalizarComunicacao: Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.AtualizarInformacoesECF: Boolean;
begin
  inherited;
  Result := False;
  // Apenas passar aqui para gerar os logs
end;

function TECF.SemComunicacao: Boolean;
begin
  inherited;
  Result := not ValidarECF;
end;

destructor TECF.Destroy;
begin
  if FLog <> nil then
    FreeAndNil(FLog);
end;

end.
