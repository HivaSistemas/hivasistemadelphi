unit _Bematech2;

interface

uses
  System.Classes, System.SysUtils, System.StrUtils, Winapi.Windows, _ECF, _Biblioteca;

type
  TBematech = class(TECF)
  protected
    function IniciarComunicacao(pPorta: Integer): Boolean; override;
  private
    FDLL: string;

    FProcBematech_FI_AbreCupom: function(CGC_CPF: AnsiString): Integer; stdcall;
    FProcBematech_FI_AbreCupomMFD: function(CGC: AnsiString; Nome: AnsiString; Endereco: AnsiString): Integer; stdCall;
    FProcBematech_FI_AumentaDescricaoItem: function(descricao: AnsiString): Integer; stdcall;

    FProcBematech_FI_VendeItemDepartamento: function(
      Codigo: AnsiString;
      Descricao: AnsiString;
      Aliquota: AnsiString;
      ValorUnitario: AnsiString;
      Quantidade: AnsiString;
      Acrescimo: AnsiString;
      Desconto: AnsiString;
      IndiceDepartamento: AnsiString;
      UnidadeMedida: AnsiString
    ): Integer; stdcall;

    FProcBematech_FI_CancelaItemGenerico: function(NumeroItem: AnsiString): Integer; stdcall;
    FProcBematech_FI_IniciaFechamentoCupom: function(AcrescimoDesconto: AnsiString; TipoAcrescimoDesconto: AnsiString; ValorAcrescimoDesconto: AnsiString): Integer; stdcall;
    FProcBematech_FI_EfetuaFormaPagamento: function(FormaPagamento: AnsiString; ValorFormaPagamento: AnsiString): Integer; stdcall;
    FProcBematech_FI_TerminaFechamentoCupom: function(Mensagem: AnsiString): Integer; stdcall;
    FProcBematech_FI_AcionaGaveta: function(): Integer; stdcall;
    FProcBematech_FI_CancelaCupom: function(): Integer; stdcall;

    FProcBematech_FI_AbreComprovanteNaoFiscalVinculado: function(FormaPagamento: AnsiString; Valor: AnsiString; NumeroCupom: AnsiString): Integer; stdcall;
    FProcBematech_FI_UsaComprovanteNaoFiscalVinculado: function(Texto: AnsiString): Integer; stdcall;
    FProcBematech_FI_FechaComprovanteNaoFiscalVinculado: function(): Integer; stdcall;
    FProcBematech_FI_AcionaGuilhotinaMFD: function(i: Integer): Integer; stdcall;

    FProcBematech_FI_AbreRecebimentoNaoFiscalMFD: function(cpf, nome, endereco: AnsiString): integer; stdcall;
    FProcBematech_FI_EfetuaRecebimentoNaoFiscalMFD: function(IndiceTotalizador: AnsiString; ValorRecebimento: AnsiString): integer; stdcall;
    FProcBematech_FI_SubTotalizaRecebimentoMFD: function(): Integer; stdcall;
    FProcBematech_FI_TotalizaRecebimentoMFD: function(): Integer; stdcall;
    FProcBematech_FI_FechaRecebimentoNaoFiscalMFD: function(mensagem: AnsiString): Integer; stdcall;
    FProcBematech_FI_CancelaRecebimentoNaoFiscalMFD: function(cpf, nome, endereco: AnsiString): Integer; stdcall;

    FProcBematech_FI_AbreRelatorioGerencialMFD: function(Indice: AnsiString): Integer; stdcall;
    FProcBematech_FI_FechaRelatorioGerencial: function(): Integer; stdcall;

    FProcBematech_FI_RetornoImpressora: function(var ACK: Integer; var ST1: Integer; var ST2: Integer): Integer; stdcall;
    FProcBematech_FI_NumeroSerieMFD: function(NumeroSerie: AnsiString): Integer; stdcall;
    FProcBematech_FI_NumeroCupom: function(NumeroCupom: AnsiString): Integer; stdcall;
    FProcBematech_FI_NumeroCaixa: function(NumeroCaixa: AnsiString): Integer; stdcall;
    FProcBematech_FI_FlagsFiscais: function(var Flag: Integer): Integer; stdcall;

    function HouveErro(const pRetorno: Integer): Boolean;
  public
    function AbrirCupom(const pCPF_CNPJ: string; const pNome: string; const pEndereco: string): Boolean; override;

    function AdicionarItemCupom(
      pCodigo: string;
      pDescricao: string;
      pMarca: string;
      pQuantidade: Double;
      pPrecoUnitario: Double;
      pTipoTributacao: string;
      pPercentualICMS: Double;
      pValorDesconto: Double;
      pUnidade: string
    ): Boolean; override;

    function CancelarItemCupom(numero_item: Integer): Boolean; override;

    function IniciarFechamentoCupom(acrescimo: Double; desconto: Double): Boolean; override;

    function TotalizarCupom(
      const pValorDinheiro: Double;
      const pValorCartao: Double;
      const pValorCobranca: Double;
      const pValorCheque: Double;
      const pValorCredito: Double;
      const pValorPOS: Double
    ): Boolean; override;

    function FinalizarCupom(mensagem: array of string): Boolean; override;

    function AbrirGaveta: Boolean; override;
    function CancelarCupomAberto: Boolean; override;
    function EmitirReducaoZ: Boolean; override;
    function EmitirLeituraX: Boolean; override;
    function RetornoImpressora: RecRetornoImpressora; override;
    function EmitirLeituraMemoriaFiscal(data_inicial, data_final: TDateTime): Boolean; override;
    function ImprimirConfiguracoes: Boolean; override;
    function NomearTotalizadorNaoSujeitoIcms(indice: Integer; totalizador: string): Boolean; override;
    function EfetuarSuprimento(valor_suprimento: Double): Boolean; override;
    function AdicionarAliquotaICMS(const pAliquota: Double): Boolean; override;
    function ProgramarFormaPagamento(forma_pagamento: string; vinculada: Boolean): Boolean; override;

    function AbrirComprovanteNaoFiscalVinculado(
      const pCondicao: string;
      const pValor: Double;
      const pNumeroCupom: Integer;
      const pNumeroParcelas: Integer
    ): Boolean; override;

    function FecharComprovanteNaoFiscalVinculado(mostrar_msg: Boolean): Boolean; override;

    function ImprimirTextoNaoFiscal(texto: string): Boolean; override;
    function AcionarGuilhotina: Boolean; override;
    function CancelarUltimoCupom: Boolean; override;

    function AbrirRecebimentoNaoFiscal(cpf: string; nome: string; endereco: string): Boolean; override;
    function EfetuarRecebimentoNaoFiscal(valorRecebimento: Double): Boolean; override;
    function SubTotalizarRecebimento: Boolean; override;
    function TotalizarRecebimento: Boolean; override;
    function EfetuarFormaPagamento(forma_pagamento: TFormaPagamento; valor: Double): Boolean; override;
    function FecharRecebimentoNaoFiscal(mensagem: string): Boolean; override;
    function CancelarRecebimentoNaoFiscal(cpf: string; nome: string; endereco: string): Boolean; override;

    function AbrirRelatorioGerencial: Boolean; override;
    function ImprimirTextoGerencial(texto: string): Boolean; override;
    function FecharRelatorioGerencial: Boolean; override;
    function AtualizarInformacoesECF: Boolean; override;
//    function BuscarDadosUltimaReducao: TRetReducao; override;
  end;

implementation

{ TBematech }

type
  TBematech_FI_AbrePortaSerial = function(): integer; stdcall;
  TBematech_FI_ReducaoZ = function(Data: AnsiString; Hora: AnsiString): Integer; stdcall;
  TBematech_FI_LeituraX = function(): Integer; stdcall;
  TBematech_FI_LeituraMemoriaFiscalData = function(DataInicial: AnsiString; DataFinal: AnsiString): Integer; stdcall;
  TBematech_FI_ImprimeConfiguracoesImpressora = function(): Integer; stdcall;
  TBematech_FI_NomeiaTotalizadorNaoSujeitoIcms = function(Indice: Integer; Totalizador: AnsiString): Integer; stdcall;
  TBematech_FI_Suprimento = function(Valor: AnsiString; FormaPagamento: AnsiString): Integer; stdcall;
  TBematech_FI_ProgramaAliquota = function(Aliquota: AnsiString; ICMS_ISS: Integer): Integer; stdcall;
  TBematech_FI_ProgramaFormaPagamentoMFD = function(FormaPagto: AnsiString; OperacaoTef: AnsiString): Integer; stdcall;
  TBematech_FI_DadosUltimaReducao = function(dadosreducao: AnsiString): Integer; stdcall;
  TBematech_FI_NumeroCupom = function(NumeroCupom: AnsiString): Integer; stdcall;

function TBematech.HouveErro(const pRetorno: Integer): Boolean;
var
  iACK: Integer;
  iST1: Integer;
  iST2: Integer;
  vMsgErro: string;

  procedure GerarErro(pMensagem: string; pMostrarMensagem: Boolean = True);
  begin
    BlockInput(False);

    if pMostrarMensagem then
      Exclamar(pMensagem);

    Result := True;
  end;

begin
  Result := False;

  if pRetorno = 0 then
    GerarErro('Erro de comunica��o!')
  else if pRetorno = -2 then
    GerarErro('Par�metro inv�lido!')
  else if pRetorno = -3 then
    GerarErro('Al�quota n�o programada!')
  else if pRetorno = -4 then
    GerarErro('Arquivo INI n�o encontrado ou par�metro inv�lido para o nome da porta!')
  else if pRetorno = -5 then
    GerarErro('Erro ao abrir a porta de comunica��o!')
  else if pRetorno = -9 then
    GerarErro('Erro ao fechar a porta de comunica��o!')
  else if pRetorno = -24 then
    GerarErro('Forma de Pagamento n�o programada!')
  else if pRetorno = -27 then
    GerarErro('Status de impressora diferente de 6,0,0 (Ack, Sti, Sta)!');

  if Result then
    Exit;

  // Verificando outros retornos
  iACK := 0;
  iST1 := 0;
  iST2 := 0;
  vMsgErro := '';

  FProcBematech_FI_RetornoImpressora(iACK, iST1, iST2);

  if iACK = 6 then begin
    // Verifica ST1
    if iST1 >= 128 then begin
      iST1 := iST1 - 128;
      vMsgErro := vMsgErro + IfThen(vMsgErro <> '', #13) + 'Fim de papel';
    end;

    if iST1 >= 64 then begin
      iST1 := iST1 - 64;
      // Pouco papel: n�o � para travar a impressora
    end;

    if iST1 >= 32 then begin
      iST1 := iST1 - 32;
      vMsgErro := vMsgErro + IfThen(vMsgErro <> '', #13) + 'Erro no rel�gio';
    end;

    if iST1 >= 16 then begin
      iST1 := iST1 - 16;
      vMsgErro := vMsgErro + IfThen(vMsgErro <> '', #13) + 'Impressora em ERRO';
    end;

    if iST1 >= 8 then begin
      iST1 := iST1 - 8;
      vMsgErro := vMsgErro + IfThen(vMsgErro <> '', #13) + 'CMD n�o inicializado com ESC';
    end;

    if iST1 >= 4 then begin
      iST1 := iST1 - 4;
      vMsgErro := vMsgErro + IfThen(vMsgErro <> '', #13) + 'Comando inexistente';
    end;

    if iST1 >= 2 then begin
      iST1 := iST1 - 2;
      vMsgErro := vMsgErro + IfThen(vMsgErro <> '', #13) + 'Cupom aberto';
    end;

    if iST1 >= 1 then begin
      iST1 := iST1 - 1;
      vMsgErro := vMsgErro + IfThen(vMsgErro <> '', #13) + 'Nr. de par�metros inv�lido';
    end;

    // Verifica ST2
    if iST2 >= 128 then begin
      iST2 := iST2 - 128;
      vMsgErro := vMsgErro + IfThen(vMsgErro <> '', #13) + 'Tipo de par�metros inv�lido';
    end;

    if iST2 >= 64 then begin
      iST2 := iST2 - 64;
      vMsgErro := vMsgErro + IfThen(vMsgErro <> '', #13) + 'Mem�ria fiscal lotada';
    end;

    if iST2 >= 32 then begin
      iST2 := iST2 - 32;
      vMsgErro := vMsgErro + IfThen(vMsgErro <> '', #13) + 'CMOS n�o vol�til';
    end;

    if iST2 >= 16 then begin
      iST2 := iST2 - 16;
      vMsgErro := vMsgErro + IfThen(vMsgErro <> '', #13) + 'Al�quota n�o programada';
    end;

    if iST2 >= 8 then begin
      iST2 := iST2 - 8;
      vMsgErro := vMsgErro + IfThen(vMsgErro <> '', #13) + 'Al�quotas lotadas';
    end;

    if iST2 >= 4 then begin
      iST2 := iST2 - 4;
      vMsgErro := vMsgErro + IfThen(vMsgErro <> '', #13) + 'Cancelamento n�o permitido';
    end;

    if iST2 >= 2 then begin
      iST2 := iST2 - 2;
      vMsgErro := vMsgErro + IfThen(vMsgErro <> '', #13) + 'CNPJ/IE n�o programados';
    end;

    if iST2 >= 1 then begin
      iST2 := iST2 - 1;
      vMsgErro := vMsgErro + IfThen(vMsgErro <> '', #13) + 'Comando n�o executado';
    end;

    if vMsgErro <> '' then begin
      GerarErro(vMsgErro);
      Exit;
    end;
  end;

  if iACK = 21 then begin
    GerarErro('A impressora retornou NAK!');
    Exit;
  end;
end;

function TBematech.IniciarComunicacao(pPorta: Integer): Boolean;
var
  ProcBematech_FI_AbrePortaSerial: TBematech_FI_AbrePortaSerial;
begin
  inherited;
  Result := False;
  FSemComunicacao := True;
  try
    FDLL := {$IFDEF WIN32} 'BEMAFI32.DLL' {$ELSE} 'BEMAFI64.DLL' {$ENDIF};
    if not FileExists(FDLL) then begin
      Exclamar('A DLL "' + FDLL + '" n�o foi encontrada na pasta do execut�vel!');
      Exit;
    end;

    (* Tentar carregar a DLL de maneira Din�mica *)
    FHandle := LoadLibrary( PWideChar(FDLL) );
    if FHandle = 0 then begin
      Exclamar('Falha ao carregar a DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_RetornoImpressora := GetProcAddress(FHandle, 'Bematech_FI_RetornoImpressora');
    if not Assigned(FProcBematech_FI_RetornoImpressora) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_RetornoImpressora" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_NumeroSerieMFD := GetProcAddress(FHandle, 'Bematech_FI_NumeroSerieMFD');
    if not Assigned(FProcBematech_FI_NumeroSerieMFD) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_NumeroSerieMFD" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_NumeroCupom := GetProcAddress(FHandle, 'Bematech_FI_NumeroCupom');
    if not Assigned(FProcBematech_FI_NumeroCupom) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_NumeroCupom" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_NumeroCaixa := GetProcAddress(FHandle, 'Bematech_FI_NumeroCaixa');
    if not Assigned(FProcBematech_FI_NumeroCaixa) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_NumeroCaixa" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @ProcBematech_FI_AbrePortaSerial := GetProcAddress(FHandle, 'Bematech_FI_AbrePortaSerial');
    if not Assigned(ProcBematech_FI_AbrePortaSerial) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_AbrePortaSerial" na DLL "' + FDLL + '"!');
      Exit;
    end;

    Result := not HouveErro( ProcBematech_FI_AbrePortaSerial() );
    if not Result then
      Exit;

    @FProcBematech_FI_AbreCupom := GetProcAddress(FHandle, 'Bematech_FI_AbreCupom');
    if not Assigned(FProcBematech_FI_AbreCupom) then begin
      Exclamar('Falha ao chamar o m�todo "ProcBematech_FI_AbreCupom" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_AbreCupomMFD := GetProcAddress(FHandle, 'Bematech_FI_AbreCupomMFD');
    if not Assigned(FProcBematech_FI_AbreCupomMFD) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_AbreCupomMFD" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_AumentaDescricaoItem := GetProcAddress(FHandle, 'Bematech_FI_AumentaDescricaoItem');
    if not Assigned(FProcBematech_FI_AumentaDescricaoItem) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_AumentaDescricaoItem" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_VendeItemDepartamento := GetProcAddress(FHandle, 'Bematech_FI_VendeItemDepartamento');
    if not Assigned(FProcBematech_FI_VendeItemDepartamento) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_VendeItemDepartamento" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_CancelaItemGenerico := GetProcAddress(FHandle, 'Bematech_FI_CancelaItemGenerico');
    if not Assigned(FProcBematech_FI_CancelaItemGenerico) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_CancelaItemGenerico" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_IniciaFechamentoCupom := GetProcAddress(FHandle, 'Bematech_FI_IniciaFechamentoCupom');
    if not Assigned(FProcBematech_FI_IniciaFechamentoCupom) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_IniciaFechamentoCupom" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_EfetuaFormaPagamento := GetProcAddress(FHandle, 'Bematech_FI_EfetuaFormaPagamento');
    if not Assigned(FProcBematech_FI_EfetuaFormaPagamento) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_EfetuaFormaPagamento" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_TerminaFechamentoCupom := GetProcAddress(FHandle, 'Bematech_FI_TerminaFechamentoCupom');
    if not Assigned(FProcBematech_FI_TerminaFechamentoCupom) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_TerminaFechamentoCupom" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_AcionaGaveta := GetProcAddress(FHandle, 'Bematech_FI_AcionaGaveta');
    if not Assigned(FProcBematech_FI_AcionaGaveta) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_AcionaGaveta" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_CancelaCupom := GetProcAddress(FHandle, 'Bematech_FI_CancelaCupom');
    if not Assigned(FProcBematech_FI_CancelaCupom) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_CancelaCupom" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_AbreComprovanteNaoFiscalVinculado := GetProcAddress(FHandle, 'Bematech_FI_AbreComprovanteNaoFiscalVinculado');
    if not Assigned(FProcBematech_FI_AbreComprovanteNaoFiscalVinculado) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_AbreComprovanteNaoFiscalVinculado" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_UsaComprovanteNaoFiscalVinculado := GetProcAddress(FHandle, 'Bematech_FI_UsaComprovanteNaoFiscalVinculado');
    if not Assigned(FProcBematech_FI_UsaComprovanteNaoFiscalVinculado) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_UsaComprovanteNaoFiscalVinculado" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_FechaComprovanteNaoFiscalVinculado := GetProcAddress(FHandle, 'Bematech_FI_FechaComprovanteNaoFiscalVinculado');
    if not Assigned(FProcBematech_FI_FechaComprovanteNaoFiscalVinculado) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_FechaComprovanteNaoFiscalVinculado" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_AcionaGuilhotinaMFD := GetProcAddress(FHandle, 'Bematech_FI_AcionaGuilhotinaMFD');
    if not Assigned(FProcBematech_FI_AcionaGuilhotinaMFD) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_AcionaGuilhotinaMFD" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_AbreRecebimentoNaoFiscalMFD := GetProcAddress(FHandle, 'Bematech_FI_AbreRecebimentoNaoFiscalMFD');
    if not Assigned(FProcBematech_FI_AbreRecebimentoNaoFiscalMFD) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_AbreRecebimentoNaoFiscalMFD" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_EfetuaRecebimentoNaoFiscalMFD := GetProcAddress(FHandle, 'Bematech_FI_EfetuaRecebimentoNaoFiscalMFD');
    if not Assigned(FProcBematech_FI_EfetuaRecebimentoNaoFiscalMFD) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_EfetuaRecebimentoNaoFiscalMFD" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_SubTotalizaRecebimentoMFD := GetProcAddress(FHandle, 'Bematech_FI_SubTotalizaRecebimentoMFD');
    if not Assigned(FProcBematech_FI_SubTotalizaRecebimentoMFD) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_SubTotalizaRecebimentoMFD" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_TotalizaRecebimentoMFD := GetProcAddress(FHandle, 'Bematech_FI_TotalizaRecebimentoMFD');
    if not Assigned(FProcBematech_FI_TotalizaRecebimentoMFD) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_TotalizaRecebimentoMFD" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_FechaRecebimentoNaoFiscalMFD := GetProcAddress(FHandle, 'Bematech_FI_FechaRecebimentoNaoFiscalMFD');
    if not Assigned(FProcBematech_FI_FechaRecebimentoNaoFiscalMFD) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_FechaRecebimentoNaoFiscalMFD" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_CancelaRecebimentoNaoFiscalMFD := GetProcAddress(FHandle, 'Bematech_FI_CancelaRecebimentoNaoFiscalMFD');
    if not Assigned(FProcBematech_FI_CancelaRecebimentoNaoFiscalMFD) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_CancelaRecebimentoNaoFiscalMFD" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_AbreRelatorioGerencialMFD := GetProcAddress(FHandle, 'Bematech_FI_AbreRelatorioGerencialMFD');
    if not Assigned(FProcBematech_FI_AbreRelatorioGerencialMFD) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_AbreRelatorioGerencialMFD" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_FechaRelatorioGerencial := GetProcAddress(FHandle, 'Bematech_FI_FechaRelatorioGerencial');
    if not Assigned(FProcBematech_FI_FechaRelatorioGerencial) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_FechaRelatorioGerencial" na DLL "' + FDLL + '"!');
      Exit;
    end;

    @FProcBematech_FI_FlagsFiscais := GetProcAddress(FHandle, 'Bematech_FI_FlagsFiscais');
    if not Assigned(FProcBematech_FI_FlagsFiscais) then begin
      Exclamar('Falha ao chamar o m�todo "Bematech_FI_FlagsFiscais" na DLL "' + FDLL + '"!');
      Exit;
    end;

    FSemComunicacao := False;
    FPorta := pPorta;
    Result := True;
  except
    on e: Exception do
      Exclamar(e.Message);
  end;
end;

function TBematech.AbrirCupom(const pCPF_CNPJ: string; const pNome: string; const pEndereco: string): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  if pNome = '' then
    Result := not HouveErro( FProcBematech_FI_AbreCupom(AnsiString(Copy(pCPF_CNPJ, 1, 20))) )
  else
    Result := not HouveErro( FProcBematech_FI_AbreCupomMFD(AnsiString(Copy(pCPF_CNPJ, 1, 20)),AnsiString(Copy(pNome, 1, 30)), AnsiString(Copy(pEndereco, 1, 79))) );
end;

function TBematech.AdicionarItemCupom(
  pCodigo: string;
  pDescricao: string;
  pMarca: string;
  pQuantidade: Double;
  pPrecoUnitario: Double;
  pTipoTributacao: string;
  pPercentualICMS: Double;
  pValorDesconto: Double;
  pUnidade: string
): Boolean;
var
  vPreco: AnsiString;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  if FAdicionarMarcaDescricao then
    pDescricao := pDescricao + ' ' + pMarca;

  pDescricao := _Biblioteca.LimparTexto(pDescricao);
  if Length(pDescricao) > 29 then begin
    pDescricao := Copy(pDescricao, 1, 200);
    Result := not HouveErro( FProcBematech_FI_AumentaDescricaoItem(AnsiString(pDescricao)) );
    if not Result then
      Exit;
  end;

  vPreco := AnsiString( LPad(RetornaNumeros(NFormat(pPrecoUnitario, 3)), 8, '0') );

  Result :=
    not HouveErro(
      FProcBematech_FI_VendeItemDepartamento(
        AnsiString(Copy(pCodigo, 1, 14)),
        AnsiString(copy(pDescricao, 1, 40)),
        AnsiString(pTipoTributacao),
        vPreco,
        AnsiString(LPad( RetornaNumeros(FormatFloat('#0.000', pQuantidade)), 7, '0')),
        AnsiString(LPad( RetornaNumeros(FormatFloat('#0.00', 0)), 10, '0')),
        AnsiString(LPad( RetornaNumeros(FormatFloat('#0.00', pValorDesconto)), 10, '0')),
        '01',
        AnsiString(LPad(pUnidade, 2))
      )
    );
end;

function TBematech.CancelarItemCupom(numero_item: Integer): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  Result := not HouveErro( FProcBematech_FI_CancelaItemGenerico( AnsiString(FormatFloat('000', numero_item))) );
end;

function TBematech.IniciarFechamentoCupom(acrescimo: Double; desconto: Double): Boolean;
var
  tipo_acao: string;
  sld: Currency;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  try
    sld := acrescimo - desconto;

    if sld >= 0 then
      tipo_acao := 'A'
    else
      tipo_acao := 'D';

    Result := not HouveErro( FProcBematech_FI_IniciaFechamentoCupom( AnsiString(tipo_acao), '$', AnsiString(RetornaNumeros(FormatFloat('#0.00', abs(sld))))) );
  except
    Result := false;
  end;
end;

function TBematech.TotalizarCupom(
  const pValorDinheiro: Double;
  const pValorCartao: Double;
  const pValorCobranca: Double;
  const pValorCheque: Double;
  const pValorCredito: Double;
  const pValorPOS: Double
): Boolean;

  function EfetuarFormaPagamento(forma_pagamento: AnsiString; valor: Double): Boolean;
  begin
    if valor = 0 then begin
      Result := True;
      Exit;
    end;

    Result := not HouveErro( FProcBematech_FI_EfetuaFormaPagamento(forma_pagamento, AnsiString(RetornaNumeros( FormatFloat('#0.00', valor)))) );
    while not Result do begin
      if not Perguntar('Impressora n�o responde. Deseja tentar novamente?') then begin
        Result := False;
        Exit;
      end;

      Result := not HouveErro( FProcBematech_FI_EfetuaFormaPagamento(forma_pagamento, AnsiString(RetornaNumeros( FormatFloat('#0.00', valor)))) );
    end;
  end;

begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  if not EfetuarFormaPagamento(NomeCheque, pValorCheque) then
    Exit;

  if not EfetuarFormaPagamento(NomeCobranca, pValorCobranca) then
    Exit;

  if not EfetuarFormaPagamento(NomeCredito, pValorCredito) then
    Exit;

  if not EfetuarFormaPagamento(NomeCartaoPOS, pValorPOS) then
    Exit;

  if not EfetuarFormaPagamento(NomeCartao, pValorCartao) then
    Exit;

  if not EfetuarFormaPagamento(NomeDinheiro, pValorDInheiro) then
    Exit;
end;

function TBematech.FinalizarCupom(mensagem: array of string): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;
end;

function TBematech.AbrirGaveta: Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;
end;

function TBematech.CancelarCupomAberto: Boolean;
var
  vStatusImpressora: Integer;
  vPermiteCancelar: Boolean;
  vCancelar: Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  vStatusImpressora := 0;
  vCancelar := False;
  vPermiteCancelar := False;
  if HouveErro( FProcBematech_FI_FlagsFiscais(vStatusImpressora) ) then
    Exit;

  if vStatusImpressora >= 128 then  // Mem�ria fiscal sem espa�o
    Dec(vStatusImpressora, 128);

  if vStatusImpressora >= 64 then // N�o utilizado
    Dec(vStatusImpressora, 64);

  if vStatusImpressora >= 32 then begin // Permite cancelar cupom fiscal
    Dec(vStatusImpressora, 32);
    vPermiteCancelar := True;
  end;

  if vStatusImpressora >= 16 then // N�o utilizado
    Dec(vStatusImpressora, 16);

  if vStatusImpressora >= 8 then // J� houve redu��o Z no dia
    Dec(vStatusImpressora, 8);

  if vStatusImpressora >= 4 then // Hor�rio de ver�o selecionado
    Dec(vStatusImpressora, 4);

  if vStatusImpressora >= 2 then begin // Fechamento das formas de pagamentos iniciado
    Dec(vStatusImpressora, 2);
    vCancelar := True;
  end;

  if vStatusImpressora >= 1 then begin // Cupom fiscal em aberto
    Dec(vStatusImpressora, 1);
    vCancelar := True;
  end;

  if not vPermiteCancelar then begin
    Exclamar('O cancelamento do cupomf fiscal n�o � poss�vel!');
    Exit;
  end
  else if not vCancelar then begin
    Exclamar('N�o foi poss�vel cancelar o cupom fiscal, verifique se existe realmente um cupom em aberto na impressora de cupom fiscal!');
    Exit;
  end;

  Result := not HouveErro( FProcBematech_FI_CancelaCupom );
end;

function TBematech.EmitirReducaoZ: Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

//FProcBematech_FI_ReducaoZ := GetProcAddress(FHandle, 'Bematech_FI_ReducaoZ');
//    if not Assigned(FProcBematech_FI_ReducaoZ) then begin
//      Exclamar('Falha ao chamar o m�todo "Bematech_FI_ReducaoZ" na DLL "' + dll + '"!');
//      Exit;
//    end;
//

//
//    FProcBematech_FI_LeituraMemoriaFiscalData := GetProcAddress(FHandle, 'Bematech_FI_LeituraMemoriaFiscalData');
//    if not Assigned(FProcBematech_FI_LeituraMemoriaFiscalData) then begin
//      Exclamar('Falha ao chamar o m�todo "Bematech_FI_LeituraMemoriaFiscalData" na DLL "' + dll + '"!');
//      Exit;
//    end;
end;

function TBematech.EmitirLeituraX: Boolean;
var
  vProcLeituraX: TBematech_FI_LeituraX;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  vProcLeituraX := GetProcAddress(FHandle, 'Bematech_FI_LeituraX');
  if not Assigned(vProcLeituraX) then begin
    Exclamar('Falha ao chamar o m�todo "Bematech_FI_LeituraX" na DLL "' + FDLL + '"!');
    Exit;
  end;

  Result := not HouveErro( vProcLeituraX );
end;

function TBematech.RetornoImpressora: RecRetornoImpressora;
var
  vNumeroECF: AnsiString;
  vNumeroCupom: AnsiString;
  vNumeroFabrica: AnsiString;
begin
  inherited;

  vNumeroCupom := AnsiString(Espacos(6));
  Result.Erro := HouveErro( FProcBematech_FI_NumeroCupom( vNumeroCupom ) );
  if Result.Erro then
    Exit;

  vNumeroECF := AnsiString(Espacos(6));
  Result.Erro := HouveErro( FProcBematech_FI_NumeroCaixa( vNumeroECF ) );
  if Result.Erro then
    Exit;

  vNumeroFabrica := AnsiString(Espacos(20));
  Result.Erro := HouveErro( FProcBematech_FI_NumeroSerieMFD( vNumeroFabrica ) );
  if Result.Erro then
    Exit;

  Result.ECF           := SFormatInt( string(vNumeroECF) );
  Result.COO           := SFormatInt( string(vNumeroCupom) );
  Result.NumeroFabrica := Trim(string(vNumeroFabrica));
end;

function TBematech.EmitirLeituraMemoriaFiscal(data_inicial, data_final: TDateTime): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;
end;

function TBematech.ImprimirConfiguracoes: Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;
end;

function TBematech.NomearTotalizadorNaoSujeitoIcms(indice: Integer; totalizador: string): Boolean;
var
  vProcNomeia: TBematech_FI_NomeiaTotalizadorNaoSujeitoIcms;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  vProcNomeia := GetProcAddress(FHandle, 'Bematech_FI_NomeiaTotalizadorNaoSujeitoIcms');
  if not Assigned(vProcNomeia) then begin
    Exclamar('Falha ao chamar o m�todo "Bematech_FI_NomeiaTotalizadorNaoSujeitoIcms" na DLL "' + FDLL + '"!');
    Exit;
  end;

  Result := not HouveErro( vProcNomeia(indice, AnsiString(totalizador)) );
end;

function TBematech.EfetuarSuprimento(valor_suprimento: Double): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;
end;

function TBematech.AdicionarAliquotaICMS(const pAliquota: Double): Boolean;
var
  vProgramaAliquota: TBematech_FI_ProgramaAliquota;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  vProgramaAliquota := GetProcAddress(FHandle, 'Bematech_FI_ProgramaAliquota');
  if not Assigned(vProgramaAliquota) then begin
    Exclamar('Falha ao chamar o m�todo "Bematech_FI_ProgramaAliquota" na DLL "' + FDLL + '"!');
    Exit;
  end;

  Result := not HouveErro( vProgramaAliquota(AnsiString(FormatFloat('0000', pAliquota * 100)), 0) );
end;

function TBematech.ProgramarFormaPagamento(forma_pagamento: string; vinculada: Boolean): Boolean;
var
  vVinculada: AnsiString;
  vProcForma: TBematech_FI_ProgramaFormaPagamentoMFD;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  vProcForma := GetProcAddress(FHandle, 'Bematech_FI_ProgramaFormaPagamentoMFD');
  if not Assigned(vProcForma) then begin
    Exclamar('Falha ao chamar o m�todo "Bematech_FI_ProgramaFormaPagamentoMFD" na DLL "' + FDLL + '"!');
    Exit;
  end;

  if vinculada then
    vVinculada := '1'
  else
    vVinculada := '0';

  Result := not HouveErro( vProcForma(AnsiString(forma_pagamento), AnsiString(vVinculada)) );
end;

function TBematech.AbrirComprovanteNaoFiscalVinculado(
  const pCondicao: string;
  const pValor: Double;
  const pNumeroCupom: Integer;
  const pNumeroParcelas: Integer
): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;
end;

function TBematech.FecharComprovanteNaoFiscalVinculado(mostrar_msg: Boolean): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;
end;

function TBematech.ImprimirTextoNaoFiscal(texto: string): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;
end;

function TBematech.AcionarGuilhotina: Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;
end;

function TBematech.CancelarUltimoCupom: Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;
end;

function TBematech.AbrirRecebimentoNaoFiscal(cpf: string; nome: string; endereco: string): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;
end;

function TBematech.EfetuarRecebimentoNaoFiscal(valorRecebimento: Double): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;
end;

function TBematech.SubTotalizarRecebimento: Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;
end;

function TBematech.TotalizarRecebimento: Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;
end;

function TBematech.EfetuarFormaPagamento(forma_pagamento: TFormaPagamento; valor: Double): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;
end;

function TBematech.FecharRecebimentoNaoFiscal(mensagem: string): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;
end;

function TBematech.CancelarRecebimentoNaoFiscal(cpf: string; nome: string; endereco: string): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;
end;

function TBematech.AbrirRelatorioGerencial: Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  Result := True;
end;

function TBematech.ImprimirTextoGerencial(texto: string): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  Result := True;
end;

function TBematech.FecharRelatorioGerencial: Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  Result := True;
end;

function TBematech.AtualizarInformacoesECF: Boolean;
var
  vNumeroCupom: AnsiString;
  vProc: TBematech_FI_NumeroCupom;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  vProc := GetProcAddress(FHandle, 'Bematech_FI_NumeroCupom');
  if not Assigned(vProc) then begin
    Exclamar('Falha ao chamar a fun��o "Bematech_FI_NumeroCupom"!');
    Exit;
  end;

  vNumeroCupom := AnsiString(Espacos(6));
  Result := not HouveErro( vProc(vNumeroCupom) );
  if Result then
    FCOO := StrToInt( string(vNumeroCupom) );
end;

//function TBematech.BuscarDadosUltimaReducao: TRetReducao;
//var
//  dt_mov: string;
//  dados: AnsiString;
//  Proc: TBematech_FI_DadosUltimaReducao;
//begin
//  inherited;
//  Result.buscou_dados := False;
//  if FSemComunicacao then
//    Exit;
//
//  Proc := GetProcAddress(FHandle, 'Bematech_FI_DadosUltimaReducao');
//  if Assigned(Proc) then begin
//    Exclamar('Falha ao chamar o m�todo "Bematech_FI_FechaRelatorioGerencial" na DLL "' + FDLL + '"!');
//    Exit;
//  end;
//
//  dados := AnsiString(Espacos(631));
//  Result.buscou_dados := not HouveErro( Proc(dados) );
//  if not Result.buscou_dados then
//    Exit;
//
//  Result.buscou_dados := true;
//  dt_mov := copy(string(dados), 596, 6);
//
//  if ValorInt(dt_mov) = 0 then
//    dt_mov := FormatDateTime('ddmmyy', date);
//
//  Result.data_movimento := StrToDate( copy(dt_mov, 1, 2) + '/' + copy(dt_mov, 3, 2) + '/20' + copy(dt_mov, 5, 2) );
//
//  Result.buscou_dados := True;
//end;

end.
