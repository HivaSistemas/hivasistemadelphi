unit _ComunicacaoECF2;

interface

uses
  Windows, Forms, _Conexao, _Biblioteca, SysUtils, Classes, Math, StrUtils, IniFiles, _ComunicacaoTEF, Winapi.ShellAPI,
  _ECF, _Bematech2, _Daruma2, _Epson2, _Sessao;

function AbrirCupom(const pCPF_CNPJ: string; const pNome: string; const pEndereco: string): Boolean;

function VenderItem(
  pCodigo: string;
  pDescricao: string;
  pMarca: string;
  pQuantidade: Double;
  pUnidade: string;
  pPrecoUnitario: Double;
  pTipoTributacao: string;
  pPercentualICMS: Double;
  pValorDesconto: Double
): Boolean;

function CancelarItem(nr_item: Integer): Boolean;

function IniciarFechamentoCupom(acrescimo: Double; desconto: Double): Boolean;

function TotalizarCupom(
  const pValorDinheiro: Double;
  const pValorCartao: Double;
  const pValorCobranca: Double;
  const pValorCheque: Double;
  const pValorCredito: Double;
  const pValorPOS: Double
): Boolean;

function FinalizarCupom(const pMensagem1: string; const pMensagem2: string): Boolean;

function CancelarCupomAberto: Boolean;
function CancelarUltimoCupom: boolean;

function AbrirGaveta: Boolean;
function FecharPorta: Boolean;
function ImprimirTextoNaoFiscal(texto: string): Boolean;
function FechaComprovanteNaoFiscalVinculado(mostrar_msg: Boolean = True): Boolean;
function EmitirLeituraX: Boolean;
function Leitura_Memoria_Fiscal(data_inicial, data_final: TDateTime): Boolean;
function EmitirReducaoZ: Boolean;
function RetornoImpressora(abertura_sistema: Boolean = False): RecRetornoImpressora;

function ImprimirRelatorioGerencial(texto: TStrings; fechar_relatorio: Boolean): Boolean;

procedure AcionaGuilhotina;
function ImprimirConfiguracoesImpressora: Boolean;

function getDataUltimaReducao: TDateTime;

function AddTotalizadorNaoFiscal(const pIndice: Integer; pTotalizador: string): Boolean;

function ImprimirComprovanteTEF(
  const pOperacao: string;
  const pNumeroCupom: Integer;
  const pValorOperacao: Double;
  pTexto_1_via: array of string;
  pTexto_2_via: array of string;
  pTelaUpdate: TForm
): Boolean;

function Suprimento(valor_suprimento: Double): boolean;

function AddAliquotaICMS(const pAliquota: Double): Boolean;

function FechaRelatorioGerencial: Boolean;

function AddFormaPagamento(const pFormaPagamento: string; const pVinculado: Boolean): Boolean;

function AbreRecebimentoNaoFiscalMFD(cpf, nome, endereco: string): Boolean;

function EfetuaRecebimentoNaoFiscalMFD(
  indice_totalizador: string;
  valorRecebimento: Double
): Boolean;

function TotalizarRecebimentoMFD: Boolean;

function FechaRecebimentoNaoFiscalMFD(mensagem: string): Boolean;

function CancelaRecebimentoNaoFiscalMFD(
  cpf, nome, endereco: string
): Boolean;

function SubTotalizaRecebimentoMFD: Boolean;
function BuscarVersaoDll: string;

implementation

function AbrirCupom(const pCPF_CNPJ: string; const pNome: string; const pEndereco: string): Boolean;
begin
  Result := Sessao.ECF.AbrirCupom(pCPF_CNPJ, pNome, pEndereco);
end;

function AbrirGaveta: Boolean;
begin
  Result := Sessao.ECF.AbrirGaveta();
end;

function VenderItem(
  pCodigo: string;
  pDescricao: string;
  pMarca: string;
  pQuantidade: Double;
  pUnidade: string;
  pPrecoUnitario: Double;
  pTipoTributacao: string;
  pPercentualICMS: Double;
  pValorDesconto: Double
): Boolean;
var
  vQtdeImprimir: Double;
  vDescontoDado: Double;
  vSaldoRestante: Double;
  vValorDescontoProporcional: Double;

  tipo_tributacao: string;
begin
  Result := False;
  vDescontoDado := 0;
  vSaldoRestante := pQuantidade;

  if Sessao.ECF is TBematech then begin
    if NFormat(pPercentualICMS) = NFormat(TECF(Sessao.ECF).PercentualICMS1) then
      tipo_tributacao := '01'
    else if NFormat(pPercentualICMS) = NFormat(TECF(Sessao.ECF).PercentualICMS2) then
      tipo_tributacao := '02'
    else if NFormat(pPercentualICMS) = NFormat(TECF(Sessao.ECF).PercentualICMS3) then
      tipo_tributacao := '03'
    else if NFormat(pPercentualICMS) = NFormat(TECF(Sessao.ECF).PercentualICMS4) then
      tipo_tributacao := '04'
    else if NFormat(pPercentualICMS) = NFormat(TECF(Sessao.ECF).PercentualICMS5) then
      tipo_tributacao := '05'
    else if NFormat(pPercentualICMS) = NFormat(TECF(Sessao.ECF).PercentualICMS6) then
      tipo_tributacao := '06'
    else if NFormat(pPercentualICMS) = NFormat(TECF(Sessao.ECF).PercentualICMS7) then
      tipo_tributacao := '07'
    else if NFormat(pPercentualICMS) = NFormat(TECF(Sessao.ECF).PercentualICMS8) then
      tipo_tributacao := '08'
    else
      tipo_tributacao := 'XX';
  end
  else begin

  end;

  while vSaldoRestante > 0 do begin
    if vSaldoRestante >= 10000 then begin
      vQtdeImprimir := 9000;
      vSaldoRestante := vSaldoRestante - 9000;

      vValorDescontoProporcional := pValorDesconto * vQtdeImprimir / pQuantidade;
      vDescontoDado := vDescontoDado + vValorDescontoProporcional;
    end
    else begin
      vQtdeImprimir := vSaldoRestante;
      vSaldoRestante := 0;

      vValorDescontoProporcional := pValorDesconto - vDescontoDado;
      vDescontoDado := pValorDesconto;
    end;

    Result :=
      Sessao.ECF.AdicionarItemCupom(
        pCodigo,
        pDescricao,
        pMarca,
        vQtdeImprimir,
        pPrecoUnitario,
        tipo_tributacao,
        pPercentualICMS,
        vValorDescontoProporcional,
        pUnidade
      );

    if not Result then
      Break;
  end;
end;

function CancelarItem(nr_item: Integer): Boolean;
begin
	Result := Sessao.ECF.CancelarItemCupom(nr_item);
end;

function CancelarUltimoCupom: boolean;
begin
  Result := Sessao.ECF.CancelarUltimoCupom;
end;

function CancelarCupomAberto: Boolean;
begin
  Result := Sessao.ECF.CancelarCupomAberto;
end;

function FecharPorta: Boolean;
begin
//  Result := ECF.FecharPorta;
end;

function FinalizarCupom(const pMensagem1: string; const pMensagem2: string): Boolean;
var
  i: Integer;
  vMensagem: array of string;

  procedure AdicionarMensagem(mensagem: string);
  begin
    if Length(vMensagem) >= 8 then
      Exit;

    if Trim(mensagem) = '' then
      Exit;

    SetLength(vMensagem, Length(vMensagem) + 1);
    vMensagem[High(vMensagem)] := Trim(Copy(mensagem, 1, 48));
  end;

begin
  try
    if pMensagem1 <> '' then
      AdicionarMensagem(pMensagem1);

    if pMensagem2 <> '' then
      AdicionarMensagem(pMensagem2);

    Result := Sessao.ECF.FinalizarCupom(vMensagem);
  except
    Result := False;
  end;
end;

function ImprimirTextoNaoFiscal(texto: string): Boolean;
begin
  Result := Sessao.ECF.ImprimirTextoNaoFiscal(texto);
end;

function FechaComprovanteNaoFiscalVinculado(mostrar_msg: Boolean = True): Boolean;
begin
  Result := Sessao.ECF.FecharComprovanteNaoFiscalVinculado(mostrar_msg);
end;

function EmitirLeituraX: Boolean;
begin
  Result := Sessao.ECF.EmitirLeituraX;
end;

function Leitura_Memoria_Fiscal(data_inicial, data_final: TDateTime): Boolean;
begin
  Result := True;

  if not Result then
    Informar('Erro na leitura da mem�ria fiscal!');
end;

function EmitirReducaoZ: Boolean;
begin
  Result := Sessao.ECF.EmitirReducaoZ;
  if not Result then
    Exclamar('Falha ao emitir a Redu��o Z!');
end;

function RetornoImpressora(abertura_sistema: Boolean = False): RecRetornoImpressora;
var
  tentativas: Integer;
begin
  tentativas := 1;
  Result := Sessao.ECF.RetornoImpressora;
  while Result.erro do begin
    Inc(tentativas);
    if tentativas = 10 then
      Break;

    Result := Sessao.ECF.RetornoImpressora;

    Sleep(1000);
  end;

  if abertura_sistema and not(Result.erro) and (Sessao.ECF is TEpson) then
    TEpson(Sessao.ECF).AbrirJornadaFiscal(Sessao.getDataHora);
end;

function IniciarFechamentoCupom(acrescimo: Double; desconto: Double): Boolean;
begin
  Result := Sessao.ECF.IniciarFechamentoCupom(acrescimo, desconto);
end;

function TotalizarCupom(
  const pValorDinheiro: Double;
  const pValorCartao: Double;
  const pValorCobranca: Double;
  const pValorCheque: Double;
  const pValorCredito: Double;
  const pValorPOS: Double
): Boolean;

begin
  Result := Sessao.ECF.TotalizarCupom(pValorDinheiro, pValorCartao, pValorCobranca, pValorCheque, pValorCredito, pValorPOS);
end;

function ImprimirRelatorioGerencial(texto: TStrings; fechar_relatorio: Boolean): Boolean;
var
  i: Integer;
  s: string;
begin
  Result := False;

  if texto.Count = 0 then
    Exit;

  if not fechar_relatorio then
    Sessao.ECF.AbrirRelatorioGerencial;

  for i := 0 to texto.Count - 1 do begin
    s := texto[i];
    if s = '' then
      s := ' ';

    Result := Sessao.ECF.ImprimirTextoGerencial( s );
    if not Result then begin
      Exit;
    end;
  end;

  if fechar_relatorio then begin
    Result := Sessao.ECF.FecharRelatorioGerencial;
  end;
end;

procedure AcionaGuilhotina;
begin
  Sessao.ECF.AcionarGuilhotina;
end;

function LimparTexto(s: string): string;
var
  i: integer;
begin
  Result := '';

  s := RetirarAcentos(s);

  for i := 1 to Length(s) do begin
    if
      CharInSet(
        s[i], [
          '0'..'9', 'A'..'Z', 'a'..'z', '!', '@', '#', '$', '%', '&', '(', ')',
          '*', '/' , '-', '_', '+', '-', '=', '.', ',', '<', '>', ':', ';', '?',
          '[', ']', '{', ' '
        ]
      )
    then begin
      Result := Result + s[i];
    end;
  end;
end;

function ImprimirConfiguracoesImpressora: Boolean;
begin
  Result := false;

end;

function AddTotalizadorNaoFiscal(const pIndice: Integer; pTotalizador: string): Boolean;
begin
  Result := Sessao.ECF.NomearTotalizadorNaoSujeitoIcms(pIndice, pTotalizador);
end;

function Suprimento(
  valor_suprimento: Double
): Boolean;
var
  ret_int: Integer;
begin
  Result := false;


  if not Result then
    Informar('Erro ao fazer o suprimento!');
end;

function AddAliquotaICMS(const pAliquota: Double): Boolean;
begin
  Result := Sessao.ECF.AdicionarAliquotaICMS(pAliquota);
end;

function FechaRelatorioGerencial: Boolean;
begin

end;

function AddFormaPagamento(const pFormaPagamento: string; const pVinculado: Boolean): Boolean;
begin
  Result := Sessao.ECF.ProgramarFormaPagamento(pFormaPagamento, pVinculado);
end;

function AbreRecebimentoNaoFiscalMFD(cpf, nome, endereco: string): Boolean;
begin
  Result := false;

end;

function  EfetuaRecebimentoNaoFiscalMFD(
  indice_totalizador: string;
  valorRecebimento: Double
): Boolean;
var
  ret_int: Integer;
begin
  Result := false;


end;

function  TotalizarRecebimentoMFD: Boolean;
var
  ret_int: Integer;
begin
  Result := true;

end;

function FechaRecebimentoNaoFiscalMFD(
  mensagem: string
): Boolean;
var
  ret_int: Integer;
begin
  Result := true;

end;

function CancelaRecebimentoNaoFiscalMFD(
  cpf, nome, endereco: string
): Boolean;
var
  ret_int: Integer;
begin
  Result := true;

end;

function  SubTotalizaRecebimentoMFD: Boolean;
var
  ret_int: Integer;
begin
  Result := true;

end;

function  BuscarVersaoDll: string;
var
  cVersao: AnsiString;
  iConta: Integer;
  iRetorno: Integer;
begin

end;

function ImprimirComprovanteTEF(
  const pOperacao: string;
  const pNumeroCupom: Integer;
  const pValorOperacao: Double;
  pTexto_1_via: array of string;
  pTexto_2_via: array of string;
  pTelaUpdate: TForm
): Boolean;
var
  vImprimiu: Boolean;
  vNomeCondicao: string;
  vQtdeTentativas: Integer;
  vTentarNovamente: Boolean;

  procedure UpdateTela;
  begin
    pTelaUpdate.Update;
    Application.ProcessMessages;
  end;

  function Imprimir1ViaComprovanteVinculado: Boolean;
  var
    i: Integer;
  begin
    Result := True;
    for i := Low(pTexto_1_via) to High(pTexto_1_via) do begin
      Result := ImprimirTextoNaoFiscal(pTexto_1_via[i]);

      if not Result then
        Break;
    end;
  end;

  function Imprimir2ViaComprovanteVinculado: Boolean;
  var
    i: Integer;
  begin
    Result := True;
    for i := Low(pTexto_2_via) to High(pTexto_2_via) do begin
      Result := ImprimirTextoNaoFiscal(pTexto_2_via[i]);

      if not Result then
        Break;
    end;
  end;

  function Imprimir1ViaComprovanteComLeituraX: Boolean;
  var
    i: Integer;
    strs: TStrings;
  begin
    UpdateTela;
    Result := False;
    strs := TStringList.Create;
//    SetForegroundWindow(telaSobrepor);
    for i := Low(pTexto_1_via) to High(pTexto_1_via) do begin
//      if ECF is TBematechNaoFiscal then begin
//        Result := ImprimirTextoNaoFiscal(texto_1_via[i]);
//        if not Result then begin
//          Exit;
//        end;
//      end;
//      else
        strs.Add(pTexto_1_via[i]);
    end;

    Result := ImprimirRelatorioGerencial(strs, False);
  end;

  function ImprimirEspacos(qtd: integer; vinculado: Boolean): boolean;
  var
    x: Integer;
    ret_int: Integer;
  begin
    Result := False;
    for x := 1 to qtd do begin
      Result := Sessao.ECF.ImprimirTextoNaoFiscal(' ');
      if not Result then begin
        Break;
      end;
    end;
  end;

  function ImprimirTodos(vinculado: Boolean): Boolean;
  begin
    Result := False;

    // Impress�o da 1� Via do comprovante
    if vinculado then begin
      Result := Imprimir1ViaComprovanteVinculado;
      if not Result then
        Exit;
    end
    else begin

    end;

    // Dando 10 espa�os
    Result := ImprimirEspacos(10, vinculado);
    if not Result then
      Exit;

    // Aguardando 5 segundo para que o usu�rio corte o papel manualmente.
    if Sessao.ECF.AcionarGuilhotina then
      Sleep(5000)
    else
      AcionaGuilhotina;

    if vinculado then begin
      Result := Imprimir2ViaComprovanteVinculado;
      if not Result then
        Exit;

//      Result := EmitirLeituraX;
      if not Result then
        Exit;

    end
    else
      Result := True;
  end;

begin
  UpdateTela;
  vImprimiu := False;
	vQtdeTentativas := 0;
  vTentarNovamente := False;

  _Biblioteca.BlockInput(True);

  if pOperacao = 'ADM' then
    vTentarNovamente := True
  else begin
    vNomeCondicao := Ifthen(pOperacao = 'CHQ', TECF(Sessao.ECF).NomeCheque, TECF(Sessao.ECF).NomeCartao);

    if not Sessao.ECF.AbrirComprovanteNaoFiscalVinculado(vNomeCondicao, pValorOperacao, pNumeroCupom, 1) then
      Exit;

    vImprimiu := ImprimirTodos(True);
    if not vImprimiu then begin
      _Biblioteca.BlockInput(False);
      if Perguntar('Impressora n�o responde. Tentar imprimir novamente?') then begin
        _Biblioteca.BlockInput(True);
        if not FechaComprovanteNaoFiscalVinculado(False) then begin
          while vQtdeTentativas < 10 do begin
            Inc(vQtdeTentativas);
            if FechaComprovanteNaoFiscalVinculado(False) then
              Break;
          end;

          _Biblioteca.BlockInput(True);
        end;
        vTentarNovamente := True;
      end;
      _Biblioteca.BlockInput(True);
    end;
  end;

  // Tentando novamente, caso necess�rio
  while vTentarNovamente do begin
    UpdateTela;

    vTentarNovamente := False;
    vImprimiu := ImprimirTodos(False);

    if not vImprimiu then begin
      _Biblioteca.BlockInput(False);

      if Perguntar('Impressora n�o responde. Tentar imprimir novamente?') then begin
        _Biblioteca.BlockInput(true);
        vQtdeTentativas := 0;
        while vQtdeTentativas < 10 do begin
          Inc(vQtdeTentativas);

          if FechaComprovanteNaoFiscalVinculado(False) then
            Break;
        end;

        _Biblioteca.BlockInput(True);
        vTentarNovamente := True;
      end;
      _Biblioteca.BlockInput(True);
    end;
  end;
  Result := vImprimiu;
  _Biblioteca.BlockInput(False);
end;

function getDataUltimaReducao: TDateTime;
begin
  Result := 0;
//  Result := ECF.
end;

end.
