unit _Epson2;

interface

uses
  System.Classes, System.SysUtils, Winapi.Windows, _ECF, _Biblioteca, System.Math;

type
  TEpson = class(TECF, IECF)
  protected
    function IniciarComunicacao(porta: Integer): Boolean; override;
  private
    FProcEPSON_Obter_Estado_ImpressoraEX: function(szEstadoImpressora:PAnsiChar; szEstadoFiscal:PAnsiChar; szRetornoComando:PAnsiChar; szMsgErro:PAnsiChar): Integer; stdcall;
    FProcEPSON_Fiscal_Abrir_Cupom: function(pszCNPJ:PAnsiChar; pszRazaoSocial:PAnsiChar; pszEndereco1:PAnsiChar; pszEndereco2:PAnsiChar; dwPosicao: Integer): Integer; stdcall;

    FProcEPSON_Fiscal_Vender_Item_AD:
      function(
        pszCodigo: PAnsiChar;
        pszDescricao: PAnsiChar;
        pszQuantidade: PAnsiChar;
        dwQuantCasasDecimais: Integer;
        pszUnidade: PAnsiChar;
        pszPrecoUnidade: PAnsiChar;
        dwPrecoCasasDecimais: Integer;
        pszAliquotas: PAnsiChar;
        dwLinhas: Integer;
        dwRoundTruncate: Integer;
        dwOwnManufactured: Integer
      ): Integer; stdcall;

    FProcEPSON_Fiscal_Cancelar_Item: function(pszNumeroItem:PAnsiChar): Integer; stdcall;

    FProcEPSON_Fiscal_Desconto_Acrescimo_Item: function (pszValor:PAnsiChar; dwCasasDecimais: Integer; bDesconto:LongBool; bPercentagem:LongBool): Integer; stdcall;
    FProcEPSON_Fiscal_Desconto_Acrescimo_Subtotal: function(pszValor:PAnsiChar; dwCasasDecimais: Integer; bDesconto:LongBool; bPercentagem:LongBool): Integer; stdcall;
    FProcEPSON_Fiscal_Pagamento: function(pszNumeroPagamento:PAnsiChar; pszValorPagamento:PAnsiChar; dwCasasDecimais: Integer; pszDescricao1:PAnsiChar; pszDescricao2:PAnsiChar): Integer; stdcall;
    FProcEPSON_Fiscal_Imprimir_MensagemEX: function (pszTextLine:PAnsiChar): Integer; stdcall;
    FProcEPSON_Fiscal_Fechar_Cupom: function(bCortarPapel:LongBool; bAdicional:LongBool): Integer; stdcall;
    FProcEPSON_Obter_Estado_Cupom: function(pszEstado:PAnsiChar): Integer; stdcall;

    FProcEPSON_Fiscal_Cancelar_CupomEX: function(): Integer; stdcall;
    FProcEPSON_NaoFiscal_Fechar_CCD: function(bCortarPapel: LongBool): Integer; stdcall;
    FProcEPSON_NaoFiscal_Fechar_Relatorio_Gerencial: function(bCortarPapel: LongBool): Integer; stdcall;
    FProcEPSON_NaoFiscal_Cancelar_ComprovanteEX: function(): Integer; stdcall;

    FProcEPSON_Obter_Contadores: function(pszDados: PAnsiChar): Integer; stdcall;
    FProcEPSON_Obter_Numero_ECF_Loja: function(pszDados:PAnsiChar): Integer; stdcall;

    FProcEPSON_NaoFiscal_Abrir_CCD: function(pszNumeroPagamento:PAnsiChar; pszValor:PAnsiChar; dwCasasDecimais: Integer; pszParcelas:PAnsiChar): Integer; stdcall;
    FProcEPSON_NaoFiscal_Imprimir_LinhaEX: function(pszLinha:PAnsiChar): Integer; stdcall;

    FProcEPSON_NaoFiscal_Abrir_Relatorio_Gerencial: function(pszNumeroRelatorio: PAnsiChar): Integer; stdcall;
    FProcEPSON_NaoFiscal_Imprimir_Linha: function(pszLinha:PAnsiChar): Integer; stdcall;

    FProcEPSON_NaoFiscal_Abrir_Comprovante: function(pszCNPJ:PAnsiChar; pszRazaoSocial:PAnsiChar; pszEndereco1:PAnsiChar; pszEndereco2:PAnsiChar; dwPosicao: Integer): Integer; stdcall;
    FProcEPSON_NaoFiscal_Vender_Item: function(pszNumeroOperacao:PAnsiChar; pszValorOperacao:PAnsiChar; dwCasasDecimais: Integer): Integer; stdcall;
    FProcEPSON_NaoFiscal_Pagamento: function(pszNumeroPagamento:PAnsiChar; pszValorPagamento:PAnsiChar; dwCasasDecimais: Integer; pszDescricao1:PAnsiChar; pszDescricao2:PAnsiChar): Integer; stdcall;
    FProcEPSON_NaoFiscal_Fechar_Comprovante: function(bCortarPapel:LongBool): Integer; stdcall;

    function HouveErro(retorno: Integer; mostrar_msg: Boolean = True): Boolean;
  public

    procedure AbrirJornadaFiscal(data_atual: TDateTime);

    function AbrirCupom(const pCPF_CNPJ: string; const pNome: string; const pEndereco: string): Boolean; override;

    function AdicionarItemCupom(
      codigo: string;
      descricao: string;
      marca: string;
      quantidade: Double;
      preco_unitario: Double;
      tipo_tributacao: string;
      perc_icms: Double;
      valor_desconto: Double;
      unidade: string
    ): Boolean; override;

    function CancelarItemCupom(numero_item: Integer): Boolean; override;

    function IniciarFechamentoCupom(acrescimo: Double; desconto: Double): Boolean; override;

    function TotalizarCupom(
      const pValorDinheiro: Double;
      const pValorCartao: Double;
      const pValorCobranca: Double;
      const pValorCheque: Double;
      const pValorCredito: Double;
      const pValorPOS: Double
    ): Boolean; override;

    function FinalizarCupom(mensagem: array of string): Boolean; override;
    function AbrirGaveta: Boolean; override;
    function CancelarCupomAberto: Boolean; override;
    function EmitirReducaoZ: Boolean; override;
    function EmitirLeituraX: Boolean; override;
    function RetornoImpressora: RecRetornoImpressora; override;
    function EmitirLeituraMemoriaFiscal(data_inicial, data_final: TDateTime): Boolean; override;
    function ImprimirConfiguracoes: Boolean; override;
    function NomearTotalizadorNaoSujeitoIcms(indice: Integer; totalizador: string): Boolean; override;
    function EfetuarSuprimento(valor_suprimento: Double): Boolean; override;
    function AdicionarAliquotaICMS(const pAliquota: Double): Boolean; override;
    function ProgramarFormaPagamento(forma_pagamento: string; vinculada: Boolean): Boolean; override;

    function AbrirComprovanteNaoFiscalVinculado(
      const pCondicao: string;
      const pValor: Double;
      const pNumeroCupom: Integer;
      const pNumeroParcelas: Integer
    ): Boolean; override;

    function FecharComprovanteNaoFiscalVinculado(mostrar_msg: Boolean): Boolean; override;

    function ImprimirTextoNaoFiscal(texto: string): Boolean; override;
    function AcionarGuilhotina: Boolean; override;
    function CancelarUltimoCupom: Boolean; override;

    function AbrirRecebimentoNaoFiscal(cpf: string; nome: string; endereco: string): Boolean; override;
    function EfetuarRecebimentoNaoFiscal(valorRecebimento: Double): Boolean; override;
    function SubTotalizarRecebimento: Boolean; override;
    function TotalizarRecebimento: Boolean; override;
    function EfetuarFormaPagamento(forma_pagamento: TFormaPagamento; valor: Double): Boolean; override;
    function FecharRecebimentoNaoFiscal(mensagem: string): Boolean; override;
    function CancelarRecebimentoNaoFiscal(cpf: string; nome: string; endereco: string): Boolean; override;

    function AbrirRelatorioGerencial: Boolean; override;
    function ImprimirTextoGerencial(texto: string): Boolean; override;
    function FecharRelatorioGerencial: Boolean; override;
//    function BuscarDadosUltimaReducao: TRetReducao; override;
  end;

implementation

{ TEpson }

type
  TEPSON_Serial_Abrir_Porta = function(dwVelocidade: Integer; wPorta: Integer): Integer; stdcall;
  TEPSON_Obter_Data_Hora_Jornada = function(pszDataHora:PAnsiChar): Integer; stdcall;
  TEPSON_RelatorioFiscal_Abrir_Jornada = function(): Integer; stdcall;
  TEPSON_RelatorioFiscal_LeituraX = function(): Integer; stdcall;
  TEPSON_RelatorioFiscal_RZEX = function(dwHorarioVerao: Integer): Integer; stdcall;
  TEPSON_Fiscal_Cancelar_CupomEX = function(): Integer; stdcall;
  TEPSON_RelatorioFiscal_Leitura_MF = function(pszInicio:PAnsiChar; pszFim:PAnsiChar; dwTipoImpressao: Integer; pszBuffer:PAnsiChar; pszArquivo:PAnsiChar; pdwTamanhoBuffer:PInteger; dwTamBuffer: Integer): Integer; stdcall;
  TEPSON_Config_Totalizador_NaoFiscal = function(pszDescricao:PAnsiChar; Var pdwNumeroTotalizador: Integer): Integer; stdcall;
  TEPSON_NaoFiscal_Fundo_Troco = function(pszValor:PAnsiChar; dwCasasDecimais: Integer): Integer; stdcall;
  TEPSON_Config_Aliquota = function(pszTaxa:PAnsiChar; bTipoISS:LongBool): Integer; stdcall;
  TEPSON_Config_Forma_PagamentoEX = function(bVinculado:LongBool; pszDescricao:PAnsiChar): Integer; stdcall;
  TEPSON_Obter_Dados_Ultima_RZ = function(pszLastRZData:PAnsiChar): Integer; stdcall;

function TEpson.HouveErro(retorno: Integer; mostrar_msg: Boolean = True): Boolean;
var
  szMsgErro: AnsiString;
  szEstadoFiscal: AnsiString;
  szRetornoComando: AnsiString;
  szEstadoImpressora: AnsiString;

  procedure GerarErro(mensagem: string);
  begin
    BlockInput(False);

    if mostrar_msg then
      Exclamar(mensagem);

    Result := True;
  end;

begin
  Result := False;
  szMsgErro := AnsiString(StringOfChar(' ', 101));
  szEstadoFiscal := AnsiString(StringOfChar(' ', 17));
  szRetornoComando := AnsiString(StringOfChar(' ', 5));
  szEstadoImpressora := AnsiString(StringOfChar(' ', 17));

  FProcEPSON_Obter_Estado_ImpressoraEX(PAnsiChar(szEstadoImpressora), PAnsiChar(szEstadoFiscal), PAnsiChar(szRetornoComando), PAnsiChar(szMsgErro));
  if Pos('Resultado sem erro', string(szMsgErro)) = 0 then
    GerarErro(string(szMsgErro));
end;

function TEpson.ImprimirConfiguracoes: Boolean;
begin
  inherited;
  Informar('Recurso n�o dispon�vel para esta impressora.');
  Result := False;
end;

function TEpson.IniciarComunicacao(porta: Integer): Boolean;
var
  ProcEPSON_Serial_Abrir_Porta: TEPSON_Serial_Abrir_Porta;
begin
  inherited;
  Result := False;
  try
    if not FileExists('InterfaceEpson.dll') then begin
      Exclamar('A DLL "InterfaceEpson.dll" n�o foi encontrada na pasta do execut�vel!');
      Exit;
    end;

    (* Tentar carregar a DLL de maneira Din�mica *)
    FHandle := LoadLibrary('InterfaceEpson.dll');
    if FHandle = 0 then begin
      Exclamar('Falha ao carregar a DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FProcEPSON_Obter_Estado_ImpressoraEX := GetProcAddress(FHandle, 'EPSON_Obter_Estado_ImpressoraEX');
    if not Assigned(FProcEPSON_Obter_Estado_ImpressoraEX) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_Obter_Estado_ImpressoraEX" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    (* Abre a porta de comunica��o com o ECF *)
    ProcEPSON_Serial_Abrir_Porta := GetProcAddress(FHandle, 'EPSON_Serial_Abrir_Porta');
    if not Assigned(ProcEPSON_Serial_Abrir_Porta) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_Serial_Abrir_Porta" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    if HouveErro(ProcEPSON_Serial_Abrir_Porta(115200, IfThen(porta = 9, 0, porta))) then begin
      Exclamar('Falha ao abrir porta de comunica��o com o ECF!');
      Exit;
    end;

    (*--------------- CARREGANDO OS M�TODOS MAIS UTILIZADOS NA MEM�RIA -----------------------*)
    FProcEPSON_Fiscal_Abrir_Cupom := GetProcAddress(FHandle, 'EPSON_Fiscal_Abrir_Cupom');
    if not Assigned(FProcEPSON_Fiscal_Abrir_Cupom) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_Fiscal_Abrir_Cupom" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FProcEPSON_Fiscal_Vender_Item_AD := GetProcAddress(FHandle, 'EPSON_Fiscal_Vender_Item_AD');
    if not Assigned(FProcEPSON_Fiscal_Vender_Item_AD) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_Fiscal_Vender_Item_AD" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FProcEPSON_Fiscal_Cancelar_Item := GetProcAddress(FHandle, 'EPSON_Fiscal_Cancelar_Item');
    if not Assigned(FProcEPSON_Fiscal_Cancelar_Item) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_Fiscal_Cancelar_Item" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FProcEPSON_Fiscal_Desconto_Acrescimo_Item := GetProcAddress(FHandle, 'EPSON_Fiscal_Desconto_Acrescimo_Item');
    if not Assigned(FProcEPSON_Fiscal_Desconto_Acrescimo_Item) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_Fiscal_Desconto_Acrescimo_Item" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FProcEPSON_Fiscal_Desconto_Acrescimo_Subtotal := GetProcAddress(FHandle, 'EPSON_Fiscal_Desconto_Acrescimo_Subtotal');
    if not Assigned(FProcEPSON_Fiscal_Desconto_Acrescimo_Subtotal) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_Fiscal_Desconto_Acrescimo_Subtotal" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FProcEPSON_Fiscal_Pagamento := GetProcAddress(FHandle, 'EPSON_Fiscal_Pagamento');
    if not Assigned(FProcEPSON_Fiscal_Pagamento) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_Fiscal_Pagamento" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FProcEPSON_Fiscal_Imprimir_MensagemEX := GetProcAddress(FHandle, 'EPSON_Fiscal_Imprimir_MensagemEX');
    if not Assigned(FProcEPSON_Fiscal_Imprimir_MensagemEX) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_Fiscal_Imprimir_MensagemEX" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FProcEPSON_Fiscal_Fechar_Cupom := GetProcAddress(FHandle, 'EPSON_Fiscal_Fechar_Cupom');
    if not Assigned(FProcEPSON_Fiscal_Fechar_Cupom) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_Fiscal_Fechar_Cupom" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FProcEPSON_Obter_Estado_Cupom := GetProcAddress(FHandle, 'EPSON_Obter_Estado_Cupom');
    if not Assigned(FProcEPSON_Obter_Estado_Cupom) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_Obter_Estado_Cupom" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FProcEPSON_Fiscal_Cancelar_CupomEX := GetProcAddress(FHandle, 'EPSON_Fiscal_Cancelar_CupomEX');
    if not Assigned(FProcEPSON_Fiscal_Cancelar_CupomEX) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_Fiscal_Cancelar_CupomEX" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FProcEPSON_NaoFiscal_Fechar_CCD := GetProcAddress(FHandle, 'EPSON_NaoFiscal_Fechar_CCD');
    if not Assigned(FProcEPSON_NaoFiscal_Fechar_CCD) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_NaoFiscal_Fechar_CCD" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FProcEPSON_NaoFiscal_Fechar_Relatorio_Gerencial := GetProcAddress(FHandle, 'EPSON_NaoFiscal_Fechar_Relatorio_Gerencial');
    if not Assigned(FProcEPSON_NaoFiscal_Fechar_Relatorio_Gerencial) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_NaoFiscal_Fechar_Relatorio_Gerencial" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FProcEPSON_NaoFiscal_Cancelar_ComprovanteEX := GetProcAddress(FHandle, 'EPSON_NaoFiscal_Cancelar_ComprovanteEX');
    if not Assigned(FProcEPSON_NaoFiscal_Cancelar_ComprovanteEX) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_NaoFiscal_Cancelar_ComprovanteEX" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FProcEPSON_Obter_Contadores := GetProcAddress(FHandle, 'EPSON_Obter_Contadores');
    if not Assigned(FProcEPSON_Obter_Contadores) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_Obter_Contadores" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FProcEPSON_Obter_Numero_ECF_Loja := GetProcAddress(FHandle, 'EPSON_Obter_Numero_ECF_Loja');
    if not Assigned(FProcEPSON_Obter_Numero_ECF_Loja) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_Obter_Numero_ECF_Loja" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FProcEPSON_NaoFiscal_Abrir_CCD := GetProcAddress(FHandle, 'EPSON_NaoFiscal_Abrir_CCD');
    if not Assigned(FProcEPSON_NaoFiscal_Abrir_CCD) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_NaoFiscal_Abrir_CCD" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FProcEPSON_NaoFiscal_Imprimir_LinhaEX := GetProcAddress(FHandle, 'EPSON_NaoFiscal_Imprimir_LinhaEX');
    if not Assigned(FProcEPSON_NaoFiscal_Imprimir_LinhaEX) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_NaoFiscal_Imprimir_LinhaEX" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FProcEPSON_NaoFiscal_Abrir_Relatorio_Gerencial := GetProcAddress(FHandle, 'EPSON_NaoFiscal_Abrir_Relatorio_Gerencial');
    if not Assigned(FProcEPSON_NaoFiscal_Abrir_Relatorio_Gerencial) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_NaoFiscal_Abrir_Relatorio_Gerencial" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FProcEPSON_NaoFiscal_Imprimir_Linha := GetProcAddress(FHandle, 'EPSON_NaoFiscal_Imprimir_Linha');
    if not Assigned(FProcEPSON_NaoFiscal_Imprimir_Linha) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_NaoFiscal_Imprimir_Linha" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FProcEPSON_NaoFiscal_Abrir_Comprovante := GetProcAddress(FHandle, 'EPSON_NaoFiscal_Abrir_Comprovante');
    if not Assigned(FProcEPSON_NaoFiscal_Abrir_Comprovante) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_NaoFiscal_Abrir_Comprovante" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FProcEPSON_NaoFiscal_Vender_Item := GetProcAddress(FHandle, 'EPSON_NaoFiscal_Vender_Item');
    if not Assigned(FProcEPSON_NaoFiscal_Vender_Item) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_NaoFiscal_Vender_Item" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FProcEPSON_NaoFiscal_Pagamento := GetProcAddress(FHandle, 'EPSON_NaoFiscal_Pagamento');
    if not Assigned(FProcEPSON_NaoFiscal_Pagamento) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_NaoFiscal_Pagamento" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FProcEPSON_NaoFiscal_Fechar_Comprovante := GetProcAddress(FHandle, 'EPSON_NaoFiscal_Fechar_Comprovante');
    if not Assigned(FProcEPSON_NaoFiscal_Fechar_Comprovante) then begin
      Exclamar('Falha ao chamar o m�todo "EPSON_NaoFiscal_Fechar_Comprovante" na DLL "InterfaceEpson.dll"!');
      Exit;
    end;

    FSemComunicacao := False;
    FPorta := porta;
    Result := True;
  except on e: Exception do
      Exclamar(e.Message);
  end;
end;

procedure TEpson.AbrirJornadaFiscal(data_atual: TDateTime);
var
  nr_str: AnsiString;
  ProcEPSON_Obter_Data_Hora_Jornada: TEPSON_Obter_Data_Hora_Jornada;
  ProcEPSON_RelatorioFiscal_Abrir_Jornada: TEPSON_RelatorioFiscal_Abrir_Jornada;
begin
  ProcEPSON_Obter_Data_Hora_Jornada := GetProcAddress(FHandle, 'EPSON_Obter_Data_Hora_Jornada');
  if not Assigned(ProcEPSON_Obter_Data_Hora_Jornada) then begin
    Exclamar('Falha ao chamar o m�todo "EPSON_Obter_Data_Hora_Jornada " na DLL "InterfaceEpson.dll"!');
    Exit;
  end;

  ProcEPSON_RelatorioFiscal_Abrir_Jornada := GetProcAddress(FHandle, 'EPSON_RelatorioFiscal_Abrir_Jornada');
  if not Assigned(ProcEPSON_RelatorioFiscal_Abrir_Jornada) then begin
    Exclamar('Falha ao chamar o m�todo "EPSON_RelatorioFiscal_Abrir_Jornada " na DLL "InterfaceEpson.dll"!');
    Exit;
  end;

  nr_str := AnsiString(StringOfChar(' ', 15));
  if HouveErro( ProcEPSON_Obter_Data_Hora_Jornada( PAnsiChar(nr_str)) ) then
    Exit;

  nr_str := Copy(nr_str, 1, 2) + '/' + Copy(nr_str, 3, 2) + '/' + Copy(nr_str, 5, 4);
  if string(nr_str) <> DFormat(data_atual) then
    HouveErro( ProcEPSON_RelatorioFiscal_Abrir_Jornada() );
end;

function TEpson.AbrirRelatorioGerencial: Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  Result := not HouveErro( FProcEPSON_NaoFiscal_Abrir_Relatorio_Gerencial('1') );
end;

function TEpson.AbrirCupom(const pCPF_CNPJ: string; const pNome: string; const pEndereco: string): Boolean;
var
  ret_int: Integer;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  if pNome = '' then
    ret_int := FProcEPSON_Fiscal_Abrir_Cupom( PAnsiChar(AnsiString(Copy(pCPF_CNPJ, 1, 20))), '', '', '', 2 )
  else
    ret_int := FProcEPSON_Fiscal_Abrir_Cupom( PAnsiChar(AnsiString(Copy(pCPF_CNPJ,1, 20))), PAnsiChar(AnsiString(Copy(pNome, 1, 30))), '', '', 2 );

  Result := not HouveErro(ret_int);
end;

function TEpson.AdicionarAliquotaICMS(const pAliquota: Double): Boolean;
var
  Proc: TEPSON_Config_Aliquota;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  Proc := GetProcAddress(FHandle, 'EPSON_Config_Aliquota');
  if not Assigned(Proc) then begin
    Exclamar('Falha ao chamar o m�todo "EPSON_Config_Aliquota " na DLL "InterfaceEpson.dll"!');
    Exit;
  end;

  Result := not HouveErro( Proc(PAnsiChar(AnsiString(FormatFloat('#0', pAliquota * 100))), False) );
end;

function TEpson.AdicionarItemCupom(
  codigo: string;
  descricao: string;
  marca: string;
  quantidade: Double;
  preco_unitario: Double;
  tipo_tributacao: string;
  perc_icms: Double;
  valor_desconto: Double;
  unidade: string
): Boolean;
var
  tipo_icms: string;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  if perc_icms > 0 then
    tipo_icms := LPad(RetornaNumeros(NFormat(perc_icms, 2)), 4, '0')
  else if tipo_tributacao = 'II' then
    tipo_icms := 'I'
  else if tipo_tributacao = 'FF' then
    tipo_icms := 'F';

  if Length(codigo) < 8 then
    codigo := LPad(codigo, 8, '0');

  Result :=
    not HouveErro(
      FProcEPSON_Fiscal_Vender_Item_AD(
        PAnsiChar(AnsiString(codigo)),
        PAnsiChar(AnsiString(descricao)),
        PAnsiChar(AnsiString(RetornaNumeros(NFormat(quantidade)))),
        2,
        PAnsiChar(AnsiString(Copy(Unidade, 1, 2))),
        PAnsiChar(AnsiString(LPad(RetornaNumeros(NFormat(preco_unitario, FQtdeDecimaisPrecoVenda)), 8, '0'))),
        FQtdeDecimaisPrecoVenda,
        PAnsiChar(AnsiString(tipo_icms)),
        1,
        1, // 1 - Trunca; 2 - Arrendonda
        1  // 1 - Fabrica��o por terceiros; 2 - Fabrica��o pr�pria
      )
    );

  if (not Result) or (NFormat(valor_desconto) = '0,00') then
    Exit;

  Result :=
    not HouveErro(
      FProcEPSON_Fiscal_Desconto_Acrescimo_Item(
      PAnsiChar(AnsiString(LPad(RetornaNumeros(NFormat(valor_desconto)), 11, '0'))),
      2,
      True,
      False
    )
   );
end;

//function TEpson.BuscarDadosUltimaReducao: TRetReducao;
//var
//  dt_mov: string;
//  dados: AnsiString;
//  Proc: TEPSON_Obter_Dados_Ultima_RZ;
//begin
//  inherited;
//  Result.buscou_dados := False;
//  if FSemComunicacao then
//    Exit;
//
//  @Proc := GetProcAddress(FHandle, 'EPSON_Obter_Dados_Ultima_RZ');
//  if not Assigned(Proc) then begin
//    Exclamar('Falha ao chamar o m�todo "EPSON_Obter_Dados_Ultima_RZ " na DLL "InterfaceEpson.dll"!');
//    Exit;
//  end;
//
//  dados := AnsiString(Espacos(1168));
//  Result.buscou_dados := not HouveErro( Proc(PAnsiChar(dados)) );
//  if not Result.buscou_dados then
//    Exit;
//
//  dt_mov := copy(string(dados), 1, 8);
//  if ValorInt(dt_mov) = 0 then
//    dt_mov := FormatDateTime('ddmmyy', date);
//
//  Result.data_movimento := StrToDate( copy(dt_mov, 1, 2) + '/' + copy(dt_mov, 3, 2) + '/' + copy(dt_mov, 5, 4) );
//end;

function TEpson.CancelarItemCupom(numero_item: Integer): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  Result := not HouveErro( FProcEPSON_Fiscal_Cancelar_Item( PAnsiChar(AnsiString(IntToStr(numero_item)))) );
end;

function TEpson.IniciarFechamentoCupom(acrescimo: Double; desconto: Double): Boolean;
var
  saldo: Currency;
  tipo_acao: string;
begin
  inherited;
  Result := True;
  if FSemComunicacao then
    Exit;

  try
    saldo := acrescimo - desconto;
    if NFormat(saldo) = '0,00' then
      Exit;

    if saldo >= 0 then
      tipo_acao := 'A$'
    else
      tipo_acao := 'D$';

    Result := not HouveErro( FProcEPSON_Fiscal_Desconto_Acrescimo_Subtotal(PAnsiChar(AnsiString(RetornaNumeros(NFormat(saldo)))), 2, tipo_acao = 'D', False) );
  except
    Result := False;
  end;
end;

function TEpson.NomearTotalizadorNaoSujeitoIcms(indice: Integer; totalizador: string): Boolean;
var
  indice_auxiliar: Integer;
  Proc: TEPSON_Config_Totalizador_NaoFiscal;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  @Proc := GetProcAddress(FHandle, 'EPSON_Config_Totalizador_NaoFiscal');
  if not Assigned(Proc) then begin
    Exclamar('Falha ao chamar o m�todo "EPSON_Config_Totalizador_NaoFiscal " na DLL "InterfaceEpson.dll"!');
    Exit;
  end;

  Result := not HouveErro( Proc(PAnsiChar(AnsiString(totalizador)), indice_auxiliar) );
end;

function TEpson.ProgramarFormaPagamento(forma_pagamento: string; vinculada: Boolean): Boolean;
var
  Proc: TEPSON_Config_Forma_PagamentoEX;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  @Proc := GetProcAddress(FHandle, 'EPSON_Config_Forma_PagamentoEX');
  if not Assigned(Proc) then begin
    Exclamar('Falha ao chamar o m�todo "EPSON_Config_Forma_PagamentoEX " na DLL "InterfaceEpson.dll"!');
    Exit;
  end;

  Result := not HouveErro( Proc(vinculada, PAnsiChar(AnsiString(forma_pagamento))) );
end;

function TEpson.TotalizarCupom(
  const pValorDinheiro: Double;
  const pValorCartao: Double;
  const pValorCobranca: Double;
  const pValorCheque: Double;
  const pValorCredito: Double;
  const pValorPOS: Double
): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  if pValorCheque > 0 then begin
    Result := not HouveErro( FProcEPSON_Fiscal_Pagamento(PAnsiChar(AnsiString(NomeCheque)), PAnsiChar(AnsiString(RetornaNumeros(NFormat(pValorCheque, 2)))), 2, '', '') );
    if not Result then
      Exit;
  end;

  if pValorPOS > 0 then begin
    Result := not HouveErro( FProcEPSON_Fiscal_Pagamento(PAnsiChar(AnsiString(NomeCartaoPOS)), PAnsiChar(AnsiString(RetornaNumeros(NFormat(pValorPOS, 2)))), 2, '', '') );
    if not Result then
      Exit;
  end;

  if pValorCartao > 0 then begin
    BlockInput(True);
    Result := not HouveErro( FProcEPSON_Fiscal_Pagamento(PAnsiChar(AnsiString(NomeCartao)), PAnsiChar(AnsiString(LPad(RetornaNumeros(NFormat(pValorCartao)), 8, '0'))), 2, '', '') );
    if not Result then
      Exit;
  end;

  if pValorCobranca > 0 then begin
    Result := not HouveErro( FProcEPSON_Fiscal_Pagamento(PAnsiChar(AnsiString(NomeCobranca)), PAnsiChar(AnsiString(RetornaNumeros(NFormat(pValorCobranca, 2)))), 2, '', '') );
    if not Result then
      Exit;
  end;

  if pValorCredito > 0 then begin
    Result := not HouveErro( FProcEPSON_Fiscal_Pagamento(PAnsiChar(AnsiString(NomeCredito)), PAnsiChar(AnsiString(RetornaNumeros(NFormat(pValorCredito, 2)))), 2, '', '') );
    if not Result then
      Exit;
  end;

  if pValorDinheiro > 0 then begin
    Result := not HouveErro( FProcEPSON_Fiscal_Pagamento(PAnsiChar(AnsiString(NomeDinheiro)), PAnsiChar(AnsiString(RetornaNumeros(NFormat(pValorDinheiro, 2)))), 2, '', '') );
    if not Result then
      Exit;
  end;
end;

function TEpson.FecharComprovanteNaoFiscalVinculado(mostrar_msg: Boolean): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  Result := not HouveErro( FProcEPSON_NaoFiscal_Fechar_CCD(True) );
end;

function TEpson.FecharRelatorioGerencial: Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  Result := not HouveErro( FProcEPSON_NaoFiscal_Fechar_Relatorio_Gerencial(True) );
end;

function TEpson.FinalizarCupom(mensagem: array of string): Boolean;

  function MontarMensagem: AnsiString;
  var
    i: Integer;
  begin
    Result := '';
    for i := Low(mensagem) to High(mensagem) do begin
      Result := Result + AnsiString(RetirarAcentos(mensagem[i])) + #13#10;

      if i = 7 then // Parando o loop caso o tamanho do vetor seja maior que 7, pois j� ter� sido inserido a qtde m�xima de linhas que a mensagem suporta.
        Break;
    end;

    Result := AnsiString( RetirarAcentos(string(Result)) );
  end;

begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  Result := not HouveErro( FProcEPSON_Fiscal_Imprimir_MensagemEX( PAnsiChar(MontarMensagem)) );
  if not Result then
    Exit;

  Result := not HouveErro( FProcEPSON_Fiscal_Fechar_Cupom(True, False) );
end;

function TEpson.AbrirGaveta: Boolean;
begin
  inherited;
  Result := True;
end;

function TEpson.CancelarCupomAberto: Boolean;
var
  estado_cupom: AnsiString;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  estado_cupom := AnsiString(StringOfChar(' ', 57));
  Result := not HouveErro( FProcEPSON_Obter_Estado_Cupom( PAnsiChar(estado_cupom)) );
  if not Result then
    Exit;

  case StrToIntDef(Copy(string(estado_cupom), 1, 2), 99) of
    0: Exit;

    1: Result := not HouveErro( FProcEPSON_Fiscal_Cancelar_CupomEX() );
    2: Result := not HouveErro( FProcEPSON_NaoFiscal_Fechar_CCD(True) );
    3: Result := not HouveErro( FProcEPSON_NaoFiscal_Fechar_CCD(True) );
    4: Result := not HouveErro( FProcEPSON_NaoFiscal_Fechar_Relatorio_Gerencial(True) );
    5: Result := not HouveErro( FProcEPSON_NaoFiscal_Cancelar_ComprovanteEX );
  end;
end;

function TEpson.EmitirReducaoZ: Boolean;
var
  Proc: TEPSON_RelatorioFiscal_RZEX;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  @Proc := GetProcAddress(FHandle, 'EPSON_RelatorioFiscal_RZEX');
  if not Assigned(Proc) then begin
    Exclamar('Falha ao chamar o m�todo "EPSON_RelatorioFiscal_RZEX " na DLL "InterfaceEpson.dll"!');
    Exit;
  end;

  Result := not HouveErro( Proc(2) );
end;

function TEpson.EfetuarSuprimento(valor_suprimento: Double): Boolean;
var
  Proc: TEPSON_NaoFiscal_Fundo_Troco;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  @Proc := GetProcAddress(FHandle, 'EPSON_NaoFiscal_Fundo_Troco');
  if not Assigned(Proc) then begin
    Exclamar('Falha ao chamar o m�todo "EPSON_NaoFiscal_Fundo_Troco " na DLL "InterfaceEpson.dll"!');
    Exit;
  end;

  Result := not HouveErro( Proc(PAnsiChar(AnsiString(RetornaNumeros(NFormat(valor_suprimento)))), 2) );
end;

function TEpson.EmitirLeituraMemoriaFiscal(data_inicial, data_final: TDateTime): Boolean;
var
  buffer: Integer;
  Proc: TEPSON_RelatorioFiscal_Leitura_MF;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  @Proc := GetProcAddress(FHandle, 'EPSON_RelatorioFiscal_Leitura_MF');
  if not Assigned(Proc) then begin
    Exclamar('Falha ao chamar o m�todo "EPSON_RelatorioFiscal_Leitura_MF " na DLL "InterfaceEpson.dll"!');
    Exit;
  end;

  Result := not HouveErro(
    Proc(
      PAnsiChar(AnsiString(FormatDateTime('ddmmyyyy', data_inicial))),
      PAnsiChar(AnsiString(FormatDateTime('ddmmyyyy', data_final))),
      5,
      nil,
      '',
      @buffer,
      10
    )
  );
end;

function TEpson.EmitirLeituraX: Boolean;
var
  Proc: TEPSON_RelatorioFiscal_LeituraX;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  @Proc := GetProcAddress(FHandle, 'EPSON_RelatorioFiscal_LeituraX');
  if not Assigned(Proc) then begin
    Exclamar('Falha ao chamar o m�todo "EPSON_RelatorioFiscal_LeituraX " na DLL "InterfaceEpson.dll"!');
    Exit;
  end;

  Result := not HouveErro( Proc() );
end;

function TEpson.RetornoImpressora: RecRetornoImpressora;
var
  nr_str: AnsiString;
  nr_caixa: AnsiString;
begin
  inherited;
  Result.erro := True;
  if FSemComunicacao then
    Exit;

  nr_str := AnsiString(Espacos(85));
  nr_caixa := AnsiString(Espacos(8));

  if HouveErro( FProcEPSON_Obter_Contadores(PAnsiChar(nr_str)) ) then
    Exit;

  if HouveErro( FProcEPSON_Obter_Numero_ECF_Loja(PAnsiChar(nr_caixa)) ) then
    Exit;

  Result.Erro := False;
  Result.ECF  := SFormatInt( Copy(string(nr_caixa), 1, 3) );
  Result.COO  := SFormatInt( Copy(string(nr_str), 1, 6) );
end;

function TEpson.CancelarUltimoCupom: Boolean;
var
  Proc: TEPSON_Fiscal_Cancelar_CupomEX;
begin
  inherited;
  Result := False;
  @Proc := GetProcAddress(FHandle, 'EPSON_Fiscal_Cancelar_CupomEX');
  if not Assigned(Proc) then begin
    Exclamar('Falha ao chamar o m�todo "EPSON_Fiscal_Cancelar_CupomEX " na DLL "InterfaceEpson.dll"!');
    Exit;
  end;

  Result := not HouveErro( Proc() );
end;

function TEpson.AbrirRecebimentoNaoFiscal(cpf: string; nome: string; endereco: string): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  Result := not HouveErro( FProcEPSON_NaoFiscal_Abrir_Comprovante(PAnsiChar(AnsiString(cpf)), PAnsiChar(AnsiString(nome)), PAnsiChar(AnsiString(Copy(endereco, 1, 40))), '', 2) )
end;

function TEpson.EfetuarRecebimentoNaoFiscal(valorRecebimento: Double): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  Result := not HouveErro( FProcEPSON_NaoFiscal_Vender_Item( PAnsiChar(AnsiString(FIndiceTotalizadorNaoFiscal)), PAnsiChar(AnsiString(LPad( RetornaNumeros(FormatFloat('#0.00', valorRecebimento)), 14, '0'))), 2) );
end;

function TEpson.SubTotalizarRecebimento: Boolean;
begin
  inherited;
  Result := True;
end;

function TEpson.TotalizarRecebimento: Boolean;
begin
  inherited;
  Result := True;
end;

function TEpson.EfetuarFormaPagamento(forma_pagamento: TFormaPagamento; valor: Double): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  Result :=
    not HouveErro(
      FProcEPSON_NaoFiscal_Pagamento(
        PAnsiChar( AnsiString(getNomeFormaPagamento(forma_pagamento)) ),
        PAnsiChar( AnsiString(LPad( RetornaNumeros(FormatFloat('#0.00', valor)), 13, '0')) ),
        2,
        '',
        ''
      )
    );
end;

function TEpson.FecharRecebimentoNaoFiscal(mensagem: string): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  Result := not HouveErro( FProcEPSON_NaoFiscal_Fechar_Comprovante(True) );
end;

function TEpson.CancelarRecebimentoNaoFiscal(cpf: string; nome: string; endereco: string): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  Result := not HouveErro( FProcEPSON_NaoFiscal_Cancelar_ComprovanteEX() );
end;

function TEpson.AbrirComprovanteNaoFiscalVinculado(
  const pCondicao: string;
  const pValor: Double;
  const pNumeroCupom: Integer;
  const pNumeroParcelas: Integer
): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  Result := not HouveErro( FProcEPSON_NaoFiscal_Abrir_CCD(PAnsiChar(AnsiString(pCondicao)), PAnsiChar(AnsiString(RetornaNumeros(NFormat(pValor)))), 2, PAnsiChar(AnsiString(IntToStr(pNumeroParcelas)))) );
end;

function TEpson.ImprimirTextoGerencial(texto: string): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  Result := not HouveErro( FProcEPSON_NaoFiscal_Imprimir_Linha(PAnsiChar(AnsiString(texto))) );
end;

function TEpson.ImprimirTextoNaoFiscal(texto: string): Boolean;
begin
  inherited;
  Result := False;
  if FSemComunicacao then
    Exit;

  Result := not HouveErro( FProcEPSON_NaoFiscal_Imprimir_LinhaEX( PAnsiChar(AnsiString(LPad(texto, 56)))) );
end;

function TEpson.AcionarGuilhotina: Boolean;
begin
  inherited;
  Result := True;
end;


end.
