unit _Daruma2;

interface

uses
  System.Classes, System.SysUtils, Winapi.Windows, _ECF, _Biblioteca;

type
  TDaruma = class(TECF, IECF)
  protected
    function IniciarComunicacao(porta: Integer): Boolean; override;
  private
    FProcInterpretarRetorno: function(iIndice: Integer; pszRetorno: Ansistring): Integer; stdcall;
    FProcRetornarAvisoErroUltimoCMD: function(pszAviso: Ansistring; pszErro: Ansistring): Integer; stdcall;
    FProcrStatusUltimoCmdIntECFDaruma: function(var iErro: Integer; var iAviso: Integer): Integer; stdcall;

    FProcAbrirCupom: function(pszCPF: AnsiString; pszNome: AnsiString; pszEndereco: AnsiString): Integer; stdcall;
    FProcVenderItem: function(pszCargaTributaria: AnsiString; pszQuantidade: AnsiString; pszPrecoUnitario: AnsiString; pszTipoDescAcresc: AnsiString; pszValorDescAcresc: AnsiString; pszCodigoItem: AnsiString; pszUnidadeMedida: AnsiString; pszDescricaoItem: AnsiString): Integer; stdcall;
    FCancelarItem: function(pszNumItem: AnsiString): Integer; stdcall;
    FProcEfetuarPagamentoFormatado: function(pszFormaPgto: AnsiString; pszValor: AnsiString): Integer; stdcall;
    FProcTotalizarCupom: function(pszTipoDescAcresc: AnsiString; pszValorDescAcresc: AnsiString): Integer; stdcall;
    FProcFinalizarCupom: function(pszMensagem: AnsiString): Integer; stdcall;
    FProcAbrirGaveta: function: Integer; stdcall;
    FProcRetornarInformacao: function(pszIndice: AnsiString; pszRetornar: AnsiString): Integer; stdcall;
    FAbrirSimplificado: function(pszFormaPgto: AnsiString; pszParcelas: AnsiString; pszDocOrigem: AnsiString; pszValor: AnsiString): Integer; stdcall;
    FImprimirTexto: function(pszTexto: AnsiString): Integer; stdcall;
    FAbrirRelatorioGerencial: function: Integer; stdcall;
    FImprimirTextoGerencial: function(pszTexto: AnsiString): Integer; stdcall;
    FAcionarGuilhotina: function(pszTipoCorte: AnsiString): Integer; stdcall;

    function DarumaFramework_Mostrar_Retorno(iRetorno: Integer; aliquota: Double = 0; mostrar_msg: Boolean = True): Boolean;
    function HouveErro(retorno: Integer): Boolean;
  public
    function SetOperador(usuario_id: Integer; nome: string): Boolean;

    function AbrirCupom(const pCPF_CNPJ: string; const pNome: string; const pEndereco: string): Boolean; override;

    function AdicionarItemCupom(
      codigo: string;
      descricao: string;
      marca: string;
      quantidade: Double;
      preco_unitario: Double;
      tipo_tributacao: string;
      perc_icms: Double;
      valor_desconto: Double;
      unidade: string
    ): Boolean; override;

    function CancelarItemCupom(numero_item: Integer): Boolean; override;

    function IniciarFechamentoCupom(acrescimo: Double; desconto: Double): Boolean; override;

    function TotalizarCupom(
      const pValorDinheiro: Double;
      const pValorCartao: Double;
      const pValorCobranca: Double;
      const pValorCheque: Double;
      const pValorCredito: Double;
      const pValorPOS: Double
    ): Boolean; override;

    function FinalizarCupom(mensagem: array of string): Boolean; override;
    function AbrirGaveta: Boolean; override;
    function CancelarCupomAberto: Boolean; override;
    function EmitirReducaoZ: Boolean; override;
    function EmitirLeituraX: Boolean; override;
    function RetornoImpressora: RecRetornoImpressora; override;

    function AbrirComprovanteNaoFiscalVinculado(
      const pCondicao: string;
      const pValor: Double;
      const pNumeroCupom: Integer;
      const pNumeroParcelas: Integer
    ): Boolean; override;

    function ImprimirTextoNaoFiscal(texto: string): Boolean; override;
    function AcionarGuilhotina: Boolean; override;
    function CancelarUltimoCupom: Boolean; override;
    function AbrirRelatorioGerencial: Boolean; override;
    function ImprimirTextoGerencial(texto: string): Boolean; override;
    function FecharRelatorioGerencial: Boolean; override;
    function FecharComprovanteNaoFiscalVinculado(mostrar_msg: Boolean): Boolean; override;

  end;

implementation

{ TDaruma }

type
  TDefinirProdutoDaruma = function(szProduto: AnsiString): Integer; stdcall;
  TVerificarImpressoraLigada = function: Integer; stdcall;
  TBuscarPortaVelocidade = function: Integer; stdcall;
  TAlterarValorDaruma = function(pszPathChave: AnsiString; pszValor: AnsiString): Integer; stdcall;
  TrCFVerificarStatusECFDaruma = function(pszStatus: AnsiString; var piStatus: Integer): Integer; stdcall;
  TProgramarOperador = function(pszValor: AnsiString): Integer; stdcall;
  TCancelarCupom = function: Integer; stdcall;
  TReducaoZ = function(pszData: AnsiString; pszHora: AnsiString): Integer; stdcall;
  TLeituraX = function: Integer; stdcall;
  TFecharCCD = function: Integer; stdcall;
  TCancelarUltimoCupom = function: Integer; stdcall;

function TDaruma.HouveErro(retorno: Integer): Boolean;

  procedure GerarErro(mensagem: string);
  begin
    Result := True;
    Exclamar(mensagem);
  end;

begin
  Result := False;
  case retorno of
     0: GerarErro('Erro durante a execu��o!');
     1: begin
        // Faz nada, segnifica que deu certo!
     end;
    -1: GerarErro(' Erro do M�todo!');
    -2: GerarErro(' Par�metro incorreto!');
    -3: GerarErro(' Al�quota (Situa��o tribut�ria) n�o programada!');
    -4: GerarErro(' Chave do Registry n�o encontrada!');
    -5: GerarErro(' Erro ao Abrir a porta de Comunica��o!');
    -6: GerarErro(' Impressora Desligada!');
    -7: GerarErro(' Erro no N�mero do Banco!');
    -8: GerarErro(' Erro ao Gravar as informa��es no arquivo de Status ou de Retorno de Info!');
    -9: GerarErro(' Erro ao Fechar a porta de Comunica��o!');
    -10: GerarErro(' O ECF n�o tem a forma de pagamento e n�o permite cadastrar esta forma!');
    -12: GerarErro(' A fun��o executou o comando por�m o ECF sinalizou Erro, chame a rStatusUltimoCmdInt_ECF_Daruma para identificar o Erro!');
    -13: GerarErro(' A Emissao da RZ ira bloquear a ECF ate as 23:59:59h, Execute o comando novamente para confirmar a Emissao!');
    -24: GerarErro(' Forma de Pagamento n�o Programada!');
    -27: GerarErro(' Foi Detectado Erro ou Warning na Impressora!');
    -28: GerarErro(' Time-Out!');
    -40: GerarErro(' Tag XML Inv�lida!');
    -41: GerarErro(' NCM n�o encontrado!');
    -42: GerarErro(' Item n�o encontrado!');
    -50: GerarErro(' Problemas ao Criar Chave no Registry!');
    -51: GerarErro(' Erro ao Gravar LOG!');
    -52: GerarErro(' Erro ao abrir arquivo!');
    -53: GerarErro(' Fim de arquivo!');
    -60: GerarErro(' Erro na tag de formata��o DHTML!');
    -78: GerarErro(' Impressora FS600 com SB 01.04, favor atualizar!');
    -90: GerarErro(' Erro Configurar a Porta de Comunica��o!');
    -99: GerarErro(' Par�metro inv�lido ou ponteiro nulo de par�metro!');
    -101: GerarErro(' Erro ao LER ou ESCREVER arquivo!');
    -102: GerarErro(' Erro arquivo corrompido!');
    -103: GerarErro(' N�o foram encontradas as DLLs auxiliares (lebin.dll e LeituraMFDBin.dll)!');
    -104: GerarErro(' Data informada � inferior ao primeiro documento emitido!');
    -105: GerarErro(' Data informada � maior que a ultima redu��o Z impressa!');
    -106: GerarErro(' N�o possui movimento no periodo solicitado!');
    -107: GerarErro(' Porta de comunica��o ocupada!');
    -110: GerarErro(' Indica que o GT foi atualizado no arquivo de registro do PAF!');
    -112: GerarErro(' O numero de s�rie j� existe no arquivo do PAF!');
    -113: GerarErro(' ECF conectado n�o cadastrado no arquivo do PAF!');
    -114: GerarErro(' MFD Danificada!');
    -115: GerarErro(' Erro ao abrir arquivos .idx/.dat/.mf!');
    -116: GerarErro(' Intervalo solicitado n�o � v�lido!');
    -117: GerarErro(' Impressora n�o identificada durante download dos bin�rios!');
    -118: GerarErro(' Erro ao abrir porta serial!');
    -119: GerarErro(' Leitura dos bin�rios abortada!');
  end;
end;

function TDaruma.DarumaFramework_Mostrar_Retorno(iRetorno: Integer; aliquota: Double = 0; mostrar_msg: Boolean = True): Boolean;
var
  Str_Msg_NumErro: Ansistring;
  Str_Msg_NumAviso: Ansistring;
  Str_Msg_RetMetodo: Ansistring;

  erro: Integer;
  aviso: Integer;
begin
   Result := False;

  if iRetorno = -12 then begin
    iRetorno := FProcrStatusUltimoCmdIntECFDaruma(erro, aviso);
    if erro = 0 then
      Exit;
  end;

  //Variaveis devem ser inicializadas
  Str_Msg_NumErro := AnsiString(StringOFChar(' ',300));
  Str_Msg_NumAviso := AnsiString(StringOFChar(' ',300));
  Str_Msg_RetMetodo := AnsiString(StringOFChar(' ',300));

  // Retorna a mensagem referente ao erro e aviso do ultimo comando enviado
  FProcRetornarAvisoErroUltimoCMD(Str_Msg_NumAviso, Str_Msg_NumErro);

  if Trim(string(Str_Msg_NumErro)) = 'RZ do movimento anterior pendente' then begin
    Exclamar('Redu��o Z pendente!');
    BlockInput(False);
    Result := True;
    Exit;
  end;

  if Trim(string(Str_Msg_NumErro)) = '�ndice inv�lido' then begin
    Exclamar('Al�quota ' + NFormat(aliquota) + ' n�o cadastrada!');
    BlockInput(False);
    Result := True;
    Exit;
  end;

  // Retorno do m�todo
  FProcInterpretarRetorno(iRetorno, Str_Msg_RetMetodo);

  if (Trim(string(Str_Msg_RetMetodo)) <> '') and (Trim(string(Str_Msg_RetMetodo)) <> 'Opera��o realizada com sucesso') then begin
    Exclamar(
      pWideChar(
        'Retorno do M�todo = ' + pWideChar(Trim(String(Str_Msg_RetMetodo))) + #13 +
        'N�mero Erro = ' + pwideChar(Trim(String(Str_Msg_NumErro))) +  #13 +
        'N�mero Aviso = ' + pWideChar(Trim(String(Str_Msg_NumAviso)))
      )
    );

    BlockInput(False);
    Result := True;
  end;
end;

function TDaruma.IniciarComunicacao(porta: Integer): Boolean;
var
  ProcVerificarImpressoraLigada: TVerificarImpressoraLigada;
  ProcBuscarPortaVelocidade: TBuscarPortaVelocidade;
  ProcDefinirProdutoDaruma: TDefinirProdutoDaruma;
  ProcAlterarValorDaruma: TAlterarValorDaruma;
begin
  inherited;
  Result := False;
  try
    if not FileExists('DarumaFramework.dll') then begin
      Exclamar('A DLL "DarumaFramework.dll" n�o foi encontrada na pasta do execut�vel!');
      Exit;
    end;

    (* Tentar carregar a DLL de maneira Din�mica *)
    FHandle := LoadLibrary('DarumaFramework.dll');
    if FHandle = 0 then begin
      Exclamar('Falha ao carregar a DLL "DarumaFramework.dll"!');
      Exit;
    end;

    (* Define como fiscal *)
    ProcDefinirProdutoDaruma := GetProcAddress(FHandle, 'eDefinirProduto_Daruma');
    if not Assigned(ProcDefinirProdutoDaruma) then begin
      Exclamar('Falha ao chamar o m�todo "eDefinirProduto_Daruma" na DLL "DarumaFramework.dll"!');
      Exit;
    end;
    ProcDefinirProdutoDaruma('FISCAL');

    (* Verifica se est� tudo certo com o ECF *)
    ProcVerificarImpressoraLigada := GetProcAddress(FHandle, 'rVerificarImpressoraLigada_ECF_Daruma');
    if not Assigned(ProcVerificarImpressoraLigada) then begin
      Exclamar('Falha ao chamar o m�todo "rVerificarImpressoraLigada_ECF_Daruma" na DLL "DarumaFramework.dll"!');
      Exit;
    end;
    if HouveErro(ProcVerificarImpressoraLigada) then
      Exit;

    (* Verifica a velocidade da porta *)
    ProcBuscarPortaVelocidade := GetProcAddress(FHandle, 'eBuscarPortaVelocidade_ECF_Daruma');
    if not Assigned(ProcBuscarPortaVelocidade) then begin
      Exclamar('Falha ao chamar o m�todo "eBuscarPortaVelocidade_ECF_Daruma" na DLL "DarumaFramework.dll"!');
      Exit;
    end;
    if HouveErro(ProcBuscarPortaVelocidade) then
      Exit;

    (* Altera algumas configura��es sempre que iniciar a DLL *)
    ProcAlterarValorDaruma := GetProcAddress(FHandle, 'regAlterarValor_Daruma');
    if not Assigned(ProcAlterarValorDaruma) then begin
      Exclamar('Falha ao chamar o m�todo "regAlterarValor_Daruma" na DLL "DarumaFramework.dll"!');
      Exit;
    end;

    ProcAlterarValorDaruma('ECF\RetornarAvisoErro','1');
    ProcAlterarValorDaruma('ECF\SinalSonoroIniciar','0');
    ProcAlterarValorDaruma('ECF\NaoAvisarPoucoPapel','1'); // N�o avisar que o papel est� acabando, toda rotina que imprime uma linha avisa, � um saco!!!
    ProcAlterarValorDaruma('ECF\RetornarAvisoErro','1'); // Ao dar erro em alguma fun��o, sempre sinalizar.
    ProcAlterarValorDaruma('ECF\ArredondarTruncar','T'); // Sempre truncar os valores.

    FProcRetornarAvisoErroUltimoCMD := GetProcAddress(FHandle, 'eRetornarAvisoErroUltimoCMD_ECF_Daruma');
    if not Assigned(FProcRetornarAvisoErroUltimoCMD) then begin
      Exclamar('Falha ao chamar o m�todo "eRetornarAvisoErroUltimoCMD_ECF_Daruma" na DLL "DarumaFramework.dll"!');
      Exit;
    end;

    FProcInterpretarRetorno := GetProcAddress(FHandle, 'eInterpretarRetorno_ECF_Daruma');
    if not Assigned(FProcInterpretarRetorno) then begin
      Exclamar('Falha ao chamar o m�todo "eInterpretarRetorno_ECF_Daruma" na DLL "DarumaFramework.dll"!');
      Exit;
    end;

    FProcrStatusUltimoCmdIntECFDaruma := GetProcAddress(FHandle, 'rStatusUltimoCmdInt_ECF_Daruma');
    if not Assigned(FProcrStatusUltimoCmdIntECFDaruma) then begin
      Exclamar('Falha ao chamar o m�todo "rStatusUltimoCmdInt_ECF_Daruma" na DLL "DarumaFramework.dll"!');
      Exit;
    end;

    FProcAbrirCupom := GetProcAddress(FHandle, 'iCFAbrir_ECF_Daruma');
    if not Assigned(FProcAbrirCupom) then begin
      Exclamar('Falha ao chamar o m�todo "iCFAbrir_ECF_Daruma" na DLL "DarumaFramework.dll"!');
      Exit;
    end;

    FProcVenderItem := GetProcAddress(FHandle, 'iCFVender_ECF_Daruma');
    if not Assigned(FProcVenderItem) then begin
      Exclamar('Falha ao chamar o m�todo "iCFVender_ECF_Daruma" na DLL "DarumaFramework.dll"!');
      Exit;
    end;

    FCancelarItem := GetProcAddress(FHandle, 'iCFCancelarItem_ECF_Daruma');
    if not Assigned(FCancelarItem) then begin
      Exclamar('Falha ao chamar o m�todo "iCFCancelarItem_ECF_Daruma" na DLL "DarumaFramework.dll"!');
      Exit;
    end;

    FProcEfetuarPagamentoFormatado := GetProcAddress(FHandle, 'iCFEfetuarPagamentoFormatado_ECF_Daruma');
    if not Assigned(FProcEfetuarPagamentoFormatado) then begin
      Exclamar('Falha ao chamar o m�todo "iCFEfetuarPagamentoFormatado_ECF_Daruma" na DLL "DarumaFramework.dll"!');
      Exit;
    end;

    FProcTotalizarCupom := GetProcAddress(FHandle, 'iCFTotalizarCupom_ECF_Daruma');
    if not Assigned(FProcTotalizarCupom) then begin
      Exclamar('Falha ao chamar o m�todo "iCFTotalizarCupom_ECF_Daruma" na DLL "DarumaFramework.dll"!');
      Exit;
    end;

    FProcFinalizarCupom := GetProcAddress(FHandle, 'iCFEncerrarConfigMsg_ECF_Daruma');
    if not Assigned(FProcTotalizarCupom) then begin
      Exclamar('Falha ao chamar o m�todo "iCFEncerrarConfigMsg_ECF_Daruma" na DLL "DarumaFramework.dll"!');
      Exit;
    end;

    FProcAbrirGaveta := GetProcAddress(FHandle, 'eAbrirGaveta_ECF_Daruma');
    if not Assigned(FProcAbrirGaveta) then begin
      Exclamar('Falha ao chamar o m�todo "eAbrirGaveta_ECF_Daruma" na DLL "DarumaFramework.dll"!');
      Exit;
    end;

    FProcRetornarInformacao := GetProcAddress(FHandle, 'rRetornarInformacao_ECF_Daruma');
    if not Assigned(FProcRetornarInformacao) then begin
      Exclamar('Falha ao chamar o m�todo "rRetornarInformacao_ECF_Daruma" na DLL "DarumaFramework.dll"!');
      Exit;
    end;

    FAbrirSimplificado := GetProcAddress(FHandle, 'iCCDAbrirSimplificado_ECF_Daruma');
    if not Assigned(FAbrirSimplificado) then begin
      Exclamar('Falha ao chamar o m�todo "iCCDAbrirSimplificado_ECF_Daruma" na DLL "DarumaFramework.dll"!');
      Exit;
    end;

    FImprimirTexto := GetProcAddress(FHandle, 'iCCDImprimirTexto_ECF_Daruma');
    if not Assigned(FImprimirTexto) then begin
      Exclamar('Falha ao chamar o m�todo "iCCDImprimirTexto_ECF_Daruma" na DLL "DarumaFramework.dll"!');
      Exit;
    end;

    FAcionarGuilhotina := GetProcAddress(FHandle, 'eAcionarGuilhotina_ECF_Daruma');
    if not Assigned(FAcionarGuilhotina) then begin
      Exclamar('Falha ao chamar o m�todo "eAcionarGuilhotina_ECF_Daruma" na DLL "DarumaFramework.dll"!');
      Exit;
    end;

    FAbrirRelatorioGerencial := GetProcAddress(FHandle, 'iRGAbrirPadrao_ECF_Daruma');
    if not Assigned(FAbrirRelatorioGerencial) then begin
      Exclamar('Falha ao chamar o m�todo "iRGAbrirPadrao_ECF_Daruma" na DLL "DarumaFramework.dll"!');
      Exit;
    end;

    FImprimirTextoGerencial := GetProcAddress(FHandle, 'iRGImprimirTexto_ECF_Daruma');
    if not Assigned(FImprimirTextoGerencial) then begin
      Exclamar('Falha ao chamar o m�todo "iRGImprimirTexto_ECF_Daruma" na DLL "DarumaFramework.dll"!');
      Exit;
    end;

    FPorta := porta;
    Result := True;
  except on e: Exception do
      Exclamar(e.Message);
  end;
end;

function TDaruma.SetOperador(usuario_id: Integer; nome: string): Boolean;
var
  ProcProgramarOperador: TProgramarOperador;
begin
  Result := False;
  ProcProgramarOperador := GetProcAddress(FHandle, 'confProgramarOperador_ECF_Daruma');
  if not Assigned(ProcProgramarOperador) then begin
    Exclamar('Falha ao chamar o m�todo "confProgramarOperador_ECF_Daruma" na DLL "DarumaFramework.dll"!');
    Exit;
  end;
  Result := not DarumaFramework_Mostrar_Retorno( ProcProgramarOperador(AnsiString(Copy(InttoStr(usuario_id) + ' - ' + nome, 1, 20))) );
end;

function TDaruma.AbrirCupom(const pCPF_CNPJ: string; const pNome: string; const pEndereco: string): Boolean;
begin
  inherited;
  Result := not DarumaFramework_Mostrar_Retorno( FProcAbrirCupom(AnsiString(pCPF_CNPJ), '', '') );
end;

function TDaruma.AdicionarItemCupom(
  codigo: string;
  descricao: string;
  marca: string;
  quantidade: Double;
  preco_unitario: Double;
  tipo_tributacao: string;
  perc_icms: Double;
  valor_desconto: Double;
  unidade: string
): Boolean;
begin
  inherited;
  Result := not DarumaFramework_Mostrar_Retorno(
    FProcVenderItem(
      AnsiString(tipo_tributacao),
      AnsiString(FormatFloat('#0.000', quantidade)),
      AnsiString(NFormat(preco_unitario, FQtdeDecimaisPrecoVenda)),
      'D$', AnsiString(FormatFloat('#0.00', valor_desconto)),
      AnsiString(Copy(codigo, 1, 13)),
      AnsiString(LPad(unidade, 2)),
      AnsiString(Copy(LimparTexto(descricao), 1, 29))
    )
  );

//  if pdv and (ret_int = -1) then begin
//    // Contornando o problema de hardware das ECF Daruma, trava o papel,
//    // por�m j� registrou o item, quando chamamos a rotina de verificar se teve problema ele nos fala que houve e mesmo assim j� registrou o produto no cupom fiscal.
//    Exclamar(
//      'Houve um problema de comunica��o com o ECF, verifique se est� tudo correto com a impressora click em "Ok"!' + chr(13) + chr(10) +
//      'Aten��o! O �ltimo item ser� cancelado!'
//    );
//
//    LogCupomFiscal.AdicionarLog('Dentro da rotina para contornar o problema das ECF Daruma.');
//    ret_int := _Daruma.iCFCancelarUltimoItem_ECF_Daruma();
//    if ret_int <> 1 then begin
//      Exclamar('Falha ao cancelar o �ltimo item, verifique se todos os itens da venda foram adicionados corretamente no cupom fiscal!');
//      LogCupomFiscal.AdicionarLog('N�o cancelou o ultimo item corretamente.');
//      Result := False;
//      Exit;
//    end;
//  end;
end;

function TDaruma.CancelarItemCupom(numero_item: Integer): Boolean;
begin
  inherited;
  Result := not DarumaFramework_Mostrar_Retorno( FCancelarItem(AnsiString(FormatFloat('000', numero_item))) );
end;

function TDaruma.IniciarFechamentoCupom(acrescimo: Double; desconto: Double): Boolean;
var
  sld: Currency;
  tipo_acao: string;
begin
  inherited;
  try
    sld := acrescimo - desconto;

    if sld >= 0 then
      tipo_acao := 'A$'
    else
      tipo_acao := 'D$';

    Result := not DarumaFramework_Mostrar_Retorno( FProcTotalizarCupom(AnsiString(tipo_acao), AnsiString(FormatFloat('#0.00', abs(sld)))) );
  except
    Result := False;
  end;
end;

function TDaruma.TotalizarCupom(
  const pValorDinheiro: Double;
  const pValorCartao: Double;
  const pValorCobranca: Double;
  const pValorCheque: Double;
  const pValorCredito: Double;
  const pValorPOS: Double
): Boolean;
begin
  inherited;
  Result := False;

  if pValorDinheiro > 0 then begin
    Result := not DarumaFramework_Mostrar_Retorno( FProcEfetuarPagamentoFormatado( NomeDinheiro, AnsiString(FormatFloat('#0.00', pValorDinheiro))) );
    if not Result then
      Exit;
  end;

  if pValorCheque > 0 then begin
    Result := not DarumaFramework_Mostrar_Retorno( FProcEfetuarPagamentoFormatado( NomeCheque, AnsiString(FormatFloat('#0.00', pValorCheque))) );
    if not Result then
      Exit;
  end;

  if pValorPOS > 0 then begin
    Result := not DarumaFramework_Mostrar_Retorno( FProcEfetuarPagamentoFormatado( NomeCartaoPOS, AnsiString(FormatFloat('#0.00', pValorPOS))) );
    if not Result then
      Exit;
  end;

  if pValorCartao > 0 then begin
    BlockInput(True);
    Result := not DarumaFramework_Mostrar_Retorno( FProcEfetuarPagamentoFormatado( NomeCartao, AnsiString(FormatFloat('#0.00', pValorCartao))) );
    if not Result then begin
      BlockInput(False);
      Exit;
    end;
  end;

  if pValorCobranca > 0 then begin
    Result := not DarumaFramework_Mostrar_Retorno( FProcEfetuarPagamentoFormatado( NomeCobranca, AnsiString(FormatFloat('#0.00', pValorCobranca))) );
    if not Result then
      Exit;
  end;

  if pValorCredito > 0 then begin
    Result := not DarumaFramework_Mostrar_Retorno( FProcEfetuarPagamentoFormatado( NomeCredito, AnsiString(FormatFloat('#0.00', pValorCredito))) );
    if not Result then
      Exit;
  end;
end;

function TDaruma.FinalizarCupom(mensagem: array of string): Boolean;

  function MontarMensagem: AnsiString;
  var
    i: Integer;
  begin
    Result := '';
    for i := Low(mensagem) to High(mensagem) do begin
      Result := Result + AnsiString(RetirarAcentos(mensagem[i])) + #13#10;

      if i = 7 then // Parando o loop caso o tamanho do vetor seja maior que 7, pois j� ter� sido inserido a qtde m�xima de linhas que a mensagem suporta.
        Break;
    end;
  end;

begin
  inherited;
  Result := not DarumaFramework_Mostrar_Retorno( FProcFinalizarCupom(MontarMensagem) );
end;

function TDaruma.AbrirGaveta: Boolean;
begin
  inherited;
  Result := not DarumaFramework_Mostrar_Retorno( FProcAbrirGaveta() );
end;

function TDaruma.CancelarCupomAberto: Boolean;
var
  Str_StatusCupomFiscal: AnsiString;
  Int_StatusCupomFiscal: Integer;

  ProcCancelarCupom: TCancelarCupom;
  ProcVerificarStatusECFDaruma: TrCFVerificarStatusECFDaruma;
begin
  inherited;
  Result := False;

  ProcVerificarStatusECFDaruma := GetProcAddress(FHandle, 'rCFVerificarStatus_ECF_Daruma');
  if not Assigned(ProcVerificarStatusECFDaruma) then begin
    Exclamar('Falha ao chamar o m�todo "rCFVerificarStatus_ECF_Daruma" na DLL "DarumaFramework.dll"!');
    Exit;
  end;

  SetLength(Str_StatusCupomFiscal, 1);
  Int_StatusCupomFiscal := 0;

  if DarumaFramework_Mostrar_Retorno(ProcVerificarStatusECFDaruma(AnsiString(Str_StatusCupomFiscal), Int_StatusCupomFiscal)) then
    Exit;

  if Int_StatusCupomFiscal <= 0 then begin
    Informar('N�o existe cupom fiscal aberto!');
    Exit;
  end;

  ProcCancelarCupom := GetProcAddress(FHandle, 'iCFCancelar_ECF_Daruma');
  if not Assigned(ProcCancelarCupom) then begin
    Exclamar('Falha ao chamar o m�todo "iCFCancelar_ECF_Daruma" na DLL "DarumaFramework.dll"!');
    Exit;
  end;
  Result := not DarumaFramework_Mostrar_Retorno( ProcCancelarCupom() );
end;

function TDaruma.EmitirReducaoZ: Boolean;
var
  nr_str: AnsiString;
  ProcReducaoZ: TReducaoZ;
begin
  inherited;
  Result := False;
  nr_str := AnsiString(Espacos(6));
  DarumaFramework_Mostrar_Retorno( FProcRetornarInformacao('27', nr_str) );

  ProcReducaoZ := GetProcAddress(FHandle, 'iReducaoZ_ECF_Daruma');
  if not Assigned(ProcReducaoZ) then begin
    Exclamar('Falha ao chamar o m�todo "iReducaoZ_ECF_Daruma" na DLL "DarumaFramework.dll"!');
    Exit;
  end;

  Result := not DarumaFramework_Mostrar_Retorno( ProcReducaoZ(' ',' ') );
end;

function TDaruma.EmitirLeituraX: Boolean;
var
  ProcLeituraX: TLeituraX;
begin
  inherited;
  Result := False;
  ProcLeituraX := GetProcAddress(FHandle, 'iLeituraX_ECF_Daruma');
  if not Assigned(ProcLeituraX) then begin
    Exclamar('Falha ao chamar o m�todo "iLeituraX_ECF_Daruma" na DLL "DarumaFramework.dll"!');
    Exit;
  end;
  Result := not DarumaFramework_Mostrar_Retorno( ProcLeituraX() );
end;

function TDaruma.RetornoImpressora: RecRetornoImpressora;
var
  nr_str: AnsiString;
  nr_caixa: AnsiString;
  nr_fabrica: AnsiString;
begin
  inherited;
  Result.erro := True;

  nr_str := AnsiString(Espacos(6));
  nr_fabrica := AnsiString(Espacos(20));
  nr_caixa := AnsiString(Espacos(3));

  if DarumaFramework_Mostrar_Retorno( FProcRetornarInformacao('26', nr_str) ) then
    Exit;

  if DarumaFramework_Mostrar_Retorno( FProcRetornarInformacao('107', nr_caixa) ) then
    Exit;

  if DarumaFramework_Mostrar_Retorno( FProcRetornarInformacao('78', nr_fabrica) ) then
    Exit;

  Result.NumeroFabrica := string(nr_fabrica);
  Result.ECF           := SFormatInt( string(nr_caixa) );
  Result.COO           := SFormatInt( string(nr_str) );

  Result.erro := False;
end;

function TDaruma.AbrirComprovanteNaoFiscalVinculado(
  const pCondicao: string;
  const pValor: Double;
  const pNumeroCupom: Integer;
  const pNumeroParcelas: Integer
): Boolean;
begin
  inherited;
  Result := not DarumaFramework_Mostrar_Retorno( FAbrirSimplificado(AnsiString(pCondicao), AnsiString(IntToStr(pNumeroParcelas)), AnsiString(IntToStr(pNumeroCupom)), AnsiString(FormatFloat('#0.00', pValor))) );
end;

function TDaruma.ImprimirTextoNaoFiscal(texto: string): Boolean;
begin
  inherited;
  if texto = '' then
    texto := ' ';

  Result := not DarumaFramework_Mostrar_Retorno( FImprimirTexto(AnsiString(texto)) );
end;

function TDaruma.AcionarGuilhotina: Boolean;
begin
  inherited;
  Result := not DarumaFramework_Mostrar_Retorno( FAcionarGuilhotina('1') );
end;

function TDaruma.CancelarUltimoCupom: Boolean;
var
  ProcCancelarUltimoCupom: TCancelarUltimoCupom;
begin
  inherited;
  Result := False;
  ProcCancelarUltimoCupom := GetProcAddress(FHandle, 'iCFCancelar_ECF_Daruma');
  if not Assigned(ProcCancelarUltimoCupom) then begin
    Exclamar('Falha ao chamar o m�todo "iCFCancelar_ECF_Daruma" na DLL "DarumaFramework.dll"!');
    Exit;
  end;
  Result := not DarumaFramework_Mostrar_Retorno( ProcCancelarUltimoCupom() );
end;

function TDaruma.AbrirRelatorioGerencial: Boolean;
begin
  inherited;
  Result := not DarumaFramework_Mostrar_Retorno( FAbrirRelatorioGerencial() );
end;

function TDaruma.ImprimirTextoGerencial(texto: string): Boolean;
begin
  inherited;
  Result := not DarumaFramework_Mostrar_Retorno( FImprimirTextoGerencial(AnsiString(texto)) );
end;

function TDaruma.FecharRelatorioGerencial: Boolean;
var
  ProcFecharCCD: TFecharCCD;
begin
  inherited;
  Result := False;
  ProcFecharCCD := GetProcAddress(FHandle, 'iCCDFechar_ECF_Daruma');
  if not Assigned(ProcFecharCCD) then begin
    Exclamar('Falha ao chamar o m�todo "iCCDFechar_ECF_Daruma" na DLL "DarumaFramework.dll"!');
    Exit;
  end;
  Result := not DarumaFramework_Mostrar_Retorno( ProcFecharCCD() );
end;

function TDaruma.FecharComprovanteNaoFiscalVinculado(mostrar_msg: Boolean): Boolean;
var
  ProcFecharCCD: TFecharCCD;
begin
  inherited;
  Result := False;
  ProcFecharCCD := GetProcAddress(FHandle, 'iCCDFechar_ECF_Daruma');
  if not Assigned(ProcFecharCCD) then begin
    Exclamar('Falha ao chamar o m�todo "iCCDFechar_ECF_Daruma" na DLL "DarumaFramework.dll"!');
    Exit;
  end;
  Result := not DarumaFramework_Mostrar_Retorno( ProcFecharCCD() );
end;

end.
