
//
// OFX - Open Financial Exchange
// OFC - Open Financial Connectivity

// 2006 - Eduardo Bento da Rocha (YoungArts)
// 2016 - Leonardo Gregianin - github.com/leogregianin

unit ofxreader;

interface

uses classes, SysUtils, System.StrUtils, _BibliotecaGenerica;

type
  TOFXItem = class
    MovType: String;
    MovDate: TDateTime;
    Value: Currency;
    ID: string;
    Document: string;
    Description: string;
  end;

  TOFXReader = class(TComponent)
  public
    BankID: string;
    BranchID: string;
    AccountID: string;
    AccountType: string;

    SaldoInicial: Currency;
    SaldoFinal: Currency;

    DataInicial: TDateTime;
    DataFinal: TDateTime;

    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    function Import: boolean;
    function Get(iIndex: integer): TOFXItem;
    function Count: integer;
  private
    FOFXFile : string;
    FListItems : TList;
    procedure Clear;
    procedure Delete( iIndex: integer );
    function Add: TOFXItem;
    function InfLine ( sLine : string ): string;
    function FindString ( sSubString, sString : string ): boolean;
  protected
  published
    property OFXFile: string read FOFXFile write FOFXFile;
  end;

procedure Register;

implementation

constructor TOFXReader.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FListItems := TList.Create;
end;

destructor TOFXReader.Destroy;
begin
  FListItems.Free;
  inherited Destroy;
end;

procedure TOFXReader.Delete( iIndex: integer );
begin
  TOFXItem(FListItems.Items[iIndex]).Free;
  FListItems.Delete( iIndex );
end;

procedure TOFXReader.Clear;
begin
  while FListItems.Count > 0 do
    Delete(0);

  FListItems.Clear;
end;

function TOFXReader.Count: integer;
begin
  Result := FListItems.Count;
end;

function TOFXReader.Get(iIndex: integer): TOFXItem;
begin
  Result := TOFXItem(FListItems.Items[iIndex]);
end;

function TOFXReader.Import: boolean;
var
  oFile : TStringList;
  i : integer;
  bOFX : boolean;
  oItem : TOFXItem;
  sLine : string;

  vTotMovSaldoInicial: Currency;
begin
  Clear;
  bOFX := False;
  SaldoInicial := 0;
  vTotMovSaldoInicial := 0;

  if not FileExists(FOFXFile) then
    raise Exception.Create('Arquivo OFX n�o encontrado!');

  oFile := TStringList.Create;
  oFile.LoadFromFile(FOFXFile);
  i := 0;

  while i < oFile.Count do begin
    sLine := oFile.Strings[i];
    if FindString('<OFX>', sLine) or FindString('<OFC>', sLine) then
      bOFX := true;

    if bOFX then begin
      // Bank
      if FindString('<BANKID>', sLine) then
        BankID := InfLine(sLine);

      // Agency
      if FindString('<BRANCHID>', sLine) then
        BranchID := InfLine(sLine);

      // Account
      if FindString('<ACCTID>', sLine) then begin
        AccountID := InfLine(sLine);

        if Pos('-', AccountID) > 0 then
          AccountID := Copy(AccountID, 1, Pos('-', AccountID) - 1)
        else
          AccountID := IntToStr(SFormatInt(Copy(AccountID, 1, Length(AccountID) - 1)));
      end;

      // Account type
      if FindString('<ACCTTYPE>', sLine) then
        AccountType := InfLine(sLine);

      // Data inicial
      if FindString('<DTSTART>', sLine) then
        DataInicial := _BibliotecaGenerica.FormatarDataAnoMesDia(InfLine(sLine));

      // Data final
      if FindString('<DTEND>', sLine) then
        DataFinal := _BibliotecaGenerica.FormatarDataAnoMesDia(InfLine(sLine));

      // Final
      if FindString('<LEDGER>', sLine) or FindString('<BALAMT>', sLine) then
         SaldoFinal := SFormatCurr(StringReplace(InfLine(sLine), '.', ',', [rfReplaceAll]));

      // Movement
      if FindString('<STMTTRN>', sLine) then begin
        oItem := Add;
        while not FindString('</STMTTRN>', sLine) do begin
          Inc(i);
          sLine := oFile.Strings[i];

//          if FindString('<TRNTYPE>', sLine) then begin
//            if (InfLine(sLine) = '0') or (InfLine(sLine) = 'CREDIT') or (InfLine(sLine) = 'XFER') or (InfLine(sLine) = 'DEP') then
//               oItem.MovType := 'C'
//            else if (InfLine(sLine) = '1') or (InfLine(sLine) = 'DEBIT') then
//               oItem.MovType := 'D'
//            else oItem.MovType := 'NI';
//          end;

          if FindString('<DTPOSTED>', sLine) then begin
            oItem.MovDate :=
              EncodeDate(
                StrToIntDef(copy(InfLine(sLine),1,4), 0),
                StrToIntDef(copy(InfLine(sLine),5,2), 0),
                StrToIntDef(copy(InfLine(sLine),7,2), 0)
              );
          end;

          if FindString('<FITID>', sLine) then
             oItem.ID := InfLine(sLine);

          if FindString('<CHKNUM>', sLine) or FindString('<CHECKNUM>', sLine) then
             oItem.Document := InfLine(sLine);

          if FindString('<MEMO>', sLine) then
             oItem.Description := InfLine(sLine);

          if FindString('<TRNAMT>', sLine) then begin
             oItem.Value := SFormatCurr(StringReplace(InfLine(sLine), '.', ',', [rfReplaceAll]));

             if oItem.Value < 0 then
               oItem.MovType := 'D'
             else
               oItem.MovType := 'C';

             vTotMovSaldoInicial := vTotMovSaldoInicial + oItem.Value;

             oItem.Value := Abs(oItem.Value);
          end;
        end;
      end;
    end;
    Inc(i);
  end;

  SaldoInicial := SaldoFinal - vTotMovSaldoInicial;

  Result := bOFX;
end;

function TOFXReader.InfLine ( sLine : string ): string;
var
   iTemp : integer;
begin
  Result := '';
  sLine := Trim(sLine);
  if FindString('>', sLine) then begin
    sLine := Trim(sLine);
    iTemp := Pos('>', sLine);
    if pos('</', sLine) > 0 then
      Result := copy(sLine, iTemp+1, pos('</', sLine)-iTemp-1)
    else
      // allows you to read the whole line when there is no completion of </ on the same line
      // made by weberdepaula@gmail.com
      Result := copy(sLine, iTemp+1, length(sLine));
  end;
end;

function TOFXReader.Add: TOFXItem;
var
  oItem : TOFXItem;
begin
  oItem := TOFXItem.Create;
  FListItems.Add(oItem);
  Result := oItem;
end;

function TOFXReader.FindString ( sSubString, sString : string ): boolean;
begin
  Result := Pos(UpperCase(sSubString), UpperCase(sString)) > 0;
end;

procedure Register;
begin
  RegisterComponents('OFXReader', [TOFXReader]);
end;

end.
