unit EditTelefoneLuka;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.StdCtrls, Vcl.Mask, Vcl.Graphics, _Biblioteca;

type
  TEditTelefoneLuka = class(TMaskEdit)
  private
    FCelular: Boolean;
    procedure FormatarMascara;
    procedure SetCelular(const pValor: Boolean);
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure validarTelefone;
  public
    constructor Create(AOwner: TComponent); override;
    function getTelefoneOk: Boolean;
  published
    property Celular: Boolean read FCelular write SetCelular default False;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('LD Sistemas', [TEditTelefoneLuka]);
end;

{ EditTelefoneLuka }

procedure TEditTelefoneLuka.CMEnter(var Message: TCMEnter);
begin
  if Self.Color = clWindow then
    Self.Color := $0080FFFF;
end;

procedure TEditTelefoneLuka.CMExit(var Message: TCMExit);
begin
  validarTelefone;
end;

constructor TEditTelefoneLuka.Create(AOwner: TComponent);
begin
  inherited;
  FormatarMascara;
end;

procedure TEditTelefoneLuka.FormatarMascara;
begin
  if FCelular then
    EditMask := '(99) 99999-9999;1; '
  else
    EditMask := '(99) 9999-9999;1; ';
end;

function TEditTelefoneLuka.getTelefoneOk: Boolean;
begin
  validarTelefone;
  Result := _Biblioteca.RetornaNumeros(Text) <> '';
end;

procedure TEditTelefoneLuka.SetCelular(const pValor: Boolean);
begin
  FCelular := pValor;
  FormatarMascara;
end;

procedure TEditTelefoneLuka.validarTelefone;
begin
  if Length(_Biblioteca.RetornaNumeros(Text)) < 10 then begin
    Text := '';
    FormatarMascara;
  end
  else if Length(_Biblioteca.RetornaNumeros(Text)) = 11 then
    EditMask := '(99) 99999-9999;1; '
  else if Length(_Biblioteca.RetornaNumeros(Text)) = 10 then
    EditMask := '(99) 9999-9999;1; ';

  if Self.Color = $0080FFFF then
    Self.Color := clWindow;
end;

end.
