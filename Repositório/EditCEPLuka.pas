unit EditCEPLuka;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.StdCtrls, Vcl.Mask, Vcl.Graphics, _Biblioteca;

type
  TEditCEPLuka = class(TMaskEdit)
  private
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
  public
    constructor Create(AOwner: TComponent); override;
    function getCepOk: Boolean;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('LD Sistemas', [TEditCEPLuka]);
end;

{ TEditCEPLuka }

procedure TEditCEPLuka.CMEnter(var Message: TCMEnter);
begin
  if Self.Color = clWindow then
    Self.Color := $0080FFFF;
end;

procedure TEditCEPLuka.CMExit(var Message: TCMExit);
begin
  if Length(_Biblioteca.RetornaNumeros(Text)) < 8 then
    Text := '';

  if Self.Color = $0080FFFF then
    Self.Color := clWindow;
end;

constructor TEditCEPLuka.Create(AOwner: TComponent);
begin
  inherited;
  EditMask := '99999-999';
end;

function TEditCEPLuka.getCepOk: Boolean;
begin
  Result := Length(_Biblioteca.RetornaNumeros(Text)) = 8;
end;

end.
