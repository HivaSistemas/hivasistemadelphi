unit Legendas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.CategoryButtons;

type
  TFormLegendas = class(TFormHerancaFinalizar)
    cbLegendas: TCategoryButtons;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    procedure AddLegenda(pCorLegenda: TColor; pTexto: string; pCorTexto: TColor);
  end;

implementation

{$R *.dfm}

{ TFormLegendas }

procedure TFormLegendas.AddLegenda(pCorLegenda: TColor; pTexto: string; pCorTexto: TColor);
var
  vBotao: TButtonCategory;
begin
  vBotao := TButtonCategory.Create(cbLegendas.Categories);

  vBotao.Color := pCorLegenda;
  vBotao.Caption := pTexto;
  vBotao.TextColor := pCorTexto;

  cbLegendas.Categories.AddItem(vBotao, cbLegendas.Categories.Count -1);
end;

procedure TFormLegendas.FormCreate(Sender: TObject);
begin
  inherited;
  cbLegendas.Categories.Clear;
end;

end.

