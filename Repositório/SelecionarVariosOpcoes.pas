unit SelecionarVariosOpcoes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.StdCtrls, _Biblioteca,
  Vcl.ExtCtrls, Vcl.Buttons;

type
  TFormSelecionarVariosOpcoes = class(TFormHerancaFinalizar)
    rgOpcoes: TRadioGroup;
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


function Selecionar(pDescricao: string; pOpcoes: TArray<string>; pOpcaoDefault: Integer = -1): TRetornoTelaFinalizar<Integer>;

implementation

{$R *.dfm}

function Selecionar(pDescricao: string; pOpcoes: TArray<string>; pOpcaoDefault: Integer = -1): TRetornoTelaFinalizar<Integer>;
var
  i: Integer;
  vForm: TFormSelecionarVariosOpcoes;
begin
  Result.Dados := -1;
  vForm := TFormSelecionarVariosOpcoes.Create(nil);

  vForm.rgOpcoes.Items.Clear;
  vForm.rgOpcoes.Caption := '  ' + pDescricao + '  ';

  for i := Low(pOpcoes) to High(pOpcoes) do
    vForm.rgOpcoes.Items.Add(pOpcoes[i]);

  vForm.rgOpcoes.ItemIndex := pOpcaoDefault;

  if Result.Ok(vForm.ShowModal, False) then
    Result.Dados := vForm.rgOpcoes.ItemIndex;
end;

procedure TFormSelecionarVariosOpcoes.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    sbFinalizarClick(Sender);
end;

end.
