unit ComboBoxLuka;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.StdCtrls, _RecordsEspeciais, Vcl.Graphics;

type
  TComboBoxLuka = class(TComboBox)
  private
    FValores: TStringList;
    procedure SetValores(const pValue: TStringList);
    procedure SetFiltros(const pFiltros: TArray<RecFiltros>);
    function getNaoFiltrar: Boolean;
  protected
    { Protected declarations }
  public
    constructor Create(AOwner: TComponent); override;

    procedure Clear; override;

    function GetValor: string;
    function GetValorInt: Integer;
    function GetValorDouble: Double;

    function GetIndice: Integer;
    procedure SetIndicePorValor(pValor: string); overload;
    procedure SetIndicePorValor(pValor: Integer); overload;

    destructor Destroy; override;
  published
    property Valores: TStringList read FValores write SetValores;
    property Filtros: TArray<RecFiltros> write SetFiltros;
    property AsInt: Integer read GetValorInt write SetIndicePorValor;
    property AsString: string read GetValor write SetIndicePorValor;
    property NaoFiltrar: Boolean read getNaoFiltrar;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('LD Sistemas', [TComboBoxLuka]);
end;

{ TComboBoxLuka }

procedure TComboBoxLuka.Clear;
begin
  inherited;

  ItemIndex := 0;
end;

constructor TComboBoxLuka.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FValores := TStringList.Create;

  Color := clWhite;
  BevelKind := bkFlat;
  Style := csOwnerDrawFixed;
end;

destructor TComboBoxLuka.Destroy;
begin
  FreeAndNil(FValores);
  inherited Destroy;
end;

function TComboBoxLuka.GetIndice: Integer;
begin
  Result := StrToInt(GetValor);
end;

function TComboBoxLuka.getNaoFiltrar: Boolean;
begin
  Result := LowerCase(GetValor) = 'naofiltrar';
end;

function TComboBoxLuka.GetValorInt: Integer;
begin
  Result := StrToIntDef(GetValor, 0);
end;

function TComboBoxLuka.GetValorDouble: Double;
begin
  Result := StrToFloatDef(GetValor, 0);
end;

function TComboBoxLuka.GetValor: string;
begin
  Result := '';

  if ItemIndex = -1 then
    Exit;

  if FValores.Count = 0 then
    Exit;

  Result := FValores[ItemIndex];
end;

procedure TComboBoxLuka.SetFiltros(const pFiltros: TArray<RecFiltros>);
var
  i: Integer;
begin
  for i := Low(pFiltros) to High(pFiltros) do begin
    if pFiltros[i].DescricaoFiltro = '' then
      Continue;

    Items.Add(pFiltros[i].DescricaoFiltro);
    FValores.Add(IntToStr(i));

    if pFiltros[i].FiltroPadrao then
      ItemIndex := pFiltros[i].indice;
  end;
end;

procedure TComboBoxLuka.SetIndicePorValor(pValor: Integer);
begin
  SetIndicePorValor(IntToStr(pValor));
end;

procedure TComboBoxLuka.SetIndicePorValor(pValor: string);
begin
  if FValores.Count > 0 then
    ItemIndex := FValores.IndexOf(pValor)
  else
    ItemIndex := Items.IndexOf(pValor);
end;

procedure TComboBoxLuka.SetValores(const pValue: TStringList);
begin
  FValores.Assign(pValue);
end;

end.
