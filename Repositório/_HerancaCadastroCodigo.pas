unit _HerancaCadastroCodigo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaBarra, Vcl.StdCtrls, EditLuka, _HerancaCadastro,
  Vcl.Buttons, Vcl.ExtCtrls, CheckBoxLuka;

type
  TFormHerancaCadastroCodigo = class(TFormHerancaCadastro)
    Label1: TLabel;
    eID: TEditLuka;
    ckAtivo: TCheckBoxLuka;
    procedure eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); virtual;
  protected
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

procedure TFormHerancaCadastroCodigo.eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then begin
    if eID.Text <> '' then
      BuscarRegistro
    else
      Modo(True);
  end;
end;

procedure TFormHerancaCadastroCodigo.Modo(pEditando: Boolean);
begin
  inherited;

  eID.Enabled := not pEditando;
  ckAtivo.Enabled := pEditando;
  ckAtivo.Checked := True;

  if not pEditando then begin
    eID.Clear;
    Self.ActiveControl := eID;
  end;
end;

end.
