unit _HerancaBarra;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPrincipal, Vcl.ExtCtrls;

type
  TFormHerancaBarra = class(TFormHerancaPrincipal)
    pnOpcoes: TPanel;
  protected
    procedure VerificarRegistro(Sender: TObject); virtual; abstract;
    procedure BuscarFiltrosTela; virtual;
    procedure SalvarFiltrosTela; virtual;
  end;

implementation

{$R *.dfm}

uses 
  _FiltrosPadroesTelasUsuario, _Sessao, Vcl.StdCtrls, Vcl.ComCtrls, _FrameHerancaPrincipal, 
  GridLuka, StaticTextLuka, _Biblioteca, _RecordsEspeciais, _ColunasPadroesTelasUsuario;

{ TFormHerancaBarra }

procedure TFormHerancaBarra.BuscarFiltrosTela;
var
  i: Integer;
  j: Integer;
  k: Integer;
  vColuna: string;
  vFiltros: TArray<RecFiltrosPadroesTelasUsuario>;
  vColunas: TArray<RecColunasPadroesTelasUsuario>;
begin
  vFiltros := _FiltrosPadroesTelasUsuario.BuscarFiltrosPadroes(Sessao.getConexaoBanco, 0, [Sessao.getUsuarioLogado.funcionario_id, LowerCase(Self.ClassName)]);
  vColunas := _ColunasPadroesTelasUsuario.BuscarColunasPadroes(Sessao.getConexaoBanco, 0, [Sessao.getUsuarioLogado.funcionario_id, LowerCase(Self.ClassName)]);

  if (vFiltros = nil) and (vColunas = nil) then
    Exit;

  for i := 0 to Self.ComponentCount -1 do begin
    if Self.Components[i].Tag <> 999 then
      Continue;

    for j := Low(vFiltros) to High(vFiltros) do begin
      if vFiltros[j].Campo <> LowerCase(Self.Components[i].Name) then
        Continue;

      if Self.Components[i] is TCustomEdit then
        TCustomEdit(Self.Components[i]).Text := vFiltros[j].ValorPesquisa
      else if Self.Components[i] is TComboBoxEx then
        TComboBoxEx(Self.Components[i]).Text := vFiltros[j].ValorPesquisa
      else if Self.Components[i] is TCustomListControl then
        TCustomListControl(Self.Components[i]).ItemIndex := StrToInt(vFiltros[j].ValorPesquisa)
      else if Self.Components[i] is TRadioGroup then
        TRadioGroup(Self.Components[i]).ItemIndex := StrToInt(vFiltros[j].ValorPesquisa)
      else if Self.Components[i] is TCheckBox then
        TCheckBox(Self.Components[i]).Checked := vFiltros[j].ValorPesquisa = 'S';

      Break;
    end;
  end;

  if vColunas = nil then
    Exit;

  for i := 0 to Self.ComponentCount -1 do begin

    if (Self.Components[i].Tag <> 999) and not (Self.Components[i] is TGridLuka) then
      Continue;

    for j := TGridLuka(Components[i]).FixedCols to TGridLuka(Components[i]).ColCount - 1 do begin

      for k := Low(vColunas) to High(vColunas) do begin

        vColuna := _Biblioteca.RemoverCaracteresEspeciais(Trim(TGridLuka(Components[i]).Cells[j, TGridLuka(Components[i]).FixedRows - 1]));

        if vColuna = vColunas[k].Coluna then begin
          TGridLuka(Components[i]).ColWidths[j] := vColunas[k].Tamanho;
          Break;
        end;
      end;
    end;
  end;
end;

procedure TFormHerancaBarra.SalvarFiltrosTela;
var
  i: Integer;
  j: Integer;
  vRetBanco: RecRetornoBD;
  vFiltros: TArray<RecFiltrosPadroesTelasUsuario>;
  vColunas: TArray<RecColunasPadroesTelasUsuario>;
begin
  for i := 0 to Self.ComponentCount -1 do begin
    if Self.Components[i].Tag <> 999 then
      Continue;

    if (Self.Components[i] is TCustomEdit) and (TCustomEdit(Self.Components[i]).Text <> '') then begin
      SetLength(vFiltros, Length(vFiltros) + 1);
      vFiltros[High(vFiltros)].Campo := LowerCase(Self.Components[i].Name);
      vFiltros[High(vFiltros)].Ordem := TCustomEdit(Self.Components[i]).TabOrder;
      vFiltros[High(vFiltros)].ValorPesquisa := TCustomEdit(Self.Components[i]).Text;
    end
    else if (Self.Components[i] is TComboBoxEx) and (TComboBoxEx(Self.Components[i]).Text <> '') then begin
      SetLength(vFiltros, Length(vFiltros) + 1);
      vFiltros[High(vFiltros)].Campo := LowerCase(Self.Components[i].Name);
      vFiltros[High(vFiltros)].Ordem := TComboBoxEx(Self.Components[i]).TabOrder;
      vFiltros[High(vFiltros)].ValorPesquisa := TComboBoxEx(Self.Components[i]).Text;
    end
    else if (Self.Components[i] is TCustomListControl) and (TCustomListControl(Self.Components[i]).ItemIndex > -1) then begin
      SetLength(vFiltros, Length(vFiltros) + 1);
      vFiltros[High(vFiltros)].Campo := LowerCase(Self.Components[i].Name);
      vFiltros[High(vFiltros)].Ordem := TCustomListControl(Self.Components[i]).TabOrder;
      vFiltros[High(vFiltros)].ValorPesquisa := IntToStr(TCustomListControl(Self.Components[i]).ItemIndex);
    end
    else if (Self.Components[i] is TRadioGroup) and (TRadioGroup(Self.Components[i]).ItemIndex > -1) then begin
      SetLength(vFiltros, Length(vFiltros) + 1);
      vFiltros[High(vFiltros)].Campo := LowerCase(Self.Components[i].Name);
      vFiltros[High(vFiltros)].Ordem := TRadioGroup(Self.Components[i]).TabOrder;
      vFiltros[High(vFiltros)].ValorPesquisa := IntToStr(TRadioGroup(Self.Components[i]).ItemIndex);
    end
    else if Self.Components[i] is TCheckBox then begin
      SetLength(vFiltros, Length(vFiltros) + 1);
      vFiltros[High(vFiltros)].Campo := LowerCase(Self.Components[i].Name);
      vFiltros[High(vFiltros)].Ordem := TCheckBox(Self.Components[i]).TabOrder;
      vFiltros[High(vFiltros)].ValorPesquisa := IIfStr(TCheckBox(Self.Components[i]).Checked, 'S', 'N');
    end
    else if Self.Components[i] is TGridLuka then begin
      for j := TGridLuka(Components[i]).FixedCols to TGridLuka(Components[i]).ColCount - 1 do begin
        SetLength(vColunas, Length(vColunas) + 1);
        vColunas[High(vColunas)].Campo   := LowerCase(Self.Components[i].Name);
        vColunas[High(vColunas)].Coluna  := _Biblioteca.RemoverCaracteresEspeciais(Trim(TGridLuka(Components[i]).Cells[j, TGridLuka(Components[i]).FixedRows - 1]));
        vColunas[High(vColunas)].Tamanho := TGridLuka(Components[i]).ColWidths[j];
      end;

      vRetBanco := _ColunasPadroesTelasUsuario.AtualizarColunasPadroes(
        Sessao.getConexaoBanco,
        LowerCase(Self.ClassName),
        Sessao.getUsuarioLogado.funcionario_id,
        vColunas
      );

      if vRetBanco.TeveErro then
        _Biblioteca.Exclamar('N�o foi poss�vel gravar configura��es de colunas da tela!' + #13#10 + vRetBanco.MensagemErro);
    end;

  end;

  vRetBanco := _FiltrosPadroesTelasUsuario.AtualizarFiltrosPadroes(
    Sessao.getConexaoBanco,
    LowerCase(Self.ClassName),
    Sessao.getUsuarioLogado.funcionario_id,
    vFiltros
  );

  if vRetBanco.TeveErro then
    _Biblioteca.Exclamar('N�o foi poss�vel gravar configura��es da tela!' + #13#10 + vRetBanco.MensagemErro);
end;

end.
