inherited FormHerancaPesquisas: TFormHerancaPesquisas
  Margins.Left = 1
  Margins.Top = 1
  Margins.Right = 1
  Margins.Bottom = 1
  BorderStyle = bsDialog
  BorderWidth = 1
  Caption = 'Heran'#231'as Pesquisas'
  ClientHeight = 300
  ClientWidth = 646
  PrintScale = poPrintToFit
  SnapBuffer = 0
  Visible = False
  OnShow = FormShow
  ExplicitWidth = 654
  ExplicitHeight = 331
  PixelsPerInch = 96
  TextHeight = 14
  object sgPesquisa: TGridLuka
    Left = 0
    Top = 46
    Width = 646
    Height = 254
    Align = alClient
    ColCount = 3
    Ctl3D = True
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRowSelect, goThumbTracking]
    ParentCtl3D = False
    ScrollBars = ssVertical
    TabOrder = 1
    OnDblClick = sgPesquisaDblClick
    OnEnter = sgPesquisaEnter
    OnKeyDown = sgPesquisaKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Sel?'
      'C'#243'digo')
    OnGetCellPicture = sgPesquisaGetCellPicture
    Grid3D = False
    RealColCount = 3
    AtivarPopUpSelecao = False
    ColWidths = (
      28
      55
      199)
  end
  object eQuantidadeDigitosPesquisa: TEdit
    Left = 16
    Top = 240
    Width = 33
    Height = 22
    Color = clRed
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    NumbersOnly = True
    ParentFont = False
    TabOrder = 2
    Visible = False
  end
  object pnFiltro1: TPanel
    Left = 0
    Top = 0
    Width = 646
    Height = 46
    Align = alTop
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 0
    DesignSize = (
      646
      46)
    object lblPesquisa: TLabel
      Left = 202
      Top = 4
      Width = 60
      Height = 14
      Caption = 'Descri'#231#227'o:'
    end
    object lblOpcoesPesquisa: TLabel
      Left = 2
      Top = 4
      Width = 100
      Height = 14
      Caption = 'Tipo de pesquisa:'
    end
    object eValorPesquisa: TEditLuka
      Left = 202
      Top = 18
      Width = 442
      Height = 22
      Anchors = [akLeft, akTop, akRight]
      CharCase = ecUpperCase
      Ctl3D = True
      ParentCtl3D = False
      TabOrder = 1
      OnKeyDown = eValorPesquisaKeyDown
      TipoCampo = tcTexto
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 0
      NaoAceitarEspaco = False
    end
    object cbOpcoesPesquisa: TComboBoxLuka
      Left = 2
      Top = 18
      Width = 197
      Height = 22
      BevelKind = bkFlat
      Style = csOwnerDrawFixed
      Color = clSkyBlue
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      OnKeyDown = ProximoCampo
      AsInt = 0
    end
  end
end
