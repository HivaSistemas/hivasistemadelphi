unit GroupBoxLuka;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.StdCtrls, Vcl.Menus, CheckBoxLuka, _BibliotecaGenerica;

type
  TGroupBoxLuka = class(TGroupBox)
  private
    FOpcaoMarcarDesmarcar: Boolean;

    pmOpcoes: TPopupMenu;
    miMarcarTodos: TMenuItem;
    miDesmarcarTodos: TMenuItem;

    procedure MarcarDesmarcarClick(Sender: TObject);
    procedure setMarcarDesmarcar(const pValue: Boolean);
  public
    constructor Create(AOwner: TComponent); override;
    function NenhumMarcado: Boolean;
    function TodosMarcados: Boolean;
    function getQuantidadeMarcados: Word;
    function GetSql(pColuna: string): string;
  published
    property OpcaoMarcarDesmarcar: Boolean read FOpcaoMarcarDesmarcar write setMarcarDesmarcar;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('LD Sistemas', [TGroupBoxLuka]);
end;

{ TGroupBoxLuka }

constructor TGroupBoxLuka.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

function TGroupBoxLuka.getQuantidadeMarcados: Word;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to ControlCount -1 do begin
    if Controls[i] is TCheckBox then begin
      if TCheckBox(Controls[i]).Checked then
        Inc(Result);
    end;
  end;
end;

function TGroupBoxLuka.GetSql(pColuna: string): string;
var
  i: Integer;
begin
  Result := '';

  if NenhumMarcado then
    Exit;

  for i := 0 to Self.ControlCount - 1 do begin
    if Controls[i] is TCheckBoxLuka then begin
      if TCheckBoxLuka(Controls[i]).Checked then
        _BibliotecaGenerica.AddVirgulaSeNecessario(Result, '''' + TCheckBoxLuka(Controls[i]).Valor + '''');
    end;
  end;
  Result := ' ' + pColuna + ' in(' + Result + ') ';
end;

procedure TGroupBoxLuka.MarcarDesmarcarClick(Sender: TObject);
var
  i: Integer;
  marcar: Boolean;
begin
  marcar := Pos('Marcar todos', TMenuItem(Sender).Caption) > 0;
  for i := 0 to Self.ControlCount - 1 do begin
    if Controls[i] is TCheckBox then
      TCheckBox(Controls[i]).Checked := marcar;
  end;
end;

function TGroupBoxLuka.NenhumMarcado: Boolean;
var
  i: Integer;
begin
  Result := True;
  for i := 0 to ControlCount -1 do begin
    if Controls[i] is TCheckBox then begin
      if TCheckBox(Controls[i]).Checked then begin
        Result := False;
        Break;
      end;
    end;
  end;
end;

procedure TGroupBoxLuka.setMarcarDesmarcar(const pValue: Boolean);
begin
  FOpcaoMarcarDesmarcar := pValue;
  if FOpcaoMarcarDesmarcar then begin
    pmOpcoes := TPopupMenu.Create(Self);

    miMarcarTodos := TMenuItem.Create(pmOpcoes);
    miMarcarTodos.Caption := 'Marcar todos';
    miMarcarTodos.OnClick := MarcarDesmarcarClick;

    pmOpcoes.Items.Add(miMarcarTodos);

    miDesmarcarTodos := TMenuItem.Create(pmOpcoes);
    miDesmarcarTodos.Caption := 'Desmarcar todos';
    miDesmarcarTodos.OnClick := MarcarDesmarcarClick;

    pmOpcoes.Items.Add(miDesmarcarTodos);

    PopupMenu := pmOpcoes;
  end;
end;

function TGroupBoxLuka.TodosMarcados: Boolean;
var
  i: Integer;
  vQtde: Integer;
begin
  vQtde := 0;
  for i := 0 to ControlCount -1 do begin
    if Controls[i] is TCheckBox then
      Inc(vQtde);
  end;

  Result := vQtde = getQuantidadeMarcados;
end;

end.
