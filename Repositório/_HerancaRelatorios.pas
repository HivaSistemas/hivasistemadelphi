unit _HerancaRelatorios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaBarra, Vcl.Buttons, Vcl.ExtCtrls, _Biblioteca,
  _FrameHenrancaPesquisas, Vcl.StdCtrls, CheckBoxLuka, FrameDataInicialFinal, _Sessao;

type
  TFormHerancaRelatorios = class(TFormHerancaBarra)
    sbCarregar: TSpeedButton;
    sbImprimir: TSpeedButton;
    ckOmitirFiltros: TCheckBoxLuka;
    procedure sbImprimirClick(Sender: TObject);
    procedure sbCarregarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FCarregou: Boolean;
  protected
    FFiltrosUtilizados: TArray<string>;

    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); virtual;
    procedure Carregar(Sender: TObject); virtual;
    procedure sbGerarPlanilhaClick(Sender: TObject);
  end;

implementation

{$R *.dfm}

procedure TFormHerancaRelatorios.sbImprimirClick(Sender: TObject);
begin
  inherited;
  Imprimir(Sender);
end;

procedure TFormHerancaRelatorios.sbGerarPlanilhaClick(Sender: TObject);
begin
  inherited;

end;

procedure TFormHerancaRelatorios.Carregar(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  FFiltrosUtilizados := nil;

  if ckOmitirFiltros.Checked then
    Exit;

  for i := 0 to ComponentCount -1 do begin
    if Self.Components[i] is TFrameHenrancaPesquisas then begin
      if TFrameHenrancaPesquisas(Self.Components[i]).EstaVazio then
        Continue;

      FFiltrosUtilizados := FFiltrosUtilizados + TFrameHenrancaPesquisas(Self.Components[i]).GetFiltrosUtilizados;
    end
    else if Self.Components[i] is TFrDataInicialFinal then begin
      if TFrDataInicialFinal(Self.Components[i]).NenhumaDataValida then
        Continue;

      FFiltrosUtilizados := FFiltrosUtilizados + TFrDataInicialFinal(Self.Components[i]).GetFiltrosUtilizados;
    end;
  end;
end;

procedure TFormHerancaRelatorios.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if (not Sessao.FinalizandoPorTempoInativo) and FCarregou and not _Biblioteca.Perguntar('Deseja realmente sair?') then
    Abort;

  FFiltrosUtilizados := nil;
  inherited;
end;

procedure TFormHerancaRelatorios.FormCreate(Sender: TObject);
begin
  inherited;
  FCarregou := False;
end;

procedure TFormHerancaRelatorios.Imprimir(Sender: TObject);
begin
  inherited;
  if ckOmitirFiltros.Checked then
    FFiltrosUtilizados := nil;
end;

procedure TFormHerancaRelatorios.sbCarregarClick(Sender: TObject);
begin
  inherited;
  VerificarRegistro(Sender);
  Carregar(Sender);
  FCarregou := True;
end;

procedure TFormHerancaRelatorios.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
