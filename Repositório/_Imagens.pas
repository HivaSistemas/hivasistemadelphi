unit _Imagens;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Imaging.pngimage;

type
  TFormImagens = class(TForm)
    im1: TImage;
    im2: TImage;
    im3: TImage;
    im4: TImage;
    im5: TImage;
    im6: TImage;
    im7: TImage;
    im8: TImage;
    im9: TImage;
    im10: TImage;
    im11: TImage;
    im12: TImage;
    im13: TImage;
    im14: TImage;
    im15: TImage;
    im16: TImage;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function getGlyph(AOwner: TComponent; pTag: Integer): TBitmap;

var
  FormImagens: TFormImagens;

implementation

{$R *.dfm}

function getGlyph(AOwner: TComponent; pTag: Integer): TBitmap;
var
  i: Integer;
  vForm: TFormImagens;
begin
  Result := nil;
  vForm := TFormImagens.Create(AOwner);
  for i := 0 to vForm.ControlCount - 1 do begin
    if vForm.Controls[i] is TImage then begin
      if pTag = TImage(vForm.Controls[i]).Tag then begin
        Result := TImage(vForm.Controls[i]).Picture.Bitmap;
        Break;
      end;
    end;
  end;
end;

end.
