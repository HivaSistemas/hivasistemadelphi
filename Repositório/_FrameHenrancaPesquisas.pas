unit _FrameHenrancaPesquisas;

interface

uses
  Buttons, GridLuka, Menus, StdCtrls, Controls, Grids, Forms, Classes, Graphics, Windows, SysUtils, Variants,
  _FrameHerancaPrincipal, ExtCtrls, System.StrUtils, System.Math, BuscaDados, _Biblioteca;

type
  TFrameHenrancaPesquisas = class(TFrameHerancaPrincipal)
    CkAspas: TCheckBox;
    CkFiltroDuplo: TCheckBox;
    CkPesquisaNumerica: TCheckBox;
    sgPesquisa: TGridLuka;
    CkMultiSelecao: TCheckBox;
    PnTitulos: TPanel;
    CkChaveUnica: TCheckBox;
    lbNomePesquisa: TLabel;
    pnPesquisa: TPanel;
    sbPesquisa: TSpeedButton;
    ckSomenteAtivos: TCheckBox;
    pnSuprimir: TPanel;
    ckSuprimir: TCheckBox;
    poOpcoes: TPopupMenu;
    miArquivo: TMenuItem;
    miSalvarTxt: TMenuItem;
    miCarregarTxt: TMenuItem;
    miExcluirTodos: TMenuItem;
    miExcluirSelecionado: TMenuItem;

    procedure SgConsultaKeyPress(Sender: TObject; var Key: Char);
    procedure sgPesquisaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont); virtual;
    procedure sbPesquisaClick(Sender: TObject);
    procedure miSalvarTxtClick(Sender: TObject);
    procedure miCarregarTxtClick(Sender: TObject);
    procedure miExcluirTodosClick(Sender: TObject);
    procedure miExcluirSelecionadoClick(Sender: TObject);
    procedure poOpcoesPopup(Sender: TObject);
  private
    FJogarFoco: Boolean;
    FDiretoTodos: Boolean; // esta vari�vel define se o procedimento de AdicionarDireto foi de apenas um registro ou de v�rios.
    FSomenteLeitura: Boolean;

    FOnAntesPesquisarEvent: TEventoObjeto;
    FOnAposPesquisarEvent: TEventoObjeto;
    FOnAntesDeletarEvent: TEventoObjeto;
    FOnAposDeletarEvent: TEventoObjeto;

    procedure Excluir;
    procedure ExcluirTodos;
    procedure Selecionou;
    procedure LimparDados;
    procedure AjustarUltimaColuna;
    procedure Pesquisar(pSender:TObject; pDispararSelecionou: Boolean);
    procedure InserirDado(pSender: TObject; pDispararSelecionou: Boolean; pMontarDados: Boolean);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Clear; override;
    procedure SetFocus; override;
    procedure SomenteLeitura(pValor: Boolean); override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
    procedure InserirDadoPorChave(pChave: string; pDispararSelecionou: Boolean = True); overload;
    procedure InserirDadoPorChave(pChave: Integer; pDispararSelecionou: Boolean = True); overload;
    procedure InserirDadoPorChaveTodos(pChaves: TArray<string>; pDispararSelecionou: Boolean = True); overload;
    procedure InserirDadoPorChaveTodos(pChaves: TArray<Integer>; pDispararSelecionou: Boolean = True); overload;

    function getQuantidadeLinhas:Integer;
    function EstaVazio: Boolean; override;
    function GetArrayOfString: TArray<string>;
    function GetArrayOfInteger: TArray<Integer>;

    function getSqlFiltros: string; overload; virtual;
    function getSqlFiltros(pPrimeiraColunaPesquisar: string; var pSqlAtual: string; pWhereOuAnd: Boolean = True): string; overload; virtual;
    function getSqlFiltros(pPrimeiraColunaPesquisar: string; pSegundaColunaPesquisar: string = ''): string; overload; virtual;

    function Coluna(pColuna: Integer; pLinha: Integer = -1): string;
    function GetFiltrosUtilizados: TArray<string>;
  protected
    FDados: TList;
    FDigitouChave: Boolean;
    FChaveDigitada: string;
    FColunaPesquisa: string;

    function AdicionarDireto: TObject; virtual;
    function AdicionarDiretoTodos: TArray<TObject>; virtual;

    function AdicionarPesquisando: TObject; virtual;
    function AdicionarPesquisandoTodos: TArray<TObject>; virtual;

    procedure MontarGrid; virtual; abstract;

    function TratarStr(pString: string): string; virtual;
    function IndiceChave(pSender: TObject): Integer; virtual;
    function TratarTextoDigitado(pString: string): string; virtual;

    procedure Loaded; override;
  published
  	{ Published declarations }

    property OnAntesPesquisar: TEventoObjeto read FOnAntesPesquisarEvent write FOnAntesPesquisarEvent;
    property OnAposPesquisar: TEventoObjeto read FOnAposPesquisarEvent write FOnAposPesquisarEvent;
    property OnAntesDeletar: TEventoObjeto read FOnAntesDeletarEvent write FOnAntesDeletarEvent;
    property OnAposDeletar: TEventoObjeto read FOnAposDeletarEvent write FOnAposDeletarEvent;
  end;

implementation

{$R *.dfm}

constructor TFrameHenrancaPesquisas.Create(AOwner:TComponent);
begin
  inherited;

  FDados := TList.Create;

  sgPesquisa.OnKeyDown := sgPesquisaKeyDown;
  sgPesquisa.OnKeyPress := SgConsultaKeyPress;

  FJogarFoco := True;
  FColunaPesquisa := '';
  FDiretoTodos := False;
  FDigitouChave := False;
end;

destructor TFrameHenrancaPesquisas.Destroy;
begin
  LimparDados;
  FDados.Free;
  inherited;
end;

procedure TFrameHenrancaPesquisas.Modo(pEditando: Boolean; pLimpar: Boolean = True);
begin
  inherited;

  sgPesquisa.EditorMode := pEditando;
  sgPesquisa.Enabled := pEditando;
  sbPesquisa.Enabled := pEditando;
  ckSuprimir.Enabled := pEditando;

  if pLimpar then
  	Clear;
end;

function TFrameHenrancaPesquisas.EstaVazio: Boolean;
begin
  Result := (FDados.Count = 0);
end;

procedure TFrameHenrancaPesquisas.Excluir;
begin
  if not EstaVazio then begin

  	if Assigned(FOnAntesDeletarEvent) then
    	FOnAntesDeletarEvent(Self);

    TObject(FDados[sgPesquisa.Row]).Free;
    FDados.Delete(sgPesquisa.Row);
    MontarGrid;

    if Assigned(FOnAposDeletarEvent) then
    	FOnAposDeletarEvent(Self);
  end;
end;

procedure TFrameHenrancaPesquisas.miExcluirSelecionadoClick(Sender: TObject);
begin
  inherited;
  Excluir;
end;

procedure TFrameHenrancaPesquisas.ExcluirTodos;
begin
  sgPesquisa.ClearGrid(0);
  FDados.Clear;
end;


function TFrameHenrancaPesquisas.getSqlFiltros: string;
begin
  Result := getSqlFiltros('');
end;

function TFrameHenrancaPesquisas.getSqlFiltros(pPrimeiraColunaPesquisar: string; var pSqlAtual: string; pWhereOuAnd: Boolean = True): string;
begin
  Result := getSqlFiltros(pPrimeiraColunaPesquisar);

  if (Result <> '') and pWhereOuAnd then
    _Biblioteca.WhereOuAnd(pSqlAtual, Result);
end;

function TFrameHenrancaPesquisas.getSqlFiltros(pPrimeiraColunaPesquisar: string; pSegundaColunaPesquisar: string = ''): string;
var
  i: Integer;
  coluna_pesquisar: string;

  function TrazerColuna(pColuna: Integer; _indice: Integer): string;
  begin
    Result := IfThen(ckAspas.Checked, '''') + TratarStr(Coluna(pColuna, _indice)) + IfThen(ckAspas.Checked, '''')
  end;

begin
  Result := '';

  if EstaVazio then
    Exit;

  with sgPesquisa do begin
    if EstaVazio then
      Exit;

    if (RowCount = 1) and (not ckFiltroDuplo.Checked) then begin
      Result := IfThen(pPrimeiraColunaPesquisar <> '', ' ' + pPrimeiraColunaPesquisar) + IIfStr(ckSuprimir.Checked, ' <> ', ' = ') + TrazerColuna(0, 0);
      Exit;
    end;

    coluna_pesquisar := IfThen(ckFiltroDuplo.Checked, ' (' + pPrimeiraColunaPesquisar + ' , ' + pSegundaColunaPesquisar + ') ', pPrimeiraColunaPesquisar);
    Result := coluna_pesquisar + IIfStr(ckSuprimir.Checked, ' not') + ' in (';

    for i := 0 to RowCount - 1 do begin
      if (coluna_pesquisar <> '') and ((i mod 1000) = 0) and (i > 0) then
        Result := Result + ') ' + IIfStr(ckSuprimir.Checked, ' and not ', ' or ') + coluna_pesquisar + ' in ('
      else if i > 0 then
        Result := Result + ',';

      Result := Result + IfThen(ckFiltroDuplo.Checked, '(' + TrazerColuna(0, i) + ',' + TrazerColuna(1, i) + ') ', TrazerColuna(0, i));
    end;
    Result := Result + ') ';
  end;

  if Result <> '' then begin
    Result := Result + ' ';

    if pPrimeiraColunaPesquisar <> '' then
      Result := '(' + Result + ') ';
  end;
end;

function TFrameHenrancaPesquisas.Coluna(pColuna: Integer; pLinha: Integer = -1): string;
begin
  Result := sgPesquisa.Cells[pColuna, IfThen(pLinha = -1, sgPesquisa.Row, pLinha)];
end;

function TFrameHenrancaPesquisas.getQuantidadeLinhas: Integer;
begin
  if EstaVazio then
    Result := 0
  else
    Result := sgPesquisa.RowCount;
end;

procedure TFrameHenrancaPesquisas.Loaded;
begin
  inherited;
  AjustarUltimaColuna;
end;

procedure TFrameHenrancaPesquisas.miExcluirTodosClick(Sender: TObject);
begin
  inherited;
  ExcluirTodos;
end;

procedure TFrameHenrancaPesquisas.miSalvarTxtClick(Sender: TObject);
var
  vLinha: Integer;
  vArquivo: string;
  vCaminho: string;
begin
  inherited;
  vArquivo := '';

  for vLinha := 0 to sgPesquisa.RowCount -1 do
    vArquivo := vArquivo + sgPesquisa.Cells[0, vLinha] + ';';

  vCaminho := _Biblioteca.getNomeCaminhoArquivoSalvar('txt', 'Caminho arquivo');
  salvarStringEmAquivo(vArquivo, vCaminho);
end;

procedure TFrameHenrancaPesquisas.sgPesquisaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (not Enabled) or FSomenteLeitura then
    Exit;

  if (Shift = [ssShift]) and (Key = VK_TAB) then begin
    keybd_event(VK_SHIFT, 0, KEYEVENTF_KEYUP, 0);// preciona o ALT
    keybd_event(VK_TAB, 0, 0, 0);// Preciona o TAB
    keybd_event(VK_SHIFT, 0, KEYEVENTF_KEYUP, 0); //Solta o ALT
    keybd_event(VK_TAB, 0, KEYEVENTF_KEYUP, 0); //Solta o TAB
  end
  else if Key = VK_RETURN then begin
    if CkChaveUnica.Checked then begin
      if EstaVazio then
        Pesquisar(Sender, True)
      else
        Keybd_Event(VK_TAB, 0, 0, 0);
    end
    else
      Pesquisar(Sender, True);
  end
  else if (Shift = [ssCtrl]) and (Key = VK_DELETE) then
    ExcluirTodos
  else if Key = VK_DELETE then
    Excluir
  else if (Shift = [ssCtrl]) and (Key in [Ord('S'), Ord('s')]) then
    miSalvarTxtClick(Sender)
  else if (Shift = [ssCtrl]) and (Key in [Ord('C'), Ord('c')]) then
    miCarregarTxtClick(Sender);
end;


procedure TFrameHenrancaPesquisas.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
	sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taLeftJustify, Rect);
end;

procedure TFrameHenrancaPesquisas.sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
	if sgPesquisa.Focused then
  	ABrush.Color := clWhite
  else
  	ABrush.Color := sgPesquisa.Color;
end;

procedure TFrameHenrancaPesquisas.Selecionou;
begin
  SetarFoco(sgPesquisa);
  FJogarFoco := True;
end;

procedure TFrameHenrancaPesquisas.SgConsultaKeyPress(Sender: TObject; var Key: Char);
begin
  if (not lbNomePesquisa.Enabled) or FSomenteLeitura then
  	Exit;
    
  if not CkPesquisaNumerica.Checked then begin
    if CharInSet(Key, ['0'..'9', 'A'..'Z', 'a'..'z' , '.', '-' , '?']) then begin
      FChaveDigitada := BuscaDados.BuscarDados(tiTexto, 'Busca digitada', Key);
      if FChaveDigitada <> '' then begin
        FDigitouChave := True;
        Pesquisar(nil, True);
      end
      else
        SetFocus;
    end;
  end
  else begin
    if CharInSet(Key, ['0'..'9']) then begin
      FChaveDigitada := BuscaDados.BuscarDados(tiNumerico, 'Busca digitada', Key);
      if StrToIntDef(FChaveDigitada, 0) > 0 then begin
        FDigitouChave := True;
        Pesquisar(nil, True);
      end
      else
        SetFocus;
    end;
  end;
end;

procedure TFrameHenrancaPesquisas.SomenteLeitura(pValor: Boolean);
begin
  inherited;
  AjustarUltimaColuna;
  FSomenteLeitura := pValor;

  _Biblioteca.Habilitar([sbPesquisa], not pValor);
end;

procedure TFrameHenrancaPesquisas.Pesquisar(pSender: TObject; pDispararSelecionou: Boolean);
var
  i: Integer;
  pesquisou: Boolean;
  vet: TArray<TObject>;
begin

  if Assigned(FOnAntesPesquisarEvent) then
  	FOnAntesPesquisarEvent(Self);

  vet := nil;
  pesquisou := False;

  if pSender = nil then begin
  	if FDiretoTodos then
    	vet := AdicionarDiretoTodos
    else begin
    	SetLength(vet, 1);
      vet[0] := AdicionarDireto;
    end;
  end
  else begin
    // Se for multisele��o e n�o for chave �nica, habilitando a busca
    if CkMultiSelecao.Checked and not CkChaveUnica.Checked then
      vet := AdicionarPesquisandoTodos
    else begin
      SetLength(vet, 1);
      vet[0] := AdicionarPesquisando;
    end;
  end;

  if (vet <> nil) and (vet[0] <> nil) then begin
    pesquisou := True;

  	for i := Low(vet) to High(vet) do
    	InserirDado(vet[i], pDispararSelecionou, i = High(vet));
  end
  else if pDispararSelecionou then
    SetFocus;

  FDiretoTodos := False;
  FDigitouChave := False;

  if pesquisou and Assigned(FOnAposPesquisarEvent) then
  	FOnAposPesquisarEvent(Self);
end;

procedure TFrameHenrancaPesquisas.poOpcoesPopup(Sender: TObject);
begin
  inherited;
  _Biblioteca.VisibilidadeTMenu(
    [
      miSalvarTxt,
      miCarregarTxt,
      miExcluirTodos,
      miArquivo
    ],
    not CkChaveUnica.Checked
  );
end;

procedure TFrameHenrancaPesquisas.miCarregarTxtClick(Sender: TObject);
var
  i: Integer;
  vValor: string;
  vArquivo: string;
begin
  inherited;
  vValor := '';

  vArquivo := _Biblioteca.getNomeCaminhoArquivoCarregar('txt', 'Caminho arquivo');
  vArquivo := _Biblioteca.carregarAquivoEmString(vArquivo);

  for i := Low(vArquivo) to High(vArquivo) do begin

    if vArquivo[i] = ';' then begin
      InserirDadoPorChave(vValor);
      vValor := '';
    end
    else
      vValor := vValor + vArquivo[i];
  end;
end;

procedure TFrameHenrancaPesquisas.sbPesquisaClick(Sender: TObject);
begin
  inherited;
  Pesquisar(Sender, True);
end;

function TFrameHenrancaPesquisas.AdicionarDireto: TObject;
begin
  Result := nil;
end;

function TFrameHenrancaPesquisas.AdicionarDiretoTodos: TArray<TObject>;
begin
	Result := nil;
end;

function TFrameHenrancaPesquisas.AdicionarPesquisando: TObject;
begin
  Result := nil;
end;

function TFrameHenrancaPesquisas.AdicionarPesquisandoTodos: TArray<TObject>;
begin
  Result := nil;
end;

procedure TFrameHenrancaPesquisas.AjustarUltimaColuna;
var
  i: Integer;
  soma: Integer;
begin
  soma := 0;
  for i := 0 to sgPesquisa.ColCount - 2 do
    soma := soma + sgPesquisa.ColWidths[i];

  sgPesquisa.ColWidths[sgPesquisa.ColCount - 1] := sgPesquisa.Width - soma - 4;
end;

function TFrameHenrancaPesquisas.IndiceChave(pSender:TObject):Integer;
begin
  Result := -1;
end;

procedure TFrameHenrancaPesquisas.SetFocus;
begin
  SetarFoco(sgPesquisa);
end;

procedure TFrameHenrancaPesquisas.InserirDado(pSender: TObject; pDispararSelecionou: Boolean; pMontarDados: Boolean);
var
  indice: Integer;
begin
  if pSender <> nil then begin
    if CkChaveUnica.Checked then
      LimparDados;

    indice := IndiceChave(pSender);
    if indice = -1 then begin
      FDados.Add(pSender);
      if pDispararSelecionou then
        Selecionou;
    end
    else
      FDados[indice] := pSender;

    if pMontarDados then begin
      MontarGrid;
      sgPesquisa.Row := sgPesquisa.RowCount - 1;
    end;
  end;
end;

procedure TFrameHenrancaPesquisas.Clear;
var
  vazio: Boolean;
begin
  inherited;

  vazio := EstaVazio;

	if not vazio and Assigned(FOnAntesDeletarEvent) then
  	FOnAntesDeletarEvent(Self);

  FDados.Clear;
  MontarGrid;

  if not vazio and Assigned(FOnAposDeletarEvent) then
  	FOnAposDeletarEvent(Self);
end;

procedure TFrameHenrancaPesquisas.InserirDadoPorChave(pChave: Integer; pDispararSelecionou: Boolean = True);
begin
  if pChave = 0 then
    Exit;

  InserirDadoPorChave(IntToStr(pChave), pDispararSelecionou);
end;

procedure TFrameHenrancaPesquisas.InserirDadoPorChave(pChave: string; pDispararSelecionou: Boolean = True);
begin
  FJogarFoco := False;
  FChaveDigitada := pChave;
  Pesquisar(nil, pDispararSelecionou);
end;

procedure TFrameHenrancaPesquisas.InserirDadoPorChaveTodos(pChaves: TArray<string>; pDispararSelecionou: Boolean = True);
begin
  if pChaves = nil then
    Exit;

  FDiretoTodos := True;
  FJogarFoco := False;
  FChaveDigitada := ' and ' + _Biblioteca.FiltroInStr(FColunaPesquisa, pChaves);
  Pesquisar(nil, pDispararSelecionou);
end;

procedure TFrameHenrancaPesquisas.InserirDadoPorChaveTodos(pChaves: TArray<Integer>; pDispararSelecionou: Boolean = True);
begin
  if pChaves = nil then
    Exit;

  FJogarFoco := False;
  FDiretoTodos := True;
  FChaveDigitada :=  ' and ' + _Biblioteca.FiltroInInt(FColunaPesquisa, pChaves);
  Pesquisar(nil, pDispararSelecionou);
end;

function TFrameHenrancaPesquisas.GetArrayOfInteger: TArray<Integer>;
var
	i: Integer;
begin
	Result := nil;

  if sgPesquisa.Cells[0, 0] = '' then
  	Exit;

  SetLength(Result, sgPesquisa.RowCount);
  for i := 0 to sgPesquisa.RowCount - 1 do
  	Result[i] := StrToIntDef(sgPesquisa.Cells[0, i], 0);
end;

function TFrameHenrancaPesquisas.GetArrayOfString: TArray<string>;
var
	i: Integer;
begin
	Result := nil;

  if sgPesquisa.Cells[0, 0] = '' then
  	Exit;

  SetLength(Result, sgPesquisa.RowCount);
  for i := 0 to sgPesquisa.RowCount - 1 do
  	Result[i] := sgPesquisa.Cells[0, i];
end;

function TFrameHenrancaPesquisas.GetFiltrosUtilizados: TArray<string>;
var
  i: Integer;
begin
  Result := nil;

  if EstaVazio then
    Exit;

  SetLength(Result, getQuantidadeLinhas + 1);

  Result[0] := lbNomePesquisa.Caption + IIfStr(ckSuprimir.Checked, '( Suprimir )');
  for i := 0 to getQuantidadeLinhas -1 do
    Result[i + 1] := '    ' + getInformacao( sgPesquisa.Cells[0, i], sgPesquisa.Cells[1, i]);
end;

procedure TFrameHenrancaPesquisas.LimparDados;
var
  i: Integer;
  vazio: Boolean;
begin
  vazio := EstaVazio;

  if not vazio and not (csDestroying in Self.ComponentState) and Assigned(FOnAntesDeletarEvent) then
  	FOnAntesDeletarEvent(Self);

  for i := 0 to FDados.Count - 1 do
    TObject(FDados[i]).Free;

  FDados.Clear;

  if not vazio and not (csDestroying in Self.ComponentState) and Assigned(FOnAposDeletarEvent) then
  	FOnAposDeletarEvent(Self);
end;

function TFrameHenrancaPesquisas.TratarStr(pString: string): string;
begin
  if CkPesquisaNumerica.Checked then
    Result := StringReplace(pString, '.', '', [rfReplaceAll])
  else
    Result := pString;
end;

function TFrameHenrancaPesquisas.TratarTextoDigitado(pString: string): string;
begin
  Result := pString;
end;

end.
