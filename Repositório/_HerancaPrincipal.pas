unit _HerancaPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, EditLuka, _Biblioteca, RadioGroupLuka, GridLuka,
  Vcl.ComCtrls, System.Math, _RecordsEspeciais{$IFDEF SIGO_VAREJO}, _LogsTelas{$ENDIF SIGO_VAREJO};

type
  TFormHerancaPrincipal = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);

    procedure ProximoCampo(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ProximoCampoForaFrame(Sender: TObject; var Key: Word; Shift: TShiftState);

    procedure Numeros(Sender: TObject; var Key: Char);
    procedure NumerosTraco(Sender: TObject; var Key: Char);
    procedure NumerosVirgula(Sender: TObject; var Key: Char);
    procedure NumerosVirgulaNegativo(Sender: TObject; var Key: Char);
    procedure NumerosVirgulaSemMascara(Sender: TObject; var Key: Char);

    procedure NumerosPonto(Sender: TObject; var Key: Char);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
  private
    FAnaliseCustoAtiva: Boolean;
    FUtilizandoAnaliseCusto: Boolean;

    procedure SetProcessoAnaliseAtivo(pAtivo: Boolean);
    function GetTipoAnaliseCusto: string;
  protected
    procedure AtivarAnaliseCusto(pAtivar: string);
    procedure VerificarAnaliseCusto();
    function getEstoqueAnalise(pEstoqueFiscal: Double; pEstoqueReal: Double): Double;

    procedure OnAtivarDesativarAnaliseCusto(const pAtivo: Boolean); virtual;

    property ProcessoAnaliseAtivo: Boolean read FUtilizandoAnaliseCusto write SetProcessoAnaliseAtivo;
    property tipoAnaliseCusto: string read GetTipoAnaliseCusto;
  end;

implementation

{$R *.dfm}

{$IFDEF SIGO_VAREJO}
  uses
    _Sessao;
{$ENDIF}

const
  cTipoCustoA = 'A'; // Fiscal
  cTipoCustoB = 'B'; // Real
  cTipoCustoC = 'C'; // Fiscal + Real

procedure TFormHerancaPrincipal.AtivarAnaliseCusto(pAtivar: string);
begin
  inherited;

end;

procedure TFormHerancaPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TFormHerancaPrincipal.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  Caption := Application.Title + ' - ' + Caption;

  for i := 0 to ControlCount - 1 do begin
    if Controls[i] is TEditLuka then begin
      if TEditLuka(Controls[i]).PadraoEstoque then
        TEditLuka(Controls[i]).CasasDecimais := _Biblioteca.getCasasDecimaisEstoque;
    end;
  end;

  {$IFDEF SIGO_VAREJO}
    if Sessao.getUsuarioLogado <> nil then
      _LogsTelas.AtualizarLogs(Sessao.getConexaoBanco, Sessao.getUsuarioLogado.funcionario_id, Self.ClassName);
  {$ENDIF}
end;

procedure TFormHerancaPrincipal.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  vPageControl: TPageControl;

  function getTPageControl(pControl: TWinControl): Boolean;
  begin
    Result := False;

    if vPageControl <> nil then
      Exit;

    while pControl <> nil do begin
      if pControl is TPageControl then begin
        vPageControl := TPageControl(pControl);
        Result := True;
        Break;
      end;

      Result := getTPageControl(pControl.Parent);
      Break;
    end;
  end;

begin
  inherited;
  vPageControl := nil;

  case Key of
    VK_ESCAPE: Close;

    VK_RETURN: begin
      if (ActiveControl <> nil) and (ActiveControl.Parent is TRadioGroupLuka) then
        ProximoCampo(ActiveControl, Key, Shift);
    end;

    VK_NEXT: begin
      if (ActiveControl <> nil) and (ActiveControl is TGridLuka) then
        Exit;

      if getTPageControl(ActiveControl) then
        vPageControl.SelectNextPage(True);
    end;

    VK_PRIOR: begin
      if (ActiveControl <> nil) and (ActiveControl is TGridLuka) then
        Exit;

      if getTPageControl(ActiveControl) then
        vPageControl.SelectNextPage(False);
    end;

    VK_F12: begin
      if (Shift = [ssCtrl, ssShift]) and (FAnaliseCustoAtiva) then
        VerificarAnaliseCusto();
    end;
  end;
end;

procedure TFormHerancaPrincipal.FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  {$IFDEF SIGO_VAREJO}
  Sessao.setTimerSistemaOciosoHouveMovimento;
  {$ENDIF SIGO_VAREJO}
end;

function TFormHerancaPrincipal.getEstoqueAnalise(pEstoqueFiscal: Double; pEstoqueReal: Double): Double;
begin
  Result := IfThen(FUtilizandoAnaliseCusto, pEstoqueReal, pEstoqueFiscal);
end;

function TFormHerancaPrincipal.GetTipoAnaliseCusto: string;
begin
  if FUtilizandoAnaliseCusto then
    Result := cTipoCustoB
  else
    Result := cTipoCustoC;
end;

procedure TFormHerancaPrincipal.ProximoCampo(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (Shift = [ssCtrl]) and (Key = VK_RETURN) then
    SelectNext(TWinControl(Sender), False, True)
  else if Key = VK_RETURN then
    if (Sender is TForm) then
      (Sender as TForm).Perform(40,0,0)
    else
      SelectNext(TWinControl(Sender), True, True);
end;

procedure TFormHerancaPrincipal.ProximoCampoForaFrame(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    keybd_event(VK_TAB, 0, 0, 0);
end;

procedure TFormHerancaPrincipal.SetProcessoAnaliseAtivo(pAtivo: Boolean);
begin
  AtivarAnaliseCusto(_Biblioteca.ToChar(pAtivo));
  if pAtivo then
    VerificarAnaliseCusto();
end;

procedure TFormHerancaPrincipal.VerificarAnaliseCusto();
begin
  FUtilizandoAnaliseCusto := not FUtilizandoAnaliseCusto;
  OnAtivarDesativarAnaliseCusto(FUtilizandoAnaliseCusto);
  if FUtilizandoAnaliseCusto then
    Self.Caption := '*' + Self.Caption
  else
    Self.Caption := StringReplace(Self.Caption, '*', '', [rfReplaceAll]);
end;

procedure TFormHerancaPrincipal.Numeros(Sender: TObject; var Key: Char);
begin
  if not CharInSet(Key, [#13, #8]) then begin
    if _Biblioteca.RetornaNumeros(Key) = '' then
      Key := #0;
  end;
end;

procedure TFormHerancaPrincipal.NumerosTraco(Sender: TObject; var Key: Char);
begin
  if CharInSet(Key, ['-']) then
    Key := '-'
  else if not CharInSet(Key, [#13, #8]) then begin
    if _Biblioteca.RetornaNumeros(Key) = '' then
      Key := #0;
  end;
end;

procedure TFormHerancaPrincipal.NumerosVirgula(Sender: TObject; var Key: Char);
begin
  if CharInSet(Key, [',','.']) then
    Key := ','
  else if not CharInSet(Key, [#13, #8]) then begin
    if _Biblioteca.RetornaNumeros(Key) = '' then
      Key := #0;
  end;
end;

procedure TFormHerancaPrincipal.NumerosVirgulaNegativo(Sender: TObject; var Key: Char);
begin
  if CharInSet(Key, [',','.']) then
    Key := ','
  else if not CharInSet(Key, [#13, #8, #45]) then begin
    if _Biblioteca.RetornaNumeros(Key) = '' then
      Key := #0;
  end;
end;

procedure TFormHerancaPrincipal.NumerosVirgulaSemMascara(Sender: TObject; var Key: Char);
begin
  if not CharInSet(Key, ['0'..'9', #46, #48, #57, #8]) then
    Key := #0;
end;

procedure TFormHerancaPrincipal.OnAtivarDesativarAnaliseCusto(const pAtivo: Boolean);
begin
  inherited;
  // Nada
end;

procedure TFormHerancaPrincipal.NumerosPonto(Sender: TObject; var Key: Char);
begin
  if CharInSet(Key, [',','.']) then
    Key := '.'
  else if not CharInSet(Key, [#13, #8]) then begin
    if _Biblioteca.RetornaNumeros(Key) = '' then
      Key := #0;
  end;
end;

end.
