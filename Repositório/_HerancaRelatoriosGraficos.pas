unit _HerancaRelatoriosGraficos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, QRCtrls, QuickRpt, Vcl.ExtCtrls, _Sessao, _RecordsCadastros,
  QRExport, QRPDFFilt, QRWebFilt, _Empresas, _Biblioteca, Vcl.Imaging.jpeg;

type
  TFormHerancaRelatoriosGraficos = class(TForm)
    qrRelatorioA4: TQuickRep;
    qrCabecalho: TQRBand;
    qrCaptionEndereco: TQRLabel;
    qrEmpresa: TQRLabel;
    qrEndereco: TQRLabel;
    qrLogoEmpresa: TQRImage;
    qrCaptionEmpresa: TQRLabel;
    qrEmitidoEm: TQRLabel;
    qr1: TQRLabel;
    qrBairro: TQRLabel;
    qr3: TQRLabel;
    qrCidadeUf: TQRLabel;
    qr5: TQRLabel;
    qrCNPJ: TQRLabel;
    qr7: TQRLabel;
    qrTelefone: TQRLabel;
    qrFax: TQRLabel;
    qr10: TQRLabel;
    qr11: TQRLabel;
    qrEmail: TQRLabel;
    qr13: TQRLabel;
    QRHTMLFilter: TQRHTMLFilter;
    QRExcelFilter: TQRExcelFilter;
    QRPDFFilter: TQRPDFFilter;
    QRRTFFilter: TQRRTFFilter;
    qrRelatorioNF: TQuickRep;
    qrBandTitulo: TQRBand;
    qrlNFNomeEmpresa: TQRLabel;
    qrNFCnpj: TQRLabel;
    qrNFEndereco: TQRLabel;
    qrNFCidadeUf: TQRLabel;
    qrNFTipoDocumento: TQRLabel;
    qrSeparador2: TQRShape;
    qrNFTitulo: TQRLabel;
    qrlNFEmitidoEm: TQRLabel;
    PageFooterBand1: TQRBand;
    qrlNFSistema: TQRLabel;
    qrlNFTelefoneEmpresa: TQRLabel;
    qrbndRodape: TQRBand;
    qrUsuarioImpressao: TQRLabel;
    qrSistema: TQRLabel;
    QRTextFilter1: TQRTextFilter;
    qrLabelCep: TQRLabel;
    qrCEP: TQRLabel;
  private
    FImpressora: RecImpressora;
  public
    constructor Create(AOwner: TComponent; pImpressora: RecImpressora); reintroduce;
  protected
    function ImpressaoGrafica: Boolean;
    function ImpressaoMatricial: Boolean;
    procedure PreencherCabecalho(pEmpresaId: Integer; pImprirmirCNPJCabecalho: Boolean = True);
    procedure Imprimir(pQtdeVias: Integer = 1; pGerarPDF: Boolean = False);
  end;

implementation

{$R *.dfm}

{ TForm2 }

constructor TFormHerancaRelatoriosGraficos.Create(AOwner: TComponent; pImpressora: RecImpressora);
var
  vDataHora: TDateTime;
  vImagem: TJPEGImage;
begin
  inherited Create(AOwner);

  vDataHora := Sessao.getDataHora;
  FImpressora := pImpressora;

  if ImpressaoGrafica then begin
    qrEmitidoEm.Caption        := 'Emitido em ' + DFormat(vDataHora) + ' �s ' + HFormat(vDataHora);
    qrSistema.Caption          := Sessao.getVersaoSistema;
    qrUsuarioImpressao.Caption := NFormat(Sessao.getUsuarioLogado.funcionario_id) + ' - ' + Sessao.getUsuarioLogado.nome;

    vImagem := nil;

    if Sessao.getParametrosEmpresa.LogoEmpresa = nil then
      Exit;

    try
      try
        Sessao.getParametrosEmpresa.LogoEmpresa.Position := 0;
        vImagem := TJPEGImage.Create;
        vImagem.LoadFromStream(Sessao.getParametrosEmpresa.LogoEmpresa);
        qrLogoEmpresa.Picture.Graphic := vImagem;
      except

      end;
    finally
      vImagem.Free;
    end;
  end
  else begin
    qrlNFEmitidoEm.Caption := 'Emitido em' + DFormat(vDataHora) + '�s' + HFormat(vDataHora);
    qrlNFSistema.Caption   := _Sessao.Sessao.getVersaoSistema;
  end;
end;

function TFormHerancaRelatoriosGraficos.ImpressaoGrafica: Boolean;
begin
  Result := FImpressora.TipoImpressora = 'G';
end;

function TFormHerancaRelatoriosGraficos.ImpressaoMatricial: Boolean;
begin
  Result := FImpressora.TipoImpressora = 'M';
end;

procedure TFormHerancaRelatoriosGraficos.Imprimir(pQtdeVias: Integer = 1; pGerarPDF: Boolean = False);
var
  i: Integer;
  vArquivoPDFSalvar: string;
begin
  if ImpressaoGrafica then begin
    qrRelatorioA4.PrinterSettings.PrinterIndex := FImpressora.getIndex;
    qrRelatorioA4.Prepare;

    if pGerarPDF then begin
      vArquivoPDFSalvar := _Biblioteca.getNomeCaminhoArquivoSalvar('pdf', 'Arquivo PDF');
      qrRelatorioA4.ExportToFilter(TQRPDFDocumentFilter.Create(vArquivoPDFSalvar));
    end
    else if FImpressora.AbrirPreview then
      qrRelatorioA4.PreviewModal
    else begin
      for i := 1 to pQtdeVias do
        qrRelatorioA4.Print;
    end;
  end
  else begin
    qrRelatorioNF.PrinterSettings.PrinterIndex := FImpressora.getIndex;
    qrRelatorioNF.Prepare;

    if pGerarPDF then begin

    end
    else if FImpressora.AbrirPreview then
      qrRelatorioNF.PreviewModal
    else begin
      for i := 1 to pQtdeVias do
        qrRelatorioNF.Print;
    end;
  end;
end;

procedure TFormHerancaRelatoriosGraficos.PreencherCabecalho(pEmpresaId: Integer; pImprirmirCNPJCabecalho: Boolean = True);
var
  vDadosEmpresa: TArray<RecEmpresas>;
begin
  if pEmpresaId = Sessao.getEmpresaLogada.EmpresaId then begin
    SetLength(vDadosEmpresa, 1);
    vDadosEmpresa[0] := Sessao.getEmpresaLogada;
  end
  else
    vDadosEmpresa := _Empresas.BuscarEmpresas(Sessao.getConexaoBanco, 0, [pEmpresaId]);

  if ImpressaoGrafica then begin
    qrEmpresa.Caption  := vDadosEmpresa[0].RazaoSocial;
    qrCNPJ.Caption     := vDadosEmpresa[0].Cnpj;
    qrEndereco.Caption := vDadosEmpresa[0].Logradouro + ' ' + vDadosEmpresa[0].Complemento + ' n� ' + vDadosEmpresa[0].Numero;
    qrBairro.Caption   := vDadosEmpresa[0].NomeBairro;
    qrCidadeUf.Caption := vDadosEmpresa[0].NomeCidade + ' / ' + vDadosEmpresa[0].EstadoId;
    qrCEP.Caption      := vDadosEmpresa[0].Cep;
    qrTelefone.Caption := vDadosEmpresa[0].TelefonePrincipal;
    qrFax.Caption      := vDadosEmpresa[0].TelefoneFax;
    qrEmail.Caption    := vDadosEmpresa[0].EMail;

    qrSistema.Caption := 'Emitido por ' + Sessao.getVersaoSistema;
  end
  else begin
    qrlNFNomeEmpresa.Caption := IIfStr(vDadosEmpresa[0].NomeResumidoImpressoesNF <> '', vDadosEmpresa[0].NomeResumidoImpressoesNF, vDadosEmpresa[0].RazaoSocial);

    if not pImprirmirCNPJCabecalho then begin
      qrNFCnpj.Enabled            := false;
      qrNFCnpj.Caption            := vDadosEmpresa[0].Cnpj;
      qrlNFNomeEmpresa.Font.Size  := 10;
      qrlNFNomeEmpresa.Top        := 18;
    end;

    qrNFEndereco.Caption         := vDadosEmpresa[0].Logradouro + ' ' + vDadosEmpresa[0].Complemento + ' n� ' + vDadosEmpresa[0].Numero;
    qrNFCidadeUf.Caption         := vDadosEmpresa[0].NomeBairro + ' ' + vDadosEmpresa[0].NomeCidade + '/' + vDadosEmpresa[0].EstadoId;
    qrlNFTelefoneEmpresa.Caption := 'Telefone: ' + vDadosEmpresa[0].TelefonePrincipal;
  end;
end;

end.
