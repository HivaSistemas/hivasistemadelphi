inherited FormHerancaPesquisasAvancadas: TFormHerancaPesquisasAvancadas
  Caption = 'Heran'#231'a pesquisas avan'#231'adas'
  ClientWidth = 660
  Visible = False
  ExplicitWidth = 666
  ExplicitHeight = 407
  DesignSize = (
    660
    378)
  PixelsPerInch = 96
  TextHeight = 14
  object sgPesquisa: TGridLuka
    Left = 2
    Top = 92
    Width = 655
    Height = 284
    Anchors = [akLeft, akTop, akRight, akBottom]
    ColCount = 3
    Ctl3D = True
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goRowSelect, goThumbTracking]
    ParentCtl3D = False
    TabOrder = 0
    OnDblClick = sgPesquisaDblClick
    OnEnter = sgPesquisaEnter
    OnKeyDown = sgPesquisaKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Sel?'
      'C'#243'digo')
    Grid3D = False
    RealColCount = 3
    AtivarPopUpSelecao = False
    ColWidths = (
      28
      55
      199)
  end
end
