unit _BibliotecaGenerica;

interface

uses
  Vcl.Forms, Vcl.Controls, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Dialogs, System.DateUtils, System.SysUtils, System.StrUtils, Vcl.ComCtrls,
  Vcl.Mask, Winapi.Windows, Winapi.WinSock, System.Math, Winapi.WinInet, Vcl.Graphics, System.Classes, System.Types, System.UITypes,
  _RecordsEspeciais, Vcl.Printers, System.Variants, TLHelp32;

// ------------ Procedimentos --------------------
procedure SetarFoco(pObjeto: TWinControl); overload;
procedure SetarFoco(pObjeto: TWinControl; Key: Word); overload;
procedure LimparCampos(pObjetos: array of TControl);
procedure WhereOuAnd(var pSqlAtual: string; pNovoComando: string);
procedure AddNoVetorSemRepetir(var pVetor: TArray<Integer>; pValor: Integer);
procedure AddVirgulaSeNecessario(var pSqlAtual: string; pNovoComando: string);
procedure SomenteLeitura(pObjetos: array of TControl; pHabilitar: Boolean); overload;
procedure SomenteLeitura(pObjetos: array of TComponent; pHabilitar: Boolean); overload;

procedure Destruir(var pObjeto: TObject); overload;
procedure Destruir(var pObjeto: TArray<TObject>); overload;

// ---------------- Fun��es ----------------------

function NomeComputador: string;
function TemConexaoInternet: Boolean;
function Espacos(pQtde: Word): string;
function EnderecoIPComputador: string;
function DataOk(pData: string): Boolean;
function getCasasDecimaisEstoque: Integer;
function MesDoAno(pData: TDateTime): string;
function SimNao(const pValor: string): string;
function DiaDaSemana(pData: TDateTime): string;
function ToChar(pValor: Boolean): string; overload;
function ToChar(pValor: TCheckBox): string; overload;
function ToData(pValor: string): TDateTime;
function nvl(pValor1: TDateTime; pValor2: Variant): Variant; overload;
function zvl(pValor1: Integer; pValor2: Integer): Integer; overload;
function zvl(pValor1: Currency; pValor2: Currency): Currency; overload;
function ToDataHora(pData: TDateTime; pHora: TDateTime): TDateTime; overload;
function TratarErro(pMensagem: string): string;
function AzulVermelho(pSimNao: string): Integer;
function RetornaMascaraPai(pTexto: string): string;
function RetornaNumeros(pTexto: string): string; overload;
function RemoverCaracteresEspeciais(pTexto: string): string;
function RetornaNumeros(pTexto: AnsiString): AnsiString; overload;
function Decode(pBase: Variant; pValores: array of Variant): Variant;
function FormatarValorECF(pValor: Double; pDecimais: Byte): AnsiString;
function Arredondar(pValor: Double; pCasasDecimais: Integer): Double;
function RemoverCaracter(const pValor: string; const pCaracter: string): string;
function BlockInput(fBlockInput: Boolean): DWORD; stdcall; external 'user32.dll';
function Em(pComparacao: string; pComparados: array of string): Boolean; overload;
function Em(pComparacao: array of string; pComparados: array of string): Boolean; overload;
function Em(pComparacao: Integer; pComparados: array of Integer): Boolean; overload;
function RPad(pTexto: string; pTamanho: Integer; pCaracter: Char = ' '): string;
function LPad(pTexto: string; pTamanho: Integer; pCaracter: Char = ' '): string;
function CPad(pTexto: string; pTamanho: Integer; pCaracter: Char = ' '): string;
function GetIndexImpressora(pImpressora: string): Integer;
function Iifn(pCondicao: Boolean; pVerdaddeiro: Integer ): Variant;
function ZeroSeNegativo(pValor: Currency): Currency;

// Fun�oes de convers�o
function NFormat(const pValor: Integer): string; overload;
function NFormatN(const pValor: Integer): string; overload;

function NFormatEstoque(const pValor: Double): string; overload;
function NFormatNEstoque(const pValor: Double): string; overload;

function NFormat(const pValor: Double; pDecimais: Integer = 2): string; overload;
function NFormatN(const pValor: Double; pDecimais: Integer = 2): string; overload;

function DFormat(const pValor: TDateTime): string;
function DFormatN(const pValor: TDateTime): string;

function HFormat(const pValor: TDateTime): string;
function HFormatN(const pValor: TDateTime): string;

function DHFormat(const pValor: TDateTime): string;

function SFormatInt(const pValor: string): Integer;
function SFormatDouble(const pValor: string): Double;
function SFormatCurr(pValor: string): Currency;

function FiltroInInt(pColuna: string; pFiltros: TArray<Integer>): string;
function FiltroNotInInt(pColuna: string; pFiltros: TArray<Integer>): string;
function FiltroInStr(pColuna: string; pFiltros: TArray<string>): string;
function RetirarAcento(c: Char): Char;
function Perguntar(pPergunta: string; pCaption: string = 'Pergunta'): Boolean;
function RetirarAcentos(pTexto: string): string;
function LimparTexto(pTexto: string): string;

function IIf(pCondicao: Boolean; pVerdadeiro: Variant; pFalso: Variant): Variant;
function getCorVerdePadraoAltis: TColor;
function KillTask(ExeFileName: string): Integer;
function FormatarDataAnoMesDia(pFormatarData: string): TDateTime;
function getMaiorValor(pValores: TArray<Currency>): Currency; overload;

const
  co_vermelho_prioridade = $006A6AFF;

  coNumeros: array[0..9] of Integer = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
  coNenhumRegistroEncontrado  = 'Nenhum registro encontrado!';
  coNovoRegistroSucessoCodigo = 'Novo registro inserido com sucesso.' + Chr(13) + Chr(10) + 'C�digo: ';

  cor_abertura     = clBlue;
  cor_suprimento   = clMenuHighlight;
  cor_saida_abert  = $00000082;
  cor_sangria      = clRed;
  cor_entrada_sang = $00FFDD97;
  cor_fechamento   = clOlive;
  cor_recebimento  = $000096DB;
  cor_saida_supri  = $000080FF;
  cor_canc_venda   = clRed;
  cor_ent_fec_cai  = $00FF6F6F;
  cor_saida_bx_tit = $000000D5;
  cor_ent_bx_tit   = $00E87400;

  (* Controle de or�amentos / vendas *)
  co_orcamento_em_aberto              = 'OE';
  co_orcamento_bloqueado              = 'OB';
  co_recebido_aguardando_gerar_reg_nf = 'RE';
  co_orcamento_cancelado              = 'CA';

  (* Cores dos grids *)
  coCorFonteEdicao1 = clBlue;
  coCorCelulaEdicao1 = $00FFF5E1;
  coCorFonteEdicao2 = clGreen;
  coCorCelulaEdicao2 = $00ECFFEC;
  coCorFonteEdicao3 = $000080FF;
  coCorCelulaEdicao3 = $00DFEFFF;

  coCaixaId    = 1;
  coVendedorId = 2;
  coAuxAdminId = 3;
  coGerenteId  = 4;

implementation

procedure LimparCampos(pObjetos: array of TControl);
var
  i: Integer;
begin
  for i := Low(pObjetos) to High(pObjetos) do begin
    if pObjetos[i] is TCustomEdit then
      TCustomEdit(pObjetos[i]).Clear
    else if pObjetos[i] is TCustomComboBox then
      TCustomComboBox(pObjetos[i]).ItemIndex := -1
    else if pObjetos[i] is TRadioGroup then
      TRadioGroup(pObjetos[i]).ItemIndex := -1
    else if pObjetos[i] is TCheckBox then
      TCheckBox(pObjetos[i]).Checked := False
    else if pObjetos[i] is TPageControl then
      TPageControl(pObjetos[i]).ActivePageIndex := 0
    else if pObjetos[i] is TStaticText then
      TStaticText(pObjetos[i]).Caption := '';
  end;
end;

procedure WhereOuAnd(var pSqlAtual: string; pNovoComando: string);
begin
  if Pos('where', pSqlAtual) = 0 then
    pSqlAtual := ' where ' + pNovoComando
  else
    pSqlAtual := pSqlAtual + ' and ' + pNovoComando;
end;

procedure AddNoVetorSemRepetir(var pVetor: TArray<Integer>; pValor: Integer);
begin
  if Em(pValor, pVetor) then
    Exit;

  SetLength(pVetor, Length(pVetor) + 1);
  pVetor[High(pVetor)] := pValor;
end;

procedure AddVirgulaSeNecessario(var pSqlAtual: string; pNovoComando: string);
begin
  if Length(pSqlAtual) = 0 then
    pSqlAtual := ' ' + pNovoComando
  else
    pSqlAtual := pSqlAtual + ', ' + pNovoComando;
end;

procedure SomenteLeitura(pObjetos: array of TControl; pHabilitar: Boolean);
var
  i: Integer;
  j: Integer;
begin
  for i := Low(pObjetos) to High(pObjetos) do begin
    if pObjetos[i] is TCustomEdit then
      TCustomEdit(pObjetos[i]).ReadOnly := pHabilitar
    else if pObjetos[i] is TCustomMaskEdit then
      TCustomMaskEdit(pObjetos[i]).ReadOnly := pHabilitar
    else if pObjetos[i] is TPageControl then begin
      for j := 0 to TPageControl(pObjetos[i]).ControlCount -1 do begin
        if TPageControl(pObjetos[i]).Controls[j] is TCustomEdit then
          TCustomEdit(TPageControl(pObjetos[i]).Controls[j]).ReadOnly := pHabilitar
        else if pObjetos[i] is TCustomMaskEdit then
          TCustomMaskEdit(TPageControl(pObjetos[i]).Controls[j]).ReadOnly := pHabilitar
      end;
    end;
  end;
end;

procedure SomenteLeitura(pObjetos: array of TComponent; pHabilitar: Boolean);
var
  i: Integer;
  j: Integer;
begin
  for i := Low(pObjetos) to High(pObjetos) do begin
    if pObjetos[i] is TCustomEdit then
      TCustomEdit(pObjetos[i]).ReadOnly := pHabilitar
    else if pObjetos[i] is TCustomMaskEdit then
      TCustomMaskEdit(pObjetos[i]).ReadOnly := pHabilitar
    else if pObjetos[i] is TPageControl then begin
      for j := 0 to TPageControl(pObjetos[i]).ControlCount -1 do begin
        if TPageControl(pObjetos[i]).Controls[j] is TCustomEdit then
          TCustomEdit(TPageControl(pObjetos[i]).Controls[j]).ReadOnly := pHabilitar
        else if pObjetos[i] is TCustomMaskEdit then
          TCustomMaskEdit(TPageControl(pObjetos[i]).Controls[j]).ReadOnly := pHabilitar
      end;
    end;
  end;
end;

procedure SetarFoco(pObjeto: TWinControl);
var
  vPai: TWinControl;
begin
  vPai := nil;
  while (vPai = nil) or (vPai.HasParent) do begin
    if not Assigned(vPai) then
      vPai := pObjeto.Parent
    else
      vPai := vPai.Parent;

    if not(vPai is TTabSheet) then
      Continue;

    if not TTabSheet(vPai).TabVisible then
      Exit;

    TPageControl(vPai.Parent).ActivePage := TTabSheet(vPai);
  end;

  try
    if pObjeto.CanFocus then
      pObjeto.SetFocus;
  except

  end;
end;

procedure SetarFoco(pObjeto: TWinControl; Key: Word);
begin
  if Key = VK_RETURN then
    SetarFoco(pObjeto);
end;

procedure Destruir(var pObjeto: TObject);
begin
  try
    if pObjeto <> nil then
      FreeAndNil(pObjeto);
  except

  end;
end;

procedure Destruir(var pObjeto: TArray<TObject>);
var
  i: Integer;
begin
  for i := Low(pObjeto) to High(pObjeto) do begin
    try
      FreeAndNil(pObjeto[i]);
    except

    end;
  end;

  try
    pObjeto := nil;
  except

  end;
end;

function NomeComputador: string;
var
  flag: dWord;
begin
  flag := MAX_COMPUTERNAME_LENGTH + 1;
  SetLength(Result, flag);
  GetComputerName( PChar(Result), flag );
  Result := string( PChar(Result) );
  Result := Trim( Result );
end;

function TemConexaoInternet: Boolean;
begin
  Result := InternetCheckConnection('http://www.google.com.br/', 1, 0);
end;

function Espacos(pQtde: Word): string;
var
  i: Word;
begin
  Result := '';
  for i := 1 to pQtde do
    Result := Result + ' ';
end;

function EnderecoIPComputador: string;
var
  lHostEnt: PHostEnt;
  lHostName: array[0..128] of char;
  lIPAddress: PAnsiChar;
begin
  GetHostName(@lHostName, 128);
  lHostEnt := GetHostByName(@lHostName);
  lIpAddress := iNet_ntoa(PInAddr(lHostEnt^.h_addr_list^)^);
  Result := String(lIpAddress);
end;

function DataOk(pData: string): Boolean;
begin
  try
    Result := True;
    StrToDate(pData);
  except
    Result := False;
  end;
end;

function getCasasDecimaisEstoque: Integer;
begin
  // Mudar pelo novo par�metro
  Result := 4;
end;

function MesDoAno(pData: TDateTime): string;
begin
  Result := '';
  case MonthOfTheYear(pData) of
     1: Result := 'Janeiro';
     2: Result := 'Fevereiro';
     3: Result := 'Mar�o';
     4: Result := 'Abril';
     5: Result := 'Maio';
     6: Result := 'Junho';
     7: Result := 'Julho';
     8: Result := 'Agosto';
     9: Result := 'Setembro';
    10: Result := 'Outubro';
    11: Result := 'Novembro';
    12: Result := 'Dezembro';
  end;
end;

function SimNao(const pValor: string): string;
begin
  Result := IfThen(pValor = 'S', 'Sim', 'N�o');
end;

function DiaDaSemana(pData: TDateTime): string;
begin
  Result := '';
  case DayOfTheWeek(pData) of
    1: Result := 'Segunda-Feira';
    2: Result := 'Ter�a-Feira';
    3: Result := 'Quarta-Feira';
    4: Result := 'Quinta-Feira';
    5: Result := 'Sexta-Feira';
    6: Result := 'S�bado';
    7: Result := 'Domingo';
  end;
end;

function ToChar(pValor: Boolean): string;
begin
  Result := IfThen(pValor, 'S', 'N');
end;

function ToChar(pValor: TCheckBox): string; overload;
begin
  Result := ToChar(pValor.Checked);
end;

function ToData(pValor: string): TDateTime;
begin
  Result := StrToDateDef(pValor, 0);
end;

function ToDataHora(pData: TDateTime; pHora: TDateTime): TDateTime; overload;
begin
  Result := pData + pHora;
end;

function nvl(pValor1: TDateTime; pValor2: Variant): Variant; overload;
begin
  if pValor1 > 0 then
    Result := pValor1
  else
    Result := pValor2;
end;

function zvl(pValor1: Integer; pValor2: Integer): Integer; overload;
begin
  if pValor1 > 0 then
    Result := pValor1
  else
    Result := pValor2;
end;

function zvl(pValor1: Currency; pValor2: Currency): Currency; overload;
begin
  if pValor1 > 0 then
    Result := pValor1
  else
    Result := pValor2;
end;

function TratarErro(pMensagem: string): string;
begin
  if Pos('ORA-20000', pMensagem) = 0 then
    Result := pMensagem
  else begin
    Result := Copy(pMensagem, Pos('ORA-20000', pMensagem) + 11, Length(pMensagem));
    Result := Copy(Result, 1, Pos('ORA-', Result) - 1);
  end;
end;

function AzulVermelho(pSimNao: string): Integer;
begin
  Result := IfThen(pSimNao = 'Sim', clBlue, clRed);
end;

function RetornaMascaraPai(pTexto: string): string;
var
  i: Integer;
  x: TStringDynArray;
begin
  Result := '';
  x := SplitString(pTexto, '.');
  for i := Low(x) to High(x) do begin
    if i = High(x) then
      Break;

    Result := Result + IfThen(i in[1,2], '.') + LPad(x[i], 3, '0');
  end;
end;

function RetornaNumeros(pTexto: string): string;
var
  i: Integer;
begin
  Result := '';
  for i := 1 to Length(pTexto) do begin
    if CharInSet(pTexto[i], ['0'..'9']) then
      Result := Result + pTexto[i];
  end;
end;

function RemoverCaracteresEspeciais(pTexto: string): string;
var
  i: Integer;

const
  co_caracteres_especiais: array[1..46] of string[1] = (
    '�','�','�','�','�',
    '�','�','�','�','�',
    '�', '�',
    '�','�','�','�',
    '�','�','�','�',
    '�','�','�','�',
    '�','�','�','�',
    '�','�','�','�','�',
    '�','�','�','�','�',
    '�','�','�','�',
    '�','�','�','�'
  );

  co_subst_carac_especiais: array[1..46] of string[1] = (
    'A','A','A','A','A',
    'a','a','a','a','a',
    'C', 'c',
    'E','E','E','E',
    'e','e','e','e',
    'I','I','I','I',
    'i','i','i','i',
    'O','O','O','O','O',
    'o','o','o','o','o',
    'U','U','U','U',
    'u','u','u','u'
  );
begin
  Result := '';
  pTexto := Trim(pTexto);

  if pTexto = '' then
    Exit;

  for i := Low(co_caracteres_especiais) to High(co_caracteres_especiais) do
     pTexto := StringReplace(pTexto, string(co_caracteres_especiais[i]), string(co_subst_carac_especiais[i]), [rfreplaceall]);

  Result := pTexto;
end;

function RetornaNumeros(pTexto: AnsiString): AnsiString;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to Length(pTexto) do begin
    if CharInSet(pTexto[i], ['0'..'9']) then
      Result := Result + pTexto[i];
  end;
end;

function Decode(pBase: Variant; pValores: array of Variant): Variant;
var
  i: Integer;
  EPar: Boolean;
  MetadeItens: Integer;
begin
  EPar := (High(pValores) + 1) mod 2 = 0;
  if EPar then
    Result := ''
  else
    Result := pValores[High(pValores)];

  MetadeItens := (High(pValores) - 1) div 2;

  for i := 0 to MetadeItens do begin
    if pBase = pValores[i * 2] then begin
      Result := pValores[i * 2 + 1];
      Break;
    end;
  end;
end;

function FormatarValorECF(pValor: Double; pDecimais: Byte): AnsiString;
begin
  Result := '';
  case pDecimais of
    2: Result := AnsiString( FormatFloat('#0.00', pValor) );
    3: Result := AnsiString( FormatFloat('#0.000', pValor) );
  end;
end;

function RemoverCaracter(const pValor: string; const pCaracter: string): string;
begin
  Result := StringReplace(pValor, pCaracter, '', [rfReplaceAll]);
end;

function Arredondar(pValor: Double; pCasasDecimais: Integer): Double;
var
  fator, fracao: Extended;
begin
  {Eleva o Valor 10 ao expoente mandado na variavel casasDecimais}
  fator := IntPower(10, pCasasDecimais);
  { Multiplica o valor pelo fator e faz a conversao de string e depois para float novamente para evitar arredondamentos. }
  pValor := StrToFloat(FloatToStr(pValor * fator));
  {Pega a Parte Inteira do Numero}
  Result := Int(pValor);
  {Pega a Parte Fracionaria}
  fracao := Frac(pValor);

  {Faz a regra de arredondamento}
  if fracao >= 0.5 then
    Result := Result + 1
  else if fracao <= -0.5 then
    Result := Result - 1;

  {O valor Final inteiro divide por 100 para transformar em decimal novamente.}
  Result := Result / fator;
end;

function Em(pComparacao: string; pComparados: array of string): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Low(pComparados) to High(pComparados) do begin
    if pComparacao = pComparados[i] then begin
      Result := True;
      Break;
    end;
  end;
end;

function Em(pComparacao: array of string; pComparados: array of string): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Low(pComparacao) to High(pComparacao) do begin
    if Em(pComparacao[i], pComparados) then begin
      Result := True;
      Break;
    end;
  end;
end;


function Em(pComparacao: Integer; pComparados: array of Integer): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := Low(pComparados) to High(pComparados) do begin
    if pComparacao = pComparados[i] then begin
      Result := True;
      Break;
    end;
  end;
end;

function RPad(pTexto: string; pTamanho: Integer; pCaracter: Char = ' '): string;
var
  i: Integer;
begin
  Result := Copy(pTexto, 1, pTamanho);
  for i := 1 to pTamanho - Length(pTexto) do
    Result := Result + pCaracter;
end;

function LPad(pTexto: string; pTamanho: Integer; pCaracter: Char = ' '): string;
var
  i: Integer;
begin
  Result := Copy(pTexto, 1, pTamanho);
  for i := 1 to pTamanho - Length(pTexto) do
    Result := pCaracter + Result;
end;

function CPad(pTexto: string; pTamanho: Integer; pCaracter: Char = ' '): string;
var
  i: Integer;
begin
  Result := Copy(pTexto, 1, pTamanho);
  for i := 1 to pTamanho - Length(pTexto) do begin
    if i mod 2 = 1 then
      Result := Result + pCaracter
    else
      Result := pCaracter + Result;
  end;
end;

function GetIndexImpressora(pImpressora: string): Integer;
begin
  Result := Vcl.Printers.Printer.Printers.IndexOf(pImpressora);
end;

function Iifn(pCondicao: Boolean; pVerdaddeiro: Integer ): Variant;
begin
  if pCondicao then
    Result := pVerdaddeiro
  else
    Result := null;
end;

function ZeroSeNegativo(pValor: Currency): Currency;
begin
  Result := pValor;
  if Result < 0 then
    Result := 0;
end;

function BuscarVersaoSistema: string;
type
  PFFI = ^vs_FixedFileInfo;
var
  vCaminhoAplicacao: String;
  F: PFFI;
  Handle: DWORD;
  Len: LongInt;
  Data: PChar;
  Buffer: Pointer;
  vTamanho: DWORD;
  vAplicacao: PChar;
begin
  vCaminhoAplicacao := Application.ExeName;
  vAplicacao := StrAlloc(Length(vCaminhoAplicacao) + 1);
  StrPcopy(vAplicacao, vCaminhoAplicacao);
  Len := GetFileVersionInfoSize(vAplicacao, Handle);
  Result := '';
  if Len > 0 then begin
    Data := StrAlloc(Len + 1);
    if GetFileVersionInfo(vAplicacao, Handle, Len, Data) then begin
      VerQueryValue(Data, '\', Buffer, vTamanho);
      F := PFFI(Buffer);
      Result :=
        Format(
          //'%d.%d.%d.%d',
          '0.0.0.1',
          [
            HiWord(F^.dwFileVersionMs),
            LoWord(F^.dwFileVersionMs),
            HiWord(F^.dwFileVersionLs),
            Loword(F^.dwFileVersionLs)
          ]
        );
    end;
    StrDispose(Data);
  end;
  StrDispose(vAplicacao);
end;

function NFormat(const pValor: Double; pDecimais: Integer = 2): string;
begin
  case pDecimais of
    2: Result := FormatFloat('#,##0.00',   pValor);
    3: Result := FormatFloat('#,##0.000',  pValor);
    4: Result := FormatFloat('#,##0.0000', pValor);
    5: Result := FormatFloat('#,##0.00000', pValor);
    6: Result := FormatFloat('#,##0.000000', pValor);
    7: Result := FormatFloat('#,##0.0000000', pValor);
  end;
end;

function NFormatN(const pValor: Double; pDecimais: Integer = 2): string;
begin
  if pValor = 0 then
    Result := ''
  else
    Result := NFormat(pValor, pDecimais);
end;

function NFormatEstoque(const pValor: Double): string;
begin
  Result := NFormat(pValor, getCasasDecimaisEstoque);
end;

function NFormatNEstoque(const pValor: Double): string;
begin
  Result := NFormatN(pValor, getCasasDecimaisEstoque);
end;

function DFormat(const pValor: TDateTime): string;
begin
  Result := FormatDateTime('dd/mm/yyyy', pValor);
end;

function DFormatN(const pValor: TDateTime): string;
begin
  if pValor = 0 then
    Result := ''
  else
    Result := DFormat(pValor);
end;

function HFormat(const pValor: TDateTime): string;
begin
  Result := FormatDateTime('hh:nn:ss', pValor);
end;

function HFormatN(const pValor: TDateTime): string;
begin
  if pValor = 0 then
    Result := ''
  else
    Result := HFormat(pValor);
end;

function DHFormat(const pValor: TDateTime): string;
begin
  Result := DFormat(pValor) + ' ' + HFormat(pValor);
end;

function SFormatInt(const pValor: string): Integer;
var
  vValor: string;
begin
  vValor := StringReplace(pValor, '.', '', [rfReplaceAll]);
  Result := StrToIntDef(vValor, 0);
end;

function SFormatDouble(const pValor: string): Double;
var
  vValor: string;
begin
  vValor := StringReplace(pValor, '.', '', [rfReplaceAll]);
  Result := StrToFloatDef(vValor, 0);
end;

function NFormat(const pValor: Integer): string;
begin
  Result := IntToStr(pValor);
end;

function NFormatN(const pValor: Integer): string;
begin
  if pValor = 0 then
    Result := ''
  else
    Result := IntToStr(pValor);
end;

function SFormatCurr(pValor: string): Currency;
begin
  pValor := StringReplace(pValor, '.', '', [rfReplaceAll]);
  Result := StrToFloatDef(pValor, 0);
end;

function FiltroInInt(pColuna: string; pFiltros: TArray<Integer>): string;
var
	i: integer;
begin
  if Length(pFiltros) = 1 then begin
    Result := ' ' + pColuna + ' = ' + IntToStr(pFiltros[Low(pFiltros)]) + ' ';
    Exit;
  end;

  Result := ' (' + pColuna + ' in (';
  for i := Low(pFiltros) to High(pFiltros) do begin
  	if i mod 1000 = 0 then begin
    	if i > 0 then begin
    		Delete(Result, Length(Result), 1);
      	Result := Result + ') or ' + pColuna + ' in (';
      end;
    end;

    if i mod 30 = 0 then
      Result := Result + #13;

    Result := Result + IntToStr(pFiltros[i]) + ',';
  end;
  Delete(Result, Length(Result), 1);
  Result := Result + ')) ';
end;

function FiltroNotInInt(pColuna: string; pFiltros: TArray<Integer>): string;
var
	i: integer;
begin
  if Length(pFiltros) = 1 then begin
    Result := ' ' + pColuna + ' != ' + IntToStr(pFiltros[Low(pFiltros)]) + ' ';
    Exit;
  end;

  Result := ' (' + pColuna + ' not in (';
  for i := Low(pFiltros) to High(pFiltros) do begin
  	if i mod 1000 = 0 then begin
    	if i > 0 then begin
    		Delete(Result, Length(Result), 1);
      	Result := Result + ') and ' + pColuna + ' not in (';
      end;
    end;

    if i mod 30 = 0 then
      Result := Result + #13;

    Result := Result + IntToStr(pFiltros[i]) + ',';
  end;
  Delete(Result, Length(Result), 1);
  Result := Result + ')) ';
end;

function FiltroInStr(pColuna: string; pFiltros: TArray<string>): string;
var
	i: integer;
begin
  Result := ' (' + pColuna + ' in (';

  for i := Low(pFiltros) to High(pFiltros) do begin

  	if i mod 1000 = 0 then begin
    	if i > 0 then begin
    		Delete(Result, Length(Result), 1);
      	Result := Result + ') or ' + pColuna
         + ' in (';
      end;
    end;

    if i mod 30 = 0 then
      Result := Result + #13;

    Result := Result + '''' + pFiltros[i] + ''',';
  end;
  Delete(Result, Length(Result), 1);
  Result := Result + ')) ';
end;

function RetirarAcento(C: Char) : Char;
begin
  case AnsiUpperCase(C)[1] of
    '�','�','�','�','�': Result := IfThen(IsCharUpper(C),'A','a')[1];
    '�','�','�','�': Result := IfThen(IsCharUpper(C),'E','e')[1];
    '�','�','�','�': Result := IfThen(IsCharUpper(C),'I','i')[1];
    '�','�','�','�','�': Result := IfThen(IsCharUpper(C),'O','o')[1];
    '�','�','�','�': Result := IfThen(IsCharUpper(C),'U','u')[1];
    '�': Result := IfThen(IsCharUpper(C),'C','c')[1];
    '�': Result := 'o';
    '�': Result := 'a';
  else
    Result := C;
  end;
end;

function Perguntar(pPergunta: string; pCaption: string): Boolean;
begin
  Result := False;
  if MessageDlg(pPergunta, mtconfirmation,[mbyes, mbno], 0) = 0 then
    Result := True
end;

function RetirarAcentos(pTexto: string): string;
var
  i: Integer;
begin
  Result := '';
  for i := 1 to Length(pTexto) do
    Result := Result + RetirarAcento(pTexto[i]);
end;

function LimparTexto(pTexto: string): string;
var
  i: integer;
begin
  Result := '';
  pTexto := RetirarAcentos(pTexto);

  for i := 1 to Length(pTexto) do begin
    if
      CharInSet(
        pTexto[i], [
          '0'..'9', 'A'..'Z', 'a'..'z', '!', '@', '#', '$', '%', '&', '(', ')',
          '*', '/' , '-', '_', '+', '-', '=', '.', ',', '<', '>', ':', ';', '?',
          '[', ']', '{', ' '
        ]
      )
    then
      Result := Result + pTexto[i];
  end;
end;

function IIf(pCondicao: Boolean; pVerdadeiro: Variant; pFalso: Variant): Variant;
begin
  if pCondicao then
    Result := pVerdadeiro
  else
    Result := pFalso;
end;

function getCorVerdePadraoAltis: TColor;
begin
  Result := RGB(219, 150, 0);
end;

function KillTask(ExeFileName: string): Integer;
const
  PROCESS_TERMINATE = $0001;
var
  ContinueLoop: BOOL;
  FSnapshotHandle: THandle;
  FProcessEntry32: TProcessEntry32;
begin
  Result := 0;
  FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  FProcessEntry32.dwSize := SizeOf(FProcessEntry32);
  ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32);
  while Integer(ContinueLoop) <> 0 do begin
    if
    ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) =
      UpperCase(ExeFileName)) or (UpperCase(FProcessEntry32.szExeFile) =
      UpperCase(ExeFileName)))
    then
      Result :=
        Integer(
          TerminateProcess(
            OpenProcess(PROCESS_TERMINATE, BOOL(0), FProcessEntry32.th32ProcessID),
            0
          )
        );

     ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32);
  end;
  CloseHandle(FSnapshotHandle);
end;

function FormatarDataAnoMesDia(pFormatarData: string): TDateTime;
var
  vData: string;
begin
  if Length(pFormatarData) = 8 then
    vData := Copy(pFormatarData, 7, 2) + '/' + Copy(pFormatarData, 5, 2) + '/' + Copy(pFormatarData, 1, 4)
  else
    vData := Copy(pFormatarData, 5, 2) + '/' + Copy(pFormatarData, 3, 2) + '/20' + Copy(pFormatarData, 1, 2);

  Result := ToData(vData)
end;

function getMaiorValor(pValores: TArray<Currency>): Currency; overload;
var
  i: Integer;
begin
  Result := -99999999;
  for i := Low(pValores) to High(pValores) do begin
    if pValores[i] > Result then
      Result := pValores[i];
  end;
end;

end.
