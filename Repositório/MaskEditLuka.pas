unit MaskEditLuka;

interface

uses
  System.SysUtils, System.Classes, Vcl.Controls, Vcl.StdCtrls, Vcl.Mask, Vcl.Graphics;

type
  TMaskEditLuka = class(TMaskEdit)
  private
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
    { Published declarations }
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Altis', [TMaskEditLuka]);
end;

{ TMaskEditLuka }

procedure TMaskEditLuka.CMEnter(var Message: TCMEnter);
begin
  if Self.Color = clWindow then
    Self.Color := $0080FFFF;
end;

procedure TMaskEditLuka.CMExit(var Message: TCMExit);
begin
  if Self.Color = $0080FFFF then
    Self.Color := clWindow;
end;

end.
