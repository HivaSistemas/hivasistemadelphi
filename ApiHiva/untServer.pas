unit untServer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Web.HTTPApp,System.JSON,System.StrUtils, Horse,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Buttons, Vcl.Menus, Registry, _Conexao, Horse.BasicAuthentication,
  System.ImageList, Vcl.ImgList, Vcl.Mask, _Criptografia,  System.Types,System.IniFiles,
  Data.DB, MemDS, DBAccess, Ora, Vcl.Themes, Vcl.Imaging.pngimage, _Logs;

const
  WM_MY_MESSAGE = WM_USER + 1;
  InputBoxMsg = WM_USER + 123;

type
  TfrmServer = class(TForm)
    btnStop: TBitBtn;
    btnStart: TBitBtn;
    edtPorta: TLabeledEdit;
    Button1: TButton;
    PopupMenu1: TPopupMenu;
    Restaurar1: TMenuItem;
    Close1: TMenuItem;
    TrayIcon1: TTrayIcon;
    Button2: TButton;
    ImageList1: TImageList;
    Panel1: TPanel;
    Image1: TImage;
    Label2: TLabel;
    lbStatus: TLabel;
    procedure btnStartClick(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure TrayIcon1DblClick(Sender: TObject);
    procedure Restaurar1Click(Sender: TObject);
    procedure Close1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    vCaminhoExeSistema: String;
    FCriptografia: TCriptografia;
    FConexaoBanco: TConexao;
    FLogs: TLogs;
    procedure Status;
    procedure Start;
    procedure Stop;
    procedure InstalarServico;
    procedure ValidacaoEmpresa(Req: THorseRequest; Res: THorseResponse; Next: TProc);
    procedure IniciarConexaoBanco;
    procedure CarregarEstilo;

  public
    { Public declarations }
  end;

var
  frmServer: TfrmServer;

implementation

{$R *.dfm}

{ TfrmServer }

procedure TfrmServer.btnStartClick(Sender: TObject);
begin
  Start;
  Status;
end;

procedure TfrmServer.btnStopClick(Sender: TObject);
begin
  Stop;
  Status;
end;

procedure TfrmServer.Button1Click(Sender: TObject);
begin
  Self.Hide();
  Self.WindowState := wsMinimized;
  TrayIcon1.Visible := True;
  TrayIcon1.Animate := True;
  TrayIcon1.ShowBalloonHint;

end;

procedure TfrmServer.Button2Click(Sender: TObject);
begin
  InstalarServico;
end;

procedure TfrmServer.CarregarEstilo;
var
  vEstilo: String;
  ResStream: TResourceStream;
  Style: TObject;
begin
  try
    vEstilo := 'SapphireKamri';
    vEstilo := ReplaceStr(vEstilo, ' ', '');
    if not FileExists(vCaminhoExeSistema + vEstilo + '.vsf') then
    begin
      ResStream := TResourceStream.Create(HInstance, vEstilo, RT_RCDATA);
      try
        ResStream.Position := 0;
        ResStream.SaveToFile(vCaminhoExeSistema + vEstilo + '.vsf');
      finally
        ResStream.Free;
      end;
    end;

    if FileExists(vCaminhoExeSistema + vEstilo + '.vsf') then
    begin
      Style := TStyleManager.LoadFromFile(vCaminhoExeSistema + vEstilo + '.vsf');
      TStyleManager.SetStyle(Style);
    end;
  except


  end;
end;

procedure TfrmServer.Close1Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmServer.FormCreate(Sender: TObject);
begin
  vCaminhoExeSistema := IncludeTrailingPathDelimiter( ExtractFileDir(Application.ExeName) );

  CarregarEstilo;

  FLogs := TLogs.Create(nil, ExtractFileDir(Application.ExeName), 'sqlcons');
end;

procedure TfrmServer.IniciarConexaoBanco;
const
  coUsuario = 0;
  coSenha   = 1;
  coServer  = 2;
  coPorta   = 3;
  coServico = 4;
  coDireto  = 5;

var
  vServidor: string;
  vAuxiliar: TStringDynArray;
  vIni: TIniFile;
begin
  if not FileExists(vCaminhoExeSistema + 'sigo.ini') then begin
    Showmessage('Arquivo de inicializa��o "sigo.ini" n�o encontrado!');
    Halt;
  end;

  vIni := TIniFile.Create(vCaminhoExeSistema + 'sigo.ini');


  FCriptografia := TCriptografia.Create(Application);
  vServidor := FCriptografia.Descriptografar(vIni.ReadString('SERVIDOR', 'server', ''), False);

  vAuxiliar := SplitString(vServidor, '|');

  try
    FConexaoBanco := _Conexao.TConexao.Create(Application);

    FConexaoBanco.Conectar(
      vAuxiliar[coUsuario],
      vAuxiliar[coSenha],
      vAuxiliar[coServer],
      StrToInt(vAuxiliar[coPorta]),
      vAuxiliar[coServico],
      vAuxiliar[coDireto] = 'S'
    );
  except
    on e: Exception do begin
      Showmessage('Falha ao instanciar objeto de conex�o com o oracle!' + Chr(13) + Chr(10) + e.Message);
      Halt;
    end;
  end;
  vIni.Free;
end;

procedure TfrmServer.InstalarServico;
var
  vExe: String;
  ResStream: TResourceStream;
  vRegistry: TRegistry;
  vMaquina, vUsuario, vSenha, _LocalExe: String;
begin
  _LocalExe := IncludeTrailingPathDelimiter(ExtractFilePath(Application.ExeName));
  try
    vExe := 'instsrv';
    if not FileExists(_LocalEXE + vExe + '.exe') then
    begin
      ResStream := TResourceStream.Create(HInstance, vExe, RT_RCDATA);
      try
        ResStream.Position := 0;
        ResStream.SaveToFile(_LocalEXE + vExe + '.exe');
      finally
        ResStream.Free;
      end;
    end;

    vExe := 'srvany';
    if not FileExists(_LocalEXE + vExe + '.exe') then
    begin
      ResStream := TResourceStream.Create(HInstance, vExe, RT_RCDATA);
      try
        ResStream.Position := 0;
        ResStream.SaveToFile(_LocalEXE + vExe + '.exe');
      finally
        ResStream.Free;
      end;
    end;

    vExe := 'ntrights';
    if not FileExists(_LocalEXE + vExe + '.exe') then
    begin
      ResStream := TResourceStream.Create(HInstance, vExe, RT_RCDATA);
      try
        ResStream.Position := 0;
        ResStream.SaveToFile(_LocalEXE + vExe + '.exe');
      finally
        ResStream.Free;
      end;
    end;

    vUsuario := GetEnvironmentVariable('USERNAME');
    vMaquina := GetEnvironmentVariable('COMPUTERNAME');

    //Buscar senha do usu�rio
    PostMessage(Handle, InputBoxMsg, 0, 0);
    vSenha := InputBox('Autentica��o', 'Informe sua senha (para login do WINDOWS):', '');

    lbStatus.Caption := 'Aguarde, instalando...';

    //Libera o usu�rio para fazer logon como servi�o
    WinExec(PAnsiChar(AnsiString('"' + _LocalEXE + 'ntrights.exe" +r SeServiceLogonRight -u ' + vMaquina + '\' + vUsuario)), SW_HIDE);

    //Cria o servi�o
    WinExec(PAnsiChar(AnsiString('"' + _LocalEXE + 'instsrv.exe" "HivaAPI" "' + _LocalEXE + 'srvany.exe"')), SW_HIDE);

    Sleep(8000);

    //Configura o usu�rio para iniciar o servico
    //Nota: os espa�os depois de "obj=" e "password=" s�o obrigat�rios!
    WinExec(PAnsiChar(AnsiString('sc.exe config "ApiHiva" obj= ".\' + vUsuario + '" password= "' + vSenha + '"')), SW_HIDE);

    //Configura o servi�o
    //HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\MEU_SERVICO
    vRegistry := TRegistry.Create;
    vRegistry.RootKey := HKEY_LOCAL_MACHINE;
    if not vRegistry.OpenKey('SYSTEM\\CurrentControlSet\\Services\\ApiHiva', False) then
    begin
      Sleep(3000);
      vRegistry.OpenKey('SYSTEM\\CurrentControlSet\\Services\\ApiHiva', False);
    end;
    vRegistry.OpenKey('Parameters', True);
    vRegistry.WriteString('Application', ParamStr(0));
    vRegistry.WriteString('AppDirectory', _LocalEXE);
    vRegistry.CloseKey;
    vRegistry.Free;
    showmessage('Servi�o instalado com sucesso!');
  except
    on E: Exception do
    begin
     showmessage('Houve um problema ao instalar a aplica��o como um servi�o. Tente novamente. Erro: ' + E.Message);
    end;
  end;

end;

procedure TfrmServer.Restaurar1Click(Sender: TObject);
begin
  TrayIcon1DblClick(self);
end;

procedure TfrmServer.Start;
begin
  //THorse.Use(Jhonson());
  THorse.Use(HorseBasicAuthentication(
    function(const AUsername, APassword: string): Boolean
    begin
      Result := AUsername.Equals('Hiva') and APassword.Equals('@123456');
    end));
  THorse.Routes.RegisterRoute(mtGet,'/validade/:cnpj', ValidacaoEmpresa);

  THorse.Listen(StrToIntDef(edtPorta.text,9008));

end;

procedure TfrmServer.Status;
begin
  btnStop.Enabled := THorse.IsRunning;
  btnStart.Enabled := not THorse.IsRunning;
  if THorse.IsRunning then
  begin
    lbStatus.Caption := 'Status: Online';
    lbStatus.Font.Color  := clGreen;
  end else
  begin
    lbStatus.Caption := 'Status: Offline';
    lbStatus.Font.Color  := clRed;
  end;

end;

procedure TfrmServer.Stop;
begin
  THorse.StopListen;
  lbStatus.Color  := clRed;
end;

procedure TfrmServer.TrayIcon1DblClick(Sender: TObject);
begin
  TrayIcon1.Visible := False;
  Show();
  WindowState := wsNormal;
  Application.BringToFront();
end;

procedure TfrmServer.ValidacaoEmpresa(Req: THorseRequest; Res: THorseResponse;
  Next: TProc);
var
  qryCliente: TOraQuery;
begin
  qryCliente := TOraQuery.Create(nil);
  try
    IniciarConexaoBanco;
    qryCliente.Session := FConexaoBanco;
    qryCliente.Close;
    qryCliente.SQL.Clear;
    qryCliente.SQL.Add('select distinct(cli.validade) as validade ');
    qryCliente.SQL.Add('  from Cadastros cad, Clientes cli, clientes_filiais cfi ');
    qryCliente.SQL.Add(' where cad.cadastro_id = cli.cadastro_id ');
    qryCliente.SQL.Add(' and cli.cadastro_id = cfi.cadastro_id(+) ');
    qryCliente.SQL.Add('   and (retorna_numeros(cad.cpf_cnpj) = ' + Req.Params.Items['cnpj'].QuotedString);
    qryCliente.SQL.Add('    or retorna_numeros(cfi.cpf_cnpj) = ' + Req.Params.Items['cnpj'].QuotedString);
    qryCliente.SQL.Add('    ) ');
    qryCliente.Open;
    Res.Send(qryCliente.FieldByName('validade').AsString);

    FLogs.AddLog(qryCliente.SQL.Text);
  finally
    qryCliente.Free;
    FConexaoBanco.Close;
  end;
end;

end.
