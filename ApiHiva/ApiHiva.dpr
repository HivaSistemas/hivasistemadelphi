program ApiHiva;

uses
  Vcl.Forms,
  Vcl.Themes,
  Vcl.Styles,
  untServer in 'untServer.pas' {Form1},
  _Criptografia in '..\Repositório\_Criptografia.pas',
  _Conexao in '..\Repositório\_Conexao.pas' {$R *.res},
  _Logs in '..\Repositório\_Logs.pas';

{$R *.res}

begin

  Application.Initialize;
  Application.MainFormOnTaskbar := True;

  Application.Title := 'API Hiva: 1.0.0.0';

  Application.CreateForm(TFrmServer, FrmServer);
  Application.Run;
end.
