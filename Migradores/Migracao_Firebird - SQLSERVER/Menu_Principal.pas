unit Menu_Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,_Conexao,_OperacoesBancoDados,_limpaBaseAltis,
  _MigraAdm, Vcl.Grids, GridLuka, REST.Backend.EMSServices, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef, Data.DB, FireDAC.Comp.Client,
  REST.Backend.EMSFireDAC, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Phys.SQLiteVDataSet, FireDAC.Comp.DataSet,
  FireDAC.Phys.ODBCBase, FireDAC.Stan.ExprFuncs, FireDAC.Phys.SQLiteDef,
  FireDAC.Phys.IBDef, IPPeerClient, FireDAC.Phys.DSDef, FireDAC.Phys.TDBXBase,
  FireDAC.Phys.DS, FireDAC.Phys.IBBase, FireDAC.Phys.IB, FireDAC.Phys.SQLite,
  MemDS, DBAccess, Ora, Vcl.ComCtrls, Vcl.CheckLst, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, FireDAC.Phys.FB, FireDAC.Phys.FBDef, System.MaskUtils, _BibliotecaGenerica;

type
  TFrmMenuPrincipal = class(TForm)
    pgcDados: TPageControl;
    tsImportados: TTabSheet;
    tsNaoImportados: TTabSheet;
    tsErros: TTabSheet;
    mmoHistorico: TMemo;
    mmoErro: TMemo;
    mmoDuplicados: TMemo;
    tsRegistros: TTabSheet;
    mmoRegistro: TMemo;
    chklstOperacoes: TCheckListBox;
    pnl1: TPanel;
    lblHistorico: TLabel;
    pnl2: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    edtserver_dest: TEdit;
    edt_user_orig: TEdit;
    edt_user_dest: TEdit;
    edt_senha_orig: TEdit;
    edt_senha_dest: TEdit;
    edt_porta_dest: TEdit;
    edt_servico_dest: TEdit;
    btnconectar_orig: TButton;
    btnconectar_dest: TButton;
    edtconectado_orig: TEdit;
    edtconectado_dest: TEdit;
    lblBase: TLabel;
    lbl1: TLabel;
    edtserver_padrao: TEdit;
    edt_user_padrao: TEdit;
    edt_senha_padrao: TEdit;
    edt_porta_padrao: TEdit;
    edt_servico_padrao: TEdit;
    btnConectar_padrao: TButton;
    edtconectado_Padrao: TEdit;
    Label2: TLabel;
    lblQuant: TLabel;
    lblImportados: TLabel;
    lblDuplicados: TLabel;
    img1: TImage;
    chkSP: TCheckBox;
    lblDupl: TLabel;
    lblImp: TLabel;
    OraStoredProc1: TOraStoredProc;
    eFornecedorId: TEdit;
    Label4: TLabel;
    fdConexao: TFDConnection;
    eCaminhoFirebird: TEdit;
    qCidades: TFDQuery;
    qBairros: TFDQuery;
    qClientes: TFDQuery;
    qCadastros: TFDQuery;
    qTelefones: TFDQuery;
    qReceber: TFDQuery;
    qFornecedor: TFDQuery;
    qPagar: TFDQuery;
    qProdutos: TFDQuery;
    qMarcas: TFDQuery;
    qUnidades: TFDQuery;
    qTelefonesTELEFONE: TStringField;
    qTelefonesCELULAR: TStringField;
    qTelefonesCOMERCIAL: TStringField;
    qUnidadesUNIDADE_ID: TStringField;
    qUnidadesNOME_UNIDADE: TStringField;
    qUnidadesATIVO: TStringField;
    qLinhaProduto: TFDQuery;
    Edit1: TEdit;
    qCadastrosclienteFornecedor: TStringField;
    qCadastrosCADASTRO_ID: TIntegerField;
    qCadastrosRAZAO_SOCIAL: TStringField;
    qCadastrosNOME_FANTASIA: TStringField;
    qCadastrosCPF_CNPJ: TStringField;
    qCadastrosie: TStringField;
    qCadastrosrg: TStringField;
    qCadastrosTIPO_CONTRIBUINTE: TStringField;
    qCadastrosID_BAIRRO: TIntegerField;
    qCadastrosCOD_IBGE: TStringField;
    qCadastrosNOME_BAIRRO: TStringField;
    qCadastrosNOME_CIDADE: TStringField;
    qCadastrosESTADO_ID: TStringField;
    qCadastrosCEP: TStringField;
    qCadastrosENDERECO: TStringField;
    qCadastrosNUMERO_ENDERECO: TStringField;
    qCadastrosEMAIL: TStringField;
    qCadastrosOBSERVACOES: TStringField;
    qCadastrosSEXO: TSmallintField;
    qCadastrosLIMITE_CREDITO: TBCDField;
    qCadastrosDATA_NASCIMENTO: TSQLTimeStampField;
    qCadastrosNOME_PAI: TStringField;
    qCadastrosNOME_MAE: TStringField;
    qCadastrosTELEFONE: TStringField;
    qCadastrosCELULAR: TStringField;
    qCadastrosCOMERCIAL: TStringField;
    qCadastrosPROFISSAO: TStringField;
    qCadastrosREFERENCIA_ENDERECO: TStringField;
    qCadastrosTIPO_ENDERECO: TStringField;
    qClientesclienteFornecedor: TStringField;
    qClientesCADASTRO_ID: TIntegerField;
    qClientesRAZAO_SOCIAL: TStringField;
    qClientesNOME_FANTASIA: TStringField;
    qClientesCPF_CNPJ: TStringField;
    qClientesie: TStringField;
    qClientesrg: TStringField;
    qClientesTIPO_CONTRIBUINTE: TStringField;
    qClientesID_BAIRRO: TIntegerField;
    qClientesCOD_IBGE: TStringField;
    qClientesNOME_BAIRRO: TStringField;
    qClientesNOME_CIDADE: TStringField;
    qClientesESTADO_ID: TStringField;
    qClientesCEP: TStringField;
    qClientesENDERECO: TStringField;
    qClientesNUMERO_ENDERECO: TStringField;
    qClientesEMAIL: TStringField;
    qClientesOBSERVACOES: TStringField;
    qClientesSEXO: TSmallintField;
    qClientesLIMITE_CREDITO: TBCDField;
    qClientesDATA_NASCIMENTO: TSQLTimeStampField;
    qClientesNOME_PAI: TStringField;
    qClientesNOME_MAE: TStringField;
    qClientesTELEFONE: TStringField;
    qClientesCELULAR: TStringField;
    qClientesCOMERCIAL: TStringField;
    qClientesPROFISSAO: TStringField;
    qClientesREFERENCIA_ENDERECO: TStringField;
    qClientesTIPO_ENDERECO: TStringField;
    qFornecedorclienteFornecedor: TStringField;
    qFornecedorCADASTRO_ID: TIntegerField;
    qFornecedorRAZAO_SOCIAL: TStringField;
    qFornecedorNOME_FANTASIA: TStringField;
    qFornecedorCPF_CNPJ: TStringField;
    qFornecedorie: TStringField;
    qFornecedorrg: TStringField;
    qFornecedorTIPO_CONTRIBUINTE: TStringField;
    qFornecedorID_BAIRRO: TIntegerField;
    qFornecedorCOD_IBGE: TStringField;
    qFornecedorNOME_BAIRRO: TStringField;
    qFornecedorNOME_CIDADE: TStringField;
    qFornecedorESTADO_ID: TStringField;
    qFornecedorCEP: TStringField;
    qFornecedorENDERECO: TStringField;
    qFornecedorNUMERO_ENDERECO: TStringField;
    qFornecedorEMAIL: TStringField;
    qFornecedorOBSERVACOES: TStringField;
    qFornecedorSEXO: TSmallintField;
    qFornecedorLIMITE_CREDITO: TBCDField;
    qFornecedorDATA_NASCIMENTO: TSQLTimeStampField;
    qFornecedorNOME_PAI: TStringField;
    qFornecedorNOME_MAE: TStringField;
    qFornecedorTELEFONE: TStringField;
    qFornecedorCELULAR: TStringField;
    qFornecedorCOMERCIAL: TStringField;
    qFornecedorPROFISSAO: TStringField;
    qFornecedorREFERENCIA_ENDERECO: TStringField;
    qFornecedorTIPO_ENDERECO: TStringField;
    qCadastroscomplemento: TStringField;
    qCidadesNOME: TStringField;
    qCidadesUF: TStringField;
    qCidadesCODIGO_IBGE: TStringField;
    qBairrosBAIRRO_ID: TIntegerField;
    qBairrosNOME_BAIRRO: TStringField;
    qBairrosNOME_CIDADE: TStringField;
    qBairrosESTADO_ID: TStringField;
    qBairrosCOD_IBGE: TStringField;
    qLinhaProdutoID: TStringField;
    qLinhaProdutoCOMISSAO: TIntegerField;
    qLinhaProdutoNOME: TStringField;
    qPagarRECEBER_ID: TIntegerField;
    qPagarCADASTRO_ID: TIntegerField;
    qPagarCPF_CNPJ: TStringField;
    qPagarNOME_CLIENTE: TStringField;
    qPagarDOCUMENTO: TStringField;
    qPagarDATA_VENCIMENTO: TSQLTimeStampField;
    qPagarVALOR_DOCUMENTO: TBCDField;
    qPagarDATA_CADASTRO: TSQLTimeStampField;
    qPagarOBSERVACOES: TStringField;
    qReceberRECEBER_ID: TIntegerField;
    qReceberCADASTRO_ID: TIntegerField;
    qReceberCPF_CNPJ: TStringField;
    qReceberNOME_CLIENTE: TStringField;
    qReceberDOCUMENTO: TStringField;
    qReceberDATA_VENCIMENTO: TSQLTimeStampField;
    qReceberVALOR_DOCUMENTO: TBCDField;
    qReceberDATA_CADASTRO: TSQLTimeStampField;
    qReceberOBSERVACOES: TStringField;
    qProdutosATIVO: TStringField;
    qProdutosPRODUTO_ID: TFDAutoIncField;
    qProdutosNOME_PRODUTO: TStringField;
    qProdutosMARCA_ID: TSmallintField;
    qProdutosNOME_MARCA: TStringField;
    qProdutosUNIDADE_ID: TStringField;
    qProdutosCODIGO_BARRAS: TStringField;
    qProdutosCODIGO_ORIGINAL_FABRICANTE: TStringField;
    qProdutosREFERENCIA: TStringField;
    qProdutosCEST: TStringField;
    qProdutosNCM_ID: TStringField;
    qProdutosTRIBUTACAO_ID: TStringField;
    qProdutosFABRICANTE_ID: TIntegerField;
    qProdutosPESO: TFMTBCDField;
    qProdutosESTOQUE_DISPONIVEL: TBCDField;
    qProdutosPRECO_VAREJO: TBCDField;
    qProdutosCUSTO_COMPRA_COMERCIAL: TBCDField;
    qProdutosCMV: TIntegerField;
    qProdutosCSOSN: TStringField;
    qProdutosFORNECEDOR_ID: TIntegerField;
    qProdutosGRUPO_ID: TStringField;
    qProdutosGRUPO_NOME: TStringField;
    qMarcasMARCA_ID: TSmallintField;
    qMarcasNOME_MARCA: TStringField;
    qReferenciaProduto: TFDQuery;
    qReferenciaProdutoPRODUTO_ID: TFDAutoIncField;
    qReferenciaProdutoREFERENCIA_PRODUTO: TStringField;
    procedure btnconectar_origClick(Sender: TObject);
    procedure btnconectar_destClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    function  roda_sql(vConexao:TConexao;vSql:string):Boolean;
    procedure Habilita_Botoes;
    procedure btnConectar_padraoClick(Sender: TObject);
    procedure img1DblClick(Sender: TObject);
  private
    { Private declarations }

    Conexao_orig : TConexao;
    Conexao_dest : TConexao;
    Conexao_Padrao : TConexao;
  public
    { Public declarations }
    procedure CalculaRegistros(Fconexao_dest:TConexao);

  end;

var
 FrmMenuPrincipal: TFrmMenuPrincipal;

implementation

{$R *.dfm}

 procedure TFrmMenuPrincipal.CalculaRegistros(Fconexao_dest:TConexao);
var
  vSql,vQtdReg:string;
  FConsulta_dest,FConsulta_dest01 : TConsulta;
  i:integer;
begin
  mmoRegistro.Lines.Clear;
  vSql :='select table_name from all_tables' +
       ' where owner = ''' + edt_user_dest.Text + '''' +
       ' ORDER BY table_name';

  FConsulta_dest   := TConsulta.Create(Fconexao_dest);
  FConsulta_dest01 := TConsulta.Create(Fconexao_dest);

  try
    mmoRegistro.Lines.Clear;
    FConsulta_dest.Limpar;
    FConsulta_dest.Add(vSql);
    FConsulta_dest.Pesquisar;

    for i := 0 to FConsulta_dest.GetQuantidadeRegistros -1 do
    begin
      vSql :='select count(*) as quantreg from ' + FConsulta_dest.GetString('table_name');
      FConsulta_dest01.Limpar;
      FConsulta_dest01.Add(vSql);
      FConsulta_dest01.Pesquisar;
      vQtdReg := FConsulta_dest01.GetString('quantreg');

      vQtdReg := FormatFloat('000000',StrToInt(vQtdReg));
      mmoRegistro.Lines.Add( vQtdReg + '  ' + FConsulta_dest.GetString('table_name'));

      FConsulta_dest.Next;
    end;
  finally
    FConsulta_dest.Free;
    FConsulta_dest01.Free;
  end;
end;

function TFrmMenuPrincipal.roda_sql(vConexao:TConexao;vSql:string):Boolean;
var
  vExec: TExecucao;
begin
  Result := True;

  try
    Application.ProcessMessages;
    vExec := TExecucao.Create(vConexao);

    vConexao.IniciarTransacao;
    vExec.SQL.Add(vSql);
    vExec.ExecSQL;
    vConexao.FinalizarTransacao;

  except
    on e: Exception do begin
      Result := False;
      vConexao.VoltarTransacao;
      ShowMessage('Erro ao executar limpeza no banco de Destino.' +
      e.Message);
      Abort;
    end;
  end;
end;

procedure TFrmMenuPrincipal.btnconectar_destClick(Sender: TObject);
var
  vProc: TProcedimentoBanco;
begin
  if btnconectar_dest.Caption = 'Conectar' then
  begin
    try
      Conexao_dest := _Conexao.TConexao.Create(Application);

      Conexao_dest.Conectar(
        edt_user_dest.Text,
        edt_senha_dest.Text,
        edtserver_dest.Text,
        StrToInt(edt_porta_dest.Text),
        edt_servico_dest.Text,
        True
      );

      if Conexao_dest.Connected then
      begin
        edtconectado_dest.Text := 'Conectado';
        edtconectado_dest.Color := clGreen;
        btnconectar_dest.Caption := 'Desconectar';

        vProc := TProcedimentoBanco.Create(Conexao_dest, 'INICIAR_SESSAO');
        vProc.Params[0].AsInteger := 1;
        vProc.Params[1].AsInteger := 1;
        vProc.Params[2].AsString := 'IMPORTADOR';
        vProc.Params[3].AsString := 'IMPORTADOR';

        vProc.Executar;

        CalculaRegistros(Conexao_dest);
      end;
    except
      on e: Exception do begin
        ShowMessage('Falha ao instanciar objeto de conex�o com o oracle.' +
        e.Message);
        edtconectado_dest.Text := 'Desconectado';
        edtconectado_dest.Color := clRed;
        //Exclamar('Falha ao instanciar objeto de conex�o com o oracle! ' + e.Message);
        //Halt;
      end;
    end;
  end else
  begin
    if Conexao_dest.Connected then
    begin
      Conexao_dest.Disconnect;
      edtconectado_dest.Text := 'Desconectado';
      edtconectado_dest.Color := clRed;
      btnconectar_dest.Caption := 'Conectar';
    end;
  end;
end;

procedure TFrmMenuPrincipal.btnconectar_origClick(Sender: TObject);
begin
  if fdConexao.Connected then begin
    fdConexao.Connected := False;
    edtconectado_orig.Text := 'Desconectedo';
    edtconectado_orig.Color := clRed;
    btnconectar_orig.Caption := 'Conecter';
  end
  else begin
    fdConexao.Params.UserName := edt_user_orig.Text;
    fdConexao.Params.Password := edt_senha_orig.Text;
    fdConexao.Params.Database := Edit1.Text;
    fdConexao.Params.Values['Server'] := eCaminhoFirebird.Text;
//    fdConexao.Params.Server := eCaminhoFirebird.Text;
//    fdConexao.Params.Strings := 'User_Name=sa' +
//      'Password=dados' +
//      'Server=DESKTOP-4IBQV4C\SQLEXPRESS' +
//      'DriverID=MSSQL';
    fdConexao.Connected := True;

    if fdConexao.Connected then begin
      edtconectado_orig.Text := 'Conectado';
      edtconectado_orig.Color := clGreen;
      btnconectar_orig.Caption := 'Desconectar';
    end;

  end;

//  if btnconectar_orig.Caption = 'Conectar' then
//  begin
//    try
//      Conexao_orig := _Conexao.TConexao.Create(Application);
//
//      Conexao_orig.Conectar(
//        edt_user_orig.Text,
//        edt_senha_orig.Text,
//        edtserver_orig.Text,
//        StrToInt(edt_porta_orig.Text),
//        edt_servico_orig.Text,
//        True
//      );
//
//      if Conexao_orig.Connected then
//      begin
//        edtconectado_orig.Text := 'Conectado';
//        edtconectado_orig.Color := clGreen;
//        btnconectar_orig.Caption := 'Desconectar';
//
//      end;
//    except
//      on e: Exception do begin
//        ShowMessage('Falha ao instanciar objeto de conex�o com o oracle.' +
//        e.Message);
//        edtconectado_orig.Text := 'Desconectado';
//        edtconectado_orig.Color := clRed;
//        //Exclamar('Falha ao instanciar objeto de conex�o com o oracle! ' + e.Message);
//        //Halt;
//      end;
//    end;
//  end else
//  begin
//    if Conexao_orig.Connected then
//    begin
//      Conexao_orig.Disconnect;
//      edtconectado_orig.Text := 'Desconectado';
//      edtconectado_orig.Color := clRed;
//      btnconectar_orig.Caption := 'Conectar';
//    end;
//  end;
end;

procedure TFrmMenuPrincipal.btnConectar_padraoClick(Sender: TObject);
begin
  if btnConectar_padrao.Caption = 'Conectar' then
  begin
    try
      Conexao_Padrao := _Conexao.TConexao.Create(Application);

      Conexao_Padrao.Conectar(
        edt_user_padrao.Text,
        edt_senha_padrao.Text,
        edtserver_padrao.Text,
        StrToInt(edt_porta_padrao.Text),
        edt_servico_padrao.Text,
        True
      );

      if Conexao_Padrao.Connected then
      begin
        edtconectado_Padrao.Text := 'Conectado';
        edtconectado_Padrao.Color := clGreen;
        btnConectar_padrao.Caption := 'Desconectar';

//        orstrdprc1.StoredProcName := 'INICIAR_SESSAO';
//                orstrdprc1.Prepare;
//        orstrdprc1.Params[0].AsInteger := 1;
//        orstrdprc1.Params[1].AsInteger := 1;
//        orstrdprc1.Params[2].AsString := 'IMPORTADOR';
//        orstrdprc1.Params[3].AsString := 'IMPORTADOR';
//
//        orstrdprc1.ExecProc;
      end;
    except
      on e: Exception do begin
        ShowMessage('Falha ao instanciar objeto de conex�o com o oracle.' +
        e.Message);
        edtconectado_Padrao.Text := 'Desconectado';
        edtconectado_Padrao.Color := clRed;
        //Exclamar('Falha ao instanciar objeto de conex�o com o oracle! ' + e.Message);
        Halt;
      end;
    end;
  end else
  begin
    if Conexao_Padrao.Connected then
    begin
      Conexao_Padrao.Disconnect;
      edtconectado_Padrao.Text := 'Desconectado';
      edtconectado_Padrao.Color := clRed;
      btnConectar_padrao.Caption := 'Conectar';
    end;
  end;
end;

procedure TFrmMenuPrincipal.FormShow(Sender: TObject);
begin
//  edt_user_orig.Text := 'BASELIMPAOFICIAL';
//  edt_senha_orig.Text := 'DADOS';
//  edtserver_orig.Text := 'localhost';

//  edt_user_dest.Text := 'BASELIMPA';
//  edt_senha_dest.Text := 'DADOS';
//  edtserver_dest.Text := 'localhost';
//  edt_senha_dest.Text := 'DADOS';
//  edt_senha_padrao.Text := 'DADOS';

 {0  Limpar Base de Destino
  1  Cidades
  2  Rotas
  3  Bairros
  4  Cargos
  5  Pessoas
  6  Funcionarios
  7  Clientes
  8  Grupo de Fornecedor
  9  Fornecedores
  10 Transportadoras
  11 Veiculos
  12 Empresas
  13 Marcas
  14 Unidades
  15 Departamentos
  16 Produtos
  17 Precos
  18 Custos
  19 Estoque
  20 Portadores
  21 Contas a Receber
  22 Contas a Pagar }
  //chklstOperacoes.ItemEnabled[2]:= False;
  //chklstOperacoes.ItemEnabled[6]:= False;
  //chklstOperacoes.ItemEnabled[8]:= False;
//  chklstOperacoes.ItemEnabled[10]:= False;
//  chklstOperacoes.ItemEnabled[11]:= False;
//  chklstOperacoes.ItemEnabled[12]:= False;
//  chklstOperacoes.ItemEnabled[19]:= False;
//  chklstOperacoes.ItemEnabled[20]:= False;
end;

procedure TFrmMenuPrincipal.Habilita_Botoes;
begin
//
end;

procedure TFrmMenuPrincipal.img1DblClick(Sender: TObject);
var
  ok:Boolean;
  Fcidade:TArray<RecCidade>;
  Fbairro:TArray<RecBairro>;
  Fcadastro:TArray<RecCadastro>;
  FReceber:TArray<RecReceber>;
  FPagar:TArray<RecPagar>;
  FProdutos: TArray<RecProduto>;
  FMarcas: TArray<RecMarca>;
  FUnidades: TArray<RecUnidade>;
  FLinha: TArray<RecLinhaProduto>;
  i: Integer;
  indTel: Integer;
begin
  if not ((edtconectado_orig.Text = 'Conectado') and (edtconectado_dest.Text = 'Conectado')) then
  begin
    ShowMessage('Verifique as conex�es!');
    Abort;
  end;

 {0  Limpar Base de Destino
  1  Cidades
  2  Rotas
  3  Bairros
  4  Cargos
  5  Pessoas
  6  Funcionarios
  7  Clientes
  8  Grupo de Fornecedor
  9  Fornecedores
  10 Transportadoras
  11 Veiculos
  12 Empresas
  13 Marcas
  14 Unidades
  15 Departamentos
  16 Produtos
  17 Precos
  18 Custos
  19 Estoque
  20 Portadores
  21 Contas a Receber
  22 Contas a Pagar }

 ok := True;

 if chklstOperacoes.Checked[0] then
   ok := LimparBaseDest(Conexao_dest,lblHistorico,lblImportados,lblDuplicados,mmoHistorico,mmoErro,edt_user_dest.Text);

// if edtconectado_Padrao.Text = 'Conectado' then
//   ok:= Importa_Origens(chkSP.Checked,Conexao_Padrao,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

 if ok and chklstOperacoes.Checked[1] then begin
  qCidades.Active := False;
  qCidades.Active := True;

  qCidades.Last;
  qCidades.First;

  SetLength(Fcidade, qCidades.RecordCount);
  qCidades.First;
  for i := 0 to qCidades.RecordCount - 1 do begin
    Fcidade[i].EstadoId  := qCidadesUF.AsString;
    Fcidade[i].CodIbge   := RetornaNumeros(qCidadesCODIGO_IBGE.AsString);
    Fcidade[i].Nome      := qCidadesNOME.AsString;

    qCidades.Next;
  end;

  if Fcidade <> nil then
    ok := Importa_Cidades(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FCidade)
  else
    ShowMessage('Nenhuma cidade para importa��o!');
 end;

// if ok and chklstOperacoes.Checked[2] then
//   ok := Importa_Rotas(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

 if ok and chklstOperacoes.Checked[3] then begin
  qBairros.Active := False;
  qBairros.Active := True;

  qBairros.Last;
  qBairros.First;

  SetLength(FBairro, qBairros.RecordCount);
  qBairros.First;

  for i := 0 to qBairros.RecordCount - 1 do begin
    Fbairro[i].Bairro  := qBairrosNOME_BAIRRO.AsString;
    Fbairro[i].Cidade  := qBairrosNOME_CIDADE.AsString;
    Fbairro[i].CodIbge := RetornaNumeros(qBairrosCOD_IBGE.AsString);
    Fbairro[i].Estado  := qBairrosESTADO_ID.AsString;

    qBairros.Next;
  end;

  if Fbairro <> nil then
    ok := Importa_Bairros(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FBairro)
  else
    ShowMessage('Nenhum bairro para importa��o!');
 end;


// if ok and chklstOperacoes.Checked[4] then
//   ok := Importa_Cargos(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[4] then
//   ok := Importa_Profissionais_Cargos(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

 if ok and chklstOperacoes.Checked[5] then begin
  Fcadastro := nil;
  qCadastros.Active := False;
  qCadastros.Active := True;

  qCadastros.Last;

  SetLength(Fcadastro, qCadastros.RecordCount);
  qCadastros.First;

  for i := 0 to qCadastros.RecordCount - 1 do begin
    Fcadastro[i].Cadastro_id := qCadastrosCADASTRO_ID.AsString;
    if Length(RetornaNumeros(qCadastrosCPF_CNPJ.AsString)) = 14 then
      Fcadastro[i].tp_pessoa := 'F'
    else
      Fcadastro[i].tp_pessoa := 'J';

    Fcadastro[i].Nome        := qCadastrosRAZAO_SOCIAL.AsString;
    Fcadastro[i].NomeFantasia := qCadastrosNOME_FANTASIA.AsString;

    if qCadastrosTIPO_ENDERECO.AsString <> '' then
      Fcadastro[i].endereco  := qCadastrosTIPO_ENDERECO.AsString + ' ' + qCadastrosENDERECO.AsString
    else
      Fcadastro[i].endereco  := qCadastrosENDERECO.AsString;
    Fcadastro[i].Bairro      := qCadastrosNOME_BAIRRO.AsString;
    Fcadastro[i].Cidade      := qCadastrosNOME_CIDADE.AsString;
    Fcadastro[i].Estado      := qCadastrosESTADO_ID.AsString;

    Fcadastro[i].cep         := RetornaNumeros(qCadastrosCEP.AsString);
    if Length(FCadastro[i].cep) <> 8 then
      FCadastro[i].cep := '7640000';

    Fcadastro[i].cep := FormatMaskText('00000\-000;0;', Fcadastro[i].cep);

    Fcadastro[i].limite_credito  := qCadastrosLIMITE_CREDITO.AsString;
    Fcadastro[i].data_nascimento := qCadastrosDATA_NASCIMENTO.AsString;
    Fcadastro[i].cpf             := qCadastrosCPF_CNPJ.AsString;
    Fcadastro[i].cnpj            := qCadastrosCPF_CNPJ.AsString;
    if qCadastrosRG.AsString <> '' then
      Fcadastro[i].inscricao       := qCadastrosRG.AsString
    else
      Fcadastro[i].inscricao       := qCadastrosIE.AsString;

    Fcadastro[i].numero_end      := qCadastrosNUMERO_ENDERECO.AsString;
    if Length(Fcadastro[i].numero_end) > 10 then
      Fcadastro[i].numero_end := '';

    Fcadastro[i].email           := qCadastrosEMAIL.AsString;
    Fcadastro[i].complemento     := qCadastroscomplemento.AsString;

    Fcadastro[i].sexo            := qCadastrosSEXO.AsString;
    Fcadastro[i].observacoes     := qCadastrosOBSERVACOES.AsString;
    if qCadastrosPROFISSAO.AsString <> '' then
      Fcadastro[i].observacoes := Fcadastro[i].observacoes  + ' Profiss�o: ' + qCadastrosPROFISSAO.AsString;


    Fcadastro[i].telefone    := RetornaNumeros(qCadastrosTELEFONE.AsString);
    if Fcadastro[i].telefone = '' then
      Fcadastro[i].telefone    := RetornaNumeros(qCadastrosCOMERCIAL.AsString);

    if (Length(Fcadastro[i].telefone) = 8) or (Length(Fcadastro[i].telefone) = 9) then
      Fcadastro[i].telefone := '62' + Fcadastro[i].telefone;

    Fcadastro[i].telefone    := FormatMaskText('\(00\)\ 0000\-0009;0;', Fcadastro[i].telefone);

    Fcadastro[i].celular     := RetornaNumeros(qCadastrosCELULAR.AsString);
    if (Length(Fcadastro[i].celular) = 8) or (Length(Fcadastro[i].celular) = 9) then
      Fcadastro[i].celular := '62' + Fcadastro[i].celular;

    Fcadastro[i].celular     := FormatMaskText('\(00\)\ 00000\-0009;0;', Fcadastro[i].celular);

    qCadastros.Next;
  end;

  if Fcadastro <> nil then
    ok := Importa_Pessoas(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, Fcadastro, qTelefones)
  else
    ShowMessage('Nenhum pessoa para importa��o!');

 end;


// if ok and chklstOperacoes.Checked[5] then
//   ok := Importa_Telefones (chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[5] then
//   ok := Importa_DIVERSOS_CONTATOS(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[5] then
//   ok := Importa_EMAILS (chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[5] then
//   ok := Importa_ENDERECOS (chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[6] then
//   ok := Importa_Funcionarios(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[6] then
//   ok := Importa_Profissionais(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[7] then
//   ok := Importa_GRUPOS_FINANCEIROS(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[7] then
//   ok := Importa_CLIENTES_GRUPOS(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[7] then
//   ok := Importa_CLIENTES_ORIGENS(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

 if ok and chklstOperacoes.Checked[7] then begin
  Fcadastro := nil;
  qClientes.Active := False;
  qClientes.Active := True;

  qClientes.Last;

  SetLength(Fcadastro, qClientes.RecordCount);
  qClientes.First;

  for i := 0 to qClientes.RecordCount - 1 do begin
    Fcadastro[i].Cadastro_id := qClientesCADASTRO_ID.AsString;
    if Length(qClientesCPF_CNPJ.AsString) = 14 then
      Fcadastro[i].tp_pessoa := 'F'
    else
      Fcadastro[i].tp_pessoa := 'J';

    Fcadastro[i].Nome        := qClientesRAZAO_SOCIAL.AsString;
    Fcadastro[i].NomeFantasia := qClientesNOME_FANTASIA.AsString;
    Fcadastro[i].endereco    := qClientesENDERECO.AsString;
    Fcadastro[i].Bairro      := qClientesNOME_BAIRRO.AsString;
    Fcadastro[i].Cidade      := qClientesNOME_CIDADE.AsString;
    Fcadastro[i].Estado      := qClientesESTADO_ID.AsString;

    Fcadastro[i].cep         := RetornaNumeros(qClientesCEP.AsString);
    if Length(FCadastro[i].cep) <> 8 then
      FCadastro[i].cep := '7640000';

    Fcadastro[i].cep := FormatMaskText('00000\-000;0;', Fcadastro[i].cep);

    Fcadastro[i].limite_credito  := qClientesLIMITE_CREDITO.AsString;
    Fcadastro[i].data_nascimento := qClientesDATA_NASCIMENTO.AsString;
    Fcadastro[i].cpf             := qClientesCPF_CNPJ.AsString;
    Fcadastro[i].cnpj            := qClientesCPF_CNPJ.AsString;

    if qCadastrosRG.AsString <> '' then
      Fcadastro[i].inscricao       := qClientesRG.AsString
    else
      Fcadastro[i].inscricao       := qClientesIE.AsString;

    Fcadastro[i].numero_end      := qClientesNUMERO_ENDERECO.AsString;
    Fcadastro[i].email           := qClientesEMAIL.AsString;

    Fcadastro[i].sexo            := qClientesSEXO.AsString;
    Fcadastro[i].observacoes     := qClientesOBSERVACOES.AsString;
    Fcadastro[i].nome_pai        := qClientesNOME_PAI.AsString;
    Fcadastro[i].nome_mae        := qClientesNOME_MAE.AsString;

    qClientes.Next;
  end;

  if Fcadastro <> nil then
   ok := Importa_Clientes(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, Fcadastro)
  else
    ShowMessage('Nenhum pessoa para importa��o!');

 end;

// if ok and chklstOperacoes.Checked[8] then
//   ok := Importa_GrupoFornecedor(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

 if ok and chklstOperacoes.Checked[9] then begin
  Fcadastro := nil;

  qFornecedor.Active := False;
  qFornecedor.Active := True;

  qFornecedor.Last;

  SetLength(Fcadastro, qFornecedor.RecordCount);
  qFornecedor.First;

  for i := 0 to qFornecedor.RecordCount - 1 do begin
    Fcadastro[i].Cadastro_id := qFornecedorCADASTRO_ID.AsString;
    if Length(qFornecedorCPF_CNPJ.AsString) = 14 then
      Fcadastro[i].tp_pessoa := 'F'
    else
      Fcadastro[i].tp_pessoa := 'J';

    Fcadastro[i].Nome        := qFornecedorRAZAO_SOCIAL.AsString;
    Fcadastro[i].NomeFantasia := qFornecedorNOME_FANTASIA.AsString;
    Fcadastro[i].endereco    := qFornecedorENDERECO.AsString;
    Fcadastro[i].Bairro      := qFornecedorNOME_BAIRRO.AsString;
    Fcadastro[i].Cidade      := qFornecedorNOME_CIDADE.AsString;
    Fcadastro[i].Estado      := qFornecedorESTADO_ID.AsString;

    Fcadastro[i].cep         := RetornaNumeros(qFornecedorCEP.AsString);
    if Length(FCadastro[i].cep) <> 8 then
      FCadastro[i].cep := '7640000';

    Fcadastro[i].cep := FormatMaskText('00000\-000;0;', Fcadastro[i].cep);

    Fcadastro[i].telefone    := RetornaNumeros(qFornecedorCOMERCIAL.AsString);
    if (Length(Fcadastro[i].telefone) = 8) or (Length(Fcadastro[i].telefone) = 9) then
      Fcadastro[i].telefone := '62' + Fcadastro[i].telefone;

    Fcadastro[i].telefone    := FormatMaskText('\(00\)\ 0000\-0009;0;', Fcadastro[i].telefone);

    Fcadastro[i].celular     := RetornaNumeros(qFornecedorCELULAR.AsString);
    if (Length(Fcadastro[i].celular) = 8) or (Length(Fcadastro[i].celular) = 9) then
      Fcadastro[i].celular := '62' + Fcadastro[i].celular;

    Fcadastro[i].celular     := FormatMaskText('\(00\)\ 00000\-0009;0;', Fcadastro[i].celular);

    Fcadastro[i].cpf             := qFornecedorCPF_CNPJ.AsString;
    Fcadastro[i].cnpj            := qFornecedorCPF_CNPJ.AsString;
    if qCadastrosRG.AsString <> '' then
      Fcadastro[i].inscricao       := qFornecedorRG.AsString
    else
      Fcadastro[i].inscricao       := qFornecedorIE.AsString;
    Fcadastro[i].numero_end      := qFornecedorNUMERO_ENDERECO.AsString;
    Fcadastro[i].email           := qFornecedorEMAIL.AsString;

    Fcadastro[i].observacoes     := qFornecedorOBSERVACOES.AsString;
//    Fcadastro[i].complemento := sheet.cells[i, 19].value;
//    Fcadastro[i].ponto_referencia := sheet.cells[i, 20].value;

    qFornecedor.Next;
  end;

  if Fcadastro <> nil then
    ok := Importa_Fornecedores(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, Fcadastro)
  else
    ShowMessage('Nenhum pessoa para importa��o!');

 end;

//  if ok and chklstOperacoes.Checked[10] then
//   ok := Importa_Transportadoras(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[11] then
//   ok := Importa_Motoristas(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[11] then
//   ok := Importa_Veiculos(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);;

// if ok and chklstOperacoes.Checked[12] then
//   ok := Importa_Empresas(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

 if ok and chklstOperacoes.Checked[13] then begin
  qMarcas.Active := False;
  qMarcas.Active := True;

  qMarcas.Last;
  qMarcas.First;

  SetLength(FMarcas, qMarcas.RecordCount);
  qMarcas.First;

  for i := 0 to qMarcas.RecordCount - 1 do begin
    FMarcas[i].Codigo  := qMarcasMARCA_ID.AsString;
    FMarcas[i].Marca   := qMarcasNOME_MARCA.AsString;
    qMarcas.Next;
  end;

  if qMarcas <> nil then
    ok := Importa_Marcas(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FMarcas)
  else
    ShowMessage('Nenhuma marca para importa��o!');
 end;

 if ok and chklstOperacoes.Checked[14] then begin
  qUnidades.Active := False;
  qUnidades.Active := True;

  qUnidades.Last;
  qUnidades.First;

  SetLength(FUnidades, qUnidades.RecordCount);
  qUnidades.First;

  for i := 0 to qUnidades.RecordCount - 1 do begin
    FUnidades[i].codigo  := qUnidadesUNIDADE_ID.AsString;
    FUnidades[i].unidade   := qUnidadesUNIDADE_ID.AsString;
    FUnidades[i].descricao   := qUnidadesNOME_UNIDADE.AsString;
    qUnidades.Next;
  end;

  if FUnidades <> nil then
    ok := Importa_Unidades(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FUnidades)
  else
    ShowMessage('Nenhuma marca para importa��o!');
 end;

 if ok and chklstOperacoes.Checked[15] then begin
  qLinhaProduto.Active := False;
  qLinhaProduto.Active := True;

  qLinhaProduto.Last;
  qLinhaProduto.First;

  SetLength(FLinha, qLinhaProduto.RecordCount);
  qLinhaProduto.First;

  for i := 0 to qLinhaProduto.RecordCount - 1 do begin
    FLinha[i].codigo  := qLinhaProdutoID.AsString;
    FLinha[i].nome   := qLinhaProdutoNOME.AsString;
    FLinha[i].comissao   := qLinhaProdutoCOMISSAO.AsString;
    qLinhaProduto.Next;
  end;

  if FLinha <> nil then
    ok := Importa_Grupos_Produtos(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FLinha)
  else
    ShowMessage('Nenhum GRUPO para importa��o!');
 end;

 if ok and chklstOperacoes.Checked[16] then begin
  FProdutos := nil;
  qProdutos.Active := False;
  qProdutos.Active := True;

  qProdutos.Last;
  qProdutos.First;

  SetLength(FProdutos, qProdutos.RecordCount);
  qProdutos.First;

  for i := 0 to qProdutos.RecordCount - 1 do begin
    FProdutos[i].Codigo         := qProdutosPRODUTO_ID.AsString;
    FProdutos[i].codigo_barras  := qProdutosCODIGO_BARRAS.AsString;
    if Length(FProdutos[i].codigo_barras) > 14 then
      FProdutos[i].codigo_barras := '';

    FProdutos[i].cod_marca      := qProdutosMARCA_ID.AsString;
    FProdutos[i].nome           := Copy(qProdutosNOME_PRODUTO.AsString, 0, 60);
    FProdutos[i].unidade        := qProdutosUNIDADE_ID.AsString;
    FProdutos[i].multiplo_venda := 0;
    FProdutos[i].referencia     := qProdutosREFERENCIA.AsString;
    FProdutos[i].ativo          := qProdutosATIVO.AsString;
    FProdutos[i].cest           := qProdutosCEST.AsString;
    FProdutos[i].fornecedor_id  := qProdutosFABRICANTE_ID.AsString;
    FProdutos[i].ncm            := RetornaNumeros(qProdutosNCM_ID.AsString);
    FProdutos[i].nomeGrupo      := qProdutosGRUPO_NOME.AsString;
    FProdutos[i].idGrupo        := qProdutosGRUPO_ID.AsString;

    if qProdutosCSOSN.AsString = '500' then begin
      FProdutos[i].tributacao_estadual_compra := 2;
      FProdutos[i].tributacao_estadual_venda  := 2;
    end
    else begin
      FProdutos[i].tributacao_estadual_compra := 1;
      FProdutos[i].tributacao_estadual_venda  := 1;
    end;

    FProdutos[i].tributacao_federal_compra  := 1;
    FProdutos[i].tributacao_federal_venda   := 1;

    FProdutos[i].tipo_def_automatica_lote := 'V';

    FProdutos[i].estoque := qProdutosESTOQUE_DISPONIVEL.AsFloat;
    FProdutos[i].preco := qProdutosPRECO_VAREJO.AsFloat;
    FProdutos[i].custo := qProdutosCUSTO_COMPRA_COMERCIAL.AsFloat;

    FProdutos[i].peso := qProdutosPESO.AsFloat;
    FProdutos[i].cst := qProdutosCSOSN.asString;

    qProdutos.Next;
  end;

  if FProdutos <> nil then
    ok := Importa_Produtos(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, eFornecedorId.Text, FProdutos)
  else
    ShowMessage('Nenhum produto para importa��o!');
 end;

// if ok and chklstOperacoes.Checked[16] then
//   ok := Importa_PRODUTOS_Relacionados(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[16] then
//   ok := Importa_PRODUTOS_kit(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

//  if ok and chklstOperacoes.Checked[16] then
//   ok := Importa_Produtos_lotes(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[16] then
//   ok := Importa_Produtos_Cod(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[16] then
//   ok := Multiplos_Compra(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

 if ok and chklstOperacoes.Checked[17] then begin
  FProdutos := nil;
  qProdutos.Active := False;
  qProdutos.Active := True;

  qProdutos.Last;
  qProdutos.First;

  SetLength(FProdutos, qProdutos.RecordCount);
  qProdutos.First;

  for i := 0 to qProdutos.RecordCount - 1 do begin
    FProdutos[i].Codigo  := qProdutosPRODUTO_ID.AsString;
    FProdutos[i].preco   := qProdutosPRECO_VAREJO.AsFloat;

    qProdutos.Next;
  end;

  if FProdutos <> nil then
    ok := Importa_Precos(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FProdutos)
  else
    ShowMessage('Nenhum produto para importa��o!');

 end;

// if ok and chklstOperacoes.Checked[17] then
//   ok := Importa_Promocoes(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

 if ok and chklstOperacoes.Checked[18] then begin
  FProdutos := nil;
  qProdutos.Active := False;
  qProdutos.Active := True;

  qProdutos.Last;
  qProdutos.First;

  SetLength(FProdutos, qProdutos.RecordCount);
  qProdutos.First;

  for i := 0 to qProdutos.RecordCount - 1 do begin
    FProdutos[i].Codigo         := qProdutosPRODUTO_ID.AsString;
    FProdutos[i].custo := qProdutosCUSTO_COMPRA_COMERCIAL.AsFloat;

    qProdutos.Next;
  end;

  if FProdutos <> nil then
    ok := Importa_Custos(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FProdutos)
  else
    ShowMessage('Nenhum produto para importa��o!');

 end;

// if ok and chklstOperacoes.Checked[19] then
//   ok := Inicia_Estoques(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);
//
// if ok and chklstOperacoes.Checked[19] then
//   ok := Inicia_Estoques_Div(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

 if ok and chklstOperacoes.Checked[19] then begin
  FProdutos := nil;
  qProdutos.Active := False;
  qProdutos.Active := True;

  qProdutos.Last;
  qProdutos.First;

  SetLength(FProdutos, qProdutos.RecordCount);
  qProdutos.First;

  for i := 0 to qProdutos.RecordCount - 1 do begin
    FProdutos[i].Codigo         := qProdutosPRODUTO_ID.AsString;
    FProdutos[i].estoque := qProdutosESTOQUE_DISPONIVEL.AsFloat;

    qProdutos.Next;
  end;

  if FProdutos <> nil then
    ok := Importa_Estoques(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FProdutos)
  else
    ShowMessage('Nenhum produto para importa��o!');

 end;

// if ok and chklstOperacoes.Checked[19] then
//   ok := Importa_Tributacao(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[20] then
//   ok := Importa_Portadores(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[20] then
//   ok := Importa_Contas(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[20] then
//   ok := Importa_Planos_Financeiros(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[20] then
//   ok := Importa_Centros_Custos(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

 if ok and chklstOperacoes.Checked[21] then begin
  qReceber.Active := False;
  qReceber.Active := True;

  qReceber.Last;
  qReceber.First;

  SetLength(FReceber, qReceber.RecordCount);
  qReceber.First;

  for i := 0 to qReceber.RecordCount - 1 do begin
    FReceber[i].id_participante  := qReceberCPF_CNPJ.AsString;
    FReceber[i].cpf_cnpj  := qReceberCPF_CNPJ.AsString;
    FReceber[i].documento := qReceberDOCUMENTO.AsString;
    FReceber[i].data_emissao  :=  Copy(qReceberDATA_CADASTRO.AsString, 0, 10);
    FReceber[i].data_vencimento  := Copy(qReceberDATA_VENCIMENTO.AsString, 0, 10);
    FReceber[i].data_lancamento  := Copy(qReceberDATA_CADASTRO.AsString, 0, 10);
    FReceber[i].observacoes  := qReceberOBSERVACOES.AsString + ' ID ANTERIOR: ' + qReceberRECEBER_ID.AsString + ' CLIENTE: ' + qReceberNOME_CLIENTE.AsString;
    FReceber[i].valor_aberto  := qReceberVALOR_DOCUMENTO.AsString;
    FReceber[i].id_receber  := qReceberRECEBER_ID.AsString;

    qReceber.Next;
  end;

  if FReceber <> nil then
    ok := Importa_Contas_Receber(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FReceber)
  else
    ShowMessage('Nenhum contas a receber para importa��o!');

 end;

 if ok and chklstOperacoes.Checked[22] then begin
  qPagar.Active := False;
  qPagar.Active := True;

  qPagar.Last;
  qPagar.First;

  SetLength(FPagar, qPagar.RecordCount);
  qPagar.First;

  for i := 0 to qPagar.RecordCount - 1 do begin
    FPagar[i].id_participante := qPagarCADASTRO_ID.AsString;
    FPagar[i].cpf_cnpj        := qPagarCPF_CNPJ.AsString;
    FPagar[i].documento       := qPagarDOCUMENTO.AsString;
    FPagar[i].data_emissao    := Copy(qPagarDATA_CADASTRO.AsString, 0, 10);
    FPagar[i].data_vencimento := Copy(qPagarDATA_VENCIMENTO.AsString, 0, 10);
    FPagar[i].data_lancamento := Copy(qPagarDATA_CADASTRO.AsString, 0, 10);
    FPagar[i].observacoes     := qPagarOBSERVACOES.AsString + ' ID ANTERIOR: ' + qPagarRECEBER_ID.AsString;
    FPagar[i].valor_aberto    := qPagarVALOR_DOCUMENTO.AsString;
    FPagar[i].id_receber      := qPagarRECEBER_ID.AsString;
    FPagar[i].nome_consumidor := qPagarNOME_CLIENTE.AsString;

    qPagar.Next;
  end;

  if FPagar <> nil then
     ok := Importa_Contas_Pagar(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FPagar)
  else
    ShowMessage('Nenhum contas a pagar para importa��o!');
 end;

 if ok and chklstOperacoes.Checked[23] then begin
  qReferenciaProduto.Active := False;
  qReferenciaProduto.Active := True;

  qReferenciaProduto.Last;
  qReferenciaProduto.First;

  SetLength(FMarcas, qReferenciaProduto.RecordCount);
  qReferenciaProduto.First;

  for i := 0 to qReferenciaProduto.RecordCount - 1 do begin
    FMarcas[i].Codigo  := qReferenciaProdutoPRODUTO_ID.AsString;
    FMarcas[i].Marca   := qReferenciaProdutoREFERENCIA_PRODUTO.AsString;
    qReferenciaProduto.Next;
  end;

  if qReferenciaProduto <> nil then
    ok := Importa_Referencia(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FMarcas)
  else
    ShowMessage('Nenhuma marca para importa��o!');
 end;

// if ok and chklstOperacoes.Checked[23] then
//   ok := Importa_icms(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[24] then
//   ok := Importa_CFOP(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[24] then
//   ok := Importa_Cfop_Cst(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[24] then
//   ok := Importa_Cfop_Cst1(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

  CalculaRegistros(Conexao_dest);

  if ok then
    ShowMessage('Importacao Realizada com Sucesso!');
end;

end.
