unit _MigraAdm;

interface

uses
  _Conexao,_OperacoesBancoDados,Vcl.StdCtrls,System.SysUtils,Vcl.Dialogs,Vcl.forms,_LimpaBaseAltis,
  System.Variants,System.Classes,Data.DB,GridLuka,REST.Backend.EMSServices, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef, FireDAC.Comp.Client,
  REST.Backend.EMSFireDAC, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Phys.SQLiteVDataSet, FireDAC.Comp.DataSet,
  FireDAC.Phys.ODBCBase, FireDAC.VCLUI.Wait, FireDAC.Comp.UI,
  _BibliotecaGenerica;

function Importa_Generico  (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Cidades   (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Rotas     (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Bairros   (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Cargos    (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Profissionais_Cargos(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Pessoas   (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Telefones (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_DIVERSOS_CONTATOS(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_EMAILS           (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Profissionais    (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Funcionarios     (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_CLIENTES_ORIGENS (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_CLIENTES_GRUPOS(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_GRUPOS_FINANCEIROS(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Clientes  (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Fornecedores   (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Transportadoras(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;

function Importa_GrupoFornecedor(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;

function Importa_Pessoas2  (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;

function Importa_icms(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Origens(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;

function Importa_credito_Pagar(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Contas_Receber(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Custos(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Precos(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Produtos(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;



function Importa_Empresas  (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Portadores(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Motoristas(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Veiculos  (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Grupos_Produtos(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Unidades  (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Marcas    (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;









function Valida_Data(vConsulta:TConsulta):Variant;
function Troca_Caracter(vStr,p1,p2:string):string;
function getFoto(pConexao: Tconexao; pFuncionarioId: Integer): TMemoryStream;
function Recria_Sequencia(aux_seq,Nome_Tabela,chave:String;FConsulta_dest:TConsulta;Conexao_dest:TConexao;lblHis:TLabel;MemErro:Tmemo):Boolean;
function NParametros(np:integer):string;
function Ajusta_Cnpj(pCnpj:string):string;
function Max_Consulta(FConsulta_dest:TConsulta;nCampo,Nome_Tabela:string):string;
function Num_Registros(pMenos:Integer;FConsulta_orig:TConsulta;pNome_Tabela:string;MemErro:Tmemo):string;
function Existe_Registro(FConsulta_dest:TConsulta;vSql,chave:string;MemErro:TMemo):Boolean;
function Busca_Registro(FConsulta_dest:TConsulta;vSql,chave:string;MemErro:TMemo):string;
function Busca_Registro_Origem(FConsulta_Orig:TConsulta;vSql,chave:string;MemErro:TMemo):string;
Function Tira_Aspas(texto : String) : String;
Function RemoveAcentos(Str:String): String;
function TrocaCaracterEspecial(aTexto : string; aLimExt : boolean) : string;
function TrocaCaracter(pValue:Currency) : string;


implementation

const
coId = 0;
coNome = 1;
coValor = 2;
coSql = 3;

function TrocaCaracter(pValue:Currency) : string;
begin
  Result := StringReplace(CurrToStr(pValue), ',', '.', []);
end;


function TrocaCaracterEspecial(aTexto : string; aLimExt : boolean) : string;
const
  //Lista de caracteres especiais
  xCarEsp: array[1..38] of String = ('�', '�', '�', '�', '�','�', '�', '�', '�', '�',
                                     '�', '�','�', '�','�', '�','�', '�',
                                     '�', '�', '�','�', '�','�', '�', '�', '�', '�',
                                     '�', '�', '�','�','�', '�','�','�','�','�');
  //Lista de caracteres para troca
  xCarTro: array[1..38] of String = ('a', 'a', 'a', 'a', 'a','A', 'A', 'A', 'A', 'A',
                                     'e', 'e','E', 'E','i', 'i','I', 'I',
                                     'o', 'o', 'o','o', 'o','O', 'O', 'O', 'O', 'O',
                                     'u', 'u', 'u','u','u', 'u','c','C','n', 'N');
  //Lista de Caracteres Extras
  xCarExt: array[1..48] of string = ('<','>','!','@','#','$','%','�','&','*',
                                     '(',')','_','+','=','{','}','[',']','?',
                                     ';',':',',','|','*','"','~','^','�','`',
                                     '�','�','�','�','�','�','�','�','�','�',
                                     '�','�','�','�','�','�','�','�');
var
  xTexto : string;
  i : Integer;
begin
   xTexto := aTexto;
   for i:=1 to 38 do
     xTexto := StringReplace(xTexto, xCarEsp[i], xCarTro[i], [rfreplaceall]);
   //De acordo com o par�metro aLimExt, elimina caracteres extras.
   if (aLimExt) then
     for i:=1 to 48 do
       xTexto := StringReplace(xTexto, xCarExt[i], '', [rfreplaceall]);
   Result := xTexto;
end;

Function RemoveAcentos(Str:String): String;
Const ComAcento = '����������������������������';
  SemAcento = 'aaeouaoaeioucuAAEOUAOAEIOUCU';
Var
x : Integer;
Begin
  For x := 1 to Length(Str) do
  Begin
  if Pos(Str[x],ComAcento)<>0 Then
  begin
  Str[x] := SemAcento[Pos(Str[x],ComAcento)];
  end;
  end;
  Result := Str;
end;

Function Tira_Aspas(texto : String) : String;
Begin
  texto := TrocaCaracterEspecial(texto,false);

  While pos('''', Texto) <> 0 Do
    delete(Texto,pos('''', Texto),1);

  While pos('''', Texto) <> 0 Do
    delete(Texto,pos('''', Texto),1);

  While pos('''', Texto) <> 0 Do
    delete(Texto,pos('''', Texto),1);

  While pos('/', Texto) <> 0 Do
    delete(Texto,pos('/', Texto),1);

  While pos('\', Texto) <> 0 Do
    delete(Texto,pos('\', Texto),1);

  While pos('?', Texto) <> 0 Do
    delete(Texto,pos('?', Texto),1);


  if Texto = 'Y' then
    Texto := '';

  if Texto = 'U' then
    Texto := '';

  if Texto = 'TATIANAAGROJET@IG.COM.BR' then
    Texto := '';

  if Texto = 'TATIANA 0** 22 81259738 JANIO 9720' then
    Texto := '';

  if Texto = 'T' then
    Texto := '';

  if Texto = 'S' then
    Texto := '';

  if Texto = 'SA' then
    Texto := '';

  if Texto = 'SE' then
    Texto := '';

  if Texto = 'R' then
    Texto := '';

  if Texto = 'P' then
    Texto := '';

  if Texto = 'OO' then
    Texto := '';

  if Texto = 'O' then
    Texto := '';

  if Texto = 'N' then
    Texto := '';

  if Texto = 'L' then
    Texto := '';

  if Texto = '..' then
    Texto := '';

  if Texto = '.' then
    Texto := '';

  if Texto = ',' then
    Texto := '';

  if Texto = '0' then
    Texto := '';

  if Texto = '1' then
    Texto := '';

  if Texto = '0800 282 5692  81 2103 5675' then
    Texto := '';

  if Texto = '207' then
    Texto := '';

  if Texto = '22 25229352 FAX' then
    Texto := '';

  if Texto = '249' then
    Texto := '';

  if Texto = '26435490' then
    Texto := '';

  if Texto = '31 3359 0235 FAXCOMPROV. ALESSANDRO' then
    Texto := '';

  if Texto = '92786520' then
    Texto := '';

  if Texto = '31 3359 0235 FAX COMPROV. ALESSANDRO' then
    Texto := '';

  if Texto = '4299167812' then
    Texto := '';

  if Texto = '19 3888 4400 SETOR COMERCIAL' then
    Texto := '';

  if Texto = 'A' then
    Texto := '';
  Result := Texto;
End;

function Existe_Registro(FConsulta_dest:TConsulta;vSql,chave:string;MemErro:TMemo):Boolean;
begin
  Result := False;
  FConsulta_dest.Limpar;
  FConsulta_dest.Add(vSql);
  MemErro.Lines.Clear;
  MemErro.Lines.Add(vSql);
  FConsulta_dest.Pesquisar;

  if FConsulta_dest.GetString(chave) <> '' then
    Result := True;
end;

function Busca_Registro(FConsulta_dest:TConsulta;vSql,chave:string;MemErro:TMemo):string;
begin
  Result := '';
  FConsulta_dest.Limpar;
  FConsulta_dest.Add(vSql);
  MemErro.Lines.Clear;
  MemErro.Lines.Add(vSql);
  FConsulta_dest.Pesquisar;

  Result := FConsulta_dest.GetString(chave);
end;

function Busca_Registro_Origem(FConsulta_Orig:TConsulta;vSql,chave:string;MemErro:TMemo):string;
begin
  Result := '';
  FConsulta_orig.Limpar;
  FConsulta_orig.Add(vSql);
  MemErro.Lines.Clear;
  MemErro.Lines.Add(vSql);
  FConsulta_orig.Pesquisar();

  Result := FConsulta_Orig.FieldByName(chave).AsString;
end;

function Num_Registros(pMenos:Integer;FConsulta_orig:TConsulta;pNome_Tabela:string;MemErro:Tmemo):string;
var
  vSql:string;
begin
  Result := '';
  FConsulta_orig.Limpar;
  vSql :='select count(*) as quantreg ' +
         'from '+ pNome_Tabela;
  FConsulta_orig.Add(vSql);
  MemErro.Lines.Clear;
  MemErro.Lines.Add(vSql);
  FConsulta_orig.Pesquisar();
  pMenos := FConsulta_orig.FieldByName('quantreg').AsInteger - pMenos;
  Result := IntToStr(pMenos);
end;

function Max_Consulta(FConsulta_dest:TConsulta;nCampo,Nome_Tabela:string):string;
begin
  Result := '';
  FConsulta_dest.Limpar;
  FConsulta_dest.Add('select max('+nCampo +') as max from ' + Nome_Tabela);
  FConsulta_dest.Pesquisar;
  Result := inttostr(FConsulta_dest.GetInt('max') + 1);
end;

function Ajusta_Cnpj(pCnpj:string):string;
var
  vText : PChar;
  vCnpj : string;
begin
  vText := PChar(pCnpj);
  Result := '';

  while (vText^ <> #0) do
  begin
    {$IFDEF UNICODE}
    if CharInSet(vText^, ['0'..'9']) then
    {$ELSE}
    if vText^ in ['0'..'9'] then
    {$ENDIF}
      Result := Result + vText^;

    Inc(vText);
  end;

  vCnpj := Result;
  if Length(vCnpj) = 11 then
    vCnpj := Copy(vCnpj,1,3) + '.' + Copy(vCnpj,4,3) + '.' + Copy(vCnpj,7,3) + '-' + Copy(vCnpj,10,2)
  else
    vCnpj := Copy(vCnpj,1,2) + '.' + Copy(vCnpj,3,3) + '.' + Copy(vCnpj,6,3) + '/' + Copy(vCnpj,9,3) + '-' + Copy(vCnpj,13,2);

  Result := vCnpj;
end;

function NParametros(np:integer):string;
var
  i:Integer;
  aux:string;
begin
  Result := '';
  for i := 1 to np do
  begin
    if i>1 then
      aux := aux + ',';
    aux := aux + ':P'+inttostr(i);
  end;
  Result := aux +')';
end;

function Recria_Sequencia(aux_seq,Nome_Tabela,chave:String;FConsulta_dest:TConsulta;Conexao_dest:TConexao;lblHis:TLabel;MemErro:Tmemo):Boolean;
var
  vSql,vSeq:string;
begin
  if aux_seq = '' then begin
    Result := True;
    exit;
  end;

  try
    vSql := 'select sequence_name from user_sequences where sequence_name = ' + ''''+ aux_seq + '''';
    FConsulta_dest.Limpar;
    FConsulta_dest.Add(vSql);
    if FConsulta_dest.Pesquisar then
    begin
      vSql := 'drop sequence ' + aux_seq;
      lblHis.Caption := 'Limpando sequence '+Nome_Tabela+'.';
      roda_sql(True,Conexao_dest,vSql,MemErro);
      lblHis.Caption := 'Limpando sequence '+Nome_Tabela+'.  *** OK ***';
    end;

    vSeq := Max_Consulta(FConsulta_dest,chave,Nome_Tabela);

    vSql := 'create sequence ' +  aux_seq +
            ' start with '+ vSeq + ' increment by 1 ' +
            'minvalue 1  '+
            'maxvalue 9999999999 ' +
            'nocycle '+
            'nocache '+
            'noorder';
    lblHis.Caption := 'Recriando sequence '+Nome_Tabela+'.';
    roda_sql(True,Conexao_dest,vSql,MemErro);
    lblHis.Caption := 'sequence '+Nome_Tabela+'.  *** OK ***';
    Result := True;
  except
    Result := False;
  end;
end;


function getFoto(pConexao: Tconexao; pFuncionarioId: Integer): TMemoryStream;
var
  p: TConsulta;
  s: TStream;
begin
  Result := nil;

  p := TConsulta.Create(pConexao);
  p.SQL.Add('select FOTO from FOTOS_FUNCIONARIOS where FUNCIONARIO_ID = :P1');

  if p.Pesquisar([pFuncionarioId]) then begin
    if not(p.Fields[0].Value = null) then begin
      Result := TMemoryStream.Create;
      s := p.CreateBlobStream(p.FieldByName('FOTO'), bmRead);
      Result.LoadFromStream(s);
      s.Free;
    end;
  end;

  p.Free;
end;

function Valida_Data(vConsulta:TConsulta):Variant;
begin
  if vConsulta.GetDouble('LIMITE_CREDITO_APROVADO') > 0 then
  begin
//    vLim := FConsulta_orig.GetString('LIMITE_CREDITO_APROVADO');
//    vFunc := '1';  // atacado
    Result := vConsulta.GetData('DATA_APROVACAO');
  end else
  begin
//    vLim := '0';
//    vFunc := '';
    Result := System.Variants.Null;
  end;
end;

function Troca_Caracter(vStr,p1,p2:string):string;
var
  i:Integer;
begin
  Result := '';
  for i := 1 to Length(vStr) do
  begin
    if Copy(vStr,i,1) = 'p1' then
      Result := Result + 'p2'
    else
      Result := Result + Copy(vStr,i,1);
  end;
end;

function Importa_Origens(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,ins_Sql,vCid,aux_seq,Nome_Tabela,vUf:string;
  FConsulta_orig,FConsulta_dest : TConsulta;
  i,j: Integer;
  vExec:TExecucao;
begin
  Result := False;

  if MessageDlg('Deseja importar Origens?',mtinformation,[mbyes,mbno],0) = 7 then
    abort;
  Nome_Tabela := 'CFOP';
  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(Conexao_orig);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    MemHist.Lines.Clear;
    MemHist.Lines.Add('Iniciando Importa��o de ' + Nome_Tabela);
    lblHis.Caption :='Importando ' + Nome_Tabela;
    MemHist.Lines.Add('');


    if MessageDlg('Deseja limpar os dados existente?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      MemHist.Lines.Add('Limpando '+Nome_Tabela+'.');
      vSql := 'DELETE FROM '+Nome_Tabela;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada em '+Nome_Tabela+'.');
    end;

    vSql :='select count(*) as quantreg ' +
           '  from cfop c,CFOP_DESCRICOES d ' +
           '  where c.cfop_id = d.cfop_id ' +
           '  and length(c.cfop_id) = 5 ';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;
    lblQuant.caption := FConsulta_orig.GetString('quantreg');

    vSql :='select c.cfop_id,c.nome, nvl(d.descricao,c.nome) descricao,c.ativo ' +
           '  from cfop c,CFOP_DESCRICOES d ' +
           '  where c.cfop_id = d.cfop_id ' +
           '  and length(c.cfop_id) = 5 ' +
           '  order by c.cfop_id ';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar();

    ins_Sql :='insert into '+Nome_Tabela+'( '+
              'cfop_id,                     '+
              'NOME,                        '+
              'descricao,                   '+
              'ATIVO) values ( '+
              NParametros(4);
    vExec.Add(ins_Sql);

    try

      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;

      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vSql :='select cfop_id from cfop where cfop_id = ''' + FConsulta_orig.GetString('cfop_id') + '''';

        if not Existe_Registro(FConsulta_dest,vSql,'cfop_id',MemErro) then
        begin

          vexec.Executar([
                          FConsulta_orig.GetString('cfop_id'),
                          FConsulta_orig.GetString('NOME'),
                          FConsulta_orig.GetString('descricao'),
                          FConsulta_orig.GetString('ATIVO')
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(vUf + ' - '+ vCid);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(vUf + ' - '+ vCid);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir '+Nome_Tabela+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' realizada com sucesso!';

    MemErro.Lines.Clear;
    Result := True;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_GrupoFornecedor(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,vId,maxId: Integer;
  vExec:TExecucao;
  Existe_Tabela,Existe_Relacionamento:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'grupos_fornecedores';
  vNome_Tabela_Orig := 'grupos_fornecedores';
  vChave_orig := 'GRUPO_FORNECEDORES_ID';
  vChave_dest := 'grupo_id';
  vNome_seq:= 'SEQ_GRUPOS_FORNECEDORES';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    maxId := strtoint(Max_Consulta(FConsulta_orig,vChave_orig,vNome_Tabela_orig));

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);

    vSql := 'select * from ' + vNome_Tabela_Orig;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'GRUPO_ID '+ // NUMBER(4,0),
               ',DESCRICAO ' +//VARCHAR2(30) NOT NULL,
               ') values ( '+
              NParametros(2);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 1;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes';
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));
        vId := FConsulta_orig.FieldByName(vChave_orig).AsInteger;
        if vId = 1 then
          vId := MaxId + 1;

        vSql_Validacao := '';//'select cidade_id from cidades where NOME = ''' + vNome + ''' and ESTADO_ID = ''' + vId + '''';


        vSql_Validacao :='select ' + vChave_dest +
                         ' from ' + vNome_Tabela_Dest +
                         ' where ' + vChave_dest + ' = '+ IntToStr(vId);
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        Existe_Relacionamento := True;

//        if i = 24520 then
//          lblHis.Caption :='Parada para correcao';

        if (not Existe_Tabela) and Existe_Relacionamento then
        begin
          j:= J +1;
          vexec.Executar([
                          FConsulta_orig.GetInt('GRUPO_FORNECEDORES_ID'),
                          FConsulta_orig.GetString('NOME')
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';

    MemErro.Lines.Clear;
    Result := True;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Portadores(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,ins_Sql,vTipo,Nome_Tabela,chave:string;
  FConsulta_orig,FConsulta_dest : TConsulta;
  i,Imp,Dupl: Integer;
  vExec,vExec_up:TExecucao;
begin
  Result := False;

  Nome_Tabela := 'Portadores';
  chave := 'PORTADOR_ID';

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(Conexao_orig);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);
  vExec_up  := TExecucao.Create(Conexao_dest);

  try
    if not vsp then
      if MessageDlg('Deseja Importar os Portadores?',mtinformation,[mbyes,mbno],0) = 7 then
        abort;

    vSql := 'DELETE FROM ' + Nome_Tabela;
    lblHis.Caption := 'Limpando ' +Nome_Tabela+ '.';
    roda_sql(True,Conexao_dest,vSql,MemErro);

    vSql :='select * ' +
           'from ' + Nome_Tabela + ' order by ' + chave;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    //FConsulta_orig.FetchOptions.RowSetSize:=1000000 ;

    ins_Sql :='insert into ' + Nome_Tabela + '( '+
              ',PORTADOR_ID'+// VARCHAR2(50),
              ',ATIVO'+// CHAR(1) DEFAULT 'S'  NOT NULL,
              ',TIPO'+// CHAR(1) NOT NULL,
              ',INCIDENCIA_FINANCEIRO'+// CHAR(1) NOT NULL,
              ',DESCRICAO'+// VARCHAR2(80) NOT NULL,

              ') values ( '+
                ':P1,'+
                ':P2,'+
                ':P3,'+
                ':P4,'+
                ':P5)';
    vExec.Add(ins_Sql);

    if FConsulta_orig.Pesquisar then
    begin
      Application.ProcessMessages;
      Imp := 0;
      Dupl := 0;
      try
        Conexao_dest.IniciarTransacao;
        lblQuant.Caption := 'Quant :' + IntToStr(FConsulta_orig.GetQuantidadeRegistros);

        for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
        begin
          Application.ProcessMessages;
          lblHis.Caption := 'Inserindo ' + FConsulta_orig.GetString('PORTADOR_ID');

          vTipo := '';
          if FConsulta_orig.GetString('FINANCEIRA') = 'S' then
            vTipo := 'FI';
          if FConsulta_orig.GetString('DUPLICATA') = 'S' then
            vTipo := 'CA';
          if FConsulta_orig.GetString('BOLETA') = 'S' then
            vTipo := 'BO';
          if FConsulta_orig.GetString('CHEQUES') = 'S' then
            vTipo := 'CH';

          vexec.Executar([
                        FConsulta_orig.GetString('PORTADOR_ID'),
                        'S'

                         ]);

          Imp := Imp + 1;
          lblImp.Caption := 'Imp :' + IntToStr(Imp);
          lblHis.Caption := FConsulta_orig.GetString('PORTADOR_ID') +' *** OK ***';

          FConsulta_orig.Next;
        end;
      except
        on e:Exception do begin
          Conexao_dest.VoltarTransacao;
          if not vsp then
          begin
            ShowMessage('Erro ao inserir '+Nome_Tabela+'.' +
            e.Message + ' ' + vexec.SQL.Text );
            abort;
          end;
        end;
      end;
      //grdDupl.SetLinhasGridPorTamanhoVetor(vlinha);
      Conexao_dest.FinalizarTransacao;
      lblImp.Caption := 'Importados:' + IntToStr(Imp);
      lblDupl.Caption := 'Duplicados:' + IntToStr(Dupl);
      lblHis.Caption := Nome_Tabela + ' inseridos com sucesso!';
    end;

    if Recria_Sequencia('SEQ_PORTADOR_ID',Nome_Tabela,chave,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' realizada com sucesso!';

    Result := True;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
    vExec_up.Free;
  end;
end;

function Importa_Cargos(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,vId,maxId: Integer;
  vExec:TExecucao;
begin
  Result := False;

  vNome_Tabela_Dest := 'CARGOS_FUNCIONARIOS';
  vNome_Tabela_Orig := 'CARGOS';
  vChave := 'Cargo_ID';
  vNome_seq:= 'SEQ_CARGO_ID';

  if not vsp then
    if MessageDlg('Deseja Importar os '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if MessageDlg('Deseja limpar os dados existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := ' ';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    maxId := strtoint(Max_Consulta(FConsulta_orig,vChave,vNome_Tabela_orig));

    vSql := ' (select distinct c.* from cargos c, funcionarios f '+
            'where c.cargo_id = f.cargo_id '+
            'order by c.cargo_id) ';

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vSql,MemErro);

    vSql := 'select distinct c.* from cargos c, funcionarios f '+
            'where c.cargo_id = f.cargo_id                     '+
            'order by c.cargo_id ';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar;


    vIns_Sql :='insert into '+ vNome_Tabela_Dest+
               '( '+
               ' CARGO_ID '+
               ',NOME '+
               ',ATIVO '+
               ') values ( '+
               NParametros(3);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 0;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));

//        vId := FConsulta_orig.FieldByName(vChave).AsInteger;
//        if vId = 1 then
//          vId := MaxId + 1;

        vSql_Validacao := 'select cargo_id from ' +vNome_Tabela_Dest+
                          ' where NOME = ''' + vNome + '''';

        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes';
          MemHist.Lines.Add(lblHis.Caption);
        end;

        if not Existe_Registro(FConsulta_dest,vSql_Validacao,vChave,MemErro) then
        begin
          j:= J +1;
          vexec.Executar([
                          j,
                          vNome,
                          'S'
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';

    MemErro.Lines.Clear;
    Result := True;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Profissionais_Cargos(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,vId,maxId: Integer;
  vExec:TExecucao;
begin
  Result := False;

  vNome_Tabela_Dest := 'PROFISSIONAIS_CARGOS';
  vNome_Tabela_Orig := 'CARGOS';
  vChave := 'Cargo_ID';
  vNome_seq:= 'SEQ_PROFISSIONAIS_CARGO_ID';

  if not vsp then
    if MessageDlg('Deseja Importar os '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if MessageDlg('Deseja limpar os dados existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := ' ';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    maxId := strtoint(Max_Consulta(FConsulta_orig,vChave,vNome_Tabela_orig));

    vSql := ' (select * from cargos '+
            'where cargo_id > 1 '+
            'order by cargo_id) ';

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vSql,MemErro);

    vSql := 'select * from cargos '+
            'where cargo_id >1    '+
            'order by cargo_id ';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar;


    vIns_Sql :='insert into '+ vNome_Tabela_Dest+
               '( '+
               ' CARGO_ID '+
               ',NOME '+
               ',ATIVO '+
               ') values ( '+
               NParametros(3);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 0;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));

//        vId := FConsulta_orig.FieldByName(vChave).AsInteger;
//        if vId = 1 then
//          vId := MaxId + 1;

        vSql_Validacao := 'select cargo_id from ' +vNome_Tabela_Dest+
                          ' where NOME = ''' + vNome + '''';

        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes';
          MemHist.Lines.Add(lblHis.Caption);
        end;

        if not Existe_Registro(FConsulta_dest,vSql_Validacao,vChave,MemErro) and (vNome <> '') then
        begin
          j:= J +1;
          vexec.Executar([
                          j,
                          vNome,
                          'S'
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';

    MemErro.Lines.Clear;
    Result := True;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Empresas(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
//var
//  vSql,Nome_Tabela,up_Sql:string;
//  FConsulta_dest : TConsulta;
//  FConsulta_orig : TConsulta;
//  i,Imp,Dupl: Integer;
//  vExec,vExec_up:TExecucao;
begin
//  Application.ProcessMessages;
//  FConsulta_orig := TConsulta.Create(nil);
//  FConsulta_dest := TConsulta.Create(Conexao_dest);
//  vExec     := TExecucao.Create(Conexao_dest);
//  vExec_up  := TExecucao.Create(Conexao_dest);
//
//  try
//    FConsulta_orig.Connection := Conexao_orig;
//
//    Nome_Tabela := 'empresas';
//
//    vSql :='select lj.*,cep.Logradouro,cep.NumeroIni,cep.bairroId '+
//           ' from dbo.loja lj, dbo.cep cep ' +
//           ' where lj.cepid = cep.cepID order by lojaID';
//    FConsulta_orig.Limpar;
//    FConsulta_orig.Add(vSql);
//    FConsulta_orig.FetchOptions.RowSetSize:=1000000 ;
//    FConsulta_orig.Pesquisar();
//
////    ins_Sql :='insert into ' + Nome_Tabela + '( '+
////              'EMPRESA_ID '+//NUMBER(3,0),
////              ',RAZAO_SOCIAL'+// VARCHAR2(60) NOT NULL,
////              ',NOME_FANTASIA'+// VARCHAR2(60) NOT NULL,
////              ',CNPJ'+// VARCHAR2(19) NOT NULL,
////              ',LOGRADOURO'+// VARCHAR2(100) NOT NULL,
////              ',COMPLEMENTO'+// VARCHAR2(100) NOT NULL,
////              ',NUMERO'+// VARCHAR2(10) DEFAULT 'SN'  NOT NULL,
////              ',BAIRRO_ID'+// NUMBER(15,0) NOT NULL,
////              ',PONTO_REFERENCIA'+// VARCHAR2(100),
////              ',CEP'+// VARCHAR2(9) NOT NULL,
////
////              ',TELEFONE_PRINCIPAL'+// VARCHAR2(15),
////              ',TELEFONE_FAX'+// VARCHAR2(15),
////              ',E_MAIL'+// VARCHAR2(30),
////              ',DATA_FUNDACAO'+// DATE,
////              ',INSCRICAO_ESTADUAL'+// VARCHAR2(20) NOT NULL,
////              ',INSCRICAO_MUNICIPAL'+// VARCHAR2(20),
////              ',CADASTRO_ID'+// NUMBER(10,0),
////              ',CNAE'+// VARCHAR2(12),
////              ',ATIVO'+// CHAR(1) DEFAULT 'S'  NOT NULL,
////              ',TIPO_EMPRESA'+// CHAR(1) DEFAULT 'M'  NOT NULL,
////
////              ',NOME_RESUMIDO_IMPRESSOES_NF'+// VARCHAR2(30),
////              ',E_MAIL_NFE'+// VARCHAR2(80),
////              ',CPF_ADMINISTRADOR'+// VARCHAR2(14),
////              ',ENDERECO_SITE'+// VARCHAR2(50),
////              ',NOME_ADMINISTRADOR'+// VARCHAR2(50),
////
////              ') values ( '+
////              NParametros(15);
////
////    vExec.Add(ins_Sql);
//
//    if FConsulta_orig.GetQuantidadeRegistros > 0 then
//    begin
//      Application.ProcessMessages;
//      Imp := 0;
//      Dupl := 0;
//      try
//        Conexao_dest.IniciarTransacao;
//        lblQuant.Caption := 'Quant :' + IntToStr(FConsulta_orig.GetQuantidadeRegistros);
//
//        for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
//        begin
//          Application.ProcessMessages;
//
//          if i = 0 then
//          begin
//            up_Sql :=
//            ' update empresas set ' +
//            'RAZAO_SOCIAL = ''' + FConsulta_orig.FieldByName('ds_loja').AsString + '''' +
//            ',NOME_FANTASIA = ''' + FConsulta_orig.FieldByName('ds_loja').AsString + '''' +
//            ',CNPJ = ' + Ajusta_Cnpj(FConsulta_orig.FieldByName('cgc').AsString) +
//            ',LOGRADOURO = ''' + FConsulta_orig.FieldByName('LOGRADOURO').AsString + '''' +
//            ',COMPLEMENTO = ''' + FConsulta_orig.FieldByName('end_complemento').AsString + '''' +
//            ',NUMERO = ''' + FConsulta_orig.FieldByName('NumeroIni').AsString + '''' +
//            ',BAIRRO_ID = ''' + FConsulta_orig.FieldByName('bairroId').AsString + '''' +
//            //',PONTO_REFERENCIA = ''''' + //FConsulta_orig.FieldByName('ds_loja').AsString + '''' +
//            ',CEP = ''' + FConsulta_orig.FieldByName('cep').AsString + '''' +
//
//            ',TELEFONE_PRINCIPAL = ''' + FConsulta_orig.FieldByName('telefone').AsString + '''' +
//            //',TELEFONE_FAX = ''''' + //FConsulta_orig.FieldByName('ds_loja').AsString + '''' +
//            //',E_MAIL = ''''' + //FConsulta_orig.FieldByName('ds_loja').AsString + '''' +
//            //',DATA_FUNDACAO = ''' + FConsulta_orig.FieldByName('ds_loja').AsString + '''' +
//            ',INSCRICAO_ESTADUAL = ''' + FConsulta_orig.FieldByName('i_est').AsString + '''' +
//            ',INSCRICAO_MUNICIPAL = ''' + FConsulta_orig.FieldByName('incricao_municipal').AsString + '''' +
//            //',CADASTRO_ID = ''' + FConsulta_orig.FieldByName('ds_loja').AsString + '''' +
//            //',CNAE = ''' + FConsulta_orig.FieldByName('ds_loja').AsString + '''' +
//            //',ATIVO = ''' + FConsulta_orig.FieldByName('ds_loja').AsString + '''' +
//            //',TIPO_EMPRESA = ''' + FConsulta_orig.FieldByName('ds_loja').AsString + '''' +
//            ',NOME_RESUMIDO_IMPRESSOES_NF = ''' + Copy(FConsulta_orig.FieldByName('ds_loja').AsString,1,30) + '''' +
//            ' where EMPRESA_ID = 1 ';
//            vExec.Add(up_Sql);
//            vexec.Executar;
//            vexec.CommitUpdates;
//          end;
//          lblHis.Caption := 'Inserindo ' + FConsulta_orig.FieldByName('lojaID').AsString;
//
//          Imp := Imp + 1;
//          lblImp.Caption := 'Imp :' + IntToStr(Imp);
//          FConsulta_orig.Next;
//        end;
//      except
//        on e:Exception do begin
//          Conexao_dest.VoltarTransacao;
//          ShowMessage('Erro ao inserir '+Nome_Tabela+'.' +
//          e.Message + ' ' + vexec.SQL.Text );
//          abort;
//        end;
//      end;
//      //grdDupl.SetLinhasGridPorTamanhoVetor(vlinha);
//      Conexao_dest.FinalizarTransacao;
//      lblImp.Caption := 'Importados:' + IntToStr(Imp);
//      lblDupl.Caption := 'Duplicados:' + IntToStr(Dupl);
//      lblHis.Caption := Nome_Tabela + ' inseridos com sucesso!';
//    end;
//
//    lblHis.Caption := 'Importacao de '+Nome_Tabela+' realizada com sucesso!';
//    Result := True;
//  finally
//    FConsulta_orig.Free;
//    FConsulta_dest.Free;
//    vExec.Free;
//    vExec_up.Free;
//  end;
end;

function Importa_Motoristas(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,ins_Sql,vCnh,Nome_Tabela:string;
  FConsulta_orig,FConsulta_dest : TConsulta;
  i,Imp,Dupl: Integer;
  vExec,vExec_up:TExecucao;
  vData:Variant;
begin
  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(Conexao_orig);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);
  vExec_up  := TExecucao.Create(Conexao_dest);

  Nome_Tabela := 'motoristas';

  try
    vSql := 'DELETE FROM ' + Nome_Tabela;
    lblHis.Caption := 'Limpando ' +Nome_Tabela+ '.';
    roda_sql(True,Conexao_dest,vSql,MemErro);

    vSql :='select * ' +
           'from ' + Nome_Tabela + ' where ativo = ''S'' order by CADASTRO_id';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    //FConsulta_orig.FetchOptions.RowSetSize:=1000000 ;

    ins_Sql :='insert into ' + Nome_Tabela + '( '+
              'CADASTRO_ID'+// NUMBER(10,0),
              ',NUMERO_CNH'+// VARCHAR2(15) NOT NULL,
              ',DATA_VALIDADE_CNH'+// DATE NOT NULL,
              ',DATA_HORA_CADASTRO'+// DATE NOT NULL,
              ',ATIVO'+// CHAR(1) DEFAULT 'S'  NOT NULL,

              ') values ( '+
                ':P1,'+
                ':P2,'+
                ':P3,'+
                ':P4,'+
                ':P5)';
    vExec.Add(ins_Sql);

    if FConsulta_orig.Pesquisar then
    begin
      Application.ProcessMessages;
      Imp := 0;
      Dupl := 0;
      try
        Conexao_dest.IniciarTransacao;
        lblQuant.Caption := 'Quant :' + IntToStr(FConsulta_orig.GetQuantidadeRegistros);

        for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
        begin
          Application.ProcessMessages;
          lblHis.Caption := 'Inserindo ' + FConsulta_orig.GetString('CADASTRO_ID');

          vCnh := FConsulta_orig.GetString('CNH');
          if vCnh = '' then
            vCnh := '123';

          if FConsulta_orig.GetString('DATA_VALIDADE_CNH') = '' then
            vData := 'trunc(sysdate)'
          else
            vData := FConsulta_orig.GetData('DATA_VALIDADE_CNH');

          vexec.Executar([
                        FConsulta_orig.GetString('CADASTRO_ID'),
                        vCnh,
                        vData,
                        FConsulta_orig.GetData('DATA_HORA_ALTERACAO'),
                        FConsulta_orig.GetString('ATIVO')
                         ]);

          Imp := Imp + 1;
          lblImp.Caption := 'Imp :' + IntToStr(Imp);
          lblHis.Caption := FConsulta_orig.GetString('CADASTRO_ID') +' *** OK ***';

          FConsulta_orig.Next;
        end;
      except
        on e:Exception do begin
          Conexao_dest.VoltarTransacao;
          if not vsp then
          begin
            ShowMessage('Erro ao inserir '+Nome_Tabela+'.' +
            e.Message + ' ' + vexec.SQL.Text );
            abort;
          end;
        end;
      end;
      //grdDupl.SetLinhasGridPorTamanhoVetor(vlinha);
      Conexao_dest.FinalizarTransacao;
      lblImp.Caption := 'Importados:' + IntToStr(Imp);
      lblDupl.Caption := 'Duplicados:' + IntToStr(Dupl);
      lblHis.Caption := Nome_Tabela + ' inseridos com sucesso!';
    end;

    lblHis.Caption := 'Importacao de '+Nome_Tabela+' realizada com sucesso!';
    Result := True;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
    vExec_up.Free;
  end;
end;

function Importa_Veiculos(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,ins_Sql,Nome_Tabela:string;
  FConsulta_orig,FConsulta_dest : TConsulta;
  i,Imp,Dupl: Integer;
  vExec,vExec_up:TExecucao;
begin
  Application.ProcessMessages;
  Nome_Tabela := 'veiculos';
  FConsulta_orig := TConsulta.Create(Conexao_orig);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);
  vExec_up  := TExecucao.Create(Conexao_dest);

  try
    vSql := 'DELETE FROM ' + Nome_Tabela;
    lblHis.Caption := 'Limpando ' +Nome_Tabela+ '.';
    roda_sql(True,Conexao_dest,vSql,MemErro);

    vSql :='select * ' +
           'from ' + Nome_Tabela + ' order by CADASTRO_id';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    //FConsulta_orig.FetchOptions.RowSetSize:=1000000 ;

    ins_Sql :='insert into ' + Nome_Tabela + '( '+
              'VEICULO_ID ' + //NUMBER(5,0),
              ',MOTORISTA_ID ' + //NUMBER(10,0) NOT NULL,
              ',NOME ' + //VARCHAR2(40) NOT NULL,
              ',PLACA ' + //VARCHAR2(8) NOT NULL,
              ',ANO ' + //NUMBER(4,0),
              ',TIPO_COMBUSTIVEL ' + //CHAR(1) DEFAULT 'G'  NOT NULL,
              ',CHASSSI ' + //VARCHAR2(20),
              ',COR ' + //VARCHAR2(15),
              ',TIPO_VEICULO ' + //CHAR(3) DEFAULT 'CAR'  NOT NULL,
              ',PESO_MAXIMO ' + //NUMBER(5,0) DEFAULT 0 NOT NULL,

              ',TARA ' + //NUMBER(5,0) DEFAULT 0 NOT NULL,
              ',ATIVO ' + //CHAR(1) DEFAULT 'S'  NOT NULL,
              ',DESCRICAO ' + //VARCHAR2(500),
              ',CODIGO_ANTT ' + //VARCHAR2(20),
              ',MARCA ' + //VARCHAR2(30),

              ') values ( '+
                ':P1,'+
                ':P2,'+
                ':P3,'+
                ':P4,'+
                ':P5,'+
                ':P6,'+
                ':P7,'+
                ':P8,'+
                ':P9,'+
                ':P10,'+
                ':P11,'+
                ':P12,'+
                ':P13,'+
                ':P14,'+
                ':P15)';
    vExec.Add(ins_Sql);

    if FConsulta_orig.Pesquisar then
    begin
      Application.ProcessMessages;
      Imp := 0;
      Dupl := 0;
      try
        Conexao_dest.IniciarTransacao;
        lblQuant.Caption := 'Quant :' + IntToStr(FConsulta_orig.GetQuantidadeRegistros);

        for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
        begin
          Application.ProcessMessages;
          lblHis.Caption := 'Inserindo ' + FConsulta_orig.GetString('PLACA_VEICULO_ID');

          vexec.Executar([
                        i+1, //NUMBER(5,0),
                        FConsulta_orig.GetDouble('CADASTRO_ID'), //NUMBER(10,0) NOT NULL,
                        FConsulta_orig.GetString('NOME'), //VARCHAR2(40) NOT NULL,
                        FConsulta_orig.GetString('PLACA_VEICULO_ID'), //VARCHAR2(8) NOT NULL,
                        '1900', //NUMBER(4,0),
                        'D', //CHAR(1) DEFAULT 'G'  NOT NULL,
                        '', //VARCHAR2(20),
                        '', //VARCHAR2(15),
                        'CAM',  //CHAR(3) DEFAULT 'CAR'  NOT NULL,
                        FConsulta_orig.GetDouble('PESO_MAXIMO'), //NUMBER(5,0) DEFAULT 0 NOT NULL,
                        FConsulta_orig.GetDouble('TARA'), //NUMBER(5,0) DEFAULT 0 NOT NULL,
                        FConsulta_orig.GetString('ATIVO'), //DEFAULT 'S'  NOT NULL,
                        FConsulta_orig.GetString('DESCRICAO'),  //VARCHAR2(500),
                        FConsulta_orig.GetString('CODIGO_ANTT'), //VARCHAR2(20),
                        FConsulta_orig.GetString('MARCA')  //VARCHAR2(30),
                         ]);

          Imp := Imp + 1;
          lblImp.Caption := 'Imp :' + IntToStr(Imp);
          lblHis.Caption := FConsulta_orig.GetString('PLACA_VEICULO_ID') +' *** OK ***';

          FConsulta_orig.Next;
        end;
      except
        on e:Exception do begin
          Conexao_dest.VoltarTransacao;
          if not vsp then
          begin
            ShowMessage('Erro ao inserir '+Nome_Tabela+'.' +
            e.Message + ' ' + vexec.SQL.Text );
            abort;
          end;
        end;
      end;
      //grdDupl.SetLinhasGridPorTamanhoVetor(vlinha);
      Conexao_dest.FinalizarTransacao;
      lblImp.Caption := 'Importados:' + IntToStr(Imp);
      lblDupl.Caption := 'Duplicados:' + IntToStr(Dupl);
      lblHis.Caption := Nome_Tabela + ' inseridos com sucesso!';
    end;

    if Recria_Sequencia('SEQ_VEICULO_ID',Nome_Tabela,'VEICULO_ID',FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' realizada com sucesso!';

    Result := True;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
    vExec_up.Free;
  end;
end;

function Importa_Grupos_Produtos(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,ins_Sql,Nome_Tabela:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig:TConsulta;
  i,j,Imp: Integer;
  vExec:TExecucao;
  vCd_Familia,vDs_Familia,vCd_Grupo,vDs_Grupo:string;
  vCd_Familia_ant:string;
  vGRUPO_PRODUTOS_ID,vNOME,vGRUPO_PAI:string;
begin

  Nome_Tabela := 'PRODUTOS_DEPTOS_SECOES_LINHAS';
  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);
  lblDupl.Caption := '0';

  try
    FConsulta_orig.Connection := Conexao_orig;
    Application.ProcessMessages;
    vExec     := TExecucao.Create(Conexao_dest);

    vSql := 'DELETE FROM PRODUTOS_DEPTOS_SECOES_LINHAS where DEPTO_SECAO_LINHA_ID <> ''999'' and ' +
            'DEPTO_SECAO_LINHA_ID <> ''999.999'' and DEPTO_SECAO_LINHA_ID <> ''999.999.999''';
    roda_sql(True,Conexao_dest,vSql,MemErro);
    MemHist.Lines.Clear;
    lblHis.Caption := 'Iniciando Importa��o de ' + Nome_Tabela;
    MemHist.Lines.Add('Iniciando Importa��o de ' + Nome_Tabela);
    MemHist.Lines.Add('');
    MemHist.Lines.Add('Limpando '+Nome_Tabela+'.');

    vSql :=' gwnet.dbo.familia f, gwnet.dbo.Grupo g ' +
           'where g.familiaID = f.familiaID';

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vSql,MemErro);

    vSql :='select f.cd_familia,f.ds_familia,g.cd_grupo,g.ds_grupo ' +
           'from gwnet.dbo.familia f, gwnet.dbo.Grupo g ' +
           'where g.familiaID = f.familiaID order by cd_familia';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);

    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar();

    ins_Sql :='insert into '+Nome_Tabela+'( '+
              'DEPTO_SECAO_LINHA_ID '+
              ',NOME '+
              ',DEPTO_SECAO_LINHA_PAI_ID'+
              ',ATIVO '+
              ') values ( '+
              NParametros(4);
    vExec.Add(ins_Sql);

    Conexao_dest.IniciarTransacao;
    try
      MemHist.Lines.Add('');
      Imp := 0;
      vCd_Familia := '';
      vDs_Familia := '';
      vCd_Grupo   := '';
      vDs_Grupo   := '';
      vCd_Familia_ant := '';
      j:= 1000;
      for i := 0 to (2*FConsulta_orig.GetQuantidadeRegistros) -2 do
      begin
        Application.ProcessMessages;

        lblHis.Caption := 'Inserindo ' + UpperCase(FConsulta_orig.FieldByName('ds_grupo').AsString);

        vCd_Familia := FConsulta_orig.FieldByName('cd_familia').AsString;
        vDs_Familia := UpperCase(TrocaCaracterEspecial(FConsulta_orig.FieldByName('ds_familia').AsString,true));
        vCd_Grupo   := FConsulta_orig.FieldByName('cd_grupo').AsString;
        vDs_Grupo   := UpperCase(TrocaCaracterEspecial(FConsulta_orig.FieldByName('ds_grupo').AsString,True));

        if vCd_Familia <> vCd_Familia_ant then
        begin
          vGRUPO_PAI := '';
          vGRUPO_PRODUTOS_ID := FConsulta_orig.FieldByName('cd_familia').AsString;
          vNOME := UpperCase(FConsulta_orig.FieldByName('ds_familia').AsString);


          vexec.Executar([
                        vGRUPO_PRODUTOS_ID,
                        vNOME,
                        vGRUPO_PAI,
                        'S'
                         ]);

          MemHist.Lines.Add(vNOME);
        end else
        begin
          j := j -1;
          vGRUPO_PAI := FConsulta_orig.FieldByName('cd_familia').AsString;
          vGRUPO_PRODUTOS_ID := FConsulta_orig.FieldByName('cd_familia').AsString + '.' +
                                FConsulta_orig.FieldByName('cd_grupo').AsString;
          vNOME := UpperCase(TrocaCaracterEspecial(FConsulta_orig.FieldByName('ds_grupo').AsString,True));
          if imp = 800 then
            vNOME := 'FAIXA1';

          vexec.Executar([
                        vGRUPO_PRODUTOS_ID,
                        vNOME,
                        vGRUPO_PAI,
                        'S'
                         ]);

          vGRUPO_PAI := vGRUPO_PRODUTOS_ID;
          vGRUPO_PRODUTOS_ID := FConsulta_orig.FieldByName('cd_familia').AsString + '.' +
                                FConsulta_orig.FieldByName('cd_grupo').AsString + '.' +
                                inttostr(j);
          vNOME := UpperCase(TrocaCaracterEspecial(FConsulta_orig.FieldByName('ds_grupo').AsString,True));
          if imp = 800 then
            vNOME := 'FAIXA1';
          vexec.Executar([
                        vGRUPO_PRODUTOS_ID,
                        vNOME,
                        vGRUPO_PAI,
                        'S'
                         ]);

          Imp := Imp +1;
          lblImp.Caption := IntToStr(Imp);
          MemHist.Lines.Add(vNOME);

          FConsulta_orig.Next;
        end;
        vCd_Familia_ant := vCd_Familia;
        if Imp = StrToInt(lblQuant.caption)then
          Break ;
      end;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        if not vsp then
        begin
          ShowMessage('Erro ao inserir '+Nome_Tabela+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;
    Conexao_dest.FinalizarTransacao;

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' realizada com sucesso!';

    Result := True;
    MemErro.Lines.Clear;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Unidades(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,ins_Sql,vMarca,Nome_Tabela:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,Imp,Dupl: Integer;
  vExec:TExecucao;
begin
  Nome_Tabela := 'Unidades';
  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);
  lblDupl.Caption := '0';

  try
    FConsulta_orig.Connection := Conexao_orig;
    Application.ProcessMessages;

    vExec     := TExecucao.Create(Conexao_dest);

    vSql := 'DELETE FROM '+Nome_Tabela+' ';
    roda_sql(True,Conexao_dest,vSql,MemErro);
    MemHist.Lines.Clear;
    lblHis.Caption := 'Iniciando Importa��o de ' + Nome_Tabela;
    MemHist.Lines.Add('Iniciando Importa��o de ' + Nome_Tabela);
    MemHist.Lines.Add('');
    MemHist.Lines.Add('Limpando '+Nome_Tabela+'.');

    lblQuant.caption := Num_Registros(0,FConsulta_orig,'dbo.unidade',MemErro);

    vSql :='select c.* from dbo.unidade c order by unidadeid';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);

    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar();

    ins_Sql :='insert into '+Nome_Tabela+'( '+
              'UNIDADE_ID '+ //NUMBER(10,0),
              ',DESCRICAO '+ //
              ',ATIVO '+ //CHAR(1) DEFAULT 'S'  NOT NULL,

              ') values ( '+
              NParametros(3);
    vExec.Add(ins_Sql);

    Conexao_dest.IniciarTransacao;
    try
      MemHist.Lines.Add('');
      Dupl :=0;
      Imp := 0;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        lblHis.Caption := 'Inserindo ' + UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('ds_unidade').AsString));

        FConsulta_dest.Limpar;
        vSql :='select UNIDADE_ID '+
               'from '+Nome_tabela+' c '+
               'where c.DESCRICAO = '''+ UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('ds_unidade').AsString)) +'''';

        FConsulta_dest.Limpar;
        FConsulta_dest.Add(vSql);
        if not FConsulta_dest.Pesquisar then
        begin
          vMarca := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('ds_unidade').AsString));
          vexec.Executar([
                        UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('cd_unidade').AsString)),
                        vMarca,
                        'S'
                         ]);
          Imp := Imp +1;
          lblImp.Caption := IntToStr(Imp);
          MemHist.Lines.Add(Uppercase(FConsulta_orig.FieldByName('ds_unidade').AsString));
        end else
        begin
          Dupl := Dupl + 1 ;
          lblDupl.Caption := IntToStr(Dupl);
        end;
        FConsulta_orig.Next;
      end;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        if not vsp then
        begin
          ShowMessage('Erro ao inserir '+Nome_Tabela+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;
    Conexao_dest.FinalizarTransacao;

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' realizada com sucesso!';

    Result := True;

    MemErro.Lines.Clear;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_icms(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,ins_Sql,vMarca,Nome_Tabela,vPRODUTO_ID,vEMPRESA_ID,vUF,vcd_cst_lucro:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,Imp,Dupl: Integer;
  vExec:TExecucao;
begin
  Nome_Tabela := 'produtos_icms';
  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);
  lblDupl.Caption := '0';

  try
    FConsulta_orig.Connection := Conexao_orig;
    Application.ProcessMessages;

    vExec     := TExecucao.Create(Conexao_dest);

    vSql := 'DELETE FROM '+Nome_Tabela+' ';
    roda_sql(True,Conexao_dest,vSql,MemErro);
    MemHist.Lines.Clear;
    lblHis.Caption := 'Iniciando Importa��o de ' + Nome_Tabela;
    MemHist.Lines.Add('Iniciando Importa��o de ' + Nome_Tabela);
    MemHist.Lines.Add('');
    MemHist.Lines.Add('Limpando '+Nome_Tabela+'.');

    vSql :=' dbo.produto_cf pc, dbo.ClassificacaoFiscal f, dbo.Produto p, dbo.uf uf  ' +
           'where pc.cfID= f.cfID and p.produtoID = pc.produtoID ' +
           ' and pc.ufID = uf.ufID and cd_uf = ''GO'''  +
           ' and f.ContribuinteIcms = 0  ';

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vSql,MemErro);

    vSql :='select p.produtoID,f.cd_cst_lucro,f.al_icms,uf.cd_uf ' +
           'from dbo.produto_cf pc, dbo.ClassificacaoFiscal f, dbo.Produto p, dbo.uf uf  ' +
           'where pc.cfID= f.cfID and p.produtoID = pc.produtoID ' +
           ' and pc.ufID = uf.ufID and cd_uf = ''GO'''  +
           ' and f.ContribuinteIcms = 0  ';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);

    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar();

    ins_Sql :='insert into '+Nome_Tabela+'( '+
              'PRODUTO_ID ' +//NUMBER(10,0),
              ',EMPRESA_ID ' +//NUMBER(3,0),
              ',ESTADO_ID ' +//CHAR(2),
              ',CST_NAO_CONTRIBUINTE ' +//VARCHAR2(3) NOT NULL,
              ',CST_CONTRIBUINTE ' +//VARCHAR2(3) NOT NULL,
              ',CST_ORGAO_PUBLICO ' +//VARCHAR2(3) NOT NULL,
              ',CST_REVENDA ' +//VARCHAR2(3) NOT NULL,
              ',CST_CONSTRUTORA ' +//VARCHAR2(3) NOT NULL,
              ',CST_CLINICA_HOSPITAL ' +//VARCHAR2(3) NOT NULL,
              ',PERCENTUAL_ICMS ' +//NUMBER(4,2) NOT NULL,
              ',CST_PRODUTOR_RURAL ' +//VARCHAR2(3) NOT NULL,
              ') values ( '+
              NParametros(11);
    vExec.Add(ins_Sql);

    Conexao_dest.IniciarTransacao;
    try
      MemHist.Lines.Add('');
      Dupl :=0;
      Imp := 0;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vUF := 'GO';
        vcd_cst_lucro := FConsulta_orig.FieldByName('cd_cst_lucro').AsString;
        if Length(vcd_cst_lucro) > 2 then
          vcd_cst_lucro := Copy(vcd_cst_lucro,2,2);
        vSql := 'select produto_id from produtos where chave_importacao = ''' +
                 FConsulta_orig.FieldByName('produtoID').AsString + '''';
        vPRODUTO_ID := Busca_Registro(FConsulta_dest,vSql,'produto_id',MemErro);

        if vPRODUTO_ID <> '' then
        begin
          vSql :='select produto_id from produtos_icms where produto_id = ''' + vPRODUTO_ID + '''';

          if not Existe_Registro(FConsulta_dest,vSql,'produto_id',MemErro) then
          begin
            vEMPRESA_ID := '1';

            vexec.Executar([
                            vPRODUTO_ID,
                            vEMPRESA_ID,
                            vUF,
                            vcd_cst_lucro,
                            vcd_cst_lucro,
                            vcd_cst_lucro,
                            vcd_cst_lucro,
                            vcd_cst_lucro,
                            vcd_cst_lucro,
                            FConsulta_orig.FieldByName('al_icms').AsCurrency,
                            vcd_cst_lucro
                             ]);

            Imp := Imp +1;
            lblImp.Caption := IntToStr(Imp);
            MemHist.Lines.Add(vPRODUTO_ID);
          end else
          begin
            Dupl := Dupl + 1 ;
            lblDupl.Caption := IntToStr(Dupl);
          end;
        end;
        FConsulta_orig.Next;
      end;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        if not vsp then
        begin
          ShowMessage('Erro ao inserir '+Nome_Tabela+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;
    Conexao_dest.FinalizarTransacao;

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' realizada com sucesso!';

    Result := True;

    MemErro.Lines.Clear;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Precos(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,ins_Sql,vMarca,vCadId,Nome_Tabela,aux_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,Imp,Dupl: Integer;
  vExec:TExecucao;
  vPRODUTO_ID : string;
  vEMPRESA_ID : string;
  vPRECO_VAREJO :currency;
  vPERC_COMISSAO_A_VISTA :currency;
  vPERC_COMISSAO_A_PRAZO :currency;
begin
  Nome_Tabela := 'precos_produtos';
  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);
  lblDupl.Caption := '0';

  try
    FConsulta_orig.Connection := Conexao_orig;
    Application.ProcessMessages;

    vExec     := TExecucao.Create(Conexao_dest);

    vSql := 'DELETE FROM '+Nome_Tabela+' ';
    roda_sql(True,Conexao_dest,vSql,MemErro);
    MemHist.Lines.Clear;
    lblHis.Caption := 'Iniciando Importa��o de ' + Nome_Tabela;
    MemHist.Lines.Add('Iniciando Importa��o de ' + Nome_Tabela);
    MemHist.Lines.Add('');
    MemHist.Lines.Add('Limpando '+Nome_Tabela+'.');

    vSql :=' dbo.produto_preco pp, produto p '+
           ' ,produto_grade g ' +//, dbo.produtoEstoque e '+
           '  where p.produtoID = g.produtoID '+
           '  and g.produtoID = p.produtoID '+
           '  and pp.produto_gradeID = g.produto_gradeID '+
           '  and pp.PrecoTabelaId = 1 ';

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vSql,MemErro);

    vSql :='SELECT p.produtoID,p.ds_completa,pp.preco,pp.markup '+
           //,'e.CustoMedio ,e.CustoUltimaCompra, e.Qtd '+
           ' FROM dbo.produto_preco pp, produto p '+
           ' ,produto_grade g ' +//, dbo.produtoEstoque e '+
           '  where p.produtoID = g.produtoID '+
           '  and g.produtoID = p.produtoID '+
           '  and pp.produto_gradeID = g.produto_gradeID '+
          // '  and e.produto_gradeID = g.produto_gradeID '+
           '  and pp.PrecoTabelaId = 1 ' +
           '  order by p.ds_completa';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);

    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar();

    ins_Sql :='insert into '+Nome_Tabela+'( '+
              'PRODUTO_ID ' +//NUMBER(10,0),
              ',EMPRESA_ID ' +// NUMBER(3,0),
              ',PRECO_VAREJO ' +// NUMBER(9,4) DEFAULT 0 NOT NULL,
              ',PERC_COMISSAO_A_VISTA ' +// NUMBER(4,2) DEFAULT 0 NOT NULL,
              ',PERC_COMISSAO_A_PRAZO ' +// NUMBER(4,2) DEFAULT 0 NOT NULL

              ') values ( '+
              NParametros(5);
    vExec.Add(ins_Sql);

    Conexao_dest.IniciarTransacao;
    try
      MemHist.Lines.Add('');
      j:=1;
      Dupl :=0;
      Imp := 0;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        lblHis.Caption := 'Inserindo ' + UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('ds_completa').AsString));

        vSql := 'select produto_id from produtos where chave_importacao = ''' +
                 FConsulta_orig.FieldByName('produtoID').AsString + '''';
        vPRODUTO_ID := Busca_Registro(FConsulta_dest,vSql,'produto_id',MemErro);
        if vPRODUTO_ID <> '' then
        begin
          vEMPRESA_ID := '1';
          vPRECO_VAREJO := FConsulta_orig.FieldByName('preco').AsCurrency;
          vPERC_COMISSAO_A_VISTA := 0;
          vPERC_COMISSAO_A_PRAZO := 0;

          vexec.Executar([
                          vPRODUTO_ID,
                          vEMPRESA_ID,
                          vPRECO_VAREJO,
                          vPERC_COMISSAO_A_VISTA,
                          vPERC_COMISSAO_A_PRAZO
                           ]);

          Imp := Imp +1;
          lblImp.Caption := IntToStr(Imp);
          MemHist.Lines.Add(Uppercase(FConsulta_orig.FieldByName('ds_completa').AsString));
        end else
        begin
          Dupl := Dupl + 1 ;
          lblDupl.Caption := IntToStr(Dupl);
        end;
        FConsulta_orig.Next;
      end;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        if not vsp then
        begin
          ShowMessage('Erro ao inserir '+Nome_Tabela+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;
    Conexao_dest.FinalizarTransacao;

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' realizada com sucesso!';

    Result := True;
    MemErro.Lines.Clear;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Custos(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,ins_Sql,vMarca,vCadId,Nome_Tabela,aux_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,Imp,Dupl: Integer;
  vExec:TExecucao;
  vEMPRESA_ID : string;
  vPRODUTO_ID : string;
  vPRECO_TABELA :currency;
  vORIGEM_ID: variant;
  vCUSTO_PEDIDO_MEDIO :currency;
  vCUSTO_PEDIDO_MEDIO_ANTERIOR :currency;
  vCUSTO_ULTIMO_PEDIDO :currency;
  vPRECO_FINAL :currency;
  vPRECO_FINAL_MEDIO :currency;
  vPRECO_FINAL_ANTERIOR :currency;
  vPRECO_LIQUIDO :currency;
  vPRECO_LIQUIDO_MEDIO :currency;
  vPRECO_LIQUIDO_ANTERIOR :currency;
  vCMV :currency;
  vCMV_ANTERIOR :currency;
  vCUSTO_COMPRA_COMERCIAL :currency;
begin
  Nome_Tabela := 'custos_produtos';
  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);
  lblDupl.Caption := '0';

  try
    FConsulta_orig.Connection := Conexao_orig;
    Application.ProcessMessages;

    vExec     := TExecucao.Create(Conexao_dest);

    vSql := 'DELETE FROM '+Nome_Tabela+' ';
    roda_sql(True,Conexao_dest,vSql,MemErro);
    MemHist.Lines.Clear;
    lblHis.Caption := 'Iniciando Importa��o de ' + Nome_Tabela;
    MemHist.Lines.Add('Iniciando Importa��o de ' + Nome_Tabela);
    MemHist.Lines.Add('');
    MemHist.Lines.Add('Limpando '+Nome_Tabela+'.');

    vSql :=' dbo.produto_preco pp, produto p                 '+
           ',produto_grade g , dbo.produtoEstoque e              '+
           'where p.produtoID = g.produtoID                      '+
           'and g.produtoID = p.produtoID                        '+
           'and pp.produto_gradeID = g.produto_gradeID           '+
           'and e.produto_gradeID = g.produto_gradeID            '+
           'and pp.PrecoTabelaId = 1                             ';

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vSql,MemErro);

    vSql :='SELECT p.produtoID,p.ds_completa,pp.preco,pp.markup '+
           ',e.CustoMedio ,e.CustoUltimaCompra, e.Qtd            '+
           'FROM dbo.produto_preco pp, produto p                 '+
           ',produto_grade g , dbo.produtoEstoque e              '+
           'where p.produtoID = g.produtoID                      '+
           'and g.produtoID = p.produtoID                        '+
           'and pp.produto_gradeID = g.produto_gradeID           '+
           'and e.produto_gradeID = g.produto_gradeID            '+
           'and pp.PrecoTabelaId = 1                             '+
           'order by p.ds_completa';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);

    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar();

    ins_Sql :='insert into '+Nome_Tabela+'( '+
              'EMPRESA_ID '+//NUMBER(3,0),
              ',PRODUTO_ID'+// NUMBER(10,0),
              ',PRECO_TABELA'+// NUMBER(9,4) DEFAULT 0 NOT NULL,
              ',ORIGEM_ID'+// NUMBER(12,0),
              ',CUSTO_PEDIDO_MEDIO'+// NUMBER(9,4) DEFAULT 0 NOT NULL,
              ',CUSTO_PEDIDO_MEDIO_ANTERIOR'+// NUMBER(9,4) DEFAULT 0 NOT NULL,
              ',CUSTO_ULTIMO_PEDIDO'+// NUMBER(9,4) DEFAULT 0 NOT NULL,
              ',PRECO_FINAL'+// NUMBER(9,4) DEFAULT 0 NOT NULL,
              ',PRECO_FINAL_MEDIO'+// NUMBER(9,4) DEFAULT 0 NOT NULL,
              ',PRECO_FINAL_ANTERIOR'+// NUMBER(9,4) DEFAULT 0 NOT NULL,
              ',PRECO_LIQUIDO'+// NUMBER(9,4) DEFAULT 0 NOT NULL,
              ',PRECO_LIQUIDO_MEDIO'+// NUMBER(9,4) DEFAULT 0 NOT NULL,
              ',PRECO_LIQUIDO_ANTERIOR'+// NUMBER(9,4) DEFAULT 0 NOT NULL,
              ',CMV'+// NUMBER(9,4) DEFAULT 0 NOT NULL,
              ',CMV_ANTERIOR'+// NUMBER(9,4) DEFAULT 0 NOT NULL,
              ',CUSTO_COMPRA_COMERCIAL'+// NUMBER(9,4) DEFAULT 0 NOT NULL,

              ') values ( '+
              NParametros(16);
    vExec.Add(ins_Sql);

    Conexao_dest.IniciarTransacao;
    try
      MemHist.Lines.Add('');
      j:=1;
      Dupl :=0;
      Imp := 0;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        lblHis.Caption := 'Inserindo ' + UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('ds_completa').AsString));

        vSql := 'select produto_id from produtos where chave_importacao = ''' +
                 FConsulta_orig.FieldByName('produtoID').AsString + '''';
        vPRODUTO_ID := Busca_Registro(FConsulta_dest,vSql,'produto_id',MemErro);
        if vPRODUTO_ID <> '' then
        begin
          vEMPRESA_ID := '1';
          vPRECO_TABELA := FConsulta_orig.FieldByName('CustoUltimaCompra').Ascurrency;
          vORIGEM_ID:= null;
          vCUSTO_PEDIDO_MEDIO := FConsulta_orig.FieldByName('CustoMedio').Ascurrency;
          vCUSTO_PEDIDO_MEDIO_ANTERIOR := FConsulta_orig.FieldByName('CustoMedio').Ascurrency;
          vCUSTO_ULTIMO_PEDIDO := FConsulta_orig.FieldByName('CustoUltimaCompra').Ascurrency;
          vPRECO_FINAL := FConsulta_orig.FieldByName('CustoUltimaCompra').Ascurrency;
          vPRECO_FINAL_MEDIO := FConsulta_orig.FieldByName('CustoMedio').Ascurrency;
          vPRECO_FINAL_ANTERIOR := FConsulta_orig.FieldByName('CustoUltimaCompra').Ascurrency;
          vPRECO_LIQUIDO := FConsulta_orig.FieldByName('CustoUltimaCompra').Ascurrency;
          vPRECO_LIQUIDO_MEDIO := FConsulta_orig.FieldByName('CustoMedio').Ascurrency;
          vPRECO_LIQUIDO_ANTERIOR := FConsulta_orig.FieldByName('CustoUltimaCompra').Ascurrency;
          vCMV := FConsulta_orig.FieldByName('CustoUltimaCompra').Ascurrency;
          vCMV_ANTERIOR := FConsulta_orig.FieldByName('CustoUltimaCompra').Ascurrency;
          vCUSTO_COMPRA_COMERCIAL := FConsulta_orig.FieldByName('CustoUltimaCompra').Ascurrency;

          vexec.Executar([
                          vEMPRESA_ID
                          ,vPRODUTO_ID
                          ,vPRECO_TABELA
                          ,vORIGEM_ID
                          ,vCUSTO_PEDIDO_MEDIO
                          ,vCUSTO_PEDIDO_MEDIO_ANTERIOR
                          ,vCUSTO_ULTIMO_PEDIDO
                          ,vPRECO_FINAL
                          ,vPRECO_FINAL_MEDIO
                          ,vPRECO_FINAL_ANTERIOR
                          ,vPRECO_LIQUIDO
                          ,vPRECO_LIQUIDO_MEDIO
                          ,vPRECO_LIQUIDO_ANTERIOR
                          ,vCMV
                          ,vCMV_ANTERIOR
                          ,vCUSTO_COMPRA_COMERCIAL
                           ]);

          Imp := Imp +1;
          lblImp.Caption := IntToStr(Imp);
          MemHist.Lines.Add(Uppercase(FConsulta_orig.FieldByName('ds_completa').AsString));
        end else
        begin
          Dupl := Dupl + 1 ;
          lblDupl.Caption := IntToStr(Dupl);
        end;
        FConsulta_orig.Next;
      end;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        if not vsp then
        begin
          ShowMessage('Erro ao inserir '+Nome_Tabela+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;
    Conexao_dest.FinalizarTransacao;

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' realizada com sucesso!';

    Result := True;
    MemErro.Lines.Clear;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Marcas    (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,ins_Sql,vMarca,vCadId,Nome_Tabela,aux_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,Imp,Dupl: Integer;
  vExec:TExecucao;
begin
  Nome_Tabela := 'marcas';
  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);
  lblDupl.Caption := '0';

  try
    FConsulta_orig.Connection := Conexao_orig;
    Application.ProcessMessages;

    vExec     := TExecucao.Create(Conexao_dest);

    vSql := 'DELETE FROM '+Nome_Tabela+' ';
    roda_sql(True,Conexao_dest,vSql,MemErro);
    MemHist.Lines.Clear;
    lblHis.Caption := 'Iniciando Importa��o de ' + Nome_Tabela;
    MemHist.Lines.Add('Iniciando Importa��o de ' + Nome_Tabela);
    MemHist.Lines.Add('');
    MemHist.Lines.Add('Limpando '+Nome_Tabela+'.');

    lblQuant.caption := Num_Registros(0,FConsulta_orig,'dbo.marca',MemErro);

    vSql :='SELECT * FROM dbo.marca pp ' +
           '  order by cd_marca';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);

    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar();

    ins_Sql :='insert into '+Nome_Tabela+'( '+
              'marca_id ' +
              ',nome ' +
              ',ativo ' +
              ') values ( '+
              NParametros(3);
    vExec.Add(ins_Sql);

    Conexao_dest.IniciarTransacao;
    try
      MemHist.Lines.Add('');
      j:=1;
      Dupl :=0;
      Imp := 0;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        lblHis.Caption := 'Inserindo ' + UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('ds_marca').AsString));

        FConsulta_dest.Limpar;
        vSql :='select marca_id '+
               'from marcas c '+
               'where c.nome = '''+ UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('ds_marca').AsString)) +'''';

        FConsulta_dest.Limpar;
        FConsulta_dest.Add(vSql);
        if not FConsulta_dest.Pesquisar then
        begin
          vCadId := inttostr(j +1);

          vMarca := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('ds_marca').AsString));
          vexec.Executar([
                        vCadId,
                        vMarca,
                        'S'
                         ]);
          j := j +1;
          Imp := Imp +1;
          lblImp.Caption := IntToStr(Imp);
          MemHist.Lines.Add(Uppercase(FConsulta_orig.FieldByName('ds_marca').AsString));
        end else
        begin
          Dupl := Dupl + 1 ;
          lblDupl.Caption := IntToStr(Dupl);
        end;
        FConsulta_orig.Next;
      end;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        if not vsp then
        begin
          ShowMessage('Erro ao inserir '+Nome_Tabela+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;
    Conexao_dest.FinalizarTransacao;

    aux_seq := 'SEQ_MARCA_ID';
    if Recria_Sequencia(aux_seq,Nome_Tabela,'MARCA_ID',FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' realizada com sucesso!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' realizada com sucesso!';

    Result := True;
    MemErro.Lines.Clear;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_credito_Pagar(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,ins_Sql,vMarca,vCadId,Nome_Tabela,aux_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,Imp,Dupl: Integer;
  vExec:TExecucao;
  vRECEBER_ID : integer;
  vEMPRESA_ID : integer;
  vORCAMENTO_ID : variant;
  vCOBRANCA_ID : integer;
  vTURNO_ID : variant;
  vVENDEDOR_ID : variant;
  vNOTA_FISCAL_ID : variant;
  vUSUARIO_CADASTRO_ID : variant;
  vDOCUMENTO : String;
  vDATA_CADASTRO: variant;
  vDATA_EMISSAO:variant;
  vDATA_VENCIMENTO:variant;
  vDATA_VENCIMENTO_ORIGINAL:variant;
  vVALOR_DOCUMENTO: currency;
  vVALOR_Pago: currency;
  vSTATUS:String;
  vOBSERVACOES :string;
  vPARCELA: integer;
  vNUMERO_PARCELAS: integer;
  vPORTADOR_ID :String;
  vPLANO_FINANCEIRO_ID :String;
  vCENTRO_CUSTO_ID :integer;
  vORIGEM:String;
  vCADASTRO_ID:String;
begin
  Nome_Tabela := 'CONTAS_PAGAR';
  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);
  lblDupl.Caption := '0';
  Result := False;

  try
    FConsulta_orig.Connection := Conexao_orig;
    Application.ProcessMessages;

    vExec     := TExecucao.Create(Conexao_dest);

    vSql := 'DELETE FROM '+Nome_Tabela+' ';
    roda_sql(True,Conexao_dest,vSql,MemErro);
    MemHist.Lines.Clear;
    lblHis.Caption := 'Iniciando Importa��o de ' + Nome_Tabela;
    MemHist.Lines.Add('Iniciando Importa��o de ' + Nome_Tabela);
    MemHist.Lines.Add('');
    MemHist.Lines.Add('Limpando '+Nome_Tabela+'.');

    vSql :=' dbo.cntReceber r ' +
           ' where (isnull(r.Vl_titulo,0) - isnull(r.vl_desc,0) + isnull(r.vl_acre,0) - isnull(r.vl_pago,0)) < 0 ' +
           '  and r.dt_pago is null ';

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vSql,MemErro);

    vSql :='select (isnull(r.Vl_titulo,0) - isnull(r.vl_desc,0) + isnull(r.vl_acre,0)) vlr,r.* '+
           ' from dbo.cntReceber r ' +
           ' where (isnull(r.Vl_titulo,0) - isnull(r.vl_desc,0) + isnull(r.vl_acre,0) - isnull(r.vl_pago,0)) < 0 ' +
           '  and r.dt_pago is null ' +
           '  order by dt_emi';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);

    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar();

    ins_Sql :='insert into '+Nome_Tabela+'( '+
              'PAGAR_ID ' + //NUMBER(12,0),
              ',CADASTRO_ID ' + // NUMBER(10,0) NOT NULL,
              ',EMPRESA_ID ' + //NUMBER(3,0) NOT NULL,
              ',COBRANCA_ID ' + //NUMBER(3,0) NOT NULL,
              ',PORTADOR_ID ' + //VARCHAR2(50) NOT NULL,
              ',USUARIO_CADASTRO_ID ' + //NUMBER(4,0) NOT NULL,
              ',DOCUMENTO ' + //VARCHAR2(20) NOT NULL,
              ',PLANO_FINANCEIRO_ID ' + //VARCHAR2(9) NOT NULL,
              ',CENTRO_CUSTO_ID ' + //NUMBER(3,0) DEFAULT 1 NOT NULL,
              ',DATA_CADASTRO ' + //DATE DEFAULT trunc(sysdate)  NOT NULL,

              ',DATA_VENCIMENTO ' + //DATE NOT NULL,
              ',DATA_VENCIMENTO_ORIGINAL ' + //DATE,
              ',VALOR_DOCUMENTO ' + //NUMBER(8,2) NOT NULL,
              ',ORIGEM ' + //CHAR(3) NOT NULL,
              ',OBSERVACOES ' + //VARCHAR2(200),
              ') values ( '+
              NParametros(15);
    vExec.Add(ins_Sql);

    Conexao_dest.IniciarTransacao;
    try
      MemHist.Lines.Add('');
      j:=0;
      Dupl :=0;
      Imp := 0;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vDATA_CADASTRO   := FConsulta_orig.FieldByName('dt_emi').AsDateTime;
        vDATA_EMISSAO    := FConsulta_orig.FieldByName('dt_emi').AsDateTime;
        vDATA_VENCIMENTO := FConsulta_orig.FieldByName('dt_venc').AsDateTime;
        vDATA_VENCIMENTO_ORIGINAL := FConsulta_orig.FieldByName('dt_venc').AsDateTime;
        vVALOR_DOCUMENTO := FConsulta_orig.FieldByName('vl_titulo').Ascurrency;
        vVALOR_Pago := FConsulta_orig.FieldByName('vl_pago').Ascurrency;

        vVALOR_DOCUMENTO := FConsulta_orig.FieldByName('vlr').Ascurrency;

        vSql := 'select cadastro_id from cadastros where chave_importacao = ' +
                FConsulta_orig.FieldByName('clienteid').AsString;
        vCADASTRO_ID := Busca_Registro(FConsulta_dest,vSql,'cadastro_id',MemErro);

        if (vVALOR_DOCUMENTO < 0) and (vCADASTRO_ID <> '') then
        begin
          vCadId := inttostr(j +1);

          vexec.Executar([
                         vCadId
                        ,vCADASTRO_ID
                        ,1
                        ,39
                        ,'9999'
                        ,1
                        ,FConsulta_orig.FieldByName('titulo').AsString
                        ,'1.002.001'
                        ,1
                        ,vDATA_CADASTRO
                        ,vDATA_VENCIMENTO
                        ,vDATA_VENCIMENTO_ORIGINAL
                        ,-1*(vVALOR_DOCUMENTO)
                        ,'MAN'
                        ,'IMPORTACAO'
                         ]);

          j := j +1;
          Imp := Imp +1;
          lblImp.Caption := IntToStr(Imp);
          MemHist.Lines.Add(Uppercase(FConsulta_orig.FieldByName('titulo').AsString));
        end else
        begin
          Dupl := Dupl + 1 ;
        end;
        lblDupl.Caption := IntToStr(Dupl);
        lblImp.Caption := IntToStr(Imp);
        FConsulta_orig.Next;
      end;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        if not vsp then
        begin
          ShowMessage('Erro ao inserir '+Nome_Tabela+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;
    Conexao_dest.FinalizarTransacao;

    aux_seq := 'SEQ_PAGAR_ID';
    if Recria_Sequencia(aux_seq,Nome_Tabela,'PAGAR_ID',FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' realizada com sucesso!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' realizada com sucesso!';

    Result := True;
    MemErro.Lines.Clear;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Contas_Receber(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,ins_Sql,vMarca,vCadId,Nome_Tabela,aux_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,Imp,Dupl: Integer;
  vExec:TExecucao;
  vRECEBER_ID : integer;
  vEMPRESA_ID : integer;
  vORCAMENTO_ID : variant;
  vCOBRANCA_ID : integer;
  vTURNO_ID : variant;
  vVENDEDOR_ID : variant;
  vNOTA_FISCAL_ID : variant;
  vUSUARIO_CADASTRO_ID : variant;
  vDOCUMENTO : String;
  vDATA_CADASTRO: variant;
  vDATA_EMISSAO:variant;
  vDATA_VENCIMENTO:variant;
  vDATA_VENCIMENTO_ORIGINAL:variant;
  vVALOR_DOCUMENTO: currency;
  vVALOR_Pago: currency;
  vSTATUS:String;
  vOBSERVACOES :string;
  vPARCELA: integer;
  vNUMERO_PARCELAS: integer;
  vPORTADOR_ID :String;
  vPLANO_FINANCEIRO_ID :String;
  vCENTRO_CUSTO_ID :integer;
  vORIGEM:String;
  vCADASTRO_ID:String;
begin
  Nome_Tabela := 'CONTAS_RECEBER';
  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);
  lblDupl.Caption := '0';
  Result := False;

  try
    FConsulta_orig.Connection := Conexao_orig;
    Application.ProcessMessages;

    vExec     := TExecucao.Create(Conexao_dest);

    vSql := 'DELETE FROM '+Nome_Tabela+' ';
    roda_sql(True,Conexao_dest,vSql,MemErro);
    MemHist.Lines.Clear;
    lblHis.Caption := 'Iniciando Importa��o de ' + Nome_Tabela;
    MemHist.Lines.Add('Iniciando Importa��o de ' + Nome_Tabela);
    MemHist.Lines.Add('');
    MemHist.Lines.Add('Limpando '+Nome_Tabela+'.');

    vSql :=' dbo.cntReceber r ' +
           'where (isnull(r.Vl_titulo,0) - isnull(r.vl_desc,0) + isnull(r.vl_acre,0) - isnull(r.vl_pago,0)) > 0 '+
		       ' and r.dt_pago is null';

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vSql,MemErro);

    vSql :='SELECT (isnull(r.Vl_titulo,0) - isnull(r.vl_desc,0) + isnull(r.vl_acre,0)) vlr,r.* ' +
           'from dbo.cntReceber r ' +
           'where (isnull(r.Vl_titulo,0) - isnull(r.vl_desc,0) + isnull(r.vl_acre,0) - isnull(r.vl_pago,0)) > 0 '+
		       ' and r.dt_pago is null' +
           '  order by r.dt_emi';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);

    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar();

    ins_Sql :='insert into '+Nome_Tabela+'( '+
              'RECEBER_ID'+// NUMBER(12,0),
              ',EMPRESA_ID'+// NUMBER(3,0) NOT NULL,
              //',ORCAMENTO_ID'+// NUMBER(10,0),
              ',COBRANCA_ID'+// NUMBER(3,0) NOT NULL,
              //',TURNO_ID'+// NUMBER(10,0),
              //',VENDEDOR_ID'+// NUMBER(4,0),
              //',NOTA_FISCAL_ID'+// NUMBER(12,0),
              ',USUARIO_CADASTRO_ID'+// NUMBER(4,0) NOT NULL,
              ',DOCUMENTO'+// VARCHAR2(20) NOT NULL,
              ',DATA_CADASTRO'+// DATE DEFAULT trunc(sysdate)  NOT NULL,

              ',DATA_EMISSAO'+// DATE,
              ',DATA_VENCIMENTO'+// DATE NOT NULL,
              ',DATA_VENCIMENTO_ORIGINAL'+// DATE,
              ',VALOR_DOCUMENTO'+// NUMBER(8,2) NOT NULL,
              //',BAIXA_ORIGEM_ID'+// NUMBER(10,0),
              //',ITEM_ID_CRT_ORCAMENTO'+// NUMBER(3,0),
              ',OBSERVACOES'+// VARCHAR2(200),
              ',PORTADOR_ID'+// VARCHAR2(50) NOT NULL,
              ',PLANO_FINANCEIRO_ID'+// VARCHAR2(9) NOT NULL,
              ',ORIGEM'+// CHAR(3) NOT NULL,

              ',CADASTRO_ID'+// NUMBER(10,0) NOT NULL,
              //',BAIXA_ID'+// NUMBER(10,0),
              //',BAIXA_PAGAR_ORIGEM_ID'+// NUMBER(10,0),
              ') values ( '+
              NParametros(15);
    vExec.Add(ins_Sql);

    Conexao_dest.IniciarTransacao;
    try
      MemHist.Lines.Add('');
      j:=0;
      Dupl :=0;
      Imp := 0;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vDATA_CADASTRO   := FConsulta_orig.FieldByName('dt_emi').AsDateTime;
        vDATA_EMISSAO    := FConsulta_orig.FieldByName('dt_emi').AsDateTime;
        vDATA_VENCIMENTO := FConsulta_orig.FieldByName('dt_venc').AsDateTime;
        vDATA_VENCIMENTO_ORIGINAL := FConsulta_orig.FieldByName('dt_venc').AsDateTime;
        vVALOR_DOCUMENTO := FConsulta_orig.FieldByName('vl_titulo').Ascurrency;
        vVALOR_Pago := FConsulta_orig.FieldByName('vl_pago').Ascurrency;
        // validar se < 0 ir para credito no contas apagar
        // se vVALOR_Pago = 0 importa
        vVALOR_DOCUMENTO := FConsulta_orig.FieldByName('vl_titulo').Ascurrency -
                            FConsulta_orig.FieldByName('vl_desc').Ascurrency  +
                            FConsulta_orig.FieldByName('vl_acre').Ascurrency -
                            FConsulta_orig.FieldByName('vl_pago').Ascurrency;

        vSql := 'select cadastro_id from cadastros where chave_importacao = ' +
                FConsulta_orig.FieldByName('clienteid').AsString;
        vCADASTRO_ID := Busca_Registro(FConsulta_dest,vSql,'cadastro_id',MemErro);

        if (vVALOR_DOCUMENTO > 0) and (vCADASTRO_ID <> '') then
        begin
          vCadId := inttostr(j +1);

          vexec.Executar([
                         vCadId
                         ,1
                         //,null
                         ,48
                         //,null
                         //,null
                         //,null
                         ,1
                         ,FConsulta_orig.FieldByName('titulo').AsString
                         ,vDATA_CADASTRO

                         ,vDATA_EMISSAO
                         ,vDATA_VENCIMENTO
                         ,vDATA_VENCIMENTO_ORIGINAL
                         ,vVALOR_DOCUMENTO
                         //,null
                         //,null
                         ,'IMPORTACAO'
                         ,'9999'
                         ,'1.001.005'
                         ,'MAN'

                         ,vCADASTRO_ID
                         //,null
                         //,null
                         ]);

          j := j +1;
          Imp := Imp +1;
          lblImp.Caption := IntToStr(Imp);
          MemHist.Lines.Add(Uppercase(FConsulta_orig.FieldByName('titulo').AsString));
        end else
        begin
          Dupl := Dupl + 1 ;
        end;
        lblDupl.Caption := IntToStr(Dupl);
        lblImp.Caption := IntToStr(Imp);
        FConsulta_orig.Next;
      end;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        if not vsp then
        begin
          ShowMessage('Erro ao inserir '+Nome_Tabela+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;
    Conexao_dest.FinalizarTransacao;

    aux_seq := 'SEQ_RECEBER_ID';
    if Recria_Sequencia(aux_seq,Nome_Tabela,'RECEBER_ID',FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' realizada com sucesso!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' realizada com sucesso!';

    Result := True;
    MemErro.Lines.Clear;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Produtos(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,ins_Sql,vMarca,vAux,vCadId,Nome_Tabela,aux_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig,FConsulta_orig01 : TConsulta;
  i,j,Imp,Dupl: Integer;
  vExec:TExecucao;
  vPRODUTO_ID : Integer;
  vDATA_CADASTRO :TDate;
  vNOME : string;
  vNOME_COMPRA : string;
  vFORNECEDOR_ID :string;
  vUNIDADE_VENDA : string;
  vMARCA_ID : string;
  vMULTIPLO_VENDA : Currency;
  vPESO : Currency;
  vVOLUME : Currency;
  vORIGEM_PRODUTO : integer;
  vLINHA_PRODUTO_ID : string;
  vCODIGO_BARRAS : string;
  vACEITAR_ESTOQUE_NEGATIVO : string;
  vINATIVAR_ZERAR_ESTOQUE : string;
  vBLOQUEADO_COMPRAS : string;
  vBLOQUEADO_VENDAS : string;
  vPERMITIR_DEVOLUCAO : string;
  vCARACTERISTICAS : string;
  vPERCENTUAL_PIS : Currency;
  vCST_ENTRADA_PIS : string;
  vCST_SAIDA_PIS : string;
  vPERCENTUAL_COFINS : Currency;
  vCST_ENTRADA_COFINS : string;
  vCST_SAIDA_COFINS : string;
  vATIVO : string;
  vMOTIVO_INATIVIDADE : string;
  vCEST : string;
  vCODIGO_NCM : string;
  vPRODUTO_DIVERSOS_PDV : string;
  vCODIGO_BALANCA : string;
  vPRODUTO_PAI_ID : Variant;
  vQUANTIDADE_VEZES_PAI : Variant;
  vTIPO_CONTROLE_ESTOQUE : string;
  vKIT_ENTREGA_AGRUPADA : string;
  vEXIGIR_DATA_FABRICACAO_LOTE : string;
  vEXIGIR_DATA_VENCIMENTO_LOTE : string;
  vSOB_ENCOMENDA : string;
  vDIAS_AVISO_VENCIMENTO : string;
  vREVENDA : string;
  vUSO_CONSUMO : string;
  vSUJEITO_COMISSAO : string;
  vBLOQUEAR_ENTRAD_SEM_PED_COMPRA : string;
  vEXIGIR_MODELO_NOTA_FISCAL : string;
  vEXIGIR_SEPARACAO : string;
  vVALOR_ADICIONAL_FRETE : Currency;
  vPERCENTUAL_COMISSAO_VISTA : Currency;
  vPERCENTUAL_COMISSAO_PRAZO : Currency;
  vCODIGO_ORIGINAL_FABRICANTE : string;
  vCHAVE_IMPORTACAO : string;
  vTIPO_DEF_AUTOMATICA_LOTE : string;
  vUNIDADE_ENTREGA_ID : string;
begin
  Nome_Tabela := 'produtos';
  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_orig01 := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);
  lblDupl.Caption := '0';

  try
    FConsulta_orig.Connection := Conexao_orig;
    FConsulta_orig01.Connection := Conexao_orig;

    Application.ProcessMessages;

    vExec     := TExecucao.Create(Conexao_dest);

    vSql := 'DELETE FROM '+Nome_Tabela+' ';
    roda_sql(True,Conexao_dest,vSql,MemErro);
    MemHist.Lines.Clear;
    lblHis.Caption := 'Iniciando Importa��o de ' + Nome_Tabela;
    MemHist.Lines.Add('Iniciando Importa��o de ' + Nome_Tabela);
    MemHist.Lines.Add('');
    MemHist.Lines.Add('Limpando '+Nome_Tabela+'.');

    lblQuant.caption := Num_Registros(0,FConsulta_orig,'dbo.produto',MemErro);

    vSql :='select p.*,u.cd_unidade,m.ds_marca,g.ds_grupo,pg.produto_gradeID '+
           'from dbo.Produto p, dbo.unidade u, dbo.Marca m,dbo.Grupo g,dbo.produto_grade pg '+
           'where p.unidadeid = u.unidadeid and p.GrupoID = g.GrupoID '+
           '	and p.MarcaID = m.MarcaID and pg.produtoID = p.produtoID order by p.cd_ref';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);

    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar();

    ins_Sql :='insert into '+Nome_Tabela+'( '+
              'PRODUTO_ID '+//NUMBER(10,0),
              ',NOME'+// VARCHAR2(60) NOT NULL,
              ',NOME_COMPRA'+// VARCHAR2(60) NOT NULL,
              ',FORNECEDOR_ID'+// NUMBER(10,0) NOT NULL,
              ',UNIDADE_VENDA'+// VARCHAR2(3) NOT NULL,
              ',MARCA_ID'+// NUMBER(6,0) NOT NULL,
              ',MULTIPLO_VENDA'+// NUMBER(7,3) NOT NULL,
              ',PESO'+// NUMBER(8,3) NOT NULL,
              ',VOLUME'+// NUMBER(9,7) NOT NULL,
              ',LINHA_PRODUTO_ID'+// VARCHAR2(11) NOT NULL,

              ',CODIGO_BARRAS'+// VARCHAR2(14),
              ',ACEITAR_ESTOQUE_NEGATIVO'+// CHAR(1) DEFAULT 'N'  NOT NULL,
              ',INATIVAR_ZERAR_ESTOQUE'+// CHAR(1) DEFAULT 'S'  NOT NULL,
              ',PERCENTUAL_PIS'+// NUMBER(4,2) DEFAULT 0 NOT NULL,
              ',PERCENTUAL_COFINS'+// NUMBER(4,2) DEFAULT 0 NOT NULL,
              ',ATIVO'+// CHAR(1) DEFAULT 'S'  NOT NULL,
              ',MOTIVO_INATIVIDADE'+// VARCHAR2(200),
              ',CEST'+// VARCHAR2(8),
              ',CODIGO_NCM'+// VARCHAR2(8),
              ',CODIGO_ORIGINAL_FABRICANTE'+// VARCHAR2(20),

              ',CHAVE_IMPORTACAO'+// VARCHAR2(100),
              ',UNIDADE_ENTREGA_ID'+// VARCHAR2(3),

              ') values ( '+
              NParametros(22);
    vExec.Add(ins_Sql);

    if Imp = 215 then
      MemHist.Lines.Add('');
    Conexao_dest.IniciarTransacao;
    try
      MemHist.Lines.Add('');
      lblImp.Caption := '0';
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        lblHis.Caption := 'Inserindo ' + UpperCase(FConsulta_orig.FieldByName('ds_completa').AsString);

        vPRODUTO_ID := FConsulta_orig.FieldByName('cd_ref').AsInteger;
        vDATA_CADASTRO := date;
        vNOME := UpperCase(FConsulta_orig.FieldByName('ds_completa').AsString);
        vNOME_COMPRA := UpperCase(FConsulta_orig.FieldByName('ds_completa').AsString);

        vSql := 'select f.cnpj from dbo.fornecedor f, dbo.ProdutoFornecedor pf '+
                'where f.fornecedorID = pf.FornecedorId ' +
                'and pf.produtoID = ' + FConsulta_orig.FieldByName('produtoID').AsString;
        vAux := Busca_Registro_Origem(FConsulta_orig01,vSql,'cnpj',MemErro);
        vSql := 'select f.cadastro_id from cadastros f, fornecedores c '+
                'where  c.cadastro_id = f.cadastro_id and f.CPF_CNPJ = ''' + Ajusta_Cnpj(vAux) +'''';
        vFORNECEDOR_ID := Busca_Registro(FConsulta_dest,vSql,'cadastro_id',MemErro);
        if vFORNECEDOR_ID = '' then
          vFORNECEDOR_ID := '3085';
        vUNIDADE_VENDA := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('cd_unidade').AsString));

        vSql := 'select marca_id from marcas '+
                'where nome = ''' + UpperCase(FConsulta_orig.FieldByName('ds_marca').AsString) + '''';
        vMARCA_ID := Busca_Registro(FConsulta_dest,vSql,'marca_id',MemErro);
        if vMARCA_ID = '' then
          vMARCA_ID := '3';
        vMULTIPLO_VENDA := 1;
        vPESO :=FConsulta_orig.FieldByName('peso').AsCurrency;
        vVOLUME := 0;
        {*
  ORIGEM_PRODUTO
  0 - Nacional
  1 - Estrangeira - Importa��o direta, exceto a indicada no c�digo 6
  2 - Estrangeira - Adquirida no mercado interno, exceto a indicada no c�digo 7
  3 - Nacional, mercadoria ou bem com Conte�do de Importa��o superior a 40% (quarenta por cento)
  4 - Nacional, cuja produ��o tenha sido feita em conformidade com os processos produtivos b�sicos de que tratam o Decreto-Lei n� 288/67, e as Leis n�s 8.248/91, 8.387/91, 10.176/01 e 11 . 4 8 4 / 0 7
  5 - Nacional, mercadoria ou bem com Conte�do de Importa��o inferior ou igual a 40% (quarenta por cento)
  6 - Estrangeira - Importa��o direta, sem similar nacional, constante em lista de Resolu��o CAMEX
  7 - Estrangeira - Adquirida no mercado interno, sem similar nacional, constante em lista de Resolu��o CAMEX�
*}
        vSql := 'select max(DEPTO_SECAO_LINHA_ID) as max from PRODUTOS_DEPTOS_SECOES_LINHAS '+
                'where nome = ''' + UpperCase(TrocaCaracterEspecial(FConsulta_orig.FieldByName('ds_grupo').AsString,True)) + '''';
        vLINHA_PRODUTO_ID := Busca_Registro(FConsulta_dest,vSql,'max',MemErro);
        if vLINHA_PRODUTO_ID = '' then
          vLINHA_PRODUTO_ID := '999.999.999';
        vSql := 'SELECT EAN ' +
                ' FROM dbo.ProdutoEan ea ,dbo.produto_grade gr ' +
                '  where ea.produto_gradeID = gr.produto_gradeID '+
                '  and gr.produtoID = ' + FConsulta_orig.FieldByName('produtoID').AsString;
        vCODIGO_BARRAS := Busca_Registro_Origem(FConsulta_orig01,vSql,'EAN',MemErro);
        vACEITAR_ESTOQUE_NEGATIVO := 'S';
        vINATIVAR_ZERAR_ESTOQUE   := 'N';
        vBLOQUEADO_COMPRAS  := 'N';
        vBLOQUEADO_VENDAS   := 'N';
        vPERMITIR_DEVOLUCAO := 'S';
        vCARACTERISTICAS    := UpperCase(FConsulta_orig.FieldByName('ds_resumida').AsString);
        vPERCENTUAL_PIS     := 1.65;
        vCST_ENTRADA_PIS    := '50';
        vCST_SAIDA_PIS      := '01';
        vPERCENTUAL_COFINS  := 7.60;
        vCST_ENTRADA_COFINS := '50';
        vCST_SAIDA_COFINS   := '01';
        vATIVO := 'S';
        vMOTIVO_INATIVIDADE :='';
        if FConsulta_orig.FieldByName('ativo').AsString = '0' then
        begin
          vATIVO := 'N';
          vMOTIVO_INATIVIDADE :='INATIVO NO SISTEMA ANTERIOR';
        end;
        vCEST := FConsulta_orig.FieldByName('cest').AsString;
        vCODIGO_NCM := FConsulta_orig.FieldByName('nbm_sh').AsString;
        vPRODUTO_DIVERSOS_PDV := 'N';
        vCODIGO_BALANCA := '';
        vPRODUTO_PAI_ID := null;
        vQUANTIDADE_VEZES_PAI := 1;
        {/* TIPO_CONTROLE_ESTOQUE
  N - Normal
  L - Lote
  P - Piso
  G - Grade
  K - Kit
  T - Tinta manipulada( Em breve )
*/}
        vTIPO_CONTROLE_ESTOQUE := 'N';
        vKIT_ENTREGA_AGRUPADA  := 'N';
        vEXIGIR_DATA_FABRICACAO_LOTE := 'N';
        vEXIGIR_DATA_VENCIMENTO_LOTE := 'N';
        vSOB_ENCOMENDA := 'N';
        vDIAS_AVISO_VENCIMENTO := '0';
        vREVENDA := 'S';
        vUSO_CONSUMO := 'N';
        vSUJEITO_COMISSAO := 'S';
        vBLOQUEAR_ENTRAD_SEM_PED_COMPRA := 'N';
        vEXIGIR_MODELO_NOTA_FISCAL := 'N';
        vEXIGIR_SEPARACAO := 'N';
        vVALOR_ADICIONAL_FRETE := 0;
        vPERCENTUAL_COMISSAO_VISTA  := 0;
        vPERCENTUAL_COMISSAO_PRAZO  := 0;
        vCODIGO_ORIGINAL_FABRICANTE := FConsulta_orig.FieldByName('cd_ref').AsString;
        vCHAVE_IMPORTACAO := FConsulta_orig.FieldByName('produtoid').AsString;
        vTIPO_DEF_AUTOMATICA_LOTE := 'N';
        vUNIDADE_ENTREGA_ID := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('cd_unidade').AsString));

        vexec.Executar([
                        vPRODUTO_ID ,
                        vNOME ,
                        vNOME_COMPRA ,
                        vFORNECEDOR_ID ,
                        vUNIDADE_VENDA ,
                        vMARCA_ID ,
                        vMULTIPLO_VENDA ,
                        vPESO ,
                        vVOLUME ,
                        vLINHA_PRODUTO_ID ,
                        vCODIGO_BARRAS ,
                        vACEITAR_ESTOQUE_NEGATIVO ,
                        vINATIVAR_ZERAR_ESTOQUE ,
                        vPERCENTUAL_PIS ,
                        vPERCENTUAL_COFINS ,
                        vATIVO ,
                        vMOTIVO_INATIVIDADE ,
                        vCEST ,
                        vCODIGO_NCM ,
                        vCODIGO_ORIGINAL_FABRICANTE ,
                        vCHAVE_IMPORTACAO ,
                        vUNIDADE_ENTREGA_ID
                         ]);

        lblImp.Caption := IntToStr(StrToInt(lblImp.Caption) + 1);
        MemHist.Lines.Add(vNOME);
        MemHist.Lines.Add(Uppercase(FConsulta_orig.FieldByName('ds_completa').AsString));

        FConsulta_orig.Next;
      end;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        if not vsp then
        begin
          ShowMessage('Erro ao inserir '+Nome_Tabela+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;
    Conexao_dest.FinalizarTransacao;

    aux_seq := 'SEQ_PRODUTO_ID';
    if Recria_Sequencia(aux_seq,Nome_Tabela,'PRODUTO_ID',FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' realizada com sucesso!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' realizada com sucesso!';

    MemErro.Lines.Clear;
  finally
    FConsulta_orig.Free;
    FConsulta_orig01.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Transportadoras(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,vId,maxId: Integer;
  vExec:TExecucao;
  Existe_Tabela,Existe_Relacionamento:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'TRANSPORTADORAS';
  vNome_Tabela_Orig := 'TRANSPORTADORAS';
  vChave_orig := 'cadastro_id';
  vChave_dest := 'cadastro_id';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    maxId := strtoint(Max_Consulta(FConsulta_orig,vChave_orig,vNome_Tabela_orig));

    vSql :='(select CADASTRO_id,trunc(DATA_HORA_ALTERACAO) as DATA_HORA_ALTERACAO,ATIVA ' +
           'from Transportadoras order by CADASTRO_id)';
    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);

    vSql := 'select CADASTRO_id,trunc(DATA_HORA_ALTERACAO) as DATA_HORA_ALTERACAO,ATIVA ' +
            'from Transportadoras order by CADASTRO_id';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'CADASTRO_ID '+ //NUMBER(10,0),
               ',DATA_HORA_CADASTRO '+ //DATE DEFAULT trunc(sysdate)  NOT NULL,
               ') values ( '+
              NParametros(2);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 1;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes';
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('cadastro_id').AsString));
        vId := FConsulta_orig.FieldByName(vChave_orig).AsInteger;
//        if vId = 1 then
//          vId := MaxId + 1;

        vSql_Validacao :='select ' + vChave_dest +
                         ' from ' + vNome_Tabela_Dest +
                         ' where ' + vChave_dest + ' = '+ IntToStr(vId);
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        vSql_Validacao :='select ' + vChave_dest +
                         ' from cadastros ' +
                         ' where ' + vChave_dest + ' = '+ IntToStr(vId);
        Existe_Relacionamento := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

//        if i = 24520 then
//          lblHis.Caption :='Parada para correcao';

        if (not Existe_Tabela) and Existe_Relacionamento then
        begin
          j:= J +1;
          vexec.Executar([
                         vId,
                         FConsulta_orig.GetData('DATA_HORA_ALTERACAO')
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';

    MemErro.Lines.Clear;
    Result := True;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Fornecedores(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq:string;
  vTipo_Fornec,vTpFrete,vSug :string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,vId,maxId: Integer;
  vExec:TExecucao;
  Existe_Tabela,Existe_Relacionamento:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'fornecedores';
  vNome_Tabela_Orig := 'fornecedores';
  vChave_orig := 'cadastro_id';
  vChave_dest := 'cadastro_id';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;


    maxId := strtoint(Max_Consulta(FConsulta_orig,vChave_orig,vNome_Tabela_orig));

    vSql := '(select f.*,c.SUPER_SIMPLES,C.DATA_CADASTRO,o.observacao,c.CPF_CNPJ_SEM_PONTOS ' +
           'from fORNECEDORES f,cadastros c '+
           ' LEFT JOIN cadastros_observacoes o on(c.cadastro_id = o.cadastro_id) ' +
           '  where f.cadastro_id= c.cadastro_id  ' +
           '  order by f.CADASTRO_id)';
    lblQuant.caption := Num_Registros(0,FConsulta_orig,vSql,MemErro);

    vSql := 'select f.*,c.SUPER_SIMPLES,C.DATA_CADASTRO,o.observacao,c.CPF_CNPJ_SEM_PONTOS ' +
           'from fORNECEDORES f,cadastros c '+
           ' LEFT JOIN cadastros_observacoes o on(c.cadastro_id = o.cadastro_id) ' +
           '  where f.cadastro_id= c.cadastro_id  ' +
           '  order by f.CADASTRO_id';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+

               ' CADASTRO_ID ' +//NUMBER(10,0),
               ', SUPER_SIMPLES ' +//CHAR(1) DEFAULT 'N'  NOT NULL,
               ', DATA_CADASTRO ' +//DATE DEFAULT trunc(sysdate)  NOT NULL,
               ', TIPO_FORNECEDOR ' +//CHAR(1) DEFAULT 'R'  NOT NULL,
               ', OBSERVACOES ' +//VARCHAR2(1000),
               ', ATIVO ' +//CHAR(1) DEFAULT 'S'  NOT NULL,
               ', REVENDA ' +//CHAR(1) DEFAULT 'S'  NOT NULL,
               ', USO_CONSUMO ' +//CHAR(1) DEFAULT 'N'  NOT NULL,
               ', PLANO_FINANCEIRO_REVENDA ' +//VARCHAR2(9),
               ', PLANO_FINANCEIRO_USO_CONSUMO ' +//VARCHAR2(9),

               ', TIPO_RATEIO_FRETE ' +//CHAR(1) DEFAULT 'M'  NOT NULL,
               ', GRUPO_FORNECEDOR_ID ' +//NUMBER(4,0),
               ', PORTADOR_ID ' +//NUMBER(4,0),
               ', DIAS_ESTOQUE_MINIMO ' +//NUMBER(3,0) DEFAULT '0' NOT NULL,
               ', PRAZO_ENTREGA ' +//NUMBER(3,0) NOT NULL,
               ', TIPO_FRETE ' +//CHAR(2) DEFAULT 'NE' NOT NULL,
               ', PERCENTUAL_FRETE_COMPRA ' +//NUMBER(4,2) DEFAULT 0 NOT NULL,
               ', VALOR_FRETE_QUANTIDADE_COMPRA ' +//NUMBER(10,2) DEFAULT 0 NOT NULL,
               ', VALOR_FRETE_TONELADA_COMPRA ' +//NUMBER(10,2) DEFAULT 0 NOT NULL,
               ', NUNCA_ALTERAR_CUSTO_ENTRADA ' +//CHAR(1) DEFAULT 'N'    NOT NULL,

               ', GERA_DESPESA_MENSAL ' +//CHAR(1) DEFAULT 'N'    NOT NULL,
               ', TIPO_DESPESA_MENSAL ' +//VARCHAR2(100),

               ') values ( '+
              NParametros(22);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 1;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes';
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('CADASTRO_ID').AsString));
        vId := FConsulta_orig.FieldByName(vChave_orig).AsInteger;
//        if vId = 1 then
//          vId := MaxId + 1;

        vSql_Validacao := '';//'select cidade_id from cidades where NOME = ''' + vNome + ''' and ESTADO_ID = ''' + vId + '''';


        vSql_Validacao :='select ' + vChave_dest +
                         ' from ' + vNome_Tabela_Dest +
                         ' where ' + vChave_dest + ' = '+ IntToStr(vId);
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        vSql_Validacao :='select ' + vChave_dest +
                         ' from CADASTROS ' +
                         ' where ' + vChave_dest + ' = '+ IntToStr(vId);
        Existe_Relacionamento := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

//        if i = 24520 then
//          lblHis.Caption :='Parada para correcao';

        if (not Existe_Tabela) and Existe_Relacionamento then
        begin
          j:= J +1;
          vTipo_Fornec := 'R';
          if FConsulta_orig.GetString('TIPO_FORNECEDOR') = 'I' then
            vTipo_Fornec := 'I';
          if FConsulta_orig.GetString('TIPO_FORNECEDOR') = 'A' then
            vTipo_Fornec := 'D';
          if FConsulta_orig.GetString('TIPO_FORNECEDOR') = 'O' then
            vTipo_Fornec := 'O';
          if FConsulta_orig.GetString('TIPO_FORNECEDOR') = 'R' then
            vTipo_Fornec := 'R';

//        I industria,R revenda,A atacado,O outras ,M importador
//        '',R revenda,D distribuidor,I industria,O outro

          vTpFrete:=  FConsulta_orig.GetString('TIPO_FRETE');
          //adm'N' nenhum,'C' cif,'O' fob frete nota,'H' fob frete conhecimento'
          //Altis 'NE', 'CI' cif 'FN' fob frete nota,'FC' fob frete conhecimento'
          if vTpFrete = 'N' then
            vTpFrete := 'NE';
          if vTpFrete = 'C' then
            vTpFrete := 'CI';
          if vTpFrete = 'O' then
            vTpFrete := 'FN';
          if vTpFrete = 'H' then
            vTpFrete := 'FC';

          if FConsulta_orig.GetString('TIPO_FRETE') = 'S' then
            vSug := 'N'
          else
            vSug := 'S';

          vexec.Executar([
                         vId
                         ,FConsulta_orig.GetString('SUPER_SIMPLES')
                         ,FConsulta_orig.GetData('DATA_CADASTRO')
                         ,vTipo_Fornec
                         ,FConsulta_orig.GetString('observacao')
                         ,FConsulta_orig.GetString('FORNECEDOR_ATIVO')
                         ,FConsulta_orig.GetString('FORNECEDOR_MAT_REVENDA')
                         ,FConsulta_orig.GetString('FORNECEDOR_CONSUMO')
                         ,null
                         ,null

                         ,FConsulta_orig.GetString('TIPO_RATEIO_FRETE')
                         ,FConsulta_orig.GetInt('GRUPO_FORNECEDORES_ID')
                         ,null
                         ,FConsulta_orig.GetDouble('DIAS_ESTOQUE_MINIMO')
                         ,FConsulta_orig.GetDouble('PRAZO_ENTREGA')
                         ,vTpFrete
                         ,FConsulta_orig.GetDouble('PERCENTUAL_FRETE_COMPRA')
                         ,FConsulta_orig.GetDouble('VALOR_FRETE_QUANTIDADE_COMPRA')
                         ,FConsulta_orig.GetDouble('VALOR_FRETE_TONELADA_COMPRA')
                         ,vSug

                         ,FConsulta_orig.GetString('UTILIZAR_APENAS_DESP_MENSAIS')
                         ,null
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';

    MemErro.Lines.Clear;
    Result := True;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Funcionarios(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave,vNome_seq:string;
  vEnd,vComp,vCad:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,vId,maxId,vFilhos : Integer;
  vExec:TExecucao;
  vVend,vCaixa,vComprador,vMotorista :string;
  vData,vDtRg,vData_adm,vDtNasc,vDtCad : Variant;
  vExec_up:TExecucao;
  pFoto: TMemoryStream;
begin
  Result := False;

  vNome_Tabela_Dest := 'FUNCIONARIOS';
  vNome_Tabela_Orig := 'FUNCIONARIOS';
  vChave := 'FUNCIONARIO_ID';
  vNome_seq:= 'SEQ_FUNCIONARIO_ID';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(Conexao_orig);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);
  vExec_up  := TExecucao.Create(Conexao_dest);

  try
    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := 'where funcionario_id > 1';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    maxId := strtoint(Max_Consulta(FConsulta_orig,vChave,vNome_Tabela_orig));

    vSql :='(select f.*,c.DATA_NASCIMENTO,c.cpf,c.RG,c.ORGAO_EXPEDIDOR, '+
           'c.LOGRADOURO,c.COMPLEMENTO,c.BAIRRO_ID,c.CEP ' +
           'from funcionarios f,cadastros c '+
           'where f.cadastro_id= c.cadastro_id  ' +
           '  and f.funcionario_id > 1 )';
    lblQuant.caption := Num_Registros(0,FConsulta_orig,vSql,MemErro);

    vSql :='select f.*,c.DATA_NASCIMENTO,c.cpf,c.RG,c.ORGAO_EXPEDIDOR, '+
           'c.LOGRADOURO,c.COMPLEMENTO,c.BAIRRO_ID,c.CEP ' +
           'from funcionarios f,cadastros c '+
           'where f.cadastro_id= c.cadastro_id  ' +
           '  and f.funcionario_id > 1  order by f.funcionario_id'; //and f.ativo = ''S''

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+'( '+
              'FUNCIONARIO_ID ' + //NUMBER(4,0),
              ',NOME ' + //VARCHAR2(60) NOT NULL,
              ',APELIDO ' + //VARCHAR2(40),
              ',DATA_CADASTRO ' + //DATE DEFAULT trunc(sysdate)  NOT NULL,
              ',DATA_NASCIMENTO ' + //DATE NOT NULL,
              ',CARGO_ID ' + //NUMBER(3,0) NOT NULL,
              ',CPF ' + //VARCHAR2(14),
              ',RG ' + //VARCHAR2(10),
              ',ORGAO_EXPEDIDOR_RG ' + //VARCHAR2(10),
              ',DATA_ADMISSAO ' + //DATE NOT NULL,

              ',LOGRADOURO ' + //VARCHAR2(100) NOT NULL,
              ',COMPLEMENTO ' + //VARCHAR2(100) NOT NULL,
              ',NUMERO ' + //VARCHAR2(10) DEFAULT 'SN'  NOT NULL,
              ',BAIRRO_ID ' + //NUMBER(15,0) NOT NULL,
              ',CEP ' + //VARCHAR2(9),
              ',E_MAIL ' + //VARCHAR2(30),
              ',SERVIDOR_SMTP ' + //VARCHAR2(50),
              ',USUARIO_E_MAIL ' + //VARCHAR2(30),
              ',SENHA_E_MAIL ' + //VARCHAR2(32),
              ',PORTA_E_MAIL ' + //NUMBER(5,0),

              ',CADASTRO_ID ' + //NUMBER(10,0) NOT NULL,
              ',PERCENTUAL_COMISSAO_A_VISTA ' + //NUMBER(4,2) DEFAULT 0 NOT NULL,
              ',PERCENTUAL_COMISSAO_A_PRAZO ' + //NUMBER(4,2) DEFAULT 0 NOT NULL,
              ',SALARIO ' + //NUMBER(7,2) DEFAULT 0 NOT NULL,
              ',ATIVO ' + //CHAR(1) DEFAULT 'S'  NOT NULL,
              ',VENDEDOR ' + //CHAR(1) DEFAULT 'N'  NOT NULL,
              ',CAIXA ' + //CHAR(1) DEFAULT 'N'  NOT NULL,
              ',COMPRADOR ' + //CHAR(1) DEFAULT 'N'  NOT NULL,
              ',DATA_DESLIGAMENTO ' + //DATE,
              ',NUMERO_CARTEIRA_TRABALHO ' + //VARCHAR2(30),

              ',SERIE_CARTEIRA_TRABALHO ' + //VARCHAR2(10),
              ',PIS_PASEP ' + //VARCHAR2(30),
              ',MOTORISTA ' + //CHAR(1) DEFAULT 'N'  NOT NULL,
              ',TITULO_ELEITORAL ' + //CVARCHAR2(15),
              ',SECAO_TITULO_ELEITORAL ' + //CVARCHAR2(5),
              ',ZONA_TITULO_ELEITORAL ' + //CVARCHAR2(5),
              ',RESERVISTA ' + //CVARCHAR2(15),
              ',DATA_EMISSAO_RG ' + //CDATE,
              ',OBSERVACAO ' + //CVARCHAR2(400),
              ',QUANTIDADE_FILHOS ' + //CNUMBER(2,0) DEFAULT 0 NOT NULL,

              ',USUARIO_ALTIS ' + //CCHAR(1) DEFAULT 'N'  NOT NULL,
              ') values ( '+
              NParametros(41);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 1;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes';
          MemHist.Lines.Add(lblHis.Caption);
        end;

        Application.ProcessMessages;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));
        vId := FConsulta_orig.FieldByName(vChave).AsInteger;

        vVend := 'N';
        vCaixa := 'N';
        vComprador := 'N';
        vMotorista := 'N';
        vFilhos := 0;
        if FConsulta_orig.GetString('POSSUI_FILHOS') = 'S' then
            vFilhos := 1;

        if FConsulta_orig.GetInt('cargo_id') = 2 then
          vVend := 'S';

        if FConsulta_orig.GetInt('cargo_id') = 3 then
          vCaixa := 'S';

        if FConsulta_orig.GetInt('cargo_id') = 8 then
          vComprador := 'S';

        if FConsulta_orig.GetInt('cargo_id') = 506 then
          vMotorista := 'S';

        vData_adm := FConsulta_orig.GetString('DATA_ADMISSAO');
        if (vData_adm <> '') then
          vData_adm := FConsulta_orig.GetData('DATA_ADMISSAO')
        else
          vData_adm := FConsulta_orig.GetData('DATA_CADASTRO');

        vData := FConsulta_orig.GetString('DATA_DESLIGAMENTO');
        if (vData <> '') then
        begin
           if (FConsulta_orig.GetData('DATA_DESLIGAMENTO') <= FConsulta_orig.GetData('DATA_ADMISSAO')) then
             vData := FConsulta_orig.GetData('DATA_ADMISSAO') + 1
           else
             vData := FConsulta_orig.GetData('DATA_DESLIGAMENTO');
        end else
          vData := System.Variants.Null;

        if (FConsulta_orig.GetString('DATA_EMISSAO_RG') = '') or
           (FConsulta_orig.GetString('DATA_EMISSAO_RG') = '30/12/1899') then
          vDtRg := System.Variants.Null
        else
          vDtRg := FConsulta_orig.GetData('DATA_EMISSAO_RG');

        if (FConsulta_orig.GetString('DATA_CADASTRO') = '') or
           (FConsulta_orig.GetString('DATA_CADASTRO') = '30/12/1899') then
          vDtCad := Date
        else
          vDtCad := FConsulta_orig.GetData('DATA_CADASTRO');

        if FConsulta_orig.GetString('DATA_NASCIMENTO') = '' then
          vDtNasc := System.Variants.Null
        else
          vDtNasc := FConsulta_orig.GetData('DATA_NASCIMENTO');

        vEnd := Tira_Aspas(FConsulta_orig.GetString('LOGRADOURO'));
        vComp:= Tira_Aspas(FConsulta_orig.GetString('COMPLEMENTO'));

        if FConsulta_orig.GetInt('FUNCIONARIO_ID') = 67 then
          vCad := FConsulta_orig.GetString('CADASTRO_ID');

        vCad := FConsulta_orig.GetString('CADASTRO_ID');
        FConsulta_dest.Limpar;
        vSql_Validacao :='select cadastro_id from cadastros where ' +
                         'CPF_CNPJ = ''' + FConsulta_orig.GetString('cpf') +''' ';
        FConsulta_dest.Add(vSql_Validacao);
        if FConsulta_dest.Pesquisar then
          vCad := FConsulta_dest.GetString('CADASTRO_ID')
        else
          vCad := '';

        FConsulta_dest.Limpar;
        vSql_Validacao :='select  f.funcionario_id '+
         'from  funcionarios f '+
         'where f.CPF = ''' + FConsulta_orig.GetString('cpf') +''' or f.cadastro_id =' + FConsulta_orig.GetString('CADASTRO_ID');
        FConsulta_dest.Add(vSql_Validacao);
        if (not FConsulta_dest.Pesquisar) and (vCad <> '') then begin

          vexec.Executar([
                      FConsulta_orig.GetInt('FUNCIONARIO_ID'),
                      FConsulta_orig.GetString('NOME'),
                      FConsulta_orig.GetString('NOME'),
                      FConsulta_orig.GetData('DATA_CADASTRO'),
                      FConsulta_orig.GetData('DATA_NASCIMENTO'),
                      FConsulta_orig.GetString('CARGO_ID'),
                      FConsulta_orig.GetString('cpf'),
                      FConsulta_orig.GetString('RG'),
                      FConsulta_orig.GetString('ORGAO_EXPEDIDOR'),
                      FConsulta_orig.GetData('DATA_ADMISSAO'),

                      vEnd,
                      vComp,
                      'SN',
                      FConsulta_orig.GetString('BAIRRO_ID'),
                      FConsulta_orig.GetString('CEP'),
                      FConsulta_orig.GetString('E_MAIL'),
                      'rl-15us.hmservers.net',
                      FConsulta_orig.GetString('USUARIO_SMTP'),
                      FConsulta_orig.GetString('SENHA_SMTP'),
                      '587',

                      vCad,
                      FConsulta_orig.GetDouble('COMISSAO_VISTA'),
                      FConsulta_orig.GetString('COMISSAO_PRAZO'),
                      FConsulta_orig.GetString('SALARIO'),
                      FConsulta_orig.GetString('ATIVO'),
                      vVend,
                      vCaixa,
                      vComprador,
                      vData,
                      FConsulta_orig.GetString('CARTEIRA_TRABALHO'),

                      FConsulta_orig.GetString('SERIE_CARTEIRA_TRABALHO'),
                      FConsulta_orig.GetString('PIS_PASEP'),
                      vMotorista,
                      FConsulta_orig.GetString('TITULO_ELEITORAL'),
                      FConsulta_orig.GetString('SESSAO_TITULO_ELEITORAL'),
                      FConsulta_orig.GetString('ZONA_TITULO_ELEITORAL'),
                      FConsulta_orig.GetString('RESERVISTA'),
                      vDtRg,
                      FConsulta_orig.GetString('OBSERVACAO'),
                      vFilhos,

                      FConsulta_orig.GetString('USUARIO')
                       ]);
          pFoto := getFoto(Conexao_orig,FConsulta_orig.GetInt('FUNCIONARIO_ID'));

          if pFoto <> nil then begin
            vExec_up.Limpar;
            vExec_up.SQL.Add('update FUNCIONARIOS set ');
            vExec_up.SQL.Add('  FOTO = :P1 ');
            vExec_up.SQL.Add('where FUNCIONARIO_ID = :P2');
            vExec_up.CarregarBinario('P1', pFoto);
            vExec_up.ParamByName('P2').AsInteger := FConsulta_orig.GetInt('FUNCIONARIO_ID');
            vExec_up.Executar;
          end;

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          vExec_up.Limpar;

          if (vVend = 'S') then
          begin
            vExec_up.SQL.Add('update funcionarios set VENDEDOR = ''' + vVend + ''' where funcionario_id = ' + FConsulta_dest.GetString('funcionario_id'));
            vExec_up.Executar;
          end;
          if (vCaixa = 'S') then
          begin
            vExec_up.SQL.Add('update funcionarios set caixa = ''' + vCaixa + ''' where funcionario_id = ' + FConsulta_dest.GetString('funcionario_id'));
            vExec_up.Executar;
          end;
          if (vComprador = 'S') then
          begin
            vExec_up.SQL.Add('update funcionarios set comprador = ''' + vComprador + ''' where funcionario_id = ' + FConsulta_dest.GetString('funcionario_id'));
            vExec_up.Executar;
          end;

          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;

        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';

    MemErro.Lines.Clear;
    Result := True;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
    vExec_up.Free;
  end;

end;

function Importa_CLIENTES_GRUPOS(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,vId,maxId: Integer;
  vExec:TExecucao;
begin
  Result := False;

  vNome_Tabela_Dest := 'CLIENTES_GRUPOS';
  vNome_Tabela_Orig := 'GRUPOS_CLIENTES';
  vChave_orig := 'GRUPO_CLIENTES_ID';
  vChave_dest := 'CLIENTE_GRUPO_ID';
  vNome_seq:= 'SEQ_CLIENTES_GRUPOS_ID';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    maxId := strtoint(Max_Consulta(FConsulta_orig,vChave_orig,vNome_Tabela_orig));

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);

    vSql := 'select * from ' + vNome_Tabela_Orig;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'CLIENTE_GRUPO_ID ' +// number(3),
               ',NOME ' +//     varchar(30) not null,
               ') values ( '+
              NParametros(2);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 1;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));
        vId := FConsulta_orig.FieldByName(vChave_orig).AsInteger;
//        if vId = 1 then
//          vId := MaxId + 1;

        vSql_Validacao := 'select CLIENTE_GRUPO_ID from CLIENTES_GRUPOS where NOME = ''' + vNome + ''' ';

        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes';
          MemHist.Lines.Add(lblHis.Caption);
        end;

        if not Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro) then
        begin
          j:= J +1;
          vexec.Executar([
                          vId,
                          vNome
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';

    MemErro.Lines.Clear;
    Result := True;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_CLIENTES_ORIGENS(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,vId,maxId: Integer;
  vExec:TExecucao;
begin
  Result := False;

  vNome_Tabela_Dest := 'CLIENTES_ORIGENS';
  vNome_Tabela_Orig := 'ORIGENS_CLIENTES';
  vChave_orig := 'ORIGEM_CLIENTE_ID';
  vChave_dest := 'CLIENTE_ORIGEM_ID';
  vNome_seq:= 'SEQ_CLIENTES_ORIGENS_ID';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    maxId := strtoint(Max_Consulta(FConsulta_orig,vChave_orig,vNome_Tabela_orig));

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);

    vSql := 'select * from ' + vNome_Tabela_Orig;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'CLIENTE_ORIGEM_ID ' +// number(3),
               ',NOME ' +//     varchar(30) not null,
               ') values ( '+
              NParametros(2);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 1;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));
        vId := FConsulta_orig.FieldByName(vChave_orig).AsInteger;
//        if vId = 1 then
//          vId := MaxId + 1;

        vSql_Validacao := 'select CLIENTE_ORIGEM_ID from CLIENTES_ORIGENS where NOME = ''' + vNome + ''' ';

        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes';
          MemHist.Lines.Add(lblHis.Caption);
        end;

        if not Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro) then
        begin
          j:= J +1;
          vexec.Executar([
                          vId,
                          vNome
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';

    MemErro.Lines.Clear;
    Result := True;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_GRUPOS_FINANCEIROS(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,vId,maxId: Integer;
  vExec:TExecucao;
begin
  Result := False;

  vNome_Tabela_Dest := 'GRUPOS_FINANCEIROS';
  vNome_Tabela_Orig := 'GRUPOS_FINANCEIROS';
  vChave_orig := 'GRUPO_FINANCEIRO_ID';
  vChave_dest := 'GRUPO_FINANCEIRO_ID';
  vNome_seq:= 'SEQ_GRUPO_FINANCEIRO_ID';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    maxId := strtoint(Max_Consulta(FConsulta_orig,vChave_orig,vNome_Tabela_orig));

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);

    vSql := 'select * from ' + vNome_Tabela_Orig;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'GRUPO_FINANCEIRO_ID ' +// number(3),
               ',NOME ' +//     varchar(30) not null,
               ') values ( '+
              NParametros(2);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 1;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));
        vId := FConsulta_orig.FieldByName(vChave_orig).AsInteger;
//        if vId = 1 then
//          vId := MaxId + 1;

        vSql_Validacao := 'select GRUPO_FINANCEIRO_ID from GRUPOS_FINANCEIROS where NOME = ''' + vNome + ''' ';

        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes';
          MemHist.Lines.Add(lblHis.Caption);
        end;

        if not Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro) then
        begin
          j:= J +1;
          vexec.Executar([
                          vId,
                          vNome
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';

    MemErro.Lines.Clear;
    Result := True;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Clientes(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  aux_seq,vTp,vCom,vPrec,vFunc,vLim:string;
  vData,vData_val: Variant;
  FConsulta_orig,FConsulta_dest : TConsulta;
  i,Imp: Integer;
  vExec:TExecucao;
  vSql,ins_Sql,Nome_Tabela:string;
  FConsulta_orig01 : TConsulta;
  dupl: Integer;
  vCADASTRO_ID : Integer;
  vTIPO_CLIENTE :string; //' + //2CHAR(2) DEFAULT 'NC'  NOT NULL,
  vDATA_CADASTRO :Variant;//' + //3DATE DEFAULT trunc(sysdate)  NOT NULL,
  vCODIGO_SPC :Variant; //' + //4VARCHAR2(9),
  vATENDENTE_SPC :Variant; //' + //5VARCHAR2(30),
  vRESTRICAO_SPC :Variant; //' + //6CHAR(1) NOT NULL,
  vLOCAL_SERASA :Variant; //' + //10VARCHAR2(40),
  vCONDICAO_ID :Variant; //'+//NUMBER(3,0),
  vVENDEDOR_ID :Variant; //'+//NUMBER(4,0),
  vATENDENTE_SERASA :Variant; //' + //8VARCHAR2(30),
  vRESTRICAO_SERASA :string; //' + //9CHAR(1) NOT NULL,
  vEMITIR_SOMENTE_NFE :string; //' + //11CHAR(1) DEFAULT 'N'  NOT NULL,
  vNAO_GERAR_COMISSAO :string; //' + //12CHAR(1) DEFAULT 'N'  NOT NULL,
  vLIMITE_CREDITO :Currency;//' + //13NUMBER(10,2) DEFAULT 0 NOT NULL,
  vLIMITE_CREDITO_MENSAL :Currency; //' + //14NUMBER(10,2) DEFAULT 0 NOT NULL,
  vUSUARIO_APROV_LIMITE_CRED_ID :Variant; //' + //15NUMBER(4,0),
  vATIVO,vRecEnt :string;
  vTIPO_PRECO :string; //' + //17CHAR(1) DEFAULT 'V'  NOT NULL,
  vEXIGIR_MODELO_NOTA_FISCAL :string; //' + //18CHAR(1) DEFAULT 'N',
  vDATA_APROVACAO_LIMITE_CREDITO :Variant;//' + //26DATE,
  vDATA_PESQUISA_SPC :Variant;//' + //29DATE,
  vDATA_RESTRICAO_SPC :Variant;//' + //30DATE,
  vDATA_PESQUISA_SERASA :Variant;//' + //31DATE,
  vDATA_RESTRICAO_SERASA :Variant;//' + //32DATE,
  vDATA_VALIDADE_LIMITE_CREDITO :Variant;//' + //33DATE,
  vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave,vNome_seq:string;
  j,vId,maxId: Integer;
  Existe_Tabela,Existe_Relacionamento:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'CLIENTES';
  vNome_Tabela_Orig := 'CLIENTES';
  vChave := 'cadastro_id';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := 'where cadastro_id > 1';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    maxId := strtoint(Max_Consulta(FConsulta_orig,vChave,vNome_Tabela_orig));

    vSql := '(select c.*, to_char(cd.DATA_CADASTRO,''DD/MM/YYYY'') as DATA_CADASTRO '+
           'from CLIENTES c, cadastros cd '+
           'where c.cadastro_id = cd.cadastro_id and c.cadastro_id > 1 )';
    lblQuant.caption := Num_Registros(0,FConsulta_orig,vSql,MemErro);

    vSql := 'select c.*, to_char(cd.DATA_CADASTRO,''DD/MM/YYYY'') as DATA_CADASTRO '+
           'from CLIENTES c, cadastros cd '+
           'where c.cadastro_id = cd.cadastro_id and c.cadastro_id > 1 order by c.cadastro_id';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'CADASTRO_ID ' + // NUMBER(10,0),
               ',TIPO_CLIENTE ' + // CHAR(2) DEFAULT 'NC'  NOT NULL,
               ',DATA_CADASTRO ' + // DATE DEFAULT trunc(sysdate)  NOT NULL,
               ',CODIGO_SPC ' + // VARCHAR2(9),
               ',DATA_PESQUISA_SPC ' + // DATE,
               ',ATENDENTE_SPC ' + // VARCHAR2(30),
               ',RESTRICAO_SPC ' + // CHAR(1) NOT NULL,
               ',DATA_RESTRICAO_SPC ' + // DATE,
               ',LOCAL_SPC ' + // VARCHAR2(40),
               ',DATA_PESQUISA_SERASA ' + // DATE,

               ',ATENDENTE_SERASA ' + // VARCHAR2(30),
               ',RESTRICAO_SERASA ' + // CHAR(1) NOT NULL,
               ',DATA_RESTRICAO_SERASA ' + // DATE,
               ',LOCAL_SERASA ' + // VARCHAR2(40),
               ',CONDICAO_ID ' + // NUMBER(3,0),
               ',VENDEDOR_ID ' + // NUMBER(4,0),
               ',EMITIR_SOMENTE_NFE ' + // CHAR(1) DEFAULT 'N'  NOT NULL,
               ',NAO_GERAR_COMISSAO ' + // CHAR(1) DEFAULT 'N'  NOT NULL,
               ',LIMITE_CREDITO ' + // NUMBER(10,2) DEFAULT 0 NOT NULL,
               ',LIMITE_CREDITO_MENSAL ' + // NUMBER(10,2) DEFAULT 0 NOT NULL,

               ',DATA_APROVACAO_LIMITE_CREDITO ' + // DATE,
               ',USUARIO_APROV_LIMITE_CRED_ID ' + // NUMBER(4,0),
               ',ATIVO ' + // CHAR(1) DEFAULT 'S'  NOT NULL,
               ',EXIGIR_NOTA_SIMPLES_FAT ' + // CHAR(1) DEFAULT 'N'  NOT NULL,
               ',TIPO_PRECO ' + // CHAR(1) DEFAULT 'V'  NOT NULL,
               ',DATA_VALIDADE_LIMITE_CREDITO ' + // DATE,
               ',EXIGIR_MODELO_NOTA_FISCAL ' + // CHAR(1) DEFAULT 'N',
               ',QTDE_DIAS_VENCIMENTO_ACUMULADO ' + // NUMBER(3,0) DEFAULT 15 NOT NULL,
               ',PROTESTO_AUTOMATICO ' + // CHAR(1) DEFAULT 'S'   NOT NULL,
               ',EXIGE_PEDIDO_COMPRA ' + // CHAR(1) DEFAULT 'N'   NOT NULL,

               ',PERMITE_RECEBER_NA_ENTREGA ' + // CHAR(1) DEFAULT 'N'   NOT NULL,
               ',PERMITE_CHEQUE_AVISTA ' + // CHAR(1) DEFAULT 'N'   NOT NULL,
               ',PERMITE_CHEQUE_APRAZO ' + // CHAR(1) DEFAULT 'N'   NOT NULL,
               ',PERMITE_DUPLICATA ' + // CHAR(1) DEFAULT 'N'   NOT NULL,
               ',PERMITE_BOLETO ' + // CHAR(1) DEFAULT 'N'   NOT NULL,
               ',EMBUTIR_FRETE_NO_ITEM ' + // CHAR(1) DEFAULT 'S'   NOT NULL,
               ',MOTIVO_DO_CREDITO ' + // VARCHAR2(200),
               ',ORIGEM_NOTA_DEVOLUCAO ' + // CHAR(1) DEFAULT 'E'   NOT NULL,
               ',MENSAGEM_AVISO_VENDA ' + // VARCHAR2(200),
               ',MENSAGEM_AVISO_CAIXA ' + // VARCHAR2(200),

               ',NOME_MAE ' + // VARCHAR2(50),
               ',NOME_PAI ' + // VARCHAR2(50),
               ',NOME_CONJUGE ' + // VARCHAR2(50),
               ',CLIENTE_ORIGEM_ID ' + // NUMBER(6,0),
               ',CLIENTE_GRUPO_ID ' + // NUMBER(4,0),
               ',GRUPO_FINANCEIRO_ID ' + // NUMBER(4,0),

              ') values ( '+
              NParametros(46);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 1;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes';
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('cadastro_id').AsString));
        vId := FConsulta_orig.FieldByName('cadastro_id').AsInteger;
//        if vId = 1 then
//          vId := MaxId + 1;

        vSql_Validacao :='select c.cadastro_id '+
                         'from CLIENTES c '+
                         'where c.cadastro_id = '+ IntToStr(vId);
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave,MemErro);

        vSql_Validacao :='select c.cadastro_id '+
                         'from CADASTROS c '+
                         'where c.cadastro_id = '+ IntToStr(vId);
        Existe_Relacionamento := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave,MemErro);

//        if i = 24520 then
//          lblHis.Caption :='Parada para correcao';

        if (not Existe_Tabela) and Existe_Relacionamento then
        begin
          j:= J +1;
          if FConsulta_orig.GetString('TIPO_CLIENTE') = 'N' then
            vTp := 'NC'
          else if FConsulta_orig.GetString('TIPO_CLIENTE') = 'C' then
            vTp := 'CO'
          else if FConsulta_orig.GetString('TIPO_CLIENTE') = 'P' then
            vTp := 'OP'
          else if FConsulta_orig.GetString('TIPO_CLIENTE') = 'R' then
            vTp := 'RE'
          else if FConsulta_orig.GetString('TIPO_CLIENTE') = 'O' then
            vTp := 'CT'
          else if FConsulta_orig.GetString('TIPO_CLIENTE') = 'H' then
            vTp := 'CH'
          else if FConsulta_orig.GetString('TIPO_CLIENTE') = 'U' then
            vTp := 'PR'
          else if FConsulta_orig.GetString('TIPO_CLIENTE') = 'A' then
            vTp := 'RE'
          else
            vTp := 'NC';

          if FConsulta_orig.GetString('GERAR_COMISSAO') = 'S' then
            vCom := 'N'
          else
            vCom := 'S';

          if FConsulta_orig.GetString('TIPO_PRECO') = 'ATA' then
            vPrec := 'A'  // atacado
          else
            vPrec := 'V'; //varejo

          if FConsulta_orig.GetString('BLOQUEAR_VENDAS_REC_ENTREGA') = 'S' then
            vRecEnt := 'N'
          else
            vRecEnt := 'S';

          if (FConsulta_orig.GetDouble('LIMITE_CREDITO_APROVADO') > 0) or
             (FConsulta_orig.GetDouble('LIMITE_CREDITO_MENSAL') > 0) then
          begin
            vLim := FConsulta_orig.GetString('LIMITE_CREDITO_APROVADO');
            vFunc := '1';  // atacado
            vData := FConsulta_orig.GetData('DATA_APROVACAO');
            vData_val := FConsulta_orig.GetData('VALIDADE_LIMITE_CREDITO');

            if (FConsulta_orig.GetString('DATA_APROVACAO') = '') or
               (FConsulta_orig.GetString('VALIDADE_LIMITE_CREDITO') = '')then
            begin
              vLim := '0';
              vFunc := '';
              vData := System.Variants.Null;
              vData_val := System.Variants.Null;
            end;
          end else
          begin
            vLim := '0';
            vFunc := '';
            vData := System.Variants.Null;
            vData_val := System.Variants.Null;
          end;

          vexec.Executar([
                        vId    //1
                        ,vtp                                    //2
                        ,FConsulta_orig.GetData('CLIENTE_DESDE')             //19
                        ,FConsulta_orig.GetString('CODIGO_SPC')  //4
                        ,FConsulta_orig.GetData('DATA_PESQUISA_SPC')
                        ,FConsulta_orig.GetString('ATENDENTE_SPC')    //5
                        ,FConsulta_orig.GetString('RESTRICAO_SPC')    //6
                        ,FConsulta_orig.GetData('DATA_RESTRICAO_SPC')
                        ,FConsulta_orig.GetString('LOCAL_SPC')        //7
                        ,FConsulta_orig.GetData('DATA_PESQUISA_SERASA')

                        ,FConsulta_orig.GetString('ATENDENTE_SERASA') //8
                        ,FConsulta_orig.GetString('RESTRICAO_SERASA') //9
                        ,FConsulta_orig.GetData('DATA_RESTRICAO_SERASA')
                        ,FConsulta_orig.GetString('LOCAL_SERASA')     //10
                        ,FConsulta_orig.GetString('CONDICAO_ID')                 //27
                        ,FConsulta_orig.GetString('FUNCIONARIO_ID')              //28
                        ,'N'                                          //11
                        ,vCom                                         //12
                        ,strtofloat(vLim)                             //13
                        ,FConsulta_orig.GetString('LIMITE_CREDITO_MENSAL')   //14

                        ,vData
                        ,vFunc //15
                        ,FConsulta_orig.GetString('CLIENTE_ATIVO')           //16
                        ,'N'
                        ,vPrec                                               //17
                        ,vData_val
                        ,FConsulta_orig.GetString('EXIGE_NOTA_MODELO_1')     //18
                        ,15
                        ,'S'
                        ,FConsulta_orig.GetString('BLOQ_VENDAS_SEM_COD_PED_COMPRA')

                        ,vRecEnt
                        ,FConsulta_orig.GetString('CHEQUE_VISTA_VENDA')
                        ,FConsulta_orig.GetString('CHEQUE_PRAZO_VENDA')
                        ,FConsulta_orig.GetString('DUPLICATA_VENDA')
                        ,FConsulta_orig.GetString('BOLETA_VENDA')
                        ,FConsulta_orig.GetString('EMBUTIR_FRETE_PRECO_ITEM_NF') //21
                        ,FConsulta_orig.GetString('JUSTIFICATIVA_CREDITO')
                        ,FConsulta_orig.GetString('ORIGEM_NOTA_DEVOLUCAO_VENDA')
                        ,FConsulta_orig.GetString('MENSAGEM_AVISO_VENDA')
                        ,''

                        ,FConsulta_orig.GetString('NOME_MAE')
                        ,FConsulta_orig.GetString('NOME_PAI')
                        ,FConsulta_orig.GetString('NOME_CONJUGE')
                        ,FConsulta_orig.GetString('ORIGEM_CLIENTE_ID')
                        ,FConsulta_orig.GetString('GRUPO_CLIENTES_ID')           //23
                        ,FConsulta_orig.GetString('GRUPO_FINANCEIRO_ID') //24
                       ]);
          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';

    MemErro.Lines.Clear;
    Result := True;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Pessoas(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave,vNome_seq:string;
  vCnpj,vInsc,vEc:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,vId,maxId: Integer;
  vExec:TExecucao;
begin
  Result := False;

  vNome_Tabela_Dest := 'CADASTROS';
  vNome_Tabela_Orig := 'CADASTROS';
  vChave := 'cadastro_id';
  vNome_seq:= 'SEQ_CADASTRO_ID';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := 'where cadastro_id > 1';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      lblHis.Caption :='Limpando registros da tabela ' + vNome_Tabela_Dest;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;
    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;

    maxId := strtoint(Max_Consulta(FConsulta_orig,vChave,vNome_Tabela_orig));

    vSql := '(select c.*, o.observacao from CADASTROS c ' +
           ' LEFT JOIN cadastros_observacoes o on c.cadastro_id = o.cadastro_id ' +
           ' where c.cadastro_id > 1 order by c.cadastro_id) ';
    lblQuant.caption := Num_Registros(0,FConsulta_orig,vSql,MemErro);

    vSql := 'select c.*, o.observacao from CADASTROS c ' +
           ' LEFT JOIN cadastros_observacoes o on c.cadastro_id = o.cadastro_id ' +
           ' where c.cadastro_id > 1 order by c.cadastro_id ';
    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql := 'insert into '+vNome_Tabela_Dest+
                '( '+
                'CADASTRO_ID '+    //NUMBER(10,0),
                ',NOME_FANTASIA '+ //VARCHAR2(100) NOT NULL,
                ',RAZAO_SOCIAL '+  //VARCHAR2(100) NOT NULL,
                ',TIPO_PESSOA '+   //CHAR(1) NOT NULL,
                ',CPF_CNPJ '+      //VARCHAR2(19) NOT NULL,
                ',LOGRADOURO '+    //VARCHAR2(100),
                ',COMPLEMENTO '+   //VARCHAR2(100),
                ',NUMERO '+        //VARCHAR2(10) DEFAULT 'SN'  NOT NULL,
                ',BAIRRO_ID '+     //NUMBER(15,0) NOT NULL,
                ',PONTO_REFERENCIA '+ //VARCHAR2(100),
                ',CEP '+ //VARCHAR2(9),
                ',TELEFONE_PRINCIPAL '+ //VARCHAR2(15),
                ',TELEFONE_CELULAR '+ //VARCHAR2(15),
                ',TELEFONE_FAX '+ //VARCHAR2(15),
                ',E_MAIL '+ //VARCHAR2(30),
                ',SEXO '+ //CHAR(1),
                ',ESTADO_CIVIL '+ //CHAR(1),
                ',DATA_NASCIMENTO '+ //DATE,
                ',DATA_CADASTRO '+ //DATE DEFAULT trunc(sysdate)  NOT NULL,
                ',INSCRICAO_ESTADUAL '+ //VARCHAR2(12) DEFAULT 'ISENTO'  NOT NULL,
                ',CNAE '+ //VARCHAR2(12),
                ',RG '+ //VARCHAR2(10),
                ',ORGAO_EXPEDIDOR_RG '+ //VARCHAR2(10),
                ',E_CLIENTE '+ //CHAR(1) DEFAULT 'N'  NOT NULL,
                ',E_FORNECEDOR '+ //CHAR(1) DEFAULT 'N'  NOT NULL,
                ',ATIVO '+ //CHAR(1) DEFAULT 'S'  NOT NULL,
                ',E_MOTORISTA '+ //CHAR(1) DEFAULT 'N'  NOT NULL,
                ',E_TRANSPORTADORA '+ //CHAR(1) DEFAULT 'N'  NOT NULL,
                ',CHAVE_IMPORTACAO '+ //VARCHAR2(100),
                ',OBSERVACOES ' +//VARCHAR2(800)
                ',DATA_EMISSAO_RG ' +//DATE,
                ',E_CONCORRENTE  ' +//CHAR(1) DEFAULT 'N'   NOT NULL,
                ',E_FUNCIONARIO ' +//CHAR(1) DEFAULT 'N'   NOT NULL,
                ',NACIONALIDADE ' +//VARCHAR2(40),
                ',NATURALIDADE ' +//VARCHAR2(40),
                ',E_PROFISSIONAL ' +//CHAR(1) DEFAULT 'N'   NOT NULL,
                ',E_MAIL_NFE2 ' +//VARCHAR2(80),
                ') values ( '+
                NParametros(37);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 1;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('NOME_RAZAO_SOCIAL').AsString));
        vId   := FConsulta_orig.FieldByName(vChave).AsInteger;
        if vId = 1 then
          vId := MaxId + 1;

        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes';
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vCnpj := FConsulta_orig.GetString('CPF_CNPJ_SEM_PONTOS');
        if Length(vCnpj) = 11 then
          vCnpj := Copy(vCnpj,1,3) + '.' + Copy(vCnpj,4,3) + '.' + Copy(vCnpj,7,3) + '-' + Copy(vCnpj,10,2)
        else
          vCnpj := Copy(vCnpj,1,2) + '.' + Copy(vCnpj,3,3) + '.' + Copy(vCnpj,6,3) + '/' + Copy(vCnpj,9,4) + '-' + Copy(vCnpj,13,2);

        if FConsulta_orig.GetString('INSCRICAO_ESTADUAL') = '' then
          vInsc := 'ISENTO'
        else
          vInsc := FConsulta_orig.GetString('INSCRICAO_ESTADUAL');

        vEc := Decode(
        FConsulta_orig.GetString('ESTADO_CIVIL'),
        ['SOL', 'S',
         'CAS', 'C',
         'VIU', 'V',
         'DIV', 'D',
         'SEP', 'E'
        ]);

        FConsulta_dest.Limpar;
        vSql_Validacao :='select c.cadastro_id '+
                         'from CADASTROS c '+
                         'where c.CPF_CNPJ = ''' + vCnpj +'''';

        if not Existe_Registro(FConsulta_dest,vSql_Validacao,vChave,MemErro) then
        begin
          j:= J +1;
          vexec.Executar([
                          FConsulta_orig.GetInt('cadastro_id'),
                          FConsulta_orig.GetString('NOME_FANTASIA'),
                          FConsulta_orig.GetString('NOME_RAZAO_SOCIAL'),
                          FConsulta_orig.GetString('TIPO_PESSOA'),
                          vCnpj,
                          FConsulta_orig.GetString('LOGRADOURO'),
                          FConsulta_orig.GetString('COMPLEMENTO'),
                          'SN',
                          FConsulta_orig.GetString('BAIRRO_ID'),
                          FConsulta_orig.GetString('PONTO_REFERENCIA'),
                          FConsulta_orig.GetString('CEP'),
                          null,null,null,
                          FConsulta_orig.GetString('E_MAIL_NFE'),
                          FConsulta_orig.GetString('SEXO'),
                          vEc,
                          FConsulta_orig.GetData('DATA_NASCIMENTO'),
                          FConsulta_orig.GetData('DATA_CADASTRO'),
                          vInsc,
                          FConsulta_orig.GetString('CNAE'),
                          FConsulta_orig.GetString('RG'),
                          FConsulta_orig.GetString('ORGAO_EXPEDIDOR'),
                          FConsulta_orig.GetString('E_CLIENTE'),
                          FConsulta_orig.GetString('E_FORNECEDOR'),
                          'S',
                          FConsulta_orig.GetString('E_MOTORISTA'),
                          FConsulta_orig.GetString('E_TRANSPORTADORA'),
                          FConsulta_orig.GetString('cadastro_id'),
                          FConsulta_orig.GetString('OBSERVACAO'),
                          FConsulta_orig.GetData('DATA_EMISSAO_RG'),
                          FConsulta_orig.GetString('E_CONCORRENTE'),
                          FConsulta_orig.GetString('FUNCIONARIO'),
                          FConsulta_orig.GetString('NACIONALIDADE'),
                          FConsulta_orig.GetString('NATURALIDADE'),
                          FConsulta_orig.GetString('E_PROFISSIONAL'),
                          FConsulta_orig.GetString('E_MAIL_NFE2')
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';

    MemErro.Lines.Clear;
    Result := True;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Telefones (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave,vNome_seq:string;
  vUp_Sql: string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,vId,vIdAnt,maxId: Integer;
  vExec:TExecucao;
begin
  Result := False;

  vNome_Tabela_Dest := 'CADASTROS_TELEFONES';
  vNome_Tabela_Orig := 'TELEFONES_CADASTROS';
  vChave := 'CADASTRO_ID';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    maxId := strtoint(Max_Consulta(FConsulta_orig,vChave,vNome_Tabela_orig));

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);

    vSql := 'select * from ' + vNome_Tabela_Orig;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'CADASTRO_ID '+//NUMBER(3,0),
               ',TIPO_TELEFONE '+// CHAR(1) NOT NULL,
               ',RAMAL '+// NUMBER(4,0),
               ',TELEFONE '+// VARCHAR2(30),
               ',OBSERVACAO '+// VARCHAR2(100)
               ') values ( '+
               NParametros(5);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 1;
      vIdAnt := 0;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('CADASTRO_ID').AsString));
        vId := FConsulta_orig.FieldByName(vChave).AsInteger;
        if vId = 1 then
          vId := MaxId + 1;

        vSql_Validacao := 'select cadastro_id from cadastros where cadastro_id = ' + IntToStr(vId);

        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes';
          MemHist.Lines.Add(lblHis.Caption);
        end;

        if Existe_Registro(FConsulta_dest,vSql_Validacao,vChave,MemErro) then
        begin
          j:= J +1;
          vExec.Limpar;
          vExec.Add(vIns_Sql);
          vexec.Executar([
                          vId,
                          FConsulta_orig.FieldByName('TIPO_TELEFONE').AsString,
                          FConsulta_orig.FieldByName('RAMAL').AsInteger,
                          FConsulta_orig.FieldByName('TELEFONE').AsString,
                          FConsulta_orig.FieldByName('OBSERVACAO').AsString
                          ]);

          vExec.Limpar;
          if FConsulta_orig.FieldByName('TIPO_TELEFONE').AsString = 'R' then
            vExec.Add('UPDATE CADASTROS SET TELEFONE_PRINCIPAL = ''' + FConsulta_orig.FieldByName('TELEFONE').AsString +
                      ' '' WHERE CADASTRO_ID = ' + IntToStr(vId) + ' and TELEFONE_PRINCIPAL = '''' ')
          else
            if FConsulta_orig.FieldByName('TIPO_TELEFONE').AsString = 'C' then
              vExec.Add('UPDATE CADASTROS SET TELEFONE_PRINCIPAL = ''' + FConsulta_orig.FieldByName('TELEFONE').AsString +
                        ' '' WHERE CADASTRO_ID = ' + IntToStr(vId) + ' and TELEFONE_PRINCIPAL = '''' ')
            else
              if FConsulta_orig.FieldByName('TIPO_TELEFONE').AsString = 'F' then
                vExec.Add('UPDATE CADASTROS SET TELEFONE_FAX = ''' + FConsulta_orig.FieldByName('TELEFONE').AsString +
                          ' '' WHERE CADASTRO_ID = ' + IntToStr(vId) + ' and TELEFONE_FAX = '''' ')
              else
                if FConsulta_orig.FieldByName('TIPO_TELEFONE').AsString = 'C' then
                  vExec.Add('UPDATE CADASTROS SET TELEFONE_CELULAR = ''' + FConsulta_orig.FieldByName('TELEFONE').AsString +
                            ' '' WHERE CADASTRO_ID = ' + IntToStr(vId) + ' and TELEFONE_CELULAR = '''' ');

          if vExec.SQL.Text <> '' then
            vExec.Executar;

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        vIdAnt := vId;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';

    MemErro.Lines.Clear;
    Result := True;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_DIVERSOS_CONTATOS(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,vId,maxId: Integer;
  vExec:TExecucao;
begin
  Result := False;

  vNome_Tabela_Dest := 'DIVERSOS_CONTATOS';
  vNome_Tabela_Orig := 'CONTATOS_CADASTROS';
  vChave := 'CADASTRO_ID';
  vNome_seq:= 'xxxxxx';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    maxId := strtoint(Max_Consulta(FConsulta_orig,vChave,vNome_Tabela_orig));

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);

    vSql := 'select * from ' + vNome_Tabela_Orig;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'CADASTRO_ID '+//NUMBER(10,0),
               ', ORDEM '+// NUMBER(3,0),
               ', DESCRICAO '+// VARCHAR2(60) NOT NULL,
               ', TELEFONE '+// VARCHAR2(30),
               ', RAMAL '+// NUMBER(5,0),
               ', CELULAR '+// VARCHAR2(30),
               ', E_MAIL '+// VARCHAR2(30),
               ', OBSERVACAO '+// VARCHAR2(200),
               ', FUNCAO '+// VARCHAR2(30),
               ', SEXO '+// CHAR(1),
               ', DATA_NASCIMENTO '+
               ') values ( '+
              NParametros(11);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;

      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('NOME').AsString));

        vId := FConsulta_orig.FieldByName(vChave).AsInteger;

        vSql_Validacao := 'select cadastro_id from cadastros where cadastro_id = ' + IntToStr(vId);

        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes';
          MemHist.Lines.Add(lblHis.Caption);
        end;

        if Existe_Registro(FConsulta_dest,vSql_Validacao,vChave,MemErro) and
                          ((FConsulta_orig.FieldByName('TELEFONE').AsString <> '') or
                           (FConsulta_orig.FieldByName('CELULAR').AsString <> '') or
                           (FConsulta_orig.FieldByName('E_MAIL').AsString <> '')) then
        begin
          vexec.Executar([
                          vId,
                          FConsulta_orig.FieldByName('ORDEM').AsInteger,
                          FConsulta_orig.FieldByName('NOME').AsString,
                          FConsulta_orig.FieldByName('TELEFONE').AsString,
                          0,
                          FConsulta_orig.FieldByName('CELULAR').AsString,
                          FConsulta_orig.FieldByName('E_MAIL').AsString,
                          FConsulta_orig.FieldByName('OBSERVACAO').AsString,
                          FConsulta_orig.FieldByName('FUNCAO').AsString,
                          FConsulta_orig.FieldByName('SEXO').AsString,
                          FConsulta_orig.FieldByName('DATA_NASCIMENTO').AsString
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';

    MemErro.Lines.Clear;
    Result := True;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_EMAILS(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,vId,maxId: Integer;
  vExec:TExecucao;
begin
  Result := False;

  vNome_Tabela_Dest := 'CADASTROS_EMAILS';
  vNome_Tabela_Orig := 'EMAILS_CADASTROS';
  vChave := 'CADASTRO_ID';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    maxId := strtoint(Max_Consulta(FConsulta_orig,vChave,vNome_Tabela_orig));

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);

    vSql := 'select * from ' + vNome_Tabela_Orig;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
                'CADASTRO_ID ' +//NUMBER(10,0),
                ',E_MAIL ' +//VARCHAR2(80),
                ',OBSERVACAO ' +//VARCHAR2(100)
               ') values ( '+
              NParametros(3);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 1;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('E_MAIL').AsString));
        vId := FConsulta_orig.FieldByName(vChave).AsInteger;
//        if vId = 1 then
//          vId := MaxId + 1;

        vSql_Validacao := 'select cadastro_id from cadastros where cadastro_id = ' + IntToStr(vId);

        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes';
          MemHist.Lines.Add(lblHis.Caption);
        end;

        if Existe_Registro(FConsulta_dest,vSql_Validacao,vChave,MemErro) then
        begin
          j:= J +1;
          vexec.Executar([
                          vId,
                          FConsulta_orig.FieldByName('E_MAIL').AsString,
                          'MIGRADO ADM'
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';

    MemErro.Lines.Clear;
    Result := True;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Profissionais(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave,vNome_seq:string;
  vCARGOID:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  FConsulta_orig1: TConsulta;
  i,j,vId,maxId : Integer;
  vExec:TExecucao;
begin
  Result := False;

  vNome_Tabela_Dest := 'PROFISSIONAIS';
  vNome_Tabela_Orig := 'PROFISSIONAIS';
  vChave := 'CADASTRO_ID';
  vNome_seq:= 'xxxxxx';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig  := TConsulta.Create(Conexao_orig);
  FConsulta_orig1 := TConsulta.Create(Conexao_orig);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    maxId := strtoint(Max_Consulta(FConsulta_orig,vChave,vNome_Tabela_orig));

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);

    vSql := 'select * from ' + vNome_Tabela_Orig;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               ' CADASTRO_ID '+//NUMBER(10,0),
               ', ATIVO '+// CHAR(1) DEFAULT 'S'    NOT NULL,
               ', CARGO_ID '+// NUMBER(6,0) NOT NULL,
               ', NUMERO_CREA '+// VARCHAR2(30),
               ', PERCENTUAL_COMISSAO '+// NUMBER(5,2) NOT NULL,
               ', PONTOS '+// NUMBER(10,0) DEFAULT 0 NOT NULL,
               ', PROFISSIONAL_INDICOU_ID '+// NUMBER(10,0),
               ', REGRA_GERAL_COMISSAO_GRUPOS '+// CHAR(1) DEFAULT 'S'    NOT NULL,
               ', TIPO_COMISSAO '+// CHAR(3) DEFAULT 'GGE'    NOT NULL,
               ', UTILIZAR_PRECO_ESPECIAL '+// CHAR(1) DEFAULT 'N'    NOT NULL,
               ', VIP '+// CHAR(1) DEFAULT 'N'    NOT NULL,
               ', INFORMACOES '+// VARCHAR2(400),
               ') values ( '+
              NParametros(12);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 1;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('cadastro_id').AsString));
        vId   := FConsulta_orig.FieldByName(vChave).AsInteger;
//        if vId = 1 then
//          vId := MaxId + 1;


        vCARGOID := Busca_Registro_Origem(FConsulta_orig1,
                                          'select nome from cargos where cargo_id = ' +
                                          FConsulta_orig.FieldByName('CARGO_ID').AsString,
                                          'nome',MemErro);

        vCARGOID := Busca_Registro(FConsulta_dest,'select cargo_id from profissionais_cargos where nome = ''' + vCARGOID + '''','cargo_id',MemErro);
        if vCARGOID = '' then
          vCARGOID := '1';

        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes';
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vSql_Validacao := 'select cadastro_id from cadastros where cadastro_id = ' + IntToStr(vId);

        if Existe_Registro(FConsulta_dest,vSql_Validacao,vChave,MemErro) then
        begin
          j:= J +1;
          vexec.Executar([
                         vId,
                         FConsulta_orig.FieldByName('ATIVO').AsString,
                         vCARGOID,
                         FConsulta_orig.FieldByName('NUMERO_CREA').AsString,
                         FConsulta_orig.FieldByName('PERCENTUAL_COMISSAO').AsCurrency,
                         FConsulta_orig.FieldByName('PONTOS').AsCurrency,
                         FConsulta_orig.FieldByName('PROFISSIONAL_INDICOU_ID').AsString,
                         FConsulta_orig.FieldByName('REGRA_GERAL_COMISSAO_GRUPOS').AsString,
                         FConsulta_orig.FieldByName('TIPO_COMISSAO').AsString,
                         FConsulta_orig.FieldByName('UTILIZAR_PRECO_PROMOCAO').AsString,
                         FConsulta_orig.FieldByName('VIP').AsString,
                         ''
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';

    MemErro.Lines.Clear;
    Result := True;
  finally
    FConsulta_orig.Free;
    FConsulta_orig1.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Pessoas2(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,ins_Sql,vBairro,aux_seq,Nome_Tabela,vCnpj,vInsc,vTp,vAtivo:string;
  nome_bairro,nome_cidade,uf:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig,FConsulta_orig01 : TConsulta;
  i,Dupl,Imp,j: Integer;
  vExec:TExecucao;
begin
  Result := False;

  Nome_Tabela := 'CADASTROS';
  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_orig01 := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);
  lblDupl.Caption := '0';

  try
    FConsulta_orig.Connection := Conexao_orig;
    FConsulta_orig01.Connection := Conexao_orig;

    MemHist.Lines.Clear;
    lblHis.Caption := 'Continuando Importa��o de ' + Nome_Tabela;
    MemHist.Lines.Add('Continuando Importa��o de ' + Nome_Tabela);
    MemHist.Lines.Add('');

    vSql :=' dbo.fornecedor c, dbo.cep ' +
           ' where c.cepID = cep.cepID  ';

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vSql,MemErro);

    vSql :='select c.*,cep.cep,cep.logradouro,cep.bairroid,cep.numeroini ' +
           'from dbo.fornecedor c, dbo.cep ' +
           'where c.cepID = cep.cepID order by c.fornecedorid';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);

    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar();

    ins_Sql :='insert into '+Nome_Tabela+'( '+
              'CADASTRO_ID '+    //NUMBER(10,0),
              ',NOME_FANTASIA '+ //VARCHAR2(100) NOT NULL,
              ',RAZAO_SOCIAL  '+ //VARCHAR2(100) NOT NULL,
              ',TIPO_PESSOA   '+ //CHAR(1) NOT NULL,
              ',CPF_CNPJ      '+ //VARCHAR2(19) NOT NULL,
              ',LOGRADOURO    '+ //VARCHAR2(100),
              ',COMPLEMENTO   '+ //VARCHAR2(100),
              ',NUMERO        '+ //VARCHAR2(10) DEFAULT 'SN'  NOT NULL,
              ',BAIRRO_ID     '+ //NUMBER(15,0) NOT NULL,
              ',PONTO_REFERENCIA '+ //VARCHAR2(100),

              ',CEP '+ //VARCHAR2(9),
              ',TELEFONE_PRINCIPAL '+ //VARCHAR2(15),
              ',TELEFONE_CELULAR '+ //VARCHAR2(15),
              ',TELEFONE_FAX '+ //VARCHAR2(15),
              ',E_MAIL '+ //VARCHAR2(30),
              ',SEXO '+ //CHAR(1),
              ',ESTADO_CIVIL '+ //CHAR(1),
              ',DATA_NASCIMENTO '+ //DATE,
              ',DATA_CADASTRO '+ //DATE DEFAULT trunc(sysdate)  NOT NULL,
              ',INSCRICAO_ESTADUAL '+ //VARCHAR2(12) DEFAULT 'ISENTO'  NOT NULL,

              ',CNAE '+ //VARCHAR2(12),
              ',RG '+ //VARCHAR2(10),
              ',ORGAO_EXPEDIDOR_RG '+ //VARCHAR2(10),
              ',E_CLIENTE '+ //CHAR(1) DEFAULT 'N'  NOT NULL,
              ',E_FORNECEDOR '+ //CHAR(1) DEFAULT 'N'  NOT NULL,
              ',ATIVO '+ //CHAR(1) DEFAULT 'S'  NOT NULL,
              ',E_MOTORISTA '+ //CHAR(1) DEFAULT 'N'  NOT NULL,
              ',E_TRANSPORTADORA '+ //CHAR(1) DEFAULT 'N'  NOT NULL,
              ',CHAVE_IMPORTACAO '+ //VARCHAR2(100),
              ',OBSERVACOES ' +//VARCHAR2(800)
              ') values ( '+
              NParametros(30);
    vExec.Add(ins_Sql);

    try
      Conexao_dest.IniciarTransacao;

      MemHist.Lines.Add('');
      j:= StrToInt(Max_Consulta(FConsulta_dest,'cadastro_id',Nome_Tabela));
      Dupl :=0;
      Imp := 0;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vCnpj := TrocaCaracterEspecial(FConsulta_orig.FieldByName('cnpj').AsString,False);
        if Length(vCnpj) = 11 then
          vCnpj := Copy(vCnpj,1,3) + '.' + Copy(vCnpj,4,3) + '.' + Copy(vCnpj,7,3) + '-' + Copy(vCnpj,10,2)
        else
          vCnpj := Copy(vCnpj,1,2) + '.' + Copy(vCnpj,3,3) + '.' + Copy(vCnpj,6,3) + '/' + Copy(vCnpj,9,3) + '-' + Copy(vCnpj,13,2);

        vTp := 'F';
        if Length(vCnpj) > 14 then
          vTp := 'J';

        if TrocaCaracterEspecial(FConsulta_orig.FieldByName('InscricaoEstadual').AsString,false) = '' then
          vInsc := 'ISENTO'
        else
          vInsc := TrocaCaracterEspecial(FConsulta_orig.FieldByName('InscricaoEstadual').AsString,false);

        vSql :='select * from dbo.bairro where bairroid = ' + FConsulta_orig.FieldByName('bairroid').AsString;
        FConsulta_orig01.SQL.Clear;
        FConsulta_orig01.SQL.Add(vSql);
        FConsulta_orig01.Open();

        nome_bairro := uppercase(Tira_Aspas(FConsulta_orig01.FieldByName('bairro').Asstring));

        vSql :='select c.ds_cidade,uf.cd_uf from dbo.cidade c,dbo.UF uf ' +
               'where c.ufID = uf.ufID and cidadeid = ' + FConsulta_orig01.FieldByName('cidadeid').AsString;
        FConsulta_orig01.SQL.Clear;
        FConsulta_orig01.SQL.Add(vSql);
        FConsulta_orig01.Open();

        nome_cidade:= uppercase(Tira_Aspas(FConsulta_orig01.FieldByName('ds_cidade').Asstring));
        uf := FConsulta_orig01.FieldByName('cd_uf').Asstring;

        vSql :='select b.BAIRRO_ID from bairros b,cidades c, estados e ' +
               'where b.CIDADE_ID = c.CIDADE_ID and c.ESTADO_ID = e.ESTADO_ID ' +
               'and b.NOME = ''' + nome_bairro + ''' ' +
               'and c.NOME = ''' + nome_cidade + ''' ' +
               'and c.ESTADO_ID = ''' + uf + ''' ';

        vBairro := Busca_Registro(FConsulta_dest,vSql,'bairro_id',MemErro);
        if vBairro = '' then
          vBairro := '1';

        vAtivo := 'S';

        // verifica na tabela do Altis  se existe
        vSql :='select c.cadastro_id '+
         'from CADASTROS c '+
         'where c.CPF_CNPJ = ''' + vCnpj +'''';

        if not Existe_Registro(FConsulta_dest,vSql,'cadastro_id',MemErro) then
        begin
          j := j +1;
          vexec.Executar([
                          j,
                          Uppercase(FConsulta_orig.FieldByName('fantasia').AsString),
                          Uppercase(FConsulta_orig.FieldByName('razao_social').AsString),
                          vTp,
                          vCnpj,
                          Uppercase(FConsulta_orig.FieldByName('logradouro').AsString),
                          Uppercase(FConsulta_orig.FieldByName('End_Complemento').AsString),
                          FConsulta_orig.FieldByName('numeroini').AsString,
                          vBairro,
                          'PONTO_REFERENCIA',

                          FConsulta_orig.FieldByName('cep').AsString,
                          FConsulta_orig.FieldByName('Telefones').AsString,
                          FConsulta_orig.FieldByName('Celular').AsString,
                          '',
                          copy(FConsulta_orig.FieldByName('Email').AsString,1,80),
                          null,
                          null,
                          null,
                          FConsulta_orig.FieldByName('dt_alt').AsDateTime,
                          vInsc,

                          '',
                          null,
                          null,
                          'N',
                          'S',
                          vAtivo,
                          'N',
                          'N',
                          FConsulta_orig.FieldByName('fornecedorid').AsString,
                          ''
                         ]);

            Imp := Imp +1;
            lblImp.Caption := IntToStr(Imp);
            MemHist.Lines.Add(Uppercase(FConsulta_orig.FieldByName('razao_social').AsString));

        end else
        begin
          Dupl := Dupl + 1 ;
          lblDupl.Caption := IntToStr(Dupl);
        end;
        FConsulta_orig.Next;
      end;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        if not vsp then
        begin
          ShowMessage('Erro ao inserir '+Nome_Tabela+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;
    Conexao_dest.FinalizarTransacao;

    aux_seq := 'SEQ_CADASTRO_ID';
    if Recria_Sequencia(aux_seq,Nome_Tabela,'cadastro_id',FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' realizada com sucesso!';

    Result := True;
    MemErro.Lines.Clear;
  finally
    FConsulta_orig.Free;
    FConsulta_orig01.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;


function Importa_Rotas(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,ins_Sql,aux_seq,Nome_Tabela,vUf:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,id,maxId: Integer;
  vExec:TExecucao;
begin
  Result := False;

  Nome_Tabela := 'rotas';
  if not vsp then
    if MessageDlg('Deseja Importar as '+ Nome_Tabela +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);
  lblDupl.Caption := '0';
  lblImp.Caption  := '0';

  try
    FConsulta_orig.Connection := Conexao_orig;

    MemHist.Lines.Clear;
    MemHist.Lines.Add('Iniciando Importa��o de ' + Nome_Tabela);
    lblHis.Caption :='Importando ' + Nome_Tabela;
    MemHist.Lines.Add('');


    if MessageDlg('Deseja limpar os dados existente?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      MemHist.Lines.Add('Limpando '+Nome_Tabela+'.');
      vSql := 'DELETE FROM '+ Nome_Tabela;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada em '+Nome_Tabela+'.');
    end;

    maxId := strtoint(Max_Consulta(FConsulta_orig,'rota_id',Nome_tabela));

    lblQuant.caption := Num_Registros(0,FConsulta_orig,'rotas',MemErro);

    vSql :='select rota_ID,'+
           'nome '+
           'from '+ Nome_Tabela +' ; ';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);

    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar();

    ins_Sql :='insert into ' + Nome_Tabela +'( '+
            'ROTA_ID,              '+
            'NOME,                 '+
            'ATIVO ) values ( '+
            NParametros(3);
    vExec.Add(ins_Sql);

    MemHist.Lines.Add('');
    try
      Conexao_dest.IniciarTransacao;

      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes';
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vSql :='select rota_id from rotas where NOME = ''' +
               FConsulta_orig.FieldByName('nome').AsString + '''';
        if not Existe_Registro(FConsulta_dest,vSql,'rota_id',MemErro) then
        begin
          vexec.Executar([
                    FConsulta_orig.GetInt('rota_ID'),
                    FConsulta_orig.GetString('nome'),
                    'S']);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(FConsulta_orig.GetString('nome'));
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(FConsulta_orig.GetString('nome'));
        end;

        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir '+Nome_Tabela+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    aux_seq := 'SEQ_ROTA_ID';
    if Recria_Sequencia(aux_seq,Nome_Tabela,'rota_id',FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de '+ Nome_Tabela +' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de '+ Nome_tabela+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de '+Nome_tabela+' realizada com sucesso!';

    MemErro.Lines.Clear;
    Result := True;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;

end;

function Importa_Generico(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,vId,maxId: Integer;
  vExec:TExecucao;
  Existe_Tabela,Existe_Relacionamento:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'xxxxx';
  vNome_Tabela_Orig := 'xxxxx';
  vChave_orig := 'xxxxx';
  vChave_dest := 'xxxxx';
  vNome_seq:= 'xxxxxx';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    maxId := strtoint(Max_Consulta(FConsulta_orig,vChave_orig,vNome_Tabela_orig));

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);

    vSql := 'select * from ' + vNome_Tabela_Orig;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
//              CAMPOS     '+
               ') values ( '+
              NParametros(7);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 1;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes';
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));
        vId := FConsulta_orig.FieldByName(vChave_orig).AsInteger;
        if vId = 1 then
          vId := MaxId + 1;

        vSql_Validacao := '';//'select cidade_id from cidades where NOME = ''' + vNome + ''' and ESTADO_ID = ''' + vId + '''';


        vSql_Validacao :='select ' + vChave_dest +
                         ' from tabela ' +
                         ' where ' + vChave_dest + ' = '+ IntToStr(vId);
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        vSql_Validacao :='select ' + vChave_dest +
                         ' from tabela_relacionada ' +
                         ' where ' + vChave_dest + ' = '+ IntToStr(vId);
        Existe_Relacionamento := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

//        if i = 24520 then
//          lblHis.Caption :='Parada para correcao';

        if (not Existe_Tabela) and Existe_Relacionamento then
        begin
          j:= J +1;
          vexec.Executar([
//                          vId,
//                          vNome,
//                          FConsulta_orig.FieldByName('xx').AsString,
//                          'S',
//                          FConsulta_orig.FieldByName('xx').AsString,
//                          FConsulta_orig.FieldByName('xx').AsString
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';

    MemErro.Lines.Clear;
    Result := True;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Cidades(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,ins_Sql,vCid,aux_seq,Nome_Tabela,vUf:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,vId,maxId: Integer;
  vExec:TExecucao;
begin
  Result := False;

  Nome_Tabela := 'CIDADES';
  if not vsp then
    if MessageDlg('Deseja Importar os '+ Nome_Tabela +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    MemHist.Lines.Clear;
    MemHist.Lines.Add('Iniciando Importa��o de Cidades');
    lblHis.Caption :='Importando Cidades';
    MemHist.Lines.Add('');

    vSql := 'ALTER TABLE CIDADES ' +
            'DROP UNIQUE ( CODIGO_IBGE ) ';
    roda_sql(False,Conexao_dest,vSql,MemErro);

    vSql := 'DROP INDEX CK_CIDADES_CODIGO_IBGE';
    roda_sql(False,Conexao_dest,vSql,MemErro);

    if MessageDlg('Deseja limpar os dados existente?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      MemHist.Lines.Add('Limpando '+Nome_Tabela+'.');
      vSql := 'DELETE FROM '+Nome_Tabela+' where cidade_id > 1';
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada em '+Nome_Tabela+'.');
    end;

    maxId := strtoint(Max_Consulta(FConsulta_orig,'cidade_id','cidades'));

    lblQuant.caption := Num_Registros(0,FConsulta_orig,Nome_Tabela,MemErro);

    vSql :='select cidade_id, '+
           'nome,             '+
           'codigo_fiscal,    '+
           'estado_id,        '+
           '''S'' as ativo,   '+
           'cidade_id as chave_importacao, '+
           'bloquear_entrega_venda '+
           'from cidades order by cidade_id ';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);

    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar();

    ins_Sql :='insert into '+Nome_Tabela+'( '+
              'CIDADE_ID,                   '+
              'NOME,                        '+
              'CODIGO_IBGE,                 '+
              'ESTADO_ID,                   '+
              'ATIVO,                       '+
              'CHAVE_IMPORTACAO,            '+
              'TIPO_BLOQUEIO_VENDA_ENTREGAR) values ( '+
              NParametros(7);
    vExec.Add(ins_Sql);

    try

      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 1;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vUf := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('estado_id').AsString));

        vSql :='select estado_id from estados where estado_id = ''' + vUf + '''';
        if (vUf = '') or not Existe_Registro(FConsulta_dest,vSql,'estado_id',MemErro) then
          vUf := 'GO';

        vCid := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));
        if vCid = '' then
          vCid:= 'GOIANIA';

        vSql :='select cidade_id from cidades where NOME = ''' + vCid + '''' +
               ' and ESTADO_ID = ''' + vUf + '''';

        if i = 0 then
        begin
          MemHist.Lines.Add('Validando Cidades existentes');
          lblHis.Caption :='Validando Cidades existentes';
        end;

        vId := FConsulta_orig.FieldByName('cidade_id').AsInteger;
        if vId = 1 then
          vId := MaxId + 1;

        if not Existe_Registro(FConsulta_dest,vSql,'cidade_id',MemErro) then
        begin
          j:= J +1;
          vexec.Executar([
                          vId,
                          vCid,
                          FConsulta_orig.FieldByName('codigo_fiscal').AsString,
                          vUf,
                          'S',
                          FConsulta_orig.FieldByName('chave_importacao').AsString,
                          FConsulta_orig.FieldByName('BLOQUEAR_ENTREGA_VENDA').AsString]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(vUf + ' - '+ vCid);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(vUf + ' - '+ vCid);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir '+Nome_Tabela+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    aux_seq := 'SEQ_CIDADE_ID';
    if Recria_Sequencia(aux_seq,Nome_Tabela,'cidade_id',FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de Cidades concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de Cidades com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de Cidades realizada com sucesso!';

    MemErro.Lines.Clear;
    Result := True;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Bairros(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela,vChave,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,vId,maxId: Integer;
  vExec:TExecucao;
begin
  Result := False;

  vNome_Tabela := 'BAIRROS';
  vChave := 'bairro_id';
  vNome_seq := 'SEQ_BAIRRO_ID';

  if not vsp then
    if MessageDlg('Deseja Importar os '+ vNome_Tabela +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando ' + vNome_Tabela;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if MessageDlg('Deseja limpar os dados existentes em '+vNome_Tabela+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := ' where BAIRRO_id > 1';
      MemHist.Lines.Add('Limpando '+vNome_Tabela+'.');
      vSql := 'DELETE FROM '+vNome_Tabela +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada em '+vNome_Tabela+'.');
    end;

    maxId := strtoint(Max_Consulta(FConsulta_orig,vChave,vNome_Tabela));

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela,MemErro);

    vSql:= 'select BAIRRO_ID,'+
           'cidade_id, '+
           'nome,      '+
           'ROTA_ID,   '+
           '''S'' as ativo '+
           'from bairros ';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;
    MemErro.Lines.Clear;
    MemErro.Lines.Add(vSql);

    vIns_Sql:='insert into BAIRROS( '+
              'BAIRRO_ID,         '+
              'NOME,              '+
              'CIDADE_ID,         '+
              'ATIVO,             '+
              'ROTA_ID,           '+
              'CHAVE_IMPORTACAO) values ( '+
              NParametros(6);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 1;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));

        vId := FConsulta_orig.FieldByName(vChave).AsInteger;
        if vId = 1 then
          vId := MaxId + 1;

        vSql_Validacao := 'select '+ vChave +' from bairros where nome = ''' + vNome + '''' +
                          ' and cidade_id = ' + FConsulta_orig.FieldByName('CIDADE_ID').Asstring ;

        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes';
          MemHist.Lines.Add(lblHis.Caption);
        end;

        if not Existe_Registro(FConsulta_dest,vSql_Validacao,vChave,MemErro) then  // valida na tabela de destino se existe o registro
        begin
          j:= J +1;
          vexec.Executar([
                          vId,
                          vNome,
                          FConsulta_orig.FieldByName('CIDADE_ID').Asinteger,
                          'S',
                          Iifn(FConsulta_orig.FieldByName('rota_id').Asinteger > 0,FConsulta_orig.FieldByName('rota_id').Asinteger),
                          FConsulta_orig.FieldByName('bairro_id').AsString
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir '+vNome_Tabela+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if vNome_seq <> '' then
      Result := Recria_Sequencia(vNome_seq,vNome_Tabela,vChave,FConsulta_dest,Conexao_dest,lblHis,MemErro);

    if Result then
      lblHis.Caption := 'Importacao de '+vNome_Tabela+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de '+vNome_Tabela+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de ' +vNome_Tabela+ ' realizada com sucesso!';

    MemErro.Lines.Clear;
    Result := True;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

end.
