unit _InsertUpdate;

interface

uses
  _Conexao,_OperacoesBancoDados,Vcl.StdCtrls,System.SysUtils,Vcl.Dialogs,Vcl.forms,_LimpaBaseAltis,
  System.Variants,System.Classes,Data.DB,GridLuka,REST.Backend.EMSServices, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef, FireDAC.Comp.Client,
  REST.Backend.EMSFireDAC, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Phys.SQLiteVDataSet, FireDAC.Comp.DataSet,
  FireDAC.Phys.ODBCBase, FireDAC.VCLUI.Wait, FireDAC.Comp.UI;

function sql_insert(pNome:String):string;
function sql_update(pNome:String):string;

implementation

function sql_insert(pNome:String):string;
var
  ins_Sql:string;
begin
  if UpperCase(pNome) = 'EMPRESAS' then
  BEGIN
    ins_Sql :='insert into EMPRESAS ( '+
              'EMPRESA_ID '+//NUMBER(3,0),
              ',RAZAO_SOCIAL'+// VARCHAR2(60) NOT NULL,
              ',NOME_FANTASIA'+// VARCHAR2(60) NOT NULL,
              ',CNPJ'+// VARCHAR2(19) NOT NULL,
              ',LOGRADOURO'+// VARCHAR2(100) NOT NULL,
              ',COMPLEMENTO'+// VARCHAR2(100) NOT NULL,
              ',NUMERO'+// VARCHAR2(10) DEFAULT 'SN'  NOT NULL,
              ',BAIRRO_ID'+// NUMBER(15,0) NOT NULL,
              ',PONTO_REFERENCIA'+// VARCHAR2(100),
              ',CEP'+// VARCHAR2(9) NOT NULL,

              ',TELEFONE_PRINCIPAL'+// VARCHAR2(15),
              ',TELEFONE_FAX'+// VARCHAR2(15),
              ',E_MAIL'+// VARCHAR2(30),
              ',DATA_FUNDACAO'+// DATE,
              ',INSCRICAO_ESTADUAL'+// VARCHAR2(20) NOT NULL,
              ',INSCRICAO_MUNICIPAL'+// VARCHAR2(20),
              ',CADASTRO_ID'+// NUMBER(10,0),
              ',CNAE'+// VARCHAR2(12),
              ',ATIVO'+// CHAR(1) DEFAULT 'S'  NOT NULL,
              ',TIPO_EMPRESA'+// CHAR(1) DEFAULT 'M'  NOT NULL,

              ',NOME_RESUMIDO_IMPRESSOES_NF'+// VARCHAR2(30),
              ',E_MAIL_NFE'+// VARCHAR2(80),
              ',CPF_ADMINISTRADOR'+// VARCHAR2(14),
              ',ENDERECO_SITE'+// VARCHAR2(50),
              ',NOME_ADMINISTRADOR'+// VARCHAR2(50),

              ') values ( ';//+
//              NParametros(15);
//
//    vExec.Add(ins_Sql);
  END;
end;

function sql_update(pNome:String):string;
var
  up_Sql:string;
begin
  if UpperCase(pNome) = 'EMPRESAS' then
  BEGIN
    up_Sql := ' update empresas set '
  END;
end;
end.
