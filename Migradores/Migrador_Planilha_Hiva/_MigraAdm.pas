unit _MigraAdm;

interface

uses
  _Conexao,_OperacoesBancoDados,Vcl.StdCtrls,System.SysUtils,Vcl.Dialogs,Vcl.forms,_LimpaBaseAltis,
  System.Variants,System.Classes,Data.DB,GridLuka,REST.Backend.EMSServices, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef, FireDAC.Comp.Client,
  REST.Backend.EMSFireDAC, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Phys.SQLiteVDataSet, FireDAC.Comp.DataSet,
  FireDAC.Phys.ODBCBase, FireDAC.VCLUI.Wait, FireDAC.Comp.UI,
  _BibliotecaGenerica,System.UITypes,System.Win.ComObj, System.MaskUtils;

function Importa_Origens(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;

function Importa_Generico  (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
procedure LeArquivo;

function Importa_Cidades   (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Rotas     (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Bairros   (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Cargos    (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Profissionais_Cargos(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Pessoas(
  vsp:Boolean;
  Conexao_orig:Tconexao;
  Conexao_dest:Tconexao;
  lblHis,
  lblQuant,
  lblImp,
  lblDupl:Tlabel;
  MemHist,
  MemErro,
  MemDupl:Tmemo;
  pBairroPadraoId: string
): Boolean;
function Importa_Telefones (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_DIVERSOS_CONTATOS(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_EMAILS           (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_ENDERECOS(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Profissionais    (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Funcionarios     (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_CLIENTES_ORIGENS (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_CLIENTES_GRUPOS(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_GRUPOS_FINANCEIROS(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Clientes  (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Fornecedores   (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Transportadoras(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_GrupoFornecedor(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Motoristas(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Veiculos  (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Empresas  (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Marcas    (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Unidades  (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Grupos_Produtos(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Produtos  (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo; fornecedor_padrao: string; ncm_padrao: string):Boolean;
function Multiplos_Compra  (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_PRODUTOS_Relacionados(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_PRODUTOS_kit(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Produtos_Cod(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Produtos_Lotes(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Precos    (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Custos    (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Promocoes (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Inicia_Estoques   (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Inicia_Estoques_Div (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Estoques  (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Tributacao(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Portadores(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Contas(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Planos_Financeiros(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Centros_Custos(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_CFOP(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Cfop_Cst(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Cfop_Cst1(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Contas_Pagar(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
function Importa_Contas_Receber(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;


function Importa_Pessoas2  (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;

function Importa_icms(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;

function Valida_Data(vConsulta:TConsulta):Variant;
function Troca_Caracter(vStr,p1,p2:string):string;
function getFoto(pConexao: Tconexao; pFuncionarioId: Integer): TMemoryStream;
function Recria_Sequencia(aux_seq,Nome_Tabela,chave:String;FConsulta_dest:TConsulta;Conexao_dest:TConexao;lblHis:TLabel;MemErro:Tmemo):Boolean;
function NParametros(np:integer):string;
function Ajusta_Cnpj(pCnpj:string):string;
function Ajusta_Cep(pCep:string):string;
function Max_Consulta(FConsulta_dest:TConsulta;nCampo,Nome_Tabela:string):string;
function Num_Registros(pMenos:Integer;FConsulta_orig:TConsulta;pNome_Tabela:string;MemErro:Tmemo):string;
function Existe_Registro(FConsulta_dest:TConsulta;vSql,chave:string;MemErro:TMemo):Boolean;
function Busca_Registro(FConsulta_dest:TConsulta;vSql,chave:string;MemErro:TMemo):string;
function Busca_Registro_Origem(FConsulta_Orig:TConsulta;vSql,chave:string;MemErro:TMemo):string;
Function Tira_Aspas(texto : String) : String;
Function RemoveAcentos(Str:String): String;
function TrocaCaracterEspecial(aTexto : string; aLimExt : boolean) : string;
function TrocaCaracter(pValue:Currency) : string;
function Valor( pValor: string ): Double;
function getCNPJ_CPFOk(pCpfCnpj: string): Boolean;

implementation

uses
  Menu_Principal;
const
coId = 0;
coNome = 1;
coValor = 2;
coSql = 3;

type
  RecMarca = record
    Codigo: string;
    Marca : string;
    Ativo : string;
  end;

type
  RecBairro = record
    Bairro    : string;
    Cidade    : string;
    Estado    : string;
    CodIbge   : string;
    Ativo     : string;
  end;

type
  RecCidade = record
    Nome: string;
    EstadoId: string;
    CodIbge: string;
    Ativo: string;
  end;

type
  RecCadastro = record
    Cadastro_id : string;
    Nome        : string;
    Estado      : string;
    Cidade      : string;
    Bairro      : string;
    tp_pessoa   : string;
    endereco    : string;
    cep         : string;
    telefone    : string;
    fax         : string;
    email       : string;
    celular     : string;
    limite_credito  : string;
    data_nascimento : string;
    cpf  : string;
    cnpj : string;
    inscricao  : string;
    numero_end : string;
    plano_financeiro_revenda: string;
    complemento: string;
    ponto_referencia: string;
  end;

type
  RecUnidade = record
    codigo    : string;
    unidade   : string;
    descricao : string;
  end;

type
  RecProduto = record
    codigo    : string;
    cod_marca : string;
    nome      : string;
    unidade   : string;
    tributacao: string;
    peso      : string;
    ncm       : string;
    preco     : Currency;
    custo     : Currency;
    promocao  : Currency;
    estoque   : Double;
    multiplo_venda: Double;
    tipo_def_automatica_lote: string;
    ativo: string;
    codigo_barras: string;
    cest: string;
    fornecedor_id: Integer;
    tributacao_estadual_compra: Integer;
    tributacao_estadual_venda: Integer;
    tributacao_federal_compra: Integer;
    tributacao_federal_venda: Integer;
    codigo_original_fabricante: string;
  end;

function TrocaCaracter(pValue:Currency) : string;
begin
  Result := StringReplace(CurrToStr(pValue), ',', '.', []);
end;


function TrocaCaracterEspecial(aTexto : string; aLimExt : boolean) : string;
const
  //Lista de caracteres especiais
  xCarEsp: array[1..38] of String = ('�', '�', '�', '�', '�','�', '�', '�', '�', '�',
                                     '�', '�','�', '�','�', '�','�', '�',
                                     '�', '�', '�','�', '�','�', '�', '�', '�', '�',
                                     '�', '�', '�','�','�', '�','�','�','�','�');
  //Lista de caracteres para troca
  xCarTro: array[1..38] of String = ('a', 'a', 'a', 'a', 'a','A', 'A', 'A', 'A', 'A',
                                     'e', 'e','E', 'E','i', 'i','I', 'I',
                                     'o', 'o', 'o','o', 'o','O', 'O', 'O', 'O', 'O',
                                     'u', 'u', 'u','u','u', 'u','c','C','n', 'N');
  //Lista de Caracteres Extras
  xCarExt: array[1..48] of string = ('<','>','!','@','#','$','%','�','&','*',
                                     '(',')','_','+','=','{','}','[',']','?',
                                     ';',':',',','|','*','"','~','^','�','`',
                                     '�','�','�','�','�','�','�','�','�','�',
                                     '�','�','�','�','�','�','�','�');
var
  xTexto : string;
  i : Integer;
begin
   xTexto := aTexto;
   for i:=1 to 38 do
     xTexto := StringReplace(xTexto, xCarEsp[i], xCarTro[i], [rfreplaceall]);
   //De acordo com o par�metro aLimExt, elimina caracteres extras.
   if (aLimExt) then
     for i:=1 to 48 do
       xTexto := StringReplace(xTexto, xCarExt[i], '', [rfreplaceall]);
   Result := xTexto;
end;

function Valor( pValor: string ): Double;
begin
  Result := StrToFloat( StringReplace(pValor, '.', ',', [rfReplaceAll]) ) ;
end;

function getCNPJ_CPFOk(pCpfCnpj: string): Boolean;
var
  vNumeros: string;
  vDigito1: string;
  vDigito2: string;

  s: Integer;
  vCont: Integer;
  vDigito: Integer;
  vSoma: Integer;

  function ValidarSequencia: Boolean;
  var
    i: Integer;
    j: Integer;
    s: string;
    vPontoInicial: Integer;
  begin
    Result := True;
    s := vNumeros;
    vPontoInicial := 0;
    if Length(vNumeros) = 11 then
      vPontoInicial := 1;

    for i := vPontoInicial to 9 do begin
      for j := 1 to Length(s) do begin
        if s[j] <> IntToStr(i) then
          Break
        else begin
          if ( s[j] = IntTostr(i) ) and ( j = Length(s) ) then begin
            Result := False;
            Exit;
          end;
        end;
      end;
    end;
  end;

begin
  vNumeros := RetornaNumeros(pCpfCnpj);

  if not ValidarSequencia then begin
    Result := false;
    Exit;
  end;

  case Length(vNumeros) of
    11: begin
      vCont :=1;
      vSoma := 0;
      for S := 9 downto 1 do begin
        vCont := vCont + 1;
        vSoma := vSoma + StrToInt(Copy(vNumeros, s, 1)) * vCont;
      end;
      vSoma := vSoma * 10;
      vDigito1 := IntToStr(vSoma mod 11);

      if SFormatInt(vDigito1) >= 10 then
        vDigito1 := '0';

      vCont := 1;
      vSoma := 0;
      for S := 10 downto 1 do begin
        vCont := vCont + 1;
        vSoma := vSoma + SFormatInt(Copy(vNumeros, s, 1)) * vCont;
      end;
      vSoma := vSoma * 10;
      vDigito2 := IntToStr(vSoma mod 11);
      if SFormatDouble(vDigito2) >= 10 then
        vDigito2 := '0';
    end;

    14: begin
      vSoma :=
        5 * SFormatInt(vNumeros[ 1]) + 4 * SFormatInt(vNumeros[ 2]) + 3 * SFormatInt(vNumeros[3]) +
        2 * SFormatInt(vNumeros[ 4]) + 9 * SFormatInt(vNumeros[ 5]) + 8 * SFormatInt(vNumeros[6]) +
        7 * SFormatInt(vNumeros[ 7]) + 6 * SFormatInt(vNumeros[ 8]) + 5 * SFormatInt(vNumeros[9]) +
        4 * SFormatInt(vNumeros[10]) + 3 * SFormatInt(vNumeros[11]) + 2 * SFormatInt(vNumeros[12]);

      vDigito := vSoma mod 11;
      if vDigito > 1 then
        vDigito := 11 - vDigito
      else
        vDigito := 0;

      vDigito1 := NFormat(vDigito);

      vSoma :=
        6 * SFormatInt(vNumeros[ 1]) + 5 * SFormatInt(vNumeros[ 2]) + 4 * SFormatInt(vNumeros[3]) +
        3 * SFormatInt(vNumeros[ 4]) + 2 * SFormatInt(vNumeros[ 5]) + 9 * SFormatInt(vNumeros[6]) +
        8 * SFormatInt(vNumeros[ 7]) + 7 * SFormatInt(vNumeros[ 8]) + 6 * SFormatInt(vNumeros[9]) +
        5 * SFormatInt(vNumeros[10]) + 4 * SFormatInt(vNumeros[11]) + 3 * SFormatInt(vNumeros[12]) + 2 * SFormatInt(vDigito1);

      vDigito := vSoma mod 11;
      if vDigito > 1 then
        vDigito := 11 - vDigito
      else
        vDigito := 0;

      vDigito2 := NFormat( vDigito );
    end;
  else
    vDigito2 := '999';
  end;
  Result := (Copy(VNumeros, Length(VNumeros)-1, 2) = vDigito1 + vDigito2);
end;

Function RemoveAcentos(Str:String): String;
Const ComAcento = '����������������������������';
  SemAcento = 'aaeouaoaeioucuAAEOUAOAEIOUCU';
Var
x : Integer;
Begin
  For x := 1 to Length(Str) do
  Begin
  if Pos(Str[x],ComAcento)<>0 Then
  begin
  Str[x] := SemAcento[Pos(Str[x],ComAcento)];
  end;
  end;
  Result := Str;
end;

Function Tira_Aspas(texto : String) : String;
Begin
  texto := TrocaCaracterEspecial(texto,false);

  While pos('''', Texto) <> 0 Do
    delete(Texto,pos('''', Texto),1);

  While pos('''', Texto) <> 0 Do
    delete(Texto,pos('''', Texto),1);

  While pos('''', Texto) <> 0 Do
    delete(Texto,pos('''', Texto),1);

 // While pos('/', Texto) <> 0 Do
 //   delete(Texto,pos('/', Texto),1);

  While pos('\', Texto) <> 0 Do
    delete(Texto,pos('\', Texto),1);

  While pos('?', Texto) <> 0 Do
    delete(Texto,pos('?', Texto),1);


  if Texto = 'Y' then
    Texto := '';

  if Texto = 'U' then
    Texto := '';

  if Texto = 'TATIANAAGROJET@IG.COM.BR' then
    Texto := '';

  if Texto = 'TATIANA 0** 22 81259738 JANIO 9720' then
    Texto := '';

  if Texto = 'T' then
    Texto := '';

  if Texto = 'S' then
    Texto := '';

  if Texto = 'SA' then
    Texto := '';

  if Texto = 'SE' then
    Texto := '';

  if Texto = 'R' then
    Texto := '';

  if Texto = 'P' then
    Texto := '';

  if Texto = 'OO' then
    Texto := '';

  if Texto = 'O' then
    Texto := '';

  if Texto = 'N' then
    Texto := '';

  if Texto = 'L' then
    Texto := '';

  if Texto = '..' then
    Texto := '';

  if Texto = '.' then
    Texto := '';

  if Texto = ',' then
    Texto := '';

  if Texto = '0' then
    Texto := '';

  if Texto = '1' then
    Texto := '';

  if Texto = '0800 282 5692  81 2103 5675' then
    Texto := '';

  if Texto = '207' then
    Texto := '';

  if Texto = '22 25229352 FAX' then
    Texto := '';

  if Texto = '249' then
    Texto := '';

  if Texto = '26435490' then
    Texto := '';

  if Texto = '31 3359 0235 FAXCOMPROV. ALESSANDRO' then
    Texto := '';

  if Texto = '92786520' then
    Texto := '';

  if Texto = '31 3359 0235 FAX COMPROV. ALESSANDRO' then
    Texto := '';

  if Texto = '4299167812' then
    Texto := '';

  if Texto = '19 3888 4400 SETOR COMERCIAL' then
    Texto := '';

  if Texto = 'A' then
    Texto := '';
  Result := Texto;
End;

function Existe_Registro(FConsulta_dest:TConsulta;vSql,chave:string;MemErro:TMemo):Boolean;
begin
  Result := False;
  FConsulta_dest.Limpar;
  FConsulta_dest.Add(vSql);
  FConsulta_dest.Pesquisar;

  if FConsulta_dest.GetString(chave) <> '' then
    Result := True;
end;

function Busca_Registro(FConsulta_dest:TConsulta;vSql,chave:string;MemErro:TMemo):string;
begin
  Result := '';
  FConsulta_dest.Limpar;
  FConsulta_dest.Add(vSql);
  FConsulta_dest.Pesquisar;

  Result := FConsulta_dest.GetString(chave);
end;

function Busca_Registro_Origem(FConsulta_Orig:TConsulta;vSql,chave:string;MemErro:TMemo):string;
begin
  Result := '';
  FConsulta_orig.Limpar;
  FConsulta_orig.Add(vSql);
  FConsulta_orig.Pesquisar();

  Result := FConsulta_Orig.FieldByName(chave).AsString;
end;

function Num_Registros(pMenos:Integer;FConsulta_orig:TConsulta;pNome_Tabela:string;MemErro:Tmemo):string;
var
  vSql:string;
begin
  Result := '';
  FConsulta_orig.Limpar;
  vSql :='select count(*) as quantreg ' +
         'from '+ pNome_Tabela;
  FConsulta_orig.Add(vSql);
//  MemErro.Lines.Clear;
//  MemErro.Lines.Add(vSql);
  FConsulta_orig.Pesquisar();
  pMenos := FConsulta_orig.FieldByName('quantreg').AsInteger - pMenos;
  Result := IntToStr(pMenos);
end;

function Max_Consulta(FConsulta_dest:TConsulta;nCampo,Nome_Tabela:string):string;
begin
  Result := '';
  FConsulta_dest.Limpar;
  FConsulta_dest.Add('select max('+nCampo +') as max from ' + Nome_Tabela);
  FConsulta_dest.Pesquisar;
  Result := inttostr(FConsulta_dest.GetInt('max') + 1);
end;

function Ajusta_Cnpj(pCnpj:string):string;
var
  vText : PChar;
  vCnpj : string;
begin
  vText := PChar(pCnpj);
  Result := '';

  while (vText^ <> #0) do
  begin
    {$IFDEF UNICODE}
    if CharInSet(vText^, ['0'..'9']) then
    {$ELSE}
    if vText^ in ['0'..'9'] then
    {$ENDIF}
      Result := Result + vText^;

    Inc(vText);
  end;

  vCnpj := Result;
  if Length(vCnpj) = 11 then
    vCnpj := Copy(vCnpj,1,3) + '.' + Copy(vCnpj,4,3) + '.' + Copy(vCnpj,7,3) + '-' + Copy(vCnpj,10,2)
  else
    vCnpj := Copy(vCnpj,1,2) + '.' + Copy(vCnpj,3,3) + '.' + Copy(vCnpj,6,3) + '/' + Copy(vCnpj,9,4) + '-' + Copy(vCnpj,13,2);

  Result := vCnpj;
end;

function Ajusta_Cep(pCep:string):string;
var
  vText : PChar;
  vCep : string;
begin
  vText := PChar(pCep);
  Result := '';

  while (vText^ <> #0) do
  begin
    {$IFDEF UNICODE}
    if CharInSet(vText^, ['0'..'9']) then
    {$ELSE}
    if vText^ in ['0'..'9'] then
    {$ENDIF}
      Result := Result + vText^;

    Inc(vText);
  end;

  vCep := Result;
  if Length(vCep) = 8 then
    vCep := Copy(vCep,1,2) + '.' + Copy(vCep,3,3) + '-' + Copy(vCep,5,3)
  else
    vCep := '74.000.000';

  Result := vCep;
end;

function NParametros(np:integer):string;
var
  i:Integer;
  aux:string;
begin
  Result := '';
  for i := 1 to np do
  begin
    if i>1 then
      aux := aux + ',';
    aux := aux + ':P'+inttostr(i);
  end;
  Result := aux +')';
end;

function Recria_Sequencia(aux_seq,Nome_Tabela,chave:String;FConsulta_dest:TConsulta;Conexao_dest:TConexao;lblHis:TLabel;MemErro:Tmemo):Boolean;
var
  vSql,vSeq:string;
begin
  if aux_seq = '' then begin
    Result := True;
    exit;
  end;

  try
    vSql := 'select sequence_name from user_sequences where sequence_name = ' + ''''+ aux_seq + '''';
    FConsulta_dest.Limpar;
    FConsulta_dest.Add(vSql);
    if FConsulta_dest.Pesquisar then
    begin
      vSql := 'drop sequence ' + aux_seq;
      lblHis.Caption := 'Limpando sequence '+Nome_Tabela+'.';
      roda_sql(True,Conexao_dest,vSql,MemErro);
      lblHis.Caption := 'Limpando sequence '+Nome_Tabela+'.  *** OK ***';
    end;

    vSeq := Max_Consulta(FConsulta_dest,chave,Nome_Tabela);

    vSql := 'create sequence ' +  aux_seq +
            ' start with '+ vSeq + ' increment by 1 ' +
            'minvalue 1  '+
            'maxvalue 9999999999 ' +
            'nocycle '+
            'nocache '+
            'noorder';
    lblHis.Caption := 'Recriando sequence '+Nome_Tabela+'.';
    roda_sql(True,Conexao_dest,vSql,MemErro);
    lblHis.Caption := 'sequence '+Nome_Tabela+'.  *** OK ***';
    Result := True;
  except
    Result := False;
  end;
end;


function getFoto(pConexao: Tconexao; pFuncionarioId: Integer): TMemoryStream;
var
  p: TConsulta;
  s: TStream;
begin
  Result := nil;

  p := TConsulta.Create(pConexao);
  p.SQL.Add('select FOTO from FOTOS_FUNCIONARIOS where FUNCIONARIO_ID = :P1');

  if p.Pesquisar([pFuncionarioId]) then begin
    if not(p.Fields[0].Value = null) then begin
      Result := TMemoryStream.Create;
      s := p.CreateBlobStream(p.FieldByName('FOTO'), bmRead);
      Result.LoadFromStream(s);
      s.Free;
    end;
  end;

  p.Free;
end;

function Valida_Data(vConsulta:TConsulta):Variant;
begin
  if vConsulta.GetDouble('LIMITE_CREDITO_APROVADO') > 0 then
  begin
//    vLim := FConsulta_orig.GetString('LIMITE_CREDITO_APROVADO');
//    vFunc := '1';  // atacado
    Result := vConsulta.GetData('DATA_APROVACAO');
  end else
  begin
//    vLim := '0';
//    vFunc := '';
    Result := System.Variants.Null;
  end;
end;

function Troca_Caracter(vStr,p1,p2:string):string;
var
  i:Integer;
begin
  Result := '';
  for i := 1 to Length(vStr) do
  begin
    if Copy(vStr,i,1) = 'p1' then
      Result := Result + 'p2'
    else
      Result := Result + Copy(vStr,i,1);
  end;
end;

function Importa_CFOP(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq,
  vCfop_id,vCfop_Ref,vId:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i: Integer;
  vExec:TExecucao;
begin
  Result := False;

  vNome_Tabela_Dest := 'CFOP';
  vNome_Tabela_Orig := 'CFOP';
  vChave_orig := 'CFOP_ID';
  vChave_dest := 'CFOP_ID';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    vSql :='select c.cfop_id,c.nome,c.CFOP_SUGESTAO_CARREGAR_XML_ID, '+
           ' c.PLANO_FINANCEIRO_ENTRADA_ID, nvl(d.descricao,c.nome) descricao,c.ativo ' +
           '  from cfop c,CFOP_DESCRICOES d ' +
           '  where c.cfop_id = d.cfop_id ' +
           '  and length(c.cfop_id) = 5 ' +
           '  order by c.cfop_id ';

    lblQuant.caption := Num_Registros(0,FConsulta_orig,'('+vSql+')',MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+'( '+
              'cfop_id,                     '+
              'NOME,                        '+
              'descricao,                   '+
              'CFOP_CORRESPONDENTE_ENTRADA_ID, ' + //NUMBER(4,0),
              'PLANO_FINANCEIRO_ENTRADA_ID, ' +//VARCHAR2(9),
              'ATIVO ' +
              ') values ( '+
              NParametros(6);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));
        vId := FConsulta_orig.FieldByName(vChave_orig).AsString;

        vCfop_id := copy(FConsulta_orig.GetString('cfop_id'),1,1) +
                    copy(FConsulta_orig.GetString('cfop_id'),3,3);

        vCfop_Ref := copy(FConsulta_orig.GetString('CFOP_SUGESTAO_CARREGAR_XML_ID'),1,1) +
                    copy(FConsulta_orig.GetString('CFOP_SUGESTAO_CARREGAR_XML_ID'),3,3);

        vSql :='select cfop_id from cfop where cfop_id = ''' + vCfop_id + '''';
        if not Existe_Registro(FConsulta_dest,vSql,'cfop_id',MemErro) then
        begin
          vexec.Executar([
                          vCfop_id,
                          FConsulta_orig.GetString('NOME'),
                          FConsulta_orig.GetString('descricao'),
                          '',//vCfop_Ref,
                          FConsulta_orig.GetString('PLANO_FINANCEIRO_ENTRADA_ID'),
                          FConsulta_orig.GetString('ATIVO')
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(vId + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(vId + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;

end;

function Importa_GrupoFornecedor(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,vId,maxId: Integer;
  vExec:TExecucao;
  Existe_Tabela,Existe_Relacionamento:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'grupos_fornecedores';
  vNome_Tabela_Orig := 'grupos_fornecedores';
  vChave_orig := 'GRUPO_FORNECEDORES_ID';
  vChave_dest := 'grupo_id';
  vNome_seq:= 'SEQ_GRUPOS_FORNECEDORES';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    maxId := strtoint(Max_Consulta(FConsulta_orig,vChave_orig,vNome_Tabela_orig));

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql := 'select * from ' + vNome_Tabela_Orig;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'GRUPO_ID '+ // NUMBER(4,0),
               ',DESCRICAO ' +//VARCHAR2(30) NOT NULL,
               ') values ( '+
              NParametros(2);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));
        vId := FConsulta_orig.FieldByName(vChave_orig).AsInteger;
        if vId = 1 then
          vId := MaxId + 1;

        vSql_Validacao :='select ' + vChave_dest +
                         ' from ' + vNome_Tabela_Dest +
                         ' where ' + vChave_dest + ' = '+ IntToStr(vId);
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        Existe_Relacionamento := True;

        if (not Existe_Tabela) and Existe_Relacionamento then
        begin
          vexec.Executar([
                          FConsulta_orig.GetInt('GRUPO_FORNECEDORES_ID'),
                          FConsulta_orig.GetString('NOME')
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Portadores(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq,
  vTipo,vIncid:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,vId: Integer;
  vExec:TExecucao;
  Existe_Tabela:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'PORTADORES';
  vNome_Tabela_Orig := 'PORTADORES';
  vChave_orig := 'PORTADOR_ID';
  vChave_dest := 'PORTADOR_ID';
  vNome_seq:= 'SEQ_PORTADOR_ID';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql := 'select * from ' + vNome_Tabela_Orig;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
                'PORTADOR_ID '+//VARCHAR2(50),
                ',ATIVO '+//CHAR(1) DEFAULT 'S'  NOT NULL,
                ',TIPO '+//CHAR(1) NOT NULL,
                ',INCIDENCIA_FINANCEIRO '+//CHAR(1) NOT NULL,
                ',DESCRICAO '+//VARCHAR2(80) NOT NULL,
                ',INSTRUCAO_BOLETO '+//VARCHAR2(400),' +
                ',EXIGE_NOTA_MOD_55'+//
               ') values ( '+
              NParametros(7);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));
        vId := FConsulta_orig.FieldByName(vChave_orig).AsInteger;

        vSql_Validacao :='select ' + vChave_dest +
                         ' from ' + vNome_Tabela_Dest +
                         ' where ' + vChave_dest + ' = '+ IntToStr(vId);
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        if (not Existe_Tabela) then
        begin

          vTipo := 'C';
          if FConsulta_orig.GetString('FINANCEIRA') = 'S' then
            vTipo := 'B';
          if FConsulta_orig.GetString('DUPLICATA') = 'S' then
            vTipo := 'C';
          if FConsulta_orig.GetString('BOLETA') = 'S' then
            vTipo := 'B';
          if FConsulta_orig.GetString('CHEQUES') = 'S' then
            vTipo := 'C';

          vIncid := '';
          if FConsulta_orig.GetString('CONTAS_PAGAR') = 'S' then
            vIncid :=  'P';
          if FConsulta_orig.GetString('CONTAS_RECEBER') = 'S' then
          begin
            if vIncid = '' then
              vIncid :=  'R'
            else
              vIncid :=  'A';
          end;
          if vIncid = '' then
            vIncid :=  'R';

          vexec.Executar([
                          vId,
                          'S',
                          vTipo,
                          vIncid,
                          FConsulta_orig.GetString('NOME'),
                          FConsulta_orig.GetString('INSTRUCAO_BOLETA_GRAFICA'),
                          FConsulta_orig.GetString('EXIGE_NOTA')
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    vSql :=
      'INSERT ALL ' +
      'INTO TIPOS_COBRANCA(COBRANCA_ID,ATIVO,BADEIRA_RETORNO_TEF,BOLETO_BANCARIO,FORMA_PAGAMENTO,INCIDENCIA_FINANCEIRA, '+
      'NOME,PERCENTUAL_COMISSAO,PERMITIR_PRAZO_MEDIO,PERMITIR_VINCULO_AUT_TEF,PORTADOR_ID,REDE_CARTAO,REDE_RETORNO_TEF, '+
      'TAXA_RETENCAO_MES,TIPO_CARTAO,TIPO_PAGAMENTO_COMISSAO,TIPO_RECEBIMENTO_CARTAO,UTILIZAR_LIMITE_CRED_CLIENTE)      '+
      'VALUES (1,''S'','''',''N'',''DIN'',''A'',''DINHEIRO'',0,''N'',''N'',''9999'',null,null,0,null,''R'',null,''N'')  '+
      'INTO TIPOS_COBRANCA(COBRANCA_ID,ATIVO,BADEIRA_RETORNO_TEF,BOLETO_BANCARIO,FORMA_PAGAMENTO,INCIDENCIA_FINANCEIRA, '+
      'NOME,PERCENTUAL_COMISSAO,PERMITIR_PRAZO_MEDIO,PERMITIR_VINCULO_AUT_TEF,PORTADOR_ID,REDE_CARTAO,REDE_RETORNO_TEF, '+
      'TAXA_RETENCAO_MES,TIPO_CARTAO,TIPO_PAGAMENTO_COMISSAO,TIPO_RECEBIMENTO_CARTAO,UTILIZAR_LIMITE_CRED_CLIENTE)      '+
      'VALUES (2,''S'','''',''N'',''CHQ'',''A'',''CHEQUE A VISTA'',0,''N'',''N'',''9997'',null,null,0,null,''R'',null,''S'') '+
      'INTO TIPOS_COBRANCA(COBRANCA_ID,ATIVO,BADEIRA_RETORNO_TEF,BOLETO_BANCARIO,FORMA_PAGAMENTO,INCIDENCIA_FINANCEIRA,      '+
      'NOME,PERCENTUAL_COMISSAO,PERMITIR_PRAZO_MEDIO,PERMITIR_VINCULO_AUT_TEF,PORTADOR_ID,REDE_CARTAO,REDE_RETORNO_TEF,      '+
      'TAXA_RETENCAO_MES,TIPO_CARTAO,TIPO_PAGAMENTO_COMISSAO,TIPO_RECEBIMENTO_CARTAO,UTILIZAR_LIMITE_CRED_CLIENTE)           '+
      'VALUES (3,''S'','''',''N'',''CHQ'',''A'',''CHEQUE A PRAZO'',0,''N'',''N'',''9998'',null,null,0,null,''R'',null,''S'') '+
      'INTO TIPOS_COBRANCA(COBRANCA_ID,ATIVO,BADEIRA_RETORNO_TEF,BOLETO_BANCARIO,FORMA_PAGAMENTO,INCIDENCIA_FINANCEIRA,      '+
      'NOME,PERCENTUAL_COMISSAO,PERMITIR_PRAZO_MEDIO,PERMITIR_VINCULO_AUT_TEF,PORTADOR_ID,REDE_CARTAO,REDE_RETORNO_TEF,      '+
      'TAXA_RETENCAO_MES,TIPO_CARTAO,TIPO_PAGAMENTO_COMISSAO,TIPO_RECEBIMENTO_CARTAO,UTILIZAR_LIMITE_CRED_CLIENTE)           '+
      'VALUES (4,''S'','''',''N'',''CHQ'',''A'',''CHEQUE DEVOLVIDO'',0,''N'',''N'',''9994'',null,null,0,null,''R'',null,''S'')'+
      'INTO TIPOS_COBRANCA(COBRANCA_ID,ATIVO,BADEIRA_RETORNO_TEF,BOLETO_BANCARIO,FORMA_PAGAMENTO,INCIDENCIA_FINANCEIRA,       '+
      'NOME,PERCENTUAL_COMISSAO,PERMITIR_PRAZO_MEDIO,PERMITIR_VINCULO_AUT_TEF,PORTADOR_ID,REDE_CARTAO,REDE_RETORNO_TEF,       '+
      'TAXA_RETENCAO_MES,TIPO_CARTAO,TIPO_PAGAMENTO_COMISSAO,TIPO_RECEBIMENTO_CARTAO,UTILIZAR_LIMITE_CRED_CLIENTE)            '+
      'VALUES (5,''S'','''',''N'',''FIN'',''R'',''CHEQUE MORADIA'',0,''N'',''N'',''8018'',null,null,0,null,''R'',null,''N'')  '+
      'INTO TIPOS_COBRANCA(COBRANCA_ID,ATIVO,BADEIRA_RETORNO_TEF,BOLETO_BANCARIO,FORMA_PAGAMENTO,INCIDENCIA_FINANCEIRA,       '+
      'NOME,PERCENTUAL_COMISSAO,PERMITIR_PRAZO_MEDIO,PERMITIR_VINCULO_AUT_TEF,PORTADOR_ID,REDE_CARTAO,REDE_RETORNO_TEF,       '+
      'TAXA_RETENCAO_MES,TIPO_CARTAO,TIPO_PAGAMENTO_COMISSAO,TIPO_RECEBIMENTO_CARTAO,UTILIZAR_LIMITE_CRED_CLIENTE)            '+
      'VALUES (6,''S'','''',''S'',''COB'',''A'',''BOLETO BANCARIO'',0,''N'',''N'',''237'',null,null,0,null,''R'',null,''S'')  '+
      'INTO TIPOS_COBRANCA(COBRANCA_ID,ATIVO,BADEIRA_RETORNO_TEF,BOLETO_BANCARIO,FORMA_PAGAMENTO,INCIDENCIA_FINANCEIRA,       '+
      'NOME,PERCENTUAL_COMISSAO,PERMITIR_PRAZO_MEDIO,PERMITIR_VINCULO_AUT_TEF,PORTADOR_ID,REDE_CARTAO,REDE_RETORNO_TEF,       '+
      'TAXA_RETENCAO_MES,TIPO_CARTAO,TIPO_PAGAMENTO_COMISSAO,TIPO_RECEBIMENTO_CARTAO,UTILIZAR_LIMITE_CRED_CLIENTE)            '+
      'VALUES (7,''S'','''',''N'',''COB'',''A'',''DUPLICATA'',0,''N'',''N'',''9999'',null,null,0,null,''R'',null,''S'')       '+
      'INTO TIPOS_COBRANCA(COBRANCA_ID,ATIVO,BADEIRA_RETORNO_TEF,BOLETO_BANCARIO,FORMA_PAGAMENTO,INCIDENCIA_FINANCEIRA,       '+
      'NOME,PERCENTUAL_COMISSAO,PERMITIR_PRAZO_MEDIO,PERMITIR_VINCULO_AUT_TEF,PORTADOR_ID,REDE_CARTAO,REDE_RETORNO_TEF,       '+
      'TAXA_RETENCAO_MES,TIPO_CARTAO,TIPO_PAGAMENTO_COMISSAO,TIPO_RECEBIMENTO_CARTAO,UTILIZAR_LIMITE_CRED_CLIENTE)            '+
      'VALUES (8,''S'','''',''N'',''FIN'',''R'',''FINANCEIRA'',0,''N'',''N'',''8019'',null,null,0,null,''R'',null,''N'')      '+
      'INTO TIPOS_COBRANCA(COBRANCA_ID,ATIVO,BADEIRA_RETORNO_TEF,BOLETO_BANCARIO,FORMA_PAGAMENTO,INCIDENCIA_FINANCEIRA,       '+
      'NOME,PERCENTUAL_COMISSAO,PERMITIR_PRAZO_MEDIO,PERMITIR_VINCULO_AUT_TEF,PORTADOR_ID,REDE_CARTAO,REDE_RETORNO_TEF,       '+
      'TAXA_RETENCAO_MES,TIPO_CARTAO,TIPO_PAGAMENTO_COMISSAO,TIPO_RECEBIMENTO_CARTAO,UTILIZAR_LIMITE_CRED_CLIENTE)            '+
      'VALUES (9,''S'','''',''N'',''COB'',''A'',''PERMUTA'',0,''N'',''N'',''9999'',null,null,0,null,''R'',null,''S'')         '+
      'INTO TIPOS_COBRANCA(COBRANCA_ID,ATIVO,BADEIRA_RETORNO_TEF,BOLETO_BANCARIO,FORMA_PAGAMENTO,INCIDENCIA_FINANCEIRA,       '+
      'NOME,PERCENTUAL_COMISSAO,PERMITIR_PRAZO_MEDIO,PERMITIR_VINCULO_AUT_TEF,PORTADOR_ID,REDE_CARTAO,REDE_RETORNO_TEF,       '+
      'TAXA_RETENCAO_MES,TIPO_CARTAO,TIPO_PAGAMENTO_COMISSAO,TIPO_RECEBIMENTO_CARTAO,UTILIZAR_LIMITE_CRED_CLIENTE)            '+
      'VALUES (10,''S'','''',''N'',''ACU'',''R'',''ACUMULADO'',0,''N'',''N'',''9999'',null,null,0,null,''R'',null,''S'')      '+
      'INTO TIPOS_COBRANCA(COBRANCA_ID,ATIVO,BADEIRA_RETORNO_TEF,BOLETO_BANCARIO,FORMA_PAGAMENTO,INCIDENCIA_FINANCEIRA,       '+
      'NOME,PERCENTUAL_COMISSAO,PERMITIR_PRAZO_MEDIO,PERMITIR_VINCULO_AUT_TEF,PORTADOR_ID,REDE_CARTAO,REDE_RETORNO_TEF,       '+
      'TAXA_RETENCAO_MES,TIPO_CARTAO,TIPO_PAGAMENTO_COMISSAO,TIPO_RECEBIMENTO_CARTAO,UTILIZAR_LIMITE_CRED_CLIENTE)            '+
      'VALUES (11,''S'','''',''N'',''COB'',''A'',''CREDITO'',0,''N'',''N'',''9999'',null,null,0,null,''R'',null,''N'')        '+
      'INTO TIPOS_COBRANCA(COBRANCA_ID,ATIVO,BADEIRA_RETORNO_TEF,BOLETO_BANCARIO,FORMA_PAGAMENTO,INCIDENCIA_FINANCEIRA,       '+
      'NOME,PERCENTUAL_COMISSAO,PERMITIR_PRAZO_MEDIO,PERMITIR_VINCULO_AUT_TEF,PORTADOR_ID,REDE_CARTAO,REDE_RETORNO_TEF,       '+
      'TAXA_RETENCAO_MES,TIPO_CARTAO,TIPO_PAGAMENTO_COMISSAO,TIPO_RECEBIMENTO_CARTAO,UTILIZAR_LIMITE_CRED_CLIENTE)            '+
      'VALUES (12,''S'','''',''N'',''COB'',''A'',''PREVISAO'',0,''N'',''N'',''9999'',null,null,0,null,''R'',null,''N'')       '+
      'INTO TIPOS_COBRANCA(COBRANCA_ID,ATIVO,BADEIRA_RETORNO_TEF,BOLETO_BANCARIO,FORMA_PAGAMENTO,INCIDENCIA_FINANCEIRA,       '+
      'NOME,PERCENTUAL_COMISSAO,PERMITIR_PRAZO_MEDIO,PERMITIR_VINCULO_AUT_TEF,PORTADOR_ID,REDE_CARTAO,REDE_RETORNO_TEF,       '+
      'TAXA_RETENCAO_MES,TIPO_CARTAO,TIPO_PAGAMENTO_COMISSAO,TIPO_RECEBIMENTO_CARTAO,UTILIZAR_LIMITE_CRED_CLIENTE)            '+
      'VALUES (13,''S'','''',''N'',''COB'',''A'',''DEPOSITO EM CONTA'',0,''N'',''N'',''9999'',null,null,0,null,''R'',null,''N'')'+

      'select * from DUAL ';
    roda_sql(True,Conexao_dest,vSql,MemErro);
    if Recria_Sequencia('SEQ_COBRANCA_ID','TIPOS_COBRANCA','COBRANCA_ID',FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      MemHist.Lines.Add('TIPOS DE COBRANCA CADASTRADOS COM SUCESSO!');

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Contas(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq,
  vTipo,vIncid,vId:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i: Integer;
  vExec:TExecucao;
  Existe_Tabela:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'CONTAS';
  vNome_Tabela_Orig := 'CONTAS';
  vChave_orig := 'CONTA_ID';
  vChave_dest := 'CONTA';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    vSql := 'select c.*,p.* from contas c , portadores p ' +
            ' where c.portador_id = p.portador_id ';

    lblQuant.caption := Num_Registros(0,FConsulta_orig,'('+vsql+')',MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
                'ACEITA_SALDO_NEGATIVO ' +//CHAR(1) DEFAULT 'N'  NOT NULL,
                ',AGENCIA ' +//CVARCHAR2(8),
                ',ATIVO ' +//CCHAR(1) DEFAULT 'S'  NOT NULL,
                ',CARTEIRA ' +//CVARCHAR2(3),
                ',CODIGO_BANCO ' +//CCHAR(3),
                ',CODIGO_EMPRESA_COBRANCA ' +//CVARCHAR2(20),
                ',DIGITO_AGENCIA ' +//CVARCHAR2(3),
                ',DIGITO_CONTA ' +//CVARCHAR2(3),
                ',EMITE_BOLETO ' +//CCHAR(1) DEFAULT 'N'  NOT NULL,
                ',EMITE_CHEQUE ' +//CCHAR(1) DEFAULT 'N'  NOT NULL,

                ',EMPRESA_ID ' +//CNUMBER(3,0) NOT NULL,
                ',NOME ' +//CVARCHAR2(60) NOT NULL,
                ',SALDO_ATUAL ' +//CNUMBER(10,2) DEFAULT 0 NOT NULL,
                ',TIPO ' +//CNUMBER(1,0) NOT NULL,
                ',CONTA ' +//CVARCHAR2(8),
                ',LIMITE_CREDITO ' +//CNUMBER(10,2) NOT NULL,
                ',CONTATO ' +//CVARCHAR2(50),
                ',TELEFONE_CONTATO ' +//CVARCHAR2(20),
               ') values ( '+
              NParametros(18);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));
        vId := FConsulta_orig.FieldByName(vChave_orig).AsString;

        vSql_Validacao :='select ' + vChave_dest +
                         ' from ' + vNome_Tabela_Dest +
                         ' where ' + vChave_dest + ' = '''+ vId +'''';
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        if (not Existe_Tabela) then
        begin
          vTipo := '0';
          if FConsulta_orig.GetString('TIPO') = 'B' then
            vTipo := '1';

          vIncid := 'N';
          if vTipo = '1' then
          begin
            vIncid := 'S';
          end;

          vexec.Executar([
                          FConsulta_orig.GetString('PERMITIR_SALDO_NEGATIVO'),
                          FConsulta_orig.GetString('AGENCIA'),
                          FConsulta_orig.GetString('ATIVA'),
                          FConsulta_orig.GetString('CARTEIRA'),
                          FConsulta_orig.GetString('PORTADOR_ID'),  // portador
                          FConsulta_orig.GetString('CODIGO_EMPRESA_COBRANCA'),
                          FConsulta_orig.GetString('DIGITO_VERIFICADOR_AGENCIA'),
                          FConsulta_orig.GetString('DIGITO_VERIFICADOR_CONTA'),
                          FConsulta_orig.GetString('BANCO_EMITE_BOLETO'),
                          'S',

                          FConsulta_orig.GetString('EMPRESA_ID'),
                          FConsulta_orig.GetString('nome'),  // portador
                          FConsulta_orig.GetDouble('SALDO'),
                          '1',
                          FConsulta_orig.GetString('CONTA_ID'),
                          FConsulta_orig.GetDouble('LIMITE_CREDITO'),
                          FConsulta_orig.GetString('CONTATO'),
                          FConsulta_orig.GetString('TELEFONE_CONTATO')
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(vId + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(vId + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Planos_Financeiros(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq,
  vId:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i: Integer;
  vExec:TExecucao;
  Existe_Tabela:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'PLANOS_FINANCEIROS';
  vNome_Tabela_Orig := 'PLANO_FINANCEIRO';
  vChave_orig := 'PLANO_FINANCEIRO_ID';
  vChave_dest := 'PLANO_FINANCEIRO_ID';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql := 'select * from ' + vNome_Tabela_Orig + ' order by PLANO_FINANCEIRO_ID';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
                'PLANO_FINANCEIRO_ID ' +//VARCHAR2(9),
                ',DESCRICAO ' +//VARCHAR2(50) NOT NULL,
                ',PLANO_FINANCEIRO_PAI_ID ' +//VARCHAR2(9),
                ',ULTIMO_SUB_GRUPO ' +//NUMBER(3,0),
                ',TIPO ' +//CHAR(1) DEFAULT 'D'
                ',EXIBIR_DRE ' +//CHAR(1) DEFAULT 'S'  NOT NULL,
               ') values ( '+
              NParametros(6);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));
        vId := FConsulta_orig.FieldByName(vChave_orig).AsString;

        vSql_Validacao :='select ' + vChave_dest +
                         ' from ' + vNome_Tabela_Dest +
                         ' where ' + vChave_dest + ' = '''+ vId +'''';
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        if (not Existe_Tabela) then
        begin
          vexec.Executar([
                          FConsulta_orig.GetString('PLANO_FINANCEIRO_ID'),
                          FConsulta_orig.GetString('NOME'),
                          FConsulta_orig.GetString('GRUPO_PAI'),
                          FConsulta_orig.GetDouble('ULTIMO_SUB_GRUPO'),
                          FConsulta_orig.GetString('TIPO'),
                          FConsulta_orig.GetString('MOSTRAR_FINANC_DEMONST_RESULT')
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(vId + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(vId + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Centros_Custos(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq,
  vId:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i: Integer;
  vExec:TExecucao;
  Existe_Tabela:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'CENTROS_CUSTOS';
  vNome_Tabela_Orig := 'CENTROS_CUSTOS';
  vChave_orig := 'CENTRO_ID';
  vChave_dest := 'CENTRO_CUSTO_ID';
  vNome_seq:= 'SEQ_CENTRO_CUSTO_ID';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql := 'select * from ' + vNome_Tabela_Orig;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
                'CENTRO_CUSTO_ID ' +//NUMBER(3,0),
                ',NOME ' +//VARCHAR2(30) NOT NULL,
                ',ATIVO ' +//CHAR(1) DEFAULT 'S'  NOT NULL,
               ') values ( '+
              NParametros(3);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));
        vId := FConsulta_orig.FieldByName(vChave_orig).AsString;

        vSql_Validacao :='select ' + vChave_dest +
                         ' from ' + vNome_Tabela_Dest +
                         ' where ' + vChave_dest + ' = '''+ vId +'''';
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        if (not Existe_Tabela) then
        begin
          vexec.Executar([
                          FConsulta_orig.GetString('CENTRO_ID'),
                          FConsulta_orig.GetString('NOME'),
                          'S'
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(vId + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(vId + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Cargos(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j: Integer;
  vExec:TExecucao;
begin
  Result := False;

  vNome_Tabela_Dest := 'CARGOS_FUNCIONARIOS';
  vNome_Tabela_Orig := 'CARGOS';
  vChave := 'Cargo_ID';
  vNome_seq:= 'SEQ_CARGO_ID';

  if not vsp then
    if MessageDlg('Deseja Importar os '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os dados existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := ' WHERE CARGO_ID > 1';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    vSql := ' select distinct c.* from cargos c, funcionarios f '+
            'where c.cargo_id = f.cargo_id  AND c.NOME <> ''GERENTE DO SISTEMA''' +
            'order by c.cargo_id ';


    lblQuant.caption := Num_Registros(0,FConsulta_orig,'('+ vSql+ ')',MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;


    vIns_Sql :='insert into '+ vNome_Tabela_Dest+
               '( '+
               ' CARGO_ID '+
               ',NOME '+
               ',ATIVO '+
               ') values ( '+
               NParametros(3);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 1;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.GetString('nome')));

        vSql_Validacao := 'select cargo_id from ' +vNome_Tabela_Dest+
                          ' where NOME = ''' + vNome + '''';

        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        if not Existe_Registro(FConsulta_dest,vSql_Validacao,vChave,MemErro) then
        begin
          j:= J +1;
          vexec.Executar([
                          j,
                          vNome,
                          'S'
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(J) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Profissionais_Cargos(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j: Integer;
  vExec:TExecucao;
begin
  Result := False;

  vNome_Tabela_Dest := 'PROFISSIONAIS_CARGOS';
  vNome_Tabela_Orig := 'CARGOS';
  vChave := 'Cargo_ID';
  vNome_seq:= 'SEQ_PROFISSIONAIS_CARGO_ID';

  if not vsp then
    if MessageDlg('Deseja Importar os '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os dados existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := ' ';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    vSql := ' (select * from cargos '+
            'where cargo_id > 1 '+
            'order by cargo_id) ';

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vSql,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql := 'select * from cargos '+
            'where cargo_id >1    '+
            'order by cargo_id ';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;


    vIns_Sql :='insert into '+ vNome_Tabela_Dest+
               '( '+
               ' CARGO_ID '+
               ',NOME '+
               ',ATIVO '+
               ') values ( '+
               NParametros(3);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 0;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));

        vSql_Validacao := 'select cargo_id from ' +vNome_Tabela_Dest+
                          ' where NOME = ''' + vNome + '''';

        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        if not Existe_Registro(FConsulta_dest,vSql_Validacao,vChave,MemErro) and (vNome <> '') then
        begin
          j:= J +1;
          vexec.Executar([
                          j,
                          vNome,
                          'S'
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(J) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Empresas(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,vId: Integer;
  vExec:TExecucao;
  Existe_Tabela,Existe_Relacionamento:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'EMPRESAS';
  vNome_Tabela_Orig := 'EMPRESAS';
  vChave_orig := 'EMPRESA_ID';
  vChave_dest := 'EMPRESA_ID';
  vNome_seq:= 'SEQ_EMPRESA_ID';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := ' WHERE EMPRESA_ID > 1';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    lblQuant.caption := Num_Registros(1,FConsulta_orig,vNome_Tabela_Orig,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql := 'select * from ' + vNome_Tabela_Orig + ' WHERE EMPRESA_ID > 1';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
                'EMPRESA_ID '+// NUMBER(3,0),
                ',RAZAO_SOCIAL '+//VARCHAR2(60) NOT NULL,
                ',NOME_FANTASIA '+//VARCHAR2(60) NOT NULL,
                ',CNPJ '+//VARCHAR2(19) NOT NULL,
                ',LOGRADOURO '+//VARCHAR2(100) NOT NULL,
                ',COMPLEMENTO '+//VARCHAR2(100) NOT NULL,
                ',NUMERO '+//VARCHAR2(10) DEFAULT 'SN'  NOT NULL,
                ',BAIRRO_ID '+//NUMBER(15,0) NOT NULL,
                ',PONTO_REFERENCIA '+//VARCHAR2(100),
                ',CEP '+//VARCHAR2(9) NOT NULL,

                ',TELEFONE_PRINCIPAL '+//VARCHAR2(15),
                ',TELEFONE_FAX '+//VARCHAR2(15),
                ',E_MAIL '+//VARCHAR2(30),
                ',DATA_FUNDACAO '+//DATE,
                ',INSCRICAO_ESTADUAL '+//VARCHAR2(20) NOT NULL,
                ',INSCRICAO_MUNICIPAL '+//VARCHAR2(20),
                ',CADASTRO_ID '+//NUMBER(10,0),
                ',CNAE '+//VARCHAR2(12),
                ',ATIVO '+//CHAR(1) DEFAULT 'S'  NOT NULL,
                ',TIPO_EMPRESA '+//CHAR(1) DEFAULT 'M'  NOT NULL,

                ',NOME_RESUMIDO_IMPRESSOES_NF '+//VARCHAR2(30),
                ',CNPJ_ESCRITORIO_CONTADOR '+//VARCHAR2(18),
                ',CPF_CONTADOR '+//VARCHAR2(18),
                ',DATA_EMISSAO_CRC_CONTADOR '+//DATE,
                ',DATA_VALIDADE_CRC_CONTADOR '+//DATE,
                ',E_MAIL_CONTADOR '+//VARCHAR2(80),
                ',LOGRADOURO_CONTADOR '+//VARCHAR2(50),
                ',CEP_CONTADOR '+//CHAR(9),
                ',COMPLEMENTO_CONTADOR '+//VARCHAR2(50),
                ',BAIRRO_ID_CONTADOR '+//NUMBER(15,0),

                ',NOME_CONTADOR '+//VARCHAR2(50),
                ',TELEFONE_CONTADOR '+//VARCHAR2(20),
                ',NR_CONTR_EMIS_CRC_CONTADOR '+//VARCHAR2(15),
                ',NUMERO_CRC_CONTADOR '+//VARCHAR2(30),
                ',NUMERO_JUNTA_COMERCIAL '+//VARCHAR2(40),
                ',NOME_ADMINISTRADOR '+//VARCHAR2(50),
                ',FAX_CONTADOR '+//VARCHAR2(20),
                ',CPF_ADMINISTRADOR '+//VARCHAR2(14),
                ',E_MAIL_NFE '+//VARCHAR2(80),
                ',SITE '+//VARCHAR2(50),

               ') values ( '+
              NParametros(40);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('RAZAO_SOCIAL').AsString));
        vId := FConsulta_orig.FieldByName(vChave_orig).AsInteger;

        vSql_Validacao :='select ' + vChave_dest +
                         ' from ' + vNome_Tabela_Dest +
                         ' where ' + vChave_dest + ' = '+ IntToStr(vId);
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        Existe_Relacionamento := True;


        if (not Existe_Tabela) and Existe_Relacionamento then
        begin
          vexec.Executar([
                          FConsulta_orig.GetInt(vChave_orig),
                          FConsulta_orig.GetString('RAZAO_SOCIAL'),
                          FConsulta_orig.GetString('NOME_FANTASIA'),
                          FConsulta_orig.GetString('CNPJ'),
                          FConsulta_orig.GetString('LOGRADOURO'),
                          FConsulta_orig.GetString('COMPLEMENTO'),
                          'SN',
                          FConsulta_orig.GetString('BAIRRO_ID'),
                          'EM FRENTE AO TERMINAL VILA BRASILIA',
                          FConsulta_orig.GetString('CEP'),

                          '(062) 3280-1529',
                          '(062) 3280-1049',
                          FConsulta_orig.GetString('E_MAIL'),
                          FConsulta_orig.GetData('DATA_JUNTA_COMERCIAL'),
                          FConsulta_orig.GetString('INSCRICAO_ESTADUAL'),
                          FConsulta_orig.GetString('INSCRICAO_MUNICIPAL'),
                          FConsulta_orig.GetString('CLIENTE_ID'),
                          FConsulta_orig.GetString('CNAE'),
                          'S',
                          FConsulta_orig.GetString('TIPO_LOJA'),

                          Copy(FConsulta_orig.GetString('NOME_FANTASIA'),1,30),
                          FConsulta_orig.GetString('CNPJ_ESCRITORIO_CONTADOR'),
                          FConsulta_orig.GetString('CPF_CONTADOR'),
                          FConsulta_orig.GetData('DATA_EMISSAO_CRC_CONTADOR'),
                          FConsulta_orig.GetData('DATA_VALIDADE_CRC_CONTADOR'),
                          FConsulta_orig.GetString('E_MAIL_CONTADOR'),
                          FConsulta_orig.GetString('LOGRADOURO_CONTADOR'),
                          FConsulta_orig.GetString('CEP_CONTADOR'),
                          FConsulta_orig.GetString('COMPLEMENTO_CONTADOR'),
                          FConsulta_orig.GetString('BAIRRO_CONTADOR_ID'),

                          FConsulta_orig.GetString('NOME_CONTADOR'),
                          FConsulta_orig.GetString('TELEFONE_CONTADOR'),
                          FConsulta_orig.GetString('NR_CONTR_EMIS_CRC_CONTADOR'),
                          FConsulta_orig.GetString('NUMERO_CRC_CONTADOR'),
                          FConsulta_orig.GetString('NUMERO_JUNTA_COMERCIAL'),
                          FConsulta_orig.GetString('NOME_ADMINISTRADOR'),
                          FConsulta_orig.GetString('FAX_CONTADOR'),
                          FConsulta_orig.GetString('CPF_ADMINISTRADOR'),
                          FConsulta_orig.GetString('E_MAIL_NFE'),
                          FConsulta_orig.GetString('SITE')
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;

      vSql := 'INSERT INTO PARAMETROS_EMPRESA(EMPRESA_ID, UTILIZA_NFE, SERIE_NFE, ' +
              '      NUMERO_ULT_NFE_EMITIDA, UTILIZA_NFCE, SERIE_NFCE, NUMERO_ULT_NFCE_EMITIDA, '+
              '      EXIGIR_NOME_CONS_FINAL_VENDA, PERMITE_ALT_FORMA_PAGTO_CAIXA, ' +
              '      PERM_ALT_FORMA_PAGTO_CX_FINANC, CONDICAO_PAGAMENTO_PDV, COND_PAGTO_PADRAO_PESQ_VENDA, ' +
              '      ULTIMO_NSU_CONSULTADO_DIST_NFE, COR_LUCRATIVIDADE_1, COR_LUCRATIVIDADE_2, ' +
              '      COR_LUCRATIVIDADE_3, COR_LUCRATIVIDADE_4, REGIME_TRIBUTARIO, ' +
              '      ALIQUOTA_SIMPLES_NACIONAL, ALIQUOTA_PIS, ALIQUOTA_COFINS, ALIQUOTA_CSLL, ' +
              '      ALIQUOTA_IRPJ, TIPO_AMBIENTE_NFE, TIPO_AMBIENTE_NFCE, INF_COMPL_DOCS_ELETRONICOS, ' +
              '      TRABALHA_RETIRAR_ATO, TRABALHA_RETIRAR, TRABALHA_ENTREGAR, TRABALHA_SEM_PREVISAO, '+
              '      CONFIRMAR_SAIDA_PRODUTOS, CONDICAO_PAGTO_PAD_VENDA_ASSIS, ' +
              '      CONDICAO_PAGAMENTO_CONS_PROD_2, CONDICAO_PAGAMENTO_CONS_PROD_3, '+
              '      CONDICAO_PAGAMENTO_CONS_PROD_4, INDICE_SABADO, INDICE_DOMINGO, TIPO_PRECO_UTILIZAR_VENDA) '+
              '      VALUES ( 2, ''N'', NULL, 0, ''N'', NULL, 0, ''S'', ''S'', ''S'', NULL, NULL, ''0'', ' +
              ' 8388608, 32768, 65280, 65535, ''LP'', NULL, 0, 0, 0, 0, ''H'', ''H'', NULL, ''S'', ' +
              ' ''S'', ''S'', ''S'', ''S'', NULL, NULL, NULL, NULL, 0.5, 0, ''APA'' ';
      roda_sql(True,Conexao_dest,vSql,MemErro);
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Motoristas(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao,vCnh:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,vId: Integer;
  vExec:TExecucao;
  vData:Variant;
  Existe_Tabela,Existe_Relacionamento:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'MOTORISTAS';
  vNome_Tabela_Orig := 'MOTORISTAS';
  vChave_orig := 'CADASTRO_ID';
  vChave_dest := 'CADASTRO_ID';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql := 'select * from ' + vNome_Tabela_Orig;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               ' CADASTRO_ID ' + //NUMBER(10,0),
               ', NUMERO_CNH ' + //VARCHAR2(15) NOT NULL,
               ', DATA_VALIDADE_CNH ' + //DATE NOT NULL,
               ', DATA_HORA_CADASTRO ' + //DATE NOT NULL,
               ', ATIVO ' +
               ') values ( '+
              NParametros(5);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('CADASTRO_ID').AsString));
        vId := FConsulta_orig.FieldByName(vChave_orig).AsInteger;

        vSql_Validacao :='select ' + vChave_dest +
                         ' from MOTORISTAS ' +
                         ' where ' + vChave_dest + ' = '+ IntToStr(vId);
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        vSql_Validacao :='select ' + vChave_dest +
                         ' from CADASTROS ' +
                         ' where ' + vChave_dest + ' = '+ IntToStr(vId);
        Existe_Relacionamento := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        if (not Existe_Tabela) and Existe_Relacionamento then
        begin
          vCnh := FConsulta_orig.GetString('CNH');
          if vCnh = '' then
            vCnh := '123';

          if FConsulta_orig.GetString('DATA_VALIDADE_CNH') = '' then
            vData := 'trunc(sysdate)'
          else
            vData := FConsulta_orig.GetData('DATA_VALIDADE_CNH');
          vexec.Executar([
                         vId,
                         vCnh,
                         vData,
                         FConsulta_orig.GetData('DATA_HORA_ALTERACAO'),
                         FConsulta_orig.GetString('ATIVO')
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Veiculos(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j: Integer;
  vExec:TExecucao;
  Existe_Relacionamento:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'veiculos';
  vNome_Tabela_Orig := 'veiculos';
  vChave_orig := 'PLACA_VEICULO_ID';
  vChave_dest := 'veiculo_id';
  vNome_seq:= 'SEQ_VEICULO_ID';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql := 'select * from ' + vNome_Tabela_Orig;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'VEICULO_ID '+//NUMBER(5,0),
               ', MOTORISTA_ID '+//NUMBER(10,0) NOT NULL,
               ', NOME '+//VARCHAR2(40) NOT NULL,
               ', PLACA '+//VARCHAR2(8) NOT NULL,
               ', ANO '+//NUMBER(4,0),
               ', TIPO_COMBUSTIVEL '+//CHAR(1) DEFAULT 'G'  NOT NULL,
               ', CHASSSI '+//VARCHAR2(20),
               ', COR '+//VARCHAR2(15),
               ', TIPO_VEICULO '+//CHAR(3) DEFAULT 'CAR'  NOT NULL,
               ', PESO_MAXIMO '+//NUMBER(5,0) DEFAULT 0 NOT NULL,
               ', TARA '+//NUMBER(5,0) DEFAULT 0 NOT NULL,
               ', ATIVO '+//CHAR(1) DEFAULT 'S'  NOT NULL,
               ', MARCA '+//VARCHAR2(30),
               ', DESCRICAO '+//VARCHAR2(500),
               ', CODIGO_ANTT '+//VARCHAR2(20),
               ') values ( '+
              NParametros(15);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 0;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));
        Existe_Relacionamento := True;
//        vSql_Validacao :='select ' + vChave_dest +
//                         ' from MOTORISTAS ' +
//                         ' where ' + vChave_dest + ' = '+ IntToStr(vId);
//        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);
//
//        vSql_Validacao :='select ' + vChave_dest +
//                         ' from CADASTROS ' +
//                         ' where ' + vChave_dest + ' = '+ IntToStr(vId);
//        Existe_Relacionamento := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        if Existe_Relacionamento then
        begin
          j:= J +1;
          vexec.Executar([
                          j,
                          FConsulta_orig.GetInt('cadastro_id'),
                          FConsulta_orig.GetString('NOME')  ,
                          FConsulta_orig.GetString('PLACA_VEICULO_ID'),
                          1900,
                          'D',
                          '',
                          '',
                          'CAM',
                          FConsulta_orig.GetDouble('PESO_MAXIMO') ,
                          FConsulta_orig.GetDouble('TARA'),
                          FConsulta_orig.GetString('ATIVO'),
                          FConsulta_orig.GetString('MARCA'),
                          FConsulta_orig.GetString('DESCRICAO'),
                          FConsulta_orig.GetString('CODIGO_ANTT')
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(J) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Grupos_Produtos(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq:string;
  vPai,vChave:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,vId,maxId: Integer;
  vExec:TExecucao;
  Existe_Tabela,Existe_Relacionamento:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'PRODUTOS_DEPTOS_SECOES_LINHAS';
  vNome_Tabela_Orig := 'GRUPOS_PRODUTOS';
  vChave_orig := 'GRUPO_PRODUTOS_ID';
  vChave_dest := 'DEPTO_SECAO_LINHA_ID';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql := 'select * from ' + vNome_Tabela_Orig + ' ORDER BY GRUPO_PRODUTOS_ID';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'DEPTO_SECAO_LINHA_ID ' +//VARCHAR2(11),
               ',NOME ' +//VARCHAR2(60) NOT NULL,
               ',DEPTO_SECAO_LINHA_PAI_ID ' +// VARCHAR2(11),
               ',ATIVO ' +// CHAR(1) DEFAULT 'S'  NOT NULL,
               ',COMISSAO_VENDA_PRAZO ' +// number(6,4) default 0 not null,
               ',COMISSAO_VENDA_VISTA ' + //number(6,4) default 0 not null,
               ',GRUPO_PRODUTOS_WEB_ID ' + // varchar2(17),
               ',PERC_COMISSAO_PROFISSIONAL ' + // number(10,4) default 0 not null,
               ',PERC_MARG_PRETEND_PRECO_VAR ' + // number(5,2) default 0 not null,
               ',PERC_MARG_PRETEND_PRECO_AT1 ' + // number(5,2) default 0 not null,

               ',PERC_MARG_PRETEND_PRECO_AT2 ' + // number(5,2) default 0 not null,
               ',PERC_MARG_PRETEND_PRECO_AT3 ' + // number(5,2) default 0 not null,
               ',PERC_MARG_PRETEND_PRECO_PON ' + // number(5,2) default 0 not null
               ') values ( '+
              NParametros(13);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));

        vChave := '';
        vPai   := '';
        if Length(FConsulta_orig.GetString(vChave_orig)) = 3 then
          vChave := FConsulta_orig.GetString(vChave_orig);

        if Length(FConsulta_orig.GetString(vChave_orig)) = 12 then
          vChave := Copy(FConsulta_orig.GetString(vChave_orig),1,4) +
                    Copy(FConsulta_orig.GetString(vChave_orig),10,3);

        if Length(FConsulta_orig.GetString(vChave_orig)) = 17 then
          vChave := Copy(FConsulta_orig.GetString(vChave_orig),1,4) +
                    Copy(FConsulta_orig.GetString(vChave_orig),10,4) +
                    Copy(FConsulta_orig.GetString(vChave_orig),15,3);

        if Length(vChave) > 3 then
          vPai := Copy(vChave,1,3);

        vSql_Validacao :='select ' + vChave_dest +
                         ' from ' + vNome_Tabela_Dest +
                         ' where ' + vChave_dest + ' = '''+ vChave + '''';
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);
        if vChave = '' then
          Existe_Relacionamento := False
        else
          Existe_Relacionamento := True;

        if (not Existe_Tabela) and Existe_Relacionamento then
        begin
          vexec.Executar([
                         vChave,
                         vNome,
                         vPai,
                         'S',
                         FConsulta_orig.GetDouble('COMISSAO_VISTA'),
                         FConsulta_orig.GetDouble('COMISSAO_PRAZO'),
                         FConsulta_orig.GetString('GRUPO_PRODUTOS_WEB_ID'),
                         FConsulta_orig.GetDouble('PERC_COMISSAO_PROFISSIONAL'),
                         FConsulta_orig.GetDouble('PERC_MARGEM_PRETENDIDA'),
                         0,

                         0,
                         0,
                         0
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(vChave + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(FConsulta_orig.GetString(vChave_orig) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Unidades(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vNome,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vChave_dest,vNome_seq:string;
  FConsulta_dest : TConsulta;
  i: Integer;
  vExec:TExecucao;
  Existe_Tabela:Boolean;
  Funidade:TArray<RecUnidade>;
  sheet: OleVariant;
  vPlanilha: OleVariant;
begin
  Result := False;

  Funidade := nil;
  //Crio o objeto que gerencia o arquivo excel
  vPlanilha:= CreateOleObject('Excel.Application');

  //Abro o arquivo
  if FrmMenuPrincipal.edtArq.Text = '' then
  begin
    ShowMessage('Informe o Caminho da Planilha!' );
    Abort;
  end;
  vPlanilha.WorkBooks.open(FrmMenuPrincipal.edtArq.Text);

  //Pego a primeira vPlanilha do arquivo
  sheet := vPlanilha.WorkSheets[1];

  i := 2;
  lblQuant.caption := '0';
  lblImp.caption   := '0';
  lblDupl.caption  := '0';
  while Trim(sheet.Cells[i, 2]) <> '' do begin
    SetLength(Funidade, Length(Funidade) + 1);
    Funidade[High(Funidade)].Codigo  := sheet.cells[i, 1].Value;
    Funidade[High(Funidade)].descricao := sheet.cells[i, 2].Value;
    Funidade[High(Funidade)].unidade   := sheet.cells[i, 3].Value;

    Inc(i);

    lblQuant.caption := NFormat( Length(Funidade) );
    lblQuant.Refresh;
  end;

  //Fecho a vPlanilha
  vPlanilha.WorkBooks.Close;

  vNome_Tabela_Dest := 'Unidades';
  vChave_dest := 'UNIDADE_ID';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');


    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'UNIDADE_ID ' +// VARCHAR2(3),
               ',DESCRICAO ' +//VARCHAR2(40) NOT NULL,
               ',ATIVO ' +//CHAR(1) DEFAULT 'S'  NOT NULL,
               ',CHAVE_IMPORTACAO ' +
               ') values ( '+
              NParametros(4);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to Length(Funidade) -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Funidade[i].unidade);

        vSql_Validacao :='select ' + vChave_dest +
                         ' from ' + vNome_Tabela_Dest +
                         ' where UNIDADE_ID = ''' + vNome + ''' ';
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        if (not Existe_Tabela) then
        begin
          vexec.Executar([
                          vNome,
                          Funidade[i].descricao,
                          'S',
                          Funidade[i].codigo
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(Funidade[i].codigo + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(Funidade[i].codigo + ' - '+ vNome);
        end;

      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;

  finally
    FConsulta_dest.Free;
    vExec.Free;
  end;

end;

function Importa_icms(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,ins_Sql,Nome_Tabela,vPRODUTO_ID,vEMPRESA_ID,vUF,vcd_cst_lucro:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,Imp,Dupl: Integer;
  vExec:TExecucao;
begin
  Nome_Tabela := 'produtos_icms';
  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);
  lblDupl.Caption := '0';

  try
    FConsulta_orig.Connection := Conexao_orig;
    Application.ProcessMessages;

    vExec     := TExecucao.Create(Conexao_dest);

    vSql := 'DELETE FROM '+Nome_Tabela+' ';
    roda_sql(True,Conexao_dest,vSql,MemErro);
    MemHist.Lines.Clear;
    lblHis.Caption := 'Iniciando Importa��o de ' + Nome_Tabela;
    MemHist.Lines.Add('Iniciando Importa��o de ' + Nome_Tabela);
    MemHist.Lines.Add('');
    MemHist.Lines.Add('Limpando '+Nome_Tabela+'.');

    vSql :=' dbo.produto_cf pc, dbo.ClassificacaoFiscal f, dbo.Produto p, dbo.uf uf  ' +
           'where pc.cfID= f.cfID and p.produtoID = pc.produtoID ' +
           ' and pc.ufID = uf.ufID and cd_uf = ''GO'''  +
           ' and f.ContribuinteIcms = 0  ';

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vSql,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql :='select p.produtoID,f.cd_cst_lucro,f.al_icms,uf.cd_uf ' +
           'from dbo.produto_cf pc, dbo.ClassificacaoFiscal f, dbo.Produto p, dbo.uf uf  ' +
           'where pc.cfID= f.cfID and p.produtoID = pc.produtoID ' +
           ' and pc.ufID = uf.ufID and cd_uf = ''GO'''  +
           ' and f.ContribuinteIcms = 0  ';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);

//    MemErro.Lines.Clear;
//    MemErro.Lines.Add(vSql);
    FConsulta_orig.Pesquisar();

    ins_Sql :='insert into '+Nome_Tabela+'( '+
              'PRODUTO_ID ' +//NUMBER(10,0),
              ',EMPRESA_ID ' +//NUMBER(3,0),
              ',ESTADO_ID ' +//CHAR(2),
              ',CST_NAO_CONTRIBUINTE ' +//VARCHAR2(3) NOT NULL,
              ',CST_CONTRIBUINTE ' +//VARCHAR2(3) NOT NULL,
              ',CST_ORGAO_PUBLICO ' +//VARCHAR2(3) NOT NULL,
              ',CST_REVENDA ' +//VARCHAR2(3) NOT NULL,
              ',CST_CONSTRUTORA ' +//VARCHAR2(3) NOT NULL,
              ',CST_CLINICA_HOSPITAL ' +//VARCHAR2(3) NOT NULL,
              ',PERCENTUAL_ICMS ' +//NUMBER(4,2) NOT NULL,
              ',CST_PRODUTOR_RURAL ' +//VARCHAR2(3) NOT NULL,
              ') values ( '+
              NParametros(11);
    vExec.Add(ins_Sql);

    Conexao_dest.IniciarTransacao;
    try
      MemHist.Lines.Add('');
      Dupl :=0;
      Imp := 0;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vUF := 'GO';
        vcd_cst_lucro := FConsulta_orig.FieldByName('cd_cst_lucro').AsString;
        if Length(vcd_cst_lucro) > 2 then
          vcd_cst_lucro := Copy(vcd_cst_lucro,2,2);
        vSql := 'select produto_id from produtos where chave_importacao = ''' +
                 FConsulta_orig.FieldByName('produtoID').AsString + '''';
        vPRODUTO_ID := Busca_Registro(FConsulta_dest,vSql,'produto_id',MemErro);

        if vPRODUTO_ID <> '' then
        begin
          vSql :='select produto_id from produtos_icms where produto_id = ''' + vPRODUTO_ID + '''';

          if not Existe_Registro(FConsulta_dest,vSql,'produto_id',MemErro) then
          begin
            vEMPRESA_ID := '1';

            vexec.Executar([
                            vPRODUTO_ID,
                            vEMPRESA_ID,
                            vUF,
                            vcd_cst_lucro,
                            vcd_cst_lucro,
                            vcd_cst_lucro,
                            vcd_cst_lucro,
                            vcd_cst_lucro,
                            vcd_cst_lucro,
                            FConsulta_orig.FieldByName('al_icms').AsCurrency,
                            vcd_cst_lucro
                             ]);

            Imp := Imp +1;
            lblImp.Caption := IntToStr(Imp);
            MemHist.Lines.Add(vPRODUTO_ID);
          end else
          begin
            Dupl := Dupl + 1 ;
            lblDupl.Caption := IntToStr(Dupl);
          end;
        end;
        FConsulta_orig.Next;
      end;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        if not vsp then
        begin
          ShowMessage('Erro ao inserir '+Nome_Tabela+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;
    Conexao_dest.FinalizarTransacao;

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' realizada com sucesso!';

    Result := True;

//    MemErro.Lines.Clear;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Produtos_Cod(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,vId: Integer;
  vExec:TExecucao;
  Existe_Tabela,Existe_Relacionamento:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'PRODUTOS_CODIGOS_BARRAS_AUX';
  vNome_Tabela_Orig := 'PRODUTOS_CODIGOS_AUXILIARES';
  vChave_orig := 'PRODUTO_ID';
  vChave_dest := 'PRODUTO_ID';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql := 'select * from ' + vNome_Tabela_Orig;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'PRODUTO_ID '+//             number(10) not null,
               ',CODIGO_BARRAS '+//          varchar2(14) not null,
               ') values ( '+
              NParametros(2);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;

      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := FConsulta_orig.GetString('CODIGO_BARRAS');
        vId := FConsulta_orig.Getint(vChave_orig);

        vSql_Validacao :='select ' + vChave_dest +
                         ' from ' + vNome_Tabela_Dest +
                         ' where ' + vChave_dest + ' = '+ IntToStr(vId);
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        if vNome <> '' then
          Existe_Relacionamento := True
        else
          Existe_Relacionamento := False;

//        if i = 24520 then
//          lblHis.Caption :='Parada para correcao';

        if (not Existe_Tabela) and Existe_Relacionamento then
        begin
          vexec.Executar([
                          vId,
                          vNome
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Produtos_lotes(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,vId: Integer;
  vExec:TExecucao;
  Existe_Tabela:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'PRODUTOS_LOTES';
  vNome_Tabela_Orig := 'LOTES';
  vChave_orig := 'PRODUTO_ID';
  vChave_dest := 'PRODUTO_ID';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    vSql := 'select * from ' + vNome_Tabela_Orig + ' where ativo = ''S''';

    lblQuant.caption := Num_Registros(0,FConsulta_orig,'('+vSql+')',MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'PRODUTO_ID '+//NUMBER(10,0),
               ',LOTE '+//VARCHAR2(80),
               ',DATA_VENCIMENTO '+//DATE,
               ',DATA_FABRICACAO '+//DATE,
               ') values ( '+
              NParametros(4);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := FConsulta_orig.GetString('LOTE');
        if vNome = '?' then
          vNome := '???' ;
        vId := FConsulta_orig.GetInt(vChave_orig);

        vSql_Validacao :='select ' + vChave_dest +
                         ' from ' + vNome_Tabela_Dest +
                         ' where ' + vChave_dest + ' = '+ IntToStr(vId) +  ' and lote = ''' + vNome + '''';
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        if not Existe_Tabela then
        begin
          vexec.Executar([
                          vId,
                          vNome,
                          FConsulta_orig.GetData('VENCIMENTO'),
                          FConsulta_orig.GetData('DATA_FABRICACAO')
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Precos(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vSql_Validacao,vNome,vIns_Sql:string;
  vNome_Tabela_Dest,vNome_seq,vChave_dest,vId:string;
  FConsulta_dest : TConsulta;
  i: Integer;
  vExec:TExecucao;
  Existe_Tabela:Boolean;
  vPercL02,vPercL03:Currency;
  Fproduto:TArray<RecProduto>;
  sheet: OleVariant;
  vPlanilha: OleVariant;
begin
  Result := False;

  Fproduto := nil;
  //Crio o objeto que gerencia o arquivo excel
  vPlanilha:= CreateOleObject('Excel.Application');

  //Abro o arquivo
  if FrmMenuPrincipal.edtArq.Text = '' then
  begin
    ShowMessage('Informe o Caminho da Planilha!' );
    Abort;
  end;
  vPlanilha.WorkBooks.open(FrmMenuPrincipal.edtArq.Text);

  //Pego a primeira vPlanilha do arquivo
  sheet := vPlanilha.WorkSheets[1];

  i := 2;
  lblQuant.caption := '0';
  lblImp.caption   := '0';
  lblDupl.caption  := '0';
  while Trim(sheet.Cells[i, 1]) <> '' do begin
    SetLength(Fproduto, Length(Fproduto) + 1);
    Fproduto[High(Fproduto)].Codigo     := sheet.cells[i, 1].Value;
    Fproduto[High(Fproduto)].preco      := sheet.cells[i, 2].Value;
    Fproduto[High(Fproduto)].custo      := sheet.cells[i, 3].Value;

    Inc(i);

    lblQuant.caption := NFormat( Length(Fproduto) );
    lblQuant.Refresh;
  end;

  //Fecho a vPlanilha
  vPlanilha.WorkBooks.Close;

  vNome_Tabela_Dest := 'PRECOS_PRODUTOS';
  vChave_dest := 'produto_id';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    vIns_Sql :=
      'update ' + vNome_Tabela_Dest + ' set ' +
      '  EMPRESA_ID = :P2,' +
      '  PERC_MARGEM_PRECO_VAREJO = :P3, ' +
      '  PRECO_VAREJO = :P4, ' +
      '  PERC_MARGEM_PRECO_PDV = :P5, ' +
      '  PRECO_PDV = :P6 ' +
      'where PRODUTO_ID = :P1 ';
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to Length(Fproduto) -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vSql_Validacao :='select produto_id ' +
                         ' from produtos ' +
                         ' where chave_importacao = ''' + Fproduto[i].codigo + ''' ';

        vId := Busca_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        if (vId <> '') then
        begin
          vexec.Executar([
                          vId,
                          1,
                          0,
                          Fproduto[i].preco,
                          0,
                          Fproduto[i].preco
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(Fproduto[i].codigo + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(Fproduto[i].codigo + ' - '+ vNome);
        end;

      end;

      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Custos(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vId:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vSql_Validacao:string;
  FConsulta_dest : TConsulta;
  i: Integer;
  vExec:TExecucao;
  Existe_Relacionamento,Existe_Tabela:Boolean;
  pAdic:Double;
  Fproduto:TArray<RecProduto>;
  sheet: OleVariant;
  vPlanilha: OleVariant;
begin
  Result := False;

  Fproduto := nil;
  //Crio o objeto que gerencia o arquivo excel
  vPlanilha:= CreateOleObject('Excel.Application');

  //Abro o arquivo
  if FrmMenuPrincipal.edtArq.Text = '' then
  begin
    ShowMessage('Informe o Caminho da Planilha!' );
    Abort;
  end;
  vPlanilha.WorkBooks.open(FrmMenuPrincipal.edtArq.Text);

  //Pego a primeira vPlanilha do arquivo
  sheet := vPlanilha.WorkSheets[1];

  i := 2;
  lblQuant.caption := '0';
  lblImp.caption   := '0';
  lblDupl.caption  := '0';
  while Trim(sheet.Cells[i, 1]) <> '' do begin
    SetLength(Fproduto, Length(Fproduto) + 1);
    Fproduto[High(Fproduto)].Codigo     := sheet.cells[i, 1].Value;
    Fproduto[High(Fproduto)].custo      := sheet.cells[i, 2].Value;

    Inc(i);

    lblQuant.caption := NFormat( Length(Fproduto) );
    lblQuant.Refresh;
  end;

  //Fecho a vPlanilha
  vPlanilha.WorkBooks.Close;
  vNome_Tabela_Dest := 'CUSTOS_PRODUTOS';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    vIns_Sql :=
       'update ' + vNome_Tabela_Dest + ' set ' +
       '  EMPRESA_ID = :P1' +//NUMBER(3,0),
       ', PRECO_TABELA = :P2 ' +//NUMBER(9,4) DEFAULT 0 NOT NULL,
       ', CUSTO_PEDIDO_MEDIO = :P3 ' +//NUMBER(9,4) DEFAULT 0 NOT NULL,
       ', CUSTO_PEDIDO_MEDIO_ANTERIOR = :P4 ' +//NUMBER(9,4) DEFAULT 0 NOT NULL,
       ', CUSTO_ULTIMO_PEDIDO = :P5 ' +//NUMBER(9,4) DEFAULT 0 NOT NULL,
       ', PRECO_FINAL = :P6 ' +//NUMBER(9,4) DEFAULT 0 NOT NULL,
       ', PRECO_FINAL_MEDIO = :P7 ' +//NUMBER(9,4) DEFAULT 0 NOT NULL,
       ', PRECO_FINAL_ANTERIOR = :P8 ' +//NUMBER(9,4) DEFAULT 0 NOT NULL,
       ', PRECO_LIQUIDO = :P9 ' +//NUMBER(9,4) DEFAULT 0 NOT NULL,
       ', PRECO_LIQUIDO_MEDIO = :P10 ' +//NUMBER(9,4) DEFAULT 0 NOT NULL,
       ', PRECO_LIQUIDO_ANTERIOR = :P11 ' +//NUMBER(9,4) DEFAULT 0 NOT NULL,
       ', CMV = :P12 ' +//NUMBER(9,4) DEFAULT 0 NOT NULL,
       ', CMV_ANTERIOR = :P13 ' +//NUMBER(9,4) DEFAULT 0 NOT NULL,
       ', CUSTO_COMPRA_COMERCIAL = :P14 ' +//NUMBER(9,4) DEFAULT 0 NOT NULL,
       'where PRODUTO_ID = :P15 ';
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to Length(Fproduto) -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(Fproduto[i].nome));

        vSql_Validacao :='select produto_id ' +
                         ' from produtos ' +
                         ' where chave_importacao = ''' + Fproduto[i].codigo + ''' ';

        vId := Busca_Registro(FConsulta_dest,vSql_Validacao,'produto_id',MemErro);

        if (vId <> '') then
        begin
          vexec.Executar([
                          1,
                          Fproduto[i].custo,
                          Fproduto[i].custo,
                          Fproduto[i].custo,
                          Fproduto[i].custo,
                          Fproduto[i].custo,
                          Fproduto[i].custo,
                          Fproduto[i].custo,
                          Fproduto[i].custo,
                          Fproduto[i].custo,
                          Fproduto[i].custo,
                          Fproduto[i].custo,
                          Fproduto[i].custo,
                          Fproduto[i].custo,
                          vId
                          ]);


          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(Fproduto[i].codigo + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(Fproduto[i].codigo + ' - '+ vNome);
        end;

      end;

      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;

  finally

    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Promocoes(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vId:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_dest,vNome_seq,vSql_Validacao:string;
  FConsulta_dest : TConsulta;
  i: Integer;
  vExec:TExecucao;
  Existe_Relacionamento,Existe_Tabela:Boolean;
  Fproduto:TArray<RecProduto>;
  sheet: OleVariant;
  vPlanilha: OleVariant;
begin
  Result := False;

  Fproduto := nil;
  //Crio o objeto que gerencia o arquivo excel
  vPlanilha:= CreateOleObject('Excel.Application');

  //Abro o arquivo
  if FrmMenuPrincipal.edtArq.Text = '' then
  begin
    ShowMessage('Informe o Caminho da Planilha!' );
    Abort;
  end;
  vPlanilha.WorkBooks.open(FrmMenuPrincipal.edtArq.Text);

  //Pego a primeira vPlanilha do arquivo
  sheet := vPlanilha.WorkSheets[1];

  i := 2;
  lblQuant.caption := '0';
  lblImp.caption   := '0';
  lblDupl.caption  := '0';
  while Trim(sheet.Cells[i, 2]) = '1' do begin
    SetLength(Fproduto, Length(Fproduto) + 1);
    Fproduto[High(Fproduto)].Codigo     := sheet.cells[i, 3].Value;
    Fproduto[High(Fproduto)].preco      := sheet.cells[i, 7].Value;
    Fproduto[High(Fproduto)].custo      := sheet.cells[i, 8].Value;
    Fproduto[High(Fproduto)].cod_marca  := sheet.cells[i, 17].Value;
    Fproduto[High(Fproduto)].nome       := sheet.cells[i, 13].Value;
    Fproduto[High(Fproduto)].promocao   := sheet.cells[i, 16].Value;

    Inc(i);

    lblQuant.caption := NFormat( Length(Fproduto) );
    lblQuant.Refresh;
  end;

  //Fecho a vPlanilha
  vPlanilha.WorkBooks.Close;

  vNome_Tabela_Dest := 'PRODUTOS_PROMOCOES';
  vNome_Tabela_Orig := 'PROMOCOES_VENDAS';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'EMPRESA_ID ' + //NUMBER(3,0),
               ',PRODUTO_ID ' + //NUMBER(10,0),
               ',PRECO_PROMOCIONAL ' +//NUMBER(8,2) NOT NULL,
               ',DATA_INICIAL_PROMOCAO ' + //DATE NOT NULL,
               ',DATA_FINAL_PROMOCAO ' + //DATE NOT NULL,
               ',QUANTIDADE_MINIMA ' + //NUMBER(10,4) DEFAULT 0 NOT NULL,
               ') values ( '+
              NParametros(6);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to Length(Fproduto) -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(Fproduto[i].nome));

        vSql_Validacao :='select produto_id ' +
                         ' from produtos ' +
                         ' where chave_importacao = ''' + Fproduto[i].codigo + ''' ';

        vId := Busca_Registro(FConsulta_dest,vSql_Validacao,'produto_id',MemErro);

        if vId = '' then
          Existe_Tabela := False
        else begin
          vSql_Validacao :='select produto_id ' +
                           ' from  ' + vNome_Tabela_Dest +
                           ' where produto_id = ' + vId;
          Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,'produto_id',MemErro);
        end;

        if (vId <> '') and (not Existe_Tabela) and (Fproduto[i].promocao > 0)then
        begin
          vexec.Executar([
                          1,
                          vId,
                          Fproduto[i].promocao,
                          date,
                          '31/12/2018',
                          0
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(Fproduto[i].codigo + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(Fproduto[i].codigo + ' - '+ vNome);
        end;

      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;

  finally
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Origens(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,ins_Sql,vCid,Nome_Tabela,vUf:string;
  FConsulta_orig,FConsulta_dest : TConsulta;
  i: Integer;
  vExec:TExecucao;
begin
  Result := False;

  if MessageDlg('Deseja importar CFOP?',mtinformation,[mbyes,mbno],0) = 7 then
    abort;
  Nome_Tabela := 'CFOP';
  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(Conexao_orig);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    MemHist.Lines.Clear;
    MemHist.Lines.Add('Iniciando Importa��o de ' + Nome_Tabela);
    lblHis.Caption :='Importando ' + Nome_Tabela;
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os dados existente?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      MemHist.Lines.Add('Limpando '+Nome_Tabela+'.');
      vSql := 'DELETE FROM '+Nome_Tabela;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada em '+Nome_Tabela+'.');
    end;

    vSql :='select count(*) as quantreg ' +
           '  from cfop c,CFOP_DESCRICOES d ' +
           '  where c.cfop_id = d.cfop_id ' +
           '  and length(c.cfop_id) = 5 ';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;
    lblQuant.caption := FConsulta_orig.GetString('quantreg');

    vSql :='select c.cfop_id,c.nome, nvl(d.descricao,c.nome) descricao,c.ativo ' +
           '  from cfop c,CFOP_DESCRICOES d ' +
           '  where c.cfop_id = d.cfop_id ' +
           '  and length(c.cfop_id) = 5 ' +
           '  order by c.cfop_id ';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    ins_Sql :='insert into '+Nome_Tabela+'( '+
              'cfop_id,                     '+
              'NOME,                        '+
              'descricao,                   '+
              'ATIVO) values ( '+
              NParametros(4);
    vExec.Add(ins_Sql);

    try

      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;

      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vSql :='select cfop_id from cfop where cfop_id = ''' + FConsulta_orig.GetString('cfop_id') + '''';

        if not Existe_Registro(FConsulta_dest,vSql,'cfop_id',MemErro) then
        begin

          vexec.Executar([
                          FConsulta_orig.GetString('cfop_id'),
                          FConsulta_orig.GetString('NOME'),
                          FConsulta_orig.GetString('descricao'),
                          FConsulta_orig.GetString('ATIVO')
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(vUf + ' - '+ vCid);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(vUf + ' - '+ vCid);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir '+Nome_Tabela+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' + Nome_Tabela + ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Inicia_Estoques(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql:string;
  i: Integer;
  Fproduto:TArray<RecProduto>;
  sheet: OleVariant;
  vPlanilha: OleVariant;
begin

  vSql := 'insert into ESTOQUES  ' +
          '(EMPRESA_ID,PRODUTO_ID) select 1,PRODUTO_ID from produtos where PRODUTO_ID not in (select PRODUTO_ID from ESTOQUES) ';

  Result := roda_sql(True,Conexao_dest,vSql,MemErro);
  exit;

end;

function Inicia_Estoques_Div(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql:string;
begin
  Application.ProcessMessages;

  vSql := 'begin  ' +
  '  insert into ESTOQUES( ' +
  '    EMPRESA_ID, ' +
  '    PRODUTO_ID '+
  '  ) '+
  '  select '+
  '    EMP.EMPRESA_ID, '+
  '    PRO.PRODUTO_ID ' +
  '  from ' +
  '    PRODUTOS PRO '+
  '  cross join EMPRESAS EMP ' +
  '  where PRO.PRODUTO_ID = PRO.PRODUTO_PAI_ID  ' +
  '  and PRO.TIPO_CONTROLE_ESTOQUE <> ''K'' ' +
  '  and (PRO.PRODUTO_ID,EMP.EMPRESA_ID) not in (select PRODUTO_ID,EMPRESA_ID from estoques ); '+
  'end;';

  Result := roda_sql(True,Conexao_dest,vSql,MemErro);
  if not Result then
    Exit;

  vSql := ' delete from ESTOQUES_DIVISAO;';
  Result := roda_sql(True,Conexao_dest,vSql,MemErro);
  if not Result  then
    Exit;

  vSql := ' delete from PRODUTOS_LOTES;';
  roda_sql(True,Conexao_dest,vSql,MemErro);

  vSql := 'begin  ' +
  '  insert into PRODUTOS_LOTES( ' +
  '    PRODUTO_ID, ' +
  '    LOTE ' +
  '  ) ' +
  '  select ' +
  '    PRO.PRODUTO_ID, ' +
  '    ''???''  ' +
  '  from ' +
  '    PRODUTOS PRO ' +
  '  where PRO.PRODUTO_ID = PRO.PRODUTO_PAI_ID; '+
  'end;';

  Result := roda_sql(True,Conexao_dest,vSql,MemErro);
  if not Result  then
    Exit;


  vSql := 'begin  ' +
  '  insert into ESTOQUES_DIVISAO( ' +
  '    EMPRESA_ID, ' +
  '    LOCAL_ID, ' +
  '    PRODUTO_ID, '+
  '    LOTE '+
  '  ) '+
  '  select '+
  '    EMP.EMPRESA_ID, '+
  '    LOC.LOCAL_ID, '+
  '    PRO.PRODUTO_ID, ' +
  '    ''???'' '+
  '  from ' +
  '    PRODUTOS PRO '+
  '  cross join LOCAIS_PRODUTOS LOC ' +
  '  cross join EMPRESAS EMP ' +
  '  where PRO.PRODUTO_ID = PRO.PRODUTO_PAI_ID ' +
  '  and PRO.TIPO_CONTROLE_ESTOQUE <> ''K''; ' +
  'end;';

  Result := roda_sql(True,Conexao_dest,vSql,MemErro);
  if not Result  then
    Exit;
end;

function Importa_Estoques(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vLocal,vLote,vId:string;
  FConsulta_dest : TConsulta;
  i,j: Integer;
  vExec:TExecucao;
  Existe_Tabela:Boolean;
  Fproduto:TArray<RecProduto>;
  sheet: OleVariant;
  vPlanilha: OleVariant;
begin
  Result := False;

  Fproduto := nil;
  //Crio o objeto que gerencia o arquivo excel
  vPlanilha:= CreateOleObject('Excel.Application');

  //Abro o arquivo
  if FrmMenuPrincipal.edtArq.Text = '' then
  begin
    ShowMessage('Informe o Caminho da Planilha!' );
    Abort;
  end;
  vPlanilha.WorkBooks.open(FrmMenuPrincipal.edtArq.Text);

  //Pego a primeira vPlanilha do arquivo
  sheet := vPlanilha.WorkSheets[1];

  i := 2;
  lblQuant.caption := '0';
  lblImp.caption   := '0';
  lblDupl.caption  := '0';
  while Trim(sheet.Cells[i, 1]) <> '' do begin
    SetLength(Fproduto, Length(Fproduto) + 1);
    Fproduto[High(Fproduto)].Codigo     := sheet.cells[i, 1].Value;
    Fproduto[High(Fproduto)].estoque    := sheet.cells[i, 2].Value;

    Inc(i);

    lblQuant.caption := NFormat( Length(Fproduto) );
    lblQuant.Refresh;
  end;

  //Fecho a vPlanilha
  vPlanilha.WorkBooks.Close;

  vNome_Tabela_Dest := 'AJUSTES_ESTOQUE_ITENS';
  vNome_Tabela_Orig := 'DIVISAO_ESTOQUE';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    vFiltro_Delete := '';

    vSql := 'DELETE FROM AJUSTES_ESTOQUE_ITENS WHERE AJUSTE_ESTOQUE_ID = (select AJUSTE_ESTOQUE_ID from AJUSTES_ESTOQUE where empresa_id = 1)';
    roda_sql(True,Conexao_dest,vSql,MemErro);
    vSql := 'DELETE FROM AJUSTES_ESTOQUE WHERE EMPRESA_ID = 1 ';
    roda_sql(True,Conexao_dest,vSql,MemErro);
    MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');

    vSql :=
      'INSERT INTO AJUSTES_ESTOQUE( ' +
      'AJUSTE_ESTOQUE_ID '+
      ',MOTIVO_AJUSTE_ID '+
      ',OBSERVACAO ' +
      ',DATA_HORA_AJUSTE ' +
      ',USUARIO_AJUSTE_ID ' +
      ',TIPO ' +
      ',VALOR_TOTAL' +
      ',PLANO_FINANCEIRO_ID)  '+
      ' VALUES (1,1,''IMPORTACAO DE DADOS ADM'',SYSDATE,1,''NOR'',1,''1''); ';

    roda_sql(True,Conexao_dest,vSql,MemErro);

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'AJUSTE_ESTOQUE_ID ' +
               ',ITEM_ID '+// NUMBER(10,0),
               ',PRODUTO_ID ' +//NUMBER(10,0) NOT NULL,
               ',QUANTIDADE ' +//NUMBER(20,4) NOT NULL,
               ',NATUREZA ' +//CHAR(1) NOT NULL,
               ',LOTE ' +//VARCHAR2(80) DEFAULT '???',
               ',LOCAL_ID ' +//NUMBER(5,0),
               ',PRECO_UNITARIO'+
               ',VALOR_TOTAL' +
               ') values ( '+
              NParametros(9);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;

      j := 0;
      for i := 0 to Length(Fproduto) -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(Fproduto[i].nome));

        vSql_Validacao :='select produto_id ' +
                         ' from produtos ' +
                         ' where chave_importacao = ''' + Fproduto[i].codigo + ''' ';

        vId := Busca_Registro(FConsulta_dest,vSql_Validacao,'produto_id',MemErro);

        if vId = '' then
          Existe_Tabela := False
        else begin
          vSql_Validacao :='select produto_id ' +
                           ' from  ESTOQUES ' +
                           ' where produto_id = ' + vId;
          Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,'produto_id',MemErro);
        end;

        if (vId <> '') and (Existe_Tabela) and (Fproduto[i].estoque > 0) then
        begin
          j:= J +1;

          vLOCAL := '1';
          vLote := '???';
          vexec.Executar([
                         1,
                         J,
                         vId,
                         Fproduto[i].estoque,
                         'E',
                         vLote,
                         vLOCAL,
                         1,
                         1
                         ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(Fproduto[i].codigo + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(Fproduto[i].codigo + ' - '+ vNome);
        end;
      end;

      Conexao_dest.FinalizarTransacao;
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    except
      on e:Exception do begin
        Result := False;
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

  finally
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Tributacao(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vNome,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vId:string;
  FConsulta_dest : TConsulta;
  i: Integer;
  vExec:TExecucao;
  Fproduto:TArray<RecProduto>;
  sheet: OleVariant;
  vPlanilha: OleVariant;
  vGRUPO_TRIB_ESTADUAL_COMPRA_ID,vGRUPO_TRIB_ESTADUAL_VENDA_ID,
  vGRUPO_TRIB_FEDERAL_COMPRA_ID,vGRUPO_TRIB_FEDERAL_VENDA_ID :string;

begin
  Result := False;

  Fproduto := nil;
  //Crio o objeto que gerencia o arquivo excel
  vPlanilha:= CreateOleObject('Excel.Application');

  //Abro o arquivo
  if FrmMenuPrincipal.edtArq.Text = '' then
  begin
    ShowMessage('Informe o Caminho da Planilha!' );
    Abort;
  end;
  vPlanilha.WorkBooks.open(FrmMenuPrincipal.edtArq.Text);

  //Pego a primeira vPlanilha do arquivo
  sheet := vPlanilha.WorkSheets[1];

  i := 2;
  lblQuant.caption := '0';
  lblImp.caption   := '0';
  lblDupl.caption  := '0';
  while Trim(sheet.Cells[i, 2]) = '1' do begin
    SetLength(Fproduto, Length(Fproduto) + 1);
    Fproduto[High(Fproduto)].Codigo     := sheet.cells[i, 3].Value;
    Fproduto[High(Fproduto)].nome       := sheet.cells[i, 7].Value;
    Fproduto[High(Fproduto)].tributacao := sheet.cells[i, 16].Value;

    Inc(i);

    lblQuant.caption := NFormat( Length(Fproduto) );
    lblQuant.Refresh;
  end;

  //Fecho a vPlanilha
  vPlanilha.WorkBooks.Close;

  vNome_Tabela_Dest := 'PRODUTOS_GRUPOS_TRIB_EMPRESAS';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'EMPRESA_ID ' +
               ',PRODUTO_ID '+// NUMBER(10,0),
               ',GRUPO_TRIB_ESTADUAL_COMPRA_ID ' +//NUMBER(10,0) NOT NULL,
               ',GRUPO_TRIB_ESTADUAL_VENDA_ID ' +//NUMBER(20,4) NOT NULL,
               ',GRUPO_TRIB_FEDERAL_COMPRA_ID ' +//CHAR(1) NOT NULL,
               ',GRUPO_TRIB_FEDERAL_VENDA_ID ' +//VARCHAR2(80) DEFAULT '???',
               ') values ( '+
              NParametros(6);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;

      for i := 0 to Length(Fproduto) -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(Fproduto[i].nome));

        vSql_Validacao :='select produto_id ' +
                         ' from produtos ' +
                         ' where chave_importacao = ''' + Fproduto[i].codigo + ''' ';

        vId := Busca_Registro(FConsulta_dest,vSql_Validacao,'produto_id',MemErro);

        if vId <> '' then
        begin
          if Fproduto[i].tributacao = '17' then
          begin
            vGRUPO_TRIB_ESTADUAL_COMPRA_ID := '2';
            vGRUPO_TRIB_ESTADUAL_VENDA_ID  := '2';
            vGRUPO_TRIB_FEDERAL_COMPRA_ID  := '1';
            vGRUPO_TRIB_FEDERAL_VENDA_ID   := '1';
          end else
          begin
            vGRUPO_TRIB_ESTADUAL_COMPRA_ID := '1';
            vGRUPO_TRIB_ESTADUAL_VENDA_ID  := '1';
            vGRUPO_TRIB_FEDERAL_COMPRA_ID  := '1';
            vGRUPO_TRIB_FEDERAL_VENDA_ID   := '1';
          end;
          vexec.Executar([
                         1,
                         vId,
                         vGRUPO_TRIB_ESTADUAL_COMPRA_ID,
                         vGRUPO_TRIB_ESTADUAL_VENDA_ID,
                         vGRUPO_TRIB_FEDERAL_COMPRA_ID,
                         vGRUPO_TRIB_FEDERAL_VENDA_ID
                         ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(Fproduto[i].codigo + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(Fproduto[i].codigo + ' - '+ vNome);
        end;
      end;

      Conexao_dest.FinalizarTransacao;
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    except
      on e:Exception do begin
        Result := False;
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

  finally
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Marcas    (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq:string;
  FConsulta_dest : TConsulta;
  i,vId,j: Integer;
  vExec:TExecucao;
  Existe_Tabela:Boolean;
  Fmarca:TArray<RecMarca>;
  sheet: OleVariant;
  vPlanilha: OleVariant;
begin
  Result := False;

  Fmarca := nil;
  //Crio o objeto que gerencia o arquivo excel
  vPlanilha:= CreateOleObject('Excel.Application');

  //Abro o arquivo
  if FrmMenuPrincipal.edtArq.Text = '' then
  begin
    ShowMessage('Informe o Caminho da Planilha!' );
    Abort;
  end;
  vPlanilha.WorkBooks.open(FrmMenuPrincipal.edtArq.Text);

  //Pego a primeira vPlanilha do arquivo
  sheet := vPlanilha.WorkSheets[1];

  i := 2;
  lblQuant.caption := '0';
  lblImp.caption   := '0';
  lblDupl.caption  := '0';
  while Trim(sheet.Cells[i, 2]) <> '' do begin
    if sheet.cells[i, 2].Value = 'MARCA' then begin
      Inc(i);

      lblQuant.caption := NFormat( Length(Fmarca) );
      lblQuant.Refresh;

      Continue;
    end;

    SetLength(Fmarca, Length(Fmarca) + 1);
    Fmarca[High(Fmarca)].Codigo  := sheet.cells[i, 1].Value;
    Fmarca[High(Fmarca)].Marca   := sheet.cells[i, 2].Value;

    Inc(i);

    lblQuant.caption := NFormat( Length(Fmarca) );
    lblQuant.Refresh;
  end;

  //Fecho a vPlanilha
  vPlanilha.WorkBooks.Close;

  vNome_Tabela_Dest := 'marcas';
  vNome_Tabela_Orig := 'marcas';
  vChave_orig := 'marca_id';
  vChave_dest := 'marca_id';
  vNome_seq:= 'SEQ_MARCA_ID';

  Application.ProcessMessages;
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'MARCA_ID '+// NUMBER(6,0),
               ',NOME ' +//VARCHAR2(40) NOT NULL,
               ',ATIVO ' +// CHAR(1) DEFAULT 'S'  NOT NULL,
               ',CHAVE_IMPORTACAO ' +
               ') values ( '+
              NParametros(4);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j:= 0;
      for i := 0 to Length(Fmarca) -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Fmarca[i].Marca);
        vId := StrToInt(Fmarca[i].Codigo);

        vSql_Validacao :='select ' + vChave_dest +
                         ' from ' + vNome_Tabela_Dest +
                         ' where nome = ''' + vNome + ''' ';
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        if (not Existe_Tabela) then
        begin
          j:= j+1;
          vexec.Executar([
                          j,
                          vNome,
                          'S',
                          vId
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;

      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Cfop_Cst(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vNome_seq,vTpMov,vCfop_id:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i: Integer;
  vExec:TExecucao;
  Existe_Relacionamento:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'CFOPS_CST_OPERACOES';
  vNome_Tabela_Orig := 'CFOP_EMPRESA';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    vSql := 'select DISTINCT EMPRESA_ID,TIPO,CFOP_ESTADUAL_ID AS CFOP_ID,CLASSE_FISCAL from CFOP_EMPRESA' +
            ' where tipo in (''VEN'',''DEV'',''TRA'')';

    lblQuant.caption := Num_Registros(0,FConsulta_orig,'('+vSql+')',MemErro);

    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'EMPRESA_ID ' +//NUMBER(3,0),
               ',CFOP_ID ' + //NUMBER(4,0) NOT NULL,
               ',TIPO_MOVIMENTO ' + //CHAR(3),
               ',CST ' + //CHAR(2),
               ',TIPO_OPERACAO ' + //CHAR(1),
               ') values ( '+
              NParametros(5);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := FConsulta_orig.GetString('CFOP_ID');

        Existe_Relacionamento := True;

        if FConsulta_orig.GetString('TIPO') = 'VEN' then
            vTpMov := 'VIT';

        if FConsulta_orig.GetString('TIPO') = 'DEV' then
          vTpMov := 'DEV';

        if FConsulta_orig.GetString('TIPO') = 'TRA' then
          vTpMov := 'TPE';

        if vTpMov = '' then
          Existe_Relacionamento := False;

        if Existe_Relacionamento then
        begin
          vCfop_id := copy(FConsulta_orig.GetString('cfop_id'),1,1) +
                      copy(FConsulta_orig.GetString('cfop_id'),3,3);
          vexec.Executar([
                         FConsulta_orig.GetString('EMPRESA_ID'),
                         vCfop_id,
                         vTpMov,
                         FConsulta_orig.GetString('CLASSE_FISCAL'),
                         'I'
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(vCfop_id + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;

  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Cfop_Cst1(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vIns_Sql:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vNome_seq,vTpMov,vCfop_id:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i: Integer;
  vExec:TExecucao;
  Existe_Relacionamento:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'CFOPS_CST_OPERACOES';
  vNome_Tabela_Orig := 'CFOP_EMPRESA';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    vSql := 'select DISTINCT EMPRESA_ID,TIPO,CFOP_INTERESTADUAL_ID AS CFOP_ID,CLASSE_FISCAL from CFOP_EMPRESA' +
            ' where tipo in (''VEN'',''DEV'',''TRA'')';

    lblQuant.caption := Num_Registros(0,FConsulta_orig,'('+vSql+')',MemErro);

    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'EMPRESA_ID ' +//NUMBER(3,0),
               ',CFOP_ID ' + //NUMBER(4,0) NOT NULL,
               ',TIPO_MOVIMENTO ' + //CHAR(3),
               ',CST ' + //CHAR(2),
               ',TIPO_OPERACAO ' + //CHAR(1),
               ') values ( '+
              NParametros(5);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := FConsulta_orig.GetString('CFOP_ID');

        Existe_Relacionamento := True;

//        if i = 24520 then
//          lblHis.Caption :='Parada para correcao';

        if FConsulta_orig.GetString('TIPO') = 'VEN' then
          vTpMov := 'VIT';

        if FConsulta_orig.GetString('TIPO') = 'DEV' then
          vTpMov := 'DEV';

        if FConsulta_orig.GetString('TIPO') = 'TRA' then
          vTpMov := 'TPE';

        if Existe_Relacionamento then
        begin
          vCfop_id := copy(FConsulta_orig.GetString('cfop_id'),1,1) +
                      copy(FConsulta_orig.GetString('cfop_id'),3,3);
          vexec.Executar([
                         FConsulta_orig.GetString('EMPRESA_ID'),
                         vCfop_id,
                         vTpMov,
                         FConsulta_orig.GetString('CLASSE_FISCAL'),
                         'E'
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(vCfop_id + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Contas_Pagar(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq,
  vMotBloq,vPrevisao,vParc,vPlFin,vCad:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,vId,vTpCobr: Integer;
  vExec:TExecucao;
  Existe_Tabela:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'CONTAS_PAGAR';
  vNome_Tabela_Orig := 'FINANCEIRO';
  vChave_orig := 'FINANCEIRO_ID';
  vChave_dest := 'PAGAR_ID';
  vNome_seq:= 'SEQ_PAGAR_ID';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    vSql := 'select f.*, c.cpf from financeiro f, cadastros c ' +
            'where f.cadastro_id = c.Cadastro_id and f.status = ''A'' '+
            'and f.natureza = ''S'' ';

    lblQuant.caption := Num_Registros(0,FConsulta_orig,'('+vSql+')',MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'PAGAR_ID '+//NUMBER(12,0),
               ',CADASTRO_ID '+//NUMBER(10,0) NOT NULL,
               ',EMPRESA_ID '+//NUMBER(3,0) NOT NULL,
               ',COBRANCA_ID '+//NUMBER(3,0) NOT NULL,
               ',PORTADOR_ID '+//VARCHAR2(50) NOT NULL,
               ',USUARIO_CADASTRO_ID '+//NUMBER(4,0) NOT NULL,
               ',DOCUMENTO '+//VARCHAR2(20) NOT NULL,
               ',NUMERO_CHEQUE '+//NUMBER(10,0),
               ',PLANO_FINANCEIRO_ID '+//VARCHAR2(9) NOT NULL,
               ',CENTRO_CUSTO_ID '+//NUMBER(3,0) DEFAULT 1 NOT NULL,

               ',DATA_CADASTRO '+//DATE DEFAULT trunc(sysdate)  NOT NULL,
               ',DATA_VENCIMENTO '+//DATE NOT NULL,
               ',DATA_VENCIMENTO_ORIGINAL '+//DATE,
               ',VALOR_DOCUMENTO '+//NUMBER(8,2) NOT NULL,
               ',STATUS '+//CHAR(1) DEFAULT 'A'  NOT NULL,
               ',NOSSO_NUMERO '+//VARCHAR2(30),
               ',CODIGO_BARRAS '+//VARCHAR2(44),
               ',PARCELA '+//NUMBER(3,0) DEFAULT 1 NOT NULL,
               ',NUMERO_PARCELAS '+//NUMBER(3,0) DEFAULT 1 NOT NULL,
               ',ORIGEM '+//CHAR(3) NOT NULL,

               ',OBSERVACOES '+//VARCHAR2(200),
               ',BLOQUEADO '+//CHAR(1) DEFAULT 'N'  NOT NULL,
               ',MOTIVO_BLOQUEIO '+//VARCHAR2(400),
               ',NUMERO_NOTA '+//NUMBER(10,0),
               ',LIBERADO_CREDITO_MOV_FINANC '+//CHAR(1) DEFAULT 'N'     NOT NULL,
               ',LIBERADO_PAGAMENTO_FINANCEIRO '+//CHAR(1) DEFAULT 'N'    NOT NULL,
               ',DATA_CONTABIL '+//DATE NOT NULL,
               ',DATA_EMISSAO '+//DATE NOT NULL,
               ',VALOR_DESCONTO '+//NUMBER(8,2) DEFAULT 0 NOT NULL,
               ',PREVISAO '+//CHAR(1),

               ',USUARIO_LIB_PAG_FINANCEIRO '+//NUMBER(8,0),
               ',DATA_LIBERACAO_PAG_FINANCEIRO '+//DATE,
               ',USUARIO_LIBERACAO_BLOQUEIO '+//NUMBER(8,0),
               ',DATA_LIBERACAO_BLOQUEIO '+//DATE,
               ',CHAVE_IMPORTACAO '+//  number(12)
               ',INDICE_CONDICAO_PAGAMENTO '+//NUMBER(10,5),
               ') values ( '+
              NParametros(36);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 0;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('cadastro_id').AsString));
        vId := FConsulta_orig.FieldByName(vChave_orig).AsInteger;

        vSql_Validacao :='select CHAVE_IMPORTACAO ' +
                         ' from ' + vNome_Tabela_Dest +
                         ' where CHAVE_IMPORTACAO = '+ IntToStr(vId);
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,'CHAVE_IMPORTACAO',MemErro);

        if i = 544 then
          lblHis.Caption :='Parada para correcao';

        if (not Existe_Tabela) then
        begin
          j:= J +1;

          vTpCobr := 1;

          if FConsulta_orig.GetString('tipo') = 'PR'  then
            vTpCobr := 12;
          if FConsulta_orig.GetString('tipo') = 'BL'  then
            vTpCobr := 6;
          if FConsulta_orig.GetString('tipo') = 'DP'  then
            vTpCobr := 7;
          if FConsulta_orig.GetString('tipo') = 'CT'  then
            vTpCobr := 11;
          if FConsulta_orig.GetString('tipo') = 'CV'  then
            vTpCobr := 2;
          if FConsulta_orig.GetString('tipo') = 'CP'  then
            vTpCobr := 3;
          if FConsulta_orig.GetString('tipo') = 'CD'  then  // cheque devolvido
            vTpCobr := 4;
          if FConsulta_orig.GetString('tipo') = 'CR'  then  // cheque reapresentado
            vTpCobr := 4;
          if FConsulta_orig.GetString('tipo') = 'FI'  then  // cheque reapresentado
            vTpCobr := 8;

//          if FConsulta_orig.GetString('tipo') = 'AD'  then    // adiantamento
//            vTpCobr := 3;
//          if FConsulta_orig.GetString('tipo') = 'CD'  then   // cartao
//            vTpCobr := 3;

          vMotBloq := '';
          if FConsulta_orig.GetString('BLOQUEADO') = 'S' then
            vMotBloq := 'IMPORTACAO';

          vPrevisao := 'N';
          if FConsulta_orig.GetString('tipo') = 'PR'  then
            vPrevisao := 'S';

          vParc := FConsulta_orig.GetString('TOTAL_PARCELAS');
          if vParc = '' then
            vParc := '1';

          vPlFin := FConsulta_orig.GetString('PLANO_FINANCEIRO_ID');
          if vPlFin = '' then
            vPlFin := '1.002.001';

          vCad := FConsulta_orig.GetString('CADASTRO_ID');
          vSql_Validacao :='select CADASTRO_ID from cadastros  where CADASTRO_ID = ' + vCad;
          if not Existe_Registro(FConsulta_dest,vSql_Validacao,'CADASTRO_ID',MemErro) then
          begin
            vSql_Validacao :='select CADASTRO_ID from cadastros  where cpf_cnpj = ''' + FConsulta_orig.GetString('cpf') +'''';
            vCad := Busca_Registro(FConsulta_dest,vSql_Validacao,'CADASTRO_ID',MemErro);
          end;


          vexec.Executar([
                          J,
                          vCad,
                          FConsulta_orig.GetString('EMPRESA_ID'),
                          vTpCobr,
                          FConsulta_orig.GetInt('PORTADOR_ID'),
                          1,
                          FConsulta_orig.GetString('DOCUMENTO'),
                          '',
                          vPlFin,
                          FConsulta_orig.GetInt('CENTRO_ID'),

                          FConsulta_orig.GetData('DATA_CADASTRO'),
                          FConsulta_orig.GetString('DATA_VENCIMENTO'),
                          FConsulta_orig.GetString('DATA_VENCIMENTO_ORIGINAL'),
                          FConsulta_orig.GetDouble('VALOR_DOCUMENTO'),
                          'A',
                          FConsulta_orig.GetString('NOSSO_NUMERO_COMPLETO'),
                          FConsulta_orig.GetString('CODIGO_BARRAS'),
                          1,
                          strtoint(vParc),
                          'MAN',

                          'IMPORTACAO ADM FINACEIRO N-' +
                          FConsulta_orig.GetString('FINANCEIRO_ID') + ' ' +
                          FConsulta_orig.GetString('OBSERVACAO'),
                          FConsulta_orig.GetString('BLOQUEADO'),
                          vMotBloq,
                          FConsulta_orig.GetString('NUMERO_NOTA'),
                          FConsulta_orig.GetString('LIBERADO_CREDITO_MOV_FINANC'),
                          FConsulta_orig.GetString('LIBERADO_PAGAMENTO_FINANCEIRO'),
                          FConsulta_orig.GetData('DATA_CONTABIL'),
                          FConsulta_orig.GetData('DATA_EMISSAO'),
                          0,
                          vPrevisao,

                          FConsulta_orig.GetString('USUARIO_LIB_PAG_FINANCEIRO'),
                          null,
                          FConsulta_orig.GetString('USUARIO_LIBERACAO_BLOQUEIO'),
                          null,
                          FConsulta_orig.GetString('FINANCEIRO_ID'),
                          FConsulta_orig.GetDouble('INDICE_CONDICAO_PAGAMENTO')
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;

end;

function Importa_Contas_Receber(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq,
  vMotBloq,vPrevisao,vParc,vPlFin,vCad,vPlFinanc,vPortador:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,vId,vTpCobr,vVend: Integer;
  vExec:TExecucao;
  Existe_Tabela,Existe_Relacionamento:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'CONTAS_RECEBER';
  vNome_Tabela_Orig := 'FINANCEIRO';
  vChave_orig := 'FINANCEIRO_ID';
  vChave_dest := 'RECEBER_ID';
  vNome_seq   := 'SEQ_RECEBER_ID';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    vSql := 'select f.*,r.*, c.cpf, c.cnpj from  cadastros c, financeiro f ' +
            'LEFT JOIN cheques_receber r on (f.FINANCEIRO_ID = r.FINANCEIRO_ID) '+
            '    where f.cadastro_id = c.Cadastro_id'+
            '    and f.status = ''A'' '+
            '    and f.natureza = ''E'' ';

    lblQuant.caption := Num_Registros(0,FConsulta_orig,'('+vSql+')',MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'RECEBER_ID '+//NUMBER(12,0),
               ',EMPRESA_ID '+//NUMBER(3,0) NOT NULL,
               ',COBRANCA_ID '+//NUMBER(3,0) NOT NULL,
               ',VENDEDOR_ID '+//NUMBER(4,0),
               ',USUARIO_CADASTRO_ID '+//NUMBER(4,0) NOT NULL,
               ',DOCUMENTO '+//VARCHAR2(20) NOT NULL,
               ',BANCO '+//VARCHAR2(5),
               ',AGENCIA '+//VARCHAR2(8),
               ',CONTA_CORRENTE '+//VARCHAR2(8),
               ',NUMERO_CHEQUE '+//NUMBER(10,0),

               ',NOME_EMITENTE '+//VARCHAR2(80),
               ',TELEFONE_EMITENTE '+//VARCHAR2(15),
               ',DATA_CADASTRO '+//DATE DEFAULT trunc(sysdate)  NOT NULL,
               ',DATA_EMISSAO '+//DATE,
               ',DATA_VENCIMENTO '+//DATE NOT NULL,
               ',DATA_VENCIMENTO_ORIGINAL '+//DATE,
               ',VALOR_DOCUMENTO '+//NUMBER(8,2) NOT NULL,
               ',STATUS '+//CHAR(1) DEFAULT 'A'  NOT NULL,
               ',NOSSO_NUMERO '+//VARCHAR2(30),
               ',CODIGO_BARRAS '+//VARCHAR2(44),

               ',NSU '+//VARCHAR2(20),
               ',CODIGO_AUTORIZACAO_TEF '+//VARCHAR2(20),
               ',PARCELA '+//NUMBER(3,0) DEFAULT 1 NOT NULL,
               ',NUMERO_PARCELAS '+//NUMBER(3,0) DEFAULT 1 NOT NULL,
               ',OBSERVACOES '+//VARCHAR2(200),
               ',PORTADOR_ID '+//VARCHAR2(50) NOT NULL,
               ',PLANO_FINANCEIRO_ID '+//VARCHAR2(9) NOT NULL,
               ',CENTRO_CUSTO_ID '+//NUMBER(3,0) DEFAULT 1 NOT NULL,
               ',ORIGEM '+//CHAR(3) NOT NULL,
               ',CADASTRO_ID '+//NUMBER(10,0) NOT NULL,

               ',VALOR_RETENCAO '+//NUMBER(8,2) DEFAULT 0 NOT NULL,
               ',NOME_ARQUIVO_REMESSA '+//VARCHAR2(100),
               ',NUMERO_NOTA '+//NUMBER(10,0),
               ',DATA_CONTABIL '+//DATE NOT NULL,
               ',DATA_ENVIO_EMAIL_BOLETO_DUPL '+//DATE,
               ',DATA_PROTESTO '+//DATE,
               ',DATA_REEMISSAO_BOLETO_DUPL '+//DATE,
               ',EMITIU_BOLETO_DUPLICATA '+//CHAR(1) DEFAULT 'N'  NOT NULL,
               ',ENVIOU_EMAIL_BOLETO_DUPLICATA '+//CHAR(1) DEFAULT 'N'  NOT NULL,
               ',CNPJ_CPF_EMITENTE_CHEQUE '+//VARCHAR2(18),

               ',INDICE_CONDICAO_PAGAMENTO '+//NUMBER(10,5),
               ',COBRANCA_JUDICIAL '+//CHAR(1) DEFAULT 'N'  NOT NULL,
               ',DATA_ENVIO_COBRANCA_JUDICIAL '+//DATE,
               ',CHEQUE_MORADIA '+//CHAR(1) DEFAULT 'N'  NOT NULL,
               ',CLIENTE_RECEBEU_AVISO_COB '+//CHAR(1) DEFAULT 'N'  NOT NULL,
               ',CLIENTE_RECEBEU_INST_PROTESTO '+//CHAR(1) DEFAULT 'N'  NOT NULL,
               ',CMC7 '+//VARCHAR2(30),
               ',NUMERO_CARTAO_TRUNCADO '+//VARCHAR2(19),
               ',PREJUIZO '+//CHAR(1) DEFAULT 'N'  NOT NULL,
               ',SAIU_DO_PREJUIZO '+//CHAR(1),

               ',STATUS_PROTESTO '+//CHAR(3) DEFAULT 'NEN' NOT NULL,
               ',VALOR_CUSTOS_CARTORIO '+//NUMBER(13,4) DEFAULT 0 NOT NULL,
               ',CHAVE_IMPORTACAO '+//NUMBER(12,0),
               ') values ( '+
              NParametros(53);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 0;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        if J= 118 then
          lblHis.Caption :='PARADA';

        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('cadastro_id').AsString));
        vId := FConsulta_orig.FieldByName(vChave_orig).AsInteger;

        vSql_Validacao :='select CHAVE_IMPORTACAO ' +
                         ' from ' + vNome_Tabela_Dest +
                         ' where CHAVE_IMPORTACAO = '+ IntToStr(vId);
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,'CHAVE_IMPORTACAO',MemErro);

        Existe_Relacionamento := True;

        if (not Existe_Tabela) and Existe_Relacionamento then
        begin
          j:= J +1;

          vTpCobr := 1;

          if FConsulta_orig.GetString('tipo') = 'PR'  then
            vTpCobr := 12;
          if FConsulta_orig.GetString('tipo') = 'BL'  then
            vTpCobr := 6;
          if FConsulta_orig.GetString('tipo') = 'DP'  then
            vTpCobr := 7;
          if FConsulta_orig.GetString('tipo') = 'CT'  then
            vTpCobr := 11;
          if FConsulta_orig.GetString('tipo') = 'CV'  then
            vTpCobr := 2;
          if FConsulta_orig.GetString('tipo') = 'CP'  then
            vTpCobr := 3;
          if FConsulta_orig.GetString('tipo') = 'CD'  then  // cheque devolvido
            vTpCobr := 4;
          if FConsulta_orig.GetString('tipo') = 'CR'  then  // cheque reapresentado
            vTpCobr := 4;
          if FConsulta_orig.GetString('tipo') = 'FI'  then  // cheque reapresentado
            vTpCobr := 8;

          if FConsulta_orig.GetString('tipo') = 'AD'  then    // adiantamento
            vTpCobr := 3;

          vPortador := FConsulta_orig.GetString('PORTADOR_ID');

          if FConsulta_orig.GetString('tipo') = 'CC'  then   // cartao
          begin
            if  vPortador = '8035' then
              vPortador := '3047';

            FConsulta_dest.Limpar;
            FConsulta_dest.SQL.Add('select t.cobranca_id,count(p.dias) ' +
                             'from tipos_cobranca t,TIPO_COBRANCA_DIAS_PRAZO p ' +
                             'where t.COBRANCA_ID = p.COBRANCA_ID ' +
                             '  and t.PORTADOR_ID = ''' + vPortador + '''' +
                             '  group by  t.cobranca_id, t.PORTADOR_ID ' +
                             '  having count(p.dias) = ' + FConsulta_orig.GetString('total_parcelas') );
            FConsulta_dest.Pesquisar;

            vTpCobr := FConsulta_dest.GetInt('cobranca_id');
          end;

          if vTpCobr = 0 then
          begin
            if not vsp then
            begin
              if MessageDlg('Portador '+ vPortador +
                            ' parcelas ' + FConsulta_orig.GetString('total_parcelas')+
                            ' nao encontrado, deseja continuar?',mtinformation,[mbyes,mbno],0) = 6 then
                vTpCobr := 1;
            end else
              vTpCobr := 4;
          end;
          vMotBloq := '';
          if FConsulta_orig.GetString('BLOQUEADO') = 'S' then
            vMotBloq := 'IMPORTACAO';

          vPrevisao := 'N';
          if FConsulta_orig.GetString('tipo') = 'PR'  then
            vPrevisao := 'S';

          vParc := FConsulta_orig.GetString('TOTAL_PARCELAS');
          if vParc = '' then
            vParc := '1';

          vPlFin := FConsulta_orig.GetString('PLANO_FINANCEIRO_ID');
          if vPlFin = '' then
            vPlFin := '1.002.001';

          vCad := FConsulta_orig.GetString('CADASTRO_ID');
          vSql_Validacao :='select CADASTRO_ID from cadastros  where CADASTRO_ID = ' + vCad;

          if not Existe_Registro(FConsulta_dest,vSql_Validacao,'CADASTRO_ID',MemErro) then
          begin
            vSql_Validacao :='select CADASTRO_ID from cadastros  where cpf_cnpj = ''' + FConsulta_orig.GetString('cpf') +'''';
            vCad := Busca_Registro(FConsulta_dest,vSql_Validacao,'CADASTRO_ID',MemErro);

            if vCad = '' then
            begin
              vSql_Validacao :='select CADASTRO_ID from cadastros  where cpf_cnpj = ''' + FConsulta_orig.GetString('cnpj') +'''';
              vCad := Busca_Registro(FConsulta_dest,vSql_Validacao,'CADASTRO_ID',MemErro);
            end;
          end;

          vPlFinanc := FConsulta_orig.GetString('PLANO_FINANCEIRO_ID');
          if vPlFinanc = '' then
            vPlFinanc := '2.012.001';

          vVend := FConsulta_orig.GetInt('VENDEDOR_ID');
          vSql_Validacao :='select FUNCIONARIO_ID from funcionarios  where FUNCIONARIO_ID = ' + IntToStr(vVend);
          if not Existe_Registro(FConsulta_dest,vSql_Validacao,'FUNCIONARIO_ID',MemErro) then
            vVend := 1;

          if j = 3 then
            lblHis.Caption :='Parada para correcao';

          vexec.Executar([
                          J,
                          FConsulta_orig.GetInt('EMPRESA_ID'),
                          vTpCobr,
                          vVend,
                          1,
                          FConsulta_orig.GetString('DOCUMENTO'),
                          FConsulta_orig.GetInt('PORTADOR_CHEQUE_ID'),
                          FConsulta_orig.GetString('AGENCIA'),
                          FConsulta_orig.GetString('CONTA'),
                          FConsulta_orig.GetString('NUMERO'),

                          FConsulta_orig.GetString('NOME'),
                          '',
                          FConsulta_orig.GetData('DATA_CADASTRO'),
                          FConsulta_orig.GetData('DATA_EMISSAO'),
                          FConsulta_orig.GetData('DATA_VENCIMENTO'),
                          FConsulta_orig.GetData('DATA_VENCIMENTO_ORIGINAL'),
                          FConsulta_orig.GetDouble('VALOR_DOCUMENTO'),
                          'A',
                          FConsulta_orig.GetString('NOSSO_NUMERO_COMPLETO'),
                          FConsulta_orig.GetString('CODIGO_BARRAS'),

                          FConsulta_orig.GetString('NSU'),
                          FConsulta_orig.GetString('CODIGO_AUTORIZACAO_CARTAO'),
                          1,
                          strtoint(vParc),
                          'IMPORTACAO ADM FINACEIRO N-' +
                          FConsulta_orig.GetString('FINANCEIRO_ID') + ' ' +
                          FConsulta_orig.GetString('OBSERVACAO'),
                          vPortador,
                          vPlfinanc,
                          FConsulta_orig.GetInt('CENTRO_ID'),
                          'MAN',
                          vCad,

                          FConsulta_orig.GetDouble('VALOR_RETENCAO_FINANCEIRA'),
                          FConsulta_orig.GetString('NOME_ARQUIVO_REMESSA'),
                          FConsulta_orig.GetString('NUMERO_NOTA'),
                          FConsulta_orig.GetData('DATA_CONTABIL'),
                          FConsulta_orig.GetData('DATA_ENVIO_EMAIL_BOLETO'),
                          FConsulta_orig.GetData('DATA_PROTESTO'),
                          FConsulta_orig.GetData('DATA_REEMISSAO'),
                          FConsulta_orig.GetString('EMITIU_DUPLICATA_BOLETA'),
                          FConsulta_orig.GetString('ENVIOU_EMAIL_BOLETO'),
                          FConsulta_orig.GetString('CNPJ_CPF'),

                          FConsulta_orig.GetDouble('INDICE_CONDICAO_PAGAMENTO'),
                          FConsulta_orig.GetString('COBRANCA_JUDICIAL'),
                          null,
                          FConsulta_orig.GetString('CHEQUE_MORADIA'),
                          FConsulta_orig.GetString('CLIENTE_RECEBEU_AVISO_COB'),
                          FConsulta_orig.GetString('CLIENTE_RECEBEU_INST_PROTESTO'),
                          FConsulta_orig.GetString('CMC7'),
                          FConsulta_orig.GetString('NUMERO_CARTAO_TRUNCADO'),
                          FConsulta_orig.GetString('PREJUIZO'),
                          FConsulta_orig.GetString('SAIU_DO_PREJUIZO'),

                          FConsulta_orig.GetString('STATUS_PROTESTO'),
                          FConsulta_orig.GetDouble('VALOR_CUSTOS_CARTORIO'),
                          FConsulta_orig.GetString('FINANCEIRO_ID')
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Produtos(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo; fornecedor_padrao: string; ncm_padrao: string):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao,
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,
  vChave_dest,vNome_seq,vblqVda,vTpEst,vEnc,
  vConf,vSprev,vGrupo,vAtivo,vModAtivo,vUnEntrega,
  vFabricante,vEstNeg,vMarca, vSqlUpdate, vSqlProdutos:string;
  FConsulta_dest : TConsulta;
  i,vId: Integer;
  vExec, vExecOriginal:TExecucao;
  vExecUpdate: TExecucao;
  vExecucao: TExecucao;
  vDiasvenc:Currency;
  Existe_Tabela:Boolean;
  Fproduto:TArray<RecProduto>;
  sheet: OleVariant;
  vPlanilha: OleVariant;
  cadastro_id: string;
  fornecedor_produto_id: string;
begin
  Result := False;

  Fproduto := nil;
  //Crio o objeto que gerencia o arquivo excel
  vPlanilha:= CreateOleObject('Excel.Application');

  //Abro o arquivo
  if FrmMenuPrincipal.edtArq.Text = '' then
  begin
    ShowMessage('Informe o Caminho da Planilha!' );
    Abort;
  end;
  vPlanilha.WorkBooks.open(FrmMenuPrincipal.edtArq.Text);

  //Pego a primeira vPlanilha do arquivo
  sheet := vPlanilha.WorkSheets[1];

  i := 2;
  lblQuant.caption := '0';
  lblImp.caption   := '0';
  lblDupl.caption  := '0';
  while Trim(sheet.Cells[i, 1]) <> '' do begin
    SetLength(Fproduto, Length(Fproduto) + 1);
    Fproduto[High(Fproduto)].Codigo        := sheet.cells[i, 1].Value;
    Fproduto[High(Fproduto)].codigo_barras := sheet.cells[i, 2].Value;
    Fproduto[High(Fproduto)].cod_marca     := sheet.cells[i, 3].Value;
    Fproduto[High(Fproduto)].nome          := Copy(sheet.cells[i, 4].Value, 1, 60);
    Fproduto[High(Fproduto)].unidade       := sheet.cells[i, 5].Value;
    Fproduto[High(Fproduto)].multiplo_venda := sheet.cells[i, 6].Value;
    Fproduto[High(Fproduto)].ativo         := sheet.cells[i, 7].Value;
    Fproduto[High(Fproduto)].cest          := sheet.cells[i, 8].Value;
    Fproduto[High(Fproduto)].fornecedor_id := sheet.cells[i, 9].Value;
    Fproduto[High(Fproduto)].ncm := sheet.cells[i, 10].Value;
    if (Fproduto[High(Fproduto)].ncm = '') or (Length(Fproduto[High(Fproduto)].ncm) <> 8) then
      Fproduto[High(Fproduto)].ncm := ncm_padrao;

    Fproduto[High(Fproduto)].tributacao_estadual_compra := sheet.cells[i, 11].Value;
    Fproduto[High(Fproduto)].tributacao_estadual_venda := sheet.cells[i, 12].Value;
    Fproduto[High(Fproduto)].tributacao_federal_compra := sheet.cells[i, 13].Value;
    Fproduto[High(Fproduto)].tributacao_federal_venda := sheet.cells[i, 14].Value;
    Fproduto[High(Fproduto)].codigo_original_fabricante := sheet.cells[i, 15].Value;
    if Fproduto[High(Fproduto)].codigo_original_fabricante = '' then
      Fproduto[High(Fproduto)].codigo_original_fabricante := '1';

    //CODIGO_ORIGINAL_FABRICANTE
    Fproduto[High(Fproduto)].tipo_def_automatica_lote := 'V';

    Inc(i);

    lblQuant.caption := NFormat( Length(Fproduto) );
    lblQuant.Refresh;
  end;

  //Fecho a vPlanilha
  vPlanilha.WorkBooks.Close;

  vNome_Tabela_Dest := 'PRODUTOS';
  vChave_dest := 'PRODUTO_ID';
  vNome_seq   := 'SEQ_PRODUTO_ID';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);
  vExecOriginal := TExecucao.Create(Conexao_dest);
  vExecUpdate := TExecucao.Create(Conexao_dest);
  vExecucao := TExecucao.Create(Conexao_dest);

  try
    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    vExec.Limpar;
    vExec.Add('alter trigger PRODUTOS_IU_BR disable');
    vexec.Executar;
    vExec.Limpar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'PRODUTO_ID '+//NUMBER(10,0),
               ', NOME '+//VARCHAR2(60) NOT NULL,
               ', NOME_COMPRA '+//VARCHAR2(60) NOT NULL,
               ', FORNECEDOR_ID '+//NUMBER(10,0) NOT NULL,
               ', UNIDADE_VENDA '+//VARCHAR2(3) NOT NULL,
               ', MARCA_ID '+//NUMBER(6,0) NOT NULL,
               ', MULTIPLO_VENDA '+//NUMBER(7,3) NOT NULL,
               ', PESO '+//NUMBER(8,3) NOT NULL,
               ', VOLUME '+//NUMBER(9,7) NOT NULL,
               ', LINHA_PRODUTO_ID '+//VARCHAR2(11) NOT NULL,
               ', CODIGO_BARRAS '+//VARCHAR2(14),
               ', ACEITAR_ESTOQUE_NEGATIVO '+//CHAR(1) DEFAULT 'N'  NOT NULL,
               ', INATIVAR_ZERAR_ESTOQUE '+//CHAR(1) DEFAULT 'S'  NOT NULL,
               ', CODIGO_NCM '+//VARCHAR2(8),
               ', CODIGO_ORIGINAL_FABRICANTE '+//VARCHAR2(20),
               ', CHAVE_IMPORTACAO '+//VARCHAR2(100),
               ', PRODUTO_PAI_ID' +
               ', CEST' +
               ', TIPO_DEF_AUTOMATICA_LOTE' +
               ') values ( '+
              NParametros(19);
    vExec.Add(vIns_Sql);

    vSqlUpdate := 'update PRODUTOS set ATIVO = :P1 where PRODUTO_ID = :P2';
    vExecUpdate.Add(vSqlUpdate);

    try
      vSql := 'select CADASTRO_ID from CADASTROS where CADASTRO_ID = ''' + fornecedor_padrao + ''' and E_FORNECEDOR = ''S'' ';
      cadastro_id := Busca_Registro(FConsulta_dest,vSql,'cadastro_id',MemErro);
      if cadastro_id = '' then begin
        ShowMessage('Informe um fornecedor padr�o v�lido!');
        Exit;
      end;

      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to Length(Fproduto) -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(Fproduto[i].nome));
        vSql := 'select NOME from PRODUTOS where NOME = ''' + vNome + ''' ';

        if Busca_Registro(FConsulta_dest,vSql,'NOME',MemErro) <> '' then
          Continue;

        vSql := 'select PRODUTO_ID from PRODUTOS where CODIGO_BARRAS = ''' + Fproduto[i].codigo_barras + ''' ';

        if Busca_Registro(FConsulta_dest,vSql,'PRODUTO_ID',MemErro) <> '' then
          Fproduto[i].codigo_barras := '';

        vSql :='select unidade_id from unidades where UNIDADE_ID = ''' +
               UpperCase(Tira_Aspas(Fproduto[i].unidade)) + ''' ';

        Fproduto[i].unidade := Busca_Registro(FConsulta_dest,vSql,'unidade_id',MemErro);
        if Fproduto[i].unidade = '' then
          Fproduto[i].unidade := 'UN';

        vSql :='select marca_id from marcas where NOME = ''' + Fproduto[i].cod_marca + ''' ';

        Fproduto[i].cod_marca := Busca_Registro(FConsulta_dest,vSql,'marca_id',MemErro);
        if Fproduto[i].cod_marca = '' then
          Fproduto[i].cod_marca := '1';

        vSql := 'select CADASTRO_ID from CADASTROS where CHAVE_IMPORTACAO = ''' + IntToStr(FProduto[i].fornecedor_id) + ''' and E_FORNECEDOR = ''S'' ';

        fornecedor_produto_id := Busca_Registro(FConsulta_dest,vSql,'cadastro_id',MemErro);
        if fornecedor_produto_id = '' then
          Fproduto[i].fornecedor_id := StrToInt(cadastro_id)
        else
          Fproduto[i].fornecedor_id := StrToInt(fornecedor_produto_id);

          //Valida��o se j� existe produto com mesma NOME e MARCA
        //vSql_Validacao :='select ' + vChave_dest +
        //                 ' from ' + vNome_Tabela_Dest +
        //                 ' where nome = ''' + vNome + ''' ' +
        //                 ' and marca_id = ''' + Fproduto[i].cod_marca + '''';
        //Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        Existe_Tabela := false;
        if (not Existe_Tabela) then
        begin
          vId := StrToInt(Max_Consulta(FConsulta_dest,vChave_dest,vNome_Tabela_Dest));
          vexec.Executar([
                          vId,
                          vNome,
                          vNome,
                          Fproduto[i].fornecedor_id,
                          Fproduto[i].unidade,
                          Fproduto[i].cod_marca,
                          Fproduto[i].multiplo_venda,
                          0,
                          0,
                          '999.999.999',
                          Fproduto[i].codigo_barras,
                          'S',
                          'N',
                          Fproduto[i].ncm,
                          Fproduto[i].codigo_original_fabricante,
                          Fproduto[i].codigo,
                          vId,
                          Fproduto[i].cest,
                          Fproduto[i].tipo_def_automatica_lote
                          ]);

          vExecUpdate.Executar([Fproduto[i].ativo, vId]);

          vSqlProdutos :=
            'insert into ESTOQUES( ' +
            '  EMPRESA_ID, ' +
            '  PRODUTO_ID, ' +
            '  BLOQUEADO, ' +
            '  COMPRAS_PENDENTES, ' +
            '  DISPONIVEL, ' +
            '  ESTOQUE, ' +
            '  FISICO, ' +
            '  MAXIMO, ' +
            '  MINIMO, ' +
            '  RESERVADO ' +
            '  ) VALUES ( ' +
            '  1, ' +
            '  :P1, ' +
            '  0, ' +
            '  0, ' +
            '  0, ' +
            '  0, ' +
            '  0, ' +
            '  0, ' +
            '  0, ' +
            '  0 ' +
            ') ';

          vExecucao.Limpar;
          vExecucao.Add(vSqlProdutos);
          vExecucao.Executar([vId]);

          vSqlProdutos :=
            'insert into PRODUTOS_LOTES( ' +
            '  LOTE, ' +
            '  PRODUTO_ID, ' +
            '  DATA_FABRICACAO, ' +
            '  DATA_HORA_ALTERACAO, ' +
            '  DATA_VENCIMENTO, ' +
            '  ESTACAO_ALTERACAO, ' +
            '  ROTINA_ALTERACAO, ' +
            '  USUARIO_SESSAO_ID ' +
            ') values ( ' +
            '  ''???'', ' +
            '  :P1, ' +
            '  NULL, ' +
            '  sysdate, ' +
            '  NULL, ' +
            '  ''IMP'', ' +
            '  ''IMPORTACAO'', ' +
            '  1 ' +
            ' ) ';

          vExecucao.Limpar;
          vExecucao.Add(vSqlProdutos);
          vExecucao.Executar([vId]);

          vSqlProdutos :=
            'INSERT INTO ESTOQUES_DIVISAO( ' +
            '  EMPRESA_ID, ' +
            '  LOCAL_ID, ' +
            '  LOTE, ' +
            '  PRODUTO_ID, ' +
            '  DATA_HORA_ALTERACAO, ' +
            '  ESTACAO_ALTERACAO, ' +
            '  ROTINA_ALTERACAO, ' +
            '  USUARIO_SESSAO_ID ' +
            ') VALUES ( ' +
            '  1, ' +
            '  1, ' +
            '  ''???'', ' +
            '  :P1, ' +
            '  sysdate, ' +
            '  ''IMP'', ' +
            '  ''IMPORTACAO'', ' +
            '  1 ' +
            ') ';

          vExecucao.Limpar;
          vExecucao.Add(vSqlProdutos);
          vExecucao.Executar([vId]);

          vSqlProdutos :=
            'insert into PRODUTOS_GRUPOS_TRIB_EMPRESAS( ' +
            '  EMPRESA_ID, ' +
            '  PRODUTO_ID, ' +
            '  DATA_HORA_ALTERACAO, ' +
            '  ESTACAO_ALTERACAO, ' +
            '  GRUPO_TRIB_ESTADUAL_COMPRA_ID, ' +
            '  GRUPO_TRIB_ESTADUAL_VENDA_ID, ' +
            '  GRUPO_TRIB_FEDERAL_COMPRA_ID, ' +
            '  GRUPO_TRIB_FEDERAL_VENDA_ID, ' +
            '  ROTINA_ALTERACAO, ' +
            '  USUARIO_SESSAO_ID ' +
            ') values ( ' +
            '  1, ' +
            '  :P1, ' +
            '  sysdate, ' +
            '  ''IMP'', ' +
            '  :P2, ' +
            '  :P3, ' +
            '  :P4, ' +
            '  :P5, ' +
            '  ''IMPORTACAO'', ' +
            '  1 ' +
            ') ';

          vExecucao.Limpar;
          vExecucao.Add(vSqlProdutos);
          vExecucao.Executar([
            vId,
            Fproduto[i].tributacao_estadual_compra,
            Fproduto[i].tributacao_estadual_venda,
            Fproduto[i].tributacao_federal_compra,
            Fproduto[i].tributacao_federal_venda
          ]);

          vSqlProdutos :=
            'insert into CUSTOS_PRODUTOS( ' +
            '  EMPRESA_ID, ' +
            '  PRODUTO_ID, ' +
            '  DATA_HORA_ALTERACAO, ' +
            '  ROTINA_ALTERACAO, ' +
            '  USUARIO_SESSAO_ID ' +
            ') values ( ' +
            '  1, ' +
            '  :P1, ' +
            '  sysdate, ' +
            '  ''IMPORTACAO'', ' +
            '  1 ' +
            ') ';

          vExecucao.Limpar;
          vExecucao.Add(vSqlProdutos);
          vExecucao.Executar([vId]);

          vSqlProdutos :=
            'insert into PRECOS_PRODUTOS( ' +
            '  EMPRESA_ID, ' +
            '  PRODUTO_ID, ' +
            '  DATA_HORA_ALTERACAO, ' +
            '  ROTINA_ALTERACAO, ' +
            '  USUARIO_SESSAO_ID ' +
            ') values ( ' +
            '  1, ' +
            '  :P1, ' +
            '  sysdate, ' +
            '  ''IMPORTACAO'', ' +
            '  1 ' +
            ') ';

          vExecucao.Limpar;
          vExecucao.Add(vSqlProdutos);
          vExecucao.Executar([vId]);

          vExecOriginal.Limpar;
          vExecOriginal.Add('insert into PRODUTOS_FORN_COD_ORIGINAIS ' +
                    '(CODIGO_ORIGINAL, FORNECEDOR_ID, PRODUTO_ID) values(:P1, :P2, :P3)');
          vExecOriginal.Executar([
            Fproduto[i].codigo_original_fabricante,
            Fproduto[i].fornecedor_id,
            vId]
          );

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(Fproduto[i].codigo + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(Fproduto[i].codigo + ' - '+ vNome);
        end;

      end;

      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    vExec.Limpar;
    vExec.Add('alter trigger PRODUTOS_IU_BR enable');
    vexec.Executar;
    vExec.Limpar;

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Multiplos_Compra(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao,vIdAnt:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,vId: Integer;
  vExec:TExecucao;
  Existe_Tabela:Boolean;
  vQUANTIDADE_GRUPO_COMPRA,vEMBALAGEM_FRACIONADA :Double;
begin
  Result := False;

  vNome_Tabela_Dest := 'PRODUTOS_MULTIPLOS_COMPRA';
  vNome_Tabela_Orig := 'PRODUTOS';
  vChave_orig := 'PRODUTO_ID';
  vChave_dest := 'PRODUTO_ID';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    vSql := 'select * from ' + vNome_Tabela_Orig;// + ' where ativo = ''S''';

    lblQuant.caption := Num_Registros(0,FConsulta_orig,'('+vSql+')',MemErro);

    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'PRODUTO_ID ' +
               ',MULTIPLO ' +
               ',QUANTIDADE_EMBALAGEM ' +
               ',UNIDADE_ID ' +
               ',ORDEM ' +
               ') values ( '+
              NParametros(5);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      J:= 0;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vId := FConsulta_orig.GetInt(vChave_orig);

        vSql_Validacao :='select ' + vChave_dest +
                         ' from ' + vNome_Tabela_Dest +
                         ' where ' + vChave_dest + ' = '+ IntToStr(vId);
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        vQUANTIDADE_GRUPO_COMPRA := FConsulta_orig.GetDouble('QUANTIDADE_GRUPO_COMPRA');
        vEMBALAGEM_FRACIONADA := FConsulta_orig.GetDouble('EMBALAGEM_FRACIONADA');

        if (not Existe_Tabela) and (vQUANTIDADE_GRUPO_COMPRA > 0) and (vEMBALAGEM_FRACIONADA > 0) then
        begin
          if vIdAnt = FConsulta_orig.GetString('PRODUTO_ID') then
            j:= J +1
          else
            j:= 1;

          vexec.Executar([
                         FConsulta_orig.GetString('PRODUTO_ID'),
                         vQUANTIDADE_GRUPO_COMPRA,
                         vEMBALAGEM_FRACIONADA,
                         FConsulta_orig.GetString('UNIDADE_COMPRA'),
                         j
                         ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;

  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_PRODUTOS_Relacionados(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,vId: Integer;
  vExec:TExecucao;
  Existe_Tabela,Existe_Relacionamento:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'PRODUTOS_RELACIONADOS';
  vNome_Tabela_Orig := 'PRODUTOS_AGREGADOS';
  vChave_orig := 'PRODUTO_AGREGADO_ID';
  vChave_dest := 'PRODUTO_PRINCIPAL_ID';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql := 'select * from ' + vNome_Tabela_Orig;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'PRODUTO_PRINCIPAL_ID '+
               ',PRODUTO_RELACIONADO_ID '+
               ',QUANTIDADE_SUGESTAO '+
               ') values ( '+
              NParametros(3);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName(vChave_orig).AsString));
        vId := FConsulta_orig.FieldByName(vChave_orig).AsInteger;

        vSql_Validacao :='select ' + vChave_dest +
                         ' from ' + vNome_Tabela_Dest +
                         ' where ' + vChave_dest + ' = '+ IntToStr(vId);
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        vSql_Validacao :='select produto_id '+
                         ' from produtos ' +
                         ' where produto_id = '+ IntToStr(vId);
        Existe_Relacionamento := Existe_Registro(FConsulta_dest,vSql_Validacao,'produto_id',MemErro);

        if not Existe_Tabela and Existe_Relacionamento then
        begin
          vexec.Executar([
                          vId,
                          FConsulta_orig.GetString('PRODUTO_ID'),
                          1
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_PRODUTOS_kit(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq,
  vIdAnt:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,vId: Integer;
  vExec:TExecucao;
  Existe_Relacionamento:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'PRODUTOS_KIT';
  vNome_Tabela_Orig := 'PRODUTOS_KIT';
  vChave_orig := 'PRODUTO_COMPOSICAO_ID';
  vChave_dest := 'PRODUTO_KIT_ID';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql := 'select * from ' + vNome_Tabela_Orig + ' order by PRODUTO_ID';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'PRODUTO_KIT_ID ' +
               ',PRODUTO_ID ' +
               ',ORDEM' +
               ',PERC_PARTICIPACAO'+
               ',QUANTIDADE'+
               ') values ( '+
              NParametros(5);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 0;
      vIdAnt := '';
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := FConsulta_orig.GetString('PRODUTO_COMPOSICAO_ID');
        vId := FConsulta_orig.GetInt('PRODUTO_ID');

        vSql_Validacao :='select produto_id '+
                         ' from produtos ' +
                         ' where produto_id = '+ IntToStr(vId);
        Existe_Relacionamento := Existe_Registro(FConsulta_dest,vSql_Validacao,'produto_id',MemErro);

        //if FConsulta_orig.GetString('PRODUTO_COMPOSICAO_ID') = '862' then

        if Existe_Relacionamento then
        begin
          if vIdAnt = FConsulta_orig.GetString('PRODUTO_ID') then
            j:= J +1
          else
            j:= 1;

          vexec.Executar([
                         FConsulta_orig.GetString('PRODUTO_ID'),
                         FConsulta_orig.GetString('PRODUTO_COMPOSICAO_ID'),
                         j,
                         FConsulta_orig.GetDouble('PERCENTUAL_PRECO'),
                         FConsulta_orig.GetDouble('QUANTIDADE')
                          ]);
          vIdAnt := FConsulta_orig.GetString('PRODUTO_ID');

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Transportadoras(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,vId: Integer;
  vExec:TExecucao;
  Existe_Tabela,Existe_Relacionamento:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'TRANSPORTADORAS';
  vNome_Tabela_Orig := 'TRANSPORTADORAS';
  vChave_orig := 'cadastro_id';
  vChave_dest := 'cadastro_id';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    vSql :='(select CADASTRO_id,trunc(DATA_HORA_ALTERACAO) as DATA_HORA_ALTERACAO,ATIVA ' +
           'from Transportadoras order by CADASTRO_id)';
    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql := 'select CADASTRO_id,trunc(DATA_HORA_ALTERACAO) as DATA_HORA_ALTERACAO,ATIVA ' +
            'from Transportadoras order by CADASTRO_id';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'CADASTRO_ID '+ //NUMBER(10,0),
               ',DATA_HORA_CADASTRO '+ //DATE DEFAULT trunc(sysdate)  NOT NULL,
               ') values ( '+
              NParametros(2);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('cadastro_id').AsString));
        vId := FConsulta_orig.FieldByName(vChave_orig).AsInteger;

        vSql_Validacao :='select ' + vChave_dest +
                         ' from ' + vNome_Tabela_Dest +
                         ' where ' + vChave_dest + ' = '+ IntToStr(vId);
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        vSql_Validacao :='select ' + vChave_dest +
                         ' from cadastros ' +
                         ' where ' + vChave_dest + ' = '+ IntToStr(vId);
        Existe_Relacionamento := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);


        if (not Existe_Tabela) and Existe_Relacionamento then
        begin
          vexec.Executar([
                         vId,
                         FConsulta_orig.GetData('DATA_HORA_ALTERACAO')
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Fornecedores(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_dest,vNome_seq,vUf,vCid,vBairro:string;
  vCnpj,vInsc,vEc, vCad:string;
  FConsulta_dest : TConsulta;
  pesq: TConsulta;
  i,vId,maxId: Integer;
  vExec:TExecucao;
  vExecUpdate:TExecucao;
  Fcadastro:TArray<RecCadastro>;
  sheet: OleVariant;
  vPlanilha: OleVariant;
  Existe_Tabela:Boolean;
  tipo_cliente: string;
begin
  Result := False;

  Fcadastro := nil;
  //Crio o objeto que gerencia o arquivo excel
  vPlanilha:= CreateOleObject('Excel.Application');

  //Abro o arquivo
  if FrmMenuPrincipal.edtArq.Text = '' then
  begin
    ShowMessage('Informe o Caminho da Planilha!' );
    Abort;
  end;
  vPlanilha.WorkBooks.open(FrmMenuPrincipal.edtArq.Text);

  //Pego a primeira vPlanilha do arquivo
  sheet := vPlanilha.WorkSheets[1];

  i := 2;
  lblQuant.caption := '0';
  lblImp.caption   := '0';
  lblDupl.caption  := '0';
  while Trim(sheet.Cells[i, 2]) <> '' do begin
    SetLength(Fcadastro, Length(Fcadastro) + 1);
    Fcadastro[High(Fcadastro)].Cadastro_id := sheet.cells[i, 1].value;
    Fcadastro[High(Fcadastro)].plano_financeiro_revenda := sheet.cells[i, 2].value;

    Inc(i);

    lblQuant.caption := NFormat( Length(Fcadastro) );
    lblQuant.Refresh;
  end;

  //Fecho a vPlanilha
  vPlanilha.WorkBooks.Close;

  vNome_Tabela_Dest := 'FORNECEDORES';
  vChave_dest := 'cadastro_id';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  pesq := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);
  vExecUpdate := TExecucao.Create(Conexao_dest);

  try
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;

    vIns_Sql := 'insert into '+ vNome_Tabela_Dest +
                '( '+
                'CADASTRO_ID, REVENDA, PLANO_FINANCEIRO_REVENDA '+    //NUMBER(10,0),
                ') values ( '+
                NParametros(3);
    vExec.Add(vIns_Sql);

    vIns_Sql := 'update CADASTROS set E_FORNECEDOR = ''S'' where CADASTRO_ID = :P1';
    vExecUpdate.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to Length(Fcadastro) -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vCad := Fcadastro[i].Cadastro_id;
        vSql := 'select CADASTRO_ID from CADASTROS where CHAVE_IMPORTACAO = ' + Fcadastro[i].Cadastro_id;

        pesq.SQL.Text := vSql;

        if not pesq.Pesquisar then begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(Fcadastro[i].Cadastro_id);
          Continue;
        end;

        Fcadastro[i].plano_financeiro_revenda := StringReplace(Fcadastro[i].plano_financeiro_revenda, ',', '.',
         [rfReplaceAll, rfIgnoreCase]);

        vexec.Executar([
                        pesq.GetString('CADASTRO_ID'),
                        'S',
                        Fcadastro[i].plano_financeiro_revenda
                        ]);

        vExecUpdate.Executar([vCad]);

        lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
        MemHist.Lines.Add(Fcadastro[i].Cadastro_id);
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;

  finally
    FConsulta_dest.Free;
    vExec.Free;
    vExecUpdate.Free;
  end;
end;

function Importa_Funcionarios(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave,vNome_seq,vCargo:string;
  vEnd,vComp,vCad:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,vId,vFilhos : Integer;
  vExec:TExecucao;
  vVend,vCaixa,vComprador,vMotorista :string;
  vData,vDtRg,vData_adm,vDtNasc,vDtCad : Variant;
  vExec_up:TExecucao;
  pFoto: TMemoryStream;
begin
  Result := False;

  vNome_Tabela_Dest := 'FUNCIONARIOS';
  vNome_Tabela_Orig := 'FUNCIONARIOS';
  vChave := 'FUNCIONARIO_ID';
  vNome_seq:= 'SEQ_FUNCIONARIO_ID';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(Conexao_orig);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);
  vExec_up  := TExecucao.Create(Conexao_dest);

  try
    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := 'where funcionario_id > 1';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    vSql :='(select f.*,c.DATA_NASCIMENTO,c.cpf,c.RG,c.ORGAO_EXPEDIDOR, '+
           'c.LOGRADOURO,c.COMPLEMENTO,c.BAIRRO_ID,c.CEP ' +
           'from funcionarios f,cadastros c '+
           'where f.cadastro_id= c.cadastro_id  ' +
           '  and f.funcionario_id > 1 )';
    lblQuant.caption := Num_Registros(0,FConsulta_orig,vSql,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql :='select f.*,c.DATA_NASCIMENTO,c.cpf,c.RG,c.ORGAO_EXPEDIDOR, '+
           'c.LOGRADOURO,c.COMPLEMENTO,c.BAIRRO_ID,c.CEP ' +
           'from funcionarios f,cadastros c '+
           'where f.cadastro_id= c.cadastro_id  ' +
           '  and f.funcionario_id > 1  order by f.funcionario_id'; //and f.ativo = ''S''

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+'( '+
              'FUNCIONARIO_ID ' + //NUMBER(4,0),
              ',NOME ' + //VARCHAR2(60) NOT NULL,
              ',APELIDO ' + //VARCHAR2(40),
              ',DATA_CADASTRO ' + //DATE DEFAULT trunc(sysdate)  NOT NULL,
              ',DATA_NASCIMENTO ' + //DATE NOT NULL,
              ',CARGO_ID ' + //NUMBER(3,0) NOT NULL,
              ',CPF ' + //VARCHAR2(14),
              ',RG ' + //VARCHAR2(10),
              ',ORGAO_EXPEDIDOR_RG ' + //VARCHAR2(10),
              ',DATA_ADMISSAO ' + //DATE NOT NULL,

              ',LOGRADOURO ' + //VARCHAR2(100) NOT NULL,
              ',COMPLEMENTO ' + //VARCHAR2(100) NOT NULL,
              ',NUMERO ' + //VARCHAR2(10) DEFAULT 'SN'  NOT NULL,
              ',BAIRRO_ID ' + //NUMBER(15,0) NOT NULL,
              ',CEP ' + //VARCHAR2(9),
              ',E_MAIL ' + //VARCHAR2(30),
              ',SERVIDOR_SMTP ' + //VARCHAR2(50),
              ',USUARIO_E_MAIL ' + //VARCHAR2(30),
              ',SENHA_E_MAIL ' + //VARCHAR2(32),
              ',PORTA_E_MAIL ' + //NUMBER(5,0),

              ',CADASTRO_ID ' + //NUMBER(10,0) NOT NULL,
              ',PERCENTUAL_COMISSAO_A_VISTA ' + //NUMBER(4,2) DEFAULT 0 NOT NULL,
              ',PERCENTUAL_COMISSAO_A_PRAZO ' + //NUMBER(4,2) DEFAULT 0 NOT NULL,
              ',SALARIO ' + //NUMBER(7,2) DEFAULT 0 NOT NULL,
              ',ATIVO ' + //CHAR(1) DEFAULT 'S'  NOT NULL,
              ',VENDEDOR ' + //CHAR(1) DEFAULT 'N'  NOT NULL,
              ',CAIXA ' + //CHAR(1) DEFAULT 'N'  NOT NULL,
              ',COMPRADOR ' + //CHAR(1) DEFAULT 'N'  NOT NULL,
              ',DATA_DESLIGAMENTO ' + //DATE,
              ',NUMERO_CARTEIRA_TRABALHO ' + //VARCHAR2(30),

              ',SERIE_CARTEIRA_TRABALHO ' + //VARCHAR2(10),
              ',PIS_PASEP ' + //VARCHAR2(30),
              ',MOTORISTA ' + //CHAR(1) DEFAULT 'N'  NOT NULL,
              ',TITULO_ELEITORAL ' + //CVARCHAR2(15),
              ',SECAO_TITULO_ELEITORAL ' + //CVARCHAR2(5),
              ',ZONA_TITULO_ELEITORAL ' + //CVARCHAR2(5),
              ',RESERVISTA ' + //CVARCHAR2(15),
              ',DATA_EMISSAO_RG ' + //CDATE,
              ',OBSERVACAO ' + //CVARCHAR2(400),
              ',QUANTIDADE_FILHOS ' + //CNUMBER(2,0) DEFAULT 0 NOT NULL,

              ',USUARIO_ALTIS ' + //CCHAR(1) DEFAULT 'N'  NOT NULL,
              ') values ( '+
              NParametros(41);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        Application.ProcessMessages;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));
        vId := FConsulta_orig.FieldByName(vChave).AsInteger;

        vVend := 'N';
        vCaixa := 'N';
        vComprador := 'N';
        vMotorista := 'N';
        vFilhos := 0;
        if FConsulta_orig.GetString('POSSUI_FILHOS') = 'S' then
            vFilhos := 1;

        if FConsulta_orig.GetInt('cargo_id') = 2 then
          vVend := 'S';

        if FConsulta_orig.GetInt('cargo_id') = 3 then
          vCaixa := 'S';

        if FConsulta_orig.GetInt('cargo_id') = 8 then
          vComprador := 'S';

        if FConsulta_orig.GetInt('cargo_id') = 506 then
          vMotorista := 'S';

        vData_adm := FConsulta_orig.GetString('DATA_ADMISSAO');
        if (vData_adm <> '') then
          vData_adm := FConsulta_orig.GetData('DATA_ADMISSAO')
        else
          vData_adm := FConsulta_orig.GetData('DATA_CADASTRO');

        vData := FConsulta_orig.GetString('DATA_DESLIGAMENTO');
        if (vData <> '') then
        begin
           if (FConsulta_orig.GetData('DATA_DESLIGAMENTO') <= FConsulta_orig.GetData('DATA_ADMISSAO')) then
             vData := FConsulta_orig.GetData('DATA_ADMISSAO') + 1
           else
             vData := FConsulta_orig.GetData('DATA_DESLIGAMENTO');
        end else
          vData := System.Variants.Null;

        if (FConsulta_orig.GetString('DATA_EMISSAO_RG') = '') or
           (FConsulta_orig.GetString('DATA_EMISSAO_RG') = '30/12/1899') then
          vDtRg := System.Variants.Null
        else
          vDtRg := FConsulta_orig.GetData('DATA_EMISSAO_RG');

        if (FConsulta_orig.GetString('DATA_CADASTRO') = '') or
           (FConsulta_orig.GetString('DATA_CADASTRO') = '30/12/1899') then
          vDtCad := Date
        else
          vDtCad := FConsulta_orig.GetData('DATA_CADASTRO');

        if FConsulta_orig.GetString('DATA_NASCIMENTO') = '' then
          vDtNasc := System.Variants.Null
        else
          vDtNasc := FConsulta_orig.GetData('DATA_NASCIMENTO');

        vCargo := Busca_Registro(FConsulta_dest,'select CARGO_ID from CARGO_ID where cargo_id = ' + FConsulta_orig.GetString('CARGO_ID'),'CARGO_ID',MemErro);

        vEnd := Tira_Aspas(FConsulta_orig.GetString('LOGRADOURO'));
        vComp:= Tira_Aspas(FConsulta_orig.GetString('COMPLEMENTO'));

        if FConsulta_orig.GetInt('FUNCIONARIO_ID') = 67 then
          vCad := FConsulta_orig.GetString('CADASTRO_ID');

        vCad := FConsulta_orig.GetString('CADASTRO_ID');
        FConsulta_dest.Limpar;
        vSql_Validacao :='select cadastro_id from cadastros where ' +
                         'CPF_CNPJ = ''' + FConsulta_orig.GetString('cpf') +''' ';
        FConsulta_dest.Add(vSql_Validacao);
        if FConsulta_dest.Pesquisar then
          vCad := FConsulta_dest.GetString('CADASTRO_ID')
        else
          vCad := '';

        FConsulta_dest.Limpar;
        vSql_Validacao :='select  f.funcionario_id '+
         'from  funcionarios f '+
         'where f.CPF = ''' + FConsulta_orig.GetString('cpf') +''' or f.cadastro_id =' + FConsulta_orig.GetString('CADASTRO_ID');
        FConsulta_dest.Add(vSql_Validacao);
        if (not FConsulta_dest.Pesquisar) and (vCad <> '') then begin

          vexec.Executar([
                      FConsulta_orig.GetInt('FUNCIONARIO_ID'),
                      FConsulta_orig.GetString('NOME'),
                      FConsulta_orig.GetString('NOME'),
                      FConsulta_orig.GetData('DATA_CADASTRO'),
                      FConsulta_orig.GetData('DATA_NASCIMENTO'),
                      vCargo,
                      FConsulta_orig.GetString('cpf'),
                      FConsulta_orig.GetString('RG'),
                      FConsulta_orig.GetString('ORGAO_EXPEDIDOR'),
                      FConsulta_orig.GetData('DATA_ADMISSAO'),

                      vEnd,
                      vComp,
                      'SN',
                      FConsulta_orig.GetString('BAIRRO_ID'),
                      FConsulta_orig.GetString('CEP'),
                      FConsulta_orig.GetString('E_MAIL'),
                      'rl-15us.hmservers.net',
                      FConsulta_orig.GetString('USUARIO_SMTP'),
                      FConsulta_orig.GetString('SENHA_SMTP'),
                      '587',

                      vCad,
                      FConsulta_orig.GetDouble('COMISSAO_VISTA'),
                      FConsulta_orig.GetString('COMISSAO_PRAZO'),
                      FConsulta_orig.GetString('SALARIO'),
                      FConsulta_orig.GetString('ATIVO'),
                      vVend,
                      vCaixa,
                      vComprador,
                      vData,
                      FConsulta_orig.GetString('CARTEIRA_TRABALHO'),

                      FConsulta_orig.GetString('SERIE_CARTEIRA_TRABALHO'),
                      FConsulta_orig.GetString('PIS_PASEP'),
                      vMotorista,
                      FConsulta_orig.GetString('TITULO_ELEITORAL'),
                      FConsulta_orig.GetString('SESSAO_TITULO_ELEITORAL'),
                      FConsulta_orig.GetString('ZONA_TITULO_ELEITORAL'),
                      FConsulta_orig.GetString('RESERVISTA'),
                      vDtRg,
                      FConsulta_orig.GetString('OBSERVACAO'),
                      vFilhos,

                      FConsulta_orig.GetString('USUARIO')
                       ]);
          pFoto := getFoto(Conexao_orig,FConsulta_orig.GetInt('FUNCIONARIO_ID'));

          if pFoto <> nil then begin
            vExec_up.Limpar;
            vExec_up.SQL.Add('update FUNCIONARIOS set ');
            vExec_up.SQL.Add('  FOTO = :P1 ');
            vExec_up.SQL.Add('where FUNCIONARIO_ID = :P2');
            vExec_up.CarregarBinario('P1', pFoto);
            vExec_up.ParamByName('P2').AsInteger := FConsulta_orig.GetInt('FUNCIONARIO_ID');
            vExec_up.Executar;
          end;

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          vExec_up.Limpar;

          if (vVend = 'S') then
          begin
            vExec_up.SQL.Add('update funcionarios set VENDEDOR = ''' + vVend + ''' where funcionario_id = ' + FConsulta_dest.GetString('funcionario_id'));
            vExec_up.Executar;
          end;
          if (vCaixa = 'S') then
          begin
            vExec_up.SQL.Add('update funcionarios set caixa = ''' + vCaixa + ''' where funcionario_id = ' + FConsulta_dest.GetString('funcionario_id'));
            vExec_up.Executar;
          end;
          if (vComprador = 'S') then
          begin
            vExec_up.SQL.Add('update funcionarios set comprador = ''' + vComprador + ''' where funcionario_id = ' + FConsulta_dest.GetString('funcionario_id'));
            vExec_up.Executar;
          end;

          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;

        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
    vExec_up.Free;
  end;

end;

function Importa_CLIENTES_GRUPOS(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,vId: Integer;
  vExec:TExecucao;
begin
  Result := False;

  vNome_Tabela_Dest := 'CLIENTES_GRUPOS';
  vNome_Tabela_Orig := 'GRUPOS_CLIENTES';
  vChave_orig := 'GRUPO_CLIENTES_ID';
  vChave_dest := 'CLIENTE_GRUPO_ID';
  vNome_seq:= 'SEQ_CLIENTES_GRUPOS_ID';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql := 'select * from ' + vNome_Tabela_Orig;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'CLIENTE_GRUPO_ID ' +// number(3),
               ',NOME ' +//     varchar(30) not null,
               ') values ( '+
              NParametros(2);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));
        vId := FConsulta_orig.FieldByName(vChave_orig).AsInteger;

        vSql_Validacao := 'select CLIENTE_GRUPO_ID from CLIENTES_GRUPOS where NOME = ''' + vNome + ''' ';

        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        if not Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro) then
        begin
          vexec.Executar([
                          vId,
                          vNome
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_CLIENTES_ORIGENS(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,vId: Integer;
  vExec:TExecucao;
begin
  Result := False;

  vNome_Tabela_Dest := 'CLIENTES_ORIGENS';
  vNome_Tabela_Orig := 'ORIGENS_CLIENTES';
  vChave_orig := 'ORIGEM_CLIENTE_ID';
  vChave_dest := 'CLIENTE_ORIGEM_ID';
  vNome_seq:= 'SEQ_CLIENTES_ORIGENS_ID';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql := 'select * from ' + vNome_Tabela_Orig;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'CLIENTE_ORIGEM_ID ' +// number(3),
               ',NOME ' +//     varchar(30) not null,
               ') values ( '+
              NParametros(2);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));
        vId := FConsulta_orig.FieldByName(vChave_orig).AsInteger;

        vSql_Validacao := 'select CLIENTE_ORIGEM_ID from CLIENTES_ORIGENS where NOME = ''' + vNome + ''' ';

        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        if not Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro) then
        begin
          vexec.Executar([
                          vId,
                          vNome
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_GRUPOS_FINANCEIROS(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,vId: Integer;
  vExec:TExecucao;
begin
  Result := False;

  vNome_Tabela_Dest := 'GRUPOS_FINANCEIROS';
  vNome_Tabela_Orig := 'GRUPOS_FINANCEIROS';
  vChave_orig := 'GRUPO_FINANCEIRO_ID';
  vChave_dest := 'GRUPO_FINANCEIRO_ID';
  vNome_seq:= 'SEQ_GRUPO_FINANCEIRO_ID';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql := 'select * from ' + vNome_Tabela_Orig;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'GRUPO_FINANCEIRO_ID ' +// number(3),
               ',NOME ' +//     varchar(30) not null,
               ') values ( '+
              NParametros(2);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));
        vId := FConsulta_orig.FieldByName(vChave_orig).AsInteger;

        vSql_Validacao := 'select GRUPO_FINANCEIRO_ID from GRUPOS_FINANCEIROS where NOME = ''' + vNome + ''' ';

        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        if not Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro) then
        begin
          vexec.Executar([
                          vId,
                          vNome
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Clientes(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_dest,vNome_seq,vUf,vCid,vBairro:string;
  vCnpj,vInsc,vEc, vCad:string;
  FConsulta_dest : TConsulta;
  pesq: TConsulta;
  i,vId,maxId: Integer;
  vExec:TExecucao;
  vExecUpdate, vExecUpdateLimite:TExecucao;
  Fcadastro:TArray<RecCadastro>;
  sheet: OleVariant;
  vPlanilha: OleVariant;
  Existe_Tabela:Boolean;
  tipo_cliente: string;
begin
  Result := False;

  Fcadastro := nil;
  //Crio o objeto que gerencia o arquivo excel
  vPlanilha:= CreateOleObject('Excel.Application');

  //Abro o arquivo
  if FrmMenuPrincipal.edtArq.Text = '' then
  begin
    ShowMessage('Informe o Caminho da Planilha!' );
    Abort;
  end;
  vPlanilha.WorkBooks.open(FrmMenuPrincipal.edtArq.Text);

  //Pego a primeira vPlanilha do arquivo
  sheet := vPlanilha.WorkSheets[1];

  i := 2;
  lblQuant.caption := '0';
  lblImp.caption   := '0';
  lblDupl.caption  := '0';
  while Trim(sheet.Cells[i, 2]) <> '' do begin
    SetLength(Fcadastro, Length(Fcadastro) + 1);
    Fcadastro[High(Fcadastro)].Cadastro_id := sheet.cells[i, 1].value;
    Fcadastro[High(Fcadastro)].limite_credito := sheet.cells[i, 12].value;

    Inc(i);

    lblQuant.caption := NFormat( Length(Fcadastro) );
    lblQuant.Refresh;
  end;

  //Fecho a vPlanilha
  vPlanilha.WorkBooks.Close;

  vNome_Tabela_Dest := 'CLIENTES';
  vChave_dest := 'cadastro_id';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  pesq := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);
  vExecUpdate := TExecucao.Create(Conexao_dest);
  vExecUpdateLimite := TExecucao.Create(Conexao_dest);

  try
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;

    vExecUpdateLimite.SQL.Add('update CLIENTES set ');
    vExecUpdateLimite.SQL.Add('  LIMITE_CREDITO = :P1, ');
    vExecUpdateLimite.SQL.Add('  LIMITE_CREDITO_MENSAL = :P2, ');
    vExecUpdateLimite.SQL.Add('  DATA_APROVACAO_LIMITE_CREDITO = TRUNC(SYSDATE) ');
    vExecUpdateLimite.SQL.Add('where CADASTRO_ID = :P3 ');

    vIns_Sql := 'insert into '+ vNome_Tabela_Dest +
                '( '+
                'CADASTRO_ID, TIPO_CLIENTE '+    //NUMBER(10,0),
                ') values ( '+
                NParametros(2);
    vExec.Add(vIns_Sql);

    vIns_Sql := 'update CADASTROS set E_CLIENTE = ''S'' where CADASTRO_ID = :P1';
    vExecUpdate.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to Length(Fcadastro) -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vCad := Fcadastro[i].Cadastro_id;
        vSql := 'select CADASTRO_ID, RETORNA_NUMEROS(CPF_CNPJ) as CPF_CNPJ, INSCRICAO_ESTADUAL, TIPO_PESSOA from CADASTROS where CHAVE_IMPORTACAO = ' + Fcadastro[i].Cadastro_id;

        pesq.SQL.Text := vSql;

        if pesq.Pesquisar([vCad]) then begin
           if pesq.GetString('TIPO_PESSOA') = 'F' then begin

             if Length(pesq.GetString('CPF_CNPJ')) = 11 then begin

               if pesq.GetString('INSCRICAO_ESTADUAL') <> 'ISENTO' then
                 tipo_cliente := 'PR'
               else
                 tipo_cliente := 'NC';

             end
             else begin

               if pesq.GetString('INSCRICAO_ESTADUAL') <> 'ISENTO' then
                 tipo_cliente := 'CO'
               else
                 tipo_cliente := 'NC';
             end;

           end
           else begin

             if pesq.GetString('INSCRICAO_ESTADUAL') <> 'ISENTO' then
               tipo_cliente := 'CO'
             else
               tipo_cliente := 'NC';

           end;
        end
        else begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(Fcadastro[i].Cadastro_id);
          Continue;
        end;

        vexec.Executar([
                        pesq.GetString('CADASTRO_ID'),
                        tipo_cliente
                        ]);

        vExecUpdate.Executar([vCad]);

        if (FCadastro[i].limite_credito <> '') and (StrToFloat(FCadastro[i].limite_credito) > 0) then
          vExecUpdateLimite.Executar([
            FCadastro[i].limite_credito,
            FCadastro[i].limite_credito,
            pesq.GetString('CADASTRO_ID')
          ]);

        lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
        MemHist.Lines.Add(Fcadastro[i].Cadastro_id);
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;

  finally
    FConsulta_dest.Free;
    vExec.Free;
    vExecUpdate.Free;
  end;
end;

function Importa_Pessoas(
  vsp:Boolean;
  Conexao_orig:Tconexao;
  Conexao_dest:Tconexao;
  lblHis,
  lblQuant,
  lblImp,
  lblDupl:Tlabel;
  MemHist,
  MemErro,
  MemDupl:Tmemo;
  pBairroPadraoId: string
):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_dest,vNome_seq,vUf,vCid,vBairro:string;
  vCnpj,vInsc,vEc:string;
  FConsulta_dest : TConsulta;
  i,vId,maxId: Integer;
  vExec:TExecucao;
  Fcadastro:TArray<RecCadastro>;
  sheet: OleVariant;
  vPlanilha: OleVariant;
  Existe_Tabela:Boolean;
begin
  Result := False;

  Fcadastro := nil;
  //Crio o objeto que gerencia o arquivo excel
  vPlanilha:= CreateOleObject('Excel.Application');

  //Abro o arquivo
  if FrmMenuPrincipal.edtArq.Text = '' then
  begin
    ShowMessage('Informe o Caminho da Planilha!' );
    Abort;
  end;
  vPlanilha.WorkBooks.open(FrmMenuPrincipal.edtArq.Text);

  //Pego a primeira vPlanilha do arquivo
  sheet := vPlanilha.WorkSheets[1];

  i := 2;
  lblQuant.caption := '0';
  lblImp.caption   := '0';
  lblDupl.caption  := '0';
  while Trim(sheet.Cells[i, 2]) <> '' do begin
    SetLength(Fcadastro, Length(Fcadastro) + 1);
    Fcadastro[High(Fcadastro)].Cadastro_id := sheet.cells[i, 1].value;
    Fcadastro[High(Fcadastro)].tp_pessoa   := sheet.cells[i, 2].value;
    Fcadastro[High(Fcadastro)].Nome        := sheet.cells[i, 3].value;
    Fcadastro[High(Fcadastro)].endereco    := sheet.cells[i, 4].value;
    if Fcadastro[High(Fcadastro)].endereco = '' then
      Fcadastro[High(Fcadastro)].endereco := '...';

    Fcadastro[High(Fcadastro)].Bairro      := sheet.cells[i, 5].value;
    Fcadastro[High(Fcadastro)].Cidade      := sheet.cells[i, 6].value;
    Fcadastro[High(Fcadastro)].Estado      := sheet.cells[i, 7].value;
    Fcadastro[High(Fcadastro)].cep         := sheet.cells[i, 8].value;
    if Fcadastro[High(Fcadastro)].cep = '' then
      Fcadastro[High(Fcadastro)].cep := '74000000';

    Fcadastro[High(Fcadastro)].cep := FormatMaskText('00000\-000;0;', Fcadastro[High(Fcadastro)].cep);

    Fcadastro[High(Fcadastro)].telefone    := sheet.cells[i, 9].value;

    Fcadastro[High(Fcadastro)].telefone    := FormatMaskText('\(00\)\ 00000\-0009;0;', Fcadastro[High(Fcadastro)].telefone);

    Fcadastro[High(Fcadastro)].fax         := sheet.cells[i, 10].value;
    Fcadastro[High(Fcadastro)].celular     := sheet.cells[i, 11].value;

    Fcadastro[High(Fcadastro)].celular     := FormatMaskText('\(00\)\ 00000\-0009;0;', Fcadastro[High(Fcadastro)].celular);

    Fcadastro[High(Fcadastro)].limite_credito  := sheet.cells[i, 12].value;
    Fcadastro[High(Fcadastro)].data_nascimento := sheet.cells[i, 13].value;
    Fcadastro[High(Fcadastro)].cpf        := sheet.cells[i, 14].value;
    Fcadastro[High(Fcadastro)].cnpj       := sheet.cells[i, 15].value;
    Fcadastro[High(Fcadastro)].inscricao  := sheet.cells[i, 16].value;
    Fcadastro[High(Fcadastro)].numero_end := sheet.cells[i, 17].value;
    Fcadastro[High(Fcadastro)].email := sheet.cells[i, 18].value;
    Fcadastro[High(Fcadastro)].complemento := sheet.cells[i, 19].value;
    Fcadastro[High(Fcadastro)].ponto_referencia := sheet.cells[i, 20].value;

    Inc(i);

    lblQuant.caption := NFormat( Length(Fcadastro) );
    lblQuant.Refresh;
  end;

  //Fecho a vPlanilha
  vPlanilha.WorkBooks.Close;

  vNome_Tabela_Dest := 'CADASTROS';
  vChave_dest := 'cadastro_id';
  vNome_seq:= 'SEQ_CADASTRO_ID';


  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;

    vIns_Sql := 'insert into '+vNome_Tabela_Dest+
                '( '+
                'CADASTRO_ID '+    //NUMBER(10,0),
                ',NOME_FANTASIA '+ //VARCHAR2(100) NOT NULL,
                ',RAZAO_SOCIAL '+  //VARCHAR2(100) NOT NULL,
                ',TIPO_PESSOA '+   //CHAR(1) NOT NULL,
                ',CPF_CNPJ '+      //VARCHAR2(19) NOT NULL,
                ',LOGRADOURO '+    //VARCHAR2(100),
                ',COMPLEMENTO '+   //VARCHAR2(100),
                ',NUMERO '+        //VARCHAR2(10) DEFAULT 'SN'  NOT NULL,
                ',BAIRRO_ID '+     //NUMBER(15,0) NOT NULL,
                ',PONTO_REFERENCIA '+ //VARCHAR2(100),
                ',CEP '+ //VARCHAR2(9),
                ',TELEFONE_PRINCIPAL '+ //VARCHAR2(15),
                ',TELEFONE_CELULAR '+ //VARCHAR2(15),
                ',TELEFONE_FAX '+ //VARCHAR2(15),
                ',E_MAIL '+ //VARCHAR2(30),
                ',SEXO '+ //CHAR(1),
                ',ESTADO_CIVIL '+ //CHAR(1),
                ',INSCRICAO_ESTADUAL '+ //VARCHAR2(12) DEFAULT 'ISENTO'  NOT NULL,
                ',CNAE '+ //VARCHAR2(12),
                ',RG '+ //VARCHAR2(10),
                ',ORGAO_EXPEDIDOR_RG '+ //VARCHAR2(10),
                ',E_CLIENTE '+ //CHAR(1) DEFAULT 'N'  NOT NULL,
                ',E_FORNECEDOR '+ //CHAR(1) DEFAULT 'N'  NOT NULL,
                ',ATIVO '+ //CHAR(1) DEFAULT 'S'  NOT NULL,
                ',E_MOTORISTA '+ //CHAR(1) DEFAULT 'N'  NOT NULL,
                ',E_TRANSPORTADORA '+ //CHAR(1) DEFAULT 'N'  NOT NULL,
                ',CHAVE_IMPORTACAO '+ //VARCHAR2(100),
                ',OBSERVACOES ' +//VARCHAR2(800)
                ',DATA_EMISSAO_RG ' +//DATE,
                ',E_CONCORRENTE  ' +//CHAR(1) DEFAULT 'N'   NOT NULL,
                ',E_FUNCIONARIO ' +//CHAR(1) DEFAULT 'N'   NOT NULL,
                ',NACIONALIDADE ' +//VARCHAR2(40),
                ',NATURALIDADE ' +//VARCHAR2(40),
                ',E_PROFISSIONAL ' +//CHAR(1) DEFAULT 'N'   NOT NULL,
                ',E_MAIL_NFE2 ' +//VARCHAR2(80),
                ') values ( '+
                NParametros(35);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to Length(Fcadastro) -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vCnpj := RetornaNumeros(Fcadastro[i].cpf);
        if vCnpj = '' then
          vCnpj := RetornaNumeros(Fcadastro[i].cnpj);

        if (Length(vCnpj) < 12) and (Length(vCnpj) > 8) then begin
          Fcadastro[i].tp_pessoa := 'F';
          if (Length(vCnpj) = 9) then
            vCnpj := '00' + vCnpj;
          if (Length(vCnpj) = 10) then
            vCnpj := '0' + vCnpj;
          vCnpj := Ajusta_Cnpj(vCnpj);
        end
        else begin
          if (Length(vCnpj) < 15) and (Length(vCnpj) > 11) then begin
            if (Length(vCnpj) = 12) then
              vCnpj := '00' + vCnpj;
            if (Length(vCnpj) = 13) then
              vCnpj := '0' + vCnpj;
            Fcadastro[i].tp_pessoa := 'J';
            vCnpj := Ajusta_Cnpj(vCnpj);
          end
          else
            vCnpj := '';
        end;

        if RetornaNumeros(Fcadastro[i].inscricao) = '' then
          vInsc := 'ISENTO'
        else
          vInsc := RetornaNumeros(Fcadastro[i].inscricao);

        vEc := '';

        vNome := UpperCase(Tira_Aspas(Fcadastro[i].Nome));
        if vNome = '' then
          vNome := 'SEM NOME';

        vUf := UpperCase(Tira_Aspas(Fcadastro[i].Estado));

        vSql :='select estado_id from estados where estado_id = ''' + vUf + '''';
        if (vUf = '') or not Existe_Registro(FConsulta_dest,vSql,'estado_id',MemErro) then
          vUf := 'GO';

        vCid := UpperCase(Tira_Aspas(Fcadastro[i].Cidade));
        if vCid = '' then
          vCid:= 'GOIANIA';

        vSql :='select cidade_id from cidades where NOME = ''' + vCid + '''' +
               ' and ESTADO_ID = ''' + vUf + '''';

        vCid:= Busca_Registro(FConsulta_dest,vSql,'cidade_id',MemErro);
        if vCid = '' then
          vCid := '1';

        vBairro := UpperCase(Tira_Aspas(Fcadastro[i].Bairro));

        vSql :='select bairro_id from bairros where NOME = ''' + vBairro + '''' +
               ' and cidade_ID = ' + vCid;

        vBairro:= Busca_Registro(FConsulta_dest,vSql,'bairro_id',MemErro);
        if vBairro = '' then
          vBairro := pBairroPadraoId;

        Existe_Tabela := True;
        FConsulta_dest.Limpar;
        if (getCNPJ_CPFOk(vCnpj)= true) then
          Existe_Tabela := True;
        if (getCNPJ_CPFOk(vCnpj)= true) and (vCnpj <> '') then
        begin
          vSql_Validacao :='select c.cadastro_id '+
                           'from CADASTROS c '+
                           'where c.CPF_CNPJ = ''' + vCnpj +'''';
          Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);
        end;

        if Fcadastro[High(Fcadastro)].complemento = '' then
          Fcadastro[High(Fcadastro)].complemento := '...';

        if Fcadastro[High(Fcadastro)].ponto_referencia = '' then
          Fcadastro[High(Fcadastro)].ponto_referencia := '...';

        if not Existe_Tabela then
        begin
          vId := StrToInt(Max_Consulta(FConsulta_dest,vChave_dest,vNome_Tabela_Dest));
          vexec.Executar([
                          vId,
                          vNome,
                          vNome,
                          Fcadastro[i].tp_pessoa,
                          vCnpj,
                          Fcadastro[i].endereco,
                          Fcadastro[i].complemento,
                          Fcadastro[i].numero_end,
                          vbairro,
                          Fcadastro[i].ponto_referencia,
                          Fcadastro[i].cep,
                          Fcadastro[i].telefone,
			                    Fcadastro[i].celular,
			                    Fcadastro[i].fax,
                          Fcadastro[i].email,
                          '',
                          vEc,
                          vInsc,
                          '',
                          '',
                          '',
                          'N',
                          'N',
                          'S',
                          'N',
                          'N',
                          Fcadastro[i].Cadastro_id,
                          Fcadastro[i].Cadastro_id + ' ' + Fcadastro[i].telefone + ' ' +
                          Fcadastro[i].fax + ' ' + Fcadastro[i].celular,
                          null,
                          'N',
                          'N',
                          null,
                          null,
                          'N',
                          null
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(Fcadastro[i].Cadastro_id + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(Fcadastro[i].Cadastro_id + ' - ' +vNome + ' - ' + Fcadastro[i].cpf + ' - ' + Fcadastro[i].cnpj);
        end;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    vSql := 'insert into CLIENTES '+
            ' ( CADASTRO_ID,ATIVO,RESTRICAO_SPC,RESTRICAO_SERASA ) ' +
            ' select CADASTRO_ID,''S'',''N'',''N'' from cadastros '+
            '  where cadastro_id not in (select cadastro_id from clientes) ';
    roda_sql(True,Conexao_dest,vSql,MemErro);

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;

  finally
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Telefones (vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,vId,maxId: Integer;
  vExec:TExecucao;
begin
  Result := False;

  vNome_Tabela_Dest := 'CADASTROS_TELEFONES';
  vNome_Tabela_Orig := 'TELEFONES_CADASTROS';
  vChave := 'CADASTRO_ID';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    maxId := strtoint(Max_Consulta(FConsulta_orig,vChave,vNome_Tabela_orig));

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql := 'select * from ' + vNome_Tabela_Orig;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'CADASTRO_ID '+//NUMBER(3,0),
               ',TIPO_TELEFONE '+// CHAR(1) NOT NULL,
               ',RAMAL '+// NUMBER(4,0),
               ',TELEFONE '+// VARCHAR2(30),
               ',OBSERVACAO '+// VARCHAR2(100)
               ') values ( '+
               NParametros(5);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('CADASTRO_ID').AsString));
        vId := FConsulta_orig.FieldByName(vChave).AsInteger;
        if vId = 1 then
          vId := MaxId + 1;

        vSql_Validacao := 'select cadastro_id from cadastros where cadastro_id = ' + IntToStr(vId);

        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        if Existe_Registro(FConsulta_dest,vSql_Validacao,vChave,MemErro) then
        begin
          vExec.Limpar;
          vExec.Add(vIns_Sql);
          vexec.Executar([
                          vId,
                          FConsulta_orig.FieldByName('TIPO_TELEFONE').AsString,
                          FConsulta_orig.FieldByName('RAMAL').AsInteger,
                          FConsulta_orig.FieldByName('TELEFONE').AsString,
                          FConsulta_orig.FieldByName('OBSERVACAO').AsString
                          ]);

          vExec.Limpar;
          if FConsulta_orig.FieldByName('TIPO_TELEFONE').AsString = 'R' then
            vExec.Add('UPDATE CADASTROS SET TELEFONE_PRINCIPAL = ''' + FConsulta_orig.FieldByName('TELEFONE').AsString +
                      ' '' WHERE CADASTRO_ID = ' + IntToStr(vId) + ' and TELEFONE_PRINCIPAL = '''' ')
          else
            if FConsulta_orig.FieldByName('TIPO_TELEFONE').AsString = 'C' then
              vExec.Add('UPDATE CADASTROS SET TELEFONE_PRINCIPAL = ''' + FConsulta_orig.FieldByName('TELEFONE').AsString +
                        ' '' WHERE CADASTRO_ID = ' + IntToStr(vId) + ' and TELEFONE_PRINCIPAL = '''' ')
            else
              if FConsulta_orig.FieldByName('TIPO_TELEFONE').AsString = 'F' then
                vExec.Add('UPDATE CADASTROS SET TELEFONE_FAX = ''' + FConsulta_orig.FieldByName('TELEFONE').AsString +
                          ' '' WHERE CADASTRO_ID = ' + IntToStr(vId) + ' and TELEFONE_FAX = '''' ')
              else
                if FConsulta_orig.FieldByName('TIPO_TELEFONE').AsString = 'C' then
                  vExec.Add('UPDATE CADASTROS SET TELEFONE_CELULAR = ''' + FConsulta_orig.FieldByName('TELEFONE').AsString +
                            ' '' WHERE CADASTRO_ID = ' + IntToStr(vId) + ' and TELEFONE_CELULAR = '''' ');

          if vExec.SQL.Text <> '' then
            vExec.Executar;

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;

  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_DIVERSOS_CONTATOS(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,vId: Integer;
  vExec:TExecucao;
begin
  Result := False;

  vNome_Tabela_Dest := 'DIVERSOS_CONTATOS';
  vNome_Tabela_Orig := 'CONTATOS_CADASTROS';
  vChave := 'CADASTRO_ID';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql := 'select * from ' + vNome_Tabela_Orig;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               'CADASTRO_ID '+//NUMBER(10,0),
               ', ORDEM '+// NUMBER(3,0),
               ', DESCRICAO '+// VARCHAR2(60) NOT NULL,
               ', TELEFONE '+// VARCHAR2(30),
               ', RAMAL '+// NUMBER(5,0),
               ', CELULAR '+// VARCHAR2(30),
               ', E_MAIL '+// VARCHAR2(30),
               ', OBSERVACAO '+// VARCHAR2(200),
               ', FUNCAO '+// VARCHAR2(30),
               ', SEXO '+// CHAR(1),
               ', DATA_NASCIMENTO '+
               ') values ( '+
              NParametros(11);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;

      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('NOME').AsString));

        vId := FConsulta_orig.FieldByName(vChave).AsInteger;

        vSql_Validacao := 'select cadastro_id from cadastros where cadastro_id = ' + IntToStr(vId);

        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        if Existe_Registro(FConsulta_dest,vSql_Validacao,vChave,MemErro) and
                          ((FConsulta_orig.FieldByName('TELEFONE').AsString <> '') or
                           (FConsulta_orig.FieldByName('CELULAR').AsString <> '') or
                           (FConsulta_orig.FieldByName('E_MAIL').AsString <> '')) then
        begin
          vexec.Executar([
                          vId,
                          FConsulta_orig.FieldByName('ORDEM').AsInteger,
                          FConsulta_orig.FieldByName('NOME').AsString,
                          FConsulta_orig.FieldByName('TELEFONE').AsString,
                          0,
                          FConsulta_orig.FieldByName('CELULAR').AsString,
                          FConsulta_orig.FieldByName('E_MAIL').AsString,
                          FConsulta_orig.FieldByName('OBSERVACAO').AsString,
                          FConsulta_orig.FieldByName('FUNCAO').AsString,
                          FConsulta_orig.FieldByName('SEXO').AsString,
                          FConsulta_orig.FieldByName('DATA_NASCIMENTO').AsString
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;

  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_EMAILS(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,vId: Integer;
  vExec:TExecucao;
begin
  Result := False;

  vNome_Tabela_Dest := 'CADASTROS_EMAILS';
  vNome_Tabela_Orig := 'EMAILS_CADASTROS';
  vChave := 'CADASTRO_ID';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql := 'select * from ' + vNome_Tabela_Orig;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
                'CADASTRO_ID ' +//NUMBER(10,0),
                ',E_MAIL ' +//VARCHAR2(80),
                ',OBSERVACAO ' +//VARCHAR2(100)
               ') values ( '+
              NParametros(3);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('E_MAIL').AsString));
        vId := FConsulta_orig.FieldByName(vChave).AsInteger;

        vSql_Validacao := 'select cadastro_id from cadastros where cadastro_id = ' + IntToStr(vId);

        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        if Existe_Registro(FConsulta_dest,vSql_Validacao,vChave,MemErro) then
        begin
          vexec.Executar([
                          vId,
                          FConsulta_orig.FieldByName('E_MAIL').AsString,
                          'MIGRADO ADM'
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;


  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_ENDERECOS(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave,vNome_seq,
  vIdAnt,vCompl:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,vId: Integer;
  vExec:TExecucao;
begin
  Result := False;

  vNome_Tabela_Dest := 'DIVERSOS_ENDERECOS';
  vNome_Tabela_Orig := 'ENDERECOS_ENTREGAS';
  vChave := 'CADASTRO_ID';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql := 'select * from ' + vNome_Tabela_Orig + ' order by CADASTRO_ID';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
                'CADASTRO_ID ' + //NUMBER(10,0),
                ',ORDEM ' + //NUMBER(3,0),
                ',TIPO_ENDERECO ' + //CHAR(1) NOT NULL,
                ',LOGRADOURO ' + //VARCHAR2(100) NOT NULL,
                ',COMPLEMENTO ' + //VARCHAR2(100) NOT NULL,
                ',BAIRRO_ID ' + //NUMBER(15,0),
                ',PONTO_REFERENCIA ' + //VARCHAR2(100),
                ',CEP ' + //VARCHAR2(9),
                ',INSCRICAO_ESTADUAL ' +//varchar2(20)
               ') values ( '+
              NParametros(9);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      j := 0;
      vIdAnt := '';
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vId := FConsulta_orig.FieldByName(vChave).AsInteger;

        vSql_Validacao := 'select cadastro_id from cadastros where cadastro_id = ' + IntToStr(vId);

        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        if Existe_Registro(FConsulta_dest,vSql_Validacao,vChave,MemErro) then
        begin
          if vIdAnt = FConsulta_orig.GetString('CADASTRO_ID') then
            j:= J +1
          else
            j:= 1;

          vCompl := FConsulta_orig.GetString('COMPLEMENTO');
          if vCompl = '' then
            vCompl := 'COMPLEMENTO';
          vexec.Executar([
                         vId,
                         j,
                         'E',
                         FConsulta_orig.GetString('LOGRADOURO'),
                         vCompl,
                         FConsulta_orig.GetString('BAIRRO_ID'),
                         FConsulta_orig.GetString('PONTO_REFERENCIA'),
                         FConsulta_orig.GetString('CEP'),
                         FConsulta_orig.GetString('INSCRICAO_ESTADUAL')
                         ]);

          vIdAnt := FConsulta_orig.GetString('CADASTRO_ID');
          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;


  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Profissionais(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave,vNome_seq:string;
  vCARGOID:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  FConsulta_orig1: TConsulta;
  i,vId : Integer;
  vExec:TExecucao;
begin
  Result := False;

  vNome_Tabela_Dest := 'PROFISSIONAIS';
  vNome_Tabela_Orig := 'PROFISSIONAIS';
  vChave := 'CADASTRO_ID';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig  := TConsulta.Create(Conexao_orig);
  FConsulta_orig1 := TConsulta.Create(Conexao_orig);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vNome_Tabela_Orig,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql := 'select * from ' + vNome_Tabela_Orig;

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
               ' CADASTRO_ID '+//NUMBER(10,0),
               ', ATIVO '+// CHAR(1) DEFAULT 'S'    NOT NULL,
               ', CARGO_ID '+// NUMBER(6,0) NOT NULL,
               ', NUMERO_CREA '+// VARCHAR2(30),
               ', PERCENTUAL_COMISSAO '+// NUMBER(5,2) NOT NULL,
               ', PONTOS '+// NUMBER(10,0) DEFAULT 0 NOT NULL,
               ', PROFISSIONAL_INDICOU_ID '+// NUMBER(10,0),
               ', REGRA_GERAL_COMISSAO_GRUPOS '+// CHAR(1) DEFAULT 'S'    NOT NULL,
               ', TIPO_COMISSAO '+// CHAR(3) DEFAULT 'GGE'    NOT NULL,
               ', UTILIZAR_PRECO_ESPECIAL '+// CHAR(1) DEFAULT 'N'    NOT NULL,
               ', VIP '+// CHAR(1) DEFAULT 'N'    NOT NULL,
               ', INFORMACOES '+// VARCHAR2(400),
               ') values ( '+
              NParametros(12);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('cadastro_id').AsString));
        vId   := FConsulta_orig.FieldByName(vChave).AsInteger;

        vCARGOID := Busca_Registro_Origem(FConsulta_orig1,
                                          'select nome from cargos where cargo_id = ' +
                                          FConsulta_orig.FieldByName('CARGO_ID').AsString,
                                          'nome',MemErro);

        vCARGOID := Busca_Registro(FConsulta_dest,'select cargo_id from profissionais_cargos where nome = ''' + vCARGOID + '''','cargo_id',MemErro);
        if vCARGOID = '' then
          vCARGOID := '1';

        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vSql_Validacao := 'select cadastro_id from cadastros where cadastro_id = ' + IntToStr(vId);

        if Existe_Registro(FConsulta_dest,vSql_Validacao,vChave,MemErro) then
        begin
          vexec.Executar([
                         vId,
                         FConsulta_orig.FieldByName('ATIVO').AsString,
                         vCARGOID,
                         FConsulta_orig.FieldByName('NUMERO_CREA').AsString,
                         FConsulta_orig.FieldByName('PERCENTUAL_COMISSAO').AsCurrency,
                         FConsulta_orig.FieldByName('PONTOS').AsCurrency,
                         FConsulta_orig.FieldByName('PROFISSIONAL_INDICOU_ID').AsString,
                         FConsulta_orig.FieldByName('REGRA_GERAL_COMISSAO_GRUPOS').AsString,
                         FConsulta_orig.FieldByName('TIPO_COMISSAO').AsString,
                         FConsulta_orig.FieldByName('UTILIZAR_PRECO_PROMOCAO').AsString,
                         FConsulta_orig.FieldByName('VIP').AsString,
                         ''
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;


  finally
    FConsulta_orig.Free;
    FConsulta_orig1.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

function Importa_Pessoas2(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,ins_Sql,vBairro,aux_seq,Nome_Tabela,vCnpj,vInsc,vTp,vAtivo:string;
  nome_bairro,nome_cidade,uf:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig,FConsulta_orig01 : TConsulta;
  i,Dupl,Imp,j: Integer;
  vExec:TExecucao;
begin
  Result := False;

  Nome_Tabela := 'CADASTROS';
  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_orig01 := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);
  lblDupl.Caption := '0';

  try
    FConsulta_orig.Connection := Conexao_orig;
    FConsulta_orig01.Connection := Conexao_orig;

    MemHist.Lines.Clear;
    lblHis.Caption := 'Continuando Importa��o de ' + Nome_Tabela;
    MemHist.Lines.Add('Continuando Importa��o de ' + Nome_Tabela);
    MemHist.Lines.Add('');

    vSql :=' dbo.fornecedor c, dbo.cep ' +
           ' where c.cepID = cep.cepID  ';

    lblQuant.caption := Num_Registros(0,FConsulta_orig,vSql,MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql :='select c.*,cep.cep,cep.logradouro,cep.bairroid,cep.numeroini ' +
           'from dbo.fornecedor c, dbo.cep ' +
           'where c.cepID = cep.cepID order by c.fornecedorid';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar();

    ins_Sql :='insert into '+Nome_Tabela+'( '+
              'CADASTRO_ID '+    //NUMBER(10,0),
              ',NOME_FANTASIA '+ //VARCHAR2(100) NOT NULL,
              ',RAZAO_SOCIAL  '+ //VARCHAR2(100) NOT NULL,
              ',TIPO_PESSOA   '+ //CHAR(1) NOT NULL,
              ',CPF_CNPJ      '+ //VARCHAR2(19) NOT NULL,
              ',LOGRADOURO    '+ //VARCHAR2(100),
              ',COMPLEMENTO   '+ //VARCHAR2(100),
              ',NUMERO        '+ //VARCHAR2(10) DEFAULT 'SN'  NOT NULL,
              ',BAIRRO_ID     '+ //NUMBER(15,0) NOT NULL,
              ',PONTO_REFERENCIA '+ //VARCHAR2(100),

              ',CEP '+ //VARCHAR2(9),
              ',TELEFONE_PRINCIPAL '+ //VARCHAR2(15),
              ',TELEFONE_CELULAR '+ //VARCHAR2(15),
              ',TELEFONE_FAX '+ //VARCHAR2(15),
              ',E_MAIL '+ //VARCHAR2(30),
              ',SEXO '+ //CHAR(1),
              ',ESTADO_CIVIL '+ //CHAR(1),
              ',DATA_NASCIMENTO '+ //DATE,
              ',DATA_CADASTRO '+ //DATE DEFAULT trunc(sysdate)  NOT NULL,
              ',INSCRICAO_ESTADUAL '+ //VARCHAR2(12) DEFAULT 'ISENTO'  NOT NULL,

              ',CNAE '+ //VARCHAR2(12),
              ',RG '+ //VARCHAR2(10),
              ',ORGAO_EXPEDIDOR_RG '+ //VARCHAR2(10),
              ',E_CLIENTE '+ //CHAR(1) DEFAULT 'N'  NOT NULL,
              ',E_FORNECEDOR '+ //CHAR(1) DEFAULT 'N'  NOT NULL,
              ',ATIVO '+ //CHAR(1) DEFAULT 'S'  NOT NULL,
              ',E_MOTORISTA '+ //CHAR(1) DEFAULT 'N'  NOT NULL,
              ',E_TRANSPORTADORA '+ //CHAR(1) DEFAULT 'N'  NOT NULL,
              ',CHAVE_IMPORTACAO '+ //VARCHAR2(100),
              ',OBSERVACOES ' +//VARCHAR2(800)
              ') values ( '+
              NParametros(30);
    vExec.Add(ins_Sql);

    try
      Conexao_dest.IniciarTransacao;

      MemHist.Lines.Add('');
      j:= StrToInt(Max_Consulta(FConsulta_dest,'cadastro_id',Nome_Tabela));
      Dupl :=0;
      Imp := 0;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        vCnpj := TrocaCaracterEspecial(FConsulta_orig.FieldByName('cnpj').AsString,False);
        if Length(vCnpj) = 11 then
          vCnpj := Copy(vCnpj,1,3) + '.' + Copy(vCnpj,4,3) + '.' + Copy(vCnpj,7,3) + '-' + Copy(vCnpj,10,2)
        else
          vCnpj := Copy(vCnpj,1,2) + '.' + Copy(vCnpj,3,3) + '.' + Copy(vCnpj,6,3) + '/' + Copy(vCnpj,9,3) + '-' + Copy(vCnpj,13,2);

        vTp := 'F';
        if Length(vCnpj) > 14 then
          vTp := 'J';

        if TrocaCaracterEspecial(FConsulta_orig.FieldByName('InscricaoEstadual').AsString,false) = '' then
          vInsc := 'ISENTO'
        else
          vInsc := TrocaCaracterEspecial(FConsulta_orig.FieldByName('InscricaoEstadual').AsString,false);

        vSql :='select * from dbo.bairro where bairroid = ' + FConsulta_orig.FieldByName('bairroid').AsString;
        FConsulta_orig01.SQL.Clear;
        FConsulta_orig01.SQL.Add(vSql);
        FConsulta_orig01.Open();

        nome_bairro := uppercase(Tira_Aspas(FConsulta_orig01.FieldByName('bairro').Asstring));

        vSql :='select c.ds_cidade,uf.cd_uf from dbo.cidade c,dbo.UF uf ' +
               'where c.ufID = uf.ufID and cidadeid = ' + FConsulta_orig01.FieldByName('cidadeid').AsString;
        FConsulta_orig01.SQL.Clear;
        FConsulta_orig01.SQL.Add(vSql);
        FConsulta_orig01.Open();

        nome_cidade:= uppercase(Tira_Aspas(FConsulta_orig01.FieldByName('ds_cidade').Asstring));
        uf := FConsulta_orig01.FieldByName('cd_uf').Asstring;

        vSql :='select b.BAIRRO_ID from bairros b,cidades c, estados e ' +
               'where b.CIDADE_ID = c.CIDADE_ID and c.ESTADO_ID = e.ESTADO_ID ' +
               'and b.NOME = ''' + nome_bairro + ''' ' +
               'and c.NOME = ''' + nome_cidade + ''' ' +
               'and c.ESTADO_ID = ''' + uf + ''' ';

        vBairro := Busca_Registro(FConsulta_dest,vSql,'bairro_id',MemErro);
        if vBairro = '' then
          vBairro := '1';

        vAtivo := 'S';

        // verifica na tabela do Altis  se existe
        vSql :='select c.cadastro_id '+
         'from CADASTROS c '+
         'where c.CPF_CNPJ = ''' + vCnpj +'''';

        if not Existe_Registro(FConsulta_dest,vSql,'cadastro_id',MemErro) then
        begin
          j := j +1;
          vexec.Executar([
                          j,
                          Uppercase(FConsulta_orig.FieldByName('fantasia').AsString),
                          Uppercase(FConsulta_orig.FieldByName('razao_social').AsString),
                          vTp,
                          vCnpj,
                          Uppercase(FConsulta_orig.FieldByName('logradouro').AsString),
                          Uppercase(FConsulta_orig.FieldByName('End_Complemento').AsString),
                          FConsulta_orig.FieldByName('numeroini').AsString,
                          vBairro,
                          'PONTO_REFERENCIA',

                          FConsulta_orig.FieldByName('cep').AsString,
                          FConsulta_orig.FieldByName('Telefones').AsString,
                          FConsulta_orig.FieldByName('Celular').AsString,
                          '',
                          copy(FConsulta_orig.FieldByName('Email').AsString,1,80),
                          null,
                          null,
                          null,
                          FConsulta_orig.FieldByName('dt_alt').AsDateTime,
                          vInsc,

                          '',
                          null,
                          null,
                          'N',
                          'S',
                          vAtivo,
                          'N',
                          'N',
                          FConsulta_orig.FieldByName('fornecedorid').AsString,
                          ''
                         ]);

            Imp := Imp +1;
            lblImp.Caption := IntToStr(Imp);
            MemHist.Lines.Add(Uppercase(FConsulta_orig.FieldByName('razao_social').AsString));

        end else
        begin
          Dupl := Dupl + 1 ;
          lblDupl.Caption := IntToStr(Dupl);
        end;
        FConsulta_orig.Next;
      end;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        if not vsp then
        begin
          ShowMessage('Erro ao inserir '+Nome_Tabela+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;
    Conexao_dest.FinalizarTransacao;

    aux_seq := 'SEQ_CADASTRO_ID';
    if Recria_Sequencia(aux_seq,Nome_Tabela,'cadastro_id',FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de '+Nome_Tabela+' realizada com sucesso!';
    end;

  finally
    FConsulta_orig.Free;
    FConsulta_orig01.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;


function Importa_Rotas(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,ins_Sql,aux_seq,vNome_Tabela_Dest:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i: Integer;
  vExec:TExecucao;
begin
  Result := False;

  vNome_Tabela_Dest := 'rotas';
  if not vsp then
    if MessageDlg('Deseja Importar as '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);
  lblDupl.Caption := '0';
  lblImp.Caption  := '0';

  try
    FConsulta_orig.Connection := Conexao_orig;

    MemHist.Lines.Clear;
    MemHist.Lines.Add('Iniciando Importa��o de ' + vNome_Tabela_Dest);
    lblHis.Caption :='Importando ' + vNome_Tabela_Dest;
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os dados existente?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+ vNome_Tabela_Dest;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada em '+vNome_Tabela_Dest+'.');
    end;

    lblQuant.caption := Num_Registros(0,FConsulta_orig,'rotas',MemErro);
    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    vSql :='select rota_ID,'+
           'nome '+
           'from '+ vNome_Tabela_Dest +' ; ';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar();

    ins_Sql :='insert into ' + vNome_Tabela_Dest +'( '+
            'ROTA_ID,              '+
            'NOME,                 '+
            'ATIVO ) values ( '+
            NParametros(3);
    vExec.Add(ins_Sql);

    MemHist.Lines.Add('');
    try
      Conexao_dest.IniciarTransacao;

      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;

        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vSql :='select rota_id from rotas where NOME = ''' +
               FConsulta_orig.FieldByName('nome').AsString + '''';
        if not Existe_Registro(FConsulta_dest,vSql,'rota_id',MemErro) then
        begin
          vexec.Executar([
                    FConsulta_orig.GetInt('rota_ID'),
                    FConsulta_orig.GetString('nome'),
                    'S']);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(FConsulta_orig.GetString('nome'));
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(FConsulta_orig.GetString('nome'));
        end;

        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    aux_seq := 'SEQ_ROTA_ID';
    if Recria_Sequencia(aux_seq,vNome_Tabela_Dest,'rota_id',FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de '+ vNome_Tabela_Dest +' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de '+ vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de '+vNome_Tabela_Dest+' realizada com sucesso!';
    end;


  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;

end;

function Importa_Generico(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vNome_Tabela_Orig,vChave_orig,vChave_dest,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_orig : TConsulta;
  i,j,vId,maxId: Integer;
  vExec:TExecucao;
  Existe_Tabela,Existe_Relacionamento:Boolean;
begin
  Result := False;

  vNome_Tabela_Dest := 'xxxxx';
  vNome_Tabela_Orig := 'xxxxx';
  vChave_orig := 'xxxxx';
  vChave_dest := 'xxxxx';
  vNome_seq:= '';

  if not vsp then
    if MessageDlg('Deseja Importar os registros para tabela '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_orig := TConsulta.Create(nil);
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    FConsulta_orig.Connection := Conexao_orig;

    lblHis.Caption :='Importando registros para tabela ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    if not vsp then
    if MessageDlg('Deseja limpar os registros existentes na tabela '+vNome_Tabela_Dest+'?',mtinformation,[mbyes,mbno],0) = 6 then
    begin
      vFiltro_Delete := '';
      MemHist.Lines.Add('Limpando '+vNome_Tabela_Dest+'.');
      vSql := 'DELETE FROM '+vNome_Tabela_Dest +' ' + vFiltro_Delete;
      roda_sql(True,Conexao_dest,vSql,MemErro);
      MemHist.Lines.Add('Limpeza executada na tabela '+vNome_Tabela_Dest+'.');
    end;

    maxId := strtoint(Max_Consulta(FConsulta_orig,vChave_orig,vNome_Tabela_orig));

    vSql := 'select * from ' + vNome_Tabela_Orig;// + ' where ativo = ''S''';

    lblQuant.caption := Num_Registros(0,FConsulta_orig,'('+vSql+')',MemErro);

    lblImp.Caption   := '0';
    lblDupl.Caption  := '0';

    FConsulta_orig.Limpar;
    FConsulta_orig.Add(vSql);
    FConsulta_orig.Pesquisar;

    vIns_Sql :='insert into '+vNome_Tabela_Dest+
               '( '+
//              CAMPOS     '+
               ') values ( '+
              NParametros(7);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      J:= 0;
      for i := 0 to FConsulta_orig.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vNome := UpperCase(Tira_Aspas(FConsulta_orig.FieldByName('nome').AsString));
        vId := FConsulta_orig.FieldByName(vChave_orig).AsInteger;
        if vId = 1 then
          vId := MaxId + 1;

        vSql_Validacao :='select ' + vChave_dest +
                         ' from ' + vNome_Tabela_Dest +
                         ' where ' + vChave_dest + ' = '+ IntToStr(vId);
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        vSql_Validacao :='select ' + vChave_dest +
                         ' from tabela_relacionada ' +
                         ' where ' + vChave_dest + ' = '+ IntToStr(vId);
        Existe_Relacionamento := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

//        if i = 24520 then
//          lblHis.Caption :='Parada para correcao';

        if (not Existe_Tabela) and Existe_Relacionamento then
        begin
          j:= J +1;
          vexec.Executar([
                          J
//                          vId,
//                          vNome,
//                          FConsulta_orig.GetString('xx'),
//                          'S',
//                          FConsulta_orig.GetString('xx'),
//                          FConsulta_orig.GetString('xx')
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(IntToStr(vId) + ' - '+ vNome);
        end;
        FConsulta_orig.Next;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir registros na tabela '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de registros para tabela '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de registros para tabela ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;

  finally
    FConsulta_orig.Free;
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

//function BuscarArquivos(
//  pConexao: TConexao;
//  pIndice: ShortInt;
//  pFiltros: array of Variant
//): TArray of Integer;
//var
////  i: Integer;
////  t: TArquivos;
//begin
////  Result := nil;
////  t := TArquivos.Create(pConexao);
////
//////  if t.Pesquisar(pIndice, pFiltros) then begin
//////    SetLength(Result, t.getQuantidadeRegistros);
//////    for i := 0 to t.getQuantidadeRegistros - 1 do begin
//////      Result[i] := t.getRecordArquivos;
//////      t.ProximoRegistro;
//////    end;
//////  end;
////
////  t.Free;
//end;

procedure LeArquivo;
const
  coCodigoNCM     = 1;
  coPercFederal   = 5;
  coPercImportado = 6;
  coPercEstadual  = 7;
  coPercMunicipal = 8;
  coFonte         = 13;

var
  i: Integer;
  sheet: OleVariant;
  vPlanilha: OleVariant;

  function Valor( pValor: string ): Double;
  begin
    Result := StrToFloat( StringReplace(pValor, '.', ',', [rfReplaceAll]) ) ;
  end;

begin
  //Crio o objeto que gerencia o arquivo excel
  vPlanilha:= CreateOleObject('Excel.Application');

  //Abro o arquivo
  vPlanilha.WorkBooks.open(FrmMenuPrincipal.edtArq.Text);

  //Pego a primeira vPlanilha do arquivo
  sheet := vPlanilha.WorkSheets[1];

  i := 2;
  while Trim(sheet.Cells[i, 3]) <> '' do begin
//    SetLength(FImpostos, Length(FImpostos) + 1);
//    FImpostos[High(FImpostos)].CodigoNCM     := sheet.cells[i, coCodigoNCM].Value;
//    FImpostos[High(FImpostos)].PercFederal   := Valor( sheet.cells[i, coPercFederal].Value );
//    FImpostos[High(FImpostos)].PercFederalImportado := Valor( sheet.cells[i, coPercImportado].Value );
//    FImpostos[High(FImpostos)].PercEstadual  := Valor( sheet.cells[i, coPercEstadual].Value );
//    FImpostos[High(FImpostos)].PercMunicipal := Valor( sheet.cells[i, coPercMunicipal].Value );
//    FImpostos[High(FImpostos)].Fonte         := sheet.cells[i, coFonte].Value;
//
//    Inc(i);
//
//    lbQtdeNCMs.Caption := NFormat( Length(FImpostos) );
//    lbQtdeNCMs.Refresh;
  end;

  //Fecho a vPlanilha
  vPlanilha.WorkBooks.Close;
//  Modo(True);
end;

function Importa_Cidades(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,ins_Sql,vCid,aux_seq,vNome_Tabela_Dest,vUf,vSql_Validacao,vNome_Tabela_Orig,
  upd_sql, vChave_orig,vChave_dest,vNome_seq:string;
  FConsulta_dest : TConsulta;
  FConsulta_id: TConsulta;
  i,vId: Integer;
  vExec:TExecucao;
  vExecUpdate: TExecucao;
  Fcidade:TArray<RecCidade>;
  sheet: OleVariant;
  vPlanilha: OleVariant;
  Existe_Tabela:Boolean;
begin
  Result := False;

  Fcidade := nil;
  //Crio o objeto que gerencia o arquivo excel
  vPlanilha:= CreateOleObject('Excel.Application');

  //Abro o arquivo
  if FrmMenuPrincipal.edtArq.Text = '' then
  begin
    ShowMessage('Informe o Caminho da Planilha!' );
    Abort;
  end;
  vPlanilha.WorkBooks.open(FrmMenuPrincipal.edtArq.Text);

  //Pego a primeira vPlanilha do arquivo
  sheet := vPlanilha.WorkSheets[1];

  i := 2;
  lblQuant.caption := '0';
  lblImp.caption   := '0';
  lblDupl.caption  := '0';
  while Trim(sheet.Cells[i, 2]) <> '' do begin
    SetLength(Fcidade, Length(Fcidade) + 1);
    Fcidade[High(Fcidade)].EstadoId  := sheet.cells[i, 1].value;
    Fcidade[High(Fcidade)].CodIbge  := sheet.cells[i, 2].value;
    Fcidade[High(Fcidade)].Nome := sheet.cells[i, 3].value;
    Inc(i);

    lblQuant.caption := NFormat( Length(Fcidade) );
    lblQuant.Refresh;
  end;

  //Fecho a vPlanilha
  vPlanilha.WorkBooks.Close;

  vNome_Tabela_Dest := 'CIDADES';
  vChave_dest := 'cidade_id';
  vNome_seq:= 'SEQ_CIDADE_ID';

  if not vsp then
    if MessageDlg('Deseja Importar os '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  FConsulta_id := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);
  vExecUpdate := TExecucao.Create(Conexao_dest);

  try
    MemHist.Lines.Clear;
    MemHist.Lines.Add('Iniciando Importa��o de Cidades');
    lblHis.Caption :='Importando Cidades';
    MemHist.Lines.Add('');

    ins_Sql :='insert into '+vNome_Tabela_Dest+'( '+
              'CIDADE_ID,                   '+
              'NOME,                        '+
              'CODIGO_IBGE,                 '+
              'ESTADO_ID,                   '+
              'ATIVO,                       '+
              'CHAVE_IMPORTACAO,            '+
              'TIPO_BLOQUEIO_VENDA_ENTREGAR) values ( '+
              NParametros(7);
    vExec.Add(ins_Sql);

    upd_sql := 'update CIDADES set ' +
               '  CODIGO_IBGE = :P1 ' +
               'where CIDADE_ID = :P2';
    vExecUpdate.Add(upd_sql);

    FConsulta_id.SQL.Add('select ');
    FConsulta_id.SQL.Add('  CIDADE_ID ');
    FConsulta_id.SQL.Add('from ');
    FConsulta_id.SQL.Add('CIDADES ');
    FConsulta_id.SQL.Add('where NOME = :P1 ');
    FConsulta_id.SQL.Add('and ESTADO_ID = :P2 ');

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to Length(Fcidade) -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vUf := UpperCase(Tira_Aspas(Fcidade[i].EstadoId));

        vSql :='select estado_id from estados where estado_id = ''' + vUf + '''';
        if (vUf = '') or not Existe_Registro(FConsulta_dest,vSql,'estado_id',MemErro) then
          vUf := 'GO';

        vCid := UpperCase(Tira_Aspas(Fcidade[i].Nome));
        if vCid = '' then
          vCid:= 'GOIANIA';

        vSql :='select cidade_id from cidades where NOME = ''' + vCid + '''' +
               ' and ESTADO_ID = ''' + vUf + '''';

        vSql_Validacao :='select ' + vChave_dest +
                         ' from ' + vNome_Tabela_Dest +
                         ' where nome = ''' + vCid + ''' ';
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        if (not Existe_Tabela) then
        begin
          vId := StrToInt(Max_Consulta(FConsulta_dest,vChave_dest,vNome_Tabela_Dest));
          vexec.Executar([
                          vId,
                          vCid,
                          Fcidade[i].CodIbge,
                          vUf,
                          'S',
                          '',
                          'N']);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(vUf + ' - '+ vCid);
        end else
        begin
          FConsulta_id.Pesquisar([vCid, vUf]);

          vExecUpdate.Executar([Fcidade[i].CodIbge, FConsulta_id.GetInt('CIDADE_ID')]);

          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(vUf + ' - '+ vCid);
        end;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    aux_seq := 'SEQ_CIDADE_ID';
    if Recria_Sequencia(aux_seq,vNome_Tabela_Dest,'cidade_id',FConsulta_dest,Conexao_dest,lblHis,MemErro) then
      lblHis.Caption := 'Importacao de Cidades concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de Cidades com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de Cidades realizada com sucesso!';
    end;

  finally
    FConsulta_dest.Free;
    vExec.Free;
    vExecUpdate.Free;
    FConsulta_id.Free;
  end;
end;

function Importa_Bairros(vsp:Boolean;Conexao_orig:Tconexao;Conexao_dest:Tconexao;lblHis,lblQuant,lblImp,lblDupl:Tlabel;MemHist,MemErro,MemDupl:Tmemo):Boolean;
var
  vSql,vNome,vFiltro_Delete,vIns_Sql,vSql_Validacao:string;
  vNome_Tabela_Dest,vChave_dest,vNome_seq,vUf,vCid,vBairro:string;
  FConsulta_dest : TConsulta;
  i,maxId,vId: Integer;
  vExec:TExecucao;
  Fbairro:TArray<RecBairro>;
  sheet: OleVariant;
  vPlanilha: OleVariant;
  Existe_Tabela:Boolean;
begin
  Result := False;

  Fbairro := nil;
  //Crio o objeto que gerencia o arquivo excel
  vPlanilha:= CreateOleObject('Excel.Application');

  //Abro o arquivo
  if FrmMenuPrincipal.edtArq.Text = '' then
  begin
    ShowMessage('Informe o Caminho da Planilha!' );
    Abort;
  end;
  vPlanilha.WorkBooks.open(FrmMenuPrincipal.edtArq.Text);

  //Pego a primeira vPlanilha do arquivo
  sheet := vPlanilha.WorkSheets[1];

  i := 2;
  lblQuant.caption := '0';
  lblImp.caption   := '0';
  lblDupl.caption  := '0';
  while Trim(sheet.Cells[i, 2]) <> '' do begin
    SetLength(Fbairro, Length(Fbairro) + 1);
    Fbairro[High(Fbairro)].Bairro  := sheet.cells[i, 1].value;
    Fbairro[High(Fbairro)].Cidade  := sheet.cells[i, 2].value;
    Fbairro[High(Fbairro)].Estado  := sheet.cells[i, 3].value;
    Inc(i);

    lblQuant.caption := NFormat( Length(Fbairro) );
    lblQuant.Refresh;
  end;

  //Fecho a vPlanilha
  vPlanilha.WorkBooks.Close;

  vNome_Tabela_Dest := 'BAIRROS';
  vChave_dest := 'bairro_id';
  vNome_seq:= 'SEQ_BAIRRO_ID';

  if not vsp then
    if MessageDlg('Deseja Importar os '+ vNome_Tabela_Dest +'?',mtinformation,[mbyes,mbno],0) = 7 then
      abort;

  Application.ProcessMessages;
  FConsulta_dest := TConsulta.Create(Conexao_dest);
  vExec     := TExecucao.Create(Conexao_dest);

  try
    lblHis.Caption :='Importando ' + vNome_Tabela_Dest;
    MemHist.Lines.Clear;
    MemHist.Lines.Add(lblHis.Caption);
    MemHist.Lines.Add('');

    vIns_Sql:='insert into BAIRROS( '+
              'BAIRRO_ID,         '+
              'NOME,              '+
              'CIDADE_ID,         '+
              'ATIVO,             '+
              'ROTA_ID,           '+
              'CHAVE_IMPORTACAO) values ( '+
              NParametros(6);
    vExec.Add(vIns_Sql);

    try
      MemHist.Lines.Add('');
      Conexao_dest.IniciarTransacao;
      for i := 0 to Length(Fbairro) -1 do
      begin
        Application.ProcessMessages;
        if i = 0 then
        begin
          lblHis.Caption :='Validando e inserindo registros existentes na tabela ' + vNome_Tabela_Dest;
          MemHist.Lines.Add(lblHis.Caption);
        end;

        vUf := UpperCase(Tira_Aspas(Fbairro[i].Estado));

        vSql :='select estado_id from estados where estado_id = ''' + vUf + '''';
        if (vUf = '') or not Existe_Registro(FConsulta_dest,vSql,'estado_id',MemErro) then
          vUf := 'GO';

        vCid := UpperCase(Tira_Aspas(Fbairro[i].Cidade));
        if vCid = '' then
          vCid:= 'GOIANIA';

        vSql :='select cidade_id from cidades where NOME = ''' + vCid + '''' +
               ' and ESTADO_ID = ''' + vUf + '''';

        vCid:= Busca_Registro(FConsulta_dest,vSql,'cidade_id',MemErro);
        if vCid = '' then
          vCid:= '2';

        vNome := UpperCase(Tira_Aspas(Fbairro[i].Bairro));
        if vNome = '' then
          vNome := 'GOIANIA';

        vSql_Validacao := 'select '+ vChave_dest +' from bairros where nome = ''' + vNome + '''' +
                          ' and cidade_id = ' + vCid ;
        Existe_Tabela := Existe_Registro(FConsulta_dest,vSql_Validacao,vChave_dest,MemErro);

        if (not Existe_Tabela) then
        begin
          vId := StrToInt(Max_Consulta(FConsulta_dest,vChave_dest,vNome_Tabela_Dest));
          vexec.Executar([
                          vId,
                          vNome,
                          vCid,
                          'S',
                          null,
                          null
                          ]);

          lblImp.Caption := inttostr(StrToInt(lblImp.Caption)+1);
          MemHist.Lines.Add(vCid + ' - '+ vNome);
        end else
        begin
          lblDupl.Caption := IntToStr(strtoint(lblDupl.Caption) + 1);
          MemDupl.Lines.Add(vCid + ' - '+ vNome);
        end;
      end;
      Conexao_dest.FinalizarTransacao;
    except
      on e:Exception do begin
        Conexao_dest.VoltarTransacao;
        MemErro.Lines.Add('');
        MemErro.Lines.Add(e.Message);
        if not vsp then
        begin
          ShowMessage('Erro ao inserir '+vNome_Tabela_Dest+'.' +
          e.Message + ' ' + vexec.SQL.Text );
          abort;
        end;
      end;
    end;

    if vNome_seq <> '' then
      Result := Recria_Sequencia(vNome_seq,vNome_Tabela_Dest,vChave_dest,FConsulta_dest,Conexao_dest,lblHis,MemErro);

    if Result then
      lblHis.Caption := 'Importacao de '+vNome_Tabela_Dest+' concluida!';

    if StrToInt(lblQuant.Caption) <> (StrToInt(lblDupl.Caption) + StrToInt(lblImp.Caption)) then
    begin
      lblHis.Caption := 'Importacao de '+vNome_Tabela_Dest+' com advertencia!';
      if not vsp then
        ShowMessage('Existem divergencias na importacao.');
    end else
    begin
      Result := True;
      lblHis.Caption := 'Importacao de ' +vNome_Tabela_Dest+ ' realizada com sucesso!';
    end;
  finally
    FConsulta_dest.Free;
    vExec.Free;
  end;
end;

end.
