program Migracao_Dados;

uses
  Vcl.Forms,
  Menu_Principal in 'Menu_Principal.pas' {FrmMenuPrincipal},
  _Conexao in '..\Orientado a Objetos\Repositório\_Conexao.pas',
  _OperacoesBancoDados in '..\Orientado a Objetos\Repositório\_OperacoesBancoDados.pas',
  _MigraAdm in '_MigraAdm.pas',
  _LimpaBaseAltis in '_LimpaBaseAltis.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrmMenuPrincipal, FrmMenuPrincipal);
  Application.Run;
end.
