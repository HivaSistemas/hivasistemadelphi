unit _LimpaBaseAltis;

interface

uses
  _Conexao,_OperacoesBancoDados,Vcl.StdCtrls,System.SysUtils,Vcl.Dialogs,Vcl.Forms,System.UITypes,System.StrUtils;

function roda_sql(vAborta:Boolean;vConexao:TConexao;vSql:string;MemErro:TMemo):Boolean;
function InArrayConst(AItem:String;AListItem:array of String):Boolean;
function LimparBaseDest(FConexaoBanco_dest:Tconexao;lblHist,lblMensagem,lblok:Tlabel;MemHist,MemErro:Tmemo;vBase:string):Boolean;
function Delete_Tabela(vNome,vFiltro:String;vConexao:TConexao):Boolean;

implementation

uses
  _MigraAdm;
function Delete_Tabela(vNome,vFiltro:String;vConexao:TConexao):Boolean;
var
  vExec: TExecucao;
  vSql:string;
begin
  Result := True;
  vExec := TExecucao.Create(vConexao);
  try
    try
      vSql := 'Delete from '+ vNome + ' ' + vFiltro;
      vConexao.IniciarTransacao;
      vExec.SQL.Add(vSql);
      vExec.ExecSQL;
      vConexao.FinalizarTransacao;
    except
      Result := False;
      vConexao.VoltarTransacao;
    end;
  finally
    vExec.Free;
  end;
end;

function roda_sql(vAborta:Boolean;vConexao:TConexao;vSql:string;MemErro:TMemo):Boolean;
var
  vExec: TExecucao;
begin
  Result := True;
  vExec := TExecucao.Create(vConexao);
  try
    try
      vConexao.IniciarTransacao;
      MemErro.Lines.Clear;
      MemErro.Lines.Add(vSql);
      vExec.SQL.Add(vSql);
      vExec.ExecSQL;
      vConexao.FinalizarTransacao;
      MemErro.Lines.Clear;
    except
      on e: Exception do begin
        Result := False;
        vConexao.VoltarTransacao;
        if vAborta then
        begin
          ShowMessage('Erro ao executar Procedimento.' +
          e.Message);
          Abort;
        end;
      end;
    end;
  finally
    vExec.Free;
  end;
end;

function InArrayConst(AItem:String;AListItem:array of String):Boolean;
var
  int_Loop: Integer;
begin
  Result := False;
  for int_Loop:=Low(AListItem) to High(AListItem) do
  begin
    Result := (AItem = AListItem[int_Loop]);

    if Result then
      Break;
  end;
end;

function LimparBaseDest(FConexaoBanco_dest:Tconexao;lblHist,lblMensagem,lblok:Tlabel;MemHist,MemErro:Tmemo;vBase:string):Boolean;
var
  vSql,vFiltro:string;
  i,j:Integer;
  FConsulta_dest : TConsulta;
const
  Lista1:Array[0..21] of string = ('CFOP',
                'CFOPS_CST_OPERACOES',
                'EMPRESAS',
                'CFOP_PARAMETROS_FISCAIS_EMPR',
                'ESTADOS',
                'FERIADOS',
                'GRUPOS_ESTOQUES',
                'IMPOSTOS_IBPT',
                'LOCAIS_PRODUTOS',
                'MOTIVOS_AJUSTE_ESTOQUE',
                'PLANOS_FINANCEIROS',
                'PORTADORES',
                'SERIES_NOTAS_FISCAIS',
                'TIPOS_COBRANCA',
                'TIPO_COBRANCA_DIAS_PRAZO',
                'BAIRROS',
                'CIDADES',
                'CONDICOES_PAGAMENTO_PRAZOS',
                'CONDICOES_CARTAO_CREDITO_PDV',
                'CONDICOES_PAGAMENTO',
                'CADASTROS',
                'PARAMETROS_EMPRESA'
                );
  Lista2:Array[0..8] of string = ('CFOPS_CST_OPERACOES',
                'EMPRESAS',
                'CFOP_PARAMETROS_FISCAIS_EMPR',
                'GRUPOS_ESTOQUES',
                'CONDICOES_PAGAMENTO_PRAZOS',
                'CONDICOES_CARTAO_CREDITO_PDV',
                'CONDICOES_PAGAMENTO',
                'CADASTROS',
                'PARAMETROS_EMPRESA'
                );
  Lista3:Array[0..2] of string = (
                'CONDICOES_PAGAMENTO_PRAZOS',
                'CONDICOES_CARTAO_CREDITO_PDV',
                'CONDICOES_PAGAMENTO'
                );
  Lista4:Array[0..0] of string = (
                'CADASTROS'
                );
label
  Reinicia;
begin

  if MessageDlg('Deseja realmente limpar essa base de dados?',mtinformation,[mbyes,mbno],0) = 7 then
    abort;

  Result := False;
  MemHist.Lines.Clear;
  MemErro.Lines.Clear;
  lblMensagem.Caption := 'Iniciando Limpeza do Banco.';
  MemHist.Lines.Add('Iniciando Limpeza do Banco.');
  MemHist.Lines.Add('');

  try
    if FConexaoBanco_dest.Connected then
    begin
      FConsulta_dest := TConsulta.Create(FConexaoBanco_dest);

      Application.ProcessMessages;
      vSql :=
      'declare '+

      '  cursor cTriggers is '+
      '  select '+
      '    TRIGGER_NAME '+
      '  from '+
      '    USER_TRIGGERS; '+

      'begin  '+

      '  for xTrig in cTriggers loop '+
      '    execute immediate ''alter trigger '' || xTrig.TRIGGER_NAME || '' disable''; '+
      '  end loop; '+
      'end;';

      MemHist.Lines.Add('Desabilitando Triggers.');
      roda_sql(True,FConexaoBanco_dest,vSql,MemErro);
      MemHist.Lines.Add('Triggers Desabilitadas.');

      vSql := 'update parametros_empresa set ' +
              'CONDICAO_PAGAMENTO_CONS_PROD_2 = 1, ' +
              'CONDICAO_PAGAMENTO_CONS_PROD_3 = 1, ' +
              'CONDICAO_PAGAMENTO_CONS_PROD_4 = 1, ' +
              'CONDICAO_PAGAMENTO_PDV = 1, ' +
              'CONDICAO_PAGTO_PAD_VENDA_ASSIS = 1, ' +
              'COND_PAGTO_PADRAO_PESQ_VENDA = 1, ' +
              'EMPRESA_RED_NOTA_ENTREGAR_ID = 1, ' +
              'EMPRESA_RED_NOTA_RETIRAR_ID  = 1, ' +
              'EMPRESA_RED_NOTA_RETIRA_ATO_ID = 1; ';

      roda_sql(True,FConexaoBanco_dest,vSql,MemErro);
      MemHist.Lines.Add('parametros_empresa ajustados COM SUCESSO!');

      vSql := 'update EMPRESAS set CNPJ = ''00.000.000/0001-00'', ' +
              'RAZAO_SOCIAL = ''NOVA'', ' +
              'NOME_FANTASIA = ''NOVA'', ' +
              'LOGRADOURO = ''NOVA'', ' +
              'COMPLEMENTO = ''NOVA'', ' +
              'PONTO_REFERENCIA = ''NOVA'', ' +
              'CEP = ''74000000'', ' +
              'TELEFONE_PRINCIPAL = NULL, ' +
              'E_MAIL = NULL, '  +
              'INSCRICAO_ESTADUAL = ''0000000000000'', ' +
              'CADASTRO_ID = 1, ' +
              'NOME_RESUMIDO_IMPRESSOES_NF = ''NOVA'' ' ;

      roda_sql(True,FConexaoBanco_dest,vSql,MemErro);
      MemHist.Lines.Add('Ajustando condicoes de clientes.');

      vSql := 'update clientes set CONDICAO_ID = 1 ';
      roda_sql(True,FConexaoBanco_dest,vSql,MemErro);
      MemHist.Lines.Add('Ajustando condicoes de clientes.');

      vSql := 'update bairros set ROTA_Id = null ';
      roda_sql(True,FConexaoBanco_dest,vSql,MemErro);
      MemHist.Lines.Add('Ajustando Rotas de bairros.');

      Reinicia: vSql:= '';

      vSql :='select table_name from all_tables' +
             ' where owner = ''' + vBase + '''';

      MemErro.Lines.Clear;
      FConsulta_dest.Limpar;
      FConsulta_dest.Add(vSql);
      FConsulta_dest.Pesquisar;
      lblok.Caption := '0';
      j := 0;
      lblMensagem.Caption :=  IntToStr(FConsulta_dest.GetQuantidadeRegistros);
      for i := 0 to FConsulta_dest.GetQuantidadeRegistros -1 do
      begin
        Application.ProcessMessages;
        lblHist.Caption := 'Limpando Tabela '+ FConsulta_dest.GetString('table_name');

        if not InArrayConst(FConsulta_dest.GetString('table_name'),Lista1) then begin
          if not Delete_Tabela(FConsulta_dest.GetString('table_name'),'',FConexaoBanco_dest) then
            j:= j +1
          else
          begin
            MemHist.Lines.Add(FConsulta_dest.GetString('table_name') + ' Limpa');
            lblok.Caption :=  IntToStr(StrToInt(lblok.Caption) +1);
          end;
        end
        else begin
          if InArrayConst(FConsulta_dest.GetString('table_name'),Lista2) then begin
            if InArrayConst(FConsulta_dest.GetString('table_name'),Lista3) then
              vFiltro := ' where CONDICAO_ID > 1 '
            else
              if InArrayConst(FConsulta_dest.GetString('table_name'),Lista4) then
                vFiltro := ' where CADASTRO_ID > 1 '
              else
                vFiltro := ' where EMPRESA_ID > 1 ';

            if not Delete_Tabela(FConsulta_dest.GetString('table_name'),vFiltro,FConexaoBanco_dest) then
              j:= j +1
            else
            begin
              MemHist.Lines.Add(FConsulta_dest.GetString('table_name') + ' Limpa');
              lblok.Caption :=  IntToStr(StrToInt(lblok.Caption) +1);
            end;
          end;
        end;
        FConsulta_dest.Next;
      end;
      if j > 0 then
        goto Reinicia;

      lblHist.Caption := 'Tabelas Limpas.';

      vSql :=
      'declare                  '+
      '  cursor cTriggers is    '+
      '  select                 '+
      '    TRIGGER_NAME         '+
      '  from                   '+
      '    USER_TRIGGERS;       '+

      'begin                    '+

      '  for xSeq in (          '+
      '      select             '+
      '        SEQUENCE_NAME,   '+
      '        MIN_VALUE,       '+
      '        MAX_VALUE        '+
      '      from               '+
      '        USER_SEQUENCES   '+
      '      where LAST_NUMBER > 1  '+
      '    ) loop                   '+
      '      execute immediate ''drop sequence '' || xSeq.SEQUENCE_NAME; '+

      '      execute immediate  '+
      '        ''create sequence '' || xSeq.SEQUENCE_NAME || '' '' || '+
      '        ''start with 1 increment by 1 '' ||                    '+
      '        ''minvalue 1 '' ||                                     '+
      '        ''maxvalue '' || xSeq.MAX_VALUE || '' '' ||            '+
      '        ''nocycle '' ||                                        '+
      '        ''nocache '' ||                                        '+
      '        ''noorder'';                                           '+
      '    end loop;                                                  '+
      'end;                                                           ';

      MemHist.Lines.Add('');
      MemHist.Lines.Add('Recriando Sequences.');
      roda_sql(True,FConexaoBanco_dest,vSql,MemErro);
      MemHist.Lines.Add('Sequencias Recriadas.');

      vSql :=
      'declare '+
      '  cursor cTriggers is '+
      '  select              '+
      '    TRIGGER_NAME      '+
      '  from                '+
      '    USER_TRIGGERS;    '+

      'begin                 '+
      '  for xTrig in cTriggers loop  '+
      '    execute immediate ''alter trigger '' || xTrig.TRIGGER_NAME || '' enable''; '+
      '  end loop; '+
      'end; ';

      MemHist.Lines.Add('Habilitando Triggers.');
      roda_sql(True,FConexaoBanco_dest,vSql,MemErro);
      MemHist.Lines.Add('Triggers HABILITADAS COM SUCESSO!');

      vSql := ' ALTER TABLE CADASTROS DISABLE ALL TRIGGERS ';
      roda_sql(True,FConexaoBanco_dest,vSql,MemErro);

      vSql :=
      'INSERT INTO MARCAS(MARCA_ID, NOME, ATIVO) '+
      'VALUES (1, ''GERAL'', ''S'');             ';
      roda_sql(True,FConexaoBanco_dest,vSql,MemErro);

      vSql := 'select SEQ_MARCA_ID.nextval from dual ';
      roda_sql(True,FConexaoBanco_dest,vSql,MemErro);
      MemHist.Lines.Add('MARCAS INSERIDAS COM SUCESSO!');

      vSql := ' ALTER TABLE CADASTROS ENABLE ALL TRIGGERS ';
      MemHist.Lines.Add('HABILITANDO TRIGGERS DE CADASTRO.');
      roda_sql(True,FConexaoBanco_dest,vSql,MemErro);
      MemHist.Lines.Add('TRIGGERS DE CADASTRO HABILITADAS!');

      vSql :=
      'INSERT INTO CARGOS_FUNCIONARIOS(CARGO_ID, NOME, ATIVO, USUARIO_SESSAO_ID, '+
      'DATA_HORA_ALTERACAO, ROTINA_ALTERACAO, ESTACAO_ALTERACAO) '+
      'VALUES (1, ''GERENTE DE SISTEMA'', ''S'', 1, sysdate, ''SEM DEFINICAO'', '+
      '''SEM DEFINICAO'');                                        ';
      roda_sql(True,FConexaoBanco_dest,vSql,MemErro);
      MemHist.Lines.Add('CARGO GERENTE DE SISTEMA INSERIDO COM SUCESSO.');

      vSql :=
      'INSERT INTO FUNCIONARIOS(FUNCIONARIO_ID, NOME, SENHA_SISTEMA, APELIDO, DATA_CADASTRO, '+
      'DATA_NASCIMENTO, CARGO_ID, CPF, RG, ORGAO_EXPEDIDOR_RG, DATA_ADMISSAO, LOGRADOURO, '+
      'COMPLEMENTO, NUMERO, BAIRRO_ID, CEP, E_MAIL, SERVIDOR_SMTP, USUARIO_E_MAIL, '+
      'SENHA_E_MAIL, PORTA_E_MAIL, CADASTRO_ID, PERCENTUAL_COMISSAO_A_VISTA, '+
      'PERCENTUAL_COMISSAO_A_PRAZO, PERCENTUAL_DESCONTO_ADIC_VENDA, '+
      'PERCENTUAL_DESCONTO_ADIC_FINAN, SALARIO, ATIVO, VENDEDOR, CAIXA, COMPRADOR, '+
      'USUARIO_SESSAO_ID, DATA_HORA_ALTERACAO, ROTINA_ALTERACAO, ESTACAO_ALTERACAO, '+
      'DATA_DESLIGAMENTO, NUMERO_CARTEIRA_TRABALHO, SERIE_CARTEIRA_TRABALHO, PIS_PASEP, '+
      'DATA_ULTIMA_SENHA) '+
      'VALUES (1, ''ALTIS SISTEMAS'', ''djSXaD8sI5eUIIfBcT85hNVPHIW3K9IC7Gzx6ypTd4U='', '+
      ' ''ALTIS SISTEMAS'',SYSDATE, SYSDATE, 1, ''111.111.111-11'', NULL, NULL, SYSDATE, '+
      ' ''RUA'', ''GERAL'', ''SN'', 1, ''74000-000'', NULL, NULL, NULL, NULL, NULL, 1, 0, '+
      ' 0, 0, 0, 0, ''S'', ''N'', ''N'', ''N'', 1, SYSDATE, ''SEM DEFINICAO'', '+
      ' ''SEM DEFINICAO'', NULL, NULL, NULL, NULL, SYSDATE); ';
      roda_sql(True,FConexaoBanco_dest,vSql,MemErro);

      vSql := 'select SEQ_FUNCIONARIO_ID.nextval from dual ';
      roda_sql(True,FConexaoBanco_dest,vSql,MemErro);
      MemHist.Lines.Add('USUARIO ALTIS CRIADO COM SUCESSO.');

      MemHist.Lines.Add('DESABILITANDO TRIGGER DE CLIENTES!');
      vSql := 'ALTER TABLE CLIENTES DISABLE ALL TRIGGERS ';
      roda_sql(True,FConexaoBanco_dest,vSql,MemErro);

      vSql :=
      'INSERT INTO CLIENTES(CADASTRO_ID, TIPO_CLIENTE, DATA_CADASTRO, CODIGO_SPC, '+
      'DATA_PESQUISA_SPC, ATENDENTE_SPC, RESTRICAO_SPC, DATA_RESTRICAO_SPC, LOCAL_SPC, '+
      'DATA_PESQUISA_SERASA, ATENDENTE_SERASA, RESTRICAO_SERASA, DATA_RESTRICAO_SERASA, '+
      'LOCAL_SERASA, CONDICAO_ID, VENDEDOR_ID, EMITIR_SOMENTE_NFE, NAO_GERAR_COMISSAO, '+
      'LIMITE_CREDITO, LIMITE_CREDITO_MENSAL, DATA_APROVACAO_LIMITE_CREDITO, '+
      'USUARIO_APROV_LIMITE_CRED_ID, ATIVO, EXIGIR_NOTA_SIMPLES_FAT, TIPO_PRECO, '+
      'USUARIO_SESSAO_ID, DATA_HORA_ALTERACAO, ROTINA_ALTERACAO, ESTACAO_ALTERACAO, '+
      'DATA_VALIDADE_LIMITE_CREDITO, EXIGIR_MODELO_NOTA_FISCAL) '+
      'VALUES (1, ''NC'', SYSDATE, NULL, NULL, NULL, ''N'', NULL, NULL, NULL, NULL, ''N'', '+
      'NULL, NULL, NULL, NULL, ''N'', ''N'', 0, 0, NULL, NULL, ''S'', ''N'', ''V'', 1, '+
      'SYSDATE, ''IMPORTACAO'', ''ALTIS'', NULL, ''N''); ';
      roda_sql(True,FConexaoBanco_dest,vSql,MemErro);

      vSql := ' ALTER TABLE CLIENTES ENABLE ALL TRIGGERS ';
      roda_sql(True,FConexaoBanco_dest,vSql,MemErro);
      MemHist.Lines.Add('TRIGGER DE CLIENTES HABILITADAS COM SUCESSO!');

      vSql :=
      'INSERT INTO PARAMETROS(CADASTRO_CONSUMIDOR_FINAL_ID, INICIAR_VENDA_CONSUMIDOR_FINAL, '+
      'USUARIO_GERENTE_SISTEMA_ID, UTILIZAR_ANALISE_CUSTO, VERSAO_ATUAL, COMANDO_ATUAL, '+
      'QTDE_MENSAGENS_OBRIGAR_LEITURA, QUANTIDADE_DIAS_VALIDADE_SENHA) '+
      'VALUES (1, ''S'', 1, ''N'', 4000, NULL, 5, 90); ';
      roda_sql(True,FConexaoBanco_dest,vSql,MemErro);
      MemHist.Lines.Add('PARAMETROS INSERIDOS COM SUCESSO!');

      MemHist.Lines.Add('CONFIGURANDO CADASTROS DE PRODUTOS!');
      vSql :=
      'INSERT INTO PRODUTOS_DEPTOS_SECOES_LINHAS(DEPTO_SECAO_LINHA_ID, NOME, '+
      'DEPTO_SECAO_LINHA_PAI_ID, ATIVO) '+
      'VALUES (''999'', ''GERAL'', NULL, ''S''); ';
      roda_sql(True,FConexaoBanco_dest,vSql,MemErro);

      vSql :=
      'INSERT INTO PRODUTOS_DEPTOS_SECOES_LINHAS(DEPTO_SECAO_LINHA_ID, NOME, '+
      'DEPTO_SECAO_LINHA_PAI_ID, ATIVO) '+
      'VALUES (''999.999'', ''GERAL'', ''999'', ''S''); ';
      roda_sql(True,FConexaoBanco_dest,vSql,MemErro);

      vSql :=
      'INSERT INTO PRODUTOS_DEPTOS_SECOES_LINHAS(DEPTO_SECAO_LINHA_ID, NOME, '+
      'DEPTO_SECAO_LINHA_PAI_ID, ATIVO) '+
      'VALUES (''999.999.999'', ''GERAL'', ''999.999'', ''S''); ';
      roda_sql(True,FConexaoBanco_dest,vSql,MemErro);

      vSql :=
      'INSERT INTO PERCENTUAIS_CUSTOS_EMPRESA(EMPRESA_ID, PERCENTUAL_COMISSAO, '+
      'PERCENTUAL_FRETE_VENDA, PERCENTUAL_CUSTO_FIXO, PERCENTUAL_PIS, PERCENTUAL_COFINS, '+
      'PERCENTUAL_CSLL, PERCENTUAL_IRPJ, USUARIO_SESSAO_ID, DATA_HORA_ALTERACAO, '+
      'ROTINA_ALTERACAO, ESTACAO_ALTERACAO)  '+
      'VALUES (1, 0, 0, 0, 0, 0, 0, 0, 1, SYSDATE, ''SEM DEFINICAO'', ''SEM DEFINICAO''); ';
      roda_sql(True,FConexaoBanco_dest,vSql,MemErro);
      MemHist.Lines.Add('CADASTROS CONFIGURADOS COM SUCESSO!');

      vSql :=
      'declare '+
      '  cursor cTriggers is '+
      '  select              '+
      '    TRIGGER_NAME      '+
      '  from                '+
      '    USER_TRIGGERS;    '+

      'begin                 '+
      '  for xTrig in cTriggers loop  '+
      '    execute immediate ''alter trigger '' || xTrig.TRIGGER_NAME || '' enable''; '+
      '  end loop; '+
      'end; ';

      MemHist.Lines.Add('Habilitando Triggers.');
      roda_sql(True,FConexaoBanco_dest,vSql,MemErro);
      MemHist.Lines.Add('Triggers HABILITADAS COM SUCESSO!');

      MemHist.Lines.Add('LIMPEZA CONCLUIDA!');
      lblMensagem.Caption := 'Limpeza Executada com Sucesso!';
      Result := True;
    end;
  except
    on e: Exception do begin
      Result := False;
      ShowMessage('Erro ao limpar base de dados.' +
      e.Message);
    end;
  end;
end;

end.
