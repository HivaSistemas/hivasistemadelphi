unit Menu_Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,_Conexao,_OperacoesBancoDados,_limpaBaseAltis,
  _MigraAdm, Vcl.Grids, GridLuka, REST.Backend.EMSServices, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef, Data.DB, FireDAC.Comp.Client,
  REST.Backend.EMSFireDAC, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Phys.SQLiteVDataSet, FireDAC.Comp.DataSet,
  FireDAC.Phys.ODBCBase, FireDAC.Stan.ExprFuncs, FireDAC.Phys.SQLiteDef,
  FireDAC.Phys.IBDef, IPPeerClient, FireDAC.Phys.DSDef, FireDAC.Phys.TDBXBase,
  FireDAC.Phys.DS, FireDAC.Phys.IBBase, FireDAC.Phys.IB, FireDAC.Phys.SQLite,
  MemDS, DBAccess, Ora, Vcl.ComCtrls, Vcl.CheckLst, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, FireDAC.Phys.FB, FireDAC.Phys.FBDef, System.MaskUtils, _BibliotecaGenerica,
  OraCall;

type
  TFrmMenuPrincipal = class(TForm)
    pgcDados: TPageControl;
    tsImportados: TTabSheet;
    tsNaoImportados: TTabSheet;
    tsErros: TTabSheet;
    mmoHistorico: TMemo;
    mmoErro: TMemo;
    mmoDuplicados: TMemo;
    tsRegistros: TTabSheet;
    mmoRegistro: TMemo;
    chklstOperacoes: TCheckListBox;
    pnl1: TPanel;
    lblHistorico: TLabel;
    pnl2: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    edtserver_dest: TEdit;
    eUsuarioOrigem: TEdit;
    edt_user_dest: TEdit;
    eSenhaOrigem: TEdit;
    edt_porta_dest: TEdit;
    edt_servico_dest: TEdit;
    btnconectar_orig: TButton;
    btnconectar_dest: TButton;
    edtconectado_orig: TEdit;
    edtconectado_dest: TEdit;
    lblBase: TLabel;
    lbl1: TLabel;
    edtserver_padrao: TEdit;
    edt_user_padrao: TEdit;
    edt_senha_padrao: TEdit;
    edt_porta_padrao: TEdit;
    edt_servico_padrao: TEdit;
    btnConectar_padrao: TButton;
    edtconectado_Padrao: TEdit;
    Label2: TLabel;
    lblQuant: TLabel;
    lblImportados: TLabel;
    lblDuplicados: TLabel;
    img1: TImage;
    chkSP: TCheckBox;
    lblDupl: TLabel;
    lblImp: TLabel;
    OraStoredProc1: TOraStoredProc;
    eFornecedorId: TEdit;
    Label4: TLabel;
    fdConexao: TFDConnection;
    qCidades: TFDQuery;
    qTelefones: TFDQuery;
    qTelefonesTELEFONE: TStringField;
    qTelefonesCELULAR: TStringField;
    qTelefonesCOMERCIAL: TStringField;
    oraOrigem: TOraSession;
    eIpServidorOrigem: TEdit;
    ePortaOrigem: TEdit;
    eServicoOrigem: TEdit;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    qCidadesOrigem: TOraQuery;
    qBairros: TOraQuery;
    qCadastros: TOraQuery;
    qClientes: TOraQuery;
    qFornecedor: TOraQuery;
    qFornecedorCADASTRO_ID: TFloatField;
    qFornecedorTIPO_FORNECEDOR: TStringField;
    qMarcas: TOraQuery;
    qMarcasMARCA_ID: TIntegerField;
    qMarcasNOME: TStringField;
    qUnidades: TOraQuery;
    qUnidadesUNIDADE_ID: TStringField;
    qUnidadesNOME: TStringField;
    qReceber: TOraQuery;
    qPagar: TOraQuery;
    qProdutos: TOraQuery;
    qPrecos: TOraQuery;
    qEstoques: TOraQuery;
    qCustos: TOraQuery;
    qCidadesOrigemCIDADE_ID: TFloatField;
    qCidadesOrigemCODIGO_IBGE: TIntegerField;
    qCidadesOrigemNOME: TStringField;
    qCidadesOrigemUF: TStringField;
    qBairrosNOME: TStringField;
    qBairrosNOME_CIDADE: TStringField;
    qBairrosESTADO_ID: TStringField;
    qCadastrosCADASTRO_ID: TFloatField;
    qCadastrosRAZAO_SOCIAL: TStringField;
    qCadastrosNOME_FANTASIA: TStringField;
    qCadastrosTIPO_PESSOA: TStringField;
    qCadastrosCPF_CNPJ: TStringField;
    qCadastrosINSCRICAO_ESTADUAL: TStringField;
    qCadastrosNOME_BAIRRO: TStringField;
    qCadastrosNOME_CIDADE: TStringField;
    qCadastrosESTADO_ID: TStringField;
    qCadastrosCEP: TStringField;
    qCadastrosLOGRADOURO: TStringField;
    qCadastrosNUMERO_LOGRADOURO: TStringField;
    qCadastrosOBSERVACOES: TStringField;
    qCadastrosDATA_NASCIMENTO: TDateTimeField;
    qCadastrosCOMPLEMENTO: TStringField;
    qCadastrosPONTO_REFERENCIA: TStringField;
    qCadastrosTELEFONE: TStringField;
    qCadastrosCELULAR: TStringField;
    qCadastrosFAX: TStringField;
    qCadastrosE_MAIL: TStringField;
    qClientesCADASTRO_ID: TFloatField;
    qClientesTIPO_CLIENTE: TStringField;
    qClientesLIMITE_CREDITO: TFloatField;
    qClientesLIMITE_CREDITO_MENSAL: TFloatField;
    qClientesDATA_APROVACAO: TDateTimeField;
    qClientesNOME_PAI: TStringField;
    qClientesNOME_MAE: TStringField;
    qModulo: TOraQuery;
    qModuloMODULO_ID: TFloatField;
    qModuloDESCRICAO: TStringField;
    qRua: TOraQuery;
    qNivel: TOraQuery;
    qVao: TOraQuery;
    qRuaRUA_ID: TFloatField;
    qRuaDESCRICAO: TStringField;
    qNivelNIVEL_ID: TFloatField;
    qNivelDESCRICAO: TStringField;
    qVaoVAO_ID: TFloatField;
    qVaoDESCRICAO: TStringField;
    qEnderecoEstoque: TOraQuery;
    qEnderecoEstoqueENDERECO_ID: TFloatField;
    qEnderecoEstoqueMODULO_ID: TFloatField;
    qEnderecoEstoqueNIVEL_ID: TFloatField;
    qEnderecoEstoqueRUA_ID: TFloatField;
    qEnderecoEstoqueVAO_ID: TFloatField;
    qProdutosPRODUTO_ID: TFloatField;
    qProdutosNOME: TStringField;
    qProdutosNOME_COMPRA: TStringField;
    qProdutosATIVO: TStringField;
    qProdutosUNIDADE_VENDA: TStringField;
    qProdutosLINHA_PRODUTO_ID: TStringField;
    qProdutosMARCA_ID: TIntegerField;
    qProdutosFORNECEDOR_ID: TFloatField;
    qProdutosCODIGO_NCM: TStringField;
    qProdutosCODIGO_BARRAS: TStringField;
    qProdutosPESO: TFloatField;
    qProdutosCODIGO_ORIGINAL_FABRICANTE: TStringField;
    qProdutosCEST: TStringField;
    qCustosPRODUTO_ID: TFloatField;
    qCustosCMV: TFloatField;
    qCustosCUSTO_COMPRA_COMERCIAL: TFloatField;
    qPrecosPRODUTO_ID: TFloatField;
    qPrecosPRECO_VAREJO: TFloatField;
    qPrecosPRECO_ATACADO_1: TFloatField;
    qMultiploCompra: TOraQuery;
    qMultiploCompraMULTIPLO: TFloatField;
    qMultiploCompraPRODUTO_ID: TFloatField;
    qMultiploCompraQUANTIDADE_EMBALAGEM: TFloatField;
    qMultiploCompraUNIDADE_ID: TStringField;
    qMultiploCompraORDEM: TIntegerField;
    qLinhasProdutos: TOraQuery;
    qLinhasProdutosDEPTO_SECAO_LINHA_ID: TStringField;
    qLinhasProdutosATIVO: TStringField;
    qLinhasProdutosCOMISSAO_VENDA_PRAZO: TFloatField;
    qLinhasProdutosCOMISSAO_VENDA_VISTA: TFloatField;
    qLinhasProdutosDEPTO_SECAO_LINHA_PAI_ID: TStringField;
    qLinhasProdutosGRUPO_PRODUTOS_WEB_ID: TStringField;
    qLinhasProdutosNOME: TStringField;
    qLinhasProdutosPERC_COMISSAO_PROFISSIONAL: TFloatField;
    qLinhasProdutosPERC_MARG_PRETEND_PRECO_AT1: TFloatField;
    qLinhasProdutosPERC_MARG_PRETEND_PRECO_AT2: TFloatField;
    qLinhasProdutosPERC_MARG_PRETEND_PRECO_AT3: TFloatField;
    qLinhasProdutosPERC_MARG_PRETEND_PRECO_PON: TFloatField;
    qLinhasProdutosPERC_MARG_PRETEND_PRECO_VAR: TFloatField;
    qPagarPAGAR_ID: TFloatField;
    qPagarCADASTRO_ID: TFloatField;
    qPagarDOCUMENTO: TStringField;
    qPagarDATA_VENCIMENTO: TDateTimeField;
    qPagarVALOR_DOCUMENTO: TFloatField;
    qPagarDATA_CADASTRO: TDateTimeField;
    qPagarVALOR_ADIANTADO: TFloatField;
    qPagarVALOR_DESCONTO: TFloatField;
    qPagarOBSERVACOES: TStringField;
    qReceberRECEBER_ID: TFloatField;
    qReceberCADASTRO_ID: TFloatField;
    qReceberDOCUMENTO: TStringField;
    qReceberDATA_VENCIMENTO: TDateTimeField;
    qReceberVALOR_DOCUMENTO: TFloatField;
    qReceberDATA_CADASTRO: TDateTimeField;
    qReceberOBSERVACOES: TStringField;
    qReceberSTATUS: TStringField;
    qReceberVALOR_ADIANTADO: TFloatField;
    eEmpresaFinanceiro: TEdit;
    Label9: TLabel;
    qEnderecoEstoqueProduto: TOraQuery;
    qEnderecoEstoqueProdutoENDERECO_ID: TFloatField;
    qEnderecoEstoqueProdutoPOSICAO: TIntegerField;
    qEnderecoEstoqueProdutoPRODUTO_ID: TFloatField;
    qEstoquesEMPRESA_ID: TIntegerField;
    qEstoquesLOCAL_ID: TIntegerField;
    qEstoquesLOTE: TStringField;
    qEstoquesPRODUTO_ID: TFloatField;
    qEstoquesDISPONIVEL: TFloatField;
    edt_senha_dest: TEdit;
    procedure btnconectar_origClick(Sender: TObject);
    procedure btnconectar_destClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    function  roda_sql(vConexao:TConexao;vSql:string):Boolean;
    procedure Habilita_Botoes;
    procedure btnConectar_padraoClick(Sender: TObject);
    procedure img1DblClick(Sender: TObject);
  private
    { Private declarations }

    Conexao_orig : TConexao;
    Conexao_dest : TConexao;
    Conexao_Padrao : TConexao;
  public
    { Public declarations }
    procedure CalculaRegistros(Fconexao_dest:TConexao);

  end;

var
 FrmMenuPrincipal: TFrmMenuPrincipal;

implementation

{$R *.dfm}

 procedure TFrmMenuPrincipal.CalculaRegistros(Fconexao_dest:TConexao);
var
  vSql,vQtdReg:string;
  FConsulta_dest,FConsulta_dest01 : TConsulta;
  i:integer;
begin
  mmoRegistro.Lines.Clear;
  vSql :='select table_name from all_tables' +
       ' where owner = ''' + edt_user_dest.Text + '''' +
       ' ORDER BY table_name';

  FConsulta_dest   := TConsulta.Create(Fconexao_dest);
  FConsulta_dest01 := TConsulta.Create(Fconexao_dest);

  try
    mmoRegistro.Lines.Clear;
    FConsulta_dest.Limpar;
    FConsulta_dest.Add(vSql);
    FConsulta_dest.Pesquisar;

    for i := 0 to FConsulta_dest.GetQuantidadeRegistros -1 do
    begin
      vSql :='select count(*) as quantreg from ' + FConsulta_dest.GetString('table_name');
      FConsulta_dest01.Limpar;
      FConsulta_dest01.Add(vSql);
      FConsulta_dest01.Pesquisar;
      vQtdReg := FConsulta_dest01.GetString('quantreg');

      vQtdReg := FormatFloat('000000',StrToInt(vQtdReg));
      mmoRegistro.Lines.Add( vQtdReg + '  ' + FConsulta_dest.GetString('table_name'));

      FConsulta_dest.Next;
    end;
  finally
    FConsulta_dest.Free;
    FConsulta_dest01.Free;
  end;
end;

function TFrmMenuPrincipal.roda_sql(vConexao:TConexao;vSql:string):Boolean;
var
  vExec: TExecucao;
begin
  Result := True;

  try
    Application.ProcessMessages;
    vExec := TExecucao.Create(vConexao);

    vConexao.IniciarTransacao;
    vExec.SQL.Add(vSql);
    vExec.ExecSQL;
    vConexao.FinalizarTransacao;

  except
    on e: Exception do begin
      Result := False;
      vConexao.VoltarTransacao;
      ShowMessage('Erro ao executar limpeza no banco de Destino.' +
      e.Message);
      Abort;
    end;
  end;
end;

procedure TFrmMenuPrincipal.btnconectar_destClick(Sender: TObject);
var
  vProc: TProcedimentoBanco;
begin
  if btnconectar_dest.Caption = 'Conectar' then
  begin
    try
      Conexao_dest := _Conexao.TConexao.Create(Application);

      Conexao_dest.Conectar(
        edt_user_dest.Text,
        edt_senha_dest.Text,
        edtserver_dest.Text,
        StrToInt(edt_porta_dest.Text),
        edt_servico_dest.Text,
        True
      );

      if Conexao_dest.Connected then
      begin
        edtconectado_dest.Text := 'Conectado';
        edtconectado_dest.Color := clGreen;
        btnconectar_dest.Caption := 'Desconectar';

        vProc := TProcedimentoBanco.Create(Conexao_dest, 'INICIAR_SESSAO');
        vProc.Params[0].AsInteger := 1;
        vProc.Params[1].AsInteger := 1;
        vProc.Params[2].AsString := 'IMPORTADOR';
        vProc.Params[3].AsString := 'IMPORTADOR';

        vProc.Executar;

        CalculaRegistros(Conexao_dest);
      end;
    except
      on e: Exception do begin
        ShowMessage('Falha ao instanciar objeto de conex�o com o oracle.' +
        e.Message);
        edtconectado_dest.Text := 'Desconectado';
        edtconectado_dest.Color := clRed;
        //Exclamar('Falha ao instanciar objeto de conex�o com o oracle! ' + e.Message);
        //Halt;
      end;
    end;
  end else
  begin
    if Conexao_dest.Connected then
    begin
      Conexao_dest.Disconnect;
      edtconectado_dest.Text := 'Desconectado';
      edtconectado_dest.Color := clRed;
      btnconectar_dest.Caption := 'Conectar';
    end;
  end;
end;

procedure TFrmMenuPrincipal.btnconectar_origClick(Sender: TObject);
begin
  if oraOrigem.Connected then begin
    oraOrigem.Connected := False;
    edtconectado_orig.Text := 'Desconectedo';
    edtconectado_orig.Color := clRed;
    btnconectar_orig.Caption := 'Conecter';
  end
  else begin
    oraOrigem.Server   := eIpServidorOrigem.Text + ':' + ePortaOrigem.Text + ':' + eServicoOrigem.Text;
    oraOrigem.Password := eSenhaOrigem.Text;
    oraOrigem.ConnectString := eUsuarioOrigem.Text + '/' + eSenhaOrigem.Text + '@' + eIpServidorOrigem.Text + ':' + ePortaOrigem.Text + ':' + eServicoOrigem.Text;
    oraOrigem.Connected := True;

    if oraOrigem.Connected then begin
      edtconectado_orig.Text := 'Conectado';
      edtconectado_orig.Color := clGreen;
      btnconectar_orig.Caption := 'Desconectar';
    end;
  end;
end;

procedure TFrmMenuPrincipal.btnConectar_padraoClick(Sender: TObject);
begin
  if btnConectar_padrao.Caption = 'Conectar' then
  begin
    try
      Conexao_Padrao := _Conexao.TConexao.Create(Application);

      Conexao_Padrao.Conectar(
        edt_user_padrao.Text,
        edt_senha_padrao.Text,
        edtserver_padrao.Text,
        StrToInt(edt_porta_padrao.Text),
        edt_servico_padrao.Text,
        True
      );

      if Conexao_Padrao.Connected then
      begin
        edtconectado_Padrao.Text := 'Conectado';
        edtconectado_Padrao.Color := clGreen;
        btnConectar_padrao.Caption := 'Desconectar';

//        orstrdprc1.StoredProcName := 'INICIAR_SESSAO';
//                orstrdprc1.Prepare;
//        orstrdprc1.Params[0].AsInteger := 1;
//        orstrdprc1.Params[1].AsInteger := 1;
//        orstrdprc1.Params[2].AsString := 'IMPORTADOR';
//        orstrdprc1.Params[3].AsString := 'IMPORTADOR';
//
//        orstrdprc1.ExecProc;
      end;
    except
      on e: Exception do begin
        ShowMessage('Falha ao instanciar objeto de conex�o com o oracle.' +
        e.Message);
        edtconectado_Padrao.Text := 'Desconectado';
        edtconectado_Padrao.Color := clRed;
        //Exclamar('Falha ao instanciar objeto de conex�o com o oracle! ' + e.Message);
        Halt;
      end;
    end;
  end else
  begin
    if Conexao_Padrao.Connected then
    begin
      Conexao_Padrao.Disconnect;
      edtconectado_Padrao.Text := 'Desconectado';
      edtconectado_Padrao.Color := clRed;
      btnConectar_padrao.Caption := 'Conectar';
    end;
  end;
end;

procedure TFrmMenuPrincipal.FormShow(Sender: TObject);
begin
//  edt_user_orig.Text := 'BASELIMPAOFICIAL';
//  edt_senha_orig.Text := 'DADOS';
//  edtserver_orig.Text := 'localhost';

//  edt_user_dest.Text := 'BASELIMPA';
//  edt_senha_dest.Text := 'DADOS';
//  edtserver_dest.Text := 'localhost';
//  edt_senha_dest.Text := 'DADOS';
//  edt_senha_padrao.Text := 'DADOS';

 {0  Limpar Base de Destino
  1  Cidades
  2  Rotas
  3  Bairros
  4  Cargos
  5  Pessoas
  6  Funcionarios
  7  Clientes
  8  Grupo de Fornecedor
  9  Fornecedores
  10 Transportadoras
  11 Veiculos
  12 Empresas
  13 Marcas
  14 Unidades
  15 Departamentos
  16 Produtos
  17 Precos
  18 Custos
  19 Estoque
  20 Portadores
  21 Contas a Receber
  22 Contas a Pagar }
  //chklstOperacoes.ItemEnabled[2]:= False;
  //chklstOperacoes.ItemEnabled[6]:= False;
  //chklstOperacoes.ItemEnabled[8]:= False;
//  chklstOperacoes.ItemEnabled[10]:= False;
//  chklstOperacoes.ItemEnabled[11]:= False;
//  chklstOperacoes.ItemEnabled[12]:= False;
//  chklstOperacoes.ItemEnabled[19]:= False;
//  chklstOperacoes.ItemEnabled[20]:= False;
end;

procedure TFrmMenuPrincipal.Habilita_Botoes;
begin
//
end;

procedure TFrmMenuPrincipal.img1DblClick(Sender: TObject);
var
  ok:Boolean;
  Fcidade:TArray<RecCidade>;
  Fbairro:TArray<RecBairro>;
  Fcadastro:TArray<RecCadastro>;
  FReceber:TArray<RecReceber>;
  FPagar:TArray<RecPagar>;
  FModulo:TArray<RecModulo>;
  FNivel:TArray<RecNivel>;
  FVao:TArray<RecVao>;
  FEnderecosEstoque:TArray<RecEnderecosEstoque>;
  FEnderecosEstoqueProduto:TArray<RecEnderecosEstoqueProduto>;
  FRua:TArray<RecRua>;
  FProdutos: TArray<RecProduto>;
  FLinhasProdutos: TArray<RecLinhasProdutos>;
  FMultiploCompra: TArray<RecMultiploCompra>;
  FMarcas: TArray<RecMarca>;
  FUnidades: TArray<RecUnidade>;
  i: Integer;
  indTel: Integer;
begin
  if not ((edtconectado_orig.Text = 'Conectado') and (edtconectado_dest.Text = 'Conectado')) then
  begin
    ShowMessage('Verifique as conex�es!');
    Abort;
  end;

 {0  Limpar Base de Destino
  1  Cidades
  2  Rotas
  3  Bairros
  4  Cargos
  5  Pessoas
  6  Funcionarios
  7  Clientes
  8  Grupo de Fornecedor
  9  Fornecedores
  10 Transportadoras
  11 Veiculos
  12 Empresas
  13 Marcas
  14 Unidades
  15 Departamentos
  16 Produtos
  17 Precos
  18 Custos
  19 Estoque
  20 Portadores
  21 Contas a Receber
  22 Contas a Pagar }

 ok := True;

 if chklstOperacoes.Checked[0] then
   ok := LimparBaseDest(Conexao_dest,lblHistorico,lblImportados,lblDuplicados,mmoHistorico,mmoErro,edt_user_dest.Text);

// if edtconectado_Padrao.Text = 'Conectado' then
//   ok:= Importa_Origens(chkSP.Checked,Conexao_Padrao,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

 if ok and chklstOperacoes.Checked[1] then begin
  qCidadesOrigem.Active := False;
  qCidadesOrigem.Active := True;

  qCidadesOrigem.Last;
  qCidadesOrigem.First;

  SetLength(Fcidade, qCidadesOrigem.RecordCount);
  qCidadesOrigem.First;
  for i := 0 to qCidadesOrigem.RecordCount - 1 do begin
    Fcidade[i].EstadoId  := qCidadesOrigemUF.AsString;
    Fcidade[i].CodIbge   := qCidadesOrigemCODIGO_IBGE.AsString;
    Fcidade[i].Nome      := qCidadesOrigemNOME.AsString;

    qCidadesOrigem.Next;
  end;

  if Fcidade <> nil then
    ok := Importa_Cidades(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FCidade)
  else
    ShowMessage('Nenhuma cidade para importa��o!');
 end;

// if ok and chklstOperacoes.Checked[2] then
//   ok := Importa_Rotas(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

 if ok and chklstOperacoes.Checked[3] then begin
  qBairros.Active := False;
  qBairros.Active := True;

  qBairros.Last;
  qBairros.First;

  SetLength(FBairro, qBairros.RecordCount);
  qBairros.First;

  for i := 0 to qBairros.RecordCount - 1 do begin
    Fbairro[i].Bairro  := qBairrosNOME.AsString;
    Fbairro[i].Cidade  := qBairrosNOME_CIDADE.AsString;
    Fbairro[i].Estado  := qBairrosESTADO_ID.AsString;

    qBairros.Next;
  end;

  if Fbairro <> nil then
    ok := Importa_Bairros(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FBairro)
  else
    ShowMessage('Nenhum bairro para importa��o!');
 end;


// if ok and chklstOperacoes.Checked[4] then
//   ok := Importa_Cargos(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[4] then
//   ok := Importa_Profissionais_Cargos(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

 if ok and chklstOperacoes.Checked[5] then begin
  Fcadastro := nil;
  qCadastros.Active := False;
  qCadastros.Active := True;

  qCadastros.Last;

  SetLength(Fcadastro, qCadastros.RecordCount);
  qCadastros.First;

  for i := 0 to qCadastros.RecordCount - 1 do begin
    Fcadastro[i].Cadastro_id        := qCadastrosCADASTRO_ID.AsString;
    Fcadastro[i].tp_pessoa          := qCadastrosTIPO_PESSOA.AsString;
    Fcadastro[i].Nome               := qCadastrosRAZAO_SOCIAL.AsString;
    Fcadastro[i].NomeFantasia       := qCadastrosNOME_FANTASIA.AsString;
    Fcadastro[i].endereco           := qCadastrosLOGRADOURO.AsString;
    Fcadastro[i].Bairro             := qCadastrosNOME_BAIRRO.AsString;
    Fcadastro[i].Cidade             := qCadastrosNOME_CIDADE.AsString;
    Fcadastro[i].Estado             := qCadastrosESTADO_ID.AsString;

    Fcadastro[i].cep                := RetornaNumeros(qCadastrosCEP.AsString);
    if Length(FCadastro[i].cep) <> 8 then
      FCadastro[i].cep := '7640000';

    Fcadastro[i].cep := FormatMaskText('00000\-000;0;', Fcadastro[i].cep);

    Fcadastro[i].data_nascimento  := qCadastrosDATA_NASCIMENTO.AsString;
    Fcadastro[i].cpf              := qCadastrosCPF_CNPJ.AsString;
    Fcadastro[i].cnpj             := qCadastrosCPF_CNPJ.AsString;
    Fcadastro[i].inscricao        := qCadastrosINSCRICAO_ESTADUAL.AsString;
    Fcadastro[i].numero_end       := qCadastrosNUMERO_LOGRADOURO.AsString;
    Fcadastro[i].complemento      := qCadastrosCOMPLEMENTO.AsString;
    Fcadastro[i].ponto_referencia := qCadastrosPONTO_REFERENCIA.AsString;
    Fcadastro[i].observacoes      := qCadastrosOBSERVACOES.AsString;
    Fcadastro[i].email            := qCadastrosE_MAIL.AsString;

    Fcadastro[i].telefone    := RetornaNumeros(qCadastrosTELEFONE.AsString);
    if (Length(Fcadastro[i].telefone) = 8) or (Length(Fcadastro[i].telefone) = 9) then
      Fcadastro[i].telefone := '62' + Fcadastro[i].telefone;
    Fcadastro[i].telefone    := FormatMaskText('\(00\)\ 0000\-0009;0;', Fcadastro[i].telefone);

    Fcadastro[i].celular     := RetornaNumeros(qCadastrosCELULAR.AsString);
    if (Length(Fcadastro[i].celular) = 8) or (Length(Fcadastro[i].celular) = 9) then
      Fcadastro[i].celular := '62' + Fcadastro[i].celular;

    Fcadastro[i].celular     := FormatMaskText('\(00\)\ 00000\-0009;0;', Fcadastro[i].celular);

    qCadastros.Next;
  end;

  if Fcadastro <> nil then
    ok := Importa_Pessoas(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, Fcadastro, qTelefones)
  else
    ShowMessage('Nenhum pessoa para importa��o!');

 end;


// if ok and chklstOperacoes.Checked[5] then
//   ok := Importa_Telefones (chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[5] then
//   ok := Importa_DIVERSOS_CONTATOS(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[5] then
//   ok := Importa_EMAILS (chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[5] then
//   ok := Importa_ENDERECOS (chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[6] then
//   ok := Importa_Funcionarios(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[6] then
//   ok := Importa_Profissionais(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[7] then
//   ok := Importa_GRUPOS_FINANCEIROS(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[7] then
//   ok := Importa_CLIENTES_GRUPOS(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[7] then
//   ok := Importa_CLIENTES_ORIGENS(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

 if ok and chklstOperacoes.Checked[7] then begin
  Fcadastro := nil;
  qClientes.Active := False;
  qClientes.Active := True;

  qClientes.Last;

  SetLength(Fcadastro, qClientes.RecordCount);
  qClientes.First;

  for i := 0 to qClientes.RecordCount - 1 do begin
    Fcadastro[i].Cadastro_id       := qClientesCADASTRO_ID.AsString;
    Fcadastro[i].tipo_cliente      := qClientesTIPO_CLIENTE.AsString;
    Fcadastro[i].limite_credito    := qClientesLIMITE_CREDITO.AsString;
    Fcadastro[i].limite_credito_m  := qClientesLIMITE_CREDITO_MENSAL.AsString;
    Fcadastro[i].data_aprovacao_c  := qClientesDATA_APROVACAO.AsString;
    Fcadastro[i].nome_pai          := qClientesNOME_PAI.AsString;
    Fcadastro[i].nome_mae          := qClientesNOME_MAE.AsString;

    qClientes.Next;
  end;

  if Fcadastro <> nil then
   ok := Importa_Clientes(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, Fcadastro)
  else
    ShowMessage('Nenhum pessoa para importa��o!');

 end;

// if ok and chklstOperacoes.Checked[8] then
//   ok := Importa_GrupoFornecedor(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

 if ok and chklstOperacoes.Checked[9] then begin
  Fcadastro := nil;

  qFornecedor.Active := False;
  qFornecedor.Active := True;

  qFornecedor.Last;

  SetLength(Fcadastro, qFornecedor.RecordCount);
  qFornecedor.First;

  for i := 0 to qFornecedor.RecordCount - 1 do begin
    Fcadastro[i].Cadastro_id := qFornecedorCADASTRO_ID.AsString;
    qFornecedor.Next;
  end;

  if Fcadastro <> nil then
    ok := Importa_Fornecedores(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, Fcadastro)
  else
    ShowMessage('Nenhum pessoa para importa��o!');

 end;

//  if ok and chklstOperacoes.Checked[10] then
//   ok := Importa_Transportadoras(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[11] then
//   ok := Importa_Motoristas(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[11] then
//   ok := Importa_Veiculos(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);;

// if ok and chklstOperacoes.Checked[12] then
//   ok := Importa_Empresas(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

 if ok and chklstOperacoes.Checked[13] then begin
  qMarcas.Active := False;
  qMarcas.Active := True;

  qMarcas.Last;
  qMarcas.First;

  SetLength(FMarcas, qMarcas.RecordCount);
  qMarcas.First;

  for i := 0 to qMarcas.RecordCount - 1 do begin
    FMarcas[i].Codigo  := qMarcasMARCA_ID.AsString;
    FMarcas[i].Marca   := qMarcasNOME.AsString;
    qMarcas.Next;
  end;

  if qMarcas <> nil then
    ok := Importa_Marcas(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FMarcas)
  else
    ShowMessage('Nenhuma marca para importa��o!');
 end;

 if ok and chklstOperacoes.Checked[14] then begin
  qUnidades.Active := False;
  qUnidades.Active := True;

  qUnidades.Last;
  qUnidades.First;

  SetLength(FUnidades, qUnidades.RecordCount);
  qUnidades.First;

  for i := 0 to qUnidades.RecordCount - 1 do begin
    FUnidades[i].codigo  := qUnidadesUNIDADE_ID.AsString;
    FUnidades[i].unidade   := qUnidadesUNIDADE_ID.AsString;
    FUnidades[i].descricao   := qUnidadesNOME.AsString;
    qUnidades.Next;
  end;

  if FUnidades <> nil then
    ok := Importa_Unidades(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FUnidades)
  else
    ShowMessage('Nenhuma unidade para importa��o!');
 end;

 if ok and chklstOperacoes.Checked[15] then begin
  qLinhasProdutos.Active := False;
  qLinhasProdutos.Active := True;

  qLinhasProdutos.Last;
  qLinhasProdutos.First;

  SetLength(FLinhasProdutos, qLinhasProdutos.RecordCount);
  qLinhasProdutos.First;

  for i := 0 to qLinhasProdutos.RecordCount - 1 do begin
    FLinhasProdutos[i].deptoSecaoLinhaId        := qLinhasProdutosDEPTO_SECAO_LINHA_ID.AsString;
    FLinhasProdutos[i].comissaoVendaPrazo       := qLinhasProdutosCOMISSAO_VENDA_PRAZO.AsFloat;
    FLinhasProdutos[i].comissaoVendaVista       := qLinhasProdutosCOMISSAO_VENDA_VISTA.AsFloat;
    FLinhasProdutos[i].deptoSecaoLinhaPaiId     := qLinhasProdutosDEPTO_SECAO_LINHA_PAI_ID.AsString;
    FLinhasProdutos[i].grupoProdutosWebId       := qLinhasProdutosGRUPO_PRODUTOS_WEB_ID.AsString;
    FLinhasProdutos[i].nome                     := qLinhasProdutosNOME.AsString;
    FLinhasProdutos[i].percComissaoProfissional := qLinhasProdutosPERC_COMISSAO_PROFISSIONAL.AsFloat;
    FLinhasProdutos[i].percMargPretendPrecoAt1  := qLinhasProdutosPERC_MARG_PRETEND_PRECO_AT1.AsFloat;
    FLinhasProdutos[i].percMargPretendPrecoAt2  := qLinhasProdutosPERC_MARG_PRETEND_PRECO_AT2.AsFloat;
    FLinhasProdutos[i].percMargPretendPrecoAt3  := qLinhasProdutosPERC_MARG_PRETEND_PRECO_AT3.AsFloat;
    FLinhasProdutos[i].percMargPretendPrecoPon  := qLinhasProdutosPERC_MARG_PRETEND_PRECO_PON.AsFloat;
    FLinhasProdutos[i].percMargPretendPrecoVar  := qLinhasProdutosPERC_MARG_PRETEND_PRECO_VAR.AsFloat;
    qLinhasProdutos.Next;
  end;

  if FLinhasProdutos <> nil then
    ok := Importa_Grupos_Produtos(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FLinhasProdutos)
  else
    ShowMessage('Nenhuma linha de produto para importa��o');
 end;

 if ok and chklstOperacoes.Checked[16] then begin
  FProdutos := nil;
  qProdutos.Active := False;
  qProdutos.Active := True;

  qProdutos.Last;
  qProdutos.First;

  SetLength(FProdutos, qProdutos.RecordCount);
  qProdutos.First;

  for i := 0 to qProdutos.RecordCount - 1 do begin
    FProdutos[i].Codigo         := qProdutosPRODUTO_ID.AsString;
    FProdutos[i].codigo_barras  := qProdutosCODIGO_BARRAS.AsString;
    FProdutos[i].cod_marca      := qProdutosMARCA_ID.AsString;
    FProdutos[i].nome           := qProdutosNOME.AsString;
    FProdutos[i].unidade        := qProdutosUNIDADE_VENDA.AsString;
    FProdutos[i].multiplo_venda := 0;
    FProdutos[i].ativo          := 'S';
    FProdutos[i].fornecedor_id  := qProdutosFORNECEDOR_ID.AsString;
    FProdutos[i].ncm            := qProdutosCODIGO_NCM.AsString;
    FProdutos[i].cest           := qProdutosCEST.AsString;

//    if qProdutosID_TRIBUTA.AsString = '060' then begin
//      FProdutos[i].tributacao_estadual_compra := 2;
//      FProdutos[i].tributacao_estadual_venda  := 2;
//    end
//    else begin
      FProdutos[i].tributacao_estadual_compra := 1;
      FProdutos[i].tributacao_estadual_venda  := 1;
//    end;

    FProdutos[i].tributacao_federal_compra  := 1;
    FProdutos[i].tributacao_federal_venda   := 1;
    FProdutos[i].tipo_def_automatica_lote := 'V';
    FProdutos[i].peso := qProdutosPESO.AsFloat;
    FProdutos[i].cod_original_fabriacante := qProdutosCODIGO_ORIGINAL_FABRICANTE.asString;
    FProdutos[i].dpto_sessao :=  qProdutosLINHA_PRODUTO_ID.AsString;

    qProdutos.Next;
  end;

  if FProdutos <> nil then
    ok := Importa_Produtos(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, eFornecedorId.Text, FProdutos)
  else
    ShowMessage('Nenhum produto para importa��o!');
 end;

// if ok and chklstOperacoes.Checked[16] then
//   ok := Importa_PRODUTOS_Relacionados(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[16] then
//   ok := Importa_PRODUTOS_kit(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

//  if ok and chklstOperacoes.Checked[16] then
//   ok := Importa_Produtos_lotes(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[16] then
//   ok := Importa_Produtos_Cod(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

 if ok and chklstOperacoes.Checked[17] then begin
  FProdutos := nil;
  qPrecos.Active := False;
  qPrecos.ParamByName('EMPRESA_ID').AsString := eEmpresaFinanceiro.Text;
  qPrecos.Active := True;

  qPrecos.Last;
  qPrecos.First;

  SetLength(FProdutos, qPrecos.RecordCount);
  qPrecos.First;

  for i := 0 to qPrecos.RecordCount - 1 do begin
    FProdutos[i].Codigo         := qPrecosPRODUTO_ID.AsString;
    FProdutos[i].multiplo_venda := 0;
    FProdutos[i].preco := qPrecosPRECO_VAREJO.AsFloat;
    FProdutos[i].preco_atacado := qPrecosPRECO_ATACADO_1.AsFloat;

    qPrecos.Next;
  end;

  if FProdutos <> nil then
    ok := Importa_Precos(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FProdutos)
  else
    ShowMessage('Nenhum produto para importa��o!');

 end;

// if ok and chklstOperacoes.Checked[17] then
//   ok := Importa_Promocoes(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

 if ok and chklstOperacoes.Checked[18] then begin
  FProdutos := nil;
  qCustos.Active := False;
  qCustos.ParamByName('EMPRESA_ID').AsString := eEmpresaFinanceiro.Text;
  qCustos.Active := True;

  qCustos.Last;
  qCustos.First;

  SetLength(FProdutos, qCustos.RecordCount);
  qCustos.First;

  for i := 0 to qCustos.RecordCount - 1 do begin
    FProdutos[i].Codigo                        := qCustosPRODUTO_ID.AsString;
    FProdutos[i].cmv                           := qCustosCMV.AsFloat;
    FProdutos[i].custo_comercial               := qCustosCUSTO_COMPRA_COMERCIAL.AsFloat;

    qCustos.Next;
  end;

  if FProdutos <> nil then
    ok := Importa_Custos(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FProdutos)
  else
    ShowMessage('Nenhum produto para importa��o!');

 end;

// if ok and chklstOperacoes.Checked[19] then
//   ok := Inicia_Estoques(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);
//
// if ok and chklstOperacoes.Checked[19] then
//   ok := Inicia_Estoques_Div(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

 if ok and chklstOperacoes.Checked[19] then begin
  FProdutos := nil;
  qEstoques.Active := False;
  qEstoques.ParamByName('EMPRESA_ID').AsString := eEmpresaFinanceiro.Text;
  qEstoques.Active := True;

  qEstoques.Last;
  qEstoques.First;

  SetLength(FProdutos, qEstoques.RecordCount);
  qEstoques.First;

  for i := 0 to qEstoques.RecordCount - 1 do begin
    FProdutos[i].Codigo         := qEstoquesPRODUTO_ID.AsString;
    FProdutos[i].estoque        := qEstoquesDISPONIVEL.AsFloat;
    FProdutos[i].local_id       := qEstoquesLOCAL_ID.AsInteger;
    FProdutos[i].lote           := qEstoquesLOTE.AsString;

    qEstoques.Next;
  end;

  if FProdutos <> nil then
    ok := Importa_Estoques(
      chkSP.Checked,
      Conexao_orig,
      Conexao_dest,
      lblHistorico,
      lblQuant,
      lblImportados,
      lblDuplicados,
      mmohistorico,
      mmoErro,
      mmoDuplicados,
      FProdutos,
      eEmpresaFinanceiro.Text
    )
  else
    ShowMessage('Nenhum produto para importa��o!');

 end;

 if ok and chklstOperacoes.Checked[20] then begin
  FMultiploCompra := nil;
  qMultiploCompra.Active := False;
  qMultiploCompra.Active := True;

  qMultiploCompra.Last;
  qMultiploCompra.First;

  SetLength(FMultiploCompra, qMultiploCompra.RecordCount);
  qMultiploCompra.First;

  for i := 0 to qMultiploCompra.RecordCount - 1 do begin
    FMultiploCompra[i].produtoId           := qMultiploCompraPRODUTO_ID.AsString;

    FMultiploCompra[i].multiplo            := qMultiploCompraMULTIPLO.AsFloat;
    FMultiploCompra[i].quantidadeEmbalagem := qMultiploCompraQUANTIDADE_EMBALAGEM.AsString;
    FMultiploCompra[i].unidaeId            := qMultiploCompraUNIDADE_ID.AsString;
    FMultiploCompra[i].ordem               := qMultiploCompraORDEM.AsString;

    qMultiploCompra.Next;
  end;

  if FMultiploCompra <> nil then
     ok := Multiplos_Compra(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FMultiploCompra)
  else
     ShowMessage('Nenhum multiplo de compra para importa��o');
 end;

// if ok and chklstOperacoes.Checked[19] then
//   ok := Importa_Tributacao(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[20] then
//   ok := Importa_Portadores(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[20] then
//   ok := Importa_Contas(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[20] then
//   ok := Importa_Planos_Financeiros(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[20] then
//   ok := Importa_Centros_Custos(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

 if ok and chklstOperacoes.Checked[21] then begin
  qReceber.Active := False;

  qReceber.ParamByName('EMPRESA_ID').AsString := eEmpresaFinanceiro.Text;

  qReceber.Active := True;

  qReceber.Last;
  qReceber.First;

  SetLength(FReceber, qReceber.RecordCount);
  qReceber.First;

  for i := 0 to qReceber.RecordCount - 1 do begin
    FReceber[i].id_participante  := qReceberCADASTRO_ID.AsString;
    FReceber[i].documento := qReceberDOCUMENTO.AsString;
    FReceber[i].data_emissao  := qReceberDATA_CADASTRO.AsString;
    FReceber[i].data_vencimento  := qReceberDATA_VENCIMENTO.AsString;
    FReceber[i].data_lancamento  := qReceberDATA_CADASTRO.AsString;
    FReceber[i].observacoes  := qReceberOBSERVACOES.AsString + ' ID Ant. ' + qReceberRECEBER_ID.AsString;
    FReceber[i].valor_aberto  := qReceberVALOR_DOCUMENTO.AsString;
    FReceber[i].id_receber  := qReceberRECEBER_ID.AsString;
    FReceber[i].valor_adiantado  := qReceberVALOR_ADIANTADO.AsString;

    qReceber.Next;
  end;

  if FReceber <> nil then
    ok := Importa_Contas_Receber(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FReceber)
  else
    ShowMessage('Nenhum contas a receber para importa��o!');

 end;

 if ok and chklstOperacoes.Checked[22] then begin
  qPagar.Active := False;
  qPagar.ParamByName('EMPRESA_ID').AsString := eEmpresaFinanceiro.Text;
  qPagar.Active := True;

  qPagar.Last;
  qPagar.First;

  SetLength(FPagar, qPagar.RecordCount);
  qPagar.First;

  for i := 0 to qPagar.RecordCount - 1 do begin
    FPagar[i].id_participante := qPagarCADASTRO_ID.AsString;
    FPagar[i].documento       := qPagarDOCUMENTO.AsString;
    FPagar[i].data_emissao    := qPagarDATA_CADASTRO.AsString;
    FPagar[i].data_vencimento := qPagarDATA_VENCIMENTO.AsString;
    FPagar[i].data_lancamento := qPagarDATA_CADASTRO.AsString;
    FPagar[i].observacoes     := qPagarOBSERVACOES.AsString + ' ID Ant. ' + qPagarPAGAR_ID.AsString;
    FPagar[i].valor_aberto    := qPagarVALOR_DOCUMENTO.AsString;
    FPagar[i].id_receber      := qPagarPAGAR_ID.AsString;
    FPagar[i].valor_adiantado := qPagarVALOR_ADIANTADO.AsString;
    FPagar[i].valor_desconto  := qPagarVALOR_DESCONTO.AsString;

    qPagar.Next;
  end;

  if FPagar <> nil then
     ok := Importa_Contas_Pagar(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FPagar)
  else
    ShowMessage('Nenhum contas a pagar para importa��o!');
 end;

// if ok and chklstOperacoes.Checked[23] then
//   ok := Importa_icms(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[24] then
//   ok := Importa_CFOP(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[24] then
//   ok := Importa_Cfop_Cst(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

// if ok and chklstOperacoes.Checked[24] then
//   ok := Importa_Cfop_Cst1(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados);

 if ok and chklstOperacoes.Checked[25] then begin
  qRua.Active := False;
  qRua.Active := True;

  qRua.Last;
  qRua.First;

  SetLength(FRua, qRua.RecordCount);
  qRua.First;

  for i := 0 to qRua.RecordCount - 1 do begin
    FRua[i].ruaId := qRuaRUA_ID.AsString;
    FRua[i].rua   := qRuaDESCRICAO.AsString;
    qRua.Next;
  end;

  if FRua <> nil then
     ok := Importa_Rua(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FRua)
  else
    ShowMessage('Nenhum rua para importa��o!');
 end;

 if ok and chklstOperacoes.Checked[26] then begin
  qNivel.Active := False;
  qNivel.Active := True;

  qNivel.Last;
  qNivel.First;

  SetLength(FNivel, qNivel.RecordCount);
  qNivel.First;

  for i := 0 to qNivel.RecordCount - 1 do begin
    FNivel[i].nivelId := qNivelNIVEL_ID.AsString;
    FNivel[i].nivel   := qNivelDESCRICAO.AsString;
    qNivel.Next;
  end;

  if FNivel <> nil then
     ok := Importa_Nivel(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FNivel)
  else
    ShowMessage('Nenhum n�vel para importa��o!');
 end;

 if ok and chklstOperacoes.Checked[27] then begin
  qModulo.Active := False;
  qModulo.Active := True;

  qModulo.Last;
  qModulo.First;

  SetLength(FModulo, qModulo.RecordCount);
  qModulo.First;

  for i := 0 to qModulo.RecordCount - 1 do begin
    FModulo[i].moduloId := qModuloMODULO_ID.AsString;
    FModulo[i].modulo   := qModuloDESCRICAO.AsString;
    qModulo.Next;
  end;

  if FModulo <> nil then
     ok := Importa_Modulos(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FModulo)
  else
    ShowMessage('Nenhum modulo para importa��o!');
 end;

 if ok and chklstOperacoes.Checked[28] then begin
  qVao.Active := False;
  qVao.Active := True;

  qVao.Last;
  qVao.First;

  SetLength(FVao, qVao.RecordCount);
  qVao.First;

  for i := 0 to qVao.RecordCount - 1 do begin
    FVao[i].vaoId := qVaoVAO_ID.AsString;
    FVao[i].vao   := qVaoDESCRICAO.AsString;
    qVao.Next;
  end;

  if FVao <> nil then
     ok := Importa_Vao(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FVao)
  else
    ShowMessage('Nenhum v�o para importa��o!');
 end;

 if ok and chklstOperacoes.Checked[30] then begin
  qEnderecoEstoqueProduto.Active := False;
  qEnderecoEstoqueProduto.Active := True;

  qEnderecoEstoqueProduto.Last;
  qEnderecoEstoqueProduto.First;

  SetLength(FEnderecosEstoqueProduto, qEnderecoEstoqueProduto.RecordCount);
  qEnderecoEstoqueProduto.First;

  for i := 0 to qEnderecoEstoqueProduto.RecordCount - 1 do begin
    FEnderecosEstoqueProduto[i].enderecoId := qEnderecoEstoqueProdutoENDERECO_ID.AsString;
    FEnderecosEstoqueProduto[i].produtoId  := qEnderecoEstoqueProdutoPRODUTO_ID.AsString;
    FEnderecosEstoqueProduto[i].posicao    := qEnderecoEstoqueProdutoPOSICAO.AsString;
    qEnderecoEstoqueProduto.Next;
  end;

  if FEnderecosEstoqueProduto <> nil then
     ok := Importa_Enderecos_Estoque_Produtos(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FEnderecosEstoqueProduto)
  else
    ShowMessage('Nenhum endere�o de estoque para importa��o!');
 end;

 if ok and chklstOperacoes.Checked[29] then begin
  qEnderecoEstoque.Active := False;
  qEnderecoEstoque.Active := True;

  qEnderecoEstoque.Last;
  qEnderecoEstoque.First;

  SetLength(FEnderecosEstoque, qEnderecoEstoque.RecordCount);
  qEnderecoEstoque.First;

  for i := 0 to qEnderecoEstoque.RecordCount - 1 do begin
    FEnderecosEstoque[i].enderecoId := qEnderecoEstoqueENDERECO_ID.AsString;
    FEnderecosEstoque[i].nivelId    := qEnderecoEstoqueNIVEL_ID.AsString;
    FEnderecosEstoque[i].moduloId   := qEnderecoEstoqueMODULO_ID.AsString;
    FEnderecosEstoque[i].ruaId      := qEnderecoEstoqueRUA_ID.AsString;
    FEnderecosEstoque[i].vaoId      := qEnderecoEstoqueVAO_ID.AsString;
    qEnderecoEstoque.Next;
  end;

  if FEnderecosEstoque <> nil then
     ok := Importa_Enderecos_Estoque(chkSP.Checked,Conexao_orig,Conexao_dest,lblHistorico,lblQuant,lblImportados,lblDuplicados,mmohistorico,mmoErro,mmoDuplicados, FEnderecosEstoque)
  else
    ShowMessage('Nenhum endere�o de estoque para importa��o!');
 end;

  CalculaRegistros(Conexao_dest);

  if ok then
    ShowMessage('Importacao Realizada com Sucesso!');
end;

end.
