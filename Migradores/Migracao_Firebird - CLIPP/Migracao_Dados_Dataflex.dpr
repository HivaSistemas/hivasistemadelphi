program Migracao_Dados_Dataflex;

uses
  Vcl.Forms,
  Menu_Principal in 'Menu_Principal.pas' {FrmMenuPrincipal},
  _Conexao in '..\Orientado a Objetos\Repositório\_Conexao.pas',
  _OperacoesBancoDados in '..\Orientado a Objetos\Repositório\_OperacoesBancoDados.pas',
  _MigraAdm in '_MigraAdm.pas',
  _LimpaBaseAltis in '_LimpaBaseAltis.pas',
  _InsertUpdate in '_InsertUpdate.pas',
  _BibliotecaGenerica in '..\Orientado a Objetos\Repositório\_BibliotecaGenerica.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrmMenuPrincipal, FrmMenuPrincipal);
  Application.Run;
end.
