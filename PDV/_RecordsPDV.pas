unit _RecordsPDV;

interface

uses
  System.Classes;

type

  RecProdutoVendaPDV = record
    ProdutoId: Integer;
    NomeProduto: string;
    MultiploVenda: Double;
    UnidadeVenda: string;
    MarcaId: Integer;
    NomeMarca: string;
    PrecoPDV: Currency;
    Quantidade: Double;
    ValorTotal: Double;
    produtoDiversosPDV: string;
    TipoControleEstoque: string;
    Foto1: TMemoryStream;
    Foto2: TMemoryStream;
    Foto3: TMemoryStream;
    Cancelado: Boolean;
  end;

implementation

end.
