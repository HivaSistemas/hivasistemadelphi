unit Buscar.Vendedor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, _Biblioteca,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameVendedores, Vcl.Buttons,
  Vcl.ExtCtrls;

type
  TFormBuscarVendedor = class(TFormHerancaFinalizar)
    FrVendedor: TFrVendedores;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Buscar(pVendedorId: Integer): TRetornoTelaFinalizar<Integer>;

implementation

{$R *.dfm}

function Buscar(pVendedorId: Integer): TRetornoTelaFinalizar<Integer>;
var
  vForm: TFormBuscarVendedor;
begin
  vForm := TFormBuscarVendedor.Create(Application);

  if pVendedorId > 0 then
    vForm.FrVendedor.InserirDadoPorChave(pVendedorId, False);

  if Result.Ok(vForm.ShowModal) then
    Result.Dados := vForm.FrVendedor.getVendedor().funcionario_id;

  vForm.Free;
end;

{ TFormBuscarVendedor }

procedure TFormBuscarVendedor.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrVendedor.EstaVazio then begin
    Exclamar('O vendedor n�o foi definido!');
    SetarFoco(FrVendedor);
    Abort;
  end;
end;

end.
