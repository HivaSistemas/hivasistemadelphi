inherited FormHerancaRelatorios1: TFormHerancaRelatorios1
  Caption = 'FormHerancaRelatorios1'
  ClientHeight = 364
  ClientWidth = 793
  ExplicitWidth = 799
  ExplicitHeight = 393
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 364
  end
  object GridLuka1: TGridLuka
    Left = 122
    Top = 0
    Width = 671
    Height = 364
    Align = alClient
    DefaultRowHeight = 19
    DrawingStyle = gdsClassic
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 1
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Data cadastro'
      'Tipo nota'
      'Numero nota'
      'Nome fantasia'
      'Valor total')
    Grid3D = False
    RealColCount = 5
    ExplicitLeft = 280
    ExplicitTop = 104
    ExplicitWidth = 320
    ExplicitHeight = 120
    ColWidths = (
      93
      64
      81
      165
      97)
  end
end
