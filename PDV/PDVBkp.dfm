inherited FormPDV: TFormPDV
  Caption = 'PDV'
  ClientHeight = 500
  ClientWidth = 785
  FormStyle = fsStayOnTop
  Visible = False
  ExplicitWidth = 791
  ExplicitHeight = 529
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel
    Left = 109
    Top = 5
    Width = 105
    Height = 16
    Caption = 'C'#243'digo do produto'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lb2: TLabel
    Left = 4
    Top = 5
    Width = 65
    Height = 16
    Caption = 'Quantidade'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lbNomeProduto: TLabel
    Left = 4
    Top = 57
    Width = 777
    Height = 31
    Alignment = taCenter
    AutoSize = False
    Caption = '458 - Video game PlayStation 4 - 2 controle'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lb4: TLabel
    Left = 4
    Top = 91
    Width = 59
    Height = 16
    Caption = 'Valor unit.'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lb5: TLabel
    Left = 107
    Top = 98
    Width = 97
    Height = 16
    Alignment = taCenter
    AutoSize = False
    Caption = 'Unidade'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lb6: TLabel
    Left = 4
    Top = 148
    Width = 59
    Height = 16
    Caption = 'Valor total'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object sbIniciarVenda: TSpeedButton
    Left = 4
    Top = 433
    Width = 83
    Height = 64
    BiDiMode = bdLeftToRight
    Caption = 'F1 Iniciar'
    ParentBiDiMode = False
    OnClick = sbIniciarVendaClick
  end
  object sbDefinirVendedor: TSpeedButton
    Left = 84
    Top = 433
    Width = 90
    Height = 64
    BiDiMode = bdLeftToRight
    Caption = 'F2 Def. vend.'
    ParentBiDiMode = False
    OnClick = sbDefinirVendedorClick
  end
  object sbPesquisarProduto: TSpeedButton
    Left = 345
    Top = 433
    Width = 90
    Height = 64
    BiDiMode = bdLeftToRight
    Caption = 'F5 Pesq. prod.'
    ParentBiDiMode = False
    OnClick = sbPesquisarProdutoClick
  end
  object sbDefinirCliente: TSpeedButton
    Left = 171
    Top = 433
    Width = 90
    Height = 64
    BiDiMode = bdLeftToRight
    Caption = 'F3 Def. cliente'
    ParentBiDiMode = False
    OnClick = sbDefinirClienteClick
  end
  object sbCancelarItem: TSpeedButton
    Left = 432
    Top = 433
    Width = 90
    Height = 64
    BiDiMode = bdLeftToRight
    Caption = 'F6 Canc. item'
    ParentBiDiMode = False
    OnClick = sbCancelarItemClick
  end
  object sbCancelarVenda: TSpeedButton
    Left = 604
    Top = 433
    Width = 90
    Height = 64
    BiDiMode = bdLeftToRight
    Caption = 'F8 Cancelar'
    ParentBiDiMode = False
    OnClick = sbCancelarVendaClick
  end
  object sbOutrasOperacoes: TSpeedButton
    Left = 691
    Top = 433
    Width = 90
    Height = 64
    BiDiMode = bdLeftToRight
    Caption = 'F9 Outras oper.'
    ParentBiDiMode = False
    OnClick = sbOutrasOperacoesClick
  end
  object lbUnidade: TLabel
    Left = 107
    Top = 117
    Width = 97
    Height = 18
    Alignment = taCenter
    AutoSize = False
    Caption = 'UND'
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object sbDefinirQuantidade: TSpeedButton
    Left = 258
    Top = 433
    Width = 90
    Height = 64
    BiDiMode = bdLeftToRight
    Caption = 'F4 Def. qtde.'
    ParentBiDiMode = False
    OnClick = sbDefinirQuantidadeClick
  end
  object sbFinalizarVenda: TSpeedButton
    Left = 519
    Top = 433
    Width = 88
    Height = 64
    BiDiMode = bdLeftToRight
    Caption = 'F7 Finalizar'
    ParentBiDiMode = False
    OnClick = sbFinalizarVendaClick
  end
  object sh1: TShape
    Left = 284
    Top = 408
    Width = 16
    Height = 16
    Anchors = [akLeft, akBottom]
    Brush.Color = clRed
    Pen.Color = clMaroon
  end
  object lb3: TLabel
    Left = 302
    Top = 410
    Width = 84
    Height = 14
    Anchors = [akLeft, akBottom]
    Caption = 'Item cancelado'
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lb7: TLabel
    Left = 513
    Top = 411
    Width = 35
    Height = 19
    Anchors = [akLeft, akBottom]
    Caption = 'Itens'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lb8: TLabel
    Left = 622
    Top = 411
    Width = 36
    Height = 19
    Anchors = [akLeft, akBottom]
    Caption = 'Total'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object sgItens: TGridLuka
    Left = 284
    Top = 91
    Width = 497
    Height = 314
    TabStop = False
    ColCount = 6
    DefaultRowHeight = 19
    DrawingStyle = gdsClassic
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goRowSelect]
    TabOrder = 2
    OnClick = sgItensClick
    OnDblClick = sgItensDblClick
    OnDrawCell = sgItensDrawCell
    OnKeyDown = sgItensKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Item'
      'Descri'#231#227'o'
      'Qtde.'
      'Valor unit.'
      'Und.'
      'Valor total')
    OnGetCellColor = sgItensGetCellColor
    Grid3D = False
    RealColCount = 6
    ColWidths = (
      38
      174
      60
      73
      39
      91)
  end
  object eCodigoProduto: TEditLuka
    Left = 109
    Top = 22
    Width = 352
    Height = 27
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = 6710886
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnKeyDown = eCodigoProdutoKeyDown
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
  end
  object eQuantidade: TEditLuka
    Left = 4
    Top = 22
    Width = 97
    Height = 27
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = 6710886
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    Text = '0,0000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 4
  end
  object eValorUnitario: TEditLuka
    Left = 4
    Top = 108
    Width = 97
    Height = 27
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = 6710886
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 3
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 2
  end
  object eValorTotal: TEditLuka
    Left = 4
    Top = 165
    Width = 97
    Height = 27
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = 6710886
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 4
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 2
  end
  object stQuantidadeItens: TStaticText
    Left = 551
    Top = 408
    Width = 66
    Height = 22
    Hint = 'Quantidade de itens adicionado no grid'
    Alignment = taRightJustify
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelKind = bkFlat
    Caption = '10 '
    Enabled = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
  end
  object stValorTotal: TStaticText
    Left = 660
    Top = 408
    Width = 121
    Height = 22
    Hint = 'Quantidade de itens adicionado no grid'
    Alignment = taRightJustify
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelKind = bkFlat
    Caption = '9.999,99'
    Enabled = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clTeal
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
  end
  inline FrImagemProduto: TFrImagem
    Left = 4
    Top = 200
    Width = 275
    Height = 230
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 7
    TabStop = True
    ExplicitLeft = 4
    ExplicitTop = 200
    ExplicitWidth = 275
    ExplicitHeight = 230
    inherited pnImagem: TPanel
      Width = 275
      Height = 230
      ExplicitWidth = 275
      ExplicitHeight = 230
      inherited imFoto: TImage
        Width = 271
        Height = 226
        OnClick = FrImagemProdutoimFotoClick
        ExplicitWidth = 156
        ExplicitHeight = 156
      end
    end
    inherited pmOpcoes: TPopupMenu
      Top = 16
      inherited miCarregarfoto: TMenuItem
        Visible = False
      end
      inherited miApagarfoto: TMenuItem
        Visible = False
      end
      object miProximafoto: TMenuItem
        Caption = 'Pr'#243'xima'
        OnClick = miProximafotoClick
      end
      object miAnterior: TMenuItem
        Caption = 'Anterior'
        OnClick = miAnteriorClick
      end
    end
  end
end
