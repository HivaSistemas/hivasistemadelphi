unit Buscar.FechamentoPDV;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.StdCtrls, _Biblioteca,
  EditLuka, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, Vcl.Buttons, Vcl.ExtCtrls,
  _RecordsOrcamentosVendas, BuscarDadosCartoes, _Sessao;

type
  RecFechamento = record
    ValorDinheiro: Double;
    ValorCartao: Double;
    ValorCredito: Double;

    Cartoes: TArray<RecOrcamentosPagamentos>
  end;

  TFormBuscarFechamentoPDV = class(TFormHerancaFinalizar)
    gb1: TGroupBox;
    lb4: TLabel;
    eValorDinheiro: TEditLuka;
    lb1: TLabel;
    eValorCartao: TEditLuka;
    lb2: TLabel;
    eValorCredito: TEditLuka;
    lb19: TLabel;
    eValorSerPago: TEditLuka;
    stPagamento: TStaticText;
    sbBuscarDadosCartoes: TSpeedButton;
    eValorDiferenca: TEditLuka;
    lb3: TLabel;
    procedure sbBuscarDadosCartoesClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure eValorCartaoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eValorCartaoExit(Sender: TObject);
    procedure eValorDinheiroChange(Sender: TObject);
  private
    FNomeCondicao: string;
    FDadosCartoes: TArray<RecOrcamentosPagamentos>;
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Buscar(const pValorSerPago: Double): TRetornoTelaFinalizar<RecFechamento>;

implementation

{$R *.dfm}

function Buscar(const pValorSerPago: Double): TRetornoTelaFinalizar<RecFechamento>;
var
  vForm: TFormBuscarFechamentoPDV;
begin
  vForm := TFormBuscarFechamentoPDV.Create(Application);

  vForm.eValorSerPago.AsDouble := pValorSerPago;

  if Result.Ok(vForm.ShowModal) then begin
    Result.Dados.ValorDinheiro := vForm.eValorDinheiro.AsDouble;
    Result.Dados.ValorCartao   := vForm.eValorCartao.AsDouble;
    Result.Dados.ValorCredito  := vForm.eValorCredito.AsDouble;

    Result.Dados.Cartoes := vForm.FDadosCartoes;
  end;

  vForm.Free;
end;

{ TFormBuscarFechamentoPDV }

procedure TFormBuscarFechamentoPDV.eValorCartaoExit(Sender: TObject);
begin
  inherited;
  if (eValorCartao.AsDouble = 0) and (FDadosCartoes <> nil) then
    _Biblioteca.Destruir(TArray<TObject>(FDadosCartoes));
end;

procedure TFormBuscarFechamentoPDV.eValorCartaoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) and (FDadosCartoes = nil) then
    sbBuscarDadosCartoesClick(nil);

  ProximoCampo(Sender, Key, Shift);
end;

procedure TFormBuscarFechamentoPDV.eValorDinheiroChange(Sender: TObject);
begin
  inherited;
  eValorDiferenca.AsCurr := eValorSerPago.AsCurr - (eValorDinheiro.AsCurr + eValorCartao.AsCurr + eValorCredito.AsCurr);
end;

procedure TFormBuscarFechamentoPDV.Finalizar(Sender: TObject);
begin
  inherited;

end;

procedure TFormBuscarFechamentoPDV.FormCreate(Sender: TObject);
begin
  inherited;
  FNomeCondicao := Sessao.getValorColunaStr('NOME','CONDICOES_PAGAMENTO', 'CONDICAO_ID = :P1', [Sessao.getParametrosEmpresa.CondicaoPagamentoPDV]);
end;

procedure TFormBuscarFechamentoPDV.sbBuscarDadosCartoesClick(Sender: TObject);
begin
  inherited;
  if eValorCartao.AsCurr > 0 then
    FDadosCartoes := BuscarDadosCartoes.BuscarCartoes(Sessao.getParametrosEmpresa.CondicaoPagamentoPDV, FNomeCondicao, eValorCartao.AsDouble, FDadosCartoes);
end;

procedure TFormBuscarFechamentoPDV.VerificarRegistro;
begin
  inherited;

  if eValorDiferenca.AsCurr > 0 then begin
    Exclamar('Existe diferenša nas formas de pagamento informada!');
    SetarFoco(eValorDinheiro);
    Abort;
  end;

end;

end.
