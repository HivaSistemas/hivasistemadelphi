unit PDV;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPrincipal, Vcl.StdCtrls, _Biblioteca, _Sessao,
  EditLuka, Vcl.Grids, GridLuka, Vcl.Buttons, _ProdutosVendaPDV, _RecordsPDV,
  Vcl.Menus, _FrameHerancaPrincipal, FrameImagem, Vcl.ExtCtrls, {_ComunicacaoECF,}
  Buscar.DadosClientePDV, Buscar.Vendedor, Buscar.FechamentoPDV, _RecordsEspeciais, _Orcamentos,
  System.StrUtils, _RecordsOrcamentosVendas, OutrasOperacoes, _ComunicacaoTEF, _OrcamentosPagamentos,
  _NotasFiscais;

type
  TFormPDV = class(TFormHerancaPrincipal)
    sgItens: TGridLuka;
    eCodigoProduto: TEditLuka;
    lb1: TLabel;
    eQuantidade: TEditLuka;
    lb2: TLabel;
    lbNomeProduto: TLabel;
    lb4: TLabel;
    eValorUnitario: TEditLuka;
    lb5: TLabel;
    lb6: TLabel;
    eValorTotal: TEditLuka;
    sbIniciarVenda: TSpeedButton;
    sbDefinirVendedor: TSpeedButton;
    sbPesquisarProduto: TSpeedButton;
    sbDefinirCliente: TSpeedButton;
    sbCancelarItem: TSpeedButton;
    sbCancelarVenda: TSpeedButton;
    sbOutrasOperacoes: TSpeedButton;
    lbUnidade: TLabel;
    sbDefinirQuantidade: TSpeedButton;
    sbFinalizarVenda: TSpeedButton;
    sh1: TShape;
    lb3: TLabel;
    lb7: TLabel;
    stQuantidadeItens: TStaticText;
    lb8: TLabel;
    stValorTotal: TStaticText;
    FrImagemProduto: TFrImagem;
    miProximafoto: TMenuItem;
    miAnterior: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sbIniciarVendaClick(Sender: TObject);
    procedure sbCancelarVendaClick(Sender: TObject);
    procedure eCodigoProdutoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbDefinirQuantidadeClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbDefinirVendedorClick(Sender: TObject);
    procedure sbDefinirClienteClick(Sender: TObject);
    procedure sbPesquisarProdutoClick(Sender: TObject);
    procedure sbCancelarItemClick(Sender: TObject);
    procedure sbOutrasOperacoesClick(Sender: TObject);
    procedure sbFinalizarVendaClick(Sender: TObject);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgItensDblClick(Sender: TObject);
    procedure miProximafotoClick(Sender: TObject);
    procedure miAnteriorClick(Sender: TObject);
    procedure sgItensClick(Sender: TObject);
    procedure FrImagemProdutoimFotoClick(Sender: TObject);
    procedure sgItensKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    FIndexFoto: Integer;
    FVendedorId: Integer;
    FEmModoCancelamento: Boolean;
    FDadosCliente: RecDadosCliente;
  //FItensCupom: TArray<RecProdutoVendaPDV>;

    procedure CalcularTotalItens;
    procedure Habilitar(Sender: TObject; pHabilitar: Boolean);
    procedure ControlarModoCancelamento;
  end;

implementation

{$R *.dfm}

uses BuscarDadosCartoesTEF;

{ TFormPDV }

resourcestring
  stNormal    = 'NORMAL';
  stCancelado = 'CANCELADO';

const
  coProdutoId   = 0;
  coItemId      = 1;
  coNomeProduto = 2;
  coQuantidade  = 3;
  coValorUnit   = 4;
  coUnidade     = 5;
  coValorTotal  = 6;

  (* Colunas ocultas *)
  coStatus      = 7;

procedure TFormPDV.CalcularTotalItens;
var
  i: Integer;
  vQtdeItens: Integer;
  vTotalItens: Double;
begin
  vQtdeItens := 0;
  vTotalItens := 0;
//  for i := Low(FItensCupom) to High(FItensCupom) do begin
//    if FItensCupom[i].Cancelado then
//      Continue;
//
//    Inc(vQtdeItens);
//    vTotalItens := vTotalItens + FItensCupom[i].ValorTotal;
//  end;

  for i := sgItens.FixedRows to sgItens.RowCount - 1 do begin
    Inc(vQtdeItens);
    vTotalItens := vTotalItens + SFormatDouble(sgItens.Cells[coValorTotal, i]);
  end;

  stQuantidadeItens.Caption := NFormatN(vQtdeItens);
  stValorTotal.Caption := NFormatN(vTotalItens);
end;

procedure TFormPDV.ControlarModoCancelamento;
begin
  eValorTotal.Clear;
  eValorUnitario.Clear;
  FrImagemProduto.Clear;
  lbUnidade.Caption := '';
  FEmModoCancelamento := not FEmModoCancelamento;

  _Biblioteca.Habilitar([
    eQuantidade,
    eCodigoProduto,
    eValorUnitario,
    eValorTotal],
    not FEmModoCancelamento
  );

  if FEmModoCancelamento then begin
    lbNomeProduto.Caption := 'MODO CANCELAMENTO DE PRODUTO';
    lbNomeProduto.Font.Color := clRed;

    sbCancelarItem.Caption := 'F6 Sair modo canc.';
    SetarFoco(sgItens);
    sgItensClick(nil);
  end
  else begin
    eQuantidade.AsDouble := 1;
    sbCancelarItem.Caption := 'F6 Canc. item';

    lbNomeProduto.Caption := '';
    lbNomeProduto.Font.Color := clBlack;

    SetarFoco(eCodigoProduto);
  end;
end;

procedure TFormPDV.eCodigoProdutoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  vProduto: TArray<RecProdutoVendaPDV>;

  procedure AddNoGrid;
  var
    vLinha: Integer;
  begin
    vLinha := sgItens.Localizar([coProdutoId], [NFormat(vProduto[0].ProdutoId)]);
    if vLinha = -1 then begin
      if sgItens.Cells[coItemId, 1] = '' then
        vLinha := 1
      else begin
        vLinha := sgItens.RowCount;
        sgItens.RowCount := sgItens.RowCount + 1;
      end;
    end;

    sgItens.Cells[coProdutoId, vLinha]      := NFormat(vProduto[0].ProdutoId);
    sgItens.Cells[coItemId, vLinha]      := NFormat(vLinha);
    sgItens.Cells[coNomeProduto, vLinha] := vProduto[0].NomeProduto;
    sgItens.Cells[coQuantidade, vLinha]  := NFormat(SFormatDouble(sgItens.Cells[coQuantidade, vLinha]) + eQuantidade.AsDouble, 4);
    sgItens.Cells[coValorUnit, vLinha]   := NFormat(vProduto[0].PrecoPDV);
    sgItens.Cells[coUnidade, vLinha]     := vProduto[0].UnidadeVenda;
    sgItens.Cells[coValorTotal, vLinha]  := NFormat(Arredondar(vProduto[0].PrecoPDV * SFormatDouble(sgItens.Cells[coQuantidade, vLinha]), 2));
    sgItens.Cells[coStatus, vLinha]      := stNormal;

//    vProduto[0].Quantidade := eQuantidade.AsDouble;
//    vProduto[0].ValorTotal := Arredondar(vProduto[0].PrecoPDV * eQuantidade.AsDouble, 2);

//    SetLength(FItensCupom, Length(FItensCupom) + 1);
//    FItensCupom[High(FItensCupom)] := vProduto[0];

    sgItens.Row := vLinha;
    sgItensClick(Sender);
  end;

begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  vProduto := _ProdutosVendaPDV.BuscarProduto(Sessao.getConexaoBanco, 0, [Sessao.getEmpresaLogada.EstadoId, eCodigoProduto.AsInt]);
  if vProduto = nil then begin
    Exclamar('Produto n�o encontrado!');
    SetarFoco(eCodigoProduto);
    eCodigoProduto.Clear;
    Exit;
  end;

  if vProduto[0].PrecoPDV = 0 then begin
    Exclamar('Este produto est� com o pre�o de venda r�pida zerado, por favor, corrija antes de utiliz�-lo!');
    SetarFoco(eCodigoProduto);
    eCodigoProduto.Clear;
    Exit;
  end;

//  if
//    not _ComunicacaoECF.VenderItem(
//      vProduto[0].ProdutoId,
//      vProduto[0].NomeProduto,
//      vProduto[0].Cst,
//      vProduto[0].PercentualIcms,
//      vProduto[0].PrecoPDV,
//      eQuantidade.AsDouble,
//      0,
//      0,
//      vProduto[0].UnidadeVenda
//    )
//  then begin
//    Exclamar('Falha ao vender produto!');
//    Exit;
//  end;

  AddNoGrid;
  eQuantidade.AsDouble := 1;

  CalcularTotalItens;
  eCodigoProduto.Clear;
  SetarFoco(eCodigoProduto);
end;

procedure TFormPDV.FormCreate(Sender: TObject);
begin
  inherited;
  Habilitar(Sender, False);
end;

procedure TFormPDV.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;

  case Key of
    VK_F1: begin
      if not sbIniciarVenda.Enabled then
        Exit;

      sbIniciarVendaClick(Sender);
    end;

    VK_F2: begin
      if not sbDefinirVendedor.Enabled then
        Exit;

      sbDefinirVendedorClick(Sender);
    end;

    VK_F3: begin
      if not sbDefinirCliente.Enabled then
        Exit;

      sbDefinirClienteClick(Sender);
    end;

    VK_F4: begin
      if not sbDefinirQuantidade.Enabled then
        Exit;

      sbDefinirQuantidadeClick(Sender);
    end;

    VK_F5: begin
      if not sbPesquisarProduto.Enabled then
        Exit;

      sbPesquisarProdutoClick(Sender);
    end;

    VK_F6: begin
      if not sbCancelarItem.Enabled then
        Exit;

      sbCancelarItemClick(Sender);
    end;

    VK_F7: begin
      if not sbFinalizarVenda.Enabled then
        Exit;

      sbFinalizarVendaClick(Sender);
    end;

    VK_F8: begin
      if not sbCancelarVenda.Enabled then
        Exit;

      sbCancelarVendaClick(Sender);
    end;

    VK_F9: begin
      if not sbOutrasOperacoes.Enabled then
        Exit;

      sbOutrasOperacoesClick(Sender);
    end;
  end;
end;

procedure TFormPDV.FrImagemProdutoimFotoClick(Sender: TObject);
begin
//  inherited;

end;

procedure TFormPDV.Habilitar(Sender: TObject; pHabilitar: Boolean);
begin
  _Biblioteca.Habilitar([
    eQuantidade,
    eCodigoProduto,
    eValorUnitario,
    eValorTotal,
    sgItens,
    sbDefinirVendedor,
    sbPesquisarProduto,
    sbDefinirCliente,
    sbFinalizarVenda,
    sbCancelarVenda,
    sbDefinirQuantidade,
    sbCancelarItem,
    stQuantidadeItens,
    stValorTotal,
    FrImagemProduto],
    pHabilitar
  );

  _Biblioteca.Habilitar([sbIniciarVenda, sbOutrasOperacoes], not pHabilitar);

  //FItensCupom := nil;
  lbUnidade.Caption := '';
  eQuantidade.AsDouble := 1;

  if Sender <> sbIniciarVenda then begin
    FVendedorId := 0;
    FDadosCliente.NomeCliente := '';
    FDadosCliente.Cpf         := '';
    FDadosCliente.Endereco    := '';
  end;

  if pHabilitar then begin
    Self.BorderIcons := [];
    SetarFoco(eCodigoProduto);
    lbNomeProduto.Caption := '';
    SetForegroundWindow(Application.Handle);
  end
  else begin
    FDadosCliente.ClienteId := Sessao.getParametros.cadastro_consumidor_final_id;
    Self.BorderIcons := [biSystemMenu];
    lbNomeProduto.Caption := 'Caixa aberto';
  end;
end;

procedure TFormPDV.miAnteriorClick(Sender: TObject);
begin
  inherited;
  Dec(FIndexFoto);
  if FIndexFoto < 1 then begin
    FIndexFoto := 1;
    Exit;
  end;

//  case FIndexFoto of
//    2: FrImagemProduto.setFoto(FItensCupom[sgItens.Row - 1].Foto2);
//    1: FrImagemProduto.setFoto(FItensCupom[sgItens.Row - 1].Foto1);
//  end;
end;

procedure TFormPDV.miProximafotoClick(Sender: TObject);
begin
  inherited;
  Inc(FIndexFoto);
  if FIndexFoto > 3 then begin
    FIndexFoto := 3;
    Exit;
  end;

//  case FIndexFoto of
//    2: begin
//      if FItensCupom[sgItens.Row - 1].Foto2 = nil then begin
//        FIndexFoto := 1;
//        Exit;
//      end;
//
//      FrImagemProduto.setFoto(FItensCupom[sgItens.Row - 1].Foto2);
//    end;
//
//    3: begin
//      if FItensCupom[sgItens.Row - 1].Foto3 = nil then begin
//        FIndexFoto := 2;
//        Exit;
//      end;
//
//      FrImagemProduto.setFoto(FItensCupom[sgItens.Row - 1].Foto3);
//    end;
//  end;
end;

procedure TFormPDV.sbCancelarVendaClick(Sender: TObject);
begin
  inherited;
  if not Perguntar('Deseja realmente cancelar esta venda?') then
    Exit;

  //_ComunicacaoECF.CancelarCupomFiscal;
  Habilitar(Sender, False);
end;

procedure TFormPDV.sbDefinirClienteClick(Sender: TObject);
var
  vDados: TRetornoTelaFinalizar<RecDadosCliente>;
begin
  inherited;
  vDados := Buscar.DadosClientePDV.Buscar(FDadosCliente);
  if vDados.RetTela = trCancelado then
    RotinaCanceladaUsuario
  else
    FDadosCliente := vDados.Dados;
end;

procedure TFormPDV.sbDefinirQuantidadeClick(Sender: TObject);
begin
  inherited;
  SetarFoco(eQuantidade);
  eQuantidade.AsDouble := 1;
end;

procedure TFormPDV.sbDefinirVendedorClick(Sender: TObject);
var
  vDados: TRetornoTelaFinalizar<Integer>;
begin
  inherited;
  vDados := Buscar.Vendedor.Buscar(FVendedorId);
  if vDados.RetTela = trCancelado then begin
    RotinaCanceladaUsuario;
    Exit;
  end;
  FVendedorId := vDados.Dados;
end;

procedure TFormPDV.sbFinalizarVendaClick(Sender: TObject);
var
  i: Integer;
  vTemProduto: Boolean;

  vTemCartaoTEF: Boolean;
  vTemCartaoPOS: Boolean;
  vRetProcessamentoTEF: RecRespostaTEF;

  vRetBanco: RecRespostaBanco;
  //vRetECF: RecRetornoImpressora;
  vItens: TArray<RecOrcamentoItens>;
  //vDadosFormasPagamento: RecDadosFormaPagamento;
  vRetornoCartoes: TRetornoTelaFinalizar<RecCartoes>;
  vDadosFechamento: TRetornoTelaFinalizar<RecFechamento>;

  procedure GerarErro(pMensagem: string);
  begin
    BlockInput(False);
    _Biblioteca.Exclamar(pMensagem);
    //_ComunicacaoECF.CancelarCupomFiscal;
    //Habilitar(Sender, False);
    Abort;
  end;

begin
  inherited;
  vItens := nil;
  vTemProduto := False;
  for i := sgItens.FixedRows to sgItens.RowCount -1 do begin
    if sgItens.Cells[coProdutoId, i] = '' then
      Exit;

    vTemProduto := True;

    SetLength(vItens, Length(vItens) + 1);
    vItens[i - 1].produto_id                  := SFormatInt(sgItens.Cells[coProdutoId, i]);
    vItens[i - 1].item_id                     := i;
    vItens[i - 1].quantidade                  := SFormatDouble(sgItens.Cells[coQuantidade, i]);
    vItens[i - 1].QuantidadeRetirarAto        := vItens[i - 1].quantidade;
    vItens[i - 1].preco_unitario              := SFormatDouble(sgItens.Cells[coValorUnit, i]);
    vItens[i - 1].valor_total_desconto        := 0;
    vItens[i - 1].valor_total                 := SFormatDouble(sgItens.Cells[coValorTotal, i]);
    vItens[i - 1].valor_total_outras_despesas := 0;
    vItens[i - 1].utilizou_preco_promocao     := 'N';
  end;

  if not vTemProduto then begin
    Exclamar('Nenhum produto foi adicionado na venda!');
    SetarFoco(eCodigoProduto);
    Exit;
  end;

  vDadosFechamento := Buscar.FechamentoPDV.Buscar(SFormatDouble(stValorTotal.Caption));
  if vDadosFechamento.RetTela = trCancelado then begin
    RotinaCanceladaUsuario;
    Exit;
  end;

  vTemCartaoTEF := False;
  vTemCartaoPOS := False;
  if vDadosFechamento.Dados.ValorCartao > 0 then begin
    for i := Low(vDadosFechamento.Dados.Cartoes) to High(vDadosFechamento.Dados.Cartoes) do begin
      if vDadosFechamento.Dados.Cartoes[i].tipo_recebimento_cartao = 'T' then
        vTemCartaoTEF := True
      else if vDadosFechamento.Dados.Cartoes[i].tipo_recebimento_cartao = 'P' then
        vTemCartaoPOS := True;
    end;

    if vTemCartaoTEF then
      vRetornoCartoes := BuscarDadosCartoesTEF.BuscarRespostaTEF(vDadosFechamento.Dados.Cartoes)
    else if vTemCartaoPOS then begin

    end;

    if vRetornoCartoes.RetTela = trCancelado then begin
      RotinaCanceladaUsuario;
      Exit;
    end;
  end;

//  if not _ComunicacaoECF.IniciarFechamentoCupomFiscal(0, 0) then
//    GerarErro('Falha no fechamento do cupom fiscal');
//
//  vRetECF := _ComunicacaoECF.getRetornoImpressora;
//  if vRetECF.teve_erro then
//    GerarErro('Falha ao buscar informa��es do ECF!');

//  vDadosFormasPagamento.nome_forma_dinheiro := Sessao.getImpressora.nome_forma_pagamento_dinheiro;
//  vDadosFormasPagamento.nome_forma_cartao   := Sessao.getImpressora.nome_forma_pagamento_cartao;
//  vDadosFormasPagamento.nome_forma_cobranca := Sessao.getImpressora.nome_forma_pagamento_cobranca;
//  vDadosFormasPagamento.nome_forma_cheque   := Sessao.getImpressora.nome_forma_pagamento_cheque;
//  vDadosFormasPagamento.nome_forma_pos      := Sessao.getImpressora.nome_forma_pagamento_pos;
//  vDadosFormasPagamento.nome_forma_credito  := Sessao.getImpressora.nome_forma_pagamento_credito;

//  if
//    not _ComunicacaoECF.EfetuarFormasPagamento(
//      vDadosFechamento.Dados.ValorDinheiro,
//      vDadosFechamento.Dados.ValorCartao,
//      0,
//      0,
//      vDadosFechamento.Dados.ValorCredito,
//      0,
//      vDadosFormasPagamento
//    )
//  then
//    GerarErro('Falha na totaliza��o do cupom fiscal!');
//
//  if not _ComunicacaoECF.TerminarFechamentoCupomFiscal() then
//    GerarErro('Falha no t�rmino do fechamento do cupom fiscal!');

  vRetBanco :=
    _Orcamentos.AtualizarOrcamentoPDV(
      Sessao.getConexaoBanco,
      IfThen(FDadosCliente.ClienteId = Sessao.getParametros.cadastro_consumidor_final_id, FDadosCliente.NomeCliente, ''),
      '',
      Sessao.getEmpresaLogada.EmpresaId,
      FDadosCliente.ClienteId,
      Sessao.getParametrosEmpresa.CondicaoPagamentoPDV, // Mudar
      FVendedorId,
      SFormatDouble(stValorTotal.Caption),
      SFormatDouble(stValorTotal.Caption), // Valor total
      vDadosFechamento.Dados.ValorDinheiro,
      vDadosFechamento.Dados.ValorCartao,
      Sessao.getTurnoCaixaAberto.TurnoId,
      Sessao.getUsuarioLogado.caixa_id,
      Sessao.getTurnoCaixaAberto.FuncionarioId,
      vItens
    );

  if vRetBanco.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    GerarErro('Falha ao gravar venda no banco de dados!' + Chr(13) + Chr(10) + vRetBanco.MensagemErro);
  end;

//  vRetBanco :=
//    _Orcamentos.AtualizarOrcamentosPagamentos(
//      Sessao.getConexaoBanco,
//      vOrcamentoId,
//      nil,
//      vDadosFechamento.Dados.Cartoes,
//      nil,
//      nil
//    );
//
//  if vRetBanco.TeveErro then begin
//    Sessao.getConexaoBanco.VoltarTransacao;
//    GerarErro('Falha ao gravar as cobran�as no banco de dados!' + Chr(13) + Chr(10) + vRetBanco.MensagemErro);
//  end;

//  for i := Low(vRetornoCartoes.Dados.Cartoes) to High(vRetornoCartoes.Dados.Cartoes) do begin
//    vRetBanco :=
//      _Orcamentos.GerarCartoesReceber(
//        Sessao.getConexaoBanco,
//        vOrcamentoId,
//        i + 1,
//        Sessao.getTurnoCaixaAberto.TurnoId,
//        Sessao.getUsuarioLogado.caixa_id,
//        Sessao.getTurnoCaixaAberto.FuncionarioId,
//        vRetornoCartoes.Dados.Cartoes[i].Nsu,
//        vRetornoCartoes.Dados.Cartoes[i].CodigoAutorizacaoTef
//      );
//
//    if vRetBanco.TeveErro then begin
//      Sessao.getConexaoBanco.VoltarTransacao;
//      GerarErro(vRetBanco.MensagemErro);
//    end;
//
//    vRetBanco :=
//      _OrcamentosPagamentos.AtualizarOrcamentosPagamentos(
//        Sessao.getConexaoBanco,
//        vOrcamentoId,
//        vRetornoCartoes.Dados.Cartoes[i].CobrancaId,
//        i + 1,
//        vRetornoCartoes.Dados.Cartoes[i].Valor,
//        'CR',
//        vRetornoCartoes.Dados.Cartoes[i].nsu
//      );
//
//    if vRetBanco.TeveErro then begin
//      Sessao.getConexaoBanco.VoltarTransacao;
//      GerarErro(vRetBanco.MensagemErro);
//    end;
//  end;

//  vRetBanco := _NotasFiscais.SetarCupomImpresso(Sessao.getConexaoBanco, Sessao.getValorColuna('NOTA_FISCAL_ID', 'NOTAS_FISCAIS', 'ORCAMENTO_ID = :P1', [vOrcamentoId]), vRetECF.numero_cupom);
//  if vRetBanco.TeveErro then begin
//    Sessao.getConexaoBanco.VoltarTransacao;
//    GerarErro(vRetBanco.MensagemErro);
//  end;

  // Imprimir as vias dos cart�es
  if vTemCartaoTEF then begin
//    if
//      not _ComunicacaoECF.ImprimirComprovanteTEF(
//        'CRT',
//        vRetECF.numero_cupom,
//        vDadosFechamento.Dados.ValorCartao,
//        BuscarDadosCartoesTEF.getTextoComprovante(vRetornoCartoes.Dados.Cartoes),
//        nil,
//        0
//      )
//    then begin
//      _Biblioteca.Exclamar('Falha ao imprimir comprovante TEF!' + chr(13) + Chr(10) + 'Reemita pela tela de outras opera��es.');
//      Habilitar(Sender, False);
//      Exit;
//    end;

    if not vRetornoCartoes.Dados.Cartoes[vRetornoCartoes.Dados.IndexUltimoCartao].Confirmado then begin
      vRetProcessamentoTEF :=
        _ComunicacaoTEF.EnviarConfirmacao(
          _ComunicacaoTEF.getId001,
          IntToStr(vRetornoCartoes.Dados.Cartoes[vRetornoCartoes.Dados.IndexUltimoCartao].id_tef),
          0,
          vRetornoCartoes.Dados.Cartoes[vRetornoCartoes.Dados.IndexUltimoCartao].rede,
          vRetornoCartoes.Dados.Cartoes[vRetornoCartoes.Dados.IndexUltimoCartao].nsu,
          vRetornoCartoes.Dados.Cartoes[vRetornoCartoes.Dados.IndexUltimoCartao].finalizacao
        );

      if not vRetProcessamentoTEF.Confirmado then begin
        CancelarCartoes(vRetornoCartoes.Dados.Cartoes);
        Exclamar('Falha ao confirmar o �ltimo cart�o TEF!');
      end;
    end;
  end;

  Habilitar(Sender, False);
end;

procedure TFormPDV.sbCancelarItemClick(Sender: TObject);
begin
  inherited;
  // if not xxx_paramentro then begin
  // Exit;

  ControlarModoCancelamento;
end;

procedure TFormPDV.sbIniciarVendaClick(Sender: TObject);
var
  vDadosCliente: TRetornoTelaFinalizar<RecDadosCliente>;
begin
  inherited;
  vDadosCliente.Dados.ClienteId := Sessao.getParametros.cadastro_consumidor_final_id;

  //if xxx.parametros then begin
    vDadosCliente := Buscar.DadosClientePDV.Buscar(FDadosCliente);
    if vDadosCliente.RetTela = trCancelado then begin
      RotinaCanceladaUsuario;
      Exit;
    end;

    FDadosCliente := vDadosCliente.Dados;

//  end;

//  if
//    not _ComunicacaoECF.AbrirCupomFiscal(
//      vDadosCliente.Dados.Cpf,
//      NFormatN(vDadosCliente.Dados.ClienteId) + ' - ' + vDadosCliente.Dados.NomeCliente,
//      vDadosCliente.Dados.Endereco
//    )
//  then
//    Exit;

  Habilitar(Sender, True);
end;

procedure TFormPDV.sbOutrasOperacoesClick(Sender: TObject);
begin
  inherited;
  Sessao.AbrirTela(TFormOutrasOperacoes);
end;

procedure TFormPDV.sbPesquisarProdutoClick(Sender: TObject);
begin
  inherited;
  //
end;

procedure TFormPDV.sgItensClick(Sender: TObject);
begin
  inherited;
//  if sgItens.Cells[coItemId, 1] = '' then
//    Exit;
//
//  lbNomeProduto.Caption   := NFormat(FItensCupom[sgItens.Row - 1].ProdutoId) + ' - ' + FItensCupom[sgItens.Row - 1].NomeProduto;
//  eValorUnitario.AsDouble := FItensCupom[sgItens.Row - 1].PrecoPDV;
//  lbUnidade.Caption       := FItensCupom[sgItens.Row - 1].UnidadeVenda;
//  eValorTotal.AsDouble    := FItensCupom[sgItens.Row - 1].ValorTotal;
//
//  FrImagemProduto.Clear;
//
//  if FItensCupom[sgItens.Row - 1].Foto1 = nil then
//    Exit;
//
//  FIndexFoto := 1;
//  FrImagemProduto.setFoto(FItensCupom[sgItens.Row - 1].Foto1);
end;

procedure TFormPDV.sgItensDblClick(Sender: TObject);
begin
  inherited;
  if not FEmModoCancelamento then
    Exit;

  if sgItens.Cells[coItemId, 1] = '' then
    Exit;

  if not Perguntar('Deseja realmente cancelar este item?') then
    Exit;

//  if not _ComunicacaoECF.CancelarItem(sgItens.Row) then begin
//    Exclamar('Falha ao cancelar o item da venda!');
//    Exit;
//  end;

 //ItensCupom[sgItens.Row - 1].Cancelado := True;

  sgItens.DeleteRow(sgItens.Row);
  sgItens.Repaint;

  CalcularTotalItens;
  ControlarModoCancelamento;
end;

procedure TFormPDV.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coProdutoId, coItemId, coQuantidade, coValorUnit, coValorTotal] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormPDV.sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgItens.Cells[coStatus, ARow] = stCancelado then begin
    AFont.Color  := clRed;
    ABrush.Color := $00F4F4FF;
  end;
end;

procedure TFormPDV.sgItensKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    sgItensDblClick(Sender);
end;

end.
