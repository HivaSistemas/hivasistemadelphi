inherited FormBuscarDadosCliente: TFormBuscarDadosCliente
  Caption = 'Buscando dados do cliente'
  ClientHeight = 196
  ClientWidth = 422
  OnShow = FormShow
  ExplicitWidth = 428
  ExplicitHeight = 225
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 156
    Top = 69
    Width = 32
    Height = 14
    Caption = 'Nome'
  end
  object lbCPF_CNPJ: TLabel [1]
    Left = 5
    Top = 69
    Width = 20
    Height = 14
    Caption = 'CPF'
  end
  object lb2: TLabel [2]
    Left = 5
    Top = 112
    Width = 52
    Height = 14
    Caption = 'Endere'#231'o'
  end
  inherited pnOpcoes: TPanel
    Top = 159
    Width = 422
    TabOrder = 4
    ExplicitTop = 159
    ExplicitWidth = 422
  end
  inline FrCliente: TFrClientes
    Left = 5
    Top = 3
    Width = 410
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 0
    TabStop = True
    ExplicitLeft = 5
    ExplicitTop = 3
    ExplicitWidth = 410
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 385
      Height = 23
      ExplicitWidth = 385
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 410
      ExplicitWidth = 410
      inherited lbNomePesquisa: TLabel
        Width = 162
        Height = 15
        Caption = 'Cliente cadastrado no sistema'
        ExplicitWidth = 162
      end
    end
    inherited pnPesquisa: TPanel
      Left = 385
      Height = 24
      ExplicitLeft = 385
      ExplicitHeight = 24
    end
  end
  object eNome: TEditLuka
    Left = 156
    Top = 84
    Width = 259
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 32
    TabOrder = 2
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
  end
  object stFormasPagamento: TStaticText
    Left = 5
    Top = 50
    Width = 410
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Dados do cliente n'#227'o cadastrado'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 5
    Transparent = False
  end
  object eCPF: TEditCPF_CNPJ_Luka
    Left = 5
    Top = 84
    Width = 128
    Height = 22
    EditMask = '999.999.999-99'
    MaxLength = 14
    TabOrder = 1
    Text = '   .   .   -  '
    OnKeyDown = ProximoCampo
    Tipo = [tccCPF]
  end
  object eEndereco: TEditLuka
    Left = 5
    Top = 127
    Width = 410
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 32
    TabOrder = 3
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
  end
end
