unit MenuPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _Sessao, Vcl.Menus, Vcl.ComCtrls, _Biblioteca, System.DateUtils,
  PDV, Vcl.Buttons, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Imaging.jpeg;

type
  TFormMenuPrincipal = class(TForm)
    mmPrincipal: TMainMenu;
    miCaixasBancos1: TMenuItem;
    miRotinasdocaixa1: TMenuItem;
    miAbertura: TMenuItem;
    miSuprimento: TMenuItem;
    miSangria: TMenuItem;
    miFechamento: TMenuItem;
    miOutrasOperacoes: TMenuItem;
    miPDV: TMenuItem;
    statSistema: TStatusBar;
    sh2: TShape;
    sbOrcamentosVendas: TSpeedButton;
    lbPDV: TLabel;
    imMenuPrincipal: TImage;
    miNotasFiscaisPendentes: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure miPDVClick(Sender: TObject);
    procedure miAberturaClick(Sender: TObject);
    procedure miSuprimentoClick(Sender: TObject);
    procedure miSangriaClick(Sender: TObject);
    procedure miFechamentoClick(Sender: TObject);
    procedure miOutrasOperacoesClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbPDVMouseEnter(Sender: TObject);
    procedure lbPDVMouseLeave(Sender: TObject);
    procedure lbPDVClick(Sender: TObject);
  end;

var
  FormMenuPrincipal: TFormMenuPrincipal;

implementation

{$R *.dfm}

uses
  AberturaCaixa, SuprimentoCaixa, SangriaCaixa, FechamentoCaixa, OutrasOperacoes;

procedure TFormMenuPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  _Sessao.Sessao.Finalizar;
end;

procedure TFormMenuPrincipal.FormCreate(Sender: TObject);
var
  vData: TDateTime;
begin
  vData := Sessao.getDataHora;
  statSistema.Panels[0].Text := '   Usu�rio: ' + IntToStr(Sessao.getUsuarioLogado.funcionario_id) + ' - ' + Sessao.getUsuarioLogado.nome;
  statSistema.Panels[1].Text := '   Empresa: ' + IntToStr(Sessao.getEmpresaLogada.EmpresaId) + ' - ' + Sessao.getEmpresaLogada.NomeFantasia;
  statSistema.Panels[2].Text := '   Data: ' + _Biblioteca.DiaDaSemana(vData) + ', ' + NFormat(DayOfTheMonth(vData)) + ' de ' + _Biblioteca.MesDoAno(vData) + ' de ' + NFormat(YearOf(vData));

  Caption := Application.Title + ' - ' + Caption;
end;

procedure TFormMenuPrincipal.FormShow(Sender: TObject);
begin
  SetForegroundWindow(Application.Handle);
end;

procedure TFormMenuPrincipal.lbPDVClick(Sender: TObject);
begin
  miPDVClick(Sender);
end;

procedure TFormMenuPrincipal.lbPDVMouseEnter(Sender: TObject);
begin
  TControl(Sender).Cursor := crHandPoint;
end;

procedure TFormMenuPrincipal.lbPDVMouseLeave(Sender: TObject);
begin
  TControl(Sender).Cursor := crDefault;
end;

procedure TFormMenuPrincipal.miAberturaClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormAberturaCaixa);
end;

procedure TFormMenuPrincipal.miFechamentoClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormFechamentoCaixa);
end;

procedure TFormMenuPrincipal.miOutrasOperacoesClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormOutrasOperacoes);
end;

procedure TFormMenuPrincipal.miPDVClick(Sender: TObject);
var
  vPDV: TFormPDV;
begin
  if Sessao.getTurnoCaixaAberto = nil then begin
    Exclamar('N�o existe um turno em aberto para o usu�rio logado, por favor, fa�a a abertura de turno antes de realizar est� opera��o!');
    Exit;
  end;

  if Sessao.getParametrosEmpresa.CondicaoPagamentoPDV = 0 then begin
    Exclamar('N�o foi parametrizado a condi��o de pagamento do PDV, verifique!');
    Exit;
  end;

  vPDV := TFormPDV.Create(Application);
  vPDV.ShowModal;
  vPDV.Free;
end;

procedure TFormMenuPrincipal.miSangriaClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormSangriaCaixa);
end;

procedure TFormMenuPrincipal.miSuprimentoClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormSuprimentoCaixa);
end;

end.
