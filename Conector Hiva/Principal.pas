unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, EditLuka, Vcl.StdCtrls, _Criptografia,
  CheckBoxLuka;

type
  TFormPrincipal = class(TForm)
    eUsuario: TEdit;
    Label1: TLabel;
    eSenha: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    ePorta: TEditLuka;
    Label4: TLabel;
    eServico: TEdit;
    Label5: TLabel;
    eChaveGerada: TEdit;
    sbGerarChave: TButton;
    eServidor: TEdit;
    Label6: TLabel;
    ckbDireto: TCheckBoxLuka;
    procedure eUsuarioKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure sbGerarChaveClick(Sender: TObject);
  private
    FCriptografia: TCriptografia;
  public
    { Public declarations }
  end;

var
  FormPrincipal: TFormPrincipal;

implementation

{$R *.dfm}

procedure TFormPrincipal.eUsuarioKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    SelectNext(ActiveControl, True, True);
end;

procedure TFormPrincipal.FormCreate(Sender: TObject);
begin
  FCriptografia := TCriptografia.Create(Self);
  ePorta.AsInt := 1521;
end;

procedure TFormPrincipal.sbGerarChaveClick(Sender: TObject);
var
  vTexto: string;
  vTeste: string;
begin
  eUsuario.Text := Trim(eUsuario.Text);
  if eUsuario.Text = '' then begin
    ShowMessage('O usu�rio do banco de dados n�o foi informado!');
    eUsuario.SetFocus;
    Exit;
  end;

  eSenha.Text := Trim(eSenha.Text);
  if eSenha.Text = '' then begin
    ShowMessage('A senha do banco de dados n�o foi informado!');
    eSenha.SetFocus;
    Exit;
  end;

  eServidor.Text := Trim(eServidor.Text);
  if eServidor.Text = '' then begin
    ShowMessage('O endere�o do servidor do banco de dados n�o foi informado!');
    eServidor.SetFocus;
    Exit;
  end;

  ePorta.Text := Trim(ePorta.Text);
  if ePorta.Text = '' then begin
    ShowMessage('A porta do banco de dados n�o foi informado!');
    ePorta.SetFocus;
    Exit;
  end;

  eServico.Text := Trim(eServico.Text);
  if ePorta.Text = '' then begin
    ShowMessage('O nome do servi�o do banco de dados n�o foi informado!');
    eServico.SetFocus;
    Exit;
  end;

  vTexto := eUsuario.Text + '|' + eSenha.Text + '|' + eServidor.Text + '|' + ePorta.Text + '|' + eServico.Text +'|'+ ckbDireto.CheckedStr;
  eChaveGerada.Text := FCriptografia.Criptografar(vTexto, False);

  vTeste := FCriptografia.Descriptografar(eChaveGerada.Text, False);
end;

end.
