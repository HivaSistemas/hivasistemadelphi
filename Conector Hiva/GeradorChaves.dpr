program GeradorChaves;

uses
  Vcl.Forms,
  Principal in 'Principal.pas' {FormPrincipal},
  _Criptografia in '..\Piloto\Repositório\_Criptografia.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFormPrincipal, FormPrincipal);
  Application.Run;
end.
