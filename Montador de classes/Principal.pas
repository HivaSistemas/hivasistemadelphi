unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Grids, System.StrUtils, Ora,
  Vcl.ComCtrls, Vcl.ExtCtrls, GridLuka, _BibliotecaGenerica, Vcl.Buttons;

type
  TFormPrincipal = class(TForm)
    lb1: TLabel;
    meClasse: TMemo;
    btnMontar: TButton;
    eNomeClasse: TEdit;
    btnSalvarClasse: TButton;
    lb2: TLabel;
    sbAdicionarCampos: TButton;
    ckChaves: TCheckBox;
    eNomeCampo: TEdit;
    lb3: TLabel;
    lb4: TLabel;
    lb5: TLabel;
    lb7: TLabel;
    eCodigoTipoCampo: TEdit;
    lb6: TLabel;
    ckSomenteLeitura: TCheckBox;
    sbGerarRecord: TButton;
    lb8: TLabel;
    cbbTabelas: TComboBox;
    lb9: TLabel;
    sgCamposTabela: TGridLuka;
    sbSubirCampo: TSpeedButton;
    sbDescerCampo: TSpeedButton;
    procedure btnMontarClick(Sender: TObject);
    procedure btnSalvarClasseClick(Sender: TObject);
    procedure eNomeCampoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ckChavesKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbAdicionarCamposClick(Sender: TObject);
    procedure eCodigoTipoCampoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eNomeClasseKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ckSomenteLeituraKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure lvCamposTabelaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbGerarRecordClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbbTabelasChange(Sender: TObject);
    procedure lvCamposTabelaDblClick(Sender: TObject);
    procedure sgCamposTabelaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sbSubirCampoClick(Sender: TObject);
    procedure sbDescerCampoClick(Sender: TObject);
    procedure sgCamposTabelaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgCamposTabelaDblClick(Sender: TObject);
  private
    FConexao: TOraSession;
    FQuery: TOraQuery;
    FLinha: Integer;

    function TratarCampoParametro(pCampo: string): string;
  public
    { Public declarations }
  end;

var
  FormPrincipal: TFormPrincipal;

implementation

{$R *.dfm}

const
  co_nome_campo      = 0;
  co_tipo            = 1;
  co_chave           = 2;
  co_somente_leitura = 3;

procedure TFormPrincipal.btnMontarClick(Sender: TObject);
var
  i: Integer;

  procedure Adicionar(pAdicao: string);
  begin
    meClasse.Lines.Add(pAdicao);
  end;

begin
  meClasse.Lines.Clear;

  if eNomeClasse.Text = '' then begin
    ShowMessage('Informe o nome da classe!');
    eNomeClasse.SetFocus;
    Exit;
  end;

  Adicionar('unit _' + eNomeClasse.Text + ';');
  Adicionar('');
  Adicionar('Interface');
  Adicionar('');
  Adicionar('uses');
  Adicionar('  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;');
  Adicionar('');
  Adicionar('{$M+}');
  Adicionar('type');

  Adicionar('  Rec' + eNomeClasse.Text + ' = record');

  for i := 1 to sgCamposTabela.RowCount -1 do
    Adicionar('    ' + TratarCampoParametro(sgCamposTabela.Cells[co_nome_campo, i]) + ': ' + sgCamposTabela.Cells[co_tipo, i] + ';');

  Adicionar('  end;');
  Adicionar('');

  Adicionar('  T' + eNomeClasse.Text + ' = class(TOperacoes)' );
  Adicionar('  public');
  Adicionar('    constructor Create(pConexao: TConexao);');
  Adicionar('  protected');
  Adicionar('    function getRecord' + eNomeClasse.Text + ': Rec' + eNomeClasse.Text + ';');
  Adicionar('  end;');
  Adicionar('');

  Adicionar('function Atualizar' + eNomeClasse.Text + '(');
  Adicionar('  pConexao: TConexao;');
  for i := 1 to sgCamposTabela.RowCount -1 do begin
    if sgCamposTabela.Cells[co_somente_leitura, i] = 'N�o' then
      Adicionar('  p' + TratarCampoParametro(sgCamposTabela.Cells[co_nome_campo, i]) + ': ' + sgCamposTabela.Cells[co_tipo, i] + IfThen(i < sgCamposTabela.RowCount -1, ';'));
  end;
  Adicionar('): RecRetornoBD;');
  Adicionar('');

  Adicionar('function Buscar' + eNomeClasse.Text + '(');
  Adicionar('  pConexao: TConexao;');
  Adicionar('  pIndice: ShortInt;');
  Adicionar('  pFiltros: array of Variant');
  Adicionar('): TArray<Rec' + eNomeClasse.Text + '>;');
  Adicionar('');

  Adicionar('function Excluir' + eNomeClasse.Text + '(');
  Adicionar('  pConexao: TConexao;');
  for i := 1 to sgCamposTabela.RowCount -1 do begin
    if sgCamposTabela.Cells[co_chave, i] = 'Sim' then
      Adicionar('  p' + TratarCampoParametro(sgCamposTabela.Cells[co_nome_campo, i]) + ': ' + sgCamposTabela.Cells[co_tipo, i] + IfThen(i < sgCamposTabela.RowCount -1, ';'));
  end;
  Adicionar('): RecRetornoBD;');
  Adicionar('');

  Adicionar('function getFiltros: TArray<RecFiltros>;');
  Adicionar('');

  Adicionar('implementation');
  Adicionar('');

  Adicionar('{ T' + eNomeClasse.Text +' }');
  Adicionar('');

  Adicionar('function getFiltros: TArray<RecFiltros>;');
  Adicionar('begin');
  Adicionar('  SetLength(Result, 1);');
  Adicionar('');
  Adicionar('  Result[0] :=');
  Adicionar('    _OperacoesBancoDados.NovoRecFiltro(');
  Adicionar('      '''',');
  Adicionar('      False,');
  Adicionar('      0,');
  Adicionar('      ''where XXX = :P1''');
  Adicionar('    );');
  Adicionar('end;');
  Adicionar('');

  Adicionar('constructor T' + eNomeClasse.Text + '.Create(pConexao: TConexao);');
  Adicionar('begin');
  Adicionar('  inherited Create(pConexao, '''+ cbbTabelas.Text + ''');');
  Adicionar('');
  Adicionar('  FSql := ');
  Adicionar('    ''select '' +');

  for i := 1 to sgCamposTabela.RowCount -1 do
    Adicionar('    ''  ' + sgCamposTabela.Cells[co_nome_campo, i] + ', '' +');

  Adicionar('    ''from '' +' );
  Adicionar('    ''  ' + cbbTabelas.Text + ''';');
  Adicionar('');
  Adicionar('  setFiltros(getFiltros);');
  Adicionar('');

  for i := 1 to sgCamposTabela.RowCount -1 do begin
    if sgCamposTabela.Cells[co_somente_leitura, i] = 'Sim' then
      Adicionar('  AddColunaSL(''' + sgCamposTabela.Cells[co_nome_campo, i] + '''' + IfThen(sgCamposTabela.Cells[co_chave, i] = 'Sim', ', True') + ');')
    else
      Adicionar('  AddColuna(''' + sgCamposTabela.Cells[co_nome_campo, i] + '''' + IfThen(sgCamposTabela.Cells[co_chave, i] = 'Sim', ', True') + ');');
  end;

  Adicionar('end;');
  Adicionar('');

  Adicionar('function T' + eNomeClasse.Text + '.getRecord' + eNomeClasse.Text + ': Rec' + eNomeClasse.Text + ';');
  Adicionar('begin');
  for i := 1 to sgCamposTabela.RowCount -1 do begin
    Adicionar(
      '  Result.' + _BibliotecaGenerica.RPad(TratarCampoParametro(sgCamposTabela.Cells[co_nome_campo, i]), 27, ' ') +  ':= get' + Decode(
        sgCamposTabela.Cells[co_tipo, i],
        [
          'Integer',   'Int',
          'Double',    'Double',
          'TDateTime', 'Data',
          'string',    'String'
        ]
      ) +
    '(''' + UpperCase(sgCamposTabela.Cells[co_nome_campo, i]) + '''' + IfThen(sgCamposTabela.Cells[co_chave, i] = 'Sim', ', True') + ');'
    );
  end;

  Adicionar('end;');
  Adicionar('');

  Adicionar('function Atualizar' + eNomeClasse.Text + '(');
  Adicionar('  pConexao: TConexao;');
  for i := 1 to sgCamposTabela.RowCount -1 do begin
    if sgCamposTabela.Cells[co_somente_leitura, i] = 'N�o' then
      Adicionar('  p' + TratarCampoParametro(sgCamposTabela.Cells[co_nome_campo, i]) + ': ' + sgCamposTabela.Cells[co_tipo, i] + IfThen(i < sgCamposTabela.RowCount -1, ';'));
  end;
  Adicionar('): RecRetornoBD;');
  Adicionar('var');
  Adicionar('  t: T' + eNomeClasse.Text + ';');
  Adicionar('  vNovo: Boolean;');
  Adicionar('  vSeq: TSequencia;');
  Adicionar('begin');
  Adicionar('  Result.Iniciar;');
  Adicionar('  pConexao.setRotina(''XXXXXXXXXXXXXXXXXXXX'');');
  Adicionar('');
  Adicionar('  t := T' + eNomeClasse.Text + '.Create(pConexao);');
  Adicionar('');
  Adicionar('  vNovo := pXXX = 0;');
  Adicionar('');
  Adicionar('  if vNovo then begin');
  Adicionar('    vSeq := TSequencia.Create(pConexao, ''SEQ_XXX'');');
  Adicionar('    pXXX := vSeq.getProximaSequencia;');
  Adicionar('    Result.AsInt := pXXX;');
  Adicionar('    vSeq.Free;');
  Adicionar('  end;');
  Adicionar('');
  Adicionar('  try');
  Adicionar('    pConexao.IniciarTransacao; ');
  Adicionar('');
  for i := 1 to sgCamposTabela.RowCount -1 do begin
    if sgCamposTabela.Cells[co_somente_leitura, i] = 'N�o' then begin
      Adicionar(
        '    t.set' +
        + Decode(
          sgCamposTabela.Cells[co_tipo, i],
          [
            'Integer',   'Int',
            'Double',    'Double',
            'TDateTime', 'Data',
            'string',    'String'
          ]
        ) +
        '(''' + UpperCase(sgCamposTabela.Cells[co_nome_campo, i]) + ''', p' + TratarCampoParametro(sgCamposTabela.Cells[co_nome_campo, i]) + IfThen(sgCamposTabela.Cells[co_chave, i] = 'Sim', ', True') + ');'
      );
    end;
  end;
  Adicionar('');
  Adicionar('    if vNovo then');
  Adicionar('      t.Inserir');
  Adicionar('    else');
  Adicionar('      t.Atualizar;');
  Adicionar('');
  Adicionar('    pConexao.FinalizarTransacao; ');
  Adicionar('  except');
  Adicionar('    on e: Exception do begin');
  Adicionar('      pConexao.VoltarTransacao; ');
  Adicionar('      Result.TratarErro(e);');
  Adicionar('    end;');
  Adicionar('  end;');
  Adicionar('');
  Adicionar('  t.Free;');
  Adicionar('end;');
  Adicionar('');

  Adicionar('function Buscar' + eNomeClasse.Text + '(');
  Adicionar('  pConexao: TConexao;');
  Adicionar('  pIndice: ShortInt;');
  Adicionar('  pFiltros: array of Variant');
  Adicionar('): TArray<Rec' + eNomeClasse.Text + '>;');
  Adicionar('var');
  Adicionar('  i: Integer;');
  Adicionar('  t: T' + eNomeClasse.Text + ';');
  Adicionar('begin');
  Adicionar('  Result := nil;');
  Adicionar('  t := T' + eNomeClasse.Text + '.Create(pConexao);');
  Adicionar('');
  Adicionar('  if t.Pesquisar(pIndice, pFiltros) then begin');
  Adicionar('    SetLength(Result, t.getQuantidadeRegistros);');
  Adicionar('    for i := 0 to t.getQuantidadeRegistros - 1 do begin');
  Adicionar('      Result[i] := t.getRecord' + eNomeClasse.Text + ';');
  Adicionar('      t.ProximoRegistro;');
  Adicionar('    end;');
  Adicionar('  end;');
  Adicionar('');
  Adicionar('  t.Free;');
  Adicionar('end;');
  Adicionar('');

  Adicionar('function Excluir' + eNomeClasse.Text + '(');
  Adicionar('  pConexao: TConexao;');
  for i := 1 to sgCamposTabela.RowCount -1 do begin
    if (sgCamposTabela.Cells[co_somente_leitura, i] = 'N�o') and (sgCamposTabela.Cells[co_chave, i] = 'Sim') then
      Adicionar('  p' + TratarCampoParametro(sgCamposTabela.Cells[co_nome_campo, i]) + ': ' + sgCamposTabela.Cells[co_tipo, i] + IfThen(i < sgCamposTabela.RowCount -1, ';'));
  end;
  Adicionar('): RecRetornoBD;');
  Adicionar('var');
  Adicionar('  t: T' + eNomeClasse.Text + ';');
  Adicionar('begin');
  Adicionar('  Result.Iniciar;');
  Adicionar('  t := T' + eNomeClasse.Text + '.Create(pConexao);');
  Adicionar('');
  Adicionar('  try');
  Adicionar('    pConexao.IniciarTransacao;');
  Adicionar('');
  for i := 1 to sgCamposTabela.RowCount -1 do begin
    if (sgCamposTabela.Cells[co_somente_leitura, i] = 'N�o') and (sgCamposTabela.Cells[co_chave, i] = 'Sim') then begin
      Adicionar(
        '    t.set' +
        + Decode(
          sgCamposTabela.Cells[co_tipo, i],
          [
            'Integer',   'Int',
            'Double',    'Double',
            'TDateTime', 'Data',
            'string',    'String'
          ]
        ) +
        '(''' + UpperCase(sgCamposTabela.Cells[co_nome_campo, i]) + ''', p' + TratarCampoParametro(sgCamposTabela.Cells[co_nome_campo, i]) + IfThen(sgCamposTabela.Cells[co_chave, i] = 'Sim', ', True') + ');'
      );
    end;
  end;

  Adicionar('');
  Adicionar('    t.Excluir;');
  Adicionar('');
  Adicionar('    pConexao.FinalizarTransacao;');
  Adicionar('  except');
  Adicionar('    on e: Exception do begin');
  Adicionar('      pConexao.VoltarTransacao;');
  Adicionar('      Result.TratarErro(e);');
  Adicionar('    end;');
  Adicionar('  end;');
  Adicionar('');
  Adicionar('  t.Free;');
  Adicionar('end;');
  Adicionar('');

  Adicionar('end.');
end;

procedure TFormPrincipal.btnSalvarClasseClick(Sender: TObject);
var
  salvar: TSaveDialog;
begin
  salvar := TSaveDialog.Create(Application);
  salvar.InitialDir := 'C:\FontesAltis\Orientado a Objetos\Piloto\Banco';
  salvar.Filter := '.pas';
  salvar.Title := 'Salvar nova classe';

  if salvar.Execute then
    meClasse.Lines.SaveToFile(salvar.FileName);

  salvar.Free;
end;

procedure TFormPrincipal.cbbTabelasChange(Sender: TObject);
var
  vChave: TOraQuery;
begin
  if not cbbTabelas.ItemIndex > -1 then
    Exit;

  sgCamposTabela.ClearGrid;

  FQuery.SQL.Clear;
  FQuery.SQL.Add('select ');
  FQuery.SQL.Add('  column_name, ');
  FQuery.SQL.Add('  data_type, ');
  FQuery.SQL.Add('  data_scale ');
  FQuery.SQL.Add('from ');
  FQuery.SQL.Add('  user_tab_columns ');
  FQuery.SQL.Add('where table_name = :P1 ');
  FQuery.SQL.Add('and column_name not in(''USUARIO_SESSAO_ID'', ''DATA_HORA_ALTERACAO'', ''ROTINA_ALTERACAO'', ''ESTACAO_ALTERACAO'') ');
  FQuery.SQL.Add('order by column_id ');
  FQuery.ParamByName('P1').AsString := cbbTabelas.Text;
  FQuery.Active := True;
  FQuery.First;

  vChave := TOraQuery.Create(FConexao);
  vChave.Session := FConexao;
  vChave.SQL.Add('select constraint_name from user_constraints where table_name = :P1 and constraint_type = ''P'' ');
  vChave.ParamByName('P1').AsString := cbbTabelas.Text;
  vChave.Active := True;
  vChave.First;

  while not FQuery.Eof do begin
    if FQuery.Fields[1].AsString <> 'BLOB' then begin
      eNomeCampo.Text := FQuery.Fields[0].AsString;

      while not vChave.Eof do begin
        ckChaves.Checked := Pos(eNomeCampo.Text, vChave.Fields[0].AsString) > 0;
        vChave.Next;
      end;

      vChave.First;

      if (FQuery.Fields[1].AsString = 'CHAR') or (FQuery.Fields[1].AsString = 'VARCHAR2') then
        eCodigoTipoCampo.Text := '2'
      else if FQuery.Fields[1].AsString = 'DATE' then
        eCodigoTipoCampo.Text := '4'
      else begin
        if FQuery.Fields[2].AsInteger = 0 then
          eCodigoTipoCampo.Text := '1'
        else
          eCodigoTipoCampo.Text := '3'
      end;
      sbAdicionarCampos.Click;
    end;
    FQuery.Next;
  end;
  FQuery.Active := False;

  eNomeClasse.Text := TratarCampoParametro(cbbTabelas.Text);
end;

procedure TFormPrincipal.ckChavesKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
    ckSomenteLeitura.SetFocus;
end;

procedure TFormPrincipal.ckSomenteLeituraKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
    sbAdicionarCampos.Click;
end;

procedure TFormPrincipal.eCodigoTipoCampoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
    ckChaves.SetFocus;
end;

procedure TFormPrincipal.eNomeCampoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    eCodigoTipoCampo.SetFocus;
end;

procedure TFormPrincipal.eNomeClasseKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    eNomeCampo.SetFocus;
end;

procedure TFormPrincipal.FormCreate(Sender: TObject);
begin
  try
    FConexao := TOraSession.Create(Self);
    FConexao.Options.Direct := True;
    FConexao.LoginPrompt := False;
    FConexao.ConnectString := 'PINHE/ALTIS@LOCALHOST:1521/ORCL';
    FConexao.Connect;
  except
    on e: exception do begin
      ShowMessage('Erro: ' + e.Message);
      Halt;
    end;
  end;

  FQuery := TOraQuery.Create(FConexao);
  FQuery.Sql.Add('select TABLE_NAME from tabs');
  FQuery.Sql.Add('union all');
  FQuery.Sql.Add('select VIEW_NAME from USER_VIEWS');
  FQuery.Sql.Add('order by TABLE_NAME');
  FQuery.Active := True;
  FQuery.First;

  while not FQuery.Eof do begin
    cbbTabelas.Items.Add(FQuery.Fields[0].AsString);
    FQuery.Next;
  end;

  FQuery.Active := False;
end;

procedure TFormPrincipal.lvCamposTabelaDblClick(Sender: TObject);
begin
  eNomeCampo.Text := sgCamposTabela.Cells[co_nome_campo, sgCamposTabela.Row];
  eCodigoTipoCampo.Text :=
    Decode(
      sgCamposTabela.Cells[co_nome_campo, sgCamposTabela.Row],
      [
        'Integer',   '1',
        'string',    '2',
        'Double',    '3',
        'TDateTime', '4'
      ]
    );

  ckChaves.Checked         := (sgCamposTabela.Cells[co_chave, sgCamposTabela.Row] = 'Sim');
  ckSomenteLeitura.Checked := (sgCamposTabela.Cells[co_somente_leitura, sgCamposTabela.Row] = 'Sim');
end;

procedure TFormPrincipal.lvCamposTabelaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_DELETE then
    sgCamposTabela.DeleteRow(sgCamposTabela.Row);
end;

procedure TFormPrincipal.sbAdicionarCamposClick(Sender: TObject);
var
  vTipo: string;
  vAumentarLinha: Boolean;
begin
  vAumentarLinha := False;
  if FLinha = 0 then begin
    if sgCamposTabela.Cells[co_nome_campo, 1] = '' then    
      FLinha := 1
    else begin
      FLinha := sgCamposTabela.RowCount;
      vAumentarLinha := True;
    end;
  end;

  sgCamposTabela.Cells[co_nome_campo, FLinha] := eNomeCampo.Text;

  case StrToInt(eCodigoTipoCampo.Text) of
    1: vTipo := 'Integer';
    2: vTipo := 'string';
    3: vTipo := 'Double';
    4: vTipo := 'TDateTime';
  end;

  sgCamposTabela.Cells[co_tipo, FLinha]            := vTipo;
  sgCamposTabela.Cells[co_chave, FLinha]           := IfThen(ckChaves.Checked, 'Sim', 'N�o');
  sgCamposTabela.Cells[co_somente_leitura, FLinha] := IfThen(ckSomenteLeitura.Checked, 'Sim', 'N�o');

  FLinha := 0;
  eNomeCampo.Clear;
  eCodigoTipoCampo.Clear;
  ckChaves.Checked := False;
  ckSomenteLeitura.Checked := False;

  if vAumentarLinha then
    sgCamposTabela.RowCount := sgCamposTabela.RowCount + 1;

  eNomeCampo.SetFocus;
end;

procedure TFormPrincipal.sbDescerCampoClick(Sender: TObject);
begin
  if sgCamposTabela.Row = sgCamposTabela.RowCount -1 then
    Exit;
    
  sgCamposTabela.TrocaRow(sgCamposTabela.Row, sgCamposTabela.Row + 1);
  sgCamposTabela.Row := sgCamposTabela.Row + 1;
end;

procedure TFormPrincipal.sbGerarRecordClick(Sender: TObject);
var
  i: Integer;
begin
  meClasse.Lines.Clear;                                                                                                     
  meClasse.Lines.Add('  Rec' + eNomeClasse.Text + ' = class');                                                              

  for i := 1 to sgCamposTabela.RowCount -1 do
    meClasse.Lines.Add('    ' + TratarCampoParametro(sgCamposTabela.Cells[co_nome_campo, i]) + ': ' + sgCamposTabela.Cells[co_tipo, i] + ';');

  meClasse.Lines.Add('  end;')
end;

procedure TFormPrincipal.sbSubirCampoClick(Sender: TObject);
begin
  if sgCamposTabela.Row > 1 then begin
    sgCamposTabela.TrocaRow(sgCamposTabela.Row, sgCamposTabela.Row - 1);
    sgCamposTabela.Row := sgCamposTabela.Row - 1;
  end;
end;

procedure TFormPrincipal.sgCamposTabelaDblClick(Sender: TObject);
begin
  FLinha := sgCamposTabela.Row;
  eNomeCampo.Text          := sgCamposTabela.Cells[co_nome_campo, sgCamposTabela.Row];
  eCodigoTipoCampo.Text    := 
    Decode(
      sgCamposTabela.Cells[co_tipo, sgCamposTabela.Row],
      [
        'Integer',   '1',
        'string',    '2',
        'Double',    '3',
        'TDateTime', '4',
        ''
      ]
    );

  ckChaves.Checked         := (sgCamposTabela.Cells[co_chave, sgCamposTabela.Row] = 'Sim');
  ckSomenteLeitura.Checked := (sgCamposTabela.Cells[co_somente_leitura, sgCamposTabela.Row] = 'Sim');  
end;

procedure TFormPrincipal.sgCamposTabelaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  sgCamposTabela.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taLeftJustify, Rect);
end;

procedure TFormPrincipal.sgCamposTabelaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_DELETE then
    sgCamposTabela.DeleteRow(sgCamposTabela.Row);
end;

function TFormPrincipal.TratarCampoParametro(pCampo: string): string;
var
  i: Integer;
  achou_underline: Boolean;
begin
  Result := UpperCase(pCampo[1]);
  for i := 2 to Length(pCampo) do begin
    if pCampo[i] = '_' then
      Continue;

    achou_underline := False;
    if i > 1 then
      achou_underline := pCampo[i - 1] = '_';

    if achou_underline then
      Result := Result + UpperCase(pCampo[i])
    else
      Result := Result + LowerCase(pCampo[i]);
  end;
end;

end.
