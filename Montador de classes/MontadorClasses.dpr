program MontadorClasses;

uses
  Vcl.Forms,
  Principal in 'Principal.pas' {FormPrincipal},
  _Biblioteca in '..\Repositório\_Biblioteca.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFormPrincipal, FormPrincipal);

  Application.Run;
end.
