object FormPrincipal: TFormPrincipal
  Left = 0
  Top = 0
  ActiveControl = eNomeClasse
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Montador de classes'
  ClientHeight = 425
  ClientWidth = 645
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lb1: TLabel
    Left = 5
    Top = 4
    Width = 74
    Height = 13
    Caption = 'Nome da classe'
  end
  object lb2: TLabel
    Left = 5
    Top = 43
    Width = 38
    Height = 13
    Caption = 'Campos'
  end
  object lb3: TLabel
    Left = 376
    Top = 43
    Width = 76
    Height = 13
    Caption = 'Nome do campo'
  end
  object lb4: TLabel
    Left = 589
    Top = 60
    Width = 52
    Height = 13
    Caption = '1 - Integer'
  end
  object lb5: TLabel
    Left = 589
    Top = 77
    Width = 43
    Height = 13
    Caption = '2 - string'
  end
  object lb7: TLabel
    Left = 589
    Top = 94
    Width = 49
    Height = 13
    Caption = '3 - Double'
  end
  object lb6: TLabel
    Left = 504
    Top = 43
    Width = 78
    Height = 13
    Caption = 'C'#243'd. tipo campo'
  end
  object lb8: TLabel
    Left = 589
    Top = 111
    Width = 39
    Height = 13
    Caption = '4 - Data'
  end
  object lb9: TLabel
    Left = 341
    Top = 4
    Width = 32
    Height = 13
    Caption = 'Tabela'
  end
  object sbSubirCampo: TSpeedButton
    Left = 347
    Top = 78
    Width = 18
    Height = 18
    Caption = '+'
    OnClick = sbSubirCampoClick
  end
  object sbDescerCampo: TSpeedButton
    Left = 347
    Top = 95
    Width = 18
    Height = 18
    Caption = '-'
    OnClick = sbDescerCampoClick
  end
  object meClasse: TMemo
    Left = 5
    Top = 162
    Width = 634
    Height = 214
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object btnMontar: TButton
    Left = 40
    Top = 380
    Width = 113
    Height = 41
    Caption = 'Montar classe'
    TabOrder = 2
    OnClick = btnMontarClick
  end
  object eNomeClasse: TEdit
    Left = 5
    Top = 18
    Width = 329
    Height = 21
    TabOrder = 0
    OnKeyDown = eNomeClasseKeyDown
  end
  object btnSalvarClasse: TButton
    Left = 159
    Top = 380
    Width = 113
    Height = 41
    Caption = 'Salvar classe'
    TabOrder = 3
    OnClick = btnSalvarClasseClick
  end
  object sbAdicionarCampos: TButton
    Left = 439
    Top = 99
    Width = 111
    Height = 32
    Caption = 'Adicionar campo'
    TabOrder = 4
    OnClick = sbAdicionarCamposClick
  end
  object ckChaves: TCheckBox
    Left = 376
    Top = 80
    Width = 97
    Height = 17
    Caption = 'Chave prim'#225'ria'
    TabOrder = 5
    OnKeyDown = ckChavesKeyDown
  end
  object eNomeCampo: TEdit
    Left = 376
    Top = 57
    Width = 111
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 6
    OnKeyDown = eNomeCampoKeyDown
  end
  object eCodigoTipoCampo: TEdit
    Left = 504
    Top = 57
    Width = 78
    Height = 21
    MaxLength = 1
    NumbersOnly = True
    TabOrder = 7
    OnKeyDown = eCodigoTipoCampoKeyDown
  end
  object ckSomenteLeitura: TCheckBox
    Left = 476
    Top = 80
    Width = 97
    Height = 17
    Caption = 'Somente leitura'
    TabOrder = 8
    OnKeyDown = ckSomenteLeituraKeyDown
  end
  object sbGerarRecord: TButton
    Left = 276
    Top = 380
    Width = 113
    Height = 41
    Caption = 'Gerar record'
    TabOrder = 9
    OnClick = sbGerarRecordClick
  end
  object cbbTabelas: TComboBox
    Left = 341
    Top = 18
    Width = 206
    Height = 22
    Style = csOwnerDrawFixed
    TabOrder = 10
    OnChange = cbbTabelasChange
  end
  object sgCamposTabela: TGridLuka
    Left = 5
    Top = 57
    Width = 341
    Height = 102
    ColCount = 4
    DefaultRowHeight = 19
    DrawingStyle = gdsClassic
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
    TabOrder = 11
    OnDblClick = sgCamposTabelaDblClick
    OnDrawCell = sgCamposTabelaDrawCell
    OnKeyDown = sgCamposTabelaKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Nome campo'
      'Tipo'
      'Chave?'
      'Somente leit.?')
    Grid3D = False
    RealColCount = 4
    AtivarPopUpSelecao = False
    ColWidths = (
      154
      47
      41
      73)
  end
end
