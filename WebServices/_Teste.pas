unit _Teste;

interface

type
  TTeste = class(TObject)
    FEmpresaId: Integer;
    FNomeEmpresa: string;
    FCNPJ: string;
  private
    procedure setEmpresaId(pValor: Integer);
    procedure setNomeEmpresa(pValor: string);
    procedure setCNPJ(pValor: string);
  published
    property EmpresaId: Integer read FEmpresaId write setEmpresaId;
    property NomeEmpresa: string read FNomeEmpresa write setNomeEmpresa;
    property CNPJ: string read FCNPJ write setCNPJ;
  end;

implementation

{ TTeste }

procedure TTeste.setCNPJ(pValor: string);
begin
  FCNPJ := pValor;
end;

procedure TTeste.setEmpresaId(pValor: Integer);
begin
  FEmpresaId := pValor;
end;

procedure TTeste.setNomeEmpresa(pValor: string);
begin
  FNomeEmpresa := pValor;
end;

end.
