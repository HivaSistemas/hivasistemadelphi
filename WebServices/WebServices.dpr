program WebServices;
{$APPTYPE GUI}

uses
  Vcl.Forms,
  Web.WebReq,
  IdHTTPWebBrokerBridge,
  Principal in 'Principal.pas' {FormPrincipal},
  ServerMetodos in 'ServerMetodos.pas',
  ServerContainer in 'ServerContainer.pas' {ServerContainer1: TDataModule},
  WebModule in 'WebModule.pas' {WebModule1: TWebModule},
  _Teste in '_Teste.pas',
  _SessaoWeb in '_SessaoWeb.pas',
  _Conexao in '..\Repositório\_Conexao.pas',
  _Criptografia in '..\Repositório\_Criptografia.pas',
  _Logs in '..\Repositório\_Logs.pas',
  _BibliotecaGenerica in '..\Repositório\_BibliotecaGenerica.pas',
  _RecordsEspeciais in '..\Repositório\_RecordsEspeciais.pas',
  _OperacoesBancoDados in '..\Repositório\_OperacoesBancoDados.pas';

{$R *.res}

begin
  if WebRequestHandler <> nil then
    WebRequestHandler.WebModuleClass := WebModuleClass;

  Application.Initialize;
  Application.CreateForm(TFormPrincipal, FormPrincipal);
  Application.Run;
end.
