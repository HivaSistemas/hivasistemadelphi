unit _SessaoWeb;

interface

uses
  DataSnap.DsService, DataSnap.DSAuth, DataSnap.DSSession, System.Classes, _Conexao, _RecordsEspeciais, System.SysUtils;

type
  TSessaoWeb = class(TComponent)

  public
    constructor Create(AOwner: TComponent); override;

    function IniciarConexaoServidorDados: RecRetornoWS;

  private
    FConexao: TConexao;
    FSesssaoWeb: TDSSession;

  end;

var
  SessaoWeb: TSessaoWeb;

implementation

{ TSessaoWeb }

constructor TSessaoWeb.Create(AOwner: TComponent);
begin
  inherited;

  FConexao := TConexao.Create(Self);


//  FSesssaoWeb := TDSSessionManager.GetThreadSession;
end;

function TSessaoWeb.IniciarConexaoServidorDados: RecRetornoWS;
begin
  Result := RecRetornoWS.Create;
  try
    FConexao.Conectar(
      'ENCASA',
      'DADOS',
      'localhost',
      1521,
      'ORCL',
      False
    );

    FConexao.IniciarSessao(1, 1, 'WebService');
  except
    on e: Exception do begin
      Result.TratarErro(e);
    end;
  end;
end;

end.
