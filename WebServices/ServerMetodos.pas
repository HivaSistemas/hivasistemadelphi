unit ServerMetodos;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth, Rest.JSON, _Teste, System.JSON, _SessaoWeb;

type
{$METHODINFO ON}
  TServerMetodos = class(TDSServerClass)
  private
    function GetTitulosVencidosEmAberto(pCnpj: string): TJSONObject;
    { Private declarations }
  public
    function GetEmpresas: TJSONObject;
    function GetExisteCompilacaoMinima: TJSONObject;
  end;
{$METHODINFO OFF}

implementation

{ TServerMetodos }

function TServerMetodos.GetEmpresas: TJSONObject;
var
  vEmpresa: TTeste;
begin
  vEmpresa := TTeste.Create;

  try
    vEmpresa.EmpresaId := 1;
    vEmpresa.NomeEmpresa := 'HIVA SOLUCOES LTDA';
    vEmpresa.CNPJ := '33.433.422/0001-00';

    Result := TJSON.ObjectToJsonObject(vEmpresa);
  finally
    vEmpresa.Free;
  end;


end;

function TServerMetodos.GetExisteCompilacaoMinima: TJSONObject;
begin

end;

function TServerMetodos.GetTitulosVencidosEmAberto(pCnpj: string): TJSONObject;
begin

end;

end.

