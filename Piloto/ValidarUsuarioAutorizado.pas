unit ValidarUsuarioAutorizado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.StdCtrls, EditLuka, _RecordsCadastros, _Biblioteca;

type
  TFormValidarUsuarioAutorizado = class(TFormHerancaFinalizar)
    eUsuario: TEditLuka;
    eSenha: TEditLuka;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure eUsuarioExit(Sender: TObject);
    procedure eSenhaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbFinalizarClick(Sender: TObject);
  private
    FUsuario: TArray<RecFuncionarios>;
    procedure BuscarFuncionario;
    procedure ValidarFuncionario;
  public
    { Public declarations }
  end;

  function validaUsuarioAutorizado: TRetornoTelaFinalizar;

var
  FormValidarUsuarioAutorizado: TFormValidarUsuarioAutorizado;

implementation

{$R *.dfm}

uses _Funcionarios, _Sessao;

function validaUsuarioAutorizado: TRetornoTelaFinalizar;
var
  vForm: TFormValidarUsuarioAutorizado;

begin
  vForm := TFormValidarUsuarioAutorizado.Create(Application);

  if Result.Ok(vForm.ShowModal, True) then
    Result.RetInt := vForm.FUsuario[0].funcionario_id;

  // vForm.Free;
end;

procedure TFormValidarUsuarioAutorizado.eSenhaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    ValidarFuncionario;
end;

procedure TFormValidarUsuarioAutorizado.eUsuarioExit(Sender: TObject);
begin
  inherited;
  if eUsuario.AsInt = 0 then begin
    _Biblioteca.LimparCampos([eUsuario, eSenha]);
    Exit;
  end;

  BuscarFuncionario;
end;

procedure TFormValidarUsuarioAutorizado.sbFinalizarClick(Sender: TObject);
begin
  ValidarFuncionario;
end;

procedure TFormValidarUsuarioAutorizado.BuscarFuncionario;
var
  i: Integer;
begin
  FUsuario := _Funcionarios.BuscarFuncionarios(Sessao.getConexaoBanco, 0, [eUsuario.AsInt], True, False, False, False, False);

  if FUsuario = nil then begin
    Exclamar('Usu�rio n�o encontrado');
    eUsuario.SetFocus;
    Exit;
  end;

  eUsuario.Text := IntToStr(FUsuario[0].funcionario_id) + ' - ' + FUsuario[0].apelido;
end;

procedure TFormValidarUsuarioAutorizado.ValidarFuncionario;
begin
  if FUsuario = nil then begin
    Exclamar('Usu�rio n�o encontrado');
    eUsuario.SetFocus;
    Exit;
  end;

  if Sessao.getCriptografia.Descriptografar(FUsuario[0].senha_sistema) <> eSenha.Text then begin
    Exclamar('A senha incorreta, verifique!');
    eSenha.SetFocus;
    Exit;
  end;

  if FUsuario[0].GerenteSistema then begin
    ModalResult := mrOk;
    Exit;
  end;

//  if not Sessao.AutorizadoRotina('visualizar_negociacao', FUsuario[0].funcionario_id) then begin
//    ModalResult := mrAbort;
//    Exit;
//  end;

  ModalResult := mrOk;
end;


end.
