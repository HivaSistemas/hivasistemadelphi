inherited FormBaixarItensCompras: TFormBaixarItensCompras
  Caption = 'Baixa itens pedido de compras'
  ClientHeight = 464
  ClientWidth = 806
  OnShow = FormShow
  ExplicitWidth = 812
  ExplicitHeight = 493
  PixelsPerInch = 96
  TextHeight = 14
  object lbl7: TLabel [0]
    Left = 702
    Top = 221
    Width = 64
    Height = 14
    Caption = 'Quantidade'
  end
  object Label1: TLabel [1]
    Left = 1
    Top = 221
    Width = 51
    Height = 14
    Caption = 'Produto'
  end
  inherited pnOpcoes: TPanel
    Top = 427
    Width = 806
    TabOrder = 3
    ExplicitTop = 427
    ExplicitWidth = 806
  end
  object sgItens: TGridLuka
    Left = 3
    Top = 18
    Width = 801
    Height = 185
    Align = alCustom
    Anchors = [akLeft, akTop, akRight]
    ColCount = 10
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 4
    OnDblClick = sgItensDblClick
    OnDrawCell = sgItensDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Produto'
      'Nome'
      'Marca'
      'Und.'
      'Qtde.'
      'Entr.'
      'Baix.'
      'Canc.'
      'Saldo'
      'Qtde.baixar')
    OnArrumarGrid = sgItensArrumarGrid
    Grid3D = False
    RealColCount = 16
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      54
      180
      132
      34
      60
      52
      59
      58
      70
      79)
  end
  object StaticTextLuka1: TStaticTextLuka
    Left = 3
    Top = 1
    Width = 801
    Height = 17
    Align = alCustom
    Alignment = taCenter
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    BevelInner = bvNone
    Caption = 
      'Itens dispon'#237'veis para baixa ( Duplo clique para selecionar item' +
      ' )'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 5
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  inline FrLocal: TFrLocais
    Left = 492
    Top = 221
    Width = 209
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 492
    ExplicitTop = 221
    ExplicitWidth = 209
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 202
      Height = 23
      ExplicitWidth = 225
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 209
      ExplicitWidth = 282
      inherited lbNomePesquisa: TLabel
        Width = 105
        Caption = 'Local de estoque'
        ExplicitWidth = 105
        ExplicitHeight = 15
      end
      inherited pnSuprimir: TPanel
        Left = 104
        ExplicitLeft = 177
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 202
      Width = 7
      Height = 24
      Visible = False
      ExplicitLeft = 218
      ExplicitWidth = 7
      ExplicitHeight = 24
      inherited sbPesquisa: TSpeedButton
        Left = -17
        Width = 8
        ExplicitWidth = 8
      end
    end
  end
  object eQtdeBaixar: TEditLuka
    Left = 702
    Top = 237
    Width = 94
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Text = '0,000'
    OnKeyDown = eQtdeBaixarKeyDown
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 3
    NaoAceitarEspaco = False
  end
  object sgItensBaixar: TGridLuka
    Left = 0
    Top = 332
    Width = 806
    Height = 95
    Align = alCustom
    Anchors = [akLeft, akTop, akRight]
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 6
    OnDrawCell = sgItensBaixarDrawCell
    OnKeyDown = sgItensBaixarKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Produto'
      'Nome'
      'Local'
      'Lote'
      'Qtde.baixar')
    Grid3D = False
    RealColCount = 8
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      53
      222
      163
      119
      104)
  end
  object StaticTextLuka2: TStaticTextLuka
    Left = 0
    Top = 311
    Width = 806
    Height = 17
    Align = alCustom
    Alignment = taCenter
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Itens selecionados para baixa ( Delete para excluir o produto )'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 7
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  inline FrLote: TFrameLotes
    Left = 0
    Top = 269
    Width = 287
    Height = 41
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    TabStop = True
    ExplicitTop = 269
    inherited lbLote: TLabel
      Width = 23
      Height = 14
      ExplicitWidth = 23
      ExplicitHeight = 14
    end
    inherited lbDataFabricacao: TLabel
      Width = 75
      Height = 14
      ExplicitWidth = 75
      ExplicitHeight = 14
    end
    inherited lbDataVencimento: TLabel
      Width = 56
      Height = 14
      ExplicitWidth = 56
      ExplicitHeight = 14
    end
    inherited lbBitola: TLabel
      Width = 33
      Height = 14
      ExplicitWidth = 33
      ExplicitHeight = 14
    end
    inherited lbTonalidade: TLabel
      Width = 62
      Height = 14
      ExplicitWidth = 62
      ExplicitHeight = 14
    end
    inherited eLote: TEditLuka
      Height = 22
      ExplicitHeight = 22
    end
    inherited eDataFabricacao: TEditLukaData
      Height = 22
      ExplicitHeight = 22
    end
    inherited eDataVencimento: TEditLukaData
      Height = 22
      ExplicitHeight = 22
    end
    inherited eBitola: TEditLuka
      Height = 22
      ExplicitHeight = 22
    end
    inherited eTonalidade: TEditLuka
      Height = 22
      ExplicitHeight = 22
    end
  end
  object stItemSelecionado: TStaticText
    Left = 0
    Top = 237
    Width = 490
    Height = 22
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Color = 38619
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 8
    Transparent = False
  end
  object st3: TStaticText
    Left = 3
    Top = 203
    Width = 801
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Item selecionado'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 9
    Transparent = False
  end
end
