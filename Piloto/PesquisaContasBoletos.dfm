inherited FormPesquisaContasBoletos: TFormPesquisaContasBoletos
  Caption = 'Pesquisa de contas boletos'
  ClientWidth = 666
  ExplicitWidth = 674
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 666
    ColCount = 6
    OnDrawCell = sgPesquisaDrawCell
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Descricao'
      'Banco'
      'Agencia'
      'Conta')
    RealColCount = 6
    ExplicitWidth = 604
    ColWidths = (
      28
      47
      304
      65
      78
      114)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Top = 224
    Text = '0'
    ExplicitTop = 224
  end
  inherited pnFiltro1: TPanel
    Width = 666
    ExplicitWidth = 604
    inherited lblPesquisa: TLabel
      Left = 205
      Width = 34
      Caption = 'Conta:'
      ExplicitLeft = 205
      ExplicitWidth = 34
    end
    inherited eValorPesquisa: TEditLuka
      Left = 205
      ExplicitLeft = 205
      ExplicitWidth = 380
    end
  end
end
