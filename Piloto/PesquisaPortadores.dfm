inherited FormPesquisaPortadores: TFormPesquisaPortadores
  Caption = 'Pesquisa de portadores'
  ClientHeight = 307
  ClientWidth = 578
  ExplicitWidth = 584
  ExplicitHeight = 336
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 578
    Height = 261
    ColCount = 4
    OnDrawCell = sgPesquisaDrawCell
    HCol.Strings = (
      'Sel.?'
      'Portador'
      'Descri'#231#227'o'
      'Ativo')
    RealColCount = 4
    ExplicitLeft = 0
    ExplicitTop = 46
    ExplicitWidth = 578
    ExplicitHeight = 261
    ColWidths = (
      36
      234
      206
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Left = 8
    Top = 264
    Text = '0'
    ExplicitLeft = 8
    ExplicitTop = 264
  end
  inherited pnFiltro1: TPanel
    Width = 578
    ExplicitWidth = 578
    inherited eValorPesquisa: TEditLuka
      Width = 369
      ExplicitWidth = 369
    end
  end
end
