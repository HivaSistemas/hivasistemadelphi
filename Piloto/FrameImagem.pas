unit FrameImagem;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.ExtCtrls, Vcl.Imaging.jpeg,
  Vcl.ExtDlgs, Vcl.Menus, _Biblioteca;

type
  TFrImagem = class(TFrameHerancaPrincipal)
    pnImagem: TPanel;
    imFoto: TImage;
    pmOpcoes: TPopupMenu;
    miCarregarfoto: TMenuItem;
    miApagarfoto: TMenuItem;
    procedure imFotoClick(Sender: TObject);
    procedure miApagarfotoClick(Sender: TObject);
    procedure miCarregarfotoClick(Sender: TObject);
    procedure pmOpcoesPopup(Sender: TObject);
  private
    FFilter: String;
    FDefaultExt: String;
  public
    procedure Clear; override;

    function EstaVazio: Boolean; override;
    procedure setFoto(pFoto: TMemoryStream);
    procedure SomenteLeitura(pValue: Boolean); override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;

    function getImagem: TMemoryStream;
  published
    property Filter : String read FFilter write FFilter;
    property DefaultExt : String read FDefaultExt write FDefaultExt;
  end;

implementation

{$R *.dfm}

{ TFrImagem }

procedure TFrImagem.Clear;
begin
  inherited;
  imFoto.Picture.Graphic := nil;
end;

function TFrImagem.EstaVazio: Boolean;
begin
  Result := (imFoto.Visible = False);;
end;

function TFrImagem.getImagem: TMemoryStream;
begin
  Result := nil;

  if imFoto.Picture.Graphic = nil then
    Exit;

  Result := TMemoryStream.Create;
  imFoto.Picture.Graphic.SaveToStream(Result);
end;

procedure TFrImagem.imFotoClick(Sender: TObject);
var
  vPesquisa: TOpenPictureDialog;
  vArquivo: file of Byte;
  vTamanhoArquivo: Integer;
  vExtImagem: string;
begin
  inherited;
  if Enabled then
  begin
    vPesquisa := TOpenPictureDialog.Create(Self);
    try
      vPesquisa.InitialDir := ExtractFileDir(Application.ExeName);

      if not FDefaultExt.IsEmpty then
        vPesquisa.DefaultExt := FDefaultExt;

      if not FFilter.IsEmpty then
        vPesquisa.Filter := FFilter;

      if vPesquisa.Execute then begin

        if not FDefaultExt.IsEmpty then
        begin
          vExtImagem := UpperCase(ExtractFileExt(vPesquisa.FileName));
          if not Em(vExtImagem,['.JPG','.BMP','.JPEG']) then
          begin
            Exclamar('Informe somente imagem do tipo .Bmp .Jpg ou .Jpeg');
            exit;
          end;
        end;

        AssignFile(vArquivo, vPesquisa.FileName);
        Reset(vArquivo);
        vTamanhoArquivo := FileSize(vArquivo);
        CloseFile(vArquivo);
        if ( vTamanhoArquivo * 0.000001) > 1 then
          Exclamar('A imagem n�o pode ser maior que 1 mb!')
        else
          imFoto.Picture.LoadFromFile(vPesquisa.FileName);
      end;

    finally
      vPesquisa.Free;
    end;
  end;
end;

procedure TFrImagem.miApagarfotoClick(Sender: TObject);
begin
  inherited;
  Clear;
end;

procedure TFrImagem.miCarregarfotoClick(Sender: TObject);
begin
  inherited;
  imFotoClick(Sender);
end;

procedure TFrImagem.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;
  Enabled := pEditando;

  if pEditando then
    imFoto.Visible := True;

  if pLimpar then
    imFoto.Picture.Graphic := nil;
end;

procedure TFrImagem.pmOpcoesPopup(Sender: TObject);
begin
  if not Enabled then
    Exit;

  inherited;
end;

procedure TFrImagem.setFoto(pFoto: TMemoryStream);
var
  vImagemJPG: TJPEGImage;
  vImagemBMP: TBitmap;
begin
  vImagemJPG := nil;
  vImagemBMP := nil;

  if pFoto = nil then
    Exit;

  imFoto.Visible := True;

  try
    try
      vImagemJPG := TJPEGImage.Create;
      vImagemBMP := TBitmap.Create;
      try
        pFoto.Position := 0;
        vImagemJPG.LoadFromStream(pFoto);
        imFoto.Picture.Graphic := vImagemJPG;
      except
        pFoto.Position := 0;
        vImagemBMP.LoadFromStream(pFoto);
        imFoto.Picture.Assign(vImagemBMP);
      end;

    except

    end;

  finally
    vImagemJPG.Free;
    vImagemBMP.Free;
  end;
end;

procedure TFrImagem.SomenteLeitura(pValue: Boolean);
begin
  inherited;
  imFoto.OnClick := nil;
  imFoto.PopupMenu := nil;
end;

end.
