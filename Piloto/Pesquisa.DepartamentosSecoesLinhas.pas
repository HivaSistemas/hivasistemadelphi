unit Pesquisa.DepartamentosSecoesLinhas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _Sessao, _Biblioteca,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _ProdutosDeptosSecoesLinhas, _RecordsEspeciais,
  _RecordsCadastros, System.Math, Vcl.ExtCtrls;

type
  TFormPesquisaDepartamentosSecoesLinhas = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    FNivel: Integer;
  protected
    procedure BuscarRegistros; override;
  end;

function Pesquisar(pNivel: Integer): TObject;

implementation

{$R *.dfm}

{ TFormPesquisaDepartamentosSecoesLinhas }

const
  coNome  = 2;
  coPai   = 3;
  coAtivo = 4;

function Pesquisar(pNivel: Integer): TObject;
var
  vForm: TFormPesquisaDepartamentosSecoesLinhas;
begin
  Result := nil;
  vForm := TFormPesquisaDepartamentosSecoesLinhas.Create(Application);

  vForm.FNivel := pNivel;
  vForm.sgPesquisa.OcultarColunas([coSelecionado]);
  vForm.cbOpcoesPesquisa.Filtros := _ProdutosDeptosSecoesLinhas.getFiltros;

  if vForm.ShowModal = mrOk then
    Result := vForm.FDados[vForm.sgPesquisa.Row - 1];

  vForm.Free;
end;

procedure TFormPesquisaDepartamentosSecoesLinhas.BuscarRegistros;
var
  i: Integer;
  vDeptos: TArray<RecProdutoDeptoSecaoLinha>;
begin
  inherited;

  vDeptos :=
    _ProdutosDeptosSecoesLinhas.BuscarProdutoDeptoSecaoLinhas(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text],
      FNivel
    );

  if vDeptos = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vDeptos);

  for i := Low(vDeptos) to High(vDeptos) do begin
    sgPesquisa.Cells[coSelecionado, i + 1] := 'N�o';
    sgPesquisa.Cells[coCodigo, i + 1]      := vDeptos[i].DeptoSecaoLinhaId;
    sgPesquisa.Cells[coNome, i + 1]         := vDeptos[i].Nome;
    sgPesquisa.Cells[coPai, i + 1]          := vDeptos[i].EstruturaPai;
    sgPesquisa.Cells[coAtivo, i + 1]        := SimNao(vDeptos[i].Ativo);
  end;

  sgPesquisa.RowCount := IfThen(Length(vDeptos) = 1, 2, High(vDeptos) + 2);
  SetarFoco(sgPesquisa);
end;

procedure TFormPesquisaDepartamentosSecoesLinhas.FormCreate(Sender: TObject);
begin
  inherited;
  FNivel := 0;
end;

procedure TFormPesquisaDepartamentosSecoesLinhas.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = coAtivo then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormPesquisaDepartamentosSecoesLinhas.sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coAtivo then begin
    AFont.Style := [fsBold];
    AFont.Color := AzulVermelho(sgPesquisa.Cells[ACol, ARow]);
  end;
end;

end.
