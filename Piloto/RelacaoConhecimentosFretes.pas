unit RelacaoConhecimentosFretes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _Biblioteca, _Sessao,
  Frame.Inteiros, FrameDataInicialFinal, FrameCFOPs, Frame.Cadastros, _ConhecimentosFretes,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameEmpresas, Vcl.ComCtrls,
  Vcl.StdCtrls, CheckBoxLuka, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Grids, GridLuka,
  FrameTransportadoras, InformacoesConhecimentoFrete;

type
  TFormRelacaoConhecimentosFretes = class(TFormHerancaRelatoriosPageControl)
    FrEmpresas: TFrEmpresas;
    FrCFOP: TFrCFOPs;
    FrDatasCadastro: TFrDataInicialFinal;
    FrDatasEmissao: TFrDataInicialFinal;
    FrCodigoConhecimento: TFrameInteiros;
    FrNumeroConhecimento: TFrameInteiros;
    sgConhec: TGridLuka;
    FrNumeroNota: TFrameInteiros;
    FrTransportadoras: TFrTransportadora;
    procedure sgConhecDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgConhecGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgConhecDblClick(Sender: TObject);
  protected
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

const
  coConhecimentoId    = 0;
  coTransportadora    = 1;
  coNumero            = 2;
  coModelo            = 3;
  coSerie             = 4;
  coDataEmissao       = 5;
  coValorConhecimento = 6;
  coBaseCalcICMS      = 7;
  coPercICMS          = 8;
  coValorICMS         = 9;
  coEmpresa           = 10;
  coChaveAcesso       = 11;

  (* Ocultas *)
  coTipoLinha         = 12;

  clDetalhe     = 'DET';
  clTotalizador = 'TOT';

procedure TFormRelacaoConhecimentosFretes.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vLinha: Integer;
  vConhec: TArray<RecConhecimentosFretes>;

  vTotais: record
    ValorTotal: Currency;
    BaseCalcICMS: Currency;
    ValorICMS: Currency;
  end;

begin
  inherited;
  sgConhec.ClearGrid;

  vTotais.ValorTotal   := 0;
  vTotais.BaseCalcICMS := 0;
  vTotais.ValorICMS    := 0;

  vSql := '';
  vLinha := 0;

  FrEmpresas.getSqlFiltros('CON.EMPRESA_ID', vSql, True);
  FrTransportadoras.getSqlFiltros('CON.TRANSPORTADORA_ID', vSql, True);
  FrCFOP.getSqlFiltros('CON.CFOP_ID', vSql, True);

  FrDatasCadastro.getSqlFiltros('CON.DATA_HORA_CADASTRO', vSql, True);
  FrDatasEmissao.getSqlFiltros('CON.DATA_EMISSAO', vSql, True);

  FrNumeroConhecimento.getSqlFiltros('CON.NUMERO', vSql, True);
  FrCodigoConhecimento.getSqlFiltros('CON.CONHECIMENTO_ID', vSql, True);

  if not FrNumeroNota.EstaVazio then
   _Biblioteca.WhereOuAnd(vSql, 'CON.CONHECIMENTO_ID in( select CONHECIMENTO_ID from CONHECIMENTOS_FRETES_ITENS where ' + FrNumeroNota.getSqlFiltros('NUMERO_NOTA') + ') ');

  vSql := vSql + 'order by CON.CONHECIMENTO_ID';

  vConhec := _ConhecimentosFretes.BuscarConhecimentoComando(Sessao.getConexaoBanco, vSql);
  if vConhec = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  for i := Low(vConhec) to High(vConhec) do begin
    Inc(vLinha);

    sgConhec.Cells[coConhecimentoId, vLinha]    := NFormat(vConhec[i].ConhecimentoId);
    sgConhec.Cells[coTransportadora, vLinha]    := getInformacao(vConhec[i].TransportadoraId, vConhec[i].NomeTransportadora);
    sgConhec.Cells[coNumero, vLinha]            := NFormat(vConhec[i].Numero);
    sgConhec.Cells[coModelo, vLinha]            := vConhec[i].Modelo;
    sgConhec.Cells[coSerie, vLinha]             := vConhec[i].Serie;
    sgConhec.Cells[coDataEmissao, vLinha]       := DFormat(vConhec[i].DataEmissao);
    sgConhec.Cells[coValorConhecimento, vLinha] := NFormat(vConhec[i].ValorFrete);
    sgConhec.Cells[coBaseCalcICMS, vLinha]      := NFormatN(vConhec[i].BaseCalculoIcms);
    sgConhec.Cells[coPercICMS, vLinha]          := NFormatN(vConhec[i].PercentualIcms);
    sgConhec.Cells[coValorICMS, vLinha]         := NFormatN(vConhec[i].ValorIcms);
    sgConhec.Cells[coEmpresa, vLinha]           := getInformacao(vConhec[i].EmpresaId, vConhec[i].NomeEmpresa);
    sgConhec.Cells[coChaveAcesso, vLinha]       := vConhec[i].ChaveConhecimento;

    sgConhec.Cells[coTipoLinha, vLinha] := clDetalhe;

    vTotais.ValorTotal   := vTotais.ValorTotal + vConhec[i].ValorFrete;
    vTotais.BaseCalcICMS := vTotais.BaseCalcICMS + vConhec[i].BaseCalculoIcms;
    vTotais.ValorICMS    := vTotais.ValorICMS + vConhec[i].ValorIcms;
  end;

  Inc(vLinha);

  sgConhec.Cells[coDataEmissao, vLinha]       := 'Totais: ';
  sgConhec.Cells[coValorConhecimento, vLinha] := NFormatN(vTotais.ValorTotal);
  sgConhec.Cells[coBaseCalcICMS, vLinha]      := NFormatN(vTotais.BaseCalcICMS);
  sgConhec.Cells[coValorICMS, vLinha]         := NFormatN(vTotais.ValorICMS);
  sgConhec.Cells[coTipoLinha, vLinha]         := clTotalizador;

  sgConhec.SetLinhasGridPorTamanhoVetor( vLinha );
  SetarFoco(sgConhec);
end;

procedure TFormRelacaoConhecimentosFretes.Imprimir(Sender: TObject);
begin
  inherited;

end;

procedure TFormRelacaoConhecimentosFretes.sgConhecDblClick(Sender: TObject);
begin
  inherited;
  InformacoesConhecimentoFrete.Informar(SFormatInt(sgConhec.Cells[coConhecimentoId, sgConhec.Row]));
end;

procedure TFormRelacaoConhecimentosFretes.sgConhecDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coConhecimentoId, coNumero, coModelo, coSerie, coValorConhecimento, coBaseCalcICMS, coPercICMS, coValorICMS] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgConhec.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoConhecimentosFretes.sgConhecGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgConhec.Cells[coTipoLinha, ARow] = clTotalizador then begin
    AFont.Color := clWhite;
    AFont.Style := [fsBold];
    ABrush.Color := $000096DB;
  end;
end;

end.
