unit Buscar.PessoaRetiradaMercadorias;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Biblioteca,
  Vcl.ExtCtrls, Vcl.StdCtrls, EditHoras, EditLukaData, Vcl.Mask,
  EditCpfCnpjLuka, EditLuka;

type
  TInfoRetirada = record
    NomePessoa: string;
    CPFPessoa: string;
    DataHoraRetirada: TDateTime;
    Observacoes: string;
  end;

  TFormBuscarPessoaRetiradaMercadorias = class(TFormHerancaFinalizar)
    eNome: TEditLuka;
    eCPF: TEditCPF_CNPJ_Luka;
    lblCPF_CNPJ: TLabel;
    lbl1: TLabel;
    lbllb12: TLabel;
    eDataRetirada: TEditLukaData;
    eHoraRetirada: TEditHoras;
    lbllb13: TLabel;
    meObservacoes: TMemo;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Buscar(): TRetornoTelaFinalizar<TInfoRetirada>;

implementation

{$R *.dfm}

function Buscar(): TRetornoTelaFinalizar<TInfoRetirada>;
var
  vForm: TFormBuscarPessoaRetiradaMercadorias;
begin
  vForm := TFormBuscarPessoaRetiradaMercadorias.Create(Application);

  if Result.Ok(vForm.ShowModal, True) then begin
    Result.Dados.NomePessoa       := vForm.eNome.Text;
    Result.Dados.CPFPessoa        := vForm.eCPF.Text;
    Result.Dados.DataHoraRetirada := ToDataHora(vForm.eDataRetirada.AsData, vForm.eHoraRetirada.AsHora);
    Result.Dados.Observacoes      := vForm.meObservacoes.Lines.Text;
  end;
end;

{ TFormBuscarPessoaRetiradaMercadorias }

procedure TFormBuscarPessoaRetiradaMercadorias.VerificarRegistro(Sender: TObject);
begin
  if eNome.Trim = '' then begin
    Exclamar('� necess�rio informar o nome da pessoa que retirou as mercadorias!');
    SetarFoco(eNome);
    Abort;
  end;

  inherited;
end;

end.
