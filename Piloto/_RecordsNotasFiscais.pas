unit _RecordsNotasFiscais;

interface

type
  RecNotaFiscal = record
    cadastro_id: Integer;
    CfopId: string;
    empresa_id: Byte;
    OrcamentoBaseId: Integer;
    orcamento_id: Integer;
    AcumuladoId: Integer;
    DevolucaoId: Integer;
    RetiradaId: Integer;
    EntregaId: Integer;
    ProducaoId: Integer;
    AjusteEstoqueId: Integer;
    numero_nota: Integer;
    ModeloNota: string;
    nota_fiscal_id: Integer;
    TipoMovimento: string;
    TipoMovimentoAnalitico: string;
    NotaFiscalOrigemCupomId: Integer;
    MovimentarEstOutrasNotas: string;
    valor_icms: Double;
    base_calculo_icms: Double;
    valor_outras_despesas: Double;
    valor_desconto: Double;
    valor_total_produtos: Double;
    base_calculo_icms_st: Double;
    valor_icms_st: Double;
    base_calculo_pis: Double;
    valor_pis: Double;
    valor_total: Double;
    valor_cofins: Double;
    valor_ipi: Double;
    valor_frete: Double;
    valor_seguro: Double;
    peso_liquido: Double;
    peso_bruto: Double;
    informacoes_complementares: string;
    OutraNotaId: Integer;
    TransferenciaProdutosId: Integer;

    valor_recebido_dinheiro: Double;
    valor_recebido_cartao_deb: Double;
    valor_recebido_cheque: Double;
    valor_recebido_cobranca: Double;
    valor_recebido_credito: Double;
    valor_recebido_cartao_cred: Double;
    valor_recebido_financeira: Double;
    valor_recebido_pix: Double;
    valor_recebido_acumulado: Double;

    base_calculo_cofins: Double;
    status: string;
    data_hora_emissao: TDateTime;
    data_hora_cadastro: TDateTime;
    enviou_email_nfe_cliente: string;
    danfe_impresso: string;

    codigo_ibge_municipio_emit: Integer;
    codigo_ibge_estado_emitent: Integer;
    razao_social_emitente: string;
    nome_fantasia_emitente: string;
    logradouro_emitente: string;
    numero_emitente: string;
    complemento_emitente: string;
    nome_bairro_emitente: string;
    nome_cidade_emitente: string;
    estado_id_emitente: string;
    cep_emitente: string;
    telefone_emitente: string;
    cnpj_emitente: string;
    inscricao_estadual_emitente: string;
    regime_tributario: string;

    estado_id_destinatario: string;
    nome_cidade_destinatario: string;
    nome_bairro_destinatario: string;
    complemento_destinatario: string;
    logradouro_destinatario: string;
    razao_social_destinatario: string;
    nome_fantasia_destinatario: string;
    inscricao_estadual_destinat: string;
    cep_destinatario: string;
    numero_pedido: Integer;
    codigo_ibge_municipio_dest: Integer;
    numero_destinatario: string;

    InscricaoEstEntrDestinat: string;
    LogradouroEntrDestinatario: string;
    complementoEntrDestinatario: string;
    NomeBairroEntrDestinatario: string;
    NomeCidadeEntrDestinatario: string;
    NumeroEntrDestinatario: string;
    EstadoIdEntrDestinatario: string;
    CepEntrDestinatario: string;
    CodigoIbgeMunicEntrDest: Integer;

    tipo_nota: string;
    telefone_consumidor_final: string;
    nome_consumidor_final: string;
    cpf_cnpj_destinatario: string;
    tipo_pessoa_destinatario: string;
    SerieNota: string;
    natureza_operacao: string;

    chave_nfe: string;
    data_hora_protocolo_nfe: TDateTime;
    protocolo_nfe: string;
//    numero_recibo_lote_nfe: string;
    status_nfe: string;
    motivo_status_nfe: string;

    StatusNotaAnalitico: string;
    TipoNotaAnalitico: string;
    DataHoraCancelamentoNota: TDateTime;
    MotivoCancelamentoNota: string;
    ProtocoloCancelamentoNFe: string;
    TipoCliente: string;
    IndiceFormasPagamento: Double;
    TelefoneWhatsapp: string;
    DevolucaoEntradaId: Integer;
    EmailCliente: string;
    TelefoneDestinatario: string;
    EspecieNFe: string;
    QuantidadeNFe: Double;
    tipo_frete: integer;
    transportadora_id: Integer;
    cnpj_transportadora: string;
    razao_transportadora: string;
    ie_transportadora: string;
    endereco_transportadora: string;
    cidade_transportadora: string;
    uf_transportadora: string;

    valor_icms_inter: Double;
  end;

  RecNotaFiscalItem = record
    nota_fiscal_id: Integer;
    produto_id: Integer;
    nome_produto: string;
    item_id: Integer;
    CfopId: string;
    orcamento_id: Integer;
    cst: string;
    codigo_barras: string;
    origem_produto: Byte;
    unidade: string;
    codigo_ncm: string;
    cst_cofins: string;
    percentual_cofins: Double;
    valor_cofins: Double;
    iva: Double;
    preco_pauta: Double;
    valor_ipi: Double;
    percentual_ipi: Double;
    base_calculo_cofins: Double;
    valor_pis: Double;
    percentual_pis: Double;
    base_calculo_icms_st: Double;
    valor_icms_st: Double;
    base_calculo_pis: Double;
    cst_pis: string;
    valor_icms: Double;
    percentual_icms: Double;
    base_calculo_icms: Double;
    valor_total: Double;
    preco_unitario: Double;
    valor_frete: Double;
    valor_seguro: Double;
    quantidade: Double;
    valor_total_desconto: Double;
    valor_total_outras_despesas: Double;
    indice_reducao_base_icms: Double;
    indice_reducao_base_icms_st: Double;
    percentual_icms_st: Double;
    informacoes_adicionais: string;
    cest: string;
    CodigoProdutoNFe: string;
    TipoControleEstoque: string;
    numero_item_pedido: Integer;
    numero_pedido: string;
    base_calculo_icms_inter: Double;
    valor_icms_inter: Double;
    percentual_icms_inter: Double;
  end;

  RecImpressaoDANFENFCe = record
    produtoId: Integer;
    nome: string;
    unidade: string;
    precoUnitario: Double;
    itemId: Integer;
    quantidade: Double;
  end;

implementation

end.
