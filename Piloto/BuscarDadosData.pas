unit BuscarDadosData;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Biblioteca,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Mask, EditLukaData;

type
  TFormBuscarData = class(TFormHerancaFinalizar)
    eData: TEditLukaData;
    lbData: TLabel;
    procedure FormShow(Sender: TObject);
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Buscar(pCaptionData: string; pDataAtual: TDateTime): TRetornoTelaFinalizar<TDateTime>;

implementation

{$R *.dfm}

function Buscar(pCaptionData: string; pDataAtual: TDateTime): TRetornoTelaFinalizar<TDateTime>;
var
  vForm: TFormBuscarData;
begin
  vForm := TFormBuscarData.Create(nil);

  vForm.lbData.Caption := pCaptionData;
  vForm.eData.AsData := pDataAtual;

  if Result.Ok(vForm.ShowModal, True) then
    Result.Dados := vForm.eData.AsData;
end;

{ TFormBuscarData }

procedure TFormBuscarData.FormShow(Sender: TObject);
begin
  inherited;

  SetarFoco(eData);
end;

procedure TFormBuscarData.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if not eData.DataOk then begin
    _Biblioteca.Exclamar('A data n�o foi informada corretamente, verifique!');
    SetarFoco(eData);
    Abort;
  end;
end;

end.
