unit PesquisaFuncoesFuncionario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _Biblioteca, _Sessao,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _RecordsCadastros, _CargosFuncionario,
  System.Math, Vcl.ExtCtrls;

type
  TFormPesquisaFuncoesFuncionario = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  protected
    procedure BuscarRegistros; override;
  end;

function PesquisarFuncaoFuncionario: TObject;

implementation

{$R *.dfm}

function PesquisarFuncaoFuncionario: TObject;
var
  r: TObject;
begin
  r := _HerancaPesquisas.Pesquisar(TFormPesquisaFuncoesFuncionario, _CargosFuncionario.GetFiltros);
  if r = nil then
    Result := nil
  else
    Result := RecEstados(r);
end;

const
  cp_nome  = 2;
  cp_ativo = 3;

{ TFormPesquisaFuncoesFuncionario }

procedure TFormPesquisaFuncoesFuncionario.BuscarRegistros;
var
  i: Integer;
  vFuncoes: TArray<RecCargosFuncionario>;
begin
  inherited;

  vFuncoes :=
    _CargosFuncionario.BuscarCargosFuncionarios(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text]
    );

  if vFuncoes = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vFuncoes);

  for i := Low(vFuncoes) to High(vFuncoes) do begin
    sgPesquisa.Cells[coSelecionado, i + 1] := 'N�o';
    sgPesquisa.Cells[coCodigo, i + 1]      := IntToStr(vFuncoes[i].CargoId);
    sgPesquisa.Cells[cp_nome, i + 1]        := vFuncoes[i].nome;
    sgPesquisa.Cells[cp_ativo, i + 1]       := _Biblioteca.SimNao(vFuncoes[i].ativo);
  end;

  sgPesquisa.RowCount := IfThen(Length(vFuncoes) = 1, 2, High(vFuncoes) + 2);
  sgPesquisa.SetFocus;
end;

procedure TFormPesquisaFuncoesFuncionario.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  alinhamento: TAlignment;
begin
  inherited;

  if _Biblioteca.Em(ACol, [coSelecionado, cp_ativo]) then
    alinhamento := taCenter
  else if ACol = coCodigo then
    alinhamento := taRightJustify
  else
    alinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], alinhamento, Rect);
end;

procedure TFormPesquisaFuncoesFuncionario.sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = cp_ativo then begin
    AFont.Style := [fsBold];
    AFont.Color := _Biblioteca.AzulVermelho(sgPesquisa.Cells[ACol, ARow]);
  end;
end;

end.
