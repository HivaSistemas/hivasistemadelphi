inherited FormBuscarDadosEnderecoEstoque: TFormBuscarDadosEnderecoEstoque
  Caption = 'Buscar dados endere'#231'o de estoque'
  ClientHeight = 154
  ClientWidth = 496
  ExplicitWidth = 502
  ExplicitHeight = 183
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 117
    Width = 496
    inherited sbFinalizar: TSpeedButton
      Left = 185
      ExplicitLeft = 185
    end
  end
  inline frEnderecoEstoque: TFrEnderecoEstoque
    Left = 5
    Top = 8
    Width = 489
    Height = 105
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 5
    ExplicitTop = 8
    ExplicitWidth = 489
    ExplicitHeight = 105
    inherited sgPesquisa: TGridLuka
      Width = 464
      Height = 88
      ExplicitWidth = 464
      ExplicitHeight = 121
    end
    inherited PnTitulos: TPanel
      Width = 489
      ExplicitWidth = 489
      inherited lbNomePesquisa: TLabel
        Width = 121
        ExplicitWidth = 121
        ExplicitHeight = 14
      end
      inherited CkChaveUnica: TCheckBox
        Left = 136
        Checked = False
        State = cbUnchecked
        ExplicitLeft = 136
      end
      inherited pnSuprimir: TPanel
        Left = 384
        ExplicitLeft = 384
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 464
      Height = 89
      ExplicitLeft = 464
      ExplicitHeight = 132
    end
  end
end
