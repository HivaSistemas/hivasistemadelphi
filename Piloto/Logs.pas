unit Logs;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Sessao,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, Vcl.StdCtrls, ComboBoxLuka, _RecordsCadastros,
  _LogsAlteracoes, _Biblioteca, System.Math, _FrameHerancaPrincipal,
  _FrameHenrancaPesquisas, FrameEmpresas, SpeedButtonLuka;

type
  TFormLogs = class(TFormHerancaFinalizar)
    Label1: TLabel;
    cbCampos: TComboBoxLuka;
    FrEmpresa: TFrEmpresas;
    sgLogs: TGridLuka;
    sbCarregar: TSpeedButtonLuka;
    procedure sgLogsDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure cbCamposChange(Sender: TObject);
    procedure sbCarregarClick(Sender: TObject);
  private
    FLogs: TArray<RecLogsAlteracoes>;

    FTabela: string;
    FColunaFiltro: TArray<string>;
    FView: string;
    FFiltro: TArray<Variant>;

    procedure MontarGrid(pCampo: string);
  public
    procedure IniciarLogs(pTabela: string; pColunaFiltro: TArray<string>; pView: string; pFiltro: TArray<Variant>; pLogsComEmpresa: Boolean);
  end;

procedure Iniciar(pTabela: string; pColunaFiltro: TArray<string>; pView: string; pFiltro: TArray<Variant>; pLogsComEmpresa: Boolean = False);

implementation

{$R *.dfm}

const
  coCampo             = 0;
  coValorAnterior     = 1;
  coNovoValor         = 2;
  coEmpresa           = 3;
  coUsuarioAlteracao  = 4;
  coDataHoraAlteracao = 5;
  coRotina            = 6;

procedure Iniciar(pTabela: string; pColunaFiltro: TArray<string>; pView: string; pFiltro: TArray<Variant>; pLogsComEmpresa: Boolean = False);
var
  vForm: TFormLogs;
begin
  vForm := TFormLogs.Create(nil);

  if not pLogsComEmpresa then
    vForm.sgLogs.OcultarColunas([coEmpresa]);

  vForm.FrEmpresa.Visible  := pLogsComEmpresa;
  vForm.sbCarregar.Visible := pLogsComEmpresa;

  vForm.FTabela := pTabela;
  vForm.FColunaFiltro := pColunaFiltro;
  vForm.FView := pView;
  vForm.FFiltro := pFiltro;

  vForm.IniciarLogs(pTabela, pColunaFiltro, pView, pFiltro, pLogsComEmpresa);
  vForm.ShowModal;

  vForm.Free;
end;

{ TFormLogs }

procedure TFormLogs.cbCamposChange(Sender: TObject);
begin
  inherited;
  MontarGrid(cbCampos.Text);
end;

procedure TFormLogs.IniciarLogs(pTabela: string; pColunaFiltro: TArray<string>; pView: string; pFiltro: TArray<Variant>; pLogsComEmpresa: Boolean);
var
  i: Integer;
  vCampos: TArray<string>;
begin
  vCampos := nil;

  FLogs := _LogsAlteracoes.BuscarLogs(Sessao.getConexaoBanco, pTabela, pColunaFiltro, pView, pFiltro, pLogsComEmpresa);
  for i := Low(FLogs) to High(FLogs) do
    _Biblioteca.AddNoVetorSemRepetir(vCampos, FLogs[i].campo);

  cbCampos.Clear;
  for i := Low(vCampos) to High(vCampos) do
    cbCampos.Items.Add(vCampos[i]);

  cbCampos.Items.Add('Todos');
  cbCampos.ItemIndex := High(vCampos) + 1;
  cbCamposChange(nil);
end;

procedure TFormLogs.MontarGrid(pCampo: string);
var
  i: Integer;
  vLinha: Integer;
begin
  vLinha := 0;
  sgLogs.ClearGrid();
  for i := Low(FLogs) to High(FLogs) do begin
    if (pCampo <> 'Todos') and (FLogs[i].campo <> pCampo) then
      Continue;

    Inc(vLinha);

    sgLogs.Cells[coCampo, vLinha]             := FLogs[i].campo;
    sgLogs.Cells[coValorAnterior, vLinha]     := FLogs[i].valorAnterior;
    sgLogs.Cells[coNovoValor, vLinha]         := FLogs[i].novoValor;
    sgLogs.Cells[coEmpresa, vLinha]           := NFormat(FLogs[i].EmpresaId) + ' - ' + FLogs[i].NomeEmpresa;
    sgLogs.Cells[coUsuarioAlteracao, vLinha]  := FLogs[i].usuarioAlteracao;
    sgLogs.Cells[coDataHoraAlteracao, vLinha] := _Biblioteca.DHFormat(FLogs[i].dataHoraAlteracao);
    sgLogs.Cells[coRotina, vLinha]            := FLogs[i].rotinaAlteracao;
  end;
  sgLogs.SetLinhasGridPorTamanhoVetor(vLinha);
end;

procedure TFormLogs.sbCarregarClick(Sender: TObject);
var
  i: Integer;
  vPosic: Integer;
begin
  inherited;
  vPosic := -1;

  for i := Low(FColunaFiltro) to High(FColunaFiltro) do begin
    if FColunaFiltro[i] = 'EMPRESA_ID' then begin
      vPosic := i;
      Break;
    end;
  end;

  if FrEmpresa.EstaVazio then begin
    if vPosic > -1 then begin
      Setlength(FColunaFiltro, Length(FColunaFiltro) - 1);
      SetLength(FFiltro, Length(FFiltro) - 1);
    end;
  end
  else begin
    if vPosic > -1 then
      FFiltro[vPosic] := FrEmpresa.getEmpresa().EmpresaId
    else begin
      SetLength(FColunaFiltro, Length(FColunaFiltro) + 1);
      FColunaFiltro[High(FColunaFiltro)] := 'EMPRESA_ID';

      SetLength(FFiltro, Length(FFiltro) + 1);
      FFiltro[High(FFiltro)] := FrEmpresa.getEmpresa().EmpresaId;
    end;
  end;

  IniciarLogs(FTabela, FColunaFiltro, FView, FFiltro, True);
end;

procedure TFormLogs.sgLogsDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coCampo, coValorAnterior, coNovoValor, coUsuarioAlteracao, coDataHoraAlteracao, coDataHoraAlteracao] then
    vAlinhamento := taLeftJustify
  else
    vAlinhamento := taLeftJustify;

  sgLogs.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
