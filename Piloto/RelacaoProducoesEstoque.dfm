inherited FormRelacaoProducoesEstoque: TFormRelacaoProducoesEstoque
  Caption = 'Rela'#231#227'o produ'#231#245'es de estoque'
  ExplicitTop = -55
  PixelsPerInch = 96
  TextHeight = 14
  inherited pcDados: TPageControl
    ActivePage = tsResultado
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      inline FrEmpresas: TFrEmpresas
        Left = 2
        Top = 1
        Width = 320
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 1
        ExplicitWidth = 320
        inherited sgPesquisa: TGridLuka
          Width = 295
          ExplicitWidth = 295
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 53
            ExplicitWidth = 53
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          ExplicitLeft = 295
        end
      end
      inline FrDataCadastro: TFrDataInicialFinal
        Left = 331
        Top = 1
        Width = 208
        Height = 41
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 331
        ExplicitTop = 1
        ExplicitWidth = 208
        inherited Label1: TLabel
          Width = 93
          Height = 14
          ExplicitWidth = 93
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrUsuarioInsercao: TFrFuncionarios
        Left = 2
        Top = 168
        Width = 320
        Height = 97
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 168
        ExplicitWidth = 320
        inherited sgPesquisa: TGridLuka
          Width = 295
          ExplicitWidth = 295
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 110
            Caption = 'Usu'#225'rio de inser'#231#227'o'
            ExplicitWidth = 110
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          ExplicitLeft = 295
        end
        inherited ckSomenteAtivos: TCheckBox
          Left = 184
          Top = 33
          ExplicitLeft = 184
          ExplicitTop = 33
        end
        inherited ckBuscarSomenteAtivos: TCheckBox
          Checked = False
          State = cbUnchecked
        end
        inherited poOpcoes: TPopupMenu
          Left = 229
          Top = 42
        end
      end
      inline FrCodigoAjuste: TFrNumeros
        Left = 333
        Top = 48
        Width = 134
        Height = 72
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 333
        ExplicitTop = 48
        inherited pnDescricao: TPanel
          Caption = 'C'#243'digo da produ'#231#227'o'
        end
      end
      inline FrProdutos: TFrProdutos
        Left = 2
        Top = 83
        Width = 320
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 4
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 83
        ExplicitWidth = 320
        inherited sgPesquisa: TGridLuka
          Width = 295
          ExplicitWidth = 295
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 48
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          ExplicitLeft = 295
        end
        inherited ckSomenteAtivos: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited ckFiltroExtra: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object StaticTextLuka1: TStaticTextLuka
        Left = 0
        Top = 0
        Width = 884
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Produ'#231#245'es'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
        ExplicitTop = 8
      end
      object sgAjustes: TGridLuka
        Left = 0
        Top = 17
        Width = 884
        Height = 195
        Align = alTop
        ColCount = 4
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
        TabOrder = 1
        OnClick = sgAjustesClick
        OnDrawCell = sgAjustesDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Produ'#231#227'o'
          'Empresa'
          'Data/hora produ'#231#227'o'
          'Usuario da produ'#231#227'o')
        OnGetCellColor = sgAjustesGetCellColor
        Grid3D = False
        RealColCount = 8
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          62
          190
          144
          210)
      end
      object StaticTextLuka2: TStaticTextLuka
        Left = 0
        Top = 212
        Width = 884
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Itens da produ'#231#227'o selecionado'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
        ExplicitTop = 218
      end
      object sgItens: TGridLuka
        Left = 0
        Top = 229
        Width = 884
        Height = 251
        Align = alClient
        ColCount = 7
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 3
        OnDrawCell = sgItensDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Produto'
          'Nome'
          'Marca'
          'Natureza'
          'Quantidade'
          'Pre'#231'o unit.'
          'Valor total')
        OnGetCellColor = sgItensGetCellColor
        Grid3D = False
        RealColCount = 14
        Indicador = True
        AtivarPopUpSelecao = False
        ExplicitHeight = 197
        ColWidths = (
          54
          253
          172
          90
          80
          98
          99)
      end
      object Panel1: TPanel
        Left = 0
        Top = 480
        Width = 884
        Height = 38
        Align = alBottom
        TabOrder = 4
        DesignSize = (
          884
          38)
        object st4: TStaticText
          Left = 520
          Top = 2
          Width = 188
          Height = 16
          Alignment = taCenter
          Anchors = [akRight, akBottom]
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Valor total de sa'#237'das'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          Transparent = False
        end
        object st1: TStaticText
          Left = 707
          Top = 2
          Width = 174
          Height = 16
          Alignment = taCenter
          Anchors = [akRight, akBottom]
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Valor total de entradas'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 1
          Transparent = False
        end
        object stTotalSaidas: TStaticText
          Left = 520
          Top = 17
          Width = 188
          Height = 16
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Alignment = taCenter
          Anchors = [akRight, akBottom]
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 2
          Transparent = False
        end
        object stTotalEntradas: TStaticText
          Left = 707
          Top = 17
          Width = 174
          Height = 16
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Alignment = taCenter
          Anchors = [akRight, akBottom]
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 3
          Transparent = False
        end
      end
    end
  end
end
