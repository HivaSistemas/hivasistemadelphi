unit RelacaoOrcamentosBloqueios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _Biblioteca,
  FrameDataInicialFinal, FrameEmpresas, _FrameHerancaPrincipal, Informacoes.Orcamento,
  _FrameHenrancaPesquisas, FrameClientes, Vcl.ComCtrls, Vcl.Buttons, System.StrUtils,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, FrameCondicoesPagamento, Vcl.StdCtrls, System.Math,
  GroupBoxLuka, _RelacaoOrcamentosBloqueios, _RecordsRelatorios, _Sessao, _OrcamentosBloqueios,
  _RecordsOrcamentosVendas, Vcl.Menus, _RecordsEspeciais, _Imagens, Impressao.OrcamentoGrafico,
  Frame.TipoAnaliseCusto, _Orcamentos, Frame.Inteiros, CheckBoxLuka, ValidarUsuarioAutorizado;

type
  TFormRelacaoBloqueiosOrcamentos = class(TFormHerancaRelatoriosPageControl)
    FrClientes: TFrClientes;
    FrEmpresas: TFrEmpresas;
    FrDataCadastro: TFrDataInicialFinal;
    sgOrcamentos: TGridLuka;
    FrCondicoesPagamentos: TFrCondicoesPagamento;
    st2: TStaticText;
    spSeparador: TSplitter;
    st1: TStaticText;
    sgItensBloqueios: TGridLuka;
    gbStatusBloqueio: TGroupBoxLuka;
    ckBloqueados: TCheckBox;
    ckDesbloqueados: TCheckBox;
    pmRotinas: TPopupMenu;
    pmOpcoes: TPopupMenu;
    FrCodigoOrcamento: TFrameInteiros;
    miVoltarOrcamentoFocado: TSpeedButton;
    miDesbloquearSelecionados: TSpeedButton;
    procedure sgOrcamentosDblClick(Sender: TObject);
    procedure sgOrcamentosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensBloqueiosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgOrcamentosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure miDesbloquearSelecionadosClick(Sender: TObject);
    procedure sgItensBloqueiosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure sgItensBloqueiosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgItensBloqueiosDblClick(Sender: TObject);
    procedure sgOrcamentosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure miVoltarOrcamentoFocadoClick(Sender: TObject);
  private
    FItensBloqueios: TArray<RecOrcamentosBloqueios>;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

const
  coOrcamentoId       = 0;
  coDataCadastro      = 1;
  coCliente           = 2;
  coVendedor          = 3;
  coValor             = 4;
  coPercDesconto      = 5;
  coValorDesconto     = 6;
  coPercLucroBruto    = 7;
  coPercLucroLiquido  = 8;
  coCondicaoPagamento = 9;
  coEmpresa           = 10;
  coStatus            = 11;

  (* Bloqueios *)
  ciSelecionado      = 0;
  ciDescricao        = 1;
  ciDataLiberacao    = 2;
  ciUsuarioLiberacao = 3;
  ciItemId           = 4;

procedure TFormRelacaoBloqueiosOrcamentos.Carregar(Sender: TObject);
var
  i: Integer;
  vComando: string;
  vOrcamentos: TArray<RecOrcamentosBloqueados>;

  vOrcamentosBloqueiosIds: TArray<Integer>;
begin
  inherited;
  FItensBloqueios := nil;
  sgOrcamentos.ClearGrid;
  sgItensBloqueios.ClearGrid;

  vComando := ' and ' + FrEmpresas.getSqlFiltros('ORC.EMPRESA_ID');

  if not FrClientes.EstaVazio then
    vComando := vComando + ' and ' + FrClientes.getSqlFiltros('ORC.CLIENTE_ID');

  if not FrCondicoesPagamentos.EstaVazio then
    vComando := vComando + ' and ' + FrCondicoesPagamentos.getSqlFiltros('ORC.EMPRESA_ID');

  if FrDataCadastro.DatasValidas then
    vComando := vComando + ' and ' + FrDataCadastro.getSqlFiltros('ORC.DATA_CADASTRO');

  if not gbStatusBloqueio.TodosMarcados then
    vComando := vComando + ' and ORC.ORCAMENTO_ID in(select ORCAMENTO_ID from ORCAMENTOS_BLOQUEIOS where DATA_HORA_LIBERACAO is ' + IfThen(ckDesbloqueados.Checked, 'not ') + ' null) ';

  if not FrCodigoOrcamento.EstaVazio then
    vComando := vComando + ' and ' + FrCodigoOrcamento.getSqlFiltros('ORC.ORCAMENTO_ID');

  vOrcamentos := _RelacaoOrcamentosBloqueios.BuscarOrcamentosBloqueios(Sessao.getConexaoBanco, vComando);
  if vOrcamentos = nil then begin
    NenhumRegistro;
    Exit;
  end;

  for i := Low(vOrcamentos) to High(vOrcamentos) do begin
    sgOrcamentos.Cells[coOrcamentoId, i + 1]       := NFormat(vOrcamentos[i].OrcamentoId);
    sgOrcamentos.Cells[coDataCadastro, i + 1]      := DFormat(vOrcamentos[i].DataCadastro);
    sgOrcamentos.Cells[coCliente, i + 1]           := NFormat(vOrcamentos[i].ClienteId) + ' - ' + vOrcamentos[i].NomeCliente;
    sgOrcamentos.Cells[coVendedor, i + 1]          := NFormat(vOrcamentos[i].VendedorId) + ' - ' + vOrcamentos[i].NomeVendedor;
    sgOrcamentos.Cells[coValor, i + 1]             := NFormat(vOrcamentos[i].ValorTotal);
    sgOrcamentos.Cells[coPercDesconto, i + 1]      := NFormatN(vOrcamentos[i].PercentualDesconto);
    sgOrcamentos.Cells[coValorDesconto, i + 1]     := NFormatN(vOrcamentos[i].ValorDesconto);
    sgOrcamentos.Cells[coPercLucroBruto, i + 1]    := NFormatN(Sessao.getCalculosSistema.CalcOrcamento.getPercLucroBruto(vOrcamentos[i].ValorTotal, vOrcamentos[i].ValorTotal - vOrcamentos[i].ValorCustoEntrada, 4), 4);
    sgOrcamentos.Cells[coPercLucroLiquido, i + 1]  := NFormatN(Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(vOrcamentos[i].ValorTotal, vOrcamentos[i].ValorTotal - vOrcamentos[i].ValorCustoFinal, 4), 4);
    sgOrcamentos.Cells[coCondicaoPagamento, i + 1] := NFormat(vOrcamentos[i].CondicaoId) + ' - ' + vOrcamentos[i].NomeCondicao;
    sgOrcamentos.Cells[coEmpresa, i + 1]           := NFormat(vOrcamentos[i].EmpresaId) + ' - ' + vOrcamentos[i].NomeEmpresa;
    sgOrcamentos.Cells[coStatus, i + 1]            := vOrcamentos[i].Status;

    AddNoVetorSemRepetir(vOrcamentosBloqueiosIds, vOrcamentos[i].OrcamentoId);
  end;
  sgOrcamentos.RowCount := IfThen(Length(vOrcamentos) > 1, High(vOrcamentos) + 2, 2);

  FItensBloqueios := _OrcamentosBloqueios.BuscarOrcamentosBloqueiosComando(Sessao.getConexaoBanco, ' where ' + _Biblioteca.FiltroInInt('ORCAMENTO_ID', vOrcamentosBloqueiosIds));
  sgOrcamentosClick(sgOrcamentos);
  pcDados.ActivePage := tsResultado;
end;

procedure TFormRelacaoBloqueiosOrcamentos.FormShow(Sender: TObject);
begin
  inherited;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId, False);
end;

procedure TFormRelacaoBloqueiosOrcamentos.Imprimir(Sender: TObject);
begin
  inherited;

end;

procedure TFormRelacaoBloqueiosOrcamentos.miDesbloquearSelecionadosClick(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  vDesbloquearIds: TArray<Integer>;
  vPercentualDescontoAplicado: Double;
  vRetTelaAutorizacao: TRetornoTelaFinalizar;
  vGravarNoOrcamentoDesbloqueio: Boolean;
begin
  inherited;

  if Sessao.getParametros.SenhaPedidosBloqueados = 'S' then begin
    vRetTelaAutorizacao := ValidarUsuarioAutorizado.validaUsuarioAutorizado;
    if vRetTelaAutorizacao.RetTela <> trOk then begin
      _Biblioteca.NaoAutorizadoRotina;
      Abort;
    end;
  end;

  vGravarNoOrcamentoDesbloqueio := False;
  vPercentualDescontoAplicado := SFormatDouble(sgOrcamentos.Cells[coPercDesconto, sgOrcamentos.Row]);
  vDesbloquearIds := nil;
  for i := sgItensBloqueios.FixedRows to sgItensBloqueios.RowCount - 1 do begin
    if Em(sgItensBloqueios.Cells[ciSelecionado, i], ['---', charNaoSelecionado]) then
      Continue;

    if pos('O percentual de desconto dado no orcamento e maior que o permitido', sgItensBloqueios.Cells[ciDescricao, i]) > 0 then begin
      if vPercentualDescontoAplicado > Sessao.getUsuarioLogado.PercentualDescontoAdicVenda then begin
        Exclamar('O Percentual de desconto aplicado � maior que o liberado ao usu�rio!');
        Continue;
      end;

      vGravarNoOrcamentoDesbloqueio := true;
    end;

    SetLength(vDesbloquearIds, Length(vDesbloquearIds) + 1);
    vDesbloquearIds[High(vDesbloquearIds)] := SFormatInt(sgItensBloqueios.Cells[ciItemId, i]);
  end;

  if vDesbloquearIds = nil then
    Exit;

  vRetBanco :=
    _OrcamentosBloqueios.Desbloquear(
      Sessao.getConexaoBanco,
      vPercentualDescontoAplicado,
      vGravarNoOrcamentoDesbloqueio,
      Sessao.getUsuarioLogado.funcionario_id,
      SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]),
      vDesbloquearIds
    );

  if vRetBanco.TeveErro then
    Exclamar(vRetBanco.MensagemErro)
  else begin
    RotinaSucesso;

    if (vRetBanco.AsString = 'Liberado') then begin
      if Perguntar('Deseja imprimir o or�amento agora?') then
        Impressao.OrcamentoGrafico.Imprimir(SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]));
    end;
    Carregar(Sender);
  end;
end;

procedure TFormRelacaoBloqueiosOrcamentos.miVoltarOrcamentoFocadoClick(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  vOrcamentoId: Integer;
begin
  inherited;

  vOrcamentoId := SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]);
  if vOrcamentoId = 0 then
    Exit;

  vRetBanco := _Orcamentos.CancelarFechamentoPedido(Sessao.getConexaoBanco, vOrcamentoId);
  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoBloqueiosOrcamentos.sgItensBloqueiosDblClick(Sender: TObject);
begin
  inherited;
  keybd_event(VK_SPACE, 0, 0, 0);
end;

procedure TFormRelacaoBloqueiosOrcamentos.sgItensBloqueiosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = ciSelecionado then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgItensBloqueios.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoBloqueiosOrcamentos.sgItensBloqueiosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if (ACol <> ciSelecionado) or (sgItensBloqueios.Cells[ACol, ARow] = '') or (sgItensBloqueios.Cells[ACol, ARow] = '---') then
   Exit;

  if sgItensBloqueios.Cells[ACol, ARow] = charNaoSelecionado then
    APicture := _Imagens.FormImagens.im1.Picture
  else
    APicture := _Imagens.FormImagens.im2.Picture;
end;

procedure TFormRelacaoBloqueiosOrcamentos.sgItensBloqueiosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_SPACE then begin
    if sgItensBloqueios.Cells[ciSelecionado, sgItensBloqueios.Row] = '---' then
      Exit;

    sgItensBloqueios.Cells[ciSelecionado, sgItensBloqueios.Row] := IfThen(sgItensBloqueios.Cells[ciSelecionado, sgItensBloqueios.Row] = charNaoSelecionado, charSelecionado, charNaoSelecionado);
  end;
end;

procedure TFormRelacaoBloqueiosOrcamentos.sgOrcamentosClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
begin
  inherited;
  vLinha := 0;
  sgItensBloqueios.ClearGrid;
  for i := Low(FItensBloqueios) to High(FItensBloqueios) do begin
    if SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]) <> FItensBloqueios[i].OrcamentoId then
      Continue;

    Inc(vLinha);

    sgItensBloqueios.Cells[ciSelecionado, vLinha]      := IfThen(FItensBloqueios[i].DataHoraLiberacao = 0, charNaoSelecionado, '---');
    sgItensBloqueios.Cells[ciDescricao, vLinha]        := FItensBloqueios[i].Descricao;
    sgItensBloqueios.Cells[ciDataLiberacao, vLinha]    := DFormatN(FItensBloqueios[i].DataHoraLiberacao);

    if FItensBloqueios[i].UsuarioLiberacaoId > 0 then
      sgItensBloqueios.Cells[ciUsuarioLiberacao, vLinha] := NFormat(FItensBloqueios[i].UsuarioLiberacaoId) + ' - ' + FItensBloqueios[i].NomeUsuarioLiberacao;

    sgItensBloqueios.Cells[ciItemId, vLinha]           := NFormat(FItensBloqueios[i].ItemId);
  end;
  sgItensBloqueios.RowCount := IfThen(vLinha > 1, vLinha + 1, 2);
end;

procedure TFormRelacaoBloqueiosOrcamentos.sgOrcamentosDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar(SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]));
end;

procedure TFormRelacaoBloqueiosOrcamentos.sgOrcamentosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coOrcamentoId, coPercDesconto, coValor, coValorDesconto, coPercLucroBruto, coPercLucroLiquido] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgOrcamentos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoBloqueiosOrcamentos.sgOrcamentosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coPercLucroBruto then
    AFont.Color := _Sessao.Sessao.getCorLucro( SFormatDouble( sgOrcamentos.Cells[coPercLucroBruto, ARow] ) )
  else if ACol = coPercLucroLiquido then
    AFont.Color := _Sessao.Sessao.getCorLucro( SFormatDouble( sgOrcamentos.Cells[coPercLucroLiquido, ARow] ) );
end;

procedure TFormRelacaoBloqueiosOrcamentos.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrEmpresas.EstaVazio then begin
    Exclamar('� necess�rio informar ao menos uma empresa!');
    SetarFoco(FrEmpresas);
    Abort;
  end;

  if gbStatusBloqueio.NenhumMarcado then begin
    Exclamar('� necess�rio ao menos um status de bloqueio marcado!');
    SetarFoco(gbStatusBloqueio);
    Abort;
  end;
end;

end.
