unit BuscaDados;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPrincipal, Vcl.StdCtrls, _Biblioteca,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls;

type
  TTipoDados = (tiTexto, tiNumerico);

  TFormBuscaDados = class(TFormHerancaPrincipal)
    pn1: TPanel;
    eBusca: TEditLuka;
    lbBusca: TLabel;
    sbFechar: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure sbFecharClick(Sender: TObject);
    procedure eBuscaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  end;

function BuscarDados(pDescricao: string; pValorAtual: string; pTamanhoMaximo: Integer = 0): string; overload;
function BuscarDados(pDescricao: string; pValorAtual: Double): Double; overload;
function BuscarDados(pDescricao: string; pValorAtual: Double; pValorMaximo: Double): TRetornoTelaFinalizar<Double>; overload;
function BuscarDados(pTipo: TTipoDados; pDescricao: string; pUltimoDigito: string): string; overload;

implementation

{$R *.dfm}

function BuscarDados(pDescricao: string; pValorAtual: string; pTamanhoMaximo: Integer = 0): string; overload;
var
  t: TFormBuscaDados;
begin
  Result := '';

  t := TFormBuscaDados.Create(Application);

  t.lbBusca.Caption   := pDescricao;
  t.eBusca.TipoCampo  := tcTexto;
  t.eBusca.Text       := pValorAtual;

  if pTamanhoMaximo > 0 then
    t.eBusca.MaxLength := pTamanhoMaximo;

  if t.ShowModal = mrOk then
    Result := t.eBusca.Text;

  t.Free;
end;

function BuscarDados(pDescricao: string; pValorAtual: Double): Double; overload;
var
  t: TFormBuscaDados;
begin
  Result := 0;

  t := TFormBuscaDados.Create(Application);

  t.lbBusca.Caption := pDescricao;
  t.eBusca.TipoCampo := tcNumerico;
  t.eBusca.CasasDecimais := 2;
  t.eBusca.AsDouble := pValorAtual;
  t.eBusca.SelectAll;

  if t.ShowModal = mrOk then
    Result := t.eBusca.AsDouble;

  t.Free;
end;

function BuscarDados(pDescricao: string; pValorAtual: Double; pValorMaximo: Double): TRetornoTelaFinalizar<Double>; overload;
var
  t: TFormBuscaDados;
begin
  Result.Dados := 0;

  t := TFormBuscaDados.Create(Application);

  t.lbBusca.Caption      := pDescricao;
  t.eBusca.TipoCampo     := tcNumerico;
  t.eBusca.CasasDecimais := 2;
  t.eBusca.AsDouble      := pValorAtual;
  t.eBusca.ValorMaximo   := pValorMaximo;
  t.eBusca.SelectAll;

  if Result.Ok(t.ShowModal) then
    Result.Dados := t.eBusca.AsDouble;
end;

function BuscarDados(pTipo: TTipoDados; pDescricao: string; pUltimoDigito: string): string;
var
  t: TFormBuscaDados;
begin
  Result := '';

  t := TFormBuscaDados.Create(Application);

  if pTipo = tiNumerico then
    t.eBusca.TipoCampo := tcNumerico;

  t.lbBusca.Caption := pDescricao;
  t.eBusca.Text := pUltimoDigito;

  if t.ShowModal = mrOk then
    Result := t.eBusca.Text;

  t.Free;
end;

procedure TFormBuscaDados.eBuscaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    sbFecharClick(nil);
end;

procedure TFormBuscaDados.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_ESCAPE then
    ModalResult := mrCancel;
end;

procedure TFormBuscaDados.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(eBusca);
  if eBusca.CasasDecimais = 0 then begin
    eBusca.SelLength := 0;
    eBusca.SelStart := 1;
  end;
end;

procedure TFormBuscaDados.sbFecharClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrOk;
end;

end.
