unit RelacaoLimiteCreditoClientes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameClientes, Vcl.ComCtrls,
  Vcl.StdCtrls, CheckBoxLuka, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Grids, GridLuka,
  Vcl.Mask, EditLukaData, _Clientes, _RecordsRelatorios, _Biblioteca,
  FrameDataInicialFinal, _Sessao;

type
  TFormRelacaoLimiteCreditoClientes = class(TFormHerancaRelatoriosPageControl)
    FrClientes: TFrClientes;
    sgClientes: TGridLuka;
    FrDataCadastro: TFrDataInicialFinal;
    procedure sgClientesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgClientesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  protected
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

const
  coCodigoCliente       = 0;
  coNomeCliente         = 1;
  coDataAprovacaoLimite = 2;
  coLimite              = 3;
  coSaldoUtilizado      = 4;
  coDisponivel          = 5;
  coDataValidadeLimite  = 6;

{ TFormRelacaoLimiteCreditoClientes }

procedure TFormRelacaoLimiteCreditoClientes.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vClientes: TArray<RecDadosLimiteCreditoCliente>;
begin
  inherited;
  vSql := '';
  vClientes := nil;
  sgClientes.ClearGrid();
  pcDados.ActivePage := tsResultado;

  if not FrClientes.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrClientes.getSqlFiltros('CAD.CADASTRO_ID'));

  if not FrDataCadastro.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrDataCadastro.getSqlFiltros('CAD.DATA_CADASTRO') );

  vSql := vSql + 'order by CAD.NOME_FANTASIA ';

  vClientes := _Clientes.BuscarLimiteCreditoClientes(Sessao.getConexaoBanco, vSql);
  if vClientes = nil then begin
    NenhumRegistro;
    Exit;
  end;

  for i := Low(vClientes) to High(vClientes) do begin
     sgClientes.Cells[coCodigoCliente, i + 1]        := _Biblioteca.NFormatN( vClientes[i].CadastroId );
     sgClientes.Cells[coNomeCliente, i + 1]          := vClientes[i].NomeCliente;
     sgClientes.Cells[coDataAprovacaoLimite , i + 1] := _Biblioteca.DFormatN( vClientes[i].DataAprovacaoLimiteCredito );
     sgClientes.Cells[coLimite, i + 1]               := _Biblioteca.NFormatN( vClientes[i].LimiteCredito );
     sgClientes.Cells[coDisponivel, i + 1]           := _Biblioteca.NFormatN( vClientes[i].SaldoRestante );
     sgClientes.Cells[coDataValidadeLimite, i + 1]   := _Biblioteca.DFormatN( vClientes[i].DataValidadeLimiteCredito );
  end;
  sgClientes.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vClientes) );
end;

procedure TFormRelacaoLimiteCreditoClientes.sgClientesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coCodigoCliente, coLimite , coSaldoUtilizado] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgClientes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoLimiteCreditoClientes.sgClientesGetCellColor(
  Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush;
  AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coDisponivel then begin
    AFont.Style := [fsBold];

    if _Biblioteca.SFormatDouble(sgClientes.Cells[ACol, ARow]) >= 0 then
      AFont.Color := clBlue
    else
      AFont.Color := clRed;
  end;
end;

end.
