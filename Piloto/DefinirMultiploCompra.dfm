inherited FormDefinirMultiploCompra: TFormDefinirMultiploCompra
  Caption = 'Def. mult. compra'
  ClientHeight = 198
  ClientWidth = 240
  ExplicitWidth = 246
  ExplicitHeight = 227
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 161
    Width = 240
    ExplicitTop = 161
    ExplicitWidth = 184
  end
  object sgUnidades: TGridLuka
    Left = 3
    Top = 3
    Width = 234
    Height = 153
    Align = alCustom
    ColCount = 3
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goRowSelect]
    TabOrder = 1
    OnDblClick = sgUnidadesDblClick
    OnDrawCell = sgUnidadesDrawCell
    OnKeyDown = sgUnidadesKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Und.'
      'M'#250'ltiplo'
      'Qtde.embalagem')
    Grid3D = False
    RealColCount = 3
    ColWidths = (
      36
      77
      100)
  end
end
