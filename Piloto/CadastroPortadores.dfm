inherited FormCadastroPortadores: TFormCadastroPortadores
  Caption = 'Cadastro de portadores'
  ClientHeight = 393
  ClientWidth = 733
  ExplicitWidth = 739
  ExplicitHeight = 422
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label1: TLabel
    Left = 124
    Top = 4
    Width = 46
    Caption = 'Portador'
    ExplicitLeft = 124
    ExplicitTop = 4
    ExplicitWidth = 46
  end
  object Label2: TLabel [1]
    Left = 315
    Top = 4
    Width = 53
    Height = 14
    Caption = 'Descri'#231#227'o'
  end
  inherited pnOpcoes: TPanel
    Height = 393
    ExplicitHeight = 393
  end
  inherited eID: TEditLuka
    Left = 124
    Width = 185
    Alignment = taLeftJustify
    TipoCampo = tcTexto
    ExplicitLeft = 124
    ExplicitWidth = 185
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 689
    Top = 1
    ExplicitLeft = 689
    ExplicitTop = 1
  end
  object eDescricao: TEditLuka
    Left = 315
    Top = 19
    Width = 414
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 3
    OnKeyDown = eIDKeyDown
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object PageControl1: TPageControl
    Left = 124
    Top = 44
    Width = 611
    Height = 353
    ActivePage = tsPrincipais
    TabOrder = 4
    object tsPrincipais: TTabSheet
      Caption = 'Principais'
      object Label3: TLabel
        Left = 2
        Top = 40
        Width = 134
        Height = 14
        Caption = 'Instru'#231#245'es para o boleto'
      end
      object rgTipo: TRadioGroupLuka
        Left = 2
        Top = 1
        Width = 185
        Height = 33
        Caption = ' Tipo '
        Columns = 2
        Items.Strings = (
          'Carteira'
          'Banc'#225'ria')
        TabOrder = 0
        OnClick = rgTipoClick
        Valores.Strings = (
          'C'
          'B')
      end
      object rgIncidenciaFinanceiro: TRadioGroupLuka
        Left = 193
        Top = 1
        Width = 407
        Height = 33
        Caption = ' Incid'#234'ncia no financeiro '
        Columns = 3
        Items.Strings = (
          'Contas a receber'
          'Contas a pagar'
          'Ambos')
        TabOrder = 1
        Valores.Strings = (
          'R'
          'P'
          'A')
      end
      object meInstrucoesBoleto: TMemo
        Left = 2
        Top = 55
        Width = 598
        Height = 82
        TabOrder = 2
      end
      object sgContas: TGridLuka
        Left = 2
        Top = 216
        Width = 599
        Height = 105
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alCustom
        ColCount = 4
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        PopupMenu = pmContas
        TabOrder = 4
        OnDrawCell = sgContasDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Empresa'
          'Nome fantasia'
          'Conta'
          'Empresa da conta')
        Grid3D = False
        RealColCount = 26
        AtivarPopUpSelecao = False
        ColWidths = (
          55
          205
          111
          201)
      end
      object StaticTextLuka1: TStaticTextLuka
        Left = 2
        Top = 199
        Width = 599
        Height = 17
        Align = alCustom
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Contas para recebimento'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      inline FrContasBoletos: TFrContasBoletos
        Left = 2
        Top = 144
        Width = 598
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 144
        ExplicitWidth = 598
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 573
          Height = 23
          ExplicitWidth = 573
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 598
          ExplicitWidth = 598
          inherited lbNomePesquisa: TLabel
            Width = 82
            ExplicitWidth = 82
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 493
            ExplicitLeft = 493
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 573
          Height = 24
          ExplicitLeft = 573
          ExplicitHeight = 24
        end
      end
    end
  end
  object pmContas: TPopupMenu
    Left = 680
    Top = 328
    object miInserirConta: TMenuItem
      Caption = 'Inserir conta para empresa selecionada ( Ins )'
      OnClick = miInserirContaClick
    end
    object miDeletarContaEmpresaSelecionada: TMenuItem
      Caption = 'Deletar conta para a empresa selecionada ( Del )'
      OnClick = miDeletarContaEmpresaSelecionadaClick
    end
  end
end
