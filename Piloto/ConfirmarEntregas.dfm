inherited FormConfirmarEntregas: TFormConfirmarEntregas
  Caption = 'Confirmar entregas'
  ClientHeight = 467
  ClientWidth = 994
  ExplicitWidth = 1000
  ExplicitHeight = 496
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 401
    Top = 10
    Width = 59
    Height = 14
    Caption = 'Or'#231'amento'
  end
  inherited pnOpcoes: TPanel
    Height = 467
    ExplicitHeight = 546
  end
  inline FrCliente: TFrClientes
    Left = 125
    Top = 8
    Width = 271
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 125
    ExplicitTop = 8
    ExplicitWidth = 271
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 246
      Height = 23
      ExplicitWidth = 246
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 271
      ExplicitWidth = 271
      inherited lbNomePesquisa: TLabel
        Width = 37
        Caption = 'Cliente'
        ExplicitWidth = 37
      end
    end
    inherited pnPesquisa: TPanel
      Left = 246
      Height = 24
      ExplicitLeft = 246
      ExplicitHeight = 24
    end
  end
  object eOrcamentoId: TEditLuka
    Left = 401
    Top = 25
    Width = 88
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 2
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
  end
  object sgEntregas: TGridLuka
    Left = 125
    Top = 67
    Width = 865
    Height = 159
    Align = alCustom
    Anchors = [akLeft, akTop, akRight]
    ColCount = 6
    DefaultRowHeight = 19
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
    PopupMenu = pmEntregas
    TabOrder = 3
    OnClick = sgEntregasClick
    OnDblClick = sgEntregasDblClick
    OnDrawCell = sgEntregasDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Entrega'
      'Or'#231'amento'
      'Cliente'
      'Data retirada'
      'Vendedor'
      'Usu'#225'rio gerou entrega')
    Grid3D = False
    RealColCount = 10
    ColWidths = (
      54
      72
      153
      86
      172
      201)
  end
  object sgItens: TGridLuka
    Left = 125
    Top = 242
    Width = 865
    Height = 223
    Align = alCustom
    Anchors = [akLeft, akTop, akRight, akBottom]
    DefaultRowHeight = 19
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goEditing, goRowSelect, goFixedRowClick]
    TabOrder = 4
    OnDrawCell = sgItensDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Produto'
      'Nome'
      'Marca'
      'Quantidade'
      'Und.')
    Grid3D = False
    RealColCount = 15
    OrdenarOnClick = True
    ColWidths = (
      56
      216
      171
      81
      42)
  end
  inline FrDataInicialFinal: TFrDataInicialFinal
    Left = 495
    Top = -8
    Width = 190
    Height = 57
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 5
    TabStop = True
    ExplicitLeft = 495
    ExplicitTop = -8
    ExplicitHeight = 57
    inherited Label1: TLabel
      Top = 6
      Width = 93
      Height = 14
      ExplicitTop = 6
      ExplicitWidth = 93
      ExplicitHeight = 14
    end
  end
  object StaticTextLuka1: TStaticTextLuka
    Left = 125
    Top = 51
    Width = 865
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Entregas'
    Color = 10066176
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 6
    Transparent = False
  end
  object StaticTextLuka2: TStaticTextLuka
    Left = 125
    Top = 226
    Width = 865
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Itens da entrega'
    Color = 10066176
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 7
    Transparent = False
  end
  object pmEntregas: TPopupMenu
    Left = 887
    Top = 133
    object miConfirmarRetirada: TMenuItem
      Caption = 'Confirmar entrega'
      OnClick = miConfirmarRetiradaClick
    end
    object miCancelarEntrega: TMenuItem
      Caption = 'Cancelar entrega'
      OnClick = miCancelarEntregaClick
    end
    object miReemitirComprovanteEntrega: TMenuItem
      Caption = 'Reemitir comprovante de entrega'
    end
  end
end
