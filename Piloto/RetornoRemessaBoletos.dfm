inherited FormRetornoRemessaBoletos: TFormRetornoRemessaBoletos
  Caption = 'Retorno de remessa de boletos'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    inherited sbImprimir: TSpeedButton
      Top = 336
      ExplicitTop = 336
    end
    object sbGravar: TSpeedButtonLuka [2]
      Left = 1
      Top = 77
      Width = 117
      Height = 35
      Caption = 'Gravar (F2)'
      Flat = True
      OnClick = sbGravarClick
      TagImagem = 9
      PedirCertificacao = False
      PermitirAutOutroUsuario = False
    end
    inherited ckOmitirFiltros: TCheckBoxLuka
      Top = 368
      ExplicitTop = 368
    end
  end
  inherited pcDados: TPageControl
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object sbPesquisaCaminhoArquivo: TSpeedButtonLuka
        Left = 243
        Top = 71
        Width = 16
        Height = 16
        Flat = True
        OnClick = sbPesquisaCaminhoArquivoClick
        TagImagem = 5
        PedirCertificacao = False
        PermitirAutOutroUsuario = False
      end
      object lb1: TLabel
        Left = 1
        Top = 51
        Width = 92
        Height = 14
        Caption = 'Caminho arquivo'
      end
      inline FrPortador: TFrPortadores
        Left = 2
        Top = 2
        Width = 320
        Height = 41
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 2
        ExplicitWidth = 320
        ExplicitHeight = 41
        inherited sgPesquisa: TGridLuka
          Width = 295
          Height = 24
          ExplicitWidth = 295
          ExplicitHeight = 24
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 46
            Caption = 'Portador'
            ExplicitWidth = 46
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          Height = 25
          ExplicitLeft = 295
          ExplicitHeight = 25
        end
      end
      object eCaminhoArquivo: TEditLuka
        Left = 2
        Top = 65
        Width = 238
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 1
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object FlatSplitter1: TFlatSplitter
        Left = 0
        Top = 177
        Width = 884
        Height = 6
        Cursor = crNoDrop
        AdvColorBorder = 0
        AdvColorFocused = 0
        Align = alTop
        ParentColor = False
        ExplicitLeft = 2
      end
      object pn1: TPanel
        Left = 0
        Top = 486
        Width = 884
        Height = 32
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object stTotalBoletosPagos: TStaticTextLuka
          Left = 0
          Top = 16
          Width = 132
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          Transparent = False
          AsInt = 0
          TipoCampo = tcNumerico
          CasasDecimais = 2
        end
        object st6: TStaticText
          Left = 0
          Top = 1
          Width = 132
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Total boletos pagos'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 1
          Transparent = False
        end
        object st5: TStaticText
          Left = 241
          Top = 1
          Width = 174
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Total de boletos para baixa'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 2
          Transparent = False
        end
        object stTotalBoletosBaixar: TStaticTextLuka
          Left = 241
          Top = 16
          Width = 174
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8421440
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 3
          Transparent = False
          AsInt = 0
          TipoCampo = tcNumerico
          CasasDecimais = 2
        end
        object st15: TStaticText
          Left = 131
          Top = 1
          Width = 108
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Qtde. boletos'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 4
          Transparent = False
        end
        object stQtdeBoletosPagos: TStaticTextLuka
          Left = 131
          Top = 16
          Width = 108
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '10'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clOlive
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 5
          Transparent = False
          AsInt = 0
          AsCurr = 10.000000000000000000
          AsDouble = 10.000000000000000000
          TipoCampo = tcNumerico
          CasasDecimais = 0
        end
        object st3: TStaticText
          Left = 414
          Top = 1
          Width = 108
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Qtde. boletos'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 6
          Transparent = False
        end
        object stQtdeBoletosBaixar: TStaticTextLuka
          Left = 414
          Top = 16
          Width = 108
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '10'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clOlive
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 7
          Transparent = False
          AsInt = 0
          AsCurr = 10.000000000000000000
          AsDouble = 10.000000000000000000
          TipoCampo = tcNumerico
          CasasDecimais = 0
        end
        object st4: TStaticText
          Left = 539
          Top = 1
          Width = 217
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Conta que ser'#225' utilizada para baixa'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 8
          Transparent = False
        end
        object stContaBaixa: TStaticTextLuka
          Left = 539
          Top = 16
          Width = 217
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '052029'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 9
          Transparent = False
          AsInt = 52029
          AsCurr = 52029.000000000000000000
          AsDouble = 52029.000000000000000000
          TipoCampo = tcNumerico
          CasasDecimais = 0
        end
        object st7: TStaticText
          Left = 755
          Top = 1
          Width = 108
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Data do cr'#233'dito'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 10
          Transparent = False
        end
        object stDataCredito: TStaticTextLuka
          Left = 755
          Top = 16
          Width = 108
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '05/12/2019'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 11
          Transparent = False
          AsInt = 0
          TipoCampo = tcTexto
          CasasDecimais = 0
        end
      end
      object sgOutros: TGridLuka
        Left = 0
        Top = 200
        Width = 884
        Height = 286
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 6
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 1
        OnDrawCell = sgOutrosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Nosso n'#250'mero'
          'Dt. vencto'
          'Vlr. documento'
          'Vlr. tarifa'
          'C'#243'd.retorno'
          'Descri'#231#227'o retorno')
        Grid3D = False
        RealColCount = 15
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          92
          75
          98
          65
          73
          260)
      end
      object st1: TStaticTextLuka
        Left = 0
        Top = 183
        Width = 884
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Outros documentos'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object st2: TStaticTextLuka
        Left = 0
        Top = 0
        Width = 884
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Documentos pagos'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object sgBoletos: TGridLuka
        Left = 0
        Top = 17
        Width = 884
        Height = 160
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alTop
        ColCount = 15
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 4
        OnDblClick = sgBoletosDblClick
        OnDrawCell = sgBoletosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Baixar?'
          'Nosso n'#250'mero'
          'Dt. vencto'
          'Dt. cr'#233'dito'
          'Vlr. documento'
          'Vlr. multa'
          'Vlr. juros'
          'Vlr. desc.'
          'Vlr. pago'
          'Vlr. tarifa'
          'C'#243'd.Hiva'
          'Nome cliente'
          'C'#243'd.retorno'
          'Descri'#231#227'o retorno'
          'Observa'#231#245'es')
        OnGetCellColor = sgBoletosGetCellColor
        Grid3D = False
        RealColCount = 15
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          47
          96
          64
          67
          89
          69
          66
          65
          68
          64
          58
          171
          72
          283
          264)
      end
    end
  end
end
