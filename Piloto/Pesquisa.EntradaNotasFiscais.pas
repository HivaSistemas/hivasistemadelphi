unit Pesquisa.EntradaNotasFiscais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms,
  Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _HerancaPrincipal,
  FrameDataInicialFinal, _FrameHerancaPrincipal, FrameFornecedores, _Biblioteca, _RecordsEstoques,
  _EntradasNotasFiscais, System.Math, _Sessao, _FrameHenrancaPesquisas, _HerancaPesquisasAvancadas;

type
  TTela = (ttEntrada, ttDevolucaoEntrada);

  TFormPesquisaEntradaNotasFiscais = class(TFormHerancaPesquisasAvancadas)
    FrFornecedor: TFrFornecedores;
    FrEmissaoDataInicialFinal: TFrDataInicialFinal;
    FrDataCadastro: TFrDataInicialFinal;
    eNumeroNota: TEditLuka;
    lb1: TLabel;
    lb2: TLabel;
    eEntradaId: TEditLuka;
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    FTela: TTela;
    FDados: TArray<RecEntradaNotaFiscal>;
  protected
    procedure VerificarRegistro; override;
    procedure BuscarRegistros; override;
  end;

function Pesquisar(pTela: TTela): TRetornoTelaFinalizar<RecEntradaNotaFiscal>;

implementation

{$R *.dfm}

const
  coEntradaId       = 0;
  coFornecedor      = 1;
  coNumeroNota      = 2;
  coValorNota       = 3;
  coDataEntrada     = 4;
  coDataEmissao     = 5;
  coStatus          = 6;
  coUsuarioCadastro = 7;
  //nao visivel
  coStatusRes       = 8;

function Pesquisar(pTela: TTela): TRetornoTelaFinalizar<RecEntradaNotaFiscal>;
var
  vForm: TFormPesquisaEntradaNotasFiscais;
begin
  vForm := TFormPesquisaEntradaNotasFiscais.Create(nil);

  vForm.FTela := pTela;

  Result.OkPesquisaAvancada(vForm.ShowModal, True, vForm.sgPesquisa, vForm.FDados);
end;

procedure TFormPesquisaEntradaNotasFiscais.BuscarRegistros;
var
  i: Integer;
  vComando: string;
begin
  inherited;
  vComando := '';

  if not FrFornecedor.EstaVazio then
    vComando := 'where ENT.FORNECEDOR_ID = ' + IntToStr(FrFornecedor.GetFornecedor().cadastro.cadastro_id);

  if not FrDataCadastro.NenhumaDataValida then
    WhereOuAnd(vComando, FrDataCadastro.getSqlFiltros('ENT.DATA_HORA_CADASTRO'));

  if not FrEmissaoDataInicialFinal.NenhumaDataValida then
    WhereOuAnd(vComando, FrEmissaoDataInicialFinal.getSqlFiltros('ENT.DATA_HORA_EMISSAO'));

  if eNumeroNota.AsInt > 0 then
    WhereOuAnd(vComando, 'ENT.NUMERO_NOTA = ' + IntToStr(eNumeroNota.AsInt));

  if eEntradaId.AsInt > 0 then
    WhereOuAnd(vComando, 'ENT.ENTRADA_ID = ' + IntToStr(eEntradaId.AsInt));

  if FTela = ttEntrada then
    _Biblioteca.WhereOuAnd(vComando, 'ENT.STATUS in(''ACM'', ''ACE'')')
  else begin
    _Biblioteca.WhereOuAnd(vComando, 'ENT.STATUS = ''BAI''');
  end;

  vComando := vComando + ' order by ENT.ENTRADA_ID desc ';

  FDados := _EntradasNotasFiscais.BuscarEntradaNotaFiscaisComando(Sessao.getConexaoBanco, vComando);
  if FDados = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := FDados;
  for i := Low(FDados) to High(FDados) do begin
    sgPesquisa.Cells[coEntradaId, i + 1]      := NFormat(FDados[i].entrada_id);
    sgPesquisa.Cells[coFornecedor, i + 1]     := NFormat(FDados[i].fornecedor_id) + ' - ' + FDados[i].Razao_Social;
    sgPesquisa.Cells[coNumeroNota, i + 1]     := NFormat(FDados[i].numero_nota);
    sgPesquisa.Cells[coValorNota, i + 1]      := NFormat(FDados[i].valor_total);
    sgPesquisa.Cells[coDataEntrada, i + 1]    := DFormat(FDados[i].data_hora_cadastro);
    sgPesquisa.Cells[coDataEmissao, i + 1]    := DFormat(FDados[i].data_hora_emissao);
    sgPesquisa.Cells[coStatus, i + 1]         :=
      Decode(
        FDados[i].Status,
        [
          'ACM', 'Aguardando chegada de mercadoria',
          'ACE', 'Aguardando consolidação de estoque',
          'ECO', 'Estoque consolidado',
          'FCO', 'Financeiro consolidado',
          'BAI', 'Baixada'
        ]
      );
    sgPesquisa.Cells[coUsuarioCadastro, i + 1]:= NFormat(FDados[i].UsuarioCadastroId) + ' - ' + FDados[i].NomeUsuarioCadastro;
    sgPesquisa.Cells[coStatusRes, i + 1]      := FDados[i].Status;
  end;
  sgPesquisa.SetLinhasGridPorTamanhoVetor( Length(FDados) );
  SetarFoco(sgPesquisa);
end;

procedure TFormPesquisaEntradaNotasFiscais.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrFornecedor);
end;

procedure TFormPesquisaEntradaNotasFiscais.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coEntradaId, coNumeroNota, coValorNota] then
    vAlinhamento := taRightJustify
  else if ACol in[coDataEntrada, coDataEmissao] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;


procedure TFormPesquisaEntradaNotasFiscais.sgPesquisaGetCellColor(
  Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush;
  AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coStatus then begin
    AFont.Style := [fsBold];
    AFont.Color := _EntradasNotasFiscais.getCorStatus( sgPesquisa.Cells[coStatusRes, ARow] );
  end;
end;

procedure TFormPesquisaEntradaNotasFiscais.VerificarRegistro;
begin
  inherited;
  FDados := nil;
  sgPesquisa.ClearGrid;

  if
    FrFornecedor.EstaVazio and
    FrDataCadastro.NenhumaDataValida and
    FrEmissaoDataInicialFinal.NenhumaDataValida
    and not (FTela = ttEntrada)
  then begin
    Exclamar('Nenhum filtro para pesquisa foi informado, verifique!');
    SetarFoco(FrFornecedor);
    Abort;
  end;
end;

end.
