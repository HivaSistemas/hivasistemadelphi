inherited FormCadastroGruposFornecedores: TFormCadastroGruposFornecedores
  Caption = 'Cadastro de grupos de fornecedores'
  ClientHeight = 206
  ClientWidth = 451
  ExplicitWidth = 457
  ExplicitHeight = 235
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label1: TLabel
    Top = 37
    ExplicitTop = 37
  end
  object lb1: TLabel [1]
    Left = 125
    Top = 87
    Width = 32
    Height = 14
    Caption = 'Nome'
  end
  inherited pnOpcoes: TPanel
    Height = 206
    TabOrder = 2
    ExplicitHeight = 206
    inherited sbDesfazer: TSpeedButton
      Top = 94
      ExplicitTop = 94
    end
    inherited sbExcluir: TSpeedButton
      Top = 155
      ExplicitTop = 155
    end
    inherited sbPesquisar: TSpeedButton
      Top = 46
      ExplicitTop = 46
    end
    inherited sbLogs: TSpeedButton
      Left = -119
      ExplicitLeft = -119
    end
  end
  inherited eID: TEditLuka
    Top = 51
    TabOrder = 0
    ExplicitTop = 51
  end
  object eDescricao: TEditLuka [4]
    Left = 126
    Top = 101
    Width = 317
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 50
    TabOrder = 1
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 126
    Top = 6
    TabOrder = 3
    ExplicitLeft = 126
    ExplicitTop = 6
  end
end
