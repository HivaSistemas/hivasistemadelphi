unit FrameEstados;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, PesquisaEstados,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _CadastroDesenvolvimento, _Biblioteca,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _RecordsCadastros, _Estados, _Sessao, System.Math,
  Vcl.Buttons, Vcl.Menus;

type
  TFrEstados = class(TFrameHenrancaPesquisas)
  public
    function getEstado(pLinha: Integer = -1): RecEstados;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrEstados }

function TFrEstados.AdicionarDireto: TObject;
var
  estados: TArray<RecEstados>;
begin
  estados := _Estados.BuscarEstados(Sessao.getConexaoBanco, 0, [FChaveDigitada] );
  if estados = nil then
    Result := nil
  else
    Result := estados[0];
end;

function TFrEstados.AdicionarPesquisando: TObject;
begin
  Result := PesquisaEstados.PesquisarEstado;
end;

function TFrEstados.GetEstado(pLinha: Integer): RecEstados;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecEstados(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrEstados.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecEstados(FDados[i]).estado_id = RecEstados(pSender).estado_id then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrEstados.MontarGrid;
var
  i: Integer;
  pSender: RecEstados;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      pSender := RecEstados(FDados[i]);
      AAdd([pSender.Estado_Id, pSender.Nome]);
    end;
  end;
end;

end.
