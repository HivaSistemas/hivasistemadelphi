unit PesquisaClientes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _RecordsCadastros,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _Clientes, _Sessao, System.Math, _Biblioteca,
  Vcl.ExtCtrls;

type
  TFormPesquisaClientes = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  protected
    procedure BuscarRegistros; override;
  end;

function Pesquisar(pSomenteAtivos: Boolean): TObject;
function PesquisarVarios(pSomenteAtivos: Boolean): TArray<TObject>;

implementation

{$R *.dfm}

{ TFormPesquisaClientes }

const
  cpRazaoSocial  = 2;
  cpNomeFantasia = 3;
  cpCpfCnpj      = 4;
  cpLogradouro   = 5;
  cpComplemento  = 6;
  cpBairro       = 7;
  cpCidadeUf     = 8;
  cpEmail        = 9;
  cpAtivo        = 10;

var
  FSomenteAtivos: Boolean;

function Pesquisar(pSomenteAtivos: Boolean): TObject;
var
  r: TObject;
begin
  FSomenteAtivos := pSomenteAtivos;
  r := _HerancaPesquisas.Pesquisar(TFormPesquisaClientes, _Clientes.GetFiltros);
  if r = nil then
    Result := nil
  else
    Result := RecClientes(r);
end;

function PesquisarVarios(pSomenteAtivos: Boolean): TArray<TObject>;
var
  r: TArray<TObject>;
begin
  FSomenteAtivos := pSomenteAtivos;
  r := _HerancaPesquisas.PesquisarVarios(TFormPesquisaClientes, _Clientes.GetFiltros);
  if r = nil then
    Result := nil
  else
    Result := TArray<TObject>(r);
end;

procedure TFormPesquisaClientes.BuscarRegistros;
var
  i: Integer;
  vClientes: TArray<RecClientes>;
begin
  inherited;

  vClientes :=
    _Clientes.BuscarClientes(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text],
      FSomenteAtivos
    );

  if vClientes = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vClientes);

  for i := Low(vClientes) to High(vClientes) do begin
    sgPesquisa.Cells[coSelecionado, i + 1]  := _Biblioteca.charNaoSelecionado;
    sgPesquisa.Cells[coCodigo, i + 1]       := NFormat(vClientes[i].cadastro_id);
    sgPesquisa.Cells[cpNomeFantasia, i + 1] := vClientes[i].RecCadastro.nome_fantasia;
    sgPesquisa.Cells[cpRazaoSocial, i + 1]  := vClientes[i].RecCadastro.razao_social;
    sgPesquisa.Cells[cpCpfCnpj, i + 1]      := vClientes[i].RecCadastro.cpf_cnpj;
    sgPesquisa.Cells[cpLogradouro, i + 1]   := vClientes[i].RecCadastro.logradouro;
    sgPesquisa.Cells[cpComplemento, i + 1]  := vClientes[i].RecCadastro.complemento;
    sgPesquisa.Cells[cpBairro, i + 1]       := NFormat(vClientes[i].RecCadastro.bairro_id) + ' - ' + vClientes[i].RecCadastro.NomeBairro;
    sgPesquisa.Cells[cpCidadeUf, i + 1]     := vClientes[i].RecCadastro.NomeCidade + '/' + vClientes[i].RecCadastro.estado_id;
    sgPesquisa.Cells[cpEmail, i + 1]        := vClientes[i].RecCadastro.e_mail;
    sgPesquisa.Cells[cpAtivo, i + 1]        := vClientes[i].ativo;
  end;

  sgPesquisa.SetLinhasGridPorTamanhoVetor( Length(vClientes) );
  sgPesquisa.SetFocus;
end;

procedure TFormPesquisaClientes.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  alinhamento: TAlignment;
begin
  inherited;

  if _Biblioteca.Em(ACol, [coSelecionado, cpAtivo]) then
    alinhamento := taCenter
  else if ACol = coCodigo then
    alinhamento := taRightJustify
  else
    alinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], alinhamento, Rect);
end;

end.
