inherited FrDadosCobranca: TFrDadosCobranca
  Width = 578
  Height = 293
  ExplicitWidth = 578
  ExplicitHeight = 293
  object pnInformacoesPrincipais: TPanel
    Left = 0
    Top = 0
    Width = 578
    Height = 38
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object lbllb10: TLabel
      Left = 331
      Top = 0
      Width = 35
      Height = 13
      Caption = 'Repetir'
    end
    object lbllb9: TLabel
      Left = 291
      Top = 0
      Width = 27
      Height = 13
      Caption = 'Prazo'
    end
    object lbllb6: TLabel
      Left = 442
      Top = 0
      Width = 24
      Height = 13
      Caption = 'Valor'
    end
    object lbllb2: TLabel
      Left = 377
      Top = 0
      Width = 55
      Height = 13
      Caption = 'Vencimento'
    end
    object lbl2: TLabel
      Left = 500
      Top = 0
      Width = 54
      Height = 13
      Caption = 'Documento'
    end
    object eRepetir: TEditLuka
      Left = 331
      Top = 14
      Width = 44
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      CharCase = ecUpperCase
      MaxLength = 2
      TabOrder = 2
      OnKeyDown = ProximoCampo
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 0
      NaoAceitarEspaco = False
    end
    object ePrazo: TEditLuka
      Left = 291
      Top = 14
      Width = 38
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      CharCase = ecUpperCase
      MaxLength = 2
      TabOrder = 1
      OnKeyDown = ProximoCampo
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 0
      NaoAceitarEspaco = False
    end
    object eValorCobranca: TEditLuka
      Left = 442
      Top = 14
      Width = 56
      Height = 21
      Alignment = taRightJustify
      CharCase = ecUpperCase
      TabOrder = 4
      Text = '0,00'
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 2
      NaoAceitarEspaco = False
    end
    object eDataVencimento: TEditLukaData
      Left = 377
      Top = 14
      Width = 63
      Height = 21
      Hint = 
        'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
        'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
      EditMask = '99/99/9999;1; '
      MaxLength = 10
      TabOrder = 3
      Text = '  /  /    '
      OnKeyDown = ProximoCampo
    end
    inline FrTiposCobranca: TFrTiposCobranca
      Left = 1
      Top = -1
      Width = 209
      Height = 40
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = []
      ParentBackground = False
      ParentColor = False
      ParentFont = False
      TabOrder = 0
      TabStop = True
      ExplicitLeft = 1
      ExplicitTop = -1
      ExplicitWidth = 209
      ExplicitHeight = 40
      inherited sgPesquisa: TGridLuka
        Width = 184
        Height = 23
        TabOrder = 1
        ExplicitWidth = 189
        ExplicitHeight = 23
      end
      inherited CkAspas: TCheckBox
        TabOrder = 2
      end
      inherited CkFiltroDuplo: TCheckBox
        TabOrder = 4
      end
      inherited CkPesquisaNumerica: TCheckBox
        TabOrder = 5
      end
      inherited CkMultiSelecao: TCheckBox
        TabOrder = 3
      end
      inherited PnTitulos: TPanel
        Width = 209
        TabOrder = 0
        ExplicitWidth = 214
        inherited lbNomePesquisa: TLabel
          Width = 93
          Caption = 'Tipo de cobran'#231'a'
          ExplicitWidth = 93
          ExplicitHeight = 14
        end
        inherited pnSuprimir: TPanel
          Left = 104
          ExplicitLeft = 109
          inherited ckSuprimir: TCheckBox
            Visible = False
          end
        end
      end
      inherited pnPesquisa: TPanel
        Left = 184
        Height = 24
        ExplicitLeft = 189
        ExplicitHeight = 24
      end
    end
    object eDocumento: TEditLuka
      Left = 500
      Top = 14
      Width = 78
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 5
      OnKeyPress = LimparEspaco
      TipoCampo = tcTexto
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 0
      NaoAceitarEspaco = False
    end
    object ckDataFixa: TCheckBoxLuka
      Left = 214
      Top = 15
      Width = 73
      Height = 17
      Caption = 'Data Fixa'
      TabOrder = 6
      CheckedStr = 'N'
    end
  end
  object pnInformacoesBoleto: TPanel
    Left = 0
    Top = 78
    Width = 578
    Height = 35
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    Visible = False
    object lbllb31: TLabel
      Left = 271
      Top = 0
      Width = 68
      Height = 13
      Caption = 'Nosso n'#250'mero'
    end
    object lbl1: TLabel
      Left = 1
      Top = 0
      Width = 82
      Height = 13
      Caption = 'C'#243'digo de barras'
    end
    object eNossoNumero: TEditLuka
      Left = 271
      Top = 13
      Width = 200
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 1
      OnKeyPress = Numeros
      TipoCampo = tcTexto
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 0
      NaoAceitarEspaco = False
    end
    object eCodigoBarras: TMaskEditLuka
      Left = 1
      Top = 13
      Width = 264
      Height = 21
      TabOrder = 0
      Text = ''
      OnKeyPress = Numeros
    end
  end
  object pn1: TPanel
    Left = 0
    Top = 113
    Width = 578
    Height = 180
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      578
      180)
    object sgCobrancas: TGridLuka
      Left = 1
      Top = 2
      Width = 455
      Height = 159
      Anchors = [akLeft, akTop, akBottom]
      ColCount = 17
      DefaultRowHeight = 19
      DrawingStyle = gdsGradient
      FixedColor = 15395562
      FixedCols = 0
      RowCount = 2
      Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
      TabOrder = 0
      OnDblClick = sgCobrancasDblClick
      OnDrawCell = sgCobrancasDrawCell
      OnKeyDown = sgCobrancasKeyDown
      IncrementExpandCol = 0
      IncrementExpandRow = 0
      CorLinhaFoco = 38619
      CorFundoFoco = 16774625
      CorLinhaDesfoque = 14869218
      CorFundoDesfoque = 16382457
      CorSuperiorCabecalho = clWhite
      CorInferiorCabecalho = 13553358
      CorSeparadorLinhas = 12040119
      CorColunaFoco = clActiveCaption
      HCol.Strings = (
        'Cobr.'
        'Descri'#231#227'o'
        'Valor'
        'Vlr. Reten'#231#227'o'
        'Documento'
        'Parc.'
        'Nr.parc.'
        'Vencimento'
        'C'#243'd. barras'
        'Nosso n'#250'mero'
        'Banco'
        'Ag'#234'ncia'
        'Conta corrente'
        'Num.cheque'
        'Nome emitente'
        'CPF/CNPJ emitente'
        'Telefone')
      Grid3D = False
      RealColCount = 17
      Indicador = True
      AtivarPopUpSelecao = False
      ColWidths = (
        34
        144
        53
        84
        72
        38
        56
        92
        159
        87
        47
        53
        96
        133
        113
        119
        73)
    end
    object stDiferenca: TStaticText
      Left = 458
      Top = 81
      Width = 120
      Height = 16
      Alignment = taCenter
      AutoSize = False
      BevelInner = bvNone
      BevelKind = bkFlat
      Caption = '0,00'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 1
      Transparent = False
    end
    object st2: TStaticText
      Left = 458
      Top = 66
      Width = 120
      Height = 16
      Alignment = taCenter
      AutoSize = False
      BevelInner = bvNone
      BevelKind = bkFlat
      Caption = 'Diferen'#231'a'
      Color = 15395562
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 2
      Transparent = False
    end
    object stValorTotalDefinido: TStaticText
      Left = 458
      Top = 49
      Width = 120
      Height = 16
      Alignment = taCenter
      AutoSize = False
      BevelInner = bvNone
      BevelKind = bkFlat
      Caption = '0,00'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 3
      Transparent = False
    end
    object stTotalDefinido: TStaticText
      Left = 458
      Top = 34
      Width = 120
      Height = 16
      Alignment = taCenter
      AutoSize = False
      BevelInner = bvNone
      BevelKind = bkFlat
      Caption = 'Total definido'
      Color = 15395562
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 4
      Transparent = False
    end
    object stValorTotal: TStaticText
      Left = 458
      Top = 2
      Width = 120
      Height = 16
      Alignment = taCenter
      AutoSize = False
      BevelInner = bvNone
      BevelKind = bkFlat
      Caption = 'Total a ser pago'
      Color = 15395562
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 5
      Transparent = False
    end
    object stValorTotalASerPago: TStaticText
      Left = 458
      Top = 17
      Width = 120
      Height = 16
      Alignment = taCenter
      AutoSize = False
      BevelInner = bvNone
      BevelKind = bkFlat
      Caption = '0,00'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 6
      Transparent = False
    end
    object StaticTextLuka4: TStaticTextLuka
      Left = 458
      Top = 98
      Width = 120
      Height = 15
      Alignment = taCenter
      AutoSize = False
      BevelInner = bvNone
      Caption = 'Qtde. t'#237'tulos'
      Color = 38619
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 7
      Transparent = False
      AsInt = 0
      TipoCampo = tcTexto
      CasasDecimais = 0
    end
    object stQtdeTitulos: TStaticText
      Left = 458
      Top = 113
      Width = 120
      Height = 16
      Alignment = taCenter
      AutoSize = False
      BevelInner = bvNone
      BevelKind = bkFlat
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 8
      Transparent = False
    end
    object ckObrigarDataBase: TCheckBoxLuka
      Left = 9
      Top = 68
      Width = 136
      Height = 17
      Caption = 'Obrigar data base'
      TabOrder = 9
      Visible = False
      CheckedStr = 'N'
    end
    object ckMostrarCodigoBarras: TCheckBoxLuka
      Left = 9
      Top = 84
      Width = 168
      Height = 17
      Caption = 'Mostrar campo c'#243'd. barras'
      TabOrder = 10
      Visible = False
      CheckedStr = 'N'
    end
    object st1: TStaticText
      Left = 458
      Top = 130
      Width = 120
      Height = 16
      Alignment = taCenter
      AutoSize = False
      BevelInner = bvNone
      BevelKind = bkFlat
      Caption = 'Prazo m'#233'dio calc.'
      Color = 15395562
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 11
      Transparent = False
    end
    object stPrazoMedioCalc: TStaticText
      Left = 458
      Top = 145
      Width = 120
      Height = 16
      Alignment = taCenter
      AutoSize = False
      BevelInner = bvNone
      BevelKind = bkFlat
      Caption = '0,00'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 12
      Transparent = False
    end
  end
  object pnInformacoesCheque: TPanel
    Left = 0
    Top = 38
    Width = 578
    Height = 40
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    Visible = False
    object lbllb1: TLabel
      Left = 1
      Top = 2
      Width = 29
      Height = 13
      Caption = 'Banco'
    end
    object lbllb3: TLabel
      Left = 46
      Top = 2
      Width = 38
      Height = 13
      Caption = 'Ag'#234'ncia'
    end
    object lbllb4: TLabel
      Left = 98
      Top = 2
      Width = 73
      Height = 13
      Caption = 'Conta corrente'
    end
    object lbllb5: TLabel
      Left = 181
      Top = 2
      Width = 63
      Height = 13
      Caption = 'Num. cheque'
    end
    object lbl3: TLabel
      Left = 251
      Top = 2
      Width = 72
      Height = 13
      Caption = 'Nome emitente'
    end
    object lbl4: TLabel
      Left = 500
      Top = 2
      Width = 42
      Height = 13
      Caption = 'Telefone'
    end
    object lbCPF_CNPJ: TLabel
      Left = 388
      Top = 2
      Width = 54
      Height = 13
      Caption = 'CPF / CNPJ'
    end
    object eBanco: TEditLuka
      Left = 1
      Top = 15
      Width = 40
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 5
      TabOrder = 0
      OnKeyDown = ProximoCampo
      OnKeyPress = Numeros
      TipoCampo = tcTexto
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 0
      NaoAceitarEspaco = False
    end
    object eAgencia: TEditLuka
      Left = 46
      Top = 15
      Width = 48
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 8
      TabOrder = 1
      OnKeyDown = ProximoCampo
      OnKeyPress = NumerosTraco
      TipoCampo = tcTexto
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 0
      NaoAceitarEspaco = False
    end
    object eContaCorrente: TEditLuka
      Left = 98
      Top = 15
      Width = 79
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 8
      TabOrder = 2
      OnKeyDown = ProximoCampo
      OnKeyPress = NumerosTraco
      TipoCampo = tcTexto
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 0
      NaoAceitarEspaco = False
    end
    object eNumeroCheque: TEditLuka
      Left = 181
      Top = 15
      Width = 67
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 10
      TabOrder = 3
      OnKeyDown = ProximoCampo
      OnKeyPress = Numeros
      TipoCampo = tcTexto
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 0
      NaoAceitarEspaco = False
    end
    object eNomeEmitente: TEditLuka
      Left = 251
      Top = 15
      Width = 134
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 80
      TabOrder = 4
      OnKeyDown = ProximoCampo
      TipoCampo = tcTexto
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 0
      NaoAceitarEspaco = False
    end
    object eTelefoneEmitente: TEditTelefoneLuka
      Left = 500
      Top = 15
      Width = 78
      Height = 21
      EditMask = '(99) 9999-9999;1; '
      MaxLength = 14
      TabOrder = 6
      Text = '(  )     -    '
      OnKeyDown = eTelefoneEmitenteKeyDown
    end
    object eCPF_CNPJ: TEditCPF_CNPJ_Luka
      Left = 388
      Top = 15
      Width = 108
      Height = 21
      TabOrder = 5
      Text = ''
      OnExit = eCPF_CNPJExit
      OnKeyDown = ProximoCampo
      Tipo = []
    end
  end
end
