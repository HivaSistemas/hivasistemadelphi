unit BuscarDadosDinheiro;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, _Biblioteca,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameContas, Vcl.StdCtrls,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _RecordsFinanceiros;

type
  RecPagamentosDinheiro = record
    ContaId: string;
    Valor: Double;
    DescricaoConta: string;
  end;

  TFormBuscarDadosDinheiro = class(TFormHerancaFinalizar)
    lbllb11: TLabel;
    eValorDinheiro: TEditLuka;
    FrConta: TFrContas;
    sgContas: TGridLuka;
    stValorTotal: TStaticText;
    stValorTotalASerPago: TStaticText;
    stValorTotalDefinido: TStaticText;
    stTotalDefinido: TStaticText;
    stDiferenca: TStaticText;
    st2: TStaticText;
    procedure eValorDinheiroKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgContasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure sgContasKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgContasDblClick(Sender: TObject);
  private
    procedure AddGrid(pValor: Double; pContaId: string; pDescricaoConta: string);
    procedure Totalizar;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Buscar(pValorBuscar: Double; pPagamentos: TArray<RecPagamentosDinheiro>): TRetornoTelaFinalizar<TArray<RecPagamentosDinheiro>>;
function BuscarDadosPix(pValorBuscar: Double; pPagamentos: TArray<RecPagamentosPix>): TRetornoTelaFinalizar<TArray<RecPagamentosPix>>;

implementation

{$R *.dfm}

{ TFrBuscarDadosDinheiro }

const
  coContaId   = 0;
  coDescricao = 1;
  coValor     = 2;

function Buscar(pValorBuscar: Double; pPagamentos: TArray<RecPagamentosDinheiro>): TRetornoTelaFinalizar<TArray<RecPagamentosDinheiro>>;
var
  i: Integer;
  vForm: TFormBuscarDadosDinheiro;
begin
  Result.Dados := pPagamentos;
  vForm := TFormBuscarDadosDinheiro.Create(nil);
  vForm.stValorTotalASerPago.Caption := NFormat(pValorBuscar);

  for i := Low(pPagamentos) to High(pPagamentos) do begin
    vForm.AddGrid(
      pPagamentos[i].Valor,
      pPagamentos[i].ContaId,
      pPagamentos[i].DescricaoConta
    );

    vForm.Totalizar;
  end;


  if Result.Ok(vForm.ShowModal, True) then begin
    Result.Dados := nil;
    SetLength(Result.Dados, vForm.sgContas.RowCount -1);
    for i := 1 to vForm.sgContas.RowCount -1 do begin
      Result.Dados[i - 1].ContaId        := vForm.sgContas.Cells[coContaId, i];
      Result.Dados[i - 1].Valor          := SFormatDouble(vForm.sgContas.Cells[coValor, i]);
      Result.Dados[i - 1].DescricaoConta := vForm.sgContas.Cells[coDescricao, i];
    end;
  end;
end;

function BuscarDadosPix(pValorBuscar: Double; pPagamentos: TArray<RecPagamentosPix>): TRetornoTelaFinalizar<TArray<RecPagamentosPix>>;
var
  i: Integer;
  vForm: TFormBuscarDadosDinheiro;
begin
  Result.Dados := pPagamentos;
  vForm := TFormBuscarDadosDinheiro.Create(nil);
  vForm.stValorTotalASerPago.Caption := NFormat(pValorBuscar);

  for i := Low(pPagamentos) to High(pPagamentos) do begin
    vForm.AddGrid(
      pPagamentos[i].Valor,
      pPagamentos[i].ContaId,
      pPagamentos[i].DescricaoConta
    );

    vForm.Totalizar;
  end;


  if Result.Ok(vForm.ShowModal, True) then begin
    Result.Dados := nil;
    SetLength(Result.Dados, vForm.sgContas.RowCount -1);
    for i := 1 to vForm.sgContas.RowCount -1 do begin
      Result.Dados[i - 1].ContaId        := vForm.sgContas.Cells[coContaId, i];
      Result.Dados[i - 1].Valor          := SFormatDouble(vForm.sgContas.Cells[coValor, i]);
      Result.Dados[i - 1].DescricaoConta := vForm.sgContas.Cells[coDescricao, i];
    end;
  end;
end;


procedure TFormBuscarDadosDinheiro.AddGrid(pValor: Double; pContaId: string; pDescricaoConta: string);
var
  vLinha: Integer;
begin
  vLinha := sgContas.Localizar([coContaId], [pContaId]);
  if vLinha = -1 then begin
    if sgContas.Cells[coContaId, 1] = '' then
      vLinha := 1
    else begin
      vLinha := sgContas.RowCount;
      sgContas.RowCount := sgContas.RowCount + 1;
    end;
  end;
  sgContas.Cells[coContaId, vLinha]   := pContaId;
  sgContas.Cells[coDescricao, vLinha] := pDescricaoConta;
  sgContas.Cells[coValor, vLinha]     := NFormat(pValor);
end;

procedure TFormBuscarDadosDinheiro.eValorDinheiroKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if eValorDinheiro.AsCurr = 0 then begin
    _Biblioteca.Exclamar('O valor do dinheiro n�o foi informado corretamente, verifique!');
    SetarFoco(eValorDinheiro);
    Exit;
  end;

  if FrConta.EstaVazio then begin
    _Biblioteca.Exclamar('A conta n�o foi informada corretamente, verifique!');
    SetarFoco(FrConta);
    Exit;
  end;

  AddGrid(eValorDinheiro.AsDouble, FrConta.GetConta().Conta, FrConta.GetConta().Nome);
  _Biblioteca.LimparCampos([FrConta, eValorDinheiro]);
  SetarFoco(FrConta);
  Totalizar;
end;

procedure TFormBuscarDadosDinheiro.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrConta);
end;

procedure TFormBuscarDadosDinheiro.sgContasDblClick(Sender: TObject);
begin
  inherited;
  if sgContas.Cells[coContaId, 1] = '' then
    Exit;

  FrConta.InserirDadoPorChave(sgContas.Cells[coContaId, sgContas.Row]);
  eValorDinheiro.AsDouble := SFormatDouble(sgContas.Cells[coValor, sgContas.Row]);
  SetarFoco(FrConta);
end;

procedure TFormBuscarDadosDinheiro.sgContasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = coValor then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgContas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormBuscarDadosDinheiro.sgContasKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_DELETE then
    sgContas.DeleteRow(sgContas.Row);
end;

procedure TFormBuscarDadosDinheiro.Totalizar;
var
  i: Integer;
  vTotalDefinido: Double;
begin
  vTotalDefinido := 0;
  for i := 1 to sgContas.RowCount -1 do
    vTotalDefinido := vTotalDefinido + SFormatDouble(sgContas.Cells[coValor, i]);

  stValorTotalDefinido.Caption := NFormatN(vTotalDefinido);
  stDiferenca.Caption          := NFormatN(SFormatDouble(stValorTotalASerPago.Caption) - vTotalDefinido);
end;

procedure TFormBuscarDadosDinheiro.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if stDiferenca.Caption <> '' then begin
    _Biblioteca.Exclamar('Existe diferen�a entre o valor a ser pago e o valor definido, verifique!');
    SetarFoco(FrConta);
    Abort;
  end;
end;

end.
