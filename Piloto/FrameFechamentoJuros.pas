unit FrameFechamentoJuros;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FrameFechamento, Vcl.StdCtrls, EditLuka, _Sessao,
  Vcl.Buttons;

type
  TFrFechamentoJuros = class(TFrFechamento)
    lb5: TLabel;
    eValorJuros: TEditLuka;
    ePercentualJuros: TEditLuka;
    lb6: TLabel;
    procedure eValorJurosChange(Sender: TObject);
    procedure ePercentualJurosChange(Sender: TObject);
  public
    procedure TotalizarValorASerPago; override;
  end;

implementation

{$R *.dfm}

{ TFrFechamentoJuros }

procedure TFrFechamentoJuros.ePercentualJurosChange(Sender: TObject);
begin
  inherited;
  if ePercentualJuros.Focused and (FTotalProdutos > 0) then begin
    eValorJuros.AsDouble := FTotalProdutos * ePercentualJuros.AsDouble * 0.01;
    eValorDesconto.AsDouble := (FTotalProdutos + eValorOutrasDespesas.AsDouble + eValorJuros.AsDouble)* ePercentualDesconto.AsDouble * 0.01;
    TotalizarValorASerPago;
  end;
end;

procedure TFrFechamentoJuros.eValorJurosChange(Sender: TObject);
begin
  inherited;
  if (eValorJuros.Focused or (Sender = nil)) and (FTotalProdutos > 0) then begin
    ePercentualJuros.AsDouble := Sessao.getCalculosSistema.CalcOrcamento.getPercJuros(FTotalProdutos, eValorJuros.AsDouble);
    eValorDescontoChange(nil);

    if eValorJuros.Focused then
      TotalizarValorASerPago;
  end;
end;

procedure TFrFechamentoJuros.TotalizarValorASerPago;
var
  vTotalFormasPagamento: Currency;
begin
  inherited;

  eValorTotalASerPago.AsCurr := FTotalProdutos + eValorOutrasDespesas.AsCurr - eValorDesconto.AsCurr + eValorFrete.AsDouble + eValorJuros.AsDouble;
  vTotalFormasPagamento      := eValorDinheiro.AsCurr + eValorCheque.AsCurr + eValorCartaoDebito.AsCurr + eValorCartaoCredito.AsCurr + eValorCobranca.AsCurr + eValorFinanceira.AsCurr + eValorAcumulativo.AsCurr + eValorCredito.AsCurr + eValorPix.AsCurr;
  eValorTroco.AsCurr         := vTotalFormasPagamento - eValorTotalASerPago.AsCurr;
end;

end.
