unit BuscarDadosTexto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.StdCtrls, EditLuka, _Biblioteca;

type
  TFormBuscarDadosTexto = class(TFormHerancaFinalizar)
    eDados: TEditLuka;
    lbDados: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function BuscarDadosString(caption: string; rotulo: string; tamanho: Integer): TRetornoTelaFinalizar<string>;

implementation

{$R *.dfm}

function BuscarDadosString(caption: string; rotulo: string; tamanho: Integer): TRetornoTelaFinalizar<string>;
var
  vForm: TFormBuscarDadosTexto;
begin
  vForm := TFormBuscarDadosTexto.Create(Application);
  vForm.Caption := caption;
  vForm.lbDados.Caption := rotulo;
  vForm.eDados.MaxLength := tamanho;

  if Result.Ok(vForm.ShowModal) then
    Result.Dados := vForm.eDados.Text;

  FreeAndNil(vForm);
end;

end.
