unit Edicao.CapaNotaFiscal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Biblioteca, _Sessao,
  Vcl.ExtCtrls, Vcl.StdCtrls, EditLuka, _FrameHerancaPrincipal, _RecordsNotasFiscais,
  _FrameHenrancaPesquisas, FrameCFOPs, ComboBoxLuka, Vcl.Mask, _RecordsEspeciais, _Empresas,
  _RecordsCadastros, FrameNomeComputador, Buscar.Enderecos, BuscarDadosEmpresaEmissaoNota,
  _ParametrosEmpresa, FrameUnidades, FrameTransportadoras;

type
  TFormEdicaoCapaNotaFiscal = class(TFormHerancaFinalizar)
    lb1: TLabel;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl27: TLabel;
    eNotaFiscalId: TEditLuka;
    eNomeCliente: TEditLuka;
    eNumeroNota: TEditLuka;
    eEmpresaNota: TEditLuka;
    lbl15: TLabel;
    lbl16: TLabel;
    lbl17: TLabel;
    lbl18: TLabel;
    lbl19: TLabel;
    lbl20: TLabel;
    lbl21: TLabel;
    lbl22: TLabel;
    lbl23: TLabel;
    lbl24: TLabel;
    lbl25: TLabel;
    lbl26: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    lbl10: TLabel;
    lbl11: TLabel;
    lbl12: TLabel;
    lbl13: TLabel;
    eValorBaseCalculoICMS: TEditLuka;
    eValorICMS: TEditLuka;
    eBaseCalculoICMSST: TEditLuka;
    eValorICMSST: TEditLuka;
    eValorTotalProdutos: TEditLuka;
    eValorFrete: TEditLuka;
    eValorSeguro: TEditLuka;
    eValorDesconto: TEditLuka;
    eValorOutrasDespesas: TEditLuka;
    eValorIPI: TEditLuka;
    eValorTotalNota: TEditLuka;
    eInformacoesComplementares: TMemo;
    eLogradouro: TEditLuka;
    eComplemento: TEditLuka;
    eNumero: TEditLuka;
    eBairro: TEditLuka;
    eCidade: TEditLuka;
    eEstadoId: TEditLuka;
    eCEP: TEditLuka;
    cbTipoNota: TComboBoxLuka;
    lb3: TLabel;
    FrCFOP: TFrCFOPs;
    cbModelo: TComboBoxLuka;
    lb2: TLabel;
    cbSerieNota: TComboBoxLuka;
    eInscricaoEstadual: TEditLuka;
    lb6: TLabel;
    sbCarregarEnderecos: TSpeedButton;
    sbAlterarEmpresaEmitente: TSpeedButton;
    SpeedButton1: TSpeedButton;
    Label1: TLabel;
    Label2: TLabel;
    ePesoBruto: TEditLuka;
    ePesoLiquido: TEditLuka;
    Label3: TLabel;
    eQuantidade: TEditLuka;
    FrUnidadeVenda: TFrUnidades;
    stSPC: TStaticText;
    Label4: TLabel;
    cbTipoFrete: TComboBoxLuka;
    FrTransportadora: TFrTransportadora;
    procedure cbTipoNotaChange(Sender: TObject);
    procedure sbCarregarEnderecosClick(Sender: TObject);
    procedure sbAlterarEmpresaEmitenteClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    FNotaFiscal: RecNotaFiscal;
    FAlterouEmpresa: Boolean;
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Editar(const pNotaFiscalId: Integer): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

uses
  _NotasFiscais, _Bairros, BuscarDadosClienteEmissaoNota;

function Editar(const pNotaFiscalId: Integer): TRetornoTelaFinalizar;
var
  vForm: TFormEdicaoCapaNotaFiscal;
  vNotaFiscal: TArray<RecNotaFiscal>;

  function FormatarChaveNFe(pChave: string): string;
  var
    i: Integer;
    vCont: Integer;
  begin
    vCont := 1;
    pChave := RetornaNumeros(pChave);
    for i := 1 to Length(pChave) do begin
      Result := Result + pChave[i];
      Inc(vCont);
      if vCont = 5 then begin
        Result := Result + '.';
        vCont := 1;
      end;
    end;
  end;

begin
  if pNotaFiscalId = 0 then
    Exit;

  vNotaFiscal := _NotasFiscais.BuscarNotasFiscais(Sessao.getConexaoBanco, 0, [pNotaFiscalId]);
  if vNotaFiscal = nil then begin
    NenhumRegistro;
    Exit;
  end;
 //Aleterado Ezequiel para poder eitar capa de notas emitidas
//  if vNotaFiscal[0].status <> 'N' then begin
//    _Biblioteca.Exclamar('N�o � permitido editar uma nota fiscal onde o status seja diferente de "N�o emitida"!');
//    Exit;
//  end;

  vForm := TFormEdicaoCapaNotaFiscal.Create(Application);

  vForm.FAlterouEmpresa := False;

  vForm.FNotaFiscal := vNotaFiscal[0];
  vForm.cbTipoNota.Enabled := (vNotaFiscal[0].cadastro_id <> Sessao.getParametros.cadastro_consumidor_final_id);

  vForm.eNotaFiscalId.AsInt          := vNotaFiscal[0].nota_fiscal_id;
  vForm.eNomeCliente.Text            := vNotaFiscal[0].nome_fantasia_destinatario;
  vForm.eEmpresaNota.Text            := NFormat(vNotaFiscal[0].empresa_id) + ' - ' + vNotaFiscal[0].razao_social_emitente;
  vForm.cbTipoNota.AsString          := vNotaFiscal[0].tipo_nota;
  vForm.cbTipoFrete.SetIndicePorValor(vNotaFiscal[0].tipo_frete);

  vForm.eQuantidade.AsDouble := vNotaFiscal[0].QuantidadeNFe;
  vForm.ePesoBruto.AsDouble := vNotaFiscal[0].peso_bruto;
  vForm.ePesoLiquido.AsDouble := vNotaFiscal[0].peso_liquido;
  vForm.FrUnidadeVenda.InserirDadoPorChave(vNotaFiscal[0].EspecieNFe);

  if vNotaFiscal[0].numero_nota > 0 then
    vForm.cbTipoNota.Enabled := False;

  vForm.eNumeroNota.AsInt            := vNotaFiscal[0].numero_nota;
  vForm.cbModelo.SetIndicePorValor(vNotaFiscal[0].ModeloNota);
  vForm.cbSerieNota.SetIndicePorValor(vNotaFiscal[0].SerieNota);
  vForm.FrCFOP.InserirDadoPorChave(vNotaFiscal[0].CfopId);
  vForm.eInscricaoEstadual.Text      := vNotaFiscal[0].inscricao_estadual_destinat;
  vForm.eLogradouro.Text             := vNotaFiscal[0].logradouro_destinatario;
  vForm.eComplemento.Text            := vNotaFiscal[0].complemento_destinatario;
  vForm.eNumero.Text                 := vNotaFiscal[0].numero_destinatario;
  vForm.eBairro.Text                 := vNotaFiscal[0].nome_bairro_destinatario;
  vForm.eCidade.Text                 := vNotaFiscal[0].nome_cidade_destinatario;
  vForm.eEstadoId.Text               := vNotaFiscal[0].estado_id_destinatario;
  vForm.eCEP.Text                    := vNotaFiscal[0].cep_destinatario;
  vForm.eValorBaseCalculoICMS.AsCurr := vNotaFiscal[0].base_calculo_icms;
  vForm.eValorICMS.AsCurr            := vNotaFiscal[0].valor_icms;
  vForm.eBaseCalculoICMSST.AsCurr    := vNotaFiscal[0].base_calculo_icms_st;
  vForm.eValorICMSST.AsCurr          := vNotaFiscal[0].valor_icms_st;
  vForm.eValorTotalProdutos.AsCurr   := vNotaFiscal[0].valor_total_produtos;
  vForm.eValorFrete.AsCurr           := vNotaFiscal[0].valor_frete;
  vForm.eValorSeguro.AsCurr          := vNotaFiscal[0].valor_seguro;
  vForm.eValorDesconto.AsCurr        := vNotaFiscal[0].valor_desconto;
  vForm.eValorOutrasDespesas.AsCurr  := vNotaFiscal[0].valor_outras_despesas;
  vForm.eValorIPI.AsCurr             := vNotaFiscal[0].valor_ipi;
  vForm.eValorTotalNota.AsCurr       := vNotaFiscal[0].valor_total;
  vForm.eInformacoesComplementares.Text := vNotaFiscal[0].informacoes_complementares;
  vForm.FrTransportadora.InserirDadoPorChave(vNotaFiscal[0].transportadora_id);

  Result.Ok(vForm.ShowModal);

  vForm.Free;
end;

{ TFormEdicaoCapaNotaFiscal }

procedure TFormEdicaoCapaNotaFiscal.cbTipoNotaChange(Sender: TObject);
begin
  inherited;
  cbModelo.AsString := IIfStr(cbTipoNota.GetValor = 'N', '55', '65');
end;

procedure TFormEdicaoCapaNotaFiscal.Finalizar(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  especieNFe: string;
  vTransportadoraId: Integer;
begin
  especieNFe := '';
  if not FrUnidadeVenda.EstaVazio then
    especieNFe := FrUnidadeVenda.getUnidade().unidade_id;

  if not FrTransportadora.EstaVazio then
    vTransportadoraId := FrTransportadora.GetDado().CadastroId;

  vRetBanco :=
    _NotasFiscais.AtualizarNotasFiscais(
      Sessao.getConexaoBanco,
      FNotaFiscal.nota_fiscal_id,
      FNotaFiscal.OutraNotaId,
      FNotaFiscal.cadastro_id,
      FrCFOP.GetCFOP().CfopPesquisaId,
      FNotaFiscal.empresa_id,
      FNotaFiscal.OrcamentoBaseId,
      FNotaFiscal.orcamento_id,
      FNotaFiscal.DevolucaoId,
      FNotaFiscal.AcumuladoId,
      FNotaFiscal.RetiradaId,
      FNotaFiscal.TipoMovimento,
      eNumeroNota.AsInt,
      cbModelo.GetValor,
      cbSerieNota.GetValor,

      FNotaFiscal.razao_social_emitente,
      FNotaFiscal.nome_fantasia_emitente,
      FNotaFiscal.regime_tributario,
      FNotaFiscal.cnpj_emitente,
      FNotaFiscal.inscricao_estadual_emitente,
      FNotaFiscal.logradouro_emitente,
      FNotaFiscal.complemento_emitente,
      FNotaFiscal.nome_bairro_emitente,
      FNotaFiscal.nome_cidade_emitente,
      FNotaFiscal.numero_emitente,
      FNotaFiscal.estado_id_emitente,
      FNotaFiscal.cep_emitente,
      FNotaFiscal.telefone_emitente,
      FNotaFiscal.codigo_ibge_municipio_emit,
      FNotaFiscal.codigo_ibge_estado_emitent,

      FNotaFiscal.nome_fantasia_destinatario,
      FNotaFiscal.razao_social_destinatario,
      FNotaFiscal.tipo_pessoa_destinatario,
      FNotaFiscal.cpf_cnpj_destinatario,
      eInscricaoEstadual.Text,
      eLogradouro.Text,
      eComplemento.Text,
      eBairro.Text,
      FNotaFiscal.nome_cidade_destinatario,
      eNumero.Text,
      FNotaFiscal.estado_id_destinatario,
      eCEP.Text,
      FNotaFiscal.codigo_ibge_municipio_dest,

      FNotaFiscal.InscricaoEstEntrDestinat,
      FNotaFiscal.LogradouroEntrDestinatario,
      FNotaFiscal.complementoEntrDestinatario,
      FNotaFiscal.NomeBairroEntrDestinatario,
      FNotaFiscal.NomeCidadeEntrDestinatario,
      FNotaFiscal.NumeroEntrDestinatario,
      FNotaFiscal.EstadoIdEntrDestinatario,
      FNotaFiscal.CepEntrDestinatario,
      FNotaFiscal.CodigoIbgeMunicEntrDest,

      FNotaFiscal.nome_consumidor_final,
      FNotaFiscal.telefone_consumidor_final,

      cbTipoNota.GetValor,
      FrCFOP.GetCFOP().nome,

      eValorTotalNota.AsCurr,
      eValorTotalProdutos.AsCurr,
      eValorDesconto.AsCurr,
      eValorOutrasDespesas.AsCurr,
      eValorFrete.AsCurr,
      eValorSeguro.AsCurr,
      eValorBaseCalculoICMS.AsCurr,
      eValorICMS.AsCurr,
      eBaseCalculoICMSST.AsCurr,
      eValorICMSST.AsCurr,
      FNotaFiscal.base_calculo_pis,
      FNotaFiscal.valor_pis,
      FNotaFiscal.base_calculo_cofins,
      FNotaFiscal.valor_cofins,
      eValorIPI.AsCurr,
      ePesoLiquido.AsDouble,
      ePesoBruto.AsDouble,
      FNotaFiscal.valor_recebido_dinheiro,
      FNotaFiscal.valor_recebido_cartao_cred,
      FNotaFiscal.valor_recebido_cartao_deb,
      FNotaFiscal.valor_recebido_credito,
      FNotaFiscal.valor_recebido_financeira,
      FNotaFiscal.valor_recebido_cobranca,
      FNotaFiscal.valor_recebido_cheque,
      FNotaFiscal.data_hora_emissao,
      FNotaFiscal.status,
      '', //FNotaFiscal.numero_recibo_lote_nfe,
      FNotaFiscal.protocolo_nfe,
      FNotaFiscal.data_hora_protocolo_nfe,
      FNotaFiscal.chave_nfe,
      FNotaFiscal.status_nfe,
      FNotaFiscal.motivo_status_nfe,
      FNotaFiscal.ProtocoloCancelamentoNFe,
      FNotaFiscal.MotivoCancelamentoNota,
      FNotaFiscal.enviou_email_nfe_cliente,
      FNotaFiscal.danfe_impresso,
      FNotaFiscal.NotaFiscalOrigemCupomId,
      FNotaFiscal.MovimentarEstOutrasNotas,
      eInformacoesComplementares.Lines.Text,
      nil,
      nil,
      FNotaFiscal.valor_recebido_pix,
      vTransportadoraId,
      eQuantidade.AsDouble,
      especieNFe,
      cbTipoFrete.GetValorInt
    );

  if vRetBanco.TeveErro then begin
    Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormEdicaoCapaNotaFiscal.sbAlterarEmpresaEmitenteClick(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<RecEmpresas>;
  vRetBanco: RecRetornoBD;
  empresas: TArray<RecEmpresas>;
  empresaAtualNota: TArray<RecParametrosEmpresa>;
  empresaNovaNota: TArray<RecParametrosEmpresa>;
begin
  inherited;

  if FNotaFiscal.numero_nota > 0 then begin
    Exclamar('N�o � permitido alterar a empresa emitente de uma nota fiscal que j� teve o n�mero gerado');
    Exit;
  end;

  vRetorno := BuscarDadosEmpresaEmissaoNota.BuscarDadosEmpresaEmitente;
  if vRetorno.BuscaCancelada then
    Exit;

  empresas := _Empresas.BuscarEmpresas(Sessao.getConexaoBanco, 0, [vRetorno.Dados.EmpresaId]);

  //Necess�rio verificar a mudan�a de regimes porque as vezes a nota tem ICMS e no novo emitente n�o vai ter
  empresaAtualNota := _ParametrosEmpresa.BuscarParametrosEmpresas(Sessao.getConexaoBanco, 0, [FNotaFiscal.empresa_id]);
  empresaNovaNota := _ParametrosEmpresa.BuscarParametrosEmpresas(Sessao.getConexaoBanco, 0, [empresas[0].EmpresaId]);

  if (empresaAtualNota[0].RegimeTributario = 'LP') and (empresaNovaNota[0].RegimeTributario <>  empresaAtualNota[0].RegimeTributario) then begin
    Informar(
      'ATEN��O! O regime tribut�rio da empresa da nota � diferente do regime da nova empresa.'+ #13#10 +
      'Antes de emitir a nota valide se a base e valor de icms est�o corretos.'
    );
  end;

  eEmpresaNota.Text := NFormat(empresas[0].EmpresaId) + ' - ' + empresas[0].RazaoSocial;
  FNotaFiscal.empresa_id := empresas[0].EmpresaId;
  FNotaFiscal.razao_social_emitente := empresas[0].RazaoSocial;
  FNotaFiscal.nome_fantasia_emitente := empresas[0].NomeFantasia;
  FNotaFiscal.cnpj_emitente := empresas[0].Cnpj;
  FNotaFiscal.inscricao_estadual_emitente  := empresas[0].InscricaoEstadual;
  FNotaFiscal.logradouro_emitente  := empresas[0].Logradouro;
  FNotaFiscal.complemento_emitente  := empresas[0].Complemento;
  FNotaFiscal.nome_bairro_emitente  := empresas[0].NomeBairro;
  FNotaFiscal.nome_cidade_emitente := empresas[0].NomeCidade;
  FNotaFiscal.numero_emitente := empresas[0].Numero;
  FNotaFiscal.estado_id_emitente  := empresas[0].EstadoId;
  FNotaFiscal.cep_emitente := empresas[0].Cep;
  FNotaFiscal.telefone_emitente  := empresas[0].TelefonePrincipal;
  FNotaFiscal.codigo_ibge_municipio_emit := empresas[0].CodigoIBGECidade;
  FNotaFiscal.codigo_ibge_estado_emitent := empresas[0].CodigoIBGEEstado;
end;

procedure TFormEdicaoCapaNotaFiscal.sbCarregarEnderecosClick(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<RecDiversosEnderecos>;
  bairros: TArray<RecBairros>;
  vRetBanco: RecRetornoBD;
begin
  inherited;

  vRetorno := Buscar.Enderecos.Buscar(FNotaFiscal.cadastro_id, False);
  if vRetorno.BuscaCancelada then
    Exit;

  eLogradouro.text := vRetorno.Dados.logradouro;
  eComplemento.text := vRetorno.Dados.complemento;
  eNumero.text := vRetorno.Dados.numero;
  eCEP.text := vRetorno.Dados.cep;
  eInscricaoEstadual.Text := vRetorno.Dados.InscricaoEstadual;

  bairros := _Bairros.BuscarBairros(Sessao.getConexaoBanco, 0, [vRetorno.Dados.bairro_id], 0);
  eBairro.Text := bairros[0].nome;
  eCidade.text := bairros[0].nome_cidade;
  eEstadoId.Text := bairros[0].EstadoId;

  FNotaFiscal.nome_cidade_destinatario := eCidade.text;
  FNotaFiscal.estado_id_destinatario := eEstadoId.Text;
  FNotaFiscal.codigo_ibge_municipio_dest := bairros[0].CodigoIbgeCidade;
  FNotaFiscal.InscricaoEstEntrDestinat := eInscricaoEstadual.Text;
  FNotaFiscal.LogradouroEntrDestinatario := eLogradouro.Text;
  FNotaFiscal.complementoEntrDestinatario := eComplemento.Text;
  FNotaFiscal.NomeBairroEntrDestinatario := eBairro.Text;
  FNotaFiscal.NomeCidadeEntrDestinatario := eCidade.text;
  FNotaFiscal.NumeroEntrDestinatario := eNumero.Text;
  FNotaFiscal.EstadoIdEntrDestinatario := eEstadoId.Text;
  FNotaFiscal.CepEntrDestinatario := eCEP.Text;
  FNotaFiscal.CodigoIbgeMunicEntrDest := bairros[0].CodigoIbgeCidade;
end;

procedure TFormEdicaoCapaNotaFiscal.SpeedButton1Click(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<RecClientes>;
  vRetBanco: RecRetornoBD;
begin
  inherited;

  vRetorno := BuscarDadosClienteEmissaoNota.BuscarDadosCliente;
  if vRetorno.BuscaCancelada then
    Exit;

  eNomeCliente.Text := vRetorno.Dados.RecCadastro.nome_fantasia;
  eLogradouro.text := vRetorno.Dados.RecCadastro.logradouro;
  eComplemento.text := vRetorno.Dados.RecCadastro.complemento;
  eNumero.text := vRetorno.Dados.RecCadastro.numero;
  eCEP.text := vRetorno.Dados.RecCadastro.cep;
  eInscricaoEstadual.Text := vRetorno.Dados.RecCadastro.inscricao_estadual;

  eBairro.Text := vRetorno.Dados.RecCadastro.NomeBairro;
  eCidade.text := vRetorno.Dados.RecCadastro.NomeCidade;
  eEstadoId.Text := vRetorno.Dados.RecCadastro.estado_id;

  FNotaFiscal.cadastro_id := vRetorno.Dados.cadastro_id;
  FNotaFiscal.nome_fantasia_destinatario := vRetorno.Dados.RecCadastro.nome_fantasia;
  FNotaFiscal.razao_social_destinatario := vRetorno.Dados.RecCadastro.razao_social;
  FNotaFiscal.cpf_cnpj_destinatario := vRetorno.Dados.RecCadastro.cpf_cnpj;
  FNotaFiscal.nome_cidade_destinatario := eCidade.text;
  FNotaFiscal.estado_id_destinatario := eEstadoId.Text;
  FNotaFiscal.codigo_ibge_municipio_dest := vRetorno.Dados.RecCadastro.CodigoIbgeCidade;
  FNotaFiscal.InscricaoEstEntrDestinat := eInscricaoEstadual.Text;
  FNotaFiscal.LogradouroEntrDestinatario := eLogradouro.Text;
  FNotaFiscal.complementoEntrDestinatario := eComplemento.Text;
  FNotaFiscal.NomeBairroEntrDestinatario := eBairro.Text;
  FNotaFiscal.NomeCidadeEntrDestinatario := eCidade.text;
  FNotaFiscal.NumeroEntrDestinatario := eNumero.Text;
  FNotaFiscal.EstadoIdEntrDestinatario := eEstadoId.Text;
  FNotaFiscal.CepEntrDestinatario := eCEP.Text;
  FNotaFiscal.CodigoIbgeMunicEntrDest := vRetorno.Dados.RecCadastro.CodigoIbgeCidade;
end;

procedure TFormEdicaoCapaNotaFiscal.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if (cbTipoFrete.GetValor = '9') and (not FrTransportadora.EstaVazio) then begin
    Exclamar('A transportadora foi definida. Por favor verifique o tipo de frete informado.');
    SetarFoco(cbTipoFrete);
    Abort;
  end;
end;

end.
