unit FrameNumeros;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.Grids, BuscaDados,
  GridLuka, _Biblioteca, Vcl.ExtCtrls;

type
  TFrNumeros = class(TFrameHerancaPrincipal)
    sgNumeros: TGridLuka;
    pnDescricao: TPanel;
    procedure sgNumerosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgNumerosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  private
    FOnAposAdicionar: TEventoObjeto;
    FOnAposDeletar: TEventoObjeto;

    procedure setValores(pValores: TArray<Integer>);
  public
    constructor Create(AOwner: TComponent); override;

    procedure Clear; override;
    procedure SetFocus; override;
    procedure Adicionar(pValor: Word);
    procedure SomenteLeitura(pValue: Boolean); override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;

    function getValores: TArray<Integer>;
    function EstaVazio: Boolean; override;

    function TrazerFiltros(pNomeColuna: string): string;
  published
    property OnAposAdicionar: TEventoObjeto read FOnAposAdicionar write FOnAposAdicionar;
    property OnAposDeletar: TEventoObjeto read FOnAposDeletar write FOnAposDeletar;
    property Valores: TArray<Integer> read getValores write setValores;
  end;

implementation

{$R *.dfm}

{ TFrNumeros }

const
  co_numeros = 0;

procedure TFrNumeros.Adicionar(pValor: Word);
begin
  if sgNumeros.Cells[co_numeros, 0] = '' then
    sgNumeros.Cells[co_numeros, 0] := IntToStr(pValor)
  else begin
    sgNumeros.RowCount := sgNumeros.RowCount + 1;
    sgNumeros.Cells[co_numeros, sgNumeros.RowCount - 1] := IntToStr(pValor);
    sgNumeros.Row := sgNumeros.RowCount - 1;
  end;

  if Assigned(FOnAposAdicionar) then
    FOnAposAdicionar(Self);
end;

procedure TFrNumeros.Clear;
begin
  inherited;
  sgNumeros.ClearGrid;
end;

constructor TFrNumeros.Create(AOwner: TComponent);
begin
  inherited;
  sgNumeros.ColWidths[co_numeros] := Self.Width - 45;
end;

function TFrNumeros.EstaVazio: Boolean;
begin
  inherited;
  Result := ( sgNumeros.Cells[co_numeros, 0] = '' );
end;

function TFrNumeros.getValores: TArray<Integer>;
var
  i: SmallInt;
begin
  if EstaVazio then
    Exit;

  SetLength(Result, sgNumeros.RowCount);
  for i := 0 to sgNumeros.RowCount - 1 do
    Result[i] := StrToInt( sgNumeros.Cells[co_numeros, i] );
end;

procedure TFrNumeros.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;
  sgNumeros.Enabled := pEditando;

  if pLimpar then
    sgNumeros.ClearGrid;
end;

procedure TFrNumeros.SetFocus;
begin
  inherited;
  sgNumeros.SetFocus;
end;

procedure TFrNumeros.setValores(pValores: TArray<Integer>);
var
  i: Integer;
begin
  for i := Low(pValores) to High(pValores) do
    Adicionar(pValores[i]);
end;

procedure TFrNumeros.sgNumerosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  inherited;
  sgNumeros.MergeCells([ARow, ACOl], [ARow, ACOl], [ARow, ACOl], taRightJustify, Rect);
end;

procedure TFrNumeros.sgNumerosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  i: Byte;
  valor: Integer;
begin
  inherited;
  if Key in[VK_NUMPAD0..VK_NUMPAD9] then begin
    valor := StrToIntDef( BuscaDados.BuscarDados(tiNumerico, 'Valor', IntToStr(Key - 96)), 0 );

    for i := 0 to sgNumeros.RowCount - 1 do begin
      if IntToStr(valor) = sgNumeros.Cells[co_numeros, i] then
        Exit;
    end;

    if sgNumeros.Cells[co_numeros, 0] = '' then
      sgNumeros.Cells[co_numeros, 0] := IntToStr(valor)
    else begin
      sgNumeros.RowCount := sgNumeros.RowCount + 1;
      sgNumeros.Cells[co_numeros, sgNumeros.RowCount - 1] := IntToStr(valor);
      sgNumeros.Row := sgNumeros.RowCount - 1;
    end;

    if Assigned(FOnAposAdicionar) then
      FOnAposAdicionar(Sender);

    sgNumeros.SetFocus;
  end
  else if Key = VK_DELETE then begin
    sgNumeros.DeleteRow(sgNumeros.Row);
    if Assigned(FOnAposDeletar) then
      FOnAposDeletar(Sender);
  end;
end;

procedure TFrNumeros.SomenteLeitura(pValue: Boolean);
begin
  inherited;
  sgNumeros.EditorMode := not pValue;
end;

function TFrNumeros.TrazerFiltros(pNomeColuna: string): string;
begin
  Result := _Biblioteca.FiltroInInt(pNomeColuna, getValores);
end;

end.
