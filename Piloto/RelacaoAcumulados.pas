unit RelacaoAcumulados;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _Biblioteca, _Sessao,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, FrameProdutos, FrameVendedores, _Acumulados,
  Frame.Inteiros, CheckBoxLuka, Vcl.StdCtrls, GroupBoxLuka, _AcumuladosItens, ImpressaoMeiaPaginaDuplicataMercantil,
  FrameCondicoesPagamento, FrameDataInicialFinal, FrameEmpresas, InformacoesAcumulado,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameClientes, Vcl.Grids,
  GridLuka, StaticTextLuka, Vcl.Menus, ImpressaoComprovantePagamentoAcumulado, BuscarIndiceDescontoVenda,
  Data.DB, frxClass, frxDBSet, Datasnap.DBClient, MemDS, DBAccess, Ora, OraCall;

type
  TFormRelacaoAcumulados = class(TFormHerancaRelatoriosPageControl)
    FrClientes: TFrClientes;
    FrEmpresas: TFrEmpresas;
    FrDataFechamento: TFrDataInicialFinal;
    FrCondicoesPagamento: TFrCondicoesPagamento;
    gbFormasPagamento: TGroupBoxLuka;
    ckCartao: TCheckBox;
    ckFinanceira: TCheckBox;
    ckCheque: TCheckBox;
    ckCobranca: TCheckBox;
    ckDinheiro: TCheckBoxLuka;
    FrPedido: TFrameInteiros;
    FrCodigoTurno: TFrameInteiros;
    FrVendedores: TFrVendedores;
    FrDataRecebimento: TFrDataInicialFinal;
    FrProdutos: TFrProdutos;
    gbStatus: TGroupBoxLuka;
    ckStatusEmAberto: TCheckBox;
    ckStatusRecebido: TCheckBox;
    FrAcumulado: TFrameInteiros;
    StaticTextLuka1: TStaticTextLuka;
    sgAcumulados: TGridLuka;
    splSeparador: TSplitter;
    StaticTextLuka2: TStaticTextLuka;
    sgItens: TGridLuka;
    pmOpcoesAcumulados: TPopupMenu;
    miN1: TMenuItem;
    miAlterarIndiceDescontoVenda: TMenuItem;
    Panel1: TPanel;
    miReimprimirComprovantePagamento: TSpeedButton;
    miReimprimirDuplicatasMercantil: TSpeedButton;
    frxReport: TfrxReport;
    qAcumulados: TOraQuery;
    qAcumuladosACUMULADO_ID: TFloatField;
    qAcumuladosCONDICAO_ID: TIntegerField;
    qAcumuladosNOME_CONDICAO_PAGAMENTO: TStringField;
    qAcumuladosDATA_HORA_FECHAMENTO: TDateTimeField;
    qAcumuladosDATA_HORA_RECEBIMENTO: TDateTimeField;
    qAcumuladosSTATUS: TStringField;
    qAcumuladosVALOR_TOTAL: TFloatField;
    dstAcumulados: TfrxDBDataset;
    dsClientesAcumulados: TDataSource;
    qClientes: TOraQuery;
    qAcumuladosCLIENTE_ID: TFloatField;
    frxClientes: TfrxDBDataset;
    qClientesCLIENTE_ID: TFloatField;
    qClientesNOME_FANTASIA: TStringField;
    dsItensAcumulados: TDataSource;
    qItensAcumulados: TOraQuery;
    dstItensAcumulados: TfrxDBDataset;
    qItensAcumuladosACUMULADO_ID: TFloatField;
    qItensAcumuladosPRODUTO_ID: TFloatField;
    qItensAcumuladosNOME_PRODUTO: TStringField;
    qItensAcumuladosPRECO_UNITARIO: TFloatField;
    qItensAcumuladosQUANTIDADE: TFloatField;
    qItensAcumuladosVALOR_TOTAL: TFloatField;
    qItensAcumuladosVALOR_TOTAL_DESCONTO: TFloatField;
    qItensAcumuladosVALOR_TOTAL_OUTRAS_DESPESAS: TFloatField;
    qItensAcumuladosVALOR_TOTAL_FRETE: TFloatField;
    qItensAcumuladosMARCA_ID: TIntegerField;
    qItensAcumuladosNOME_MARCA: TStringField;
    qItensAcumuladosUNIDADE_VENDA: TStringField;
    qItensAcumuladosPRECO_FINAL: TFloatField;
    qItensAcumuladosPRECO_FINAL_MEDIO: TFloatField;
    qItensAcumuladosPRECO_LIQUIDO: TFloatField;
    qItensAcumuladosPRECO_LIQUIDO_MEDIO: TFloatField;
    qItensAcumuladosCMV: TFloatField;
    qItensAcumuladosCUSTO_ULTIMO_PEDIDO: TFloatField;
    qItensAcumuladosCUSTO_PEDIDO_MEDIO: TFloatField;
    qItensAcumuladosVALOR_IMPOSTOS: TFloatField;
    qItensAcumuladosVALOR_ENCARGOS: TFloatField;
    qItensAcumuladosVALOR_CUSTO_VENDA: TFloatField;
    qItensAcumuladosVALOR_CUSTO_FINAL: TFloatField;
    qAcumuladosVALOR_OUTRAS_DESPESAS: TFloatField;
    qAcumuladosVALOR_DESCONTO: TFloatField;
    qAcumuladosVALOR_JUROS: TFloatField;
    qAcumuladosVALOR_TOTAL_PRODUTOS: TFloatField;
    ckPix: TCheckBox;
    qClientesRAZAO_SOCIAL: TStringField;
    procedure sgAcumuladosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgAcumuladosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure FormShow(Sender: TObject);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgAcumuladosClick(Sender: TObject);
    procedure sgAcumuladosDblClick(Sender: TObject);
    procedure miReimprimirComprovantePagamentoClick(Sender: TObject);
    procedure miAlterarIndiceDescontoVendaClick(Sender: TObject);
    procedure miReimprimirDuplicatasMercantilClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FItens: TArray<RecAcumuladosItens>;
    vAcumulados: TArray<RecAcumulados>;
    vComando: string;
    vComandoClientesPadrao: string;
    vComandoAcumuladosPadrao: string;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

const
  coAcumuladoId      = 0;
  coCliente          = 1;
  coStatusAnalitico  = 2;
  coDataHoraFecham   = 3;
  coDataHoraRec      = 4;
  coCondicaoPagto    = 5;
  coValorProdutos    = 6;
  coValorTotal       = 7;
  coValorOutDesp     = 8;
  coValorDesconto    = 9;
  coValorJuros       = 10;
  coValorLucroBruto  = 11;
  coPercLucroBruto   = 12;
  coValorLucroLiq    = 13;
  coPercLucroLiquido = 14;
  coEmpresa          = 15;
  (* Ocultas *)
  coIndiceDescVenda  = 16;
  coTipoLinha        = 17;
  coStatus           = 18;

  (* Grid de produto *)
  cpProdutoId         = 0;
  cpNome              = 1;
  cpMarca             = 2;
  cpPrecoUnitario     = 3;
  cpQuantidade        = 4;
  cpUnidade           = 5;
  cpValorTotal        = 6;
  cpValorOutDesp      = 7;
  cpValorDesconto     = 8;
  cpValorLucroBruto   = 9;
  cpPercLucroBruto    = 10;
  cpValorLucroLiquido = 11;
  cpPercLucroLiquido  = 12;

  coLinhaDetalhe  = 'D';
  coLinhaTotVenda = 'TV';

{ TFormRelacaoAcumulados }

procedure TFormRelacaoAcumulados.Carregar(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vAcumuladosIds: TArray<Integer>;
  vSqlFormasPagto: string;
  vSqlStatus: string;

  vTotais: record
    totalProdutos: Double;
    totalOutrasDespesas: Double;
    totalDesconto: Double;
    valorTotal: Double;
    ValorLucroBruto: Double;
    PercLucroBruto: Double;
    ValorLucroLiquido: Double;
    PercLucroLiquido: Double;
  end;

  procedure SqlFormaPagamento(pColuna: string; pCheckBox: TCheckBox);
  begin
    if pCheckBox.Checked then
      AddOrSeNecessario(vSqlFormasPagto, ' ' + pColuna + ' > 0');
  end;

  procedure SqlStatus(pValor: string; pCheckBox: TCheckBox);
  begin
    if pCheckBox.Checked then
      AddVirgulaSeNecessario(vSqlStatus, '''' + pValor + '''');
  end;

begin
  inherited;

  vComando := '';
  if not FrPedido.EstaVazio then begin
    _Biblioteca.WhereOuAnd(
      vComando,
      'ACU.ACUMULADO_ID in (select distinct ACUMULADO_ID from ACUMULADOS_ORCAMENTOS where ' + FrPedido.getSqlFiltros('ORCAMENTO_ID') + ') '
    );
  end
  else begin
    if not FrCodigoTurno.EstaVazio then
      _Biblioteca.WhereOuAnd(vComando, FrCodigoTurno.getSqlFiltros('ACU.TURNO_ID'));

    if not FrEmpresas.EstaVazio then
      _Biblioteca.WhereOuAnd(vComando, FrEmpresas.getSqlFiltros('ACU.EMPRESA_ID'));

    if not FrClientes.EstaVazio then
      _Biblioteca.WhereOuAnd(vComando, FrClientes.getSqlFiltros('ACU.CLIENTE_ID'));

    if not FrCondicoesPagamento.EstaVazio then
      _Biblioteca.WhereOuAnd(vComando, FrCondicoesPagamento.getSqlFiltros('ACU.CONDICAO_ID'));

    if not FrDataFechamento.NenhumaDataValida then
      _Biblioteca.WhereOuAnd(vComando, FrDataFechamento.getSqlFiltros('trunc(ACU.DATA_HORA_FECHAMENTO)'));

  //  if not FrVendedores.EstaVazio then
  //    _Biblioteca.WhereOuAnd(vComando, FrVendedores.TrazerFiltros('VENDEDOR_ID'));

    if not FrDataRecebimento.NenhumaDataValida then
      _Biblioteca.WhereOuAnd(vComando, FrDataRecebimento.getSqlFiltros('trunc(ACU.DATA_HORA_RECEBIMENTO)'));

    if not FrProdutos.EstaVazio then begin
      _Biblioteca.WhereOuAnd(
        vComando,
        'ACU.ACUMULADO_ID in(select distinct ACUMULADO_ID from ACUMULADOS_ITENS where ' + FiltroInInt('PRODUTO_ID', FrProdutos.GetArrayOfInteger) + ') '
      );
    end;

    if not FrAcumulado.EstaVazio then
      _Biblioteca.WhereOuAnd(vComando, FrAcumulado.getSqlFiltros('ACU.ACUMULADO_ID'));

    SqlFormaPagamento('VALOR_DINHEIRO', ckDinheiro);
    SqlFormaPagamento('VALOR_PIX', ckPix);
    SqlFormaPagamento('VALOR_CHEQUE', ckCheque);
    SqlFormaPagamento('VALOR_CARTAO_DEBITO', ckCartao);
    SqlFormaPagamento('VALOR_CARTAO_CREDITO', ckCartao);
    SqlFormaPagamento('VALOR_FINANCEIRA', ckFinanceira);
    SqlFormaPagamento('VALOR_COBRANCA + VALOR_CREDITO', ckCobranca);
    vComando := vComando + ' and ( ' + vSqlFormasPagto + ' )';

    SqlStatus('AR', ckStatusEmAberto);
    SqlStatus('RE', ckStatusRecebido);
    vComando := vComando + ' and ACU.STATUS in( ' + vSqlStatus + ') ';
  end;

  vComando := vComando + ' order by ACU.ACUMULADO_ID';

  vAcumulados := _Acumulados.BuscarAcumuladosComando(Sessao.getConexaoBanco, vComando);
  if vAcumulados = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vTotais.totalProdutos := 0;
  vTotais.totalOutrasDespesas := 0;
  vTotais.totalDesconto := 0;
  vTotais.valorTotal := 0;
  vTotais.ValorLucroBruto := 0;
  vTotais.PercLucroBruto := 0;
  vTotais.ValorLucroLiquido := 0;
  vTotais.PercLucroLiquido := 0;

  vLinha := 1;
  for i := Low(vAcumulados) to High(vAcumulados) do begin
    sgAcumulados.Cells[coAcumuladoId, vLinha]      := NFormat(vAcumulados[i].AcumuladoId);
    sgAcumulados.Cells[coCliente, vLinha]          := getInformacao(vAcumulados[i].ClienteId, vAcumulados[i].NomeCliente);
    sgAcumulados.Cells[coStatusAnalitico, vLinha]  := vAcumulados[i].StatusAnalitico;
    sgAcumulados.Cells[coDataHoraFecham, vLinha]   := DHFormat(vAcumulados[i].DataHoraFechamento);
    sgAcumulados.Cells[coDataHoraRec, vLinha]   := DHFormatN(vAcumulados[i].DataHoraRecebimento);
    sgAcumulados.Cells[coCondicaoPagto, vLinha]    := getInformacao(vAcumulados[i].CondicaoId, vAcumulados[i].NomeCondicaoPagamento);
    sgAcumulados.Cells[coValorProdutos, vLinha]    := NFormatN(vAcumulados[i].ValorTotalProdutos);
    sgAcumulados.Cells[coValorTotal, vLinha]       := NFormatN(vAcumulados[i].ValorTotal);
    sgAcumulados.Cells[coValorOutDesp, vLinha]     := NFormatN(vAcumulados[i].ValorOutrasDespesas);
    sgAcumulados.Cells[coValorDesconto, vLinha]    := NFormatN(vAcumulados[i].ValorDesconto);
    sgAcumulados.Cells[coValorLucroBruto, vLinha]  := NFormatN(vAcumulados[i].ValorTotal - vAcumulados[i].ValorCustoEntrada);
    sgAcumulados.Cells[coValorLucroLiq, vLinha]    := NFormatN(vAcumulados[i].ValorTotal - vAcumulados[i].ValorCustoFinal);
    sgAcumulados.Cells[coPercLucroBruto, vLinha]   := NFormatN(Sessao.getCalculosSistema.CalcOrcamento.getPercLucroBruto(vAcumulados[i].ValorTotal, vAcumulados[i].ValorTotal - vAcumulados[i].ValorCustoEntrada, 4), 4);
    sgAcumulados.Cells[coPercLucroLiquido, vLinha] := NFormatN(Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(vAcumulados[i].ValorTotal, vAcumulados[i].ValorTotal - vAcumulados[i].ValorCustoFinal, 4), 4);
    sgAcumulados.Cells[coEmpresa, vLinha]          := getInformacao(vAcumulados[i].EmpresaId, vAcumulados[i].NomeEmpresa);
    sgAcumulados.Cells[coIndiceDescVenda, vLinha]  := NFormatN(vAcumulados[i].IndiceDescontoVendaId);
    sgAcumulados.Cells[coStatus, vLinha]           := vAcumulados[i].Status;
    sgAcumulados.Cells[coTipoLinha, vLinha]        := coLinhaDetalhe;

    vTotais.totalProdutos       := vTotais.totalProdutos + vAcumulados[i].ValorTotalProdutos;
    vTotais.totalOutrasDespesas := vTotais.totalOutrasDespesas + vAcumulados[i].ValorOutrasDespesas;
    vTotais.totalDesconto       := vTotais.totalDesconto + vAcumulados[i].ValorDesconto;
    vTotais.valorTotal          := vTotais.valorTotal + vAcumulados[i].ValorTotal;
    vTotais.ValorLucroBruto     := vTotais.ValorLucroBruto + SFormatDouble( sgAcumulados.Cells[coValorLucroBruto, vLinha] );
    vTotais.ValorLucroLiquido   := vTotais.ValorLucroLiquido + SFormatDouble( sgAcumulados.Cells[coValorLucroLiq, vLinha] );
    vTotais.PercLucroBruto      := vTotais.PercLucroBruto + SFormatDouble( sgAcumulados.Cells[coPercLucroBruto, vLinha] );
    vTotais.PercLucroLiquido    := vTotais.PercLucroLiquido + SFormatDouble( sgAcumulados.Cells[coPercLucroLiquido, vLinha] );

    _Biblioteca.AddNoVetorSemRepetir(vAcumuladosIds, vAcumulados[i].AcumuladoId);
    Inc(vLinha);
  end;

  sgAcumulados.Cells[coCondicaoPagto, vLinha]    := 'TOTAIS: ';
  sgAcumulados.Cells[coValorProdutos, vLinha]    := NFormatN(vTotais.totalProdutos);
  sgAcumulados.Cells[coValorOutDesp, vLinha]     := NFormatN(vTotais.totalOutrasDespesas);
  sgAcumulados.Cells[coValorDesconto, vLinha]    := NFormatN(vTotais.totalDesconto);
  sgAcumulados.Cells[coValorTotal, vLinha]       := NFormatN(vTotais.valorTotal);
  sgAcumulados.Cells[coValorLucroBruto, vLinha]  := NFormatN(vTotais.ValorLucroBruto);
  sgAcumulados.Cells[coValorLucroLiq, vLinha]    := NFormatN(vTotais.ValorLucroLiquido);
  sgAcumulados.Cells[coPercLucroBruto, vLinha]   := NFormatN( Sessao.getCalculosSistema.CalcOrcamento.getPercLucroBruto(vTotais.valorTotal, vTotais.ValorLucroBruto, 4), 4 );
  sgAcumulados.Cells[coPercLucroLiquido, vLinha] := NFormatN( Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(vTotais.valorTotal, vTotais.ValorLucroLiquido,4 ), 4);

  sgAcumulados.Cells[coTipoLinha, vLinha]     := coLinhaTotVenda;

  Inc(vLinha);
  sgAcumulados.RowCount := vLinha;

  FItens := _AcumuladosItens.BuscarAcumuladosItensComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('ITE.ACUMULADO_ID', vAcumuladosIds) + ' order by PRO.NOME ');
  sgAcumuladosClick(sgAcumulados);
  SetarFoco(sgAcumulados);
end;

procedure TFormRelacaoAcumulados.FormCreate(Sender: TObject);
begin
  inherited;
  qAcumulados.Session := Sessao.getConexaoBanco;
  qClientes.Session := Sessao.getConexaoBanco;
  vComandoClientesPadrao := qClientes.SQL.Text;
  vComandoAcumuladosPadrao := qAcumulados.SQL.Text;
end;

procedure TFormRelacaoAcumulados.FormShow(Sender: TObject);
begin
  inherited;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId, True);
end;

procedure TFormRelacaoAcumulados.Imprimir(Sender: TObject);
var
  vCnt: Integer;
  vClientes: TArray<Integer>;
  vClientesComandoFinal: string;
  totalAcumulado: Double;
begin
  inherited;
  if vAcumulados = nil then begin
    NenhumRegistro;
    Exit;
  end;

  totalAcumulado := 0.0;

  qAcumulados.SQL.Text := vComandoAcumuladosPadrao;
  qAcumulados.SQL.Add(' ' +  vComando);

  totalAcumulado := SFormatDouble(sgAcumulados.Cells[coValorTotal, sgAcumulados.RowCount - 1]);

  for vCnt := Low(vAcumulados) to High(vAcumulados) do
    _Biblioteca.AddNoVetorSemRepetir(vClientes, vAcumulados[vCnt].ClienteId);

  qClientes.SQL.Text := vComandoClientesPadrao;
  qClientes.Close;
  vClientesComandoFinal := ' ' + FiltroInInt('ACU.CLIENTE_ID', vClientes);
  qClientes.SQL.Add( ' and ' + vClientesComandoFinal);
  qClientes.Open;

  TfrxMemoView(frxReport.FindComponent('mmValorProduto')).Text :=   'Valor Total.....: ' + NFormat(totalAcumulado);
  frxReport.ShowReport;
end;

procedure TFormRelacaoAcumulados.miAlterarIndiceDescontoVendaClick(Sender: TObject);
var
  vAcumuladoId: Integer;
  vIndiceDescId: Integer;
  vRetTela: TRetornoTelaFinalizar<Integer>;
begin
  inherited;
  if not Sessao.AutorizadoRotina('definir_indice_desconto_acumulado') then begin
    _Biblioteca.NaoAutorizadoRotina;
    Exit;
  end;

  vAcumuladoId  := SFormatInt(sgAcumulados.Cells[coAcumuladoId, sgAcumulados.Row]);
  vIndiceDescId := SFormatInt(sgAcumulados.Cells[coIndiceDescVenda, sgAcumulados.Row]);

  vRetTela := BuscarIndiceDescontoVenda.Buscar(vAcumuladoId, 'ACU', vIndiceDescId);

  if vRetTela.BuscaCancelada then
    Exit;

  sgAcumulados.Cells[coIndiceDescVenda, sgAcumulados.Row] := NFormat(vIndiceDescId);
  _Biblioteca.RotinaSucesso;
end;

procedure TFormRelacaoAcumulados.miReimprimirComprovantePagamentoClick(Sender: TObject);
begin
  inherited;
  ImpressaoComprovantePagamentoAcumulado.Imprimir( SFormatInt(sgAcumulados.Cells[coAcumuladoId, sgAcumulados.Row]) );
end;

procedure TFormRelacaoAcumulados.miReimprimirDuplicatasMercantilClick(Sender: TObject);
begin
  inherited;
  ImpressaoMeiaPaginaDuplicataMercantil.Emitir( SFormatInt(sgAcumulados.Cells[coAcumuladoId, sgAcumulados.Row]), tpAcumulado);
end;

procedure TFormRelacaoAcumulados.sgAcumuladosClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
begin
  inherited;
  sgItens.ClearGrid;
  if sgAcumulados.Cells[coAcumuladoId, 1] = '' then
    Exit;

  vLinha := 0;
  for i := Low(FItens) to High(FItens) do begin
    if SFormatInt(sgAcumulados.Cells[coAcumuladoId, sgAcumulados.Row]) <> FItens[i].AcumuladoId then
      Continue;

    Inc(vLinha);

    sgItens.Cells[cpProdutoId, vLinha]         := NFormat(FItens[i].ProdutoId);
    sgItens.Cells[cpNome, vLinha]              := FItens[i].NomeProduto;
    sgItens.Cells[cpMarca, vLinha]             := getInformacao(FItens[i].MarcaId, FItens[i].NomeMarca);
    sgItens.Cells[cpPrecoUnitario, vLinha]     := NFormat(FItens[i].PrecoUnitario);
    sgItens.Cells[cpQuantidade, vLinha]        := NFormatEstoque(FItens[i].Quantidade);
    sgItens.Cells[cpUnidade, vLinha]           := FItens[i].UnidadeVenda;
    sgItens.Cells[cpValorTotal, vLinha]        := NFormat(FItens[i].ValorTotal);
    sgItens.Cells[cpValorOutDesp, vLinha]      := NFormatN(FItens[i].ValorTotalOutrasDespesas);
    sgItens.Cells[cpValorDesconto, vLinha]     := NFormatN(FItens[i].ValorTotalDesconto);
    sgItens.Cells[cpValorLucroBruto, vLinha]   := NFormat(FItens[i].ValorTotal - FItens[i].ValorTotalDesconto - FItens[i].ValorCustoEntrada);
    sgItens.Cells[cpValorLucroLiquido, vLinha] := NFormat(FItens[i].ValorTotal - FItens[i].ValorTotalDesconto - FItens[i].ValorCustoFinal);
    sgItens.Cells[cpPercLucroBruto, vLinha]    := NFormatN(Sessao.getCalculosSistema.CalcOrcamento.getPercLucroBruto(FItens[i].ValorTotal - FItens[i].ValorTotalDesconto, FItens[i].ValorTotal - FItens[i].ValorTotalDesconto - FItens[i].ValorCustoEntrada, 4), 4);
    sgItens.Cells[cpPercLucroLiquido, vLinha]  := NFormatN(Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(FItens[i].ValorTotal - FItens[i].ValorTotalDesconto, FItens[i].ValorTotal - FItens[i].ValorTotalDesconto - FItens[i].ValorCustoFinal, 4), 4);
  end;
  sgItens.SetLinhasGridPorTamanhoVetor( vLinha );
end;

procedure TFormRelacaoAcumulados.sgAcumuladosDblClick(Sender: TObject);
begin
  inherited;
  InformacoesAcumulado.Informar( SFormatInt(sgAcumulados.Cells[coAcumuladoId, sgAcumulados.Row]) );
end;

procedure TFormRelacaoAcumulados.sgAcumuladosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coAcumuladoId, coValorProdutos, coValorOutDesp, coValorDesconto, coValorJuros, coValorTotal, coValorLucroBruto, coValorLucroLiq, coPercLucroBruto, coPercLucroLiquido] then
    vAlinhamento := taRightJustify
  else if ACol = coStatusAnalitico then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgAcumulados.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoAcumulados.sgAcumuladosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coPercLucroBruto then
    AFont.Color := _Sessao.Sessao.getCorLucro( SFormatDouble( sgAcumulados.Cells[coPercLucroBruto, ARow] ) )
  else if ACol = coPercLucroLiquido then
    AFont.Color := _Sessao.Sessao.getCorLucro( SFormatDouble( sgAcumulados.Cells[coPercLucroLiquido, ARow] ) );

  if sgAcumulados.Cells[coTipoLinha, ARow] = coLinhaTotVenda then begin
    AFont.Color := clWhite;
    AFont.Style := [fsBold];
    ABrush.Color := $000096DB;
  end
  else if ACol = coStatusAnalitico then begin
    AFont.Style := [fsBold];
    AFont.Color := IIf(sgAcumulados.Cells[coStatus, ARow] = 'RE', clBlue, coCorFonteEdicao3);
  end;
end;

procedure TFormRelacaoAcumulados.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[
    cpProdutoId,
    cpPrecoUnitario,
    cpQuantidade,
    cpValorTotal,
    cpValorLucroBruto,
    cpValorLucroLiquido,
    cpPercLucroBruto,
    cpPercLucroLiquido]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoAcumulados.VerificarRegistro(Sender: TObject);
begin
  inherited;
  FItens := nil;
  sgAcumulados.ClearGrid();
  sgItens.ClearGrid();
end;

end.
