unit BuscarDadosParceiro;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameProfissionais,
  Vcl.Buttons, Vcl.ExtCtrls, _Biblioteca, _RecordsCadastros, _Profissionais;

type
  TFormBuscarDadosParceiros = class(TFormHerancaFinalizar)
    FrProfissional: TFrProfissionais;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function BuscarDadosParceiroVenda(parceiroId: Integer): TRetornoTelaFinalizar<RecProfissionais>;

implementation

{$R *.dfm}

function BuscarDadosParceiroVenda(parceiroId: Integer): TRetornoTelaFinalizar<RecProfissionais>;
var
  vForm: TFormBuscarDadosParceiros;
begin
  vForm := TFormBuscarDadosParceiros.Create(Application);

  if parceiroId > 0 then
    vForm.FrProfissional.InserirDadoPorChave(parceiroId);

  if Result.Ok(vForm.ShowModal) then
    Result.Dados := vForm.FrProfissional.GetDado(0);

  FreeAndNil(vForm);
end;

end.
