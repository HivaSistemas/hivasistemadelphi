inherited FormInformacoesEntradaNotaFiscal: TFormInformacoesEntradaNotaFiscal
  Caption = 'Informa'#231#245'es da entrada de nota fiscal'
  ClientHeight = 571
  ClientWidth = 920
  OnShow = FormShow
  ExplicitWidth = 926
  ExplicitHeight = 600
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 4
    Top = 3
    Width = 37
    Height = 14
    Caption = 'C'#243'digo'
  end
  object lbl1: TLabel [1]
    Left = 89
    Top = 3
    Width = 61
    Height = 14
    Caption = 'Fornecedor'
  end
  object lbl2: TLabel [2]
    Left = 470
    Top = 3
    Width = 42
    Height = 14
    Caption = 'Nr. nota'
  end
  object lbl3: TLabel [3]
    Left = 604
    Top = 3
    Width = 28
    Height = 14
    Caption = 'S'#233'rie'
  end
  object lbl4: TLabel [4]
    Left = 537
    Top = 3
    Width = 42
    Height = 14
    Caption = 'Modelo'
  end
  object lbl27: TLabel [5]
    Left = 305
    Top = 3
    Width = 47
    Height = 14
    Caption = 'Empresa'
  end
  inherited pnOpcoes: TPanel
    Top = 534
    Width = 920
    ExplicitTop = 534
    ExplicitWidth = 920
  end
  object eEntradaId: TEditLuka
    Left = 4
    Top = 17
    Width = 80
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 1
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eFornecedor: TEditLuka
    Left = 89
    Top = 17
    Width = 211
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 2
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eNumeroNota: TEditLuka
    Left = 470
    Top = 17
    Width = 62
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 3
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eSerieNota: TEditLuka
    Left = 604
    Top = 17
    Width = 53
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 4
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eModeloNota: TEditLuka
    Left = 537
    Top = 17
    Width = 62
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 5
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eEmpresa: TEditLuka
    Left = 305
    Top = 17
    Width = 161
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 6
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object stStatus: TStaticText
    Left = 663
    Top = 23
    Width = 254
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Ag. chegada das mercadorias'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = 33023
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 7
    Transparent = False
  end
  object StaticTextLuka2: TStaticTextLuka
    Left = 663
    Top = 7
    Width = 254
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Status'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 8
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object pcInfo: TPageControl
    Left = 3
    Top = 41
    Width = 914
    Height = 492
    ActivePage = tsProdutos
    TabOrder = 9
    object tsPrincipais: TTabSheet
      Caption = 'Principais'
      object lb4: TLabel
        Left = 3
        Top = 0
        Width = 118
        Height = 14
        Caption = 'Chave de acesso NF-e'
      end
      object lb5: TLabel
        Left = 414
        Top = 0
        Width = 72
        Height = 14
        Caption = 'Data entrada'
      end
      object lb6: TLabel
        Left = 325
        Top = 0
        Width = 76
        Height = 14
        Caption = 'Data emiss'#227'o'
      end
      object lb7: TLabel
        Left = 3
        Top = 42
        Width = 56
        Height = 14
        Caption = 'Base ICMS'
      end
      object lb8: TLabel
        Left = 77
        Top = 42
        Width = 57
        Height = 14
        Caption = 'Valor ICMS'
      end
      object lb9: TLabel
        Left = 145
        Top = 42
        Width = 71
        Height = 14
        Caption = 'Base ICMS ST'
      end
      object lb10: TLabel
        Left = 221
        Top = 41
        Width = 72
        Height = 14
        Caption = 'Valor ICMS ST'
      end
      object lb11: TLabel
        Left = 299
        Top = 42
        Width = 79
        Height = 14
        Caption = 'Total produtos'
      end
      object lb12: TLabel
        Left = 471
        Top = 42
        Width = 69
        Height = 14
        Caption = 'Outras desp.'
      end
      object lb14: TLabel
        Left = 545
        Top = 43
        Width = 45
        Height = 14
        Caption = 'Valor IPI'
      end
      object lb15: TLabel
        Left = 601
        Top = 42
        Width = 74
        Height = 14
        Caption = 'Valor do frete'
      end
      object lb16: TLabel
        Left = 381
        Top = 42
        Width = 81
        Height = 14
        Caption = 'Valor desconto'
      end
      object lb17: TLabel
        Left = 689
        Top = 42
        Width = 102
        Height = 14
        Caption = 'Valor total da nota'
      end
      object lbl8: TLabel
        Left = 504
        Top = 0
        Width = 69
        Height = 14
        Caption = 'Data 1'#186' parc.'
      end
      object lbl9: TLabel
        Left = 2
        Top = 85
        Width = 96
        Height = 14
        Caption = 'Modalidade frete'
      end
      object lblTipoMovimento: TLabel
        Left = 209
        Top = 85
        Width = 124
        Height = 14
        Caption = 'Conhecimento de frete'
      end
      object sbInformacoesConhecimento: TSpeedButtonLuka
        Left = 354
        Top = 105
        Width = 15
        Height = 16
        Hint = 'Abrir informa'#231#245'es do conhecimento de frete'
        Caption = '...'
        Flat = True
        OnClick = sbInformacoesConhecimentoClick
        TagImagem = 0
        PedirCertificacao = False
        PermitirAutOutroUsuario = False
      end
      object eBaseCalculoICMS: TEditLuka
        Left = 3
        Top = 57
        Width = 66
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 0
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorICMS: TEditLuka
        Left = 77
        Top = 57
        Width = 62
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 1
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eBaseCalculoICMSST: TEditLuka
        Left = 147
        Top = 57
        Width = 67
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 2
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorICMSST: TEditLuka
        Left = 222
        Top = 57
        Width = 68
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 3
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorTotalProdutos: TEditLuka
        Left = 298
        Top = 57
        Width = 75
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 4
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eOutrasDespesas: TEditLuka
        Left = 470
        Top = 57
        Width = 65
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 5
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorIPI: TEditLuka
        Left = 543
        Top = 57
        Width = 48
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 6
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorFrete: TEditLuka
        Left = 601
        Top = 57
        Width = 79
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 7
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eDataContabil: TEditLukaData
        Left = 414
        Top = 15
        Width = 82
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        TabOrder = 8
        Text = '  /  /    '
      end
      object eDataEmissaoNota: TEditLukaData
        Left = 325
        Top = 15
        Width = 81
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        TabOrder = 9
        Text = '  /  /    '
      end
      object eValorDesconto: TEditLuka
        Left = 381
        Top = 57
        Width = 83
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 10
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorTotalNota: TEditLuka
        Left = 689
        Top = 57
        Width = 100
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 11
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eChaveAcessoNFe: TMaskEdit
        Left = 3
        Top = 15
        Width = 299
        Height = 22
        EditMask = '9999.9999.9999.9999.9999.9999.9999.9999.9999.9999.9999;1;_'
        MaxLength = 54
        TabOrder = 12
        Text = '    .    .    .    .    .    .    .    .    .    .    '
      end
      object eDataPrimeiraParcela: TEditLukaData
        Left = 504
        Top = 15
        Width = 85
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        TabOrder = 13
        Text = '  /  /    '
      end
      object cbModalidadeFrete: TComboBoxLuka
        Left = 3
        Top = 99
        Width = 200
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = 16645629
        ItemIndex = 0
        TabOrder = 14
        Text = '0 - Por conta do emitente'
        Items.Strings = (
          '0 - Por conta do emitente'
          '1 - Por conta do dest./rem.'
          '2 - Por conta de terceiros'
          '9 - Sem frete')
        Valores.Strings = (
          '0'
          '1'
          '2'
          '9')
        AsInt = 0
        AsString = '0'
      end
      object eConhecimentoId: TEditLuka
        Left = 209
        Top = 99
        Width = 62
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 15
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eValorConhecimento: TEditLuka
        Left = 270
        Top = 99
        Width = 83
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 16
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      inline FrCFOPCapa: TFrCFOPs
        Left = 592
        Top = -2
        Width = 289
        Height = 41
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 17
        TabStop = True
        ExplicitLeft = 592
        ExplicitTop = -2
        ExplicitWidth = 289
        ExplicitHeight = 41
        inherited sgPesquisa: TGridLuka
          Width = 264
          Height = 24
          ExplicitWidth = 264
          ExplicitHeight = 24
        end
        inherited PnTitulos: TPanel
          Width = 289
          ExplicitWidth = 289
          inherited lbNomePesquisa: TLabel
            Width = 26
            Caption = 'CFOP'
            ExplicitWidth = 26
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 184
            ExplicitLeft = 184
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited cbTipoCFOPs: TComboBoxLuka
          Height = 22
          ExplicitHeight = 22
        end
        inherited pnPesquisa: TPanel
          Left = 264
          Height = 25
          ExplicitLeft = 264
          ExplicitHeight = 25
        end
      end
      object st2: TStaticText
        Left = 371
        Top = 89
        Width = 173
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Atualizar custo compra?'
        Color = 15395562
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 18
        Transparent = False
        Visible = False
      end
      object stTipoEntrega: TStaticText
        Left = 371
        Top = 104
        Width = 173
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Sim'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        TabOrder = 19
        Transparent = False
        Visible = False
      end
      object st3: TStaticText
        Left = 550
        Top = 89
        Width = 173
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Atualizar custo final?'
        Color = 15395562
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 20
        Transparent = False
      end
      object stAtualizarCustoFinal: TStaticText
        Left = 550
        Top = 104
        Width = 173
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Sim'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        TabOrder = 21
        Transparent = False
      end
      object stTipoRateioFrete: TStaticText
        Left = 729
        Top = 104
        Width = 173
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Valor'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        TabOrder = 22
        Transparent = False
      end
      object st6: TStaticText
        Left = 729
        Top = 89
        Width = 173
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Rateio de frete'
        Color = 15395562
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 23
        Transparent = False
      end
      object StaticTextLuka1: TStaticTextLuka
        Left = 3
        Top = 125
        Width = 578
        Height = 15
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Financeiro'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 24
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      inline FrPlanoFinanceiro: TFrPlanosFinanceiros
        Left = 4
        Top = 143
        Width = 261
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 25
        TabStop = True
        ExplicitLeft = 4
        ExplicitTop = 143
        ExplicitWidth = 261
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 236
          Height = 23
          ExplicitWidth = 236
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 261
          ExplicitWidth = 261
          inherited lbNomePesquisa: TLabel
            Width = 88
            Caption = 'Plano financeiro'
            ExplicitWidth = 88
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 156
            ExplicitLeft = 156
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 236
          Height = 24
          ExplicitLeft = 236
          ExplicitHeight = 24
        end
      end
      inline FrCentroCusto: TFrCentroCustos
        Left = 264
        Top = 143
        Width = 261
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 26
        TabStop = True
        ExplicitLeft = 264
        ExplicitTop = 143
        ExplicitWidth = 261
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 236
          Height = 23
          ExplicitWidth = 236
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 261
          ExplicitWidth = 261
          inherited lbNomePesquisa: TLabel
            Width = 150
            Caption = 'Centro de custo ( Opcional )'
            ExplicitWidth = 150
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 156
            ExplicitLeft = 156
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 236
          Height = 24
          ExplicitLeft = 236
          ExplicitHeight = 24
        end
      end
      inline FrDadosCobranca: TFrDadosCobranca
        Left = 4
        Top = 183
        Width = 578
        Height = 243
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 27
        TabStop = True
        ExplicitLeft = 4
        ExplicitTop = 183
        ExplicitHeight = 243
        inherited pnInformacoesPrincipais: TPanel
          inherited lbllb10: TLabel
            Width = 39
            Height = 14
            ExplicitWidth = 39
            ExplicitHeight = 14
          end
          inherited lbllb9: TLabel
            Width = 29
            Height = 14
            ExplicitWidth = 29
            ExplicitHeight = 14
          end
          inherited lbllb6: TLabel
            Width = 28
            Height = 14
            ExplicitWidth = 28
            ExplicitHeight = 14
          end
          inherited lbllb2: TLabel
            Width = 64
            Height = 14
            ExplicitWidth = 64
            ExplicitHeight = 14
          end
          inherited lbl2: TLabel
            Width = 62
            Height = 14
            ExplicitWidth = 62
            ExplicitHeight = 14
          end
          inherited eRepetir: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited ePrazo: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eValorCobranca: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eDataVencimento: TEditLukaData
            Height = 22
            ExplicitHeight = 22
          end
          inherited FrTiposCobranca: TFrTiposCobranca
            inherited sgPesquisa: TGridLuka
              ExplicitWidth = 184
            end
            inherited PnTitulos: TPanel
              ExplicitWidth = 209
              inherited lbNomePesquisa: TLabel
                Height = 15
              end
              inherited pnSuprimir: TPanel
                ExplicitLeft = 104
              end
            end
            inherited pnPesquisa: TPanel
              ExplicitLeft = 184
            end
          end
          inherited eDocumento: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
        end
        inherited pnInformacoesBoleto: TPanel
          inherited lbllb31: TLabel
            Width = 79
            Height = 14
            ExplicitWidth = 79
            ExplicitHeight = 14
          end
          inherited lbl1: TLabel
            Width = 92
            Height = 14
            ExplicitWidth = 92
            ExplicitHeight = 14
          end
          inherited eNossoNumero: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eCodigoBarras: TMaskEditLuka
            Height = 22
            ExplicitHeight = 22
          end
        end
        inherited pn1: TPanel
          Height = 130
          ExplicitHeight = 130
          inherited sgCobrancas: TGridLuka
            Height = 127
            ExplicitHeight = 127
          end
        end
        inherited pnInformacoesCheque: TPanel
          inherited lbllb1: TLabel
            Width = 33
            Height = 14
            ExplicitWidth = 33
            ExplicitHeight = 14
          end
          inherited lbllb3: TLabel
            Width = 43
            Height = 14
            ExplicitWidth = 43
            ExplicitHeight = 14
          end
          inherited lbllb4: TLabel
            Width = 79
            Height = 14
            ExplicitWidth = 79
            ExplicitHeight = 14
          end
          inherited lbllb5: TLabel
            Width = 71
            Height = 14
            ExplicitWidth = 71
            ExplicitHeight = 14
          end
          inherited lbl3: TLabel
            Width = 85
            Height = 14
            ExplicitWidth = 85
            ExplicitHeight = 14
          end
          inherited lbl4: TLabel
            Width = 48
            Height = 14
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited lbCPF_CNPJ: TLabel
            Width = 52
            Height = 14
            ExplicitWidth = 52
            ExplicitHeight = 14
          end
          inherited eBanco: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eAgencia: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eContaCorrente: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eNumeroCheque: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eNomeEmitente: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eTelefoneEmitente: TEditTelefoneLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eCPF_CNPJ: TEditCPF_CNPJ_Luka
            Height = 22
            ExplicitHeight = 22
          end
        end
      end
    end
    object tsProdutos: TTabSheet
      Caption = 'Produtos'
      ImageIndex = 1
      DesignSize = (
        906
        463)
      object sgItens: TGridLuka
        Left = 0
        Top = 0
        Width = 906
        Height = 342
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alTop
        ColCount = 27
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        PopupMenu = pmOpcoes
        TabOrder = 0
        OnClick = sgItensClick
        OnDrawCell = sgItensDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Legenda'
          'Produto'
          'Nome'
          'Marca'
          'CFOP'
          'Quantidade'
          'Und.'
          'Qtde.ent.Hiva'
          'Und.ent.Hiva'
          'Valor unit.'
          'Valor total'
          'Valor desc.'
          'Valor out.desp.'
          'Valor frete'
          'CST'
          'Base ICMS'
          '% ICMS'
          'Valor ICMS'
          'Base ST'
          '% ICMS ST'
          'ICMS ST'
          '% IPI'
          'Valor IPI'
          'Local'
          'Nome local'
          'Pre'#231'o final'
          'Pre'#231'o l'#237'quido'
          'CST PIS'
          'CST COF.'
          'NCM'
          'C'#243'digo de barras')
        OnGetCellColor = sgItensGetCellColor
        Grid3D = False
        RealColCount = 50
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          53
          52
          193
          98
          34
          69
          32
          80
          74
          65
          79
          67
          88
          65
          25
          64
          43
          64
          50
          61
          51
          35
          56
          37
          133
          64
          80)
      end
      object st7: TStaticText
        Left = 677
        Top = 341
        Width = 116
        Height = 15
        Alignment = taCenter
        Anchors = [akRight, akBottom]
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Qtde total'
        Color = 15395562
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        Transparent = False
      end
      object stQtdeTotal: TStaticText
        Left = 677
        Top = 355
        Width = 115
        Height = 16
        Alignment = taCenter
        Anchors = [akRight, akBottom]
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        Transparent = False
      end
      object stValorTotal: TStaticText
        Left = 791
        Top = 355
        Width = 115
        Height = 16
        Alignment = taCenter
        Anchors = [akRight, akBottom]
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = '0,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        Transparent = False
      end
      object st8: TStaticText
        Left = 791
        Top = 341
        Width = 115
        Height = 15
        Alignment = taCenter
        Anchors = [akRight, akBottom]
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Valor total'
        Color = 15395562
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 4
        Transparent = False
      end
      object st9: TStaticText
        Left = 563
        Top = 341
        Width = 115
        Height = 15
        Alignment = taCenter
        Anchors = [akRight, akBottom]
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Peso total'
        Color = 15395562
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        Transparent = False
      end
      object stPesoTotal: TStaticText
        Left = 563
        Top = 355
        Width = 115
        Height = 16
        Alignment = taCenter
        Anchors = [akRight, akBottom]
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        TabOrder = 6
        Transparent = False
      end
      inline FrBuscarLotes: TFrBuscarLotes
        Left = 0
        Top = 342
        Width = 225
        Height = 121
        Anchors = [akLeft, akBottom]
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 7
        TabStop = True
        ExplicitTop = 342
        ExplicitWidth = 225
        ExplicitHeight = 121
        inherited sgValores: TGridLuka
          Width = 225
          Height = 104
          ExplicitWidth = 225
          ExplicitHeight = 104
        end
        inherited StaticTextLuka1: TStaticTextLuka
          Width = 225
          Caption = 'Lotes ( Produto selecionado )'
          ExplicitWidth = 225
        end
      end
      inline FrBuscarPedidosCompras: TFrBuscarPedidosCompras
        Left = 227
        Top = 342
        Width = 278
        Height = 124
        Anchors = [akLeft, akBottom]
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 8
        TabStop = True
        ExplicitLeft = 227
        ExplicitTop = 342
        ExplicitWidth = 278
        ExplicitHeight = 124
        inherited sgValores: TGridLuka
          Width = 276
          Height = 104
          ExplicitWidth = 276
          ExplicitHeight = 104
          ColWidths = (
            57
            78
            99)
        end
        inherited StaticTextLuka1: TStaticTextLuka
          Width = 276
          Caption = 'Pedidos de compras ( Produto selecionado )'
          ExplicitWidth = 276
        end
        inherited StaticTextLuka2: TStaticTextLuka
          Left = 282
          ExplicitLeft = 282
        end
        inherited sgPedidos: TGridLuka
          Left = 282
          ExplicitLeft = 282
        end
      end
    end
    object tsFinanceiro: TTabSheet
      Caption = 'Financeiro'
      ImageIndex = 2
      OnShow = tsFinanceiroShow
      object sgFinanceiros: TGridLuka
        Left = 0
        Top = 0
        Width = 906
        Height = 463
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        PopupMenu = FrBuscarPedidosCompras.pmOpcoes
        TabOrder = 0
        OnDblClick = sgFinanceirosDblClick
        OnDrawCell = sgFinanceirosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'C'#243'digo'
          'Tipo de cobran'#231'a'
          'Valor'
          'Status'
          'Data vencimento')
        Grid3D = False
        RealColCount = 5
        AtivarPopUpSelecao = False
        ColWidths = (
          57
          257
          99
          97
          103)
      end
    end
  end
  object pmOpcoes: TPopupMenu
    Left = 808
    Top = 480
    object miInformaesPrecoFinalLiquido: TMenuItem
      Caption = 'Informa'#231#245'es do pre'#231'o final/l'#237'quido'
      OnClick = miInformaesPrecoFinalLiquidoClick
    end
  end
end
