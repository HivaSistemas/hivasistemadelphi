unit BaixarCancelarItensBloqueioEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Biblioteca, _Sessao,
  Vcl.ExtCtrls, _BloqueiosEstoquesItens, Vcl.Grids, GridLuka, _BloqueiosEstoquesBaixas, _RecordsEspeciais,
  _BloqueiosEstBaixasItens, Vcl.StdCtrls, MemoAltis;

type
  TFormBaixarCancelarItensBloqueioEstoque = class(TFormHerancaFinalizar)
    sgItens: TGridLuka;
    meObservacoes: TMemoAltis;
    lbObservacoes: TLabel;
    procedure FormShow(Sender: TObject);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
    procedure sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
  private
    FBaixando: Boolean;
    FBloqueioId: Integer;
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function BaixarCancelar(pBloqueioId: Integer; pBaixar: Boolean): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

const
  coProdutoId    = 0;
  coNomeProduto  = 1;
  coQuantidade   = 2;
  coUnidade      = 3;
  coLote         = 4;
  coLocal        = 5;
  coSaldo        = 6;
  coBaixarCanc   = 7;

  (* Ocultas *)
  coItemId        = 8;
  coLocalId       = 9;
  coMultiploVenda = 10;


function BaixarCancelar(pBloqueioId: Integer; pBaixar: Boolean): TRetornoTelaFinalizar;
var
  i: Integer;
  vForm: TFormBaixarCancelarItensBloqueioEstoque;
  vItens: TArray<RecBloqueiosEstoquesItens>;
begin
  Result.RetTela := trCancelado;

  if pBloqueioId = 0 then
    Exit;

  vItens := _BloqueiosEstoquesItens.BuscarBloqueiosEstoquesItens(Sessao.getConexaoBanco, 1, [pBloqueioId]);
  if vItens = nil then begin
    _Biblioteca.NenhumRegistroSelecionado;
    Exit;
  end;

  vForm := TFormBaixarCancelarItensBloqueioEstoque.Create(Application);

  vForm.FBaixando   := pBaixar;
  vForm.FBloqueioId := pBloqueioId;

  for i := Low(vItens) to High(vItens) do begin
    vForm.sgItens.Cells[coProdutoId, i + 1]   := NFormat(vItens[i].ProdutoId);
    vForm.sgItens.Cells[coNomeProduto, i + 1] := vItens[i].NomeProduto;
    vForm.sgItens.Cells[coQuantidade, i + 1]  := NFormatNEstoque(vItens[i].Quantidade);
    vForm.sgItens.Cells[coUnidade, i + 1]     := vItens[i].UnidadeVenda;
    vForm.sgItens.Cells[coLote, i + 1]        := vItens[i].Lote;
    vForm.sgItens.Cells[coLocal, i + 1]       := getInformacao(vItens[i].LocalId, vItens[i].NomeLocal);
    vForm.sgItens.Cells[coSaldo, i + 1]       := NFormatNEstoque(vItens[i].Saldo);
    vForm.sgItens.Cells[coBaixarCanc, i + 1]  := '';

    vForm.sgItens.Cells[coLocalId, i + 1]       := NFormat(vItens[i].LocalId);
    vForm.sgItens.Cells[coItemId, i + 1]        := NFormat(vItens[i].ItemId);
    vForm.sgItens.Cells[coMultiploVenda, i + 1] := NFormatNEstoque(vItens[i].MultiploVenda);
  end;
  vForm.sgItens.SetLinhasGridPorTamanhoVetor( Length(vItens) );

  Result.Ok(vForm.ShowModal, True);
end;

procedure TFormBaixarCancelarItensBloqueioEstoque.Finalizar(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  vItens: TArray<RecBloqueiosEstBaixasItens>;
begin
  inherited;

  vItens := nil;
  for i := 1 to sgItens.RowCount -1 do begin
    if SFormatCurr(sgItens.Cells[coBaixarCanc, i]) = 0 then
      Continue;

    SetLength(vItens, Length(vItens) + 1);
    vItens[High(vItens)].ItemId     := SFormatInt(sgItens.Cells[coItemId, i]);
    vItens[High(vItens)].Quantidade := SFormatCurr(sgItens.Cells[coBaixarCanc, i]);
  end;

  if vItens = nil then begin
    _Biblioteca.Exclamar('Nenhum item foi definido para ' + IIfStr(FBaixando, 'baixa', 'cancelamento') + '!');
    Abort;
  end;

  vRetBanco :=
    _BloqueiosEstoquesBaixas.AtualizarBloqueiosEstoquesBaixas(
      Sessao.getConexaoBanco,
      0,
      FBloqueioId,
      IIfStr(FBaixando, 'B', 'C'),
      meObservacoes.Lines.Text,
      vItens
    );

  Sessao.AbortarSeHouveErro(vRetBanco);
end;

procedure TFormBaixarCancelarItensBloqueioEstoque.FormShow(Sender: TObject);
begin
  inherited;
  sgItens.Col := coBaixarCanc;

  if FBaixando then
    sgItens.Cells[coBaixarCanc, 0] := 'Qtde. baixar'
  else
    sgItens.Cells[coBaixarCanc, 0] := 'Qtde. canc.';
end;

procedure TFormBaixarCancelarItensBloqueioEstoque.sgItensArrumarGrid(
  Sender: TObject; ARow, ACol: Integer; var TextCell: string);
begin
  inherited;
  if ACol <> coBaixarCanc then
    Exit;

  if not ValidarMultiplo(SFormatDouble(TextCell), SFormatDouble(sgItens.Cells[coMultiploVenda, ARow])) then begin
    TextCell := '';
    Exit;
  end;

  TextCell := NFormatNEstoque(SFormatDouble(TextCell));
  if SFormatCurr(TextCell) > SFormatCurr(sgItens.Cells[coSaldo, ARow]) then begin

    if FBaixando then
      _Biblioteca.Exclamar('A quantidade a baixar n�o pode ser maior que o saldo pendente!')
    else
      _Biblioteca.Exclamar('A quantidade a cancelar n�o pode ser maior que o saldo pendente!');

    TextCell := NFormatNEstoque(SFormatCurr(sgItens.Cells[coSaldo, ARow]));
    sgItens.SetFocus;
    Exit;
  end;
end;

procedure TFormBaixarCancelarItensBloqueioEstoque.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coQuantidade, coSaldo, coBaixarCanc] then
    vAlinhamento := taRightJustify
  else if ACol in[coUnidade] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormBaixarCancelarItensBloqueioEstoque.sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  CanSelect := ACol = coBaixarCanc;
end;

procedure TFormBaixarCancelarItensBloqueioEstoque.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
