inherited FrPisCofins: TFrPisCofins
  Width = 785
  Height = 245
  ExplicitWidth = 785
  ExplicitHeight = 245
  DesignSize = (
    785
    245)
  object lb1: TLabel
    Left = 3
    Top = 3
    Width = 30
    Height = 13
    Caption = '% PIS'
  end
  object lb2: TLabel
    Left = 152
    Top = 3
    Width = 52
    Height = 13
    Caption = '% COFINS'
  end
  object lb3: TLabel
    Left = 3
    Top = 61
    Width = 38
    Height = 13
    Caption = 'Entrada'
  end
  object lb4: TLabel
    Left = 3
    Top = 101
    Width = 26
    Height = 13
    Caption = 'Sa'#237'da'
  end
  object lb5: TLabel
    Left = 3
    Top = 162
    Width = 38
    Height = 13
    Caption = 'Entrada'
  end
  object lb6: TLabel
    Left = 3
    Top = 202
    Width = 26
    Height = 13
    Caption = 'Sa'#237'da'
  end
  object cbPercentualPIS: TComboBoxLuka
    Left = 3
    Top = 17
    Width = 144
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    ItemIndex = 0
    TabOrder = 0
    Text = 'Utilizar par'#226'metros'
    Items.Strings = (
      'Utilizar par'#226'metros'
      '0,65'
      '1,65')
    Valores.Strings = (
      '0,00'
      '0,65'
      '1,65')
    AsInt = 0
    AsString = '0,00'
  end
  object cbPercentualCOFINS: TComboBoxLuka
    Left = 152
    Top = 17
    Width = 141
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    ItemIndex = 0
    TabOrder = 1
    Text = 'Utilizar par'#226'metros'
    Items.Strings = (
      'Utilizar par'#226'metros'
      '3,00'
      '7,60')
    Valores.Strings = (
      '0,00'
      '3,00'
      '7,60')
    AsInt = 0
    AsString = '0,00'
  end
  object st4: TStaticText
    Left = 3
    Top = 45
    Width = 778
    Height = 15
    Align = alCustom
    Alignment = taCenter
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    Caption = 'CST PIS'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    Transparent = False
  end
  object st1: TStaticText
    Left = 3
    Top = 143
    Width = 778
    Height = 15
    Align = alCustom
    Alignment = taCenter
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    Caption = 'CST COFINS'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 3
    Transparent = False
  end
  object cbCstEntradaPIS: TComboBoxLuka
    Left = 3
    Top = 75
    Width = 778
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Anchors = [akLeft, akTop, akRight]
    Color = clWhite
    TabOrder = 4
    AsInt = 0
  end
  object cbCstSaidaPIS: TComboBoxLuka
    Left = 3
    Top = 115
    Width = 778
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Anchors = [akLeft, akTop, akRight]
    Color = clWhite
    TabOrder = 5
    AsInt = 0
  end
  object cbCstEntradaCOFINS: TComboBoxLuka
    Left = 3
    Top = 176
    Width = 778
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Anchors = [akLeft, akTop, akRight]
    Color = clWhite
    TabOrder = 6
    AsInt = 0
  end
  object cbCstSaidaCOFINS: TComboBoxLuka
    Left = 3
    Top = 216
    Width = 778
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Anchors = [akLeft, akTop, akRight]
    Color = clWhite
    TabOrder = 7
    AsInt = 0
  end
end
