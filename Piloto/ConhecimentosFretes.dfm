inherited FormConhecimentosFretes: TFormConhecimentosFretes
  Caption = 'Entrada de conhecimento de transporte '
  ClientHeight = 584
  ClientWidth = 782
  ExplicitWidth = 788
  ExplicitHeight = 613
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label1: TLabel
    Left = 124
    Top = 2
    ExplicitLeft = 124
    ExplicitTop = 2
  end
  object lbl1: TLabel [1]
    Left = 515
    Top = 2
    Width = 105
    Height = 14
    Caption = 'N'#250'm.conhecimento'
  end
  object lbllb3: TLabel [2]
    Left = 626
    Top = 1
    Width = 42
    Height = 14
    Caption = 'Modelo'
  end
  object lbllb2: TLabel [3]
    Left = 716
    Top = 1
    Width = 28
    Height = 14
    Caption = 'S'#233'rie'
  end
  object lbllb1: TLabel [4]
    Left = 371
    Top = 47
    Width = 47
    Height = 14
    Caption = 'N'#186' da NF'
  end
  object lbl2: TLabel [5]
    Left = 421
    Top = 47
    Width = 42
    Height = 14
    Caption = 'Modelo'
  end
  object lbl3: TLabel [6]
    Left = 487
    Top = 47
    Width = 28
    Height = 14
    Caption = 'S'#233'rie'
  end
  object lbllb6: TLabel [7]
    Left = 534
    Top = 47
    Width = 76
    Height = 14
    Caption = 'Data emiss'#227'o'
  end
  object lbllb13: TLabel [8]
    Left = 613
    Top = 47
    Width = 26
    Height = 14
    Caption = 'Peso'
  end
  object lbl4: TLabel [9]
    Left = 668
    Top = 47
    Width = 26
    Height = 14
    Caption = 'Qtde'
  end
  object lbl5: TLabel [10]
    Left = 723
    Top = 47
    Width = 56
    Height = 14
    Caption = 'Valor nota'
  end
  object lbl6: TLabel [11]
    Left = 623
    Top = 278
    Width = 76
    Height = 14
    Caption = 'Data emiss'#227'o'
  end
  object lbl7: TLabel [12]
    Left = 705
    Top = 278
    Width = 74
    Height = 14
    Caption = 'Data cont'#225'bil'
  end
  object lbllb7: TLabel [13]
    Left = 124
    Top = 279
    Width = 56
    Height = 14
    Caption = 'Base ICMS'
  end
  object lbllb8: TLabel [14]
    Left = 193
    Top = 279
    Width = 38
    Height = 14
    Caption = '% ICMS'
  end
  object lbl8: TLabel [15]
    Left = 246
    Top = 279
    Width = 57
    Height = 14
    Caption = 'Valor ICMS'
  end
  object lbl9: TLabel [16]
    Left = 316
    Top = 279
    Width = 67
    Height = 14
    Caption = 'Valor do frete'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl12: TLabel [17]
    Left = 124
    Top = 321
    Width = 112
    Height = 14
    Caption = 'Chave conhecimento'
  end
  inherited pnOpcoes: TPanel
    Height = 584
    ExplicitHeight = 584
    inherited sbLogs: TSpeedButton
      Left = 0
      Top = 543
      Width = 117
      ExplicitLeft = 0
      ExplicitTop = 543
      ExplicitWidth = 117
    end
  end
  inherited eID: TEditLuka
    Left = 124
    Top = 16
    ExplicitLeft = 124
    ExplicitTop = 16
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 783
    Top = 8
    TabOrder = 24
    Visible = False
    ExplicitLeft = 783
    ExplicitTop = 8
  end
  inline FrTransportadora: TFrTransportadora
    Left = 209
    Top = 0
    Width = 302
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 209
    ExplicitWidth = 302
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 277
      Height = 23
      ExplicitWidth = 277
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 302
      ExplicitWidth = 302
      inherited lbNomePesquisa: TLabel
        Width = 83
        Caption = 'Transportadora'
        ExplicitWidth = 83
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 232
        ExplicitLeft = 232
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 277
      Height = 24
      ExplicitLeft = 277
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  object eNumeroConhecimento: TEditLuka
    Left = 515
    Top = 16
    Width = 105
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 3
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object cbModeloConhecimento: TComboBoxLuka
    Left = 626
    Top = 16
    Width = 84
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = 16645629
    TabOrder = 4
    OnKeyDown = ProximoCampo
    Items.Strings = (
      '11'
      '55'
      '57'
      '59'
      '8'
      '8B')
    Valores.Strings = (
      '11'
      '55'
      '57'
      '59'
      '8'
      '8B')
    AsInt = 0
  end
  object cbSerieConhecimento: TComboBoxLuka
    Left = 716
    Top = 16
    Width = 53
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = 16645629
    ItemIndex = 0
    TabOrder = 5
    Text = '0'
    OnKeyDown = ProximoCampo
    Items.Strings = (
      '0'
      '1'
      '2'
      '3'
      '4'
      '5'
      '6'
      '7'
      '8'
      '9'
      'U-1'
      'U-2')
    Valores.Strings = (
      '0'
      '1'
      '2'
      '3'
      '4'
      '5'
      '6'
      '7'
      '8'
      '9')
    AsInt = 0
    AsString = '0'
  end
  inline FrFornecedor: TFrFornecedores
    Left = 124
    Top = 45
    Width = 244
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 6
    TabStop = True
    ExplicitLeft = 124
    ExplicitTop = 45
    ExplicitWidth = 244
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 219
      Height = 23
      ExplicitWidth = 219
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 244
      ExplicitWidth = 244
      inherited lbNomePesquisa: TLabel
        Width = 61
        Caption = 'Fornecedor'
        ExplicitWidth = 61
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 174
        ExplicitLeft = 174
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 219
      Height = 24
      ExplicitLeft = 219
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  object eNumeroNotaFiscal: TEditLuka
    Left = 371
    Top = 61
    Width = 47
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 7
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object cbModeloNota: TComboBoxLuka
    Left = 421
    Top = 61
    Width = 63
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = 16645629
    TabOrder = 8
    OnKeyDown = ProximoCampo
    Items.Strings = (
      '1'
      '1-A'
      '1-B'
      '2'
      '3'
      '3-A'
      '4'
      '5'
      '6'
      '7'
      '8'
      '8B'
      '11'
      '21'
      '22'
      '24'
      '28'
      '29'
      '55'
      '56'
      '57'
      '59'
      '65'
      '98'
      '99')
    Valores.Strings = (
      '1'
      '1-A'
      '1-B'
      '2'
      '3'
      '3-A'
      '4'
      '5'
      '6'
      '7'
      '8'
      '8B'
      '11'
      '21'
      '22'
      '24'
      '28'
      '29'
      '55'
      '56'
      '57'
      '59'
      '65'
      '98'
      '99')
    AsInt = 0
  end
  object cbSerieNota: TComboBoxLuka
    Left = 487
    Top = 61
    Width = 44
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = 16645629
    ItemIndex = 0
    TabOrder = 9
    Text = '0'
    OnKeyDown = ProximoCampo
    Items.Strings = (
      '0'
      '1'
      '2'
      '3'
      '4'
      '5'
      '6'
      '7'
      '8'
      '9')
    Valores.Strings = (
      '0'
      '1'
      '2'
      '3'
      '4'
      '5'
      '6'
      '7'
      '8'
      '9')
    AsInt = 0
    AsString = '0'
  end
  object eDataEmissaoNota: TEditLukaData
    Left = 534
    Top = 61
    Width = 76
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1; '
    MaxLength = 10
    TabOrder = 10
    Text = '  /  /    '
    OnKeyDown = ProximoCampo
  end
  object ePesoNota: TEditLuka
    Left = 613
    Top = 61
    Width = 52
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 11
    Text = '0,000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 3
    NaoAceitarEspaco = False
  end
  object eQtdeNota: TEditLuka
    Left = 668
    Top = 61
    Width = 52
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 12
    Text = '0,000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 3
    NaoAceitarEspaco = False
  end
  object eValorNota: TEditLuka
    Left = 723
    Top = 61
    Width = 56
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 13
    Text = '0,00'
    OnKeyDown = eValorNotaKeyDown
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object sgNotasConhecimento: TGridLuka
    Left = 124
    Top = 88
    Width = 655
    Height = 152
    Align = alCustom
    ColCount = 8
    Ctl3D = True
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    ParentCtl3D = False
    TabOrder = 14
    OnDblClick = sgNotasConhecimentoDblClick
    OnDrawCell = sgNotasConhecimentoDrawCell
    OnKeyDown = sgNotasConhecimentoKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Fornecedor'
      'N'#186' da NF'
      'Modelo'
      'S'#233'rie'
      'Data emiss'#227'o'
      'Peso'
      'Qtde.'
      'Valor nota')
    Grid3D = False
    RealColCount = 9
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      219
      53
      63
      47
      81
      60
      52
      61)
  end
  inline FrCFOP: TFrCFOPs
    Left = 403
    Top = 276
    Width = 216
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 20
    TabStop = True
    ExplicitLeft = 403
    ExplicitTop = 276
    ExplicitWidth = 216
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 191
      Height = 23
      ExplicitWidth = 191
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 216
      ExplicitWidth = 216
      inherited lbNomePesquisa: TLabel
        Width = 26
        Caption = 'CFOP'
        ExplicitWidth = 26
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 146
        ExplicitLeft = 146
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited cbTipoCFOPs: TComboBoxLuka
      Height = 22
      ExplicitHeight = 22
    end
    inherited pnPesquisa: TPanel
      Left = 191
      Height = 24
      ExplicitLeft = 191
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
    inherited ckEntradaConhecimentoFrete: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  object eDataEmissao: TEditLukaData
    Left = 623
    Top = 292
    Width = 76
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    Ctl3D = True
    EditMask = '99/99/9999;1; '
    MaxLength = 10
    ParentCtl3D = False
    TabOrder = 21
    Text = '  /  /    '
    OnKeyDown = ProximoCampo
  end
  object eDataContabil: TEditLukaData
    Left = 705
    Top = 292
    Width = 74
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    Ctl3D = True
    EditMask = '99/99/9999;1; '
    MaxLength = 10
    ParentCtl3D = False
    TabOrder = 22
    Text = '  /  /    '
    OnKeyDown = ProximoCampo
  end
  object StaticTextLuka2: TStaticTextLuka
    Left = 362
    Top = 241
    Width = 105
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Qtde. notas'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 25
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object stQtdeNotas: TStaticText
    Left = 362
    Top = 257
    Width = 105
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '10,0000'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 26
    Transparent = False
  end
  object StaticTextLuka1: TStaticTextLuka
    Left = 466
    Top = 241
    Width = 105
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Peso'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 27
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object stTotalPeso: TStaticText
    Left = 466
    Top = 257
    Width = 105
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '10,0000'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 28
    Transparent = False
  end
  object StaticTextLuka3: TStaticTextLuka
    Left = 570
    Top = 241
    Width = 105
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Quantidade'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 29
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object stTotalQtde: TStaticText
    Left = 570
    Top = 257
    Width = 105
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '10,0000'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 30
    Transparent = False
  end
  object StaticTextLuka4: TStaticTextLuka
    Left = 674
    Top = 241
    Width = 105
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Valor total'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 31
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object stValorTotal: TStaticText
    Left = 674
    Top = 257
    Width = 105
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '10,0000'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 32
    Transparent = False
  end
  object eBaseCalculoICMS: TEditLuka
    Left = 124
    Top = 292
    Width = 65
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 16
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object ePercICMS: TEditLuka
    Left = 193
    Top = 292
    Width = 49
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 17
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorICMS: TEditLuka
    Left = 246
    Top = 292
    Width = 66
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 18
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorFrete: TEditLuka
    Left = 316
    Top = 292
    Width = 82
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    TabOrder = 19
    Text = '0,00'
    OnChange = eValorFreteChange
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object StaticTextLuka5: TStaticTextLuka
    Left = 124
    Top = 360
    Width = 578
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Financeiros do conhecimento'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 33
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  inline FrEmpresa: TFrEmpresas
    Left = 124
    Top = 240
    Width = 237
    Height = 38
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 15
    TabStop = True
    ExplicitLeft = 124
    ExplicitTop = 240
    ExplicitWidth = 237
    ExplicitHeight = 38
    inherited sgPesquisa: TGridLuka
      Top = 14
      Width = 212
      Height = 23
      ExplicitTop = 14
      ExplicitWidth = 212
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 237
      Height = 14
      ExplicitWidth = 237
      ExplicitHeight = 14
      inherited lbNomePesquisa: TLabel
        Width = 144
        Height = 13
        Caption = 'Empresa do conhecimento'
        ExplicitWidth = 144
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 167
        Height = 14
        ExplicitLeft = 167
        ExplicitHeight = 14
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 212
      Top = 14
      Height = 24
      ExplicitLeft = 212
      ExplicitTop = 14
      ExplicitHeight = 24
    end
  end
  object eChaveConhecimento: TMaskEditLuka
    Left = 124
    Top = 335
    Width = 307
    Height = 22
    EditMask = '9999.9999.9999.9999.9999.9999.9999.9999.9999.9999.9999;1;_'
    MaxLength = 54
    TabOrder = 23
    Text = '    .    .    .    .    .    .    .    .    .    .    '
    OnKeyDown = ProximoCampo
  end
  inline FrDadosCobranca: TFrDadosCobranca
    Left = 124
    Top = 377
    Width = 578
    Height = 206
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 34
    TabStop = True
    ExplicitLeft = 124
    ExplicitTop = 377
    ExplicitHeight = 206
    inherited pnInformacoesPrincipais: TPanel
      inherited lbllb10: TLabel
        Width = 39
        Height = 14
        ExplicitWidth = 39
        ExplicitHeight = 14
      end
      inherited lbllb9: TLabel
        Width = 29
        Height = 14
        ExplicitWidth = 29
        ExplicitHeight = 14
      end
      inherited lbllb6: TLabel
        Width = 28
        Height = 14
        ExplicitWidth = 28
        ExplicitHeight = 14
      end
      inherited lbllb2: TLabel
        Width = 64
        Height = 14
        ExplicitWidth = 64
        ExplicitHeight = 14
      end
      inherited lbl2: TLabel
        Width = 62
        Height = 14
        ExplicitWidth = 62
        ExplicitHeight = 14
      end
      inherited eRepetir: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited ePrazo: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorCobranca: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eDataVencimento: TEditLukaData
        Height = 22
        ExplicitHeight = 22
      end
      inherited FrTiposCobranca: TFrTiposCobranca
        inherited PnTitulos: TPanel
          inherited lbNomePesquisa: TLabel
            Height = 15
          end
        end
      end
      inherited eDocumento: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
    end
    inherited pnInformacoesBoleto: TPanel
      inherited lbllb31: TLabel
        Width = 79
        Height = 14
        ExplicitWidth = 79
        ExplicitHeight = 14
      end
      inherited lbl1: TLabel
        Width = 92
        Height = 14
        ExplicitWidth = 92
        ExplicitHeight = 14
      end
      inherited eNossoNumero: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eCodigoBarras: TMaskEditLuka
        Height = 22
        ExplicitHeight = 22
      end
    end
    inherited pn1: TPanel
      Height = 93
      ExplicitHeight = 93
      inherited sgCobrancas: TGridLuka
        Top = 2
        Height = 90
        ExplicitTop = 2
        ExplicitHeight = 90
      end
      inherited StaticTextLuka4: TStaticTextLuka
        Color = 38619
      end
      inherited ckMostrarCodigoBarras: TCheckBoxLuka
        Checked = True
        State = cbChecked
        CheckedStr = 'S'
      end
    end
    inherited pnInformacoesCheque: TPanel
      inherited lbllb1: TLabel
        Width = 33
        Height = 14
        ExplicitWidth = 33
        ExplicitHeight = 14
      end
      inherited lbllb3: TLabel
        Width = 43
        Height = 14
        ExplicitWidth = 43
        ExplicitHeight = 14
      end
      inherited lbllb4: TLabel
        Width = 79
        Height = 14
        ExplicitWidth = 79
        ExplicitHeight = 14
      end
      inherited lbllb5: TLabel
        Width = 71
        Height = 14
        ExplicitWidth = 71
        ExplicitHeight = 14
      end
      inherited lbl3: TLabel
        Width = 85
        Height = 14
        ExplicitWidth = 85
        ExplicitHeight = 14
      end
      inherited lbl4: TLabel
        Width = 48
        Height = 14
        ExplicitWidth = 48
        ExplicitHeight = 14
      end
      inherited lbCPF_CNPJ: TLabel
        Width = 52
        Height = 14
        ExplicitWidth = 52
        ExplicitHeight = 14
      end
      inherited eBanco: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eAgencia: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eContaCorrente: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eNumeroCheque: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eNomeEmitente: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eTelefoneEmitente: TEditTelefoneLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eCPF_CNPJ: TEditCPF_CNPJ_Luka
        Height = 22
        ExplicitHeight = 22
      end
    end
  end
  object rgTipoRateioConhecimento: TRadioGroupLuka
    Left = 437
    Top = 319
    Width = 265
    Height = 39
    Caption = ' Rateio do conhecimento na entrada por: '
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'Valor'
      'Peso'
      'Quantidade')
    TabOrder = 35
    TabStop = True
    Valores.Strings = (
      'V'
      'P'
      'Q'
      'M')
  end
end
