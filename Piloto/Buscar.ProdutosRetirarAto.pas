unit Buscar.ProdutosRetirarAto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Grids, GridLuka, System.Math,
  Vcl.Buttons, Vcl.ExtCtrls, _RecordsOrcamentosVendas, _Biblioteca, _Sessao,
  Vcl.StdCtrls, EditHoras, Vcl.Mask, EditLukaData, _Imagens;

type
  RecProdutosRetirar = record
    DataEntrega: TDateTime;
    HoraEntrega: TDateTime;
    Itens: TArray<RecOrcamentoItens>;
  end;

  RecLotes = record
    ItemId: Integer;
    Lotes: TArray<RecDefinicoesLoteVenda>;
  end;

  TFormBuscarProdutosRetirarAto = class(TFormHerancaFinalizar)
    pnSuperior: TPanel;
    sgProdutos: TGridLuka;
    pnMeio: TPanel;
    pnInferior: TPanel;
    lb12: TLabel;
    eDataEntrega: TEditLukaData;
    eHoraEntrega: TEditHoras;
    sbInformacoesEstoque: TPanel;
    sgLocais: TGridLuka;
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgProdutosSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure FormShow(Sender: TObject);
    procedure sgProdutosArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
    procedure sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sbInformacoesEstoqueClick(Sender: TObject);
    procedure sgProdutosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure sgProdutosClick(Sender: TObject);
    procedure sgLocaisDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgLocaisSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure sgLocaisArrumarGrid(Sender: TObject; ARow, ACol: Integer;
      var TextCell: string);
    procedure FormCreate(Sender: TObject);
    procedure sgLocaisGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    FLotes: TArray<RecLotes>;
    FLocais: TArray<RecDefinicoesLocaisVenda>;
    FTipoEntrega: string;

    function getQuantidadeEntregar(pLinha: Integer): Currency;
    function getTemEntregaProgramada: Boolean;
    function getTemEntrega(pLinha: Integer): Boolean;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Buscar(pItens: TArray<RecOrcamentoItens>; pDataEntrega: TDateTime; pHoraEntrega: TDateTime; pTipoEntrega: string): TRetornoTelaFinalizar<RecProdutosRetirar>;

implementation

uses
  BuscarDefinicaoLotesVenda, _EstoquesDivisao;

{$R *.dfm}

const
  coProdutoId           = 0;
  coNomeProduto         = 1;
  coMarca               = 2;
  coUnidade             = 3;
  coQuantidade          = 4;
  coQtdeRetirarAto      = 5;
  coQtdeRetirar         = 6;
  coQtdeEntregar        = 7;
  coQtdeSemPrevisao     = 8;
  coEstoque             = 9;
  coDefiniuLotes        = 10;

  (* Colunas ocultas *)
  coItemId              = 11;
  coMultiploVenda       = 12;
  coTipoControleEstoque = 13;

  //sgLocais
  loEmpresaId          = 0;
  loNomeEmpresa        = 1;
  loLocalId            = 2;
  loNomeLocal          = 3;
  loDisponivel         = 4;
  loQuantidadeAto      = 5;
  loQuantidadeRetirar  = 6;
  loQuantidadeEntregar = 7;

  //Colunas ocultas
  loPermiteNegativo   = 8;
  loPosicaoArray      = 9;

function Buscar(pItens: TArray<RecOrcamentoItens>; pDataEntrega: TDateTime; pHoraEntrega: TDateTime; pTipoEntrega: string): TRetornoTelaFinalizar<RecProdutosRetirar>;
var
  i: Integer;
  j: Integer;
  idxLocais: Integer;
  idxLocaisDefinicaoAutomatica: Integer;
  indice: Integer;
  locais: TArray<RecDefinicoesLocaisVenda>;
  locaisDefinicaoAutomatica: TArray<RecDefinicoesLocaisVenda>;
  vForm: TFormBuscarProdutosRetirarAto;
  quantidadeNecessaria: Double;

  function getLotes( pItemId: Integer ): TArray<RecDefinicoesLoteVenda>;
  var
    i: Integer;
  begin
    Result := nil;
    for i := Low(vForm.FLotes) to High(vForm.FLotes) do begin
      if vForm.FLotes[i].ItemId <> pItemId then
        Continue;

      Result := vForm.FLotes[i].Lotes;
      Break;
    end;
  end;

begin
  Result.Dados.Itens := pItens;
  if pItens = nil then
    Exit;

  vForm := TFormBuscarProdutosRetirarAto.Create(Application);

  for i := Low(Result.Dados.Itens) to High(Result.Dados.Itens) do begin
    locais := nil;

    locais := _EstoquesDivisao.BuscarEstoquesLocaisDisponiveis(
      Sessao.getConexaoBanco,
      Sessao.getEmpresaLogada.EmpresaId,
      Result.Dados.Itens[i].produto_id,
      Sessao.getParametros.SomenteLocaisEstoquePositivo = 'S'
    );

    if locais = nil then
      Continue;

    for j := Low(locais) to High(locais) do begin
      indice := Length(vForm.FLocais);
      SetLength(vForm.FLocais, Length(vForm.FLocais) + 1);

      vForm.FLocais[indice].ProdutoId := locais[j].ProdutoId;
      vForm.FLocais[indice].LocalId := locais[j].LocalId;
      vForm.FLocais[indice].Nome := locais[j].Nome;
      vForm.FLocais[indice].Disponivel := locais[j].Disponivel;
      vForm.FLocais[indice].EmpresaId := Locais[j].EmpresaId;
      vForm.FLocais[indice].NomeEmpresa := Locais[j].NomeEmpresa;
      vForm.FLocais[indice].AceitaEstoqueNegativo := Locais[j].AceitaEstoqueNegativo;
    end;

    if Sessao.getParametros.PreencherLocaisAutomaticamente = 'S' then begin

      if pTipoEntrega = 'RA' then begin
      //Definindo automatico locais Retira Ato
        quantidadeNecessaria := Result.Dados.Itens[i].Quantidade;
        locaisDefinicaoAutomatica := nil;

        locaisDefinicaoAutomatica := _EstoquesDivisao.BuscarEstoquesLocaisDisponiveisPorTipoEntrega(
          Sessao.getConexaoBanco,
          Sessao.getEmpresaLogada.EmpresaId,
          Result.Dados.Itens[i].produto_id,
          'A'
        );

        for idxLocaisDefinicaoAutomatica := Low(locaisDefinicaoAutomatica) to High(locaisDefinicaoAutomatica) do begin

          if locaisDefinicaoAutomatica[idxLocaisDefinicaoAutomatica].Disponivel <= 0 then
            Continue;

          for idxLocais := Low(vForm.FLocais) to High(vForm.FLocais) do begin

            if (locaisDefinicaoAutomatica[idxLocaisDefinicaoAutomatica].ProdutoId = vForm.FLocais[idxLocais].ProdutoId) and
              (locaisDefinicaoAutomatica[idxLocaisDefinicaoAutomatica].LocalId = vForm.FLocais[idxLocais].LocalId) and
              (locaisDefinicaoAutomatica[idxLocaisDefinicaoAutomatica].EmpresaId = vForm.FLocais[idxLocais].EmpresaId)
            then begin

              if quantidadeNecessaria <= vForm.FLocais[idxLocais].Disponivel then begin
                vForm.FLocais[idxLocais].QuantidadeAto := quantidadeNecessaria;
                quantidadeNecessaria := 0;
              end
              else begin
                vForm.FLocais[idxLocais].QuantidadeAto := vForm.FLocais[idxLocais].Disponivel;
                quantidadeNecessaria := quantidadeNecessaria - vForm.FLocais[idxLocais].QuantidadeAto;
              end;

            end;

          end;

          if quantidadeNecessaria = 0 then
            Break;
        end;

      end
      //Definindo automatico locais Retirar
      else if pTipoEntrega = 'RE' then begin
        quantidadeNecessaria := Result.Dados.Itens[i].Quantidade;
        locaisDefinicaoAutomatica := nil;

        locaisDefinicaoAutomatica := _EstoquesDivisao.BuscarEstoquesLocaisDisponiveisPorTipoEntrega(
          Sessao.getConexaoBanco,
          Sessao.getEmpresaLogada.EmpresaId,
          Result.Dados.Itens[i].produto_id,
          'R'
        );

        for idxLocaisDefinicaoAutomatica := Low(locaisDefinicaoAutomatica) to High(locaisDefinicaoAutomatica) do begin

          if locaisDefinicaoAutomatica[idxLocaisDefinicaoAutomatica].Disponivel <= 0 then
            Continue;

          for idxLocais := Low(vForm.FLocais) to High(vForm.FLocais) do begin

            if (locaisDefinicaoAutomatica[idxLocaisDefinicaoAutomatica].ProdutoId = vForm.FLocais[idxLocais].ProdutoId) and
              (locaisDefinicaoAutomatica[idxLocaisDefinicaoAutomatica].LocalId = vForm.FLocais[idxLocais].LocalId) and
              (locaisDefinicaoAutomatica[idxLocaisDefinicaoAutomatica].EmpresaId = vForm.FLocais[idxLocais].EmpresaId)
            then begin

              if quantidadeNecessaria <= vForm.FLocais[idxLocais].Disponivel then begin
                vForm.FLocais[idxLocais].QuantidadeRetirar := quantidadeNecessaria;
                quantidadeNecessaria := 0;
              end
              else begin
                vForm.FLocais[idxLocais].QuantidadeRetirar := vForm.FLocais[idxLocais].Disponivel;
                quantidadeNecessaria := quantidadeNecessaria - vForm.FLocais[idxLocais].QuantidadeRetirar;
              end;

            end;

          end;

          if quantidadeNecessaria = 0 then
            Break;
        end;
      end
      //Definindo automatico Entregar
      else if pTipoEntrega = 'EN' then begin
        quantidadeNecessaria := Result.Dados.Itens[i].Quantidade;
        locaisDefinicaoAutomatica := nil;

        locaisDefinicaoAutomatica := _EstoquesDivisao.BuscarEstoquesLocaisDisponiveisPorTipoEntrega(
          Sessao.getConexaoBanco,
          Sessao.getEmpresaLogada.EmpresaId,
          Result.Dados.Itens[i].produto_id,
          'E'
        );

        for idxLocaisDefinicaoAutomatica := Low(locaisDefinicaoAutomatica) to High(locaisDefinicaoAutomatica) do begin

          if locaisDefinicaoAutomatica[idxLocaisDefinicaoAutomatica].Disponivel <= 0 then
            Continue;

          for idxLocais := Low(vForm.FLocais) to High(vForm.FLocais) do begin

            if (locaisDefinicaoAutomatica[idxLocaisDefinicaoAutomatica].ProdutoId = vForm.FLocais[idxLocais].ProdutoId) and
              (locaisDefinicaoAutomatica[idxLocaisDefinicaoAutomatica].LocalId = vForm.FLocais[idxLocais].LocalId) and
              (locaisDefinicaoAutomatica[idxLocaisDefinicaoAutomatica].EmpresaId = vForm.FLocais[idxLocais].EmpresaId)
            then begin

              if quantidadeNecessaria <= vForm.FLocais[idxLocais].Disponivel then begin
                vForm.FLocais[idxLocais].QuantidadeEntregar := quantidadeNecessaria;
                quantidadeNecessaria := 0;
              end
              else begin
                vForm.FLocais[idxLocais].QuantidadeEntregar := vForm.FLocais[idxLocais].Disponivel;
                quantidadeNecessaria := quantidadeNecessaria - vForm.FLocais[idxLocais].QuantidadeEntregar;
              end;

            end;

          end;

          if quantidadeNecessaria = 0 then
            Break;
        end;
      end;
    end;
  end;

  vForm.FTipoEntrega := pTipoEntrega;
  vForm.FLotes       := nil;

  if pTipoEntrega = 'RA' then begin
    vForm.sgProdutos.OcultarColunas([coQtdeRetirar, coQtdeEntregar, coQtdeSemPrevisao]);
    vForm.sgLocais.OcultarColunas([loQuantidadeRetirar, loQuantidadeEntregar]);
  end
  else if pTipoEntrega = 'RE' then begin
    vForm.sgProdutos.OcultarColunas([coQtdeEntregar, coQtdeSemPrevisao]);
    vForm.sgLocais.OcultarColunas([loQuantidadeEntregar]);
  end
  else if pTipoEntrega = 'EN' then begin
    vForm.sgProdutos.OcultarColunas([coQtdeSemPrevisao]);
    vForm.eDataEntrega.AsData := pDataEntrega;
    vForm.eHoraEntrega.AsHora := pHoraEntrega;
  end;

  for i := Low(pItens) to High(pItens) do begin
    vForm.sgProdutos.Cells[coProdutoId, i + 1]       := NFormat(pItens[i].produto_id);
    vForm.sgProdutos.Cells[coNomeProduto, i + 1]     := pItens[i].nome;
    vForm.sgProdutos.Cells[coMarca, i + 1]           := pItens[i].nome_marca;
    vForm.sgProdutos.Cells[coUnidade, i + 1]         := pItens[i].unidade_venda;
    vForm.sgProdutos.Cells[coQuantidade, i + 1]      := NFormatEstoque(pItens[i].quantidade);

    if pTipoEntrega = 'RA' then
      vForm.sgProdutos.Cells[coQtdeRetirarAto, i + 1]  := NFormatNEstoque(pItens[i].Quantidade)
    else if pTipoEntrega = 'RE' then
      vForm.sgProdutos.Cells[coQtdeRetirar, i + 1]     := NFormatNEstoque(pItens[i].Quantidade)
    else if pTipoEntrega = 'EN' then
      vForm.sgProdutos.Cells[coQtdeEntregar, i + 1]    := NFormatNEstoque(pItens[i].Quantidade)
    else if pTipoEntrega = 'SP' then
      vForm.sgProdutos.Cells[coQtdeSemPrevisao, i + 1] := NFormatNEstoque(pItens[i].Quantidade);

    vForm.sgProdutos.Cells[coEstoque, i + 1]         := NFormatEstoque(pItens[i].Estoque);
    vForm.sgProdutos.Cells[coItemId, i + 1]          := NFormat(pItens[i].item_id);
    vForm.sgProdutos.Cells[coMultiploVenda, i + 1]   := NFormat(pItens[i].multiplo_venda);
    vForm.sgProdutos.Cells[coTipoControleEstoque, i + 1] := pItens[i].TipoControleEstoque;

    if Em(pItens[i].TipoControleEstoque, ['P', 'L', 'G']) and (pTipoEntrega <> 'SP') then
      vForm.sgProdutos.Cells[coDefiniuLotes, i + 1] := charNaoSelecionado
    else
      vForm.sgProdutos.Cells[coDefiniuLotes, i + 1] := charSelecionado;
  end;
  vForm.sgProdutos.RowCount := IfThen(Length(pItens) > 1, High(pItens) + 2, 2);

  if Result.Ok(vForm.ShowModal, True) then begin
    if Em(pTipoEntrega, ['EN', 'SP']) then begin
      Result.Dados.DataEntrega := vForm.eDataEntrega.AsData;
      Result.Dados.HoraEntrega := vForm.eHoraEntrega.AsHora;
    end;

    for i := 1 to vForm.sgProdutos.RowCount - 1 do begin
      Result.Dados.Itens[i - 1].QuantidadeRetirarAto  := SFormatCurr(vForm.sgProdutos.Cells[coQtdeRetirarAto, i]);
      Result.Dados.Itens[i - 1].QuantidadeRetirar     := SFormatCurr(vForm.sgProdutos.Cells[coQtdeRetirar, i]);
      Result.Dados.Itens[i - 1].QuantidadeEntregar    := SFormatCurr(vForm.sgProdutos.Cells[coQtdeEntregar, i]);
      Result.Dados.Itens[i - 1].QuantidadeSemPrevisao := SFormatCurr(vForm.sgProdutos.Cells[coQtdeSemPrevisao, i]);

      Result.Dados.Itens[i - 1].Lotes := getLotes( Result.Dados.Itens[i - 1].item_id );

      if vForm.pnMeio.Visible then begin

        for j := Low(vForm.FLocais) to High(vForm.FLocais) do begin
          if Result.Dados.Itens[i - 1].produto_id <> vForm.FLocais[j].ProdutoId then
            Continue;

          if (vForm.FLocais[j].QuantidadeAto = 0) and (vForm.FLocais[j].QuantidadeRetirar = 0) and (vForm.FLocais[j].QuantidadeEntregar = 0) then
            Continue;

          indice := Length(Result.Dados.Itens[i - 1].Locais);
          SetLength(Result.Dados.Itens[i - 1].Locais, indice + 1);
          Result.Dados.Itens[i - 1].Locais[indice] := vForm.FLocais[j];
        end;
      end;
    end;
  end;
end;

procedure TFormBuscarProdutosRetirarAto.FormCreate(Sender: TObject);
begin
  inherited;
  pnMeio.Visible := Sessao.getParametros.DefinirLocalManual = 'S';

end;

procedure TFormBuscarProdutosRetirarAto.FormShow(Sender: TObject);
begin
  inherited;
  sgProdutos.Col := coQtdeRetirarAto;
end;

procedure TFormBuscarProdutosRetirarAto.sbInformacoesEstoqueClick(Sender: TObject);
var
  vLotes: TRetornoTelaFinalizar< TArray<RecDefinicoesLoteVenda> >;

  function ExisteProdutoVetor(pItemId: Integer): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    for i := Low(FLotes) to High(FLotes) do begin
      Result := FLotes[i].ItemId = pItemId;
      if Result then
        Exit;
    end;
  end;

begin
  inherited;
  if not Em(sgProdutos.Cells[coTipoControleEstoque, sgProdutos.Row], ['P', 'G', 'L']) then begin
    _Biblioteca.Exclamar('O produto selecionado n�o possui controle do tipo "Piso", "Grade" ou "Lote"!');
    Exit;
  end;

  vLotes :=
    BuscarDefinicaoLotesVenda.Buscar(
      Sessao.getEmpresaLogada.EmpresaId,
      SFormatInt(sgProdutos.Cells[coProdutoId, sgProdutos.Row]),
      sgProdutos.Cells[coNomeProduto, sgProdutos.Row],
      SFormatDouble(sgProdutos.Cells[coQtdeRetirarAto, sgProdutos.Row]),
      SFormatDouble(sgProdutos.Cells[coQtdeRetirar, sgProdutos.Row]),
      SFormatDouble(sgProdutos.Cells[coQtdeEntregar, sgProdutos.Row]),
      SFormatDouble(sgProdutos.Cells[coMultiploVenda, sgProdutos.Row]),
      ttOrcamentosVendas
      );

  if vLotes.BuscaCancelada then
    Exit;

  if not ExisteProdutoVetor(SFormatInt(sgProdutos.Cells[coItemId, sgProdutos.Row])) then begin
    SetLength(FLotes, Length(FLotes) + 1);

    FLotes[High(FLotes)].ItemId := SFormatInt(sgProdutos.Cells[coItemId, sgProdutos.Row]);
    FLotes[High(FLotes)].Lotes  := vLotes.Dados;
  end;

  sgProdutos.Cells[coDefiniuLotes, sgProdutos.Row] := charSelecionado;
  sgProdutos.Repaint;
end;

procedure TFormBuscarProdutosRetirarAto.sgLocaisArrumarGrid(Sender: TObject;
  ARow, ACol: Integer; var TextCell: string);
var
  i: Integer;
  posicao: Integer;
  quantidadeInformada: Double;
  quantidadePermitida: Double;
  quantidadeInformadaLocal: Double;
  aceitaNegativo: Boolean;
begin
  inherited;
  quantidadePermitida := SFormatCurr(sgProdutos.Cells[coQuantidade, sgProdutos.Row]);

  quantidadeInformada := SFormatDouble(TextCell);
// Esse campo valida se o multiplo do produto porem ele esta fazendo a valida��o errada tem que verificar//
//  if not ValidarMultiplo(quantidadeInformada, SFormatDouble(sgProdutos.Cells[coMultiploVenda, sgProdutos.Row])) then begin
//    TextCell := NFormatEstoque(0);
//    Exit;
//  end;

  aceitaNegativo := sgLocais.Cells[loPermiteNegativo, ARow] = 'S';

  for i := 1 to sgLocais.RowCount - 1 do begin

    quantidadeInformadaLocal :=
      SFormatDouble(sgLocais.Cells[loQuantidadeAto, i]) +
      SFormatDouble(sgLocais.Cells[loQuantidadeRetirar, i]) +
      SFormatDouble(sgLocais.Cells[loQuantidadeEntregar, i]);

    if (ACol = loQuantidadeAto) and (i <> ARow) then begin
      quantidadeInformada := quantidadeInformada + SFormatDouble(sgLocais.Cells[loQuantidadeAto, i]);
    end;

    if (ACol = loQuantidadeRetirar) and (i <> ARow) then begin
      quantidadeInformada := quantidadeInformada + SFormatDouble(sgLocais.Cells[loQuantidadeRetirar, i]);
    end;

    if (ACol = loQuantidadeEntregar) and (i <> ARow) then begin
      quantidadeInformada := quantidadeInformada + SFormatDouble(sgLocais.Cells[loQuantidadeEntregar, i]);
    end;

    if (not aceitaNegativo) and (quantidadeInformadaLocal > 0) then begin
      if (SFormatDouble(sgLocais.Cells[loDisponivel, i]) - quantidadeInformadaLocal) < 0 then begin
        Exclamar('O produto ' + sgProdutos.Cells[coProdutoId, sgProdutos.Row] + ' - ' + sgProdutos.Cells[coNomeProduto, sgProdutos.Row] + ' n�o permite estoque negativo!');
        TextCell := NFormatEstoque(0);
        Exit;
      end;
    end;
  end;

  if quantidadePermitida < quantidadeInformada then begin
    Exclamar('A quantidade definida n�o pode ser maior que a quantidade da venda!');
    TextCell := NFormatEstoque(0);
    Exit;
  end;

  posicao := SFormatInt(sgLocais.Cells[loPosicaoArray, ARow]);

  if ACol = loQuantidadeAto then begin
    FLocais[posicao].QuantidadeAto := SFormatDouble(sgLocais.Cells[loQuantidadeAto, ARow]);
    sgLocais.Cells[ACol, ARow]   := NFormatEstoque(FLocais[posicao].QuantidadeAto);
  end
  else if ACol = loQuantidadeRetirar then begin
    FLocais[posicao].QuantidadeRetirar := SFormatDouble(sgLocais.Cells[loQuantidadeRetirar, ARow]);
    sgLocais.Cells[ACol, ARow]   := NFormatEstoque(FLocais[posicao].QuantidadeRetirar);
  end
  else if ACol = loQuantidadeEntregar then begin
    FLocais[posicao].QuantidadeEntregar := SFormatDouble(sgLocais.Cells[loQuantidadeEntregar, ARow]);
    sgLocais.Cells[ACol, ARow]   := NFormatEstoque(FLocais[posicao].QuantidadeEntregar);
  end;

//  FLocais[posicao].Quantidade := SFormatDouble(sgLocais.Cells[loQuantidade, ARow]);
//  sgLocais.Cells[ACol, ARow]   := NFormatEstoque(TextCell);
end;

procedure TFormBuscarProdutosRetirarAto.sgLocaisDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[loLocalId, loDisponivel, loQuantidadeAto, loQuantidadeRetirar, loQuantidadeEntregar] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgLocais.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormBuscarProdutosRetirarAto.sgLocaisGetCellColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = loQuantidadeAto then begin
    AFont.Color := coCorFonteEdicao1;
    ABrush.Color := coCorCelulaEdicao1;
  end
  else if ACol = loQuantidadeRetirar then begin
    AFont.Color := coCorFonteEdicao2;
    ABrush.Color := coCorCelulaEdicao2;
  end
  else if ACol = loQuantidadeEntregar then begin
    AFont.Color := coCorFonteEdicao3;
    ABrush.Color := coCorCelulaEdicao3;
  end;
end;

procedure TFormBuscarProdutosRetirarAto.sgLocaisSelectCell(Sender: TObject;
  ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  CanSelect := ACol in[loQuantidadeAto, loQuantidadeRetirar, loQuantidadeEntregar];
end;

procedure TFormBuscarProdutosRetirarAto.sgProdutosArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);

  procedure AjustarQuantidade;
  var
    vPosicCol: Integer;
    vRestante: Currency;
  begin
    if FTipoEntrega = 'RA' then
      Exit;

    vPosicCol := ACol;
    vRestante := getQuantidadeEntregar(ARow) - SFormatCurr(sgProdutos.Cells[coQuantidade, ARow]);

    while vRestante > 0 do begin
      Inc(vPosicCol);

      // Se n�o for mais as colunas de defini��o de entregas ent�o passa pra frente.
      if vPosicCol > coQtdeSemPrevisao then begin
        vPosicCol := coQtdeRetirarAto -1;
        Continue;
      end;

      // Se for uma coluna inv�sivel ent�o passa pra frente.
      if sgProdutos.ColWidths[vPosicCol] < 0 then
        Continue;

      // Se a coluna atual for a mesma que acabou de ser definida ent�o vai pra frente
      if vPosicCol = ACol then
        Continue;

      // Se n�o tem quantidade ent�o passa pra frente
      if SFormatCurr(sgProdutos.Cells[vPosicCol, ARow]) = 0 then
        Continue;

      if SFormatCurr(sgProdutos.Cells[vPosicCol, ARow]) >= vRestante then begin
        sgProdutos.Cells[vPosicCol, ARow] := NFormatNEstoque( SFormatCurr(sgProdutos.Cells[vPosicCol, ARow]) - vRestante );
        vRestante := 0;
      end
      else begin
        vRestante := vRestante - SFormatCurr(sgProdutos.Cells[vPosicCol, ARow]);
        sgProdutos.Cells[vPosicCol, ARow] := '';
      end;
    end;
  end;

begin
  inherited;
  if ARow = 0 then
    Exit;

  TextCell := NFormatNEstoque(SFormatDouble(TextCell));
  if TextCell = '' then
    Exit;

  if SFormatCurr(TextCell) > SFormatCurr(sgProdutos.Cells[coQuantidade, ARow]) then begin
    _Biblioteca.Exclamar('A quantidade definida n�o pode ser maior que a quantidade da venda!');
    TextCell := '';
    Exit;
  end;

  if not ValidarMultiplo(SFormatDouble(sgProdutos.Cells[ACol, sgProdutos.Row]), SFormatDouble(sgProdutos.Cells[coMultiploVenda, sgProdutos.Row])) then begin
    SetarFoco(sgProdutos);
    TextCell := '';
    Exit;
  end;

  AjustarQuantidade;

  if Em(sgProdutos.Cells[coTipoControleEstoque, ARow], ['P', 'L', 'G']) and getTemEntrega(ARow) then
    sgProdutos.Cells[coDefiniuLotes, ARow] := charNaoSelecionado
  else
    sgProdutos.Cells[coDefiniuLotes, ARow] := charSelecionado;

  _Biblioteca.Habilitar([eDataEntrega, eHoraEntrega], getTemEntregaProgramada, False);
end;

procedure TFormBuscarProdutosRetirarAto.sgProdutosClick(Sender: TObject);
var
  i: Integer;
  indice: Integer;
begin
  inherited;
  sgLocais.ClearGrid;

  if Em(sgProdutos.Cells[coTipoControleEstoque, sgProdutos.Row], ['P', 'G', 'L']) then
    Exit;

  indice := 0;

  for i := Low(FLocais) to High(FLocais) do begin
    if Flocais[i].ProdutoId <> SFormatInt(sgProdutos.Cells[coProdutoId, sgProdutos.Row]) then
      Continue;

    sgLocais.Cells[loEmpresaId, indice + 1]      := NFormat(FLocais[i].EmpresaId);
    sgLocais.Cells[loNomeEmpresa, indice + 1]    := FLocais[i].NomeEmpresa;
    sgLocais.Cells[loLocalId, indice + 1]      := NFormat(FLocais[i].LocalId);
    sgLocais.Cells[loNomeLocal, indice + 1]    := FLocais[i].nome;
    sgLocais.Cells[loDisponivel, indice + 1]   := NFormatEstoque(FLocais[i].Disponivel);
    sgLocais.Cells[loQuantidadeAto, indice + 1]   := NFormatEstoque(FLocais[i].QuantidadeAto);
    sgLocais.Cells[loQuantidadeRetirar, indice + 1]   := NFormatEstoque(FLocais[i].QuantidadeRetirar);
    sgLocais.Cells[loQuantidadeEntregar, indice + 1]   := NFormatEstoque(FLocais[i].QuantidadeEntregar);
    sgLocais.Cells[loPermiteNegativo, indice + 1] := FLocais[i].AceitaEstoqueNegativo;
    sgLocais.Cells[loPosicaoArray, indice + 1] := NFormat(i);

    Inc(indice);
  end;

  sgLocais.RowCount := IIfInt(indice + 1 > 2, indice + 1, 2);
end;

procedure TFormBuscarProdutosRetirarAto.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coProdutoId, coQuantidade, coQtdeRetirarAto, coQtdeRetirar, coQtdeEntregar, coQtdeSemPrevisao, coEstoque] then
    vAlinhamento := taRightJustify
  else if ACol in[coUnidade, coDefiniuLotes] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormBuscarProdutosRetirarAto.sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coQtdeRetirarAto then begin
    AFont.Color := coCorFonteEdicao1;
    ABrush.Color := coCorCelulaEdicao1;
  end
  else if ACol = coQtdeRetirar then begin
    AFont.Color := coCorFonteEdicao2;
    ABrush.Color := coCorCelulaEdicao2;
  end
  else if ACol = coQtdeEntregar then begin
    AFont.Color := coCorFonteEdicao3;
    ABrush.Color := coCorCelulaEdicao3;
  end
  else if ACol = coQtdeSemPrevisao then begin
    AFont.Color := coCorFonteEdicao4;
    ABrush.Color := coCorCelulaEdicao4;
  end;
end;

procedure TFormBuscarProdutosRetirarAto.sgProdutosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coDefiniuLotes then begin
    if sgPRodutos.Cells[ACol, ARow] = charNaoSelecionado then
      APicture := _Imagens.FormImagens.im1.Picture
    else if sgPRodutos.Cells[ACol, ARow] = charSelecionado then
      APicture := _Imagens.FormImagens.im2.Picture;
  end;
end;

procedure TFormBuscarProdutosRetirarAto.sgProdutosSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  CanSelect := ACol in[coQtdeRetirarAto, coQtdeRetirar, coQtdeEntregar, coQtdeSemPrevisao];
end;

procedure TFormBuscarProdutosRetirarAto.VerificarRegistro(Sender: TObject);
var
  i: Integer;
  j: Integer;
  quantidadeAto: Double;
  quantidadeRetirar: Double;
  quantidadeEntregar: Double;
begin
  for i := 1 to sgProdutos.RowCount - 1 do begin
    if getQuantidadeEntregar(i) <> SFormatCurr(sgProdutos.Cells[coQuantidade, i]) then begin
      Exclamar('A quantidade definida para o produto ' + sgProdutos.Cells[coNomeProduto, i] + ' n�o est� de acordo com a quantidade vendida!');
      Abort;
    end;

    if sgProdutos.Cells[coDefiniuLotes, i] = charNaoSelecionado then begin
      Exclamar('Os lotes n�o foram definidos corretamente para o produto ' + sgProdutos.Cells[coNomeProduto, i] + '!');
      Abort;
    end;
  end;

  if getTemEntregaProgramada and (not eDataEntrega.DataOk) then begin
    _Biblioteca.Exclamar('Existem produtos que possuem quantidades a entregar e n�o tem nenhuma data de entrega definida, por favor, defina uma previs�o de entrega!');
    SetarFoco(eDataEntrega);
    Abort;
  end;

  if pnMeio.Visible then begin
    for i := 1 to sgProdutos.RowCount - 1 do begin

      if Em(sgProdutos.Cells[coTipoControleEstoque, i], ['P', 'G', 'L']) then
        Continue;

//      quantidadeAto := SFormatDouble(sgProdutos.Cells([coQtdeRetirarAto, i]));
//      quantidadeEntregar := SFormatDouble(sgProdutos.Cells([coQtdeRetirar, i]));
//      quantidadeRetirar := SFormatDouble(sgProdutos.Cells([coQtdeEntregar, i]));
//      quantidadeSemPrevisao := SFormatDouble(sgProdutos.Cells([coQtdeSemPrevisao, i]));

      quantidadeAto := 0;
      quantidadeEntregar := 0;
      quantidadeRetirar := 0;
      for j := Low(FLocais) to High(FLocais) do begin

        if SFormatInt(sgProdutos.Cells[coProdutoId, i]) <> FLocais[j].ProdutoId then
          Continue;

        quantidadeAto := quantidadeAto + FLocais[j].QuantidadeAto;
        quantidadeEntregar := quantidadeEntregar + FLocais[j].QuantidadeEntregar;
        quantidadeRetirar := quantidadeRetirar + FLocais[j].QuantidadeRetirar;
      end;

      if quantidadeAto <> SFormatDouble(sgProdutos.Cells[coQtdeRetirarAto, i]) then begin
        Exclamar(
          'Os locais do produto ' + sgProdutos.Cells[coProdutoId, i] + ' - ' + sgProdutos.Cells[coNomeProduto, i] +
          ' n�o foram definidos corretamente para retirar no ato!'
        );
        sgProdutos.SetFocus;
        sgProdutos.Row := i;
        Abort;
      end;

      if quantidadeEntregar <> SFormatDouble(sgProdutos.Cells[coQtdeEntregar, i]) then begin
        Exclamar(
          'Os locais do produto ' + sgProdutos.Cells[coProdutoId, i] + ' - ' + sgProdutos.Cells[coNomeProduto, i] +
          ' n�o foram definidos corretamente para entregar!'
        );
        sgProdutos.SetFocus;
        sgProdutos.Row := i;
        Abort;
      end;

      if quantidadeRetirar <> SFormatDouble(sgProdutos.Cells[coQtdeRetirar, i]) then begin
        Exclamar(
          'Os locais do produto ' + sgProdutos.Cells[coProdutoId, i] + ' - ' + sgProdutos.Cells[coNomeProduto, i] +
          ' n�o foram definidos corretamente para retirar!'
        );
        sgProdutos.SetFocus;
        sgProdutos.Row := i;
        Abort;
      end;
    end;

//  coQuantidade          = 4;
//  coQtdeRetirarAto      = 5;
//  coQtdeRetirar         = 6;
//  coQtdeEntregar        = 7;
//  coQtdeSemPrevisao     = 8;
  end;

//  if pnMeio.Visible then begin
//
//    for i := 1 to sgProdutos.RowCount - 1 do begin
//      if Em(sgProdutos.Cells[coTipoControleEstoque, i], ['P', 'G', 'L']) then
//        Continue;
//
//      quantidade := 0;
//      for j := Low(FLocais) to High(FLocais) do begin
//        if SFormatInt(sgProdutos.Cells[coProdutoId, i]) <> FLocais[j].ProdutoId then
//          Continue;
//
//        quantidade := quantidade + FLocais[j].Quantidade;
//      end;
//
//      if quantidade <> SFormatDouble(sgProdutos.Cells[coQuantidade, i]) then begin
//        Exclamar(
//          'Os locais do produto ' + sgProdutos.Cells[coProdutoId, i] + ' - ' + sgProdutos.Cells[coNomeProduto, i] +
//          ' n�o foram definidos corretamente!'
//        );
//        sgProdutos.SetFocus;
//        sgProdutos.Row := i;
//        Abort;
//
//      end;
//    end;
//  end;

  inherited;
end;

function TFormBuscarProdutosRetirarAto.getQuantidadeEntregar(pLinha: Integer): Currency;
begin
  Result :=
    SFormatCurr(sgProdutos.Cells[coQtdeRetirarAto, pLinha]) +
    SFormatCurr(sgProdutos.Cells[coQtdeRetirar, pLinha]) +
    SFormatCurr(sgProdutos.Cells[coQtdeEntregar, pLinha]) +
    SFormatCurr(sgProdutos.Cells[coQtdeSemPrevisao, pLinha]);
end;

function TFormBuscarProdutosRetirarAto.getTemEntrega(pLinha: Integer): Boolean;
begin
  Result :=
    SFormatCurr(sgProdutos.Cells[coQtdeRetirarAto, pLinha]) +
    SFormatCurr(sgProdutos.Cells[coQtdeRetirar, pLinha]) +
    SFormatCurr(sgProdutos.Cells[coQtdeEntregar, pLinha]) > 0;
end;

function TFormBuscarProdutosRetirarAto.getTemEntregaProgramada: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 1 to sgProdutos.RowCount - 1 do begin
    Result := SFormatCurr(sgProdutos.Cells[coQtdeEntregar, i]) > 0;
    if Result then
      Break;
  end;
end;

end.
