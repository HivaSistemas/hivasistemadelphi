unit ImportacaoImpostosIBPT;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastro, _ImpostosIBPT, _FrameHerancaPrincipal,
  FrameDiretorioArquivo, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Biblioteca,
  _RecordsEspeciais, _Sessao, Vcl.StdCtrls, _FrameHenrancaPesquisas, System.Win.ComObj,
  FrameEstados;

type
  TFormImportacaoImpostosIBPT = class(TFormHerancaCadastro)
    FrArquivo: TFrCaminhoArquivo;
    lbNCMs: TLabel;
    lbQtdeNCMs: TLabel;
    FrEstado: TFrEstados;
    procedure FormCreate(Sender: TObject);
  private
    FImpostos: TArray<RecImpostosIBPT>;
    procedure FrArquivoOnAposPesquisar(Sender: TObject);
  protected
    procedure Modo(pEditando: Boolean); override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

procedure TFormImportacaoImpostosIBPT.FormCreate(Sender: TObject);
begin
  inherited;
  FrArquivo.setFiltro('Arquivo CSV (*.csv)|*.csv|');
  FrArquivo.OnAposPesquisarArquivo := FrArquivoOnAposPesquisar;
end;

procedure TFormImportacaoImpostosIBPT.FrArquivoOnAposPesquisar(Sender: TObject);
const
  coCodigoNCM     = 1;
  coPercFederal   = 5;
  coPercImportado = 6;
  coPercEstadual  = 7;
  coPercMunicipal = 8;
  coFonte         = 13;

var
  i: Integer;
  sheet: OleVariant;
  vPlanilha: OleVariant;

  function NCMInvalido( pNCM: string ): Boolean;
  var
    j: Integer;
  begin
    Result := Length(pNCM) > 8;
    if Result then
      Exit;

    for j := Low(FImpostos) to High(FImpostos) do begin
      Result := FImpostos[j].CodigoNCM = pNCM;
      if Result then
        Exit;
    end;
  end;

  function Valor( pValor: string ): Double;
  begin
    Result := StrToFloat( StringReplace(pValor, '.', ',', [rfReplaceAll]) ) ;
  end;

begin
  FImpostos := nil;
  //Crio o objeto que gerencia o arquivo excel
  vPlanilha:= CreateOleObject('Excel.Application');

  //Abro o arquivo
  vPlanilha.WorkBooks.open(FrArquivo.getCaminhoArquivo);

  //Pego a primeira vPlanilha do arquivo
  sheet := vPlanilha.WorkSheets[1];

  i := 2;
  while Trim(sheet.Cells[i, coCodigoNCM]) <> '' do begin
    if NCMInvalido(sheet.cells[i, coCodigoNCM].Value) then begin
      Inc(i);
      Continue;
    end;

    SetLength(FImpostos, Length(FImpostos) + 1);
    FImpostos[High(FImpostos)].CodigoNCM     := sheet.cells[i, coCodigoNCM].Value;
    FImpostos[High(FImpostos)].PercFederal   := Valor( sheet.cells[i, coPercFederal].Value );
    FImpostos[High(FImpostos)].PercFederalImportado := Valor( sheet.cells[i, coPercImportado].Value );
    FImpostos[High(FImpostos)].PercEstadual  := Valor( sheet.cells[i, coPercEstadual].Value );
    FImpostos[High(FImpostos)].PercMunicipal := Valor( sheet.cells[i, coPercMunicipal].Value );
    FImpostos[High(FImpostos)].Fonte         := sheet.cells[i, coFonte].Value;

    Inc(i);

    lbQtdeNCMs.Caption := NFormat( Length(FImpostos) );
    lbQtdeNCMs.Refresh;
  end;

  //Fecho a vPlanilha
  vPlanilha.WorkBooks.Close;
  Modo(True);
end;

procedure TFormImportacaoImpostosIBPT.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vRetBanco := _ImpostosIBPT.AtualizarImpostos(Sessao.getConexaoBanco, FrEstado.getEstado.estado_id, FImpostos);
  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.RotinaSucesso;
end;

procedure TFormImportacaoImpostosIBPT.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([FrArquivo], not pEditando, False);
  _Biblioteca.Habilitar([FrEstado], pEditando);

  if not pEditando then
    lbQtdeNCMs.Caption := '0';
end;

procedure TFormImportacaoImpostosIBPT.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if FrEstado.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio informar a qual estado o arquivo caregado se refere!');
    SetarFoco(FrEstado);
    Abort;
  end;
end;

end.
