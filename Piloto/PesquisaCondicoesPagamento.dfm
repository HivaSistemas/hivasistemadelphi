inherited FormPesquisaCondicoesPagamento: TFormPesquisaCondicoesPagamento
  Caption = 'Pesquisa de condi'#231#245'es de pagamento'
  ClientHeight = 316
  ClientWidth = 611
  ExplicitWidth = 619
  ExplicitHeight = 347
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 611
    Height = 270
    ColCount = 5
    OnDrawCell = sgPesquisaDrawCell
    CorLinhaFoco = 38619
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Nome'
      'Indice acr'#233's.'
      'Ativo')
    RealColCount = 5
    ExplicitWidth = 611
    ExplicitHeight = 270
    ColWidths = (
      28
      55
      199
      83
      48)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Left = 6
    Top = 287
    Text = '0'
    ExplicitLeft = 6
    ExplicitTop = 287
  end
  inherited pnFiltro1: TPanel
    Width = 611
    ExplicitWidth = 611
    inherited lblPesquisa: TLabel
      Left = 240
      Width = 106
      Caption = 'Tipo de pagamento'
      ExplicitLeft = 240
      ExplicitWidth = 106
    end
    inherited lblOpcoesPesquisa: TLabel
      Left = 1
      ExplicitLeft = 1
    end
    inherited eValorPesquisa: TEditLuka
      Left = 240
      Width = 369
      ExplicitLeft = 240
      ExplicitWidth = 369
    end
    inherited cbOpcoesPesquisa: TComboBoxLuka
      Left = 1
      Width = 234
      ExplicitLeft = 1
      ExplicitWidth = 234
    end
  end
end
