unit FrameFaixaNumeros;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.StdCtrls, _Biblioteca,
  EditLuka;

type
  TFrFaixaNumeros = class(TFrameHerancaPrincipal)
    lb1: TLabel;
    eValor1: TEditLuka;
    lb2: TLabel;
    eValor2: TEditLuka;
  public
    function GetFiltrosUtilizados: TArray<string>;

    function getSQL(pColuna: string): string;
    procedure SetFocus; override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
    procedure Clear; override;
    function EstaVazio: Boolean; override;
  end;

implementation

{$R *.dfm}

{ TFrFaixaNumeros }

procedure TFrFaixaNumeros.Clear;
begin
  inherited;
  _Biblioteca.LimparCampos([eValor1, eValor2]);
end;

function TFrFaixaNumeros.EstaVazio: Boolean;
begin
  if (eValor1.AsCurr <> 0) and (eValor2.AsCurr <> 0) and (eValor1.AsCurr > eValor2.AsCurr) then begin
    _Biblioteca.Exclamar('O primeiro valor para o campo "' + lb1.Caption + '" n�o pode ser maior que o segundo valor, por favor verifique!');
    SetarFoco(eValor1);
    Abort;
  end;

  Result := (eValor1.AsCurr = 0) and (eValor2.AsCurr = 0);
end;

function TFrFaixaNumeros.GetFiltrosUtilizados: TArray<string>;
begin
  Result := nil;

  if EstaVazio then
    Exit;

  SetLength(Result, 2);

  Result[0] := lb1.Caption;
  Result[1] := '    ';
  if (eValor1.AsCurr <> 0) and (evalor2.AsCurr <> 0) then
    Result[1] := Result[1] + eValor1.Text + ' � ' + evalor2.Text
  else if eValor1.AsCurr <> 0 then
    Result[1] := Result[1] + eValor1.Text
  else if evalor2.AsCurr <> 0 then
    Result[1] := Result[1] + evalor2.Text;
end;

function TFrFaixaNumeros.getSQL(pColuna: string): string;
begin
  Result := '';

  if (eValor1.AsCurr <> 0) and (eValor2.AsCurr <> 0) and (eValor1.AsCurr > eValor2.AsCurr) then begin
    _Biblioteca.Exclamar('O primeiro valor para o campo "' + lb1.Caption + '" n�o pode ser maior que o segundo valor, por favor verifique!');
    SetarFoco(eValor1);
    Abort;
  end;

  if (eValor1.AsCurr <> 0) and (evalor2.AsCurr <> 0) then begin
    if eValor1.AsCurr = evalor2.AsCurr then
      Result := ' ' + pColuna + ' = ' + NFormatOracle(eValor1.AsCurr)
    else
      Result := ' ' + pColuna + ' between ' + NFormatOracle(eValor1.AsCurr) + ' and ' + NFormatOracle(eValor2.AsCurr)
  end
  else if eValor1.AsCurr <> 0 then
    Result := ' ' + pColuna + ' >= ' + NFormatOracle(eValor1.AsCurr)
  else if eValor2.AsCurr <> 0 then
    Result := ' ' + pColuna + ' <= ' + NFormatOracle(eValor2.AsCurr);
end;

procedure TFrFaixaNumeros.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([eValor1, eValor2], pEditando, pLimpar);
end;

procedure TFrFaixaNumeros.SetFocus;
begin
  inherited;
  SetarFoco(eValor1);
end;

end.
