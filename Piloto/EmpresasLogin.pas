unit EmpresasLogin;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPrincipal, Vcl.Grids, GridLuka, _RecordsCadastros,
  _Empresas, System.Math, _Biblioteca;

type
  TFormEmpresasLogin = class(TFormHerancaPrincipal)
    sgEmpresas: TGridLuka;
    procedure sgEmpresasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgEmpresasKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgEmpresasDblClick(Sender: TObject);
  private
    FDados: TList;
  end;

function BuscarEmpresaLogin: RecEmpresas;

implementation

{$R *.dfm}

uses
  _Sessao, _Funcionarios;

const
  co_codigo         = 0;
  co_nome_fantasia  = 1;
  co_razao_social   = 2;
  co_cnpj           = 3;

function BuscarEmpresaLogin: RecEmpresas;
var
  i: Integer;
  vComando: string;
  t: TFormEmpresasLogin;
  vEmpresas: TArray<RecEmpresas>;
  vEmpresasAutorizadas: TArray<Integer>;
begin
  Result := nil;

  vEmpresasAutorizadas := _Funcionarios.BuscarEmpresasFuncionario(Sessao.getConexaoBanco, Sessao.getUsuarioLogado.funcionario_id);

  vComando := '';
  for i := Low(vEmpresasAutorizadas) to High(vEmpresasAutorizadas) do begin
    if vComando = '' then begin
      vComando := ' where EMP.EMPRESA_ID in(' + IntToStr(vEmpresasAutorizadas[i]);
      Continue;
    end;

    vComando := vComando + ' ,' + IntToStr(vEmpresasAutorizadas[i]);
  end;
  vComando := vComando + ') order by EMP.EMPRESA_ID asc';

  vEmpresas := _Empresas.BuscarEmpresasComando(Sessao.getConexaoBanco, vComando);
  if vEmpresas = nil then
    Exit;

  if Length(vEmpresas) = 1 then begin
    Result := vEmpresas[0];
    Exit;
  end;

  t := TFormEmpresasLogin.Create(Application);
  t.FDados := TList.Create;
  for i := Low(vEmpresas) to High(vEmpresas) do begin
    t.FDados.Add(vEmpresas[i]);
    t.sgEmpresas.Cells[co_codigo, i + 1]        := NFormat(vEmpresas[i].EmpresaId);
    t.sgEmpresas.Cells[co_nome_fantasia, i + 1] := vEmpresas[i].NomeFantasia;
    t.sgEmpresas.Cells[co_razao_social, i + 1]  := vEmpresas[i].RazaoSocial;
    t.sgEmpresas.Cells[co_cnpj, i + 1]          := vEmpresas[i].Cnpj;
  end;
  t.sgEmpresas.RowCount := IfThen(Length(vEmpresas) > 1, High(vEmpresas) + 2, 2);

  if t.ShowModal = mrOk then
    Result := RecEmpresas(t.FDados[t.sgEmpresas.Row - 1])
  else
    Halt;

  for i := 0 to t.FDados.Count -1 do begin
    if i = t.sgEmpresas.Row -1 then
      Continue;

    RecEmpresas(t.FDados[i]).Free;
  end;

  FreeAndNil(t.FDados);
  t.Free;
end;

procedure TFormEmpresasLogin.sgEmpresasDblClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrOk;
end;

procedure TFormEmpresasLogin.sgEmpresasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  alinhamento: TAlignment;
begin
  inherited;

  if ACol = co_codigo then
    alinhamento := taRightJustify
  else
    alinhamento := taLeftJustify;

  sgEmpresas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], alinhamento, Rect);
end;

procedure TFormEmpresasLogin.sgEmpresasKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    ModalResult := mrOk;
end;

end.
