unit Cadastrar.EnderecoEstoque.Nivel;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _Sessao, _Biblioteca, _RecordsEspeciais, _RecordsCadastros,
  Pesquisa.MotivosAjusteEstoque, _HerancaCadastro, _FrameHerancaPrincipal,
  _FrameHenrancaPesquisas, FramePlanosFinanceiros, CheckBoxLuka, _EnderecoEstoqueNivel;

type
  TFormEnderecoEstoqueNivel = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eDescricao: TEditLuka;
  private
    procedure PreencherRegistro(pModuloId: RecEnderecoEstoqueNivel);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

uses Pesquisa.EnderecoEstoqueNivel;

{ TFormEnderecoEstoqueNivel }

procedure TFormEnderecoEstoqueNivel.BuscarRegistro;
var
  vEnderecoEstoqueNivel: TArray<RecEnderecoEstoqueNivel>;
begin
  vEnderecoEstoqueNivel := _EnderecoEstoqueNivel.BuscarEnderecoEstoqueNivel(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if vEnderecoEstoqueNivel = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    eID.SetFocus;
    Exit;
  end;

  inherited;
  PreencherRegistro(vEnderecoEstoqueNivel[0]);
end;

procedure TFormEnderecoEstoqueNivel.ExcluirRegistro;
var
  vRetorno: RecRetornoBD;
begin
  vRetorno := _EnderecoEstoqueNivel.ExcluirEnderecoEstoqueNivel(Sessao.getConexaoBanco, eId.AsInt);
  if vRetorno.TeveErro then begin
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormEnderecoEstoqueNivel.GravarRegistro(Sender: TObject);
var
  vRetorno: RecRetornoBD;
begin
  inherited;
  vRetorno :=
    _EnderecoEstoqueNivel.AtualizarEnderecoEstoqueNivel(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eDescricao.Text
    );

  if vRetorno.TeveErro then begin
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetorno.AsInt);
end;

procedure TFormEnderecoEstoqueNivel.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([eDescricao, ckAtivo], pEditando);

  if pEditando then begin
    SetarFoco(eDescricao);
    ckAtivo.Checked := True;
  end;
end;

procedure TFormEnderecoEstoqueNivel.PesquisarRegistro;
var
  vEnderecoEstoqueNivel: RecEnderecoEstoqueNivel;
begin
  vEnderecoEstoqueNivel := RecEnderecoEstoqueNivel(Pesquisa.EnderecoEstoqueNivel.Pesquisar());  //AquiS
  if vEnderecoEstoqueNivel = nil then
    Exit;

  inherited;
  PreencherRegistro(vEnderecoEstoqueNivel);
end;

procedure TFormEnderecoEstoqueNivel.PreencherRegistro(pModuloId: RecEnderecoEstoqueNivel);
begin
  eID.AsInt                     := pModuloId.nivel_id;
  eDescricao.Text               := pModuloId.descricao;
  pModuloId.Free;
end;

procedure TFormEnderecoEstoqueNivel.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eDescricao.Trim = '' then begin
    _Biblioteca.Exclamar('A descri��o n�o foi informado corretamente, verifique!');
    SetarFoco(eDescricao);
    Abort;
  end;
end;

end.
