unit FrameCargosProfissionais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Buttons,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _CargosProfissionais,
  Vcl.Menus;

type
  TFrCargosProfissionais = class(TFrameHenrancaPesquisas)
  public
    function GetCargoProfissional(pLinha: Integer = -1): RecCargosProfissionais;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

uses PesquisaCargosProfissionais, _Sessao, _Biblioteca;

{ TFrCargosProfissionais }

function TFrCargosProfissionais.AdicionarDireto: TObject;
var
  vDados: TArray<RecCargosProfissionais>;
begin
  vDados := _CargosProfissionais.BuscarCargosProfissionais(Sessao.getConexaoBanco, 0, [FChaveDigitada], ckSomenteAtivos.Checked);
  if vDados = nil then
    Result := nil
  else
    Result := vDados[0];
end;

function TFrCargosProfissionais.AdicionarPesquisando: TObject;
begin
  Result := PesquisaCargosProfissionais.PesquisarCargoProfissional(ckSomenteAtivos.Checked);
end;

function TFrCargosProfissionais.GetCargoProfissional(pLinha: Integer): RecCargosProfissionais;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecCargosProfissionais(FDados[IIfInt(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrCargosProfissionais.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecCargosProfissionais(FDados[i]).CargoId = RecCargosProfissionais(pSender).CargoId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrCargosProfissionais.MontarGrid;
var
  i: Integer;
  vSender: RecCargosProfissionais;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecCargosProfissionais(FDados[i]);
      AAdd([IntToStr(vSender.CargoId), vSender.nome]);
    end;
  end;
end;

end.
