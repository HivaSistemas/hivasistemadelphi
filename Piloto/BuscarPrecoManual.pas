unit BuscarPrecoManual;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Grids, GridLuka, _RecordsOrcamentosVendas,
  Vcl.Buttons, Vcl.ExtCtrls, _Biblioteca, _Sessao, StaticTextLuka, Vcl.StdCtrls, Vcl.Menus, Legendas;

type
  TFormBuscaPrecoManual = class(TFormHerancaFinalizar)
    sgItens: TGridLuka;
    st5: TStaticText;
    st1: TStaticText;
    st4: TStaticText;
    stTotalProdutosOriginal: TStaticTextLuka;
    stTotalDesconto: TStaticTextLuka;
    stDescontoMedio: TStaticTextLuka;
    st2: TStaticText;
    stNovoTotalProdutos: TStaticTextLuka;
    pmOpcoesGrid: TPopupMenu;
    miLegendas: TMenuItem;
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
    procedure sgItensGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure miLegendasClick(Sender: TObject);
  private
    FLegendaGrid: TImage;

    procedure CalcularPercDesconto(pLinha: Integer = -1);
    procedure CalcularNovoPreco(pLinha: Integer);
    procedure CalcularTotais;

    function LengendaGrid(pEmPromocao: Boolean): TPicture;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Buscar(pItens: TArray<RecOrcamentoItens>): TRetornoTelaFinalizar< TArray<RecOrcamentoItens> >;

implementation

uses
  ValidarUsuarioAutorizado, _Funcionarios, _RecordsCadastros;

{$R *.dfm}

const
  coLegenda      = 0;
  coProdutoId    = 1;
  coNome         = 2;
  coMarca        = 3;
  coQuantidade   = 4;
  coUnidade      = 5;
  coPrecoNormal  = 6;
  coPercDesconto = 7;
  coNovoPreco    = 8;

  (* Ocultas *)
  coTipoPreco    = 9;

  coCorPromocao     = clGreen;

function Buscar(pItens: TArray<RecOrcamentoItens>): TRetornoTelaFinalizar< TArray<RecOrcamentoItens> >;
var
  i: Integer;
  vForm: TFormBuscaPrecoManual;
begin
  Result.Dados := pItens;
  vForm := TFormBuscaPrecoManual.Create(nil);

  for i := Low(pItens) to High(pItens) do begin
    vForm.sgItens.Cells[coProdutoId, i + 1]    := NFormat(pItens[i].produto_id);
    vForm.sgItens.Cells[coNome, i + 1]         := pItens[i].nome;
    vForm.sgItens.Cells[coMarca, i + 1]        := pItens[i].nome_marca;
    vForm.sgItens.Cells[coQuantidade, i + 1]   := NFormatNEstoque(pItens[i].quantidade);
    vForm.sgItens.Cells[coUnidade, i + 1]      := pItens[i].unidade_venda;
    vForm.sgItens.Cells[coPrecoNormal, i + 1]  := NFormat( IIfDbl(pItens[i].PrecoManual > 0, pItens[i].PrecoVarejoBasePrecoManual, pItens[i].preco_unitario) );
    vForm.sgItens.Cells[coPercDesconto, i + 1] := NFormatN(pItens[i].PercDescontoPrecoManual, 5);
    vForm.sgItens.Cells[coNovoPreco, i + 1]    := NFormat( IIfDbl(pItens[i].PrecoManual > 0, pItens[i].PrecoManual, pItens[i].preco_unitario) );
    vForm.sgItens.Cells[coTipoPreco, i + 1]    := pItens[i].TipoPrecoUtilizado;

    vForm.CalcularPercDesconto(i + 1);
    vForm.stTotalProdutosOriginal.Somar( pItens[i].PrecoVarejoBasePrecoManual * pItens[i].quantidade );
  end;
  vForm.sgItens.SetLinhasGridPorTamanhoVetor( Length(pItens) );

  vForm.sgItens.Col := coNovoPreco;
  SetarFoco(vForm.sgItens);

  vForm.CalcularTotais;

  if Result.ok(vForm.ShowModal) then begin
    for i := 1 to vForm.sgItens.RowCount -1 do begin
      Result.Dados[i - 1].PrecoManual              := SFormatDouble( vForm.sgItens.Cells[coNovoPreco, i] );
      Result.Dados[i - 1].PercDescontoPrecoManual  := SFormatDouble( vForm.sgItens.Cells[coPercDesconto, i] );
      Result.Dados[i - 1].ValorDescUnitPrecoManual := SFormatDouble( vForm.sgItens.Cells[coPrecoNormal, i] ) - SFormatDouble( vForm.sgItens.Cells[coNovoPreco, i] );
    end;
  end;
end;

procedure TFormBuscaPrecoManual.CalcularNovoPreco(pLinha: Integer);
var
  vNovoPreco: Double;
begin
  if SFormatCurr(sgItens.Cells[coPercDesconto, pLinha]) = 0 then
    vNovoPreco := SFormatDouble(sgItens.Cells[coPrecoNormal, pLinha])
  else
    vNovoPreco := Arredondar(SFormatDouble(sgItens.Cells[coPrecoNormal, pLinha]) * (1 - SFormatDouble(sgItens.Cells[coPercDesconto, pLinha]) * 0.01), 2);

  sgItens.Cells[coNovoPreco, pLinha] := NFormatN( vNovoPreco );

  CalcularTotais;
end;

procedure TFormBuscaPrecoManual.CalcularPercDesconto(pLinha: Integer = -1);
var
  vPercDesconto: Double;
begin
  vPercDesconto := Sessao.getCalculosSistema.CalcOrcamento.getPercDescontoValorLiquido( SFormatDouble(sgItens.Cells[coPrecoNormal, pLinha]), SFormatDouble(sgItens.Cells[coNovoPreco, pLinha]), 5);
  sgItens.Cells[coPercDesconto, pLinha] := NFormatN( vPercDesconto, 5 );

  CalcularTotais;
end;

procedure TFormBuscaPrecoManual.CalcularTotais;
var
  i: Integer;

  vNovoTotalProdutos: Double;
  vTotalDesconto: Currency;
  vDescontoMedio: Double;
begin
  vNovoTotalProdutos  := 0;
  vTotalDesconto      := 0;

  _Biblioteca.LimparCampos([stNovoTotalProdutos, stTotalDesconto, stDescontoMedio]);
  for i := 1 to sgItens.RowCount -1 do begin
    vNovoTotalProdutos  := vNovoTotalProdutos + SFormatDouble(sgItens.Cells[coQuantidade, i]) * SFormatDouble(sgItens.Cells[coNovoPreco, i]);
    vTotalDesconto      := vTotalDesconto + ( (SFormatCurr(sgItens.Cells[coPrecoNormal, i]) - SFormatCurr(sgItens.Cells[coNovoPreco, i])) * SFormatDouble(sgItens.Cells[coQuantidade, i]) );
  end;

  if (vTotalDesconto = 0) or (stTotalProdutosOriginal.AsCurr = 0) then
    vDescontoMedio := 0
  else
    vDescontoMedio := Arredondar(vTotalDesconto / stTotalProdutosOriginal.AsCurr * 100, 5);

  stNovoTotalProdutos.Somar( vNovoTotalProdutos );
  stTotalDesconto.Somar( vTotalDesconto );
  stDescontoMedio.Somar( vDescontoMedio );
end;

procedure TFormBuscaPrecoManual.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FLegendaGrid.Free;
  inherited;
end;

procedure TFormBuscaPrecoManual.FormCreate(Sender: TObject);
begin
  inherited;
  FLegendaGrid := TImage.Create(Self);
  FLegendaGrid.Height := 15;
end;

function TFormBuscaPrecoManual.LengendaGrid(pEmPromocao: Boolean): TPicture;
var
	p: Integer;
  l: Integer;
begin
 	l := 0;

  if pEmPromocao then
    Inc(l, 10);

  p := 1;
  FLegendaGrid.Width := l + p;
  FLegendaGrid.Picture := nil;

  if pEmPromocao then begin
  	FLegendaGrid.Canvas.Pen.Color := coCorPromocao;
  	FLegendaGrid.Canvas.Brush.Color := coCorPromocao;
  	FLegendaGrid.Canvas.Rectangle(p, 0, p + 5, 15);
//    Inc(p, 10);
  end;

  Result := FLegendaGrid.Picture;
end;

procedure TFormBuscaPrecoManual.miLegendasClick(Sender: TObject);
var
  vLegendas: TFormLegendas;
begin
  inherited;
  vLegendas := TFormLegendas.Create(Self);

  vLegendas.AddLegenda(coCorPromocao, 'Utilizando pre�o promocional', clWhite);

  vLegendas.Show;
end;

procedure TFormBuscaPrecoManual.sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coNovoPreco then begin
    TextCell := NFormatN(SFormatDouble(TextCell));

    if sgItens.Col = coNovoPreco then
      CalcularPercDesconto(ARow);
  end
  else if ACol = coPercDesconto then begin
    TextCell := NFormatN(SFormatDouble(TextCell), 5);

    if sgItens.Col = coPercDesconto then
      CalcularNovoPreco(ARow);
  end;
end;

procedure TFormBuscaPrecoManual.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[
    coProdutoId,
    coQuantidade,
    coPrecoNormal,
    coPercDesconto,
    coNovoPreco]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormBuscaPrecoManual.sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if (ACol in[coPercDesconto, coNovoPreco]) and (Em(sgItens.Cells[coTipoPreco, ARow], ['VAR', 'MAN'])) then begin
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
  end;
end;

procedure TFormBuscaPrecoManual.sgItensGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgItens.Cells[coProdutoId, ARow] = '' then
    Exit;

  if ACol = coLegenda then begin
    APicture :=
      LengendaGrid(sgItens.Cells[coTipoPreco, ARow] = 'PRO');
  end
end;

procedure TFormBuscaPrecoManual.sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if (ACol in[coPercDesconto, coNovoPreco]) and (Em(sgItens.Cells[coTipoPreco, ARow], ['VAR', 'MAN'])) then
    sgItens.Options := sgItens.Options + [goEditing]
  else
    sgItens.Options := sgItens.Options - [goEditing];
end;

procedure TFormBuscaPrecoManual.VerificarRegistro(Sender: TObject);
var
  i: Integer;
  vPosic: Integer;
  vMaiorPercDesconto: Double;
  vRetTelaEntregasAto: TRetornoTelaFinalizar;
  funcionarioId: Integer;
  funcionarios: TArray<RecFuncionarios>;
begin
  inherited;
  vPosic := 1;
  vMaiorPercDesconto := 0;
  for i := 1 to sgItens.RowCount -1 do begin
    if SFormatDouble(sgItens.Cells[coPercDesconto, i]) > vMaiorPercDesconto then begin
      vPosic := i;
      vMaiorPercDesconto := SFormatDouble(sgItens.Cells[coPercDesconto, i]);
    end;
  end;

  if vMaiorPercDesconto > Sessao.getUsuarioLogado.PercDescItemPrecoManual then begin
    _Biblioteca.Exclamar(
      'O percentual de desconto efetuado para o item ' + sgItens.Cells[coNome, vPosic
      ] + ' � maior que o permitido!' + sLineBreak +
      'Perc. autorizado: ' + NFormat(Sessao.getUsuarioLogado.PercDescItemPrecoManual) + sLineBreak +
      'Perc. efetuado..: ' + sgItens.Cells[coPercDesconto, vPosic]
    );

    vRetTelaEntregasAto := ValidarUsuarioAutorizado.validaUsuarioAutorizado;
    if vRetTelaEntregasAto.RetTela <> trOk then begin
      _Biblioteca.NaoAutorizadoRotina;
      Abort;
    end;

    funcionarioId := vRetTelaEntregasAto.RetInt;
    funcionarios := _Funcionarios.BuscarFuncionarios(Sessao.getConexaoBanco, 0, [funcionarioId], False, False, False, False, False);

    if funcionarios[0].GerenteSistema = False then begin
      if funcionarios[0].PercentualDescontoAdicVenda < vMaiorPercDesconto then begin
        Exclamar('Funcion�rio n�o est� autorizado a conceder este percentual de desconto!');
        Abort;
      end;
    end
  end;
end;

end.
