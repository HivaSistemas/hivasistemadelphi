inherited FormBuscarDadosCliente: TFormBuscarDadosCliente
  Caption = 'Buscando dados do cliente'
  ClientHeight = 211
  ClientWidth = 421
  OnShow = FormShow
  ExplicitWidth = 427
  ExplicitHeight = 240
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 174
    Width = 421
    ExplicitTop = 159
    ExplicitWidth = 422
  end
  object pnFrameCliente: TPanel
    Left = 0
    Top = 0
    Width = 421
    Height = 56
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitLeft = 8
    ExplicitTop = 58
    ExplicitWidth = 418
    inline FrCliente: TFrClientes
      Left = 5
      Top = 3
      Width = 410
      Height = 40
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      Color = clWhite
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      TabStop = True
      ExplicitLeft = 5
      ExplicitTop = 3
      ExplicitWidth = 410
      ExplicitHeight = 40
      inherited sgPesquisa: TGridLuka
        Width = 385
        Height = 23
        ExplicitWidth = 385
        ExplicitHeight = 23
      end
      inherited PnTitulos: TPanel
        Width = 410
        ExplicitWidth = 410
        inherited lbNomePesquisa: TLabel
          Width = 167
          Caption = 'Cliente cadastrado no sistema'
          ExplicitWidth = 167
          ExplicitHeight = 14
        end
        inherited pnSuprimir: TPanel
          Left = 305
          ExplicitLeft = 305
        end
      end
      inherited pnPesquisa: TPanel
        Left = 385
        Height = 24
        ExplicitLeft = 385
        ExplicitHeight = 24
      end
    end
  end
  object pnCpfNome: TPanel
    Left = 0
    Top = 56
    Width = 421
    Height = 70
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object lb1: TLabel
      Left = 139
      Top = 24
      Width = 32
      Height = 14
      Caption = 'Nome'
    end
    object lbCPF_CNPJ: TLabel
      Left = 5
      Top = 24
      Width = 18
      Height = 14
      Caption = 'CPF'
    end
    object eNome: TEditLuka
      Left = 139
      Top = 39
      Width = 274
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 32
      TabOrder = 2
      OnKeyDown = ProximoCampo
      TipoCampo = tcTexto
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 0
      NaoAceitarEspaco = False
    end
    object stFormasPagamento: TStaticText
      Left = 5
      Top = 4
      Width = 409
      Height = 16
      Alignment = taCenter
      AutoSize = False
      BevelInner = bvNone
      BevelKind = bkFlat
      Caption = 'Dados do cliente n'#227'o cadastrado'
      Color = 15395562
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 0
      Transparent = False
    end
    object eCPF: TEditCPF_CNPJ_Luka
      Left = 5
      Top = 39
      Width = 128
      Height = 22
      EditMask = '999.999.999-99'
      MaxLength = 14
      TabOrder = 1
      Text = '   .   .   -  '
      OnKeyDown = ProximoCampo
      Tipo = [tccCPF]
    end
  end
  object pnEndereco: TPanel
    Left = 0
    Top = 126
    Width = 421
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitLeft = 8
    ExplicitTop = 205
    ExplicitWidth = 418
    ExplicitHeight = 43
    object lb2: TLabel
      Left = 5
      Top = -1
      Width = 50
      Height = 14
      Caption = 'Endere'#231'o'
    end
    object eEndereco: TEditLuka
      Left = 5
      Top = 14
      Width = 410
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 32
      TabOrder = 0
      OnKeyDown = ProximoCampo
      TipoCampo = tcTexto
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 0
      NaoAceitarEspaco = False
    end
  end
end
