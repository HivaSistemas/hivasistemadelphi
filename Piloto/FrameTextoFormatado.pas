unit FrameTextoFormatado;

interface

uses
  Windows, SysUtils, Graphics, Controls, Forms, ComCtrls, StdCtrls, ExtCtrls, _FrameHerancaPrincipal, Classes, _Biblioteca;

type

  TFormAmpliar = class(TForm)
    // ...........
  private
    objeto_local: TWinControl;
    owner_local: TWinControl;

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  public
    constructor Create(AOwner: TComponent); override;
    procedure Ampliar(objeto: TWinControl);
  end;

  TFrTextoFormatado = class(TFrameHerancaPrincipal)
    stbStatus: TStatusBar;
    rtTexto: TRichEdit;
  private
    { Private declarations }
  public
    { Public declarations }

    constructor Create(AOwner: TComponent); override;
    procedure SetFocus; override;

    procedure Clear; override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
    procedure SetTexto(pTexto: string);
    function  GetTexto: string;
    procedure SetInformacaoAdicional(pTexto: string);
    function  EstaVazio: Boolean; override;
  protected
    procedure AmpliarFrame; virtual;
    procedure AjustarQuebraLinha; virtual;
    procedure Atalhos(pSender: TObject; var pKey: Word; pShift: TShiftState); virtual;
  end;

implementation

{$R *.dfm}

procedure TFrTextoFormatado.Atalhos(pSender: TObject; var pKey: Word; pShift: TShiftState);
begin
  if pKey = VK_F11 then
    AmpliarFrame
  else if (pShift = [ssCtrl]) and (pKey = Ord('Q')) then
    AjustarQuebraLinha;
end;

procedure TFrTextoFormatado.Clear;
begin
  inherited;
  rtTexto.Clear;
  SetInformacaoAdicional('');
end;

constructor TFrTextoFormatado.Create(AOwner: TComponent);
begin
  inherited;

  rtTexto.OnKeyDown := Atalhos;
  rtTexto.Paragraph.RightIndent := 15;
end;

function TFrTextoFormatado.EstaVazio: Boolean;
begin
  Result := (Trim(rtTexto.Text) = '');
end;

function TFrTextoFormatado.GetTexto: string;
var
  s: TStringStream;
begin

	Result := '';
  if Trim(rtTexto.Text) = '' then
  	Exit;

  s := nil;
  try
    s := TStringStream.Create('');
    rtTexto.Lines.SaveToStream(s);
    Result := s.DataString;
    // Retirando a terminação nula da string resultante.
    Delete(Result, Length(Result), 1);
  finally
    s.Free;
  end;
end;

procedure TFrTextoFormatado.SetTexto(pTexto: string);
var
  s: TStringStream;
begin

	rtTexto.Clear;
  if pTexto = '' then
  	Exit;

  s := nil;
  try
    s := TStringStream.Create(pTexto);
    rtTexto.Lines.LoadFromStream(s);
  finally
    s.Free;
  end;
end;

procedure TFrTextoFormatado.SetFocus;
begin
  SetarFoco(rtTexto);
end;

procedure TFrTextoFormatado.SetInformacaoAdicional(pTexto: string);
begin
  stbStatus.Panels[2].Text := pTexto;
end;

procedure TFrTextoFormatado.Modo(pEditando: Boolean; pLimpar: Boolean = True);
begin
  inherited;
  rtTexto.ReadOnly := not pEditando;
  if pLimpar then
    Clear;
end;

procedure TFrTextoFormatado.AjustarQuebraLinha;
begin
  rtTexto.WordWrap := not rtTexto.WordWrap;
end;

procedure TFrTextoFormatado.AmpliarFrame;
var
  formulario: TForm;
begin

  if Self.Parent is TFormAmpliar then
    Exit;

  formulario := nil;
  try
    formulario := TFormAmpliar.Create(Application);
    TFormAmpliar(formulario).Ampliar(Self);
  finally
    formulario.Free;
  end;
end;




{ TFormAmpliar }

procedure TFormAmpliar.Ampliar(objeto: TWinControl);
begin
  objeto_local := objeto;
  owner_local := objeto.Parent;
  objeto.Parent := Self;
  Self.ShowModal;
end;

constructor TFormAmpliar.Create(AOwner: TComponent);
begin
  inherited;

  Self.Color := clWhite;
  Self.BorderWidth := 2;
  Self.ClientWidth := 800;
  Self.ClientHeight := 600;
  Self.Position := poDesktopCenter;
  Self.WindowState := wsMaximized;
  Self.BorderIcons := [biSystemMenu, biMaximize];
  Self.OnClose := Self.FormClose;
end;

procedure TFormAmpliar.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  objeto_local.Parent := owner_local;
  Self.Free;
end;

end.
