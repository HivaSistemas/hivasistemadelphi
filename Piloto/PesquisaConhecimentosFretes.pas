unit PesquisaConhecimentosFretes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisasAvancadas, Vcl.Grids, _Sessao,
  GridLuka, Vcl.StdCtrls, EditLuka, FrameDataInicialFinal, FrameTransportadoras, _Biblioteca,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameEmpresas, _ConhecimentosFretes;

type
  TFormPesquisaConhecimentosFretes = class(TFormHerancaPesquisasAvancadas)
    FrEmpresa: TFrEmpresas;
    FrTransportadora: TFrTransportadora;
    FrDataCadastro: TFrDataInicialFinal;
    FrDataEmissao: TFrDataInicialFinal;
    FrDataContabil: TFrDataInicialFinal;
    lbl1: TLabel;
    eNumeroConhecimento: TEditLuka;
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
  private
    FDados: TArray<RecConhecimentosFretes>;
  protected
    procedure BuscarRegistros; override;
  end;

function Pesquisar: TRetornoTelaFinalizar<RecConhecimentosFretes>;

implementation

{$R *.dfm}

const
  coConhecimentoId = 0;
  coTransportadora = 1;
  coNumero         = 2;
  coModelo         = 3;
  coSerie          = 4;
  coEmpresa        = 5;
  coValor          = 6;
  coDataEmissao    = 7;

function Pesquisar: TRetornoTelaFinalizar<RecConhecimentosFretes>;
var
  vForm: TFormPesquisaConhecimentosFretes;
begin
  vForm := TFormPesquisaConhecimentosFretes.Create(nil);
  Result.OkPesquisaAvancada(vForm.ShowModal, True, vForm.sgPesquisa, vForm.FDados);
end;

procedure TFormPesquisaConhecimentosFretes.BuscarRegistros;
var
  i: Integer;
  vSql: string;
begin
  inherited;

  vSql := '';
  FDados := _ConhecimentosFretes.BuscarConhecimentoComando(Sessao.getConexaoBanco, vSql);

  for i := Low(FDados) to High(FDados) do begin
    sgPesquisa.Cells[coConhecimentoId, i + 1] := NFormat(FDados[i].ConhecimentoId);
    sgPesquisa.Cells[coTransportadora, i + 1] := NFormat(FDados[i].TransportadoraId) + ' - ' + FDados[i].NomeTransportadora;
    sgPesquisa.Cells[coNumero, i + 1]         := NFormat(FDados[i].Numero);
    sgPesquisa.Cells[coModelo, i + 1]         := FDados[i].Modelo;
    sgPesquisa.Cells[coSerie, i + 1]          := FDados[i].Serie;
    sgPesquisa.Cells[coEmpresa, i + 1]        := NFormat(FDados[i].EmpresaId) + ' - ' + FDados[i].NomeEmpresa;
    sgPesquisa.Cells[coValor, i + 1]          := NFormat(FDados[i].ValorFrete);
    sgPesquisa.Cells[coDataEmissao, i + 1]    := DFormat(FDados[i].DataEmissao);
  end;
  sgPesquisa.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(FDados) );
end;

procedure TFormPesquisaConhecimentosFretes.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrEmpresa);
end;

procedure TFormPesquisaConhecimentosFretes.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[
    coConhecimentoId,
    coNumero,
    coValor]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
