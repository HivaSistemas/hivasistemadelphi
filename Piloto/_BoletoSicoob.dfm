inherited FormBoletoSicoob: TFormBoletoSicoob
  Caption = 'FormBoletoSicoob'
  ClientHeight = 605
  ClientWidth = 935
  Scaled = False
  ExplicitWidth = 941
  ExplicitHeight = 634
  PixelsPerInch = 96
  TextHeight = 14
  object qrBoletoSICOOB: TQuickRep
    Left = 133
    Top = -406
    Width = 794
    Height = 1123
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE')
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Options = [FirstPageHeader, LastPageFooter]
    Page.Columns = 2
    Page.Orientation = poPortrait
    Page.PaperSize = A4
    Page.Continuous = False
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    PrinterSettings.Copies = 1
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.UseStandardprinter = False
    PrinterSettings.UseCustomBinCode = False
    PrinterSettings.CustomBinCode = 0
    PrinterSettings.ExtendedDuplex = 0
    PrinterSettings.UseCustomPaperCode = False
    PrinterSettings.CustomPaperCode = 0
    PrinterSettings.PrintMetaFile = False
    PrinterSettings.MemoryLimit = 1000000
    PrinterSettings.PrintQuality = 0
    PrinterSettings.Collate = 0
    PrinterSettings.ColorOption = 0
    PrintIfEmpty = True
    SnapToGrid = True
    Units = MM
    Zoom = 100
    PrevFormStyle = fsNormal
    PreviewInitialState = wsMaximized
    PrevInitialZoom = qrZoomToFit
    PreviewDefaultSaveType = stQRP
    PreviewLeft = 0
    PreviewTop = 0
    object qrbnd1: TQRBand
      Left = 38
      Top = 38
      Width = 718
      Height = 1065
      Frame.Style = psClear
      AlignToBottom = False
      Color = clWindow
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        2817.812500000000000000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbTitle
      object QRShape121: TQRShape
        Left = 8
        Top = 131
        Width = 561
        Height = 36
        Size.Values = (
          95.250000000000000000
          21.166666666666670000
          346.604166666666700000
          1484.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape13: TQRShape
        Left = 432
        Top = 131
        Width = 137
        Height = 36
        Size.Values = (
          95.250000000000000000
          1143.000000000000000000
          346.604166666666700000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape125: TQRShape
        Left = 568
        Top = 131
        Width = 137
        Height = 35
        Frame.Color = 14671839
        Size.Values = (
          92.604166666666680000
          1502.833333333333000000
          346.604166666666700000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 13290186
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape127: TQRShape
        Left = 8
        Top = 165
        Width = 561
        Height = 28
        Size.Values = (
          74.083333333333320000
          21.166666666666670000
          436.562500000000000000
          1484.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape16: TQRShape
        Left = 432
        Top = 165
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333320000
          1143.000000000000000000
          436.562500000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape126: TQRShape
        Left = 568
        Top = 165
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333320000
          1502.833333333333000000
          436.562500000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape1: TQRShape
        Left = 8
        Top = 192
        Width = 561
        Height = 55
        Size.Values = (
          145.520833333333300000
          21.166666666666670000
          508.000000000000000000
          1484.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape193: TQRShape
        Left = 568
        Top = 192
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333320000
          1502.833333333333000000
          508.000000000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape199: TQRShape
        Left = 568
        Top = 219
        Width = 137
        Height = 28
        Frame.Color = 14671839
        Size.Values = (
          74.083333333333320000
          1502.833333333333000000
          579.437500000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 13290186
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape122: TQRShape
        Left = 8
        Top = 293
        Width = 561
        Height = 36
        Size.Values = (
          95.250000000000000000
          21.166666666666670000
          775.229166666666800000
          1484.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape207: TQRShape
        Left = 568
        Top = 293
        Width = 137
        Height = 36
        Frame.Color = 14671839
        Size.Values = (
          95.250000000000000000
          1502.833333333333000000
          775.229166666666800000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 13290186
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape212: TQRShape
        Left = 8
        Top = 328
        Width = 561
        Height = 42
        Size.Values = (
          111.125000000000000000
          21.166666666666670000
          867.833333333333200000
          1484.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape211: TQRShape
        Left = 568
        Top = 328
        Width = 137
        Height = 42
        Size.Values = (
          111.125000000000000000
          1502.833333333333000000
          867.833333333333200000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape213: TQRShape
        Left = 8
        Top = 369
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333320000
          21.166666666666670000
          976.312500000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape214: TQRShape
        Left = 144
        Top = 369
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333320000
          381.000000000000000000
          976.312500000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape216: TQRShape
        Left = 256
        Top = 369
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333320000
          677.333333333333200000
          976.312500000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape215: TQRShape
        Left = 368
        Top = 369
        Width = 89
        Height = 28
        Size.Values = (
          74.083333333333320000
          973.666666666666800000
          976.312500000000000000
          235.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape217: TQRShape
        Left = 456
        Top = 369
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333320000
          1206.500000000000000000
          976.312500000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape218: TQRShape
        Left = 568
        Top = 369
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333320000
          1502.833333333333000000
          976.312500000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape219: TQRShape
        Left = 8
        Top = 396
        Width = 96
        Height = 28
        Size.Values = (
          74.083333333333320000
          21.166666666666670000
          1047.750000000000000000
          254.000000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape220: TQRShape
        Left = 103
        Top = 396
        Width = 82
        Height = 28
        Size.Values = (
          74.083333333333320000
          272.520833333333300000
          1047.750000000000000000
          216.958333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape221: TQRShape
        Left = 184
        Top = 396
        Width = 105
        Height = 28
        Size.Values = (
          74.083333333333320000
          486.833333333333300000
          1047.750000000000000000
          277.812500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape222: TQRShape
        Left = 288
        Top = 396
        Width = 145
        Height = 28
        Size.Values = (
          74.083333333333320000
          762.000000000000000000
          1047.750000000000000000
          383.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape223: TQRShape
        Left = 432
        Top = 396
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333320000
          1143.000000000000000000
          1047.750000000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape224: TQRShape
        Left = 568
        Top = 396
        Width = 137
        Height = 28
        Frame.Color = 14671839
        Size.Values = (
          74.083333333333320000
          1502.833333333333000000
          1047.750000000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 13290186
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape225: TQRShape
        Left = 8
        Top = 423
        Width = 561
        Height = 121
        Size.Values = (
          320.145833333333300000
          21.166666666666670000
          1119.187500000000000000
          1484.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape226: TQRShape
        Left = 568
        Top = 423
        Width = 137
        Height = 26
        Size.Values = (
          68.791666666666680000
          1502.833333333333000000
          1119.187500000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape227: TQRShape
        Left = 568
        Top = 447
        Width = 137
        Height = 25
        Size.Values = (
          66.145833333333320000
          1502.833333333333000000
          1182.687500000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape228: TQRShape
        Left = 568
        Top = 471
        Width = 137
        Height = 25
        Size.Values = (
          66.145833333333320000
          1502.833333333333000000
          1246.187500000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape229: TQRShape
        Left = 568
        Top = 495
        Width = 137
        Height = 25
        Size.Values = (
          66.145833333333320000
          1502.833333333333000000
          1309.687500000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape230: TQRShape
        Left = 568
        Top = 519
        Width = 137
        Height = 25
        Size.Values = (
          66.145833333333320000
          1502.833333333333000000
          1373.187500000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 14671839
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape231: TQRShape
        Left = 8
        Top = 543
        Width = 697
        Height = 59
        Size.Values = (
          156.104166666666700000
          21.166666666666670000
          1436.687500000000000000
          1844.145833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape95: TQRShape
        Left = 8
        Top = 715
        Width = 561
        Height = 36
        Size.Values = (
          95.250000000000000000
          21.166666666666670000
          1891.770833333333000000
          1484.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape98: TQRShape
        Left = 568
        Top = 715
        Width = 137
        Height = 36
        Frame.Color = 14671839
        Size.Values = (
          95.250000000000000000
          1502.833333333333000000
          1891.770833333333000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 13290186
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape100: TQRShape
        Left = 8
        Top = 750
        Width = 561
        Height = 28
        Size.Values = (
          74.083333333333320000
          21.166666666666670000
          1984.375000000000000000
          1484.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape99: TQRShape
        Left = 568
        Top = 750
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333320000
          1502.833333333333000000
          1984.375000000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape101: TQRShape
        Left = 8
        Top = 777
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333320000
          21.166666666666670000
          2055.812500000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape102: TQRShape
        Left = 144
        Top = 777
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333320000
          381.000000000000000000
          2055.812500000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape104: TQRShape
        Left = 256
        Top = 777
        Width = 97
        Height = 28
        Size.Values = (
          74.083333333333320000
          677.333333333333200000
          2055.812500000000000000
          256.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape103: TQRShape
        Left = 352
        Top = 777
        Width = 89
        Height = 28
        Size.Values = (
          74.083333333333320000
          931.333333333333200000
          2055.812500000000000000
          235.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape105: TQRShape
        Left = 440
        Top = 777
        Width = 129
        Height = 28
        Size.Values = (
          74.083333333333320000
          1164.166666666667000000
          2055.812500000000000000
          341.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape106: TQRShape
        Left = 568
        Top = 777
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333320000
          1502.833333333333000000
          2055.812500000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape107: TQRShape
        Left = 8
        Top = 804
        Width = 96
        Height = 28
        Size.Values = (
          74.083333333333320000
          21.166666666666670000
          2127.250000000000000000
          254.000000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape108: TQRShape
        Left = 103
        Top = 804
        Width = 82
        Height = 28
        Size.Values = (
          74.083333333333320000
          272.520833333333300000
          2127.250000000000000000
          216.958333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape109: TQRShape
        Left = 184
        Top = 804
        Width = 105
        Height = 28
        Size.Values = (
          74.083333333333320000
          486.833333333333300000
          2127.250000000000000000
          277.812500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape110: TQRShape
        Left = 288
        Top = 804
        Width = 145
        Height = 28
        Size.Values = (
          74.083333333333320000
          762.000000000000000000
          2127.250000000000000000
          383.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape111: TQRShape
        Left = 432
        Top = 804
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333320000
          1143.000000000000000000
          2127.250000000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape112: TQRShape
        Left = 568
        Top = 804
        Width = 137
        Height = 28
        Frame.Color = 14671839
        Size.Values = (
          74.083333333333320000
          1502.833333333333000000
          2127.250000000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 13290186
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape113: TQRShape
        Left = 8
        Top = 831
        Width = 561
        Height = 121
        Size.Values = (
          320.145833333333400000
          21.166666666666670000
          2198.687500000000000000
          1484.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape114: TQRShape
        Left = 568
        Top = 831
        Width = 137
        Height = 26
        Size.Values = (
          68.791666666666680000
          1502.833333333333000000
          2198.687500000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape115: TQRShape
        Left = 568
        Top = 855
        Width = 137
        Height = 25
        Size.Values = (
          66.145833333333340000
          1502.833333333333000000
          2262.187500000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape116: TQRShape
        Left = 568
        Top = 879
        Width = 137
        Height = 25
        Size.Values = (
          66.145833333333340000
          1502.833333333333000000
          2325.687500000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape117: TQRShape
        Left = 568
        Top = 903
        Width = 137
        Height = 25
        Size.Values = (
          66.145833333333340000
          1502.833333333333000000
          2389.187500000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape118: TQRShape
        Left = 568
        Top = 927
        Width = 137
        Height = 25
        Size.Values = (
          66.145833333333340000
          1502.833333333333000000
          2452.687500000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 14671839
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape119: TQRShape
        Left = 8
        Top = 951
        Width = 697
        Height = 59
        Size.Values = (
          156.104166666666700000
          21.166666666666670000
          2516.187500000000000000
          1844.145833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qr1: TQRLabel
        Left = 12
        Top = 717
        Width = 301
        Height = 15
        Size.Values = (
          39.687500000000000000
          31.750000000000000000
          1897.062500000000000000
          796.395833333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Local de Pagamento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr2: TQRLabel
        Left = 571
        Top = 716
        Width = 132
        Height = 16
        Size.Values = (
          42.333333333333340000
          1510.770833333333000000
          1894.416666666667000000
          349.250000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vencimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrVencimentoBra: TQRLabel
        Left = 570
        Top = 734
        Width = 133
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1942.041666666667000000
          351.895833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrVencimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr3: TQRLabel
        Left = 12
        Top = 752
        Width = 193
        Height = 14
        Size.Values = (
          37.041666666666670000
          31.750000000000000000
          1989.666666666667000000
          510.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Benefici'#225'rio'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr4: TQRLabel
        Left = 571
        Top = 752
        Width = 130
        Height = 12
        Size.Values = (
          31.750000000000000000
          1510.770833333333000000
          1989.666666666667000000
          343.958333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ag'#234'ncia/C'#243'd.Benefici'#225'rio'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr5: TQRLabel
        Left = 520
        Top = 1011
        Width = 121
        Height = 14
        Size.Values = (
          37.041666666666670000
          1375.833333333333000000
          2674.937500000000000000
          320.145833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Autentica'#231#227'o Mec'#226'nica'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr6: TQRLabel
        Left = 12
        Top = 779
        Width = 101
        Height = 12
        Size.Values = (
          31.750000000000000000
          31.750000000000000000
          2061.104166666667000000
          267.229166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Data do Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr7: TQRLabel
        Left = 149
        Top = 779
        Width = 107
        Height = 12
        Size.Values = (
          31.750000000000000000
          394.229166666666700000
          2061.104166666667000000
          283.104166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nr. Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr8: TQRLabel
        Left = 260
        Top = 779
        Width = 85
        Height = 12
        Size.Values = (
          31.750000000000000000
          687.916666666666800000
          2061.104166666667000000
          224.895833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Esp'#233'cie Doc.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr9: TQRLabel
        Left = 444
        Top = 779
        Width = 122
        Height = 12
        Size.Values = (
          31.750000000000000000
          1174.750000000000000000
          2061.104166666667000000
          322.791666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Data Processamento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr10: TQRLabel
        Left = 356
        Top = 779
        Width = 81
        Height = 12
        Size.Values = (
          31.750000000000000000
          941.916666666666800000
          2061.104166666667000000
          214.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Aceite'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr11: TQRLabel
        Left = 12
        Top = 806
        Width = 77
        Height = 12
        Size.Values = (
          31.750000000000000000
          31.750000000000000000
          2132.541666666667000000
          203.729166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Uso do Banco'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr12: TQRLabel
        Left = 107
        Top = 806
        Width = 65
        Height = 12
        Size.Values = (
          31.750000000000000000
          283.104166666666700000
          2132.541666666667000000
          171.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Carteira'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr13: TQRLabel
        Left = 188
        Top = 806
        Width = 85
        Height = 12
        Size.Values = (
          31.750000000000000000
          497.416666666666700000
          2132.541666666667000000
          224.895833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Esp'#233'cie Moeda'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr14: TQRLabel
        Left = 292
        Top = 806
        Width = 108
        Height = 12
        Size.Values = (
          31.750000000000000000
          772.583333333333200000
          2132.541666666667000000
          285.750000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Quantidade'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr15: TQRLabel
        Left = 440
        Top = 806
        Width = 127
        Height = 12
        Size.Values = (
          31.750000000000000000
          1164.166666666667000000
          2132.541666666667000000
          336.020833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Valor'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrInstrucaoBra: TQRLabel
        Left = 12
        Top = 845
        Width = 551
        Height = 60
        Size.Values = (
          158.750000000000000000
          31.750000000000000000
          2235.729166666667000000
          1457.854166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrInstrucaoBra'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr16: TQRLabel
        Left = 12
        Top = 955
        Width = 88
        Height = 14
        Size.Values = (
          37.041666666666670000
          31.750000000000000000
          2526.770833333333000000
          232.833333333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Pagador'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr17: TQRLabel
        Left = 571
        Top = 779
        Width = 130
        Height = 12
        Size.Values = (
          32.000000000000000000
          1510.770833333333000000
          2061.000000000000000000
          343.958333333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nosso N'#250'mero'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr18: TQRLabel
        Left = 571
        Top = 805
        Width = 130
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          2129.895833333333000000
          343.958333333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor do Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr19: TQRLabel
        Left = 571
        Top = 833
        Width = 130
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          2203.979166666667000000
          343.958333333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(-) Desconto'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qr20: TQRLabel
        Left = 571
        Top = 857
        Width = 133
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          2267.479166666667000000
          351.895833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(-) Outras Dedu'#231#245'es/Abatimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qr21: TQRLabel
        Left = 571
        Top = 881
        Width = 130
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          2330.979166666667000000
          343.958333333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(+) Mora / Multa / Juros'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qr22: TQRLabel
        Left = 571
        Top = 904
        Width = 130
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          2391.833333333333000000
          343.958333333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(+) Outros Acr'#233'scimos'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qr23: TQRLabel
        Left = 571
        Top = 928
        Width = 130
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          2455.333333333333000000
          343.958333333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(=) Valor Cobrado'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrBeneficiario: TQRLabel
        Left = 12
        Top = 763
        Width = 419
        Height = 13
        Size.Values = (
          34.395833333333330000
          31.750000000000000000
          2018.770833333333000000
          1108.604166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' qrBeneficiario'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrDataEmissaoBra: TQRLabel
        Left = 12
        Top = 789
        Width = 92
        Height = 12
        Size.Values = (
          31.750000000000000000
          31.750000000000000000
          2087.562500000000000000
          243.416666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrDataEmissao'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrNumeroDocumentoBra: TQRLabel
        Left = 149
        Top = 789
        Width = 107
        Height = 13
        Size.Values = (
          34.395833333333330000
          394.229166666666700000
          2087.562500000000000000
          283.104166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNrDocumento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrEspecieDocumentoBra: TQRLabel
        Left = 260
        Top = 789
        Width = 85
        Height = 13
        Size.Values = (
          34.395833333333330000
          687.916666666666800000
          2087.562500000000000000
          224.895833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'DM'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr24: TQRLabel
        Left = 188
        Top = 818
        Width = 85
        Height = 12
        Size.Values = (
          31.750000000000000000
          497.416666666666700000
          2164.291666666667000000
          224.895833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'R$'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrCarteiraBra: TQRLabel
        Left = 107
        Top = 818
        Width = 65
        Height = 12
        Size.Values = (
          31.750000000000000000
          283.104166666666700000
          2164.291666666667000000
          171.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '99'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrAceiteBra: TQRLabel
        Left = 356
        Top = 789
        Width = 81
        Height = 13
        Size.Values = (
          34.395833333333330000
          941.916666666666800000
          2087.562500000000000000
          214.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'N'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrDataProcessamentoBra: TQRLabel
        Left = 444
        Top = 789
        Width = 122
        Height = 12
        Size.Values = (
          31.750000000000000000
          1174.750000000000000000
          2087.562500000000000000
          322.791666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrDataProcessamento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrValorBra: TQRLabel
        Left = 440
        Top = 818
        Width = 127
        Height = 12
        Enabled = False
        Size.Values = (
          31.750000000000000000
          1164.166666666667000000
          2164.291666666667000000
          336.020833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Valor'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrPagador: TQRLabel
        Left = 108
        Top = 953
        Width = 253
        Height = 12
        Size.Values = (
          31.750000000000000000
          285.750000000000000000
          2521.479166666667000000
          669.395833333333200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrPagador'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrValorDocumentoBra: TQRLabel
        Left = 571
        Top = 816
        Width = 130
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          2159.000000000000000000
          343.958333333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Valor'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrAgenciaBeneficiario: TQRLabel
        Left = 571
        Top = 763
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333340000
          1510.770833333333000000
          2018.770833333333000000
          343.958333333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '9999-D / 999999-D'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr25: TQRLabel
        Left = 12
        Top = 734
        Width = 517
        Height = 15
        Size.Values = (
          39.687500000000000000
          31.750000000000000000
          1942.041666666667000000
          1367.895833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'PAGAVEL PREFERENCIALMENTE NO SICOOB'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrNossoNumeroBra: TQRLabel
        Left = 572
        Top = 790
        Width = 131
        Height = 12
        Size.Values = (
          31.750000000000000000
          1513.416666666667000000
          2090.208333333333000000
          346.604166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNossoNumeroBra'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr26: TQRLabel
        Left = 12
        Top = 994
        Width = 93
        Height = 14
        Size.Values = (
          37.041666666666670000
          31.750000000000000000
          2629.958333333333000000
          246.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Sacador/Avalista'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRShape123: TQRShape
        Left = 8
        Top = 1088
        Width = 697
        Height = 14
        Frame.Style = psDot
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          2878.666666666667000000
          1844.145833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Pen.Style = psDot
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object qrCodigoBarraBra: TQRBarcode
        Left = 18
        Top = 1012
        Width = 389
        Height = 43
        Size.Values = (
          113.770833333333300000
          47.625000000000000000
          2677.583333333333000000
          1029.229166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Picture.Data = {
          07544269746D61707AAC0000424D7AAC00000000000036000000280000006900
          000069000000010020000000000044AC00000000000000000000000000000000
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF0000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
          FFFF00000000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF00FFFFFFFF00000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF0000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
          FFFF00000000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF00FFFFFFFF00000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF0000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
          FFFF00000000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF00FFFFFFFF00000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF0000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
          FFFF00000000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF00FFFFFFFF00000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF00000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFFFF0000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          0000FFFFFF000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
          FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF000000
          000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
          0000FFFFFF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFF
          FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
          0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF000000
          000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
          FFFF00000000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
          FF000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00000000000000
          0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
          FF0000000000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF
          FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF}
        BarHeight = 35
        BarTop = 1
        BarLeft = 1
        BarModul = 1
        BarRatio = 2.000000000000000000
        BarTyp = bcCode_2_5_interleaved
        BarText = '00000000000000000000000000000000000000000000'
      end
      object QRShape124: TQRShape
        Left = 8
        Top = 1065
        Width = 697
        Height = 14
        Frame.Style = psDot
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          2817.812500000000000000
          1844.145833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Pen.Style = psDot
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object qr27: TQRLabel
        Left = 571
        Top = 132
        Width = 132
        Height = 16
        Size.Values = (
          42.333333333333330000
          1510.770833333333000000
          349.250000000000000000
          349.250000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vencimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrVencimentoBra3: TQRLabel
        Left = 571
        Top = 150
        Width = 132
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          396.875000000000000000
          349.250000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrVencimento3'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr28: TQRLabel
        Left = 12
        Top = 136
        Width = 193
        Height = 13
        Size.Values = (
          34.395833333333330000
          31.750000000000000000
          359.833333333333300000
          510.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Benefici'#225'rio'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr29: TQRLabel
        Left = 571
        Top = 194
        Width = 132
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          513.291666666666700000
          349.250000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nosso N'#250'mero'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr30: TQRLabel
        Left = 571
        Top = 221
        Width = 132
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          584.729166666666800000
          349.250000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor do Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrBeneficiario3: TQRLabel
        Left = 12
        Top = 150
        Width = 389
        Height = 14
        Size.Values = (
          37.041666666666670000
          31.750000000000000000
          396.875000000000000000
          1029.229166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' qrBeneficiario'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrValorDocumentoBra3: TQRLabel
        Left = 571
        Top = 232
        Width = 132
        Height = 13
        Size.Values = (
          34.395833333333330000
          1510.770833333333000000
          613.833333333333200000
          349.250000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Valor'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrAgenciaBeneficiario3: TQRLabel
        Left = 435
        Top = 150
        Width = 132
        Height = 14
        Size.Values = (
          37.041666666666670000
          1150.937500000000000000
          396.875000000000000000
          349.250000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '9999-D / 999999-D'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrNossoNumeroBra3: TQRLabel
        Left = 571
        Top = 206
        Width = 132
        Height = 12
        Size.Values = (
          31.750000000000000000
          1510.770833333333000000
          545.041666666666700000
          349.250000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNossoNumeroBra3'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr31: TQRLabel
        Left = 240
        Top = 695
        Width = 48
        Height = 19
        Size.Values = (
          50.270833333333330000
          635.000000000000000000
          1838.854166666667000000
          127.000000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '756-0'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 11
      end
      object QRShape132: TQRShape
        Left = 232
        Top = 692
        Width = 8
        Height = 23
        Frame.Style = psDot
        Size.Values = (
          60.854166666666680000
          613.833333333333200000
          1830.916666666667000000
          21.166666666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape133: TQRShape
        Left = 287
        Top = 692
        Width = 7
        Height = 23
        Frame.Style = psDot
        Size.Values = (
          60.854166666666680000
          759.354166666666800000
          1830.916666666667000000
          18.520833333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qrLinhaDigitavelBRA: TQRLabel
        Left = 316
        Top = 698
        Width = 389
        Height = 17
        Size.Values = (
          44.979166666666670000
          836.083333333333400000
          1846.791666666667000000
          1029.229166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrLinhaDigitavelBarras'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qr32: TQRLabel
        Left = 12
        Top = 295
        Width = 301
        Height = 15
        Size.Values = (
          39.687500000000000000
          31.750000000000000000
          780.520833333333200000
          796.395833333333200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Local de Pagamento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr33: TQRLabel
        Left = 571
        Top = 294
        Width = 132
        Height = 16
        Size.Values = (
          42.333333333333330000
          1510.770833333333000000
          777.875000000000000000
          349.250000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vencimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrVencimentoBra2: TQRLabel
        Left = 571
        Top = 312
        Width = 132
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          825.500000000000000000
          349.250000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrVencimento3'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr34: TQRLabel
        Left = 12
        Top = 330
        Width = 193
        Height = 12
        Size.Values = (
          31.750000000000000000
          31.750000000000000000
          873.125000000000000000
          510.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Benefici'#225'rio'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr35: TQRLabel
        Left = 571
        Top = 330
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333330000
          1510.770833333333000000
          873.125000000000000000
          343.958333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ag'#234'ncia/C'#243'd.Benefici'#225'rio'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr36: TQRLabel
        Left = 12
        Top = 371
        Width = 93
        Height = 12
        Size.Values = (
          31.750000000000000000
          31.750000000000000000
          981.604166666666800000
          246.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Data de Emiss'#227'o'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr37: TQRLabel
        Left = 149
        Top = 371
        Width = 106
        Height = 12
        Size.Values = (
          31.750000000000000000
          394.229166666666700000
          981.604166666666800000
          280.458333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nr. Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr38: TQRLabel
        Left = 260
        Top = 371
        Width = 107
        Height = 12
        Size.Values = (
          31.750000000000000000
          687.916666666666800000
          981.604166666666800000
          283.104166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Esp'#233'cie Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr39: TQRLabel
        Left = 460
        Top = 371
        Width = 107
        Height = 12
        Size.Values = (
          31.750000000000000000
          1217.083333333333000000
          981.604166666666800000
          283.104166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Data Processamento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr40: TQRLabel
        Left = 372
        Top = 371
        Width = 81
        Height = 12
        Size.Values = (
          31.750000000000000000
          984.250000000000000000
          981.604166666666800000
          214.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Aceite'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr41: TQRLabel
        Left = 12
        Top = 398
        Width = 87
        Height = 12
        Size.Values = (
          31.750000000000000000
          31.750000000000000000
          1053.041666666667000000
          230.187500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Uso do Banco'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr42: TQRLabel
        Left = 107
        Top = 398
        Width = 65
        Height = 12
        Size.Values = (
          31.750000000000000000
          283.104166666666700000
          1053.041666666667000000
          171.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Carteira'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr43: TQRLabel
        Left = 188
        Top = 398
        Width = 85
        Height = 12
        Size.Values = (
          31.750000000000000000
          497.416666666666700000
          1053.041666666667000000
          224.895833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Esp'#233'cie Moeda'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr44: TQRLabel
        Left = 292
        Top = 398
        Width = 108
        Height = 12
        Size.Values = (
          31.750000000000000000
          772.583333333333200000
          1053.041666666667000000
          285.750000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Quantidade'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr45: TQRLabel
        Left = 440
        Top = 398
        Width = 127
        Height = 12
        Size.Values = (
          31.750000000000000000
          1164.166666666667000000
          1053.041666666667000000
          336.020833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Valor'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrInstrucaoBra2: TQRLabel
        Left = 12
        Top = 438
        Width = 551
        Height = 57
        Size.Values = (
          150.812500000000000000
          31.750000000000000000
          1158.875000000000000000
          1457.854166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrInstrucaoBra3'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr46: TQRLabel
        Left = 12
        Top = 547
        Width = 88
        Height = 14
        Size.Values = (
          37.041666666666670000
          31.750000000000000000
          1447.270833333333000000
          232.833333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Pagador'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr47: TQRLabel
        Left = 571
        Top = 371
        Width = 130
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          981.604166666666800000
          343.958333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nosso N'#250'mero'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr48: TQRLabel
        Left = 571
        Top = 398
        Width = 130
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          1053.041666666667000000
          343.958333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor do Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr49: TQRLabel
        Left = 571
        Top = 426
        Width = 130
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          1127.125000000000000000
          343.958333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(-) Desconto'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qr50: TQRLabel
        Left = 571
        Top = 449
        Width = 130
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          1187.979166666667000000
          343.958333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(-) Outras Dedu'#231#245'es'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qr51: TQRLabel
        Left = 571
        Top = 473
        Width = 130
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          1251.479166666667000000
          343.958333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(+) Mora/Multa'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qr52: TQRLabel
        Left = 571
        Top = 497
        Width = 130
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          1314.979166666667000000
          343.958333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(+) Outros Acr'#233'scimos'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qr53: TQRLabel
        Left = 571
        Top = 521
        Width = 130
        Height = 14
        Size.Values = (
          37.041666666666670000
          1510.770833333333000000
          1378.479166666667000000
          343.958333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(=) Valor Cobrado'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrBeneficiario2: TQRLabel
        Left = 12
        Top = 342
        Width = 419
        Height = 12
        Size.Values = (
          31.750000000000000000
          31.750000000000000000
          904.875000000000000000
          1108.604166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' qrBeneficiario'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrDataEmissaoBra2: TQRLabel
        Left = 12
        Top = 382
        Width = 93
        Height = 12
        Size.Values = (
          31.750000000000000000
          31.750000000000000000
          1010.708333333333000000
          246.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrDataEmissao'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrNumeroDocumentoBra2: TQRLabel
        Left = 149
        Top = 382
        Width = 106
        Height = 12
        Size.Values = (
          31.750000000000000000
          394.229166666666700000
          1010.708333333333000000
          280.458333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNrDocumento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrEspecieDocumentoBra2: TQRLabel
        Left = 260
        Top = 382
        Width = 107
        Height = 12
        Size.Values = (
          31.750000000000000000
          687.916666666666800000
          1010.708333333333000000
          283.104166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'DM'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr54: TQRLabel
        Left = 188
        Top = 409
        Width = 85
        Height = 14
        Size.Values = (
          37.041666666666670000
          497.416666666666700000
          1082.145833333333000000
          224.895833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'R$'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrCarteiraBra2: TQRLabel
        Left = 107
        Top = 409
        Width = 65
        Height = 14
        Size.Values = (
          37.041666666666670000
          283.104166666666700000
          1082.145833333333000000
          171.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '1'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrAceite: TQRLabel
        Left = 372
        Top = 382
        Width = 81
        Height = 12
        Size.Values = (
          31.750000000000000000
          984.250000000000000000
          1010.708333333333000000
          214.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'N'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrDataProcessamentoBra2: TQRLabel
        Left = 460
        Top = 382
        Width = 107
        Height = 12
        Size.Values = (
          31.750000000000000000
          1217.083333333333000000
          1010.708333333333000000
          283.104166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrDataProcessamento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrValorBra2: TQRLabel
        Left = 440
        Top = 408
        Width = 127
        Height = 14
        Enabled = False
        Size.Values = (
          37.041666666666670000
          1164.166666666667000000
          1079.500000000000000000
          336.020833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Valor'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrPagador2: TQRLabel
        Left = 108
        Top = 548
        Width = 299
        Height = 14
        Size.Values = (
          37.041666666666670000
          285.750000000000000000
          1449.916666666667000000
          791.104166666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrPagador2'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrValorDocumentoBra2: TQRLabel
        Left = 571
        Top = 409
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333330000
          1510.770833333333000000
          1082.145833333333000000
          343.958333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Valor'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrAgenciaBeneficiario2: TQRLabel
        Left = 571
        Top = 348
        Width = 130
        Height = 12
        Size.Values = (
          31.750000000000000000
          1510.770833333333000000
          920.750000000000000000
          343.958333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '9999-D / 999999-D'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr55: TQRLabel
        Left = 12
        Top = 312
        Width = 517
        Height = 15
        Size.Values = (
          39.687500000000000000
          31.750000000000000000
          825.500000000000000000
          1367.895833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'PAGAVEL PREFERENCIALMENTE NO SICOOB'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrNossoNumeroBra2: TQRLabel
        Left = 571
        Top = 382
        Width = 130
        Height = 13
        Size.Values = (
          34.395833333333330000
          1510.770833333333000000
          1010.708333333333000000
          343.958333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNossoNumeroBra2'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr56: TQRLabel
        Left = 12
        Top = 588
        Width = 93
        Height = 13
        Size.Values = (
          34.395833333333330000
          31.750000000000000000
          1555.750000000000000000
          246.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Sacador/Avalista'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr57: TQRLabel
        Left = 506
        Top = 113
        Width = 199
        Height = 17
        Size.Values = (
          44.979166666666670000
          1338.791666666667000000
          298.979166666666700000
          526.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Recibo do Benefici'#225'rio'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRShape236: TQRShape
        Left = 8
        Top = 253
        Width = 697
        Height = 14
        Frame.Style = psDot
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          669.395833333333200000
          1844.145833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Pen.Style = psDot
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object qrCpfCnpjBra: TQRLabel
        Left = 418
        Top = 953
        Width = 95
        Height = 12
        Size.Values = (
          31.750000000000000000
          1105.958333333333000000
          2521.479166666667000000
          251.354166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrCpfCnpjBra'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrLogradouroBra: TQRLabel
        Left = 108
        Top = 966
        Width = 405
        Height = 12
        Size.Values = (
          31.750000000000000000
          285.750000000000000000
          2555.875000000000000000
          1071.562500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrLogradouro'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrCepBra: TQRLabel
        Left = 108
        Top = 979
        Width = 71
        Height = 12
        Size.Values = (
          31.750000000000000000
          285.750000000000000000
          2590.270833333333000000
          187.854166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrCep'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrCidadeBra: TQRLabel
        Left = 186
        Top = 979
        Width = 327
        Height = 12
        Size.Values = (
          31.750000000000000000
          492.125000000000000000
          2590.270833333333000000
          865.187500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrCidade'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrCpfCnpjBra2: TQRLabel
        Left = 464
        Top = 548
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          1227.666666666667000000
          1449.916666666667000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrCpfCnpj'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrLogradouroBra2: TQRLabel
        Left = 108
        Top = 561
        Width = 469
        Height = 12
        Size.Values = (
          31.750000000000000000
          285.750000000000000000
          1484.312500000000000000
          1240.895833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrLogradouro'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrCepBra2: TQRLabel
        Left = 108
        Top = 573
        Width = 71
        Height = 14
        Size.Values = (
          37.041666666666670000
          285.750000000000000000
          1516.062500000000000000
          187.854166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrCep'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrCidadeBra2: TQRLabel
        Left = 186
        Top = 573
        Width = 391
        Height = 14
        Size.Values = (
          37.041666666666670000
          492.125000000000000000
          1516.062500000000000000
          1034.520833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrCidade'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRShape235: TQRShape
        Left = 8
        Top = 673
        Width = 697
        Height = 14
        Frame.Style = psDot
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1780.645833333333000000
          1844.145833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Pen.Style = psDot
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object qr58: TQRLabel
        Left = 12
        Top = 426
        Width = 149
        Height = 13
        Size.Values = (
          34.395833333333330000
          31.750000000000000000
          1127.125000000000000000
          394.229166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Instru'#231#245'es'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr59: TQRLabel
        Left = 12
        Top = 833
        Width = 517
        Height = 13
        Size.Values = (
          34.395833333333330000
          31.750000000000000000
          2203.979166666667000000
          1367.895833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 
          'Instru'#231#245'es ( Todas informa'#231#245'es deste bloqueto s'#227'o de exclusiva r' +
          'esponsabilidade do benefici'#225'rio )'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrNumero_Nota1: TQRLabel
        Left = 8
        Top = 500
        Width = 276
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          21.166666666666670000
          1322.916666666667000000
          730.250000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNumero_Nota1'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrNumero_Nota: TQRLabel
        Left = 12
        Top = 916
        Width = 282
        Height = 14
        Enabled = False
        Size.Values = (
          37.041666666666670000
          31.750000000000000000
          2423.583333333333000000
          746.125000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNumero_Nota'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr60: TQRLabel
        Left = 324
        Top = 196
        Width = 237
        Height = 14
        Size.Values = (
          37.041666666666670000
          857.250000000000000000
          518.583333333333300000
          627.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Assinatura do cliente por extenso'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr61: TQRLabel
        Left = 12
        Top = 167
        Width = 409
        Height = 13
        Size.Values = (
          34.395833333333330000
          31.750000000000000000
          441.854166666666700000
          1082.145833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Pagador'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrPagador3: TQRLabel
        Left = 12
        Top = 178
        Width = 419
        Height = 12
        Size.Values = (
          31.750000000000000000
          31.750000000000000000
          470.958333333333300000
          1108.604166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrPagador3'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrNumeroDocumentoBra3: TQRLabel
        Left = 571
        Top = 178
        Width = 132
        Height = 13
        Size.Values = (
          34.395833333333330000
          1510.770833333333000000
          470.958333333333300000
          349.250000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNrDocumento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr62: TQRLabel
        Left = 571
        Top = 167
        Width = 132
        Height = 12
        Size.Values = (
          31.750000000000000000
          1510.770833333333000000
          441.854166666666700000
          349.250000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nr. Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrPedido: TQRLabel
        Left = 295
        Top = 916
        Width = 268
        Height = 14
        Enabled = False
        Size.Values = (
          37.041666666666670000
          780.520833333333200000
          2423.583333333333000000
          709.083333333333200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrPedido'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrPedido1: TQRLabel
        Left = 290
        Top = 506
        Width = 273
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          767.291666666666800000
          1338.791666666667000000
          722.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrPedido1'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrDocumento: TQRLabel
        Left = 12
        Top = 903
        Width = 551
        Height = 14
        Enabled = False
        Size.Values = (
          37.041666666666670000
          31.750000000000000000
          2389.187500000000000000
          1457.854166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrDocumento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrDocumento1: TQRLabel
        Left = 12
        Top = 493
        Width = 551
        Height = 14
        Enabled = False
        Size.Values = (
          37.041666666666670000
          31.750000000000000000
          1304.395833333333000000
          1457.854166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrDocumento1'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrlDataEmissao: TQRLabel
        Left = 435
        Top = 167
        Width = 132
        Height = 12
        Size.Values = (
          31.750000000000000000
          1150.937500000000000000
          441.854166666666700000
          349.250000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Data de Emiss'#227'o'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrDataEmissaoBra3: TQRLabel
        Left = 435
        Top = 178
        Width = 132
        Height = 12
        Size.Values = (
          31.750000000000000000
          1150.937500000000000000
          470.958333333333300000
          349.250000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '99/99/9999'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr63: TQRLabel
        Left = 427
        Top = 813
        Width = 10
        Height = 12
        Size.Values = (
          31.750000000000000000
          1129.770833333333000000
          2151.062500000000000000
          26.458333333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'X'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrBeneficiarioAvalista: TQRLabel
        Left = 108
        Top = 994
        Width = 293
        Height = 12
        Size.Values = (
          31.750000000000000000
          285.750000000000000000
          2629.958333333333000000
          775.229166666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrAvalista'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr64: TQRLabel
        Left = 559
        Top = 1046
        Width = 137
        Height = 14
        Size.Values = (
          37.041666666666670000
          1479.020833333333000000
          2767.541666666667000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'FICHA DE COMPENSA'#199#195'O'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRShape2: TQRShape
        Left = 641
        Top = 1013
        Width = 64
        Height = 8
        Size.Values = (
          21.166666666666670000
          1695.979166666667000000
          2680.229166666667000000
          169.333333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape3: TQRShape
        Left = 457
        Top = 1013
        Width = 64
        Height = 8
        Size.Values = (
          21.166666666666670000
          1209.145833333333000000
          2680.229166666667000000
          169.333333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape4: TQRShape
        Left = 454
        Top = 1017
        Width = 8
        Height = 25
        Size.Values = (
          66.145833333333320000
          1201.208333333333000000
          2690.812500000000000000
          21.166666666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape5: TQRShape
        Left = 700
        Top = 1017
        Width = 8
        Height = 25
        Size.Values = (
          66.145833333333320000
          1852.083333333333000000
          2690.812500000000000000
          21.166666666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qr65: TQRLabel
        Left = 522
        Top = 994
        Width = 71
        Height = 12
        Enabled = False
        Size.Values = (
          31.750000000000000000
          1381.125000000000000000
          2629.958333333333000000
          187.854166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'C'#243'digo de baixa'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr66: TQRLabel
        Left = 240
        Top = 111
        Width = 48
        Height = 19
        Size.Values = (
          50.270833333333330000
          635.000000000000000000
          293.687500000000000000
          127.000000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '756-0'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 11
      end
      object QRShape6: TQRShape
        Left = 232
        Top = 107
        Width = 8
        Height = 23
        Frame.Style = psDot
        Size.Values = (
          60.854166666666680000
          613.833333333333200000
          283.104166666666700000
          21.166666666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape7: TQRShape
        Left = 287
        Top = 107
        Width = 7
        Height = 23
        Frame.Style = psDot
        Size.Values = (
          60.854166666666680000
          759.354166666666800000
          283.104166666666700000
          18.520833333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qr67: TQRLabel
        Left = 240
        Top = 272
        Width = 48
        Height = 19
        Size.Values = (
          50.270833333333330000
          635.000000000000000000
          719.666666666666800000
          127.000000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '756-0'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 11
      end
      object QRShape10: TQRShape
        Left = 232
        Top = 268
        Width = 8
        Height = 23
        Frame.Style = psDot
        Size.Values = (
          60.854166666666680000
          613.833333333333200000
          709.083333333333200000
          21.166666666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape11: TQRShape
        Left = 287
        Top = 268
        Width = 7
        Height = 23
        Frame.Style = psDot
        Size.Values = (
          60.854166666666680000
          759.354166666666800000
          709.083333333333200000
          18.520833333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qr68: TQRLabel
        Left = 427
        Top = 405
        Width = 10
        Height = 12
        Size.Values = (
          31.750000000000000000
          1129.770833333333000000
          1071.562500000000000000
          26.458333333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'X'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr69: TQRLabel
        Left = 480
        Top = 611
        Width = 121
        Height = 14
        Size.Values = (
          37.041666666666670000
          1270.000000000000000000
          1616.604166666667000000
          320.145833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Autentica'#231#227'o Mec'#226'nica'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRShape12: TQRShape
        Left = 384
        Top = 605
        Width = 321
        Height = 8
        Size.Values = (
          21.166666666666670000
          1016.000000000000000000
          1600.729166666667000000
          849.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape14: TQRShape
        Left = 382
        Top = 609
        Width = 8
        Height = 25
        Size.Values = (
          66.145833333333320000
          1010.708333333333000000
          1611.312500000000000000
          21.166666666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape15: TQRShape
        Left = 700
        Top = 609
        Width = 8
        Height = 25
        Size.Values = (
          66.145833333333320000
          1852.083333333333000000
          1611.312500000000000000
          21.166666666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qr70: TQRLabel
        Left = 435
        Top = 136
        Width = 132
        Height = 13
        Size.Values = (
          34.395833333333330000
          1150.937500000000000000
          359.833333333333300000
          349.250000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ag'#234'ncia/C'#243'd.Benefici'#225'rio'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrDocumento2: TQRLabel
        Left = 12
        Top = 194
        Width = 305
        Height = 14
        Size.Values = (
          37.041666666666670000
          31.750000000000000000
          513.291666666666700000
          806.979166666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrDocumento2'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrPedido2: TQRLabel
        Left = 12
        Top = 208
        Width = 305
        Height = 15
        Enabled = False
        Size.Values = (
          39.687500000000000000
          31.750000000000000000
          550.333333333333300000
          806.979166666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrPedido2'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrNumero_Nota2: TQRLabel
        Left = 12
        Top = 220
        Width = 305
        Height = 15
        Size.Values = (
          39.687500000000000000
          31.750000000000000000
          582.083333333333200000
          806.979166666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNumero_Nota2'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrNomeFantasiaEmpresa: TQRLabel
        Left = 63
        Top = 233
        Width = 105
        Height = 13
        Size.Values = (
          34.395833333333330000
          166.687500000000000000
          616.479166666666700000
          277.812500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'qrNomeFantasiaEmpresa'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr71: TQRLabel
        Left = 12
        Top = 233
        Width = 49
        Height = 13
        Size.Values = (
          34.395833333333330000
          31.750000000000000000
          616.479166666666800000
          129.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Empresa:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr72: TQRLabel
        Left = 506
        Top = 275
        Width = 199
        Height = 17
        Size.Values = (
          44.979166666666670000
          1338.791666666667000000
          727.604166666666800000
          526.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Recibo do Pagador'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrEnderecoBeneficiario: TQRLabel
        Left = 12
        Top = 355
        Width = 555
        Height = 12
        Size.Values = (
          31.750000000000000000
          31.750000000000000000
          939.270833333333200000
          1468.437500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrEnderecoBeneficiario'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr73: TQRLabel
        Left = 364
        Top = 953
        Width = 53
        Height = 13
        Size.Values = (
          34.395833333333330000
          963.083333333333200000
          2521.479166666667000000
          140.229166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cnpj/Cpf:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrCPFCNPJBeneficiario: TQRLabel
        Left = 432
        Top = 763
        Width = 135
        Height = 13
        Size.Values = (
          34.395833333333330000
          1143.000000000000000000
          2018.770833333333000000
          357.187500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrCPFCNPJBeneficiario'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrCPFCNPJBeneficiario2: TQRLabel
        Left = 432
        Top = 342
        Width = 135
        Height = 13
        Size.Values = (
          34.395833333333330000
          1143.000000000000000000
          904.875000000000000000
          357.187500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrCPFCNPJBeneficiario2'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr74: TQRLabel
        Left = 410
        Top = 548
        Width = 53
        Height = 13
        Size.Values = (
          34.395833333333330000
          1084.791666666667000000
          1449.916666666667000000
          140.229166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cnpj/Cpf:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRImage2: TQRImage
        Left = 11
        Top = 105
        Width = 171
        Height = 21
        Size.Values = (
          55.562500000000000000
          29.104166666666670000
          277.812500000000000000
          452.437500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Picture.Data = {
          0A544A504547496D61676523240000FFD8FFE000104A46494600010101006000
          600000FFE101D24578696600004D4D002A000000080005510000040000000100
          00000051010003000000010001000051020001000001800000004A5103000100
          0000017F00000051040001000000017F00000000000000C6D300E3EFD1476D74
          BAC600E4EAEB00313B02817500A19400756CDDE4E58EB759B7D59173B0261646
          4F8A95017E8802FDFEFE70A926EEF2F2A6B8BB7AB92A799599F1F4F4BCCACD23
          505896C5593BB8ADCED568659A21ECF0AAAAB50056851DDBE8C96BA324C9EBE8
          65858AC8D3D5009A8DCFE5B30C3C45F5F9E9C1CD00002E38F6F9F9869FA4B7C6
          6B8CA3A73B636AB3C2C5CED92C97A200F8FAFAD1DBDCE3E983517A17C4D0D356
          797FFBFCF2305A6278B62800958877A21BB3BE0092A9AC77B328A2AC02009E90
          DAE16655BCB47DA24FE1E8724CA99297ACB001918384BA399EB2B5ACD9D6CED8
          DAABBCBF319B92FDFEF9D9E150F4F7F7D2D900FBFCFC76B6241AAA9ED7DFE1F1
          F6D9CBD5D77F999E7ACAC4BBE6E375B722B9C7CA9CCB62C2CED1728F94ABE0DC
          EAEFEF5E901F6B898FFAFCF8FAFBFBCED91C77B826008A7FB6C5C7BFCCCEB0C0
          C3D8DF1A34A49A87C4BF5E7F85A8BC87179C907FBB31CBD50DB1D1CA00646738
          803789C142D1D499C6E1A463A9A45DC2BB00343FFFFFFFFFDB00430002010102
          0101020202020202020203050303030303060404030507060707070607070809
          0B0908080A0807070A0D0A0A0B0C0C0C0C07090E0F0D0C0E0B0C0C0CFFDB0043
          01020202030303060303060C0807080C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C
          0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C
          0CFFC0001108003C00FA03012200021101031101FFC4001F0000010501010101
          010100000000000000000102030405060708090A0BFFC400B510000201030302
          0403050504040000017D01020300041105122131410613516107227114328191
          A1082342B1C11552D1F02433627282090A161718191A25262728292A34353637
          38393A434445464748494A535455565758595A636465666768696A7374757677
          78797A838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4
          B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8
          E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F01000301010101010101010100000000
          00000102030405060708090A0BFFC400B5110002010204040304070504040001
          0277000102031104052131061241510761711322328108144291A1B1C1092333
          52F0156272D10A162434E125F11718191A262728292A35363738393A43444546
          4748494A535455565758595A636465666768696A737475767778797A82838485
          868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BA
          C2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6
          F7F8F9FAFFDA000C03010002110311003F00FD61FDB6BF6E5D57F677F1C58787
          342D2ADAE2F25B45BE9EE6EC9F2F6B33AAA2A81C9CA124E7D07AD785DC7FC157
          FE204071FD93E1F3FF000071FD6BE84FF828DFECEBFF000B6FE140F116996A65
          F1078555A60117325CDA1E658F8EA571BD47FB2C072D5F9C571189A2E39C57F3
          978839FF0010E579C4E11C44A34A7670B592B6D6DB74F7FBFA9F259AE2B15431
          0D29B517B1EEFE20FF0082C47C4BD36190C5A2F86B2325498E438F4CF3C8FCBA
          57057BFF0005DCF8ABA76ACB6D3681E160AE4AAB88A4C671DF27F0E33CD792EB
          FA62CF136403C1EA335E53E3AF024FADDDC76D6B6F34F7571208E08E342EF23B
          602AA81C9627000EFC7AD70F0D78879A52C653AB8AAAEA453578BD9AEABEEDBB
          3B33C7C76271B89C2D4A14AB4A9CA49A525BC5F47F7EEBAABA3EB8FF0087E9FC
          51FF00A02785FF00EFD3FF008D1FF0FD3F8A3FF404F0BFFDFA7FF1AF98FF0069
          7FD997C59FB2478EED7C35E3082D9350BAD3E0BF8E6B77DF04CB22FCC15B0325
          1C3A3703942470413E77F6C5F55FCEBFB66860703569C6AC20ACD5FEF3F94730
          E3DE32C0E26A60F158BA919C1B4D5D68D7CBEE7D56A7DC1FF0FD3F8A3FF404F0
          BFFDFA7FF1AD7F00FF00C1763C710F8CB4E3E21F0D68B79A2B4EAB77159EE8EE
          0C64E0942720B0CE403D48C646735F047DB17D57F3AFB03FE08F1FB249FDA07E
          3D8F176AF69E6F857C09225C9DE331DE5FFDE822F709FEB5BD36A03C3D462B05
          81A54A552505648F478638CF8CF35CD286030B8C9B94E4B7B3492D5B6ADAA493
          6D755A1FB0B1C8258D5C02030C8CF0696B94F8DDF1CFC23FB377C30D57C67E3A
          D7F4EF0CF863458C49777F7B26C8E3C90AAA0756766215514166620004902BF3
          03E3A7FC1DBBF0ABC1DAF4967E00F86FE2FF001C5B42E50DFDFDDC5A341301FC
          51A9596420FF00B6887DABE2E8E1AA55F815CFED5C566187C35BDBCD2FCFEE5A
          9FAD5457E447C0BFF83B97E1CF8BFC636B61F103E17788FC11A4DCB847D56C35
          34D623B4C9C6F922114526C0393E5876C74526BF5B742D76CBC51A2596A7A6DD
          5BDF69DA8C09736B73038922B889D4323A30E195948208E0834AB61EA52D2A2B
          0F099861F129BA12BDBFAEA5AA2BF337C53FF07487C13F86DF1C7C4BE08F14F8
          2FE23E992F8635BBBD12E750B6B7B4BBB62F6F70F0B4A144CB26C3B0B70A5B1D
          89AFD0AF829F1A3C31FB44FC28D07C71E0CD5EDB5DF0BF89AD16F74FBE832166
          8DBD5580656520AB2300CACACA40208A2A50A94D27356B8F0F8EA15DB8D29A6D
          6E7514550F1478A74CF047876F758D6B51B1D2749D3616B8BBBCBC9D60B7B68D
          4659DDD8855503A92715F9C1FB4EFF00C1D2DFB3EFC16D52EF4CF065878A3E29
          6A16ACD1FDA34D856CB4B66524102E2621986470D1C4EA47209A54A854A8ED05
          71E271B430EAF5A4A27E97515F8C3A07FC1E0DA24FAE409AAFC08D5AD74D6702
          69AD3C531DC4E8BDCAC6D6C8AC47A175CFA8AFD4DFD8E7F6C1F047EDD5F01349
          F88DF0FEFAE2F341D55A485A3B98BC9BAB29E36DB24134793B2453E84820AB29
          2AC09BAD85AB495E71B1961334C2E264E342776BD57E67A8D15E47FB60FEDD1F
          0B7F610F87ABE25F89FE2BB2F0FDA5C129676D869EFB51718CA4102032484646
          481B541CB1039AFCD0F89FFF00077C78374BD7DE1F06FC19F12EB7A6A1C0BAD5
          F5B874D95FDC451C73803EAF9F6A28E16AD557846E18BCD30B867CB5A693EDBB
          FB91FB17457E4BFC03FF0083B6BE14F8D75D8ACBE217C3AF16F812099C20D42C
          6EA3D6ADA107ABC8A1629428FF0061243ED5FA81F067E36784BF688F871A6F8B
          FC0FE21D2BC51E1AD590BDAEA1A7CE25864C1C329C72AEA72195806520820104
          52AD86A94BE38D8BC266187C4FF0269FE7F73D4EA28A2BE72FDBC7FE0AADF05F
          FE09D3A5C03E21F8918EBF7D179D65E1ED2E2FB5EAB769CE1FCA0408D0904079
          59109040248C565084A4F962AECE8AB5A14A2E751A497567D1B457E30F8BFF00
          E0F04D12DB5B963D03E05EAD7DA6AB1F2E7D43C4F1DACF22F62634B79154FB6F
          3F5AF5DFD96FFE0EA9F81FF18B5DB4D27C7FE1EF12FC2DBBBB9044B7D70C9A9E
          951924005E68C2CA83279630ED51C96039AEA96031095DC4F329E7D809CB9554
          57F9AFC5AB1FA834551F0CF89B4DF1A7876C757D1EFECF54D2B53812E6D2F2D2
          659A0BA89C065911D490CA4104107041AF877FE0A31FF05FBF86DFF04E5F8E53
          FC3AD77C1BE38F12788E0B083502FA7ADAC765B2604A2F9924A1F3C73FBBE3DE
          B9E9D29D4972C15D9DF88C5D2A10F6956565DCFBC28AFC6DBEFF0083C0BC2B1D
          ECAB6BF037C4535B0FF56F2F89218E46FAA88180FC18D759F0B7FE0EE2F841E2
          1BE820F177C35F1FF86125215EE2CA5B6D521872792DF344E547FB284FB5743C
          BF1095F97F238239F601BB2A8BEE7FE47EB3515E73FB2EFED6BF0EFF006CFF00
          85D078C7E1A78A34FF00146852BF9324906E49AD250A18C3344E0491480303B5
          D41C10790413E8D5C928B4ECD1EAC2719C54A2EE98300C0820107820F7AFCC1F
          DB93F6786F801F19EE16CA068FC3DAF97BDD348002C593FBC8063A796C4607F7
          593A9CD7E9F57977ED7BF0022FDA23E0CDFE931A20D66CBFD334A95B8DB70A0E
          109ECAE3287D3703D857C27883C30B39CAE51A6BF7B4FDE879F78FFDBCBF1B1E
          766B82FAC51697C4B55FE5F33F282FED8107A007A57D27FF0004B7FD93E3F881
          F14DFE20EB1681F48F0A4BB74E574052E6F88FBDCF511290DFEFB2107E522BE7
          4F1031F0FC377F6E8E5B67B1DC268DD4ABC6CA705483C8604631EB5EFF00FB35
          7FC14C75FF00017C3AD23C37A57823C3D6BA6E990EC07CF99E599C925E4639FB
          CCE589E3A9C70315FCD9C299BE4F95E611C6E7751C29D3D52516EF25B2692D96
          FEA92EE787C3F974F1157DA5B48FE7D3FCCFA1FF00E0AC5FB1DB7ED55FB355DD
          DE8F6867F19F83164D4B4811A665BB40A0CF6830327CC450540EB2247DB35F85
          9FF0968FF24D7EDB68DFF051CF12EA085A5F0E68A9919F96497FC7FC2BE18F8E
          7FF04E1F09FC6AF8BFE22F16C1ABEA9E175F11DEBDFBE976104525B5AC921DD2
          08CBE582B3966C745DD818000AFDDB01F49FE06C2C5D2AD899DBA7EEAA3F5D91
          F1DE25F8458ACEF150CC72F82F68D72CD3695EDF0CB5EA968FCADD8F913C0D0E
          A5F11FC69A4787B46B57BDD5F5DBC8AC2CADD09DD34D2305451F527AF6EB5FD0
          9FEC77FB3569FF00B267ECF7E1FF000558B473DC58C3E76A374A31F6DBC7F9A6
          97D705B8507908AA3B57C79FF049EFF824F786FE047C4E6F8A771AAEABAEDC58
          C32D968D1DF43124704AE36CB72BB464B04DD18E71F3C9C64023F446BF57C271
          76178832FA39865FCDEC6A2E68B945C5B5B27CB2B3B3DD5F74EEB468DBC35F0D
          E5C3D2AB8AC6A5EDE5EEAB34F963BBD56976F7F24BCCFE787FE0EA3FDADF5AF8
          9BFB6D587C2687509D3C27F0DF4DB6BA92C558AC536A7751995E7719C3B2DBC9
          0A2E47C9BA5C7DF6CF807FC1213FE0909AB7FC156FC5DE2F821F1A69FE09D0FC
          1315AB5F5CBD99BEBB9E4B832F969141BE31B710C859D9C0076801B271F70FFC
          1D01FF0004BEF16F897E2345FB45782B4BB8D73473A5C361E2EB5B488C973A71
          B704457C5464B42622A8E40FDDF94AC7E56629F91DFB3F7ED29E3DFD95BE2141
          E2CF873E2CD63C23AFC0A505D69F36D1321EB1C887292A1EBB1D597201C640AF
          A8C2BE6C2A545D9FEBD4EECCD7B3CCE53C645CA2DFA5D74B7A1FAB3F16BFE0D0
          6F15E93E1D9EE3C0DF1A342D73548D731D9EB5A0CBA6C329F4F3A29672BEDFBB
          3F857EC5FEC6FF0004EEFF0066DFD937E1AFC3FBFBB8EFAFFC19E1AB0D1EEAE2
          324C72CB05BA46E573CECDCA76E7B62BF1C7F61BFF0083AFFC51A16B361A27C7
          DF0BD86B9A3C852193C49E1E83ECD7D6C380659AD4931CC3A93E518C81F7518E
          057EDB7C27F8AFE1CF8E9F0DF45F17F84757B3D7BC35E21B55BCD3EFED5B7457
          31374233C820E4152032904100822BC8C73C4A4A35F6EE7D664B1CB9B954C168
          DAD56B7FC4FE43FF006E8FF93DCF8CBFF63D6B9FFA709EBFA1DFF8369E779BFE
          0913E015776658B52D6150139DA3FB467381ED924FE35FCF17EDD1FF0027B9F1
          97FEC7AD73FF004E13D7EEE7FC113FE234FF0007BFE0DEABEF16DB67ED3E18D2
          FC55AAC38383BE09AEE55E7EAA2BD1CC55F0F14BBAFC8F9EE1D9A8E3EAC9F452
          FCD1F9DDFF00070AFF00C15735AFDB07F68CD67E15F863529ED7E157C3DD41EC
          1E085CAA78835285D926B99718DD1C6E19225395F90C9C971B7E7AFF00826FFF
          00C128BE29FF00C14D7C63796DE0BB5B4D2BC35A348B1EADE24D4F7A58593100
          F94BB4169A72A777969D01058A0604FCCDE6BCF9924769247F99998E4B13C927
          DC9AFEADFF00E0881F06B4DF825FF04AFF0083563A7430C6FAEE811788AF2445
          01AE2E2FBFD29998F72048A833D16351D00ABC4D5FAA5051A6BFAEE619761DE6
          B8D94F10F45AFF0092F43F3E0FFC19EAFF00D8CA47C7D5FED1C720F83FF719F6
          3F6CDD5F647FC1367F6451FF000433FF00827EFC4B9FE2378A74ED7ACF4AD535
          0F17DE5DE991C8B10B54B586348D11C03E738B71F28CFCCE1416C027EECAFCEF
          FF0083A13E20DD7833FE0953AB69D6E0ECF1678934BD2AE181236C6B235DF6F5
          6B551CFAD7931C4D5C44952A8EE9B5D8FAB9E5D84C04258AA30B4A29F57FAB3F
          00FF006D9FDB2FC65FB797ED11AEFC47F1ADDB3DFEAB295B4B14919ADB47B553
          FBAB5841E888BD4F0598B31F9989AFBEFF00E09A9FF06C9EBDFB5C7C0CD1BE23
          7C4AF1A5D780748F135B8BDD1F49B1D3D6E750B8B575CC5712BBB048838C3AA0
          57250A92549C0FCD6F813E048BE29FC71F05F85E76DB0F8935EB0D2A43D30B3D
          CC711FD1EBFB34D374E8347D3ADED2D618EDED6D6358618A350A91A28015401D
          00000C57A798E2654231A74B4FF807CD70FE5D0C6D4A95F15EF5BF16CFE5D7FE
          0ADFFF000458F197FC12DB51D27586D6A1F1A7C3CF10DC1B3B2D723B536B35B5
          D6D2E2DAE61DCC1199558AB2B15708DF748DB5B1FF00040CFF008293EAFF00B0
          BFED89A3786F52D4AE1BE197C4ABF8749D6AC5DF30595D4ACB15BEA0809C2323
          955908FBD133643148F6FEDFFF00C176FE18D97C53FF00824FFC64B6BB861964
          D1F475D6ED5DC64C32DA4D1CE194F6255197E8E477AFE54D98A29652432F20FA
          1AD30953EB541C6A7A7FC131CD70FF00D9B8E8D4C368B75FAAF43FAE9FF829A7
          EDBFA7FF00C13DBF634F16FC4AB98ADEF354B1896CB43B195B0B7FA8CC764119
          C104A03991F073E5C6E4722BF93DF8A3F13FC53FB43FC58D5FC57E28D4B50F12
          F8B7C577C6E6F2EA40649EEE791B01554741D1511400AA15540000AFD7BFF839
          CFE396A5E34FD88FF657B5B99251FF0009A5ABF896F00255649A3D3ED00CAFA8
          FB6C98CF4C9AF81FFE0889E00D37E25FFC1573E08E99AB4426B3875E3A904232
          0CB696D35D43F94B0C67F0ACF2EA6A9509556B5D7F037CFF00112C56361854ED
          1F757CE56D7F13EEEFD963FE0D2BD47C6FF07ACB58F8ADF122F3C23E2AD52D84
          E344D234E8AED7492C32A93CCEE049201F79630143640760371FCF6FF82997FC
          133BC6FF00F04C4F8E107853C553DBEB3A46B30BDDE83AF5AC4D15BEAF02B057
          F9092639632CA1E324EDDEA41656563FD6B57E5FFF00C1D7FF000CEC3C51FF00
          04F5F0DF8926555D4FC2BE30B516D26CC931DC413C72C79EC0911B7B98C0F4AE
          5C266356559466EE99E9E6DC3F85A78394E8AB4A2AF7BEFDEE7C95FF0006BEFF
          00C14B356F875F1B97F67AF146A325D784BC6226BAF0C1B894B7F646A288647B
          78F3D229D15CEDCE04A8368CCAC4F917FC1D0DFF002953D4FF00EC59D2FF00F4
          07AF8FFF0061EF1ADDFC39FDB3FE12EBB6324B15D695E30D26E14C5CB902F22D
          CA3D772E571DF38AFB03FE0E86FF0094A9EA7FF62CE97FFA03D7A0A8A8E2F997
          54FF0043C078B9D5CA5D39BBF2C95BD1A650FF008216FF00C122FC11FF00054D
          6F894DE32F1378AFC3C9E083A70B65D14DBA9B8FB48B9DC5CCB1BE36F90B8C0E
          EDED8CCFF82DAFFC11BECFFE095FADF83351F0EF8BAF7C53E14F1B1B98205D46
          18E3BFB09E058D995CC784911964043055208208E84F8E7EC05FF054BF8ABFF0
          4D73E28FF85692F87517C5E6D5B515D534EFB58736E2511EDF994AFF00AE7CE0
          F3C7A571DFB617EDD5F14FF6F3F1F5AF88FE2978AAEBC477BA7C4D05840228ED
          ACF4E8D9B732C30C6151371037360B36D5DCC768C68A9D7F6EE5CDEE76F97F99
          CF2AF81FA8AA7C8FDAF7F9FAF6F23EC4FF00835C3E30F88BC0DFF052F87C2DA6
          4B74FE1FF1C6837B16B36CA4987FD1A333C13B0E9B91C140C7A0B861FC55FD22
          57E547FC1B37FB277C07F017C3BD6FE20F823E2158FC4BF89F7D6B1586B6DF63
          7B193C2D0B9DFF00658EDE5025DB23A7339F965F2404C05607F55EBC2CCAA29D
          76D2D8FB9E1DC3CE960A2A6EF76DAB3BD9760A28A2B80F70FCDCFF0082B87ECC
          B73E17F88FA578B74883CBF0F78A2E48D4963E0457EA3233E82450CFDF2E8C4F
          515E3BF0E3C2A2C6DA20536955EBDFDFFCFF00857EB07C61F85BA6FC68F86FAA
          F86B55456B5D4E1D81F6EE682407292AFF00B4AC030FA57E775E7C35BEF873E2
          6BDD0F5288477DA64E60971F75C8E8C3B95230C0FA30AFE19FA46F0D55CA71F1
          CC682FDC622FE919EF25FF006F7C4BE69688F4F2A8538C5C20ADADFEFEA49A1D
          80810123057DB8CFA7E15D7FC36F01DDFC4BF1AD868D660892EE4C3BE322141C
          B39F6033F5381DEB0228C451AA8E40AFAE3F631F8427C1DE0E7F105EC25351D6
          D4792180DD15B755FA6F3F31F6095F8BF853C09538BB88A960649FB18FBF55F6
          826AEAFD1C9DA2BB5EFB267A389AEA9536FAF43D73C33E1CB3F08E8167A65844
          20B3B18845120E7000EA4F727A93DC9ABD4515FEA4D0A14E8D38D1A51518C524
          92D124B4492E892D8F9C6EFAB2ACBABD8B6AA34D7BAB537D2C26716AD22F9AD1
          67697D9D4AE4819C632715F167ED99FF0006FBFECE5FB60DD5F6AC3C3327C3EF
          155E9791F56F0BB2D989656FE396D8830392792422B31272D939AFCB1FF839CF
          F682F12C3FF0547B08347BEF127870F817C316561617B6ED358192595A4BA965
          B79976975C4D12165380D115EAA6BE52D27FE0AF1FB4FE89E1D9B4AB7F8EDF12
          BEC93285632EAF24D3A81FDD99F322FF00C05857BB87CBEAF2C6A539DAE7C866
          19F617DACF0F88A5CCA2EDD3FA4725FB7BFEC8B7FF00B08FED6FE33F853A8EB1
          67AFDC7852E2154D42DA331A5D4535BC571131424947F2E540CB938607058609
          FD94FF008346BE2DEB7E27FD993E28F83AFAE27B8D17C25AF5ADDE96242596DB
          ED9148668909E8BBE00FB4746958FF00157E217803E1CFC41FDAE7E2FF00F667
          87F4CF137C40F1B7886E0CB2AC424BEBDBA91D86E9659189206482D248C14756
          6039AFE9D3FE08B3FF0004E093FE09ADFB1DDB78675992D2EBC73E26BB3AD789
          67B76DF14770C8A896C8DFC490C6AAB9E8CE6461C362BAB33A8A341426EF2D3F
          E1CF2F86E84E78D75E945A82BFE3B2BF5FF807F351FB747FC9EE7C65FF00B1EB
          5CFF00D384F5FBE3FF000415F86D0FC65FF82105AF842E08583C550789348909
          2400B717375113F93D7E06FEDD3228FDB77E3282C011E3AD73BFFD4427AFE86F
          FE0DA220FF00C122FC0A41041D5358FF00D384F4B3276C3C5AEEBF22B87237C7
          D44FAA97E68FE68FC5FE0FD4FE1E78B755F0FEB5692586B3A0DE4DA75FDB3E37
          5BDC4323472C671C655D5871E95FD187FC1B69FF000504F0BFED05FB13F87FE1
          4DEEAD6B6BF10FE18C2FA7369B3CA167D434E572D6F730A9E5D111844FB7255A
          2CB001D09F0FFF0082FA7FC1067C41F1ABC7DA97C70F823A47F6AEBBAA012F8A
          BC316F81717F30017EDB68A78691940F32204162BBD433B303F8912C7E23F839
          E3DD8EBAE784FC51A14FD184B61A869F30FF00BE648DC7E06B592A78CA292767
          F9339A9BAF93E31B946F17A7AAF5EE7F6955F0F7FC1C53F01EF3E3AFFC129FC7
          C74EB56BBBFF00064B6BE298D01C158AD64FF497F7DB6CF3B63FD9AFE7F23FF8
          2AD7ED311694B64BF1E7E2B0810607FC549725F18C63796DC7F135FB71FF0006
          CB7C7BD57F6A2FF827CF8C349F1C6A57DE2DBFD23C577B617971AC5DBDFCF7F6
          D736F0CD899A52CCC099255C124103EA2BCDA9829E1AD59BBD9A3E8B0F9CD1CC
          B9B08A2D7327AB3F9D9F0678B2EFC07E31D235DB02AB7FA25EC1A85B13D04914
          8B227FE3CA2BFB1CFD9C3E3F7873F6A4F819E16F883E13BD8AFB41F1569F15FD
          B3AB02D16E505A2703EEC91B6E4653CAB2907A57F399FF000586FF00821F78DB
          F606F893ACF8A3C17A26ABE24F82F7B2BDD596A16A8D753787A323735BDE851B
          9110E42CC7E4650BB983E457CD3FB2D7FC1433E35FEC596D776FF0BFE237883C
          29617EE659EC6268EE2C6490800C9F67995E2121000DE1431000CE2BD1C5508E
          2E119D37B1F3F96E3A794D69D2C445D9FE9D57747EFEFF00C1CA3FB53E8FF013
          FE09A7E23F0ACD7710F12FC529A1D0B4BB412012BC4B2A4D7536DEBE5A448509
          E9BA68C1FBD5FCDCFC31F873AA7C61F893E1EF08E89035D6B3E29D4ADF49B188
          759279E558907FDF4C2BA4F8BFF1D3E257ED9BF1661D57C63E20F137C42F186A
          8CB696A2767BBB87CB7CB0410A8C282C788E25032781935FB35FF06F5FFC10EF
          C41F023C6367F1DBE32691268FE23B781D7C29E1BBA5C5CE9A64528F7B749FC1
          2942CB1C67940ECCC036D0A41470541F33BBFD475A55338C6A74E2D45597A2F3
          F3F2343FE0E9EFD97E4B3FD847E11F88347B3926D3FE16EA69A25C3221636D69
          736C9123B1ECBE6DBC0993FC52AF735F909FF04D8FDA52C7F640FDBC3E16FC46
          D5495D1BC37AE46DA9B88CC8D159CCAD6F712051CB32452BB003925457F589FB
          417C08F0DFED3BF053C4DF0FFC5F65FDA1E1BF165849A7DF440ED70AC38746FE
          191182BAB7556553DABF96CFF8293FFC127FE27FFC136BE255F5A78834ABDD63
          C0D34EC347F165ADBB358DEC25B08B330C8827C101A27239CED2CB86AC32DAF1
          9D37426F5D7E773B388F03568E2238DA4AEB4BF935B7C8FEAEF45D6ACFC49A35
          A6A3A75D5BDF69F7F0A5C5B5CC120922B889D4323A30E195948208E0835F907F
          F076BFED59A668FF00053C03F066C6FD24D7B5CD54789754B68DC16B7B282396
          287CC1D409269095F5FB337A57E50FECF5FF000555FDA1FF00656F87DFF08A78
          0BE2B789743F0DAA95874F3E4DE41680F24402747F241249C47B46493D79AF36
          B7B5F88BFB62FC6A90C51F8B7E2578FF00C4D38790A89B53D4AFDF017731F99C
          8030327E55503A014F0D963A553DA4E4AC89CC38916270CE8528352968FF00E0
          773D53FE091FF02EFBF68AFF0082937C1BF0E59452C890F896D758BC645DC22B
          5B27FB5CAC7B01B612B93DD80EA40AFA1BFE0E86FF0094A9EA7FF62CE97FFA03
          D7E9BFFC1037FE08D373FF0004F4F03EA1E3EF88705A49F16BC5D6C2D8DBC520
          9A3F0DD812AE6D430F95A7775569594951B111490ACCFF0098FF00F0742B03FF
          000554D5002091E19D2F3EDF23D6B4B131AB8BF776499CF89CBE785CA7F78AD2
          9493B76D1D8F5FFF00835B3F649F85FF00B5243F1B7FE163F803C27E383A31D1
          96C0EB5A6C5786C8482F7CCF2F783B776C4C91C9DA3D2AE7FC1C95FF0004A9F8
          37FB1CFC27F06FC49F863A447E0CBED6B5DFEC3BED12DEE1DECEF11ADE6985C4
          68EC4C6C8620A42108448380464FE7D7EC4FFF000528F8BFFF0004F47F119F85
          5E22B2D097C5620FED35B8D2EDAF84E60F33CA23CD462A57CD7FBB8CE79CE056
          17ED51FB6DFC5AFDBBFC7161A9FC4CF186AFE30D46D4791A75AB22456F6BBC80
          560B6855635663804AA6E6C2824E0634FAB56FACBABCD68F6F91CCB30C2FF672
          C33A77A9AEB65A6B7DF7D8EDBFE0927FB50F883F649FDBFBE1F789B40925617F
          7E345D4ACC48522D42D2E7F76F148075018A4833D1E243DABFAD4AFC1EFF0083
          7BFF00E0897E32BEF8DFA4FC70F8B7E1DBEF0CF86FC2FBE7F0F689AADB18AEF5
          9BB68CA2DC490B8DD1C11062CBB82B3C8108F954EEFDE1AF2B35AB09555C9D37
          3EA385F0D5A9619BABA26EE93FCFE61451457987D2857CF5FB6F7C1E5D42C20F
          1858C24DCD9A8B6BF0BFC5113F24847AA93B49F461D96BE85A8EF6CE1D46CE5B
          7B88A39E09D0C7246EA195D48C1041EA08AF8FE3CE0EC3713E495F27C569CEAF
          195AFCB35AC64BD1EEB4BC5B57D4D685574E6A68F883F674F84AFF00173E22DB
          DACD1B1D2ECB1717CDC81E583C267D58F1EB8C9ED5F71451AC31AA22AA220015
          40C00076ACAF08780F46F00D9CB6FA369D6BA74333EF9044B82E7D49EA7FA56B
          57CAF841E1853E0CCAA587A9255311565CD52693B3B6918ABD9F2C55DEBD5C99
          AE2B12EACAFD10514515FAD1CA60FC43F857E18F8B9A13E97E2BF0E685E26D32
          5055AD755B08AF21604608D92291CFD2BC5E5FF824A7ECC534C646F809F0A771
          24F1E1CB6039F6098AFA1A8AB8D492D13B194E8539BBCE29FAA397F855F03FC1
          7F02B405D27C13E12F0DF8474C4E96BA3E9B0D945F52B1AA827DCD75145152DB
          7AB348C545592B1E67AB7EC59F06F5ED56EAFEFBE137C33BDBEBE99EE2E6E27F
          0C58C92DC4AEC59E4763112CCCC49249C924935DA780FE1DF87FE16786E1D17C
          31A168FE1CD1EDD9DE2B1D2ECA3B3B68D9D8B3158E301416624920724935B145
          3726F46C98D2845DE2920AE13E32FECBBF0D7F689B41078F7C03E0EF18C6B8DB
          FDB1A44178D1E3A156914953F422BBBA292934EE8738464AD25747CF569FF049
          8FD98ECAE1258FE02FC290E8430DDE1CB5619FA1420FF8F35EDFE0CF01E85F0E
          3428B4BF0EE8BA4E83A641C4769A75A476B047C63848C051F80AD6A2AA55252D
          DDC8A7469C358452F44365892789A39155D1C15656190C0F5047A5785F8F7FE0
          97FF00B3A7C4FD7EE355D77E097C32D4352BB7F327B93E1FB78E599BBB332282
          C4F727AD7BB514A33947E1761D4A509E9349FAA3CDBE097EC6FF0009BF66D9A5
          97C01F0DBC11E0EB89CE659F49D1ADED6693FDE9114311C9E09AF49A28A52936
          EECA84231568AB20AADAC68D67E21D327B2D42D2DAFACAE5764D05C44B2C52AF
          A32B0208F635668A5729A3E7BD6FFE0939FB33788B5792FEEFE047C2D92EA590
          CAECBE1EB78C3B13924AAA80727B118AF50F835FB387C3EFD9DB487B0F00F81F
          C27E0CB3939922D174A82C44A7D5BCB51B8FB9C9AED28AB9549B566D98C30F4A
          2F9A3149FA20AF37F8C7FB1CFC25FDA1F558EFFC79F0CBC05E31D422411ADD6B
          3A15B5ECEA83A2F992216DA3D338AF48A2A63269DD3349C2325692BA3E744FF8
          2457ECC11DD89C7C07F8605C1CE0E85094FF00BE08DB8F6C62BD2FE147EC9BF0
          B7E04387F04FC38F02F84A45FF00969A46856D6727FDF51A03FAD7A05154EACD
          E8DB33861A941DE304BE4828A28A8363FFD9}
        Stretch = True
      end
      object QRImage1: TQRImage
        Left = 11
        Top = 267
        Width = 171
        Height = 21
        Size.Values = (
          55.562500000000000000
          29.104166666666670000
          706.437500000000000000
          452.437500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Picture.Data = {
          0A544A504547496D61676523240000FFD8FFE000104A46494600010101006000
          600000FFE101D24578696600004D4D002A000000080005510000040000000100
          00000051010003000000010001000051020001000001800000004A5103000100
          0000017F00000051040001000000017F00000000000000C6D300E3EFD1476D74
          BAC600E4EAEB00313B02817500A19400756CDDE4E58EB759B7D59173B0261646
          4F8A95017E8802FDFEFE70A926EEF2F2A6B8BB7AB92A799599F1F4F4BCCACD23
          505896C5593BB8ADCED568659A21ECF0AAAAB50056851DDBE8C96BA324C9EBE8
          65858AC8D3D5009A8DCFE5B30C3C45F5F9E9C1CD00002E38F6F9F9869FA4B7C6
          6B8CA3A73B636AB3C2C5CED92C97A200F8FAFAD1DBDCE3E983517A17C4D0D356
          797FFBFCF2305A6278B62800958877A21BB3BE0092A9AC77B328A2AC02009E90
          DAE16655BCB47DA24FE1E8724CA99297ACB001918384BA399EB2B5ACD9D6CED8
          DAABBCBF319B92FDFEF9D9E150F4F7F7D2D900FBFCFC76B6241AAA9ED7DFE1F1
          F6D9CBD5D77F999E7ACAC4BBE6E375B722B9C7CA9CCB62C2CED1728F94ABE0DC
          EAEFEF5E901F6B898FFAFCF8FAFBFBCED91C77B826008A7FB6C5C7BFCCCEB0C0
          C3D8DF1A34A49A87C4BF5E7F85A8BC87179C907FBB31CBD50DB1D1CA00646738
          803789C142D1D499C6E1A463A9A45DC2BB00343FFFFFFFFFDB00430002010102
          0101020202020202020203050303030303060404030507060707070607070809
          0B0908080A0807070A0D0A0A0B0C0C0C0C07090E0F0D0C0E0B0C0C0CFFDB0043
          01020202030303060303060C0807080C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C
          0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C
          0CFFC0001108003C00FA03012200021101031101FFC4001F0000010501010101
          010100000000000000000102030405060708090A0BFFC400B510000201030302
          0403050504040000017D01020300041105122131410613516107227114328191
          A1082342B1C11552D1F02433627282090A161718191A25262728292A34353637
          38393A434445464748494A535455565758595A636465666768696A7374757677
          78797A838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4
          B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8
          E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F01000301010101010101010100000000
          00000102030405060708090A0BFFC400B5110002010204040304070504040001
          0277000102031104052131061241510761711322328108144291A1B1C1092333
          52F0156272D10A162434E125F11718191A262728292A35363738393A43444546
          4748494A535455565758595A636465666768696A737475767778797A82838485
          868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BA
          C2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6
          F7F8F9FAFFDA000C03010002110311003F00FD61FDB6BF6E5D57F677F1C58787
          342D2ADAE2F25B45BE9EE6EC9F2F6B33AAA2A81C9CA124E7D07AD785DC7FC157
          FE204071FD93E1F3FF000071FD6BE84FF828DFECEBFF000B6FE140F116996A65
          F1078555A60117325CDA1E658F8EA571BD47FB2C072D5F9C571189A2E39C57F3
          978839FF0010E579C4E11C44A34A7670B592B6D6DB74F7FBFA9F259AE2B15431
          0D29B517B1EEFE20FF0082C47C4BD36190C5A2F86B2325498E438F4CF3C8FCBA
          57057BFF0005DCF8ABA76ACB6D3681E160AE4AAB88A4C671DF27F0E33CD792EB
          FA62CF136403C1EA335E53E3AF024FADDDC76D6B6F34F7571208E08E342EF23B
          602AA81C9627000EFC7AD70F0D78879A52C653AB8AAAEA453578BD9AEABEEDBB
          3B33C7C76271B89C2D4A14AB4A9CA49A525BC5F47F7EEBAABA3EB8FF0087E9FC
          51FF00A02785FF00EFD3FF008D1FF0FD3F8A3FF404F0BFFDFA7FF1AF98FF0069
          7FD997C59FB2478EED7C35E3082D9350BAD3E0BF8E6B77DF04CB22FCC15B0325
          1C3A3703942470413E77F6C5F55FCEBFB66860703569C6AC20ACD5FEF3F94730
          E3DE32C0E26A60F158BA919C1B4D5D68D7CBEE7D56A7DC1FF0FD3F8A3FF404F0
          BFFDFA7FF1AD7F00FF00C1763C710F8CB4E3E21F0D68B79A2B4EAB77159EE8EE
          0C64E0942720B0CE403D48C646735F047DB17D57F3AFB03FE08F1FB249FDA07E
          3D8F176AF69E6F857C09225C9DE331DE5FFDE822F709FEB5BD36A03C3D462B05
          81A54A552505648F478638CF8CF35CD286030B8C9B94E4B7B3492D5B6ADAA493
          6D755A1FB0B1C8258D5C02030C8CF0696B94F8DDF1CFC23FB377C30D57C67E3A
          D7F4EF0CF863458C49777F7B26C8E3C90AAA0756766215514166620004902BF3
          03E3A7FC1DBBF0ABC1DAF4967E00F86FE2FF001C5B42E50DFDFDDC5A341301FC
          51A9596420FF00B6887DABE2E8E1AA55F815CFED5C566187C35BDBCD2FCFEE5A
          9FAD5457E447C0BFF83B97E1CF8BFC636B61F103E17788FC11A4DCB847D56C35
          34D623B4C9C6F922114526C0393E5876C74526BF5B742D76CBC51A2596A7A6DD
          5BDF69DA8C09736B73038922B889D4323A30E195948208E0834AB61EA52D2A2B
          0F099861F129BA12BDBFAEA5AA2BF337C53FF07487C13F86DF1C7C4BE08F14F8
          2FE23E992F8635BBBD12E750B6B7B4BBB62F6F70F0B4A144CB26C3B0B70A5B1D
          89AFD0AF829F1A3C31FB44FC28D07C71E0CD5EDB5DF0BF89AD16F74FBE832166
          8DBD5580656520AB2300CACACA40208A2A50A94D27356B8F0F8EA15DB8D29A6D
          6E7514550F1478A74CF047876F758D6B51B1D2749D3616B8BBBCBC9D60B7B68D
          4659DDD8855503A92715F9C1FB4EFF00C1D2DFB3EFC16D52EF4CF065878A3E29
          6A16ACD1FDA34D856CB4B66524102E2621986470D1C4EA47209A54A854A8ED05
          71E271B430EAF5A4A27E97515F8C3A07FC1E0DA24FAE409AAFC08D5AD74D6702
          69AD3C531DC4E8BDCAC6D6C8AC47A175CFA8AFD4DFD8E7F6C1F047EDD5F01349
          F88DF0FEFAE2F341D55A485A3B98BC9BAB29E36DB24134793B2453E84820AB29
          2AC09BAD85AB495E71B1961334C2E264E342776BD57E67A8D15E47FB60FEDD1F
          0B7F610F87ABE25F89FE2BB2F0FDA5C129676D869EFB51718CA4102032484646
          481B541CB1039AFCD0F89FFF00077C78374BD7DE1F06FC19F12EB7A6A1C0BAD5
          F5B874D95FDC451C73803EAF9F6A28E16AD557846E18BCD30B867CB5A693EDBB
          FB91FB17457E4BFC03FF0083B6BE14F8D75D8ACBE217C3AF16F812099C20D42C
          6EA3D6ADA107ABC8A1629428FF0061243ED5FA81F067E36784BF688F871A6F8B
          FC0FE21D2BC51E1AD590BDAEA1A7CE25864C1C329C72AEA72195806520820104
          52AD86A94BE38D8BC266187C4FF0269FE7F73D4EA28A2BE72FDBC7FE0AADF05F
          FE09D3A5C03E21F8918EBF7D179D65E1ED2E2FB5EAB769CE1FCA0408D0904079
          59109040248C565084A4F962AECE8AB5A14A2E751A497567D1B457E30F8BFF00
          E0F04D12DB5B963D03E05EAD7DA6AB1F2E7D43C4F1DACF22F62634B79154FB6F
          3F5AF5DFD96FFE0EA9F81FF18B5DB4D27C7FE1EF12FC2DBBBB9044B7D70C9A9E
          951924005E68C2CA83279630ED51C96039AEA96031095DC4F329E7D809CB9554
          57F9AFC5AB1FA834551F0CF89B4DF1A7876C757D1EFECF54D2B53812E6D2F2D2
          659A0BA89C065911D490CA4104107041AF877FE0A31FF05FBF86DFF04E5F8E53
          FC3AD77C1BE38F12788E0B083502FA7ADAC765B2604A2F9924A1F3C73FBBE3DE
          B9E9D29D4972C15D9DF88C5D2A10F6956565DCFBC28AFC6DBEFF0083C0BC2B1D
          ECAB6BF037C4535B0FF56F2F89218E46FAA88180FC18D759F0B7FE0EE2F841E2
          1BE820F177C35F1FF86125215EE2CA5B6D521872792DF344E547FB284FB5743C
          BF1095F97F238239F601BB2A8BEE7FE47EB3515E73FB2EFED6BF0EFF006CFF00
          85D078C7E1A78A34FF00146852BF9324906E49AD250A18C3344E0491480303B5
          D41C10790413E8D5C928B4ECD1EAC2719C54A2EE98300C0820107820F7AFCC1F
          DB93F6786F801F19EE16CA068FC3DAF97BDD348002C593FBC8063A796C4607F7
          593A9CD7E9F57977ED7BF0022FDA23E0CDFE931A20D66CBFD334A95B8DB70A0E
          109ECAE3287D3703D857C27883C30B39CAE51A6BF7B4FDE879F78FFDBCBF1B1E
          766B82FAC51697C4B55FE5F33F282FED8107A007A57D27FF0004B7FD93E3F881
          F14DFE20EB1681F48F0A4BB74E574052E6F88FBDCF511290DFEFB2107E522BE7
          4F1031F0FC377F6E8E5B67B1DC268DD4ABC6CA705483C8604631EB5EFF00FB35
          7FC14C75FF00017C3AD23C37A57823C3D6BA6E990EC07CF99E599C925E4639FB
          CCE589E3A9C70315FCD9C299BE4F95E611C6E7751C29D3D52516EF25B2692D96
          FEA92EE787C3F974F1157DA5B48FE7D3FCCFA1FF00E0AC5FB1DB7ED55FB355DD
          DE8F6867F19F83164D4B4811A665BB40A0CF6830327CC450540EB2247DB35F85
          9FF0968FF24D7EDB68DFF051CF12EA085A5F0E68A9919F96497FC7FC2BE18F8E
          7FF04E1F09FC6AF8BFE22F16C1ABEA9E175F11DEBDFBE976104525B5AC921DD2
          08CBE582B3966C745DD818000AFDDB01F49FE06C2C5D2AD899DBA7EEAA3F5D91
          F1DE25F8458ACEF150CC72F82F68D72CD3695EDF0CB5EA968FCADD8F913C0D0E
          A5F11FC69A4787B46B57BDD5F5DBC8AC2CADD09DD34D2305451F527AF6EB5FD0
          9FEC77FB3569FF00B267ECF7E1FF000558B473DC58C3E76A374A31F6DBC7F9A6
          97D705B8507908AA3B57C79FF049EFF824F786FE047C4E6F8A771AAEABAEDC58
          C32D968D1DF43124704AE36CB72BB464B04DD18E71F3C9C64023F446BF57C271
          76178832FA39865FCDEC6A2E68B945C5B5B27CB2B3B3DD5F74EEB468DBC35F0D
          E5C3D2AB8AC6A5EDE5EEAB34F963BBD56976F7F24BCCFE787FE0EA3FDADF5AF8
          9BFB6D587C2687509D3C27F0DF4DB6BA92C558AC536A7751995E7719C3B2DBC9
          0A2E47C9BA5C7DF6CF807FC1213FE0909AB7FC156FC5DE2F821F1A69FE09D0FC
          1315AB5F5CBD99BEBB9E4B832F969141BE31B710C859D9C0076801B271F70FFC
          1D01FF0004BEF16F897E2345FB45782B4BB8D73473A5C361E2EB5B488C973A71
          B704457C5464B42622A8E40FDDF94AC7E56629F91DFB3F7ED29E3DFD95BE2141
          E2CF873E2CD63C23AFC0A505D69F36D1321EB1C887292A1EBB1D597201C640AF
          A8C2BE6C2A545D9FEBD4EECCD7B3CCE53C645CA2DFA5D74B7A1FAB3F16BFE0D0
          6F15E93E1D9EE3C0DF1A342D73548D731D9EB5A0CBA6C329F4F3A29672BEDFBB
          3F857EC5FEC6FF0004EEFF0066DFD937E1AFC3FBFBB8EFAFFC19E1AB0D1EEAE2
          324C72CB05BA46E573CECDCA76E7B62BF1C7F61BFF0083AFFC51A16B361A27C7
          DF0BD86B9A3C852193C49E1E83ECD7D6C380659AD4931CC3A93E518C81F7518E
          057EDB7C27F8AFE1CF8E9F0DF45F17F84757B3D7BC35E21B55BCD3EFED5B7457
          31374233C820E4152032904100822BC8C73C4A4A35F6EE7D664B1CB9B954C168
          DAD56B7FC4FE43FF006E8FF93DCF8CBFF63D6B9FFA709EBFA1DFF8369E779BFE
          0913E015776658B52D6150139DA3FB467381ED924FE35FCF17EDD1FF0027B9F1
          97FEC7AD73FF004E13D7EEE7FC113FE234FF0007BFE0DEABEF16DB67ED3E18D2
          FC55AAC38383BE09AEE55E7EAA2BD1CC55F0F14BBAFC8F9EE1D9A8E3EAC9F452
          FCD1F9DDFF00070AFF00C15735AFDB07F68CD67E15F863529ED7E157C3DD41EC
          1E085CAA78835285D926B99718DD1C6E19225395F90C9C971B7E7AFF00826FFF
          00C128BE29FF00C14D7C63796DE0BB5B4D2BC35A348B1EADE24D4F7A58593100
          F94BB4169A72A777969D01058A0604FCCDE6BCF9924769247F99998E4B13C927
          DC9AFEADFF00E0881F06B4DF825FF04AFF0083563A7430C6FAEE811788AF2445
          01AE2E2FBFD29998F72048A833D16351D00ABC4D5FAA5051A6BFAEE619761DE6
          B8D94F10F45AFF0092F43F3E0FFC19EAFF00D8CA47C7D5FED1C720F83FF719F6
          3F6CDD5F647FC1367F6451FF000433FF00827EFC4B9FE2378A74ED7ACF4AD535
          0F17DE5DE991C8B10B54B586348D11C03E738B71F28CFCCE1416C027EECAFCEF
          FF0083A13E20DD7833FE0953AB69D6E0ECF1678934BD2AE181236C6B235DF6F5
          6B551CFAD7931C4D5C44952A8EE9B5D8FAB9E5D84C04258AA30B4A29F57FAB3F
          00FF006D9FDB2FC65FB797ED11AEFC47F1ADDB3DFEAB295B4B14919ADB47B553
          FBAB5841E888BD4F0598B31F9989AFBEFF00E09A9FF06C9EBDFB5C7C0CD1BE23
          7C4AF1A5D780748F135B8BDD1F49B1D3D6E750B8B575CC5712BBB048838C3AA0
          57250A92549C0FCD6F813E048BE29FC71F05F85E76DB0F8935EB0D2A43D30B3D
          CC711FD1EBFB34D374E8347D3ADED2D618EDED6D6358618A350A91A28015401D
          00000C57A798E2654231A74B4FF807CD70FE5D0C6D4A95F15EF5BF16CFE5D7FE
          0ADFFF000458F197FC12DB51D27586D6A1F1A7C3CF10DC1B3B2D723B536B35B5
          D6D2E2DAE61DCC1199558AB2B15708DF748DB5B1FF00040CFF008293EAFF00B0
          BFED89A3786F52D4AE1BE197C4ABF8749D6AC5DF30595D4ACB15BEA0809C2323
          955908FBD133643148F6FEDFFF00C176FE18D97C53FF00824FFC64B6BB861964
          D1F475D6ED5DC64C32DA4D1CE194F6255197E8E477AFE54D98A29652432F20FA
          1AD30953EB541C6A7A7FC131CD70FF00D9B8E8D4C368B75FAAF43FAE9FF829A7
          EDBFA7FF00C13DBF634F16FC4AB98ADEF354B1896CB43B195B0B7FA8CC764119
          C104A03991F073E5C6E4722BF93DF8A3F13FC53FB43FC58D5FC57E28D4B50F12
          F8B7C577C6E6F2EA40649EEE791B01554741D1511400AA15540000AFD7BFF839
          CFE396A5E34FD88FF657B5B99251FF0009A5ABF896F00255649A3D3ED00CAFA8
          FB6C98CF4C9AF81FFE0889E00D37E25FFC1573E08E99AB4426B3875E3A904232
          0CB696D35D43F94B0C67F0ACF2EA6A9509556B5D7F037CFF00112C56361854ED
          1F757CE56D7F13EEEFD963FE0D2BD47C6FF07ACB58F8ADF122F3C23E2AD52D84
          E344D234E8AED7492C32A93CCEE049201F79630143640760371FCF6FF82997FC
          133BC6FF00F04C4F8E107853C553DBEB3A46B30BDDE83AF5AC4D15BEAF02B057
          F9092639632CA1E324EDDEA41656563FD6B57E5FFF00C1D7FF000CEC3C51FF00
          04F5F0DF8926555D4FC2BE30B516D26CC931DC413C72C79EC0911B7B98C0F4AE
          5C266356559466EE99E9E6DC3F85A78394E8AB4A2AF7BEFDEE7C95FF0006BEFF
          00C14B356F875F1B97F67AF146A325D784BC6226BAF0C1B894B7F646A288647B
          78F3D229D15CEDCE04A8368CCAC4F917FC1D0DFF002953D4FF00EC59D2FF00F4
          07AF8FFF0061EF1ADDFC39FDB3FE12EBB6324B15D695E30D26E14C5CB902F22D
          CA3D772E571DF38AFB03FE0E86FF0094A9EA7FF62CE97FFA03D7A0A8A8E2F997
          54FF0043C078B9D5CA5D39BBF2C95BD1A650FF008216FF00C122FC11FF00054D
          6F894DE32F1378AFC3C9E083A70B65D14DBA9B8FB48B9DC5CCB1BE36F90B8C0E
          EDED8CCFF82DAFFC11BECFFE095FADF83351F0EF8BAF7C53E14F1B1B98205D46
          18E3BFB09E058D995CC784911964043055208208E84F8E7EC05FF054BF8ABFF0
          4D73E28FF85692F87517C5E6D5B515D534EFB58736E2511EDF994AFF00AE7CE0
          F3C7A571DFB617EDD5F14FF6F3F1F5AF88FE2978AAEBC477BA7C4D05840228ED
          ACF4E8D9B732C30C6151371037360B36D5DCC768C68A9D7F6EE5CDEE76F97F99
          CF2AF81FA8AA7C8FDAF7F9FAF6F23EC4FF00835C3E30F88BC0DFF052F87C2DA6
          4B74FE1FF1C6837B16B36CA4987FD1A333C13B0E9B91C140C7A0B861FC55FD22
          57E547FC1B37FB277C07F017C3BD6FE20F823E2158FC4BF89F7D6B1586B6DF63
          7B193C2D0B9DFF00658EDE5025DB23A7339F965F2404C05607F55EBC2CCAA29D
          76D2D8FB9E1DC3CE960A2A6EF76DAB3BD9760A28A2B80F70FCDCFF0082B87ECC
          B73E17F88FA578B74883CBF0F78A2E48D4963E0457EA3233E82450CFDF2E8C4F
          515E3BF0E3C2A2C6DA20536955EBDFDFFCFF00857EB07C61F85BA6FC68F86FAA
          F86B55456B5D4E1D81F6EE682407292AFF00B4AC030FA57E775E7C35BEF873E2
          6BDD0F5288477DA64E60971F75C8E8C3B95230C0FA30AFE19FA46F0D55CA71F1
          CC682FDC622FE919EF25FF006F7C4BE69688F4F2A8538C5C20ADADFEFEA49A1D
          80810123057DB8CFA7E15D7FC36F01DDFC4BF1AD868D660892EE4C3BE322141C
          B39F6033F5381DEB0228C451AA8E40AFAE3F631F8427C1DE0E7F105EC25351D6
          D4792180DD15B755FA6F3F31F6095F8BF853C09538BB88A960649FB18FBF55F6
          826AEAFD1C9DA2BB5EFB267A389AEA9536FAF43D73C33E1CB3F08E8167A65844
          20B3B18845120E7000EA4F727A93DC9ABD4515FEA4D0A14E8D38D1A51518C524
          92D124B4492E892D8F9C6EFAB2ACBABD8B6AA34D7BAB537D2C26716AD22F9AD1
          67697D9D4AE4819C632715F167ED99FF0006FBFECE5FB60DD5F6AC3C3327C3EF
          155E9791F56F0BB2D989656FE396D8830392792422B31272D939AFCB1FF839CF
          F682F12C3FF0547B08347BEF127870F817C316561617B6ED358192595A4BA965
          B79976975C4D12165380D115EAA6BE52D27FE0AF1FB4FE89E1D9B4AB7F8EDF12
          BEC93285632EAF24D3A81FDD99F322FF00C05857BB87CBEAF2C6A539DAE7C866
          19F617DACF0F88A5CCA2EDD3FA4725FB7BFEC8B7FF00B08FED6FE33F853A8EB1
          67AFDC7852E2154D42DA331A5D4535BC571131424947F2E540CB938607058609
          FD94FF008346BE2DEB7E27FD993E28F83AFAE27B8D17C25AF5ADDE96242596DB
          ED9148668909E8BBE00FB4746958FF00157E217803E1CFC41FDAE7E2FF00F667
          87F4CF137C40F1B7886E0CB2AC424BEBDBA91D86E9659189206482D248C14756
          6039AFE9D3FE08B3FF0004E093FE09ADFB1DDB78675992D2EBC73E26BB3AD789
          67B76DF14770C8A896C8DFC490C6AAB9E8CE6461C362BAB33A8A341426EF2D3F
          E1CF2F86E84E78D75E945A82BFE3B2BF5FF807F351FB747FC9EE7C65FF00B1EB
          5CFF00D384F5FBE3FF000415F86D0FC65FF82105AF842E08583C550789348909
          2400B717375113F93D7E06FEDD3228FDB77E3282C011E3AD73BFFD4427AFE86F
          FE0DA220FF00C122FC0A41041D5358FF00D384F4B3276C3C5AEEBF22B87237C7
          D44FAA97E68FE68FC5FE0FD4FE1E78B755F0FEB5692586B3A0DE4DA75FDB3E37
          5BDC4323472C671C655D5871E95FD187FC1B69FF000504F0BFED05FB13F87FE1
          4DEEAD6B6BF10FE18C2FA7369B3CA167D434E572D6F730A9E5D111844FB7255A
          2CB001D09F0FFF0082FA7FC1067C41F1ABC7DA97C70F823A47F6AEBBAA012F8A
          BC316F81717F30017EDB68A78691940F32204162BBD433B303F8912C7E23F839
          E3DD8EBAE784FC51A14FD184B61A869F30FF00BE648DC7E06B592A78CA292767
          F9339A9BAF93E31B946F17A7AAF5EE7F6955F0F7FC1C53F01EF3E3AFFC129FC7
          C74EB56BBBFF00064B6BE298D01C158AD64FF497F7DB6CF3B63FD9AFE7F23FF8
          2AD7ED311694B64BF1E7E2B0810607FC549725F18C63796DC7F135FB71FF0006
          CB7C7BD57F6A2FF827CF8C349F1C6A57DE2DBFD23C577B617971AC5DBDFCF7F6
          D736F0CD899A52CCC099255C124103EA2BCDA9829E1AD59BBD9A3E8B0F9CD1CC
          B9B08A2D7327AB3F9D9F0678B2EFC07E31D235DB02AB7FA25EC1A85B13D04914
          8B227FE3CA2BFB1CFD9C3E3F7873F6A4F819E16F883E13BD8AFB41F1569F15FD
          B3AB02D16E505A2703EEC91B6E4653CAB2907A57F399FF000586FF00821F78DB
          F606F893ACF8A3C17A26ABE24F82F7B2BDD596A16A8D753787A323735BDE851B
          9110E42CC7E4650BB983E457CD3FB2D7FC1433E35FEC596D776FF0BFE237883C
          29617EE659EC6268EE2C6490800C9F67995E2121000DE1431000CE2BD1C5508E
          2E119D37B1F3F96E3A794D69D2C445D9FE9D57747EFEFF00C1CA3FB53E8FF013
          FE09A7E23F0ACD7710F12FC529A1D0B4BB412012BC4B2A4D7536DEBE5A448509
          E9BA68C1FBD5FCDCFC31F873AA7C61F893E1EF08E89035D6B3E29D4ADF49B188
          759279E558907FDF4C2BA4F8BFF1D3E257ED9BF1661D57C63E20F137C42F186A
          8CB696A2767BBB87CB7CB0410A8C282C788E25032781935FB35FF06F5FFC10EF
          C41F023C6367F1DBE32691268FE23B781D7C29E1BBA5C5CE9A64528F7B749FC1
          2942CB1C67940ECCC036D0A41470541F33BBFD475A55338C6A74E2D45597A2F3
          F3F2343FE0E9EFD97E4B3FD847E11F88347B3926D3FE16EA69A25C3221636D69
          736C9123B1ECBE6DBC0993FC52AF735F909FF04D8FDA52C7F640FDBC3E16FC46
          D5495D1BC37AE46DA9B88CC8D159CCAD6F712051CB32452BB003925457F589FB
          417C08F0DFED3BF053C4DF0FFC5F65FDA1E1BF165849A7DF440ED70AC38746FE
          191182BAB7556553DABF96CFF8293FFC127FE27FFC136BE255F5A78834ABDD63
          C0D34EC347F165ADBB358DEC25B08B330C8827C101A27239CED2CB86AC32DAF1
          9D37426F5D7E773B388F03568E2238DA4AEB4BF935B7C8FEAEF45D6ACFC49A35
          A6A3A75D5BDF69F7F0A5C5B5CC120922B889D4323A30E195948208E0835F907F
          F076BFED59A668FF00053C03F066C6FD24D7B5CD54789754B68DC16B7B282396
          287CC1D409269095F5FB337A57E50FECF5FF000555FDA1FF00656F87DFF08A78
          0BE2B789743F0DAA95874F3E4DE41680F24402747F241249C47B46493D79AF36
          B7B5F88BFB62FC6A90C51F8B7E2578FF00C4D38790A89B53D4AFDF017731F99C
          8030327E55503A014F0D963A553DA4E4AC89CC38916270CE8528352968FF00E0
          773D53FE091FF02EFBF68AFF0082937C1BF0E59452C890F896D758BC645DC22B
          5B27FB5CAC7B01B612B93DD80EA40AFA1BFE0E86FF0094A9EA7FF62CE97FFA03
          D7E9BFFC1037FE08D373FF0004F4F03EA1E3EF88705A49F16BC5D6C2D8DBC520
          9A3F0DD812AE6D430F95A7775569594951B111490ACCFF0098FF00F0742B03FF
          000554D5002091E19D2F3EDF23D6B4B131AB8BF776499CF89CBE785CA7F78AD2
          9493B76D1D8F5FFF00835B3F649F85FF00B5243F1B7FE163F803C27E383A31D1
          96C0EB5A6C5786C8482F7CCF2F783B776C4C91C9DA3D2AE7FC1C95FF0004A9F8
          37FB1CFC27F06FC49F863A447E0CBED6B5DFEC3BED12DEE1DECEF11ADE6985C4
          68EC4C6C8620A42108448380464FE7D7EC4FFF000528F8BFFF0004F47F119F85
          5E22B2D097C5620FED35B8D2EDAF84E60F33CA23CD462A57CD7FBB8CE79CE056
          17ED51FB6DFC5AFDBBFC7161A9FC4CF186AFE30D46D4791A75AB22456F6BBC80
          560B6855635663804AA6E6C2824E0634FAB56FACBABCD68F6F91CCB30C2FF672
          C33A77A9AEB65A6B7DF7D8EDBFE0927FB50F883F649FDBFBE1F789B40925617F
          7E345D4ACC48522D42D2E7F76F148075018A4833D1E243DABFAD4AFC1EFF0083
          7BFF00E0897E32BEF8DFA4FC70F8B7E1DBEF0CF86FC2FBE7F0F689AADB18AEF5
          9BB68CA2DC490B8DD1C11062CBB82B3C8108F954EEFDE1AF2B35AB09555C9D37
          3EA385F0D5A9619BABA26EE93FCFE61451457987D2857CF5FB6F7C1E5D42C20F
          1858C24DCD9A8B6BF0BFC5113F24847AA93B49F461D96BE85A8EF6CE1D46CE5B
          7B88A39E09D0C7246EA195D48C1041EA08AF8FE3CE0EC3713E495F27C569CEAF
          195AFCB35AC64BD1EEB4BC5B57D4D685574E6A68F883F674F84AFF00173E22DB
          DACD1B1D2ECB1717CDC81E583C267D58F1EB8C9ED5F71451AC31AA22AA220015
          40C00076ACAF08780F46F00D9CB6FA369D6BA74333EF9044B82E7D49EA7FA56B
          57CAF841E1853E0CCAA587A9255311565CD52693B3B6918ABD9F2C55DEBD5C99
          AE2B12EACAFD10514515FAD1CA60FC43F857E18F8B9A13E97E2BF0E685E26D32
          5055AD755B08AF21604608D92291CFD2BC5E5FF824A7ECC534C646F809F0A771
          24F1E1CB6039F6098AFA1A8AB8D492D13B194E8539BBCE29FAA397F855F03FC1
          7F02B405D27C13E12F0DF8474C4E96BA3E9B0D945F52B1AA827DCD75145152DB
          7AB348C545592B1E67AB7EC59F06F5ED56EAFEFBE137C33BDBEBE99EE2E6E27F
          0C58C92DC4AEC59E4763112CCCC49249C924935DA780FE1DF87FE16786E1D17C
          31A168FE1CD1EDD9DE2B1D2ECA3B3B68D9D8B3158E301416624920724935B145
          3726F46C98D2845DE2920AE13E32FECBBF0D7F689B41078F7C03E0EF18C6B8DB
          FDB1A44178D1E3A156914953F422BBBA292934EE8738464AD25747CF569FF049
          8FD98ECAE1258FE02FC290E8430DDE1CB5619FA1420FF8F35EDFE0CF01E85F0E
          3428B4BF0EE8BA4E83A641C4769A75A476B047C63848C051F80AD6A2AA55252D
          DDC8A7469C358452F44365892789A39155D1C15656190C0F5047A5785F8F7FE0
          97FF00B3A7C4FD7EE355D77E097C32D4352BB7F327B93E1FB78E599BBB332282
          C4F727AD7BB514A33947E1761D4A509E9349FAA3CDBE097EC6FF0009BF66D9A5
          97C01F0DBC11E0EB89CE659F49D1ADED6693FDE9114311C9E09AF49A28A52936
          EECA84231568AB20AADAC68D67E21D327B2D42D2DAFACAE5764D05C44B2C52AF
          A32B0208F635668A5729A3E7BD6FFE0939FB33788B5792FEEFE047C2D92EA590
          CAECBE1EB78C3B13924AAA80727B118AF50F835FB387C3EFD9DB487B0F00F81F
          C27E0CB3939922D174A82C44A7D5BCB51B8FB9C9AED28AB9549B566D98C30F4A
          2F9A3149FA20AF37F8C7FB1CFC25FDA1F558EFFC79F0CBC05E31D422411ADD6B
          3A15B5ECEA83A2F992216DA3D338AF48A2A63269DD3349C2325692BA3E744FF8
          2457ECC11DD89C7C07F8605C1CE0E85094FF00BE08DB8F6C62BD2FE147EC9BF0
          B7E04387F04FC38F02F84A45FF00969A46856D6727FDF51A03FAD7A05154EACD
          E8DB33861A941DE304BE4828A28A8363FFD9}
        Stretch = True
      end
      object QRImage3: TQRImage
        Left = 11
        Top = 689
        Width = 171
        Height = 21
        Size.Values = (
          55.562500000000000000
          29.104166666666670000
          1822.979166666667000000
          452.437500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Picture.Data = {
          0A544A504547496D61676523240000FFD8FFE000104A46494600010101006000
          600000FFE101D24578696600004D4D002A000000080005510000040000000100
          00000051010003000000010001000051020001000001800000004A5103000100
          0000017F00000051040001000000017F00000000000000C6D300E3EFD1476D74
          BAC600E4EAEB00313B02817500A19400756CDDE4E58EB759B7D59173B0261646
          4F8A95017E8802FDFEFE70A926EEF2F2A6B8BB7AB92A799599F1F4F4BCCACD23
          505896C5593BB8ADCED568659A21ECF0AAAAB50056851DDBE8C96BA324C9EBE8
          65858AC8D3D5009A8DCFE5B30C3C45F5F9E9C1CD00002E38F6F9F9869FA4B7C6
          6B8CA3A73B636AB3C2C5CED92C97A200F8FAFAD1DBDCE3E983517A17C4D0D356
          797FFBFCF2305A6278B62800958877A21BB3BE0092A9AC77B328A2AC02009E90
          DAE16655BCB47DA24FE1E8724CA99297ACB001918384BA399EB2B5ACD9D6CED8
          DAABBCBF319B92FDFEF9D9E150F4F7F7D2D900FBFCFC76B6241AAA9ED7DFE1F1
          F6D9CBD5D77F999E7ACAC4BBE6E375B722B9C7CA9CCB62C2CED1728F94ABE0DC
          EAEFEF5E901F6B898FFAFCF8FAFBFBCED91C77B826008A7FB6C5C7BFCCCEB0C0
          C3D8DF1A34A49A87C4BF5E7F85A8BC87179C907FBB31CBD50DB1D1CA00646738
          803789C142D1D499C6E1A463A9A45DC2BB00343FFFFFFFFFDB00430002010102
          0101020202020202020203050303030303060404030507060707070607070809
          0B0908080A0807070A0D0A0A0B0C0C0C0C07090E0F0D0C0E0B0C0C0CFFDB0043
          01020202030303060303060C0807080C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C
          0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C
          0CFFC0001108003C00FA03012200021101031101FFC4001F0000010501010101
          010100000000000000000102030405060708090A0BFFC400B510000201030302
          0403050504040000017D01020300041105122131410613516107227114328191
          A1082342B1C11552D1F02433627282090A161718191A25262728292A34353637
          38393A434445464748494A535455565758595A636465666768696A7374757677
          78797A838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4
          B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8
          E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F01000301010101010101010100000000
          00000102030405060708090A0BFFC400B5110002010204040304070504040001
          0277000102031104052131061241510761711322328108144291A1B1C1092333
          52F0156272D10A162434E125F11718191A262728292A35363738393A43444546
          4748494A535455565758595A636465666768696A737475767778797A82838485
          868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BA
          C2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6
          F7F8F9FAFFDA000C03010002110311003F00FD61FDB6BF6E5D57F677F1C58787
          342D2ADAE2F25B45BE9EE6EC9F2F6B33AAA2A81C9CA124E7D07AD785DC7FC157
          FE204071FD93E1F3FF000071FD6BE84FF828DFECEBFF000B6FE140F116996A65
          F1078555A60117325CDA1E658F8EA571BD47FB2C072D5F9C571189A2E39C57F3
          978839FF0010E579C4E11C44A34A7670B592B6D6DB74F7FBFA9F259AE2B15431
          0D29B517B1EEFE20FF0082C47C4BD36190C5A2F86B2325498E438F4CF3C8FCBA
          57057BFF0005DCF8ABA76ACB6D3681E160AE4AAB88A4C671DF27F0E33CD792EB
          FA62CF136403C1EA335E53E3AF024FADDDC76D6B6F34F7571208E08E342EF23B
          602AA81C9627000EFC7AD70F0D78879A52C653AB8AAAEA453578BD9AEABEEDBB
          3B33C7C76271B89C2D4A14AB4A9CA49A525BC5F47F7EEBAABA3EB8FF0087E9FC
          51FF00A02785FF00EFD3FF008D1FF0FD3F8A3FF404F0BFFDFA7FF1AF98FF0069
          7FD997C59FB2478EED7C35E3082D9350BAD3E0BF8E6B77DF04CB22FCC15B0325
          1C3A3703942470413E77F6C5F55FCEBFB66860703569C6AC20ACD5FEF3F94730
          E3DE32C0E26A60F158BA919C1B4D5D68D7CBEE7D56A7DC1FF0FD3F8A3FF404F0
          BFFDFA7FF1AD7F00FF00C1763C710F8CB4E3E21F0D68B79A2B4EAB77159EE8EE
          0C64E0942720B0CE403D48C646735F047DB17D57F3AFB03FE08F1FB249FDA07E
          3D8F176AF69E6F857C09225C9DE331DE5FFDE822F709FEB5BD36A03C3D462B05
          81A54A552505648F478638CF8CF35CD286030B8C9B94E4B7B3492D5B6ADAA493
          6D755A1FB0B1C8258D5C02030C8CF0696B94F8DDF1CFC23FB377C30D57C67E3A
          D7F4EF0CF863458C49777F7B26C8E3C90AAA0756766215514166620004902BF3
          03E3A7FC1DBBF0ABC1DAF4967E00F86FE2FF001C5B42E50DFDFDDC5A341301FC
          51A9596420FF00B6887DABE2E8E1AA55F815CFED5C566187C35BDBCD2FCFEE5A
          9FAD5457E447C0BFF83B97E1CF8BFC636B61F103E17788FC11A4DCB847D56C35
          34D623B4C9C6F922114526C0393E5876C74526BF5B742D76CBC51A2596A7A6DD
          5BDF69DA8C09736B73038922B889D4323A30E195948208E0834AB61EA52D2A2B
          0F099861F129BA12BDBFAEA5AA2BF337C53FF07487C13F86DF1C7C4BE08F14F8
          2FE23E992F8635BBBD12E750B6B7B4BBB62F6F70F0B4A144CB26C3B0B70A5B1D
          89AFD0AF829F1A3C31FB44FC28D07C71E0CD5EDB5DF0BF89AD16F74FBE832166
          8DBD5580656520AB2300CACACA40208A2A50A94D27356B8F0F8EA15DB8D29A6D
          6E7514550F1478A74CF047876F758D6B51B1D2749D3616B8BBBCBC9D60B7B68D
          4659DDD8855503A92715F9C1FB4EFF00C1D2DFB3EFC16D52EF4CF065878A3E29
          6A16ACD1FDA34D856CB4B66524102E2621986470D1C4EA47209A54A854A8ED05
          71E271B430EAF5A4A27E97515F8C3A07FC1E0DA24FAE409AAFC08D5AD74D6702
          69AD3C531DC4E8BDCAC6D6C8AC47A175CFA8AFD4DFD8E7F6C1F047EDD5F01349
          F88DF0FEFAE2F341D55A485A3B98BC9BAB29E36DB24134793B2453E84820AB29
          2AC09BAD85AB495E71B1961334C2E264E342776BD57E67A8D15E47FB60FEDD1F
          0B7F610F87ABE25F89FE2BB2F0FDA5C129676D869EFB51718CA4102032484646
          481B541CB1039AFCD0F89FFF00077C78374BD7DE1F06FC19F12EB7A6A1C0BAD5
          F5B874D95FDC451C73803EAF9F6A28E16AD557846E18BCD30B867CB5A693EDBB
          FB91FB17457E4BFC03FF0083B6BE14F8D75D8ACBE217C3AF16F812099C20D42C
          6EA3D6ADA107ABC8A1629428FF0061243ED5FA81F067E36784BF688F871A6F8B
          FC0FE21D2BC51E1AD590BDAEA1A7CE25864C1C329C72AEA72195806520820104
          52AD86A94BE38D8BC266187C4FF0269FE7F73D4EA28A2BE72FDBC7FE0AADF05F
          FE09D3A5C03E21F8918EBF7D179D65E1ED2E2FB5EAB769CE1FCA0408D0904079
          59109040248C565084A4F962AECE8AB5A14A2E751A497567D1B457E30F8BFF00
          E0F04D12DB5B963D03E05EAD7DA6AB1F2E7D43C4F1DACF22F62634B79154FB6F
          3F5AF5DFD96FFE0EA9F81FF18B5DB4D27C7FE1EF12FC2DBBBB9044B7D70C9A9E
          951924005E68C2CA83279630ED51C96039AEA96031095DC4F329E7D809CB9554
          57F9AFC5AB1FA834551F0CF89B4DF1A7876C757D1EFECF54D2B53812E6D2F2D2
          659A0BA89C065911D490CA4104107041AF877FE0A31FF05FBF86DFF04E5F8E53
          FC3AD77C1BE38F12788E0B083502FA7ADAC765B2604A2F9924A1F3C73FBBE3DE
          B9E9D29D4972C15D9DF88C5D2A10F6956565DCFBC28AFC6DBEFF0083C0BC2B1D
          ECAB6BF037C4535B0FF56F2F89218E46FAA88180FC18D759F0B7FE0EE2F841E2
          1BE820F177C35F1FF86125215EE2CA5B6D521872792DF344E547FB284FB5743C
          BF1095F97F238239F601BB2A8BEE7FE47EB3515E73FB2EFED6BF0EFF006CFF00
          85D078C7E1A78A34FF00146852BF9324906E49AD250A18C3344E0491480303B5
          D41C10790413E8D5C928B4ECD1EAC2719C54A2EE98300C0820107820F7AFCC1F
          DB93F6786F801F19EE16CA068FC3DAF97BDD348002C593FBC8063A796C4607F7
          593A9CD7E9F57977ED7BF0022FDA23E0CDFE931A20D66CBFD334A95B8DB70A0E
          109ECAE3287D3703D857C27883C30B39CAE51A6BF7B4FDE879F78FFDBCBF1B1E
          766B82FAC51697C4B55FE5F33F282FED8107A007A57D27FF0004B7FD93E3F881
          F14DFE20EB1681F48F0A4BB74E574052E6F88FBDCF511290DFEFB2107E522BE7
          4F1031F0FC377F6E8E5B67B1DC268DD4ABC6CA705483C8604631EB5EFF00FB35
          7FC14C75FF00017C3AD23C37A57823C3D6BA6E990EC07CF99E599C925E4639FB
          CCE589E3A9C70315FCD9C299BE4F95E611C6E7751C29D3D52516EF25B2692D96
          FEA92EE787C3F974F1157DA5B48FE7D3FCCFA1FF00E0AC5FB1DB7ED55FB355DD
          DE8F6867F19F83164D4B4811A665BB40A0CF6830327CC450540EB2247DB35F85
          9FF0968FF24D7EDB68DFF051CF12EA085A5F0E68A9919F96497FC7FC2BE18F8E
          7FF04E1F09FC6AF8BFE22F16C1ABEA9E175F11DEBDFBE976104525B5AC921DD2
          08CBE582B3966C745DD818000AFDDB01F49FE06C2C5D2AD899DBA7EEAA3F5D91
          F1DE25F8458ACEF150CC72F82F68D72CD3695EDF0CB5EA968FCADD8F913C0D0E
          A5F11FC69A4787B46B57BDD5F5DBC8AC2CADD09DD34D2305451F527AF6EB5FD0
          9FEC77FB3569FF00B267ECF7E1FF000558B473DC58C3E76A374A31F6DBC7F9A6
          97D705B8507908AA3B57C79FF049EFF824F786FE047C4E6F8A771AAEABAEDC58
          C32D968D1DF43124704AE36CB72BB464B04DD18E71F3C9C64023F446BF57C271
          76178832FA39865FCDEC6A2E68B945C5B5B27CB2B3B3DD5F74EEB468DBC35F0D
          E5C3D2AB8AC6A5EDE5EEAB34F963BBD56976F7F24BCCFE787FE0EA3FDADF5AF8
          9BFB6D587C2687509D3C27F0DF4DB6BA92C558AC536A7751995E7719C3B2DBC9
          0A2E47C9BA5C7DF6CF807FC1213FE0909AB7FC156FC5DE2F821F1A69FE09D0FC
          1315AB5F5CBD99BEBB9E4B832F969141BE31B710C859D9C0076801B271F70FFC
          1D01FF0004BEF16F897E2345FB45782B4BB8D73473A5C361E2EB5B488C973A71
          B704457C5464B42622A8E40FDDF94AC7E56629F91DFB3F7ED29E3DFD95BE2141
          E2CF873E2CD63C23AFC0A505D69F36D1321EB1C887292A1EBB1D597201C640AF
          A8C2BE6C2A545D9FEBD4EECCD7B3CCE53C645CA2DFA5D74B7A1FAB3F16BFE0D0
          6F15E93E1D9EE3C0DF1A342D73548D731D9EB5A0CBA6C329F4F3A29672BEDFBB
          3F857EC5FEC6FF0004EEFF0066DFD937E1AFC3FBFBB8EFAFFC19E1AB0D1EEAE2
          324C72CB05BA46E573CECDCA76E7B62BF1C7F61BFF0083AFFC51A16B361A27C7
          DF0BD86B9A3C852193C49E1E83ECD7D6C380659AD4931CC3A93E518C81F7518E
          057EDB7C27F8AFE1CF8E9F0DF45F17F84757B3D7BC35E21B55BCD3EFED5B7457
          31374233C820E4152032904100822BC8C73C4A4A35F6EE7D664B1CB9B954C168
          DAD56B7FC4FE43FF006E8FF93DCF8CBFF63D6B9FFA709EBFA1DFF8369E779BFE
          0913E015776658B52D6150139DA3FB467381ED924FE35FCF17EDD1FF0027B9F1
          97FEC7AD73FF004E13D7EEE7FC113FE234FF0007BFE0DEABEF16DB67ED3E18D2
          FC55AAC38383BE09AEE55E7EAA2BD1CC55F0F14BBAFC8F9EE1D9A8E3EAC9F452
          FCD1F9DDFF00070AFF00C15735AFDB07F68CD67E15F863529ED7E157C3DD41EC
          1E085CAA78835285D926B99718DD1C6E19225395F90C9C971B7E7AFF00826FFF
          00C128BE29FF00C14D7C63796DE0BB5B4D2BC35A348B1EADE24D4F7A58593100
          F94BB4169A72A777969D01058A0604FCCDE6BCF9924769247F99998E4B13C927
          DC9AFEADFF00E0881F06B4DF825FF04AFF0083563A7430C6FAEE811788AF2445
          01AE2E2FBFD29998F72048A833D16351D00ABC4D5FAA5051A6BFAEE619761DE6
          B8D94F10F45AFF0092F43F3E0FFC19EAFF00D8CA47C7D5FED1C720F83FF719F6
          3F6CDD5F647FC1367F6451FF000433FF00827EFC4B9FE2378A74ED7ACF4AD535
          0F17DE5DE991C8B10B54B586348D11C03E738B71F28CFCCE1416C027EECAFCEF
          FF0083A13E20DD7833FE0953AB69D6E0ECF1678934BD2AE181236C6B235DF6F5
          6B551CFAD7931C4D5C44952A8EE9B5D8FAB9E5D84C04258AA30B4A29F57FAB3F
          00FF006D9FDB2FC65FB797ED11AEFC47F1ADDB3DFEAB295B4B14919ADB47B553
          FBAB5841E888BD4F0598B31F9989AFBEFF00E09A9FF06C9EBDFB5C7C0CD1BE23
          7C4AF1A5D780748F135B8BDD1F49B1D3D6E750B8B575CC5712BBB048838C3AA0
          57250A92549C0FCD6F813E048BE29FC71F05F85E76DB0F8935EB0D2A43D30B3D
          CC711FD1EBFB34D374E8347D3ADED2D618EDED6D6358618A350A91A28015401D
          00000C57A798E2654231A74B4FF807CD70FE5D0C6D4A95F15EF5BF16CFE5D7FE
          0ADFFF000458F197FC12DB51D27586D6A1F1A7C3CF10DC1B3B2D723B536B35B5
          D6D2E2DAE61DCC1199558AB2B15708DF748DB5B1FF00040CFF008293EAFF00B0
          BFED89A3786F52D4AE1BE197C4ABF8749D6AC5DF30595D4ACB15BEA0809C2323
          955908FBD133643148F6FEDFFF00C176FE18D97C53FF00824FFC64B6BB861964
          D1F475D6ED5DC64C32DA4D1CE194F6255197E8E477AFE54D98A29652432F20FA
          1AD30953EB541C6A7A7FC131CD70FF00D9B8E8D4C368B75FAAF43FAE9FF829A7
          EDBFA7FF00C13DBF634F16FC4AB98ADEF354B1896CB43B195B0B7FA8CC764119
          C104A03991F073E5C6E4722BF93DF8A3F13FC53FB43FC58D5FC57E28D4B50F12
          F8B7C577C6E6F2EA40649EEE791B01554741D1511400AA15540000AFD7BFF839
          CFE396A5E34FD88FF657B5B99251FF0009A5ABF896F00255649A3D3ED00CAFA8
          FB6C98CF4C9AF81FFE0889E00D37E25FFC1573E08E99AB4426B3875E3A904232
          0CB696D35D43F94B0C67F0ACF2EA6A9509556B5D7F037CFF00112C56361854ED
          1F757CE56D7F13EEEFD963FE0D2BD47C6FF07ACB58F8ADF122F3C23E2AD52D84
          E344D234E8AED7492C32A93CCEE049201F79630143640760371FCF6FF82997FC
          133BC6FF00F04C4F8E107853C553DBEB3A46B30BDDE83AF5AC4D15BEAF02B057
          F9092639632CA1E324EDDEA41656563FD6B57E5FFF00C1D7FF000CEC3C51FF00
          04F5F0DF8926555D4FC2BE30B516D26CC931DC413C72C79EC0911B7B98C0F4AE
          5C266356559466EE99E9E6DC3F85A78394E8AB4A2AF7BEFDEE7C95FF0006BEFF
          00C14B356F875F1B97F67AF146A325D784BC6226BAF0C1B894B7F646A288647B
          78F3D229D15CEDCE04A8368CCAC4F917FC1D0DFF002953D4FF00EC59D2FF00F4
          07AF8FFF0061EF1ADDFC39FDB3FE12EBB6324B15D695E30D26E14C5CB902F22D
          CA3D772E571DF38AFB03FE0E86FF0094A9EA7FF62CE97FFA03D7A0A8A8E2F997
          54FF0043C078B9D5CA5D39BBF2C95BD1A650FF008216FF00C122FC11FF00054D
          6F894DE32F1378AFC3C9E083A70B65D14DBA9B8FB48B9DC5CCB1BE36F90B8C0E
          EDED8CCFF82DAFFC11BECFFE095FADF83351F0EF8BAF7C53E14F1B1B98205D46
          18E3BFB09E058D995CC784911964043055208208E84F8E7EC05FF054BF8ABFF0
          4D73E28FF85692F87517C5E6D5B515D534EFB58736E2511EDF994AFF00AE7CE0
          F3C7A571DFB617EDD5F14FF6F3F1F5AF88FE2978AAEBC477BA7C4D05840228ED
          ACF4E8D9B732C30C6151371037360B36D5DCC768C68A9D7F6EE5CDEE76F97F99
          CF2AF81FA8AA7C8FDAF7F9FAF6F23EC4FF00835C3E30F88BC0DFF052F87C2DA6
          4B74FE1FF1C6837B16B36CA4987FD1A333C13B0E9B91C140C7A0B861FC55FD22
          57E547FC1B37FB277C07F017C3BD6FE20F823E2158FC4BF89F7D6B1586B6DF63
          7B193C2D0B9DFF00658EDE5025DB23A7339F965F2404C05607F55EBC2CCAA29D
          76D2D8FB9E1DC3CE960A2A6EF76DAB3BD9760A28A2B80F70FCDCFF0082B87ECC
          B73E17F88FA578B74883CBF0F78A2E48D4963E0457EA3233E82450CFDF2E8C4F
          515E3BF0E3C2A2C6DA20536955EBDFDFFCFF00857EB07C61F85BA6FC68F86FAA
          F86B55456B5D4E1D81F6EE682407292AFF00B4AC030FA57E775E7C35BEF873E2
          6BDD0F5288477DA64E60971F75C8E8C3B95230C0FA30AFE19FA46F0D55CA71F1
          CC682FDC622FE919EF25FF006F7C4BE69688F4F2A8538C5C20ADADFEFEA49A1D
          80810123057DB8CFA7E15D7FC36F01DDFC4BF1AD868D660892EE4C3BE322141C
          B39F6033F5381DEB0228C451AA8E40AFAE3F631F8427C1DE0E7F105EC25351D6
          D4792180DD15B755FA6F3F31F6095F8BF853C09538BB88A960649FB18FBF55F6
          826AEAFD1C9DA2BB5EFB267A389AEA9536FAF43D73C33E1CB3F08E8167A65844
          20B3B18845120E7000EA4F727A93DC9ABD4515FEA4D0A14E8D38D1A51518C524
          92D124B4492E892D8F9C6EFAB2ACBABD8B6AA34D7BAB537D2C26716AD22F9AD1
          67697D9D4AE4819C632715F167ED99FF0006FBFECE5FB60DD5F6AC3C3327C3EF
          155E9791F56F0BB2D989656FE396D8830392792422B31272D939AFCB1FF839CF
          F682F12C3FF0547B08347BEF127870F817C316561617B6ED358192595A4BA965
          B79976975C4D12165380D115EAA6BE52D27FE0AF1FB4FE89E1D9B4AB7F8EDF12
          BEC93285632EAF24D3A81FDD99F322FF00C05857BB87CBEAF2C6A539DAE7C866
          19F617DACF0F88A5CCA2EDD3FA4725FB7BFEC8B7FF00B08FED6FE33F853A8EB1
          67AFDC7852E2154D42DA331A5D4535BC571131424947F2E540CB938607058609
          FD94FF008346BE2DEB7E27FD993E28F83AFAE27B8D17C25AF5ADDE96242596DB
          ED9148668909E8BBE00FB4746958FF00157E217803E1CFC41FDAE7E2FF00F667
          87F4CF137C40F1B7886E0CB2AC424BEBDBA91D86E9659189206482D248C14756
          6039AFE9D3FE08B3FF0004E093FE09ADFB1DDB78675992D2EBC73E26BB3AD789
          67B76DF14770C8A896C8DFC490C6AAB9E8CE6461C362BAB33A8A341426EF2D3F
          E1CF2F86E84E78D75E945A82BFE3B2BF5FF807F351FB747FC9EE7C65FF00B1EB
          5CFF00D384F5FBE3FF000415F86D0FC65FF82105AF842E08583C550789348909
          2400B717375113F93D7E06FEDD3228FDB77E3282C011E3AD73BFFD4427AFE86F
          FE0DA220FF00C122FC0A41041D5358FF00D384F4B3276C3C5AEEBF22B87237C7
          D44FAA97E68FE68FC5FE0FD4FE1E78B755F0FEB5692586B3A0DE4DA75FDB3E37
          5BDC4323472C671C655D5871E95FD187FC1B69FF000504F0BFED05FB13F87FE1
          4DEEAD6B6BF10FE18C2FA7369B3CA167D434E572D6F730A9E5D111844FB7255A
          2CB001D09F0FFF0082FA7FC1067C41F1ABC7DA97C70F823A47F6AEBBAA012F8A
          BC316F81717F30017EDB68A78691940F32204162BBD433B303F8912C7E23F839
          E3DD8EBAE784FC51A14FD184B61A869F30FF00BE648DC7E06B592A78CA292767
          F9339A9BAF93E31B946F17A7AAF5EE7F6955F0F7FC1C53F01EF3E3AFFC129FC7
          C74EB56BBBFF00064B6BE298D01C158AD64FF497F7DB6CF3B63FD9AFE7F23FF8
          2AD7ED311694B64BF1E7E2B0810607FC549725F18C63796DC7F135FB71FF0006
          CB7C7BD57F6A2FF827CF8C349F1C6A57DE2DBFD23C577B617971AC5DBDFCF7F6
          D736F0CD899A52CCC099255C124103EA2BCDA9829E1AD59BBD9A3E8B0F9CD1CC
          B9B08A2D7327AB3F9D9F0678B2EFC07E31D235DB02AB7FA25EC1A85B13D04914
          8B227FE3CA2BFB1CFD9C3E3F7873F6A4F819E16F883E13BD8AFB41F1569F15FD
          B3AB02D16E505A2703EEC91B6E4653CAB2907A57F399FF000586FF00821F78DB
          F606F893ACF8A3C17A26ABE24F82F7B2BDD596A16A8D753787A323735BDE851B
          9110E42CC7E4650BB983E457CD3FB2D7FC1433E35FEC596D776FF0BFE237883C
          29617EE659EC6268EE2C6490800C9F67995E2121000DE1431000CE2BD1C5508E
          2E119D37B1F3F96E3A794D69D2C445D9FE9D57747EFEFF00C1CA3FB53E8FF013
          FE09A7E23F0ACD7710F12FC529A1D0B4BB412012BC4B2A4D7536DEBE5A448509
          E9BA68C1FBD5FCDCFC31F873AA7C61F893E1EF08E89035D6B3E29D4ADF49B188
          759279E558907FDF4C2BA4F8BFF1D3E257ED9BF1661D57C63E20F137C42F186A
          8CB696A2767BBB87CB7CB0410A8C282C788E25032781935FB35FF06F5FFC10EF
          C41F023C6367F1DBE32691268FE23B781D7C29E1BBA5C5CE9A64528F7B749FC1
          2942CB1C67940ECCC036D0A41470541F33BBFD475A55338C6A74E2D45597A2F3
          F3F2343FE0E9EFD97E4B3FD847E11F88347B3926D3FE16EA69A25C3221636D69
          736C9123B1ECBE6DBC0993FC52AF735F909FF04D8FDA52C7F640FDBC3E16FC46
          D5495D1BC37AE46DA9B88CC8D159CCAD6F712051CB32452BB003925457F589FB
          417C08F0DFED3BF053C4DF0FFC5F65FDA1E1BF165849A7DF440ED70AC38746FE
          191182BAB7556553DABF96CFF8293FFC127FE27FFC136BE255F5A78834ABDD63
          C0D34EC347F165ADBB358DEC25B08B330C8827C101A27239CED2CB86AC32DAF1
          9D37426F5D7E773B388F03568E2238DA4AEB4BF935B7C8FEAEF45D6ACFC49A35
          A6A3A75D5BDF69F7F0A5C5B5CC120922B889D4323A30E195948208E0835F907F
          F076BFED59A668FF00053C03F066C6FD24D7B5CD54789754B68DC16B7B282396
          287CC1D409269095F5FB337A57E50FECF5FF000555FDA1FF00656F87DFF08A78
          0BE2B789743F0DAA95874F3E4DE41680F24402747F241249C47B46493D79AF36
          B7B5F88BFB62FC6A90C51F8B7E2578FF00C4D38790A89B53D4AFDF017731F99C
          8030327E55503A014F0D963A553DA4E4AC89CC38916270CE8528352968FF00E0
          773D53FE091FF02EFBF68AFF0082937C1BF0E59452C890F896D758BC645DC22B
          5B27FB5CAC7B01B612B93DD80EA40AFA1BFE0E86FF0094A9EA7FF62CE97FFA03
          D7E9BFFC1037FE08D373FF0004F4F03EA1E3EF88705A49F16BC5D6C2D8DBC520
          9A3F0DD812AE6D430F95A7775569594951B111490ACCFF0098FF00F0742B03FF
          000554D5002091E19D2F3EDF23D6B4B131AB8BF776499CF89CBE785CA7F78AD2
          9493B76D1D8F5FFF00835B3F649F85FF00B5243F1B7FE163F803C27E383A31D1
          96C0EB5A6C5786C8482F7CCF2F783B776C4C91C9DA3D2AE7FC1C95FF0004A9F8
          37FB1CFC27F06FC49F863A447E0CBED6B5DFEC3BED12DEE1DECEF11ADE6985C4
          68EC4C6C8620A42108448380464FE7D7EC4FFF000528F8BFFF0004F47F119F85
          5E22B2D097C5620FED35B8D2EDAF84E60F33CA23CD462A57CD7FBB8CE79CE056
          17ED51FB6DFC5AFDBBFC7161A9FC4CF186AFE30D46D4791A75AB22456F6BBC80
          560B6855635663804AA6E6C2824E0634FAB56FACBABCD68F6F91CCB30C2FF672
          C33A77A9AEB65A6B7DF7D8EDBFE0927FB50F883F649FDBFBE1F789B40925617F
          7E345D4ACC48522D42D2E7F76F148075018A4833D1E243DABFAD4AFC1EFF0083
          7BFF00E0897E32BEF8DFA4FC70F8B7E1DBEF0CF86FC2FBE7F0F689AADB18AEF5
          9BB68CA2DC490B8DD1C11062CBB82B3C8108F954EEFDE1AF2B35AB09555C9D37
          3EA385F0D5A9619BABA26EE93FCFE61451457987D2857CF5FB6F7C1E5D42C20F
          1858C24DCD9A8B6BF0BFC5113F24847AA93B49F461D96BE85A8EF6CE1D46CE5B
          7B88A39E09D0C7246EA195D48C1041EA08AF8FE3CE0EC3713E495F27C569CEAF
          195AFCB35AC64BD1EEB4BC5B57D4D685574E6A68F883F674F84AFF00173E22DB
          DACD1B1D2ECB1717CDC81E583C267D58F1EB8C9ED5F71451AC31AA22AA220015
          40C00076ACAF08780F46F00D9CB6FA369D6BA74333EF9044B82E7D49EA7FA56B
          57CAF841E1853E0CCAA587A9255311565CD52693B3B6918ABD9F2C55DEBD5C99
          AE2B12EACAFD10514515FAD1CA60FC43F857E18F8B9A13E97E2BF0E685E26D32
          5055AD755B08AF21604608D92291CFD2BC5E5FF824A7ECC534C646F809F0A771
          24F1E1CB6039F6098AFA1A8AB8D492D13B194E8539BBCE29FAA397F855F03FC1
          7F02B405D27C13E12F0DF8474C4E96BA3E9B0D945F52B1AA827DCD75145152DB
          7AB348C545592B1E67AB7EC59F06F5ED56EAFEFBE137C33BDBEBE99EE2E6E27F
          0C58C92DC4AEC59E4763112CCCC49249C924935DA780FE1DF87FE16786E1D17C
          31A168FE1CD1EDD9DE2B1D2ECA3B3B68D9D8B3158E301416624920724935B145
          3726F46C98D2845DE2920AE13E32FECBBF0D7F689B41078F7C03E0EF18C6B8DB
          FDB1A44178D1E3A156914953F422BBBA292934EE8738464AD25747CF569FF049
          8FD98ECAE1258FE02FC290E8430DDE1CB5619FA1420FF8F35EDFE0CF01E85F0E
          3428B4BF0EE8BA4E83A641C4769A75A476B047C63848C051F80AD6A2AA55252D
          DDC8A7469C358452F44365892789A39155D1C15656190C0F5047A5785F8F7FE0
          97FF00B3A7C4FD7EE355D77E097C32D4352BB7F327B93E1FB78E599BBB332282
          C4F727AD7BB514A33947E1761D4A509E9349FAA3CDBE097EC6FF0009BF66D9A5
          97C01F0DBC11E0EB89CE659F49D1ADED6693FDE9114311C9E09AF49A28A52936
          EECA84231568AB20AADAC68D67E21D327B2D42D2DAFACAE5764D05C44B2C52AF
          A32B0208F635668A5729A3E7BD6FFE0939FB33788B5792FEEFE047C2D92EA590
          CAECBE1EB78C3B13924AAA80727B118AF50F835FB387C3EFD9DB487B0F00F81F
          C27E0CB3939922D174A82C44A7D5BCB51B8FB9C9AED28AB9549B566D98C30F4A
          2F9A3149FA20AF37F8C7FB1CFC25FDA1F558EFFC79F0CBC05E31D422411ADD6B
          3A15B5ECEA83A2F992216DA3D338AF48A2A63269DD3349C2325692BA3E744FF8
          2457ECC11DD89C7C07F8605C1CE0E85094FF00BE08DB8F6C62BD2FE147EC9BF0
          B7E04387F04FC38F02F84A45FF00969A46856D6727FDF51A03FAD7A05154EACD
          E8DB33861A941DE304BE4828A28A8363FFD9}
        Stretch = True
      end
      object qrObservacao1: TQRLabel
        Left = 12
        Top = 521
        Width = 551
        Height = 18
        Enabled = False
        Size.Values = (
          47.625000000000000000
          31.750000000000000000
          1378.479166666667000000
          1457.854166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrObservacaoBanco'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object qrObservacao: TQRLabel
        Left = 11
        Top = 928
        Width = 551
        Height = 21
        Enabled = False
        Size.Values = (
          55.562500000000000000
          29.104166666666670000
          2455.333333333333000000
          1457.854166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrObservacao'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
    end
    object qrbnd2: TQRBand
      Left = 38
      Top = 1103
      Width = 718
      Height = 1051
      Frame.Style = psClear
      AlignToBottom = False
      Color = clWindow
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        2780.770833333333000000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbTitle
      object qr75: TQRLabel
        Left = 16
        Top = 177
        Width = 681
        Height = 17
        Size.Values = (
          44.979166666666670000
          42.333333333333340000
          468.312500000000100000
          1801.812500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 
          'Garanta a felicidade da sua empresa e de seu cliente utilizando ' +
          'o ADM - Sistema de Gest'#227'o Completo para sua Empresa.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRImage8: TQRImage
        Left = 8
        Top = 392
        Width = 145
        Height = 25
        Size.Values = (
          66.145833333333340000
          21.166666666666670000
          1037.166666666667000000
          383.645833333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Picture.Data = {
          0A544A504547496D6167655E180000FFD8FFE000104A46494600010100000100
          010000FFDB004300080606070605080707070909080A0C140D0C0B0B0C191213
          0F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F2739
          3D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232
          3232323232323232323232323232323232323232323232323232323232323232
          32323232323232323232323232FFC2001108041702CE03012200021101031101
          FFC4001A000100030101010000000000000000000000020506040307FFC40014
          010100000000000000000000000000000000FFDA000C03010002100310000001
          DFBE6FA5346CCFA9A167E45F315606954B406E5931AC646E8B463B4677327AC0
          0000000000000000000000000000000000000000000000000000033B2D00ACF0
          BA14DD5DE29BCEF4524AE452CAE0574FB853CED4717AF4000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000FFFC4002010000203000104030000000000000000000304010205001113
          14601523D0FFDA0008010100010502F5A4EB4A24DE85D431DD6E0D2F1CD6F25C
          29ACCC3989DD72BC2E8106EC364F198E877C4DDC7995D264C270C626424C5DBA
          48EA179D3F8C9E448C25F49AE4D6004CAA92252A4DAD9F11CBA1D4B0A0EA8DF3
          E26233220A5CEEE5E50FBFE3A074167083C32743882AD17295193CDD2EE588B5
          6EC8072107E6A47FFFC400141101000000000000000000000000000000B0FFDA
          0008010301013F0139AFFFC400141101000000000000000000000000000000B0
          FFDA0008010201013F0139AFFFC4003110000201030204030606030000000000
          00010203000411122113314151226181142432607191052342A1B1C1D0D1F0FF
          DA0008010100063F02F96ACE7E018C897C5719C0C67FE1BD3926378D08D48AA7
          5007CF90ABB58521D36C0312F9E58CD4315AA46247884C78BC80F4A782110092
          155E297CE093DAA59C2E9D50BEDF7AFC2E3B768C078793677217AD2C6A61910C
          A1084C92B9DB73CB9F4ABD930B981982FA0A0D25B7B466CB5685EF9AB62B7313
          B367C5206258F60399AB230C51EBB8D790C79115722E151648E40A74F23BA9FE
          E9A6C28889FCBEF8EE69E5BF859B32E62B8CF8547407B54B37555DBEBD2A5B44
          78D8695901439DF186FDFE4A16ED712B5B83911EC3EF53271E458666D6C831F1
          55D36A6F795D2DE5B62A2686678A58E3E187D8E57CC53491DCCB1BB8024231E2
          C7F06BD917223D1A2AD744CE8D6E30AD8076C6299B8F2683371B46DF154D8B89
          5239BE345C73A8E58E778CAC7C2D80DC7AD40B04AF1707233B1C83CF9D5B6967
          F77D5A73E75346C5B12B063E98FF005533A16C4A7515E80D32CB732340C73C2C
          0EF9E744BCF2B299049A7A6DD3E951CFA995D015F0F507BD2C6646908FD4DCCF
          F8D49FFFC4002310010100020202020300030000000000000111002131415161
          71816091A1D0E1F0FFDA0008010100013F21FC6BA74A3DAD18D887006F9F3700
          687B0791781E4EF1D7510A56A03BD3E319C3DA56EA1C96E742E029D93D7B71AF
          0DCF4807FA63B6D434B61A751D7BFAC1804713A1E130E15A54B18C52EF2FCA2D
          026DA2B4F1AAEF03518DC2487B217D59BC8808109D9BE3B9F5EF28EDDFD513E8
          62891C9E434FC8F5D60DB02CB57738B3E5706F02372F87F5335A5C0EC367C0D7
          BFC2B867CC0EEC4152E6A3E0DDD56A59A358BEA070E82D3F7911C44207484C7E
          065B512F1F61FAC6D40A8E766DF9DDCD493B5082A2730CD1441306D5ACA9EB05
          E0BC30C4B66A9CF9C997BAA62DE8E112C00150351CA9FEB37202DE37DEEBDE1D
          946E69075FAE6E0639BD94F9CBEC63E89A59FF005C43B0D647A676DFD1DED14D
          268F53CA3B3DE1CAF8BDFF001A927FFFDA000C03010002000300000010E34F30
          D3CD38D3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF1CF
          1CB0CF1C30CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CFFFC400141101000000000000000000
          000000000000B0FFDA0008010301013F1039AFFFC40014110100000000000000
          0000000000000000B0FFDA0008010201013F1039AFFFC4001F10010101000301
          000301010000000000000111210031415160618171D0FFDA0008010100013F10
          FC69EC1EA80C92CC0C8BD02F11204DFD23EEAC72E8A70C501ABD580D4150108D
          A22108B383252ED843DB8A255529D82A18FA684CAB82B1514954ECA47291875C
          70D34F1DFAD2083E97A853A0B4E87F49658056A8EB8BC83F652AB64CF9CA7E2D
          95140345AF943BC6DAF46840DC1AF934609ECDCA42046942A1530DE16B96BA1A
          1683916E6CB03BAD655F5059419020DBDF0B0148AC200420A91170AD5F59385E
          0878B1FDF380E7BADA7DB4350A33ACC3F09321EF6CD8572B29F4908184166A88
          1A3C4727719C2A104180573DC4EDDE2A44726488938B49AFB00BE996AB9DBD57
          261335CEB4531963A65293257AE20325914081583215C723CA5D053E80201404
          F5DE1E9A2B0820920307FA5338AAAF8EFC02159B3CE25DF6F4510002210AF481
          38E00477F4EA7493F778B15D1D00147496B7B78451FA6707D8F66AF44813821A
          0E01438FBE40110870EECB62649E84047AA7478615CA083283180E1A9CD578D8
          6DABFD876C0AAEFF00CD49FFD9}
      end
      object QRShape135: TQRShape
        Left = 8
        Top = 416
        Width = 561
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          1100.666666666667000000
          1484.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape136: TQRShape
        Left = 568
        Top = 416
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1100.666666666667000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qr76: TQRLabel
        Left = 8
        Top = 417
        Width = 177
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1103.312500000000000000
          468.312500000000100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Local de Pagamento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr77: TQRLabel
        Left = 8
        Top = 429
        Width = 249
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1135.062500000000000000
          658.812500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Pag'#225'vel em qualquer banco at'#233' o vencimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr78: TQRLabel
        Left = 570
        Top = 417
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1103.312500000000000000
          354.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vencimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr79: TQRLabel
        Left = 568
        Top = 429
        Width = 137
        Height = 14
        Size.Values = (
          37.041666666666670000
          1502.833333333333000000
          1135.062500000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'QRLabel33'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRShape137: TQRShape
        Left = 568
        Top = 443
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1172.104166666667000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape138: TQRShape
        Left = 8
        Top = 443
        Width = 561
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          1172.104166666667000000
          1484.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qr80: TQRLabel
        Left = 8
        Top = 444
        Width = 193
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1174.750000000000000000
          510.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Cedente'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr81: TQRLabel
        Left = 570
        Top = 444
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1174.750000000000000000
          354.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ag'#234'ncia/C'#243'd.Cedente'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRShape139: TQRShape
        Left = 8
        Top = 470
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          1243.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape140: TQRShape
        Left = 120
        Top = 470
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          317.500000000000000000
          1243.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape141: TQRShape
        Left = 344
        Top = 470
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          910.166666666666600000
          1243.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape142: TQRShape
        Left = 232
        Top = 470
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          613.833333333333400000
          1243.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape143: TQRShape
        Left = 456
        Top = 470
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          1206.500000000000000000
          1243.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape144: TQRShape
        Left = 568
        Top = 470
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1243.541666666667000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape145: TQRShape
        Left = 8
        Top = 497
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          1314.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape146: TQRShape
        Left = 120
        Top = 497
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          317.500000000000000000
          1314.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape147: TQRShape
        Left = 232
        Top = 497
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          613.833333333333400000
          1314.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape148: TQRShape
        Left = 344
        Top = 497
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          910.166666666666600000
          1314.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape149: TQRShape
        Left = 456
        Top = 497
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          1206.500000000000000000
          1314.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape150: TQRShape
        Left = 568
        Top = 497
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1314.979166666667000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape151: TQRShape
        Left = 8
        Top = 524
        Width = 561
        Height = 136
        Size.Values = (
          359.833333333333400000
          21.166666666666670000
          1386.416666666667000000
          1484.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape152: TQRShape
        Left = 568
        Top = 524
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1386.416666666667000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape153: TQRShape
        Left = 568
        Top = 551
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1457.854166666667000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape154: TQRShape
        Left = 568
        Top = 578
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1529.291666666667000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape155: TQRShape
        Left = 568
        Top = 605
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1600.729166666667000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape156: TQRShape
        Left = 568
        Top = 632
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1672.166666666667000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape157: TQRShape
        Left = 8
        Top = 659
        Width = 697
        Height = 74
        Size.Values = (
          195.791666666666700000
          21.166666666666670000
          1743.604166666667000000
          1844.145833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape158: TQRShape
        Left = 8
        Top = 376
        Width = 697
        Height = 25
        Frame.Style = psDot
        Size.Values = (
          66.145833333333340000
          21.166666666666670000
          994.833333333333400000
          1844.145833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Pen.Style = psDot
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape159: TQRShape
        Left = 8
        Top = 792
        Width = 697
        Height = 25
        Frame.Style = psDot
        Size.Values = (
          66.145833333333340000
          21.166666666666670000
          2095.500000000000000000
          1844.145833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Pen.Style = psDot
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object qr82: TQRLabel
        Left = 600
        Top = 734
        Width = 105
        Height = 14
        Size.Values = (
          37.041666666666670000
          1587.500000000000000000
          1942.041666666667000000
          277.812500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Autentica'#231#227'o Mec'#226'nica'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr83: TQRLabel
        Left = 600
        Top = 750
        Width = 105
        Height = 14
        Size.Values = (
          37.041666666666670000
          1587.500000000000000000
          1984.375000000000000000
          277.812500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ficha de Compensa'#231#227'o'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr84: TQRLabel
        Left = 8
        Top = 471
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1246.187500000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Data Docto'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr85: TQRLabel
        Left = 120
        Top = 471
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          317.500000000000000000
          1246.187500000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Nr. Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr86: TQRLabel
        Left = 232
        Top = 471
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          613.833333333333400000
          1246.187500000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Esp'#233'cie'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr87: TQRLabel
        Left = 456
        Top = 471
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          1206.500000000000000000
          1246.187500000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Data Proc.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr88: TQRLabel
        Left = 344
        Top = 471
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          910.166666666666600000
          1246.187500000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Aceite'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr89: TQRLabel
        Left = 8
        Top = 498
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1317.625000000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Uso do Banco'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr90: TQRLabel
        Left = 120
        Top = 498
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          317.500000000000000000
          1317.625000000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Carteira'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr91: TQRLabel
        Left = 232
        Top = 498
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          613.833333333333400000
          1317.625000000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Moeda'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr92: TQRLabel
        Left = 344
        Top = 498
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          910.166666666666600000
          1317.625000000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Quantidade'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr93: TQRLabel
        Left = 456
        Top = 498
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          1206.500000000000000000
          1317.625000000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Valor'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr94: TQRLabel
        Left = 8
        Top = 532
        Width = 193
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1407.583333333333000000
          510.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Instru'#231#245'es'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr95: TQRLabel
        Left = 8
        Top = 660
        Width = 193
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1746.250000000000000000
          510.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Sacado'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr96: TQRLabel
        Left = 8
        Top = 716
        Width = 193
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1894.416666666667000000
          510.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Sacador/Avalista:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr97: TQRLabel
        Left = 570
        Top = 471
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1246.187500000000000000
          354.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nosso N'#250'mero'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr98: TQRLabel
        Left = 570
        Top = 498
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1317.625000000000000000
          354.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor do Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr99: TQRLabel
        Left = 570
        Top = 525
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1389.062500000000000000
          354.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(-) Desconto/Abatimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr100: TQRLabel
        Left = 570
        Top = 552
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1460.500000000000000000
          354.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(-) Outras Dedu'#231#245'es'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr101: TQRLabel
        Left = 570
        Top = 579
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1531.937500000000000000
          354.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(+) Mora/Multa'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr102: TQRLabel
        Left = 570
        Top = 606
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1603.375000000000000000
          354.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(+) Outros'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr103: TQRLabel
        Left = 570
        Top = 633
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1674.812500000000000000
          354.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(=) Valor Cobrado'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRShape160: TQRShape
        Left = 8
        Top = 208
        Width = 697
        Height = 25
        Frame.Style = psDot
        Size.Values = (
          66.145833333333340000
          21.166666666666670000
          550.333333333333400000
          1844.145833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Pen.Style = psDot
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape161: TQRShape
        Left = 8
        Top = 265
        Width = 561
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          701.145833333333400000
          1484.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRImage9: TQRImage
        Left = 8
        Top = 240
        Width = 145
        Height = 25
        Size.Values = (
          66.145833333333340000
          21.166666666666670000
          635.000000000000000000
          383.645833333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Picture.Data = {
          0A544A504547496D6167655E180000FFD8FFE000104A46494600010100000100
          010000FFDB004300080606070605080707070909080A0C140D0C0B0B0C191213
          0F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F2739
          3D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232
          3232323232323232323232323232323232323232323232323232323232323232
          32323232323232323232323232FFC2001108041702CE03012200021101031101
          FFC4001A000100030101010000000000000000000000020506040307FFC40014
          010100000000000000000000000000000000FFDA000C03010002100310000001
          DFBE6FA5346CCFA9A167E45F315606954B406E5931AC646E8B463B4677327AC0
          0000000000000000000000000000000000000000000000000000033B2D00ACF0
          BA14DD5DE29BCEF4524AE452CAE0574FB853CED4717AF4000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000FFFC4002010000203000104030000000000000000000304010205001113
          14601523D0FFDA0008010100010502F5A4EB4A24DE85D431DD6E0D2F1CD6F25C
          29ACCC3989DD72BC2E8106EC364F198E877C4DDC7995D264C270C626424C5DBA
          48EA179D3F8C9E448C25F49AE4D6004CAA92252A4DAD9F11CBA1D4B0A0EA8DF3
          E26233220A5CEEE5E50FBFE3A074167083C32743882AD17295193CDD2EE588B5
          6EC8072107E6A47FFFC400141101000000000000000000000000000000B0FFDA
          0008010301013F0139AFFFC400141101000000000000000000000000000000B0
          FFDA0008010201013F0139AFFFC4003110000201030204030606030000000000
          00010203000411122113314151226181142432607191052342A1B1C1D0D1F0FF
          DA0008010100063F02F96ACE7E018C897C5719C0C67FE1BD3926378D08D48AA7
          5007CF90ABB58521D36C0312F9E58CD4315AA46247884C78BC80F4A782110092
          155E297CE093DAA59C2E9D50BEDF7AFC2E3B768C078793677217AD2C6A61910C
          A1084C92B9DB73CB9F4ABD930B981982FA0A0D25B7B466CB5685EF9AB62B7313
          B367C5206258F60399AB230C51EBB8D790C79115722E151648E40A74F23BA9FE
          E9A6C28889FCBEF8EE69E5BF859B32E62B8CF8547407B54B37555DBEBD2A5B44
          78D8695901439DF186FDFE4A16ED712B5B83911EC3EF53271E458666D6C831F1
          55D36A6F795D2DE5B62A2686678A58E3E187D8E57CC53491DCCB1BB8024231E2
          C7F06BD917223D1A2AD744CE8D6E30AD8076C6299B8F2683371B46DF154D8B89
          5239BE345C73A8E58E778CAC7C2D80DC7AD40B04AF1707233B1C83CF9D5B6967
          F77D5A73E75346C5B12B063E98FF005533A16C4A7515E80D32CB732340C73C2C
          0EF9E744BCF2B299049A7A6DD3E951CFA995D015F0F507BD2C6646908FD4DCCF
          F8D49FFFC4002310010100020202020300030000000000000111002131415161
          71816091A1D0E1F0FFDA0008010100013F21FC6BA74A3DAD18D887006F9F3700
          687B0791781E4EF1D7510A56A03BD3E319C3DA56EA1C96E742E029D93D7B71AF
          0DCF4807FA63B6D434B61A751D7BFAC1804713A1E130E15A54B18C52EF2FCA2D
          026DA2B4F1AAEF03518DC2487B217D59BC8808109D9BE3B9F5EF28EDDFD513E8
          62891C9E434FC8F5D60DB02CB57738B3E5706F02372F87F5335A5C0EC367C0D7
          BFC2B867CC0EEC4152E6A3E0DDD56A59A358BEA070E82D3F7911C44207484C7E
          065B512F1F61FAC6D40A8E766DF9DDCD493B5082A2730CD1441306D5ACA9EB05
          E0BC30C4B66A9CF9C997BAA62DE8E112C00150351CA9FEB37202DE37DEEBDE1D
          946E69075FAE6E0639BD94F9CBEC63E89A59FF005C43B0D647A676DFD1DED14D
          268F53CA3B3DE1CAF8BDFF001A927FFFDA000C03010002000300000010E34F30
          D3CD38D3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF1CF
          1CB0CF1C30CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CFFFC400141101000000000000000000
          000000000000B0FFDA0008010301013F1039AFFFC40014110100000000000000
          0000000000000000B0FFDA0008010201013F1039AFFFC4001F10010101000301
          000301010000000000000111210031415160618171D0FFDA0008010100013F10
          FC69EC1EA80C92CC0C8BD02F11204DFD23EEAC72E8A70C501ABD580D4150108D
          A22108B383252ED843DB8A255529D82A18FA684CAB82B1514954ECA47291875C
          70D34F1DFAD2083E97A853A0B4E87F49658056A8EB8BC83F652AB64CF9CA7E2D
          95140345AF943BC6DAF46840DC1AF934609ECDCA42046942A1530DE16B96BA1A
          1683916E6CB03BAD655F5059419020DBDF0B0148AC200420A91170AD5F59385E
          0878B1FDF380E7BADA7DB4350A33ACC3F09321EF6CD8572B29F4908184166A88
          1A3C4727719C2A104180573DC4EDDE2A44726488938B49AFB00BE996AB9DBD57
          261335CEB4531963A65293257AE20325914081583215C723CA5D053E80201404
          F5DE1E9A2B0820920307FA5338AAAF8EFC02159B3CE25DF6F4510002210AF481
          38E00477F4EA7493F778B15D1D00147496B7B78451FA6707D8F66AF44813821A
          0E01438FBE40110870EECB62649E84047AA7478615CA083283180E1A9CD578D8
          6DABFD876C0AAEFF00CD49FFD9}
      end
      object QRShape162: TQRShape
        Left = 8
        Top = 292
        Width = 561
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          772.583333333333400000
          1484.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape163: TQRShape
        Left = 8
        Top = 319
        Width = 561
        Height = 40
        Size.Values = (
          105.833333333333300000
          21.166666666666670000
          844.020833333333500000
          1484.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qr104: TQRLabel
        Left = 8
        Top = 266
        Width = 193
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          703.791666666666800000
          510.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Cedente'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
    end
    object qrbnd3: TQRBand
      Left = 38
      Top = 2154
      Width = 718
      Height = 1051
      Frame.Style = psClear
      AlignToBottom = False
      Color = clWindow
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        2780.770833333333000000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbTitle
      object qr105: TQRLabel
        Left = 16
        Top = 177
        Width = 681
        Height = 17
        Size.Values = (
          44.979166666666670000
          42.333333333333340000
          468.312500000000100000
          1801.812500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 
          'Garanta a felicidade da sua empresa e de seu cliente utilizando ' +
          'o ADM - Sistema de Gest'#227'o Completo para sua Empresa.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRImage10: TQRImage
        Left = 8
        Top = 392
        Width = 145
        Height = 25
        Size.Values = (
          66.145833333333340000
          21.166666666666670000
          1037.166666666667000000
          383.645833333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Picture.Data = {
          0A544A504547496D6167655E180000FFD8FFE000104A46494600010100000100
          010000FFDB004300080606070605080707070909080A0C140D0C0B0B0C191213
          0F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F2739
          3D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232
          3232323232323232323232323232323232323232323232323232323232323232
          32323232323232323232323232FFC2001108041702CE03012200021101031101
          FFC4001A000100030101010000000000000000000000020506040307FFC40014
          010100000000000000000000000000000000FFDA000C03010002100310000001
          DFBE6FA5346CCFA9A167E45F315606954B406E5931AC646E8B463B4677327AC0
          0000000000000000000000000000000000000000000000000000033B2D00ACF0
          BA14DD5DE29BCEF4524AE452CAE0574FB853CED4717AF4000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000FFFC4002010000203000104030000000000000000000304010205001113
          14601523D0FFDA0008010100010502F5A4EB4A24DE85D431DD6E0D2F1CD6F25C
          29ACCC3989DD72BC2E8106EC364F198E877C4DDC7995D264C270C626424C5DBA
          48EA179D3F8C9E448C25F49AE4D6004CAA92252A4DAD9F11CBA1D4B0A0EA8DF3
          E26233220A5CEEE5E50FBFE3A074167083C32743882AD17295193CDD2EE588B5
          6EC8072107E6A47FFFC400141101000000000000000000000000000000B0FFDA
          0008010301013F0139AFFFC400141101000000000000000000000000000000B0
          FFDA0008010201013F0139AFFFC4003110000201030204030606030000000000
          00010203000411122113314151226181142432607191052342A1B1C1D0D1F0FF
          DA0008010100063F02F96ACE7E018C897C5719C0C67FE1BD3926378D08D48AA7
          5007CF90ABB58521D36C0312F9E58CD4315AA46247884C78BC80F4A782110092
          155E297CE093DAA59C2E9D50BEDF7AFC2E3B768C078793677217AD2C6A61910C
          A1084C92B9DB73CB9F4ABD930B981982FA0A0D25B7B466CB5685EF9AB62B7313
          B367C5206258F60399AB230C51EBB8D790C79115722E151648E40A74F23BA9FE
          E9A6C28889FCBEF8EE69E5BF859B32E62B8CF8547407B54B37555DBEBD2A5B44
          78D8695901439DF186FDFE4A16ED712B5B83911EC3EF53271E458666D6C831F1
          55D36A6F795D2DE5B62A2686678A58E3E187D8E57CC53491DCCB1BB8024231E2
          C7F06BD917223D1A2AD744CE8D6E30AD8076C6299B8F2683371B46DF154D8B89
          5239BE345C73A8E58E778CAC7C2D80DC7AD40B04AF1707233B1C83CF9D5B6967
          F77D5A73E75346C5B12B063E98FF005533A16C4A7515E80D32CB732340C73C2C
          0EF9E744BCF2B299049A7A6DD3E951CFA995D015F0F507BD2C6646908FD4DCCF
          F8D49FFFC4002310010100020202020300030000000000000111002131415161
          71816091A1D0E1F0FFDA0008010100013F21FC6BA74A3DAD18D887006F9F3700
          687B0791781E4EF1D7510A56A03BD3E319C3DA56EA1C96E742E029D93D7B71AF
          0DCF4807FA63B6D434B61A751D7BFAC1804713A1E130E15A54B18C52EF2FCA2D
          026DA2B4F1AAEF03518DC2487B217D59BC8808109D9BE3B9F5EF28EDDFD513E8
          62891C9E434FC8F5D60DB02CB57738B3E5706F02372F87F5335A5C0EC367C0D7
          BFC2B867CC0EEC4152E6A3E0DDD56A59A358BEA070E82D3F7911C44207484C7E
          065B512F1F61FAC6D40A8E766DF9DDCD493B5082A2730CD1441306D5ACA9EB05
          E0BC30C4B66A9CF9C997BAA62DE8E112C00150351CA9FEB37202DE37DEEBDE1D
          946E69075FAE6E0639BD94F9CBEC63E89A59FF005C43B0D647A676DFD1DED14D
          268F53CA3B3DE1CAF8BDFF001A927FFFDA000C03010002000300000010E34F30
          D3CD38D3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF1CF
          1CB0CF1C30CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CFFFC400141101000000000000000000
          000000000000B0FFDA0008010301013F1039AFFFC40014110100000000000000
          0000000000000000B0FFDA0008010201013F1039AFFFC4001F10010101000301
          000301010000000000000111210031415160618171D0FFDA0008010100013F10
          FC69EC1EA80C92CC0C8BD02F11204DFD23EEAC72E8A70C501ABD580D4150108D
          A22108B383252ED843DB8A255529D82A18FA684CAB82B1514954ECA47291875C
          70D34F1DFAD2083E97A853A0B4E87F49658056A8EB8BC83F652AB64CF9CA7E2D
          95140345AF943BC6DAF46840DC1AF934609ECDCA42046942A1530DE16B96BA1A
          1683916E6CB03BAD655F5059419020DBDF0B0148AC200420A91170AD5F59385E
          0878B1FDF380E7BADA7DB4350A33ACC3F09321EF6CD8572B29F4908184166A88
          1A3C4727719C2A104180573DC4EDDE2A44726488938B49AFB00BE996AB9DBD57
          261335CEB4531963A65293257AE20325914081583215C723CA5D053E80201404
          F5DE1E9A2B0820920307FA5338AAAF8EFC02159B3CE25DF6F4510002210AF481
          38E00477F4EA7493F778B15D1D00147496B7B78451FA6707D8F66AF44813821A
          0E01438FBE40110870EECB62649E84047AA7478615CA083283180E1A9CD578D8
          6DABFD876C0AAEFF00CD49FFD9}
      end
      object QRShape164: TQRShape
        Left = 8
        Top = 416
        Width = 561
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          1100.666666666667000000
          1484.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape165: TQRShape
        Left = 568
        Top = 416
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1100.666666666667000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qr106: TQRLabel
        Left = 8
        Top = 417
        Width = 177
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1103.312500000000000000
          468.312500000000100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Local de Pagamento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr107: TQRLabel
        Left = 8
        Top = 429
        Width = 249
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1135.062500000000000000
          658.812500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Pag'#225'vel em qualquer banco at'#233' o vencimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr108: TQRLabel
        Left = 570
        Top = 417
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1103.312500000000000000
          354.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vencimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr109: TQRLabel
        Left = 568
        Top = 429
        Width = 137
        Height = 14
        Size.Values = (
          37.041666666666670000
          1502.833333333333000000
          1135.062500000000000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'QRLabel63'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRShape166: TQRShape
        Left = 568
        Top = 443
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1172.104166666667000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape167: TQRShape
        Left = 8
        Top = 443
        Width = 561
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          1172.104166666667000000
          1484.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qr110: TQRLabel
        Left = 8
        Top = 444
        Width = 193
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1174.750000000000000000
          510.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Cedente'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr111: TQRLabel
        Left = 570
        Top = 444
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1174.750000000000000000
          354.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ag'#234'ncia/C'#243'd.Cedente'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRShape168: TQRShape
        Left = 8
        Top = 470
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          1243.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape169: TQRShape
        Left = 120
        Top = 470
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          317.500000000000000000
          1243.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape170: TQRShape
        Left = 344
        Top = 470
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          910.166666666666600000
          1243.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape171: TQRShape
        Left = 232
        Top = 470
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          613.833333333333400000
          1243.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape172: TQRShape
        Left = 456
        Top = 470
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          1206.500000000000000000
          1243.541666666667000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape173: TQRShape
        Left = 568
        Top = 470
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1243.541666666667000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape174: TQRShape
        Left = 8
        Top = 497
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          1314.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape175: TQRShape
        Left = 120
        Top = 497
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          317.500000000000000000
          1314.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape176: TQRShape
        Left = 232
        Top = 497
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          613.833333333333400000
          1314.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape177: TQRShape
        Left = 344
        Top = 497
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          910.166666666666600000
          1314.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape178: TQRShape
        Left = 456
        Top = 497
        Width = 113
        Height = 28
        Size.Values = (
          74.083333333333340000
          1206.500000000000000000
          1314.979166666667000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape179: TQRShape
        Left = 568
        Top = 497
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1314.979166666667000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape180: TQRShape
        Left = 8
        Top = 524
        Width = 561
        Height = 136
        Size.Values = (
          359.833333333333400000
          21.166666666666670000
          1386.416666666667000000
          1484.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape181: TQRShape
        Left = 568
        Top = 524
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1386.416666666667000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape182: TQRShape
        Left = 568
        Top = 551
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1457.854166666667000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape183: TQRShape
        Left = 568
        Top = 578
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1529.291666666667000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape184: TQRShape
        Left = 568
        Top = 605
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1600.729166666667000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape185: TQRShape
        Left = 568
        Top = 632
        Width = 137
        Height = 28
        Size.Values = (
          74.083333333333340000
          1502.833333333333000000
          1672.166666666667000000
          362.479166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape186: TQRShape
        Left = 8
        Top = 659
        Width = 697
        Height = 74
        Size.Values = (
          195.791666666666700000
          21.166666666666670000
          1743.604166666667000000
          1844.145833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape187: TQRShape
        Left = 8
        Top = 376
        Width = 697
        Height = 25
        Frame.Style = psDot
        Size.Values = (
          66.145833333333340000
          21.166666666666670000
          994.833333333333400000
          1844.145833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Pen.Style = psDot
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape188: TQRShape
        Left = 8
        Top = 792
        Width = 697
        Height = 25
        Frame.Style = psDot
        Size.Values = (
          66.145833333333340000
          21.166666666666670000
          2095.500000000000000000
          1844.145833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Pen.Style = psDot
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object qr112: TQRLabel
        Left = 600
        Top = 734
        Width = 105
        Height = 14
        Size.Values = (
          37.041666666666670000
          1587.500000000000000000
          1942.041666666667000000
          277.812500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Autentica'#231#227'o Mec'#226'nica'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr113: TQRLabel
        Left = 600
        Top = 750
        Width = 105
        Height = 14
        Size.Values = (
          37.041666666666670000
          1587.500000000000000000
          1984.375000000000000000
          277.812500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ficha de Compensa'#231#227'o'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr114: TQRLabel
        Left = 8
        Top = 471
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1246.187500000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Data Docto'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr115: TQRLabel
        Left = 120
        Top = 471
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          317.500000000000000000
          1246.187500000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Nr. Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr116: TQRLabel
        Left = 232
        Top = 471
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          613.833333333333400000
          1246.187500000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Esp'#233'cie'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr117: TQRLabel
        Left = 456
        Top = 471
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          1206.500000000000000000
          1246.187500000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Data Proc.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr118: TQRLabel
        Left = 344
        Top = 471
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          910.166666666666600000
          1246.187500000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Aceite'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr119: TQRLabel
        Left = 8
        Top = 498
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1317.625000000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Uso do Banco'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr120: TQRLabel
        Left = 120
        Top = 498
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          317.500000000000000000
          1317.625000000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Carteira'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr121: TQRLabel
        Left = 232
        Top = 498
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          613.833333333333400000
          1317.625000000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Moeda'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr122: TQRLabel
        Left = 344
        Top = 498
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          910.166666666666600000
          1317.625000000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Quantidade'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr123: TQRLabel
        Left = 456
        Top = 498
        Width = 113
        Height = 14
        Size.Values = (
          37.041666666666670000
          1206.500000000000000000
          1317.625000000000000000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Valor'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr124: TQRLabel
        Left = 8
        Top = 532
        Width = 193
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1407.583333333333000000
          510.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Instru'#231#245'es'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr125: TQRLabel
        Left = 8
        Top = 660
        Width = 193
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1746.250000000000000000
          510.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Sacado'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr126: TQRLabel
        Left = 8
        Top = 716
        Width = 193
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          1894.416666666667000000
          510.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Sacador/Avalista:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr127: TQRLabel
        Left = 570
        Top = 471
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1246.187500000000000000
          354.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nosso N'#250'mero'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr128: TQRLabel
        Left = 570
        Top = 498
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1317.625000000000000000
          354.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor do Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr129: TQRLabel
        Left = 570
        Top = 525
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1389.062500000000000000
          354.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(-) Desconto/Abatimento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr130: TQRLabel
        Left = 570
        Top = 552
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1460.500000000000000000
          354.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(-) Outras Dedu'#231#245'es'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr131: TQRLabel
        Left = 570
        Top = 579
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1531.937500000000000000
          354.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(+) Mora/Multa'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr132: TQRLabel
        Left = 570
        Top = 606
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1603.375000000000000000
          354.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(+) Outros'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr133: TQRLabel
        Left = 570
        Top = 633
        Width = 134
        Height = 14
        Size.Values = (
          37.041666666666670000
          1508.125000000000000000
          1674.812500000000000000
          354.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(=) Valor Cobrado'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRShape189: TQRShape
        Left = 8
        Top = 208
        Width = 697
        Height = 25
        Frame.Style = psDot
        Size.Values = (
          66.145833333333340000
          21.166666666666670000
          550.333333333333400000
          1844.145833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Pen.Style = psDot
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape190: TQRShape
        Left = 8
        Top = 265
        Width = 561
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          701.145833333333400000
          1484.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRImage11: TQRImage
        Left = 8
        Top = 240
        Width = 145
        Height = 25
        Size.Values = (
          66.145833333333340000
          21.166666666666670000
          635.000000000000000000
          383.645833333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Picture.Data = {
          0A544A504547496D6167655E180000FFD8FFE000104A46494600010100000100
          010000FFDB004300080606070605080707070909080A0C140D0C0B0B0C191213
          0F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F2739
          3D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232
          3232323232323232323232323232323232323232323232323232323232323232
          32323232323232323232323232FFC2001108041702CE03012200021101031101
          FFC4001A000100030101010000000000000000000000020506040307FFC40014
          010100000000000000000000000000000000FFDA000C03010002100310000001
          DFBE6FA5346CCFA9A167E45F315606954B406E5931AC646E8B463B4677327AC0
          0000000000000000000000000000000000000000000000000000033B2D00ACF0
          BA14DD5DE29BCEF4524AE452CAE0574FB853CED4717AF4000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000FFFC4002010000203000104030000000000000000000304010205001113
          14601523D0FFDA0008010100010502F5A4EB4A24DE85D431DD6E0D2F1CD6F25C
          29ACCC3989DD72BC2E8106EC364F198E877C4DDC7995D264C270C626424C5DBA
          48EA179D3F8C9E448C25F49AE4D6004CAA92252A4DAD9F11CBA1D4B0A0EA8DF3
          E26233220A5CEEE5E50FBFE3A074167083C32743882AD17295193CDD2EE588B5
          6EC8072107E6A47FFFC400141101000000000000000000000000000000B0FFDA
          0008010301013F0139AFFFC400141101000000000000000000000000000000B0
          FFDA0008010201013F0139AFFFC4003110000201030204030606030000000000
          00010203000411122113314151226181142432607191052342A1B1C1D0D1F0FF
          DA0008010100063F02F96ACE7E018C897C5719C0C67FE1BD3926378D08D48AA7
          5007CF90ABB58521D36C0312F9E58CD4315AA46247884C78BC80F4A782110092
          155E297CE093DAA59C2E9D50BEDF7AFC2E3B768C078793677217AD2C6A61910C
          A1084C92B9DB73CB9F4ABD930B981982FA0A0D25B7B466CB5685EF9AB62B7313
          B367C5206258F60399AB230C51EBB8D790C79115722E151648E40A74F23BA9FE
          E9A6C28889FCBEF8EE69E5BF859B32E62B8CF8547407B54B37555DBEBD2A5B44
          78D8695901439DF186FDFE4A16ED712B5B83911EC3EF53271E458666D6C831F1
          55D36A6F795D2DE5B62A2686678A58E3E187D8E57CC53491DCCB1BB8024231E2
          C7F06BD917223D1A2AD744CE8D6E30AD8076C6299B8F2683371B46DF154D8B89
          5239BE345C73A8E58E778CAC7C2D80DC7AD40B04AF1707233B1C83CF9D5B6967
          F77D5A73E75346C5B12B063E98FF005533A16C4A7515E80D32CB732340C73C2C
          0EF9E744BCF2B299049A7A6DD3E951CFA995D015F0F507BD2C6646908FD4DCCF
          F8D49FFFC4002310010100020202020300030000000000000111002131415161
          71816091A1D0E1F0FFDA0008010100013F21FC6BA74A3DAD18D887006F9F3700
          687B0791781E4EF1D7510A56A03BD3E319C3DA56EA1C96E742E029D93D7B71AF
          0DCF4807FA63B6D434B61A751D7BFAC1804713A1E130E15A54B18C52EF2FCA2D
          026DA2B4F1AAEF03518DC2487B217D59BC8808109D9BE3B9F5EF28EDDFD513E8
          62891C9E434FC8F5D60DB02CB57738B3E5706F02372F87F5335A5C0EC367C0D7
          BFC2B867CC0EEC4152E6A3E0DDD56A59A358BEA070E82D3F7911C44207484C7E
          065B512F1F61FAC6D40A8E766DF9DDCD493B5082A2730CD1441306D5ACA9EB05
          E0BC30C4B66A9CF9C997BAA62DE8E112C00150351CA9FEB37202DE37DEEBDE1D
          946E69075FAE6E0639BD94F9CBEC63E89A59FF005C43B0D647A676DFD1DED14D
          268F53CA3B3DE1CAF8BDFF001A927FFFDA000C03010002000300000010E34F30
          D3CD38D3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF1CF
          1CB0CF1C30CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF
          3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3
          CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3C
          F3CF3CF3CF3CF3CF3CF3CF3CF3CF3CF3CFFFC400141101000000000000000000
          000000000000B0FFDA0008010301013F1039AFFFC40014110100000000000000
          0000000000000000B0FFDA0008010201013F1039AFFFC4001F10010101000301
          000301010000000000000111210031415160618171D0FFDA0008010100013F10
          FC69EC1EA80C92CC0C8BD02F11204DFD23EEAC72E8A70C501ABD580D4150108D
          A22108B383252ED843DB8A255529D82A18FA684CAB82B1514954ECA47291875C
          70D34F1DFAD2083E97A853A0B4E87F49658056A8EB8BC83F652AB64CF9CA7E2D
          95140345AF943BC6DAF46840DC1AF934609ECDCA42046942A1530DE16B96BA1A
          1683916E6CB03BAD655F5059419020DBDF0B0148AC200420A91170AD5F59385E
          0878B1FDF380E7BADA7DB4350A33ACC3F09321EF6CD8572B29F4908184166A88
          1A3C4727719C2A104180573DC4EDDE2A44726488938B49AFB00BE996AB9DBD57
          261335CEB4531963A65293257AE20325914081583215C723CA5D053E80201404
          F5DE1E9A2B0820920307FA5338AAAF8EFC02159B3CE25DF6F4510002210AF481
          38E00477F4EA7493F778B15D1D00147496B7B78451FA6707D8F66AF44813821A
          0E01438FBE40110870EECB62649E84047AA7478615CA083283180E1A9CD578D8
          6DABFD876C0AAEFF00CD49FFD9}
      end
      object QRShape191: TQRShape
        Left = 8
        Top = 292
        Width = 561
        Height = 28
        Size.Values = (
          74.083333333333340000
          21.166666666666670000
          772.583333333333400000
          1484.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape192: TQRShape
        Left = 8
        Top = 319
        Width = 561
        Height = 40
        Size.Values = (
          105.833333333333300000
          21.166666666666670000
          844.020833333333500000
          1484.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qr134: TQRLabel
        Left = 8
        Top = 266
        Width = 193
        Height = 14
        Size.Values = (
          37.041666666666670000
          21.166666666666670000
          703.791666666666800000
          510.645833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ' Cedente'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
    end
  end
end
