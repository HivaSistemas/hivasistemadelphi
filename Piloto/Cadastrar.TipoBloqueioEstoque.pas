unit Cadastrar.TipoBloqueioEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls,
  CheckBoxLuka, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _RecordsCadastros, _Biblioteca,
  _Sessao, _RecordsEspeciais;

type
  TFormCadastroTipoBloqueioEstoque = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eDescricao: TEditLuka;
  private
    procedure PreencherRegistro(pTipoBloqueioEstoque: RecTipoBloqueioEstoque);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

uses Pesquisa.TipoBloqueioEstoque, _TipoBloqueioEstoque;

{ TFormTipoBloqueioEstoque }

procedure TFormCadastroTipoBloqueioEstoque.BuscarRegistro;
var
  vTipoBloqueioEstoque: TArray<RecTipoBloqueioEstoque>;
begin
  vTipoBloqueioEstoque := _TipoBloqueioEstoque.BuscarTipoBloqueioEstoque(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if vTipoBloqueioEstoque = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    eID.SetFocus;
    Exit;
  end;

  inherited;
  PreencherRegistro(vTipoBloqueioEstoque[0]);
end;

procedure TFormCadastroTipoBloqueioEstoque.ExcluirRegistro;
var
  vRetorno: RecRetornoBD;
begin
  vRetorno := _TipoBloqueioEstoque.ExcluirTipoBloqueioEstoque(Sessao.getConexaoBanco, eId.AsInt);
  if vRetorno.TeveErro then begin
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroTipoBloqueioEstoque.GravarRegistro(Sender: TObject);
var
  vRetorno: RecRetornoBD;
begin
  inherited;
  vRetorno :=
    _TipoBloqueioEstoque.AtualizarTipoBloqueioEstoque(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eDescricao.Text,
      ckAtivo.CheckedStr
    );

  if vRetorno.TeveErro then begin
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetorno.AsInt);
end;

procedure TFormCadastroTipoBloqueioEstoque.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([eDescricao, ckAtivo], pEditando);

  if pEditando then begin
    SetarFoco(eDescricao);
    ckAtivo.Checked := True;
  end;
end;

procedure TFormCadastroTipoBloqueioEstoque.PesquisarRegistro;
var
  vTipoBloqueioEstoque: RecTipoBloqueioEstoque;
begin
  vTipoBloqueioEstoque := RecTipoBloqueioEstoque(Pesquisa.TipoBloqueioEstoque.Pesquisar());  //AquiS
  if vTipoBloqueioEstoque = nil then
    Exit;

  inherited;
  PreencherRegistro(vTipoBloqueioEstoque);
end;

procedure TFormCadastroTipoBloqueioEstoque.PreencherRegistro(pTipoBloqueioEstoque: RecTipoBloqueioEstoque);
begin
  eID.AsInt                     := pTipoBloqueioEstoque.tipo_bloqueio_id;
  eDescricao.Text               := pTipoBloqueioEstoque.descricao;
  ckAtivo.CheckedStr            := pTipoBloqueioEstoque.ativo;
  pTipoBloqueioEstoque.Free;
end;

procedure TFormCadastroTipoBloqueioEstoque.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eDescricao.Trim = '' then begin
    _Biblioteca.Exclamar('A descri��o n�o foi informado corretamente, verifique!');
    SetarFoco(eDescricao);
    Abort;
  end;
end;

end.
