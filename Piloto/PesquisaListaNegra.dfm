inherited FormPesquisaListaNegra: TFormPesquisaListaNegra
  Caption = 'Pesquisa de lista negra'
  ClientHeight = 233
  ClientWidth = 396
  ExplicitWidth = 404
  ExplicitHeight = 264
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 396
    Height = 187
    ColCount = 4
    OnDrawCell = sgPesquisaDrawCell
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Nome'
      'Ativo?')
    RealColCount = 4
    ExplicitWidth = 396
    ExplicitHeight = 187
    ColWidths = (
      28
      55
      222
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Left = 8
    Top = 176
    Text = '0'
    ExplicitLeft = 8
    ExplicitTop = 176
  end
  inherited pnFiltro1: TPanel
    Width = 396
    ExplicitWidth = 396
    inherited eValorPesquisa: TEditLuka
      Width = 192
      ExplicitWidth = 192
    end
  end
end
