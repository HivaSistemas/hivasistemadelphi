inherited FormInformacoesTransferenciaEmpresas: TFormInformacoesTransferenciaEmpresas
  Caption = 'Informa'#231#245'es de transfer'#234'ncias de empresas'
  ClientHeight = 448
  ClientWidth = 797
  ExplicitWidth = 803
  ExplicitHeight = 477
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 4
    Top = 3
    Width = 37
    Height = 14
    Caption = 'C'#243'digo'
  end
  object lb2: TLabel [1]
    Left = 77
    Top = 3
    Width = 105
    Height = 14
    Caption = 'Empresa de origem'
  end
  object Label1: TLabel [2]
    Left = 328
    Top = 3
    Width = 108
    Height = 14
    Caption = 'Empresa de destino'
  end
  object Label2: TLabel [3]
    Left = 327
    Top = 86
    Width = 53
    Height = 14
    Caption = 'Motorista'
  end
  object Label4: TLabel [4]
    Left = 4
    Top = 46
    Width = 93
    Height = 14
    Caption = 'Usu'#225'rio cadastro'
  end
  object Label5: TLabel [5]
    Left = 278
    Top = 46
    Width = 106
    Height = 14
    Caption = 'Data/hora cadastro'
  end
  object Label6: TLabel [6]
    Left = 403
    Top = 46
    Width = 76
    Height = 14
    Caption = 'Usu'#225'rio baixa'
  end
  object Label7: TLabel [7]
    Left = 674
    Top = 46
    Width = 89
    Height = 14
    Caption = 'Data/hora baixa'
  end
  object Label8: TLabel [8]
    Left = 4
    Top = 86
    Width = 123
    Height = 14
    Caption = 'Usu'#225'rio cancelamento'
  end
  object Label9: TLabel [9]
    Left = 207
    Top = 86
    Width = 97
    Height = 14
    Caption = 'Data/hora cancel.'
  end
  object Label3: TLabel [10]
    Left = 563
    Top = 3
    Width = 121
    Height = 14
    Caption = 'Valor de transfer'#234'ncia'
  end
  inherited pnOpcoes: TPanel
    Top = 411
    Width = 797
    ExplicitTop = 411
    ExplicitWidth = 797
  end
  object eTransferenciaId: TEditLuka
    Left = 4
    Top = 18
    Width = 67
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 1
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eEmpresaOrigem: TEditLuka
    Left = 77
    Top = 18
    Width = 245
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 2
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eEmpresaDestino: TEditLuka
    Left = 328
    Top = 18
    Width = 229
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 3
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eMotorista: TEditLuka
    Left = 327
    Top = 100
    Width = 229
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 4
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eUsuarioCadastro: TEditLuka
    Left = 4
    Top = 60
    Width = 268
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 5
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eDataHoraCadastro: TEditLukaData
    Left = 278
    Top = 60
    Width = 119
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999 99:99:99;1;_'
    MaxLength = 19
    ReadOnly = True
    TabOrder = 6
    Text = '  /  /       :  :  '
    TipoData = tdDataHora
  end
  object eUsuarioBaixa: TEditLuka
    Left = 403
    Top = 60
    Width = 265
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 7
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eDataHoraBaixa: TEditLukaData
    Left = 674
    Top = 60
    Width = 119
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999 99:99:99;1;_'
    MaxLength = 19
    ReadOnly = True
    TabOrder = 8
    Text = '  /  /       :  :  '
    TipoData = tdDataHora
  end
  object eUsuarioCancelamento: TEditLuka
    Left = 4
    Top = 100
    Width = 197
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 9
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eDataHoraCancelamento: TEditLukaData
    Left = 207
    Top = 100
    Width = 114
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999 99:99:99;1;_'
    MaxLength = 19
    ReadOnly = True
    TabOrder = 10
    Text = '  /  /       :  :  '
    TipoData = tdDataHora
  end
  object eValorTransferencia: TEditLuka
    Left = 563
    Top = 18
    Width = 125
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 11
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object StaticTextLuka1: TStaticTextLuka
    Left = 562
    Top = 88
    Width = 124
    Height = 20
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Status'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 12
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object stStatus: TStaticText
    Left = 562
    Top = 102
    Width = 124
    Height = 20
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Aberto'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 13
    Transparent = False
  end
  object sgItens: TGridLuka
    Left = 4
    Top = 128
    Width = 789
    Height = 281
    Align = alCustom
    ColCount = 8
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
    TabOrder = 14
    OnDrawCell = sgItensDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Produto'
      'Nome'
      'Marca'
      'Preco unit'#225'rio'
      'Quantidade'
      'Valor total'
      'Lote'
      'Local de origem')
    Grid3D = False
    RealColCount = 10
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      54
      158
      140
      87
      81
      83
      60
      99)
  end
  object StaticTextLuka2: TStaticTextLuka
    Left = 686
    Top = 88
    Width = 107
    Height = 15
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Qtde de itens'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 15
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object stQuantidadeItensGrid: TStaticText
    Left = 685
    Top = 102
    Width = 108
    Height = 20
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 16
    Transparent = False
  end
end
