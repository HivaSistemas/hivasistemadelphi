unit FrameCodigoBarrasAuxiliar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Frame.HerancaInsercaoExclusao, BuscaDados,
  Vcl.Menus, Vcl.StdCtrls, StaticTextLuka, Vcl.Grids, GridLuka, _ProdutosCodigosBarrasAux;

type
  TFrCodigoBarrasAuxiliar = class(TFrameHerancaInsercaoExclusao)
    procedure sgValoresKeyPress(Sender: TObject; var Key: Char);
    procedure sgValoresDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  private
    procedure AddGrid(pValor: string);
    procedure setCodigoBarras(pValor: TArray<RecProdutosCodigosBarrasAux>);
    function getCodigoBarras: TArray<RecProdutosCodigosBarrasAux>;
  protected
    procedure Inserir; override;
  published
    property CodigoBarras: TArray<RecProdutosCodigosBarrasAux> read getCodigoBarras write setCodigoBarras;
  end;

implementation

{$R *.dfm}

const
  coCodigoBarras = 0;

{ TFrCodigoBarrasAuxiliar }

procedure TFrCodigoBarrasAuxiliar.AddGrid(pValor: string);
var
  vLinha: Integer;
begin
  if sgValores.Cells[coCodigoBarras, 1] = '' then
    vLinha := 1
  else begin
    vLinha := sgValores.Localizar([coCodigoBarras], [pValor]);
    if vLinha > 0 then begin
      sgValores.Row := vLinha;
      Exit;
    end;

    vLinha := sgValores.RowCount;
  end;

  sgValores.Cells[coCodigoBarras, vLinha] := pValor;

  sgValores.SetLinhasGridPorTamanhoVetor( vLinha );
  sgValores.Row := vLinha;
  Self.SetFocus;
end;

function TFrCodigoBarrasAuxiliar.getCodigoBarras: TArray<RecProdutosCodigosBarrasAux>;
var
  i: Integer;
begin
  Result := nil;
  if sgValores.Cells[coCodigoBarras, 1] = '' then
    Exit;

  SetLength(Result, sgValores.RowCount -1);
  for i := 1 to sgValores.RowCount -1 do
    Result[i - 1].CodigoBarras := sgValores.Cells[coCodigoBarras, i];
end;

procedure TFrCodigoBarrasAuxiliar.Inserir;
var
  vCodigoBarras: string;
begin
  inherited;
  vCodigoBarras := BuscaDados.BuscarDados('C�digo de barras', '', 14);
  if vCodigoBarras = '' then
    Exit;

  AddGrid(vCodigoBarras);
end;

procedure TFrCodigoBarrasAuxiliar.setCodigoBarras(pValor: TArray<RecProdutosCodigosBarrasAux>);
var
  i: Integer;
begin
  for i := Low(pValor) to High(pValor) do
    sgValores.Cells[coCodigoBarras, i + 1] := pValor[i].CodigoBarras;

  sgValores.SetLinhasGridPorTamanhoVetor( Length(pValor) );
end;

procedure TFrCodigoBarrasAuxiliar.sgValoresDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  inherited;
  sgValores.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taLeftJustify, Rect);
end;

procedure TFrCodigoBarrasAuxiliar.sgValoresKeyPress(Sender: TObject; var Key: Char);
var
  vCodigoBarras: string;
begin
  inherited;
  if CharInSet(Key, ['0'..'9', 'A'..'Z', 'a'..'z' , '.', '-' , '?']) then begin
    vCodigoBarras := BuscaDados.BuscarDados(tiTexto, 'Busca digitada', Key);
    if vCodigoBarras <> '' then
      AddGrid(vCodigoBarras)
    else
      SetFocus;
  end;
end;

end.
