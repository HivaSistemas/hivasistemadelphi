inherited FormCadastroCreditosManuaisPagar: TFormCadastroCreditosManuaisPagar
  Caption = 'Lan'#231'amento de cr'#233'dito a pagar'
  ClientHeight = 352
  ClientWidth = 706
  ExplicitWidth = 712
  ExplicitHeight = 381
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label1: TLabel
    Top = 2
    ExplicitTop = 2
  end
  inherited lbl1: TLabel
    Top = 45
    ExplicitTop = 45
  end
  inherited lbllb12: TLabel
    Top = 85
    ExplicitTop = 85
  end
  inherited lbl2: TLabel
    Left = 435
    Top = 85
    ExplicitLeft = 435
    ExplicitTop = 85
  end
  inherited pnOpcoes: TPanel
    Height = 352
    ExplicitHeight = 318
    inherited sbDesfazer: TSpeedButton
      Top = 48
      ExplicitTop = 48
    end
    inherited sbPesquisar: TSpeedButton
      Top = 94
      ExplicitTop = 94
    end
  end
  inherited eID: TEditLuka
    Top = 16
    ExplicitTop = 16
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 707
    ExplicitLeft = 707
  end
  inherited FrEmpresa: TFrEmpresas
    Top = 0
    Width = 237
    ExplicitTop = 0
    ExplicitWidth = 237
    inherited sgPesquisa: TGridLuka
      Width = 212
      ExplicitWidth = 212
    end
    inherited PnTitulos: TPanel
      Width = 237
      ExplicitWidth = 237
      inherited lbNomePesquisa: TLabel
        Height = 15
      end
      inherited pnSuprimir: TPanel
        Left = 132
        ExplicitLeft = 132
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 212
      ExplicitLeft = 212
    end
  end
  inherited eValorCreditoGerar: TEditLuka
    Top = 59
    Font.Color = clRed
    ExplicitTop = 59
  end
  inherited FrPlanoFinanceiro: TFrPlanosFinanceiros
    Top = 42
    Width = 247
    ExplicitTop = 42
    ExplicitWidth = 247
    inherited sgPesquisa: TGridLuka
      Width = 222
      ExplicitWidth = 222
    end
    inherited PnTitulos: TPanel
      Width = 247
      ExplicitWidth = 247
      inherited lbNomePesquisa: TLabel
        Height = 15
      end
      inherited pnSuprimir: TPanel
        Left = 142
        ExplicitLeft = 142
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 222
      ExplicitLeft = 222
    end
  end
  inherited FrCentroCusto: TFrCentroCustos
    Top = 42
    Width = 197
    ExplicitTop = 42
    ExplicitWidth = 197
    inherited sgPesquisa: TGridLuka
      Width = 172
      ExplicitWidth = 172
    end
    inherited PnTitulos: TPanel
      Width = 197
      ExplicitWidth = 197
      inherited lbNomePesquisa: TLabel
        Height = 15
      end
      inherited pnSuprimir: TPanel
        Left = 92
        ExplicitLeft = 92
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 172
      ExplicitLeft = 172
    end
  end
  inherited ckImportacao: TCheckBox
    Left = 597
    Top = 298
    Width = 99
    Height = 16
    Caption = 'Cr'#233'dito manual'
    ExplicitLeft = 597
    ExplicitTop = 298
    ExplicitWidth = 99
    ExplicitHeight = 16
  end
  inherited eDataVencimento: TEditLukaData
    Top = 98
    ExplicitTop = 98
  end
  inherited meObservacoes: TMemoAltis
    Left = 435
    Top = 102
    Width = 266
    Height = 191
    ExplicitLeft = 435
    ExplicitTop = 102
    ExplicitWidth = 266
    ExplicitHeight = 191
  end
  inherited FrCadastro: TFrameCadastros
    Top = 1
    Width = 248
    ExplicitTop = 1
    ExplicitWidth = 248
    inherited sgPesquisa: TGridLuka
      Width = 223
      ExplicitWidth = 223
    end
    inherited PnTitulos: TPanel
      Width = 248
      ExplicitWidth = 248
      inherited lbNomePesquisa: TLabel
        Height = 15
      end
      inherited pnSuprimir: TPanel
        Left = 143
        ExplicitLeft = 143
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 223
      ExplicitLeft = 223
    end
  end
  inherited FrPagamentoFinanceiro: TFrPagamentoFinanceiro
    Left = 126
    Top = 127
    Height = 218
    ExplicitLeft = 126
    ExplicitTop = 127
    ExplicitHeight = 218
    inherited grpFechamento: TGroupBox
      Height = 218
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitHeight = 189
      inherited lbllb13: TLabel
        Left = 32
        Top = 105
        Width = 78
        ExplicitLeft = 32
        ExplicitTop = 105
        ExplicitWidth = 78
      end
      inherited lbllb14: TLabel
        Top = 147
        ExplicitTop = 147
      end
      inherited lbllb15: TLabel
        Left = 290
        Top = 182
        ExplicitLeft = 290
        ExplicitTop = 182
      end
      inherited lbllb10: TLabel
        Top = 191
        ExplicitTop = 191
      end
      inherited lbllb7: TLabel
        ExplicitTop = -12
      end
      inherited lbllb19: TLabel
        Left = 7
        Top = 18
        ExplicitLeft = 7
        ExplicitTop = 18
      end
      inherited lb4: TLabel
        Top = 125
        ExplicitTop = 125
      end
      inherited sbBuscarDadosCartoesDebito: TImage
        Top = 101
        ExplicitTop = 101
      end
      inherited sbBuscarDadosCartoesCredito: TImage
        Top = 122
        ExplicitTop = 122
      end
      inherited sbBuscarDadosCobranca: TImage
        Top = 143
        ExplicitTop = 143
      end
      inherited sbBuscarDadosCreditos: TImage
        Left = 449
        Top = 178
        ExplicitLeft = 449
        ExplicitTop = 178
      end
      inherited Label1: TLabel
        Top = 168
        ExplicitTop = 168
      end
      inherited sbBuscarDadosPix: TImage
        Top = 164
        ExplicitTop = 164
      end
      inherited eValorCartaoDebito: TEditLuka
        Top = 97
        OnChange = nil
        ExplicitTop = 97
      end
      inherited eValorCobranca: TEditLuka
        Top = 139
        ExplicitTop = 139
      end
      inherited eValorCredito: TEditLuka
        Left = 352
        Top = 174
        ExplicitLeft = 352
        ExplicitTop = 174
      end
      inherited stPagamento: TStaticText
        Width = 280
        Color = clSilver
        ExplicitWidth = 280
      end
      inherited eValorDiferencaPagamentos: TEditLuka
        Top = 184
        ExplicitTop = 184
      end
      inherited eValorCartaoCredito: TEditLuka
        Top = 118
        ExplicitTop = 118
      end
      inherited stValorTotal: TStaticText
        Color = clSilver
      end
      inherited stDinheiroDefinido: TStaticText
        Top = 59
        ExplicitTop = 59
      end
      inherited stChequeDefinido: TStaticText
        Height = 18
        ExplicitHeight = 18
      end
      inherited stCartaoDebitoDefinido: TStaticText
        Top = 102
        ExplicitTop = 102
      end
      inherited stCobrancaDefinido: TStaticText
        Top = 143
        Height = 18
        ExplicitTop = 143
        ExplicitHeight = 18
      end
      inherited stCreditoDefinido: TStaticText
        Left = 468
        Top = 178
        Height = 18
        ExplicitLeft = 468
        ExplicitTop = 178
        ExplicitHeight = 18
      end
      inherited stCartaoCreditoDefinido: TStaticText
        Top = 122
        ExplicitTop = 122
      end
      inherited eValorPix: TEditLuka
        Top = 160
        Height = 22
        ExplicitTop = 160
        ExplicitHeight = 22
      end
      inherited stPixDefinido: TStaticText
        Top = 164
        ExplicitTop = 164
      end
    end
  end
end
