inherited FormDefinirContaCustodiaContasReceber: TFormDefinirContaCustodiaContasReceber
  Caption = 'Definir conta cust'#243'dia de contas a receber'
  ClientHeight = 93
  ExplicitWidth = 603
  ExplicitHeight = 122
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 312
    Top = 9
    Width = 136
    Height = 14
    Caption = 'Observa'#231#245'es da cust'#243'dia'
  end
  inherited pnOpcoes: TPanel
    Top = 56
    ExplicitTop = 56
  end
  inline FrContaCustodia: TFrContasCustodia
    Left = 7
    Top = 6
    Width = 298
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 7
    ExplicitTop = 6
    ExplicitWidth = 298
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 273
      Height = 23
      ExplicitWidth = 273
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 298
      ExplicitWidth = 298
      inherited lbNomePesquisa: TLabel
        Width = 87
        ExplicitWidth = 87
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 228
        ExplicitLeft = 228
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 273
      Height = 24
      ExplicitLeft = 273
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  object eObservacoes: TEditLuka
    Left = 312
    Top = 23
    Width = 277
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 200
    TabOrder = 2
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
