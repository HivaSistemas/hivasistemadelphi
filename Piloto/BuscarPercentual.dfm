inherited FormBuscarPercentual: TFormBuscarPercentual
  Caption = 'Percentual de comiss'#227'o a vista'
  ClientHeight = 82
  ClientWidth = 262
  ExplicitWidth = 268
  ExplicitHeight = 111
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 45
    Width = 262
    inherited sbFinalizar: TSpeedButton
      Left = 57
      ExplicitLeft = 57
    end
  end
  object eVersao: TEditLuka
    Left = 8
    Top = 8
    Width = 246
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Color = clMenuBar
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
end
