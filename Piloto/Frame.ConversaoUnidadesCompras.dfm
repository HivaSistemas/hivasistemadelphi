inherited FrameConversaoUnidadesCompra: TFrameConversaoUnidadesCompra
  Width = 234
  ExplicitWidth = 234
  inherited sgValores: TGridLuka
    Width = 234
    ColCount = 3
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goRowSelect]
    OnDrawCell = sgValoresDrawCell
    HCol.Strings = (
      'Und.'
      'M'#250'ltiplo'
      'Qtde.embalagem')
    RealColCount = 3
    ExplicitWidth = 234
    ColWidths = (
      44
      67
      102)
  end
  inherited StaticTextLuka1: TStaticTextLuka
    Width = 234
    ExplicitWidth = 234
  end
end
