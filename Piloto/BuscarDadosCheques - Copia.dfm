inherited FormBuscarDadosCheque: TFormBuscarDadosCheque
  ActiveControl = FrTiposCobranca.sgPesquisa
  Caption = 'Buscar dados de cheques'
  ClientHeight = 408
  ClientWidth = 639
  ExplicitWidth = 645
  ExplicitHeight = 437
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 5
    Top = 82
    Width = 33
    Height = 14
    Anchors = [akLeft, akRight]
    Caption = 'Banco'
  end
  object lb2: TLabel [1]
    Left = 385
    Top = 37
    Width = 64
    Height = 14
    Anchors = [akLeft, akRight]
    Caption = 'Vencimento'
    Visible = False
  end
  object lb3: TLabel [2]
    Left = 100
    Top = 82
    Width = 43
    Height = 14
    Anchors = [akLeft, akRight]
    Caption = 'Ag'#234'ncia'
  end
  object lb4: TLabel [3]
    Left = 195
    Top = 82
    Width = 79
    Height = 14
    Anchors = [akLeft, akRight]
    Caption = 'Conta corrente'
  end
  object lb5: TLabel [4]
    Left = 290
    Top = 82
    Width = 71
    Height = 14
    Anchors = [akLeft, akRight]
    Caption = 'Num. cheque'
  end
  object lb6: TLabel [5]
    Left = 385
    Top = 82
    Width = 28
    Height = 14
    Anchors = [akLeft, akRight]
    Caption = 'Valor'
  end
  object lb7: TLabel [6]
    Left = 5
    Top = 125
    Width = 49
    Height = 14
    Anchors = [akLeft, akRight]
    Caption = 'Emitente'
  end
  object lb8: TLabel [7]
    Left = 290
    Top = 125
    Width = 48
    Height = 14
    Anchors = [akLeft, akRight]
    Caption = 'Telefone'
  end
  object lb9: TLabel [8]
    Left = 290
    Top = 37
    Width = 29
    Height = 14
    Anchors = [akLeft, akRight]
    Caption = 'Prazo'
    Visible = False
  end
  object lb10: TLabel [9]
    Left = 332
    Top = 37
    Width = 39
    Height = 14
    Anchors = [akLeft, akRight]
    Caption = 'Repetir'
    Visible = False
  end
  object stCondicaoPagamento: TStaticText [10]
    Left = 5
    Top = 16
    Width = 628
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = 5921326
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 12
    Transparent = False
  end
  object st3: TStaticText [11]
    Left = 5
    Top = 1
    Width = 628
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Condi'#231#227'o de pagamento'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 11
    Transparent = False
  end
  inherited pnOpcoes: TPanel
    Top = 371
    Width = 639
    TabOrder = 20
    ExplicitTop = 371
    ExplicitWidth = 639
    inherited sbFinalizar: TSpeedButton
      Left = 257
      ExplicitLeft = 257
    end
  end
  object eBanco: TEditLuka
    Left = 5
    Top = 97
    Width = 89
    Height = 22
    Alignment = taRightJustify
    Anchors = [akLeft, akRight]
    CharCase = ecUpperCase
    TabOrder = 4
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  inline FrTiposCobranca: TFrTiposCobranca
    Left = 5
    Top = 35
    Width = 279
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Anchors = [akLeft, akRight]
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 0
    TabStop = True
    ExplicitLeft = 5
    ExplicitTop = 35
    ExplicitWidth = 279
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 254
      Height = 23
      TabOrder = 1
      ExplicitWidth = 254
      ExplicitHeight = 23
    end
    inherited CkAspas: TCheckBox
      TabOrder = 2
    end
    inherited CkFiltroDuplo: TCheckBox
      TabOrder = 4
    end
    inherited CkPesquisaNumerica: TCheckBox
      TabOrder = 5
    end
    inherited CkMultiSelecao: TCheckBox
      TabOrder = 3
    end
    inherited PnTitulos: TPanel
      Width = 279
      TabOrder = 0
      ExplicitWidth = 279
      inherited lbNomePesquisa: TLabel
        Width = 99
        Height = 15
        ExplicitWidth = 99
        ExplicitHeight = 14
      end
    end
    inherited pnPesquisa: TPanel
      Left = 254
      Height = 24
      ExplicitLeft = 254
      ExplicitHeight = 24
    end
  end
  object eDataVencimento: TEditLukaData
    Left = 385
    Top = 52
    Width = 82
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    Anchors = [akLeft, akRight]
    EditMask = '99/99/9999;1; '
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
    Visible = False
    OnKeyDown = ProximoCampo
  end
  object sgChequesLancados: TGridLuka
    Left = 5
    Top = 167
    Width = 628
    Height = 197
    Anchors = [akLeft, akRight]
    ColCount = 11
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect]
    TabOrder = 19
    OnDblClick = sgChequesLancadosDblClick
    OnDrawCell = sgChequesLancadosDrawCell
    OnKeyDown = sgChequesLancadosKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Vencimento'
      'Banco'
      'Agencia'
      'Conta'
      'Num. cheque'
      'Valor'
      'Nome do dono do cheque'
      'Telefone'
      'Tipo de cobran'#231'a'
      'Parcela'
      'Nr. parcelas')
    Grid3D = True
    RealColCount = 11
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      76
      64
      58
      58
      81
      78
      158
      91
      136
      51
      78)
  end
  object eAgencia: TEditLuka
    Left = 100
    Top = 97
    Width = 89
    Height = 22
    Alignment = taRightJustify
    Anchors = [akLeft, akRight]
    CharCase = ecUpperCase
    TabOrder = 5
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eContaCorrente: TEditLuka
    Left = 195
    Top = 97
    Width = 89
    Height = 22
    Alignment = taRightJustify
    Anchors = [akLeft, akRight]
    CharCase = ecUpperCase
    TabOrder = 6
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eNumeroCheque: TEditLuka
    Left = 290
    Top = 97
    Width = 89
    Height = 22
    Anchors = [akLeft, akRight]
    CharCase = ecUpperCase
    TabOrder = 7
    OnKeyDown = ProximoCampo
    OnKeyPress = Numeros
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eValorCheque: TEditLuka
    Left = 385
    Top = 97
    Width = 82
    Height = 22
    Alignment = taRightJustify
    Anchors = [akLeft, akRight]
    CharCase = ecUpperCase
    TabOrder = 8
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eNomeEmitente: TEditLuka
    Left = 5
    Top = 140
    Width = 279
    Height = 22
    Anchors = [akLeft, akRight]
    CharCase = ecUpperCase
    TabOrder = 9
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eTelefoneEmitente: TEditTelefoneLuka
    Left = 290
    Top = 140
    Width = 177
    Height = 22
    Anchors = [akLeft, akRight]
    EditMask = '(99) 9999-9999;1; '
    MaxLength = 14
    TabOrder = 10
    Text = '(  )     -    '
    OnKeyDown = eTelefoneEmitenteKeyDown
  end
  object stValorTotalASerPago: TStaticText
    Left = 473
    Top = 83
    Width = 160
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akRight]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 14
    Transparent = False
  end
  object stValorTotal: TStaticText
    Left = 473
    Top = 68
    Width = 160
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akRight]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Total a ser pago'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 13
    Transparent = False
  end
  object stTotalDefinido: TStaticText
    Left = 473
    Top = 100
    Width = 160
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akRight]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Total definido'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 15
    Transparent = False
  end
  object stValorTotalDefinido: TStaticText
    Left = 473
    Top = 115
    Width = 160
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akRight]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 16
    Transparent = False
  end
  object st2: TStaticText
    Left = 473
    Top = 132
    Width = 160
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akRight]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Diferen'#231'a'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 17
    Transparent = False
  end
  object stDiferenca: TStaticText
    Left = 473
    Top = 147
    Width = 160
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akRight]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = 4868863
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 18
    Transparent = False
  end
  object ePrazo: TEditLuka
    Left = 290
    Top = 52
    Width = 36
    Height = 22
    TabStop = False
    Alignment = taRightJustify
    Anchors = [akLeft, akRight]
    CharCase = ecUpperCase
    MaxLength = 2
    TabOrder = 1
    Visible = False
    OnKeyDown = ePrazoKeyDown
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eRepetir: TEditLuka
    Left = 332
    Top = 52
    Width = 47
    Height = 22
    TabStop = False
    Alignment = taRightJustify
    Anchors = [akLeft, akRight]
    CharCase = ecUpperCase
    MaxLength = 2
    TabOrder = 2
    Visible = False
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
