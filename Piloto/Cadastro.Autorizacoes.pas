{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N-,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$WARN SYMBOL_DEPRECATED ON}
{$WARN SYMBOL_LIBRARY ON}
{$WARN SYMBOL_PLATFORM ON}
{$WARN SYMBOL_EXPERIMENTAL ON}
{$WARN UNIT_LIBRARY ON}
{$WARN UNIT_PLATFORM ON}
{$WARN UNIT_DEPRECATED ON}
{$WARN UNIT_EXPERIMENTAL ON}
{$WARN HRESULT_COMPAT ON}
{$WARN HIDING_MEMBER ON}
{$WARN HIDDEN_VIRTUAL ON}
{$WARN GARBAGE ON}
{$WARN BOUNDS_ERROR ON}
{$WARN ZERO_NIL_COMPAT ON}
{$WARN STRING_CONST_TRUNCED ON}
{$WARN FOR_LOOP_VAR_VARPAR ON}
{$WARN TYPED_CONST_VARPAR ON}
{$WARN ASG_TO_TYPED_CONST ON}
{$WARN CASE_LABEL_RANGE ON}
{$WARN FOR_VARIABLE ON}
{$WARN CONSTRUCTING_ABSTRACT ON}
{$WARN COMPARISON_FALSE ON}
{$WARN COMPARISON_TRUE ON}
{$WARN COMPARING_SIGNED_UNSIGNED ON}
{$WARN COMBINING_SIGNED_UNSIGNED ON}
{$WARN UNSUPPORTED_CONSTRUCT ON}
{$WARN FILE_OPEN ON}
{$WARN FILE_OPEN_UNITSRC ON}
{$WARN BAD_GLOBAL_SYMBOL ON}
{$WARN DUPLICATE_CTOR_DTOR ON}
{$WARN INVALID_DIRECTIVE ON}
{$WARN PACKAGE_NO_LINK ON}
{$WARN PACKAGED_THREADVAR ON}
{$WARN IMPLICIT_IMPORT ON}
{$WARN HPPEMIT_IGNORED ON}
{$WARN NO_RETVAL ON}
{$WARN USE_BEFORE_DEF ON}
{$WARN FOR_LOOP_VAR_UNDEF ON}
{$WARN UNIT_NAME_MISMATCH ON}
{$WARN NO_CFG_FILE_FOUND ON}
{$WARN IMPLICIT_VARIANTS ON}
{$WARN UNICODE_TO_LOCALE ON}
{$WARN LOCALE_TO_UNICODE ON}
{$WARN IMAGEBASE_MULTIPLE ON}
{$WARN SUSPICIOUS_TYPECAST ON}
{$WARN PRIVATE_PROPACCESSOR ON}
{$WARN UNSAFE_TYPE OFF}
{$WARN UNSAFE_CODE OFF}
{$WARN UNSAFE_CAST OFF}
{$WARN OPTION_TRUNCATED ON}
{$WARN WIDECHAR_REDUCED ON}
{$WARN DUPLICATES_IGNORED ON}
{$WARN UNIT_INIT_SEQ ON}
{$WARN LOCAL_PINVOKE ON}
{$WARN MESSAGE_DIRECTIVE ON}
{$WARN TYPEINFO_IMPLICITLY_ADDED ON}
{$WARN RLINK_WARNING ON}
{$WARN IMPLICIT_STRING_CAST ON}
{$WARN IMPLICIT_STRING_CAST_LOSS ON}
{$WARN EXPLICIT_STRING_CAST OFF}
{$WARN EXPLICIT_STRING_CAST_LOSS OFF}
{$WARN CVT_WCHAR_TO_ACHAR ON}
{$WARN CVT_NARROWING_STRING_LOST ON}
{$WARN CVT_ACHAR_TO_WCHAR ON}
{$WARN CVT_WIDENING_STRING_LOST ON}
{$WARN NON_PORTABLE_TYPECAST ON}
{$WARN XML_WHITESPACE_NOT_ALLOWED ON}
{$WARN XML_UNKNOWN_ENTITY ON}
{$WARN XML_INVALID_NAME_START ON}
{$WARN XML_INVALID_NAME ON}
{$WARN XML_EXPECTED_CHARACTER ON}
{$WARN XML_CREF_NO_RESOLVE ON}
{$WARN XML_NO_PARM ON}
{$WARN XML_NO_MATCHING_PARM ON}
{$WARN IMMUTABLE_STRINGS OFF}
unit Cadastro.Autorizacoes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastro, Vcl.StdCtrls, System.Math, EditLuka, Vcl.Buttons,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, _Biblioteca, _FrameHenrancaPesquisas, FrameFuncionarios, Vcl.ComCtrls,
  Vcl.ImgList, _RecordsEspeciais, _RecordsCadastros, System.ImageList, Buscar.Funcionario,
  SpeedButtonLuka;

type
  TNo = record
    No: TTreeNode;
    Pai: string;
    Autorizado: Boolean;
    Id: string;
  end;

  TFormCadastroAutorizacoes = class(TFormHerancaCadastro)
    FrFuncionario: TFrFuncionarios;
    tvTelasSistema: TTreeView;
    stFormasPagamento: TStaticText;
    tvRotinasSistema: TTreeView;
    StaticText1: TStaticText;
    ilAutorizacoes: TImageList;
    sbCopiarAutorizacoes: TSpeedButtonLuka;
    procedure FormCreate(Sender: TObject);
    procedure tvTelasSistemaDblClick(Sender: TObject);
    procedure tvTelasSistemaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbGravarClick(Sender: TObject);
    procedure sbCopiarAutorizacoesClick(Sender: TObject);
  private
    FNosTelas: array of TNo;
    FNosRotinas: array of TNo;

    procedure AddNo(pTreeView: TTreeView; pCaminho: array of string; const pId: string = '');

    procedure PreencherTelas;
    procedure PreencherAutorizacoesTelas(pFuncionarioId: Integer);

    procedure PreencherRotinas;
    procedure PreencherAutorizacoesRotinas(pFuncionarioId: Integer);

    procedure FrFuncionarioOnAposPesquisar(Sender: TObject);
    procedure FrFuncionarioOnAposDeletar(Sender: TObject);
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses
  _Autorizacoes, _Sessao;

const
  coSelecionado    = 0;
  coNaoSelecionado = 1;

procedure TFormCadastroAutorizacoes.AddNo(pTreeView: TTreeView; pCaminho: array of string; const pId: string = '');
var
  i: Integer;
  vPai: string;
  vNo: TTreeNode;
begin
  vNo := nil;

  // Localizando n� pai
  vPai := '';
  for i := Low(pCaminho) to High(pCaminho) - 1 do
    vPai := vPai + IIfStr(i <> Low(pCaminho), '\') + pCaminho[i];

  if pTreeView = tvTelasSistema  then begin
    if vPai = '' then
      vNo := pTreeView.Items.Add(nil, pCaminho[High(pCaminho)])
    else begin
      for i := Low(FNosTelas) to High(FNosTelas) do begin
        if vPai = FNosTelas[i].Pai then begin
          vNo := FNosTelas[i].No;
          Break;
        end;
      end;

      vNo := pTreeView.Items.AddChild(vNo, pCaminho[High(pCaminho)]);
    end;
  end
  else begin
    if vPai = '' then
      vNo := pTreeView.Items.Add(nil, pCaminho[High(pCaminho)])
    else begin
      for i := Low(FNosRotinas) to High(FNosRotinas) do begin
        if vPai = FNosRotinas[i].Pai then begin
          vNo := FNosRotinas[i].No;
          Break;
        end;
      end;

      vNo := pTreeView.Items.AddChild(vNo, pCaminho[High(pCaminho)]);
    end;
  end;

  if pId = '' then begin
    vNo.ImageIndex := -1;
    vNo.SelectedIndex := -1;
  end
  else begin
    vNo.ImageIndex := coNaoSelecionado;
    vNo.SelectedIndex := coNaoSelecionado;
  end;

  if pTreeView = tvTelasSistema  then begin
    SetLength(FNosTelas, Length(FNosTelas) + 1);

    FNosTelas[High(FNosTelas)].No  := vNo;
    FNosTelas[High(FNosTelas)].Pai := vPai + IIfStr(vPai <> '', '\') + pCaminho[High(pCaminho)];;
    FNosTelas[High(FNosTelas)].Id  := pId;
  end
  else begin
    SetLength(FNosRotinas, Length(FNosRotinas) + 1);

    FNosRotinas[High(FNosRotinas)].No  := vNo;
    FNosRotinas[High(FNosRotinas)].Pai := vPai + IIfStr(vPai <> '', '\') + pCaminho[High(pCaminho)];;
    FNosRotinas[High(FNosRotinas)].Id  := pId;
  end;
end;

procedure TFormCadastroAutorizacoes.FormCreate(Sender: TObject);
begin
  FNosTelas := nil;
  FNosRotinas := nil;

  FrFuncionario.OnAposPesquisar := FrFuncionarioOnAposPesquisar;
  FrFuncionario.OnAposDeletar := FrFuncionarioOnAposDeletar;
end;

procedure TFormCadastroAutorizacoes.FrFuncionarioOnAposDeletar(Sender: TObject);
begin
  tvTelasSistema.Items.Clear;
  tvRotinasSistema.Items.Clear;

  tvTelasSistema.Enabled := False;
  tvRotinasSistema.Enabled := False;

  _Biblioteca.Habilitar([sbGravar, sbCopiarAutorizacoes], False);
  FNosTelas := nil;
  FNosRotinas := nil;
end;

procedure TFormCadastroAutorizacoes.FrFuncionarioOnAposPesquisar(Sender: TObject);
begin
  tvTelasSistema.Enabled := True;
  tvRotinasSistema.Enabled := True;

  PreencherTelas;
  PreencherAutorizacoesTelas( FrFuncionario.GetFuncionario.funcionario_id );

  PreencherRotinas;
  PreencherAutorizacoesRotinas( FrFuncionario.GetFuncionario.funcionario_id) ;

  _Biblioteca.Habilitar([sbGravar, sbCopiarAutorizacoes], True);
end;

procedure TFormCadastroAutorizacoes.PreencherAutorizacoesRotinas(pFuncionarioId: Integer);
var
  i: Integer;
  j: Integer;
  vAutorizacoes: TArray<RecAutorizacoes>;
begin
  vAutorizacoes := _Autorizacoes.BuscarAutorizacoes(Sessao.getConexaoBanco, 0, [pFuncionarioId, 'R']);
  for i := Low(vAutorizacoes) to High(vAutorizacoes) do begin
    for j := Low(FNosRotinas) to High(FNosRotinas) do begin
      if vAutorizacoes[i].id = FNosRotinas[j].Id then begin
        FNosRotinas[j].Autorizado := True;

        FNosRotinas[j].No.ImageIndex    := coSelecionado;
        FNosRotinas[j].No.SelectedIndex := coSelecionado;
      end;
    end;
  end;
end;

procedure TFormCadastroAutorizacoes.PreencherAutorizacoesTelas(pFuncionarioId: Integer);
var
  i: Integer;
  j: Integer;
  vAutorizacoes: TArray<RecAutorizacoes>;
begin
  vAutorizacoes := _Autorizacoes.BuscarAutorizacoes(Sessao.getConexaoBanco, 0, [pFuncionarioId, 'T']);
  for i := Low(vAutorizacoes) to High(vAutorizacoes) do begin
    for j := Low(FNosTelas) to High(FNosTelas) do begin
      if vAutorizacoes[i].id = FNosTelas[j].Id then begin
        FNosTelas[j].Autorizado := True;

        FNosTelas[j].No.ImageIndex    := coSelecionado;
        FNosTelas[j].No.SelectedIndex := coSelecionado;
      end;
    end;
  end;
end;

procedure TFormCadastroAutorizacoes.PreencherRotinas;

  procedure AddCadastros;
  begin
    AddNo(tvRotinasSistema, ['Cadastros']);
    AddNo(tvRotinasSistema, ['Cadastros', 'Clientes']);
    AddNo(tvRotinasSistema, ['Cadastros', 'Clientes', 'Clientes']);
    AddNo(tvRotinasSistema, ['Cadastros', 'Clientes', 'Clientes', 'Par�metros adicionais do cliente'], 'parametros_adicionais_cliente');
    AddNo(tvRotinasSistema, ['Cadastros', 'Clientes', 'Clientes', 'Ativar/Desativar cliente'], 'ativar_desativar_cliente');
  end;

  procedure AddFiscal;
  begin
    AddNo(tvRotinasSistema, ['Notas Fiscais']);
    AddNo(tvRotinasSistema, ['Notas Fiscais', 'Relat�rios']);
    AddNo(tvRotinasSistema, ['Notas Fiscais', 'Relat�rios', 'Aplicar Indice de Dedu��o'], 'aplicar_indice_deducao');
    AddNo(tvRotinasSistema, ['Notas Fiscais', 'Relat�rios', 'Selecionar v�rias notas para emiss�o'], 'selecionar_varias_notas_emissao');
  end;

  procedure AddExpedicao;
  begin
    AddNo(tvRotinasSistema, ['Log�stica']);
    AddNo(tvRotinasSistema, ['Log�stica', 'Reimprimir comprovante de entrega'], 'reimprimir_comprovante_entrega');
  end;

  procedure AddEstoque;
  begin
    AddNo(tvRotinasSistema, ['Estoque']);
    AddNo(tvRotinasSistema, ['Estoque', 'Corre��es de estoque']);
    AddNo(tvRotinasSistema, ['Estoque', 'Corre��es de estoque', 'Zerar estoque de produtos'], 'zerar_estoque_produtos');
  end;

  procedure AddComprasEntradas;
  begin

  end;


  procedure AddVendas;
  begin
    AddNo(tvRotinasSistema, ['Vendas/Devolu��es']);
    AddNo(tvRotinasSistema, ['Vendas/Devolu��es', 'Vendas/Devolu��es']);
    AddNo(tvRotinasSistema, ['Vendas/Devolu��es', 'Vendas/Devolu��es', 'Visualizar lucratividade'], 'visualizar_negociacao');
    AddNo(tvRotinasSistema, ['Vendas/Devolu��es', 'Vendas/Devolu��es', 'Alterar pre�o manualmente'], 'alterar_preco_manualmente');
    AddNo(tvRotinasSistema, ['Vendas/Devolu��es', 'Vendas/Devolu��es', 'Alterar indice na venda'], 'alterar_indice_na_venda');
  end;

  procedure AddCaixaBancos;
  begin
    AddNo(tvRotinasSistema, ['Caixas / Bancos']);
    AddNo(tvRotinasSistema, ['Caixas / Bancos', 'Rotinas do caixa']);
    AddNo(tvRotinasSistema, ['Caixas / Bancos', 'Rotinas do caixa', 'Vendas']);
    AddNo(tvRotinasSistema, ['Caixas / Bancos', 'Rotinas do caixa', 'Vendas', 'Receber pedidos']);
    AddNo(tvRotinasSistema, ['Caixas / Bancos', 'Rotinas do caixa', 'Vendas', 'Receber pedidos', 'Cancelar fec. de pedido a receber na entrega'], 'cancelar_fec_pedido_recimento_entrega');
    AddNo(tvRotinasSistema, ['Caixas / Bancos', 'Rotinas do caixa', 'Vendas', 'Receber pedidos', 'Voltar pedido para orcamento'], 'voltar_pedido_para_orcamento');

    AddNo(tvRotinasSistema, ['Caixas / Bancos', 'Rotinas do caixa', 'Fechamento']);
    AddNo(tvRotinasSistema, ['Caixas / Bancos', 'Rotinas do caixa', 'Fechamento', 'Fechar turno com diferen�a'], 'fechar_turno_com_diferenca');
    AddNo(tvRotinasSistema, ['Caixas / Bancos', 'Rotinas do caixa', 'Fechamento', 'N�o obrigar verifica��o dos documentos(cheque, cart�o, cobran�a)'], 'nao_obrigar_verificar_documentos_financeiro');

  end;

  procedure AddFinanceiro;
  begin
    AddNo(tvRotinasSistema, ['Financeiro']);
    AddNo(tvRotinasSistema, ['Financeiro', 'Contas a receber']);
    AddNo(tvRotinasSistema, ['Financeiro', 'Contas a receber', 'Cr�ditos a receber']);
    AddNo(tvRotinasSistema, ['Financeiro', 'Contas a receber', 'Cr�ditos a receber', 'Cadastrar cr�dito de importa��o'], 'cadastrar_credito_importacao_receber');
    AddNo(tvRotinasSistema, ['Financeiro', 'Contas a receber', 'Boletos']);
    AddNo(tvRotinasSistema, ['Financeiro', 'Contas a receber', 'Boletos','Emitir boletos']);
    AddNo(tvRotinasSistema, ['Financeiro', 'Contas a receber', 'Boletos','Emitir boletos','Gerar novo nosso n�mero'],'gerar_novo_nosso_numero');

    AddNo(tvRotinasSistema, ['Financeiro', 'Contas a pagar']);
    AddNo(tvRotinasSistema, ['Financeiro', 'Contas a pagar', 'Cr�ditos a pagar']);
    AddNo(tvRotinasSistema, ['Financeiro', 'Contas a pagar', 'Cr�ditos a pagar', 'Cadastrar cr�dito de importa��o'], 'cadastrar_credito_importacao_pagar');

   end;


  procedure AddDesenvolvimento;
  begin

  end;

  procedure AddRelatorioGerais;
  begin
    AddNo(tvRotinasSistema, ['Relat�rios Gerais']);
    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Vendas/Devolu��es']);
    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Vendas/Devolu��es', 'Rela��o de or�amentos e vendas']);
    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Vendas/Devolu��es', 'Rela��o de or�amentos e vendas', 'Reimprimir comprovante de pagamento'], 'reimprimir_comprovante_pagamento');
    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Vendas/Devolu��es', 'Rela��o de or�amentos e vendas', 'Permitir visualizar vendas de outtros vendedores'], 'visualizar_vendas_outras_vendedores');
    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Vendas/Devolu��es', 'Rela��o de or�amentos e vendas', 'Permitir visualizar vendas fora do dia atual'], 'visualizar_vendas_fora_dia_atual');
    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Vendas/Devolu��es', 'Rela��o de or�amentos e vendas', 'Permitir cancelar o fechamento do pedido'], 'cancelar_fechamento_pedido');
    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Vendas/Devolu��es', 'Rela��o de or�amentos e vendas', 'Visualizar lucro da venda'], 'visualizar_lucro_venda');
    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Vendas/Devolu��es', 'Rela��o de or�amentos e vendas', 'Definir �ndice de desconto de venda'], 'definir_indice_desconto_venda');
    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Vendas/Devolu��es', 'Rela��o de devolu��es']);
    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Vendas/Devolu��es', 'Rela��o de devolu��es', 'Reimprimir comprovante de devolu��o'], 'reimprimir_comprovante_devolucao');
    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Vendas/Devolu��es', 'Rela��o de acumulados']);
    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Vendas/Devolu��es', 'Rela��o de acumulados', 'Definir �ndice de desconto de acumulado'], 'definir_indice_desconto_acumulado');

    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Estoque', 'Corre��es de estoque', 'Emitir nota de ajuste de estoque'], 'emitir_nota_ajuste_estoque');

    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Caixa/Banco']);
    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Caixa/Banco', 'Movimenta��es de contas']);
    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Caixa/Banco', 'Movimenta��es de contas', 'Conciliar movimento'], 'conciliar_movimento_conta');
    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Caixa/Banco', 'Movimenta��es de contas', 'Desconciliar movimento'], 'desconciliar_movimento_conta');

    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Financeiro']);
    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Financeiro', 'Rela��o de contas a receber']);
    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Financeiro', 'Rela��o de contas a receber', 'Alterar informa��es de conta a receber'], 'alterar_informacoes_contas_receber');

    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Financeiro', 'Rela��o de contas a pagar']);
    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Financeiro', 'Rela��o de contas a pagar', 'Permitir altera��o do campo "Documento"'], 'alterar_documento_conta_pagar');
    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Financeiro', 'Rela��o de contas a pagar', 'Permitir altera��o da data de vencimento'], 'permitir_alterar_data_vencimento_pagar');
    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Financeiro', 'Rela��o de contas a pagar', 'Permitir altera��o do valor do documento'], 'permitir_alterar_valor_documento_pagar');
    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Financeiro', 'Rela��o de contas a pagar', 'Permitir bloquear/desbloquear cr�ditos'], 'permitir_bloquear_desbloquear_credito_pagar');
    AddNo(tvRotinasSistema, ['Relat�rios Gerais', 'Financeiro', 'Rela��o de contas a pagar', 'Alterar fornecedor'], 'permitir_alterar_fornecedor');

  end;


  procedure AddAjuda;
  begin
    AddNo(tvRotinasSistema, ['Ajuda']);
    AddNo(tvRotinasSistema, ['Ajuda', 'Baixar nova compila��o'], 'baixar_nova_compilcao');
  end;

begin
  AddCadastros;
  AddVendas;
  AddComprasEntradas;
  AddEstoque;
  AddFiscal;
  AddExpedicao;
  AddCaixaBancos;
  AddFinanceiro;
  AddRelatorioGerais;
  AddDesenvolvimento;
  AddAjuda;
end;

procedure TFormCadastroAutorizacoes.PreencherTelas;

  procedure AddCadastros;
  begin
    AddNo(tvTelasSistema, ['Cadastros']);
    AddNo(tvTelasSistema, ['Cadastros', 'Clientes']);
    AddNo(tvTelasSistema, ['Cadastros', 'Clientes', 'Lista negra'], 'tformcadastrolistanegra');
    AddNo(tvTelasSistema, ['Cadastros', 'Clientes', 'Grupos de clientes'], 'tformcadastrogruposclientes');
    AddNo(tvTelasSistema, ['Cadastros', 'Clientes', 'Clientes'], 'tformcadastroclientes');

    AddNo(tvTelasSistema, ['Cadastros', 'Fornecedores']);
    AddNo(tvTelasSistema, ['Cadastros', 'Fornecedores', 'Fornecedores'], 'tformcadastrofornecedores');
    AddNo(tvTelasSistema, ['Cadastros', 'Fornecedores', 'Grupo de fornecedores'], 'tformcadastrogruposfornecedores');

    AddNo(tvTelasSistema, ['Cadastros', 'Parceiros']);
    AddNo(tvTelasSistema, ['Cadastros', 'Parceiros', 'Parceiros'], 'tformcadastroprofissionais');
    AddNo(tvTelasSistema, ['Cadastros', 'Parceiros', 'Grupo de parceiros'], 'tformcadastrocargosprofissionais');

    AddNo(tvTelasSistema, ['Cadastros', 'Motoristas da loja']);
    AddNo(tvTelasSistema, ['Cadastros', 'Motoristas da loja', 'Motoristas'], 'tformcadastromotorista');
    AddNo(tvTelasSistema, ['Cadastros', 'Motoristas da loja', 'Ve�culos'], 'tformcadastroveiculos');

    AddNo(tvTelasSistema, ['Cadastros', 'Transportadoras'], 'tformcadastrotransportadoras');

    AddNo(tvTelasSistema, ['Cadastros', 'Empresas'], 'tformcadastroempresas');

    AddNo(tvTelasSistema, ['Cadastros', 'Endere�os']);
    AddNo(tvTelasSistema, ['Cadastros', 'Endere�os', 'Bairros'], 'tformcadastrobairros');
    AddNo(tvTelasSistema, ['Cadastros', 'Endere�os', 'Cidades'], 'tformcadastrocidades');
    AddNo(tvTelasSistema, ['Cadastros', 'Endere�os', 'Estados'], 'tformcadastroestados');
    AddNo(tvTelasSistema, ['Cadastros', 'Endere�os', 'Rotas'], 'tformcadastrorotas');

    AddNo(tvTelasSistema, ['Cadastros', 'Produtos']);
    AddNo(tvTelasSistema, ['Cadastros', 'Produtos', 'CFOP'], 'tformcadastrocfop');
    AddNo(tvTelasSistema, ['Cadastros', 'Produtos', 'Marcas'], 'tformcadastromarcas');

    AddNo(tvTelasSistema, ['Cadastros', 'Produtos', 'Locais']);
    AddNo(tvTelasSistema, ['Cadastros', 'Produtos', 'Locais de produtos'], 'tformcadastrolocaisprodutos');
    AddNo(tvTelasSistema, ['Cadastros', 'Produtos', 'Ordena��o de locais para entrega por produto'], 'tformordenacaolocaisentregaproduto');

    AddNo(tvTelasSistema, ['Cadastros', 'Produtos', 'Endrere�o de estoque']);
    AddNo(tvTelasSistema, ['Cadastros', 'Produtos', 'Endere�o estoque'], 'tformenderecoestoque');
    AddNo(tvTelasSistema, ['Cadastros', 'Produtos', 'Endere�o estoque - Rua'], 'tformenderecoestoquerua');
    AddNo(tvTelasSistema, ['Cadastros', 'Produtos', 'Endere�o estoque - M�dulo'], 'tformenderecoestoquemodulo');
    AddNo(tvTelasSistema, ['Cadastros', 'Produtos', 'Endere�o estoque - N�vel'], 'tformenderecoestoquenivel');
    AddNo(tvTelasSistema, ['Cadastros', 'Produtos', 'Endere�o estoque - V�o'], 'tformenderecoestoquevao');

    AddNo(tvTelasSistema, ['Cadastros', 'Produtos', 'Produtos'], 'tformcadastroprodutos');
    AddNo(tvTelasSistema, ['Cadastros', 'Produtos', 'Forma��o de custos e pre�os'], 'tformformacaocustopreco');
    AddNo(tvTelasSistema, ['Cadastros', 'Produtos', 'Promo��es'], 'tformdefinirpromocoes');
    AddNo(tvTelasSistema, ['Cadastros', 'Produtos', 'Pre�os'], 'tformdefinirprecosvenda');
    AddNo(tvTelasSistema, ['Cadastros', 'Produtos', 'Produto pai/filho'], 'tformcadastroprodutopai');
    AddNo(tvTelasSistema, ['Cadastros', 'Produtos', 'Produtos relacionados'], 'tformcadastroprodutosrelacionados');
    AddNo(tvTelasSistema, ['Cadastros', 'Produtos', 'Unidades'], 'tformcadastrounidades');
    AddNo(tvTelasSistema, ['Cadastros', 'Produtos', 'Motivos de ajuste de estoque'], 'tformcadastromotivosajusteestoque');
    AddNo(tvTelasSistema, ['Cadastros', 'Produtos', 'Importa��es de dados']);
    AddNo(tvTelasSistema, ['Cadastros', 'Produtos', 'Importa��es de dados', 'Impostos IBPT'], 'tformimportacaoimpostosibpt');
    AddNo(tvTelasSistema, ['Cadastros', 'Produtos', 'Grupos de produtos'], 'tformcadastrodepartamentossecoeslinhas');


    AddNo(tvTelasSistema, ['Cadastros', 'Financeiro']);
    AddNo(tvTelasSistema, ['Cadastros', 'Financeiro', 'Administradoras de cart�es'], 'tformcadastroadministradorascartoes');
    AddNo(tvTelasSistema, ['Cadastros', 'Financeiro', 'Configura��es de contas para boletos'], 'tfrmcadastrocontasboletos');
    AddNo(tvTelasSistema, ['Cadastros', 'Financeiro', 'Contas'], 'tformcadastrocontas');
    AddNo(tvTelasSistema, ['Cadastros', 'Financeiro', 'Contas cust�dia'], 'tformcadastrocontascustodia');
    AddNo(tvTelasSistema, ['Cadastros', 'Financeiro', 'Portadores'], 'tformcadastroportadores');
    AddNo(tvTelasSistema, ['Cadastros', 'Financeiro', 'Tipos de cobran�a'], 'tformcadastrotiposcobranca');
    AddNo(tvTelasSistema, ['Cadastros', 'Financeiro', 'Condi��es de pagamento'], 'tformcadastrocondicoespagamento');
    AddNo(tvTelasSistema, ['Cadastros', 'Financeiro', 'Gerenciadores de cart�es TEF'], 'tformcadastrogerenciadorescartoestef');
    AddNo(tvTelasSistema, ['Cadastros', 'Financeiro', 'Centro de custos'], 'tformcadastrocentrocustos');
    AddNo(tvTelasSistema, ['Cadastros', 'Financeiro', 'Condi��es de pagamento cart�es PDV'], 'tformcondicoespagamentocartaocreditopdv');
    AddNo(tvTelasSistema, ['Cadastros', 'Financeiro', 'Planos financeiros'], 'tformcadastroplanosfinanceiros');
    AddNo(tvTelasSistema, ['Cadastros', 'Financeiro', 'Grupos de planos financeiros'], 'tformgruposplanosfinanceiros');

    AddNo(tvTelasSistema, ['Cadastros', 'Funcion�rios']);
    AddNo(tvTelasSistema, ['Cadastros', 'Funcion�rios', 'Autoriza��es'], 'tformcadastroautorizacoes');
    AddNo(tvTelasSistema, ['Cadastros', 'Funcion�rios', 'Cargos'], 'tformcadastrofuncoesfuncionarios');
    AddNo(tvTelasSistema, ['Cadastros', 'Funcion�rios', 'Funcion�rios'], 'tformcadastrofuncionarios');




    AddNo(tvTelasSistema, ['Cadastros', 'Par�metros do sistema']);
    AddNo(tvTelasSistema, ['Cadastros', 'Par�metros do sistema', 'Par�metros'], 'tformparametros');
    AddNo(tvTelasSistema, ['Cadastros', 'Par�metros do sistema', 'Par�metros por empresa'], 'tformparametrosempresa');
    AddNo(tvTelasSistema, ['Cadastros', 'Par�metros do sistema', 'Grupos de estoque'], 'tformgruposestoques');
    AddNo(tvTelasSistema, ['Cadastros', 'Par�metros do sistema', 'Percentuais de custos de empresas'], 'tformpercentuaiscustoempresa');
    AddNo(tvTelasSistema, ['Cadastros', 'Par�metros do sistema', 'Defini��o de CFOP de opera��es'], 'tformdefinircfopoperacoes');
    AddNo(tvTelasSistema, ['Cadastros', 'Par�metros do sistema', 'S�ries de notas fiscais'], 'tformcadastrosseriesnotasfiscais');
    AddNo(tvTelasSistema, ['Cadastros', 'Par�metros do sistema', 'Par�metros de dias de devolu��o'], 'tformparametrodevolucaoaposprazo');

    AddNo(tvTelasSistema, ['Cadastros', 'Par�metros do sistema', 'Impressoras']);
    AddNo(tvTelasSistema, ['Cadastros', 'Par�metros do sistema', 'Impressoras', 'Esta��o'], 'tformimpressorasestacao');
    AddNo(tvTelasSistema, ['Cadastros', 'Par�metros do sistema', 'Impressoras', 'Usu�rios'], 'tformimpressorasusuario');
    AddNo(tvTelasSistema, ['Cadastros', 'Par�metros do sistema', 'Esta��o'], 'tformparametrosestacao');
    AddNo(tvTelasSistema, ['Cadastros', 'Par�metros do sistema', 'Indices de descontos de venda'], 'tformcadastroindicesdescontosvenda');
    AddNo(tvTelasSistema, ['Cadastros', 'Par�metros do sistema', 'Tipo de acompanhamento or�amento'], 'tformcadastrotipoacompanhamentoorcamento');
  end;

  procedure AddVendas;
  begin
    AddNo(tvTelasSistema, ['Vendas/Devolu��es']);

    AddNo(tvTelasSistema, ['Vendas/Devolu��es', 'Or�amentos / Vendas'], 'tformorcamentosvendas');
    AddNo(tvTelasSistema, ['Vendas/Devolu��es', 'Devolu��o de produtos'], 'tformdevolucaoprodutos');

    AddNo(tvTelasSistema, ['Vendas/Devolu��es', 'Acumulados']);
    AddNo(tvTelasSistema, ['Vendas/Devolu��es', 'Acumulados', 'Adiantamento de acumulado'], 'tformadiantamentoacumulado');
    AddNo(tvTelasSistema, ['Vendas/Devolu��es', 'Acumulados', 'Fechar acumulado'], 'tformfechamentoacumulados');
    AddNo(tvTelasSistema, ['Vendas/Devolu��es', 'Liberar vendas/orc. bloqueados'], 'tformrelacaobloqueiosorcamentos');
    AddNo(tvTelasSistema, ['Vendas/Devolu��es', 'Consulta de pedidos'], 'tformconsultapedidos');

  end;

  procedure AddComprasEntradas;
  begin
    AddNo(tvTelasSistema, ['Compras Entradas']);
    AddNo(tvTelasSistema, ['Compras Entradas', 'Compras']);
    AddNo(tvTelasSistema, ['Compras Entradas', 'Compras', 'Sugest�o de compras'], 'tformsugestaocompras');
    AddNo(tvTelasSistema, ['Compras Entradas', 'Compras', 'Compras'], 'tformcadastrocompras');

    AddNo(tvTelasSistema, ['Compras Entradas', 'Entradas']);
    AddNo(tvTelasSistema, ['Compras Entradas', 'Entradas', 'Entradas-Futuras'], 'tformpreentradasdocumentosfiscais');
    AddNo(tvTelasSistema, ['Compras Entradas', 'Entradas', 'Conhecimentos de frete'], 'tformconhecimentosfretes');
    AddNo(tvTelasSistema, ['Compras Entradas', 'Entradas', 'Entradas de NF'], 'tformentradanotasfiscais');
    AddNo(tvTelasSistema, ['Compras Entradas', 'Entradas', 'Devolu��o de Entrada'], 'TFormDevolucaoEntradaNotaFiscal');
  end;

   procedure AddEstoque;
  begin
    AddNo(tvTelasSistema, ['Estoque']);
    AddNo(tvTelasSistema, ['Estoque', 'Ajuste de Estoque'], 'tformajusteestoque');
    AddNo(tvTelasSistema, ['Estoque', 'Bloqueio de estoque'], 'tformbloqueioestoque');
    AddNo(tvTelasSistema, ['Estoque', 'An�lise de produtos que controlam lotes/piso'], 'tformanaliseprodutolotes');
    AddNo(tvTelasSistema, ['Estoque', 'Transfer�ncia de produtos entre empresas'], 'tformtransferenciasprodutosentreempresas');
    AddNo(tvTelasSistema, ['Estoque', 'Transfer�ncia de locais'], 'tformtransferencialocais');
    AddNo(tvTelasSistema, ['Estoque', 'Confirmar transfer�ncia de locais'], 'tformconfirmartransferenciasdelocais');
    AddNo(tvTelasSistema, ['Estoque', 'Confirma��o de devolu��o'], 'tformconfirmardevolucoes');
    AddNo(tvTelasSistema, ['Estoque', 'Etiquetas'], 'tformetiquetas');
  end;

  procedure AddFiscal;
  begin
    AddNo(tvTelasSistema, ['Documentos Fiscais']);

    AddNo(tvTelasSistema, ['Documentos Fiscais', 'Movimentos pendentes de emiss�o de nota fiscal de transfer�ncia'], 'tformmovimentospendentesemissaonotafiscaltransferencia');
    AddNo(tvTelasSistema, ['Documentos Fiscais', 'Nota Manual'], 'tformoutrasnotas');
  end;

  procedure AddExpedicao;
  begin
    AddNo(tvTelasSistema, ['Log�stica']);

    AddNo(tvTelasSistema, ['Log�stica', 'Retiradas de produtos']);
    AddNo(tvTelasSistema, ['Log�stica', 'Retiradas de produtos', 'Gerar retiradas'], 'tformgerarretiradas');
    AddNo(tvTelasSistema, ['Log�stica', 'Retiradas de produtos', 'Confirmar retiradas'], 'tformconfirmarretiradas');

    AddNo(tvTelasSistema, ['Log�stica', 'Entregas']);
    AddNo(tvTelasSistema, ['Log�stica', 'Entregas', 'Gerar entregas'], 'tformgerarentregas');
    AddNo(tvTelasSistema, ['Log�stica', 'Entregas', 'Controle de entregas'], 'tformcontroleentregas');
    AddNo(tvTelasSistema, ['Log�stica', 'Entregas', 'Agendar itens sem previs�o'], 'tformagendaritenssemprevisao');
  end;

  procedure AddCaixaBancos;
  begin

    AddNo(tvTelasSistema, ['Caixa/Banco']);

    AddNo(tvTelasSistema, ['Caixa/Banco', 'Rotinas do caixa']);
    AddNo(tvTelasSistema, ['Caixa/Banco', 'Rotinas do caixa', 'Abertura'], 'tformaberturacaixa');

    AddNo(tvTelasSistema, ['Caixa/Banco', 'Rotinas do caixa', 'Vendas']);
    AddNo(tvTelasSistema, ['Caixa/Banco', 'Rotinas do caixa', 'Vendas', 'Receber pedidos'], 'tformrecebimentopedidos');
    AddNo(tvTelasSistema, ['Caixa/Banco', 'Rotinas do caixa', 'Vendas', 'Cancelar recebimento de pedidos'], 'tformcancelarrecebimentopedido');
    AddNo(tvTelasSistema, ['Caixa/Banco', 'Rotinas do caixa', 'Vendas', 'Cancelar recebimento de acumulados'], 'tformcancelarrecebimentoacumulado');
    AddNo(tvTelasSistema, ['Caixa/Banco', 'Rotinas do caixa', 'Vendas', 'PDV'], 'tformpdv');

    AddNo(tvTelasSistema, ['Caixa/Banco', 'Rotinas do caixa', 'Suprimento'], 'tformsuprimentocaixa');
    AddNo(tvTelasSistema, ['Caixa/Banco', 'Rotinas do caixa', 'Sangria'], 'tformsangriacaixa');
    AddNo(tvTelasSistema, ['Caixa/Banco', 'Rotinas do caixa', 'Fechamento'], 'tformfechamentocaixa');
    AddNo(tvTelasSistema, ['Caixa/Banco', 'Rotinas do caixa', 'Outras opera��es'], 'tformoutrasoperacoes');

  end;

  procedure AddFinanceiro;
  begin
    AddNo(tvTelasSistema, ['Financeiro']);

    AddNo(tvTelasSistema, ['Financeiro', 'Contas a receber']);
    AddNo(tvTelasSistema, ['Financeiro', 'Contas a receber', 'Inclus�o de t�tulos'], 'tforminclusaotitulosreceber');
    AddNo(tvTelasSistema, ['Financeiro', 'Contas a receber', 'Cr�ditos a receber'], 'tformcadastrocreditomanualreceber');

    AddNo(tvTelasSistema, ['Financeiro', 'Contas a receber', 'Boletos']);
    AddNo(tvTelasSistema, ['Financeiro', 'Contas a receber', 'Boletos', 'Gerar arquivo de remesssa de boletos'], 'tformgerararquivoremessaboleto');
    AddNo(tvTelasSistema, ['Financeiro', 'Contas a receber', 'Boletos', 'Retorno de remessa de boletos'], 'tformretornoremessaboletos');
    AddNo(tvTelasSistema, ['Financeiro', 'Contas a receber', 'Boletos', 'Emitir boletos'], 'tformrelacaoboletos');

    AddNo(tvTelasSistema, ['Financeiro', 'Contas a receber', 'Cart�es']);
    AddNo(tvTelasSistema, ['Financeiro', 'Contas a receber', 'Cart�es', 'Vincular cart�es TEF'], 'tformvinculacaocartaoesreceber');
    AddNo(tvTelasSistema, ['Financeiro', 'Contas a receber', 'Cart�es', 'Baixa com arquivo CIELO'], 'tformbaixaautomaticacartaocielo');

    AddNo(tvTelasSistema, ['Financeiro', 'Contas a pagar']);
    AddNo(tvTelasSistema, ['Financeiro', 'Contas a pagar', 'Inclus�o de t�tulos'], 'tforminclusaotitulospagar');
    AddNo(tvTelasSistema, ['Financeiro', 'Contas a pagar', 'Cr�ditos a pagar'], 'tformcadastrocreditosmanuaispagar');


  end;

  procedure AddRelatorioGerais;
  begin
    AddNo(tvTelasSistema, ['Relat�rios Gerais']);

    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Dashboard'], 'tformrelacaodashboard');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Cadastros']);
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Cadastros', 'Cadastros'], '');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Cadastros', 'Produtos'], 'tformrelacaoprodutos');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Cadastros', 'Produtos pai/filho'], 'tformrelacaoprodutospaifilho');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Cadastros', 'Planos financeiros'], 'tformrelacaoplanosfinanceiros');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Cadastros', 'Limite de cr�dito cliente'], 'tformrelacaolimitecreditoclientes');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Cadastros', 'Vida do cliente'], 'tformvidacliente');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Cadastros', 'Clientes que n�o compram a X dias'], 'tformrelacaoclientesemcomprar');

    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Vendas/Devolu��es']);
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Vendas/Devolu��es', 'Rela��o de or�amentos e vendas'], 'tformrelacaoorcamentosvendas');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Vendas/Devolu��es', 'Rela��o de vendas por produtos'], 'tformrelacaovendasprodutos');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Vendas/Devolu��es', 'Rela��o de acumulados'], 'tformrelacaoacumulados');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Vendas/Devolu��es', 'Rela��o de devolu��es'], 'tformrelacaodevolucoesvendas');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Vendas/Devolu��es', 'Acompanhamento de or�amentos'], 'tformrelacaoorcamentos');

    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Vendas/Devolu��es', 'Comiss�es']);
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Vendas/Devolu��es', 'Comiss�es', 'Comiss�es por funcion�ros'], 'tformrelacaocomissoesporfuncionario');

    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Compras/Entradas']);
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Compras/Entradas', 'Compras'], 'tformrelacaocompras');

    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Compras/Entradas', 'Entradas'], '');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Compras/Entradas', 'Entradas', 'Conhecimento de fretes'], 'tformrelacaoconhecimentosfretes');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Compras/Entradas', 'Entradas', 'Entradas'], 'tformrelacaoentradanotasfiscais');


    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Estoque']);
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Estoque', 'Cadastro de contagem'], 'TFormCadastroContagemEstoque');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Estoque', 'Lista de contagem simplificada'], 'TFormListaContagem');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Estoque', 'Lista de contagem avan�ada'], 'TFormListaContagemAvancada');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Estoque', 'Lan�amento de contagem'], 'TFormLancamentoContagem');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Estoque', 'Acompanhamento de contagem'], 'TFormAcompanhamentoContagem');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Estoque', 'Corre��o de estoque'], 'tformrelacaoajusteestoque');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Estoque', 'Bloqueio de estoque'], 'tformrelacaobloqueiosestoques');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Estoque', 'Valor de estoque'], 'tformrelacaoestoque');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Estoque', 'Hist�rico do produto'], 'tformvidaproduto');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Estoque', 'Rela��o de marketplace'], 'TFormRelacaoMarketPlace');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Estoque', 'Ajustar finaliza��o mensal'], 'tformajustefinalizacaomensal');

    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Notas Fiscais']);
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Notas Fiscais', 'Notas fiscais'], 'tformrelacaonotasfiscais');

    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'L�g�stica']);
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'L�g�stica', 'Entregas pendentes'], 'tformrelacaoentregaspendentes');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'L�g�stica', 'Entregas realizadas'], 'tformrelacaoretiradasentregas');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'L�g�stica', 'Manifesto de transporte'], 'tformrelacaocontrolesentregas');

    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Caixa/Banco']);
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Caixa/Banco', 'Movimenta��o de bancos/caixas'], 'tformrelacaomovimentacoesbancoscaixas');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Caixa/Banco', 'Turnos de caixa'], 'tformrelacaoturnoscaixa');

    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Financeiros']);
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Financeiros',  'Rela��o de contas a receber'], 'tformrelacaocontasreceber');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Financeiros',  'Rela��o de contas a receber baixas'], 'tformrelacaocontasreceberbaixas');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Financeiros',  'Rela��o de contas a pagar'], 'tformrelacaocontaspagar');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Financeiros',  'Rela��o de contas a pagar baixas'], 'tformrelacaocontaspagarbaixas');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Financeiros',  'Movimenta��es'], 'tformmovimentosfinanceiros');
    AddNo(tvTelasSistema, ['Relat�rios Gerais', 'Financeiros',  'DRE Cont�bil'], 'tfrmdrecontabil');
  end;


  procedure AddDesenvolvimento;
  begin
    AddNo(tvTelasSistema, ['Desenvolvimento']);
    AddNo(tvTelasSistema, ['Desenvolvimento', 'Cadastrar novo desenvolvimento'], 'tformcadastrodesenvolvimento');
    AddNo(tvTelasSistema, ['Desenvolvimento', 'Acompanhamento de desenvolvimento'], 'tformacompanhamentodesenvolvimentos');
    AddNo(tvTelasSistema, ['Desenvolvimento', 'Central de relacionamento do cliente'], 'tformcentralrelacionamentocliente');
    AddNo(tvTelasSistema, ['Desenvolvimento', 'Rela��o de ocorr�ncias'], 'tformrelacaocrmocorrencias');
  end;

begin
  AddCadastros;
  AddVendas;
  AddComprasEntradas;
  AddEstoque;
  AddFiscal;
  AddExpedicao;
  AddCaixaBancos;
  AddFinanceiro;
  AddRelatorioGerais;
  AddDesenvolvimento;
end;

procedure TFormCadastroAutorizacoes.sbCopiarAutorizacoesClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar<Integer>;
begin
  inherited;

  vRetTela := Buscar.Funcionario.Buscar(0, -1);
  if vRetTela.BuscaCancelada then
    Exit;

  PreencherAutorizacoesTelas( vRetTela.Dados );
  PreencherAutorizacoesRotinas( vRetTela.Dados ) ;
end;

procedure TFormCadastroAutorizacoes.sbGravarClick(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  vAutorizacoesTelas: array of string;
  vAutorizacoesRotinas: array of string;
begin
  vAutorizacoesTelas := nil;
  for i := Low(FNosTelas) to High(FNosTelas) do begin
    if (not FNosTelas[i].Autorizado) or (FNosTelas[i].Id = '') then
      Continue;

    SetLength(vAutorizacoesTelas, Length(vAutorizacoesTelas) + 1);
    vAutorizacoesTelas[High(vAutorizacoesTelas)] := FNosTelas[i].Id;
  end;

  vRetBanco :=
    _Autorizacoes.AtualizarAutorizacoes(
      Sessao.getConexaoBanco,
      FrFuncionario.GetFuncionario().funcionario_id,
      'T',
      vAutorizacoesTelas
    );

  if vRetBanco.TeveErro then begin
    Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  vAutorizacoesRotinas := nil;
  for i := Low(FNosRotinas) to High(FNosRotinas) do begin
    if (not FNosRotinas[i].Autorizado) or (FNosRotinas[i].Id = '') then
      Continue;

    SetLength(vAutorizacoesRotinas, Length(vAutorizacoesRotinas) + 1);
    vAutorizacoesRotinas[High(vAutorizacoesRotinas)] := FNosRotinas[i].Id;
  end;

  vRetBanco :=
    _Autorizacoes.AtualizarAutorizacoes(
      Sessao.getConexaoBanco,
      FrFuncionario.GetFuncionario().funcionario_id,
      'R',
      vAutorizacoesRotinas
    );

  if vRetBanco.TeveErro then begin
    Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  _Biblioteca.Informar(coAlteracaoRegistroSucesso);

  FrFuncionario.Clear;
  FrFuncionario.SetFocus;
end;

procedure TFormCadastroAutorizacoes.tvTelasSistemaDblClick(Sender: TObject);
begin
  inherited;

  if TTreeView(Sender).Selected.HasChildren then
    Exit;

  TTreeView(Sender).Selected.ImageIndex := IfThen(TTreeView(Sender).Selected.ImageIndex = coNaoSelecionado, coSelecionado, coNaoSelecionado);
  TTreeView(Sender).Selected.SelectedIndex := TTreeView(Sender).Selected.ImageIndex;

  if TTreeView(Sender) = tvTelasSistema then
    FNosTelas[TTreeView(Sender).Selected.AbsoluteIndex].Autorizado := ( TTreeView(Sender).Selected.ImageIndex = coSelecionado )
  else
    FNosRotinas[TTreeView(Sender).Selected.AbsoluteIndex].Autorizado := ( TTreeView(Sender).Selected.ImageIndex = coSelecionado );
end;

procedure TFormCadastroAutorizacoes.tvTelasSistemaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_SPACE then
    tvTelasSistemaDblClick(Sender);
end;

end.
