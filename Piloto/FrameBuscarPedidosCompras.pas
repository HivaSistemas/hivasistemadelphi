unit FrameBuscarPedidosCompras;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.Generics.Collections, EntradaCarregarPedidos,
  Vcl.Menus, Vcl.StdCtrls, StaticTextLuka, Vcl.Grids, GridLuka, _Biblioteca, _EntradasNFItensCompras,
  System.Generics.Defaults, Frame.HerancaInsercaoExclusao, _Compras, _ComprasItens, _Sessao;

type
  TAcaoPedido = record
    const RemoveuPedido = 'RemoveuPedido';
    const InseriuPedido = 'InseriuPedido';
    const AlterouQuantidade = 'AlterouQuantidade';
  end;

  TOnFrameChange = procedure(Sender: TObject; TipoMudanca: string) of object;

  TFrBuscarPedidosCompras = class(TFrameHerancaInsercaoExclusao)
    StaticTextLuka2: TStaticTextLuka;
    sgPedidos: TGridLuka;
    procedure sgValoresDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgValoresGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgValoresArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
    procedure sgValoresSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure sgPedidosDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
  private
    FOnChange: TOnFrameChange;
    FEmpresaId: Integer;
    FItemId: Integer;
    FProdutoId: Integer;
    FMultiploCompra: Double;
    FFornecedorId: Integer;
    FQuantidadeBuscar: Currency;
    FItens: TArray<RecEntradasNFItensCompras>;
    FPedidosCompra: TArray<RecDadosCompras>;

    function AddPedidoGrid(
      pCompraId: Integer;
      pItemCompraId: Integer;
      pSaldo: Double;
      pQuantidade: Double
    ): Boolean;

    procedure setProdutoId(pProdutoId: Integer);

    procedure addNoVetor(
      pCompraId: Integer;
      pItemCompraId: Integer;
      pProdutoId: Integer;
      pItemId: Integer;
      pSaldo: Double;
      pQuantidade: Double
    );

    function getQuantidadeDefinida: Currency;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
    function VerificarDivergenciaAdicionandoNovoPedido(
      pProdutoId: Integer;
      pQuantidade: Double
    ): Boolean;
    function VincularItemIdEQuantidade(
      pProdutoId,
      pItemId: Integer;
      pQuantidade: Double
    ): Boolean;
    procedure DesvincularItemEQuantidade(
      pProdutoId: Integer
    );
    procedure NotificarMudanca(TipoMudanca: string);
    procedure BuscarPedidosCompraCarregados();
    property OnChange: TOnFrameChange read FOnChange write FOnChange;
  protected
    procedure Inserir; override;
    procedure Excluir; override;
  published
    property EmpresaId: Integer read FEmpresaId write FEmpresaId;
    property ProdutoId: Integer read FProdutoId write setProdutoId;
    property ItemId: Integer read FItemId write FItemId;
    property MultiploCompra: Double read FMultiploCompra write FMultiploCompra;
    property FornecedorId: Integer read FFornecedorId write FFornecedorId;
    property QuantidadeBuscar: Currency read FQuantidadeBuscar write FQuantidadeBuscar;
    property ItensPedidoCompra: TArray<RecEntradasNFItensCompras> read FItens write FItens;
  end;

implementation

{$R *.dfm}

const
  coCompraId     = 0;
  coSaldo        = 1;
  coQuantidade   = 2;
  coItemCompraId = 3;

  //GridPedidos
  pePedidoId      = 0;
  peData          = 1;
  peValor         = 2;

procedure TFrBuscarPedidosCompras.addNoVetor(
  pCompraId: Integer;
  pItemCompraId: Integer;
  pProdutoId: Integer;
  pItemId: Integer;
  pSaldo: Double;
  pQuantidade: Double
);
var
  i: Integer;
  vPosic: Integer;
begin
  vPosic := -1;
  for i := Low(FItens) to High(FItens) do begin
    if
      (pCompraId = FItens[i].CompraId) and
      (pItemCompraId = FItens[i].ItemCompraId) and
      (pProdutoId = FItens[i].ProdutoId)
    then begin
      vPosic := i;
      Break;
    end;
  end;

  if vPosic = -1 then begin
    SetLength(FItens, Length(FItens) + 1);
    vPosic := High(FItens);
  end;

  FItens[vPosic].CompraId     := pCompraId;
  FItens[vPosic].ItemCompraId := pItemCompraId;
  FItens[vPosic].ProdutoId    := pProdutoId;
  FItens[vPosic].ItemId       := pItemId;
  FItens[vPosic].Saldo        := pSaldo;
  FItens[vPosic].Quantidade   := pQuantidade;
end;

function TFrBuscarPedidosCompras.AddPedidoGrid(
  pCompraId: Integer;
  pItemCompraId: Integer;
  pSaldo: Double;
  pQuantidade: Double
): Boolean;
var
  vLinha: Integer;
begin
  Result := False;
  vLinha := sgValores.Localizar([coCompraId], [NFormat(pCompraId)]);
  if vLinha > -1 then begin
    sgValores.Row := vLinha;
    Exit;
  end;

  if sgValores.Cells[coCompraId, 1] = '' then
    vLinha := 1
  else
    vLinha := sgValores.RowCount;

  sgValores.Cells[coCompraId, vLinha]     := NFormat(pCompraId);
  sgValores.Cells[coSaldo, vLinha]        := NFormatEstoque(pSaldo);
  sgValores.Cells[coQuantidade, vLinha]   := NFormatEstoque(pQuantidade);
  sgValores.Cells[coItemCompraId, vLinha] := NFormat(pItemCompraId);

  sgValores.RowCount := vLinha + 1;
  sgValores.Row := vLinha;

  Result := True;
end;

procedure TFrBuscarPedidosCompras.BuscarPedidosCompraCarregados;
var
  i: Integer;
  j: Integer;
  l: Integer;
  k: Integer;
  vExisteVetor: Boolean;
  vPedidos: TArray<Integer>;
  vInserido: Boolean;
  FDados: TArray<RecCompras>;
  FItensDadosItens: TArray<RecComprasItens>;
  FItensAux: TArray<RecComprasItens>;
  vSql: string;
begin
  for i := Low(FItens) to High(FItens) do begin

    vInserido := False;
    for j := Low(vPedidos) to High(vPedidos) do begin
      if FItens[i].CompraId <> vPedidos[j] then
        Continue;

      vInserido := True;
    end;


    if not vInserido then begin
      SetLength(vPedidos, Length(vPedidos) + 1);
      vPedidos[High(vPedidos)] := FItens[i].CompraId;
    end;
  end;

  if vPedidos = nil then
    Exit;

  vSql := ' where ' + _Biblioteca.FiltroInInt('COM.COMPRA_ID', vPedidos);
  FDados := _Compras.BuscarComprasComando(Sessao.getConexaoBanco, vSql);
  FItensDadosItens := _ComprasItens.BuscarComprasItensComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('ITE.COMPRA_ID', vPedidos));

  for i := Low(FDados) to High(FDados) do begin
    FItensAux := nil;
    for j := Low(FItensDadosItens) to High(FItensDadosItens) do begin
      if FDados[i].CompraId <> FItensDadosItens[j].CompraId then
        Continue;

      SetLength(FItensAux, Length(FItensAux) + 1);
      FItensAux[High(FItensAux)] := FItensDadosItens[j];
    end;

    SetLength(FPedidosCompra, Length(FPedidosCompra) + 1);
    FPedidosCompra[High(FPedidosCompra)].Compra := FDados[i];
    FPedidosCompra[High(FPedidosCompra)].Itens := FItensAux;

    for l := Low(FItensAux) to High(FItensAux) do begin
      vExisteVetor := False;
      for k := Low(FItens) to High(FItens) do begin
        if (FItensAux[l].CompraId = FItens[k].CompraId) and (FItensAux[l].ProdutoId = FItens[k].ProdutoId) then
          vExisteVetor := True;
      end;

      if not vExisteVetor then
        addNoVetor(
          FItensAux[l].CompraId,
          FItensAux[l].ItemId,
          FItensAux[l].ProdutoId,
          0,
          FItensAux[l].Saldo,
          0
        );

    end;
  end;

  sgPedidos.ClearGrid();
  //Limpando o grid de pedidos e adicionando
  //novamente todos j� carregados at� o momento
  for i := Low(FPedidosCompra) to High(FPedidosCompra) do begin
    sgPedidos.Cells[pePedidoId, i + 1] := NFormat(FPedidosCompra[i].Compra.CompraId);
    sgPedidos.Cells[peData, i + 1]     := DFormat(FPedidosCompra[i].Compra.DataHoraCompra);
    sgPedidos.Cells[peValor, i + 1]    := NFormatN(FPedidosCompra[i].Compra.ValorLiquido);
  end;

  sgPedidos.SetLinhasGridPorTamanhoVetor(Length(FPedidosCompra));
  SetarFoco(sgPedidos);
end;

constructor TFrBuscarPedidosCompras.Create(AOwner: TComponent);
begin
  inherited;
  sgValores.Col := coQuantidade;
end;

procedure TFrBuscarPedidosCompras.Excluir;
var
  i: Integer;
  vAuxiliar: TArray<RecEntradasNFItensCompras>;
  vPedidoId: string;
  vPedidosCompraAux: TArray<RecDadosCompras>;
begin
  vPedidoId := sgPedidos.Cells[pePedidoId, sgPedidos.Row];
  if vPedidoId = '' then
    Exit;

  for i := Low(FPedidosCompra) to High(FPedidosCompra) do begin
    if FPedidosCompra[i].Compra.CompraId = SFormatInt(vPedidoId) then
      Continue;

    SetLength(vPedidosCompraAux, Length(vPedidosCompraAux) + 1);
    vPedidosCompraAux[High(vPedidosCompraAux)] := FPedidosCompra[i];
  end;

  FPedidosCompra := vPedidosCompraAux;
  sgPedidos.ClearGrid();
  for i := Low(FPedidosCompra) to High(FPedidosCompra) do begin
    sgPedidos.Cells[pePedidoId, i + 1] := NFormat(FPedidosCompra[i].Compra.CompraId);
    sgPedidos.Cells[peData, i + 1]     := DFormat(FPedidosCompra[i].Compra.DataHoraCompra);
    sgPedidos.Cells[peValor, i + 1]    := NFormatN(FPedidosCompra[i].Compra.ValorLiquido);
  end;

  sgPedidos.SetLinhasGridPorTamanhoVetor(Length(FPedidosCompra));
  SetarFoco(sgPedidos);

  vAuxiliar := nil;
  for i := Low(FItens) to High(FItens) do begin
    if (SFormatInt(vPedidoId) = FItens[i].CompraId) then
      Continue;

    SetLength(vAuxiliar, Length(vAuxiliar) + 1);
    vAuxiliar[High(vAuxiliar)] := FItens[i];
  end;

  inherited;

  (* Se chegou at� aqui quer dizer que o item foi deletado da grid, ou seja, n�o � mais para vincular o pedido de compra *)
  (* ent�o vamos apenas recriar o array sem o item que acabou de ser deletado *)
  FItens := nil;
  FItens := vAuxiliar;
  NotificarMudanca(TAcaoPedido.RemoveuPedido); //Implementar
end;

function TFrBuscarPedidosCompras.getQuantidadeDefinida: Currency;
var
  i: Integer;
begin
  Result := 0;
  for i := 1 to sgValores.RowCount -1 do
    Result := Result + SFormatCurr(sgValores.Cells[coQuantidade, i]);
end;

procedure TFrBuscarPedidosCompras.Inserir;
var
  i: Integer;
  j: Integer;
  l: Integer;
  vRetTela: TRetornoTelaFinalizar<TArray<RecDadosCompras>>;
  pedidosTempArray: TArray<RecDadosCompras>;
  idxHigh: Integer;
  jaExiste: Boolean;
begin
  inherited;
  vRetTela := EntradaCarregarPedidos.Pesquisar(FEmpresaId, FFornecedorId);
  if vRetTela.BuscaCancelada then
    Exit;

  for j := Low(vRetTela.Dados) to High(vRetTela.Dados) do begin
    //Se for o primeiro pedido de compra carregado
    //j� adicionando aqui mesmo antes do loop
    if FPedidosCompra = nil then begin
      SetLength(pedidosTempArray, Length(pedidosTempArray) + 1);
      idxHigh := High(pedidosTempArray);
      pedidosTempArray[idxHigh].Compra := vRetTela.Dados[j].Compra;
      pedidosTempArray[idxHigh].Itens := vRetTela.Dados[j].Itens;

      for l := Low(vRetTela.Dados[j].Itens) to High(vRetTela.Dados[j].Itens) do begin
        addNoVetor(
          vRetTela.Dados[j].Itens[l].CompraId,
          vRetTela.Dados[j].Itens[l].ItemId,
          vRetTela.Dados[j].Itens[l].ProdutoId,
          0,
          vRetTela.Dados[j].Itens[l].Saldo,
          0
        );
      end;
    end
    else begin
      jaExiste := False;
      for i := Low(FPedidosCompra) to High(FPedidosCompra) do begin
        // Verificando quais pedidos ainda n�o foram selecionados
        // Depois de verificar j� adiciona no vetor
        if vRetTela.Dados[j].Compra.CompraId <> FPedidosCompra[i].Compra.CompraId then
          Continue;

        jaExiste := True;
        Break;
      end;

      if not jaExiste then begin
        SetLength(pedidosTempArray, Length(pedidosTempArray) + 1);
        idxHigh := High(pedidosTempArray);
        pedidosTempArray[idxHigh].Compra := vRetTela.Dados[j].Compra;
        pedidosTempArray[idxHigh].Itens := vRetTela.Dados[j].Itens;

        for l := Low(vRetTela.Dados[j].Itens) to High(vRetTela.Dados[j].Itens) do begin
          addNoVetor(
            vRetTela.Dados[j].Itens[l].CompraId,
            vRetTela.Dados[j].Itens[l].ItemId,
            vRetTela.Dados[j].Itens[l].ProdutoId,
            0,
            vRetTela.Dados[j].Itens[l].Saldo,
            0
          );
        end;
      end;
    end;
  end;

  //Copiando do array temporario os pedidos novos carregados
  //para os pedidos de compra carregados anteriormente
  for i := Low(pedidosTempArray) to High(pedidosTempArray) do begin
    SetLength(FPedidosCompra, Length(FPedidosCompra) + 1);
    idxHigh := High(FPedidosCompra);
    FPedidosCompra[idxHigh].Compra := pedidosTempArray[i].Compra;
    FPedidosCompra[idxHigh].Itens := pedidosTempArray[i].Itens;
  end;

  // Ordenando o array pelo campo CompraId (do menor para o maior)
  TArray.Sort<RecDadosCompras>(FPedidosCompra, TComparer<RecDadosCompras>.Construct(
    function(const Left, Right: RecDadosCompras): Integer
    begin
      Result := Left.Compra.CompraId - Right.Compra.CompraId;
    end)
  );

  sgPedidos.ClearGrid();
  //Limpando o grid de pedidos e adicionando
  //novamente todos j� carregados at� o momento
  for i := Low(FPedidosCompra) to High(FPedidosCompra) do begin
    sgPedidos.Cells[pePedidoId, i + 1] := NFormat(FPedidosCompra[i].Compra.CompraId);
    sgPedidos.Cells[peData, i + 1]     := DFormat(FPedidosCompra[i].Compra.DataHoraCompra);
    sgPedidos.Cells[peValor, i + 1]    := NFormatN(FPedidosCompra[i].Compra.ValorLiquido);
  end;

  sgPedidos.SetLinhasGridPorTamanhoVetor(Length(FPedidosCompra));
  SetarFoco(sgPedidos);
  NotificarMudanca(TAcaoPedido.InseriuPedido);
end;

procedure TFrBuscarPedidosCompras.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;
  if pLimpar then begin
    FItens := nil;
    sgPedidos.ClearGrid();
    FPedidosCompra := nil;
  end;
end;

procedure TFrBuscarPedidosCompras.NotificarMudanca(TipoMudanca: string);
begin
  if Assigned(FOnChange) then
    FOnChange(Self, TipoMudanca);
end;

procedure TFrBuscarPedidosCompras.setProdutoId(pProdutoId: Integer);
var
  i: Integer;
begin
  sgValores.ClearGrid();
  FProdutoId := pProdutoId;
  for i := Low(FItens) to High(FItens) do begin
    if FProdutoId <> FItens[i].ProdutoId then
      Continue;

    AddPedidoGrid(
      FItens[i].CompraId,
      FItens[i].ItemCompraId,
      FItens[i].Saldo,
      FItens[i].Quantidade
    );
  end;
end;

procedure TFrBuscarPedidosCompras.sgPedidosDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  inherited;
  sgPedidos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taRightJustify, Rect);
end;

procedure TFrBuscarPedidosCompras.sgValoresArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
begin
  inherited;
  if ARow = 0 then
    Exit;

  TextCell := NFormatNEstoque(SFormatDouble(TextCell));
  if TextCell = '' then
    Exit;

  if getQuantidadeDefinida > QuantidadeBuscar then begin
    _Biblioteca.Exclamar('A quantidade a se utilizar n�o pode ser maior que a quantidade necess�ria!');
    TextCell := '';
    Exit;
  end;

  if SFormatCurr(TextCell) > SFormatCurr(sgValores.Cells[coSaldo, sgValores.Row]) then begin
    _Biblioteca.Exclamar('A quantidade a se utilizar n�o pode que o saldo dispon�vel!');
    TextCell := '';
    Exit;
  end;

  addNoVetor(
    SFormatInt(sgValores.Cells[coCompraId, sgValores.Row]),
    SFormatInt(sgValores.Cells[coItemCompraId, sgValores.Row]),
    FProdutoId,
    FItemId,
    SFormatDouble(sgValores.Cells[coSaldo, sgValores.Row]),
    SFormatDouble(TextCell)
  );

  NotificarMudanca(TAcaoPedido.AlterouQuantidade);
end;

procedure TFrBuscarPedidosCompras.sgValoresDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  inherited;
  sgValores.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taRightJustify, Rect);
end;

procedure TFrBuscarPedidosCompras.sgValoresGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coQuantidade then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end;
end;

procedure TFrBuscarPedidosCompras.sgValoresSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  CanSelect := ACol = coQuantidade;
end;

function TFrBuscarPedidosCompras.VerificarDivergenciaAdicionandoNovoPedido(
  pProdutoId: Integer;
  pQuantidade: Double
): Boolean;
var
  i: Integer;
  vQuantidadeTotal: Double;
  vTotalSaldo: Double;
begin
  vQuantidadeTotal := 0;
  vTotalSaldo := 0;
  for i := Low(FItens) to High(FItens) do begin
    if FItens[i].ProdutoId <> pProdutoId then
      Continue;

    vQuantidadeTotal := vQuantidadeTotal + FItens[i].Quantidade;
    vTotalSaldo := vTotalSaldo + FItens[i].Saldo;
  end;

  if vTotalSaldo = 0 then
    Result := True
  else
    Result := vTotalSaldo < pQuantidade;
end;

function TFrBuscarPedidosCompras.VincularItemIdEQuantidade(
  pProdutoId,
  pItemId: Integer;
  pQuantidade: Double
): Boolean;
var
  i: Integer;
  quantidadeFalta: Double;
  vTemDivergencia: Boolean;
  vTotalSaldo: Double;
begin
  vTemDivergencia := False;
  quantidadeFalta := pQuantidade;
  vTotalSaldo := 0;
  if FItens = nil then
    vTemDivergencia := False
  else begin
    vTemDivergencia := True;
    for i := Low(FItens) to High(FItens) do begin
      if FItens[i].ProdutoId <> pProdutoId then
        Continue;

      FItens[i].ItemId := pItemId;
      vTotalSaldo := vTotalSaldo + FItens[i].Saldo;

      if quantidadeFalta >= FItens[i].Saldo then begin
        FItens[i].Quantidade := FItens[i].Saldo;
        quantidadeFalta := quantidadeFalta - FItens[i].Saldo;
      end
      else begin
        FItens[i].Quantidade := quantidadeFalta;
        quantidadeFalta := 0;
      end;
    end;

    if vTotalSaldo = 0 then
      vTemDivergencia := True
    else
      vTemDivergencia := vTotalSaldo < pQuantidade;
  end;

  NotificarMudanca('AlterouQuantidade');
  Result := vTemDivergencia;
end;

procedure TFrBuscarPedidosCompras.DesvincularItemEQuantidade(pProdutoId: Integer);
var
  i: Integer;
begin
  for i := Low(FItens) to High(FItens) do begin
    if FItens[i].ProdutoId <> pProdutoId then
      Continue;

    FItens[i].ItemId := 0;
    FItens[i].Quantidade := 0;
  end;
end;

end.
