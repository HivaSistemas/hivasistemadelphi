unit FrameGruposTributacoesEstadualVenda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Buttons, _Sessao,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Biblioteca, _GruposTribEstadualVenda,
  PesquisaGruposTributacoesEstadualVenda, Vcl.Menus;

type
  TFrGruposTributacoesEstadualVenda = class(TFrameHenrancaPesquisas)
  public
    function getGrupo(pLinha: Integer = -1): RecGruposTribEstadualVenda;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function AdicionarPesquisandoTodos: TArray<TObject>; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrGruposTributacoesEstadualVenda }

function TFrGruposTributacoesEstadualVenda.AdicionarDireto: TObject;
var
  vDados: TArray<RecGruposTribEstadualVenda>;
begin
  vDados := _GruposTribEstadualVenda.BuscarGruposTribEstadualVenda(Sessao.getConexaoBanco, 0, [FChaveDigitada]);
  if vDados = nil then
    Result := nil
  else
    Result := vDados[0];
end;

function TFrGruposTributacoesEstadualVenda.AdicionarPesquisando: TObject;
begin
  Result := PesquisaGruposTributacoesEstadualVenda.Pesquisar;
end;

function TFrGruposTributacoesEstadualVenda.AdicionarPesquisandoTodos: TArray<TObject>;
begin

end;

function TFrGruposTributacoesEstadualVenda.getGrupo(pLinha: Integer): RecGruposTribEstadualVenda;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecGruposTribEstadualVenda(FDados[IIfInt(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrGruposTributacoesEstadualVenda.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecGruposTribEstadualVenda(FDados[i]).GrupoTribEstadualVendaId = RecGruposTribEstadualVenda(pSender).GrupoTribEstadualVendaId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrGruposTributacoesEstadualVenda.MontarGrid;
var
  i: Integer;
  vSender: RecGruposTribEstadualVenda;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecGruposTribEstadualVenda(FDados[i]);
      AAdd([IntToStr(vSender.GrupoTribEstadualVendaId), vSender.Descricao]);
    end;
  end;
end;

end.
