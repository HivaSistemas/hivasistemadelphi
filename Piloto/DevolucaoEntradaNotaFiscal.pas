unit DevolucaoEntradaNotaFiscal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Biblioteca, _Sessao,
  CheckBoxLuka, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, SpeedButtonLuka, Vcl.Grids, Pesquisa.EntradaNotasFiscais,
  GridLuka, Vcl.Mask, EditLukaData, _FrameHerancaPrincipal, _RecordsEstoques, _EntradasNotasFiscais,
  Frame.HerancaInsercaoExclusao, FrameBuscarLotes, _EntradasNotasFiscaisItens, _RecordsEspeciais,
  StaticTextLuka, _DevolucoesEntradasNotasFisc, _DevolucoesEntradasNfItens, _EntradasNfItensLotes,
  Vcl.Menus, _DevolucoesEntrNfItensLotes;

type
  TFormDevolucaoEntradaNotaFiscal = class(TFormHerancaCadastroCodigo)
    sbInformacoesEntrada: TSpeedButtonLuka;
    lb2: TLabel;
    eFornecedor: TEditLuka;
    lb1: TLabel;
    eNumeroNotaFiscal: TEditLuka;
    lb3: TLabel;
    lb4: TLabel;
    eModeloNota: TEditLuka;
    eSerieNota: TEditLuka;
    lb6: TLabel;
    eDataEmissaoNota: TEditLukaData;
    lb5: TLabel;
    eObservacoes: TEditLuka;
    eMotivoDevolucao: TEditLuka;
    lb7: TLabel;
    sgItens: TGridLuka;
    st1: TStaticTextLuka;
    st3: TStaticTextLuka;
    st4: TStaticTextLuka;
    st5: TStaticTextLuka;
    st6: TStaticTextLuka;
    st7: TStaticTextLuka;
    stValorProdutos: TStaticTextLuka;
    stOutrasDespesas: TStaticTextLuka;
    stDesconto: TStaticTextLuka;
    stValorIcmsSt: TStaticTextLuka;
    stIPI: TStaticTextLuka;
    stTotalDevolucao: TStaticTextLuka;
    st2: TStaticTextLuka;
    stBaseICMS: TStaticTextLuka;
    st9: TStaticTextLuka;
    stValorICMS: TStaticTextLuka;
    stBaseIcmsSt: TStaticTextLuka;
    st12: TStaticTextLuka;
    FrBuscarLotes: TFrBuscarLotes;
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
    procedure eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); override;
    procedure sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure sgItensClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FrBuscarLotesExit(Sender: TObject);
  private
    FLotes: TArray<RecEntradasNfItensLotes>;
    FEntrada: RecEntradaNotaFiscal;

    procedure PreencherRegistro(pNota: RecEntradaNotaFiscal; pNotaItens: TArray<RecEntradaNotaFiscalItem>);
    procedure AjustarTotalDevolucao(pLinha: Integer);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormDevolucaoEntradaNotaFiscal }

const
  coProdutoId       = 0;
  coNomeProduto     = 1;
  coMarca           = 2;
  coQtdeNota        = 3;
  coUnidadeNota     = 4;
  coQtdeDevolver    = 5;
  coUnidadeAltis    = 6;
  coQtdeDevolvAltis = 7;
  coLocalId         = 8;
  coNomeLocal       = 9;

  (* Ocultas *)
  coItemId        = 10;
  coMultiplo      = 11;
  coQtdeEmbalagem = 12;

  coBaseIcms      = 13;
  coValorIcms     = 14;
  coBaseIcmsSt    = 15;
  coValorIcmsSt   = 16;
  coValorIpi      = 17;
  coOutrasDesp    = 18;
  coDesconto      = 19;
  coPrecoUnitario = 20;
  coQtdeEntrada   = 21;
  coPercIcms      = 22;
  coPercIcmsSt    = 23;
  coPercIPI       = 24;
  coPesoUnit         = 25;
  coTipoControleEst  = 26;
  coMultiploCompra   = 27;
  coExigirDataFabr   = 28;
  coExigirDataVencto = 29;

procedure TFormDevolucaoEntradaNotaFiscal.AjustarTotalDevolucao(pLinha: Integer);
var
  i: Integer;
  vQtde: Currency;
  vQtdeEntrada: Currency;
begin
  _Biblioteca.LimparCampos([
    stValorProdutos,
    stOutrasDespesas,
    stDesconto,
    stBaseICMS,
    stValorICMS,
    stBaseIcmsSt,
    stValorIcmsSt,
    stIPI,
    stTotalDevolucao
  ]);

  sgItens.Cells[coQtdeDevolvAltis, pLinha] :=
    NFormatNEstoque(
      SFormatCurr(sgItens.Cells[coQtdeDevolver, pLinha]) * SFormatCurr(sgItens.Cells[coQtdeEmbalagem, pLinha])
    );

  for i := 1 to sgItens.RowCount -1 do begin
    vQtde := SFormatCurr(sgItens.Cells[coQtdeDevolver, i]);
    if vQtde = 0 then
      Continue;

    vQtdeEntrada := SFormatCurr(sgItens.Cells[coQtdeEntrada, i]);

    stValorProdutos.Somar( Arredondar(SFormatCurr(sgItens.Cells[coPrecoUnitario, i]) * vQtde, 2) );
    stOutrasDespesas.Somar( Arredondar(SFormatCurr(sgItens.Cells[coOutrasDesp, i]) / vQtdeEntrada * vQtde, 2) );
    stDesconto.Somar( Arredondar(SFormatCurr(sgItens.Cells[coDesconto, i])  / vQtdeEntrada * vQtde, 2) );
    stBaseICMS.Somar( Arredondar(SFormatCurr(sgItens.Cells[coBaseIcms, i]) / vQtdeEntrada * vQtde, 2) );
    stValorICMS.Somar( Arredondar(SFormatCurr(sgItens.Cells[coValorIcms, i]) / vQtdeEntrada * vQtde, 2) );
    stBaseIcmsSt.Somar( Arredondar(SFormatCurr(sgItens.Cells[coBaseIcmsSt, i]) / vQtdeEntrada * vQtde, 2) );
    stValorIcmsSt.Somar( Arredondar(SFormatCurr(sgItens.Cells[coValorIcmsSt, i]) / vQtdeEntrada * vQtde, 2) );
    stIPI.Somar( Arredondar(SFormatCurr(sgItens.Cells[coValorIpi, i]) / vQtdeEntrada * vQtde, 2) );
  end;

  stTotalDevolucao.AsCurr :=
    stValorProdutos.AsCurr +
    stOutrasDespesas.AsCurr -
    stDesconto.AsCurr +
    stValorIcmsSt.AsCurr +
    stIPI.AsCurr;
end;

procedure TFormDevolucaoEntradaNotaFiscal.BuscarRegistro;
var
  vNota: TArray<RecEntradaNotaFiscal>;
  vItens: TArray<RecEntradaNotaFiscalItem>;
begin
  vNota := _EntradasNotasFiscais.BuscarEntradaNotaFiscais(Sessao.getConexaoBanco, 0, [eID.AsInt]);
  if vNota = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  vItens := _EntradasNotasFiscaisItens.BuscarEntradaNotaFiscalItens(Sessao.getConexaoBanco, 1, [eID.AsInt]);
  if vItens = nil then begin
    _Biblioteca.Exclamar('N�o foi encontrado nenhum item com saldo para devolu��o!');
    Exit;
  end;

  inherited;
  PreencherRegistro(vNota[0], vItens);
end;

procedure TFormDevolucaoEntradaNotaFiscal.eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key <> VK_RETURN then
    Exit;

  if eID.AsInt <> 0 then begin
    BuscarRegistro;
    Exit;
  end;

  inherited;
end;

procedure TFormDevolucaoEntradaNotaFiscal.ExcluirRegistro;
begin
  inherited;

end;

procedure TFormDevolucaoEntradaNotaFiscal.FormCreate(Sender: TObject);
begin
  inherited;
  FrBuscarLotes.Tela := ttDevolucaEntradas;
end;

procedure TFormDevolucaoEntradaNotaFiscal.FrBuscarLotesExit(Sender: TObject);
begin
  inherited;
  FrBuscarLotes.ProdutoId := SFormatInt( sgItens.Cells[coProdutoId, sgItens.Row] );
  FLotes                  := FrBuscarLotes.Lotes;
end;

procedure TFormDevolucaoEntradaNotaFiscal.GravarRegistro(Sender: TObject);
var
  i: Integer;
  j: Integer;

  vQtde: Currency;
  vQtdeEntrada: Currency;

  vRetBanco: RecRetornoBD;
  vItens: TArray<RecDevolucoesEntradasNfItens>;
  vLotes: TArray<RecDevolucoesEntrNfItensLotes>;
begin
  inherited;

  vItens := nil;
  vLotes := nil;
  for i := 1 to sgItens.RowCount -1 do begin
    vQtde := SFormatCurr(sgItens.Cells[coQtdeDevolver, i]);
    if vQtde = 0 then
      Continue;

    vQtdeEntrada := SFormatCurr(sgItens.Cells[coQtdeEntrada, i]);

    SetLength(vItens, Length(vItens) + 1);

    vItens[High(vItens)].ProdutoId := SFormatInt(sgItens.Cells[coProdutoId, i]);
    vItens[High(vItens)].ItemId    := SFormatInt(sgItens.Cells[coItemId, i]);
    vItens[High(vItens)].LocalId   := SFormatInt(sgItens.Cells[coLocalId, i]);

    vItens[High(vItens)].ValorTotal               := Arredondar(SFormatCurr(sgItens.Cells[coPrecoUnitario, i]) * vQtde, 2);
    vItens[High(vItens)].PrecoUnitario            := SFormatCurr(sgItens.Cells[coPrecoUnitario, i]);
    vItens[High(vItens)].Quantidade               := vQtde;
    vItens[High(vItens)].ValorTotalDesconto       := Arredondar(SFormatCurr(sgItens.Cells[coDesconto, i])  / vQtdeEntrada * vQtde, 2);
    vItens[High(vItens)].ValorTotalOutrasDespesas := Arredondar(SFormatCurr(sgItens.Cells[coOutrasDesp, i]) / vQtdeEntrada * vQtde, 2);

    vItens[High(vItens)].BaseCalculoIcms  := Arredondar(SFormatCurr(sgItens.Cells[coBaseIcms, i]) / vQtdeEntrada * vQtde, 2);
    vItens[High(vItens)].PercentualIcms   := SFormatInt(sgItens.Cells[coPercIcms, i]);
    vItens[High(vItens)].ValorIcms        := Arredondar(SFormatCurr(sgItens.Cells[coValorIcms, i]) / vQtdeEntrada * vQtde, 2);

    vItens[High(vItens)].BaseCalculoIcmsSt := Arredondar(SFormatCurr(sgItens.Cells[coBaseIcmsSt, i]) / vQtdeEntrada * vQtde, 2);
    vItens[High(vItens)].PercentualIcmsSt  := SFormatInt(sgItens.Cells[coPercIcmsSt, i]);
    vItens[High(vItens)].ValorIcmsSt       := Arredondar(SFormatCurr(sgItens.Cells[coValorIcmsSt, i]) / vQtdeEntrada * vQtde, 2);

    vItens[High(vItens)].PercentualIpi  := SFormatInt(sgItens.Cells[coPercIPI, i]);
    vItens[High(vItens)].ValorIpi       := Arredondar(SFormatCurr(sgItens.Cells[coValorIpi, i]) / vQtdeEntrada * vQtde, 2);

    vItens[High(vItens)].PesoUnitario := SFormatInt(sgItens.Cells[coPesoUnit, i]);
  end;

  if vItens = nil then begin
    _Biblioteca.Exclamar('Nenhum item foi definido para devolu��o!');
    Abort;
  end;

  for i := Low(FLotes) to High(FLotes) do begin
    if FLotes[i].Quantidade = 0 then
      Continue;

    SetLength(vLotes, Length(vLotes) + 1);

    vLotes[High(vLotes)].ItemId     := FLotes[i].ItemId;
    vLotes[High(vLotes)].Lote       := FLotes[i].Lote;
    vLotes[High(vLotes)].Quantidade := FLotes[i].Quantidade;
  end;

  vRetBanco :=
    _DevolucoesEntradasNotasFisc.AtualizarDevolucoesEntradasNotasFisc(
      Sessao.getConexaoBanco,
      0,
      eID.AsInt,
      FEntrada.empresa_id,
      stTotalDevolucao.AsCurr,
      stValorProdutos.AsCurr,
      stDesconto.AsCurr,
      stOutrasDespesas.AsCurr,
      stBaseICMS.AsCurr,
      stValorICMS.AsCurr,
      stBaseIcmsSt.AsCurr,
      stValorIcmsSt.AsCurr,
      stIPI.AsCurr,
      eMotivoDevolucao.Text,
      eObservacoes.Text,
      'AGC',
      vItens,
      vLotes
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormDevolucaoEntradaNotaFiscal.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    sbInformacoesEntrada,
    eFornecedor,
    eNumeroNotaFiscal,
    eModeloNota,
    eSerieNota,
    eDataEmissaoNota,
    eObservacoes,
    eMotivoDevolucao,

    stValorProdutos,
    stOutrasDespesas,
    stDesconto,
    stBaseICMS,
    stValorICMS,
    stBaseIcmsSt,
    stValorIcmsSt,
    stIPI,
    stTotalDevolucao,
    FrBuscarLotes,

    sgItens],
    pEditando
  );

  if pEditando then begin
    sgItens.Col := coQtdeDevolver;
    SetarFoco(eMotivoDevolucao);
  end;
end;

procedure TFormDevolucaoEntradaNotaFiscal.PesquisarRegistro;
var
  vRetTela: TRetornoTelaFinalizar<RecEntradaNotaFiscal>;
  vItens: TArray<RecEntradaNotaFiscalItem>;
begin
  vRetTela := Pesquisa.EntradaNotasFiscais.Pesquisar(ttDevolucaoEntrada);
  if vRetTela.BuscaCancelada then
    Exit;

  vItens := _EntradasNotasFiscaisItens.BuscarEntradaNotaFiscalItens(Sessao.getConexaoBanco, 0, [vRetTela.Dados.entrada_id]);
  if vItens = nil then begin
    _Biblioteca.Exclamar('N�o h� nenhum produto com saldo para devolu��o!');
    Exit;
  end;

  eID.AsInt := vRetTela.Dados.entrada_id;
  inherited;
  PreencherRegistro(vRetTela.Dados, vItens);
end;

procedure TFormDevolucaoEntradaNotaFiscal.PreencherRegistro(pNota: RecEntradaNotaFiscal; pNotaItens: TArray<RecEntradaNotaFiscalItem>);
var
  i: Integer;
  vItensIds: TArray<Integer>;
begin

  if pNota.Status <> 'BAI' then begin
    _Biblioteca.Exclamar('Apenas entradas de notas fiscais baixados podem ter a devolu��o realizada!');
    Modo(False);
    Abort;
  end;

  FEntrada := pNota;

  eFornecedor.SetInformacao(pNota.fornecedor_id, pNota.Razao_Social);
  eNumeroNotaFiscal.AsInt := pNota.numero_nota;
  eModeloNota.Text        := pNota.modelo_nota;
  eSerieNota.Text         := pNota.serie_nota;
  eDataEmissaoNota.AsData := pNota.data_hora_emissao;

  for i := Low(pNotaItens) to High(pNotaItens) do begin
    sgItens.Cells[coProdutoId, i + 1]       := NFormat(pNotaItens[i].produto_id);
    sgItens.Cells[coNomeProduto, i + 1]     := pNotaItens[i].nome_produto;
    sgItens.Cells[coMarca, i + 1]           := pNotaItens[i].nome_marca;
    sgItens.Cells[coQtdeNota, i + 1]        := NFormatNEstoque(pNotaItens[i].quantidade - pNotaItens[i].QuantidadeDevolvidos);
    sgItens.Cells[coUnidadeNota, i + 1]     := pNotaItens[i].UnidadeCompraId;
    sgItens.Cells[coQtdeDevolver, i + 1]    := '';
    sgItens.Cells[coUnidadeAltis, i + 1]    := pNotaItens[i].UnidadeEntradaId;
    sgItens.Cells[coQtdeDevolvAltis, i + 1] := '';

    sgItens.Cells[coLocalId, i + 1]   := NFormat(pNotaItens[i].LocalEntradaId);
    sgItens.Cells[coNomeLocal, i + 1] := pNotaItens[i].NomeLocal;

    sgItens.Cells[coItemId, i + 1]          := NFormat(pNotaItens[i].item_id);
    sgItens.Cells[coMultiplo, i + 1]        := NFormatNEstoque(pNotaItens[i].MultiploCompra);
    sgItens.Cells[coQtdeEmbalagem, i + 1]   := NFormatNEstoque(pNotaItens[i].QuantidadeEmbalagem);

    sgItens.Cells[coBaseIcms, i + 1]      := NFormatN(pNotaItens[i].base_calculo_icms);
    sgItens.Cells[coValorIcms, i + 1]     := NFormatN(pNotaItens[i].valor_icms);
    sgItens.Cells[coBaseIcmsSt, i + 1]    := NFormatN(pNotaItens[i].base_calculo_icms_st);
    sgItens.Cells[coValorIcmsSt, i + 1]   := NFormatN(pNotaItens[i].valor_icms_st);
    sgItens.Cells[coValorIpi, i + 1]      := NFormatN(pNotaItens[i].valor_ipi);
    sgItens.Cells[coOutrasDesp, i + 1]    := NFormatN(pNotaItens[i].valor_total_outras_despesas);
    sgItens.Cells[coDesconto, i + 1]      := NFormatN(pNotaItens[i].valor_total_desconto);
    sgItens.Cells[coPrecoUnitario, i + 1] := NFormatN(pNotaItens[i].preco_unitario, 6);
    sgItens.Cells[coQtdeEntrada, i + 1]   := NFormatNEstoque(pNotaItens[i].quantidade);
    sgItens.Cells[coPercIcms, i + 1]      := NFormatN(pNotaItens[i].percentual_icms);
    sgItens.Cells[coPercIcmsSt, i + 1]    := NFormatN(pNotaItens[i].percentual_icms_st);
    sgItens.Cells[coPercIPI, i + 1]       := NFormatN(pNotaItens[i].percentual_ipi);
    sgItens.Cells[coPesoUnit, i + 1]      := NFormatN(pNotaItens[i].Peso);
    sgItens.Cells[coTipoControleEst, i + 1]  := pNotaItens[i].TipoControleEstoque;
    sgItens.Cells[coMultiploCompra, i + 1]   := NFormatN(pNotaItens[i].MultiploCompra);
    sgItens.Cells[coExigirDataFabr, i + 1]   := pNotaItens[i].ExigirDataFabricacaoLote;
    sgItens.Cells[coExigirDataVencto, i + 1] := pNotaItens[i].ExigirDataVencimentoLote;

    _Biblioteca.AddNoVetorSemRepetir(vItensIds, pNotaItens[i].item_id);
  end;
  sgItens.SetLinhasGridPorTamanhoVetor( Length(pNotaItens) );

  FLotes := _EntradasNfItensLotes.BuscarEntradasNfItensLotesComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('ITL.ITEM_ID', vItensIds));
  // Limpando a quantidade pois utilizaremos ela para definir quais os lotes que est�o sendo devolvidos ao gravar!
  for i := Low(FLotes) to High(FLotes) do
    FLotes[i].Quantidade := 0;
end;

procedure TFormDevolucaoEntradaNotaFiscal.sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
begin
  inherited;
  if ACol <> coQtdeDevolver then
    Exit;

  TextCell := NFormatNEstoque(SFormatDouble(TextCell));
  if TextCell = '' then begin
    sgItens.Cells[coQtdeDevolver, ARow] := '';
    AjustarTotalDevolucao(ARow);
    Exit;
  end;

  if not ValidarMultiplo(SFormatDouble(sgItens.Cells[ACol, sgItens.Row]), SFormatDouble(sgItens.Cells[coMultiplo, sgItens.Row])) then begin
    SetarFoco(sgItens);

    sgItens.Cells[coQtdeDevolver, ARow] := '';
    TextCell := '';

    AjustarTotalDevolucao(ARow);
    Exit;
  end;

  if SFormatCurr(TextCell) > SFormatCurr(sgItens.Cells[coQtdeNota, ARow]) then begin
    _Biblioteca.Exclamar('A quantidade a ser devolvida n�o pode ser maior que o saldo da entrada!');

    sgItens.Cells[coQtdeDevolver, ARow] := '';
    TextCell := '';

    AjustarTotalDevolucao(ARow);
    Exit;
  end;

  AjustarTotalDevolucao(ARow);
end;

procedure TFormDevolucaoEntradaNotaFiscal.sgItensClick(Sender: TObject);
begin
  inherited;
  FrBuscarLotes.setTipoControleEstoque(
    sgItens.Cells[coTipoControleEst, sgItens.Row],
    sgItens.Cells[coExigirDataFabr, sgItens.Row] = 'S',
    sgItens.Cells[coExigirDataVencto, sgItens.Row] = 'S'
  );

  FrBuscarLotes.ProdutoId      := SFormatInt( sgItens.Cells[coProdutoId, sgItens.Row] );
  FrBuscarLotes.MultiploCompra := SFormatDouble( sgItens.Cells[coMultiploCompra, sgItens.Row] );
  FrBuscarLotes.Lotes          := FLotes;
end;

procedure TFormDevolucaoEntradaNotaFiscal.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    coNomeProduto,
    coMarca,
    coNomeLocal]
  then
    vAlinhamento := taLeftJustify
  else if ACol in[
    coUnidadeNota,
    coUnidadeAltis]
  then
    vAlinhamento := taCenter
  else
    vAlinhamento := taRightJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormDevolucaoEntradaNotaFiscal.sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;

  if ARow = 0 then
    Exit;

  if ACol = coQtdeDevolver then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end;
end;

procedure TFormDevolucaoEntradaNotaFiscal.sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  if ACol = coQtdeDevolver then
    sgItens.Options := sgItens.Options + [goEditing]
  else
    sgItens.Options := sgItens.Options - [goEditing];
end;

procedure TFormDevolucaoEntradaNotaFiscal.VerificarRegistro(Sender: TObject);
var
  i: Integer;

  procedure ValidarExisteDiferencaLotes(
    pProdutoId: Integer;
    pNomeProduto: string;
    pLinha: Integer;
    pQuantidadeEntrada: Double;
    pExigirDataFabricao: Boolean;
    pExigirDataVencimento: Boolean
  );
  var
    j: Integer;
    vQtde: Double;
  begin
    vQtde := 0;

    for j := Low(FLotes) to High(FLotes) do begin
      if pProdutoId <> FLotes[j].ProdutoId then
        Continue;

      if pExigirDataFabricao and (DFormatN(FLotes[j].DataFabricacao) = '') then begin
        _Biblioteca.Exclamar('A data de fabrica��o do lote para o produto ' + pNomeProduto + ' n�o foi informado corretamente!');
        sgItens.Row := pLinha;
        sgItens.Col := coQtdeDevolver;
        SetarFoco(sgItens);
        Abort;
      end;

      if pExigirDataVencimento and (DFormatN(FLotes[j].DataVencimento) = '') then begin
        _Biblioteca.Exclamar('A data de vencimento do lote para o produto ' + pNomeProduto + ' n�o foi informado corretamente!');
        sgItens.Row := pLinha;
        sgItens.Col := coQtdeDevolver;
        SetarFoco(sgItens);
        Abort;
      end;

      vQtde := vQtde + FLotes[j].Quantidade;
    end;

    if NFormatNEstoque(pQuantidadeEntrada) <> NFormatNEstoque(vQtde) then begin
      _Biblioteca.Exclamar('Existe diferen�a de quantidades nos lotes informados para o produto ' + pNomeProduto);
      sgItens.Row := pLinha;
      sgItens.Col := coQtdeDevolver;
      SetarFoco(sgItens);
      Abort;
    end;
  end;

begin
  inherited;
  FrBuscarLotesExit(Sender);

  if eMotivoDevolucao.Trim = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar o motivo da devolu��o!');
    SetarFoco(eMotivoDevolucao);
    Abort;
  end;

  for i := 1 to sgItens.RowCount -1 do begin
    if SFormatCurr(sgItens.Cells[coQtdeDevolver, i]) >  SFormatCurr(sgItens.Cells[coQtdeNota, i]) then begin
      _Biblioteca.Exclamar('A quantidade a ser devolvida n�o pode ser maior que o saldo da entrada!');
      sgItens.Col := coQtdeDevolver;
      sgItens.Row := i;
      SetarFoco(sgItens);
      Abort;
    end;

    if Em(sgItens.Cells[coTipoControleEst, i], ['L', 'G', 'P']) then begin
      ValidarExisteDiferencaLotes(
        SFormatInt(sgItens.Cells[coProdutoId, i]),
        sgItens.Cells[coNomeProduto, i],
        i,
        SFormatDouble(sgItens.Cells[coQtdeDevolver, i]),
        sgItens.Cells[coExigirDataFabr, i] = 'S',
        sgItens.Cells[coExigirDataVencto, i] = 'S'
      );
    end;
  end;
end;

end.
