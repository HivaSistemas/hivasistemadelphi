inherited FormParametroDevolucaoAposPrazo: TFormParametroDevolucaoAposPrazo
  Caption = 'Par'#226'metro devolu'#231#227'o ap'#243's o prazo'
  ClientHeight = 340
  ClientWidth = 504
  ExplicitWidth = 510
  ExplicitHeight = 369
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 340
    ExplicitHeight = 340
  end
  inline FrEmpresa: TFrEmpresas
    Left = 125
    Top = 2
    Width = 284
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 125
    ExplicitTop = 2
    ExplicitWidth = 284
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 259
      Height = 23
      ExplicitWidth = 259
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 284
      ExplicitWidth = 284
      inherited lbNomePesquisa: TLabel
        Width = 47
        Caption = 'Empresa'
        ExplicitWidth = 47
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 179
        ExplicitLeft = 179
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 259
      Height = 24
      ExplicitLeft = 259
      ExplicitHeight = 24
      inherited sbPesquisa: TSpeedButton
        Visible = False
      end
    end
  end
  object sgFaixas: TGridLuka
    Left = 125
    Top = 88
    Width = 369
    Height = 247
    Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    ColCount = 4
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goEditing]
    TabOrder = 2
    OnDrawCell = sgFaixasDrawCell
    OnKeyDown = sgFaixasKeyDown
    OnKeyPress = NumerosVirgula
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = clWindow
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Faixa'
      'Valor inicial'
      'Valor final'
      '% Desconto')
    Grid3D = False
    RealColCount = 31
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      61
      119
      83
      81)
  end
  object StaticTextLuka5: TStaticTextLuka
    Left = 125
    Top = 65
    Width = 367
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Faixa de dias de devolu'#231#227'o'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 3
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
end
