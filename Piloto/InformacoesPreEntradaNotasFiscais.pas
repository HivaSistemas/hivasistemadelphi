unit InformacoesPreEntradaNotasFiscais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Biblioteca, _PreEntradasNotasFiscais,
  Vcl.ExtCtrls, StaticTextLuka, Vcl.StdCtrls, EditLuka, SpeedButtonLuka, _Sessao,
  Vcl.Mask, EditLukaData, Vcl.ComCtrls, Vcl.OleCtrls, SHDocVw;

type
  TFormInformacoesPreEntradaNotasFiscais = class(TFormHerancaFinalizar)
    lb1: TLabel;
    ePreEntradaId: TEditLuka;
    eCNPJEmitente: TEditLuka;
    lb2: TLabel;
    lb13: TLabel;
    eRazaoSocialEmitente: TEditLuka;
    stStatusAltis: TStaticText;
    st1: TStaticTextLuka;
    lb11: TLabel;
    eNumeroNota: TEditLuka;
    lb12: TLabel;
    eSerieNota: TEditLuka;
    pcDados: TPageControl;
    tsPrincipais: TTabSheet;
    tsXML: TTabSheet;
    lb5: TLabel;
    eUsuarioCienciaOperacao: TEditLuka;
    eDataHoraCiencia: TEditLukaData;
    lb6: TLabel;
    lb3: TLabel;
    eOperacaoNaoRealizada: TEditLuka;
    eDataHoraOperacaoNaoRealizada: TEditLukaData;
    lb4: TLabel;
    lb7: TLabel;
    eUsuarioDesconhecimento: TEditLuka;
    eDataHoraDesconhecimento: TEditLukaData;
    lb8: TLabel;
    lb9: TLabel;
    eUsuarioConfirmacao: TEditLuka;
    eDataHoraConfirmacao: TEditLukaData;
    lb10: TLabel;
    st2: TStaticTextLuka;
    stStatusSefaz: TStaticText;
    lb14: TLabel;
    eEntradaId: TEditLuka;
    sbInformaocoesEntrada: TSpeedButtonLuka;
    lb15: TLabel;
    eEmpresa: TEditLuka;
    lb16: TLabel;
    eDataHoraEmissao: TEditLukaData;
    lb17: TLabel;
    eValorTotalNota: TEditLuka;
    wbXML: TWebBrowser;
    lb18: TLabel;
    eMotivoOperacaoNaoRealizada: TEditLuka;
    lb19: TLabel;
    eChaveAcessoNFe: TEditLuka;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure Informar(pPreEntradaId: Integer);

implementation

{$R *.dfm}

procedure Informar(pPreEntradaId: Integer);
var
  vForm: TFormInformacoesPreEntradaNotasFiscais;
  vPreEntrada: TArray<RecPreEntradasNotasFiscais>;
begin
  if pPreEntradaId = 0 then
    Exit;

  vPreEntrada := _PreEntradasNotasFiscais.BuscarPreEntradasNotasFiscais(Sessao.getConexaoBanco, 0, [pPreEntradaId]);
  if vPreEntrada = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  vForm := TFormInformacoesPreEntradaNotasFiscais.Create(Application);

  vForm.ePreEntradaId.AsInt         := vPreEntrada[0].PreEntradaId;
  vForm.eCNPJEmitente.Text          := vPreEntrada[0].Cnpj;
  vForm.eRazaoSocialEmitente.Text   := vPreEntrada[0].RazaoSocial;
  vForm.eNumeroNota.AsInt           := vPreEntrada[0].NumeroNota;
  vForm.eSerieNota.Text             := vPreEntrada[0].SerieNota;
  vForm.stStatusAltis.Caption       := vPreEntrada[0].StatusAnalitico;
  vForm.stStatusSefaz.Caption       := vPreEntrada[0].StatusNotaAnalitico;
  vForm.eEntradaId.SetInformacao(vPreEntrada[0].EntradaId);

  vForm.eEmpresa.SetInformacao(vPreEntrada[0].EmpresaId, '');
  vForm.eDataHoraEmissao.AsDataHora := vPreEntrada[0].DataHoraEmissao;

  vForm.eValorTotalNota.AsDouble    := vPreEntrada[0].ValorTotalNota;

  vForm.eUsuarioCienciaOperacao.SetInformacao(vPreEntrada[0].UsuarioManifestacaoId, '');
  vForm.eDataHoraCiencia.AsDataHora := vPreEntrada[0].DataHoraManifestacao;

  vForm.eUsuarioDesconhecimento.SetInformacao(vPreEntrada[0].UsuarioDesconhecimentoId, '');
  vForm.eDataHoraDesconhecimento.AsDataHora := vPreEntrada[0].DataHoraDesconhecimento;

  vForm.eOperacaoNaoRealizada.SetInformacao(vPreEntrada[0].UsuarioOperNaoRealizadaId, '');
  vForm.eDataHoraOperacaoNaoRealizada.AsDataHora := vPreEntrada[0].DataHoraOperNaoRealizada;
  vForm.eMotivoOperacaoNaoRealizada.Text         := vPreEntrada[0].MotivoOperacaoNaoRealzada;

  vForm.eUsuarioConfirmacao.SetInformacao(vPreEntrada[0].UsuarioConfirmacaoId, '');
  vForm.eDataHoraConfirmacao.AsDataHora := vPreEntrada[0].DataHoraConfirmacao;

  vForm.eChaveAcessoNFe.Text := vPreEntrada[0].ChaveAcessoNfe;

  _Biblioteca.salvarStringEmAquivo(vPreEntrada[0].XmlTexto, Sessao.getCaminhoPastaTemporaria + '\xml_temporario.xml');
  vForm.wbXml.Navigate(Sessao.getCaminhoPastaTemporaria + '\xml_temporario.xml');

  vForm.ShowModal;

  vForm.Free;
end;

procedure TFormInformacoesPreEntradaNotasFiscais.FormShow(Sender: TObject);
begin
  inherited;
  pcDados.ActivePageIndex := 0;
end;

end.
