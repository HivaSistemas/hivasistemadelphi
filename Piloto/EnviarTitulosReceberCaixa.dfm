inherited FormEnviarTitulosReceberCaixa: TFormEnviarTitulosReceberCaixa
  Caption = 'Envio de t'#237'tulos a receber para o ca'#237'xa'
  ClientHeight = 443
  ClientWidth = 808
  ExplicitWidth = 814
  ExplicitHeight = 472
  PixelsPerInch = 96
  TextHeight = 14
  object lb22: TLabel [0]
    Left = 314
    Top = 138
    Width = 66
    Height = 14
    Caption = 'Valor t'#237'tulos'
  end
  object lb23: TLabel [1]
    Left = 404
    Top = 138
    Width = 57
    Height = 14
    Caption = 'Valor juros'
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lb1: TLabel [2]
    Left = 494
    Top = 138
    Width = 65
    Height = 14
    Caption = 'Valor total'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  inherited pnOpcoes: TPanel
    Top = 406
    Width = 808
    ExplicitTop = 406
    ExplicitWidth = 808
  end
  object eValorTitulos: TEditLuka
    Left = 314
    Top = 153
    Width = 85
    Height = 22
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 1
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorJuros: TEditLuka
    Left = 404
    Top = 153
    Width = 85
    Height = 22
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 2
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorTotal: TEditLuka
    Left = 494
    Top = 153
    Width = 130
    Height = 22
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 3
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object sgTitulos: TGridLuka
    Left = 2
    Top = 2
    Width = 803
    Height = 135
    Align = alCustom
    ColCount = 9
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
    TabOrder = 4
    OnDblClick = sgTitulosDblClick
    OnDrawCell = sgTitulosDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Documento'
      'Cliente'
      'Parcela'
      'Valor'
      'Juros'
      'Valor total'
      'Data Vencto.'
      'Dias atraso'
      'Tipo de cobran'#231'a')
    OnGetCellColor = sgTitulosGetCellColor
    Grid3D = False
    RealColCount = 15
    OrdenarOnClick = True
    AtivarPopUpSelecao = False
    ColWidths = (
      89
      153
      48
      62
      61
      79
      84
      68
      136)
  end
  inline FrPagamento: TFrPagamentoFinanceiro
    Left = 4
    Top = 142
    Width = 303
    Height = 261
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 5
    TabStop = True
    ExplicitLeft = 4
    ExplicitTop = 142
    ExplicitHeight = 261
    inherited grpFechamento: TGroupBox
      Height = 261
      ExplicitHeight = 261
      inherited lbllb7: TLabel
        Width = 9
        Height = 14
        ExplicitWidth = 9
        ExplicitHeight = 14
      end
      inherited eValorDinheiro: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorCheque: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorCartaoDebito: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorCobranca: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorCredito: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited stPagamento: TStaticText
        Color = 38619
      end
      inherited eValorDiferencaPagamentos: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorDesconto: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited ePercentualDesconto: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorJuros: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorCartaoCredito: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited stValorTotal: TStaticText
        Color = 38619
      end
      inherited eValorMulta: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
    end
  end
end
