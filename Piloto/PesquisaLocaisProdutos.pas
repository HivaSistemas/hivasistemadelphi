unit PesquisaLocaisProdutos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _RecordsCadastros, _Sessao,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, System.Math, _LocaisProdutos, _Biblioteca,
  Vcl.ExtCtrls;

type
  TFormPesquisaLocaisProdutos = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
  protected
    procedure BuscarRegistros; override;
  end;

function PesquisarLocalProduto: TObject;

implementation

{$R *.dfm}

const
  cp_nome  = 2;
  cp_tipo  = 3;
  cp_ativo = 4;

{ TFormPesquisaLocaisProdutos }

function PesquisarLocalProduto: TObject;
var
  r: TObject;
begin
  r := _HerancaPesquisas.Pesquisar(TFormPesquisaLocaisProdutos, _LocaisProdutos.GetFiltros);
  if r = nil then
    Result := nil
  else
    Result := RecLocaisProdutos(r);
end;

procedure TFormPesquisaLocaisProdutos.BuscarRegistros;
var
  i: Integer;
  locais: TArray<RecLocaisProdutos>;
begin
  inherited;

  locais :=
    _LocaisProdutos.BuscarLocaisProdutos(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text]
    );

  if locais = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(locais);

  for i := Low(locais) to High(locais) do begin
    sgPesquisa.Cells[coSelecionado, i + 1] := 'N�o';
    sgPesquisa.Cells[coCodigo, i + 1] := IntToStr(locais[i].local_id);
    sgPesquisa.Cells[cp_nome, i + 1] := locais[i].nome;
    sgPesquisa.Cells[cp_tipo, i + 1] := locais[i].tipo_local_analitico;
    sgPesquisa.Cells[cp_ativo, i + 1] := locais[i].ativo;
  end;

  sgPesquisa.RowCount := IfThen(Length(locais) = 1, 2, High(locais) + 2);
  sgPesquisa.SetFocus;
end;

procedure TFormPesquisaLocaisProdutos.sgPesquisaDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  alinhamento: TAlignment;
begin
  inherited;

  if _Biblioteca.Em(ACol, [coSelecionado, cp_ativo]) then
    alinhamento := taCenter
  else if ACol = coCodigo then
    alinhamento := taRightJustify
  else
    alinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], alinhamento, Rect);
end;

end.
