unit FrameFuncionarios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _RecordsCadastros,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Biblioteca, System.Math, _Funcionarios, _Sessao,
  PesquisaFuncionarios, Vcl.Buttons, RadioGroupLuka, Vcl.Menus;

type
  TFrFuncionarios = class(TFrameHenrancaPesquisas)
    ckBuscarSomenteAtivos: TCheckBox;
    ckBuscarTodasEmpresas: TCheckBox;
    ckVendedor: TCheckBox;
    ckMotorista: TCheckBox;
    ckComprador: TCheckBox;
    ckCaixa: TCheckBox;
    ckAjudante: TCheckBox;
  public
    constructor Create(AOwner: TComponent); override;
    function GetFuncionario(pLinha: Integer = -1): RecFuncionarios;
//    procedure SetFuncao(pFuncaoId: Integer);
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarDiretoTodos: TArray<TObject>; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrFuncionarios }

function TFrFuncionarios.AdicionarDireto: TObject;
var
  vFuncionarios: TArray<RecFuncionarios>;
begin
  vFuncionarios :=
    _Funcionarios.BuscarFuncionarios(
      Sessao.getConexaoBanco,
      0,
      [FChaveDigitada],
      ckSomenteAtivos.Checked,
      ckVendedor.Checked,
      ckCaixa.Checked,
      ckComprador.Checked,
      ckAjudante.Checked
    );

  if vFuncionarios = nil then
    Result := nil
  else
    Result := vFuncionarios[0];
end;

function TFrFuncionarios.AdicionarDiretoTodos: TArray<TObject>;
begin
  Result := TArray<TObject>(_Funcionarios.BuscarFuncionariosComando(Sessao.getConexaoBanco, 'where 1 = 1 ' + FChaveDigitada));
end;

function TFrFuncionarios.AdicionarPesquisando: TObject;
begin
  Result :=
    PesquisaFuncionarios.PesquisarFuncionario(
      ckVendedor.Checked,
      ckCaixa.Checked,
      ckComprador.Checked,
      ckMotorista.Checked,
      Sessao.getEmpresaLogada.EmpresaId,
      ckBuscarSomenteAtivos.Checked,
      ckBuscarTodasEmpresas.Checked,
      ckAjudante.Checked
    );
end;

constructor TFrFuncionarios.Create(AOwner: TComponent);
begin
  inherited;
  FColunaPesquisa := 'FUN.FUNCIONARIO_ID';
end;

function TFrFuncionarios.GetFuncionario(pLinha: Integer): RecFuncionarios;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecFuncionarios(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrFuncionarios.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecFuncionarios(FDados[i]).funcionario_id = RecFuncionarios(pSender).funcionario_id then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrFuncionarios.MontarGrid;
var
  i: Integer;
  pSender: RecFuncionarios;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      pSender := RecFuncionarios(FDados[i]);
      AAdd([IntToStr(pSender.funcionario_id), pSender.nome]);
    end;
  end;
end;

//procedure TFrFuncionarios.SetFuncao(pFuncaoId: Integer);
//begin
//
//end;

end.
