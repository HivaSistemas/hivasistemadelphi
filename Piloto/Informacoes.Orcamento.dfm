inherited FormInformacoesOrcamento: TFormInformacoesOrcamento
  Caption = 'Informa'#231#245'es do pedido'
  ClientHeight = 572
  ClientWidth = 844
  OnShow = FormShow
  ExplicitTop = -111
  ExplicitWidth = 850
  ExplicitHeight = 601
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 3
    Top = 1
    Width = 54
    Height = 14
    Caption = 'N'#186' Pedido'
  end
  object lb2: TLabel [1]
    Left = 183
    Top = 1
    Width = 39
    Height = 14
    Caption = 'Cliente'
  end
  object lb12: TLabel [2]
    Left = 420
    Top = 1
    Width = 52
    Height = 14
    Caption = 'Vendedor'
  end
  object lb13: TLabel [3]
    Left = 592
    Top = 1
    Width = 47
    Height = 14
    Caption = 'Empresa'
  end
  object lb24: TLabel [4]
    Left = 95
    Top = 1
    Width = 61
    Height = 14
    Caption = 'Acumulado'
  end
  object sbInformacoesAcumulado: TSpeedButtonLuka [5]
    Left = 157
    Top = 19
    Width = 18
    Height = 18
    Hint = 'Abrir informa'#231#245'es do acumulado'
    Flat = True
    NumGlyphs = 2
    OnClick = sbInformacoesAcumuladoClick
    TagImagem = 13
    PedirCertificacao = False
    PermitirAutOutroUsuario = False
  end
  inherited pnOpcoes: TPanel
    Top = 535
    Width = 844
    ExplicitTop = 535
    ExplicitWidth = 844
    object sbLogs: TSpeedButtonLuka
      Left = 771
      Top = -2
      Width = 69
      Height = 35
      Caption = '&Logs'
      Flat = True
      Visible = False
      OnClick = sbLogsClick
      TagImagem = 8
      PedirCertificacao = False
      PermitirAutOutroUsuario = False
    end
  end
  object eOrcamentoId: TEditLuka
    Left = 3
    Top = 15
    Width = 76
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 1
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eCliente: TEditLuka
    Left = 183
    Top = 15
    Width = 232
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 2
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eVendedor: TEditLuka
    Left = 420
    Top = 15
    Width = 164
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 3
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eEmpresa: TEditLuka
    Left = 592
    Top = 15
    Width = 244
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 4
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object pcDados: TPageControlAltis
    Left = -2
    Top = 37
    Width = 844
    Height = 498
    ActivePage = tsGerais
    TabOrder = 5
    CarregouAba = False
    object tsGerais: TTabSheet
      Caption = 'Gerais'
      object lb3: TLabel
        Left = 225
        Top = 43
        Width = 76
        Height = 14
        Caption = 'Data cadastro'
      end
      object lb4: TLabel
        Left = 191
        Top = 88
        Width = 128
        Height = 14
        Caption = 'Data/hora recebimento'
      end
      object lb5: TLabel
        Left = 397
        Top = 152
        Width = 27
        Height = 14
        Caption = 'Total'
      end
      object lb6: TLabel
        Left = 1
        Top = 151
        Width = 79
        Height = 14
        Caption = 'Total produtos'
      end
      object lb7: TLabel
        Left = 97
        Top = 151
        Width = 92
        Height = 14
        Caption = 'Outras despesas'
      end
      object lb8: TLabel
        Left = 203
        Top = 151
        Width = 51
        Height = 14
        Caption = 'Desconto'
      end
      object lb9: TLabel
        Left = 278
        Top = 169
        Width = 10
        Height = 14
        Caption = '+'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lb10: TLabel
        Left = 193
        Top = 168
        Width = 5
        Height = 14
        Caption = '-'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lb11: TLabel
        Left = 378
        Top = 168
        Width = 13
        Height = 19
        Caption = '='
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lb14: TLabel
        Left = 166
        Top = 219
        Width = 51
        Height = 14
        Caption = 'Overprice'
        Visible = False
      end
      object lb15: TLabel
        Left = 2
        Top = 44
        Width = 110
        Height = 14
        Caption = 'Usu'#225'rio de inser'#231#227'o'
      end
      object lb16: TLabel
        Left = 2
        Top = 88
        Width = 115
        Height = 14
        Caption = 'Usu'#225'rio recebimento'
      end
      object lb17: TLabel
        Left = 328
        Top = 88
        Width = 30
        Height = 14
        Caption = 'Turno'
      end
      object lb18: TLabel
        Left = 2
        Top = 2
        Width = 132
        Height = 14
        Caption = 'Condi'#231#227'o de pagamento'
      end
      object lb23: TLabel
        Left = 290
        Top = 152
        Width = 57
        Height = 14
        Caption = 'Valor frete'
      end
      object sbAlterarEnderecoEntrega: TSpeedButton
        Left = 675
        Top = 364
        Width = 115
        Height = 22
        Caption = 'Alterar end. entrega'
        OnClick = sbAlterarEnderecoEntregaClick
      end
      object sbInformacoesTurno: TSpeedButtonLuka
        Left = 386
        Top = 108
        Width = 18
        Height = 18
        Hint = 'Abrir informa'#231#245'es do turno'
        Flat = True
        NumGlyphs = 2
        OnClick = sbInformacoesTurnoClick
        TagImagem = 13
        PedirCertificacao = False
        PermitirAutOutroUsuario = False
      end
      object sbMixVenda: TSpeedButtonLuka
        Left = 2
        Top = 199
        Width = 114
        Height = 34
        Caption = 'Lucratividade'
        Flat = True
        OnClick = sbMixVendaClick
        TagImagem = 14
        PedirCertificacao = False
        PermitirAutOutroUsuario = False
      end
      object lb25: TLabel
        Left = 281
        Top = 219
        Width = 101
        Height = 14
        Caption = #205'ndice de dedu'#231#227'o'
        Visible = False
      end
      object lb26: TLabel
        Left = 397
        Top = 190
        Width = 93
        Height = 14
        Caption = 'Total em promoc.'
      end
      object Label1: TLabel
        Left = 84
        Top = 169
        Width = 10
        Height = 14
        Caption = '+'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object sbAtualizarContatoCliente: TSpeedButton
        Left = 194
        Top = 439
        Width = 70
        Height = 22
        Caption = 'Atualizar'
        OnClick = sbAtualizarContatoClienteClick
      end
      object eDataCadastro: TEditLukaData
        Left = 225
        Top = 60
        Width = 98
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        ReadOnly = True
        TabOrder = 0
        Text = '  /  /    '
      end
      object eDataRecebimento: TEditLukaData
        Left = 191
        Top = 102
        Width = 133
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999 99:99:99;1;_'
        MaxLength = 19
        ReadOnly = True
        TabOrder = 1
        Text = '  /  /       :  :  '
        TipoData = tdDataHora
      end
      object eTotalOrcamento: TEditLuka
        Left = 397
        Top = 165
        Width = 93
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 2
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eTotalProdutos: TEditLuka
        Left = 1
        Top = 165
        Width = 79
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 3
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eOutrasDespesas: TEditLuka
        Left = 96
        Top = 165
        Width = 93
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorDesconto: TEditLuka
        Left = 203
        Top = 165
        Width = 72
        Height = 23
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = '|'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 5
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eOverprice: TEditLuka
        Left = 166
        Top = 233
        Width = 98
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 6
        Text = '0,00'
        Visible = False
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eUsuarioCadastro: TEditLuka
        Left = 1
        Top = 57
        Width = 218
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 7
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eUsuarioRecebimento: TEditLuka
        Left = 2
        Top = 102
        Width = 183
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 8
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eTurnoId: TEditLuka
        Left = 328
        Top = 102
        Width = 57
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 9
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object stSPC: TStaticText
        Left = 2
        Top = 130
        Width = 545
        Height = 15
        Alignment = taCenter
        AutoSize = False
        Caption = 'Totais do pedido'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 10
        Transparent = False
      end
      object eCondicaoPagamento: TEditLuka
        Left = 2
        Top = 16
        Width = 340
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 11
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object StaticText1: TStaticText
        Left = 185
        Top = 305
        Width = 173
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Tipo de entrega'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 12
        Transparent = False
      end
      object stTipoEntrega: TStaticText
        Left = 185
        Top = 320
        Width = 173
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Retirar ato'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 13
        Transparent = False
      end
      inline FrEnderecoEntrega: TFrEndereco
        Left = 2
        Top = 348
        Width = 660
        Height = 88
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 14
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 348
        ExplicitHeight = 88
        inherited eCEP: TEditCEPLuka
          Width = 72
          Height = 22
          ExplicitWidth = 72
          ExplicitHeight = 22
        end
        inherited eLogradouro: TEditLuka
          Height = 22
          ExplicitHeight = 22
        end
        inherited eComplemento: TEditLuka
          Height = 22
          ExplicitHeight = 22
        end
        inherited FrBairro: TFrBairros
          Width = 355
          ExplicitWidth = 355
          inherited sgPesquisa: TGridLuka
            Width = 353
            Height = 24
            Align = alNone
            ExplicitWidth = 353
            ExplicitHeight = 24
          end
          inherited PnTitulos: TPanel
            Width = 355
            ExplicitWidth = 355
            inherited lbNomePesquisa: TLabel
              Height = 15
            end
            inherited pnSuprimir: TPanel
              Left = 250
              ExplicitLeft = 250
            end
          end
          inherited pnPesquisa: TPanel
            Left = 330
            Visible = False
            ExplicitLeft = 330
            inherited sbPesquisa: TSpeedButton
              Visible = False
            end
          end
        end
        inherited ckValidarComplemento: TCheckBox
          Width = 137
          Checked = True
          State = cbChecked
          ExplicitWidth = 137
        end
        inherited ckValidarBairro: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited ckValidarCEP: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited ckValidarEndereco: TCheckBox
          Width = 127
          Checked = True
          State = cbChecked
          ExplicitWidth = 127
        end
        inherited ePontoReferencia: TEditLuka
          Height = 22
          ExplicitHeight = 22
        end
        inherited ckValidarPontoReferencia: TCheckBox
          Width = 180
          ExplicitWidth = 180
        end
        inherited eNumero: TEditLuka
          Width = 72
          Height = 22
          ExplicitWidth = 72
          ExplicitHeight = 22
        end
      end
      object eValorFrete: TEditLuka
        Left = 290
        Top = 165
        Width = 85
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 15
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object st1: TStaticText
        Left = 2
        Top = 305
        Width = 173
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Tipo de venda'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 16
        Transparent = False
      end
      object stOrigemVenda: TStaticText
        Left = 2
        Top = 320
        Width = 173
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'PDV'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = 33023
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 17
        Transparent = False
      end
      inline FrFechamento: TFrFechamento
        Left = 544
        Top = 2
        Width = 290
        Height = 312
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 18
        TabStop = True
        ExplicitLeft = 544
        ExplicitTop = 2
        ExplicitHeight = 312
        inherited gbFechamento: TGroupBox
          Height = 312
          Caption = ''
          ExplicitHeight = 312
          inherited lb19: TLabel
            Top = 87
            ExplicitTop = 87
          end
          inherited lb20: TLabel
            Top = 49
            Width = 9
            Height = 14
            ExplicitTop = 49
            ExplicitWidth = 9
            ExplicitHeight = 14
          end
          inherited lb21: TLabel
            Top = 70
            ExplicitTop = 70
          end
          inherited lb22: TLabel
            Top = 50
            ExplicitTop = 50
          end
          inherited lb7: TLabel
            Top = 70
            Width = 9
            Height = 14
            ExplicitTop = 70
            ExplicitWidth = 9
            ExplicitHeight = 14
          end
          inherited lb11: TLabel
            Top = 154
            ExplicitTop = 154
          end
          inherited lb12: TLabel
            Top = 174
            ExplicitTop = 174
          end
          inherited lb13: TLabel
            Top = 194
            ExplicitTop = 194
          end
          inherited lb14: TLabel
            Top = 234
            ExplicitTop = 234
          end
          inherited lb15: TLabel
            Top = 294
            ExplicitTop = 294
          end
          inherited sbBuscarDadosCheques: TSpeedButton
            Top = 169
            Visible = False
            ExplicitTop = 169
          end
          inherited sbBuscarDadosCartoesDebito: TSpeedButton
            Top = 189
            Visible = False
            ExplicitTop = 189
          end
          inherited sbBuscarDadosCobranca: TSpeedButton
            Top = 230
            Visible = False
            ExplicitTop = 230
          end
          inherited lb10: TLabel
            Top = 315
            Visible = False
            ExplicitTop = 315
          end
          inherited lb1: TLabel
            Top = 274
            ExplicitTop = 274
          end
          inherited lb2: TLabel
            Top = 227
            ExplicitTop = 227
          end
          inherited sbBuscarCreditos: TSpeedButton
            Top = 290
            Visible = False
            ExplicitTop = 290
          end
          inherited lb3: TLabel
            Top = 30
            ExplicitTop = 30
          end
          inherited lb4: TLabel
            Top = 214
            ExplicitTop = 214
          end
          inherited sbBuscarDadosCartoesCredito: TSpeedButton
            Top = 210
            Visible = False
            ExplicitTop = 210
          end
          inherited Label1: TLabel
            Top = 254
            ExplicitTop = 254
          end
          inherited sbBuscarDadosCobrancaPix: TSpeedButton
            Top = 250
            Visible = False
            ExplicitTop = 250
          end
          inherited eValorTotalASerPago: TEditLuka
            Top = 82
            ExplicitTop = 82
          end
          inherited ePercentualOutrasDespesas: TEditLuka
            Top = 42
            Height = 22
            ExplicitTop = 42
            ExplicitHeight = 22
          end
          inherited eValorDesconto: TEditLuka
            Top = 62
            Height = 22
            ExplicitTop = 62
            ExplicitHeight = 22
          end
          inherited eValorOutrasDespesas: TEditLuka
            Top = 42
            Height = 22
            ExplicitTop = 42
            ExplicitHeight = 22
          end
          inherited ePercentualDesconto: TEditLuka
            Top = 62
            Height = 22
            ExplicitTop = 62
            ExplicitHeight = 22
          end
          inherited eValorDinheiro: TEditLuka
            Top = 147
            Height = 22
            ExplicitTop = 147
            ExplicitHeight = 22
          end
          inherited eValorCheque: TEditLuka
            Top = 167
            Height = 22
            ExplicitTop = 167
            ExplicitHeight = 22
          end
          inherited eValorCartaoDebito: TEditLuka
            Top = 187
            Height = 22
            ExplicitTop = 187
            ExplicitHeight = 22
          end
          inherited eValorCobranca: TEditLuka
            Top = 227
            Height = 22
            ExplicitTop = 227
            ExplicitHeight = 22
          end
          inherited eValorCredito: TEditLuka
            Top = 287
            Height = 22
            ExplicitTop = 287
            ExplicitHeight = 22
          end
          inherited stSPC: TStaticText
            Top = 128
            Caption = 'Formas de pagamento'
            Color = 38619
            ExplicitTop = 128
          end
          inherited eValorTroco: TEditLuka
            Top = 307
            Visible = False
            ExplicitTop = 307
          end
          inherited eValorAcumulativo: TEditLuka
            Top = 267
            Height = 22
            ExplicitTop = 267
            ExplicitHeight = 22
          end
          inherited eValorFinanceira: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eValorFrete: TEditLuka
            Top = 22
            Height = 22
            ExplicitTop = 22
            ExplicitHeight = 22
          end
          inherited eValorCartaoCredito: TEditLuka
            Top = 206
            Height = 22
            ExplicitTop = 206
            ExplicitHeight = 22
          end
          inherited eValorPix: TEditLuka
            Top = 247
            Height = 22
            ExplicitTop = 247
            ExplicitHeight = 22
          end
        end
      end
      object eIndiceDescontoVenda: TEditLuka
        Left = 281
        Top = 233
        Width = 210
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 19
        Visible = False
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      inline FrArquivos: TFrArquivos
        Left = 675
        Top = 407
        Width = 117
        Height = 22
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 20
        TabStop = True
        ExplicitLeft = 675
        ExplicitTop = 407
        ExplicitHeight = 22
        inherited sbArquivos: TSpeedButtonLuka
          Height = 22
          Caption = 'Anexos'
          ExplicitTop = 1
          ExplicitHeight = 35
        end
        inherited tiPiscar: TTimer
          Top = 40
        end
      end
      object eTotalProdutosPromocao: TEditLuka
        Left = 397
        Top = 203
        Width = 93
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 21
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object st2: TStaticText
        Left = 366
        Top = 305
        Width = 128
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Receber na entrega?'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 22
        Transparent = False
      end
      object stReceberNaEntrega: TStaticTextLuka
        Left = 366
        Top = 320
        Width = 128
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Sim'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 23
        Transparent = False
        AsInt = 0
        TipoCampo = tcSimNao
        CasasDecimais = 0
      end
      object StaticText2: TStaticText
        Left = 2
        Top = 283
        Width = 540
        Height = 15
        Alignment = taCenter
        AutoSize = False
        Caption = 'Dados de entrega'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 24
        Transparent = False
      end
      object st3: TStaticText
        Left = 359
        Top = 6
        Width = 176
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Situa'#231#227'o do pedido'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 25
        Transparent = False
      end
      object stStatus: TStaticText
        Left = 359
        Top = 21
        Width = 176
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Recebido'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 26
        Transparent = False
      end
      object ckAguardandoContatoCliente: TCheckBoxLuka
        Left = 5
        Top = 439
        Width = 188
        Height = 22
        Caption = 'Aguardando contato do cliente'
        TabOrder = 27
        WordWrap = True
        CheckedStr = 'N'
      end
    end
    object tsItens: TTabSheet
      Caption = 'Produtos'
      ImageIndex = 1
      object sgProdutos: TGridLuka
        Left = 0
        Top = 0
        Width = 836
        Height = 469
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 13
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        PopupMenu = pmProdutos
        TabOrder = 0
        OnDblClick = sgProdutosDblClick
        OnDrawCell = sgProdutosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Produto'
          'Nome'
          'Marca'
          'Tipo pr.utilizado'
          'Pre'#231'o unit.'
          'Quantidade'
          'Und.'
          'Valor desc.'
          '% Desc.manual'
          'Vlr.desc.manual'
          'Outras desp.'
          'Valor L'#237'quido'
          'Valor frete')
        OnGetCellColor = sgProdutosGetCellColor
        Grid3D = False
        RealColCount = 13
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          51
          244
          116
          133
          65
          71
          37
          67
          89
          94
          77
          80
          64)
      end
    end
    object tsFinanceiro: TTabSheet
      Caption = 'Financeiro'
      ImageIndex = 8
      OnShow = tsFinanceiroShow
      object sgReceber: TGridLuka
        Left = 0
        Top = 0
        Width = 836
        Height = 469
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 8
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        PopupMenu = pmFinanceiro
        TabOrder = 0
        OnDblClick = sgReceberDblClick
        OnDrawCell = sgReceberDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Receber'
          'Tipo cobran'#231'a'
          'Status'
          'Documento'
          'Data vencimento'
          'Valor documento'
          'Parcela'
          'Nr.Parc.')
        OnGetCellColor = sgReceberGetCellColor
        Grid3D = False
        RealColCount = 10
        AtivarPopUpSelecao = False
        ColWidths = (
          54
          241
          74
          109
          101
          103
          53
          64)
      end
    end
    object tsCartoes: TTabSheet
      Caption = 'Cart'#245'es'
      ImageIndex = 9
      OnShow = tsCartoesShow
      object sgCartoes: TGridLuka
        Left = 0
        Top = 0
        Width = 836
        Height = 469
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 6
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        PopupMenu = pmSgCartoes
        TabOrder = 0
        OnDrawCell = sgCartoesDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Tipo de cobran'#231'a'
          'Valor'
          'C'#243'd. autoriza'#231#227'o'
          'NSU'
          'N'#250'mero cart'#227'o'
          'Tipo recebimento')
        Grid3D = False
        RealColCount = 10
        AtivarPopUpSelecao = False
        ColWidths = (
          265
          66
          115
          78
          135
          105)
      end
    end
    object tsObservacoes: TTabSheet
      Caption = 'Observa'#231#245'es'
      ImageIndex = 7
      object lb19: TLabel
        Left = 3
        Top = -1
        Width = 124
        Height = 14
        Caption = 'Obseva'#231#245'es do pedido'
      end
      object lb20: TLabel
        Left = 3
        Top = 169
        Width = 162
        Height = 14
        Caption = 'Obseva'#231#245'es para a expedi'#231#227'o'
      end
      object lb21: TLabel
        Left = 3
        Top = 84
        Width = 221
        Height = 14
        Caption = 'Obseva'#231#245'es para a nota fiscal eletr'#244'nica'
      end
      object sbAdicionarObservacaoCaixa: TSpeedButton
        Left = 588
        Top = 54
        Width = 101
        Height = 28
        Caption = 'Adic. observa'#231#227'o'
        OnClick = sbAdicionarObservacaoCaixaClick
      end
      object sbAdicionarObservacaoNFe: TSpeedButton
        Left = 588
        Top = 139
        Width = 101
        Height = 28
        Caption = 'Adic. observa'#231#227'o'
        OnClick = sbAdicionarObservacaoCaixaClick
      end
      object sbAdicionarObservacaoExpedicao: TSpeedButton
        Left = 588
        Top = 224
        Width = 101
        Height = 28
        Caption = 'Adic. observa'#231#227'o'
        OnClick = sbAdicionarObservacaoCaixaClick
      end
      object eObservacoesCaixa: TMemo
        Left = 3
        Top = 13
        Width = 584
        Height = 69
        MaxLength = 800
        ReadOnly = True
        TabOrder = 0
      end
      object eObservacoesExpedicao: TMemo
        Left = 3
        Top = 183
        Width = 584
        Height = 69
        MaxLength = 800
        ReadOnly = True
        TabOrder = 1
      end
      object eObservacoesNotaFiscalEletronica: TMemo
        Left = 3
        Top = 98
        Width = 584
        Height = 69
        MaxLength = 800
        ReadOnly = True
        TabOrder = 2
      end
    end
    object tsBloqueios: TTabSheet
      Caption = 'Bloqueios'
      ImageIndex = 2
      object sgItensBloqueios: TGridLuka
        Left = 0
        Top = 0
        Width = 836
        Height = 469
        Align = alClient
        ColCount = 4
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect, goFixedRowClick]
        TabOrder = 0
        OnDrawCell = sgItensBloqueiosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Item'
          'Descri'#231#227'o'
          'Data libera'#231#227'o'
          'Usuario libera'#231#227'o')
        Grid3D = False
        RealColCount = 15
        OrdenarOnClick = True
        AtivarPopUpSelecao = False
        ColWidths = (
          36
          483
          93
          166)
      end
    end
    object tsTramites: TTabSheet
      Caption = 'Tr'#226'mites'
      ImageIndex = 5
      OnShow = tsTramitesShow
      object sgTramites: TGridLuka
        Left = 0
        Top = 0
        Width = 836
        Height = 469
        Align = alClient
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        TabOrder = 0
        OnDrawCell = sgTramitesDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Data/hora tr'#226'mite'
          'Tipo'
          'Valor'
          'Usu'#225'rio'
          'Esta'#231#227'o')
        Grid3D = False
        RealColCount = 10
        AtivarPopUpSelecao = False
        ColWidths = (
          141
          166
          77
          198
          163)
      end
    end
    object tsDevolucoes: TTabSheet
      Caption = 'Devolu'#231#245'es'
      ImageIndex = 6
      OnShow = tsDevolucoesShow
      object sgDevolucoes: TGridLuka
        Left = 0
        Top = 0
        Width = 836
        Height = 469
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 4
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        PopupMenu = pmsgEntregas
        TabOrder = 0
        OnDblClick = sgDevolucoesDblClick
        OnDrawCell = sgDevolucoesDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Devolu'#231#227'o'
          'Data/hora devolu'#231#227'o'
          'Empresa'
          'Valor devolu'#231#227'o')
        Grid3D = False
        RealColCount = 10
        AtivarPopUpSelecao = False
        ColWidths = (
          67
          124
          178
          101)
      end
    end
    object tsEntregas: TTabSheet
      Caption = 'Log'#237'stica'
      ImageIndex = 3
      OnShow = tsEntregasShow
      object pcEntregas: TPageControlAltis
        Left = 0
        Top = 0
        Width = 836
        Height = 469
        ActivePage = tsRetirarAto
        Align = alClient
        TabOrder = 0
        CarregouAba = False
        object tsRetirarAto: TTabSheet
          Caption = 'Retiradas'
          OnShow = tsRetirarAtoShow
          object sgRetirados: TGridLuka
            Left = 0
            Top = 0
            Width = 828
            Height = 440
            Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
            Align = alClient
            ColCount = 7
            DefaultRowHeight = 19
            DrawingStyle = gdsGradient
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
            PopupMenu = pmsgRetiradas
            TabOrder = 0
            OnDblClick = sgRetiradosDblClick
            OnDrawCell = sgRetiradosDrawCell
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 38619
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clActiveCaption
            HCol.Strings = (
              'Retirada'
              'Empresa'
              'Data/hora cadastro'
              'Nome pessoa retirou'
              'Conf.?'
              'Usu'#225'rio confirma'#231#227'o'
              'Data/hora confirma'#231#227'o')
            OnGetCellColor = sgRetiradosGetCellColor
            Grid3D = False
            RealColCount = 10
            AtivarPopUpSelecao = False
            ColWidths = (
              57
              148
              134
              122
              47
              153
              140)
          end
        end
        object tsEntregues: TTabSheet
          Caption = 'Entregas'
          ImageIndex = 1
          OnShow = tsEntreguesShow
          object sgEntregas: TGridLuka
            Left = 0
            Top = 0
            Width = 828
            Height = 440
            Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
            Align = alClient
            ColCount = 6
            DefaultRowHeight = 19
            DrawingStyle = gdsGradient
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
            PopupMenu = pmsgEntregas
            TabOrder = 0
            OnDblClick = sgEntregasDblClick
            OnDrawCell = sgEntregasDrawCell
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 38619
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clActiveCaption
            HCol.Strings = (
              'Entrega'
              'Empresa'
              'Local'
              'Data/hora cadastro'
              'Status'
              'Data/hora entrega')
            Grid3D = False
            RealColCount = 10
            AtivarPopUpSelecao = False
            ColWidths = (
              57
              206
              161
              138
              121
              119)
          end
        end
        object tsRetirar: TTabSheet
          Caption = 'A retirar'
          ImageIndex = 2
          OnShow = tsRetirarShow
          object sgARetirar: TGridLuka
            Left = 0
            Top = 0
            Width = 828
            Height = 440
            Align = alClient
            ColCount = 11
            DefaultRowHeight = 19
            DrawingStyle = gdsGradient
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
            TabOrder = 0
            OnDrawCell = sgARetirarDrawCell
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 38619
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clActiveCaption
            HCol.Strings = (
              'Empresa'
              'Produto'
              'Nome'
              'Marca'
              'Quantidade'
              'Und.'
              'Local'
              'Lote'
              'Entregues'
              'Devolvidos'
              'Saldo')
            Grid3D = False
            RealColCount = 11
            AtivarPopUpSelecao = False
            ColWidths = (
              129
              52
              113
              67
              73
              33
              82
              72
              62
              64
              57)
          end
        end
        object tsEntregar: TTabSheet
          Caption = 'A entregar'
          ImageIndex = 3
          OnShow = tsEntregarShow
          object sgProdutosPendentes: TGridLuka
            Left = 0
            Top = 0
            Width = 828
            Height = 440
            Align = alClient
            ColCount = 11
            DefaultRowHeight = 19
            DrawingStyle = gdsGradient
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
            TabOrder = 0
            OnDrawCell = sgProdutosPendentesDrawCell
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 38619
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clActiveCaption
            HCol.Strings = (
              'Empresa'
              'Produto'
              'Nome'
              'Marca'
              'Quantidade'
              'Und.'
              'Local'
              'Lote'
              'Entregues'
              'Devolvidos'
              'Saldo'
              'Ag.cont.cli.?')
            OnGetCellColor = sgProdutosPendentesGetCellColor
            Grid3D = False
            RealColCount = 12
            Indicador = True
            AtivarPopUpSelecao = False
            ColWidths = (
              109
              47
              131
              70
              71
              33
              84
              69
              61
              66
              56)
          end
        end
        object tsEntregaSemPrevisao: TTabSheet
          Caption = 'Sem previs'#227'o'
          ImageIndex = 4
          OnShow = tsEntregaSemPrevisaoShow
          object sgProdutosSemPrevisao: TGridLuka
            Left = 0
            Top = 0
            Width = 828
            Height = 440
            Align = alClient
            ColCount = 7
            DefaultRowHeight = 19
            DrawingStyle = gdsGradient
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
            TabOrder = 0
            OnDrawCell = sgProdutosSemPrevisaoDrawCell
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 38619
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clActiveCaption
            HCol.Strings = (
              'Produto'
              'Nome'
              'Quantidade'
              'Entregues'
              'Devolvidos'
              'Saldo'
              'Ag.contato cliente?')
            OnGetCellColor = sgProdutosSemPrevisaoGetCellColor
            Grid3D = False
            RealColCount = 10
            AtivarPopUpSelecao = False
            ColWidths = (
              57
              267
              81
              69
              70
              74
              114)
          end
        end
      end
    end
    object tsNotasFiscais: TTabSheet
      Caption = 'Notas fiscais'
      ImageIndex = 4
      object sgNotasFiscais: TGridLuka
        Left = 0
        Top = 0
        Width = 836
        Height = 469
        Align = alClient
        ColCount = 6
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        TabOrder = 0
        OnDblClick = sgNotasFiscaisDblClick
        OnDrawCell = sgNotasFiscaisDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'C'#243'digo'
          'N'#250'mero nota'
          'Tipo'
          'Data emiss'#227'o'
          'Tipo movimento'
          'Valor total')
        Grid3D = False
        RealColCount = 10
        AtivarPopUpSelecao = False
        ColWidths = (
          54
          82
          89
          89
          130
          123)
      end
    end
  end
  object eAcumuladoId: TEditLuka
    Left = 95
    Top = 15
    Width = 61
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 6
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object pmsgEntregas: TPopupMenu
    Left = 792
    Top = 320
    object miImprimirComprovanteEntrega: TMenuItem
      Caption = 'Imprimir comprovante de entrega'
      OnClick = miImprimirComprovanteEntregaClick
    end
  end
  object pmsgRetiradas: TPopupMenu
    Left = 792
    Top = 232
    object miImprimirComprovanteEntregaRetirada: TMenuItem
      Caption = 'Imprimir comprovante de entrega'
      OnClick = miImprimirComprovanteEntregaRetiradaClick
    end
  end
  object pmSgCartoes: TPopupMenu
    Left = 576
    Top = 360
    object miAlterarDadosTransacao: TMenuItem
      Caption = 'Alterar dados da transa'#231#227'o'
      OnClick = miAlterarDadosTransacaoClick
    end
    object miN1: TMenuItem
      Caption = '-'
    end
    object miAlterarTipoCobranca: TMenuItem
      Caption = 'Alterar tipo de cobran'#231'a'
      OnClick = miAlterarTipoCobrancaClick
    end
  end
  object pmFinanceiro: TPopupMenu
    Left = 680
    Top = 328
    object miReemitirDuplicatasVenda: TMenuItem
      Caption = 'Reemitir duplicatas de venda'
      OnClick = miReemitirDuplicatasVendaClick
    end
  end
  object pmProdutos: TPopupMenu
    Left = 680
    Top = 280
    object miInformacoesComposicaoKit: TMenuItem
      Caption = 'Informa'#231#245'es da composi'#231#227'o do kit'
      ShortCut = 16459
      OnClick = miInformacoesComposicaoKitClick
    end
  end
end
