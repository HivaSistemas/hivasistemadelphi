inherited FormImpressaoOrcamentoGrafico: TFormImpressaoOrcamentoGrafico
  Caption = 'FormImpressaoOrcamentoGrafico'
  ClientHeight = 749
  ClientWidth = 1109
  ExplicitWidth = 1125
  ExplicitHeight = 788
  PixelsPerInch = 96
  TextHeight = 13
  inherited qrRelatorioNF: TQuickRep
    Left = 86
    Top = -11
    Height = 527
    BeforePrint = qrRelatorioNFBeforePrint
    Font.Height = -7
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    OnNeedData = qrRelatorioNFNeedData
    Page.Values = (
      0.000000000000000000
      871.471354166666800000
      10.000000000000000000
      750.755208333333200000
      10.000000000000000000
      0.000000000000000000
      0.000000000000000000)
    ExplicitLeft = 86
    ExplicitTop = -11
    ExplicitHeight = 527
    inherited qrBandTitulo: TQRBand
      Height = 236
      Size.Values = (
        390.260416666666700000
        740.833333333333400000)
      ExplicitHeight = 236
      inherited qrlNFNomeEmpresa: TQRLabel
        Left = 1
        Top = 3
        Width = 439
        Height = 17
        Size.Values = (
          28.111979166666670000
          1.653645833333333000
          4.960937500000000000
          725.950520833333200000)
        FontSize = 6
        ExplicitLeft = 1
        ExplicitTop = 3
        ExplicitWidth = 439
        ExplicitHeight = 17
      end
      inherited qrNFCnpj: TQRLabel
        Left = 1
        Top = 16
        Height = 17
        Size.Values = (
          28.111979166666670000
          1.653645833333333000
          26.458333333333330000
          641.614583333333200000)
        Font.Height = -8
        FontSize = 6
        ExplicitLeft = 1
        ExplicitTop = 16
        ExplicitHeight = 17
      end
      inherited qrNFEndereco: TQRLabel
        Left = 1
        Top = 32
        Height = 19
        Size.Values = (
          31.419270833333330000
          1.653645833333333000
          52.916666666666670000
          639.960937500000000000)
        Font.Height = -8
        FontSize = 6
        ExplicitLeft = 1
        ExplicitTop = 32
        ExplicitHeight = 19
      end
      inherited qrNFCidadeUf: TQRLabel
        Left = 1
        Top = 46
        Height = 19
        Size.Values = (
          31.419270833333330000
          1.653645833333333000
          76.067708333333320000
          277.812500000000000000)
        Font.Charset = ANSI_CHARSET
        Font.Height = -8
        FontSize = 6
        ExplicitLeft = 1
        ExplicitTop = 46
        ExplicitHeight = 19
      end
      inherited qrNFTipoDocumento: TQRLabel
        Left = 130
        Top = 96
        Width = 213
        Size.Values = (
          33.072916666666670000
          214.973958333333300000
          158.750000000000000000
          352.226562500000000000)
        Caption = 'OR'#199'AMENTO DE VENDAS'
        FontSize = 7
        ExplicitLeft = 130
        ExplicitTop = 96
        ExplicitWidth = 213
      end
      inherited qrSeparador2: TQRShape
        Left = -8
        Top = 122
        Width = 462
        Height = 1
        Size.Values = (
          1.653645833333333000
          -13.229166666666670000
          201.744791666666700000
          763.984375000000000000)
        ExplicitLeft = -8
        ExplicitTop = 122
        ExplicitWidth = 462
        ExplicitHeight = 1
      end
      inherited qrNFTitulo: TQRLabel
        Left = 432
        Width = 4
        Size.Values = (
          33.072916666666670000
          714.375000000000000000
          122.369791666666700000
          6.614583333333332000)
        Font.Height = -7
        FontSize = 5
        ExplicitLeft = 432
        ExplicitWidth = 4
      end
      inherited qrlNFEmitidoEm: TQRLabel
        Left = 1
        Top = 74
        Width = 202
        Height = 17
        Size.Values = (
          28.111979166666670000
          1.653645833333333000
          122.369791666666700000
          334.036458333333300000)
        Alignment = taLeftJustify
        Font.Height = -8
        Font.Style = []
        FontSize = 6
        ExplicitLeft = 1
        ExplicitTop = 74
        ExplicitWidth = 202
        ExplicitHeight = 17
      end
      inherited qrlNFTelefoneEmpresa: TQRLabel
        Left = 1
        Top = 60
        Width = 157
        Height = 17
        Size.Values = (
          28.111979166666670000
          1.653645833333333000
          99.218750000000000000
          259.622395833333300000)
        Font.Charset = ANSI_CHARSET
        Font.Height = -8
        FontSize = 6
        ExplicitLeft = 1
        ExplicitTop = 60
        ExplicitWidth = 157
        ExplicitHeight = 17
      end
      object QRLabel4: TQRLabel
        Left = 1
        Top = 126
        Width = 94
        Height = 17
        Size.Values = (
          28.111979166666670000
          1.653645833333333000
          208.359375000000000000
          155.442708333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'N'#186' Or'#231'amento:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel6: TQRLabel
        Left = 241
        Top = 126
        Width = 39
        Height = 17
        Size.Values = (
          28.111979166666670000
          398.528645833333300000
          208.359375000000000000
          64.492187500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vend.:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrOrcamentoIdTM: TQRLabel
        Left = 96
        Top = 126
        Width = 41
        Height = 17
        Size.Values = (
          28.111979166666670000
          158.750000000000000000
          208.359375000000000000
          67.799479166666680000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '1615'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrVendedorTM: TQRLabel
        Left = 280
        Top = 126
        Width = 166
        Height = 17
        Size.Values = (
          28.111979166666670000
          463.020833333333300000
          208.359375000000000000
          274.505208333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ricardo Sampaio'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrBairroClienteTM: TQRLabel
        Left = 285
        Top = 174
        Width = 162
        Height = 17
        Size.Values = (
          28.111979166666670000
          471.289062500000000000
          287.734375000000000000
          267.890625000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Jardim Rosa do Sul'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrCelularClienteTM: TQRLabel
        Left = 292
        Top = 141
        Width = 153
        Height = 17
        Size.Values = (
          28.111979166666670000
          482.864583333333300000
          233.164062500000000000
          253.007812500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(62) 98631-2848'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrCepClienteTM: TQRLabel
        Left = 32
        Top = 190
        Width = 66
        Height = 17
        Size.Values = (
          28.111979166666670000
          52.916666666666670000
          314.192708333333300000
          109.140625000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '74980-970'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrCidadeClienteTM: TQRLabel
        Left = 51
        Top = 174
        Width = 192
        Height = 17
        Size.Values = (
          28.111979166666670000
          84.335937500000000000
          287.734375000000000000
          317.500000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Aparecida de Goiania'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrClienteTM: TQRLabel
        Left = 54
        Top = 141
        Width = 186
        Height = 17
        Size.Values = (
          28.111979166666670000
          89.296875000000000000
          233.164062500000000000
          307.578125000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ezequiel Eugenio do Prado'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrEnderecoClienteTM: TQRLabel
        Left = 31
        Top = 157
        Width = 208
        Height = 17
        Size.Values = (
          28.111979166666670000
          51.263020833333330000
          259.622395833333300000
          343.958333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Av. Brasil qd 27 lt 30 N'#186' 606'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrEstadoClienteTM: TQRLabel
        Left = 264
        Top = 157
        Width = 182
        Height = 17
        Size.Values = (
          28.111979166666670000
          436.562500000000000000
          259.622395833333300000
          300.963541666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Mato Grosso do Sul'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel11: TQRLabel
        Left = 1
        Top = 190
        Width = 33
        Height = 17
        Size.Values = (
          28.111979166666670000
          1.653645833333333000
          314.192708333333300000
          54.570312500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'CEP:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel12: TQRLabel
        Left = 241
        Top = 141
        Width = 51
        Height = 17
        Size.Values = (
          28.111979166666670000
          398.528645833333300000
          233.164062500000000000
          84.335937500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Celular:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel13: TQRLabel
        Left = 241
        Top = 157
        Width = 24
        Height = 17
        Size.Values = (
          28.111979166666670000
          398.528645833333300000
          259.622395833333300000
          39.687500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'UF:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel5: TQRLabel
        Left = 1
        Top = 157
        Width = 30
        Height = 17
        Size.Values = (
          28.111979166666670000
          1.653645833333333000
          259.622395833333300000
          49.609375000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'End.:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel7: TQRLabel
        Left = 241
        Top = 174
        Width = 44
        Height = 17
        Size.Values = (
          28.111979166666670000
          398.528645833333300000
          287.734375000000000000
          72.760416666666680000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Bairro:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel8: TQRLabel
        Left = 1
        Top = 141
        Width = 50
        Height = 17
        Size.Values = (
          28.111979166666670000
          1.653645833333333000
          233.164062500000000000
          82.682291666666680000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cliente:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel9: TQRLabel
        Left = 1
        Top = 174
        Width = 50
        Height = 17
        Size.Values = (
          28.111979166666670000
          1.653645833333333000
          287.734375000000000000
          82.682291666666680000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cidade:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRShape4: TQRShape
        Left = -8
        Top = 213
        Width = 462
        Height = 1
        Size.Values = (
          1.653645833333333000
          -13.229166666666670000
          352.226562500000000000
          763.984375000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 15395562
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel1: TQRLabel
        Left = 4
        Top = 217
        Width = 30
        Height = 18
        Size.Values = (
          29.765625000000000000
          6.614583333333332000
          358.841145833333300000
          49.609375000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cod.:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel2: TQRLabel
        Left = 34
        Top = 217
        Width = 244
        Height = 18
        Size.Values = (
          29.765625000000000000
          56.223958333333330000
          358.841145833333300000
          403.489583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nome'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel3: TQRLabel
        Left = 280
        Top = 217
        Width = 42
        Height = 18
        Size.Values = (
          29.765625000000000000
          463.020833333333300000
          358.841145833333300000
          69.453125000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vl unt'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel14: TQRLabel
        Left = 324
        Top = 217
        Width = 32
        Height = 18
        Size.Values = (
          29.765625000000000000
          535.781250000000000000
          358.841145833333300000
          52.916666666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Qtd'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel15: TQRLabel
        Left = 358
        Top = 217
        Width = 27
        Height = 18
        Size.Values = (
          29.765625000000000000
          592.005208333333200000
          358.841145833333300000
          44.648437500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Und'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel16: TQRLabel
        Left = 390
        Top = 217
        Width = 52
        Height = 18
        Size.Values = (
          29.765625000000000000
          644.921875000000000000
          358.841145833333300000
          85.989583333333320000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
    end
    object QRBand1: TQRBand [1]
      Left = 6
      Top = 242
      Width = 448
      Height = 22
      AlignToBottom = False
      BeforePrint = QRBand1BeforePrint
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        36.380208333333330000
        740.833333333333400000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbDetail
      object qrProdutoIdTM: TQRLabel
        Left = 1
        Top = 1
        Width = 30
        Height = 20
        Size.Values = (
          33.072916666666670000
          1.653645833333333000
          1.653645833333333000
          49.609375000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cod.:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrNomeProdutoTM: TQRLabel
        Left = 31
        Top = 1
        Width = 244
        Height = 20
        Size.Values = (
          33.072916666666670000
          51.263020833333330000
          1.653645833333333000
          403.489583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nome'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrValorUnitarioTM: TQRLabel
        Left = 277
        Top = 1
        Width = 42
        Height = 20
        Size.Values = (
          33.072916666666670000
          458.059895833333300000
          1.653645833333333000
          69.453125000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vl unt'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrQuantidadeTM: TQRLabel
        Left = 321
        Top = 1
        Width = 32
        Height = 20
        Size.Values = (
          33.072916666666670000
          530.820312500000000000
          1.653645833333333000
          52.916666666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Qtd'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrUnidadeTM: TQRLabel
        Left = 355
        Top = 1
        Width = 27
        Height = 20
        Size.Values = (
          33.072916666666670000
          587.044270833333200000
          1.653645833333333000
          44.648437500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Und'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrValorTotalTM: TQRLabel
        Left = 387
        Top = 1
        Width = 58
        Height = 20
        Size.Values = (
          33.072916666666670000
          639.960937500000000000
          1.653645833333333000
          95.911458333333320000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
    end
    inherited PageFooterBand1: TQRBand
      Top = 264
      Height = 81
      Size.Values = (
        133.945312500000000000
        740.833333333333400000)
      ExplicitTop = 264
      ExplicitHeight = 81
      inherited qrlNFSistema: TQRLabel
        Left = 328
        Top = 293
        Width = 126
        Size.Values = (
          23.151041666666670000
          542.395833333333400000
          484.518229166666800000
          208.359375000000000000)
        FontSize = -6
        ExplicitLeft = 328
        ExplicitTop = 293
        ExplicitWidth = 126
      end
      object QRLabel10: TQRLabel
        Left = 243
        Top = 4
        Width = 118
        Height = 17
        Size.Values = (
          28.111979166666670000
          401.835937500000000000
          6.614583333333332000
          195.130208333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total produtos:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel17: TQRLabel
        Left = 245
        Top = 18
        Width = 110
        Height = 17
        Size.Values = (
          28.111979166666670000
          405.143229166666700000
          29.765625000000000000
          181.901041666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Outras despesas:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel18: TQRLabel
        Left = 245
        Top = 33
        Width = 118
        Height = 17
        Size.Values = (
          28.111979166666670000
          405.143229166666700000
          54.570312500000000000
          195.130208333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Frete:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel19: TQRLabel
        Left = 245
        Top = 46
        Width = 118
        Height = 17
        Size.Values = (
          28.111979166666670000
          405.143229166666700000
          76.067708333333320000
          195.130208333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Desconto:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel20: TQRLabel
        Left = 243
        Top = 62
        Width = 128
        Height = 17
        Size.Values = (
          28.111979166666670000
          401.835937500000000000
          102.526041666666700000
          211.666666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total geral:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrTotalProdutosTM: TQRLabel
        Left = 351
        Top = 3
        Width = 87
        Height = 17
        Size.Values = (
          28.111979166666670000
          580.429687500000000000
          4.960937500000000000
          143.867187500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total produtos'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrValorOutrasDespesastm: TQRLabel
        Left = 352
        Top = 18
        Width = 86
        Height = 18
        Size.Values = (
          29.765625000000000000
          582.083333333333200000
          29.765625000000000000
          142.213541666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Outras despesas'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrlGrValorFreteTM: TQRLabel
        Left = 351
        Top = 34
        Width = 87
        Height = 17
        Size.Values = (
          28.111979166666670000
          580.429687500000000000
          56.223958333333330000
          143.867187500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Frete'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrValorDescontoTM: TQRLabel
        Left = 351
        Top = 48
        Width = 87
        Height = 17
        Size.Values = (
          28.111979166666670000
          580.429687500000000000
          79.375000000000000000
          143.867187500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Desconto'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrTotalSerPagoTM: TQRLabel
        Left = 351
        Top = 62
        Width = 87
        Height = 17
        Size.Values = (
          28.111979166666670000
          580.429687500000000000
          102.526041666666700000
          143.867187500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total geral'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
    end
    object qrMensagensFinaisTM: TQRMemo
      Left = 6
      Top = 450
      Width = 239
      Height = 75
      Size.Values = (
        124.023437500000000000
        9.921875000000000000
        744.140625000000000000
        395.221354166666700000)
      XLColumn = 0
      XLNumFormat = nfGeneral
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = False
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -8
      Font.Name = 'Arial'
      Font.Style = []
      Lines.Strings = (
        'Or'#231'amento v'#225'lido pelo prazo de 15 dias'
        '')
      ParentFont = False
      Transparent = False
      FullJustify = False
      MaxBreakChars = 0
      FontSize = 6
    end
  end
  inherited qrRelatorioA4: TQuickRep
    Left = 68
    Top = 21
    Width = 952
    Height = 1347
    BeforePrint = qrRelatorioBeforePrint
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    OnNeedData = qrRelatorioNeedData
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    Zoom = 120
    ExplicitLeft = 68
    ExplicitTop = 21
    ExplicitWidth = 952
    ExplicitHeight = 1347
    inherited qrCabecalho: TQRBand
      Left = 45
      Top = 45
      Width = 861
      Height = 143
      Size.Values = (
        315.295138888888900000
        1898.385416666667000000)
      ExplicitLeft = 45
      ExplicitTop = 45
      ExplicitWidth = 861
      ExplicitHeight = 143
      inherited qrCaptionEndereco: TQRLabel
        Left = 103
        Top = 27
        Width = 64
        Height = 17
        Size.Values = (
          37.482638888888890000
          227.100694444444400000
          59.531250000000000000
          141.111111111111100000)
        Alignment = taLeftJustify
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 103
        ExplicitTop = 27
        ExplicitWidth = 64
        ExplicitHeight = 17
      end
      inherited qrEmpresa: TQRLabel
        Left = 171
        Top = 10
        Width = 333
        Height = 17
        Size.Values = (
          37.482638888888890000
          377.031250000000000000
          22.048611111111110000
          734.218750000000000000)
        Caption = 'Hiva Solucoes Ltda'
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        FontSize = 9
        ExplicitLeft = 171
        ExplicitTop = 10
        ExplicitWidth = 333
        ExplicitHeight = 17
      end
      inherited qrEndereco: TQRLabel
        Left = 171
        Top = 27
        Width = 333
        Height = 17
        Size.Values = (
          37.482638888888890000
          377.031250000000000000
          59.531250000000000000
          734.218750000000000000)
        Caption = 'Av Independencia qd 20 lote 30 Nr 100'
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 171
        ExplicitTop = 27
        ExplicitWidth = 333
        ExplicitHeight = 17
      end
      inherited qrCaptionEmpresa: TQRLabel
        Left = 103
        Top = 10
        Width = 64
        Height = 17
        Size.Values = (
          37.482638888888890000
          227.100694444444400000
          22.048611111111110000
          141.111111111111100000)
        Alignment = taLeftJustify
        Caption = 'Empresa:'
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 103
        ExplicitTop = 10
        ExplicitWidth = 64
        ExplicitHeight = 17
      end
      inherited qrEmitidoEm: TQRLabel
        Left = 744
        Top = 3
        Width = 112
        Height = 13
        Size.Values = (
          28.663194444444440000
          1640.416666666667000000
          6.614583333333332000
          246.944444444444400000)
        Caption = 'Data 11/04/2014 '#225's 13:57:55'
        Font.Name = 'Cambria'
        FontSize = 5
        ExplicitLeft = 744
        ExplicitTop = 3
        ExplicitWidth = 112
        ExplicitHeight = 13
      end
      inherited qr1: TQRLabel
        Left = 510
        Top = 27
        Width = 65
        Height = 17
        Size.Values = (
          37.482638888888890000
          1124.479166666667000000
          59.531250000000000000
          143.315972222222200000)
        Alignment = taLeftJustify
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 510
        ExplicitTop = 27
        ExplicitWidth = 65
        ExplicitHeight = 17
      end
      inherited qrBairro: TQRLabel
        Left = 584
        Top = 27
        Width = 273
        Height = 17
        Size.Values = (
          37.482638888888890000
          1287.638888888889000000
          59.531250000000000000
          601.927083333333200000)
        Caption = 'Setor Serra Dourada III'
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 584
        ExplicitTop = 27
        ExplicitWidth = 273
        ExplicitHeight = 17
      end
      inherited qr3: TQRLabel
        Left = 510
        Top = 46
        Width = 72
        Height = 17
        Size.Values = (
          37.482638888888890000
          1124.479166666667000000
          101.423611111111100000
          158.750000000000000000)
        Alignment = taLeftJustify
        Caption = 'Cid./UF:'
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 510
        ExplicitTop = 46
        ExplicitWidth = 72
        ExplicitHeight = 17
      end
      inherited qrCidadeUf: TQRLabel
        Left = 584
        Top = 46
        Width = 273
        Height = 17
        Size.Values = (
          37.482638888888890000
          1287.638888888889000000
          101.423611111111100000
          601.927083333333200000)
        Caption = 'Aparecida de Goi'#226'nia / GO'
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 584
        ExplicitTop = 46
        ExplicitWidth = 273
        ExplicitHeight = 17
      end
      inherited qr5: TQRLabel
        Left = 510
        Top = 10
        Width = 72
        Height = 17
        Size.Values = (
          37.482638888888890000
          1124.479166666667000000
          22.048611111111110000
          158.750000000000000000)
        Alignment = taLeftJustify
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 510
        ExplicitTop = 10
        ExplicitWidth = 72
        ExplicitHeight = 17
      end
      inherited qrCNPJ: TQRLabel
        Left = 584
        Top = 10
        Width = 152
        Height = 17
        Size.Values = (
          37.482638888888890000
          1287.638888888889000000
          22.048611111111110000
          335.138888888888900000)
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 584
        ExplicitTop = 10
        ExplicitWidth = 152
        ExplicitHeight = 17
      end
      inherited qr7: TQRLabel
        Left = 103
        Top = 46
        Width = 64
        Height = 17
        Size.Values = (
          37.482638888888890000
          227.100694444444400000
          101.423611111111100000
          141.111111111111100000)
        Alignment = taLeftJustify
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 103
        ExplicitTop = 46
        ExplicitWidth = 64
        ExplicitHeight = 17
      end
      inherited qrTelefone: TQRLabel
        Left = 171
        Top = 46
        Width = 111
        Height = 17
        Size.Values = (
          37.482638888888890000
          377.031250000000000000
          101.423611111111100000
          244.739583333333300000)
        Caption = '(62) 9999-9999'
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 171
        ExplicitTop = 46
        ExplicitWidth = 111
        ExplicitHeight = 17
      end
      inherited qrFax: TQRLabel
        Left = 171
        Top = 64
        Width = 111
        Height = 17
        Size.Values = (
          37.482638888888890000
          377.031250000000000000
          141.111111111111100000
          244.739583333333300000)
        Caption = '(62) 9999-9999'
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 171
        ExplicitTop = 64
        ExplicitWidth = 111
        ExplicitHeight = 17
      end
      inherited qr10: TQRLabel
        Left = 103
        Top = 64
        Width = 64
        Height = 17
        Size.Values = (
          37.482638888888890000
          227.100694444444400000
          141.111111111111100000
          141.111111111111100000)
        Alignment = taLeftJustify
        Caption = 'Whatsapp: '
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 103
        ExplicitTop = 64
        ExplicitWidth = 64
        ExplicitHeight = 17
      end
      inherited qr11: TQRLabel
        Left = 510
        Top = 64
        Width = 72
        Height = 17
        Size.Values = (
          37.482638888888890000
          1124.479166666667000000
          141.111111111111100000
          158.750000000000000000)
        Alignment = taLeftJustify
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 510
        ExplicitTop = 64
        ExplicitWidth = 72
        ExplicitHeight = 17
      end
      inherited qrEmail: TQRLabel
        Left = 584
        Top = 64
        Width = 273
        Height = 17
        Size.Values = (
          37.482638888888890000
          1287.638888888889000000
          141.111111111111100000
          601.927083333333200000)
        Caption = 'hivasolucoes@gmail.com'
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 584
        ExplicitTop = 64
        ExplicitWidth = 273
        ExplicitHeight = 17
      end
      inherited qr13: TQRLabel
        Left = 316
        Top = 90
        Width = 240
        Height = 28
        Size.Values = (
          61.736111111111100000
          696.736111111111000000
          198.437500000000000000
          529.166666666666700000)
        Caption = 'Or'#231'amento'
        Font.Charset = ANSI_CHARSET
        Font.Height = -20
        Font.Name = 'Cambria'
        FontSize = 15
        ExplicitLeft = 316
        ExplicitTop = 90
        ExplicitWidth = 240
        ExplicitHeight = 28
      end
      inherited QRShape1: TQRShape
        Top = 120
        Width = 862
        Height = 2
        Enabled = True
        Size.Values = (
          4.409722222222222000
          0.000000000000000000
          264.583333333333300000
          1900.590277777778000000)
        ExplicitTop = 120
        ExplicitWidth = 862
        ExplicitHeight = 2
      end
      inherited qrLogoEmpresa: TQRImage
        Left = 6
        Width = 96
        Height = 96
        Size.Values = (
          211.666666666666700000
          13.229166666666670000
          2.645833333333333000
          211.666666666666700000)
        ExplicitLeft = 6
        ExplicitWidth = 96
        ExplicitHeight = 96
      end
    end
    inherited qrTitulo: TQRBand
      Left = 45
      Top = 188
      Width = 861
      Height = 94
      Size.Values = (
        207.256944444444400000
        1898.385416666667000000)
      ExplicitLeft = 45
      ExplicitTop = 188
      ExplicitWidth = 861
      ExplicitHeight = 94
      inherited QRShape2: TQRShape
        Left = -1
        Top = 77
        Width = 862
        Height = 13
        Size.Values = (
          28.663194444444440000
          -2.204861111111111000
          169.774305555555600000
          1900.590277777778000000)
        ExplicitLeft = -1
        ExplicitTop = 77
        ExplicitWidth = 862
        ExplicitHeight = 13
      end
      object qr2: TQRLabel
        Left = 6
        Top = 6
        Width = 75
        Height = 17
        Size.Values = (
          37.482638888888890000
          13.229166666666670000
          13.229166666666670000
          165.364583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Or'#231'amento: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrOrcamentoId: TQRLabel
        Left = 83
        Top = 6
        Width = 114
        Height = 17
        Size.Values = (
          37.482638888888890000
          183.003472222222200000
          13.229166666666670000
          251.354166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '146'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr8: TQRLabel
        Left = 6
        Top = 22
        Width = 66
        Height = 17
        Size.Values = (
          37.482638888888890000
          13.229166666666670000
          48.506944444444440000
          145.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cliente: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrCliente: TQRLabel
        Left = 73
        Top = 22
        Width = 388
        Height = 17
        Size.Values = (
          37.482638888888890000
          160.954861111111100000
          48.506944444444440000
          855.486111111111000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '8 - TALITA C'#194'NDIA LIMA SILVA'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr12: TQRLabel
        Left = 6
        Top = 38
        Width = 66
        Height = 17
        Size.Values = (
          37.482638888888890000
          13.229166666666670000
          83.784722222222220000
          145.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Endere'#231'o: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrEnderecoCliente: TQRLabel
        Left = 73
        Top = 38
        Width = 393
        Height = 17
        Size.Values = (
          37.482638888888890000
          160.954861111111100000
          83.784722222222220000
          866.510416666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'RUA 1020 QD36B LT 15 N'#186' 503'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr15: TQRLabel
        Left = 467
        Top = 38
        Width = 43
        Height = 17
        Size.Values = (
          37.482638888888890000
          1029.670138888889000000
          83.784722222222220000
          94.809027777777780000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Bairro: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrBairroCliente: TQRLabel
        Left = 511
        Top = 38
        Width = 346
        Height = 17
        Size.Values = (
          37.482638888888890000
          1126.684027777778000000
          83.784722222222220000
          762.881944444444400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'SETOR PEDRO LUDOVICO'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrCidadeCliente: TQRLabel
        Left = 73
        Top = 55
        Width = 388
        Height = 17
        Size.Values = (
          37.482638888888890000
          160.954861111111100000
          121.267361111111100000
          855.486111111111000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'GOI'#194'NIA'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr18: TQRLabel
        Left = 6
        Top = 55
        Width = 66
        Height = 17
        Size.Values = (
          37.482638888888890000
          13.229166666666670000
          121.267361111111100000
          145.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cid./UF:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrCepCliente: TQRLabel
        Left = 511
        Top = 55
        Width = 79
        Height = 17
        Size.Values = (
          37.482638888888890000
          1126.684027777778000000
          121.267361111111100000
          174.184027777777800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '74800-008'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr21: TQRLabel
        Left = 467
        Top = 55
        Width = 43
        Height = 17
        Size.Values = (
          37.482638888888890000
          1029.670138888889000000
          121.267361111111100000
          94.809027777777780000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'CEP: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr4: TQRLabel
        Left = 467
        Top = 22
        Width = 32
        Height = 17
        Size.Values = (
          37.482638888888890000
          1029.670138888889000000
          48.506944444444440000
          70.555555555555560000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Tel.: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrTelefoneCliente: TQRLabel
        Left = 500
        Top = 22
        Width = 111
        Height = 17
        Size.Values = (
          37.482638888888890000
          1102.430555555556000000
          48.506944444444440000
          244.739583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(62) 3288-1655'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr14: TQRLabel
        Left = 636
        Top = 22
        Width = 29
        Height = 17
        Size.Values = (
          37.482638888888890000
          1402.291666666667000000
          48.506944444444440000
          63.940972222222220000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cel.: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrCelularCliente: TQRLabel
        Left = 667
        Top = 22
        Width = 117
        Height = 17
        Size.Values = (
          37.482638888888890000
          1470.642361111111000000
          48.506944444444440000
          257.968750000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(62) 9876-5432'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr9: TQRLabel
        Left = 195
        Top = 6
        Width = 66
        Height = 17
        Size.Values = (
          37.482638888888890000
          429.947916666666700000
          13.229166666666670000
          145.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vendedor:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrVendedor: TQRLabel
        Left = 262
        Top = 6
        Width = 199
        Height = 17
        Size.Values = (
          37.482638888888890000
          577.673611111111000000
          13.229166666666670000
          438.767361111111100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'JOSE DA SILVA'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr17: TQRLabel
        Left = 467
        Top = 6
        Width = 84
        Height = 17
        Size.Values = (
          37.482638888888890000
          1029.670138888889000000
          13.229166666666670000
          185.208333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cond. pagto.: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrCondicaoPagamento: TQRLabel
        Left = 552
        Top = 6
        Width = 314
        Height = 17
        Size.Values = (
          37.482638888888890000
          1217.083333333333000000
          13.229166666666670000
          692.326388888889000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '1 - Avista'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrIndice: TQRLabel
        Left = 703
        Top = 54
        Width = 79
        Height = 17
        Enabled = False
        Size.Values = (
          37.482638888888900000
          1550.017361111111000000
          119.062500000000000000
          174.184027777777800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '1,25'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
    end
    inherited qrBandaCabecalhoColunas: TQRBand
      Left = 45
      Top = 282
      Width = 861
      Height = 17
      Size.Values = (
        37.482638888888890000
        1898.385416666667000000)
      ExplicitLeft = 45
      ExplicitTop = 282
      ExplicitWidth = 861
      ExplicitHeight = 17
      object qr23: TQRLabel
        Left = 2
        Top = 2
        Width = 52
        Height = 15
        Size.Values = (
          33.072916666666670000
          4.409722222222222000
          4.409722222222222000
          114.652777777777800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Produto'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr24: TQRLabel
        Left = 54
        Top = 2
        Width = 296
        Height = 15
        Size.Values = (
          33.072916666666670000
          119.062500000000000000
          4.409722222222222000
          652.638888888889000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nome'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr25: TQRLabel
        Left = 349
        Top = 2
        Width = 77
        Height = 15
        Size.Values = (
          33.072916666666670000
          769.496527777777800000
          4.409722222222222000
          169.774305555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Marca'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr26: TQRLabel
        Left = 427
        Top = 2
        Width = 55
        Height = 15
        Size.Values = (
          33.072916666666670000
          941.475694444444400000
          4.409722222222222000
          121.267361111111100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Qtde.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr27: TQRLabel
        Left = 484
        Top = 2
        Width = 45
        Height = 15
        Size.Values = (
          33.072916666666670000
          1067.152777777778000000
          4.409722222222222000
          99.218750000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Und.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr6: TQRLabel
        Left = 529
        Top = 2
        Width = 70
        Height = 15
        Size.Values = (
          33.072916666666670000
          1166.371527777778000000
          4.409722222222222000
          154.340277777777800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Pre'#231'o unit.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr28: TQRLabel
        Left = 760
        Top = 2
        Width = 100
        Height = 15
        Size.Values = (
          33.072916666666670000
          1675.694444444444000000
          4.409722222222222000
          220.486111111111100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total produto'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qlDescontoUnitario: TQRLabel
        Left = 601
        Top = 2
        Width = 65
        Height = 15
        Size.Values = (
          33.072916666666670000
          1325.121527777778000000
          4.409722222222222000
          143.315972222222200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Desconto'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qlPrcoUnitLiq: TQRLabel
        Left = 668
        Top = 2
        Width = 88
        Height = 15
        Size.Values = (
          33.072916666666670000
          1472.847222222222000000
          4.409722222222222000
          194.027777777777800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Prco unit liq'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
    end
    inherited qrBandaDetalhes: TQRBand
      Left = 45
      Top = 299
      Width = 861
      Height = 21
      BeforePrint = qrBandaDetalhesBeforePrint
      Size.Values = (
        46.302083333333340000
        1898.385416666667000000)
      ExplicitLeft = 45
      ExplicitTop = 299
      ExplicitWidth = 861
      ExplicitHeight = 21
      object qrProdutoId: TQRLabel
        Left = 2
        Top = 0
        Width = 51
        Height = 16
        Size.Values = (
          35.277777777777780000
          4.409722222222222000
          0.000000000000000000
          112.447916666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Produto'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrNomeProduto: TQRLabel
        Left = 54
        Top = 0
        Width = 296
        Height = 16
        Size.Values = (
          35.277777777777780000
          119.062500000000000000
          0.000000000000000000
          652.638888888889000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nome'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrMarca: TQRLabel
        Left = 350
        Top = 0
        Width = 77
        Height = 16
        Size.Values = (
          35.277777777777780000
          771.701388888889000000
          0.000000000000000000
          169.774305555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Votorantim'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrValorUnitario: TQRLabel
        Left = 529
        Top = 0
        Width = 69
        Height = 16
        Size.Values = (
          35.277777777777780000
          1166.371527777778000000
          0.000000000000000000
          152.135416666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Pre'#231'o unit.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrQuantidade: TQRLabel
        Left = 428
        Top = 0
        Width = 53
        Height = 16
        Size.Values = (
          35.277777777777780000
          943.680555555555600000
          0.000000000000000000
          116.857638888888900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '2350,000'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrUnidade: TQRLabel
        Left = 483
        Top = 0
        Width = 45
        Height = 16
        Size.Values = (
          35.277777777777780000
          1064.947916666667000000
          0.000000000000000000
          99.218750000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Und.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrValorTotal: TQRLabel
        Left = 760
        Top = 0
        Width = 100
        Height = 16
        Size.Values = (
          35.277777777777780000
          1675.694444444444000000
          0.000000000000000000
          220.486111111111100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total do produto'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrDescontoUnitario: TQRLabel
        Left = 600
        Top = 0
        Width = 65
        Height = 16
        Size.Values = (
          35.277777777777780000
          1322.916666666667000000
          0.000000000000000000
          143.315972222222200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '150,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRShape3: TQRShape
        Left = -1
        Top = 18
        Width = 862
        Height = 2
        Size.Values = (
          4.409722222222222000
          -2.204861111111111000
          39.687500000000000000
          1900.590277777778000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object qrPrcoUnitLiq: TQRLabel
        Left = 671
        Top = 0
        Width = 84
        Height = 16
        Size.Values = (
          35.277777777777780000
          1479.461805555556000000
          0.000000000000000000
          185.208333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '150,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
    end
    inherited qrbndRodape: TQRBand
      Left = 45
      Top = 320
      Width = 861
      Height = 186
      Size.Values = (
        410.104166666666700000
        1898.385416666667000000)
      ExplicitLeft = 45
      ExplicitTop = 320
      ExplicitWidth = 861
      ExplicitHeight = 186
      inherited qrUsuarioImpressao: TQRLabel
        Left = 4
        Top = 169
        Width = 161
        Height = 14
        Size.Values = (
          30.868055555555560000
          8.819444444444444000
          372.621527777777800000
          354.982638888888900000)
        Caption = 'EZEQUIEL EUGENIO DO PRADO'
        Font.Height = -7
        Font.Name = 'Cambria'
        FontSize = 5
        ExplicitLeft = 4
        ExplicitTop = 169
        ExplicitWidth = 161
        ExplicitHeight = 14
      end
      inherited qrSistema: TQRLabel
        Left = 4
        Top = 74
        Width = 168
        Height = 14
        Size.Values = (
          30.868055555555560000
          8.819444444444444000
          163.159722222222200000
          370.416666666666700000)
        Alignment = taLeftJustify
        Caption = 'Hiva 3.0'
        Font.Height = -7
        Font.Name = 'Cambria'
        FontSize = 5
        ExplicitLeft = 4
        ExplicitTop = 74
        ExplicitWidth = 168
        ExplicitHeight = 14
      end
      object qrMensagensFinais: TQRMemo
        Left = 4
        Top = 94
        Width = 853
        Height = 56
        Size.Values = (
          123.472222222222200000
          8.819444444444444000
          207.256944444444400000
          1880.746527777778000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        Lines.Strings = (
          'Or'#231'amento v'#225'lido pelo prazo de 15 dias'
          '')
        ParentFont = False
        Transparent = False
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 9
      end
      object qr20: TQRLabel
        Left = 601
        Top = 22
        Width = 122
        Height = 17
        Size.Values = (
          37.482638888888890000
          1325.121527777778000000
          48.506944444444440000
          268.993055555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Outras despesas:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr22: TQRLabel
        Left = 601
        Top = 54
        Width = 122
        Height = 17
        Size.Values = (
          37.482638888888890000
          1325.121527777778000000
          119.062500000000000000
          268.993055555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor desconto: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr29: TQRLabel
        Left = 601
        Top = 71
        Width = 122
        Height = 17
        Size.Values = (
          37.482638888888890000
          1325.121527777778000000
          156.545138888888900000
          268.993055555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total geral:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrTotalSerPago: TQRLabel
        Left = 725
        Top = 71
        Width = 111
        Height = 17
        Size.Values = (
          37.482638888888890000
          1598.524305555556000000
          156.545138888888900000
          244.739583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total ser pago: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrValorDesconto: TQRLabel
        Left = 725
        Top = 54
        Width = 111
        Height = 17
        Size.Values = (
          37.482638888888890000
          1598.524305555556000000
          119.062500000000000000
          244.739583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor desconto: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrValorOutrasDespesas: TQRLabel
        Left = 725
        Top = 22
        Width = 111
        Height = 17
        Size.Values = (
          37.482638888888890000
          1598.524305555556000000
          48.506944444444440000
          244.739583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor outras desp: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrl1: TQRLabel
        Left = 601
        Top = 37
        Width = 119
        Height = 17
        Size.Values = (
          37.482638888888890000
          1325.121527777778000000
          81.579861111111100000
          262.378472222222200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Frete:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrlGrValorFrete: TQRLabel
        Left = 725
        Top = 37
        Width = 108
        Height = 17
        Size.Values = (
          37.482638888888890000
          1598.524305555556000000
          81.579861111111100000
          238.125000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor frete'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrTotalProdutos: TQRLabel
        Left = 725
        Top = 6
        Width = 111
        Height = 17
        Size.Values = (
          37.482638888888890000
          1598.524305555556000000
          13.229166666666670000
          244.739583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total produtos: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr16: TQRLabel
        Left = 601
        Top = 6
        Width = 122
        Height = 17
        Size.Values = (
          37.482638888888890000
          1325.121527777778000000
          13.229166666666670000
          268.993055555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total produtos: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
    end
  end
  object QRPDFFilter1: TQRPDFFilter
    CompressionOn = False
    TextEncoding = AnsiEncoding
    Codepage = '1252'
    Left = 24
    Top = 248
  end
end
