inherited FormBuscarDadosOutrosCustosEntradas: TFormBuscarDadosOutrosCustosEntradas
  Caption = 'Buscar outros custos de entrada'
  ClientHeight = 331
  ClientWidth = 698
  OnShow = FormShow
  ExplicitWidth = 704
  ExplicitHeight = 360
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 294
    Width = 698
    ExplicitTop = 253
    ExplicitWidth = 664
  end
  inline FrOutrosCustos: TFrDadosCobrancaOutrosCustos
    Left = 2
    Top = 1
    Width = 696
    Height = 291
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 2
    ExplicitTop = 1
    ExplicitWidth = 696
    ExplicitHeight = 291
    inherited pnInformacoesPrincipais: TPanel
      Width = 696
      ExplicitWidth = 662
      inherited lbllb10: TLabel
        Width = 39
        Height = 14
        ExplicitWidth = 39
        ExplicitHeight = 14
      end
      inherited lbllb9: TLabel
        Width = 29
        Height = 14
        ExplicitWidth = 29
        ExplicitHeight = 14
      end
      inherited lbllb6: TLabel
        Width = 28
        Height = 14
        ExplicitWidth = 28
        ExplicitHeight = 14
      end
      inherited lbllb2: TLabel
        Width = 64
        Height = 14
        ExplicitWidth = 64
        ExplicitHeight = 14
      end
      inherited lbl2: TLabel
        Width = 62
        Height = 14
        ExplicitWidth = 62
        ExplicitHeight = 14
      end
      inherited eRepetir: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited ePrazo: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorCobranca: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eDataVencimento: TEditLukaData
        Height = 22
        ExplicitHeight = 22
      end
      inherited eDocumento: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited FrFornecedor: TFrFornecedores
        inherited PnTitulos: TPanel
          inherited lbNomePesquisa: TLabel
            Width = 61
            Height = 14
            ExplicitWidth = 61
            ExplicitHeight = 14
          end
        end
      end
      inherited FrPlanoFinanceiro: TFrPlanosFinanceiros
        inherited PnTitulos: TPanel
          inherited lbNomePesquisa: TLabel
            Width = 88
            Height = 14
            ExplicitWidth = 88
            ExplicitHeight = 14
          end
        end
      end
      inherited FrCentroCustos: TFrCentroCustos
        inherited sgPesquisa: TGridLuka
          ExplicitHeight = 24
        end
        inherited PnTitulos: TPanel
          inherited lbNomePesquisa: TLabel
            Width = 90
            Height = 14
            ExplicitWidth = 90
            ExplicitHeight = 14
          end
        end
        inherited pnPesquisa: TPanel
          ExplicitHeight = 25
        end
      end
    end
    inherited pn1: TPanel
      Width = 696
      Height = 210
      ExplicitTop = 80
      ExplicitWidth = 693
      ExplicitHeight = 168
      DesignSize = (
        696
        210)
      inherited sgCobrancas: TGridLuka
        Height = 208
        ExplicitHeight = 166
        ColWidths = (
          34
          144
          41
          118
          58
          70
          31
          48
          85
          148
          182)
      end
    end
  end
end
