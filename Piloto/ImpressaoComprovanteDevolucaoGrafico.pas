unit ImpressaoComprovanteDevolucaoGrafico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosGraficos, QRExport, QRPDFFilt, QRWebFilt,
  QRCtrls, QuickRpt, Vcl.ExtCtrls, _Sessao, _Biblioteca, _Devolucoes, _DevolucoesItens,
  _RecordsOrcamentosVendas;

type
  TFormImpressaoComprovanteDevolucaoGrafico = class(TFormHerancaRelatoriosGraficos)
    qrbndNFDetailBand1: TQRBand;
    qrlNFNomeProduto: TQRLabel;
    qrlNFCabUnidade: TQRLabel;
    qrlNFLote: TQRLabel;
    qrlNFQuantidade: TQRLabel;
    qrlCabProdutoId: TQRLabel;
    qrl1: TQRLabel;
    qrlNFMarca: TQRLabel;
    qrl4: TQRLabel;
    qrlNFUnidade: TQRLabel;
    qrl3: TQRLabel;
    qrlNFCodigoBarras: TQRLabel;
    qrl7: TQRLabel;
    qrlNFCodigoOriginal: TQRLabel;
    qrlNFCodigoMovimento: TQRLabel;
    qrlNFPedido: TQRLabel;
    qrlNFCliente: TQRLabel;
    qrl5: TQRLabel;
    QRShape2: TQRShape;
    qrlNFUsuarioDevolucao: TQRLabel;
    qrlNFMotivoDevolucao: TQRMemo;
    QRShape1: TQRShape;
    qrTitulo: TQRBand;
    QRShape3: TQRShape;
    qr2: TQRLabel;
    qrDevolucaoId: TQRLabel;
    qr8: TQRLabel;
    qrCliente: TQRLabel;
    qr12: TQRLabel;
    qrEnderecoCliente: TQRLabel;
    qr15: TQRLabel;
    qrBairroCliente: TQRLabel;
    qrCidadeCliente: TQRLabel;
    qr18: TQRLabel;
    qrCepCliente: TQRLabel;
    qr21: TQRLabel;
    qr4: TQRLabel;
    qrTelefoneCliente: TQRLabel;
    qr14: TQRLabel;
    qrCelularCliente: TQRLabel;
    qr9: TQRLabel;
    qrUsuarioConfirmacao: TQRLabel;
    qr17: TQRLabel;
    qrCondicaoPagamento: TQRLabel;
    QRLabel22: TQRLabel;
    qrApelido: TQRLabel;
    qrBandaCabecalhoColunas: TQRBand;
    qr23: TQRLabel;
    qr24: TQRLabel;
    qr25: TQRLabel;
    qr26: TQRLabel;
    qr27: TQRLabel;
    qr6: TQRLabel;
    qr28: TQRLabel;
    qrBandaDetalhes: TQRBand;
    qrProdutoId: TQRLabel;
    qrNomeProduto: TQRLabel;
    qrMarca: TQRLabel;
    qrValorUnitario: TQRLabel;
    qrQuantidade: TQRLabel;
    qrUnidade: TQRLabel;
    qrValorTotal: TQRLabel;
    QRLabel1: TQRLabel;
    qrOrcamentoId: TQRLabel;
    QRLabel2: TQRLabel;
    qrUsuarioDevolucao: TQRLabel;
    QRLabel3: TQRLabel;
    qrDataHoraDevolucao: TQRLabel;
    QRLabel5: TQRLabel;
    qrDataHoraConfirmacao: TQRLabel;
    qr20: TQRLabel;
    qr22: TQRLabel;
    qrValorDesconto: TQRLabel;
    qrValorOutrasDespesas: TQRLabel;
    qrTotalProdutos: TQRLabel;
    qr16: TQRLabel;
    QRShape4: TQRShape;
    qrlGrValorFrete: TQRLabel;
    QRLabel4: TQRLabel;
    qrCpfCnpjUsuarioImpressao: TQRLabel;
    qrQtdePaginas: TQRLabel;
    qrNumeroFolha: TQRSysData;
    QRLabel6: TQRLabel;
    lbGrCreditoGerado: TQRLabel;
    qrlGrCreditoGerado: TQRLabel;
    procedure qrbndNFDetailBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qrRelatorioNFBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure qrRelatorioNFNeedData(Sender: TObject; var MoreData: Boolean);
    procedure qrBandaDetalhesBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure qrRelatorioA4BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure qrRelatorioA4NeedData(Sender: TObject; var MoreData: Boolean);
    procedure qrNumeroFolhaPrint(sender: TObject; var Value: string);
  private
    FPosicao: Integer;
    FItens: TArray<RecDevolucoesItens>;
    FTemProdutosEntregues: Boolean;
  public
    { Public declarations }
  end;

procedure Imprimir(pDevolucaoId: Integer);

implementation

uses
  _ContasPagar;

{$R *.dfm}

procedure Imprimir(pDevolucaoId: Integer);
var
  vImpressora: RecImpressora;
  vDevolucoes: TArray<RecDevolucoes>;
  vValorCreditoGerado: Double;

  vForm: TFormImpressaoComprovanteDevolucaoGrafico;
begin
  if pDevolucaoId = 0 then
    Exit;

  vDevolucoes := _Devolucoes.BuscarDevolucoes(Sessao.getConexaoBanco, 0, [pDevolucaoId]);
  if vDevolucoes = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vValorCreditoGerado := 0.00;
  if Sessao.getParametrosEmpresa.PermitirDevolucaoAposPrazo = 'S' then
    vValorCreditoGerado := _ContasPagar.BuscarCreditoDevolucao(Sessao.getConexaoBanco, pDevolucaoId);

  if not Sessao.AutorizadoRotina('reimprimir_comprovante_devolucao') then begin
    _Biblioteca.NaoAutorizadoRotina;
    Exit;
  end;

  vImpressora := Sessao.getImpressora( toComprovanteDevolucao );
  if vImpressora.CaminhoImpressora = '' then begin
    _Biblioteca.ImpressoraNaoConfigurada;
    Exit;
  end;

  vForm := TFormImpressaoComprovanteDevolucaoGrafico.Create(nil, vImpressora);
  vForm.PreencherCabecalho( vDevolucoes[0].EmpresaId, False );

  vForm.FItens := _DevolucoesItens.BuscarDevolucoesItens(Sessao.getConexaoBanco, 0, [pDevolucaoId]);

  vForm.qrlNFCodigoMovimento.Caption  := 'Devolu��o: ' + NFormat(vDevolucoes[0].devolucao_id);
  vForm.qrlNFPedido.Caption           := 'Pedido: ' + NFormat(vDevolucoes[0].orcamento_id);
  vForm.qrlNFCliente.Caption          := 'Cliente: ' + NFormat(vDevolucoes[0].ClienteId) + ' - ' + vDevolucoes[0].NomeCliente;
  vForm.qrlNFUsuarioDevolucao.Caption := 'Usu�rio dev.: ' + NFormat(vDevolucoes[0].usuario_cadastro_id) + ' - ' + vDevolucoes[0].NomeUsuarioDevolucao;
  vForm.qrlNFMotivoDevolucao.Lines.Text := 'Motivo: ' + vDevolucoes[0].MotivoDevolucao;

  vForm.qrDevolucaoId.Caption         := NFormat(vDevolucoes[0].devolucao_id);
  vForm.qrOrcamentoId.Caption         := NFormat(vDevolucoes[0].orcamento_id);
  vForm.qrUsuarioDevolucao.Caption    := NFormat(vDevolucoes[0].usuario_cadastro_id) + ' - ' + vDevolucoes[0].NomeUsuarioDevolucao;
  vForm.qrDataHoraDevolucao.Caption   := DFormatN(vDevolucoes[0].data_hora_devolucao) + ' �s ' + HFormatN(vDevolucoes[0].data_hora_devolucao);
  vForm.qrUsuarioConfirmacao.Caption  := NFormat(vDevolucoes[0].UsuarioConfirmacaoId) + ' - ' + vDevolucoes[0].NomeUsuarioConfirmacao;
  vForm.qrDataHoraConfirmacao.Caption := DFormatN(vDevolucoes[0].DataHoraConfirmacao) + ' �s ' + HFormatN(vDevolucoes[0].DataHoraConfirmacao);
  vForm.qrCondicaoPagamento.Caption   := vDevolucoes[0].NomeCondicaoPagamento;
  vForm.qrCliente.Caption             := NFormat(vDevolucoes[0].ClienteId) + ' - ' + vDevolucoes[0].NomeCliente;
  vForm.qrApelido.Caption             := vDevolucoes[0].Apelido;
  vForm.qrEnderecoCliente.Caption     := vDevolucoes[0].Logradouro + ' ' + vDevolucoes[0].Complemento + ' ' + vDevolucoes[0].Numero;
  vForm.qrBairroCliente.Caption       := vDevolucoes[0].NomeBairro;
  vForm.qrCidadeCliente.Caption       := vDevolucoes[0].NomeCidade + ' - ' + vDevolucoes[0].NomeEstado;
  vForm.qrCepCliente.Caption          := vDevolucoes[0].Cep;
  vForm.qrTelefoneCliente.Caption     := vDevolucoes[0].TelefonePrincipal;
  vForm.qrCelularCliente.Caption      := vDevolucoes[0].TelefoneCelular;

  vForm.qrTotalProdutos.Caption       := NFormat(vDevolucoes[0].valor_bruto);
  vForm.qrValorOutrasDespesas.Caption := NFormat(vDevolucoes[0].ValorOutrasDespesas);
  vForm.qrValorDesconto.Caption       := NFormat(vDevolucoes[0].valor_desconto);
  vForm.qrlGrValorFrete.Caption       := NFormat(vDevolucoes[0].valor_liquido);

  if vValorCreditoGerado > 0.00 then begin
    vForm.lbGrCreditoGerado.Enabled := True;
    vForm.qrlGrCreditoGerado.Enabled := True;

    vForm.qrlGrCreditoGerado.Caption       := NFormat(vValorCreditoGerado);
  end;

  try
    vForm.qrRelatorioNF.Height :=
      vForm.qrBandTitulo.Height + ( vForm.qrbndNFDetailBand1.Height * Length(vForm.FItens) ) +
      vForm.PageFooterBand1.Height + 10;

    vForm.qrRelatorioA4.Prepare;
    vForm.qrQtdePaginas.Caption := ' de ' + LPad(NFormat(vForm.qrRelatorioA4.QRPrinter.PageCount), 2, '0');

    vForm.Imprimir;
  finally
    vForm.Free;
  end;
end;

procedure TFormImpressaoComprovanteDevolucaoGrafico.qrBandaDetalhesBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrProdutoId.Caption     := NFormat(FItens[FPosicao].ProdutoId);
  qrNomeProduto.Caption   := FItens[FPosicao].NomeProduto;
  qrMarca.Caption         := FItens[FPosicao].NomeMarca;
  qrValorUnitario.Caption := NFormat(FItens[FPosicao].PrecoUnitario);
  qrQuantidade.Caption    := NFormatEstoque(FItens[FPosicao].QuantidadeDevolvidosEntregues + FItens[FPosicao].QuantidadeDevolvSemPrevisao + FItens[FPosicao].QuantidadeDevolvSemPrevisao);
  qrUnidade.Caption       := FItens[FPosicao].UnidadeVenda;
  qrValorTotal.Caption    := NFormat(FItens[FPosicao].ValorBruto);
end;

procedure TFormImpressaoComprovanteDevolucaoGrafico.qrbndNFDetailBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
var
  quantidadeImpressao: Double;
begin
  inherited;
  qrlNFNomeProduto.Caption    := _Biblioteca.NFormat(FItens[FPosicao].ProdutoId) + ' - ' + FItens[FPosicao].NomeProduto;
  if Length(qrlNFNomeProduto.Caption) > 38 then
    qrlNFNomeProduto.Font.Size := 6
  else
    qrlNFNomeProduto.Font.Size := 7;

  qrlNFMarca.Caption          := FItens[FPosicao].NomeMarca;

  if FItens[FPosicao].QuantidadeDevolvidosEntregues > 0 then
    quantidadeImpressao := FItens[FPosicao].QuantidadeDevolvidosEntregues
  else
    quantidadeImpressao := FItens[FPosicao].QuantidadeDevolvidosPendencias;

  if FItens[FPosicao].UnidadeEntregaId <> '' then begin
    qrlNFQuantidade.Caption := _Biblioteca.NFormatEstoque(quantidadeImpressao / FItens[FPosicao].MultiploVenda);
    qrlNFUnidade.Caption := FItens[FPosicao].UnidadeEntregaId;
  end
  else begin
    qrlNFQuantidade.Caption := _Biblioteca.NFormatEstoque(quantidadeImpressao);
    qrlNFUnidade.Caption := FItens[FPosicao].UnidadeVenda;
  end;

  qrlNFCodigoBarras.Caption   := FItens[FPosicao].CodigoBarras;
  qrlNFCodigoOriginal.Caption := FItens[FPosicao].CodigoOriginalFabricante;
end;

procedure TFormImpressaoComprovanteDevolucaoGrafico.qrNumeroFolhaPrint(
  sender: TObject; var Value: string);
begin
  inherited;
  Value := LPad(Value, 2, '0');
end;

procedure TFormImpressaoComprovanteDevolucaoGrafico.qrRelatorioA4BeforePrint(
  Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicao := -1;
end;

procedure TFormImpressaoComprovanteDevolucaoGrafico.qrRelatorioA4NeedData(
  Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicao);
  MoreData := FPosicao < Length(FItens);
end;

procedure TFormImpressaoComprovanteDevolucaoGrafico.qrRelatorioNFBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicao := -1;
end;

procedure TFormImpressaoComprovanteDevolucaoGrafico.qrRelatorioNFNeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicao);
  MoreData := FPosicao < Length(FItens);
end;

end.
