unit BuscarDadosCobrancaFinanceiro;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Sessao, _RecordsFinanceiros,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, FrameDadosCobranca, _Biblioteca,
  Vcl.StdCtrls;

type
  TFormBuscarDadosCobrancaFinanceiro = class(TFormHerancaFinalizar)
    FrDadosCobranca: TFrDadosCobranca;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Buscar(
  pValor: Double;
  pFormaPagamento: string;
  pNatureza: string;
  pDados: TArray<RecTitulosFinanceiros>
): TRetornoTelaFinalizar<TArray<RecTitulosFinanceiros>>;

implementation

{$R *.dfm}

function Buscar(
  pValor: Double;
  pFormaPagamento: string;
  pNatureza: string;
  pDados: TArray<RecTitulosFinanceiros>
): TRetornoTelaFinalizar<TArray<RecTitulosFinanceiros>>;
var
  vForm: TFormBuscarDadosCobrancaFinanceiro;
begin
  Result.Dados := pDados;

  vForm := TFormBuscarDadosCobrancaFinanceiro.Create(nil);

  vForm.FrDadosCobranca.FormaPagamento := pFormaPagamento;
  vForm.FrDadosCobranca.ValorPagar := pValor;
  vForm.FrDadosCobranca.Titulos := pDados;
  vForm.FrDadosCobranca.Natureza := pNatureza;

  if Result.Ok(vForm.ShowModal, True) then
    Result.Dados := vForm.FrDadosCobranca.Titulos;
end;

procedure TFormBuscarDadosCobrancaFinanceiro.FormCreate(Sender: TObject);
begin
  inherited;
  FrDadosCobranca.DataBase := Sessao.getDataHora;
end;

procedure TFormBuscarDadosCobrancaFinanceiro.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrDadosCobranca);
end;

procedure TFormBuscarDadosCobrancaFinanceiro.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrDadosCobranca.ExisteDiferenca then begin
    _Biblioteca.Exclamar('Os valores dos t�tulos n�o foram informados corretamente, verifique!');
    SetarFoco(FrDadosCobranca);
    Abort;
  end;
end;

end.
