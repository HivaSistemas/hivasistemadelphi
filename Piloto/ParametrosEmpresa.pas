unit ParametrosEmpresa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastro,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameEmpresas, Vcl.StdCtrls, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, Vcl.ComCtrls, FrameDiretorioArquivo,
  _Biblioteca, _ParametrosEmpresa, _RecordsCadastros, _Sessao, _RecordsEspeciais, _FaixasSimplesNacional, System.Math,
  FrameCondicoesPagamento, ComboBoxLuka, _BibliotecaGenerica, CheckBoxLuka, Vcl.Printers,
  GroupBoxLuka, StaticTextLuka, FramePlanosFinanceiros, FrameImagem,
  RadioGroupLuka, SpeedButtonLuka, Vcl.Menus;

type
  TFormParametrosEmpresa = class(TFormHerancaCadastro)
    FrEmpresa: TFrEmpresas;
    pcParametros: TPageControl;
    tsGeral: TTabSheet;
    tsDocumentosFiscais: TTabSheet;
    ckUtilizaNFe: TCheckBox;
    eNumeroUltimaNFeEmitida: TEditLuka;
    lbNumeroUltimaNFeEmitida: TLabel;
    ckPermiteAlterarFormaPagtoCaixa: TCheckBox;
    ckPermiteAlterarFormaPagtoCxFin: TCheckBox;
    lb2: TLabel;
    eSerieNFe: TEditLuka;
    tsVendas: TTabSheet;
    cbCorLucratividade1: TColorBox;
    lb3: TLabel;
    lb1: TLabel;
    cbCorLucratividade2: TColorBox;
    lb4: TLabel;
    cbCorLucratividade3: TColorBox;
    lb5: TLabel;
    cbCorLucratividade4: TColorBox;
    tsFiscal: TTabSheet;
    lb6: TLabel;
    cbRegimeTributario: TComboBoxLuka;
    lb7: TLabel;
    cbFaixaSimplesNacional: TComboBoxLuka;
    gbPercentuaisImpostos: TGroupBox;
    lb9: TLabel;
    ePercentualPIS: TEditLuka;
    lb8: TLabel;
    ePercentualCOFINS: TEditLuka;
    lb10: TLabel;
    ePercentualCSLL: TEditLuka;
    lb11: TLabel;
    ePercentualIRPJ: TEditLuka;
    eSerieNFCe: TEditLuka;
    eNumeroUltimaNFCeEmitida: TEditLuka;
    Label2: TLabel;
    Label3: TLabel;
    ckUtilizaNFCe: TCheckBox;
    Label4: TLabel;
    Label5: TLabel;
    cbTipoAmbienteNfe: TComboBoxLuka;
    cbTipoAmbienteNfce: TComboBoxLuka;
    meInformacoesComplementaresDocsEletronicos: TMemo;
    Label6: TLabel;
    tsExpedicao: TTabSheet;
    gbFormasPagamento: TGroupBoxLuka;
    ckTrabalhaEntregar: TCheckBox;
    ckTrabalhaRetirar: TCheckBox;
    ckTrabalhaSemPrevisao: TCheckBox;
    ckTrabalhaRetirarAto: TCheckBoxLuka;
    ckConfirmarSaidaProdutos: TCheckBox;
    tsEstoque: TTabSheet;
    lbl1: TLabel;
    cbIndiceSabado: TComboBoxLuka;
    cbIndiceDomingo: TComboBoxLuka;
    lbl2: TLabel;
    StaticTextLuka1: TStaticTextLuka;
    FrPlanosFinanceiros1: TFrPlanosFinanceiros;
    ckExigirNomeConsumidorFinalVenda: TCheckBox;
    ckExirgirDadosCatao: TCheckBox;
    FrCondicaoPagamentoPDV: TFrCondicoesPagamento;
    FrCondPagtoPadraoPesq: TFrCondicoesPagamento;
    FrCondicaoPagtoPadraoVendaAssistida: TFrCondicoesPagamento;
    FrCondicaoPagamentoConsProd2: TFrCondicoesPagamento;
    FrCondicaoPagamentoConsProd3: TFrCondicoesPagamento;
    FrCondicaoPagamentoConsProd4: TFrCondicoesPagamento;
    lb12: TLabel;
    cbTipoPrecoUtilizarVenda: TComboBoxLuka;
    FrLogoEmpresa: TFrImagem;
    StaticTextLuka2: TStaticTextLuka;
    lb13: TLabel;
    eValorMinDifTurnoContasRec: TEditLuka;
    ckTrabalhaSemPrevisaoEntregar: TCheckBox;
    ckImprimirLoteCompPagamento: TCheckBox;
    ckImprimirCodigoOriginalCompPagamento: TCheckBox;
    ckImprimirCodBarrasCompEnt: TCheckBox;
    rgTipoCustoVisualizarLucro: TRadioGroupLuka;
    lb14: TLabel;
    eQtdeDiasValidadeOrcamento: TEditLuka;
    tsFinanceiro: TTabSheet;
    lb15: TLabel;
    ePercentualJurosMensal: TEditLuka;
    ePercentualMulta: TEditLuka;
    lb16: TLabel;
    lb17: TLabel;
    eUltimoNsuConsultadoDistNFe: TEditLuka;
    lb18: TLabel;
    cbTipoRedirecionamentoNota: TComboBoxLuka;
    FrEmpresaDirecionarNotaRetirarAto: TFrEmpresas;
    FrEmpresaDirecionarNotaRetirar: TFrEmpresas;
    FrEmpresaDirecionarNotaEntregar: TFrEmpresas;
    ckTrabalharControleSeparacao: TCheckBoxLuka;
    lb19: TLabel;
    eValorFretePadraoVenda: TEditLuka;
    lb20: TLabel;
    eQtdeMinDiasEntrega: TEditLuka;
    ckObrigarVendedorSelTipoCobr: TCheckBox;
    rgTipoCustoAjusteEstoque: TRadioGroupLuka;
    gbTrabalharConferencia: TGroupBoxLuka;
    ckTrabalharConferenciaRetAto: TCheckBoxLuka;
    ckTrabalharConferenciaRetirar: TCheckBoxLuka;
    ckTrabalharConferenciaEntregar: TCheckBoxLuka;
    ckPermBaixarEntRecPendente: TCheckBoxLuka;
    lb21: TLabel;
    eUltimoNsuConsultadoDistCTe: TEditLuka;
    lb22: TLabel;
    eNumeroEstabelecimentoCielo: TEditLuka;
    lb23: TLabel;
    eQtdeDiasBloqVendaTitVenc: TEditLuka;
    StaticTextLuka3: TStaticTextLuka;
    StaticTextLuka4: TStaticTextLuka;
    StaticTextLuka5: TStaticTextLuka;
    ckGerarNfAgrupada: TCheckBoxLuka;
    ckCalcularFreteVendaKM: TCheckBox;
    ckHabilitarRecebimentoPix: TCheckBox;
    Label1: TLabel;
    mmInformacoesComplementaresComprovantePagamento: TMemo;
    ckBaixaContasReceberPixTurnoAberto: TCheckBox;
    ckAlertaEntregaPendente: TCheckBox;
    edtSeriaCertificado: TEditLuka;
    Label7: TLabel;
    Label8: TLabel;
    edtSenhaCertificado: TEdit;
    btnCertificado: TSpeedButton;
    cbxImpressoras: TComboBoxLuka;
    Label9: TLabel;
    Label10: TLabel;
    edtCSCNFCe: TEdit;
    Label11: TLabel;
    edtIdToken: TEdit;
    Label12: TLabel;
    edtModeloImpressora: TEdit;
    ppmCertificado: TPopupMenu;
    SalvarCertificado1: TMenuItem;
    ckPermitirDevolucaoAposPrazo: TCheckBoxLuka;
    Label13: TLabel;
    edtBuscarMenu: TButtonedEdit;
    lbParametros: TListBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ckUtilizaNFeClick(Sender: TObject);
    procedure sbGravarClick(Sender: TObject);
    procedure cbRegimeTributarioChange(Sender: TObject);
    procedure ckUtilizaNFCeClick(Sender: TObject);
    procedure btnCertificadoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SalvarCertificado1Click(Sender: TObject);
    procedure edtBuscarMenuEnter(Sender: TObject);
    procedure edtBuscarMenuChange(Sender: TObject);
    procedure edtBuscarMenuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure lbParametrosDblClick(Sender: TObject);
    procedure lbParametrosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    stlCertificado: TStrings;
    procedure EmpresaOnAposPesquisar(Sender: TObject);
    procedure FindComponentsByHint(const SearchText: string; ComponentList: TStrings);
    procedure FindComponentsByHintRecursive(ParentControl: TWinControl; const SearchText: string; ComponentList: TStrings);
    procedure FocusComponentByHintRecursive(ParentControl: TWinControl; const SearchText: string);
  protected
    procedure Modo(pEditando: Boolean); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

procedure TFormParametrosEmpresa.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if cbRegimeTributario.GetValor = 'SN' then begin
    if cbFaixaSimplesNacional.ItemIndex = -1 then begin
      Exclamar('� necess�rio definir a faixa do simples nacional!');
      _BibliotecaGenerica.SetarFoco(cbFaixaSimplesNacional);
      Abort;
    end;
  end;

  if cbTipoRedirecionamentoNota.AsString <> 'NRE' then begin
    if
      FrEmpresaDirecionarNotaRetirarAto.EstaVazio and
      FrEmpresaDirecionarNotaRetirar.EstaVazio and
      FrEmpresaDirecionarNotaEntregar.EstaVazio
    then begin
      _Biblioteca.Exclamar(
        'Quando o "Tipo de redirecionamento de notas" for diferente de "' + cbTipoRedirecionamentoNota.Items[0] + '" uma das empresas para redirecionamento deve estar parametrizada!'
      );
      _Biblioteca.SetarFoco(FrEmpresaDirecionarNotaRetirar);
      Abort;
    end;
  end;

  if ckCalcularFreteVendaKM.Checked then begin
    if eValorFretePadraoVenda.AsDouble <= 0 then begin
      _Biblioteca.Exclamar(
        'Quando o "C�lculo de frete na venda" for por "KM" o campo "Vlr. frete padr�o venda" deve ser maior que zero!'
      );
      _Biblioteca.SetarFoco(eValorFretePadraoVenda);
      Abort;
    end;
  end;
(*
  if ckUtilizaNFe.Checked then
  begin
    if edtSeriaCertificado.Trim.IsEmpty then
    begin
      _Biblioteca.Exclamar('S�rie certificado invalida!');
      _Biblioteca.SetarFoco(edtSeriaCertificado);
      Abort;
    end;
    if edtSenhaCertificado.Trim.IsEmpty then
    begin
      _Biblioteca.Exclamar('Senha certificado invalida!');
      _Biblioteca.SetarFoco(edtSenhaCertificado);
      Abort;
    end;
    if cbxImpressoras.ItemIndex = 0 then
    begin
      _Biblioteca.Exclamar('Impressora NFe invalida!');
      _Biblioteca.SetarFoco(cbxImpressoras);
      Abort;
    end;

    if stlCertificado.Text.IsEmpty then
    begin
      _Biblioteca.Exclamar('Certificado invalido!');
      Abort;
    end;

  end;

  if ckUtilizaNFCe.Checked then
  begin
    if edtCSCNFCe.Trim.IsEmpty then
    begin
      _Biblioteca.Exclamar('CSC NFC-e invalida!');
      _Biblioteca.SetarFoco(edtCSCNFCe);
      Abort;
    end;
    if edtIdToken.Trim.IsEmpty then
    begin
      _Biblioteca.Exclamar('Id Token invalido!');
      _Biblioteca.SetarFoco(edtIdToken);
      Abort;
    end;
    if edtModeloImpressora.Trim.IsEmpty then
    begin
      _Biblioteca.Exclamar('Modelo de impressora invalido!');
      _Biblioteca.SetarFoco(edtModeloImpressora);
      Abort;
    end;

  end;
*)
end;

procedure TFormParametrosEmpresa.btnCertificadoClick(Sender: TObject);
var
  vOpenDialog : TOpenDialog;
begin
  vOpenDialog := TOpenDialog.Create(self);
  try
    vOpenDialog.InitialDir := ExtractFileDir(Application.ExeName);
    vOpenDialog.DefaultExt := '*.pfx';
    vOpenDialog.Filter := 'Certificado|*.pfx';

    if vOpenDialog.Execute then
    begin
      stlCertificado.LoadFromFile(vOpenDialog.FileName);
      btnCertificado.Caption := 'Carregado...';
    end;
  finally
    vOpenDialog.Free;
  end;

end;

procedure TFormParametrosEmpresa.cbRegimeTributarioChange(Sender: TObject);
begin
  inherited;
  cbFaixaSimplesNacional.Enabled := (cbRegimeTributario.GetValor = 'SN');
  _Biblioteca.Habilitar([
    ePercentualPIS,
    ePercentualCOFINS,
    ePercentualCSLL,
    ePercentualIRPJ],
    cbRegimeTributario.GetValor <> 'SN'
  );
end;

procedure TFormParametrosEmpresa.ckUtilizaNFCeClick(Sender: TObject);
begin
  inherited;
  eNumeroUltimaNFCeEmitida.Enabled := ckUtilizaNFCe.Checked;
  eSerieNFCe.Enabled := ckUtilizaNFCe.Checked;
  cbTipoAmbienteNfce.Enabled := ckUtilizaNFCe.Checked;
  edtCSCNFCe.Enabled := ckUtilizaNFCe.Checked;
  edtIdToken.Enabled := ckUtilizaNFCe.Checked;
  edtModeloImpressora.Enabled := ckUtilizaNFCe.Checked;
  if not ckUtilizaNFCe.Checked then begin
    eNumeroUltimaNFCeEmitida.Clear;
    eSerieNFCe.Clear;
    cbTipoAmbienteNfce.ItemIndex := 0;
    edtCSCNFCe.Clear;
    edtIdToken.Clear;
    edtModeloImpressora.Clear;
  end;
end;

procedure TFormParametrosEmpresa.ckUtilizaNFeClick(Sender: TObject);
begin
  inherited;
  eNumeroUltimaNFeEmitida.Enabled := ckUtilizaNFe.Checked;
  eSerieNFe.Enabled := ckUtilizaNFe.Checked;
  cbTipoAmbienteNFe.Enabled := ckUtilizaNFe.Checked;
  edtSeriaCertificado.Enabled := ckUtilizaNFe.Checked;
  edtSenhaCertificado.Enabled := ckUtilizaNFe.Checked;
  cbxImpressoras.Enabled := ckUtilizaNFe.Checked;
  btnCertificado.Enabled := ckUtilizaNFe.Checked;
  if not ckUtilizaNFe.Checked then begin
    eNumeroUltimaNFeEmitida.Clear;
    eSerieNFe.Clear;
    cbTipoAmbienteNFe.ItemIndex := 0;
    edtSeriaCertificado.Clear;
    edtSenhaCertificado.Clear;
    stlCertificado.Clear;
  end;
end;

procedure TFormParametrosEmpresa.edtBuscarMenuChange(Sender: TObject);
var
  ComponentsFound: TStringList;
begin
  if (edtBuscarMenu.Text = '') then begin
    lbParametros.Visible := False;
    Exit;
  end;

  if Length(edtBuscarMenu.Text) < 3 then
    Exit;

  ComponentsFound := TStringList.Create;
  try
    // Busca todos os componentes com o Hint correspondente
    FindComponentsByHint(edtBuscarMenu.Text, ComponentsFound);

    lbParametros.Items.Clear;
    // Adiciona os componentes encontrados ao ListBox
    lbParametros.Items.Assign(ComponentsFound);

    lbParametros.Visible := True;
    lbParametros.BringToFront;
  finally
    ComponentsFound.Free;
  end;
end;

procedure TFormParametrosEmpresa.edtBuscarMenuEnter(Sender: TObject);
begin
  inherited;
  edtBuscarMenu.Clear;
  lbParametros.Clear;
end;

procedure TFormParametrosEmpresa.edtBuscarMenuKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) or (Key = VK_DOWN) then begin
    if (lbParametros.Count > 0) then begin
      lbParametros.SetFocus;
      lbParametros.ItemIndex := 0;
    end;
  end
  else if (Key = VK_ESCAPE) then begin
    edtBuscarMenu.Clear;
    lbParametros.Visible := False;
  end;
end;

procedure TFormParametrosEmpresa.EmpresaOnAposPesquisar(Sender: TObject);
var
  vParametro: TArray<RecParametrosEmpresa>;
begin
  Modo(True);
  FrEmpresa.Modo(False, False);

  vParametro := _ParametrosEmpresa.BuscarParametrosEmpresas(Sessao.getConexaoBanco, 0, [FrEmpresa.getEmpresa.EmpresaId]);
  if vParametro = nil then begin
    _Biblioteca.Exclamar('N�o foram encontrados os par�metros para a empresa selecionada!');
    sbDesfazerClick(nil);
    Exit;
  end;

  (* Aba Geral *)
  FrLogoEmpresa.setFoto(vParametro[0].LogoEmpresa);

  eValorMinDifTurnoContasRec.AsDouble      := vParametro[0].ValorMinDifTurnoContasRec;
  ckPermiteAlterarFormaPagtoCaixa.Checked  := (vParametro[0].PermiteAltFormaPagtoCaixa = 'S');
  ckPermiteAlterarFormaPagtoCxFin.Checked  := (vParametro[0].PermAltFormaPagtoCxFinanc = 'S');
  ckExirgirDadosCatao.Checked              := (vParametro[0].ExirgirDadosCatao = 'S');
  ckGerarNfAgrupada.Checked                := (vParametro[0].GerarNfAgrupada = 'S');
  ckPermitirDevolucaoAposPrazo.Checked     := (vParametro[0].PermitirDevolucaoAposPrazo = 'S');

  (* Aba Documentos fiscais *)
  ckUtilizaNFe.Checked                     := (vParametro[0].UtilizaNfe = 'S');
  ckUtilizaNFeClick(nil);
  eNumeroUltimaNFeEmitida.AsInt             := vParametro[0].NumeroUltNfeEmitida;
  eSerieNFe.AsInt                           := vParametro[0].SerieNFe;
  cbTipoAmbienteNfe.SetIndicePorValor(vParametro[0].tipoAmbienteNFe);
  edtSeriaCertificado.Text                  := vParametro[0].SerieCertificado;
  edtSenhaCertificado.Text                  := vParametro[0].SenhaCertificado;
  cbxImpressoras.SetIndicePorValor(vParametro[0].ImpressoraNFe);
  stlCertificado.LoadFromStream(vParametro[0].Certificado);
  btnCertificado.Caption := IIfStr(stlCertificado.Text.IsEmpty,'Vazio...','Carregado...');

  ckUtilizaNFCe.Checked := (vParametro[0].UtilizaNFCe = 'S');
  ckUtilizaNFCeClick(nil);
  eNumeroUltimaNFCeEmitida.AsInt            := vParametro[0].NumeroUltNfceEmitida;
  eSerieNFCe.AsInt                          := vParametro[0].SerieNFce;
  cbTipoAmbienteNfce.SetIndicePorValor(vParametro[0].tipoAmbienteNFCe);
  edtCSCNFCe.Text                           := vParametro[0].CscNFce;
  edtIdToken.Text                           := vParametro[0].IdToken;
  edtModeloImpressora.Text                  := vParametro[0].ModeloImpressora;

  meInformacoesComplementaresDocsEletronicos.Lines.Text := vParametro[0].infComplDocsEletronicos;
  mmInformacoesComplementaresComprovantePagamento.Lines.Text := vParametro[0].infComplCompPagamentos;
  eUltimoNsuConsultadoDistNFe.Text := vParametro[0].UltimoNsuConsultadoDistNFe;
  eUltimoNsuConsultadoDistCTe.Text := vParametro[0].UltimoNsuConsultadoDistCTe;

  (* Aba Or�amentos / vendas *)
  FrCondicaoPagamentoPDV.InserirDadoPorChave(vParametro[0].CondicaoPagamentoPDV, False);
  FrCondPagtoPadraoPesq.InserirDadoPorChave(vParametro[0].CondPagtoPadraoPesqVenda, False);
  FrCondicaoPagtoPadraoVendaAssistida.InserirDadoPorChave(vParametro[0].CondicaoPagtoPadVendaAssis, False);
  FrCondicaoPagamentoConsProd2.InserirDadoPorChave(vParametro[0].CondicaoPagamentoConsProd2, False);
  FrCondicaoPagamentoConsProd3.InserirDadoPorChave(vParametro[0].CondicaoPagamentoConsProd3, False);
  FrCondicaoPagamentoConsProd4.InserirDadoPorChave(vParametro[0].CondicaoPagamentoConsProd4, False);

  cbCorLucratividade1.Selected := vParametro[0].CorLucratividade1;
  cbCorLucratividade2.Selected := vParametro[0].CorLucratividade2;
  cbCorLucratividade3.Selected := vParametro[0].CorLucratividade3;
  cbCorLucratividade4.Selected := vParametro[0].CorLucratividade4;

  ckExigirNomeConsumidorFinalVenda.Checked := (vParametro[0].ExigirNomeConsFinalVenda = 'S');
  cbTipoPrecoUtilizarVenda.SetIndicePorValor( vParametro[0].TipoPrecoUtilizarVenda );
  rgTipoCustoVisualizarLucro.SetIndicePorValor( vParametro[0].TipoCustoVisLucroVenda );
  eQtdeDiasValidadeOrcamento.AsInt     := vParametro[0].QtdeDiasValidadeOrcamento;
  eValorFretePadraoVenda.AsDouble      := vParametro[0].ValorFretePadraoVenda;
  ckObrigarVendedorSelTipoCobr.Checked := vParametro[0].ObrigarVendedorSelTipoCobr = 'S';
  eQtdeDiasBloqVendaTitVenc.AsInt      := vParametro[0].QtdeDiasBloqVendaTitVenc;
  ckCalcularFreteVendaKM.Checked := (vParametro[0].CalcularFreteVendaPorKM = 'S');
  ckAlertaEntregaPendente.Checked := vParametro[0].AlertaEntregaPendente = 'S';
  ckHabilitarRecebimentoPix.Checked := (vParametro[0].HabilitarRecebimentoPix = 'S');
  ckBaixaContasReceberPixTurnoAberto.Checked := (vParametro[0].BaixaContasReceberPixTurnoAberto = 'S');

  (* Aba Fiscal *)
  cbRegimeTributario.SetIndicePorValor(vParametro[0].RegimeTributario);
  cbRegimeTributarioChange(Sender);
  cbFaixaSimplesNacional.SetIndicePorValor(_BibliotecaGenerica.NFormat(vParametro[0].AliquotaSimplesNacional));
  ePercentualPIS.AsDouble    := vParametro[0].AliquotaPIS;
  ePercentualCOFINS.AsDouble := vParametro[0].AliquotaCOFINS;
  ePercentualCSLL.AsDouble   := vParametro[0].AliquotaCSLL;
  ePercentualIRPJ.AsDouble   := vParametro[0].AliquotaIRPJ;

  (* Aba Logistica *)
  ckTrabalhaRetirarAto.Checked            := (vParametro[0].TrabalhaRetirarAto = 'S');
  ckTrabalhaRetirar.Checked               := (vParametro[0].TrabalhaRetirar = 'S');
  ckTrabalhaEntregar.Checked              := (vParametro[0].TrabalhaEntregar = 'S');
  ckTrabalhaSemPrevisao.Checked           := (vParametro[0].TrabalhaSemPrevisao = 'S');
  ckTrabalhaSemPrevisaoEntregar.Checked   := (vParametro[0].TrabalhaSemPrevisaoEntregar = 'S');
  ckConfirmarSaidaProdutos.Checked        := (vParametro[0].ConfirmarSaidaProdutos = 'S');
  ckTrabalharControleSeparacao.CheckedStr := vParametro[0].TrabalharControleSeparacao;
  ckImprimirLoteCompPagamento.Checked     := (vParametro[0].ImprimirLoteCompEntrega = 'S');
  ckImprimirCodigoOriginalCompPagamento.Checked := (vParametro[0].ImprimirCodOriginalCompEnt = 'S');
  ckImprimirCodBarrasCompEnt.Checked      := (vParametro[0].ImprimirCodBarrasCompEnt = 'S');
  cbTipoRedirecionamentoNota.AsString     := vParametro[0].TipoRedirecionamentoNota;
  FrEmpresaDirecionarNotaRetirarAto.InserirDadoPorChave(vParametro[0].EmpresaRedNotaRetirarAtoId, False);
  FrEmpresaDirecionarNotaRetirar.InserirDadoPorChave(vParametro[0].EmpresaRedNotaRetirarId, False);
  FrEmpresaDirecionarNotaEntregar.InserirDadoPorChave(vParametro[0].EmpresaRedNotaEntregarId, False);
  eQtdeMinDiasEntrega.AsInt               := vParametro[0].QuantidadeMinDiasEntrega;
  ckTrabalharConferenciaRetAto.CheckedStr   := vParametro[0].TrabalharConferenciaRetAto;
  ckTrabalharConferenciaRetirar.CheckedStr  := vParametro[0].TrabalharConferenciaRetirar;
  ckTrabalharConferenciaEntregar.CheckedStr := vParametro[0].TrabalharConferenciaEntrega;
  ckPermBaixarEntRecPendente.CheckedStr     := vParametro[0].PermBaixarEntRecPendente;


  (* Aba Estoque *)
  cbIndiceSabado.SetIndicePorValor( _Biblioteca.NFormat(vParametro[0].IndiceSabado, 1) );
  cbIndiceDomingo.SetIndicePorValor( _Biblioteca.NFormat(vParametro[0].IndiceDomingo, 1) );
  rgTipoCustoAjusteEstoque.SetIndicePorValor( vParametro[0].TipoCustoAjusteEstoque );

  (* Aba Financeiro *)
  ePercentualJurosMensal.AsDouble  := vParametro[0].PercentualJurosMensal;
  ePercentualMulta.AsDouble        := vParametro[0].PercentualMulta;
  eNumeroEstabelecimentoCielo.Text := vParametro[0].NumeroEstabelecimentoCielo;

  _Biblioteca.Destruir(TArray<TObject>(vParametro));
end;

procedure TFormParametrosEmpresa.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  stlCertificado.Free;
  inherited;

end;

procedure TFormParametrosEmpresa.FormCreate(Sender: TObject);
var
  i: Integer;
  vFaixasSimplesNacional: TArray<RecFaixaSimplesNacional>;
begin
  inherited;
  pcParametros.ActivePage := tsGeral;

  FrEmpresa.OnAposPesquisar := EmpresaOnAposPesquisar;
  FrEmpresa.Modo(True);

  cbFaixaSimplesNacional.Items.Clear;
  cbFaixaSimplesNacional.Valores.Clear;

  vFaixasSimplesNacional := _FaixasSimplesNacional.BuscarFaixaSimplesNacionals(Sessao.getConexaoBanco, 0, []);
  for i := Low(vFaixasSimplesNacional) to High(vFaixasSimplesNacional) do begin
    cbFaixaSimplesNacional.Items.Add(vFaixasSimplesNacional[i].Descricao);
    cbFaixaSimplesNacional.Valores.Add(_BibliotecaGenerica.NFormat(vFaixasSimplesNacional[i].Percentual));
  end;

  cbxImpressoras.Items := Printer.Printers;
  cbxImpressoras.Items.Insert(0,'<Impressoras>');
  cbxImpressoras.ItemIndex := 0;

  stlCertificado := TStringList.Create;

  lbParametros.Top := 341;
  lbParametros.Height := 121;
  lbParametros.Width := 593;
end;

procedure TFormParametrosEmpresa.FormShow(Sender: TObject);
begin
  inherited;
  FrEmpresa.SetFocus;
end;

procedure TFormParametrosEmpresa.lbParametrosDblClick(Sender: TObject);
var
  i: Integer;
  SelectedText: string;
  Component: TControl;
begin
  // Obt�m o texto selecionado no ListBox (pode ser Hint ou Caption)
  SelectedText := lbParametros.Items[lbParametros.ItemIndex];

  // Busca o componente correspondente ao texto selecionado
  for i := 0 to Self.ControlCount - 1 do
  begin
    Component := Self.Controls[i];

    // Se o componente for um checkbox, compara pelo Caption
    if (Component is TCheckBox) and (TCheckBox(Component).Caption = SelectedText) then
    begin
      // Se o componente est� em uma aba, ativa a aba correspondente
      if (Component.Parent is TTabSheet) and (Component.Parent.Parent is TPageControl) then
      begin
        TPageControl(Component.Parent.Parent).ActivePage := TTabSheet(Component.Parent);
      end;

      _Biblioteca.SetarFoco(TWinControl(Component));
      lbParametros.Visible := False;
      Exit;  // Sai ap�s focar no primeiro componente encontrado
    end
    else if (Component is TCheckBoxLuka) and (TCheckBoxLuka(Component).Caption = SelectedText) then
    begin
      // Se o componente est� em uma aba, ativa a aba correspondente
      if (Component.Parent is TTabSheet) and (Component.Parent.Parent is TPageControl) then
      begin
        TPageControl(Component.Parent.Parent).ActivePage := TTabSheet(Component.Parent);
      end;

      _Biblioteca.SetarFoco(TWinControl(Component));
      lbParametros.Visible := False;
      Exit;  // Sai ap�s focar no primeiro componente encontrado
    end
    // Se n�o for checkbox, compara pelo Hint
    else if (Component.Hint = SelectedText) then
    begin
      // Se o componente est� em uma aba, ativa a aba correspondente
      if (Component.Parent is TTabSheet) and (Component.Parent.Parent is TPageControl) then
      begin
        TPageControl(Component.Parent.Parent).ActivePage := TTabSheet(Component.Parent);
      end;

      _Biblioteca.SetarFoco(TWinControl(Component));
      lbParametros.Visible := False;
      Exit;  // Sai ap�s focar no primeiro componente encontrado
    end;

    // Busca recursiva para encontrar o componente
    if Component is TWinControl then
      FocusComponentByHintRecursive(TWinControl(Component), SelectedText);
  end;
end;

procedure TFormParametrosEmpresa.lbParametrosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) and (lbParametros.Items.Count > 0) then
    lbParametrosDblClick(Sender);
end;

procedure TFormParametrosEmpresa.Modo(pEditando: Boolean);
begin
  _Biblioteca.Habilitar([
    pcParametros,
    meInformacoesComplementaresDocsEletronicos,
    mmInformacoesComplementaresComprovantePagamento,
    FrLogoEmpresa,
    ckUtilizaNFCe,
    eValorMinDifTurnoContasRec,
    eNumeroUltimaNFCeEmitida,
    eSerieNFCe,
    cbTipoAmbienteNfce,
    eUltimoNsuConsultadoDistNFe,
    ckUtilizaNFe,
    eNumeroUltimaNFeEmitida,
    FrCondicaoPagamentoPDV,
    FrCondPagtoPadraoPesq,
    FrCondicaoPagtoPadraoVendaAssistida,
    ckPermiteAlterarFormaPagtoCaixa,
    ckPermiteAlterarFormaPagtoCxFin,
    ckExigirNomeConsumidorFinalVenda,
    ckCalcularFreteVendaKM,
    ckAlertaEntregaPendente,
    ckHabilitarRecebimentoPix,
    ckBaixaContasReceberPixTurnoAberto,
    ckExirgirDadosCatao,
    ckObrigarVendedorSelTipoCobr,
    cbCorLucratividade1,
    cbCorLucratividade2,
    cbCorLucratividade3,
    cbCorLucratividade4,
    ePercentualPIS,
    ePercentualCOFINS,
    ePercentualCSLL,
    ePercentualIRPJ,
    ckTrabalhaRetirarAto,
    ckTrabalhaRetirar,
    ckTrabalhaEntregar,
    ckTrabalhaSemPrevisao,
    ckTrabalhaSemPrevisaoEntregar,
    ckTrabalharConferenciaRetAto,
    ckTrabalharConferenciaRetirar,
    ckTrabalharConferenciaEntregar,
    ckConfirmarSaidaProdutos,
    ckPermBaixarEntRecPendente,
    ckTrabalharControleSeparacao,
    FrCondicaoPagamentoConsProd2,
    FrCondicaoPagamentoConsProd3,
    FrCondicaoPagamentoConsProd4,
    ckImprimirLoteCompPagamento,
    ckImprimirCodigoOriginalCompPagamento,
    ckImprimirCodBarrasCompEnt,
    cbIndiceSabado,
    cbIndiceDomingo,
    cbTipoPrecoUtilizarVenda,
    rgTipoCustoVisualizarLucro,
    rgTipoCustoAjusteEstoque,
    eQtdeDiasValidadeOrcamento,
    cbTipoRedirecionamentoNota,
    FrEmpresaDirecionarNotaRetirarAto,
    FrEmpresaDirecionarNotaRetirar,
    FrEmpresaDirecionarNotaEntregar,
    eValorFretePadraoVenda,
    eQtdeMinDiasEntrega,
    rgTipoCustoAjusteEstoque,
    eNumeroEstabelecimentoCielo,
    ckGerarNfAgrupada,
    ckPermitirDevolucaoAposPrazo,
    eQtdeDiasBloqVendaTitVenc,
    edtSeriaCertificado,
    edtSenhaCertificado,
    btnCertificado,
    cbxImpressoras,
    edtCSCNFCe,
    edtIdToken,
    edtModeloImpressora],
    pEditando
  );

  _Biblioteca.Habilitar([cbFaixaSimplesNacional], False);

  sbGravar.Enabled := pEditando;
  sbDesfazer.Enabled := pEditando;
  if not pEditando then begin
    FrEmpresa.Modo(True);
    _BibliotecaGenerica.SetarFoco(FrEmpresa);
  end;
end;

procedure TFormParametrosEmpresa.SalvarCertificado1Click(Sender: TObject);
var
  vSaveDialog : TSaveDialog;
begin
  vSaveDialog := TSaveDialog.Create(self);
  try
    vSaveDialog.InitialDir := ExtractFileDir(Application.ExeName);
    vSaveDialog.DefaultExt := '*.pfx';
    vSaveDialog.Filter := 'Certificado|*.pfx';

    if vSaveDialog.Execute then
    begin
      stlCertificado.SaveToFile(vSaveDialog.FileName);
      btnCertificado.Caption := 'Carregado...';
    end;
  finally
    vSaveDialog.Free;
  end;
end;

procedure TFormParametrosEmpresa.sbGravarClick(Sender: TObject);
var
  vRetorno: RecRetornoBD;
  vCondicaoPagamentoPDV: Integer;
  vCondPagtoPadraoPesqVenda: Integer;
  vCondPadraoVendaAssistida: Integer;

  vCondicaoPagamentoConsProd2: Integer;
  vCondicaoPagamentoConsProd3: Integer;
  vCondicaoPagamentoConsProd4: Integer;

  vEmpresaRedNotaRetirarAtoId: Integer;
  vEmpresaRedNotaRetirarId: Integer;
  vEmpresaRedNotaEntregarId: Integer;

  vCertificado: TMemoryStream;
begin
  VerificarRegistro(Sender);

  Sessao.getConexaoBanco.IniciarTransacao;

  vCondicaoPagamentoPDV := 0;
  if not FrCondicaoPagamentoPDV.EstaVazio then
    vCondicaoPagamentoPDV := FrCondicaoPagamentoPDV.getDados().condicao_id;

  vCondPagtoPadraoPesqVenda := 0;
  if not FrCondPagtoPadraoPesq.EstaVazio then
    vCondPagtoPadraoPesqVenda := FrCondPagtoPadraoPesq.getDados().condicao_id;

  vCondPadraoVendaAssistida := 0;
  if not FrCondicaoPagtoPadraoVendaAssistida.EstaVazio then
    vCondPadraoVendaAssistida := FrCondicaoPagtoPadraoVendaAssistida.getDados().condicao_id;

  vCondicaoPagamentoConsProd2 := 0;
  if not FrCondicaoPagamentoConsProd2.EstaVazio then
    vCondicaoPagamentoConsProd2 := FrCondicaoPagamentoConsProd2.getDados().condicao_id;

  vCondicaoPagamentoConsProd3 := 0;
  if not FrCondicaoPagamentoConsProd3.EstaVazio then
    vCondicaoPagamentoConsProd3 := FrCondicaoPagamentoConsProd3.getDados().condicao_id;

  vCondicaoPagamentoConsProd4 := 0;
  if not FrCondicaoPagamentoConsProd4.EstaVazio then
    vCondicaoPagamentoConsProd4 := FrCondicaoPagamentoConsProd4.getDados().condicao_id;

  vEmpresaRedNotaRetirarAtoId := 0;
  if not FrEmpresaDirecionarNotaRetirarAto.EstaVazio then
    vEmpresaRedNotaRetirarAtoId := FrEmpresaDirecionarNotaRetirarAto.getEmpresa().EmpresaId;

  vEmpresaRedNotaRetirarId := 0;
  if not FrEmpresaDirecionarNotaRetirar.EstaVazio then
    vEmpresaRedNotaRetirarId := FrEmpresaDirecionarNotaRetirar.getEmpresa().EmpresaId;

  vEmpresaRedNotaEntregarId := 0;
  if not FrEmpresaDirecionarNotaEntregar.EstaVazio then
    vEmpresaRedNotaEntregarId := FrEmpresaDirecionarNotaEntregar.getEmpresa().EmpresaId;

  vCertificado := TMemoryStream.Create;
  stlCertificado.SaveToStream(vCertificado);

  vRetorno :=
    _ParametrosEmpresa.AtualizarParametrosEmpresa(
     Sessao.getConexaoBanco,
     FrEmpresa.getEmpresa().EmpresaId,
     _BibliotecaGenerica.ToChar(ckUtilizaNFe),
     eValorMinDifTurnoContasRec.AsDouble,
     _BibliotecaGenerica.ToChar(ckExigirNomeConsumidorFinalVenda),
     _BibliotecaGenerica.ToChar(ckExirgirDadosCatao),
     _BibliotecaGenerica.ToChar(ckPermiteAlterarFormaPagtoCaixa),
     _BibliotecaGenerica.ToChar(ckPermiteAlterarFormaPagtoCxFin),
     _BibliotecaGenerica.zvl(eNumeroUltimaNFeEmitida.AsInt, -1),
     vCondicaoPagamentoPDV,
     _BibliotecaGenerica.zvl(eSerieNFe.AsInt, -1),
     eUltimoNsuConsultadoDistNFe.Text,
     eUltimoNsuConsultadoDistCTe.Text,
     cbCorLucratividade1.Selected,
     cbCorLucratividade2.Selected,
     cbCorLucratividade3.Selected,
     cbCorLucratividade4.Selected,
     cbRegimeTributario.GetValor,
     IfThen(cbRegimeTributario.GetValor = 'SN', SFormatDouble(cbFaixaSimplesNacional.GetValor), 0),
     ePercentualPIS.AsDouble,
     ePercentualCOFINS.AsDouble,
     ePercentualCSLL.AsDouble,
     ePercentualIRPJ.AsDouble,
     vCondPagtoPadraoPesqVenda,
     _BibliotecaGenerica.ToChar(ckUtilizaNFCe),
     _BibliotecaGenerica.zvl(eNumeroUltimaNFCeEmitida.AsInt, -1),
     _BibliotecaGenerica.zvl(eSerieNFCe.AsInt, -1),
     cbTipoAmbienteNfe.GetValor,
     cbTipoAmbienteNfce.GetValor,
     meInformacoesComplementaresDocsEletronicos.Lines.Text,
     _BibliotecaGenerica.ToChar(ckTrabalhaRetirarAto),
     _BibliotecaGenerica.ToChar(ckTrabalhaRetirar),
     _BibliotecaGenerica.ToChar(ckTrabalhaEntregar),
     _BibliotecaGenerica.ToChar(ckTrabalhaSemPrevisao),
     _BibliotecaGenerica.ToChar(ckTrabalhaSemPrevisaoEntregar),
     _BibliotecaGenerica.ToChar(ckConfirmarSaidaProdutos),
     ckTrabalharControleSeparacao.CheckedStr,
     vCondPadraoVendaAssistida,
     vCondicaoPagamentoConsProd2,
     vCondicaoPagamentoConsProd3,
     vCondicaoPagamentoConsProd4,
     SFormatDouble(cbIndiceSabado.GetValor),
     SFormatDouble(cbIndiceDomingo.GetValor),
     cbTipoPrecoUtilizarVenda.GetValor,
     FrLogoEmpresa.getImagem,
     _Biblioteca.ToChar( ckImprimirLoteCompPagamento.Checked ),
     _Biblioteca.ToChar( ckImprimirCodigoOriginalCompPagamento.Checked ),
     _Biblioteca.ToChar( ckImprimirCodBarrasCompEnt.Checked ),
     rgTipoCustoVisualizarLucro.GetValor,
     eQtdeDiasValidadeOrcamento.AsInt,
     ePercentualJurosMensal.AsDouble,
     ePercentualMulta.AsDouble,
     cbTipoRedirecionamentoNota.AsString,
     vEmpresaRedNotaRetirarAtoId,
     vEmpresaRedNotaRetirarId,
     vEmpresaRedNotaEntregarId,
     eValorFretePadraoVenda.AsDouble,
     eQtdeMinDiasEntrega.AsInt,
     _Biblioteca.ToChar(ckObrigarVendedorSelTipoCobr),
     rgTipoCustoAjusteEstoque.GetValor,
     ckTrabalharConferenciaRetAto.CheckedStr,
     ckTrabalharConferenciaRetirar.CheckedStr,
     ckTrabalharConferenciaEntregar.CheckedStr,
     ckPermBaixarEntRecPendente.CheckedStr,
     eNumeroEstabelecimentoCielo.Text,
     _BibliotecaGenerica.ToChar(ckGerarNfAgrupada),
     eQtdeDiasBloqVendaTitVenc.AsInt,
     _BibliotecaGenerica.ToChar(ckCalcularFreteVendaKM),
     _BibliotecaGenerica.ToChar(ckHabilitarRecebimentoPix),
     _BibliotecaGenerica.ToChar(ckBaixaContasReceberPixTurnoAberto),
     False,
     mmInformacoesComplementaresComprovantePagamento.Lines.Text,
     _BibliotecaGenerica.ToChar(ckAlertaEntregaPendente),
     edtSeriaCertificado.Text,
     edtSenhaCertificado.Text,
     cbxImpressoras.Text,
     edtCSCNFCe.Text,
     edtIdToken.Text,
     edtModeloImpressora.Text,
     vCertificado,
     ckPermitirDevolucaoAposPrazo.CheckedStr
    );

  if vRetorno.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Exit;
  end;

  vRetorno := Sessao.AtualizarParametrosEmpresasBanco;
  if vRetorno.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    _Biblioteca.Exclamar('Problema ao atulizar os parametros de empresa, feche o sistema e abra novamente: ' + vRetorno.MensagemErro);
    Exit;
  end;

  Sessao.getConexaoBanco.FinalizarTransacao;

  _Biblioteca.Informar(coAlteracaoRegistroSucesso);

  sbDesfazerClick(Sender);
  _BibliotecaGenerica.SetarFoco(FrEmpresa);
end;

procedure TFormParametrosEmpresa.FindComponentsByHint(const SearchText: string; ComponentList: TStrings);
var
  i: Integer;
  Component: TControl;
begin
  for i := 0 to Self.ControlCount - 1 do
  begin
    Component := Self.Controls[i];

    // Se o componente for um checkbox, busca pelo Caption
    if (Component is TCheckBox) and (Pos(LowerCase(SearchText), LowerCase(TCheckBox(Component).Caption)) > 0) then
    begin
      ComponentList.Add(TCheckBox(Component).Caption); // Adiciona o Caption do checkbox
    end
    else if (Component is TCheckBoxLuka) and (Pos(LowerCase(SearchText), LowerCase(TCheckBoxLuka(Component).Caption)) > 0) then
    begin
      ComponentList.Add(TCheckBoxLuka(Component).Caption); // Adiciona o Caption do checkbox
    end
    // Se n�o for checkbox, busca pelo Hint normalmente
    else if (Component.Hint <> '') and (Pos(LowerCase(SearchText), LowerCase(Component.Hint)) > 0) then
    begin
      ComponentList.Add(Component.Hint); // Adiciona o Hint dos outros componentes
    end;

    // Se o componente for um container, fa�a busca recursiva
    if Component is TWinControl then
      FindComponentsByHintRecursive(TWinControl(Component), SearchText, ComponentList);
  end;
end;

procedure TFormParametrosEmpresa.FindComponentsByHintRecursive(ParentControl: TWinControl; const SearchText: string; ComponentList: TStrings);
var
  i: Integer;
  ChildControl: TControl;
begin
  for i := 0 to ParentControl.ControlCount - 1 do
  begin
    ChildControl := ParentControl.Controls[i];

    // Se o componente for um checkbox, busca pelo Caption
    if (ChildControl is TCheckBox) and (Pos(LowerCase(SearchText), LowerCase(TCheckBox(ChildControl).Caption)) > 0) then
    begin
      ComponentList.Add(TCheckBox(ChildControl).Caption); // Adiciona o Caption do checkbox
    end
    // Se n�o for checkbox, busca pelo Hint normalmente
    else if (ChildControl.Hint <> '') and (Pos(LowerCase(SearchText), LowerCase(ChildControl.Hint)) > 0) then
    begin
      ComponentList.Add(ChildControl.Hint); // Adiciona o Hint dos outros componentes
    end;

    // Busca recursiva em containers filhos
    if ChildControl is TWinControl then
      FindComponentsByHintRecursive(TWinControl(ChildControl), SearchText, ComponentList);
  end;
end;

procedure TFormParametrosEmpresa.FocusComponentByHintRecursive(ParentControl: TWinControl; const SearchText: string);
var
  i: Integer;
  ChildControl: TControl;
begin
  for i := 0 to ParentControl.ControlCount - 1 do
  begin
    ChildControl := ParentControl.Controls[i];

    // Se for checkbox, busca pelo Caption
    if (ChildControl is TCheckBox) and (TCheckBox(ChildControl).Caption = SearchText) then
    begin
      // Se o componente est� em uma aba, ativa a aba correta
      if (ChildControl.Parent is TTabSheet) and (ChildControl.Parent.Parent is TPageControl) then
      begin
        TPageControl(ChildControl.Parent.Parent).ActivePage := TTabSheet(ChildControl.Parent);
      end;

      _Biblioteca.SetarFoco(TWinControl(ChildControl));
      lbParametros.Visible := False;
      Exit;
    end
    else if (ChildControl is TCheckBoxLuka) and (TCheckBoxLuka(ChildControl).Caption = SearchText) then
    begin
      // Se o componente est� em uma aba, ativa a aba correta
      if (ChildControl.Parent is TTabSheet) and (ChildControl.Parent.Parent is TPageControl) then
      begin
        TPageControl(ChildControl.Parent.Parent).ActivePage := TTabSheet(ChildControl.Parent);
      end;

      _Biblioteca.SetarFoco(TWinControl(ChildControl));
      lbParametros.Visible := False;
      Exit;
    end
    // Caso contr�rio, busca pelo Hint
    else if (ChildControl.Hint = SearchText) then
    begin
      // Se o componente est� em uma aba, ativa a aba correta
      if (ChildControl.Parent is TTabSheet) and (ChildControl.Parent.Parent is TPageControl) then
      begin
        TPageControl(ChildControl.Parent.Parent).ActivePage := TTabSheet(ChildControl.Parent);
      end;

      _Biblioteca.SetarFoco(TWinControl(ChildControl));
      lbParametros.Visible := False;
      Exit;
    end;

    if ChildControl is TWinControl then
      FocusComponentByHintRecursive(TWinControl(ChildControl), SearchText);
  end;
end;

end.
