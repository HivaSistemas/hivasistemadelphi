inherited FormRelacaoAjusteEstoque: TFormRelacaoAjusteEstoque
  Caption = 'Rela'#231#227'o corre'#231#245'es de estoque'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    inherited sbImprimir: TSpeedButton
      Visible = False
    end
    object SpeedButton1: TSpeedButton [2]
      Left = 3
      Top = 221
      Width = 110
      Height = 40
      BiDiMode = bdLeftToRight
      Caption = 'Emitir Nota'
      Flat = True
      NumGlyphs = 2
      ParentBiDiMode = False
      OnClick = SpeedButton1Click
    end
  end
  inherited pcDados: TPageControl
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object Label2: TLabel
        Left = 333
        Top = 185
        Width = 115
        Height = 14
        Caption = 'Tipo de est. ajustado'
      end
      object Label1: TLabel
        Left = 333
        Top = 136
        Width = 73
        Height = 14
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alCustom
        Caption = 'Agrupamento'
      end
      inline FrEmpresas: TFrEmpresas
        Left = 2
        Top = 1
        Width = 320
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 1
        ExplicitWidth = 320
        inherited sgPesquisa: TGridLuka
          Width = 295
          ExplicitWidth = 295
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 53
            ExplicitWidth = 53
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          ExplicitLeft = 295
        end
      end
      inline FrMotivosAjusteEstoque: TFrMotivosAjusteEstoque
        Left = 2
        Top = 86
        Width = 320
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 86
        ExplicitWidth = 320
        inherited sgPesquisa: TGridLuka
          Width = 295
          ExplicitWidth = 295
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 161
            ExplicitWidth = 161
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          ExplicitLeft = 295
        end
      end
      inline FrDataCadastro: TFrDataInicialFinal
        Left = 331
        Top = 1
        Width = 198
        Height = 41
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 331
        ExplicitTop = 1
        ExplicitWidth = 198
        inherited Label1: TLabel
          Width = 93
          Height = 14
          ExplicitWidth = 93
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrUsuarioInsercao: TFrFuncionarios
        Left = 2
        Top = 256
        Width = 320
        Height = 97
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 256
        ExplicitWidth = 320
        inherited sgPesquisa: TGridLuka
          Width = 295
          ExplicitWidth = 295
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 110
            Caption = 'Usu'#225'rio de inser'#231#227'o'
            ExplicitWidth = 110
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          ExplicitLeft = 295
        end
        inherited ckBuscarSomenteAtivos: TCheckBox
          Checked = False
          State = cbUnchecked
        end
      end
      inline FrCodigoAjuste: TFrNumeros
        Left = 333
        Top = 48
        Width = 134
        Height = 72
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 4
        TabStop = True
        ExplicitLeft = 333
        ExplicitTop = 48
        inherited pnDescricao: TPanel
          Caption = 'C'#243'digo do ajuste'
        end
      end
      inline FrProdutos: TFrProdutos
        Left = 2
        Top = 171
        Width = 320
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 5
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 171
        ExplicitWidth = 320
        inherited sgPesquisa: TGridLuka
          Width = 295
          ExplicitWidth = 295
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 48
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          ExplicitLeft = 295
        end
        inherited ckSomenteAtivos: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited ckFiltroExtra: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      object cbTipoEstoqueAjustado: TComboBoxLuka
        Left = 333
        Top = 201
        Width = 134
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 6
        Text = 'Todos'
        Items.Strings = (
          'Todos'
          'F'#237'sico'
          'Fiscal')
        Valores.Strings = (
          'Todos'
          'Fisico'
          'Fiscal')
        AsInt = 0
        AsString = 'Todos'
      end
      object cbAgrupamento: TComboBoxLuka
        Left = 333
        Top = 153
        Width = 134
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 7
        Text = 'Nenhum'
        Items.Strings = (
          'Nenhum'
          'Motivo de ajuste')
        Valores.Strings = (
          'NEN'
          'MOT')
        AsInt = 0
        AsString = 'NEN'
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object splSeparador: TSplitter
        Left = 0
        Top = 212
        Width = 884
        Height = 6
        Cursor = crVSplit
        Align = alTop
        ResizeStyle = rsUpdate
      end
      object StaticTextLuka1: TStaticTextLuka
        Left = 0
        Top = 0
        Width = 884
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Ajustes'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object sgAjustes: TGridLuka
        Left = 0
        Top = 17
        Width = 884
        Height = 195
        Align = alTop
        ColCount = 9
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
        TabOrder = 1
        OnClick = sgAjustesClick
        OnDblClick = sgAjustesDblClick
        OnDrawCell = sgAjustesDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Ajuste'
          'Empresa'
          'Tipo'
          'Valor total'
          'Data/hora ajuste'
          'Usuario do ajuste'
          'Motivo do ajuste'
          'Tipo de est. ajustado'
          'Observa'#231#227'o')
        OnGetCellColor = sgAjustesGetCellColor
        Grid3D = False
        RealColCount = 9
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          45
          190
          144
          89
          114
          129
          136
          128
          312)
      end
      object StaticTextLuka2: TStaticTextLuka
        Left = 0
        Top = 218
        Width = 884
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Itens do ajuste selecionado'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object sgItens: TGridLuka
        Left = 0
        Top = 235
        Width = 884
        Height = 245
        Align = alClient
        ColCount = 9
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 3
        OnDrawCell = sgItensDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Produto'
          'Nome'
          'Marca'
          'Natureza'
          'Quantidade'
          'Qtde. Informada'
          'Und.'
          'Pre'#231'o unit.'
          'Valor total'
          'Qtde. Informada')
        OnGetCellColor = sgItensGetCellColor
        Grid3D = False
        RealColCount = 14
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          54
          253
          172
          90
          80
          98
          78
          85
          64)
      end
      object Panel1: TPanel
        Left = 0
        Top = 480
        Width = 884
        Height = 38
        Align = alBottom
        TabOrder = 4
        DesignSize = (
          884
          38)
        object st4: TStaticText
          Left = 520
          Top = 2
          Width = 188
          Height = 16
          Alignment = taCenter
          Anchors = [akRight, akBottom]
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Valor total de sa'#237'das'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          Transparent = False
        end
        object st1: TStaticText
          Left = 707
          Top = 2
          Width = 174
          Height = 16
          Alignment = taCenter
          Anchors = [akRight, akBottom]
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Valor total de entradas'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 1
          Transparent = False
        end
        object stTotalSaidas: TStaticText
          Left = 520
          Top = 17
          Width = 188
          Height = 16
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Alignment = taCenter
          Anchors = [akRight, akBottom]
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 2
          Transparent = False
        end
        object stTotalEntradas: TStaticText
          Left = 707
          Top = 17
          Width = 174
          Height = 16
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Alignment = taCenter
          Anchors = [akRight, akBottom]
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 3
          Transparent = False
        end
      end
    end
  end
end
