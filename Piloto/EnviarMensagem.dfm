inherited FormEnviarMensagem: TFormEnviarMensagem
  Caption = 'Enviar mensagem'
  ClientHeight = 403
  ClientWidth = 794
  Position = poMainFormCenter
  Visible = False
  OnShow = FormShow
  ExplicitWidth = 800
  ExplicitHeight = 432
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Width = 207
    Height = 403
    BevelKind = bkNone
    ExplicitWidth = 207
    ExplicitHeight = 403
    object cpOpcoes: TCategoryPanelGroup
      Left = 0
      Top = 0
      Width = 207
      Height = 403
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      VertScrollBar.Smooth = True
      VertScrollBar.Style = ssFlat
      VertScrollBar.Tracking = True
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      ChevronColor = clTeal
      ChevronHotColor = clSkyBlue
      Color = clWhite
      GradientBaseColor = clWhite
      GradientColor = clWhite
      HeaderFont.Charset = DEFAULT_CHARSET
      HeaderFont.Color = clBlack
      HeaderFont.Height = -11
      HeaderFont.Name = 'Tahoma'
      HeaderFont.Style = []
      HeaderHeight = 18
      TabOrder = 0
      object cpCargos: TCategoryPanel
        Top = 222
        Height = 102
        Caption = 'Cargos'
        Color = clWhite
        TabOrder = 0
        inline FrCargosFuncionarios: TFrCargosFuncionarios
          Left = 2
          Top = 1
          Width = 202
          Height = 81
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = []
          ParentBackground = False
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          TabStop = True
          ExplicitLeft = 2
          ExplicitTop = 1
          ExplicitWidth = 202
          inherited sgPesquisa: TGridLuka
            Width = 177
            ExplicitWidth = 177
          end
          inherited PnTitulos: TPanel
            Width = 202
            ExplicitWidth = 202
            inherited lbNomePesquisa: TLabel
              Width = 36
              ExplicitWidth = 36
              ExplicitHeight = 14
            end
            inherited pnSuprimir: TPanel
              Left = 97
              ExplicitLeft = 97
            end
          end
          inherited pnPesquisa: TPanel
            Left = 177
            ExplicitLeft = 177
          end
        end
      end
      object cpEmpresas: TCategoryPanel
        Top = 118
        Height = 104
        Caption = 'Empresas'
        Color = clWhite
        TabOrder = 1
        Visible = False
        inline FrEmpresas: TFrEmpresas
          Left = 2
          Top = 3
          Width = 201
          Height = 81
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = []
          ParentBackground = False
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          TabStop = True
          ExplicitLeft = 2
          ExplicitTop = 3
          ExplicitWidth = 201
          inherited sgPesquisa: TGridLuka
            Width = 176
            ExplicitWidth = 176
          end
          inherited PnTitulos: TPanel
            Width = 201
            ExplicitWidth = 201
            inherited lbNomePesquisa: TLabel
              Width = 53
              ExplicitWidth = 53
              ExplicitHeight = 14
            end
            inherited pnSuprimir: TPanel
              Left = 96
              ExplicitLeft = 96
            end
          end
          inherited pnPesquisa: TPanel
            Left = 176
            ExplicitLeft = 176
          end
        end
      end
      object cpUsuarios: TCategoryPanel
        Top = 0
        Height = 118
        Caption = 'Usu'#225'rios'
        Color = clWhite
        TabOrder = 2
        inline FrFuncionarios: TFrFuncionarios
          Left = 2
          Top = 0
          Width = 201
          Height = 97
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = []
          ParentBackground = False
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          TabStop = True
          ExplicitLeft = 2
          ExplicitWidth = 201
          inherited sgPesquisa: TGridLuka
            Width = 176
            ExplicitWidth = 176
          end
          inherited PnTitulos: TPanel
            Width = 201
            ExplicitWidth = 201
            inherited lbNomePesquisa: TLabel
              Width = 71
              ExplicitWidth = 71
              ExplicitHeight = 14
            end
            inherited CkChaveUnica: TCheckBox
              Checked = False
              State = cbUnchecked
            end
            inherited pnSuprimir: TPanel
              Left = 96
              ExplicitLeft = 96
            end
          end
          inherited pnPesquisa: TPanel
            Left = 176
            ExplicitLeft = 176
          end
        end
      end
    end
  end
  object pn1: TPanel
    Left = 207
    Top = 0
    Width = 587
    Height = 403
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      587
      403)
    object lb1: TLabel
      Left = 5
      Top = 5
      Width = 44
      Height = 14
      Caption = 'Assunto'
    end
    object lb2: TLabel
      Left = 5
      Top = 48
      Width = 60
      Height = 14
      Caption = 'Mensagem'
    end
    object sbEnviarMensagem: TSpeedButtonLuka
      Left = 467
      Top = 372
      Width = 117
      Height = 28
      Anchors = [akTop, akRight]
      Caption = 'Enviar (F9)'
      Flat = True
      OnClick = sbEnviarMensagemClick
      TagImagem = 0
      PedirCertificacao = False
      PermitirAutOutroUsuario = False
    end
    object eAssunto: TEditLuka
      Left = 5
      Top = 20
      Width = 579
      Height = 22
      Anchors = [akLeft, akTop, akRight]
      CharCase = ecUpperCase
      MaxLength = 60
      TabOrder = 0
      TipoCampo = tcTexto
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 0
      NaoAceitarEspaco = False
    end
    object eMensagem: TMemo
      Left = 5
      Top = 63
      Width = 579
      Height = 305
      Anchors = [akLeft, akTop, akRight]
      Color = clCream
      MaxLength = 2000
      TabOrder = 1
    end
    object ckPermitirResposta: TCheckBox
      Left = 5
      Top = 375
      Width = 117
      Height = 17
      Caption = 'Permitir resposta'
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
    object ckImportante: TCheckBox
      Left = 301
      Top = 374
      Width = 87
      Height = 17
      Caption = 'Importante'
      TabOrder = 3
      Visible = False
    end
  end
end
