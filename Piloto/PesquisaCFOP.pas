unit PesquisaCFOP;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _Biblioteca, _Sessao,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, System.Math, _RecordsCadastros, _Cfop,
  Vcl.ExtCtrls;

type
  TFormPesquisaCFOP = class(TFormHerancaPesquisas)
    lb2: TLabel;
    eDescricaoCFOP: TMemo;
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgPesquisaClick(Sender: TObject);
  private
    FTipo: TTipoCFOP;
    FInicioCFOP: string;
    FSomenteAtivos: Boolean;
    FSomenteEntradaMercadorias: Boolean;
    FSomenteEntradaServicos: Boolean;
    FSomenteEntradaConhecFrete: Boolean;
  protected
    procedure BuscarRegistros; override;
  end;

function PesquisarCFOP(
  pTipo: TTipoCFOP;
  pSomenteAtivos: Boolean;
  pInicioCFOP: string;
  pSomenteEntradaMercadorias: Boolean;
  pSomenteEntradaServicos: Boolean;
  pSomenteEntradaConhecFrete: Boolean
): TObject;

implementation

{$R *.dfm}

const
  cp_nome      = 2;
  cp_ativo     = 3;
  cp_descricao = 4;

function PesquisarCFOP(
  pTipo: TTipoCFOP;
  pSomenteAtivos: Boolean;
  pInicioCFOP: string;
  pSomenteEntradaMercadorias: Boolean;
  pSomenteEntradaServicos: Boolean;
  pSomenteEntradaConhecFrete: Boolean
): TObject;
var
  vForm: TFormPesquisaCFOP;
begin
  Result := nil;

  vForm := TFormPesquisaCFOP.Create(Application);
  vForm.cbOpcoesPesquisa.Filtros := _Cfop.GetFiltros;
  vform.sgPesquisa.OcultarColunas([coSelecionado]);

  vForm.FTipo                      := pTipo;
  vForm.FInicioCFOP                := pInicioCFOP;
  vForm.FSomenteAtivos             := pSomenteAtivos;
  vForm.FSomenteEntradaMercadorias := pSomenteEntradaMercadorias;
  vForm.FSomenteEntradaServicos    := pSomenteEntradaServicos;
  vForm.FSomenteEntradaConhecFrete := pSomenteEntradaConhecFrete;

  if vForm.ShowModal = mrOk then
    Result := RecCFOPs(vForm.FDados[vForm.sgPesquisa.Row - 1]);

  FreeAndNil(vForm);
end;

procedure TFormPesquisaCFOP.BuscarRegistros;
var
  i: Integer;
  vCfops: TArray<RecCFOPs>;
begin
  inherited;
  eDescricaoCFOP.Lines.Clear;

  vCfops :=
    _Cfop.BuscarCFOPs(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text],
      FTipo,
      FSomenteAtivos,
      FInicioCFOP,
      FSomenteEntradaMercadorias,
      FSomenteEntradaServicos,
      FSomenteEntradaConhecFrete
    );

  if vCfops = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vCfops);

  for i := Low(vCfops) to High(vCfops) do begin
    sgPesquisa.Cells[coSelecionado, i + 1] := 'N';
    sgPesquisa.Cells[coCodigo, i + 1]      := vCfops[i].CfopPesquisaId;
    sgPesquisa.Cells[cp_nome, i + 1]       := vCfops[i].nome;
    sgPesquisa.Cells[cp_ativo, i + 1]      := vCfops[i].ativo;
    sgPesquisa.Cells[cp_descricao, i + 1]  := vCfops[i].descricao;
  end;

  sgPesquisa.RowCount := IfThen(Length(vCfops) = 1, 2, High(vCfops) + 2);
  sgPesquisaClick(nil);
  sgPesquisa.SetFocus;
end;

procedure TFormPesquisaCFOP.sgPesquisaClick(Sender: TObject);
begin
  inherited;
  eDescricaoCFOP.Lines.Clear;
  eDescricaoCFOP.Lines.Add(sgPesquisa.Cells[cp_descricao, sgPesquisa.Row]);
end;

procedure TFormPesquisaCFOP.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if _Biblioteca.Em(ACol, [coSelecionado, cp_ativo]) then
    vAlinhamento := taCenter
  else if ACol = coCodigo then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
