inherited FormCentralMensagensInternas: TFormCentralMensagensInternas
  Caption = 'Mensagens Hiva'
  ClientHeight = 477
  ClientWidth = 935
  OnShow = FormShow
  ExplicitWidth = 941
  ExplicitHeight = 506
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Width = 0
    Height = 477
    Visible = False
    ExplicitTop = 73
    ExplicitWidth = 0
    ExplicitHeight = 308
  end
  object tvGrupoMensagens: TTreeView
    Left = 0
    Top = 0
    Width = 193
    Height = 65
    Align = alCustom
    Color = clWhite
    Indent = 19
    ReadOnly = True
    TabOrder = 1
    OnClick = tvGrupoMensagensClick
    Items.NodeData = {
      03020000002C0000000000000000000000FFFFFFFFFFFFFFFF00000000000000
      0000000000010745006E00740072006100640061002E00000000000000000000
      00FFFFFFFFFFFFFFFF000000000000000000000000010845006E007600690061
      00640061007300}
  end
  object pnMensagens: TPanel
    Left = 194
    Top = 0
    Width = 742
    Height = 477
    Align = alCustom
    TabOrder = 2
    object eMensagem: TMemo
      Left = 0
      Top = 137
      Width = 741
      Height = 339
      Align = alCustom
      Color = clCream
      ImeMode = imDisable
      ReadOnly = True
      TabOrder = 1
    end
    object sgMensagens: TGridLuka
      Left = 0
      Top = 1
      Width = 741
      Height = 137
      Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
      Align = alCustom
      Color = cl3DLight
      DefaultRowHeight = 19
      DrawingStyle = gdsGradient
      FixedColor = 15395562
      FixedCols = 0
      RowCount = 2
      Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
      PopupMenu = pmOpcoes
      TabOrder = 0
      OnClick = sgMensagensClick
      OnDrawCell = sgMensagensDrawCell
      IncrementExpandCol = 0
      IncrementExpandRow = 0
      CorLinhaFoco = 38619
      CorFundoFoco = 16774625
      CorLinhaDesfoque = 14869218
      CorFundoDesfoque = 16382457
      CorSuperiorCabecalho = clWhite
      CorInferiorCabecalho = 13553358
      CorSeparadorLinhas = 12040119
      CorColunaFoco = clActiveCaption
      HCol.Strings = (
        'Mensagem'
        'Assunto'
        'Usu'#225'rio envio'
        'Data/hora envio'
        'Respondida?')
      OnGetCellColor = sgMensagensGetCellColor
      Grid3D = False
      RealColCount = 5
      AtivarPopUpSelecao = False
      ColWidths = (
        64
        245
        224
        122
        82)
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 67
    Width = 193
    Height = 410
    TabOrder = 3
    object miEnviarNovaMensagem: TSpeedButton
      Left = 6
      Top = 31
      Width = 182
      Height = 25
      Caption = 'Nova mensagem'
      Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = miEnviarNovaMensagemClick
    end
    object miResponderMensagem: TSpeedButton
      Left = 6
      Top = 79
      Width = 182
      Height = 23
      Caption = 'Responder mensagem'
      Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = miResponderMensagemClick
    end
    object miEncaminharMensagem: TSpeedButton
      Left = 6
      Top = 126
      Width = 182
      Height = 25
      Caption = 'Encaminhar mensagem'
      Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = miEncaminharMensagemClick
    end
  end
  object pmOpcoes: TPopupMenu
    Left = 50
    Top = 425
  end
end
