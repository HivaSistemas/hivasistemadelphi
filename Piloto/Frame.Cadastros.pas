unit Frame.Cadastros;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Buttons, _RecordsCadastros,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Cadastros, _Sessao, System.Math,
  Pesquisa.Cadastros, Vcl.Menus;

type
  TFrameCadastros = class(TFrameHenrancaPesquisas)
  public
    function getCadastro(pLinha: Integer = -1): RecCadastros;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function AdicionarPesquisandoTodos: TArray<TObject>; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrameHenrancaCadastros }

function TFrameCadastros.AdicionarDireto: TObject;
var
  vCadastros: TArray<RecCadastros>;
begin
  vCadastros := _Cadastros.BuscarCadastros(Sessao.getConexaoBanco, 0, [FChaveDigitada]);
  if vCadastros = nil then
    Result := nil
  else
    Result := vCadastros[0];
end;

function TFrameCadastros.AdicionarPesquisando: TObject;
begin
  Result := Pesquisa.Cadastros.Pesquisar(ckSomenteAtivos.Checked);
end;

function TFrameCadastros.AdicionarPesquisandoTodos: TArray<TObject>;
begin
  Result := Pesquisa.Cadastros.PesquisarVarios(ckSomenteAtivos.Checked);
end;

function TFrameCadastros.getCadastro(pLinha: Integer): RecCadastros;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecCadastros(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrameCadastros.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecCadastros(FDados[i]).cadastro_id = RecCadastros(pSender).cadastro_id then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrameCadastros.MontarGrid;
var
  i: Integer;
  vSender: RecCadastros;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecCadastros(FDados[i]);
      AAdd([IntToStr(vSender.cadastro_id), vSender.nome_fantasia]);
    end;
  end;
end;

end.
