unit Impressao.ComprovanteEntregaGrafico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, QuickRpt, QRCtrls, Vcl.ExtCtrls, _HerancaPrincipal, _Entregas,
  _BibliotecaGenerica, _EntregasItens, _RecordsExpedicao, _Sessao, _Biblioteca, _Retiradas,
  _RetiradasItens;

type
  TFormComprovanteEntregaGrafico = class(TFormHerancaPrincipal)
    qrImpNaoFiscal: TQuickRep;
    qrBandTitulo: TQRBand;
    qrEmpresa: TQRLabel;
    qrCabProdutoId: TQRLabel;
    qrCabNomeProduto: TQRLabel;
    qrCabUnidade: TQRLabel;
    qrCabQuantidade: TQRLabel;
    qrCnpjEmpresa: TQRLabel;
    qrEndereco: TQRLabel;
    qrCidadeUf: TQRLabel;
    qrTipoDocumento: TQRLabel;
    qrSeparador2: TQRShape;
    qrBandItens: TQRBand;
    qrProdutoId: TQRLabel;
    qrQuantidade: TQRLabel;
    qrUnidade: TQRLabel;
    qrBandTotalItens: TQRBand;
    qrSeparador8: TQRShape;
    qrTitQuantidadeTotalItens: TQRLabel;
    qrQuantidadeTotalItens: TQRLabel;
    qrCabValorUnitarioItem: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    qrMarca: TQRLabel;
    qrLote: TQRLabel;
    qrVendedor: TQRLabel;
    qrCliente: TQRLabel;
    qrPedido: TQRLabel;
    qrCodigoMovimento: TQRLabel;
    qrNomeSistema: TQRLabel;
    qrNomeProduto: TQRLabel;
    qrEnderecoEntrega: TQRLabel;
    procedure qrImpNaoFiscalNeedData(Sender: TObject; var MoreData: Boolean);
    procedure qrBandItensBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qrImpNaoFiscalBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
  private
    FPosicao: Integer;
    FTipoMovimento: string;
    FItensEnt: TArray<RecEntregaItem>;
    FItensRet: TArray<RecRetiradaItem>;
  public
    { Public declarations }
  end;

procedure Imprimir(pId: Integer; pTipoMovimento: string; pCaminhoImpressora: string);

implementation

{$R *.dfm}

procedure Imprimir(pId: Integer; pTipoMovimento: string; pCaminhoImpressora: string);
type
  RecInformacoesComprovante = record
    Id: Integer;
    OrcamentoId: Integer;
    ClienteId: Integer;
    NomeCliente: string;
    VendedorId: Integer;
    NomeVendedor: string;
    EnderecoEntrega: string;
  end;

var
  vEntregas: TArray<RecEntrega>;
  vRetiradas: TArray<RecRetirada>;
  vForm: TFormComprovanteEntregaGrafico;
  vInformacoes: RecInformacoesComprovante;
begin

  if pTipoMovimento = 'EN' then begin
    vEntregas := _Entregas.BuscarEntregas(Sessao.getConexaoBanco, 1, [pId]);
    if vEntregas = nil then begin
      _Biblioteca.Exclamar('Entrega n�o encontrada! Entrega: ' + _Biblioteca.NFormat(pId));
      Exit;
    end;
    vInformacoes.Id              := pId;
    vInformacoes.OrcamentoId     := vEntregas[0].OrcamentoId;
    vInformacoes.ClienteId       := vEntregas[0].ClienteId;
    vInformacoes.NomeCliente     := vEntregas[0].NomeCliente;
    vInformacoes.VendedorId      := vEntregas[0].VendedorId;
    vInformacoes.NomeVendedor    := vEntregas[0].NomeVendedor;

    vInformacoes.EnderecoEntrega :=
      'End: ' + vEntregas[0].Logradouro + ' ' + vEntregas[0].Complemento + ' nr' + vEntregas[0].Numero + ' ' +
      vEntregas[0].NomeBairro + ', ' + vEntregas[0].Cidade + '-' + vEntregas[0].UF;
  end
  else begin
    vRetiradas := _Retiradas.BuscarRetiradas(Sessao.getConexaoBanco, 0, [pId]);
    if vRetiradas = nil then begin
      _Biblioteca.Exclamar('Retirada n�o encontrada! Retirada: ' + _Biblioteca.NFormat(pId));
      Exit;
    end;

    vInformacoes.Id              := pId;
    vInformacoes.OrcamentoId     := vRetiradas[0].OrcamentoId;
    vInformacoes.ClienteId       := vRetiradas[0].ClienteId;
    vInformacoes.NomeCliente     := vRetiradas[0].NomeCliente;
    vInformacoes.VendedorId      := vRetiradas[0].VendedorId;
    vInformacoes.NomeVendedor    := vRetiradas[0].NomeVendedor;

    vInformacoes.EnderecoEntrega := '';
  end;

  vForm := TFormComprovanteEntregaGrafico.Create(nil);

  vForm.qrNomeSistema.Caption := _Sessao.Sessao.getVersaoSistema;

  if pTipoMovimento = 'EN' then
    vForm.FItensEnt := _EntregasItens.BuscarEntregaItens(Sessao.getConexaoBanco, 0, [pId])
  else
    vForm.FItensRet := _RetiradasItens.BuscarRetiradaItens(Sessao.getConexaoBanco, 0, [pId]);

  vForm.FTipoMovimento        := pTipoMovimento;
  vForm.qrCnpjEmpresa.Caption := Sessao.getEmpresaLogada.Cnpj + ' - ' + Sessao.getEmpresaLogada.RazaoSocial;
  vForm.qrEndereco.Caption    := Sessao.getEmpresaLogada.Logradouro + ' ' + Sessao.getEmpresaLogada.Complemento;
  vForm.qrCidadeUf.Caption    := Sessao.getEmpresaLogada.NomeCidade + '/' + Sessao.getEmpresaLogada.EstadoId;

  vForm.qrQuantidadeTotalItens.Caption := _Biblioteca.NFormat(Length(vForm.FItensEnt));
  vForm.qrCodigoMovimento.Caption := IIfStr(pTipoMovimento = 'EN', 'Entrega.: ', 'Retirada.:') + _Biblioteca.NFormat(vInformacoes.Id);
  vForm.qrPedido.Caption          := 'Pedido..: ' + _Biblioteca.NFormat(vInformacoes.OrcamentoId);
  vForm.qrCliente.Caption         := 'Cliente.: ' + _Biblioteca.NFormat(vInformacoes.ClienteId) + ' - ' + vInformacoes.NomeCliente;
  vForm.qrVendedor.Caption        := 'Vendedor: ' + _Biblioteca.NFormat(vInformacoes.VendedorId) + ' - ' + vInformacoes.NomeVendedor;
  vForm.qrEnderecoEntrega.Caption := vInformacoes.EnderecoEntrega;

  try
    vForm.qrImpNaoFiscal.PrinterSettings.PrinterIndex := _BibliotecaGenerica.GetIndexImpressora( pCaminhoImpressora );
    vForm.qrImpNaoFiscal.Print;
  finally
    vForm.Free;
  end;
end;

procedure TFormComprovanteEntregaGrafico.qrBandItensBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  if FTipoMovimento = 'EN' then begin
    qrProdutoId.Caption   := _Biblioteca.NFormat(FItensEnt[FPosicao].ProdutoId);
    qrNomeProduto.Caption := FItensEnt[FPosicao].NomeProduto;
    qrMarca.Caption       := FItensEnt[FPosicao].NomeMarca;
    qrLote.Caption        := FItensEnt[FPosicao].Lote;
    qrQuantidade.Caption  := _Biblioteca.NFormatEstoque(FItensEnt[FPosicao].Quantidade);
    qrUnidade.Caption     := FItensEnt[FPosicao].Unidade;
  end
  else begin
    qrProdutoId.Caption   := _Biblioteca.NFormat(FItensRet[FPosicao].ProdutoId);
    qrNomeProduto.Caption := FItensRet[FPosicao].NomeProduto;
    qrMarca.Caption       := FItensRet[FPosicao].NomeMarca;
    qrLote.Caption        := FItensRet[FPosicao].Lote;
    qrQuantidade.Caption  := _Biblioteca.NFormatEstoque(FItensRet[FPosicao].Quantidade);
    qrUnidade.Caption     := FItensRet[FPosicao].Unidade;
  end;
end;

procedure TFormComprovanteEntregaGrafico.qrImpNaoFiscalBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicao := -1;
end;

procedure TFormComprovanteEntregaGrafico.qrImpNaoFiscalNeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicao);
  if FTipoMovimento = 'EN' then
    MoreData := FPosicao < Length(FItensEnt)
  else
    MoreData := FPosicao < Length(FItensRet);
end;

end.
