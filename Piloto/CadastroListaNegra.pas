unit CadastroListaNegra;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _ListaNegra,
  CheckBoxLuka, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _Sessao, _Biblioteca, _RecordsEspeciais,
  PesquisaListaNegra;

type
  TFormCadastroListaNegra = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eNome: TEditLuka;
  private
    procedure PreencherRegistro(pDados: RecListaNegra);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroListaNegra }

procedure TFormCadastroListaNegra.BuscarRegistro;
var
  vDados: TArray<RecListaNegra>;
begin
  vDados := _ListaNegra.BuscarListaNegra(Sessao.getConexaoBanco, 0, [eId.AsInt], False);
  if vDados = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    SetarFoco(eID);
    Exit;
  end;

  inherited;
  PreencherRegistro(vDados[0]);
end;

procedure TFormCadastroListaNegra.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _ListaNegra.ExcluirListaNegra(Sessao.getConexaoBanco, eId.AsInt);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroListaNegra.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vRetBanco :=
    _ListaNegra.AtualizarListaNegra(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eNome.Text,
      _Biblioteca.ToChar(ckAtivo)
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormCadastroListaNegra.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    eNome,
    ckAtivo],
    pEditando
  );

  if pEditando then
    SetarFoco(eNome);
end;

procedure TFormCadastroListaNegra.PesquisarRegistro;
var
  vDado: RecListaNegra;
begin
  vDado := RecListaNegra(PesquisaListaNegra.Pesquisar(False));
  if vDado = nil then
    Exit;

  inherited;
  PreencherRegistro(vDado);
end;

procedure TFormCadastroListaNegra.PreencherRegistro(pDados: RecListaNegra);
begin
  eID.AsInt          := pDados.ListaId;
  eNome.Text         := pDados.Nome;
  ckAtivo.CheckedStr := pDados.Ativo;

  pDados.Free;
end;

procedure TFormCadastroListaNegra.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eNome.Trim = '' then begin
    _Biblioteca.Exclamar('O nome da lista negra n�o foi informada corretamente, verifique!');
    SetarFoco(eNome);
    Abort;
  end;
end;

end.
