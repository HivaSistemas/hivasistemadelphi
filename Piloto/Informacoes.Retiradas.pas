unit Informacoes.Retiradas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Retiradas, _RetiradasItens,
  Vcl.ExtCtrls, Vcl.StdCtrls, EditLuka, Vcl.Mask, EditLukaData, Vcl.Grids, _Sessao, _Biblioteca,
  GridLuka, _BibliotecaGenerica, _RecordsExpedicao, StaticTextLuka,
  SpeedButtonLuka;

type
  TFormInformacoesRetirada = class(TFormHerancaFinalizar)
    lb1: TLabel;
    eRetiradaId: TEditLuka;
    eCliente: TEditLuka;
    lb2: TLabel;
    eEmpresa: TEditLuka;
    lb13: TLabel;
    stTipo: TStaticText;
    lb17: TLabel;
    eOrcamentoId: TEditLuka;
    eNotaFiscalId: TEditLuka;
    Label1: TLabel;
    lb4: TLabel;
    eDataHoraRetirada: TEditLukaData;
    eNomePessoaRetirou: TEditLuka;
    lb16: TLabel;
    sgItens: TGridLuka;
    StaticTextLuka1: TStaticTextLuka;
    Label2: TLabel;
    eLocal: TEditLuka;
    sbInformacoesOrcamento: TSpeedButtonLuka;
    sbNotaFiscalId: TSpeedButtonLuka;
    procedure sbInformacoesOrcamentoClick(Sender: TObject);
    procedure sbNotaFiscalIdClick(Sender: TObject);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure Informar(pRetiradaId: Integer);

implementation

{$R *.dfm}

uses
  Informacoes.Orcamento, Informacoes.NotaFiscal;

const
  ciProdutoId  = 0;
  ciNome       = 1;
  ciMarca      = 2;
  ciLote       = 3;
  ciUnidade    = 4;
  ciQuantidade = 5;
  ciDevolvidos = 6;

procedure Informar(pRetiradaId: Integer);
var
  i: Integer;
  vRetirada: TArray<RecRetirada>;
  vItens: TArray<RecRetiradaItem>;
  vForm: TFormInformacoesRetirada;
begin
  if pRetiradaId = 0 then
    Exit;

  vRetirada := _Retiradas.BuscarRetiradas(Sessao.getConexaoBanco, 0, [pRetiradaId]);
  if vRetirada = nil then begin
    _Biblioteca.Exclamar('Retirada n�o encontrada!');
    Exit;
  end;

  vItens := _RetiradasItens.BuscarRetiradaItens(Sessao.getConexaoBanco, 0, [pRetiradaId]);
  if vItens = nil then begin
    _Biblioteca.Exclamar('Itens da retirada n�o encontrados!');
    Exit;
  end;

  vForm := TFormInformacoesRetirada.Create(nil);

  vForm.eRetiradaId.AsInt := vRetirada[0].RetiradaId;
  vForm.eCliente.SetInformacao(vRetirada[0].ClienteId, vRetirada[0].NomeCliente);
  vForm.eEmpresa.SetInformacao(vRetirada[0].EmpresaId, vRetirada[0].NomeEmpresa);
  vForm.eLocal.SetInformacao(vRetirada[0].LocalId, vRetirada[0].NomeLocal);
  vForm.stTipo.Caption := vRetirada[0].TipoMovimentoAnalitico;
  vForm.eOrcamentoId.SetInformacao(vRetirada[0].OrcamentoId);
  vForm.eNotaFiscalId.SetInformacao(vRetirada[0].NotaFiscalId);
  vForm.eDataHoraRetirada.AsDataHora := vRetirada[0].DataHoraRetirada;
  vForm.eNomePessoaRetirou.Text := vRetirada[0].NomePessoaRetirada;

  for i := Low(vItens) to High(vItens) do begin
    vForm.sgItens.Cells[ciProdutoId, i + 1]  := _Biblioteca.NFormat(vItens[i].ProdutoId);
    vForm.sgItens.Cells[ciNome, i + 1]       := vItens[i].NomeProduto;
    vForm.sgItens.Cells[ciMarca, i + 1]      := vItens[i].NomeMarca;
    vForm.sgItens.Cells[ciLote , i + 1]      := vItens[i].Lote;
    vForm.sgItens.Cells[ciUnidade, i + 1]    := vItens[i].Unidade;
    vForm.sgItens.Cells[ciQuantidade, i + 1] := _Biblioteca.NFormatEstoque(vItens[i].Quantidade);
    vForm.sgItens.Cells[ciDevolvidos, i + 1] := _Biblioteca.NFormatNEstoque(vItens[i].Devolvidos);
  end;
  vForm.sgItens.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor(Length(vItens));

  vForm.ShowModal;

  vForm.Free;
end;

procedure TFormInformacoesRetirada.sbInformacoesOrcamentoClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar(eOrcamentoId.AsInt);
end;

procedure TFormInformacoesRetirada.sbNotaFiscalIdClick(Sender: TObject);
begin
  inherited;
  Informacoes.NotaFiscal.Informar(eNotaFiscalId.AsInt);
end;

procedure TFormInformacoesRetirada.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[ciProdutoId, ciQuantidade, ciDevolvidos] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
