unit EditarInformacoesMemo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.StdCtrls, _Biblioteca, _RecordsEspeciais, _Sessao, _ContasReceber;

type
  TFuncionalidade = (trContasReceber, trContasPagar);

  TFormEditarInformacoesMemo = class(TFormHerancaFinalizar)
    meObservacao: TMemo;
  private
    fId: Integer;
    fFuncionalidade: TFuncionalidade;
  protected
    procedure Finalizar(Sender: TObject); override;
  public
    { Public declarations }
  end;

function AlterarDados(
  pId: Integer;
  pTexto: string;
  pFuncionalidade: TFuncionalidade;
  pMaxLength: Integer;
  pTitulo: string
): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

{ TFormHerancaFinalizar1 }

function AlterarDados(
  pId: Integer;
  pTexto: string;
  pFuncionalidade: TFuncionalidade;
  pMaxLength: Integer;
  pTitulo: string
): TRetornoTelaFinalizar;
var
  vForm: TFormEditarInformacoesMemo;
begin
  Result.RetTela := trCancelado;

  if pId = 0 then
    Exit;

  vForm := TFormEditarInformacoesMemo.Create(Application);

  vForm.Caption := pTitulo;
  vForm.meObservacao.MaxLength := pMaxLength;
  vForm.meObservacao.Text := pTexto;

  vForm.fId := pId;
  vForm.fFuncionalidade := pFuncionalidade;

  Result.Ok(vForm.ShowModal, True);
end;

procedure TFormEditarInformacoesMemo.Finalizar(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  case fFuncionalidade of
    trContasReceber:
      begin
        vRetBanco :=
          _ContasReceber.AtualizarObservacoes(
            Sessao.getConexaoBanco,
            fId,
            meObservacao.Text
          );

        Sessao.AbortarSeHouveErro(vRetBanco);

        _Biblioteca.RotinaSucesso;
      end;
  end;
end;

end.
