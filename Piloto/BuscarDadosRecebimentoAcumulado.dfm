inherited FormBuscarDadosRecebimentoAcumulado: TFormBuscarDadosRecebimentoAcumulado
  Caption = 'Busca dados do recebimento do acumulado'
  ClientHeight = 341
  ClientWidth = 484
  ExplicitWidth = 490
  ExplicitHeight = 370
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 304
    Width = 484
    ExplicitTop = 304
    ExplicitWidth = 484
  end
  inline FrPagamento: TFrFechamentoJuros
    Left = 4
    Top = 3
    Width = 290
    Height = 299
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 4
    ExplicitTop = 3
    ExplicitHeight = 299
    inherited gbFechamento: TGroupBox
      Height = 299
      ExplicitHeight = 299
      inherited lb20: TLabel
        Width = 9
        Height = 14
        ExplicitWidth = 9
        ExplicitHeight = 14
      end
      inherited lb7: TLabel
        Width = 9
        Height = 14
        ExplicitWidth = 9
        ExplicitHeight = 14
      end
      inherited sbBuscarDadosCheques: TSpeedButton
        Caption = '?'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      inherited sbBuscarDadosCartoesDebito: TSpeedButton
        Caption = '?'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      inherited sbBuscarDadosCobranca: TSpeedButton
        Caption = '?'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      inherited sbBuscarCreditos: TSpeedButton
        Caption = '?'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      inherited sbBuscarDadosCartoesCredito: TSpeedButton
        Caption = '?'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      inherited lb6: TLabel
        Width = 9
        Height = 14
        ExplicitWidth = 9
        ExplicitHeight = 14
      end
      inherited Label1: TLabel
        Left = 46
        Top = 239
        ExplicitLeft = 46
        ExplicitTop = 239
      end
      inherited sbBuscarDadosCobrancaPix: TSpeedButton
        Top = 236
        Caption = '?'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Calibri'
        ParentFont = False
        ExplicitTop = 236
      end
      inherited ePercentualOutrasDespesas: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorDesconto: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorOutrasDespesas: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited ePercentualDesconto: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorDinheiro: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorCheque: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorCartaoDebito: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorCobranca: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorCredito: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited stSPC: TStaticText
        Caption = 'Pagamento'
        Color = 38619
      end
      inherited eValorAcumulativo: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorFinanceira: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorFrete: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorCartaoCredito: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorPix: TEditLuka
        Top = 233
        Height = 22
        ExplicitTop = 233
        ExplicitHeight = 22
      end
      inherited eValorJuros: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited ePercentualJuros: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
    end
  end
  inline FrTipoNotaGerar: TFrTipoNotaGerar
    Left = 298
    Top = 3
    Width = 159
    Height = 150
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 298
    ExplicitTop = 3
    ExplicitWidth = 159
    ExplicitHeight = 150
    inherited grpDocumentosEmitir: TGroupBox
      Left = 2
      Top = 3
      ExplicitLeft = 2
      ExplicitTop = 3
      inherited ckRecibo: TCheckBoxLuka
        Caption = 'Recibo de pagamento'
        Checked = True
        State = cbChecked
        CheckedStr = 'S'
      end
      inherited ckEmitirNFCe: TCheckBoxLuka
        Top = 109
        Visible = False
        ExplicitTop = 109
      end
      inherited ckEmitirNFe: TCheckBoxLuka
        Top = 72
        ExplicitTop = 72
      end
    end
  end
  object ckGerarCreditoTroco: TCheckBox
    Left = 116
    Top = 281
    Width = 140
    Height = 17
    Caption = 'Gerar cr'#233'dito do troco'
    TabOrder = 3
    Visible = False
  end
end
