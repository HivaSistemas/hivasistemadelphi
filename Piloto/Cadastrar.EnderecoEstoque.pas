unit Cadastrar.EnderecoEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _Sessao, _Biblioteca, _RecordsEspeciais, _RecordsCadastros,
  Pesquisa.MotivosAjusteEstoque, _HerancaCadastro, _FrameHerancaPrincipal,
  _FrameHenrancaPesquisas, FramePlanosFinanceiros, CheckBoxLuka, _EnderecoEstoque,
  Frame.EnderecoEstoqueVao, Frame.EnderecoEstoqueNivel,
  Frame.EnderecoEstoqueModulo, Frame.EnderecoEstoqueRua, Pesquisa.EnderecoEstoque;

type
  TFormEnderecoEstoque = class(TFormHerancaCadastroCodigo)
    FrRua: TFrEnderecoEstoqueRua;
    FrModulo: TFrEnderecoEstoqueModulo;
    FrNivel: TFrEnderecoEstoqueNivel;
    FrVao: TFrEnderecoEstoqueVao;
  private
    procedure PreencherRegistro(pEndereco: RecEnderecosEstoque);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

//uses Pesquisa.EnderecosEstoque;

{ TFormEnderecoEstoque }

procedure TFormEnderecoEstoque.BuscarRegistro;
var
  vEnderecosEstoque: TArray<RecEnderecosEstoque>;
begin
  vEnderecosEstoque := _EnderecoEstoque.BuscarEnderecoEstoque(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if vEnderecosEstoque = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    eID.SetFocus;
    Exit;
  end;

  inherited;
  PreencherRegistro(vEnderecosEstoque[0]);
end;

procedure TFormEnderecoEstoque.ExcluirRegistro;
var
  vRetorno: RecRetornoBD;
begin
  vRetorno := _EnderecoEstoque.ExcluirEnderecoEstoque(Sessao.getConexaoBanco, eId.AsInt);
  if vRetorno.TeveErro then begin
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormEnderecoEstoque.GravarRegistro(Sender: TObject);
var
  vRetorno: RecRetornoBD;
begin
  inherited;
  vRetorno :=
    _EnderecoEstoque.AtualizarEnderecoEstoque(
      Sessao.getConexaoBanco,
      eID.AsInt,
      FrRua.GetEnderecoEstoqueRua(0).rua_id,
      FrModulo.GetEnderecoEstoqueModulo(0).Modulo_id,
      FrNivel.GetEnderecoEstoqueNivel(0).Nivel_id,
      FrVao.GetEnderecoEstoqueVao(0).Vao_id
    );

  if vRetorno.TeveErro then begin
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetorno.AsInt);
end;

procedure TFormEnderecoEstoque.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([ckAtivo, frRua, frModulo, frNivel, frVao], pEditando);

  if pEditando then begin
    SetarFoco(frRua);
    ckAtivo.Checked := True;
  end;
end;

procedure TFormEnderecoEstoque.PesquisarRegistro;
var
  vEnderecosEstoque: RecEnderecosEstoque;
begin
  vEnderecosEstoque := RecEnderecosEstoque(Pesquisa.EnderecoEstoque.Pesquisar());  //AquiS
  if vEnderecosEstoque = nil then
    Exit;

  inherited;
  PreencherRegistro(vEnderecosEstoque);
end;

procedure TFormEnderecoEstoque.PreencherRegistro(pEndereco: RecEnderecosEstoque);
begin
  eID.AsInt := pEndereco.endereco_id;
  FrRua.InserirDadoPorChave(pEndereco.rua_id);
  FrModulo.InserirDadoPorChave(pEndereco.modulo_id);
  FrNivel.InserirDadoPorChave(pEndereco.nivel_id);
  FrVao.InserirDadoPorChave(pEndereco.vao_id);
  pEndereco.Free;
end;

procedure TFormEnderecoEstoque.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if frRua.EstaVazio then begin
    _Biblioteca.Exclamar('A Rua n�o foi informada!');
    SetarFoco(frRua);
    Abort;
  end;

  if frModulo.EstaVazio then begin
    _Biblioteca.Exclamar('O M�dulo n�o foi informada!');
    SetarFoco(frModulo);
    Abort;
  end;

  if frNivel.EstaVazio then begin
    _Biblioteca.Exclamar('O N�vel n�o foi informada!');
    SetarFoco(frNivel);
    Abort;
  end;

  if frVao.EstaVazio then begin
    _Biblioteca.Exclamar('O V�o n�o foi informada!');
    SetarFoco(frVao);
    Abort;
  end;
end;

end.
