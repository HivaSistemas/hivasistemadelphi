inherited FormBloqueioEstoque: TFormBloqueioEstoque
  Caption = 'Bloqueio de estoque'
  ClientHeight = 405
  ClientWidth = 930
  ExplicitWidth = 936
  ExplicitHeight = 434
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 470
    Top = 5
    Width = 63
    Height = 14
    Caption = 'Observa'#231#227'o'
  end
  object lb7: TLabel [1]
    Left = 419
    Top = 93
    Width = 64
    Height = 14
    Caption = 'Quantidade'
  end
  inherited pnOpcoes: TPanel
    Height = 405
    TabOrder = 7
    ExplicitHeight = 405
    inherited sbDesfazer: TSpeedButton
      Top = 50
      ExplicitTop = 50
    end
    inherited sbExcluir: TSpeedButton
      Top = 106
      ExplicitTop = 106
    end
    inherited sbPesquisar: TSpeedButton
      Top = 214
      Visible = False
      ExplicitTop = 214
    end
    inherited sbLogs: TSpeedButton
      Left = 0
      Top = 362
      Width = 117
      ExplicitLeft = 0
      ExplicitTop = 362
      ExplicitWidth = 117
    end
  end
  inherited eID: TEditLuka
    TabOrder = 13
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 920
    Top = 0
    TabOrder = 14
    Visible = False
    ExplicitLeft = 920
    ExplicitTop = 0
  end
  inline FrLocal: TFrLocais
    Left = 427
    Top = 45
    Width = 339
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 4
    TabStop = True
    ExplicitLeft = 427
    ExplicitTop = 45
    ExplicitWidth = 339
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 314
      Height = 23
      ExplicitWidth = 314
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 339
      ExplicitWidth = 339
      inherited lbNomePesquisa: TLabel
        Width = 28
        Caption = 'Local'
        ExplicitWidth = 28
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 234
        ExplicitLeft = 234
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 314
      Height = 24
      ExplicitLeft = 314
      ExplicitHeight = 24
    end
  end
  object eObservacao: TEditLuka
    Left = 467
    Top = 19
    Width = 223
    Height = 22
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    MaxLength = 200
    ParentFont = False
    TabOrder = 1
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  inline FrProduto: TFrProdutos
    Left = 126
    Top = 46
    Width = 297
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 46
    ExplicitWidth = 297
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 272
      Height = 23
      TabOrder = 1
      ExplicitWidth = 272
      ExplicitHeight = 23
    end
    inherited CkAspas: TCheckBox
      TabOrder = 3
    end
    inherited CkFiltroDuplo: TCheckBox
      TabOrder = 5
    end
    inherited CkPesquisaNumerica: TCheckBox
      TabOrder = 7
    end
    inherited PnTitulos: TPanel
      Width = 297
      TabOrder = 0
      ExplicitWidth = 297
      inherited lbNomePesquisa: TLabel
        Width = 42
        Caption = 'Produto'
        ExplicitWidth = 42
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 192
        ExplicitLeft = 192
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 272
      Height = 24
      TabOrder = 2
      ExplicitLeft = 272
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      TabOrder = 6
    end
    inherited ckFiltroExtra: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  object eQuantidade: TEditLuka
    Left = 419
    Top = 107
    Width = 81
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    Text = '0,0000'
    OnKeyDown = eQuantidadeKeyDown
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 4
    PadraoEstoque = True
    NaoAceitarEspaco = False
  end
  inline FrLote: TFrameLotes
    Left = 125
    Top = 93
    Width = 287
    Height = 41
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 5
    TabStop = True
    ExplicitLeft = 125
    ExplicitTop = 93
    inherited lbLote: TLabel
      Width = 23
      Height = 14
      ExplicitWidth = 23
      ExplicitHeight = 14
    end
    inherited lbDataFabricacao: TLabel
      Width = 75
      Height = 14
      ExplicitWidth = 75
      ExplicitHeight = 14
    end
    inherited lbDataVencimento: TLabel
      Width = 56
      Height = 14
      ExplicitWidth = 56
      ExplicitHeight = 14
    end
    inherited lbBitola: TLabel
      Width = 33
      Height = 14
      ExplicitWidth = 33
      ExplicitHeight = 14
    end
    inherited lbTonalidade: TLabel
      Width = 62
      Height = 14
      ExplicitWidth = 62
      ExplicitHeight = 14
    end
    inherited eLote: TEditLuka
      Height = 22
      ExplicitHeight = 22
    end
    inherited eDataFabricacao: TEditLukaData
      Height = 22
      ExplicitHeight = 22
    end
    inherited eDataVencimento: TEditLukaData
      Height = 22
      ExplicitHeight = 22
    end
    inherited eBitola: TEditLuka
      Height = 22
      ExplicitHeight = 22
    end
    inherited eTonalidade: TEditLuka
      Height = 22
      ExplicitHeight = 22
    end
  end
  object sgItens: TGridLuka
    Left = 125
    Top = 135
    Width = 841
    Height = 270
    Align = alCustom
    ColCount = 8
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 8
    OnDrawCell = sgItensDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Produto'
      'Nome'
      'Marca'
      'Und.'
      'Lote'
      'Quantidade'
      'Local'
      'Nome')
    Grid3D = False
    RealColCount = 10
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      49
      209
      95
      32
      73
      78
      40
      142)
  end
  object st1: TStaticTextLuka
    Left = 625
    Top = 96
    Width = 105
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Est. fisico'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 9
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object stEstoqueFisico: TStaticText
    Left = 625
    Top = 112
    Width = 105
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '5,0000'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 10
    Transparent = False
  end
  object stEstoqueDisponivel: TStaticText
    Left = 510
    Top = 112
    Width = 105
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '10,0000'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 11
    Transparent = False
  end
  object st2: TStaticTextLuka
    Left = 510
    Top = 96
    Width = 105
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Est. dispon'#237'vel'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 12
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  inline FrEmpresa: TFrEmpresas
    Left = 210
    Top = 1
    Width = 255
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 0
    TabStop = True
    ExplicitLeft = 210
    ExplicitTop = 1
    ExplicitWidth = 255
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 230
      Height = 24
      ExplicitWidth = 230
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 255
      ExplicitWidth = 255
      inherited lbNomePesquisa: TLabel
        Width = 47
        Caption = 'Empresa'
        ExplicitWidth = 47
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 150
        ExplicitLeft = 150
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 230
      Height = 25
      ExplicitLeft = 230
      ExplicitHeight = 25
    end
  end
  inline FrTipoBloqueioEstoque: TFrTipoBloqueioEstoque
    Left = 695
    Top = 1
    Width = 228
    Height = 43
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 695
    ExplicitTop = 1
    ExplicitWidth = 228
    ExplicitHeight = 43
    inherited sgPesquisa: TGridLuka
      Width = 203
      Height = 26
      ExplicitWidth = 203
      ExplicitHeight = 26
    end
    inherited PnTitulos: TPanel
      Width = 228
      ExplicitWidth = 228
      inherited lbNomePesquisa: TLabel
        Width = 94
        ExplicitWidth = 94
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 123
        ExplicitLeft = 123
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 203
      Height = 27
      ExplicitLeft = 203
      ExplicitHeight = 27
    end
  end
end
