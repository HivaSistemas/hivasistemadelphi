unit RelacaoContasPagar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, Vcl.ComCtrls, Vcl.Buttons,
  Vcl.ExtCtrls, FrameFornecedores, FrameTiposCobranca, Vcl.StdCtrls, GroupBoxLuka, _ContasPagar,
  FrameDataInicialFinal, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameEmpresas, VCLTee.TeEngine,
  VCLTee.Series, VCLTee.TeeProcs, VCLTee.Chart, Vcl.Grids, GridLuka, _Sessao, _Imagens, _Biblioteca,
  Vcl.Menus, System.StrUtils, System.Math, _RecordsRelatorios, InclusaoTitulosPagar, _RelacaoContasPagar,
  Informacoes.TituloPagar, BaixarTitulosPagar, VclTee.TeeGDIPlus, FrameCentroCustos, FramePortadores,
  Frame.Inteiros, _RecordsEspeciais, Frame.Cadastros, BuscarDadosData, ImpressaoComprovanteCreditoPagarGrafico,
  ComboBoxLuka, BloquearDesbloquearTitulos, FramePlanosFinanceiros, FrameNumeros, BuscaDados,
  CheckBoxLuka, AdiantamentoContasPagar, BuscarPlanoFinanceiro, Data.DB,
  Datasnap.DBClient, frxClass, frxDBSet, FrameTextos, BuscarDadosFornecedor, _RecordsCadastros;

type
  TFormRelacaoContasPagar = class(TFormHerancaRelatoriosPageControl)
    FrEmpresas: TFrEmpresas;
    FrDataCadastro: TFrDataInicialFinal;
    FrDataVencimento: TFrDataInicialFinal;
    gbStatusFinanceiro: TGroupBoxLuka;
    ckTitulosEmAberto: TCheckBox;
    ckTitulosBaixados: TCheckBox;
    FrDataBaixa: TFrDataInicialFinal;
    FrDataPagamento: TFrDataInicialFinal;
    pcResultado: TPageControl;
    tsAnalitico: TTabSheet;
    sgTitulos: TGridLuka;
    tsSinteticoTipoCobranca: TTabSheet;
    sgTitulosTipoCobranca: TGridLuka;
    pnGraficoTiposCobranca: TPanel;
    chTiposCobranca: TChart;
    rgTipoRepresentacaoGrafico: TRadioGroup;
    pmRotinas: TPopupMenu;
    pnAnalitico: TPanel;
    st3: TStaticText;
    st5: TStaticText;
    stValorCobranca: TStaticText;
    st1: TStaticText;
    stValorCartao: TStaticText;
    st4: TStaticText;
    stValorCheque: TStaticText;
    st6: TStaticText;
    stValorTotal: TStaticText;
    st7: TStaticText;
    stQuantidadeTitulos: TStaticText;
    FrPortadores: TFrPortadores;
    FrCentrosCustos: TFrCentroCustos;
    FrTiposCobranca: TFrTiposCobranca;
    psrsSeries1: TPieSeries;
    FrFornecedores: TFrameCadastros;
    lbllb1: TLabel;
    cbBloqueados: TComboBoxLuka;
    FrPlanosFinanceiros: TFrPlanosFinanceiros;
    FrDataEmissao: TFrDataInicialFinal;
    FrDataContabil: TFrDataInicialFinal;
    frPedidoCompra: TFrNumeros;
    frContaPagar: TFrNumeros;
    frCodigoEntrada: TFrNumeros;
    frCodigoBaixa: TFrNumeros;
    miBaixarTitulo: TSpeedButton;
    miBaixarTitulosSelecionados: TSpeedButton;
    Panel1: TPanel;
    miMarcarDesmarcarTItuloFocado: TSpeedButton;
    miAdiantarTituloFocado: TSpeedButton;
    miMarcarDesmarcarTodosTitulos: TSpeedButton;
    miAlterarDataVencimento: TSpeedButton;
    miAlterarCampoDocumento: TSpeedButton;
    miExcluirTitulo: TSpeedButton;
    miAlterarValorDocumento: TSpeedButton;
    miAlterarValorDesconto: TSpeedButton;
    miReimprimirComprovanteCredito: TSpeedButton;
    miIncluirNovoTitulo: TSpeedButton;
    miAlterarplanofinanceiroFocado1: TSpeedButton;
    miBloquearDesbloquearTitulosSelecionados: TSpeedButton;
    frxReport: TfrxReport;
    dstPagar: TfrxDBDataset;
    SpeedButton2: TSpeedButton;
    FrNumeroDocumento: TFrTextos;
    cdsPagar: TClientDataSet;
    cdsPagarCodigo: TStringField;
    cdsPagarNomeFornecedor: TStringField;
    cdsPagarsituacao: TStringField;
    cdsPagarDocumento: TStringField;
    cdsPagarParcela: TStringField;
    cdsPagarDtVencto: TDateField;
    cdsPagarValor: TFloatField;
    cdsPagarAdiantamento: TFloatField;
    cdsPagarDesconto: TFloatField;
    cdsPagarSaldo: TFloatField;
    cdsPagarDiasAtraso: TIntegerField;
    gbOrigem: TGroupBoxLuka;
    ckNfe: TCheckBoxLuka;
    ckNFCe: TCheckBoxLuka;
    CheckBoxLuka1: TCheckBoxLuka;
    CheckBoxLuka2: TCheckBoxLuka;
    CheckBoxLuka3: TCheckBoxLuka;
    CheckBoxLuka4: TCheckBoxLuka;
    CheckBoxLuka5: TCheckBoxLuka;
    CheckBoxLuka6: TCheckBoxLuka;
    CheckBoxLuka7: TCheckBoxLuka;
    CheckBoxLuka8: TCheckBoxLuka;
    CheckBoxLuka9: TCheckBoxLuka;
    CheckBoxLuka10: TCheckBoxLuka;
    cbAgrupamento: TComboBoxLuka;
    Label2: TLabel;
    SpeedButton1: TSpeedButton;
    miAlterarDataContabil: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure sgTitulosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgTitulosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgTitulosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure sgTitulosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure miIncluirNovoTituloClick(Sender: TObject);
    procedure rgTipoRepresentacaoGraficoClick(Sender: TObject);
    procedure sgTitulosTipoCobrancaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgTitulosDblClick(Sender: TObject);
    procedure miBaixarTituloClick(Sender: TObject);
    procedure pmRotinasPopup(Sender: TObject);
    procedure miBaixarTitulosSelecionadosClick(Sender: TObject);
    procedure miMarcarDesmarcarTItuloFocadoClick(Sender: TObject);
    procedure miMarcarDesmarcarTodosTitulosClick(Sender: TObject);
    procedure miExcluirTituloClick(Sender: TObject);
    procedure miAlterarDataVencimentoClick(Sender: TObject);
    procedure miReimprimirComprovanteCreditoClick(Sender: TObject);
    procedure miBloquearDesbloquearTitulosSelecionadosClick(Sender: TObject);
    procedure miAlterarValorDescontoClick(Sender: TObject);
    procedure miAlterarCampoDocumentoClick(Sender: TObject);
    procedure miAlterarValorDocumentoClick(Sender: TObject);
    procedure miAdiantarTituloFocadoClick(Sender: TObject);
    procedure miAlterarplanofinanceiroFocado1Click(Sender: TObject);
    procedure sbGerarPlanilhaClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure miAlterarDataContabilClick(Sender: TObject);
    //    procedure miEnviarParaRecebimentoCaixaClick(Sender: TObject);
  private
    FTitulos: TArray<RecContasPagar>;
    FDadosCobrancas: TArray<RecDadosCobrancas>;
  public
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}


const
(* Grid t�tulos *)
  tiSelecionado     = 0;  // Sel.?
  tiPagarId         = 1;  // C�digo
  tiFornecedor      = 2;  // Fornecedor
  tiSituacao        = 3;  // Situacao
  tiDocumento       = 4;  // Documento
  tiParcela         = 5;  // Parcela
  tiDataContabil    = 6;
  tiDataVencimento  = 7;  // Data Vencto.
  tiValorDocumento  = 8;  // Valor
  tiValorAdiantado  = 9;  // Adiantado
  tiValorDesconto   = 10;  // Desconto
  tiValorSaldo      = 11;  // Saldo
  tiTipoCobranca    = 12; // Tipo de cobran�a
  tiDataCadastro    = 13; // Data cad.
  tiDiasAtraso      = 14; //Dias atraso
  tiEmpresa         = 15; //Empresa
  tiPlanoFinanceiro = 16;
  tiCentroDeCustos  = 17;

  // Campos ocultos
  tiFornecedorId    = 18;
  tiStatusSintetico = 19;
  tiFormaPagamento  = 20;
  tiStatusAnalitico = 21; //Status
  tiBloqueado       = 22; // Bloq.?
  tiPlanoFinanceiroId    = 23;
  tiBaixaReceberOrigemId = 24;
  tiCobrancaId           = 25;
  tiTipoLinha       = 26;
  tiOrigem          = 27;


(* Grid de tipos de cobran�a *)
  tcCobranca        = 0;
  tcValorDocumentos = 1;
  tcValorJuros      = 2;
  tcValorTotal      = 3;
  tcQtdeTitulos     = 4;

  // Tipos de linhas
  coLinhaDetalhe   = 'D';
  coLinhaCabAgrup  = 'CAGRU';
  coLinhaTotVenda  = 'TV';
  coTotalAgrupador = 'TAGRU';

procedure TFormRelacaoContasPagar.Carregar(Sender: TObject);
var
  i: Integer;
  j: Integer;
  vComando: string;
  vAgrupadorId: string;
  vLinha: Integer;

  vValorCartao: Currency;
  vValorCheque: Currency;
  vValorCobranca: Currency;

  vTiposCobrancasIds: TArray<Integer>;

  vTotais: record
    totalDocumento: Double;
    totalAdiantamento: Double;
    totalDesconto: Double;
    totalSaldo: Double;
  end;

  vTotaisAgrupador: record
    totalDocumento: Double;
    totalAdiantamento: Double;
    totalDesconto: Double;
    totalSaldo: Double;
  end;

  procedure TotalizarGrupo;
  begin
    Inc(vLinha);

    sgTitulos.Cells[tiFornecedor, vLinha]        := 'TOTAL DO PLANO FINANCEIRO: ';
    sgTitulos.Cells[tiValorDocumento, vLinha] := NFormatN(vTotaisAgrupador.totalDocumento);
    sgTitulos.Cells[tiValorAdiantado, vLinha] := NFormatN(vTotaisAgrupador.totalAdiantamento);
    sgTitulos.Cells[tiValorDesconto, vLinha]  := NFormatN(vTotaisAgrupador.totalDesconto);
    sgTitulos.Cells[tiValorSaldo, vLinha]     := NFormatN(vTotaisAgrupador.totalSaldo);;
    sgTitulos.Cells[tiTipoLinha, vLinha]      := coTotalAgrupador;
    sgTitulos.Cells[tiSelecionado, vLinha]    := '---';

    vTotaisAgrupador.totalDocumento    := 0;
    vTotaisAgrupador.totalAdiantamento := 0;
    vTotaisAgrupador.totalDesconto     := 0;
    vTotaisAgrupador.totalSaldo        := 0;

    Inc(vLinha);
  end;
begin
  inherited;
  vComando := '';
  sgTitulos.ClearGrid;
  sgTitulosTipoCobranca.ClearGrid;

  vValorCartao   := 0;
  vValorCheque   := 0;
  vValorCobranca := 0;

  FDadosCobrancas := nil;
  vTiposCobrancasIds := nil;

  if not frContaPagar.EstaVazio then
    WhereOuAnd(vComando, frContaPagar.TrazerFiltros('CON.PAGAR_ID'));

  if not FrFornecedores.EstaVazio then
    WhereOuAnd(vComando, FrFornecedores.getSqlFiltros('CON.CADASTRO_ID'));

  if not FrEmpresas.EstaVazio then
    WhereOuAnd(vComando, FrEmpresas.getSqlFiltros('CON.EMPRESA_ID'));

  if not FrCodigoEntrada.EstaVazio then
    WhereOuAnd(vComando, FrCodigoEntrada.TrazerFiltros('CON.ENTRADA_ID'));

  if not FrPedidoCompra.EstaVazio then
    WhereOuAnd(vComando, FrPedidoCompra.TrazerFiltros('CON.COMPRA_ID'));

  if not FrCodigoBaixa.EstaVazio then
    WhereOuAnd(vComando, FrCodigoBaixa.TrazerFiltros('BAI.BAIXA_ID'));

  if not FrTiposCobranca.EstaVazio then
    WhereOuAnd(vComando, FrTiposCobranca.getSqlFiltros('CON.COBRANCA_ID'));

  if FrDataCadastro.DatasValidas then
    WhereOuAnd(vComando, FrDataCadastro.getSqlFiltros('CON.DATA_CADASTRO'));

  if FrDataEmissao.DatasValidas then
    WhereOuAnd(vComando, FrDataEmissao.getSqlFiltros('CON.DATA_EMISSAO'));

  if FrDataContabil.DatasValidas then
    WhereOuAnd(vComando, FrDataContabil.getSqlFiltros('CON.DATA_CONTABIL'));

  if FrDataVencimento.DatasValidas then
    WhereOuAnd(vComando, FrDataVencimento.getSqlFiltros('CON.DATA_VENCIMENTO'));

  if FrDataPagamento.DatasValidas then
    WhereOuAnd(vComando, FrDataPagamento.getSqlFiltros('BAI.DATA_PAGAMENTO'));

  if FrDataBaixa.DatasValidas then
    WhereOuAnd(vComando, FrDataBaixa.getSqlFiltros('BAI.DATA_HORA_BAIXA'));

  if not FrNumeroDocumento.EstaVazio then
    WhereOuAnd(vComando, FrNumeroDocumento.TrazerFiltros('CON.DOCUMENTO'));

  if not gbOrigem.NenhumMarcado then
    WhereOuAnd(vComando, gbOrigem.GetSql('CON.ORIGEM'));

  if not gbStatusFinanceiro.TodosMarcados then begin
    if ckTitulosEmAberto.Checked then
      WhereOuAnd(vComando, ' CON.STATUS = ''A'' ')
    else if ckTitulosBaixados.Checked then
      WhereOuAnd(vComando, ' CON.STATUS = ''B'' ');
  end;

  if cbBloqueados.GetValor <> _Biblioteca.cbNaoFiltrar then
    _Biblioteca.WhereOuAnd(vComando, ' CON.BLOQUEADO = ''' + cbBloqueados.GetValor + '''');

  if not FrCentrosCustos.EstaVazio then
    WhereOuAnd(vComando, FrCentrosCustos.getSqlFiltros('CON.CENTRO_CUSTO_ID'));

  if not FrPlanosFinanceiros.EstaVazio then
    WhereOuAnd(vComando, FrPlanosFinanceiros.getSqlFiltros('CON.PLANO_FINANCEIRO_ID'));

   vComando := vComando + ' order by ';
   if cbAgrupamento.GetValor = 'PLA' then
     vComando := vComando + ' CON.PLANO_FINANCEIRO_ID, CON.DATA_VENCIMENTO '
   else
     vComando := vComando + ' CON.DATA_VENCIMENTO ';

  FTitulos := _RelacaoContasPagar.BuscarContasPagarComando(Sessao.getConexaoBanco, vComando);
  if FTitulos = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  vAgrupadorId := '';

  vTotais.totalDocumento     := 0;
  vTotais.totalAdiantamento  := 0;
  vTotais.totalDesconto      := 0;
  vTotais.totalSaldo         := 0;

  vTotaisAgrupador.totalDocumento    := 0;
  vTotaisAgrupador.totalAdiantamento := 0;
  vTotaisAgrupador.totalDesconto     := 0;
  vTotaisAgrupador.totalSaldo        := 0;

  vLinha := 0;
  for i := Low(FTitulos) to High(FTitulos) do begin
    if cbAgrupamento.GetValor = 'PLA' then begin
      if vAgrupadorId <> FTitulos[i].PlanoFinanceiroId then begin
        if i > 0 then begin
          TotalizarGrupo;
        end;
        Inc(vLinha);

        sgTitulos.Cells[tiFornecedor, vLinha] := getInformacao(FTitulos[i].PlanoFinanceiroId, FTitulos[i].NomePlanoFinanceiro);
        sgTitulos.Cells[tiTipoLinha, vLinha]    := coLinhaCabAgrup;
        sgTitulos.Cells[tiSelecionado, vLinha]  := '---';

        vAgrupadorId := FTitulos[i].PlanoFinanceiroId;
      end;
    end;

    Inc(vLinha);

    sgTitulos.Cells[tiSelecionado, vLinha]     := IfThen((FTitulos[i].status = 'Bx'), charNaoPermiteSelecionar, charNaoSelecionado);
    sgTitulos.Cells[tiPagarId, vLinha]         := NFormat(FTitulos[i].PagarId);
    sgTitulos.Cells[tiFornecedor, vLinha]      := NFormat(FTitulos[i].fornecedor_id) + ' - ' + FTitulos[i].nome_fornecedor;

    if FTitulos[i].status = 'A' then begin
      if FTitulos[i].bloqueado = 'S' then
        sgTitulos.Cells[tiSituacao, vLinha] := 'Bl'
      else
        sgTitulos.Cells[tiSituacao, vLinha] := 'Ab';
    end
    else
      sgTitulos.Cells[tiSituacao, vLinha]   := 'Bx';

    sgTitulos.Cells[tiDocumento, vLinha]       := FTitulos[i].documento;
    sgTitulos.Cells[tiParcela, vLinha]         := NFormat(FTitulos[i].parcela) + '/' + NFormat(FTitulos[i].numero_parcelas);
    sgTitulos.Cells[tiDataContabil, vLinha]    := DFormat(FTitulos[i].Data_Contabil);
    sgTitulos.Cells[tiDataVencimento, vLinha]  := DFormat(FTitulos[i].data_vencimento);
    sgTitulos.Cells[tiValorDocumento, vLinha]  := NFormatN(FTitulos[i].valor_documento);
    sgTitulos.Cells[tiValorAdiantado, vLinha]  := NFormatN(FTitulos[i].ValorAdiantado);
    sgTitulos.Cells[tiValorDesconto, vLinha]   := NFormatN(FTitulos[i].ValorDesconto);
    sgTitulos.Cells[tiValorSaldo, vLinha]      := NFormatN(FTitulos[i].ValorSaldo);
    sgTitulos.Cells[tiTipoCobranca, vLinha]    := NFormat(FTitulos[i].cobranca_id) + ' - ' + FTitulos[i].nome_tipo_cobranca;
    sgTitulos.Cells[tiDataCadastro, vLinha]    := DFormat(FTitulos[i].data_cadastro);
    sgTitulos.Cells[tiDiasAtraso, vLinha]      := NFormatN(IfThen(FTitulos[i].dias_atraso > 0, FTitulos[i].dias_atraso));
    sgTitulos.Cells[tiEmpresa, vLinha]         := NFormat(FTitulos[i].empresa_id) + ' - ' + FTitulos[i].nome_empresa;
    sgTitulos.Cells[tiPlanoFinanceiro, vLinha] := getInformacao(FTitulos[i].PlanoFinanceiroId, FTitulos[i].NomePlanoFinanceiro);
    sgTitulos.Cells[tiCentroDeCustos, vLinha]  := getInformacao(FTitulos[i].CentroCustoId, FTitulos[i].NomeCentroCusto);
    sgTitulos.Cells[tiFornecedorId, vLinha]    := NFormat(FTitulos[i].fornecedor_id);
    sgTitulos.Cells[tiStatusSintetico, vLinha] := FTitulos[i].status;
    sgTitulos.Cells[tiFormaPagamento, vLinha]  := FTitulos[i].forma_pagamento;
    sgTitulos.Cells[tiStatusAnalitico, vLinha] := IfThen(FTitulos[i].status = 'A', 'Em aberto', 'Baixado');

    sgTitulos.Cells[tiOrigem, vLinha]  := FTitulos[i].origem;

    if FTitulos[i].forma_pagamento = 'CHQ' then
      vValorCheque := vValorCheque + FTitulos[i].valor_documento
    else if FTitulos[i].forma_pagamento = 'CRT' then
      vValorCartao := vValorCartao + FTitulos[i].valor_documento
    else
      vValorCobranca := vValorCobranca + FTitulos[i].valor_documento;

    sgTitulos.Cells[tiBloqueado, vLinha]            := _Biblioteca.SimNao(FTitulos[i].bloqueado);
    sgTitulos.Cells[tiPlanoFinanceiroId, vLinha]    := FTitulos[i].PlanoFinanceiroId;
    sgTitulos.Cells[tiBaixaReceberOrigemId, vLinha] := NFormat(FTitulos[i].BaixaReceberOrigemId);
    sgTitulos.Cells[tiCobrancaId, vLinha]           := NFormat(FTitulos[i].cobranca_id);
    sgTitulos.Cells[tiTipoLinha, vLinha]            := coLinhaDetalhe;

    vTotais.totalDocumento    := vTotais.totalDocumento + _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorDocumento, vLinha]);
    vTotais.totalAdiantamento := vTotais.totalAdiantamento + _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorAdiantado, vLinha]);
    vTotais.totalDesconto     := vTotais.totalDesconto + _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorDesconto, vLinha]);
    vTotais.totalSaldo        := vTotais.totalSaldo + _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorSaldo, vLinha]);

    vTotaisAgrupador.totalDocumento      := vTotaisAgrupador.totalDocumento + _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorDocumento, vLinha]);
    vTotaisAgrupador.totalAdiantamento   := vTotaisAgrupador.totalAdiantamento + _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorAdiantado, vLinha]);
    vTotaisAgrupador.totalDesconto := vTotaisAgrupador.totalDesconto + _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorDesconto, vLinha]);
    vTotaisAgrupador.totalSaldo        := vTotaisAgrupador.totalSaldo + _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorSaldo, vLinha]);

    if (sgTitulos.Cells[tiTipoLinha, vLinha] <> coLinhaDetalhe) then
      sgTitulos.Cells[tiSelecionado, vLinha] := charNaoPermiteSelecionar
    else
      sgTitulos.Cells[tiSelecionado, vLinha] := charNaoSelecionado;

    _Biblioteca.AddNoVetorSemRepetir(vTiposCobrancasIds, FTitulos[i].cobranca_id);
  end;

  if cbAgrupamento.GetValor <> 'NEN' then begin
    TotalizarGrupo;
    Inc(vLinha);
  end;

  sgTitulos.SetLinhasGridPorTamanhoVetor(vLinha);

  //sgTitulos.RowCount := IfThen(Length(FTitulos) > 1, High(FTitulos) + 2, 2);

  stValorCheque.Caption   := NFormat(vValorCheque);
  stValorCartao.Caption   := NFormat(vValorCartao);
  stValorCobranca.Caption := NFormat(vValorCobranca);
  stValorTotal.Caption    := NFormat(vValorCheque + vValorCartao + vValorCobranca);

  stQuantidadeTitulos.Caption := NFormat(Length(FTitulos));

  SetLength(FDadosCobrancas, Length(vTiposCobrancasIds));
  for i := Low(vTiposCobrancasIds) to High(vTiposCobrancasIds) do begin
    FDadosCobrancas[i].cobranca_id   := vTiposCobrancasIds[i];
    FDadosCobrancas[i].qtde_titulos  := 0;
    FDadosCobrancas[i].valor_titulos := 0;
    FDadosCobrancas[i].valor_juros   := 0;
    FDadosCobrancas[i].valor_total   := 0;
    for j := Low(FTitulos) to High(FTitulos) do begin
      if vTiposCobrancasIds[i] <> FTitulos[j].cobranca_id then
        Continue;

      FDadosCobrancas[i].nome_tipo_cobranca := FTitulos[j].nome_tipo_cobranca;
      FDadosCobrancas[i].valor_titulos      := FDadosCobrancas[i].valor_titulos + FTitulos[j].valor_documento;
      (*FDadosCobrancas[i].valor_juros      := FDadosCobrancas[i].valor_juros + FTitulos[j].valor_juros*);
      FDadosCobrancas[i].valor_total        := FDadosCobrancas[i].valor_total + FTitulos[j].valor_documento;

      Inc(FDadosCobrancas[i].qtde_titulos);
    end;
  end;

  with sgTitulosTipoCobranca do begin
    for i := Low(FDadosCobrancas) to High(FDadosCobrancas) do begin
      Cells[tcCobranca, i + 1]        := NFormat(FDadosCobrancas[i].cobranca_id) + ' - ' + FDadosCobrancas[i].nome_tipo_cobranca;
      Cells[tcValorDocumentos, i + 1] := NFormat(FDadosCobrancas[i].valor_titulos);
      Cells[tcValorJuros, i + 1]      := NFormat(FDadosCobrancas[i].valor_juros);
      Cells[tcValorTotal, i + 1]      := NFormat(FDadosCobrancas[i].valor_total);
      Cells[tcQtdeTitulos, i + 1]     := NFormat(FDadosCobrancas[i].qtde_titulos);
    end;
    RowCount := IfThen(Length(FDadosCobrancas) > 1, High(FDadosCobrancas) + 2, 2);
  end;

  pcDados.ActivePage := tsResultado;
  pcResultado.ActivePage := tsAnalitico;

  rgTipoRepresentacaoGraficoClick(nil);
end;

procedure TFormRelacaoContasPagar.FormCreate(Sender: TObject);
begin
  inherited;
  pcDados.ActivePageIndex := 0;
  pcResultado.ActivePageIndex := 0;

  ActiveControl := FrEmpresas;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId, False);
  FrTiposCobranca.setNatureza('P');
end;

procedure TFormRelacaoContasPagar.Imprimir(Sender: TObject);
var
  vCnt: Integer;
  totalPagar: Double;
begin
  cdsPagar.Close;
  cdsPagar.CreateDataSet;
  cdsPagar.Open;
  inherited;
  totalPagar := 0.0;
  for vCnt := sgTitulos.RowCount - 1 downto 1 do
  begin
    if (sgTitulos.Cells[tiPagarId,vCnt] = '') and (sgTitulos.Cells[tiFornecedor,vCnt] = '') then
      Continue;

    cdsPagar.Insert;
    cdsPagarCodigo.AsString       := sgTitulos.Cells[tiPagarId,vCnt];
    cdsPagarDocumento.AsString    := sgTitulos.Cells[tiDocumento,vCnt];
    cdsPagarNomeFornecedor.AsString  := sgTitulos.Cells[tiFornecedor,vCnt];
    cdsPagarsituacao.AsString     := sgTitulos.Cells[tiSituacao,vCnt];
    cdsPagarParcela.AsString      := sgTitulos.Cells[tiParcela,vCnt];
    cdsPagarValor.AsFloat         := SFormatDouble(sgTitulos.Cells[tiValorDocumento,vCnt]);
    cdsPagarSaldo.AsFloat         := SFormatDouble(sgTitulos.Cells[tiValorSaldo,vCnt]);

    if sgTitulos.Cells[tiPagarId,vCnt] <> '' then begin
      cdsPagarDtVencto.AsDateTime   := ToData(sgTitulos.Cells[tiDataVencimento,vCnt]);
      cdsPagarDiasAtraso.AsInteger  := StrToIntDef(sgTitulos.Cells[tiDiasAtraso,vCnt],0);
      cdsPagarAdiantamento.AsFloat  := SFormatDouble(sgTitulos.Cells[tiValorAdiantado,vCnt]);
      cdsPagarDesconto.AsFloat      := SFormatDouble(sgTitulos.Cells[tiValorDesconto,vCnt]);

      totalPagar := totalPagar + SFormatDouble(sgTitulos.Cells[tiValorSaldo,vCnt]);
    end;

    cdsPagar.Post;
  end;


  TfrxMemoView(frxReport.FindComponent('mmValorProduto')).Text :=   'Valor Total Liquido.....: ' + NFormat(totalPagar);
  frxReport.ShowReport;
end;

procedure TFormRelacaoContasPagar.miAdiantarTituloFocadoClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar;
begin
  inherited;
  vRetTela := AdiantamentoContasPagar.AdiantarTitulo(SFormatInt( sgTitulos.Cells[tiPagarId, sgTitulos.Row]));
  if vRetTela.BuscaCancelada then
    Exit;

  Carregar(Sender);
end;

procedure TFormRelacaoContasPagar.miAlterarCampoDocumentoClick(Sender: TObject);
var
  vDocumento: string;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if not Sessao.AutorizadoRotina('alterar_documento_conta_pagar') then begin
    _Biblioteca.NaoAutorizadoRotina;
    Exit;
  end;

  if SFormatInt(sgTitulos.Cells[tiPagarId, sgTitulos.Row]) = 0 then
    Exit;

  if Sessao.TipoCobrancaNaoPermiteAlteracaoValor(SFormatInt(sgTitulos.Cells[tiCobrancaId, sgTitulos.Row])) then begin
    _Biblioteca.Exclamar('N�o � permitido alterar o valor deste financeiro por causa do seu tipo de cobran�a!');
    Exit;
  end;

  if SFormatInt(sgTitulos.Cells[tiBaixaReceberOrigemId, sgTitulos.Row]) > 0 then begin
    _Biblioteca.Exclamar('N�o � permitido alterar um contas a pagar originado de uma baixa de contas a receber!');
    Exit;
  end;

  vDocumento := BuscaDados.BuscarDados('Documento', sgTitulos.Cells[tiDocumento, sgTitulos.Row], 24);
  if vDocumento = '' then
    Exit;

  vRetBanco :=
    _ContasPagar.AtualizarCampoDocumento(
      Sessao.getConexaoBanco,
      SFormatInt(sgTitulos.Cells[tiPagarId, sgTitulos.Row]),
      vDocumento
    );

  Sessao.AbortarSeHouveErro( vRetBanco );

  _Biblioteca.RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoContasPagar.miAlterarDataContabilClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar<TDateTime>;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if not Sessao.AutorizadoRotina('permitir_alterar_data_contabil_pagar') then begin
    _Biblioteca.NaoAutorizadoRotina;
    Exit;
  end;

  vRetTela := BuscarDadosData.Buscar( 'Data cont�bil', _Biblioteca.ToData( sgTitulos.Cells[tiDataContabil, sgTitulos.row] ) );
  if vRetTela.BuscaCancelada then
    Exit;


  vRetBanco := _ContasPagar.AtualizarDataContabil( Sessao.getConexaoBanco, SFormatInt(sgTitulos.Cells[tiPagarId, sgTitulos.row]), vRetTela.Dados );
  Sessao.AbortarSeHouveErro( vRetBanco );

  _Biblioteca.RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoContasPagar.miAlterarDataVencimentoClick(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  vPagarIds: TArray<Integer>;
  vPosicPrimeiroSelecionado: Integer;
  vRetTela: TRetornoTelaFinalizar<TDateTime>;
begin
  inherited;

  if not Sessao.AutorizadoRotina('permitir_alterar_data_vencimento_pagar') then begin
    _Biblioteca.NaoAutorizadoRotina;
    Exit;
  end;

  vPagarIds := nil;
  vPosicPrimeiroSelecionado := -1;
  for i := 1 to sgTitulos.RowCount -1 do begin
    if sgTitulos.Cells[tiSelecionado, i] = charNaoSelecionado then
      Continue;

    if vPosicPrimeiroSelecionado = -1 then
      vPosicPrimeiroSelecionado := i;

    SetLength(vPagarIds, Length(vPagarIds) + 1);
    vPagarIds[High(vPagarIds)] := SFormatInt( sgTitulos.Cells[tiPagarId, i] );
  end;

  if vPagarIds = nil then begin
    _Biblioteca.Exclamar('Nenhum t�tulo foi selecionado!');
    SetarFoco(sgTitulos);
    Exit;
  end;

  vRetTela := BuscarDadosData.Buscar( 'Data de vencto', _Biblioteca.ToData( sgTitulos.Cells[tiDataVencimento, vPosicPrimeiroSelecionado] ) );
  if vRetTela.BuscaCancelada then
    Exit;

  vRetBanco := _ContasPagar.AtualizarDataVencimento( Sessao.getConexaoBanco, vPagarIds, vRetTela.Dados );
  Sessao.AbortarSeHouveErro( vRetBanco );

  _Biblioteca.RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoContasPagar.miAlterarplanofinanceiroFocado1Click(Sender: TObject);
var
  vPagarId: Integer;
  vRetBanco: RecRetornoBD;
  vRetTela: TRetornoTelaFinalizar<string>;
begin
  inherited;
  vPagarId := SFormatInt(sgTitulos.Cells[tiPagarId, sgTitulos.Row]);
  if vPagarId = 0 then
    Exit;

  vRetTela := BuscarPlanoFinanceiro.Buscar(sgTitulos.Cells[tiPlanoFinanceiroId, sgTitulos.Row]);
  if vRetTela.BuscaCancelada then
    Exit;

  vRetBanco := _ContasPagar.AtualizarPlanoFinanceiro( Sessao.getConexaoBanco, vPagarId, vRetTela.Dados );
  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoContasPagar.miAlterarValorDescontoClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar<Double>;
  vRetBanco: RecRetornoBD;
begin
  inherited;

  if SFormatInt(sgTitulos.Cells[tiPagarId, sgTitulos.Row]) = 0 then
    Exit;

  vRetTela := BuscaDados.BuscarDados('Valor do desconto', SFormatDouble(sgTitulos.Cells[tiValorDesconto, sgTitulos.Row]), SFormatDouble(sgTitulos.Cells[tiValorDocumento, sgTitulos.Row]) - 0.01);
  if vRetTela.BuscaCancelada then
    Exit;

  vRetBanco :=
    _ContasPagar.AtualizarValorDesconto(
      Sessao.getConexaoBanco,
      SFormatInt(sgTitulos.Cells[tiPagarId, sgTitulos.Row]),
      vRetTela.Dados
    );

  Sessao.AbortarSeHouveErro( vRetBanco );

  _Biblioteca.RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoContasPagar.miAlterarValorDocumentoClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar<Double>;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if sgTitulos.Cells[tiStatusSintetico, sgTitulos.Row] = 'B' then begin
    Exclamar('Este t�tulo j� foi baixado!');
    SetarFoco(sgTitulos);
    Exit;
  end;

  if SFormatInt(sgTitulos.Cells[tiPagarId, sgTitulos.Row]) = 0 then
    Exit;

  if not Sessao.AutorizadoRotina('permitir_alterar_valor_documento_pagar') then begin
    _Biblioteca.NaoAutorizadoRotina;
    Exit;
  end;

  vRetTela := BuscaDados.BuscarDados('Valor do documento', SFormatDouble(sgTitulos.Cells[tiValorDocumento, sgTitulos.Row]), 0);
  if vRetTela.BuscaCancelada then
    Exit;

  vRetBanco :=
    _ContasPagar.AtualizarValorDocumento(
      Sessao.getConexaoBanco,
      SFormatInt(sgTitulos.Cells[tiPagarId, sgTitulos.Row]),
      vRetTela.Dados
    );

  Sessao.AbortarSeHouveErro( vRetBanco );

  _Biblioteca.RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoContasPagar.miBaixarTituloClick(Sender: TObject);
begin
  inherited;
  if sgTitulos.Cells[tiStatusSintetico, sgTitulos.Row] = 'B' then begin
    Exclamar('Este t�tulo j� foi baixado!');
    SetarFoco(sgTitulos);
    Exit;
  end;

  if (sgTitulos.Cells[tiBloqueado, sgTitulos.Row] = 'Sim') then begin
    Exclamar('Este t�tulo est� bloqueado! Opera��o n�o permitida.');
    Exit;
  end;

  if BaixarTitulosPagar.BaixarTitulos( SFormatInt(sgTitulos.Cells[tiFornecedorId, sgTitulos.Row]), [SFormatInt(sgTitulos.Cells[tiPagarId, sgTitulos.Row])], True ).RetTela = trOk then
    Carregar(Sender);
end;

procedure TFormRelacaoContasPagar.miBaixarTitulosSelecionadosClick(Sender: TObject);
var
  i: Integer;
  vQtdeBloqueados: Integer;
  vTitulosIds: TArray<Integer>;

  vFornecedorId: Integer;
  vTemFornecedorDiferente: Boolean;
begin
  inherited;

  vFornecedorId := 0;
  vQtdeBloqueados := 0;

  vTitulosIds := nil;
  vTemFornecedorDiferente := False;
  for i := sgTitulos.FixedRows to sgTitulos.RowCount -1 do begin
    if
      (sgTitulos.Cells[tiSelecionado, i] = charNaoSelecionado) or
      (sgTitulos.Cells[tiStatusSintetico, i] = 'B')
    then
      Continue;

    if (sgTitulos.Cells[tiBloqueado, i] = 'Sim') then begin
      vQtdeBloqueados := vQtdeBloqueados + 1;
      Continue;
    end;

    SetLength(vTitulosIds, Length(vTitulosIds) + 1);
    vTitulosIds[High(vTitulosIds)] := SFormatInt(sgTitulos.Cells[tiPagarId, i]);

    if vFornecedorId = 0 then
      vFornecedorId := SFormatInt(sgTitulos.Cells[tiFornecedorId, i])
    else if not vTemFornecedorDiferente then
      vTemFornecedorDiferente := vFornecedorId <> SFormatInt(sgTitulos.Cells[tiFornecedorId, i]);
  end;

  if vQtdeBloqueados > 0 then begin
    Exclamar('Existem ' + NFormat(vQtdeBloqueados) + ' t�tulos bloqueados selecionados, opera��o n�o permitida!');
    Exit;
  end;

  if BaixarTitulosPagar.BaixarTitulos(vFornecedorId, vTitulosIds, not vTemFornecedorDiferente).RetTela = trOk then
    Carregar(Sender)
end;

procedure TFormRelacaoContasPagar.miBloquearDesbloquearTitulosSelecionadosClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar;

  i: Integer;
  vQtdeBaixados: Integer;
  vTitulosIds: TArray<Integer>;
begin
  inherited;

  vQtdeBaixados   := 0;
  for i := sgTitulos.FixedRows to sgTitulos.RowCount -1 do begin
    if sgTitulos.Cells[tiSelecionado, i] = charNaoSelecionado then
      Continue;

    if sgTitulos.Cells[tiStatusSintetico, i] = 'B' then begin
      vQtdeBaixados := vQtdeBaixados + 1;
      Continue;
    end;

    _Biblioteca.AddNoVetorSemRepetir(vTitulosIds, SFormatInt(sgTitulos.Cells[tiPagarId, i]));
  end;

  if vQtdeBaixados > 0 then begin
    Exclamar('Existem ' + NFormat(vQtdeBaixados) + ' t�tulos baixados selecionados, opera��o n�o permitida!');
    Exit;
  end;

  if vTitulosIds = nil then begin
    _Biblioteca.NenhumRegistroSelecionado;
    Exit;
  end;

  if not Sessao.AutorizadoRotina('permitir_bloquear_desbloquear_credito_pagar') then begin
    _Biblioteca.NaoAutorizadoRotina;
    Exit;
  end;

  vRetTela := BloquearDesbloquearTitulos.Bloquear(vTitulosIds);
  if vRetTela.BuscaCancelada then
    Exit;

  _Biblioteca.RotinaSucesso;
  Carregar(Sender);
end;

//procedure TFormRelacaoContasPagar.miEnviarParaRecebimentoCaixaClick(
//  Sender: TObject);
//begin
  //inherited;

//end;

procedure TFormRelacaoContasPagar.miExcluirTituloClick(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;

  if not _Biblioteca.Perguntar('Aten��o, deseja realmente excluir o financeiro selecionado? Este processo � irrevers�vel.') then
    Exit;

  vRetBanco := _ContasPagar.ExcluirContaPagar(Sessao.getConexaoBanco, SFormatInt(sgTitulos.Cells[tiPagarId, sgTitulos.Row]) );
  Sessao.AbortarSeHouveErro( vRetBanco );

  _Biblioteca.Informar('Conta a pagar exclu�do com sucesso.');
  Carregar(Sender);
end;

procedure TFormRelacaoContasPagar.miIncluirNovoTituloClick(Sender: TObject);
begin
  inherited;
  Sessao.AbrirTela(TFormInclusaoTitulosPagar);
end;

procedure TFormRelacaoContasPagar.miMarcarDesmarcarTItuloFocadoClick(Sender: TObject);
begin
  inherited;
  MarcarDesmarcarTodos(sgTitulos, tiSelecionado, VK_SPACE, []);
end;

procedure TFormRelacaoContasPagar.miMarcarDesmarcarTodosTitulosClick(Sender: TObject);
begin
  inherited;
  MarcarDesmarcarTodos(sgTitulos, tiSelecionado, VK_SPACE, [ssCtrl]);
end;

procedure TFormRelacaoContasPagar.miReimprimirComprovanteCreditoClick(Sender: TObject);
begin
  inherited;
  ImpressaoComprovanteCreditoPagarGrafico.Imprimir( SFormatInt(sgTitulos.Cells[tiPagarId, sgTitulos.Row]) );
end;

procedure TFormRelacaoContasPagar.pmRotinasPopup(Sender: TObject);
var
  i: Integer;
  vHabilitarBaixas: Boolean;
begin
  inherited;
  vHabilitarBaixas := False;
  for i := sgTitulos.FixedRows to sgTitulos.RowCount -1 do begin
    if
      (sgTitulos.Cells[tiSelecionado, i] = charNaoSelecionado) or
      (sgTitulos.Cells[tiStatusSintetico, i] = 'B')
    then
      Continue;

    vHabilitarBaixas := True;
    Break;
  end;
  miBaixarTitulo.Enabled := (sgTitulos.Cells[tiStatusSintetico, sgTitulos.Row] = 'A');
  miBaixarTitulosSelecionados.Enabled  := vHabilitarBaixas;
  //miEnviarParaRecebimentoCaixa.Enabled := vHabilitarBaixas;
end;

procedure TFormRelacaoContasPagar.rgTipoRepresentacaoGraficoClick(Sender: TObject);
var
  i: Integer;

const
  trValorTotal  = 0;
  trQtdeTitulos = 1;
begin
  inherited;
  chTiposCobranca.Series[0].Clear;
  chTiposCobranca.Series[0].Marks.FontSeriesColor := True;
  chTiposCobranca.Title.Text.Text := 'Representa��o gr�fica(' + IfThen(rgTipoRepresentacaoGrafico.ItemIndex = trValorTotal, 'Valor total', 'Quantidade de t�tulos') + ')';

  for i := Low(FDadosCobrancas) to High(FDadosCobrancas) do begin
    if rgTipoRepresentacaoGrafico.ItemIndex = trQtdeTitulos then begin
      chTiposCobranca.Series[0].Add(
        FDadosCobrancas[i].qtde_titulos / Length(FTitulos) * 100,
        '  ' + NFormat(FDadosCobrancas[i].cobranca_id) + ' - ' + FDadosCobrancas[i].nome_tipo_cobranca
      );
    end
    else begin
      chTiposCobranca.Series[0].Add(
        FDadosCobrancas[i].valor_total / SFormatDouble(stValorTotal.Caption) * 100,
        '  ' + NFormat(FDadosCobrancas[i].cobranca_id) + ' - ' + FDadosCobrancas[i].nome_tipo_cobranca
      );
    end;
  end;
  chTiposCobranca.Repaint;
end;

procedure TFormRelacaoContasPagar.sbGerarPlanilhaClick(Sender: TObject);
begin
  inherited;
  GridToExcel(sgTitulos);
end;

procedure TFormRelacaoContasPagar.sgTitulosDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.TituloPagar.Informar( SFormatInt(sgTitulos.Cells[tiPagarId, sgTitulos.Row]) );
end;

procedure TFormRelacaoContasPagar.sgTitulosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[tiPagarId, tiValorDocumento, tiParcela, tiValorDesconto, tiValorSaldo, tiValorAdiantado, tiDiasAtraso] then
    vAlinhamento := taRightJustify
  else if ACol in[tiSelecionado, tiStatusAnalitico, tiBloqueado, tiSituacao, tiDataContabil, tiDataVencimento] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgTitulos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoContasPagar.sgTitulosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = tiSituacao then begin
    AFont.Style := [fsBold];
    if sgTitulos.Cells[tiSituacao, ARow] = 'Ab' then
      AFont.Color := clGreen
    else
    if sgTitulos.Cells[tiSituacao, ARow] = 'Bl' then
      AFont.Color := clRed
    else
      AFont.Color := clBlue;
  end;

  if ACol = tiStatusAnalitico then begin
    AFont.Style := [fsBold];
    if sgTitulos.Cells[tiStatusSintetico, ARow] = 'A' then
      AFont.Color := $000096DB
    else
      AFont.Color := clBlue;
  end
  else if ACol = tiDiasAtraso then
    AFont.Color := clRed
  else if ACol = tiValorSaldo then
    AFont.Color := $000096DB
  else if ACol = tiBloqueado then begin
    AFont.Style := [fsBold];
    AFont.Color := _Biblioteca.VermelhoAzul(sgTitulos.Cells[ACol, ARow]);
  end;

  if sgTitulos.Cells[tiTipoLinha, ARow] = coLinhaTotVenda then begin
    AFont.Color := clWhite;
    AFont.Style := [fsBold];
    ABrush.Color := $000096DB;
  end
  else if sgTitulos.Cells[tiTipoLinha, ARow] = coLinhaCabAgrup then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end
  else if sgTitulos.Cells[tiTipoLinha, ARow] = coTotalAgrupador then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao3;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao3;
  end;
end;

procedure TFormRelacaoContasPagar.sgTitulosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if (ACol <> tiSelecionado) or (sgTitulos.Cells[ACol, ARow] = '') then
   Exit;

  if sgTitulos.Cells[ACol, ARow] = charNaoSelecionado then
    APicture := _Imagens.FormImagens.im1.Picture
  else if sgTitulos.Cells[ACol, ARow] = charSelecionado then
    APicture := _Imagens.FormImagens.im2.Picture;
end;

procedure TFormRelacaoContasPagar.sgTitulosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  MarcarDesmarcarTodos(sgTitulos, tiSelecionado, Key ,Shift);
end;

procedure TFormRelacaoContasPagar.sgTitulosTipoCobrancaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = tcCobranca then
    vAlinhamento := taLeftJustify
  else
    vAlinhamento := taRightJustify;

  sgTitulosTipoCobranca.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoContasPagar.SpeedButton1Click(Sender: TObject);
var
  i: Integer;
  vSomenteManual: Boolean;
  vRetorno: TRetornoTelaFinalizar<string>;
  vRetornoDadosFornecedor: TRetornoTelaFinalizar<RecFornecedores>;
  vPagarIds: TArray<Integer>;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if not Sessao.AutorizadoRotina('permitir_alterar_fornecedor') then begin
    _Biblioteca.NaoAutorizadoRotina;
    Exit;
  end;

  vSomenteManual := true;

  vRetornoDadosFornecedor := BuscarDadosFornecedor.BuscarDadosFornecedorConta();
  if vRetornoDadosFornecedor.BuscaCancelada then
    Exit;

  for i := sgTitulos.FixedRows to sgTitulos.RowCount -2 do begin
    if sgTitulos.Cells[tiOrigem, i] = '' then begin
      Continue;
    end;

    if sgTitulos.Cells[tiOrigem, i] <> 'MAN' then begin
      vSomenteManual := false;
      Continue;
    end;

    if sgTitulos.Cells[tiSelecionado, i] <> charSelecionado then
      Continue;

    SetLength(vPagarIds, Length(vPagarIds) + 1);
    vPagarIds[High(vPagarIds)] := StrToInt(_Biblioteca.RetornaNumeros(sgTitulos.Cells[tiPagarId, i]));
  end;

  if vPagarIds = nil then begin
    _Biblioteca.Informar('Somente t�tulos de origem manual podem ser alterados!');
    Abort;
  end;

  if not vSomenteManual then
    _Biblioteca.Informar('T�tulos que n�o tem lan�amento manual n�o v�o ter os dados do fornecedor alterado!');

  vRetBanco :=
    _ContasPagar.AtualizarFornecedor(
      Sessao.getConexaoBanco,
      vPagarIds,
      vRetornoDadosFornecedor.Dados.cadastro_id
    );

  Sessao.AbortarSeHouveErro( vRetBanco );

  _Biblioteca.RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoContasPagar.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if gbOrigem.NenhumMarcado then begin
    Exclamar('� necess�rio selecionar pelo menos uma origem.');
    gbOrigem.SetFocus;
    Abort;
  end;
end;

end.
