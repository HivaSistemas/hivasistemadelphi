inherited FormPesquisaGruposPlanosFinanceiros: TFormPesquisaGruposPlanosFinanceiros
  Caption = 'Pesquisa de grupos de planos financeiros'
  ClientWidth = 579
  ExplicitWidth = 587
  ExplicitHeight = 240
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 579
    OnDrawCell = sgPesquisaDrawCell
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Descri'#231#227'o')
    RealColCount = 6
    ExplicitWidth = 579
    ColWidths = (
      28
      103
      427)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Text = '0'
  end
  inherited pnFiltro1: TPanel
    Width = 579
    ExplicitWidth = 579
    inherited eValorPesquisa: TEditLuka
      Width = 372
      ExplicitWidth = 439
    end
  end
end
