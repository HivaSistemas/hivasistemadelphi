inherited FormPesquisaOrcamentos: TFormPesquisaOrcamentos
  Caption = 'Pesquisa de or'#231'amentos'
  ClientHeight = 560
  ClientWidth = 778
  Visible = False
  OnShow = FormShow
  ExplicitWidth = 784
  ExplicitHeight = 589
  PixelsPerInch = 96
  TextHeight = 14
  object sgOrcamentos: TGridLuka
    Left = 0
    Top = 122
    Width = 778
    Height = 151
    Align = alTop
    ColCount = 6
    Ctl3D = True
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goThumbTracking]
    ParentCtl3D = False
    TabOrder = 0
    OnClick = sgOrcamentosClick
    OnDblClick = sgOrcamentosDblClick
    OnDrawCell = sgOrcamentosDrawCell
    OnEnter = sgOrcamentosEnter
    OnKeyDown = sgOrcamentosKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Or'#231'amento'
      'Data cadastro'
      'Cliente'
      'Vendedor'
      'Valor'
      'Empresa'
      'Status')
    Grid3D = False
    RealColCount = 6
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      67
      82
      199
      167
      68
      169)
  end
  object st1: TStaticTextLuka
    Left = 0
    Top = 273
    Width = 778
    Height = 17
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Itens do or'#231'amento / venda focado'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object sgProdutos: TGridLuka
    Left = 0
    Top = 290
    Width = 778
    Height = 270
    Align = alClient
    ColCount = 9
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
    TabOrder = 2
    OnDrawCell = sgProdutosDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Produto'
      'Nome'
      'Marca'
      'Pre'#231'o unit.'
      'Quantidade'
      'Und.'
      'Valor total'
      'Valor out.desp.'
      'Valor desc.')
    Grid3D = False
    RealColCount = 9
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      54
      269
      154
      69
      74
      33
      105
      89
      70)
  end
  object pnlTopo: TPanel
    Left = 0
    Top = 0
    Width = 778
    Height = 122
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object lbNomeFantasiaApelido: TLabel
      Left = 279
      Top = 45
      Width = 184
      Height = 14
      Caption = 'Nome do cliente consumidor final'
    end
    object SpeedButton1: TSpeedButton
      Left = 130
      Top = 94
      Width = 176
      Height = 22
      Caption = 'Buscar Pedido Web'
      Glyph.Data = {
        36060000424D3606000000000000360000002800000010000000200000000100
        18000000000000060000600F0000600F00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFD4EDF922A3DF109CDCB4E0F4FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3F3FB1CA1
        DE0095DA0095DA139DDDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFF0F9FD2DA8E10095DA0095DA0095DA26A5E0FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9FDFE42B0E40095DA0095
        DA0095DA1FA2DED7EEF9FFFFFFFFFFFFFFFFFF85A0AD2546560D26310D27322C
        50609BB4C059BAE70095DA0095DA0095DA32AAE1E7F5FCFFFFFFFFFFFFE2E9EC
        16323F0000000000000000000000000000000000000039540091D50095DA4AB4
        E5F4FAFDFFFFFFFFFFFFF9FAFB1029350000000000003741468C8C8D84868627
        303500000000000000375066BFE9FCFEFFFFFFFFFFFFFFFFFFFF618190000000
        010202A3A5A6FFFFFFFFFFFFFFFFFFFFFFFF7A7E800000000000008FAAB7FFFF
        FFFFFFFFFFFFFFFFFFFF0E252F000000565F63FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF2E383D000000224453FFFFFFFFFFFFFFFFFFFFFFFF000405000000
        B7B7B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F9091000000081C26FFFF
        FFFFFFFFFFFFFFFFFFFF000203000000C2C2C2FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF99999A000000071922FFFFFFFFFFFFFFFFFFFFFFFF081922000000
        6F767AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF444F540000001A3846FFFF
        FFFFFFFFFFFFFFFFFFFF46677600000004090BD0D0D0FFFFFFFFFFFFFFFFFFFF
        FFFFB0B2B3000101000000718F9DFFFFFFFFFFFFFFFFFFFFFFFFE8EDEF05151C
        00000004090B6E777AC3C3C3BDBDBD5C666B0102030000000F2733FCFDFDFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFBDCCD205151C00000000000000000000000000
        00000000000D232DDAE2E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        E9EDEF496978091B230002030004050C222B5B7B89F7F8F9FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      OnClick = SpeedButton1Click
    end
    inline FrCliente: TFrClientes
      Left = 3
      Top = 43
      Width = 270
      Height = 40
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      Color = clWhite
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      TabStop = True
      ExplicitLeft = 3
      ExplicitTop = 43
      ExplicitWidth = 270
      ExplicitHeight = 40
      inherited sgPesquisa: TGridLuka
        Width = 245
        Height = 23
        ExplicitWidth = 245
        ExplicitHeight = 23
      end
      inherited PnTitulos: TPanel
        Width = 270
        ExplicitWidth = 270
        inherited lbNomePesquisa: TLabel
          Width = 39
          Caption = 'Cliente'
          ExplicitWidth = 39
          ExplicitHeight = 14
        end
        inherited pnSuprimir: TPanel
          Left = 165
          ExplicitLeft = 165
          inherited ckSuprimir: TCheckBox
            Visible = False
          end
        end
      end
      inherited pnPesquisa: TPanel
        Left = 245
        Height = 24
        ExplicitLeft = 245
        ExplicitHeight = 24
      end
    end
    inline FrDataCadastro: TFrDataInicialFinal
      Left = 519
      Top = 44
      Width = 201
      Height = 39
      Color = clWhite
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      TabStop = True
      ExplicitLeft = 519
      ExplicitTop = 44
      ExplicitWidth = 201
      ExplicitHeight = 39
      inherited Label1: TLabel
        Width = 93
        Height = 14
        ExplicitWidth = 93
        ExplicitHeight = 14
      end
      inherited lb1: TLabel
        Width = 18
        Height = 14
        ExplicitWidth = 18
        ExplicitHeight = 14
      end
      inherited eDataFinal: TEditLukaData
        Height = 22
        ExplicitHeight = 22
      end
      inherited eDataInicial: TEditLukaData
        Height = 22
        ExplicitHeight = 22
      end
    end
    inline FrVendedor: TFrVendedores
      Left = 207
      Top = 1
      Width = 237
      Height = 40
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      Color = clWhite
      ParentBackground = False
      ParentColor = False
      TabOrder = 2
      TabStop = True
      ExplicitLeft = 207
      ExplicitTop = 1
      ExplicitWidth = 237
      ExplicitHeight = 40
      inherited sgPesquisa: TGridLuka
        Width = 212
        Height = 23
        ExplicitWidth = 212
        ExplicitHeight = 23
      end
      inherited PnTitulos: TPanel
        Width = 237
        ExplicitWidth = 237
        inherited lbNomePesquisa: TLabel
          Width = 52
          Caption = 'Vendedor'
          ExplicitWidth = 52
          ExplicitHeight = 14
        end
        inherited pnSuprimir: TPanel
          Left = 132
          ExplicitLeft = 132
          inherited ckSuprimir: TCheckBox
            Visible = False
          end
        end
      end
      inherited pnPesquisa: TPanel
        Left = 212
        Height = 24
        ExplicitLeft = 212
        ExplicitHeight = 24
        inherited sbPesquisa: TSpeedButton
          ExplicitLeft = 0
        end
      end
    end
    inline FrEmpresa: TFrEmpresas
      Left = 3
      Top = 1
      Width = 202
      Height = 40
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      Color = clWhite
      ParentBackground = False
      ParentColor = False
      TabOrder = 3
      TabStop = True
      ExplicitLeft = 3
      ExplicitTop = 1
      ExplicitWidth = 202
      ExplicitHeight = 40
      inherited sgPesquisa: TGridLuka
        Width = 177
        Height = 23
        ExplicitWidth = 177
        ExplicitHeight = 23
      end
      inherited PnTitulos: TPanel
        Width = 202
        ExplicitWidth = 202
        inherited lbNomePesquisa: TLabel
          Width = 47
          Caption = 'Empresa'
          ExplicitWidth = 47
          ExplicitHeight = 14
        end
        inherited pnSuprimir: TPanel
          Left = 97
          ExplicitLeft = 97
          inherited ckSuprimir: TCheckBox
            Visible = False
          end
        end
      end
      inherited pnPesquisa: TPanel
        Left = 177
        Height = 24
        ExplicitLeft = 177
        ExplicitHeight = 24
      end
    end
    object eNomeCliente: TEditLuka
      Left = 279
      Top = 59
      Width = 236
      Height = 22
      CharCase = ecUpperCase
      TabOrder = 4
      OnKeyDown = ProximoCampo
      TipoCampo = tcTexto
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 0
      NaoAceitarEspaco = False
    end
    inline FrProduto: TFrProdutos
      Left = 447
      Top = 1
      Width = 266
      Height = 41
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      Color = clWhite
      ParentBackground = False
      ParentColor = False
      TabOrder = 5
      TabStop = True
      ExplicitLeft = 447
      ExplicitTop = 1
      ExplicitWidth = 266
      ExplicitHeight = 41
      inherited sgPesquisa: TGridLuka
        Width = 241
        Height = 24
        ExplicitWidth = 241
        ExplicitHeight = 24
      end
      inherited PnTitulos: TPanel
        Width = 266
        ExplicitWidth = 266
        inherited lbNomePesquisa: TLabel
          Width = 42
          Caption = 'Produto'
          ExplicitWidth = 42
          ExplicitHeight = 14
        end
        inherited pnSuprimir: TPanel
          Left = 161
          ExplicitLeft = 161
          inherited ckSuprimir: TCheckBox
            Visible = False
          end
        end
      end
      inherited pnPesquisa: TPanel
        Left = 241
        Height = 25
        ExplicitLeft = 241
        ExplicitHeight = 25
      end
    end
    object ckxPedidoWeb: TCheckBoxLuka
      Left = 3
      Top = 97
      Width = 97
      Height = 17
      Caption = 'Pedidos Web'
      TabOrder = 6
      CheckedStr = 'N'
      Valor = 'N'
    end
  end
end
