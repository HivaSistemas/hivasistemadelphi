unit ListaContagem;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl,
  Vcl.ComCtrls, Vcl.StdCtrls, CheckBoxLuka, Vcl.Buttons, Vcl.ExtCtrls,
  Vcl.Grids, GridLuka, ComboBoxLuka, FrameDataInicialFinal, FrameMarcas,
  Frame.DepartamentosSecoesLinhas, _FrameHerancaPrincipal,
  _FrameHenrancaPesquisas, FrameProdutos, _ListaContagem, _Biblioteca, _Sessao,
  ImpressaoListaContagemEstoque, FrameLocais, FrameEmpresas, RadioGroupLuka,
  Frame.EnderecoEstoque, Frame.EnderecoEstoqueVao, Frame.EnderecoEstoqueNivel,
  Frame.EnderecoEstoqueModulo, Frame.EnderecoEstoqueRua, FrameFaixaNumeros,
  Data.DB, Datasnap.DBClient, frxClass, frxDBSet;

type
  TFormListaContagem = class(TFormHerancaRelatoriosPageControl)
    sgItens: TGridLuka;
    FrProdutos: TFrProdutos;
    FrDepartamentosSecoesLinhas: TFrameDepartamentosSecoesLinhas;
    FrMarcas: TFrMarcas;
    FrDataCadastroProduto: TFrDataInicialFinal;
    lbTipoControleEstoque: TLabel;
    cbTipoControleEstoque: TComboBoxLuka;
    cbAtivo: TComboBoxLuka;
    lbAtivo: TLabel;
    FrLocais: TFrLocais;
    FrEmpresas: TFrEmpresas;
    cbSituacaoDoEstoque: TComboBoxLuka;
    lb3: TLabel;
    pnEnderecos: TPanel;
    FrEnderecoEstoque: TFrEnderecoEstoque;
    FrEnderecoEstoqueRua: TFrEnderecoEstoqueRua;
    FrEnderecoEstoqueModulo: TFrEnderecoEstoqueModulo;
    FrEnderecoEstoqueNivel: TFrEnderecoEstoqueNivel;
    FrEnderecoEstoqueVao: TFrEnderecoEstoqueVao;
    Label1: TLabel;
    cbQuantidadeEstoque: TComboBoxLuka;
    pnQuantidadeEstoque: TPanel;
    eValor1: TEdit;
    lb1: TLabel;
    lb2: TLabel;
    eValor2: TEdit;
    Label2: TLabel;
    cbAgruparLocais: TComboBoxLuka;
    FrDataAjuste: TFrDataInicialFinal;
    cbAgrupamento: TComboBoxLuka;
    Label3: TLabel;
    frxReport: TfrxReport;
    dstPagar: TfrxDBDataset;
    cdsPagar: TClientDataSet;
    cdsProduto: TStringField;
    cdsNome: TStringField;
    cdsCodigoBarras: TStringField;
    cdsMarca: TStringField;
    cdsUnidade: TStringField;
    cdsQuantidadeFisico: TFloatField;
    cdsCodigoOriginal: TStringField;
    cdsLote: TStringField;
    cdsQuantidadeContada: TStringField;
    cdsAgrupador: TStringField;
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure sbImprimirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbQuantidadeEstoqueChange(Sender: TObject);
    procedure eValor1KeyPress(Sender: TObject; var Key: Char);
    procedure eValor2KeyPress(Sender: TObject; var Key: Char);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    vEstoques: TArray<RecRelacaoEstoque>;
  protected
    procedure Carregar(Sender: TObject); override;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses _AjustesEstoque;

const
  coProdutoId        = 0;
  coNome             = 1;
  coUnidade          = 2;
  coMarca            = 3;
  coLote             = 4;
  coLocal            = 5;

  coCodigoBarras     = 6;
  coQuantidadeFisico = 7;
  coCodigoOriginal   = 8;

  coTipoLinha        = 9;

  coLinhaDetalhe   = 'D';
  coLinhaCabAgrup  = 'CAGRU';

{ TFormHerancaRelatoriosPageControl1 }

procedure TFormListaContagem.Carregar(Sender: TObject);
var
  i: Integer;
  j: Integer;
  vLinha: Integer;
  vAgrupadorId: Integer;
  vSql: string;
  vSqlEnderecoEstoque: string;
  vEstoquesTotal: TArray<RecRelacaoEstoque>;
  vEstoqueAjuste: TArray<RecProdutosZerarEstoque>;
  vSqlAjustes: string;
  vAdicionarProduto: Boolean;
begin
  inherited;
  vSql := '';
  sgItens.ClearGrid;

  if (FrDataAjuste.DatasValidas) and (cbAgruparLocais.GetValor = 'S') then begin
    Exclamar('O filtro "Produtos que n�o tiveram corre��es" n�o pode ser aplicado junto ao filtro "Agrupar locais"');
    SetarFoco(cbAgruparLocais);
    Abort;
  end;

  if not FrProdutos.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrProdutos.getSqlFiltros('PRO.PRODUTO_ID'));

  if not FrDepartamentosSecoesLinhas.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrDepartamentosSecoesLinhas.getSqlFiltros('PRO.LINHA_PRODUTO_ID'));

  if not FrLocais.EstaVazio then
   _Biblioteca.WhereOuAnd(vSql, FrLocais.getSqlFiltros('DIV.LOCAL_ID'));

  if not FrEmpresas.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, 'EST.EMPRESA_ID in (select EMPRESA_ID from ESTOQUES_DIVISAO where '+ FrEmpresas.getSqlFiltros('EMPRESA_ID') + ')');


  if not FrDataCadastroProduto.NenhumaDataValida then
    _Biblioteca.WhereOuAnd(vSql, FrDataCadastroProduto.getSqlFiltros('PRO.DATA_CADASTRO'));

  if not FrMarcas.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrMarcas.getSqlFiltros('PRO.MARCA_ID'));

  if cbTipoControleEstoque.GetValor <> 'NaoFiltrar' then
    _Biblioteca.WhereOuAnd(vSql, 'PRO.TIPO_CONTROLE_ESTOQUE = ''' + cbTipoControleEstoque.GetValor + ''' ');

  if cbAtivo.GetValor <> 'NaoFiltrar' then
    _Biblioteca.WhereOuAnd(vSql, 'PRO.ATIVO = ''' + cbAtivo.GetValor + ''' ');

  if FrEmpresas.EstaVazio then begin
    Exclamar('A empresa n�o foi definida!Por favor verifique!');
    SetarFoco(FrEmpresas);
    Abort;
  end;

  if FrLocais.EstaVazio then begin
    Exclamar('N�o foi definido um local de produtos!Por favor verifique!');
    SetarFoco(FrLocais);
    Abort;
  end;

  vSqlEnderecoEstoque := '';

  if not frEnderecoEstoque.EstaVazio then begin
    vSqlEnderecoEstoque :=
      ' inner join ENDERECOS_ESTOQUE_PRODUTO EPR ' +
      ' on EPR.PRODUTO_ID = PRO.PRODUTO_ID ' +
      ' and ' + FrEnderecoEstoque.getSqlFiltros('EPR.ENDERECO_ID');
  end
  else if (not FrEnderecoEstoqueRua.EstaVazio) or (not FrEnderecoEstoqueModulo.EstaVazio) or (not FrEnderecoEstoqueNivel.EstaVazio) or (not FrEnderecoEstoqueVao.EstaVazio) then begin
    vSqlEnderecoEstoque :=
      ' inner join ENDERECOS_ESTOQUE_PRODUTO EPR ' +
      ' on EPR.PRODUTO_ID = PRO.PRODUTO_ID ' +
      ' and EPR.ENDERECO_ID in ( ' +
      '   select ' +
      '     ENDERECO_ID ' +
      '   from ENDERECO_ESTOQUE EES ' +
      '   where 1 = 1 ';

    if not FrEnderecoEstoqueRua.EstaVazio then
      vSqlEnderecoEstoque := vSqlEnderecoEstoque + ' and ' + FrEnderecoEstoqueRua.getSqlFiltros('EES.RUA_ID');

    if not FrEnderecoEstoqueModulo.EstaVazio then
      vSqlEnderecoEstoque := vSqlEnderecoEstoque + ' and ' + FrEnderecoEstoqueModulo.getSqlFiltros('EES.MODULO_ID');

    if not FrEnderecoEstoqueNivel.EstaVazio then
      vSqlEnderecoEstoque := vSqlEnderecoEstoque + ' and ' + FrEnderecoEstoqueNivel.getSqlFiltros('EES.NIVEL_ID');

    if not FrEnderecoEstoqueVao.EstaVazio then
      vSqlEnderecoEstoque := vSqlEnderecoEstoque + ' and ' + FrEnderecoEstoqueVao.getSqlFiltros('EES.VAO_ID');

    vSqlEnderecoEstoque := vSqlEnderecoEstoque + ') ';
  end;

//      vColunaEstoque :=
//    _Biblioteca.Decode(
//      rgTipoEstoque.GetValor, [
//      'FIS', 'DIV.FISICO',
//      'BLO', 'DIV.BLOQUEADO'
//    ]);
  if cbSituacaoDoEstoque.GetValor <> 'NaoFiltrar' then begin
    if cbSituacaoDoEstoque.GetValor = 'POS' then
      vSql := vSql + ' and ' + 'DIV.FISICO' + ' > 0 '
    else if cbSituacaoDoEstoque.GetValor = 'NEG' then
      vSql := vSql + ' and ' + 'DIV.FISICO' + ' < 0 '
    else
      vSql := vSql + ' and ' + 'DIV.FISICO' + ' = 0 ';
  end;

  if cbQuantidadeEstoque.GetValor <> 'NaoFiltrar' then begin
    if cbQuantidadeEstoque.GetValor = 'EN' then begin
      vSql := vSql + ' and ' + 'DIV.FISICO' + ' > ' + eValor1.Text + ' and DIV.FISICO < ' + eValor2.Text + ' ';
    end
    else if cbQuantidadeEstoque.GetValor = 'MA' then begin
      vSql := vSql + ' and ' + 'DIV.FISICO' + ' > ' + eValor1.Text + ' ';
    end
    else if cbQuantidadeEstoque.GetValor = 'ME' then begin
      vSql := vSql + ' and ' + 'DIV.FISICO' + ' < ' + eValor1.Text + ' ';
    end;
  end;

  if cbAgruparLocais.GetValor = 'N' then begin
    if cbAgrupamento.GetValor = 'MAR' then
      vSql := vSql + ' order by MAR.NOME ASC, PRO.NOME ASC '
    else
      vSql := vSql + ' order by PRO.NOME ';
  end;

  vEstoquesTotal := _ListaContagem.BuscarEstoque(
    Sessao.getConexaoBanco,
    vSql,
    vSqlEnderecoEstoque,
    cbAgruparLocais.GetValor = 'S',
    cbAgrupamento.GetValor = 'MAR',
    IIfStr(cbAgrupamento.GetValor = 'NEN', ' order by PRO.NOME ', ' order by MAR.NOME ASC, PRO.NOME ASC ')
  );
  if vEstoquesTotal = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  if FrDataAjuste.DatasValidas then begin
    vSqlAjustes := ' and ' + FrDataAjuste.getSqlFiltros('AES.DATA_HORA_AJUSTE');

    if not FrEmpresas.EstaVazio then
      vSqlAjustes := vSqlAjustes + ' and ' + FrEmpresas.getSqlFiltros('AES.EMPRESA_ID') + ' ';

    if not FrLocais.EstaVazio then
      vSqlAjustes := vSqlAjustes + ' and ' + FrLocais.getSqlFiltros('AEI.LOCAL_ID') + ' ';

    if not FrProdutos.EstaVazio then
      vSqlAjustes := vSqlAjustes + ' and ' + FrProdutos.getSqlFiltros('AEI.PRODUTO_ID') + ' ';

    vEstoqueAjuste := _AjustesEstoque.BuscarProdutosComAjuste(Sessao.getConexaoBanco, vSqlAjustes);
  end;

  if vEstoqueAjuste = nil then begin
    vEstoques := vEstoquesTotal;
  end
  else begin
    for i := Low(vEstoquesTotal) to High(vEstoquesTotal) do begin

      vAdicionarProduto := true;
      for j := Low(vEstoqueAjuste) to High(vEstoqueAjuste) do begin
        if
          (vEstoqueAjuste[j].EmpresaId = vEstoquesTotal[i].EmpresaId) and
          (vEstoqueAjuste[j].ProdutoId = vEstoquesTotal[i].ProdutoId) and
          (vEstoqueAjuste[j].Lote = vEstoquesTotal[i].Lote) and
          (vEstoqueAjuste[j].LocalId = vEstoquesTotal[i].LocalId)
        then begin
          vAdicionarProduto := false;
          Break;
        end;
      end;

      if vAdicionarProduto then begin
        SetLength(vEstoques, Length(vEstoques) + 1);
        vEstoques[High(vEstoques)] := vEstoquesTotal[i];
      end;
    end;
  end;

  vLinha := 0;
  vAgrupadorId := -1;
  for i := Low(vEstoques) to High(vEstoques) do begin
    if cbAgrupamento.GetValor = 'MAR' then begin
      if vAgrupadorId <> vEstoques[i].MarcaId then begin
        Inc(vLinha);
        sgItens.Cells[coNome, vLinha]        := vEstoques[i].NomeMarca;
        sgItens.Cells[coTipoLinha, vLinha]   := coLinhaCabAgrup;

        vAgrupadorId := vEstoques[i].MarcaId;
      end;
    end;

    Inc(vLinha);
    sgItens.Cells[coProdutoId, vLinha]       := NFormat(vEstoques[i].ProdutoId);
    sgItens.Cells[coNome, vLinha]            := vEstoques[i].NomeProduto;
    sgItens.Cells[coUnidade, vLinha]         := vEstoques[i].UnidadeVenda;
    sgItens.Cells[coMarca, vLinha]           := NFormatN(vEstoques[i].MarcaId) + ' - ' + vEstoques[i].NomeMarca;
    sgItens.Cells[coLote, vLinha]            := vEstoques[i].Lote;
    sgItens.Cells[coLocal, vLinha]           := NFormatN(vEstoques[i].LocalId);

    // ocultas
    sgItens.Cells[coCodigoBarras, vLinha] := vEstoques[i].CodigoBarras;
    sgItens.Cells[coQuantidadeFisico, vLinha] := vEstoques[i].EstoqueFisico;
    sgItens.Cells[coCodigoOriginal, vLinha] := vEstoques[i].CodigoOriginal;

    sgItens.Cells[coTipoLinha, vLinha]       := coLinhaDetalhe;
  end;

  sgItens.SetLinhasGridPorTamanhoVetor( vLinha );
  SetarFoco(sgItens)
end;

procedure TFormListaContagem.cbQuantidadeEstoqueChange(Sender: TObject);
begin
  inherited;
  pnQuantidadeEstoque.Visible := cbQuantidadeEstoque.GetValor <> 'NaoFiltrar';

  if cbQuantidadeEstoque.GetValor = 'EN' then begin
    lb1.Caption := 'Valor entre';
    eValor2.Visible := True;
    lb2.Visible := True;
  end
  else if cbQuantidadeEstoque.GetValor = 'MA' then begin
    lb1.Caption := 'Valor maior que';
    eValor2.Visible := False;
    lb2.Visible := False;
  end
  else if cbQuantidadeEstoque.GetValor = 'ME' then begin
    lb1.Caption := 'Valor menor que';
    eValor2.Visible := False;
    lb2.Visible := False;
  end;
end;

procedure TFormListaContagem.eValor1KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if not (Key in ['0'..'9', Chr(8), '-']) then
    Key := #0;

  if (Key = '-') and ((Pos('-', eValor1.Text) > 0)) then
    Key := #0;
end;

procedure TFormListaContagem.eValor2KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if not (Key in ['0'..'9', Chr(8), '-']) then
    Key := #0;

  if (Key = '-') and  ((Pos('-', eValor2.Text) > 0)) then
    Key := #0;
end;

procedure TFormListaContagem.FormCreate(Sender: TObject);
begin
  inherited;
  pnEnderecos.Visible := Sessao.getParametros.UtilEnderecoEstoque = 'S';
end;

procedure TFormListaContagem.sbImprimirClick(Sender: TObject);
var
  vCnt: Integer;
begin
  inherited;
  //
//  ImpressaoListaContagemEstoque.Imprimir(vEstoques);
  if sgItens.IsRowEmpty(1) then
    exit;
  cdsPagar.Close;
  cdsPagar.CreateDataSet;
  cdsPagar.Open;

  for vCnt := sgItens.RowCount - 1 downto 1 do begin
    cdsPagar.Insert;

    if sgItens.Cells[coTipoLinha, vCnt] = coLinhaCabAgrup then begin
      cdsAgrupador.AsString         := sgItens.Cells[coNome, vCnt];

      if vCnt <> sgItens.RowCount - 1 then begin
        cdsPagar.Post;
        cdsPagar.Insert;
      end;
    end
    else begin
      cdsProduto.AsString           := sgItens.Cells[coProdutoId, vCnt];
      cdsNome.AsString              := sgItens.Cells[coNome, vCnt];
      cdsCodigoBarras.AsString      := sgItens.Cells[coCodigoBarras, vCnt];
      cdsMarca.AsString             := sgItens.Cells[coMarca, vCnt];
      cdsUnidade.AsString           := sgItens.Cells[coUnidade, vCnt];
      cdsCodigoOriginal.AsString    := sgItens.Cells[coCodigoOriginal, vCnt];
      cdsLote.AsString              := sgItens.Cells[coLote, vCnt];
      cdsQuantidadeFisico.AsFloat   := SFormatDouble(sgItens.Cells[coQuantidadeFisico, vCnt]);
      cdsQuantidadeContada.AsString := '--------------';
    end;

    cdsPagar.Post;
  end;

  frxReport.ShowReport;
end;

procedure TFormListaContagem.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coNome, coUnidade, coMarca, coLote, coLocal] then
    vAlinhamento := taLeftJustify
  else
    vAlinhamento := taRightJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormListaContagem.sgItensGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgItens.Cells[coTipoLinha, ARow] = coLinhaCabAgrup then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end;
end;

end.
