unit RelacaoEntregasRealizadas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  _HerancaRelatoriosPageControl, _Biblioteca, _Sessao, Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, FrameDataInicialFinal, Frame.Inteiros,
  FrameEmpresas, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameClientes, Vcl.StdCtrls, StaticTextLuka, Vcl.Grids, GridLuka,
  FrameProdutos, CheckBoxLuka, GroupBoxLuka, _RelacaoEntregasRealizadas, Informacoes.Retiradas, Informacoes.Entrega,
  Vcl.Menus, frxClass, FrameLocais, FrameVendedores;

type
  TFormRelacaoRetiradasEntregas = class(TFormHerancaRelatoriosPageControl)
    FrClientes: TFrClientes;
    FrEmpresas: TFrEmpresas;
    FrPedido: TFrameInteiros;
    FrDataCadastro: TFrDataInicialFinal;
    FrRetirada: TFrameInteiros;
    FrEntrega: TFrameInteiros;
    splSeparador: TSplitter;
    sgEntregas: TGridLuka;
    sgItens: TGridLuka;
    StaticTextLuka1: TStaticTextLuka;
    StaticTextLuka2: TStaticTextLuka;
    FrProdutos: TFrProdutos;
    gbTiposEntrega: TGroupBoxLuka;
    ckEntrega: TCheckBox;
    ckRetiradaAto: TCheckBoxLuka;
    ckRetirada: TCheckBoxLuka;
    pmOpcoes: TPopupMenu;
    dsItensEntraga: TfrxUserDataSet;
    dsEntregaRetirada: TfrxUserDataSet;
    frxReport: TfrxReport;
    FrVendedores: TFrVendedores;
    FrLocalProduto: TFrLocais;
    st4: TStaticText;
    stTotalRetirar: TStaticTextLuka;
    StaticText1: TStaticText;
    stTotalEntregar: TStaticTextLuka;
    StaticText2: TStaticText;
    stTotalSemPrevisao: TStaticTextLuka;
    miGerarTransporte: TSpeedButton;
    procedure sgEntregasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgEntregasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgEntregasDblClick(Sender: TObject);
    procedure sgEntregasClick(Sender: TObject);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure dsEntregaRetiradaFirst(Sender: TObject);
    procedure dsEntregaRetiradaGetValue(const VarName: string; var Value: Variant);
    procedure dsEntregaRetiradaNext(Sender: TObject);
    procedure dsItensEntragaCheckEOF(Sender: TObject; var Eof: Boolean);
    procedure dsItensEntragaFirst(Sender: TObject);
    procedure dsItensEntragaGetValue(const VarName: string; var Value: Variant);
    procedure dsItensEntragaNext(Sender: TObject);
    procedure dsItensEntragaPrior(Sender: TObject);
    procedure dsEntregaRetiradaPrior(Sender: TObject);
    procedure miGerarTransporteClick(Sender: TObject);
  private
    FItens: TArray<RecEntregasItens>;
    MasterNo: Integer;
    DetailNo: Integer;
  protected
    procedure Carregar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;

  end;

implementation

uses
  ImpressaoComprovanteEntregaGrafico;

{$R *.dfm}

{ TFormRelacaoRetiradasEntregas }

const
  coCodigo             = 0;
  coTipo               = 1;
  coCliente            = 2;
  coPedido             = 3;
  coVendedor           = 4;
  coPesoTotal          = 5;
  coQtdeProdutos       = 6;
  coEmpresa            = 7;
  coLocal              = 8;
  coDataHoraCadastro   = 9;
  coUsuarioCadastro    = 10;
  coUsuarioConfirmacao = 11;
  coTipoSintetico      = 12;

  (* Itens *)
  ciProduto    = 0;
  ciNome       = 1;
  ciMarca      = 2;
  ciLote       = 3;
  ciQuantidade = 4;
  ciUnidade    = 5;

procedure TFormRelacaoRetiradasEntregas.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vEntregas: TArray<RecEntregasRealizadas>;

  vTipoEntrega: string;
  vIds: TArray<Integer>;
  vTotalRetirar: Integer;
  vTotalEntregar: Integer;
  vTotalSemPrevisao: Integer;

  procedure SqlTipoEntrega(pValor: string; pCheckBox: TCheckBox);
  begin
    if pCheckBox.Checked then
      AddVirgulaSeNecessario(vTipoEntrega, '''' + pValor + '''');
  end;

begin
  sgEntregas.ClearGrid();
  sgItens.ClearGrid();
  FItens := nil;
  vIds := nil;

  vTotalRetirar := 0;
  vTotalEntregar := 0;
  vTotalSemPrevisao := 0;

  if not FrEmpresas.EstaVazio then
    _Biblioteca.WhereOuAnd( vSql, '( ' + FrEmpresas.getSqlFiltros('EMPRESA_ID') + ' or EMPRESA_ID is null) ' );

  if not FrClientes.EstaVazio then
    _Biblioteca.WhereOuAnd( vSql, FrClientes.getSqlFiltros('CLIENTE_ID') );

  if not FrProdutos.EstaVazio then
    _Biblioteca.WhereOuAnd( vSql, '(TIPO_MOVIMENTO, MOVIMENTO_ID) in(select distinct TIPO_MOVIMENTO, MOVIMENTO_ID from VW_ENTREGAS_ITENS where ' + FrProdutos.getSqlFiltros('PRODUTO_ID') + ')' );

  if not FrDataCadastro.EstaVazio then
    _Biblioteca.WhereOuAnd( vSql, FrDataCadastro.getSqlFiltros('trunc(DATA_HORA_CADASTRO)') );

  if not FrRetirada.EstaVazio then
    _Biblioteca.WhereOuAnd( vSql, '(TIPO_MOVIMENTO in(''R'', ''A'') and ' + FrRetirada.getSqlFiltros('MOVIMENTO_ID') + ') ' );

  if not FrEntrega.EstaVazio then
    _Biblioteca.WhereOuAnd( vSql, '(TIPO_MOVIMENTO = ''E'' and ' + FrEntrega.getSqlFiltros('MOVIMENTO_ID') + ') ' );

  if not FrPedido.EstaVazio then
    _Biblioteca.WhereOuAnd( vSql, FrPedido.getSqlFiltros('ORCAMENTO_ID') );

  if not FrVendedores.EstaVazio then
    _Biblioteca.WhereOuAnd( vSql, FrVendedores.getSqlFiltros('VENDEDOR_ID') );

  if not FrLocalProduto.EstaVazio then
    _Biblioteca.WhereOuAnd( vSql, FrLocalProduto.getSqlFiltros('LOCAL_ID') );

  if not gbTiposEntrega.TodosMarcados then begin
    SqlTipoEntrega('A', ckRetiradaAto);
    SqlTipoEntrega('R', ckRetirada);
    SqlTipoEntrega('E', ckEntrega);

    _Biblioteca.WhereOuAnd( vSql, 'TIPO_MOVIMENTO in(' + vTipoEntrega + ') ' );
  end;

  vSql := vSql + ' order by DATA_HORA_CADASTRO ';

  vEntregas := _RelacaoEntregasRealizadas.BuscarEntregas(Sessao.getConexaoBanco, vSql);
  if vEntregas = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  for i := Low(vEntregas) to High(vEntregas) do begin
    sgEntregas.Cells[coCodigo, i + 1]             := _Biblioteca.NFormat(vEntregas[i].MovimentoId);
    sgEntregas.Cells[coTipo, i + 1]               := vEntregas[i].TipoMovimentoAnalitico;
    sgEntregas.Cells[coCliente, i + 1]            := _Biblioteca.NFormat(vEntregas[i].ClienteId) + ' - ' + vEntregas[i].NomeCliente;
    sgEntregas.Cells[coPedido, i + 1]             := _Biblioteca.NFormat(vEntregas[i].OrcamentoId);
    sgEntregas.Cells[coVendedor, i + 1]           := _Biblioteca.NFormat(vEntregas[i].VendedorId) + ' - ' + vEntregas[i].NomeVendedor;
    sgEntregas.Cells[coPesoTotal, i + 1]          := _Biblioteca.NFormatNEstoque(vEntregas[i].PesoTotal);
    sgEntregas.Cells[coQtdeProdutos, i + 1]       := _Biblioteca.NFormatNEstoque(vEntregas[i].QtdeProdutos);
    sgEntregas.Cells[coEmpresa, i + 1]            := _Biblioteca.NFormat(vEntregas[i].EmpresaId) + ' - ' + vEntregas[i].NomeEmpresa;
    sgEntregas.Cells[coLocal, i + 1]              := _Biblioteca.NFormat(vEntregas[i].LocalId) + ' - ' + vEntregas[i].NomeLocal;
    sgEntregas.Cells[coDataHoraCadastro, i + 1]   := _Biblioteca.DHFormatN(vEntregas[i].DataHoraCadastro);
    sgEntregas.Cells[coUsuarioCadastro, i + 1]    := _Biblioteca.NFormat(vEntregas[i].UsuarioCadastroId) + ' - ' + vEntregas[i].NomeUsuarioCadastro;
    sgEntregas.Cells[coUsuarioConfirmacao, i + 1] := _Biblioteca.NFormat(vEntregas[i].UsuarioConfirmacaoId) + ' - ' + vEntregas[i].NomeUsuarioConfirmacao;
    sgEntregas.Cells[coTipoSintetico, i + 1]      := vEntregas[i].TipoMovimento;

    _Biblioteca.AddNoVetorSemRepetir( vIds, vEntregas[i].MovimentoId);

    if vEntregas[i].TipoMovimento = 'E' then
      vTotalEntregar := vTotalEntregar + 1
    else if vEntregas[i].TipoMovimento = 'R' then
      vTotalRetirar := vTotalRetirar + 1
    else
      vTotalSemPrevisao := vTotalSemPrevisao + 1;
  end;
  sgEntregas.SetLinhasGridPorTamanhoVetor( Length(vEntregas) );

  stTotalEntregar.AsInt := vTotalEntregar;
  stTotalRetirar.AsInt := vTotalRetirar;
  stTotalSemPrevisao.AsInt := vTotalSemPrevisao;

  FItens := _RelacaoEntregasRealizadas.BuscarItensEntregas( Sessao.getConexaoBanco, 'where ' + FiltroInInt('MOVIMENTO_ID', vIds) );

  dsEntregaRetirada.RangeEnd := reCount;
  dsEntregaRetirada.RangeEndCount := i;

  dsItensEntraga.RangeEnd := reCount;
  dsItensEntraga.RangeEndCount := High(FItens);


  sgEntregasClick(Sender);
  _Biblioteca.SetarFoco(sgEntregas);
end;

procedure TFormRelacaoRetiradasEntregas.dsEntregaRetiradaFirst(Sender: TObject);
begin
  MasterNo := 1;
end;

procedure TFormRelacaoRetiradasEntregas.dsEntregaRetiradaGetValue(const VarName: string;
  var Value: Variant);
begin
  inherited;
  if VarName = 'pedido' then
    Value := SFormatInt(sgEntregas.Cells[coCodigo ,MasterNo]);
  if VarName = 'tipo' then
    Value := sgEntregas.Cells[coTipo ,MasterNo];
  if VarName = 'cliente' then
    Value := sgEntregas.Cells[coCliente ,MasterNo];
  if VarName = 'DtCadastro' then
    Value := sgEntregas.Cells[coDataHoraCadastro ,MasterNo];

end;

procedure TFormRelacaoRetiradasEntregas.dsEntregaRetiradaNext(Sender: TObject);
begin
  Inc(MasterNo);
end;

procedure TFormRelacaoRetiradasEntregas.dsEntregaRetiradaPrior(Sender: TObject);
begin
  Dec(MasterNo);
end;

procedure TFormRelacaoRetiradasEntregas.dsItensEntragaCheckEOF(Sender: TObject;
  var Eof: Boolean);
var
  vTotalItens : integer;
begin
  vTotalItens := High(FItens);
  if not Eof then
    Eof := DetailNo > vTotalItens;
end;

procedure TFormRelacaoRetiradasEntregas.dsItensEntragaFirst(Sender: TObject);
begin
  DetailNo := 0;
  while (not dsItensEntraga.Eof)
    and ((FItens[DetailNo].MovimentoId <> SFormatInt(sgEntregas.Cells[coCodigo ,MasterNo]))
    or (FItens[DetailNo].TipoMovimento <> sgEntregas.Cells[coTipoSintetico, MasterNo])) do
  begin
    Inc(DetailNo);
  end;
end;

procedure TFormRelacaoRetiradasEntregas.dsItensEntragaGetValue(const VarName: string;
  var Value: Variant);
begin
  inherited;
  if VarName = 'pedido' then
    Value := FItens[DetailNo].MovimentoId;
  if VarName = 'produto' then
    Value := FItens[DetailNo].ProdutoId;
  if VarName = 'nome' then
    Value := FItens[DetailNo].Nome;
  if VarName = 'marca' then
    Value := FItens[DetailNo].NomeMarca;
  if VarName = 'qtde' then
    Value := FItens[DetailNo].Quantidade;
  if VarName = 'unid' then
    Value := FItens[DetailNo].UnidadeVenda;
  if VarName = 'lote' then
    Value := FItens[DetailNo].Lote;
end;

procedure TFormRelacaoRetiradasEntregas.dsItensEntragaNext(Sender: TObject);
begin
  Inc(DetailNo);
  while (not dsItensEntraga.Eof)
    and ((FItens[DetailNo].MovimentoId <> SFormatInt(sgEntregas.Cells[coCodigo ,MasterNo]))
    or (FItens[DetailNo].TipoMovimento <> sgEntregas.Cells[coTipoSintetico, MasterNo])) do
  begin
    Inc(DetailNo);
  end;
end;

procedure TFormRelacaoRetiradasEntregas.dsItensEntragaPrior(Sender: TObject);
begin
  inherited;
  Dec(DetailNo);
  while (not dsItensEntraga.Eof)
    and ((FItens[DetailNo].MovimentoId <> SFormatInt(sgEntregas.Cells[coCodigo ,MasterNo]))
    or (FItens[DetailNo].TipoMovimento <> sgEntregas.Cells[coTipoSintetico, MasterNo])) do
  begin
    Dec(DetailNo);
  end;
end;

procedure TFormRelacaoRetiradasEntregas.Imprimir(Sender: TObject);
begin
  inherited;
  frxReport.ShowReport();
end;

procedure TFormRelacaoRetiradasEntregas.miGerarTransporteClick(Sender: TObject);
begin
  inherited;
  ImpressaoComprovanteEntregaGrafico.Imprimir(
    SFormatInt(sgEntregas.Cells[coCodigo, sgEntregas.Row]),
    tpEntrega
  );
end;

procedure TFormRelacaoRetiradasEntregas.sgEntregasClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
begin
  inherited;

  vLinha := 0;
  sgItens.ClearGrid();
  for i := Low(FItens) to High(FItens) do begin
    if
      (FItens[i].TipoMovimento <> sgEntregas.Cells[coTipoSintetico, sgEntregas.Row]) or
      (FItens[i].MovimentoId <> SFormatInt(sgEntregas.Cells[coCodigo, sgEntregas.Row]))
    then
      Continue;

    Inc(vLinha);
    sgItens.Cells[ciProduto, vLinha]    := NFormat(FItens[i].ProdutoId);
    sgItens.Cells[ciNome, vLinha]       := FItens[i].Nome;
    sgItens.Cells[ciMarca, vLinha]      := NFormat(FItens[i].MarcaId) + ' - ' + FItens[i].NomeMarca;
    sgItens.Cells[ciLote, vLinha]       := FItens[i].Lote;
    sgItens.Cells[ciQuantidade, vLinha] := NFormatEstoque(FItens[i].Quantidade);
    sgItens.Cells[ciUnidade, vLinha]    := FItens[i].UnidadeVenda;
  end;
  sgItens.SetLinhasGridPorTamanhoVetor( vLinha );
end;

procedure TFormRelacaoRetiradasEntregas.sgEntregasDblClick(Sender: TObject);
begin
  inherited;
  if sgEntregas.Cells[coTipo, sgEntregas.Row] = 'Entregar' then
    Informacoes.Entrega.Informar( SFormatInt(sgEntregas.Cells[coCodigo, sgEntregas.Row]) )
  else
    Informacoes.Retiradas.Informar( SFormatInt(sgEntregas.Cells[coCodigo, sgEntregas.Row]) );
end;

procedure TFormRelacaoRetiradasEntregas.sgEntregasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coCodigo, coPedido, coPesoTotal, coQtdeProdutos] then
    vAlinhamento := taRightJustify
  else if ACol = coTipo then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgEntregas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoRetiradasEntregas.sgEntregasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coTipo then begin
    AFont.Style := [fsBold];

    if sgEntregas.Cells[coTipo, ARow] = 'Retirar' then
      AFont.Color := clBlue
    else if sgEntregas.Cells[coTipo, ARow] = 'Entregar' then
      AFont.Color := $000096DB
    else
      AFont.Color := clGreen;
  end;
end;

procedure TFormRelacaoRetiradasEntregas.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[ciProduto, ciQuantidade] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoRetiradasEntregas.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if gbTiposEntrega.NenhumMarcado then begin
    _Biblioteca.Exclamar('� necess�rio definir ao menos um tipo de entrega como filtro!');
    SetarFoco(ckRetiradaAto);
    Abort;
  end;
end;

end.
