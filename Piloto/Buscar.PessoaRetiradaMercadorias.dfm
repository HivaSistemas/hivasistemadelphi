inherited FormBuscarPessoaRetiradaMercadorias: TFormBuscarPessoaRetiradaMercadorias
  Caption = 'Buscar pessoa respons'#225'vel por retirada'
  ClientHeight = 200
  ClientWidth = 495
  ExplicitWidth = 501
  ExplicitHeight = 229
  PixelsPerInch = 96
  TextHeight = 14
  object lblCPF_CNPJ: TLabel [0]
    Left = 229
    Top = 3
    Width = 18
    Height = 14
    Caption = 'CPF'
  end
  object lbl1: TLabel [1]
    Left = 3
    Top = 3
    Width = 32
    Height = 14
    Caption = 'Nome'
  end
  object lbllb12: TLabel [2]
    Left = 380
    Top = 3
    Width = 108
    Height = 14
    Caption = 'Data / hora retirada'
  end
  object lbllb13: TLabel [3]
    Left = 3
    Top = 47
    Width = 65
    Height = 14
    Caption = 'Obseva'#231#245'es'
  end
  inherited pnOpcoes: TPanel
    Top = 163
    Width = 495
    TabOrder = 5
    ExplicitTop = 163
    ExplicitWidth = 495
  end
  object eNome: TEditLuka
    Left = 3
    Top = 18
    Width = 216
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 0
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
  end
  object eCPF: TEditCPF_CNPJ_Luka
    Left = 229
    Top = 18
    Width = 144
    Height = 22
    EditMask = '999.999.999-99'
    MaxLength = 14
    TabOrder = 1
    Text = '   .   .   -  '
    OnKeyDown = ProximoCampo
    Tipo = [tccCPF]
  end
  object eDataRetirada: TEditLukaData
    Left = 380
    Top = 18
    Width = 72
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 2
    Text = '  /  /    '
    OnKeyDown = ProximoCampo
  end
  object eHoraRetirada: TEditHoras
    Left = 451
    Top = 18
    Width = 41
    Height = 22
    EditMask = '!90:00;1;_'
    MaxLength = 5
    TabOrder = 3
    Text = '  :  '
    OnKeyDown = ProximoCampo
  end
  object meObservacoes: TMemo
    Left = 3
    Top = 62
    Width = 489
    Height = 99
    TabOrder = 4
    OnKeyDown = ProximoCampo
  end
end
