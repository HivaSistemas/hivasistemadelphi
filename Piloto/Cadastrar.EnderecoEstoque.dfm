inherited FormEnderecoEstoque: TFormEnderecoEstoque
  Caption = 'Cadastro Endere'#231'o de Estoque'
  ClientHeight = 276
  ClientWidth = 486
  ExplicitWidth = 492
  ExplicitHeight = 305
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 276
    ExplicitHeight = 276
  end
  inline FrRua: TFrEnderecoEstoqueRua
    Left = 129
    Top = 48
    Width = 345
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 129
    ExplicitTop = 48
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Height = 23
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      inherited lbNomePesquisa: TLabel
        Width = 21
        ExplicitWidth = 21
        ExplicitHeight = 14
      end
      inherited CkChaveUnica: TCheckBox
        Left = 152
        ExplicitLeft = 152
      end
      inherited pnSuprimir: TPanel
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Height = 24
      ExplicitHeight = 24
    end
  end
  inline FrModulo: TFrEnderecoEstoqueModulo
    Left = 129
    Top = 96
    Width = 345
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 4
    TabStop = True
    ExplicitLeft = 129
    ExplicitTop = 96
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Height = 23
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      inherited lbNomePesquisa: TLabel
        Width = 42
        ExplicitWidth = 42
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Height = 24
      ExplicitHeight = 24
    end
  end
  inline FrNivel: TFrEnderecoEstoqueNivel
    Left = 129
    Top = 144
    Width = 345
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 5
    TabStop = True
    ExplicitLeft = 129
    ExplicitTop = 144
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Height = 23
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      inherited lbNomePesquisa: TLabel
        Width = 27
        ExplicitWidth = 27
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Height = 24
      ExplicitHeight = 24
    end
  end
  inline FrVao: TFrEnderecoEstoqueVao
    Left = 129
    Top = 192
    Width = 345
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 6
    TabStop = True
    ExplicitLeft = 129
    ExplicitTop = 192
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Height = 23
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      inherited lbNomePesquisa: TLabel
        Width = 20
        ExplicitWidth = 20
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Height = 24
      ExplicitHeight = 24
    end
  end
end
