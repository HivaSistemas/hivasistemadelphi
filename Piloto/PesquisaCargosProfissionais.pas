unit PesquisaCargosProfissionais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, Vcl.ExtCtrls;

type
  TFormPesquisaCargosProfissionais = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  protected
    procedure BuscarRegistros; override;
  end;

function PesquisarCargoProfissional(pSomenteAtivos: Boolean): TObject;

implementation

{$R *.dfm}

uses _CargosProfissionais, _Biblioteca, _Sessao;

const
  coNome   = 2;
  coAtivo  = 3;

function PesquisarCargoProfissional(pSomenteAtivos: Boolean): TObject;
var
  vObj: TObject;
begin
  vObj := _HerancaPesquisas.Pesquisar(TFormPesquisaCargosProfissionais, _CargosProfissionais.GetFiltros, [pSomenteAtivos]);
  if vObj = nil then
    Result := nil
  else
    Result := RecCargosProfissionais(vObj);
end;

procedure TFormPesquisaCargosProfissionais.BuscarRegistros;
var
  i: Integer;
  vCargosProfissionais: TArray<RecCargosProfissionais>;
begin
  inherited;

  vCargosProfissionais :=
    _CargosProfissionais.BuscarCargosProfissionais(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text],
      FAuxiliares[0]
    );

  if vCargosProfissionais = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vCargosProfissionais);

  for i := Low(vCargosProfissionais) to High(vCargosProfissionais) do begin
    sgPesquisa.Cells[coSelecionado, i + 1]  := _Biblioteca.charNaoSelecionado;
    sgPesquisa.Cells[coCodigo, i + 1]       := NFormat(vCargosProfissionais[i].CargoId);
    sgPesquisa.Cells[coNome, i + 1]         := vCargosProfissionais[i].Nome;
    sgPesquisa.Cells[coAtivo, i + 1]        := vCargosProfissionais[i].Ativo;
  end;

  sgPesquisa.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vCargosProfissionais) );
  SetarFoco(sgPesquisa);
end;

procedure TFormPesquisaCargosProfissionais.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coSelecionado, coAtivo] then
    vAlinhamento := taCenter
  else if ACol = coCodigo then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
