inherited FormImpressaoReciboPagamentoContasPagar: TFormImpressaoReciboPagamentoContasPagar
  Caption = 'FormImpressaoReciboPagamentoContasPagar'
  ExplicitTop = -279
  ExplicitWidth = 901
  ExplicitHeight = 754
  PixelsPerInch = 96
  TextHeight = 13
  inherited qrRelatorioNF: TQuickRep
    Left = 623
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Page.Values = (
      0.000000000000000000
      509.300000000000000000
      0.000000000000000000
      750.100000000000000000
      0.000000000000000000
      0.000000000000000000
      0.000000000000000000)
    ExplicitLeft = 623
    inherited qrBandTitulo: TQRBand
      Size.Values = (
        289.388020833333300000
        750.755208333333300000)
      inherited qrlNFNomeEmpresa: TQRLabel
        Size.Values = (
          34.726562500000000000
          6.614583333333332000
          16.536458333333330000
          744.140625000000000000)
        FontSize = 6
      end
      inherited qrNFCnpj: TQRLabel
        Size.Values = (
          33.072916666666670000
          6.614583333333332000
          51.263020833333340000
          744.140625000000000000)
        FontSize = 7
      end
      inherited qrNFEndereco: TQRLabel
        Size.Values = (
          34.726562500000000000
          6.614583333333332000
          85.989583333333340000
          744.140625000000000000)
        FontSize = 7
      end
      inherited qrNFCidadeUf: TQRLabel
        Size.Values = (
          42.994791666666670000
          6.614583333333332000
          122.369791666666700000
          744.140625000000000000)
        FontSize = 7
      end
      inherited qrNFTipoDocumento: TQRLabel
        Size.Values = (
          33.072916666666670000
          1.653645833333333000
          211.666666666666700000
          719.335937500000000000)
        FontSize = 7
      end
      inherited qrSeparador2: TQRShape
        Size.Values = (
          3.307291666666666000
          3.307291666666666000
          281.119791666666700000
          724.296875000000000000)
      end
      inherited qrNFTitulo: TQRLabel
        Size.Values = (
          33.072916666666670000
          1.653645833333333000
          246.393229166666700000
          719.335937500000000000)
        FontSize = 7
      end
      inherited qrlNFEmitidoEm: TQRLabel
        Size.Values = (
          23.151041666666670000
          370.416666666666700000
          3.307291666666666000
          357.187500000000000000)
        FontSize = -6
      end
      inherited qrlNFTelefoneEmpresa: TQRLabel
        Size.Values = (
          42.994791666666670000
          6.614583333333332000
          167.018229166666700000
          744.140625000000000000)
        FontSize = 7
      end
    end
    inherited PageFooterBand1: TQRBand
      Size.Values = (
        24.804687500000000000
        750.755208333333300000)
      inherited qrlNFSistema: TQRLabel
        Size.Values = (
          23.151041666666670000
          582.083333333333400000
          0.000000000000000000
          138.906250000000000000)
        FontSize = -6
      end
    end
  end
  inherited qrRelatorioA4: TQuickRep
    Left = 64
    Top = 79
    BeforePrint = qrRelatorioA4BeforePrint
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    OnNeedData = qrRelatorioA4NeedData
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    ExplicitLeft = 64
    ExplicitTop = 79
    inherited qrCabecalho: TQRBand
      Height = 163
      Size.Values = (
        431.270833333333300000
        1899.708333333333000000)
      ExplicitHeight = 163
      inherited qrCaptionEndereco: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          44.979166666666670000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrEmpresa: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          7.937500000000000000
          568.854166666666700000)
        FontSize = 7
      end
      inherited qrEndereco: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          44.979166666666670000
          1513.416666666667000000)
        FontSize = 7
      end
      inherited qrLogoEmpresa: TQRImage
        Size.Values = (
          211.666666666666700000
          13.229166666666670000
          2.645833333333333000
          211.666666666666700000)
      end
      inherited qrCaptionEmpresa: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          7.937500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrEmitidoEm: TQRLabel
        Size.Values = (
          29.104166666666670000
          1529.291666666667000000
          0.000000000000000000
          370.416666666666700000)
        FontSize = 5
      end
      inherited qr1: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          82.020833333333320000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrBairro: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          82.020833333333320000
          568.854166666666700000)
        FontSize = 7
      end
      inherited qr3: TQRLabel
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          82.020833333333320000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrCidadeUf: TQRLabel
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          82.020833333333320000
          568.854166666666700000)
        FontSize = 7
      end
      inherited qr5: TQRLabel
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          7.937500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrCNPJ: TQRLabel
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          7.937500000000000000
          248.708333333333300000)
        FontSize = 7
      end
      inherited qr7: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrTelefone: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          119.062500000000000000
          187.854166666666700000)
        FontSize = 7
      end
      inherited qrFax: TQRLabel
        Size.Values = (
          34.395833333333330000
          767.291666666666800000
          119.062500000000000000
          187.854166666666700000)
        FontSize = 7
      end
      inherited qr10: TQRLabel
        Size.Values = (
          34.395833333333330000
          603.250000000000000000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qr11: TQRLabel
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrEmail: TQRLabel
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          119.062500000000000000
          769.937500000000000000)
        FontSize = 7
      end
      inherited qr13: TQRLabel
        Size.Values = (
          52.916666666666670000
          711.729166666666700000
          164.041666666666700000
          529.166666666666700000)
        Caption = 'Recibo de pagamento'
        FontSize = 12
      end
      object qrTexto: TQRMemo
        Left = 5
        Top = 87
        Width = 707
        Height = 52
        Size.Values = (
          137.583333333333300000
          13.229166666666670000
          230.187500000000000000
          1870.604166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Lines.Strings = (
          
            'Eu :NOME_BENEFICIARIO, inscrito sob o CPF/CNPJ :CPF_CNPJ_BENEFIC' +
            'IARIO, declaro para qualquer finalidade que recebi da empresa :R' +
            'AZAO_SOCIAL, inscrita sob o CNPJ :CNPJ a import'#226'ncia de R$ :VALO' +
            'R [ :DINHEIRO_POR_EXTENSO ] referentes aos t'#237'tulos descriminados' +
            ' abaixo:')
        ParentFont = False
        Transparent = False
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 8
      end
      object qrlCabProdutoId: TQRLabel
        Left = 3
        Top = 144
        Width = 62
        Height = 15
        Size.Values = (
          39.687500000000000000
          7.937500000000000000
          381.000000000000000000
          164.041666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'C'#243'd.Hiva:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr2: TQRLabel
        Left = 69
        Top = 144
        Width = 127
        Height = 15
        Size.Values = (
          39.687500000000000000
          182.562500000000000000
          381.000000000000000000
          336.020833333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Documento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr6: TQRLabel
        Left = 200
        Top = 144
        Width = 69
        Height = 15
        Size.Values = (
          39.687500000000000000
          529.166666666666800000
          381.000000000000000000
          182.562500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Data vencto.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr9: TQRLabel
        Left = 273
        Top = 144
        Width = 69
        Height = 15
        Size.Values = (
          39.687500000000000000
          722.312500000000000000
          381.000000000000000000
          182.562500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor doc.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr14: TQRLabel
        Left = 346
        Top = 144
        Width = 90
        Height = 15
        Size.Values = (
          39.687500000000000000
          915.458333333333200000
          381.000000000000000000
          238.125000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Val multa+.juros'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr16: TQRLabel
        Left = 440
        Top = 144
        Width = 66
        Height = 15
        Size.Values = (
          39.687500000000000000
          1164.166666666667000000
          381.000000000000000000
          174.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor desc.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr18: TQRLabel
        Left = 512
        Top = 144
        Width = 73
        Height = 15
        Size.Values = (
          39.687500000000000000
          1354.666666666667000000
          381.000000000000000000
          193.145833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor adiant.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr20: TQRLabel
        Left = 592
        Top = 144
        Width = 81
        Height = 15
        Size.Values = (
          39.687500000000000000
          1566.333333333333000000
          381.000000000000000000
          214.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Saldo l'#237'quido'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
    end
    inherited qrbndRodape: TQRBand
      Top = 222
      Height = 189
      Size.Values = (
        500.062500000000000000
        1899.708333333333000000)
      ExplicitTop = 222
      ExplicitHeight = 189
      inherited qrUsuarioImpressao: TQRLabel
        Top = 172
        Size.Values = (
          31.750000000000000000
          7.937500000000000000
          455.083333333333300000
          354.541666666666700000)
        Caption = 'EZEQUIEL EUGENIO DO PRADO'
        FontSize = 7
        ExplicitTop = 172
      end
      inherited qrSistema: TQRLabel
        Top = 172
        Size.Values = (
          31.750000000000000000
          1529.291666666667000000
          455.083333333333300000
          370.416666666666700000)
        Caption = 'hiva 3.0'
        FontSize = 7
        ExplicitTop = 172
      end
      object qrmeFormasPagto: TQRMemo
        Left = 5
        Top = 17
        Width = 271
        Height = 47
        Size.Values = (
          124.354166666666700000
          13.229166666666670000
          44.979166666666670000
          717.020833333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Lines.Strings = (
          'Dinheiro........: R$ 1.000,00'
          'Cobran'#231'a.......: R$ 1.000,00'
          'Cr'#233'dito..........: R$ 1.000,00')
        ParentFont = False
        Transparent = False
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 9
      end
      object qrl18: TQRLabel
        Left = 5
        Top = 1
        Width = 271
        Height = 18
        Size.Values = (
          47.625000000000000000
          13.229166666666670000
          2.645833333333333000
          717.020833333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Formas de pagamento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRShape2: TQRShape
        Left = 24
        Top = 115
        Width = 329
        Height = 11
        Size.Values = (
          29.104166666666670000
          63.500000000000000000
          304.270833333333400000
          870.479166666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object qrEmpresaPagadora: TQRLabel
        Left = 49
        Top = 126
        Width = 277
        Height = 15
        Size.Values = (
          39.687500000000000000
          129.645833333333300000
          333.375000000000000000
          732.895833333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'CONSTRUARTE MATERIAIS DE CONSTRU'#199#195'O LTDA'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrCpnjPagadora: TQRLabel
        Left = 49
        Top = 141
        Width = 277
        Height = 15
        Size.Values = (
          39.687500000000000000
          129.645833333333300000
          373.062500000000000000
          732.895833333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '01.005.001/0001-45'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr23: TQRLabel
        Left = 290
        Top = 32
        Width = 62
        Height = 15
        Size.Values = (
          39.687500000000000000
          767.291666666666800000
          84.666666666666680000
          164.041666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Dt.pagto:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr22: TQRLabel
        Left = 290
        Top = 17
        Width = 62
        Height = 15
        Size.Values = (
          39.687500000000000000
          767.291666666666800000
          44.979166666666670000
          164.041666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'C'#243'd.baixa:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr24: TQRLabel
        Left = 290
        Top = 47
        Width = 62
        Height = 15
        Size.Values = (
          39.687500000000000000
          767.291666666666800000
          124.354166666666700000
          164.041666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Dt.baixa:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrBaixaId: TQRLabel
        Left = 354
        Top = 17
        Width = 62
        Height = 15
        Size.Values = (
          39.687500000000000000
          936.625000000000100000
          44.979166666666670000
          164.041666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '1500'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrDataPagamento: TQRLabel
        Left = 354
        Top = 32
        Width = 62
        Height = 15
        Size.Values = (
          39.687500000000000000
          936.625000000000100000
          84.666666666666680000
          164.041666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '1500'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrDataBaixa: TQRLabel
        Left = 354
        Top = 47
        Width = 62
        Height = 15
        Size.Values = (
          39.687500000000000000
          936.625000000000100000
          124.354166666666700000
          164.041666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '1500'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrCpfCnpjBeneficiario: TQRLabel
        Left = 385
        Top = 141
        Width = 277
        Height = 15
        Size.Values = (
          39.687500000000000000
          1018.645833333333000000
          373.062500000000000000
          732.895833333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '025.905.500-40'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrBeneficiario: TQRLabel
        Left = 360
        Top = 126
        Width = 329
        Height = 15
        Size.Values = (
          39.687500000000000000
          952.500000000000000000
          333.375000000000000000
          870.479166666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'EZEQUIEL EUGENIO DO PRADO SILVA'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRShape1: TQRShape
        Left = 360
        Top = 115
        Width = 329
        Height = 11
        Size.Values = (
          29.104166666666670000
          952.500000000000000000
          304.270833333333400000
          870.479166666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
    end
    object qrbndDetailBand1: TQRBand
      Left = 38
      Top = 201
      Width = 718
      Height = 21
      AlignToBottom = False
      BeforePrint = qrbndDetailBand1BeforePrint
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        55.562500000000000000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbDetail
      object qrPagarId: TQRLabel
        Left = 3
        Top = 1
        Width = 62
        Height = 15
        Size.Values = (
          39.687500000000000000
          7.937500000000000000
          2.645833333333333000
          164.041666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '15.900'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrDocumento: TQRLabel
        Left = 69
        Top = 1
        Width = 127
        Height = 15
        Size.Values = (
          39.687500000000000000
          182.562500000000000000
          2.645833333333333000
          336.020833333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '000000000000000000002'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrDataVencimento: TQRLabel
        Left = 200
        Top = 1
        Width = 69
        Height = 15
        Size.Values = (
          39.687500000000000000
          529.166666666666800000
          2.645833333333333000
          182.562500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '26/10/2018'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrValorDocumento: TQRLabel
        Left = 273
        Top = 1
        Width = 69
        Height = 15
        Size.Values = (
          39.687500000000000000
          722.312500000000000000
          2.645833333333333000
          182.562500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '1500,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrMultaJuros: TQRLabel
        Left = 346
        Top = 0
        Width = 90
        Height = 15
        Size.Values = (
          39.687500000000000000
          915.458333333333200000
          0.000000000000000000
          238.125000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '1500,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrValorDesconto: TQRLabel
        Left = 440
        Top = 1
        Width = 66
        Height = 15
        Size.Values = (
          39.687500000000000000
          1164.166666666667000000
          2.645833333333333000
          174.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '15'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrValorAdiantado: TQRLabel
        Left = 512
        Top = 1
        Width = 73
        Height = 15
        Size.Values = (
          39.687500000000000000
          1354.666666666667000000
          2.645833333333333000
          193.145833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '15,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrSaldoLiquido: TQRLabel
        Left = 592
        Top = 1
        Width = 81
        Height = 15
        Size.Values = (
          39.687500000000000000
          1566.333333333333000000
          2.645833333333333000
          214.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '15,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
    end
  end
end
