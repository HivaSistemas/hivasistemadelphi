inherited FormPesquisaGruposTributacoesEstadualCompra: TFormPesquisaGruposTributacoesEstadualCompra
  Caption = 'FormPesquisaGruposTributacoesEstadualCompra'
  ClientWidth = 500
  ExplicitWidth = 508
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 500
    ColCount = 4
    OnDrawCell = sgPesquisaDrawCell
    CorLinhaFoco = 38619
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Descri'#231#227'o'
      'Ativo?')
    RealColCount = 4
    ExplicitWidth = 500
    ColWidths = (
      28
      55
      328
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Text = '0'
  end
  inherited pnFiltro1: TPanel
    Width = 500
    ExplicitWidth = 500
    inherited lblPesquisa: TLabel
      Width = 167
      Caption = 'Grupo de tributacoes estadual'
      ExplicitWidth = 167
    end
    inherited eValorPesquisa: TEditLuka
      Width = 296
      ExplicitWidth = 296
    end
  end
end
