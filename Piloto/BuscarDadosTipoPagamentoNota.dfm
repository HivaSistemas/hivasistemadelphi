inherited FormBuscarDadosTipoPagamentoNota: TFormBuscarDadosTipoPagamentoNota
  Caption = 'Buscar dados de tipo de pagamento nota'
  ClientHeight = 278
  ClientWidth = 286
  ExplicitWidth = 292
  ExplicitHeight = 307
  PixelsPerInch = 96
  TextHeight = 14
  object lbllb11: TLabel [0]
    Left = 48
    Top = 13
    Width = 60
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Total pago:'
  end
  object Label2: TLabel [1]
    Left = 48
    Top = 41
    Width = 60
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Dinheiro'
  end
  object Label3: TLabel [2]
    Left = 48
    Top = 62
    Width = 60
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Cheque'
  end
  object Label4: TLabel [3]
    Left = 30
    Top = 82
    Width = 78
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Cart'#227'o d'#233'bito'
  end
  object Label5: TLabel [4]
    Left = 30
    Top = 104
    Width = 78
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Cart'#227'o cr'#233'dito'
  end
  object Label6: TLabel [5]
    Left = 48
    Top = 125
    Width = 60
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Cobran'#231'a'
  end
  object Label7: TLabel [6]
    Left = 0
    Top = 146
    Width = 108
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Pix/Transfer'#234'ncia'
  end
  object Label8: TLabel [7]
    Left = 29
    Top = 167
    Width = 79
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Acumulado'
  end
  object Label9: TLabel [8]
    Left = 48
    Top = 187
    Width = 60
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Cr'#233'dito'
  end
  object Label10: TLabel [9]
    Left = 1
    Top = 215
    Width = 107
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Diferen'#231'a'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  inherited pnOpcoes: TPanel
    Top = 241
    Width = 286
    inherited sbFinalizar: TSpeedButton
      Left = 83
      ExplicitLeft = 83
    end
  end
  object eTotalPago: TEditLuka
    Left = 114
    Top = 5
    Width = 97
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 1
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorDinheiro: TEditLuka
    Left = 114
    Top = 33
    Width = 97
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 2
    Text = '0,00'
    OnChange = eValorDinheiroChange
    OnDblClick = eValorDinheiroDblClick
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorCheque: TEditLuka
    Left = 114
    Top = 54
    Width = 97
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 3
    Text = '0,00'
    OnChange = eValorDinheiroChange
    OnDblClick = eValorDinheiroDblClick
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorCartaoDebito: TEditLuka
    Left = 114
    Top = 75
    Width = 97
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 4
    Text = '0,00'
    OnChange = eValorDinheiroChange
    OnDblClick = eValorDinheiroDblClick
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorCartaoCredito: TEditLuka
    Left = 114
    Top = 96
    Width = 97
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 5
    Text = '0,00'
    OnChange = eValorDinheiroChange
    OnDblClick = eValorDinheiroDblClick
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorCobranca: TEditLuka
    Left = 114
    Top = 117
    Width = 97
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 6
    Text = '0,00'
    OnChange = eValorDinheiroChange
    OnDblClick = eValorDinheiroDblClick
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorPix: TEditLuka
    Left = 114
    Top = 138
    Width = 97
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 7
    Text = '0,00'
    OnChange = eValorDinheiroChange
    OnDblClick = eValorDinheiroDblClick
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorAcumulativo: TEditLuka
    Left = 114
    Top = 159
    Width = 97
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 8
    Text = '0,00'
    OnChange = eValorDinheiroChange
    OnDblClick = eValorDinheiroDblClick
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorCredito: TEditLuka
    Left = 114
    Top = 179
    Width = 97
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 9
    Text = '0,00'
    OnChange = eValorDinheiroChange
    OnDblClick = eValorDinheiroDblClick
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorTroco: TEditLuka
    Left = 114
    Top = 207
    Width = 97
    Height = 22
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 10
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
end
