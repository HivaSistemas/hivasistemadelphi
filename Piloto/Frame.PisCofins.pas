unit Frame.PisCofins;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.StdCtrls, _Biblioteca,
  ComboBoxLuka, _Sessao;

type
  TFrPisCofins = class(TFrameHerancaPrincipal)
    cbPercentualPIS: TComboBoxLuka;
    lb1: TLabel;
    lb2: TLabel;
    cbPercentualCOFINS: TComboBoxLuka;
    st4: TStaticText;
    st1: TStaticText;
    lb3: TLabel;
    cbCstEntradaPIS: TComboBoxLuka;
    lb4: TLabel;
    cbCstSaidaPIS: TComboBoxLuka;
    lb5: TLabel;
    cbCstEntradaCOFINS: TComboBoxLuka;
    lb6: TLabel;
    cbCstSaidaCOFINS: TComboBoxLuka;
  private
    { Private declarations }
  public
    constructor Create(AOwner: TComponent); override;

    procedure Clear; override;
    procedure SomenteLeitura(pValue: Boolean); override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
    procedure VerificarRegistro;
  end;

implementation

{$R *.dfm}

{ TFrPisCofins }

procedure TFrPisCofins.Clear;
begin
  inherited;
  _Biblioteca.LimparCampos([
    cbPercentualPIS,
    cbPercentualCOFINS,
    cbCstEntradaPIS,
    cbCstSaidaPIS,
    cbCstEntradaCOFINS,
    cbCstSaidaCOFINS]
  );
end;

constructor TFrPisCofins.Create(AOwner: TComponent);
begin
  inherited;
  _Sessao.Sessao.SetComboBoxPisCofins(cbCstEntradaPIS);
  _Sessao.Sessao.SetComboBoxPisCofins(cbCstSaidaPIS);

  _Sessao.Sessao.SetComboBoxPisCofins(cbCstEntradaCOFINS);
  _Sessao.Sessao.SetComboBoxPisCofins(cbCstSaidaCOFINS);
end;

procedure TFrPisCofins.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    cbPercentualPIS,
    cbPercentualCOFINS,
    cbCstEntradaPIS,
    cbCstSaidaPIS,
    cbCstEntradaCOFINS,
    cbCstSaidaCOFINS],
    pEditando,
    pLimpar
  );
end;

procedure TFrPisCofins.SomenteLeitura(pValue: Boolean);
begin
  inherited;
  _Biblioteca.SomenteLeitura([
    cbPercentualPIS,
    cbPercentualCOFINS,
    cbCstEntradaPIS,
    cbCstSaidaPIS,
    cbCstEntradaCOFINS,
    cbCstSaidaCOFINS],
    pValue
  );
end;

procedure TFrPisCofins.VerificarRegistro;
begin
  if cbPercentualPIS.ItemIndex = -1 then begin
    _Biblioteca.Exclamar('O percentual de PIS n�o foi informado corretamente, verifique!');
    SetarFoco(cbPercentualPIS);
    Abort;
  end;

  if cbPercentualCOFINS.ItemIndex = -1 then begin
    _Biblioteca.Exclamar('O percentual da COFINS n�o foi informado corretamente, verifique!');
    SetarFoco(cbPercentualCOFINS);
    Abort;
  end;

  if cbCstEntradaPIS.ItemIndex = -1 then begin
    _Biblioteca.Exclamar('O CST de entrada do PIS n�o foi informado corretamente, verifique!');
    SetarFoco(cbCstEntradaPIS);
    Abort;
  end;

  if cbCstSaidaPIS.ItemIndex = -1 then begin
    _Biblioteca.Exclamar('O CST de sa�da do PIS n�o foi informado corretamente, verifique!');
    SetarFoco(cbCstSaidaPIS);
    Abort;
  end;

  if cbCstEntradaCOFINS.ItemIndex = -1 then begin
    _Biblioteca.Exclamar('O CST de entrada da COFINS n�o foi informado corretamente, verifique!');
    SetarFoco(cbCstEntradaCOFINS);
    Abort;
  end;

  if cbCstSaidaCOFINS.ItemIndex = -1 then begin
    _Biblioteca.Exclamar('O CST de sa�da da COFINS n�o foi informado corretamente, verifique!');
    SetarFoco(cbCstSaidaCOFINS);
    Abort;
  end;
end;

end.
