inherited FrTipoAnaliseCusto: TFrTipoAnaliseCusto
  Width = 168
  Height = 94
  ExplicitWidth = 168
  ExplicitHeight = 94
  object rgTiposCusto: TRadioGroupLuka
    Left = 3
    Top = 3
    Width = 158
    Height = 84
    Caption = ' Tipo de an'#225'lise de custo '
    Items.Strings = (
      'A'
      'B'
      'C'
      'B + C')
    TabOrder = 0
    Valores.Strings = (
      'A'
      'B'
      'C')
  end
end
