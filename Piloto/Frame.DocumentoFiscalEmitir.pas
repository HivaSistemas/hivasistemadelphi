unit Frame.DocumentoFiscalEmitir;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.StdCtrls, _Biblioteca,
  CheckBoxLuka;

type
  RecDocumentoFiscalEmitir = record
    EmitirRecibo: Boolean;
    EmitirNFCe: Boolean;
    EmitirNFe: Boolean;
    NenhumDocumentoFiscalEmitir: Boolean;
    TipoNotaGerar: string;
  end;

  TFrameDocumentoFiscalEmitir = class(TFrameHerancaPrincipal)
    gbDocumentosEmitir: TGroupBox;
    ckRecibo: TCheckBoxLuka;
    ckEmitirNFCe: TCheckBoxLuka;
    ckEmitirNFe: TCheckBoxLuka;
  private
    { Private declarations }
  public
    procedure VerificarRegistro;
    function GetDocumentosEmitir: RecDocumentoFiscalEmitir;
  end;

implementation

{$R *.dfm}

{ TFrameDocumentoFiscalEmitir }

function TFrameDocumentoFiscalEmitir.GetDocumentosEmitir: RecDocumentoFiscalEmitir;
begin
  Result.EmitirRecibo := ckRecibo.Checked;
  Result.EmitirNFCe := ckEmitirNFCe.Checked;
  Result.EmitirNFe := ckEmitirNFe.Checked;
  Result.NenhumDocumentoFiscalEmitir := not(Result.EmitirNFCe or Result.EmitirNFe);

  Result.TipoNotaGerar := 'NI';
  if ckEmitirNFCe.Checked then
    Result.TipoNotaGerar := 'NE'
  else if ckEmitirNFe.Checked then
    Result.TipoNotaGerar := 'NF';
end;

procedure TFrameDocumentoFiscalEmitir.VerificarRegistro;
begin
  if ckEmitirNFCe.Checked and ckEmitirNFe.Checked then begin
    _Biblioteca.Exclamar('N�o � poss�vel emitir NFCe e NFe ao mesmo tempo, selecione apenas um tipo de documento!');
    SetarFoco(ckEmitirNFCe);
    Abort;
  end;
end;

end.
