unit EntradaNotasFiscaisServicos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Biblioteca, _Sessao,
  CheckBoxLuka, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, FrameDadosCobranca, _EntradasNotasFiscServicos,
  FrameCFOPs, Vcl.Mask, EditLukaData, FrameEmpresas, ComboBoxLuka, _RecordsEspeciais,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameFornecedores, _EntradasNfServicosFinanc,
  FramePlanosFinanceiros, FrameCentroCustos;

type
  TFormEntradaNotasFiscaisServicos = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    lb2: TLabel;
    lb3: TLabel;
    FrFornecedor: TFrFornecedores;
    eNumeroNotaFiscal: TEditLuka;
    cbModeloNota: TComboBoxLuka;
    cbSerieNota: TComboBoxLuka;
    FrEmpresa: TFrEmpresas;
    lb6: TLabel;
    eDataEmissaoNota: TEditLukaData;
    FrCFOPCapa: TFrCFOPs;
    lb17: TLabel;
    eValorTotalNota: TEditLuka;
    FrDadosCobranca: TFrDadosCobranca;
    FrPlanoFinanceiro: TFrPlanosFinanceiros;
    lb4: TLabel;
    eDataContabil: TEditLukaData;
    FrCentroCusto: TFrCentroCustos;
    procedure cbSerieNotaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eValorTotalNotaChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    function getComponentesCabecalho: TArray<TControl>;
    function getComponentesItens: TArray<TControl>;
    procedure IniciarComponentes;

    procedure PreencherRegistro(pNota: RecEntradasNotasFiscServicos);
  protected
    procedure Modo(pEditando: Boolean); override;
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
  end;

implementation

{$R *.dfm}

{ TFormEntradaNotasFiscaisServicos }

function TFormEntradaNotasFiscaisServicos.getComponentesItens: TArray<TControl>;
begin
  Result :=
   [eDataEmissaoNota,
   eDataContabil,
   FrCFOPCapa,
   eValorTotalNota,
   FrPlanoFinanceiro,
   FrCentroCusto,
   FrDadosCobranca];
end;

procedure TFormEntradaNotasFiscaisServicos.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  vCentroCustoId: Integer;
begin
  inherited;

  vCentroCustoId := 0;
  if not FrCentroCusto.EstaVazio then
    vCentroCustoId := FrCentroCusto.GetCentro().CentroCustoId;

  vRetBanco :=
    _EntradasNotasFiscServicos.AtualizarEntradasNotasFiscServicos(
      Sessao.getConexaoBanco,
      eID.AsInt,
      FrEmpresa.getEmpresa().EmpresaId,
      FrFornecedor.GetFornecedor().cadastro_id,
      eNumeroNotaFiscal.AsInt,
      cbModeloNota.GetValor,
      cbSerieNota.GetValor,
      eDataEmissaoNota.AsData,
      eDataContabil.AsData,
      FrCFOPCapa.GetCFOP().CfopPesquisaId,
      eValorTotalNota.AsCurr,
      FrPlanoFinanceiro.getDados().PlanoFinanceiroId,
      vCentroCustoId,
      FrDadosCobranca.Titulos
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormEntradaNotasFiscaisServicos.IniciarComponentes;
var
  vInicial: string;
begin
  if FrFornecedor.GetFornecedor().cadastro.estado_id = FrEmpresa.getEmpresa.EstadoId then
    vInicial := '1'
  else
    vInicial := '2';

  FrCFOPCapa.SetInicioCFOPPesquisa(vInicial);
end;

procedure TFormEntradaNotasFiscaisServicos.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar(getComponentesCabecalho, pEditando);
  _Biblioteca.Habilitar(getComponentesItens, False);

  if pEditando then
    FrEmpresa.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId, True);
end;

procedure TFormEntradaNotasFiscaisServicos.BuscarRegistro;
var
  vNota: TArray<RecEntradasNotasFiscServicos>;
begin
  vNota := _EntradasNotasFiscServicos.BuscarEntradasNotasFiscServicos(Sessao.getConexaoBanco, 0, [eID.AsInt]);
  if vNota = nil then begin
    NenhumRegistro;
    Exit;
  end;

  inherited;

  PreencherRegistro(vNota[0]);
end;

procedure TFormEntradaNotasFiscaisServicos.PesquisarRegistro;
begin
  inherited;

end;

procedure TFormEntradaNotasFiscaisServicos.PreencherRegistro(pNota: RecEntradasNotasFiscServicos);
begin
  eID.AsInt := pNota.EntradaId;
  FrEmpresa.InserirDadoPorChave(pNota.EmpresaId, False);
  FrFornecedor.InserirDadoPorChave(pNota.FornecedorId, False);
  eNumeroNotaFiscal.AsInt := pNota.NumeroNota;
  cbModeloNota.SetIndicePorValor(pNota.ModeloNota);
  cbSerieNota.SetIndicePorValor(pNota.SerieNota);

  eDataEmissaoNota.AsData := pNota.DataEmissao;
  eDataContabil.AsData    := pNota.DataContabil;
  FrCFOPCapa.InserirDadoPorChave(pNota.CfopId, False);
  FrPlanoFinanceiro.InserirDadoPorChave(pNota.PlanoFinanceiroId, False);
  FrCentroCusto.InserirDadoPorChave(pNota.CentroCustoId);
  eValorTotalNota.AsCurr  := pNota.ValorTotalNota;

  FrDadosCobranca.Titulos := _EntradasNfServicosFinanc.BuscarEntradasNfServicosFinanc(Sessao.getConexaoBanco, 0, [pNota.EntradaId]);
end;

procedure TFormEntradaNotasFiscaisServicos.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if not eDataEmissaoNota.DataOk then begin
    _Biblioteca.Exclamar('A data de emiss�o n�o foi informada corretamente, verifique!');
    SetarFoco(eDataEmissaoNota);
    Abort;
  end;

  if not eDataContabil.DataOk then begin
    _Biblioteca.Exclamar('A data cont�bil n�o foi informada corretamente, verifique!');
    SetarFoco(eDataContabil);
    Abort;
  end;

  if FrCFOPCapa.EstaVazio then begin
    _Biblioteca.Exclamar('O CFOP n�o foi informado corretamente, verifique!');
    SetarFoco(FrCFOPCapa);
    Abort;
  end;

  if FrFornecedor.EstaVazio then begin
    _Biblioteca.Exclamar('O fornecedor n�o foi informado corretamente, verifique!');
    SetarFoco(FrFornecedor);
    Abort;
  end;

  if eValorTotalNota.AsCurr = 0 then begin
    _Biblioteca.Exclamar('O valor total da nota n�o foi informado corretamente, verifique!');
    SetarFoco(eValorTotalNota);
    Abort;
  end;

  if FrPlanoFinanceiro.EstaVazio then begin
    _Biblioteca.Exclamar('O plano financeiro n�o foi informado corretamente, verifique!');
    SetarFoco(FrPlanoFinanceiro);
    Abort;
  end;

  if FrDadosCobranca.ExisteDiferenca then begin
    Exclamar('Existe diferen�a nos t�tulos financeiros lan�ados, verifique!');
    SetarFoco(FrDadosCobranca);
    Abort;
  end;
end;

procedure TFormEntradaNotasFiscaisServicos.cbSerieNotaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if FrEmpresa.EstaVazio then begin
    _Biblioteca.Exclamar('A empresa da entrada deve ser informada, verifique!');
    SetarFoco(FrEmpresa);
    Exit;
  end;

  if FrFornecedor.EstaVazio then begin
    _Biblioteca.Exclamar('O fornecedor da entrada deve ser informado, verifique!');
    SetarFoco(FrFornecedor);
    Exit;
  end;

  if eNumeroNotaFiscal.AsInt = 0 then begin
    _Biblioteca.Exclamar('O numero da nota fiscal deve ser informado, verifique!');
    SetarFoco(eNumeroNotaFiscal);
    Exit;
  end;

  if cbModeloNota.Text = '' then begin
    _Biblioteca.Exclamar('O modelo da nota fiscal deve ser informado, verifique!');
    SetarFoco(cbModeloNota);
    Exit;
  end;

  if cbSerieNota.Text = '' then begin
    _Biblioteca.Exclamar('A s�rie da nota fiscal deve ser informado, verifique!');
    SetarFoco(cbSerieNota);
    Exit;
  end;

//  if _EntradasNotasFiscais.ExisteEntradaNFe(Sessao.getConexaoBanco, FrFornecedor.GetFornecedor().cadastro_id, eNumeroNotaFiscal.AsInt, cbModeloNota.Text, cbSerieNota.Text) then begin
//    _Biblioteca.Exclamar('J� existe uma entrada de nota fiscal com os dados informados!');
//    SetarFoco(FrFornecedor);
//    Exit;
//  end;

  _Biblioteca.Habilitar(getComponentesCabecalho, False, False);
  _Biblioteca.Habilitar(getComponentesItens, True, False);
  SetarFoco(eDataEmissaoNota);

  IniciarComponentes;
end;

procedure TFormEntradaNotasFiscaisServicos.eValorTotalNotaChange(Sender: TObject);
begin
  inherited;
  FrDadosCobranca.ValorPagar := eValorTotalNota.AsDouble;
end;

procedure TFormEntradaNotasFiscaisServicos.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _EntradasNotasFiscServicos.ExcluirEntradasNotasFiscServicos(Sessao.getConexaoBanco, eID.AsInt);
  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.RegistroExcluidoSucesso;

  inherited;
end;

procedure TFormEntradaNotasFiscaisServicos.FormCreate(Sender: TObject);
begin
  inherited;
  Sessao.SetComboBoxSeriesNotas(cbSerieNota);
  FrDadosCobranca.Natureza := 'P';
  FrDadosCobranca.setTelaEntrada;
end;

function TFormEntradaNotasFiscaisServicos.getComponentesCabecalho: TArray<TControl>;
begin
  Result :=
    [FrEmpresa,
    FrFornecedor,
    eNumeroNotaFiscal,
    cbModeloNota,
    cbSerieNota];
end;

end.
