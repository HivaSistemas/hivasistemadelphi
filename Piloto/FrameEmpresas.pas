unit FrameEmpresas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _RecordsCadastros,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, PesquisaEmpresas, _Empresas, System.Math, _Sessao,
  Vcl.Buttons, Vcl.Menus, _Funcionarios;

type
  TFrEmpresas = class(TFrameHenrancaPesquisas)
  public
    function getEmpresa(pLinha: Integer = -1): RecEmpresas;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function AdicionarPesquisandoTodos: TArray<TObject>; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrEmpresas }

function TFrEmpresas.AdicionarDireto: TObject;
var
  empresas: TArray<RecEmpresas>;
  empresasFiltradas: TArray<RecEmpresas>;
  empresasAutorizadas: TArray<Integer>;
  i: Integer;
  j: Integer;
begin
  empresas := _Empresas.BuscarEmpresas(Sessao.getConexaoBanco, 0, [FChaveDigitada] );

  if Sessao.getUsuarioLogado.GerenteSistema then
    empresasFiltradas := empresas
  else begin
    empresasAutorizadas := _Funcionarios.BuscarEmpresasFuncionario(
      Sessao.getConexaoBanco,
      Sessao.getUsuarioLogado.funcionario_id
    );

    for i := Low(empresas) to High(empresas) do begin
      for j := Low(empresasAutorizadas) to High(empresasAutorizadas) do begin
        if empresasAutorizadas[j] = empresas[i].EmpresaId then begin
          SetLength(empresasFiltradas, Length(empresasFiltradas) + 1);
          empresasFiltradas[High(empresasFiltradas)] := empresas[i];
          Continue;
        end;
      end;
    end;
  end;

  if empresasFiltradas = nil then
    Result := nil
  else
    Result := empresasFiltradas[0];
end;

function TFrEmpresas.AdicionarPesquisando: TObject;
begin
  Result := PesquisaEmpresas.PesquisarEmpresa;
end;

function TFrEmpresas.AdicionarPesquisandoTodos: TArray<TObject>;
begin
  Result := PesquisaEmpresas.PesquisarEmpresas;
end;

function TFrEmpresas.getEmpresa(pLinha: Integer): RecEmpresas;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecEmpresas(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrEmpresas.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecEmpresas(FDados[i]).EmpresaId = RecEmpresas(pSender).EmpresaId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrEmpresas.MontarGrid;
var
  i: Integer;
  pSender: RecEmpresas;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      pSender := RecEmpresas(FDados[i]);
      AAdd([IntToStr(pSender.EmpresaId), pSender.NomeFantasia]);
    end;
  end;
end;

end.
