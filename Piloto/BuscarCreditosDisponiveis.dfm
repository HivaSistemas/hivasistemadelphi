inherited FormBuscarCreditosDisponiveis: TFormBuscarCreditosDisponiveis
  Caption = 'Busca de cr'#233'ditos dispon'#237'veis'
  ClientHeight = 309
  ClientWidth = 573
  ExplicitWidth = 579
  ExplicitHeight = 338
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 272
    Width = 573
    ExplicitTop = 272
    ExplicitWidth = 573
  end
  object sgCreditos: TGridLuka
    Left = 0
    Top = 0
    Width = 573
    Height = 272
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alClient
    ColCount = 10
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
    TabOrder = 1
    OnDblClick = sgCreditosDblClick
    OnDrawCell = sgCreditosDrawCell
    OnKeyDown = sgCreditosKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Sel.?'
      'C'#243'digo'
      'Tipo'
      'Data cadastro'
      'Documento'
      'Valor'
      'Ind. Cond. Pagto'
      'Data Vencimento'
      'Cliente'
      'Bloqueado')
    OnGetCellColor = sgCreditosGetCellColor
    OnGetCellPicture = sgCreditosGetCellPicture
    Grid3D = False
    RealColCount = 10
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      31
      44
      92
      84
      101
      91
      94
      102
      293
      64)
  end
end
