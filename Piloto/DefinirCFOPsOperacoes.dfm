inherited FormDefinirCFOPOperacoes: TFormDefinirCFOPOperacoes
  Caption = 'Defini'#231#227'o de CFOPs de opera'#231#245'es'
  ClientHeight = 379
  ClientWidth = 668
  ExplicitWidth = 674
  ExplicitHeight = 408
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 379
    ExplicitHeight = 379
    inherited sbExcluir: TSpeedButton
      Left = -120
      Visible = False
      ExplicitLeft = -120
    end
    inherited sbPesquisar: TSpeedButton
      Left = -120
      Visible = False
      ExplicitLeft = -120
    end
  end
  inline FrEmpresa: TFrEmpresas
    Left = 125
    Top = 5
    Width = 426
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 125
    ExplicitTop = 5
    ExplicitWidth = 426
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 401
      Height = 23
      ExplicitWidth = 401
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 426
      ExplicitWidth = 426
      inherited lbNomePesquisa: TLabel
        Width = 47
        Caption = 'Empresa'
        ExplicitWidth = 47
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 321
        ExplicitLeft = 321
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 401
      Height = 24
      ExplicitLeft = 401
      ExplicitHeight = 24
    end
  end
  object sgCFOPs: TGridLuka
    Left = 125
    Top = 49
    Width = 540
    Height = 328
    Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
    Align = alCustom
    ColCount = 4
    Ctl3D = True
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goRowSelect]
    ParentCtl3D = False
    PopupMenu = pmOperacoes
    TabOrder = 2
    OnDrawCell = sgCFOPsDrawCell
    OnKeyDown = sgCFOPsKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Tipo opera'#231#227'o'
      'CST'
      'Tipo de movimento'
      'CFOP')
    Grid3D = False
    RealColCount = 8
    AtivarPopUpSelecao = False
    ColWidths = (
      123
      67
      247
      78)
  end
  object pmOperacoes: TPopupMenu
    Left = 576
    Top = 224
    object miInserirOperacao: TMenuItem
      Caption = 'Inserir opera'#231#227'o ( Insert )'
      OnClick = miInserirOperacaoClick
    end
    object miN1: TMenuItem
      Caption = '-'
    end
    object miDeletarOperacao: TMenuItem
      Caption = 'Deletar opera'#231'ao ( Delete )'
      OnClick = miDeletarOperacaoClick
    end
  end
end
