unit Relacao.ProdutosPaiFilho;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _Sessao, _Biblioteca,
  FrameDataInicialFinal, Frame.DepartamentosSecoesLinhas, _Produtos,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameProdutos, Vcl.ComCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, Vcl.Grids, GridLuka, Vcl.StdCtrls, StaticTextLuka,
  CheckBoxLuka;

type
  TFormRelacaoProdutosPaiFilho = class(TFormHerancaRelatoriosPageControl)
    FrProdutosPai: TFrProdutos;
    FrDepartamentosSecoesLinhas: TFrameDepartamentosSecoesLinhas;
    FrDataCadastroProduto: TFrDataInicialFinal;
    FrProdutosFilho: TFrProdutos;
    StaticTextLuka1: TStaticTextLuka;
    spSeparador: TSplitter;
    StaticTextLuka2: TStaticTextLuka;
    sgProdutosPai: TGridLuka;
    sgProdutosFilho: TGridLuka;
    procedure sgProdutosPaiDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgProdutosFilhoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgProdutosPaiClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FProdutos: TArray<RecProdutosPaiFilho>;
  protected
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

const
  cpProdutoId    = 0;
  cpNome         = 1;

  cfProdutoID    = 0;
  cfNome         = 1;
  cfQtdeVezesPai = 2;

procedure TFormRelacaoProdutosPaiFilho.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vLinha: Integer;
  vPosic: Integer;
begin
  inherited;
  sgProdutosPai.ClearGrid();
  sgProdutosFilho.ClearGrid();

  if not FrProdutosPai.EstaVazio then
    vSql := vSql + ' and ' + FrProdutosPai.getSqlFiltros('PRO.PRODUTO_PAI_ID');

  if not FrProdutosFilho.EstaVazio then
    vSql := vSql + ' and ' + FrProdutosFilho.getSqlFiltros('PRO.PRODUTO_ID');

  if not FrDepartamentosSecoesLinhas.EstaVazio then
    vSql := vSql + ' and ' + FrDepartamentosSecoesLinhas.getSqlFiltros('PRO.LINHA_PRODUTO_ID');

  if not FrDataCadastroProduto.NenhumaDataValida then
    vSql := vSql + ' and ' + FrDataCadastroProduto.getSqlFiltros('PRO.DATA_CADASTRO');

  FProdutos := _Produtos.BuscarProdutosPaiFilho(Sessao.getConexaoBanco, vSql);
  if FProdutos = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vLinha := 0;
  for i := Low(FProdutos) to High(FProdutos) do begin
    vPosic := sgProdutosPai.Localizar([cpProdutoId], [NFormat(FProdutos[i].ProdutoPaiId)]);
    if vPosic > 0 then
      Continue;

    Inc(vLinha);

    sgProdutosPai.Cells[cpProdutoId, vLinha] := NFormat(FProdutos[i].ProdutoPaiId);
    sgProdutosPai.Cells[cpNome, vLinha]      := FProdutos[i].NomeProdutoPai;
    sgProdutosPai.SetLinhasGridPorTamanhoVetor( vLinha );
  end;

  SetarFoco(sgProdutosPai);
  sgProdutosPaiClick(nil);
end;

procedure TFormRelacaoProdutosPaiFilho.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrProdutosPai);
end;

procedure TFormRelacaoProdutosPaiFilho.sgProdutosFilhoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[cfProdutoId, cfQtdeVezesPai] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgProdutosFilho.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoProdutosPaiFilho.sgProdutosPaiClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
begin
  inherited;
  vLinha := 0;
  sgProdutosFilho.ClearGrid();
  for i := Low(FProdutos) to High(FProdutos) do begin
    if SFormatInt(sgProdutosPai.Cells[cpProdutoId, sgProdutosPai.Row]) <> FProdutos[i].ProdutoPaiId then
      Continue;

    Inc(vLinha);

    sgProdutosFilho.Cells[cfProdutoId, vLinha]    := NFormat(FProdutos[i].ProdutoId);
    sgProdutosFilho.Cells[cfNome, vLinha]         := FProdutos[i].Nome;
    sgProdutosFilho.Cells[cfQtdeVezesPai, vLinha] := NFormat(FProdutos[i].QtdeVezesPai, 6);
  end;
  sgProdutosFilho.RowCount := IIfInt(vLinha > 1, vLinha + 1, 2);
end;

procedure TFormRelacaoProdutosPaiFilho.sgProdutosPaiDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[cpProdutoId] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgProdutosPai.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
