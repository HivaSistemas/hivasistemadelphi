unit Relacao.NotasFiscaisPreEntradas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatorios, Vcl.Buttons, _RecordsNFE, _Biblioteca,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Grids, GridLuka, _ComunicacaoNFE, _Sessao, System.Math,
  Vcl.Menus;

type
  TFormRelacaoNotasFiscaisPreEntradas = class(TFormHerancaRelatorios)
    pnNotasFiscais: TPanel;
    sgNotasFiscais: TGridLuka;
    pmOperacoesManifesto: TPopupMenu;
    miManifestarCienciaOperacao: TMenuItem;
    miConfirmacaoOperacao: TMenuItem;
    miDesconhecimentoOperacao: TMenuItem;
    miOperacaoNaoRealizada: TMenuItem;
    procedure sgNotasFiscaisDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  protected
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormRelacaoNotasFiscaisPreEntradas }

const
  coCNPJEmitente  = 0;
  coEmitente      = 1;
  coDataEmissao   = 2;
  coValorTotal    = 3;
  coChaveNFe      = 4;

procedure TFormRelacaoNotasFiscaisPreEntradas.Carregar(Sender: TObject);
var
  i: Integer;
  vNFe: TComunicacaoNFe;
  vRespConsulta: RecRespostaNFE<RecResumoNFe>;
begin
  inherited;
  sgNotasFiscais.ClearGrid;

  vNFe := TComunicacaoNFe.Create(Self);
  vRespConsulta := vNFe.ConsultarNFeDistribuicao;
  vNFe.Free;

  if vRespConsulta.HouveErro then begin
    Exclamar(vRespConsulta.mensagem_erro);
    Exit;
  end;

  for i := Low(vRespConsulta.Dados) to High(vRespConsulta.Dados) do begin
    sgNotasFiscais.Cells[coEmitente, i + 1]     := vRespConsulta.Dados[i].RazaoSocial;
    sgNotasFiscais.Cells[coCNPJEmitente, i + 1] := vRespConsulta.Dados[i].CNPJ;
    sgNotasFiscais.Cells[coDataEmissao, i + 1]  := DFormat(vRespConsulta.Dados[i].DataEmissao);
    sgNotasFiscais.Cells[coValorTotal, i + 1]   := NFormat(vRespConsulta.Dados[i].ValorNota);
    sgNotasFiscais.Cells[coChaveNFe, i + 1]     := vRespConsulta.Dados[i].ChaveNFe;
  end;
  sgNotasFiscais.RowCount := IfThen(Length(vRespConsulta.Dados) > 1, High(vRespConsulta.Dados) + 2, 2);
end;

procedure TFormRelacaoNotasFiscaisPreEntradas.sgNotasFiscaisDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coValorTotal] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgNotasFiscais.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
