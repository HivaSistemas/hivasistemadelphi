inherited FormAlterarOrdensLocaisEntregasProdutos: TFormAlterarOrdensLocaisEntregasProdutos
  Caption = 'Altera'#231#227'o de ordens de locais para entrega de produtos'
  ClientHeight = 380
  ClientWidth = 682
  OnShow = FormShow
  ExplicitWidth = 688
  ExplicitHeight = 409
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 343
    Width = 682
    ExplicitTop = 343
    ExplicitWidth = 682
  end
  inline FrEmpresasAlterar: TFrEmpresas
    Left = 5
    Top = 0
    Width = 320
    Height = 81
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 5
    ExplicitWidth = 320
    inherited sgPesquisa: TGridLuka
      Width = 295
      ExplicitWidth = 295
    end
    inherited CkMultiSelecao: TCheckBox
      Checked = True
      State = cbChecked
    end
    inherited PnTitulos: TPanel
      Width = 320
      ExplicitWidth = 320
      inherited lbNomePesquisa: TLabel
        Width = 167
        Caption = 'Empresas que ser'#227'o alteradas'
        ExplicitWidth = 167
        ExplicitHeight = 14
      end
      inherited CkChaveUnica: TCheckBox
        Checked = False
        State = cbUnchecked
      end
      inherited pnSuprimir: TPanel
        Left = 215
        ExplicitLeft = 215
      end
    end
    inherited pnPesquisa: TPanel
      Left = 295
      ExplicitLeft = 295
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  inline FrOrdemAto: TFrOrdenacaoLocais
    Left = 5
    Top = 82
    Width = 222
    Height = 258
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 5
    ExplicitTop = 82
    inherited Panel1: TPanel
      inherited sgLocais: TGridLuka
        CorLinhaFoco = 38619
        CorColunaFoco = clActiveCaption
      end
    end
    inherited StaticTextLuka1: TStaticTextLuka
      ExplicitTop = 0
    end
  end
  inline FrOrdemRetirar: TFrOrdenacaoLocais
    Left = 231
    Top = 82
    Width = 222
    Height = 258
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 231
    ExplicitTop = 82
    inherited Panel1: TPanel
      inherited sgLocais: TGridLuka
        CorLinhaFoco = 38619
        CorColunaFoco = clActiveCaption
      end
    end
    inherited StaticTextLuka1: TStaticTextLuka
      Caption = 'A retirar'
      ExplicitTop = 0
    end
  end
  inline FrOrdemEntregar: TFrOrdenacaoLocais
    Left = 457
    Top = 82
    Width = 222
    Height = 258
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 4
    TabStop = True
    ExplicitLeft = 457
    ExplicitTop = 82
    inherited Panel1: TPanel
      inherited sgLocais: TGridLuka
        CorLinhaFoco = 38619
        CorColunaFoco = clActiveCaption
      end
    end
    inherited StaticTextLuka1: TStaticTextLuka
      Caption = 'A entregar'
      ExplicitTop = 0
    end
  end
end
