unit RelacaoComissoesPorLucratividade;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _ParametrosEmpresa,
  FrameDataInicialFinal, FrameFuncionarios, _FrameHerancaPrincipal, _RecordsCadastros,
  _FrameHenrancaPesquisas, FrameEmpresas, Vcl.Grids, GridLuka, Vcl.ComCtrls,
  Vcl.StdCtrls, CheckBoxLuka, Vcl.Buttons, Vcl.ExtCtrls, _RecordsOrcamentosVendas, _Biblioteca,
  Data.DB, Datasnap.DBClient, frxClass, frxDBSet;

type
  TFormRelacaoComissoesPorLucratividade = class(TFormHerancaRelatoriosPageControl)
    sgComissoes: TGridLuka;
    FrEmpresas: TFrEmpresas;
    FrFuncionarios: TFrFuncionarios;
    FrDataRecebimento: TFrDataInicialFinal;
    frxReportAnalitico: TfrxReport;
    dstComissao: TfrxDBDataset;
    cdsComissao: TClientDataSet;
    cdsComissaoIdVendedor: TIntegerField;
    cdsComissaoNomeVendedor: TStringField;
    cdsComissaoTipoComissao: TStringField;
    cdsComissaoOrc_Dev: TStringField;
    cdsComissaoDataRecDev: TDateField;
    cdsComissaoCliente: TStringField;
    cdsComissaoBaseComissao: TFloatField;
    cdsComissaoPerComissao: TFloatField;
    cdsComissaoValorComissao: TFloatField;
    cdsComissaoValorLucratividade: TFloatField;
    procedure sgComissoesDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgComissoesGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure FormCreate(Sender: TObject);
  private
    FTipoComissao: string;
  public
    { Public declarations }
  protected
    procedure Carregar(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

var
  FormRelacaoComissoesPorLucratividade: TFormRelacaoComissoesPorLucratividade;

implementation

uses
  _Sessao, _RelacaoComissoesPorFuncionarios;

{$R *.dfm}

const
  coTipo          = 0;
  coId            = 1;
  coData          = 2;
  coCliente       = 3;
  coBaseComissao  = 4;
  coValorLucro    = 5;
  coPercComissao  = 6;
  coValorComissao = 7;

  (* Colunas ocultas *)
  coTipoLinha     = 8;
  coIdVendedor    = 9;
  coNomeVendedor  = 10;
  coPercentualLucro = 11;
  coValorLucratividade = 12;

  ctCabecalho   = 'CAB';
  ctLinhaNorm   = 'LIN';
  ctSubTotal    = 'SUB';
  ctTotalizador = 'TOT';

{ TFormRelacaoComissoesPorLucratividade }

procedure TFormRelacaoComissoesPorLucratividade.Carregar(Sender: TObject);
var
  i, j: Integer;
  vLinha: Integer;
  vComando: string;
  vFuncionarioId: Integer;
  vNomeFuncionario: string;
  vLinhaSintetico: Integer;
  vComissoes: TArray<RecComissoesPorFuncionarios>;

  vComissao: Double;
  vValorVenda: Double;
  vBaseComissao: Double;

  vTotalComissao: Double;
  vTotalValorVenda: Double;
  vTotalBaseComissao: Double;

  vTotalLucroComissao: Double;
  vLucroComissao: Double;
  vValorComissaoLucratividade: Double;
  vFaixas: TArray<RecParametrosComissao>;
  vTipoComissao: string;
  vLucroComissaoAtual: Double;
  vComissaoLucratividadeTotal: Double;

  procedure TotalizarFuncionario;
  var
    j: Integer;
  begin
    sgComissoes.Cells[coCliente, vLinha]       := 'Total: ';
    sgComissoes.Cells[coBaseComissao, vLinha]  := NFormatN(vBaseComissao);

    if NFormatN(vBaseComissao) <> '' then begin
      sgComissoes.Cells[coPercentualLucro, vLinha] := NFormatN(
        (vLucroComissao / vBaseComissao) * 100,
        4
      );

      sgComissoes.Cells[coValorLucro, vLinha]  := NFormatN(vLucroComissao);
    end;

    if vTipoComissao = 'T' then begin
      for j := Low(vFaixas) to High(vFaixas) do begin
        if (vFaixas[j].ValorInicial <= vLucroComissao) and (vFaixas[j].ValorFinal >= vLucroComissao) then begin
          vValorComissaoLucratividade := vLucroComissao * (vFaixas[j].Percentual / 100);
          sgComissoes.Cells[coPercComissao, vLinha]  := NFormatN(vFaixas[j].Percentual);
          sgComissoes.Cells[coValorComissao, vLinha] := NFormatN(vValorComissaoLucratividade);
          sgComissoes.Cells[coIdVendedor, vLinha]    := IntToStr(vFuncionarioId);
          Break;
        end;
      end;
    end
    else begin
      sgComissoes.Cells[coValorComissao, vLinha] := NFormatN(vComissaoLucratividadeTotal);
      sgComissoes.Cells[coPercComissao, vLinha]  := NFormatN(((vComissaoLucratividadeTotal / vLucroComissao) * 100), 2)
    end;

    sgComissoes.Cells[coTipoLinha, vLinha]     := ctSubTotal;

    vTotalComissao          := vTotalComissao + vComissao;
    vTotalValorVenda        := vTotalValorVenda + vValorVenda;
    vTotalBaseComissao      := vTotalBaseComissao + vBaseComissao;
    vTotalLucroComissao     := vTotalLucroComissao + vLucroComissao;

    vComissao := 0;
    vValorVenda := 0;
    vBaseComissao := 0;
    vLucroComissao := 0;
    vComissaoLucratividadeTotal := 0;

    Inc(vLinha, 2);
  end;

begin
  inherited;
  sgComissoes.ClearGrid;

  FTipoComissao := '';

  vFaixas := _ParametrosEmpresa.BuscarParametrosComissao(
    Sessao.getConexaoBanco,
    FrEmpresas.getEmpresa().EmpresaId
  );

  if vFaixas = nil then begin
    Exclamar('� necess�rio definir as faixas de comiss�o por lucratividade na tela de Par�metros de comiss�o!');
    Abort;
  end;

  vTipoComissao := vFaixas[0].TipoComissao;
  FTipoComissao := vFaixas[0].TipoComissao;

  vComando := ' where COM.EMPRESA_ID ' + FrEmpresas.getSqlFiltros + ' ';

  //Se for vendedor s� vai ver a comiss�o dele
  if Sessao.getUsuarioLogado.nome_funcao = 'VENDEDOR' then
    vComando := vComando + ' and COM.FUNCIONARIO_ID = ' + IntToStr(Sessao.getUsuarioLogado.funcionario_id) + ' '
  else if not FrFuncionarios.EstaVazio then
    vComando := vComando + ' and COM.FUNCIONARIO_ID ' + FrFuncionarios.getSqlFiltros + ' ';

  if not FrDataRecebimento.NenhumaDataValida then
    vComando := vComando + ' and ' + FrDataRecebimento.getSqlFiltros('COM.DATA') + ' ';

  vComando :=
    vComando +
      'order by ' +
      '  FUNCIONARIO_ID, ' +
      '  DATA, ' +
      '  ID ';

  vComissoes := _RelacaoComissoesPorFuncionarios.BuscarComissoes(Sessao.getConexaoBanco, vComando, Sessao.getParametros.CalcularComissaoAcumuladoRecFin, Sessao.getParametros.CalcularComissaoAcumuladoRecPedido);
  if vComissoes = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vLinha := 1;
  vLinhaSintetico := 1;

  vFuncionarioId := -1;
  vNomeFuncionario := '';

  vComissao := 0;
  vValorVenda := 0;
  vBaseComissao := 0;

  vTotalComissao := 0;
  vTotalValorVenda := 0;
  vTotalBaseComissao := 0;
  vTotalLucroComissao := 0;
  vLucroComissao := 0;

  for i := Low(vComissoes) to High(vComissoes) do begin
    if vFuncionarioId <> vComissoes[i].FuncionarioId then begin
      if i > 0 then
        TotalizarFuncionario;

      vFuncionarioId   := vComissoes[i].FuncionarioId;
      vNomeFuncionario := vComissoes[i].NomeFuncionario;

      sgComissoes.Cells[coId, vLinha]        := '  ' + NFormat(vFuncionarioId) + ' - ' + vNomeFuncionario;
      sgComissoes.Cells[coTipoLinha, vLinha] := ctCabecalho;
      Inc(vLinha);
    end;

    sgComissoes.Cells[coTipo, vLinha]          := vComissoes[i].Tipo;
    sgComissoes.Cells[coId, vLinha]            := NFormat(vComissoes[i].Id);
    sgComissoes.Cells[coData, vLinha]          := DFormat(vComissoes[i].Data);
    sgComissoes.Cells[coCliente, vLinha]       := NFormat(vComissoes[i].CadastroId) + ' - ' + vComissoes[i].NomeCliente;
    sgComissoes.Cells[coBaseComissao, vLinha]  := NFormatN(vComissoes[i].BaseComissao);
    sgComissoes.Cells[coTipoLinha, vLinha]     := ctLinhaNorm;
    sgComissoes.Cells[coIdVendedor, vLinha]    := IntToStr(vFuncionarioId);
    sgComissoes.Cells[coNomeVendedor, vLinha]  := vNomeFuncionario;

    if Em(vComissoes[i].Tipo, ['ORC', 'ACU', 'FIN']) then begin
      sgComissoes.Cells[coPercentualLucro, vLinha] := NFormatN(
        Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(
          vComissoes[i].ValorVenda, vComissoes[i].ValorVenda - vComissoes[i].ValorCustoFinal,
          4
        ),
        4
      );

      vLucroComissaoAtual := (Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(
          vComissoes[i].ValorVenda, vComissoes[i].ValorVenda - vComissoes[i].ValorCustoFinal,
          4
        ) / 100) * vComissoes[i].BaseComissao;

      if vTipoComissao = 'V' then begin
        for j := Low(vFaixas) to High(vFaixas) do begin
          if (vFaixas[j].ValorInicial <= vLucroComissaoAtual) and (vFaixas[j].ValorFinal >= vLucroComissaoAtual) then begin
            vValorComissaoLucratividade := ((Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(
              vComissoes[i].ValorVenda, vComissoes[i].ValorVenda - vComissoes[i].ValorCustoFinal,
              4
            ) / 100) * vComissoes[i].BaseComissao) * (vFaixas[j].Percentual / 100);

            vComissaoLucratividadeTotal := vComissaoLucratividadeTotal + vValorComissaoLucratividade;
            sgComissoes.Cells[coPercComissao, vLinha]  := NFormatN(vFaixas[j].Percentual);
            sgComissoes.Cells[coValorComissao, vLinha] := NFormatN(vValorComissaoLucratividade);
            sgComissoes.Cells[coIdVendedor, vLinha]    := IntToStr(vFuncionarioId);
            Break;
          end;
        end;
      end;

      vLucroComissao := vLucroComissao + vLucroComissaoAtual;

      sgComissoes.Cells[coValorLucro, vLinha]    := NFormat(
        (Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(
                  vComissoes[i].ValorVenda, vComissoes[i].ValorVenda - vComissoes[i].ValorCustoFinal,
                  4
                ) / 100) * vComissoes[i].BaseComissao
      );
      vComissao          := vComissao + vComissoes[i].ValorComissao;
      vValorVenda        := vValorVenda + vComissoes[i].ValorVenda;
      vBaseComissao      := vBaseComissao + vComissoes[i].BaseComissao;
    end
    else begin
      vLucroComissaoAtual := (Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(
          vComissoes[i].ValorVenda, vComissoes[i].ValorVenda - vComissoes[i].ValorCustoFinal,
          4
        ) / 100) * vComissoes[i].BaseComissao;

      if vTipoComissao = 'V' then begin
        for j := Low(vFaixas) to High(vFaixas) do begin
          if (vFaixas[j].ValorInicial <= vLucroComissaoAtual) and (vFaixas[j].ValorFinal >= vLucroComissaoAtual) then begin
            vValorComissaoLucratividade := ((Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(
              vComissoes[i].ValorVenda, vComissoes[i].ValorVenda - vComissoes[i].ValorCustoFinal,
              4
            ) / 100) * vComissoes[i].BaseComissao) * (vFaixas[j].Percentual / 100);

            vComissaoLucratividadeTotal := vComissaoLucratividadeTotal - vValorComissaoLucratividade;
            sgComissoes.Cells[coPercComissao, vLinha]  := NFormatN(vFaixas[j].Percentual);
            sgComissoes.Cells[coValorComissao, vLinha] := NFormatN(vValorComissaoLucratividade);
            sgComissoes.Cells[coIdVendedor, vLinha]    := IntToStr(vFuncionarioId);
            Break;
          end;
        end;
      end;
      vLucroComissao := vLucroComissao - vLucroComissaoAtual;

      sgComissoes.Cells[coValorLucro, vLinha]    := NFormat(
        (Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(
                  vComissoes[i].ValorVenda, vComissoes[i].ValorVenda - vComissoes[i].ValorCustoFinal,
                  4
                ) / 100) * vComissoes[i].BaseComissao
      );

      vComissao          := vComissao - vComissoes[i].ValorComissao;
      vBaseComissao      := vBaseComissao - vComissoes[i].BaseComissao;
    end;

    Inc(vLinha);
  end;
  TotalizarFuncionario;

  sgComissoes.RowCount := vLinha;
  pcDados.ActivePage := tsResultado;
end;

procedure TFormRelacaoComissoesPorLucratividade.FormCreate(Sender: TObject);
begin
  inherited;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId);
end;

procedure TFormRelacaoComissoesPorLucratividade.Imprimir(Sender: TObject);
var
  i: Integer;
begin
  inherited;
    cdsComissao.Close;
    cdsComissao.CreateDataSet;
    cdsComissao.Open;
    for i := sgComissoes.RowCount + 1 downto 1 do
    begin
      if FTipoComissao = 'T' then begin
        if (sgComissoes.Cells[coIdVendedor, i] = '') and (SFormatDouble(sgComissoes.Cells[coPercComissao, i]) = 0) then
          continue;

        cdsComissao.Insert;
        cdsComissaoIdVendedor.AsString        := sgComissoes.Cells[coIdVendedor, i];
        cdsComissaoNomeVendedor.AsString      := sgComissoes.Cells[coNomeVendedor, i];
        cdsComissaoTipoComissao.AsString      := sgComissoes.Cells[coTipo, i];
        cdsComissaoOrc_Dev.AsString           := sgComissoes.Cells[coId, i];
        cdsComissaoDataRecDev.AsString        := sgComissoes.Cells[coData, i];
        cdsComissaoCliente.AsString           := sgComissoes.Cells[coCliente, i];
        cdsComissaoBaseComissao.AsFloat       := SFormatDouble(sgComissoes.Cells[coBaseComissao, i]);
        cdsComissaoValorLucratividade.AsFloat := SFormatDouble(sgComissoes.Cells[coValorLucro, i]);

        if SFormatDouble(sgComissoes.Cells[coPercComissao, i]) > 0 then begin
          cdsComissaoPerComissao.AsFloat        := SFormatDouble(sgComissoes.Cells[coPercComissao, i]);
          cdsComissaoValorComissao.AsFloat      := SFormatDouble(sgComissoes.Cells[coValorComissao, i]);
        end;

        cdsComissao.Post;
      end
      else begin
        if (sgComissoes.Cells[coIdVendedor, i] = '') and (SFormatDouble(sgComissoes.Cells[coPercComissao, i]) = 0) then
          continue;

        cdsComissao.Insert;
        cdsComissaoIdVendedor.AsString        := sgComissoes.Cells[coIdVendedor, i];
        cdsComissaoNomeVendedor.AsString      := sgComissoes.Cells[coNomeVendedor, i];
        cdsComissaoTipoComissao.AsString      := sgComissoes.Cells[coTipo, i];
        cdsComissaoOrc_Dev.AsString           := sgComissoes.Cells[coId, i];
        cdsComissaoDataRecDev.AsString        := sgComissoes.Cells[coData, i];
        cdsComissaoCliente.AsString           := sgComissoes.Cells[coCliente, i];
        cdsComissaoBaseComissao.AsFloat       := SFormatDouble(sgComissoes.Cells[coBaseComissao, i]);
        cdsComissaoValorLucratividade.AsFloat := SFormatDouble(sgComissoes.Cells[coValorLucro, i]);

        cdsComissaoPerComissao.AsFloat        := SFormatDouble(sgComissoes.Cells[coPercComissao, i]);
        cdsComissaoValorComissao.AsFloat      := SFormatDouble(sgComissoes.Cells[coValorComissao, i]);

        cdsComissao.Post;
      end;
    end;

    frxReportAnalitico.ShowReport
end;

procedure TFormRelacaoComissoesPorLucratividade.sgComissoesDrawCell(
  Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coId, coValorLucro, coBaseComissao, coPercComissao, coValorComissao] then
    vAlinhamento := taRightJustify
  else if ACol in[coTipo] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  if (ARow > 0) and (sgComissoes.Cells[coTipoLinha, ARow] = ctCabecalho) then
    sgComissoes.MergeCells([ARow, ACol], [ARow, coId], [ARow, coCliente], vAlinhamento, Rect)
  else
    sgComissoes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoComissoesPorLucratividade.sgComissoesGetCellColor(
  Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush;
  AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgComissoes.Cells[coTipoLinha, ARow] = ctCabecalho then begin
    AFont.Style  := [fsBold];
    AFont.Color  := clWhite;
    ABrush.Color := $000096DB;
  end
  else if sgComissoes.Cells[coTipoLinha, ARow] = ctSubTotal then begin
    AFont.Style  := [fsBold];
    AFont.Color  := clBlack;
    ABrush.Color := clGreen;
  end
  else if sgComissoes.Cells[coTipoLinha, ARow] = ctTotalizador then begin
    AFont.Style  := [fsBold];
    AFont.Color  := clWhite;
    ABrush.Color := $006C6C6C;
  end
  else if ACol = coTipo then begin
    AFont.Style := [fsBold];
    if sgComissoes.Cells[coTipo, ARow] = 'DEV' then
      AFont.Color := clRed
    else if sgComissoes.Cells[coTipo, ARow] = 'ORC' then
      AFont.Color := clBlue
    else if sgComissoes.Cells[coTipo, ARow] = 'ACU' then
      AFont.Color := $000096DB
    else if sgComissoes.Cells[coTipo, ARow] = 'FIN' then
      AFont.Color := $006C6C6C;
  end;
end;

procedure TFormRelacaoComissoesPorLucratividade.VerificarRegistro(
  Sender: TObject);
begin
  inherited;
  if FrEmpresas.EstaVazio then begin
    Exclamar('Ao menos uma empresa deve ser informada!');
    SetarFoco(FrEmpresas);
    Abort;
  end;
end;

end.
