unit FrameSeriesNotasFiscais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Frame.HerancaInsercaoExclusao, BuscaDados,
  Vcl.Menus, Vcl.StdCtrls, StaticTextLuka, Vcl.Grids, GridLuka, _Biblioteca;

type
  TFrSeriesNotasFiscais = class(TFrameHerancaInsercaoExclusao)
    procedure sgValoresDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
  private
    function getSeries: TArray<string>;
    procedure setSeries(pSeries: TArray<string>);
  protected
    procedure Inserir; override;
  published
    property Series: TArray<string> read getSeries write setSeries;
  end;

implementation

{$R *.dfm}

{ TFrSeriesNotasFiscais }

const
  coSerie = 0;

function TFrSeriesNotasFiscais.getSeries: TArray<string>;
var
  i: Integer;
begin
  SetLength(Result, sgValores.RowCount);
  for i := 0 to sgValores.RowCount -1 do
    Result[i] := sgValores.Cells[coSerie, i];
end;

procedure TFrSeriesNotasFiscais.Inserir;
var
  vLinha: Integer;
  vSerie: string;
begin
  inherited;

  vSerie := BuscaDados.BuscarDados('S�rie', '', 5);
  if vSerie = '' then
    Exit;

  vLinha := sgValores.Localizar([coSerie], [vSerie]);
  if vLinha > -1 then begin
    sgValores.Row := vLinha;
    Exit;
  end;

  if sgValores.Cells[coSerie, 0] = '' then
    vLinha := 0
  else
    vLinha := sgValores.RowCount;

  sgValores.Cells[coSerie, vLinha] := vSerie;
  sgValores.RowCount := vLinha + 1;

  sgValores.Row := vLinha;
  SetarFoco(sgValores);
end;

procedure TFrSeriesNotasFiscais.setSeries(pSeries: TArray<string>);
var
  i: Integer;
begin
  for i := Low(pSeries) to High(pSeries) do
    sgValores.Cells[coSerie, i] := pSeries[i];

  sgValores.RowCount := Length(pSeries);
end;

procedure TFrSeriesNotasFiscais.sgValoresDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  inherited;
  sgValores.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taCenter, Rect);
end;

end.
