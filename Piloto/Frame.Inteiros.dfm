inherited FrameInteiros: TFrameInteiros
  Width = 132
  Height = 84
  ExplicitWidth = 132
  ExplicitHeight = 84
  object sgInteiros: TGridLuka
    Left = 0
    Top = 18
    Width = 132
    Height = 66
    Align = alClient
    BevelInner = bvNone
    BevelOuter = bvNone
    ColCount = 1
    DefaultRowHeight = 19
    DrawingStyle = gdsClassic
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 0
    OnDrawCell = sgInteirosDrawCell
    OnKeyDown = sgInteirosKeyDown
    OnKeyPress = sgInteirosKeyPress
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    Grid3D = False
    RealColCount = 1
    AtivarPopUpSelecao = False
    ColWidths = (
      108)
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 132
    Height = 18
    Align = alTop
    Alignment = taLeftJustify
    BevelOuter = bvNone
    Caption = 'C'#243'digo'
    TabOrder = 1
  end
end
