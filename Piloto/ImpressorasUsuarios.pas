unit ImpressorasUsuarios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, HerancaImpressorasEstacaoUsuario, _Biblioteca, _Sessao,
  FrameFuncionarios, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, _RecordsEspeciais,
  FrameEmpresas, Vcl.ComCtrls, Vcl.StdCtrls, ComboBoxLuka, Vcl.Buttons, _ImpressorasUsuarios,
  Vcl.ExtCtrls, CheckBoxLuka;

type
  TFormImpressorasUsuario = class(TFormHerancaImpressoraEstacaoUsuario)
    FrEmpresa: TFrEmpresas;
    FrFuncionario: TFrFuncionarios;
    procedure FormCreate(Sender: TObject);
  private
    procedure FrFuncionarioEmpresaOnAposPesquisar(Sender: TObject);
  protected
    procedure Modo(pEditando: Boolean); override;
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

procedure TFormImpressorasUsuario.BuscarRegistro;
var
  vImpressoras: TArray<RecImpressorasUsuarios>;
begin
  inherited;

  vImpressoras := _ImpressorasUsuarios.BuscarImpressoras(Sessao.getConexaoBanco, 0, [FrEmpresa.getEmpresa.EmpresaId, FrFuncionario.GetFuncionario.funcionario_id]);
  if vImpressoras = nil then
    Exit;

  PreencherRegistros(
    vImpressoras[0].TipoImpressoraNFe,
    vImpressoras[0].ImpressoraNFe,
    vImpressoras[0].AbrirPreviewNFe,

    vImpressoras[0].TipoImpressoraNFCe,
    vImpressoras[0].ImpressoraNFCe,
    vImpressoras[0].AbrirPreviewNFCe,

    vImpressoras[0].TipoImpressoraCompPagamento,
    vImpressoras[0].ImpressoraCompPagamento,
    vImpressoras[0].AbrirPreviewCompPagamento,

    vImpressoras[0].TipoImpressoraComproEntrega,
    vImpressoras[0].ImpressoraComproEntrega,
    vImpressoras[0].AbrirPreviewComproEntrega,

    vImpressoras[0].TipoImpressoraListaSeparac,
    vImpressoras[0].ImpressoraListaSeparac,
    vImpressoras[0].AbrirPreviewListaSeparacao,

    vImpressoras[0].TipoImpressoraOrcamento,
    vImpressoras[0].ImpressoraOrcamento,
    vImpressoras[0].AbrirPreviewOrcamento,

    vImpressoras[0].TipoImpCompPagtoFinanceiro,
    vImpressoras[0].ImpCompPagtoFinanceiro,
    vImpressoras[0].AbrirPrevCompPagtoFinanc,

    vImpressoras[0].TipoImpressoraCompDevolucao,
    vImpressoras[0].ImpressoraCompDevolucao,
    vImpressoras[0].AbrirPrevCompDevolucao,

    vImpressoras[0].TipoImpressoraCompCaixa,
    vImpressoras[0].ImpressoraCompCaixa,
    vImpressoras[0].AbrirPrevCompCaixa,
    vImpressoras[0].ModeloBalanca,
    vImpressoras[0].PortaBalanca,

    vImpressoras[0].TipoImpressoraCtzPromo,
    vImpressoras[0].ImpressoraCtzPromo,
    vImpressoras[0].AbrirPrevCtzPromo
  );
end;

procedure TFormImpressorasUsuario.FormCreate(Sender: TObject);
begin
  inherited;
  FrFuncionario.OnAposPesquisar := FrFuncionarioEmpresaOnAposPesquisar;
  FrEmpresa.OnAposPesquisar := FrFuncionarioEmpresaOnAposPesquisar;
end;

procedure TFormImpressorasUsuario.FrFuncionarioEmpresaOnAposPesquisar(Sender: TObject);
begin
  if FrFuncionario.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio informar o funcion�rio!');
    SetarFoco(FrFuncionario);
    Exit;
  end;

  if FrEmpresa.EstaVazio then
    Exit;

  BuscarRegistro;
end;

procedure TFormImpressorasUsuario.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;

  vRetBanco :=
    _ImpressorasUsuarios.AtualizarImpressoras(
      Sessao.getConexaoBanco,
      FrEmpresa.getEmpresa.EmpresaId,
      FrFuncionario.GetFuncionario().funcionario_id,

      cbTipoImpressoraNFe.GetValor,
      cbCaminhoImpressoraNFe.Text,
      ckAbrirPreviewNFe.CheckedStr,

      cbTipoImpressoraNFCe.GetValor,
      cbCaminhoImpressoraNFCe.Text,
      ckAbrirPreviewNFCe.CheckedStr,

      cbTipoImpressoraCompPagamento.GetValor,
      cbCaminhoImpressoraCompPagamento.Text,
      ckAbrirPreviewCompPagamento.CheckedStr,

      cbTipoImpressoraComproEntrega.GetValor,
      cbCaminhoImpressoraComproEntrega.Text,
      ckAbrirPreviewCompEntrega.CheckedStr,

      cbTipoImpressoraListaSeparac.GetValor,
      cbCaminhoImpressoraListaSeparac.Text,
      ckAbrirPreviewListaSeparacao.CheckedStr,

      cbTipoImpressoraOrcamento.GetValor,
      cbCaminhoImpressoraOrcamento.Text,
      ckAbrirPreviewOrcamento.CheckedStr,

      cbTipoImpressoraCompPagtoFinanc.GetValor,
      cbCaminhoImpressoraCompPagtoFinanc.Text,
      ckAbrirPreviewCompPagtoFinanc.CheckedStr,

      cbTipoImpressoraComprovanteDevolucao.GetValor,
      cbCaminhoImpressoraComprovanteDevolucao.Text,
      ckAbrirPreviewComprovanteDevolucao.CheckedStr,

      cbTipoImpressoraCompCaixa.GetValor,
      cbCaminhoImpressoraCompCaixa.Text,
      ckAbrirPreviewCompCaixa.CheckedStr,
      cbxModeloBalanca.Text,
      cbxPortaBalanca.Text,

      cbTipoImpressoraCartazPromocional.GetValor,
      cbCaminhoImpressoraCartazPromocional.Text,
      ckAbrirPreviewCartazPromocional.CheckedStr
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  RotinaSucesso;
end;

procedure TFormImpressorasUsuario.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    cbTipoImpressoraNFe,
    cbTipoImpressoraNFCe,
    cbTipoImpressoraCompPagamento,
    cbTipoImpressoraComproEntrega,
    cbTipoImpressoraListaSeparac,
    cbTipoImpressoraOrcamento,
    cbTipoImpressoraCompPagtoFinanc,
    cbTipoImpressoraComprovanteDevolucao,
    cbTipoImpressoraCompCaixa,
    cbCaminhoImpressoraNFe,
    cbCaminhoImpressoraNFCe,
    cbCaminhoImpressoraCompPagamento,
    cbCaminhoImpressoraComproEntrega,
    cbCaminhoImpressoraListaSeparac,
    cbCaminhoImpressoraOrcamento,
    cbCaminhoImpressoraCompPagtoFinanc,
    cbCaminhoImpressoraComprovanteDevolucao,
    cbCaminhoImpressoraCompCaixa,
    ckAbrirPreviewNFe,
    ckAbrirPreviewNFCe,
    ckAbrirPreviewCompPagamento,
    ckAbrirPreviewCompEntrega,
    ckAbrirPreviewListaSeparacao,
    ckAbrirPreviewOrcamento,
    ckAbrirPreviewCompPagtoFinanc,
    ckAbrirPreviewComprovanteDevolucao,
    ckAbrirPreviewCompCaixa],
    pEditando
  );

  if pEditando then begin
    _Biblioteca.Habilitar([FrFuncionario, FrEmpresa], False, False);
    SetarFoco(cbCaminhoImpressoraNFe);
    setIndexesDefault;
  end
  else begin
    _Biblioteca.Habilitar([FrFuncionario, FrEmpresa], True);
    SetarFoco(FrFuncionario);
  end;
end;

procedure TFormImpressorasUsuario.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
