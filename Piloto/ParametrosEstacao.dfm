inherited FormParametrosEstacao: TFormParametrosEstacao
  Caption = 'Par'#226'metros por esta'#231#227'o'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 125
    Top = 2
    Width = 42
    Height = 14
    Caption = 'Esta'#231#227'o'
  end
  inherited pnOpcoes: TPanel
    inherited sbDesfazer: TSpeedButton
      Visible = False
    end
    inherited sbExcluir: TSpeedButton
      Visible = False
    end
    inherited sbPesquisar: TSpeedButton
      Visible = False
    end
  end
  object eEstacao: TEditLuka
    Left = 125
    Top = 16
    Width = 356
    Height = 22
    CharCase = ecUpperCase
    Enabled = False
    ReadOnly = True
    TabOrder = 1
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object ckUtilizarTEF: TCheckBoxLuka
    Left = 125
    Top = 44
    Width = 84
    Height = 17
    Caption = 'Utilizar TEF'
    TabOrder = 2
    OnClick = ckUtilizarTEFClick
    CheckedStr = 'N'
  end
  inline FrGerenciadorCartaoTEF: TFrGerenciadoresCartoesTEF
    Left = 125
    Top = 62
    Width = 320
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 125
    ExplicitTop = 62
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Height = 24
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      inherited lbNomePesquisa: TLabel
        Width = 142
        Caption = 'Gerenciador de cart'#227'o TEF'
        ExplicitWidth = 142
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Height = 25
      ExplicitHeight = 25
    end
  end
end
