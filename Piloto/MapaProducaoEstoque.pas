unit MapaProducaoEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls,
  CheckBoxLuka, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _FrameHerancaPrincipal,
  _FrameHenrancaPesquisas, FrameProdutos, FrameLocais, StaticTextLuka, _RecordsOrcamentosVendas,
  Vcl.Grids, GridLuka, _RecordsEstoques, System.Math, _Biblioteca, _ProdutosKit, _Sessao, _RecordsEspeciais;

type
  TFormMapaProducao = class(TFormHerancaCadastroCodigo)
    FrProduto: TFrProdutos;
    FrLocalProduto: TFrLocais;
    sgProdutosProducao: TGridLuka;
    lb7: TLabel;
    eQuantidade: TEditLuka;
    StaticText2: TStaticText;
    procedure FormCreate(Sender: TObject);
    procedure sgProdutosProducaoDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  protected
    procedure GravarRegistro(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Modo(pEditando: Boolean); override;
    procedure BuscarRegistro; override;
    procedure ExcluirRegistro; override;
    procedure PesquisarRegistro; override;
  private
    procedure PreencherRegistro(pProducao: RecProducaoEstoque; pProducaoItens: TArray<RecProducaoEstoqueItens>);
    procedure AddProdutoGridKit(pProdutoId: Integer; pNomeProduto: string; pQuantidade: Double; pParticipacao: Double; pQuantidadeDisponivel: Double);

    procedure FrProdutoOnAposPesquisar(Sender: TObject);
    procedure FrProdutoOnAposDeletar(Sender: TObject);
  public
    { Public declarations }
  end;

var
  FormMapaProducao: TFormMapaProducao;

const
  coProdutoId      = 0;
  coNomeProduto    = 1;
  coQtdeNecessaria = 2;
  coQtdeDisponivel = 3;

implementation

{$R *.dfm}

uses _Estoques, _AjustesEstoque, _ProducaoEstoque, _EstoquesDivisao,
  _ProducaoEstoqueItens, _ProdutosVenda, _ComunicacaoNFE, _FormacaoCustosPrecos;

procedure TFormMapaProducao.FrProdutoOnAposPesquisar(Sender: TObject);
var
  vProdutosProducao: TArray<RecProdutosKit>;
  i: Integer;
  j: Integer;
  quantidadeDisponivel: Double;
  locaisProduto: TArray<RecDefinicoesLocaisVenda>;
begin
  if FrProduto.getProduto.TipoControleEstoque <> 'I' then begin
    _Biblioteca.Informar('Produtos que n�o s�o controlados por "Industrializa��o" n�o podem ser produzidos. Ajuste no cadastro de produtos!');
    SetarFoco(FrProduto);
    FrProduto.Clear;
    Exit;
  end;

  vProdutosProducao := _ProdutosKit.BuscarProdutosKit(Sessao.getConexaoBanco, 0, [FrProduto.getProduto.produto_id]);
  for i := Low(vProdutosProducao) to High(vProdutosProducao) do begin
    locaisProduto := nil;
    quantidadeDisponivel := 0;

    locaisProduto := _EstoquesDivisao.BuscarEstoquesLocaisDisponiveisMapaProducao(
      Sessao.getConexaoBanco,
      Sessao.getEmpresaLogada.EmpresaId,
      vProdutosProducao[i].ProdutoId
    );

    if locaisProduto <> nil then begin
      for j := Low(locaisProduto) to High(locaisProduto) do
        quantidadeDisponivel := locaisProduto[j].Disponivel + quantidadeDisponivel;

    end;

    AddProdutoGridKit(
      vProdutosProducao[i].ProdutoId,
      vProdutosProducao[i].NomeProduto,
      vProdutosProducao[i].Quantidade,
      vProdutosProducao[i].PercParticipacao,
      quantidadeDisponivel
    );
  end;

end;

procedure TFormMapaProducao.GravarRegistro(Sender: TObject);
var
  i: Integer;
  j: Integer;
  quantidadeNecessaria: Double;
  vRetornoBanco: RecRetornoBD;
  vProducaoEstoque: TArray<RecProducaoEstoqueItens>;
  locaisProduto: TArray<RecDefinicoesLocaisVenda>;
  FItens: TArray<RecItensFormacaoCusto>;
  notasFiscais: TArray<Integer>;
  retornoNota: RecRetornoBD;
  filtro: string;
begin
  inherited;
  SetLength(vProducaoEstoque, Length(vProducaoEstoque) + 1);
  vProducaoEstoque[0].produto_id := FrProduto.getProduto(0).produto_id;
  vProducaoEstoque[0].LocalId := FrLocalProduto.GetLocais(0).local_id;
  vProducaoEstoque[0].Quantidade := eQuantidade.AsDouble;
  vProducaoEstoque[0].Natureza := 'E';

  filtro := ' AND PRO.PRODUTO_ID = ' + IntToStr(FrProduto.getProduto.produto_id);
  FItens := _FormacaoCustosPrecos.BuscarPrecoVarejo(
    Sessao.getConexaoBanco,
    Sessao.getEmpresaLogada.EmpresaId,
    filtro
  );

  if FItens = nil then begin
    Exclamar('O pre�o de venda do produto ' + sgProdutosProducao.Cells[coProdutoId, i]  + ' n�o foi definido');
    Abort;
  end;

  vProducaoEstoque[0].PrecoUnitario := FItens[0].CustoCompraComercial;

  for i := 1 to sgProdutosProducao.RowCount -1 do begin
    locaisProduto := nil;
    quantidadeNecessaria := 0;

    locaisProduto := _EstoquesDivisao.BuscarEstoquesLocaisDisponiveisMapaProducao(
      Sessao.getConexaoBanco,
      Sessao.getEmpresaLogada.EmpresaId,
      SFormatInt(sgProdutosProducao.Cells[coProdutoId, i])
    );

    if locaisProduto = nil then begin
      Exclamar('N�o foi encontrado estoque suficiente do produto ' + sgProdutosProducao.Cells[coProdutoId, i]);
      Abort;
    end;

    filtro := ' AND PRO.PRODUTO_ID = ' + IntToStr(SFormatInt(sgProdutosProducao.Cells[coProdutoId, i]));
    FItens := _FormacaoCustosPrecos.BuscarPrecoVarejo(
      Sessao.getConexaoBanco,
      Sessao.getEmpresaLogada.EmpresaId,
      filtro
    );

    quantidadeNecessaria := SFormatDouble(sgProdutosProducao.Cells[coQtdeNecessaria, i]) * eQuantidade.AsDouble;

    for j := Low(locaisProduto) to High(locaisProduto) do begin
        if locaisProduto[j].Disponivel > 0 then begin
          if locaisProduto[j].Disponivel >= quantidadeNecessaria then begin
            SetLength(vProducaoEstoque, Length(vProducaoEstoque) + 1);
            vProducaoEstoque[High(vProducaoEstoque)].produto_id := SFormatInt(sgProdutosProducao.Cells[coProdutoId, i]);
            vProducaoEstoque[High(vProducaoEstoque)].LocalId := locaisProduto[j].LocalId;
            vProducaoEstoque[High(vProducaoEstoque)].Quantidade := quantidadeNecessaria;
            vProducaoEstoque[High(vProducaoEstoque)].Natureza := 'S';
            quantidadeNecessaria := 0;
          end
          else begin
            SetLength(vProducaoEstoque, Length(vProducaoEstoque) + 1);
            vProducaoEstoque[High(vProducaoEstoque)].produto_id := SFormatInt(sgProdutosProducao.Cells[coProdutoId, i]);
            vProducaoEstoque[High(vProducaoEstoque)].LocalId := locaisProduto[j].LocalId;
            vProducaoEstoque[High(vProducaoEstoque)].Quantidade := locaisProduto[j].Disponivel;
            vProducaoEstoque[High(vProducaoEstoque)].Natureza := 'S';
            quantidadeNecessaria := quantidadeNecessaria - locaisProduto[j].Disponivel;
          end;

          vProducaoEstoque[High(vProducaoEstoque)].PrecoUnitario := FItens[0].CustoCompraComercial;
        end;

      if quantidadeNecessaria = 0 then
        Break;
    end;

    if quantidadeNecessaria > 0 then begin
      Exclamar('O estoque dispon�vel do produto ' + sgProdutosProducao.Cells[coProdutoId, i] + ' � menor do que o necess�rio para a produ��o!');
      Abort;
    end;
  end;

 vRetornoBanco :=
    _ProducaoEstoque.AtualizarProducaoEstoque(
      Sessao.getConexaoBanco,
      eID.AsInt,
      Sessao.getEmpresaLogada.EmpresaId,
      vProducaoEstoque
    );

  if vRetornoBanco.TeveErro then begin
    Exclamar(vRetornoBanco.MensagemErro);
    Abort;
  end;

  _Biblioteca.Informar('Nova produ��o de estoque efetuada com sucesso, c�digo: ' + NFormat(vRetornoBanco.AsInt));

  if _Biblioteca.Perguntar('Deseja emitir as notas fiscais geradas?') then begin
    for i := Low(vRetornoBanco.AsArrayInt) to High(vRetornoBanco.AsArrayInt) do begin
      if not _ComunicacaoNFE.EnviarNFe(vRetornoBanco.AsArrayInt[i]) then begin
        Exclamar(
          'Ocorreu um problema ao tentar emitir a nota fiscal eletr�nica automaticamente!' + Chr(13) + Chr(10) +
          'Fa�a a emiss�o pela tela "Rela��o de notas fiscais"'
        );

        Break;
      end;
    end;
  end;
end;

procedure TFormMapaProducao.Modo(pEditando: Boolean);
begin
  inherited;
   _Biblioteca.Habilitar([
    FrProduto,
    FrLocalProduto,
    eQuantidade,
    sgProdutosProducao],
    pEditando
  );
end;

procedure TFormMapaProducao.AddProdutoGridKit(pProdutoId: Integer; pNomeProduto: string; pQuantidade: Double; pParticipacao: Double; pQuantidadeDisponivel: Double);
var
  vLinha: Integer;
  vEstoques: TArray<RecEstoque>;
begin
  if sgProdutosProducao.Cells[coProdutoId, 1] = '' then
    vLinha := 1
  else
    vLinha := sgProdutosProducao.RowCount;

  sgProdutosProducao.Cells[coProdutoId, vLinha]    := NFormat(pProdutoId);
  sgProdutosProducao.Cells[coNomeProduto, vLinha]  := pNomeProduto;
  sgProdutosProducao.Cells[coQtdeNecessaria, vLinha]   := NFormat(pQuantidade, 4);
  sgProdutosProducao.Cells[coQtdeDisponivel, vLinha] := NFormat(pQuantidadeDisponivel, 4);

  sgProdutosProducao.RowCount := IfThen(vLinha > 1, vLinha + 1, 2);
end;


procedure TFormMapaProducao.BuscarRegistro;
var
  vProducao: TArray<RecProducaoEstoque>;
  vProducaoItens: TArray<RecProducaoEstoqueItens>;
begin
  vProducao := _ProducaoEstoque.BuscarProducaoEstoque(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if vProducao = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    eID.SetFocus;
    Exit;
  end;

  vProducaoItens := _ProducaoEstoqueItens.BuscarProducaoEstoqueItens(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if vProducaoItens = nil then begin
    _Biblioteca.Exclamar('Produtos da produ��o n�o encontrado!');
    eID.SetFocus;
    Exit;
  end;

  inherited;
  PreencherRegistro(vProducao[0], vProducaoItens);
end;

procedure TFormMapaProducao.eIDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  FrProduto.SetFocus;
end;

procedure TFormMapaProducao.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _ProducaoEstoque.ExcluirProducaoEstoque(Sessao.getConexaoBanco, eID.AsInt);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormMapaProducao.FormCreate(Sender: TObject);
begin
  inherited;
  FrProduto.OnAposDeletar := FrProdutoOnAposDeletar;
  FrProduto.OnAposPesquisar := FrProdutoOnAposPesquisar;
end;

procedure TFormMapaProducao.FrProdutoOnAposDeletar(Sender: TObject);
begin
  sgProdutosProducao.ClearGrid();
end;

procedure TFormMapaProducao.PesquisarRegistro;
begin
  inherited;
  //
end;

procedure TFormMapaProducao.PreencherRegistro(pProducao: RecProducaoEstoque; pProducaoItens: TArray<RecProducaoEstoqueItens>);
var
  i: Integer;
begin
  eID.AsInt := pProducao.producao_estoque_id;

  for i := Low(pProducaoItens) to High(pProducaoItens) do begin
    if pProducaoItens[i].Natureza = 'E' then begin
      FrProduto.InserirDadoPorChave(pProducaoItens[i].Produto_id);
      FrLocalProduto.InserirDadoPorChave(pProducaoItens[i].LocalId);
      eQuantidade.AsDouble := pProducaoItens[i].Quantidade;
    end;
  end;

  Informar('N�o � poss�vel editar ou excluir esta produ��o.');
  sbGravar.Enabled := False;
  sgProdutosProducao.Enabled := False;
  sbExcluir.Enabled := False;
end;

procedure TFormMapaProducao.sgProdutosProducaoDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coProdutoId, coQtdeNecessaria, coQtdeDisponivel] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgProdutosProducao.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormMapaProducao.VerificarRegistro(Sender: TObject);
var
  i: Integer;
  quantidadeNecessaria: Double;
  quantidadeDisponivel: Double;
begin
  inherited;
  if eQuantidade.AsDouble = 0 then begin
    Exclamar('A quantidade a produzir precisa ser informada!');
    eQuantidade.SetFocus;
    Abort;
  end;

  for i := 1 to sgProdutosProducao.RowCount -1 do begin
    quantidadeNecessaria := SFormatDouble(sgProdutosProducao.Cells[coQtdeNecessaria, i]) * eQuantidade.AsDouble;
    quantidadeDisponivel := SFormatDouble(sgProdutosProducao.Cells[coQtdeDisponivel, i]);

    if quantidadeNecessaria > quantidadeDisponivel then begin
      Exclamar('O estoque dispon�vel do produto ' + sgProdutosProducao.Cells[coProdutoId, i] + ' - ' + sgProdutosProducao.Cells[coNomeProduto, i] + ' � menor do que o estoque necess�rio!');
      eQuantidade.SetFocus;
      Abort;
    end;
  end;
end;

end.
