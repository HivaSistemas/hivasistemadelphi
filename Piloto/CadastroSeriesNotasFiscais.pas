unit CadastroSeriesNotasFiscais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastro, Vcl.Buttons, _Biblioteca, _SeriesNotasFiscais,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, Frame.HerancaInsercaoExclusao, _Sessao,
  FrameSeriesNotasFiscais, _RecordsEspeciais;

type
  TFormCadastrosSeriesNotasFiscais = class(TFormHerancaCadastro)
    FrSeriesNotasFiscais: TFrSeriesNotasFiscais;
    procedure FormCreate(Sender: TObject);
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastrosSeriesNotasFiscais }

procedure TFormCadastrosSeriesNotasFiscais.FormCreate(Sender: TObject);
begin
  inherited;
  Modo(True);
  FrSeriesNotasFiscais.Series := _SeriesNotasFiscais.BuscarSeriesNotas(Sessao.getConexaoBanco, 0, []);
end;

procedure TFormCadastrosSeriesNotasFiscais.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _SeriesNotasFiscais.AtualizarSeriesNotas(Sessao.getConexaoBanco, FrSeriesNotasFiscais.Series);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  _Biblioteca.RotinaSucesso;
  Abort; // N�o fazer mais nada daqui pra frente se tudo deu certo
end;

procedure TFormCadastrosSeriesNotasFiscais.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([FrSeriesNotasFiscais], pEditando);
end;

procedure TFormCadastrosSeriesNotasFiscais.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if FrSeriesNotasFiscais.EstaVazio then begin
    _Biblioteca.Exclamar('Nenhuma s�rie foi informada, verifique!');
    SetarFoco(FrSeriesNotasFiscais);
    Abort;
  end;
end;

end.
