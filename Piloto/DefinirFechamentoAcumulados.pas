unit DefinirFechamentoAcumulados;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Biblioteca, _Sessao,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _FrameHerancaPrincipal, _OrcamentosItens, _RecordsOrcamentosVendas,
  _FrameHenrancaPesquisas, FrameCondicoesPagamento, Vcl.ComCtrls, Vcl.StdCtrls, _ContasPagar,
  FrameFechamento, FrameFechamentoJuros, StaticTextLuka, _Acumulados, _RecordsEspeciais, _AcumuladosItens,
  FrameEndereco, _RecordsCadastros, _Cadastros, FrameIndicesDescontosVenda, _ContasReceberBaixas,
  _ClientesCondicPagtoRestrit;

type
  RecInfoPedidos = record
    OrcamentoId: Integer;
    ValorLiquido: Double;
  end;

  TFormDefinirFechamentoAcumulados = class(TFormHerancaFinalizar)
    pcInfo: TPageControl;
    tsProdutos: TTabSheet;
    tsFechamento: TTabSheet;
    FrCondicoesPagamento: TFrCondicoesPagamento;
    sgItens: TGridLuka;
    eObservacoesNotaFiscalEletronica: TMemo;
    lb14: TLabel;
    FrFechamentoJuros: TFrFechamentoJuros;
    st5: TStaticText;
    st1: TStaticText;
    st4: TStaticText;
    st6: TStaticText;
    stValorReceber: TStaticTextLuka;
    stTotalProdutos: TStaticTextLuka;
    stTotalDesconto: TStaticTextLuka;
    stTotalOutrasDespesas: TStaticTextLuka;
    st3: TStaticText;
    stTotalAjuste: TStaticTextLuka;
    st7: TStaticText;
    stTotalFrete: TStaticTextLuka;
    FrEnderecoNota: TFrEndereco;
    st8: TStaticText;
    sbCarregarEnderecoPrincipalEntrega: TSpeedButton;
    sbCarregarEnderecos: TSpeedButton;
    sbCadastrarNovoEndereco: TSpeedButton;
    StaticTextLuka1: TStaticTextLuka;
    FrIndiceDescontoVenda: TFrIndicesDescontosVenda;
    Label1: TLabel;
    StaticText1: TStaticText;
    stTotalAdiantado: TStaticTextLuka;
    procedure FormShow(Sender: TObject);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sbCadastrarNovoEnderecoClick(Sender: TObject);
    procedure sbCarregarEnderecosClick(Sender: TObject);
    procedure sbCarregarEnderecoPrincipalEntregaClick(Sender: TObject);
  private
    FClienteId: Integer;
    FMaiorIndiceCondicaoPedidos: Double;
    FValorJurosCalculadosPedidos: Double;

    FTotalLiquidoPedidos: Double;
    FPedidosIds: TArray<Integer>;
    FValorOriginalPedidos: Double;
    FItens: TArray<RecOrcamentoItens>;

    procedure PreencherItens;
    procedure FrCondicoesPagamentoOnAposPesquisar(Sender: TObject);
    procedure FrCondicoesPagamentoOnAposDeletar(Sender: TObject);
    procedure AplicarIndiceAcrescimoCondicao;
    procedure CalcularValoresItens;
    procedure RatearValoresItens;
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Definir(
  pClienteId: Integer;
  pPedidosIds: TArray<Integer>;
  pTotalLiquidoFechar: Double;
  pTotalJuros: Double;
  pTotalAdiantado: Double;
  pInscricaoEstadual: string
): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

uses _DiversosEnderecos;

const
  coLegenda        = 0;
  coProdutoId      = 1;
  coNomeProduto    = 2;
  coMarca          = 3;
  coQuantidade     = 4;
  coUnidade        = 5;
  coPrecoUnitario  = 6;
  coTotalProduto   = 7;
  coOutrasDespesas = 8;
  coDesconto       = 9;
  coFrete          = 10;

  (* Ocultas *)
  coPrecoUnitarioBase  = 11;
  coDescontoProporc    = 12;
  coOutrasDespProporc  = 13;

function Definir(
  pClienteId: Integer;
  pPedidosIds: TArray<Integer>;
  pTotalLiquidoFechar: Double;
  pTotalJuros: Double;
  pTotalAdiantado: Double;
  pInscricaoEstadual: string
): TRetornoTelaFinalizar;
var
  vSql: string;
  vForm: TFormDefinirFechamentoAcumulados;
  vEnderecos: TArray<RecDiversosEnderecos>;
begin
  if pPedidosIds = nil then
    Exit;

  vForm := TFormDefinirFechamentoAcumulados.Create(nil);

  vForm.FrEnderecoNota.SomenteLeitura(True);
  vForm.FPedidosIds           := pPedidosIds;
  vForm.FTotalLiquidoPedidos  := pTotalLiquidoFechar;
  vForm.FClienteId            := pClienteId;
  vForm.FValorJurosCalculadosPedidos := pTotalJuros;
  vForm.FMaiorIndiceCondicaoPedidos  := 1;

  vForm.FrFechamentoJuros.eValorJuros.AsDouble := pTotalJuros;
  vForm.FrFechamentoJuros.CadastroId           := pClienteId;
  vForm.FrFechamentoJuros.TelaChamada          := ttAcumulado;
  vForm.FrFechamentoJuros.PedidosAcumuladosIds := pPedidosIds;

  vSql :=
    'where ' + FiltroInInt('ITE.ORCAMENTO_ID', pPedidosIds) +
    'and ( ' +
    '  ITE.QUANTIDADE - ' +
    '  ITE.QUANTIDADE_DEVOLV_ENTREGUES - ' +
    '  ITE.QUANTIDADE_DEVOLV_PENDENTES - ' +
    '  ITE.QUANTIDADE_DEVOLV_SEM_PREVISAO ' +
    ') > 0 ';

  VForm.stTotalAdiantado.Somar(pTotalAdiantado);
  vForm.FItens := _OrcamentosItens.BuscarOrcamentosItensComando( Sessao.getConexaoBanco, vSql );
  vForm.PreencherItens;

  vForm.FValorOriginalPedidos :=
    vForm.stTotalProdutos.AsDouble +
    vForm.stTotalAjuste.AsDouble -
    vForm.stTotalDesconto.AsDouble +
    vForm.stTotalOutrasDespesas.AsDouble +
    vForm.stTotalFrete.AsDouble;

  if pInscricaoEstadual <> '' then begin
    vForm.sbCarregarEnderecoPrincipalEntregaClick(nil);

    if RetornaNumeros(vForm.FrEnderecoNota.getInscricaoEstadual) <> RetornaNumeros(pInscricaoEstadual) then begin
      vEnderecos := _DiversosEnderecos.BuscarDiversosEnderecos(Sessao.getConexaoBanco, 1, [pClienteId, pInscricaoEstadual], True);
      if vEnderecos = nil then
        Exit;

      vForm.FrEnderecoNota.setLogradouro(vEnderecos[0].logradouro);
      vForm.FrEnderecoNota.setComplemento(vEnderecos[0].complemento);
      vForm.FrEnderecoNota.setNumero(vEnderecos[0].numero);
      vForm.FrEnderecoNota.setBairroId(vEnderecos[0].bairro_id);
      vForm.FrEnderecoNota.setPontoReferencia(vEnderecos[0].ponto_referencia);
      vForm.FrEnderecoNota.setCep(vEnderecos[0].cep);
      vForm.FrEnderecoNota.setInscricaoEstadual(vEnderecos[0].InscricaoEstadual);
    end;
  end;

  Result.Ok(vForm.ShowModal, True);
end;

procedure TFormDefinirFechamentoAcumulados.AplicarIndiceAcrescimoCondicao;
var
  i: Integer;
  vIndiceCondicao: Double;
begin
  vIndiceCondicao := 1;

  if (not FrCondicoesPagamento.EstaVazio) and (FrCondicoesPagamento.getDados.indice_acrescimo > FMaiorIndiceCondicaoPedidos) then
    vIndiceCondicao := FrCondicoesPagamento.getDados.indice_acrescimo / FMaiorIndiceCondicaoPedidos;

  for i := sgItens.FixedRows to sgItens.RowCount - 1 do
    sgItens.Cells[coPrecoUnitario, i] := NFormatN( Arredondar(SFormatDouble(sgItens.Cells[coPrecoUnitarioBase, i]) * vIndiceCondicao, 2) );

  CalcularValoresItens;
end;

procedure TFormDefinirFechamentoAcumulados.CalcularValoresItens;
var
  i: Integer;
begin
  _Biblioteca.LimparCampos([stTotalProdutos]);

  for i := sgItens.FixedRows to sgItens.RowCount - 1 do begin
    sgItens.Cells[coTotalProduto, i] := NFormatN(Arredondar(SFormatDouble(sgItens.Cells[coQuantidade, i]) * SFormatDouble(sgItens.Cells[coPrecoUnitario, i]), 2));

    stTotalProdutos.Somar( SFormatDouble(sgItens.Cells[coTotalProduto, i]) );
  end;

  stValorReceber.AsDouble :=
    stTotalProdutos.AsDouble +
    stTotalAjuste.AsCurr +
    stTotalOutrasDespesas.AsDouble -
    stTotalDesconto.AsDouble +
    stTotalFrete.AsDouble;

  FrFechamentoJuros.setTotalProdutos( stValorReceber.AsDouble );
  FrFechamentoJuros.TotalizarValorASerPago;
end;

procedure TFormDefinirFechamentoAcumulados.Finalizar(Sender: TObject);
var
  i: Integer;
  vAcumuladoId: Integer;
  vIndicePedidos: Double;
  vRetBanco: RecRetornoBD;
  vIndiceDescontoVendaId: Integer;
  vItens: TArray<RecAcumuladosItens>;

  vTotalDesconto: Currency;
  vTotalOutrasDesp: Currency;
begin
  vItens := nil;

  vIndiceDescontoVendaId := 0;
  if not FrIndiceDescontoVenda.EstaVazio then
    vIndiceDescontoVendaId := FrIndiceDescontoVenda.getDados().IndiceId;

  // Verificando se tem produto para realizar o fechamento
  if sgItens.Cells[coProdutoId, 1] <> '' then begin
    for i := 1 to sgItens.RowCount -1 do begin
      SetLength(vItens, Length(vItens) + 1);

      vItens[High(vItens)].ProdutoId                := SFormatInt(sgItens.Cells[coProdutoId, i]);
      vItens[High(vItens)].PrecoUnitario            := SFormatDouble(sgItens.Cells[coPrecoUnitario, i]);
      vItens[High(vItens)].Quantidade               := SFormatDouble(sgItens.Cells[coQuantidade, i]);
      vItens[High(vItens)].ValorTotal               := SFormatDouble(sgItens.Cells[coTotalProduto, i]);
      vItens[High(vItens)].ValorTotalDesconto       := SFormatDouble(sgItens.Cells[coDesconto, i]) + SFormatDouble(sgItens.Cells[coDescontoProporc, i]);
      vItens[High(vItens)].ValorTotalOutrasDespesas := SFormatDouble(sgItens.Cells[coOutrasDespesas, i]) + SFormatDouble(sgItens.Cells[coOutrasDespProporc, i]);;
      vItens[High(vItens)].ValorTotalFrete          := SFormatDouble(sgItens.Cells[coFrete, i]);

      vItens[High(vItens)].ValorImpostos   := 0;
      vItens[High(vItens)].ValorEncargos   := 0;
      vItens[High(vItens)].ValorCustoVenda := 0;
      vItens[High(vItens)].ValorCustoFinal := 0;
    end;
  end;

  vIndicePedidos := FrFechamentoJuros.eValorTotalASerPago.AsCurr / (FTotalLiquidoPedidos);

  vTotalDesconto   := stTotalDesconto.AsCurr + FrFechamentoJuros.eValorDesconto.AsCurr;
  vTotalOutrasDesp := stTotalOutrasDespesas.AsDouble + FrFechamentoJuros.eValorOutrasDespesas.AsCurr;

  if stTotalAjuste.AsCurr > 0 then
    vTotalOutrasDesp := vTotalOutrasDesp + stTotalAjuste.AsCurr
  else
    vTotalDesconto := vTotalDesconto - stTotalAjuste.AsCurr;

  Sessao.getConexaoBanco.IniciarTransacao;

  vRetBanco :=
    _Acumulados.AtualizarAcumulados(
      Sessao.getConexaoBanco,
      0,
      Sessao.getEmpresaLogada.EmpresaId,
      FClienteId,
      FrCondicoesPagamento.getDados().condicao_id,
      vIndiceDescontoVendaId,
      'AR',
      stTotalProdutos.AsDouble,
      FrFechamentoJuros.eValorTotalASerPago.AsCurr,
      vTotalOutrasDesp,
      vTotalDesconto,
      stTotalFrete.AsDouble,
      FrFechamentoJuros.eValorJuros.AsCurr,
      vIndicePedidos,
      _Biblioteca.ToChar(sgItens.Cells[coProdutoId, 1] <> ''),
      FrFechamentoJuros.eValorDinheiro.AsCurr,
      FrFechamentoJuros.eValorCheque.AsCurr,
      FrFechamentoJuros.eValorCartaoDebito.AsCurr,
      FrFechamentoJuros.eValorCartaoCredito.AsCurr,
      FrFechamentoJuros.eValorCobranca.AsCurr,
      FrFechamentoJuros.eValorFinanceira.AsCurr,
      FrFechamentoJuros.eValorCredito.AsCurr,
      FrFechamentoJuros.eValorPix.AsCurr,
      0,
      eObservacoesNotaFiscalEletronica.Lines.Text,
      FrEnderecoNota.getLogradouro,
      FrEnderecoNota.getComplemento,
      FrEnderecoNota.getNumero,
      FrEnderecoNota.getPontoReferencia,
      FrEnderecoNota.getBairroId,
      FrEnderecoNota.getCep,
      FrEnderecoNota.getInscricaoEstadual,
      vItens,
      FrFechamentoJuros.Creditos,
      FPedidosIds,
      True
    );

  Sessao.AbortarSeHouveErro( vRetBanco );

  vAcumuladoId := vRetBanco.AsInt;

  // Atualizando os titulos a serem gerados no financeiro
  vRetBanco :=
    _Acumulados.AtualizarAcumuladoPagamentos(
      Sessao.getConexaoBanco,
      vAcumuladoId,
      FrFechamentoJuros.Cheques,
      FrFechamentoJuros.Cartoes,
      FrFechamentoJuros.Cobrancas,
      nil,
      True
    );

  Sessao.AbortarSeHouveErro( vRetBanco );

  Sessao.getConexaoBanco.FinalizarTransacao;

  _Biblioteca.Informar('Fechamento de acumulado realizado com sucesso. C�digo: ' + NFormat(vAcumuladoId));
  inherited;
end;

procedure TFormDefinirFechamentoAcumulados.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrCondicoesPagamento);
  pcInfo.ActivePageIndex := 0;
  FrCondicoesPagamento.FechamentoAcumulado := True;
  FrCondicoesPagamento.FSomenteEmpresaLogada := True;

  FrCondicoesPagamento.OnAposPesquisar := FrCondicoesPagamentoOnAposPesquisar;
  FrCondicoesPagamento.OnAposDeletar := FrCondicoesPagamentoOnAposDeletar;

  FrIndiceDescontoVenda.Visible := Sessao.AutorizadoRotina('definir_indice_desconto_acumulado');
end;

procedure TFormDefinirFechamentoAcumulados.FrCondicoesPagamentoOnAposDeletar(Sender: TObject);
begin
  FrFechamentoJuros.LimparDadosTiposCobranca(True);
  AplicarIndiceAcrescimoCondicao;
end;

procedure TFormDefinirFechamentoAcumulados.FrCondicoesPagamentoOnAposPesquisar(Sender: TObject);
begin
  if
    (FrCondicoesPagamento.getDados().Restrita = 'S') and
    not _ClientesCondicPagtoRestrit.CondicaoRestritaLiberada(
      Sessao.getConexaoBanco,
      FrCondicoesPagamento.getDados().condicao_id,
      FClienteId
    )
  then begin
    _Biblioteca.Exclamar('N�o � permitido utilizar esta condi��o de pagamento restrita para este cliente!');
    FrCondicoesPagamento.Clear;
    SetarFoco(FrCondicoesPagamento);
    Abort;
  end;

  FrFechamentoJuros.setCondicaoPagamento(
    FrCondicoesPagamento.getDados.condicao_id,
    FrCondicoesPagamento.getDados.nome,
    FrCondicoesPagamento.getDados.PrazoMedio
  );

  AplicarIndiceAcrescimoCondicao;
end;

procedure TFormDefinirFechamentoAcumulados.PreencherItens;
var
  i: Integer;
  j: Integer;

  vTotal: Currency;
  vTotalOriginal: Currency;
  vPrecoUnitario: Double;

  vAchou: Boolean;
  vItensAuxiliar: TArray<RecOrcamentoItens>;
begin
  vItensAuxiliar := nil;

  // Agrupando os produtos que s�o iguais
  for i := Low(FItens) to High(FItens) do begin
    vAchou := False;
    for j := Low(vItensAuxiliar) to High(vItensAuxiliar) do begin
      if FItens[i].produto_id = vItensAuxiliar[j].produto_id then begin
        {$REGION 'REALIZANDO MEDIA PONDERADA DO PRE�O'}
          vTotalOriginal := (FItens[i].preco_unitario * FItens[i].SaldoRestanteAcumulado) + (vItensAuxiliar[j].preco_unitario * vItensAuxiliar[j].SaldoRestanteAcumulado);

          vPrecoUnitario := _Biblioteca.Arredondar(vTotalOriginal / (FItens[i].SaldoRestanteAcumulado + vItensAuxiliar[j].SaldoRestanteAcumulado), 2);
          vItensAuxiliar[j].preco_unitario         := vPrecoUnitario;
          vItensAuxiliar[j].SaldoRestanteAcumulado := vItensAuxiliar[j].SaldoRestanteAcumulado + FItens[i].SaldoRestanteAcumulado;

          vTotal := vPrecoUnitario * vItensAuxiliar[j].SaldoRestanteAcumulado;
          vItensAuxiliar[j].valor_total := vTotal;

          if vTotalOriginal <> vTotal then
            stTotalAjuste.AsDouble := stTotalAjuste.AsDouble + (vTotalOriginal - vTotal);
        {$ENDREGION}

        if FItens[i].valor_total_outras_despesas > 0 then begin
          vItensAuxiliar[j].valor_total_outras_despesas :=
            vItensAuxiliar[j].valor_total_outras_despesas + (Arredondar(FItens[i].valor_total_outras_despesas / FItens[i].quantidade * FItens[i].SaldoRestanteAcumulado, 2));
        end;

        if FItens[i].valor_total_desconto > 0 then begin
          vItensAuxiliar[j].valor_total_desconto :=
            vItensAuxiliar[j].valor_total_desconto + (Arredondar(FItens[i].valor_total_desconto / FItens[i].quantidade * FItens[i].SaldoRestanteAcumulado, 2));
        end;

        if FItens[i].ValorTotalFrete > 0 then begin
          vItensAuxiliar[j].ValorTotalFrete :=
            vItensAuxiliar[j].ValorTotalFrete + (Arredondar(FItens[i].ValorTotalFrete / FItens[i].quantidade * FItens[i].SaldoRestanteAcumulado, 2));
        end;

        vAchou := True;
        Break;
      end;
    end;

    // Se encontrou o produto j� fez a m�dia ponderada, ent�o passando para o pr�ximo produto
    if vAchou then
      Continue;

    SetLength(vItensAuxiliar, Length(vItensAuxiliar) + 1);
    vItensAuxiliar[High(vItensAuxiliar)] := FItens[i];

    vItensAuxiliar[High(vItensAuxiliar)].valor_total_outras_despesas := 0;
    vItensAuxiliar[High(vItensAuxiliar)].valor_total_desconto        := 0;
    vItensAuxiliar[High(vItensAuxiliar)].ValorTotalFrete             := 0;

    if FItens[i].valor_total_outras_despesas > 0 then
      vItensAuxiliar[High(vItensAuxiliar)].valor_total_outras_despesas := Arredondar(FItens[i].valor_total_outras_despesas / FItens[i].quantidade * FItens[i].SaldoRestanteAcumulado, 2);

    if FItens[i].valor_total_desconto > 0 then
      vItensAuxiliar[High(vItensAuxiliar)].valor_total_desconto := Arredondar(FItens[i].valor_total_desconto / FItens[i].quantidade * FItens[i].SaldoRestanteAcumulado, 2);

    if FItens[i].ValorTotalFrete > 0 then
      vItensAuxiliar[High(vItensAuxiliar)].ValorTotalFrete := Arredondar(FItens[i].ValorTotalFrete / FItens[i].quantidade * FItens[i].SaldoRestanteAcumulado, 2);

    if FItens[i].IndiceCondicaoPagamento > FMaiorIndiceCondicaoPedidos then
      FMaiorIndiceCondicaoPedidos := vItensAuxiliar[i].IndiceCondicaoPagamento;
  end;

  FItens := vItensAuxiliar;

  for i := Low(FItens) to High(FItens) do begin
    sgItens.Cells[coProdutoId, i + 1]          := NFormat(FItens[i].produto_id);
    sgItens.Cells[coNomeProduto, i + 1]        := FItens[i].nome;
    sgItens.Cells[coMarca, i + 1]              := FItens[i].nome_marca;
    sgItens.Cells[coQuantidade, i + 1]         := NFormatNEstoque(FItens[i].SaldoRestanteAcumulado);
    sgItens.Cells[coUnidade, i + 1]            := FItens[i].unidade_venda;
    sgItens.Cells[coPrecoUnitario, i + 1]      := NFormat(FItens[i].preco_unitario);
    sgItens.Cells[coTotalProduto, i + 1]       := NFormat(FItens[i].valor_total);
    sgItens.Cells[coOutrasDespesas, i + 1]     := NFormatN(FItens[i].valor_total_outras_despesas);
    sgItens.Cells[coDesconto, i + 1]           := NFormatN(FItens[i].valor_total_desconto);
    sgItens.Cells[coFrete, i + 1]              := NFormatN(FItens[i].ValorTotalFrete);
    sgItens.Cells[coPrecoUnitarioBase, i + 1]  := NFormat(FItens[i].preco_unitario);

    stTotalOutrasDespesas.Somar( FItens[i].valor_total_outras_despesas );
    stTotalDesconto.Somar( FItens[i].valor_total_desconto );
    stTotalFrete.Somar( FItens[i].ValorTotalFrete );
  end;
  sgItens.SetLinhasGridPorTamanhoVetor( Length(FItens) );

  CalcularValoresItens;

  FrFechamentoJuros.eValorJuros.AsCurr      := FValorJurosCalculadosPedidos;
  FrFechamentoJuros.eValorJurosChange(nil);
end;

procedure TFormDefinirFechamentoAcumulados.sbCadastrarNovoEnderecoClick(Sender: TObject);
begin
  inherited;
  FrEnderecoNota.CadastrarNovoEndereco(FClienteId);
end;

procedure TFormDefinirFechamentoAcumulados.sbCarregarEnderecoPrincipalEntregaClick(Sender: TObject);
var
  vCadastro: TArray<RecCadastros>;
begin
  inherited;
  vCadastro := _Cadastros.BuscarCadastros(Sessao.getConexaoBanco, 0, [FClienteId]);

  FrEnderecoNota.setLogradouro(vCadastro[0].logradouro);
  FrEnderecoNota.setComplemento(vCadastro[0].complemento);
  FrEnderecoNota.setNumero(vCadastro[0].numero);
  FrEnderecoNota.setBairroId(vCadastro[0].bairro_id);
  FrEnderecoNota.setPontoReferencia(vCadastro[0].ponto_referencia);
  FrEnderecoNota.setCep(vCadastro[0].cep);
  FrEnderecoNota.setInscricaoEstadual(vCadastro[0].inscricao_estadual);

  _Biblioteca.Destruir(TArray<TObject>(vCadastro));
end;

procedure TFormDefinirFechamentoAcumulados.sbCarregarEnderecosClick(Sender: TObject);
begin
  inherited;
  FrEnderecoNota.CarregarEnderecoCliente(FClienteId, True);
end;

procedure TFormDefinirFechamentoAcumulados.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    coProdutoId,
    coQuantidade,
    coPrecoUnitario,
    coTotalProduto,
    coFrete,
    coOutrasDespesas,
    coDesconto]
  then
    vAlinhamento := taRightJustify
  else if ACol = coLegenda then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormDefinirFechamentoAcumulados.VerificarRegistro(Sender: TObject);
begin
  inherited;
  CalcularValoresItens;
  RatearValoresItens;

  if FrCondicoesPagamento.EstaVazio then begin
    _Biblioteca.Exclamar('A condi��o de pagamento n�o foi informada corretamente, verifique!');
    SetarFoco(FrCondicoesPagamento);
    Abort;
  end;

  if FrFechamentoJuros.eValorTroco.AsCurr < 0 then begin
    _Biblioteca.Exclamar('Existe diferen�a no valor a ser pago e as formas de pagamentos definidas!');
    SetarFoco(FrFechamentoJuros.eValorDinheiro);
    Abort;
  end;

  if FrFechamentoJuros.getFormasPagamentoMaiorTotalPagarEUtilizandoCredito then begin
    _Biblioteca.Exclamar('As formas de pagamento j� definidas � maior ou igual ao valor a ser pago pelo pedido, n�o ser� poss�vel utilizar cr�ditos!');
    SetarFoco(FrFechamentoJuros.eValorCredito);
    Abort;
  end;

  if FrFechamentoJuros.getCreditoMaiorTotalPagarETemFormasPagamento then begin
    _Biblioteca.Exclamar('Os cr�ditos que est�o sendo utilizados j� cobrem o valor do pedido, n�o ser� poss�vel utilizar outras formas de pagamento!');
    SetarFoco(FrFechamentoJuros.eValorDinheiro);
    Abort;
  end;

  if FrFechamentoJuros.eValorCredito.AsCurr > 0 then begin
    if _ContasPagar.getTemCreditoIndiceMaiorCondicao( Sessao.getConexaoBanco, FrCondicoesPagamento.getDados.indice_acrescimo, FrFechamentoJuros.Creditos ) then begin
      _Biblioteca.Exclamar('O �ndice do cr�dito utilizado e maior que o �ndice da condi��o de pagamento atual! utilize uma condi��o de pagamento que tenha um �ndice igual ou maior que o �ndice do cr�dito!');
      SetarFoco(FrFechamentoJuros.eValorCredito);
      Abort;
    end;
  end;

  if _ContasReceberBaixas.ExisteBaixaPendenteAdiantamentoAcumulado(Sessao.getConexaoBanco, FPedidosIds) then begin
    _Biblioteca.Exclamar('Existem baixas para os pedidos selecionados que est�o no caixa e ainda n�o foram recebidas, por favor finalize o recebimento!');
    SetarFoco(FrCondicoesPagamento);
    Abort;
  end;

  FrEnderecoNota.VerificarDados;
end;

(* Procedimento criado para ratear os valores *)
(* de outras despesas, desconto e frete *)
procedure TFormDefinirFechamentoAcumulados.RatearValoresItens;
var
  i: Integer;
  vIndiceRateio: Double;
  vIndiceRateioDesc: Double;

  vTotalDesconto: Currency;
  vTotalOutDespesas: Currency;
  vValorFaltaRatearDesconto: Currency;
  vValorFaltaRatearOutDespesas: Currency;

  vLinhaItemMaisSignificativo: Integer;
  vValorItemMaisSignificativo: Currency;
begin
  vLinhaItemMaisSignificativo  := -1;
  vValorItemMaisSignificativo  := 0;

  vTotalDesconto    := FrFechamentoJuros.eValorDesconto.AsDouble;
  vTotalOutDespesas := FrFechamentoJuros.eValorOutrasDespesas.AsDouble + FrFechamentoJuros.eValorJuros.AsDouble;

  if stTotalAjuste.AsCurr > 0 then
    vTotalOutDespesas := vTotalOutDespesas + stTotalAjuste.AsCurr
  else
    vTotalDesconto := vTotalDesconto + Abs(stTotalAjuste.AsCurr);

  vValorFaltaRatearDesconto    := vTotalDesconto;
  vValorFaltaRatearOutDespesas := vTotalOutDespesas;

  for i := sgItens.FixedRows to sgItens.RowCount - 1 do begin
    sgItens.Cells[coDescontoProporc, i]   := '';
    sgItens.Cells[coOutrasDespProporc, i] := '';
  end;

  for i := sgItens.FixedRows to sgItens.RowCount - 1 do begin
    if sgItens.Cells[coNomeProduto, i] = '' then
     Continue;

    vIndiceRateio := _Biblioteca.SFormatDouble(sgItens.Cells[coTotalProduto, i]) / SFormatDouble(stTotalProdutos.Caption);

    if _Biblioteca.SFormatCurr(sgItens.Cells[coTotalProduto, i]) > vValorItemMaisSignificativo then begin
      vLinhaItemMaisSignificativo := i;
      vValorItemMaisSignificativo := _Biblioteca.SFormatCurr(sgItens.Cells[coTotalProduto, i]);
    end;

    vIndiceRateioDesc := vIndiceRateio;

    if vValorFaltaRatearDesconto > 0 then begin
      sgItens.Cells[coDescontoProporc, i] := NFormat(_Biblioteca.Arredondar(vTotalDesconto * vIndiceRateioDesc, 2));
      vValorFaltaRatearDesconto := vValorFaltaRatearDesconto - SFormatDouble(sgItens.Cells[coDescontoProporc, i]);

      if SFormatCurr(sgItens.Cells[coDescontoProporc, i]) >= SFormatCurr(sgItens.Cells[coTotalProduto, i]) then begin
        _Biblioteca.Exclamar('O valor total do desconto n�o pode ser maior ou igual ao valor dos produtos sem promo��o!');
        Abort;
      end;
    end;

    if vValorFaltaRatearOutDespesas > 0 then begin
      sgItens.Cells[coOutrasDespProporc, i] := NFormat(_Biblioteca.Arredondar(vTotalOutDespesas * vIndiceRateio, 2));
      vValorFaltaRatearOutDespesas := vValorFaltaRatearOutDespesas - SFormatDouble(sgItens.Cells[coOutrasDespProporc, i]);
    end;
  end;

  if NFormatN(vValorFaltaRatearDesconto) <> '' then begin
    if vLinhaItemMaisSignificativo > -1 then begin
      sgItens.Cells[coDescontoProporc, vLinhaItemMaisSignificativo] := NFormat(SFormatDouble(sgItens.Cells[coDescontoProporc, vLinhaItemMaisSignificativo]) + vValorFaltaRatearDesconto);
      vValorFaltaRatearDesconto := 0;
    end;
  end;

  if NFormatN(vValorFaltaRatearOutDespesas) <> '' then begin
    if vLinhaItemMaisSignificativo > -1 then
      sgItens.Cells[coOutrasDespProporc, vLinhaItemMaisSignificativo] := NFormat(SFormatDouble(sgItens.Cells[coOutrasDespProporc, vLinhaItemMaisSignificativo]) + vValorFaltaRatearOutDespesas);
  end;

  if NFormatN(vValorFaltaRatearDesconto) <> '' then begin
    _Biblioteca.Exclamar('N�o foi poss�vel ratear o desconto em todos os produtos, por favor verifique!');
    Abort;
  end;
end;

end.
