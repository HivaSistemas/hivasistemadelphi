unit BuscarDados.TitulosFechamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Grids, GridLuka, _RecordsCaixa, _Sessao, System.Math, System.StrUtils, _Biblioteca, _ContasReceber,
  _RecordsFinanceiros, StaticTextLuka;

type
  TTipoBusca = (ttCartoes, ttCobrancas, ttCheques);

  RecFechamento = record
    receber_id: Integer;
    tipo: TTipoBusca;
    valor: Double;
    orcamento_id: Integer;
    BaixaOrigemId: Integer;
    //nsu_cartao: string;
    ItemIdCrtOrcamento: Integer;
  end;

  TFormBuscarDadosTitulosFechamento = class(TFormHerancaFinalizar)
    sgCartoes: TGridLuka;
    sgCobrancas: TGridLuka;
    sgCheques: TGridLuka;
    stTipoTitulos: TStaticTextLuka;
    procedure sgCartoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgChequesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgCobrancasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure GridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GridGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    FTipo: TTipoBusca;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function BuscarTitulosFechamento(pTipo: TTipoBusca; pTitulos: TArray<RecFechamento>; pTurnoId: Integer): TRetornoTelaFinalizar<TArray<RecFechamento>>;

implementation

{$R *.dfm}

const
  coSelecionado     = 0; // Igual para todos os grids

// Grid de cart�es
  ccNumeroTransacao    = 1;
  ccCliente            = 2;
  ccValor              = 3;
  ccCobranca           = 4;
  ccOrcamentoId        = 5;
  coBaixaOrigemId      = 6;
  ccReceberId          = 7;
  coItemIdCrtOrcamento = 8;

// Grid de cobran�as
  coDocumento   = 1;
  coCliente     = 2;
  coValor       = 3;
  coVencimento  = 4;
  coReceberId   = 5;

// Grid de cheques
  ceNumeroCheque = 1;
  ceCliente      = 2;
  ceValor        = 3;
  ceBanco        = 4;
  ceAgencia      = 5;
  ceConta        = 6;
  ceVencimento   = 7;
  ceEmitente     = 8;
  ceTelefoneEmit = 9;
  ceReceberId    = 10;

function BuscarTitulosFechamento(pTipo: TTipoBusca; pTitulos: TArray<RecFechamento>; pTurnoId: Integer): TRetornoTelaFinalizar<TArray<RecFechamento>>;
var
  i: Integer;
  vCartoes: TArray<RecCartoesTurno>;
  vChequesCobrancas: TArray<RecContasReceber>;
  vForm: TFormBuscarDadosTitulosFechamento;

  function VerificarJaTinhaSidoSelecionado(pOrcamentoId: Integer; pItemId: Integer; pReceberId: Integer): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    for i := Low(pTitulos) to High(pTitulos) do begin
      if pTipo = ttCartoes then begin
        if (pTitulos[i].orcamento_id = pOrcamentoId) and (pTitulos[i].ItemIdCrtOrcamento = pItemId) then begin
          Result := True;
          Break;
        end;
      end
      else begin
        if (pTitulos[i].receber_id = pReceberId) then begin
          Result := True;
          Break;
        end;
      end;
    end;
  end;

begin
  Result.Dados := nil;
  vForm := TFormBuscarDadosTitulosFechamento.Create(Application);

  vForm.FTipo := pTipo;

  if pTipo = ttCartoes then begin
    vForm.stTipoTitulos.Caption := 'Cart�es';
    vForm.sgCartoes.Visible := (pTipo = ttCartoes);
    vForm.sgCartoes.Align := alClient;
  end
  else if pTipo = ttCobrancas then begin
    vForm.stTipoTitulos.Caption := 'Cobran�as';
    vForm.sgCobrancas.Visible := (pTipo = ttCobrancas);
    vForm.sgCobrancas.Align := alClient;
  end
  else begin
    vForm.stTipoTitulos.Caption := 'Cheques';
    vForm.sgCheques.Visible := (pTipo = ttCheques);
    vForm.sgCheques.Align := alClient;
  end;

  if pTipo = ttCartoes then begin
    vCartoes := _ContasReceber.BuscarCartoesTurno(Sessao.getConexaoBanco, pTurnoId);
    for i := Low(vCartoes) to High(vCartoes) do begin
      vForm.sgCartoes.Cells[ccNumeroTransacao, i + 1]    := vCartoes[i].nsu;
      vForm.sgCartoes.Cells[ccCliente, i + 1]            := NFormat(vCartoes[i].CadastroId) + ' - ' + vCartoes[i].nome_fantasia;
      vForm.sgCartoes.Cells[ccValor, i + 1]              := NFormat(vCartoes[i].valor);
      vForm.sgCartoes.Cells[ccCobranca, i + 1]           := NFormat(vCartoes[i].cobranca_id) + ' - ' + vCartoes[i].nome_cobranca;
      vForm.sgCartoes.Cells[ccOrcamentoId, i + 1]        := NFormat(vCartoes[i].orcamento_id);
      vForm.sgCartoes.Cells[coBaixaOrigemId, i + 1]      := NFormat(vCartoes[i].BaixaOrigemId);
      vForm.sgCartoes.Cells[coItemIdCrtOrcamento, i + 1] := NFormat(vCartoes[i].itemIdCrtOrcamento);

      if VerificarJaTinhaSidoSelecionado(vCartoes[i].orcamento_id, vCartoes[i].itemIdCrtOrcamento, 0) then
        vForm.sgCartoes.Cells[coSelecionado, i + 1]     := 'Sim'
      else
        vForm.sgCartoes.Cells[coSelecionado, i + 1]     := 'N�o';
    end;
    vForm.sgCartoes.RowCount := IfThen(Length(vCartoes) > 1, High(vCartoes) + 2, 2);
  end
  else if pTipo = ttCheques then begin
    vChequesCobrancas := _ContasReceber.BuscarContasReceber(Sessao.getConexaoBanco, 1, [pTurnoId]);
    for i := Low(vChequesCobrancas) to High(vChequesCobrancas) do begin
      vForm.sgCheques.Cells[ceNumeroCheque, i + 1] := NFormat(vChequesCobrancas[i].numero_cheque);
      vForm.sgCheques.Cells[ceCliente, i + 1]      := NFormat(vChequesCobrancas[i].CadastroId) + ' - ' + vChequesCobrancas[i].NomeCliente;
      vForm.sgCheques.Cells[ceValor, i + 1]        := NFormat(vChequesCobrancas[i].ValorDocumento);
//      vForm.sgCheques.Cells[ceBanco, i + 1]        := NFormat(vChequesCobrancas[i].banco_caixa_id);
      vForm.sgCheques.Cells[ceAgencia, i + 1]      := vChequesCobrancas[i].agencia;
      vForm.sgCheques.Cells[ceConta, i + 1]        := vChequesCobrancas[i].conta_corrente;
      vForm.sgCheques.Cells[ceVencimento, i + 1]   := DFormat(vChequesCobrancas[i].data_vencimento);
      vForm.sgCheques.Cells[ceEmitente, i + 1]     := vChequesCobrancas[i].nome_emitente;
      vForm.sgCheques.Cells[ceTelefoneEmit, i + 1] := vChequesCobrancas[i].telefone_emitente;
      vForm.sgCheques.Cells[ceReceberId, i + 1]    := NFormat(vChequesCobrancas[i].ReceberId);

      if VerificarJaTinhaSidoSelecionado(0, 0, vChequesCobrancas[i].ReceberId) then
        vForm.sgCheques.Cells[coSelecionado, i + 1]     := 'Sim'
      else
        vForm.sgCheques.Cells[coSelecionado, i + 1]     := 'N�o';
    end;
    vForm.sgCheques.RowCount := IfThen(Length(vChequesCobrancas) > 1, High(vChequesCobrancas) + 2, 2);
  end
  else begin
    vChequesCobrancas := _ContasReceber.BuscarContasReceber(Sessao.getConexaoBanco, 2, [pTurnoId]);
    for i := Low(vChequesCobrancas) to High(vChequesCobrancas) do begin
      vForm.sgCobrancas.Cells[coDocumento, i + 1]  := vChequesCobrancas[i].documento;
      vForm.sgCobrancas.Cells[coCliente, i + 1]    := NFormat(vChequesCobrancas[i].CadastroId) + ' - ' + vChequesCobrancas[i].NomeCliente;
      vForm.sgCobrancas.Cells[coValor, i + 1]      := NFormat(vChequesCobrancas[i].ValorDocumento);
      vForm.sgCobrancas.Cells[coVencimento, i + 1] := DFormat(vChequesCobrancas[i].data_vencimento);
      vForm.sgCobrancas.Cells[coReceberId, i + 1]  := NFormat(vChequesCobrancas[i].ReceberId);

      if VerificarJaTinhaSidoSelecionado(0, 0, vChequesCobrancas[i].ReceberId) then
        vForm.sgCobrancas.Cells[coSelecionado, i + 1]     := 'Sim'
      else
        vForm.sgCobrancas.Cells[coSelecionado, i + 1]     := 'N�o';
    end;
    vForm.sgCobrancas.RowCount := IfThen(Length(vChequesCobrancas) > 1, High(vChequesCobrancas) + 2, 2);
  end;

  if Result.Ok(vForm.ShowModal) then begin
    if pTipo = ttCartoes then begin
      for i := 1 to vForm.sgCartoes.RowCount - 1 do begin
        if vForm.sgCartoes.Cells[coSelecionado, i] = 'N�o' then
          Continue;

        SetLength(Result.Dados, Length(Result.Dados) + 1);
        Result.Dados[High(Result.Dados)].orcamento_id       := SFormatInt(vForm.sgCartoes.Cells[ccOrcamentoId, i]);
        Result.Dados[High(Result.Dados)].BaixaOrigemId      := SFormatInt(vForm.sgCartoes.Cells[coBaixaOrigemId, i]);
        Result.Dados[High(Result.Dados)].tipo               := ttCartoes;
        Result.Dados[High(Result.Dados)].valor              := SFormatDouble(vForm.sgCartoes.Cells[ccValor, i]);
        Result.Dados[High(Result.Dados)].ItemIdCrtOrcamento := SFormatInt(vForm.sgCartoes.Cells[coItemIdCrtOrcamento, i]);
      end;
    end
    else if pTipo = ttCheques then begin
      for i := 1 to vForm.sgCheques.RowCount - 1 do begin
        if vForm.sgCheques.Cells[coSelecionado, i] = 'N�o' then
          Continue;

        SetLength(Result.Dados, Length(Result.Dados) + 1);
        Result.Dados[High(Result.Dados)].orcamento_id := 0;
        Result.Dados[High(Result.Dados)].tipo         := ttCheques;
        Result.Dados[High(Result.Dados)].valor        := SFormatDouble(vForm.sgCheques.Cells[coValor, i]);
        Result.Dados[High(Result.Dados)].receber_id   := SFormatInt(vForm.sgCheques.Cells[ceReceberId, i]);
      end;
    end
    else begin
      for i := 1 to vForm.sgCobrancas.RowCount - 1 do begin
        if vForm.sgCobrancas.Cells[coSelecionado, i] = 'N�o' then
          Continue;

        SetLength(Result.Dados, Length(Result.Dados) + 1);
        Result.Dados[High(Result.Dados)].orcamento_id := 0;
        Result.Dados[High(Result.Dados)].tipo         := ttCheques;
        Result.Dados[High(Result.Dados)].valor        := SFormatDouble(vForm.sgCobrancas.Cells[ceValor, i]);
        Result.Dados[High(Result.Dados)].receber_id   := SFormatInt(vForm.sgCobrancas.Cells[coReceberId, i]);
      end;
    end;
  end;

  vForm.Free;
end;

procedure TFormBuscarDadosTitulosFechamento.sgCartoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol = coSelecionado then
    vAlinhamento := taCenter
  else if ACol = ccValor then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgCartoes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormBuscarDadosTitulosFechamento.GridGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if (ARow = 0) or (ACol <> coSelecionado) then
    Exit;

  AFont.Style := [fsBold];
  AFont.Color := _Biblioteca.AzulVermelho(TGridLuka(Sender).Cells[ACol, ARow]);
end;

procedure TFormBuscarDadosTitulosFechamento.sgChequesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol = coSelecionado then
    vAlinhamento := taCenter
  else if ACol = ceValor then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgCheques.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormBuscarDadosTitulosFechamento.sgCobrancasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol = coSelecionado then
    vAlinhamento := taCenter
  else if ACol = coValor then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgCobrancas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormBuscarDadosTitulosFechamento.VerificarRegistro(Sender: TObject);
var
  i: Integer;
  vGrid: TGridLuka;
  vTemTituloMarcado: Boolean;
begin
  inherited;

  if FTipo = ttCartoes then
    vGrid := sgCartoes
  else if FTipo = ttCheques then
    vGrid := sgCheques
  else
    vGrid := sgCobrancas;

  vTemTituloMarcado := False;
  for i := 1 to vGrid.RowCount - 1 do begin
    if vGrid.Cells[coSelecionado, i] = 'Sim' then begin
      vTemTituloMarcado := True;
      Break;
    end;
  end;

  if not vTemTituloMarcado then begin
    if not Perguntar('N�o foi informado nenhum t�tulo para o fechamento') then
      Abort;
  end;
end;

procedure TFormBuscarDadosTitulosFechamento.GridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_SPACE then
    Exit;

  TGridLuka(Sender).Cells[coSelecionado, TGridLuka(Sender).Row] := IfThen(TGridLuka(Sender).Cells[coSelecionado, TGridLuka(Sender).Row] = 'Sim', 'N�o', 'Sim');
end;

end.
