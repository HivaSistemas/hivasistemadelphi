inherited FormEdicaoNotaFiscalItens: TFormEdicaoNotaFiscalItens
  Caption = 'Edi'#231#227'o dos itens da nota fiscal'
  ClientHeight = 672
  ClientWidth = 1014
  ExplicitWidth = 1020
  ExplicitHeight = 701
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 635
    Width = 1014
    ExplicitTop = 635
    ExplicitWidth = 1014
  end
  object sgProdutos: TGridLuka
    Left = 0
    Top = 0
    Width = 1014
    Height = 598
    Align = alClient
    ColCount = 28
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 2
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goEditing]
    TabOrder = 1
    OnDrawCell = sgProdutosDrawCell
    OnSelectCell = sgProdutosSelectCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Produto'
      'Nome'
      'Pre'#231'o unit.'
      'Quantidade'
      'Und.'
      'Valor total'
      'NCM'
      'CEST'
      'Valor desc.'
      'Valor out. desp.'
      'Base calc. ICMS'
      '% ICMS'
      'Valor ICMS'
      'Base calc. ICMS ST'
      '% ICMS ST'
      'Valor ICMS ST'
      'CST PIS'
      'Base calc. PIS'
      '% PIS'
      'Valor PIS'
      'CST COFINS'
      'Base calc. COFINS'
      '% COFINS'
      'Valor COFINS'
      'Valor IPI'
      '% IPI'
      'C'#243'd. barras'
      'CFOP')
    OnArrumarGrid = sgProdutosArrumarGrid
    Grid3D = False
    RealColCount = 28
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      64
      263
      79
      87
      54
      69
      131
      59
      77
      95
      92
      54
      74
      111
      72
      86
      51
      87
      42
      72
      71
      110
      64
      81
      64
      40
      177
      70)
  end
  object pn1: TPanel
    Left = 0
    Top = 598
    Width = 1014
    Height = 37
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object stTit_Total_Notas: TStaticText
      Left = 239
      Top = 1
      Width = 91
      Height = 18
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSingle
      Caption = 'Base c'#225'lc. ICMS'
      Color = 15395562
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 0
      Transparent = False
    end
    object st2: TStaticText
      Left = 329
      Top = 1
      Width = 95
      Height = 18
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSingle
      Caption = 'Valor ICMS'
      Color = 15395562
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 1
      Transparent = False
    end
    object st6: TStaticText
      Left = 802
      Top = 1
      Width = 95
      Height = 18
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSingle
      Caption = 'Valor IPI'
      Color = 15395562
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 2
      Transparent = False
    end
    object st7: TStaticText
      Left = 423
      Top = 1
      Width = 109
      Height = 18
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSingle
      Caption = 'Base c'#225'lc. ICMS ST'
      Color = 15395562
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 3
      Transparent = False
    end
    object st8: TStaticText
      Left = 531
      Top = 1
      Width = 84
      Height = 18
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSingle
      Caption = 'Valor ICMS ST'
      Color = 15395562
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 4
      Transparent = False
    end
    object st9: TStaticText
      Left = 614
      Top = 1
      Width = 95
      Height = 18
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSingle
      Caption = 'Outras desp.'
      Color = 15395562
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 5
      Transparent = False
    end
    object st11: TStaticText
      Left = 708
      Top = 1
      Width = 95
      Height = 18
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSingle
      Caption = 'Desconto'
      Color = 15395562
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 6
      Transparent = False
    end
    object st12: TStaticText
      Left = 896
      Top = 1
      Width = 117
      Height = 18
      Alignment = taCenter
      AutoSize = False
      BorderStyle = sbsSingle
      Caption = 'Total dos produtos'
      Color = 15395562
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 7
      Transparent = False
    end
    object stTotalBaseCalculoICMS: TStaticText
      Left = 239
      Top = 18
      Width = 91
      Height = 18
      Alignment = taRightJustify
      AutoSize = False
      BorderStyle = sbsSingle
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 8
      Transparent = False
    end
    object stTotalValorICMS: TStaticText
      Left = 329
      Top = 18
      Width = 95
      Height = 18
      Alignment = taRightJustify
      AutoSize = False
      BorderStyle = sbsSingle
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 9
      Transparent = False
    end
    object stTotalValorIPI: TStaticText
      Left = 802
      Top = 18
      Width = 95
      Height = 18
      Alignment = taRightJustify
      AutoSize = False
      BorderStyle = sbsSingle
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 10
      Transparent = False
    end
    object stTotalBaseCalculoICMSST: TStaticText
      Left = 423
      Top = 18
      Width = 109
      Height = 18
      Alignment = taRightJustify
      AutoSize = False
      BorderStyle = sbsSingle
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 11
      Transparent = False
    end
    object stTotalValorICMSST: TStaticText
      Left = 531
      Top = 18
      Width = 84
      Height = 18
      Alignment = taRightJustify
      AutoSize = False
      BorderStyle = sbsSingle
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 12
      Transparent = False
    end
    object stTotalOutrasDespesas: TStaticText
      Left = 614
      Top = 18
      Width = 95
      Height = 18
      Alignment = taRightJustify
      AutoSize = False
      BorderStyle = sbsSingle
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 13
      Transparent = False
    end
    object stTotalDesconto: TStaticText
      Left = 708
      Top = 18
      Width = 95
      Height = 18
      Alignment = taRightJustify
      AutoSize = False
      BorderStyle = sbsSingle
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 14
      Transparent = False
    end
    object stTotalProdutos: TStaticText
      Left = 896
      Top = 18
      Width = 117
      Height = 18
      Alignment = taRightJustify
      AutoSize = False
      BorderStyle = sbsSingle
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 15
      Transparent = False
    end
  end
end
