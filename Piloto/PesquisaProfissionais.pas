unit PesquisaProfissionais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, Vcl.ExtCtrls;

type
  TFormPesquisaProfissionais = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  protected
    procedure BuscarRegistros; override;
  end;

function Pesquisar(pSomenteAtivos: Boolean): TObject;
function PesquisarVarios(pSomenteAtivos: Boolean): TArray<TObject>;

implementation

{$R *.dfm}

uses _Profissionais, _Biblioteca, _Sessao;

const
  coRazaoSocial  = 2;
  coNomeFantasia = 3;

function Pesquisar(pSomenteAtivos: Boolean): TObject;
var
  vObj: TObject;
begin
  vObj := _HerancaPesquisas.Pesquisar(TFormPesquisaProfissionais, _Profissionais.GetFiltros, [pSomenteAtivos]);
  if vObj = nil then
    Result := nil
  else
    Result := RecProfissionais(vObj);
end;

procedure TFormPesquisaProfissionais.BuscarRegistros;
var
  i: Integer;
  vDados: TArray<RecProfissionais>;
begin
  inherited;

  vDados :=
    _Profissionais.BuscarProfissionais(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text],
      False
    );

  if vDados = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vDados);

  for i := Low(vDados) to High(vDados) do begin
    sgPesquisa.Cells[coSelecionado, i + 1]  := _Biblioteca.charNaoSelecionado;
    sgPesquisa.Cells[coCodigo, i + 1]       := NFormat(vDados[i].CadastroId);
    sgPesquisa.Cells[coNomeFantasia, i + 1] := vDados[i].Cadastro.nome_fantasia;
    sgPesquisa.Cells[coRazaoSocial, i + 1]  := vDados[i].Cadastro.razao_social;
  end;

  sgPesquisa.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor(Length(vDados));
  SetarFoco(sgPesquisa);
end;

procedure TFormPesquisaProfissionais.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = coSelecionado then
    vAlinhamento := taCenter
  else if ACol = coCodigo then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

function PesquisarVarios(pSomenteAtivos: Boolean): TArray<TObject>;
var
  vRet: TArray<TObject>;
begin
  vRet := _HerancaPesquisas.PesquisarVarios(TFormPesquisaProfissionais, _Profissionais.GetFiltros);
  if vRet = nil then
    Result := nil
  else
    Result := TArray<TObject>(vRet);
end;

end.
