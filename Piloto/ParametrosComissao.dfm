inherited FormParametrosComissao: TFormParametrosComissao
  Caption = 'Par'#226'metros de comiss'#227'o'
  ClientHeight = 428
  ClientWidth = 499
  ExplicitTop = -34
  ExplicitWidth = 505
  ExplicitHeight = 457
  PixelsPerInch = 96
  TextHeight = 14
  object lb10: TLabel [0]
    Left = 125
    Top = 88
    Width = 191
    Height = 14
    Caption = 'Tipo de comiss'#227'o por lucratividade'
  end
  object Label1: TLabel [1]
    Left = 285
    Top = 406
    Width = 207
    Height = 14
    Caption = 'Obs: CTRL + Delete para remover linha'
  end
  inherited pnOpcoes: TPanel
    Height = 428
    ExplicitHeight = 428
    inherited sbDesfazer: TSpeedButton
      Top = 53
      ExplicitTop = 53
    end
    inherited sbExcluir: TSpeedButton
      Visible = False
    end
    inherited sbPesquisar: TSpeedButton
      Visible = False
    end
  end
  inline FrEmpresa: TFrEmpresas
    Left = 125
    Top = 2
    Width = 284
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 125
    ExplicitTop = 2
    ExplicitWidth = 284
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 259
      Height = 23
      ExplicitWidth = 259
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 284
      ExplicitWidth = 284
      inherited lbNomePesquisa: TLabel
        Width = 47
        Caption = 'Empresa'
        ExplicitWidth = 47
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 179
        ExplicitLeft = 179
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 259
      Height = 24
      ExplicitLeft = 259
      ExplicitHeight = 24
      inherited sbPesquisa: TSpeedButton
        Visible = False
      end
    end
  end
  object StaticTextLuka5: TStaticTextLuka
    Left = 125
    Top = 65
    Width = 367
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Faixas de comiss'#227'o por lucratividade'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object cbTipoComissao: TComboBoxLuka
    Left = 125
    Top = 108
    Width = 193
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = 16645629
    TabOrder = 3
    OnKeyDown = ProximoCampo
    Items.Strings = (
      'Pelo Totalizador'
      'Por Venda')
    Valores.Strings = (
      'T'
      'V')
    AsInt = 0
  end
  object sgFaixas: TGridLuka
    Left = 125
    Top = 133
    Width = 369
    Height = 270
    Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    ColCount = 4
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goEditing]
    TabOrder = 4
    OnDrawCell = sgFaixasDrawCell
    OnKeyDown = sgFaixasKeyDown
    OnKeyPress = NumerosVirgula
    OnSelectCell = sgFaixasSelectCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = clWindow
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Faixa'
      'Valor inicial'
      'Valor final'
      '% Comiss'#227'o')
    Grid3D = False
    RealColCount = 31
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      61
      119
      83
      81)
  end
end
