unit Buscar.DadosClientePDV;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Sessao,
  Vcl.ExtCtrls, Vcl.StdCtrls, EditLuka, _FrameHerancaPrincipal, _Biblioteca,
  _FrameHenrancaPesquisas, FrameClientes, Vcl.Mask, EditCpfCnpjLuka,
  EditTelefoneLuka;

type
  RecDadosCliente = record
    ClienteId: Integer;
    NomeCliente: string;
    Cpf: string;
    Endereco: string;
    Telefone: string;
  end;

  TFormBuscarDadosCliente = class(TFormHerancaFinalizar)
    pnFrameCliente: TPanel;
    FrCliente: TFrClientes;
    pnCpfNome: TPanel;
    lb1: TLabel;
    lbCPF_CNPJ: TLabel;
    eNome: TEditLuka;
    stFormasPagamento: TStaticText;
    eCPF: TEditCPF_CNPJ_Luka;
    pnEndereco: TPanel;
    lb2: TLabel;
    eEndereco: TEditLuka;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure HabilitarCampos(pHabilitar: Boolean);
    procedure FrClienteOnAposPesquisar(Sender: TObject);
    procedure FrClienteOnAposDeletar(Sender: TObject);
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Buscar(pDadosCliente: RecDadosCliente; pOcultarCampos: Boolean = False): TRetornoTelaFinalizar<RecDadosCliente>;

implementation

{$R *.dfm}

function Buscar(pDadosCliente: RecDadosCliente; pOcultarCampos: Boolean = False): TRetornoTelaFinalizar<RecDadosCliente>;
var
  vForm: TFormBuscarDadosCliente;
begin
  vForm := TFormBuscarDadosCliente.Create(Application);

  vForm.FrCliente.InserirDadoPorChave(pDadosCliente.ClienteId, False);
  vForm.FrClienteOnAposPesquisar(vForm);
  if pDadosCliente.NomeCliente <> '' then begin
    vForm.eNome.Text     := pDadosCliente.NomeCliente;
    vForm.eCPF.Text      := pDadosCliente.Cpf;
    vForm.eEndereco.Text := pDadosCliente.Endereco;
  end;

  if pOcultarCampos then begin
    vForm.pnFrameCliente.Visible := False;
    vForm.pnEndereco.Visible := False;
  end;

  if Result.Ok(vForm.ShowModal) then begin
    if Sessao.getParametros.cadastro_consumidor_final_id = vForm.FrCliente.getCliente.cadastro_id then begin
      Result.Dados.ClienteId   := Sessao.getParametros.cadastro_consumidor_final_id;
      Result.Dados.NomeCliente := vForm.eNome.Text;
      Result.Dados.Cpf         := vForm.eCPF.Text;
      Result.Dados.Endereco    := vForm.eEndereco.Text;
    end
    else begin
      Result.Dados.ClienteId   := vForm.FrCliente.getCliente.cadastro_id;
//      Result.Dados.NomeCliente := vForm.FrCliente.getCliente.cadastro.nome_fantasia;
//      Result.Dados.Cpf         := vForm.FrCliente.getCliente.cadastro.cpf_cnpj;
//      Result.Dados.Endereco    := vForm.FrCliente.getCliente.cadastro.logradouro;
    end;
  end;

  vForm.Free;
end;

{ TFormBuscarDadosCliente }

procedure TFormBuscarDadosCliente.FormCreate(Sender: TObject);
begin
  inherited;
  FrCliente.OnAposPesquisar := FrClienteOnAposPesquisar;
  FrCliente.OnAposDeletar := FrClienteOnAposDeletar;
  ActiveControl := FrCliente.sgPesquisa;
  HabilitarCampos(True);
end;

procedure TFormBuscarDadosCliente.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(eCPF);
end;

procedure TFormBuscarDadosCliente.FrClienteOnAposDeletar(Sender: TObject);
begin
  HabilitarCampos(False);
end;

procedure TFormBuscarDadosCliente.FrClienteOnAposPesquisar(Sender: TObject);
begin
  if Sessao.getParametros.cadastro_consumidor_final_id = FrCliente.getCliente.cadastro_id then
    HabilitarCampos(True);
end;

procedure TFormBuscarDadosCliente.HabilitarCampos(pHabilitar: Boolean);
begin
  _Biblioteca.Habilitar([eNome, eCPF, eEndereco], pHabilitar);
end;

procedure TFormBuscarDadosCliente.VerificarRegistro(Sender: TObject);
begin
  inherited;
  eNome.Text := Trim(eNome.Text);
  if FrCliente.EstaVazio then begin
    Exclamar('� necess�rio informar o cliente!');
    SetarFoco(FrCliente);
    Abort;
  end;
end;

end.

