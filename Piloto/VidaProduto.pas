unit VidaProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  _HerancaRelatoriosPageControl, Vcl.Buttons, Vcl.ExtCtrls, FrameEmpresas, FrameDataInicialFinal, _FrameHerancaPrincipal, _FrameHenrancaPesquisas,
  FrameProdutos, _HerancaFinalizar, Vcl.ComCtrls, _Sessao, _Biblioteca, _RecordsEstoques, Vcl.Grids, GridLuka, System.Math,
  Informacoes.Orcamento, Informacoes.Devolucao, Informacoes.AjustaEstoque, _VidaProduto, Informacoes.Retiradas, Informacoes.NotaFiscal,
  Vcl.StdCtrls, RadioGroupLuka, Informacoes.Entrega, InformacoesEntradaNotaFiscal,
  CheckBoxLuka, _Estoques, StaticTextLuka, frxClass;

type
  TFormVidaProduto = class(TFormHerancaRelatoriosPageControl)
    FrProduto: TFrProdutos;
    FrDataMovimentoInicialFinal: TFrDataInicialFinal;
    FrEmpresa: TFrEmpresas;
    sgMovimentacoes: TGridLuka;
    rgTipoEstoque: TRadioGroupLuka;
    st1: TStaticText;
    st2: TStaticText;
    st4: TStaticText;
    stEstoqueDisponivel: TStaticTextLuka;
    stEstoqueFisico: TStaticTextLuka;
    stEstoqueContabil: TStaticTextLuka;
    sgResumo: TGridLuka;
    spl1: TSplitter;
    dsVidaProduto: TfrxUserDataSet;
    frxReport: TfrxReport;
    procedure sgMovimentacoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgMovimentacoesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgMovimentacoesDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sgResumoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgResumoGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure dsVidaProdutoFirst(Sender: TObject);
    procedure dsVidaProdutoGetValue(const VarName: string; var Value: Variant);
    procedure dsVidaProdutoNext(Sender: TObject);
    procedure dsVidaProdutoPrior(Sender: TObject);
    procedure dsVidaProdutoCheckEOF(Sender: TObject; var Eof: Boolean);
  private
    MasterNo: Integer;
    procedure FrProdutoOnAposPesquisar(Sender: TObject);
    procedure FrProdutoOnAposDeletar(Sender: TObject);
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormVidaProduto }

const
  coDataHoraMovimento      = 0;
  coTipoMovimentoAnalitico = 1;
  coMovimentoId            = 2;
  coCliente                = 3;
  coNumDoc                 = 4;
  coEntrada                = 5;
  coSaida                  = 6;
  coSaldo                  = 7;
  coTipoMovimento          = 8;
  coNatureza               = 9;
  coCadastroId             =10;

  coResumoTipoMovimento    = 0;
  coResumoEntrada          = 1;
  coResumoSaida            = 2;
  coResumoSaldo            = 3;

type
  RecResumoMovimento = record
    tipo_movimento: string;
    entrada: Double;
    saida: Double;
    saldo: Double;
  end;

procedure TFormVidaProduto.Carregar(Sender: TObject);
var
  i: Integer;
  j: Integer;
  vSql: string;
  vProdutos: TArray<RecVidaProduto>;

  vSaldoInicial: Double;
  vSaldo: Double;
  vLinha: Integer;
  vLinhaResumo: Integer;
  vAchou: Boolean;
begin
  inherited;

  vLinha := 1;
  vLinhaResumo := 1;
  sgMovimentacoes.ClearGrid;
  sgResumo.ClearGrid;

  vSql :=
    'where PRODUTO_ID ' + FrProduto.getSqlFiltros + ' ' +
    'and EMPRESA_ID ' + FrEmpresa.getSqlFiltros + ' ' +
    'and ' + FrDataMovimentoInicialFinal.getSqlFiltros('trunc(DATA_HORA_MOVIMENTO)');

  vSql := vSql + 'order by DATA_HORA_MOVIMENTO';

  vProdutos := _VidaProduto.BuscarVidaProdutoComando(Sessao.getConexaoBanco, vSql, rgTipoEstoque.GetValor);
  if vProdutos = nil then begin
    _Biblioteca.Exclamar('N�o foram encontrados movimento para o produto!');
    Exit;
  end;

  vSql :=
    'where PRODUTO_ID ' + FrProduto.getSqlFiltros + ' ' +
    'and EMPRESA_ID ' + FrEmpresa.getSqlFiltros + ' ' +
    'and ' + FrDataMovimentoInicialFinal.getSQLDataInicialMenor('trunc(DATA_HORA_MOVIMENTO)');

  vSaldoInicial :=  _VidaProduto.BuscarSaldoInicialVidaProduto(Sessao.getConexaoBanco, vSql, rgTipoEstoque.GetValor);
  vSaldo := vSaldoInicial;

  sgMovimentacoes.Cells[coDataHoraMovimento, vLinha] := 'Inicial';
  sgMovimentacoes.Cells[coSaldo, vLinha] := NFormatEstoque(vSaldoInicial);
  Inc(vLinha);

  for i := Low(vProdutos) to High(vProdutos) do begin
    vSaldo := vSaldo + (vProdutos[i].quantidade * IfThen(vProdutos[i].natureza = 'E', 1, -1));

    sgMovimentacoes.Cells[coDataHoraMovimento, vLinha]      := _Biblioteca.DFormat(vProdutos[i].data_hora_movimento) + ' ' + HFormat(vProdutos[i].data_hora_movimento);
    sgMovimentacoes.Cells[coTipoMovimentoAnalitico, vLinha] := vProdutos[i].tipo_movimento_analitico;
    sgMovimentacoes.Cells[coMovimentoId, vLinha]            := NFormat(vProdutos[i].movimento_id);
    sgMovimentacoes.Cells[coCliente, vLinha]                := getInformacao(vProdutos[i].CadastroId, vProdutos[i].NomeCadastro);
    sgMovimentacoes.Cells[coNumDoc, vLinha]                 := vProdutos[i].NumeroDocumento;

    if vProdutos[i].natureza = 'E' then
      sgMovimentacoes.Cells[coEntrada, vLinha]              := _Biblioteca.NFormatNEstoque(vProdutos[i].quantidade)
    else
      sgMovimentacoes.Cells[coEntrada, vLinha]              := '';

    if vProdutos[i].natureza = 'S' then
      sgMovimentacoes.Cells[coSaida, vLinha]                := _Biblioteca.NFormatNEstoque(vProdutos[i].quantidade)
    else
      sgMovimentacoes.Cells[coSaida, vLinha]                := '';

    sgMovimentacoes.Cells[coSaldo, vLinha]                  := _Biblioteca.NFormatNEstoque(vSaldo);
    sgMovimentacoes.Cells[coTipoMovimento, vLinha]          := vProdutos[i].tipo_movimento;
    sgMovimentacoes.Cells[coNatureza, vLinha]               := vProdutos[i].natureza;
    sgMovimentacoes.Cells[coCadastroId, vLinha]             := NFormat(vProdutos[i].CadastroId);

    vAchou := False;
    if i = Low(vProdutos) then
    begin
      sgResumo.Cells[coResumoTipoMovimento, 1] := 'Inicial';
      sgResumo.Cells[coResumoSaldo, 1] := NFormatEstoque(vSaldoInicial);
      Inc(vLinhaResumo);
    end;

    for j := 2 to vLinhaResumo do begin
      if sgResumo.Cells[coResumoTipoMovimento, j] = vProdutos[i].tipo_movimento_analitico then begin

        if vProdutos[i].natureza = 'E' then
          sgResumo.Cells[coResumoEntrada, j] := _Biblioteca.NFormatNEstoque(SFormatDouble(sgResumo.Cells[coResumoEntrada, j]) + vProdutos[i].quantidade)
        else
          sgResumo.Cells[coResumoSaida, j] := _Biblioteca.NFormatNEstoque(SFormatDouble(sgResumo.Cells[coResumoSaida, j]) + vProdutos[i].quantidade);

        sgResumo.Cells[coResumoSaldo, j] := _Biblioteca.NFormatNEstoque(SFormatDouble(sgResumo.Cells[coResumoEntrada, j]) - SFormatDouble(sgResumo.Cells[coResumoSaida, j]));
        vAchou := True;

      end;
    end;

    if not vAchou then begin
      sgResumo.Cells[coResumoTipoMovimento, vLinhaResumo] := vProdutos[i].tipo_movimento_analitico;
      if vProdutos[i].natureza = 'E' then
        sgResumo.Cells[coResumoEntrada, vLinhaResumo] := _Biblioteca.NFormatNEstoque(vProdutos[i].quantidade)
      else
        sgResumo.Cells[coResumoSaida, vLinhaResumo] := _Biblioteca.NFormatNEstoque(vProdutos[i].quantidade);

      sgResumo.Cells[coResumoSaldo, vLinhaResumo] := _Biblioteca.NFormatNEstoque(SFormatDouble(sgResumo.Cells[coResumoEntrada, vLinhaResumo]) - SFormatDouble(sgResumo.Cells[coResumoSaida, vLinhaResumo]));
      Inc(vLinhaResumo);
    end;

    Inc(vLinha);
  end;


  sgResumo.Cells[coResumoTipoMovimento, vLinhaResumo] := 'Final';
  sgResumo.Cells[coResumoSaldo, vLinhaResumo] := _Biblioteca.NFormatNEstoque(vSaldo);
  Inc(vLinhaResumo);

  sgMovimentacoes.RowCount := vLinha;
  sgResumo.RowCount := vLinhaResumo;
  SetarFoco(sgMovimentacoes);
end;

procedure TFormVidaProduto.dsVidaProdutoCheckEOF(Sender: TObject;
  var Eof: Boolean);
begin
  if not Eof then
    Eof := MasterNo > sgMovimentacoes.RowCount;
end;

procedure TFormVidaProduto.dsVidaProdutoFirst(Sender: TObject);
begin
  inherited;
  MasterNo := 1;
end;

procedure TFormVidaProduto.dsVidaProdutoGetValue(const VarName: string;
  var Value: Variant);
begin
  inherited;
  Value := sgMovimentacoes.Cells[dsVidaProduto.Fields.IndexOf(VarName) ,MasterNo];
end;

procedure TFormVidaProduto.dsVidaProdutoNext(Sender: TObject);
begin
  inherited;
  Inc(MasterNo);
end;

procedure TFormVidaProduto.dsVidaProdutoPrior(Sender: TObject);
begin
  inherited;
  Dec(MasterNo);
end;

procedure TFormVidaProduto.FormShow(Sender: TObject);
begin
  inherited;
  FrProduto.OnAposPesquisar := FrProdutoOnAposPesquisar;
  FrProduto.OnAposDeletar   := FrProdutoOnAposDeletar;

  FrEmpresa.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId, False);
  SetarFoco(FrEmpresa);
  rgTipoEstoque.ItemIndex := 0;
end;

procedure TFormVidaProduto.FrProdutoOnAposPesquisar(Sender: TObject);
var
  vEstoques: TArray<RecEstoque>;
begin
  _Biblioteca.LimparCampos([stEstoqueDisponivel, stEstoqueFisico, stEstoqueContabil]);

  if FrProduto.getProduto.EProdutoFilho then begin
    _Biblioteca.Exclamar('N�o � poss�vel consultar a vida do produto de um produto filho, consulte o produto pai! ' + chr(13) + 'Produto pai: ' + NFormat(FrProduto.getProduto().ProdutoPaiId));
    FrProduto.Clear;
    SetarFoco(FrProduto);
    Exit;
  end;

  if Em(FrProduto.getProduto.TipoControleEstoque, ['K', 'A']) then begin
    _Biblioteca.Exclamar('N�o � poss�vel consultar a vida do produto de um produto kit, consulte os problemas que o comp�e!');
    FrProduto.Clear;
    SetarFoco(FrProduto);
    Exit;
  end;

  vEstoques := _Estoques.BuscarEstoque(Sessao.getConexaoBanco, 0, [FrProduto.getProduto.produto_id, Sessao.getEmpresaLogada.EmpresaId]);
  if vEstoques <> nil then begin
    stEstoqueDisponivel.AsDouble := vEstoques[0].Disponivel;
    stEstoqueFisico.AsDouble     := vEstoques[0].Fisico;
    stEstoqueContabil.AsDouble   := vEstoques[0].Estoque;
  end;

end;

procedure TFormVidaProduto.FrProdutoOnAposDeletar(Sender: TObject);
begin
  _Biblioteca.LimparCampos([stEstoqueDisponivel, stEstoqueFisico, stEstoqueContabil]);
end;

procedure TFormVidaProduto.Imprimir(Sender: TObject);
begin
  inherited;
  TfrxMemoView(frxReport.FindComponent('mmProduto')).Text := IntToStr(FrProduto.getProduto.produto_id) + ' - ' + FrProduto.getProduto.nome;
  TfrxMemoView(frxReport.FindComponent('mmDataInicio')).Text := DFormat(FrDataMovimentoInicialFinal.eDataInicial.AsData);
  TfrxMemoView(frxReport.FindComponent('mmDataFim')).Text := DFormat(FrDataMovimentoInicialFinal.eDataFinal.AsData);
  frxReport.ShowReport();
end;

procedure TFormVidaProduto.sgMovimentacoesDblClick(Sender: TObject);
var
  vMovimento: string;
  vMovimentoId: Integer;
begin
  inherited;
  vMovimento := sgMovimentacoes.Cells[coTipoMovimento, sgMovimentacoes.Row];
  vMovimentoId := SFormatInt(sgMovimentacoes.Cells[coMovimentoId, sgMovimentacoes.Row]);
  if rgTipoEstoque.GetValor = 'D' then begin
    if Em(vMovimento, ['RAT', 'RTI']) then
      Informacoes.Retiradas.Informar( vMovimentoId )
    else if Em(vMovimento, ['VEN']) then
      Informacoes.Entrega.Informar( vMovimentoId )
    else if vMovimento = 'DEV' then
      Informacoes.Devolucao.Informar( vMovimentoId )
    else if vMovimento = 'AJU' then
      Informacoes.AjustaEstoque.Informar( vMovimentoId )
    else if Em(vMovimento, ['RET', 'ENG']) then
      Informacoes.Orcamento.Informar( vMovimentoId );
  end
  else if rgTipoEstoque.GetValor = 'F' then begin
    if Em(vMovimento, ['RAT', 'RET']) then
      Informacoes.Retiradas.Informar( vMovimentoId )
    else if Em(vMovimento, ['VEN']) then
      Informacoes.Entrega.Informar( vMovimentoId )
    else if vMovimento = 'DEV' then
      Informacoes.Devolucao.Informar( vMovimentoId )
    else if vMovimento = 'AJU' then
      Informacoes.AjustaEstoque.Informar( vMovimentoId )
    else if Em(vMovimento, ['RET', 'ENG']) then
      Informacoes.Orcamento.Informar( vMovimentoId );
  end
  else begin
    if Em(sgMovimentacoes.Cells[coTipoMovimento, sgMovimentacoes.Row], ['ENT']) then
      InformacoesEntradaNotaFiscal.Informar( vMovimentoId )
    else
      Informacoes.NotaFiscal.Informar( vMovimentoId );
  end;
end;

procedure TFormVidaProduto.sgMovimentacoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coEntrada, coSaida, coSaldo, coNumDoc] then
    vAlinhamento := taRightJustify
  else if ACol in[coMovimentoId, coTipoMovimentoAnalitico] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgMovimentacoes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormVidaProduto.sgMovimentacoesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
const
  coCorEntradaNota = clBlue;
  coCorVenda       = $000096DB;
  coCorAjusteEst   = clOlive;
  coCorDevolucao   = $000000A8;
  coCorProducao    = clGreen;
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgMovimentacoes.Cells[0,1] = '' then
    Exit;

  if ACol = coTipoMovimentoAnalitico then begin
    AFont.Style := [fsBold];
    AFont.Color :=
      Decode(
        sgMovimentacoes.Cells[coTipoMovimento, ARow],
        [
          'ENT', coCorEntradaNota,
          'ORC', coCorVenda,
          'AJU', coCorAjusteEst,
          'DEV', coCorDevolucao,
          'PRO', coCorProducao,
          clBlack
        ]
      );
  end
  else if ACol in [coEntrada, coSaida, coSaldo] then begin
    AFont.Style := [fsBold];

    if ACol = coSaldo then begin
      if SFormatCurr(sgMovimentacoes.Cells[ACol, ARow]) < 0 then
        AFont.Color := clRed
      else if SFormatCurr(sgMovimentacoes.Cells[ACol, ARow]) > 0 then
        AFont.Color := clBlue;
    end
    else begin
      if sgMovimentacoes.Cells[coNatureza, ARow] = 'S' then
        AFont.Color := clRed
      else
        AFont.Color := clBlue;
    end;
  end;
end;

procedure TFormVidaProduto.sgResumoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coResumoEntrada, coResumoSaida, coResumoSaldo] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgResumo.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormVidaProduto.sgResumoGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
const
  coCorEntradaNota = clBlue;
  coCorVenda       = $000096DB;
  coCorAjusteEst   = clOlive;
  coCorDevolucao   = $000000A8;
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgResumo.Cells[0,1] = '' then
    Exit;

  if ACol = coResumoTipoMovimento then begin
    AFont.Style := [fsBold];

    AFont.Color :=
      Decode(
        sgResumo.Cells[coResumoTipoMovimento, ARow],
        [
          'ENT', coCorEntradaNota,
          'ORC', coCorVenda,
          'DEV', coCorDevolucao,
          'AJU', coCorAjusteEst,
          clBlack
        ]
      );
  end
  else if ACol in [coResumoEntrada, coResumoSaida, coResumoSaldo] then begin
    AFont.Style := [fsBold];

    if ACol = coResumoSaldo then begin
      if SFormatCurr(sgResumo.Cells[coResumoSaldo, ARow]) < 0 then
        AFont.Color := clRed
      else if SFormatCurr(sgResumo.Cells[coResumoSaldo, ARow]) > 0 then
        AFont.Color := clBlue;
    end
    else begin
      if sgResumo.Cells[coResumoSaida, ARow] = 'S' then
        AFont.Color := clRed
      else
        AFont.Color := clBlue;

      if ACol = coResumoSaida then
        AFont.Color := clRed;
    end;
  end;
end;

procedure TFormVidaProduto.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if not FrDataMovimentoInicialFinal.DatasValidas then begin
    _Biblioteca.Exclamar('Data do movimento inv�lida!');
    SetarFoco(FrDataMovimentoInicialFinal);
    Abort;
  end;

  if FrEmpresa.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio informar a empresa!');
    SetarFoco(FrEmpresa);
    Abort;
  end;

  if FrProduto.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio informar o produto!');
    SetarFoco(FrProduto);
    Abort;
  end;
end;

end.
