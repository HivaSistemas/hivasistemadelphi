inherited FormBaixaAutomaticaCartaoCielo: TFormBaixaAutomaticaCartaoCielo
  Caption = 'Baixa de cart'#245'es com arquivo CIELO'
  ClientHeight = 571
  ClientWidth = 994
  ExplicitWidth = 1000
  ExplicitHeight = 600
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 571
    ExplicitHeight = 571
    inherited sbCarregar: TSpeedButton
      Glyph.Data = {
        360C0000424D360C000000000000360000002800000020000000200000000100
        180000000000000C0000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFADADAD1C1C1C0000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000001B1B1BADADADFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF1111110000000000000000000000000000000000
        000000001D1D1D7171717676762A2A2A00000000000000000000000000000000
        0000000000000000111111FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000
        00414141F5F5F5FFFFFFFFFFFFFCFCFC5B5B5B00000000000000000000000000
        0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000
        00D4D4D4E8F5FC34ABE228A6E0D6EEF9EFEFEF06060600000000000000000000
        0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000A0A
        0AFFFFFF98D4F00095DA0095DA77C6EBFFFFFF2B2B2B00000000000000000000
        0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000F0F
        0FFFFFFF8FD0EF0095DA0095DA6FC3EAFFFFFF2F2F2F00000000000000000000
        0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000F0F
        0FFFFFFF8FD0EF0095DA0095DA6FC3EAFFFFFF2F2F2F00000000000000000000
        0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000909093939
        39FFFFFF8FD0EF0095DA0095DA6FC3EAFFFFFF5555550D0D0D00000000000000
        0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000004C4C4CEEEEEEFFFF
        FFFFFFFF8FD0EF0095DA0095DA6FC3EAFFFFFFFFFFFFF5F5F565656500000000
        0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000B0B0BF1F1F1D7EEF964BF
        E95FBCE835ABE20095DA0095DA29A6E05FBCE861BDE8C6E7F7FBFBFB21212100
        0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000333333FFFFFF70C4EA0095
        DA0095DA0095DA0095DA0095DA0095DA0095DA0095DA4EB5E5FFFFFF55555500
        0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000B0B0BF0F0F0D4EDF90B9A
        DC0095DA0095DA0095DA0095DA0095DA0095DA0296DABBE3F5FBFBFB22222200
        0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000005B5B5BFFFFFFA8DB
        F20095DA0095DA0095DA0095DA0095DA0095DA89CEEEFFFFFF7B7B7B00000000
        0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000909090FFFF
        FF74C5EB0095DA0095DA0095DA0095DA54B8E6FEFFFFB0B0B000000000000000
        0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000040404C0C0
        C0FCFEFF42B0E40095DA0095DA2BA7E0F3FAFDD7D7D70D0D0D00000000000000
        0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000001616
        16E3E3E3ECF7FC28A6E0199FDEDAF0FAF2F2F229292900000000000000000000
        0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000
        00363636F5F5F5FEFFFFFCFEFFFCFCFC4F4F4F00000000000000000000000000
        0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000
        000000002727278A8A8A90909035353500000000000000000000000000000000
        0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000
        00000000000000000000000000000000000000020202666666A7A7A7AFAFAFAF
        AFAFAFAFAFAFAFAFAFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000
        00000000000000000000000000000000000000787878B6B6B64141413F3F3F3F
        3F3F3F3F3F3F3F3F767676FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000
        00000000000000000000000000000000000000AEAEAE51515100000000000000
        0000000000363636F0F0F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000
        00000000000000000000000000000000000000AFAFAF4F4F4F00000000000000
        0000363636F0F0F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000
        00000000000000000000000000000000000000AFAFAF4F4F4F00000000000031
        3131F0F0F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000
        00000000000000000000000000000000000000AFAFAF4F4F4F0000002C2C2CEA
        EAEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF1111110000000000000000000000000000000000
        00000000000000000000000000000000000000AFAFAF4F4F4F2C2C2CEAEAEAFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFADADAD1B1B1B0000000000000000000000000000
        00000000000000000000000000000000000000AFAFAF7C7C7CEAEAEAFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 1
    end
    inherited sbImprimir: TSpeedButton
      Left = -120
      ExplicitLeft = -120
    end
    object sbGravar: TSpeedButtonLuka [2]
      Left = 1
      Top = 78
      Width = 117
      Height = 35
      Caption = 'Gravar (F2)'
      Flat = True
      NumGlyphs = 2
      OnClick = sbGravarClick
      TagImagem = 9
      PedirCertificacao = False
      PermitirAutOutroUsuario = False
    end
    inherited ckOmitirFiltros: TCheckBoxLuka
      Left = -115
      ExplicitLeft = -115
    end
  end
  object pnPrincipal: TPanel
    Left = 122
    Top = 0
    Width = 872
    Height = 571
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object pn2: TPanel
      Left = 0
      Top = 0
      Width = 872
      Height = 42
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      inline FrCaminhoArquivo: TFrCaminhoArquivo
        Left = -3
        Top = 1
        Width = 877
        Height = 45
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = -3
        ExplicitTop = 1
        ExplicitWidth = 877
        inherited lb1: TLabel
          Width = 92
          Height = 14
          Caption = 'Caminho arquivo'
          ExplicitWidth = 92
          ExplicitHeight = 14
        end
        inherited sbPesquisarArquivo: TSpeedButton
          Left = 850
          ExplicitLeft = 850
        end
        inherited eCaminhoArquivo: TEditLuka
          Width = 845
          Height = 22
          ExplicitWidth = 845
          ExplicitHeight = 22
        end
      end
    end
    object pnComprovantes: TPanel
      Left = 0
      Top = 42
      Width = 872
      Height = 529
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object splSeparador: TSplitter
        Left = 0
        Top = 212
        Width = 872
        Height = 6
        Cursor = crVSplit
        Align = alTop
        ResizeStyle = rsUpdate
      end
      object sgRO: TGridLuka
        Left = 0
        Top = 17
        Width = 872
        Height = 195
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alTop
        ColCount = 12
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 3
        FixedRows = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
        TabOrder = 0
        OnClick = sgROClick
        OnDrawCell = sgRODrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Resumo'
          'Nr.estab.'
          'Status pagto'
          'Valor bruto'
          'Valor ret.'
          'Valor l'#237'q.'
          'Data pagto'
          'Qtde.cvs'
          'Qtde.'#241'.ident.'
          'Valor bruto'
          'Valor ret.'
          'Valor l'#237'q.')
        OnGetCellColor = sgROGetCellColor
        Grid3D = False
        RealColCount = 15
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          78
          105
          75
          80
          61
          65
          70
          53
          76
          66
          56
          59)
      end
      object st1: TStaticTextLuka
        Left = 0
        Top = 218
        Width = 872
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Comprovantes de vendas do resumo de opera'#231#227'o focado'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object sgCV: TGridLuka
        Left = 0
        Top = 235
        Width = 872
        Height = 190
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 18
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 3
        FixedRows = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        PopupMenu = pmOpcoesCV
        TabOrder = 2
        OnDblClick = sgCVDblClick
        OnDrawCell = sgCVDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Ident.?'
          'C'#243'd.Hiva'
          'Cliente'
          'Tipo trans.'
          'Nr.crt.trunc.'
          'NSU'
          'C'#243'd.autoriz.'
          'Data venda'
          'Valor bruto'
          '% Ret.'
          'Valor ret.'
          'Parc.'
          'Qtd.parc.'
          'Bandeira'
          'Valor doc.'
          'Valor ret.'
          'Valor l'#237'q.'
          'Status')
        OnGetCellColor = sgCVGetCellColor
        OnGetCellPicture = sgCVGetCellPicture
        Grid3D = False
        RealColCount = 18
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          43
          56
          253
          84
          67
          62
          73
          72
          71
          42
          57
          32
          55
          103
          69
          64
          68
          74)
      end
      object st2: TStaticTextLuka
        Left = 0
        Top = 0
        Width = 872
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Resumo de opera'#231#245'es'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object pn4: TPanel
        Left = 0
        Top = 425
        Width = 872
        Height = 104
        Align = alBottom
        TabOrder = 4
        object st8: TStaticText
          Left = 210
          Top = 71
          Width = 108
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Valor l'#237'quido'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          Transparent = False
        end
        object st4: TStaticText
          Left = 0
          Top = 71
          Width = 104
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Valor bruto'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 1
          Transparent = False
        end
        object stValorBrutoAltis: TStaticTextLuka
          Left = 0
          Top = 86
          Width = 104
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 2
          Transparent = False
          AsInt = 0
          TipoCampo = tcNumerico
          CasasDecimais = 2
        end
        object stValorLiquidoAltis: TStaticTextLuka
          Left = 210
          Top = 86
          Width = 108
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8421440
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 3
          Transparent = False
          AsInt = 0
          TipoCampo = tcNumerico
          CasasDecimais = 2
        end
        object st3: TStaticText
          Left = 103
          Top = 71
          Width = 108
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Valor reten'#231#227'o'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 4
          Transparent = False
        end
        object stValorRetencaoAltis: TStaticTextLuka
          Left = 103
          Top = 86
          Width = 108
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 33023
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 5
          Transparent = False
          AsInt = 0
          TipoCampo = tcNumerico
          CasasDecimais = 2
        end
        object st9: TStaticTextLuka
          Left = 0
          Top = 54
          Width = 425
          Height = 17
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          Caption = 'Total identificados no Hiva ( Valor que ser'#225' baixado )'
          Color = 38619
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 6
          Transparent = False
          AsInt = 0
          TipoCampo = tcTexto
          CasasDecimais = 0
        end
        object st5: TStaticText
          Left = 210
          Top = 17
          Width = 108
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Valor l'#237'quido'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 7
          Transparent = False
        end
        object st6: TStaticText
          Left = 0
          Top = 17
          Width = 104
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Valor bruto'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 8
          Transparent = False
        end
        object stValorBrutoArquivo: TStaticTextLuka
          Left = 0
          Top = 32
          Width = 104
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 9
          Transparent = False
          AsInt = 0
          TipoCampo = tcNumerico
          CasasDecimais = 2
        end
        object stValorLiquidoArquivo: TStaticTextLuka
          Left = 210
          Top = 32
          Width = 108
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8421440
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 10
          Transparent = False
          AsInt = 0
          TipoCampo = tcNumerico
          CasasDecimais = 2
        end
        object st11: TStaticText
          Left = 103
          Top = 17
          Width = 108
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Valor reten'#231#227'o'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 11
          Transparent = False
        end
        object stValorRetencaoArquivo: TStaticTextLuka
          Left = 103
          Top = 32
          Width = 108
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 33023
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 12
          Transparent = False
          AsInt = 0
          TipoCampo = tcNumerico
          CasasDecimais = 2
        end
        object st13: TStaticTextLuka
          Left = 0
          Top = 0
          Width = 425
          Height = 17
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          Caption = 'Total do arquivo'
          Color = 38619
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 13
          Transparent = False
          AsInt = 0
          TipoCampo = tcTexto
          CasasDecimais = 0
        end
        object stQtdeCVsArquivo: TStaticTextLuka
          Left = 317
          Top = 32
          Width = 108
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '10'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clOlive
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 14
          Transparent = False
          AsInt = 0
          AsCurr = 10.000000000000000000
          AsDouble = 10.000000000000000000
          TipoCampo = tcNumerico
          CasasDecimais = 0
        end
        object st15: TStaticText
          Left = 317
          Top = 17
          Width = 108
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Qtde. CVs'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 15
          Transparent = False
        end
        object stQtdeCVsNaoIdentAltis: TStaticTextLuka
          Left = 317
          Top = 86
          Width = 108
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '10'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clOlive
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 16
          Transparent = False
          AsInt = 0
          AsCurr = 10.000000000000000000
          AsDouble = 10.000000000000000000
          TipoCampo = tcNumerico
          CasasDecimais = 0
        end
        object st17: TStaticText
          Left = 317
          Top = 71
          Width = 108
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Qtde.CVs '#241'.ident.'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 17
          Transparent = False
        end
        object sgValorBaixaConta: TGridLuka
          Left = 431
          Top = 18
          Width = 363
          Height = 84
          ColCount = 3
          DefaultRowHeight = 19
          DrawingStyle = gdsGradient
          FixedColor = 15395562
          FixedCols = 0
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
          TabOrder = 18
          OnDrawCell = sgValorBaixaContaDrawCell
          IncrementExpandCol = 0
          IncrementExpandRow = 0
          CorLinhaFoco = 16768407
          CorFundoFoco = 16774625
          CorLinhaDesfoque = 14869218
          CorFundoDesfoque = 16382457
          CorSuperiorCabecalho = clWhite
          CorInferiorCabecalho = 13553358
          CorSeparadorLinhas = 12040119
          CorColunaFoco = clMenuHighlight
          HCol.Strings = (
            'Conta'
            'Valor l'#237'q.d'#233'bito'
            'Valor l'#237'q.cr'#233'dito')
          Grid3D = False
          RealColCount = 3
          Indicador = True
          AtivarPopUpSelecao = False
          ColWidths = (
            128
            99
            110)
        end
        object st7: TStaticTextLuka
          Left = 431
          Top = 0
          Width = 363
          Height = 18
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          Caption = 'Valores da baixa por conta'
          Color = 38619
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 19
          Transparent = False
          AsInt = 0
          TipoCampo = tcTexto
          CasasDecimais = 0
        end
      end
    end
  end
  object pmOpcoesCV: TPopupMenu
    Left = 922
    Top = 320
    object miIdentificarManualmente: TMenuItem
      Caption = 'Identificar manualmente'
      OnClick = miIdentificarManualmenteClick
    end
  end
end
