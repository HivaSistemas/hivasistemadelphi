inherited FormMovimentosFinanceiros: TFormMovimentosFinanceiros
  Caption = 'Movimentos financeiros'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    object SpeedButton2: TSpeedButton [2]
      Left = 4
      Top = 214
      Width = 110
      Height = 40
      BiDiMode = bdLeftToRight
      Caption = 'Gerar Planilha'
      Flat = True
      NumGlyphs = 2
      ParentBiDiMode = False
      OnClick = sbImprimirClick
    end
  end
  inherited pcDados: TPageControl
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object lb1: TLabel
        Left = 385
        Top = 242
        Width = 73
        Height = 14
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alCustom
        Caption = 'Agrupamento'
      end
      inline FrEmpresas: TFrEmpresas
        Left = 1
        Top = 0
        Width = 336
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 1
        ExplicitWidth = 336
        inherited sgPesquisa: TGridLuka
          Width = 333
          ExplicitWidth = 333
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 336
          ExplicitWidth = 336
          inherited lbNomePesquisa: TLabel
            Width = 53
            ExplicitWidth = 53
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 231
            ExplicitLeft = 231
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 333
          Width = 3
          Visible = False
          ExplicitLeft = 333
          ExplicitWidth = 3
          inherited sbPesquisa: TSpeedButton
            Left = -19
            ExplicitLeft = -21
          end
        end
      end
      inline FrDataAbertura: TFrDataInicialFinal
        Left = 385
        Top = 1
        Width = 217
        Height = 40
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 385
        ExplicitTop = 1
        ExplicitWidth = 217
        ExplicitHeight = 40
        inherited Label1: TLabel
          Left = 1
          Width = 107
          Height = 14
          Caption = 'Data do movimento'
          ExplicitLeft = 1
          ExplicitWidth = 107
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          Caption = #224'te'
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Left = 111
          Height = 22
          ExplicitLeft = 111
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      object cbAgrupamento: TComboBoxLuka
        Left = 385
        Top = 256
        Width = 195
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 2
        Text = 'Nenhum'
        Items.Strings = (
          'Nenhum'
          'Tipo de movimento'
          'Cadastros')
        Valores.Strings = (
          'NEN'
          'TIP'
          'CAD')
        AsInt = 0
        AsString = 'NEN'
      end
      object gbFormasPagamento: TGroupBoxLuka
        Left = 385
        Top = 146
        Width = 332
        Height = 86
        Caption = 'Formas de pagamento    '
        TabOrder = 3
        OpcaoMarcarDesmarcar = True
        object ckCartao: TCheckBox
          Left = 14
          Top = 58
          Width = 88
          Height = 17
          Caption = 'Cart'#227'o'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object ckFinanceira: TCheckBox
          Left = 87
          Top = 17
          Width = 81
          Height = 17
          Caption = 'Financeira'
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
        object ckCheque: TCheckBox
          Left = 14
          Top = 37
          Width = 88
          Height = 17
          Caption = 'Cheque'
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
        object ckCobranca: TCheckBox
          Left = 86
          Top = 37
          Width = 80
          Height = 17
          Caption = 'Cobran'#231'a'
          Checked = True
          State = cbChecked
          TabOrder = 3
        end
        object ckAcumulado: TCheckBox
          Left = 86
          Top = 58
          Width = 82
          Height = 17
          Caption = 'Acumulado'
          Checked = True
          State = cbChecked
          TabOrder = 4
        end
        object ckDinheiro: TCheckBoxLuka
          Left = 14
          Top = 17
          Width = 71
          Height = 17
          Caption = 'Dinheiro'
          Checked = True
          State = cbChecked
          TabOrder = 5
          CheckedStr = 'S'
        end
        object ckPix: TCheckBox
          Left = 174
          Top = 17
          Width = 88
          Height = 17
          Caption = 'Pix'
          Checked = True
          State = cbChecked
          TabOrder = 6
        end
      end
      inline FrCadastros: TFrameCadastros
        Left = 0
        Top = 93
        Width = 360
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 4
        TabStop = True
        ExplicitTop = 93
        ExplicitWidth = 360
        inherited sgPesquisa: TGridLuka
          Width = 335
          ExplicitLeft = -8
          ExplicitWidth = 311
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 360
          ExplicitWidth = 336
          inherited lbNomePesquisa: TLabel
            Width = 117
            Caption = 'Clintes/Fornecedores'
            ExplicitWidth = 117
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 255
            ExplicitLeft = 231
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 335
          ExplicitLeft = 311
        end
      end
      object gbTipoMovimento: TGroupBoxLuka
        Left = 385
        Top = 43
        Width = 195
        Height = 94
        Caption = 'Tipo de movimento'
        TabOrder = 5
        OpcaoMarcarDesmarcar = True
        object ckRecebimentoAcumulados: TCheckBoxLuka
          Left = 8
          Top = 35
          Width = 178
          Height = 17
          Caption = 'Recebimento de acumulados'
          Checked = True
          State = cbChecked
          TabOrder = 0
          CheckedStr = 'S'
          Valor = 'ACU'
        end
        object ckBaixaContasReceber: TCheckBoxLuka
          Left = 8
          Top = 52
          Width = 163
          Height = 17
          Caption = 'Baixa contas receber'
          Checked = True
          State = cbChecked
          TabOrder = 1
          CheckedStr = 'S'
          Valor = 'BXR'
        end
        object ckRecebimentoVenda: TCheckBoxLuka
          Left = 8
          Top = 18
          Width = 163
          Height = 17
          Caption = 'Recebimento de venda'
          Checked = True
          State = cbChecked
          TabOrder = 2
          CheckedStr = 'S'
          Valor = 'REV'
        end
        object ckBaixaContasPagar: TCheckBoxLuka
          Left = 8
          Top = 69
          Width = 167
          Height = 17
          Caption = 'Baixa contas pagar'
          Checked = True
          State = cbChecked
          TabOrder = 3
          CheckedStr = 'S'
          Valor = 'BXP'
        end
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object sgMovimentacoes: TGridLuka
        Left = 0
        Top = 0
        Width = 884
        Height = 518
        Align = alClient
        ColCount = 13
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 0
        OnDrawCell = sgMovimentacoesDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'C'#243'digo'
          'Opera'#231#227'o'
          'Cadastro'
          'Dinheiro'
          'Cheque'
          'Cart'#227'o'
          'Cobran'#231'a'
          'Acumulado'
          'Cr'#233'dito'
          'Pix'
          'Total'
          'Conta orig./dest. din.'
          'Data/hora movimento')
        OnGetCellColor = sgMovimentacoesGetCellColor
        Grid3D = False
        RealColCount = 13
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          67
          206
          200
          91
          84
          85
          83
          87
          92
          92
          108
          165
          138)
      end
    end
  end
end
