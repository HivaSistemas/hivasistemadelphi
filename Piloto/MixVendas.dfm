inherited FormMixVenda: TFormMixVenda
  Caption = 'Lucratividade'
  ClientHeight = 484
  ClientWidth = 1461
  OnShow = FormShow
  ExplicitWidth = 1467
  ExplicitHeight = 513
  PixelsPerInch = 96
  TextHeight = 14
  object lb11: TLabel [0]
    Left = 120
    Top = 260
    Width = 89
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Custo de compras'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lb12: TLabel [1]
    Left = 114
    Top = 292
    Width = 95
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Custo fixo (loja)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lb13: TLabel [2]
    Left = 96
    Top = 323
    Width = 113
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Taxa (cond. pag.)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lb15: TLabel [3]
    Left = 114
    Top = 427
    Width = 95
    Height = 14
    AutoSize = False
    Caption = 'Totais de custos'
    Font.Charset = ANSI_CHARSET
    Font.Color = 33023
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lb1: TLabel [4]
    Left = 612
    Top = 260
    Width = 70
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Produtos'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lb2: TLabel [5]
    Left = 612
    Top = 357
    Width = 70
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Out. despesas'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lb3: TLabel [6]
    Left = 622
    Top = 389
    Width = 60
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Desconto'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lb4: TLabel [7]
    Left = 600
    Top = 427
    Width = 82
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Total L'#237'quido'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lb7: TLabel [8]
    Left = 824
    Top = 358
    Width = 11
    Height = 13
    Caption = '%'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lb6: TLabel [9]
    Left = 824
    Top = 390
    Width = 11
    Height = 13
    Caption = '%'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lb5: TLabel [10]
    Left = 1096
    Top = 260
    Width = 70
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Lucro bruto'
    Font.Charset = ANSI_CHARSET
    Font.Color = clGreen
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lb8: TLabel [11]
    Left = 1096
    Top = 292
    Width = 70
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Lucro l'#237'quido'
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lb16: TLabel [12]
    Left = 1326
    Top = 293
    Width = 11
    Height = 13
    Caption = '%'
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lb17: TLabel [13]
    Left = 1326
    Top = 261
    Width = 11
    Height = 13
    Caption = '%'
    Font.Charset = ANSI_CHARSET
    Font.Color = clGreen
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lb9: TLabel [14]
    Left = 1096
    Top = 427
    Width = 70
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Indicador'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel [15]
    Left = 608
    Top = 325
    Width = 70
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Frete'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lb10: TLabel [16]
    Left = 74
    Top = 353
    Width = 135
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Deprecia'#231#227'o financeira'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lb14: TLabel [17]
    Left = 612
    Top = 292
    Width = 70
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Promocional'
    Font.Charset = ANSI_CHARSET
    Font.Color = clGreen
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  inherited pnOpcoes: TPanel
    Top = 447
    Width = 1461
    TabOrder = 17
    ExplicitTop = 447
    ExplicitWidth = 1245
  end
  object sgProdutos: TGridLuka
    Left = 2
    Top = 32
    Width = 1439
    Height = 190
    Align = alCustom
    ColCount = 18
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 3
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect]
    TabOrder = 18
    OnDrawCell = sgProdutosDrawCell
    OnSelectCell = sgProdutosSelectCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Produto'
      'Nome'
      'Res.'
      'Qtde.'
      'Pre'#231'o unit.'
      'Out. desp.'
      'Desc.'
      'Vl liquido'
      'Custo comp.'
      'Custo comp. unit.'
      'Custo fixo'
      'Taxas'
      'Deprecia'#231#227'o finan.'
      'Total custos'
      'Lcr. bruto'
      '% bruto'
      'Lcr. L'#237'q.'
      '% L'#237'q.')
    OnGetCellColor = sgProdutosGetCellColor
    Grid3D = False
    RealColCount = 19
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      58
      212
      32
      64
      73
      57
      59
      67
      71
      96
      64
      60
      107
      76
      69
      65
      68
      76)
    RowHeights = (
      19
      19)
  end
  object eTotalCustoEntrada: TEditLuka
    Left = 211
    Top = 253
    Width = 120
    Height = 21
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 0
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eTotalImpostos: TEditLuka
    Left = 211
    Top = 285
    Width = 120
    Height = 21
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eTotalEncargos: TEditLuka
    Left = 211
    Top = 316
    Width = 120
    Height = 21
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clOlive
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 2
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eTotalCustoFinal: TEditLuka
    Left = 211
    Top = 424
    Width = 120
    Height = 21
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = 33023
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 3
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object stSPC: TStaticText
    Left = 2
    Top = 228
    Width = 465
    Height = 15
    Alignment = taCenter
    AutoSize = False
    Caption = 'Custos'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 19
    Transparent = False
  end
  object eTotalProdutos: TEditLuka
    Left = 684
    Top = 253
    Width = 97
    Height = 21
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 4
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eTotalOutrasDesp: TEditLuka
    Left = 684
    Top = 350
    Width = 97
    Height = 21
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    Text = '0,00'
    OnChange = eTotalOutrasDespChange
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eTotalDesconto: TEditLuka
    Left = 684
    Top = 382
    Width = 97
    Height = 21
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 9
    Text = '0,00'
    OnChange = eTotalDescontoChange
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eTotalLiquido: TEditLuka
    Left = 684
    Top = 424
    Width = 151
    Height = 21
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 11
    Text = '0,00'
    OnChange = eTotalLiquidoChange
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object st1: TStaticText
    Left = 976
    Top = 228
    Width = 465
    Height = 15
    Alignment = taCenter
    AutoSize = False
    Caption = 'Resultado final'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 20
    Transparent = False
  end
  object eTotalPercOutrasDesp: TEditLuka
    Left = 784
    Top = 350
    Width = 38
    Height = 21
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    MaxLength = 5
    ParentFont = False
    ReadOnly = True
    TabOrder = 8
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eTotalPercDesconto: TEditLuka
    Left = 784
    Top = 382
    Width = 38
    Height = 21
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    MaxLength = 5
    ParentFont = False
    TabOrder = 10
    Text = '0,00'
    OnChange = eTotalPercDescontoChange
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eTotalLucroBruto: TEditLuka
    Left = 1172
    Top = 253
    Width = 97
    Height = 21
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clGreen
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 12
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eTotalLucroLiquido: TEditLuka
    Left = 1172
    Top = 285
    Width = 97
    Height = 21
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 14
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eTotalPercLucroLiquido: TEditLuka
    Left = 1272
    Top = 285
    Width = 52
    Height = 21
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    MaxLength = 5
    ParentFont = False
    ReadOnly = True
    TabOrder = 15
    Text = '0,0000'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 4
    NaoAceitarEspaco = False
  end
  object eTotalPercLucroBruto: TEditLuka
    Left = 1272
    Top = 253
    Width = 52
    Height = 21
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clGreen
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    MaxLength = 5
    ParentFont = False
    ReadOnly = True
    TabOrder = 13
    Text = '0,0000'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 4
    NaoAceitarEspaco = False
  end
  object stResultadoVenda: TStaticText
    Left = 1172
    Top = 424
    Width = 165
    Height = 21
    Alignment = taCenter
    AutoSize = False
    BorderStyle = sbsSingle
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 16
    Transparent = False
  end
  object st3: TStaticText
    Left = 2
    Top = 1
    Width = 669
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Cliente'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 21
    Transparent = False
  end
  object stCliente: TStaticText
    Left = 2
    Top = 17
    Width = 669
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 22
    Transparent = False
  end
  object st4: TStaticText
    Left = 772
    Top = 1
    Width = 669
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Condi'#231#227'o de pagamento'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 23
    Transparent = False
  end
  object stCondicaoPagamento: TStaticText
    Left = 772
    Top = 17
    Width = 669
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 24
    Transparent = False
  end
  object eValorFrete: TEditLuka
    Left = 684
    Top = 318
    Width = 97
    Height = 21
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 6
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eTotalCustoVenda: TEditLuka
    Left = 211
    Top = 350
    Width = 120
    Height = 21
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 25
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eTotalPromocional: TEditLuka
    Left = 684
    Top = 285
    Width = 97
    Height = 21
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clGreen
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 5
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object StaticText1: TStaticText
    Left = 482
    Top = 228
    Width = 479
    Height = 15
    Alignment = taCenter
    AutoSize = False
    Caption = 'Totais gerais'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 26
    Transparent = False
  end
end
