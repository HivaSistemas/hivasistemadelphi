inherited FormCancelarRecebimentoPedido: TFormCancelarRecebimentoPedido
  Caption = 'Cancelar recebimento de pedido'
  ClientHeight = 149
  ClientWidth = 481
  ExplicitWidth = 487
  ExplicitHeight = 178
  PixelsPerInch = 96
  TextHeight = 14
  object lb2: TLabel [0]
    Left = 125
    Top = 53
    Width = 39
    Height = 14
    Caption = 'Cliente'
  end
  object lb12: TLabel [1]
    Left = 301
    Top = 53
    Width = 52
    Height = 14
    Caption = 'Vendedor'
  end
  object lb18: TLabel [2]
    Left = 125
    Top = 97
    Width = 132
    Height = 14
    Caption = 'Condi'#231#227'o de pagamento'
  end
  object lb3: TLabel [3]
    Left = 378
    Top = 5
    Width = 76
    Height = 14
    Caption = 'Data cadastro'
  end
  object lb5: TLabel [4]
    Left = 301
    Top = 97
    Width = 27
    Height = 14
    Caption = 'Total'
  end
  inherited Label1: TLabel
    Left = 125
    Width = 38
    Caption = 'Pedido'
    ExplicitLeft = 125
    ExplicitWidth = 38
  end
  object sbInformacoesOrcamento: TSpeedButtonLuka [6]
    Left = 205
    Top = 23
    Width = 18
    Height = 18
    Hint = 'Abrir informa'#231#245'es do pedido'
    Flat = True
    NumGlyphs = 2
    OnClick = sbInformacoesOrcamentoClick
    TagImagem = 13
    PedirCertificacao = False
    PermitirAutOutroUsuario = False
  end
  inherited pnOpcoes: TPanel
    Height = 149
    ExplicitHeight = 149
    inherited sbDesfazer: TSpeedButton
      Top = 89
      ExplicitTop = 89
    end
    inherited sbExcluir: TSpeedButton
      Left = -112
      ExplicitLeft = -112
    end
    inherited sbPesquisar: TSpeedButton
      Top = 44
      ExplicitTop = 44
    end
  end
  inherited eID: TEditLuka
    Left = 125
    TabOrder = 6
    ExplicitLeft = 125
  end
  inherited ckAtivo: TCheckBoxLuka
    TabOrder = 7
  end
  object eCliente: TEditLuka
    Left = 125
    Top = 67
    Width = 171
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 1
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eVendedor: TEditLuka
    Left = 301
    Top = 67
    Width = 175
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 3
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eCondicaoPagamento: TEditLuka
    Left = 125
    Top = 111
    Width = 171
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 5
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eDataCadastro: TEditLukaData
    Left = 378
    Top = 19
    Width = 98
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1; '
    MaxLength = 10
    ReadOnly = True
    TabOrder = 2
    Text = '  /  /    '
  end
  object eTotalOrcamento: TEditLuka
    Left = 301
    Top = 111
    Width = 175
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 4
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
end
