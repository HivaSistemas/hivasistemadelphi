unit Frame.EnderecoEstoqueModulo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _Sessao, _Biblioteca,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _EnderecoEstoqueModulo, Pesquisa.EnderecoEstoqueModulo,
  _RecordsCadastros, System.Math, Vcl.Buttons, Vcl.Menus;

type
  TFrEnderecoEstoqueModulo = class(TFrameHenrancaPesquisas)
  public
    function GetEnderecoEstoqueModulo(pLinha: Integer = -1): RecEnderecoEstoqueModulo;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrameEnderecoEstoqueModulo }

function TFrEnderecoEstoqueModulo.AdicionarDireto: TObject;
var
  vMotivos: TArray<RecEnderecoEstoqueModulo>;
begin
  vMotivos := _EnderecoEstoqueModulo.BuscarEnderecoEstoqueModulo(Sessao.getConexaoBanco, 0, [FChaveDigitada]);
  if vMotivos = nil then
    Result := nil
  else
    Result := vMotivos[0];
end;

function TFrEnderecoEstoqueModulo.AdicionarPesquisando: TObject;
begin
  Result := Pesquisa.EnderecoEstoqueModulo.Pesquisar();
end;

function TFrEnderecoEstoqueModulo.GetEnderecoEstoqueModulo(pLinha: Integer): RecEnderecoEstoqueModulo;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecEnderecoEstoqueModulo(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrEnderecoEstoqueModulo.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecEnderecoEstoqueModulo(FDados[i]).Modulo_id = RecEnderecoEstoqueModulo(pSender).Modulo_id then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrEnderecoEstoqueModulo.MontarGrid;
var
  i: Integer;
  vSender: RecEnderecoEstoqueModulo;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecEnderecoEstoqueModulo(FDados[i]);
      AAdd([IntToStr(vSender.Modulo_id), vSender.descricao]);
    end;
  end;
end;

end.
