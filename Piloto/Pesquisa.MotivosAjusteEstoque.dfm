inherited FormPesquisaMotivosAjusteEstoque: TFormPesquisaMotivosAjusteEstoque
  Caption = 'Pesquisa de motivos de ajuste de estoque'
  ClientHeight = 218
  ClientWidth = 464
  ExplicitWidth = 470
  ExplicitHeight = 246
  PixelsPerInch = 96
  TextHeight = 14
  inherited eValorPesquisa: TEditLuka
    Width = 304
    ExplicitWidth = 304
  end
  inherited sgPesquisa: TGridLuka
    Width = 456
    Height = 171
    ColCount = 4
    OnDrawCell = sgPesquisaDrawCell
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Descri'#231#227'o'
      'Ativo')
    OnGetCellColor = sgPesquisaGetCellColor
    RealColCount = 4
    ExplicitWidth = 456
    ExplicitHeight = 171
    ColWidths = (
      28
      55
      217
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Top = 192
    Text = '0'
    ExplicitTop = 192
  end
end
