unit InformacoesConhecimentoFrete;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Biblioteca, _Sessao,
  Vcl.ExtCtrls, Vcl.StdCtrls, EditLuka, Vcl.Mask, EditLukaData, StaticTextLuka, _ConhecimentosFretes,
  Vcl.Grids, GridLuka, _ConhecimentosFretesItens, Vcl.ComCtrls, _ContasPagar, _RecordsFinanceiros,
  Informacoes.TituloPagar;

type
  TFormInformacoesConhecimentoFrete = class(TFormHerancaFinalizar)
    lb1: TLabel;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl27: TLabel;
    eConhecimentoId: TEditLuka;
    eTransportadora: TEditLuka;
    eNumero: TEditLuka;
    eSerie: TEditLuka;
    eModelo: TEditLuka;
    eEmpresaNota: TEditLuka;
    pcDados: TPageControl;
    tsPrincipais: TTabSheet;
    lbl14: TLabel;
    eCFOPNota: TEditLuka;
    lbllb3: TLabel;
    eDataCadastro: TEditLukaData;
    lbl28: TLabel;
    eChaveAcessoNFe: TMaskEdit;
    lbl5: TLabel;
    eDataEmissao: TEditLukaData;
    lbl15: TLabel;
    lbl16: TLabel;
    lbl17: TLabel;
    eValorBaseCalculoICMS: TEditLuka;
    eValorICMS: TEditLuka;
    ePercICMS: TEditLuka;
    lb2: TLabel;
    eValorTotal: TEditLuka;
    sgNotasConhecimento: TGridLuka;
    st1: TStaticTextLuka;
    lb3: TLabel;
    eUsuarioCadastro: TEditLuka;
    tsFinanceiro: TTabSheet;
    sgFinanceiros: TGridLuka;
    procedure sgNotasConhecimentoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgFinanceirosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure tsFinanceiroShow(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sgFinanceirosDblClick(Sender: TObject);
  end;

procedure Informar(pConhecimentoId: Integer);

implementation

{$R *.dfm}

const
  cnFornecedor  = 0;
  cnNumeroNF    = 1;
  cnModelo      = 2;
  cnSerie       = 3;
  cnDataEmissao = 4;
  cnPeso        = 5;
  cnQuantidade  = 6;
  cnValorNota   = 7;
  (* Ocultas *)
  cnFornecedorId = 8;

  (* Grid financeiros *)
  cpPagarId        = 0;
  cpTipoCobranca   = 1;
  cpValor          = 2;
  cpStatus         = 3;
  cpDataVencimento = 4;

procedure Informar(pConhecimentoId: Integer);
var
  i: Integer;

  vConhec: TArray<RecConhecimentosFretes>;
  vForm: TFormInformacoesConhecimentoFrete;
  vItens: TArray<RecConhecimentosFretesItens>;
begin
  if pConhecimentoId = 0 then
    Exit;

  vConhec := _ConhecimentosFretes.BuscarConhecimento(Sessao.getConexaoBanco, 0, [pConhecimentoId]);
  if vConhec = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  vItens := _ConhecimentosFretesItens.BuscarItens(Sessao.getConexaoBanco, 0, [pConhecimentoId]);

  vForm := TFormInformacoesConhecimentoFrete.Create(Application);

  vForm.eConhecimentoId.SetInformacao(vConhec[0].ConhecimentoId);
  vForm.eTransportadora.SetInformacao(vConhec[0].TransportadoraId, vConhec[0].NomeTransportadora);
  vForm.eEmpresaNota.SetInformacao(vConhec[0].EmpresaId, vConhec[0].NomeEmpresa);

  vForm.eNumero.AsInt := vConhec[0].Numero;
  vForm.eModelo.Text  := vConhec[0].Modelo;
  vForm.eSerie.Text   := vConhec[0].Serie;
  vForm.eCFOPNota.SetInformacao(vConhec[0].CfopId, vConhec[0].NomeCfop);

  vForm.eDataCadastro.AsData := vConhec[0].DataHoraCadastro;
  vForm.eDataEmissao.AsData  := vConhec[0].DataEmissao;

  vForm.eChaveAcessoNFe.Text         := vConhec[0].ChaveConhecimento;
  vForm.eValorBaseCalculoICMS.AsCurr := vConhec[0].BaseCalculoIcms;
  vForm.ePercICMS.AsCurr             := vConhec[0].PercentualIcms;
  vForm.eValorICMS.AsCurr            := vConhec[0].ValorIcms;
  vForm.eValorTotal.AsCurr           := vConhec[0].ValorFrete;

  vForm.eUsuarioCadastro.SetInformacao(vConhec[0].UsuarioCadastroId, vConhec[0].NomeUsuarioCadastro);

  for i := Low(vItens) to High(vItens) do begin
    vForm.sgNotasConhecimento.Cells[cnFornecedor, i + 1]  := getInformacao(vItens[i].FornecedorNotaId, vItens[i].NomeFornecedor);
    vForm.sgNotasConhecimento.Cells[cnNumeroNF, i + 1]    := NFormat(vItens[i].NumeroNota);
    vForm.sgNotasConhecimento.Cells[cnModelo, i + 1]      := vItens[i].ModeloNota;
    vForm.sgNotasConhecimento.Cells[cnSerie, i + 1]       := vItens[i].SerieNota;
    vForm.sgNotasConhecimento.Cells[cnDataEmissao, i + 1] := DFormat(vItens[i].DataEmissaoNota);
    vForm.sgNotasConhecimento.Cells[cnPeso, i + 1]        := NFormatEstoque(vItens[i].PesoNota);
    vForm.sgNotasConhecimento.Cells[cnQuantidade, i + 1]  := NFormatEstoque(vItens[i].QuantidadeNota);
    vForm.sgNotasConhecimento.Cells[cnValorNota, i + 1]   := NFormatN(vItens[i].ValorTotalNota);
  end;
  vForm.sgNotasConhecimento.SetLinhasGridPorTamanhoVetor( Length(vItens) );

  vForm.abrirModalnformacoes;
end;

procedure TFormInformacoesConhecimentoFrete.FormShow(Sender: TObject);
begin
  inherited;
  pcDados.ActivePage := tsPrincipais;
end;

procedure TFormInformacoesConhecimentoFrete.sgFinanceirosDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.TituloPagar.Informar(SFormatInt(sgFinanceiros.Cells[cpPagarId, sgFinanceiros.Row]));
end;

procedure TFormInformacoesConhecimentoFrete.sgFinanceirosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[cpPagarId, cpValor] then
    vAlinhamento := taRightJustify
  else if ACol = cpStatus then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgFinanceiros.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesConhecimentoFrete.sgNotasConhecimentoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    cnNumeroNF,
    cnModelo,
    cnSerie,
    cnPeso,
    cnQuantidade,
    cnValorNota]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgNotasConhecimento.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesConhecimentoFrete.tsFinanceiroShow(Sender: TObject);
var
  i: Integer;
  vFinanceiros: TArray<RecContaPagar>;
begin
  inherited;
  sgFinanceiros.ClearGrid();

  vFinanceiros := _ContasPagar.BuscarContasPagar(Sessao.getConexaoBanco, 8, [eConhecimentoId.AsInt]);
  for i := Low(vFinanceiros) to High(vFinanceiros) do begin
    sgFinanceiros.Cells[cpPagarId, i + 1]        := NFormat(vFinanceiros[i].PagarId);
    sgFinanceiros.Cells[cpTipoCobranca, i + 1]   := NFormat(vFinanceiros[i].cobranca_id) + ' - ' + vFinanceiros[i].nome_tipo_cobranca;
    sgFinanceiros.Cells[cpValor, i + 1]          := NFormat(vFinanceiros[i].ValorDocumento);
    sgFinanceiros.Cells[cpStatus, i + 1]         := vFinanceiros[i].StatusAnalitico;
    sgFinanceiros.Cells[cpDataVencimento, i + 1] := DFormat(vFinanceiros[i].data_vencimento);
  end;
  sgFinanceiros.SetLinhasGridPorTamanhoVetor( Length(vFinanceiros) );
end;

end.
