unit FrameCfopParametrosFiscaisEmpresas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.Grids, _Biblioteca, _Sessao,
  GridLuka, Vcl.StdCtrls, StaticTextLuka, _CfopParametrosFiscaisEmpr, _RecordsCadastros,
  _Imagens;

type
  TFrCfopParametrosFiscaisEmpresas = class(TFrameHerancaPrincipal)
    st1: TStaticTextLuka;
    sgParametros: TGridLuka;
    procedure sgParametrosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgParametrosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgParametrosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
  private
    function getRecCfopParametrosFiscaisEmpr: TArray<RecCfopParametrosFiscaisEmpr>;
    procedure setCfopParametrosFiscaisEmpr(const pParametros: TArray<RecCfopParametrosFiscaisEmpr>);
  public
    constructor Create(AOwner: TComponent); override;
    procedure Clear; override;
    function EstaVazio: Boolean; override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
  published
    property ParametrosCFOPEmpresas: TArray<RecCfopParametrosFiscaisEmpr> read getRecCfopParametrosFiscaisEmpr write setCfopParametrosFiscaisEmpr;
  end;

implementation

{$R *.dfm}

{ TFrCfopParametrosFiscaisEmpresas }

const
  coEmpresaId         = 0;
  coNomeEmpresa       = 1;
  coCalcularPisCofins = 2;

procedure TFrCfopParametrosFiscaisEmpresas.Clear;
var
  i: Integer;
begin
  inherited;
  for i := 1 to sgParametros.RowCount -1 do
    sgParametros.Cells[coCalcularPisCofins, i] := charNaoSelecionado;
end;

constructor TFrCfopParametrosFiscaisEmpresas.Create(AOwner: TComponent);
begin
  inherited;
  sgParametros.Col := coCalcularPisCofins;
end;

function TFrCfopParametrosFiscaisEmpresas.EstaVazio: Boolean;
begin
  Result := False;
end;

function TFrCfopParametrosFiscaisEmpresas.getRecCfopParametrosFiscaisEmpr: TArray<RecCfopParametrosFiscaisEmpr>;
var
  i: Integer;
begin
  SetLength(Result, sgParametros.RowCount -1);
  for i := 1 to sgParametros.RowCount -1 do begin
    Result[i - 1].EmpresaId         := SFormatInt(sgParametros.Cells[coEmpresaId, i]);
    Result[i - 1].CalcularPisCofins := _Biblioteca.SelecaoToChar(sgParametros.Cells[coCalcularPisCofins, i]);
  end;
end;

procedure TFrCfopParametrosFiscaisEmpresas.Modo(pEditando, pLimpar: Boolean);
var
  i: Integer;
  vEmpresas: TArray<RecEmpresas>;
begin
  inherited;

  _Biblioteca.Habilitar([sgParametros], pEditando, pLimpar);

  if pEditando then begin
    vEmpresas := _Sessao.Sessao.getEmpresas;
    for i := Low(vEmpresas) to High(vEmpresas) do begin
      sgParametros.Cells[coEmpresaId, i + 1]         := NFormat(vEmpresas[i].EmpresaId);
      sgParametros.Cells[coNomeEmpresa, i + 1]       := vEmpresas[i].NomeFantasia;
      sgParametros.Cells[coCalcularPisCofins, i + 1] := _Biblioteca.charNaoSelecionado;
    end;
    sgParametros.SetLinhasGridPorTamanhoVetor( Length(vEmpresas) );
    sgParametros.Repaint;
  end;
end;

procedure TFrCfopParametrosFiscaisEmpresas.setCfopParametrosFiscaisEmpr(const pParametros: TArray<RecCfopParametrosFiscaisEmpr>);
var
  i: Integer;
  vPosic: Integer;
begin
  for i := Low(pParametros) to High(pParametros) do begin
    vPosic := sgParametros.Localizar([coEmpresaId], [NFormat(pParametros[i].EmpresaId)]);

    sgParametros.Cells[coCalcularPisCofins, vPosic] := _Biblioteca.ToSelecao(pParametros[i].CalcularPisCofins);
  end;
end;

procedure TFrCfopParametrosFiscaisEmpresas.sgParametrosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol = coEmpresaId then
    vAlinhamento := taRightJustify
  else if ACol = coCalcularPisCofins then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgParametros.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFrCfopParametrosFiscaisEmpresas.sgParametrosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if (ACol <> coCalcularPisCofins) or (sgParametros.Cells[ACol, ARow] = '') then
   Exit;

  if sgParametros.Cells[ACol, ARow] = charNaoSelecionado then
    APicture := _Imagens.FormImagens.im1.Picture
  else
    APicture := _Imagens.FormImagens.im2.Picture;
end;

procedure TFrCfopParametrosFiscaisEmpresas.sgParametrosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;

  if Key = VK_SPACE then begin
    if sgParametros.Col = coCalcularPisCofins then begin
      sgParametros.Cells[coCalcularPisCofins, sgParametros.Row] :=
        IIfStr(
          sgParametros.Cells[coCalcularPisCofins, sgParametros.Row] = charSelecionado,
          charNaoSelecionado,
          charSelecionado
        );
    end;
  end;
end;

end.
