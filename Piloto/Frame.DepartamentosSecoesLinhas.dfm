inherited FrameDepartamentosSecoesLinhas: TFrameDepartamentosSecoesLinhas
  Height = 88
  ExplicitHeight = 88
  inherited sgPesquisa: TGridLuka
    Height = 71
    ExplicitHeight = 70
    ColWidths = (
      90
      269)
  end
  inherited CkAspas: TCheckBox
    Checked = True
    State = cbChecked
  end
  inherited CkPesquisaNumerica: TCheckBox
    Checked = False
    State = cbUnchecked
  end
  inherited PnTitulos: TPanel
    inherited lbNomePesquisa: TLabel
      Width = 27
      Caption = 'Nome'
      ExplicitWidth = 27
    end
  end
  object rgNivel: TRadioGroup [8]
    Left = 113
    Top = 37
    Width = 259
    Height = 41
    Caption = 'Nivel'
    Columns = 5
    ItemIndex = 0
    Items.Strings = (
      'Todos'
      '1'
      '2'
      '3')
    TabOrder = 8
    Visible = False
  end
end
