unit FramePagamentoFinanceiro;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.StdCtrls, _RecordsFinanceiros,
  EditLuka, Vcl.Buttons, BuscarDadosCobrancaFinanceiro, BuscarDadosDinheiro, _Biblioteca,
  BuscarCreditosDisponiveis, _Sessao, _BibliotecaGenerica, Vcl.Imaging.pngimage,
  Vcl.ExtCtrls;

type
  TFrPagamentoFinanceiro = class(TFrameHerancaPrincipal)
    grpFechamento: TGroupBox;
    lbllb11: TLabel;
    lbllb12: TLabel;
    lbllb13: TLabel;
    lbllb14: TLabel;
    lbllb15: TLabel;
    lbllb10: TLabel;
    lbllb21: TLabel;
    lbllb7: TLabel;
    lbllb19: TLabel;
    lbllb1: TLabel;
    eValorDinheiro: TEditLuka;
    eValorCheque: TEditLuka;
    eValorCartaoDebito: TEditLuka;
    eValorCobranca: TEditLuka;
    eValorCredito: TEditLuka;
    stPagamento: TStaticText;
    eValorDiferencaPagamentos: TEditLuka;
    eValorDesconto: TEditLuka;
    ePercentualDesconto: TEditLuka;
    eValorTotalASerPago: TEditLuka;
    eValorJuros: TEditLuka;
    lb4: TLabel;
    eValorCartaoCredito: TEditLuka;
    stValorTotal: TStaticText;
    stDinheiroDefinido: TStaticText;
    stChequeDefinido: TStaticText;
    stCartaoDebitoDefinido: TStaticText;
    stCobrancaDefinido: TStaticText;
    stCreditoDefinido: TStaticText;
    stCartaoCreditoDefinido: TStaticText;
    eValorMulta: TEditLuka;
    lb1: TLabel;
    sbBuscarDadosDinheiro: TImage;
    sbBuscarDadosCheques: TImage;
    sbBuscarDadosCartoesDebito: TImage;
    sbBuscarDadosCartoesCredito: TImage;
    sbBuscarDadosCobranca: TImage;
    sbBuscarDadosCreditos: TImage;
    Label1: TLabel;
    eValorPix: TEditLuka;
    stPixDefinido: TStaticText;
    sbBuscarDadosPix: TImage;
    procedure sbBuscarDadosCobrancaClick(Sender: TObject);
    procedure sbBuscarDadosDinheiroClick(Sender: TObject);
    procedure sbBuscarDadosChequesClick(Sender: TObject);
    procedure sbBuscarDadosCartoesDebitoClick(Sender: TObject);
    procedure sbBuscarDadosCreditosClick(Sender: TObject);
    procedure eValorDinheiroKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eValorChequeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eValorCartaoDebitoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eValorCobrancaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eValorCreditoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure checarDefinidos(Sender: TObject);
    procedure eValorDescontoChange(Sender: TObject);
    procedure eValorCartaoCreditoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbBuscarDadosCartoesCreditoClick(Sender: TObject);
    procedure eValorMultaChange(Sender: TObject);
    procedure ePercentualDescontoChange(Sender: TObject);
    procedure eValorDinheiroDblClick(Sender: TObject);
    procedure sbBuscarDadosPixClick(Sender: TObject);
    procedure eValorPixKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure eValorPixDblClick(Sender: TObject);
  private
    FNatureza: string;
    FCadastroId: Integer;
    FCadastroIds: TArray<Integer>;
    FValorTitulos: Double;
    FCreditos: TArray<Integer>;
    FDadosPix: TArray<RecPagamentosPix>;

    FDadosDinheiro: TArray<RecPagamentosDinheiro>;
    FDadosCheques: TArray<RecTitulosFinanceiros>;
    FDadosCartoesDeb: TArray<RecTitulosFinanceiros>;
    FDadosCartoesCred: TArray<RecTitulosFinanceiros>;
    FDadosCobranca: TArray<RecTitulosFinanceiros>;

    function getValorPagar: Double;
    function getValorDinheiro: Double;
    function getValorCheque: Double;
    function getValorCartaoDebito: Double;
    function getValorCartaoCredito: Double;
    function getValorCobranca: Double;
    function getValorCredito: Double;
    function getValorPix: Double;

    procedure setValorTitulos(pValor: Double);
    procedure setValorPagar(pValor: Double);
    procedure setValorDinheiro(pValor: Double);
    procedure setValorCheque(pValor: Double);
    procedure setValorCartaoDebito(pValor: Double);
    procedure setValorCartaoCredito(pValor: Double);
    procedure setValorCobranca(pValor: Double);
    procedure setValorCredito(pValor: Double);

    function getDinheiro: TArray<RecTitulosBaixasPagtoDin>;
    procedure setDinheiro(pDinheiro: TArray<RecTitulosBaixasPagtoDin>);
    function getCartoesDebito: TArray<RecTitulosFinanceiros>;
    procedure setCartoesDebito(pCartoes: TArray<RecTitulosFinanceiros>);
    function getCartoesCredito: TArray<RecTitulosFinanceiros>;
    procedure setCartoesCredito(pCartoes: TArray<RecTitulosFinanceiros>);
    function getCobrancas: TArray<RecTitulosFinanceiros>;
    procedure setCobrancas(pCobrancas: TArray<RecTitulosFinanceiros>);
    function getCheques: TArray<RecTitulosFinanceiros>;
    procedure setCheques(pCobrancas: TArray<RecTitulosFinanceiros>);

    function getExisteDiferenca: Boolean;
    function getTotalPagamentos: Currency;
    function getPix: TArray<RecPagamentosPix>;
    procedure setPix(const Value: TArray<RecPagamentosPix>);
  public
    constructor Create(AOwner: TComponent); override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
    procedure Clear; override;
    procedure SetFocus; override;

    procedure ApenasDinheiro;
    procedure ApenasDinheiroCheque;
    procedure VerificarRegistro;
    procedure setUsuarioTemTurnoAberto;
    procedure TotalizarValoresASerPago;
    function ExisteFormasPagtoNaoDefinidas(pValidarCartoes: Boolean): Boolean;
    procedure setUtilizaTef(pUtilizadaTef: Boolean);

    procedure setNatureza(const pValor: string);
    procedure setCadastroId(const pValor: Integer);
    procedure setCadastroIds(const pValor: TArray<Integer>);
  published
    property Natureza: string read FNatureza write setNatureza;
    property ValorTitulos: Double read FValorTitulos write setValorTitulos;
    property ValorPagar: Double read getValorPagar write setValorPagar;
    property TotalPagamentos: Currency read getTotalPagamentos;
    property CadastroId: Integer read FCadastroId write setCadastroId;
    property CadastroIds: TArray<Integer> read FCadastroIds write setCadastroIds;
    property ValorDinheiro: Double read getValorDinheiro write setValorDinheiro;
    property ValorCheque: Double read getValorCheque write setValorCheque;
    property ValorCartaoDebito: Double read getValorCartaoDebito write setValorCartaoDebito;
    property ValorCartaoCredito: Double read getValorCartaoCredito write setValorCartaoCredito;
    property ValorCobranca: Double read getValorCobranca write setValorCobranca;
    property ValorCredito: Double read getValorCredito write setValorCredito;

    property Dinheiro: TArray<RecTitulosBaixasPagtoDin> read getDinheiro write setDinheiro;
    property CartoesDebito: TArray<RecTitulosFinanceiros> read getCartoesDebito write setCartoesDebito;
    property CartoesCredito: TArray<RecTitulosFinanceiros> read getCartoesCredito write setCartoesCredito;
    property Cobrancas: TArray<RecTitulosFinanceiros> read getCobrancas write setCobrancas;
    property Cheques: TArray<RecTitulosFinanceiros> read getCheques write setCheques;
    property Creditos: TArray<Integer> read FCreditos write FCreditos;
    property Pix: TArray<RecPagamentosPix> read getPix write setPix;

    property ExisteDiferenca: Boolean read getExisteDiferenca;
  end;

implementation

{$R *.dfm}

uses BuscarDadosCartoes;

procedure TFrPagamentoFinanceiro.sbBuscarDadosDinheiroClick(Sender: TObject);
begin
  inherited;
  if not sbBuscarDadosDinheiro.Enabled then
    Exit;

  if eValorDinheiro.AsCurr > 0 then
    FDadosDinheiro := BuscarDadosDinheiro.Buscar(eValorDinheiro.AsCurr, FDadosDinheiro).Dados;

  checarDefinidos(Sender);
end;

procedure TFrPagamentoFinanceiro.sbBuscarDadosPixClick(Sender: TObject);
begin
  inherited;
  if not sbBuscarDadosPix.Enabled then
    Exit;

  if eValorPix.AsCurr > 0 then
    FDadosPix := BuscarDadosDinheiro.BuscarDadosPix(eValorPix.AsCurr, FDadosPix).Dados;

  checarDefinidos(Sender);
end;

procedure TFrPagamentoFinanceiro.setCadastroId(const pValor: Integer);
begin
  FCadastroId := pValor;
end;

procedure TFrPagamentoFinanceiro.setCadastroIds(const pValor: TArray<Integer>);
begin
  FCadastroIds := pValor;
end;

procedure TFrPagamentoFinanceiro.setCartoesCredito(pCartoes: TArray<RecTitulosFinanceiros>);
begin
  FDadosCartoesCred := pCartoes;
  checarDefinidos(nil);
end;

procedure TFrPagamentoFinanceiro.setCartoesDebito(pCartoes: TArray<RecTitulosFinanceiros>);
begin
  FDadosCartoesDeb := pCartoes;
  checarDefinidos(nil);
end;

procedure TFrPagamentoFinanceiro.setCheques(pCobrancas: TArray<RecTitulosFinanceiros>);
begin

end;

procedure TFrPagamentoFinanceiro.setCobrancas(pCobrancas: TArray<RecTitulosFinanceiros>);
begin

end;

procedure TFrPagamentoFinanceiro.setDinheiro(pDinheiro: TArray<RecTitulosBaixasPagtoDin>);
begin

end;

procedure TFrPagamentoFinanceiro.SetFocus;
begin
  inherited;
  _Biblioteca.SetarFoco(eValorDinheiro);
end;

procedure TFrPagamentoFinanceiro.setNatureza(const pValor: string);
begin
  FNatureza := pValor;
end;

procedure TFrPagamentoFinanceiro.setPix(const Value: TArray<RecPagamentosPix>);
begin
//
end;

procedure TFrPagamentoFinanceiro.setUsuarioTemTurnoAberto;
begin
  sbBuscarDadosDinheiro.Enabled := False;
end;

procedure TFrPagamentoFinanceiro.setUtilizaTef(pUtilizadaTef: Boolean);
begin
  _Biblioteca.Visibilidade([
    sbBuscarDadosCartoesDebito,
    sbBuscarDadosCartoesCredito,
    stCartaoDebitoDefinido,
    stCartaoCreditoDefinido,
    stValorTotal],
    not pUtilizadaTef
  );
end;

procedure TFrPagamentoFinanceiro.setValorCartaoCredito(pValor: Double);
begin
  eValorCartaoCredito.AsDouble := pValor;
end;

procedure TFrPagamentoFinanceiro.setValorCartaoDebito(pValor: Double);
begin
  eValorCartaoDebito.AsDouble := pValor;
end;

procedure TFrPagamentoFinanceiro.setValorCheque(pValor: Double);
begin
  eValorCheque.AsDouble := pValor;
end;

procedure TFrPagamentoFinanceiro.setValorCobranca(pValor: Double);
begin
  eValorCobranca.AsDouble := pValor;
end;

procedure TFrPagamentoFinanceiro.setValorCredito(pValor: Double);
begin
  eValorCredito.AsDouble := pValor;
end;

procedure TFrPagamentoFinanceiro.setValorDinheiro(pValor: Double);
begin
  eValorDinheiro.AsDouble := pValor;
end;

procedure TFrPagamentoFinanceiro.setValorPagar(pValor: Double);
begin
  FValorTitulos                := pValor;
  eValorTotalASerPago.AsDouble := pValor;
end;

procedure TFrPagamentoFinanceiro.setValorTitulos(pValor: Double);
begin
  FValorTitulos := pValor;
  TotalizarValoresASerPago;
end;

procedure TFrPagamentoFinanceiro.TotalizarValoresASerPago;
var
  vTotalFormasPagamento: Currency;
begin
  eValorTotalASerPago.AsCurr := FValorTitulos + eValorMulta.AsCurr + eValorJuros.AsCurr - eValorDesconto.AsCurr;

  vTotalFormasPagamento :=
    eValorDinheiro.AsCurr +
    eValorCheque.AsCurr +
    eValorCartaoDebito.AsCurr +
    eValorCartaoCredito.AsCurr +
    eValorCobranca.AsCurr +
    eValorCredito.AsCurr;

  eValorDiferencaPagamentos.AsCurr := vTotalFormasPagamento - eValorTotalASerPago.AsCurr;
end;

procedure TFrPagamentoFinanceiro.VerificarRegistro;
begin
  // Valida��es do uso de cr�ditos
  if eValorCredito.AsCurr > 0 then begin
    if (eValorDinheiro.AsCurr + eValorCheque.AsCurr +  eValorCobranca.AsCurr) >= eValorTotalASerPago.AsCurr then begin
      _Biblioteca.Exclamar('As formas de pagamento j� definidas � maior ou igual ao valor a ser pago pelo pedido, n�o ser� poss�vel utilizar cr�ditos!');
      _Biblioteca.SetarFoco(eValorCredito);
      Abort;
    end;

    if (eValorCredito.AsCurr >= eValorTotalASerPago.AsCurr) and (eValorDinheiro.AsCurr + eValorCheque.AsCurr +  eValorCobranca.AsCurr > 0) then begin
      _Biblioteca.Exclamar('Os cr�ditos que est�o sendo utilizados j� cobrem o valor do pedido, n�o ser� poss�vel utilizar outras formas de pagamento!');
      _Biblioteca.SetarFoco(eValorDinheiro);
      Abort;
    end;
  end;

  if eValorPix.AsCurr > 0 then begin
    if FDadosPix = nil then begin
      _Biblioteca.Exclamar('Os dados da conta do Pix/Transfer�ncia n�o foram informados!');
      _Biblioteca.SetarFoco(eValorPix);
      Abort;
    end;
  end;

  if eValorDiferencaPagamentos.AsCurr < 0 then begin
    _Biblioteca.Exclamar('Existe diferen�a no valor a ser pago e as formas de pagamentos definidas!');
    _Biblioteca.SetarFoco(eValorDinheiro);
    Abort;
  end;

  if (eValorCheque.AsCurr > 0) and (Cheques = nil) then begin
    _Biblioteca.Exclamar('Os dados dos cheques n�o foram informados corretamente, verifique!');
    _Biblioteca.SetarFoco(eValorCheque);
    Abort;
  end;

  if (eValorCartaoDebito.AsCurr > 0) and (CartoesDebito = nil) then begin
    _Biblioteca.Exclamar('Os dados dos cart�es de d�bito n�o foram informados corretamente, verifique!');
    _Biblioteca.SetarFoco(eValorCartaoDebito);
    Abort;
  end;

  if (eValorCobranca.AsCurr > 0) and (Cobrancas = nil) then begin
    _Biblioteca.Exclamar('Os dados das cobran�as n�o foram informados corretamente, verifique!');
    _Biblioteca.SetarFoco(eValorCobranca);
    Abort;
  end;
end;

procedure TFrPagamentoFinanceiro.sbBuscarDadosChequesClick(Sender: TObject);
begin
  inherited;
  if eValorCheque.AsDouble > 0 then
    FDadosCheques := BuscarDadosCobrancaFinanceiro.Buscar(eValorCheque.AsDouble, 'CHQ', FNatureza, FDadosCheques).Dados;

  checarDefinidos(Sender);
end;

procedure TFrPagamentoFinanceiro.ApenasDinheiro;
begin
  _Biblioteca.Habilitar([
    eValorCheque,
    eValorCartaoDebito,
    eValorCartaoCredito,
    eValorCobranca,
    sbBuscarDadosCheques,
    sbBuscarDadosCartoesDebito,
    sbBuscarDadosCartoesCredito,
    sbBuscarDadosCobranca],
    False,
    True
  );
end;

procedure TFrPagamentoFinanceiro.ApenasDinheiroCheque;
begin
  ApenasDinheiro;
  _Biblioteca.Habilitar([
    eValorCheque,
    sbBuscarDadosCheques],
    True,
    True
  );
end;

procedure TFrPagamentoFinanceiro.checarDefinidos;
var
  i: Integer;
  vTotal: Currency;

  procedure DefinirStaticText(pStatic: TStaticText; pDefinido: Boolean);
  begin
    pStatic.Caption    := IIfStr(pDefinido, 'SIM', 'N�O');
    pStatic.Font.Color := IIfInt(pDefinido, clBlue, clRed);
  end;

begin
  DefinirStaticText(stDinheiroDefinido, False);
  if eValorDinheiro.AsCurr > 0 then begin
    vTotal := 0;
    for i := Low(FDadosDinheiro) to High(FDadosDinheiro) do
      vTotal := vTotal + FDadosDinheiro[i].Valor;

    DefinirStaticText(stDinheiroDefinido, eValorDinheiro.AsCurr = vTotal);
  end;

  DefinirStaticText(stChequeDefinido, False);
  if eValorCheque.AsCurr > 0 then begin
    vTotal := 0;
    for i := Low(FDadosCheques) to High(FDadosCheques) do
      vTotal := vTotal + FDadosCheques[i].Valor;

    DefinirStaticText(stChequeDefinido, eValorCheque.AsCurr = vTotal);
  end;

  DefinirStaticText(stCartaoDebitoDefinido, False);
  if eValorCartaoDebito.AsCurr > 0 then begin
    vTotal := 0;
    for i := Low(FDadosCartoesDeb) to High(FDadosCartoesDeb) do
      vTotal := vTotal + FDadosCartoesDeb[i].Valor;

    DefinirStaticText(stCartaoDebitoDefinido, eValorCartaoDebito.AsCurr = vTotal);
  end;

  DefinirStaticText(stCartaoCreditoDefinido, False);
  if eValorCartaoCredito.AsCurr > 0 then begin
    vTotal := 0;
    for i := Low(FDadosCartoesCred) to High(FDadosCartoesCred) do
      vTotal := vTotal + FDadosCartoesCred[i].Valor;

    DefinirStaticText(stCartaoCreditoDefinido, eValorCartaoCredito.AsCurr = vTotal);
  end;

  DefinirStaticText(stCobrancaDefinido, False);
  if eValorCobranca.AsCurr > 0 then begin
    vTotal := 0;
    for i := Low(FDadosCobranca) to High(FDadosCobranca) do
      vTotal := vTotal + FDadosCobranca[i].Valor;

    DefinirStaticText(stCobrancaDefinido, eValorCobranca.AsCurr = vTotal);
  end;

  DefinirStaticText(stCreditoDefinido, False);
  if eValorCredito.AsCurr > 0 then
    DefinirStaticText(stCreditoDefinido, True);

  DefinirStaticText(stPixDefinido, False);
  if eValorPix.AsCurr > 0 then begin
    vTotal := 0;
    for i := Low(FDadosPix) to High(FDadosPix) do
      vTotal := vTotal + FDadosPix[i].Valor;

    DefinirStaticText(stPixDefinido, eValorPix.AsCurr = vTotal);
  end;

  vTotal :=
    eValorDinheiro.AsCurr +
    eValorCheque.AsCurr +
    eValorCartaoDebito.AsCurr +
    eValorCartaoCredito.AsCurr +
    eValorCobranca.AsCurr +
    eValorCredito.AsCurr +
    eValorPix.AsCurr;

  eValorDiferencaPagamentos.AsCurr := vTotal - eValorTotalASerPago.AsCurr;
end;

procedure TFrPagamentoFinanceiro.Clear;
begin
  inherited;
  _Biblioteca.LimparCampos([
    eValorJuros,
    eValorDesconto,
    ePercentualDesconto,
    eValorTotalASerPago,
    eValorDinheiro,
    eValorCheque,
    eValorCartaoDebito,
    eValorCartaoCredito,
    eValorCobranca,
    eValorCredito,
    eValorPix,
    eValorDiferencaPagamentos]
  );

  FCreditos         := nil;
  FDadosDinheiro    := nil;
  FDadosCartoesDeb  := nil;
  FDadosCartoesCred := nil;
  FDadosCobranca    := nil;
  FDadosPix         := nil;
end;

constructor TFrPagamentoFinanceiro.Create(AOwner: TComponent);
begin
  inherited;
  eValorCartaoDebito.OnKeyDown  := eValorCartaoDebitoKeyDown;
  eValorCartaoCredito.OnKeyDown := eValorCartaoCreditoKeyDown;

  sbBuscarDadosCartoesDebito.OnClick  := sbBuscarDadosCartoesDebitoClick;
  sbBuscarDadosCartoesCredito.OnClick := sbBuscarDadosCartoesCreditoClick;
end;

procedure TFrPagamentoFinanceiro.ePercentualDescontoChange(Sender: TObject);
begin
  inherited;
  if ePercentualDesconto.Focused and (FValorTitulos > 0) then begin
    eValorDesconto.AsDouble :=
      _BibliotecaGenerica.ZeroSeNegativo((
        FValorTitulos +
        eValorJuros.AsDouble +
        eValorMulta.AsDouble) *
        ePercentualDesconto.AsDouble * 0.01
      );

    TotalizarValoresASerPago;
  end;
end;

procedure TFrPagamentoFinanceiro.eValorCartaoCreditoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if eValorCartaoCredito.AsCurr > 0 then
    sbBuscarDadosCartoesCreditoClick(Sender)
  else
    ProximoCampo(Sender, Key, Shift);
end;

procedure TFrPagamentoFinanceiro.eValorCartaoDebitoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if eValorCartaoDebito.AsCurr > 0 then
    sbBuscarDadosCartoesDebitoClick(Sender)
  else
    ProximoCampo(Sender, Key, Shift);
end;

procedure TFrPagamentoFinanceiro.eValorChequeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if eValorCheque.AsCurr > 0 then
    sbBuscarDadosChequesClick(Sender)
  else
    ProximoCampo(Sender, Key, Shift);
end;

procedure TFrPagamentoFinanceiro.eValorCobrancaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if eValorCobranca.AsCurr > 0 then
    sbBuscarDadosCobrancaClick(Sender)
  else
    ProximoCampo(Sender, Key, Shift);
end;

procedure TFrPagamentoFinanceiro.eValorCreditoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    sbBuscarDadosCreditosClick(Sender);
end;

procedure TFrPagamentoFinanceiro.eValorDescontoChange(Sender: TObject);
begin
  inherited;
  if (eValorDesconto.Focused or (Sender = nil)) and(FValorTitulos > 0) then begin
    ePercentualDesconto.AsDouble := Sessao.getCalculosSistema.CalcOrcamento.getPercDesconto(FValorTitulos, eValorMulta.AsDouble + eValorJuros.AsDouble, eValorDesconto.AsDouble);

    if eValorDesconto.Focused then
      TotalizarValoresASerPago;
  end;
end;

procedure TFrPagamentoFinanceiro.eValorDinheiroDblClick(Sender: TObject);
begin
  inherited;
  if TEditLuka(Sender).AsCurr > 0 then
    TEditLuka(Sender).AsCurr := 0
  else
    TEditLuka(Sender).AsCurr := Abs(eValorDiferencaPagamentos.AsCurr);
end;

procedure TFrPagamentoFinanceiro.eValorDinheiroKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if (eValorDinheiro.AsCurr > 0) and (sbBuscarDadosDinheiro.Enabled) and (sbBuscarDadosDinheiro.Visible) then
    sbBuscarDadosDinheiroClick(Sender)
  else
    ProximoCampo(Sender, Key, Shift);
end;

procedure TFrPagamentoFinanceiro.eValorMultaChange(Sender: TObject);
begin
  inherited;
  if (eValorJuros.Focused or (Sender = nil) or eValorMulta.Focused) and (FValorTitulos > 0) then begin
    eValorDescontoChange(nil);

    if eValorJuros.Focused or eValorMulta.Focused then
      TotalizarValoresASerPago;
  end;
end;

procedure TFrPagamentoFinanceiro.eValorPixDblClick(Sender: TObject);
begin
  inherited;
  if TEditLuka(Sender).AsCurr > 0 then
    TEditLuka(Sender).AsCurr := 0
  else
    TEditLuka(Sender).AsCurr := Abs(eValorDiferencaPagamentos.AsCurr);
end;

procedure TFrPagamentoFinanceiro.eValorPixKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if (eValorPix.AsCurr > 0) and (sbBuscarDadosPix.Enabled)  then
    sbBuscarDadosPixClick(Sender)
  else
    ProximoCampo(Sender, Key, Shift);
end;

function TFrPagamentoFinanceiro.getCartoesCredito: TArray<RecTitulosFinanceiros>;
begin
  Result := FDadosCartoesCred;
end;

function TFrPagamentoFinanceiro.getCartoesDebito: TArray<RecTitulosFinanceiros>;
begin
  Result := FDadosCartoesDeb;
end;

function TFrPagamentoFinanceiro.getCheques: TArray<RecTitulosFinanceiros>;
var
  i: Integer;
begin
  Result := nil;

  if FDadosCheques = nil then
    Exit;

  SetLength(Result, Length(FDadosCheques));
  for i := Low(FDadosCheques) to High(FDadosCheques) do
    Result[i] := FDadosCheques[i];
end;

function TFrPagamentoFinanceiro.getCobrancas: TArray<RecTitulosFinanceiros>;
var
  i: Integer;
begin
  Result := nil;

  if FDadosCobranca = nil then
    Exit;

  SetLength(Result, Length(FDadosCobranca));
  for i := Low(FDadosCobranca) to High(FDadosCobranca) do
    Result[i] := FDadosCobranca[i];
end;

function TFrPagamentoFinanceiro.getDinheiro: TArray<RecTitulosBaixasPagtoDin>;
var
  i: Integer;
begin
  Result := nil;

  if FDadosDinheiro = nil then
    Exit;

  SetLength(Result, Length(FDadosDinheiro));
  for i := Low(FDadosDinheiro) to High(FDadosDinheiro) do begin
    Result[i].BaixaId := 0;
    Result[i].ContaId := FDadosDinheiro[i].ContaId;
    Result[i].Valor   := FDadosDinheiro[i].Valor;
  end;
end;

function TFrPagamentoFinanceiro.getExisteDiferenca: Boolean;
begin
  Result := eValorDiferencaPagamentos.AsCurr <> 0;
end;

function TFrPagamentoFinanceiro.getPix: TArray<RecPagamentosPix>;
var
  i: Integer;
begin
  Result := nil;

  if FDadosPix = nil then
    Exit;

  SetLength(Result, Length(FDadosPix));
  for i := Low(FDadosPix) to High(FDadosPix) do begin
    Result[i].BaixaId := 0;
    Result[i].ContaId := FDadosPix[i].ContaId;
    Result[i].Valor   := FDadosPix[i].Valor;
  end;
end;

function TFrPagamentoFinanceiro.ExisteFormasPagtoNaoDefinidas(pValidarCartoes: Boolean): Boolean;

  function checarFormaNaoDefinida(pEditForma: TEditLuka; pStaticText: TStaticText):  Boolean;
  begin
    Result := (pEditForma.AsCurr > 0) and (pStaticText.Caption = 'N�O');
  end;

begin
  if pValidarCartoes then begin
    Result :=
     checarFormaNaoDefinida(eValorCheque, stChequeDefinido) or
     checarFormaNaoDefinida(eValorCartaoDebito, stCartaoDebitoDefinido) or
     checarFormaNaoDefinida(eValorCartaoCredito, stCartaoCreditoDefinido) or
     checarFormaNaoDefinida(eValorCobranca, stCobrancaDefinido) or
     checarFormaNaoDefinida(eValorCredito, stCreditoDefinido);
  end
  else begin
    Result :=
     checarFormaNaoDefinida(eValorCheque, stChequeDefinido) or
     checarFormaNaoDefinida(eValorCobranca, stCobrancaDefinido) or
     checarFormaNaoDefinida(eValorCredito, stCreditoDefinido);
  end;
end;

function TFrPagamentoFinanceiro.getValorCartaoCredito: Double;
begin
  Result := eValorCartaoCredito.AsDouble;
end;

function TFrPagamentoFinanceiro.getValorCartaoDebito: Double;
begin
  Result := eValorCartaoDebito.AsDouble;
end;

function TFrPagamentoFinanceiro.getValorCheque: Double;
begin
  Result := eValorCheque.AsDouble;
end;

function TFrPagamentoFinanceiro.getValorCobranca: Double;
begin
  Result := eValorCobranca.AsDouble;
end;

function TFrPagamentoFinanceiro.getValorCredito: Double;
begin
  Result := eValorCredito.AsDouble;
end;

function TFrPagamentoFinanceiro.getValorDinheiro: Double;
begin
  Result := eValorDinheiro.AsDouble;
end;

function TFrPagamentoFinanceiro.getValorPagar: Double;
begin
  Result := eValorTotalASerPago.AsDouble;
end;

function TFrPagamentoFinanceiro.getValorPix: Double;
begin
  Result := eValorPix.AsDouble;
end;

procedure TFrPagamentoFinanceiro.Modo(pEditando, pLimpar: Boolean);

  procedure SetarNaoCampos( pCampos: TArray<TStaticText> );
  var
    i: Integer;
  begin
    for i := Low(pCampos) to High(pCampos) do begin
      pCampos[i].Caption := 'N�O';
      pCampos[i].Font.Color := clRed;
    end;
  end;

begin
  inherited;
  _Biblioteca.Habilitar([
    eValorJuros,
    eValorDesconto,
    ePercentualDesconto,
    eValorTotalASerPago,
    eValorPix,
    eValorDinheiro,
    sbBuscarDadosDinheiro,
    eValorCheque,
    sbBuscarDadosCheques,
    eValorCartaoDebito,
    eValorCartaoCredito,
    sbBuscarDadosCartoesDebito,
    sbBuscarDadosCartoesCredito,
    eValorCobranca,
    sbBuscarDadosCobranca,
    eValorCredito,
    sbBuscarDadosCreditos,
    eValorDiferencaPagamentos,
    stDinheiroDefinido,
    stChequeDefinido,
    stCartaoDebitoDefinido,
    stCartaoCreditoDefinido,
    stCobrancaDefinido,
    stCreditoDefinido,
    stPixDefinido,
    sbBuscarDadosPix],
    pEditando,
    pLimpar
  );

  if pLimpar then begin
    FDadosDinheiro    := nil;
    FDadosCheques     := nil;
    FDadosCartoesDeb  := nil;
    FDadosCartoesCred := nil;
    FDadosCobranca    := nil;
    FCreditos         := nil;
  end;

  if pEditando then
    SetarNaoCampos([stDinheiroDefinido, stChequeDefinido, stCartaoDebitoDefinido, stCartaoCreditoDefinido, stCobrancaDefinido, stCreditoDefinido]);
end;

procedure TFrPagamentoFinanceiro.sbBuscarDadosCartoesCreditoClick(Sender: TObject);
begin
  inherited;
  if eValorCartaoCredito.AsCurr > 0 then
    FDadosCartoesCred := BuscarDadosCartoes.BuscarCartoes(0, 'T�tulo financeiro', eValorCartaoCredito.AsDouble, FDadosCartoesCred, FSomenteLeitura, 0, 'C').Dados;

  checarDefinidos(Sender);
end;

procedure TFrPagamentoFinanceiro.sbBuscarDadosCartoesDebitoClick(Sender: TObject);
begin
  inherited;
  if eValorCartaoDebito.AsCurr > 0 then
    FDadosCartoesDeb := BuscarDadosCartoes.BuscarCartoes(0, 'T�tulo financeiro', eValorCartaoDebito.AsDouble, FDadosCartoesDeb, False, 0, 'D').Dados;

  checarDefinidos(Sender);
end;

procedure TFrPagamentoFinanceiro.sbBuscarDadosCobrancaClick(Sender: TObject);
begin
  inherited;
  if eValorCobranca.AsCurr > 0 then
    FDadosCobranca := BuscarDadosCobrancaFinanceiro.Buscar(eValorCobranca.AsDouble, 'COB', FNatureza, FDadosCobranca).Dados;

  checarDefinidos(Sender);
end;

procedure TFrPagamentoFinanceiro.sbBuscarDadosCreditosClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar< RecCreditos >;
begin
  inherited;
  // Se for um contas a receber ent�o buscando os cr�ditos a pagar
  // Se for um contas a pagar ent�o buscando os cr�ditos a receber

  vRetTela := BuscarCreditosDisponiveis.Buscar(
    FCadastroId,
    FCadastroIds,
    FNatureza,
    False,
    nil
  );

  FCreditos := vRetTela.Dados.Creditos;
  eValorCredito.AsDouble := vRetTela.Dados.ValorTotal;

  checarDefinidos(Sender);
end;

function TFrPagamentoFinanceiro.getTotalPagamentos: Currency;
begin
  Result :=
    eValorDinheiro.AsCurr +
    eValorCheque.AsCurr +
    eValorCartaoDebito.AsCurr +
    eValorCartaoCredito.AsCurr +
    eValorCobranca.AsCurr +
    eValorCredito.AsCurr +
    eValorPix.asCurr;
end;

end.
