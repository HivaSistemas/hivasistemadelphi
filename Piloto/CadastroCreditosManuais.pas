unit CadastroCreditosManuais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _FrameHerancaPrincipal, _Biblioteca, _Sessao,
  _FrameHenrancaPesquisas, FrameEmpresas, FrameDadosCobranca,
  FramePlanosFinanceiros, FrameCentroCustos, Frame.Cadastros, MemoAltis,
  Vcl.Mask, EditLukaData, FramePagamentoFinanceiro, CheckBoxLuka;

type
  TFormCadastroCreditosManuais = class(TFormHerancaCadastroCodigo)
    FrEmpresa: TFrEmpresas;
    lbl1: TLabel;
    eValorCreditoGerar: TEditLuka;
    FrPlanoFinanceiro: TFrPlanosFinanceiros;
    FrCentroCusto: TFrCentroCustos;
    ckImportacao: TCheckBox;
    eDataVencimento: TEditLukaData;
    lbllb12: TLabel;
    meObservacoes: TMemoAltis;
    lbl2: TLabel;
    FrCadastro: TFrameCadastros;
    FrPagamentoFinanceiro: TFrPagamentoFinanceiro;
    procedure eValorCreditoGerarChange(Sender: TObject);
    procedure ckImportacaoClick(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure BuscarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroCreditosManuais }

procedure TFormCadastroCreditosManuais.BuscarRegistro;
begin
  _Biblioteca.Exclamar('N�o � permitido editar cr�dito lan�ados!');
  eID.Clear;
  Exit;

  inherited;
end;

procedure TFormCadastroCreditosManuais.ckImportacaoClick(Sender: TObject);
begin
  inherited;
  _Biblioteca.Habilitar([FrPagamentoFinanceiro], not ckImportacao.Checked, False);
end;

procedure TFormCadastroCreditosManuais.eValorCreditoGerarChange(Sender: TObject);
begin
  inherited;
  FrPagamentoFinanceiro.ValorPagar := eValorCreditoGerar.AsDouble;
end;

procedure TFormCadastroCreditosManuais.Modo(pEditando: Boolean);
begin
  inherited;

  _Biblioteca.Habilitar([
    FrEmpresa,
    FrCadastro,
    eValorCreditoGerar,
    eDataVencimento,
    meObservacoes,
    ckImportacao,
    FrPagamentoFinanceiro,
    FrPlanoFinanceiro,
    FrCentroCusto],
    pEditando
  );

  if pEditando then
    SetarFoco(FrEmpresa);
end;

procedure TFormCadastroCreditosManuais.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if FrEmpresa.EstaVazio then begin
    _Biblioteca.Exclamar('A empresa para gera��o de cr�dito n�o foi informada corretamente, verifique!');
    SetarFoco(FrEmpresa);
    Abort;
  end;

  if FrCadastro.EstaVazio then begin
    _Biblioteca.Exclamar('O cliente para gera��o de cr�dito n�o foi informado corretamente, verifique!');
    SetarFoco(FrCadastro);
    Abort;
  end;

  if eValorCreditoGerar.AsCurr = 0 then begin
    _Biblioteca.Exclamar('O valor do cr�dito a ser gerado n�o foi informado corretamente, verifique!');
    SetarFoco(eValorCreditoGerar);
    Abort;
  end;

  if FrPlanoFinanceiro.EstaVazio then begin
    _Biblioteca.Exclamar('O plano financeiro do cr�dito a ser gerado n�o foi informado corretamente, verifique!');
    SetarFoco(FrPlanoFinanceiro);
    Abort;
  end;

  if not eDataVencimento.DataOk then begin
    _Biblioteca.Exclamar('A data de vencimento do cr�dito n�o foi informada corretamente, verifique!');
    SetarFoco(eDataVencimento);
    Abort;
  end;

  if not ckImportacao.Checked then begin
    if FrPagamentoFinanceiro.ExisteDiferenca then begin
      _Biblioteca.Exclamar('As formas de pagamento n�o foram informadas corretamente, verifique!');
      SetarFoco(FrPagamentoFinanceiro);
      Abort;
    end;

    if FrPagamentoFinanceiro.ExisteFormasPagtoNaoDefinidas(True) then begin
      _Biblioteca.Exclamar('As formas de pagamento n�o foram bem definidas, verifique onde foram informados valores se os t�tulos foram informados corretamente!');
      SetarFoco(FrPagamentoFinanceiro);
      Abort;
    end;
  end;
end;

end.
