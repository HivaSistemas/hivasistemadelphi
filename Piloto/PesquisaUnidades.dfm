inherited FormPesquisaUnidades: TFormPesquisaUnidades
  Caption = 'Pesquisa de unidades'
  ClientHeight = 225
  ClientWidth = 390
  ExplicitWidth = 396
  ExplicitHeight = 254
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 390
    Height = 179
    ColCount = 4
    OnDrawCell = sgPesquisaDrawCell
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Descri'#231#227'o'
      'Ativo')
    RealColCount = 4
    ExplicitLeft = 0
    ExplicitTop = 46
    ExplicitWidth = 390
    ExplicitHeight = 179
    ColWidths = (
      28
      55
      206
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Top = 192
    Text = '0'
    ExplicitTop = 192
  end
  inherited pnFiltro1: TPanel
    Width = 390
    ExplicitWidth = 390
    inherited lblPesquisa: TLabel
      Left = 131
      Width = 38
      ExplicitLeft = 131
      ExplicitWidth = 38
    end
    inherited eValorPesquisa: TEditLuka
      Left = 131
      Width = 254
      ExplicitLeft = 131
      ExplicitWidth = 254
    end
    inherited cbOpcoesPesquisa: TComboBoxLuka
      Width = 125
      ExplicitWidth = 125
    end
  end
end
