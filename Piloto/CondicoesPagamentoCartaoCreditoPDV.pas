unit CondicoesPagamentoCartaoCreditoPDV;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _CondicoesCartaoCreditoPDV,
  _HerancaCadastro, Vcl.Buttons, Vcl.ExtCtrls, _FrameHerancaPrincipal, _Biblioteca, _FrameHenrancaPesquisas, FrameEmpresas, Vcl.Grids, GridLuka,
  FrameCondicoesPagamento, Vcl.StdCtrls, EditLuka, Vcl.Menus, _RecordsEspeciais, _Sessao;

type
  TFormCondicoesPagamentoCartaoCreditoPDV = class(TFormHerancaCadastro)
    FrEmpresa: TFrEmpresas;
    FrCondicoesPagamento: TFrCondicoesPagamento;
    sgCondicoes: TGridLuka;
    lb9: TLabel;
    eQtdeParcelas: TEditLuka;
    sbDescer: TSpeedButton;
    sbSubir: TSpeedButton;
    pmOpcoes: TPopupMenu;
    miDeletar: TMenuItem;
    procedure sbSubirClick(Sender: TObject);
    procedure sbDescerClick(Sender: TObject);
    procedure eQtdeParcelasKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgCondicoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure miDeletarClick(Sender: TObject);
  private
    procedure AddGrid(pCondicaoId: Integer; pNome: string; pQtdeParcelas: Integer);
    procedure FrEmpresaOnAposPesquisar(Sender: TObject);
    procedure FrEmpresaOnAposDeletar(Sender: TObject);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

const
  coCondicaoId   = 0;
  coNome         = 1;
  coQtdeParcelas = 2;

procedure TFormCondicoesPagamentoCartaoCreditoPDV.AddGrid(pCondicaoId: Integer; pNome: string; pQtdeParcelas: Integer);
var
  vLinha: Integer;
begin
  vLinha := sgCondicoes.Localizar([coCondicaoId], [NFormat(pCondicaoId)]);
  if vLinha = -1 then begin
    if sgCondicoes.Cells[coCondicaoId, 1] = '' then
      vLinha := 1
    else begin
      vLinha := sgCondicoes.RowCount;
      sgCondicoes.RowCount := sgCondicoes.RowCount + 1;
    end;
  end;

  sgCondicoes.Cells[coCondicaoId, vLinha] := NFormat(pCondicaoId);
  sgCondicoes.Cells[coNome, vLinha] := pNome;
  sgCondicoes.Cells[coQtdeParcelas, vLinha] := NFormat(pQtdeParcelas);
end;

procedure TFormCondicoesPagamentoCartaoCreditoPDV.BuscarRegistro;
var
  i: Integer;
  vCondicoes: TArray<RecCondicoesCartaoCreditoPDV>;
begin
  inherited;

  vCondicoes := _CondicoesCartaoCreditoPDV.BuscarCondicoesCartaoCreditoPDV(Sessao.getConexaoBanco, 0, [FrEmpresa.getEmpresa.EmpresaId]);
  if vCondicoes = nil then
    Exit;

  for i := Low(vCondicoes) to High(vCondicoes) do
    AddGrid(vCondicoes[i].CondicaoId, vCondicoes[i].NomeCondicao, vCondicoes[i].QuantidadeParcelas);
end;

procedure TFormCondicoesPagamentoCartaoCreditoPDV.eQtdeParcelasKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if FrCondicoesPagamento.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio informar a condi��o de pagamento!');
    SetarFoco(FrCondicoesPagamento);
    Exit;
  end;

  if eQtdeParcelas.AsInt = 0 then begin
    _Biblioteca.Exclamar('� necess�rio informar a quantidade de parcelas!');
    SetarFoco(eQtdeParcelas);
    Exit;
  end;

  AddGrid(FrCondicoesPagamento.getDados().condicao_id, FrCondicoesPagamento.getDados().nome, eQtdeParcelas.AsInt);

  _Biblioteca.LimparCampos([FrCondicoesPagamento, eQtdeParcelas]);
  SetarFoco(FrCondicoesPagamento);
end;

procedure TFormCondicoesPagamentoCartaoCreditoPDV.FormCreate(Sender: TObject);
begin
  inherited;
  FrEmpresa.OnAposPesquisar := FrEmpresaOnAposPesquisar;
  FrEmpresa.OnAposDeletar := FrEmpresaOnAposDeletar;
end;

procedure TFormCondicoesPagamentoCartaoCreditoPDV.FrEmpresaOnAposDeletar(Sender: TObject);
begin
  Modo(False);
end;

procedure TFormCondicoesPagamentoCartaoCreditoPDV.FrEmpresaOnAposPesquisar(Sender: TObject);
begin
  Modo(True);
  BuscarRegistro;
end;

procedure TFormCondicoesPagamentoCartaoCreditoPDV.GravarRegistro(Sender: TObject);
var
  vCondicoes: TArray<RecCondicoesCartaoCreditoPDV>;
  vRetBanco: RecRetornoBD;
  i: Integer;
begin
  inherited;

  vCondicoes := nil;
  for i := 1 to sgCondicoes.RowCount -1 do begin
    if sgCondicoes.Cells[coCondicaoId, i] = '' then
      Continue;

    SetLength(vCondicoes, Length(vCondicoes) + 1);

    vCondicoes[High(vCondicoes)].CondicaoId         := SFormatInt( sgCondicoes.Cells[coCondicaoId, i] );
    vCondicoes[High(vCondicoes)].Ordem              := i;
    vCondicoes[High(vCondicoes)].QuantidadeParcelas := SFormatInt( sgCondicoes.Cells[coQtdeParcelas, i] );
  end;

  vRetBanco := _CondicoesCartaoCreditoPDV.AtualizarCondicoesCartaoCreditoPDV(Sessao.getConexaoBanco, FrEmpresa.getEmpresa().EmpresaId, vCondicoes);
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
end;

procedure TFormCondicoesPagamentoCartaoCreditoPDV.miDeletarClick(Sender: TObject);
begin
  inherited;
  sgCondicoes.DeleteRow( sgCondicoes.Row );
end;

procedure TFormCondicoesPagamentoCartaoCreditoPDV.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    FrCondicoesPagamento,
    eQtdeParcelas,
    sgCondicoes,
    sbSubir,
    sbDescer],
    pEditando
  );

  if pEditando then
    SetarFoco(FrCondicoesPagamento)
  else begin
    _Biblioteca.Habilitar([FrEmpresa], True);
    SetarFoco(FrEmpresa);
  end;
end;

procedure TFormCondicoesPagamentoCartaoCreditoPDV.sbDescerClick(Sender: TObject);
begin
  inherited;
  if sgCondicoes.Row + 1 < sgCondicoes.RowCount then begin
    sgCondicoes.TrocaRow(sgCondicoes.Row + 1, sgCondicoes.Row);
    sgCondicoes.Row := sgCondicoes.Row + 1;
  end;
end;

procedure TFormCondicoesPagamentoCartaoCreditoPDV.sbSubirClick(Sender: TObject);
begin
  inherited;
  if sgCondicoes.Row - 1 > -1 then begin
    sgCondicoes.TrocaRow(sgCondicoes.Row -1, sgCondicoes.Row);
    sgCondicoes.Row := sgCondicoes.Row -1;
  end;
end;

procedure TFormCondicoesPagamentoCartaoCreditoPDV.sgCondicoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coCondicaoId, coQtdeParcelas] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgCondicoes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormCondicoesPagamentoCartaoCreditoPDV.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
