inherited FormBuscarFuncionario: TFormBuscarFuncionario
  Caption = 'Busca de usu'#225'rios'
  ClientHeight = 83
  ClientWidth = 409
  ExplicitWidth = 415
  ExplicitHeight = 112
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 46
    Width = 409
    ExplicitTop = 46
    ExplicitWidth = 409
  end
  inline FrFuncionario: TFrFuncionarios
    Left = 4
    Top = 3
    Width = 400
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 4
    ExplicitTop = 3
    ExplicitWidth = 400
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 375
      Height = 23
      ExplicitWidth = 375
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 400
      ExplicitWidth = 400
      inherited lbNomePesquisa: TLabel
        Width = 71
        ExplicitWidth = 71
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 330
        ExplicitLeft = 330
      end
    end
    inherited pnPesquisa: TPanel
      Left = 375
      Height = 24
      ExplicitLeft = 375
      ExplicitHeight = 24
    end
  end
end
