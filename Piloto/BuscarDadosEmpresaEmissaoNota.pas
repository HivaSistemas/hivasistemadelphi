unit BuscarDadosEmpresaEmissaoNota;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameEmpresas,
  _RecordsCadastros, _Biblioteca, Vcl.StdCtrls;

type
  TFormBuscarDadosEmpresaEmissaoNota = class(TFormHerancaFinalizar)
    FrEmpresas: TFrEmpresas;
  private
    //
  public
    { Public declarations }
  end;

function BuscarDadosEmpresaEmitente: TRetornoTelaFinalizar<RecEmpresas>;

implementation

{$R *.dfm}

{ BuscarDadosEmpresaEmitente }

function BuscarDadosEmpresaEmitente: TRetornoTelaFinalizar<RecEmpresas>;
var
  vForm: TFormBuscarDadosEmpresaEmissaoNota;
begin
  vForm := TFormBuscarDadosEmpresaEmissaoNota.Create(Application);

  if Result.Ok(vForm.ShowModal) then
    Result.Dados := vForm.FrEmpresas.getEmpresa(0);

  FreeAndNil(vForm);
end;

end.
