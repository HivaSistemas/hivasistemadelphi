unit _ContasRecHistoricoCobrancas;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecContasRecHistoricoCobrancas = record
    ReceberId: Integer;
    ItemId: Integer;
    Historico: string;
    NomeUsuarioCobranca: string;
    UsuarioCobrancaId: Integer;
    DataHoraCobranca: TDateTime;
  end;

  TContasRecHistoricoCobrancas = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordContasRecHistoricoCobrancas: RecContasRecHistoricoCobrancas;
  end;

function AtualizarContasRecHistoricoCobrancas(
  pConexao: TConexao;
  pReceberId: Integer;
  pHistorico: string
): RecRetornoBD;

function BuscarContasRecHistoricoCobrancas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecContasRecHistoricoCobrancas>;

function ExcluirContasRecHistoricoCobrancas(
  pConexao: TConexao;
  pReceberId: Integer;
  pHistorico: string
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TContasRecHistoricoCobrancas }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where RECEBER_ID = :P1 '
    );
end;

constructor TContasRecHistoricoCobrancas.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONTAS_REC_HISTORICO_COBRANCAS');

  FSql := 
    'select ' +
    '  HIS.RECEBER_ID, ' +
    '  HIS.ITEM_ID, ' +
    '  HIS.HISTORICO, ' +
    '  HIS.USUARIO_COBRANCA_ID, ' +
    '  FUN.NOME as NOME_USUARIO_COBRANCA, ' +
    '  HIS.DATA_HORA_COBRANCA ' +
    'from ' +
    '  CONTAS_REC_HISTORICO_COBRANCAS HIS ' +

    'inner join FUNCIONARIOS FUN ' +
    'on HIS.USUARIO_COBRANCA_ID = FUN.FUNCIONARIO_ID';

  setFiltros(getFiltros);

  AddColuna('RECEBER_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('HISTORICO', True);
  AddColunaSL('USUARIO_COBRANCA_ID');
  AddColunaSL('NOME_USUARIO_COBRANCA');
  AddColunaSL('DATA_HORA_COBRANCA');
end;

function TContasRecHistoricoCobrancas.getRecordContasRecHistoricoCobrancas: RecContasRecHistoricoCobrancas;
begin
  Result.ReceberId                  := getInt('RECEBER_ID', True);
  Result.ItemId                     := getInt('ITEM_ID', True);
  Result.Historico                  := getString('HISTORICO', True);
  Result.UsuarioCobrancaId          := getInt('USUARIO_COBRANCA_ID');
  Result.NomeUsuarioCobranca        := getString('NOME_USUARIO_COBRANCA');
  Result.DataHoraCobranca           := getData('DATA_HORA_COBRANCA');
end;

function AtualizarContasRecHistoricoCobrancas(
  pConexao: TConexao;
  pReceberId: Integer;
  pHistorico: string
): RecRetornoBD;
var
  t: TContasRecHistoricoCobrancas;

  vSql: TConsulta;
  vSeq: Integer;
begin
  Result.Iniciar;
  pConexao.setRotina('ATUALIZAR_HISTORICO_CONTAS_REC');

  t := TContasRecHistoricoCobrancas.Create(pConexao);
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  count(*) ');
  vSql.Add('from ');
  vSql.Add('  CONTAS_REC_HISTORICO_COBRANCAS ');
  vSql.Add('where RECEBER_ID = :P1 ');
  vSql.Pesquisar([pReceberId]);
  vSeq := vSql.GetInt(0) + 1;

  try
    pConexao.IniciarTransacao; 

    t.setInt('RECEBER_ID', pReceberId, True);
    t.setInt('ITEM_ID', vSeq, True);
    t.setString('HISTORICO', pHistorico, True);

    t.Inserir;

    pConexao.FinalizarTransacao; 
  except
    on e: Exception do begin
      pConexao.VoltarTransacao; 
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarContasRecHistoricoCobrancas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecContasRecHistoricoCobrancas>;
var
  i: Integer;
  t: TContasRecHistoricoCobrancas;
begin
  Result := nil;
  t := TContasRecHistoricoCobrancas.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordContasRecHistoricoCobrancas;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirContasRecHistoricoCobrancas(
  pConexao: TConexao;
  pReceberId: Integer;
  pHistorico: string
): RecRetornoBD;
var
  t: TContasRecHistoricoCobrancas;
begin
  Result.Iniciar;
  t := TContasRecHistoricoCobrancas.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('RECEBER_ID', pReceberId, True);
    t.setString('HISTORICO', pHistorico, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
