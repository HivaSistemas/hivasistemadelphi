unit _CondicoesPagamento;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros, _CondicoesPagamentoPrazos;

{$M+}
type
  TCondicoesPagamento = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordCondicoesPagamento: RecCondicoesPagamento;
  end;

function AtualizarCondicoesPagamento(
  pConexao: TConexao;
  pCondicaoId: Integer;
  pNome: string;
  pTipo: string;
  pIndiceAcrescimo: Double;
  pValorMinimoVenda: Double;
  pValorMinimoParcela: Double;
  pPercentualDescontoPermVenda: Double;
  pAtivo: string;
  pPrazoMedio: Integer;
  pPercentualJuros: double;
  pPercentualEncargos: double;
  pPercentualDescontoComercial: double;
  pExigirModeloNotaFiscal: string;
  pAplicarIndPrecoPromocional: string;
  pPermitirUsoPontaEstoque: string;
  pPermitirUsoFechamentoAcumu: string;
  pTipoPreco: string;
  pPermitirPrecoProcional: string;
  pNaoPermitirConsFinal: string;
  pRestrita: string;
  pBloquearVendaSempre: string;
  pRecebimentoNaEntrega: string;
  pAcumulativo: string;
  pImprimirConfissaoDivida: string;
  pImprimirCompFaturamentoVenda: string;
  pPrazos: TArray<Integer>;
  pPercentualCustoVenda: Double;
  pEmTransacao: Boolean;
  pEmpresasAutorizadas: TArray<Integer>;
  pExportarWeb: string
): RecRetornoBD;

function BuscarCondicoesPagamentos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean;
  pFechamentoAcumulado: Boolean;
  pSomenteEmpresaLogada: Boolean;
  pEmpresaId: Integer = 0
): TArray<RecCondicoesPagamento>;

function BuscarCondicoesPagamentosComando(pConexao: TConexao; pComando: string): TArray<RecCondicoesPagamento>;

function ExcluirCondicoesPagamento(
  pConexao: TConexao;
  pCondicaoId: Integer
): RecRetornoBD;

function RecebimentoNaEntrega( pConexao: TConexao; pOrcamentoId: Integer ): Boolean;
function BuscarQtdePrazos(pConexao: TConexao; pCondicaoId: Integer): Integer;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TCondicoesPagamento }

uses _CondicoesPagamentoEmpresa;

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CPP.CONDICAO_ID = :P1 ',
      ' order by CPP.NOME '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome da condi��o de pagamento',
      True,
      0,
      'where CPP.NOME like :P1 || ''%'' ',
      ' order by CPP.NOME '
    );
end;

constructor TCondicoesPagamento.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONDICOES_PAGAMENTO');

  FSql :=
    'select distinct' +
    '  CPP.CONDICAO_ID, ' +
    '  CPP.NOME, ' +
    '  CPP.TIPO, ' +
    '  CPP.INDICE_ACRESCIMO, ' +
    '  CPP.VALOR_MINIMO_VENDA, ' +
    '  CPP.VALOR_MINIMO_PARCELA, ' +
    '  CPP.PERCENTUAL_DESCONTO_PERM_VENDA, ' +
    '  CPP.PRAZO_MEDIO, ' +
    '  CPP.PERCENTUAL_JUROS, ' +
    '  CPP.PERCENTUAL_ENCARGOS, ' +
    '  CPP.PERCENTUAL_DESCONTO_COMERCIAL, ' +
    '  CPP.EXIGIR_MODELO_NOTA_FISCAL, ' +
    '  CPP.PERMITIR_PRECO_PROMOCIONAL, ' +
    '  CPP.APLICAR_IND_PRECO_PROMOCIONAL, ' +
    '  CPP.PERMITIR_USO_PONTA_ESTOQUE, ' +
    '  CPP.PERIMTIR_USO_FECHAMENTO_ACUMU, ' +
    '  CPP.TIPO_PRECO, ' +
    '  CPP.PERCENTUAL_CUSTO_VENDA, ' +
    '  CPP.NAO_PERMITIR_CONS_FINAL, ' +
    '  CPP.ATIVO, ' +
    '  CPP.RESTRITA, ' +
    '  CPP.BLOQUEAR_VENDA_SEMPRE, ' +
    '  CPP.RECEBIMENTO_NA_ENTREGA, ' +
    '  CPP.ACUMULATIVO, ' +
    '  CPP.IMPRIMIR_CONFISSAO_DIVIDA, ' +
    '  CPP.EXPORTAR_WEB, '+
    '  CPP.IMP_COMP_FATURAMENTO_VENDA ' +
    'from ' +
    '  CONDICOES_PAGAMENTO CPP ' +

    'inner join CONDICOES_PAGAMENTO_EMPRESA CPE ' +
    'on CPE.CONDICAO_ID = CPP.CONDICAO_ID ';

  SetFiltros(GetFiltros);

  AddColuna('CONDICAO_ID', True);
  AddColuna('NOME');
  AddColuna('TIPO');
  AddColuna('INDICE_ACRESCIMO');
  AddColuna('VALOR_MINIMO_VENDA');
  AddColuna('VALOR_MINIMO_PARCELA');
  AddColuna('PERCENTUAL_DESCONTO_PERM_VENDA');
  AddColuna('PRAZO_MEDIO');
  AddColuna('PERCENTUAL_JUROS');
  AddColuna('PERCENTUAL_ENCARGOS');
  AddColuna('PERCENTUAL_DESCONTO_COMERCIAL');
  AddColuna('EXIGIR_MODELO_NOTA_FISCAL');
  AddColuna('PERMITIR_PRECO_PROMOCIONAL');
  AddColuna('APLICAR_IND_PRECO_PROMOCIONAL');
  AddColuna('PERMITIR_USO_PONTA_ESTOQUE');
  AddColuna('PERIMTIR_USO_FECHAMENTO_ACUMU');
  AddColuna('TIPO_PRECO');
  AddColuna('PERCENTUAL_CUSTO_VENDA');
  AddColuna('NAO_PERMITIR_CONS_FINAL');
  AddColuna('RESTRITA');
  AddColuna('BLOQUEAR_VENDA_SEMPRE');
  AddColuna('RECEBIMENTO_NA_ENTREGA');
  AddColuna('ATIVO');
  AddColuna('ACUMULATIVO');
  AddColuna('IMPRIMIR_CONFISSAO_DIVIDA');
  AddColuna('IMP_COMP_FATURAMENTO_VENDA');
  AddColuna('EXPORTAR_WEB');
end;

function TCondicoesPagamento.GetRecordCondicoesPagamento: RecCondicoesPagamento;
begin
  Result := RecCondicoesPagamento.Create;
  Result.condicao_id                    := GetInt('CONDICAO_ID', True);
  Result.nome                           := GetString('NOME');
  Result.tipo                           := GetString('TIPO');
  Result.indice_acrescimo               := GetDouble('INDICE_ACRESCIMO');
  Result.valor_minimo_venda             := GetDouble('VALOR_MINIMO_VENDA');
  Result.ValorMinimoParcela             := GetDouble('VALOR_MINIMO_PARCELA');
  Result.PercentualDescontoPermVenda    := GetDouble('PERCENTUAL_DESCONTO_PERM_VENDA');
  Result.ativo                          := GetString('ATIVO');
  Result.PrazoMedio                     := GetInt('PRAZO_MEDIO');
  Result.PercentualJuros                := GetDouble('PERCENTUAL_JUROS');
  Result.PercentualEncagos              := GetDouble('PERCENTUAL_ENCARGOS');
  Result.PercentualDescontoComercial    := GetDouble('PERCENTUAL_DESCONTO_COMERCIAL');
  Result.ExigirModeloNotaFiscal         := GetString('EXIGIR_MODELO_NOTA_FISCAL');
  Result.PermitirPrecoPromocional       := GetString('PERMITIR_PRECO_PROMOCIONAL');
  Result.AplicarIndPrecoPromocional     := GetString('APLICAR_IND_PRECO_PROMOCIONAL');
  Result.PermitirUsoPontaEstoque        := GetString('PERMITIR_USO_PONTA_ESTOQUE');
  Result.PermitirUsoFechamentoAcumu     := GetString('PERIMTIR_USO_FECHAMENTO_ACUMU');
  Result.TipoPreco                      := GetString('TIPO_PRECO');
  Result.PercentualCustoVenda           := GetDouble('PERCENTUAL_CUSTO_VENDA');
  Result.NaoPermitirConsFinal           := GetString('NAO_PERMITIR_CONS_FINAL');
  Result.Restrita                       := GetString('RESTRITA');
  Result.BloquearVendaSempre            := GetString('BLOQUEAR_VENDA_SEMPRE');
  Result.RecebimentoNaEntrega           := GetString('RECEBIMENTO_NA_ENTREGA');
  Result.Acumulativo                    := GetString('ACUMULATIVO');
  Result.ImprimirConfissaoDivida        := GetString('IMPRIMIR_CONFISSAO_DIVIDA');
  Result.ImprimirCompFaturamentoVenda   := GetString('IMP_COMP_FATURAMENTO_VENDA');
  Result.ExportarWeb                    := GetString('EXPORTAR_WEB');

end;

function AtualizarCondicoesPagamento(
  pConexao: TConexao;
  pCondicaoId: Integer;
  pNome: string;
  pTipo: string;
  pIndiceAcrescimo: Double;
  pValorMinimoVenda: Double;
  pValorMinimoParcela: Double;
  pPercentualDescontoPermVenda: Double;
  pAtivo: string;
  pPrazoMedio: Integer;
  pPercentualJuros: Double;
  pPercentualEncargos: Double;
  pPercentualDescontoComercial: Double;
  pExigirModeloNotaFiscal: string;
  pAplicarIndPrecoPromocional: string;
  pPermitirUsoPontaEstoque: string;
  pPermitirUsoFechamentoAcumu: string;
  pTipoPreco: string;
  pPermitirPrecoProcional: string;
  pNaoPermitirConsFinal: string;
  pRestrita: string;
  pBloquearVendaSempre: string;
  pRecebimentoNaEntrega: string;
  pAcumulativo: string;
  pImprimirConfissaoDivida: string;
  pImprimirCompFaturamentoVenda: string;
  pPrazos: TArray<Integer>;
  pPercentualCustoVenda: Double;
  pEmTransacao: Boolean;
  pEmpresasAutorizadas: TArray<Integer>;
  pExportarWeb: string
): RecRetornoBD;
var
  i: Integer;
  novo: Boolean;
  vExec: TExecucao;
  t: TCondicoesPagamento;
  vPrazo: TCondicoesPagamentoPrazos;
  vCondicoesPagamentoEmpresa: TCondicoesPagamentoEmpresa;
begin
  Result.Iniciar;

  t := TCondicoesPagamento.Create(pConexao);
  vPrazo := TCondicoesPagamentoPrazos.Create(pConexao);
  vCondicoesPagamentoEmpresa := TCondicoesPagamentoEmpresa.Create(pConexao);
  vExec := TExecucao.Create(pConexao);

  novo := pCondicaoId = 0;

  if novo then begin
    pCondicaoId := TSequencia.Create(pConexao, 'SEQ_CONDICAO_ID').GetProximaSequencia;
    Result.AsInt := pCondicaoId;
  end;

  // Sempre retornar
  Result.AsInt := pCondicaoId;

  t.SetInt('CONDICAO_ID', pCondicaoId, True);
  t.SetString('NOME', pNome);
  t.SetString('TIPO', pTipo);
  t.SetDouble('INDICE_ACRESCIMO', pIndiceAcrescimo);
  t.setDouble('VALOR_MINIMO_VENDA', pValorMinimoVenda);
  t.setDouble('VALOR_MINIMO_PARCELA', pValorMinimoParcela);
  t.setDouble('PERCENTUAL_DESCONTO_PERM_VENDA', pPercentualDescontoPermVenda);
  t.SetString('ATIVO', pAtivo);
  t.setInt('PRAZO_MEDIO', pPrazoMedio);
  t.setDouble('PERCENTUAL_DESCONTO_COMERCIAL', pPercentualDescontoComercial);
  t.setDouble('PERCENTUAL_JUROS', pPercentualJuros);
  t.setDouble('PERCENTUAL_ENCARGOS', pPercentualEncargos);
  t.SetString('EXIGIR_MODELO_NOTA_FISCAL', pExigirModeloNotaFiscal);
  t.SetString('APLICAR_IND_PRECO_PROMOCIONAL', pAplicarIndPrecoPromocional);
  t.SetString('PERMITIR_USO_PONTA_ESTOQUE', pPermitirUsoPontaEstoque);
  t.SetString('PERIMTIR_USO_FECHAMENTO_ACUMU', pPermitirUsoFechamentoAcumu);
  t.SetString('TIPO_PRECO', pTipoPreco);
  t.SetString('PERMITIR_PRECO_PROMOCIONAL', pPermitirPrecoProcional);
  t.setDouble('PERCENTUAL_CUSTO_VENDA', pPercentualCustoVenda);
  t.SetString('NAO_PERMITIR_CONS_FINAL', pNaoPermitirConsFinal);
  t.SetString('RESTRITA', pRestrita);
  t.SetString('BLOQUEAR_VENDA_SEMPRE', pBloquearVendaSempre);
  t.SetString('RECEBIMENTO_NA_ENTREGA', pRecebimentoNaEntrega);
  t.SetString('ACUMULATIVO', pAcumulativo);
  t.SetString('IMPRIMIR_CONFISSAO_DIVIDA', pImprimirConfissaoDivida);
  t.SetString('IMP_COMP_FATURAMENTO_VENDA', pImprimirCompFaturamentoVenda);
  t.SetString('EXPORTAR_WEB', pExportarWeb);


  try
    if not pEmTransacao then
      pConexao.IniciarTransacao;

    if novo then
      t.Inserir
    else
      t.Atualizar;

    vExec.Clear;
    vExec.Add('delete from CONDICOES_PAGAMENTO_PRAZOS ');
    vExec.Add('where CONDICAO_ID = :P1 ');
    vExec.Executar([pCondicaoId]);

    vPrazo.setInt('CONDICAO_ID', pCondicaoId, True);
    for i := Low(pPrazos) to High(pPrazos) do begin
      vPrazo.setInt('DIAS', pPrazos[i], True);
      vPrazo.Inserir;
    end;

    vExec.Clear;
    vExec.Add('delete from CONDICOES_PAGAMENTO_EMPRESA ');
    vExec.Add('where CONDICAO_ID = :P1 ');
    vExec.Executar([pCondicaoId]);

    for i := Low(pEmpresasAutorizadas) to High(pEmpresasAutorizadas) do begin
      vCondicoesPagamentoEmpresa.setInt('CONDICAO_ID', pCondicaoId, True);
      vCondicoesPagamentoEmpresa.setInt('EMPRESA_ID', pEmpresasAutorizadas[I], True);
      vCondicoesPagamentoEmpresa.Inserir;
    end;

    if not pEmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pEmTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarCondicoesPagamentos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean;
  pFechamentoAcumulado: Boolean;
  pSomenteEmpresaLogada: Boolean;
  pEmpresaId: Integer = 0
): TArray<RecCondicoesPagamento>;
var
  i: Integer;
  t: TCondicoesPagamento;
  vSqlAuxiliar: string;
begin
  Result := nil;
  t := TCondicoesPagamento.Create(pConexao);

  if pSomenteAtivos then
    vSqlAuxiliar := ' and CPP.ATIVO = ''S'' ';

  if pFechamentoAcumulado then
    vSqlAuxiliar := vSqlAuxiliar + ' and CPP.PERIMTIR_USO_FECHAMENTO_ACUMU = ''S'' ';

  if pSomenteEmpresaLogada then
    vSqlAuxiliar := vSqlAuxiliar + ' and CPE.EMPRESA_ID = ' + IntToStr(pEmpresaId) + ' ';

  if t.Pesquisar(pIndice, pFiltros, vSqlAuxiliar) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordCondicoesPagamento;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarCondicoesPagamentosComando(pConexao: TConexao; pComando: string): TArray<RecCondicoesPagamento>;
var
  i: Integer;
  t: TCondicoesPagamento;
begin
  Result := nil;
  t := TCondicoesPagamento.Create(pConexao);
  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordCondicoesPagamento;
      t.ProximoRegistro;
    end;
  end;
  t.Free;
end;

function ExcluirCondicoesPagamento(
  pConexao: TConexao;
  pCondicaoId: Integer
): RecRetornoBD;
var
  t: TCondicoesPagamento;
begin
  Result.TeveErro := False;
  t := TCondicoesPagamento.Create(pConexao);

  t.SetInt('CONDICAO_ID', pCondicaoId, True);

  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function RecebimentoNaEntrega( pConexao: TConexao; pOrcamentoId: Integer ): Boolean;
var
  vSql: TConsulta;
begin
  Result := False;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  PAG.RECEBIMENTO_NA_ENTREGA ');
  vSql.Add('from ');
  vSql.Add('  ORCAMENTOS ORC ');

  vSql.Add('inner join CONDICOES_PAGAMENTO PAG ');
  vSql.Add('on ORC.CONDICAO_ID = PAG.CONDICAO_ID ');

  vSql.Add('where ORC.ORCAMENTO_ID = :P1 ');

  if vSql.Pesquisar([pOrcamentoId]) then
    Result := vSql.GetString('RECEBIMENTO_NA_ENTREGA') = 'S';

  vSql.Free;
end;

function BuscarQtdePrazos(pConexao: TConexao; pCondicaoId: Integer): Integer;
var
  vSql: TConsulta;
begin
  Result := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  count(*) as QTDE_PRAZOS ');
  vSql.Add('from ');
  vSql.Add('  CONDICOES_PAGAMENTO_PRAZOS ');
  vSql.Add('where CONDICAO_ID = :P1 ');

  if vSql.Pesquisar([pCondicaoId]) then
    Result := vSql.GetInt('QTDE_PRAZOS');

  vSql.Free;
end;

end.
