unit _PortadoresContas;

interface

uses
  _OperacoesBancoDados, _RecordsEspeciais, _Conexao, System.SysUtils, System.Variants, _BibliotecaGenerica;

{$M+}
type
  RecPortadoresContas = record
    EmpresaId: Integer;
    PortadorId: string;
    ContaId: string;

    EmpresaContaId: Integer;
    DigitoConta: string;
    NomeEmpresa: string;
  end;

  TPortadoresContas = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordPortador: RecPortadoresContas;
  end;

function BuscarPortadores(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant
): TArray<RecPortadoresContas>;


function GetFiltros: TArray<RecFiltros>;

implementation

{ TPortadoresContas }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Portador',
      True,
      0,
      'where PCO.PORTADOR_ID like ''%'' || :P1 || ''%'' '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Portador',
      True,
      0,
      'where PCO.PORTADOR_ID = :P1 ' +
      'and PCO.EMPRESA_ID = :P2 '
    );
end;

constructor TPortadoresContas.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PORTADORES_CONTAS');

  FSql :=
    'select ' +
    '  PCO.EMPRESA_ID, ' +
    '  PCO.PORTADOR_ID, ' +
    '  PCO.CONTA_ID, ' +
    '  CON.DIGITO_CONTA, ' +
    '  EMP.EMPRESA_ID as EMPRESA_CONTA_ID, ' +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA ' +
    'from ' +
    '  PORTADORES_CONTAS PCO ' +

    'inner join CONTAS CON ' +
    'on PCO.CONTA_ID = CON.CONTA ' +

    'inner join EMPRESAS EMP ' +
    'on CON.EMPRESA_ID = EMP.EMPRESA_ID ';

  SetFiltros(GetFiltros);

  AddColuna('EMPRESA_ID', True);
  AddColuna('PORTADOR_ID', True);
  AddColuna('CONTA_ID', True);

  AddColunaSL('DIGITO_CONTA');
  AddColunaSL('EMPRESA_CONTA_ID');
  AddColunaSL('NOME_EMPRESA');
end;

function TPortadoresContas.GetRecordPortador: RecPortadoresContas;
begin
  Result.PortadorId := GetString('PORTADOR_ID', True);
  Result.EmpresaId  := GetInt('EMPRESA_ID', True);
  Result.ContaId    := GetString('CONTA_ID', True);

  Result.EmpresaContaId := GetInt('EMPRESA_CONTA_ID');
  Result.DigitoConta    := GetString('DIGITO_CONTA');
  Result.NomeEmpresa    := GetString('NOME_EMPRESA');
end;

function BuscarPortadores(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant
): TArray<RecPortadoresContas>;
var
  i: Integer;
  t: TPortadoresContas;
begin
  Result := nil;
  t := TPortadoresContas.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordPortador;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
