unit _RelacaoContasPagar;

interface

uses
  System.SysUtils, _Conexao, _OperacoesBancoDados, _RecordsRelatorios;

function BuscarContasPagarComando(pConexao: TConexao; pComando: string): TArray<RecContasPagar>;

implementation

function BuscarContasPagarComando(pConexao: TConexao; pComando: string): TArray<RecContasPagar>;
var
  i: Word;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  CON.PAGAR_ID, ');
  vSql.Add('  CON.DOCUMENTO, ');
  vSql.Add('  CON.CADASTRO_ID as FORNECEDOR_ID, ');
  vSql.Add('  CAD.RAZAO_SOCIAL as NOME_FORNECEDOR, ');
  vSql.Add('  CON.ENTRADA_ID, ');
  vSql.Add('  CON.NUMERO_CHEQUE, ');
  vSql.Add('  CON.COBRANCA_ID, ');
  vSql.Add('  TCO.NOME as NOME_TIPO_COBRANCA, ');
  vSql.Add('  TCO.FORMA_PAGAMENTO, ');
  vSql.Add('  CON.EMPRESA_ID, ');
  vSql.Add('  EMP.NOME_FANTASIA as NOME_EMPRESA, ');
  vSql.Add('  CON.VALOR_DOCUMENTO, ');
  vSql.Add('  CON.VALOR_ADIANTADO, ');
  vSql.Add('  CON.VALOR_DESCONTO, ');
  vSql.Add('  CON.VALOR_DOCUMENTO - CON.VALOR_ADIANTADO - CON.VALOR_DESCONTO as SALDO, ');
  vSql.Add('  CON.NOSSO_NUMERO, ');
  vSql.Add('  CON.STATUS, ');
  vSql.Add('  BAI.DATA_PAGAMENTO, ');
  vSql.Add('  CON.DATA_VENCIMENTO_ORIGINAL, ');
  vSql.Add('  CON.DATA_VENCIMENTO, ');
  vSql.Add('  CON.DATA_CADASTRO, ');
  vSql.Add('  CON.BLOQUEADO, ');
  vSql.Add('  CON.PLANO_FINANCEIRO_ID, ');
  vSql.Add('  PLF.DESCRICAO as NOME_PLANO_FINANCEIRO, ');
  vSql.Add('  CON.CENTRO_CUSTO_ID, ');
  vSql.Add('  CCT.NOME as NOME_CENTRO_CUSTO, ');
  vSql.Add('  CON.DATA_CONTABIL, ');

  vSql.Add('  case when con.status = ''A'' then ');
  vSql.Add('    case when trunc(sysdate) - CON.DATA_VENCIMENTO > 0 then trunc(sysdate) - CON.DATA_VENCIMENTO else 0 end ');
  vSql.Add('  else ');
  vSql.Add('    case when trunc(BAI.DATA_PAGAMENTO) - CON.DATA_VENCIMENTO > 0 then trunc(BAI.DATA_PAGAMENTO) - CON.DATA_VENCIMENTO else 0 end ');
  vSql.Add('  end as DIAS_ATRASO, ');

  vSql.Add('  CON.CODIGO_BARRAS, ');
  vSql.Add('  CON.PARCELA, ');
  vSql.Add('  CON.NUMERO_PARCELAS, ');
  vSql.Add('  CON.BAIXA_RECEBER_ORIGEM_ID, ');
  vSql.Add('  CON.BAIXA_ID, ');
  vSql.Add('  CON.ORIGEM ');
  vSql.Add('from ');
  vSql.Add('  CONTAS_PAGAR CON ');

  vSql.Add('inner join CADASTROS CAD ');
  vSql.Add('on CON.CADASTRO_ID = CAD.CADASTRO_ID ');

  vSql.Add('inner join EMPRESAS EMP ');
  vSql.Add('on CON.EMPRESA_ID = EMP.EMPRESA_ID ');

  vSql.Add('inner join TIPOS_COBRANCA TCO ');
  vSql.Add('on CON.COBRANCA_ID = TCO.COBRANCA_ID ');

  vSql.Add('inner join PLANOS_FINANCEIROS PLF ');
  vSql.Add('on CON.PLANO_FINANCEIRO_ID = PLF.PLANO_FINANCEIRO_ID ');

  vSql.Add('inner join CENTROS_CUSTOS CCT ');
  vSql.Add('on CON.CENTRO_CUSTO_ID = CCT.CENTRO_CUSTO_ID ');

  vSql.Add('left join CONTAS_PAGAR_BAIXAS BAI ');
  vSql.Add('on NVL(CON.BAIXA_ORIGEM_ID, CON.BAIXA_ID) = BAI.BAIXA_ID ');

  vSql.Add('left join CONTAS_PAGAR_BAIXAS_ITENS BXI ');
  vSql.Add('on CON.PAGAR_ID = BXI.PAGAR_ID ');
  vSql.Add('and BAI.BAIXA_ID = BXI.BAIXA_ID ');

  vSql.Add(pComando);

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].PagarId                  := vSql.GetInt('PAGAR_ID');
      Result[i].documento                := vSql.GetString('DOCUMENTO');
      Result[i].fornecedor_id            := vSql.GetInt('FORNECEDOR_ID');
      Result[i].nome_fornecedor          := vSql.GetString('NOME_FORNECEDOR');
      Result[i].entrada_id               := vSql.GetInt('ENTRADA_ID');
      Result[i].numero_cheque            := vSql.GetInt('NUMERO_CHEQUE');
      Result[i].cobranca_id              := vSql.GetInt('COBRANCA_ID');
      Result[i].nome_tipo_cobranca       := vSql.GetString('NOME_TIPO_COBRANCA');
      Result[i].forma_pagamento          := vSql.GetString('FORMA_PAGAMENTO');
      Result[i].empresa_id               := vSql.GetInt('EMPRESA_ID');
      Result[i].dias_atraso              := vSql.GetInt('DIAS_ATRASO');
      Result[i].nome_empresa             := vSql.GetString('NOME_EMPRESA');
      Result[i].valor_documento          := vSql.GetDouble('VALOR_DOCUMENTO');
      Result[i].ValorAdiantado           := vSql.GetDouble('VALOR_ADIANTADO');
      Result[i].ValorDesconto            := vSql.GetDouble('VALOR_DESCONTO');
      Result[i].ValorSaldo               := vSql.GetDouble('SALDO');
      Result[i].nosso_numero             := vSql.GetString('NOSSO_NUMERO');
      Result[i].status                   := vSql.GetString('STATUS');
      Result[i].documento                := vSql.GetString('DOCUMENTO');
      Result[i].data_pagamento           := vSql.GetData('DATA_PAGAMENTO');
      Result[i].data_vencimento_original := vSql.GetData('DATA_VENCIMENTO_ORIGINAL');
      Result[i].data_vencimento          := vSql.GetData('DATA_VENCIMENTO');
      Result[i].data_cadastro            := vSql.GetData('DATA_CADASTRO');
      Result[i].codigo_barras            := vSql.GetString('CODIGO_BARRAS');
      Result[i].parcela                  := vSql.GetInt('PARCELA');
      Result[i].numero_parcelas          := vSql.GetInt('NUMERO_PARCELAS');
      Result[i].bloqueado                := vSql.GetString('BLOQUEADO');
      Result[i].PlanoFinanceiroId        := vSql.GetString('PLANO_FINANCEIRO_ID');
      Result[i].NomePlanoFinanceiro      := vSql.GetString('NOME_PLANO_FINANCEIRO');
      Result[i].CentroCustoId            := vSql.GetInt('CENTRO_CUSTO_ID');
      Result[i].NomeCentroCusto          := vSql.GetString('NOME_CENTRO_CUSTO');
      Result[i].BaixaReceberOrigemId     := vSql.GetInt('BAIXA_RECEBER_ORIGEM_ID');
      Result[i].BaixaId                  := vSql.GetInt('BAIXA_ID');
      Result[i].origem                   := vSql.GetString('ORIGEM');
      Result[i].Data_Contabil            := vSql.GetData('DATA_CONTABIL');

      vSql.Next;
    end;
  end;
  vSql.Active := False;
  FreeAndNil(vSql);
end;

end.
