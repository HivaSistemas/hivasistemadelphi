unit _RelacaoEstoque;

interface

uses
  _Conexao, _OperacoesBancoDados;

type
  RecRelacaoEstoque = record
    ProdutoId: Integer;
    NomeProduto: string;
    EstoqueFiscal: Double;
    Fisico: Double;
    Disponivel: Double;
    Reservado: Double;
    CMV: Double;
    PrecoFinal: Double;
    PrecoCompra: Double;
    MarcaId: Integer;
    NomeMarca: string;
    UnidadeVenda: string;
    CodigoBarras: string;
    CodigoOriginal: string;
    NCM: string;
  end;

function BuscarEstoque(pConexao: TConexao; pComando: string): TArray<RecRelacaoEstoque>;
function BuscarEstoqueLocais(pConexao: TConexao; pComando: string): TArray<RecRelacaoEstoque>;
function BuscarEstoqueRetroativo(pConexao: TConexao; pComando: string): TArray<RecRelacaoEstoque>;

implementation

function BuscarEstoque(pConexao: TConexao; pComando: string): TArray<RecRelacaoEstoque>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  EST.PRODUTO_ID, ');
  vSql.Add('  PRO.NOME as NOME_PRODUTO, ');
  vSql.Add('  EST.ESTOQUE as ESTOQUE_FISCAL, ');
  vSql.Add('  EST.FISICO, ');
  vSql.Add('  EST.DISPONIVEL, ');
  vSql.Add('  EST.RESERVADO, ');
  vSql.Add('  CUS.CMV, ');
  vSql.Add('  CUS.PRECO_FINAL, ');
  vSql.Add('  CUS.PRECO_LIQUIDO, ');
  vSql.Add('  PRO.MARCA_ID, ');
  vSql.Add('  MAR.NOME as NOME_MARCA, ');
  vSql.Add('  PRO.UNIDADE_VENDA, ');
  vSql.Add('  PRO.CODIGO_BARRAS, ');
  vSql.Add('  PRO.CODIGO_NCM, ');
  vSql.Add('  PRO.CODIGO_ORIGINAL_FABRICANTE ');

  vSql.Add('from ');
  vSql.Add('  ESTOQUES EST ');

  vSql.Add('inner join PRODUTOS PRO ');
  vSql.Add('on EST.PRODUTO_ID = PRO.PRODUTO_ID ');
//  vSql.Add('and PRO.PRODUTO_ID = PRO.PRODUTO_PAI_ID ');

  vSql.Add('inner join CUSTOS_PRODUTOS CUS ');
  vSql.Add('on PRO.PRODUTO_ID = CUS.PRODUTO_ID ');
  vSql.Add('and EST.EMPRESA_ID = CUS.EMPRESA_ID ');

  vSql.Add('inner join MARCAS MAR ');
  vSql.Add('on PRO.MARCA_ID = MAR.MARCA_ID ');

  vSql.Add(pComando);

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].ProdutoId     := vSql.GetInt('PRODUTO_ID');
      Result[i].NomeProduto   := vSql.GetString('NOME_PRODUTO');
      Result[i].EstoqueFiscal := vSql.GetDouble('ESTOQUE_FISCAL');
      Result[i].Fisico        := vSql.GetDouble('FISICO');
      Result[i].Disponivel    := vSql.GetDouble('DISPONIVEL');
      Result[i].Reservado     := vSql.GetDouble('RESERVADO');
      Result[i].CMV           := vSql.GetDouble('CMV');
      Result[i].PrecoFinal    := vSql.GetDouble('PRECO_FINAL');
      Result[i].PrecoCompra   := vSql.GetDouble('PRECO_LIQUIDO');
      Result[i].MarcaId       := vSql.getInt('MARCA_ID');
      Result[i].NomeMarca     := vSql.GetString('NOME_MARCA');
      Result[i].UnidadeVenda  := vSql.GetString('UNIDADE_VENDA');
      Result[i].CodigoBarras  := vSql.GetString('CODIGO_BARRAS');
      Result[i].CodigoOriginal  := vSql.GetString('CODIGO_ORIGINAL_FABRICANTE');
      Result[i].NCM           := vSql.GetString('CODIGO_NCM');

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function BuscarEstoqueLocais(pConexao: TConexao; pComando: string): TArray<RecRelacaoEstoque>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  EST.PRODUTO_ID, ');
  vSql.Add('  PRO.NOME as NOME_PRODUTO, ');
  vSql.Add('  0 as ESTOQUE_FISCAL, ');
  vSql.Add('  SUM(EST.FISICO) as FISICO, ');
  vSql.Add('  SUM(EST.DISPONIVEL) as DISPONIVEL, ');
  vSql.Add('  SUM(EST.RESERVADO) as RESERVADO, ');
  vSql.Add('  CUS.CMV, ');
  vSql.Add('  CUS.PRECO_FINAL, ');
  vSql.Add('  CUS.PRECO_LIQUIDO, ');
  vSql.Add('  PRO.MARCA_ID, ');
  vSql.Add('  MAR.NOME as NOME_MARCA, ');
  vSql.Add('  PRO.UNIDADE_VENDA, ');
  vSql.Add('  PRO.CODIGO_BARRAS, ');
  vSql.Add('  PRO.CODIGO_NCM, ');
  vSql.Add('  PRO.CODIGO_ORIGINAL_FABRICANTE ');
  vSql.Add('from ');
  vSql.Add('  ESTOQUES_DIVISAO EST ');

  vSql.Add('inner join PRODUTOS PRO ');
  vSql.Add('on EST.PRODUTO_ID = PRO.PRODUTO_ID ');

  vSql.Add('inner join CUSTOS_PRODUTOS CUS ');
  vSql.Add('on PRO.PRODUTO_ID = CUS.PRODUTO_ID ');
  vSql.Add('and EST.EMPRESA_ID = CUS.EMPRESA_ID ');

  vSql.Add('inner join MARCAS MAR ');
  vSql.Add('on PRO.MARCA_ID = MAR.MARCA_ID ');

  vSql.Add(pComando);

  vSql.Add('group by ');
  vSql.Add('  EST.PRODUTO_ID, ');
  vSql.Add('  PRO.NOME, ');
  vSql.Add('  CUS.CMV, ');
  vSql.Add('  CUS.PRECO_FINAL, ');
  vSql.Add('  CUS.PRECO_LIQUIDO, ');
  vSql.Add('  PRO.MARCA_ID, ');
  vSql.Add('  MAR.NOME, ');
  vSql.Add('  PRO.UNIDADE_VENDA, ');
  vSql.Add('  PRO.CODIGO_BARRAS, ');
  vSql.Add('  PRO.CODIGO_NCM, ');
  vSql.Add('  PRO.CODIGO_ORIGINAL_FABRICANTE ');

  vSql.Add('order by PRO.NOME');

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].ProdutoId     := vSql.GetInt('PRODUTO_ID');
      Result[i].NomeProduto   := vSql.GetString('NOME_PRODUTO');
      Result[i].EstoqueFiscal := vSql.GetDouble('ESTOQUE_FISCAL');
      Result[i].Fisico        := vSql.GetDouble('FISICO');
      Result[i].Disponivel    := vSql.GetDouble('DISPONIVEL');
      Result[i].Reservado     := vSql.GetDouble('RESERVADO');
      Result[i].CMV           := vSql.GetDouble('CMV');
      Result[i].PrecoFinal    := vSql.GetDouble('PRECO_FINAL');
      Result[i].PrecoCompra   := vSql.GetDouble('PRECO_LIQUIDO');
      Result[i].MarcaId       := vSql.getInt('MARCA_ID');
      Result[i].NomeMarca     := vSql.GetString('NOME_MARCA');
      Result[i].UnidadeVenda  := vSql.GetString('UNIDADE_VENDA');
      Result[i].CodigoBarras  := vSql.GetString('CODIGO_BARRAS');
      Result[i].CodigoOriginal  := vSql.GetString('CODIGO_ORIGINAL_FABRICANTE');
      Result[i].NCM           := vSql.GetString('CODIGO_NCM');

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function BuscarEstoqueRetroativo(pConexao: TConexao; pComando: string): TArray<RecRelacaoEstoque>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  EST.PRODUTO_ID, ');
  vSql.Add('  PRO.NOME as NOME_PRODUTO, ');
  vSql.Add('  EST.ESTOQUE as ESTOQUE_FISCAL, ');
  vSql.Add('  EST.FISICO, ');
  vSql.Add('  EST.DISPONIVEL, ');
  vSql.Add('  EST.RESERVADO, ');
  vSql.Add('  CUS.CMV, ');
  vSql.Add('  CUS.PRECO_FINAL, ');
  vSql.Add('  CUS.PRECO_LIQUIDO, ');
  vSql.Add('  PRO.MARCA_ID, ');
  vSql.Add('  MAR.NOME as NOME_MARCA, ');
  vSql.Add('  PRO.UNIDADE_VENDA, ');
  vSql.Add('  PRO.CODIGO_BARRAS, ');
  vSql.Add('  PRO.CODIGO_NCM, ');
  vSql.Add('  PRO.CODIGO_ORIGINAL_FABRICANTE ');

  vSql.Add('from ');
  vSql.Add('  ESTOQUES_ANO_MES EST ');

  vSql.Add('inner join PRODUTOS PRO ');
  vSql.Add('on EST.PRODUTO_ID = PRO.PRODUTO_ID ');
  vSql.Add('and PRO.PRODUTO_ID = PRO.PRODUTO_PAI_ID ');

  vSql.Add('inner join CUSTOS_PRODUTOS_ANO_MES CUS ');
  vSql.Add('on PRO.PRODUTO_ID = CUS.PRODUTO_ID ');
  vSql.Add('and EST.EMPRESA_ID = CUS.EMPRESA_ID ');

  vSql.Add('inner join MARCAS MAR ');
  vSql.Add('on PRO.MARCA_ID = MAR.MARCA_ID ');

  vSql.Add(pComando);

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].ProdutoId     := vSql.GetInt('PRODUTO_ID');
      Result[i].NomeProduto   := vSql.GetString('NOME_PRODUTO');
      Result[i].EstoqueFiscal := vSql.GetDouble('ESTOQUE_FISCAL');
      Result[i].Fisico        := vSql.GetDouble('FISICO');
      Result[i].Disponivel    := vSql.GetDouble('DISPONIVEL');
      Result[i].Reservado     := vSql.GetDouble('RESERVADO');
      Result[i].CMV           := vSql.GetDouble('CMV');
      Result[i].PrecoFinal    := vSql.GetDouble('PRECO_FINAL');
      Result[i].PrecoCompra   := vSql.GetDouble('PRECO_LIQUIDO');
      Result[i].MarcaId       := vSql.getInt('MARCA_ID');
      Result[i].NomeMarca     := vSql.GetString('NOME_MARCA');
      Result[i].UnidadeVenda  := vSql.GetString('UNIDADE_VENDA');
      Result[i].CodigoBarras  := vSql.GetString('CODIGO_BARRAS');
      Result[i].CodigoOriginal  := vSql.GetString('CODIGO_ORIGINAL_FABRICANTE');
      Result[i].NCM           := vSql.GetString('CODIGO_NCM');

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

end.


