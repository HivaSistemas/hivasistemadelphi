unit _ApuracoesImpostos;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecApuracoesImpostos = record
    MovimentoId: Integer;
    TipoMovimento: string;
    CfopId: string;
    EmpresaId: Integer;
    ValorContabil: Double;
    BaseCalculoIcms: Double;
    ValorIcms: Double;
    BaseCaculoIcmsSt: Double;
    ValorIcmsSt: Double;
    ValorIpi: Double;
    BaseCaculoPis: Double;
    ValorPis: Double;
    BaseCaculoCofins: Double;
    ValorCofins: Double;
    Natureza: string;
    DataContabil: TDateTime;
  end;

  TApuracoesImpostos = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordApuracoesImpostos: RecApuracoesImpostos;
  end;

function BuscarApuracoesImpostos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecApuracoesImpostos>;

function BuscarApuracoesImpostosComando(pConexao: TConexao; pComando: string): TArray<RecApuracoesImpostos>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TApuracoesImpostos }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where XXX = :P1'
    );
end;

constructor TApuracoesImpostos.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'APURACOES_IMPOSTOS');

  FSql := 
    'select ' +
    '  MOVIMENTO_ID, ' +
    '  TIPO_MOVIMENTO, ' +
    '  CFOP_ID, ' +
    '  EMPRESA_ID, ' +
    '  VALOR_CONTABIL, ' +
    '  BASE_CALCULO_ICMS, ' +
    '  VALOR_ICMS, ' +
    '  BASE_CACULO_ICMS_ST, ' +
    '  VALOR_ICMS_ST, ' +
    '  VALOR_IPI, ' +
    '  BASE_CACULO_PIS, ' +
    '  VALOR_PIS, ' +
    '  BASE_CACULO_COFINS, ' +
    '  VALOR_COFINS, ' +
    '  NATUREZA, ' +
    '  DATA_CONTABIL ' +
    'from ' +
    '  APURACOES_IMPOSTOS';

  setFiltros(getFiltros);

  AddColuna('MOVIMENTO_ID', True);
  AddColuna('TIPO_MOVIMENTO', True);
  AddColuna('CFOP_ID', True);
  AddColuna('EMPRESA_ID');
  AddColuna('VALOR_CONTABIL');
  AddColuna('BASE_CALCULO_ICMS');
  AddColuna('VALOR_ICMS');
  AddColuna('BASE_CACULO_ICMS_ST');
  AddColuna('VALOR_ICMS_ST');
  AddColuna('VALOR_IPI');
  AddColuna('BASE_CACULO_PIS');
  AddColuna('VALOR_PIS');
  AddColuna('BASE_CACULO_COFINS');
  AddColuna('VALOR_COFINS');
  AddColuna('NATUREZA');
  AddColuna('DATA_CONTABIL');
end;

function TApuracoesImpostos.getRecordApuracoesImpostos: RecApuracoesImpostos;
begin
  Result.MovimentoId       := getInt('MOVIMENTO_ID', True);
  Result.TipoMovimento     := getString('TIPO_MOVIMENTO', True);
  Result.CfopId            := getString('CFOP_ID', True);
  Result.EmpresaId         := getInt('EMPRESA_ID');
  Result.ValorContabil     := getDouble('VALOR_CONTABIL');
  Result.BaseCalculoIcms   := getDouble('BASE_CALCULO_ICMS');
  Result.ValorIcms         := getDouble('VALOR_ICMS');
  Result.BaseCaculoIcmsSt  := getDouble('BASE_CACULO_ICMS_ST');
  Result.ValorIcmsSt       := getDouble('VALOR_ICMS_ST');
  Result.ValorIpi          := getDouble('VALOR_IPI');
  Result.BaseCaculoPis     := getDouble('BASE_CACULO_PIS');
  Result.ValorPis          := getDouble('VALOR_PIS');
  Result.BaseCaculoCofins  := getDouble('BASE_CACULO_COFINS');
  Result.ValorCofins       := getDouble('VALOR_COFINS');
  Result.Natureza          := getString('NATUREZA');
  Result.DataContabil      := getData('DATA_CONTABIL');
end;

function BuscarApuracoesImpostos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecApuracoesImpostos>;
var
  i: Integer;
  t: TApuracoesImpostos;
begin
  Result := nil;
  t := TApuracoesImpostos.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordApuracoesImpostos;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarApuracoesImpostosComando(pConexao: TConexao; pComando: string): TArray<RecApuracoesImpostos>;
var
  i: Integer;
  t: TApuracoesImpostos;
begin
  Result := nil;
  t := TApuracoesImpostos.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordApuracoesImpostos;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;


end.
