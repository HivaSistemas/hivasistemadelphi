unit _OrcamentosTramites;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils;

{$M+}
type
  RecOrcamentoTramite = record
    orcamentoId: Integer;
    status: string;
    valorTotal: Double;
    statusAnalitico: string;
    dataHoraAlteracao: TDateTime;
    usuarioAlteracao: string;
    estacaoAlteracao: string;
  end;

  TOrcamentoTramite = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordMarca: RecOrcamentoTramite;
  end;

function BuscarTramites(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecOrcamentoTramite>;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TOrcamentoTramite }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ORCAMENTO_ID = :P1 ' +
      'order by ' +
      '  DATA_HORA_ALTERACAO desc'
    );

end;

constructor TOrcamentoTramite.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ORCAMENTOS_TRAMITES');

  FSql :=
    'select ' +
    '  ORC.ORCAMENTO_ID, ' +
    '  ORC.STATUS, ' +
    '  ORC.VALOR_TOTAL, ' +
    '  ORC.DATA_HORA_ALTERACAO, ' +
    '  ORC.ESTACAO_ALTERACAO, ' +
    '  ORC.USUARIO_SESSAO_ID || '' - '' || nvl(FUN.APELIDO, FUN.NOME) as USUARIO ' +
    'from ' +
    '  ORCAMENTOS_TRAMITES ORC ' +

    'inner join FUNCIONARIOS FUN ' +
    'on ORC.USUARIO_SESSAO_ID = FUN.FUNCIONARIO_ID ';

  SetFiltros(GetFiltros);

  AddColunaSL('ORCAMENTO_ID');
  AddColunaSL('STATUS');
  AddColunaSL('VALOR_TOTAL');
  AddColunaSL('DATA_HORA_ALTERACAO');
  AddColunaSL('ESTACAO_ALTERACAO');
  AddColunaSL('USUARIO');
end;

function TOrcamentoTramite.GetRecordMarca: RecOrcamentoTramite;
begin
  Result.orcamentoId       := getInt('ORCAMENTO_ID');
  Result.status            := getString('STATUS');

  Result.statusAnalitico   :=
    _BibliotecaGenerica.Decode(
      Result.status,[
        'OE', 'Or�amento',
        'OB', 'Or�amento bloqueado',
        'VB', 'Venda bloqueada',
        'VR', 'Venda ag. recebimen.',
        'VE', 'Aguard.receb.entrega',
        'RE', 'Recebido',
        'CA', 'Cancelado']
    );

  Result.usuarioAlteracao  := getString('USUARIO');
  Result.dataHoraAlteracao := getData('DATA_HORA_ALTERACAO');
  Result.valorTotal        := getDouble('VALOR_TOTAL');
  Result.estacaoAlteracao  := getString('ESTACAO_ALTERACAO');
end;


function BuscarTramites(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecOrcamentoTramite>;
var
  i: Integer;
  t: TOrcamentoTramite;
begin
  Result := nil;
  t := TOrcamentoTramite.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordMarca;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;


end.
