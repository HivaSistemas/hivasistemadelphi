unit _ProdutosDeptosSecoesLinhas;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros, System.StrUtils;

{$M+}
type
  TTipoCadastro = (ttNenhum, ttDepartamento, ttSecao, ttLinha);

  TProdutoDeptoSecaoLinha = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao; pNivel: Integer);
  protected
    function getRecordProdutoDeptoSecaoLinha: RecProdutoDeptoSecaoLinha;
  end;

function AtualizarProdutoDeptoSecaoLinha(
  pConexao: TConexao;
  pDeptoSecaoLinhaId: string;
  pNome: string;
  pDeptoSecaoLinhaPaiId: string;
  pAtivo: string;
  pTipoCadastro: TTipoCadastro
): RecRetornoBD;

function BuscarProdutoDeptoSecaoLinhas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pNivel: Integer
): TArray<RecProdutoDeptoSecaoLinha>;

function ExcluirProdutoDeptoSecaoLinha(
  pConexao: TConexao;
  pDeptoSecaoLinhaId: string
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TProdutoDeptoSecaoLinha }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where DEP1.DEPTO_SECAO_LINHA_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome do grupo',
      True,
      0,
      'where DEP1.NOME like :P1 || ''%'' '
    );
end;

constructor TProdutoDeptoSecaoLinha.Create(pConexao: TConexao; pNivel: Integer);
begin
  inherited Create(pConexao, 'PRODUTOS_DEPTOS_SECOES_LINHAS');

  FSql :=
    'select ' +
    '  DEP1.DEPTO_SECAO_LINHA_ID, ' +
    '  DEP1.DEPTO_SECAO_LINHA_PAI_ID, ' +
    '  DEP1.NOME, ' +
    '  DEP1.ATIVO, ' +

    '  case length(DEP1.DEPTO_SECAO_LINHA_ID) ' +
    '    when 3 then DEP1.DEPTO_SECAO_LINHA_ID ' +
    '    when 7 then DEP2.DEPTO_SECAO_LINHA_ID ' +
    '    when 11 then DEP2.DEPTO_SECAO_LINHA_ID ' +
    '    else null ' +
    '  end as DEPARTAMENTO_ID, ' +

    '  case length(DEP1.DEPTO_SECAO_LINHA_ID) ' +
    '    when 3 then DEP1.NOME ' +
    '    when 7 then DEP2.NOME ' +
    '    when 11 then DEP2.NOME ' +
    '    else null ' +
    '  end as NOME_DEPARTAMENTO, ' +

    '  case when length(DEP1.DEPTO_SECAO_LINHA_ID) in(7,11) then DEP3.DEPTO_SECAO_LINHA_ID else null end SECAO_ID, ' +
    '  case when length(DEP1.DEPTO_SECAO_LINHA_ID) in(7,11) then DEP3.NOME else null end as NOME_SECAO, ' +
    '  case when length(DEP1.DEPTO_SECAO_LINHA_ID)= 11 then DEP1.DEPTO_SECAO_LINHA_ID else null end as LINHA_ID, ' +
    '  case when length(DEP1.DEPTO_SECAO_LINHA_ID)= 11 then DEP1.NOME else null end as NOME_LINHA ' +
    'from ' +
    '  PRODUTOS_DEPTOS_SECOES_LINHAS DEP1 ' +

    'left join PRODUTOS_DEPTOS_SECOES_LINHAS DEP2 ' +
    'on substr(DEP1.DEPTO_SECAO_LINHA_ID, 1, 3) = DEP2.DEPTO_SECAO_LINHA_ID ' +

    'left join PRODUTOS_DEPTOS_SECOES_LINHAS DEP3 ' +
    'on substr(DEP1.DEPTO_SECAO_LINHA_ID, 1, 7) = DEP3.DEPTO_SECAO_LINHA_ID ';

  setFiltros(getFiltros);

  case pNivel of
    1: SetFiltros('and length(DEP1.DEPTO_SECAO_LINHA_ID) = 3 order by DEP1.NOME ');
    2: SetFiltros('and length(DEP1.DEPTO_SECAO_LINHA_ID) = 7 order by DEP1.NOME ');
    3: SetFiltros('and length(DEP1.DEPTO_SECAO_LINHA_ID) = 11 order by DEP1.NOME ');
  end;

  AddColuna('DEPTO_SECAO_LINHA_ID', True);
  AddColuna('NOME');
  AddColuna('DEPTO_SECAO_LINHA_PAI_ID');
  AddColuna('ATIVO');

  AddColunaSL('DEPARTAMENTO_ID');
  AddColunaSL('NOME_DEPARTAMENTO');
  AddColunaSL('SECAO_ID');
  AddColunaSL('NOME_SECAO');
  AddColunaSL('LINHA_ID');
  AddColunaSL('NOME_LINHA');
end;

function TProdutoDeptoSecaoLinha.getRecordProdutoDeptoSecaoLinha: RecProdutoDeptoSecaoLinha;
begin
  Result := RecProdutoDeptoSecaoLinha.Create;

  Result.DeptoSecaoLinhaId    := getString('DEPTO_SECAO_LINHA_ID', True);
  Result.Nome                 := getString('NOME');
  Result.DeptoSecaoLinhaPaiId := getString('DEPTO_SECAO_LINHA_PAI_ID');
  Result.Ativo                := getString('ATIVO');

  Result.DepartamentoId   := getString('DEPARTAMENTO_ID');
  Result.NomeDepartamento := getString('NOME_DEPARTAMENTO');
  Result.SecaoId          := getString('SECAO_ID');
  Result.NomeSecao        := getString('NOME_SECAO');
  Result.LinhaId          := getString('LINHA_ID');
  Result.NomeLinha        := getString('NOME_LINHA');

  Result.EstruturaPai :=
    IfThen(
      Length(Result.DeptoSecaoLinhaId) > 3,
      Result.DepartamentoId + ' - ' + Result.NomeDepartamento +
      IfThen(Length(Result.DeptoSecaoLinhaId) = 11, ' -> ' + Result.SecaoId + ' - ' + Result.NomeSecao)
    );

  Result.Estrutura :=
    IfThen(
      Length(Result.DeptoSecaoLinhaId) > 3,
      Result.NomeDepartamento +
      IfThen(Length(Result.DeptoSecaoLinhaId) = 7 , ' -> ' + Result.Nome, ' -> ' + Result.NomeSecao) +
      IfThen(Length(Result.DeptoSecaoLinhaId) = 11 , ' -> ' + Result.NomeLinha)
    );
end;

function AtualizarProdutoDeptoSecaoLinha(
  pConexao: TConexao;
  pDeptoSecaoLinhaId: string;
  pNome: string;
  pDeptoSecaoLinhaPaiId: string;
  pAtivo: string;
  pTipoCadastro: TTipoCadastro
): RecRetornoBD;
var
  vNovo: Boolean;
  vSeq: TSequencia;
  t: TProdutoDeptoSecaoLinha;
begin
  Result.TeveErro := False;
  t := TProdutoDeptoSecaoLinha.Create(pConexao, 0);

  vNovo := Pos('???', pDeptoSecaoLinhaId) > 0;
  if vNovo then begin
    if pTipoCadastro = ttDepartamento then
      vSeq := TSequencia.Create(pConexao, 'SEQ_PRODUTOS_DEPARTAMENTOS_ID')
    else if pTipoCadastro = ttSecao then
      vSeq := TSequencia.Create(pConexao, 'SEQ_PRODUTOS_SECOES_ID')
    else
      vSeq := TSequencia.Create(pConexao, 'SEQ_PRODUTOS_LINHAS_ID');

    pDeptoSecaoLinhaId := Copy(pDeptoSecaoLinhaId, 1, Pos('???', pDeptoSecaoLinhaId) - 1) + LPad(IntToStr(vSeq.GetProximaSequencia), 3, '0');
    Result.AsString := pDeptoSecaoLinhaId;
    vSeq.Free;
  end;

  try
    t.setString('DEPTO_SECAO_LINHA_ID', pDeptoSecaoLinhaId, True);
    t.setString('NOME', pNome);
    t.setStringN('DEPTO_SECAO_LINHA_PAI_ID', pDeptoSecaoLinhaPaiId);
    t.setString('ATIVO', pAtivo);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function BuscarProdutoDeptoSecaoLinhas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pNivel: Integer
): TArray<RecProdutoDeptoSecaoLinha>;
var
  i: Integer;
  t: TProdutoDeptoSecaoLinha;
begin
  Result := nil;
  t := TProdutoDeptoSecaoLinha.Create(pConexao, pNivel);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordProdutoDeptoSecaoLinha;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirProdutoDeptoSecaoLinha(
  pConexao: TConexao;
  pDeptoSecaoLinhaId: string
): RecRetornoBD;
var
  t: TProdutoDeptoSecaoLinha;
begin
  Result.TeveErro := False;
  t := TProdutoDeptoSecaoLinha.Create(pConexao, 0);

  try
    t.setString('DEPTO_SECAO_LINHA_ID', pDeptoSecaoLinhaId, True);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
