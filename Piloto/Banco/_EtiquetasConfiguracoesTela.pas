unit _EtiquetasConfiguracoesTela;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecEtiquetasConfiguracoesTela = record
    EmpresaId: Integer;
    CondicaoId: Integer;
    TipoCodigoImpressao: string;
    CaminhoImpressao: string;
    ModeloId: Integer;
  end;

  TEtiquetasConfiguracoesTela = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordEtiquetasConfiguracoesTela: RecEtiquetasConfiguracoesTela;
  end;

function AtualizarEtiquetasConfiguracoesTela(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pCondicaoId: Integer;
  pTipoCodigoImpressao: string;
  pCaminhoImpressao: string;
  pModeloId: Integer
): RecRetornoBD;

function BuscarEtiquetasConfiguracoesTela(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEtiquetasConfiguracoesTela>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TEtiquetasConfiguracoesTela }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where EMPRESA_ID = :P1'
    );
end;

constructor TEtiquetasConfiguracoesTela.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ETIQUETAS_CONFIGURACOES_TELA');

  FSql :=
    'select ' +
    '  EMPRESA_ID, ' +
    '  CONDICAO_ID, ' +
    '  TIPO_CODIGO_IMPRESSAO, ' +
    '  CAMINHO_IMPRESSAO, ' +
    '  MODELO_ID ' +
    'from ' +
    '  ETIQUETAS_CONFIGURACOES_TELA';

  setFiltros(getFiltros);

  AddColuna('EMPRESA_ID', True);
  AddColuna('CONDICAO_ID');
  AddColuna('TIPO_CODIGO_IMPRESSAO');
  AddColuna('CAMINHO_IMPRESSAO');
  AddColuna('MODELO_ID');
end;

function TEtiquetasConfiguracoesTela.getRecordEtiquetasConfiguracoesTela: RecEtiquetasConfiguracoesTela;
begin
  Result.EmpresaId           := getInt('EMPRESA_ID', True);
  Result.CondicaoId          := getInt('CONDICAO_ID');
  Result.TipoCodigoImpressao := getString('TIPO_CODIGO_IMPRESSAO');
  Result.CaminhoImpressao    := getString('CAMINHO_IMPRESSAO');
  Result.ModeloId            := getInt('MODELO_ID');
end;

function AtualizarEtiquetasConfiguracoesTela(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pCondicaoId: Integer;
  pTipoCodigoImpressao: string;
  pCaminhoImpressao: string;
  pModeloId: Integer
): RecRetornoBD;
var
  vNovo: Boolean;
  vSql: TConsulta;
  t: TEtiquetasConfiguracoesTela;
begin
  Result.Iniciar;
  pConexao.setRotina('ATU_ETIQUETAS_CONFIG_TELA');

  t := TEtiquetasConfiguracoesTela.Create(pConexao);
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select count(EMPRESA_ID) from ETIQUETAS_CONFIGURACOES_TELA where EMPRESA_ID = :P1');
  vSql.Pesquisar([pEmpresaId]);

  vNovo := vSql.GetInt(0) = 0;

  vSql.Active := False;
  vSql.Free;

  try
    pConexao.IniciarTransacao;

    t.setInt('EMPRESA_ID', pEmpresaId, True);
    t.setIntN('CONDICAO_ID', pCondicaoId);
    t.setString('TIPO_CODIGO_IMPRESSAO', pTipoCodigoImpressao);
    t.setString('CAMINHO_IMPRESSAO', pCaminhoImpressao);
    t.setInt('MODELO_ID', pModeloId);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarEtiquetasConfiguracoesTela(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEtiquetasConfiguracoesTela>;
var
  i: Integer;
  t: TEtiquetasConfiguracoesTela;
begin
  Result := nil;
  t := TEtiquetasConfiguracoesTela.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordEtiquetasConfiguracoesTela;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.

