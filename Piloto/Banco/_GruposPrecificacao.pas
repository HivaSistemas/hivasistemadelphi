unit _GruposPrecificacao;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecGruposPrecificacao = record
    EmpresaId: Integer;
    EmpresaGrupoId: Integer;
    NomeFantasia: string;
    Ordem: Integer;
  end;

  TGruposPrecificacao = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordGruposPrecificacao: RecGruposPrecificacao;
  end;

function BuscarGruposPrecificacao(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecGruposPrecificacao>;

function AtualizarGruposPrecificacao(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pEmpresasGrupos: TArray<Integer>
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TMovimentosProdutos }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where GRU.EMPRESA_ID = :P1 ' +
      'order by ' +
      '  GRU.ORDEM '
    );
end;

constructor TGruposPrecificacao.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'GRUPOS_PRECIFICACAO');

  FSql :=
    'select ' +
    '  GRU.EMPRESA_ID, ' +
    '  GRU.EMPRESA_GRUPO_ID, ' +
    '  EMP.NOME_FANTASIA, ' +
    '  GRU.ORDEM ' +
    'from ' +
    '  GRUPOS_PRECIFICACAO GRU ' +

    'inner join EMPRESAS EMP ' +
    'on GRU.EMPRESA_GRUPO_ID = EMP.EMPRESA_ID ';

  setFiltros(getFiltros);

  AddColuna('EMPRESA_ID', True);
  AddColuna('EMPRESA_GRUPO_ID', True);
  AddColunaSL('NOME_FANTASIA');
  AddColuna('ORDEM', True);
end;

function TGruposPrecificacao.getRecordGruposPrecificacao: RecGruposPrecificacao;
begin
  Result.EmpresaId      := getInt('EMPRESA_ID', True);
  Result.EmpresaGrupoId := getInt('EMPRESA_GRUPO_ID', True);
  Result.NomeFantasia   := getString('NOME_FANTASIA');
  Result.Ordem          := getInt('ORDEM', True);
end;

function BuscarGruposPrecificacao(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecGruposPrecificacao>;
var
  i: Integer;
  t: TGruposPrecificacao;
begin
  Result := nil;
  t := TGruposPrecificacao.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordGruposPrecificacao;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function AtualizarGruposPrecificacao(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pEmpresasGrupos: TArray<Integer>
): RecRetornoBD;
var
  t: TGruposPrecificacao;
  vExec: TExecucao;
  i: Integer;
begin
  Result.TeveErro := False;
  pConexao.SetRotina('ATUALIZAR_GRUPOS_PRECIFICACAO');
  t := TGruposPrecificacao.Create(pConexao);
  vExec := TExecucao.Create(pConexao);

  vExec.SQL.Add('delete from GRUPOS_PRECIFICACAO');
  vExec.SQL.Add('where EMPRESA_ID = :P1 ');

  try
    vExec.Executar([pEmpresaId]);

    for i := Low(pEmpresasGrupos) to High(pEmpresasGrupos) do begin
      t.setInt('EMPRESA_ID', pEmpresaId, True);
      t.setInt('EMPRESA_GRUPO_ID', pEmpresasGrupos[i], True);
      t.setInt('ORDEM', i, True);

      t.Inserir
    end;
  except
    on e: exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  t.Free;
end;

end.
