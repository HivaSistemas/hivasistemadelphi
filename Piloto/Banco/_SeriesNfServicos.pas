unit _SeriesNfServicos;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecSeriesNfServicos = record
    SrieNota: string;
  end;

  TSeriesNfServicos = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordSeriesNfServicos: RecSeriesNfServicos;
  end;

function AtualizarSeriesNfServicos(
  pConexao: TConexao;
  pSrieNota: string
): RecRetornoBD;

function BuscarSeriesNfServicos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecSeriesNfServicos>;

function ExcluirSeriesNfServicos(
  pConexao: TConexao;
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TSeriesNfServicos }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where XXX = :P1'
    );
end;

constructor TSeriesNfServicos.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'VW_SERIES_NF_SERVICOS');

  FSql := 
    'select ' +
    '  SRIE_NOTA, ' +
    'from ' +
    '  VW_SERIES_NF_SERVICOS';

  setFiltros(getFiltros);

  AddColuna('SRIE_NOTA');
end;

function TSeriesNfServicos.getRecordSeriesNfServicos: RecSeriesNfServicos;
begin
  Result.SrieNota                   := getString('SRIE_NOTA');
end;

function AtualizarSeriesNfServicos(
  pConexao: TConexao;
  pSrieNota: string
): RecRetornoBD;
var
  t: TSeriesNfServicos;
  vNovo: Boolean;
  vSeq: TSequencia;
begin
  Result.Iniciar;
  pConexao.setRotina('XXXXXXXXXXXXXXXXXXXX');

  t := TSeriesNfServicos.Create(pConexao);

  vNovo := pXXX = 0;

  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_XXX');
    pXXX := vSeq.getProximaSequencia;
    Result.AsInt := pXXX;
    vSeq.Free;
  end;

  try
    pConexao.IniciarTransacao; 

    t.setString('SRIE_NOTA', pSrieNota);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao; 
  except
    on e: Exception do begin
      pConexao.VoltarTransacao; 
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarSeriesNfServicos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecSeriesNfServicos>;
var
  i: Integer;
  t: TSeriesNfServicos;
begin
  Result := nil;
  t := TSeriesNfServicos.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordSeriesNfServicos;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirSeriesNfServicos(
  pConexao: TConexao;
): RecRetornoBD;
var
  t: TSeriesNfServicos;
begin
  Result.Iniciar;
  t := TSeriesNfServicos.Create(pConexao);

  try
    pConexao.IniciarTransacao;


    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
