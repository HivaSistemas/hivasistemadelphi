unit _OrcamentosPagamentos;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsFinanceiros,
  System.Variants;

{$M+}
type
  TOrcamentosPagamentos = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordOrcamentosPagamentos: RecTitulosFinanceiros;
  end;

function AtualizarOrcamentosPagamentos(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pCobrancaId: Integer;
  pItemId: Integer;
  pValor: Double;
  pTipo: string;
  pNsuTef: string;
  pCodigoAutorizacao: string
): RecRetornoBD;

function BuscarOrcamentosPagamentos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;

function ExcluirOrcamentosPagamentos(
  pConexao: TConexao
): RecRetornoBD;

function AtualizarTipoCobranca(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pCobrancaId: Integer;
  pItemIdCartao: Integer
): RecRetornoBD;

function AtualizarDadosCartoes(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pItemIdCartao: Integer;
  pNumeroCartao: string;
  pNsu: string;
  pCodigoAutorizacao: string
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TOrcamentosPagamentos }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 5);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where OPG.ORCAMENTO_ID = :P1 ' +
      'and TCO.TIPO_CARTAO = :P2 ' +
      'and OPG.TIPO = ''CR'' '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where OPG.ORCAMENTO_ID = :P1 ' +
      'and OPG.TIPO = ''CO'' ' +
      'order by ' +
      '  OPG.COBRANCA_ID, ' +
      '  OPG.DATA_VENCIMENTO '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ORC.TURNO_ID = :P1 ' +
      'and OPG.TIPO = :P2'
    );

  Result[3] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where OPG.ORCAMENTO_ID = :P1 ' +
      'and OPG.TIPO = ''FI'' ' +
      'order by ' +
      '  OPG.COBRANCA_ID, ' +
      '  OPG.DATA_VENCIMENTO '
    );

  Result[4] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where OPG.ORCAMENTO_ID = :P1 ' +
      'and OPG.TIPO = ''CR'' ',
      'order by ' +
      '  OPG.ITEM_ID'
    );
end;

constructor TOrcamentosPagamentos.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ORCAMENTOS_PAGAMENTOS');

  FSql :=
    'select ' +
    '  OPG.ORCAMENTO_ID, ' +
    '  OPG.COBRANCA_ID, ' +
    '  OPG.ITEM_ID, ' +
    '  OPG.VALOR, ' +
    '  OPG.TIPO, ' +
    '  OPG.DATA_VENCIMENTO, ' +
    '  OPG.NSU_TEF, ' +
    '  OPG.CODIGO_AUTORIZACAO, ' +
    '  OPG.TIPO_RECEB_CARTAO, ' +
    '  OPG.NUMERO_CARTAO, ' +
    '  OPG.PARCELA, ' +
    '  OPG.NUMERO_PARCELAS, ' +
    '  TCO.NOME as NOME_TIPO_COBRANCA, ' +
    '  TCO.TIPO_CARTAO, ' +
    '  TCO.TIPO_RECEBIMENTO_CARTAO, ' +
    '  CAD.NOME_FANTASIA, ' +
    '  TCO.UTILIZAR_LIMITE_CRED_CLIENTE ' +
    'from ' +
    '  ORCAMENTOS_PAGAMENTOS OPG ' +

    'inner join ORCAMENTOS ORC ' +
    'on OPG.ORCAMENTO_ID = ORC.ORCAMENTO_ID ' +

    'inner join TIPOS_COBRANCA TCO ' +
    'on OPG.COBRANCA_ID = TCO.COBRANCA_ID ' +

    'inner join CADASTROS CAD ' +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID ';

  setFiltros(getFiltros);

  AddColuna('ORCAMENTO_ID', True);
  AddColuna('COBRANCA_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('VALOR');
  AddColuna('TIPO');
  AddColuna('DATA_VENCIMENTO');
  AddColuna('NSU_TEF');
  AddColuna('CODIGO_AUTORIZACAO');
  AddColuna('TIPO_RECEB_CARTAO');
  AddColuna('NUMERO_CARTAO');
  AddColuna('PARCELA');
  AddColuna('NUMERO_PARCELAS');
  AddColunaSL('NOME_TIPO_COBRANCA');
  AddColunaSL('TIPO_CARTAO');
  AddColunaSL('TIPO_RECEBIMENTO_CARTAO');
  AddColunaSL('NOME_FANTASIA');
  AddColunaSL('UTILIZAR_LIMITE_CRED_CLIENTE');
end;

function TOrcamentosPagamentos.getRecordOrcamentosPagamentos: RecTitulosFinanceiros;
begin
  Result.Id                             := getInt('ORCAMENTO_ID', True);
  Result.CobrancaId                     := getInt('COBRANCA_ID', True);
  Result.ItemId                         := getInt('ITEM_ID', True);
  Result.valor                          := getDouble('VALOR');
  Result.tipo                           := getString('TIPO');
  Result.DataVencimento                 := getData('DATA_VENCIMENTO');
  Result.NsuTef                         := getString('NSU_TEF');
  Result.CodigoAutorizacao              := getString('CODIGO_AUTORIZACAO');
  Result.TipoRecebCartao                := getString('TIPO_RECEB_CARTAO');
  Result.NumeroCartao                   := getString('NUMERO_CARTAO');
  Result.Parcela                        := getInt('PARCELA');
  Result.NumeroParcelas                 := getInt('NUMERO_PARCELAS');
  Result.NomeCobranca                   := getString('NOME_TIPO_COBRANCA');
  Result.TipoCartao                     := getString('TIPO_CARTAO');
  Result.tipoRecebimentoCartao          := getString('TIPO_RECEBIMENTO_CARTAO');
  Result.UtilizarLimiteCredCliente      := getString('UTILIZAR_LIMITE_CRED_CLIENTE');
end;

function AtualizarOrcamentosPagamentos(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pCobrancaId: Integer;
  pItemId: Integer;
  pValor: Double;
  pTipo: string;
  pNsuTef: string;
  pCodigoAutorizacao: string
): RecRetornoBD;
var
  novo: Boolean;
  vSql: TConsulta;
  t: TOrcamentosPagamentos;
begin
  Result.TeveErro := False;
  vSql := TConsulta.Create(pConexao);
  t := TOrcamentosPagamentos.Create(pConexao);
  try
    vSql.SQL.Add('select count(*) ');
    vSql.SQL.Add('from ORCAMENTOS_PAGAMENTOS ');
    vSql.SQL.Add('where ORCAMENTO_ID = :P1 ');
    vSql.SQL.Add('and ITEM_ID = :P2');
    vSql.Pesquisar([pOrcamentoId, pItemId]);

    novo := vSql.GetInt(0) = 0;

    t.setInt('ORCAMENTO_ID', pOrcamentoId, True);
    t.setInt('COBRANCA_ID', pCobrancaId, True);
    t.setInt('ITEM_ID', pItemId, True);
    t.setDouble('VALOR', pValor);
    t.setString('TIPO', pTipo);
    t.setString('NSU_TEF', pNsuTef);
    t.setString('CODIGO_AUTORIZACAO', pCodigoAutorizacao);

    if novo then
      t.Inserir
    else
      t.Atualizar;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vSql.Active := False;
  vSql.Free;
  t.Free;
end;

function BuscarOrcamentosPagamentos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;
var
  i: Integer;
  t: TOrcamentosPagamentos;
begin
  Result := nil;
  t := TOrcamentosPagamentos.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordOrcamentosPagamentos;

      t.ProximoRegistro;
    end;
  end;
  t.Free;
end;

function ExcluirOrcamentosPagamentos(
  pConexao: TConexao
): RecRetornoBD;
var
  t: TOrcamentosPagamentos;
begin
  Result.TeveErro := False;
  t := TOrcamentosPagamentos.Create(pConexao);

  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function AtualizarTipoCobranca(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pCobrancaId: Integer;
  pItemIdCartao: Integer
): RecRetornoBD;
var
  vExec: TExecucao;
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_TIPO_COB_ORCAMENTO');

  vExec := TExecucao.Create(pConexao);
  vProc := TProcedimentoBanco.Create(pConexao, 'ATUALIZAR_RETENCOES_CONTAS_REC');

  try
    pConexao.IniciarTransacao;

    vExec.Limpar;
    vExec.Add('update ORCAMENTOS_PAGAMENTOS set');
    vExec.Add('  COBRANCA_ID = :P3 ');
    vExec.Add('where ORCAMENTO_ID = :P1 ');
    vExec.Add('and ITEM_ID = :P2 ');
    vExec.Executar([pOrcamentoId, pItemIdCartao, pCobrancaId]);

    vExec.Limpar;
    vExec.Add('update CONTAS_RECEBER set');
    vExec.Add('  COBRANCA_ID = :P3 ');
    vExec.Add('where ORCAMENTO_ID = :P1 ');
    vExec.Add('and ITEM_ID_CRT_ORCAMENTO = :P2 ');
    vExec.Executar([pOrcamentoId, pItemIdCartao, pCobrancaId]);

    vProc.Executar([pOrcamentoId, 'ORC']);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
  vProc.Free;
end;

function AtualizarDadosCartoes(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pItemIdCartao: Integer;
  pNumeroCartao: string;
  pNsu: string;
  pCodigoAutorizacao: string
): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_DADOS_CARTOES_REC');

  vExec := TExecucao.Create(pConexao);
  try
    pConexao.IniciarTransacao;

    vExec.Limpar;
    vExec.Add('update ORCAMENTOS_PAGAMENTOS set');
    vExec.Add('  NUMERO_CARTAO = :P3, ');
    vExec.Add('  NSU_TEF = :P4, ');
    vExec.Add('  CODIGO_AUTORIZACAO = :P5 ');
    vExec.Add('where ORCAMENTO_ID = :P1 ');
    vExec.Add('and ITEM_ID = :P2 ');
    vExec.Executar([pOrcamentoId, pItemIdCartao, pNumeroCartao, pNsu, pCodigoAutorizacao]);

    vExec.Limpar;
    vExec.Add('update CONTAS_RECEBER set');
    vExec.Add('  NUMERO_CARTAO_TRUNCADO = :P3, ');
    vExec.Add('  NSU = :P4, ');
    vExec.Add('  CODIGO_AUTORIZACAO_TEF = :P5 ');
    vExec.Add('where ORCAMENTO_ID = :P1 ');
    vExec.Add('and ITEM_ID_CRT_ORCAMENTO = :P2 ');
    vExec.Executar([pOrcamentoId, pItemIdCartao, pNumeroCartao, pNsu, pCodigoAutorizacao]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
end;

end.
