unit _GruposTribFederalVenda;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecGruposTribFederalVenda = class(TObject)
  public
    GrupoTribFederalVendaId: Integer;
    Descricao: string;
    OrigemProduto: Integer;
    CstPis: string;
    CstCofins: string;
    Ativo: string;
  end;

  TGruposTribFederalVenda = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordGruposTribFederalVenda: RecGruposTribFederalVenda;
  end;

function AtualizarGruposTribFederalVenda(
  pConexao: TConexao;
  pGrupoTribFederalVendaId: Integer;
  pDescricao: string;
  pOrigemProduto: Integer;
  pCstPis: string;
  pCstCofins: string;
  pAtivo: string
): RecRetornoBD;

function BuscarGruposTribFederalVenda(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecGruposTribFederalVenda>;

function ExcluirGruposTribFederalVenda(
  pConexao: TConexao;
  pGrupoTribFederalVendaId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TGruposTribFederalVenda }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where GRUPO_TRIB_FEDERAL_VENDA_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Descri��o (Avan�ado)',
      True,
      0,
      'where DESCRICAO like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  GRUPO_TRIB_FEDERAL_VENDA_ID ',
      '',
      True
    );
end;

constructor TGruposTribFederalVenda.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'GRUPOS_TRIB_FEDERAL_VENDA');

  FSql := 
    'select ' +
    '  GRUPO_TRIB_FEDERAL_VENDA_ID, ' +
    '  DESCRICAO, ' +
    '  ORIGEM_PRODUTO, ' +
    '  CST_PIS, ' +
    '  CST_COFINS, ' +
    '  ATIVO ' +
    'from ' +
    '  GRUPOS_TRIB_FEDERAL_VENDA';

  setFiltros(getFiltros);

  AddColuna('GRUPO_TRIB_FEDERAL_VENDA_ID', True);
  AddColuna('DESCRICAO');
  AddColuna('ORIGEM_PRODUTO');
  AddColuna('CST_PIS');
  AddColuna('CST_COFINS');
  AddColuna('ATIVO');
end;

function TGruposTribFederalVenda.getRecordGruposTribFederalVenda: RecGruposTribFederalVenda;
begin
  Result := RecGruposTribFederalVenda.Create;

  Result.GrupoTribFederalVendaId    := getInt('GRUPO_TRIB_FEDERAL_VENDA_ID', True);
  Result.Descricao                  := getString('DESCRICAO');
  Result.OrigemProduto              := getInt('ORIGEM_PRODUTO');
  Result.CstPis                     := getString('CST_PIS');
  Result.CstCofins                  := getString('CST_COFINS');
  Result.Ativo                      := getString('ATIVO');
end;

function AtualizarGruposTribFederalVenda(
  pConexao: TConexao;
  pGrupoTribFederalVendaId: Integer;
  pDescricao: string;
  pOrigemProduto: Integer;
  pCstPis: string;
  pCstCofins: string;
  pAtivo: string
): RecRetornoBD;
var
  t: TGruposTribFederalVenda;
  vNovo: Boolean;
  vSeq: TSequencia;
begin
  Result.Iniciar;
  pConexao.setRotina('ATUALIZAR_GRUPO_TRIB_FED_VEN');

  t := TGruposTribFederalVenda.Create(pConexao);

  vNovo := pGrupoTribFederalVendaId = 0;
  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_GRUPOS_TRIB_FEDERAL_VENDA');
    pGrupoTribFederalVendaId := vSeq.getProximaSequencia;
    Result.AsInt := pGrupoTribFederalVendaId;
    vSeq.Free;
  end;

  try
    pConexao.IniciarTransacao; 

    t.setInt('GRUPO_TRIB_FEDERAL_VENDA_ID', pGrupoTribFederalVendaId, True);
    t.setString('DESCRICAO', pDescricao);
    t.setInt('ORIGEM_PRODUTO', pOrigemProduto);
    t.setString('CST_PIS', pCstPis);
    t.setString('CST_COFINS', pCstCofins);
    t.setString('ATIVO', pAtivo);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao; 
  except
    on e: Exception do begin
      pConexao.VoltarTransacao; 
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarGruposTribFederalVenda(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecGruposTribFederalVenda>;
var
  i: Integer;
  t: TGruposTribFederalVenda;
begin
  Result := nil;
  t := TGruposTribFederalVenda.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordGruposTribFederalVenda;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirGruposTribFederalVenda(
  pConexao: TConexao;
  pGrupoTribFederalVendaId: Integer
): RecRetornoBD;
var
  t: TGruposTribFederalVenda;
begin
  Result.Iniciar;
  t := TGruposTribFederalVenda.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('GRUPO_TRIB_FEDERAL_VENDA_ID', pGrupoTribFederalVendaId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
