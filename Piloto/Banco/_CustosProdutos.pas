unit _CustosProdutos;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TCustoProduto = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordCustoProduto: RecCustoProduto;
  end;

function BuscarCustoProdutos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCustoProduto>;

function CustoCadastradoEmpresa(pConexao: TConexao; pProdutoId: Integer; pEmpresaId: Integer): Boolean;

function getFiltros: TArray<RecFiltros>;

implementation

{ TCustoProduto }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where PCU.PRODUTO_ID = :P1 ' +
      'and PCU.EMPRESA_ID = :P2 '
    );
end;

constructor TCustoProduto.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CUSTOS_PRODUTOS');

//BUSCAR_CUSTO_COMPRA_PRODUTO(PRE.EMPRESA_ID, PRO.PRODUTO_ID) CUSTO_COMPRA_COMERCIAL
  FSql :=
    'select ' +
    '  PCU.PRODUTO_ID, ' +
    '  PCU.EMPRESA_ID, ' +
    '  PCU.PRECO_FINAL,' +
    '  PCU.PRECO_FINAL_MEDIO,' +
    '  PCU.PRECO_LIQUIDO, ' +
    '  PCU.PRECO_LIQUIDO_MEDIO, ' +
    '  case PAE.TIPO_CUSTO_VIS_LUCRO_VENDA ' +
    '    when ''FIN'' then zvl(zvl(PCU.PRECO_LIQUIDO, PCU.CUSTO_ULTIMO_PEDIDO), BUSCAR_CUSTO_COMPRA_PRODUTO(PAE.EMPRESA_ID, PCU.PRODUTO_ID)) ' +
    '    when ''COM'' then zvl(zvl(PCU.CUSTO_ULTIMO_PEDIDO, PCU.PRECO_LIQUIDO), BUSCAR_CUSTO_COMPRA_PRODUTO(PAE.EMPRESA_ID, PCU.PRODUTO_ID)) ' +
    '    else PCU.CMV ' +
    '  end as CUSTO_COMPRA_COMERCIAL, ' +
    '  case PAE.TIPO_CUSTO_AJUSTE_ESTOQUE when ''FIN'' then zvl(PCU.PRECO_LIQUIDO, PCU.CUSTO_ULTIMO_PEDIDO) when ''COM'' then zvl(PCU.CUSTO_ULTIMO_PEDIDO, PCU.PRECO_LIQUIDO) else PCU.CMV end as CUSTO_AJUSTE_ESTOQUE ' +
    'from ' +
    '  VW_CUSTOS_PRODUTOS PCU ' +

    'inner join PARAMETROS_EMPRESA PAE ' +
    'on PCU.EMPRESA_ID = PAE.EMPRESA_ID ';

  setFiltros(getFiltros);

  AddColuna('PRODUTO_ID', True);
  AddColuna('EMPRESA_ID', True);
  AddColuna('PRECO_LIQUIDO');
  AddColuna('CUSTO_COMPRA_COMERCIAL');
  AddColuna('PRECO_FINAL');
  AddColuna('PRECO_FINAL_MEDIO');
  AddColuna('PRECO_LIQUIDO');
  AddColuna('PRECO_LIQUIDO_MEDIO');
  AddColuna('CUSTO_AJUSTE_ESTOQUE');
end;

function TCustoProduto.getRecordCustoProduto: RecCustoProduto;
begin
  Result.ProdutoId            := getInt('PRODUTO_ID', True);
  Result.EmpresaId            := getInt('EMPRESA_ID', True);
  Result.CustoCompraComercial := getDouble('CUSTO_COMPRA_COMERCIAL');
  Result.PrecoFinal           := getDouble('PRECO_FINAL');
  Result.PrecoFinalMedio      := getDouble('PRECO_FINAL_MEDIO');
  Result.PrecoLiquido         := getDouble('PRECO_LIQUIDO');
  Result.PrecoLiquidoMedio    := getDouble('PRECO_LIQUIDO_MEDIO');
  Result.CustoAjusteEstoque   := getDouble('CUSTO_AJUSTE_ESTOQUE');
end;

function BuscarCustoProdutos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCustoProduto>;
var
  i: Integer;
  t: TCustoProduto;
begin
  Result := nil;
  t := TCustoProduto.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordCustoProduto;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function CustoCadastradoEmpresa(pConexao: TConexao; pProdutoId: Integer; pEmpresaId: Integer): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  count(PRODUTO_ID) ');
  vSql.Add('from ');
  vSql.Add('  CUSTOS_PRODUTOS ');
  vSql.Add('where PRODUTO_ID = :P1 ');
  vSql.Add('and EMPRESA_ID = :P2');

  vSql.Pesquisar([pProdutoId, pEmpresaId]);

  Result := vSql.GetInt(0) > 0;

  vSql.Active := False;
  vSql.Free;
end;

end.
