unit _ContasPagarBaixas;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _Sessao, _RecordsFinanceiros, _ContasPagarBaixasPagtosDin,
  _ContasPagarBaixasPagtosChq, _ContasPagarBaixasPagamentos, _ContasPagarBaixasCreditos;

{$M+}
type
  TContasPagarBaixas = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordContasPagarBaixas: RecContasPagarBaixas;
  end;

function AtualizarContasPagarBaixas(
  pConexao: TConexao;
  pBaixaId: Integer;
  pEmpresaId: Integer;
  pCadastroId: Integer;
  pTurnoId: Integer;
  pStatusCaixa: string;
  pValorDinheiro: Double;
  pValorCheque: Double;
  pValorCartao: Double;
  pValorCobranca: Double;
  pValorCredito: Double;
  pValorTitulos: Double;
  pValorJuros: Double;
  pValorDesconto: Double;
  pDataPagamento: TDateTime;
  pObservacoes: string;
  pTipo: string;
  pPagarAdiantadoId: Integer;
  pDinheiro: TArray<RecTitulosBaixasPagtoDin>;
  pCartoes: TArray<RecTitulosFinanceiros>;
  pCobrancas: TArray<RecTitulosFinanceiros>;
  pCheques: TArray<RecTitulosFinanceiros>;
  pCreditosIds: TArray<Integer>;
  pTransacaoAberta: Boolean
): RecRetornoBD;

function BuscarContasPagarBaixas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecContasPagarBaixas>;

function CancelarBaixaContasPagar(pConexao: TConexao; pBaixaId: Integer): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TContasPagarBaixas }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where BAIXA_ID = :P1 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where PAGAR_ADIANTADO_ID = :P1 '
    );
end;

constructor TContasPagarBaixas.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONTAS_PAGAR_BAIXAS');

  FSql :=
    'select ' +
    '  CPB.BAIXA_ID, ' +
    '  CPB.EMPRESA_ID, ' +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA_BAIXA, ' +
    '  EMP.RAZAO_SOCIAL as RAZAO_SOCIAL_EMPRESA_BAIXA, ' +
    '  EMP.CNPJ as CNPJ_EMPRESA_BAIXA, ' +
    '  CPB.CADASTRO_ID, ' +
    '  CAD.RAZAO_SOCIAL as NOME_FORNECEDOR, ' +
    '  CAD.CPF_CNPJ as CPF_CNPJ_FORNECEDOR, ' +
    '  CPB.USUARIO_BAIXA_ID, ' +
    '  FUN.NOME as NOME_USUARIO_BAIXA, ' +
    '  CPB.VALOR_TITULOS, ' +
    '  CPB.VALOR_JUROS, ' +
    '  CPB.VALOR_DESCONTO, ' +
    '  CPB.VALOR_DINHEIRO, ' +
    '  CPB.VALOR_CHEQUE, ' +
    '  CPB.VALOR_CARTAO, ' +
    '  CPB.VALOR_COBRANCA, ' +
    '  CPB.VALOR_CREDITO, ' +
    '  CPB.VALOR_TROCA, ' +
    '  CPB.DATA_HORA_BAIXA, ' +
    '  CPB.DATA_PAGAMENTO, ' +
    '  CPB.OBSERVACOES, ' +
    '  CPB.TIPO, ' +
    '  CPB.VALOR_LIQUIDO, ' +
    '  CPB.BAIXA_RECEBER_ORIGEM_ID, ' +
    '  CPB.ORCAMENTO_ID, ' +
    '  CPB.ACUMULADO_ID, ' +
    '  CPB.TURNO_ID, ' +
    '  CPB.STATUS_CAIXA, ' +
    '  CPB.PAGAR_ADIANTADO_ID ' +
    'from ' +
    '  CONTAS_PAGAR_BAIXAS CPB ' +

    'inner join FUNCIONARIOS FUN ' +
    'on CPB.USUARIO_BAIXA_ID = FUN.FUNCIONARIO_ID ' +

    'inner join EMPRESAS EMP ' +
    'on CPB.EMPRESA_ID = EMP.EMPRESA_ID ' +

    'left join CADASTROS CAD ' +
    'on CPB.CADASTRO_ID = CAD.CADASTRO_ID ';

  setFiltros(getFiltros);

  AddColuna('BAIXA_ID', True);
  AddColuna('EMPRESA_ID');
  AddColunaSL('NOME_EMPRESA_BAIXA');
  AddColunaSL('RAZAO_SOCIAL_EMPRESA_BAIXA');
  AddColunaSL('CNPJ_EMPRESA_BAIXA');
  AddColuna('CADASTRO_ID');
  AddColunaSL('NOME_FORNECEDOR');
  AddColunaSL('CPF_CNPJ_FORNECEDOR');
  AddColunaSL('USUARIO_BAIXA_ID');
  AddColunaSL('NOME_USUARIO_BAIXA');
  AddColuna('VALOR_DINHEIRO');
  AddColuna('VALOR_CHEQUE');
  AddColuna('VALOR_CARTAO');
  AddColuna('VALOR_COBRANCA');
  AddColuna('VALOR_CREDITO');
  AddColunaSL('VALOR_TROCA');
  AddColuna('VALOR_TITULOS');
  AddColuna('VALOR_JUROS');
  AddColuna('VALOR_DESCONTO');
  AddColuna('TIPO');
  AddColunaSL('DATA_HORA_BAIXA');
  AddColuna('DATA_PAGAMENTO');
  AddColuna('OBSERVACOES');
  AddColunaSL('VALOR_LIQUIDO');
  AddColunaSL('BAIXA_RECEBER_ORIGEM_ID');
  AddColunaSL('ORCAMENTO_ID');
  AddColunaSL('ACUMULADO_ID');
  AddColuna('TURNO_ID');
  AddColuna('STATUS_CAIXA');
  AddColuna('PAGAR_ADIANTADO_ID');
end;

function TContasPagarBaixas.getRecordContasPagarBaixas: RecContasPagarBaixas;
begin
  Result.baixa_id                 := getInt('BAIXA_ID', True);
  Result.empresa_id               := getInt('EMPRESA_ID');
  Result.NomeEmpresaBaixa         := getString('NOME_EMPRESA_BAIXA');
  Result.RazaoSocialEmpresaBaixa  := getString('RAZAO_SOCIAL_EMPRESA_BAIXA');
  Result.CnpjEmpresaBaixa         := getString('CNPJ_EMPRESA_BAIXA');
  Result.CadastroId               := getInt('CADASTRO_ID');
  Result.NomeFornecedor           := getString('NOME_FORNECEDOR');
  Result.CpfCpnjFornecedor        := getString('CPF_CNPJ_FORNECEDOR');
  Result.usuario_baixa_id         := getInt('USUARIO_BAIXA_ID');
  Result.NomeUsuarioBaixa         := getString('NOME_USUARIO_BAIXA');
  Result.valor_dinheiro           := getDouble('VALOR_DINHEIRO');
  Result.valor_cheque             := getDouble('VALOR_CHEQUE');
  Result.valor_cartao             := getDouble('VALOR_CARTAO');
  Result.valor_cobranca           := getDouble('VALOR_COBRANCA');
  Result.valor_credito            := getDouble('VALOR_CREDITO');
  Result.ValorTroca               := getDouble('VALOR_TROCA');
  Result.ValorTitulos             := getDouble('VALOR_TITULOS');
  Result.ValorJuros               := getDouble('VALOR_JUROS');
  Result.valor_desconto           := getDouble('VALOR_DESCONTO');
  Result.data_hora_baixa          := getData('DATA_HORA_BAIXA');
  Result.data_pagamento           := getData('DATA_PAGAMENTO');
  Result.observacoes              := getString('OBSERVACOES');
  Result.Tipo                     := getString('TIPO');
  Result.ValorLiquido             := getDouble('VALOR_LIQUIDO');
  Result.BaixaReceberOrigemId     := getInt('BAIXA_RECEBER_ORIGEM_ID');
  Result.OrcamentoId              := getInt('ORCAMENTO_ID');
  Result.AcumuladoId              := getInt('ACUMULADO_ID');
  Result.TurnoId                  := getInt('TURNO_ID');
  Result.StatusCaixa              := getString('STATUS_CAIXA');
  Result.PagarAdiantadoId         := getInt('PAGAR_ADIANTADO_ID');

  Result.TipoAnalitico :=
    _Biblioteca.Decode(
      Result.Tipo,[
        'BCP', 'Baixa contas pagar',
        'CRE', 'Cr�dito a receber',
        'BXN', 'Baixa de t�tulos normais',
        'ECP', 'Encontro de contas a pagar',
        'FAC', 'Fechamento de acumulados',
        'FPE', 'Fechamento de pedido',
        'ADI', 'Adiantamento']
    );
end;

function AtualizarContasPagarBaixas(
  pConexao: TConexao;
  pBaixaId: Integer;
  pEmpresaId: Integer;
  pCadastroId: Integer;
  pTurnoId: Integer;
  pStatusCaixa: string;
  pValorDinheiro: Double;
  pValorCheque: Double;
  pValorCartao: Double;
  pValorCobranca: Double;
  pValorCredito: Double;
  pValorTitulos: Double;
  pValorJuros: Double;
  pValorDesconto: Double;
  pDataPagamento: TDateTime;
  pObservacoes: string;
  pTipo: string;
  pPagarAdiantadoId: Integer;
  pDinheiro: TArray<RecTitulosBaixasPagtoDin>;
  pCartoes: TArray<RecTitulosFinanceiros>;
  pCobrancas: TArray<RecTitulosFinanceiros>;
  pCheques: TArray<RecTitulosFinanceiros>;
  pCreditosIds: TArray<Integer>;
  pTransacaoAberta: Boolean
): RecRetornoBD;
var
  i: Integer;
  vNovo: Boolean;
  t: TContasPagarBaixas;

  vDin: TContasPagarBaixasPagtosDin;
  vChq: TContasPagarBaixasPagtosChq;
  vPagto: TContasPagarBaixasPagamento;
  vCreditos: TContasPagarBaixasCreditos;

  vExec: TExecucao;
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;

  t := TContasPagarBaixas.Create(pConexao);
  vDin := TContasPagarBaixasPagtosDin.Create(pConexao);
  vChq := TContasPagarBaixasPagtosChq.Create(pConexao);
  vPagto := TContasPagarBaixasPagamento.Create(pConexao);
  vCreditos := TContasPagarBaixasCreditos.Create(pConexao);

  vExec := TExecucao.Create(pConexao);
  vProc := TProcedimentoBanco.Create(pConexao, 'CONSOLIDAR_BAIXA_CONTAS_PAGAR');

  pConexao.SetRotina('ATU_CONTAS_PAG_BAIXAS');

  vNovo := pBaixaId = 0;
  if vNovo then begin
    pBaixaId := TSequencia.Create(pConexao, 'SEQ_CONTAS_PAGAR_BAIXAS').getProximaSequencia;
    Result.AsInt := pBaixaId;
  end;

  try
    if not pTransacaoAberta then
      pConexao.IniciarTransacao;

    t.setInt('BAIXA_ID', pBaixaId, True);
    t.setInt('EMPRESA_ID', pEmpresaId);
    t.setInt('CADASTRO_ID', pCadastroId);
    t.setIntN('TURNO_ID', pTurnoId);
    t.setIntN('PAGAR_ADIANTADO_ID', pPagarAdiantadoId);
    t.setString('STATUS_CAIXA', pStatusCaixa);
    t.setDouble('VALOR_TITULOS', pValorTitulos);
    t.setDouble('VALOR_JUROS', pValorJuros);
    t.setDouble('VALOR_DESCONTO', pValorDesconto);
    t.setDouble('VALOR_DINHEIRO', pValorDinheiro);
    t.setDouble('VALOR_CHEQUE', pValorCheque);
    t.setDouble('VALOR_CARTAO', pValorCartao);
    t.setDouble('VALOR_COBRANCA', pValorCobranca);
    t.setDouble('VALOR_CREDITO', pValorCredito);
    t.setString('TIPO', pTipo);

    t.setData('DATA_PAGAMENTO', pDataPagamento);
    t.setString('OBSERVACOES', pObservacoes);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    vExec.Clear;
    vExec.Add('delete from CONTAS_PAGAR_BAIXAS_PAGTOS_DIN ');
    vExec.Add('where BAIXA_ID = :P1');
    vExec.Executar([pBaixaId]);
    for i := Low(pDinheiro) to High(pDinheiro) do begin
      vDin.setInt('BAIXA_ID', pBaixaId, True);
      vDin.setString('CONTA_ID', pDinheiro[i].ContaId, True);
      vDin.setDouble('VALOR', pDinheiro[i].Valor);

      vDin.Inserir;
    end;

    vExec.Clear;
    vExec.Add('delete from CONTAS_REC_BAIXAS_PAGAMENTOS ');
    vExec.Add('where BAIXA_ID = :P1');
    vExec.Executar([pBaixaId]);

    vPagto.setInt('BAIXA_ID', pBaixaId, True);
    vPagto.setString('TIPO', 'CR');
    for i := Low(pCartoes) to High(pCartoes) do begin
      vPagto.setInt('COBRANCA_ID', pCartoes[i].CobrancaId, True);
      vPagto.setInt('ITEM_ID', i + 1, True);
      vPagto.setIntN('PARCELA', 0);
      vPagto.setIntN('NUMERO_PARCELAS', 0);
      vPagto.setDataN('DATA_VENCIMENTO', 0);
      vPagto.setStringN('NSU_TEF', '');
      vPagto.setDouble('VALOR', pCartoes[i].Valor);

      vPagto.Inserir;
    end;

    vPagto.setInt('BAIXA_ID', pBaixaId, True);
    vPagto.setString('TIPO', 'CO');
    for i := Low(pCobrancas) to High(pCobrancas) do begin
      vPagto.setInt('COBRANCA_ID', pCobrancas[i].CobrancaId, True);
      vPagto.setInt('ITEM_ID', Length(pCartoes) + i + 1, True);
      vPagto.setIntN('PARCELA', pCobrancas[i].Parcela);
      vPagto.setIntN('NUMERO_PARCELAS', pCobrancas[i].NumeroParcelas);
      vPagto.setDataN('DATA_VENCIMENTO', pCobrancas[i].DataVencimento);
      vPagto.setStringN('NSU_TEF', '');
      vPagto.setDouble('VALOR', pCobrancas[i].Valor);

      vPagto.Inserir;
    end;

    vExec.Clear;
    vExec.Add('delete from CONTAS_REC_BAIXAS_PAGTOS_CHQ ');
    vExec.Add('where BAIXA_ID = :P1');
    vExec.Executar([pBaixaId]);

    vChq.setInt('BAIXA_ID', pBaixaId, True);
    for i := Low(pCheques) to High(pCheques) do begin
      vChq.setInt('COBRANCA_ID', pCheques[i].CobrancaId, True);
      vChq.setInt('ITEM_ID', i + 1, True);
      vChq.setIntN('PARCELA', pCheques[i].Parcela);
      vChq.setIntN('NUMERO_PARCELAS', pCheques[i].NumeroParcelas);
      vChq.setDataN('DATA_VENCIMENTO', pCheques[i].DataVencimento);
      vChq.setDouble('VALOR_CHEQUE', pCheques[i].Valor);
      vChq.setInt('NUMERO_CHEQUE', pCheques[i].NumeroCheque);

      vChq.Inserir;
    end;

    for i := Low(pCreditosIds) to High(pCreditosIds) do begin
      vCreditos.setInt('BAIXA_ID', pBaixaId, True);
      vCreditos.setInt('RECEBER_ID', pCreditosIds[i], True);

      vCreditos.Inserir;
    end;

    vProc.Executar([pBaixaId]);

    if not pTransacaoAberta then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pTransacaoAberta then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;
  vCreditos.Free;
  vPagto.Free;
  vExec.Free;
  vProc.Free;
  vChq.Free;
  vDin.Free;
  t.Free;
end;

function BuscarContasPagarBaixas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecContasPagarBaixas>;
var
  i: Integer;
  t: TContasPagarBaixas;
begin
  Result := nil;
  t := TContasPagarBaixas.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordContasPagarBaixas;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function CancelarBaixaContasPagar(pConexao: TConexao; pBaixaId: Integer): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  pConexao.SetRotina('CANCELAR_BAIXA_CONTAS_PAGAR');

  Result.Iniciar;
  vProc := TProcedimentoBanco.Create(pConexao, 'CANCELAR_BAIXA_CONTAS_PAGAR');
  try
    pConexao.IniciarTransacao;

    vProc.Executar([pBaixaId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  vProc.Free;
end;

end.
