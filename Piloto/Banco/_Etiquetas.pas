unit _Etiquetas;

interface

uses
  _Conexao, _OperacoesBancoDados;

type
  RecEtiquetas = record
    ProdutoId: Integer;
    NomeProduto: string;
    PrecoVarejo: Double;
    MarcaId: Integer;
    NomeMarca: string;
    UnidadeVenda: string;
    CodigoBarras: string;
    CodigoOriginal: string;
    Quantidade: Integer;
    Caracteristicas: string;
    PrecoPromocional: Double;
    DataFinalPromocao: TDate;
  end;

function BuscarProdutos(pConexao: TConexao; pComando: string; pFiltroPromocao: Boolean): TArray<RecEtiquetas>;

implementation

function BuscarProdutos(pConexao: TConexao; pComando: string; pFiltroPromocao: Boolean): TArray<RecEtiquetas>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  PRO.PRODUTO_ID, ');
  vSql.Add('  PRO.NOME as NOME_PRODUTO, ');
  vSql.Add('  PRE.PRECO_VAREJO, ');
  vSql.Add('  PRO.MARCA_ID, ');
  vSql.Add('  MAR.NOME as NOME_MARCA, ');
  vSql.Add('  PRO.UNIDADE_VENDA, ');
  vSql.Add('  PRO.CODIGO_BARRAS, ');
  vSql.Add('  PRO.CODIGO_ORIGINAL_FABRICANTE, ');
  vSql.Add('  PRO.CARACTERISTICAS, ');
  vSql.Add('  PRV.DATA_FINAL_PROMOCAO, ');

  vSql.Add('  PRV.PRECO_PROMOCIONAL ');
  vSql.Add('from ');
  vSql.Add('  PRODUTOS PRO ');

  vSql.Add('inner join MARCAS MAR ');
  vSql.Add('on PRO.MARCA_ID = MAR.MARCA_ID  ');

  vSql.Add('inner join PRECOS_PRODUTOS PRE ');
  vSql.Add('on PRO.PRODUTO_ID = PRE.PRODUTO_ID ');

  vSql.Add('left join PRODUTOS_PROMOCOES PRV ');
  vSql.Add('on PRO.PRODUTO_ID = PRV.PRODUTO_ID ');
  vSql.Add('and PRE.EMPRESA_ID = PRV.EMPRESA_ID ');
  vSql.Add('and trunc(sysdate) between PRV.DATA_INICIAL_PROMOCAO and PRV.DATA_FINAL_PROMOCAO ');

  vSql.Add(pComando);

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].ProdutoId      := vSql.GetInt('PRODUTO_ID');
      Result[i].NomeProduto    := vSql.GetString('NOME_PRODUTO');
      Result[i].PrecoVarejo    := vSql.GetDouble('PRECO_VAREJO');
      Result[i].MarcaId        := vSql.GetInt('MARCA_ID');
      Result[i].NomeMarca      := vSql.GetString('NOME_MARCA');
      Result[i].UnidadeVenda   := vSql.GetString('UNIDADE_VENDA');
      Result[i].CodigoBarras   := vSql.GetString('CODIGO_BARRAS');
      Result[i].CodigoOriginal := vSql.GetString('CODIGO_ORIGINAL_FABRICANTE');
      Result[i].Caracteristicas := vSql.GetString('CARACTERISTICAS');
      Result[i].PrecoPromocional := vSql.GetDouble('PRECO_PROMOCIONAL');
      Result[i].DataFinalPromocao := vSql.GetData('DATA_FINAL_PROMOCAO');

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

end.
