unit _Estoques;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _RelacaoEstoque,
  _Biblioteca, System.SysUtils, _RecordsEstoques, _RecordsCadastros;

{$M+}
type
  TEstoque = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordEstoque: RecEstoque;
  end;

function BuscarEstoque(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant
): TArray<RecEstoque>;

function BuscarEstoqueComando(pConexao: TConexao;  pComando: string): TArray<RecEstoque>;
function AtualizarCMVProdutos(pConexao: TConexao; pProdutoId: Integer; empresaId: Integer; empresas: TArray<RecEmpresas>): RecRetornoBD;
function AtualizarCustoEstoque(pConexao: TConexao; vEstoques: TArray<RecRelacaoEstoque>; indice: Double; empresaId: Integer): RecRetornoBD;
function AtualizarCustoEstoqueRetroativo(pConexao: TConexao; vEstoques: TArray<RecRelacaoEstoque>; indice: Double; empresaId: Integer; periodoRetroativo: string): RecRetornoBD;
function AtualizarFinalizacaoMensal(
  pConexao: TConexao;
  pProdutos: TArray<RecProdutosAnoMesFinalizado>;
  pEmpresaId: Integer;
  pPeriodo: Integer
): RecRetornoBD;
function BuscarMesFinalizado(
  pConexao: TConexao;
  pComando: string;
  pEmpresaId: Integer;
  pAnoMes: Integer
): TArray<RecProdutosAnoMesFinalizado>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TEstoque }

uses _Empresas;

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where EST.PRODUTO_ID = :P1 ' +
      'and EST.EMPRESA_ID = :P2 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where EST.PRODUTO_ID = :P1 ' +
      'order by ' +
      '  EST.EMPRESA_ID '
    );
end;

constructor TEstoque.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ESTOQUES');

  FSql :=
    'select ' +
    '  EST.PRODUTO_ID, ' +
    '  EST.EMPRESA_ID, ' +
    '  EST.ESTOQUE, ' +
    '  EST.FISICO, ' +
    '  EST.RESERVADO, ' +
    '  EST.BLOQUEADO, ' +
    '  EST.DISPONIVEL, ' +
    '  EST.COMPRAS_PENDENTES, ' +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA ' +
    'from ' +
    '  VW_ESTOQUES EST ' +

    'inner join EMPRESAS EMP ' +
    'on EST.EMPRESA_ID = EMP.EMPRESA_ID ';

  setFiltros(getFiltros);

  AddColuna('PRODUTO_ID', True);
  AddColuna('EMPRESA_ID', True);
  AddColuna('ESTOQUE');
  AddColuna('FISICO');
  AddColuna('RESERVADO');
  AddColuna('BLOQUEADO');
  AddColuna('DISPONIVEL');
  AddColuna('COMPRAS_PENDENTES');
  AddColuna('NOME_EMPRESA');
end;

function TEstoque.getRecordEstoque: RecEstoque;
begin
  Result.produto_id  := getInt('PRODUTO_ID', True);
  Result.empresa_id  := getInt('EMPRESA_ID', True);
  Result.Estoque     := getDouble('ESTOQUE');
  Result.Fisico      := getDouble('FISICO');
  Result.Reservado   := getDouble('RESERVADO');
  Result.Bloqueado   := getDouble('BLOQUEADO');
  Result.Disponivel  := getDouble('DISPONIVEL');
  Result.ComprasPendentes := getDouble('COMPRAS_PENDENTES');
  Result.NomeEmpresa := getString('NOME_EMPRESA');
end;

function BuscarEstoque(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant
): TArray<RecEstoque>;
var
  i: Integer;
  Est: TEstoque;
begin
  Result := nil;
  Est := TEstoque.Create(pConexao);

  if Est.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, Est.GetQuantidadeRegistros);
    for i := 0 to Est.GetQuantidadeRegistros -1 do begin
      Result[i] := Est.getRecordEstoque;
      Est.ProximoRegistro;
    end;
  end;
  Est.Free;
end;

function BuscarEstoqueComando(pConexao: TConexao;  pComando: string): TArray<RecEstoque>;
var
  i: Integer;
  Est: TEstoque;
begin
  Result := nil;
  Est := TEstoque.Create(pConexao);

  if Est.PesquisarComando(pComando) then begin
    SetLength(Result, Est.GetQuantidadeRegistros);
    for i := 0 to Est.GetQuantidadeRegistros -1 do begin
      Result[i] := Est.getRecordEstoque;
      Est.ProximoRegistro;
    end;
  end;

  Est.Free;
end;

function AtualizarCMVProdutos(pConexao: TConexao; pProdutoId: Integer; empresaId: Integer; empresas: TArray<RecEmpresas>): RecRetornoBD;
var
  i: Integer;
  valorCMV: Double;
  pesquisaCMV: TConsulta;
  atualizarCMV: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_CMV_PRODUTO');
  atualizarCMV := TExecucao.Create(pConexao);
  pesquisaCMV := TConsulta.Create(pConexao);

  atualizarCMV.SQL.Text := 'update CUSTOS_PRODUTOS set CMV = :P1 where EMPRESA_ID = :P2 and PRODUTO_ID = :P3';
  pesquisaCMV.SQL.Add('select nvl(CMV, 0) as CMV from CUSTOS_PRODUTOS where PRODUTO_ID = :P1 and EMPRESA_ID = :P2');

  try
    pConexao.IniciarTransacao;

    for i := 0 to Length(empresas) - 1 do begin
      pesquisaCMV.Pesquisar([pProdutoId, empresas[i].EmpresaId]);

      if pesquisaCMV.GetDouble(0) > 0 then begin
        valorCMV := pesquisaCMV.GetDouble(0);
        Continue;
      end;
    end;

    if valorCMV > 0 then
      atualizarCMV.Executar([valorCMV, empresaId, pProdutoId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  pesquisaCMV.Free;
  atualizarCMV.Free;
end;

function AtualizarFinalizacaoMensal(
  pConexao: TConexao;
  pProdutos: TArray<RecProdutosAnoMesFinalizado>;
  pEmpresaId: Integer;
  pPeriodo: Integer
): RecRetornoBD;
var
  i: Integer;
  vExecEstoque: TExecucao;
  vExecCusto: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_FIN_MENSAL');
  vExecEstoque := TExecucao.Create(pConexao);
  vExecCusto := TExecucao.Create(pConexao);

  vExecEstoque.Add('update ESTOQUES_ANO_MES set ');
  vExecEstoque.Add('  ESTOQUE = :P1 ');
  vExecEstoque.Add('where PRODUTO_ID = :P2 ');
  vExecEstoque.Add('and ANO_MES = :P3 ');
  vExecEstoque.Add('and EMPRESA_ID = :P4 ');

  vExecCusto.Add('update CUSTOS_PRODUTOS_ANO_MES set ');
  vExecCusto.Add('  CMV = :P1, ');
  vExecCusto.Add('  CUSTO_ULTIMO_PEDIDO = :P2 ');
  vExecCusto.Add('where PRODUTO_ID = :P3 ');
  vExecCusto.Add('and ANO_MES = :P4 ');
  vExecCusto.Add('and EMPRESA_ID = :P5 ');

  try
    pConexao.IniciarTransacao;

    for I := Low(pProdutos) to High(pProdutos) do begin
      vExecEstoque.Executar([
        pProdutos[i].estoque,
        pProdutos[i].produtoId,
        pPeriodo,
        pEmpresaId
      ]);

      vExecCusto.Executar([
        pProdutos[i].cmv,
        pProdutos[i].custoUltimoPedido,
        pProdutos[i].produtoId,
        pPeriodo,
        pEmpresaId
      ]);
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
end;

function AtualizarCustoEstoque(pConexao: TConexao; vEstoques: TArray<RecRelacaoEstoque>; indice: Double; empresaId: Integer): RecRetornoBD;
var
  i: Integer;
  atualizarCMV: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_CMV_PRODUTO');
  atualizarCMV := TExecucao.Create(pConexao);

  atualizarCMV.SQL.Text := 'update CUSTOS_PRODUTOS set CMV = :P1 where EMPRESA_ID = :P2 and PRODUTO_ID = :P3';

  try
    pConexao.IniciarTransacao;

    for i := 0 to Length(vEstoques) - 1 do begin
      if vEstoques[i].CMV > 0 then
        atualizarCMV.Executar([vEstoques[i].CMV * indice, empresaId, vEstoques[i].ProdutoId]);
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  atualizarCMV.Free;
end;

function AtualizarCustoEstoqueRetroativo(pConexao: TConexao; vEstoques: TArray<RecRelacaoEstoque>; indice: Double; empresaId: Integer; periodoRetroativo: string): RecRetornoBD;
var
  i: Integer;
  atualizarCMV: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_CMV_PRODUTO');
  atualizarCMV := TExecucao.Create(pConexao);

  atualizarCMV.SQL.Text := 'update CUSTOS_PRODUTOS_ANO_MES set CMV = :P1 where EMPRESA_ID = :P2 and PRODUTO_ID = :P3 and ANO_MES = :P4';

  try
    pConexao.IniciarTransacao;

    for i := 0 to Length(vEstoques) - 1 do begin
      if vEstoques[i].CMV > 0 then
        atualizarCMV.Executar([vEstoques[i].CMV * indice, empresaId, vEstoques[i].ProdutoId, periodoRetroativo]);
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  atualizarCMV.Free;
end;

function BuscarMesFinalizado(
  pConexao: TConexao;
  pComando: string;
  pEmpresaId: Integer;
  pAnoMes: Integer
): TArray<RecProdutosAnoMesFinalizado>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  PRO.PRODUTO_ID, ');
  vSql.Add('  EST.ESTOQUE, ');
  vSql.Add('  CUS.CMV, ');
  vSql.Add('  CUS.CUSTO_ULTIMO_PEDIDO, ');
  vSql.Add('  PRO.NOME as NOME_PRODUTO, ');
  vSql.Add('  MAR.NOME as NOME_MARCA, ');
  vSql.Add('  EST.ANO_MES ');
  vSql.Add('from PRODUTOS PRO ');

  vSql.Add('inner join ESTOQUES_ANO_MES EST ');
  vSql.Add('on PRO.PRODUTO_ID = EST.PRODUTO_ID ');

  vSql.Add('inner join CUSTOS_PRODUTOS_ANO_MES CUS ');
  vSql.Add('on PRO.PRODUTO_ID = CUS.PRODUTO_ID ');

  vSql.Add('inner join MARCAS MAR ');
  vSql.Add('on PRO.MARCA_ID = MAR.MARCA_ID ');

  vSql.Add('where EST.EMPRESA_ID = :P1 ');
  vSql.Add('and CUS.EMPRESA_ID = :P1 ');
  vSql.Add('and EST.ANO_MES = :P2 ');
  vSql.Add('and CUS.ANO_MES = :P2 ');

  vSql.Add(pComando);

  if vSql.Pesquisar([pEmpresaId, pAnoMes]) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].produtoId           := vSql.GetInt('PRODUTO_ID');
      Result[i].nomeProduto         := vSql.GetString('NOME_PRODUTO');
      Result[i].nomeMarca           := vSql.GetString('NOME_MARCA');
      Result[i].estoque             := vSql.GetDouble('ESTOQUE');
      Result[i].cmv                 := vSql.GetDouble('CMV');
      Result[i].custoUltimoPedido   := vSql.GetDouble('CUSTO_ULTIMO_PEDIDO');

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

end.
