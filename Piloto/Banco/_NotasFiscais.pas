unit _NotasFiscais;

Interface

uses
  System.Classes, _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsNotasFiscais,
  _NotasFiscaisItens, System.Variants, System.UITypes, Vcl.Graphics, _NotasFiscaisReferencias;

{$M+}
type
  RecXMLs = record
    notaFiscalId: Integer;
    xml: TStringList;
    chave: string;
  end;

  RecValoresNotaFiscal = record
    valorDinheiro: Double;
    valorAcumulado: Double;
    valorCartaoCredito: Double;
    valorCartaoDebito: Double;
    valorCheque: Double;
    valorCobranca: Double;
    valorCredito: Double;
    valorPix: Double;
  end;

  RecDadosCartoes = record
    TipoRecebCartao: string;
    CnpjAdminsitradora: string;
    Bandeira: string;
    CodigoAutorizacao: string;
    TipoCartao: string;
    ValorCartao: Currency;
    TotalMovimento: Currency;
  end;

  TNotasFiscais = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordNotasFiscais: RecNotaFiscal;
  end;

function getCorTipoMovimento(pTipoMovimento: string): TColor;
function getTipoMovimentoAnalitico(pTipoMovimento: string): string;

function BuscarNotasFiscais(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant
): TArray<RecNotaFiscal>;

function BuscarNotasFiscaisComando(pConexao: TConexao; pComando: string): TArray<RecNotaFiscal>;

function AtualizarVariasNotasFiscais(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pRazaoSocial: string;
  pNomeFantasia: string;
  pCnpj: string;
  pInscricaoEstadual: string;
  pLogradouro: string;
  pComplemento: string;
  pNomeBairro: string;
  pNomeCidade: string;
  pNumero: string;
  pEstadoId: string;
  pCep: string;
  pTelefone: string;
  pCodigoIbgeMunicipio: Integer;
  pCodigoIbgeEstado: Integer;
  pNotasFiscaisIds: TArray<Integer>
): RecRetornoBD;

function AtualizarDestinatarioNotasFiscais(
  pConexao: TConexao;
  pNotasFiscaisIds: TArray<Integer>;
  pNomeFantasiaDestinatario: string;
  pRazaoSocialDestinatario: string;
  pTipoPessoaDestinatario: string;
  pCPFCNPJDestinatario: string;
  pInscricaoEstadualDestinat: string;
  pLogradouroDestinatario: string;
  pComplementoDestinatario: string;
  pNomeBairroDestinatario: string;
  pNomeCidadeDestinatario: string;
  pNumeroDestinatario: string;
  pEstadoIdDestinatario: string;
  pCepDestinatario: string;
  pCodigoIBGEMunicipioDest: Integer;
  pInscricaoEstEntrDestinat: string;
  pLogradouroEntrDestinatario: string;
  pcomplementoEntrDestinatario: string;
  pNomeBairroEntrDestinatario: string;
  pNomeCidadeEntrDestinatario: string;
  pNumeroEntrDestinatario: string;
  pEstadoIdEntrDestinatario: string;
  pCepEntrDestinatario: string;
  pCodigoIbgeMunicEntrDest: Integer
): RecRetornoBD;

function AtualizarNotasFiscais(
  pConexao: TConexao;
  pNotaFiscalId: Integer;
  pOutraNotaId: Integer;
  pCadastroId: Integer;
  pCFOPId: string;
  pEmpresaId: Integer;
  pOrcamentoBaseId: Integer;
  pOrcamentoId: Integer;
  pDevolucaoId: Integer;
  pAcumuladoId: Integer;
  pRetiradaId: Integer;
  pTipoMovimento: string;
  pNumeroNota: Integer;
  pModeloNota: string;
  pSerieNota: string;
  // Dados do emitente da nota
  pRazaoSocialEmitente: string;
  pNomeFantasiaEmitente: string;
  pRegimeTributario: string;
  pCNPJEmitente: string;
  pInscricaoEstadualEmitente: string;
  pLogradouroEmitente: string;
  pComplementoEmitente: string;
  pNomeBairroEmitente: string;
  pNomeCidadeEmitente: string;
  pNumeroEmitente: string;
  pEstadoIdEmitente: string;
  pCepEmitente: string;
  pTelefoneEmitente: string;
  pCodigoIBGEMunicipioEmit: Integer;
  pCodigoIBGEEstadoEmitent: Integer;
  // Dados do destinatario
  pNomeFantasiaDestinatario: string;
  pRazaoSocialDestinatario: string;
  pTipoPessoaDestinatario: string;
  pCPFCNPJDestinatario: string;
  pInscricaoEstadualDestinat: string;
  pLogradouroDestinatario: string;
  pComplementoDestinatario: string;
  pNomeBairroDestinatario: string;
  pNomeCidadeDestinatario: string;
  pNumeroDestinatario: string;
  pEstadoIdDestinatario: string;
  pCepDestinatario: string;
  pCodigoIBGEMunicipioDest: Integer;
  pInscricaoEstEntrDestinat: string;
  pLogradouroEntrDestinatario: string;
  pcomplementoEntrDestinatario: string;
  pNomeBairroEntrDestinatario: string;
  pNomeCidadeEntrDestinatario: string;
  pNumeroEntrDestinatario: string;
  pEstadoIdEntrDestinatario: string;
  pCepEntrDestinatario: string;
  pCodigoIbgeMunicEntrDest: Integer;
  pNomeConsumidorFinal: string;
  pTelefoneConsumidorFinal: string;
  pTipoNota: string;
  pNaturezaOperacao: string;
  // Valores da nota
  pValorTotal: Double;
  pValorTotalProdutos: Double;
  pValorDesconto: Double;
  pValorOutrasDespesas: Double;
  pValorFrete: Double;
  pValorSeguro: Double;
  pBaseCalculoICMS: Double;
  pValorICMS: Double;
  pBaseCalculoICMSST: Double;
  pValorICMSST: Double;
  pBaseCalculoPIS: Double;
  pValorPis: Double;
  pBaseCalculoCofins: Double;
  pValorCofins: Double;
  pValorIpi: Double;
  pPesoLiquido: Double;
  pPesoBruto: Double;
  pValorRecebidoDinheiro: Double;
  pValorRecebidoCartaoCred: Double;
  pValorRecebidoCartaoDeb: Double;
  pValorRecebidoCredito: Double;
  pValorRecebidoFinanceira: Double;
  pValorRecebidoCobranca: Double;
  pValorRecebidoCheque: Double;
  pDataHoraEmissao: TDateTime;
  pStatus: string;
  pNumeroReciboLoteNFE: string;
  pProtocoloNFE: string;
  pDataHoraProtocoloNFE: TDateTime;
  pChaveNFE: string;
  pStatusNFE: string;
  pMotivoStatusNFE: string;
  pProtocoloCancelamentoNFE: string;
  pMotivoCancelamentoNFE: string;
  pEnviouEmailNFECliente: string;
  pDanfeImpresso: string;
  pNotaFiscalOrigemCupomId: Integer;
  pMovimentarEstoqueOutrasNotas: string;
  pInformacoesComplementares: string;
  pNotaItens: TArray<RecNotaFiscalItem>;
  pNotasReferenciadas: TArray<RecNotasFiscaisReferencias>;
  pValorRecebidoPix: Double;
  pTransportadoraId: Integer;
  pQuantidadeNFe: Double = 0;
  pEspecieNFe: string = '';
  pTipoFrete: Integer = 9
): RecRetornoBD;

function ExcluirNotasFiscais(
  pConexao: TConexao;
  pNotaFiscalId: Integer
): RecRetornoBD;

function SetarCupomImpresso(
  pConexao: TConexao;
  pNotaFiscalId: Integer;
  pNumeroNota: Integer
): RecRetornoBD;

function SetarNFeEmitida(
  pConexao: TConexao;
  pNotaFiscalId: Integer;
  pProtocoloNF: string;
  pDataHoraProtocoloNFe: TDateTime;
  pChaveNFe: string;
  pDataHoraEmissao: TDateTime
): RecRetornoBD;

function AtualizarNumeroReciboLoteNFE(
  pConexao: TConexao;
  pNotaFiscalId: Integer;
  pNumeroNota: Integer;
  pNumeroRecibo: string;
  pStatusLoteNFE: string;
  pMotivo: string;
  pChaveNFE: string
): RecRetornoBD;

function AtualizarTiposPagamentoNota(
  pConexao: TConexao;
  pTiposPagamento: RecValoresNotaFiscal;
  pNotaFiscalId: Integer
): RecRetornoBD;

function AtualizarStatusLoteNFE(
  pConexao: TConexao;
  pNotaFiscalId: Integer;
  pStatusLoteNFE: string;
  pMotivo: string
): RecRetornoBD;

function SalvarXMLNFe(pConexao: TConexao; pNotaFiscalId: Integer; pCaminhoXML: string; pTextoXML: string): RecRetornoBD;

function SetarNFeCancelada(
  pConexao: TConexao;
  const pNotaFiscalId: Integer;
  const pProtocoloCancelamento: string;
  const pMotivo: string;
  const pDataHoraProtocolo: TDateTime
): RecRetornoBD;

function BuscarNotaFiscalIdOrcamento(pConexao: TConexao; const pOrcamentoId: Integer): Integer;
function BuscarNumeroNota(pConexao: TConexao; const pNotaFiscalId: Integer): Integer;
function SetarDanfeImpresso(pConexao: TConexao; pNotaFiscalId: Integer): RecRetornoBD;
function GerarNFeDeNFCe(pConexao: TConexao; const pNotaFiscalId: Integer): RecRetornoBD;
function BuscarXMLNFe(pConexao: TConexao; pNotaFiscalId: Integer): string;
function PodeCancelarNFe(pConexao: TConexao; pNotaFiscalId: Integer): RecRetornoBD;
function BuscarXMLsNFe(pConexao: TConexao; pComando: string): TArray<RecXMLs>;
function PedidoPodeGerarNFe(pConexao: TConexao; pOrcamentoId: Integer): RecRetornoBD;
function BuscarIdsNotasDevolucoes(pConexao: TConexao; pDevolucaoId: Integer): TArray<Integer>;
function BuscarDadosCartoesNFe(pConexao: TConexao; pMovimentoId: Integer; pTipoMovimento: string): TArray<RecDadosCartoes>;
function BuscarTiposPagamentosNota(pConexao: TConexao; pNotaFiscalId: Integer): Double;

function getFiltros: TArray<RecFiltros>;

implementation

{ TNotasFiscais }

function getCorTipoMovimento(pTipoMovimento: string): TColor;
begin
  Result := _Biblioteca.Decode(
    pTipoMovimento,
    [
      'VRA', coCorFonteEdicao1,
      'VRE', coCorFonteEdicao2,
      'VEN', coCorFonteEdicao3,
      'DRE', coCorFonteEdicao4,
      'DEN', clMaroon,
      'OUT', $000096DB,
      'ACU', clNavy,
      'DAC', clOlive,
      'TFI', $000045FF,
      'TFE', clLime,
      'DEF', clNavy,
      'NNE', coCorFonteEdicao1,
      'REN', coCorFonteEdicao2,
      'PEN', $000096DB,
      'PSA', $000096DB,
      'AEE', coCorFonteEdicao1,
      'AES', coCorFonteEdicao2,
      clBlack
    ]
  );
end;

function getTipoMovimentoAnalitico(pTipoMovimento: string): string;
begin
  Result := _Biblioteca.Decode(
    pTipoMovimento,
    [
      'VRA', 'Venda retirar ato',
      'VRE', 'Venda retirar',
      'VEN', 'Venda entregar',
      'DRE', 'Dev. venda retirar',
      'DEN', 'Dev. venda entregar',
      'OUT', 'Outras notas',
      'ACU', 'Acumulado',
      'DAC', 'Dev. acumulado',
      'TPE', 'Transf. produtos',
      'TFI', 'Transfer�ncia fiscal',
      'NNE', 'NFe de NFCe',
      'REN', 'Retorno parc/tot. entr.',
      'DEF', 'Dev. entrada de NF',
      'PEN', 'Produ��o entrada',
      'PSA', 'Produ��o sa�da',
      'AEE', 'Ajuste est. entrada',
      'AES', 'Ajuste est. sa�da',
      'N�o implementado'
    ]
  );
end;

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 8);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where NOTA_FISCAL_ID = :P1 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ORCAMENTO_BASE_ID = :P1 ' +
      'order by ' +
      '  NOTA_FISCAL_ID '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where DATA_HORA_EMISSAO is null ' +
      'and NFI.EMPRESA_ID = :P1 ' +
      'and TIPO_MOVIMENTO = ''VRA'' ' +
      'and NUMERO_NOTA is not null ' +
      'order by ' +
      '  TIPO_NOTA,  ' +
      '  NOTA_FISCAL_ID '
    );

  Result[3] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'order by ' +
      '  TIPO_NOTA, ' +
      '  NOTA_FISCAL_ID desc '
    );

  Result[4] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where NOTA_FISCAL_ID = :P1 ' +
      'and TIPO_MOVIMENTO = ''OUT'' '
    );

  Result[5] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where RETIRADA_ID = :P1 '
    );

  Result[6] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where NFI.DEVOLUCAO_ID = :P1 ' +
      'order by ' +
      '  NFI.NOTA_FISCAL_ID '
    );

  Result[7] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where NFI.ACUMULADO_ID = :P1 ',
      'order by ' +
      '  NFI.NOTA_FISCAL_ID '
    );
end;

constructor TNotasFiscais.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'NOTAS_FISCAIS');

  FSql :=
    'select ' +
    '  NFI.NOTA_FISCAL_ID, ' +
    '  NFI.CADASTRO_ID, ' +
    '  NFI.CFOP_ID, ' +
    '  NFI.EMPRESA_ID, ' +
    '  NFI.ORCAMENTO_BASE_ID, ' +
    '  NFI.ORCAMENTO_ID, ' +
    '  NFI.DEVOLUCAO_ID, ' +
    '  NFI.ACUMULADO_ID, ' +
    '  NFI.RETIRADA_ID, ' +
    '  NFI.PRODUCAO_ESTOQUE_ID, ' +
    '  NFI.AJUSTE_ESTOQUE_ID, ' +
    '  NFI.ENTREGA_ID, ' +
    '  NFI.NUMERO_NOTA, ' +
    '  NFI.MODELO_NOTA, ' +
    '  NFI.SERIE_NOTA, ' +
    '  NFI.TIPO_MOVIMENTO, ' +
    '  NFI.VALOR_ICMS, ' +
    '  NFI.BASE_CALCULO_ICMS, ' +
    '  NFI.VALOR_OUTRAS_DESPESAS, ' +
    '  NFI.VALOR_DESCONTO, ' +
    '  NFI.VALOR_TOTAL_PRODUTOS, ' +
    '  NFI.BASE_CALCULO_ICMS_ST, ' +
    '  NFI.VALOR_ICMS_ST, ' +
    '  NFI.BASE_CALCULO_PIS, ' +
    '  NFI.VALOR_PIS, ' +
    '  NFI.VALOR_TOTAL, ' +
    '  NFI.VALOR_COFINS, ' +
    '  NFI.VALOR_IPI, ' +
    '  NFI.VALOR_SEGURO, ' +
    '  NFI.VALOR_FRETE, ' +
    '  NFI.VALOR_RECEBIDO_CARTAO_CRED, ' +
    '  NFI.VALOR_RECEBIDO_CARTAO_DEB, ' +
    '  NFI.VALOR_RECEBIDO_DINHEIRO, ' +
    '  NFI.VALOR_RECEBIDO_PIX, ' +
    '  NFI.VALOR_RECEBIDO_COBRANCA, ' +
    '  NFI.VALOR_RECEBIDO_CREDITO, ' +
    '  NFI.VALOR_RECEBIDO_FINANCEIRA, ' +
    '  NFI.VALOR_RECEBIDO_CHEQUE, ' +
    '  NFI.BASE_CALCULO_COFINS, ' +
    '  NFI.CHAVE_NFE, ' +
    '  NFI.DATA_HORA_PROTOCOLO_NFE, ' +
    '  NFI.PROTOCOLO_NFE, ' +
    '  NFI.NUMERO_RECIBO_LOTE_NFE, ' +
    '  NFI.STATUS_NFE, ' +
    '  NFI.MOTIVO_STATUS_NFE, ' +
    '  NFI.STATUS, ' +
    '  NFI.NATUREZA_OPERACAO, ' +
    '  NFI.PESO_LIQUIDO, ' +
    '  NFI.PESO_BRUTO, ' +
    '  NFI.DATA_HORA_EMISSAO, ' +
    '  NFI.DATA_HORA_CADASTRO, ' +
    '  NFI.ENVIOU_EMAIL_NFE_CLIENTE, ' +
    '  NFI.DANFE_IMPRESSO, ' +
    '  NFI.NOTA_FISCAL_ORIGEM_CUPOM_ID, ' +
    '  NFI.RAZAO_SOCIAL_EMITENTE, ' +
    '  NFI.NOME_FANTASIA_EMITENTE, ' +
    '  NFI.REGIME_TRIBUTARIO, ' +
    '  NFI.CNPJ_EMITENTE, ' +
    '  NFI.INSCRICAO_ESTADUAL_EMITENTE, ' +
    '  NFI.LOGRADOURO_EMITENTE, ' +
    '  NFI.COMPLEMENTO_EMITENTE, ' +
    '  NFI.NOME_BAIRRO_EMITENTE, ' +
    '  NFI.NOME_CIDADE_EMITENTE, ' +
    '  NFI.NUMERO_EMITENTE, ' +
    '  NFI.ESTADO_ID_EMITENTE, ' +
    '  NFI.CEP_EMITENTE, ' +
    '  NFI.TELEFONE_EMITENTE, ' +
    '  NFI.CODIGO_IBGE_MUNICIPIO_EMIT, ' +
    '  NFI.CODIGO_IBGE_ESTADO_EMITENT, ' +
    '  NFI.ESTADO_ID_DESTINATARIO, ' +
    '  NFI.NOME_CIDADE_DESTINATARIO, ' +
    '  NFI.NOME_BAIRRO_DESTINATARIO, ' +
    '  NFI.COMPLEMENTO_DESTINATARIO, ' +
    '  NFI.LOGRADOURO_DESTINATARIO, ' +
    '  NFI.CODIGO_IBGE_MUNICIPIO_DEST, ' +
    '  NFI.TIPO_NOTA, ' +
    '  NFI.TELEFONE_CONSUMIDOR_FINAL, ' +
    '  NFI.NOME_CONSUMIDOR_FINAL, ' +
    '  NFI.INSCRICAO_ESTADUAL_DESTINAT, ' +
    '  case when NFI.CADASTRO_ID = PAR.CADASTRO_CONSUMIDOR_FINAL_ID then NFI.CPF_CONSUMIDOR_FINAL ELSE NFI.CPF_CNPJ_DESTINATARIO END AS CPF_CNPJ_DESTINATARIO, ' +
    '  NFI.TIPO_PESSOA_DESTINATARIO, ' +
    '  NFI.RAZAO_SOCIAL_DESTINATARIO, ' +
    '  NFI.NOME_FANTASIA_DESTINATARIO, ' +
    '  NFI.NUMERO_DESTINATARIO, ' +
    '  NFI.CEP_DESTINATARIO, ' +
    '  NFI.INSCRICAO_EST_ENTR_DESTINAT, ' +
    '  NFI.LOGRADOURO_ENTR_DESTINATARIO, ' +
    '  NFI.COMPLEMENTO_ENTR_DESTINATARIO, ' +
    '  NFI.NOME_BAIRRO_ENTR_DESTINATARIO, ' +
    '  NFI.NOME_CIDADE_ENTR_DESTINATARIO, ' +
    '  NFI.NUMERO_ENTR_DESTINATARIO, ' +
    '  NFI.ESTADO_ID_ENTR_DESTINATARIO, ' +
    '  NFI.CEP_ENTR_DESTINATARIO, ' +
    '  NFI.CODIGO_IBGE_MUNIC_ENTR_DEST, ' +
    '  decode(NFI.STATUS, ''E'', ''Emitida'', ''N'', ''N�o emitida'', ''C'', ''Cancelada'', ''D'', ''Denegada'', ''N�o implementado'') as STATUS_NOTA_ANALITICO, ' +
    '  decode(NFI.TIPO_NOTA, ''C'', ''NFC-e'', ''N'', ''NF-e'') as TIPO_NOTA_ANALITICO, ' +
    '  NFI.DATA_HORA_CANCELAMENTO_NOTA, ' +
    '  NFI.MOTIVO_CANCELAMENTO_NFE, ' +
    '  NFI.PROTOCOLO_CANCELAMENTO_NFE, ' +
    '  NFI.MOVIMENTAR_EST_OUTRAS_NOTAS, ' +
    '  NFI.INFORMACOES_COMPLEMENTARES, ' +
    '  NFI.OUTRA_NOTA_ID, ' +
    '  NFI.INDICE_FORMAS_PAGAMENTO, ' +
    '  NFI.TRANSFERENCIA_PRODUTOS_ID, ' +
    '  NFI.DEVOLUCAO_ENTRADA_ID, ' +
    '  NFI.ESPECIE_NFE, ' +
    '  NFI.QUANTIDADE_NFE, ' +
    '  nvl(CLI.TIPO_CLIENTE, ''X'') as TIPO_CLIENTE, ' +
    '  EMP.TELEFONE_FAX, ' +
    '  VALOR_RECEBIDO_ACUMULADO, ' +
    '  CAD.E_MAIL, '+
    '  NVL(NFI.TIPO_FRETE,9) AS TIPO_FRETE, ' +
    '  NFI.TRANSPORTADORA_ID, ' +
    '  CAD.TELEFONE_PRINCIPAL AS TELEFONE_DESTINATARIO, ' +
    '  CTR.CPF_CNPJ AS CNPJ_TRANSP, ' +
    '  CTR.RAZAO_SOCIAL AS RAZAO_TRANSP, ' +
    '  CTR.INSCRICAO_ESTADUAL AS IE_TRANSP, ' +
    '  CTR.LOGRADOURO AS END_TRANSP, ' +
    '  CID.NOME AS CIDADE_TRANSP, ' +
    '  CID.ESTADO_ID AS UF_TRANSP, ' +
    '  NFI.VALOR_ICMS_INTER ' +

    'from ' +
    '  NOTAS_FISCAIS NFI ' +

    'left join CLIENTES CLI ' +
    'on NFI.CADASTRO_ID = CLI.CADASTRO_ID ' +

    'left join CADASTROS CAD '+
    'on CLI.CADASTRO_ID = CAD.CADASTRO_ID '+

    'left join ORCAMENTOS ORC ' +
    'on ORC.ORCAMENTO_ID = NFI.ORCAMENTO_ID ' +

    'left join TRANSPORTADORAS TRA ' +
    'on NFI.TRANSPORTADORA_ID = TRA.CADASTRO_ID ' +

    'left join CADASTROS CTR ' +
    'on TRA.CADASTRO_ID = CTR.CADASTRO_ID ' +

    'left join BAIRROS BAI ' +
    'on CTR.BAIRRO_ID = BAI.BAIRRO_ID ' +

    'left join CIDADES CID ' +
    'on CID.CIDADE_ID = BAI.CIDADE_ID ' +

    'inner join EMPRESAS EMP ' +
    'on EMP.EMPRESA_ID = NFI.EMPRESA_ID ' +

    'cross join PARAMETROS PAR';

  setFiltros(getFiltros);

  AddColuna('NOTA_FISCAL_ID', True);
  AddColuna('CADASTRO_ID');
  AddColuna('CFOP_ID');
  AddColuna('EMPRESA_ID');
  AddColuna('ORCAMENTO_BASE_ID');
  AddColuna('ORCAMENTO_ID');
  AddColuna('DEVOLUCAO_ID');
  AddColuna('ACUMULADO_ID');
  AddColuna('RETIRADA_ID');
  AddColunaSL('ENTREGA_ID');
  AddColunaSL('PRODUCAO_ESTOQUE_ID');
  AddColunaSL('AJUSTE_ESTOQUE_ID');
  AddColuna('NUMERO_NOTA');
  AddColuna('MODELO_NOTA');
  AddColuna('SERIE_NOTA');
  AddColuna('TIPO_MOVIMENTO');
  AddColuna('VALOR_ICMS');
  AddColuna('BASE_CALCULO_ICMS');
  AddColuna('VALOR_OUTRAS_DESPESAS');
  AddColuna('VALOR_DESCONTO');
  AddColuna('VALOR_TOTAL_PRODUTOS');
  AddColuna('BASE_CALCULO_ICMS_ST');
  AddColuna('VALOR_ICMS_ST');
  AddColuna('BASE_CALCULO_PIS');
  AddColuna('VALOR_PIS');
  AddColuna('VALOR_TOTAL');
  AddColuna('VALOR_COFINS');
  AddColuna('VALOR_IPI');
  AddColuna('VALOR_SEGURO');
  AddColuna('VALOR_FRETE');
  AddColuna('VALOR_RECEBIDO_CARTAO_CRED');
  AddColuna('VALOR_RECEBIDO_CARTAO_DEB');
  AddColuna('VALOR_RECEBIDO_CHEQUE');
  AddColuna('VALOR_RECEBIDO_DINHEIRO');
  AddColuna('VALOR_RECEBIDO_PIX');
  AddColuna('VALOR_RECEBIDO_COBRANCA');
  AddColuna('VALOR_RECEBIDO_CREDITO');
  AddColuna('VALOR_RECEBIDO_FINANCEIRA');
  AddColuna('BASE_CALCULO_COFINS');
  AddColuna('CHAVE_NFE');
  AddColuna('DATA_HORA_PROTOCOLO_NFE');
  AddColuna('PROTOCOLO_NFE');
  AddColuna('NUMERO_RECIBO_LOTE_NFE');
  AddColuna('STATUS_NFE');
  AddColuna('MOTIVO_STATUS_NFE');
  AddColuna('STATUS');
  AddColuna('NATUREZA_OPERACAO');
  AddColuna('PESO_LIQUIDO');
  AddColuna('PESO_BRUTO');
  AddColuna('DATA_HORA_EMISSAO');
  AddColunaSL('DATA_HORA_CADASTRO');
  AddColuna('ENVIOU_EMAIL_NFE_CLIENTE');
  AddColuna('DANFE_IMPRESSO');
  AddColuna('NOTA_FISCAL_ORIGEM_CUPOM_ID');
  AddColuna('MOVIMENTAR_EST_OUTRAS_NOTAS');

  AddColuna('RAZAO_SOCIAL_EMITENTE');
  AddColuna('NOME_FANTASIA_EMITENTE');
  AddColuna('REGIME_TRIBUTARIO');
  AddColuna('CNPJ_EMITENTE');
  AddColuna('INSCRICAO_ESTADUAL_EMITENTE');
  AddColuna('LOGRADOURO_EMITENTE');
  AddColuna('COMPLEMENTO_EMITENTE');
  AddColuna('NOME_BAIRRO_EMITENTE');
  AddColuna('NOME_CIDADE_EMITENTE');
  AddColuna('NUMERO_EMITENTE');
  AddColuna('ESTADO_ID_EMITENTE');
  AddColuna('CEP_EMITENTE');
  AddColuna('TELEFONE_EMITENTE');
  AddColuna('CODIGO_IBGE_MUNICIPIO_EMIT');
  AddColuna('CODIGO_IBGE_ESTADO_EMITENT');

  AddColuna('ESTADO_ID_DESTINATARIO');
  AddColuna('NOME_CIDADE_DESTINATARIO');
  AddColuna('NOME_BAIRRO_DESTINATARIO');
  AddColuna('COMPLEMENTO_DESTINATARIO');
  AddColuna('LOGRADOURO_DESTINATARIO');
  AddColuna('CODIGO_IBGE_MUNICIPIO_DEST');
  AddColuna('INSCRICAO_ESTADUAL_DESTINAT');
  AddColuna('CPF_CNPJ_DESTINATARIO');
  AddColuna('RAZAO_SOCIAL_DESTINATARIO');
  AddColuna('NOME_FANTASIA_DESTINATARIO');
  AddColuna('NUMERO_DESTINATARIO');
  AddColuna('CEP_DESTINATARIO');

  AddColuna('INSCRICAO_EST_ENTR_DESTINAT');
  AddColuna('LOGRADOURO_ENTR_DESTINATARIO');
  AddColuna('COMPLEMENTO_ENTR_DESTINATARIO');
  AddColuna('NOME_BAIRRO_ENTR_DESTINATARIO');
  AddColuna('NOME_CIDADE_ENTR_DESTINATARIO');
  AddColuna('NUMERO_ENTR_DESTINATARIO');
  AddColuna('ESTADO_ID_ENTR_DESTINATARIO');
  AddColuna('CEP_ENTR_DESTINATARIO');
  AddColuna('CODIGO_IBGE_MUNIC_ENTR_DEST');

  AddColuna('TIPO_NOTA');
  AddColuna('TELEFONE_CONSUMIDOR_FINAL');
  AddColuna('NOME_CONSUMIDOR_FINAL');
  AddColuna('TIPO_PESSOA_DESTINATARIO');
  AddColuna('INFORMACOES_COMPLEMENTARES');
  AddColuna('OUTRA_NOTA_ID');
  AddColunaSL('TRANSFERENCIA_PRODUTOS_ID');
  AddColunaSL('DEVOLUCAO_ENTRADA_ID');
  AddColunaSL('TIPO_CLIENTE');
  AddColunaSL('INDICE_FORMAS_PAGAMENTO');

  AddColunaSL('STATUS_NOTA_ANALITICO');
  AddColunaSL('TIPO_NOTA_ANALITICO');
  AddColunaSL('DATA_HORA_CANCELAMENTO_NOTA');
  AddColunaSL('MOTIVO_CANCELAMENTO_NFE');
  AddColunaSL('PROTOCOLO_CANCELAMENTO_NFE');
  AddColunaSL('TELEFONE_FAX');
  AddColunaSL('VALOR_RECEBIDO_ACUMULADO');
  AddColunaSL('E_MAIL');
  AddColunaSL('TELEFONE_DESTINATARIO');
  AddColuna('QUANTIDADE_NFE');
  AddColuna('ESPECIE_NFE');
  AddColuna('TIPO_FRETE');

  AddColuna('TRANSPORTADORA_ID');
  AddColunaSL('CNPJ_TRANSP');
  AddColunaSL('RAZAO_TRANSP');
  AddColunaSL('IE_TRANSP');
  AddColunaSL('END_TRANSP');
  AddColunaSL('CIDADE_TRANSP');
  AddColunaSL('UF_TRANSP');

  AddColunaSL('VALOR_ICMS_INTER');
end;

function TNotasFiscais.getRecordNotasFiscais: RecNotaFiscal;
begin
  Result.nota_fiscal_id              := getInt('NOTA_FISCAL_ID', True);
  Result.cadastro_id                 := getInt('CADASTRO_ID');
  Result.CfopId                      := getString('CFOP_ID');
  Result.empresa_id                  := getInt('EMPRESA_ID');
  Result.OrcamentoBaseId             := getInt('ORCAMENTO_BASE_ID');
  Result.orcamento_id                := getInt('ORCAMENTO_ID');
  Result.DevolucaoId                 := getInt('DEVOLUCAO_ID');
  Result.AcumuladoId                 := getInt('ACUMULADO_ID');
  Result.RetiradaId                  := getInt('RETIRADA_ID');
  Result.ProducaoId                  := getInt('PRODUCAO_ESTOQUE_ID');
  Result.AjusteEstoqueId             := getInt('AJUSTE_ESTOQUE_ID');
  Result.EntregaId                   := getInt('ENTREGA_ID');
  Result.numero_nota                 := getInt('NUMERO_NOTA');
  Result.ModeloNota                  := getString('MODELO_NOTA');
  Result.SerieNota                   := getString('SERIE_NOTA');
  Result.TipoMovimento               := getString('TIPO_MOVIMENTO');
  Result.TipoMovimentoAnalitico      := getTipoMovimentoAnalitico(Result.TipoMovimento);
  Result.valor_icms                  := getDouble('VALOR_ICMS');
  Result.base_calculo_icms           := getDouble('BASE_CALCULO_ICMS');
  Result.valor_outras_despesas       := getDouble('VALOR_OUTRAS_DESPESAS');
  Result.valor_desconto              := getDouble('VALOR_DESCONTO');
  Result.valor_total_produtos        := getDouble('VALOR_TOTAL_PRODUTOS');
  Result.base_calculo_icms_st        := getDouble('BASE_CALCULO_ICMS_ST');
  Result.valor_icms_st               := getDouble('VALOR_ICMS_ST');
  Result.base_calculo_pis            := getDouble('BASE_CALCULO_PIS');
  Result.valor_pis                   := getDouble('VALOR_PIS');
  Result.valor_total                 := getDouble('VALOR_TOTAL');
  Result.valor_cofins                := getDouble('VALOR_COFINS');
  Result.valor_ipi                   := getDouble('VALOR_IPI');
  Result.valor_seguro                := getDouble('VALOR_SEGURO');
  Result.valor_frete                 := getDouble('VALOR_FRETE');
  Result.valor_recebido_dinheiro     := getDouble('VALOR_RECEBIDO_DINHEIRO');
  Result.valor_recebido_pix          := getDouble('VALOR_RECEBIDO_PIX');
  Result.valor_recebido_cartao_cred  := getDouble('VALOR_RECEBIDO_CARTAO_CRED');
  Result.valor_recebido_cartao_deb   := getDouble('VALOR_RECEBIDO_CARTAO_DEB');
  Result.valor_recebido_cheque       := getDouble('VALOR_RECEBIDO_CHEQUE');
  Result.valor_recebido_cobranca     := getDouble('VALOR_RECEBIDO_COBRANCA');
  Result.valor_recebido_credito      := getDouble('VALOR_RECEBIDO_CREDITO');
  Result.valor_recebido_financeira   := getDouble('VALOR_RECEBIDO_FINANCEIRA');
  Result.base_calculo_cofins         := getDouble('BASE_CALCULO_COFINS');
  Result.chave_nfe                   := getString('CHAVE_NFE');
  Result.data_hora_protocolo_nfe     := getData('DATA_HORA_PROTOCOLO_NFE');
  Result.protocolo_nfe               := getString('PROTOCOLO_NFE');
//  Result.numero_recibo_lote_nfe      := getString('NUMERO_RECIBO_LOTE_NFE');
  Result.status_nfe                  := getString('STATUS_NFE');
  Result.motivo_status_nfe           := getString('MOTIVO_STATUS_NFE');
  Result.status                      := getString('STATUS');
  Result.data_hora_emissao           := getData('DATA_HORA_EMISSAO');
  Result.data_hora_cadastro          := getData('DATA_HORA_CADASTRO');
  Result.danfe_impresso              := getString('DANFE_IMPRESSO');
  Result.NotaFiscalOrigemCupomId     := getInt('NOTA_FISCAL_ORIGEM_CUPOM_ID');
  Result.MovimentarEstOutrasNotas    := getString('MOVIMENTAR_EST_OUTRAS_NOTAS');
  Result.enviou_email_nfe_cliente    := getString('ENVIOU_EMAIL_NFE_CLIENTE');
  Result.natureza_operacao           := getString('NATUREZA_OPERACAO');
  Result.peso_bruto                  := getDouble('PESO_BRUTO');
  Result.peso_liquido                := getDouble('PESO_LIQUIDO');

  Result.codigo_ibge_municipio_emit  := getInt('CODIGO_IBGE_MUNICIPIO_EMIT');
  Result.codigo_ibge_estado_emitent  := getInt('CODIGO_IBGE_ESTADO_EMITENT');
  Result.razao_social_emitente       := getString('RAZAO_SOCIAL_EMITENTE');
  Result.nome_fantasia_emitente      := getString('NOME_FANTASIA_EMITENTE');
  Result.logradouro_emitente         := getString('LOGRADOURO_EMITENTE');
  Result.numero_emitente             := getString('NUMERO_EMITENTE');
  Result.complemento_emitente        := getString('COMPLEMENTO_EMITENTE');
  Result.nome_bairro_emitente        := getString('NOME_BAIRRO_EMITENTE');
  Result.nome_cidade_emitente        := getString('NOME_CIDADE_EMITENTE');
  Result.estado_id_emitente          := getString('ESTADO_ID_EMITENTE');
  Result.cep_emitente                := getString('CEP_EMITENTE');
  Result.telefone_emitente           := getString('TELEFONE_EMITENTE');
  Result.cnpj_emitente               := getString('CNPJ_EMITENTE');
  Result.inscricao_estadual_emitente := getString('INSCRICAO_ESTADUAL_EMITENTE');
  Result.regime_tributario           := getString('REGIME_TRIBUTARIO');

  Result.razao_social_destinatario   := getString('RAZAO_SOCIAL_DESTINATARIO');
  Result.nome_fantasia_destinatario  := getString('NOME_FANTASIA_DESTINATARIO');
  Result.estado_id_destinatario      := getString('ESTADO_ID_DESTINATARIO');
  Result.nome_cidade_destinatario    := getString('NOME_CIDADE_DESTINATARIO');
  Result.nome_bairro_destinatario    := getString('NOME_BAIRRO_DESTINATARIO');
  Result.complemento_destinatario    := getString('COMPLEMENTO_DESTINATARIO');
  Result.logradouro_destinatario     := getString('LOGRADOURO_DESTINATARIO');
  Result.codigo_ibge_municipio_dest  := getInt('CODIGO_IBGE_MUNICIPIO_DEST');
  Result.inscricao_estadual_destinat := getString('INSCRICAO_ESTADUAL_DESTINAT');
  Result.cpf_cnpj_destinatario       := getString('CPF_CNPJ_DESTINATARIO');
  Result.cep_destinatario            := getString('CEP_DESTINATARIO');
  Result.numero_destinatario         := getString('NUMERO_DESTINATARIO');

  Result.InscricaoEstEntrDestinat     := getString('INSCRICAO_EST_ENTR_DESTINAT');
  Result.LogradouroEntrDestinatario  := getString('LOGRADOURO_ENTR_DESTINATARIO');
  Result.complementoEntrDestinatario := getString('COMPLEMENTO_ENTR_DESTINATARIO');
  Result.NomeBairroEntrDestinatario  := getString('NOME_BAIRRO_ENTR_DESTINATARIO');
  Result.NomeCidadeEntrDestinatario  := getString('NOME_CIDADE_ENTR_DESTINATARIO');
  Result.NumeroEntrDestinatario      := getString('NUMERO_ENTR_DESTINATARIO');
  Result.EstadoIdEntrDestinatario    := getString('ESTADO_ID_ENTR_DESTINATARIO');
  Result.CepEntrDestinatario         := getString('CEP_ENTR_DESTINATARIO');
  Result.CodigoIbgeMunicEntrDest     := getInt('CODIGO_IBGE_MUNIC_ENTR_DEST');

  Result.tipo_nota                   := getString('TIPO_NOTA');
  Result.telefone_consumidor_final   := getString('TELEFONE_CONSUMIDOR_FINAL');
  Result.nome_consumidor_final       := getString('NOME_CONSUMIDOR_FINAL');
  Result.tipo_pessoa_destinatario    := getString('TIPO_PESSOA_DESTINATARIO');
  Result.informacoes_complementares  := getString('INFORMACOES_COMPLEMENTARES');
  Result.OutraNotaId                 := getInt('OUTRA_NOTA_ID');
  Result.TransferenciaProdutosId     := getInt('TRANSFERENCIA_PRODUTOS_ID');
  Result.DevolucaoEntradaId          := getInt('DEVOLUCAO_ENTRADA_ID');

  Result.StatusNotaAnalitico         := getString('STATUS_NOTA_ANALITICO');
  Result.TipoNotaAnalitico           := getString('TIPO_NOTA_ANALITICO');
  Result.DataHoraCancelamentoNota    := getData('DATA_HORA_CANCELAMENTO_NOTA');
  Result.MotivoCancelamentoNota      := getString('MOTIVO_CANCELAMENTO_NFE');
  Result.ProtocoloCancelamentoNFe    := getString('PROTOCOLO_CANCELAMENTO_NFE');

  Result.TipoCliente                := getString('TIPO_CLIENTE');
  Result.IndiceFormasPagamento      := getDouble('INDICE_FORMAS_PAGAMENTO');
  Result.TelefoneWhatsapp           := getString('TELEFONE_FAX');
  Result.valor_recebido_acumulado   := getDouble('VALOR_RECEBIDO_ACUMULADO');
  Result.EmailCliente               := getString('E_MAIL');
  Result.TelefoneDestinatario       := getString('TELEFONE_DESTINATARIO');
  Result.EspecieNFe                 := getString('ESPECIE_NFE');
  Result.QuantidadeNFe              := getDouble('QUANTIDADE_NFE');
  Result.tipo_frete                 := getInt('TIPO_FRETE');

  Result.transportadora_id          := getInt('TRANSPORTADORA_ID');
  Result.cnpj_transportadora        := getString('CNPJ_TRANSP');
  Result.razao_transportadora       := getString('RAZAO_TRANSP');
  Result.ie_transportadora          := getString('IE_TRANSP');
  Result.endereco_transportadora    := getString('END_TRANSP');
  Result.cidade_transportadora      := getString('CIDADE_TRANSP');
  Result.uf_transportadora          := getString('UF_TRANSP');

  Result.valor_icms_inter           := getDouble('VALOR_ICMS_INTER');
end;

function BuscarNotasFiscais(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant
): TArray<RecNotaFiscal>;
var
  i: Integer;
  t: TNotasFiscais;
begin
  Result := nil;
  t := TNotasFiscais.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordNotasFiscais;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarNotasFiscaisComando(pConexao: TConexao; pComando: string): TArray<RecNotaFiscal>;
var
  i: Integer;
  t: TNotasFiscais;
begin
  Result := nil;
  t := TNotasFiscais.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordNotasFiscais;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function AtualizarVariasNotasFiscais(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pRazaoSocial: string;
  pNomeFantasia: string;
  pCnpj: string;
  pInscricaoEstadual: string;
  pLogradouro: string;
  pComplemento: string;
  pNomeBairro: string;
  pNomeCidade: string;
  pNumero: string;
  pEstadoId: string;
  pCep: string;
  pTelefone: string;
  pCodigoIbgeMunicipio: Integer;
  pCodigoIbgeEstado: Integer;
  pNotasFiscaisIds: TArray<Integer>
): RecRetornoBD;
var
  i: Integer;
  vExec: TExecucao;
begin
  try
    pConexao.IniciarTransacao;

    vExec := TExecucao.Create(pConexao);

    vExec.SQL.Add('UPDATE NOTAS_FISCAIS SET ');
    vExec.SQL.Add('  EMPRESA_ID = :P1, ');
    vExec.SQL.Add('  RAZAO_SOCIAL_EMITENTE = :P2, ');
    vExec.SQL.Add('  NOME_FANTASIA_EMITENTE = :P3, ');
    vExec.SQL.Add('  CNPJ_EMITENTE = :P4, ');
    vExec.SQL.Add('  INSCRICAO_ESTADUAL_EMITENTE = :P5, ');
    vExec.SQL.Add('  LOGRADOURO_EMITENTE = :P6, ');
    vExec.SQL.Add('  COMPLEMENTO_EMITENTE = :P7, ');
    vExec.SQL.Add('  NOME_BAIRRO_EMITENTE = :P8, ');
    vExec.SQL.Add('  NOME_CIDADE_EMITENTE = :P9, ');
    vExec.SQL.Add('  NUMERO_EMITENTE = :P10, ');
    vExec.SQL.Add('  ESTADO_ID_EMITENTE = :P11, ');
    vExec.SQL.Add('  CEP_EMITENTE = :P12, ');
    vExec.SQL.Add('  TELEFONE_EMITENTE = :P13, ');
    vExec.SQL.Add('  CODIGO_IBGE_MUNICIPIO_EMIT = :P14, ');
    vExec.SQL.Add('  CODIGO_IBGE_ESTADO_EMITENT = :P15 ');
    vExec.SQL.Add('WHERE NOTA_FISCAL_ID = :P16 ');

    for I := Low(pNotasFiscaisIds) to High(pNotasFiscaisIds) do begin

      vExec.Executar([
        pEmpresaId,
        pRazaoSocial,
        pNomeFantasia,
        pCnpj,
        pInscricaoEstadual,
        pLogradouro,
        pComplemento,
        pNomeBairro,
        pNomeCidade,
        pNumero,
        pEstadoId,
        pCep,
        pTelefone,
        pCodigoIbgeMunicipio,
        pCodigoIbgeEstado,
        pNotasFiscaisIds[i]
      ]);
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
end;

function AtualizarDestinatarioNotasFiscais(
  pConexao: TConexao;
  pNotasFiscaisIds: TArray<Integer>;
  pNomeFantasiaDestinatario: string;
  pRazaoSocialDestinatario: string;
  pTipoPessoaDestinatario: string;
  pCPFCNPJDestinatario: string;
  pInscricaoEstadualDestinat: string;
  pLogradouroDestinatario: string;
  pComplementoDestinatario: string;
  pNomeBairroDestinatario: string;
  pNomeCidadeDestinatario: string;
  pNumeroDestinatario: string;
  pEstadoIdDestinatario: string;
  pCepDestinatario: string;
  pCodigoIBGEMunicipioDest: Integer;
  pInscricaoEstEntrDestinat: string;
  pLogradouroEntrDestinatario: string;
  pcomplementoEntrDestinatario: string;
  pNomeBairroEntrDestinatario: string;
  pNomeCidadeEntrDestinatario: string;
  pNumeroEntrDestinatario: string;
  pEstadoIdEntrDestinatario: string;
  pCepEntrDestinatario: string;
  pCodigoIbgeMunicEntrDest: Integer
): RecRetornoBD;
var
  i: Integer;
  vExec: TExecucao;
begin
  Result.Iniciar;
  try
    pConexao.IniciarTransacao;

    vExec := TExecucao.Create(pConexao);

    vExec.SQL.Add('UPDATE NOTAS_FISCAIS SET ');
    vExec.SQL.Add('  NOME_FANTASIA_DESTINATARIO = :P1, ');
    vExec.SQL.Add('  RAZAO_SOCIAL_DESTINATARIO = :P2, ');
    vExec.SQL.Add('  TIPO_PESSOA_DESTINATARIO = :P3, ');
    vExec.SQL.Add('  CPF_CNPJ_DESTINATARIO = :P4, ');
    vExec.SQL.Add('  INSCRICAO_ESTADUAL_DESTINAT = :P5, ');
    vExec.SQL.Add('  LOGRADOURO_DESTINATARIO = :P6, ');
    vExec.SQL.Add('  COMPLEMENTO_DESTINATARIO = :P7, ');
    vExec.SQL.Add('  NOME_BAIRRO_DESTINATARIO = :P8, ');
    vExec.SQL.Add('  NOME_CIDADE_DESTINATARIO = :P9, ');
    vExec.SQL.Add('  NUMERO_DESTINATARIO = :P10, ');
    vExec.SQL.Add('  ESTADO_ID_DESTINATARIO = :P11, ');
    vExec.SQL.Add('  CEP_DESTINATARIO = :P12, ');
    vExec.SQL.Add('  CODIGO_IBGE_MUNICIPIO_DEST = :P13, ');
    vExec.SQL.Add('  INSCRICAO_EST_ENTR_DESTINAT = :P14, ');
    vExec.SQL.Add('  LOGRADOURO_ENTR_DESTINATARIO = :P15, ');
    vExec.SQL.Add('  COMPLEMENTO_ENTR_DESTINATARIO = :P16, ');
    vExec.SQL.Add('  NOME_BAIRRO_ENTR_DESTINATARIO = :P17, ');
    vExec.SQL.Add('  NOME_CIDADE_ENTR_DESTINATARIO = :P18, ');
    vExec.SQL.Add('  NUMERO_ENTR_DESTINATARIO = :P19, ');
    vExec.SQL.Add('  ESTADO_ID_ENTR_DESTINATARIO = :P20, ');
    vExec.SQL.Add('  CEP_ENTR_DESTINATARIO = :P21, ');
    vExec.SQL.Add('  CODIGO_IBGE_MUNIC_ENTR_DEST = :P22 ');
    vExec.SQL.Add('WHERE NOTA_FISCAL_ID = :P23 ');

    for I := Low(pNotasFiscaisIds) to High(pNotasFiscaisIds) do begin

      vExec.Executar([
        pNomeFantasiaDestinatario,
        pRazaoSocialDestinatario,
        pTipoPessoaDestinatario,
        pCPFCNPJDestinatario,
        pInscricaoEstadualDestinat,
        pLogradouroDestinatario,
        pComplementoDestinatario,
        pNomeBairroDestinatario,
        pNomeCidadeDestinatario,
        pNumeroDestinatario,
        pEstadoIdDestinatario,
        pCepDestinatario,
        pCodigoIBGEMunicipioDest,
        pInscricaoEstEntrDestinat,
        pLogradouroEntrDestinatario,
        pcomplementoEntrDestinatario,
        pNomeBairroEntrDestinatario,
        pNomeCidadeEntrDestinatario,
        pNumeroEntrDestinatario,
        pEstadoIdEntrDestinatario,
        pCepEntrDestinatario,
        pCodigoIbgeMunicEntrDest,
        pNotasFiscaisIds[i]
      ]);
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
end;

function AtualizarNotasFiscais(
  pConexao: TConexao;
  pNotaFiscalId: Integer;
  pOutraNotaId: Integer;
  pCadastroId: Integer;
  pCFOPId: string;
  pEmpresaId: Integer;
  pOrcamentoBaseId: Integer;
  pOrcamentoId: Integer;
  pDevolucaoId: Integer;
  pAcumuladoId: Integer;
  pRetiradaId: Integer;
  pTipoMovimento: string;
  pNumeroNota: Integer;
  pModeloNota: string;
  pSerieNota: string;
  // Dados do emitente da nota
  pRazaoSocialEmitente: string;
  pNomeFantasiaEmitente: string;
  pRegimeTributario: string;
  pCNPJEmitente: string;
  pInscricaoEstadualEmitente: string;
  pLogradouroEmitente: string;
  pComplementoEmitente: string;
  pNomeBairroEmitente: string;
  pNomeCidadeEmitente: string;
  pNumeroEmitente: string;
  pEstadoIdEmitente: string;
  pCepEmitente: string;
  pTelefoneEmitente: string;
  pCodigoIBGEMunicipioEmit: Integer;
  pCodigoIBGEEstadoEmitent: Integer;
  // Dados do destinatario
  pNomeFantasiaDestinatario: string;
  pRazaoSocialDestinatario: string;
  pTipoPessoaDestinatario: string;
  pCPFCNPJDestinatario: string;
  pInscricaoEstadualDestinat: string;
  pLogradouroDestinatario: string;
  pComplementoDestinatario: string;
  pNomeBairroDestinatario: string;
  pNomeCidadeDestinatario: string;
  pNumeroDestinatario: string;
  pEstadoIdDestinatario: string;
  pCepDestinatario: string;
  pCodigoIBGEMunicipioDest: Integer;
  pInscricaoEstEntrDestinat: string;
  pLogradouroEntrDestinatario: string;
  pcomplementoEntrDestinatario: string;
  pNomeBairroEntrDestinatario: string;
  pNomeCidadeEntrDestinatario: string;
  pNumeroEntrDestinatario: string;
  pEstadoIdEntrDestinatario: string;
  pCepEntrDestinatario: string;
  pCodigoIbgeMunicEntrDest: Integer;
  pNomeConsumidorFinal: string;
  pTelefoneConsumidorFinal: string;
  pTipoNota: string;
  pNaturezaOperacao: string;
  // Valores da nota
  pValorTotal: Double;
  pValorTotalProdutos: Double;
  pValorDesconto: Double;
  pValorOutrasDespesas: Double;
  pValorFrete: Double;
  pValorSeguro: Double;
  pBaseCalculoICMS: Double;
  pValorICMS: Double;
  pBaseCalculoICMSST: Double;
  pValorICMSST: Double;
  pBaseCalculoPIS: Double;
  pValorPis: Double;
  pBaseCalculoCofins: Double;
  pValorCofins: Double;
  pValorIpi: Double;
  pPesoLiquido: Double;
  pPesoBruto: Double;
  pValorRecebidoDinheiro: Double;
  pValorRecebidoCartaoCred: Double;
  pValorRecebidoCartaoDeb: Double;
  pValorRecebidoCredito: Double;
  pValorRecebidoFinanceira: Double;
  pValorRecebidoCobranca: Double;
  pValorRecebidoCheque: Double;
  pDataHoraEmissao: TDateTime;
  pStatus: string;
  pNumeroReciboLoteNFE: string;
  pProtocoloNFE: string;
  pDataHoraProtocoloNFE: TDateTime;
  pChaveNFE: string;
  pStatusNFE: string;
  pMotivoStatusNFE: string;
  pProtocoloCancelamentoNFE: string;
  pMotivoCancelamentoNFE: string;
  pEnviouEmailNFECliente: string;
  pDanfeImpresso: string;
  pNotaFiscalOrigemCupomId: Integer;
  pMovimentarEstoqueOutrasNotas: string;
  pInformacoesComplementares: string;
  pNotaItens: TArray<RecNotaFiscalItem>;
  pNotasReferenciadas: TArray<RecNotasFiscaisReferencias>;
  pValorRecebidoPix: Double;
  pTransportadoraId: Integer;
  pQuantidadeNFe: Double = 0;
  pEspecieNFe: string = '';
  pTipoFrete: Integer = 9
): RecRetornoBD;
var
  vSeq: TSequencia;
  vNota: TNotasFiscais;
  vItens: TNotasFiscaisItens;
  vReferencias: TNotasFiscaisReferencias;

  vSeqOutra: TSequencia;

  i: Integer;
  vNovo: Boolean;
  vExecucao: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_NOTA_FISCAL');

  vExecucao := TExecucao.Create(pConexao);
  vNota := TNotasFiscais.Create(pConexao);
  vItens := TNotasFiscaisItens.Create(pConexao);
  vReferencias := TNotasFiscaisReferencias.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    vNovo := (pNotaFiscalId = 0);
    if vNovo then begin
      vSeq := TSequencia.Create(pConexao, 'SEQ_NOTA_FISCAL_ID');
      pNotaFiscalId := vSeq.GetProximaSequencia;
      Result.AsInt := pNotaFiscalId;
      vSeq.Free;
    end;

    if (pTipoMovimento = 'OUT') and (pOutraNotaId = 0) then begin
      vSeqOutra := TSequencia.Create(pConexao, 'SEQ_OUTRAS_NOTAS');
      pOutraNotaId := vSeqOutra.GetProximaSequencia;
      vSeqOutra.Free;
    end;

    vNota.setInt('NOTA_FISCAL_ID', pNotaFiscalId, True);
    vNota.setInt('CADASTRO_ID', pCadastroId);
    vNota.setString('CFOP_ID', pCFOPId);
    vNota.setInt('EMPRESA_ID', pEmpresaId);
    vNota.setIntN('ORCAMENTO_BASE_ID', pOrcamentoBaseId);
    vNota.setIntN('ORCAMENTO_ID', pOrcamentoId);
    vNota.setIntN('DEVOLUCAO_ID', pDevolucaoId);
    vNota.setIntN('ACUMULADO_ID', pAcumuladoId);
    vNota.setIntN('OUTRA_NOTA_ID', pOutraNotaId);
    vNota.setIntN('RETIRADA_ID', pRetiradaId);
    vNota.setString('TIPO_MOVIMENTO', pTipoMovimento);

    vNota.setIntN('NUMERO_NOTA', pNumeroNota);
    vNota.SetString('MODELO_NOTA', pModeloNota);
    vNota.SetString('SERIE_NOTA', pSerieNota);

    vNota.setString('RAZAO_SOCIAL_EMITENTE', pRazaoSocialEmitente);
    vNota.setString('NOME_FANTASIA_EMITENTE', pNomeFantasiaEmitente);
    vNota.setString('REGIME_TRIBUTARIO', pRegimeTributario);
    vNota.setString('CNPJ_EMITENTE', pCNPJEmitente);
    vNota.setString('INSCRICAO_ESTADUAL_EMITENTE', pInscricaoEstadualEmitente);
    vNota.setString('LOGRADOURO_EMITENTE', pLogradouroEmitente);
    vNota.setString('COMPLEMENTO_EMITENTE', pComplementoEmitente);
    vNota.setString('NOME_BAIRRO_EMITENTE', pNomeBairroEmitente);
    vNota.setString('NOME_CIDADE_EMITENTE', pNomeCidadeEmitente);
    vNota.setString('NUMERO_EMITENTE', pNumeroEmitente);
    vNota.setString('ESTADO_ID_EMITENTE', pEstadoIdEmitente);
    vNota.setString('CEP_EMITENTE', pCepEmitente);
    vNota.setString('TELEFONE_EMITENTE', pTelefoneEmitente);
    vNota.setInt('CODIGO_IBGE_MUNICIPIO_EMIT', pCodigoIBGEMunicipioEmit);
    vNota.setInt('CODIGO_IBGE_ESTADO_EMITENT', pCodigoIBGEEstadoEmitent);
    // Dados do destinatario
    vNota.setString('NOME_FANTASIA_DESTINATARIO', pNomeFantasiaDestinatario);
    vNota.setString('RAZAO_SOCIAL_DESTINATARIO', pRazaoSocialDestinatario);
    vNota.setString('TIPO_PESSOA_DESTINATARIO', pTipoPessoaDestinatario);
    vNota.setString('CPF_CNPJ_DESTINATARIO', pCPFCNPJDestinatario);
    vNota.setString('INSCRICAO_ESTADUAL_DESTINAT', pInscricaoEstadualDestinat);
    vNota.setString('LOGRADOURO_DESTINATARIO', pLogradouroDestinatario);
    vNota.setString('COMPLEMENTO_DESTINATARIO', pComplementoDestinatario);
    vNota.setString('NOME_BAIRRO_DESTINATARIO', pNomeBairroDestinatario);
    vNota.setString('NOME_CIDADE_DESTINATARIO', pNomeCidadeDestinatario);
    vNota.setString('NUMERO_DESTINATARIO', pNumeroDestinatario);
    vNota.setString('ESTADO_ID_DESTINATARIO', pEstadoIdDestinatario);
    vNota.setString('CEP_DESTINATARIO', pCepDestinatario);
    vNota.setInt('CODIGO_IBGE_MUNICIPIO_DEST', pCodigoIBGEMunicipioDest);

    vNota.setString('INSCRICAO_EST_ENTR_DESTINAT', pInscricaoEstEntrDestinat);
    vNota.setString('LOGRADOURO_ENTR_DESTINATARIO', pLogradouroEntrDestinatario);
    vNota.setString('COMPLEMENTO_ENTR_DESTINATARIO', pcomplementoEntrDestinatario);
    vNota.setString('NOME_BAIRRO_ENTR_DESTINATARIO', pNomeBairroEntrDestinatario);
    vNota.setString('NOME_CIDADE_ENTR_DESTINATARIO', pNomeCidadeEntrDestinatario);
    vNota.setString('NUMERO_ENTR_DESTINATARIO', pNumeroEntrDestinatario);
    vNota.setString('ESTADO_ID_ENTR_DESTINATARIO', pEstadoIdEntrDestinatario);
    vNota.setString('CEP_ENTR_DESTINATARIO', pCepEntrDestinatario);
    vNota.setIntN('CODIGO_IBGE_MUNIC_ENTR_DEST', pCodigoIbgeMunicEntrDest);

    vNota.setString('NOME_CONSUMIDOR_FINAL', pNomeConsumidorFinal);
    vNota.setString('TELEFONE_CONSUMIDOR_FINAL', pTelefoneConsumidorFinal);
    vNota.setString('TIPO_NOTA', pTipoNota);
    vNota.setString('NATUREZA_OPERACAO', pNaturezaOperacao);
    // Valores da nota
    vNota.setDouble('VALOR_TOTAL', pValorTotal);
    vNota.setDouble('VALOR_TOTAL_PRODUTOS', pValorTotalProdutos);
    vNota.setDouble('VALOR_DESCONTO', pValorDesconto);
    vNota.setDouble('VALOR_OUTRAS_DESPESAS', pValorOutrasDespesas);
    vNota.setDouble('VALOR_FRETE', pValorFrete);
    vNota.setDouble('VALOR_SEGURO', pValorSeguro);
    vNota.setDouble('BASE_CALCULO_ICMS', pBaseCalculoICMS);
    vNota.setDouble('VALOR_ICMS', pValorICMS);
    vNota.setDouble('BASE_CALCULO_ICMS_ST', pBaseCalculoICMSST);
    vNota.setDouble('VALOR_ICMS_ST', pValorICMSST);
    vNota.setDouble('BASE_CALCULO_PIS', pBaseCalculoPIS);
    vNota.setDouble('VALOR_PIS', pValorPis);
    vNota.setDouble('BASE_CALCULO_COFINS', pBaseCalculoCofins);
    vNota.setDouble('VALOR_COFINS', pValorCofins);
    vNota.setDouble('VALOR_IPI', pValorIpi);
    vNota.setDouble('PESO_LIQUIDO', pPesoLiquido);
    vNota.setDouble('PESO_BRUTO', pPesoBruto);

    vNota.setDouble('VALOR_RECEBIDO_DINHEIRO', pValorRecebidoDinheiro);
    vNota.setDouble('VALOR_RECEBIDO_PIX', pValorRecebidoPix);
    vNota.setDouble('VALOR_RECEBIDO_CARTAO_CRED', pValorRecebidoCartaoCred);
    vNota.setDouble('VALOR_RECEBIDO_CARTAO_DEB', pValorRecebidoCartaoDeb);
    vNota.setDouble('VALOR_RECEBIDO_CREDITO', pValorRecebidoCredito);
    vNota.setDouble('VALOR_RECEBIDO_FINANCEIRA', pValorRecebidoFinanceira);
    vNota.setDouble('VALOR_RECEBIDO_COBRANCA', pValorRecebidoCobranca);
    vNota.setDouble('VALOR_RECEBIDO_CHEQUE', pValorRecebidoCheque);

    vNota.setDataN('DATA_HORA_EMISSAO', pDataHoraEmissao);
    vNota.setString('STATUS', pStatus);

    vNota.setStringN('NUMERO_RECIBO_LOTE_NFE', pNumeroReciboLoteNFE);
    vNota.setStringN('PROTOCOLO_NFE', pProtocoloNFE);
    vNota.setDataN('DATA_HORA_PROTOCOLO_NFE', pDataHoraProtocoloNFE);
    vNota.setStringN('CHAVE_NFE', pChaveNFE);
    vNota.setStringN('STATUS_NFE', pStatusNFE);
    vNota.setStringN('MOTIVO_STATUS_NFE', pMotivoStatusNFE);
    vNota.setStringN('PROTOCOLO_CANCELAMENTO_NFE', pProtocoloCancelamentoNFE);
    vNota.setStringN('MOTIVO_CANCELAMENTO_NFE', pMotivoCancelamentoNFE);
    vNota.setString('ENVIOU_EMAIL_NFE_CLIENTE', pEnviouEmailNFECliente);
    vNota.setString('DANFE_IMPRESSO', pDanfeImpresso);
    vNota.SetIntN('NOTA_FISCAL_ORIGEM_CUPOM_ID', pNotaFiscalOrigemCupomId);
    vNota.setString('MOVIMENTAR_EST_OUTRAS_NOTAS', pMovimentarEstoqueOutrasNotas);
    vNota.SetStringN('INFORMACOES_COMPLEMENTARES', pInformacoesComplementares);

    vNota.setString('ESPECIE_NFE', pEspecieNFe);
    vNota.setDouble('QUANTIDADE_NFE', pQuantidadeNFe);
    vNota.setDouble('PESO_BRUTO', pPesoBruto);
    vNota.setDouble('PESO_LIQUIDO', pPesoLiquido);
    vNota.setInt('TIPO_FRETE', pTipoFrete);
    vNota.setIntN('TRANSPORTADORA_ID', pTransportadoraId);

    if vNovo then
      vNota.Inserir
    else
      vNota.Atualizar;

    if pNotaItens <> nil then begin
      if not vNovo then begin
        vExecucao.Limpar;
        vExecucao.Add('delete from NOTAS_FISCAIS_ITENS ');
        vExecucao.Add('where NOTA_FISCAL_ID = :P1');
        vExecucao.Executar([pNotaFiscalID]);
      end;

      for i := Low(pNotaItens) to High(pNotaItens) do begin
        vItens.setInt('NOTA_FISCAL_ID', pNotaFiscalId, True);
        vItens.setInt('PRODUTO_ID', pNotaItens[i].produto_id, True);
        vItens.setInt('ITEM_ID', pNotaItens[i].item_id, True);
        vItens.setString('NOME_PRODUTO', pNotaItens[i].nome_produto);
        vItens.setString('UNIDADE', pNotaItens[i].unidade);
        vItens.setString('CFOP_ID', pNotaItens[i].CfopId);
        vItens.setString('CST', pNotaItens[i].cst);
        vItens.setString('CODIGO_NCM', pNotaItens[i].codigo_ncm);
        vItens.setDouble('VALOR_TOTAL', pNotaItens[i].valor_total);
        vItens.setDouble('PRECO_UNITARIO', pNotaItens[i].preco_unitario);
        vItens.setDouble('QUANTIDADE', pNotaItens[i].quantidade);
        vItens.setDouble('VALOR_TOTAL_DESCONTO', pNotaItens[i].valor_total_desconto);
        vItens.setDouble('VALOR_TOTAL_OUTRAS_DESPESAS', pNotaItens[i].valor_total_outras_despesas);
        vItens.setString('CODIGO_BARRAS', pNotaItens[i].codigo_barras);
        vItens.setInt('ORIGEM_PRODUTO', pNotaItens[i].origem_produto);

        vItens.setDouble('INDICE_REDUCAO_BASE_ICMS', pNotaItens[i].indice_reducao_base_icms);
        vItens.setDouble('BASE_CALCULO_ICMS', pNotaItens[i].base_calculo_icms);
        vItens.setDouble('PERCENTUAL_ICMS', pNotaItens[i].percentual_icms);
        vItens.setDouble('VALOR_ICMS', pNotaItens[i].valor_icms);

        vItens.setDouble('INDICE_REDUCAO_BASE_ICMS_ST', pNotaItens[i].indice_reducao_base_icms_st);
        vItens.setDouble('BASE_CALCULO_ICMS_ST', pNotaItens[i].base_calculo_icms_st);
        vItens.setDouble('PERCENTUAL_ICMS_ST', pNotaItens[i].percentual_icms_st);
        vItens.setDouble('VALOR_ICMS_ST', pNotaItens[i].valor_icms_st);

        vItens.setString('CST_PIS', pNotaItens[i].cst_pis);
        vItens.setDouble('BASE_CALCULO_PIS', pNotaItens[i].base_calculo_pis);
        vItens.setDouble('PERCENTUAL_PIS', pNotaItens[i].percentual_pis);
        vItens.setDouble('VALOR_PIS', pNotaItens[i].valor_pis);

        vItens.setString('CST_COFINS', pNotaItens[i].cst_cofins);
        vItens.setDouble('BASE_CALCULO_COFINS', pNotaItens[i].base_calculo_cofins);
        vItens.setDouble('PERCENTUAL_COFINS', pNotaItens[i].percentual_cofins);
        vItens.setDouble('VALOR_COFINS', pNotaItens[i].valor_cofins);

        vItens.setDouble('VALOR_IPI', pNotaItens[i].valor_ipi);
        vItens.setDouble('PERCENTUAL_IPI', pNotaItens[i].percentual_ipi);

        vItens.setDouble('IVA', pNotaItens[i].iva);
        vItens.setDouble('PRECO_PAUTA', pNotaItens[i].preco_pauta);

        vItens.setString('INFORMACOES_ADICIONAIS', pNotaItens[i].informacoes_adicionais);
        vItens.setStringN('CODIGO_PRODUTO_NFE', pNotaItens[i].CodigoProdutoNFe);

        vItens.setIntN('NUMERO_ITEM_PEDIDO', pNotaItens[i].numero_item_pedido);
        vItens.setStringN('NUMERO_PEDIDO', pNotaItens[i].numero_pedido);

        vItens.Inserir;
      end;
    end;

    if pNotasReferenciadas <> nil then begin
      if not vNovo then begin
        vExecucao.Limpar;
        vExecucao.Add('delete from NOTAS_FISCAIS_REFERENCIAS ');
        vExecucao.Add('where NOTA_FISCAL_ID = :P1');
        vExecucao.Executar([pNotaFiscalID]);
      end;

      for i := Low(pNotasReferenciadas) to High(pNotasReferenciadas) do begin
        vReferencias.setInt('NOTA_FISCAL_ID', pNotaFiscalId, True);
        vReferencias.setInt('ITEM_ID', i, True);
        vReferencias.setStringN('TIPO_REFERENCIA', pNotasReferenciadas[i].TipoReferencia);
        vReferencias.setStringN('TIPO_NOTA', pNotasReferenciadas[i].TipoNota);
        vReferencias.setIntN('NOTA_FISCAL_ID_REFERENCIADA', pNotasReferenciadas[i].NotaFiscalIdReferenciada);
        vReferencias.setStringN('CHAVE_NFE_REFERENCIADA', pNotasReferenciadas[i].ChaveNfeReferenciada);

        vReferencias.Inserir;
      end;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  vNota.Free;
  vItens.Free;
  vExecucao.Free;
  vReferencias.Free;
end;

function ExcluirNotasFiscais(
  pConexao: TConexao;
  pNotaFiscalId: Integer
): RecRetornoBD;
var
  t: TNotasFiscais;
begin
  Result.TeveErro := False;
  t := TNotasFiscais.Create(pConexao);
  try

    t.Excluir;

  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function SetarCupomImpresso(
  pConexao: TConexao;
  pNotaFiscalId: Integer;
  pNumeroNota: Integer
): RecRetornoBD;
var
  procedimento: TProcedimentoBanco;
begin
  Result.TeveErro := False;
  procedimento := TProcedimentoBanco.Create(pConexao, 'SETAR_CUPOM_IMPRESSO');

  try
    procedimento.Params[0].AsInteger := pNotaFiscalId;
    procedimento.Params[1].AsInteger := pNumeroNota;

    procedimento.Executar;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  FreeAndNil(procedimento);
end;

function SetarNFeEmitida(
  pConexao: TConexao;
  pNotaFiscalId: Integer;
  pProtocoloNF: string;
  pDataHoraProtocoloNFe: TDateTime;
  pChaveNFe: string;
  pDataHoraEmissao: TDateTime
): RecRetornoBD;
var
  procedimento: TProcedimentoBanco;
begin
  Result.TeveErro := False;
  procedimento := TProcedimentoBanco.Create(pConexao, 'SETAR_NFE_EMITIDA');

  try
    pConexao.IniciarTransacao;

    procedimento.Params[0].AsInteger  := pNotaFiscalId;
    procedimento.Params[1].AsString   := pChaveNFe;
    procedimento.Params[2].AsString   := pProtocoloNF;
    procedimento.Params[3].AsDateTime := pDataHoraEmissao;
    procedimento.Params[4].AsDateTime := pDataHoraProtocoloNFe;

    procedimento.Executar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  FreeAndNil(procedimento);
end;

function AtualizarNumeroReciboLoteNFE(
  pConexao: TConexao;
  pNotaFiscalId: Integer;
  pNumeroNota: Integer;
  pNumeroRecibo: string;
  pStatusLoteNFE: string;
  pMotivo: string;
  pChaveNFE: string
): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.TeveErro := False;
  vExec := TExecucao.Create(pConexao);
  try
    pConexao.IniciarTransacao;
    
    vExec.SQL.Add('update NOTAS_FISCAIS set');
    vExec.SQL.Add('  NUMERO_RECIBO_LOTE_NFE = :P2, ');
    vExec.SQL.Add('  STATUS_NFE = :P3, ');
    vExec.SQL.Add('  MOTIVO_STATUS_NFE = :P4, ');
    vExec.SQL.Add('  CHAVE_NFE = :P5, ');
    vExec.SQL.Add('  NUMERO_NOTA = :P6 ');
    vExec.SQL.Add('where NOTA_FISCAL_ID = :P1');

    vExec.Executar([pNotaFiscalId, pNumeroRecibo, pStatusLoteNFE, pMotivo, pChaveNFE, pNumeroNota]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;    
  end;
  vExec.Free;
end;

function AtualizarTiposPagamentoNota(
  pConexao: TConexao;
  pTiposPagamento: RecValoresNotaFiscal;
  pNotaFiscalId: Integer
): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.TeveErro := False;
  vExec := TExecucao.Create(pConexao);
  try
    pConexao.IniciarTransacao;

    vExec.SQL.Add('update NOTAS_FISCAIS set ');
    vExec.SQL.Add('  VALOR_RECEBIDO_ACUMULADO = :P1, ');
    vExec.SQL.Add('  VALOR_RECEBIDO_CARTAO_CRED = :P2, ');
    vExec.SQL.Add('  VALOR_RECEBIDO_CARTAO_DEB = :P3, ');
    vExec.SQL.Add('  VALOR_RECEBIDO_CHEQUE = :P4, ');
    vExec.SQL.Add('  VALOR_RECEBIDO_COBRANCA = :P5, ');
    vExec.SQL.Add('  VALOR_RECEBIDO_CREDITO = :P6, ');
    vExec.SQL.Add('  VALOR_RECEBIDO_DINHEIRO = :P7, ');
    vExec.SQL.Add('  VALOR_RECEBIDO_FINANCEIRA = :P8, ');
    vExec.SQL.Add('  VALOR_RECEBIDO_PIX = :P9 ');
    vExec.SQL.Add('where NOTA_FISCAL_ID = :P10 ');

    vExec.Executar([
      pTiposPagamento.valorAcumulado,
      pTiposPagamento.valorCartaoCredito,
      pTiposPagamento.valorCartaoDebito,
      pTiposPagamento.valorCheque,
      pTiposPagamento.valorCobranca,
      pTiposPagamento.valorCredito,
      pTiposPagamento.valorDinheiro,
      0,
      pTiposPagamento.valorPix,
      pNotaFiscalId
    ]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vExec.Free;
end;

function AtualizarStatusLoteNFE(
  pConexao: TConexao;
  pNotaFiscalId: Integer;
  pStatusLoteNFE: string;
  pMotivo: string
): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.TeveErro := False;
  vExec := TExecucao.Create(pConexao);
  try
    pConexao.IniciarTransacao;

    vExec.SQL.Add('update NOTAS_FISCAIS set');
    vExec.SQL.Add('  STATUS_NFE = :P2, ');
    vExec.SQL.Add('  MOTIVO_STATUS_NFE = :P3 ');
    vExec.SQL.Add('where NOTA_FISCAL_ID = :P1');

    vExec.Executar([pNotaFiscalId, pStatusLoteNFE, pMotivo]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vExec.Free;
end;

function SalvarXMLNFe(pConexao: TConexao; pNotaFiscalId: Integer; pCaminhoXML: string; pTextoXML: string): RecRetornoBD;
var
  vExecucao: TExecucao;
  vArquivo: TMemoryStream;
begin
  Result.TeveErro := False;
  vExecucao := TExecucao.Create(pConexao);
  try
    pConexao.IniciarTransacao;

    vExecucao.SQL.Add('delete from NOTAS_FISCAIS_XML where NOTA_FISCAL_ID = :P1');
    vExecucao.Executar([pNotaFiscalId]);

    vExecucao.SQL.Clear;
    vExecucao.SQL.Add('insert into NOTAS_FISCAIS_XML(NOTA_FISCAL_ID)values(:P1)');
    vExecucao.Executar([pNotaFiscalId]);

    vArquivo := TMemoryStream.Create;
    vArquivo.LoadFromFile(pCaminhoXML);

    vExecucao.SQL.Clear;
    vExecucao.SQL.Add('update NOTAS_FISCAIS_XML set ');
    vExecucao.SQL.Add('  XML = :P2, ');
    vExecucao.SQL.Add('  XML_TEXTO = :P3 ');
    vExecucao.SQL.Add('where NOTA_FISCAL_ID = :P1');

    vExecucao.ParamByName('P1').AsInteger := pNotaFiscalId;
    vExecucao.CarregarBinario('P2', vArquivo);
    vExecucao.CarregarClob('P3', TStringStream.Create(pTextoXML) );

    vExecucao.Executar([pNotaFiscalId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vExecucao.Free;
end;

function SetarNFeCancelada(
  pConexao: TConexao;
  const pNotaFiscalId: Integer;
  const pProtocoloCancelamento: string;
  const pMotivo: string;
  const pDataHoraProtocolo: TDateTime
): RecRetornoBD;
var
  vProcedimento: TProcedimentoBanco;
begin
  Result.TeveErro := False;
  vProcedimento := TProcedimentoBanco.Create(pConexao, 'SETAR_NFE_CANCELADA');

  try
    pConexao.IniciarTransacao;

    vProcedimento.Params[0].AsInteger  := pNotaFiscalId;
    vProcedimento.Params[1].AsString   := pProtocoloCancelamento;
    vProcedimento.Params[2].AsString   := pMotivo;
    vProcedimento.Params[3].AsDateTime := pDataHoraProtocolo;

    vProcedimento.Executar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  FreeAndNil(vProcedimento);
end;

function BuscarNotaFiscalIdOrcamento(pConexao: TConexao; const pOrcamentoId: Integer): Integer;
var
  vSql: TConsulta;
begin
  Result := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  NFI.NOTA_FISCAL_ID ');
  vSql.SQL.Add('from ');
  vSql.SQL.Add('  NOTAS_FISCAIS NFI ');
  vSql.SQL.Add('where NFI.ORCAMENTO_ID = :P1 ');

  if vSql.Pesquisar([pOrcamentoId]) then
    Result := vSql.GetInt(0);

  vSql.Active := False;
  vSql.Free;
end;

function BuscarNumeroNota(pConexao: TConexao; const pNotaFiscalId: Integer): Integer;
var
  vSql: TConsulta;
begin
  Result := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select NUMERO_NOTA ');
  vSql.SQL.Add('from NOTAS_FISCAIS ');
  vSql.SQL.Add('where NOTA_FISCAL_ID = :P1 ');

  if vSql.Pesquisar([pNotaFiscalId]) then
    Result := vSql.GetInt(0);

  vSql.Active := False;
  vSql.Free;
end;

function SetarDanfeImpresso(pConexao: TConexao; pNotaFiscalId: Integer): RecRetornoBD;
var
  vExecucao: TExecucao;
begin
  Result.TeveErro := False;
  vExecucao := TExecucao.Create(pConexao);
  try
    pConexao.IniciarTransacao;

    vExecucao.SQL.Add('update NOTAS_FISCAIS set DANFE_IMPRESSO = ''S'' where NOTA_FISCAL_ID = :P1');
    vExecucao.Executar([pNotaFiscalId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := False;
      Result.MensagemErro := e.Message;
    end;
  end;
end;

function GerarNFeDeNFCe(pConexao: TConexao; const pNotaFiscalId: Integer): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('GERAR_NFE_DE_NFCE');

  vProc := TProcedimentoBanco.Create(pConexao, 'GERAR_NFE_DE_NFCE');

  try
    pConexao.IniciarTransacao;

    vProc.Params[0].AsInteger := pNotaFiscalId;
    vProc.Executar;

    Result.AsInt := vProc.Params[1].AsInteger;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vProc.Free;
end;

function BuscarXMLNFe(pConexao: TConexao; pNotaFiscalId: Integer): string;
var
  vSql: TConsulta;
begin
  Result := '';
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select XML_TEXTO ');
  vSql.SQL.Add('from NOTAS_FISCAIS_XML ');
  vSql.SQL.Add('where NOTA_FISCAL_ID = :P1 ');

  if vSql.Pesquisar([pNotaFiscalId]) then
    Result := vSql.GetString(0);

  vSql.Active := False;
  vSql.Free;
end;

function BuscarTiposPagamentosNota(pConexao: TConexao; pNotaFiscalId: Integer): Double;
var
  vSql: TConsulta;
begin
  Result := 0.0;
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  VALOR_RECEBIDO_ACUMULADO + ');
  vSql.SQL.Add('  VALOR_RECEBIDO_CARTAO_CRED + ');
  vSql.SQL.Add('  VALOR_RECEBIDO_CARTAO_DEB + ');
  vSql.SQL.Add('  VALOR_RECEBIDO_CHEQUE + ');
  vSql.SQL.Add('  VALOR_RECEBIDO_COBRANCA + ');
  vSql.SQL.Add('  VALOR_RECEBIDO_CREDITO + ');
  vSql.SQL.Add('  VALOR_RECEBIDO_DINHEIRO + ');
  vSql.SQL.Add('  VALOR_RECEBIDO_FINANCEIRA + ');
  vSql.SQL.Add('  VALOR_RECEBIDO_PIX as TOTAL_TIPOS_PAGAMENTO ');
  vSql.SQL.Add('from ');
  vSql.SQL.Add('  NOTAS_FISCAIS ');
  vSql.SQL.Add('where NOTA_FISCAL_ID = :P1 ');

  if vSql.Pesquisar([pNotaFiscalId]) then
    Result := vSql.GetDouble(0);

  vSql.Active := False;
  vSql.Free;
end;

function PodeCancelarNFe(pConexao: TConexao; pNotaFiscalId: Integer): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.TeveErro := False;
  vProc := TProcedimentoBanco.Create(pConexao, 'PODE_CANCELAR_NFE');

  try
    vProc.Params[0].AsInteger := pNotaFiscalId;
    vProc.Execute;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  vProc.Free;
end;

function BuscarXMLsNFe(pConexao: TConexao; pComando: string): TArray<RecXMLs>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  NFI.NOTA_FISCAL_ID, ');
  vSql.Add('  NFI.CHAVE_NFE, ');
  vSql.Add('  XML.XML_TEXTO ');
  vSql.Add('from ');
  vSql.Add('  NOTAS_FISCAIS_XML XML ');

  vSql.Add('inner join NOTAS_FISCAIS NFI ');
  vSql.Add('on XML.NOTA_FISCAL_ID = NFI.NOTA_FISCAL_ID ');
  vSql.Add(pComando);

  if vSql.Pesquisar() then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].notaFiscalId := vSql.GetInt('NOTA_FISCAL_ID');

      Result[i].xml := TStringList.Create;
      Result[i].xml.Add(vSql.GetString('XML_TEXTO'));

      Result[i].chave := vSql.GetString('CHAVE_NFE');

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function PedidoPodeGerarNFe(pConexao: TConexao; pOrcamentoId: Integer): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.TeveErro := False;
  vProc := TProcedimentoBanco.Create(pConexao, 'PEDIDO_PODE_GERAR_NFE');

  try
    vProc.Params[0].AsInteger := pOrcamentoId;
    vProc.Execute;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  vProc.Free;
end;

function BuscarIdsNotasDevolucoes(pConexao: TConexao; pDevolucaoId: Integer): TArray<Integer>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;

  vSql := TConsulta.Create(pConexao);
  vSql.Add('select ');
  vSql.Add('  NOTA_FISCAL_ID ');
  vSql.Add('from ');
  vSql.Add('  NOTAS_FISCAIS ');
  vSql.Add('where DEVOLUCAO_ID = :P1 ');

  if vSql.Pesquisar([pDevolucaoId]) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros -1 do begin
      Result[i] := vSql.GetInt(0);
      vSql.Next;
    end;
  end;

  vSql.Free;
end;

function BuscarDadosCartoesNFe(pConexao: TConexao; pMovimentoId: Integer; pTipoMovimento: string): TArray<RecDadosCartoes>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;

  if not Em(pTipoMovimento, ['VRA', 'VRE', 'VEN', 'ACU']) then
    Exit;

  vSql := TConsulta.Create(pConexao);

  if Em(pTipoMovimento, ['VRA', 'VRE', 'VEN']) then begin
    vSql.Add('select ');
    vSql.Add('  ADM.CNPJ as CNPJ_ADMINISTRADORA, ');
    vSql.Add('  TCO.BANDEIRA_CARTAO_NFE, ');
    vSql.Add('  PAG.CODIGO_AUTORIZACAO, ');
    vSql.Add('  PAG.TIPO_RECEB_CARTAO, ');
    vSql.Add('  TCO.TIPO_CARTAO, ');
    vSql.Add('  PAG.VALOR as VALOR_CARTAO, ');
    vSql.Add('  ORC.VALOR_TOTAL as TOTAL_MOVIMENTO ');
    vSql.Add('from ');
    vSql.Add('  ORCAMENTOS_PAGAMENTOS PAG ');

    vSql.Add('inner join TIPOS_COBRANCA TCO ');
    vSql.Add('on PAG.COBRANCA_ID = TCO.COBRANCA_ID ');

    vSql.Add('inner join ORCAMENTOS ORC ');
    vSql.Add('on PAG.ORCAMENTO_ID = ORC.ORCAMENTO_ID ');

    vSql.Add('inner join ADMINISTRADORAS_CARTOES ADM ');
    vSql.Add('on TCO.ADMINISTRADORA_CARTAO_ID = ADM.ADMINISTRADORA_ID ');

    vSql.Add('where PAG.ORCAMENTO_ID = :P1 ');
  end
  else begin
    vSql.Add('select ');
    vSql.Add('  ADM.CNPJ as CNPJ_ADMINISTRADORA, ');
    vSql.Add('  TCO.BANDEIRA_CARTAO_NFE, ');
    vSql.Add('  PAG.CODIGO_AUTORIZACAO, ');
    vSql.Add('  PAG.TIPO_RECEB_CARTAO, ');
    vSql.Add('  TCO.TIPO_CARTAO, ');
    vSql.Add('  PAG.VALOR as VALOR_CARTAO, ');
    vSql.Add('  ACU.VALOR_TOTAL as TOTAL_MOVIMENTO ');
    vSql.Add('from ');
    vSql.Add('  ACUMULADOS_PAGAMENTOS PAG ');

    vSql.Add('inner join TIPOS_COBRANCA TCO ');
    vSql.Add('on PAG.COBRANCA_ID = TCO.COBRANCA_ID ');

    vSql.Add('inner join ACUMULADOS ACU ');
    vSql.Add('on ACU.ACUMULADO_ID = ACU.ACUMULADO_ID ');

    vSql.Add('inner join ADMINISTRADORAS_CARTOES ADM ');
    vSql.Add('on TCO.ADMINISTRADORA_CARTAO_ID = ADM.ADMINISTRADORA_ID ');

    vSql.Add('where PAG.ACUMULADO_ID = :P1 ');
  end;

  if vSql.Pesquisar([pMovimentoId]) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros -1 do begin
      Result[i].CnpjAdminsitradora := vSql.GetString(0);
      Result[i].Bandeira           := vSql.GetString(1);
      Result[i].CodigoAutorizacao  := vSql.GetString(2);
      Result[i].TipoRecebCartao    := vSql.GetString(3);
      Result[i].TipoCartao         := vSql.GetString(4);
      Result[i].ValorCartao        := vSql.GetDouble(5);
      Result[i].TotalMovimento       := vSql.GetDouble(6);

      vSql.Next;
    end;
  end;

  vSql.Free;
end;

end.



//begin
//  EXECUTE IMMEDIATE 'ALTER TABLE NOTAS_FISCAIS DISABLE ALL TRIGGERS';
//
//  EXECUTE IMMEDIATE 'ALTER TABLE NOTAS_FISCAIS ADD VALOR_ICMS_INTER NUMBER(8,2) DEFAULT 0';
//
//  EXECUTE IMMEDIATE 'ALTER TABLE NOTAS_FISCAIS ENABLE ALL TRIGGERS';
//end;
