unit _NotasFiscaisCartasCorrXml;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecNotasFiscaisCartasCorrXml = record
    NotaFiscalId: Integer;
    Sequencia: Integer;
    XmlTexto: string;
  end;

  TNotasFiscaisCartasCorrXml = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordNotasFiscaisCartasCorrXml: RecNotasFiscaisCartasCorrXml;
  end;

function BuscarNotasFiscaisCartasCorrXml(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecNotasFiscaisCartasCorrXml>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TNotasFiscaisCartasCorrXml }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where NOTA_FISCAL_ID = :P1'
    );
end;

constructor TNotasFiscaisCartasCorrXml.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'NOTAS_FISCAIS_CARTAS_CORR_XML');

  FSql := 
    'select ' +
    '  NOTA_FISCAL_ID, ' +
    '  SEQUENCIA, ' +
    '  XML_TEXTO ' +
    'from ' +
    '  NOTAS_FISCAIS_CARTAS_CORR_XML';

  setFiltros(getFiltros);

  AddColuna('NOTA_FISCAL_ID', True);
  AddColuna('SEQUENCIA');
  AddColuna('XML_TEXTO');
end;

function TNotasFiscaisCartasCorrXml.getRecordNotasFiscaisCartasCorrXml: RecNotasFiscaisCartasCorrXml;
begin
  Result.NotaFiscalId               := getInt('NOTA_FISCAL_ID', True);
  Result.Sequencia                  := getInt('SEQUENCIA');
  Result.XmlTexto                   := getString('XML_TEXTO');
end;

function BuscarNotasFiscaisCartasCorrXml(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecNotasFiscaisCartasCorrXml>;
var
  i: Integer;
  t: TNotasFiscaisCartasCorrXml;
begin
  Result := nil;
  t := TNotasFiscaisCartasCorrXml.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordNotasFiscaisCartasCorrXml;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
