unit _RelacaoCRMOcorrencias;

interface

uses
  _Conexao, _OperacoesBancoDados, _BibliotecaGenerica, _RecordsEspeciais, SysUtils, _RecordsCadastros;

type
  RecRelacaoCRMOcorrencias = record
    ocorrencia_id: Integer;
    cadastro_id: Integer;
    nome_fantasia: string;
    data_cadastro: TDateTime;
    data_conclusao: TDateTime;
    descricao: string;
    solucao: string;
    status: string;
    status_analitico: string;
    prioridade: string;
    prioridade_analitico: string;
    usuario_id: Integer;
    usuario_nome: string;
    solicitacao_id: Integer;
  end;

function BuscarOcorrencias(pConexao: TConexao; pComando: string): TArray<RecRelacaoCRMOcorrencias>;

implementation

function BuscarOcorrencias(pConexao: TConexao; pComando: string): TArray<RecRelacaoCRMOcorrencias>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  CRM.OCORRENCIA_ID, ');
  vSql.Add('  CRM.CADASTRO_ID, ');
  vSql.Add('  CAD.NOME_FANTASIA, ');
  vSql.Add('  CRM.DATA_CADASTRO, ');
  vSql.Add('  CRM.DATA_CONCLUSAO, ');
  vSql.Add('  CRM.DESCRICAO, ');
  vSql.Add('  CRM.SOLUCAO, ');
  vSql.Add('  CRM.STATUS, ');
  vSql.Add('  CRM.USUARIO, ');
  vSql.Add('  FUN.NOME AS USUARIO_NOME, ');
  vSql.Add('  CRM.SOLICITACAO, ');
  vSql.Add('  CRM.PRIORIDADE ');
  vSql.Add('from ');
  vSql.Add('  CRM_OCORRENCIAS CRM ');

  vSql.Add('inner join CADASTROS CAD ');
  vSql.Add('on CAD.CADASTRO_ID = CRM.CADASTRO_ID ');

  vSql.Add('left join FUNCIONARIOS FUN ');
  vSql.Add('on CRM.USUARIO = FUN.FUNCIONARIO_ID ');

  vSql.Add(pComando);

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].ocorrencia_id     := vSql.GetInt('OCORRENCIA_ID');
      Result[i].cadastro_id       := vSql.GetInt('CADASTRO_ID');
      Result[i].nome_fantasia     := vSql.GetString('NOME_FANTASIA');
      Result[i].data_cadastro     := vSql.GetData('DATA_CADASTRO');
      Result[i].data_conclusao    := vSql.GetData('DATA_CONCLUSAO');
      Result[i].descricao         := vSql.GetString('DESCRICAO');
      Result[i].solucao           := vSql.GetString('SOLUCAO');
      Result[i].status            := vSql.GetString('STATUS');
      Result[i].prioridade         := vSql.GetString('PRIORIDADE');
      Result[i].usuario_id        := vSql.GetInt('USUARIO');
      Result[i].usuario_nome      := vSql.GetString('USUARIO_NOME');
      Result[i].solicitacao_id    := vSql.GetInt('SOLICITACAO');

      Result[i].status_analitico :=
        _BibliotecaGenerica.Decode(
          Result[i].status,
          [
            'AB', 'Aberta',
            'CO', 'Conclu�da',
            'PR', 'Programa��o',
            'RE', 'Recusada',
            'TE', 'Teste'
          ]
        );

      Result[i].prioridade_analitico :=
        _BibliotecaGenerica.Decode(
          Result[i].prioridade,
          [
            'BA', 'Baixa',
            'ME', 'M�dia',
            'AL', 'Alta',
            'UR', 'Recusada'
          ]
        );

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

end.
