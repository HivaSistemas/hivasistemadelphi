unit _CondicoesPagamentoPrazos;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecCondicoesPagamentoPrazos = record
    CondicaoId: Integer;
    Dias: Integer;
  end;

  TCondicoesPagamentoPrazos = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordOrdem: RecCondicoesPagamentoPrazos;
  end;

function BuscarCondicoesPagamentoPrazos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCondicoesPagamentoPrazos>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TCondicoesPagamentoPrazos }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CONDICAO_ID = :P1 ' +
      'order by ' +
      '  DIAS '
    );
end;

constructor TCondicoesPagamentoPrazos.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONDICOES_PAGAMENTO_PRAZOS');

  FSql :=
    'select ' +
    '  CONDICAO_ID, ' +
    '  DIAS ' +
    'from ' +
    '  CONDICOES_PAGAMENTO_PRAZOS ';

  setFiltros(getFiltros);

  AddColuna('CONDICAO_ID', True);
  AddColuna('DIAS', True);
end;

function TCondicoesPagamentoPrazos.getRecordOrdem: RecCondicoesPagamentoPrazos;
begin
  Result.CondicaoId := getInt('CONDICAO_ID', True);
  Result.Dias       := getInt('DIAS', True);
end;

function BuscarCondicoesPagamentoPrazos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCondicoesPagamentoPrazos>;
var
  i: Integer;
  t: TCondicoesPagamentoPrazos;
begin
  Result := nil;
  t := TCondicoesPagamentoPrazos.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordOrdem;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
