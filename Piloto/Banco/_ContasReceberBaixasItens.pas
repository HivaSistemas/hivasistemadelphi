unit _ContasReceberBaixasItens;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsFinanceiros;

{$M+}
type
  TContaReceberBaixaItem = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordContaReceberBaixaItem: RecContaReceberBaixaItem;
  end;

function AtualizarContasReceberBaixasItens(
  pConexao: TConexao;
  pBaixaId: Integer;
  pReceberId: Integer;
  pValorDinheiro: Double;
  pValorCartaoDeb: Double;
  pValorCartaoCre: Double;
  pValorCheque: Double;
  pValorCobranca: Double;
  pValorCredito: Double;
  pValorDesconto: Double;
  pValorMulta: Double;
  pValorJuros: Double;
  pValorPix: Double;
  pTransacaoAberta: Boolean
): RecRetornoBD;

function BuscarContaReceberBaixaItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecContaReceberBaixaItem>;

function ExcluirContaReceberBaixaItem(
  pConexao: TConexao;
  pBaixaId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TContaReceberBaixaItem }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CRI.BAIXA_ID = :P1 ' +
      'order by CRI.RECEBER_ID '
    );
end;

constructor TContaReceberBaixaItem.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONTAS_RECEBER_BAIXAS_ITENS');

  FSql :=
    'select ' +
    '  CRI.BAIXA_ID, ' +
    '  CRI.RECEBER_ID, ' +
    '  CRI.VALOR_DINHEIRO, ' +
    '  CRI.VALOR_CARTAO_DEBITO, ' +
    '  CRI.VALOR_CARTAO_CREDITO, ' +
    '  CRI.VALOR_CHEQUE, ' +
    '  CRI.VALOR_COBRANCA, ' +
    '  CRI.VALOR_CREDITO, ' +
    '  CRI.VALOR_DESCONTO, ' +
    '  CRI.VALOR_MULTA, ' +
    '  CRI.VALOR_JUROS, ' +
    '  CRI.VALOR_PIX, ' +
    '  COR.CADASTRO_ID, ' +
    '  CAD.RAZAO_SOCIAL as NOME_CLIENTE, ' +
    '  COR.DOCUMENTO, ' +
    '  COR.DATA_VENCIMENTO, ' +
    '  COR.VALOR_DOCUMENTO, ' +
    '  case when COR.STATUS = ''A'' then ' +
    '    case when trunc(sysdate) - COR.DATA_VENCIMENTO > 0 then trunc(sysdate) - COR.DATA_VENCIMENTO else 0 end ' +
    '  else ' +
    '    case when trunc(CRB.DATA_PAGAMENTO) - COR.DATA_VENCIMENTO > 0 then trunc(CRB.DATA_PAGAMENTO) - COR.DATA_VENCIMENTO else 0 end ' +
    '  end as DIAS_ATRASO, ' +
    '  COR.BANCO, ' +
    '  COR.AGENCIA, ' +
    '  COR.CONTA_CORRENTE, ' +
    '  COR.NUMERO_CHEQUE, ' +
    '  COR.TELEFONE_EMITENTE, ' +
    '  COR.NOME_EMITENTE, ' +
    '  COR.COBRANCA_ID, ' +
    '  TPC.NOME as NOME_TIPO_COBRANCA ' +
    'from ' +
    '  CONTAS_RECEBER_BAIXAS_ITENS CRI ' +

    'inner join CONTAS_RECEBER_BAIXAS CRB ' +
    'on CRI.BAIXA_ID = CRB.BAIXA_ID ' +

    'inner join CONTAS_RECEBER COR ' +
    'on CRI.RECEBER_ID = COR.RECEBER_ID ' +

    'inner join CADASTROS CAD ' +
    'on COR.CADASTRO_ID = CAD.CADASTRO_ID ' +

    'inner join TIPOS_COBRANCA TPC ' +
    'on COR.COBRANCA_ID = TPC.COBRANCA_ID ';

  setFiltros(getFiltros);

  AddColuna('BAIXA_ID', True);
  AddColuna('RECEBER_ID', True);
  AddColuna('VALOR_DINHEIRO');
  AddColuna('VALOR_CARTAO_DEBITO');
  AddColuna('VALOR_CARTAO_CREDITO');
  AddColuna('VALOR_CHEQUE');
  AddColuna('VALOR_COBRANCA');
  AddColuna('VALOR_CREDITO');
  AddColuna('VALOR_DESCONTO');
  AddColuna('VALOR_MULTA');
  AddColuna('VALOR_JUROS');
  AddColuna('VALOR_PIX');

  AddColunaSL('CADASTRO_ID');
  AddColunaSL('NOME_CLIENTE');
  AddColunaSL('DOCUMENTO');
  AddColunaSL('DATA_VENCIMENTO');
  AddColunaSL('VALOR_DOCUMENTO');
  AddColunaSL('DIAS_ATRASO');
  AddColunaSL('BANCO');
  AddColunaSL('AGENCIA');
  AddColunaSL('CONTA_CORRENTE');
  AddColunaSL('NUMERO_CHEQUE');
  AddColunaSL('NOME_EMITENTE');
  AddColunaSL('TELEFONE_EMITENTE');
  AddColunaSL('COBRANCA_ID');
  AddColunaSL('NOME_TIPO_COBRANCA');
end;

function TContaReceberBaixaItem.getRecordContaReceberBaixaItem: RecContaReceberBaixaItem;
begin
  Result.baixa_id                := getInt('BAIXA_ID', True);
  Result.receber_id              := getInt('RECEBER_ID', True);
  Result.valor_dinheiro          := getDouble('VALOR_DINHEIRO');
  Result.ValorCartaoDebito       := getDouble('VALOR_CARTAO_DEBITO');
  Result.ValorCartaoCredito      := getDouble('VALOR_CARTAO_CREDITO');
  Result.valor_cheque            := getDouble('VALOR_CHEQUE');
  Result.valor_cobranca          := getDouble('VALOR_COBRANCA');
  Result.valor_credito           := getDouble('VALOR_CREDITO');
  Result.valor_desconto          := getDouble('VALOR_DESCONTO');
  Result.valor_pix               := getDouble('VALOR_PIX');
  Result.ValorMulta              := getDouble('VALOR_MULTA');
  Result.ValorJuros              := getDouble('VALOR_JUROS');
  Result.CadastroId              := getInt('CADASTRO_ID');
  Result.nome_cliente            := getString('NOME_CLIENTE');
  Result.documento               := getString('DOCUMENTO');
  Result.data_vencimento         := getData('DATA_VENCIMENTO');
  Result.valor_documento         := getDouble('VALOR_DOCUMENTO');
  Result.DiasAtraso              := getInt('DIAS_ATRASO');
  Result.Banco                   := getInt('BANCO');
  Result.Agencia                 := getInt('AGENCIA');
  Result.ContaCorrente           := getString('CONTA_CORRENTE');
  Result.NumeroCheque            := getInt('NUMERO_CHEQUE');
  Result.NomeEmitente            := getString('NOME_EMITENTE');
  Result.TelefoneEmitente        := getString('TELEFONE_EMITENTE');
  Result.CobrancaId              := getInt('COBRANCA_ID');
  Result.NomeTipoCobranca        := getString('NOME_TIPO_COBRANCA');
end;

function AtualizarContasReceberBaixasItens(
  pConexao: TConexao;
  pBaixaId: Integer;
  pReceberId: Integer;
  pValorDinheiro: Double;
  pValorCartaoDeb: Double;
  pValorCartaoCre: Double;
  pValorCheque: Double;
  pValorCobranca: Double;
  pValorCredito: Double;
  pValorDesconto: Double;
  pValorMulta: Double;
  pValorJuros: Double;
  pValorPix: Double;
  pTransacaoAberta: Boolean
): RecRetornoBD;
var
  t: TContaReceberBaixaItem;
begin
  Result.TeveErro := False;
  t := TContaReceberBaixaItem.Create(pConexao);
  try
    if not pTransacaoAberta then
      pConexao.IniciarTransacao;

    t.setInt('BAIXA_ID', pBaixaId, True);
    t.setInt('RECEBER_ID', pReceberId, True);
    t.setDouble('VALOR_DINHEIRO', pValorDinheiro);
    t.setDouble('VALOR_CARTAO_DEBITO', pValorCartaoDeb);
    t.setDouble('VALOR_CARTAO_CREDITO', pValorCartaoCre);
    t.setDouble('VALOR_CHEQUE', pValorCheque);
    t.setDouble('VALOR_COBRANCA', pValorCobranca);
    t.setDouble('VALOR_CREDITO', pValorCredito);
    t.setDouble('VALOR_DESCONTO', pValorDesconto);
    t.setDouble('VALOR_MULTA', pValorMulta);
    t.setDouble('VALOR_JUROS', pValorJuros);
    t.setDouble('VALOR_PIX', pValorPix);

    t.Inserir;

    if not pTransacaoAberta then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pTransacaoAberta then
        pConexao.VoltarTransacao;

      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function BuscarContaReceberBaixaItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecContaReceberBaixaItem>;
var
  i: Integer;
  t: TContaReceberBaixaItem;
begin
  Result := nil;
  t := TContaReceberBaixaItem.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordContaReceberBaixaItem;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirContaReceberBaixaItem(
  pConexao: TConexao;
  pBaixaId: Integer
): RecRetornoBD;
var
  t: TContaReceberBaixaItem;
begin
  Result.TeveErro := False;
  t := TContaReceberBaixaItem.Create(pConexao);

  try
    t.setInt('BAIXA_ID', pBaixaId, True);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
