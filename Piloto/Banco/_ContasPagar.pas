unit _ContasPagar;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _Sessao, _RecordsFinanceiros;

{$M+}
type
  RecCreditosAdiantamentosAcumulados = record
    PagarId: Integer;
    Valor: Currency;
  end;

  TContaPagar = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordContaPagar: RecContaPagar;
  end;

function AtualizarContaPagar(
  pConexao: TConexao;
  pContaPagarId: Integer;
  pEmpresaId: Integer;
  pFornecedorId: Integer;
  pCobrancaId: Integer;
  pPortadorId: string;
  pPlanoFinanceiroId: string;
  pDocumento: string;
  pValorDocumento: Double;
  pValorDesconto: Double;
  pOrigem: string;
  pBaixaReceberOrigemId: Integer;
  pParcela: Integer;
  pNumeroParcelas: Integer;
  pNumeroCheque: Integer;
  pStatus: string;
  pDataVencimento: TDateTime;
  pDataVencimentoOriginal: TDateTime;
  pNossoNumero: string;
  pCodigoBarras: string;
  pBloqueado: string;
  pMotivoBloqueio: string;
  pOrcamentoId: Integer;
  pDataEmissao: TDateTime;
  pDataContabil: TDateTime;
  pObservacoes: string;
  pCentroCustoId: Integer;
  pEmTransacao: Boolean
): RecRetornoBD;

function BuscarContasPagar(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecContaPagar>;

function BuscarContasPagarComando(
  pConexao: TConexao;
  pComando: string
): TArray<RecContaPagar>;

function ExcluirContaPagar(
  pConexao: TConexao;
  pContaPagarId: Integer
): RecRetornoBD;

function AtualizarFornecedor(
  pConexao: TConexao;
  pPagarIds: TArray<Integer>;
  pCadastroId: Integer
): RecRetornoBD;

function getTemCreditoIndiceMaiorCondicao( pConexao: TConexao; pIndiceCondicao: Double; pCreditosIds: TArray<Integer> ): Boolean;
function ExistemTitulosBaixados(pConexao: TConexao; pReceberIds: TArray<Integer>): Boolean;
function AtualizarDataContabil(pConexao: TConexao; pPagarId: Integer; pDataVencimento: TDateTime): RecRetornoBD;
function AtualizarDataVencimento(pConexao: TConexao; pPagarIds: TArray<Integer>; pDataVencimento: TDateTime): RecRetornoBD;
function AtualizarBloquearDesbloquear(pConexao: TConexao; pPagarIds: TArray<Integer>; pBloquear: string; pMotivoBloqueio: string): RecRetornoBD;
function AtualizarValorDesconto(pConexao: TConexao; pPagarId: Integer; pValorDesconto: Double): RecRetornoBD;
function AtualizarCampoDocumento(pConexao: TConexao; pPagarId: Integer; pDocumento: string): RecRetornoBD;
function BuscarCreditosAdiantamentosAcumulados(pConexao: TConexao; CadastroId: Integer): TArray<RecCreditosAdiantamentosAcumulados>;
function BuscarCreditoDevolucao(pConexao: TConexao; DevolucaoId: Integer): Double;
function AtualizarValorDocumento(pConexao: TConexao; pPagarId: Integer; pValorDocumento: Double): RecRetornoBD;
function LancarAdiantamentoContasPagar(pConexao: TConexao; const pPagarId: Integer; pEmTransacao: Boolean): RecRetornoBD;
function AtualizarPlanoFinanceiro(pConexao: TConexao; pPagarId: Integer; pPlanoFinanceiroId: string): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TContaPagar }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 9);  // Sempre setar a quantidade de results primeiro para acrescentar um novo Result

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COP.PAGAR_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COP.PAGAR_ID = :P1 ' +
      'and COP.STATUS = ''A'' '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COP.CADASTRO_ID = :P1 ' +
      'and COP.STATUS = ''A'' ' +
      'and (COP.COBRANCA_ID = :P2 or COP.COBRANCA_ID = :P3) ' +
      'and COP.ORIGEM not in(''ADA'') ' + // N�o trazer os cr�ditos de adiantamentos de acumulados
      'order by ' +
      '  COP.PAGAR_ID '
    );

  Result[3] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COP.BAIXA_ORIGEM_ID = :P1 ' +
      'order by ' +
      '  COP.PAGAR_ID '
    );

  Result[4] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COP.ENTRADA_ID = :P1 ' +
      'order by ' +
      '  COP.PAGAR_ID '
    );

  Result[5] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COP.BAIXA_ID in(select BAIXA_ID from CONTAS_PAGAR_BAIXAS where BAIXA_RECEBER_ORIGEM_ID = :P1) ' +
      'and (COP.COBRANCA_ID = :P2 or COP.COBRANCA_ID = :P3) ' +
      'order by ' +
      '  COP.PAGAR_ID '
    );

  Result[6] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COP.COMPRA_ID = :P1 ' +
      'order by ' +
      '  COP.PAGAR_ID '
    );

  Result[7] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COP.CADASTRO_ID = :P1 ' +
      'and COP.STATUS = ''A'' ' +
      'and (COP.COBRANCA_ID = :P2 or COP.COBRANCA_ID = :P3) ' +
      'order by ' +
      '  COP.PAGAR_ID '
    );

  Result[8] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COP.CONHECIMENTO_FRETE_ID = :P1 ' +
      'order by ' +
      '  COP.PAGAR_ID '
    );
end;

constructor TContaPagar.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONTAS_PAGAR');

  FSql :=
    'select ' +
    '  COP.PAGAR_ID, ' +
    '  COP.CADASTRO_ID, ' +
    '  CAD.RAZAO_SOCIAL as NOME_FORNECEDOR, ' +
    '  COP.EMPRESA_ID, ' +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA, ' +
    '  COP.ENTRADA_ID, ' +
    '  COP.COBRANCA_ID, ' +
    '  COP.PORTADOR_ID, ' +
    '  COP.PLANO_FINANCEIRO_ID, ' +
    '  TCO.NOME as NOME_TIPO_COBRANCA, ' +
    '  COP.USUARIO_CADASTRO_ID, ' +
    '  FUN.NOME as NOME_USUARIO_CADASTRO, ' +
    '  COP.DOCUMENTO, ' +
    '  COP.VALOR_DOCUMENTO, ' +
    '  COP.VALOR_DESCONTO, ' +
    '  COP.PARCELA, ' +
    '  COP.NUMERO_PARCELAS, ' +
    '  COP.NUMERO_CHEQUE, ' +
    '  COP.STATUS, ' +
    '  COP.INDICE_CONDICAO_PAGAMENTO, ' +
    '  COP.DATA_CADASTRO, ' +
    '  COP.DATA_VENCIMENTO, ' +
    '  COP.DATA_VENCIMENTO_ORIGINAL, ' +
    '  COP.NOSSO_NUMERO, ' +
    '  COP.CODIGO_BARRAS, ' +
    '  COP.BAIXA_ORIGEM_ID, ' +
    '  COP.DEVOLUCAO_ID, ' +
    '  BAX.BAIXA_ID, ' +
    '  CPB.USUARIO_BAIXA_ID, ' +
    '  FBA.NOME as NOME_USUARIO_BAIXA, ' +
    '  CPB.DATA_PAGAMENTO, ' +
    '  CPB.DATA_HORA_BAIXA, ' +
    '  case when COP.STATUS = ''A'' then ' +
    '    case when trunc(sysdate) - COP.DATA_VENCIMENTO > 0 then trunc(sysdate) - COP.DATA_VENCIMENTO else 0 end ' +
    '  else ' +
    '    case when trunc(BAI.DATA_PAGAMENTO) - COP.DATA_VENCIMENTO > 0 then trunc(BAI.DATA_PAGAMENTO) - COP.DATA_VENCIMENTO else 0 end ' +
    '  end as DIAS_ATRASO, ' +
    '  COP.BAIXA_RECEBER_ORIGEM_ID, ' +
    '  COP.ORIGEM, ' +
    '  COP.BLOQUEADO, ' +
    '  COP.MOTIVO_BLOQUEIO, ' +
    '  COP.ORCAMENTO_ID, ' +
    '  COP.ACUMULADO_ID, ' +
    '  COP.DATA_EMISSAO, ' +
    '  COP.DATA_CONTABIL, ' +
    '  COP.OBSERVACOES, ' +
    '  COP.ORIGEM, ' +
    '  COP.VALOR_ADIANTADO, ' +
    '  PLF.DESCRICAO AS NOME_PLANO_FINANCEIRO, ' +
    '  COP.CENTRO_CUSTO_ID, ' +
    '  CCT.NOME AS NOME_CENTRO_CUSTO, ' +
    '  POT.DESCRICAO AS NOME_PORTADOR ' +
    'from ' +
    '  CONTAS_PAGAR COP ' +

    'inner join CADASTROS CAD ' +
    'on COP.CADASTRO_ID = CAD.CADASTRO_ID ' +

    'inner join TIPOS_COBRANCA TCO ' +
    'on COP.COBRANCA_ID = TCO.COBRANCA_ID ' +

    'inner join FUNCIONARIOS FUN ' +
    'on COP.USUARIO_CADASTRO_ID = FUN.FUNCIONARIO_ID ' +

    'inner join EMPRESAS EMP ' +
    'on COP.EMPRESA_ID = EMP.EMPRESA_ID ' +

    'inner join PLANOS_FINANCEIROS PLF ' +
    'on COP.PLANO_FINANCEIRO_ID = PLF.PLANO_FINANCEIRO_ID ' +

    'inner join CENTROS_CUSTOS CCT ' +
    'on COP.CENTRO_CUSTO_ID = CCT.CENTRO_CUSTO_ID ' +

    'inner join PORTADORES POT ' +
    'on POT.PORTADOR_ID = COP.PORTADOR_ID ' +

    'left join CONTAS_PAGAR_BAIXAS_ITENS BAX ' +
    'on COP.PAGAR_ID = BAX.PAGAR_ID ' +

    'left join CONTAS_PAGAR_BAIXAS CPB ' +
    'on BAX.BAIXA_ID = CPB.BAIXA_ID ' +

    'left join FUNCIONARIOS FBA ' +
    'on CPB.USUARIO_BAIXA_ID = FBA.FUNCIONARIO_ID ' +

    'left join CONTAS_PAGAR_BAIXAS BAI ' +
    'on COP.BAIXA_ORIGEM_ID = BAI.BAIXA_ID ' +

    'left join CONTAS_PAGAR_BAIXAS_ITENS BXI ' +
    'on COP.PAGAR_ID = BXI.PAGAR_ID ' +
    'and BAI.BAIXA_ID = BXI.BAIXA_ID ';

  setFiltros(getFiltros);

  AddColuna('PAGAR_ID', True);
  AddColunaSL('NOME_FORNECEDOR');
  AddColuna('CADASTRO_ID');
  AddColuna('EMPRESA_ID');
  AddColunaSL('NOME_EMPRESA');
  AddColuna('ENTRADA_ID');
  AddColuna('COBRANCA_ID');
  AddColuna('PORTADOR_ID');
  AddColuna('PLANO_FINANCEIRO_ID');
  AddColuna('ORIGEM');
  AddColuna('BLOQUEADO');
  AddColuna('MOTIVO_BLOQUEIO');
  AddColuna('ORCAMENTO_ID');
  AddColunaSL('ACUMULADO_ID');
  AddColunaSL('NOME_TIPO_COBRANCA');
  AddColunaSL('USUARIO_CADASTRO_ID');
  AddColunaSL('NOME_USUARIO_CADASTRO');
  AddColuna('DOCUMENTO');
  AddColuna('VALOR_DOCUMENTO');
  AddColuna('VALOR_DESCONTO');
  AddColuna('PARCELA');
  AddColuna('NUMERO_PARCELAS');
  AddColuna('NUMERO_CHEQUE');
  AddColuna('STATUS');
  AddColunaSL('INDICE_CONDICAO_PAGAMENTO');
  AddColunaSL('DATA_CADASTRO');
  AddColuna('DATA_VENCIMENTO');
  AddColuna('DATA_VENCIMENTO_ORIGINAL');
  AddColuna('NOSSO_NUMERO');
  AddColuna('CODIGO_BARRAS');
  AddColuna('BAIXA_ORIGEM_ID');
  AddColuna('DATA_EMISSAO');
  AddColuna('DATA_CONTABIL');
  AddColuna('OBSERVACOES');
  AddColunaSL('DEVOLUCAO_ID');
  AddColunaSL('ORIGEM');
  AddColunaSL('VALOR_ADIANTADO');
  AddColuna('BAIXA_RECEBER_ORIGEM_ID');
  AddColunaSL('BAIXA_ID');
  AddColunaSL('USUARIO_BAIXA_ID');
  AddColunaSL('NOME_USUARIO_BAIXA');
  AddColunaSL('DATA_PAGAMENTO');
  AddColunaSL('DATA_HORA_BAIXA');
  AddColunaSL('DIAS_ATRASO');
  AddColunaSL('NOME_PLANO_FINANCEIRO');
  AddColuna('CENTRO_CUSTO_ID');
  AddColunaSL('NOME_CENTRO_CUSTO');
  AddColunaSL('NOME_PORTADOR');
end;

function TContaPagar.getRecordContaPagar: RecContaPagar;
begin
  Result.PagarId                  := getInt('PAGAR_ID', True);
  Result.CadastroId               := getInt('CADASTRO_ID');
  Result.nome_fornecedor          := getString('NOME_FORNECEDOR');
  Result.empresa_id               := getInt('EMPRESA_ID');
  Result.nome_empresa             := getString('NOME_EMPRESA');
  Result.entrada_id               := getInt('ENTRADA_ID');
  Result.cobranca_id              := getInt('COBRANCA_ID');
  Result.nome_tipo_cobranca       := getString('NOME_TIPO_COBRANCA');
  Result.usuario_cadastro_id      := getInt('USUARIO_CADASTRO_ID');
  Result.nome_usuario_cadastro    := getString('NOME_USUARIO_CADASTRO');
  Result.documento                := getString('DOCUMENTO');
  Result.ValorDocumento          := getDouble('VALOR_DOCUMENTO');
  Result.ValorDesconto            := getDouble('VALOR_DESCONTO');
  Result.ValorAdiantado           := getDouble('VALOR_ADIANTADO');
  Result.ValorSaldo               := Result.ValorDocumento - Result.ValorDesconto - Result.ValorAdiantado;
  Result.parcela                  := getInt('PARCELA');
  Result.numero_parcelas          := getInt('NUMERO_PARCELAS');
  Result.numero_cheque            := getInt('NUMERO_CHEQUE');
  Result.status                   := getString('STATUS');
  Result.IndiceCondicaoPagamento  := getDouble('INDICE_CONDICAO_PAGAMENTO');
  Result.data_cadastro            := getData('DATA_CADASTRO');
  Result.data_vencimento          := getData('DATA_VENCIMENTO');
  Result.data_vencimento_original := getData('DATA_VENCIMENTO_ORIGINAL');
  Result.nosso_numero             := getString('NOSSO_NUMERO');
  Result.codigo_barras            := getString('CODIGO_BARRAS');
  Result.Bloqueado                := getString('BLOQUEADO');
  Result.MotivoBloqueio           := getString('MOTIVO_BLOQUEIO');
  Result.OrcamentoId              := getInt('ORCAMENTO_ID');
  Result.AcumuladoId              := getInt('ACUMULADO_ID');
  Result.baixa_origem_id          := getInt('BAIXA_ORIGEM_ID');
  Result.DevolucaoId              := getInt('DEVOLUCAO_ID');
  Result.BaixaReceberOrigemId     := getInt('BAIXA_RECEBER_ORIGEM_ID');
  Result.baixa_id                 := getInt('BAIXA_ID');
  Result.usuario_baixa_id         := getInt('USUARIO_BAIXA_ID');
  Result.nome_usuario_baixa       := getString('NOME_USUARIO_BAIXA');
  Result.data_pagamento           := getData('DATA_PAGAMENTO');
  Result.data_hora_baixa          := getData('DATA_HORA_BAIXA');
  Result.dias_atraso              := getInt('DIAS_ATRASO');
  Result.PlanoFinanceiroId        := getString('PLANO_FINANCEIRO_ID');
  Result.NomePlanoFinanceiro      := getString('NOME_PLANO_FINANCEIRO');
  Result.CentroCustoId            := getInt('CENTRO_CUSTO_ID');
  Result.NomeCentroCusto          := getString('NOME_CENTRO_CUSTO');
  Result.PortadorId               := getInt('PORTADOR_ID');
  Result.NomePortador             := getString('NOME_PORTADOR');
  Result.DataEmissao              := getData('DATA_EMISSAO');
  Result.DataContabil             := getData('DATA_CONTABIL');
  Result.Observacoes              := getString('OBSERVACOES');
  Result.Origem                   := getString('ORIGEM');

  Result.StatusAnalitico := IIfStr(Result.status = 'A', 'Aberto', 'Baixado');

  Result.OrigemAnalitico :=
    _Biblioteca.Decode(
      Result.Origem, [
      'MAN', 'Manual',
      'COM', 'Compras',
      'ENT', 'Entradas',
      'ENS', 'Entradas de nf. servi�os',
      'CON', 'Conhec. de frete',
      'BCP', 'Baixa de contas a pagar',
      'BCR', 'Baixa de contas a receb.',
      'DEV', 'Devolu��es',
      'CVE', 'Cr�dito de venda',
      'CAC', 'Cr�dito de acumulado',
      'ADA', 'Adiantamento de acum.'
    ]);
end;

function AtualizarContaPagar(
  pConexao: TConexao;
  pContaPagarId: Integer;
  pEmpresaId: Integer;
  pFornecedorId: Integer;
  pCobrancaId: Integer;
  pPortadorId: string;
  pPlanoFinanceiroId: string;
  pDocumento: string;
  pValorDocumento: Double;
  pValorDesconto: Double;
  pOrigem: string;
  pBaixaReceberOrigemId: Integer;
  pParcela: Integer;
  pNumeroParcelas: Integer;
  pNumeroCheque: Integer;
  pStatus: string;
  pDataVencimento: TDateTime;
  pDataVencimentoOriginal: TDateTime;
  pNossoNumero: string;
  pCodigoBarras: string;
  pBloqueado: string;
  pMotivoBloqueio: string;
  pOrcamentoId: Integer;
  pDataEmissao: TDateTime;
  pDataContabil: TDateTime;
  pObservacoes: string;
  pCentroCustoId: Integer;
  pEmTransacao: Boolean
): RecRetornoBD;
var
  t: TContaPagar;
  vNovo: Boolean;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_CONTAS_PAGAR');
  t := TContaPagar.Create(pConexao);

  vNovo := pContaPagarId = 0;
  if vNovo then begin
    pContaPagarId := TSequencia.Create(pConexao, 'SEQ_PAGAR_ID').GetProximaSequencia;
    Result.AsInt := pContaPagarId;
  end;

  try
    if not pEmTransacao then
      pConexao.IniciarTransacao;

    t.setInt('PAGAR_ID', pContaPagarId, True);
    t.setInt('EMPRESA_ID', pEmpresaId);
    t.setInt('CADASTRO_ID', pFornecedorId);
    t.setInt('COBRANCA_ID', pCobrancaId);
    t.setString('PORTADOR_ID', pPortadorId);
    t.setString('PLANO_FINANCEIRO_ID', pPlanoFinanceiroId);
    t.setString('DOCUMENTO', pDocumento);
    t.setDouble('VALOR_DOCUMENTO', pValorDocumento);
    t.setDouble('VALOR_DESCONTO', pValorDesconto);
    t.setString('ORIGEM', pOrigem);
    t.setIntN('BAIXA_RECEBER_ORIGEM_ID', pBaixaReceberOrigemId);
    t.setInt('PARCELA', pParcela);
    t.setInt('NUMERO_PARCELAS', pNumeroParcelas);
    t.setInt('NUMERO_CHEQUE', pNumeroCheque);
    t.setString('STATUS', pStatus);
    t.setData('DATA_VENCIMENTO', pDataVencimento);
    t.setData('DATA_VENCIMENTO_ORIGINAL', pDataVencimentoOriginal);
    t.setStringN('NOSSO_NUMERO', pNossoNumero);
    t.setStringN('CODIGO_BARRAS', pCodigoBarras);
    t.setString('BLOQUEADO', pBloqueado);
    t.setString('MOTIVO_BLOQUEIO', pMotivoBloqueio);
    t.setIntN('ORCAMENTO_ID', pOrcamentoId);
    t.setData('DATA_EMISSAO', pDataEmissao);
    t.setData('DATA_CONTABIL', pDataContabil);
    t.setStringN('OBSERVACOES', pObservacoes);
    t.setInt('CENTRO_CUSTO_ID', pCentroCustoId);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    if not pEmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pEmTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarContasPagar(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecContaPagar>;
var
  i: Integer;
  t: TContaPagar;
begin
  Result := nil;
  t := TContaPagar.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordContaPagar;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarContasPagarComando(
  pConexao: TConexao;
  pComando: string
): TArray<RecContaPagar>;
var
  i: Integer;
  t: TContaPagar;
begin
  Result := nil;
  t := TContaPagar.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordContaPagar;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirContaPagar(
  pConexao: TConexao;
  pContaPagarId: Integer
): RecRetornoBD;
var
  t: TContaPagar;
begin
  Result.Iniciar;
  pConexao.SetRotina('EXCLUIR_CONTA_PAGAR');
  t := TContaPagar.Create(pConexao);

  try
    pConexao.IniciarTransacao;
    
    t.setInt('PAGAR_ID', pContaPagarId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function getTemCreditoIndiceMaiorCondicao( pConexao: TConexao; pIndiceCondicao: Double; pCreditosIds: TArray<Integer> ): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  max(INDICE_CONDICAO_PAGAMENTO) ');
  vSql.Add('from ');
  vSql.Add('  CONTAS_PAGAR ');
  vSql.Add('where ' + FiltroInInt('PAGAR_ID', pCreditosIds));

  vSql.Pesquisar;

  Result := vSql.GetDouble(0) > pIndiceCondicao;

  vSql.Free;
end;

function ExistemTitulosBaixados(pConexao: TConexao; pReceberIds: TArray<Integer>): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  count(*) ');
  vSql.Add('from ');
  vSql.Add('  CONTAS_PAGAR ');

  vSql.Add('where ' + FiltroInInt('PAGAR_ID', pReceberIds));
  vSql.Add('and STATUS = ''B'' ');

  vSql.Pesquisar;
  Result := vSql.GetInt(0) > 0;

  vSql.Active := False;
  vSql.Free;

end;

function AtualizarDataContabil(pConexao: TConexao; pPagarId: Integer; pDataVencimento: TDateTime): RecRetornoBD;
var
  vExecucao: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_DATA_CONTABIL_PAGAR'); //grava o log do ultimo registro
  vExecucao := TExecucao.Create(pConexao);

  vExecucao.Add('update CONTAS_PAGAR set ');
  vExecucao.Add('  DATA_CONTABIL = :P1 ');
  vExecucao.Add('where PAGAR_ID = :P2');
  try
    pConexao.IniciarTransacao;

    vExecucao.Executar([pDatavencimento,pPagarId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
end;

function AtualizarDataVencimento(pConexao: TConexao; pPagarIds: TArray<Integer>; pDataVencimento: TDateTime): RecRetornoBD;
var
  vExecucao: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_DATA_VENCIMENTO_PAGAR'); //grava o log do ultimo registro
  vExecucao := TExecucao.Create(pConexao);

  vExecucao.Add('update CONTAS_PAGAR set ');
  vExecucao.Add('  DATA_VENCIMENTO = :P1 ');
  vExecucao.Add('where ' + FiltroInInt('PAGAR_ID', pPagarIds));
  try
    pConexao.IniciarTransacao;

    vExecucao.Executar([pDatavencimento]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
end;

function AtualizarBloquearDesbloquear(pConexao: TConexao; pPagarIds: TArray<Integer>; pBloquear: string; pMotivoBloqueio: string): RecRetornoBD;
var
  vExecucao: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_BLOQUEAR_DESB_TITULOS'); //grava o log do ultimo registro
  vExecucao := TExecucao.Create(pConexao);

  vExecucao.Add('update CONTAS_PAGAR set ');
  vExecucao.Add('  BLOQUEADO = :P1, ');
  vExecucao.Add('  MOTIVO_BLOQUEIO = :P2 ');
  vExecucao.Add('where ' + FiltroInInt('PAGAR_ID', pPagarIds));
  try
    pConexao.IniciarTransacao;

    vExecucao.Executar([pBloquear, pMotivoBloqueio]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
end;

function AtualizarValorDesconto(pConexao: TConexao; pPagarId: Integer; pValorDesconto: Double): RecRetornoBD;
var
  vExecucao: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_VALOR_DESC_CONTAS_PAGAR');
  vExecucao := TExecucao.Create(pConexao);

  vExecucao.Add('update CONTAS_PAGAR set ');
  vExecucao.Add('  VALOR_DESCONTO = :P2 ');
  vExecucao.Add('where PAGAR_ID = :P1');
  try
    pConexao.IniciarTransacao;

    vExecucao.Executar([pPagarId, pValorDesconto]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
end;

function AtualizarCampoDocumento(pConexao: TConexao; pPagarId: Integer; pDocumento: string): RecRetornoBD;
var
  vExecucao: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_DOCUMENTO_CONTAS_PAGAR');
  vExecucao := TExecucao.Create(pConexao);

  vExecucao.Add('update CONTAS_PAGAR set ');
  vExecucao.Add('  DOCUMENTO = :P2 ');
  vExecucao.Add('where PAGAR_ID = :P1');
  try
    pConexao.IniciarTransacao;

    vExecucao.Executar([pPagarId, pDocumento]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
end;

function BuscarCreditoDevolucao(pConexao: TConexao; DevolucaoId: Integer): Double;
var
  i: Integer;
  vSql: TConsulta;
begin
   Result := 0.00;

  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  VALOR_DOCUMENTO ');
  vSql.Add('from CONTAS_PAGAR ');
  vSql.Add('where DEVOLUCAO_ID = :P1 ');

  if vSql.Pesquisar([DevolucaoId]) then begin
    for i := 0 to vSql.GetQuantidadeRegistros -1 do begin
      Result := vSql.GetDouble(0);
      vSql.Next;
    end;
  end;

  vSql.Free;
end;

function BuscarCreditosAdiantamentosAcumulados(pConexao: TConexao; CadastroId: Integer): TArray<RecCreditosAdiantamentosAcumulados>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  PAGAR_ID, ');
  vSql.Add('  VALOR_DOCUMENTO - VALOR_ADIANTADO ');
  vSql.Add('from ');
  vSql.Add('  CONTAS_PAGAR ');
  //vSql.Add('where ' + FiltroInInt('ORCAMENTO_ID', pOrcamentosIds));
  //vSql.Add('and ORIGEM = ''ADA'' ');
  vSql.Add('where CADASTRO_ID = :P1');
  vSql.Add('and COBRANCA_ID in (11, 12)');
  vSql.Add('and STATUS = ''A'' ');

  if vSql.Pesquisar([CadastroId]) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros -1 do begin
      Result[i].PagarId := vSql.GetInt(0);
      Result[i].Valor   := vSql.GetDouble(1);
      vSql.Next;
    end;
  end;

  vSql.Free;
end;

function AtualizarValorDocumento(pConexao: TConexao; pPagarId: Integer; pValorDocumento: Double): RecRetornoBD;
var
  vExex: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_VALOR_DOCUMENTO_CONTAS_PAGAR');
  vExex := TExecucao.Create(pConexao);

  vExex.Add('update CONTAS_PAGAR set ');
  vExex.Add('  VALOR_DOCUMENTO = :P2 ');
  vExex.Add('where PAGAR_ID = :P1');
  try
    pConexao.IniciarTransacao;

    vExex.Executar([pPagarId, pValorDocumento]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
end;

function LancarAdiantamentoContasPagar(pConexao: TConexao; const pPagarId: Integer; pEmTransacao: Boolean): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.setRotina('LANCAR_ADIANTAMENTO_CONTAS_PAG');
  vProc := TProcedimentoBanco.Create(pConexao, 'LANCAR_ADIANT_CONTAS_PAGAR');

  try
    if not pEmTransacao then
      pConexao.IniciarTransacao;

    vProc.Params[0].AsInteger := pPagarId;

    vProc.ExecProc;

    Result.AsInt := vProc.Params[1].AsInteger;

    if not pEmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pEmTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;
  vProc.Free;
end;

function AtualizarPlanoFinanceiro(pConexao: TConexao; pPagarId: Integer; pPlanoFinanceiroId: string): RecRetornoBD;
var
  vExex: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_PLAN_FINANCEIRO_CONTAS_PAGAR');
  vExex := TExecucao.Create(pConexao);

  vExex.Add('update CONTAS_PAGAR set ');
  vExex.Add('  PLANO_FINANCEIRO_ID = :P2 ');
  vExex.Add('where PAGAR_ID = :P1');
  try
    pConexao.IniciarTransacao;

    vExex.Executar([pPagarId, pPlanoFinanceiroId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
end;

function AtualizarFornecedor(
  pConexao: TConexao;
  pPagarIds: TArray<Integer>;
  pCadastroId: Integer
): RecRetornoBD;
var
  vExex: TExecucao;
  i: Integer;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_FORNECEDOR_CONTAS_PAGAR');
  vExex := TExecucao.Create(pConexao);

  vExex.Add('update CONTAS_PAGAR set ');
  vExex.Add('  CADASTRO_ID = :P2 ');
  vExex.Add('where PAGAR_ID = :P1');

  try
    pConexao.IniciarTransacao;

    for i := Low(pPagarIds) to High(pPagarIds) do
      vExex.Executar([pPagarIds[i], pCadastroId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
end;

end.
