unit _ImpressorasUsuarios;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecImpressorasUsuarios = record
    EmpresaId: Integer;
    UsuarioId: Integer;

    FuncionarioId: Integer;

    TipoImpressoraNFe: string;
    ImpressoraNFe: string;
    AbrirPreviewNFe: string;

    TipoImpressoraNFCe: string;
    ImpressoraNFCe: string;
    AbrirPreviewNFCe: string;

    TipoImpressoraCompPagamento: string;
    ImpressoraCompPagamento: string;
    AbrirPreviewCompPagamento: string;

    TipoImpressoraComproEntrega: string;
    ImpressoraComproEntrega: string;
    AbrirPreviewComproEntrega: string;

    TipoImpressoraListaSeparac: string;
    ImpressoraListaSeparac: string;
    AbrirPreviewListaSeparacao: string;

    TipoImpressoraOrcamento: string;
    ImpressoraOrcamento: string;
    AbrirPreviewOrcamento: string;

    TipoImpCompPagtoFinanceiro: string;
    ImpCompPagtoFinanceiro: string;
    AbrirPrevCompPagtoFinanc: string;

    TipoImpressoraCompDevolucao: string;
    ImpressoraCompDevolucao: string;
    AbrirPrevCompDevolucao: string;

    TipoImpressoraCompCaixa: string;
    ImpressoraCompCaixa: string;
    AbrirPrevCompCaixa: string;

    TipoImpressoraCtzPromo: string;
    ImpressoraCtzPromo: string;
    AbrirPrevCtzPromo: string;

    ModeloBalanca: string;
    PortaBalanca: string
  end;

  TImpressorasUsuarios = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecImpressorasUsuarios: RecImpressorasUsuarios;
  end;

function BuscarImpressoras(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecImpressorasUsuarios>;

function AtualizarImpressoras(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pFuncionarioId: Integer;

  pTipoImpressoraNFe: string;
  pImpressoraNFe: string;
  pAbrirPreviewNFe: string;

  pTipoImpressoraNFCe: string;
  pImpressoraNFCe: string;
  pAbrirPreviewNFCe: string;

  pTipoImpressoraCompPagamento: string;
  pImpressoraCompPagamento: string;
  pAbrirPreviewCompPagamento: string;

  pTipoImpressoraComproEntrega: string;
  pImpressoraComproEntrega: string;
  pAbrirPreviewComproEntrega: string;

  pTipoImpressoraListaSeparac: string;
  pImpressoraListaSeparac: string;
  pAbrirPreviewListaSeparacao: string;

  pTipoImpressoraOrcamento: string;
  pImpressoraOrcamento: string;
  pAbrirPreviewOrcamento: string;

  pTipoImpCompPagtoFinanceiro: string;
  pImpCompPagtoFinanceiro: string;
  pAbrirPrevCompPagtoFinanc: string;

  pTipoImpressoraCompDevolucao: string;
  pImpressoraCompDevolucao: string;
  pAbrirPrevCompDevolucao: string;

  pTipoImpressoraCompCaixa: string;
  pImpressoraCompCaixa: string;
  pAbrirPrevCompCaixa: string;

  pModeloBalanca: string;
  pPortaBalanca: string;

  pTipoImpressoraCartazPromocional: string;
  pCaminhoImpressoraCartazPromocional: string;
  pAbrirPreviewCartazPromocional: string

): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TImpressorasUsuarios }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where EMPRESA_ID = :P1 ' +
      'and USUARIO_ID = :P2 '
    );
end;

constructor TImpressorasUsuarios.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'IMPRESSORAS_USUARIOS');

  FSql :=
    'select ' +
    '  EMPRESA_ID, ' +
    '  USUARIO_ID, ' +
    '  TIPO_IMPRESSORA_NFE, ' +
    '  ABRIR_PREVIEW_NFE, ' +
    '  IMPRESSORA_NFE, ' +
    '  TIPO_IMPRESSORA_NFCE, ' +
    '  ABRIR_PREVIEW_NFCE, ' +
    '  IMPRESSORA_NFCE, ' +
    '  TIPO_IMPRESSORA_COMP_PAGAMENTO, ' +
    '  ABRIR_PREVIEW_COMP_PAGAMENTO, ' +
    '  IMPRESSORA_COMP_PAGAMENTO, ' +
    '  TIPO_IMPRESSORA_COMPRO_ENTREGA, ' +
    '  ABRIR_PREVIEW_COMPRO_ENTREGA, ' +
    '  IMPRESSORA_COMPRO_ENTREGA, ' +
    '  TIPO_IMPRESSORA_LISTA_SEPARAC, ' +
    '  ABRIR_PREVIEW_LISTA_SEPARACAO, ' +
    '  IMPRESSORA_LISTA_SEPARAC, ' +
    '  TIPO_IMPRESSORA_ORCAMENTO, ' +
    '  ABRIR_PREVIEW_ORCAMENTO, ' +
    '  IMPRESSORA_ORCAMENTO, ' +
    '  TIPO_IMP_COMP_PAGTO_FINANCEIRO, ' +
    '  IMP_COMP_PAGTO_FINANCEIRO, ' +
    '  ABRIR_PREV_COMP_PAGTO_FINANC, ' +
    '  TIPO_IMPRESSORA_COMP_DEVOLUCAO, ' +
    '  IMPRESSORA_COMP_DEVOLUCAO, ' +
    '  ABRIR_PREV_COMP_DEVOLUCAO, ' +
    '  TIPO_IMPRESSORA_COMP_CAIXA, ' +
    '  IMPRESSORA_COMP_CAIXA, ' +
    '  ABRIR_PREV_COMP_CAIXA, ' +

    '  TIPO_IMPRESSORA_CTZ_PROMO, ' +
    '  IMPRESSORA_CTZ_PROMO, ' +
    '  ABRIR_PREV_CTZ_PROMO, ' +

    '  MODELO_BALANCA, '+
    '  PORTA_BALANCA '+
    'from ' +
    '  IMPRESSORAS_USUARIOS ';

  setFiltros(getFiltros);

  AddColuna('EMPRESA_ID', True);
  AddColuna('USUARIO_ID', True);
  AddColuna('TIPO_IMPRESSORA_NFE');
  AddColuna('IMPRESSORA_NFE');
  AddColuna('ABRIR_PREVIEW_NFE');
  AddColuna('TIPO_IMPRESSORA_NFCE');
  AddColuna('IMPRESSORA_NFCE');
  AddColuna('ABRIR_PREVIEW_NFCE');
  AddColuna('TIPO_IMPRESSORA_COMP_PAGAMENTO');
  AddColuna('IMPRESSORA_COMP_PAGAMENTO');
  AddColuna('ABRIR_PREVIEW_COMP_PAGAMENTO');

  AddColuna('TIPO_IMPRESSORA_COMPRO_ENTREGA');
  AddColuna('IMPRESSORA_COMPRO_ENTREGA');
  AddColuna('ABRIR_PREVIEW_COMPRO_ENTREGA');

  AddColuna('TIPO_IMPRESSORA_LISTA_SEPARAC');
  AddColuna('IMPRESSORA_LISTA_SEPARAC');
  AddColuna('ABRIR_PREVIEW_LISTA_SEPARACAO');

  AddColuna('TIPO_IMPRESSORA_ORCAMENTO');
  AddColuna('IMPRESSORA_ORCAMENTO');
  AddColuna('ABRIR_PREVIEW_ORCAMENTO');

  AddColuna('TIPO_IMP_COMP_PAGTO_FINANCEIRO');
  AddColuna('IMP_COMP_PAGTO_FINANCEIRO');
  AddColuna('ABRIR_PREV_COMP_PAGTO_FINANC');

  AddColuna('TIPO_IMPRESSORA_COMP_DEVOLUCAO');
  AddColuna('IMPRESSORA_COMP_DEVOLUCAO');
  AddColuna('ABRIR_PREV_COMP_DEVOLUCAO');

  AddColuna('TIPO_IMPRESSORA_COMP_CAIXA');
  AddColuna('IMPRESSORA_COMP_CAIXA');
  AddColuna('ABRIR_PREV_COMP_CAIXA');

  AddColuna('TIPO_IMPRESSORA_CTZ_PROMO');
  AddColuna('IMPRESSORA_CTZ_PROMO');
  AddColuna('ABRIR_PREV_CTZ_PROMO');

  AddColuna('MODELO_BALANCA');
  AddColuna('PORTA_BALANCA');

end;

function TImpressorasUsuarios.getRecImpressorasUsuarios: RecImpressorasUsuarios;
begin
  Result.EmpresaId                   := getInt('EMPRESA_ID', True);
  Result.UsuarioId                   := getInt('USUARIO_ID', True);

  Result.TipoImpressoraNFe           := getString('TIPO_IMPRESSORA_NFE');
  Result.ImpressoraNFe               := getString('IMPRESSORA_NFE');
  Result.AbrirPreviewNFe             := getString('ABRIR_PREVIEW_NFE');

  Result.TipoImpressoraNFCe          := getString('TIPO_IMPRESSORA_NFCE');
  Result.ImpressoraNFCe              := getString('IMPRESSORA_NFCE');
  Result.AbrirPreviewNFCe            := getString('ABRIR_PREVIEW_NFCE');

  Result.TipoImpressoraCompPagamento := getString('TIPO_IMPRESSORA_COMP_PAGAMENTO');
  Result.ImpressoraCompPagamento     := getString('IMPRESSORA_COMP_PAGAMENTO');
  Result.AbrirPreviewCompPagamento   := getString('ABRIR_PREVIEW_COMP_PAGAMENTO');

  Result.TipoImpressoraComproEntrega := getString('TIPO_IMPRESSORA_COMPRO_ENTREGA');
  Result.ImpressoraComproEntrega     := getString('IMPRESSORA_COMPRO_ENTREGA');
  Result.AbrirPreviewComproEntrega   := getString('ABRIR_PREVIEW_COMPRO_ENTREGA');

  Result.TipoImpressoraListaSeparac  := getString('TIPO_IMPRESSORA_LISTA_SEPARAC');
  Result.ImpressoraListaSeparac      := getString('IMPRESSORA_LISTA_SEPARAC');
  Result.AbrirPreviewListaSeparacao  := getString('ABRIR_PREVIEW_LISTA_SEPARACAO');

  Result.TipoImpressoraOrcamento     := getString('TIPO_IMPRESSORA_ORCAMENTO');
  Result.ImpressoraOrcamento         := getString('IMPRESSORA_ORCAMENTO');
  Result.AbrirPreviewOrcamento       := getString('ABRIR_PREVIEW_ORCAMENTO');

  Result.TipoImpCompPagtoFinanceiro  := getString('TIPO_IMP_COMP_PAGTO_FINANCEIRO');
  Result.ImpCompPagtoFinanceiro      := getString('IMP_COMP_PAGTO_FINANCEIRO');
  Result.AbrirPrevCompPagtoFinanc    := getString('ABRIR_PREV_COMP_PAGTO_FINANC');

  Result.TipoImpressoraCompDevolucao := getString('TIPO_IMPRESSORA_COMP_DEVOLUCAO');
  Result.ImpressoraCompDevolucao     := getString('IMPRESSORA_COMP_DEVOLUCAO');
  Result.AbrirPrevCompDevolucao      := getString('ABRIR_PREV_COMP_DEVOLUCAO');

  Result.TipoImpressoraCompCaixa     := getString('TIPO_IMPRESSORA_COMP_CAIXA');
  Result.ImpressoraCompCaixa         := getString('IMPRESSORA_COMP_CAIXA');
  Result.AbrirPrevCompCaixa          := getString('ABRIR_PREV_COMP_CAIXA');

  Result.TipoImpressoraCtzPromo      := getString('TIPO_IMPRESSORA_CTZ_PROMO');
  Result.ImpressoraCtzPromo          := getString('IMPRESSORA_CTZ_PROMO');
  Result.AbrirPrevCtzPromo           := getString('ABRIR_PREV_CTZ_PROMO');

  Result.ModeloBalanca               := getString('MODELO_BALANCA');
  Result.PortaBalanca                := getString('PORTA_BALANCA');
end;

function BuscarImpressoras(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecImpressorasUsuarios>;
var
  i: Integer;
  t: TImpressorasUsuarios;
begin
  Result := nil;
  t := TImpressorasUsuarios.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecImpressorasUsuarios;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function AtualizarImpressoras(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pFuncionarioId: Integer;
  pTipoImpressoraNFe: string;
  pImpressoraNFe: string;
  pAbrirPreviewNFe: string;
  pTipoImpressoraNFCe: string;
  pImpressoraNFCe: string;
  pAbrirPreviewNFCe: string;
  pTipoImpressoraCompPagamento: string;
  pImpressoraCompPagamento: string;
  pAbrirPreviewCompPagamento: string;
  pTipoImpressoraComproEntrega: string;
  pImpressoraComproEntrega: string;
  pAbrirPreviewComproEntrega: string;
  pTipoImpressoraListaSeparac: string;
  pImpressoraListaSeparac: string;
  pAbrirPreviewListaSeparacao: string;
  pTipoImpressoraOrcamento: string;
  pImpressoraOrcamento: string;
  pAbrirPreviewOrcamento: string;

  pTipoImpCompPagtoFinanceiro: string;
  pImpCompPagtoFinanceiro: string;
  pAbrirPrevCompPagtoFinanc: string;

  pTipoImpressoraCompDevolucao: string;
  pImpressoraCompDevolucao: string;
  pAbrirPrevCompDevolucao: string;

  pTipoImpressoraCompCaixa: string;
  pImpressoraCompCaixa: string;
  pAbrirPrevCompCaixa: string;

  pModeloBalanca: string;
  pPortaBalanca: string;

  pTipoImpressoraCartazPromocional: string;
  pCaminhoImpressoraCartazPromocional: string;
  pAbrirPreviewCartazPromocional: string
): RecRetornoBD;
var
  t: TImpressorasUsuarios;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_IMP_ESTACOES');

  t := TImpressorasUsuarios.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('EMPRESA_ID', pEmpresaId, True);
    t.setInt('USUARIO_ID', pFuncionarioId, True);

    t.setString('TIPO_IMPRESSORA_NFE', pTipoImpressoraNFe);
    t.setString('IMPRESSORA_NFE', pImpressoraNFe);
    t.setString('ABRIR_PREVIEW_NFE', pAbrirPreviewNFe);

    t.setString('TIPO_IMPRESSORA_NFCE', pTipoImpressoraNFCe);
    t.setString('IMPRESSORA_NFCE', pImpressoraNFCe);
    t.setString('ABRIR_PREVIEW_NFCE', pAbrirPreviewNFCe);

    t.setString('TIPO_IMPRESSORA_COMP_PAGAMENTO', pTipoImpressoraCompPagamento);
    t.setString('IMPRESSORA_COMP_PAGAMENTO', pImpressoraCompPagamento);
    t.setString('ABRIR_PREVIEW_COMP_PAGAMENTO', pAbrirPreviewCompPagamento);

    t.setString('TIPO_IMPRESSORA_COMPRO_ENTREGA', pTipoImpressoraComproEntrega);
    t.setString('IMPRESSORA_COMPRO_ENTREGA', pImpressoraComproEntrega);
    t.setString('ABRIR_PREVIEW_COMPRO_ENTREGA', pAbrirPreviewComproEntrega);

    t.setString('TIPO_IMPRESSORA_LISTA_SEPARAC', pTipoImpressoraListaSeparac);
    t.setString('IMPRESSORA_LISTA_SEPARAC', pImpressoraListaSeparac);
    t.setString('ABRIR_PREVIEW_LISTA_SEPARACAO', pAbrirPreviewListaSeparacao);

    t.setString('TIPO_IMPRESSORA_ORCAMENTO', pTipoImpressoraOrcamento);
    t.setString('IMPRESSORA_ORCAMENTO', pImpressoraOrcamento);
    t.setString('ABRIR_PREVIEW_ORCAMENTO', pAbrirPreviewOrcamento);

    t.setString('TIPO_IMP_COMP_PAGTO_FINANCEIRO', pTipoImpCompPagtoFinanceiro);
    t.setString('IMP_COMP_PAGTO_FINANCEIRO', pImpCompPagtoFinanceiro);
    t.setString('ABRIR_PREV_COMP_PAGTO_FINANC', pAbrirPrevCompPagtoFinanc);

    t.setString('TIPO_IMPRESSORA_COMP_DEVOLUCAO', pTipoImpressoraCompDevolucao);
    t.setString('IMPRESSORA_COMP_DEVOLUCAO', pImpressoraCompDevolucao);
    t.setString('ABRIR_PREV_COMP_DEVOLUCAO', pAbrirPrevCompDevolucao);

    t.setString('TIPO_IMPRESSORA_COMP_CAIXA', pTipoImpressoraCompCaixa);
    t.setString('IMPRESSORA_COMP_CAIXA', pImpressoraCompCaixa);
    t.setString('ABRIR_PREV_COMP_CAIXA', pAbrirPrevCompCaixa);

    t.setString('TIPO_IMPRESSORA_CTZ_PROMO', pTipoImpressoraCartazPromocional);
    t.setString('IMPRESSORA_CTZ_PROMO', pCaminhoImpressoraCartazPromocional);
    t.setString('ABRIR_PREV_CTZ_PROMO', pAbrirPreviewCartazPromocional);

    t.setString('MODELO_BALANCA', pModeloBalanca);
    t.setString('PORTA_BALANCA', pPortaBalanca);


    // Limpa se houver e depois insere novamente
    t.Excluir;

    t.Inserir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
