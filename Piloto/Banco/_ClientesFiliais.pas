unit _ClientesFiliais;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  Rec_ClientesFiliais = record
    CadastroId: Integer;
    CpfCnpj: string;
  end;

  T_ClientesFiliais = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecord_ClientesFiliais: Rec_ClientesFiliais;
  end;

function Atualizar_ClientesFiliais(
  pConexao: TConexao;
  pCadastroId: Integer;
  pClienteFilial : TArray<Rec_ClientesFiliais>
): RecRetornoBD;

function Buscar_ClientesFiliais(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<Rec_ClientesFiliais>;

function Excluir_ClientesFiliais(
  pConexao: TConexao;
  pCadastroId: Integer;
  pCpfCnpj: string
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ T_ClientesFiliais }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CADASTRO_ID = :P1'
    );
end;

constructor T_ClientesFiliais.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CLIENTES_FILIAIS');

  FSql :=
    'select ' +
    '  CADASTRO_ID, ' +
    '  CPF_CNPJ ' +
    'from ' +
    '  CLIENTES_FILIAIS';

  setFiltros(getFiltros);

  AddColuna('CADASTRO_ID', True);
  AddColuna('CPF_CNPJ', True);
end;

function T_ClientesFiliais.getRecord_ClientesFiliais: Rec_ClientesFiliais;
begin
  Result.CadastroId                 := getInt('CADASTRO_ID', True);
  Result.CpfCnpj                    := getString('CPF_CNPJ', True);
end;

function Atualizar_ClientesFiliais(
  pConexao: TConexao;
  pCadastroId: Integer;
  pClienteFilial : TArray<Rec_ClientesFiliais>
): RecRetornoBD;
var
  t: T_ClientesFiliais;
  vCnt: Integer;
  vExecucao: TExecucao;
begin
  Result.Iniciar;

  t := T_ClientesFiliais.Create(pConexao);
  vExecucao := TExecucao.Create(pConexao);


  try
    vExecucao.Add('DELETE CLIENTES_FILIAIS ');
    vExecucao.Add('where CADASTRO_ID = :P1');
    vExecucao.Executar([pCadastroId]);

    t.setInt('CADASTRO_ID', pCadastroId, True);
    for vCnt := Low(pClienteFilial) to High(pClienteFilial) do
    begin
      t.setString('CPF_CNPJ', pClienteFilial[vCnt].CpfCnpj, True);

      t.Inserir
    end;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  vExecucao.Free;
  t.Free;
end;

function Buscar_ClientesFiliais(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<Rec_ClientesFiliais>;
var
  i: Integer;
  t: T_ClientesFiliais;
begin
  Result := nil;
  t := T_ClientesFiliais.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecord_ClientesFiliais;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function Excluir_ClientesFiliais(
  pConexao: TConexao;
  pCadastroId: Integer;
  pCpfCnpj: string
): RecRetornoBD;
var
  t: T_ClientesFiliais;
begin
  Result.Iniciar;
  t := T_ClientesFiliais.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('CADASTRO_ID', pCadastroId, True);
    t.setString('CPF_CNPJ', pCpfCnpj, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.

