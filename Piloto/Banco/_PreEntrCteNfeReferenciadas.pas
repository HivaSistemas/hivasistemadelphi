unit _PreEntrCteNfeReferenciadas;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecPreEntrCteNfeReferenciadas = record
    PreEntradaId: Integer;
    ItemId: Integer;
    ChaveAcessoNfe: string;

    PreEntradaIdNfeRef: Integer;
    NumeroNotaNFeRef: Integer;
    SerieNotaNfeRef: string;
    CnpjEmitenteNFeRef: string;
    RazaoSocialEmienteNfeRef: string;
    DataHoraEmissaoNFeRef: TDateTime;

    ValorTotalNotaNFeRef: Double;
  end;

  TPreEntrCteNfeReferenciadas = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordPreEntrCteNfeReferenciadas: RecPreEntrCteNfeReferenciadas;
  end;

function BuscarPreEntrCteNfeReferenciadas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecPreEntrCteNfeReferenciadas>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TPreEntrCteNfeReferenciadas }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where REF.PRE_ENTRADA_ID = :P1'
    );
end;

constructor TPreEntrCteNfeReferenciadas.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PRE_ENTR_CTE_NFE_REFERENCIADAS');

  FSql := 
    'select ' +
    '  REF.PRE_ENTRADA_ID, ' +
    '  REF.ITEM_ID, ' +
    '  REF.CHAVE_ACESSO_NFE, ' +
    '  PRE.PRE_ENTRADA_ID as PRE_ENTRADA_ID_NFE_REF, ' +
    '  PRE.NUMERO_NOTA as NUMERO_NOTA_NFE_REF, ' +
    '  PRE.SERIE_NOTA as SERIE_NOTA_NFE_REF, ' +
    '  PRE.CNPJ as CNPJ_EMITENTE_NFE_REF, ' +
    '  PRE.RAZAO_SOCIAL as RAZAO_SOCIAL_EMITENTE_NFE_REF, ' +
    '  PRE.DATA_HORA_EMISSAO as DATA_HORA_EMISSAO_NFE_REF, ' +
    '  PRE.VALOR_TOTAL_NOTA as VALOR_TOTAL_NOTA_NFE_REF ' +
    'from ' +
    '  PRE_ENTR_CTE_NFE_REFERENCIADAS REF ' +

    'left join PRE_ENTRADAS_NOTAS_FISCAIS PRE ' +
    'on REF.CHAVE_ACESSO_NFE = PRE.CHAVE_ACESSO_NFE ';

  setFiltros(getFiltros);

  AddColuna('PRE_ENTRADA_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('CHAVE_ACESSO_NFE');
  AddColunaSL('PRE_ENTRADA_ID_NFE_REF');
  AddColunaSL('NUMERO_NOTA_NFE_REF');
  AddColunaSL('SERIE_NOTA_NFE_REF');
  AddColunaSL('CNPJ_EMITENTE_NFE_REF');
  AddColunaSL('RAZAO_SOCIAL_EMITENTE_NFE_REF');
  AddColunaSL('DATA_HORA_EMISSAO_NFE_REF');
  AddColunaSL('VALOR_TOTAL_NOTA_NFE_REF');
end;

function TPreEntrCteNfeReferenciadas.getRecordPreEntrCteNfeReferenciadas: RecPreEntrCteNfeReferenciadas;
begin
  Result.PreEntradaId    := getInt('PRE_ENTRADA_ID', True);
  Result.ItemId          := getInt('ITEM_ID', True);
  Result.ChaveAcessoNfe  := getString('CHAVE_ACESSO_NFE');

  Result.PreEntradaIdNfeRef        := getInt('PRE_ENTRADA_ID_NFE_REF');
  Result.NumeroNotaNFeRef          := getInt('NUMERO_NOTA_NFE_REF');
  Result.SerieNotaNfeRef           := getString('SERIE_NOTA_NFE_REF');
  Result.CnpjEmitenteNFeRef        := getString('CNPJ_EMITENTE_NFE_REF');
  Result.RazaoSocialEmienteNfeRef  := getString('RAZAO_SOCIAL_EMITENTE_NFE_REF');
  Result.DataHoraEmissaoNFeRef     := getData('DATA_HORA_EMISSAO_NFE_REF');
  Result.ValorTotalNotaNFeRef      := getDouble('VALOR_TOTAL_NOTA_NFE_REF');
end;

function BuscarPreEntrCteNfeReferenciadas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecPreEntrCteNfeReferenciadas>;
var
  i: Integer;
  t: TPreEntrCteNfeReferenciadas;
begin
  Result := nil;
  t := TPreEntrCteNfeReferenciadas.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordPreEntrCteNfeReferenciadas;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
