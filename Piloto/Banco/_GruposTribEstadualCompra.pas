unit _GruposTribEstadualCompra;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _GruposTribEstadualCompEst;

{$M+}
type
  RecGruposTribEstadualCompra = class(TObject)
  public
    GrupoTribEstadualCompraId: Integer;
    Descricao: string;
    Ativo: string;
  end;

  TGruposTribEstadualCompra = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordGruposTribEstadualCompra: RecGruposTribEstadualCompra;
  end;

function AtualizarGruposTribEstadualCompra(
  pConexao: TConexao;
  pGrupoTribEstadualCompraId: Integer;
  pDescricao: string;
  pAtivo: string;
  pGruposEstados: TArray<RecGruposTribEstadualCompEst>
): RecRetornoBD;

function BuscarGruposTribEstadualCompra(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecGruposTribEstadualCompra>;

function ExcluirGruposTribEstadualCompra(
  pConexao: TConexao;
  pGrupoTribEstadualCompraId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TGruposTribEstadualCompra }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where GRUPO_TRIB_ESTADUAL_COMPRA_ID = :P1 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Descri��o (Avan�ado)',
      True,
      0,
      'where DESCRICAO like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  GRUPO_TRIB_ESTADUAL_COMPRA_ID',
      '',
      True
    );
end;

constructor TGruposTribEstadualCompra.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'GRUPOS_TRIB_ESTADUAL_COMPRA');

  FSql := 
    'select ' +
    '  GRUPO_TRIB_ESTADUAL_COMPRA_ID, ' +
    '  DESCRICAO, ' +
    '  ATIVO ' +
    'from ' +
    '  GRUPOS_TRIB_ESTADUAL_COMPRA';

  setFiltros(getFiltros);

  AddColuna('GRUPO_TRIB_ESTADUAL_COMPRA_ID', True);
  AddColuna('DESCRICAO');
  AddColuna('ATIVO');
end;

function TGruposTribEstadualCompra.getRecordGruposTribEstadualCompra: RecGruposTribEstadualCompra;
begin
  Result := RecGruposTribEstadualCompra.Create;

  Result.GrupoTribEstadualCompraId  := getInt('GRUPO_TRIB_ESTADUAL_COMPRA_ID', True);
  Result.Descricao                  := getString('DESCRICAO');
  Result.Ativo                      := getString('ATIVO');
end;

function AtualizarGruposTribEstadualCompra(
  pConexao: TConexao;
  pGrupoTribEstadualCompraId: Integer;
  pDescricao: string;
  pAtivo: string;
  pGruposEstados: TArray<RecGruposTribEstadualCompEst>
): RecRetornoBD;
var
  t: TGruposTribEstadualCompra;

  i: Integer;
  vNovo: Boolean;
  vSeq: TSequencia;
  vExec: TExecucao;
  vEstados: TGruposTribEstadualCompEst;
begin
  Result.Iniciar;
  pConexao.setRotina('ATUALIZAR_GRU_TRIB_EST_COMPRA');

  t := TGruposTribEstadualCompra.Create(pConexao);
  vExec := TExecucao.Create(pConexao);
  vEstados := TGruposTribEstadualCompEst.Create(pConexao);

  vNovo := pGrupoTribEstadualCompraId = 0;
  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_GRUPOS_TRIB_EST_COMPRA');
    pGrupoTribEstadualCompraId := vSeq.getProximaSequencia;
    Result.AsInt := pGrupoTribEstadualCompraId;
    vSeq.Free;
  end;

  try
    pConexao.IniciarTransacao; 

    t.setInt('GRUPO_TRIB_ESTADUAL_COMPRA_ID', pGrupoTribEstadualCompraId, True);
    t.setString('DESCRICAO', pDescricao);
    t.setString('ATIVO', pAtivo);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    vExec.Limpar;
    vExec.Add('delete from GRUPOS_TRIB_ESTADUAL_COMP_EST');
    vExec.Add('where GRUPO_TRIB_ESTADUAL_COMPRA_ID = :P1 ');
    vExec.Executar([pGrupoTribEstadualCompraId]);

    for i := Low(pGruposEstados) to High(pGruposEstados) do begin
      vEstados.setInt('GRUPO_TRIB_ESTADUAL_COMPRA_ID', pGrupoTribEstadualCompraId, True);
      vEstados.setString('ESTADO_ID', pGruposEstados[i].EstadoId, True);
      vEstados.setString('CST_REVENDA', pGruposEstados[i].CstRevenda);
      vEstados.setString('CST_DISTRIBUIDORA', pGruposEstados[i].CstDistribuidora);
      vEstados.setString('CST_INDUSTRIA', pGruposEstados[i].CstIndustria);
      vEstados.setDouble('INDICE_REDUCAO_BASE_ICMS', pGruposEstados[i].IndiceReducaoBaseIcms);
      vEstados.setDouble('IVA', pGruposEstados[i].Iva);
      vEstados.setDouble('PRECO_PAUTA', pGruposEstados[i].PrecoPauta);

      vEstados.Inserir;
    end;

    pConexao.FinalizarTransacao; 
  except
    on e: Exception do begin
      pConexao.VoltarTransacao; 
      Result.TratarErro(e);
    end;
  end;

  vEstados.Free;
  vExec.Free;
  t.Free;
end;

function BuscarGruposTribEstadualCompra(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecGruposTribEstadualCompra>;
var
  i: Integer;
  t: TGruposTribEstadualCompra;
begin
  Result := nil;
  t := TGruposTribEstadualCompra.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordGruposTribEstadualCompra;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirGruposTribEstadualCompra(
  pConexao: TConexao;
  pGrupoTribEstadualCompraId: Integer
): RecRetornoBD;
var
  t: TGruposTribEstadualCompra;
begin
  Result.Iniciar;
  t := TGruposTribEstadualCompra.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('GRUPO_TRIB_ESTADUAL_COMPRA_ID', pGrupoTribEstadualCompraId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
