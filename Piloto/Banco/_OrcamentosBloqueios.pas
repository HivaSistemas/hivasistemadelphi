unit _OrcamentosBloqueios;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsOrcamentosVendas;

{$M+}
type
  TOrcamentosBloqueios = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordOrcamentosBloqueios: RecOrcamentosBloqueios;
  end;

function AtualizarOrcamentosBloqueios(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pBloqueios: TArray<RecOrcamentosBloqueios>
): RecRetornoBD;

function BuscarOrcamentosBloqueios(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecOrcamentosBloqueios>;

function BuscarOrcamentosBloqueiosComando(pConexao: TConexao; pComando: string): TArray<RecOrcamentosBloqueios>;

function Desbloquear(
  pConexao: TConexao;
  pPercentualDescontoAplicado: Double;
  pGravarNoOrcamentoDesbloqueio: Boolean;
  const pUsuarioLiberacaoId: Integer;
  const pOrcamentoId: Integer;
  const pItensIds: TArray<Integer>
): RecRetornoBD;

function ExcluirOrcamentosBloqueios(
  pConexao: TConexao
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TTOrcamentosBloqueios }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ORCAMENTO_ID = :P1'
    );
end;

constructor TOrcamentosBloqueios.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ORCAMENTOS_BLOQUEIOS');

  FSql :=
    'select ' +
    '  ORB.ORCAMENTO_ID, ' +
    '  ORB.ITEM_ID, ' +
    '  ORB.DESCRICAO, ' +
    '  ORB.DATA_HORA_LIBERACAO, ' +
    '  ORB.USUARIO_LIBERACAO_ID, ' +
    '  FUN.NOME as NOME_USUARIO_LIBERACAO ' +
    'from ' +
    '  ORCAMENTOS_BLOQUEIOS ORB ' +

    'left join FUNCIONARIOS FUN ' +
    'on ORB.USUARIO_LIBERACAO_ID = FUN.FUNCIONARIO_ID ';

  setFiltros(getFiltros);

  AddColuna('ORCAMENTO_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('DESCRICAO');
  AddColunaSL('DATA_HORA_LIBERACAO');
  AddColunaSL('USUARIO_LIBERACAO_ID');
  AddColunaSL('NOME_USUARIO_LIBERACAO');
end;

function TOrcamentosBloqueios.getRecordOrcamentosBloqueios: RecOrcamentosBloqueios;
begin
  Result.OrcamentoId          := getInt('ORCAMENTO_ID', True);
  Result.ItemId               := getInt('ITEM_ID', True);
  Result.Descricao            := getString('DESCRICAO');
  Result.DataHoraLiberacao    := getData('DATA_HORA_LIBERACAO');
  Result.UsuarioLiberacaoId   := getInt('USUARIO_LIBERACAO_ID');
  Result.NomeUsuarioLiberacao := getString('NOME_USUARIO_LIBERACAO');
end;

function AtualizarOrcamentosBloqueios(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pBloqueios: TArray<RecOrcamentosBloqueios>
): RecRetornoBD;
var
  i: Integer;
  vExec: TExecucao;
  t: TOrcamentosBloqueios;
begin
  Result.TeveErro := False;
  vExec := TExecucao.Create(pConexao);
  t := TOrcamentosBloqueios.Create(pConexao);
  try
    vExec.SQL.Add('delete from ORCAMENTOS_BLOQUEIOS where ORCAMENTO_ID = :P1');
    vExec.Executar([pOrcamentoId]);

    t.setInt('ORCAMENTO_ID', pOrcamentoId, True);
    for i := Low(pBloqueios) to High(pBloqueios) do begin
      t.setInt('ITEM_ID', pBloqueios[i].ItemId, True);
      t.setString('DESCRICAO', pBloqueios[i].Descricao);

      t.Inserir;
    end;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vExec.Free;
  t.Free;
end;

function BuscarOrcamentosBloqueios(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecOrcamentosBloqueios>;
var
  i: Integer;
  t: TOrcamentosBloqueios;
begin
  Result := nil;
  t := TOrcamentosBloqueios.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordOrcamentosBloqueios;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarOrcamentosBloqueiosComando(pConexao: TConexao; pComando: string): TArray<RecOrcamentosBloqueios>;
var
  i: Integer;
  t: TOrcamentosBloqueios;
begin
  Result := nil;
  t := TOrcamentosBloqueios.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordOrcamentosBloqueios;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirOrcamentosBloqueios(
  pConexao: TConexao
): RecRetornoBD;
var
  t: TOrcamentosBloqueios;
begin
  Result.TeveErro := False;
  t := TOrcamentosBloqueios.Create(pConexao);

  try

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function Desbloquear(
  pConexao: TConexao;
  pPercentualDescontoAplicado: Double;
  pGravarNoOrcamentoDesbloqueio: Boolean;
  const pUsuarioLiberacaoId: Integer;
  const pOrcamentoId: Integer;
  const pItensIds: TArray<Integer>
): RecRetornoBD;
var
  pCons: TConsulta;
  vExec: TExecucao;
begin
  Result.TeveErro := False;
  pCons := TConsulta.Create(pConexao);
  vExec := TExecucao.Create(pConexao);
  try
    pConexao.IniciarTransacao;

    vExec.SQL.Add('update ORCAMENTOS_BLOQUEIOS set ');
    vExec.SQL.Add('  USUARIO_LIBERACAO_ID = :P1, ');
    vExec.SQL.Add('  DATA_HORA_LIBERACAO = sysdate ');
    vExec.SQL.Add('where ORCAMENTO_ID = :P2 ');
    vExec.SQL.Add('and ' + FiltroInInt('ITEM_ID', pItensIds));

    vExec.Executar([pUsuarioLiberacaoId, pOrcamentoId]);

    pCons.SQL.Add('select count(*) ');
    pCons.SQL.Add('from ORCAMENTOS_BLOQUEIOS ');
    pCons.SQL.Add('where ORCAMENTO_ID = :P1 ');
    pCons.SQL.Add('and USUARIO_LIBERACAO_ID is null ');

    if pCons.Pesquisar([pOrcamentoId]) then begin
      if pCons.GetInt(0) = 0 then begin
        Result.AsString := 'Liberado';

        vExec.SQL.Clear;
        vExec.SQL.Add('update ORCAMENTOS set ');
        vExec.SQL.Add('  STATUS = case when STATUS = ''OB'' then ''OE'' else ''VR'' end ');
        vExec.SQL.Add('where ORCAMENTO_ID = :P1 ');

        vExec.Executar([pOrcamentoId]);
      end;
    end;

    if pGravarNoOrcamentoDesbloqueio then begin
      vExec.SQL.Clear;
      vExec.SQL.Add('update ORCAMENTOS set ');
      vExec.SQL.Add('  PEDIDO_LIBERADO = :P2, PERCENTUAL_LIBERADO = :P3 ');
      vExec.SQL.Add('where ORCAMENTO_ID = :P1 ');

      vExec.Executar([pOrcamentoId, 'S', pPercentualDescontoAplicado]);
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  pCons.Free;
  vExec.Free;
end;

end.
