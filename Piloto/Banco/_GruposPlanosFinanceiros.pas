unit _GruposPlanosFinanceiros;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils,
  _ItensGruposPlanosFinanceiros;

{$M+}
type
  RecGruposPlanosFinanceiros = class
  public
    GrupoFinanceiroId: string;
    Descricao: string;
    GrupoFinanceiroPaiId: string;
    PermiteAlteracao: string;
  end;

  TGruposPlanosFinanceiros = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordGruposPlanos: RecGruposPlanosFinanceiros;
  end;

function AtualizarGruposPlanosFinanceiros(
  pConexao: TConexao;
  pGrupoFinanceiroId: string;
  pDescricao: string;
  pGrupoFinanceiroPaiId: string;
  pItensGrupoFinanceiro: TArray<RecItensGruposPlanosFinanceiros>
): RecRetornoBD;

function BuscarGruposPlanosFinanceiros(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecGruposPlanosFinanceiros>;

function ExcluirGrupoPlanoFinanceiro(
  pConexao: TConexao;
  pGrupoFinanceiroId: string
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TPlanosFinanceiros }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result,5);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where GRUPO_FINANCEIRO_ID = :P1 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Descri��o ( avan�ado )',
      True,
      0,
      'where NOME like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  GRUPO_FINANCEIRO_ID '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'order by ' +
      '  GRUPO_FINANCEIRO_ID '
    );

  Result[3] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'C�digo grupo fin.( Avan�ado )',
      False,
      1,
      'where GRUPO_FINANCEIRO_ID like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  GRUPO_FINANCEIRO_ID '
    );

  Result[4] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      4,
      'where GRUPO_FINANCEIRO_PAI_ID  = :P1 '
    );

end;

constructor TGruposPlanosFinanceiros.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'GRUPOS_FINANCEIROS');

  FSql :=
    'select ' +
    '  GRUPO_FINANCEIRO_ID, ' +
    '  NOME, ' +
    '  GRUPO_FINANCEIRO_PAI_ID, ' +
    '  PERMITE_ALTERACAO ' +
    'from ' +
    '  GRUPOS_FINANCEIROS ';

  SetFiltros(GetFiltros);

  AddColuna('GRUPO_FINANCEIRO_ID', True);
  AddColuna('NOME');
  AddColuna('GRUPO_FINANCEIRO_PAI_ID');
  AddColunaSL('PERMITE_ALTERACAO');
end;

function TGruposPlanosFinanceiros.GetRecordGruposPlanos: RecGruposPlanosFinanceiros;
begin
  Result := RecGruposPlanosFinanceiros.Create;

  Result.GrupoFinanceiroId    := GetString('GRUPO_FINANCEIRO_ID', True);
  Result.Descricao            := GetString('NOME');
  Result.GrupoFinanceiroPaiId := GetString('GRUPO_FINANCEIRO_PAI_ID');
  Result.PermiteAlteracao     := GetString('PERMITE_ALTERACAO');
end;

function AtualizarGruposPlanosFinanceiros(
  pConexao: TConexao;
  pGrupoFinanceiroId: string;
  pDescricao: string;
  pGrupoFinanceiroPaiId: string;
  pItensGrupoFinanceiro: TArray<RecItensGruposPlanosFinanceiros>
): RecRetornoBD;
var
  i: Integer;
  vNovo: Boolean;
  vSql: TConsulta;
  vItens: TItensGruposPlanosFinanceiros;
  vExec: TExecucao;
  t: TGruposPlanosFinanceiros;
begin
  Result.Iniciar;

  vNovo := (pGrupoFinanceiroId = '');
  if vNovo then begin
    vSql := TConsulta.Create(pConexao);

    if pGrupoFinanceiroPaiId = '' then begin
      vSql.SQL.Add('select ');
      vSql.SQL.Add('    MAX(TO_NUMBER(CASE ');
      vSql.SQL.Add('        WHEN INSTR(grupo_financeiro_id, ''.'') > 0 THEN SUBSTR(grupo_financeiro_id, 1, INSTR(grupo_financeiro_id, ''.'') - 1) ');
      vSql.SQL.Add('        ELSE grupo_financeiro_id ');
      vSql.SQL.Add('    END)) + 1 AS subcadeia ');
      vSql.SQL.Add('from ');
      vSql.SQL.Add('  GRUPOS_FINANCEIROS ');
      vSql.Pesquisar;

      pGrupoFinanceiroId := vSql.GetString(0);
      if pGrupoFinanceiroId.IsEmpty then
        pGrupoFinanceiroId := '1';
    end
    else begin
      vSql.SQL.Add('SELECT LPAD(MAX(TO_NUMBER(SUBSTR(GRUPO_FINANCEIRO_ID, 3, LENGTH(GRUPO_FINANCEIRO_ID)) )) + 1 ,2,''0'') AS SUBGRUPO ');
      vSql.SQL.Add(' FROM GRUPOS_FINANCEIROS G ');
      vSql.SQL.Add('WHERE G.GRUPO_FINANCEIRO_PAI_ID = :P1');
      vSql.Pesquisar([pGrupoFinanceiroPaiId]);

      pGrupoFinanceiroId := pGrupoFinanceiroPaiId + '.' + Iif(vSql.GetString(0).IsEmpty,'01',vSql.GetString(0));
    end;

    Result.AsString := pGrupoFinanceiroId;
    vSql.Active := False;
    vSql.Free;
  end;

  t := TGruposPlanosFinanceiros.Create(pConexao);
  vItens := TItensGruposPlanosFinanceiros.Create(pConexao);
  vExec := TExecucao.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setString('GRUPO_FINANCEIRO_ID', pGrupoFinanceiroId, True);
    t.setString('NOME', pDescricao);
    t.setString('GRUPO_FINANCEIRO_PAI_ID', pGrupoFinanceiroPaiId);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    vExec.Clear;
    vExec.Add('delete from ITENS_GRUPOS_FINANCEIROS ');
    vExec.Add('where GRUPO_FINANCEIRO_ID = :P1 ');
    vExec.Executar([pGrupoFinanceiroId]);

    vItens.setString('GRUPO_FINANCEIRO_ID', pGrupoFinanceiroId, True);
    for i := Low(pItensGrupoFinanceiro) to High(pItensGrupoFinanceiro) do begin
      vItens.setString('PLANO_FINANCEIRO_ID', pItensGrupoFinanceiro[i].PlanoFinanceiroId);
      vItens.setString('SQL', pItensGrupoFinanceiro[i].SQL);
      vItens.setInt('ORDEM', i);

      vItens.Inserir;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
  vItens.Free;
  vExec.Free;
end;

function BuscarGruposPlanosFinanceiros(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecGruposPlanosFinanceiros>;
var
  i: Integer;
  vSqlAuxiliar: string;
  t: TGruposPlanosFinanceiros;
begin
  Result := nil;
  t := TGruposPlanosFinanceiros.Create(pConexao);

  vSqlAuxiliar := '';

  if t.Pesquisar(pIndice, pFiltros, vSqlAuxiliar) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordGruposPlanos;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirGrupoPlanoFinanceiro(
  pConexao: TConexao;
  pGrupoFinanceiroId: string
): RecRetornoBD;
var
  t: TGruposPlanosFinanceiros;
  vExec: TExecucao;
begin
  pConexao.SetRotina('EXCLUIR_GRUPO_FIN');
  Result.TeveErro := False;
  t := TGruposPlanosFinanceiros.Create(pConexao);
  vExec := TExecucao.Create(pConexao);

  t.SetString('GRUPO_FINANCEIRO_ID', pGrupoFinanceiroId, True);


  try
    pConexao.IniciarTransacao;

    vExec.Clear;
    vExec.Add('delete from ITENS_GRUPOS_FINANCEIROS ');
    vExec.Add('where GRUPO_FINANCEIRO_ID = :P1 ');
    vExec.Executar([pGrupoFinanceiroId]);


    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
  t.Free;
end;

end.
