unit _TiposCobrancaDiasPrazo;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TTipoCobrancaDiasPrazo = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordTipoCobrancaDiasPrazo: RecTipoCobrancaDiasPrazo;
  end;

function BuscarTipoCobrancaDiasPrazos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTipoCobrancaDiasPrazo>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TTipoCobrancaDiasPrazo }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COBRANCA_ID = :P1 ' +
      'order by DIAS '
    );
end;

constructor TTipoCobrancaDiasPrazo.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'TIPO_COBRANCA_DIAS_PRAZO');

  FSql :=
    'select ' +
    '  COBRANCA_ID, ' +
    '  DIAS ' +
    'from ' +
    '  TIPO_COBRANCA_DIAS_PRAZO';

  setFiltros(getFiltros);

  AddColuna('COBRANCA_ID', True);
  AddColuna('DIAS', True);
end;

function TTipoCobrancaDiasPrazo.getRecordTipoCobrancaDiasPrazo: RecTipoCobrancaDiasPrazo;
begin
  Result.CobrancaId := getInt('COBRANCA_ID', True);
  Result.Dias        := getInt('DIAS', True);
end;

function BuscarTipoCobrancaDiasPrazos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTipoCobrancaDiasPrazo>;
var
  i: Integer;
  t: TTipoCobrancaDiasPrazo;
begin
  Result := nil;
  t := TTipoCobrancaDiasPrazo.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordTipoCobrancaDiasPrazo;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
