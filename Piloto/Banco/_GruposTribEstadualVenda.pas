unit _GruposTribEstadualVenda;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _GruposTribEstadualVendaEst;

{$M+}
type
  RecGruposTribEstadualVenda = class(TObject)
  public
    GrupoTribEstadualVendaId: Integer;
    Descricao: string;
    Ativo: string;
  end;

  TGruposTribEstadualVenda = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordGruposTribEstadualVenda: RecGruposTribEstadualVenda;
  end;

function AtualizarGruposTribEstadualVenda(
  pConexao: TConexao;
  pGrupoTribEstadualVendaId: Integer;
  pDescricao: string;
  pAtivo: string;
  pGruposEstados: TArray<RecGruposTribEstadualVendaEst>
): RecRetornoBD;

function BuscarGruposTribEstadualVenda(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecGruposTribEstadualVenda>;

function ExcluirGruposTribEstadualVenda(
  pConexao: TConexao;
  pGrupoTribEstadualVendaId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TGruposTribEstadualVenda }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where GRUPO_TRIB_ESTADUAL_VENDA_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Descri��o (Avan�ado)',
      True,
      0,
      'where DESCRICAO like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  GRUPO_TRIB_ESTADUAL_VENDA_ID ',
      '',
      True
    );
end;

constructor TGruposTribEstadualVenda.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'GRUPOS_TRIB_ESTADUAL_VENDA');

  FSql := 
    'select ' +
    '  GRUPO_TRIB_ESTADUAL_VENDA_ID, ' +
    '  DESCRICAO, ' +
    '  ATIVO ' +
    'from ' +
    '  GRUPOS_TRIB_ESTADUAL_VENDA';

  setFiltros(getFiltros);

  AddColuna('GRUPO_TRIB_ESTADUAL_VENDA_ID', True);
  AddColuna('DESCRICAO');
  AddColuna('ATIVO');
end;

function TGruposTribEstadualVenda.getRecordGruposTribEstadualVenda: RecGruposTribEstadualVenda;
begin
  Result := RecGruposTribEstadualVenda.Create;

  Result.GrupoTribEstadualVendaId   := getInt('GRUPO_TRIB_ESTADUAL_VENDA_ID', True);
  Result.Descricao                  := getString('DESCRICAO');
  Result.Ativo                      := getString('ATIVO');
end;

function AtualizarGruposTribEstadualVenda(
  pConexao: TConexao;
  pGrupoTribEstadualVendaId: Integer;
  pDescricao: string;
  pAtivo: string;
  pGruposEstados: TArray<RecGruposTribEstadualVendaEst>
): RecRetornoBD;
var
  t: TGruposTribEstadualVenda;

  i: Integer;
  vNovo: Boolean;
  vSeq: TSequencia;
  vExec: TExecucao;
  vGruposEst: TGruposTribEstadualVendaEst;
begin
  Result.Iniciar;
  pConexao.setRotina('ATU_GRUPO_TRIB_EST_VENDA');

  t := TGruposTribEstadualVenda.Create(pConexao);
  vExec := TExecucao.Create(pConexao);
  vGruposEst := TGruposTribEstadualVendaEst.Create(pConexao);

  vNovo := pGrupoTribEstadualVendaId = 0;
  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_GRUPOS_TRIB_EST_VENDA');
    pGrupoTribEstadualVendaId := vSeq.getProximaSequencia;
    Result.AsInt := pGrupoTribEstadualVendaId;
    vSeq.Free;
  end;

  try
    pConexao.IniciarTransacao; 

    t.setInt('GRUPO_TRIB_ESTADUAL_VENDA_ID', pGrupoTribEstadualVendaId, True);
    t.setString('DESCRICAO', pDescricao);
    t.setString('ATIVO', pAtivo);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    vExec.Limpar;
    vExec.Add('delete from GRUPOS_TRIB_ESTADUAL_VENDA_EST');
    vExec.Add('where GRUPO_TRIB_ESTADUAL_VENDA_ID = :P1 ');
    vExec.Executar([pGrupoTribEstadualVendaId]);

    for i := Low(pGruposEstados) to High(pGruposEstados) do begin
      vGruposEst.setInt('GRUPO_TRIB_ESTADUAL_VENDA_ID', pGrupoTribEstadualVendaId, True);
      vGruposEst.setString('ESTADO_ID', pGruposEstados[i].EstadoId, True);
      vGruposEst.setString('CST_NAO_CONTRIBUINTE', pGruposEstados[i].CstNaoContribuinte);
      vGruposEst.setString('CST_CONTRIBUINTE', pGruposEstados[i].CstContribuinte);
      vGruposEst.setString('CST_ORGAO_PUBLICO', pGruposEstados[i].CstOrgaoPublico);
      vGruposEst.setString('CST_REVENDA', pGruposEstados[i].CstRevenda);
      vGruposEst.setString('CST_CONSTRUTORA', pGruposEstados[i].CstConstrutora);
      vGruposEst.setString('CST_CLINICA_HOSPITAL', pGruposEstados[i].CstClinicaHospital);
      vGruposEst.setString('CST_PRODUTOR_RURAL', pGruposEstados[i].CstProdutorRural);
      vGruposEst.setDouble('INDICE_REDUCAO_BASE_ICMS', pGruposEstados[i].IndiceReducaoBaseIcms);
      vGruposEst.setDouble('PERCENTUAL_ICMS', pGruposEstados[i].PercentualIcms);
      vGruposEst.setDouble('PERCENTUAL_ICMS_INTER', pGruposEstados[i].PercentualIcmsInter);
      vGruposEst.setDouble('IVA', pGruposEstados[i].Iva);
      vGruposEst.setDouble('PRECO_PAUTA', pGruposEstados[i].PrecoPauta);

      vGruposEst.Inserir;
    end;


    pConexao.FinalizarTransacao; 
  except
    on e: Exception do begin
      pConexao.VoltarTransacao; 
      Result.TratarErro(e);
    end;
  end;

  vGruposEst.Free;
  vExec.Free;
  t.Free;
end;

function BuscarGruposTribEstadualVenda(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecGruposTribEstadualVenda>;
var
  i: Integer;
  t: TGruposTribEstadualVenda;
begin
  Result := nil;
  t := TGruposTribEstadualVenda.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordGruposTribEstadualVenda;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirGruposTribEstadualVenda(
  pConexao: TConexao;
  pGrupoTribEstadualVendaId: Integer
): RecRetornoBD;
var
  t: TGruposTribEstadualVenda;
begin
  Result.Iniciar;
  t := TGruposTribEstadualVenda.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('GRUPO_TRIB_ESTADUAL_VENDA_ID', pGrupoTribEstadualVendaId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
