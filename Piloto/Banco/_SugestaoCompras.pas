unit _SugestaoCompras;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils;

{$M+}
type
  RecItensSugestao = record
  public
    EmpresaId: Integer;
    ProdutoId: Integer;
    Nome: string;
    NomeCompra: string;
    CodigoBarras: string;
    CodigoOriginalFabricante: string;
    UnidadeVenda: string;
    MarcaId: Integer;
    NomeMarca: string;
    Estoque: Double;
    DiasEstoque: Integer;
    ComprasPendentes: Double;
    UltimaEntradaId: Integer;
    PrecoFinal: Double;
    QuantidadeEmbalagem: Double;
    VendasPeriodo: Double;
    MultiploCompra: Double;
    MultiploVenda: Double;

    FornecedorId: Double;
    NomeFornecedor: string;

    MediaVendasDiaria: Double;
    SugestaoCompra: Double;
    QtdeDiasUteisPeriodo: Integer;
    LocalId: Integer;
    NomeLocal: string;
  end;

  RecMovimentacoes = record
    ProdutoId: Integer;
    AnoMes: string;
    QtdeVendida: Double;
    QtdeDevolvida: Double;
    QtdeComprada: Double;
  end;

function BuscarDados(pConexao: TConexao; pFiltros: string): TArray<RecItensSugestao>;
function BuscarDadosPorLocais(pConexao: TConexao; pFiltros: string): TArray<RecItensSugestao>;
function getDiasUteisPeriodo( pConexao: TConexao; pEmpresaId: Integer; pDataInicial: TDate; pDataFinal: TDate ): Integer;
function BuscarMovimentacoesProdutos(pConexao: TConexao; pEmpresasIds: TArray<Integer>; pProdutosIds: TArray<Integer>): TArray<RecMovimentacoes>;

implementation

function BuscarDados(pConexao: TConexao; pFiltros: string): TArray<RecItensSugestao>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  EST.EMPRESA_ID, ');
  vSql.Add('  PRO.PRODUTO_ID, ');
  vSql.Add('  PRO.NOME, ');
  vSql.Add('  PRO.NOME_COMPRA, ');
  vSql.Add('  PRO.CODIGO_BARRAS, ');
  vSql.Add('  PRO.CODIGO_ORIGINAL_FABRICANTE, ');
  vSql.Add('  PRO.UNIDADE_VENDA, ');
  vSql.Add('  PRO.MARCA_ID, ');
  vSql.Add('  MAR.NOME as NOME_MARCA, ');
  vSql.Add('  EST.DISPONIVEL, ');
  vSql.Add('  EST.COMPRAS_PENDENTES, ');
  vSql.Add('  EST.ULTIMA_ENTRADA_ID, ');
  vSql.Add('  CUS.PRECO_FINAL, ');
  vSql.Add('  nvl(PMC.QUANTIDADE_EMBALAGEM, 1) as QUANTIDADE_EMBALAGEM, ');
  vSql.Add('  nvl(PMC.MULTIPLO, 1) as MULTIPLO_COMPRA, ');
  vSql.Add('  sum( nvl(VEN.QUANTIDADE, 0) * case when VEN.TIPO_MOVIMENTO = ''VEN'' then 1 else -1 end ) as VENDAS_PERIODO, ');
  vSql.Add('  PRO.MULTIPLO_VENDA, ');
  vSql.Add('  PRO.FORNECEDOR_ID, ');
  vSql.Add('  CAD.NOME_FANTASIA as NOME_FORNECEDOR ');
  vSql.Add('from ');
  vSql.Add('  PRODUTOS PRO ');

  vSql.Add('inner join VW_ESTOQUES EST ');
  vSql.Add('on PRO.PRODUTO_ID = EST.PRODUTO_ID ');

  vSql.Add('inner join MARCAS MAR ');
  vSql.Add('on PRO.MARCA_ID = MAR.MARCA_ID ');

  vSql.Add('inner join CUSTOS_PRODUTOS CUS ');
  vSql.Add('on EST.EMPRESA_ID = CUS.EMPRESA_ID ');
  vSql.Add('and EST.PRODUTO_ID = CUS.PRODUTO_ID ');

  vSql.Add('inner join CADASTROS CAD ');
  vSql.Add('on PRO.FORNECEDOR_ID = CAD.CADASTRO_ID ');

  vSql.Add('left join VW_VENDAS_PRODUTOS_SUG_COMPRAS VEN ');
  vSql.Add('on EST.EMPRESA_ID = VEN.EMPRESA_ID ');
  vSql.Add('and EST.PRODUTO_ID = VEN.PRODUTO_ID ');

  vSql.Add('left join PRODUTOS_MULTIPLOS_COMPRA PMC ');
  vSql.Add('on PRO.PRODUTO_ID = PMC.PRODUTO_ID ');
  vSql.Add('and ORDEM = 1 ');

  vSql.Add('where PRO.ATIVO = ''S'' ');

  vSql.Add(pFiltros);

  vSql.Add('group by ');
  vSql.Add('  EST.EMPRESA_ID, ');
  vSql.Add('  PRO.PRODUTO_ID, ');
  vSql.Add('  PRO.NOME, ');
  vSql.Add('  PRO.NOME_COMPRA, ');
  vSql.Add('  PRO.CODIGO_BARRAS, ');
  vSql.Add('  PRO.CODIGO_ORIGINAL_FABRICANTE, ');
  vSql.Add('  PRO.UNIDADE_VENDA, ');
  vSql.Add('  PRO.MARCA_ID, ');
  vSql.Add('  MAR.NOME, ');
  vSql.Add('  EST.DISPONIVEL, ');
  vSql.Add('  EST.COMPRAS_PENDENTES, ');
  vSql.Add('  EST.ULTIMA_ENTRADA_ID, ');
  vSql.Add('  CUS.PRECO_FINAL, ');
  vSql.Add('  nvl(PMC.QUANTIDADE_EMBALAGEM, 1), ');
  vSql.Add('  nvl(PMC.MULTIPLO, 1), ');
  vSql.Add('  PRO.MULTIPLO_VENDA, ');
  vSql.Add('  PRO.FORNECEDOR_ID, ');
  vSql.Add('  CAD.NOME_FANTASIA ');

  vSql.Add('order by ');
  vSql.Add('  PRO.NOME ');

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros -1 do begin
      Result[i].EmpresaId           := vSql.GetInt('EMPRESA_ID');
      Result[i].ProdutoId           := vSql.GetInt('PRODUTO_ID');
      Result[i].Nome                := vSql.GetString('NOME');
      Result[i].NomeCompra          := vSql.GetString('NOME_COMPRA');
      Result[i].CodigoBarras        := vSql.GetString('CODIGO_BARRAS');
      Result[i].CodigoOriginalFabricante := vSql.GetString('CODIGO_ORIGINAL_FABRICANTE');
      Result[i].UnidadeVenda         := vSql.GetString('UNIDADE_VENDA');
      Result[i].MarcaId             := vSql.GetInt('MARCA_ID');
      Result[i].NomeMarca           := vSql.GetString('NOME_MARCA');
      Result[i].Estoque             := vSql.GetDouble('DISPONIVEL');
      Result[i].ComprasPendentes    := vSql.GetDouble('COMPRAS_PENDENTES');
      Result[i].UltimaEntradaId     := vSql.GetInt('ULTIMA_ENTRADA_ID');
      Result[i].PrecoFinal          := vSql.GetDouble('PRECO_FINAL');
      Result[i].QuantidadeEmbalagem := vSql.GetDouble('QUANTIDADE_EMBALAGEM');
      Result[i].MultiploCompra      := vSql.GetDouble('MULTIPLO_COMPRA');
      Result[i].MultiploVenda       := vSql.GetDouble('MULTIPLO_VENDA');
      Result[i].VendasPeriodo       := vSql.GetDouble('VENDAS_PERIODO');
      Result[i].FornecedorId        := vSql.GetInt('FORNECEDOR_ID');
      Result[i].NomeFornecedor      := vSql.GetString('NOME_FORNECEDOR');

      vSql.Next;
    end;
  end;

  vSql.Free;
end;

function BuscarDadosPorLocais(pConexao: TConexao; pFiltros: string): TArray<RecItensSugestao>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  VWS.PRODUTO_ID, ');
  vSql.Add('  sum( nvl(VWS.QUANTIDADE, 0) * case when VWS.TIPO_MOVIMENTO = ''VEN'' then 1 else -1 end ) as VENDAS_PERIODO, ');
  vSql.Add('  VWS.LOCAL_ID, ');
  vSql.Add('  VWS.NOME as NOME_LOCAL, ');
  vSql.Add('  VWS.EMPRESA_ID, ');
  vSql.Add('  EST.FISICO, ');
  vSql.Add('  EST.DISPONIVEL, ');
  vSql.Add('  nvl(PMC.QUANTIDADE_EMBALAGEM, 1) as QUANTIDADE_EMBALAGEM, ');
  vSql.Add('  nvl(PMC.MULTIPLO, 1) as MULTIPLO_COMPRA, ');
  vSql.Add('  PRO.MULTIPLO_VENDA, ');
  vSql.Add('  PRO.FORNECEDOR_ID, ');
  vSql.Add('  ESS.COMPRAS_PENDENTES, ');
  vSql.Add('  ESS.ULTIMA_ENTRADA_ID ');
  vSql.Add('from VW_SUGESTAO_COMPRA_LOCAIS VWS ');

  vSql.Add('inner join PRODUTOS PRO ');
  vSql.Add('on PRO.PRODUTO_ID = VWS.PRODUTO_ID ');

  vSql.Add('inner join ESTOQUES ESS ');
  vSql.Add('on ESS.PRODUTO_ID = VWS.PRODUTO_ID ');
  vSql.Add('and ESS.EMPRESA_ID = VWS.EMPRESA_ID ');

  vSql.Add('inner join ESTOQUES_DIVISAO EST ');
  vSql.Add('on EST.PRODUTO_ID = VWS.PRODUTO_ID ');
  vSql.Add('and EST.EMPRESA_ID = VWS.EMPRESA_ID ');
  vSql.Add('and EST.LOCAL_ID = VWS.LOCAL_ID ');

  vSql.Add('left join PRODUTOS_MULTIPLOS_COMPRA PMC ');
  vSql.Add('on PRO.PRODUTO_ID = PMC.PRODUTO_ID ');
  vSql.Add('and ORDEM = 1 ');

  vSql.Add(pFiltros);

  vSql.Add(' group by ');
  vSql.Add('  VWS.LOCAL_ID, ');
  vSql.Add('  VWS.NOME, ');
  vSql.Add('  VWS.EMPRESA_ID, ');
  vSql.Add('  VWS.PRODUTO_ID, ');
  vSql.Add('  EST.FISICO, ');
  vSql.Add('  EST.DISPONIVEL, ');
  vSql.Add('  PMC.QUANTIDADE_EMBALAGEM, ');
  vSql.Add('  PMC.MULTIPLO, ');
  vSql.Add('  PRO.MULTIPLO_VENDA, ');
  vSql.Add('  PRO.FORNECEDOR_ID, ');
  vSql.Add('  ESS.COMPRAS_PENDENTES, ');
  vSql.Add('  ESS.ULTIMA_ENTRADA_ID ');

  vSql.Add('order by VWS.LOCAL_ID');

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros -1 do begin
      Result[i].EmpresaId           := vSql.GetInt('EMPRESA_ID');
      Result[i].ProdutoId           := vSql.GetInt('PRODUTO_ID');
      Result[i].LocalId             := vSql.GetInt('LOCAL_ID');
      Result[i].NomeLocal           := vSql.GetString('NOME_LOCAL');
      Result[i].Estoque             := vSql.GetDouble('DISPONIVEL');
      Result[i].ComprasPendentes    := vSql.GetDouble('COMPRAS_PENDENTES');
      Result[i].UltimaEntradaId     := vSql.GetInt('ULTIMA_ENTRADA_ID');
      Result[i].QuantidadeEmbalagem := vSql.GetDouble('QUANTIDADE_EMBALAGEM');
      Result[i].MultiploCompra      := vSql.GetDouble('MULTIPLO_COMPRA');
      Result[i].MultiploVenda       := vSql.GetDouble('MULTIPLO_VENDA');
      Result[i].VendasPeriodo       := vSql.GetDouble('VENDAS_PERIODO');
      Result[i].FornecedorId        := vSql.GetInt('FORNECEDOR_ID');

      vSql.Next;
    end;
  end;

  vSql.Free;
end;

function getDiasUteisPeriodo( pConexao: TConexao; pEmpresaId: Integer; pDataInicial: TDate; pDataFinal: TDate ): Integer;
var
  vProc: TProcedimentoBanco;
begin
  vProc := TProcedimentoBanco.Create(pConexao, 'DIAS_UTEIS_PERIODO');

  try
    vProc.Params[1].AsInteger := pEmpresaId;
    vProc.Params[2].AsDate    := pDataInicial;
    vProc.Params[3].AsDate    := pDataFinal;

    vProc.Executar;

    Result := vProc.Params[0].AsInteger;
  except
    on e: Exception do
      Result := 0;
  end;

  vProc.Free;
end;

function BuscarMovimentacoesProdutos(pConexao: TConexao; pEmpresasIds: TArray<Integer>; pProdutosIds: TArray<Integer>): TArray<RecMovimentacoes>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;

  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  MOV.PRODUTO_ID, ');
  vSql.Add('  to_char(MOV.DATA_HORA_MOVIMENTO, ''MM/YYYY'') as ANO_MES, ');
  vSql.Add('  sum(case when MOV.TIPO_MOVIMENTO = ''VEN'' then MOV.QUANTIDADE else 0 end) as QTDE_VENDIDA, ');
  vSql.Add('  sum(case when MOV.TIPO_MOVIMENTO = ''DEV'' then MOV.QUANTIDADE else 0 end) as QTDE_DEVOLVIDA, ');
  vSql.Add('  sum(case when MOV.TIPO_MOVIMENTO = ''COM'' then MOV.QUANTIDADE else 0 end) as QTDE_COMPRADA ');
  vSql.Add('from ');
  vSql.Add('  VW_VENDAS_PRODUTOS_SUG_COMPRAS MOV ');

  vSql.Add('inner join PRODUTOS PRO ');
  vSql.Add('on MOV.PRODUTO_ID = PRO.PRODUTO_ID ');

  vSql.Add('where TRUNC(MOV.DATA_HORA_MOVIMENTO, ''MONTH'') >= to_date(''01/'' || to_char(add_months(sysdate, ''-12''), ''MM/YYYY'')) ');
  vSql.Add('and ' + FiltroInInt('MOV.EMPRESA_ID', pEmpresasIds));
  vSql.Add('and ' + FiltroInInt('MOV.PRODUTO_ID', pProdutosIds));
  vSql.Add('and MOV.DATA_HORA_MOVIMENTO is not null ');

  vSql.Add('group by ');
  vSql.Add('  MOV.PRODUTO_ID, ');
  vSql.Add('  to_char(MOV.DATA_HORA_MOVIMENTO, ''MM/YYYY'') ');

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros -1 do begin
      Result[i].ProdutoId     := vSql.GetInt('PRODUTO_ID');
      Result[i].AnoMes        := vSql.GetString('ANO_MES');
      Result[i].QtdeVendida   := vSql.GetDouble('QTDE_VENDIDA');
      Result[i].QtdeDevolvida := vSql.GetDouble('QTDE_DEVOLVIDA');
      Result[i].QtdeComprada  := vSql.GetDouble('QTDE_COMPRADA');

      vSql.Next;
    end;
  end;

  vSql.Free;
end;

end.
