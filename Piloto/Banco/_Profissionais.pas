unit _Profissionais;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros, _Cadastros, _CadastrosTelefones, _DiversosEnderecos;

{$M+}
type
  RecProfissionais = class
  public
    Cadastro: RecCadastros;

    CadastroId: Integer;
    Ativo: string;
    CargoId: Integer;
    NumeroCrea: string;
    PercentualComissao: Double;
    PercentualComissaoPrazo: Double;
    RegraGeralComissaoGrupos: string;
    Vip: string;
    Informacoes: string;
  end;

  TProfissionais = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao; pSomenteAtivos: Boolean);
  protected
    function GetRecordProfissional: RecProfissionais;
  end;

function AtualizarProfissional(
  pConexao: TConexao;
  pPesquisouCadastro: Boolean;
  pCadastroId: Integer;
  pCadastro: RecCadastros;
  pTelefones: TArray<RecCadastrosTelefones>;
  pEnderecos: TArray<RecDiversosEnderecos>;
  pAtivo: string;
  pCargoId: Integer;
  pNumeroCrea: string;
  pPercentualComissao: Double;
  pPercentualComissaoPrazo: Double;
  pRegraGeralComissaoGrupos: string;
  pVip: string;
  pInformacoes: string
): RecRetornoBD;

function BuscarProfissionais(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecProfissionais>;

function ExcluirProfissional(
  pConexao: TConexao;
  pCadastroId: Integer
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TProfissionais }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 4);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'and PRF.CADASTRO_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome fantasia',
      False,
      0,
      'and CAD.NOME_FANTASIA like :P1 || ''%'' ' +
      'order by ' +
      '  CAD.NOME_FANTASIA '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Raz�o social',
      True,
      1,
      'and CAD.RAZAO_SOCIAL like :P1 || ''%'' ' +
      'order by ' +
      '  CAD.RAZAO_SOCIAL '
    );

  Result[3] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'CPF/CNPJ',
      False,
      2,
      'and CAD.CPF_CNPJ = :P1 ' +
      'order by ' +
      '  CAD.CPF_CNPJ '
    );
end;

constructor TProfissionais.Create(pConexao: TConexao; pSomenteAtivos: Boolean);
begin
  inherited Create(pConexao, 'PROFISSIONAIS');

  FSql :=
    'select ' +
    '  PRF.CADASTRO_ID, ' +
    '  PRF.ATIVO, ' +
    '  PRF.CARGO_ID, ' +
    '  PRF.NUMERO_CREA, ' +
    '  PRF.PERCENTUAL_COMISSAO, ' +
    '  PRF.PERCENTUAL_COMISSAO_PRAZO, ' +
    '  PRF.REGRA_GERAL_COMISSAO_GRUPOS, ' +
    '  PRF.VIP, ' +
    '  PRF.INFORMACOES, ' +
    // Cadastro
    '  CAD.NOME_FANTASIA, ' +
    '  CAD.RAZAO_SOCIAL, ' +
    '  CAD.TIPO_PESSOA, ' +
    '  CAD.CPF_CNPJ, ' +
    '  CAD.LOGRADOURO, ' +
    '  CAD.COMPLEMENTO, ' +
    '  CAD.NUMERO, ' +
    '  CAD.BAIRRO_ID, ' +
    '  CAD.PONTO_REFERENCIA, ' +
    '  CAD.CEP, ' +
    '  CAD.E_MAIL, ' +
    '  CAD.SEXO, ' +
    '  CAD.ESTADO_CIVIL, ' +
    '  CAD.DATA_NASCIMENTO, ' +
    '  CAD.DATA_CADASTRO, ' +
    '  CAD.INSCRICAO_ESTADUAL, ' +
    '  CAD.CNAE, ' +
    '  CAD.RG, ' +
    '  CAD.ORGAO_EXPEDIDOR_RG, ' +
    '  CAD.E_CLIENTE, ' +
    '  CAD.E_FORNECEDOR, ' +
    '  CAD.E_MOTORISTA, ' +
    '  CAD.E_PROFISSIONAL, ' +
    '  CAD.ATIVO as CADASTRO_ATIVO, ' +
    '  CID.ESTADO_ID ' +
    'from ' +
    '  PROFISSIONAIS PRF ' +

    'inner join CADASTROS CAD ' +
    'on PRF.CADASTRO_ID = CAD.CADASTRO_ID ' +

    'inner join BAIRROS BAI ' +
    'on CAD.BAIRRO_ID = BAI.BAIRRO_ID ' +

    'inner join CIDADES CID ' +
    'on BAI.CIDADE_ID = CID.CIDADE_ID ';

  if pSomenteAtivos then
    FSql := FSql + ' where PRF.ATIVO = ''S'''
  else
    FSql := FSql + ' where PRF.ATIVO in(''S'', ''N'') ';

  SetFiltros(GetFiltros);

  AddColuna('CADASTRO_ID', True);
  AddColuna('ATIVO');
  AddColuna('CARGO_ID');
  AddColuna('NUMERO_CREA');
  AddColuna('PERCENTUAL_COMISSAO');
  AddColuna('PERCENTUAL_COMISSAO_PRAZO');
  AddColuna('REGRA_GERAL_COMISSAO_GRUPOS');
  AddColuna('VIP');
  AddColuna('INFORMACOES');

  // CADASTRO
  AddColunaSL('NOME_FANTASIA');
  AddColunaSL('RAZAO_SOCIAL');
  AddColunaSL('TIPO_PESSOA');
  AddColunaSL('CPF_CNPJ');
  AddColunaSL('LOGRADOURO');
  AddColunaSL('COMPLEMENTO');
  AddColunaSL('NUMERO');
  AddColunaSL('BAIRRO_ID');
  AddColunaSL('CEP');
  AddColunaSL('PONTO_REFERENCIA');
  AddColunaSL('E_MAIL');
  AddColunaSL('SEXO');
  AddColunaSL('ESTADO_CIVIL');
  AddColunaSL('DATA_NASCIMENTO');
  AddColunaSL('DATA_CADASTRO');
  AddColunaSL('INSCRICAO_ESTADUAL');
  AddColunaSL('CNAE');
  AddColunaSL('RG');
  AddColunaSL('ORGAO_EXPEDIDOR_RG');
  AddColunaSL('E_CLIENTE');
  AddColunaSL('E_FORNECEDOR');
  AddColunaSL('E_MOTORISTA');
  AddColunaSL('E_PROFISSIONAL');
  AddColunaSL('CADASTRO_ATIVO');
  AddColunaSL('ESTADO_ID');
end;

function TProfissionais.GetRecordProfissional: RecProfissionais;
begin
  Result := RecProfissionais.Create;
  Result.Cadastro := RecCadastros.Create;

  Result.CadastroId                  := getInt('CADASTRO_ID', True);
  Result.Ativo                       := getString('ATIVO');
  Result.CargoId                     := getInt('CARGO_ID');
  Result.NumeroCrea                  := getString('NUMERO_CREA');
  Result.PercentualComissao          := getDouble('PERCENTUAL_COMISSAO');
  Result.PercentualComissaoPrazo     := getDouble('PERCENTUAL_COMISSAO_PRAZO');
  Result.RegraGeralComissaoGrupos    := getString('REGRA_GERAL_COMISSAO_GRUPOS');
  Result.Vip                         := getString('VIP');
  Result.Informacoes                 := getString('INFORMACOES');

  // Cadastro
  Result.Cadastro.cadastro_id        := GetInt('CADASTRO_ID', True);
  Result.Cadastro.razao_social       := getString('RAZAO_SOCIAL');
  Result.Cadastro.nome_fantasia      := getString('NOME_FANTASIA');
  Result.Cadastro.tipo_pessoa        := getString('TIPO_PESSOA');
  Result.Cadastro.cpf_cnpj           := getString('CPF_CNPJ');
  Result.Cadastro.logradouro         := getString('LOGRADOURO');
  Result.Cadastro.complemento        := getString('COMPLEMENTO');
  Result.Cadastro.numero             := getString('NUMERO');
  Result.Cadastro.bairro_id          := getInt('BAIRRO_ID');
  Result.Cadastro.ponto_referencia   := getString('PONTO_REFERENCIA');
  Result.Cadastro.cep                := getString('CEP');
  Result.Cadastro.e_mail             := getString('E_MAIL');
  Result.Cadastro.sexo               := getString('SEXO');
  Result.Cadastro.estado_civil       := getString('ESTADO_CIVIL');
  Result.Cadastro.data_nascimento    := getData('DATA_NASCIMENTO');
  Result.Cadastro.data_cadastro      := GetData('DATA_CADASTRO');
  Result.Cadastro.inscricao_estadual := getString('INSCRICAO_ESTADUAL');
  Result.Cadastro.cnae               := getString('CNAE');
  Result.Cadastro.rg                 := getString('RG');
  Result.Cadastro.orgao_expedidor_rg := getString('ORGAO_EXPEDIDOR_RG');
  Result.Cadastro.e_cliente          := getString('E_CLIENTE');
  Result.Cadastro.e_fornecedor       := getString('E_FORNECEDOR');
  Result.cadastro.EMotorista         := getString('E_MOTORISTA');
  Result.cadastro.EProfissional      := getString('E_PROFISSIONAL');
  Result.Cadastro.ativo              := getString('CADASTRO_ATIVO');
  Result.Cadastro.estado_id          := getString('ESTADO_ID');
end;

function AtualizarProfissional(
  pConexao: TConexao;
  pPesquisouCadastro: Boolean;
  pCadastroId: Integer;
  pCadastro: RecCadastros;
  pTelefones: TArray<RecCadastrosTelefones>;
  pEnderecos: TArray<RecDiversosEnderecos>;
  pAtivo: string;
  pCargoId: Integer;
  pNumeroCrea: string;
  pPercentualComissao: Double;
  pPercentualComissaoPrazo: Double;
  pRegraGeralComissaoGrupos: string;
  pVip: string;
  pInformacoes: string
): RecRetornoBD;
var
  vPro: TProfissionais;
  vNovo: Boolean;
  vCad: TCadastro;
  vRetBanco: RecRetornoBD;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_PROFISSIONAL');
  vNovo := pCadastroId = 0;
  vPro := TProfissionais.Create(pConexao, False);
  vCad := TCadastro.Create(pConexao);

  if vNovo then begin
    pCadastroId := TSequencia.Create(pConexao, 'SEQ_CADASTRO_ID').GetProximaSequencia;
    Result.AsInt := pCadastroId;
    pCadastro.cadastro_id := pCadastroId;
  end;

  try
    pConexao.IniciarTransacao;

    vCad.setCadastro(pCadastro);
    if vNovo then
      vCad.Inserir
    else
      vCad.Atualizar;

    vPro.setInt('CADASTRO_ID', pCadastroId, True);
    vPro.SetString('ATIVO', pAtivo);
    vPro.setInt('CARGO_ID', pCargoId);
    vPro.SetString('NUMERO_CREA', pNumeroCrea);
    vPro.setDouble('PERCENTUAL_COMISSAO', pPercentualComissao);
    vPro.setDouble('PERCENTUAL_COMISSAO_PRAZO', pPercentualComissaoPrazo);
    vPro.SetString('REGRA_GERAL_COMISSAO_GRUPOS', pRegraGeralComissaoGrupos);
    vPro.SetString('VIP', pVip);
    vPro.SetString('INFORMACOES', pInformacoes);

    if vNovo or pPesquisouCadastro then
      vPro.Inserir
    else
      vPro.Atualizar;

    vRetBanco := _CadastrosTelefones.AtualizarTelefones(pConexao, pCadastroId, pTelefones);
    if vRetBanco.TeveErro then
      raise Exception.Create(vRetBanco.MensagemErro);

    if pEnderecos <> nil then begin
      vRetBanco := _DiversosEnderecos.AtualizarDiversosEndereco(pConexao, pCadastroId, pEnderecos);

      if vRetBanco.TeveErro then
        raise Exception.Create(vRetBanco.MensagemErro);
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vCad.Free;
  vPro.Free;
end;

function BuscarProfissionais(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecProfissionais>;
var
  i: Integer;
  t: TProfissionais;
begin
  Result := nil;
  pConexao.SetRotina('BUSCAR_PROFISSIONAIS');
  t := TProfissionais.Create(pConexao, pSomenteAtivos);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordProfissional;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirProfissional(
  pConexao: TConexao;
  pCadastroId: Integer
): RecRetornoBD;
var
  ex: TExecucao;
  t: TProfissionais;
begin
  Result.Iniciar;
  pConexao.SetRotina('EXCLUIR_PROFISSIONAL');

  t := TProfissionais.Create(pConexao, False);
  ex := TExecucao.Create(pConexao);

  t.SetInt('CADASTRO_ID', pCadastroId, True);

  try
    pConexao.IniciarTransacao;

    ex.SQL.Add('delete from DIVERSOS_CONTATOS where CADASTRO_ID = :P1');
    ex.Executar([pCadastroId]);

    ex.SQL.Clear;
    ex.SQL.Add('delete from DIVERSOS_ENDERECOS where CADASTRO_ID = :P1');
    ex.Executar([pCadastroId]);

    t.Excluir;
    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
