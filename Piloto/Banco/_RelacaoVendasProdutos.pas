unit _RelacaoVendasProdutos;

interface

uses
  _Conexao, _OperacoesBancoDados;

type
  RecRelacaoVendasProdutos = record
    TipoMovimento: string;
    MovimentoId: Integer;
    DataMovimento: TDateTime;
    ProdutoId: Integer;
    NomeProduto: string;
    NomeMarca: string;
    UnidadeVenda: string;
    Quantidade: Double;
    ValorLiquido: Double;
    VendedorId: Integer;
    NomeVendedor: string;
    ValorCustoEntrada: Double;
    ValorCustoVenda: Double;
  end;

  RecMixProdutos = record
    ClienteId: Integer;
    TipoMovimento: string;
    ProdutoId: Integer;
    PrecoMedio: Double;
    PercentualParticipacao: Double;
    PercentualMargem: Double;
    NomeProduto: string;
    NomeMarca: string;
    ValorTotal: Double;
    UnidadeVenda: string;
    Quantidade: Double;
  end;

function BuscarVendasProdutos(pConexao: TConexao; pComando: string): TArray<RecRelacaoVendasProdutos>;
function BuscarMixProdutosCliente(pConexao: TConexao; pClienteId: Integer): TArray<RecMixProdutos>;

implementation

function BuscarVendasProdutos(pConexao: TConexao; pComando: string): TArray<RecRelacaoVendasProdutos>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  TIPO_MOVIMENTO, ');
  vSql.Add('  MOVIMENTO_ID, ');
  vSql.Add('  DATA_HORA_MOVIMENTO, ');
  vSql.Add('  EMPRESA_ID, ');
  vSql.Add('  PRODUTO_ID, ');
  vSql.Add('  NOME_PRODUTO, ');
  vSql.Add('  NOME_MARCA, ');
  vSql.Add('  UNIDADE_VENDA, ');
  vSql.Add('  QUANTIDADE, ');
  vSql.Add('  VALOR_LIQUIDO, ');
  vSql.Add('  VENDEDOR_ID, ');
  vSql.Add('  NOME_VENDEDOR, ');
  vSql.Add('  VALOR_CUSTO_ENTRADA, ');
  vSql.Add('  VALOR_CUSTO_VENDA ');
  vSql.Add('from ');
  vSql.Add('  VW_RELACAO_VENDAS_PRODUTOS ');

  vSql.Add(pComando);

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].TipoMovimento := vSql.GetString('TIPO_MOVIMENTO');
      Result[i].MovimentoId   := vSql.GetInt('MOVIMENTO_ID');
      Result[i].DataMovimento := vSql.GetData('DATA_HORA_MOVIMENTO');
      Result[i].ProdutoId     := vSql.GetInt('PRODUTO_ID');
      Result[i].NomeProduto   := vSql.GetString('NOME_PRODUTO');
      Result[i].NomeMarca     := vSql.GetString('NOME_MARCA');
      Result[i].UnidadeVenda  := vSql.GetString('UNIDADE_VENDA');
      Result[i].Quantidade    := vSql.GetDouble('QUANTIDADE');
      Result[i].ValorLiquido  := vSql.GetDouble('VALOR_LIQUIDO');
      Result[i].ValorCustoEntrada := vSql.GetDouble('VALOR_CUSTO_ENTRADA');
      Result[i].ValorCustoVenda := vSql.GetDouble('VALOR_CUSTO_VENDA');
      Result[i].VendedorId    := vSql.GetInt('VENDEDOR_ID');
      Result[i].NomeVendedor  := vSql.GetString('NOME_VENDEDOR');

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function BuscarMixProdutosCliente(pConexao: TConexao; pClienteId: Integer): TArray<RecMixProdutos>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  RVP.CLIENTE_ID, ');
  vSql.Add('  RVP.TIPO_MOVIMENTO, ');
  vSql.Add('  RVP.PRODUTO_ID, ');
  vSql.Add('  RVP.NOME_PRODUTO, ');
  vSql.Add('  RVP.NOME_MARCA, ');
  vSql.Add('  RVP.UNIDADE_VENDA, ');
  vSql.Add('  sum(RVP.QUANTIDADE) as QUANTIDADE, ');
  vSql.Add('  round(sum(RVP.VALOR_LIQUIDO) / sum(RVP.QUANTIDADE), 4) as PRECO_MEDIO, ');
  vSql.Add('  round( ');
  vSql.Add('  ( ');
  vSql.Add('    sum(RVP.VALOR_LIQUIDO) ');
  vSql.Add('    / ');
  vSql.Add('    ( ');
  vSql.Add('      select  ');
  vSql.Add('        sum(VALOR_LIQUIDO) ');
  vSql.Add('      from ');
  vSql.Add('        VW_RELACAO_VENDAS_PRODUTOS ');
  vSql.Add('      where CLIENTE_ID = RVP.CLIENTE_ID ');
  vSql.Add('    ) ');
  vSql.Add('  ) * 100, 4) as PERCENTUAL_PARTICIPACAO, ');
  vSql.Add('   round((sum(RVP.VALOR_LUCRO_BRUTO) / sum(RVP.QUANTIDADE)) / (sum(RVP.VALOR_LIQUIDO) / sum(RVP.QUANTIDADE)), 4) as MARGEM, ');
  vSql.Add('   round(sum(RVP.VALOR_LIQUIDO * RVP.QUANTIDADE), 4) as VALOR_TOTAL ');
  vSql.Add('from ');
  vSql.Add('  VW_RELACAO_VENDAS_PRODUTOS RVP ');

  vSql.Add('where RVP.CLIENTE_ID = :P1 ');
  vSql.Add('and (trunc(sysdate) - trunc(RVP.DATA_HORA_MOVIMENTO)) <= 365 ');

  vSql.Add('group by');
  vSql.Add('  RVP.CLIENTE_ID,');
  vSql.Add('  RVP.PRODUTO_ID,');
  vSql.Add('  RVP.NOME_PRODUTO,');
  vSql.Add('  RVP.NOME_MARCA,');
  vSql.Add('  RVP.UNIDADE_VENDA,');
  vSql.Add('  RVP.TIPO_MOVIMENTO');

  vSql.Add('order by');
  vSql.Add('  PERCENTUAL_PARTICIPACAO,');
  vSql.Add('  MARGEM desc ');

  if vSql.Pesquisar([pClienteId]) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].ClienteId              := vSql.GetInt('CLIENTE_ID');
      Result[i].ProdutoId              := vSql.GetInt('PRODUTO_ID');
      Result[i].TipoMovimento          := vSql.GetString('TIPO_MOVIMENTO');
      Result[i].NomeProduto            := vSql.GetString('NOME_PRODUTO');
      Result[i].NomeMarca              := vSql.GetString('NOME_MARCA');
      Result[i].UnidadeVenda           := vSql.GetString('UNIDADE_VENDA');
      Result[i].Quantidade             := vSql.GetDouble('QUANTIDADE');
      Result[i].PrecoMedio             := vSql.GetDouble('PRECO_MEDIO');
      Result[i].PercentualParticipacao := vSql.GetDouble('PERCENTUAL_PARTICIPACAO');
      Result[i].PercentualMargem       := vSql.GetDouble('MARGEM');
      Result[i].ValorTotal             := vSql.GetDouble('VALOR_TOTAL');

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

end.
