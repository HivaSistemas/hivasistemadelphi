unit _CST;

interface

uses
  _OperacoesBancoDados, _RecordsEspeciais, _Conexao, System.SysUtils, _RecordsCadastros, System.Variants;

function getCSTs(pConexao: TConexao): TArray<RecCST>;

implementation

function getCSTs(pConexao: TConexao): TArray<RecCST>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  CST, ');
  vSql.SQL.Add('  DESCRICAO ');
  vSql.SQL.Add('from ');
  vSql.SQL.Add('  VW_CST ');

  vSql.Pesquisar;
  SetLength(Result, vSql.GetQuantidadeRegistros);
  for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
    Result[i].CST       := vSql.GetString(0);
    Result[i].Descricao := vSql.GetString(1);

    vSql.Next;
  end;

  vSql.Active := False;
  vSql.Free;
end;

end.
