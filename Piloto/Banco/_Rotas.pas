unit _Rotas;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils;

{$M+}
type
  RecRotas = class
  public
    RotaId: Integer;
    Nome: string;
    Ativo: string;
  end;

  TRotas = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordRotas: RecRotas;
  end;

function AtualizarRota(
  pConexao: TConexao;
  pRotaId: Integer;
  pNome: string;
  pAtivo: string
): RecRetornoBD;

function BuscarRotas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecRotas>;

function ExcluirRota(
  pConexao: TConexao;
  pRotaId: Integer
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TRotas }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ROTA_ID = :P1 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Rotas ( avan�ado )',
      True,
      0,
      'where NOME like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  NOME'
    );
end;

constructor TRotas.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ROTAS');

  FSql :=
    'select ' +
    '  ROTA_ID, ' +
    '  NOME, ' +
    '  ATIVO ' +
    'from ' +
    '  ROTAS ';

  SetFiltros(GetFiltros);

  AddColuna('ROTA_ID', True);
  AddColuna('NOME');
  AddColuna('ATIVO');
end;

function TRotas.GetRecordRotas: RecRotas;
begin
  Result := RecRotas.Create;

  Result.RotaId  := GetInt('ROTA_ID', True);
  Result.Nome    := GetString('NOME');
  Result.Ativo   := GetString('ATIVO');
end;

function AtualizarRota(
  pConexao: TConexao;
  pRotaId: Integer;
  pNome: string;
  pAtivo: string
): RecRetornoBD;
var
  vRota: TRotas;
  vNovo: Boolean;
begin
  vNovo := pRotaId = 0;
  Result.TeveErro := False;
  vRota := TRotas.Create(pConexao);

  if vNovo then begin
    pRotaId := TSequencia.Create(pConexao, 'SEQ_ROTA_ID').GetProximaSequencia;
    Result.AsInt := pRotaId;
  end;

  vRota.SetInt('ROTA_ID', pRotaId, True);
  vRota.SetString('NOME', pNome);
  vRota.SetString('ATIVO', pAtivo);

  try
    pConexao.IniciarTransacao;

    if vNovo then
      vRota.Inserir
    else
      vRota.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vRota.Free;
end;

function BuscarRotas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecRotas>;
var
  i: Integer;
  t: TRotas;

  vSqlAdicional: string;
begin
  Result := nil;
  t := TRotas.Create(pConexao);

  vSqlAdicional := '';
  if pSomenteAtivos then
    vSqlAdicional := 'and ATIVO = ''S'' ';

  if t.Pesquisar(pIndice, pFiltros, vSqlAdicional) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordRotas;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirRota(
  pConexao: TConexao;
  pRotaId: Integer
): RecRetornoBD;
var
  t: TRotas;
begin
  pConexao.SetRotina('EXCLUIR_ROTA');
  Result.TeveErro := False;
  t := TRotas.Create(pConexao);

  t.SetInt('ROTA_ID', pRotaId, True);

  try
    pConexao.IniciarTransacao;

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
