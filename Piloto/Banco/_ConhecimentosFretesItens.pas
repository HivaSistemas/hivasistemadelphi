unit _ConhecimentosFretesItens;

interface

uses
  _OperacoesBancoDados, _RecordsEspeciais, _Conexao, System.SysUtils, System.Variants, _BibliotecaGenerica;

{$M+}
type
  RecConhecimentosFretesItens = record
    ConhecimentoId: Integer;
    ItemId: Integer;
    FornecedorNotaId: Integer;
    NumeroNota: Integer;
    ModeloNota: string;
    SerieNota: string;
    DataEmissaoNota: TDateTime;
    PesoNota: Double;
    QuantidadeNota: Double;
    ValorTotalNota: Double;
    Indice: Double;
    ValorCohecimentoFrete: Double;
    ValorICMSFrete: Double;

    NomeFornecedor: string;
  end;

  TConhecimentosFretesItens = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordConhecimentoItem: RecConhecimentosFretesItens;
  end;

function BuscarItens(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant
): TArray<RecConhecimentosFretesItens>;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TConhecimentosFretesItens }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ITE.CONHECIMENTO_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ITE.FORNECEDOR_NOTA_ID = :P1 ' +
      'and ITE.NUMERO_NOTA = :P2 ' +
      'and ITE.MODELO_NOTA = :P3 ' +
      'and ITE.SERIE_NOTA = :P4 '
    );
end;

constructor TConhecimentosFretesItens.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONHECIMENTOS_FRETES_ITENS');

  FSql :=
    'select ' +
    '  ITE.CONHECIMENTO_ID, ' +
    '  ITE.ITEM_ID, ' +
    '  ITE.FORNECEDOR_NOTA_ID, ' +
    '  CAD.NOME_FANTASIA as NOME_FORNECEDOR, ' +
    '  ITE.NUMERO_NOTA, ' +
    '  ITE.MODELO_NOTA, ' +
    '  ITE.SERIE_NOTA, ' +
    '  ITE.DATA_EMISSAO_NOTA, ' +
    '  ITE.PESO_NOTA, ' +
    '  ITE.QUANTIDADADE_NOTA, ' +
    '  ITE.VALOR_TOTAL_NOTA, ' +
    '  ITE.INDICE, ' +
    '  round(CON.VALOR_FRETE * ITE.INDICE, 2) as VALOR_CONHECIMENTO_FRETE, ' +
    '  round(CON.VALOR_ICMS * ITE.INDICE, 2) as VALOR_ICMS_FRETE ' +
    'from ' +
    '  CONHECIMENTOS_FRETES_ITENS ITE ' +

    'inner join CONHECIMENTOS_FRETES CON ' +
    'on ITE.CONHECIMENTO_ID = CON.CONHECIMENTO_ID ' +

    'inner join CADASTROS CAD ' +
    'on ITE.FORNECEDOR_NOTA_ID = CAD.CADASTRO_ID ';

  SetFiltros(GetFiltros);

  AddColuna('CONHECIMENTO_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('FORNECEDOR_NOTA_ID');
  AddColuna('NUMERO_NOTA');
  AddColuna('MODELO_NOTA');
  AddColuna('SERIE_NOTA');
  AddColuna('DATA_EMISSAO_NOTA');
  AddColuna('PESO_NOTA');
  AddColuna('QUANTIDADADE_NOTA');
  AddColuna('VALOR_TOTAL_NOTA');
  AddColuna('INDICE');
  AddColunaSL('VALOR_CONHECIMENTO_FRETE');
  AddColunaSL('VALOR_ICMS_FRETE');
  AddColunaSL('NOME_FORNECEDOR');
end;

function TConhecimentosFretesItens.GetRecordConhecimentoItem: RecConhecimentosFretesItens;
begin
  Result.ConhecimentoId   := GetInt('CONHECIMENTO_ID', True);
  Result.ItemId           := GetInt('ITEM_ID', True);
  Result.FornecedorNotaId := GetInt('FORNECEDOR_NOTA_ID');
  Result.NumeroNota       := GetInt('NUMERO_NOTA');
  Result.ModeloNota       := GetString('MODELO_NOTA');
  Result.SerieNota        := GetString('SERIE_NOTA');
  Result.DataEmissaoNota  := GetData('DATA_EMISSAO_NOTA');
  Result.PesoNota         := getDouble('PESO_NOTA');
  Result.QuantidadeNota   := getDouble('QUANTIDADADE_NOTA');
  Result.ValorTotalNota   := getDouble('VALOR_TOTAL_NOTA');
  Result.Indice           := getDouble('INDICE');
  Result.ValorCohecimentoFrete := getDouble('VALOR_CONHECIMENTO_FRETE');
  Result.ValorICMSFrete   := getDouble('VALOR_ICMS_FRETE');
  Result.NomeFornecedor   := GetString('NOME_FORNECEDOR');
end;

function BuscarItens(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant
): TArray<RecConhecimentosFretesItens>;
var
  i: Integer;
  t: TConhecimentosFretesItens;
begin
  Result := nil;
  t := TConhecimentosFretesItens.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordConhecimentoItem;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
