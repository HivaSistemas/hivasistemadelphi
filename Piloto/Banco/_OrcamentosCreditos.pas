unit _OrcamentosCreditos;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TOrcamentosCreditos = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  end;

implementation

{ TOrcamentosCreditos }

constructor TOrcamentosCreditos.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ORCAMENTOS_CREDITOS');

  FSql :=
    '';

  AddColuna('ORCAMENTO_ID', True);
  AddColuna('PAGAR_ID', True);
  AddColuna('MOMENTO_USO');
end;

end.
