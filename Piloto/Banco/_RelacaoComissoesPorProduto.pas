unit _RelacaoComissoesPorProduto;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsOrcamentosVendas;

function BuscarComissoes(pConexao: TConexao; const pComando: string): TArray<RecComissoesPorProdutos>;

implementation

function BuscarComissoes(pConexao: TConexao; const pComando: string): TArray<RecComissoesPorProdutos>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  with vSql.SQL do begin
    Add('select ');
    Add('  COM.ID, ');
    Add('  COM.TIPO, ');
    Add('  COM.VENDEDOR_ID, ');
    Add('  COM.NOME_FUNCIONARIO, ');
    Add('  COM.DATA, ');
    Add('  COM.CLIENTE_ID, ');
    Add('  COM.NOME_CLIENTE, ');
    Add('  COM.PRODUTO_ID, ');
    Add('  COM.MARCA_ID, ');
    Add('  COM.NOME_MARCA, ');
    Add('  COM.NOME_PRODUTO, ');
    Add('  COM.VALOR_VENDA, ');
    Add('  COM.BASE_COMISSAO_PRODUTO, ');
    Add('  COM.TIPO_COMISSAO, ');
    Add('  COM.PERCENTUAL_COMISSAO_PRODUTO, ');
    Add('  COM.VALOR_COMISSAO_PRODUTO ');
    Add('from ');
    Add('  VW_COMISSAO_POR_PRODUTOS COM ');
    Add(pComando);
  end;

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].Id                        := vSql.GetInt('ID');
      Result[i].Tipo                      := vSql.GetString('TIPO');
      Result[i].VendedorId                := vSql.GetInt('VENDEDOR_ID');
      Result[i].NomeFuncionario           := vSql.GetString('NOME_FUNCIONARIO');
      Result[i].Data                      := vSql.GetData('DATA');
      Result[i].CadastroId                := vSql.GetInt('CLIENTE_ID');
      Result[i].NomeCliente               := vSql.GetString('NOME_CLIENTE');
      Result[i].ProdutoId                 := vSql.GetInt('PRODUTO_ID');
      Result[i].NomeProduto               := vSql.GetString('NOME_PRODUTO');
      Result[i].MarcaId                   := vSql.GetInt('MARCA_ID');
      Result[i].NomeMarca                 := vSql.GetString('NOME_MARCA');
      Result[i].ValorVenda                := vSql.GetDouble('VALOR_VENDA');
      Result[i].BaseComissaoProduto       := vSql.GetDouble('BASE_COMISSAO_PRODUTO');
      Result[i].TipoComissao              := vSql.GetString('TIPO_COMISSAO');
      Result[i].PercentualComissaoProduto := vSql.GetDouble('PERCENTUAL_COMISSAO_PRODUTO');
      Result[i].ValorComissaoProduto      := vSql.GetDouble('VALOR_COMISSAO_PRODUTO');

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

end.
