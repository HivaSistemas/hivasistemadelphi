unit _NotasFiscaisCartasCorrecoes;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, System.Classes;

{$M+}
type
  RecNotasFiscaisCartasCorrecoes = record
    NotaFiscalId: Integer;
    Sequencia: Integer;
    TextoCorrecao: string;
    UsuarioCadastroId: Integer;
    NomeUsuarioCadastro: string;
    DataHoraCadastro: TDateTime;
    Status: string;
    DataHoraEmissao: TDateTime;
    ProtocoloNfe: string;
    StatusAnalitico: string;
  end;

  TNotasFiscaisCartasCorrecoes = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordNotasFiscaisCartasCorrecoes: RecNotasFiscaisCartasCorrecoes;
  end;

function AtualizarNotasFiscaisCartasCorrecoes(
  pConexao: TConexao;
  pNotaFiscalId: Integer;
  pSequencia: Integer;
  pTextoCorrecao: string;
  pStatus: string
): RecRetornoBD;

function salvarXMLEmissaoCartaCorrecao(
  pConexao: TConexao;
  pNotaFiscalId: Integer;
  pSequencia: Integer;
  pProtocoloNFe: string;
  pTextoXML: string;
  pCaminhoXML: string
): RecRetornoBD;

function BuscarNotasFiscaisCartasCorrecoes(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecNotasFiscaisCartasCorrecoes>;

function ExcluirNotasFiscaisCartasCorrecoes(
  pConexao: TConexao;
  pNotaFiscalId: Integer;
  pSequencia: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TNotasFiscaisCartasCorrecoes }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where NOTA_FISCAL_ID = :P1'
    );
end;

constructor TNotasFiscaisCartasCorrecoes.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'NOTAS_FISCAIS_CARTAS_CORRECOES');

  FSql := 
    'select ' +
    '  COR.NOTA_FISCAL_ID, ' +
    '  COR.SEQUENCIA, ' +
    '  COR.TEXTO_CORRECAO, ' +
    '  COR.USUARIO_CADASTRO_ID, ' +
    '  FCC.NOME as NOME_USUARIO_CADASTRO, ' +
    '  COR.DATA_HORA_CADASTRO, ' +
    '  COR.STATUS, ' +
    '  COR.DATA_HORA_EMISSAO, ' +
    '  COR.PROTOCOLO_NFE ' +
    'from ' +
    '  NOTAS_FISCAIS_CARTAS_CORRECOES COR ' +

    'inner join FUNCIONARIOS FCC ' +
    'on COR.USUARIO_CADASTRO_ID = FCC.FUNCIONARIO_ID';

  setFiltros(getFiltros);

  AddColuna('NOTA_FISCAL_ID', True);
  AddColuna('SEQUENCIA', True);
  AddColuna('TEXTO_CORRECAO');
  AddColunaSL('USUARIO_CADASTRO_ID');
  AddColunaSL('NOME_USUARIO_CADASTRO');
  AddColunaSL('DATA_HORA_CADASTRO');
  AddColuna('STATUS');
  AddColunaSL('DATA_HORA_EMISSAO');
  AddColunaSL('PROTOCOLO_NFE');
end;

function TNotasFiscaisCartasCorrecoes.getRecordNotasFiscaisCartasCorrecoes: RecNotasFiscaisCartasCorrecoes;
begin
  Result.NotaFiscalId               := getInt('NOTA_FISCAL_ID', True);
  Result.Sequencia                  := getInt('SEQUENCIA', True);
  Result.TextoCorrecao              := getString('TEXTO_CORRECAO');
  Result.UsuarioCadastroId          := getInt('USUARIO_CADASTRO_ID');
  Result.NomeUsuarioCadastro        := getString('NOME_USUARIO_CADASTRO');
  Result.DataHoraCadastro           := getData('DATA_HORA_CADASTRO');
  Result.Status                     := getString('STATUS');
  Result.DataHoraEmissao            := getData('DATA_HORA_EMISSAO');
  Result.ProtocoloNfe               := getString('PROTOCOLO_NFE');
  Result.StatusAnalitico            := IIfStr(Result.Status = 'N', 'N�o emitida', 'Emitida');
end;

function AtualizarNotasFiscaisCartasCorrecoes(
  pConexao: TConexao;
  pNotaFiscalId: Integer;
  pSequencia: Integer;
  pTextoCorrecao: string;
  pStatus: string
): RecRetornoBD;
var
  t: TNotasFiscaisCartasCorrecoes;
  vNovo: Boolean;

  vSequencia: Integer;
  vSql: TConsulta;
begin
  Result.Iniciar;
  pConexao.setRotina('ATUALIZAR_CARTA_CORRECAO');

  t := TNotasFiscaisCartasCorrecoes.Create(pConexao);

  vNovo := pSequencia = 0;
  vSequencia := pSequencia;

  if vNovo then begin
    vSql := TConsulta.Create(pConexao);
    vSql.Add('select ');
    vSql.Add('  nvl(max(SEQUENCIA), 0) + 1 as SEQUENCIA ');
    vSql.Add('from ');
    vSql.Add('  NOTAS_FISCAIS_CARTAS_CORRECOES ');
    vSql.Add('where NOTA_FISCAL_ID = :P1 ');

    vSql.Pesquisar([pNotaFiscalId]);

    vSequencia := vSql.GetInt(0);

    vSql.Free;
  end;

  try
    pConexao.IniciarTransacao; 

    t.setInt('NOTA_FISCAL_ID', pNotaFiscalId, True);
    t.setInt('SEQUENCIA', vSequencia, True);
    t.setString('TEXTO_CORRECAO', pTextoCorrecao);
    t.setString('STATUS', pStatus);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao; 
  except
    on e: Exception do begin
      pConexao.VoltarTransacao; 
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function salvarXMLEmissaoCartaCorrecao(
  pConexao: TConexao;
  pNotaFiscalId: Integer;
  pSequencia: Integer;
  pProtocoloNFe: string;
  pTextoXML: string;
  pCaminhoXML: string
): RecRetornoBD;
var
  vExec: TExecucao;
  vArquivo: TMemoryStream;
begin
  Result.Iniciar;
  pConexao.SetRotina('SALVAR_XML_CARTA_CORRECAO');
  vExec := TExecucao.Create(pConexao);
  try
    pConexao.IniciarTransacao;

    vExec.Add('delete from NOTAS_FISCAIS_CARTAS_CORR_XML');
    vExec.Add('where NOTA_FISCAL_ID = :P1');
    vExec.Add('and SEQUENCIA = :P2');
    vExec.Executar([pNotaFiscalId, pSequencia]);

    vExec.Limpar;
    vExec.Add('insert into NOTAS_FISCAIS_CARTAS_CORR_XML( ');
    vExec.Add('  NOTA_FISCAL_ID, ');
    vExec.Add('  SEQUENCIA ');
    vExec.Add(')values( ');
    vExec.Add('  :P1, ');
    vExec.Add('  :P2 ');
    vExec.Add(') ');
    vExec.Executar([pNotaFiscalId, pSequencia]);

    vArquivo := TMemoryStream.Create;
    vArquivo.LoadFromFile(pCaminhoXML);

    vExec.Limpar;
    vExec.Add('update NOTAS_FISCAIS_CARTAS_CORR_XML set ');
    vExec.Add('  XML = :P3, ');
    vExec.Add('  XML_TEXTO = :P4 ');
    vExec.Add('where NOTA_FISCAL_ID = :P1');
    vExec.Add('and SEQUENCIA = :P2');

    vExec.ParamByName('P1').AsInteger := pNotaFiscalId;
    vExec.ParamByName('P2').AsInteger := pSequencia;

    vExec.CarregarBinario('P3', vArquivo);
    vExec.CarregarClob('P4', TStringStream.Create(pTextoXML) );

    vExec.Executar;

    vExec.Limpar;
    vExec.Add('update NOTAS_FISCAIS_CARTAS_CORRECOES set ');
    vExec.Add('  STATUS = ''E'', ');
    vExec.Add('  PROTOCOLO_NFE = :P3 ');
    vExec.Add('where NOTA_FISCAL_ID = :P1');
    vExec.Add('and SEQUENCIA = :P2');

    vExec.Executar([pNotaFiscalId, pSequencia, pProtocoloNFe]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  vExec.Free;
end;


function BuscarNotasFiscaisCartasCorrecoes(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecNotasFiscaisCartasCorrecoes>;
var
  i: Integer;
  t: TNotasFiscaisCartasCorrecoes;
begin
  Result := nil;
  t := TNotasFiscaisCartasCorrecoes.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordNotasFiscaisCartasCorrecoes;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirNotasFiscaisCartasCorrecoes(
  pConexao: TConexao;
  pNotaFiscalId: Integer;
  pSequencia: Integer
): RecRetornoBD;
var
  t: TNotasFiscaisCartasCorrecoes;
begin
  Result.Iniciar;
  t := TNotasFiscaisCartasCorrecoes.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('NOTA_FISCAL_ID', pNotaFiscalId, True);
    t.setInt('SEQUENCIA', pSequencia, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
