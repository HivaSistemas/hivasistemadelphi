unit _EntregasItens;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsExpedicao;

{$M+}
type
  TEntregaItem = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordEntregaItem: RecEntregaItem;
  end;

function BuscarEntregaItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEntregaItem>;

function BuscarEntregaItensComando(pConexao: TConexao; pComando: string): TArray<RecEntregaItem>;

function ExcluirEntregaItem(
  pConexao: TConexao;
  pEntregaId: Integer;
  pProdutoId: Integer;
  pItemId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TEntregaItem }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ITE.ENTREGA_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ENT.STATUS = ''ESE'' ' +
      'and PRO.EXIGIR_SEPARACAO = ''S'' ' +
      'and ENT.ENTREGA_ID = :P1 '
    );
end;

constructor TEntregaItem.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ENTREGAS_ITENS');

  FSql :=
    'select ' +
    '  ITE.ENTREGA_ID, ' +
    '  ITE.PRODUTO_ID, ' +
    '  PRO.NOME as NOME_PRODUTO, ' +
    '  MAR.NOME as NOME_MARCA, ' +
    '  ITE.ITEM_ID, ' +
    '  ITE.QUANTIDADE, ' +
    '  PRO.UNIDADE_VENDA as UNIDADE, ' +
    '  PRO.UNIDADE_ENTREGA_ID, ' +
    '  ITE.LOTE, ' +
    '  PRO.MULTIPLO_VENDA, ' +
    '  ITE.QUANTIDADE * COALESCE(PRO.PESO, 0) as PESO_TOTAL, ' +
    '  ITE.DEVOLVIDOS, ' +
    '  ITE.RETORNADOS, ' +
    '  ITE.NAO_SEPARADOS, ' +
    '  ENT.DATA_HORA_CADASTRO, ' +
    '  PRO.CODIGO_ORIGINAL_FABRICANTE, ' +
    '  PRO.CODIGO_BARRAS, ' +
    '  PRO.CONF_SOMENTE_CODIGO_BARRAS, ' +
    '  OIT.TIPO_CONTROLE_ESTOQUE ' +
    'from ' +
    '  ENTREGAS_ITENS ITE ' +

    'inner join ENTREGAS ENT ' +
    'on ITE.ENTREGA_ID = ENT.ENTREGA_ID ' +

    'inner join PRODUTOS PRO ' +
    'on ITE.PRODUTO_ID = PRO.PRODUTO_ID ' +

    'inner join MARCAS MAR ' +
    'on PRO.MARCA_ID = MAR.MARCA_ID ' +

    'inner join VW_ORC_ITENS_SEM_KITS_AGRUPADO OIT ' +
    'on ENT.ORCAMENTO_ID = OIT.ORCAMENTO_ID ' +
    'and ITE.ITEM_ID = OIT.ITEM_ID ';

  setFiltros(getFiltros);

  AddColuna('ENTREGA_ID', True);
  AddColuna('PRODUTO_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('QUANTIDADE');
  AddColunaSL('NOME_PRODUTO');
  AddColunaSL('NOME_MARCA');
  AddColunaSL('UNIDADE');
  AddColunaSL('UNIDADE_ENTREGA_ID');
  AddColunaSL('LOTE');
  AddColunaSL('PESO_TOTAL');
  AddColunaSL('MULTIPLO_VENDA');
  AddColunaSL('DEVOLVIDOS');
  AddColunaSL('RETORNADOS');
  AddColunaSL('NAO_SEPARADOS');
  AddColunaSL('DATA_HORA_CADASTRO');
  AddColunaSL('CODIGO_ORIGINAL_FABRICANTE');
  AddColunaSL('CODIGO_BARRAS');
  AddColunaSL('CONF_SOMENTE_CODIGO_BARRAS');
  AddColunaSL('TIPO_CONTROLE_ESTOQUE');
end;

function TEntregaItem.getRecordEntregaItem: RecEntregaItem;
begin
  Result.EntregaId                 := getInt('ENTREGA_ID', True);
  Result.ProdutoId                 := getInt('PRODUTO_ID', True);
  Result.ItemId                    := getInt('ITEM_ID', True);
  Result.Quantidade                := getDouble('QUANTIDADE');
  Result.NomeProduto               := getString('NOME_PRODUTO');
  Result.NomeMarca                 := getString('NOME_MARCA');
  Result.Unidade                   := getString('UNIDADE');
  Result.UnidadeEntregaId          := getString('UNIDADE_ENTREGA_ID');
  Result.Lote                      := getString('LOTE');
  Result.PesoTotal                      := getDouble('PESO_TOTAL');
  Result.MultiploVenda             := getDouble('MULTIPLO_VENDA');
  Result.Devolvidos                := getDouble('DEVOLVIDOS');
  Result.Retornados                := getDouble('RETORNADOS');
  Result.NaoSeparados              := getDouble('NAO_SEPARADOS');
  Result.DataHoraCadastro          := getData('DATA_HORA_CADASTRO');
  Result.CodigoOriginalFabricante  := getString('CODIGO_ORIGINAL_FABRICANTE');
  Result.CodigoBarras              := getString('CODIGO_BARRAS');
  Result.ConfSomenteCodigoBarras   := getString('CONF_SOMENTE_CODIGO_BARRAS');
  Result.TipoControleEstoque       := getString('TIPO_CONTROLE_ESTOQUE');
end;

function BuscarEntregaItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEntregaItem>;
var
  i: Integer;
  t: TEntregaItem;
begin
  Result := nil;
  t := TEntregaItem.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordEntregaItem;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarEntregaItensComando(pConexao: TConexao; pComando: string): TArray<RecEntregaItem>;
var
  i: Integer;
  t: TEntregaItem;
begin
  Result := nil;
  t := TEntregaItem.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordEntregaItem;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirEntregaItem(
  pConexao: TConexao;
  pEntregaId: Integer;
  pProdutoId: Integer;
  pItemId: Integer
): RecRetornoBD;
var
  t: TEntregaItem;
begin
  Result.TeveErro := False;
  t := TEntregaItem.Create(pConexao);

  try
    t.setInt('ENTREGA_ID', pEntregaId, True);
    t.setInt('PRODUTO_ID', pProdutoId, True);
    t.setInt('ITEM_ID', pItemId, True);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
