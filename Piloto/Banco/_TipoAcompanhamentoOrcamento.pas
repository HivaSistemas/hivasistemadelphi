unit _TipoAcompanhamentoOrcamento;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _Sessao, _RecordsCadastros;

{$M+}
type
  TTipoAcompanhamentoOrcamento = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordTipoAcompanhamentoOrcamento: RecTipoAcompanhamentoOrcamento;
  end;

function AtualizarTipoAcompanhamentoOrcamento(
  pConexao: TConexao;
  pTipoAcompanhamentoId: Integer;
  pDescricao: string;
  pAtivo: string
): RecRetornoBD;

function BuscarTipoAcompanhamentoOrcamento(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTipoAcompanhamentoOrcamento>;

function ExcluirTipoAcompanhamentoOrcamento(
  pConexao: TConexao;
  pTipoAcompanhamentoId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TTipoAcompanhamentoOrcamento }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where TIPO_ACOMPANHAMENTO_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Descri��o',
      True,
      0,
      'where DESCRICAO like :P1 || ''%'' ' +
      'order by ' +
      '  DESCRICAO '
    );
end;

constructor TTipoAcompanhamentoOrcamento.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'TIPO_ACOMPANHAMENTO_ORCAMENTO');

  FSql :=
    'select ' +
    '  TIPO_ACOMPANHAMENTO_ID, ' +
    '  DESCRICAO, ' +
    '  ATIVO ' +
    'from ' +
    '  TIPO_ACOMPANHAMENTO_ORCAMENTO ';

  setFiltros(getFiltros);

  AddColuna('TIPO_ACOMPANHAMENTO_ID', True);
  AddColuna('DESCRICAO');
  AddColuna('ATIVO');
end;

function TTipoAcompanhamentoOrcamento.getRecordTipoAcompanhamentoOrcamento: RecTipoAcompanhamentoOrcamento;
begin
  Result := RecTipoAcompanhamentoOrcamento.Create;
  Result.tipo_acompanhamento_id := getInt('TIPO_ACOMPANHAMENTO_ID', True);
  Result.descricao              := getString('DESCRICAO');
  Result.ativo                  := getString('ATIVO');
end;

function AtualizarTipoAcompanhamentoOrcamento(
  pConexao: TConexao;
  pTipoAcompanhamentoId: Integer;
  pDescricao: string;
  pAtivo: string
): RecRetornoBD;
var
  t: TTipoAcompanhamentoOrcamento;
  vNovo: Boolean;
  seq: TSequencia;
begin
  Result.Iniciar;
  t := TTipoAcompanhamentoOrcamento.Create(pConexao);

  vNovo := pTipoAcompanhamentoId = 0;
  if vNovo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_TIP_ACO_ORC_ID');
    pTipoAcompanhamentoId := seq.getProximaSequencia;
    result.AsInt := pTipoAcompanhamentoId;
    seq.Free;
  end;

  try
    pConexao.IniciarTransacao;

    t.setInt('TIPO_ACOMPANHAMENTO_ID', pTipoAcompanhamentoId, True);
    t.setString('DESCRICAO', pDescricao);
    t.setString('ATIVO', pAtivo);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarTipoAcompanhamentoOrcamento(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTipoAcompanhamentoOrcamento>;
var
  i: Integer;
  t: TTipoAcompanhamentoOrcamento;
begin
  Result := nil;
  t := TTipoAcompanhamentoOrcamento.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordTipoAcompanhamentoOrcamento;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirTipoAcompanhamentoOrcamento(
  pConexao: TConexao;
  pTipoAcompanhamentoId: Integer
): RecRetornoBD;
var
  t: TTipoAcompanhamentoOrcamento;
begin
  Result.TeveErro := False;
  t := TTipoAcompanhamentoOrcamento.Create(pConexao);

  try
    t.setInt('TIPO_ACOMPANHAMENTO_ID', pTipoAcompanhamentoId, True);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
