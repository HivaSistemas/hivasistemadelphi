unit _ContasReceberBaixas;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsFinanceiros, _RecordsOrcamentosVendas,
  _ContasRecBaixasPagamentos, _ContasRecBaixasPagtosChq, System.StrUtils, _ContasRecBaixasPagtosDin, _ContasRecBaixasCreditos, _ContasRecBaixasPagtosPix;

{$M+}
type
  TContaReceberBaixa = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordContaReceberBaixa: RecContaReceberBaixa;
  end;

function AtualizarContaReceberBaixa(
  pConexao: TConexao;
  pBaixaId: Integer;
  pEmpresaId: Integer;
  pCadastroId: Integer;
  pTurnoId: Integer;

  pValorTitulos: Currency;
  pValorMulta: Currency;
  pValorJuros: Currency;
  pValorDesconto: Double;
  pValorRetencao: Currency;
  pValorAdiantado: Currency;

  pValorDinheiro: Double;
  pValorCheque: Double;
  pValorCartaoDebito: Double;
  pValorCartaoCredito: Double;
  pValorCobranca: Double;
  pValorCredito: Double;

  pDataPagamento: TDateTime;
  pReceberCaixa: string;
  pObservacoes: string;
  pRecebido: string;
  pTipo: string;
  pValorTroco: Currency;
  pDinheiro: TArray<RecTitulosBaixasPagtoDin>;
  pCartoesDebito: TArray<RecTitulosFinanceiros>;
  pCartoesCredito: TArray<RecTitulosFinanceiros>;
  pCobrancas: TArray<RecTitulosFinanceiros>;
  pCheques: TArray<RecTitulosFinanceiros>;
  pCreditosIds: TArray<Integer>;

  pReceberAdiantadoId: Integer;
  pValorCreditoGerar: Currency;
  pTransacaoAberta: Boolean;

  pValorPix: Double;
  pPix: TArray<RecPagamentosPix>;
  pBaixaPixRecebimentoCaixa: string
): RecRetornoBD;

function BuscarContaReceberBaixas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecContaReceberBaixa>;

function ReceberBaixaNoCaixa(pConexao: TConexao; pBaixaId: Integer; pTurnoId: Integer; pCartoes: TArray<RecTitulosFinanceiros>): RecRetornoBD;
function CancelarBaixa(pConexao: TConexao; const pReceberBaixaId: Integer; pProcessoCaixa: Boolean): RecRetornoBD;
function getQtdeMaximaParcelas(pConexao: TConexao; pBaixaId: Integer): Integer;
function ExisteBaixaPendenteAdiantamentoAcumulado(pConexao: TConexao; pPedidosIds: TArray<Integer>): Boolean;

function LancarAdiantamentoContasRceber(pConexao: TConexao; const pReceberId: Integer; pEmTransacao: Boolean): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TContaReceberBaixa }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 3);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CRB.BAIXA_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CRB.EMPRESA_ID = :P1 ' +
      'and CRB.RECEBER_CAIXA = ''S'' ' +
      'and CRB.DATA_PAGAMENTO is null '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CRB.RECEBER_ADIANTADO_ID = :P1'
    );
end;

constructor TContaReceberBaixa.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONTAS_RECEBER_BAIXAS');

  FSql :=
    'select ' +
    '  CRB.BAIXA_ID, ' +
    '  CRB.EMPRESA_ID, ' +
    '  CRB.CADASTRO_ID, ' +
    '  CAD.RAZAO_SOCIAL as NOME_CLIENTE, ' +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA_BAIXA, ' +
    '  CRB.USUARIO_BAIXA_ID, ' +
    '  UBA.NOME as NOME_USUARIO_BAIXA, ' +
    '  CRB.VALOR_TITULOS, ' +
    '  CRB.VALOR_LIQUIDO, ' +
    '  CRB.VALOR_MULTA, ' +
    '  CRB.VALOR_JUROS, ' +
    '  CRB.VALOR_RETENCAO, ' +
    '  CRB.VALOR_DESCONTO, ' +
    '  CRB.VALOR_ADIANTADO, ' +
    '  CRB.VALOR_PIX, ' +
    '  CRB.VALOR_DINHEIRO, ' +
    '  CRB.VALOR_CHEQUE, ' +
    '  CRB.VALOR_CARTAO_DEBITO, ' +
    '  CRB.VALOR_CARTAO_CREDITO, ' +
    '  CRB.VALOR_COBRANCA, ' +
    '  CRB.VALOR_CREDITO, ' +
    '  CRB.VALOR_TROCO, ' +
    '  CRB.RECEBIDO, ' +
    '  CRB.RECEBER_ADIANTADO_ID, ' +
    '  CRB.DATA_HORA_BAIXA, ' +
    '  CRB.DATA_PAGAMENTO, ' +
    '  CRB.RECEBER_CAIXA, ' +
    '  CRB.OBSERVACOES, ' +
    '  CRB.TURNO_ID, ' +
    '  CRB.TIPO, ' +
    '  CRB.BAIXA_PAGAR_ORIGEM_ID, ' +
    '  CRB.USUARIO_ENVIO_CAIXA_ID, ' +
    '  CRB.BAIXA_PIX_REC_CX, ' +
    '  ENV.NOME as NOME_USUARIO_ENV_CAIXA ' +
    'from ' +
    '  CONTAS_RECEBER_BAIXAS CRB ' +

    'inner join EMPRESAS EMP ' +
    'on CRB.EMPRESA_ID = EMP.EMPRESA_ID ' +

    'left join FUNCIONARIOS UBA ' +
    'on CRB.USUARIO_BAIXA_ID = UBA.FUNCIONARIO_ID ' +

    'left join FUNCIONARIOS ENV ' +
    'on CRB.USUARIO_ENVIO_CAIXA_ID = ENV.FUNCIONARIO_ID ' +

    'left join CADASTROS CAD ' +
    'on CRB.CADASTRO_ID = CAD.CADASTRO_ID ';

  setFiltros(getFiltros);

  AddColuna('BAIXA_ID', True);
  AddColuna('EMPRESA_ID');
  AddColuna('CADASTRO_ID');
  AddColunaSL('NOME_EMPRESA_BAIXA');
  AddColunaSL('USUARIO_BAIXA_ID');
  AddColunaSL('NOME_USUARIO_BAIXA');
  AddColuna('VALOR_TITULOS');
  AddColunaSL('VALOR_LIQUIDO');
  AddColuna('VALOR_MULTA');
  AddColuna('VALOR_JUROS');
  AddColuna('VALOR_RETENCAO');
  AddColuna('VALOR_DESCONTO');
  AddColuna('VALOR_ADIANTADO');
  AddColuna('VALOR_PIX');
  AddColuna('VALOR_DINHEIRO');
  AddColuna('VALOR_CHEQUE');
  AddColuna('VALOR_CARTAO_DEBITO');
  AddColuna('VALOR_CARTAO_CREDITO');
  AddColuna('VALOR_COBRANCA');
  AddColuna('VALOR_CREDITO');
  AddColuna('VALOR_TROCO');
  AddColuna('RECEBIDO');
  AddColuna('RECEBER_ADIANTADO_ID');
  AddColunaSL('DATA_HORA_BAIXA');
  AddColuna('DATA_PAGAMENTO');
  AddColuna('RECEBER_CAIXA');
  AddColuna('OBSERVACOES');
  AddColuna('TIPO');
  AddColuna('TURNO_ID');
  AddColunaSL('USUARIO_ENVIO_CAIXA_ID');
  AddColunaSL('NOME_USUARIO_ENV_CAIXA');
  AddColunaSL('NOME_CLIENTE');
  AddColunaSL('BAIXA_PAGAR_ORIGEM_ID');
  AddColuna('BAIXA_PIX_REC_CX');
end;

function TContaReceberBaixa.getRecordContaReceberBaixa: RecContaReceberBaixa;
begin
  Result.BaixaId                    := getInt('BAIXA_ID', True);
  Result.EmpresaId                  := getInt('EMPRESA_ID');
  Result.CadastroId                 := getInt('CADASTRO_ID');
  Result.NomeCliente                := getString('NOME_CLIENTE');
  Result.nome_empresa_baixa         := getString('NOME_EMPRESA_BAIXA');
  Result.usuario_baixa_id           := getInt('USUARIO_BAIXA_ID');
  Result.nome_usuario_baixa         := getString('NOME_USUARIO_BAIXA');
  Result.ValorTitulos               := getDouble('VALOR_TITULOS');
  Result.ValorLiquido               := getDouble('VALOR_LIQUIDO');
  Result.ValorMulta                 := getDouble('VALOR_MULTA');
  Result.ValorJuros                 := getDouble('VALOR_JUROS');
  Result.ValorRetencao              := getDouble('VALOR_RETENCAO');
  Result.valor_desconto             := getDouble('VALOR_DESCONTO');
  Result.ValorAdiantado             := getDouble('VALOR_ADIANTADO');
  Result.valor_dinheiro             := getDouble('VALOR_DINHEIRO');
  Result.valor_pix                  := getDouble('VALOR_PIX');
  Result.valor_cheque               := getDouble('VALOR_CHEQUE');
  Result.ValorCartaoDebito          := getDouble('VALOR_CARTAO_DEBITO');
  Result.ValorCartaoCredito         := getDouble('VALOR_CARTAO_CREDITO');
  Result.valor_cobranca             := getDouble('VALOR_COBRANCA');
  Result.valor_credito              := getDouble('VALOR_CREDITO');
  Result.data_hora_baixa            := getData('DATA_HORA_BAIXA');
  Result.data_pagamento             := getData('DATA_PAGAMENTO');
  Result.ReceberCaixa               := getString('RECEBER_CAIXA');
  Result.observacoes                := getString('OBSERVACOES');
  Result.Tipo                       := getString('TIPO');
  Result.UsuarioEnvioCaixaId        := getInt('USUARIO_ENVIO_CAIXA_ID');
  Result.NomeUsuarioEnvCaixa        := getString('NOME_USUARIO_ENV_CAIXA');
  Result.TurnoId                    := getInt('TURNO_ID');
  Result.Recebido                   := getString('RECEBIDO');
  Result.ReceberAdiantadoId         := getInt('RECEBER_ADIANTADO_ID');
  Result.ValorTroco                 := getDouble('VALOR_TROCO');
  Result.BaixaPagarOrigemId         := getInt('BAIXA_PAGAR_ORIGEM_ID');
  Result.BaixaPixRecCx              := getString('BAIXA_PIX_REC_CX');

  Result.TipoAnalitico :=
    _Biblioteca.Decode(
      Result.Tipo,[
      'CRE', 'Cr�dito a pagar',
      'BCR', 'Baixa de t�tulos a rec.',
      'ECR', 'Encontro de t�tulos a rec.',
      'ADI', 'Adiantamento',
      'PIX', 'Pix'
      ]
    );
end;

function AtualizarContaReceberBaixa(
  pConexao: TConexao;
  pBaixaId: Integer;
  pEmpresaId: Integer;
  pCadastroId: Integer;
  pTurnoId: Integer;

  pValorTitulos: Currency;
  pValorMulta: Currency;
  pValorJuros: Currency;
  pValorDesconto: Double;
  pValorRetencao: Currency;
  pValorAdiantado: Currency;

  pValorDinheiro: Double;
  pValorCheque: Double;
  pValorCartaoDebito: Double;
  pValorCartaoCredito: Double;
  pValorCobranca: Double;
  pValorCredito: Double;

  pDataPagamento: TDateTime;
  pReceberCaixa: string;
  pObservacoes: string;
  pRecebido: string;
  pTipo: string;
  pValorTroco: Currency;
  pDinheiro: TArray<RecTitulosBaixasPagtoDin>;
  pCartoesDebito: TArray<RecTitulosFinanceiros>;
  pCartoesCredito: TArray<RecTitulosFinanceiros>;
  pCobrancas: TArray<RecTitulosFinanceiros>;
  pCheques: TArray<RecTitulosFinanceiros>;
  pCreditosIds: TArray<Integer>;

  pReceberAdiantadoId: Integer;
  pValorCreditoGerar: Currency;
  pTransacaoAberta: Boolean;

  pValorPix: Double;
  pPix: TArray<RecPagamentosPix>;
  pBaixaPixRecebimentoCaixa: string
): RecRetornoBD;
var
  t: TContaReceberBaixa;
  vPag: TContasRecBaixasPagamentos;
  vPagChq: TContasRecBaixasPagtosChq;
  vPagDin: TContasRecBaixasPagtosDin;
  vPagPix: TContasRecBaixasPagtosPix;
  vCreditos: TContasRecBaixasCreditos;

  i: Integer;
  vNovo: Boolean;
  vSeq: TSequencia;
  vExec: TExecucao;
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_CONTAS_REC_BAIXA');

  vExec := TExecucao.Create(pConexao);
  t := TContaReceberBaixa.Create(pConexao);
  vPag := TContasRecBaixasPagamentos.Create(pConexao);
  vPagChq := TContasRecBaixasPagtosChq.Create(pConexao);
  vPagDin := TContasRecBaixasPagtosDin.Create(pConexao);
  vPagPix := TContasRecBaixasPagtosPix.Create(pConexao);
  vCreditos := TContasRecBaixasCreditos.Create(pConexao);

  vProc := TProcedimentoBanco.Create(pConexao, 'CONSOLIDAR_BAIXA_CONTAS_REC');

  vNovo := pBaixaId = 0;
  if vNovo then begin
    pBaixaId := TSequencia.Create(pConexao, 'SEQ_CONTAS_RECEBER_BAIXAS').getProximaSequencia;
    Result.AsInt := pBaixaId;
  end;

  try
    if not pTransacaoAberta then
      pConexao.IniciarTransacao;

    t.setInt('BAIXA_ID', pBaixaId, True);
    t.setInt('EMPRESA_ID', pEmpresaId);
    t.setIntN('CADASTRO_ID', pCadastroId);
    t.setIntN('TURNO_ID', pTurnoId);
    t.setDouble('VALOR_TITULOS', pValorTitulos);
    t.setDouble('VALOR_MULTA', pValorMulta);
    t.setDouble('VALOR_JUROS', pValorJuros);
    t.setDouble('VALOR_RETENCAO', pValorRetencao);
    t.setDouble('VALOR_DESCONTO', pValorDesconto);
    t.setDouble('VALOR_ADIANTADO', pValorAdiantado);
    t.setDouble('VALOR_PIX', pValorPix);
    t.setDouble('VALOR_DINHEIRO', pValorDinheiro);
    t.setDouble('VALOR_CHEQUE', pValorCheque);
    t.setDouble('VALOR_CARTAO_DEBITO', pValorCartaoDebito);
    t.setDouble('VALOR_CARTAO_CREDITO', pValorCartaoCredito);
    t.setDouble('VALOR_COBRANCA', pValorCobranca);
    t.setDouble('VALOR_CREDITO', pValorCredito);
    t.setDataN('DATA_PAGAMENTO', pDataPagamento);
    t.setString('RECEBER_CAIXA', pReceberCaixa);
    t.setString('TIPO', pTipo);
    t.setString('OBSERVACOES', pObservacoes);
    t.setDouble('VALOR_TROCO', pValorTroco);
    t.setString('RECEBIDO', pRecebido);
    t.setIntN('RECEBER_ADIANTADO_ID', pReceberAdiantadoId);
    t.setString('BAIXA_PIX_REC_CX', pBaixaPixRecebimentoCaixa);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    vExec.Clear;
    vExec.Add('delete from CONTAS_REC_BAIXAS_PAGTOS_DIN ');
    vExec.Add('where BAIXA_ID = :P1');
    vExec.Executar([pBaixaId]);
    for i := Low(pDinheiro) to High(pDinheiro) do begin
      if Trim(pDinheiro[i].ContaId) = '' then
        Continue;

      vPagDin.setInt('BAIXA_ID', pBaixaId, True);
      vPagDin.setString('CONTA_ID', pDinheiro[i].ContaId, True);
      vPagDin.setDouble('VALOR', pDinheiro[i].Valor);

      vPagDin.Inserir;
    end;

    vExec.Clear;
    vExec.Add('delete from CONTAS_REC_BAIXAS_PAGTOS_PIX ');
    vExec.Add('where BAIXA_ID = :P1');
    vExec.Executar([pBaixaId]);
    for i := Low(pPix) to High(pPix) do begin
      vPagDin.setInt('BAIXA_ID', pBaixaId, True);
      vPagDin.setString('CONTA_ID', pPix[i].ContaId, True);
      vPagDin.setDouble('VALOR', pPix[i].Valor);

      vPagDin.Inserir;
    end;

    vExec.Clear;
    vExec.Add('delete from CONTAS_REC_BAIXAS_PAGAMENTOS ');
    vExec.Add('where BAIXA_ID = :P1');
    vExec.Executar([pBaixaId]);

    vPag.setInt('BAIXA_ID', pBaixaId, True);
    vPag.setString('TIPO', 'CR');
    for i := Low(pCartoesDebito) to High(pCartoesDebito) do begin
      vPag.setInt('COBRANCA_ID', pCartoesDebito[i].CobrancaId, True);
      vPag.setInt('ITEM_ID', i + 1, True);
      vPag.setIntN('PARCELA', 0);
      vPag.setIntN('NUMERO_PARCELAS', 0);
      vPag.setDataN('DATA_VENCIMENTO', 0);
      vPag.setString('NSU_TEF', '');
      vPag.setString('TIPO_RECEB_CARTAO', 'POS');
      vPag.setDouble('VALOR', pCartoesDebito[i].Valor);

      vPag.Inserir;
    end;

    for i := Low(pCartoesCredito) to High(pCartoesCredito) do begin
      vPag.setInt('COBRANCA_ID', pCartoesCredito[i].CobrancaId, True);
      vPag.setInt('ITEM_ID', Length(pCartoesDebito) + i + 1, True);
      vPag.setIntN('PARCELA', 0);
      vPag.setIntN('NUMERO_PARCELAS', 0);
      vPag.setDataN('DATA_VENCIMENTO', 0);
      vPag.setStringN('NSU_TEF', '');
      vPag.setString('TIPO_RECEB_CARTAO', 'POS');
      vPag.setDouble('VALOR', pCartoesCredito[i].Valor);

      vPag.Inserir;
    end;

    vPag.setInt('BAIXA_ID', pBaixaId, True);
    vPag.setString('TIPO', 'CO');
    for i := Low(pCobrancas) to High(pCobrancas) do begin
      vPag.setInt('COBRANCA_ID', pCobrancas[i].CobrancaId, True);
      vPag.setInt('ITEM_ID', Length(pCartoesDebito) + Length(pCartoesCredito) + i + 1, True);
      vPag.setIntN('PARCELA', pCobrancas[i].Parcela);
      vPag.setIntN('NUMERO_PARCELAS', pCobrancas[i].NumeroParcelas);
      vPag.setDataN('DATA_VENCIMENTO', pCobrancas[i].DataVencimento);
      vPag.setStringN('NSU_TEF', '');
      vPag.setDouble('VALOR', pCobrancas[i].Valor);

      vPag.Inserir;
    end;

    vExec.Clear;
    vExec.Add('delete from CONTAS_REC_BAIXAS_PAGTOS_CHQ ');
    vExec.Add('where BAIXA_ID = :P1');
    vExec.Executar([pBaixaId]);

    vPagChq.setInt('BAIXA_ID', pBaixaId, True);
    for i := Low(pCheques) to High(pCheques) do begin
      vPagChq.setInt('COBRANCA_ID', pCheques[i].CobrancaId, True);
      vPagChq.setInt('ITEM_ID', i + 1, True);
      vPagChq.setIntN('PARCELA', pCheques[i].Parcela);
      vPagChq.setIntN('NUMERO_PARCELAS', pCheques[i].NumeroParcelas);
      vPagChq.setDataN('DATA_VENCIMENTO', pCheques[i].DataVencimento);
      vPagChq.setDouble('VALOR_CHEQUE', pCheques[i].Valor);
      vPagChq.setString('BANCO', pCheques[i].Banco);
      vPagChq.setString('AGENCIA', pCheques[i].Agencia);
      vPagChq.setString('CONTA_CORRENTE', pCheques[i].ContaCorrente);
      vPagChq.setInt('NUMERO_CHEQUE', pCheques[i].NumeroCheque);
      vPagChq.setString('NOME_EMITENTE', pCheques[i].NomeEmitente);
      vPagChq.setString('CPF_CNPJ_EMITENTE', pCheques[i].CpfCnpjEmitente);
      vPagChq.setString('TELEFONE_EMITENTE', pCheques[i].TelefoneEmitente);

      vPagChq.Inserir;
    end;

    for i := Low(pCreditosIds) to High(pCreditosIds) do begin
      vCreditos.setInt('BAIXA_ID', pBaixaId, True);
      vCreditos.setInt('PAGAR_ID', pCreditosIds[i], True);

      vCreditos.Inserir;
    end;

    if pReceberCaixa = 'N' then
      vProc.Executar([pBaixaId, pTurnoId]);
      //vProc.Executar([pBaixaId, pCadastroId, pValorCreditoGerar]);

    if not pTransacaoAberta then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pTransacaoAberta then
        pConexao.VoltarTransacao;

      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vCreditos.Free;
  vPagDin.Free;
  vPagChq.Free;
  vExec.Free;
  vProc.Free;
  vPag.Free;
  t.Free;
end;

function BuscarContaReceberBaixas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecContaReceberBaixa>;
var
  i: Integer;
  t: TContaReceberBaixa;
begin
  Result := nil;
  t := TContaReceberBaixa.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordContaReceberBaixa;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ReceberBaixaNoCaixa(pConexao: TConexao; pBaixaId: Integer; pTurnoId: Integer; pCartoes: TArray<RecTitulosFinanceiros>): RecRetornoBD;
var
  i: Integer;
  vSql: TExecucao;
  vProc: TProcedimentoBanco;

  vPag: TContasRecBaixasPagamentos;
begin
  Result.Iniciar;
  pConexao.SetRotina('RECEBER_BAIXA_CONTAS_REC_CAIXA');

  vSql := TExecucao.Create(pConexao);
  vPag := TContasRecBaixasPagamentos.Create(pConexao);
  vProc := TProcedimentoBanco.Create(pConexao, 'RECEBER_CONTAS_RECEBER_BAIXA');

  try
    pConexao.IniciarTransacao;

    vSql.Clear;
    vSql.Add('delete from CONTAS_REC_BAIXAS_PAGAMENTOS ');
    vSql.Add('where BAIXA_ID = :P1 ');
    vSql.Add('and TIPO = ''CR'' ');
    vSql.Executar([pBaixaId]);

    vPag.setInt('BAIXA_ID', pBaixaId, True);
    vPag.setString('TIPO', 'CR');
    for i := Low(pCartoes) to High(pCartoes) do begin
      vPag.setInt('COBRANCA_ID', pCartoes[i].CobrancaId, True);
      vPag.setInt('ITEM_ID', i + 1, True);
      vPag.setIntN('PARCELA', 0);
      vPag.setIntN('NUMERO_PARCELAS', 0);
      vPag.setDataN('DATA_VENCIMENTO', 0);

      vPag.setStringN('NSU_TEF', pCartoes[i].NsuTef);
      vPag.setStringN('CODIGO_AUTORIZACAO', pCartoes[i].CodigoAutorizacao);
      vPag.setStringN('NUMERO_CARTAO', pCartoes[i].NumeroCartao);
      vPag.setStringN('TIPO_RECEB_CARTAO', pCartoes[i].TipoRecebCartao);

      vPag.setDouble('VALOR', pCartoes[i].Valor);

      vPag.Inserir;
    end;

    vProc.Params[0].AsInteger := pBaixaId;
    vProc.Params[1].AsInteger := pTurnoId;

    vProc.Executar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      pConexao.VoltarTransacao;
      Result.MensagemErro := e.Message;
    end;
  end;
  vPag.Free;
  vSql.Free;
  vProc.Free;
end;

function CancelarBaixa(pConexao: TConexao; const pReceberBaixaId: Integer; pProcessoCaixa: Boolean): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.setRotina('CANCELAR_BAIXA_CONTAS_RECEBER');
  vProc := TProcedimentoBanco.Create(pConexao, 'CANCELAR_BAIXA_CONTAS_RECEBER');

  try
    pConexao.IniciarTransacao;

    vProc.Params[0].AsInteger := pReceberBaixaId;
    vProc.Params[1].AsString := IIfStr(pProcessoCaixa , 'S', 'N');
    vProc.ExecProc;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  vProc.Free;
end;

function getQtdeMaximaParcelas(pConexao: TConexao; pBaixaId: Integer): Integer;
var
  vSql: TConsulta;
begin
  Result := 1;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  max(QTDE_PARCELAS) as QTDE_PARCELAS ');
  vSql.Add('from ( ');
  vSql.Add('  select ');
  vSql.Add('    CRP.COBRANCA_ID, ');
  vSql.Add('    count(*) as QTDE_PARCELAS ');
  vSql.Add('  from ');
  vSql.Add('    CONTAS_REC_BAIXAS_PAGAMENTOS CRP ');

  vSql.Add('  inner join TIPO_COBRANCA_DIAS_PRAZO TCO ');
  vSql.Add('  on CRP.COBRANCA_ID = TCO.COBRANCA_ID ');

  vSql.Add('  where CRP.BAIXA_ID = :P1 ');

  vSql.Add('  group by ');
  vSql.Add('    CRP.COBRANCA_ID ');
  vSql.Add(') ');

  if vSql.Pesquisar([pBaixaId]) then
    Result := vSql.GetInt(0);

  vSql.Free;
end;

function ExisteBaixaPendenteAdiantamentoAcumulado(pConexao: TConexao; pPedidosIds: TArray<Integer>): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  count(*) ');
  vSql.Add('from ');
  vSql.Add('  CONTAS_RECEBER_BAIXAS CRB ');

  vSql.Add('inner join CONTAS_RECEBER_BAIXAS_ITENS ITE ');
  vSql.Add('on CRB.BAIXA_ID = ITE.BAIXA_ID ');

  vSql.Add('inner join CONTAS_RECEBER COR ');
  vSql.Add('on ITE.RECEBER_ID = COR.RECEBER_ID ');

  vSql.Add('where ' + FiltroInInt('COR.ORCAMENTO_ID', pPedidosIds) );
  vSql.Add('and CRB.RECEBER_CAIXA = ''S'' ');
  vSql.Add('and CRB.RECEBIDO = ''N'' ');

  vSql.Pesquisar;
  Result := vSql.GetInt(0) > 0;

  vSql.Free;
end;

function LancarAdiantamentoContasRceber(pConexao: TConexao; const pReceberId: Integer; pEmTransacao: Boolean): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.setRotina('LANCAR_ADIANTAMENTO_CR');
  vProc := TProcedimentoBanco.Create(pConexao, 'LANCAR_ADIANTAMENTO_CONTAS_REC');

  try
    if not pEmTransacao then
      pConexao.IniciarTransacao;

    vProc.Params[0].AsInteger := pReceberId;

    vProc.ExecProc;

    Result.AsInt := vProc.Params[1].AsInteger;

    if not pEmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pEmTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;
  vProc.Free;
end;

end.
