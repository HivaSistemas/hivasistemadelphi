unit _BloqueiosEstBaixasItens;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecBloqueiosEstBaixasItens = record
    BaixaId: Integer;
    ItemId: Integer;
    Quantidade: Double;
  end;

  TBloqueiosEstBaixasItens = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordBloqueiosEstBaixasItens: RecBloqueiosEstBaixasItens;
  end;

function BuscarBloqueiosEstBaixasItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecBloqueiosEstBaixasItens>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TBloqueiosEstBaixasItens }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where BAIXA_ID = :P1'
    );
end;

constructor TBloqueiosEstBaixasItens.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'BLOQUEIOS_EST_BAIXAS_ITENS');

  FSql := 
    'select ' +
    '  BAIXA_ID, ' +
    '  ITEM_ID, ' +
    '  QUANTIDADE ' +
    'from ' +
    '  BLOQUEIOS_EST_BAIXAS_ITENS ';

  setFiltros(getFiltros);

  AddColuna('BAIXA_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('QUANTIDADE');
end;

function TBloqueiosEstBaixasItens.getRecordBloqueiosEstBaixasItens: RecBloqueiosEstBaixasItens;
begin
  Result.BaixaId                    := getInt('BAIXA_ID', True);
  Result.ItemId                     := getInt('ITEM_ID', True);
  Result.Quantidade                 := getDouble('QUANTIDADE');
end;

function BuscarBloqueiosEstBaixasItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecBloqueiosEstBaixasItens>;
var
  i: Integer;
  t: TBloqueiosEstBaixasItens;
begin
  Result := nil;
  t := TBloqueiosEstBaixasItens.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordBloqueiosEstBaixasItens;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
