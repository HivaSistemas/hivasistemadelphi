unit _CRM;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TCRM = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordCRM: RecCRM;
  end;

function AtualizarCRM(
  pConexao: TConexao;
  pCRMId: Integer;
  pCliente: Integer;
  pObservacao: string;
  pAcessosRemotos: TArray<RecCRMAcessoRemoto>;
  pBancoDados: TArray<RecCRMBancoDados>
): RecRetornoBD;

function BuscarCRM(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCRM>;

function ExcluirCRM(
  pConexao: TConexao;
  pCRMId: Integer;
  pClienteId: Integer
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TCRM }

uses _CRMAcessoRemoto, _CRMBancoDados;

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CENTRAL_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      1,
      'where CADASTRO_ID = :P1'
    );
end;

constructor TCRM.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CRM');

  FSql :=
    'select ' +
    '  CENTRAL_ID, ' +
    '  CADASTRO_ID, ' +
    '  OBSERVACAO ' +
    'from ' +
    '  CRM ';

  SetFiltros(GetFiltros);

  AddColuna('CENTRAL_ID', True);
  AddColuna('CADASTRO_ID', True);
  AddColuna('OBSERVACAO');
end;

function TCRM.GetRecordCRM: RecCRM;
begin
  Result := RecCRM.Create;
  Result.central_id  := GetInt('CENTRAL_ID', True);
  Result.cadastro_id := GetInt('CADASTRO_ID', True);
  Result.observacao  := getString('OBSERVACAO');
end;

function AtualizarCRM(
  pConexao: TConexao;
  pCRMId: Integer;
  pCliente: Integer;
  pObservacao: string;
  pAcessosRemotos: TArray<RecCRMAcessoRemoto>;
  pBancoDados: TArray<RecCRMBancoDados>
): RecRetornoBD;
var
  t: TCRM;
  acessoRemoto: TCRMAcessoRemoto;
  bancoDados: TCRMBancoDados;
  vExec: TExecucao;
  novo: Boolean;
  seq: TSequencia;
  i: Integer;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_CRM');

  t := TCRM.Create(pConexao);
  acessoRemoto := TCRMAcessoRemoto.Create(pConexao);
  bancoDados := TCRMBancoDados.Create(pConexao);
  vExec := TExecucao.Create(pConexao);

  novo := pCRMId = 0;
  if novo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_CRM_ID');
    pCRMId := seq.GetProximaSequencia;
    result.AsInt := pCRMId;
    seq.Free;
  end;

  t.SetInt('CENTRAL_ID', pCRMId, True);
  t.SetInt('CADASTRO_ID', pCliente, True);
  t.SetString('OBSERVACAO', pObservacao);

  try
    pConexao.IniciarTransacao;

    if novo then
      t.Inserir
    else
      t.Atualizar;

    vExec.Clear;
    vExec.Add('delete from CRM_ACESSO_REMOTO ');
    vExec.Add('where CENTRAL_ID = :P1 ');
    vExec.Executar([pCRMId]);

    for i := Low(pAcessosRemotos) to High(pAcessosRemotos) do begin
      acessoRemoto.setInt('CENTRAL_ID', pCRMId, True);
      acessoRemoto.setInt('ITEM_ID', pAcessosRemotos[i].item_id);
      acessoRemoto.setString('IP', pAcessosRemotos[i].ip);
      acessoRemoto.setString('SENHA', pAcessosRemotos[i].senha);
      acessoRemoto.setString('OBSERVACAO', pAcessosRemotos[i].observacao);
      acessoRemoto.setString('USUARIO', pAcessosRemotos[i].usuario);

      acessoRemoto.Inserir;
    end;

    vExec.Clear;
    vExec.Add('delete from CRM_BANCO_DADOS ');
    vExec.Add('where CENTRAL_ID = :P1 ');
    vExec.Executar([pCRMId]);

    for i := Low(pBancoDados) to High(pBancoDados) do begin
      bancoDados.setInt('CENTRAL_ID', pCRMId, True);
      bancoDados.setInt('ITEM_ID', pBancoDados[i].item_id);
      bancoDados.setString('IP_SERVIDOR', pBancoDados[i].ip_servidor);
      bancoDados.setString('SENHA', pBancoDados[i].senha);
      bancoDados.setString('USUARIO', pBancoDados[i].usuario);
      bancoDados.setString('PORTA', pBancoDados[i].porta);
      bancoDados.setString('SERVICO', pBancoDados[i].servico);

      bancoDados.Inserir;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
  acessoRemoto.Free;
  bancoDados.Free;
end;

function BuscarCRM(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCRM>;
var
  i: Integer;
  t: TCRM;
begin
  Result := nil;
  t := TCRM.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordCRM;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirCRM(
  pConexao: TConexao;
  pCRMId: Integer;
  pClienteId: Integer
): RecRetornoBD;
var
  t: TCRM;
begin
  Result.TeveErro := False;
  t := TCRM.Create(pConexao);

  t.SetInt('CENTRAL_ID', pCRMId, True);
  t.SetInt('CADASTRO_ID', pClienteId, True);

  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
