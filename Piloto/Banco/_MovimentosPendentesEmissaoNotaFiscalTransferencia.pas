unit _MovimentosPendentesEmissaoNotaFiscalTransferencia;

interface

uses
  _Conexao, _OperacoesBancoDados, _BibliotecaGenerica, _RecordsEspeciais, System.SysUtils;

type
  RecMovimentosPendentes = record
    MovimentoId: Integer;
    TipoMovimento: string;
    TipoMovimentoAnalitico: string;
    DataHoraEmissao: TDateTime;
    ClienteId: Integer;
    NomeCliente: string;
    EmpresaDestinoId: Integer;
    NomeEmpresaDestino: string;
  end;

function buscarMovimentos(
  pConexao: TConexao;
  pEmpresaOrigemId: Integer;
  pDataEmissaoInicial: TDateTime;
  pDataEmissaoFinal: TDateTime
): TArray<RecMovimentosPendentes>;

function gerarNotaTransferencia(
  pConexao: TConexao;
  pEmpresaOrigemId: Integer;
  pEmpresaDestinoId: Integer;
  pDataInicial: TDateTime;
  pDataFinal: TDateTime
): RecRetornoBD;

implementation

function buscarMovimentos(
  pConexao: TConexao;
  pEmpresaOrigemId: Integer;
  pDataEmissaoInicial: TDateTime;
  pDataEmissaoFinal: TDateTime
): TArray<RecMovimentosPendentes>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;

  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  MOVIMENTO_ID, ');
  vSql.Add('  TIPO_MOVIMENTO, ');
  vSql.Add('  DATA_HORA_EMISSAO, ');
  vSql.Add('  CLIENTE_ID, ');
  vSql.Add('  NOME_CLIENTE, ');
  vSql.Add('  EMPRESA_ORIGEM_ID, ');
  vSql.Add('  NOME_EMPRESA_ORIGEM, ');
  vSql.Add('  EMPRESA_DESTINO_ID, ');
  vSql.Add('  NOME_EMPRESA_DESTINO ');
  vSql.Add('from ');
  vSql.Add('  VW_MOV_PEND_EMISSAO_NF_TRANSF ');
  vSql.Add('where EMPRESA_ORIGEM_ID = :P1 ');
  vSql.Add('and trunc(DATA_HORA_EMISSAO) between :P2 and :P3 ');

  if vSql.Pesquisar([pEmpresaOrigemid, pDataEmissaoInicial, pDataEmissaoFinal]) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros -1 do begin
      Result[i].MovimentoId        := vSql.GetInt('MOVIMENTO_ID');
      Result[i].TipoMovimento      := vSql.GetString('TIPO_MOVIMENTO');
      Result[i].DataHoraEmissao    := vSql.GetData('DATA_HORA_EMISSAO');
      Result[i].ClienteId          := vSql.GetInt('CLIENTE_ID');
      Result[i].NomeCliente        := vSql.GetString('NOME_CLIENTE');
      Result[i].EmpresaDestinoId   := vSql.GetInt('EMPRESA_DESTINO_ID');
      Result[i].NomeEmpresaDestino := vSql.GetString('NOME_EMPRESA_DESTINO');

      Result[i].TipoMovimentoAnalitico :=
        Decode(
          Result[i].TipoMovimento,[
            'VRA', 'Venda retirar ato',
            'VRE', 'Venda retirar',
            'VEN', 'Venda entregar'
          ]
        );

      vSql.Next;
    end;
  end;
  vSql.Free;
end;

function gerarNotaTransferencia(
  pConexao: TConexao;
  pEmpresaOrigemId: Integer;
  pEmpresaDestinoId: Integer;
  pDataInicial: TDateTime;
  pDataFinal: TDateTime
): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('GERAR_NF_TRANSF_MOVIMENTOS');
  vProc := TProcedimentoBanco.Create(pConexao, 'GERAR_NOTA_TRANSF_MOVIMENTOS');

  try
    pConexao.IniciarTransacao;

    vProc.ParamByName('iEMPRESA_ORIGEM_ID').AsInteger  := pEmpresaOrigemId;
    vProc.ParamByName('iEMPRESA_DESTINO_ID').AsInteger := pEmpresaDestinoId;
    vProc.ParamByName('iDATA_INICIAL').AsDateTime      := pDataInicial;
    vProc.ParamByName('iDATA_FINAL').AsDateTime        := pDataFinal;

    vProc.Executar;

    Result.AsInt := vProc.ParamByName('oNOTA_FISCAL_ID').AsInteger;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  vProc.Free;
end;

end.
