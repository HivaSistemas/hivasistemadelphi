unit _CFOP;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros, System.Math, _CfopParametrosFiscaisEmpr;

{$M+}
type
  TTipoCFOP = (tpEntrada, tpSaida, tpTodos);

  TCFOP = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordCFOP: RecCFOPs;
  end;

function AtualizarCFOP(
  pConexao: TConexao;
  pCfopId: string;
  pFlutuacao: Integer;
  pCfopCorrespondenteId: string;
  pNome: string;
  pDescricao: string;
  pAtivo: string;
  pPlanoFinanceiroEntradaId: string;
  pEntradaMercadorias: string;
  pEntradaMercadoriasRevenda: string;
  pEntradaMercUsoConsumo: string;
  pEntradaMercadoriasBonific: string;
  pEntradaServicos: string;
  pEntradaConhecimentoFrete: string;
  pNaoExigirItensEntrada: string;
  pCfopEmpresas: TArray<RecCfopParametrosFiscaisEmpr>
): RecRetornoBD;

function BuscarCFOPs(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pTipo: TTipoCFOP;
  pSomenteAtivos: Boolean;
  pInicioCFOP: string;
  pSomenteEntradaMercadorias: Boolean;
  pSomenteEntradaServicos: Boolean;
  pSomenteEntradaConhecimentoFrete: Boolean
): TArray<RecCFOPs>;

function BuscarCFOPsComando(pConexao: TConexao; pComando: string): TArray<RecCFOPs>;

function ExcluirCFOP(
  pConexao: TConexao;
  pCfopId: Integer
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TCFOP }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'C�digo do CFOP',
      False,
      0,
      'where CFOP_PESQUISA_ID = :P1 ',
      'order by ' +
      '  CFOP_ID'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome CFOP(Avan�ada)',
      True,
      1,
      'where NOME like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  CFOP_ID ',
      '',
      True
    );
end;

constructor TCFOP.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CFOP');

  FSql :=
    'select ' +
    '  CFOP_ID, ' +
    '  FLUTUACAO, ' +
    '  CFOP_PESQUISA_ID, ' +
    '  CFOP_CORRESPONDENTE_ENTRADA_ID, ' +
    '  NOME, ' +
    '  DESCRICAO, ' +
    '  ATIVO, ' +
    '  PLANO_FINANCEIRO_ENTRADA_ID, ' +
    '  ENTRADA_MERCADORIAS, ' +
    '  ENTRADA_MERCADORIAS_REVENDA, ' +
    '  ENTRADA_MERC_USO_CONSUMO, ' +
    '  ENTRADA_MERCADORIAS_BONIFIC, ' +
    '  ENTRADA_SERVICOS, ' +
    '  ENTRADA_CONHECIMENTO_FRETE, ' +
    '  NAO_EXIGIR_ITENS_ENTRADA ' +
    'from ' +
    '  CFOP ';

  SetFiltros(GetFiltros);

  AddColuna('CFOP_ID', True);
  AddColuna('FLUTUACAO', True);
  AddColunaSL('CFOP_PESQUISA_ID');
  AddColuna('CFOP_CORRESPONDENTE_ENTRADA_ID');
  AddColuna('NOME');
  AddColuna('DESCRICAO');
  AddColuna('ATIVO');
  AddColuna('PLANO_FINANCEIRO_ENTRADA_ID');
  AddColuna('ENTRADA_MERCADORIAS');
  AddColuna('ENTRADA_MERCADORIAS_REVENDA');
  AddColuna('ENTRADA_MERC_USO_CONSUMO');
  AddColuna('ENTRADA_MERCADORIAS_BONIFIC');
  AddColuna('ENTRADA_SERVICOS');
  AddColuna('ENTRADA_CONHECIMENTO_FRETE');
  AddColuna('NAO_EXIGIR_ITENS_ENTRADA');
end;

function TCFOP.GetRecordCFOP: RecCFOPs;
begin
  Result := RecCFOPs.Create;
  Result.CfopId                    := getString('CFOP_ID', True);
  Result.Flutuacao                 := getInt('FLUTUACAO', True);
  Result.CfopPesquisaId            := getString('CFOP_PESQUISA_ID');
  Result.CfopCorrespondenteId      := getString('CFOP_CORRESPONDENTE_ENTRADA_ID');
  Result.nome                      := GetString('NOME');
  Result.descricao                 := GetString('DESCRICAO');
  Result.ativo                     := GetString('ATIVO');
  Result.PlanoFinanceiroEntradaId  := GetString('PLANO_FINANCEIRO_ENTRADA_ID');
  Result.EntradaMercadorias        := GetString('ENTRADA_MERCADORIAS');
  Result.EntradaMercadoriasRevenda := GetString('ENTRADA_MERCADORIAS_REVENDA');
  Result.EntradaMercUsoConsumo     := GetString('ENTRADA_MERC_USO_CONSUMO');
  Result.EntradaMercadoriasBonific := GetString('ENTRADA_MERCADORIAS_BONIFIC');
  Result.EntradaServicos           := GetString('ENTRADA_SERVICOS');
  Result.EntradaConhecimentoFrete  := GetString('ENTRADA_CONHECIMENTO_FRETE');
  Result.NaoExigirItensEntrada     := GetString('NAO_EXIGIR_ITENS_ENTRADA');
end;

function AtualizarCFOP(
  pConexao: TConexao;
  pCfopId: string;
  pFlutuacao: Integer;
  pCfopCorrespondenteId: string;
  pNome: string;
  pDescricao: string;
  pAtivo: string;
  pPlanoFinanceiroEntradaId: string;
  pEntradaMercadorias: string;
  pEntradaMercadoriasRevenda: string;
  pEntradaMercUsoConsumo: string;
  pEntradaMercadoriasBonific: string;
  pEntradaServicos: string;
  pEntradaConhecimentoFrete: string;
  pNaoExigirItensEntrada: string;
  pCfopEmpresas: TArray<RecCfopParametrosFiscaisEmpr>
): RecRetornoBD;
var
  vCfop: TCFOP;
  vNovo: Boolean;
  vSql: TConsulta;

  i: Integer;
  vExec: TExecucao;
  vCfopPesquisaId: string;
  vCfopEmp: TCfopParametrosFiscaisEmpr;
begin
  Result.Iniciar;
  vCfop := TCFOP.Create(pConexao);
  vExec := TExecucao.Create(pConexao);
  vCfopEmp := TCfopParametrosFiscaisEmpr.Create(pConexao);

  vCfopPesquisaId := pCfopId + '-' + IntToStr(pFlutuacao);

  vSql := TConsulta.Create(pConexao);
  vSql.SQL.Add('select count(CFOP_PESQUISA_ID) from CFOP where CFOP_PESQUISA_ID = :P1');
  vSql.Pesquisar([vCfopPesquisaId]);

  vNovo := vSql.GetInt(0) = 0;

  vSql.Active := False;
  vSql.Free;

  vCfop.SetString('CFOP_ID', pCfopId, True);
  vCfop.setInt('FLUTUACAO', pFlutuacao, True);
  vCfop.SetStringN('CFOP_CORRESPONDENTE_ENTRADA_ID', pCfopCorrespondenteId);
  vCfop.SetString('NOME', pNome);
  vCfop.SetString('DESCRICAO', pDescricao);
  vCfop.SetString('ATIVO', pAtivo);
  vCfop.setStringN('PLANO_FINANCEIRO_ENTRADA_ID', pPlanoFinanceiroEntradaId);
  vCfop.setString('ENTRADA_MERCADORIAS', pEntradaMercadorias);
  vCfop.setString('ENTRADA_MERCADORIAS_REVENDA', pEntradaMercadoriasRevenda);
  vCfop.setString('ENTRADA_MERC_USO_CONSUMO', pEntradaMercUsoConsumo);
  vCfop.setString('ENTRADA_MERCADORIAS_BONIFIC', pEntradaMercadoriasBonific);
  vCfop.setString('ENTRADA_SERVICOS', pEntradaServicos);
  vCfop.setString('ENTRADA_CONHECIMENTO_FRETE', pEntradaConhecimentoFrete);
  vCfop.setString('NAO_EXIGIR_ITENS_ENTRADA', pNaoExigirItensEntrada);

  try
    pConexao.IniciarTransacao;

    if vNovo then
      vCfop.Inserir
    else
      vCfop.Atualizar;

    vExec.Limpar;
    vExec.Add('delete from CFOP_PARAMETROS_FISCAIS_EMPR ');
    vExec.Add('where CFOP_ID = :P1 ');
    vExec.Executar([vCfopPesquisaId]);

    for i := Low(pCfopEmpresas) to High(pCfopEmpresas) do begin
      vCfopEmp.setInt('EMPRESA_ID', pCfopEmpresas[i].EmpresaId, True);
      vCfopEmp.setString('CFOP_ID', vCfopPesquisaId, True);
      vCfopEmp.setString('CALCULAR_PIS_COFINS', pCfopEmpresas[i].CalcularPisCofins);

      vCfopEmp.Inserir;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vCfopEmp.Free;
  vExec.Free;
  vCfop.Free;
end;

function BuscarCFOPs(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pTipo: TTipoCFOP;
  pSomenteAtivos: Boolean;
  pInicioCFOP: string;
  pSomenteEntradaMercadorias: Boolean;
  pSomenteEntradaServicos: Boolean;
  pSomenteEntradaConhecimentoFrete: Boolean
): TArray<RecCFOPs>;
var
  i: Integer;
  t: TCFOP;

  vSql: string;
  vSqlFiltrosEntradas: string;
begin
  Result := nil;
  t := TCFOP.Create(pConexao);

  vSql := '';
  case pTipo of
    tpEntrada: vSql := vSql + 'and substr(CFOP_ID, 1, 1) in(''1'', ''2'', ''3'') ';
    tpSaida: vSql := vSql + 'and  substr(CFOP_ID, 1, 1) in(''5'', ''6'', ''7'') ';
  end;

  if pSomenteAtivos then
    vSql := vSql + ' and ATIVO = ''S'' ';

  if pInicioCFOP <> '' then
    vSql := vSql + ' and substr(CFOP_ID, 1, 1) = ''' + pInicioCFOP + ''' ';

  vSqlFiltrosEntradas := '';
  if pSomenteEntradaMercadorias then
    _Biblioteca.AddOrSeNecessario(vSqlFiltrosEntradas, 'ENTRADA_MERCADORIAS = ''S'' ');

  if pSomenteEntradaServicos then
    _Biblioteca.AddOrSeNecessario(vSqlFiltrosEntradas, 'ENTRADA_SERVICOS = ''S'' ');

  if pSomenteEntradaConhecimentoFrete then
    _Biblioteca.AddOrSeNecessario(vSqlFiltrosEntradas, 'ENTRADA_CONHECIMENTO_FRETE = ''S'' ');

  if vSqlFiltrosEntradas <> '' then
    vSql := vSql + ' and(' + vSqlFiltrosEntradas + ') ';

  if t.Pesquisar(pIndice, pFiltros, vSql) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordCFOP;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarCFOPsComando(pConexao: TConexao; pComando: string): TArray<RecCFOPs>;
var
  i: Integer;
  t: TCFOP;
begin
  Result := nil;
  t := TCFOP.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordCFOP;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirCFOP(
  pConexao: TConexao;
  pCfopId: Integer
): RecRetornoBD;
var
  t: TCFOP;
begin
  Result.TeveErro := False;
  t := TCFOP.Create(pConexao);

  t.SetInt('CFOP_ID', pCfopId, True);

  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
