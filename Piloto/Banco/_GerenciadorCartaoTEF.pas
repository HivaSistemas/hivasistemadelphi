unit _GerenciadorCartaoTEF;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TGerenciadorCartaoTEF = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordGerenciadorCartaoTEF: RecGerenciadorCartaoTEF;
  end;

function AtualizarGerenciadorCartaoTEF(
  pConexao: TConexao;
  pGerenciadorCartaoId: Integer;
  pNome: string;
  pDiretorioArquivosRequisicoes: string;
  pDiretorioArquivosRespostas: string;
  pTextoProcurarBandeira1: string;
  pPosicaoInicialProcBand1: Integer;
  pQtdeCaracteresCopiarBand1: Integer;
  pTextoProcurarBandeira2: string;
  pPosicaoInicialProcBand2: Integer;
  pQtdeCaracteresCopiarBand2: Integer;
  pTextoProcurarRede1: string;
  pPosicaoInicialProcRede1: Integer;
  pQtdeCaracteresCopiarRede1: Integer;
  pTextoProcurarRede2: string;
  pPosicaoInicialProcRede2: Integer;
  pQtdeCaracteresCopiarRede2: Integer;
  pTextoProcurarNumeroCartao1: string;
  pPosicIniProcNumeroCartao1: Integer;
  pQtdeCaracCopiarNrCartao1: Integer;
  pTextoProcurarNumeroCartao2: string;
  pPosicIniProcNumeroCartao2: Integer;
  pQtdeCaracCopiarNrCartao2: Integer;
  pTextoProcurarNsu1: string;
  pPosicaoIniProcurarNsu1: Integer;
  pQtdeCaracCopiarNsu1: Integer;
  pTextoProcurarNsu2: string;
  pPosicaoIniProcurarNsu2: Integer;
  pQtdeCaracCopiarNsu2: Integer;
  pTextoProcurarCodAutoriz1: string;
  pPosicaoIniProCodAutoriz1: Integer;
  pQtdeCaracCopCodAutoriz1: Integer;
  pTextoProcurarCodAutoriz2: string;
  pPosicaoIniProCodAutoriz2: Integer;
  pQtdeCaracCopCodAutoriz2: Integer;
  pAtivo: string
): RecRetornoBD;

function BuscarGerenciadorCartaoTEF(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecGerenciadorCartaoTEF>;

function ExcluirGerenciadorCartaoTEF(
  pConexao: TConexao;
  pGerenciadorId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TGerenciadorCartaoTEF }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where GERENCIADOR_CARTAO_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome do gerenciador',
      True,
      0,
      'where NOME like :P1 || ''%'' '
    );
end;

constructor TGerenciadorCartaoTEF.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'GERENCIADORES_CARTOES_TEF');

  FSql :=
    'select ' +
    '  GERENCIADOR_CARTAO_ID, ' +
    '  NOME, ' +
    '  DIRETORIO_ARQUIVOS_REQUISICOES, ' +
    '  DIRETORIO_ARQUIVOS_RESPOSTAS, ' +
    '  TEXTO_PROCURAR_BANDEIRA_1, ' +
    '  POSICAO_INICIAL_PROC_BAND_1, ' +
    '  QTDE_CARACTERES_COPIAR_BAND_1, ' +
    '  TEXTO_PROCURAR_BANDEIRA_2, ' +
    '  POSICAO_INICIAL_PROC_BAND_2, ' +
    '  QTDE_CARACTERES_COPIAR_BAND_2, ' +
    '  TEXTO_PROCURAR_REDE_1, ' +
    '  POSICAO_INICIAL_PROC_REDE_1, ' +
    '  QTDE_CARACTERES_COPIAR_REDE_1, ' +
    '  TEXTO_PROCURAR_REDE_2, ' +
    '  POSICAO_INICIAL_PROC_REDE_2, ' +
    '  QTDE_CARACTERES_COPIAR_REDE_2, ' +
    '  TEXTO_PROCURAR_NUMERO_CARTAO_1, ' +
    '  POSIC_INI_PROC_NUMERO_CARTAO_1, ' +
    '  QTDE_CARAC_COPIAR_NR_CARTAO_1, ' +
    '  TEXTO_PROCURAR_NUMERO_CARTAO_2, ' +
    '  POSIC_INI_PROC_NUMERO_CARTAO_2, ' +
    '  QTDE_CARAC_COPIAR_NR_CARTAO_2, ' +
    '  TEXTO_PROCURAR_NSU_1, ' +
    '  POSICAO_INI_PROCURAR_NSU_1, ' +
    '  QTDE_CARAC_COPIAR_NSU_1, ' +
    '  TEXTO_PROCURAR_NSU_2, ' +
    '  POSICAO_INI_PROCURAR_NSU_2, ' +
    '  QTDE_CARAC_COPIAR_NSU_2, ' +
    '  TEXTO_PROCURAR_COD_AUTORIZ_1, ' +
    '  POSICAO_INI_PRO_COD_AUTORIZ_1, ' +
    '  QTDE_CARAC_COP_COD_AUTORIZ_1, ' +
    '  TEXTO_PROCURAR_COD_AUTORIZ_2, ' +
    '  POSICAO_INI_PRO_COD_AUTORIZ_2, ' +
    '  QTDE_CARAC_COP_COD_AUTORIZ_2, ' +
    '  ATIVO ' +
    'from ' +
    '  GERENCIADORES_CARTOES_TEF ';

  setFiltros(getFiltros);

  AddColuna('GERENCIADOR_CARTAO_ID', True);
  AddColuna('NOME');
  AddColuna('DIRETORIO_ARQUIVOS_REQUISICOES');
  AddColuna('DIRETORIO_ARQUIVOS_RESPOSTAS');
  AddColuna('TEXTO_PROCURAR_BANDEIRA_1');
  AddColuna('POSICAO_INICIAL_PROC_BAND_1');
  AddColuna('QTDE_CARACTERES_COPIAR_BAND_1');
  AddColuna('TEXTO_PROCURAR_BANDEIRA_2');
  AddColuna('POSICAO_INICIAL_PROC_BAND_2');
  AddColuna('QTDE_CARACTERES_COPIAR_BAND_2');
  AddColuna('TEXTO_PROCURAR_REDE_1');
  AddColuna('POSICAO_INICIAL_PROC_REDE_1');
  AddColuna('QTDE_CARACTERES_COPIAR_REDE_1');
  AddColuna('TEXTO_PROCURAR_REDE_2');
  AddColuna('POSICAO_INICIAL_PROC_REDE_2');
  AddColuna('QTDE_CARACTERES_COPIAR_REDE_2');
  AddColuna('TEXTO_PROCURAR_NUMERO_CARTAO_1');
  AddColuna('POSIC_INI_PROC_NUMERO_CARTAO_1');
  AddColuna('QTDE_CARAC_COPIAR_NR_CARTAO_1');
  AddColuna('TEXTO_PROCURAR_NUMERO_CARTAO_2');
  AddColuna('POSIC_INI_PROC_NUMERO_CARTAO_2');
  AddColuna('QTDE_CARAC_COPIAR_NR_CARTAO_2');
  AddColuna('TEXTO_PROCURAR_NSU_1');
  AddColuna('POSICAO_INI_PROCURAR_NSU_1');
  AddColuna('QTDE_CARAC_COPIAR_NSU_1');
  AddColuna('TEXTO_PROCURAR_NSU_2');
  AddColuna('POSICAO_INI_PROCURAR_NSU_2');
  AddColuna('QTDE_CARAC_COPIAR_NSU_2');
  AddColuna('TEXTO_PROCURAR_COD_AUTORIZ_1');
  AddColuna('POSICAO_INI_PRO_COD_AUTORIZ_1');
  AddColuna('QTDE_CARAC_COP_COD_AUTORIZ_1');
  AddColuna('TEXTO_PROCURAR_COD_AUTORIZ_2');
  AddColuna('POSICAO_INI_PRO_COD_AUTORIZ_2');
  AddColuna('QTDE_CARAC_COP_COD_AUTORIZ_2');
  AddColuna('ATIVO');
end;

function TGerenciadorCartaoTEF.getRecordGerenciadorCartaoTEF: RecGerenciadorCartaoTEF;
begin
  Result := RecGerenciadorCartaoTEF.Create;
  Result.gerenciador_cartao_id          := getInt('GERENCIADOR_CARTAO_ID', True);
  Result.nome                           := getString('NOME');
  Result.diretorio_arquivos_requisicoes := getString('DIRETORIO_ARQUIVOS_REQUISICOES');
  Result.diretorio_arquivos_respostas   := getString('DIRETORIO_ARQUIVOS_RESPOSTAS');
  Result.TextoProcurarBandeira1         := getString('TEXTO_PROCURAR_BANDEIRA_1');
  Result.PosicaoInicialProcBand1        := getInt('POSICAO_INICIAL_PROC_BAND_1');
  Result.QtdeCaracteresCopiarBand1      := getInt('QTDE_CARACTERES_COPIAR_BAND_1');
  Result.TextoProcurarBandeira2         := getString('TEXTO_PROCURAR_BANDEIRA_2');
  Result.PosicaoInicialProcBand2        := getInt('POSICAO_INICIAL_PROC_BAND_2');
  Result.QtdeCaracteresCopiarBand2      := getInt('QTDE_CARACTERES_COPIAR_BAND_2');
  Result.TextoProcurarRede1             := getString('TEXTO_PROCURAR_REDE_1');
  Result.PosicaoInicialProcRede1        := getInt('POSICAO_INICIAL_PROC_REDE_1');
  Result.QtdeCaracteresCopiarRede1      := getInt('QTDE_CARACTERES_COPIAR_REDE_1');
  Result.TextoProcurarRede2             := getString('TEXTO_PROCURAR_REDE_2');
  Result.PosicaoInicialProcRede2        := getInt('POSICAO_INICIAL_PROC_REDE_2');
  Result.QtdeCaracteresCopiarRede2      := getInt('QTDE_CARACTERES_COPIAR_REDE_2');
  Result.TextoProcurarNumeroCartao1     := getString('TEXTO_PROCURAR_NUMERO_CARTAO_1');
  Result.PosicIniProcNumeroCartao1      := getInt('POSIC_INI_PROC_NUMERO_CARTAO_1');
  Result.QtdeCaracCopiarNrCartao1       := getInt('QTDE_CARAC_COPIAR_NR_CARTAO_1');
  Result.TextoProcurarNumeroCartao2     := getString('TEXTO_PROCURAR_NUMERO_CARTAO_2');
  Result.PosicIniProcNumeroCartao2      := getInt('POSIC_INI_PROC_NUMERO_CARTAO_2');
  Result.QtdeCaracCopiarNrCartao2       := getInt('QTDE_CARAC_COPIAR_NR_CARTAO_2');
  Result.TextoProcurarNsu1              := getString('TEXTO_PROCURAR_NSU_1');
  Result.PosicaoIniProcurarNsu1         := getInt('POSICAO_INI_PROCURAR_NSU_1');
  Result.QtdeCaracCopiarNsu1            := getInt('QTDE_CARAC_COPIAR_NSU_1');
  Result.TextoProcurarNsu2              := getString('TEXTO_PROCURAR_NSU_2');
  Result.PosicaoIniProcurarNsu2         := getInt('POSICAO_INI_PROCURAR_NSU_2');
  Result.QtdeCaracCopiarNsu2            := getInt('QTDE_CARAC_COPIAR_NSU_2');
  Result.TextoProcurarCodAutoriz1       := getString('TEXTO_PROCURAR_COD_AUTORIZ_1');
  Result.PosicaoIniProCodAutoriz1       := getInt('POSICAO_INI_PRO_COD_AUTORIZ_1');
  Result.QtdeCaracCopCodAutoriz1        := getInt('QTDE_CARAC_COP_COD_AUTORIZ_1');
  Result.TextoProcurarCodAutoriz2       := getString('TEXTO_PROCURAR_COD_AUTORIZ_2');
  Result.PosicaoIniProCodAutoriz2       := getInt('POSICAO_INI_PRO_COD_AUTORIZ_2');
  Result.QtdeCaracCopCodAutoriz2        := getInt('QTDE_CARAC_COP_COD_AUTORIZ_2');
  Result.ativo                          := getString('ATIVO');
end;

function AtualizarGerenciadorCartaoTEF(
  pConexao: TConexao;
  pGerenciadorCartaoId: Integer;
  pNome: string;
  pDiretorioArquivosRequisicoes: string;
  pDiretorioArquivosRespostas: string;
  pTextoProcurarBandeira1: string;
  pPosicaoInicialProcBand1: Integer;
  pQtdeCaracteresCopiarBand1: Integer;
  pTextoProcurarBandeira2: string;
  pPosicaoInicialProcBand2: Integer;
  pQtdeCaracteresCopiarBand2: Integer;
  pTextoProcurarRede1: string;
  pPosicaoInicialProcRede1: Integer;
  pQtdeCaracteresCopiarRede1: Integer;
  pTextoProcurarRede2: string;
  pPosicaoInicialProcRede2: Integer;
  pQtdeCaracteresCopiarRede2: Integer;
  pTextoProcurarNumeroCartao1: string;
  pPosicIniProcNumeroCartao1: Integer;
  pQtdeCaracCopiarNrCartao1: Integer;
  pTextoProcurarNumeroCartao2: string;
  pPosicIniProcNumeroCartao2: Integer;
  pQtdeCaracCopiarNrCartao2: Integer;
  pTextoProcurarNsu1: string;
  pPosicaoIniProcurarNsu1: Integer;
  pQtdeCaracCopiarNsu1: Integer;
  pTextoProcurarNsu2: string;
  pPosicaoIniProcurarNsu2: Integer;
  pQtdeCaracCopiarNsu2: Integer;
  pTextoProcurarCodAutoriz1: string;
  pPosicaoIniProCodAutoriz1: Integer;
  pQtdeCaracCopCodAutoriz1: Integer;
  pTextoProcurarCodAutoriz2: string;
  pPosicaoIniProCodAutoriz2: Integer;
  pQtdeCaracCopCodAutoriz2: Integer;
  pAtivo: string
): RecRetornoBD;
var
  t: TGerenciadorCartaoTEF;
  vNovo: Boolean;
  vSeq: TSequencia;
begin
  Result.Iniciar;
  t := TGerenciadorCartaoTEF.Create(pConexao);

  vNovo := pGerenciadorCartaoId = 0;
  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_GERENCIADOR_CARTAO_ID');
    pGerenciadorCartaoId := vSeq.getProximaSequencia;
    result.AsInt := pGerenciadorCartaoId;
    vSeq.Free;
  end;

  t.setInt('GERENCIADOR_CARTAO_ID', pGerenciadorCartaoId, True);
  t.setString('NOME', pNome);
  t.setString('DIRETORIO_ARQUIVOS_REQUISICOES', pDiretorioArquivosRequisicoes);
  t.setString('DIRETORIO_ARQUIVOS_RESPOSTAS', pDiretorioArquivosRespostas);
  t.setString('TEXTO_PROCURAR_BANDEIRA_1', pTextoProcurarBandeira1);
  t.setInt('POSICAO_INICIAL_PROC_BAND_1', pPosicaoInicialProcBand1);
  t.setInt('QTDE_CARACTERES_COPIAR_BAND_1', pQtdeCaracteresCopiarBand1);
  t.setString('TEXTO_PROCURAR_BANDEIRA_2', pTextoProcurarBandeira2);
  t.setInt('POSICAO_INICIAL_PROC_BAND_2', pPosicaoInicialProcBand2);
  t.setInt('QTDE_CARACTERES_COPIAR_BAND_2', pQtdeCaracteresCopiarBand2);
  t.setString('TEXTO_PROCURAR_REDE_1', pTextoProcurarRede1);
  t.setInt('POSICAO_INICIAL_PROC_REDE_1', pPosicaoInicialProcRede1);
  t.setInt('QTDE_CARACTERES_COPIAR_REDE_1', pQtdeCaracteresCopiarRede1);
  t.setString('TEXTO_PROCURAR_REDE_2', pTextoProcurarRede2);
  t.setInt('POSICAO_INICIAL_PROC_REDE_2', pPosicaoInicialProcRede2);
  t.setInt('QTDE_CARACTERES_COPIAR_REDE_2', pQtdeCaracteresCopiarRede2);
  t.setString('TEXTO_PROCURAR_NUMERO_CARTAO_1', pTextoProcurarNumeroCartao1);
  t.setInt('POSIC_INI_PROC_NUMERO_CARTAO_1', pPosicIniProcNumeroCartao1);
  t.setInt('QTDE_CARAC_COPIAR_NR_CARTAO_1', pQtdeCaracCopiarNrCartao1);
  t.setString('TEXTO_PROCURAR_NUMERO_CARTAO_2', pTextoProcurarNumeroCartao2);
  t.setInt('POSIC_INI_PROC_NUMERO_CARTAO_2', pPosicIniProcNumeroCartao2);
  t.setInt('QTDE_CARAC_COPIAR_NR_CARTAO_2', pQtdeCaracCopiarNrCartao2);
  t.setString('TEXTO_PROCURAR_NSU_1', pTextoProcurarNsu1);
  t.setInt('POSICAO_INI_PROCURAR_NSU_1', pPosicaoIniProcurarNsu1);
  t.setInt('QTDE_CARAC_COPIAR_NSU_1', pQtdeCaracCopiarNsu1);
  t.setString('TEXTO_PROCURAR_NSU_2', pTextoProcurarNsu2);
  t.setInt('POSICAO_INI_PROCURAR_NSU_2', pPosicaoIniProcurarNsu2);
  t.setInt('QTDE_CARAC_COPIAR_NSU_2', pQtdeCaracCopiarNsu2);
  t.setString('TEXTO_PROCURAR_COD_AUTORIZ_1', pTextoProcurarCodAutoriz1);
  t.setInt('POSICAO_INI_PRO_COD_AUTORIZ_1', pPosicaoIniProCodAutoriz1);
  t.setInt('QTDE_CARAC_COP_COD_AUTORIZ_1', pQtdeCaracCopCodAutoriz1);
  t.setString('TEXTO_PROCURAR_COD_AUTORIZ_2', pTextoProcurarCodAutoriz2);
  t.setInt('POSICAO_INI_PRO_COD_AUTORIZ_2', pPosicaoIniProCodAutoriz2);
  t.setInt('QTDE_CARAC_COP_COD_AUTORIZ_2', pQtdeCaracCopCodAutoriz2);
  t.setString('ATIVO', pativo);

  try
    if vNovo then
      t.Inserir
    else
      t.Atualizar;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function BuscarGerenciadorCartaoTEF(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecGerenciadorCartaoTEF>;
var
  i: Integer;
  t: TGerenciadorCartaoTEF;
begin
  Result := nil;
  t := TGerenciadorCartaoTEF.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordGerenciadorCartaoTEF;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirGerenciadorCartaoTEF(
  pConexao: TConexao;
  pGerenciadorId: Integer
): RecRetornoBD;
var
  t: TGerenciadorCartaoTEF;
begin
  Result.TeveErro := False;
  t := TGerenciadorCartaoTEF.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('GERENCIADOR_CARTAO_ID', pGerenciadorId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
