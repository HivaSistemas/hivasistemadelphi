unit _AcumuladosCreditos;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecAcumuladosCreditos = record
    AcumuladoId: Integer;
    PagarId: Integer;
  end;

  TAcumuladosCreditos = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordAcumuladosCreditos: RecAcumuladosCreditos;
  end;

function BuscarAcumuladosCreditos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecAcumuladosCreditos>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TAcumuladosCreditos }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ACUMULADO_ID = :P1'
    );
end;

constructor TAcumuladosCreditos.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ACUMULADOS_CREDITOS');

  FSql := 
    'select ' +
    '  ACUMULADO_ID, ' +
    '  PAGAR_ID ' +
    'from ' +
    '  ACUMULADOS_CREDITOS';

  setFiltros(getFiltros);

  AddColuna('ACUMULADO_ID', True);
  AddColuna('PAGAR_ID', True);
end;

function TAcumuladosCreditos.getRecordAcumuladosCreditos: RecAcumuladosCreditos;
begin
  Result.AcumuladoId  := getInt('ACUMULADO_ID', True);
  Result.PagarId      := getInt('PAGAR_ID', True);
end;

function BuscarAcumuladosCreditos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecAcumuladosCreditos>;
var
  i: Integer;
  t: TAcumuladosCreditos;
begin
  Result := nil;
  t := TAcumuladosCreditos.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordAcumuladosCreditos;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
