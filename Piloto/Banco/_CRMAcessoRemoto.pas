unit _CRMAcessoRemoto;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TCRMAcessoRemoto = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordCRMAcessoRemoto: RecCRMAcessoRemoto;
  end;

function BuscarCRMAcessoRemoto(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCRMAcessoRemoto>;

function ExcluirCRMAcessoRemoto(
  pConexao: TConexao;
  pCRMAcessoRemotoId: Integer
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TCRMAcessoRemoto }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CENTRAL_ID = :P1',
      'order by ITEM_ID asc'
    );
end;

constructor TCRMAcessoRemoto.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CRM_ACESSO_REMOTO');

  FSql :=
    'select ' +
    '  CENTRAL_ID, ' +
    '  ITEM_ID, ' +
    '  IP, ' +
    '  SENHA, ' +
    '  OBSERVACAO, ' +
    '  USUARIO ' +
    'from ' +
    '  CRM_ACESSO_REMOTO ';

  SetFiltros(GetFiltros);

  AddColuna('CENTRAL_ID', True);
  AddColuna('ITEM_ID');
  AddColuna('IP');
  AddColuna('SENHA');
  AddColuna('OBSERVACAO');
  AddColuna('USUARIO');
end;

function TCRMAcessoRemoto.GetRecordCRMAcessoRemoto: RecCRMAcessoRemoto;
begin
  Result.central_id  := GetInt('CENTRAL_ID', True);
  Result.item_id     := GetInt('ITEM_ID');
  Result.ip          := getString('IP');
  Result.senha       := getString('SENHA');
  Result.observacao  := getString('OBSERVACAO');
  Result.usuario     := getString('USUARIO');
end;

function BuscarCRMAcessoRemoto(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCRMAcessoRemoto>;
var
  i: Integer;
  t: TCRMAcessoRemoto;
begin
  Result := nil;
  t := TCRMAcessoRemoto.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordCRMAcessoRemoto;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirCRMAcessoRemoto(
  pConexao: TConexao;
  pCRMAcessoRemotoId: Integer
): RecRetornoBD;
var
  t: TCRMAcessoRemoto;
begin
  Result.TeveErro := False;
  t := TCRMAcessoRemoto.Create(pConexao);

  t.SetInt('CENTRAL_ID', pCRMAcessoRemotoId, True);

  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
