unit _RelacaoEntregasPendentes;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsRelatorios, _BibliotecaGenerica;

type
  RecEntregasPendentes = record
    TipoMovimento: string;
    TipoMovimentoAnalitico: string;
    OrcamentoId: Integer;
    ClienteId: Integer;
    NomeCliente: string;
    EmpresaId: Integer;
    NomeEmpresa: string;
    LocalId: Integer;
    NomeLocal: string;
    VendedorId: Integer;
    NomeVendedor: string;
    DataHoraCadastro: TDateTime;
    PrevisaoEntrega: TDateTime;
  end;

  RecEntregasItensPendentes = record
    TipoMovimento: string;
    EmpresaId: Integer;
    ProdutoId: Integer;
    Nome: string;
    Quantidade: Double;
    Entregues: Double;
    Devolvidos: Double;
    Saldo: Double;
    Lote: string;
    PrevisaoEntrega: TDateTime;
    OrcamentoId: Integer;
    LocalId: Integer;
    MarcaId: Integer;
    NomeMarca: string;
    UnidadeVenda: string;
    PrecoUnitario: Double;
  end;

function BuscarEntregas(pConexao: TConexao; pComando: string): TArray<RecEntregasPendentes>;
function BuscarItensEntregas(pConexao: TConexao; pComando: string): TArray<RecEntregasItensPendentes>;

implementation

function BuscarEntregas(pConexao: TConexao; pComando: string): TArray<RecEntregasPendentes>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;

  vSql := TConsulta.Create( pConexao );
  vSql.Add('select ');
  vSql.Add('  TIPO_MOVIMENTO, ');
  vSql.Add('  ORCAMENTO_ID, ');
  vSql.Add('  CLIENTE_ID, ');
  vSql.Add('  NOME_CLIENTE, ');
  vSql.Add('  EMPRESA_ID, ');
  vSql.Add('  NOME_EMPRESA, ');
  vSql.Add('  LOCAL_ID, ');
  vSql.Add('  NOME_LOCAL, ');
  vSql.Add('  VENDEDOR_ID, ');
  vSql.Add('  NOME_VENDEDOR, ');
  vSql.Add('  DATA_HORA_CADASTRO, ');
  vSql.Add('  PREVISAO_ENTREGA ');
  vSql.Add('from ');
  vSql.Add('  VW_ENTREGAS_PENDENTES ');
  vSql.Add(pComando);

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros -1 do begin
      Result[i].TipoMovimento    := vSql.GetString('TIPO_MOVIMENTO');
      Result[i].OrcamentoId      := vSql.GetInt('ORCAMENTO_ID');
      Result[i].ClienteId        := vSql.GetInt('CLIENTE_ID');
      Result[i].NomeCliente      := vSql.GetString('NOME_CLIENTE');
      Result[i].EmpresaId        := vSql.GetInt('EMPRESA_ID');
      Result[i].NomeEmpresa      := vSql.GetString('NOME_EMPRESA');
      Result[i].LocalId          := vSql.GetInt('LOCAL_ID');
      Result[i].NomeLocal        := vSql.GetString('NOME_LOCAL');
      Result[i].VendedorId       := vSql.GetInt('VENDEDOR_ID');
      Result[i].NomeVendedor     := vSql.GetString('NOME_VENDEDOR');
      Result[i].DataHoraCadastro := vSql.getData('DATA_HORA_CADASTRO');
      Result[i].PrevisaoEntrega  := vSql.getData('PREVISAO_ENTREGA');

      Result[i].TipoMovimentoAnalitico :=
        _BibliotecaGenerica.Decode(Result[i].TipoMovimento, ['R', 'Retirar', 'E', 'Entregar', 'S', 'Sem previs�o']);

      vSql.Next;
    end;
  end;

  vSql.Free;
end;

function BuscarItensEntregas(pConexao: TConexao; pComando: string): TArray<RecEntregasItensPendentes>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;

  vSql := TConsulta.Create( pConexao );
  vSql.Add('select ');
  vSql.Add('  TIPO_MOVIMENTO, ');
  vSql.Add('  EMPRESA_ID, ');
  vSql.Add('  PRODUTO_ID, ');
  vSql.Add('  NOME, ');
  vSql.Add('  QUANTIDADE, ');
  vSql.Add('  ENTREGUES, ');
  vSql.Add('  DEVOLVIDOS, ');
  vSql.Add('  SALDO, ');
  vSql.Add('  LOTE, ');
  vSql.Add('  PREVISAO_ENTREGA, ');
  vSql.Add('  ORCAMENTO_ID, ');
  vSql.Add('  LOCAL_ID, ');
  vSql.Add('  MARCA_ID, ');
  vSql.Add('  NOME_MARCA, ');
  vSql.Add('  UNIDADE_VENDA, ');
  vSql.Add('  PRECO_UNITARIO ');
  vSql.Add('from ');
  vSql.Add('  VW_ITENS_ENTREGAS_PENDENTES ');
  vSql.Add(pComando);

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros -1 do begin
      Result[i].TipoMovimento   := vSql.GetString('TIPO_MOVIMENTO');
      Result[i].EmpresaId       := vSql.GetInt('EMPRESA_ID');
      Result[i].ProdutoId       := vSql.GetInt('PRODUTO_ID');
      Result[i].Nome            := vSql.GetString('NOME');
      Result[i].Quantidade      := vSql.GetDouble('QUANTIDADE');
      Result[i].Entregues       := vSql.GetDouble('ENTREGUES');
      Result[i].Devolvidos      := vSql.GetDouble('DEVOLVIDOS');
      Result[i].Saldo           := vSql.GetDouble('SALDO');
      Result[i].Lote            := vSql.GetString('LOTE');
      Result[i].PrevisaoEntrega := vSql.getData('PREVISAO_ENTREGA');
      Result[i].OrcamentoId     := vSql.GetInt('ORCAMENTO_ID');
      Result[i].LocalId         := vSql.GetInt('LOCAL_ID');
      Result[i].MarcaId         := vSql.GetInt('MARCA_ID');
      Result[i].NomeMarca       := vSql.GetString('NOME_MARCA');
      Result[i].UnidadeVenda    := vSql.GetString('UNIDADE_VENDA');
      Result[i].PrecoUnitario   := vSql.GetDouble('PRECO_UNITARIO');

      vSql.Next;
    end;
  end;

  vSql.Free;
end;

end.
