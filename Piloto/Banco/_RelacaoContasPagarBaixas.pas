unit _RelacaoContasPagarBaixas;

interface

uses
  System.SysUtils, _Conexao, _OperacoesBancoDados, _RecordsRelatorios, _Biblioteca;

function BuscarContasPagarBaixas(pConexao: TConexao; pComando: string): TArray<RecContasPagarBaixas>;

implementation

function BuscarContasPagarBaixas(pConexao: TConexao; pComando: string): TArray<RecContasPagarBaixas>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  with vSql.SQL do begin
    Add('select ');
    Add('  CPB.BAIXA_ID, ');
    Add('  CPB.USUARIO_BAIXA_ID, ');
    Add('  FBA.NOME as NOME_USUARIO_BAIXA, ');
    Add('  CPB.VALOR_DINHEIRO, ');
    Add('  CPB.VALOR_CHEQUE, ');
    Add('  CPB.VALOR_CARTAO, ');
    Add('  CPB.VALOR_COBRANCA, ');
    Add('  CPB.VALOR_CREDITO, ');
    Add('  CPB.DATA_HORA_BAIXA, ');
    Add('  CPB.DATA_PAGAMENTO, ');
    Add('  CPB.OBSERVACOES, ');
    Add('  CPB.EMPRESA_ID, ');
    Add('  EMP.NOME_FANTASIA as NOME_EMPRESA, ');
    Add('  CPB.TIPO ');
    Add('from ');
    Add('  CONTAS_PAGAR_BAIXAS CPB ');

    Add('inner join FUNCIONARIOS FBA ');
    Add('on CPB.USUARIO_BAIXA_ID = FBA.FUNCIONARIO_ID ');

    Add('inner join EMPRESAS EMP ');
    Add('on CPB.EMPRESA_ID = EMP.EMPRESA_ID ');

    Add(pComando);
  end;

  if vSql.Pesquisar() then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].baixa_id           := vSql.GetInt('BAIXA_ID');
      Result[i].usuario_baixa_id   := vSql.GetInt('USUARIO_BAIXA_ID');
      Result[i].nome_usuario_baixa := vSql.GetString('NOME_USUARIO_BAIXA');
      Result[i].valor_dinheiro     := vSql.GetDouble('VALOR_DINHEIRO');
      Result[i].valor_cheque       := vSql.GetDouble('VALOR_CHEQUE');
      Result[i].valor_cartao       := vSql.GetDouble('VALOR_CARTAO');
      Result[i].valor_cobranca     := vSql.GetDouble('VALOR_COBRANCA');
      Result[i].valor_credito      := vSql.GetDouble('VALOR_CREDITO');
      Result[i].data_baixa         := vSql.GetData('DATA_HORA_BAIXA');
      Result[i].data_pagamento     := vSql.GetData('DATA_PAGAMENTO');
      Result[i].observacoes        := vSql.GetString('OBSERVACOES');
      Result[i].empresa_id         := vSql.GetInt('EMPRESA_ID');
      Result[i].nome_empresa       := vSql.GetString('NOME_EMPRESA');
      Result[i].Tipo               := vSql.GetString('TIPO');

      Result[i].TipoAnalitico :=
        _Biblioteca.Decode(
          Result[i].Tipo,[
            'BCP', 'Baixa contas pagar',
            'CRE', 'Cr�dito a receber',
            'BXN', 'Baixa de t�tulos normais',
            'ECP', 'Encontro de contas a pagar',
            'FAC', 'Fechamento de acumulados',
            'FPE', 'Fechamento de pedido']
        );

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

end.
