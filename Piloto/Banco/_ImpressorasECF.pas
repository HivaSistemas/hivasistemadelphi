unit _ImpressorasECF;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TECF = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordECF: RecECF;
  end;

function AtualizarECF(
  pConexao: TConexao;
  pECFId: Word;
  pModeloECF: Word;
  pEmpresaId: Word;
  pNomeFormaPagamentoCobranca: string;
  pNomeFormaPagamentoCheque: string;
  pNomeFormaPagamentoCredito: string;
  pNomeFormaPagamentoPOS: string;
  pNomeFormaPagamentoCartao: string;
  pNomeFormapagamentodinheiro: string;
  pComputadorECF: string;
  pGerenciadorCartaoId: Integer;
  pCodigoFabrica: string;
  pNumeroECF: Integer;
  pIndiceRecebimentoNaoFiscal: Integer;
  pPortaSerial: Integer;
  pPercentualAliquota1: Double;
  pPercentualAliquota2: Double;
  pPercentualAliquota3: Double;
  pPercentualAliquota4: Double;
  pPercentualAliquota5: Double;
  pPercentualAliquota6: Double;
  pPercentualAliquota7: Double;
  pPercentualAliquota8: Double;
  pAtivo: string
): RecRespostaBanco;

function BuscarECFs(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecECF>;

function ExcluirECF(pConexao: TConexao; pECFId: Word): RecRespostaBanco;
function BuscarModelosECF(pConexao: TConexao): TArray<RecModelosECF>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TECF }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 4);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where IMP.ECF_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome do computador',
      True,
      0,
      'where IMP.COMPUTADOR_ECF like :P1 || ''%'' '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Modelo do ECF',
      False,
      1,
      'where upper(MOD.MODELO_ECF) like :P1 || ''%'' '
    );

  Result[3] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where IMP.EMPRESA_ID = :P1 ' +
      'and IMP.COMPUTADOR_ECF = :P2 ' +
      'and IMP.ATIVO = ''S'' '
    );
end;

constructor TECF.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'IMPRESSORAS_ECF');

  _sql := 
    'select ' +
    '  IMP.ECF_ID, ' +
    '  IMP.MODELO_ECF, ' +
    '  IMP.EMPRESA_ID, ' +
    '  IMP.NOME_FORMA_PAGAMENTO_COBRANCA, ' +
    '  IMP.NOME_FORMA_PAGAMENTO_CHEQUE, ' +
    '  IMP.NOME_FORMA_PAGAMENTO_CREDITO, ' +
    '  IMP.NOME_FORMA_PAGAMENTO_POS, ' +
    '  IMP.ATIVO, ' +
    '  IMP.NOME_FORMA_PAGAMENTO_DINHEIRO, ' +
    '  IMP.COMPUTADOR_ECF, ' +
    '  IMP.NOME_FORMA_PAGAMENTO_CARTAO, ' +
    '  IMP.GERENCIADOR_CARTAO_ID, ' +
    '  IMP.CODIGO_FABRICA, ' +
    '  IMP.NUMERO_ECF, ' +
    '  IMP.INDICE_RECEBIMENTO_NAO_FISCAL, ' +
    '  IMP.PORTA_SERIAL, ' +
    '  IMP.PERCENTUAL_ALIQUOTA_1, ' +
    '  IMP.PERCENTUAL_ALIQUOTA_2, ' +
    '  IMP.PERCENTUAL_ALIQUOTA_3, ' +
    '  IMP.PERCENTUAL_ALIQUOTA_4, ' +
    '  IMP.PERCENTUAL_ALIQUOTA_5, ' +
    '  IMP.PERCENTUAL_ALIQUOTA_6, ' +
    '  IMP.PERCENTUAL_ALIQUOTA_7, ' +
    '  IMP.PERCENTUAL_ALIQUOTA_8, ' +
    '  MOD.MODELO_ECF as NOME_MODELO_ECF, ' +
    '  GER.DIRETORIO_ARQUIVOS_REQUISICOES, ' +
    '  GER.DIRETORIO_ARQUIVOS_RESPOSTAS ' +
    'from ' +
    '  IMPRESSORAS_ECF IMP ' +

    'inner join VW_MODELOS_ECF MOD ' +
    'on IMP.MODELO_ECF = MOD.MODELO_ID ' +

    'left join GERENCIADORES_CARTOES_TEF GER ' +
    'on IMP.GERENCIADOR_CARTAO_ID = GER.GERENCIADOR_CARTAO_ID ';

  setFiltros(getFiltros);

  AddColuna('ECF_ID', True);
  AddColuna('MODELO_ECF');
  AddColuna('EMPRESA_ID');
  AddColuna('NOME_FORMA_PAGAMENTO_COBRANCA');
  AddColuna('NOME_FORMA_PAGAMENTO_CHEQUE');
  AddColuna('NOME_FORMA_PAGAMENTO_CREDITO');
  AddColuna('NOME_FORMA_PAGAMENTO_POS');
  AddColuna('NOME_FORMA_PAGAMENTO_DINHEIRO');
  AddColuna('NOME_FORMA_PAGAMENTO_CARTAO');
  AddColuna('COMPUTADOR_ECF');
  AddColuna('GERENCIADOR_CARTAO_ID');
  AddColuna('CODIGO_FABRICA');
  AddColuna('NUMERO_ECF');
  AddColuna('INDICE_RECEBIMENTO_NAO_FISCAL');
  AddColuna('PORTA_SERIAL');
  AddColuna('PERCENTUAL_ALIQUOTA_1');
  AddColuna('PERCENTUAL_ALIQUOTA_2');
  AddColuna('PERCENTUAL_ALIQUOTA_3');
  AddColuna('PERCENTUAL_ALIQUOTA_4');
  AddColuna('PERCENTUAL_ALIQUOTA_5');
  AddColuna('PERCENTUAL_ALIQUOTA_6');
  AddColuna('PERCENTUAL_ALIQUOTA_7');
  AddColuna('PERCENTUAL_ALIQUOTA_8');
  AddColuna('ATIVO');
  AddColunaSL('NOME_MODELO_ECF');
  AddColunaSL('DIRETORIO_ARQUIVOS_REQUISICOES');
  AddColunaSL('DIRETORIO_ARQUIVOS_RESPOSTAS');
end;

function TECF.getRecordECF: RecECF;
begin
  Result := RecECF.Create;
  Result.EcfId                         := getInt('ECF_ID', True);
  Result.ModeloEcf                     := getInt('MODELO_ECF');
  Result.empresa_id                    := getInt('EMPRESA_ID');
  Result.nome_forma_pagamento_cobranca := getString('NOME_FORMA_PAGAMENTO_COBRANCA');
  Result.nome_forma_pagamento_cheque   := getString('NOME_FORMA_PAGAMENTO_CHEQUE');
  Result.nome_forma_pagamento_credito  := getString('NOME_FORMA_PAGAMENTO_CREDITO');
  Result.nome_forma_pagamento_pos      := getString('NOME_FORMA_PAGAMENTO_POS');
  Result.nome_forma_pagamento_dinheiro := getString('NOME_FORMA_PAGAMENTO_DINHEIRO');
  Result.nome_forma_pagamento_cartao   := getString('NOME_FORMA_PAGAMENTO_CARTAO');
  Result.computador_ecf                := getString('COMPUTADOR_ECF');
  Result.GerenciadorCartaoId           := getInt('GERENCIADOR_CARTAO_ID');
  Result.CodigoFabrica                 := getString('CODIGO_FABRICA');
  Result.NumeroEcf                     := getInt('NUMERO_ECF');
  Result.IndiceRecebimentoNaoFiscal    := getInt('INDICE_RECEBIMENTO_NAO_FISCAL');
  Result.ativo                         := getString('ATIVO');
  Result.nome_modelo_ecf               := getString('NOME_MODELO_ECF');
  Result.porta_serial                  := getInt('PORTA_SERIAL');
  Result.PercentualAliquota1           := getDouble('PERCENTUAL_ALIQUOTA_1');
  Result.PercentualAliquota2           := getDouble('PERCENTUAL_ALIQUOTA_2');
  Result.PercentualAliquota3           := getDouble('PERCENTUAL_ALIQUOTA_3');
  Result.PercentualAliquota4           := getDouble('PERCENTUAL_ALIQUOTA_4');
  Result.PercentualAliquota5           := getDouble('PERCENTUAL_ALIQUOTA_5');
  Result.PercentualAliquota6           := getDouble('PERCENTUAL_ALIQUOTA_6');
  Result.PercentualAliquota7           := getDouble('PERCENTUAL_ALIQUOTA_7');
  Result.PercentualAliquota8           := getDouble('PERCENTUAL_ALIQUOTA_8');
  Result.DiretorioArquivosRequisicoes  := getString('DIRETORIO_ARQUIVOS_REQUISICOES');
  Result.DiretorioArquivosRespostas    := getString('DIRETORIO_ARQUIVOS_RESPOSTAS');
end;

function AtualizarECF(
  pConexao: TConexao;
  pECFId: Word;
  pModeloECF: Word;
  pEmpresaId: Word;
  pNomeFormaPagamentoCobranca: string;
  pNomeFormaPagamentoCheque: string;
  pNomeFormaPagamentoCredito: string;
  pNomeFormaPagamentoPOS: string;
  pNomeFormaPagamentoCartao: string;
  pNomeFormapagamentodinheiro: string;
  pComputadorECF: string;
  pGerenciadorCartaoId: Integer;
  pCodigoFabrica: string;
  pNumeroECF: Integer;
  pIndiceRecebimentoNaoFiscal: Integer;
  pPortaSerial: Integer;
  pPercentualAliquota1: Double;
  pPercentualAliquota2: Double;
  pPercentualAliquota3: Double;
  pPercentualAliquota4: Double;
  pPercentualAliquota5: Double;
  pPercentualAliquota6: Double;
  pPercentualAliquota7: Double;
  pPercentualAliquota8: Double;
  pAtivo: string
): RecRespostaBanco;
var
  t: TECF;
  novo: Boolean;
  seq: TSequencia;
begin
  Result.TeveErro := False;
  t := TECF.Create(pConexao);

  novo := pECFId = 0;

  try
    if novo then begin
      seq := TSequencia.Create(pConexao, 'SEQ_ECF_ID');
      pECFId := seq.getProximaSequencia;
      result.AsInt := pECFId;
      seq.Free;
    end;

    t.setInt('ECF_ID', pECFId, True);
    t.setInt('MODELO_ECF', pModeloECF);
    t.setInt('EMPRESA_ID', pEmpresaId);
    t.setString('NOME_FORMA_PAGAMENTO_COBRANCA', pNomeFormaPagamentoCobranca);
    t.setString('NOME_FORMA_PAGAMENTO_CHEQUE', pNomeFormaPagamentoCheque);
    t.setString('NOME_FORMA_PAGAMENTO_CREDITO', pNomeFormaPagamentoCredito);
    t.setString('NOME_FORMA_PAGAMENTO_POS', pNomeFormaPagamentoPOS);
    t.setString('NOME_FORMA_PAGAMENTO_DINHEIRO', pNomeFormapagamentodinheiro);
    t.setString('NOME_FORMA_PAGAMENTO_CARTAO', pNomeFormaPagamentoCartao);
    t.setString('COMPUTADOR_ECF', pComputadorECF);
    t.setIntN('GERENCIADOR_CARTAO_ID', pGerenciadorCartaoId);
    t.setString('CODIGO_FABRICA', pCodigoFabrica);
    t.setInt('NUMERO_ECF', pNumeroECF);
    t.setInt('INDICE_RECEBIMENTO_NAO_FISCAL', pIndiceRecebimentoNaoFiscal);
    t.setInt('PORTA_SERIAL', pPortaSerial);
    t.setDouble('PERCENTUAL_ALIQUOTA_1', pPercentualAliquota1);
    t.setDouble('PERCENTUAL_ALIQUOTA_2', pPercentualAliquota2);
    t.setDouble('PERCENTUAL_ALIQUOTA_3', pPercentualAliquota3);
    t.setDouble('PERCENTUAL_ALIQUOTA_4', pPercentualAliquota4);
    t.setDouble('PERCENTUAL_ALIQUOTA_5', pPercentualAliquota5);
    t.setDouble('PERCENTUAL_ALIQUOTA_6', pPercentualAliquota6);
    t.setDouble('PERCENTUAL_ALIQUOTA_7', pPercentualAliquota7);
    t.setDouble('PERCENTUAL_ALIQUOTA_8', pPercentualAliquota8);
    t.setString('ATIVO', pAtivo);

    if novo then
      t.Inserir
    else
      t.Atualizar;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function BuscarECFs(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecECF>;
var
  i: Integer;
  t: TECF;
begin
  Result := nil;
  t := TECF.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordECF;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirECF(pConexao: TConexao; pECFId: Word): RecRespostaBanco;
var
  t: TECF;
begin
  Result.TeveErro := False;
  t := TECF.Create(pConexao);

  t.setInt('ECF_ID', pECFId, True);

  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function BuscarModelosECF(pConexao: TConexao): TArray<RecModelosECF>;
var
  i: Integer;
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);
  vSql.SQL.Text := 'select * from VW_MODELOS_ECF';
  vSql.Pesquisar;

  SetLength(Result, vSql.GetQuantidadeRegistros);
  for i := 0 to vSql.GetQuantidadeRegistros -1 do begin
    Result[i].modelo_id := vSql.GetInt(0);
    Result[i].modelo_ecf := vSql.GetString(1);

    vSql.Next;
  end;
  vSql.Free;
end;

end.
