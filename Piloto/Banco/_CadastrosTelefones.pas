unit _CadastrosTelefones;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecCadastrosTelefones = record
    CadastroId: Integer;
    TipoTelefone: string;
    TipoTelefoneAnalitico: string;
    Ramal: Integer;
    Telefone: string;
    Observacao: string;
    Ordem: Integer;
  end;

  TCadastrosTelefones = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordCadastrosTelefones: RecCadastrosTelefones;
  end;

function BuscarCadastrosTelefones(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCadastrosTelefones>;

function AtualizarTelefones(
  pConexao: TConexao;
  pCadastroId: Integer;
  pTelefones: TArray<RecCadastrosTelefones>
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TCadastrosTelefones }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CADASTRO_ID = :P1 ',
      'order by ' +
      '  CADASTRO_ID '
    );
end;

constructor TCadastrosTelefones.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CADASTROS_TELEFONES');

  FSql := 
    'select ' +
    '  CADASTRO_ID, ' +
    '  TIPO_TELEFONE, ' +
    '  RAMAL, ' +
    '  TELEFONE, ' +
    '  OBSERVACAO, ' +
    '  ORDEM ' +
    'from ' +
    '  CADASTROS_TELEFONES ';

  setFiltros(getFiltros);

  AddColuna('CADASTRO_ID', True);
  AddColuna('TIPO_TELEFONE');
  AddColuna('RAMAL');
  AddColuna('TELEFONE');
  AddColuna('OBSERVACAO');
  AddColuna('ORDEM', True);
end;

function TCadastrosTelefones.getRecordCadastrosTelefones: RecCadastrosTelefones;
begin
  Result.CadastroId                 := getInt('CADASTRO_ID', True);
  Result.TipoTelefone               := getString('TIPO_TELEFONE');

  Result.TipoTelefoneAnalitico :=
    _Biblioteca.Decode(
      Result.TipoTelefone,[
        'R', 'Residencial',
        'C', 'Comercial',
        'F', 'Fax',
        'E', 'Celular',
        'A', 'Recado',
        'P', 'Pai/Mae',
        'G', 'Gratuito'
      ]
    );

  Result.Ramal                      := getInt('RAMAL');
  Result.Telefone                   := getString('TELEFONE');
  Result.Observacao                 := getString('OBSERVACAO');
  Result.Ordem                      := getInt('ORDEM', True);
end;

function BuscarCadastrosTelefones(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCadastrosTelefones>;
var
  i: Integer;
  t: TCadastrosTelefones;
begin
  Result := nil;
  t := TCadastrosTelefones.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordCadastrosTelefones;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function AtualizarTelefones(
  pConexao: TConexao;
  pCadastroId: Integer;
  pTelefones: TArray<RecCadastrosTelefones>
): RecRetornoBD;
var
  i: Integer;
  vExec: TExecucao;
  t: TCadastrosTelefones;
begin
  Result.Iniciar;

  t := TCadastrosTelefones.Create(pConexao);
  vExec := TExecucao.Create(pConexao);

  try
    vExec.Add('delete from CADASTROS_TELEFONES ');
    vExec.Add('where CADASTRO_ID = :P1 ');
    vExec.Executar([pCadastroId]);

    t.SetInt('CADASTRO_ID', pCadastroId, True);
    for i := Low(pTelefones) to High(pTelefones) do begin
      t.SetInt('ORDEM', pTelefones[i].Ordem, True);
      t.SetString('TIPO_TELEFONE', pTelefones[i].TipoTelefone);
      t.SetString('OBSERVACAO', pTelefones[i].Observacao);
      t.SetString('TELEFONE', pTelefones[i].Telefone);
      t.SetIntN('RAMAL', pTelefones[i].Ramal);
      t.SetString('OBSERVACAO', pTelefones[i].Observacao);

      t.Inserir;
    end;
  except
    on e: Exception do
      Result.TratarErro(e);
  end;

  vExec.Free;
  t.Free;
end;

end.
