unit _ClientesCondicPagtoRestrit;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TClientesCondicPagtoRestrit = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordClientesCondicPagtoNaoLib: RecClientesCondicPagtoRestrit;
  end;

function AtualizarClientesCondicPagtoRestrit(
  pConexao: TConexao;
  pClienteId: Integer;
  pCondicoesNaoLibsIds: TArray<Integer>
): RecRetornoBD;

function BuscarClientesCondicPagtoRestrit(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecClientesCondicPagtoRestrit>;

function ExcluirClientesCondicPagtoRestrit(
  pConexao: TConexao;
  pClienteId: Integer;
  pCondicaoId: Integer
): RecRetornoBD;

function CondicaoRestritaLiberada(pConexao: TConexao; pCondicaoId: Integer; pClienteId: Integer): Boolean;

function getFiltros: TArray<RecFiltros>;

implementation

{ TClientesCondicPagtoRestrit }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CLIENTE_ID = :P1 '
    );
end;

constructor TClientesCondicPagtoRestrit.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CLIENTES_CONDIC_PAGTO_RESTRIT');

  FSql :=
    'select ' +
    '  CLIENTE_ID, ' +
    '  CONDICAO_ID ' +
    'from ' +
    '  CLIENTES_CONDIC_PAGTO_RESTRIT ';

  setFiltros(getFiltros);

  AddColuna('CLIENTE_ID', True);
  AddColuna('CONDICAO_ID', True);
end;

function TClientesCondicPagtoRestrit.getRecordClientesCondicPagtoNaoLib: RecClientesCondicPagtoRestrit;
begin
  Result.ClienteId  := getInt('CLIENTE_ID', True);
  Result.CondicaoId := getInt('CONDICAO_ID', True);
end;

function AtualizarClientesCondicPagtoRestrit(
  pConexao: TConexao;
  pClienteId: Integer;
  pCondicoesNaoLibsIds: TArray<Integer>
): RecRetornoBD;
var
  i: Integer;
  vExec: TExecucao;
  t: TClientesCondicPagtoRestrit;
begin
  Result.Iniciar;
  vExec := TExecucao.Create(pConexao);
  t := TClientesCondicPagtoRestrit.Create(pConexao);

  try
    vExec.Add('delete from CLIENTES_CONDIC_PAGTO_RESTRIT ');
    vExec.Add('where CLIENTE_ID = :P1 ');
    vExec.Executar([pClienteId]);

    t.setInt('CLIENTE_ID', pClienteId, True);
    for i := Low(pCondicoesNaoLibsIds) to High(pCondicoesNaoLibsIds) do begin
      t.setInt('CONDICAO_ID', pCondicoesNaoLibsIds[i], True);

      t.Inserir;
    end;

  except
    on e: Exception do
      Result.TratarErro(e);
  end;

  t.Free;
end;

function BuscarClientesCondicPagtoRestrit(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecClientesCondicPagtoRestrit>;
var
  i: Integer;
  t: TClientesCondicPagtoRestrit;
begin
  Result := nil;
  t := TClientesCondicPagtoRestrit.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordClientesCondicPagtoNaoLib;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirClientesCondicPagtoRestrit(
  pConexao: TConexao;
  pClienteId: Integer;
  pCondicaoId: Integer
): RecRetornoBD;
var
  t: TClientesCondicPagtoRestrit;
begin
  Result.TeveErro := False;
  t := TClientesCondicPagtoRestrit.Create(pConexao);

  try
    t.setInt('CLIENTE_ID', pClienteId, True);
    t.setInt('CONDICAO_ID', pCondicaoId, True);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function CondicaoRestritaLiberada(pConexao: TConexao; pCondicaoId: Integer; pClienteId: Integer): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  count(*) ');
  vSql.Add('from ');
  vSql.Add('  CLIENTES_CONDIC_PAGTO_RESTRIT ');
  vSql.Add('where CONDICAO_ID = :P1 ');
  vSql.Add('and CLIENTE_ID = :P2 ');

  vSql.Pesquisar([pCondicaoId, pClienteId]);

  Result := vSql.GetInt(0) > 0;

  vSql.Free;
end;

end.
