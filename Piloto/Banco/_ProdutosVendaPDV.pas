unit _ProdutosVendaPDV;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _RecordsPDV;

type
  TProdutoVendaPDV = class(TOperacoes)
  private
    function getRecordProdutoVendaPDV: RecProdutoVendaPDV;
  public
    constructor Create(pConexao: TConexao);
  end;

function BuscarProduto(pConexao: TConexao; pIndice: Integer; pFiltros: array of Variant): TArray<RecProdutoVendaPDV>;
function getProdutoIdViaCodigo(pConexao: TConexao; pCodigo: string; pTipo: string): Integer;

function getFiltros: TArray<RecFiltros>;

implementation

{ TProdutoVendaPDV }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where PRODUTO_ID = :P2 '
    );
end;

constructor TProdutoVendaPDV.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'VW_PRODUTOS_VENDA_PDV');

  FSql :=
    'select ' +
    '  PRODUTO_ID, ' +
    '  NOME_PRODUTO, ' +
    '  MULTIPLO_VENDA, ' +
    '  UNIDADE_VENDA, ' +
    '  MARCA_ID, ' +
    '  NOME_MARCA, ' +
    '  PRECO_PDV, ' +
    '  PRODUTO_DIVERSOS_PDV, ' +
    '  TIPO_CONTROLE_ESTOQUE ' +
    'from ' +
    '  VW_PRODUTOS_VENDA_PDV ';

  SetFiltros(getFiltros);

  AddColunaSL('PRODUTO_ID');
  AddColunaSL('NOME_PRODUTO');
  AddColunaSL('MULTIPLO_VENDA');
  AddColunaSL('UNIDADE_VENDA');
  AddColunaSL('MARCA_ID');
  AddColunaSL('NOME_MARCA');
  AddColunaSL('PRECO_PDV');
  AddColunaSL('PRODUTO_DIVERSOS_PDV');
  AddColunaSL('TIPO_CONTROLE_ESTOQUE');
end;

function TProdutoVendaPDV.getRecordProdutoVendaPDV: RecProdutoVendaPDV;
begin
  Result.ProdutoId      := getInt('PRODUTO_ID');
  Result.NomeProduto    := getString('NOME_PRODUTO');
  Result.MultiploVenda  := getDouble('MULTIPLO_VENDA');
  Result.UnidadeVenda   := getString('UNIDADE_VENDA');
  Result.MarcaId        := getInt('MARCA_ID');
  Result.NomeMarca      := getString('NOME_MARCA');
  Result.PrecoPDV       := getDouble('PRECO_PDV');
  Result.produtoDiversosPDV  := getString('PRODUTO_DIVERSOS_PDV');
  Result.TipoControleEstoque := getString('TIPO_CONTROLE_ESTOQUE');
end;

function BuscarProduto(pConexao: TConexao; pIndice: Integer; pFiltros: array of Variant): TArray<RecProdutoVendaPDV>;
var
  i: Integer;
  t: TProdutoVendaPDV;

  procedure CarregarFotos;
  var
    vSql: TConsulta;
  begin
    vSql := TConsulta.Create(pConexao);

    with vSql.SQL do begin
      Add('select ');
      Add('  FOTO_1, ');
      Add('  FOTO_2, ');
      Add('  FOTO_3 ');
      Add('from ');
      Add('  PRODUTOS ');
      Add('where PRODUTO_ID = :P1 ');
      Add('and FOTO_1 is not null ');
    end;

    if vSql.Pesquisar([Result[i].ProdutoId]) then begin
      Result[i].Foto1     := vSql.getFoto(0);
      Result[i].Foto2     := vSql.getFoto(1);
      Result[i].Foto3     := vSql.getFoto(2);
    end;
    vSql.Active := False;
    vSql.Free;
  end;

begin
  Result := nil;
  t := TProdutoVendaPDV.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordProdutoVendaPDV;
      CarregarFotos;

      t.ProximoRegistro;
    end;
  end;
  t.Free;
end;

{
  pTipo
  C - C�digo de barras
  D - C�digo de balan�a
}
function getProdutoIdViaCodigo(pConexao: TConexao; pCodigo: string; pTipo: string): Integer;
var
  vSql: TConsulta;
begin
  Result := -1;
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  PRODUTO_ID ');
  vSql.SQL.Add('from ');
  vSql.SQL.Add('  VW_PRODUTOS_VENDA_PDV ');

  if pTipo = 'C' then
    vSql.SQL.Add('where CODIGO_BARRAS = :P1')
  else
    vSql.SQL.Add('where CODIGO_BALANCA = :P1');

  if vSql.Pesquisar([pCodigo]) then
    Result := vSql.GetInt(0);

  vSql.Free;
end;

end.
