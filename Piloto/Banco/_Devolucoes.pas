unit _Devolucoes;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsOrcamentosVendas,
  _DevolucoesItens, _BibliotecaGenerica;

{$M+}
type
  TDevolucoes = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordDevolucoes: RecDevolucoes;
  end;

function AtualizarDevolucoes(
  pConexao: TConexao;
  pDevolucaoId: Integer;
  pEmpresaId: Integer;
  pOrcamentoId: Integer;
  pClienteCreditoId: Integer;
  pValorBruto: Double;
  pValorLiquido: Double;
  pValorDesconto: Double;
  pValorOutrasDespesas: Double;
  pValorFrete: Double;
  pObservacoes: string;
  pDevolverFrete: string;
  pDevolverOutrasDespesas: string;
  pMotivoDevolucao: string;
  pTemItemConfirmar: Boolean;
  pSomenteFiscal: Boolean;
  pItens: TArray<RecDevolucoesItens>;
  pQtdeDiasDevolucao: Integer
): RecRetornoBD;

function BuscarDevolucoes(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecDevolucoes>;

function BuscarDevolucoesComando(pConexao: TConexao; pComando: string): TArray<RecDevolucoes>;

function ExcluirDevolucoes(
  pConexao: TConexao;
  pDevolucaoId: Integer
): RecRetornoBD;

function ExistemDevolucoesPendentes(pConexao: TConexao; pOrcamentoId: Integer): Boolean;
function PodeGerarDevolucao(pConexao: TConexao; pOrcamentoId: Integer): RecRetornoBD;
function CancelarDevolucao(pConexao: TConexao; pDevolucaoId: Integer): RecRetornoBD;
function ConsolidarDevolucao(pConexao: TConexao; pDevolucaoId: Integer; pEmTransacao: Boolean): RecRetornoBD;
function MaiorDataEntrega(pConexao: TConexao; pOrcamentoId: Integer; pItens: TArray<Integer>): TDate;

function getFiltros: TArray<RecFiltros>;

implementation

{ TDevolucoes }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where DEV.DEVOLUCAO_ID = :P1 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where DEV.ORCAMENTO_ID = :P1 '
    );
end;

constructor TDevolucoes.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'DEVOLUCOES');

  FSql :=
    'select ' +
    '  DEV.DEVOLUCAO_ID, ' +
    '  DEV.EMPRESA_ID, ' +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA, ' +
    '  DEV.ORCAMENTO_ID, ' +
    '  DEV.CLIENTE_CREDITO_ID, ' +
    '  CCR.NOME_FANTASIA as NOME_CLIENTE_CREDITO, ' +
    '  DEV.USUARIO_CADASTRO_ID, ' +
    '  FUN.NOME as NOME_USUARIO_DEVOLUCAO, ' +
    '  DEV.VALOR_BRUTO, ' +
    '  DEV.STATUS, ' +
    '  DEV.VALOR_LIQUIDO, ' +
    '  DEV.VALOR_DESCONTO, ' +
    '  DEV.VALOR_OUTRAS_DESPESAS, ' +
    '  DEV.VALOR_FRETE, ' +
    '  DEV.DATA_HORA_DEVOLUCAO, ' +
    '  DEV.OBSERVACOES, ' +
    '  ORC.CLIENTE_ID, ' +
    '  CAD.RAZAO_SOCIAL as NOME_CLIENTE, ' +

    '  CAD.NOME_FANTASIA as APELIDO, ' +
    '  CAD.LOGRADOURO, ' +
    '  CAD.COMPLEMENTO, ' +
    '  CAD.NUMERO, ' +
    '  CAD.BAIRRO_ID, ' +
    '  BAI.NOME as NOME_BAIRRO, ' +
    '  CAD.CEP, ' +
    '  CID.CIDADE_ID, ' +
    '  CID.NOME as NOME_CIDADE, ' +
    '  EST.ESTADO_ID, ' +
    '  EST.NOME as NOME_ESTADO, ' +
    '  CAD.TELEFONE_PRINCIPAL, ' +
    '  CAD.TELEFONE_CELULAR, ' +
    '  ORC.CONDICAO_ID, ' +
    '  CON.NOME as NOME_CONDICAO_PAGAMENTO, ' +

    '  DEV.USUARIO_CONFIRMACAO_ID, ' +
    '  FUC.NOME as NOME_USUARIO_CONFIRMACAO, ' +
    '  DEV.DATA_HORA_CONFIRMACAO, ' +
    '  DEV.DEVOLVER_FRETE, ' +
    '  DEV.DEVOLVER_OUTRAS_DESPESAS, ' +
    '  DEV.MOTIVO_DEVOLUCAO, ' +
    '  PAG.PAGAR_ID as CREDITO_PAGAR_ID, ' +
    '  DEV.SOMENTE_FISCAL, ' +
    '  DEV.QTDE_DIAS_DEVOLUCAO, ' +
    '  case when ITE.QUANTIDADE_PRODUTOS_ENT > 0 then ''S'' else ''N'' end as TEM_PRODUTOS_ENTREGUES ' +
    'from ' +
    '  DEVOLUCOES DEV ' +

    'inner join FUNCIONARIOS FUN ' +
    'on DEV.USUARIO_CADASTRO_ID = FUN.FUNCIONARIO_ID ' +

    'inner join EMPRESAS EMP ' +
    'on DEV.EMPRESA_ID = EMP.EMPRESA_ID ' +

    'inner join ORCAMENTOS ORC ' +
    'on DEV.ORCAMENTO_ID = ORC.ORCAMENTO_ID ' +

    'inner join CADASTROS CAD ' +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID ' +

    'inner join BAIRROS BAI ' +
    'on CAD.BAIRRO_ID = BAI.BAIRRO_ID ' +

    'inner join CIDADES CID ' +
    'on BAI.CIDADE_ID =  CID.CIDADE_ID ' +

    'inner join ESTADOS EST ' +
    'on CID.ESTADO_ID = EST.ESTADO_ID ' +

    'inner join CONDICOES_PAGAMENTO CON ' +
    'on ORC.CONDICAO_ID = CON.CONDICAO_ID ' +

    'left join CADASTROS CCR ' +
    'on DEV.CLIENTE_CREDITO_ID = CCR.CADASTRO_ID ' +

    'left join CONTAS_PAGAR PAG ' +
    'on DEV.DEVOLUCAO_ID = PAG.DEVOLUCAO_ID ' +

    'left join ( ' +
    '  select ' +
    '    DEVOLUCAO_ID, ' +
    '    count(*) as QUANTIDADE_PRODUTOS_ENT ' +
    '  from ' +
    '    DEVOLUCOES_ITENS ' +
    '  where DEVOLVIDOS_ENTREGUES > 0 ' +
    '  group by ' +
    '    DEVOLUCAO_ID ' +
    ') ITE ' +
    'on DEV.DEVOLUCAO_ID = ITE.DEVOLUCAO_ID ' +

    'left join FUNCIONARIOS FUC ' +
    'on DEV.USUARIO_CONFIRMACAO_ID = FUC.FUNCIONARIO_ID ';

  setFiltros(getFiltros);

  AddColuna('DEVOLUCAO_ID', True);
  AddColuna('EMPRESA_ID');
  AddColunaSL('NOME_EMPRESA');
  AddColuna('ORCAMENTO_ID');
  AddColuna('CLIENTE_CREDITO_ID');
  AddColunaSL('USUARIO_CADASTRO_ID');
  AddColunaSL('NOME_USUARIO_DEVOLUCAO');
  AddColuna('VALOR_BRUTO');
  AddColuna('VALOR_LIQUIDO');
  AddColuna('VALOR_DESCONTO');
  AddColuna('VALOR_OUTRAS_DESPESAS');
  AddColuna('VALOR_FRETE');
  AddColunaSL('DATA_HORA_DEVOLUCAO');
  AddColunaSL('STATUS');
  AddColuna('OBSERVACOES');
  AddColunaSL('CLIENTE_ID');
  AddColunaSL('NOME_CLIENTE');
  AddColunaSL('NOME_CLIENTE_CREDITO');
  AddColunaSL('USUARIO_CONFIRMACAO_ID');
  AddColunaSL('NOME_USUARIO_CONFIRMACAO');
  AddColunaSL('DATA_HORA_CONFIRMACAO');
  AddColuna('DEVOLVER_FRETE');
  AddColuna('DEVOLVER_OUTRAS_DESPESAS');
  AddColuna('MOTIVO_DEVOLUCAO');
  AddColunaSL('CREDITO_PAGAR_ID');
  AddColunaSL('TEM_PRODUTOS_ENTREGUES');
  AddColuna('SOMENTE_FISCAL');

  AddColunaSL('APELIDO');
  AddColunaSL('LOGRADOURO');
  AddColunaSL('COMPLEMENTO');
  AddColunaSL('NUMERO');
  AddColunaSL('BAIRRO_ID');
  AddColunaSL('NOME_BAIRRO');
  AddColunaSL('CEP');
  AddColunaSL('CIDADE_ID');
  AddColunaSL('NOME_CIDADE');
  AddColunaSL('ESTADO_ID');
  AddColunaSL('NOME_ESTADO');
  AddColunaSL('TELEFONE_PRINCIPAL');
  AddColunaSL('TELEFONE_CELULAR');
  AddColunaSL('CONDICAO_ID');
  AddColunaSL('NOME_CONDICAO_PAGAMENTO');
  AddColuna('QTDE_DIAS_DEVOLUCAO');
end;

function TDevolucoes.getRecordDevolucoes: RecDevolucoes;
begin
  Result.devolucao_id         := getInt('DEVOLUCAO_ID', True);
  Result.EmpresaId            := getInt('EMPRESA_ID');
  Result.NomeEmpresa          := getString('NOME_EMPRESA');
  Result.orcamento_id         := getInt('ORCAMENTO_ID');
  Result.ClienteCreditoId     := getInt('CLIENTE_CREDITO_ID');
  Result.usuario_cadastro_id  := getInt('USUARIO_CADASTRO_ID');
  Result.NomeUsuarioDevolucao := getString('NOME_USUARIO_DEVOLUCAO');
  Result.valor_bruto          := getDouble('VALOR_BRUTO');
  Result.valor_liquido        := getDouble('VALOR_LIQUIDO');
  Result.valor_desconto       := getDouble('VALOR_DESCONTO');
  Result.ValorOutrasDespesas  := getDouble('VALOR_OUTRAS_DESPESAS');
  Result.ValorFrete           := getDouble('VALOR_FRETE');
  Result.data_hora_devolucao  := getData('DATA_HORA_DEVOLUCAO');
  Result.observacoes          := getString('OBSERVACOES');
  Result.ClienteId            := getInt('CLIENTE_ID');
  Result.NomeCliente          := getString('NOME_CLIENTE');
  Result.NomeClienteCredito   := getString('NOME_CLIENTE_CREDITO');
  Result.Status               := getString('STATUS');
  Result.UsuarioConfirmacaoId   := getInt('USUARIO_CONFIRMACAO_ID');
  Result.NomeUsuarioConfirmacao := getString('NOME_USUARIO_CONFIRMACAO');
  Result.DataHoraConfirmacao    := getData('DATA_HORA_CONFIRMACAO');
  Result.DevolverFrete          := getString('DEVOLVER_FRETE');
  Result.DevolverOutrasDespesas := getString('DEVOLVER_OUTRAS_DESPESAS');
  Result.MotivoDevolucao        := getString('MOTIVO_DEVOLUCAO');
  Result.CreditoPagarId         := getInt('CREDITO_PAGAR_ID');
  Result.TemProdutosEntregues   := getString('TEM_PRODUTOS_ENTREGUES');
  Result.SomenteFiscal          := getString('SOMENTE_FISCAL');

  Result.Apelido                := getString('APELIDO');
  Result.Logradouro             := getString('LOGRADOURO');
  Result.Complemento            := getString('COMPLEMENTO');
  Result.Numero                 := getString('NUMERO');
  Result.BairroId               := getInt('BAIRRO_ID');
  Result.NomeBairro             := getString('NOME_BAIRRO');
  Result.Cep                    := getString('CEP');
  Result.CidadeId               := getInt('CIDADE_ID');
  Result.NomeCidade             := getString('NOME_CIDADE');
  Result.EstadoId               := getString('ESTADO_ID');
  Result.NomeEstado             := getString('NOME_ESTADO');
  Result.TelefonePrincipal      := getString('TELEFONE_PRINCIPAL');
  Result.TelefoneCelular        := getString('TELEFONE_CELULAR');
  Result.CondicaoId             := getInt('CONDICAO_ID');
  Result.NomeCondicaoPagamento  := getString('NOME_CONDICAO_PAGAMENTO');
  Result.QtdeDiasDevolucao      := getInt('QTDE_DIAS_DEVOLUCAO');
end;

function AtualizarDevolucoes(
  pConexao: TConexao;
  pDevolucaoId: Integer;
  pEmpresaId: Integer;
  pOrcamentoId: Integer;
  pClienteCreditoId: Integer;
  pValorBruto: Double;
  pValorLiquido: Double;
  pValorDesconto: Double;
  pValorOutrasDespesas: Double;
  pValorFrete: Double;
  pObservacoes: string;
  pDevolverFrete: string;
  pDevolverOutrasDespesas: string;
  pMotivoDevolucao: string;
  pTemItemConfirmar: Boolean;
  pSomenteFiscal: Boolean;
  pItens: TArray<RecDevolucoesItens>;
  pQtdeDiasDevolucao: Integer
): RecRetornoBD;
var
  i: Integer;
  t: TDevolucoes;
  vNovo: Boolean;
  vSeq: TSequencia;
  vItem: TDevolucoesItens;
  vProc: TProcedimentoBanco;
begin
  Result.TeveErro := False;
  t := TDevolucoes.Create(pConexao);
  vItem := TDevolucoesItens.Create(pConexao);
  pConexao.SetRotina('ATUALIZAR_DEVOLUCAO');
  vProc := TProcedimentoBanco.Create(pConexao, 'CONSOLIDAR_DEVOLUCAO');

  try
    pConexao.IniciarTransacao;

    vNovo := pDevolucaoId = 0;
    if vNovo then begin
      vSeq := TSequencia.Create(pConexao, 'SEQ_DEVOLUCAO_ID');
      pDevolucaoId := vSeq.getProximaSequencia;
      Result.AsInt := pDevolucaoId;
      vSeq.Free;
    end;

    t.setInt('DEVOLUCAO_ID', pDevolucaoId, True);
    t.setInt('EMPRESA_ID', pEmpresaId);
    t.setInt('ORCAMENTO_ID', pOrcamentoId);
    t.setIntN('CLIENTE_CREDITO_ID', pClienteCreditoId);
    t.setDouble('VALOR_BRUTO', pValorBruto);
    t.setDouble('VALOR_LIQUIDO', pValorLiquido);
    t.setDouble('VALOR_DESCONTO', pValorDesconto);
    t.setDouble('VALOR_OUTRAS_DESPESAS', pValorOutrasDespesas);
    t.setDouble('VALOR_FRETE', pValorFrete);
    t.setStringN('OBSERVACOES', pObservacoes);
    t.setStringN('DEVOLVER_FRETE', pDevolverFrete);
    t.setStringN('DEVOLVER_OUTRAS_DESPESAS', pDevolverOutrasDespesas);
    t.setStringN('MOTIVO_DEVOLUCAO', pMotivoDevolucao);
    t.setString('SOMENTE_FISCAL', IIfStr(pSomenteFiscal, 'S', 'N'));
    t.setInt('QTDE_DIAS_DEVOLUCAO', pQtdeDiasDevolucao);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    vItem.SetInt('DEVOLUCAO_ID', pDevolucaoId, True);
    for i := Low(pItens) to High(pItens) do begin
      vItem.SetInt('ITEM_ID', pItens[i].ItemId, True);
      vItem.setInt('PRODUTO_ID', pItens[i].ProdutoId);

      vItem.setDouble('DEVOLVIDOS_ENTREGUES', pItens[i].QuantidadeDevolvidosEntregues);
      vItem.setDouble('DEVOLVIDOS_PENDENCIAS', pItens[i].QuantidadeDevolvidosPendencias);
      vItem.setDouble('DEVOLVIDOS_SEM_PREVISAO', pItens[i].QuantidadeDevolvSemPrevisao);

      vItem.setDouble('VALOR_BRUTO', pItens[i].ValorBruto);
      vItem.setDouble('VALOR_LIQUIDO', pItens[i].ValorLiquido);

      vItem.setDouble('VALOR_DESCONTO', pItens[i].ValorDesconto);
      vItem.setDouble('VALOR_OUTRAS_DESPESAS', pItens[i].ValorOutrasDespesas);
      vItem.setDouble('VALOR_FRETE', pItens[i].ValorFrete);
      vItem.setDouble('VALOR_ST', pItens[i].ValorST);
      vItem.setDouble('VALOR_IPI', pItens[i].ValorIPI);

      vItem.setDouble('PRECO_UNITARIO', pItens[i].PrecoUnitario);
      vItem.setString('SOMENTE_FISCAL', IIfStr(pSomenteFiscal, 'S', 'N'));

      vItem.Inserir;
    end;

    if not pTemItemConfirmar then begin
      pConexao.SetRotina('CONSOLIDAR_DEVOLUCAO');
      vProc.Params[0].AsInteger := pDevolucaoId;
      vProc.Executar;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vProc.Free;
  vItem.Free;
  t.Free;
end;

function BuscarDevolucoes(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecDevolucoes>;
var
  i: Integer;
  t: TDevolucoes;
begin
  Result := nil;
  t := TDevolucoes.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordDevolucoes;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarDevolucoesComando(pConexao: TConexao; pComando: string): TArray<RecDevolucoes>;
var
  i: Integer;
  t: TDevolucoes;
begin
  Result := nil;
  t := TDevolucoes.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordDevolucoes;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirDevolucoes(
  pConexao: TConexao;
  pDevolucaoId: Integer
): RecRetornoBD;
var
  t: TDevolucoes;
begin
  Result.TeveErro := False;
  t := TDevolucoes.Create(pConexao);

  try
    t.setInt('DEVOLUCAO_ID', pDevolucaoId, True);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function ExistemDevolucoesPendentes(pConexao: TConexao; pOrcamentoId: Integer): Boolean;
var
  p: TConsulta;
begin
  p := TConsulta.Create(pConexao);

  p.Add('select ');
  p.Add('  count(*) ');
  p.Add('from ');
  p.Add('  DEVOLUCOES ');
  p.Add('where ORCAMENTO_ID = :P1 ');
  p.Add('and STATUS = ''A'' ');
  p.Pesquisar([pOrcamentoId]);

  Result := p.GetInt(0) > 0;

  p.Free;
end;

function MaiorDataEntrega(pConexao: TConexao; pOrcamentoId: Integer; pItens: TArray<Integer>): TDate;
var
  p: TConsulta;
begin
  p := TConsulta.Create(pConexao);

  p.Add('select ');
  p.Add('  max(nvl(trunc(ENT.DATA_HORA_ENTREGA), trunc(ORC.DATA_HORA_RECEBIMENTO))) as DATA_ENTREGA ');
  p.Add('from ');
  p.Add('  VW_ENTREGAS ENT ');

  p.Add('inner join VW_ENTREGAS_ITENS ITE ');
  p.Add('on ITE.MOVIMENTO_ID = ENT.MOVIMENTO_ID ');

  p.Add('inner join ORCAMENTOS ORC ');
  p.Add('on ORC.ORCAMENTO_ID = ENT.ORCAMENTO_ID');

  p.Add('where ENT.ORCAMENTO_ID = :P1 ');
  p.Add('and ' + _Biblioteca.FiltroInInt('ITE.PRODUTO_ID', pItens));

  p.Pesquisar([pOrcamentoId]);

  Result := p.GetData('DATA_ENTREGA');

  p.Free;
end;

function PodeGerarDevolucao(pConexao: TConexao; pOrcamentoId: Integer): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('VER_PODE_GERAR_DEVOLUCAO');
  vProc := TProcedimentoBanco.Create(pConexao, 'PODE_GERAR_DEVOLUCAO');

  try
    pConexao.IniciarTransacao;

    vProc.Params[0].AsInteger := pOrcamentoId;
    vProc.Executar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vProc.Free;
end;

function CancelarDevolucao(pConexao: TConexao; pDevolucaoId: Integer): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('CANCELAR_DEVOLUCAO');
  vProc := TProcedimentoBanco.Create(pConexao, 'CANCELAR_DEVOLUCAO');

  try
    pConexao.IniciarTransacao;

    vProc.Params[0].AsInteger := pDevolucaoId;
    vProc.Executar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vProc.Free;
end;

function ConsolidarDevolucao(pConexao: TConexao; pDevolucaoId: Integer; pEmTransacao: Boolean): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.TeveErro := False;
  pConexao.SetRotina('CONSOLIDAR_DEVOLUCAO');
  vProc := TProcedimentoBanco.Create(pConexao, 'CONSOLIDAR_DEVOLUCAO');

  try
    if not pEmTransacao then
      pConexao.IniciarTransacao;

    vProc.Params[0].AsInteger := pDevolucaoId;
    vProc.Executar;

    if not pEmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pEmTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;

  vProc.Free;
end;

end.
