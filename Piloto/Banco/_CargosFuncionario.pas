unit _CargosFuncionario;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TCargosFuncionario = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordFuncoesFuncionario: RecCargosFuncionario;
  end;

function AtualizarCargosFuncionario(
  pConexao: TConexao;
  pCargoId: Integer;
  pNome: string;
  pAtivo: string
): RecRetornoBD;

function BuscarCargosFuncionarios(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCargosFuncionario>;

function ExcluirCargosFuncionario(
  pConexao: TConexao;
  pCargoId: Integer
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TCargosFuncionario }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CARGO_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome do cargo',
      True,
      0,
      'where NOME like :P1 || ''%'' '
    );
end;

constructor TCargosFuncionario.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CARGOS_FUNCIONARIOS');

  FSql :=
    'select ' +
    '  CARGO_ID, ' +
    '  NOME, ' +
    '  ATIVO ' +
    'from ' +
    '  CARGOS_FUNCIONARIOS';

  SetFiltros(GetFiltros);

  AddColuna('CARGO_ID', True);
  AddColuna('NOME');
  AddColuna('ATIVO');
end;

function TCargosFuncionario.GetRecordFuncoesFuncionario: RecCargosFuncionario;
begin
  Result := RecCargosFuncionario.Create;
  Result.CargoId    := GetInt('CARGO_ID', True);
  Result.nome      := getString('NOME');
  Result.ativo     := GetString('ATIVO');
end;

function AtualizarCargosFuncionario(
  pConexao: TConexao;
  pCargoId: Integer;
  pNome: string;
  pAtivo: string
): RecRetornoBD;
var
  t: TCargosFuncionario;
  novo: Boolean;
  seq: TSequencia;
begin
  Result.Iniciar;
  t := TCargosFuncionario.Create(pConexao);

  novo := pCargoId = 0;

  if novo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_CARGO_ID');
    pCargoId := seq.GetProximaSequencia;
    result.AsInt := pCargoId;
    seq.Free;
  end;

  t.SetInt('CARGO_ID', pCargoId, True);
  t.SetString('NOME', pNome);
  t.SetString('ATIVO', pAtivo);

  try
    pConexao.IniciarTransacao;

    if novo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarCargosFuncionarios(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCargosFuncionario>;
var
  i: Integer;
  t: TCargosFuncionario;
begin
  Result := nil;
  t := TCargosFuncionario.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordFuncoesFuncionario;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirCargosFuncionario(
  pConexao: TConexao;
  pCargoId: Integer
): RecRetornoBD;
var
  t: TCargosFuncionario;
begin
  Result.TeveErro := False;
  t := TCargosFuncionario.Create(pConexao);

  t.SetInt('CARGO_ID', pCargoId, True);

  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
