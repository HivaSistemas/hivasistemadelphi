unit _ColunasPadroesTelasUsuario;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, System.SysUtils, _Biblioteca;

type
  RecColunasPadroesTelasUsuario = record
    UsuarioId: Integer;
    Tela: string;
    Campo: string;
    Coluna: string;
    Tamanho: Integer;
  end;

  TColunasPadroesTelasUsuario = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordColunasPadroesTelasUsuario: RecColunasPadroesTelasUsuario;
  end;

function AtualizarColunasPadroes(
  pConexao: TConexao;
  pTela: string;
  pUsuarioId: Integer;
  pColunas: TArray<RecColunasPadroesTelasUsuario>
): RecRetornoBD;

function BuscarColunasPadroes(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant
): TArray<RecColunasPadroesTelasUsuario>;

function GetFiltros: TArray<RecFiltros>;

implementation

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CPT.USUARIO_ID = :P1 ' +
      'and CPT.TELA = :P2 '
    );

end;

constructor TColunasPadroesTelasUsuario.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'COLUNAS_PADROES_TELAS_USUARIO');

  FSql :=
    'select ' +
    '  CPT.USUARIO_ID, ' +
    '  CPT.TELA, ' +
    '  CPT.CAMPO, ' +
    '  CPT.COLUNA, ' +
    '  CPT.TAMANHO ' +
    'from ' +
    '  COLUNAS_PADROES_TELAS_USUARIO CPT ';

  SetFiltros(GetFiltros);

  AddColuna('USUARIO_ID', True);
  AddColuna('TELA', True);
  AddColuna('CAMPO', True);
  AddColuna('COLUNA', True);
  AddColuna('TAMANHO');
end;

function TColunasPadroesTelasUsuario.GetRecordColunasPadroesTelasUsuario: RecColunasPadroesTelasUsuario;
begin
  Result.UsuarioId := getInt('USUARIO_ID', True);
  Result.Tela      := getString('TELA', True);
  Result.Campo     := getString('CAMPO', True);
  Result.Coluna    := getString('COLUNA', True);
  Result.Tamanho   := getInt('TAMANHO');
end;

function AtualizarColunasPadroes(
  pConexao: TConexao;
  pTela: string;
  pUsuarioId: Integer;
  pColunas: TArray<RecColunasPadroesTelasUsuario>
): RecRetornoBD;
var
  i: Integer;
  vExec: TExecucao;
  t: TColunasPadroesTelasUsuario;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZ_COL_PADR_TELA_USUARIO');

  vExec := TExecucao.Create(pConexao);
  t := TColunasPadroesTelasUsuario.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    vExec.Add('delete from COLUNAS_PADROES_TELAS_USUARIO ');
    vExec.Add('where TELA = :P1 ');
    vExec.Add('and USUARIO_ID = :P2 ');
    vExec.Executar([pTela, pUsuarioId]);

    for i := Low(pColunas) to High(pColunas) do begin
      t.setInt('USUARIO_ID', pUsuarioId, True);
      t.setString('TELA', pTela, True);
      t.setString('CAMPO', pColunas[i].Campo, True);
      t.setString('COLUNA', pColunas[i].Coluna, True);
      t.setInt('TAMANHO', pColunas[i].Tamanho);

      t.Inserir;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
  vExec.Free;
end;

function BuscarColunasPadroes(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant
): TArray<RecColunasPadroesTelasUsuario>;
var
  i: Integer;
  t: TColunasPadroesTelasUsuario;
begin
  Result := nil;
  pConexao.SetRotina('BUSCAR_FILTROS_PADR_TELAS_USU');
  t := TColunasPadroesTelasUsuario.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordColunasPadroesTelasUsuario;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
