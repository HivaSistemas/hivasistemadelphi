unit _CargosProfissionais;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils;

{$M+}
type
  RecCargosProfissionais = class
  public
    CargoId: Integer;
    Nome: string;
    Ativo: string;
  end;

  TCargosProfissionais = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordCargosProfissionais: RecCargosProfissionais;
  end;

function AtualizarCargoProfissional(
  pConexao: TConexao;
  pCargoId: Integer;
  pNome: string;
  pAtivo: string
): RecRetornoBD;

function BuscarCargosProfissionais(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecCargosProfissionais>;

function ExcluirCargoProfissional(
  pConexao: TConexao;
  pCargoId: Integer
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TCargosProfissionais }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CARGO_ID = :P1 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome ( avan�ado )',
      True,
      0,
      'where NOME like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  NOME'
    );
end;

constructor TCargosProfissionais.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PROFISSIONAIS_CARGOS');

  FSql :=
    'select ' +
    '  CARGO_ID, ' +
    '  NOME, ' +
    '  ATIVO ' +
    'from ' +
    '  PROFISSIONAIS_CARGOS ';

  SetFiltros(GetFiltros);

  AddColuna('CARGO_ID', True);
  AddColuna('NOME');
  AddColuna('ATIVO');
end;

function TCargosProfissionais.GetRecordCargosProfissionais: RecCargosProfissionais;
begin
  Result := RecCargosProfissionais.Create;

  Result.CargoId  := GetInt('CARGO_ID', True);
  Result.Nome    := GetString('NOME');
  Result.Ativo   := GetString('ATIVO');
end;

function AtualizarCargoProfissional(
  pConexao: TConexao;
  pCargoId: Integer;
  pNome: string;
  pAtivo: string
): RecRetornoBD;
var
  vNovo: Boolean;
  vCargoProfissional: TCargosProfissionais;
begin
  Result.Iniciar;
  vNovo := pCargoId = 0;
  vCargoProfissional := TCargosProfissionais.Create(pConexao);

  if vNovo then begin
    pCargoId := TSequencia.Create(pConexao, 'SEQ_PROFISSIONAIS_CARGO_ID').GetProximaSequencia;
    Result.AsInt := pCargoId;
  end;

  vCargoProfissional.SetInt('CARGO_ID', pCargoId, True);
  vCargoProfissional.SetString('NOME', pNome);
  vCargoProfissional.SetString('ATIVO', pAtivo);

  try
    pConexao.IniciarTransacao;

    if vNovo then
      vCargoProfissional.Inserir
    else
      vCargoProfissional.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vCargoProfissional.Free;
end;

function BuscarCargosProfissionais(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecCargosProfissionais>;
var
  i: Integer;
  t: TCargosProfissionais;

  vSqlAdicional: string;
begin
  Result := nil;
  t := TCargosProfissionais.Create(pConexao);

  vSqlAdicional := '';
  if pSomenteAtivos then
    vSqlAdicional := 'and ATIVO = ''S'' ';

  if t.Pesquisar(pIndice, pFiltros, vSqlAdicional) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordCargosProfissionais;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirCargoProfissional(
  pConexao: TConexao;
  pCargoId: Integer
): RecRetornoBD;
var
  t: TCargosProfissionais;
begin
  pConexao.SetRotina('EXCLUIR_CARGO_PROFIS');
  Result.TeveErro := False;
  t := TCargosProfissionais.Create(pConexao);

  t.SetInt('CARGO_ID', pCargoId, True);

  try
    pConexao.IniciarTransacao;

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
