unit _ItensGruposPlanosFinanceiros;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils;

{$M+}
type
  RecItensGruposPlanosFinanceiros = record
    GrupoFinanceiroId: string;
    PlanoFinanceiroId: string;
    Descricao: string;
    SQL: string;
    Ordem: integer;
  end;

  TItensGruposPlanosFinanceiros = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordItensGruposPlanos: RecItensGruposPlanosFinanceiros;
  end;

function BuscarItensGruposPlanosFinanceiros(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecItensGruposPlanosFinanceiros>;

function ExcluirItensGrupoPlanoFinanceiro(
  pConexao: TConexao;
  pGrupoFinanceiroId: string
  ): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TPlanosFinanceiros }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where  IG.GRUPO_FINANCEIRO_ID = :P1 ' );
end;

constructor TItensGruposPlanosFinanceiros.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ITENS_GRUPOS_FINANCEIROS');

  FSql :=
    'select ' +
    '  IG.GRUPO_FINANCEIRO_ID, ' +
    '  IG.PLANO_FINANCEIRO_ID, ' +
    '  IG.SQL, ' +
    '  PF.DESCRICAO, ' +
    '  IG.ORDEM ' +
    'from ' +
    '  ITENS_GRUPOS_FINANCEIROS IG ' +

    'LEFT JOIN PLANOS_FINANCEIROS PF '+
    ' ON IG.PLANO_FINANCEIRO_ID = PF.PLANO_FINANCEIRO_ID';

  SetFiltros(GetFiltros);

  AddColuna('GRUPO_FINANCEIRO_ID', True);
  AddColuna('PLANO_FINANCEIRO_ID');
  AddColuna('SQL');
  AddColunaSL('DESCRICAO');
  AddColuna('ORDEM');

end;

function TItensGruposPlanosFinanceiros.GetRecordItensGruposPlanos: RecItensGruposPlanosFinanceiros;
begin
  Result.GrupoFinanceiroId    := GetString('GRUPO_FINANCEIRO_ID', True);
  Result.PlanoFinanceiroId    := GetString('PLANO_FINANCEIRO_ID');
  Result.Descricao            := GetString('DESCRICAO');
  Result.SQL                  := GetString('SQL');
  Result.Ordem                := getInt('ORDEM');
end;

function BuscarItensGruposPlanosFinanceiros(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecItensGruposPlanosFinanceiros>;
var
  i: Integer;
  vSqlAuxiliar: string;
  t: TItensGruposPlanosFinanceiros;
begin
  Result := nil;
  t := TItensGruposPlanosFinanceiros.Create(pConexao);

  vSqlAuxiliar := '';

  if t.Pesquisar(pIndice, pFiltros, vSqlAuxiliar) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordItensGruposPlanos;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirItensGrupoPlanoFinanceiro(
  pConexao: TConexao;
  pGrupoFinanceiroId: string
): RecRetornoBD;
var
  t: TItensGruposPlanosFinanceiros;
begin
  pConexao.SetRotina('EXCLUIR_ITENS_GRUPO_FIN');
  Result.TeveErro := False;
  t := TItensGruposPlanosFinanceiros.Create(pConexao);

  t.SetString('grupo_financeiro_id', pGrupoFinanceiroId, True);

  try
    pConexao.IniciarTransacao;

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
