unit _ComprasItensBaixas;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, _RecordsOrcamentosVendas;

{$M+}
type
  RecComprasItensBaixas = record
    BaixaId: Integer;
    ItemId: Integer;
    ProdutoId: Integer;
    NomeProduto: string;
    Quantidade: Double;
  end;

  TComprasItensBaixas = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordComprasItens: RecComprasItensBaixas;
  end;

function BuscarComprasItensBaixas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecComprasItensBaixas>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TComprasItensBaixas }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ITE.BAIXA_ID = :P1 '
    );
end;

constructor TComprasItensBaixas.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'COMPRAS_ITENS_BAIXAS');

  FSql :=
    'select ' +
    '  ITB.BAIXA_ID, ' +
    '  ITB.ITEM_ID, ' +
    '  TCO.NOME as NOME_COBRANCA, ' +
    '  ITE.VALOR ' +
    'from ' +
    '  COMPRAS_ITENS_BAIXAS ITB ' +

    'inner join TIPOS_COBRANCA TCO ' +
    'on ITE.COBRANCA_ID = TCO.COBRANCA_ID ';

  setFiltros(getFiltros);

  AddColuna('BAIXA_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('QUANTIDADE');
  AddColunaSL('NOME_PRODUTO');
end;

function TComprasItensBaixas.getRecordComprasItens: RecComprasItensBaixas;
begin
  Result.BaixaId     := getInt('BAIXA_ID', True);
  Result.ItemId      := getInt('ITEM_ID', True);
  Result.Quantidade  := getDouble('QUANTIDADE');
end;

function BuscarComprasItensBaixas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecComprasItensBaixas>;
var
  i: Integer;
  t: TComprasItensBaixas;
begin
  Result := nil;
  t := TComprasItensBaixas.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordComprasItens;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
