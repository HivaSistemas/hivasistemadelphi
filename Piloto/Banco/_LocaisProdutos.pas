unit _LocaisProdutos;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TLocaisProduto = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordLocaisProduto: RecLocaisProdutos;
  end;

function AtualizarLocaisProduto(
  pConexao: TConexao;
  pLocalId: Integer;
  pNome: string;
  pTipoLocal: string;
  pPermiteDevolucaoVenda: string;
  pAtivo: string;
  pEmpresaDirecionarNota: Integer
): RecRetornoBD;

function BuscarLocaisProdutos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecLocaisProdutos>;

function ExcluirLocaisProduto(
  pConexao: TConexao;
  pLocalId: Integer
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TLocaisProduto }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where LOCAL_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome do local',
      True,
      0,
      'where NOME like :P1 || ''%'' '
    );
end;

constructor TLocaisProduto.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'LOCAIS_PRODUTOS');

  FSql :=
    'select ' +
    '  LOCAL_ID, ' +
    '  NOME, ' +
    '  TIPO_LOCAL, ' +
    '  PERMITE_DEVOLUCAO_VENDA, ' +
    '  ATIVO, ' +
    '  EMPRESA_DIRECIONAR_NOTA ' +
    'from ' +
    '  LOCAIS_PRODUTOS';

  SetFiltros(GetFiltros);

  AddColuna('LOCAL_ID', True);
  AddColuna('NOME');
  AddColuna('TIPO_LOCAL');
  AddColuna('PERMITE_DEVOLUCAO_VENDA');
  AddColuna('ATIVO');
  AddColuna('EMPRESA_DIRECIONAR_NOTA');
end;

function TLocaisProduto.GetRecordLocaisProduto: RecLocaisProdutos;
begin
  Result := RecLocaisProdutos.Create;
  Result.local_id                := getInt('LOCAL_ID', True);
  Result.nome                    := getString('NOME');
  Result.tipo_local              := getString('TIPO_LOCAL');
  Result.tipo_local_analitico    := _Biblioteca.Decode(Result.tipo_local, ['L', 'Loja', 'D', 'Dep�sito', 'E', 'Exposi��o']);
  Result.permite_devolucao_venda := getString('PERMITE_DEVOLUCAO_VENDA');
  Result.ativo                   := getString('ATIVO');
  Result.empresa_direcionar_nota := getInt('EMPRESA_DIRECIONAR_NOTA');
end;

function AtualizarLocaisProduto(
  pConexao: TConexao;
  pLocalId: Integer;
  pNome: string;
  pTipoLocal: string;
  pPermiteDevolucaoVenda: string;
  pAtivo: string;
  pEmpresaDirecionarNota: Integer
): RecRetornoBD;
var
  t: TLocaisProduto;
  novo: Boolean;
  seq: TSequencia;
begin
  Result.TeveErro := False;
  t := TLocaisProduto.Create(pConexao);

  novo := pLocalId = 0;

  if novo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_LOCAL_ID');
    pLocalId := seq.GetProximaSequencia;
    result.AsInt := pLocalId;
    seq.Free;
  end;

  t.SetInt('LOCAL_ID', pLocalId, True);
  t.SetString('NOME', pNome);
  t.SetString('TIPO_LOCAL', pTipoLocal);
  t.SetString('PERMITE_DEVOLUCAO_VENDA', pPermiteDevolucaoVenda);
  t.SetString('ATIVO', pAtivo);
  t.setInt('EMPRESA_DIRECIONAR_NOTA', pEmpresaDirecionarNota);

  try
    if novo then
      t.Inserir
    else
      t.Atualizar;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function BuscarLocaisProdutos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecLocaisProdutos>;
var
  i: Integer;
  t: TLocaisProduto;
begin
  Result := nil;
  t := TLocaisProduto.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordLocaisProduto;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirLocaisProduto(
  pConexao: TConexao;
  pLocalId: Integer
): RecRetornoBD;
var
  t: TLocaisProduto;
begin
  Result.TeveErro := False;
  t := TLocaisProduto.Create(pConexao);

  t.SetInt('LOCAL_ID', pLocalId, True);

  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
