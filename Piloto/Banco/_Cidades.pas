unit _Cidades;

interface

uses
  _OperacoesBancoDados, _RecordsEspeciais, _Conexao, System.SysUtils, _RecordsCadastros, System.Variants;

{$M+}
type
  TCidade = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordCidade: RecCidades;
  end;

function AtualizarCidade(
  pConexao: TConexao;
  pCidadeId: Integer;
  pNome: string;
  pCodigoIBGE: Cardinal;
  pEstadoId: string;
  pAtivo: string;
  pTipoBloqueioVendaEntregar: string;
  pValorCalculoFretePorKM: Cardinal
): RecRetornoBD;

function BuscarCidades(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant;
  pFiltrosAuxiliares: string = ''
): TArray<RecCidades>;

function ExcluirCidade(
  pConexao: TConexao;
  pCidadeId: Integer
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TCidade }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 3);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CIDADE_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Sigla do estado',
      False,
      0,
      'where ESTADO_ID like :P1 || ''%'' '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome da cidade',
      True,
      1,
      'where NOME like :P1 || ''%'' '
    );
end;

constructor TCidade.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CIDADES');

  FSql :=
    'select ' +
    '  CIDADE_ID, ' +
    '  NOME, ' +
    '  CODIGO_IBGE, ' +
    '  ESTADO_ID, ' +
    '  TIPO_BLOQUEIO_VENDA_ENTREGAR, ' +
    '  VALOR_CALCULO_FRETE_KM, ' +
    '  ATIVO ' +
    'from ' +
    '  CIDADES ';

  SetFiltros(GetFiltros);

  AddColuna('CIDADE_ID', True);
  AddColuna('NOME');
  AddColuna('CODIGO_IBGE');
  AddColuna('ESTADO_ID');
  AddColuna('TIPO_BLOQUEIO_VENDA_ENTREGAR');
  AddColuna('VALOR_CALCULO_FRETE_KM');
  AddColuna('ATIVO');
end;

function TCidade.GetRecordCidade: RecCidades;
begin
  Result := RecCidades.Create;

  Result.cidade_id                 := GetInt('CIDADE_ID', True);
  Result.nome                      := GetString('NOME');
  Result.codigo_ibge               := GetInt('CODIGO_IBGE');
  Result.estado_id                 := GetString('ESTADO_ID');
  Result.TipoBloqueioVendaEntregar := GetString('TIPO_BLOQUEIO_VENDA_ENTREGAR');
  Result.valor_calculo_frete_km    := GetInt('VALOR_CALCULO_FRETE_KM');
  Result.ativo                     := GetString('ATIVO');
end;

function AtualizarCidade(
  pConexao: TConexao;
  pCidadeId: Integer;
  pNome: string;
  pCodigoIBGE: Cardinal;
  pEstadoId: string;
  pAtivo: string;
  pTipoBloqueioVendaEntregar: string;
  pValorCalculoFretePorKM: Cardinal
): RecRetornoBD;
var
  t: TCidade;
  novo: Boolean;
  seq: TSequencia;
begin
  Result.Iniciar;
  t := TCidade.Create(pConexao);

  novo := pCidadeId = 0;
  if novo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_CIDADE_ID');
    pCidadeId := seq.GetProximaSequencia;
    result.AsInt := pCidadeId;
    seq.Free;
  end;

  t.SetInt('CIDADE_ID', pCidadeId, True);
  t.SetString('NOME', pNome);
  t.setIntN('CODIGO_IBGE', pCodigoIBGE);
  t.SetString('ESTADO_ID', pEstadoId);
  t.SetString('ATIVO', pAtivo);
  t.SetString('TIPO_BLOQUEIO_VENDA_ENTREGAR', pTipoBloqueioVendaEntregar);
  t.setIntN('VALOR_CALCULO_FRETE_KM', pValorCalculoFretePorKM);

  try
    pConexao.IniciarTransacao;

    if novo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarCidades(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant;
  pFiltrosAuxiliares: string
): TArray<RecCidades>;
var
  i: Integer;
  t: TCidade;
begin
  Result := nil;
  t := TCidade.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros, pFiltrosAuxiliares) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordCidade;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirCidade(
  pConexao: TConexao;
  pCidadeId: Integer
): RecRetornoBD;
var
  t: TCidade;
begin
  result.TeveErro := False;
  t := TCidade.Create(pConexao);
  try
    t.SetInt('CIDADE_ID', pCidadeId, True);
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
