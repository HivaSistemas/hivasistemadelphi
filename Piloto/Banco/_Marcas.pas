unit _Marcas;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TMarca = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordMarca: RecMarcas;
  end;

function AtualizarMarca(
  pConexao: TConexao;
  pMarcaId: Integer;
  pNome: string;
  pAtivo: string
): RecRetornoBD;

function BuscarMarcas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecMarcas>;

function ExcluirMarca(
  pConexao: TConexao;
  pMarcaId: Integer
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TMarca }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where MARCA_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome da marca',
      True,
      0,
      'where NOME like :P1 || ''%'' '
    );
end;

constructor TMarca.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'MARCAS');

  FSql :=
    'select ' +
    '  MARCA_ID, ' +
    '  NOME, ' +
    '  ATIVO ' +
    'from ' +
    '  MARCAS';

  SetFiltros(GetFiltros);

  AddColuna('MARCA_ID', True);
  AddColuna('NOME');
  AddColuna('ATIVO');
end;

function TMarca.GetRecordMarca: RecMarcas;
begin
  Result := RecMarcas.Create;
  Result.marca_id := getInt('MARCA_ID', True);
  Result.nome     := getString('NOME');
  Result.ativo    := getString('ATIVO');
end;

function AtualizarMarca(
  pConexao: TConexao;
  pMarcaId: Integer;
  pNome: string;
  pAtivo: string
): RecRetornoBD;
var
  t: TMarca;
  novo: Boolean;
  seq: TSequencia;
begin
  Result.TeveErro := False;
  t := TMarca.Create(pConexao);

  novo := pMarcaId = 0;

  if novo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_MARCA_ID');
    pMarcaId := seq.GetProximaSequencia;
    result.AsInt := pMarcaId;
    seq.Free;
  end;

  t.SetInt('MARCA_ID', pMarcaId, True);
  t.SetString('NOME', pNome);
  t.SetString('ATIVO', pAtivo);

  try
    if novo then
      t.Inserir
    else
      t.Atualizar;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function BuscarMarcas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecMarcas>;
var
  i: Integer;
  t: TMarca;
begin
  Result := nil;
  t := TMarca.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordMarca;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirMarca(
  pConexao: TConexao;
  pMarcaId: Integer
): RecRetornoBD;
var
  t: TMarca;
begin
  Result.TeveErro := False;
  t := TMarca.Create(pConexao);

  t.SetInt('MARCA_ID', pMarcaId, True);

  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
