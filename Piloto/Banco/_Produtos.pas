unit _Produtos;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros,
  _ProdutosRelacionados, _Estoques, _PrecosProdutos, _EstoquesBkp2017, _ProdutosCodigosBarrasAux,
  System.Classes, System.Variants, Data.DB, _CustosProdutos, _ProdutosMultiplosCompras, _ProdutosKit,
  _ProdutosFornCodOriginais, _ProdutosGruposTribEmpresas;

{$M+}
type
  RecProdutoCustosFixo = record
    ProdutoId: Integer;
    EmpresaId: Integer;
    EmpresaNome: string;
    CustoFixo: Double;
    ZerarCusto: string;
  end;

  RecProdutoCodigoBarras = record
    ProdutoId: Integer;
    CodigoBarras: string;
    CodigoOriginal: string;
    FornecedorId: Integer;
    NomeFornecedor: string;
  end;

  RecProdutosPaiFilho = record
    ProdutoId: Integer;
    Nome: string;
    QtdeVezesPai: Double;
    ProdutoPaiId: Integer;
    NomeProdutoPai: string;
  end;

   RecProdutosSemVender = record
    produtoId: Integer;
    nomeProduto: string;
    empresaId: Integer;
    nomeEmpresa: string;
    marcaId: Integer;
    nomeMarca: string;
    unidade: string;
    qtdeDiasSemVender: Integer;
    qtdeEstoque: Double;
    custoUnitario: Double;
    custoTotal: Double;
    ativo: string;
  end;

  TProduto = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordProduto: RecProdutos;
  end;

function AtualizarProduto(
  pConexao: TConexao;
  pProdutoId: Integer;
  pNome: string;
  pEmpresasEstoque: TArray<Integer>;
  pCodigoNCM: string;
  pMarcaId: Integer;
  pFornecedorId: Integer;
  pMultiploVenda: Double;
  pPeso: Double;
  pVolume: Double;
  pUnidadeVenda: string;
  pBloqueadoCompras: string;
  pBloqueadoVendas: string;
  pPermitirDevolucao: string;
  pCaracteristicas: string;
  pNomeCompra: string;
  pAceitarEstoqueNegativo: string;
  pCodigoBarras: string;
  pCodigoBalanca: string;
  pLinhaProdutoId: string;
  pInativarZerarEstoque: string;
  pCest: string;
  pFoto1: TMemoryStream;
  pFoto2: TMemoryStream;
  pFoto3: TMemoryStream;
  pMultiplosCompra: TArray<RecProdutoMultiploCompra>;
  pAtivo: string;
  pProdutoDiversosPDV: string;
  pTipoControleEstoque: string;
  pExigirDataFabricacaoLote: string;
  pExigirDataValidadeLote: string;
  pProdutosKit: TArray<RecProdutosKit>;
  pSobEncomenda: string;
  pRevenda: string;
  pUsoConsumo: string;
  pAtivoImobilizado: string;
  pServico: string;
  pSujeitoComissao: string;
  pBloquearEntradSemPedCompra: string;
  pExigirModeloNotaFiscal: string;
  pExigirSeparacao: string;
  pDiasAvisoVencimento: Integer;
  pMotivoInatividade: string;
  pValorAdicionalFrete: Double;
  pPercentualComissaoVista: Double;
  pPercentualComissaoPrazo: Double;
  pCodigoOriginalFabricante: string;
  pUnidadeEntregaId: string;
  pSepararSomenteCodigoBarras: string;
  pCodigosOriginais: TArray<RecProdutosFornCodOriginais>;
  pTipoDefAutomaticaLote: string;
  pGruposTributacoes: TArray<RecProdutosGruposTribEmpresas>;
  pCodigoBarrasAuxiliares: TArray<RecProdutosCodigosBarrasAux>;
  pControlaPeso: string;
  pMultiploVenda_2: Double;
  pMultiploVenda_3: Double;
  pEmitirSomenteNfe: string;
  pEnderecosEstoque: TArray<Integer>;
  pCustoFixo: TArray<RecProdutoCustosFixo>;
  pExportarWeb: string
): RecRetornoBD;

function BuscarProdutos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean;
  pFiltroExtra: string
): TArray<RecProdutos>;

function BuscarProdutosComando(pConexao: TConexao; pComando: string): TArray<RecProdutos>;

function ExcluirProduto(
  pConexao: TConexao;
  pProdutoId: Integer
): RecRetornoBD;

function BuscarProdutosSemVender(
  pConexao: TConexao;
  qtdeDias: Integer;
  EmpresaId: Integer;
  somenteProdutosSemVender: Boolean;
  situacao: string;
  sqlTributacao: string;
  sqlFiltro: string
): TArray<RecProdutosSemVender>;
function BuscarProdutoEntradaNota(pConexao: TConexao; const pProdutoID: Integer): RecProdutoEntrada; overload;
function BuscarProdutosPromocao(pConexao: TConexao; pSQL: string): TArray<Integer>;
function BuscarProdutoEntradaNota(pConexao: TConexao; const pNomeProduto: string; pCodigoBarras: string): RecProdutoEntrada; overload;
procedure BuscarImpostosProduto(pConexao: TConexao; var pProdutos: TArray<RecProdutoImpostosCalculados>; pEmpresaId: Integer; pClienteId: Integer); overload;

procedure BuscarImpostosProduto(
  pConexao: TConexao;
  var pProdutos: TArray<RecProdutoImpostosCalculados>;
  pEmpresaId: Integer;
  pCST: string;
  pEstadoDestinoId: string
); overload;

function BuscarCSTsPisCofins(pConexao: TConexao): TArray<RecCstPisCofins>;
function PodeVincularProdutoPaiFilho(pConexao: TConexao; pProdutoId: Integer): RecRetornoBD;
function AtualizarProdutoPaiFilho(pConexao: TConexao; pProdutoId: Integer; pProdutoPaiId: Integer; pQuantidadeVezesPai: Double): RecRetornoBD;
function BuscarProdutosPaiFilho(pConexao: TConexao; pFiltros: string): TArray<RecProdutosPaiFilho>;
function BuscarUnidadeVenda(pConexao: TConexao; pProdutoId: Integer): string;
function AtualizarProdutoXml(pConexao: TConexao; pProdutoId: Integer; pCampo: string; pValor: Variant): RecRetornoBD;
function AtualizarCampoProduto(pConexao: TConexao; pValor: string; pProdutosIds: TArray<Integer>; pCampo: string; pMotivo: string = ''): RecRetornoBD;
function AtualizarEnderecosEstoqueProdutos(pConexao: TConexao; enderecos: TArray<Integer>; pProdutosIds: TArray<Integer>): RecRetornoBD;
function AtualizarZerarCustoProdutos(pConexao: TConexao; pProdutosIds: TArray<Integer>; pValor: string): RecRetornoBD;
function AtualizarCustoVendaProdutos(pConexao: TConexao; pProdutosIds: TArray<Integer>; pValor: Double): RecRetornoBD;
function buscarProdutosCodigoBarras(pConexao: TConexao; pCodigoBarras: TArray<string>): TArray<RecProdutoCodigoBarras>;
function buscarCustoFixoProdutosPorEmpresa(pConexao: TConexao; pProdutoId: Integer): TArray<RecProdutoCustosFixo>;
function ExisteProdutoNomeMarcaCadastrado(pConexao: TConexao; pProdutoId: Integer; pNome: string; pMarcaId: Integer): Boolean;
procedure BuscarPrecoVendaCustoCompra(pConexao: TConexao; pEmpresaId: Integer; pProdutoId: Integer; var precoVenda: Double; var custoCompra: Double);
function BuscarEnderecosEstoque(pConexao: TConexao; ProdutoId: Integer): TArray<Integer>;
function BuscarEnderecosEstoqueCompleto(pConexao: TConexao; ProdutoId: Integer): TArray<RecEnderecosEstoque>;
function AtualizarComissaoAVista(pConexao: TConexao; pProdutosIds: TArray<Integer>; pValor: Double): RecRetornoBD;
function AtualizarComissaoAPrazo(pConexao: TConexao; pProdutosIds: TArray<Integer>; pValor: Double): RecRetornoBD;
function AtualizarTributacaoProdutos(pConexao: TConexao; pProdutosIds: TArray<Integer>; tributacoes: TArray<RecProdutosGruposTribEmpresas>): RecRetornoBD;
function AtualizarCEST(pConexao: TConexao; pProdutosIds: TArray<Integer>; pValor: string): RecRetornoBD;
function AtualizarNCM(pConexao: TConexao; pProdutosIds: TArray<Integer>; pValor: string): RecRetornoBD;

function BuscarProdutosRelacaoProdutos(
  pConexao: TConexao;
  pComandoSQL: string
): TArray<RecProdutos>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TProduto }

uses _EnderecoEstoque;

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 9);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'C�digo do sistema',

      False,
      0,
      'where PRO.PRODUTO_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome do produto (Avan�ado)',
      False,
      1,
      'where PRO.NOME like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  PRO.NOME',
      '',
      True
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome de compra do produto (Avan�ado)',
      False,
      2,
      'where PRO.NOME_COMPRA like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  PRO.NOME_COMPRA ',
      '',
      True
    );

  Result[3] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'C�digo de barras',
      False,
      3,
      'where (PRO.CODIGO_BARRAS = :P1 or PRO.PRODUTO_ID IN (select PRODUTO_ID from PRODUTOS_CODIGOS_BARRAS_AUX where CODIGO_BARRAS = :P1))',
      'order by PRO.NOME_COMPRA '
    );

  Result[4] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome/Marca (Avan�ado)',
      True,
      4,
      'where PRO.NOME like ''%'' || :P1 || ''%'' ' +
      'and MAR.NOME like ''%'' || :P2 || ''%'' ',
      'order by PRO.NOME ',
      '',
      True
    );

  Result[5] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome/Marca',
      False,
      5,
      'where PRO.NOME like :P1 || ''%'' ' +
      'and MAR.NOME like :P2 || ''%'' ',
      'order by PRO.NOME ',
      '',
      False
    );

  Result[6] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'C�digo Sistema Anterior',
      False,
      6,
      'where PRO.CHAVE_IMPORTACAO = :P1 ',
      'order by PRO.NOME ',
      '',
      False
    );

  Result[7] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'C�digo Refer�ncia Ind�stria',
      False,
      7,
      'and PRO.CODIGO_ORIGINAL_FABRICANTE like ''%'' || :P2 || ''%'' ',
      'order by ' +
      '  PRO.NOME '
    );

  Result[8] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome/C�digo Refer�ncia Ind�stria',
      False,
      8,
      'and PRO.NOME like :P2 || ''%'' ' +
      'and PRO.CODIGO_ORIGINAL_FABRICANTE like ''%'' || :P3 || ''%'' ',
      'order by ' +
      '  PRO.NOME ',
      ''
    );
end;

constructor TProduto.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PRODUTOS');

  FSql :=
    'select ' +
    '  PRO.PRODUTO_ID, ' +
    '  PRO.DATA_CADASTRO, ' +
    '  PRO.CODIGO_NCM, ' +
    '  PRO.MARCA_ID, ' +
    '  PRO.FORNECEDOR_ID, ' +
    '  PRO.MULTIPLO_VENDA, ' +
    '  0 as MULTIPLO_VENDA_2, ' +
    '  0 as MULTIPLO_VENDA_3, ' +
    '  PRO.PESO, ' +
    '  PRO.VOLUME, ' +
    '  PRO.UNIDADE_VENDA, ' +
    '  PRO.BLOQUEADO_COMPRAS, ' +
    '  PRO.BLOQUEADO_VENDAS, ' +
    '  PRO.PERMITIR_DEVOLUCAO, ' +
    '  PRO.CARACTERISTICAS, ' +
    '  PRO.ATIVO, ' +
    '  PRO.PRODUTO_DIVERSOS_PDV, ' +
    '  PRO.NOME_COMPRA, ' +
    '  PRO.CEST, ' +
    '  PRO.ACEITAR_ESTOQUE_NEGATIVO, ' +
    '  PRO.CODIGO_BARRAS, ' +
    '  PRO.CODIGO_BALANCA, ' +
    '  PRO.LINHA_PRODUTO_ID, ' +
    '  PRO.NOME, ' +
    '  PRO.INATIVAR_ZERAR_ESTOQUE, ' +
    '  PRO.PRODUTO_PAI_ID, ' +
    '  PRO.QUANTIDADE_VEZES_PAI, ' +
    '  PRO.TIPO_CONTROLE_ESTOQUE, ' +
    '  MAR.NOME as NOME_MARCA, ' +
    '  CAD.NOME_FANTASIA as NOME_FABRICANTE, ' +
    '  PRO.EXIGIR_DATA_FABRICACAO_LOTE, ' +
    '  PRO.EXIGIR_DATA_VENCIMENTO_LOTE, ' +
    '  PRO.SOB_ENCOMENDA, ' +
    '  PRO.CONTROLA_PESO, '+
    '  PRO.DIAS_AVISO_VENCIMENTO, ' +
    '  PRO.MOTIVO_INATIVIDADE, ' +
    '  PRO.REVENDA, ' +
    '  PRO.CODIGO_ORIGINAL_FABRICANTE, ' +
    '  PRO.USO_CONSUMO, ' +
    '  PRO.ATIVO_IMOBILIZADO, ' +
    '  PRO.SERVICO, ' +
    '  PRO.SUJEITO_COMISSAO, ' +
    '  PRO.BLOQUEAR_ENTRAD_SEM_PED_COMPRA, ' +
    '  PRO.EXIGIR_MODELO_NOTA_FISCAL, ' +
    '  PRO.EXIGIR_SEPARACAO, ' +
    '  PRO.VALOR_ADICIONAL_FRETE, ' +
    '  PRO.PERCENTUAL_COMISSAO_VISTA, ' +
    '  PRO.PERCENTUAL_COMISSAO_PRAZO, ' +
    '  PRO.TIPO_DEF_AUTOMATICA_LOTE, ' +
    '  PRO.UNIDADE_ENTREGA_ID, ' +
    '  PRO.EMITIR_SOMENTE_NFE, ' +
    '  PRO.EXPORTAR_WEB, '+
    '  PRO.CONF_SOMENTE_CODIGO_BARRAS, ' +
    '  PRO.CHAVE_IMPORTACAO ' +
    'from ' +
    '  PRODUTOS PRO ' +

    'inner join MARCAS MAR ' +
    'on PRO.MARCA_ID = MAR.MARCA_ID ' +

    'inner join CADASTROS CAD ' +
    'on PRO.FORNECEDOR_ID = CAD.CADASTRO_ID ';

  setFiltros(getFiltros);

  AddColuna('PRODUTO_ID', True);
  AddColunaSL('DATA_CADASTRO');
  AddColuna('CODIGO_NCM');
  AddColuna('MARCA_ID');
  AddColuna('FORNECEDOR_ID');
  AddColuna('MULTIPLO_VENDA');
  AddColuna('MULTIPLO_VENDA_2');
  AddColuna('MULTIPLO_VENDA_3');
  AddColuna('PESO');
  AddColuna('VOLUME');
  AddColuna('UNIDADE_VENDA');
  AddColuna('BLOQUEADO_COMPRAS');
  AddColuna('BLOQUEADO_VENDAS');
  AddColuna('PERMITIR_DEVOLUCAO');
  AddColuna('CARACTERISTICAS');
  AddColuna('ATIVO');
  AddColuna('PRODUTO_DIVERSOS_PDV');
  AddColuna('NOME_COMPRA');
  AddColuna('CEST');
  AddColuna('ACEITAR_ESTOQUE_NEGATIVO');
  AddColuna('CODIGO_BARRAS');
  AddColuna('CODIGO_BALANCA');
  AddColuna('LINHA_PRODUTO_ID');
  AddColuna('NOME');
  AddColuna('INATIVAR_ZERAR_ESTOQUE');
  AddColunaSL('PRODUTO_PAI_ID');
  AddColunaSL('QUANTIDADE_VEZES_PAI');
  AddColunaSL('NOME_MARCA');
  AddColunaSL('NOME_FABRICANTE');
  AddColuna('TIPO_CONTROLE_ESTOQUE');
  AddColuna('EXIGIR_DATA_FABRICACAO_LOTE');
  AddColuna('EXIGIR_DATA_VENCIMENTO_LOTE');
  AddColuna('SOB_ENCOMENDA');
  AddColuna('CONTROLA_PESO');
  AddColuna('DIAS_AVISO_VENCIMENTO');
  AddColuna('MOTIVO_INATIVIDADE');
  AddColuna('REVENDA');
  AddColuna('CODIGO_ORIGINAL_FABRICANTE');
  AddColuna('USO_CONSUMO');
  AddColuna('ATIVO_IMOBILIZADO');
  AddColuna('SERVICO');
  AddColuna('SUJEITO_COMISSAO');
  AddColuna('BLOQUEAR_ENTRAD_SEM_PED_COMPRA');
  AddColuna('EXIGIR_MODELO_NOTA_FISCAL');
  AddColuna('EXIGIR_SEPARACAO');
  AddColuna('VALOR_ADICIONAL_FRETE');
  AddColuna('PERCENTUAL_COMISSAO_VISTA');
  AddColuna('PERCENTUAL_COMISSAO_PRAZO');
  AddColuna('TIPO_DEF_AUTOMATICA_LOTE');
  AddColuna('UNIDADE_ENTREGA_ID');
  AddColuna('CONF_SOMENTE_CODIGO_BARRAS');
  AddColuna('EMITIR_SOMENTE_NFE');
  AddColuna('EXPORTAR_WEB');
  AddColunaSL('CHAVE_IMPORTACAO');

end;

function TProduto.getRecordProduto: RecProdutos;
begin
  Result := RecProdutos.Create;

  Result.produto_id               := getInt('PRODUTO_ID', True);
  Result.data_cadastro            := getData('DATA_CADASTRO');
  Result.codigo_ncm               := getString('CODIGO_NCM');
  Result.marca_id                 := getInt('MARCA_ID');
  Result.fornecedor_id            := getInt('FORNECEDOR_ID');
  Result.multiplo_venda           := getDouble('MULTIPLO_VENDA');
  Result.multiplo_venda_2         := getDouble('MULTIPLO_VENDA_2');
  Result.multiplo_venda_3         := getDouble('MULTIPLO_VENDA_3');
  Result.peso                     := getDouble('PESO');
  Result.volume                   := getDouble('VOLUME');
  Result.unidade_venda            := getString('UNIDADE_VENDA');
  Result.bloqueado_compras        := getString('BLOQUEADO_COMPRAS');
  Result.bloqueado_vendas         := getString('BLOQUEADO_VENDAS');
  Result.permitir_devolucao       := getString('PERMITIR_DEVOLUCAO');
  Result.caracteristicas          := getString('CARACTERISTICAS');
  Result.ativo                    := getString('ATIVO');
  Result.produtoDiversosPDV       := getString('PRODUTO_DIVERSOS_PDV');
  Result.nome_compra              := getString('NOME_COMPRA');
  Result.cest                     := getString('CEST');
  Result.aceitar_estoque_negativo := getString('ACEITAR_ESTOQUE_NEGATIVO');
  Result.codigo_barras            := getString('CODIGO_BARRAS');
  Result.codigoBalanca            := getString('CODIGO_BALANCA');
  Result.LinhaProdutoId           := getString('LINHA_PRODUTO_ID');
  Result.nome                     := getString('NOME');
  Result.inativar_zerar_estoque   := getString('INATIVAR_ZERAR_ESTOQUE');
  Result.nome_fabricante          := GetString('NOME_FABRICANTE');
  Result.nome_marca               := GetString('NOME_MARCA');
  Result.ProdutoPaiId             := getInt('PRODUTO_PAI_ID');
  Result.QuantidadeVezesPai       := getDouble('QUANTIDADE_VEZES_PAI');
  Result.TipoControleEstoque      := GetString('TIPO_CONTROLE_ESTOQUE');
  Result.ExigirDataFabricacaoLote := GetString('EXIGIR_DATA_FABRICACAO_LOTE');
  Result.ExigirDataVencimentoLote := GetString('EXIGIR_DATA_VENCIMENTO_LOTE');
  Result.SobEncomenda             := GetString('SOB_ENCOMENDA');
  Result.ControlaPeso             := GetString('CONTROLA_PESO');
  Result.DiasAvisoVencimento      := GetInt('DIAS_AVISO_VENCIMENTO');
  Result.MotivoInatividade        := getString('MOTIVO_INATIVIDADE');
  Result.CodigoOriginalFabricante := GetString('CODIGO_ORIGINAL_FABRICANTE');
  Result.Revenda                  := GetString('REVENDA');
  Result.UsoConsumo               := GetString('USO_CONSUMO');
  Result.AtivoImobilizado         := GetString('ATIVO_IMOBILIZADO');
  Result.SujeitoComissao          := GetString('SUJEITO_COMISSAO');
  Result.BloquearEntradSemPedCompra := GetString('BLOQUEAR_ENTRAD_SEM_PED_COMPRA');
  Result.NaoExigirModeloNotaFiscal  := GetString('EXIGIR_MODELO_NOTA_FISCAL');
  Result.ExigirSeparacao            := GetString('EXIGIR_SEPARACAO');
  Result.ValorAdicionalFrete        := GetDouble('VALOR_ADICIONAL_FRETE');
  Result.PercentualComissaoVista    := GetDouble('PERCENTUAL_COMISSAO_VISTA');
  Result.PercentualComissaoPrazo    := GetDouble('PERCENTUAL_COMISSAO_PRAZO');
  Result.TipoDefAutomaticaLote      := GetString('TIPO_DEF_AUTOMATICA_LOTE');
  Result.UnidadeEntregaId           := GetString('UNIDADE_ENTREGA_ID');
  Result.ConfSomenteCodigoBarras    := GetString('CONF_SOMENTE_CODIGO_BARRAS');
  Result.emitir_somente_nfe         := getString('EMITIR_SOMENTE_NFE');
  Result.exportar_web               := getString('EXPORTAR_WEB');
  Result.ChaveImportacao            := getString('CHAVE_IMPORTACAO');
end;

function AtualizarProduto(
  pConexao: TConexao;
  pProdutoId: Integer;
  pNome: string;
  pEmpresasEstoque: TArray<Integer>;
  pCodigoNCM: string;
  pMarcaId: Integer;
  pFornecedorId: Integer;
  pMultiploVenda: Double;
  pPeso: Double;
  pVolume: Double;
  pUnidadeVenda: string;
  pBloqueadoCompras: string;
  pBloqueadoVendas: string;
  pPermitirDevolucao: string;
  pCaracteristicas: string;
  pNomeCompra: string;
  pAceitarEstoqueNegativo: string;
  pCodigoBarras: string;
  pCodigoBalanca: string;
  pLinhaProdutoId: string;
  pInativarZerarEstoque: string;
  pCest: string;
  pFoto1: TMemoryStream;
  pFoto2: TMemoryStream;
  pFoto3: TMemoryStream;
  pMultiplosCompra: TArray<RecProdutoMultiploCompra>;
  pAtivo: string;
  pProdutoDiversosPDV: string;
  pTipoControleEstoque: string;
  pExigirDataFabricacaoLote: string;
  pExigirDataValidadeLote: string;
  pProdutosKit: TArray<RecProdutosKit>;
  pSobEncomenda: string;
  pRevenda: string;
  pUsoConsumo: string;
  pAtivoImobilizado: string;
  pServico: string;
  pSujeitoComissao: string;
  pBloquearEntradSemPedCompra: string;
  pExigirModeloNotaFiscal: string;
  pExigirSeparacao: string;
  pDiasAvisoVencimento: Integer;
  pMotivoInatividade: string;
  pValorAdicionalFrete: Double;
  pPercentualComissaoVista: Double;
  pPercentualComissaoPrazo: Double;
  pCodigoOriginalFabricante: string;
  pUnidadeEntregaId: string;
  pSepararSomenteCodigoBarras: string;
  pCodigosOriginais: TArray<RecProdutosFornCodOriginais>;
  pTipoDefAutomaticaLote: string;
  pGruposTributacoes: TArray<RecProdutosGruposTribEmpresas>;
  pCodigoBarrasAuxiliares: TArray<RecProdutosCodigosBarrasAux>;
  pControlaPeso: string;
  pMultiploVenda_2: Double;
  pMultiploVenda_3: Double;
  pEmitirSomenteNfe: string;
  pEnderecosEstoque: TArray<Integer>;
  pCustoFixo: TArray<RecProdutoCustosFixo>;
  pExportarWeb: string
): RecRetornoBD;
var
  i: Integer;
  j: Integer;
  vNovo: Boolean;

  vProduto: TProduto;
  vExec: TExecucao;
  vSeq: TSequencia;
  vEstoque: TEstoque;

  vGruposTrib: TProdutosGruposTribEmpresas;
  vProdMultiplos: TProdutoMultiploCompra;
  vProdutosKit: TProdutosKit;
  vProdutosCodigos: TProdutosFornCodOriginais;
  vProdutosCodBarras: TProdutosCodigosBarrasAux;

  vPrecos: TPrecosProduto;
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_PRODUTO');

  vNovo := pProdutoId = 0;
  vProduto := TProduto.Create(pConexao);
  vExec := TExecucao.Create(pConexao);
  vEstoque := TEstoque.Create(pConexao);
  vGruposTrib := TProdutosGruposTribEmpresas.Create(pConexao);
  vPrecos := TPrecosProduto.Create(pConexao);
  vProdMultiplos := TProdutoMultiploCompra.Create(pConexao);
  vProdutosKit := TProdutosKit.Create(pConexao);
  vProdutosCodigos := TProdutosFornCodOriginais.Create(pConexao);
  vProdutosCodBarras := TProdutosCodigosBarrasAux.Create(pConexao);
  vProc := TProcedimentoBanco.Create(pConexao, 'INICIAR_INFORMAC_NOVO_PRODUTO');

  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_PRODUTO_ID');
    pProdutoId := vSeq.getProximaSequencia;
    result.AsInt := pProdutoId;
    vSeq.Free;
  end;

  try
    pConexao.IniciarTransacao;

    vProduto.setInt('PRODUTO_ID', pProdutoId, True);
    vProduto.setString('NOME', pNome);
    vProduto.setString('CODIGO_NCM', pCodigoNCM);
    vProduto.setInt('MARCA_ID', pMarcaId);
    vProduto.setInt('FORNECEDOR_ID', pFornecedorId);
    vProduto.setDouble('MULTIPLO_VENDA', pMultiploVenda);
    vProduto.setDouble('MULTIPLO_VENDA_2', pMultiploVenda_2);
    vProduto.setDouble('MULTIPLO_VENDA_3', pMultiploVenda_3);
    vProduto.setDouble('PESO', pPeso);
    vProduto.setDouble('VOLUME', pVolume);
    vProduto.setString('UNIDADE_VENDA', pUnidadeVenda);
    vProduto.setString('BLOQUEADO_COMPRAS', pBloqueadoCompras);
    vProduto.setString('BLOQUEADO_VENDAS', pBloqueadoVendas);
    vProduto.setString('PERMITIR_DEVOLUCAO', pPermitirDevolucao);
    vProduto.setString('CARACTERISTICAS', pCaracteristicas);
    vProduto.setString('NOME_COMPRA', pNomeCompra);
    vProduto.setString('ACEITAR_ESTOQUE_NEGATIVO', pAceitarEstoqueNegativo);
    vProduto.setString('CODIGO_BARRAS', pCodigoBarras);
    vProduto.setString('CODIGO_BALANCA', pCodigoBalanca);
    vProduto.setString('LINHA_PRODUTO_ID', pLinhaProdutoId);
    vProduto.setString('INATIVAR_ZERAR_ESTOQUE', pInativarZerarEstoque);
    vProduto.setStringN('CEST', pCest);
    vProduto.setString('ATIVO', pAtivo);
    vProduto.setString('PRODUTO_DIVERSOS_PDV', pProdutoDiversosPDV);
    vProduto.setString('TIPO_CONTROLE_ESTOQUE', pTipoControleEstoque);
    vProduto.setString('EXIGIR_DATA_FABRICACAO_LOTE', pExigirDataFabricacaoLote);
    vProduto.setString('EXIGIR_DATA_VENCIMENTO_LOTE', pExigirDataValidadeLote);
    vProduto.setString('SOB_ENCOMENDA', pSobEncomenda);
    vProduto.setString('CONTROLA_PESO', pControlaPeso);
    vProduto.setInt('DIAS_AVISO_VENCIMENTO', pDiasAvisoVencimento);
    vProduto.setString('MOTIVO_INATIVIDADE',pMotivoInatividade);
    vProduto.setString('REVENDA', pRevenda);
    vProduto.setString('USO_CONSUMO', pUsoConsumo);
    vProduto.setString('ATIVO_IMOBILIZADO', pAtivoImobilizado);
    vProduto.setString('SERVICO', pServico);
    vProduto.setString('CODIGO_ORIGINAL_FABRICANTE', pCodigoOriginalFabricante);
    vProduto.setString('SUJEITO_COMISSAO', pSujeitoComissao);
    vProduto.setString('BLOQUEAR_ENTRAD_SEM_PED_COMPRA', pBloquearEntradSemPedCompra);
    vProduto.setString('EXIGIR_MODELO_NOTA_FISCAL', pExigirModeloNotaFiscal);
    vProduto.setString('EXIGIR_SEPARACAO', pExigirSeparacao);
    vProduto.setString('TIPO_DEF_AUTOMATICA_LOTE', pTipoDefAutomaticaLote);
    vProduto.setString('UNIDADE_ENTREGA_ID', pUnidadeEntregaId);
    vProduto.setString('CONF_SOMENTE_CODIGO_BARRAS', pSepararSomenteCodigoBarras);
    vProduto.setDouble('VALOR_ADICIONAL_FRETE', pValorAdicionalFrete);
    vProduto.setDouble('PERCENTUAL_COMISSAO_VISTA', pPercentualComissaoVista);
    vProduto.setDouble('PERCENTUAL_COMISSAO_PRAZO', pPercentualComissaoPrazo);
    vProduto.setString('EMITIR_SOMENTE_NFE', pEmitirSomenteNfe);
    vProduto.setString('EXPORTAR_WEB', pExportarWeb);


    if vNovo then begin
      vProduto.Inserir;

      // Criando os registros nas tabelas PRODUTOS_LOTES, ESTOQUES, ESTOQUES_DIVISAO
      vProc.Params[0].AsInteger := pProdutoId;
      vProc.Executar;
    end
    else
      vProduto.Atualizar;

    vExec.Clear;
    vExec.Add('update PRODUTOS set ');
    vExec.Add('  FOTO_1 = null, ');
    vExec.Add('  FOTO_2 = null, ');
    vExec.Add('  FOTO_3 = null ');
    vExec.Add('where PRODUTO_ID = :P1');
    vExec.Executar([pProdutoId]);

    if pFoto1 <> nil then begin
      vExec.Clear;
      vExec.Add('update PRODUTOS set ');
      vExec.Add('  FOTO_1 = :P1, ');
      vExec.Add('  FOTO_2 = :P2, ');
      vExec.Add('  FOTO_3 = :P3 ');
      vExec.Add('where PRODUTO_ID = :P4');

      vExec.CarregarBinario('P1', pFoto1);

      if pFoto2 <> nil then
        vExec.CarregarBinario('P2', pFoto2)
      else
        vExec.ParamByName('P2').Value := null;

      if pFoto3 <> nil then
        vExec.CarregarBinario('P3', pFoto3)
      else
        vExec.ParamByName('P3').Value := null;

      vExec.ParamByName('P4').AsInteger := pProdutoId;
      vExec.Executar;
    end;

    vExec.Clear;
    vExec.Add('delete from PRODUTOS_GRUPOS_TRIB_EMPRESAS ');
    vExec.Add('where PRODUTO_ID = :P1 ');
    vExec.Executar([pProdutoId]);

    for i := Low(pGruposTributacoes) to High(pGruposTributacoes) do begin
      vGruposTrib.setInt('PRODUTO_ID', pProdutoId, True);
      vGruposTrib.setInt('EMPRESA_ID', pGruposTributacoes[i].EmpresaId, True);
      vGruposTrib.setInt('GRUPO_TRIB_ESTADUAL_COMPRA_ID', pGruposTributacoes[i].GrupoTribEstadualCompraId);
      vGruposTrib.setInt('GRUPO_TRIB_FEDERAL_COMPRA_ID', pGruposTributacoes[i].GrupoTribFederalCompraId);
      vGruposTrib.setInt('GRUPO_TRIB_ESTADUAL_VENDA_ID', pGruposTributacoes[i].GrupoTribEstadualVendaId);
      vGruposTrib.setInt('GRUPO_TRIB_FEDERAL_VENDA_ID', pGruposTributacoes[i].GrupoTribFederalVendaId);

      vGruposTrib.Inserir;
    end;

    vExec.Clear;
    vExec.Add('delete from ENDERECOS_ESTOQUE_PRODUTO ');
    vExec.Add('where PRODUTO_ID = :P1 ');
    vExec.Executar([pProdutoId]);

    // ----------------------- Alimentando a tabela de endere�os de estoque ----------------------------------
    for i := 0 to Length(pEnderecosEstoque) - 1 do begin
      vExec.Clear;
      vExec.Add('insert into ENDERECOS_ESTOQUE_PRODUTO(POSICAO, ENDERECO_ID, PRODUTO_ID) values (:P1, :P2, :P3)');
      vExec.Executar([i + 1, pEnderecosEstoque[i], pProdutoId]);
    end;

    vExec.Clear;
    vExec.Add('update PRODUTOS_CUSTO_FIXO set ');
    vExec.Add('  CUSTO_FIXO = :P1, ZERAR_CUSTO = :P2 ');
    vExec.Add('where PRODUTO_ID = :P3 and EMPRESA_ID = :P4');
    for i := 0 to Length(pCustoFixo) - 1 do
      vExec.Executar([pCustoFixo[i].CustoFixo, pCustoFixo[i].ZerarCusto, pCustoFixo[i].ProdutoId, pCustoFixo[i].EmpresaId]);

    // ----------------------- Alimentando a tabela de Pre�os dos produtos ----------------------------------
    vExec.SQL.Clear;
    vExec.SQL.Add('delete from PRODUTOS_MULTIPLOS_COMPRA where PRODUTO_ID = :P1');
    vExec.Executar([pProdutoId]);

    vProdMultiplos.setInt('PRODUTO_ID', pProdutoId, True);
    for i := Low(pMultiplosCompra) to High(pMultiplosCompra) do begin
      vProdMultiplos.setString('UNIDADE_ID', pMultiplosCompra[i].UnidadeId, True);
      vProdMultiplos.setDouble('MULTIPLO', pMultiplosCompra[i].Multiplo, True);
      vProdMultiplos.setDouble('QUANTIDADE_EMBALAGEM', pMultiplosCompra[i].QuantidadeEmbalagem, True);
      vProdMultiplos.setInt('ORDEM', i + 1);

      vProdMultiplos.Inserir;
    end;
    // ------------------------------------------------------------------------------------------------------

    // ----------------------- Alimentando os kit ----------------------------------
    vExec.SQL.Clear;
    vExec.SQL.Add('delete from PRODUTOS_KIT where PRODUTO_KIT_ID = :P1');
    vExec.Executar([pProdutoId]);

    vProdutosKit.setInt('PRODUTO_KIT_ID', pProdutoId, True);
    for i := Low(pProdutosKit) to High(pProdutosKit) do begin
      vProdutosKit.setInt('PRODUTO_ID', pProdutosKit[i].ProdutoId, True);
      vProdutosKit.setInt('ORDEM', pProdutosKit[i].Ordem);
      vProdutosKit.setDouble('PERC_PARTICIPACAO', pProdutosKit[i].PercParticipacao);
      vProdutosKit.setDouble('QUANTIDADE', pProdutosKit[i].Quantidade);

      vProdutosKit.Inserir;
    end;
    // ------------------------------------------------------------------------------------------------------

    // ----------------------- Alimentando os c�digos originais ---------------------------------------------
    vExec.SQL.Clear;
    vExec.SQL.Add('delete from PRODUTOS_FORN_COD_ORIGINAIS where PRODUTO_ID = :P1');
    vExec.Executar([pProdutoId]);

    vProdutosCodigos.setInt('PRODUTO_ID', pProdutoId, True);
    for i := Low(pCodigosOriginais) to High(pCodigosOriginais) do begin
      vProdutosCodigos.setInt('FORNECEDOR_ID', pCodigosOriginais[i].FornecedorId, True);
      vProdutosCodigos.setString('CODIGO_ORIGINAL', pCodigosOriginais[i].CodigoOriginal, True);

      vProdutosCodigos.Inserir;
    end;
    // ------------------------------------------------------------------------------------------------------

    // ----------------------- Alimentando os c�digos de barras auxiliares ----------------------------------
    vExec.Limpar;
    vExec.Add('delete from PRODUTOS_CODIGOS_BARRAS_AUX ');
    vExec.Add('where PRODUTO_ID = :P1 ');
    vExec.Executar([pProdutoId]);

    vProdutosCodBarras.setInt('PRODUTO_ID', pProdutoId, True);
    for i := Low(pCodigoBarrasAuxiliares) to High(pCodigoBarrasAuxiliares) do begin
      vProdutosCodBarras.setString('CODIGO_BARRAS', pCodigoBarrasAuxiliares[i].CodigoBarras, True);

      vProdutosCodBarras.Inserir;
    end;
    // ------------------------------------------------------------------------------------------------------

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vProdutosCodBarras.Free;
  vProdutosCodigos.Free;
  vProdMultiplos.Free;
  vProdutosKit.Free;
  vProduto.Free;
  vEstoque.Free;
  vPrecos.Free;
  vGruposTrib.Free;
  vExec.Free;
  vProc.Free;
end;

function BuscarProdutos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean;
  pFiltroExtra: string
): TArray<RecProdutos>;
var
  i: Integer;
  t: TProduto;
  vSqlAuxiliar: string;

  procedure getFotos(pConexao: TConexao; pProdutoId: Integer; var pVetor: RecProdutos);
  var
    p: TConsulta;
    s: TStream;
  begin
    pVetor.foto_1 := nil;
    pVetor.foto_2 := nil;
    pVetor.foto_3 := nil;

    p := TConsulta.Create(pConexao);
    p.SQL.Add('select FOTO_1, FOTO_2, FOTO_3 from PRODUTOS where PRODUTO_ID = :P1');

    if p.Pesquisar([pProdutoId]) then begin
      if not(p.Fields[0].Value = null) then begin
        pVetor.foto_1 := TMemoryStream.Create;
        s := p.CreateBlobStream(p.FieldByName('FOTO_1'), bmRead);
        pVetor.foto_1.LoadFromStream(s);
        s.Free;
      end;

      if not(p.Fields[1].Value = null) then begin
        pVetor.foto_2 := TMemoryStream.Create;
        s := p.CreateBlobStream(p.FieldByName('FOTO_2'), bmRead);
        pVetor.foto_2.LoadFromStream(s);
        s.Free;
      end;

      if not(p.Fields[2].Value = null) then begin
        pVetor.foto_3 := TMemoryStream.Create;
        s := p.CreateBlobStream(p.FieldByName('FOTO_3'), bmRead);
        pVetor.foto_3.LoadFromStream(s);
        s.Free;
      end;
    end;

    p.Free;
  end;

begin
  Result := nil;
  t := TProduto.Create(pConexao);

  vSqlAuxiliar := '';
  if pSomenteAtivos then
    vSqlAuxiliar :=  'and PRO.ATIVO = ''S'' ';

  if pFiltroExtra <> '' then
    vSqlAuxiliar :=  vSqlAuxiliar + pFiltroExtra;

  if t.Pesquisar(pIndice, pFiltros, vSqlAuxiliar) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordProduto;
      getFotos(pConexao, Result[i].produto_id, Result[i]);
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarProdutosComando(pConexao: TConexao; pComando: string): TArray<RecProdutos>;
var
  i: Integer;
  t: TProduto;
begin
  Result := nil;
  t := TProduto.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordProduto;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirProduto(
  pConexao: TConexao;
  pProdutoId: Integer
): RecRetornoBD;
var
  vProduto: TProduto;
  vExec: TExecucao;
begin
  Result.TeveErro := False;
  vProduto := TProduto.Create(pConexao);
  vExec := TExecucao.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    vExec.SQL.Clear;
    vExec.SQL.Add('delete from CUSTOS_PRODUTOS where PRODUTO_ID = :P1');
    vExec.Executar([pProdutoId]);

    vExec.SQL.Clear;
    vExec.SQL.Add('delete from PRODUTOS_ICMS where PRODUTO_ID = :P1');
    vExec.Executar([pProdutoId]);

    vExec.SQL.Clear;
    vExec.SQL.Add('delete from PRODUTOS_ICMS_COMPRA where PRODUTO_ID = :P1');
    vExec.Executar([pProdutoId]);

    vExec.SQL.Clear;
    vExec.SQL.Add('delete from PRECOS_PRODUTOS where PRODUTO_ID = :P1');
    vExec.Executar([pProdutoId]);

    vExec.SQL.Clear;
    vExec.SQL.Add('delete from ESTOQUES where PRODUTO_ID = :P1');
    vExec.Executar([pProdutoId]);

    vExec.SQL.Clear;
    vExec.SQL.Add('delete from PRODUTOS_RELACIONADOS where PRODUTO_PRINCIPAL_ID = :P1');
    vExec.Executar([pProdutoId]);

    vProduto.setInt('PRODUTO_ID', pProdutoId, True);

    vProduto.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vProduto.Free;
  vExec.Free;
end;

function BuscarProdutosPromocao(pConexao: TConexao; pSQL: string): TArray<Integer>;
var
  vSql: TConsulta;
  i: Integer;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  PRODUTO_ID ');
  vSql.SQL.Add('from PRODUTOS_PROMOCOES ');
  vSql.SQL.Add(pSQL);

  vSql.Pesquisar;
  SetLength(Result, vSql.GetQuantidadeRegistros);
  for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
    Result[i] := vSql.GetInt(0);
    vSql.Next;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function BuscarProdutoEntradaNota(pConexao: TConexao; const pProdutoID: Integer): RecProdutoEntrada;
var
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  PRO.PRODUTO_ID, ');
  vSql.SQL.Add('  PRO.NOME, ');
  vSql.SQL.Add('  MAR.NOME ');
  vSql.SQL.Add('from ');
  vSql.SQL.Add('  PRODUTOS PRO ');
  vSql.SQL.Add('inner join MARCAS MAR ');
  vSql.SQL.Add('on PRO.MARCA_ID = MAR.MARCA_ID ');
  vSql.SQL.Add('where PRO.PRODUTO_ID = :P1 ');

  if vSql.Pesquisar([pProdutoID]) then begin
    Result := RecProdutoEntrada.Create;
    Result.produto_id := vSql.GetInt(0);
    Result.nome       := vSql.GetString(1);
    Result.marca      := vSql.GetString(2);
  end;
  vSql.Active := False;
  vSql.Free;
end;

function BuscarProdutoEntradaNota(pConexao: TConexao; const pNomeProduto: string; pCodigoBarras: string): RecProdutoEntrada;
var
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  PRO.PRODUTO_ID, ');
  vSql.SQL.Add('  PRO.NOME, ');
  vSql.SQL.Add('  MAR.NOME ');
  vSql.SQL.Add('from ');
  vSql.SQL.Add('  PRODUTOS PRO ');
  vSql.SQL.Add('inner join MARCAS MAR ');
  vSql.SQL.Add('on PRO.MARCA_ID = MAR.MARCA_ID ');
  vSql.SQL.Add('where PRO.CODIGO_BARRAS = :P1 ');

  if vSql.Pesquisar([pCodigoBarras]) then begin
    Result := RecProdutoEntrada.Create;
    Result.produto_id := vSql.GetInt(0);
    Result.nome       := vSql.GetString(1);
    Result.marca      := vSql.GetString(2);
    Result.metodo_encontrou := 'CB'; // C�digo de barras
  end;

  // Se n�o encontrou pelo c�digo de barras, tentando pelo nome de venda
  if Result = nil then begin
    vSql.SQL.Clear;

    vSql.SQL.Add('select ');
    vSql.SQL.Add('  PRO.PRODUTO_ID, ');
    vSql.SQL.Add('  PRO.NOME, ');
    vSql.SQL.Add('  MAR.NOME ');
    vSql.SQL.Add('from ');
    vSql.SQL.Add('  PRODUTOS PRO ');
    vSql.SQL.Add('inner join MARCAS MAR ');
    vSql.SQL.Add('on PRO.MARCA_ID = MAR.MARCA_ID ');
    vSql.SQL.Add('where PRO.NOME like :P1 || ''%'' ');

    if vSql.Pesquisar([pNomeProduto]) then begin
      Result := RecProdutoEntrada.Create;
      Result.produto_id := vSql.GetInt(0);
      Result.nome       := vSql.GetString(1);
      Result.marca      := vSql.GetString(2);
      Result.metodo_encontrou := 'NV'; // Nome de venda
    end;
  end;

  // Se n�o encontrou pelo c�digo de barras, nem pelo nome de venda, tentando pelo nome de compra
  if Result = nil then begin
    vSql.SQL.Clear;

    vSql.SQL.Add('select ');
    vSql.SQL.Add('  PRO.PRODUTO_ID, ');
    vSql.SQL.Add('  PRO.NOME, ');
    vSql.SQL.Add('  MAR.NOME ');
    vSql.SQL.Add('from ');
    vSql.SQL.Add('  PRODUTOS PRO ');
    vSql.SQL.Add('inner join MARCAS MAR ');
    vSql.SQL.Add('on PRO.MARCA_ID = MAR.MARCA_ID ');
    vSql.SQL.Add('where PRO.NOME_COMPRA like :P1 || ''%'' ');

    if vSql.Pesquisar([pNomeProduto]) then begin
      Result := RecProdutoEntrada.Create;
      Result.produto_id := vSql.GetInt(0);
      Result.nome       := vSql.GetString(1);
      Result.marca      := vSql.GetString(2);
      Result.metodo_encontrou := 'NC'; // Nome de compra
    end;
  end;
  vSql.Active := False;
  vSql.Free;
end;

procedure BuscarImpostosProduto(pConexao: TConexao; var pProdutos: TArray<RecProdutoImpostosCalculados>; pEmpresaId: Integer; pClienteId: Integer);
var
  i: Integer;
  vProc: TProcedimentoBanco;
begin
  vProc := TProcedimentoBanco.Create(pConexao, 'BUSCAR_CALC_IMPOSTOS_PRODUTO');

  try
    for i := Low(pProdutos) to High(pProdutos) do begin
      vProc.Params[0].AsInteger := pProdutos[i].ProdutoId;
      vProc.Params[1].AsInteger := pEmpresaId;
      vProc.Params[2].AsInteger := pClienteId;

      vProc.Params[3].AsFloat := pProdutos[i].ValorTotalProdutos;
      vProc.Params[4].AsFloat := pProdutos[i].ValorOutrasDesp;
      vProc.Params[5].AsFloat := pProdutos[i].ValorDesconto;

      vProc.ExecProc;

      pProdutos[i].ValorICMS   := vProc.ParamByName('oVALOR_ICMS').AsFloat;
      pProdutos[i].ValorPIS    := vProc.ParamByName('oVALOR_PIS').AsFloat;
      pProdutos[i].ValorCOFINS := vProc.ParamByName('oVALOR_COFINS').AsFloat;
      pProdutos[i].ValorIPI    := vProc.ParamByName('oVALOR_IPI').AsFloat;
    end;
  finally
    vProc.Free;
  end;
end;

procedure BuscarImpostosProduto(
  pConexao: TConexao;
  var pProdutos: TArray<RecProdutoImpostosCalculados>;
  pEmpresaId: Integer;
  pCST: string;
  pEstadoDestinoId: string
); overload;
var
  i: Integer;
  vProc: TProcedimentoBanco;
begin
  vProc := TProcedimentoBanco.Create(pConexao, 'BUSCAR_CALC_IMPOSTOS_PROD_CST');

  try
    for i := Low(pProdutos) to High(pProdutos) do begin
      vProc.ParamByName('iPRODUTO_ID').AsInteger        := pProdutos[i].ProdutoId;
      vProc.ParamByName('iEMPRESA_ID').AsInteger        := pEmpresaId;
      vProc.ParamByName('iCST').AsString                := pCST;
      vProc.ParamByName('iESTADO_DESTINO_ID').AsString  := pEstadoDestinoId;

      vProc.ParamByName('iVALOR_TOTAL_PRODUTO').AsFloat := pProdutos[i].ValorTotalProdutos;
      vProc.ParamByName('iVALOR_OUTRAS_DESP').AsFloat   := pProdutos[i].ValorOutrasDesp;
      vProc.ParamByName('iVALOR_DESCONTO').AsFloat      := pProdutos[i].ValorDesconto;

      vProc.ExecProc;

      pProdutos[i].ValorICMS         := vProc.ParamByName('oVALOR_ICMS').AsFloat;

      pProdutos[i].BaseCalculoPis    := vProc.ParamByName('oBASE_CALCULO_PIS').AsFloat;
      pProdutos[i].PercentualPIS     := vProc.ParamByName('oPERCENTUAL_PIS').AsFloat;
      pProdutos[i].ValorPIS          := vProc.ParamByName('oVALOR_PIS').AsFloat;

      pProdutos[i].BaseCalculoCofins := vProc.ParamByName('oBASE_CALCULO_COFINS').AsFloat;
      pProdutos[i].PercentualCofins  := vProc.ParamByName('oPERCENTUAL_COFINS').AsFloat;
      pProdutos[i].ValorCOFINS       := vProc.ParamByName('oVALOR_COFINS').AsFloat;

      pProdutos[i].ValorIPI          := vProc.ParamByName('oVALOR_IPI').AsFloat;
    end;
  finally
    vProc.Free;
  end;
end;


function BuscarCSTsPisCofins(pConexao: TConexao): TArray<RecCstPisCofins>;
var
  i: Integer;
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  CST, ');
  vSql.SQL.Add('  DESCRICAO, ');
  vSql.SQL.Add('  CALCULAR_BASE, ');
  vSql.SQL.Add('  TIPO ');
  vSql.SQL.Add('from ');
  vSql.SQL.Add('  VW_CST_PIS_COFINS ');
  vSql.Pesquisar;

  SetLength(Result, vSql.GetQuantidadeRegistros);
  for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
    Result[i].Cst          := vSql.GetString(0);
    Result[i].Descricao    := vSql.GetString(1);
    Result[i].CalcularBase := vSql.GetString(2);
    Result[i].Tipo         := vSql.GetString(3);

    vSql.Next;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function PodeVincularProdutoPaiFilho(pConexao: TConexao; pProdutoId: Integer): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.TeveErro := False;

  vProc := TProcedimentoBanco.Create(pConexao, 'VER_PODE_VINCULAR_PAI_FILHO');
  vProc.Params[0].AsInteger := pProdutoId;

  try
    vProc.Executar;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  vProc.Free;
end;

function AtualizarProdutoPaiFilho(pConexao: TConexao; pProdutoId: Integer; pProdutoPaiId: Integer; pQuantidadeVezesPai: Double): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.TeveErro := False;
  vExec := TExecucao.Create(pConexao);

  vExec.Add('update PRODUTOS set');
  vExec.Add('  PRODUTO_PAI_ID = :P2, ');
  vExec.Add('  QUANTIDADE_VEZES_PAI = :P3 ');
  vExec.Add('where PRODUTO_ID = :P1');

  try
    pConexao.IniciarTransacao;

    vExec.Executar([pProdutoId, pProdutoPaiId, pQuantidadeVezesPai]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vExec.Free;
end;

function BuscarProdutosPaiFilho(pConexao: TConexao; pFiltros: string): TArray<RecProdutosPaiFilho>;
var
  i: Integer;
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  PRO.PRODUTO_ID, ');
  vSql.Add('  PRO.NOME, ');
  vSql.Add('  PRO.QUANTIDADE_VEZES_PAI, ');
  vSql.Add('  PRO.PRODUTO_PAI_ID, ');
  vSql.Add('  PRP.NOME as NOME_PRODUTO_PAI ');
  vSql.Add('from ');
  vSql.Add('  PRODUTOS PRO ');

  vSql.Add('inner join PRODUTOS PRP ');
  vSql.Add('on PRO.PRODUTO_PAI_ID = PRP.PRODUTO_ID ');

  vSql.Add('where PRO.PRODUTO_ID <> PRO.PRODUTO_PAI_ID ');
  vSql.Add(pFiltros);

  vSql.Add('order by ');
  vSql.Add('  PRP.NOME, ');
  vSql.Add('  PRO.NOME ');

  vSql.Pesquisar;

  SetLength(Result, vSql.GetQuantidadeRegistros);
  for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
    Result[i].ProdutoId      := vSql.GetInt(0);
    Result[i].Nome           := vSql.GetString(1);
    Result[i].QtdeVezesPai   := vSql.GetDouble(2);
    Result[i].ProdutoPaiId   := vSql.GetInt(3);
    Result[i].NomeProdutoPai := vSql.GetString(4);

    vSql.Next;
  end;

  vSql.Free;
end;

function BuscarUnidadeVenda(pConexao: TConexao; pProdutoId: Integer): string;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  UNIDADE_VENDA ');
  vSql.Add('from ');
  vSql.Add('  PRODUTOS ');
  vSql.Add('where PRODUTO_ID = :P1 ');
  vSql.Pesquisar([pProdutoId]);

  Result := vSql.GetString('UNIDADE_VENDA');

  vSql.Free;
end;

function AtualizarProdutoXml(pConexao: TConexao; pProdutoId: Integer; pCampo: string; pValor: Variant): RecRetornoBD;
var
  vExec: TExecucao;
begin
  pConexao.SetRotina('ATUALIZAR_PROD_XML_ENTRADA');
  Result.TeveErro := False;
  vExec := TExecucao.Create(pConexao);

  vExec.Add('update PRODUTOS set');
  vExec.Add('  ' + pCampo + ' = :p2 ');
  vExec.Add('where PRODUTO_ID = :P1');

  try
    pConexao.IniciarTransacao;

    vExec.Executar([pProdutoId, pValor]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vExec.Free;
end;

function AtualizarCampoProduto(pConexao: TConexao; pValor: string; pProdutosIds: TArray<Integer>; pCampo: string; pMotivo: string = ''): RecRetornoBD;
var
  vExec: TExecucao;
begin
  pConexao.SetRotina('ATUALIZAR_ACEITA_EST_NEGATIVO');
  Result.TeveErro := False;
  vExec := TExecucao.Create(pConexao);

  vExec.Add('update PRODUTOS set');

  if (pCampo = 'ATIVO') and (pValor = 'N') then begin
    vExec.Add('ATIVO = :P1, ');
    vExec.Add('MOTIVO_INATIVIDADE = :P2 ');
  end
  else
    vExec.Add(pCampo + ' = :P1');

  vExec.Add('where ' + FiltroInInt('PRODUTO_ID', pProdutosIds) );

  try
    pConexao.IniciarTransacao;

    if (pCampo = 'ATIVO') and (pValor = 'N') then
      vExec.Executar([pValor, pMotivo])
    else
      vExec.Executar([pValor]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vExec.Free;

end;

function AtualizarCustoVendaProdutos(pConexao: TConexao; pProdutosIds: TArray<Integer>; pValor: Double): RecRetornoBD;
var
  vExec: TExecucao;
begin
  pConexao.SetRotina('ATUALIZAR_CUSTO_FIXO');
  Result.TeveErro := False;
  vExec := TExecucao.Create(pConexao);

  vExec.Add('update PRODUTOS_CUSTO_FIXO set CUSTO_FIXO = :P1 where ' + FiltroInInt('PRODUTO_ID', pProdutosIds));

  try
    pConexao.IniciarTransacao;
    vExec.Executar([pValor]);
    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vExec.Free;
end;

function AtualizarZerarCustoProdutos(pConexao: TConexao; pProdutosIds: TArray<Integer>; pValor: string): RecRetornoBD;
var
  vExec: TExecucao;
begin
  pConexao.SetRotina('ATUALIZAR_ZERAR_CUSTO');
  Result.TeveErro := False;
  vExec := TExecucao.Create(pConexao);

  vExec.Add('update PRODUTOS_CUSTO_FIXO set ZERAR_CUSTO = :P1 where ' + FiltroInInt('PRODUTO_ID', pProdutosIds));

  try
    pConexao.IniciarTransacao;
    vExec.Executar([pValor]);
    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vExec.Free;
end;

function AtualizarEnderecosEstoqueProdutos(pConexao: TConexao; enderecos: TArray<Integer>; pProdutosIds: TArray<Integer>): RecRetornoBD;
var
  vExec: TExecucao;
  i: Integer;
  j: Integer;
begin
  pConexao.SetRotina('ATUALIZAR_ENDERECOS_ESTOQUE');
  Result.TeveErro := False;
  vExec := TExecucao.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    for i := 0 to Length(pProdutosIds) - 1 do begin
      vExec.Clear;
      vExec.SQL.Add('delete from ENDERECOS_ESTOQUE_PRODUTO where PRODUTO_ID = :P1');
      vExec.Executar([pProdutosIds[i]]);

      for j := 0 to Length(enderecos) - 1 do begin
        vExec.Clear;
        vExec.Add('insert into ENDERECOS_ESTOQUE_PRODUTO(POSICAO, ENDERECO_ID, PRODUTO_ID) values (:P1, :P2, :P3)');
        vExec.Executar([j + 1, enderecos[j], pProdutosIds[i]]);
      end;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vExec.Free;

end;

function buscarCustoFixoProdutosPorEmpresa(pConexao: TConexao; pProdutoId: Integer): TArray<RecProdutoCustosFixo>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  PRO.PRODUTO_ID, ');
  vSql.Add('  EMP.EMPRESA_ID, ');
  vSql.Add('  EMP.NOME_FANTASIA as EMPRESA_NOME, ');
  vSql.Add('  PRO.CUSTO_FIXO, ');
  vSql.Add('  PRO.ZERAR_CUSTO ');
  vSql.Add('from PRODUTOS_CUSTO_FIXO PRO ');

  vSql.Add('inner join EMPRESAS EMP ');
  vSql.Add('on EMP.EMPRESA_ID = PRO.EMPRESA_ID ');

  vSql.Add('where PRO.PRODUTO_ID = :PRODUTO_ID ');

  vSql.Add('order by EMP.EMPRESA_ID asc ');

  if vSql.Pesquisar([pProdutoId]) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros -1 do begin
      Result[i].ProdutoId    := vSql.GetInt('PRODUTO_ID');
      Result[i].EmpresaId    := vSql.GetInt('EMPRESA_ID');
      Result[i].EmpresaNome  := vSql.GetString('EMPRESA_NOME');
      Result[i].CustoFixo    := vSql.GetDouble('CUSTO_FIXO');
      Result[i].ZerarCusto   := vSql.GetString('ZERAR_CUSTO');

      vSql.Next;
    end;
  end;

  vSql.Free;
end;

function buscarProdutosCodigoBarras(pConexao: TConexao; pCodigoBarras: TArray<string>): TArray<RecProdutoCodigoBarras>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  PRO.PRODUTO_ID, ');
  vSql.Add('  PRO.CODIGO_BARRAS, ');
  vSql.Add('  PRO.CODIGO_ORIGINAL_FABRICANTE, ');
  vSql.Add('  PRO.FORNECEDOR_ID, ');
  vSql.Add('  CAD.RAZAO_SOCIAL as NOME_FORNECEDOR ');
  vSql.Add('from ');
  vSql.Add('  PRODUTOS PRO ');

  vSql.Add('inner join CADASTROS CAD ');
  vSql.Add('on PRO.FORNECEDOR_ID = CAD.CADASTRO_ID ');

  vSql.Add('where ' + FiltroInStr('PRO.CODIGO_BARRAS', pCodigoBarras));
  vSql.Add('or PRO.PRODUTO_ID in( ');
  vSql.Add('  select ');
  vSql.Add('    PRODUTO_ID ');
  vSql.Add('  from ');
  vSql.Add('    PRODUTOS_CODIGOS_BARRAS_AUX ');
  vSql.Add('  where ' + FiltroInStr('CODIGO_BARRAS', pCodigoBarras));
  vSql.Add(') ');

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros -1 do begin
      Result[i].ProdutoId       := vSql.GetInt(0);
      Result[i].CodigoBarras    := vSql.GetString(1);
      Result[i].CodigoOriginal  := vSql.GetString(2);
      Result[i].FornecedorId    := vSql.GetInt(3);
      Result[i].NomeFornecedor  := vSql.GetString(4);

      vSql.Next;
    end;
  end;

  vSql.Free;
end;

function ExisteProdutoNomeMarcaCadastrado(pConexao: TConexao; pProdutoId: Integer; pNome: string; pMarcaId: Integer): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  count(*) ');
  vSql.Add('from ');
  vSql.Add('  PRODUTOS ');
  vSql.Add('where NOME = :P1');
  vSql.Add('and MARCA_ID = :P2 ');
  vSql.Add('and PRODUTO_ID <> :P3 ');

  vSql.Pesquisar([pNome, pMarcaId, pProdutoId]);
  Result := vSql.GetInt(0) > 0;

  vSql.Free;
end;

procedure BuscarPrecoVendaCustoCompra(pConexao: TConexao; pEmpresaId: Integer; pProdutoId: Integer; var precoVenda: Double; var custoCompra: Double);
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Clear;
  vSql.Add('select PRECO_VAREJO from PRECOS_PRODUTOS where PRODUTO_ID = :P1 and EMPRESA_ID = :P2');
  vSql.Pesquisar([pProdutoId, pEmpresaId]);
  precoVenda := vSql.GetDouble('PRECO_VAREJO');

  vSql.SQL.Clear;
  vSql.Add('select PRECO_LIQUIDO from CUSTOS_PRODUTOS where PRODUTO_ID = :P1 and EMPRESA_ID = :P2');
  vSql.Pesquisar([pProdutoId, pEmpresaId]);
  custoCompra := vSql.GetDouble('PRECO_LIQUIDO');

  vSql.Free;
end;

function BuscarEnderecosEstoque(pConexao: TConexao; ProdutoId: Integer): TArray<Integer>;
var
  vSql: TConsulta;
  i: Integer;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Clear;
  vSql.Add('select ENDERECO_ID from ENDERECOS_ESTOQUE_PRODUTO where PRODUTO_ID = :P1 order by POSICAO asc');

  if vSql.Pesquisar([ProdutoId]) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros -1 do begin
      Result[i] := vSql.GetInt(0);
      vSql.Next;
    end;
  end;

  vSql.Free;
end;

function BuscarEnderecosEstoqueCompleto(pConexao: TConexao; ProdutoId: Integer): TArray<RecEnderecosEstoque>;
var
  vSql: TConsulta;
  i: Integer;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Clear;
  vSql.Add(
    'select ' +
    '  EES.ENDERECO_ID, ' +
    '  EER.DESCRICAO AS NOME_RUA, ' +
    '  EEM.DESCRICAO AS NOME_MODULO, ' +
    '  EEN.DESCRICAO AS NOME_NIVEL, ' +
    '  EEV.DESCRICAO AS NOME_VAO ' +
    'from ' +
    '  ENDERECOS_ESTOQUE_PRODUTO EEP ' +

    'inner join ENDERECO_ESTOQUE EES ' +
    'on EES.ENDERECO_ID = EEP.ENDERECO_ID ' +

    'inner join ENDERECO_EST_RUA EER ' +
    'on EER.RUA_ID = EES.RUA_ID ' +

    'inner join ENDERECO_EST_MODULO EEM ' +
    'on EEM.MODULO_ID = EES.MODULO_ID ' +

    'inner join ENDERECO_EST_NIVEL EEN ' +
    'on EEN.NIVEL_ID = EES.NIVEL_ID ' +

    'inner join ENDERECO_EST_VAO EEV ' +
    'on EEV.VAO_ID = EES.VAO_ID ' +

    'where EEP.PRODUTO_ID = :P1'
  );

  Result := nil;

  if vSql.Pesquisar([ProdutoId]) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);

    for i := 0 to vSql.GetQuantidadeRegistros -1 do begin
      Result[i] := RecEnderecosEstoque.Create;
      Result[i].endereco_id := vSql.GetInt('ENDERECO_ID');
      Result[i].nome_rua := vSql.GetString('NOME_RUA');
      Result[i].nome_modulo := vSql.GetString('NOME_MODULO');
      Result[i].nome_nivel := vSql.GetString('NOME_NIVEL');
      Result[i].nome_vao := vSql.GetString('NOME_VAO');
      vSql.Next;
    end;
  end;

  vSql.Free;
end;

function AtualizarComissaoAVista(pConexao: TConexao; pProdutosIds: TArray<Integer>; pValor: Double): RecRetornoBD;
var
  vExec: TExecucao;
begin
  pConexao.SetRotina('ATUALIZAR_COMISSAO_VISTA');
  Result.TeveErro := False;
  vExec := TExecucao.Create(pConexao);

  vExec.Add('update PRODUTOS set PERCENTUAL_COMISSAO_VISTA = :P1 where ' + FiltroInInt('PRODUTO_ID', pProdutosIds));

  try
    pConexao.IniciarTransacao;
    vExec.Executar([pValor]);
    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vExec.Free;
end;

function AtualizarComissaoAPrazo(pConexao: TConexao; pProdutosIds: TArray<Integer>; pValor: Double): RecRetornoBD;
var
  vExec: TExecucao;
begin
  pConexao.SetRotina('ATUALIZAR_COMISSAO_PRAZO');
  Result.TeveErro := False;
  vExec := TExecucao.Create(pConexao);

  vExec.Add('update PRODUTOS set PERCENTUAL_COMISSAO_PRAZO = :P1 where ' + FiltroInInt('PRODUTO_ID', pProdutosIds));

  try
    pConexao.IniciarTransacao;
    vExec.Executar([pValor]);
    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vExec.Free;
end;

function AtualizarTributacaoProdutos(pConexao: TConexao; pProdutosIds: TArray<Integer>; tributacoes: TArray<RecProdutosGruposTribEmpresas>): RecRetornoBD;
var
  vExec: TExecucao;
  vGruposTrib: TProdutosGruposTribEmpresas;
  idxTrib: Integer;
  idxProduto: Integer;
begin
  pConexao.SetRotina('ATUALIZAR_TRIBUTACOES_PROD');
  Result.TeveErro := False;

  vExec := TExecucao.Create(pConexao);
  vGruposTrib := TProdutosGruposTribEmpresas.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    vExec.Clear;
    vExec.Add('delete from PRODUTOS_GRUPOS_TRIB_EMPRESAS ');
    vExec.Add('where ' + FiltroInInt('PRODUTO_ID', pProdutosIds));
    vExec.Executar;

    for idxProduto := Low(pProdutosIds) to High(pProdutosIds) do begin

      for idxTrib := Low(tributacoes) to High(tributacoes) do begin
        vGruposTrib.setInt('PRODUTO_ID', pProdutosIds[idxProduto], True);
        vGruposTrib.setInt('EMPRESA_ID', tributacoes[idxTrib].EmpresaId, True);
        vGruposTrib.setInt('GRUPO_TRIB_ESTADUAL_COMPRA_ID', tributacoes[idxTrib].GrupoTribEstadualCompraId);
        vGruposTrib.setInt('GRUPO_TRIB_FEDERAL_COMPRA_ID', tributacoes[idxTrib].GrupoTribFederalCompraId);
        vGruposTrib.setInt('GRUPO_TRIB_ESTADUAL_VENDA_ID', tributacoes[idxTrib].GrupoTribEstadualVendaId);
        vGruposTrib.setInt('GRUPO_TRIB_FEDERAL_VENDA_ID', tributacoes[idxTrib].GrupoTribFederalVendaId);

        vGruposTrib.Inserir;
      end;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vExec.Free;
end;

function BuscarProdutosRelacaoProdutos(
  pConexao: TConexao;
  pComandoSQL: string
): TArray<RecProdutos>;
var
  vSql: TConsulta;
  i: Integer;
begin
  vSql := TConsulta.Create(pConexao);
  Result := nil;

  vSql.Add('select ');
  vSql.Add('  PRO.PRODUTO_ID, ');
  vSql.Add('  PRO.DATA_CADASTRO, ');
  vSql.Add('  PRO.CODIGO_NCM, ');
  vSql.Add('  PRO.MARCA_ID, ');
  vSql.Add('  PRO.FORNECEDOR_ID, ');
  vSql.Add('  PRO.MULTIPLO_VENDA, ');
  vSql.Add('  0 as MULTIPLO_VENDA_2, ');
  vSql.Add('  0 as MULTIPLO_VENDA_3, ');
  vSql.Add('  PRO.PESO, ');
  vSql.Add('  PRO.VOLUME, ');
  vSql.Add('  PRO.UNIDADE_VENDA, ');
  vSql.Add('  PRO.BLOQUEADO_COMPRAS, ');
  vSql.Add('  PRO.BLOQUEADO_VENDAS, ');
  vSql.Add('  PRO.PERMITIR_DEVOLUCAO, ');
  vSql.Add('  PRO.CARACTERISTICAS, ');
  vSql.Add('  PRO.ATIVO, ');
  vSql.Add('  PRO.PRODUTO_DIVERSOS_PDV, ');
  vSql.Add('  PRO.NOME_COMPRA, ' );
  vSql.Add('  PRO.CEST, ');
  vSql.Add('  PRO.ACEITAR_ESTOQUE_NEGATIVO, ');
  vSql.Add('  PRO.CODIGO_BARRAS, ');
  vSql.Add('  PRO.CODIGO_BALANCA, ');
  vSql.Add('  PRO.LINHA_PRODUTO_ID, ');
  vSql.Add('  PRO.NOME, ');
  vSql.Add('  PRO.INATIVAR_ZERAR_ESTOQUE, ');
  vSql.Add('  PRO.PRODUTO_PAI_ID, ');
  vSql.Add('  PRO.QUANTIDADE_VEZES_PAI, ');
  vSql.Add('  PRO.TIPO_CONTROLE_ESTOQUE, ');
  vSql.Add('  MAR.NOME as NOME_MARCA, ');
  vSql.Add('  CAD.NOME_FANTASIA as NOME_FABRICANTE, ');
  vSql.Add('  PRO.EXIGIR_DATA_FABRICACAO_LOTE, ');
  vSql.Add('  PRO.EXIGIR_DATA_VENCIMENTO_LOTE, ');
  vSql.Add('  PRO.SOB_ENCOMENDA, ');
  vSql.Add('  PRO.CONTROLA_PESO, ');
  vSql.Add('  PRO.DIAS_AVISO_VENCIMENTO, ');
  vSql.Add('  PRO.MOTIVO_INATIVIDADE, ');
  vSql.Add('  PRO.REVENDA, ');
  vSql.Add('  PRO.CODIGO_ORIGINAL_FABRICANTE, ');
  vSql.Add('  PRO.USO_CONSUMO, ');
  vSql.Add('  PRO.ATIVO_IMOBILIZADO, ');
  vSql.Add('  PRO.SERVICO, ' );
  vSql.Add('  PRO.SUJEITO_COMISSAO, ');
  vSql.Add('  PRO.BLOQUEAR_ENTRAD_SEM_PED_COMPRA, ');
  vSql.Add('  PRO.EXIGIR_MODELO_NOTA_FISCAL, ');
  vSql.Add('  PRO.EXIGIR_SEPARACAO, ');
  vSql.Add('  PRO.VALOR_ADICIONAL_FRETE, ');
  vSql.Add('  PRO.PERCENTUAL_COMISSAO_VISTA, ');
  vSql.Add('  PRO.PERCENTUAL_COMISSAO_PRAZO, ');
  vSql.Add('  PRO.TIPO_DEF_AUTOMATICA_LOTE, ');
  vSql.Add('  PRO.UNIDADE_ENTREGA_ID, ');
  vSql.Add('  PRO.CONF_SOMENTE_CODIGO_BARRAS, ');
  vSql.Add('  PRO.CHAVE_IMPORTACAO, ');
  vSql.Add('  TRE.GRUPO_TRIB_ESTADUAL_VENDA_ID, ');
  vSql.Add('  TRE.DESCRICAO as GRUPO_TRIB_ESTADUAL_VENDA_NOME, ');
  vSql.Add('  PRO.LINHA_PRODUTO_ID, ');
  vSql.Add('  PDS.NOME as LINHA_PRODUTO_NOME ');
  vSql.Add('from ');
  vSql.Add('  PRODUTOS PRO ');

  vSql.Add('inner join MARCAS MAR ');
  vSql.Add('on PRO.MARCA_ID = MAR.MARCA_ID ');

  vSql.Add('inner join CADASTROS CAD ');
  vSql.Add('on PRO.FORNECEDOR_ID = CAD.CADASTRO_ID ');

  vSql.Add('left join PRODUTOS_DEPTOS_SECOES_LINHAS PDS ');
  vSql.Add('on PRO.LINHA_PRODUTO_ID = PDS.DEPTO_SECAO_LINHA_ID ');

  vSql.Add('left join VW_GRUPOS_TRIBUTACOES_ESTADUAL GTE ');
  vSql.Add('on PRO.PRODUTO_ID = GTE.PRODUTO_ID ');

  vSql.Add('left join GRUPOS_TRIB_ESTADUAL_VENDA TRE ');
  vSql.Add('on GTE.GRUPO_TRIB_ESTADUAL_VENDA_ID = TRE.GRUPO_TRIB_ESTADUAL_VENDA_ID ');

  vSql.Add(pComandoSQL);

  if vSql.Pesquisar() then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);

    for i := 0 to vSql.GetQuantidadeRegistros -1 do begin
      Result[i] := RecProdutos.Create;
      Result[i].produto_id               := vSql.getInt('PRODUTO_ID');
      Result[i].data_cadastro            := vSql.getData('DATA_CADASTRO');
      Result[i].codigo_ncm               := vSql.getString('CODIGO_NCM');
      Result[i].marca_id                 := vSql.getInt('MARCA_ID');
      Result[i].fornecedor_id            := vSql.getInt('FORNECEDOR_ID');
      Result[i].multiplo_venda           := vSql.getDouble('MULTIPLO_VENDA');
      Result[i].multiplo_venda_2         := vSql.getDouble('MULTIPLO_VENDA_2');
      Result[i].multiplo_venda_3         := vSql.getDouble('MULTIPLO_VENDA_3');
      Result[i].peso                     := vSql.getDouble('PESO');
      Result[i].volume                   := vSql.getDouble('VOLUME');
      Result[i].unidade_venda            := vSql.getString('UNIDADE_VENDA');
      Result[i].bloqueado_compras        := vSql.getString('BLOQUEADO_COMPRAS');
      Result[i].bloqueado_vendas         := vSql.getString('BLOQUEADO_VENDAS');
      Result[i].permitir_devolucao       := vSql.getString('PERMITIR_DEVOLUCAO');
      Result[i].caracteristicas          := vSql.getString('CARACTERISTICAS');
      Result[i].ativo                    := vSql.getString('ATIVO');
      Result[i].produtoDiversosPDV       := vSql.getString('PRODUTO_DIVERSOS_PDV');
      Result[i].nome_compra              := vSql.getString('NOME_COMPRA');
      Result[i].cest                     := vSql.getString('CEST');
      Result[i].aceitar_estoque_negativo := vSql.getString('ACEITAR_ESTOQUE_NEGATIVO');
      Result[i].codigo_barras            := vSql.getString('CODIGO_BARRAS');
      Result[i].codigoBalanca            := vSql.getString('CODIGO_BALANCA');
      Result[i].LinhaProdutoId           := vSql.getString('LINHA_PRODUTO_ID');
      Result[i].nome                     := vSql.getString('NOME');
      Result[i].inativar_zerar_estoque   := vSql.getString('INATIVAR_ZERAR_ESTOQUE');
      Result[i].nome_fabricante          := vSql.GetString('NOME_FABRICANTE');
      Result[i].nome_marca               := vSql.GetString('NOME_MARCA');
      Result[i].ProdutoPaiId             := vSql.getInt('PRODUTO_PAI_ID');
      Result[i].QuantidadeVezesPai       := vSql.getDouble('QUANTIDADE_VEZES_PAI');
      Result[i].TipoControleEstoque      := vSql.GetString('TIPO_CONTROLE_ESTOQUE');
      Result[i].ExigirDataFabricacaoLote := vSql.GetString('EXIGIR_DATA_FABRICACAO_LOTE');
      Result[i].ExigirDataVencimentoLote := vSql.GetString('EXIGIR_DATA_VENCIMENTO_LOTE');
      Result[i].SobEncomenda             := vSql.GetString('SOB_ENCOMENDA');
      Result[i].ControlaPeso             := vSql.GetString('CONTROLA_PESO');
      Result[i].DiasAvisoVencimento      := vSql.GetInt('DIAS_AVISO_VENCIMENTO');
      Result[i].MotivoInatividade        := vSql.getString('MOTIVO_INATIVIDADE');
      Result[i].CodigoOriginalFabricante := vSql.GetString('CODIGO_ORIGINAL_FABRICANTE');
      Result[i].Revenda                  := vSql.GetString('REVENDA');
      Result[i].UsoConsumo               := vSql.GetString('USO_CONSUMO');
      Result[i].AtivoImobilizado         := vSql.GetString('ATIVO_IMOBILIZADO');
      Result[i].SujeitoComissao          := vSql.GetString('SUJEITO_COMISSAO');
      Result[i].BloquearEntradSemPedCompra := vSql.GetString('BLOQUEAR_ENTRAD_SEM_PED_COMPRA');
      Result[i].NaoExigirModeloNotaFiscal  := vSql.GetString('EXIGIR_MODELO_NOTA_FISCAL');
      Result[i].ExigirSeparacao            := vSql.GetString('EXIGIR_SEPARACAO');
      Result[i].ValorAdicionalFrete        := vSql.GetDouble('VALOR_ADICIONAL_FRETE');
      Result[i].PercentualComissaoVista    := vSql.GetDouble('PERCENTUAL_COMISSAO_VISTA');
      Result[i].PercentualComissaoPrazo    := vSql.GetDouble('PERCENTUAL_COMISSAO_PRAZO');
      Result[i].TipoDefAutomaticaLote      := vSql.GetString('TIPO_DEF_AUTOMATICA_LOTE');
      Result[i].UnidadeEntregaId           := vSql.GetString('UNIDADE_ENTREGA_ID');
      Result[i].ConfSomenteCodigoBarras    := vSql.GetString('CONF_SOMENTE_CODIGO_BARRAS');

      Result[i].LinhaProdutoId             := vSql.GetString('LINHA_PRODUTO_ID');
      Result[i].LinhaProdutoNome           := vSql.GetString('LINHA_PRODUTO_NOME');

      Result[i].GrupoTribEstadualVendaId   := vSql.GetInt('GRUPO_TRIB_ESTADUAL_VENDA_ID');
      Result[i].GrupoTribEstadualVendaNome := vSql.GetString('GRUPO_TRIB_ESTADUAL_VENDA_NOME');
      vSql.Next;
    end;
  end;

  vSql.Free;
end;

function BuscarProdutosSemVender(
  pConexao: TConexao;
  qtdeDias: Integer;
  EmpresaId: Integer;
  somenteProdutosSemVender: Boolean;
  situacao: string;
  sqlTributacao: string;
  sqlFiltro: string
): TArray<RecProdutosSemVender>;
var
  vSql: TConsulta;
  vSqlProdutosJaVendidos: string;
  vSqlProdutosSemVender: string;
  i: Integer;
begin
  vSql := TConsulta.Create(pConexao);
  vSqlProdutosSemVender :=
    'select ' +
    '  :P1 as EMPRESA_ID, ' +
    '  PRO.PRODUTO_ID, ' +
    '  PRO.ATIVO, ' +
    '  PRO.NOME, ' +
    '  PRO.MARCA_ID, ' +
    '  MAR.NOME as MARCA_NOME, ' +
    '  PRO.UNIDADE_VENDA, ' +
    '  EST.DISPONIVEL as ESTOQUE_DISPONIVEL, ' +
    '  CUS.PRECO_LIQUIDO as CUSTO_UNITARIO, ' +
    '  EST.DISPONIVEL * CUS.PRECO_LIQUIDO as CUSTO_TOTAL, ' +
    '  TRUNC(SYSDATE) - PRO.DATA_CADASTRO as QTDE_DIAS_SEM_VENDER ' +
    'from ' +
    '  PRODUTOS PRO ' +

    'inner join MARCAS MAR ' +
    'on MAR.MARCA_ID = PRO.MARCA_ID ' +

    'inner join ESTOQUES EST ' +
    'on EST.PRODUTO_ID = PRO.PRODUTO_ID ' +
    'and EST.EMPRESA_ID = :P1 ' +
    //'--and EST.DISPONIVEL > 0 ' +

    'inner join CUSTOS_PRODUTOS CUS ' +
    'on CUS.PRODUTO_ID = PRO.PRODUTO_ID ' +
    'and CUS.EMPRESA_ID = :P1 ' +

    sqlTributacao +

    'where PRO.PRODUTO_ID not in( ' +
    '  select ' +
    '    PRODUTO_ID ' +
    '  from ' +
    '    PRODUTOS ' +
    '  where PRODUTO_ID in( ' +
    '   select ' +
    '     PRODUTO_ID ' +
    '   from ORCAMENTOS_ITENS ITE     ' +

    '   inner join ORCAMENTOS ORC ' +
    '   on ORC.ORCAMENTO_ID = ITE.ORCAMENTO_ID ' +

    '   where ORC.EMPRESA_ID = :P1 ' +
    '  ) ' +
    ') ' + sqlFiltro;

    if not somenteProdutosSemVender then
      vSqlProdutosSemVender := vSqlProdutosSemVender + '  and ((TRUNC(SYSDATE) - TRUNC(PRO.DATA_CADASTRO)) > :P2) ';

    if situacao <> 'T' then
      vSqlProdutosSemVender := vSqlProdutosSemVender + ' and PRO.ATIVO = ''' + situacao + ''' ';

  vSqlProdutosJaVendidos :=
    'select ' +
    '  ORC.EMPRESA_ID, ' +
    '  PRO.PRODUTO_ID, ' +
    '  PRO.ATIVO, ' +
    '  PRO.NOME, ' +
    '  PRO.MARCA_ID, ' +
    '  MAR.NOME as MARCA_NOME, ' +
    '  PRO.UNIDADE_VENDA, ' +
    '  EST.DISPONIVEL as ESTOQUE_DISPONIVEL, ' +
    '  CUS.PRECO_LIQUIDO as CUSTO_UNITARIO, ' +
    '  EST.DISPONIVEL * CUS.PRECO_LIQUIDO as CUSTO_TOTAL, ' +
    '  TRUNC(SYSDATE) - TRUNC(ORC.DATA_HORA_RECEBIMENTO) as QTDE_DIAS_SEM_VENDER ' +
    'from ' +
    '  ORCAMENTOS_ITENS ITE ' +

    'inner join PRODUTOS PRO ' +
    'on PRO.PRODUTO_ID = ITE.PRODUTO_ID ' +

    'inner join ORCAMENTOS ORC ' +
    'on ORC.ORCAMENTO_ID = ITE.ORCAMENTO_ID ' +

    'inner join MARCAS MAR ' +
    'on MAR.MARCA_ID = PRO.MARCA_ID ' +

    'inner join ESTOQUES EST ' +
    'on EST.PRODUTO_ID = PRO.PRODUTO_ID ' +
    'and EST.EMPRESA_ID = :P1 ' +
    //'--and EST.DISPONIVEL > 0 ' +

    'inner join CUSTOS_PRODUTOS CUS ' +
    'on CUS.PRODUTO_ID = PRO.PRODUTO_ID ' +
    'and CUS.EMPRESA_ID = :P1 ' +

    sqlTributacao +

    'inner join ( ' +
    '  select max (ORC.ORCAMENTO_ID) AS ORCAMENTO_ID, ' +
    '    ITE.PRODUTO_ID ' +
    '  from ' +
    '    ORCAMENTOS_ITENS ITE ' +
    '  inner join ORCAMENTOS ORC ' +
    '  on ITE.ORCAMENTO_ID = ORC.ORCAMENTO_ID ' +

    '  where ORC.STATUS = ''RE'' ' +

    '  group by  ITE.PRODUTO_ID ' +

    '  order by ITE.PRODUTO_ID ' +
    ') VEN ' +
    'on VEN.PRODUTO_ID = ITE.PRODUTO_ID ' +
    'and ITE.ORCAMENTO_ID = VEN.ORCAMENTO_ID ' +

    'where ITE.PRODUTO_ID not in( ' +
    '  select ' +
    '    PRO.PRODUTO_ID ' +
    '  from ' +
    '    ORCAMENTOS_ITENS ITE ' +

    '  inner join PRODUTOS PRO ' +
    '  on PRO.PRODUTO_ID = ITE.PRODUTO_ID ' +

    '  inner join ORCAMENTOS ORC ' +
    '  on ORC.ORCAMENTO_ID = ITE.ORCAMENTO_ID ' +

    '  inner join ( ' +
    '    select max (ORC.ORCAMENTO_ID) AS ORCAMENTO_ID, ' +
    '      ITE.PRODUTO_ID ' +
    '    from ' +
    '      ORCAMENTOS_ITENS ITE ' +

    '    inner join ORCAMENTOS ORC ' +
    '    on ITE.ORCAMENTO_ID = ORC.ORCAMENTO_ID ' +

    '    where ORC.STATUS = ''RE'' ' +

    '    group by  ITE.PRODUTO_ID ' +

    '    order by ITE.PRODUTO_ID ' +
    '  ) VEN ' +
    '  on VEN.PRODUTO_ID = ITE.PRODUTO_ID ' +
    '  and ITE.ORCAMENTO_ID = VEN.ORCAMENTO_ID ' +

    '  where ((TRUNC(SYSDATE) - TRUNC(ORC.DATA_HORA_RECEBIMENTO)) < :P2) ' +
    '  and ORC.EMPRESA_ID = :P1 ' +
    ') ' + sqlFiltro;

    if situacao <> 'T' then
      vSqlProdutosJaVendidos := vSqlProdutosJaVendidos + ' and PRO.ATIVO = ''' + situacao + ''' ';

  vSql.SQL.Clear;

  if not somenteProdutosSemVender then begin
    vSql.Add(vSqlProdutosJaVendidos);
    vSql.Add(' union all ');
    vSql.Add(vSqlProdutosSemVender);

    if vSql.Pesquisar([empresaId, qtdeDias]) then begin
      SetLength(Result, vSql.GetQuantidadeRegistros);

      for i := 0 to vSql.GetQuantidadeRegistros -1 do begin
        Result[i].produtoId := vSql.GetInt('PRODUTO_ID');
        Result[i].nomeProduto := vSql.GetString('NOME');
        Result[i].empresaId := vSql.GetInt('EMPRESA_ID');
        Result[i].marcaId := vSql.GetInt('MARCA_ID');
        Result[i].nomeMarca := vSql.GetString('MARCA_NOME');
        Result[i].unidade := vSql.GetString('UNIDADE_VENDA');
        Result[i].qtdeDiasSemVender := vSql.GetInt('QTDE_DIAS_SEM_VENDER');
        Result[i].qtdeEstoque := vSql.GetDouble('ESTOQUE_DISPONIVEL');
        Result[i].custoUnitario := vSql.GetDouble('CUSTO_UNITARIO');
        Result[i].custoTotal := vSql.GetDouble('CUSTO_TOTAL');
        Result[i].ativo := vSql.getString('ATIVO');
        vSql.Next;
      end;
    end;
  end
  else begin
    vSql.Add(vSqlProdutosSemVender);
    if vSql.Pesquisar([empresaId]) then begin
      SetLength(Result, vSql.GetQuantidadeRegistros);

      for i := 0 to vSql.GetQuantidadeRegistros -1 do begin
        Result[i].produtoId := vSql.GetInt('PRODUTO_ID');
        Result[i].nomeProduto := vSql.GetString('NOME');
        Result[i].empresaId := vSql.GetInt('EMPRESA_ID');
        Result[i].marcaId := vSql.GetInt('MARCA_ID');
        Result[i].nomeMarca := vSql.GetString('MARCA_NOME');
        Result[i].unidade := vSql.GetString('UNIDADE_VENDA');
        Result[i].qtdeDiasSemVender := vSql.GetInt('QTDE_DIAS_SEM_VENDER');
        Result[i].qtdeEstoque := vSql.GetDouble('ESTOQUE_DISPONIVEL');
        Result[i].custoUnitario := vSql.GetDouble('CUSTO_UNITARIO');
        Result[i].custoTotal := vSql.GetDouble('CUSTO_TOTAL');
        Result[i].ativo := vSql.getString('ATIVO');
        vSql.Next;
      end;
    end;
  end;

  vSql.Free;
end;

function AtualizarCEST(pConexao: TConexao; pProdutosIds: TArray<Integer>; pValor: string): RecRetornoBD;
var
  vExec: TExecucao;
begin
  pConexao.SetRotina('ATUALIZAR_CUSTO_FIXO');
  Result.TeveErro := False;
  vExec := TExecucao.Create(pConexao);

  vExec.Add('update PRODUTOS set CEST = :P1 where ' + FiltroInInt('PRODUTO_ID', pProdutosIds));

  try
    pConexao.IniciarTransacao;
    vExec.Executar([pValor]);
    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vExec.Free;
end;

function AtualizarNCM(pConexao: TConexao; pProdutosIds: TArray<Integer>; pValor: string): RecRetornoBD;
var
  vExec: TExecucao;
begin
  pConexao.SetRotina('ATUALIZAR_NCM');
  Result.TeveErro := False;
  vExec := TExecucao.Create(pConexao);

  vExec.Add('update PRODUTOS set CODIGO_NCM = :P1 where ' + FiltroInInt('PRODUTO_ID', pProdutosIds));

  try
    pConexao.IniciarTransacao;
    vExec.Executar([pValor]);
    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vExec.Free;
end;

end.
