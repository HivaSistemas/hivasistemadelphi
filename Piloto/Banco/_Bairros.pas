unit _Bairros;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TBairro = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao; pCidadeId: Integer);
  protected
    function GetRecordBairro: RecBairros;
  end;

function AtualizarBairro(
  pConexao: TConexao;
  pBairroId: Integer;
  pNome: string;
  pCidadeId: Integer;
  pRotaId: Integer;
  pAtivo: string
): RecRetornoBD;

function BuscarBairros(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pCidadeId: Integer
): TArray<RecBairros>;

function ExcluirBairro(
  pConexao: TConexao;
  pBairroId: Integer
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TBairro }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 4);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'and BAI.BAIRRO_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome do bairro (Avan�.)',
      True,
      0,
      'and BAI.NOME like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  BAI.NOME ',
      '',
      True
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Cidade do bairro (Avan�.)',
      False,
      1,
      'and CID.NOME like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  CID.NOME ',
      '',
      True
    );

  Result[3] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0 ,
      'and BAI.NOME = :P1 ' +
      'and CID.CODIGO_IBGE = :P2 ' +
      'order by ' +
      '  BAI.NOME '
    );
end;

constructor TBairro.Create(pConexao: TConexao; pCidadeId: Integer);
begin
  inherited Create(pConexao, 'BAIRROS');

  FSql :=
    'select ' +
    '  BAI.BAIRRO_ID, ' +
    '  BAI.NOME, ' +
    '  BAI.CIDADE_ID, ' +
    '  BAI.ROTA_ID, ' +
    '  BAI.ATIVO, ' +
    '  CID.NOME as NOME_CIDADE, ' +
    '  CID.TIPO_BLOQUEIO_VENDA_ENTREGAR, ' +
    '  CID.ESTADO_ID, ' +
    '  CID.CODIGO_IBGE as CODIGO_IBGE_CIDADE, ' +
    '  ROT.NOME as NOME_ROTA ' +
    'from ' +
    '  BAIRROS BAI ' +

    'inner join CIDADES CID ' +
    'on BAI.CIDADE_ID = CID.CIDADE_ID ' +

    'left join ROTAS ROT ' +
    'on BAI.ROTA_ID = ROT.ROTA_ID ';

    if pCidadeId = 0 then
      FSql := FSql + 'where 1 = 1 '
    else
      FSql := FSql + 'where CID.CIDADE_ID = ' + IntToStr(pCidadeId);

  SetFiltros(GetFiltros);

  AddColuna('BAIRRO_ID', True);
  AddColuna('NOME');
  AddColuna('CIDADE_ID');
  AddColuna('ROTA_ID');
  AddColuna('ATIVO');
  AddColunaSL('NOME_CIDADE');
  AddColunaSL('TIPO_BLOQUEIO_VENDA_ENTREGAR');
  AddColunaSL('ESTADO_ID');
  AddColunaSL('CODIGO_IBGE_CIDADE');
  AddColunaSL('NOME_ROTA');
end;

function TBairro.GetRecordBairro: RecBairros;
begin
  Result := RecBairros.Create;
  Result.bairro_id                  := GetInt('BAIRRO_ID', True);
  Result.nome                       := GetString('NOME');
  Result.cidade_id                  := GetInt('CIDADE_ID');
  Result.rota_id                    := GetInt('ROTA_ID');
  Result.nome_cidade                := GetString('NOME_CIDADE');
  Result.ativo                      := GetString('ATIVO');
  Result.TipoBloqueioVendaEntregar  := GetString('TIPO_BLOQUEIO_VENDA_ENTREGAR');
  Result.EstadoId                   := GetString('ESTADO_ID');
  Result.CodigoIbgeCidade           := getInt('CODIGO_IBGE_CIDADE');
  Result.nome_rota                  := GetString('NOME_ROTA');
end;

function AtualizarBairro(
  pConexao: TConexao;
  pBairroId: Integer;
  pNome: string;
  pCidadeId: Integer;
  pRotaId: Integer;
  pAtivo: string
): RecRetornoBD;
var
  t: TBairro;
  novo: Boolean;
  seq: TSequencia;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_BAIRROS');

  t := TBairro.Create(pConexao, 0);

  novo := pBairroId = 0;
  if novo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_BAIRRO_ID');
    pBairroId := seq.GetProximaSequencia;
    result.AsInt := pBairroId;
    seq.Free;
  end;

  t.SetInt('BAIRRO_ID', pBairroId, True);
  t.SetString('NOME', pNOME);
  t.SetInt('CIDADE_ID', pCidadeId);
  t.SetIntN('ROTA_ID', pRotaId);
  t.SetString('ATIVO', pAtivo);

  try
    pConexao.IniciarTransacao;

    if novo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarBairros(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pCidadeId: Integer
): TArray<RecBairros>;
var
  i: Integer;
  t: TBairro;
begin
  Result := nil;
  t := TBairro.Create(pConexao, pCidadeId);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordBairro;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirBairro(
  pConexao: TConexao;
  pBairroId: Integer
): RecRetornoBD;
var
  t: TBairro;
begin
  Result.TeveErro := False;
  t := TBairro.Create(pConexao, 0);

  t.SetInt('BAIRRO_ID', pBairroId, True);

  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
