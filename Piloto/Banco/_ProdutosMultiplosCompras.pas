unit _ProdutosMultiplosCompras;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TProdutoMultiploCompra = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordProdutoMultiploCompra: RecProdutoMultiploCompra;
  end;

function BuscarProdutoMultiploCompras(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecProdutoMultiploCompra>;

function BuscarProdutoMultiploComprasComando(pConexao: TConexao; pComando: string): TArray<RecProdutoMultiploCompra>;

function ExcluirProdutoMultiploCompra(
  pConexao: TConexao;
  pUnidadeId: string;
  pMultiplo: Double
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TProdutoMultiploCompra }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where PRODUTO_ID = :P1 ' +
      'order by ' +
      '  ORDEM '
    );
end;

constructor TProdutoMultiploCompra.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PRODUTOS_MULTIPLOS_COMPRA');

  FSql :=
    'select ' +
    '  PRODUTO_ID, ' +
    '  UNIDADE_ID, ' +
    '  MULTIPLO, ' +
    '  QUANTIDADE_EMBALAGEM, ' +
    '  ORDEM ' +
    'from ' +
    '  PRODUTOS_MULTIPLOS_COMPRA ';

  setFiltros(getFiltros);

  AddColuna('PRODUTO_ID', True);
  AddColuna('UNIDADE_ID', True);
  AddColuna('MULTIPLO', True);
  AddColuna('QUANTIDADE_EMBALAGEM', True);
  AddColuna('ORDEM');
end;

function TProdutoMultiploCompra.getRecordProdutoMultiploCompra: RecProdutoMultiploCompra;
begin
  Result.ProdutoId := getInt('PRODUTO_ID', True);
  Result.UnidadeId := getString('UNIDADE_ID', True);
  Result.Multiplo  := getDouble('MULTIPLO', True);
  Result.QuantidadeEmbalagem := getDouble('QUANTIDADE_EMBALAGEM', True);
  Result.Ordem     := getInt('ORDEM');
end;

function BuscarProdutoMultiploCompras(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecProdutoMultiploCompra>;
var
  i: Integer;
  t: TProdutoMultiploCompra;
begin
  Result := nil;
  t := TProdutoMultiploCompra.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordProdutoMultiploCompra;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarProdutoMultiploComprasComando(pConexao: TConexao; pComando: string): TArray<RecProdutoMultiploCompra>;
var
  i: Integer;
  t: TProdutoMultiploCompra;
begin
  Result := nil;
  t := TProdutoMultiploCompra.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordProdutoMultiploCompra;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirProdutoMultiploCompra(
  pConexao: TConexao;
  pUnidadeId: string;
  pMultiplo: Double
): RecRetornoBD;
var
  t: TProdutoMultiploCompra;
begin
  Result.TeveErro := False;
  t := TProdutoMultiploCompra.Create(pConexao);

  try
    t.setString('UNIDADE_ID', pUnidadeId, True);
    t.setDouble('MULTIPLO', pMultiplo, True);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
