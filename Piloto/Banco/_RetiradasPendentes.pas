unit _RetiradasPendentes;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, _RetiradasItens, System.Variants, System.StrUtils, _RetiradasItensPendentes;

{$M+}
type
  RecRetiradasPendentes = record
    OrcamentoId: Integer;
    LocalId: Integer;
    NomeLocal: string;
    EmpresaId: Integer;
    NomeEmpresa: string;
    ClienteId: Integer;
    NomeCliente: string;
    VendedorId: Integer;
    NomeVendedor: string;
    DataCadastro: TDateTime;
    ObservacoesExpedicao: string;
  end;

  TRetiradasPendentes = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordRetiradaPendente: RecRetiradasPendentes;
  end;

function BuscarRetiradas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecRetiradasPendentes>;

function BuscarRetiradasComando(pConexao: TConexao; pComando: string): TArray<RecRetiradasPendentes>;

function GerarRetiradaRetiraPendente(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pEmpresaPendenciaId: Integer;
  pLocalId: Integer;
  pTipoNotaGerar: string;
  pProdutos: TArray<RecRetiradasItensPendentes>
): RecRetornoBD;

function GerarEntregaPendenteRetira(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pEmpresaPendenciaId: Integer;
  pLocalId: Integer;
  pProdutos: TArray<RecRetiradasItensPendentes>;
  pDefinirLocalManual: string
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

uses
  _OrcamentosItensDefLocais;

{ TRetiradasPendentes }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ORC.EMPRESA_ID = :P1 ' +
      'RET.RETIRADA_ID = :P1 '
    );

end;

constructor TRetiradasPendentes.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'RETIRADAS');

  FSql :=
    'select ' +
    '  RET.ORCAMENTO_ID, ' +
    '  RET.LOCAL_ID, ' +
    '  LOC.NOME as NOME_LOCAL, ' +
    '  RET.EMPRESA_ID, ' +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA, ' +
    '  ORC.CLIENTE_ID, ' +
    '  case when ORC.CLIENTE_ID = PAR.CADASTRO_CONSUMIDOR_FINAL_ID then ORC.NOME_CONSUMIDOR_FINAL else CAD.NOME_FANTASIA end as NOME_CLIENTE, ' +
    '  ORC.VENDEDOR_ID, ' +
    '  nvl(FUN.APELIDO, FUN.NOME) as NOME_VENDEDOR, ' +
    '  ORC.DATA_CADASTRO, ' +
    '  ORC.OBSERVACOES_EXPEDICAO ' +
    'from ' +
    '  RETIRADAS_PENDENTES RET ' +

    'inner join ORCAMENTOS ORC ' +
    'on RET.ORCAMENTO_ID = ORC.ORCAMENTO_ID ' +

    'inner join CADASTROS CAD ' +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID ' +

    'inner join FUNCIONARIOS FUN ' +
    'on ORC.VENDEDOR_ID = FUN.FUNCIONARIO_ID ' +

    'inner join LOCAIS_PRODUTOS LOC ' +
    'on RET.LOCAL_ID = LOC.LOCAL_ID ' +

    'inner join EMPRESAS EMP ' +
    'on RET.EMPRESA_ID = EMP.EMPRESA_ID ' +

    'cross join PARAMETROS PAR ' +

    'where ORC.STATUS in(''RE'', ''VE'') ';

  setFiltros(getFiltros);

  AddColunaSL('ORCAMENTO_ID');
  AddColunaSL('LOCAL_ID');
  AddColunaSL('NOME_LOCAL');
  AddColunaSL('EMPRESA_ID');
  AddColunaSL('NOME_EMPRESA');
  AddColunaSL('CLIENTE_ID');
  AddColunaSL('NOME_CLIENTE');
  AddColunaSL('VENDEDOR_ID');
  AddColunaSL('NOME_VENDEDOR');
  AddColunaSL('DATA_CADASTRO');
  AddColunaSL('OBSERVACOES_EXPEDICAO');
end;

function TRetiradasPendentes.getRecordRetiradaPendente: RecRetiradasPendentes;
begin
  Result.OrcamentoId            := getInt('ORCAMENTO_ID');
  Result.LocalId                := getInt('LOCAL_ID');
  Result.NomeLocal              := getString('NOME_LOCAL');
  Result.EmpresaId              := getInt('EMPRESA_ID');
  Result.NomeEmpresa            := getString('NOME_EMPRESA');
  Result.ClienteId              := getInt('CLIENTE_ID');
  Result.NomeCliente            := getString('NOME_CLIENTE');
  Result.VendedorId             := getInt('VENDEDOR_ID');
  Result.NomeVendedor           := getString('NOME_VENDEDOR');
  Result.DataCadastro           := getData('DATA_CADASTRO');
  Result.ObservacoesExpedicao   := getString('OBSERVACOES_EXPEDICAO');
end;

function BuscarRetiradas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecRetiradasPendentes>;
var
  i: Integer;
  t: TRetiradasPendentes;
begin
  Result := nil;
  t := TRetiradasPendentes.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordRetiradaPendente;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarRetiradasComando(pConexao: TConexao; pComando: string): TArray<RecRetiradasPendentes>;
var
  i: Integer;
  t: TRetiradasPendentes;
begin
  Result := nil;
  t := TRetiradasPendentes.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordRetiradaPendente;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function GerarRetiradaRetiraPendente(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pEmpresaPendenciaId: Integer;
  pLocalId: Integer;
  pTipoNotaGerar: string;
  pProdutos: TArray<RecRetiradasItensPendentes>
): RecRetornoBD;
var
  i: Integer;
  vProc: TProcedimentoBanco;

  vProdutoIds: array of Integer;
  vItensIds: array of Integer;
  vLocaisIds: array of Integer;
  vQuantidades: array of Double;
  vLotes: array of string;
begin
  Result.TeveErro := False;
  pConexao.SetRotina('GERAR_RET_RETIRA_PEND');
  vProc := TProcedimentoBanco.Create(pConexao, 'GERAR_RETIRADA_RETIRA_PENDENTE');

  try

    SetLength(vProdutoIds, Length(pProdutos));
    SetLength(vItensIds, Length(pProdutos));
    SetLength(vLocaisIds, Length(pProdutos));
    SetLength(vQuantidades, Length(pProdutos));
    SetLength(vLotes, Length(pProdutos));

    for i := Low(pProdutos) to High(pProdutos) do begin
      vProdutoIds[i]  := pProdutos[i].ProdutoId;
      vItensIds[i]    := pProdutos[i].ItemId;
      vLocaisIds[i]   := pProdutos[i].LocalId;
      vQuantidades[i] := pProdutos[i].Quantidade;
      vLotes[i]       := pProdutos[i].Lote;
    end;

    vProc.Params[0].AsInteger := pOrcamentoId;
    vProc.Params[1].AsInteger := pEmpresaPendenciaId;
    vProc.Params[2].AsInteger := pLocalId;
    vProc.Params[3].AsString  := pTipoNotaGerar;
    vProc.Params[4].Value     := null;

    (* Produtos *)
    vProc.Params[5].Value := vProdutoIds;
    vProc.Params[6].Value := vItensIds;
    vProc.Params[7].Value := vLocaisIds;
    vProc.Params[8].Value := vQuantidades;
    vProc.Params[9].Value := vLotes;

    vProc.Executar;

    Result.AsInt := vProc.ParamByName('oRETIRADA_ID').AsInteger;
  except
    on e: Exception do
      Result.TratarErro(e);
  end;

  vProc.Free;
end;

function GerarEntregaPendenteRetira(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pEmpresaPendenciaId: Integer;
  pLocalId: Integer;
  pProdutos: TArray<RecRetiradasItensPendentes>;
  pDefinirLocalManual: string
): RecRetornoBD;
var
  i: Integer;
  vProc: TProcedimentoBanco;
  vSql: TConsulta;
  vItensDefLocais: TOrcamentosItensDefLocais;

  vProdutoIds: array of Integer;
  vItensIds: array of Integer;
  vLocaisIds: array of Integer;
  vQuantidades: array of Double;
  vLotes: array of string;
begin
  Result.TeveErro := False;
  pConexao.SetRotina('GERAR_ENT_PEND_RETIRA');
  vProc := TProcedimentoBanco.Create(pConexao, 'GERAR_ENTREGA_PENDENTE_RETIRA');
  vSql := TConsulta.Create(pConexao);
  vItensDefLocais := TOrcamentosItensDefLocais.Create(pConexao);

  try
    vSql.SQL.Add('select ');
    vSql.SQL.Add('  MAX(ITEM_ID) as ITEM_ID ');
    vSql.SQL.Add('from ');
    vSql.SQL.Add('  ORCAMENTOS_ITENS_DEF_LOCAIS ');
    vSql.SQL.Add('where ORCAMENTO_ID = :P1 ');

    SetLength(vProdutoIds, Length(pProdutos));
    SetLength(vItensIds, Length(pProdutos));
    SetLength(vLocaisIds, Length(pProdutos));
    SetLength(vQuantidades, Length(pProdutos));
    SetLength(vLotes, Length(pProdutos));

    for i := Low(pProdutos) to High(pProdutos) do begin
      vProdutoIds[i]  := pProdutos[i].ProdutoId;
      vItensIds[i]    := pProdutos[i].ItemId;
      vLocaisIds[i]   := pProdutos[i].LocalId;
      vQuantidades[i] := pProdutos[i].Quantidade;
      vLotes[i]       := pProdutos[i].Lote;

      if pDefinirLocalManual = 'S' then begin
        vSql.Pesquisar([pOrcamentoId]);

        vItensDefLocais.SetInt('ORCAMENTO_ID', pOrcamentoId, True);
        vItensDefLocais.SetInt('EMPRESA_ID', pEmpresaPendenciaId, True);
        vItensDefLocais.SetInt('PRODUTO_ID', pProdutos[i].ProdutoId, True);
        vItensDefLocais.SetInt('LOCAL_ID', pProdutos[i].LocalId, True);
        vItensDefLocais.setDouble('QUANTIDADE_ENTREGAR', pProdutos[i].Quantidade);
        vItensDefLocais.setDouble('QUANTIDADE_RETIRAR', 0.0);
        vItensDefLocais.setDouble('QUANTIDADE_ATO', 0.0);
        vItensDefLocais.setInt('ITEM_ID', vSql.GetInt('ITEM_ID') + 1);
        vItensDefLocais.setString('STATUS', 'PEN');
        vItensDefLocais.setString('LOTE', pProdutos[i].Lote);

        vItensDefLocais.Inserir;
      end;

    end;

    vProc.Params[0].AsInteger := pOrcamentoId;
    vProc.Params[1].AsInteger := pEmpresaPendenciaId;
    vProc.Params[2].AsInteger := pLocalId;
    vProc.Params[3].AsDateTime := Date;

    (* Produtos *)
    vProc.Params[4].Value := vProdutoIds;
    vProc.Params[5].Value := vItensIds;
    vProc.Params[6].Value := vLocaisIds;
    vProc.Params[7].Value := vQuantidades;
    vProc.Params[8].Value := vLotes;

    vProc.Executar;
  except
    on e: Exception do
      Result.TratarErro(e);
  end;

  vProc.Free;
  vSql.Free;
  vItensDefLocais.Free;
end;

end.
