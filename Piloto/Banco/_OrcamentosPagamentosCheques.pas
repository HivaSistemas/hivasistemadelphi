unit _OrcamentosPagamentosCheques;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsFinanceiros;

{$M+}
type
  TOrcamentosPagamentosCheques = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordOrcamentosPagamentosCheques: RecTitulosFinanceiros;
  end;

function AtualizarOrcamentosPagamentosCheques(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pCobrancaId: Integer;
  pItemId: Integer;
  pAgencia: Integer;
  pNumeroCheque: Integer;
  pBanco: Integer;
  pValorCheque: Double;
  pContaCorrente: string;
  pNomeEmitente: string;
  pCpfCnpjEmitente: string;
  pTelefoneEmitente: string;
  pDataVencimento: TDateTime
): RecRetornoBD;

function BuscarOrcamentosPagamentosCheques(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;

function ExcluirOrcamentosPagamentosCheques(
  pConexao: TConexao
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TOrcamentosPagamentosCheques }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ORCAMENTO_ID = :P1'
    );
end;

constructor TOrcamentosPagamentosCheques.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ORCAMENTOS_PAGAMENTOS_CHEQUES');

  FSql :=
    'select ' +
    '  ORCAMENTO_ID, ' +
    '  COBRANCA_ID, ' +
    '  ITEM_ID, ' +
    '  AGENCIA, ' +
    '  NUMERO_CHEQUE, ' +
    '  BANCO, ' +
    '  VALOR_CHEQUE, ' +
    '  CONTA_CORRENTE, ' +
    '  NOME_EMITENTE, ' +
    '  CPF_CNPJ_EMITENTE, ' +
    '  TELEFONE_EMITENTE, ' +
    '  DATA_VENCIMENTO, ' +
    '  PARCELA, ' +
    '  NUMERO_PARCELAS ' +
    'from ' +
    '  ORCAMENTOS_PAGAMENTOS_CHEQUES';

  setFiltros(getFiltros);

  AddColuna('ORCAMENTO_ID', True);
  AddColuna('COBRANCA_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('AGENCIA');
  AddColuna('NUMERO_CHEQUE');
  AddColuna('BANCO');
  AddColuna('VALOR_CHEQUE');
  AddColuna('CONTA_CORRENTE');
  AddColuna('NOME_EMITENTE');
  AddColuna('CPF_CNPJ_EMITENTE');
  AddColuna('TELEFONE_EMITENTE');
  AddColuna('DATA_VENCIMENTO');
  AddColuna('PARCELA');
  AddColuna('NUMERO_PARCELAS');
end;

function TOrcamentosPagamentosCheques.getRecordOrcamentosPagamentosCheques: RecTitulosFinanceiros;
begin
  Result.Id                := getInt('ORCAMENTO_ID', True);
  Result.CobrancaId        := getInt('COBRANCA_ID', True);
  Result.ItemId            := getInt('ITEM_ID', True);
  Result.agencia           := getString('AGENCIA');
  Result.NumeroCheque      := getInt('NUMERO_CHEQUE');
  Result.banco             := getString('BANCO');
  Result.Valor             := getDouble('VALOR_CHEQUE');
  Result.ContaCorrente     := getString('CONTA_CORRENTE');
  Result.NomeEmitente      := getString('NOME_EMITENTE');
  Result.CpfCnpjEmitente   := getString('CPF_CNPJ_EMITENTE');
  Result.TelefoneEmitente  := getString('TELEFONE_EMITENTE');
  Result.DataVencimento    := getData('DATA_VENCIMENTO');
  Result.parcela           := getInt('PARCELA');
  Result.NumeroParcelas    := getInt('NUMERO_PARCELAS');
end;

function AtualizarOrcamentosPagamentosCheques(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pCobrancaId: Integer;
  pItemId: Integer;
  pAgencia: Integer;
  pNumeroCheque: Integer;
  pBanco: Integer;
  pValorCheque: Double;
  pContaCorrente: string;
  pNomeEmitente: string;
  pCpfCnpjEmitente: string;
  pTelefoneEmitente: string;
  pDataVencimento: TDateTime
): RecRetornoBD;
var
  t: TOrcamentosPagamentosCheques;
begin
  Result.Iniciar;
  t := TOrcamentosPagamentosCheques.Create(pConexao);

  t.setInt('ORCAMENTO_ID', pOrcamentoId, True);
  t.setInt('COBRANCA_ID', pCobrancaId, True);
  t.setInt('ITEM_ID', pItemId, True);
  t.setInt('AGENCIA', pAgencia);
  t.setInt('NUMERO_CHEQUE', pNumeroCheque);
  t.setInt('BANCO', pBanco);
  t.setDouble('VALOR_CHEQUE', pValorCheque);
  t.setString('CONTA_CORRENTE', pContaCorrente);
  t.setString('NOME_EMITENTE', pNomeEmitente);
  t.setString('CPF_CNPJ_EMITENTE', pCpfCnpjEmitente);
  t.setString('TELEFONE_EMITENTE', pTelefoneEmitente);
  t.setData('DATA_VENCIMENTO', pDataVencimento);

  try
    t.Inserir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  t.Free;
end;

function BuscarOrcamentosPagamentosCheques(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;
var
  i: Integer;
  t: TOrcamentosPagamentosCheques;
begin
  Result := nil;
  t := TOrcamentosPagamentosCheques.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordOrcamentosPagamentosCheques;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirOrcamentosPagamentosCheques(
  pConexao: TConexao
): RecRetornoBD;
var
  t: TOrcamentosPagamentosCheques;
begin
  Result.TeveErro := False;
  t := TOrcamentosPagamentosCheques.Create(pConexao);


  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
