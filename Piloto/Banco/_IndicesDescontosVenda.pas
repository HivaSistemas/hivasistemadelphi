unit _IndicesDescontosVenda;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecIndicesDescontosVenda = class
  public
    IndiceId: Integer;
    Descricao: string;
    PrecoCusto: string;
    PercentualDesconto: Integer;
    Ativo: string;
    TipoCusto: string;
    TipoDescontoPrecoCusto: string;
    TipoDescontoPrecoCustoAnalitico: string;
  end;

  TIndicesDescontosVenda = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordIndicesDescontosVenda: RecIndicesDescontosVenda;
  end;

function AtualizarIndicesDescontosVenda(
  pConexao: TConexao;
  pIndiceId: Integer;
  pDescricao: string;
  pPrecoCusto: string;
  pPercentualDesconto: Integer;
  pAtivo: string;
  pTipoCusto: string;
  pTipoDescontoPrecoCusto: string
): RecRetornoBD;

function BuscarIndicesDescontosVenda(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecIndicesDescontosVenda>;

function ExcluirIndicesDescontosVenda(
  pConexao: TConexao;
  pIndiceId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TIndicesDescontosVenda }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where INDICE_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Descri��o',
      True,
      0,
      'where DESCRICAO like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  DESCRICAO '
    );
end;

constructor TIndicesDescontosVenda.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'INDICES_DESCONTOS_VENDA');

  FSql := 
    'select ' +
    '  INDICE_ID, ' +
    '  DESCRICAO, ' +
    '  PRECO_CUSTO, ' +
    '  PERCENTUAL_DESCONTO, ' +
    '  ATIVO, ' +
    '  TIPO_CUSTO, ' +
    '  TIPO_DESCONTO_PRECO_CUSTO ' +
    'from ' +
    '  INDICES_DESCONTOS_VENDA ';

  setFiltros(getFiltros);

  AddColuna('INDICE_ID', True);
  AddColuna('DESCRICAO');
  AddColuna('PRECO_CUSTO');
  AddColuna('PERCENTUAL_DESCONTO');
  AddColuna('ATIVO');
  AddColuna('TIPO_CUSTO');
  AddColuna('TIPO_DESCONTO_PRECO_CUSTO');
end;

function TIndicesDescontosVenda.getRecordIndicesDescontosVenda: RecIndicesDescontosVenda;
begin
  Result := RecIndicesDescontosVenda.Create;

  Result.IndiceId               := getInt('INDICE_ID', True);
  Result.Descricao              := getString('DESCRICAO');
  Result.PrecoCusto             := getString('PRECO_CUSTO');
  Result.PercentualDesconto     := getInt('PERCENTUAL_DESCONTO');
  Result.Ativo                  := getString('ATIVO');
  Result.TipoCusto              := getString('TIPO_CUSTO');
  Result.TipoDescontoPrecoCusto := getString('TIPO_DESCONTO_PRECO_CUSTO');

  Result.TipoDescontoPrecoCustoAnalitico :=
    _Biblioteca.Decode(
      Result.TipoDescontoPrecoCusto,[
        'N', 'Nenhum',
        'D', 'Diminuir',
        'A', 'Aumentar']
      );
end;

function AtualizarIndicesDescontosVenda(
  pConexao: TConexao;
  pIndiceId: Integer;
  pDescricao: string;
  pPrecoCusto: string;
  pPercentualDesconto: Integer;
  pAtivo: string;
  pTipoCusto: string;
  pTipoDescontoPrecoCusto: string
): RecRetornoBD;
var
  t: TIndicesDescontosVenda;
  vNovo: Boolean;
  vSeq: TSequencia;
begin
  Result.Iniciar;
  pConexao.setRotina('ATUALIZAR_IND_DESC_VENDA');

  t := TIndicesDescontosVenda.Create(pConexao);

  vNovo := pIndiceId = 0;
  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_INDICES_DESCONTOS_VENDA');
    pIndiceId := vSeq.getProximaSequencia;
    Result.AsInt := pIndiceId;
    vSeq.Free;
  end;

  try
    pConexao.IniciarTransacao; 

    t.setInt('INDICE_ID', pIndiceId, True);
    t.setString('DESCRICAO', pDescricao);
    t.setString('PRECO_CUSTO', pPrecoCusto);
    t.setInt('PERCENTUAL_DESCONTO', pPercentualDesconto);
    t.setString('ATIVO', pAtivo);
    t.setString('TIPO_CUSTO', pTipoCusto);
    t.setString('TIPO_DESCONTO_PRECO_CUSTO', pTipoDescontoPrecoCusto);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao; 
  except
    on e: Exception do begin
      pConexao.VoltarTransacao; 
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarIndicesDescontosVenda(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecIndicesDescontosVenda>;
var
  i: Integer;
  t: TIndicesDescontosVenda;
begin
  Result := nil;
  t := TIndicesDescontosVenda.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordIndicesDescontosVenda;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirIndicesDescontosVenda(
  pConexao: TConexao;
  pIndiceId: Integer
): RecRetornoBD;
var
  t: TIndicesDescontosVenda;
begin
  Result.Iniciar;
  t := TIndicesDescontosVenda.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('INDICE_ID', pIndiceId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
