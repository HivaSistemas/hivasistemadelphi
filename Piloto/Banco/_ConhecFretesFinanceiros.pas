unit _ConhecFretesFinanceiros;

interface

uses
  _OperacoesBancoDados, _RecordsEspeciais, _Conexao, System.SysUtils, System.Variants, _BibliotecaGenerica, _RecordsFinanceiros;

{$M+}
type
  TConhecFretesFinanceiros = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordConhecimentoFinanc: RecTitulosFinanceiros;
  end;

function BuscarItens(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TConhecFretesFinanceiros }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 3);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CONHECIMENTO_ID = :P1'
    );
end;

constructor TConhecFretesFinanceiros.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONHEC_FRETES_FINANCEIROS');

  FSql :=
    'select ' +
    '  CONHECIMENTO_ID, ' +
    '  ITEM_ID, ' +
    '  COBRANCA_ID, ' +
    '  DOCUMENTO, ' +
    '  DATA_VENCIMENTO, ' +
    '  VALOR, ' +
    '  PARCELA, ' +
    '  NUMERO_PARCELAS, ' +
    '  NOSSO_NUMERO, ' +
    '  CODIGO_BARRAS ' +
    'from ' +
    '  CONHEC_FRETES_FINANCEIROS ';

  SetFiltros(GetFiltros);

  AddColuna('CONHECIMENTO_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('COBRANCA_ID');
  AddColuna('DOCUMENTO');
  AddColuna('DATA_VENCIMENTO');
  AddColuna('VALOR');
  AddColuna('PARCELA');
  AddColuna('NUMERO_PARCELAS');
  AddColuna('NOSSO_NUMERO');
  AddColuna('CODIGO_BARRAS');
end;

function TConhecFretesFinanceiros.GetRecordConhecimentoFinanc: RecTitulosFinanceiros;
begin
  Result.Id             := GetInt('CONHECIMENTO_ID', True);
  Result.ItemId         := GetInt('ITEM_ID', True);
  Result.CobrancaId     := GetInt('COBRANCA_ID');
  Result.Documento      := GetString('DOCUMENTO');
  Result.DataVencimento := GetData('DATA_VENCIMENTO');
  Result.Valor          := getDouble('VALOR');
  Result.Parcela        := GetInt('PARCELA');
  Result.NumeroParcelas := GetInt('NUMERO_PARCELAS');
  Result.NossoNumero    := GetString('NOSSO_NUMERO');
  Result.CodigoBarras   := GetString('CODIGO_BARRAS');
end;

function BuscarItens(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;
var
  i: Integer;
  t: TConhecFretesFinanceiros;
begin
  Result := nil;
  t := TConhecFretesFinanceiros.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordConhecimentoFinanc;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
