unit _MovimentosTurnos;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCaixa,
  System.Variants, System.StrUtils;

{$M+}
type
  RecMovimentoSangria = record
    sangriaId: Integer;
    turnoId: Integer;
    contaId: Integer;
    funcionarioId: Integer;
    valorDinheiro: Double;
    valorCobranca: Double;
    valorCheque: Double;
    valorCartao: Double;
    observacoes: string;
  end;

  RecMovimentosTurnos = record
    TurnoId: Integer;
    Id: Integer;
    ContaId: string;
    NomeContaOrigem: string;
    UsuarioMovimentoId: Integer;
    ValorDinheiro: Double;
    ValorCheque: Double;
    ValorCredito: Double;
    ValorPix: Double;
    ValorCartao: Double;
    ValorCobranca: Double;
    ValorAcumulado: Double;
    DataHoraMovimento: TDateTime;
    TipoMovimento: string;
    TipoMovimentoAnalitico: string;
    Funcionario: string;
    Observacao: string;
  end;

  TMovimentosTurnos = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordMovimentoBancosCaixas: RecMovimentosTurnos;
  end;

function AtualizarMovimentosTurno(
  pConexao: TConexao;
  pMovimentoId: Integer;
  pTurnoId: Integer;
  pContaId: string;
  pValorDinheiro: Double;
  pValorCheque: Double;
  pValorCredito: Double;
  pValorCartao: Double;
  pValorCobranca: Double;
  pTipoMovimento: string;
  pObservacao: string
): RecRetornoBD;

function BuscarMovimentosTurnos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecMovimentosTurnos>;

function BuscarMovimentosTurnosComando(pConexao: TConexao; pComando: string): TArray<RecMovimentosTurnos>;
function ExisteMovimentoFechamentoTurno(pConexao: TConexao; const pTurnoId: Integer): Boolean;
function BuscarMovimentoSangria(pConexao: TConexao; const pSangriaId: Integer): TArray<RecMovimentoSangria>;
function BuscarValorDinheiroTurno(pConexao: TConexao; const pTurnoId: Integer): Double;
function ExcluirSangria(pConexao: TConexao; pMovimentoId: Integer; pTurnoId: Integer): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TMovimentosTurnos }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where MOV.TURNO_ID = :P1 ' +
      'order by ' +
      '  MOV.DATA_HORA_MOVIMENTO '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where MOV.MOVIMENTO_TURNO_ID = :P1 '
    );
end;

constructor TMovimentosTurnos.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'MOVIMENTOS_TURNOS');

  FSql :=
    'select ' +
    '  MOV.MOVIMENTO_TURNO_ID, ' +
    '  MOV.TURNO_ID, ' +
    '  MOV.ID, ' +
    '  MOV.CONTA_ID, ' +
    '  CON.NOME as NOME_CONTA_ORIGEM, ' +
    '  MOV.USUARIO_MOVIMENTO_ID, ' +
    '  MOV.VALOR_DINHEIRO, ' +
    '  MOV.VALOR_PIX, ' +
    '  MOV.VALOR_CHEQUE, ' +
    '  MOV.VALOR_CREDITO, ' +
    '  MOV.VALOR_CARTAO, ' +
    '  MOV.VALOR_COBRANCA, ' +
    '  MOV.VALOR_ACUMULADO, ' +
    '  MOV.DATA_HORA_MOVIMENTO, ' +
    '  MOV.TIPO_MOVIMENTO, ' +
    '  nvl(FUN.APELIDO, FUN.NOME) as FUNCIONARIO, ' +
    '  MOV.OBSERVACAO ' +
    'from ' +
    '  VW_MOVIMENTOS_TURNOS MOV ' +

    'left join CONTAS CON ' +
    'on MOV.CONTA_ID = CON.CONTA ' +

    'inner join FUNCIONARIOS FUN ' +
    'on MOV.USUARIO_MOVIMENTO_ID = FUN.FUNCIONARIO_ID ';

  setFiltros(getFiltros);

  AddColuna('MOVIMENTO_TURNO_ID', True);
  AddColuna('TURNO_ID');
  AddColuna('CONTA_ID');
  AddColuna('TIPO_MOVIMENTO');
  AddColuna('VALOR_DINHEIRO');
  AddColuna('VALOR_PIX');
  AddColuna('VALOR_CHEQUE');
  AddColuna('VALOR_CARTAO');
  AddColuna('VALOR_COBRANCA');
  AddColuna('OBSERVACAO');

  AddColunaSL('VALOR_ACUMULADO');
  AddColunaSL('DATA_HORA_MOVIMENTO');
  AddColunaSL('ID');
  AddColunaSL('NOME_CONTA_ORIGEM');
  AddColunaSL('USUARIO_MOVIMENTO_ID');
  AddColunaSL('VALOR_CREDITO');
  AddColunaSL('FUNCIONARIO');
end;

function TMovimentosTurnos.getRecordMovimentoBancosCaixas: RecMovimentosTurnos;
begin
  Result.TurnoId            := getInt('TURNO_ID');
  Result.Id                 := getInt('ID');
  Result.ContaId            := getString('CONTA_ID');
  Result.NomeContaOrigem    := getString('NOME_CONTA_ORIGEM');
  Result.UsuarioMovimentoId := getInt('USUARIO_MOVIMENTO_ID');
  Result.ValorDinheiro      := getDouble('VALOR_DINHEIRO');
  Result.ValorPix           := getDouble('VALOR_PIX');
  Result.ValorCheque        := getDouble('VALOR_CHEQUE');
  Result.ValorCredito       := getDouble('VALOR_CREDITO');
  Result.ValorCartao        := getDouble('VALOR_CARTAO');
  Result.ValorCobranca      := getDouble('VALOR_COBRANCA');
  Result.ValorAcumulado     := getDouble('VALOR_ACUMULADO');
  Result.DataHoraMovimento  := getData('DATA_HORA_MOVIMENTO');
  Result.Observacao         := getString('OBSERVACAO');
  Result.Funcionario        := getString('FUNCIONARIO');

  Result.TipoMovimento      := getString('TIPO_MOVIMENTO');
  Result.TipoMovimentoAnalitico :=
    _Biblioteca.Decode(
      Result.TipoMovimento,[
        'ABE', 'Abertura',
        'SUP', 'Suprimento',
        'SAN', 'Sangria',
        'FEC', 'Fechamento',
        'REV', 'Recebimento de venda',
        'ACU', 'Recebimento de acumulados',
        'BXR', 'Baixa contas receber',
        'BXP', 'Baixa contas pagar'
      ]
    );

  Result.Funcionario        := getString('FUNCIONARIO');
end;

function AtualizarMovimentosTurno(
  pConexao: TConexao;
  pMovimentoId: Integer;
  pTurnoId: Integer;
  pContaId: string;
  pValorDinheiro: Double;
  pValorCheque: Double;
  pValorCredito: Double;
  pValorCartao: Double;
  pValorCobranca: Double;
  pTipoMovimento: string;
  pObservacao: string
): RecRetornoBD;
var
  novo: Boolean;
  seq: TSequencia;
  t: TMovimentosTurnos;
begin
  Result.TeveErro := False;
  t := TMovimentosTurnos.Create(pConexao);
  pConexao.SetRotina('ATU_MOV_TURNOS');

  novo := pMovimentoId = 0;
  if novo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_MOVIMENTO_TURNO_ID');
    pMovimentoId := seq.getProximaSequencia;
    Result.AsInt := pMovimentoId;
    seq.Free;
  end;

  t.setInt('MOVIMENTO_TURNO_ID', pMovimentoId, True);
  t.setIntN('TURNO_ID', pTurnoId);
  t.setStringN('CONTA_ID', pContaId);
  t.setDouble('VALOR_DINHEIRO', pValorDinheiro);
  t.setDouble('VALOR_PIX', 0);
  t.setDouble('VALOR_CHEQUE', pValorCheque);
  t.setDouble('VALOR_CARTAO', pValorCartao);
  t.setDouble('VALOR_COBRANCA', pValorCobranca);
  t.setString('TIPO_MOVIMENTO', pTipoMovimento);
  t.setStringN('OBSERVACAO', pObservacao);

  try
    if novo then
      t.Inserir
    else
      t.Atualizar;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function BuscarMovimentosTurnos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecMovimentosTurnos>;
var
  i: Integer;
  t: TMovimentosTurnos;
begin
  Result := nil;
  t := TMovimentosTurnos.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordMovimentoBancosCaixas;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarMovimentosTurnosComando(pConexao: TConexao; pComando: string): TArray<RecMovimentosTurnos>;
var
  i: Integer;
  t: TMovimentosTurnos;
begin
  Result := nil;
  t := TMovimentosTurnos.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordMovimentoBancosCaixas;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExisteMovimentoFechamentoTurno(pConexao: TConexao; const pTurnoId: Integer): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select count(*) ');
  vSql.SQL.Add('from MOVIMENTOS_TURNOS ');
  vSql.SQL.Add('where TIPO_MOVIMENTO = ''FEC'' ');
  vSql.SQL.Add('and TURNO_ID = :P1 ');
  vSql.Pesquisar([pTurnoId]);

  Result := vSql.GetInt(0) > 0;

  vSql.Active := False;
  vSql.Free;
end;

function BuscarMovimentoSangria(pConexao: TConexao; const pSangriaId: Integer): TArray<RecMovimentoSangria>;
var
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  MOV.MOVIMENTO_TURNO_ID, ');
  vSql.SQL.Add('  MOV.TURNO_ID, ');
  vSql.SQL.Add('  MOV.CONTA_ID, ');
  vSql.SQL.Add('  MOV.VALOR_DINHEIRO, ');
  vSql.SQL.Add('  MOV.VALOR_CHEQUE, ');
  vSql.SQL.Add('  MOV.VALOR_CARTAO, ');
  vSql.SQL.Add('  MOV.VALOR_COBRANCA, ');
  vSql.SQL.Add('  MOV.OBSERVACAO, ');
  vSql.SQL.Add('  TUR.FUNCIONARIO_ID ');
  vSql.SQL.Add('from MOVIMENTOS_TURNOS MOV ');

  vSql.SQL.Add('inner join TURNOS_CAIXA TUR ');
  vSql.SQL.Add('on MOV.TURNO_ID = TUR.TURNO_ID ');
  vSql.SQL.Add('and TUR.STATUS = ''A''  ');
  vSql.SQL.Add('and MOV.TIPO_MOVIMENTO = ''SAN''  ');

  vSql.SQL.Add('where MOV.MOVIMENTO_TURNO_ID = :P1 ');
  if vSql.Pesquisar([pSangriaId]) then begin
    SetLength(Result, Length(Result) + 1);
    Result[0].sangriaId := vSql.GetInt('MOVIMENTO_TURNO_ID');
    Result[0].turnoId := vSql.GetInt('TURNO_ID');
    Result[0].contaId := vSql.GetInt('CONTA_ID');
    Result[0].funcionarioId := vSql.GetInt('FUNCIONARIO_ID');
    Result[0].valorDinheiro := vSql.GetDouble('VALOR_DINHEIRO');
    Result[0].valorCheque := vSql.GetDouble('VALOR_CHEQUE');
    Result[0].valorCobranca := vSql.GetDouble('VALOR_COBRANCA');
    Result[0].valorCartao := vSql.GetDouble('VALOR_CARTAO');
    Result[0].observacoes := vSql.GetString('OBSERVACAO');
  end;

  vSql.Active := False;
  vSql.Free;
end;

function BuscarValorDinheiroTurno(pConexao: TConexao; const pTurnoId: Integer): Double;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  sum(VALOR_DINHEIRO * case when TIPO_MOVIMENTO = ''SAN'' then -1 else 1 end) ');
  vSql.SQL.Add('from VW_MOVIMENTOS_TURNOS ');
  vSql.SQL.Add('where TURNO_ID = :P1 ');
  // vSql.SQL.Add('and TIPO_MOVIMENTO in (''SAN'', ''ABE'', ''REV'', ''BXR'', ''SUP'') ');
  vSql.Pesquisar([pTurnoId]);

  Result := vSql.GetDouble(0);

  vSql.Active := False;
  vSql.Free;
end;

function ExcluirSangria(pConexao: TConexao; pMovimentoId: Integer; pTurnoId: Integer): RecRetornoBD;
var
  vMovimento: TMovimentosTurnos;
  vExec: TExecucao;
begin
  Result.TeveErro := False;
  vMovimento := TMovimentosTurnos.Create(pConexao);
  vExec := TExecucao.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    vExec.Add('update CONTAS_RECEBER set ');
    vExec.Add('  TURNO_ID = :P1 ');
    vExec.Add('where RECEBER_ID in ( ');
    vExec.Add('  select ');
    vExec.Add('    ID ');
    vExec.Add('  from MOVIMENTOS_TURNOS_ITENS ');
    vExec.Add('  where movimento_turno_id = :P2 ');
    vExec.Add(') ');
    vExec.Executar([pTurnoId, pMovimentoId]);

    vExec.Clear;
    vExec.Add('delete from MOVIMENTOS_TURNOS_ITENS ');
    vExec.Add('where MOVIMENTO_TURNO_ID = :P1 ');
    vExec.Executar([pMovimentoId]);

    vMovimento.setInt('MOVIMENTO_TURNO_ID', pMovimentoId, True);
    vMovimento.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
  vMovimento.Free;
end;

end.
