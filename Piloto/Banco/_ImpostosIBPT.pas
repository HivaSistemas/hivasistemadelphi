unit _ImpostosIBPT;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecImpostosIBPT = record
    CodigoNCM: string;
    EstadoId: string;
    PercFederal: Double;
    PercFederalImportado: Double;
    PercEstadual: Double;
    PercMunicipal: Double;
    Fonte: string;
  end;

  TImpostosIBPT = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecord: RecImpostosIBPT;
  end;

function BuscarImpostos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecImpostosIBPT>;

function AtualizarImpostos(
  pConexao: TConexao;
  pEstadoId: string;
  pImpostos: TArray<RecImpostosIBPT>
): RecRetornoBD;

implementation

{ TImpostosIBPT }

constructor TImpostosIBPT.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'IMPOSTOS_IBPT');

  FSql :=
    '';

//  setFiltros(getFiltros);

  AddColuna('CODIGO_NCM', True);
  AddColuna('ESTADO_ID', True);

  AddColuna('FONTE');
  AddColuna('PERC_FEDERAL_NACIONAL');
  AddColuna('PERC_FEDERAL_IMPORTADO');
  AddColuna('PERC_ESTADUAL');
  AddColuna('PERC_MUNICIPAL');
end;

function TImpostosIBPT.getRecord: RecImpostosIBPT;
begin
  Result.CodigoNCM      := getString('CODIGO_NCM', True);
  Result.EstadoId       := getString('ESTADO_ID', True);

  Result.Fonte          := getString('FONTE');
  Result.PercFederal    := getDouble('PERC_FEDERAL_NACIONAL');
  Result.PercFederalImportado := getDouble('PERC_FEDERAL_IMPORTADO');
  Result.PercEstadual   := getDouble('PERC_ESTADUAL');
  Result.PercMunicipal  := getDouble('PERC_MUNICIPAL');
end;

function BuscarImpostos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecImpostosIBPT>;
var
  i: Integer;
  t: TImpostosIBPT;
begin
  Result := nil;
  t := TImpostosIBPT.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecord;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function AtualizarImpostos(
  pConexao: TConexao;
  pEstadoId: string;
  pImpostos: TArray<RecImpostosIBPT>
): RecRetornoBD;
var
  t: TImpostosIBPT;
  vExec: TExecucao;
  i: Integer;
begin
  Result.TeveErro := False;
  pConexao.SetRotina('ATUALIZAR_IMPOSTOS_IBPT');
  t := TImpostosIBPT.Create(pConexao);
  vExec := TExecucao.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    vExec.Add('delete from IMPOSTOS_IBPT ');
    vExec.Add('where ESTADO_ID = :P1 ');
    vExec.Executar([pEstadoId]);

    for i := Low(pImpostos) to High(pImpostos) do begin
      t.setString('CODIGO_NCM', pImpostos[i].CodigoNCM, True);
      t.setString('ESTADO_ID', pEstadoId, True);

      t.setString('FONTE', pImpostos[i].Fonte);
      t.setDouble('PERC_FEDERAL_NACIONAL', pImpostos[i].PercFederal);
      t.setDouble('PERC_FEDERAL_IMPORTADO', pImpostos[i].PercFederalImportado);
      t.setDouble('PERC_ESTADUAL', pImpostos[i].PercEstadual);
      t.setDouble('PERC_MUNICIPAL', pImpostos[i].PercMunicipal);

      t.Inserir
    end;

    pConexao.FinalizarTransacao;
  except
    on e: exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  t.Free;
end;

end.
