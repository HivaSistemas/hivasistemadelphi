unit _ControlesEntregas;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, _ControlesEntregasItens, _Biblioteca,
  _ControlesEntregasAjudantes;

{$M+}
type
  RecControlesEntregas = record
  public
    ControleEntregaId: Integer;
    EmpresaId: Integer;
    NomeEmpresa: string;
    VeiculoId: Integer;
    NomeVeiculo: string;
    MotoristaId: Integer;
    NomeMotorista: string;
    UsuarioCadastroId: Integer;
    NomeUsuarioCadastro: string;
    DataHoraCadastro: TDateTime;
    Status: string;
    StatusAnalitico: string;
    UsuarioBaixaId: Integer;
    NomeUsuarioBaixa: string;
    DataHoraBaixa: TDateTime;
    QtdeEntregas: Integer;
    QtdeProdutos: Integer;
    PesoTotal: Double;
    ValorEntregas: Double;
  end;

  TControlesEntregas = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordControles: RecControlesEntregas;
  end;

function AtualizarControles(
  pConexao: TConexao;
  pControleEntregaId: Integer;
  pEmpresaId: Integer;
  pVeiculoId: Integer;
  pMotoristaId: Integer;
  pEntregasIds: TArray<Integer>;
  pAjudantesIds: TArray<Integer>
): RecRetornoBD;

function BuscarControles(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecControlesEntregas>;

function BuscarControlesComando(pConexao: TConexao; pComando: string): TArray<RecControlesEntregas>;

function ExcluirControle(
  pConexao: TConexao;
  pControleEntregaId: Integer
): RecRetornoBD;

function BaixarControleEntrega(pConexao: TConexao; pControleEntregaId: Integer): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TControlesEntregas }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CON.CONTROLE_ENTREGA_ID = :P1 '
    );
end;

constructor TControlesEntregas.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONTROLES_ENTREGAS');

  FSql :=
    'select ' +
    '  CON.CONTROLE_ENTREGA_ID, ' +
    '  CON.EMPRESA_ID, ' +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA, ' +
    '  CON.VEICULO_ID, ' +
    '  VEI.NOME as NOME_VEICULO, ' +
    '  CON.MOTORISTA_ID, ' +
    '  CAD.NOME_FANTASIA as NOME_MOTORISTA, ' +
    '  CON.USUARIO_CADASTRO_ID, ' +
    '  FCA.NOME as NOME_USUARIO_CADASTRO, ' +
    '  CON.DATA_HORA_CADASTRO, ' +
    '  CON.STATUS, ' +
    '  CON.USUARIO_BAIXA_ID, ' +
    '  FBA.NOME as NOME_USUARIO_BAIXA, ' +
    '  CON.DATA_HORA_BAIXA, ' +
    '  ITE.VALOR_ENTREGAS, ' +
    '  ENT.QTDE_ENTREGAS, ' +
    '  ITE.QTDE_PRODUTOS, ' +
    '  ITE.PESO_TOTAL ' +
    'from ' +
    '  CONTROLES_ENTREGAS CON ' +

    'inner join VEICULOS VEI ' +
    'on CON.VEICULO_ID = VEI.VEICULO_ID ' +

    'inner join CADASTROS CAD ' +
    'on CON.MOTORISTA_ID = CAD.CADASTRO_ID ' +

    'inner join EMPRESAS EMP ' +
    'on CON.EMPRESA_ID = EMP.EMPRESA_ID ' +

    'inner join( ' +
    '  select ' +
    '    CONTROLE_ENTREGA_ID, ' +
    '    count(*) as QTDE_ENTREGAS ' +
    '  from ' +
    '    CONTROLES_ENTREGAS_ITENS ' +
    '  group by ' +
    '    CONTROLE_ENTREGA_ID ' +
    ') ENT ' +
    'on CON.CONTROLE_ENTREGA_ID = ENT.CONTROLE_ENTREGA_ID ' +

    'inner join( ' +
    '  select ' +
    '    ENT.CONTROLE_ENTREGA_ID, ' +
    '    sum(round((ORC.VALOR_TOTAL - ORC.VALOR_FRETE) * OIT.VALOR_TOTAL / OIT.QUANTIDADE * ITE.QUANTIDADE / ORC.VALOR_TOTAL_PRODUTOS, 2)) as VALOR_ENTREGAS, ' +
    '    count(ITE.PRODUTO_ID) as QTDE_PRODUTOS, ' +
    '    sum(ITE.QUANTIDADE * PRO.PESO) as PESO_TOTAL ' +
    '  from ' +
    '    ENTREGAS ENT ' +

    '  inner join ENTREGAS_ITENS ITE ' +
    '  on ENT.ENTREGA_ID = ITE.ENTREGA_ID ' +

    '  inner join ORCAMENTOS ORC ' +
    '  on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID ' +

    '  inner join ORCAMENTOS_ITENS OIT ' +
    '  on ORC.ORCAMENTO_ID = OIT.ORCAMENTO_ID ' +
    '  and ITE.ITEM_ID = OIT.ITEM_ID ' +

    '  inner join PRODUTOS PRO ' +
    '  on ITE.PRODUTO_ID = PRO.PRODUTO_ID ' +

    '  where ENT.CONTROLE_ENTREGA_ID is not null ' +

    '  group by ' +
    '    ENT.CONTROLE_ENTREGA_ID ' +
    ') ITE ' +
    'on CON.CONTROLE_ENTREGA_ID = ITE.CONTROLE_ENTREGA_ID ' +

    'inner join FUNCIONARIOS FCA ' +
    'on CON.USUARIO_CADASTRO_ID = FCA.FUNCIONARIO_ID ' +

    'left join FUNCIONARIOS FBA ' +
    'on CON.USUARIO_BAIXA_ID = FBA.FUNCIONARIO_ID ';

  SetFiltros(GetFiltros);

  AddColuna('CONTROLE_ENTREGA_ID', True);
  AddColuna('EMPRESA_ID');
  AddColunaSL('NOME_EMPRESA');
  AddColuna('VEICULO_ID');
  AddColunaSL('NOME_VEICULO');
  AddColuna('MOTORISTA_ID');
  AddColunaSL('NOME_MOTORISTA');
  AddColunaSL('USUARIO_CADASTRO_ID');
  AddColunaSL('NOME_USUARIO_CADASTRO');
  AddColunaSL('DATA_HORA_CADASTRO');
  AddColunaSL('STATUS');
  AddColunaSL('USUARIO_BAIXA_ID');
  AddColunaSL('NOME_USUARIO_BAIXA');
  AddColunaSL('DATA_HORA_BAIXA');
  AddColunaSL('QTDE_ENTREGAS');
  AddColunaSL('VALOR_ENTREGAS');
  AddColunaSL('QTDE_PRODUTOS');
  AddColunaSL('PESO_TOTAL');
end;

function TControlesEntregas.GetRecordControles: RecControlesEntregas;
begin
  Result.ControleEntregaId   := GetInt('CONTROLE_ENTREGA_ID', True);
  Result.EmpresaId           := GetInt('EMPRESA_ID');
  Result.NomeEmpresa         := GetString('NOME_EMPRESA');
  Result.VeiculoId           := GetInt('VEICULO_ID');
  Result.NomeVeiculo         := GetString('NOME_VEICULO');
  Result.MotoristaId         := GetInt('MOTORISTA_ID');
  Result.NomeMotorista       := GetString('NOME_MOTORISTA');
  Result.UsuarioCadastroId   := GetInt('USUARIO_CADASTRO_ID');
  Result.NomeUsuarioCadastro := GetString('NOME_USUARIO_CADASTRO');
  Result.DataHoraCadastro    := GetData('DATA_HORA_CADASTRO');
  Result.Status              := GetString('STATUS');
  Result.UsuarioBaixaId      := GetInt('USUARIO_BAIXA_ID');
  Result.NomeUsuarioBaixa    := GetString('NOME_USUARIO_BAIXA');
  Result.DataHoraBaixa       := GetData('DATA_HORA_BAIXA');
  Result.QtdeEntregas        := GetInt('QTDE_ENTREGAS');
  Result.ValorEntregas       := getDouble('VALOR_ENTREGAS');
  Result.QtdeProdutos        := GetInt('QTDE_PRODUTOS');
  Result.PesoTotal           := getDouble('PESO_TOTAL');
  Result.StatusAnalitico     := _Biblioteca.IIfStr( Result.Status = 'T', 'Em transporte', 'Baixado' );
end;

function AtualizarControles(
  pConexao: TConexao;
  pControleEntregaId: Integer;
  pEmpresaId: Integer;
  pVeiculoId: Integer;
  pMotoristaId: Integer;
  pEntregasIds: TArray<Integer>;
  pAjudantesIds: TArray<Integer>
): RecRetornoBD;
var
  i: Integer;
  vNovo: Boolean;

  vObj: TControlesEntregas;
  vIte: TControlesEntregasItens;
  vAjud: TControlesEntregasAjudantes;
begin
  Result.TeveErro := False;
  vNovo := pControleEntregaId = 0;
  vObj := TControlesEntregas.Create(pConexao);
  vIte := TControlesEntregasItens.Create(pConexao);
  vAjud := TControlesEntregasAjudantes.Create(pConexao);

  pConexao.SetRotina('ATUALIZAR_CONTROLE_ENTREGA');

  if vNovo then begin
    pControleEntregaId := TSequencia.Create(pConexao, 'SEQ_CONTROLE_ENTREGA_ID').GetProximaSequencia;
    Result.AsInt := pControleEntregaId;
  end;

  vObj.SetInt('CONTROLE_ENTREGA_ID', pControleEntregaId, True);
  vObj.SetInt('EMPRESA_ID', pEmpresaId);
  vObj.SetInt('VEICULO_ID', pVeiculoId);
  vObj.SetInt('MOTORISTA_ID', pMotoristaId);

  try
    pConexao.IniciarTransacao;

    if vNovo then
      vObj.Inserir
    else
      vObj.Atualizar;

    for i := Low(pEntregasIds) to High(pEntregasIds) do begin
      vIte.setInt('CONTROLE_ENTREGA_ID', pControleEntregaId, True);
      vIte.setInt('ENTREGA_ID', pEntregasIds[i], True);

      vIte.Inserir;
    end;

    for i := Low(pAjudantesIds) to High(pAjudantesIds) do begin
      vAjud.setInt('CONTROLE_ENTREGA_ID', pControleEntregaId, True);
      vAjud.setInt('AJUDANTE_ID', pAjudantesIds[i], True);

      vAjud.Inserir;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vAjud.Free;
  vIte.Free;
  vObj.Free;
end;

function BuscarControles(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecControlesEntregas>;
var
  i: Integer;
  t: TControlesEntregas;
begin
  Result := nil;
  t := TControlesEntregas.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordControles;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarControlesComando(pConexao: TConexao; pComando: string): TArray<RecControlesEntregas>;
var
  i: Integer;
  t: TControlesEntregas;
begin
  Result := nil;
  t := TControlesEntregas.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordControles;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirControle(
  pConexao: TConexao;
  pControleEntregaId: Integer
): RecRetornoBD;
var
  t: TControlesEntregas;
begin
  pConexao.SetRotina('EXCLUIR_CONTROLE_ENTREGA');
  Result.TeveErro := False;
  t := TControlesEntregas.Create(pConexao);

  t.SetInt('CONTROLE_ENTREGA_ID', pControleEntregaId, True);

  try
    pConexao.IniciarTransacao;

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BaixarControleEntrega(pConexao: TConexao; pControleEntregaId: Integer): RecRetornoBD;
var
  vExec: TExecucao;
begin
  pConexao.SetRotina('BAIXAR_CONTROLE_ENTREGA');
  vExec := TExecucao.Create(pConexao);
  Result.TeveErro := False;

  vExec.Add('update CONTROLES_ENTREGAS set ');
  vExec.Add('  STATUS = ''B'' ');
  vExec.Add('where CONTROLE_ENTREGA_ID = :P1 ');

  try
    vExec.Executar([pControleEntregaId]);
  except
    on e: Exception do
      Result.TratarErro(e);
  end;

  vExec.Free;
end;

end.
