unit _CFOPsCstOperacoes;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils;

{$M+}
type
  RecCFOPsCSTOperacoes = record
    CFOPid: string;
    Cst: string;
    EmpresaId: Integer;
    TipoMovimento: string;
    TipoOperacao: string;
    TipoMovimentoAnalitico: string;
    TipoOperacaoAnalitico: string;
  end;

  TCFOPsCSTOperacoes = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordCFOPsCst: RecCFOPsCSTOperacoes;
  end;

function BuscarCFOPCstOperacao(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCFOPsCSTOperacoes>;

function AtualizarCFOPsCst(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pItens: TArray<RecCFOPsCSTOperacoes>
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TCFOPsCSTOperacoes }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where EMPRESA_ID = :P1 ' +
      'order by ' +
      '  TIPO_OPERACAO, ' +
      '  TIPO_MOVIMENTO, ' +
      '  CST, ' +
      '  CFOP_ID '
    );
end;

constructor TCFOPsCSTOperacoes.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CFOPS_CST_OPERACOES');

  FSql :=
    'select ' +
    '  EMPRESA_ID, ' +
    '  CFOP_ID, ' +
    '  CST, ' +
    '  TIPO_MOVIMENTO, ' +
    '  TIPO_OPERACAO ' +
    'from ' +
    '  CFOPS_CST_OPERACOES ';

  setFiltros(getFiltros);

  AddColuna('EMPRESA_ID', True);
  AddColuna('CFOP_ID', True);
  AddColuna('CST', True);
  AddColuna('TIPO_MOVIMENTO', True);
  AddColuna('TIPO_OPERACAO', True);
end;

function TCFOPsCSTOperacoes.getRecordCFOPsCst: RecCFOPsCSTOperacoes;
begin
  Result.EmpresaId     := getInt('EMPRESA_ID', True);
  Result.CFOPid        := getString('CFOP_ID', True);
  Result.Cst           := getString('CST', True);
  Result.TipoMovimento := getString('TIPO_MOVIMENTO', True);
  Result.TipoOperacao  := getString('TIPO_OPERACAO', True);

  Result.TipoMovimentoAnalitico :=
    _BibliotecaGenerica.Decode(
      Result.TipoMovimento,[
        'VIT', 'Venda',
        'DEV', 'Dev. de venda',
        'REC', 'Remessa para cons.',
        'TPE', 'Transf.prod. entre emp.',
        'REN', 'Retorno de entrega',
        'RDF', 'Remessa dep.fechado',
        'RRD', 'Retorno dep.fechado',
        'DEN', 'Dev. de entrada',
        'PEN', 'Produ��o entrada',
        'PSA', 'Produ��o sa�da',
        'AEE', 'Ajuste entrada',
        'AES', 'Ajuste sa�da'
      ]
    );

  Result.TipoOperacaoAnalitico :=
    _BibliotecaGenerica.Decode(
      Result.TipoOperacao,[
        'I', 'Interna',
        'E', 'Interestadual',
        'X', 'Exterior'
      ]
    );
end;

function BuscarCFOPCstOperacao(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCFOPsCSTOperacoes>;
var
  i: Integer;
  t: TCFOPsCSTOperacoes;
begin
  Result := nil;
  t := TCFOPsCSTOperacoes.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordCFOPsCst;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function AtualizarCFOPsCst(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pItens: TArray<RecCFOPsCSTOperacoes>
): RecRetornoBD;
var
  i: Integer;
  vExec: TExecucao;
  vCfop: TCFOPsCSTOperacoes;
begin
  Result.TeveErro := False;

  vCfop := TCFOPsCSTOperacoes.Create(pConexao);

  vExec := TExecucao.Create(pConexao);
  vExec.Add('delete from CFOPS_CST_OPERACOES');
  vExec.Add('where EMPRESA_ID = :P1');

  try
    pConexao.IniciarTransacao;

    vExec.Executar([pEmpresaId]);

    for i := Low(pItens) to High(pItens) do begin
      vCfop.setInt('EMPRESA_ID', pEmpresaId, True);
      vCfop.setString('CFOP_ID', pItens[i].CFOPid, True);
      vCfop.setString('CST', pItens[i].Cst, True);
      vCfop.setString('TIPO_OPERACAO', pItens[i].TipoOperacao, True);
      vCfop.setString('TIPO_MOVIMENTO', pItens[i].TipoMovimento, True);

      vCfop.Inserir;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vCfop.Free;
  vExec.Free;
end;

end.
