unit _ControlesEntregasAjudantes;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecControlesEntregasAjudantes = record
    ControleEntregaId: Integer;
    AjudanteId: Integer;
  end;

  TControlesEntregasAjudantes = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordControlesEntregasAjudantes: RecControlesEntregasAjudantes;
  end;

function BuscarControlesEntregasAjudantes(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecControlesEntregasAjudantes>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TControlesEntregasAjudantes }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CONTROLE_ENTREGA_ID = :P1'
    );
end;

constructor TControlesEntregasAjudantes.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONTROLES_ENTREGAS_AJUDANTES');

  FSql := 
    'select ' +
    '  CONTROLE_ENTREGA_ID, ' +
    '  AJUDANTE_ID ' +
    'from ' +
    '  CONTROLES_ENTREGAS_AJUDANTES ';

  setFiltros(getFiltros);

  AddColuna('CONTROLE_ENTREGA_ID', True);
  AddColuna('AJUDANTE_ID', True);
end;

function TControlesEntregasAjudantes.getRecordControlesEntregasAjudantes: RecControlesEntregasAjudantes;
begin
  Result.ControleEntregaId          := getInt('CONTROLE_ENTREGA_ID', True);
  Result.AjudanteId                 := getInt('AJUDANTE_ID', True);
end;

function BuscarControlesEntregasAjudantes(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecControlesEntregasAjudantes>;
var
  i: Integer;
  t: TControlesEntregasAjudantes;
begin
  Result := nil;
  t := TControlesEntregasAjudantes.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordControlesEntregasAjudantes;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
