unit _MapaProducao;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils,
  _RecordsEstoques, _Biblioteca,  Vcl.Graphics;

{$M+}
type
  TMapaProducao = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordMapaProducao: RecMapaProducao;
  end;

function AtualizarMapaProducao(
  pConexao: TConexao;
  pMapaProducaoId: Integer;
  pEmpresaId: Integer;
  pMapaProducao: RecMapaProducao;
  pTipoMovimento: string
): RecRetornoBD;

function BuscarMapaProducao(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecMapaProducao>;

function BuscarMapaProducaoComando(pConexao: TConexao; pComando: string): TArray<RecMapaProducao>;

function ExcluirMapaProducao(
  pConexao: TConexao;
  pMapaProducaoId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TMapaProducao }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where MAPA_PRODUCAO_ID = :P1'
    );
end;

constructor TMapaProducao.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'MAPA_PRODUCAO');

  FSql :=
    'select ' +
    '  AJU.AJUSTE_ESTOQUE_ID, ' +
    '  AJU.EMPRESA_ID, ' +
    '  EMP.NOME_FANTASIA, ' +
    '  AJU.MOTIVO_AJUSTE_ID, ' +
    '  AJU.PLANO_FINANCEIRO_ID, ' +
    '  AJU.BAIXA_COMPRA_ID, ' +
    '  AJU.TIPO, ' +
    '  MOT.DESCRICAO as MOTIVO_AJUSTE_ESTOQUE, ' +
    '  AJU.USUARIO_AJUSTE_ID, ' +
    '  FUN.NOME as NOME_USUARIO_AJUSTE, ' +
    '  AJU.DATA_HORA_AJUSTE, ' +
    '  AJU.OBSERVACAO, ' +
    '  AJU.VALOR_TOTAL ' +
    'from ' +
    '  AJUSTES_ESTOQUE AJU ' +

    'inner join MOTIVOS_AJUSTE_ESTOQUE MOT ' +
    'on AJU.MOTIVO_AJUSTE_ID = MOT.MOTIVO_AJUSTE_ID ' +

    'inner join FUNCIONARIOS FUN ' +
    'on AJU.USUARIO_AJUSTE_ID = FUN.FUNCIONARIO_ID ' +

    'inner join EMPRESAS EMP ' +
    'on AJU.EMPRESA_ID = EMP.EMPRESA_ID ';

  setFiltros(getFiltros);

  AddColuna('AJUSTE_ESTOQUE_ID', True);
  AddColuna('EMPRESA_ID');
  AddColunaSL('NOME_FANTASIA');
  AddColuna('MOTIVO_AJUSTE_ID');
  AddColuna('PLANO_FINANCEIRO_ID');
  AddColuna('TIPO');
  AddColunaSL('MOTIVO_AJUSTE_ESTOQUE');
  AddColunaSL('USUARIO_AJUSTE_ID');
  AddColunaSL('NOME_USUARIO_AJUSTE');
  AddColunaSL('DATA_HORA_AJUSTE');
  AddColuna('OBSERVACAO');
  AddColuna('BAIXA_COMPRA_ID');
  AddColuna('VALOR_TOTAL');
end;

function TMapaProducao.getRecordMapaProducao: RecMapaProducao;
begin
  Result.ajuste_estoque_id   := getInt('AJUSTE_ESTOQUE_ID', True);
  Result.EmpresaId           := getInt('EMPRESA_ID');
  Result.NomeFantasia        := getString('NOME_FANTASIA');
  Result.motivo_ajuste_id    := getInt('MOTIVO_AJUSTE_ID');
  Result.PlanoFinanceiroId   := getString('PLANO_FINANCEIRO_ID');
  Result.MotivoMapaProducao := getString('MOTIVO_AJUSTE_ESTOQUE');
  Result.usuario_ajuste_id   := getInt('USUARIO_AJUSTE_ID');
  Result.NomeUsuarioAjuste   := getString('NOME_USUARIO_AJUSTE');
  Result.data_hora_ajuste    := getData('DATA_HORA_AJUSTE');
  Result.observacao          := getString('OBSERVACAO');
  Result.Tipo                := getString('TIPO');
  Result.BaixaCompraId       := getInt('BAIXA_COMPRA_ID');
  Result.ValorTotal          := getDouble('VALOR_TOTAL');

  if Result.Tipo = 'NOR' then
    Result.TipoAnalitico := 'Normal'
  else if Result.Tipo = 'RCO' then
    Result.TipoAnalitico := 'Recebim. de compras'
  else if Result.Tipo = 'OUT' then
    Result.TipoAnalitico := 'Outras notas';
end;

function AtualizarMapaProducao(
  pConexao: TConexao;
  pMapaProducaoId: Integer;
  pEmpresaId: Integer;
  pMapaProducao: RecMapaProducao;
  pTipoMovimento: string
): RecRetornoBD;
var
  t: TMapaProducao;
  vItem: TMapaProducaoItens;

  i: Integer;
  novo: Boolean;
  seq: TSequencia;
  vExec: TExecucao;
begin

end;

function AtualizarAjustesEstoque(
  pConexao: TConexao;
  pMapaProducaoId: Integer;
  pEmpresaId: Integer;
  pMotivoAjusteId: Integer;
  pObservacao: string;
  pPlanoFinanceiroId: string;
  pTipo: string;
  pBaixaCompraId: Integer;
  pValorTotal: Double;
  pAjusteItens: TArray<RecMapaProducaoItens>;
  pEmTransacao: Boolean
): RecRetornoBD;
var
  t: TMapaProducao;
  vItem: TMapaProducaoItens;

  i: Integer;
  novo: Boolean;
  seq: TSequencia;
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('REALIZAR_AJUSTE_ESTOQUE');

  vExec := TExecucao.Create(pConexao);
  t := TMapaProducao.Create(pConexao);
  vItem := TMapaProducaoItens.Create(pConexao);

  novo := pMapaProducaoId = 0;
  if novo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_AJUSTE_ESTOQUE_ID');
    pMapaProducaoId := seq.getProximaSequencia;
    result.AsInt := pMapaProducaoId;
    seq.Free;
  end;

  try
    if not pEmTransacao then
      pConexao.IniciarTransacao;

    t.setInt('AJUSTE_ESTOQUE_ID', pMapaProducaoId, True);
    t.setInt('EMPRESA_ID', pEmpresaId);
    t.setInt('MOTIVO_AJUSTE_ID', pMotivoAjusteId);
    t.setStringN('OBSERVACAO', pObservacao);
    t.setString('PLANO_FINANCEIRO_ID', pPlanoFinanceiroId);
    t.setString('TIPO', pTipo);
    t.setIntN('BAIXA_COMPRA_ID', pBaixaCompraId);
    t.setDouble('VALOR_TOTAL', pValorTotal);

    if novo then
      t.Inserir
    else
      t.Atualizar;

    if not novo then begin
      vExec.Limpar;
      vExec.Add('delete from AJUSTES_ESTOQUE_ITENS ');
      vExec.Add('where AJUSTE_ESTOQUE_ID = :P1');
      vExec.Executar([pMapaProducaoId]);
    end;

    vItem.setInt('AJUSTE_ESTOQUE_ID', pMapaProducaoId, True);
    for i := Low(pAjusteItens) to High(pAjusteItens) do begin
      vItem.setInt('LOCAL_ID', pAjusteItens[i].LocalId, True);
      vItem.setInt('ITEM_ID', pAjusteItens[i].item_id, True);
      vItem.setString('LOTE', pAjusteItens[i].Lote, True);
      vItem.setInt('PRODUTO_ID', pAjusteItens[i].produto_id);
      vItem.setDouble('QUANTIDADE', pAjusteItens[i].quantidade);
      vItem.setString('NATUREZA', pAjusteItens[i].Natureza);
      vItem.setDouble('PRECO_UNITARIO', pAjusteItens[i].PrecoUnitario);
      vItem.setDouble('VALOR_TOTAL', pAjusteItens[i].ValorTotal);
      vItem.setDataN('DATA_FABRICACAO', pAjusteItens[i].DataFabricacao);
      vItem.setDataN('DATA_VENCIMENTO', pAjusteItens[i].DataVencimento);

      vItem.Inserir;
    end;

    if not pEmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pEmTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;
  vExec.Free;
  vItem.Free;
  t.Free;
end;

function BuscarAjustesEstoque(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecMapaProducao>;
var
  i: Integer;
  t: TMapaProducao;
begin
  Result := nil;
  t := TMapaProducao.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordMapaProducao;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarAjustesEstoqueComando(pConexao: TConexao; pComando: string): TArray<RecMapaProducao>;
var
  i: Integer;
  t: TMapaProducao;
begin
  Result := nil;
  t := TMapaProducao.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordMapaProducao;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;


function ExcluirAjustesEstoque(
  pConexao: TConexao;
  pMapaProducaoId: Integer
): RecRetornoBD;
var
  vAju: TMapaProducao;
  vExec: TExecucao;
begin
  Result.TeveErro := False;
  vAju := TMapaProducao.Create(pConexao);
  vExec := TExecucao.Create(pConexao);

  vExec.Add('delete from AJUSTES_ESTOQUE_ITENS');
  vExec.Add('where AJUSTE_ESTOQUE_ID = :P1');

  try
    pConexao.IniciarTransacao;

    vExec.Executar([pMapaProducaoId]);

    vAju.setInt('AJUSTE_ESTOQUE_ID', pMapaProducaoId, True);
    vAju.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
  vAju.Free;
end;

end.
