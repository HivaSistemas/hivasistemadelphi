unit _ContasPagarBaixasPagtosDin;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, _RecordsFinanceiros;

{$M+}
type
  TContasPagarBaixasPagtosDin = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecContasPagarBaixasPagtosDin: RecTitulosBaixasPagtoDin;
  end;

function BuscarContasRecBaixasPagtoDin(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosBaixasPagtoDin>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TContasPagarBaixasPagtosDin }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CPD.BAIXA_ID = :P1 '
    );
end;

constructor TContasPagarBaixasPagtosDin.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONTAS_PAGAR_BAIXAS_PAGTOS_DIN');

  FSql :=
    'select ' +
    '  CPD.BAIXA_ID, ' +
    '  CPD.CONTA_ID, ' +
    '  CON.NOME as DESCRICAO, ' +
    '  CPD.VALOR ' +
    'from ' +
    '  CONTAS_PAGAR_BAIXAS_PAGTOS_DIN CPD ' +

    'inner join CONTAS CON ' +
    'on CPD.CONTA_ID = CON.CONTA ';

  setFiltros(getFiltros);

  AddColuna('BAIXA_ID', True);
  AddColuna('CONTA_ID', True);
  AddColuna('VALOR');
  AddColunaSL('DESCRICAO');
end;

function TContasPagarBaixasPagtosDin.getRecContasPagarBaixasPagtosDin: RecTitulosBaixasPagtoDin;
begin
  Result.BaixaId   := getInt('BAIXA_ID', True);
  Result.ContaId   := getString('CONTA_ID', True);
  Result.Valor     := getDouble('VALOR');
  Result.Descricao := getString('DESCRICAO');
end;

function BuscarContasRecBaixasPagtoDin(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosBaixasPagtoDin>;
var
  i: Integer;
  t: TContasPagarBaixasPagtosDin;
begin
  Result := nil;
  t := TContasPagarBaixasPagtosDin.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecContasPagarBaixasPagtosDin;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;


end.
