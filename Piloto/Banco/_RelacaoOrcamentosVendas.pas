unit _RelacaoOrcamentosVendas;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsRelatorios, ImpressaoRelacaoVendasProdutosGrafico, Sysutils,
  _RecordsEspeciais;

function BuscarOrcamentos(pConexao: TConexao; pComando: string): TArray<RecOrcamentosVendas>;
function BuscarProdutosOrcamentos(pConexao: TConexao; const pComando: string): TArray<RecProdutoOrcamento>;
procedure GravarTabelaTemporariaItens(pConexao: TConexao; registros: TArray<RecDadosVendDevol>; produtos: TArray<RecDadosProdutoVendDevol>);
function AtualizarTipoAcompanhamentoOrcamento(
  pConexao: TConexao;
  pTipoAcompanhamentoId: Integer;
  pOrcamentoId: Integer
): RecRetornoBD;

implementation

function BuscarOrcamentos(pConexao: TConexao; pComando: string): TArray<RecOrcamentosVendas>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  with vSql.SQL do begin
    Add('select ');
    Add('  ORCAMENTO_ID, ');
    Add('  CLIENTE_ID, ');
    Add('  NOME_CLIENTE, ');
    Add('  DATA_CADASTRO, ');
    Add('  VENDEDOR_ID, ');
    Add('  NOME_VENDEDOR, ');
    Add('  CONDICAO_ID, ');
    Add('  NOME_CONDICAO_PAGAMENTO, ');
    Add('  VALOR_TOTAL_PRODUTOS, ');
    Add('  VALOR_OUTRAS_DESPESAS, ');
    Add('  VALOR_DESCONTO, ');
    Add('  VALOR_TOTAL, ');
    Add('  EMPRESA_ID, ');
    Add('  NOME_EMPRESA, ');
    Add('  TIPO_ANALISE_CUSTO, ');
    Add('  INDICE_DESCONTO_VENDA_ID, ');
    Add('  VALOR_CUSTO_ENTRADA, ');
    Add('  VALOR_CUSTO_FINAL, ');
    Add('  STATUS, ');
    Add('  DATA_HORA_RECEBIMENTO, ');
    Add('  CLIENTE_GRUPO_ID, ');
    Add('  NOME_GRUPO_CLIENTE, ');
    Add('  VALOR_FRETE, ');
    Add('  TIPO_ACOMPANHAMENTO_ID, ');
    Add('  TIPO_ACOMPANHAMENTO_DESC ');
    Add('from ');
    Add('  VW_RELACAO_ORCAMENTOS_VENDAS ');
    Add(pComando);
  end;

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].OrcamentoId           := vSql.GetInt('ORCAMENTO_ID');
      Result[i].ClienteId             := vSql.GetInt('CLIENTE_ID');
      Result[i].NomeCliente           := vSql.GetString('NOME_CLIENTE');
      Result[i].DataCadastro          := vSql.GetData('DATA_CADASTRO');
      Result[i].VendedorId            := vSql.GetInt('VENDEDOR_ID');
      Result[i].NomeVendedor          := vSql.GetString('NOME_VENDEDOR');
      Result[i].CondicaoId            := vSql.GetInt('CONDICAO_ID');
      Result[i].NomeCondicaoPagamento := vSql.GetString('NOME_CONDICAO_PAGAMENTO');
      Result[i].ValorTotalProdutos    := vSql.GetDouble('VALOR_TOTAL_PRODUTOS');
      Result[i].ValorOutrasDespesas   := vSql.GetDouble('VALOR_OUTRAS_DESPESAS');
      Result[i].ValorFrete            := vSql.GetDouble('VALOR_FRETE');
      Result[i].ValorDesconto         := vSql.GetDouble('VALOR_DESCONTO');
      Result[i].ValorTotal            := vSql.GetDouble('VALOR_TOTAL');
      Result[i].EmpresaId             := vSql.GetInt('EMPRESA_ID');
      Result[i].NomeEmpresa           := vSql.GetString('NOME_EMPRESA');
      Result[i].tipoAnaliseCusto      := vSql.GetString('TIPO_ANALISE_CUSTO');
      Result[i].IndiceDescontoVendaId := vSql.GetInt('INDICE_DESCONTO_VENDA_ID');
      Result[i].ValorCustoEntrada     := vSql.GetDouble('VALOR_CUSTO_ENTRADA');
      Result[i].ValorCustoFinal       := vSql.GetDouble('VALOR_CUSTO_FINAL');
      Result[i].Status                := vSql.GetString('STATUS');
      Result[i].DataHoraRecebimento   := vSql.GetData('DATA_HORA_RECEBIMENTO');
      Result[i].ClienteGrupoId        := vSql.GetInt('CLIENTE_GRUPO_ID');
      Result[i].NomeGrupoCliente      := vSql.GetString('NOME_GRUPO_CLIENTE');
      Result[i].TipoAcompanhamentoId  := vSql.GetInt('TIPO_ACOMPANHAMENTO_ID');
      Result[i].TipoAcompanhamentoDesc:= vSql.GetString('TIPO_ACOMPANHAMENTO_DESC');

      if Result[i].Status  = 'OE' then
        Result[i].StatusAnalitico := 'Aberto'
      else if Result[i].Status  = 'OB' then
        Result[i].StatusAnalitico := 'Bloqueado'
      else if Result[i].Status = 'VB' then
        Result[i].StatusAnalitico := 'Venda bloqueada'
      else if Result[i].Status = 'VR' then
        Result[i].StatusAnalitico := 'Aguardando recebimento'
      else if Result[i].Status = 'VE' then
        Result[i].StatusAnalitico := 'Aguard. recebimento entrega'
      else if Result[i].Status = 'RE' then
        Result[i].StatusAnalitico := 'Recebido'
      else if Result[i].Status = 'CA' then
        Result[i].StatusAnalitico := 'Cancelado';

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function BuscarProdutosOrcamentos(pConexao: TConexao; const pComando: string): TArray<RecProdutoOrcamento>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  with vSql.SQL do begin
    Add('select ');
    Add('  PRODUTO_ID, ');
    Add('  ITEM_ID, ');
    Add('  NOME_PRODUTO, ');
    Add('  MARCA_ID, ');
    Add('  NOME_MARCA, '); // 4
    Add('  PRECO_UNITARIO, ');
    Add('  QUANTIDADE, ');
    Add('  UNIDADE_VENDA, ');
    Add('  VALOR_TOTAL, ');
    Add('  ORCAMENTO_ID ');
    Add('from ');
    Add('  VW_ORCAMENTOS_ITENS_IMPRESSAO ');
    Add(pComando);
  end;

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].ProdutoId     := vSql.GetInt(0);
      Result[i].ItemId        := vSql.GetInt(1);
      Result[i].NomeProduto   := vSql.GetString(2);
      Result[i].MarcaId       := vSql.GetInt(3);
      Result[i].NomeMarca     := vSql.GetString(4);
      Result[i].PrecoUnitario := vSql.GetDouble(5);
      Result[i].Quantidade    := vSql.GetDouble(6);
      Result[i].UnidadeVenda  := vSql.GetString(7);
      Result[i].ValorTotal    := vSql.GetDouble(8);
      Result[i].OrcamentoId   := vSql.GetInt(9);

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

procedure GravarTabelaTemporariaItens(pConexao: TConexao; registros: TArray<RecDadosVendDevol>; produtos: TArray<RecDadosProdutoVendDevol>);
var
  vExex: TExecucao;
  vConsulta: TConsulta;
  i: Integer;
  retorno: Integer;
begin
  pConexao.SetRotina('GRAVAR_TABELA_TEMP_ITENS');
  vExex := TExecucao.Create(pConexao);
  vConsulta := TConsulta.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    try
      vConsulta.SQL.Add('select COUNT(*) from user_tables where table_name = ''TEMP_PROD_VEND_DEVOL''');
      vConsulta.Pesquisar;

      if vConsulta.GetInt(0) > 0 then begin
       vExex.SQL.Clear;
        vExex.SQL.Text := 'drop table TEMP_PROD_VEND_DEVOL ';
        vExex.Executar;
      end;

      vConsulta.SQL.Clear;
      vConsulta.SQL.Add('select COUNT(*) from user_tables where table_name = ''TEMP_VEND_DEVOL''');
      vConsulta.Pesquisar;

      if vConsulta.GetInt(0) > 0 then begin
       vExex.SQL.Clear;
        vExex.SQL.Text := 'drop table TEMP_VEND_DEVOL ';
        vExex.Executar;
      end;
    except
      //
    end;

    vExex.SQL.Clear;
    vExex.SQL.add('create table TEMP_VEND_DEVOL( ');
    vExex.SQL.add('  PEDIDO varchar(20), ');
    vExex.SQL.add('  MOVIMENTO varchar(20), ');
    vExex.SQL.add('  DATA_RECEBIMENTO varchar(20), ');
    vExex.SQL.add('  CLIENTE varchar(200), ');
    vExex.SQL.add('  VENDEDOR varchar(200), ');
    vExex.SQL.add('  COND_PGTO varchar(200), ');
    vExex.SQL.add('  VALOR_PRODUTO NUMBER(8,2), ');
    vExex.SQL.add('  VALOR_TOTAL NUMBER(8,2), ');
    vExex.SQL.add('  OUTRAS_DESPESAS NUMBER(8,2), ');
    vExex.SQL.add('  DESCONTO NUMBER(8,2), ');
    vExex.SQL.add('  VALOR_BRUTO NUMBER(8,2), ');
    vExex.SQL.add('  VALOR_LIQUIDO NUMBER(8,2), ');
    vExex.SQL.add('  PERC_LUCRO_BRUTO NUMBER(8,2), ');
    vExex.SQL.add('  PERC_LUCRO_LIQUIDO NUMBER(8,2), ');
    vExex.SQL.add('  EMPRESA varchar(200), ');
    vExex.SQL.add('  DEVOLUCAO varchar(50), ');
    vExex.SQL.add('  POSICAO number(10,0) ');
    vExex.SQL.add(') ');
    vExex.Executar;

    vExex.SQL.Clear;
    vExex.SQL.add('create table TEMP_PROD_VEND_DEVOL( ');
    vExex.SQL.add('  POSICAO                           NUMBER(10,0), ');
    vExex.SQL.add('  PRODUTO_ID                        NUMBER(10,0), ');
    vExex.SQL.add('  NOME                              VARCHAR2(60), ');
    vExex.SQL.add('  MARCA                             VARCHAR2(60), ');
    vExex.SQL.add('  PRECO_UNITARIO                    NUMBER(8,2), ');
    vExex.SQL.add('  QUANTIDADE                        NUMBER(8,2), ');
    vExex.SQL.add('  UNIDADE                           VARCHAR2(10), ');
    vExex.SQL.add('  VALOR_TOTAL                       NUMBER(8,2), ');
    vExex.SQL.add('  OUTRAS_DESPESAS                   NUMBER(8,2), ');
    vExex.SQL.add('  DESCONTO                          NUMBER(8,2), ');
    vExex.SQL.add('  VALOR_LUCRO_BRUTO                 NUMBER(8,2), ');
    vExex.SQL.add('  VALOR_LUCRO_LIQUIDO               NUMBER(8,2), ');
    vExex.SQL.add('  PERC_LUCRO_BRUTO                  NUMBER(8,2), ');
    vExex.SQL.add('  PERC_LUCRO_LIQUIDO                NUMBER(8,2) ');
    vExex.SQL.add(')                        ');
    vExex.Executar;

    vExex.SQL.Clear;
    vExex.SQL.add('insert into TEMP_VEND_DEVOL( ');
    vExex.SQL.add('  PEDIDO, ');
    vExex.SQL.add('  MOVIMENTO, ');
    vExex.SQL.add('  DATA_RECEBIMENTO, ');
    vExex.SQL.add('  CLIENTE, ');
    vExex.SQL.add('  VENDEDOR, ');
    vExex.SQL.add('  COND_PGTO, ');
    vExex.SQL.add('  VALOR_PRODUTO, ');
    vExex.SQL.add('  VALOR_TOTAL, ');
    vExex.SQL.add('  OUTRAS_DESPESAS, ');
    vExex.SQL.add('  DESCONTO, ');
    vExex.SQL.add('  VALOR_BRUTO, ');
    vExex.SQL.add('  VALOR_LIQUIDO, ');
    vExex.SQL.add('  PERC_LUCRO_BRUTO, ');
    vExex.SQL.add('  PERC_LUCRO_LIQUIDO, ');
    vExex.SQL.add('  EMPRESA, ');
    vExex.SQL.add('  DEVOLUCAO, ');
    vExex.SQL.add('  POSICAO ');
    vExex.SQL.add(')values( ');
    vExex.SQL.add('  :P1, ');
    vExex.SQL.add('  :P2, ');
    vExex.SQL.add('  :P3, ');
    vExex.SQL.add('  :P4, ');
    vExex.SQL.add('  :P5, ');
    vExex.SQL.add('  :P6, ');
    vExex.SQL.add('  :P7, ');
    vExex.SQL.add('  :P8, ');
    vExex.SQL.add('  :P9, ');
    vExex.SQL.add('  :P10, ');
    vExex.SQL.add('  :P11, ');
    vExex.SQL.add('  :P12, ');
    vExex.SQL.add('  :P13, ');
    vExex.SQL.add('  :P14, ');
    vExex.SQL.add('  :P15, ');
    vExex.SQL.add('  :P16, ');
    vExex.SQL.add('  :P17 ');
    vExex.SQL.add(') ');

    for I := 0 to Length(registros) - 1 do begin
      vExex.Executar([
        registros[i].Pedido,
        registros[i].Movimento,
        registros[i].DataRecebimento,
        registros[i].Cliente,
        registros[i].Vendedor,
        registros[i].CondPagto,
        registros[i].ValorProduto,
        registros[i].ValorTotal,
        registros[i].OutrasDespesas,
        registros[i].Desconto,
        registros[i].ValorBruto,
        registros[i].ValorLiquido,
        registros[i].PercLucroBruto,
        registros[i].PercLucroLiquido,
        registros[i].Empresa,
        registros[i].Devolucao,
        registros[i].Posicao
      ]);
    end;

    vExex.SQL.Clear;
    vExex.SQL.add('insert into TEMP_PROD_VEND_DEVOL( ');
    vExex.SQL.add('  POSICAO, ');
    vExex.SQL.add('  PRODUTO_ID, ');
    vExex.SQL.add('  NOME, ');
    vExex.SQL.add('  MARCA, ');
    vExex.SQL.add('  PRECO_UNITARIO, ');
    vExex.SQL.add('  QUANTIDADE, ');
    vExex.SQL.add('  UNIDADE, ');
    vExex.SQL.add('  VALOR_TOTAL, ');
    vExex.SQL.add('  VALOR_LUCRO_BRUTO, ');
    vExex.SQL.add('  VALOR_LUCRO_LIQUIDO, ');
    vExex.SQL.add('  PERC_LUCRO_BRUTO, ');
    vExex.SQL.add('  PERC_LUCRO_LIQUIDO, ');
    vExex.SQL.add('  OUTRAS_DESPESAS, ');
    vExex.SQL.add('  DESCONTO ');
    vExex.SQL.add(') values ( ');
    vExex.SQL.add('  :P1, ');
    vExex.SQL.add('  :P2, ');
    vExex.SQL.add('  :P3, ');
    vExex.SQL.add('  :P4, ');
    vExex.SQL.add('  :P5, ');
    vExex.SQL.add('  :P6, ');
    vExex.SQL.add('  :P7, ');
    vExex.SQL.add('  :P8, ');
    vExex.SQL.add('  :P9, ');
    vExex.SQL.add('  :P10, ');
    vExex.SQL.add('  :P11, ');
    vExex.SQL.add('  :P12, ');
    vExex.SQL.add('  :P13, ');
    vExex.SQL.add('  :P14 ');
    vExex.SQL.add(')      ');

    for i := 0 to Length(produtos) - 1 do begin
      vExex.Executar([
        produtos[i].Posicao,
        produtos[i].ProdutoId,
        produtos[i].Nome,
        produtos[i].Marca,
        produtos[i].PrecoUnitario,
        produtos[i].Quantidade,
        produtos[i].Unidade,
        produtos[i].ValorTotal,
        produtos[i].ValorLucroBruto,
        produtos[i].ValorLucroLiquido,
        produtos[i].PercLucroBruto,
        produtos[i].PercLucroLiquido,
        produtos[i].OutrasDespesas,
        produtos[i].Desconto
      ]);
    end;

    vConsulta.SQL.Clear;
    vConsulta.SQL.Add('select COUNT(*) from TEMP_VEND_DEVOL');
    vConsulta.Pesquisar;
    retorno := vConsulta.GetInt(0);

    vConsulta.SQL.Clear;
    vConsulta.SQL.Add('select COUNT(*) from TEMP_PROD_VEND_DEVOL');
    vConsulta.Pesquisar;
    retorno := vConsulta.GetInt(0);

    pConexao.FinalizarTransacao;

    vConsulta.SQL.Clear;
    vConsulta.SQL.Add('select COUNT(*) from TEMP_VEND_DEVOL');
    vConsulta.Pesquisar;
    retorno := vConsulta.GetInt(0);

    vConsulta.SQL.Clear;
    vConsulta.SQL.Add('select COUNT(*) from TEMP_PROD_VEND_DEVOL');
    vConsulta.Pesquisar;
    retorno := vConsulta.GetInt(0);
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
    end;
  end;

  vConsulta.Free;
  vExex.Free;
end;

function AtualizarTipoAcompanhamentoOrcamento(
  pConexao: TConexao;
  pTipoAcompanhamentoId: Integer;
  pOrcamentoId: Integer
): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  vExec := TExecucao.Create(pConexao);
  vExec.SQL.Text := 'update ORCAMENTOS set TIPO_ACOMPANHAMENTO_ID = :P1 where ORCAMENTO_ID = :P2';

  try
    pConexao.IniciarTransacao;

    vExec.Executar([pTipoAcompanhamentoId, pOrcamentoId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

end;

end.
