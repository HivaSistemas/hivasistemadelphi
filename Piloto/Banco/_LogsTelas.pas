unit _LogsTelas;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils;

{$M+}
type
  TLogsTelas = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
//  protected
//    function GetRecordCentros: RecCentrosCustos;
  end;

function AtualizarLogs(
  pConexao: TConexao;
  pFuncionarioId: Integer;
  pForm: string
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TLogsTelas }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where  = :P1'
    );
end;

constructor TLogsTelas.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'LOGS_TELAS');

  FSql :=
    'select ' +
    '  FUNCIONARIO_ID, ' +
    '  FORM, ' +
    '  DATA_HORA ' +
    'from ' +
    '  LOGS_TELAS ';

  SetFiltros(GetFiltros);

  AddColuna('FUNCIONARIO_ID', True);
  AddColuna('FORM', True);
  AddColunaSL('DATA_HORA', True);
end;

//function TLogsTelas.GetRecordCentros: RecCentrosCustos;
//begin
//  Result := RecCentrosCustos.Create;
//
//  Result.CentroCustoId := GetInt('CENTRO_CUSTO_ID', True);
//  Result.Nome          := GetString('NOME');
//  Result.Ativo         := GetString('ATIVO');
//end;

function AtualizarLogs(
  pConexao: TConexao;
  pFuncionarioId: Integer;
  pForm: string
): RecRetornoBD;
var
  t: TLogsTelas;
begin
  Result.Iniciar;
  t := TLogsTelas.Create(pConexao);

  t.SetInt('FUNCIONARIO_ID', pFuncionarioId, True);
  t.SetString('FORM', pForm, True);

  try
    pConexao.IniciarTransacao;

    t.Inserir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
