unit _Funcionarios;

Interface

uses
  Vcl.Forms, _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros, System.Classes,
  System.Variants, Data.DB;

{$M+}
type
  TFuncionario = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordFuncionario: RecFuncionarios;
  end;

function AtualizarFuncionario(
  pConexao: TConexao;
  pFuncionarioId: Integer;
  pNome: string;
  pApelido: string;
  pDataNascimento: TDateTime;
  pFuncaoId: Integer;
  pCPF: string;
  pRG: string;
  pOrgaoExpedidorRg: string;
  pDataAdmissao: TDateTime;
  pLogradouro: string;
  pComplemento: string;
  pNumero: string;
  pBairroId: Integer;
  pCEP: string;
  pEmail: string;
  pServidorSmtp: string;
  pUsuarioEmail: string;
  pSenhaEmail: string;
  pPortaEmail: Integer;
  pCadastroId: Integer;
  pFoto: TMemoryStream;
  pPercentualDescontoAdicVenda: Double;
  pPercentualDescontoAdicFinan: Double;
  pPercentualComissaoVista: Double;
  pPercentualComissaoPrazo: Double;
  pPercDescItemPrecoManual: Double;
  pSalario: Double;
  pAtivo: string;
  pVendedor: string;
  pComprador: string;
  pCaixa: string;
  pMotorista: string;
  pAjudante: string;
  pNumeroCarteiraTrabalho: string;
  pSerieCarteiraTrabalho: string;
  pPisPasep: string;
  pDataDesligamento: TDateTime;
  pAcessaAltisW: string;
  pEmpresasId: TArray<Integer>;
  pFinalizarSessao: string;
  pDevolucaoSomenteFiscal: string;
  pAcessoHivaMobile: string
): RecRetornoBD;

function BuscarFuncionarios(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean;
  pVendedor: Boolean;
  pCaixa: Boolean;
  pComprador: Boolean;
  pAjudante: Boolean
): TArray<RecFuncionarios>;

function BuscarFuncionariosComando(
  pConexao: TConexao;
  pComando: string
): TArray<RecFuncionarios>;

function ExcluirFuncionario(
  pConexao: TConexao;
  pFuncionarioId: Integer
): RecRetornoBD;

function AtualizarSenhaFuncionario(pConexao: TConexao; const pUsuarioId: Integer; const pSenha: string; pSenhaAltisW: Boolean): RecRetornoBD;
function BuscarFuncionariosEnvioMensagem(pConexao: TConexao; pFuncionariosIds: TArray<Integer>; pEmpresasIds: TArray<Integer>; pFuncoesIds: TArray<Integer>): TArray<Integer>;
function BuscarEmpresasFuncionario(pConexao: TConexao; pFuncionarioId: Integer): TArray<Integer>;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TFuncionario }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 5);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where FUN.FUNCIONARIO_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome do funcionário',
      True,
      0,
      'where FUN.NOME like :P1 || ''%'' '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Função do funcionário',
      False,
      1,
      'where CFU.NOME like :P1 || ''%'' '
    );

  Result[3] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Apelido do funcionário',
      False,
      2,
      'where FUN.APELIDO = :P1 '
    );

  Result[4] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where FUN.CPF = :P1 '
    );
end;

constructor TFuncionario.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'FUNCIONARIOS');

  FSql :=
    'select ' +
    '  FUN.FUNCIONARIO_ID, ' +
    '  FUN.NOME, ' +
    '  FUN.SENHA_SISTEMA, ' +
    '  FUN.APELIDO, ' +
    '  FUN.DATA_CADASTRO, ' +
    '  FUN.DATA_NASCIMENTO, ' +
    '  FUN.CARGO_ID, ' +
    '  FUN.CPF, ' +
    '  FUN.RG, ' +
    '  FUN.ORGAO_EXPEDIDOR_RG, ' +
    '  FUN.DATA_ADMISSAO, ' +
    '  FUN.LOGRADOURO, ' +
    '  FUN.COMPLEMENTO, ' +
    '  FUN.NUMERO, ' +
    '  FUN.BAIRRO_ID, ' +
    '  FUN.CEP, ' +
    '  FUN.E_MAIL, ' +
    '  FUN.SERVIDOR_SMTP, ' +
    '  FUN.USUARIO_E_MAIL, ' +
    '  FUN.SENHA_E_MAIL, ' +
    '  FUN.PORTA_E_MAIL, ' +
    '  FUN.CADASTRO_ID, ' +
    '  FUN.PERCENTUAL_DESCONTO_ADIC_VENDA, ' +
    '  FUN.PERCENTUAL_DESCONTO_ADIC_FINAN, ' +
    '  FUN.PERCENTUAL_COMISSAO_A_VISTA, ' +
    '  FUN.PERCENTUAL_COMISSAO_A_PRAZO, ' +
    '  FUN.PERC_DESC_ITEM_PRECO_MANUAL, ' +
    '  FUN.SALARIO, ' +
    '  FUN.ATIVO, ' +
    '  FUN.VENDEDOR, ' +
    '  FUN.COMPRADOR, ' +
    '  FUN.CAIXA, ' +
    '  FUN.MOTORISTA, ' +
    '  FUN.AJUDANTE, ' +
    '  FUN.NUMERO_CARTEIRA_TRABALHO, ' +
    '  FUN.SERIE_CARTEIRA_TRABALHO, ' +
    '  FUN.PIS_PASEP, ' +
    '  FUN.DATA_DESLIGAMENTO, ' +
    '  FUN.DATA_ULTIMA_SENHA, ' +
    '  FUN.FINALIZAR_SESSAO,'+
    '  FUN.DEVOLUCAO_SOMENTE_FISCAL, '+
    '  FUN.ACESSO_HIVA_MOBILE, '+
   // '  FUN.ACESSA_ALTIS_W, ' +
    //'  FUN.SENHA_ALTIS_W, ' +
    '  CFU.NOME as NOME_FUNCAO ' +
    'from ' +
    '  FUNCIONARIOS FUN ' +

    'inner join CARGOS_FUNCIONARIOS CFU ' +
    'on FUN.CARGO_ID = CFU.CARGO_ID ';

  SetFiltros(GetFiltros);

  AddColuna('FUNCIONARIO_ID', True);
  AddColuna('NOME');
  AddColunaSL('SENHA_SISTEMA');
  AddColuna('APELIDO');
  AddColunaSL('DATA_CADASTRO');
  AddColuna('DATA_NASCIMENTO');
  AddColuna('CARGO_ID');
  AddColuna('CPF');
  AddColuna('RG');
  AddColuna('ORGAO_EXPEDIDOR_RG');
  AddColuna('DATA_ADMISSAO');
  AddColuna('LOGRADOURO');
  AddColuna('COMPLEMENTO');
  AddColuna('NUMERO');
  AddColuna('BAIRRO_ID');
  AddColuna('CEP');
  AddColuna('E_MAIL');
  AddColuna('SERVIDOR_SMTP');
  AddColuna('USUARIO_E_MAIL');
  AddColuna('SENHA_E_MAIL');
  AddColuna('PORTA_E_MAIL');
  AddColuna('CADASTRO_ID');
  AddColuna('PERCENTUAL_DESCONTO_ADIC_VENDA');
  AddColuna('PERCENTUAL_DESCONTO_ADIC_FINAN');
  AddColuna('PERCENTUAL_COMISSAO_A_VISTA');
  AddColuna('PERCENTUAL_COMISSAO_A_PRAZO');
  AddColuna('PERC_DESC_ITEM_PRECO_MANUAL');
  AddColuna('SALARIO');
  AddColuna('ATIVO');
  AddColuna('VENDEDOR');
  AddColuna('CAIXA');
  AddColuna('COMPRADOR');
  AddColuna('MOTORISTA');
  AddColuna('AJUDANTE');
  AddColuna('NUMERO_CARTEIRA_TRABALHO');
  AddColuna('SERIE_CARTEIRA_TRABALHO');
  AddColuna('PIS_PASEP');
  AddColuna('DATA_DESLIGAMENTO');
  AddColunaSL('DATA_ULTIMA_SENHA');
  AddColunaSL('NOME_FUNCAO');
  AddColuna('FINALIZAR_SESSAO');
  AddColuna('DEVOLUCAO_SOMENTE_FISCAL');
  AddColuna('ACESSO_HIVA_MOBILE');
 // AddColuna('ACESSA_ALTIS_W');
  //AddColunaSL('SENHA_ALTIS_W');
end;

function TFuncionario.GetRecordFuncionario: RecFuncionarios;
begin
  Result := RecFuncionarios.Create(Application);
  Result.funcionario_id              := getInt('FUNCIONARIO_ID', True);
  Result.nome                        := getString('NOME');
  Result.senha_sistema               := getString('SENHA_SISTEMA');
  Result.apelido                     := getString('APELIDO');
  Result.data_cadastro               := getData('DATA_CADASTRO');
  Result.data_nascimento             := getData('DATA_NASCIMENTO');
  Result.CargoId                     := getInt('CARGO_ID');
  Result.cpf                         := getString('CPF');
  Result.rg                          := getString('RG');
  Result.orgao_expedidor_rg          := getString('ORGAO_EXPEDIDOR_RG');
  Result.data_admissao               := getData('DATA_ADMISSAO');
  Result.logradouro                  := getString('LOGRADOURO');
  Result.complemento                 := getString('COMPLEMENTO');
  Result.numero                      := getString('NUMERO');
  Result.bairro_id                   := getInt('BAIRRO_ID');
  Result.cep                         := getString('CEP');
  Result.e_mail                      := getString('E_MAIL');
  Result.servidor_smtp               := getString('SERVIDOR_SMTP');
  Result.usuario_e_mail              := getString('USUARIO_E_MAIL');
  Result.senha_e_mail                := getString('SENHA_E_MAIL');
  Result.porta_e_mail                := getInt('PORTA_E_MAIL');
  Result.cadastro_id                 := getInt('CADASTRO_ID');
  Result.PercentualDescontoAdicVenda := getDouble('PERCENTUAL_DESCONTO_ADIC_VENDA');
  Result.PercentualDescontoAdicFinan := getDouble('PERCENTUAL_DESCONTO_ADIC_FINAN');
  Result.percentual_comissao_a_vista := getDouble('PERCENTUAL_COMISSAO_A_VISTA');
  Result.percentual_comissao_a_prazo := getDouble('PERCENTUAL_COMISSAO_A_PRAZO');
  Result.PercDescItemPrecoManual     := getDouble('PERC_DESC_ITEM_PRECO_MANUAL');
  Result.salario                     := getDouble('SALARIO');
  Result.ativo                       := getString('ATIVO');
  Result.Vendedor                    := getString('VENDEDOR');
  Result.Comprador                   := getString('COMPRADOR');
  Result.Motorista                   := getString('MOTORISTA');
  Result.Ajudante                    := getString('AJUDANTE');
  Result.NumeroCarteiraTrabalho      := getString('NUMERO_CARTEIRA_TRABALHO');
  Result.SerieCarteiraTrabalho       := getString('SERIE_CARTEIRA_TRABALHO');
  Result.PisPasep                    := getString('PIS_PASEP');
  Result.DataDesligamento            := getData('DATA_DESLIGAMENTO');
  Result.Caixa                       := getString('CAIXA');
  Result.DataUltimaSenha             := getData('DATA_ULTIMA_SENHA');
  Result.nome_funcao                 := getString('NOME_FUNCAO');
  Result.FinalizarSessao             := getString('FINALIZAR_SESSAO');
  Result.DevolucaoSomenteFiscal      := getString('DEVOLUCAO_SOMENTE_FISCAL');
  Result.AcessoHivaMobile            := getString('ACESSO_HIVA_MOBILE');
 // Result.AcessaAltisW                := getString('ACESSA_ALTIS_W');
 // Result.SenhaAltisW                 := getString('SENHA_ALTIS_W');
  Result.GerenteSistema              := Result.CargoId = 1;
end;

function AtualizarFuncionario(
  pConexao: TConexao;
  pFuncionarioId: Integer;
  pNome: string;
  pApelido: string;
  pDataNascimento: TDateTime;
  pFuncaoId: Integer;
  pCPF: string;
  pRG: string;
  pOrgaoExpedidorRg: string;
  pDataAdmissao: TDateTime;
  pLogradouro: string;
  pComplemento: string;
  pNumero: string;
  pBairroId: Integer;
  pCEP: string;
  pEmail: string;
  pServidorSmtp: string;
  pUsuarioEmail: string;
  pSenhaEmail: string;
  pPortaEmail: Integer;
  pCadastroId: Integer;
  pFoto: TMemoryStream;
  pPercentualDescontoAdicVenda: Double;
  pPercentualDescontoAdicFinan: Double;
  pPercentualComissaoVista: Double;
  pPercentualComissaoPrazo: Double;
  pPercDescItemPrecoManual: Double;
  pSalario: Double;
  pAtivo: string;
  pVendedor: string;
  pComprador: string;
  pCaixa: string;
  pMotorista: string;
  pAjudante: string;
  pNumeroCarteiraTrabalho: string;
  pSerieCarteiraTrabalho: string;
  pPisPasep: string;
  pDataDesligamento: TDateTime;
  pAcessaAltisW: string;
  pEmpresasId: TArray<Integer>;
  pFinalizarSessao: string;
  pDevolucaoSomenteFiscal: string;
  pAcessoHivaMobile: string
): RecRetornoBD;
var
  t: TFuncionario;
  novo: Boolean;
  ex: TExecucao;
  seq: TSequencia;
  i: Integer;
begin
  Result.Iniciar;

  pConexao.SetRotina('ATUALIZAR_FUNCIONARIOS');
  t := TFuncionario.Create(pConexao);
  novo := pFuncionarioId = 0;

  if novo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_FUNCIONARIO_ID');
    pFuncionarioId := seq.GetProximaSequencia;
    result.AsInt := pFuncionarioId;
    seq.Free;
  end;

  t.setInt('FUNCIONARIO_ID', pFuncionarioId, True);
  t.SetString('NOME', pNome);
  t.SetString('APELIDO', pApelido);
  t.setData('DATA_NASCIMENTO', pDataNascimento);
  t.setInt('CARGO_ID', pFuncaoId);
  t.setString('CPF', pCPF);
  t.setStringN('RG', pRG);
  t.setString('ORGAO_EXPEDIDOR_RG', pOrgaoExpedidorRg);
  t.setData('DATA_ADMISSAO', pDataAdmissao);
  t.setString('LOGRADOURO', pLogradouro);
  t.SetString('COMPLEMENTO', pComplemento);
  t.setString('NUMERO', pNumero);
  t.setInt('BAIRRO_ID', pBairroId);
  t.SetString('CEP', pCEP);
  t.SetString('E_MAIL', pEmail);
  t.SetString('SERVIDOR_SMTP', pServidorSmtp);
  t.SetString('USUARIO_E_MAIL', pUsuarioEmail);
  t.SetString('SENHA_E_MAIL', pSenhaEmail);
  t.setInt('PORTA_E_MAIL', pPortaEmail);
  t.SetInt('CADASTRO_ID', pCadastroId);
  t.SetDouble('PERCENTUAL_DESCONTO_ADIC_VENDA', pPercentualDescontoAdicVenda);
  t.SetDouble('PERCENTUAL_DESCONTO_ADIC_FINAN', pPercentualDescontoAdicFinan);
  t.SetDouble('PERCENTUAL_COMISSAO_A_VISTA', pPercentualComissaoVista);
  t.SetDouble('PERCENTUAL_COMISSAO_A_PRAZO', pPercentualComissaoPrazo);
  t.SetDouble('PERC_DESC_ITEM_PRECO_MANUAL', pPercDescItemPrecoManual);
  t.SetDouble('SALARIO', psalario);
  t.setString('ATIVO', pativo);
  t.setString('VENDEDOR', pVendedor);
  t.setString('COMPRADOR', pComprador);
  t.setString('CAIXA', pCaixa);
  t.setString('MOTORISTA', pMotorista);
  t.setString('AJUDANTE', pAjudante);
  t.setString('NUMERO_CARTEIRA_TRABALHO', pNumeroCarteiraTrabalho);
  t.setString('SERIE_CARTEIRA_TRABALHO', pSerieCarteiraTrabalho);
  t.setString('PIS_PASEP', pPisPasep);
  t.setDataN('DATA_DESLIGAMENTO', pDataDesligamento);
  t.setString('FINALIZAR_SESSAO', pFinalizarSessao);
  t.setString('DEVOLUCAO_SOMENTE_FISCAL', pDevolucaoSomenteFiscal);
  t.setString('ACESSO_HIVA_MOBILE', pAcessoHivaMobile);
 // t.setString('ACESSA_ALTIS_W', pAcessaAltisW);

  try
    pConexao.IniciarTransacao;

    if novo then
      t.Inserir
    else
      t.Atualizar;

    if pFoto <> nil then begin
      ex := TExecucao.Create(pConexao);
      ex.Add('update FUNCIONARIOS set ');
      ex.Add('  FOTO = :P1 ');
      ex.Add('where FUNCIONARIO_ID = :P2');
      ex.CarregarBinario('P1', pFoto);
      ex.ParamByName('P2').AsInteger := pFuncionarioId;
      ex.Executar;
      ex.Free;
    end;

    if pEmpresasId <> nil then  begin
      ex := TExecucao.Create(pConexao);
      ex.Add('delete from EMPRESAS_FUNCIONARIO where FUNCIONARIO_ID = :P1');
      ex.ParamByName('P1').AsInteger := pFuncionarioId;
      ex.Executar;
      ex.Free;

      ex := TExecucao.Create(pConexao);
      ex.Add('insert into EMPRESAS_FUNCIONARIO( ');
      ex.Add('  FUNCIONARIO_ID, ');
      ex.Add('  EMPRESA_ID) values ( ');
      ex.Add('  :P1, :P2) ');

      for I := Low(pEmpresasId) to High(pEmpresasId) do begin
        ex.ParamByName('P1').AsInteger := pFuncionarioId;
        ex.ParamByName('P2').AsInteger := pEmpresasId[i];
        ex.Executar;
      end;

      ex.Free;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function BuscarFuncionarios(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean;
  pVendedor: Boolean;
  pCaixa: Boolean;
  pComprador: Boolean;
  pAjudante: Boolean
): TArray<RecFuncionarios>;
var
  i: Integer;
  t: TFuncionario;
  vSqlAuxilar: string;

  function getFoto(pConexao: TConexao; pFuncionarioId: Integer): TMemoryStream;
  var
    p: TConsulta;
    s: TStream;
  begin
    Result := nil;

    p := TConsulta.Create(pConexao);
    p.SQL.Add('select FOTO from FUNCIONARIOS where FUNCIONARIO_ID = :P1');

    if p.Pesquisar([pFuncionarioId]) then begin
      if not(p.Fields[0].Value = null) then begin
        Result := TMemoryStream.Create;
        s := p.CreateBlobStream(p.FieldByName('FOTO'), bmRead);
        Result.LoadFromStream(s);
        s.Free;
      end;
    end;

    p.Free;
  end;

begin
  Result := nil;
  t := TFuncionario.Create(pConexao);

  vSqlAuxilar := '';
  if pSomenteAtivos then
    vSqlAuxilar := ' and FUN.ATIVO = ''S'' ';

  if pVendedor then
    vSqlAuxilar := vSqlAuxilar + ' and FUN.VENDEDOR = ''S'' ';

  if pCaixa then
    vSqlAuxilar := vSqlAuxilar + ' and FUN.CAIXA = ''S'' ';

  if pComprador then
    vSqlAuxilar := vSqlAuxilar + ' and FUN.COMPRADOR = ''S'' ';

  if pAjudante then
    vSqlAuxilar := vSqlAuxilar + ' and FUN.AJUDANTE = ''S'' ';

  if t.Pesquisar(pIndice, pFiltros, vSqlAuxilar) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordFuncionario;
      Result[i].foto := getFoto(pConexao, Result[i].funcionario_id);

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarFuncionariosComando(
  pConexao: TConexao;
  pComando: string
): TArray<RecFuncionarios>;
var
  i: Integer;
  t: TFuncionario;

  function getFoto(pConexao: TConexao; pFuncionarioId: Integer): TMemoryStream;
  var
    p: TConsulta;
    s: TStream;
  begin
    Result := nil;

    p := TConsulta.Create(pConexao);
    p.SQL.Add('select FOTO from FUNCIONARIOS where FUNCIONARIO_ID = :P1');

    if p.Pesquisar([pFuncionarioId]) then begin
      if not(p.Fields[0].Value = null) then begin
        Result := TMemoryStream.Create;
        s := p.CreateBlobStream(p.FieldByName('FOTO'), bmRead);
        Result.LoadFromStream(s);
        s.Free;
      end;
    end;

    p.Free;
  end;

begin
  Result := nil;
  t := TFuncionario.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordFuncionario;
      Result[i].foto := getFoto(pConexao, Result[i].funcionario_id);
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirFuncionario(
  pConexao: TConexao;
  pFuncionarioId: Integer
): RecRetornoBD;
var
  t: TFuncionario;
begin
  Result.Iniciar;
  t := TFuncionario.Create(pConexao);

  t.setInt('FUNCIONARIO_ID', pFuncionarioId, True);

  try
    pConexao.IniciarTransacao;

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  t.Free;
end;

function AtualizarSenhaFuncionario(pConexao: TConexao; const pUsuarioId: Integer; const pSenha: string; pSenhaAltisW: Boolean): RecRetornoBD;
var
  vSql: TExecucao;
begin
  Result.TeveErro := False;
  vSql := TExecucao.Create(pConexao);
  pConexao.SetRotina('ALTERAR_SENHA_USUARIO');

  try
    pConexao.IniciarTransacao;

    vSql.SQL.Add('update FUNCIONARIOS set ');

    if not pSenhaAltisW then
      vSql.SQL.Add('  SENHA_SISTEMA = :P2')
    else
      vSql.SQL.Add('  SENHA_ALTIS_W = :P2');

    vSql.SQL.Add('where FUNCIONARIO_ID = :P1');

    vSql.Executar([pUsuarioId, pSenha]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vSql.Free;
end;

function BuscarFuncionariosEnvioMensagem(pConexao: TConexao; pFuncionariosIds: TArray<Integer>; pEmpresasIds: TArray<Integer>; pFuncoesIds: TArray<Integer>): TArray<Integer>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select distinct ');
  vSql.SQL.Add('  FUNCIONARIO_ID ');
  vSql.SQL.Add('from ');
  vSql.SQL.Add('  FUNCIONARIOS ');
  vSql.SQL.Add('where ');

  if pFuncionariosIds <> nil then
    vSql.SQL.Add('  ' + FiltroInInt('FUNCIONARIO_ID', pFuncionariosIds));

  if pEmpresasIds <> nil then
    vSql.SQL.Add('  ' + FiltroInInt('EMPRESA_ID', pEmpresasIds));

  if pFuncoesIds <> nil then
    vSql.SQL.Add('  ' + FiltroInInt('CARGO_ID', pFuncoesIds));

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i] := vSql.GetInt(0);
      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function BuscarEmpresasFuncionario(pConexao: TConexao; pFuncionarioId: Integer): TArray<Integer>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  EMPRESA_ID ');
  vSql.SQL.Add('from ');
  vSql.SQL.Add('  EMPRESAS_FUNCIONARIO ');
  vSql.SQL.Add('where FUNCIONARIO_ID = :P1 ');
  vSql.SQL.Add('order by EMPRESA_ID asc ');

  if vSql.Pesquisar([pFuncionarioId]) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i] := vSql.GetInt('EMPRESA_ID');
      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

end.
