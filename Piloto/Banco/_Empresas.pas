unit _Empresas;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros,
  System.Variants, Vcl.Forms;

{$M+}
type
  TEmpresa = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordEmpresa: RecEmpresas;
  end;

function AtualizarEmpresa(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pNomeFantasia: string;
  pRazaoSocial: string;
  pNomeResumidoImpressoesNF: string;
  pTipoEmpresa: string;
  pCadastroId: Integer;
  pBairroId: Integer;
  pLogradouro: string;
  pComplemento: string;
  pNumero: string;
  pPontoReferencia: string;
  pCEP: string;
  pTelefonePrincipal: string;
  pTelefoneFax: string;
  pEmail: string;
  pDataFundacao: TDateTime;
  pInscricaoEstadual: string;
  pInscricaoMunicipal: string;
  pCnae: string;
  pCnpj: string;
  pAtivo: string
): RecRetornoBD;

function BuscarEmpresas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEmpresas>;
function BuscarEmpresasComando(pConexao: TConexao; pComando: string): TArray<RecEmpresas>;

function ExcluirEmpresa(
  pConexao: TConexao;
  pEmpresaId: Integer
): RecRetornoBD;

function QuantidadeEmpresas(pConexao: TConexao): Word;
function BuscarIdsEmpresas(pConexao: TConexao): TArray<Integer>;
function CNPJCadastrado(pConexao: TConexao; pCNPJ: string): Boolean;
function AtualizarDataValidadeEmpresa(pConexao: TConexao; pEmpresaId: Integer; pDataValidade: string): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TEmpresa }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 6);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where EMPRESA_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Raz�o social',
      True,
      0,
      'where RAZAO_SOCIAL like :P1 || ''%'' ' +
      'order by ' +
      '  NOME_FANTASIA '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome fantasia',
      False,
      1,
      'where NOME_FANTASIA like :P1 || ''%'' ' +
      'order by ' +
      '  NOME_FANTASIA '
    );

  Result[3] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'CNPJ',
      False,
      2,
      'where CNPJ = :P1 ' +
      'order by ' +
      '  NOME_FANTASIA '
    );

  Result[4] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      2,
      'order by EMPRESA_ID'
    );

  Result[5] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      2,
      'where EMP.ATIVO = ''S'' ' +
      'order by EMP.EMPRESA_ID'
    );
end;

constructor TEmpresa.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'EMPRESAS');

  FSql :=
    'select ' +
    '  EMP.EMPRESA_ID, ' +
    '  EMP.CADASTRO_ID, ' +
    '  EMP.BAIRRO_ID, ' +
    '  BAI.CIDADE_ID, ' +
    '  BAI.NOME_BAIRRO, ' +
    '  EMP.LOGRADOURO, ' +
    '  EMP.COMPLEMENTO, ' +
    '  EMP.NUMERO, ' +
    '  EMP.PONTO_REFERENCIA, ' +
    '  EMP.CEP, ' +
    '  EMP.TELEFONE_PRINCIPAL, ' +
    '  EMP.TELEFONE_FAX, ' +
    '  EMP.E_MAIL, ' +
    '  EMP.DATA_FUNDACAO, ' +
    '  EMP.INSCRICAO_ESTADUAL, ' +
    '  EMP.INSCRICAO_MUNICIPAL, ' +
    '  EMP.CNAE, ' +
    '  EMP.CNPJ, ' +
    '  EMP.NOME_FANTASIA, ' +
    '  EMP.NOME_RESUMIDO_IMPRESSOES_NF, ' +
    '  EMP.ATIVO, ' +
    '  EMP.RAZAO_SOCIAL, ' +
    '  EMP.TIPO_EMPRESA, ' +
    '  EMP.DATA_VALIDADE, ' +
    '  EMP.DATA_ULTIMA_CONSULTA_API, '+
    '  CID.NOME as NOME_CIDADE, ' +
    '  CID.ESTADO_ID, ' +
    '  CID.CODIGO_IBGE as CODIGO_IBGE_CIDADE, ' +
    '  EST.CODIGO_IBGE as CODIGO_IBGE_ESTADO ' +
    'from ' +
    '  EMPRESAS EMP ' +

    'inner join VW_BAIRROS BAI ' +
    'on EMP.BAIRRO_ID = BAI.BAIRRO_ID ' +

    'inner join CIDADES CID ' +
    'on BAI.CIDADE_ID = CID.CIDADE_ID ' +

    'inner join ESTADOS EST ' +
    'on CID.ESTADO_ID = EST.ESTADO_ID ';

  setFiltros(getFiltros);

  AddColuna('EMPRESA_ID', True);
  AddColuna('CADASTRO_ID');
  AddColuna('BAIRRO_ID');
  AddColunaSL('CIDADE_ID');
  AddColuna('LOGRADOURO');
  AddColuna('COMPLEMENTO');
  AddColuna('NUMERO');
  AddColuna('PONTO_REFERENCIA');
  AddColuna('CEP');
  AddColuna('TELEFONE_PRINCIPAL');
  AddColuna('TELEFONE_FAX');
  AddColuna('E_MAIL');
  AddColuna('DATA_FUNDACAO');
  AddColuna('INSCRICAO_ESTADUAL');
  AddColuna('INSCRICAO_MUNICIPAL');
  AddColuna('CNAE');
  AddColuna('CNPJ');
  AddColuna('NOME_FANTASIA');
  AddColuna('NOME_RESUMIDO_IMPRESSOES_NF');
  AddColuna('ATIVO');
  AddColuna('RAZAO_SOCIAL');
  AddColuna('TIPO_EMPRESA');
  AddColunaSL('ESTADO_ID');
  AddColunaSL('NOME_BAIRRO');
  AddColunaSL('NOME_CIDADE');
  AddColunaSL('CODIGO_IBGE_CIDADE');
  AddColunaSL('CODIGO_IBGE_ESTADO');
  AddColuna('DATA_VALIDADE');
  AddColuna('DATA_ULTIMA_CONSULTA_API');
end;

function TEmpresa.getRecordEmpresa: RecEmpresas;
begin
  Result := RecEmpresas.Create(Application);
  Result.EmpresaId                := getInt('EMPRESA_ID', True);
  Result.NomeFantasia             := getString('NOME_FANTASIA');
  Result.NomeResumidoImpressoesNF := getString('NOME_RESUMIDO_IMPRESSOES_NF');
  Result.RazaoSocial              := getString('RAZAO_SOCIAL');
  Result.TipoEmpresa              := getString('TIPO_EMPRESA');
  Result.CadastroId               := getInt('CADASTRO_ID');
  Result.BairroId                 := getInt('BAIRRO_ID');
  Result.CidadeId                 := getInt('CIDADE_ID');
  Result.Logradouro               := getString('LOGRADOURO');
  Result.Complemento              := getString('COMPLEMENTO');
  Result.Numero                   := getString('NUMERO');
  Result.PontoReferencia          := getString('PONTO_REFERENCIA');
  Result.Cep                      := getString('CEP');
  Result.TelefonePrincipal        := getString('TELEFONE_PRINCIPAL');
  Result.TelefoneFax              := getString('TELEFONE_FAX');
  Result.EMail                    := getString('E_MAIL');
  Result.DataFundacao             := getData('DATA_FUNDACAO');
  Result.InscricaoEstadual        := getString('INSCRICAO_ESTADUAL');
  Result.InscricaoMunicipal       := getString('INSCRICAO_MUNICIPAL');
  Result.Cnae                     := getString('CNAE');
  Result.Cnpj                     := getString('CNPJ');
  Result.EstadoId                 := getString('ESTADO_ID');
  Result.NomeBairro               := getString('NOME_BAIRRO');
  Result.NomeCidade               := getString('NOME_CIDADE');
  Result.CodigoIBGECidade         := getInt('CODIGO_IBGE_CIDADE');
  Result.CodigoIBGEEstado         := getInt('CODIGO_IBGE_ESTADO');
  Result.Ativo                    := getString('ATIVO');
  Result.DataValidade             := getString('DATA_VALIDADE');
  Result.DataUltimaConsultaAPI    := getData('DATA_ULTIMA_CONSULTA_API');
end;

function AtualizarEmpresa(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pNomeFantasia: string;
  pRazaoSocial: string;
  pNomeResumidoImpressoesNF: string;
  pTipoEmpresa: string;
  pCadastroId: Integer;
  pBairroId: Integer;
  pLogradouro: string;
  pComplemento: string;
  pNumero: string;
  pPontoReferencia: string;
  pCEP: string;
  pTelefonePrincipal: string;
  pTelefoneFax: string;
  pEmail: string;
  pDataFundacao: TDateTime;
  pInscricaoEstadual: string;
  pInscricaoMunicipal: string;
  pCnae: string;
  pCnpj: string;
  pAtivo: string
): RecRetornoBD;
var
  t: TEmpresa;
  vNovo: Boolean;
  seq: TSequencia;
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  t := TEmpresa.Create(pConexao);
  vProc := TProcedimentoBanco.Create(pConexao, 'INICIAR_INFORMACOES_EMPRESA');

  vNovo := pEmpresaId = 0;
  if vNovo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_EMPRESA_ID');
    pEmpresaId := seq.getProximaSequencia;
    Result.AsInt := pEmpresaId;
    seq.Free;
  end;

  t.setInt('EMPRESA_ID', pEmpresaId, True);
  t.setString('NOME_FANTASIA', pNomeFantasia);
  t.setString('RAZAO_SOCIAL', pRazaoSocial);
  t.setString('NOME_RESUMIDO_IMPRESSOES_NF', pNomeResumidoImpressoesNF);
  t.setString('TIPO_EMPRESA', pTipoEmpresa);
  t.setIntN('CADASTRO_ID', pCadastroId);
  t.setInt('BAIRRO_ID', pBairroId);
  t.setString('LOGRADOURO', pLogradouro);
  t.setString('COMPLEMENTO', pComplemento);
  t.setString('NUMERO', pNumero);
  t.setString('PONTO_REFERENCIA', pPontoReferencia);
  t.setString('CEP', pCEP);
  t.setString('TELEFONE_PRINCIPAL', pTelefonePrincipal);
  t.setString('TELEFONE_FAX', pTelefoneFax);
  t.setString('E_MAIL', pEmail);
  t.setDataN('DATA_FUNDACAO', pDataFundacao);
  t.setStringN('INSCRICAO_ESTADUAL', pInscricaoEstadual);
  t.setString('INSCRICAO_MUNICIPAL', pInscricaoMunicipal);
  t.setString('CNAE', pCnae);
  t.setString('CNPJ', pCnpj);
  t.setString('ATIVO', pAtivo);

  try
    pConexao.IniciarTransacao;

    if vNovo then begin
      t.Inserir;
      vProc.Executar([pEmpresaId]);
    end
    else
      t.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vProc.Free;
  t.Free;
end;

function BuscarEmpresas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEmpresas>;
var
  i: Integer;
  t: TEmpresa;
begin
  Result := nil;
  t := TEmpresa.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordEmpresa;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarEmpresasComando(pConexao: TConexao; pComando: string): TArray<RecEmpresas>;
var
  i: Integer;
  t: TEmpresa;
begin
  Result := nil;
  t := TEmpresa.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordEmpresa;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirEmpresa(
  pConexao: TConexao;
  pEmpresaId: Integer
): RecRetornoBD;
var
  t: TEmpresa;
begin
  Result.TeveErro := False;
  t := TEmpresa.Create(pConexao);

  t.setInt('EMPRESA_ID', pEmpresaId, True);

  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function QuantidadeEmpresas(pConexao: TConexao): Word;
var
  p: TConsulta;
begin
  p := TConsulta.Create(pConexao);

  p.SQL.Clear;
  p.SQL.Add('select count(EMPRESA_ID) from EMPRESAS ');
  p.Pesquisar([]);

  Result := p.GetInt(0);

  p.Free;
end;

function BuscarIdsEmpresas(pConexao: TConexao): TArray<Integer>;
var
  i: Integer;
  p: TConsulta;
begin
  Result := nil;
  p := TConsulta.Create(pConexao);

  p.SQL.Clear;
  p.SQL.Add('select EMPRESA_ID from EMPRESAS ');

  if p.Pesquisar([]) then begin
    SetLength(Result, p.GetQuantidadeRegistros);
    for i := 0 to p.GetQuantidadeRegistros - 1 do begin
      Result[i] := p.GetInt(0);
      p.Next;
    end;
  end;

  p.Free;
end;

function CNPJCadastrado(pConexao: TConexao; pCNPJ: string): Boolean;
var
  p: TConsulta;
begin
  p := TConsulta.Create(pConexao);

  p.Add('select ');
  p.Add('  count(*) ');
  p.Add('from ');
  p.Add('  EMPRESAS ');
  p.Add('where RETORNA_NUMEROS(CNPJ) = :P1 ');

  p.Pesquisar([ RetornaNumeros(pCNPJ) ]);
  Result := p.GetInt(0) > 0;

  p.Free;
end;

function AtualizarDataValidadeEmpresa(pConexao: TConexao; pEmpresaId: Integer; pDataValidade: string): RecRetornoBD;
var
  vExecucao: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATU_DATA_VALIDADE_EMPRESA');
  vExecucao := TExecucao.Create(pConexao);

  vExecucao.Add('update EMPRESAS set ');
  vExecucao.Add('  DATA_VALIDADE = :P2, ');
  vExecucao.Add('  DATA_ULTIMA_CONSULTA_API = TRUNC(SYSDATE) ');
  vExecucao.Add('where EMPRESA_ID = :P1');
  try
    pConexao.IniciarTransacao;

    vExecucao.Executar([pEmpresaId, pDataValidade]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pConexao.EmTransacao then
        pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

end;

end.
