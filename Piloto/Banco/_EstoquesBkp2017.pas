unit _EstoquesBkp2017;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsEstoques;

{$M+}
type
  TEstoqueBkp2017 = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordEstoque: RecEstoqueBkp2017;
  end;

function BuscarEstoque(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant
): TArray<RecEstoqueBkp2017>;

function ProdutoCadastradoEstoqueEmpresa(pConexao: TConexao; pProdutoId: Integer; pEmpresaId: Integer): Boolean;

function getFiltros: TArray<RecFiltros>;

implementation

{ TEstoque }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where PRODUTO_ID = :P1 ' +
      'and EMPRESA_ID = :P2 '
    );
end;

constructor TEstoqueBkp2017.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ESTOQUES_BKP_2017');

  FSql :=
    'select ' +
    '  PRODUTO_ID, ' +
    '  EMPRESA_ID, ' +
    '  FISICO, ' +
    '  RESERVADO ' +
    'from ' +
    '  ESTOQUES_BKP_2017 ';

  setFiltros(getFiltros);

  AddColuna('PRODUTO_ID', True);
  AddColuna('EMPRESA_ID', True);
  AddColuna('FISICO');
  AddColuna('RESERVADO');
end;

function TEstoqueBkp2017.getRecordEstoque: RecEstoqueBkp2017;
begin
  Result.produto_id        := getInt('PRODUTO_ID', True);
  Result.empresa_id        := getInt('EMPRESA_ID', True);
  Result.fisico            := getDouble('ESTOQUE');
  Result.reservado         := getDouble('RESERVADO');
end;

function BuscarEstoque(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant
): TArray<RecEstoqueBkp2017>;
var
  i: Integer;
  Est: TEstoqueBkp2017;
begin
  Result := nil;
  Est := TEstoqueBkp2017.Create(pConexao);

  if Est.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, Est.GetQuantidadeRegistros);
    for i := 0 to Est.GetQuantidadeRegistros -1 do begin
      Result[i] := Est.getRecordEstoque;
      Est.ProximoRegistro;
    end;
  end;
  Est.Free;
end;

function ProdutoCadastradoEstoqueEmpresa(pConexao: TConexao; pProdutoId: Integer; pEmpresaId: Integer): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);
  vSql.SQL.Add('select count(PRODUTO_ID) from ESTOQUES_BKP_2017 where PRODUTO_ID = :P1 and EMPRESA_ID = :P2');
  vSql.Pesquisar([pProdutoId, pEmpresaId]);

  Result := vSql.GetInt(0) > 0;

  vSql.Active := False;
  vSql.Free;
end;

end.
