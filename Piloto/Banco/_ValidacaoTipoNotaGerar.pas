unit _ValidacaoTipoNotaGerar;

interface

uses
  System.Variants, _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsOrcamentosVendas,
  System.Math, _OrcamentosItens, _OrcamentosPagamentos, _OrcamentosPagamentosCheques, System.StrUtils, _BibliotecaGenerica,
  _RecordsExpedicao, _Retiradas, _OrcamentosItensDefLotes, _RecordsFinanceiros, _OrcamentosCreditos;

{$M+}
type
  RecValidacaoTipoNotaGerar = record
    ExigirNotaProduto: Boolean;
    ExigirNotaCliente: Boolean;
    ExigirNotaCondicaoPagamento: Boolean;
    SomenteNFe: Boolean;
  end;

function BuscarDadosValidacao(pConexao: TConexao; pCadastroId: Integer; pOrcamentoId: Integer; pCondicaoId: Integer): RecValidacaoTipoNotaGerar;

implementation

function BuscarDadosValidacao(pConexao: TConexao; pCadastroId: Integer; pOrcamentoId: Integer; pCondicaoId: Integer): RecValidacaoTipoNotaGerar;
var
  gerar_nfe_produto: Boolean;
  vSql: TConsulta;
begin
  Result.ExigirNotaProduto := False;
  Result.ExigirNotaCliente := False;
  Result.ExigirNotaCondicaoPagamento := False;

  vSql := TConsulta.Create(pConexao);

  vSql.Add('SELECT ');
  vSql.Add('  CASE ');
  vSql.Add('    WHEN COUNT(CASE WHEN EXIGIR_MODELO_NOTA_FISCAL = ''S'' THEN 1 END) > 0 THEN ''S'' ');
  vSql.Add('    ELSE ''N'' ');
  vSql.Add('  END AS EXIGIR_MODELO_NOTA_FISCAL, ');
  vSql.Add('  CASE ');
  vSql.Add('    WHEN COUNT(CASE WHEN EMITIR_SOMENTE_NFE = ''S'' THEN 1 END) > 0 THEN ''S'' ');
  vSql.Add('    ELSE ''N'' ');
  vSql.Add('  END AS GERAR_NFE ');
  vSql.Add('FROM ');
  vSql.Add('  orcamentos_itens ite ');

  vSql.Add('INNER JOIN produtos pro ');
  vSql.Add('ON ite.produto_id = pro.produto_id ');

  vSql.Add('WHERE ite.orcamento_id = :P1 ');

  vSql.Add('union all ');

  vSql.Add('select ');
  vSql.Add('  EXIGIR_MODELO_NOTA_FISCAL, ');
  vSql.Add('  ''N'' as GERAR_NFE ');
  vSql.Add('from ');
  vSql.Add('  CONDICOES_PAGAMENTO ');
  vSql.Add('where CONDICAO_ID = :P2 ');

  vSql.Add('union all ');

  vSql.Add('select ');
  vSql.Add('  CLI.EXIGIR_MODELO_NOTA_FISCAL, ');
  vSql.Add('  case when CLI.EMITIR_SOMENTE_NFE = ''N'' then case when CAD.TIPO_PESSOA = ''J'' then ''S'' else ''N'' end else ''S'' end as GERAR_NFE ');
  vSql.Add('from ');
  vSql.Add('  CLIENTES CLI ');

  vSql.Add('inner join CADASTROS CAD ');
  vSql.Add('on CLI.CADASTRO_ID = CAD.CADASTRO_ID ');

  vSql.Add('where CLI.CADASTRO_ID = :P3 ');

//  exigirModeloNotaFiscalProduto: string;
//  gerar_nfe_produto: string;
//  exigirModeloNotaFiscalCondicao: string;
//  exibirModeloNotaFiscalCliente: string;
//  gerar_nfe_cliente: string;

  if vSql.Pesquisar([pOrcamentoId, pCondicaoId, pCadastroId]) then begin
    Result.ExigirNotaProduto      := vSql.GetString('EXIGIR_MODELO_NOTA_FISCAL') = 'S';
    gerar_nfe_produto             := vSql.GetString('GERAR_NFE') = 'S';

    vSql.Next;
    Result.ExigirNotaCondicaoPagamento := vSql.GetString('EXIGIR_MODELO_NOTA_FISCAL') = 'S';

    vSql.Next;
    Result.ExigirNotaCliente := vSql.GetString('EXIGIR_MODELO_NOTA_FISCAL') = 'S';
    Result.SomenteNFe        := (vSql.GetString('GERAR_NFE') = 'S') or (gerar_nfe_produto);
  end;

  vSql.Free;
end;


end.

