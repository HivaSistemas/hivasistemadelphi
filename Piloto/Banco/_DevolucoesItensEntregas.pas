unit _DevolucoesItensEntregas;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, _Biblioteca;

{$M+}
type
  RecDevolucoesItensEntregas = record
    DevolucaoId: Integer;
    ItemId: Integer;
    Lote: string;
    LocalDevolucaoId: Integer;
    EntregaId: Integer;
    Quantidade: Double;
  end;

  TDevolucoesItensEntregas = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordDevolucoesItensEnt: RecDevolucoesItensEntregas;
  end;

function AtualizarDevolucoesItensEntregas(
  pConexao: TConexao;
  pDevolucaoId: Integer;
  pLocalDevolucaoId: Integer;
  pItens: TArray<RecDevolucoesItensEntregas>;
  pEmTransacao: Boolean
): RecRetornoBD;

function BuscarDevolucoesItensEnt(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecDevolucoesItensEntregas>;

function BuscarDevolucoesItensComandoEnt(pConexao: TConexao; pComando: string): TArray<RecDevolucoesItensEntregas>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TTDevolucoesItens }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ITE.DEVOLUCAO_ID = :P1 '
    );
end;

constructor TDevolucoesItensEntregas.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'DEVOLUCOES_ITENS_ENTREGAS');

  FSql :=
    'select ' +
    '  ITE.DEVOLUCAO_ID, ' +
    '  ITE.ITEM_ID, ' +
    '  ITE.LOTE, ' +
    '  ITE.LOCAL_DEVOLUCAO_ID, ' +
    '  ITE.ENTREGA_ID, ' +
    '  ITE.QUANTIDADE ' +
    'from ' +
    '  DEVOLUCOES_ITENS_ENTREGAS ITE ';

  setFiltros(getFiltros);

  AddColuna('DEVOLUCAO_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('LOTE', True);
  AddColuna('ENTREGA_ID', True);
  AddColuna('LOCAL_DEVOLUCAO_ID');
  AddColuna('QUANTIDADE');
end;

function TDevolucoesItensEntregas.getRecordDevolucoesItensEnt: RecDevolucoesItensEntregas;
begin
  Result.DevolucaoId      := getInt('DEVOLUCAO_ID', True);
  Result.ItemId           := getInt('ITEM_ID', True);
  Result.Lote             := getString('LOTE', True);
  Result.EntregaId        := getInt('ENTREGA_ID', True);
  Result.LocalDevolucaoId := getInt('LOCAL_DEVOLUCAO_ID');
  Result.Quantidade       := getDouble('QUANTIDADE');
end;

function AtualizarDevolucoesItensEntregas(
  pConexao: TConexao;
  pDevolucaoId: Integer;
  pLocalDevolucaoId: Integer;
  pItens: TArray<RecDevolucoesItensEntregas>;
  pEmTransacao: Boolean
): RecRetornoBD;
var
  i: Integer;
  vItem: TDevolucoesItensEntregas;
begin
  Result.Iniciar;
  vItem := TDevolucoesItensEntregas.Create(pConexao);
  pConexao.SetRotina('ATUALIZAR_DEVOLUCAO_ITENS_ENT');

  try
    if not pEmTransacao then
      pConexao.IniciarTransacao;

    for i := Low(pItens) to High(pItens) do begin
      vItem.SetInt('DEVOLUCAO_ID', pDevolucaoId, True);
      vItem.SetInt('ITEM_ID', pItens[i].ItemId, True);
      vItem.setIntN('ENTREGA_ID', pItens[i].EntregaId, True);
      vItem.setString('LOTE', pItens[i].Lote, True);

      vItem.SetInt('LOCAL_DEVOLUCAO_ID', pLocalDevolucaoId);
      vItem.setDouble('QUANTIDADE', pItens[i].Quantidade);

      vItem.Inserir;
    end;

    if not pEmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pEmTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;
  vItem.Free;
end;

function BuscarDevolucoesItensEnt(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecDevolucoesItensEntregas>;
var
  i: Integer;
  t: TDevolucoesItensEntregas;
begin
  Result := nil;
  t := TDevolucoesItensEntregas.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordDevolucoesItensEnt;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarDevolucoesItensComandoEnt(pConexao: TConexao; pComando: string): TArray<RecDevolucoesItensEntregas>;
var
  i: Integer;
  t: TDevolucoesItensEntregas;
begin
  Result := nil;
  t := TDevolucoesItensEntregas.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordDevolucoesItensEnt;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
