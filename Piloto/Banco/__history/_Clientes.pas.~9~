unit _Clientes;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros, _Cadastros,
  _CadastrosTelefones, _DiversosEnderecos, System.Variants, _ClientesCondicPagtoRestrit, _RecordsRelatorios;

{$M+}
type
  TCliente = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao; pSomenteAtivos: Boolean);
  protected
    function GetRecordCliente: RecClientes;
  end;

function AtualizarCliente(
  pConexao: TConexao;
  pPesquisouCadastro: Boolean;
  pCadastroId: Integer;
  pCadastro: RecCadastros;
  pTelefones: TArray<RecCadastrosTelefones>;
  pEnderecos: TArray<RecDiversosEnderecos>;
  pTipoCliente: string;
  pCodigoSpc: string;
  pDataPesquisaSpc: TDateTime;
  pLocalSpc: string;
  pDataPesquisaSerasa: TDateTime;
  pLocalSerasa: string;
  pAtendenteSpc: string;
  pRestricaoSpc: string;
  pDataRestricaoSpc: TDateTime;
  pAtendenteSerasa: string;
  pRestricaoSerasa: string;
  pDataRestricaoSerasa: TDateTime;
  pCondicaoId: Integer;
  pVendedorId: Integer;
  pEmitirSomenteNfe: string;
  pExigirModeloNotaFiscal: string;
  pCondicoesPagtosRestritasIds: TArray<Integer>;
  pLimiteCredito: Double;
  pLimiteCreditoMensal: Double;
  pDataAprovLimiteCredito: TDateTime;
  pNaoGerarComissao: string;
  pAtivo: string;
  pExigirNotaSimplesFat: string;
  pTipoPreco: string;
  pDataValidadeLimiteCredito: TDateTime;
  pQtdeDiasVencimentoAcumulado: Integer;
  pIgnorarRedirRegraNotaFisc: string;
  pIndiceDescontoVendaId: Integer;
  pListaNegraId: Integer;
  pObservacoeListaNegra: string;
  pIgnorarBloqueiosVenda: string;
  pGrupoClienteId: Integer;
  pObservacoesVenda: string;
  pUsuarioBanco: string;
  pSenhaBanco: string;
  pIpServidor: string;
  pServicoOracle: string;
  pQtdEstacoes: string;
  pResponsavelEmp: string;
  pValorAdesao: string;
  pValorMens: string;
  pDataAdesao: TDateTime;
  pDataVenc: string;
  pPorta: string;
  pCalcularIndiceDesconto: string
): RecRetornoBD;

function BuscarClientes(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecClientes>;

function ExcluirCliente(
  pConexao: TConexao;
  pCadastroId: Integer
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

function ECliente(pConexao: TConexao; pCadastroId: Integer): Boolean;
function BuscarDadosVidaCliente(pConexao: TConexao; pCadastroId: Integer): TArray<RecDadosVidaCliente>;
function BuscarLimiteCreditoClientes(pConexao: TConexao; pComando: string): TArray<RecDadosLimiteCreditoCliente>;
function ValidadeLimiteCreditoVencida(pConexao: TConexao; pClienteId: Integer): Boolean;

implementation

{ TCliente }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 4);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'and CLI.CADASTRO_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome fantasia (Avan�ado)',
      False,
      0,
      'and CAD.NOME_FANTASIA like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  CAD.NOME_FANTASIA ',
      '',
      True
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Raz�o social (Avan�ado)',
      True,
      1,
      'and CAD.RAZAO_SOCIAL like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  CAD.RAZAO_SOCIAL ',
      '',
      True
    );

  Result[3] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'CPF/CNPJ',
      False,
      2,
      'and RETORNA_NUMEROS(CAD.CPF_CNPJ) = :P1 ' +
      'order by ' +
      '  CAD.CPF_CNPJ '
    );
end;

constructor TCliente.Create(pConexao: TConexao; pSomenteAtivos: Boolean);
begin
  inherited Create(pConexao, 'CLIENTES');

  FSql :=
    'select ' +
    '  CLI.CADASTRO_ID, ' +
    '  CLI.TIPO_CLIENTE, ' +
    '  CLI.CODIGO_SPC, ' +
    '  CLI.DATA_PESQUISA_SPC, ' +
    '  CLI.LOCAL_SPC, ' +
    '  CLI.DATA_PESQUISA_SERASA, ' +
    '  CLI.LOCAL_SERASA, ' +
    '  CLI.ATENDENTE_SPC, ' +
    '  CLI.RESTRICAO_SPC, ' +
    '  CLI.DATA_RESTRICAO_SPC, ' +
    '  CLI.ATENDENTE_SERASA, ' +
    '  CLI.RESTRICAO_SERASA, ' +
    '  CLI.DATA_RESTRICAO_SERASA, ' +
    '  CLI.CONDICAO_ID, ' +
    '  CLI.VENDEDOR_ID, ' +
    '  CLI.EMITIR_SOMENTE_NFE, ' +
    '  CLI.ATIVO, ' +
    '  CLI.LIMITE_CREDITO, ' +
    '  CLI.LIMITE_CREDITO_MENSAL, ' +
    '  CLI.DATA_APROVACAO_LIMITE_CREDITO, ' +
    '  CLI.USUARIO_APROV_LIMITE_CRED_ID, ' +
    '  CLI.NAO_GERAR_COMISSAO, ' +
    '  CLI.EXIGIR_NOTA_SIMPLES_FAT, ' +
    '  CLI.EXIGIR_MODELO_NOTA_FISCAL, ' +
    '  CLI.TIPO_PRECO, ' +
    '  CLI.DATA_VALIDADE_LIMITE_CREDITO, ' +
    '  CLI.QTDE_DIAS_VENCIMENTO_ACUMULADO, ' +
    '  CLI.IGNORAR_REDIR_REGRA_NOTA_FISC, ' +
    '  CLI.INDICE_DESCONTO_VENDA_ID, ' +
    '  CLI.LISTA_NEGRA_ID, ' +
    '  CLI.OBSERVACOES_LISTA_NEGRA, ' +
    '  CLI.IGNORAR_BLOQUEIOS_VENDA, ' +
    '  CLI.CLIENTE_GRUPO_ID, ' +
    '  CLI.OBSERVACOES_VENDA, ' +
    '  CLI.USUARIO_BANCO, ' +
    '  CLI.SENHA_BANCO_DADOS, ' +
    '  CLI.IP_SERVIDOR, ' +
    '  CLI.SV_BANCO_DADOS, ' +
    '  CLI.QTD_ESTACOES, ' +
    '  CLI.RESP_EMPRESA, ' +
    '  CLI.VL_ADESAO, ' +
    '  CLI.VL_MENSAL, ' +
    '  CLI.DATA_ADESAO, ' +
    '  CLI.DATA_VENC_MENSAL, ' +
    '  CLI.PORTA, ' +
    // CADASTRO
    '  CAD.NOME_FANTASIA, ' +
    '  CAD.RAZAO_SOCIAL, ' +
    '  CAD.TIPO_PESSOA, ' +
    '  CAD.CPF_CNPJ, ' +
    '  CAD.LOGRADOURO, ' +
    '  CAD.COMPLEMENTO, ' +
    '  CAD.NUMERO, ' +
    '  CAD.BAIRRO_ID, ' +
    '  CAD.PONTO_REFERENCIA, ' +
    '  CAD.CEP, ' +
    '  CAD.E_MAIL, ' +
    '  CAD.SEXO, ' +
    '  CAD.ESTADO_CIVIL, ' +
    '  CAD.DATA_NASCIMENTO, ' +
    '  CAD.DATA_CADASTRO, ' +
    '  CAD.INSCRICAO_ESTADUAL, ' +
    '  CAD.CNAE, ' +
    '  CAD.RG, ' +
    '  CAD.ORGAO_EXPEDIDOR_RG, ' +
    '  CAD.E_CLIENTE, ' +
    '  CAD.E_FORNECEDOR, ' +
    '  CAD.ATIVO as CADASTRO_ATIVO, ' +
    '  BAI.NOME as NOME_BAIRRO, ' +
    '  CID.NOME as NOME_CIDADE, ' +
    '  CID.CODIGO_IBGE as CODIGO_IBGE_CIDADE, ' +
    '  CID.ESTADO_ID, ' +
    '  CLI.CALCULAR_INDICE_DES ' +
    'from ' +
    '  CLIENTES CLI ' +

    'inner join CADASTROS CAD ' +
    'on CLI.CADASTRO_ID = CAD.CADASTRO_ID ' +

    'inner join BAIRROS BAI ' +
    'on CAD.BAIRRO_ID = BAI.BAIRRO_ID ' +

    'inner join CIDADES CID ' +
    'on BAI.CIDADE_ID = CID.CIDADE_ID ';

  if pSomenteAtivos then
    FSql := FSql + ' where CLI.ATIVO = ''S'''
  else
    FSql := FSql + ' where CLI.ATIVO in(''S'', ''N'') ';

  SetFiltros(GetFiltros);

  AddColuna('CADASTRO_ID', True);
  AddColuna('TIPO_CLIENTE');
  AddColunaSL('DATA_CADASTRO');
  AddColuna('CODIGO_SPC');
  AddColuna('DATA_PESQUISA_SPC');
  AddColuna('LOCAL_SPC');
  AddColuna('DATA_PESQUISA_SERASA');
  AddColuna('LOCAL_SERASA');
  AddColuna('ATENDENTE_SPC');
  AddColuna('RESTRICAO_SPC');
  AddColuna('DATA_RESTRICAO_SPC');
  AddColuna('ATENDENTE_SERASA');
  AddColuna('RESTRICAO_SERASA');
  AddColuna('DATA_RESTRICAO_SERASA');
  AddColuna('CONDICAO_ID');
  AddColuna('VENDEDOR_ID');
  AddColuna('EMITIR_SOMENTE_NFE');
  AddColuna('NAO_GERAR_COMISSAO');
  AddColuna('ATIVO');
  AddColuna('EXIGIR_NOTA_SIMPLES_FAT');
  AddColuna('EXIGIR_MODELO_NOTA_FISCAL');
  AddColuna('TIPO_PRECO');
  AddColuna('DATA_VALIDADE_LIMITE_CREDITO');
  AddColuna('QTDE_DIAS_VENCIMENTO_ACUMULADO');
  AddColuna('IGNORAR_REDIR_REGRA_NOTA_FISC');
  AddColuna('INDICE_DESCONTO_VENDA_ID');
  AddColuna('LISTA_NEGRA_ID');
  AddColuna('OBSERVACOES_LISTA_NEGRA');
  AddColuna('IGNORAR_BLOQUEIOS_VENDA');
  AddColuna('CLIENTE_GRUPO_ID');
  AddColuna('OBSERVACOES_VENDA');
  AddColuna('LIMITE_CREDITO');
  AddColuna('LIMITE_CREDITO_MENSAL');
  AddColuna('DATA_APROVACAO_LIMITE_CREDITO');
  AddColunaSL('USUARIO_APROV_LIMITE_CRED_ID');
  AddColuna('USUARIO_BANCO');
  AddColuna('SENHA_BANCO_DADOS');
  AddColuna('IP_SERVIDOR');
  AddColuna('SV_BANCO_DADOS');
  AddColuna('QTD_ESTACOES');
  AddColuna('RESP_EMPRESA');
  AddColuna('VL_ADESAO');
  AddColuna('VL_MENSAL');
  AddColuna('DATA_ADESAO');
  AddColuna('DATA_VENC_MENSAL');
  AddColuna('PORTA');

  // CADASTRO
  AddColunaSL('NOME_FANTASIA');
  AddColunaSL('RAZAO_SOCIAL');
  AddColunaSL('TIPO_PESSOA');
  AddColunaSL('CPF_CNPJ');
  AddColunaSL('LOGRADOURO');
  AddColunaSL('COMPLEMENTO');
  AddColunaSL('NUMERO');
  AddColunaSL('BAIRRO_ID');
  AddColunaSL('CEP');
  AddColunaSL('PONTO_REFERENCIA');
  AddColunaSL('E_MAIL');
  AddColunaSL('SEXO');
  AddColunaSL('ESTADO_CIVIL');
  AddColunaSL('DATA_NASCIMENTO');
  AddColunaSL('DATA_CADASTRO');
  AddColunaSL('INSCRICAO_ESTADUAL');
  AddColunaSL('CNAE');
  AddColunaSL('RG');
  AddColunaSL('ORGAO_EXPEDIDOR_RG');
  AddColunaSL('E_CLIENTE');
  AddColunaSL('E_FORNECEDOR');
  AddColunaSL('CADASTRO_ATIVO');
  AddColunaSL('NOME_BAIRRO');
  AddColunaSL('NOME_CIDADE');
  AddColunaSL('CODIGO_IBGE_CIDADE');
  AddColunaSL('ESTADO_ID');
end;

function TCliente.GetRecordCliente: RecClientes;
begin
  Result := RecClientes.Create;
  Result.RecCadastro := RecCadastros.Create;

  Result.cadastro_id           := getInt('CADASTRO_ID', True);
  Result.tipo_cliente          := GetString('TIPO_CLIENTE');
  Result.codigo_spc            := GetString('CODIGO_SPC');
  Result.data_pesquisa_spc     := getData('DATA_PESQUISA_SPC');
  Result.data_restricao_spc    := GetData('DATA_RESTRICAO_SPC');
  Result.local_spc             := GetString('LOCAL_SPC');
  Result.atendente_spc         := GetString('ATENDENTE_SPC');
  Result.restricao_spc         := GetString('RESTRICAO_SPC');
  Result.data_pesquisa_serasa  := GetData('DATA_PESQUISA_SERASA');
  Result.local_serasa          := GetString('LOCAL_SERASA');
  Result.data_restricao_serasa := GetData('DATA_RESTRICAO_SERASA');
  Result.atendente_serasa      := GetString('ATENDENTE_SERASA');
  Result.restricao_serasa      := GetString('RESTRICAO_SERASA');
  Result.condicao_id           := GetInt('CONDICAO_ID');
  Result.vendedor_id           := GetInt('VENDEDOR_ID');
  Result.emitir_somente_nfe    := getString('EMITIR_SOMENTE_NFE');
  Result.ativo                 := getString('ATIVO');
  Result.LimiteCredito         := getDouble('LIMITE_CREDITO');
  Result.LimiteCreditoMensal   := getDouble('LIMITE_CREDITO_MENSAL');
  Result.DataAprovacaoLimiteCredito  := getData('DATA_APROVACAO_LIMITE_CREDITO');
  Result.UsuarioAprovLimiteCredId    := getInt('USUARIO_APROV_LIMITE_CRED_ID');
  Result.NaoGerarComissao            := getString('NAO_GERAR_COMISSAO');
  Result.ExigirNotaFaturamento       := getString('EXIGIR_NOTA_SIMPLES_FAT');
  Result.ExigirModeloNotaFiscal      := getString('EXIGIR_MODELO_NOTA_FISCAL');
  Result.TipoPreco                   := getString('TIPO_PRECO');
  Result.DataValidadeLimiteCredito   := getData('DATA_VALIDADE_LIMITE_CREDITO');
  Result.QtdeDiasVencimentoAcumulado := GetInt('QTDE_DIAS_VENCIMENTO_ACUMULADO');
  Result.IgnorarRedirRegraNotaFisc   := getString('IGNORAR_REDIR_REGRA_NOTA_FISC');
  Result.IndiceDescontoVendaId       := GetInt('INDICE_DESCONTO_VENDA_ID');
  Result.ListaNegraId                := GetInt('LISTA_NEGRA_ID');
  Result.ObservacoesListaNegra       := getString('OBSERVACOES_LISTA_NEGRA');
  Result.IgnorarBLoqueiosVenda       := getString('IGNORAR_BLOQUEIOS_VENDA');
  Result.GrupoClienteId              := GetInt('CLIENTE_GRUPO_ID');
  Result.ObservacoesVenda            := getString('OBSERVACOES_VENDA');
  Result.UsuarioBanco                := getString('USUARIO_BANCO');
  Result.SenhaBanco                  := getString('SENHA_BANCO_DADOS');
  Result.IpServidor                  := getString('IP_SERVIDOR');
  Result.ServicoOracle               := getString('SV_BANCO_DADOS');
  Result.QtdEstacoes                 := getString('QTD_ESTACOES');
  Result.ResponsavelEmp              := getString('RESP_EMPRESA');
  Result.ValorAdesao                 := getString('VL_ADESAO');
  Result.ValorMens                   := getString('VL_MENSAL');
  Result.DataAdesao                  := getData('DATA_ADESAO');
  Result.DataVenc                    := getString('DATA_VENC_MENSAL');
  Result.Porta                       := getString('PORTA');

  // Cadastro
  Result.RecCadastro.cadastro_id        := GetInt('CADASTRO_ID', True);
  Result.RecCadastro.razao_social       := getString('RAZAO_SOCIAL');
  Result.RecCadastro.nome_fantasia      := getString('NOME_FANTASIA');
  Result.RecCadastro.tipo_pessoa        := getString('TIPO_PESSOA');
  Result.RecCadastro.cpf_cnpj           := getString('CPF_CNPJ');
  Result.RecCadastro.logradouro         := getString('LOGRADOURO');
  Result.RecCadastro.complemento        := getString('COMPLEMENTO');
  Result.RecCadastro.numero             := getString('NUMERO');
  Result.RecCadastro.bairro_id          := getInt('BAIRRO_ID');
  Result.RecCadastro.ponto_referencia   := getString('PONTO_REFERENCIA');
  Result.RecCadastro.cep                := getString('CEP');
  Result.RecCadastro.e_mail             := getString('E_MAIL');
  Result.RecCadastro.sexo               := getString('SEXO');
  Result.RecCadastro.estado_civil       := getString('ESTADO_CIVIL');
  Result.RecCadastro.data_nascimento    := getData('DATA_NASCIMENTO');
  Result.RecCadastro.data_cadastro      := GetData('DATA_CADASTRO');
  Result.RecCadastro.inscricao_estadual := getString('INSCRICAO_ESTADUAL');
  Result.RecCadastro.cnae               := getString('CNAE');
  Result.RecCadastro.rg                 := getString('RG');
  Result.RecCadastro.orgao_expedidor_rg := getString('ORGAO_EXPEDIDOR_RG');
  Result.RecCadastro.e_cliente          := getString('E_CLIENTE');
  Result.RecCadastro.e_fornecedor       := getString('E_FORNECEDOR');
  Result.RecCadastro.ativo              := getString('ATIVO');
  Result.RecCadastro.NomeBairro         := getString('NOME_BAIRRO');
  Result.RecCadastro.NomeCidade         := getString('NOME_CIDADE');
  Result.RecCadastro.CodigoIBGECidade   := getInt('CODIGO_IBGE_CIDADE');
  Result.RecCadastro.estado_id          := getString('ESTADO_ID');
end;

function AtualizarCliente(
  pConexao: TConexao;
  pPesquisouCadastro: Boolean;
  pCadastroId: Integer;
  pCadastro: RecCadastros;
  pTelefones: TArray<RecCadastrosTelefones>;
  pEnderecos: TArray<RecDiversosEnderecos>;
  pTipoCliente: string;
  pCodigoSpc: string;
  pDataPesquisaSpc: TDateTime;
  pLocalSpc: string;
  pDataPesquisaSerasa: TDateTime;
  pLocalSerasa: string;
  pAtendenteSpc: string;
  pRestricaoSpc: string;
  pDataRestricaoSpc: TDateTime;
  pAtendenteSerasa: string;
  pRestricaoSerasa: string;
  pDataRestricaoSerasa: TDateTime;
  pCondicaoId: Integer;
  pVendedorId: Integer;
  pEmitirSomenteNfe: string;
  pExigirModeloNotaFiscal: string;
  pCondicoesPagtosRestritasIds: TArray<Integer>;
  pLimiteCredito: Double;
  pLimiteCreditoMensal: Double;
  pDataAprovLimiteCredito: TDateTime;
  pNaoGerarComissao: string;
  pAtivo: string;
  pExigirNotaSimplesFat: string;
  pTipoPreco: string;
  pDataValidadeLimiteCredito: TDateTime;
  pQtdeDiasVencimentoAcumulado: Integer;
  pIgnorarRedirRegraNotaFisc: string;
  pIndiceDescontoVendaId: Integer;
  pListaNegraId: Integer;
  pObservacoeListaNegra: string;
  pIgnorarBloqueiosVenda: string;
  pGrupoClienteId: Integer;
  pObservacoesVenda: string;
  pUsuarioBanco: string;
  pSenhaBanco: string;
  pIpServidor: string;
  pServicoOracle: string;
  pQtdEstacoes: string;
  pResponsavelEmp: string;
  pValorAdesao: string;
  pValorMens: string;
  pDataAdesao: TDateTime;
  pDataVenc: string;
  pPorta: string;
  pCalcularIndiceDesconto: string
): RecRetornoBD;
var
  t: TCliente;
  vNovo: Boolean;
  vCadastro: TCadastro;
  vSeq: TSequencia;
  vRetBanco: RecRetornoBD;
begin
  Result.Iniciar;
  vNovo := pCadastroId = 0;
  t := TCliente.Create(pConexao, True);
  vCadastro := TCadastro.Create(pConexao);
  pConexao.SetRotina('ATUALIZAR_CLIENTES');

  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_CADASTRO_ID');
    pCadastroId := vSeq.GetProximaSequencia;
    Result.AsInt := pCadastroId;
    pCadastro.cadastro_id := pCadastroId;
    vSeq.Free;
  end;

  try
    pConexao.IniciarTransacao;

    vCadastro.setCadastro(pCadastro);
    if vNovo then
      vCadastro.Inserir
    else
      vCadastro.Atualizar;

    t.SetInt('CADASTRO_ID', pCadastroId, True);
    t.SetString('TIPO_CLIENTE', pTipoCliente);
    t.SetString('CODIGO_SPC', pCodigoSpc);
    t.SetDataN('DATA_PESQUISA_SPC', pDataPesquisaSpc);
    t.SetString('LOCAL_SPC', pLocalSpc);
    t.SetDataN('DATA_PESQUISA_SERASA', pDataPesquisaSerasa);
    t.SetString('LOCAL_SERASA', pLocalSerasa);
    t.SetString('ATENDENTE_SPC', pAtendenteSpc);
    t.SetString('RESTRICAO_SPC', pRestricaoSpc);
    t.SetDataN('DATA_RESTRICAO_SPC', pDataRestricaoSpc);
    t.SetString('ATENDENTE_SERASA', pAtendenteSerasa);
    t.SetString('RESTRICAO_SERASA', pRestricaoSerasa);
    t.setDataN('DATA_RESTRICAO_SERASA', pDataRestricaoSerasa);
    t.SetIntN('CONDICAO_ID', pCondicaoId);
    t.SetIntN('VENDEDOR_ID', pVendedorId);
    t.setString('EMITIR_SOMENTE_NFE', pEmitirSomenteNfe);
    t.SetDouble('LIMITE_CREDITO', pLimiteCredito);
    t.SetDouble('LIMITE_CREDITO_MENSAL', pLimiteCreditoMensal);
    t.SetDataN('DATA_APROVACAO_LIMITE_CREDITO', pDataAprovLimiteCredito);
    t.setString('NAO_GERAR_COMISSAO', pNaoGerarComissao);
    t.setString('EXIGIR_NOTA_SIMPLES_FAT', pExigirNotaSimplesFat);
    t.setString('EXIGIR_MODELO_NOTA_FISCAL', pExigirModeloNotaFiscal);
    t.setString('TIPO_PRECO', pTipoPreco);
    t.setDataN('DATA_VALIDADE_LIMITE_CREDITO', pDataValidadeLimiteCredito);
    t.setInt('QTDE_DIAS_VENCIMENTO_ACUMULADO', pQtdeDiasVencimentoAcumulado);
    t.setString('IGNORAR_REDIR_REGRA_NOTA_FISC', pIgnorarRedirRegraNotaFisc);
    t.setIntN('INDICE_DESCONTO_VENDA_ID', pIndiceDescontoVendaId);
    t.setIntN('LISTA_NEGRA_ID', pListaNegraId);
    t.setString('OBSERVACOES_LISTA_NEGRA', pObservacoeListaNegra);
    t.setString('IGNORAR_BLOQUEIOS_VENDA', pIgnorarBloqueiosVenda);
    t.setIntN('CLIENTE_GRUPO_ID', pGrupoClienteId);
    t.setString('OBSERVACOES_VENDA', pObservacoesVenda);
    t.setString('USUARIO_BANCO', pUsuarioBanco);
    t.setString('SENHA_BANCO_DADOS', pSenhaBanco);
    t.setString('IP_SERVIDOR', pIpServidor);
    t.setString('SV_BANCO_DADOS', pServicoOracle);
    t.setString('QTD_ESTACOES', pQtdEstacoes);
    t.setString('RESP_EMPRESA', pResponsavelEmp);
    t.setString('VL_ADESAO', pValorAdesao);
    t.setString('VL_MENSAL', pValorMens);
    t.setDataN('DATA_ADESAO', pDataAdesao);
    t.setString('DATA_VENC_MENSAL', pDataVenc);
    t.SetString('ATIVO', pAtivo);
    t.setString('PORTA', pPorta);

    if vNovo or pPesquisouCadastro then
      t.Inserir
    else
      t.Atualizar;

    vRetBanco := _CadastrosTelefones.AtualizarTelefones(pConexao, pCadastroId, pTelefones);
    if vRetBanco.TeveErro then
      raise Exception.Create(vRetBanco.MensagemErro);

    vRetBanco := _DiversosEnderecos.AtualizarDiversosEndereco(pConexao, pCadastroId, pEnderecos);
    if vRetBanco.TeveErro then
      raise Exception.Create(vRetBanco.MensagemErro);

    vRetBanco := _ClientesCondicPagtoRestrit.AtualizarClientesCondicPagtoRestrit(pConexao, pCadastroId, pCondicoesPagtosRestritasIds);
    if vRetBanco.TeveErro then
      raise Exception.Create(vRetBanco.MensagemErro);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vCadastro.Free;
  t.Free;
end;

function BuscarClientes(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecClientes>;
var
  i: Integer;
  t: TCliente;
begin
  Result := nil;
  t := TCliente.Create(pConexao, pSomenteAtivos);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordCliente;
      t.ProximoRegistro;
    end;
  end;
  t.Free;
end;

function ExcluirCliente(
  pConexao: TConexao;
  pCadastroId: Integer
): RecRetornoBD;
var
  t: TCliente;
  ex: TExecucao;
begin
  Result.TeveErro := False;
  t := TCliente.Create(pConexao, True);
  ex := TExecucao.Create(pConexao);

  t.setInt('CADASTRO_ID', pCadastroId, True);

  try
    pConexao.IniciarTransacao;
    
    ex.SQL.Add('delete from DIVERSOS_CONTATOS where CADASTRO_ID = :P1');
    ex.Executar([pCadastroId]);

    ex.SQL.Clear;
    ex.SQL.Add('delete from DIVERSOS_ENDERECOS where CADASTRO_ID = :P1');
    ex.Executar([pCadastroId]);

    t.Excluir;
    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  ex.Free;
  t.Free;
end;

function ECliente(pConexao: TConexao; pCadastroId: Integer): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select count(*) from CLIENTES where CADASTRO_ID = :P1');
  vSql.Pesquisar([pCadastroId]);

  Result := vSql.GetInt(0) > 0;

  vSql.Free;
end;

function BuscarDadosVidaCliente(pConexao: TConexao; pCadastroId: Integer): TArray<RecDadosVidaCliente>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select');
  vSql.Add('  CAD.CADASTRO_ID,');
  vSql.Add('  CAD.DATA_NASCIMENTO,');
  vSql.Add('  CAD.TELEFONE_PRINCIPAL,');
  vSql.Add('  CAD.TELEFONE_CELULAR,');
  vSql.Add('  CLI.DATA_CADASTRO,');
  vSql.Add('  CLI.LIMITE_CREDITO,');
  vSql.Add('  CLI.DATA_APROVACAO_LIMITE_CREDITO,');
  vSql.Add('  CLI.DATA_VALIDADE_LIMITE_CREDITO,');
  vSql.Add('  LIM.VALOR_UTILIZADO as VALOR_UTILIZADO_LIMITE,');
  vSql.Add('  CLI.LIMITE_CREDITO - nvl(LIM.VALOR_UTILIZADO, 0) as SALDO_RESTANTE,');
  vSql.Add('  FIA.VALOR_FINANCEIRO_ABERTO,');
  vSql.Add('  FIA.QTDE_FINANCEIROS_ABERTOS,');
  vSql.Add('  FIB.VALOR_FINANCEIRO_BAIXADO,');
  vSql.Add('  FIB.QTDE_FINANCEIROS_BAIXADOS,');
  vSql.Add('  OCA.QTDE_ORC_ABERTO,');
  vSql.Add('  PED.QTDE_PEDIDOS,');
  vSql.Add('  DEV.QTDE_DEVOLUCOES,');
  vSql.Add('  UTV.DATA_ULTIMA_VENDA,');
  vSql.Add('  (trunc(sysdate) - trunc(UTV.DATA_ULTIMA_VENDA)) as QTDE_DIAS_SEM_COMPRAR,');
  vSql.Add('  ENT.DATA_ULTIMA_ENTREGA,');
  vSql.Add('  ENT.VALOR_ULTIMA_ENTREGA,');
  vSql.Add('  QTE.QTDE_ENTREGAS,');
  vSql.Add('  QTR.QTDE_RETORNADAS,');
  vSql.Add('  RET.DATA_ULTIMA_RETIRADA,');
  vSql.Add('  RET.VALOR_ULTIMA_RETIRADA,');
  vSql.Add('  RTE.QTDE_RETIRADAS');

  vSql.Add('from');
  vSql.Add('  CADASTROS CAD');

  vSql.Add('inner join CLIENTES CLI');
  vSql.Add('on CLI.CADASTRO_ID = CAD.CADASTRO_ID');

  vSql.Add('left join(');
  vSql.Add('  select');
  vSql.Add('    CTR.CADASTRO_ID,');
  vSql.Add('    sum(VALOR_DOCUMENTO) as VALOR_UTILIZADO');
  vSql.Add('  from');
  vSql.Add('    CONTAS_RECEBER CTR');

  vSql.Add('  inner join TIPOS_COBRANCA TPC');
  vSql.Add('  on TPC.COBRANCA_ID = CTR.COBRANCA_ID');

  vSql.Add('  where CTR.STATUS = ''A''');
  vSql.Add('  and TPC.UTILIZAR_LIMITE_CRED_CLIENTE = ''S''');

  vSql.Add('  group by');
  vSql.Add('    CTR.CADASTRO_ID');
  vSql.Add(') LIM');
  vSql.Add('on LIM.CADASTRO_ID = CAD.CADASTRO_ID');

  vSql.Add('left join(');
  vSql.Add('  select');
  vSql.Add('    CTR.CADASTRO_ID,');
  vSql.Add('    count(*) as QTDE_FINANCEIROS_ABERTOS,');
  vSql.Add('    sum(VALOR_DOCUMENTO) as VALOR_FINANCEIRO_ABERTO');
  vSql.Add('  from');
  vSql.Add('    CONTAS_RECEBER CTR');

  vSql.Add('  where CTR.STATUS = ''A''');

  vSql.Add('  group by');
  vSql.Add('   CTR.CADASTRO_ID');
  vSql.Add(') FIA');
  vSql.Add('on FIA.CADASTRO_ID = CAD.CADASTRO_ID');

  vSql.Add('left join(');
  vSql.Add('  select');
  vSql.Add('    CTR.CADASTRO_ID,');
  vSql.Add('    count(*) as QTDE_FINANCEIROS_BAIXADOS,');
  vSql.Add('    sum(VALOR_DOCUMENTO) as VALOR_FINANCEIRO_BAIXADO');
  vSql.Add('  from ');
  vSql.Add('    CONTAS_RECEBER CTR');

  vSql.Add('  where CTR.STATUS = ''B''');

  vSql.Add('  group by');
  vSql.Add('    CTR.CADASTRO_ID');
  vSql.Add(') FIB');
  vSql.Add('on FIB.CADASTRO_ID = CAD.CADASTRO_ID');

  vSql.Add('left join(');
  vSql.Add('  select');
  vSql.Add('    ORC.CLIENTE_ID as CADASTRO_ID,');
  vSql.Add('    count(*) as QTDE_ORC_ABERTO');
  vSql.Add('  from');
  vSql.Add('    ORCAMENTOS ORC');
  vSql.Add('  where ORC.STATUS = ''OE''');

  vSql.Add('  group by');
  vSql.Add('    ORC.CLIENTE_ID');
  vSql.Add(') OCA');
  vSql.Add('on OCA.CADASTRO_ID = CAD.CADASTRO_ID');

  vSql.Add('left join(');
  vSql.Add('  select');
  vSql.Add('    ORC.CLIENTE_ID as CADASTRO_ID,');
  vSql.Add('    count(*) as QTDE_PEDIDOS');
  vSql.Add('  from ');
  vSql.Add('    ORCAMENTOS ORC ');

  vSql.Add('  where ORC.STATUS in (''VB'', ''VR'', ''VE'', ''RE'') ');

  vSql.Add('  group by');
  vSql.Add('    ORC.CLIENTE_ID');
  vSql.Add(') PED');
  vSql.Add('on PED.CADASTRO_ID = CAD.CADASTRO_ID');

  vSql.Add('left join(');
  vSql.Add('  select');
  vSql.Add('    ORC.CLIENTE_ID as CADASTRO_ID,');
  vSql.Add('    count(*) as QTDE_DEVOLUCOES ');
  vSql.Add('  from ');
  vSql.Add('    DEVOLUCOES DEV ');

  vSql.Add('  inner join ORCAMENTOS ORC ');
  vSql.Add('  on ORC.ORCAMENTO_ID = DEV.ORCAMENTO_ID ');

  vSql.Add('  group by ');
  vSql.Add('    ORC.CLIENTE_ID');
  vSql.Add(') DEV ');
  vSql.Add('on DEV.CADASTRO_ID = CAD.CADASTRO_ID ');


  vSql.Add('left join(');
  vSql.Add('  select  ');
  vSql.Add('    ORC.CLIENTE_ID as CADASTRO_ID, ');
  vSql.Add('    max(trunc(DATA_HORA_RECEBIMENTO)) as DATA_ULTIMA_VENDA');
  vSql.Add('  from');
  vSql.Add('    ORCAMENTOS ORC ');

  vSql.Add('  where ORC.STATUS in ( ''RE'')');

  vSql.Add('  group by');
  vSql.Add('    ORC.CLIENTE_ID');
  vSql.Add(') UTV');
  vSql.Add('on UTV.CADASTRO_ID = CAD.CADASTRO_ID');

  vSql.Add('left join(');
  vSql.Add('  select ');
  vSql.Add('    ORC.CLIENTE_ID as CADASTRO_ID,');
  vSql.Add('    ENT.DATA_HORA_REALIZOU_ENTREGA as DATA_ULTIMA_ENTREGA,');
  vSql.Add('    sum(round((ORC.VALOR_TOTAL - ORC.VALOR_FRETE) * OIT.VALOR_TOTAL / OIT.QUANTIDADE * ITE.QUANTIDADE / ORC.VALOR_TOTAL_PRODUTOS, 2)) as VALOR_ULTIMA_ENTREGA ');
  vSql.Add('  from');
  vSql.Add('    ENTREGAS ENT');

  vSql.Add('  inner join ENTREGAS_ITENS ITE');
  vSql.Add('  on ENT.ENTREGA_ID = ITE.ENTREGA_ID');

  vSql.Add('  inner join ORCAMENTOS ORC');
  vSql.Add('  on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID');

  vSql.Add('  inner join ORCAMENTOS_ITENS OIT');
  vSql.Add('  on ORC.ORCAMENTO_ID = OIT.ORCAMENTO_ID');
  vSql.Add('  and ITE.ITEM_ID = OIT.ITEM_ID ');

  vSql.Add('where ENT.DATA_HORA_REALIZOU_ENTREGA in (');
  vSql.Add('  select');
  vSql.Add('    max(ET1.DATA_HORA_REALIZOU_ENTREGA) ');
  vSql.Add('  from ');
  vSql.Add('    ENTREGAS ET1  ');

  vSql.Add('  inner join ORCAMENTOS OR1  ');
  vSql.Add('  on ET1.ORCAMENTO_ID = OR1.ORCAMENTO_ID  ');

  vSql.Add('  where OR1.CLIENTE_ID = ORC.CLIENTE_ID  ');
  vSql.Add(')');

  vSql.Add('  group by');
  vSql.Add('    ORC.CLIENTE_ID, ');
  vSql.Add('    ENT.DATA_HORA_REALIZOU_ENTREGA');

  vSql.Add(') ENT ');
  vSql.Add('on ENT.CADASTRO_ID = CAD.CADASTRO_ID ');

  vSql.Add('left join( ');
  vSql.Add('  select');
  vSql.Add('    ORC.CLIENTE_ID as CADASTRO_ID, ');
  vSql.Add('    count(*) as QTDE_ENTREGAS');
  vSql.Add('  from');
  vSql.Add('    ENTREGAS ENT');

  vSql.Add('  inner join ORCAMENTOS ORC ');
  vSql.Add('  on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID ');

  vSql.Add('  group by ');
  vSql.Add('    ORC.CLIENTE_ID ');
  vSql.Add(') QTE');
  vSql.Add('on QTE.CADASTRO_ID = CAD.CADASTRO_ID');

  vSql.Add('left join(');
  vSql.Add('  select');
  vSql.Add('    ORC.CLIENTE_ID as CADASTRO_ID, ');
  vSql.Add('    count(*) as QTDE_RETORNADAS');
  vSql.Add('  from');
  vSql.Add('    ENTREGAS ENT ');

  vSql.Add('  inner join ORCAMENTOS ORC');
  vSql.Add('  on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID');

  vSql.Add('  where ENT.STATUS in (''RPA'', ''RTO'')');

  vSql.Add('  group by ');
  vSql.Add('    ORC.CLIENTE_ID');
  vSql.Add(') QTR');
  vSql.Add('on QTR.CADASTRO_ID = CAD.CADASTRO_ID');

  vSql.Add('left join(');
  vSql.Add('  select ');
  vSql.Add('    ORC.CLIENTE_ID as CADASTRO_ID,');
  vSql.Add('    ENT.DATA_HORA_RETIRADA as DATA_ULTIMA_RETIRADA,');
  vSql.Add('    sum(round((ORC.VALOR_TOTAL - ORC.VALOR_FRETE) * OIT.VALOR_TOTAL / OIT.QUANTIDADE * ITE.QUANTIDADE / ORC.VALOR_TOTAL_PRODUTOS, 2)) as VALOR_ULTIMA_RETIRADA');
  vSql.Add('  from ');
  vSql.Add('    RETIRADAS ENT ');

  vSql.Add('  inner join RETIRADAS_ITENS ITE  ');
  vSql.Add('  on ENT.RETIRADA_ID = ITE.RETIRADA_ID ');

  vSql.Add('  inner join ORCAMENTOS ORC ');
  vSql.Add('  on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID ');

  vSql.Add('  inner join ORCAMENTOS_ITENS OIT ');
  vSql.Add('  on ORC.ORCAMENTO_ID = OIT.ORCAMENTO_ID ');
  vSql.Add('  and ITE.ITEM_ID = OIT.ITEM_ID ');

  vSql.Add('where ENT.DATA_HORA_RETIRADA in (');
  vSql.Add('  select ');
  vSql.Add('    max(ET1.DATA_HORA_RETIRADA) ');
  vSql.Add('  from ');
  vSql.Add('    RETIRADAS ET1 ');
  vSql.Add('  inner join ORCAMENTOS OR1 ');
  vSql.Add('  on ET1.ORCAMENTO_ID = OR1.ORCAMENTO_ID ');
  vSql.Add('  where OR1.CLIENTE_ID = ORC.CLIENTE_ID ');
  vSql.Add(')');

  vSql.Add('  group by');
  vSql.Add('    ORC.CLIENTE_ID,');
  vSql.Add('    ENT.DATA_HORA_RETIRADA');
  vSql.Add(') RET ');
  vSql.Add('on RET.CADASTRO_ID = CAD.CADASTRO_ID ');

  vSql.Add('left join( ');
  vSql.Add('  select ');
  vSql.Add('    ORC.CLIENTE_ID as CADASTRO_ID, ');
  vSql.Add('    count(*) as QTDE_RETIRADAS ');
  vSql.Add('  from ');
  vSql.Add('    RETIRADAS ENT ');

  vSql.Add('  inner join ORCAMENTOS ORC ');
  vSql.Add('  on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID ');

  vSql.Add('  group by ');
  vSql.Add('    ORC.CLIENTE_ID ');
  vSql.Add(') RTE ');
  vSql.Add('on RTE.CADASTRO_ID = CAD.CADASTRO_ID ');

  vSql.Add('where CAD.CADASTRO_ID = :P1 ');

  if vSql.Pesquisar([pCadastroId]) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin

      Result[i].CadastroId                  := vSql.GetInt('CADASTRO_ID');
      Result[i].DataNascimento              := vSql.GetData('DATA_NASCIMENTO');
      Result[i].TelefonePrincipal           := vSql.GetString('TELEFONE_PRINCIPAL');
      Result[i].TelefoneCelular             := vSql.GetString('TELEFONE_CELULAR');
      Result[i].DataCadastro                := vSql.GetData('DATA_CADASTRO');
      Result[i].LimiteCredito               := vSql.GetDouble('LIMITE_CREDITO');
      Result[i].DataAprovacaoLimiteCredito  := vSql.GetData('DATA_APROVACAO_LIMITE_CREDITO');
      Result[i].DataValidadeLimiteCredito   := vSql.GetData('DATA_VALIDADE_LIMITE_CREDITO');
      Result[i].ValorUtilizadoLimiteCredito := vSql.GetDouble('VALOR_UTILIZADO_LIMITE');
      Result[i].SaldoRestante               := vSql.GetDouble('SALDO_RESTANTE');
      Result[i].ValorFinanceiroAberto       := vSql.GetDouble('VALOR_FINANCEIRO_ABERTO');
      Result[i].QtdeFinanceirosAbertos      := vSql.GetInt('QTDE_FINANCEIROS_ABERTOS');
      Result[i].ValorFinanceiroBaixado      := vSql.GetDouble('VALOR_FINANCEIRO_BAIXADO');
      Result[i].QtdeFinanceirosBaixados     := vSql.GetInt('QTDE_FINANCEIROS_BAIXADOS');
      Result[i].QtdeOrcamentosAbertos       := vSql.GetInt('QTDE_ORC_ABERTO');
      Result[i].QtdePedidos                 := vSql.GetInt('QTDE_PEDIDOS');
      Result[i].QtdeDevolucoes              := vSql.GetInt('QTDE_DEVOLUCOES');
      Result[i].DataUltimaVenda             := vSql.GetData('DATA_ULTIMA_VENDA');
      Result[i].QtdeDiasSemComprar          := vSql.GetInt('QTDE_DIAS_SEM_COMPRAR');
      Result[i].DataUltimaEntrega           := vSql.GetData('DATA_ULTIMA_ENTREGA');
      Result[i].ValorUltimaEntrega          := vSql.GetDouble('VALOR_ULTIMA_ENTREGA');
      Result[i].QtdeEntregas                := vSql.GetInt('QTDE_ENTREGAS');
      Result[i].QtdeEntregasRetornadas      := vSql.GetInt('QTDE_RETORNADAS');
      Result[i].DataUltimaRetirada          := vSql.GetData('DATA_ULTIMA_RETIRADA');
      Result[i].ValorUltimaRetirada         := vSql.GetDouble('VALOR_ULTIMA_RETIRADA');
      Result[i].QtdeRetiradas               := vSql.GetInt('QTDE_RETIRADAS');

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  FreeAndNil(vSql);
end;

function BuscarLimiteCreditoClientes(pConexao: TConexao; pComando: string): TArray<RecDadosLimiteCreditoCliente>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  CAD.CADASTRO_ID,');
  vSql.Add('  CAD.NOME_FANTASIA,');
  vSql.Add('  CLI.LIMITE_CREDITO,');
  vSql.Add('  CLI.DATA_APROVACAO_LIMITE_CREDITO,');
  vSql.Add('  CLI.DATA_VALIDADE_LIMITE_CREDITO,');
  vSql.Add('  nvl(LIM.VALOR_UTILIZADO, 0) as VALOR_UTILIZADO_LIMITE,');
  vSql.Add('  CLI.LIMITE_CREDITO - nvl(LIM.VALOR_UTILIZADO, 0) as SALDO_RESTANTE');

  vSql.Add('from');
  vSql.Add('  CADASTROS CAD ');

  vSql.Add('inner join CLIENTES CLI');
  vSql.Add('on CLI.CADASTRO_ID = CAD.CADASTRO_ID ');

  vSql.Add('left join( ');
  vSql.Add('  select ');
  vSql.Add('    CTR.CADASTRO_ID,');
  vSql.Add('    sum(VALOR_DOCUMENTO) as VALOR_UTILIZADO ');
  vSql.Add('  from');
  vSql.Add('    CONTAS_RECEBER CTR');
  vSql.Add('  inner join TIPOS_COBRANCA TPC');
  vSql.Add('  on TPC.COBRANCA_ID = CTR.COBRANCA_ID');
  vSql.Add('  where CTR.STATUS = ''A''');
  vSql.Add('  and TPC.UTILIZAR_LIMITE_CRED_CLIENTE = ''S''');
  vSql.Add('  group by');
  vSql.Add('    CTR.CADASTRO_ID');
  vSql.Add(') LIM');
  vSql.Add('on LIM.CADASTRO_ID = CAD.CADASTRO_ID ');

  vSql.Add(pComando);

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin

      Result[i].CadastroId                  := vSql.GetInt('CADASTRO_ID');
      Result[i].NomeCliente                 := vSql.GetString('NOME_FANTASIA');
      Result[i].LimiteCredito               := vSql.GetDouble('LIMITE_CREDITO');
      Result[i].DataAprovacaoLimiteCredito  := vSql.GetData('DATA_APROVACAO_LIMITE_CREDITO');
      Result[i].DataValidadeLimiteCredito   := vSql.GetData('DATA_VALIDADE_LIMITE_CREDITO');
      Result[i].ValorUtilizadoLimiteCredito := vSql.GetDouble('VALOR_UTILIZADO_LIMITE');
      Result[i].SaldoRestante               := vSql.GetDouble('SALDO_RESTANTE');

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  FreeAndNil(vSql);
end;

function ValidadeLimiteCreditoVencida(pConexao: TConexao; pClienteId: Integer): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select');
  vSql.Add('  count(*) ');
  vSql.Add('from ');
  vSql.Add('  CLIENTES ');
  vSql.Add('where CADASTRO_ID = :P1 ');
  vSql.Add('and DATA_VALIDADE_LIMITE_CREDITO is not null ');
  vSql.Add('and DATA_VALIDADE_LIMITE_CREDITO < trunc(sysdate) ');

  vSql.Pesquisar([pClienteId]);

  Result := vSql.GetInt(0) > 0;

  vSql.Free;
end;

end.
