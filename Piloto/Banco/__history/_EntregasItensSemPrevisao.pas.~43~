unit _EntregasItensSemPrevisao;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, _OrcamentosItensDefLotes, _RecordsOrcamentosVendas, _OrcamentosItensDefLocais;

{$M+}
type
  RecEntregasItensSemPrevisao = record
    OrcamentoId: Integer;
    ProdutoId: Integer;
    Nome: string;
    ItemId: Integer;
    Quantidade: Double;
    Entregues: Double;
    Devolvidos: Double;
    Saldo: Double;
    AguardarContatoCliente: string;
    Unidade: string;
    Marca: string;
    TipoControleEstoque: string;
    MultiploVenda: Double;
    Lotes: TArray<RecDefinicoesLoteVenda>;
    Locais: TArray<RecDefinicoesLocaisVenda>;
  end;

  TEntregasItensSemPrevisao = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordEntregasItens: RecEntregasItensSemPrevisao;
  end;

function BuscarEntregasItensSemPrevisao(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEntregasItensSemPrevisao>;

function GetFiltros: TArray<RecFiltros>;

function AgendarItens(
  pConexao: TConexao;
  pEmpresaGeracaoId: Integer;
  pOrcamentoId: Integer;
  pDataHoraEntrega: TDateTime;
  pItens: TArray<RecEntregasItensSemPrevisao>;
  pDefinirLocalManual: string
): RecRetornoBD;

implementation

{ TOrcamentoTramite }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where SPE.ORCAMENTO_ID = :P1 ' +
      'order by ' +
      '  PRO.NOME '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where SPE.ORCAMENTO_ID = :P1 ' +
      'and SPE.SALDO > 0 ' +
      'order by ' +
      '  PRO.NOME '
    );

end;

constructor TEntregasItensSemPrevisao.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ENTREGAS_ITENS_SEM_PREVISAO');

  FSql :=
    'select ' +
    '  SPE.ORCAMENTO_ID, ' +
    '  SPE.PRODUTO_ID, ' +
    '  PRO.NOME, ' +
    '  SPE.ITEM_ID, ' +
    '  SPE.QUANTIDADE, ' +
    '  SPE.ENTREGUES, ' +
    '  SPE.DEVOLVIDOS, ' +
    '  SPE.SALDO, ' +
    '  SPE.AGUARDAR_CONTATO_CLIENTE, ' +
    '  PRO.UNIDADE_VENDA, ' +
    '  MAR.NOME as NOME_MARCA, ' +
    '  IOR.TIPO_CONTROLE_ESTOQUE, ' +
    '  PRO.MULTIPLO_VENDA ' +
    'from ' +
    '  ENTREGAS_ITENS_SEM_PREVISAO SPE ' +

    'inner join VW_ORC_ITENS_SEM_KITS_AGRUPADO IOR ' +
    'on SPE.ORCAMENTO_ID = IOR.ORCAMENTO_ID ' +
    'and SPE.PRODUTO_ID = IOR.PRODUTO_ID ' +
    'and SPE.ITEM_ID = IOR.ITEM_ID ' +

    'inner join PRODUTOS PRO ' +
    'on SPE.PRODUTO_ID = PRO.PRODUTO_ID ' +

    'inner join MARCAS MAR ' +
    'on PRO.MARCA_ID = MAR.MARCA_ID ';

  SetFiltros(GetFiltros);

  AddColunaSL('ORCAMENTO_ID');
  AddColunaSL('PRODUTO_ID');
  AddColunaSL('NOME');
  AddColunaSL('ITEM_ID');
  AddColunaSL('QUANTIDADE');
  AddColunaSL('ENTREGUES');
  AddColunaSL('DEVOLVIDOS');
  AddColunaSL('SALDO');
  AddColunaSL('AGUARDAR_CONTATO_CLIENTE');
  AddColunaSL('UNIDADE_VENDA');
  AddColunaSL('NOME_MARCA');
  AddColunaSL('TIPO_CONTROLE_ESTOQUE');
  AddColunaSL('MULTIPLO_VENDA');
end;

function TEntregasItensSemPrevisao.GetRecordEntregasItens: RecEntregasItensSemPrevisao;
begin
  Result.OrcamentoId       := getInt('ORCAMENTO_ID');
  Result.ProdutoId         := getInt('PRODUTO_ID');
  Result.Nome              := getString('NOME');
  Result.ItemId            := getInt('ITEM_ID');
  Result.Quantidade        := getDouble('QUANTIDADE');
  Result.Entregues         := getDouble('ENTREGUES');
  Result.Devolvidos        := getDouble('DEVOLVIDOS');
  Result.Saldo             := getDouble('SALDO');
  Result.AguardarContatoCliente := getString('AGUARDAR_CONTATO_CLIENTE');
  Result.Unidade                := getString('UNIDADE_VENDA');
  Result.Marca                  := getString('NOME_MARCA');
  Result.TipoControleEstoque    := getString('TIPO_CONTROLE_ESTOQUE');
  Result.MultiploVenda          := getDouble('MULTIPLO_VENDA');
end;


function BuscarEntregasItensSemPrevisao(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEntregasItensSemPrevisao>;
var
  i: Integer;
  t: TEntregasItensSemPrevisao;
begin
  Result := nil;
  t := TEntregasItensSemPrevisao.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordEntregasItens;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;


function AgendarItens(
  pConexao: TConexao;
  pEmpresaGeracaoId: Integer;
  pOrcamentoId: Integer;
  pDataHoraEntrega: TDateTime;
  pItens: TArray<RecEntregasItensSemPrevisao>;
  pDefinirLocalManual: string
): RecRetornoBD;
var
  i: Integer;
  j: Integer;
  item_id: Integer;
  vExec: TExecucao;
  vPesq: TConsulta;
  vUpdate: TExecucao;
  vConsultaLote: TConsulta;
  vProcedimento: TProcedimentoBanco;
  vItensDefLotes: TOrcamentosItensDefLotes;
  vItensDefLocais: TOrcamentosItensDefLocais;

  vProdutoIds: array of Integer;
  vItensIds: array of Integer;
  vQuantidades: array of Double;
begin
  Result.Iniciar;
  pConexao.SetRotina('AGENDAR_ITENS_SEM_PREVISAO');

  vExec := TExecucao.Create(pConexao);
  vPesq := TConsulta.Create(pConexao);
  vProcedimento := TProcedimentoBanco.Create(pConexao, 'AGENDAR_ITENS_SEM_PREVISAO');
  vItensDefLotes := TOrcamentosItensDefLotes.Create(pConexao);
  vItensDefLocais := TOrcamentosItensDefLocais.Create(pConexao);
  vUpdate := TExecucao.Create(pConexao);
  vConsultaLote := TConsulta.Create(pConexao);

  vPesq.SQL.Add('select ');
  vPesq.SQL.Add('  MAX(ITEM_ID) as ITEM_ID ');
  vPesq.SQL.Add('from ');
  vPesq.SQL.Add('  ORCAMENTOS_ITENS_DEF_LOCAIS ');
  vPesq.SQL.Add('where ORCAMENTO_ID = :P1');

  vConsultaLote.SQL.Add(
    'select ' +
    '  count(*) as CONTADOR ' +
    'from ' +
    '  ORCAMENTOS_ITENS_DEF_LOTES ' +
    'where ORCAMENTO_ID = :P1 ' +
    'and LOTE = :P2 ' +
    'and ITEM_ID = :P3'
  );

  vUpdate.Add(
    'update ORCAMENTOS_ITENS_DEF_LOTES set ' +
    '  QUANTIDADE_ENTREGAR = QUANTIDADE_ENTREGAR + :P1, ' +
    '  QUANTIDADE_RETIRAR = QUANTIDADE_RETIRAR + :P2, ' +
    '  QUANTIDADE_RETIRAR_ATO = QUANTIDADE_RETIRAR_ATO + :P3 ' +
    'where ORCAMENTO_ID = :P4 ' +
    'and ITEM_ID = :P5 ' +
    'and LOTE = :P6 '
  );

  try
    pConexao.IniciarTransacao;

    SetLength(vProdutoIds, Length(pItens));
    SetLength(vItensIds, Length(pItens));
    SetLength(vQuantidades, Length(pItens));

    for i := Low(pItens) to High(pItens) do begin
      vProdutoIds[i]  := pItens[i].ProdutoId;
      vItensIds[i]    := pItens[i].ItemId;
      vQuantidades[i] := pItens[i].Quantidade;

      for j := Low(pItens[i].Lotes) to High(pItens[i].Lotes) do begin

        vConsultaLote.Pesquisar([pOrcamentoId, pItens[i].Lotes[j].Lote, pItens[i].ItemId]);

        if vConsultaLote.GetInt('CONTADOR') = 0 then begin
          vItensDefLotes.SetInt('ORCAMENTO_ID', pOrcamentoId, True);
          vItensDefLotes.SetInt('ITEM_ID', pItens[i].ItemId, True);
          vItensDefLotes.setString('LOTE', pItens[i].Lotes[j].Lote, True);
          vItensDefLotes.setDouble('QUANTIDADE_RETIRAR_ATO', 0);
          vItensDefLotes.setDouble('QUANTIDADE_RETIRAR', 0);
          vItensDefLotes.setDouble('QUANTIDADE_ENTREGAR', pItens[i].Lotes[j].QuantidadeEntregar);

          vItensDefLotes.Inserir;
        end
        else begin
          vUpdate.Executar([
            pItens[i].Lotes[j].QuantidadeEntregar,
            pItens[i].Lotes[j].QuantidadeRetirar,
            pItens[i].Lotes[j].QuantidadeRetirarAto,
            pOrcamentoId,
            pItens[i].ItemId,
            pItens[i].Lotes[j].Lote
          ]);
        end;

        if pDefinirLocalManual = 'S' then begin
          vPesq.Pesquisar([pOrcamentoId]);

          vItensDefLocais.SetInt('ORCAMENTO_ID', pOrcamentoId, True);
          vItensDefLocais.SetInt('EMPRESA_ID', pEmpresaGeracaoId, True);
          vItensDefLocais.SetInt('PRODUTO_ID', pItens[i].ProdutoId, True);
          vItensDefLocais.SetInt('LOCAL_ID', pItens[i].Lotes[j].LocalId, True);
          vItensDefLocais.setString('LOTE', pItens[i].Lotes[j].Lote);
          vItensDefLocais.setDouble('QUANTIDADE_ATO', pItens[i].Lotes[j].QuantidadeRetirarAto);
          vItensDefLocais.setDouble('QUANTIDADE_ENTREGAR', pItens[i].Lotes[j].QuantidadeEntregar);
          vItensDefLocais.setDouble('QUANTIDADE_RETIRAR', pItens[i].Lotes[j].QuantidadeRetirar);
          vItensDefLocais.setInt('ITEM_ID', vPesq.GetInt('ITEM_ID') + 1);
          vItensDefLocais.setString('STATUS', 'PEN');

          vItensDefLocais.Inserir;
        end;
      end;

      for j := Low(pItens[i].Locais) to High(pItens[i].Locais) do begin
        vPesq.Pesquisar([pOrcamentoId]);

        vItensDefLocais.SetInt('ORCAMENTO_ID', pOrcamentoId, True);
        vItensDefLocais.SetInt('EMPRESA_ID', pItens[i].Locais[j].EmpresaId, True);
        vItensDefLocais.SetInt('PRODUTO_ID', pItens[i].produtoId, True);
        vItensDefLocais.SetInt('LOCAL_ID', pItens[i].Locais[j].LocalId, True);
        vItensDefLocais.setString('LOTE', '???');
        vItensDefLocais.setDouble('QUANTIDADE_ATO', pItens[i].Locais[j].QuantidadeAto);
        vItensDefLocais.setDouble('QUANTIDADE_ENTREGAR', pItens[i].Locais[j].QuantidadeEntregar);
        vItensDefLocais.setDouble('QUANTIDADE_RETIRAR', pItens[i].Locais[j].QuantidadeRetirar);
        vItensDefLocais.setInt('ITEM_ID', vPesq.GetInt('ITEM_ID') + 1);
        vItensDefLocais.setString('STATUS', 'PEN');

        vItensDefLocais.Inserir;
      end;
    end;

    vProcedimento.Params[0].AsInteger  := pOrcamentoId;
    vProcedimento.Params[1].AsInteger  := pEmpresaGeracaoId;
    vProcedimento.Params[2].AsDateTime := pDataHoraEntrega;
    vProcedimento.Params[3].Value := vProdutoIds;
    vProcedimento.Params[4].Value := vItensIds;
    vProcedimento.Params[5].Value := vQuantidades;

    vProcedimento.Executar;

    vExec.Limpar;
    vExec.Add('delete from ORCAMENTOS_ITENS_DEF_LOTES');
    vExec.Add('where ORCAMENTO_ID = :P1');
    vExec.Executar([pOrcamentoId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vItensDefLotes.Free;
  vProcedimento.Free;
  vExec.Free;
  vItensDefLocais.Free;
  vPesq.Free;
  vConsultaLote.Active := False;
  vConsultaLote.Free;
  vUpdate.Free;
end;


end.
