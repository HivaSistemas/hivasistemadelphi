unit _EstoquesDivisao;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsEstoques, _BibliotecaGenerica,
  _RecordsOrcamentosVendas;

{$M+}
type
  RecEstoquesDivisao = record
    EmpresaId: Integer;
    ProdutoId: Integer;
    LocalId: Integer;
    NomeLocal: string;
    NomeEmpresa: string;
    PontaEstoque: string;
    MultiploPontaEstoque: Double;
    Fisico: Double;
    Reservado: Double;
    Bloqueado: Currency;
    Disponivel: Currency;
    Lote: string;
    DataFabricacao: TDateTime;
    DataVencimento: TDateTime;
    NomeProduto: string;
    NomeMarca: string;
    UnidadeVenda: string;
    TipoControleEstoque: string;
    CustoTransferencia: Currency;
    ProdutoPaiId: Integer;
  end;

  RecEstoquesLocaisDisponiveis = record
    LocalId: Integer;
    Nome: string;
    Disponivel: Double;
  end;

  RecLotesDisponiveis = record
    EmpresaId: Integer;
    EmpresaNome: string;
    Lote: string;
    DataFabricacao: TDateTime;
    DataVencimento: TDateTime;
    DisponivelAto: Double;
    DisponivelRetirar: Double;
    DisponivelEntregar: Double;
    LocalId: Integer;
    LocalNome: string;
  end;

  TEstoquesDivisao = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordEstoquesDivisao: RecEstoquesDivisao;
  end;

function BuscarEstoque(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant
): TArray<RecEstoquesDivisao>;

function AtualizarLotesPontaEstoque(pConexao: TConexao; pItens: TArray<RecEstoquesDivisao>): RecRetornoBD;

function BuscarLotesDisponiveis(pConexao: TConexao; pEmpresaId: Integer; pProdutoId: Integer; pDefinirLocalManual: string): TArray<RecLotesDisponiveis>;

function BuscarEstoquesLocaisDisponiveis(pConexao: TConexao; pEmpresaId: Integer; pProdutoId: Integer): TArray<RecDefinicoesLocaisVenda>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TEstoquesDivisao }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 3);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ESD.PRODUTO_ID = :P1 ' +
      'order by ' +
      '  ESD.EMPRESA_ID '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ESD.EMPRESA_ID = :P1 ' +
      'and ESD.LOCAL_ID = :P2 ' +
      'and ESD.PRODUTO_ID = :P3 '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ESD.EMPRESA_ID = :P1 '
    );
end;

constructor TEstoquesDivisao.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ESTOQUES');

  FSql :=
    'select ' +
    '  ESD.EMPRESA_ID, ' +
    '  ESD.PRODUTO_ID, ' +
    '  ESD.LOCAL_ID, ' +
    '  ESD.NOME_LOCAL, ' +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA, ' +
    '  ESD.PONTA_ESTOQUE, ' +
    '  ESD.MULTIPLO_PONTA_ESTOQUE, ' +
    '  ESD.FISICO, ' +
    '  ESD.BLOQUEADO, ' +
    '  ESD.DISPONIVEL, ' +
    '  ESD.RESERVADO, ' +
    '  ESD.LOTE, ' +
    '  ESD.DATA_FABRICACAO, ' +
    '  ESD.DATA_VENCIMENTO, ' +
    '  ESD.NOME_PRODUTO, ' +
    '  ESD.NOME_MARCA, ' +
    '  ESD.UNIDADE_VENDA, ' +
    '  ESD.TIPO_CONTROLE_ESTOQUE, ' +
    '  ESD.CUSTO_TRANSFERENCIA, ' +
    '  ESD.PRODUTO_PAI_ID ' +
    'from ' +
    '  VW_ESTOQUES_DIVISAO ESD ' +

    'inner join EMPRESAS EMP ' +
    'on ESD.EMPRESA_ID = EMP.EMPRESA_ID ';

  setFiltros(getFiltros);

  AddColunaSL('EMPRESA_ID');
  AddColunaSL('PRODUTO_ID');
  AddColunaSL('LOCAL_ID');
  AddColunaSL('NOME_LOCAL');
  AddColunaSL('NOME_EMPRESA');
  AddColunaSL('PONTA_ESTOQUE');
  AddColunaSL('MULTIPLO_PONTA_ESTOQUE');
  AddColunaSL('FISICO');
  AddColunaSL('BLOQUEADO');
  AddColunaSL('DISPONIVEL');
  AddColunaSL('RESERVADO');
  AddColunaSL('LOTE');
  AddColunaSL('DATA_FABRICACAO');
  AddColunaSL('DATA_VENCIMENTO');
  AddColunaSL('NOME_PRODUTO');
  AddColunaSL('NOME_MARCA');
  AddColunaSL('UNIDADE_VENDA');
  AddColunaSL('TIPO_CONTROLE_ESTOQUE');
  AddColunaSL('CUSTO_TRANSFERENCIA');
  AddColunaSL('PRODUTO_PAI_ID');
end;

function TEstoquesDivisao.getRecordEstoquesDivisao: RecEstoquesDivisao;
begin
  Result.EmpresaId            := getInt('EMPRESA_ID');
  Result.ProdutoId            := getInt('PRODUTO_ID');
  Result.LocalId              := getInt('LOCAL_ID');
  Result.NomeLocal            := getString('NOME_LOCAL');
  Result.NomeEmpresa          := getString('NOME_EMPRESA');
  Result.PontaEstoque         := getString('PONTA_ESTOQUE');
  Result.MultiploPontaEstoque := getDouble('MULTIPLO_PONTA_ESTOQUE');
  Result.Fisico               := getDouble('FISICO');
  Result.Bloqueado            := getDouble('BLOQUEADO');
  Result.Disponivel           := getDouble('DISPONIVEL');
  Result.Reservado            := getDouble('RESERVADO');
  Result.Lote                 := getString('LOTE');
  Result.DataFabricacao       := getData('DATA_FABRICACAO');
  Result.DataVencimento       := getData('DATA_VENCIMENTO');
  Result.NomeProduto          := getString('NOME_PRODUTO');
  Result.NomeMarca            := getString('NOME_MARCA');
  Result.UnidadeVenda         := getString('UNIDADE_VENDA');
  Result.TipoControleEstoque  := getString('TIPO_CONTROLE_ESTOQUE');
  Result.CustoTransferencia   := getDouble('CUSTO_TRANSFERENCIA');
  Result.ProdutoPaiId         := getInt('PRODUTO_PAI_ID');
end;

function BuscarEstoque(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant
): TArray<RecEstoquesDivisao>;
var
  i: Integer;
  vDivisao: TEstoquesDivisao;
begin
  Result := nil;
  vDivisao := TEstoquesDivisao.Create(pConexao);

  if vDivisao.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, vDivisao.GetQuantidadeRegistros);
    for i := 0 to vDivisao.GetQuantidadeRegistros -1 do begin
      Result[i] := vDivisao.getRecordEstoquesDivisao;
      vDivisao.ProximoRegistro;
    end;
  end;
  vDivisao.Free;
end;

function AtualizarLotesPontaEstoque(pConexao: TConexao; pItens: TArray<RecEstoquesDivisao>): RecRetornoBD;
var
  i: Integer;
  vSql: TExecucao;
begin
  Result.TeveErro := False;
  vSql := TExecucao.Create(pConexao);
  pConexao.SetRotina('ATUALIZAR_PONTA_ESTOQUE');

  vSql.Add('update ESTOQUES_DIVISAO set ');
  vSql.Add('  PONTA_ESTOQUE = :P4, ');
  vSql.Add('  MULTIPLO_PONTA_ESTOQUE = :P5 ');
  vSql.Add('where EMPRESA_ID = :P1 ');
  vSql.Add('and PRODUTO_ID = :P2 ');
  vSql.Add('and LOTE = :P3 ');

  try
    pConexao.IniciarTransacao;

    for i := Low(pItens) to High(pItens) do begin
      vSql.Executar([
        pItens[i].EmpresaId,
        pItens[i].ProdutoId,
        pItens[i].Lote,
        pItens[i].PontaEstoque,
        pItens[i].MultiploPontaEstoque]
      );
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vSql.Free;
end;

function BuscarLotesDisponiveis(pConexao: TConexao; pEmpresaId: Integer; pProdutoId: Integer; pDefinirLocalManual: string): TArray<RecLotesDisponiveis>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  LOTE, ');
  if pDefinirLocalManual = 'S' then begin
    vSql.Add('  EMPRESA_ID, ');
    vSql.Add('  NOME_FANTASIA, ');
    vSql.Add('  LOCAL_ID, ');
    vSql.Add('  NOME_LOCAL, ');
  end;
  vSql.Add('  DATA_FABRICACAO, ');
  vSql.Add('  DATA_VENCIMENTO, ');
  vSql.Add('  sum(DISPONIVEL_RETIRAR_ATO) as DISPONIVEL_RETIRAR_ATO, ');
  vSql.Add('  sum(DISPONIVEL_RETIRAR) as DISPONIVEL_RETIRAR, ');
  vSql.Add('  sum(DISPONIVEL_ENTREGAR) as DISPONIVEL_ENTREGAR ');
  vSql.Add('from ( ');
  vSql.Add('  select ');
  vSql.Add('    DIV.LOTE, ');
  if pDefinirLocalManual = 'S' then begin
    vSql.Add('  DIV.EMPRESA_ID, ');
    vSql.Add('  DIV.NOME_FANTASIA, ');
    vSql.Add('  DIV.LOCAL_ID, ');
    vSql.Add('  DIV.NOME_LOCAL, ');
  end;
  vSql.Add('    DIV.DATA_FABRICACAO, ');
  vSql.Add('    DIV.DATA_VENCIMENTO, ');
  vSql.Add('    sum(DIV.DISPONIVEL) as DISPONIVEL_RETIRAR_ATO, ');
  vSql.Add('    0 as DISPONIVEL_RETIRAR, ');
  vSql.Add('    0 as DISPONIVEL_ENTREGAR ');
  vSql.Add('  from ');
  vSql.Add('    VW_ESTOQUES_DIVISAO DIV ');
  if pDefinirLocalManual = 'S' then begin
    vSql.Add('  INNER JOIN EMPRESAS EMP ON DIV.EMPRESA_ID = EMP.EMPRESA_ID ');
  end;
  vSql.Add('  inner join GRUPOS_ESTOQUES GRU ');
  vSql.Add('  on DIV.EMPRESA_ID = GRU.EMPRESA_ID ');
  vSql.Add('  and GRU.EMPRESA_GRUPO_ID = :P1 ');
  vSql.Add('  and GRU.TIPO = ''A'' ');
  vSql.Add('  where DIV.PRODUTO_ID = :P2 ');
  vSql.Add('  and DIV.LOTE <> ''???'' ');
  vSql.Add('  group by ');
  vSql.Add('    DIV.LOTE, ');
  if pDefinirLocalManual = 'S' then begin
    vSql.Add('  DIV.EMPRESA_ID, ');
    vSql.Add('  DIV.NOME_FANTASIA, ');
    vSql.Add('    DIV.LOCAL_ID, ');
    vSql.Add('    DIV.NOME_LOCAL, ');
  end;
  vSql.Add('    DIV.DATA_FABRICACAO, ');
  vSql.Add('    DIV.DATA_VENCIMENTO, ');
  vSql.Add('    GRU.TIPO ');

  vSql.Add('  union all ');

  vSql.Add('  select ');
  vSql.Add('    DIV.LOTE, ');
  if pDefinirLocalManual = 'S' then begin
    vSql.Add('  DIV.EMPRESA_ID, ');
    vSql.Add('  DIV.NOME_FANTASIA, ');
    vSql.Add('  DIV.LOCAL_ID, ');
    vSql.Add('  DIV.NOME_LOCAL, ');
  end;
  vSql.Add('    DIV.DATA_FABRICACAO, ');
  vSql.Add('    DIV.DATA_VENCIMENTO, ');
  vSql.Add('    0 as DISPONIVEL_RETIRAR_ATO, ');
  vSql.Add('    sum(DIV.DISPONIVEL) as DISPONIVEL_RETIRAR, ');
  vSql.Add('    0 as DISPONIVEL_ENTREGAR ');
  vSql.Add('  from ');
  vSql.Add('    VW_ESTOQUES_DIVISAO DIV ');
  if pDefinirLocalManual = 'S' then begin
    vSql.Add('  INNER JOIN EMPRESAS EMP ON DIV.EMPRESA_ID = EMP.EMPRESA_ID ');
  end;
  vSql.Add('  inner join GRUPOS_ESTOQUES GRU ');
  vSql.Add('  on DIV.EMPRESA_ID = GRU.EMPRESA_ID ');
  vSql.Add('  and GRU.EMPRESA_GRUPO_ID = :P1 ');
  vSql.Add('  and GRU.TIPO = ''R'' ');
  vSql.Add('  where DIV.PRODUTO_ID = :P2 ');
  vSql.Add('  and DIV.LOTE <> ''???'' ');
  vSql.Add('  group by ');
  vSql.Add('    DIV.LOTE, ');
  if pDefinirLocalManual = 'S' then begin
    vSql.Add('    DIV.EMPRESA_ID, ');
    vSql.Add('    DIV.NOME_FANTASIA, ');
    vSql.Add('    DIV.LOCAL_ID, ');
    vSql.Add('    DIV.NOME_LOCAL, ');
  end;
  vSql.Add('    DIV.DATA_FABRICACAO, ');
  vSql.Add('    DIV.DATA_VENCIMENTO, ');
  vSql.Add('    GRU.TIPO ');

  vSql.Add('  union all ');

  vSql.Add('  select ');
  vSql.Add('    DIV.LOTE, ');
  if pDefinirLocalManual = 'S' then begin
    vSql.Add('  DIV.EMPRESA_ID, ');
    vSql.Add('  DIV.NOME_FANTASIA, ');
    vSql.Add('  DIV.LOCAL_ID, ');
    vSql.Add('  DIV.NOME_LOCAL, ');
  end;
  vSql.Add('    DIV.DATA_FABRICACAO, ');
  vSql.Add('    DIV.DATA_VENCIMENTO, ');
  vSql.Add('    0 as DISPONIVEL_RETIRAR_ATO, ');
  vSql.Add('    0 as DISPONIVEL_RETIRAR, ');
  vSql.Add('    sum(DIV.DISPONIVEL) as DISPONIVEL_ENTREGAR ');
  vSql.Add('  from ');
  vSql.Add('    VW_ESTOQUES_DIVISAO DIV ');
  if pDefinirLocalManual = 'S' then begin
    vSql.Add('  INNER JOIN EMPRESAS EMP ON DIV.EMPRESA_ID = EMP.EMPRESA_ID ');
  end;
  vSql.Add('  inner join GRUPOS_ESTOQUES GRU ');
  vSql.Add('  on DIV.EMPRESA_ID = GRU.EMPRESA_ID ');
  vSql.Add('  and GRU.EMPRESA_GRUPO_ID = :P1 ');
  vSql.Add('  and GRU.TIPO = ''E'' ');
  vSql.Add('  where DIV.PRODUTO_ID = :P2 ');
  vSql.Add('  and DIV.LOTE <> ''???'' ');
  vSql.Add('  group by ');
  vSql.Add('    DIV.LOTE, ');
  if pDefinirLocalManual = 'S' then begin
    vSql.Add('    DIV.EMPRESA_ID, ');
    vSql.Add('    DIV.NOME_FANTASIA, ');
    vSql.Add('    DIV.LOCAL_ID, ');
    vSql.Add('    DIV.NOME_LOCAL, ');
  end;
  vSql.Add('    DIV.DATA_FABRICACAO, ');
  vSql.Add('    DIV.DATA_VENCIMENTO, ');
  vSql.Add('    GRU.TIPO ');
  vSql.Add(') ');
  vSql.Add('group by ');
  vSql.Add('  LOTE, ');
  if pDefinirLocalManual = 'S' then begin
    vSql.Add('    EMPRESA_ID, ');
    vSql.Add('    DIV.NOME_FANTASIA, ');
    vSql.Add('    LOCAL_ID, ');
    vSql.Add('    NOME_LOCAL, ');
  end;
  vSql.Add('  DATA_FABRICACAO, ');
  vSql.Add('  DATA_VENCIMENTO ');

  if vSql.Pesquisar([pEmpresaId, pProdutoId]) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].Lote               := vSql.GetString('LOTE');
      Result[i].DataFabricacao     := vSql.GetData('DATA_FABRICACAO');
      Result[i].DataVencimento     := vSql.GetData('DATA_VENCIMENTO');
      Result[i].DisponivelAto      := vSql.GetDouble('DISPONIVEL_RETIRAR_ATO');
      Result[i].DisponivelRetirar  := vSql.GetDouble('DISPONIVEL_RETIRAR');
      Result[i].DisponivelEntregar := vSql.GetDouble('DISPONIVEL_ENTREGAR');

      if pDefinirLocalManual = 'S' then begin
        Result[i].EmpresaId := vSql.GetInt('EMPRESA_ID');
        Result[i].EmpresaNome := vSql.GetString('NOME_FANTASIA');
        Result[i].LocalId := vSql.GetInt('LOCAL_ID');
        Result[i].LocalNome := vSql.GetString('NOME_LOCAL');
      end;

      vSql.Next;
    end;
  end;
  vSql.Free;
end;

function BuscarEstoquesLocaisDisponiveis(pConexao: TConexao; pEmpresaId: Integer; pProdutoId: Integer): TArray<RecDefinicoesLocaisVenda>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select distinct');
  vSql.Add('  GES.EMPRESA_GRUPO_ID as EMPRESA_ID, ');
  vSql.Add('  EMP.NOME_FANTASIA as NOME_EMPRESA, ');
  vSql.Add('  LOC.LOCAL_ID, ');
  vSql.Add('  LOC.NOME, ');
  vSql.Add('  EST.DISPONIVEL, ');
  vSql.Add('  EMP.NOME_FANTASIA, ');
  vSql.Add('  PRO.ACEITAR_ESTOQUE_NEGATIVO ');
  vSql.Add('  from ESTOQUES_DIVISAO EST ');

  vSql.Add('  inner join PRODUTOS PRO ');
  vSql.Add('  on PRO.PRODUTO_iD = EST.PRODUTO_ID ');

  vSql.Add('  inner join LOCAIS_PRODUTOS LOC ');
  vSql.Add('  on LOC.LOCAL_ID = EST.LOCAL_ID ');

  vSql.Add('  inner join GRUPOS_ESTOQUES GES ');
  vSql.Add('  on EST.EMPRESA_ID = GES.EMPRESA_GRUPO_ID ');

  vSql.Add('  inner join EMPRESAS EMP ');
  vSql.Add('  on EMP.EMPRESA_ID = EST.EMPRESA_ID ');

  vSql.Add('  and EST.PRODUTO_ID = :P1 ');
  vSql.Add('  and GES.EMPRESA_ID = :P2 ');

  vSql.Add('  order by LOC.LOCAL_ID, GES.EMPRESA_GRUPO_ID ');

  if vSql.Pesquisar([pProdutoId, pEmpresaId]) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].EmpresaId              := vSql.GetInt('EMPRESA_ID');
      Result[i].NomeEmpresa            := vSql.GetString('NOME_EMPRESA');
      Result[i].LocalId                := vSql.GetInt('LOCAL_ID');
      Result[i].Nome                   := vSql.GetString('NOME');
      Result[i].Disponivel             := vSql.GetDouble('DISPONIVEL');
      Result[i].AceitaEstoqueNegativo := vSql.GetString('ACEITAR_ESTOQUE_NEGATIVO');
      Result[i].ProdutoId              := pProdutoId;

      vSql.Next;
    end;
  end;
  vSql.Free;
end;

end.
