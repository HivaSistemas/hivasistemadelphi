unit _CadastroDesenvolvimento;

interface

uses
  _Conexao, _OperacoesBancoDados, System.SysUtils, _RecordsEspeciais, _RecordsCadastros;

{$M+}
type
  TCadastroDesenvolvimento = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecDesenvolvimento: RecDesenvolvimento;
  end;

function AtualizarCadastroDesenvolvimento(
  pConexao: TConexao;
  pDesenvolvimentoId: Integer;
  pPrioridade: string;
  pStatus: string;
  pTipoSolicitacao: string;
  pTextoHistorico : string;
  pTextoAnalise   : string;
  pTextoDesenvolvimento : string;
  pTextoTestes    : string;
  pTextoDocumentacao : string;
  pTextoLiberacao : string

): RecRetornoBD;

function BuscarDesenvolvimentos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pDesenvolvimento_id: Integer
): TArray<RecDesenvolvimento>;

function BuscarDesenvolvimentosComando(
  pConexao: TConexao;
  pComando: string
): TArray<RecDesenvolvimento>;

function ExcluirCadastroDesenvolvimento(
  pConexao: TConexao;
  pDesenvolvimentoId: Integer
): RecRetornoBD;

function AtualizarStatusDesenvolvimento(
  pConexao: TConexao;
  pDesenvolvimentoId: Integer;
  pNovoStatus: string
): RecRetornoBD;

function AtualizarTextos(
  pConexao: TConexao;
  pDesenvolvimentoId: Integer;
  pColuna: string;
  pTexto: string
): RecRetornoBD;

implementation

{ TCadastroDesenvolvimento }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] := 
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where DESENVOLVIMENTO_ID = :P1'
    );                               
    
end;

constructor TCadastroDesenvolvimento.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'DESENVOLVIMENTOS');

  FSql :=
    'select ' +
    '  DESENVOLVIMENTO_ID, ' +
    '  PRIORIDADE, ' +
    '  DATA_CADASTRO, ' +
    '  STATUS, ' +
    '  TIPO_SOLICITACAO,' +
    '  TEXTO_ANALISE, ' +
    '  TEXTO_DESENVOLVIMENTO, ' +
    '  TEXTO_HISTORICO, ' +
    '  TEXTO_LIBERACAO, ' +
    '  TEXTO_TESTES, ' +
    '  TEXTO_DOCUMENTACAO ' +
    'from ' +
    '  DESENVOLVIMENTOS';

  SetFiltros(GetFiltros);

  AddColuna('DESENVOLVIMENTO_ID', True);
  AddColuna('PRIORIDADE');
  AddColuna('STATUS');
  AddColuna('TIPO_SOLICITACAO');
  AddColunasl('DATA_CADASTRO');
  AddColuna('TEXTO_ANALISE');
  AddColuna('TEXTO_DESENVOLVIMENTO');
  AddColuna('TEXTO_HISTORICO');
  AddColuna('TEXTO_LIBERACAO');
  AddColuna('TEXTO_TESTES');
  AddColuna('TEXTO_DOCUMENTACAO');
end;

function AtualizarCadastroDesenvolvimento(
  pConexao: TConexao;
  pDesenvolvimentoId: Integer;
  pPrioridade: string;
  pStatus: string;
  pTipoSolicitacao: string;
  pTextoHistorico : string;
  pTextoAnalise   : string;
  pTextoDesenvolvimento : string;
  pTextoTestes    : string;
  pTextoDocumentacao : string;
  pTextoLiberacao : string
): RecRetornoBD;
var
  vNovo: Boolean;
  t: TCadastroDesenvolvimento;
begin
  Result.Iniciar;
  t := TCadastroDesenvolvimento.Create(pConexao);

  vNovo := ( pDesenvolvimentoId = 0 );
  if vNovo then begin
    pDesenvolvimentoId := TSequencia.Create(pConexao, 'SEQ_DESENVOLVIMENTO_ID').GetProximaSequencia;
    Result.AsInt := pDesenvolvimentoId;
  end;

  try
    t.SetInt('DESENVOLVIMENTO_ID', pDesenvolvimentoId, True);
    t.SetString('PRIORIDADE', pPrioridade);
    t.SetString('STATUS', pStatus);
    t.SetString('TIPO_SOLICITACAO', pTipoSolicitacao);
    t.SetString('TEXTO_HISTORICO', pTextoHistorico);
    t.SetString('TEXTO_ANALISE', pTextoAnalise);
    t.SetString('TEXTO_DESENVOLVIMENTO', pTextoDesenvolvimento);
    t.SetString('TEXTO_TESTES', pTextoTestes);
    t.SetString('TEXTO_DOCUMENTACAO', pTextoDocumentacao);
    t.SetString('TEXTO_LIBERACAO', pTextoLiberacao);
    pConexao.IniciarTransacao;

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  t.Free;
end;

function BuscarDesenvolvimentos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pDesenvolvimento_id: Integer
): TArray<RecDesenvolvimento>;
var
  i: Integer;
  t: TCadastroDesenvolvimento;
begin
  Result := nil;
  t := TCadastroDesenvolvimento.Create(pConexao);

  if t.Pesquisar(0, [pDesenvolvimento_id]) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecDesenvolvimento;
      t.ProximoRegistro;
    end;
  end;

  FreeAndNil(t);
end;

function BuscarDesenvolvimentosComando(
  pConexao: TConexao;
  pComando: string
): TArray<RecDesenvolvimento>;
var
  i: Integer;
  t: TCadastroDesenvolvimento;
begin
  Result := nil;
  t := TCadastroDesenvolvimento.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecDesenvolvimento;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirCadastroDesenvolvimento(
  pConexao: TConexao;
  pDesenvolvimentoId: Integer
): RecRetornoBD;
var
  t: TCadastroDesenvolvimento;
begin
  Result.TeveErro := False;
  t := TCadastroDesenvolvimento.Create(pConexao);

  try
    t.SetInt('DESENVOLVIMENTO_ID', pDesenvolvimentoId, True);
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function AtualizarStatusDesenvolvimento(
  pConexao: TConexao;
  pDesenvolvimentoId: Integer;
  pNovoStatus: string
): RecRetornoBD;
var
  ex: TExecucao;
begin
  Result.TeveErro := False;
  ex := TExecucao.Create(pConexao);

  ex.SQL.Add('update DESENVOLVIMENTOS set ');
  ex.SQL.Add('  STATUS = :P1');
  ex.SQL.Add('where DESENVOLVIMENTO_ID = :P2');

  try
    ex.Executar([pNovoStatus, pDesenvolvimentoId])
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  ex.Free;
end;

function AtualizarTextos(
  pConexao: TConexao;
  pDesenvolvimentoId: Integer;
  pColuna: string;
  pTexto: string
): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  vExec := TExecucao.Create(pConexao);

  vExec.Add('update DESENVOLVIMENTOS set ');
  vExec.Add('  ' + pColuna + ' = :P2 ');
  vExec.Add('where DESENVOLVIMENTO_ID = :P1 ');

  try
    pConexao.IniciarTransacao;

    vExec.Executar([pDesenvolvimentoId, pTexto]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  vExec.Free;
end;

function TCadastroDesenvolvimento.getRecDesenvolvimento: RecDesenvolvimento;
begin
  Result.desenvolvimento_id        := GetInt('DESENVOLVIMENTO_ID', True);
  Result.prioridade                := getString('PRIORIDADE');
  Result.tipo_solicitacao          := getString('TIPO_SOLICITACAO');
  Result.data_cadastro             := getData('DATA_CADASTRO');
  Result.status                    := GetString('STATUS');
  Result.texto_historico           := GetString('TEXTO_HISTORICO');
  Result.texto_analise             := GetString('TEXTO_ANALISE');
  Result.texto_desenvolvimento     := GetString('TEXTO_DESENVOLVIMENTO');
  Result.texto_testes              := GetString('TEXTO_TESTES');
  Result.texto_documentacao        := GetString('TEXTO_DOCUMENTACAO');
  Result.texto_liberacao           := GetString('TEXTO_LIBERACAO');
end;

end.
