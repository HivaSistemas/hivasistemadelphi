unit _Parametros;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TParametros = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordParametros: RecParametros;
  end;

function AtualizarParametros(
  pConexao: TConexao;
  pCadastroConsumidorFinalId: Integer;
  pIniciarVendaConsumidorFinal: string;
  pUtilizarAnaliseCusto: string;
  pQtdeMensagensObrigarLeitura: Integer;
  pQuantidadeDiasValidadeSenha: Integer;
  pMotivoAjuEstBxCompraId: Integer;
  pTipoCobrancaGeracaoCredId: Integer;
  pTipoCobrancaGerCredImpId: Integer;
  pTipoCobRecebEntregaId: Integer;
  pTipoCobAcumulativoId: Integer;
  pTipoCobAdiantamentoAcuId: Integer;
  pTipoCobAdiantamentoFinId: Integer;
  pTipoCobFechamentoTurnoId: Integer;
  pBloquearCreditoDevolucao: string;
  pBloquearCreditoPagarManual: string;
  pQtdeSegTravarAltisOcioso: Integer;
  pQtdeMaximaDiasDevolucao: Integer;
  pDefinirLocalManual: string;
  pAplicarIndiceDeducao: string;
  pEntradaFutura : string;
  pVendaRapida : string;
  pControleEntrega : string;
  pImprimeListaSeparacaoEntrega: string;
  pTipoCobEntradaNfeXmlId: Integer
): RecRetornoBD;

function BuscarParametros(pConexao: TConexao): RecParametros;

function getCompilacaoSistemaBanco(pConexao: TConexao): string;
function atualizarCompilacaoSistemaBanco(pConexao: TConexao; pNovaCompilacao: string): RecRetornoBD;
function FinalizarAnoMes(pConexao: TConexao): RecRetornoBD;

implementation

{ TParametros }

constructor TParametros.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PARAMETROS');

  FSql :=
    'select ' +
    '  CADASTRO_CONSUMIDOR_FINAL_ID, ' +
    '  INICIAR_VENDA_CONSUMIDOR_FINAL, ' +
    '  UTILIZAR_ANALISE_CUSTO, ' +
    '  QTDE_MENSAGENS_OBRIGAR_LEITURA, ' +
    '  QUANTIDADE_DIAS_VALIDADE_SENHA, ' +
    '  MOTIVO_AJU_EST_BX_COMPRA_ID, ' +
    '  TIPO_COBRANCA_GERACAO_CRED_ID, ' +
    '  TIPO_COBRANCA_GER_CRED_IMP_ID, ' +
    '  TIPO_COB_RECEB_ENTREGA_ID, ' +
    '  TIPO_COB_ACUMULATIVO_ID, ' +
    '  TIPO_COB_ADIANTAMENTO_ACU_ID, ' +
    '  TIPO_COB_ADIANTAMENTO_FIN_ID, ' +
    '  TIPO_COB_FECHAMENTO_TURNO_ID, ' +
    '  BLOQUEAR_CREDITO_DEVOLUCAO, ' +
    '  BLOQUEAR_CREDITO_PAGAR_MANUAL, ' +
    '  UTIL_MODULO_PRE_ENTRADAS, ' +
    '  QTDE_SEG_TRAVAR_ALTIS_OCIOSO, ' +
    '  QTDE_MAXIMA_DIAS_DEVOLUCAO, ' +
    '  DEFINIR_LOCAL_MANUAL, ' +
    '  APLICAR_INDICE_DEDUCAO, ' +
    '  UTILIZA_VENDA_RAPIDA, ' +
    '  UTILIZA_CONTROLE_ENTREGA, ' +
    '  IMPRIME_LISTA_SEPARACAO_VEND, ' +
    '  TIPO_COB_ENTRADA_NFE_XML_ID ' +
    'from ' +
    '  PARAMETROS';

  AddColuna('CADASTRO_CONSUMIDOR_FINAL_ID');
  AddColuna('INICIAR_VENDA_CONSUMIDOR_FINAL');
  AddColuna('UTILIZAR_ANALISE_CUSTO');
  AddColuna('QTDE_MENSAGENS_OBRIGAR_LEITURA');
  AddColuna('QUANTIDADE_DIAS_VALIDADE_SENHA');
  AddColuna('MOTIVO_AJU_EST_BX_COMPRA_ID');
  AddColuna('TIPO_COBRANCA_GERACAO_CRED_ID');
  AddColuna('TIPO_COBRANCA_GER_CRED_IMP_ID');
  AddColuna('TIPO_COB_RECEB_ENTREGA_ID');
  AddColuna('TIPO_COB_ACUMULATIVO_ID');
  AddColuna('TIPO_COB_ADIANTAMENTO_ACU_ID');
  AddColuna('TIPO_COB_ADIANTAMENTO_FIN_ID');
  AddColuna('TIPO_COB_FECHAMENTO_TURNO_ID');
  AddColuna('BLOQUEAR_CREDITO_DEVOLUCAO');
  AddColuna('BLOQUEAR_CREDITO_PAGAR_MANUAL');
  AddColuna('QTDE_SEG_TRAVAR_ALTIS_OCIOSO');
  AddColuna('QTDE_MAXIMA_DIAS_DEVOLUCAO');
  AddColuna('UTIL_MODULO_PRE_ENTRADAS');
  AddColuna('DEFINIR_LOCAL_MANUAL');
  AddColuna('APLICAR_INDICE_DEDUCAO');
  AddColuna('UTILIZA_VENDA_RAPIDA');
  AddColuna('UTILIZA_CONTROLE_ENTREGA');
  addColuna('IMPRIME_LISTA_SEPARACAO_VEND');

end;

function TParametros.getRecordParametros: RecParametros;
begin
  Result.cadastro_consumidor_final_id   := getInt('CADASTRO_CONSUMIDOR_FINAL_ID');
  Result.iniciar_venda_consumidor_final := getString('INICIAR_VENDA_CONSUMIDOR_FINAL');
  Result.UtilizarAnaliseCusto           := getString('UTILIZAR_ANALISE_CUSTO');
  Result.QtdeMensagensObrigarLeitura    := getInt('QTDE_MENSAGENS_OBRIGAR_LEITURA');
  Result.QuantidadeDiasValidadeSenha    := getInt('QUANTIDADE_DIAS_VALIDADE_SENHA');
  Result.MotivoAjuEstBxCompraId         := getInt('MOTIVO_AJU_EST_BX_COMPRA_ID');
  Result.TipoCobrancaGeracaoCredId      := getInt('TIPO_COBRANCA_GERACAO_CRED_ID');
  Result.TipoCobrancaGerCredImpId       := getInt('TIPO_COBRANCA_GER_CRED_IMP_ID');
  Result.TipoCobRecebEntregaId          := getInt('TIPO_COB_RECEB_ENTREGA_ID');
  Result.TipoCobAcumulativoId           := getInt('TIPO_COB_ACUMULATIVO_ID');
  Result.TipoCobAdiantamentoAcuId       := getInt('TIPO_COB_ADIANTAMENTO_ACU_ID');
  Result.TipoCobAdiantamentoFinId       := getInt('TIPO_COB_ADIANTAMENTO_FIN_ID');
  Result.TipoCobFechamentoTurnoId       := getInt('TIPO_COB_FECHAMENTO_TURNO_ID');
  Result.BloquearCreditoDevolucao       := getString('BLOQUEAR_CREDITO_DEVOLUCAO');
  Result.BloquearCreditoPagarManual     := getString('BLOQUEAR_CREDITO_PAGAR_MANUAL');
  Result.UtilModuloPreEntradas          := getString('UTIL_MODULO_PRE_ENTRADAS');
  Result.QtdeSegTravarAltisOcioso       := getInt('QTDE_SEG_TRAVAR_ALTIS_OCIOSO');
  Result.QtdeMaximaDiasDevolucao        := getInt('QTDE_MAXIMA_DIAS_DEVOLUCAO');
  Result.DefinirLocalManual             := getString('DEFINIR_LOCAL_MANUAL');
  Result.AplicarIndiceDeducao           := getString('APLICAR_INDICE_DEDUCAO');
  Result.UtilVendaRapida                := getString('UTILIZA_VENDA_RAPIDA');
  Result.UtilControleEntrega            := getString('UTILIZA_CONTROLE_ENTREGA');
  Result.ImprimirListaSeparacaoVenda    := getString('IMPRIME_LISTA_SEPARACAO_VEND');


end;

function AtualizarParametros(
  pConexao: TConexao;
  pCadastroConsumidorFinalId: Integer;
  pIniciarVendaConsumidorFinal: string;
  pUtilizarAnaliseCusto: string;
  pQtdeMensagensObrigarLeitura: Integer;
  pQuantidadeDiasValidadeSenha: Integer;
  pMotivoAjuEstBxCompraId: Integer;
  pTipoCobrancaGeracaoCredId: Integer;
  pTipoCobrancaGerCredImpId: Integer;
  pTipoCobRecebEntregaId: Integer;
  pTipoCobAcumulativoId: Integer;
  pTipoCobAdiantamentoAcuId: Integer;
  pTipoCobAdiantamentoFinId: Integer;
  pTipoCobFechamentoTurnoId: Integer;
  pBloquearCreditoDevolucao: string;
  pBloquearCreditoPagarManual: string;
  pQtdeSegTravarAltisOcioso: Integer;
  pQtdeMaximaDiasDevolucao: Integer;
  pDefinirLocalManual: string;
  pAplicarIndiceDeducao: string;
  pEntradaFutura : string;
  pVendaRapida : string;
  pControleEntrega : string;
  pImprimeListaSeparacaoEntrega: string;
  pTipoCobEntradaNfeXmlId: Integer
): RecRetornoBD;
var
  t: TParametros;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_PARAMETROS');

  t := TParametros.Create(pConexao);

  try
    t.setInt('CADASTRO_CONSUMIDOR_FINAL_ID', pCadastroConsumidorFinalId);
    t.setString('INICIAR_VENDA_CONSUMIDOR_FINAL', pIniciarVendaConsumidorFinal);
    t.setString('UTILIZAR_ANALISE_CUSTO', pUtilizarAnaliseCusto);
    t.setInt('QTDE_MENSAGENS_OBRIGAR_LEITURA', pQtdeMensagensObrigarLeitura);
    t.setInt('QUANTIDADE_DIAS_VALIDADE_SENHA', pQuantidadeDiasValidadeSenha);
    t.setIntN('MOTIVO_AJU_EST_BX_COMPRA_ID', pMotivoAjuEstBxCompraId);

    t.setIntN('TIPO_COBRANCA_GERACAO_CRED_ID', pTipoCobrancaGeracaoCredId);
    t.setIntN('TIPO_COBRANCA_GER_CRED_IMP_ID', pTipoCobrancaGerCredImpId);
    t.setIntN('TIPO_COB_RECEB_ENTREGA_ID', pTipoCobRecebEntregaId);
    t.setIntN('TIPO_COB_ACUMULATIVO_ID', pTipoCobAcumulativoId);
    t.setIntN('TIPO_COB_ADIANTAMENTO_ACU_ID', pTipoCobAdiantamentoAcuId);
    t.setIntN('TIPO_COB_ADIANTAMENTO_FIN_ID', pTipoCobAdiantamentoFinId);
    t.setIntN('TIPO_COB_FECHAMENTO_TURNO_ID', pTipoCobFechamentoTurnoId);

    t.setString('BLOQUEAR_CREDITO_DEVOLUCAO', pBloquearCreditoDevolucao);
    t.setString('BLOQUEAR_CREDITO_PAGAR_MANUAL', pBloquearCreditoPagarManual);
    t.setInt('QTDE_SEG_TRAVAR_ALTIS_OCIOSO', pQtdeSegTravarAltisOcioso);
    t.setInt('QTDE_MAXIMA_DIAS_DEVOLUCAO', pQtdeMaximaDiasDevolucao);
    t.setString('DEFINIR_LOCAL_MANUAL', pDefinirLocalManual);
    t.setString('APLICAR_INDICE_DEDUCAO', pAplicarIndiceDeducao);
    t.setString('UTIL_MODULO_PRE_ENTRADAS', pEntradaFutura);
    t.setString('UTILIZA_VENDA_RAPIDA', pVendaRapida);
    t.setString('UTILIZA_CONTROLE_ENTREGA', pControleEntrega);
    t.setString('IMPRIME_LISTA_SEPARACAO_VEND', pImprimeListaSeparacaoEntrega);
    t.setIntN('TIPO_COB_ENTRADA_NFE_XML_ID', pTipoCobEntradaNfeXmlId);

    t.Atualizar;
  except
    on e: Exception do
      Result.TratarErro(e);
  end;

  t.Free;
end;

function BuscarParametros(pConexao: TConexao): RecParametros;
var
  t: TParametros;
begin
  t := TParametros.Create(pConexao);

  t.PesquisarComando('');
  Result := t.getRecordParametros;

  t.Free;
end;

function getCompilacaoSistemaBanco(pConexao: TConexao): string;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select VERSAO_ATUAL from PARAMETROS');
  vSql.Pesquisar;

  Result := vSql.getString(0);
  
  vSql.Active := False;
  vSql.Free;
end;

function atualizarCompilacaoSistemaBanco(pConexao: TConexao; pNovaCompilacao: string): RecRetornoBD;
var
  vSql: TExecucao;
begin
  Result.TeveErro := False;
  vSql := TExecucao.Create(pConexao);

  try
    pConexao.IniciarTransacao;
    
    vSql.SQL.Add('update PARAMETROS set VERSAO_ATUAL = :P1');
    vSql.Executar([pNovaCompilacao]);
  
    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vSql.Free;
end;

function FinalizarAnoMes(pConexao: TConexao): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('FINALIZAR_ANO_MES');

  vProc := TProcedimentoBanco.Create(pConexao, 'FINALIZAR_ANO_MES');
  try
    pConexao.IniciarTransacao;

    vProc.Executar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  vProc.Free;
end;

end.
