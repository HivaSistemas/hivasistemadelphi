unit _FormacaoCustosPrecos;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, _Biblioteca;

type
  RecItensFormacaoCusto = record
    ProdutoId: Integer;
    Nome: string;
    NomeMarca: string;
    UnidadeVenda: string;
    CodigoBarras: string;
    CodigoOriginalFabricante: string;
    Unidade: string;
    EstoqueDisponivel: Double;

    PrecoTabela: Double;
    PercentualDescontoEntrada: Double;
    PercentualIpiEntrada: Double;
    PercentualOutrasDespesasEnt: Double;
    PercentualFreteEntrada: Double;
    PercentualSTEntrada: Double;
    PercentualIcmsEntrada: Double;
    PercentualIcmsFreteEntrada: Double;
    PercentualPisEntrada: Double;
    PercentualCofinsEntrada: Double;
    PercentualOutrosCustosEntr: Double;
    PercentualCustoFixoEnt: Double;
    PercentualPisFreteEntrada: Double;
    PercentualCofinsFreteEnt: Double;
    PercentualDifalEntrada: Double;

    CustoPedidoMedio: Double;
    CMV: Double;

    PercMargemPrecoVarejo: Double;
    PrecoVarejo: Double;
    QuantidadeMinPrecoVarejo: Double;
    DataPrecoVarejo: Double;
    PercMargemPrecoPontaEst: Double;
    PrecoPontaEstoque: Double;
    DataPrecoPontaEstoque: TDateTime;
    PercMargemPrecoAtacado1: Double;
    PrecoAtacado1: Double;
    QuantidadeMinPrecoAtac1: Double;
    DataPrecoAtacado1: TDateTime;
    PercMargemPrecoAtacado2: Double;
    PrecoAtacado2: Double;
    QuantidadeMinPrecoAtac2: Double;
    DataPrecoAtacado2: TDateTime;
    PercMargemPrecoAtacado3: Double;
    PrecoAtacado3: Double;
    QuantidadeMinPrecoAtac3: Double;
    DataPrecoAtacado3: TDateTime;
    PercMargemPrecoPdv: Double;
    PrecoPdv: Double;
    QuantidadeMinimaPdv: Double;
    DataPrecoPdv: TDateTime;
    PercComissaoVista: Double;
    PercComissaoPrazo: Double;
    PercentualCustoVenda: Double;
    CustoCompraComercial: Double;
    CustoUltimoPedido: Double;
    PrecoFinal: Double;
  end;

function BuscarProdutosFormacao(pConexao: TConexao; pEmpresaId: Integer; pFiltro: string): TArray<RecItensFormacaoCusto>;
function AtualizarCustos(pConexao: TConexao; pEmpresasIds: TArray<Integer>; pTipo: string; pItens: TArray<RecItensFormacaoCusto>; pColunasAlterar: TArray<string>): RecRetornoBD;
function AtualizarPrecos(pConexao: TConexao; pEmpresasIds: TArray<Integer>; pItens: TArray<RecItensFormacaoCusto>; pColunasAlterar: TArray<string>): RecRetornoBD;
function BuscarPercentualCustoVenda(pConexao: TConexao; pEmpresaId: Integer; pProdutoId: Integer): Double;
function BuscarPrecoVarejo(pConexao: TConexao; pEmpresaId: Integer; pFiltro: string): TArray<RecItensFormacaoCusto>;
function AtualizarPrecoVarejo(pConexao: TConexao; pEmpresasIds: TArray<Integer>; pItens: TArray<RecItensFormacaoCusto>): RecRetornoBD;
function BuscarPrecoVendaAnterior(pConexao: TConexao; pEmpresaId: Integer; pProdutoId: Integer): string;

implementation

function BuscarProdutosFormacao(pConexao: TConexao; pEmpresaId: Integer; pFiltro: string): TArray<RecItensFormacaoCusto>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  PCU.EMPRESA_ID, ');
  vSql.Add('  PCU.PRODUTO_ID, ');
  vSql.Add('  PRO.NOME, ');
  vSql.Add('  PRO.CODIGO_BARRAS, ');
  vSql.Add('  PRO.CODIGO_ORIGINAL_FABRICANTE, ');
  vSql.Add('  PRO.UNIDADE_VENDA, ');
  vSql.Add('  MAR.NOME as NOME_MARCA, ');
  vSql.Add('  EST.DISPONIVEL, ');

  (* Utilizado para o custo PRECO_FINAL *)
  vSql.Add('  PCU.PRECO_TABELA, ');
  vSql.Add('  PCU.PERCENTUAL_DESCONTO_ENTRADA, ');
  vSql.Add('  PCU.PERCENTUAL_IPI_ENTRADA, ');
  vSql.Add('  PCU.PERCENTUAL_OUTRAS_DESPESAS_ENT, ');
  vSql.Add('  PCU.PERCENTUAL_FRETE_ENTRADA, ');
  vSql.Add('  PCU.PERCENTUAL_ST_ENTRADA, ');
  vSql.Add('  PCU.PERCENTUAL_ICMS_ENTRADA, ');
  vSql.Add('  PCU.PERCENTUAL_ICMS_FRETE_ENTRADA, ');
  vSql.Add('  PCU.PERCENTUAL_PIS_ENTRADA, ');
  vSql.Add('  PCU.PERCENTUAL_COFINS_ENTRADA, ');
  vSql.Add('  PCU.PERCENTUAL_OUTROS_CUSTOS_ENTR, ');
  vSql.Add('  PCU.PERCENTUAL_CUSTO_FIXO_ENT, ');
  vSql.Add('  PCU.PERCENTUAL_PIS_FRETE_ENTRADA, ');
  vSql.Add('  PCU.PERCENTUAL_COFINS_FRETE_ENT, ');
  vSql.Add('  PCU.PERCENTUAL_DIFAL_ENTRADA, ');

  (* Utilizado para o custo PRECO_COMPRA *)
  vSql.Add('  PCU.CUSTO_PEDIDO_MEDIO, ');
  vSql.Add('  PCU.CUSTO_ULTIMO_PEDIDO, ');

  (* Utilizado para o custo CMV *)
  vSql.Add('  PCU.CMV, ');

  vSql.Add('  PCU.CUSTO_COMPRA_COMERCIAL, ');

  (* Pre�os *)
  vSql.Add('  PRE.PERC_MARGEM_PRECO_VAREJO, ');
  vSql.Add('  PRE.PRECO_VAREJO, ');
  vSql.Add('  PRE.QUANTIDADE_MIN_PRECO_VAREJO, ');
  vSql.Add('  PRE.DATA_PRECO_VAREJO, ');
  vSql.Add('  PRE.PERC_MARGEM_PRECO_PONTA_EST, ');
  vSql.Add('  PRE.PRECO_PONTA_ESTOQUE, ');
  vSql.Add('  PRE.DATA_PRECO_PONTA_ESTOQUE, ');
  vSql.Add('  PRE.PERC_MARGEM_PRECO_ATACADO_1, ');
  vSql.Add('  PRE.PRECO_ATACADO_1, ');
  vSql.Add('  PRE.QUANTIDADE_MIN_PRECO_ATAC_1, ');
  vSql.Add('  PRE.DATA_PRECO_ATACADO_1, ');
  vSql.Add('  PRE.PERC_MARGEM_PRECO_ATACADO_2, ');
  vSql.Add('  PRE.PRECO_ATACADO_2, ');
  vSql.Add('  PRE.QUANTIDADE_MIN_PRECO_ATAC_2, ');
  vSql.Add('  PRE.DATA_PRECO_ATACADO_2, ');
  vSql.Add('  PRE.PERC_MARGEM_PRECO_ATACADO_3, ');
  vSql.Add('  PRE.PRECO_ATACADO_3, ');
  vSql.Add('  PRE.QUANTIDADE_MIN_PRECO_ATAC_3, ');
  vSql.Add('  PRE.DATA_PRECO_ATACADO_3, ');
  vSql.Add('  PRE.PERC_MARGEM_PRECO_PDV, ');
  vSql.Add('  PRE.PRECO_PDV, ');
  vSql.Add('  PRE.QUANTIDADE_MINIMA_PDV, ');
  vSql.Add('  PRE.DATA_PRECO_PDV, ');

  vSql.Add('  PRE.PERC_COMISSAO_A_VISTA, ');
  vSql.Add('  PRE.PERC_COMISSAO_A_PRAZO, ');
  (* Custos da empresa *)
  vSql.Add('  BUSCAR_PERC_CUSTO_VENDA_PROD(PCU.EMPRESA_ID, PCU.PRODUTO_ID) as PERCENTUAL_CUSTO_VENDA ');
  vSql.Add('from ');
  vSql.Add('  VW_CUSTOS_PRODUTOS PCU ');

  vSql.Add('inner join PRECOS_PRODUTOS PRE ');
  vSql.Add('on PCU.EMPRESA_ID = PRE.EMPRESA_ID ');
  vSql.Add('and PCU.PRODUTO_ID = PRE.PRODUTO_ID ');

  vSql.Add('inner join PRODUTOS PRO ');
  vSql.Add('on PCU.PRODUTO_ID = PRO.PRODUTO_ID ');

  vSql.Add('inner join MARCAS MAR ');
  vSql.Add('on PRO.MARCA_ID = MAR.MARCA_ID ');

  vSql.Add('inner join VW_ESTOQUES EST ');
  vSql.Add('on PRO.PRODUTO_ID = EST.PRODUTO_ID ');
  vSql.Add('and EST.EMPRESA_ID = :P1 ');

  vSql.Add('inner join PERCENTUAIS_CUSTOS_EMPRESA PCE ');
  vSql.Add('on PCU.EMPRESA_ID = PCE.EMPRESA_ID ');

  vSql.Add('where PRO.ATIVO = ''S'' ');
  vSql.Add('and PRO.TIPO_CONTROLE_ESTOQUE not in(''K'', ''A'') ');
  vSql.Add('and PCU.EMPRESA_ID = :P1 ');
  vSql.Add(pFiltro);

  if vSql.Pesquisar([pEmpresaId]) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].ProdutoId                   := vSql.GetInt('PRODUTO_ID');
      Result[i].Nome                        := vSql.GetString('NOME');
      Result[i].NomeMarca                   := vSql.GetString('NOME_MARCA');
      Result[i].UnidadeVenda                := vSql.GetString('UNIDADE_VENDA');
      Result[i].CodigoBarras                := vSql.GetString('CODIGO_BARRAS');
      Result[i].CodigoOriginalFabricante    := vSql.GetString('CODIGO_ORIGINAL_FABRICANTE');
      Result[i].EstoqueDisponivel           := vSql.GetDouble('DISPONIVEL');

      Result[i].PrecoTabela                 := vSql.GetDouble('PRECO_TABELA');
      Result[i].PercentualDescontoEntrada   := vSql.GetDouble('PERCENTUAL_DESCONTO_ENTRADA');
      Result[i].PercentualIpiEntrada        := vSql.GetDouble('PERCENTUAL_IPI_ENTRADA');
      Result[i].PercentualOutrasDespesasEnt := vSql.GetDouble('PERCENTUAL_OUTRAS_DESPESAS_ENT');
      Result[i].PercentualFreteEntrada      := vSql.GetDouble('PERCENTUAL_FRETE_ENTRADA');
      Result[i].PercentualSTEntrada         := vSql.GetDouble('PERCENTUAL_ST_ENTRADA');
      Result[i].PercentualIcmsEntrada       := vSql.GetDouble('PERCENTUAL_ICMS_ENTRADA');
      Result[i].PercentualIcmsFreteEntrada  := vSql.GetDouble('PERCENTUAL_ICMS_FRETE_ENTRADA');
      Result[i].PercentualPisEntrada        := vSql.GetDouble('PERCENTUAL_PIS_ENTRADA');
      Result[i].PercentualCofinsEntrada     := vSql.GetDouble('PERCENTUAL_COFINS_ENTRADA');
      Result[i].PercentualOutrosCustosEntr  := vSql.GetDouble('PERCENTUAL_OUTROS_CUSTOS_ENTR');
      Result[i].PercentualCustoFixoEnt      := vSql.GetDouble('PERCENTUAL_CUSTO_FIXO_ENT');
      Result[i].PercentualPisFreteEntrada   := vSql.GetDouble('PERCENTUAL_PIS_FRETE_ENTRADA');
      Result[i].PercentualCofinsFreteEnt    := vSql.GetDouble('PERCENTUAL_COFINS_FRETE_ENT');
      Result[i].PercentualDifalEntrada      := vSql.GetDouble('PERCENTUAL_DIFAL_ENTRADA');

      Result[i].CustoPedidoMedio            := vSql.GetDouble('CUSTO_PEDIDO_MEDIO');
      Result[i].CMV                         := vSql.GetDouble('CMV');
      Result[i].CustoCompraComercial        := vSql.GetDouble('CUSTO_COMPRA_COMERCIAL');
      Result[i].CustoUltimoPedido           := vSql.GetDouble('CUSTO_ULTIMO_PEDIDO');

      Result[i].PercMargemPrecoVarejo       := vSql.GetDouble('PERC_MARGEM_PRECO_VAREJO');
      Result[i].PrecoVarejo                 := vSql.GetDouble('PRECO_VAREJO');
      Result[i].QuantidadeMinPrecoVarejo    := vSql.GetDouble('QUANTIDADE_MIN_PRECO_VAREJO');
      Result[i].DataPrecoVarejo             := vSql.GetData('DATA_PRECO_VAREJO');
      Result[i].PercMargemPrecoPontaEst     := vSql.GetDouble('PERC_MARGEM_PRECO_PONTA_EST');
      Result[i].PrecoPontaEstoque           := vSql.GetDouble('PRECO_PONTA_ESTOQUE');
      Result[i].DataPrecoPontaEstoque       := vSql.GetData('DATA_PRECO_PONTA_ESTOQUE');
      Result[i].PercMargemPrecoAtacado1     := vSql.GetDouble('PERC_MARGEM_PRECO_ATACADO_1');
      Result[i].PrecoAtacado1               := vSql.GetDouble('PRECO_ATACADO_1');
      Result[i].QuantidadeMinPrecoAtac1     := vSql.GetDouble('QUANTIDADE_MIN_PRECO_ATAC_1');
      Result[i].DataPrecoAtacado1           := vSql.GetData('DATA_PRECO_ATACADO_1');
      Result[i].PercMargemPrecoAtacado2     := vSql.GetDouble('PERC_MARGEM_PRECO_ATACADO_2');
      Result[i].PrecoAtacado2               := vSql.GetDouble('PRECO_ATACADO_2');
      Result[i].QuantidadeMinPrecoAtac2     := vSql.GetDouble('QUANTIDADE_MIN_PRECO_ATAC_2');
      Result[i].DataPrecoAtacado2           := vSql.GetData('DATA_PRECO_ATACADO_2');
      Result[i].PercMargemPrecoAtacado3     := vSql.GetDouble('PERC_MARGEM_PRECO_ATACADO_3');
      Result[i].PrecoAtacado3               := vSql.GetDouble('PRECO_ATACADO_3');
      Result[i].QuantidadeMinPrecoAtac3     := vSql.GetDouble('QUANTIDADE_MIN_PRECO_ATAC_3');
      Result[i].DataPrecoAtacado3           := vSql.GetData('DATA_PRECO_ATACADO_3');
      Result[i].PercMargemPrecoPdv          := vSql.GetDouble('PERC_MARGEM_PRECO_PDV');
      Result[i].PrecoPdv                    := vSql.GetDouble('PRECO_PDV');
      Result[i].QuantidadeMinimaPdv         := vSql.GetDouble('QUANTIDADE_MINIMA_PDV');
      Result[i].DataPrecoPdv                := vSql.GetData('DATA_PRECO_PDV');
      Result[i].PercentualCustoVenda        := vSql.GetDouble('PERCENTUAL_CUSTO_VENDA');

      vSql.Next;
    end;
  end;

  vSql.Free;
end;

function AtualizarCustos(pConexao: TConexao; pEmpresasIds: TArray<Integer>; pTipo: string; pItens: TArray<RecItensFormacaoCusto>; pColunasAlterar: TArray<string>): RecRetornoBD;
var
  i: Integer;
  j: Integer;
  k: Integer;
  vExec: TExecucao;
  //vParametros: TArray<Double>;

  vParametros: array of Variant;

  procedure AdicionarParametro(pColuna: string; pValor: Variant; pAdicionarVirgular: Boolean = True);
  begin
    vExec.Add(' ' + _Biblioteca.IIfStr(pAdicionarVirgular and (vParametros <> nil), ',', '') + pColuna +' = :P' + _Biblioteca.NFormat(High(vParametros) + 2));

    SetLength(vParametros, Length(vParametros) + 1);
    vParametros[High(vParametros)] := pValor;
  end;

begin
  Result.TeveErro := False;

  if pColunasAlterar = nil then
    Exit;

  pConexao.SetRotina('ATUALIZAR_CUSTOS');
  vExec := TExecucao.Create(pConexao);

  try
    for i := Low(pEmpresasIds) to High(pEmpresasIds) do begin
      for j := Low(pItens) to High(pItens) do begin
        if  pTipo = 'FIN' then begin
          // Pre�o final
          vExec.Clear;
          vParametros := nil;
          vExec.Add('update CUSTOS_PRODUTOS set ');

          for k := Low(pColunasAlterar) to High(pColunasAlterar) do begin

            if pColunasAlterar[k] = 'PRECO_TABELA' then
              AdicionarParametro('PRECO_TABELA', pItens[j].PrecoTabela)
            else if pColunasAlterar[k] = 'PERCENTUAL_DESCONTO_ENTRADA' then
              AdicionarParametro('PERCENTUAL_DESCONTO_ENTRADA', pItens[j].PercentualDescontoEntrada)
            else if pColunasAlterar[k] = 'PERCENTUAL_OUTRAS_DESPESAS_ENT' then
              AdicionarParametro('PERCENTUAL_OUTRAS_DESPESAS_ENT', pItens[j].PercentualOutrasDespesasEnt)
            else if pColunasAlterar[k] = 'PERCENTUAL_IPI_ENTRADA' then
              AdicionarParametro('PERCENTUAL_IPI_ENTRADA', pItens[j].PercentualIpiEntrada)
            else if pColunasAlterar[k] = 'PERCENTUAL_FRETE_ENTRADA' then
              AdicionarParametro('PERCENTUAL_FRETE_ENTRADA', pItens[j].PercentualFreteEntrada)
            else if pColunasAlterar[k] = 'PERCENTUAL_ST_ENTRADA' then
              AdicionarParametro('PERCENTUAL_ST_ENTRADA', pItens[j].PercentualSTEntrada)
            else if pColunasAlterar[k] = 'PERCENTUAL_ICMS_ENTRADA' then
              AdicionarParametro('PERCENTUAL_ICMS_ENTRADA', pItens[j].PercentualIcmsEntrada)
            else if pColunasAlterar[k] = 'PERCENTUAL_ICMS_FRETE_ENTRADA' then
              AdicionarParametro('PERCENTUAL_ICMS_FRETE_ENTRADA', pItens[j].PercentualIcmsFreteEntrada)
            else if pColunasAlterar[k] = 'PERCENTUAL_PIS_ENTRADA' then
              AdicionarParametro('PERCENTUAL_PIS_ENTRADA', pItens[j].PercentualPisEntrada)
            else if pColunasAlterar[k] = 'PERCENTUAL_COFINS_ENTRADA' then
              AdicionarParametro('PERCENTUAL_COFINS_ENTRADA', pItens[j].PercentualCofinsEntrada)
            else if pColunasAlterar[k] = 'PERCENTUAL_OUTROS_CUSTOS_ENTR' then
              AdicionarParametro('PERCENTUAL_OUTROS_CUSTOS_ENTR', pItens[j].PercentualOutrosCustosEntr)
            else if pColunasAlterar[k] = 'PERCENTUAL_DIFAL_ENTRADA' then
              AdicionarParametro('PERCENTUAL_DIFAL_ENTRADA', pItens[j].PercentualDifalEntrada)
            else if pColunasAlterar[k] = 'PERCENTUAL_CUSTO_FIXO_ENT' then
              AdicionarParametro('PERCENTUAL_CUSTO_FIXO_ENT', pItens[j].PercentualCustoFixoEnt)
            else if pColunasAlterar[k] = 'CUSTO_COMPRA_COMERCIAL' then
              AdicionarParametro('CUSTO_COMPRA_COMERCIAL', pItens[j].CustoCompraComercial)
            else if pColunasAlterar[k] = 'PRECO_FINAL' then
              AdicionarParametro('PRECO_FINAL', pItens[j].CustoCompraComercial);
          end;
          AdicionarParametro('PRECO_LIQUIDO', pItens[j].CustoCompraComercial);

          //Filtros where do update
          AdicionarParametro('where EMPRESA_ID', pEmpresasIds[i], False);
          AdicionarParametro('and PRODUTO_ID', pItens[j].ProdutoId, False);

         vExec.Executar(vParametros);
        end
        else if pTipo = 'COM' then begin
          // Pre�o de compra
          vExec.Clear;
          vExec.Add('update CUSTOS_PRODUTOS set ');
          vExec.Add('  CUSTO_ULTIMO_PEDIDO = :P3 ');
          vExec.Add('where EMPRESA_ID = :P1 ');
          vExec.Add('and PRODUTO_ID = :P2 ');

          if _BibliotecaGenerica.NFormatN(pItens[j].CustoUltimoPedido) = '' then
            Continue;

          vExec.Executar([
            pEmpresasIds[i],
            pItens[j].ProdutoId,
            pItens[j].CustoUltimoPedido
          ]);
        end
        else begin
          // CMV
          vExec.Clear;
          vExec.Add('update CUSTOS_PRODUTOS set ');
          vExec.Add('  CMV = :P3 ');
          vExec.Add('where EMPRESA_ID = :P1 ');
          vExec.Add('and PRODUTO_ID = :P2 ');

          if _BibliotecaGenerica.NFormatN(pItens[j].CMV) = '' then
            Continue;

          vExec.Executar([
            pEmpresasIds[i],
            pItens[j].ProdutoId,
            pItens[j].CMV
          ]);
        end;
      end;
    end;
  except
    on e: Exception do
      Result.TratarErro(e);
  end;

  vExec.Free;
end;

function AtualizarPrecos(pConexao: TConexao; pEmpresasIds: TArray<Integer>; pItens: TArray<RecItensFormacaoCusto>; pColunasAlterar: TArray<string>): RecRetornoBD;
var
  i: Integer;
  j: Integer;
  k: Integer;
  vExec: TExecucao;
  vParametros: array of Variant;

  procedure AdicionarParametro(pColuna: string; pValor: Variant; pAdicionarVirgular: Boolean = True);
  begin
    vExec.Add(' ' + _Biblioteca.IIfStr(pAdicionarVirgular and (vParametros <> nil), ',', '') + pColuna +' = :P' + _Biblioteca.NFormat(High(vParametros) + 2));

    SetLength(vParametros, Length(vParametros) + 1);
    vParametros[High(vParametros)] := pValor;
  end;

begin
  Result.TeveErro := False;

  if pColunasAlterar = nil then
    Exit;

  pConexao.SetRotina('ATUALIZAR_PRECOS');
  vExec := TExecucao.Create(pConexao);

  try
    for i := Low(pEmpresasIds) to High(pEmpresasIds) do begin
      for j := Low(pItens) to High(pItens) do begin

        vExec.Clear;
        vParametros := nil;
        vExec.Add('update PRECOS_PRODUTOS set ');

        for k := Low(pColunasAlterar) to High(pColunasAlterar) do begin

          if pColunasAlterar[k] = 'PERC_MARGEM_PRECO_VAREJO' then
            AdicionarParametro('PERC_MARGEM_PRECO_VAREJO', pItens[j].PercMargemPrecoVarejo)
          else if pColunasAlterar[k] = 'PRECO_VAREJO' then
            AdicionarParametro('PRECO_VAREJO', pItens[j].PrecoVarejo)
          else if pColunasAlterar[k] = 'QUANTIDADE_MIN_PRECO_VAREJO' then
            AdicionarParametro('QUANTIDADE_MIN_PRECO_VAREJO', pItens[j].QuantidadeMinPrecoVarejo)
          else if pColunasAlterar[k] = 'PERC_MARGEM_PRECO_PONTA_EST' then
            AdicionarParametro('PERC_MARGEM_PRECO_PONTA_EST', pItens[j].PercMargemPrecoPontaEst)
          else if pColunasAlterar[k] = 'PRECO_PONTA_ESTOQUE' then
            AdicionarParametro('PRECO_PONTA_ESTOQUE', pItens[j].PrecoPontaEstoque)
          else if pColunasAlterar[k] = 'PERC_MARGEM_PRECO_ATACADO_1' then
            AdicionarParametro('PERC_MARGEM_PRECO_ATACADO_1', pItens[j].PercMargemPrecoAtacado1)
          else if pColunasAlterar[k] = 'PRECO_ATACADO_1' then
            AdicionarParametro('PRECO_ATACADO_1', pItens[j].PrecoAtacado1)
          else if pColunasAlterar[k] = 'QUANTIDADE_MIN_PRECO_ATAC_1' then
            AdicionarParametro('QUANTIDADE_MIN_PRECO_ATAC_1', pItens[j].QuantidadeMinPrecoAtac1)
          else if pColunasAlterar[k] = 'PERC_MARGEM_PRECO_ATACADO_2' then
            AdicionarParametro('PERC_MARGEM_PRECO_ATACADO_2', pItens[j].PercMargemPrecoAtacado2)
          else if pColunasAlterar[k] = 'PRECO_ATACADO_2' then
            AdicionarParametro('PRECO_ATACADO_2', pItens[j].PrecoAtacado2)
          else if pColunasAlterar[k] = 'QUANTIDADE_MIN_PRECO_ATAC_2' then
            AdicionarParametro('QUANTIDADE_MIN_PRECO_ATAC_2', pItens[j].QuantidadeMinPrecoAtac2)
          else if pColunasAlterar[k] = 'PERC_MARGEM_PRECO_ATACADO_3' then
            AdicionarParametro('PERC_MARGEM_PRECO_ATACADO_3', pItens[j].PercMargemPrecoAtacado3)
          else if pColunasAlterar[k] = 'PRECO_ATACADO_3' then
            AdicionarParametro('PRECO_ATACADO_3', pItens[j].PrecoAtacado3)
          else if pColunasAlterar[k] = 'QUANTIDADE_MIN_PRECO_ATAC_3' then
            AdicionarParametro('QUANTIDADE_MIN_PRECO_ATAC_3', pItens[j].QuantidadeMinPrecoAtac3)
          else if pColunasAlterar[k] = 'PERC_MARGEM_PRECO_PDV' then
            AdicionarParametro('PERC_MARGEM_PRECO_PDV', pItens[j].PercMargemPrecoPdv)
          else if pColunasAlterar[k] = 'PRECO_PDV' then
            AdicionarParametro('PRECO_PDV', pItens[j].PrecoPdv)
          else if pColunasAlterar[k] = 'QUANTIDADE_MINIMA_PDV' then
            AdicionarParametro('QUANTIDADE_MINIMA_PDV', pItens[j].QuantidadeMinimaPdv)
        end;

        //Filtros where do update
        AdicionarParametro('where EMPRESA_ID', pEmpresasIds[i], False);
        AdicionarParametro('and PRODUTO_ID', pItens[j].ProdutoId, False);

        vExec.Executar(vParametros);
      end;
    end;
  except
    on e: Exception do
      Result.TratarErro(e);
  end;

  vExec.Free;
end;

function BuscarPercentualCustoVenda(pConexao: TConexao; pEmpresaId: Integer; pProdutoId: Integer): Double;
var
  vProc: TProcedimentoBanco;
begin
  vProc := TProcedimentoBanco.Create(pConexao, 'BUSCAR_PERC_CUSTO_VENDA_PROD');

  vProc.ParamByName('iEMPRESA_ID').AsInteger := pEmpresaId;
  vProc.ParamByName('iPRODUTO_ID').AsInteger := pProdutoId;
  vProc.Executar;

  Result := vProc.Params[0].AsFloat;

  vProc.Free;
end;

function BuscarPrecoVarejo(pConexao: TConexao; pEmpresaId: Integer; pFiltro: string): TArray<RecItensFormacaoCusto>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  PRO.PRODUTO_ID, ');
  vSql.Add('  PRO.NOME, ');
  vSql.Add('  BUSCAR_CUSTO_COMPRA_PRODUTO(PRE.EMPRESA_ID, PRO.PRODUTO_ID) CUSTO_COMPRA_COMERCIAL, ');
  vSql.Add('  BUSCAR_PERC_CUSTO_VENDA_PROD(PRE.EMPRESA_ID, PRO.PRODUTO_ID) as PERCENTUAL_CUSTO_VENDA, ');
  vSql.Add('  PRE.PRECO_VAREJO, ');
  vSql.Add('  PRE.PERC_MARGEM_PRECO_VAREJO, ');
  vSql.Add('  PRE.DATA_PRECO_VAREJO ');
  vSql.Add('from ');
  vSql.Add('  PRODUTOS PRO ');

  vSql.Add('inner join PRECOS_PRODUTOS PRE ');
  vSql.Add('on PRO.PRODUTO_ID = PRE.PRODUTO_ID ');
  vSql.Add('and PRE.EMPRESA_ID = :P1 ');

  vSql.Add('where PRO.ATIVO = ''S'' ');
  vSql.Add(PFiltro);

  if vSql.Pesquisar([pEmpresaId]) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].ProdutoId             := vSql.GetInt('PRODUTO_ID');
      Result[i].Nome                  := vSql.GetString('NOME');
      Result[i].CustoCompraComercial  := vSql.GetDouble('CUSTO_COMPRA_COMERCIAL');
      Result[i].PercentualCustoVenda  := vSql.GetDouble('PERCENTUAL_CUSTO_VENDA');
      Result[i].PrecoVarejo           := vSql.GetDouble('PRECO_VAREJO');
      Result[i].PercMargemPrecoVarejo := vSql.GetDouble('PERC_MARGEM_PRECO_VAREJO');
      Result[i].DataPrecoVarejo       := vSql.GetDouble('DATA_PRECO_VAREJO');

      vSql.Next;
    end;
  end;

  vSql.Free;
end;

function AtualizarPrecoVarejo(pConexao: TConexao; pEmpresasIds: TArray<Integer>; pItens: TArray<RecItensFormacaoCusto>): RecRetornoBD;
var
  i: Integer;
  j: Integer;
  vExec: TExecucao;
begin
  Result.TeveErro := False;
  pConexao.SetRotina('ATUALIZAR_PRECOS');
  vExec := TExecucao.Create(pConexao);

  vExec.Add('update PRECOS_PRODUTOS set ');
  vExec.Add('  PRECO_VAREJO = :P3, ');
  vExec.Add('  QUANTIDADE_MIN_PRECO_VAREJO = :P4, ');
  vExec.Add('  PERC_MARGEM_PRECO_VAREJO = :P5 ');
  vExec.Add('where EMPRESA_ID = :P1 ');
  vExec.Add('and PRODUTO_ID = :P2 ');

  try
    for i := Low(pEmpresasIds) to High(pEmpresasIds) do begin
      for j := Low(pItens) to High(pItens) do begin
        vExec.Executar([
          pEmpresasIds[i],
          pItens[j].ProdutoId,
          pItens[j].PrecoVarejo,
          pItens[j].QuantidadeMinPrecoVarejo,
          pItens[j].PercMargemPrecoVarejo
        ]);
      end;
    end;
  except
    on e: Exception do
      Result.TratarErro(e);
  end;

  vExec.Free;
end;

function BuscarPrecoVendaAnterior(pConexao: TConexao; pEmpresaId: Integer; pProdutoId: Integer): Double;
var
  vSql: TConsulta;
begin
  Result := 0.0;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  VALOR_ANTERIOR ');
  vSql.Add('from LOGS_PRECOS_PRODUTOS ');
  vSql.Add('where PRODUTO_ID = :P1 ');
  vSql.Add('and EMPRESA_ID = :P2 ');
  vSql.Add('and TIPO_ALTERACAO_LOG_ID = 2 ');
  vSql.Add('order by DATA_HORA_ALTERACAO desc ');

  if vSql.Pesquisar([pProdutoId, pEmpresaId]) then begin
    vSql.First;
    Result := vSql.GetDouble('VALOR_ANTERIOR');
  end;

  vSql.Free;
end;

end.
