unit _AcumuladosOrcamentos;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecAcumuladosOrcamentos = record
    AcumuladoId: Integer;
    OrcamentoId: Integer;
    DataHoraRecebimento: TDateTime;
    VendedorId: Integer;
    NomeVendedor: string;
    ValorTotal: Double;
    ValorDevAcumuladoAberto: Double;
    ObservacoesNfe: string;
  end;

  TAcumuladosOrcamentos = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordAcumuladosOrcamentos: RecAcumuladosOrcamentos;
  end;

function BuscarAcumuladosOrcamentos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecAcumuladosOrcamentos>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TAcumuladosOrcamentos }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ACO.ACUMULADO_ID = :P1 '
    );
end;

constructor TAcumuladosOrcamentos.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ACUMULADOS_ORCAMENTOS');

  FSql := 
    'select ' +
    '  ACO.ACUMULADO_ID, ' +
    '  ACO.ORCAMENTO_ID, ' +
    '  ORC.DATA_HORA_RECEBIMENTO, ' +
    '  ORC.VENDEDOR_ID, ' +
    '  FVE.NOME as NOME_VENDEDOR, ' +
    '  ORC.VALOR_TOTAL, ' +
    '  ORC.VALOR_DEV_ACUMULADO_ABERTO' +
    'from ' +
    '  ACUMULADOS_ORCAMENTOS ACO ' +

    'inner join ORCAMENTOS ORC ' +
    'on ACO.ORCAMENTO_ID = ORC.ORCAMENTO_ID ' +

    'inner join FUNCIONARIOS FVE ' +
    'on ORC.VENDEDOR_ID = FVE.FUNCIONARIO_ID ';

  setFiltros(getFiltros);

  AddColuna('ACUMULADO_ID', True);
  AddColuna('ORCAMENTO_ID', True);
  AddColunaSL('DATA_HORA_RECEBIMENTO');
  AddColunaSL('VENDEDOR_ID');
  AddColunaSL('NOME_VENDEDOR');
  AddColunaSL('VALOR_TOTAL');
  AddColunaSL('VALOR_DEV_ACUMULADO_ABERTO');
end;

function TAcumuladosOrcamentos.getRecordAcumuladosOrcamentos: RecAcumuladosOrcamentos;
begin
  Result.AcumuladoId             := getInt('ACUMULADO_ID', True);
  Result.OrcamentoId             := getInt('ORCAMENTO_ID', True);
  Result.DataHoraRecebimento     := getData('DATA_HORA_RECEBIMENTO');
  Result.VendedorId              := getInt('VENDEDOR_ID');
  Result.NomeVendedor            := getString('NOME_VENDEDOR');
  Result.ValorTotal              := getDouble('VALOR_TOTAL');
  Result.ValorDevAcumuladoAberto := getDouble('VALOR_DEV_ACUMULADO_ABERTO');
end;

function BuscarAcumuladosOrcamentos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecAcumuladosOrcamentos>;
var
  i: Integer;
  t: TAcumuladosOrcamentos;
begin
  Result := nil;
  t := TAcumuladosOrcamentos.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordAcumuladosOrcamentos;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
