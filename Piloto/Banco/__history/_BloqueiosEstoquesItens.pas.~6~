unit _BloqueiosEstoquesItens;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecBloqueiosEstoquesItens = record
    BloqueioId: Integer;
    ItemId: Integer;
    ProdutoId: Integer;
    NomeProduto: string;
    Lote: string;
    LocalId: Integer;
    NomeLocal: string;
    NomeMarca: string;
    Quantidade: Double;
    Baixados: Double;
    Cancelados: Double;
    Saldo: Double;
    MultiploVenda: Double;
    UnidadeVenda: string;
  end;

  TBloqueiosEstoquesItens = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordBloqueiosEstoquesItens: RecBloqueiosEstoquesItens;
  end;

function BuscarBloqueiosEstoquesItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecBloqueiosEstoquesItens>;

function BuscarBloqueiosEstoquesItensComando(pConexao: TConexao; pComando: string): TArray<RecBloqueiosEstoquesItens>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TBloqueiosEstoquesItens }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where BLOQUEIO_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where BEI.BLOQUEIO_ID = :P1 ' +
      'and BEI.SALDO > 0',
      'order by ' +
      '  PRO.NOME '
    );
end;

constructor TBloqueiosEstoquesItens.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'BLOQUEIOS_ESTOQUES_ITENS');

  FSql := 
    'select ' +
    '  BEI.BLOQUEIO_ID, ' +
    '  BEI.ITEM_ID, ' +
    '  BEI.PRODUTO_ID, ' +
    '  PRO.NOME as NOME_PRODUTO, ' +
    '  BEI.LOTE, ' +
    '  BEI.LOCAL_ID, ' +
    '  BEI.QUANTIDADE, ' +
    '  BEI.BAIXADOS, ' +
    '  BEI.CANCELADOS, ' +
    '  BEI.SALDO, ' +
    '  MAR.NOME as NOME_MARCA, ' +
    '  LOC.NOME as NOME_LOCAL, ' +
    '  PRO.UNIDADE_VENDA, ' +
    '  PRO.MULTIPLO_VENDA, ' +
    '  CPR.PRECO_LIQUIDO AS CUSTO_PRODUTO, ' +
    '  BEI.QUANTIDADE * CPR.PRECO_LIQUIDO AS CUSTO_PRODUTO_TOTAL ' +
    'from ' +
    '  BLOQUEIOS_ESTOQUES_ITENS BEI ' +

    'inner join PRODUTOS PRO ' +
    'on BEI.PRODUTO_ID = PRO.PRODUTO_ID ' +

    'inner join MARCAS MAR ' +
    'on PRO.MARCA_ID = MAR.MARCA_ID ' +

    'inner join BLOQUEIOS_ESTOQUES BLO ' +
    'on BLO.BLOQUEIO_ID = BEI.BLOQUEIO_ID ' +

    'inner join CUSTOS_PRODUTOS CPR ' +
    'on CPR.PRODUTO_ID = BEI.PRODUTO_ID ' +
    'and BLO.EMPRESA_ID = CPR.EMPRESA_ID ' +

    'inner join LOCAIS_PRODUTOS LOC ' +
    'on BEI.LOCAL_ID = LOC.LOCAL_ID ';

  setFiltros(getFiltros);

  AddColuna('BLOQUEIO_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('PRODUTO_ID');
  AddColunaSL('NOME_PRODUTO');
  AddColuna('LOTE');
  AddColuna('LOCAL_ID');
  AddColuna('QUANTIDADE');
  AddColunaSL('BAIXADOS');
  AddColunaSL('CANCELADOS');
  AddColunaSL('SALDO');
  AddColunaSL('NOME_MARCA');
  AddColunaSL('NOME_LOCAL');
  AddColunaSL('UNIDADE_VENDA');
  AddColunaSL('MULTIPLO_VENDA');
end;

function TBloqueiosEstoquesItens.getRecordBloqueiosEstoquesItens: RecBloqueiosEstoquesItens;
begin
  Result.BloqueioId         := getInt('BLOQUEIO_ID', True);
  Result.ItemId             := getInt('ITEM_ID', True);
  Result.ProdutoId          := getInt('PRODUTO_ID');
  Result.NomeProduto        := getString('NOME_PRODUTO');
  Result.Lote               := getString('LOTE');
  Result.LocalId            := getInt('LOCAL_ID');
  Result.NomeLocal          := getString('NOME_LOCAL');
  Result.NomeMarca          := getString('NOME_MARCA');
  Result.Quantidade         := getDouble('QUANTIDADE');
  Result.Baixados           := getDouble('BAIXADOS');
  Result.Cancelados         := getDouble('CANCELADOS');
  Result.Saldo              := getDouble('SALDO');
  Result.MultiploVenda      := getDouble('MULTIPLO_VENDA');
  Result.UnidadeVenda       := getString('UNIDADE_VENDA');
end;

function BuscarBloqueiosEstoquesItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecBloqueiosEstoquesItens>;
var
  i: Integer;
  t: TBloqueiosEstoquesItens;
begin
  Result := nil;
  t := TBloqueiosEstoquesItens.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordBloqueiosEstoquesItens;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarBloqueiosEstoquesItensComando(pConexao: TConexao; pComando: string): TArray<RecBloqueiosEstoquesItens>;
var
  i: Integer;
  t: TBloqueiosEstoquesItens;
begin
  Result := nil;
  t := TBloqueiosEstoquesItens.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordBloqueiosEstoquesItens;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
