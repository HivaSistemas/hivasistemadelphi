unit _BloqueiosEstoques;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _BloqueiosEstoquesItens;

{$M+}
type
  RecBloqueiosEstoques = record
    BloqueioId: Integer;
    EmpresaId: Integer;
    NomeEmpresa: string;
    UsuarioCadastroId: Integer;
    NomeUsuarioCadastro: string;
    DataHoraCadastro: TDateTime;
    Status: string;
    StatusAnalitico: string;
    UsuarioBaixaId: Integer;
    NomeUsuarioBaixa: string;
    DataHoraBaixa: TDateTime;
    Observacoes: string;
  end;

  TBloqueiosEstoques = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordBloqueiosEstoques: RecBloqueiosEstoques;
  end;

function AtualizarBloqueiosEstoques(
  pConexao: TConexao;
  pBloqueioId: Integer;
  pEmpresaId: Integer;
  pStatus: string;
  pObservacoes: string;
  pItens: TArray<RecBloqueiosEstoquesItens>
): RecRetornoBD;

function BuscarBloqueiosEstoques(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecBloqueiosEstoques>;

function BuscarBloqueiosEstoquesComando(pConexao: TConexao; pComando: string): TArray<RecBloqueiosEstoques>;

function ExcluirBloqueiosEstoques(
  pConexao: TConexao;
  pBloqueioId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TBloqueiosEstoques }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where BLO.BLOQUEIO_ID = :P1'
    );
end;

constructor TBloqueiosEstoques.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'BLOQUEIOS_ESTOQUES');

  FSql := 
    'select distinct' +
    '  BLO.BLOQUEIO_ID, ' +
    '  BLO.EMPRESA_ID, ' +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA, ' +
    '  BLO.USUARIO_CADASTRO_ID, ' +
    '  FUN.NOME as NOME_USUARIO_CADASTRO, ' +
    '  BLO.DATA_HORA_CADASTRO, ' +
    '  BLO.STATUS, ' +
    '  BLO.USUARIO_BAIXA_ID, ' +
    '  FBA.NOME as NOME_USUARIO_BAIXA, ' +
    '  BLO.DATA_HORA_BAIXA, ' +
    '  BLO.OBSERVACOES ' +
    'from ' +
    '  BLOQUEIOS_ESTOQUES BLO ' +

    'inner join BLOQUEIOS_ESTOQUES_ITENS BTI ' +
    'on BTI.BLOQUEIO_ID = BLO.BLOQUEIO_ID ' +

    'inner join EMPRESAS EMP ' +
    'on BLO.EMPRESA_ID = EMP.EMPRESA_ID ' +

    'inner join FUNCIONARIOS FUN ' +
    'on BLO.USUARIO_CADASTRO_ID = FUN.FUNCIONARIO_ID ' +

    'left join FUNCIONARIOS FBA ' +
    'on BLO.USUARIO_BAIXA_ID = FBA.FUNCIONARIO_ID ';

  setFiltros(getFiltros);

  AddColuna('BLOQUEIO_ID', True);
  AddColuna('EMPRESA_ID');
  AddColunaSL('NOME_EMPRESA');
  AddColunaSL('USUARIO_CADASTRO_ID');
  AddColunaSL('NOME_USUARIO_CADASTRO');
  AddColunaSL('DATA_HORA_CADASTRO');
  AddColuna('STATUS');
  AddColunaSL('USUARIO_BAIXA_ID');
  AddColunaSL('NOME_USUARIO_BAIXA');
  AddColunaSL('DATA_HORA_BAIXA');
  AddColuna('OBSERVACOES');
end;

function TBloqueiosEstoques.getRecordBloqueiosEstoques: RecBloqueiosEstoques;
begin
  Result.BloqueioId                 := getInt('BLOQUEIO_ID', True);
  Result.EmpresaId                  := getInt('EMPRESA_ID');
  Result.NomeEmpresa                := getString('NOME_EMPRESA');
  Result.UsuarioCadastroId          := getInt('USUARIO_CADASTRO_ID');
  Result.NomeUsuarioCadastro        := getString('NOME_USUARIO_CADASTRO');
  Result.DataHoraCadastro           := getData('DATA_HORA_CADASTRO');
  Result.Status                     := getString('STATUS');
  Result.UsuarioBaixaId             := getInt('USUARIO_BAIXA_ID');
  Result.NomeUsuarioBaixa           := getString('NOME_USUARIO_BAIXA');
  Result.DataHoraBaixa              := getData('DATA_HORA_BAIXA');
  Result.Observacoes                := getString('OBSERVACOES');

  Result.StatusAnalitico := IIfStr(Result.Status = 'A', 'Aberto', 'Baixado');
end;

function AtualizarBloqueiosEstoques(
  pConexao: TConexao;
  pBloqueioId: Integer;
  pEmpresaId: Integer;
  pStatus: string;
  pObservacoes: string;
  pItens: TArray<RecBloqueiosEstoquesItens>
): RecRetornoBD;
var
  t: TBloqueiosEstoques;
  vItens: TBloqueiosEstoquesItens;

  i: Integer;
  vNovo: Boolean;
  vSeq: TSequencia;
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.setRotina('ATUALIZAR_BLOQUEIO_ESTOQUE');

  vExec := TExecucao.Create(pConexao);
  t := TBloqueiosEstoques.Create(pConexao);
  vItens := TBloqueiosEstoquesItens.Create(pConexao);

  vNovo := pBloqueioId = 0;
  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_BLOQUEIOS_ESTOQUES');
    pBloqueioId := vSeq.getProximaSequencia;
    Result.AsInt := pBloqueioId;
    vSeq.Free;
  end;

  try
    pConexao.IniciarTransacao; 

    t.setInt('BLOQUEIO_ID', pBloqueioId, True);
    t.setInt('EMPRESA_ID', pEmpresaId);
    t.setString('STATUS', pStatus);
    t.setString('OBSERVACOES', pObservacoes);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    if vNovo then begin
      vExec.Add('delete from BLOQUEIOS_ESTOQUES_ITENS');
      vExec.Add('where BLOQUEIO_ID = :P1');
      vExec.Executar([pBloqueioId]);
    end;

    for i := Low(pItens) to High(pItens) do begin
      vItens.setInt('BLOQUEIO_ID', pBloqueioId, True);
      vItens.setInt('ITEM_ID', i + 1, True);
      vItens.setInt('PRODUTO_ID', pItens[i].ProdutoId);
      vItens.setString('LOTE', pItens[i].Lote);
      vItens.setInt('LOCAL_ID', pItens[i].LocalId);
      vItens.setDouble('QUANTIDADE', pItens[i].Quantidade);

      vItens.Inserir;
    end;

    pConexao.FinalizarTransacao; 
  except
    on e: Exception do begin
      pConexao.VoltarTransacao; 
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
  vItens.Free;
  t.Free;
end;

function BuscarBloqueiosEstoques(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecBloqueiosEstoques>;
var
  i: Integer;
  t: TBloqueiosEstoques;
begin
  Result := nil;
  t := TBloqueiosEstoques.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordBloqueiosEstoques;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarBloqueiosEstoquesComando(pConexao: TConexao; pComando: string): TArray<RecBloqueiosEstoques>;
var
  i: Integer;
  t: TBloqueiosEstoques;
begin
  Result := nil;
  t := TBloqueiosEstoques.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordBloqueiosEstoques;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirBloqueiosEstoques(
  pConexao: TConexao;
  pBloqueioId: Integer
): RecRetornoBD;
var
  t: TBloqueiosEstoques;
  vExec: TExecucao;
begin
  Result.Iniciar;

  vExec := TExecucao.Create(pConexao);
  t := TBloqueiosEstoques.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    vExec.Add('delete from BLOQUEIOS_ESTOQUES_ITENS ');
    vExec.Add('where BLOQUEIO_ID = :P1');
    vExec.Executar([pBloqueioId]);

    t.setInt('BLOQUEIO_ID', pBloqueioId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
  t.Free;
end;

end.
