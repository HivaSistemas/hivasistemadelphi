unit _RelacaoContasReceber;

interface

uses
  System.SysUtils, _Conexao, _OperacoesBancoDados, _RecordsRelatorios, _Biblioteca;


function BuscarContasReceberComando(pConexao: TConexao; pComando: string): TArray<RecContasReceber>;

implementation

function BuscarContasReceberComando(pConexao: TConexao; pComando: string): TArray<RecContasReceber>;
var
  i: Word;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  with vSql.SQL do begin
    Add('select ');
    Add('  CON.RECEBER_ID, ');
    Add('  CON.DOCUMENTO, ');
    Add('  CON.CADASTRO_ID, ');
    Add('  CAD.RAZAO_SOCIAL as NOME_CLIENTE, ');
    Add('  CON.NUMERO_CHEQUE, ');
    Add('  CON.AGENCIA, ');
    Add('  CON.BANCO, ');
    Add('  CON.NOTA_FISCAL_ID, ');
    Add('  CON.VENDEDOR_ID, ');
    Add('  FUN.APELIDO as NOME_VENDEDOR, ');
    Add('  CON.TURNO_ID, ');
    Add('  CON.COBRANCA_ID, ');
    Add('  TCO.NOME as NOME_TIPO_COBRANCA, ');
    Add('  TCO.FORMA_PAGAMENTO, ');
    Add('  TCO.BOLETO_BANCARIO, ');
    Add('  CON.ORCAMENTO_ID, ');
    Add('  CON.EMPRESA_ID, ');
    Add('  EMP.NOME_FANTASIA as NOME_EMPRESA, ');
    Add('  CON.VALOR_RETENCAO, ');
    Add('  CON.VALOR_DOCUMENTO, ');
    Add('  CON.VALOR_ADIANTADO, ');
    Add('  CON.NOME_EMITENTE, ');
    Add('  CON.TELEFONE_EMITENTE, ');
    Add('  CON.NOSSO_NUMERO, ');
    Add('  CON.STATUS, ');
    Add('  BAI.DATA_PAGAMENTO, ');
    Add('  CON.DATA_VENCIMENTO_ORIGINAL, ');
    Add('  CON.DATA_VENCIMENTO, ');
    Add('  CON.DATA_EMISSAO, ');
    Add('  CON.DATA_CADASTRO, ');
    Add('  CON.ORIGEM, ');

    Add('  case when con.status = ''A'' then ');
    Add('    case when trunc(sysdate) - CON.DATA_VENCIMENTO > 0 then trunc(sysdate) - CON.DATA_VENCIMENTO else 0 end ');
    Add('  else ');
    Add('    case when trunc(BAI.DATA_PAGAMENTO) - CON.DATA_VENCIMENTO > 0 then trunc(BAI.DATA_PAGAMENTO) - CON.DATA_VENCIMENTO else 0 end ');
    Add('  end as DIAS_ATRASO, ');

    Add('  CON.CODIGO_BARRAS, ');
    Add('  CON.CONTA_CORRENTE, ');
    Add('  CON.NSU, ');
    Add('  PARCELA, ');
    Add('  NUMERO_PARCELAS, ');
    Add('  case when CON.TURNO_ID is not null then ''S'' else ''N'' end as NO_CAIXA, ');
    Add('  CON.PORTADOR_ID, ');
    Add('  POR.DESCRICAO as NOME_PORTADOR ');

    Add('from ');
    Add('  CONTAS_RECEBER CON ');

    Add('inner join CADASTROS CAD ');
    Add('on CON.CADASTRO_ID = CAD.CADASTRO_ID ');

    Add('left join FUNCIONARIOS FUN ');
    Add('on CON.VENDEDOR_ID = FUN.FUNCIONARIO_ID ');

    Add('inner join EMPRESAS EMP ');
    Add('on CON.EMPRESA_ID = EMP.EMPRESA_ID ');

    Add('inner join TIPOS_COBRANCA TCO ');
    Add('on CON.COBRANCA_ID = TCO.COBRANCA_ID ');

    Add('inner join PORTADORES POR ');
    Add('on CON.PORTADOR_ID = POR.PORTADOR_ID ');

    (* ---- Este join tr�s informa��es sobre a baixa que originou o t�tulo ---- *)
    Add('left join CONTAS_RECEBER_BAIXAS BAI ');
    Add('on CON.BAIXA_ORIGEM_ID = BAI.BAIXA_ID ');

    Add('left join CONTAS_RECEBER_BAIXAS_ITENS BXI ');
    Add('on CON.RECEBER_ID = BXI.RECEBER_ID ');
    Add('and BAI.BAIXA_ID = BXI.RECEBER_ID ');
    (* ---- --------------------------------------------------------------- ---- *)

    (* ---- Este join tr�s informa��es sobre a baixa que t�tulo pertence ------- *)
    Add('left join CONTAS_RECEBER_BAIXAS_ITENS BXA ');
    Add('on CON.RECEBER_ID = BXA.RECEBER_ID ');

    Add('left join CONTAS_RECEBER_BAIXAS BAX ');
    Add('on BXA.BAIXA_ID = BAX.BAIXA_ID ');
    (* ---- --------------------------------------------------------------- ---- *)

    Add('left join TURNOS_CAIXA TUR ');
    Add('on CON.TURNO_ID = TUR.TURNO_ID ');

    Add('left join FUNCIONARIOS FTU ');
    Add('on TUR.FUNCIONARIO_ID = FTU.FUNCIONARIO_ID ');

    Add('left join CLIENTES CLI ');
    Add('on CON.CADASTRO_ID = CLI.CADASTRO_ID ');

    Add(pComando);
  end;

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].receber_id               := vSql.GetInt('RECEBER_ID');
      Result[i].documento                := vSql.GetString('DOCUMENTO');
      Result[i].cliente_id               := vSql.GetInt('CADASTRO_ID');
      Result[i].nome_cliente             := vSql.GetString('NOME_CLIENTE');
      Result[i].numero_cheque            := vSql.GetInt('NUMERO_CHEQUE');
      Result[i].agencia                  := vSql.GetString('AGENCIA');
      Result[i].banco                    := vSql.GetString('BANCO');
      Result[i].nota_fiscal_id           := vSql.GetInt('NOTA_FISCAL_ID');
      Result[i].vendedor_id              := vSql.GetInt('VENDEDOR_ID');
      Result[i].nome_vendedor            := vSql.GetString('NOME_VENDEDOR');
      Result[i].turno_id                 := vSql.GetInt('TURNO_ID');
      Result[i].cobranca_id              := vSql.GetInt('COBRANCA_ID');
      Result[i].nome_tipo_cobranca       := vSql.GetString('NOME_TIPO_COBRANCA');
      Result[i].forma_pagamento          := vSql.GetString('FORMA_PAGAMENTO');
      Result[i].BoletoBancario           := vSql.GetString('BOLETO_BANCARIO');
      Result[i].orcamento_id             := vSql.GetInt('ORCAMENTO_ID');
      Result[i].empresa_id               := vSql.GetInt('EMPRESA_ID');
      Result[i].dias_atraso              := vSql.GetInt('DIAS_ATRASO');
      Result[i].nome_empresa             := vSql.GetString('NOME_EMPRESA');
      Result[i].ValorRetencao            := vSql.GetDouble('VALOR_RETENCAO');
      Result[i].valor_documento          := vSql.GetDouble('VALOR_DOCUMENTO');
      Result[i].ValorAdiantado           := vSql.GetDouble('VALOR_ADIANTADO');
      Result[i].nome_emitente            := vSql.GetString('NOME_EMITENTE');
      Result[i].telefone_emitente        := vSql.GetString('TELEFONE_EMITENTE');
      Result[i].nosso_numero             := vSql.GetString('NOSSO_NUMERO');
      Result[i].status                   := vSql.GetString('STATUS');
      Result[i].documento                := vSql.GetString('DOCUMENTO');
      Result[i].data_pagamento           := vSql.GetData('DATA_PAGAMENTO');
      Result[i].data_vencimento_original := vSql.GetData('DATA_VENCIMENTO_ORIGINAL');
      Result[i].data_vencimento          := vSql.GetData('DATA_VENCIMENTO');
      Result[i].data_emissao             := vSql.GetData('DATA_EMISSAO');
      Result[i].data_cadastro            := vSql.GetData('DATA_CADASTRO');
      Result[i].Origem                   := vSql.GetString('ORIGEM');
      Result[i].codigo_barras            := vSql.GetString('CODIGO_BARRAS');
      Result[i].conta_corrente           := vSql.GetString('CONTA_CORRENTE');
      Result[i].nsu                      := vSql.GetString('NSU');
      Result[i].parcela                  := vSql.GetInt('PARCELA');
      Result[i].numero_parcelas          := vSql.GetInt('NUMERO_PARCELAS');
      Result[i].NoCaixa                  := vSql.getString('NO_CAIXA');
      Result[i].PortadorId               := vSql.getString('PORTADOR_ID');
      Result[i].NomePortador             := vSql.getString('NOME_PORTADOR');

      // Ser�o calculados na tela de contas a receber
      Result[i].ValorMulta               := 0;
      Result[i].ValorJuros               := 0;

      vSql.Next;
    end;
  end;
  vSql.Active := False;
  FreeAndNil(vSql);
end;

end.
