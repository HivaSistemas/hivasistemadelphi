unit _MovimentosTurnos;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCaixa,
  System.Variants, System.StrUtils;

{$M+}
type
  RecMovimentosTurnos = record
    TurnoId: Integer;
    Id: Integer;
    ContaId: string;
    NomeContaOrigem: string;
    UsuarioMovimentoId: Integer;
    ValorDinheiro: Double;
    ValorCheque: Double;
    ValorCredito: Double;
    ValorPix: Double;
    ValorCartao: Double;
    ValorCobranca: Double;
    ValorAcumulado: Double;
    DataHoraMovimento: TDateTime;
    TipoMovimento: string;
    TipoMovimentoAnalitico: string;
    Funcionario: string;
    Observacao: string;
  end;

  TMovimentosTurnos = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordMovimentoBancosCaixas: RecMovimentosTurnos;
  end;

function AtualizarMovimentosTurno(
  pConexao: TConexao;
  pMovimentoId: Integer;
  pTurnoId: Integer;
  pContaId: string;
  pValorDinheiro: Double;
  pValorCheque: Double;
  pValorCredito: Double;
  pValorCartao: Double;
  pValorCobranca: Double;
  pTipoMovimento: string;
  pObservacao: string;
  pValorPix: Double
): RecRetornoBD;

function BuscarMovimentosTurnos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecMovimentosTurnos>;

function BuscarMovimentosTurnosComando(pConexao: TConexao; pComando: string): TArray<RecMovimentosTurnos>;
function ExisteMovimentoFechamentoTurno(pConexao: TConexao; const pTurnoId: Integer): Boolean;

function getFiltros: TArray<RecFiltros>;

implementation

{ TMovimentosTurnos }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where MOV.TURNO_ID = :P1 ' +
      'order by ' +
      '  MOV.DATA_HORA_MOVIMENTO '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where MOV.MOVIMENTO_TURNO_ID = :P1 '
    );
end;

constructor TMovimentosTurnos.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'MOVIMENTOS_TURNOS');

  FSql :=
    'select ' +
    '  MOV.MOVIMENTO_TURNO_ID, ' +
    '  MOV.TURNO_ID, ' +
    '  MOV.ID, ' +
    '  MOV.CONTA_ID, ' +
    '  CON.NOME as NOME_CONTA_ORIGEM, ' +
    '  MOV.USUARIO_MOVIMENTO_ID, ' +
    '  MOV.VALOR_DINHEIRO, ' +
    '  MOV.VALOR_PIX, ' +
    '  MOV.VALOR_CHEQUE, ' +
    '  MOV.VALOR_CREDITO, ' +
    '  MOV.VALOR_CARTAO, ' +
    '  MOV.VALOR_COBRANCA, ' +
    '  MOV.VALOR_ACUMULADO, ' +
    '  MOV.DATA_HORA_MOVIMENTO, ' +
    '  MOV.TIPO_MOVIMENTO, ' +
    '  nvl(FUN.APELIDO, FUN.NOME) as FUNCIONARIO, ' +
    '  MOV.OBSERVACAO ' +
    'from ' +
    '  VW_MOVIMENTOS_TURNOS MOV ' +

    'left join CONTAS CON ' +
    'on MOV.CONTA_ID = CON.CONTA ' +

    'inner join FUNCIONARIOS FUN ' +
    'on MOV.USUARIO_MOVIMENTO_ID = FUN.FUNCIONARIO_ID ';

  setFiltros(getFiltros);

  AddColuna('MOVIMENTO_TURNO_ID', True);
  AddColuna('TURNO_ID');
  AddColuna('CONTA_ID');
  AddColuna('TIPO_MOVIMENTO');
  AddColuna('VALOR_DINHEIRO');
  AddColuna('VALOR_PIX');
  AddColuna('VALOR_CHEQUE');
  AddColuna('VALOR_CARTAO');
  AddColuna('VALOR_COBRANCA');
  AddColuna('OBSERVACAO');

  AddColunaSL('VALOR_ACUMULADO');
  AddColunaSL('DATA_HORA_MOVIMENTO');
  AddColunaSL('ID');
  AddColunaSL('NOME_CONTA_ORIGEM');
  AddColunaSL('USUARIO_MOVIMENTO_ID');
  AddColunaSL('VALOR_CREDITO');
  AddColunaSL('FUNCIONARIO');
end;

function TMovimentosTurnos.getRecordMovimentoBancosCaixas: RecMovimentosTurnos;
begin
  Result.TurnoId            := getInt('TURNO_ID');
  Result.Id                 := getInt('ID');
  Result.ContaId            := getString('CONTA_ID');
  Result.NomeContaOrigem    := getString('NOME_CONTA_ORIGEM');
  Result.UsuarioMovimentoId := getInt('USUARIO_MOVIMENTO_ID');
  Result.ValorDinheiro      := getDouble('VALOR_DINHEIRO');
  Result.ValorPix           := getDouble('VALOR_PIX');
  Result.ValorCheque        := getDouble('VALOR_CHEQUE');
  Result.ValorCredito       := getDouble('VALOR_CREDITO');
  Result.ValorCartao        := getDouble('VALOR_CARTAO');
  Result.ValorCobranca      := getDouble('VALOR_COBRANCA');
  Result.ValorAcumulado     := getDouble('VALOR_ACUMULADO');
  Result.DataHoraMovimento  := getData('DATA_HORA_MOVIMENTO');
  Result.Observacao         := getString('OBSERVACAO');

  Result.TipoMovimento      := getString('TIPO_MOVIMENTO');
  Result.TipoMovimentoAnalitico :=
    _Biblioteca.Decode(
      Result.TipoMovimento,[
        'ABE', 'Abertura',
        'SUP', 'Suprimento',
        'SAN', 'Sangria',
        'FEC', 'Fechamento',
        'REV', 'Recebimento de venda',
        'ACU', 'Recebimento de acumulados',
        'BXR', 'Baixa contas receber',
        'BXP', 'Baixa contas pagar'
      ]
    );

  Result.Funcionario        := getString('FUNCIONARIO');
end;

function AtualizarMovimentosTurno(
  pConexao: TConexao;
  pMovimentoId: Integer;
  pTurnoId: Integer;
  pContaId: string;
  pValorDinheiro: Double;
  pValorCheque: Double;
  pValorCredito: Double;
  pValorCartao: Double;
  pValorCobranca: Double;
  pTipoMovimento: string;
  pObservacao: string;
  pValorPix: Double
): RecRetornoBD;
var
  novo: Boolean;
  seq: TSequencia;
  t: TMovimentosTurnos;
begin
  Result.TeveErro := False;
  t := TMovimentosTurnos.Create(pConexao);
  pConexao.SetRotina('ATU_MOV_TURNOS');

  novo := pMovimentoId = 0;
  if novo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_MOVIMENTO_TURNO_ID');
    pMovimentoId := seq.getProximaSequencia;
    Result.AsInt := pMovimentoId;
    seq.Free;
  end;

  t.setInt('MOVIMENTO_TURNO_ID', pMovimentoId, True);
  t.setIntN('TURNO_ID', pTurnoId);
  t.setStringN('CONTA_ID', pContaId);
  t.setDouble('VALOR_DINHEIRO', pValorDinheiro);
  t.setDouble('VALOR_CHEQUE', pValorCheque);
  t.setDouble('VALOR_CARTAO', pValorCartao);
  t.setDouble('VALOR_COBRANCA', pValorCobranca);
  t.setString('TIPO_MOVIMENTO', pTipoMovimento);
  t.setStringN('OBSERVACAO', pObservacao);

  try
    if novo then
      t.Inserir
    else
      t.Atualizar;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function BuscarMovimentosTurnos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecMovimentosTurnos>;
var
  i: Integer;
  t: TMovimentosTurnos;
begin
  Result := nil;
  t := TMovimentosTurnos.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordMovimentoBancosCaixas;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarMovimentosTurnosComando(pConexao: TConexao; pComando: string): TArray<RecMovimentosTurnos>;
var
  i: Integer;
  t: TMovimentosTurnos;
begin
  Result := nil;
  t := TMovimentosTurnos.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordMovimentoBancosCaixas;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExisteMovimentoFechamentoTurno(pConexao: TConexao; const pTurnoId: Integer): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select count(*) ');
  vSql.SQL.Add('from MOVIMENTOS_TURNOS ');
  vSql.SQL.Add('where TIPO_MOVIMENTO = ''FEC'' ');
  vSql.SQL.Add('and TURNO_ID = :P1 ');
  vSql.Pesquisar([pTurnoId]);

  Result := vSql.GetInt(0) > 0;

  vSql.Active := False;
  vSql.Free;
end;


end.
