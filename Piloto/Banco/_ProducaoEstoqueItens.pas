unit _ProducaoEstoqueItens;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsEstoques, System.StrUtils;

{$M+}
type
  TProducaoEstoqueItens = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordProducaoEstoqueItens: RecProducaoEstoqueItens;
  end;

function BuscarProducaoEstoqueItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecProducaoEstoqueItens>;

function BuscarProducaoEstoqueItensComando(pConexao: TConexao; pComando: string): TArray<RecProducaoEstoqueItens>;

function ExcluirProducaoEstoqueItens(
  pConexao: TConexao;
  pProducaoEstoqueId: Integer;
  pItemId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TProducaoEstoqueItens }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where PRODUCAO_ESTOQUE_ID = :P1 ' +
      'order by ' +
      '  PRODUTO_ID '
    );
end;

constructor TProducaoEstoqueItens.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PRODUCAO_ESTOQUE_ITENS');

  FSql :=
    'select ' +
    '  ITE.PRODUCAO_ESTOQUE_ID, ' +
    '  ITE.LOCAL_ID, ' +
    '  LOC.NOME as NOME_LOCAL, ' +
    '  ITE.PRODUTO_ID, ' +
    '  PRO.NOME as NOME_PRODUTO, ' +
    '  ITE.QUANTIDADE, ' +
    '  ITE.PRECO_UNITARIO, ' +
    '  MAR.MARCA_ID, ' +
    '  MAR.NOME AS NOME_MARCA,' +
    '  ITE.NATUREZA ' +
    'from ' +
    '  PRODUCAO_ESTOQUE_ITENS ITE ' +

    'inner join PRODUTOS PRO ' +
    'on ITE.PRODUTO_ID = PRO.PRODUTO_ID ' +

    'inner join MARCAS MAR ' +
    'on PRO.MARCA_ID = MAR.MARCA_ID ' +

    'inner join LOCAIS_PRODUTOS LOC ' +
    'on ITE.LOCAL_ID = LOC.LOCAL_ID ';

  setFiltros(getFiltros);

  AddColuna('PRODUCAO_ESTOQUE_ID', True);
  AddColuna('LOCAL_ID', True);
  AddColuna('PRODUTO_ID');
  AddColunaSL('NOME_PRODUTO');
  AddColuna('QUANTIDADE');
  AddColuna('PRECO_UNITARIO');
  AddColuna('NATUREZA');
  AddColunaSL('NOME_LOCAL');
  AddColunaSL('MARCA_ID');
  AddColunaSL('NOME_MARCA');
end;

function TProducaoEstoqueItens.getRecordProducaoEstoqueItens: RecProducaoEstoqueItens;
begin
  Result.Producao_estoque_id := getInt('PRODUCAO_ESTOQUE_ID', True);
  Result.LocalId           := getInt('LOCAL_ID', True);
  Result.produto_id        := getInt('PRODUTO_ID');
  Result.NomeProduto       := getString('NOME_PRODUTO');
  Result.quantidade        := getDouble('QUANTIDADE');
  Result.PrecoUnitario     := getDouble('PRECO_UNITARIO');
  Result.NomeLocal         := getString('NOME_LOCAL');
  Result.MarcaId           := getInt('MARCA_ID');
  Result.NomeMarca         := getString('NOME_MARCA');

  Result.Natureza          := getString('NATUREZA');
  Result.NaturezaAnalitico := IfThen(Result.Natureza = 'E', 'Entrada', 'Sa�da');
end;

function BuscarProducaoEstoqueItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecProducaoEstoqueItens>;
var
  i: Integer;
  t: TProducaoEstoqueItens;
begin
  Result := nil;
  t := TProducaoEstoqueItens.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordProducaoEstoqueItens;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarProducaoEstoqueItensComando(pConexao: TConexao; pComando: string): TArray<RecProducaoEstoqueItens>;
var
  i: Integer;
  t: TProducaoEstoqueItens;
begin
  Result := nil;
  t := TProducaoEstoqueItens.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordProducaoEstoqueItens;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirProducaoEstoqueItens(
  pConexao: TConexao;
  pProducaoEstoqueId: Integer;
  pItemId: Integer
): RecRetornoBD;
var
  t: TProducaoEstoqueItens;
begin
  Result.TeveErro := False;
  t := TProducaoEstoqueItens.Create(pConexao);

  try
    t.setInt('PRODUCAO_ESTOQUE_ID', pProducaoEstoqueId, True);
    t.setInt('ITEM_ID', pItemId, True);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
