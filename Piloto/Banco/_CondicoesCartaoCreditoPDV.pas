unit _CondicoesCartaoCreditoPDV;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecCondicoesCartaoCreditoPDV = record
    EmpresaId: Integer;
    CondicaoId: Integer;
    NomeCondicao: string;
    Ordem: Integer;
    QuantidadeParcelas: Integer;
    IndiceAcrescimo: Double;
    AplicarIndPrecoPromocional: string;
    PermitirPrecoPromocional: string;
  end;

  TCondicoesCartaoCreditoPDV = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordCondicoesCartaoCreditoPDV: RecCondicoesCartaoCreditoPDV;
  end;

function AtualizarCondicoesCartaoCreditoPDV(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pCondicoes: TArray<RecCondicoesCartaoCreditoPDV>
): RecRetornoBD;

function BuscarCondicoesCartaoCreditoPDV(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCondicoesCartaoCreditoPDV>;

function ExcluirCondicoesCartaoCreditoPDV(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pCondicaoId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TCondicoesCartaoCreditoPDV }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CCR.EMPRESA_ID = :P1 ',
      'order by ' +
      '  CCR.ORDEM '
    );
end;

constructor TCondicoesCartaoCreditoPDV.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONDICOES_CARTAO_CREDITO_PDV');

  FSql :=
    'select ' +
    '  CCR.EMPRESA_ID, ' +
    '  CCR.CONDICAO_ID, ' +
    '  CON.NOME as NOME_CONDICAO, ' +
    '  CCR.ORDEM, ' +
    '  CCR.QUANTIDADE_PARCELAS, ' +
    '  CON.INDICE_ACRESCIMO, ' +
    '  CON.APLICAR_IND_PRECO_PROMOCIONAL, ' +
    '  CON.PERMITIR_PRECO_PROMOCIONAL ' +
    'from ' +
    '  CONDICOES_CARTAO_CREDITO_PDV CCR ' +

    'inner join CONDICOES_PAGAMENTO CON ' +
    'on CCR.CONDICAO_ID = CON.CONDICAO_ID ';

  setFiltros(getFiltros);

  AddColuna('EMPRESA_ID', True);
  AddColuna('CONDICAO_ID', True);
  AddColunaSL('NOME_CONDICAO');
  AddColuna('ORDEM');
  AddColuna('QUANTIDADE_PARCELAS');
  AddColunaSL('INDICE_ACRESCIMO');
  AddColunaSL('APLICAR_IND_PRECO_PROMOCIONAL');
  AddColunaSL('PERMITIR_PRECO_PROMOCIONAL');
end;

function TCondicoesCartaoCreditoPDV.getRecordCondicoesCartaoCreditoPDV: RecCondicoesCartaoCreditoPDV;
begin
  Result.EmpresaId                  := getInt('EMPRESA_ID', True);
  Result.CondicaoId                 := getInt('CONDICAO_ID', True);
  Result.NomeCondicao               := getString('NOME_CONDICAO');
  Result.Ordem                      := getInt('ORDEM');
  Result.QuantidadeParcelas         := getInt('QUANTIDADE_PARCELAS');
  Result.IndiceAcrescimo            := getDouble('INDICE_ACRESCIMO');
  Result.AplicarIndPrecoPromocional := getString('APLICAR_IND_PRECO_PROMOCIONAL');
  Result.PermitirPrecoPromocional   := getString('PERMITIR_PRECO_PROMOCIONAL');
end;

function AtualizarCondicoesCartaoCreditoPDV(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pCondicoes: TArray<RecCondicoesCartaoCreditoPDV>
): RecRetornoBD;
var
  i: Integer;
  vExec: TExecucao;
  t: TCondicoesCartaoCreditoPDV;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATU_COND_CARTAO_CRE_PDV');
  t := TCondicoesCartaoCreditoPDV.Create(pConexao);

  vExec := TExecucao.Create(pConexao);

  vExec.Add('delete from CONDICOES_CARTAO_CREDITO_PDV');
  vExec.Add('where EMPRESA_ID = :P1 ');

  try
    pConexao.IniciarTransacao;

    vExec.Executar([pEmpresaId]);

    t.setInt('EMPRESA_ID', pEmpresaId, True);
    for i := Low(pCondicoes) to High(pCondicoes) do begin
      t.setInt('CONDICAO_ID', pCondicoes[i].CondicaoId, True);
      t.setInt('ORDEM', pCondicoes[i].Ordem);
      t.setInt('QUANTIDADE_PARCELAS', pCondicoes[i].QuantidadeParcelas);

      t.Inserir;
    end;

    pConexao.FinalizarTransacao; 
  except
    on e: Exception do begin
      pConexao.VoltarTransacao; 
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
  t.Free;
end;

function BuscarCondicoesCartaoCreditoPDV(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCondicoesCartaoCreditoPDV>;
var
  i: Integer;
  t: TCondicoesCartaoCreditoPDV;
begin
  Result := nil;
  t := TCondicoesCartaoCreditoPDV.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordCondicoesCartaoCreditoPDV;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirCondicoesCartaoCreditoPDV(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pCondicaoId: Integer
): RecRetornoBD;
var
  t: TCondicoesCartaoCreditoPDV;
begin
  Result.TeveErro := False;
  t := TCondicoesCartaoCreditoPDV.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('EMPRESA_ID', pEmpresaId, True);
    t.setInt('CONDICAO_ID', pCondicaoId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
