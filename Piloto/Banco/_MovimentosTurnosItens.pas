unit _MovimentosTurnosItens;

interface

uses
  System.Classes, System.SysUtils, _RecordsEspeciais, _OperacoesBancoDados, _RecordsCaixa, _Conexao;

type
  RecMovimentoTurnoItens = record
    MovimentoTurnoId: Integer;
    id: Integer;
    tipo: string;
    CobrancaId: Integer;
    NomeCobranca: string;
    documento: string;
    orcamento_id: Integer;
    entrada_id: Integer;
    ValorDocumento: Double;
  end;

  TMovimentosTurnosItens = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordMovItens: RecMovimentoTurnoItens;
  end;

function AtualizarMovimentoTurnoItens(
  pConexao: TConexao;
  pMovimentoId: Integer;
  pId: Integer;
  pTipo: string;
  pControlandoTransacao: Boolean
): RecRetornoBD;

function BuscarMovimentosTurnosItens(pConexao: TConexao; pIndice: Integer; pFiltros: array of Variant): TArray<RecMovimentoTurnoItens>;

function getFiltros: TArray<RecFiltros>;

implementation

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where MOV.MOVIMENTO_TURNO_ID = :P1 '
    );
end;

constructor TMovimentosTurnosItens.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'MOVIMENTOS_TURNOS_ITENS');

  FSql :=
    'select ' +
    '  MOV.MOVIMENTO_TURNO_ID, ' +
    '  MOV.ID, ' +
    '  MOV.TIPO, ' +
    '  TPC.COBRANCA_ID, ' +
    '  TPC.NOME as NOME_COBRANCA, ' +
    '  case when MOV.TIPO = ''R'' then COR.DOCUMENTO else COP.DOCUMENTO end as DOCUMENTO, ' +
    '  COR.ORCAMENTO_ID, ' +
    '  COP.ENTRADA_ID, ' +
    '  case when MOV.TIPO = ''R'' then COR.VALOR_DOCUMENTO else cop.VALOR_DOCUMENTO end as VALOR_DOCUMENTO ' +
    'from ' +
    '  MOVIMENTOS_TURNOS_ITENS MOV ' +

    'left join CONTAS_RECEBER COR ' +
    'on MOV.ID = case when MOV.TIPO = ''P'' then 0 else COR.RECEBER_ID end ' +

    'left join CONTAS_PAGAR COP ' +
    'on MOV.ID = case when MOV.TIPO = ''R'' then 0 else COP.PAGAR_ID end ' +

    'inner join TIPOS_COBRANCA TPC ' +
    'on COR.COBRANCA_ID = TPC.COBRANCA_ID ';

  setFiltros(getFiltros);

  AddColuna('MOVIMENTO_TURNO_ID', True);
  AddColuna('ID', True);
  AddColuna('TIPO', True);
  AddColunaSL('COBRANCA_ID');
  AddColunaSL('NOME_COBRANCA');
  AddColunaSL('DOCUMENTO');
  AddColunaSL('ORCAMENTO_ID');
  AddColunaSL('ENTRADA_ID');
  AddColunaSL('VALOR_DOCUMENTO');
end;

function AtualizarMovimentoTurnoItens(
  pConexao: TConexao;
  pMovimentoId: Integer;
  pId: Integer;
  pTipo: string;
  pControlandoTransacao: Boolean
): RecRetornoBD;
var
  oMov: TMovimentosTurnosItens;
begin
  oMov := TMovimentosTurnosItens.Create(pConexao);
  Result.TeveErro := False;

  try
    if not pControlandoTransacao then
      pConexao.IniciarTransacao;

    oMov.setInt('MOVIMENTO_TURNO_ID', pMovimentoId, True);
    oMov.setInt('ID', pId, True);
    oMov.setString('TIPO', pTipo, True);

    oMov.Inserir;

    if not pControlandoTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pControlandoTransacao then
        pConexao.VoltarTransacao;

      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  oMov.Free;
end;

function BuscarMovimentosTurnosItens(pConexao: TConexao; pIndice: Integer; pFiltros: array of Variant): TArray<RecMovimentoTurnoItens>;
var
  i: Integer;
  vItem: TMovimentosTurnosItens;
begin
  Result := nil;
  vItem := TMovimentosTurnosItens.Create(pConexao);

  if vItem.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, vItem.getQuantidadeRegistros);
    for i := 0 to vItem.getQuantidadeRegistros - 1 do begin
      Result[i] := vItem.getRecordMovItens;
      vItem.ProximoRegistro;
    end;
  end;

  vItem.Free;
end;

function TMovimentosTurnosItens.getRecordMovItens: RecMovimentoTurnoItens;
begin
  Result.MovimentoTurnoId := getInt('MOVIMENTO_TURNO_ID', True);
  Result.id               := getInt('ID', True);
  Result.tipo             := getString('TIPO', True);
  Result.CobrancaId      := getInt('COBRANCA_ID');
  Result.NomeCobranca    := getString('NOME_COBRANCA');
  Result.documento        := getString('DOCUMENTO');
  Result.orcamento_id     := getInt('ORCAMENTO_ID');
  Result.entrada_id       := getInt('ENTRADA_ID');
  Result.ValorDocumento  := getDouble('VALOR_DOCUMENTO');
end;

end.
