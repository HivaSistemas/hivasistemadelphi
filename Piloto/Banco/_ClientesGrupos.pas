unit _ClientesGrupos;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecClientesGrupos = class
  public
    ClienteGrupoId: Integer;
    Nome: string;
    Ativo: string;
  end;

  TClientesGrupos = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordClientesGrupos: RecClientesGrupos;
  end;

function AtualizarClientesGrupos(
  pConexao: TConexao;
  pClienteGrupoId: Integer;
  pNome: string;
  pAtivo: string
): RecRetornoBD;

function BuscarClientesGrupos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecClientesGrupos>;

function ExcluirClientesGrupos(
  pConexao: TConexao;
  pClienteGrupoId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TClientesGrupos }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CLIENTE_GRUPO_ID = :P1 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome (Avan�ada)',
      True,
      0,
      'where NOME like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  NOME ',
      '',
      True
    );
end;

constructor TClientesGrupos.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CLIENTES_GRUPOS');

  FSql := 
    'select ' +
    '  CLIENTE_GRUPO_ID, ' +
    '  NOME, ' +
    '  ATIVO ' +
    'from ' +
    '  CLIENTES_GRUPOS ';

  setFiltros(getFiltros);

  AddColuna('CLIENTE_GRUPO_ID', True);
  AddColuna('NOME');
  AddColuna('ATIVO');
end;

function TClientesGrupos.getRecordClientesGrupos: RecClientesGrupos;
begin
  Result := RecClientesGrupos.Create;

  Result.ClienteGrupoId             := getInt('CLIENTE_GRUPO_ID', True);
  Result.Nome                       := getString('NOME');
  Result.Ativo                      := getString('ATIVO');
end;

function AtualizarClientesGrupos(
  pConexao: TConexao;
  pClienteGrupoId: Integer;
  pNome: string;
  pAtivo: string
): RecRetornoBD;
var
  t: TClientesGrupos;
  vNovo: Boolean;
  vSeq: TSequencia;
begin
  Result.Iniciar;
  pConexao.setRotina('ATUALIZAR_CLIENTES_GRUPOS');

  t := TClientesGrupos.Create(pConexao);

  vNovo := pClienteGrupoId = 0;
  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_CLIENTES_GRUPOS_ID');
    pClienteGrupoId := vSeq.getProximaSequencia;
    Result.AsInt := pClienteGrupoId;
    vSeq.Free;
  end;

  try
    pConexao.IniciarTransacao; 

    t.setInt('CLIENTE_GRUPO_ID', pClienteGrupoId, True);
    t.setString('NOME', pNome);
    t.setString('ATIVO', pAtivo);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao; 
  except
    on e: Exception do begin
      pConexao.VoltarTransacao; 
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarClientesGrupos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecClientesGrupos>;
var
  i: Integer;
  vSql: string;
  t: TClientesGrupos;
begin
  Result := nil;
  t := TClientesGrupos.Create(pConexao);

  vSql := '';
  if pSomenteAtivos then
    vSql := ' and ATIVO = ''S'' ';

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordClientesGrupos;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirClientesGrupos(
  pConexao: TConexao;
  pClienteGrupoId: Integer
): RecRetornoBD;
var
  t: TClientesGrupos;
begin
  Result.Iniciar;
  t := TClientesGrupos.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('CLIENTE_GRUPO_ID', pClienteGrupoId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
