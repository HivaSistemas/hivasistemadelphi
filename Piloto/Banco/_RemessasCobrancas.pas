unit _RemessasCobrancas;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _ContasReceber, _CobrancaBancaria, _RecordsRelatorios;

{$M+}
type
  RecRemessasCobrancas = record
    RemessaId: Integer;
    ContaId: string;
    NumeroRemessa: Integer;
    QuantidadeTitulos: Integer;
    DataHoraCadastro: TDateTime;
    DataEmissaoInicial: TDateTime;
    DataEmissaoFinal: TDateTime;
    ContasCobrancaId: Integer;
  end;

  TRemessasCobrancas = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordRemessasCobrancas: RecRemessasCobrancas;
  end;

function AtualizarRemessasCobrancas(
  pConexao: TConexao;
  pRemessaId: Integer;
  pContaId: string;
  pNumeroRemessa: Integer;
  pQuantidadeTitulos: Integer;
  pDataEmissaoInicial: TDateTime;
  pDataEmissaoFinal: TDateTime;
  pContasCobrancaId: Integer
): RecRetornoBD;

function BuscarRemessasCobrancas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecRemessasCobrancas>;

function ExcluirRemessasCobrancas(
  pConexao: TConexao;
  pRemessaId: Integer
): RecRetornoBD;

function AtualizarDadosRemessaFinanceiro(
  pConexao: TConexao;
  pRemessaId: Integer;
  pBoletos: TArray<RecContasReceber>;
  pNomeArquivo: string
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TRemessasCobrancas }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where XXX = :P1'
    );
end;

constructor TRemessasCobrancas.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'REMESSAS_COBRANCAS');

  FSql :=
    'select ' +
    '  REMESSA_ID, ' +
    '  CONTA_ID, ' +
    '  NUMERO_REMESSA, ' +
    '  QUANTIDADE_TITULOS, ' +
    '  DATA_HORA_CADASTRO, ' +
    '  DATA_EMISSAO_INICIAL, ' +
    '  DATA_EMISSAO_FINAL, ' +
    '  CONTABOLETO_ID '+
    'from ' +
    '  REMESSAS_COBRANCAS';

  setFiltros(getFiltros);

  AddColuna('REMESSA_ID', True);
  AddColuna('CONTA_ID');
  AddColuna('NUMERO_REMESSA');
  AddColuna('QUANTIDADE_TITULOS');
  AddColunaSL('DATA_HORA_CADASTRO');
  AddColuna('DATA_EMISSAO_INICIAL');
  AddColuna('DATA_EMISSAO_FINAL');
  AddColuna('CONTABOLETO_ID');

end;

function TRemessasCobrancas.getRecordRemessasCobrancas: RecRemessasCobrancas;
begin
  Result.RemessaId          := getInt('REMESSA_ID', True);
  Result.ContaId            := getString('CONTA_ID');
  Result.NumeroRemessa      := getInt('NUMERO_REMESSA');
  Result.QuantidadeTitulos  := getInt('QUANTIDADE_TITULOS');
  Result.DataHoraCadastro   := getData('DATA_HORA_CADASTRO');
  Result.DataEmissaoInicial := getData('DATA_EMISSAO_INICIAL');
  Result.DataEmissaoFinal   := getData('DATA_EMISSAO_FINAL');
  Result.ContasCobrancaId   := getInt('CONTABOLETO_ID');
end;

function AtualizarRemessasCobrancas(
  pConexao: TConexao;
  pRemessaId: Integer;
  pContaId: string;
  pNumeroRemessa: Integer;
  pQuantidadeTitulos: Integer;
  pDataEmissaoInicial: TDateTime;
  pDataEmissaoFinal: TDateTime;
  pContasCobrancaId: Integer
): RecRetornoBD;
var
  t: TRemessasCobrancas;
  vNovo: Boolean;
  vSeq: TSequencia;
begin
  Result.Iniciar;
  t := TRemessasCobrancas.Create(pConexao);
  pConexao.SetRotina('ATUALIZAR_REMESSA_COB');

  vNovo := pRemessaId = 0;
  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_REMESSAS_COBRANCAS');
    pRemessaId := vSeq.getProximaSequencia;
    Result.AsInt := pRemessaId;
    vSeq.Free;
  end;

  try
    pConexao.IniciarTransacao;

    t.setInt('REMESSA_ID', pRemessaId, True);
    t.setString('CONTA_ID', pContaId);
    t.setInt('NUMERO_REMESSA', pNumeroRemessa);
    t.setInt('QUANTIDADE_TITULOS', pQuantidadeTitulos);
    t.setData('DATA_EMISSAO_INICIAL', pDataEmissaoInicial);
    t.setData('DATA_EMISSAO_FINAL', pDataEmissaoFinal);
    t.setInt('CONTABOLETO_ID', pContasCobrancaId);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarRemessasCobrancas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecRemessasCobrancas>;
var
  i: Integer;
  t: TRemessasCobrancas;
begin
  Result := nil;
  t := TRemessasCobrancas.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordRemessasCobrancas;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirRemessasCobrancas(
  pConexao: TConexao;
  pRemessaId: Integer
): RecRetornoBD;
var
  t: TRemessasCobrancas;
begin
  Result.Iniciar;
  t := TRemessasCobrancas.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('REMESSA_ID', pRemessaId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function AtualizarDadosRemessaFinanceiro(
  pConexao: TConexao;
  pRemessaId: Integer;
  pBoletos: TArray<RecContasReceber>;   //_RecordsRelatorios.RecContasReceber
  pNomeArquivo: string
): RecRetornoBD;
var
  i: Integer;
  vExec: TExecucao;
begin
  Result.Iniciar;
	pConexao.SetRotina('ATUALIZAR_NR_REMESSA_FIN');

  if pBoletos = nil then
    Exit;

  vExec := TExecucao.Create(pConexao);
  vExec.Add('update CONTAS_RECEBER set ');
  vExec.Add('  REMESSA_ID = :P2, ');
  vExec.Add('  NOME_ARQUIVO_REMESSA = :P3 ');
  vExec.Add('where RECEBER_ID = :P1');
	try
		pConexao.IniciarTransacao;

    for i := Low(pBoletos) to High(pBoletos) do
      vExec.Executar([pBoletos[i].receber_id, pRemessaId, pNomeArquivo]);

		pConexao.FinalizarTransacao;
	except on e: Exception do
  	begin
  		pConexao.VoltarTransacao;
  		Result.TratarErro(e);
  	end;
	end;

  vExec.Free;
end;

end.

