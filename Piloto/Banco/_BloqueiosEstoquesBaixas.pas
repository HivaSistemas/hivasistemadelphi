unit _BloqueiosEstoquesBaixas;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _BloqueiosEstBaixasItens;

{$M+}
type
  RecBloqueiosEstoquesBaixas = record
    BaixaId: Integer;
    BloqueioId: Integer;
    Tipo: string;
    UsuarioBaixaId: Integer;
    DataHoraBaixa: TDateTime;
    Observacoes: string;
  end;

  TBloqueiosEstoquesBaixas = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordBloqueiosEstoquesBaixas: RecBloqueiosEstoquesBaixas;
  end;

function AtualizarBloqueiosEstoquesBaixas(
  pConexao: TConexao;
  pBaixaId: Integer;
  pBloqueioId: Integer;
  pTipo: string;
  pObservacoes: string;
  pItens: TArray<RecBloqueiosEstBaixasItens>
): RecRetornoBD;

function BuscarBloqueiosEstoquesBaixas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecBloqueiosEstoquesBaixas>;

function ExcluirBloqueiosEstoquesBaixas(
  pConexao: TConexao;
  pBaixaId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TBloqueiosEstoquesBaixas }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where BLOQUEIO_ID = :P1 '
    );
end;

constructor TBloqueiosEstoquesBaixas.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'BLOQUEIOS_ESTOQUES_BAIXAS');

  FSql := 
    'select ' +
    '  BAIXA_ID, ' +
    '  BLOQUEIO_ID, ' +
    '  TIPO, ' +
    '  USUARIO_BAIXA_ID, ' +
    '  DATA_HORA_BAIXA, ' +
    '  OBSERVACOES ' +
    'from ' +
    '  BLOQUEIOS_ESTOQUES_BAIXAS';

  setFiltros(getFiltros);

  AddColuna('BAIXA_ID', True);
  AddColuna('BLOQUEIO_ID');
  AddColuna('TIPO');
  AddColunaSL('USUARIO_BAIXA_ID');
  AddColunaSL('DATA_HORA_BAIXA');
  AddColuna('OBSERVACOES');
end;

function TBloqueiosEstoquesBaixas.getRecordBloqueiosEstoquesBaixas: RecBloqueiosEstoquesBaixas;
begin
  Result.BaixaId                    := getInt('BAIXA_ID', True);
  Result.BloqueioId                 := getInt('BLOQUEIO_ID');
  Result.Tipo                       := getString('TIPO');
  Result.UsuarioBaixaId             := getInt('USUARIO_BAIXA_ID');
  Result.DataHoraBaixa              := getData('DATA_HORA_BAIXA');
  Result.Observacoes                := getString('OBSERVACOES');
end;

function AtualizarBloqueiosEstoquesBaixas(
  pConexao: TConexao;
  pBaixaId: Integer;
  pBloqueioId: Integer;
  pTipo: string;
  pObservacoes: string;
  pItens: TArray<RecBloqueiosEstBaixasItens>
): RecRetornoBD;
var
  i: Integer;
  vSeq: TSequencia;
  vProc: TProcedimentoBanco;
  t: TBloqueiosEstoquesBaixas;
  vItem: TBloqueiosEstBaixasItens;
begin
  Result.Iniciar;
  pConexao.setRotina('ATUALIZAR_BLOQ_EST_BAIXA');

  t := TBloqueiosEstoquesBaixas.Create(pConexao);
  vItem := TBloqueiosEstBaixasItens.Create(pConexao);
  vProc := TProcedimentoBanco.Create(pConexao, 'CONSOLIDAR_BX_BLOQUEIO_ESTOQUE');

  vSeq := TSequencia.Create(pConexao, 'SEQ_BLOQUEIOS_ESTOQUES_BAIXAS');
  pBaixaId := vSeq.getProximaSequencia;
  Result.AsInt := pBaixaId;
  vSeq.Free;

  try
    pConexao.IniciarTransacao; 

    t.setInt('BAIXA_ID', pBaixaId, True);
    t.setInt('BLOQUEIO_ID', pBloqueioId);
    t.setString('TIPO', pTipo);
    t.setString('OBSERVACOES', pObservacoes);

     t.Inserir;

    for i := Low(pItens) to High(pItens) do begin
      vItem.setInt('BAIXA_ID', pBaixaId, True);
      vItem.setInt('ITEM_ID', pItens[i].ItemId, True);
      vItem.setDouble('QUANTIDADE', pItens[i].Quantidade);

      vItem.Inserir;
    end;

    vProc.Executar([pBaixaId]);

    pConexao.FinalizarTransacao; 
  except
    on e: Exception do begin
      pConexao.VoltarTransacao; 
      Result.TratarErro(e);
    end;
  end;

  vProc.Free;
  vItem.Free;
  t.Free;
end;

function BuscarBloqueiosEstoquesBaixas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecBloqueiosEstoquesBaixas>;
var
  i: Integer;
  t: TBloqueiosEstoquesBaixas;
begin
  Result := nil;
  t := TBloqueiosEstoquesBaixas.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordBloqueiosEstoquesBaixas;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirBloqueiosEstoquesBaixas(
  pConexao: TConexao;
  pBaixaId: Integer
): RecRetornoBD;
var
  t: TBloqueiosEstoquesBaixas;
begin
  Result.Iniciar;
  t := TBloqueiosEstoquesBaixas.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('BAIXA_ID', pBaixaId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
