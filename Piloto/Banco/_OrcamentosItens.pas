unit _OrcamentosItens;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsOrcamentosVendas,
  _RecordsExpedicao, _RecordsCadastros;

{$M+}
type
  TOrcamentoItens = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordOrcamento: RecOrcamentoItens;
  end;

function BuscarOrcamentosItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecOrcamentoItens>;

function BuscarOrcamentosItensComando(
  pConexao: TConexao;
  pComando: string
): TArray<RecOrcamentoItens>;

function BuscarOrcamentosItensAmbiente(
  pConexao: TConexao;
  pOrcamentoId: Integer
): TArray<RecAmbientes>;

function BuscarInformacoesImpressao(pConexao: TConexao; const pOrcamentoId: Integer): TArray<RecOrcamentosItensImpressao>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TOrcamentoItens }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ITE.ORCAMENTO_ID = :P1 ' +
      'and ITE.ITEM_KIT_ID is null ' +
      'order by ' +
      '  ITE.ITEM_ID '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ITE.ORCAMENTO_ID = :P1 ' +
      'and ITE.TIPO_CONTROLE_ESTOQUE <> ''A'' ' +  // N�o trazendo apenas os itens que � kit e entrega agrupada
      'and ( ' +
      '  ITE.QUANTIDADE - ' +
      '  ITE.QUANTIDADE_DEVOLV_ENTREGUES - ' +
      '  ITE.QUANTIDADE_DEVOLV_PENDENTES - ' +
      '  ITE.QUANTIDADE_DEVOLV_SEM_PREVISAO ' +
      ') > 0 ',
      'order by ' +
      '  ITE.ITEM_ID '
    );
end;

constructor TOrcamentoItens.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ORCAMENTOS_ITENS');

  FSql :=
    'select ' +
    '  ITE.ORCAMENTO_ID, ' +
    '  ITE.PRODUTO_ID, ' +
    '  ITE.ITEM_ID, ' +
    '  ITE.VALOR_TOTAL, ' +

    '  ITE.QUANTIDADE, ' +
    '  ITE.QUANTIDADE_RETIRAR_ATO, ' +
    '  ITE.QUANTIDADE_RETIRAR, ' +
    '  ITE.QUANTIDADE_ENTREGAR, ' +
    '  ITE.QUANTIDADE_SEM_PREVISAO, ' +

    '  ITE.QUANTIDADE_DEVOLV_ENTREGUES, ' +
    '  ITE.QUANTIDADE_DEVOLV_PENDENTES, ' +
    '  ITE.QUANTIDADE_DEVOLV_SEM_PREVISAO, ' +

    '  ITE.QUANTIDADE_RETIRADOS, ' +
    '  ITE.QUANTIDADE_ENTREGUES, ' +

    '  ITE.PRECO_UNITARIO, ' +
    '  ITE.VALOR_TOTAL_DESCONTO, ' +
    '  ITE.VALOR_TOTAL_OUTRAS_DESPESAS, ' +
    '  ITE.VALOR_TOTAL_FRETE, ' +
    '  PRO.PESO * ITE.QUANTIDADE AS PESO_TOTAL, ' +

    '  PRO.NOME, ' +
    '  PRO.UNIDADE_VENDA, ' +
    '  PRO.UNIDADE_ENTREGA_ID, ' +
    '  PRO.MULTIPLO_VENDA, ' +
    '  0 as MULTIPLO_VENDA_2, ' +
    '  0 as MULTIPLO_VENDA_3, ' +
    '  PRO.CODIGO_ORIGINAL_FABRICANTE, ' +
    '  PRO.CODIGO_BARRAS, ' +
    '  MAR.NOME as NOME_MARCA, ' +
    '  PRE.PRECO_VAREJO, ' +
    '  ITE.TIPO_CONTROLE_ESTOQUE, ' +
    '  ITE.TIPO_PRECO_UTILIZADO, ' +
    '  ITE.PERC_DESCONTO_PRECO_MANUAL, ' +
    '  ITE.VALOR_DESC_UNIT_PRECO_MANUAL, ' +
    '  ITE.PRECO_BASE, ' +
    '  ITE.VALOR_IMPOSTOS, ' +
    '  ITE.VALOR_ENCARGOS, ' +
    '  ITE.VALOR_CUSTO_VENDA, ' +
    '  ITE.VALOR_CUSTO_FINAL, ' +
    '  ORC.INDICE_CONDICAO_PAGAMENTO, ' +
    '  case PAE.TIPO_CUSTO_VIS_LUCRO_VENDA when ''FIN'' then zvl(ITE.PRECO_LIQUIDO, ITE.CUSTO_ULTIMO_PEDIDO) when ''COM'' then zvl(ITE.CUSTO_ULTIMO_PEDIDO, ITE.PRECO_LIQUIDO) else ITE.CMV end as CUSTO_ENTRADA ' +
    'from ' +
    '  ORCAMENTOS_ITENS ITE ' +

    'inner join ORCAMENTOS ORC ' +
    'on ITE.ORCAMENTO_ID = ORC.ORCAMENTO_ID ' +

    'inner join PRODUTOS PRO ' +
    'on ITE.PRODUTO_ID = PRO.PRODUTO_ID ' +

    'inner join MARCAS MAR ' +
    'on PRO.MARCA_ID = MAR.MARCA_ID ' +

    'inner join PRECOS_PRODUTOS PRE ' +
    'on ITE.PRODUTO_ID = PRE.PRODUTO_ID ' +
    'and ORC.EMPRESA_ID = PRE.EMPRESA_ID ' +

    'inner join PARAMETROS_EMPRESA PAE ' +
    'on ORC.EMPRESA_ID = PAE.EMPRESA_ID ';

  setFiltros(getFiltros);

  AddColuna('ORCAMENTO_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('PRODUTO_ID');
  AddColuna('VALOR_TOTAL');
  AddColuna('QUANTIDADE');

  AddColuna('QUANTIDADE_RETIRAR_ATO');
  AddColuna('QUANTIDADE_RETIRAR');
  AddColuna('QUANTIDADE_ENTREGAR');
  AddColuna('QUANTIDADE_SEM_PREVISAO');

  AddColuna('QUANTIDADE_DEVOLV_ENTREGUES');
  AddColuna('QUANTIDADE_DEVOLV_PENDENTES');
  AddColuna('QUANTIDADE_DEVOLV_SEM_PREVISAO');

  AddColuna('QUANTIDADE_RETIRADOS');
  AddColuna('QUANTIDADE_ENTREGUES');

  AddColuna('PRECO_UNITARIO');
  AddColuna('VALOR_TOTAL_DESCONTO');
  AddColuna('VALOR_TOTAL_OUTRAS_DESPESAS');
  AddColuna('VALOR_TOTAL_FRETE');
  AddColuna('TIPO_CONTROLE_ESTOQUE');
  AddColuna('TIPO_PRECO_UTILIZADO');
  AddColuna('PERC_DESCONTO_PRECO_MANUAL');
  AddColuna('VALOR_DESC_UNIT_PRECO_MANUAL');
  AddColuna('PRECO_BASE');
  AddColuna('VALOR_IMPOSTOS');
  AddColuna('VALOR_ENCARGOS');
  AddColuna('VALOR_CUSTO_VENDA');
  AddColuna('VALOR_CUSTO_FINAL');

  AddColunaSL('NOME');
  AddColunaSL('NOME_MARCA');
  AddColunaSL('UNIDADE_VENDA');
  AddColunaSL('UNIDADE_ENTREGA_ID');
  AddColunaSL('MULTIPLO_VENDA');
  AddColunaSL('MULTIPLO_VENDA_2');
  AddColunaSL('MULTIPLO_VENDA_3');
  AddColunaSL('CODIGO_ORIGINAL_FABRICANTE');
  AddColunaSL('CODIGO_BARRAS');
  AddColunaSL('PRECO_VAREJO');
  AddColunaSL('CUSTO_ENTRADA');
  AddColunaSL('INDICE_CONDICAO_PAGAMENTO');

  AddColunaSL('PESO_TOTAL');
end;

function TOrcamentoItens.getRecordOrcamento: RecOrcamentoItens;
begin
  Result.orcamento_id                 := getInt('ORCAMENTO_ID', True);
  Result.produto_id                   := getInt('PRODUTO_ID');
  Result.item_id                      := getInt('ITEM_ID', True);
  Result.valor_total                  := getDouble('VALOR_TOTAL');
  Result.Quantidade                   := getDouble('QUANTIDADE');

  Result.QuantidadeRetirarAto         := getDouble('QUANTIDADE_RETIRAR_ATO');
  Result.QuantidadeRetirar            := getDouble('QUANTIDADE_RETIRAR');
  Result.QuantidadeEntregar           := getDouble('QUANTIDADE_ENTREGAR');
  Result.QuantidadeSemPrevisao        := getDouble('QUANTIDADE_SEM_PREVISAO');

  Result.QuantidadeDevolvEntregues    := getDouble('QUANTIDADE_DEVOLV_ENTREGUES');
  Result.QuantidadeDevolvPendentes    := getDouble('QUANTIDADE_DEVOLV_PENDENTES');
  Result.QuantidadeDevolvSemPrevisao  := getDouble('QUANTIDADE_DEVOLV_SEM_PREVISAO');

  Result.QuantidadeRetirados          := getDouble('QUANTIDADE_RETIRADOS');
  Result.QuantidadeEntregues          := getDouble('QUANTIDADE_ENTREGUES');

  Result.SaldoRestanteAcumulado       := Result.Quantidade - Result.QuantidadeDevolvEntregues - Result.QuantidadeDevolvPendentes - Result.QuantidadeDevolvSemPrevisao;

  Result.preco_unitario              := getDouble('PRECO_UNITARIO');
  Result.preco_varejo                := getDouble('PRECO_VAREJO');
  Result.nome                        := getString('NOME');
  Result.multiplo_venda              := getDouble('MULTIPLO_VENDA');
  Result.multiplo_venda_2            := getDouble('MULTIPLO_VENDA_2');
  Result.multiplo_venda_3            := getDouble('MULTIPLO_VENDA_3');
  Result.nome_marca                  := getString('NOME_MARCA');
  Result.unidade_venda               := getString('UNIDADE_VENDA');
  Result.UnidadeEntregaId            := getString('UNIDADE_ENTREGA_ID');
  Result.CodigoOriginalFabricante    := getString('CODIGO_ORIGINAL_FABRICANTE');
  Result.CodigoBarras                := getString('CODIGO_BARRAS');
  Result.valor_total_desconto        := getDouble('VALOR_TOTAL_DESCONTO');
  Result.valor_total_outras_despesas := getDouble('VALOR_TOTAL_OUTRAS_DESPESAS');
  Result.ValorTotalFrete             := getDouble('VALOR_TOTAL_FRETE');

  Result.ValorImpostos               := getDouble('VALOR_IMPOSTOS');
  Result.ValorEncargos               := getDouble('VALOR_ENCARGOS');
  Result.ValorCustoVenda             := getDouble('VALOR_CUSTO_VENDA');
  Result.ValorCustoFinal             := getDouble('VALOR_CUSTO_FINAL');

  Result.TipoControleEstoque         := getString('TIPO_CONTROLE_ESTOQUE');
  Result.TipoPrecoUtilizado          := getString('TIPO_PRECO_UTILIZADO');
  Result.PercDescontoPrecoManual     := getDouble('PERC_DESCONTO_PRECO_MANUAL');
  Result.ValorDescUnitPrecoManual    := getDouble('VALOR_DESC_UNIT_PRECO_MANUAL');
  Result.PrecoBase                   := getDouble('PRECO_BASE');

  Result.CustoEntrada                := getDouble('CUSTO_ENTRADA');
  Result.IndiceCondicaoPagamento     := getDouble('INDICE_CONDICAO_PAGAMENTO');

  Result.PesoTotal                   := getDouble('PESO_TOTAL');

  Result.ValorLiquido := Result.valor_total + Result.valor_total_outras_despesas - Result.valor_total_desconto;

  Result.TipoPrecoUtilizadoAnalitico :=
    _Biblioteca.Decode(
      Result.TipoPrecoUtilizado,[
        'VAR', 'Varejo',
        'PRO', 'Promocional',
        'AT1', 'Atacado 1',
        'AT2', 'Atacado 2',
        'AT3', 'Atacado 3',
        'PDV', 'PDV',
        'PES', 'Ponta de est.',
        'MAN', 'Manual'
      ]
    );
end;

function BuscarOrcamentosItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecOrcamentoItens>;
var
  i: Integer;
  t: TOrcamentoItens;
begin
  Result := nil;
  t := TOrcamentoItens.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordOrcamento;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarOrcamentosItensAmbiente(
  pConexao: TConexao;
  pOrcamentoId: Integer
): TArray<RecAmbientes>;
var
  i: Integer;
  j: Integer;
  vSql: TConsulta;
  posAmbiente: Integer;
  posProdutos: Integer;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  AMBIENTE, ');
  vSql.Add('  PRODUTO_ID, ');
  vSql.Add('  QUANTIDADE ');
  vSql.Add('from ORCAMENTOS_ITENS_AMBIENTE ');
  vSql.Add('where ORCAMENTO_ID = :P1 ');
  vSql.Add('order by AMBIENTE, PRODUTO_ID, DATA_HORA_ALTERACAO asc ');

  if vSql.Pesquisar([pOrcamentoId]) then begin
    //SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      if Result = nil then begin
        SetLength(Result, Length(Result) + 1);
        posAmbiente := High(Result);
      end
      else begin
        posAmbiente := -1;
        for j := Low(Result) to High(Result) do begin
          if Result[j].NomeAmbiente = vSql.GetString('AMBIENTE')  then begin
            posAmbiente := j;
            Break;
          end;
        end;

        if posAmbiente = -1 then begin
          SetLength(Result, Length(Result) + 1);
          posAmbiente := High(Result);
        end;
      end;

      SetLength(Result[posAmbiente].Produtos, Length(Result[posAmbiente].Produtos) + 1);
      posProdutos := High(Result[posAmbiente].Produtos);

      Result[posAmbiente].NomeAmbiente := vSql.GetString('AMBIENTE');
      Result[posAmbiente].Produtos[posProdutos].ProdutoId  := vSql.GetInt('PRODUTO_ID');
      Result[posAmbiente].Produtos[posProdutos].Quantidade := vSql.GetDouble('QUANTIDADE');

      vSql.Next;
    end;
  end;

  vSql.Free;
end;

function BuscarOrcamentosItensComando(
  pConexao: TConexao;
  pComando: string
): TArray<RecOrcamentoItens>;
var
  i: Integer;
  t: TOrcamentoItens;
begin
  Result := nil;
  t := TOrcamentoItens.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordOrcamento;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarInformacoesImpressao(pConexao: TConexao; const pOrcamentoId: Integer): TArray<RecOrcamentosItensImpressao>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  with vSql.SQL do begin
    Add('select ');
    Add('  PRODUTO_ID, ');
    Add('  ITEM_ID, ');
    Add('  NOME_PRODUTO, ');
    Add('  MARCA_ID, ');
    Add('  NOME_MARCA, '); // 4
    Add('  PRECO_UNITARIO, ');
    Add('  QUANTIDADE, ');
    Add('  UNIDADE_VENDA, ');
    Add('  VALOR_TOTAL, ');
    Add('  TIPO_PRECO_UTILIZADO, ');
    Add('  VALOR_ORIGINAL, ');
    Add('  VALOR_TOTAL_DESCONTO, ');
    Add('  VALOR_DESC_UNIT_PRECO_MANUAL, ');
    Add('  UNIDADE_ENTREGA_ID, ');
    Add('  MULTIPLO_VENDA ');
    Add('from ');
    Add('  VW_ORCAMENTOS_ITENS_IMPRESSAO ITE ');
    Add('where ORCAMENTO_ID = :P1 ');
    Add('order by ITEM_ID asc ');
  end;

  if vSql.Pesquisar([pOrcamentoId]) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].ProdutoId     := vSql.GetInt(0);
      Result[i].ItemId        := vSql.GetInt(1);
      Result[i].NomeProduto   := vSql.GetString(2);
      Result[i].MarcaId       := vSql.GetInt(3);
      Result[i].NomeMarca     := vSql.GetString(4);
      Result[i].PrecoUnitario := vSql.GetDouble(5);
      Result[i].Quantidade    := vSql.GetDouble(6);
      Result[i].UnidadeVenda  := vSql.GetString(7);
      Result[i].ValorTotal    := vSql.GetDouble(8);
      Result[i].TipoPrecoUtilitario   := vSql.GetString(9);
      Result[i].ValorOriginal         := vSql.GetDouble(10);
      Result[i].ValorTotalDesconto    := vSql.GetDouble(11);
      Result[i].ValorDescUnitPrecoManual := vSql.GetDouble(12);

      Result[i].UnidadeEntrega       := vSql.GetString(13);
      Result[i].MultiploVenda        := vSql.GetDouble(14);


      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

end.
