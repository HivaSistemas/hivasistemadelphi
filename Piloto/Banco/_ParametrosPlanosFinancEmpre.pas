unit _ParametrosPlanosFinancEmpre;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecParametrosPlanosFinancEmpre = record
    EmpresaId: Integer;
    PlanoFinAjusteEstNormalId: string;
  end;

  TParametrosPlanosFinancEmpre = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordParametro: RecParametrosPlanosFinancEmpre;
  end;

function BuscarParametros(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecParametrosPlanosFinancEmpre>;

function AtualizarParametros(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pPlanoFinAjusteEstNormalId: string
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TParametrosPlanosFinancEmpre }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where EMPRESA_ID = :P1 '
    );
end;

constructor TParametrosPlanosFinancEmpre.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PARAMETROS_PLANOS_FINANC_EMPRE');

  FSql :=
    'select ' +
    '  EMPRESA_ID, ' +
    '  PLANO_FIN_AJUSTE_EST_NORMAL_ID ' +
    'from ' +
    '  PARAMETROS_PLANOS_FINANC_EMPRE ';

  setFiltros(getFiltros);

  AddColuna('EMPRESA_ID', True);
  AddColuna('PLANO_FIN_AJUSTE_EST_NORMAL_ID');
end;

function TParametrosPlanosFinancEmpre.getRecordParametro: RecParametrosPlanosFinancEmpre;
begin
  Result.EmpresaId                 := getInt('EMPRESA_ID', True);
  Result.PlanoFinAjusteEstNormalId := getString('PLANO_FIN_AJUSTE_EST_NORMAL_ID');
end;

function BuscarParametros(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecParametrosPlanosFinancEmpre>;
var
  i: Integer;
  t: TParametrosPlanosFinancEmpre;
begin
  Result := nil;
  t := TParametrosPlanosFinancEmpre.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordParametro;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function AtualizarParametros(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pPlanoFinAjusteEstNormalId: string
): RecRetornoBD;
var
  t: TParametrosPlanosFinancEmpre;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_PAR_PLANOS_FINANCEIRO');

  t := TParametrosPlanosFinancEmpre.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('EMPRESA_ID', pEmpresaId, True);
    t.setString('PLANO_FIN_AJUSTE_EST_NORMAL_ID', pPlanoFinAjusteEstNormalId);

    t.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
