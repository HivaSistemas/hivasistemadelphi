unit _ValidaAcessoSistema;

interface

uses System.Classes, System.Net.HttpClient, System.NetEncoding,  System.SysUtils,
     _RecordsCadastros, _RecordsEspeciais, _Empresas, System.DateUtils;

  Function ValidarSistema:Boolean;

implementation

uses _Sessao, _Criptografia, _Biblioteca;

Function ValidarSistema:Boolean;
var
  vEmpresas: TArray<RecEmpresas>;
  vValidade: TDateTime;
  vRetBanco: RecRetornoBD;
  vRetornoAPI: string;
  vDiasCarrencia: Integer;

  function BuscaChaveAPI(pCnpj: string): string;
  var
    HttpClient: THTTPClient;
    Response: IHTTPResponse;
    Base64: TBase64Encoding;
  begin
    HttpClient := THTTPClient.Create;
    try
      Base64 := TBase64Encoding. Create;
      HttpClient.ContentType := 'application/json';
      httpClient.CustomHeaders['Authorization'] := 'Basic ' + base64.Encode('Hiva:@123456');
      try
        Response := HttpClient.Get('http://hivasistema.ddns.net:9008/validade/'+RetornaNumeros(pCnpj));
        Result := Response.ContentAsString();
      except
        result := '';
      end;
    finally
      Base64.Free;
      HttpClient.Free;
    end;
  end;


begin
  Result := True;
  vDiasCarrencia := 0;
  vEmpresas := Sessao.getEmpresas;
  if not vEmpresas[0].DataValidade.IsEmpty then
    vValidade := StrToDateDef(Sessao.getCriptografia.Descriptografar(vEmpresas[0].DataValidade, False),StrToDate('31/12/1899'));

  if (vValidade < Date) or (vEmpresas[0].DataUltimaConsultaAPI < Date) then
  begin
    vRetornoAPI := BuscaChaveAPI(vEmpresas[0].Cnpj);
    if not vRetornoAPI.IsEmpty then
    begin
      vRetBanco :=
        _Empresas.AtualizarDataValidadeEmpresa(
          Sessao.getConexaoBanco,
          vEmpresas[0].EmpresaId,
          vRetornoAPI);
      vEmpresas := _Empresas.BuscarEmpresas(Sessao.getConexaoBanco, 0, [ vEmpresas[0].EmpresaId]);
      vValidade := StrToDateDef(Sessao.getCriptografia.Descriptografar(vEmpresas[0].DataValidade, False),StrToDate('31/12/1899'));
    end;

    if (vValidade < date) then
    begin
      vDiasCarrencia := DaysBetween(date, vValidade);
      case vDiasCarrencia of
        1: vDiasCarrencia := 5;
        2: vDiasCarrencia := 4;
        3: vDiasCarrencia := 3;
        4: vDiasCarrencia := 2;
        5: vDiasCarrencia := 1;
      end;
      if vDiasCarrencia <= 5 then
        _Biblioteca.Exclamar('Licen�a expirada, sistema ser� bloqueado em '+vDiasCarrencia.ToString+' dia(s)! ');
    end;

  end;

  Result := (vValidade >= date) or (vDiasCarrencia <= 5) or (vValidade <= StrToDate('31/12/1899'));

end;

end.
