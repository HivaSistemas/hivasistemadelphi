unit _Motoristas;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros, _Cadastros, _CadastrosTelefones, _DiversosEnderecos;

{$M+}
type
  RecMotoristas = class
  public
    Cadastro: RecCadastros;

    CadastroId: Integer;
    NumeroCNH: string;
    DataValidadeCNH: TDateTime;
    DataHoraCadastro: TDateTime;
  end;

  TMotoristas = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao; pSomenteAtivos: Boolean);
  protected
    function GetRecordMotorista: RecMotoristas;
  end;

function AtualizarMotorista(
  pConexao: TConexao;
  pPesquisouCadastro: Boolean;
  pCadastroId: Integer;
  pCadastro: RecCadastros;
  pTelefones: TArray<RecCadastrosTelefones>;
  pEnderecos: TArray<RecDiversosEnderecos>;
  pNumeroCNH: string;
  pDataValidadeCNH: TDateTime
): RecRetornoBD;

function BuscarMotoristas(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecMotoristas>;

function ExcluirMotoristas(
  pConexao: TConexao;
  pCadastroId: Integer
): RecRetornoBD;

function EFornecedor(pConexao: TConexao; pCadastroId: Integer): Boolean;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TMotoristas }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 4);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'and MOT.CADASTRO_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome fantasia (Avan�ado)',
      False,
      0,
      'and CAD.NOME_FANTASIA like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  CAD.NOME_FANTASIA ',
      '',
      True
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Raz�o social (Avan�ado)',
      True,
      1,
      'and CAD.RAZAO_SOCIAL like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  CAD.RAZAO_SOCIAL ',
      '',
      True
    );

  Result[3] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'CPF/CNPJ',
      False,
      2,
      'and CAD.CPF_CNPJ = :P1 ' +
      'order by ' +
      '  CAD.CPF_CNPJ '
    );
end;

constructor TMotoristas.Create(pConexao: TConexao; pSomenteAtivos: Boolean);
begin
  inherited Create(pConexao, 'MOTORISTAS');

  FSql :=
    'select ' +
    '  MOT.CADASTRO_ID, ' +
    '  MOT.NUMERO_CNH, ' +
    '  MOT.DATA_VALIDADE_CNH, ' +
    '  MOT.DATA_HORA_CADASTRO, ' +
    // Cadastro
    '  CAD.NOME_FANTASIA, ' +
    '  CAD.RAZAO_SOCIAL, ' +
    '  CAD.TIPO_PESSOA, ' +
    '  CAD.CPF_CNPJ, ' +
    '  CAD.LOGRADOURO, ' +
    '  CAD.COMPLEMENTO, ' +
    '  CAD.NUMERO, ' +
    '  CAD.BAIRRO_ID, ' +
    '  CAD.PONTO_REFERENCIA, ' +
    '  CAD.CEP, ' +
    '  CAD.E_MAIL, ' +
    '  CAD.SEXO, ' +
    '  CAD.ESTADO_CIVIL, ' +
    '  CAD.DATA_NASCIMENTO, ' +
    '  CAD.DATA_CADASTRO, ' +
    '  CAD.INSCRICAO_ESTADUAL, ' +
    '  CAD.CNAE, ' +
    '  CAD.RG, ' +
    '  CAD.ORGAO_EXPEDIDOR_RG, ' +
    '  CAD.E_CLIENTE, ' +
    '  CAD.E_FORNECEDOR, ' +
    '  CAD.E_MOTORISTA, ' +
    '  CAD.ATIVO as CADASTRO_ATIVO, ' +
    '  CID.ESTADO_ID ' +
    'from ' +
    '  MOTORISTAS MOT ' +

    'inner join CADASTROS CAD ' +
    'on MOT.CADASTRO_ID = CAD.CADASTRO_ID ' +

    'inner join BAIRROS BAI ' +
    'on CAD.BAIRRO_ID = BAI.BAIRRO_ID ' +

    'inner join CIDADES CID ' +
    'on BAI.CIDADE_ID = CID.CIDADE_ID ';

  if pSomenteAtivos then
    FSql := FSql + ' where CAD.ATIVO = ''S'''
  else
    FSql := FSql + ' where CAD.ATIVO in(''S'', ''N'') ';

  SetFiltros(GetFiltros);

  AddColuna('CADASTRO_ID', True);
  AddColuna('NUMERO_CNH');
  AddColuna('DATA_VALIDADE_CNH');

  AddColunaSL('DATA_HORA_CADASTRO');
  // CADASTRO
  AddColunaSL('NOME_FANTASIA');
  AddColunaSL('RAZAO_SOCIAL');
  AddColunaSL('TIPO_PESSOA');
  AddColunaSL('CPF_CNPJ');
  AddColunaSL('LOGRADOURO');
  AddColunaSL('COMPLEMENTO');
  AddColunaSL('NUMERO');
  AddColunaSL('BAIRRO_ID');
  AddColunaSL('CEP');
  AddColunaSL('PONTO_REFERENCIA');
  AddColunaSL('E_MAIL');
  AddColunaSL('SEXO');
  AddColunaSL('ESTADO_CIVIL');
  AddColunaSL('DATA_NASCIMENTO');
  AddColunaSL('DATA_CADASTRO');
  AddColunaSL('INSCRICAO_ESTADUAL');
  AddColunaSL('CNAE');
  AddColunaSL('RG');
  AddColunaSL('ORGAO_EXPEDIDOR_RG');
  AddColunaSL('E_CLIENTE');
  AddColunaSL('E_FORNECEDOR');
  AddColunaSL('E_MOTORISTA');
  AddColunaSL('CADASTRO_ATIVO');
  AddColunaSL('ESTADO_ID');
end;

function TMotoristas.GetRecordMotorista: RecMotoristas;
begin
  Result := RecMotoristas.Create;
  Result.Cadastro := RecCadastros.Create;

  Result.CadastroId       := getInt('CADASTRO_ID', True);
  Result.NumeroCNH        := GetString('NUMERO_CNH');
  Result.DataValidadeCNH  := GetData('DATA_VALIDADE_CNH');
  Result.DataHoraCadastro := GetData('DATA_HORA_CADASTRO');

  // Cadastro
  Result.Cadastro.cadastro_id        := GetInt('CADASTRO_ID', True);
  Result.Cadastro.razao_social       := getString('RAZAO_SOCIAL');
  Result.Cadastro.nome_fantasia      := getString('NOME_FANTASIA');
  Result.Cadastro.tipo_pessoa        := getString('TIPO_PESSOA');
  Result.Cadastro.cpf_cnpj           := getString('CPF_CNPJ');
  Result.Cadastro.logradouro         := getString('LOGRADOURO');
  Result.Cadastro.complemento        := getString('COMPLEMENTO');
  Result.Cadastro.numero             := getString('NUMERO');
  Result.Cadastro.bairro_id          := getInt('BAIRRO_ID');
  Result.Cadastro.ponto_referencia   := getString('PONTO_REFERENCIA');
  Result.Cadastro.cep                := getString('CEP');
  Result.Cadastro.e_mail             := getString('E_MAIL');
  Result.Cadastro.sexo               := getString('SEXO');
  Result.Cadastro.estado_civil       := getString('ESTADO_CIVIL');
  Result.Cadastro.data_nascimento    := getData('DATA_NASCIMENTO');
  Result.Cadastro.data_cadastro      := GetData('DATA_CADASTRO');
  Result.Cadastro.inscricao_estadual := getString('INSCRICAO_ESTADUAL');
  Result.Cadastro.cnae               := getString('CNAE');
  Result.Cadastro.rg                 := getString('RG');
  Result.Cadastro.orgao_expedidor_rg := getString('ORGAO_EXPEDIDOR_RG');
  Result.Cadastro.e_cliente          := getString('E_CLIENTE');
  Result.Cadastro.e_fornecedor       := getString('E_FORNECEDOR');
  Result.Cadastro.ativo              := getString('CADASTRO_ATIVO');
  Result.Cadastro.estado_id          := getString('ESTADO_ID');
end;

function AtualizarMotorista(
  pConexao: TConexao;
  pPesquisouCadastro: Boolean;
  pCadastroId: Integer;
  pCadastro: RecCadastros;
  pTelefones: TArray<RecCadastrosTelefones>;
  pEnderecos: TArray<RecDiversosEnderecos>;
  pNumeroCNH: string;
  pDataValidadeCNH: TDateTime
): RecRetornoBD;
var
  vMot: TMotoristas;
  vNovo: Boolean;
  vCad: TCadastro;
  vRetBanco: RecRetornoBD;
begin
  vNovo := pCadastroId = 0;
  Result.Iniciar;
  vMot := TMotoristas.Create(pConexao, False);
  vCad := TCadastro.Create(pConexao);

  if vNovo then begin
    pCadastroId := TSequencia.Create(pConexao, 'SEQ_CADASTRO_ID').GetProximaSequencia;
    Result.AsInt := pCadastroId;
    pCadastro.cadastro_id := pCadastroId;
  end;

  try
    pConexao.IniciarTransacao;

    vCad.setCadastro(pCadastro);
    if vNovo then
      vCad.Inserir
    else
      vCad.Atualizar;

    vMot.setInt('CADASTRO_ID', pCadastroId, True);
    vMot.SetString('NUMERO_CNH', pNumeroCNH);
    vMot.setData('DATA_VALIDADE_CNH', pDataValidadeCNH);

    if vNovo or pPesquisouCadastro then
      vMot.Inserir
    else
      vMot.Atualizar;

    vRetBanco := _CadastrosTelefones.AtualizarTelefones(pConexao, pCadastroId, pTelefones);
    if vRetBanco.TeveErro then
      raise Exception.Create(vRetBanco.MensagemErro);

    if pEnderecos <> nil then begin
      vRetBanco := _DiversosEnderecos.AtualizarDiversosEndereco(pConexao, pCadastroId, pEnderecos);

      if vRetBanco.TeveErro then
        raise Exception.Create(vRetBanco.MensagemErro);
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vCad.Free;
  vMot.Free;
end;

function BuscarMotoristas(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecMotoristas>;
var
  i: Integer;
  t: TMotoristas;
begin
  Result := nil;
  t := TMotoristas.Create(pConexao, pSomenteAtivos);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordMotorista;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirMotoristas(
  pConexao: TConexao;
  pCadastroId: Integer
): RecRetornoBD;
var
  ex: TExecucao;
  t: TMotoristas;
begin
  Result.TeveErro := False;
  t := TMotoristas.Create(pConexao, False);
  ex := TExecucao.Create(pConexao);

  t.SetInt('CADASTRO_ID', pCadastroId, True);

  try
    pConexao.IniciarTransacao;

    ex.SQL.Add('delete from DIVERSOS_CONTATOS where CADASTRO_ID = :P1');
    ex.Executar([pCadastroId]);

    ex.SQL.Clear;
    ex.SQL.Add('delete from DIVERSOS_ENDERECOS where CADASTRO_ID = :P1');
    ex.Executar([pCadastroId]);

    t.Excluir;
    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function EFornecedor(pConexao: TConexao; pCadastroId: Integer): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select count(*) from FORNECEDORES where CADASTRO_ID = :P1');
  vSql.Pesquisar([pCadastroId]);

  Result := vSql.GetInt(0) > 0;

  vSql.Free;
end;


end.
