unit _OrcamentosItensDefLotes;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TOrcamentosItensDefLotes = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  end;

implementation

{ TOrcamentosItensDefLotes }

constructor TOrcamentosItensDefLotes.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ORCAMENTOS_ITENS_DEF_LOTES');

  FSql :=
    '';

  AddColuna('ORCAMENTO_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('LOTE', True);
  AddColuna('QUANTIDADE_RETIRAR_ATO');
  AddColuna('QUANTIDADE_RETIRAR');
  AddColuna('QUANTIDADE_ENTREGAR');
end;

end.
