unit _CentroCustos;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils;

{$M+}
type
  RecCentrosCustos = class
  public
    CentroCustoId: Integer;
    Nome: string;
    Ativo: string;
  end;

  TCentrosCustos = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordCentros: RecCentrosCustos;
  end;

function AtualizarCentroCusto(
  pConexao: TConexao;
  pCentroCustoId: Integer;
  pNome: string;
  pAtivo: string
): RecRetornoBD;

function BuscarCentroCustos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCentrosCustos>;

function ExcluirCentro(
  pConexao: TConexao;
  pCentroCustoId: Integer
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TCentrosCustos }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CENTRO_CUSTO_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Centro ( avan�ado )',
      True,
      0,
      'where NOME like ''%'' || :P1 || ''%'' ' +
      'order by ' +
      '  NOME'
    );
end;

constructor TCentrosCustos.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CENTROS_CUSTOS');

  FSql :=
    'select ' +
    '  CENTRO_CUSTO_ID, ' +
    '  NOME, ' +
    '  ATIVO ' +
    'from ' +
    '  CENTROS_CUSTOS ';

  SetFiltros(GetFiltros);

  AddColuna('CENTRO_CUSTO_ID', True);
  AddColuna('NOME');
  AddColuna('ATIVO');
end;

function TCentrosCustos.GetRecordCentros: RecCentrosCustos;
begin
  Result := RecCentrosCustos.Create;

  Result.CentroCustoId := GetInt('CENTRO_CUSTO_ID', True);
  Result.Nome          := GetString('NOME');
  Result.Ativo         := GetString('ATIVO');
end;

function AtualizarCentroCusto(
  pConexao: TConexao;
  pCentroCustoId: Integer;
  pNome: string;
  pAtivo: string
): RecRetornoBD;
var
  t: TCentrosCustos;
  vNovo: Boolean;
begin
  vNovo := pCentroCustoId = 0;
  Result.TeveErro := False;
  t := TCentrosCustos.Create(pConexao);

  if vNovo then begin
    pCentroCustoId := TSequencia.Create(pConexao, 'SEQ_CENTRO_CUSTO_ID').GetProximaSequencia;
    Result.AsInt := pCentroCustoId;
  end;

  t.SetInt('CENTRO_CUSTO_ID', pCentroCustoId, True);
  t.SetString('NOME', pNome);
  t.SetString('ATIVO', pAtivo);

  try
    pConexao.IniciarTransacao;

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarCentroCustos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCentrosCustos>;
var
  i: Integer;
  t: TCentrosCustos;
begin
  Result := nil;
  t := TCentrosCustos.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordCentros;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirCentro(
  pConexao: TConexao;
  pCentroCustoId: Integer
): RecRetornoBD;
var
  t: TCentrosCustos;
begin
  pConexao.SetRotina('EXCLUIR_CENTRO');
  Result.TeveErro := False;
  t := TCentrosCustos.Create(pConexao);

  t.SetInt('CENTRO_CUSTO_ID', pCentroCustoId, True);

  try
    pConexao.IniciarTransacao;

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
