unit _ContasRecBaixasPagamentos;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsFinanceiros;

{$M+}
type
  TContasRecBaixasPagamentos = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordContasRecBaixasPagamentos: RecTitulosFinanceiros;
  end;

function BuscarContasRecBaixasPagamentos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TContasRecBaixasPagamentos }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 4);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CRP.BAIXA_ID = :P1 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CRP.BAIXA_ID = :P1 ' +
      'and CRP.TIPO = ''CR'' ' +
      'and TCO.TIPO_CARTAO = ''D'' '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CRP.BAIXA_ID = :P1 ' +
      'and CRP.TIPO = ''CR'' ' +
      'and TCO.TIPO_CARTAO = ''C'' '
    );

  Result[3] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CRP.BAIXA_ID = :P1 ' +
      'and CRP.TIPO = ''CO'' '
    );
end;

constructor TContasRecBaixasPagamentos.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONTAS_REC_BAIXAS_PAGAMENTOS');

  FSql :=
    'select ' +
    '  CRP.BAIXA_ID, ' +
    '  CRP.COBRANCA_ID, ' +
    '  CRP.ITEM_ID, ' +
    '  CRP.TIPO, ' +
    '  CRP.PARCELA, ' +
    '  CRP.NUMERO_PARCELAS, ' +
    '  CRP.DATA_VENCIMENTO, ' +
    '  CRP.NSU_TEF, ' +
    '  CRP.CODIGO_AUTORIZACAO, ' +
    '  CRP.NUMERO_CARTAO, ' +
    '  CRP.TIPO_RECEB_CARTAO, ' +
    '  CRP.VALOR, ' +
    '  TCO.NOME as NOME_TIPO_COBRANCA, ' +
    '  TCO.TIPO_CARTAO, ' +
    '  TCO.TIPO_RECEBIMENTO_CARTAO ' +
    'from ' +
    '  CONTAS_REC_BAIXAS_PAGAMENTOS CRP ' +

    'inner join TIPOS_COBRANCA TCO ' +
    'on CRP.COBRANCA_ID = TCO.COBRANCA_ID ';

  setFiltros(getFiltros);

  AddColuna('BAIXA_ID', True);
  AddColuna('COBRANCA_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('TIPO');
  AddColuna('PARCELA');
  AddColuna('NUMERO_PARCELAS');
  AddColuna('DATA_VENCIMENTO');
  AddColuna('NSU_TEF');
  AddColuna('CODIGO_AUTORIZACAO');
  AddColuna('NUMERO_CARTAO');
  AddColuna('VALOR');
  AddColunaSL('COBRANCA_ID');
  AddColunaSL('NOME_TIPO_COBRANCA');
  AddColunaSL('TIPO_CARTAO');
  AddColunaSL('TIPO_RECEBIMENTO_CARTAO');
  AddColuna('TIPO_RECEB_CARTAO');
end;

function TContasRecBaixasPagamentos.getRecordContasRecBaixasPagamentos: RecTitulosFinanceiros;
begin
  Result.Id                    := getInt('BAIXA_ID', True);
  Result.CobrancaId            := getInt('COBRANCA_ID', True);
  Result.ItemId                := getInt('ITEM_ID', True);
  Result.Tipo                  := getString('TIPO');
  Result.Parcela               := getInt('PARCELA');
  Result.NumeroParcelas        := getInt('NUMERO_PARCELAS');
  Result.DataVencimento        := getData('DATA_VENCIMENTO');
  Result.NsuTef                := getString('NSU_TEF');
  Result.CodigoAutorizacao     := getString('CODIGO_AUTORIZACAO');
  Result.NumeroCartao          := getString('NUMERO_CARTAO');
  Result.Valor                 := getDouble('VALOR');
  Result.NomeCobranca          := getString('NOME_TIPO_COBRANCA');
  Result.TipoCartao            := getString('TIPO_CARTAO');
  Result.TipoRecebimentoCartao := getString('TIPO_RECEBIMENTO_CARTAO');
  Result.TipoRecebCartao      := getString('TIPO_RECEB_CARTAO'); // Tipo de recebimento de fato que ocorreu no momento do recebimento
end;

function BuscarContasRecBaixasPagamentos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;
var
  i: Integer;
  t: TContasRecBaixasPagamentos;
begin
  Result := nil;
  t := TContasRecBaixasPagamentos.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordContasRecBaixasPagamentos;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
