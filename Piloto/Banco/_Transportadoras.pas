unit _Transportadoras;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros, _Cadastros, _CadastrosTelefones, _DiversosEnderecos;

{$M+}
type
  RecTransportadoras = class
  public
    Cadastro: RecCadastros;

    CadastroId: Integer;
    DataHoraCadastro: TDateTime;
  end;

  TTransportadoras = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao; pSomenteAtivos: Boolean);
  protected
    function GetRecordTransportadora: RecTransportadoras;
  end;

function AtualizarTransportadora(
  pConexao: TConexao;
  pPesquisouCadastro: Boolean;
  pCadastroId: Integer;
  pCadastro: RecCadastros;
  pTelefones: TArray<RecCadastrosTelefones>;
  pEnderecos: TArray<RecDiversosEnderecos>
): RecRetornoBD;

function BuscarTransportadoras(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecTransportadoras>;

function ExcluirTransportadora(
  pConexao: TConexao;
  pCadastroId: Integer
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TTransportadoras }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 4);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'and TRA.CADASTRO_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome fantasia',
      False,
      0,
      'and CAD.NOME_FANTASIA like :P1 || ''%'' ' +
      'order by ' +
      '  CAD.NOME_FANTASIA '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Raz�o social',
      True,
      1,
      'and CAD.RAZAO_SOCIAL like :P1 || ''%'' ' +
      'order by ' +
      '  CAD.RAZAO_SOCIAL '
    );

  Result[3] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'CPF/CNPJ',
      False,
      2,
      'and CAD.CPF_CNPJ = :P1 ' +
      'order by ' +
      '  CAD.CPF_CNPJ '
    );
end;

constructor TTransportadoras.Create(pConexao: TConexao; pSomenteAtivos: Boolean);
begin
  inherited Create(pConexao, 'TRANSPORTADORAS');

  FSql :=
    'select ' +
    '  TRA.CADASTRO_ID, ' +
    '  TRA.DATA_HORA_CADASTRO, ' +
    // Cadastro
    '  CAD.NOME_FANTASIA, ' +
    '  CAD.RAZAO_SOCIAL, ' +
    '  CAD.TIPO_PESSOA, ' +
    '  CAD.CPF_CNPJ, ' +
    '  CAD.LOGRADOURO, ' +
    '  CAD.COMPLEMENTO, ' +
    '  CAD.NUMERO, ' +
    '  CAD.BAIRRO_ID, ' +
    '  CAD.PONTO_REFERENCIA, ' +
    '  CAD.CEP, ' +
    '  CAD.E_MAIL, ' +
    '  CAD.SEXO, ' +
    '  CAD.ESTADO_CIVIL, ' +
    '  CAD.DATA_NASCIMENTO, ' +
    '  CAD.DATA_CADASTRO, ' +
    '  CAD.INSCRICAO_ESTADUAL, ' +
    '  CAD.CNAE, ' +
    '  CAD.RG, ' +
    '  CAD.ORGAO_EXPEDIDOR_RG, ' +
    '  CAD.E_CLIENTE, ' +
    '  CAD.E_FORNECEDOR, ' +
    '  CAD.E_MOTORISTA, ' +
    '  CAD.ATIVO as CADASTRO_ATIVO, ' +
    '  CID.ESTADO_ID ' +
    'from ' +
    '  TRANSPORTADORAS TRA ' +

    'inner join CADASTROS CAD ' +
    'on TRA.CADASTRO_ID = CAD.CADASTRO_ID ' +

    'inner join BAIRROS BAI ' +
    'on CAD.BAIRRO_ID = BAI.BAIRRO_ID ' +

    'inner join CIDADES CID ' +
    'on BAI.CIDADE_ID = CID.CIDADE_ID ';

  if pSomenteAtivos then
    FSql := FSql + ' where CAD.ATIVO = ''S'''
  else
    FSql := FSql + ' where CAD.ATIVO in(''S'', ''N'') ';

  SetFiltros(GetFiltros);

  AddColuna('CADASTRO_ID', True);
  AddColunaSL('DATA_HORA_CADASTRO');
  // CADASTRO
  AddColunaSL('NOME_FANTASIA');
  AddColunaSL('RAZAO_SOCIAL');
  AddColunaSL('TIPO_PESSOA');
  AddColunaSL('CPF_CNPJ');
  AddColunaSL('LOGRADOURO');
  AddColunaSL('COMPLEMENTO');
  AddColunaSL('NUMERO');
  AddColunaSL('BAIRRO_ID');
  AddColunaSL('CEP');
  AddColunaSL('PONTO_REFERENCIA');
  AddColunaSL('E_MAIL');
  AddColunaSL('SEXO');
  AddColunaSL('ESTADO_CIVIL');
  AddColunaSL('DATA_NASCIMENTO');
  AddColunaSL('DATA_CADASTRO');
  AddColunaSL('INSCRICAO_ESTADUAL');
  AddColunaSL('CNAE');
  AddColunaSL('RG');
  AddColunaSL('ORGAO_EXPEDIDOR_RG');
  AddColunaSL('E_CLIENTE');
  AddColunaSL('E_FORNECEDOR');
  AddColunaSL('E_MOTORISTA');
  AddColunaSL('CADASTRO_ATIVO');
  AddColunaSL('ESTADO_ID');
end;

function TTransportadoras.GetRecordTransportadora: RecTransportadoras;
begin
  Result := RecTransportadoras.Create;
  Result.Cadastro := RecCadastros.Create;

  Result.CadastroId       := getInt('CADASTRO_ID', True);
  Result.DataHoraCadastro := GetData('DATA_HORA_CADASTRO');

  // Cadastro
  Result.Cadastro.cadastro_id        := GetInt('CADASTRO_ID', True);
  Result.Cadastro.razao_social       := getString('RAZAO_SOCIAL');
  Result.Cadastro.nome_fantasia      := getString('NOME_FANTASIA');
  Result.Cadastro.tipo_pessoa        := getString('TIPO_PESSOA');
  Result.Cadastro.cpf_cnpj           := getString('CPF_CNPJ');
  Result.Cadastro.logradouro         := getString('LOGRADOURO');
  Result.Cadastro.complemento        := getString('COMPLEMENTO');
  Result.Cadastro.numero             := getString('NUMERO');
  Result.Cadastro.bairro_id          := getInt('BAIRRO_ID');
  Result.Cadastro.ponto_referencia   := getString('PONTO_REFERENCIA');
  Result.Cadastro.cep                := getString('CEP');
  Result.Cadastro.e_mail             := getString('E_MAIL');
  Result.Cadastro.sexo               := getString('SEXO');
  Result.Cadastro.estado_civil       := getString('ESTADO_CIVIL');
  Result.Cadastro.data_nascimento    := getData('DATA_NASCIMENTO');
  Result.Cadastro.data_cadastro      := GetData('DATA_CADASTRO');
  Result.Cadastro.inscricao_estadual := getString('INSCRICAO_ESTADUAL');
  Result.Cadastro.cnae               := getString('CNAE');
  Result.Cadastro.rg                 := getString('RG');
  Result.Cadastro.orgao_expedidor_rg := getString('ORGAO_EXPEDIDOR_RG');
  Result.Cadastro.e_cliente          := getString('E_CLIENTE');
  Result.Cadastro.e_fornecedor       := getString('E_FORNECEDOR');
  Result.Cadastro.ativo              := getString('CADASTRO_ATIVO');
  Result.Cadastro.estado_id          := getString('ESTADO_ID');
end;

function AtualizarTransportadora(
  pConexao: TConexao;
  pPesquisouCadastro: Boolean;
  pCadastroId: Integer;
  pCadastro: RecCadastros;
  pTelefones: TArray<RecCadastrosTelefones>;
  pEnderecos: TArray<RecDiversosEnderecos>
): RecRetornoBD;
var
  vTra: TTransportadoras;
  vNovo: Boolean;
  vCad: TCadastro;
  vRetBanco: RecRetornoBD;
begin
  Result.Iniciar;
  vNovo := pCadastroId = 0;
  vTra := TTransportadoras.Create(pConexao, False);
  vCad := TCadastro.Create(pConexao);

  if vNovo then begin
    pCadastroId := TSequencia.Create(pConexao, 'SEQ_CADASTRO_ID').GetProximaSequencia;
    Result.AsInt := pCadastroId;
    pCadastro.cadastro_id := pCadastroId;
  end;

  try
    pConexao.IniciarTransacao;

    vCad.setCadastro(pCadastro);
    if vNovo then
      vCad.Inserir
    else
      vCad.Atualizar;

    vTra.setInt('CADASTRO_ID', pCadastroId, True);

    if vNovo or pPesquisouCadastro then
      vTra.Inserir
    else
      vTra.Atualizar;

    vRetBanco := _CadastrosTelefones.AtualizarTelefones(pConexao, pCadastroId, pTelefones);
    if vRetBanco.TeveErro then
      raise Exception.Create(vRetBanco.MensagemErro);

    if pEnderecos <> nil then begin
      vRetBanco := _DiversosEnderecos.AtualizarDiversosEndereco(pConexao, pCadastroId, pEnderecos);

      if vRetBanco.TeveErro then
        raise Exception.Create(vRetBanco.MensagemErro);
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vCad.Free;
  vTra.Free;
end;

function BuscarTransportadoras(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecTransportadoras>;
var
  i: Integer;
  t: TTransportadoras;
begin
  Result := nil;
  t := TTransportadoras.Create(pConexao, False);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordTransportadora;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirTransportadora(
  pConexao: TConexao;
  pCadastroId: Integer
): RecRetornoBD;
var
  ex: TExecucao;
  t: TTransportadoras;
begin
  Result.TeveErro := False;
  t := TTransportadoras.Create(pConexao, False);
  ex := TExecucao.Create(pConexao);

  t.SetInt('CADASTRO_ID', pCadastroId, True);

  try
    pConexao.IniciarTransacao;

    ex.SQL.Add('delete from DIVERSOS_CONTATOS where CADASTRO_ID = :P1');
    ex.Executar([pCadastroId]);

    ex.SQL.Clear;
    ex.SQL.Add('delete from DIVERSOS_ENDERECOS where CADASTRO_ID = :P1');
    ex.Executar([pCadastroId]);

    t.Excluir;
    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
