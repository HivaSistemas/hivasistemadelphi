unit _EntradasNfServicosFinanc;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsFinanceiros;

{$M+}
type

  TEntradasNfServicosFinanc = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordEntradasNfServicosFinanc: RecTitulosFinanceiros;
  end;

function BuscarEntradasNfServicosFinanc(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;


function getFiltros: TArray<RecFiltros>;

implementation

{ TEntradasNfServicosFinanc }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ENTRADA_ID = :P1 '
    );
end;

constructor TEntradasNfServicosFinanc.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ENTRADAS_NF_SERVICOS_FINANC');

  FSql := 
    'select ' +
    '  ENTRADA_ID, ' +
    '  ITEM_ID, ' +
    '  COBRANCA_ID, ' +
    '  DATA_VENCIMENTO, ' +
    '  PARCELA, ' +
    '  NUMERO_PARCELAS, ' +
    '  VALOR_DOCUMENTO, ' +
    '  NOSSO_NUMERO, ' +
    '  CODIGO_BARRAS, ' +
    '  DOCUMENTO ' +
    'from ' +
    '  ENTRADAS_NF_SERVICOS_FINANC ';

  setFiltros(getFiltros);

  AddColuna('ENTRADA_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('COBRANCA_ID');
  AddColuna('DATA_VENCIMENTO');
  AddColuna('PARCELA');
  AddColuna('NUMERO_PARCELAS');
  AddColuna('VALOR_DOCUMENTO');
  AddColuna('NOSSO_NUMERO');
  AddColuna('CODIGO_BARRAS');
  AddColuna('DOCUMENTO');
end;

function TEntradasNfServicosFinanc.getRecordEntradasNfServicosFinanc: RecTitulosFinanceiros;
begin
  Result.Id                         := getInt('ENTRADA_ID', True);
  Result.ItemId                     := getInt('ITEM_ID', True);
  Result.CobrancaId                 := getInt('COBRANCA_ID');
  Result.DataVencimento             := getData('DATA_VENCIMENTO');
  Result.Parcela                    := getInt('PARCELA');
  Result.NumeroParcelas             := getInt('NUMERO_PARCELAS');
  Result.Valor                      := getDouble('VALOR_DOCUMENTO');
  Result.NossoNumero                := getString('NOSSO_NUMERO');
  Result.CodigoBarras               := getString('CODIGO_BARRAS');
  Result.Documento                  := getString('DOCUMENTO');
end;

function BuscarEntradasNfServicosFinanc(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;
var
  i: Integer;
  t: TEntradasNfServicosFinanc;
begin
  Result := nil;
  t := TEntradasNfServicosFinanc.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordEntradasNfServicosFinanc;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
