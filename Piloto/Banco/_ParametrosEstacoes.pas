unit _ParametrosEstacoes;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, System.SysUtils;

{$M+}
type
  RecParametrosEstacoes = record
    Estacao: string;
    UtilizarTef: string;
    GerenciadorCartaoId: Integer;
    DiretorioArquivosRequisicao: string;
    DiretorioArquivosResposta: string;
  end;


  TParametrosEstacoes = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordParametro: RecParametrosEstacoes;
  end;

function AtualizarParametro(
  pConexao: TConexao;
  pEstacao: string;
  pUtilizarTef: string;
  pGerenciadorCartaoId: Integer
): RecRetornoBD;

function BuscarParametros(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecParametrosEstacoes>;

function ExcluirParametro(
  pConexao: TConexao;
  pEstacao: string
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TParametrosEstacoes }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ESTACAO = :P1 '
    );
end;

constructor TParametrosEstacoes.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PARAMETROS_ESTACOES');

  FSql :=
    'select ' +
    '  PAE.ESTACAO, ' +
    '  PAE.UTILIZAR_TEF, ' +
    '  PAE.GERENCIADOR_CARTAO_ID, ' +
    '  GER.DIRETORIO_ARQUIVOS_REQUISICOES, ' +
    '  GER.DIRETORIO_ARQUIVOS_RESPOSTAS ' +
    'from ' +
    '  PARAMETROS_ESTACOES PAE ' +

    'left join GERENCIADORES_CARTOES_TEF GER ' +
    'on PAE.GERENCIADOR_CARTAO_ID = GER.GERENCIADOR_CARTAO_ID ';

  SetFiltros(GetFiltros);

  AddColuna('ESTACAO', True);
  AddColuna('UTILIZAR_TEF');
  AddColuna('GERENCIADOR_CARTAO_ID');
  AddColunaSL('DIRETORIO_ARQUIVOS_REQUISICOES');
  AddColunaSL('DIRETORIO_ARQUIVOS_RESPOSTAS');
end;

function TParametrosEstacoes.GetRecordParametro: RecParametrosEstacoes;
begin
  Result.Estacao             := getString('ESTACAO', True);
  Result.UtilizarTef         := getString('UTILIZAR_TEF');
  Result.GerenciadorCartaoId := getInt('GERENCIADOR_CARTAO_ID');
  Result.DiretorioArquivosRequisicao := getString('DIRETORIO_ARQUIVOS_REQUISICOES');
  Result.DiretorioArquivosResposta   := getString('DIRETORIO_ARQUIVOS_RESPOSTAS');
end;

function AtualizarParametro(
  pConexao: TConexao;
  pEstacao: string;
  pUtilizarTef: string;
  pGerenciadorCartaoId: Integer
): RecRetornoBD;
var
  vPar: TParametrosEstacoes;
  vSql: TConsulta;
  vNovo: Boolean;
begin
  Result.Iniciar;
  vPar := TParametrosEstacoes.Create(pConexao);
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select count(ESTACAO) from PARAMETROS_ESTACOES where ESTACAO = :P1');
  vSql.Pesquisar([pEstacao]);

  vNovo := vSql.GetInt(0) = 0;

  vSql.Active := False;
  vSql.Free;

  vPar.setString('ESTACAO', pEstacao, True);
  vPar.setString('UTILIZAR_TEF', pUtilizarTef);
  vPar.setIntN('GERENCIADOR_CARTAO_ID', pGerenciadorCartaoId);

  try
    pConexao.IniciarTransacao;

    if vNovo then
      vPar.Inserir
    else
      vPar.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vPar.Free;
end;

function BuscarParametros(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecParametrosEstacoes>;
var
  i: Integer;
  t: TParametrosEstacoes;
begin
  Result := nil;
  t := TParametrosEstacoes.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordParametro;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirParametro(
  pConexao: TConexao;
  pEstacao: string
): RecRetornoBD;
var
  t: TParametrosEstacoes;
begin
  Result.Iniciar;
  t := TParametrosEstacoes.Create(pConexao);

  t.SetString('ESTACAO', pEstacao, True);

  try
    pConexao.IniciarTransacao;

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
