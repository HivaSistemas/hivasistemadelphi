unit _ContasCustodia;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecContasCustodia = class
  public
    ContaCustodiaId: Integer;
    Nome: string;
    Ativo: string;
  end;

  TContasCustodia = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordContasCustodia: RecContasCustodia;
  end;

function AtualizarContasCustodia(
  pConexao: TConexao;
  pContaCustodiaId: Integer;
  pNome: string;
  pAtivo: string
): RecRetornoBD;

function BuscarContasCustodia(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecContasCustodia>;

function ExcluirContasCustodia(
  pConexao: TConexao;
  pContaCustodiaId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TContasCustodia }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CONTA_CUSTODIA_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome da conta ( Avan�ado )',
      True,
      0,
      'where NOME like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  NOME ',
      '',
      True
    );
end;

constructor TContasCustodia.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONTAS_CUSTODIA');

  FSql := 
    'select ' +
    '  CONTA_CUSTODIA_ID, ' +
    '  NOME, ' +
    '  ATIVO ' +
    'from ' +
    '  CONTAS_CUSTODIA';

  setFiltros(getFiltros);

  AddColuna('CONTA_CUSTODIA_ID', True);
  AddColuna('NOME');
  AddColuna('ATIVO');
end;

function TContasCustodia.getRecordContasCustodia: RecContasCustodia;
begin
  Result                            := RecContasCustodia.Create;

  Result.ContaCustodiaId            := getInt('CONTA_CUSTODIA_ID', True);
  Result.Nome                       := getString('NOME');
  Result.Ativo                      := getString('ATIVO');
end;

function AtualizarContasCustodia(
  pConexao: TConexao;
  pContaCustodiaId: Integer;
  pNome: string;
  pAtivo: string
): RecRetornoBD;
var
  t: TContasCustodia;
  vNovo: Boolean;
  vSeq: TSequencia;
begin
  Result.Iniciar;
  pConexao.setRotina('ATUALIZAR_CONTA_CUSTODIA');

  t := TContasCustodia.Create(pConexao);

  vNovo := pContaCustodiaId = 0;
  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_CONTAS_CUSTODIA');
    pContaCustodiaId := vSeq.getProximaSequencia;
    Result.AsInt := pContaCustodiaId;
    vSeq.Free;
  end;

  try
    pConexao.IniciarTransacao; 

    t.setInt('CONTA_CUSTODIA_ID', pContaCustodiaId, True);
    t.setString('NOME', pNome);
    t.setString('ATIVO', pAtivo);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao; 
  except
    on e: Exception do begin
      pConexao.VoltarTransacao; 
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarContasCustodia(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecContasCustodia>;
var
  i: Integer;
  t: TContasCustodia;
  vSql: string;
begin
  Result := nil;
  t := TContasCustodia.Create(pConexao);

  vSql := '';
  if pSomenteAtivos then
    vSql := ' and ATIVO = ''S'' ';

  if t.Pesquisar(pIndice, pFiltros, vSql) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordContasCustodia;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirContasCustodia(
  pConexao: TConexao;
  pContaCustodiaId: Integer
): RecRetornoBD;
var
  t: TContasCustodia;
begin
  Result.Iniciar;
  t := TContasCustodia.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('CONTA_CUSTODIA_ID', pContaCustodiaId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
