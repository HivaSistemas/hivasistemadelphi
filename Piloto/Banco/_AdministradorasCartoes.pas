unit _AdministradorasCartoes;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecAdministradorasCartoes = class(TObject)
  public
    AdministradoraId: Integer;
    Descricao: string;
    Cnpj: string;
    Ativo: string;
  end;

  TAdministradorasCartoes = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordAdministradorasCartoes: RecAdministradorasCartoes;
  end;

function AtualizarAdministradorasCartoes(
  pConexao: TConexao;
  pAdministradoraId: Integer;
  pDescricao: string;
  pCnpj: string;
  pAtivo: string
): RecRetornoBD;

function BuscarAdministradorasCartoes(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecAdministradorasCartoes>;

function ExcluirAdministradorasCartoes(
  pConexao: TConexao;
  pAdministradoraId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TAdministradorasCartoes }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ADMINISTRADORA_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Descri��o (Avan�ada)',
      True,
      0,
      'where DESCRICAO like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  DESCRICAO ',
      '',
      True
    );
end;

constructor TAdministradorasCartoes.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ADMINISTRADORAS_CARTOES');

  FSql := 
    'select ' +
    '  ADMINISTRADORA_ID, ' +
    '  DESCRICAO, ' +
    '  CNPJ, ' +
    '  ATIVO ' +
    'from ' +
    '  ADMINISTRADORAS_CARTOES ';

  setFiltros(getFiltros);

  AddColuna('ADMINISTRADORA_ID', True);
  AddColuna('DESCRICAO');
  AddColuna('CNPJ');
  AddColuna('ATIVO');
end;

function TAdministradorasCartoes.getRecordAdministradorasCartoes: RecAdministradorasCartoes;
begin
  Result := RecAdministradorasCartoes.Create;

  Result.AdministradoraId           := getInt('ADMINISTRADORA_ID', True);
  Result.Descricao                  := getString('DESCRICAO');
  Result.Cnpj                       := getString('CNPJ');
  Result.Ativo                      := getString('ATIVO');
end;

function AtualizarAdministradorasCartoes(
  pConexao: TConexao;
  pAdministradoraId: Integer;
  pDescricao: string;
  pCnpj: string;
  pAtivo: string
): RecRetornoBD;
var
  t: TAdministradorasCartoes;

  vNovo: Boolean;
  vSeq: TSequencia;
begin
  Result.Iniciar;
  pConexao.setRotina('ATUALIZAR_ADMINISTRADORA_CARTAO');

  t := TAdministradorasCartoes.Create(pConexao);

  vNovo := pAdministradoraId = 0;
  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_ADMINISTRADORAS_CARTOES');
    pAdministradoraId := vSeq.getProximaSequencia;
    Result.AsInt := pAdministradoraId;
    vSeq.Free;
  end;

  try
    pConexao.IniciarTransacao; 

    t.setInt('ADMINISTRADORA_ID', pAdministradoraId, True);
    t.setString('DESCRICAO', pDescricao);
    t.setString('CNPJ', pCnpj);
    t.setString('ATIVO', pAtivo);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao; 
  except
    on e: Exception do begin
      pConexao.VoltarTransacao; 
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarAdministradorasCartoes(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecAdministradorasCartoes>;
var
  i: Integer;
  t: TAdministradorasCartoes;

  vSql: string;
begin
  Result := nil;
  t := TAdministradorasCartoes.Create(pConexao);

  vSql := '';
  if pSomenteAtivos then
    vSql := 'and ATIVO = ''S'' ';

  if t.Pesquisar(pIndice, pFiltros, vSql) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordAdministradorasCartoes;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirAdministradorasCartoes(
  pConexao: TConexao;
  pAdministradoraId: Integer
): RecRetornoBD;
var
  t: TAdministradorasCartoes;
begin
  Result.Iniciar;
  t := TAdministradorasCartoes.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('ADMINISTRADORA_ID', pAdministradoraId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
