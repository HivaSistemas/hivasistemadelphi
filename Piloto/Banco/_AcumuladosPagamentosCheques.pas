unit _AcumuladosPagamentosCheques;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsFinanceiros;

{$M+}
type
  RecAcumuladosPagamentosCheques = record
    AcumuladoId: Integer;
    CobrancaId: Integer;
    ItemId: Integer;
    DataVencimento: TDateTime;
    Banco: string;
    Agencia: string;
    ContaCorrente: string;
    NumeroCheque: Integer;
    ValorCheque: Double;
    NomeEmitente: string;
    TelefoneEmitente: string;
    Parcela: Integer;
    NumeroParcelas: Integer;
  end;

  TAcumuladosPagamentosCheques = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordAcumuladosPagamentosCheques: RecTitulosFinanceiros;
  end;

function BuscarAcumuladosPagamentosCheques(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TAcumuladosPagamentosCheques }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ACUMULADO_ID = :P1'
    );
end;

constructor TAcumuladosPagamentosCheques.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ACUMULADOS_PAGAMENTOS_CHEQUES');

  FSql := 
    'select ' +
    '  ACUMULADO_ID, ' +
    '  COBRANCA_ID, ' +
    '  ITEM_ID, ' +
    '  DATA_VENCIMENTO, ' +
    '  BANCO, ' +
    '  AGENCIA, ' +
    '  CONTA_CORRENTE, ' +
    '  NUMERO_CHEQUE, ' +
    '  VALOR_CHEQUE, ' +
    '  NOME_EMITENTE, ' +
    '  CPF_CNPJ_EMITENTE, ' +
    '  TELEFONE_EMITENTE, ' +
    '  PARCELA, ' +
    '  NUMERO_PARCELAS ' +
    'from ' +
    '  ACUMULADOS_PAGAMENTOS_CHEQUES';

  setFiltros(getFiltros);

  AddColuna('ACUMULADO_ID', True);
  AddColuna('COBRANCA_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('DATA_VENCIMENTO');
  AddColuna('BANCO');
  AddColuna('AGENCIA');
  AddColuna('CONTA_CORRENTE');
  AddColuna('NUMERO_CHEQUE');
  AddColuna('VALOR_CHEQUE');
  AddColuna('NOME_EMITENTE');
  AddColuna('CPF_CNPJ_EMITENTE');
  AddColuna('TELEFONE_EMITENTE');
  AddColuna('PARCELA');
  AddColuna('NUMERO_PARCELAS');
end;

function TAcumuladosPagamentosCheques.getRecordAcumuladosPagamentosCheques: RecTitulosFinanceiros;
begin
  Result.Id               := getInt('ACUMULADO_ID', True);
  Result.CobrancaId       := getInt('COBRANCA_ID', True);
  Result.ItemId           := getInt('ITEM_ID', True);
  Result.DataVencimento   := getData('DATA_VENCIMENTO');
  Result.Banco            := getString('BANCO');
  Result.Agencia          := getString('AGENCIA');
  Result.ContaCorrente    := getString('CONTA_CORRENTE');
  Result.NumeroCheque     := getInt('NUMERO_CHEQUE');
  Result.Valor            := getDouble('VALOR_CHEQUE');
  Result.NomeEmitente     := getString('NOME_EMITENTE');
  Result.CpfCnpjEmitente  := getString('CPF_CNPJ_EMITENTE');
  Result.TelefoneEmitente := getString('TELEFONE_EMITENTE');
  Result.Parcela          := getInt('PARCELA');
  Result.NumeroParcelas   := getInt('NUMERO_PARCELAS');
end;

function BuscarAcumuladosPagamentosCheques(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;
var
  i: Integer;
  t: TAcumuladosPagamentosCheques;
begin
  Result := nil;
  t := TAcumuladosPagamentosCheques.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordAcumuladosPagamentosCheques;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
