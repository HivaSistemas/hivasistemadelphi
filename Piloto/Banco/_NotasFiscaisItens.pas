unit _NotasFiscaisItens;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsNotasFiscais,
  System.Variants;

{$M+}
type
  TNotasFiscaisItens = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordNotasFiscaisItens: RecNotaFiscalItem;
  end;

function BuscarNotasFiscaisItens(
  pConexao: TConexao;
  pIndice: Byte;
  pFiltros: array of Variant
): TArray<RecNotaFiscalItem>;

function BuscarNotasFiscaisItensComando(pConexao: TConexao; pComando: string): TArray<RecNotaFiscalItem>;

function AtualizarItensEdicaoNota(pConexao: TConexao; const pNotaFiscalId: Integer; pItens: TArray<RecNotaFiscalItem>): RecRetornoBD;
function BuscarProdutosImpressaoDANFENFCe(pConexao: TConexao; pNotaFiscalId: Integer): TArray<RecImpressaoDANFENFCe>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TNotasFiscaisItens }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ITE.NOTA_FISCAL_ID = :P1 ORDER BY ITE.ITEM_ID '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where NOF.ORCAMENTO_ID = :P1'
    );
end;

constructor TNotasFiscaisItens.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'NOTAS_FISCAIS_ITENS');

  FSql :=
    'select ' +
    '  ITE.CFOP_ID, ' +
    '  ITE.ITEM_ID, ' +
    '  ITE.PRODUTO_ID, ' +
    '  ITE.NOTA_FISCAL_ID, ' +
    '  ITE.CODIGO_NCM, ' +
    '  ITE.PERCENTUAL_COFINS, ' +
    '  ITE.VALOR_COFINS, ' +
    '  ITE.IVA, ' +
    '  ITE.PRECO_PAUTA, ' +
    '  ITE.VALOR_IPI, ' +
    '  ITE.PERCENTUAL_IPI, ' +
    '  ITE.BASE_CALCULO_COFINS, ' +
    '  ITE.VALOR_PIS, ' +
    '  ITE.PERCENTUAL_PIS, ' +
    '  ITE.BASE_CALCULO_ICMS_ST, ' +
    '  ITE.VALOR_ICMS_ST, ' +
    '  ITE.BASE_CALCULO_PIS, ' +
    '  ITE.VALOR_ICMS, ' +
    '  ITE.PERCENTUAL_ICMS, ' +
    '  ITE.BASE_CALCULO_ICMS, ' +
    '  ITE.VALOR_TOTAL, ' +
    '  ITE.PRECO_UNITARIO, ' +
    '  ITE.QUANTIDADE, ' +
    '  ITE.VALOR_TOTAL_DESCONTO, ' +
    '  ITE.VALOR_TOTAL_OUTRAS_DESPESAS, ' +
    '  ITE.INDICE_REDUCAO_BASE_ICMS, ' +
    '  ITE.CST, ' +
    '  ITE.CODIGO_BARRAS, ' +
    '  ITE.NOME_PRODUTO, ' +
    '  ITE.UNIDADE, ' +
    '  ITE.ORIGEM_PRODUTO, ' +
    '  ITE.INDICE_REDUCAO_BASE_ICMS_ST, ' +
    '  ITE.PERCENTUAL_ICMS_ST, ' +
    '  ITE.INFORMACOES_ADICIONAIS, ' +
    '  ITE.CST_PIS, ' +
    '  ITE.CST_COFINS, ' +
    '  NOF.ORCAMENTO_ID, ' +
    '  ITE.CEST, ' +
    '  ITE.CODIGO_PRODUTO_NFE, ' +
    '  PRO.TIPO_CONTROLE_ESTOQUE, ' +
    '  ITE.NUMERO_ITEM_PEDIDO, ' +
    '  ITE.NUMERO_PEDIDO, ' +
    '  ITE.VALOR_ICMS_INTER, ' +
    '  ITE.BASE_CALCULO_ICMS_INTER, ' +
    '  ITE.PERCENTUAL_ICMS_INTER ' +
    'from ' +
    '  NOTAS_FISCAIS_ITENS ITE ' +

    'inner join NOTAS_FISCAIS NOF ' +
    'on ITE.NOTA_FISCAL_ID = NOF.NOTA_FISCAL_ID ' +

    'inner join PRODUTOS PRO ' +
    'on ITE.PRODUTO_ID = PRO.PRODUTO_ID ';

  setFiltros(getFiltros);

  AddColuna('NOTA_FISCAL_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('PRODUTO_ID', True);
  AddColuna('NOME_PRODUTO');
  AddColuna('CFOP_ID');
  AddColuna('CODIGO_NCM');
  AddColunaSL('ORCAMENTO_ID');
  AddColuna('PERCENTUAL_COFINS');
  AddColuna('VALOR_COFINS');
  AddColuna('IVA');
  AddColuna('PRECO_PAUTA');
  AddColuna('VALOR_IPI');
  AddColuna('PERCENTUAL_IPI');
  AddColuna('BASE_CALCULO_COFINS');
  AddColuna('VALOR_PIS');
  AddColuna('PERCENTUAL_PIS');
  AddColuna('BASE_CALCULO_ICMS_ST');
  AddColuna('VALOR_ICMS_ST');
  AddColuna('BASE_CALCULO_PIS');
  AddColuna('VALOR_ICMS');
  AddColuna('PERCENTUAL_ICMS');
  AddColuna('BASE_CALCULO_ICMS');
  AddColuna('VALOR_TOTAL');
  AddColuna('PRECO_UNITARIO');
  AddColuna('QUANTIDADE');
  AddColuna('VALOR_TOTAL_DESCONTO');
  AddColuna('VALOR_TOTAL_OUTRAS_DESPESAS');
  AddColuna('INDICE_REDUCAO_BASE_ICMS');
  AddColuna('CST');
  AddColuna('CODIGO_BARRAS');
  AddColuna('ORIGEM_PRODUTO');
  AddColuna('UNIDADE');
  AddColuna('INDICE_REDUCAO_BASE_ICMS_ST');
  AddColuna('PERCENTUAL_ICMS_ST');
  AddColuna('INFORMACOES_ADICIONAIS');
  AddColuna('CST_PIS');
  AddColuna('CST_COFINS');
  AddColuna('CEST');
  AddColuna('CODIGO_PRODUTO_NFE');
  AddColunaSL('TIPO_CONTROLE_ESTOQUE');
  AddColuna('NUMERO_ITEM_PEDIDO');
  AddColuna('NUMERO_PEDIDO');
  AddColunaSL('VALOR_ICMS_INTER');
  AddColunaSL('BASE_CALCULO_ICMS_INTER');
  AddColunaSL('PERCENTUAL_ICMS_INTER');
end;

function TNotasFiscaisItens.getRecordNotasFiscaisItens: RecNotaFiscalItem;
begin
  Result.nota_fiscal_id              := getInt('NOTA_FISCAL_ID', True);
  Result.produto_id                  := getInt('PRODUTO_ID', True);
  Result.item_id                     := getInt('ITEM_ID', True);
  Result.CfopId                      := getString('CFOP_ID');
  Result.codigo_ncm                  := getString('CODIGO_NCM');
  Result.cst_cofins                  := getString('CST_COFINS');
  Result.percentual_cofins           := getDouble('PERCENTUAL_COFINS');
  Result.valor_cofins                := getDouble('VALOR_COFINS');
  Result.iva                         := getDouble('IVA');
  Result.preco_pauta                 := getDouble('PRECO_PAUTA');
  Result.valor_ipi                   := getDouble('VALOR_IPI');
  Result.percentual_ipi              := getDouble('PERCENTUAL_IPI');
  Result.base_calculo_cofins         := getDouble('BASE_CALCULO_COFINS');
  Result.cst_pis                     := getString('CST_PIS');
  Result.valor_pis                   := getDouble('VALOR_PIS');
  Result.percentual_pis              := getDouble('PERCENTUAL_PIS');
  Result.base_calculo_icms_st        := getDouble('BASE_CALCULO_ICMS_ST');
  Result.valor_icms_st               := getDouble('VALOR_ICMS_ST');
  Result.base_calculo_pis            := getDouble('BASE_CALCULO_PIS');
  Result.valor_icms                  := getDouble('VALOR_ICMS');
  Result.percentual_icms             := getDouble('PERCENTUAL_ICMS');
  Result.base_calculo_icms           := getDouble('BASE_CALCULO_ICMS');
  Result.valor_total                 := getDouble('VALOR_TOTAL');
  Result.preco_unitario              := getDouble('PRECO_UNITARIO');
  Result.quantidade                  := getDouble('QUANTIDADE');
  Result.valor_total_desconto        := getDouble('VALOR_TOTAL_DESCONTO');
  Result.valor_total_outras_despesas := getDouble('VALOR_TOTAL_OUTRAS_DESPESAS');
  Result.indice_reducao_base_icms    := getDouble('INDICE_REDUCAO_BASE_ICMS');
  Result.cst                         := getString('CST');
  Result.codigo_barras               := getString('CODIGO_BARRAS');
  Result.origem_produto              := getInt('ORIGEM_PRODUTO');
  Result.nome_produto                := getString('NOME_PRODUTO');
  Result.unidade                     := getString('UNIDADE');
  Result.indice_reducao_base_icms_st := getDouble('INDICE_REDUCAO_BASE_ICMS_ST');
  Result.percentual_icms_st          := getDouble('PERCENTUAL_ICMS_ST');
  Result.informacoes_adicionais      := getString('INFORMACOES_ADICIONAIS');
  Result.cest                        := getString('CEST');
  Result.CodigoProdutoNFe            := getString('CODIGO_PRODUTO_NFE');
  Result.TipoControleEstoque         := getString('TIPO_CONTROLE_ESTOQUE');
  Result.numero_item_pedido          := getInt('NUMERO_ITEM_PEDIDO');
  Result.numero_pedido               := getString('NUMERO_PEDIDO');

  Result.valor_icms_inter            := getDouble('VALOR_ICMS_INTER');
  Result.base_calculo_icms_inter     := getDouble('BASE_CALCULO_ICMS_INTER');
  Result.percentual_icms_inter       := getDouble('PERCENTUAL_ICMS_INTER');
end;

function BuscarNotasFiscaisItens(
  pConexao: TConexao;
  pIndice: Byte;
  pFiltros: array of Variant
): TArray<RecNotaFiscalItem>;
var
  i: Integer;
  t: TNotasFiscaisItens;
begin
  Result := nil;
  t := TNotasFiscaisItens.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordNotasFiscaisItens;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarNotasFiscaisItensComando(pConexao: TConexao; pComando: string): TArray<RecNotaFiscalItem>;
var
  i: Integer;
  t: TNotasFiscaisItens;
begin
  Result := nil;
  t := TNotasFiscaisItens.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordNotasFiscaisItens;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function AtualizarItensEdicaoNota(pConexao: TConexao; const pNotaFiscalId: Integer; pItens: TArray<RecNotaFiscalItem>): RecRetornoBD;
var
  i: Integer;
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_NTF_ITENS');
  vExec := TExecucao.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    vExec.SQL.Clear;
    vExec.SQL.Add('update NOTAS_FISCAIS_ITENS set ');
    vExec.SQL.Add('  PRECO_UNITARIO = :P4, ');
    vExec.SQL.Add('  QUANTIDADE = :P5, ');
    vExec.SQL.Add('  VALOR_TOTAL = :P6, ');
    vExec.SQL.Add('  CODIGO_NCM = :P7, ');
    vExec.SQL.Add('  VALOR_TOTAL_DESCONTO = :P8, ');
    vExec.SQL.Add('  VALOR_TOTAL_OUTRAS_DESPESAS = :P9, ');
    vExec.SQL.Add('  BASE_CALCULO_ICMS = :P10, ');
    vExec.SQL.Add('  PERCENTUAL_ICMS = :P11, ');
    vExec.SQL.Add('  VALOR_ICMS = :P12, ');
    vExec.SQL.Add('  BASE_CALCULO_ICMS_ST = :P13, ');
    vExec.SQL.Add('  PERCENTUAL_ICMS_ST = :P14, ');
    vExec.SQL.Add('  VALOR_ICMS_ST = :P15, ');
    vExec.SQL.Add('  CST_PIS = :P16, ');
    vExec.SQL.Add('  BASE_CALCULO_PIS = :P17, ');
    vExec.SQL.Add('  PERCENTUAL_PIS = :P18, ');
    vExec.SQL.Add('  VALOR_PIS = :P19, ');
    vExec.SQL.Add('  CST_COFINS = :P20, ');
    vExec.SQL.Add('  BASE_CALCULO_COFINS = :P21, ');
    vExec.SQL.Add('  PERCENTUAL_COFINS = :P22, ');
    vExec.SQL.Add('  VALOR_COFINS = :P23, ');
    vExec.SQL.Add('  VALOR_IPI = :P24, ');
    vExec.SQL.Add('  PERCENTUAL_IPI = :P25, ');
    vExec.SQL.Add('  CODIGO_BARRAS = :P26, ');
    vExec.SQL.Add('  CEST = :P27, ');
    vExec.SQL.Add('  CFOP_ID = :P28 ');
    vExec.SQL.Add('where NOTA_FISCAL_ID = :P1 ');
    vExec.SQL.Add('and PRODUTO_ID = :P2 ');
    vExec.SQL.Add('and ITEM_ID = :P3 ');

    for i := Low(pItens) to High(pItens) do begin
      vExec.Executar([
        pNotaFiscalId,
        pItens[i].produto_id,
        pItens[i].item_id,
        pItens[i].preco_unitario,
        pItens[i].quantidade,
        pItens[i].valor_total,
        pItens[i].codigo_ncm,
        pItens[i].valor_total_desconto,
        pItens[i].valor_total_outras_despesas,
        pItens[i].base_calculo_icms,
        pItens[i].percentual_icms,
        pItens[i].valor_icms,
        pItens[i].base_calculo_icms_st,
        pItens[i].percentual_icms_st,
        pItens[i].valor_icms_st,
        pItens[i].cst_pis,
        pItens[i].base_calculo_pis,
        pItens[i].percentual_pis,
        pItens[i].valor_pis,
        pItens[i].cst_cofins,
        pItens[i].base_calculo_cofins,
        pItens[i].percentual_cofins,
        pItens[i].valor_cofins,
        pItens[i].valor_ipi,
        pItens[i].percentual_ipi,
        pItens[i].codigo_barras,
        pItens[i].cest,
        pItens[i].CfopId
      ]);
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
end;

function BuscarProdutosImpressaoDANFENFCe(pConexao: TConexao; pNotaFiscalId: Integer): TArray<RecImpressaoDANFENFCe>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  NIT.PRODUTO_ID, ');
  vSql.SQL.Add('  PRO.NOME, ');
  vSql.SQL.Add('  NIT.UNIDADE, ');
  vSql.SQL.Add('  NIT.PRECO_UNITARIO, ');
  vSql.SQL.Add('  NIT.QUANTIDADE, ');
  vSql.SQL.Add('  NIT.ITEM_ID ');
  vSql.SQL.Add('from ');
  vSql.SQL.Add('  NOTAS_FISCAIS_ITENS NIT ');

  vSql.SQL.Add('inner join PRODUTOS PRO ');
  vSql.SQL.Add('on NIT.PRODUTO_ID = PRO.PRODUTO_ID ');

  vSql.SQL.Add('where NIT.NOTA_FISCAL_ID = :P1 ');

  vSql.SQL.Add('order by ');
  vSql.SQL.Add('  NIT.ITEM_ID ');

  if vSql.Pesquisar([pNotaFiscalId]) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.getQuantidadeRegistros - 1 do begin
      Result[i].produtoId := vSql.GetInt('PRODUTO_ID');
      Result[i].nome := vSql.GetString('NOME');
      Result[i].unidade := vSql.GetString('UNIDADE');
      Result[i].quantidade := vSql.GetDouble('QUANTIDADE');
      Result[i].precoUnitario := vSql.GetDouble('PRECO_UNITARIO');
      Result[i].itemId := vSql.GetInt('ITEM_ID');

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

end.


//begin
//  EXECUTE IMMEDIATE 'ALTER TABLE NOTAS_FISCAIS_ITENS DISABLE ALL TRIGGERS';
//
//  EXECUTE IMMEDIATE 'ALTER TABLE NOTAS_FISCAIS_ITENS ADD VALOR_ICMS_INTER NUMBER(8,2) DEFAULT 0';
//
//  EXECUTE IMMEDIATE 'ALTER TABLE NOTAS_FISCAIS_ITENS ADD BASE_CALCULO_ICMS_INTER NUMBER(8,2) DEFAULT 0';
//
//  EXECUTE IMMEDIATE 'ALTER TABLE NOTAS_FISCAIS_ITENS ADD PERCENTUAL_ICMS_INTER NUMBER(4,2) DEFAULT 0';
//
//  EXECUTE IMMEDIATE 'ALTER TABLE NOTAS_FISCAIS_ITENS ENABLE ALL TRIGGERS';
//end;
