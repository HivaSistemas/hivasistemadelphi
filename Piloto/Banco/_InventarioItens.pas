unit _InventarioItens;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecInventarioItens = record
    InventarioId: Integer;
    ProdutoId: Integer;
    Estoque: Double;
    Quantidade: Double;
    Contou: String;
    LocalId: Integer;
  end;
  RecInventarioLancamentoItens = record
    InventarioId: Integer;
    ProdutoId: Integer;
    Estoque: Double;
    Quantidade: Double;
    NomeProduto: string;
    Lote: string;
    LocalId: Integer;
    MarcaId: Integer;
    NomeMarca: string;
    UnidadeVenda: string;
    Contou: String;
    CodigoBarra: string;
  end;

  RecHistoricoContagemItens = record
    InventarioId: Integer;
    InventarioNome: string;
    InventarioInico: TDateTime;
    InventarioFim: TDateTime;
    ProdutoId: Integer;
    Estoque: Double;
    Quantidade: Double;
    ContagemNumero: Integer;
    ContagemData: TDateTime;
    ContagemDiferenca: Integer;
    NomeProduto: string;
    MarcaId: Integer;
    NomeMarca: string;
    UnidadeVenda: string;
    LocalId: Integer;
  end;

  TInventarioItens = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordInventarioItens: RecInventarioItens;
  end;

function AtualizarInventarioItens(
  pConexao: TConexao;
  pInventarioId: Integer;
  pProdutoId: Integer;
  pLocalId: Integer;
  pEstoque: Double;
  pQuantidade: Double;
  pContou: String
): RecRetornoBD;

function BuscarInventarioItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecInventarioItens>;

function BuscarLancamentoItens(
  pConexao: TConexao;
  pComando: string
): TArray<RecInventarioLancamentoItens>;

function BuscarHistoricoContagemItens(
  pConexao: TConexao;
  pComando: string
): TArray<RecHistoricoContagemItens>;

function ProdutosEmContagem(
  pConexao: TConexao;
  pProdutos: string;
  pLocal: string;
  pEmpresaId: string
): Boolean;


function ExcluirInventarioItens(
  pConexao: TConexao;
  pInventarioId: Integer;
  pProdutoId: Integer;
  pLocalId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TInventarioItens }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Produto',
      False,
      0,
      'where TO_CHAR(PRODUTO_ID) IN (:P1)'+
//      '  and INVENTARIO_ID <> :P2'+
      '  and (select count(*) '+
      '    from inventario i '+
      '   where i.inventario_id = inventario_id  '+
      '     and i.Empresa_Id = :P2 '+
      '     and i.data_final is null) > 0 '
    );
end;

constructor TInventarioItens.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'INVENTARIO_ITENS');

  FSql :=
    'select ' +
    '  INVENTARIO_ID, ' +
    '  PRODUTO_ID, ' +
    '  ESTOQUE, ' +
    '  QUANTIDADE, ' +
    '  CONTOU, ' +
    '  LOCAL_ID '+
    'from ' +
    '  INVENTARIO_ITENS ';

  setFiltros(getFiltros);

  AddColuna('INVENTARIO_ID', True);
  AddColuna('PRODUTO_ID', True);
  AddColuna('LOCAL_ID', True);
  AddColuna('ESTOQUE');
  AddColuna('QUANTIDADE');
  AddColuna('CONTOU');
end;

function TInventarioItens.getRecordInventarioItens: RecInventarioItens;
begin
  Result.InventarioId               := getInt('INVENTARIO_ID', True);
  Result.ProdutoId                  := getInt('PRODUTO_ID', True);
  Result.LocalId                    := getInt('LOCAL_ID', True);
  Result.Estoque                    := getInt('ESTOQUE');
  Result.Quantidade                 := getInt('QUANTIDADE');
  Result.Contou                     := getString('CONTOU');

end;

function AtualizarInventarioItens(
  pConexao: TConexao;
  pInventarioId: Integer;
  pProdutoId: Integer;
  pLocalId: Integer;
  pEstoque: Double;
  pQuantidade: Double;
  pContou: String
): RecRetornoBD;
var
  t: TInventarioItens;
  vSeq: TSequencia;
begin
  Result.Iniciar;
  pConexao.setRotina('ATUALIZAR_INVENTARIO_ITENS');

  t := TInventarioItens.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('INVENTARIO_ID', pInventarioId, True);
    t.setInt('PRODUTO_ID', pProdutoId, True);
    t.setInt('LOCAL_ID', pLocalId, True);
    t.setDouble('ESTOQUE', pEstoque);
    t.setDouble('QUANTIDADE', pQuantidade);
    T.setString('CONTOU', pContou);


    t.Inserir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarInventarioItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecInventarioItens>;
var
  i: Integer;
  t: TInventarioItens;
begin
  Result := nil;
  t := TInventarioItens.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordInventarioItens;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirInventarioItens(
  pConexao: TConexao;
  pInventarioId: Integer;
  pProdutoId: Integer;
  pLocalId: Integer
): RecRetornoBD;
var
  t: TInventarioItens;
begin
  Result.Iniciar;
  t := TInventarioItens.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('INVENTARIO_ID', pInventarioId, True);
    t.setInt('PRODUTO_ID', pProdutoId, True);
    t.setInt('LOCAL_ID', pLocalId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarLancamentoItens(
  pConexao: TConexao;
  pComando: string
): TArray<RecInventarioLancamentoItens>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select invi.inventario_id, ');
  vSql.Add('       invi.produto_id, ');
  vSql.Add('       invi.estoque, ');
  vSql.Add('       invi.quantidade, ');
  vSql.Add('       invi.contou, ');
  vSql.Add('       invi.local_id, ');
  vSql.Add('       div.lote, ');
  vSql.Add('       prod.nome AS Nome_Produto, ');
  vSql.Add('       prod.unidade_venda, ');
  vSql.Add('       prod.codigo_barras, ');
  vSql.Add('       mar.marca_id, ');
  vSql.Add('       mar.nome as Nome_Marca ');
  vSql.Add('  from inventario_itens invi ');
  vSql.Add(' inner join estoques_divisao div on invi.produto_id = div.produto_id and invi.local_id = div.local_id');
  vSql.Add(' inner join produtos prod        on div.produto_id  = prod.produto_id ');
  vSql.Add(' inner join marcas mar           on prod.marca_id   = mar.marca_id ');

  vSql.Add(pComando);

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].InventarioId        := vSql.getInt('INVENTARIO_ID');
      Result[i].ProdutoId           := vSql.getInt('PRODUTO_ID');
      Result[i].LocalId             := vSql.getInt('LOCAL_ID');
      Result[i].Estoque             := vSql.getInt('ESTOQUE');
      Result[i].Quantidade          := vSql.getInt('QUANTIDADE');
      Result[i].NomeProduto         := vSql.GetString('NOME_PRODUTO');
      Result[i].Lote                := vSql.GetString('LOTE');
      Result[i].MarcaId             := vSql.getInt('MARCA_ID');
      Result[i].NomeMarca           := vSql.GetString('NOME_MARCA');
      Result[i].UnidadeVenda        := vSql.GetString('UNIDADE_VENDA');
      Result[i].Contou              := vSql.GetString('CONTOU');
      Result[i].CodigoBarra         := vSql.GetString('CODIGO_BARRAS');

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function BuscarHistoricoContagemItens(
  pConexao: TConexao;
  pComando: string
): TArray<RecHistoricoContagemItens>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select inv.inventario_id, ');
  vSql.Add('       inv.nome as nome_contagem, ');
  vSql.Add('       inv.data_inicio, ');
  vSql.Add('       inv.data_final, ');
  vSql.Add('       invi.estoque, ');
  vSql.Add('       invi.quantidade, ');
  vSql.Add('       invi.local_id, ');
  vSql.Add('       hico.numero_contagem, ');
  vSql.Add('       hico.produto_id, ');
  vSql.Add('       hico.data as contagem_data, ');
  vSql.Add('       hico.diferenca contagem_diferenca, ');
  vSql.Add('       prod.nome as nome_produto, ');
  vSql.Add('       prod.unidade_venda, ');
  vSql.Add('       mar.marca_id, ');
  vSql.Add('       mar.nome as Nome_Marca ');

  vSql.Add('   from historico_contagem hico ');
  vSql.Add('  inner join inventario inv        on hico.inventario_id = inv.inventario_id  and hico.diferenca <> 0 ');
  vSql.Add('  inner join inventario_itens invi on hico.inventario_id = invi.inventario_id and hico.produto_id = invi.produto_id   and invi.contou = ''S'' ');
  vSql.Add('  inner join produtos prod         on hico.produto_id = prod.produto_id ');
  vSql.Add('  inner join marcas mar            on prod.marca_id = mar.marca_id ');

  vSql.Add(pComando);

  vSql.Add(' order by hico.numero_contagem ');

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin

      Result[i].InventarioId      := vSql.getInt('INVENTARIO_ID');
      Result[i].InventarioNome    := vSql.GetString('NOME_CONTAGEM');
      Result[i].InventarioInico   := vSql.GetData('DATA_INICIO');
      Result[i].InventarioFim     := vSql.GetData('DATA_FINAL');
      Result[i].Estoque           := vSql.getInt('ESTOQUE');
      Result[i].Quantidade        := vSql.getInt('QUANTIDADE');
      Result[i].ContagemNumero    := vSql.getInt('NUMERO_CONTAGEM');
      Result[i].ProdutoId         := vSql.getInt('PRODUTO_ID');
      Result[i].ContagemData      := vSql.GetData('CONTAGEM_DATA');
      Result[i].ContagemDiferenca := vSql.getInt('CONTAGEM_DIFERENCA');
      Result[i].NomeProduto       := vSql.GetString('NOME_PRODUTO');
      Result[i].MarcaId           := vSql.getInt('MARCA_ID');
      Result[i].NomeMarca         := vSql.GetString('NOME_MARCA');
      Result[i].UnidadeVenda      := vSql.GetString('UNIDADE_VENDA');
      Result[i].LocalId           := vSql.GetInt('LOCAL_ID');

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;

end;

function ProdutosEmContagem(
  pConexao: TConexao;
  pProdutos: string;
  pLocal: string;
  pEmpresaId: string
): Boolean;
var
  vSql: TConsulta;
begin
  Result := False;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select count(0) AS QTDE');
  vSql.Add('  from INVENTARIO_ITENS ');
  vSql.Add(' where TO_CHAR(PRODUTO_ID) IN ('+pProdutos+') ');
  vSql.Add('   and TO_CHAR(LOCAL_ID) IN ('+pLocal+') ');
  vSql.Add('   and (select count(*) ');
  vSql.Add('          from inventario i ');
  vSql.Add('         where i.inventario_id = inventario_id ');
  vSql.Add('           and i.Empresa_Id = '+ pEmpresaId);
  vSql.Add('           and i.data_final is null) > 0 ');

  vSql.Pesquisar;
  Result := vSql.getInt('QTDE') > 0;

  vSql.Active := False;
  vSql.Free;
end;

end.


