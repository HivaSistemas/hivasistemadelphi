unit _FaixasSimplesNacional;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TFaixaSimplesNacional = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordFaixaSimplesNacional: RecFaixaSimplesNacional;
  end;

function BuscarFaixaSimplesNacionals(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecFaixaSimplesNacional>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TFaixaSimplesNacional }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      ''
    );
end;

constructor TFaixaSimplesNacional.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'VW_FAIXAS_SIMPLES_NACIONAL');

  FSql :=
    'select ' +
    '  FAIXA, ' +
    '  DESCRICAO, ' +
    '  PERCENTUAL ' +
    'from ' +
    '  VW_FAIXAS_SIMPLES_NACIONAL';

  setFiltros(getFiltros);

  AddColunaSL('FAIXA');
  AddColunaSL('DESCRICAO');
  AddColunaSL('PERCENTUAL');
end;

function TFaixaSimplesNacional.getRecordFaixaSimplesNacional: RecFaixaSimplesNacional;
begin
  Result.Faixa      := getInt('FAIXA');
  Result.Descricao  := getString('DESCRICAO');
  Result.Percentual := getDouble('PERCENTUAL');
end;

function BuscarFaixaSimplesNacionals(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecFaixaSimplesNacional>;
var
  i: Integer;
  t: TFaixaSimplesNacional;
begin
  Result := nil;
  t := TFaixaSimplesNacional.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordFaixaSimplesNacional;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
