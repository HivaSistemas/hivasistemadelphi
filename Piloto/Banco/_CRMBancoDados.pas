unit _CRMBancoDados;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TCRMBancoDados = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordCRMBancoDados: RecCRMBancoDados;
  end;

function BuscarCRMBancoDados(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCRMBancoDados>;

function ExcluirCRMBancoDados(
  pConexao: TConexao;
  pCRMBancoDadosId: Integer
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TCRMBancoDados }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CENTRAL_ID = :P1',
      'order by ITEM_ID asc'
    );
end;

constructor TCRMBancoDados.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CRM_BANCO_DADOS');

  FSql :=
    'select ' +
    '  CENTRAL_ID, ' +
    '  IP_SERVIDOR, ' +
    '  ITEM_ID, ' +
    '  PORTA, ' +
    '  SENHA, ' +
    '  SERVICO, ' +
    '  USUARIO ' +
    'from ' +
    '  CRM_BANCO_DADOS ';

  SetFiltros(GetFiltros);

  AddColuna('CENTRAL_ID', True);
  AddColuna('ITEM_ID');
  AddColuna('IP_SERVIDOR');
  AddColuna('PORTA');
  AddColuna('SENHA');
  AddColuna('SERVICO');
  AddColuna('USUARIO');
end;

function TCRMBancoDados.GetRecordCRMBancoDados: RecCRMBancoDados;
begin
  Result.central_id  := GetInt('CENTRAL_ID', True);
  Result.item_id     := GetInt('ITEM_ID');
  Result.ip_servidor := getString('IP_SERVIDOR');
  Result.senha       := getString('SENHA');
  Result.usuario     := getString('USUARIO');
  Result.porta       := getString('PORTA');
  Result.servico     := getString('SERVICO');
end;

function BuscarCRMBancoDados(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCRMBancoDados>;
var
  i: Integer;
  t: TCRMBancoDados;
begin
  Result := nil;
  t := TCRMBancoDados.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordCRMBancoDados;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirCRMBancoDados(
  pConexao: TConexao;
  pCRMBancoDadosId: Integer
): RecRetornoBD;
var
  t: TCRMBancoDados;
begin
  Result.TeveErro := False;
  t := TCRMBancoDados.Create(pConexao);

  t.SetInt('CENTRAL_ID', pCRMBancoDadosId, True);

  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
