unit _EntradasNFItensCompras;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils;

{$M+}
type
  RecEntradasNFItensCompras = Record
  public
    EntradaId: Integer;
    ItemId: Integer;
    CompraId: Integer;
    ItemCompraId: Integer;
    Quantidade: Double;

    (* Utilizado na tela de entradas *)
    ProdutoId: Integer;
    Saldo: Double;
  end;

  TEntradasNFItensCompras = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordItem: RecEntradasNFItensCompras;
  end;

function BuscarEntradasNFItensCompras(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEntradasNFItensCompras>;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TEntradasNFItensCompras }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ITC.ENTRADA_ID = :P1'
    );
end;

constructor TEntradasNFItensCompras.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ENTRADAS_NF_ITENS_COMPRAS');

  FSql :=
    'select ' +
    '  ITC.ENTRADA_ID, ' +
    '  ITC.ITEM_ID, ' +
    '  ITC.COMPRA_ID, ' +
    '  ITC.ITEM_COMPRA_ID, ' +
    '  ITC.QUANTIDADE, ' +
    '  ITC.QUANTIDADE + CIE.SALDO as SALDO, ' +
    '  ITE.PRODUTO_ID ' +
    'from ' +
    '  ENTRADAS_NF_ITENS_COMPRAS ITC ' +

    'inner join ENTRADAS_NOTAS_FISCAIS_ITENS ITE ' +
    'on ITC.ENTRADA_ID = ITE.ENTRADA_ID ' +
    'and ITC.ITEM_ID = ITE.ITEM_ID ' +

    'inner join COMPRAS_ITENS CIE ' +
    'on ITC.COMPRA_ID = CIE.COMPRA_ID ' +
    'and ITC.ITEM_COMPRA_ID = CIE.ITEM_ID ';

  SetFiltros(GetFiltros);

  AddColuna('ENTRADA_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('COMPRA_ID', True);
  AddColuna('ITEM_COMPRA_ID');
  AddColuna('QUANTIDADE');
  AddColunaSL('SALDO');
  AddColunaSL('PRODUTO_ID');
end;

function TEntradasNFItensCompras.GetRecordItem: RecEntradasNFItensCompras;
begin
  Result.EntradaId    := GetInt('ENTRADA_ID', True);
  Result.ItemId       := GetInt('ITEM_ID', True);
  Result.CompraId     := GetInt('COMPRA_ID', True);
  Result.ItemCompraId := GetInt('ITEM_COMPRA_ID');
  Result.Quantidade   := GetDouble('QUANTIDADE');
  Result.Saldo        := GetDouble('SALDO');
  Result.ProdutoId    := getInt('PRODUTO_ID');
end;

function BuscarEntradasNFItensCompras(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEntradasNFItensCompras>;
var
  i: Integer;
  t: TEntradasNFItensCompras;
begin
  Result := nil;
  t := TEntradasNFItensCompras.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordItem;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
