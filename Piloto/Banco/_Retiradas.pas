unit _Retiradas;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsExpedicao,
  _RetiradasItens, System.Variants, System.StrUtils, _BibliotecaGenerica;

{$M+}
type
  TRetirada = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordRetirada: RecRetirada;
  end;

function AtualizarRetirada(
  pConexao: TConexao;
  pRetiradaId: Integer;
  pOrcamentoId: Integer;
  pConfirmada: string;
  pTipoMovimento: string;
  pDataHoraRetirada: TDateTime;
  pNomePessoaRetirada: string;
  pCpfPessoaRetirada: string;
  pObservacoes: string;
  pItens: TArray<RecRetiradaItem>;
  pControlarTransacao: Boolean
): RecRetornoBD;

function BuscarRetiradas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecRetirada>;

function BuscarRetiradasComando(pConexao: TConexao; pComando: string): TArray<RecRetirada>;

function ExcluirRetirada(
  pConexao: TConexao
): RecRetornoBD;

function GerarNotaRetirada(
  pConexao: TConexao;
  const pRetiradaId: Integer;
  const pNotaFiscalId: Integer;
  const pControlarTransacao: Boolean
): RecRetornoBD;

function ConfirmarRetirada(
  pConexao: TConexao;
  const pRetiradaId: Integer;
  const pNomePessoaRetirada: string;
  const pCPFPessoaRetirada: string;
  const pDataHoraRetirada: TDateTime;
  const pObservacoes: string
): RecRetornoBD;

function CancelarRetirada(pConexao: TConexao; const pRetiradaId: Integer): RecRetornoBD;

function BuscarRetiradaIdAto(pConexao: TConexao; pOrcamentoId: Integer): TArray<Integer>;
function getAgrupamentoId(pConexao: TConexao): Integer;
function setImprimiuListaSeparacao(pConexao: TConexao; pEntregaId: Integer): RecRetornoBD;
function getQtdeViasImpressasComprovanteRetirada(pConexao: TConexao; pRetiradaId: Integer): Integer;
function IncrementarQtdeViasCompRetiradaImpressos(pConexao: TConexao; pRetiradaId: Integer): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TRetirada }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 3);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where RET.RETIRADA_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where RET.ORCAMENTO_ID = :P1 ' +
      'order by ' +
      '  RET.RETIRADA_ID desc '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where RET.ORCAMENTO_ID = :P1 ' +
      'and RET.TIPO_MOVIMENTO = ''R'' ' +
      'order by ' +
      '  RET.RETIRADA_ID desc '
    );
end;

constructor TRetirada.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'RETIRADAS');

  FSql :=
    'select ' +
    '  RET.RETIRADA_ID, ' +
    '  RET.EMPRESA_ID, ' +
    '  EMP.NOME_FANTASIA, ' +
    '  RET.ORCAMENTO_ID, ' +
    '  RET.USUARIO_CADASTRO_ID, ' +
    '  FUC.NOME as NOME_USUARIO_CADASTRO, ' +
    '  RET.DATA_HORA_CADASTRO, ' +
    '  RET.TIPO_MOVIMENTO, ' +
    '  RET.CONFIRMADA, ' +
    '  RET.DATA_HORA_RETIRADA, ' +
    '  RET.NOME_PESSOA_RETIRADA, ' +
    '  RET.CPF_PESSOA_RETIRADA, ' +
    '  RET.OBSERVACOES, ' +
    '  ORC.CLIENTE_ID, ' +
    '  case when ORC.CLIENTE_ID = PAR.CADASTRO_CONSUMIDOR_FINAL_ID then ORC.NOME_CONSUMIDOR_FINAL else CAD.RAZAO_SOCIAL end as NOME_CLIENTE, ' +
    '  CAD.NOME_FANTASIA as APELIDO, ' +
    '  ORC.VENDEDOR_ID, ' +
    '  CAD.TELEFONE_PRINCIPAL, ' +
    '  CAD.TELEFONE_CELULAR, ' +
    '  VEN.NOME as NOME_VENDEDOR, ' +
    '  RET.USUARIO_CONFIRMACAO_ID, ' +
    '  FCO.NOME as NOME_USUARIO_CONFIRMACAO, ' +
    '  NFI.NOTA_FISCAL_ID, ' +
    '  RET.LOCAL_ID, ' +
    '  LOC.NOME as NOME_LOCAL, ' +
    '  ORC.CONDICAO_ID, ' +
    '  CON.NOME as NOME_CONDICAO_PAGAMENTO ' +
    'from ' +
    '  RETIRADAS RET ' +

    'inner join ORCAMENTOS ORC ' +
    'on RET.ORCAMENTO_ID = ORC.ORCAMENTO_ID ' +

    'inner join CONDICOES_PAGAMENTO CON ' +
    'on ORC.CONDICAO_ID = CON.CONDICAO_ID ' +

    'inner join CADASTROS CAD ' +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID ' +

    'inner join FUNCIONARIOS FUC ' +
    'on RET.USUARIO_CADASTRO_ID = FUC.FUNCIONARIO_ID ' +

    'inner join EMPRESAS EMP ' +
    'on RET.EMPRESA_ID = EMP.EMPRESA_ID ' +

    'inner join LOCAIS_PRODUTOS LOC ' +
    'on RET.LOCAL_ID = LOC.LOCAL_ID ' +

    'cross join PARAMETROS PAR ' +

    'left join FUNCIONARIOS VEN ' +
    'on ORC.VENDEDOR_ID = VEN.FUNCIONARIO_ID ' +

    'left join FUNCIONARIOS FCO ' +
    'on RET.USUARIO_CONFIRMACAO_ID = FCO.FUNCIONARIO_ID ' +

    'left join NOTAS_FISCAIS NFI ' +
    'on RET.RETIRADA_ID = NFI.RETIRADA_ID ' +
    ' and NFI.TIPO_MOVIMENTO = case when RET.TIPO_MOVIMENTO = ''A'' then ''VRA'' else ''VRE'' end ';

  setFiltros(getFiltros);

  AddColuna('RETIRADA_ID', True);
  AddColuna('LOCAL_ID');
  AddColuna('NOME_LOCAL');
  AddColuna('EMPRESA_ID');
  AddColuna('ORCAMENTO_ID');
  AddColunaSL('USUARIO_CADASTRO_ID');
  AddColunaSL('NOME_USUARIO_CADASTRO');
  AddColunaSL('DATA_HORA_CADASTRO');
  AddColuna('TIPO_MOVIMENTO');
  AddColuna('CONFIRMADA');
  AddColuna('DATA_HORA_RETIRADA');
  AddColuna('NOME_PESSOA_RETIRADA');
  AddColuna('CPF_PESSOA_RETIRADA');
  AddColuna('OBSERVACOES');
  AddColunaSL('CLIENTE_ID');
  AddColunaSL('NOME_CLIENTE');
  AddColunaSL('APELIDO');
  AddColunaSL('VENDEDOR_ID');
  AddColunaSL('TELEFONE_PRINCIPAL');
  AddColunaSL('TELEFONE_CELULAR');
  AddColunaSL('NOME_VENDEDOR');
  AddColunaSL('USUARIO_CONFIRMACAO_ID');
  AddColunaSL('NOME_USUARIO_CONFIRMACAO');
  AddColunaSL('NOTA_FISCAL_ID');
  AddColunaSL('NOME_FANTASIA');
  AddColunaSL('CONDICAO_ID');
  AddColunaSL('NOME_CONDICAO_PAGAMENTO');
end;

function TRetirada.getRecordRetirada: RecRetirada;
begin
  Result.RetiradaId             := getInt('RETIRADA_ID', True);
  Result.LocalId                := getInt('LOCAL_ID');
  Result.NomeLocal              := getString('NOME_LOCAL');
  Result.EmpresaId              := getInt('EMPRESA_ID');
  Result.OrcamentoId            := getInt('ORCAMENTO_ID');
  Result.UsuarioCadastroId      := getInt('USUARIO_CADASTRO_ID');
  Result.NomeUsuarioCadastro    := getString('NOME_USUARIO_CADASTRO');
  Result.DataHoraCadastro       := getData('DATA_HORA_CADASTRO');
  Result.TipoMovimento          := getString('TIPO_MOVIMENTO');

  Result.TipoMovimentoAnalitico := IfThen(Result.TipoMovimento = 'A', 'Ato', 'Retirar');

  Result.Confirmada             := getString('CONFIRMADA');
  Result.DataHoraRetirada       := getData('DATA_HORA_RETIRADA');
  Result.NomePessoaRetirada     := getString('NOME_PESSOA_RETIRADA');
  Result.CpfPessoaRetirada      := getString('CPF_PESSOA_RETIRADA');
  Result.Observacoes            := getString('OBSERVACOES');
  Result.ClienteId              := getInt('CLIENTE_ID');
  Result.NomeCliente            := getString('NOME_CLIENTE');
  Result.Apelido                := getString('APELIDO');
  Result.VendedorId             := getInt('VENDEDOR_ID');
  Result.TelefonePrincipal      := getString('TELEFONE_PRINCIPAL');
  Result.TelefoneCelular        := getString('TELEFONE_CELULAR');
  Result.NomeVendedor           := getString('NOME_VENDEDOR');
  Result.UsuarioConfirmacaoId   := getInt('USUARIO_CONFIRMACAO_ID');
  Result.NomeUsuarioConfirmacao := getString('NOME_USUARIO_CONFIRMACAO');
  Result.NomeEmpresa            := getString('NOME_FANTASIA');
  Result.NotaFiscalId           := getInt('NOTA_FISCAL_ID');
  Result.CondicaoId             := getInt('CONDICAO_ID');
  Result.NomeCondicaoPagamento  := getString('NOME_CONDICAO_PAGAMENTO');
end;

function AtualizarRetirada(
  pConexao: TConexao;
  pRetiradaId: Integer;
  pOrcamentoId: Integer;
  pConfirmada: string;
  pTipoMovimento: string;
  pDataHoraRetirada: TDateTime;
  pNomePessoaRetirada: string;
  pCpfPessoaRetirada: string;
  pObservacoes: string;
  pItens: TArray<RecRetiradaItem>;
  pControlarTransacao: Boolean
): RecRetornoBD;
var
  i: Integer;
  t: TRetirada;
  vNovo: Boolean;
  vSeq: TSequencia;
  vIte: TRetiradaItem;
begin
  Result.TeveErro := False;
  t := TRetirada.Create(pConexao);
  vIte := TRetiradaItem.Create(pConexao);
  pConexao.SetRotina('ATUALIZAR_RETIRADA');

  vNovo := pRetiradaId = 0;
  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_RETIRADAS_ID');
    pRetiradaId := vSeq.getProximaSequencia;
    Result.AsInt := pRetiradaId;
    vSeq.Free;
  end;

  try
    if pControlarTransacao then
      pConexao.IniciarTransacao;

    t.setInt('RETIRADA_ID', pRetiradaId, True);
    t.setInt('ORCAMENTO_ID', pOrcamentoId);
    t.setString('CONFIRMADA', pConfirmada);
    t.setString('TIPO_MOVIMENTO', pTipoMovimento);
    t.setDataN('DATA_HORA_RETIRADA', pDataHoraRetirada);
    t.setString('NOME_PESSOA_RETIRADA', pNomePessoaRetirada);
    t.setString('CPF_PESSOA_RETIRADA', pCpfPessoaRetirada);
    t.setString('OBSERVACOES', pObservacoes);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    for i := Low(pItens) to High(pItens) do begin
      vIte.setInt('RETIRADA_ID', pRetiradaId, True);
      vIte.setInt('PRODUTO_ID', pItens[i].ProdutoId, True);
      vIte.setInt('ITEM_ID', pItens[i].ItemId, True);
      vIte.setDouble('QUANTIDADE', pItens[i].Quantidade);
      vIte.setString('LOTE', pItens[i].Lote);

      vIte.Inserir;
    end;

    if pControlarTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if pControlarTransacao then
        pConexao.VoltarTransacao;

      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  vIte.Free;
  t.Free;
end;

function BuscarRetiradas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecRetirada>;
var
  i: Integer;
  t: TRetirada;
begin
  Result := nil;
  t := TRetirada.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordRetirada;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarRetiradasComando(pConexao: TConexao; pComando: string): TArray<RecRetirada>;
var
  i: Integer;
  t: TRetirada;
begin
  Result := nil;
  t := TRetirada.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordRetirada;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirRetirada(
  pConexao: TConexao
): RecRetornoBD;
var
  t: TRetirada;
begin
  Result.TeveErro := False;
  t := TRetirada.Create(pConexao);

  try

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function GerarNotaRetirada(
  pConexao: TConexao;
  const pRetiradaId: Integer;
  const pNotaFiscalId: Integer;
  const pControlarTransacao: Boolean
): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.TeveErro := False;

  try
//    if pControlarTransacao then
//      pConexao.IniciarTransacao;
//
//    vProc := TProcedimentoBanco.Create(pConexao, 'GERAR_NOTA_RETIRADA');
//
//    vProc.Params[0].AsInteger := pRetiradaId;
//
//    if pNotaFiscalId > 0 then
//      vProc.Params[1].AsInteger := pNotaFiscalId
//    else
//      vProc.Params[1].Value := null;
//
//    vProc.Executar;
//
//    Result.AsInt := vProc.Params[2].AsInteger;
//
//    if pControlarTransacao then
//      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if pControlarTransacao then
        pConexao.VoltarTransacao;

      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  FreeAndNil(vProc);
end;

function ConfirmarRetirada(
  pConexao: TConexao;
  const pRetiradaId: Integer;
  const pNomePessoaRetirada: string;
  const pCPFPessoaRetirada: string;
  const pDataHoraRetirada: TDateTime;
  const pObservacoes: string
): RecRetornoBD;
var
  vExec: TExecucao;
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('CONFIRMAR_RETIRADA');

  vExec := TExecucao.Create(pConexao);
  vProc := TProcedimentoBanco.Create(pConexao, 'CONFIRMAR_RETIRADA');
  try
    pConexao.IniciarTransacao;

    vExec.SQL.Add('update RETIRADAS set ');
    vExec.SQL.Add('  CONFIRMADA = ''S'', ');
    vExec.SQL.Add('  NOME_PESSOA_RETIRADA = :P2, ');
    vExec.SQL.Add('  CPF_PESSOA_RETIRADA = :P3, ');
    vExec.SQL.Add('  DATA_HORA_RETIRADA = :P4, ');
    vExec.SQL.Add('  OBSERVACOES = :P5 ');
    vExec.SQL.Add('where RETIRADA_ID = :P1 ');

    vExec.Executar([pRetiradaId, pNomePessoaRetirada, pCPFPessoaRetirada, _Biblioteca.nvl(pDataHoraRetirada, null), pObservacoes]);

    vProc.Params[0].AsInteger := pRetiradaId;
    vProc.Params[1].AsString := pNomePessoaRetirada;
    vProc.ExecProc;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  vExec.Free;
  vProc.Free;
end;

function CancelarRetirada(pConexao: TConexao; const pRetiradaId: Integer): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.TeveErro := False;
  vProc := TProcedimentoBanco.Create(pConexao, 'CANCELAR_RETIRADA');
  try
    pConexao.IniciarTransacao;

    vProc.Params[0].AsInteger := pRetiradaId;
    vProc.ExecProc;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  vProc.Free;
end;


function BuscarRetiradaIdAto(pConexao: TConexao; pOrcamentoId: Integer): TArray<Integer>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  RETIRADA_ID ');
  vSql.Add('from ');
  vSql.Add('  RETIRADAS ');
  vSql.Add('where ORCAMENTO_ID = :P1 ');
  vSql.Add('and TIPO_MOVIMENTO = ''A'' ');

  if vSql.Pesquisar([pOrcamentoId]) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros -1 do begin
      Result[i] := vSql.GetInt(0);

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function getAgrupamentoId(pConexao: TConexao): Integer;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select SEQ_AGRUP_RETIRADAS_ID.nextval from dual');
  vSql.Pesquisar;
  Result := vSql.GetInt(0);

  vSql.Free;
end;

function setImprimiuListaSeparacao(pConexao: TConexao; pEntregaId: Integer): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ALTERAR_IMP_LISTA_SEPARACAO');

  vExec := TExecucao.Create(pConexao);

  vExec.Add('update ENTREGAS set ');
  vExec.Add('  IMPRIMIU_LISTA_SEPARACAO = ''S'' ');
  vExec.Add('where ENTREGA_ID = :P1 ');

  try
    pConexao.IniciarTransacao;

    vExec.Executar([pEntregaId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
end;

function getQtdeViasImpressasComprovanteRetirada(pConexao: TConexao; pRetiradaId: Integer): Integer;
var
  vSql: TConsulta;
begin
  Result := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  QTDE_VIAS_IMPRESSAS_COMP_RET ');
  vSql.Add('from ');
  vSql.Add('  RETIRADAS ');
  vSql.Add('where RETIRADA_ID = :P1');

  if vSql.Pesquisar([pRetiradaId]) then
    Result := vSql.GetInt(0);

  vSql.Free;
end;

function IncrementarQtdeViasCompRetiradaImpressos(pConexao: TConexao; pRetiradaId: Integer): RecRetornoBD;
var
  vExe: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATU_QTDE_VIAS_IMP_COMP_RETIRADA');

  vExe := TExecucao.Create(pConexao);

  vExe.Add('update RETIRADAS set ');
  vExe.Add('  QTDE_VIAS_IMPRESSAS_COMP_RET = QTDE_VIAS_IMPRESSAS_COMP_RET + 1 ');
  vExe.Add('where RETIRADA_ID = :P1');

  try
    pConexao.IniciarTransacao;

    vExe.Executar([pRetiradaId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  vExe.Free;
end;

end.
