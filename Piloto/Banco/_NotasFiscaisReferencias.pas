unit _NotasFiscaisReferencias;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecNotasFiscaisReferencias = record
    NotaFiscalId: Integer;
    ItemId: Integer;
    TipoReferencia: string;
    TipoNota: string;
    NotaFiscalIdReferenciada: Integer;
    ChaveNfeReferenciada: string;
  end;

  TNotasFiscaisReferencias = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordNotasFiscaisReferencias: RecNotasFiscaisReferencias;
  end;

function BuscarNotasFiscaisReferencias(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecNotasFiscaisReferencias>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TNotasFiscaisReferencias }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where REF.NOTA_FISCAL_ID = :P1 '
    );
end;

constructor TNotasFiscaisReferencias.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'NOTAS_FISCAIS_REFERENCIAS');

  FSql := 
    'select ' +
    '  REF.NOTA_FISCAL_ID, ' +
    '  REF.ITEM_ID, ' +
    '  REF.TIPO_REFERENCIA, ' +
    '  REF.TIPO_NOTA, ' +
    '  REF.NOTA_FISCAL_ID_REFERENCIADA, ' +
    '  nvl(REF.CHAVE_NFE_REFERENCIADA, NFI.CHAVE_NFE) as CHAVE_NFE_REFERENCIADA ' +
    'from ' +
    '  NOTAS_FISCAIS_REFERENCIAS REF ' +

    'left join NOTAS_FISCAIS NFI ' +
    'on REF.NOTA_FISCAL_ID_REFERENCIADA = NFI.NOTA_FISCAL_ID ';

  setFiltros(getFiltros);

  AddColuna('NOTA_FISCAL_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('TIPO_REFERENCIA');
  AddColuna('TIPO_NOTA');
  AddColuna('NOTA_FISCAL_ID_REFERENCIADA');
  AddColuna('CHAVE_NFE_REFERENCIADA');
end;

function TNotasFiscaisReferencias.getRecordNotasFiscaisReferencias: RecNotasFiscaisReferencias;
begin
  Result.NotaFiscalId               := getInt('NOTA_FISCAL_ID', True);
  Result.ItemId                     := getInt('ITEM_ID', True);
  Result.TipoReferencia             := getString('TIPO_REFERENCIA');
  Result.TipoNota                   := getString('TIPO_NOTA');
  Result.NotaFiscalIdReferenciada   := getInt('NOTA_FISCAL_ID_REFERENCIADA');
  Result.ChaveNfeReferenciada       := getString('CHAVE_NFE_REFERENCIADA');
end;

function BuscarNotasFiscaisReferencias(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecNotasFiscaisReferencias>;
var
  i: Integer;
  t: TNotasFiscaisReferencias;
begin
  Result := nil;
  t := TNotasFiscaisReferencias.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordNotasFiscaisReferencias;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
