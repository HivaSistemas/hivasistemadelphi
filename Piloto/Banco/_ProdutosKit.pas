unit _ProdutosKit;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecProdutosKit = record
    ProdutoKitId: Integer;
    ProdutoId: Integer;
    NomeProduto: string;
    Quantidade: Double;
    Ordem: Integer;
    PercParticipacao: Double;
  end;

  TProdutosKit = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordProdutosKit: RecProdutosKit;
  end;

function BuscarProdutosKit(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecProdutosKit>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TProdutosKit }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where PKT.PRODUTO_KIT_ID = :P1 ' +
      'order by ' +
      '  PKT.ORDEM '
    );
end;

constructor TProdutosKit.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PRODUTOS_KIT');

  FSql :=
    'select ' +
    '  PKT.PRODUTO_KIT_ID, ' +
    '  PKT.PRODUTO_ID, ' +
    '  PRO.NOME as NOME_PRODUTO, ' +
    '  PKT.ORDEM, ' +
    '  PKT.PERC_PARTICIPACAO, ' +
    '  PKT.QUANTIDADE ' +
    'from ' +
    '  PRODUTOS_KIT PKT ' +

    'inner join PRODUTOS PRO ' +
    'on PKT.PRODUTO_ID = PRO.PRODUTO_ID ';

  setFiltros(getFiltros);

  AddColuna('PRODUTO_KIT_ID', True);
  AddColuna('PRODUTO_ID', True);
  AddColunaSL('NOME_PRODUTO');
  AddColuna('ORDEM');
  AddColuna('PERC_PARTICIPACAO');
  AddColuna('QUANTIDADE');
end;

function TProdutosKit.getRecordProdutosKit: RecProdutosKit;
begin
  Result.ProdutoKitId     := getInt('PRODUTO_KIT_ID', True);
  Result.ProdutoId        := getInt('PRODUTO_ID', True);
  Result.NomeProduto      := getString('NOME_PRODUTO');
  Result.Ordem            := getInt('ORDEM');
  Result.PercParticipacao := getDouble('PERC_PARTICIPACAO');
  Result.Quantidade       := getDouble('QUANTIDADE');
end;

function BuscarProdutosKit(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecProdutosKit>;
var
  i: Integer;
  t: TProdutosKit;
begin
  Result := nil;
  t := TProdutosKit.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordProdutosKit;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
