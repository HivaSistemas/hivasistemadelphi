unit _AjustesEstoque;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, _RecordsEstoques, _AjustesEstoqueItens, _Biblioteca,  Vcl.Graphics;

{$M+}
type
  RecProdutosZerarEstoque = record
    EmpresaId: Integer;
    ProdutoId: Integer;
    ProdutoNome: string;
    MarcaId: Integer;
    MarcaNome: string;
    PrecoUnitario: Double;
    LocalId: Integer;
    LocalNome: string;
    Quantidade: Double;
    Lote: string;
    DataFabricacao: TDate;
    DataVencimento: TDate;
    Unidade: string;
  end;

  RecProdutosSelecionados = record
    Itens: TArray<RecProdutosZerarEstoque>;
  end;

  TAjusteEstoque = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordAjusteEstoque: RecAjusteEstoque;
  end;

function getCorTipoAjuste(pTipoAjuste: string): TColor;
function getCorTipoEstoqueAjustado(pTipoEstoqueAjustado: string): TColor;
function BuscarProdutosComAjuste(pConexao: TConexao; pSql: string): TArray<RecProdutosZerarEstoque>;
function BuscarTodosProdutos(
  pConexao: TConexao;
  pSql: string;
  pEmpresaId: Integer
): TArray<RecProdutosZerarEstoque>;

function AtualizarAjustesEstoque(
  pConexao: TConexao;
  pAjusteEstoqueId: Integer;
  pEmpresaId: Integer;
  pMotivoAjusteId: Integer;
  pObservacao: string;
  pPlanoFinanceiroId: string;
  pTipo: string;
  pBaixaCompraId: Integer;
  pValorTotal: Double;
  pTipoEstoqueAjustado: string;
  pAjusteItens: TArray<RecAjusteEstoqueItens>;
  pEmTransacao: Boolean
): RecRetornoBD;

function BuscarProdutosEstoqueNegativo(
  pConexao: TConexao;
  pSql: string;
  pEmpresaId: Integer
): TArray<RecProdutosZerarEstoque>;

function AtualizarMapaProducao(
  pConexao: TConexao;
  pMapaProducaoId: Integer;
  pEmpresaId: Integer;
  pMapaProducao: RecMapaProducao;
  pTipoMovimento: string
): RecRetornoBD;

function BuscarAjustesEstoque(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecAjusteEstoque>;

function BuscarAjustesEstoqueComando(pConexao: TConexao; pComando: string): TArray<RecAjusteEstoque>;

function ExcluirAjustesEstoque(
  pConexao: TConexao;
  pAjusteEstoqueId: Integer
): RecRetornoBD;

function AtualizarAjusteEstoque(
  pConexao: TConexao;
  pAjusteEstoqueId: Integer;
  pExisteEntrada: Boolean;
  pExisteSaida: Boolean
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TAjusteEstoque }

function getCorTipoEstoqueAjustado(pTipoEstoqueAjustado: string): TColor;
begin
  Result := _Biblioteca.Decode(
    pTipoEstoqueAjustado,
    [
      'FISICO', clBlue,
      'FISCAL', clGreen,
      clBlack
    ]
  );
end;

function getCorTipoAjuste(pTipoAjuste: string): TColor;
begin
  Result := _Biblioteca.Decode(
    pTipoAjuste,
    [
      'NOR', $000096DB,
      'RCO', clNavy,
      'OUT', clOlive,
      clBlack
    ]
  );
end;

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where AJUSTE_ESTOQUE_ID = :P1'
    );
end;

constructor TAjusteEstoque.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'AJUSTES_ESTOQUE');

  FSql :=
    'select ' +
    '  AJU.AJUSTE_ESTOQUE_ID, ' +
    '  AJU.EMPRESA_ID, ' +
    '  EMP.NOME_FANTASIA, ' +
    '  AJU.MOTIVO_AJUSTE_ID, ' +
    '  AJU.PLANO_FINANCEIRO_ID, ' +
    '  AJU.BAIXA_COMPRA_ID, ' +
    '  AJU.TIPO, ' +
    '  MOT.DESCRICAO as MOTIVO_AJUSTE_ESTOQUE, ' +
    '  AJU.USUARIO_AJUSTE_ID, ' +
    '  FUN.NOME as NOME_USUARIO_AJUSTE, ' +
    '  AJU.DATA_HORA_AJUSTE, ' +
    '  AJU.OBSERVACAO, ' +
    '  AJU.TIPO_ESTOQUE_AJUSTADO, ' +
    '  AJU.VALOR_TOTAL ' +
    'from ' +
    '  AJUSTES_ESTOQUE AJU ' +

    'inner join MOTIVOS_AJUSTE_ESTOQUE MOT ' +
    'on AJU.MOTIVO_AJUSTE_ID = MOT.MOTIVO_AJUSTE_ID ' +

    'inner join FUNCIONARIOS FUN ' +
    'on AJU.USUARIO_AJUSTE_ID = FUN.FUNCIONARIO_ID ' +

    'inner join EMPRESAS EMP ' +
    'on AJU.EMPRESA_ID = EMP.EMPRESA_ID ';

  setFiltros(getFiltros);

  AddColuna('AJUSTE_ESTOQUE_ID', True);
  AddColuna('EMPRESA_ID');
  AddColunaSL('NOME_FANTASIA');
  AddColuna('MOTIVO_AJUSTE_ID');
  AddColuna('PLANO_FINANCEIRO_ID');
  AddColuna('TIPO');
  AddColunaSL('MOTIVO_AJUSTE_ESTOQUE');
  AddColunaSL('USUARIO_AJUSTE_ID');
  AddColunaSL('NOME_USUARIO_AJUSTE');
  AddColunaSL('DATA_HORA_AJUSTE');
  AddColuna('OBSERVACAO');
  AddColuna('BAIXA_COMPRA_ID');
  AddColuna('VALOR_TOTAL');
  AddColuna('TIPO_ESTOQUE_AJUSTADO');
end;

function TAjusteEstoque.getRecordAjusteEstoque: RecAjusteEstoque;
begin
  Result.ajuste_estoque_id   := getInt('AJUSTE_ESTOQUE_ID', True);
  Result.EmpresaId           := getInt('EMPRESA_ID');
  Result.NomeFantasia        := getString('NOME_FANTASIA');
  Result.motivo_ajuste_id    := getInt('MOTIVO_AJUSTE_ID');
  Result.PlanoFinanceiroId   := getString('PLANO_FINANCEIRO_ID');
  Result.MotivoAjusteEstoque := getString('MOTIVO_AJUSTE_ESTOQUE');
  Result.usuario_ajuste_id   := getInt('USUARIO_AJUSTE_ID');
  Result.NomeUsuarioAjuste   := getString('NOME_USUARIO_AJUSTE');
  Result.data_hora_ajuste    := getData('DATA_HORA_AJUSTE');
  Result.observacao          := getString('OBSERVACAO');
  Result.Tipo                := getString('TIPO');
  Result.BaixaCompraId       := getInt('BAIXA_COMPRA_ID');
  Result.ValorTotal          := getDouble('VALOR_TOTAL');
  Result.TipoEstoqueAjustado := getString('TIPO_ESTOQUE_AJUSTADO');

  if Result.Tipo = 'NOR' then
    Result.TipoAnalitico := 'Normal'
  else if Result.Tipo = 'RCO' then
    Result.TipoAnalitico := 'Recebim. de compras'
  else if Result.Tipo = 'OUT' then
    Result.TipoAnalitico := 'Outras notas';
end;

function AtualizarMapaProducao(
  pConexao: TConexao;
  pMapaProducaoId: Integer;
  pEmpresaId: Integer;
  pMapaProducao: RecMapaProducao;
  pTipoMovimento: string
): RecRetornoBD;
var
  t: TAjusteEstoque;
  vItem: TAjusteEstoqueItens;

  i: Integer;
  novo: Boolean;
  seq: TSequencia;
  vExec: TExecucao;
begin

end;

function AtualizarAjustesEstoque(
  pConexao: TConexao;
  pAjusteEstoqueId: Integer;
  pEmpresaId: Integer;
  pMotivoAjusteId: Integer;
  pObservacao: string;
  pPlanoFinanceiroId: string;
  pTipo: string;
  pBaixaCompraId: Integer;
  pValorTotal: Double;
  pTipoEstoqueAjustado: string;
  pAjusteItens: TArray<RecAjusteEstoqueItens>;
  pEmTransacao: Boolean
): RecRetornoBD;
var
  t: TAjusteEstoque;
  vItem: TAjusteEstoqueItens;

  i: Integer;
  novo: Boolean;
  seq: TSequencia;
  vExec: TExecucao;
begin
  Result.Iniciar;
  if pTipoEstoqueAjustado = 'FISICO' then
    pConexao.SetRotina('REALIZAR_AJUSTE_ESTOQUE')
  else
    pConexao.SetRotina('REALIZAR_AJU_EST_FISCAL');

  vExec := TExecucao.Create(pConexao);
  t := TAjusteEstoque.Create(pConexao);
  vItem := TAjusteEstoqueItens.Create(pConexao);

  novo := pAjusteEstoqueId = 0;
  if novo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_AJUSTE_ESTOQUE_ID');
    pAjusteEstoqueId := seq.getProximaSequencia;
    result.AsInt := pAjusteEstoqueId;
    seq.Free;
  end;

  try
    if not pEmTransacao then
      pConexao.IniciarTransacao;

    t.setInt('AJUSTE_ESTOQUE_ID', pAjusteEstoqueId, True);
    t.setInt('EMPRESA_ID', pEmpresaId);
    t.setInt('MOTIVO_AJUSTE_ID', pMotivoAjusteId);
    t.setStringN('OBSERVACAO', pObservacao);
    t.setString('PLANO_FINANCEIRO_ID', pPlanoFinanceiroId);
    t.setString('TIPO', pTipo);
    t.setIntN('BAIXA_COMPRA_ID', pBaixaCompraId);
    t.setDouble('VALOR_TOTAL', pValorTotal);
    t.setString('TIPO_ESTOQUE_AJUSTADO', pTipoEstoqueAjustado);

    if novo then
      t.Inserir
    else
      t.Atualizar;

    if not novo then begin
      vExec.Limpar;
      vExec.Add('delete from AJUSTES_ESTOQUE_ITENS ');
      vExec.Add('where AJUSTE_ESTOQUE_ID = :P1');
      vExec.Executar([pAjusteEstoqueId]);
    end;

    vItem.setInt('AJUSTE_ESTOQUE_ID', pAjusteEstoqueId, True);
    for i := Low(pAjusteItens) to High(pAjusteItens) do begin

      if pTipoEstoqueAjustado = 'FISICO' then
        vItem.setInt('LOCAL_ID', pAjusteItens[i].LocalId, True)
      else
        vItem.setInt('LOCAL_ID', 1, True);

      vItem.setInt('ITEM_ID', pAjusteItens[i].item_id, True);
      vItem.setString('LOTE', pAjusteItens[i].Lote, True);
      vItem.setInt('PRODUTO_ID', pAjusteItens[i].produto_id);
      vItem.setDouble('QUANTIDADE', pAjusteItens[i].quantidade);
      vItem.setString('NATUREZA', pAjusteItens[i].Natureza);
      vItem.setDouble('PRECO_UNITARIO', pAjusteItens[i].PrecoUnitario);
      vItem.setDouble('VALOR_TOTAL', pAjusteItens[i].ValorTotal);
      vItem.setDataN('DATA_FABRICACAO', pAjusteItens[i].DataFabricacao);
      vItem.setDataN('DATA_VENCIMENTO', pAjusteItens[i].DataVencimento);
      vItem.setDouble('ESTOQUE_INFORMADO', pAjusteItens[i].quantidadeInformado);

      vItem.Inserir;
    end;

    if not pEmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pEmTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;
  vExec.Free;
  vItem.Free;
  t.Free;
end;

function BuscarAjustesEstoque(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecAjusteEstoque>;
var
  i: Integer;
  t: TAjusteEstoque;
begin
  Result := nil;
  t := TAjusteEstoque.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordAjusteEstoque;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarAjustesEstoqueComando(pConexao: TConexao; pComando: string): TArray<RecAjusteEstoque>;
var
  i: Integer;
  t: TAjusteEstoque;
begin
  Result := nil;
  t := TAjusteEstoque.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordAjusteEstoque;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;


function ExcluirAjustesEstoque(
  pConexao: TConexao;
  pAjusteEstoqueId: Integer
): RecRetornoBD;
var
  vAju: TAjusteEstoque;
  vExec: TExecucao;
begin
  Result.TeveErro := False;
  vAju := TAjusteEstoque.Create(pConexao);
  vExec := TExecucao.Create(pConexao);

  vExec.Add('delete from AJUSTES_ESTOQUE_ITENS');
  vExec.Add('where AJUSTE_ESTOQUE_ID = :P1');

  try
    pConexao.IniciarTransacao;

    vExec.Executar([pAjusteEstoqueId]);

    vAju.setInt('AJUSTE_ESTOQUE_ID', pAjusteEstoqueId, True);
    vAju.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
  vAju.Free;
end;

function AtualizarAjusteEstoque(
  pConexao: TConexao;
  pAjusteEstoqueId: Integer;
  pExisteEntrada: Boolean;
  pExisteSaida: Boolean
): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('GERAR_NT_AJUS_ESTOQUE');

  try
    pConexao.IniciarTransacao;

    //Gerando nota de entrada
    if pExisteEntrada then begin
      vProc := TProcedimentoBanco.Create(pConexao, 'GERAR_NOTA_AJUSTE_ESTOQUE');
      vProc.Executar([pAjusteEstoqueId, 'E']);
      Result.AddInt(vProc.ParamByName('oNOTA_FISCAL_ID').AsInteger);
      Result.AddString(vProc.ParamByName('oTIPO_NOTA_GERAR').AsString);    end;

    //Gerando nota de sa�da
    if pExisteSaida then begin
      vProc := TProcedimentoBanco.Create(pConexao, 'GERAR_NOTA_AJUSTE_ESTOQUE');
      vProc.Executar([pAjusteEstoqueId, 'S']);
      Result.AddInt(vProc.ParamByName('oNOTA_FISCAL_ID').AsInteger);
      Result.AddString(vProc.ParamByName('oTIPO_NOTA_GERAR').AsString);
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vProc.Free;
end;

function BuscarProdutosComAjuste(pConexao: TConexao; pSql: string): TArray<RecProdutosZerarEstoque>;
var
  i: Integer;
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  AEI.PRODUTO_ID, ');
  vSql.SQL.Add('  AEI.LOCAL_ID, ');
  vSql.SQL.Add('  AEI.LOTE, ');
  vSql.SQL.Add('  AES.EMPRESA_ID ');
  vSql.SQL.Add('from AJUSTES_ESTOQUE_ITENS AEI ');

  vSql.SQL.Add('inner join AJUSTES_ESTOQUE AES ');
  vSql.SQL.Add('on AES.AJUSTE_ESTOQUE_ID = AEI.AJUSTE_ESTOQUE_ID ');

  vSql.SQL.Add('where 1 = 1 ');
  vSql.SQL.Add(pSql);
  vSql.Pesquisar;

  SetLength(Result, vSql.GetQuantidadeRegistros);
  for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
    Result[i].ProdutoId := vSql.GetInt(0);
    Result[i].LocalId   := vSql.GetInt(1);
    Result[i].Lote      := vSql.GetString(2);
    Result[i].EmpresaId := vSql.GetInt(3);

    vSql.Next;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function BuscarProdutosEstoqueNegativo(
  pConexao: TConexao;
  pSql: string;
  pEmpresaId: Integer
): TArray<RecProdutosZerarEstoque>;
var
  i: Integer;
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  DIV.EMPRESA_ID, ');
  vSql.SQL.Add('  PRO.PRODUTO_ID, ');
  vSql.SQL.Add('  PRO.NOME AS NOME_PRODUTO, ');
  vSql.SQL.Add('  PRO.MARCA_ID, ');
  vSql.SQL.Add('  MAR.NOME AS NOME_MARCA, ');
  vSql.SQL.Add('  DIV.LOCAL_ID, ');
  vSql.SQL.Add('  LOC.NOME AS LOCAL_NOME, ');
  vSql.SQL.Add('  DIV.LOTE, ');
  vSql.SQL.Add('  DIV.DATA_FABRICACAO, ');
  vSql.SQL.Add('  DIV.DATA_VENCIMENTO, ');
  vSql.SQL.Add('  DIV.DISPONIVEL, ');
  vSql.SQL.Add('  case PAE.TIPO_CUSTO_AJUSTE_ESTOQUE ');
  vSql.SQL.Add('    when ''FIN'' then zvl(CUS.PRECO_LIQUIDO, CUS.CUSTO_ULTIMO_PEDIDO) ');
  vSql.SQL.Add('    when ''COM'' then zvl(CUS.CUSTO_ULTIMO_PEDIDO, CUS.PRECO_LIQUIDO) ');
  vSql.SQL.Add('    else CUS.CMV ');
  vSql.SQL.Add('  end as PRECO_UNITARIO, ');
  vSql.SQL.Add('  PRO.UNIDADE_VENDA ');
  vSql.SQL.Add('from VW_ESTOQUES_DIVISAO DIV ');

  vSql.SQL.Add('inner join PRODUTOS PRO ');
  vSql.SQL.Add('on DIV.PRODUTO_ID = PRO.PRODUTO_ID ');

  vSql.SQL.Add('inner join EMPRESAS EMP ');
  vSql.SQL.Add('on EMP.EMPRESA_ID = DIV.EMPRESA_ID ');

  vSql.SQL.Add('inner join MARCAS MAR ');
  vSql.SQL.Add('on MAR.MARCA_ID = PRO.MARCA_ID ');

  vSql.SQL.Add('inner join VW_CUSTOS_PRODUTOS CUS ');
  vSql.SQL.Add('on CUS.PRODUTO_ID = DIV.PRODUTO_ID ');
  vSql.SQL.Add('and EMP.EMPRESA_ID = CUS.EMPRESA_ID ');

  vSql.SQL.Add('inner join PARAMETROS_EMPRESA PAE ');
  vSql.SQL.Add('on PAE.EMPRESA_ID = CUS.EMPRESA_ID ');

  vSql.SQL.Add('inner join LOCAIS_PRODUTOS LOC ');
  vSql.SQL.Add('on LOC.LOCAL_ID = DIV.LOCAL_ID ');

  vSql.SQL.Add('where 1 = 1 ');
  vSql.SQL.Add('and PRO.ATIVO = ''S'' ');
  vSql.SQL.Add('and DIV.DISPONIVEL < 0 ');
  vSql.SQL.Add('and DIV.RESERVADO = 0 ');
  vSql.SQL.Add('and DIV.BLOQUEADO = 0 ');
  vSql.SQL.Add('and PRO.TIPO_CONTROLE_ESTOQUE = ''N''');

  vSql.SQL.Add('and PRO.PRODUTO_ID IN( ');
  vSql.SQL.Add('  select ');
  vSql.SQL.Add('    EST.PRODUTO_ID ');
  vSql.SQL.Add('  from ESTOQUES EST ');

  vSql.SQL.Add('  inner join ( ');
  vSql.SQL.Add('    select ');
  vSql.SQL.Add('      PRODUTO_ID, ');
  vSql.SQL.Add('      sum(RESERVADO) AS RESERVADO_DIV ');
  vSql.SQL.Add('    from VW_ESTOQUES_DIVISAO ');
  vSql.SQL.Add('    where EMPRESA_ID = :P1 ');
  vSql.SQL.Add('    group by PRODUTO_ID ');
  vSql.SQL.Add('  ) RES ');
  vSql.SQL.Add('  on RES.PRODUTO_ID = EST.PRODUTO_ID ');
  vSql.SQL.Add('  and RES.RESERVADO_DIV = EST.RESERVADO ');

  vSql.SQL.Add('  where EST.EMPRESA_ID = :P2 ');
  vSql.SQL.Add(') ');

  vSql.SQL.Add(pSql);
  vSql.Pesquisar([pEmpresaId, pEmpresaId]);

  SetLength(Result, vSql.GetQuantidadeRegistros);
  for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
    Result[i].ProdutoId    := vSql.GetInt('PRODUTO_ID');
    Result[i].ProdutoNome  := vSql.GetString('NOME_PRODUTO');

    Result[i].LocalId      := vSql.GetInt('LOCAL_ID');
    Result[i].LocalNome    := vSql.GetString('LOCAL_NOME');

    Result[i].MarcaId      := vSql.GetInt('MARCA_ID');
    Result[i].MarcaNome    := vSql.GetString('NOME_MARCA');

    Result[i].Lote           := vSql.GetString('LOTE');
    Result[i].DataFabricacao := vSql.GetData('DATA_FABRICACAO');
    Result[i].DataVencimento := vSql.GetData('DATA_VENCIMENTO');

    Result[i].Quantidade     := vSql.GetDouble('DISPONIVEL');
    Result[i].PrecoUnitario  := vSql.GetDouble('PRECO_UNITARIO');
    Result[i].Unidade        := vSql.GetString('UNIDADE_VENDA');

    vSql.Next;
  end;

  vSql.Active := False;
  vSql.Free;
end;


function BuscarTodosProdutos(
  pConexao: TConexao;
  pSql: string;
  pEmpresaId: Integer
): TArray<RecProdutosZerarEstoque>;
var
  i: Integer;
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  DIV.EMPRESA_ID, ');
  vSql.SQL.Add('  PRO.PRODUTO_ID, ');
  vSql.SQL.Add('  PRO.NOME AS NOME_PRODUTO, ');
  vSql.SQL.Add('  PRO.MARCA_ID, ');
  vSql.SQL.Add('  MAR.NOME AS NOME_MARCA, ');
  vSql.SQL.Add('  DIV.LOCAL_ID, ');
  vSql.SQL.Add('  LOC.NOME AS LOCAL_NOME, ');
  vSql.SQL.Add('  DIV.LOTE, ');
  vSql.SQL.Add('  DIV.DATA_FABRICACAO, ');
  vSql.SQL.Add('  DIV.DATA_VENCIMENTO, ');
  vSql.SQL.Add('  DIV.DISPONIVEL, ');
  vSql.SQL.Add('  case PAE.TIPO_CUSTO_AJUSTE_ESTOQUE ');
  vSql.SQL.Add('    when ''FIN'' then zvl(CUS.PRECO_LIQUIDO, CUS.CUSTO_ULTIMO_PEDIDO) ');
  vSql.SQL.Add('    when ''COM'' then zvl(CUS.CUSTO_ULTIMO_PEDIDO, CUS.PRECO_LIQUIDO) ');
  vSql.SQL.Add('    else CUS.CMV ');
  vSql.SQL.Add('  end as PRECO_UNITARIO, ');
  vSql.SQL.Add('  PRO.UNIDADE_VENDA ');
  vSql.SQL.Add('from VW_ESTOQUES_DIVISAO DIV ');

  vSql.SQL.Add('inner join PRODUTOS PRO ');
  vSql.SQL.Add('on DIV.PRODUTO_ID = PRO.PRODUTO_ID ');

  vSql.SQL.Add('inner join EMPRESAS EMP ');
  vSql.SQL.Add('on EMP.EMPRESA_ID = DIV.EMPRESA_ID ');

  vSql.SQL.Add('inner join MARCAS MAR ');
  vSql.SQL.Add('on MAR.MARCA_ID = PRO.MARCA_ID ');

  vSql.SQL.Add('inner join VW_CUSTOS_PRODUTOS CUS ');
  vSql.SQL.Add('on CUS.PRODUTO_ID = DIV.PRODUTO_ID ');
  vSql.SQL.Add('and EMP.EMPRESA_ID = CUS.EMPRESA_ID ');

  vSql.SQL.Add('inner join PARAMETROS_EMPRESA PAE ');
  vSql.SQL.Add('on PAE.EMPRESA_ID = CUS.EMPRESA_ID ');

  vSql.SQL.Add('inner join LOCAIS_PRODUTOS LOC ');
  vSql.SQL.Add('on LOC.LOCAL_ID = DIV.LOCAL_ID ');

  vSql.SQL.Add('where 1 = 1 ');
  vSql.SQL.Add('and PRO.ATIVO = ''S'' ');
  vSql.SQL.Add('and DIV.DISPONIVEL <> 0 ');
  vSql.SQL.Add('and DIV.RESERVADO = 0 ');
  vSql.SQL.Add('and DIV.BLOQUEADO = 0 ');
  vSql.SQL.Add('and PRO.TIPO_CONTROLE_ESTOQUE = ''N''');

  vSql.SQL.Add('and PRO.PRODUTO_ID IN( ');
  vSql.SQL.Add('  select ');
  vSql.SQL.Add('    EST.PRODUTO_ID ');
  vSql.SQL.Add('  from ESTOQUES EST ');

  vSql.SQL.Add('  inner join ( ');
  vSql.SQL.Add('    select ');
  vSql.SQL.Add('      PRODUTO_ID, ');
  vSql.SQL.Add('      sum(RESERVADO) AS RESERVADO_DIV ');
  vSql.SQL.Add('    from VW_ESTOQUES_DIVISAO ');
  vSql.SQL.Add('    where EMPRESA_ID = :P1 ');
  vSql.SQL.Add('    group by PRODUTO_ID ');
  vSql.SQL.Add('  ) RES ');
  vSql.SQL.Add('  on RES.PRODUTO_ID = EST.PRODUTO_ID ');
  vSql.SQL.Add('  and RES.RESERVADO_DIV = EST.RESERVADO ');

  vSql.SQL.Add('  where EST.EMPRESA_ID = :P2 ');
  vSql.SQL.Add(') ');

  vSql.SQL.Add(pSql);
  vSql.Pesquisar([pEmpresaId, pEmpresaId]);

  SetLength(Result, vSql.GetQuantidadeRegistros);
  for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
    Result[i].ProdutoId    := vSql.GetInt('PRODUTO_ID');
    Result[i].ProdutoNome  := vSql.GetString('NOME_PRODUTO');

    Result[i].LocalId      := vSql.GetInt('LOCAL_ID');
    Result[i].LocalNome    := vSql.GetString('LOCAL_NOME');

    Result[i].MarcaId      := vSql.GetInt('MARCA_ID');
    Result[i].MarcaNome    := vSql.GetString('NOME_MARCA');

    Result[i].Lote           := vSql.GetString('LOTE');
    Result[i].DataFabricacao := vSql.GetData('DATA_FABRICACAO');
    Result[i].DataVencimento := vSql.GetData('DATA_VENCIMENTO');

    Result[i].Quantidade     := vSql.GetDouble('DISPONIVEL');
    Result[i].PrecoUnitario  := vSql.GetDouble('PRECO_UNITARIO');
    Result[i].Unidade        := vSql.GetString('UNIDADE_VENDA');

    vSql.Next;
  end;

  vSql.Active := False;
  vSql.Free;
end;

end.
