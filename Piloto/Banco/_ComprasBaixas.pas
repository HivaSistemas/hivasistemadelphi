unit _ComprasBaixas;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, _RecordsOrcamentosVendas, _ComprasItensBaixas;

{$M+}
type
  RecComprasBaixas = record
    BaixaId: Integer;
    CompraId: Integer;
    UsuarioBaixaId: Integer;
    DataHoraBaixa: TDateTime;
    NomeUsuarioBaixa: string;
  end;

  TComprasBaixas = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordComprasBaixas: RecComprasBaixas;
  end;

function BuscarComprasBaixas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecComprasBaixas>;

function AtualizarComprasBaixas(
  pConexao: TConexao;
  pCompraId: Integer;
  pItens: TArray<RecComprasItensBaixas>;
  pEmTransacao: Boolean
): RecRetornoBD;

function CancelarCompraBaixa(pConexao: TConexao; pBaixaId: Integer): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TComprasBaixas }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where BAI.COMPRA_ID = :P1 ',
      'order by ' +
      '  BAI.BAIXA_ID'
    );
end;

constructor TComprasBaixas.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'COMPRAS_BAIXAS');

  FSql :=
    'select ' +
    '  BAI.BAIXA_ID, ' +
    '  BAI.COMPRA_ID, ' +
    '  BAI.USUARIO_BAIXA_ID, ' +
    '  BAI.DATA_HORA_BAIXA, ' +
    '  FBA.NOME as NOME_USUARIO_BAIXA ' +
    'from ' +
    '  COMPRAS_BAIXAS BAI ' +

    'inner join FUNCIONARIOS FBA ' +
    'on BAI.USUARIO_BAIXA_ID = FBA.FUNCIONARIO_ID ';

  setFiltros(getFiltros);

  AddColuna('BAIXA_ID', True);
  AddColuna('COMPRA_ID', True);
  AddColunaSL('USUARIO_BAIXA_ID');
  AddColunaSL('DATA_HORA_BAIXA');
  AddColunaSL('NOME_USUARIO_BAIXA');
end;

function TComprasBaixas.getRecordComprasBaixas: RecComprasBaixas;
begin
  Result.BaixaId          := getInt('BAIXA_ID', True);
  Result.CompraId         := getInt('COMPRA_ID', True);
  Result.UsuarioBaixaId   := getInt('USUARIO_BAIXA_ID');
  Result.DataHoraBaixa    := getData('DATA_HORA_BAIXA');
  Result.NomeUsuarioBaixa := getString('NOME_USUARIO_BAIXA');
end;

function BuscarComprasBaixas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecComprasBaixas>;
var
  i: Integer;
  t: TComprasBaixas;
begin
  Result := nil;
  t := TComprasBaixas.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordComprasBaixas;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function AtualizarComprasBaixas(
  pConexao: TConexao;
  pCompraId: Integer;
  pItens: TArray<RecComprasItensBaixas>;
  pEmTransacao: Boolean
): RecRetornoBD;
var
  i: Integer;
  vBaixaId: Integer;
  vBai: TComprasBaixas;
  vItem: TComprasItensBaixas;

  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('BAIXAR_COMPRAS');
  vBai := TComprasBaixas.Create(pConexao);
  vItem := TComprasItensBaixas.Create(pConexao);
  vProc := TProcedimentoBanco.Create(pConexao, 'CONSOLIDAR_BAIXA_COMPRA');

  try
    vBaixaId := TSequencia.Create(pConexao, 'SEQ_COMPRAS_BAIXA_ID').GetProximaSequencia;
    Result.AsInt := vBaixaId;

    if not pEmTransacao then
      pConexao.IniciarTransacao;

    vBai.setInt('BAIXA_ID', vBaixaId, True);
    vBai.setInt('COMPRA_ID', pCompraId, True);
    vBai.Inserir;

    for i := Low(pItens) to High(pItens) do begin
      vItem.setInt('BAIXA_ID', vBaixaId, True);
      vItem.setInt('ITEM_ID', pItens[i].ItemId, True);
      vItem.setDouble('QUANTIDADE', pItens[i].Quantidade);

      vItem.Inserir;
    end;

    vProc.Params[0].AsInteger := vBaixaId;
    vProc.Executar;

    if not pEmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pEmTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;

  vProc.Free;
  vItem.Free;
  vBai.Free;
end;

function CancelarCompraBaixa(pConexao: TConexao; pBaixaId: Integer): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('CANCELAR_COMPRA_BAIXA');
  vProc := TProcedimentoBanco.Create(pConexao, 'CANCELAR_COMPRA_BAIXA');

  try
    pConexao.IniciarTransacao;

    vProc.Executar([pBaixaId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vProc.Free;
end;

end.
