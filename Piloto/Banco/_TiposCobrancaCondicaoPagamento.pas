unit _TiposCobrancaCondicaoPagamento;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros,
  System.StrUtils;

{$M+}
type
  TTiposCobrCondPagamento = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordTiposCobrCondPagamento: RecTiposCobrancaCondicaoPagamentos;
  end;

function AtualizarTiposCobrCondPagamento(
  pConexao: TConexao;
  pCondicaoId: Integer;
  pCobrancaIds: array of Integer
): RecRetornoBD;

function BuscarTiposCobrCondPagamentos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<Integer>;

function ExcluirTiposCobrCondPagamento(
  pConexao: TConexao;
  pCondicaoId: Integer
): RecRetornoBD;

function BuscarCartoesLiberadosCondicao(pConexao: TConexao; pCondicaoId: Integer; pTipoCartao: string): TArray<RecTiposCobrancaLiberadosCondicao>;
function BuscarFormasPagamentoLiberadas(pConexao: TConexao; pCondicaoId: Integer): TArray<string>;
function GetFiltros: TArray<RecFiltros>;

implementation

{ TTiposCobrCondPagamento }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CONDICAO_ID = :P1 '
    );
end;

constructor TTiposCobrCondPagamento.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'TIPOS_COBR_COND_PAGAMENTO');

  FSql :=
    'select ' +
    '  CONDICAO_ID, ' +
    '  COBRANCA_ID ' +
    'from ' +
    '  TIPOS_COBR_COND_PAGAMENTO ';

  SetFiltros(GetFiltros);

  AddColuna('CONDICAO_ID', True);
  AddColuna('COBRANCA_ID', True);
end;

function TTiposCobrCondPagamento.GetRecordTiposCobrCondPagamento: RecTiposCobrancaCondicaoPagamentos;
begin
  Result.condicao_id := GetInt('CONDICAO_ID', True);
  Result.cobranca_id := GetInt('COBRANCA_ID', True);
end;

function AtualizarTiposCobrCondPagamento(
  pConexao: TConexao;
  pCondicaoId: Integer;
  pCobrancaIds: array of Integer
): RecRetornoBD;
var
  i: Integer;
  ex: TExecucao;
  t: TTiposCobrCondPagamento;
begin
  Result.TeveErro := False;
  Result.AsInt := pCondicaoId;
  t := TTiposCobrCondPagamento.Create(pConexao);

  ex := TExecucao.Create(pConexao);
  ex.Add('delete from TIPOS_COBR_COND_PAGAMENTO ');
  ex.Add('where CONDICAO_ID = :P1 ');

  try
    ex.Executar([pCondicaoId]);

    t.SetInt('CONDICAO_ID', pCondicaoId, True);

    for i := Low(pCobrancaIds) to High(pCobrancaIds) do begin
      t.SetInt('COBRANCA_ID', pCobrancaIds[i], True);
      t.Inserir
    end;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  ex.Free;
  t.Free;
end;

function BuscarTiposCobrCondPagamentos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<Integer>;
var
  i: Integer;
  t: TTiposCobrCondPagamento;
begin
  Result := nil;
  t := TTiposCobrCondPagamento.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetInt('COBRANCA_ID', True);
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirTiposCobrCondPagamento(
  pConexao: TConexao;
  pCondicaoId: Integer
): RecRetornoBD;
var
  ex: TExecucao;
begin
  Result.TeveErro := False;
  ex := TExecucao.Create(pConexao);
  ex.SQL.Add('delete from TIPOS_COBR_COND_PAGAMENTO where CONDICAO_ID = :P1');
  try
    ex.Executar([pCondicaoId]);
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  ex.Free;
end;

function BuscarCartoesLiberadosCondicao(pConexao: TConexao; pCondicaoId: Integer; pTipoCartao: string): TArray<RecTiposCobrancaLiberadosCondicao>;
var
  i: Integer;
  vSql: TConsulta;

  vPesquisou: Boolean;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  TIC.COBRANCA_ID, ');
  vSql.SQL.Add('  TIC.NOME as NOME_TIPO_COBRANCA, ');
  vSql.SQL.Add('  TIC.TIPO_CARTAO, ');
  vSql.SQL.Add('  TIC.REDE_CARTAO, ');
  vSql.SQL.Add('  TIC.TIPO_RECEBIMENTO_CARTAO, ');
  vSql.SQL.Add('  TIC.UTILIZAR_LIMITE_CRED_CLIENTE ');
  vSql.SQL.Add('from ');
  vSql.SQL.Add('  TIPOS_COBRANCA TIC ');

  if pCondicaoId > 0 then begin
    vSql.SQL.Add('inner join TIPOS_COBR_COND_PAGAMENTO TCP ');
    vSql.SQL.Add('on TIC.COBRANCA_ID = TCP.COBRANCA_ID ');
  end;

  vSql.SQL.Add('where TIC.TIPO_CARTAO is not null ');
  vSql.SQL.Add('and TIC.ATIVO = ''S'' ');

  if pCondicaoId > 0 then
    vSql.SQL.Add('and TCP.CONDICAO_ID = :P1 ');

  if pTipoCartao <> '' then
    vSql.SQL.Add('and TIC.TIPO_CARTAO = ''' + pTipoCartao + '''');

  vSql.SQL.Add('order by ');
  vSql.SQL.Add('  TIC.TIPO_CARTAO, ');
  vSql.SQL.Add('  TIC.NOME');

  if pCondicaoId > 0 then
    vPesquisou := vSql.Pesquisar([pCondicaoId])
  else
    vPesquisou := vSql.Pesquisar;

  if vPesquisou then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].tipo_cobranca_id          := vSql.GetInt(0);
      Result[i].nome_tipo_cobranca        := vSql.GetString(1);
      Result[i].TipoCartao                := vSql.GetString(2);
      Result[i].TipoCartaoAnalitico       := IfThen(vSql.GetString(2) = 'C', 'Cr�dito', 'D�bito');
      Result[i].rede_cartao               := _Biblioteca.Decode(vSql.GetString(3), ['C', 'Cielo', 'V', 'VisaNet', 'R', 'RedeCard']);
      Result[i].TipoRecebimentoCartao     := vSql.GetString(4);
      Result[i].UtilizarLimiteCredCliente := vSql.GetString(5);

      vSql.Next;
    end;
  end;
  FreeAndNil(vSql);
end;

function BuscarFormasPagamentoLiberadas(pConexao: TConexao; pCondicaoId: Integer): TArray<string>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select distinct ');
  vSql.Add('  case when TPC.FORMA_PAGAMENTO = ''CRT'' then case when TPC.TIPO_CARTAO = ''C'' then ''CRC'' else ''CRD'' end else TPC.FORMA_PAGAMENTO end');
  vSql.Add('from ');
  vSql.Add('  TIPOS_COBR_COND_PAGAMENTO TCP ');

  vSql.Add('inner join TIPOS_COBRANCA TPC ');
  vSql.Add('on TCP.COBRANCA_ID = TPC.COBRANCA_ID ');

  vSql.Add('where TPC.FORMA_PAGAMENTO in(''CHQ'',''CRT'',''COB'',''FIN'',''ACU'') ');
  vSql.Add('and TPC.ATIVO = ''S'' ');

  if pCondicaoId > 0 then
    vSql.Add('and TCP.CONDICAO_ID = :P1 ');

  if vSql.Pesquisar([pCondicaoId]) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i] := vSql.GetString(0);

      vSql.Next;
    end;
  end;
  FreeAndNil(vSql);
end;

end.
