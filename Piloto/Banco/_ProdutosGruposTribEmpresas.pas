unit _ProdutosGruposTribEmpresas;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecProdutosGruposTribEmpresas = record
    EmpresaId: Integer;
    ProdutoId: Integer;

    GrupoTribEstadualCompraId: Integer;
    NomeGrupoTribEstadualCompra: string;

    GrupoTribFederalCompraId: Integer;
    NomeGrupoTribEstadualVenda: string;

    GrupoTribEstadualVendaId: Integer;
    NomeGrupoTribFederalCompra: string;

    GrupoTribFederalVendaId: Integer;
    NomeGrupoTribFederalVenda: string;
  end;

  RecTributacoesFederal = record
    ProdutoId: Integer;
    EmpresaId: Integer;

    GrupoTribFederalCompraId: Integer;
    OrigemProdutoCompra: Integer;
    CstPisCompra: string;
    CstCofinsCompra: string;

    GrupoTribFederalVendaId: Integer;
    OrigemProdutoVenda: Integer;
    CstPisVenda: string;
    CstCofinsVenda: string;
  end;

  RecTributacaoEstadual = record
    ProdutoId: Integer;
    EmpresaId: Integer;
    EstadoIdCompra: string;
    EstadoIdVenda: string;

    GrupoTribEstadualCompraId: Integer;
    CstRevendaCompra: string;
    CstIndustriaCompra: string;
    CstDistribuidoraCompra: string;

    GrupoTribEstadualVendaId: Integer;
    CstClinicaHospitalVenda: string;
    CstConstrutoraVenda: string;
    CstContribuinteVenda: string;
    CstNaoContribuinteVenda: string;
    CstOrgaoPublicoVenda: string;
    CstProdutorRuralVenda: string;
    CstRevendaVenda: string;
    PercIcmsVenda: Double;
    IndiceReducaoBaseIcmsVenda: Double;
    IvaVenda: Double;
  end;

  TProdutosGruposTribEmpresas = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordProdutosGruposTribEmpresas: RecProdutosGruposTribEmpresas;
  end;

function AtualizarProdutosGruposTribEmpresas(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pProdutoId: Integer;
  pGrupoTribEstadualCompraId: Integer;
  pGrupoTribFederalCompraId: Integer;
  pGrupoTribEstadualVendaId: Integer;
  pGrupoTribFederalVendaId: Integer
): RecRetornoBD;

function BuscarProdutosGruposTribEmpresas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecProdutosGruposTribEmpresas>;

function ExcluirProdutosGruposTribEmpresas(
  pConexao: TConexao;
  pProdutoId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

function getTributacoesFederal(pConexao: TConexao; pEmpresaId: Integer; pProdutoId: Integer): RecTributacoesFederal;
function getTributacaoEstadual(pConexao: TConexao; pEmpresaId: Integer; pEstadoId: string; pProdutoId: Integer): RecTributacaoEstadual;

implementation

{ TProdutosGruposTribEmpresas }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where PGT.PRODUTO_ID = :P1'
    );
end;

constructor TProdutosGruposTribEmpresas.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PRODUTOS_GRUPOS_TRIB_EMPRESAS');

  FSql :=
    'select ' +
    '  PGT.EMPRESA_ID, ' +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA, ' +
    '  PGT.PRODUTO_ID, ' +

    '  PGT.GRUPO_TRIB_ESTADUAL_COMPRA_ID, ' +
    '  GEC.DESCRICAO as NOME_GRUPO_TRIB_ESTADUAL_COMP, ' +

    '  PGT.GRUPO_TRIB_FEDERAL_COMPRA_ID, ' +
    '  GEV.DESCRICAO as NOME_GRUPO_TRIB_ESTADUAL_VENDA, ' +

    '  PGT.GRUPO_TRIB_ESTADUAL_VENDA_ID, ' +
    '  GFC.DESCRICAO as NOME_GRUPO_TRIB_FEDERAL_COMPRA, ' +

    '  PGT.GRUPO_TRIB_FEDERAL_VENDA_ID, ' +
    '  GFV.DESCRICAO as NOME_GRUPO_TRIB_FEDERAL_VENDA ' +
    'from ' +
    '  PRODUTOS_GRUPOS_TRIB_EMPRESAS PGT ' +

    'inner join EMPRESAS EMP ' +
    'on PGT.EMPRESA_ID = EMP.EMPRESA_ID ' +

    'inner join GRUPOS_TRIB_ESTADUAL_COMPRA GEC ' +
    'on PGT.GRUPO_TRIB_ESTADUAL_COMPRA_ID = GEC.GRUPO_TRIB_ESTADUAL_COMPRA_ID ' +

    'inner join GRUPOS_TRIB_ESTADUAL_VENDA GEV ' +
    'on PGT.GRUPO_TRIB_ESTADUAL_VENDA_ID = GEV.GRUPO_TRIB_ESTADUAL_VENDA_ID ' +

    'inner join GRUPOS_TRIB_FEDERAL_COMPRA GFC ' +
    'on PGT.GRUPO_TRIB_FEDERAL_COMPRA_ID = GFC.GRUPO_TRIB_FEDERAL_COMPRA_ID ' +

    'inner join GRUPOS_TRIB_FEDERAL_VENDA GFV ' +
    'on PGT.GRUPO_TRIB_FEDERAL_VENDA_ID = GFV.GRUPO_TRIB_FEDERAL_VENDA_ID ';

  setFiltros(getFiltros);

  AddColuna('EMPRESA_ID', True);
  AddColuna('PRODUTO_ID', True);

  AddColuna('GRUPO_TRIB_ESTADUAL_COMPRA_ID');
  AddColunaSL('NOME_GRUPO_TRIB_ESTADUAL_COMP');

  AddColuna('GRUPO_TRIB_FEDERAL_COMPRA_ID');
  AddColunaSL('NOME_GRUPO_TRIB_FEDERAL_COMPRA');

  AddColuna('GRUPO_TRIB_ESTADUAL_VENDA_ID');
  AddColunaSL('NOME_GRUPO_TRIB_ESTADUAL_VENDA');

  AddColuna('GRUPO_TRIB_FEDERAL_VENDA_ID');
  AddColunaSL('NOME_GRUPO_TRIB_FEDERAL_VENDA');
end;

function TProdutosGruposTribEmpresas.getRecordProdutosGruposTribEmpresas: RecProdutosGruposTribEmpresas;
begin
  Result.EmpresaId                   := getInt('EMPRESA_ID', True);
  Result.ProdutoId                   := getInt('PRODUTO_ID', True);

  Result.GrupoTribEstadualCompraId   := getInt('GRUPO_TRIB_ESTADUAL_COMPRA_ID');
  Result.NomeGrupoTribEstadualCompra := getString('NOME_GRUPO_TRIB_ESTADUAL_COMP');

  Result.GrupoTribFederalCompraId    := getInt('GRUPO_TRIB_FEDERAL_COMPRA_ID');
  Result.NomeGrupoTribFederalCompra  := getString('NOME_GRUPO_TRIB_FEDERAL_COMPRA');

  Result.GrupoTribEstadualVendaId    := getInt('GRUPO_TRIB_ESTADUAL_VENDA_ID');
  Result.NomeGrupoTribEstadualVenda  := getString('NOME_GRUPO_TRIB_ESTADUAL_VENDA');

  Result.GrupoTribFederalVendaId     := getInt('GRUPO_TRIB_FEDERAL_VENDA_ID');
  Result.NomeGrupoTribFederalVenda   := getString('NOME_GRUPO_TRIB_FEDERAL_VENDA');
end;

function AtualizarProdutosGruposTribEmpresas(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pProdutoId: Integer;
  pGrupoTribEstadualCompraId: Integer;
  pGrupoTribFederalCompraId: Integer;
  pGrupoTribEstadualVendaId: Integer;
  pGrupoTribFederalVendaId: Integer
): RecRetornoBD;
var
  t: TProdutosGruposTribEmpresas;
begin
  Result.Iniciar;
  pConexao.setRotina('ATU_PROD_GRU_TRIB_EMPRESAS');

  t := TProdutosGruposTribEmpresas.Create(pConexao);

  try
    pConexao.IniciarTransacao; 

    t.setInt('EMPRESA_ID', pEmpresaId, True);
    t.setInt('PRODUTO_ID', pProdutoId, True);
    t.setInt('GRUPO_TRIB_ESTADUAL_COMPRA_ID', pGrupoTribEstadualCompraId);
    t.setInt('GRUPO_TRIB_FEDERAL_COMPRA_ID', pGrupoTribFederalCompraId);
    t.setInt('GRUPO_TRIB_ESTADUAL_VENDA_ID', pGrupoTribEstadualVendaId);
    t.setInt('GRUPO_TRIB_FEDERAL_VENDA_ID', pGrupoTribFederalVendaId);

    t.Inserir;

    pConexao.FinalizarTransacao; 
  except
    on e: Exception do begin
      pConexao.VoltarTransacao; 
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarProdutosGruposTribEmpresas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecProdutosGruposTribEmpresas>;
var
  i: Integer;
  t: TProdutosGruposTribEmpresas;
begin
  Result := nil;
  t := TProdutosGruposTribEmpresas.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordProdutosGruposTribEmpresas;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirProdutosGruposTribEmpresas(
  pConexao: TConexao;
  pProdutoId: Integer
): RecRetornoBD;
var
  t: TProdutosGruposTribEmpresas;
begin
  Result.Iniciar;
  t := TProdutosGruposTribEmpresas.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('PRODUTO_ID', pProdutoId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function getTributacoesFederal(pConexao: TConexao; pEmpresaId: Integer; pProdutoId: Integer): RecTributacoesFederal;
var
  vSql: TConsulta;
begin
  Result.ProdutoId := -1;

  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  PRODUTO_ID, ');
  vSql.Add('  EMPRESA_ID, ');

  vSql.Add('  GRUPO_TRIB_FEDERAL_COMPRA_ID, ');
  vSql.Add('  CST_PIS_COMPRA, ');
  vSql.Add('  CST_COFINS_COMPRA, ');
  vSql.Add('  ORIGEM_PRODUTO_COMPRA, ');
  vSql.Add('  NOME_GRUPO_TRIB_FEDRAL_COMPRA, ');

  vSql.Add('  GRUPO_TRIB_FEDERAL_VENDA_ID, ');
  vSql.Add('  CST_PIS_VENDA, ');
  vSql.Add('  CST_COFINS_VENDA, ');
  vSql.Add('  ORIGEM_PRODUTO_VENDA, ');
  vSql.Add('  NOME_GRUPO_TRIB_FEDERAL_VENDA ');
  vSql.Add('from ');
  vSql.Add('  VW_GRUPOS_TRIBUTACOES_FEDERAL ');
  vSql.Add('where EMPRESA_ID = :P1 ');
  vSql.Add('and PRODUTO_ID = :P2 ');

  if vSql.Pesquisar([pEmpresaId, pProdutoId]) then begin
    Result.ProdutoId                := vSql.GetInt('PRODUTO_ID');
    Result.EmpresaId                := vSql.GetInt('EMPRESA_ID');

    Result.GrupoTribFederalCompraId := vSql.GetInt('GRUPO_TRIB_FEDERAL_COMPRA_ID');
    Result.CstPisCompra             := vSql.GetString('CST_PIS_COMPRA');
    Result.CstCofinsCompra          := vSql.GetString('CST_COFINS_COMPRA');
    Result.OrigemProdutoCompra      := vSql.GetInt('ORIGEM_PRODUTO_COMPRA');

    Result.GrupoTribFederalVendaId  := vSql.GetInt('GRUPO_TRIB_FEDERAL_VENDA_ID');
    Result.CstPisVenda              := vSql.GetString('CST_PIS_VENDA');
    Result.CstCofinsVenda           := vSql.GetString('CST_COFINS_VENDA');
    Result.OrigemProdutoVenda       := vSql.GetInt('ORIGEM_PRODUTO_VENDA');
  end;

  vSql.Active := False;
  vSql.Free;
end;

function getTributacaoEstadual(pConexao: TConexao; pEmpresaId: Integer; pEstadoId: string; pProdutoId: Integer): RecTributacaoEstadual;
var
  vSql: TConsulta;
begin
  Result.ProdutoId := -1;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  PRODUTO_ID, ');
  vSql.Add('  EMPRESA_ID, ');
  vSql.Add('  ESTADO_ID_COMPRA, ');

  vSql.Add('  GRUPO_TRIB_ESTADUAL_COMPRA_ID, ');
  vSql.Add('  CST_REVENDA_COMPRA, ');
  vSql.Add('  CST_INDUSTRIA_COMPRA, ');
  vSql.Add('  CST_DISTRIBUIDORA_COMPRA, ');

  vSql.Add('  GRUPO_TRIB_ESTADUAL_VENDA_ID, ');
  vSql.Add('  CST_CLINICA_HOSPITAL_VENDA, ');
  vSql.Add('  CST_CONSTRUTORA_VENDA, ');
  vSql.Add('  CST_CONTRIBUINTE_VENDA, ');
  vSql.Add('  CST_NAO_CONTRIBUINTE_VENDA, ');
  vSql.Add('  CST_ORGAO_PUBLICO_VENDA, ');
  vSql.Add('  CST_PRODUTOR_RURAL_VENDA, ');
  vSql.Add('  CST_REVENDA_VENDA, ');
  vSql.Add('  PERC_ICMS_VENDA, ');
  vSql.Add('  INDICE_REDUCAO_BASE_ICMS_VENDA, ');
  vSql.Add('  IVA_VENDA ');
  vSql.Add('from ');
  vSql.Add('  VW_GRUPOS_TRIBUTACOES_ESTADUAL ');

  vSql.Add('where EMPRESA_ID = :P1 ');
  vSql.Add('and ESTADO_ID_COMPRA = :P2 ');
  vSql.Add('and PRODUTO_ID = :P3 ');

  if vSql.Pesquisar([pEmpresaId, pEstadoId, pProdutoId]) then begin
    Result.ProdutoId                  := vSql.GetInt('PRODUTO_ID');

    Result.EmpresaId                  := vSql.GetInt('EMPRESA_ID');
    Result.EstadoIdCompra             := vSql.GetString('ESTADO_ID_COMPRA');
    Result.EstadoIdVenda              := vSql.GetString('PRODUTO_ID');

    Result.GrupoTribEstadualCompraId  := vSql.GetInt('GRUPO_TRIB_ESTADUAL_COMPRA_ID');
    Result.CstRevendaCompra           := vSql.GetString('CST_REVENDA_COMPRA');
    Result.CstIndustriaCompra         := vSql.GetString('CST_INDUSTRIA_COMPRA');
    Result.CstDistribuidoraCompra     := vSql.GetString('CST_DISTRIBUIDORA_COMPRA');

    Result.GrupoTribEstadualVendaId   := vSql.GetInt('GRUPO_TRIB_ESTADUAL_VENDA_ID');
    Result.CstClinicaHospitalVenda    := vSql.GetString('CST_CLINICA_HOSPITAL_VENDA');
    Result.CstConstrutoraVenda        := vSql.GetString('CST_CONSTRUTORA_VENDA');
    Result.CstContribuinteVenda       := vSql.GetString('CST_CONTRIBUINTE_VENDA');
    Result.CstNaoContribuinteVenda    := vSql.GetString('CST_NAO_CONTRIBUINTE_VENDA');
    Result.CstOrgaoPublicoVenda       := vSql.GetString('CST_ORGAO_PUBLICO_VENDA');
    Result.CstProdutorRuralVenda      := vSql.GetString('CST_PRODUTOR_RURAL_VENDA');
    Result.CstRevendaVenda            := vSql.GetString('CST_REVENDA_VENDA');

    Result.PercIcmsVenda              := vSql.GetDouble('PERC_ICMS_VENDA');
    Result.IndiceReducaoBaseIcmsVenda := vSql.GetDouble('INDICE_REDUCAO_BASE_ICMS_VENDA');
    Result.IvaVenda                   := vSql.GetDouble('IVA_VENDA');
  end;

  vSql.Active := False;
  vSql.Free;
end;

end.
