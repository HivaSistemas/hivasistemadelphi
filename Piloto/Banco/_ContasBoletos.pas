unit _ContasBoletos;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils,
  System.Classes;

{$M+}
type
  RecContasBoletos = class
  public
    ContaboletoId: Integer;
    Aceite: string;
    Caracteristica: string;
    Carteira: string;
    Convenio: string;
    Descricao: string;
    DiasBaixaDevolucao: Integer;
    DiasLimitePagamento: Integer;
    DiasProtesto: Integer;
    DiasMultaJuros: Integer;
    DigitoAgencia: string;
    Especie: string;
    EspecieMoeda: string;
    ExtArquivoRemessa: string;
    FimNossoNumero: Integer;
    InicioNossoNumero: Integer;
    Instrucao1: string;
    Instrucao2: string;
    Instrucao3: string;
    Layout: string;
    LocalPagamento: string;
    Modalidade: string;
    ModeloImpressao: string;
    NomeArquivoRemessa: string;
    NumeroAgencia: Integer;
    NumeroBanco: string;
    NumeroConta: Integer;
    Operacao: string;
    ProximoNossoNumero: Integer;
    RespEmissao: string;
    SequencialRemessa: Integer;
    TextoLivre: string;
    TipoCarteira: string;
    VersaoLayout: string;
    VersaoLote: string;
    Transmissao: string;
    DigitoConta: string;
    Cedente: string;
    DvAgenciaConta: string;
    Logo : TMemoryStream;
    LayoutImpressao : TMemoryStream;
    TipoNegativacao: string;
  end;

  TContasBoletos = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordContasBoletos: RecContasBoletos;
  end;
function AtualizarProximoNossoNumero(pConexao: TConexao; pContaboletoId: Integer; pProximoNossoNumero: string): RecRetornoBD;
function AtualizarProximoNumeroRemessa(pConexao: TConexao; pContaboletoId: Integer; pProximoNumeroRemessa: integer): RecRetornoBD;

function AtualizarContasBoletos(
  pConexao: TConexao;
  pContaboletoId: Integer;
  pAceite: string;
  pCaracteristica: string;
  pCarteira: string;
  pConvenio: string;
  pDescricao: string;
  pDiasBaixaDevolucao: Integer;
  pDiasLimitePagamento: Integer;
  pDiasProtesto: Integer;
  pDiasMultaJuros: Integer;
  pDigitoAgencia: string;
  pEspecie: string;
  pEspecieMoeda: string;
  pExtArquivoRemessa: string;
  pFimNossoNumero: Integer;
  pInicioNossoNumero: Integer;
  pInstrucao1: string;
  pInstrucao2: string;
  pInstrucao3: string;
  pLayout: string;
  pLocalPagamento: string;
  pModalidade: string;
  pModeloImpressao: string;
  pNomeArquivoRemessa: string;
  pNumeroAgencia: Integer;
  pNumeroBanco: string;
  pNumeroConta: Integer;
  pOperacao: string;
  pProximoNossoNumero: Integer;
  pRespEmissao: string;
  pSequencialRemessa: Integer;
  pTextoLivre: string;
  pTipoCarteira: string;
  pVersaoLayout: string;
  pVersaoLote: string;
  pTransmissao: string;
  pDigitoConta: string;
  pCedente: string;
  pDvAgenciaConta: string;
  pLogo: TMemoryStream;
  pLayoutImpressao : TMemoryStream;
  pTipoNegativacao: string
): RecRetornoBD;

function BuscarContasBoletos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecContasBoletos>;

function ExcluirContasBoletos(
  pConexao: TConexao;
  pContaboletoId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TContasBoletos }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 3);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'C�digo',
      True,
      0,
      'where CONTABOLETO_ID = :P1'
    );
  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Descri��o',
      True,
      1,
      'where DESCRICAO like :P1 || ''%'' '
    );
  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      2,
      'where CONTABOLETO_ID = :P1'
     +' FOR UPDATE '
    );
end;

constructor TContasBoletos.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONTAS_BOLETOS');

  FSql :=
    'select ' +
    '  CONTABOLETO_ID, ' +
    '  ACEITE, ' +
    '  CARACTERISTICA, ' +
    '  CARTEIRA, ' +
    '  CONVENIO, ' +
    '  DESCRICAO, ' +
    '  DIAS_BAIXA_DEVOLUCAO, ' +
    '  DIAS_LIMITE_PAGAMENTO, ' +
    '  DIAS_PROTESTO, ' +
    '  DIAS_MULTA_JUROS, '+
    '  DIGITO_AGENCIA, ' +
    '  ESPECIE, ' +
    '  ESPECIE_MOEDA, ' +
    '  EXT_ARQUIVO_REMESSA, ' +
    '  FIM_NOSSO_NUMERO, ' +
    '  INICIO_NOSSO_NUMERO, ' +
    '  INSTRUCAO1, ' +
    '  INSTRUCAO2, ' +
    '  INSTRUCAO3, ' +
    '  LAYOUT, ' +
    '  LOCAL_PAGAMENTO, ' +
    '  MODALIDADE, ' +
    '  MODELO_IMPRESSAO, ' +
    '  NOME_ARQUIVO_REMESSA, ' +
    '  NUMERO_AGENCIA, ' +
    '  NUMERO_BANCO, ' +
    '  NUMERO_CONTA, ' +
    '  OPERACAO, ' +
    '  PROXIMO_NOSSO_NUMERO, ' +
    '  RESP_EMISSAO, ' +
    '  SEQUENCIAL_REMESSA, ' +
    '  TEXTO_LIVRE, ' +
    '  TIPO_CARTEIRA, ' +
    '  VERSAO_LAYOUT, ' +
    '  VERSAO_LOTE, ' +
    '  TRANSMISSAO, ' +
    '  DIGITO_CONTA, ' +
    '  CEDENTE, ' +
    '  DV_AGENCIA_CONTA, ' +
    '  LOGO, ' +
    '  LAYOUT_IMPRESSAO, ' +
    '  TIPO_NEGATIVACAO ' +
    'from ' +
    '  CONTAS_BOLETOS';

  setFiltros(getFiltros);

  AddColunaSL('CONTABOLETO_ID', True);
  AddColuna('ACEITE');
  AddColuna('CARACTERISTICA');
  AddColuna('CARTEIRA');
  AddColuna('CONVENIO');
  AddColuna('DESCRICAO');
  AddColuna('DIAS_BAIXA_DEVOLUCAO');
  AddColuna('DIAS_LIMITE_PAGAMENTO');
  AddColuna('DIAS_PROTESTO');
  AddColuna('DIAS_MULTA_JUROS');
  AddColuna('DIGITO_AGENCIA');
  AddColuna('ESPECIE');
  AddColuna('ESPECIE_MOEDA');
  AddColuna('EXT_ARQUIVO_REMESSA');
  AddColuna('FIM_NOSSO_NUMERO');
  AddColuna('INICIO_NOSSO_NUMERO');
  AddColuna('INSTRUCAO1');
  AddColuna('INSTRUCAO2');
  AddColuna('INSTRUCAO3');
  AddColuna('LAYOUT');
  AddColuna('LOCAL_PAGAMENTO');
  AddColuna('MODALIDADE');
  AddColuna('MODELO_IMPRESSAO');
  AddColuna('NOME_ARQUIVO_REMESSA');
  AddColuna('NUMERO_AGENCIA');
  AddColuna('NUMERO_BANCO');
  AddColuna('NUMERO_CONTA');
  AddColuna('OPERACAO');
  AddColuna('PROXIMO_NOSSO_NUMERO');
  AddColuna('RESP_EMISSAO');
  AddColuna('SEQUENCIAL_REMESSA');
  AddColuna('TEXTO_LIVRE');
  AddColuna('TIPO_CARTEIRA');
  AddColuna('VERSAO_LAYOUT');
  AddColuna('VERSAO_LOTE');
  AddColuna('TRANSMISSAO');
  AddColuna('DIGITO_CONTA');
  AddColuna('CEDENTE');
  AddColuna('DV_AGENCIA_CONTA');
  AddColuna('LOGO');
  AddColuna('LAYOUT_IMPRESSAO');
  AddColuna('TIPO_NEGATIVACAO');
end;

function TContasBoletos.getRecordContasBoletos: RecContasBoletos;
begin
  Result := RecContasBoletos.Create;

  Result.ContaboletoId              := getInt('CONTABOLETO_ID', True);
  Result.Aceite                     := getString('ACEITE');
  Result.Caracteristica             := getString('CARACTERISTICA');
  Result.Carteira                   := getString('CARTEIRA');
  Result.Convenio                   := getString('CONVENIO');
  Result.Descricao                  := getString('DESCRICAO');
  Result.DiasBaixaDevolucao         := getInt('DIAS_BAIXA_DEVOLUCAO');
  Result.DiasLimitePagamento        := getInt('DIAS_LIMITE_PAGAMENTO');
  Result.DiasProtesto               := getInt('DIAS_PROTESTO');
  Result.DiasMultaJuros             := getInt('DIAS_MULTA_JUROS');
  Result.DigitoAgencia              := getString('DIGITO_AGENCIA');
  Result.Especie                    := getString('ESPECIE');
  Result.EspecieMoeda               := getString('ESPECIE_MOEDA');
  Result.ExtArquivoRemessa          := getString('EXT_ARQUIVO_REMESSA');
  Result.FimNossoNumero             := getInt('FIM_NOSSO_NUMERO');
  Result.InicioNossoNumero          := getInt('INICIO_NOSSO_NUMERO');
  Result.Instrucao1                 := getString('INSTRUCAO1');
  Result.Instrucao2                 := getString('INSTRUCAO2');
  Result.Instrucao3                 := getString('INSTRUCAO3');
  Result.Layout                     := getString('LAYOUT');
  Result.LocalPagamento             := getString('LOCAL_PAGAMENTO');
  Result.Modalidade                 := getString('MODALIDADE');
  Result.ModeloImpressao            := getString('MODELO_IMPRESSAO');
  Result.NomeArquivoRemessa         := getString('NOME_ARQUIVO_REMESSA');
  Result.NumeroAgencia              := getInt('NUMERO_AGENCIA');
  Result.NumeroBanco                := getString('NUMERO_BANCO');
  Result.NumeroConta                := getInt('NUMERO_CONTA');
  Result.Operacao                   := getString('OPERACAO');
  Result.ProximoNossoNumero         := getInt('PROXIMO_NOSSO_NUMERO');
  Result.RespEmissao                := getString('RESP_EMISSAO');
  Result.SequencialRemessa          := getInt('SEQUENCIAL_REMESSA');
  Result.TextoLivre                 := getString('TEXTO_LIVRE');
  Result.TipoCarteira               := getString('TIPO_CARTEIRA');
  Result.VersaoLayout               := getString('VERSAO_LAYOUT');
  Result.VersaoLote                 := getString('VERSAO_LOTE');
  Result.Transmissao                := getString('TRANSMISSAO');
  Result.DigitoConta                := getString('DIGITO_CONTA');
  Result.Cedente                    := getString('CEDENTE');
  Result.DvAgenciaConta             := getString('DV_AGENCIA_CONTA');
  Result.TipoNegativacao            := getString('TIPO_NEGATIVACAO');
end;

function AtualizarProximoNossoNumero(pConexao: TConexao; pContaboletoId: Integer; pProximoNossoNumero: string): RecRetornoBD;
var
  vExecucao: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATU_PROXIMO_NOSSO_NUMERO');
  vExecucao := TExecucao.Create(pConexao);

  vExecucao.Add('update CONTAS_BOLETOS set ');
  vExecucao.Add('  PROXIMO_NOSSO_NUMERO = :P2 ');
  vExecucao.Add('where CONTABOLETO_ID = :P1');
  try
    if not pConexao.EmTransacao then
      pConexao.IniciarTransacao;

    vExecucao.Executar([pContaboletoId, pProximoNossoNumero]);
    if not pConexao.EmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pConexao.EmTransacao then
        pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
end;

function AtualizarProximoNumeroRemessa(pConexao: TConexao; pContaboletoId: Integer; pProximoNumeroRemessa: integer): RecRetornoBD;
var
  vExecucao: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATU_PROXIMO_NUMERO_REMESSA');
  vExecucao := TExecucao.Create(pConexao);

  vExecucao.Add('update CONTAS_BOLETOS set ');
  vExecucao.Add('  SEQUENCIAL_REMESSA = :P2 ');
  vExecucao.Add('where CONTABOLETO_ID = :P1');
  try
    pConexao.IniciarTransacao;

    vExecucao.Executar([pContaboletoId, pProximoNumeroRemessa]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pConexao.EmTransacao then
        pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

end;

function AtualizarContasBoletos(
  pConexao: TConexao;
  pContaboletoId: Integer;
  pAceite: string;
  pCaracteristica: string;
  pCarteira: string;
  pConvenio: string;
  pDescricao: string;
  pDiasBaixaDevolucao: Integer;
  pDiasLimitePagamento: Integer;
  pDiasProtesto: Integer;
  pDiasMultaJuros: Integer;
  pDigitoAgencia: string;
  pEspecie: string;
  pEspecieMoeda: string;
  pExtArquivoRemessa: string;
  pFimNossoNumero: Integer;
  pInicioNossoNumero: Integer;
  pInstrucao1: string;
  pInstrucao2: string;
  pInstrucao3: string;
  pLayout: string;
  pLocalPagamento: string;
  pModalidade: string;
  pModeloImpressao: string;
  pNomeArquivoRemessa: string;
  pNumeroAgencia: Integer;
  pNumeroBanco: string;
  pNumeroConta: Integer;
  pOperacao: string;
  pProximoNossoNumero: Integer;
  pRespEmissao: string;
  pSequencialRemessa: Integer;
  pTextoLivre: string;
  pTipoCarteira: string;
  pVersaoLayout: string;
  pVersaoLote: string;
  pTransmissao: string;
  pDigitoConta: string;
  pCedente: string;
  pDvAgenciaConta: string;
  pLogo: TMemoryStream;
  pLayoutImpressao : TMemoryStream;
  pTipoNegativacao: string
): RecRetornoBD;
var
  t: TContasBoletos;
  vNovo: Boolean;
  vExec: TExecucao;
  vSeq: TSequencia;
begin
  Result.Iniciar;
  pConexao.setRotina('ATUALIZAR_CONTA_BOLETOS');

  t := TContasBoletos.Create(pConexao);
  vExec := TExecucao.Create(pConexao);
  vNovo := pContaboletoId = 0;

  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_CONTAS_BOLETOS_ID');
    pContaboletoId := vSeq.getProximaSequencia;
    result.AsInt := pContaboletoId;
    vSeq.Free;
  end;

  try
    pConexao.IniciarTransacao;
    t.setInt('CONTABOLETO_ID', pContaboletoId, True);
    t.setString('ACEITE', pAceite);
    t.setString('CARACTERISTICA', pCaracteristica);
    t.setString('CARTEIRA', pCarteira);
    t.setString('CONVENIO', pConvenio);
    t.setString('DESCRICAO', pDescricao);
    t.setInt('DIAS_BAIXA_DEVOLUCAO', pDiasBaixaDevolucao);
    t.setInt('DIAS_LIMITE_PAGAMENTO', pDiasLimitePagamento);
    t.setInt('DIAS_PROTESTO', pDiasProtesto);
    t.setInt('DIAS_MULTA_JUROS', pDiasMultaJuros);
    t.setString('DIGITO_AGENCIA', pDigitoAgencia);
    t.setString('ESPECIE', pEspecie);
    t.setString('ESPECIE_MOEDA', pEspecieMoeda);
    t.setString('EXT_ARQUIVO_REMESSA', pExtArquivoRemessa);
    t.setInt('FIM_NOSSO_NUMERO', pFimNossoNumero);
    t.setInt('INICIO_NOSSO_NUMERO', pInicioNossoNumero);
    t.setString('INSTRUCAO1', pInstrucao1);
    t.setString('INSTRUCAO2', pInstrucao2);
    t.setString('INSTRUCAO3', pInstrucao3);
    t.setString('LAYOUT', pLayout);
    t.setString('LOCAL_PAGAMENTO', pLocalPagamento);
    t.setString('MODALIDADE', pModalidade);
    t.setString('MODELO_IMPRESSAO', pModeloImpressao);
    t.setString('NOME_ARQUIVO_REMESSA', pNomeArquivoRemessa);
    t.setInt('NUMERO_AGENCIA', pNumeroAgencia);
    t.setString('NUMERO_BANCO', pNumeroBanco);
    t.setInt('NUMERO_CONTA', pNumeroConta);
    t.setString('OPERACAO', pOperacao);
    t.setInt('PROXIMO_NOSSO_NUMERO', pProximoNossoNumero);
    t.setString('RESP_EMISSAO', pRespEmissao);
    t.setInt('SEQUENCIAL_REMESSA', pSequencialRemessa);
    t.setString('TEXTO_LIVRE', pTextoLivre);
    t.setString('TIPO_CARTEIRA', pTipoCarteira);
    t.setString('TIPO_NEGATIVACAO', pTipoNegativacao);
    t.setString('VERSAO_LAYOUT', pVersaoLayout);
    t.setString('VERSAO_LOTE', pVersaoLote);
    t.setString('TRANSMISSAO', pTransmissao);
    t.setString('DIGITO_CONTA', pDigitoConta);
    t.setString('CEDENTE', pCedente);
    t.setString('DV_AGENCIA_CONTA', pDvAgenciaConta);


    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    vExec.Clear;
    vExec.Add('update CONTAS_BOLETOS set ');
    vExec.Add('  LOGO = :P1, ');
    vExec.Add('  LAYOUT_IMPRESSAO = :P2 ');

    vExec.Add('where CONTABOLETO_ID = :P3');

    vExec.CarregarBinario('P1', pLogo);
    vExec.CarregarClob('P2', pLayoutImpressao);

    vExec.ParamByName('P3').AsInteger := pContaboletoId;
    vExec.Executar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarContasBoletos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecContasBoletos>;
var
  i: Integer;
  t: TContasBoletos;
  procedure CarregarLogoLayoutImpressao;
  var
    vSql: TConsulta;
  begin
    vSql := TConsulta.Create(pConexao);

    with vSql.SQL do begin
      Add('select ');
      Add('  LOGO, ');
      Add('  LAYOUT_IMPRESSAO ');
      Add('from ');
      Add('  CONTAS_BOLETOS ');
      Add('where CONTABOLETO_ID = :P1 ');
      Add('and LOGO is not null ');
    end;

    if vSql.Pesquisar([Result[i].ContaboletoId]) then begin
      Result[i].Logo            := vSql.getFoto(0);
      Result[i].LayoutImpressao := vSql.getFoto(1);
    end;
    vSql.Active := False;
    vSql.Free;
  end;

begin
  Result := nil;
  t := TContasBoletos.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordContasBoletos;
      CarregarLogoLayoutImpressao;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirContasBoletos(
  pConexao: TConexao;
  pContaboletoId: Integer
): RecRetornoBD;
var
  t: TContasBoletos;
begin
  Result.Iniciar;
  t := TContasBoletos.Create(pConexao);
  t.setInt('CONTABOLETO_ID', pContaboletoId, True);

  try
    pConexao.IniciarTransacao;

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.

