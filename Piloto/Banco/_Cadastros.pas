unit _Cadastros;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros,
  System.Variants;

{$M+}
type
  TCadastro = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
    procedure setCadastro(pCadastro: RecCadastros);
  protected
    function GetRecordCadastro: RecCadastros;
    function BuscarCadastros(pIndice: ShortInt; pFiltros: array of Variant): Boolean;
  end;

function BuscarCadastros(pConexao: TConexao; pIndice: Integer; pFiltros: array of Variant): TArray<RecCadastros>;
function EFornecedor(pConexao: TConexao; pCadastroId: Integer): Boolean;

function ExcluirCadastro(
  pConexao: TConexao;
  pCadastroId: Integer
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TCadastro }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 5);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'C�digo do cadastro',
      False,
      0,
      'where CAD.CADASTRO_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome fantasia (Avan�ado)',
      False,
      1,
      'where CAD.NOME_FANTASIA like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  CAD.NOME_FANTASIA ',
      '',
      True
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Raz�o social (Avan�ado)',
      True,
      2,
      'where CAD.RAZAO_SOCIAL like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  CAD.RAZAO_SOCIAL ',
      '',
      True
    );

  Result[3] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'CPF/CNPJ (avan�ado)',
      False,
      3,
      'where RETORNA_NUMEROS(CAD.CPF_CNPJ) like ''%'' || RETORNA_NUMEROS(:P1) || ''%'' ',
      'order by ' +
      '  CAD.NOME_FANTASIA ',
      '',
      True
    );

  Result[4] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where (CAD.CPF_CNPJ = :P1 or retorna_numeros(CAD.CPF_CNPJ) = :P1) ' +
      'and CAD.CADASTRO_ID <> :P2 ' +
      'order by ' +
      '  CAD.NOME_FANTASIA '
    );
end;

constructor TCadastro.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CADASTROS');

  FSql :=
    'select ' +
    '  CAD.CADASTRO_ID, ' +
    '  CAD.NOME_FANTASIA, ' +
    '  CAD.RAZAO_SOCIAL, ' +
    '  CAD.TIPO_PESSOA, ' +
    '  CAD.CPF_CNPJ, ' +
    '  CAD.LOGRADOURO, ' +
    '  CAD.COMPLEMENTO, ' +
    '  CAD.NUMERO, ' +
    '  CAD.BAIRRO_ID, ' +
    '  BAI.NOME_BAIRRO, ' +
    '  BAI.NOME_CIDADE, ' +
    '  CAD.PONTO_REFERENCIA, ' +
    '  CAD.CEP, ' +
    '  CAD.TELEFONE_PRINCIPAL, ' +
    '  CAD.TELEFONE_CELULAR, ' +
    '  CAD.TELEFONE_FAX, ' +
    '  CAD.E_MAIL, ' +
    '  CAD.SEXO, ' +
    '  CAD.ESTADO_CIVIL, ' +
    '  CAD.DATA_NASCIMENTO, ' +
    '  CAD.DATA_CADASTRO, ' +
    '  CAD.INSCRICAO_ESTADUAL, ' +
    '  CAD.CNAE, ' +
    '  CAD.RG, ' +
    '  CAD.ORGAO_EXPEDIDOR_RG, ' +
    '  CAD.E_CLIENTE, ' +
    '  CAD.E_FORNECEDOR, ' +
    '  CAD.E_MOTORISTA, ' +
    '  CAD.E_TRANSPORTADORA, ' +
    '  CAD.E_PROFISSIONAL, ' +
    '  CAD.ATIVO, ' +
    '  CID.ESTADO_ID, ' +
    '  CAD.OBSERVACOES ' +
    'from ' +
    '  CADASTROS CAD ' +

    'inner join VW_BAIRROS BAI ' +
    'on CAD.BAIRRO_ID = BAI.BAIRRO_ID ' +

    'inner join CIDADES CID ' +
    'on BAI.CIDADE_ID = CID.CIDADE_ID ';

  SetFiltros(GetFiltros);

  AddColuna('CADASTRO_ID', True);
  AddColuna('NOME_FANTASIA');
  AddColuna('RAZAO_SOCIAL');
  AddColuna('TIPO_PESSOA');
  AddColuna('CPF_CNPJ');
  AddColuna('LOGRADOURO');
  AddColuna('COMPLEMENTO');
  AddColuna('NUMERO');
  AddColuna('BAIRRO_ID');
  AddColunaSL('NOME_BAIRRO');
  AddColunaSL('NOME_CIDADE');
  AddColuna('PONTO_REFERENCIA');
  AddColuna('CEP');
  AddColuna('TELEFONE_PRINCIPAL');
  AddColuna('TELEFONE_CELULAR');
  AddColuna('TELEFONE_FAX');
  AddColuna('E_MAIL');
  AddColuna('SEXO');
  AddColuna('ESTADO_CIVIL');
  AddColuna('DATA_NASCIMENTO');
  AddColunaSL('DATA_CADASTRO');
  AddColuna('INSCRICAO_ESTADUAL');
  AddColuna('CNAE');
  AddColuna('RG');
  AddColuna('ORGAO_EXPEDIDOR_RG');
  AddColuna('E_CLIENTE');
  AddColuna('E_FORNECEDOR');
  AddColuna('E_MOTORISTA');
  AddColuna('E_TRANSPORTADORA');
  AddColuna('E_PROFISSIONAL');
  AddColuna('ATIVO');
  AddColunaSL('ESTADO_ID');
  AddColuna('OBSERVACOES');
end;

function TCadastro.GetRecordCadastro: RecCadastros;
begin
  Result := RecCadastros.Create;
  Result.cadastro_id        := GetInt('CADASTRO_ID', True);
  Result.nome_fantasia      := GetString('NOME_FANTASIA');
  Result.razao_social       := GetString('RAZAO_SOCIAL');
  Result.tipo_pessoa        := GetString('TIPO_PESSOA');
  Result.cpf_cnpj           := GetString('CPF_CNPJ');
  Result.logradouro         := GetString('LOGRADOURO');
  Result.complemento        := GetString('COMPLEMENTO');
  Result.numero             := GetString('NUMERO');
  Result.bairro_id          := GetInt('BAIRRO_ID');
  Result.NomeBairro         := GetString('NOME_BAIRRO');
  Result.NomeCidade         := GetString('NOME_CIDADE');
  Result.ponto_referencia   := GetString('PONTO_REFERENCIA');
  Result.cep                := GetString('CEP');
  Result.TelefonePrincipal  := GetString('TELEFONE_PRINCIPAL');
  Result.TelefoneCelular    := GetString('TELEFONE_CELULAR');
  Result.TelefoneFax        := GetString('TELEFONE_FAX');
  Result.e_mail             := GetString('E_MAIL');
  Result.sexo               := GetString('SEXO');
  Result.estado_civil       := GetString('ESTADO_CIVIL');
  Result.data_nascimento    := getData('DATA_NASCIMENTO');
  Result.data_cadastro      := GetData('DATA_CADASTRO');
  Result.inscricao_estadual := GetString('INSCRICAO_ESTADUAL');
  Result.cnae               := GetString('CNAE');
  Result.rg                 := GetString('RG');
  Result.orgao_expedidor_rg := GetString('ORGAO_EXPEDIDOR_RG');
  Result.e_cliente          := GetString('E_CLIENTE');
  Result.e_fornecedor       := GetString('E_FORNECEDOR');
  Result.EMotorista         := GetString('E_MOTORISTA');
  Result.ETransportadora    := GetString('E_TRANSPORTADORA');
  Result.EProfissional      := GetString('E_PROFISSIONAL');
  Result.ativo              := GetString('ATIVO');
  Result.estado_id          := GetString('ESTADO_ID');
  Result.Observacoes        := GetString('OBSERVACOES');
end;

procedure TCadastro.setCadastro(pCadastro: RecCadastros);
begin
  Self.SetInt('CADASTRO_ID', pCadastro.cadastro_id, True);
  Self.SetString('NOME_FANTASIA', pCadastro.nome_fantasia);
  Self.SetString('RAZAO_SOCIAL', pCadastro.razao_social);
  Self.SetString('TIPO_PESSOA', pCadastro.tipo_pessoa);
  Self.SetString('CPF_CNPJ', pCadastro.cpf_cnpj);
  Self.SetString('LOGRADOURO', pCadastro.logradouro);
  Self.SetString('COMPLEMENTO', pCadastro.complemento);
  Self.setString('NUMERO', pCadastro.numero);
  Self.SetInt('BAIRRO_ID', pCadastro.bairro_id);
  Self.SetString('PONTO_REFERENCIA', pCadastro.ponto_referencia);
  Self.SetString('CEP', pCadastro.cep);
  Self.SetString('TELEFONE_PRINCIPAL', pCadastro.TelefonePrincipal);
  Self.SetString('TELEFONE_CELULAR', pCadastro.TelefoneCelular);
  Self.SetString('TELEFONE_FAX', pCadastro.TelefoneFax);
  Self.SetString('E_MAIL', pCadastro.e_mail);
  Self.SetString('SEXO', pCadastro.sexo);
  Self.SetString('ESTADO_CIVIL', pCadastro.estado_civil);
  Self.SetDataN('DATA_NASCIMENTO', pCadastro.data_nascimento);
  Self.SetString('INSCRICAO_ESTADUAL', pCadastro.inscricao_estadual);
  Self.SetString('CNAE', pCadastro.cnae);
  Self.SetString('RG', pCadastro.rg);
  Self.SetString('ORGAO_EXPEDIDOR_RG', pCadastro.orgao_expedidor_rg);
  Self.SetString('OBSERVACOES', pCadastro.Observacoes);
  Self.SetString('E_CLIENTE', pCadastro.e_cliente);
  Self.SetString('E_FORNECEDOR', pCadastro.e_fornecedor);
  Self.SetString('E_MOTORISTA', pCadastro.EMotorista);
  Self.SetString('E_TRANSPORTADORA', pCadastro.ETransportadora);
  Self.SetString('E_PROFISSIONAL', pCadastro.EProfissional);
  Self.SetString('ATIVO', pCadastro.ativo);

end;

function TCadastro.BuscarCadastros(
  pIndice: ShortInt;
  pFiltros: array of Variant
): Boolean;
begin
  Result := Pesquisar(pIndice, pFiltros);
end;

function ExcluirCadastro(
  pConexao: TConexao;
  pCadastroId: Integer
): RecRetornoBD;
var
  t: TCadastro;
begin
  Result.TeveErro := False;
  t := TCadastro.Create(pConexao);

  t.SetInt('CADASTRO_ID', pCadastroId, True);

  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function BuscarCadastros(pConexao: TConexao; pIndice: Integer; pFiltros: array of Variant): TArray<RecCadastros>;
var
  i: Integer;
  t: TCadastro;
begin
  Result := nil;
  t := TCadastro.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordCadastro;

      t.ProximoRegistro;
    end;
  end;
  t.Free;
end;

function EFornecedor(pConexao: TConexao; pCadastroId: Integer): Boolean;
var
  vSql: TConsulta;
begin
  Result := False;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add(' E_FORNECEDOR ');
  vSql.Add('from ');
  vSql.Add('  CADASTROS ');
  vSql.Add('where CADASTRO_ID = :P1 ');

  if vSql.Pesquisar([pCadastroId]) then
    Result := vSql.GetString('E_FORNECEDOR') = 'S';

  vSql.Free;
end;

end.
