unit _RelacaoContasReceberBaixas;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsRelatorios, _Biblioteca;

function BuscarContasReceberBaixas(pConexao: TConexao; pComando: string): TArray<RecContasReceberBaixas>;

implementation

function BuscarContasReceberBaixas(pConexao: TConexao; pComando: string): TArray<RecContasReceberBaixas>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  with vSql.SQL do begin
    Add('select ');
    Add('  CRB.BAIXA_ID, ');
    Add('  CRB.USUARIO_BAIXA_ID, ');
    Add('  FBA.NOME as NOME_USUARIO_BAIXA, ');
    Add('  CRB.VALOR_DINHEIRO, ');
    Add('  CRB.VALOR_CHEQUE, ');
    Add('  CRB.VALOR_CARTAO_DEBITO + CRB.VALOR_CARTAO_CREDITO as VALOR_CARTAO, ');
    Add('  CRB.VALOR_COBRANCA, ');
    Add('  CRB.VALOR_CREDITO, ');
    Add('  CRB.VALOR_PIX, ');
    Add('  CRB.DATA_HORA_BAIXA, ');
    Add('  CRB.DATA_PAGAMENTO, ');
    Add('  CRB.OBSERVACOES, ');
    Add('  CRB.EMPRESA_ID, ');
    Add('  CRB.TIPO, ');
    Add('  EMP.NOME_FANTASIA as NOME_EMPRESA ');
    Add('from ');
    Add('  CONTAS_RECEBER_BAIXAS CRB ');

    Add('inner join FUNCIONARIOS FBA ');
    Add('on CRB.USUARIO_BAIXA_ID = FBA.FUNCIONARIO_ID ');

    Add('inner join EMPRESAS EMP ');
    Add('on CRB.EMPRESA_ID = EMP.EMPRESA_ID ');

    Add(pComando);
  end;

  if vSql.Pesquisar() then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].baixa_id           := vSql.GetInt('BAIXA_ID');
      Result[i].usuario_baixa_id   := vSql.GetInt('USUARIO_BAIXA_ID');
      Result[i].nome_usuario_baixa := vSql.GetString('NOME_USUARIO_BAIXA');
      Result[i].valor_dinheiro     := vSql.GetDouble('VALOR_DINHEIRO');
      Result[i].valor_cheque       := vSql.GetDouble('VALOR_CHEQUE');
      Result[i].valor_cartao       := vSql.GetDouble('VALOR_CARTAO');
      Result[i].valor_cobranca     := vSql.GetDouble('VALOR_COBRANCA');
      Result[i].valor_credito      := vSql.GetDouble('VALOR_CREDITO');
      Result[i].valor_pix          := vSql.GetDouble('VALOR_PIX');
      Result[i].data_baixa         := vSql.GetData('DATA_HORA_BAIXA');
      Result[i].data_pagamento     := vSql.GetData('DATA_PAGAMENTO');
      Result[i].observacoes        := vSql.GetString('OBSERVACOES');
      Result[i].empresa_id         := vSql.GetInt('EMPRESA_ID');
      Result[i].nome_empresa       := vSql.GetString('NOME_EMPRESA');
      Result[i].Tipo               := vSql.GetString('TIPO');

      Result[i].TipoAnalitico :=
        _Biblioteca.Decode(
          Result[i].Tipo,[
          'CRE', 'Cr�dito a pagar',
          'BCR', 'Baixa de t�tulos a rec.',
          'ECR', 'Encontro de t�tulos a rec.',
          'ADI', 'Adiantamento',
          'PIX', 'Recebimento Pix']
        );

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

end.
