unit _EntregasPendentes;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, _EntregasItensPendentes;

{$M+}
type
  RecEntregasPendentes = record
    OrcamentoId: Integer;
    ClienteId: Integer;
    NomeCliente: string;
    VendedorId: Integer;
    NomeVendedor: string;
    DataCadastro: TDateTime;
    ObservacoesExpedicao: string;
    PrevisaoEntrega: TDateTime;
    LocalId: Integer;
    EmpresaId: Integer;
    NomeEmpresa: string;
    NomeLocal: string;
    TipoRetornoEntrega: string;
    RotaId: Integer;
    NomeRota: string;
  end;

  TEntregasPendentes = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordEntregasPendentes: RecEntregasPendentes;
  end;

function BuscarEntregasPendentesComando(pConexao: TConexao; pComando: string): TArray<RecEntregasPendentes>;

function GerarEntregaPendencia(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pEmpresaPendenciaId: Integer;
  pLocalId: Integer;
  pPrevisaoEntrega: TDateTime;
  pAgrupamentoId: Integer;
  pTipoNotaGerar: string;
  pProdutos: TArray<RecEntregasItensPendentes>;
  pUtilizaControleManifesto: Boolean
): RecRetornoBD;

function GerarRetiradaPendenciaEntrega(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pEmpresaPendenciaId: Integer;
  pLocalId: Integer;
  pPrevisaoEntrega: TDateTime;
  pAgrupamentoId: Integer;
  pTipoNotaGerar: string;
  pProdutos: TArray<RecEntregasItensPendentes>
): RecRetornoBD;

function DesagendarEntregas(
  pConexao: TConexao;
  pEmpresaPendenciaId: Integer;
  pOrcamentoId: Integer;
  pPrevisaoEntrega: TDateTime;
  pLocalId: Integer;
  pProdutos: TArray<RecEntregasItensPendentes>
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TOrcamentoTramite }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ORC.ORCAMENTO_ID = :P1 '
    );
end;

constructor TEntregasPendentes.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ENTREGAS_ITENS_PENDENTES');

  FSql :=
    'select ' +
    '  ENT.ORCAMENTO_ID, ' +
    '  ORC.CLIENTE_ID, ' +
    '  CAD.NOME_FANTASIA as NOME_CLIENTE, ' +
    '  ORC.VENDEDOR_ID, ' +
    '  nvl(FUN.APELIDO, FUN.NOME) as NOME_VENDEDOR, ' +
    '  ORC.DATA_CADASTRO, ' +
    '  ORC.OBSERVACOES_EXPEDICAO, ' +
    '  ENT.PREVISAO_ENTREGA, ' +
    '  ENT.EMPRESA_ID, ' +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA, ' +
    '  ENT.LOCAL_ID, ' +
    '  LOC.NOME as NOME_LOCAL, ' +
    '  ENT.TIPO_RETORNO_ENTREGA, ' +
    '  BAI.ROTA_ID, ' +
    '  ROT.NOME as NOME_ROTA ' +
    'from ' +
    '  ENTREGAS_PENDENTES ENT ' +

    'inner join ORCAMENTOS ORC ' +
    'on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID ' +
    'and ORC.STATUS in(''RE'', ''VE'') ' +

    'inner join CADASTROS CAD ' +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID ' +

    'inner join FUNCIONARIOS FUN ' +
    'on ORC.VENDEDOR_ID = FUN.FUNCIONARIO_ID ' +

    'inner join EMPRESAS EMP ' +
    'on ENT.EMPRESA_ID = EMP.EMPRESA_ID ' +

    'inner join LOCAIS_PRODUTOS LOC ' +
    'on ENT.LOCAL_ID = LOC.LOCAL_ID  ' +

    'inner join VW_BAIRROS BAI ' +
    'on ORC.BAIRRO_ID = BAI.BAIRRO_ID ' +

    'left join ROTAS ROT ' +
    'on BAI.ROTA_ID = ROT.ROTA_ID ';

  SetFiltros(GetFiltros);

  AddColunaSL('ORCAMENTO_ID');
  AddColunaSL('CLIENTE_ID');
  AddColunaSL('NOME_CLIENTE');
  AddColunaSL('VENDEDOR_ID');
  AddColunaSL('NOME_VENDEDOR');
  AddColunaSL('DATA_CADASTRO');
  AddColunaSL('OBSERVACOES_EXPEDICAO');
  AddColunaSL('PREVISAO_ENTREGA');
  AddColunaSL('EMPRESA_ID');
  AddColunaSL('NOME_EMPRESA');
  AddColunaSL('LOCAL_ID');
  AddColunaSL('NOME_LOCAL');
  AddColunaSL('TIPO_RETORNO_ENTREGA');
  AddColunaSL('ROTA_ID');
  AddColunaSL('NOME_ROTA');
end;

function TEntregasPendentes.GetRecordEntregasPendentes: RecEntregasPendentes;
begin
  Result.OrcamentoId          := getInt('ORCAMENTO_ID');
  Result.ClienteId            := getInt('CLIENTE_ID');
  Result.NomeCliente          := getString('NOME_CLIENTE');
  Result.VendedorId           := getInt('VENDEDOR_ID');
  Result.NomeVendedor         := getString('NOME_VENDEDOR');
  Result.DataCadastro         := getData('DATA_CADASTRO');
  Result.ObservacoesExpedicao := getString('OBSERVACOES_EXPEDICAO');
  Result.PrevisaoEntrega      := getData('PREVISAO_ENTREGA');
  Result.LocalId              := getInt('LOCAL_ID');
  Result.NomeLocal            := getString('NOME_LOCAL');
  Result.EmpresaId            := getInt('EMPRESA_ID');
  Result.NomeEmpresa          := getString('NOME_EMPRESA');
  Result.TipoRetornoEntrega   := getString('TIPO_RETORNO_ENTREGA');
  Result.RotaId               := getInt('ROTA_ID');
  Result.NomeRota             := getString('NOME_ROTA');
end;

function BuscarEntregasPendentesComando(pConexao: TConexao; pComando: string): TArray<RecEntregasPendentes>;
var
  i: Integer;
  t: TEntregasPendentes;
begin
  Result := nil;
  t := TEntregasPendentes.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordEntregasPendentes;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function GerarEntregaPendencia(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pEmpresaPendenciaId: Integer;
  pLocalId: Integer;
  pPrevisaoEntrega: TDateTime;
  pAgrupamentoId: Integer;
  pTipoNotaGerar: string;
  pProdutos: TArray<RecEntregasItensPendentes>;
  pUtilizaControleManifesto: Boolean
): RecRetornoBD;
var
  i: Integer;
  vProc: TProcedimentoBanco;

  vProdutoIds: array of Integer;
  vItensIds: array of Integer;
  vLocaisIds: array of Integer;
  vQuantidades: array of Double;
  vLotes: array of string;
begin
  Result.TeveErro := False;

  if pUtilizaControleManifesto then
    pConexao.SetRotina('GERAR_ENTREGA_PENDENCIA')
  else
    pConexao.SetRotina('GERAR_ENTREGA_PENDENCIA_S_M');

  vProc := TProcedimentoBanco.Create(pConexao, 'GERAR_ENTREGA_DE_PENDENCIA');

  try

    SetLength(vProdutoIds, Length(pProdutos));
    SetLength(vItensIds, Length(pProdutos));
    SetLength(vLocaisIds, Length(pProdutos));
    SetLength(vQuantidades, Length(pProdutos));
    SetLength(vLotes, Length(pProdutos));

    for i := Low(pProdutos) to High(pProdutos) do begin
      vProdutoIds[i]  := pProdutos[i].ProdutoId;
      vItensIds[i]    := pProdutos[i].ItemId;
      vLocaisIds[i]   := pProdutos[i].LocalId;
      vQuantidades[i] := pProdutos[i].QtdeEntregar;
      vLotes[i]       := pProdutos[i].Lote;
    end;

    vProc.Params[0].AsInteger  := pOrcamentoId;
    vProc.Params[1].AsInteger  := pEmpresaPendenciaId;
    vProc.Params[2].AsInteger  := pLocalId;
    vProc.Params[3].AsDateTime := pPrevisaoEntrega;
    vProc.Params[4].AsInteger  := pAgrupamentoId;
    vProc.Params[5].AsString   := pTipoNotaGerar;

    vProc.Params[6].Value  := vProdutoIds;
    vProc.Params[7].Value  := vItensIds;
    vProc.Params[8].Value  := vLocaisIds;
    vProc.Params[9].Value  := vQuantidades;
    vProc.Params[10].Value := vLotes;

    vProc.Executar;

    Result.AsInt := vProc.ParamByName('oENTREGA_ID').AsInteger;
  except
    on e: Exception do
      Result.TratarErro(e);
  end;

  vProc.Free;
end;

function GerarRetiradaPendenciaEntrega(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pEmpresaPendenciaId: Integer;
  pLocalId: Integer;
  pPrevisaoEntrega: TDateTime;
  pAgrupamentoId: Integer;
  pTipoNotaGerar: string;
  pProdutos: TArray<RecEntregasItensPendentes>
): RecRetornoBD;
var
  i: Integer;
  vProc: TProcedimentoBanco;

  vProdutoIds: array of Integer;
  vItensIds: array of Integer;
  vLocaisIds: array of Integer;
  vQuantidades: array of Double;
  vLotes: array of string;
begin
  Result.TeveErro := False;
  pConexao.SetRotina('GERAR_RET_ENTREGA_PENDENTE');
  vProc := TProcedimentoBanco.Create(pConexao, 'GERAR_RETIRADA_ENT_PENDENTE');

  try

    SetLength(vProdutoIds, Length(pProdutos));
    SetLength(vItensIds, Length(pProdutos));
    SetLength(vLocaisIds, Length(pProdutos));
    SetLength(vQuantidades, Length(pProdutos));
    SetLength(vLotes, Length(pProdutos));

    for i := Low(pProdutos) to High(pProdutos) do begin
      vProdutoIds[i]  := pProdutos[i].ProdutoId;
      vItensIds[i]    := pProdutos[i].ItemId;
      vLocaisIds[i]   := pProdutos[i].LocalId;
      vQuantidades[i] := pProdutos[i].QtdeEntregar;
      vLotes[i]       := pProdutos[i].Lote;
    end;

    vProc.Params[0].AsInteger  := pOrcamentoId;
    vProc.Params[1].AsInteger  := pEmpresaPendenciaId;
    vProc.Params[2].AsInteger  := pLocalId;
    vProc.Params[3].AsDateTime := pPrevisaoEntrega;
    vProc.Params[4].AsString   := pTipoNotaGerar;
    vProc.Params[5].AsInteger  := pAgrupamentoId;

    vProc.Params[6].Value     := vProdutoIds;
    vProc.Params[7].Value     := vItensIds;
    vProc.Params[8].Value     := vLocaisIds;
    vProc.Params[9].Value     := vQuantidades;
    vProc.Params[10].Value    := vLotes;

    vProc.Executar;

    Result.AsInt := vProc.ParamByName('oRETIRADA_ID').AsInteger;
  except
    on e: Exception do
      Result.TratarErro(e);
  end;

  vProc.Free;
end;

function DesagendarEntregas(
  pConexao: TConexao;
  pEmpresaPendenciaId: Integer;
  pOrcamentoId: Integer;
  pPrevisaoEntrega: TDateTime;
  pLocalId: Integer;
  pProdutos: TArray<RecEntregasItensPendentes>
): RecRetornoBD;
var
  i: Integer;
  vProc: TProcedimentoBanco;

  vProdutoIds: array of Integer;
  vItensIds: array of Integer;
  vQuantidades: array of Double;
  vLotes: array of string;
begin
  Result.TeveErro := False;
  pConexao.SetRotina('DESAGENDAR_ENTREGA_PENDENTE');
  vProc := TProcedimentoBanco.Create(pConexao, 'DESAGENDAR_ENTREGA_PENDENTE');

  try

    SetLength(vProdutoIds, Length(pProdutos));
    SetLength(vItensIds, Length(pProdutos));
    SetLength(vQuantidades, Length(pProdutos));
    SetLength(vLotes, Length(pProdutos));

    for i := Low(pProdutos) to High(pProdutos) do begin
      vProdutoIds[i]  := pProdutos[i].ProdutoId;
      vItensIds[i]    := pProdutos[i].ItemId;
      vQuantidades[i] := pProdutos[i].QtdeEntregar;
      vLotes[i]       := pProdutos[i].Lote;
    end;

    vProc.Params[0].AsInteger  := pEmpresaPendenciaId;
    vProc.Params[1].AsInteger  := pOrcamentoId;
    vProc.Params[2].AsDateTime := pPrevisaoEntrega;
    vProc.Params[3].AsInteger  := pLocalId;

    vProc.Params[4].Value := vProdutoIds;
    vProc.Params[5].Value := vItensIds;
    vProc.Params[6].Value := vQuantidades;
    vProc.Params[7].Value := vLotes;

    vProc.Executar;

  except
    on e: Exception do
      Result.TratarErro(e);
  end;

  vProc.Free;
end;

end.
