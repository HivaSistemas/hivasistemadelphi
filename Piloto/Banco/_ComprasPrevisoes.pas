unit _ComprasPrevisoes;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, _RecordsFinanceiros;

{$M+}
type
  TComprasPrevisoes = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordComprasPrevisoes: RecTitulosFinanceiros;
  end;

function BuscarComprasPrevisoes(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TComprasPrevisoes }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ITE.COMPRA_ID = :P1 '
    );
end;

constructor TComprasPrevisoes.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'COMPRAS_PREVISOES');

  FSql :=
    'select ' +
    '  ITE.COMPRA_ID, ' +
    '  ITE.ITEM_ID, ' +
    '  ITE.COBRANCA_ID, ' +
    '  TCO.NOME as NOME_COBRANCA, ' +
    '  ITE.VALOR, ' +
    '  ITE.PARCELA, ' +
    '  ITE.NUMERO_PARCELAS, ' +
    '  ITE.DATA_VENCIMENTO, ' +
    '  ITE.DOCUMENTO ' +
    'from ' +
    '  COMPRAS_PREVISOES ITE ' +

    'inner join TIPOS_COBRANCA TCO ' +
    'on ITE.COBRANCA_ID = TCO.COBRANCA_ID ';

  setFiltros(getFiltros);

  AddColuna('COMPRA_ID', True);
  AddColuna('COBRANCA_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('VALOR');
  AddColuna('PARCELA');
  AddColuna('NUMERO_PARCELAS');
  AddColuna('DATA_VENCIMENTO');
  AddColunaSL('NOME_COBRANCA');
  AddColuna('DOCUMENTO');
end;

function TComprasPrevisoes.getRecordComprasPrevisoes: RecTitulosFinanceiros;
begin
  Result.Id             := getInt('COMPRA_ID', True);
  Result.ItemId         := getInt('ITEM_ID', True);
  Result.CobrancaId     := getInt('COBRANCA_ID', True);
  Result.NomeCobranca   := getString('NOME_COBRANCA');
  Result.Valor          := getDouble('VALOR');
  Result.NumeroParcelas := getInt('NUMERO_PARCELAS');
  Result.Parcela        := getInt('PARCELA');
  Result.DataVencimento := getData('DATA_VENCIMENTO');
  Result.Documento      := getString('DOCUMENTO');
end;

function BuscarComprasPrevisoes(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;
var
  i: Integer;
  t: TComprasPrevisoes;
begin
  Result := nil;
  t := TComprasPrevisoes.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordComprasPrevisoes;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
