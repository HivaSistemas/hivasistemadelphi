unit _ProdutosICMS;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TProdutosICMS = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordProdutosICMS: RecProdutosICMS;
  end;

function AtualizarProdutosICMS(
  pConexao: TConexao;
  pProdutoid: Integer;
  pIva: Double;
  pPercentualIcms: Double;
  pPrecoPauta: Double;
  pCstOrgaoPublico: string;
  pCstConstrutora: string;
  pCstClinicaHospital: string;
  pCstProdutorRural: string;
  pCstContribuinte: string;
  pCstNaoContribuinte: string;
  pEstadoId: string;
  pCstRevenda: string
): RecRetornoBD;

function BuscarProdutosICMSs(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecProdutosICMS>;

function ExcluirProdutosICMS(
  pConexao: TConexao;
  pProdutoId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TProdutosICMS }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where PRODUTO_ID = :P1 '
    );
end;

constructor TProdutosICMS.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PRODUTOS_ICMS');

  FSql :=
    'select ' +
    '  PRODUTO_ID, ' +
    '  EMPRESA_ID, '+
    '  ESTADO_ID, ' +
    '  IVA, ' +
    '  PERCENTUAL_ICMS, ' +
    '  INDICE_REDUCAO_BASE_ICMS, ' +
    '  PRECO_PAUTA, ' +
    '  CST_CONTRIBUINTE, ' +
    '  CST_NAO_CONTRIBUINTE, ' +
    '  CST_ORGAO_PUBLICO, ' +
    '  CST_CONSTRUTORA, ' +
    '  CST_CLINICA_HOSPITAL, ' +
    '  CST_PRODUTOR_RURAL, ' +
    '  CST_REVENDA ' +
    'from ' +
    '  PRODUTOS_ICMS ';

  setFiltros(getFiltros);

  AddColuna('PRODUTO_ID', True);
  AddColuna('EMPRESA_ID', True);
  AddColuna('ESTADO_ID', True);
  AddColuna('IVA');
  AddColuna('PERCENTUAL_ICMS');
  AddColuna('INDICE_REDUCAO_BASE_ICMS');
  AddColuna('PRECO_PAUTA');

  AddColuna('CST_CONTRIBUINTE');
  AddColuna('CST_NAO_CONTRIBUINTE');
  AddColuna('CST_ORGAO_PUBLICO');
  AddColuna('CST_CONSTRUTORA');
  AddColuna('CST_CLINICA_HOSPITAL');
  AddColuna('CST_PRODUTOR_RURAL');
  AddColuna('CST_REVENDA');
end;

function TProdutosICMS.getRecordProdutosICMS: RecProdutosICMS;
begin
  Result := RecProdutosICMS.Create;
  Result.ProdutoId               := getInt('PRODUTO_ID', True);
  Result.EstadoId                := getString('ESTADO_ID', True);
  Result.IVA                     := getDouble('IVA');
  Result.PercentualIcms          := getDouble('PERCENTUAL_ICMS');
  Result.PrecoPauta              := getDouble('PRECO_PAUTA');

  Result.CstOrgaoPublico         := getString('CST_ORGAO_PUBLICO');
  Result.CstConstrutora          := getString('CST_CONSTRUTORA');
  Result.CstClinicaHospital       := getString('CST_CLINICA_HOSPITAL');
  Result.CstProdutorRural        := getString('CST_PRODUTOR_RURAL');
  Result.CstContribuinte         := getString('CST_CONTRIBUINTE');
  Result.CstNaoContribuinte      := getString('CST_NAO_CONTRIBUINTE');
  Result.CstRevenda              := getString('CST_REVENDA');

  Result.IndiceReducaoBaseIcms   := getDouble('INDICE_REDUCAO_BASE_ICMS');
end;

function AtualizarProdutosICMS(
  pConexao: TConexao;
  pProdutoid: Integer;
  pIva: Double;
  pPercentualIcms: Double;
  pPrecoPauta: Double;
  pCstOrgaoPublico: string;
  pCstConstrutora: string;
  pCstClinicaHospital: string;
  pCstProdutorRural: string;
  pCstContribuinte: string;
  pCstNaoContribuinte: string;
  pEstadoId: string;
  pCstRevenda: string
): RecRetornoBD;
var
  t: TProdutosICMS;
  //novo: Boolean;
  //seq: TSequencia;
begin
  Result.TeveErro := False;
  t := TProdutosICMS.Create(pConexao);

  t.setInt('PRODUTO_ID', pProdutoid, True);
  t.setString('ESTADO_ID', pEstadoId, True);
  t.setDouble('IVA', pIva);
  t.setDouble('PERCENTUAL_ICMS', pPercentualIcms);
  t.setDouble('PRECO_PAUTA', pPrecoPauta);
  t.setString('CST_ORGAO_PUBLICO', pCstOrgaoPublico);
  t.setString('CST_CONSTRUTORA', pCstConstrutora);
  t.setString('CST_CLINICA_HOSPITAL', pCstClinicaHospital);
  t.setString('CST_PRODUTOR_RURAL', pCstProdutorRural);
  t.setString('CST_CONTRIBUINTE', pCstContribuinte);
  t.setString('CST_NAO_CONTRIBUINTE', pCstNaoContribuinte);
  t.setString('CST_REVENDA', pCstRevenda);

  try
//    if novo then
//      t.Inserir
//    else
//      t.Atualizar;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function BuscarProdutosICMSs(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecProdutosICMS>;
var
  i: Integer;
  t: TProdutosICMS;
begin
  Result := nil;
  t := TProdutosICMS.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordProdutosICMS;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirProdutosICMS(
  pConexao: TConexao;
  pProdutoId: Integer
): RecRetornoBD;
var
  t: TProdutosICMS;
begin
  Result.TeveErro := False;
  t := TProdutosICMS.Create(pConexao);
  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
