unit _PrecosProdutos;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TPrecosProduto = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordPrecosProduto: RecPrecosProdutos;
  end;

function BuscarPrecosProdutos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecPrecosProdutos>;

function PrecoCadastradoEmpresa(pConexao: TConexao; pProdutoId: Integer; pEmpresaId: Integer): Boolean;

function getFiltros: TArray<RecFiltros>;

implementation

{ TPrecosProduto }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where PRODUTO_ID = :P1 ' +
      'and EMPRESA_ID = :P2 '
    );
end;

constructor TPrecosProduto.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PRECOS_PRODUTOS');

  FSql :=
    'select ' +
    '  PRODUTO_ID, ' +
    '  EMPRESA_ID, ' +
    '  PERC_COMISSAO_A_VISTA, ' +
    '  PERC_COMISSAO_A_PRAZO, ' +
    '  PRECO_ATACADO_1, ' +
    '  QUANTIDADE_MIN_PRECO_VAREJO, ' +
    '  QUANTIDADE_MINIMA_PDV, ' +
    '  PRECO_PDV, ' +
    '  QUANTIDADE_MIN_PRECO_ATAC_3, ' +
    '  PRECO_VAREJO, ' +
    '  QUANTIDADE_MIN_PRECO_ATAC_1, ' +
    '  PRECO_ATACADO_2, ' +
    '  QUANTIDADE_MIN_PRECO_ATAC_2, ' +
    '  PRECO_ATACADO_3, ' +
    '  DATA_PRECO_ATACADO_1, ' +
    '  DATA_PRECO_ATACADO_3, ' +
    '  DATA_PRECO_ATACADO_2, ' +
    '  DATA_PRECO_PDV, ' +
    '  DATA_PRECO_VAREJO ' +
    'from ' +
    '  PRECOS_PRODUTOS ';

  setFiltros(getFiltros);

  AddColuna('PRODUTO_ID', True);
  AddColuna('EMPRESA_ID', True);
  AddColuna('PERC_COMISSAO_A_VISTA');
  AddColuna('PERC_COMISSAO_A_PRAZO');
  AddColuna('PRECO_ATACADO_1');
  AddColuna('QUANTIDADE_MIN_PRECO_VAREJO');
  AddColuna('QUANTIDADE_MINIMA_PDV');
  AddColuna('PRECO_PDV');
  AddColuna('QUANTIDADE_MIN_PRECO_ATAC_3');
  AddColuna('PRECO_VAREJO');
  AddColuna('QUANTIDADE_MIN_PRECO_ATAC_1');
  AddColuna('PRECO_ATACADO_2');
  AddColuna('QUANTIDADE_MIN_PRECO_ATAC_2');
  AddColuna('PRECO_ATACADO_3');
  AddColunaSL('DATA_PRECO_ATACADO_1');
  AddColunaSL('DATA_PRECO_ATACADO_3');
  AddColunaSL('DATA_PRECO_ATACADO_2');
  AddColunaSL('DATA_PRECO_PDV');
  AddColunaSL('DATA_PRECO_VAREJO');
end;

function TPrecosProduto.getRecordPrecosProduto: RecPrecosProdutos;
begin
  Result := RecPrecosProdutos.Create;
  Result.produto_id                  := getInt('PRODUTO_ID', True);
  Result.empresa_id                  := getInt('EMPRESA_ID', True);
  Result.perc_comissao_a_vista       := getDouble('PERC_COMISSAO_A_VISTA');
  Result.perc_comissao_a_prazo       := getDouble('PERC_COMISSAO_A_PRAZO');
  Result.preco_atacado_1             := getDouble('PRECO_ATACADO_1');
  Result.quantidade_min_preco_varejo := getDouble('QUANTIDADE_MIN_PRECO_VAREJO');
  Result.quantidade_minima_pdv       := getDouble('QUANTIDADE_MINIMA_PDV');
  Result.preco_pdv                   := getDouble('PRECO_PDV');
  Result.quantidade_min_preco_atac_3 := getDouble('QUANTIDADE_MIN_PRECO_ATAC_3');
  Result.preco_varejo                := getDouble('PRECO_VAREJO');
  Result.quantidade_min_preco_atac_1 := getDouble('QUANTIDADE_MIN_PRECO_ATAC_1');
  Result.preco_atacado_2             := getDouble('PRECO_ATACADO_2');
  Result.quantidade_min_preco_atac_2 := getDouble('QUANTIDADE_MIN_PRECO_ATAC_2');
  Result.preco_atacado_3             := getDouble('PRECO_ATACADO_3');
  Result.data_preco_atacado_1        := getData('DATA_PRECO_ATACADO_1');
  Result.data_preco_atacado_2        := getData('DATA_PRECO_ATACADO_2');
  Result.data_preco_atacado_3        := getData('DATA_PRECO_ATACADO_3');
  Result.data_preco_pdv              := getData('DATA_PRECO_PDV');
  Result.data_preco_varejo           := getData('DATA_PRECO_VAREJO');
end;

function BuscarPrecosProdutos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecPrecosProdutos>;
var
  i: Integer;
  t: TPrecosProduto;
begin
  Result := nil;
  t := TPrecosProduto.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordPrecosProduto;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function PrecoCadastradoEmpresa(pConexao: TConexao; pProdutoId: Integer; pEmpresaId: Integer): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);
  vSql.SQL.Add('select count(PRODUTO_ID) from PRECOS_PRODUTOS where PRODUTO_ID = :P1 and EMPRESA_ID = :P2');
  vSql.Pesquisar([pProdutoId, pEmpresaId]);

  Result := vSql.GetInt(0) > 0;

  vSql.Active := False;
  vSql.Free;
end;

end.
