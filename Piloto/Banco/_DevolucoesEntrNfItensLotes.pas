unit _DevolucoesEntrNfItensLotes;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecDevolucoesEntrNfItensLotes = record
    DevolucaoId: Integer;
    ItemId: Integer;
    Lote: string;
    Quantidade: Double;
  end;

  TDevolucoesEntrNfItensLotes = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordDevolucoesEntrNfItensLotes: RecDevolucoesEntrNfItensLotes;
  end;

function BuscarDevolucoesEntrNfItensLotes(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecDevolucoesEntrNfItensLotes>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TDevolucoesEntrNfItensLotes }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where DEVOLUCAO_ID = :P1 '
    );
end;

constructor TDevolucoesEntrNfItensLotes.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'DEVOLUCOES_ENTR_NF_ITENS_LOTES');

  FSql := 
    'select ' +
    '  DEVOLUCAO_ID, ' +
    '  ITEM_ID, ' +
    '  LOTE, ' +
    '  QUANTIDADE ' +
    'from ' +
    '  DEVOLUCOES_ENTR_NF_ITENS_LOTES ';

  setFiltros(getFiltros);

  AddColuna('DEVOLUCAO_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('LOTE', True);
  AddColuna('QUANTIDADE');
end;

function TDevolucoesEntrNfItensLotes.getRecordDevolucoesEntrNfItensLotes: RecDevolucoesEntrNfItensLotes;
begin
  Result.DevolucaoId   := getInt('DEVOLUCAO_ID', True);
  Result.ItemId        := getInt('ITEM_ID', True);
  Result.Lote          := getString('LOTE', True);
  Result.Quantidade    := getDouble('QUANTIDADE');
end;

function BuscarDevolucoesEntrNfItensLotes(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecDevolucoesEntrNfItensLotes>;
var
  i: Integer;
  t: TDevolucoesEntrNfItensLotes;
begin
  Result := nil;
  t := TDevolucoesEntrNfItensLotes.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordDevolucoesEntrNfItensLotes;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
