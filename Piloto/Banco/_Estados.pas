unit _Estados;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _RecordsCadastros, System.SysUtils, _Biblioteca, Vcl.Forms, System.Variants;

{$M+}
type
  TEstados = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  end;

function GetFiltros: TArray<RecFiltros>;

function AtualizarEstado(
  pConexao: TConexao;
  pEstadoId: string;
  pNome: string;
  pCodigoIBGE: Cardinal;
  pPercIcmsInterno: Double;
  pAtivo: string
): RecRetornoBD;

function BuscarEstados(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEstados>;

function ExcluirEstados(
  pConexao: TConexao;
  pEstadoId: string
): RecRetornoBD;

implementation

{ TEstados }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Sigla do estado',
      False,
      0,
      'where ESTADO_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome do estado',
      True,
      1,
      'where NOME like :P1 || ''%'' '
    );

end;

constructor TEstados.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ESTADOS');

  FSql :=
    'select ' +
    '  ESTADO_ID, ' +
    '  NOME, ' +
    '  CODIGO_IBGE, ' +
    '  PERC_ICMS_INTERNO, ' +
    '  ATIVO ' +
    'from ' +
    '  ESTADOS ';

  SetFiltros(GetFiltros);

  AddColuna('ESTADO_ID', True);
  AddColuna('NOME');
  AddColuna('CODIGO_IBGE');
  AddColuna('PERC_ICMS_INTERNO');
  AddColuna('ATIVO');
end;

function AtualizarEstado(
  pConexao: TConexao;
  pEstadoId: string;
  pNome: string;
  pCodigoIBGE: Cardinal;
  pPercIcmsInterno: Double;
  pAtivo: string
): RecRetornoBD;
var
  t: TEstados;
  p: TConsulta;
  novo: Boolean;
begin
  Result.TeveErro := False;
  t := TEstados.Create(pConexao);
  p := TConsulta.Create(pConexao);

  p.SQL.Add('select count(ESTADO_ID) from ESTADOS where ESTADO_ID = :P1');
  p.Pesquisar([pEstadoId]);

  novo := p.GetInt(0) = 0;

  p.Active := False;
  p.Free;

  t.SetString('ESTADO_ID', pEstadoId, True);
  t.SetString('NOME', pNome);
  t.setIntN('CODIGO_IBGE', pCodigoIBGE);
  t.setDouble('PERC_ICMS_INTERNO', pPercIcmsInterno);
  t.SetString('ATIVO', pAtivo);

  try
    pConexao.IniciarTransacao;

    if novo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  t.Free;
end;

function BuscarEstados(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEstados>;
var
  i: Integer;
  t: TEstados;
begin
  Result := nil;
  t := TEstados.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := RecEstados.Create;

      Result[i].estado_id       := t.getString('ESTADO_ID', True);
      Result[i].nome            := t.GetString('NOME');
      Result[i].codigo_ibge     := t.getInt('CODIGO_IBGE');
      Result[i].PercIcmsInterno := t.getDouble('PERC_ICMS_INTERNO');
      Result[i].ativo           := t.GetString('ATIVO');

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirEstados(
  pConexao: TConexao;
  pEstadoId: string
): RecRetornoBD;
var
  t: TEstados;
begin
  Result.TeveErro := False;
  t := TEstados.Create(pConexao);

  t.SetString('ESTADO_ID', pEstadoId, True);
  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
