unit _TransfProdutosEmpresasItens;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecTransfProdutosEmpresasItens = record
    TransferenciaId: Integer;
    ItemId: Integer;
    ProdutoId: Integer;
    NomeProduto: string;
    LocalOrigemId: Integer;
    NomeLocalOrigem: string;
    MarcaID: Integer;
    NomeMarca: string;
    Lote: string;
    Quantidade: Double;
    TipoControleEstoque: string;
    PrecoUnitario: Double;
    ValorTotal: Double;
  end;

  TTransfProdutosEmpresasItens = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordTransfProdutosEmpresasItens: RecTransfProdutosEmpresasItens;
  end;

function BuscarTransfProdutosEmpresasItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTransfProdutosEmpresasItens>;

function BuscarTransfProdutosEmpresasItensComando(
  pConexao: TConexao;
  pComando: string
): TArray<RecTransfProdutosEmpresasItens>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TTransfProdutosEmpresasItens }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where TRANSFERENCIA_ID = :P1 '
    );
end;

constructor TTransfProdutosEmpresasItens.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'TRANSF_PRODUTOS_EMPRESAS_ITENS');

  FSql := 
    'select ' +
    '  ITE.TRANSFERENCIA_ID, ' +
    '  ITE.ITEM_ID, ' +
    '  ITE.PRODUTO_ID, ' +
    '  PRO.NOME as NOME_PRODUTO, ' +
    '  MAR.MARCA_ID, ' +
    '  MAR.NOME as NOME_MARCA, ' +
    '  ITE.LOCAL_ORIGEM_ID, ' +
    '  LOC.NOME as NOME_LOCAL_ORIGEM, ' +
    '  ITE.LOTE, ' +
    '  ITE.QUANTIDADE, ' +
    '  ITE.TIPO_CONTROLE_ESTOQUE, ' +
    '  ITE.PRECO_UNITARIO, ' +
    '  ITE.VALOR_TOTAL ' +
    'from ' +
    '  TRANSF_PRODUTOS_EMPRESAS_ITENS ITE ' +

    'inner join PRODUTOS PRO ' +
    'on ITE.PRODUTO_ID = PRO.PRODUTO_ID ' +

    'inner join MARCAS MAR ' +
    'on MAR.MARCA_ID = PRO.MARCA_ID ' +

    'inner join LOCAIS_PRODUTOS LOC ' +
    'on LOC.LOCAL_ID = ITE.LOCAL_ORIGEM_ID ';

  setFiltros(getFiltros);

  AddColuna('TRANSFERENCIA_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('PRODUTO_ID');
  AddColunaSL('NOME_PRODUTO');
  AddColuna('LOCAL_ORIGEM_ID');
  AddColunaSL('NOME_LOCAL_ORIGEM');
  AddColunaSL('MARCA_ID');
  AddColunaSL('NOME_MARCA');
  AddColuna('LOTE');
  AddColuna('QUANTIDADE');
  AddColuna('TIPO_CONTROLE_ESTOQUE');
  AddColuna('PRECO_UNITARIO');
  AddColuna('VALOR_TOTAL');
end;

function TTransfProdutosEmpresasItens.getRecordTransfProdutosEmpresasItens: RecTransfProdutosEmpresasItens;
begin
  Result.TransferenciaId            := getInt('TRANSFERENCIA_ID', True);
  Result.ItemId                     := getInt('ITEM_ID', True);
  Result.ProdutoId                  := getInt('PRODUTO_ID');
  Result.NomeProduto                := getString('NOME_PRODUTO');
  Result.MarcaId                    := getInt('MARCA_ID');
  Result.NomeMarca                  := getString('NOME_MARCA');
  Result.LocalOrigemId              := getInt('LOCAL_ORIGEM_ID');
  Result.NomeLocalOrigem            := getString('NOME_LOCAL_ORIGEM');
  Result.Lote                       := getString('LOTE');
  Result.Quantidade                 := getDouble('QUANTIDADE');
  Result.TipoControleEstoque        := getString('TIPO_CONTROLE_ESTOQUE');
  Result.PrecoUnitario              := getDouble('PRECO_UNITARIO');
  Result.ValorTotal                 := getDouble('VALOR_TOTAL');
end;

function BuscarTransfProdutosEmpresasItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTransfProdutosEmpresasItens>;
var
  i: Integer;
  t: TTransfProdutosEmpresasItens;
begin
  Result := nil;
  t := TTransfProdutosEmpresasItens.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordTransfProdutosEmpresasItens;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarTransfProdutosEmpresasItensComando(
  pConexao: TConexao;
  pComando: string
): TArray<RecTransfProdutosEmpresasItens>;
var
  i: Integer;
  t: TTransfProdutosEmpresasItens;
begin
  Result := nil;
  t := TTransfProdutosEmpresasItens.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordTransfProdutosEmpresasItens;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
