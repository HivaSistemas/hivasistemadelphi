unit _TransfProdutosEmpresas;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _TransfProdutosEmpresasItens, Vcl.Graphics;

{$M+}
type
  RecTransfProdutosEmpresas = record
    TransferenciaId: Integer;
    EmpresaOrigemId: Integer;
    NomeEmpresaOrigem: string;
    EmpresaDestinoId: Integer;
    NomeEmpresaDestino: string;
    DataHoraCadastro: TDateTime;
    MotoristaId: Integer;
    NomeMotorista: string;
    UsuarioCadastroId: Integer;
    NomeUsuarioCadastro: string;
    Status: string;
    StatusAnalitico: string;
    ValorTransferencia: Double;
    DataHoraBaixa: TDateTime;
    UsuarioBaixaId: Integer;
    NomeUsuarioBaixa: string;
    DataHoraCancelamento: TDateTime;
    UsuarioCancelamentoId: Integer;
    NomeUsuarioCancelamento: string;
  end;

  TTransfProdutosEmpresas = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordTransfProdutosEmpresas: RecTransfProdutosEmpresas;
  end;

function AtualizarTransfProdutosEmpresas(
  pConexao: TConexao;
  pTransferenciaId: Integer;
  pEmpresaOrigemId: Integer;
  pEmpresaDestinoId: Integer;
  pMotoristaId: Integer;
  pStatus: string;
  pValorTransferencia: Double;
  pItens: TArray<RecTransfProdutosEmpresasItens>
): RecRetornoBD;

function BuscarTransfProdutosEmpresas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTransfProdutosEmpresas>;

function BuscarTransfProdutosEmpresasComando(
  pConexao: TConexao;
  pComando: string
): TArray<RecTransfProdutosEmpresas>;

function ExcluirTransfProdutosEmpresas(
  pConexao: TConexao;
  pTransferenciaId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

function getCorStatus(pStatus: string): TColor;

implementation

{ TTransfProdutosEmpresas }


function getCorStatus(pStatus: string): TColor;
begin
  Result := _Biblioteca.Decode(
    pStatus,
    [
      'A', clGreen,
      'B', clBlue,
      'C', clRed,
      'T', clWebOrange,
      clBlack
    ]
  );
end;

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where TRANSFERENCIA_ID = :P1'
    );
end;

constructor TTransfProdutosEmpresas.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'TRANSF_PRODUTOS_EMPRESAS');

  FSql := 
    'select ' +
    '  TRA.TRANSFERENCIA_ID, ' +
    '  TRA.EMPRESA_ORIGEM_ID, ' +
    '  EPO.NOME_FANTASIA as NOME_EMPRESA_ORIGEM,' +
    '  TRA.EMPRESA_DESTINO_ID, ' +
    '  EPD.NOME_FANTASIA as NOME_EMPRESA_DESTINO, ' +
    '  TRA.DATA_HORA_CADASTRO, ' +
    '  TRA.MOTORISTA_ID, ' +
    '  FMT.NOME_FANTASIA as NOME_MOTORISTA, ' +
    '  TRA.USUARIO_CADASTRO_ID, ' +
    '  FNC.NOME as NOME_USUARIO_CADASTRO, ' +
    '  TRA.STATUS, ' +
    '  TRA.VALOR_TRANSFERENCIA, ' +
    '  TRA.DATA_HORA_BAIXA, ' +
    '  TRA.USUARIO_BAIXA_ID, ' +
    '  FBX.NOME as NOME_USUARIO_BAIXA, ' +
    '  TRA.DATA_HORA_CANCELAMENTO, ' +
    '  TRA.USUARIO_CANCELAMENTO_ID, ' +
    '  FBC.NOME as NOME_USUARIO_CANCELAMENTO ' +
    'from ' +
    '  TRANSF_PRODUTOS_EMPRESAS TRA ' +

    'inner join EMPRESAS EPO ' +
    'on TRA.EMPRESA_ORIGEM_ID = EPO.EMPRESA_ID ' +

    'inner join EMPRESAS EPD ' +
    'on TRA.EMPRESA_DESTINO_ID = EPD.EMPRESA_ID ' +

    'inner join FUNCIONARIOS FNC ' +
    'on TRA.USUARIO_CADASTRO_ID = FNC.FUNCIONARIO_ID ' +

    'inner join CADASTROS FMT ' +
    'on TRA.MOTORISTA_ID = FMT.CADASTRO_ID ' +

    'left join FUNCIONARIOS FBX ' +
    'on TRA.USUARIO_BAIXA_ID = FBX.FUNCIONARIO_ID ' +

    'left join FUNCIONARIOS FBC ' +
    'on TRA.USUARIO_CANCELAMENTO_ID = FBC.FUNCIONARIO_ID ';

  setFiltros(getFiltros);

  AddColuna('TRANSFERENCIA_ID', True);
  AddColuna('EMPRESA_ORIGEM_ID');
  AddColunaSL('NOME_EMPRESA_ORIGEM');
  AddColuna('EMPRESA_DESTINO_ID');
  AddColunaSL('NOME_EMPRESA_DESTINO');
  AddColunaSL('DATA_HORA_CADASTRO');
  AddColuna('MOTORISTA_ID');
  AddColunaSL('NOME_MOTORISTA');
  AddColunaSL('USUARIO_CADASTRO_ID');
  AddColunaSL('NOME_USUARIO_CADASTRO');
  AddColuna('STATUS');
  AddColuna('VALOR_TRANSFERENCIA');
  AddColunaSL('DATA_HORA_BAIXA');
  AddColunaSL('USUARIO_BAIXA_ID');
  AddColunaSL('NOME_USUARIO_BAIXA');
  AddColunaSL('DATA_HORA_CANCELAMENTO');
  AddColunaSL('USUARIO_CANCELAMENTO_ID');
  AddColunaSL('NOME_USUARIO_CANCELAMENTO');
end;

function TTransfProdutosEmpresas.getRecordTransfProdutosEmpresas: RecTransfProdutosEmpresas;
begin
  Result.TransferenciaId            := getInt('TRANSFERENCIA_ID', True);
  Result.EmpresaOrigemId            := getInt('EMPRESA_ORIGEM_ID');
  Result.NomeEmpresaOrigem          := getString('NOME_EMPRESA_ORIGEM');
  Result.EmpresaDestinoId           := getInt('EMPRESA_DESTINO_ID');
  Result.NomeEmpresaDestino         := getString('NOME_EMPRESA_DESTINO');
  Result.DataHoraCadastro           := getData('DATA_HORA_CADASTRO');
  Result.MotoristaId                := getInt('MOTORISTA_ID');
  Result.NomeMotorista              := getString('NOME_MOTORISTA');
  Result.UsuarioCadastroId          := getInt('USUARIO_CADASTRO_ID');
  Result.NomeUsuarioCadastro        := getString('NOME_USUARIO_CADASTRO');
  Result.Status                     := getString('STATUS');
  Result.ValorTransferencia         := getDouble('VALOR_TRANSFERENCIA');
  Result.DataHoraBaixa              := getData('DATA_HORA_BAIXA');
  Result.UsuarioBaixaId             := getInt('USUARIO_BAIXA_ID');
  Result.NomeUsuarioBaixa           := getString('NOME_USUARIO_BAIXA');
  Result.DataHoraCancelamento       := getData('DATA_HORA_CANCELAMENTO');
  Result.UsuarioCancelamentoId      := getInt('USUARIO_CANCELAMENTO_ID');
  Result.NomeUsuarioCancelamento    := getString('NOME_USUARIO_CANCELAMENTO');

  if Result.Status = 'A' then
    Result.StatusAnalitico := 'Ag. carregamento'
  else if Result.Status = 'B' then
    Result.StatusAnalitico := 'Baixada'
  else if Result.Status = 'C' then
    Result.StatusAnalitico := 'Cancelada'
  else if Result.Status = 'T' then
    Result.StatusAnalitico := 'Em tr�nsito';

end;

function AtualizarTransfProdutosEmpresas(
  pConexao: TConexao;
  pTransferenciaId: Integer;
  pEmpresaOrigemId: Integer;
  pEmpresaDestinoId: Integer;
  pMotoristaId: Integer;
  pStatus: string;
  pValorTransferencia: Double;
  pItens: TArray<RecTransfProdutosEmpresasItens>
): RecRetornoBD;
var
  i: Integer;
  vNovo: Boolean;
  vSeq: TSequencia;

  vProc: TProcedimentoBanco;
  t: TTransfProdutosEmpresas;
  vItem: TTransfProdutosEmpresasItens;

begin
  Result.Iniciar;
  pConexao.setRotina('ATU_TRANS_PROD_EMPRESAS');

  t := TTransfProdutosEmpresas.Create(pConexao);
  vItem := TTransfProdutosEmpresasItens.Create(pConexao);
  vProc := TProcedimentoBanco.Create(pConexao, 'GERAR_NOTA_TRANSF_PROD_EMPR');

  vNovo := pTransferenciaId = 0;
  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_TRANSF_PROD_EMPRESAS');
    pTransferenciaId := vSeq.getProximaSequencia;
    Result.AsInt := pTransferenciaId;
    vSeq.Free;
  end;

  try
    pConexao.IniciarTransacao; 

    t.setInt('TRANSFERENCIA_ID', pTransferenciaId, True);
    t.setInt('EMPRESA_ORIGEM_ID', pEmpresaOrigemId);
    t.setInt('EMPRESA_DESTINO_ID', pEmpresaDestinoId);
    t.setIntN('MOTORISTA_ID', pMotoristaId);
    t.setString('STATUS', pStatus);
    t.setDouble('VALOR_TRANSFERENCIA', pValorTransferencia);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    for i := Low(pItens) to High(pItens) do begin
      vItem.setInt('TRANSFERENCIA_ID', pTransferenciaId, True);
      vItem.setInt('ITEM_ID', pItens[i].ItemId, True);
      vItem.setInt('PRODUTO_ID', pItens[i].ProdutoId);
      vItem.setInt('LOCAL_ORIGEM_ID', pItens[i].LocalOrigemId);
      vItem.setString('LOTE', pItens[i].Lote);
      vItem.setDouble('QUANTIDADE', pItens[i].Quantidade);
      vItem.setString('TIPO_CONTROLE_ESTOQUE', pItens[i].TipoControleEstoque);
      vItem.setDouble('PRECO_UNITARIO', pItens[i].PrecoUnitario);
      vItem.setDouble('VALOR_TOTAL', pItens[i].ValorTotal);

      vItem.Inserir;
    end;

    vProc.Executar([pTransferenciaId]);

    // ID da nota fiscal gerada
    Result.AsInt2 := vProc.Params[1].AsInteger;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao; 
      Result.TratarErro(e);
    end;
  end;

  vProc.Free;
  vItem.Free;
  t.Free;
end;

function BuscarTransfProdutosEmpresas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTransfProdutosEmpresas>;
var
  i: Integer;
  t: TTransfProdutosEmpresas;
begin
  Result := nil;
  t := TTransfProdutosEmpresas.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordTransfProdutosEmpresas;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirTransfProdutosEmpresas(
  pConexao: TConexao;
  pTransferenciaId: Integer
): RecRetornoBD;
var
  t: TTransfProdutosEmpresas;
begin
  Result.Iniciar;
  t := TTransfProdutosEmpresas.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('TRANSFERENCIA_ID', pTransferenciaId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarTransfProdutosEmpresasComando(
  pConexao: TConexao;
  pComando: string
): TArray<RecTransfProdutosEmpresas>;
var
  i: Integer;
  t: TTransfProdutosEmpresas;
begin
  Result := nil;
  t := TTransfProdutosEmpresas.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordTransfProdutosEmpresas;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
