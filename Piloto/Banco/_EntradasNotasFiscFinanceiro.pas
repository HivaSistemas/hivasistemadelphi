unit _EntradasNotasFiscFinanceiro;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsEstoques, _RecordsFinanceiros;

{$M+}
type
  TEntradaNotaFiscalFinanceiro = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordEntradaNotaFiscalFinanceiro: RecTitulosFinanceiros;
  end;

function BuscarEntradaNotaFiscalFinanceiro(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TEntradaNotaFiscalFinanceiro }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ENTRADA_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ENTRADA_ID = :P1 ' +
      'and TIPO = :P2 '
    );
end;

constructor TEntradaNotaFiscalFinanceiro.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ENTRADAS_NOTAS_FISC_FINANCEIRO');

  FSql :=
    'select ' +
    '  FIN.ENTRADA_ID, ' +
    '  FIN.ITEM_ID, ' +
    '  FIN.COBRANCA_ID, ' +
    '  FIN.PARCELA, ' +
    '  FIN.NUMERO_PARCELAS, ' +
    '  FIN.DATA_VENCIMENTO, ' +
    '  FIN.VALOR_DOCUMENTO, ' +
    '  FIN.CODIGO_BARRAS, ' +
    '  FIN.NOSSO_NUMERO, ' +
    '  FIN.DOCUMENTO, ' +
    '  TCO.NOME as NOME_TIPO_COBRANCA, ' +
    '  FIN.TIPO, ' +
    '  FIN.FORNECEDOR_ID, ' +
    '  CAD.RAZAO_SOCIAL as NOME_FORNECEDOR, ' +
    '  FIN.PLANO_FINANCEIRO_ID, ' +
    '  FIN.CENTRO_CUSTO_ID ' +
    'from ' +
    '  ENTRADAS_NOTAS_FISC_FINANCEIRO FIN ' +

    'inner join TIPOS_COBRANCA TCO ' +
    'on FIN.COBRANCA_ID = TCO.COBRANCA_ID ' +

    'left join CADASTROS CAD ' +
    'on FIN.FORNECEDOR_ID = CAD.CADASTRO_ID ';

  setFiltros(getFiltros);

  AddColuna('ENTRADA_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('COBRANCA_ID');
  AddColuna('PARCELA');
  AddColuna('NUMERO_PARCELAS');
  AddColuna('DATA_VENCIMENTO');
  AddColuna('VALOR_DOCUMENTO');
  AddColuna('DOCUMENTO');
  AddColuna('CODIGO_BARRAS');
  AddColuna('NOSSO_NUMERO');
  AddColunaSL('NOME_TIPO_COBRANCA');
  AddColuna('TIPO');
  AddColuna('PLANO_FINANCEIRO_ID');
  AddColuna('CENTRO_CUSTO_ID');
  AddColuna('FORNECEDOR_ID');
  AddColunaSL('NOME_FORNECEDOR');
end;

function TEntradaNotaFiscalFinanceiro.getRecordEntradaNotaFiscalFinanceiro: RecTitulosFinanceiros;
begin
  Result.Id             := getInt('ENTRADA_ID', True);
  Result.ItemId         := getInt('ITEM_ID', True);
  Result.CobrancaId     := getInt('COBRANCA_ID');
  Result.Parcela        := getInt('PARCELA');
  Result.NumeroParcelas := getInt('NUMERO_PARCELAS');
  Result.DataVencimento := getData('DATA_VENCIMENTO');
  Result.Valor          := getDouble('VALOR_DOCUMENTO');
  Result.NomeCobranca   := getString('NOME_TIPO_COBRANCA');
  Result.NossoNumero    := getString('NOSSO_NUMERO');
  Result.Documento      := getString('DOCUMENTO');
  Result.CodigoBarras   := getString('CODIGO_BARRAS');
  Result.Tipo           := getString('TIPO');
  Result.FornecedorId   := getInt('FORNECEDOR_ID');
  Result.NomeFornecedor := getString('NOME_FORNECEDOR');
  Result.PlanoFinanceiroId := getString('PLANO_FINANCEIRO_ID');
  Result.CentroCustoId  := getInt('CENTRO_CUSTO_ID');
end;

function BuscarEntradaNotaFiscalFinanceiro(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;
var
  i: Integer;
  t: TEntradaNotaFiscalFinanceiro;
begin
  Result := nil;
  t := TEntradaNotaFiscalFinanceiro.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordEntradaNotaFiscalFinanceiro;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
