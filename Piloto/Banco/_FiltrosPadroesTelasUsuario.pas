unit _FiltrosPadroesTelasUsuario;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, System.SysUtils;

type
  RecFiltrosPadroesTelasUsuario = record
    UsuarioId: Integer;
    Tela: string;
    Campo: string;
    Ordem: Integer;
    ValorPesquisa: string;
  end;

  TFiltrosPadroesTelasUsuario = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordFiltrosPadroesTelasUsuario: RecFiltrosPadroesTelasUsuario;
  end;

function AtualizarFiltrosPadroes(
  pConexao: TConexao;
  pTela: string;
  pUsuarioId: Integer;
  pFiltros: TArray<RecFiltrosPadroesTelasUsuario>
): RecRetornoBD;

function BuscarFiltrosPadroes(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant
): TArray<RecFiltrosPadroesTelasUsuario>;

function GetFiltros: TArray<RecFiltros>;

implementation

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where FPT.USUARIO_ID = :P1 ' +
      'and FPT.TELA = :P2 ',
      'order by FPT.ORDEM '
    );

end;

constructor TFiltrosPadroesTelasUsuario.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'FILTROS_PADROES_TELAS_USUARIO');

  FSql :=
    'select ' +
    '  FPT.USUARIO_ID, ' +
    '  FPT.TELA, ' +
    '  FPT.CAMPO, ' +
    '  FPT.ORDEM, ' +
    '  FPT.VALOR_PESQUISA ' +
    'from ' +
    '  FILTROS_PADROES_TELAS_USUARIO FPT ';

  SetFiltros(GetFiltros);

  AddColuna('USUARIO_ID', True);
  AddColuna('TELA', True);
  AddColuna('CAMPO', True);
  AddColuna('ORDEM');
  AddColuna('VALOR_PESQUISA');
end;

function TFiltrosPadroesTelasUsuario.GetRecordFiltrosPadroesTelasUsuario: RecFiltrosPadroesTelasUsuario;
begin
  Result.UsuarioId     := getInt('USUARIO_ID', True);
  Result.Tela          := getString('TELA', True);
  Result.Campo         := getString('CAMPO', True);
  Result.Ordem         := getInt('ORDEM');
  Result.ValorPesquisa := getString('VALOR_PESQUISA');
end;

function AtualizarFiltrosPadroes(
  pConexao: TConexao;
  pTela: string;
  pUsuarioId: Integer;
  pFiltros: TArray<RecFiltrosPadroesTelasUsuario>
): RecRetornoBD;
var
  i: Integer;
  vExec: TExecucao;
  t: TFiltrosPadroesTelasUsuario;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZ_FILT_PADR_TELA_USUARIO');

  vExec := TExecucao.Create(pConexao);
  t := TFiltrosPadroesTelasUsuario.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    vExec.Add('delete from FILTROS_PADROES_TELAS_USUARIO ');
    vExec.Add('where TELA = :P1 ');
    vExec.Add('and USUARIO_ID = :P2 ');
    vExec.Executar([pTela, pUsuarioId]);

    for i := Low(pFiltros) to High(pFiltros) do begin
      t.setInt('USUARIO_ID', pUsuarioId, True);
      t.setString('TELA', pTela, True);
      t.setString('CAMPO', pFiltros[i].Campo, True);
      t.setInt('ORDEM', pFiltros[i].Ordem);
      t.setString('VALOR_PESQUISA', pFiltros[i].ValorPesquisa);

      t.Inserir;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
  vExec.Free;
end;

function BuscarFiltrosPadroes(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant
): TArray<RecFiltrosPadroesTelasUsuario>;
var
  i: Integer;
  t: TFiltrosPadroesTelasUsuario;
begin
  Result := nil;
  pConexao.SetRotina('BUSCAR_FILTROS_PADR_TELAS_USU');
  t := TFiltrosPadroesTelasUsuario.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordFiltrosPadroesTelasUsuario;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
