unit _MotivosAjusteEstoque;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _Sessao, _RecordsCadastros;

{$M+}
type
  TMotivoAjusteEstoque = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordMotivoAjusteEstoque: RecMotivoAjusteEstoque;
  end;

function AtualizarMotivoAjusteEstoque(
  pConexao: TConexao;
  pmotivoajusteid: Integer;
  pdescricao: string;
  pPlanoFinanceiroId: string;
  pativo: string;
  pIncideFinanceiro: string
): RecRetornoBD;

function BuscarMotivoAjusteEstoques(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecMotivoAjusteEstoque>;

function ExcluirMotivoAjusteEstoque(
  pConexao: TConexao;
  pMotivoAjusteId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TMotivoAjusteEstoque }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where MOTIVO_AJUSTE_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Descri��o',
      True,
      0,
      'where DESCRICAO like :P1 || ''%'' ' +
      'order by ' +
      '  DESCRICAO '
    );
end;

constructor TMotivoAjusteEstoque.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'MOTIVOS_AJUSTE_ESTOQUE');

  FSql :=
    'select ' +
    '  MOTIVO_AJUSTE_ID, ' +
    '  DESCRICAO, ' +
    '  PLANO_FINANCEIRO_ID, ' +
    '  ATIVO, ' +
    '  INCIDE_FINANCEIRO ' +
    'from ' +
    '  MOTIVOS_AJUSTE_ESTOQUE ';

  setFiltros(getFiltros);

  AddColuna('MOTIVO_AJUSTE_ID', True);
  AddColuna('DESCRICAO');
  AddColuna('PLANO_FINANCEIRO_ID');
  AddColuna('ATIVO');
  AddColuna('INCIDE_FINANCEIRO');
end;

function TMotivoAjusteEstoque.getRecordMotivoAjusteEstoque: RecMotivoAjusteEstoque;
begin
  Result := RecMotivoAjusteEstoque.Create;
  Result.motivo_ajuste_id  := getInt('MOTIVO_AJUSTE_ID', True);
  Result.descricao         := getString('DESCRICAO');
  Result.PlanoFinanceiroId := getString('PLANO_FINANCEIRO_ID');
  Result.ativo             := getString('ATIVO');
  Result.IncideFinanceiro  := getString('INCIDE_FINANCEIRO');
end;

function AtualizarMotivoAjusteEstoque(
  pConexao: TConexao;
  pMotivoAjusteId: Integer;
  pDescricao: string;
  pPlanoFinanceiroId: string;
  pAtivo: string;
  pIncideFinanceiro: string
): RecRetornoBD;
var
  t: TMotivoAjusteEstoque;
  vNovo: Boolean;
  seq: TSequencia;
begin
  Result.Iniciar;
  t := TMotivoAjusteEstoque.Create(pConexao);

  vNovo := pMotivoAjusteId = 0;
  if vNovo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_MOTIVO_AJUSTE_ID');
    pMotivoAjusteId := seq.getProximaSequencia;
    result.AsInt := pMotivoAjusteId;
    seq.Free;
  end;

  try
    pConexao.IniciarTransacao;

    t.setInt('MOTIVO_AJUSTE_ID', pMotivoAjusteId, True);
    t.setString('DESCRICAO', pDescricao);
    t.setString('PLANO_FINANCEIRO_ID', pPlanoFinanceiroId);
    t.setString('ATIVO', pAtivo);
    t.setString('INCIDE_FINANCEIRO', pIncideFinanceiro);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarMotivoAjusteEstoques(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecMotivoAjusteEstoque>;
var
  i: Integer;
  t: TMotivoAjusteEstoque;
begin
  Result := nil;
  t := TMotivoAjusteEstoque.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordMotivoAjusteEstoque;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirMotivoAjusteEstoque(
  pConexao: TConexao;
  pMotivoAjusteId: Integer
): RecRetornoBD;
var
  t: TMotivoAjusteEstoque;
begin
  Result.TeveErro := False;
  t := TMotivoAjusteEstoque.Create(pConexao);

  try
    t.setInt('MOTIVO_AJUSTE_ID', pMotivoAjusteId, True);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
