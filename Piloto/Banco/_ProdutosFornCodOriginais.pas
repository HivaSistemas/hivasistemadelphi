unit _ProdutosFornCodOriginais;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils;

{$M+}
type
  RecProdutosFornCodOriginais = record
  public
    ProdutoId: Integer;
    FornecedorId: Integer;
    NomeFornecedor: string;
    CodigoOriginal: string;
  end;

  TProdutosFornCodOriginais = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordCodigos: RecProdutosFornCodOriginais;
  end;

function BuscarCodigosOriginais(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecProdutosFornCodOriginais>;

function BuscarCodigosOriginaisComando(pConexao: TConexao; pComando: string): TArray<RecProdutosFornCodOriginais>;
function AtualizarProdutosCodigosOriginais(pConexao: TConexao; pFornecedorId: Integer; pProdutos: TArray<RecProdutosFornCodOriginais>): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TProdutosFornCodOriginais }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COD.PRODUTO_ID = :P1 '
    );
end;

constructor TProdutosFornCodOriginais.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PRODUTOS_FORN_COD_ORIGINAIS');

  FSql :=
    'select ' +
    '  COD.PRODUTO_ID, ' +
    '  COD.FORNECEDOR_ID, ' +
    '  CAD.RAZAO_SOCIAL as NOME_FORNECEDOR, ' +
    '  COD.CODIGO_ORIGINAL ' +
    'from ' +
    '  PRODUTOS_FORN_COD_ORIGINAIS COD ' +

    'inner join CADASTROS CAD ' +
    'on COD.FORNECEDOR_ID = CAD.CADASTRO_ID ';

  SetFiltros(GetFiltros);

  AddColuna('PRODUTO_ID', True);
  AddColuna('FORNECEDOR_ID', True);
  AddColuna('CODIGO_ORIGINAL', True);
  AddColunaSL('NOME_FORNECEDOR');
end;

function TProdutosFornCodOriginais.GetRecordCodigos: RecProdutosFornCodOriginais;
begin
  Result.ProdutoId      := GetInt('PRODUTO_ID', True);
  Result.FornecedorId   := GetInt('FORNECEDOR_ID', True);
  Result.CodigoOriginal := GetString('CODIGO_ORIGINAL', True);
  Result.NomeFornecedor := GetString('NOME_FORNECEDOR');
end;

function BuscarCodigosOriginais(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecProdutosFornCodOriginais>;
var
  i: Integer;
  t: TProdutosFornCodOriginais;
begin
  Result := nil;
  t := TProdutosFornCodOriginais.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordCodigos;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarCodigosOriginaisComando(pConexao: TConexao; pComando: string): TArray<RecProdutosFornCodOriginais>;
var
  i: Integer;
  t: TProdutosFornCodOriginais;
begin
  Result := nil;
  t := TProdutosFornCodOriginais.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordCodigos;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function AtualizarProdutosCodigosOriginais(pConexao: TConexao; pFornecedorId: Integer; pProdutos: TArray<RecProdutosFornCodOriginais>): RecRetornoBD;
var
  i: Integer;
  vSql: TConsulta;
  vExiste: Boolean;
  vObj: TProdutosFornCodOriginais;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_PROD_COD_ORIGINAL');

  vObj := TProdutosFornCodOriginais.Create(pConexao);

  vSql := TConsulta.Create(pConexao);
  vSql.Add('select ');
  vSql.Add('  count(*) ');
  vSql.Add('from ');
  vSql.Add('  PRODUTOS_FORN_COD_ORIGINAIS ');
  vSql.Add('where FORNECEDOR_ID = :P1 ');
  vSql.Add('and PRODUTO_ID = :P2 ');
  vSql.Add('and CODIGO_ORIGINAL = :P3 ');

  try
    pConexao.IniciarTransacao;

    for i := Low(pProdutos) to High(pProdutos) do begin
      vSql.Pesquisar([pFornecedorId, pProdutos[i].ProdutoId, pProdutos[i].CodigoOriginal]);

      vExiste := vSql.GetInt(0) > 0;
      if vExiste then
        Continue;

      vObj.setInt('PRODUTO_ID', pProdutos[i].ProdutoId, True);
      vObj.setInt('FORNECEDOR_ID', pFornecedorId, True);
      vObj.setString('CODIGO_ORIGINAL', pProdutos[i].CodigoOriginal, True);

      vObj.Inserir;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vSql.Free;
  vObj.Free;
end;

end.
