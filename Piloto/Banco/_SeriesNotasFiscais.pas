unit _SeriesNotasFiscais;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils;

{$M+}
type
  TSeriesNotasFiscais = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  end;

function AtualizarSeriesNotas(pConexao: TConexao; pSeries: TArray<string>): RecRetornoBD;

function BuscarSeriesNotas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<string>;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TSeriesNotasFiscais }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      ''
    );
end;

constructor TSeriesNotasFiscais.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'SERIES_NOTAS_FISCAIS');

  FSql :=
    'select ' +
    '  SERIE ' +
    'from ' +
    '  SERIES_NOTAS_FISCAIS ';

  SetFiltros(GetFiltros);

  AddColuna('SERIE', True);
end;

function AtualizarSeriesNotas(pConexao: TConexao; pSeries: TArray<string>): RecRetornoBD;
var
  i: Integer;
  vExec: TExecucao;
  vSerie: TSeriesNotasFiscais;
begin
  vExec := TExecucao.Create(pConexao);
  Result.TeveErro := False;
  vSerie := TSeriesNotasFiscais.Create(pConexao);

  vExec.Add('delete from SERIES_NOTAS_FISCAIS');

  try
    pConexao.IniciarTransacao;

    vExec.Executar;

    for i := Low(pSeries) to High(pSeries) do begin
      vSerie.setString('SERIE', pSeries[i], True);

      vSerie.Inserir;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
  vSerie.Free;
end;

function BuscarSeriesNotas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<string>;
var
  i: Integer;
  t: TSeriesNotasFiscais;
begin
  Result := nil;
  t := TSeriesNotasFiscais.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.getString('SERIE', True);

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
