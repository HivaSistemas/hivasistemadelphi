unit _Fornecedores;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros, _Cadastros, _CadastrosTelefones, _DiversosEnderecos;

{$M+}
type
  RecCadastroIdRazaoSocial = record
    CadastroId: Integer;
    RazaoSocial: string;
  end;

  TFornecedor = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordFornecedor: RecFornecedores;
  end;

function AtualizarFornecedor(
  pConexao: TConexao;
  FPesquisouCadastro: Boolean;
  pCadastroId: Integer;
  pCadastro: RecCadastros;
  pTelefones: TArray<RecCadastrosTelefones>;
  pEnderecos: TArray<RecDiversosEnderecos>;
  pSuperSimples: string;
  pObservacoes: string;
  pTipoFornecedor: string;
  pAtivo: string;
  pRevenda: string;
  pUsoConsumo: string;
  pServico: string;
  pPlanoFinanceiroRevenda: string;
  pPlanoFinanceiroUsoConsumo: string;
  pPlanoFinanceiroServico: string;
  pTipoRateioFrete: string;
  pGrupoFornecedorId: Integer
): RecRetornoBD;

function BuscarFornecedores(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant;
  pRevenda: Boolean;
  pUsoConsumo: Boolean;
  pServico: Boolean
): TArray<RecFornecedores>;

function ExcluirFornecedor(
  pConexao: TConexao;
  pCadastroId: Integer
): RecRetornoBD;

function EFornecedor(pConexao: TConexao; pCadastroId: Integer): Boolean;
function getCodigoNomeFantasiaFornecedor(pConexao: TConexao; pCPFCNPJ: string): RecCadastroIdRazaoSocial;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TFornecedor }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 4);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where FRN.CADASTRO_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome fantasia (Avan�ado)',
      False,
      0,
      'where CAD.NOME_FANTASIA like ''%'' || :P1 || ''%'' '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Raz�o social (Avan�ado)',
      True,
      1,
      'where CAD.RAZAO_SOCIAL like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  CAD.RAZAO_SOCIAL',
      '',
      True
    );

  Result[3] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'CPF/CNPJ (avan�ado)',
      False,
      2,
      'where RETORNA_NUMEROS(CAD.CPF_CNPJ) like ''%'' || RETORNA_NUMEROS(:P1) || ''%'' ',
      'order by ' +
      '  CAD.CPF_CNPJ ',
      '',
      True
    );
end;

constructor TFornecedor.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'FORNECEDORES');

  FSql :=
    'select ' +
    '  FRN.CADASTRO_ID, ' +
    '  FRN.SUPER_SIMPLES, ' +
    '  FRN.ATIVO, ' +
    '  FRN.OBSERVACOES, ' +
    '  FRN.TIPO_FORNECEDOR, ' +
    '  FRN.REVENDA, ' +
    '  FRN.USO_CONSUMO, ' +
    '  FRN.SERVICO, ' +
    '  FRN.PLANO_FINANCEIRO_REVENDA, ' +
    '  FRN.PLANO_FINANCEIRO_USO_CONSUMO, ' +
    '  FRN.PLANO_FINANCEIRO_SERVICO, ' +
    '  FRN.TIPO_RATEIO_FRETE, ' +
    '  FRN.GRUPO_FORNECEDOR_ID, ' +
    // Cadastro
    '  CAD.NOME_FANTASIA, ' +
    '  CAD.RAZAO_SOCIAL, ' +
    '  CAD.TIPO_PESSOA, ' +
    '  CAD.CPF_CNPJ, ' +
    '  CAD.LOGRADOURO, ' +
    '  CAD.COMPLEMENTO, ' +
    '  CAD.NUMERO, ' +
    '  CAD.BAIRRO_ID, ' +
    '  CAD.PONTO_REFERENCIA, ' +
    '  CAD.CEP, ' +
    '  CAD.E_MAIL, ' +
    '  CAD.SEXO, ' +
    '  CAD.ESTADO_CIVIL, ' +
    '  CAD.DATA_NASCIMENTO, ' +
    '  CAD.DATA_CADASTRO, ' +
    '  CAD.INSCRICAO_ESTADUAL, ' +
    '  CAD.CNAE, ' +
    '  CAD.RG, ' +
    '  CAD.ORGAO_EXPEDIDOR_RG, ' +
    '  CAD.E_CLIENTE, ' +
    '  CAD.E_FORNECEDOR, ' +
    '  CAD.ATIVO as CADASTRO_ATIVO, ' +
    '  CID.ESTADO_ID ' +
    'from ' +
    '  FORNECEDORES FRN ' +

    'inner join CADASTROS CAD ' +
    'on FRN.CADASTRO_ID = CAD.CADASTRO_ID ' +

    'inner join BAIRROS BAI ' +
    'on CAD.BAIRRO_ID = BAI.BAIRRO_ID ' +

    'inner join CIDADES CID ' +
    'on BAI.CIDADE_ID = CID.CIDADE_ID ';

  SetFiltros(GetFiltros);

  AddColuna('CADASTRO_ID', True);
  AddColuna('SUPER_SIMPLES');
  AddColuna('ATIVO');
  AddColuna('OBSERVACOES');
  AddColuna('TIPO_FORNECEDOR');
  AddColuna('REVENDA');
  AddColuna('USO_CONSUMO');
  AddColuna('SERVICO');
  AddColuna('PLANO_FINANCEIRO_REVENDA');
  AddColuna('PLANO_FINANCEIRO_USO_CONSUMO');
  AddColuna('PLANO_FINANCEIRO_SERVICO');
  AddColuna('TIPO_RATEIO_FRETE');
  AddColuna('GRUPO_FORNECEDOR_ID');

  // CADASTRO
  AddColunaSL('NOME_FANTASIA');
  AddColunaSL('RAZAO_SOCIAL');
  AddColunaSL('TIPO_PESSOA');
  AddColunaSL('CPF_CNPJ');
  AddColunaSL('LOGRADOURO');
  AddColunaSL('COMPLEMENTO');
  AddColunaSL('NUMERO');
  AddColunaSL('BAIRRO_ID');
  AddColunaSL('CEP');
  AddColunaSL('PONTO_REFERENCIA');
  AddColunaSL('E_MAIL');
  AddColunaSL('SEXO');
  AddColunaSL('ESTADO_CIVIL');
  AddColunaSL('DATA_NASCIMENTO');
  AddColunaSL('DATA_CADASTRO');
  AddColunaSL('INSCRICAO_ESTADUAL');
  AddColunaSL('CNAE');
  AddColunaSL('RG');
  AddColunaSL('ORGAO_EXPEDIDOR_RG');
  AddColunaSL('E_CLIENTE');
  AddColunaSL('E_FORNECEDOR');
  AddColunaSL('CADASTRO_ATIVO');
  AddColunaSL('ESTADO_ID');
end;

function TFornecedor.GetRecordFornecedor: RecFornecedores;
begin
  Result := RecFornecedores.Create;
  Result.cadastro := RecCadastros.Create;

  Result.cadastro_id     := getInt('CADASTRO_ID', True);
  Result.super_simples   := GetString('SUPER_SIMPLES');
  Result.data_cadastro   := GetData('DATA_CADASTRO');
  Result.observacoes     := GetString('OBSERVACOES');
  Result.tipo_fornecedor := GetString('TIPO_FORNECEDOR');
  Result.ativo           := GetString('ATIVO');
  Result.Revenda                   := GetString('REVENDA');
  Result.UsoConsumo                := GetString('USO_CONSUMO');
  Result.Servico                   := GetString('SERVICO');
  Result.PlanoFinanceiroRevenda    := GetString('PLANO_FINANCEIRO_REVENDA');
  Result.PlanoFinanceiroUsoConsumo := GetString('PLANO_FINANCEIRO_USO_CONSUMO');
  Result.PlanoFinanceiroServico    := GetString('PLANO_FINANCEIRO_SERVICO');
  Result.TipoRateioFrete           := GetString('TIPO_RATEIO_FRETE');
  Result.GrupoFornecedorId         := getInt('GRUPO_FORNECEDOR_ID');

  // Cadastro
  Result.cadastro.cadastro_id        := GetInt('CADASTRO_ID', True);
  Result.cadastro.razao_social       := getString('RAZAO_SOCIAL');
  Result.cadastro.nome_fantasia      := getString('NOME_FANTASIA');
  Result.cadastro.tipo_pessoa        := getString('TIPO_PESSOA');
  Result.cadastro.cpf_cnpj           := getString('CPF_CNPJ');
  Result.cadastro.logradouro         := getString('LOGRADOURO');
  Result.cadastro.complemento        := getString('COMPLEMENTO');
  Result.cadastro.numero             := getString('NUMERO');
  Result.cadastro.bairro_id          := getInt('BAIRRO_ID');
  Result.cadastro.ponto_referencia   := getString('PONTO_REFERENCIA');
  Result.cadastro.cep                := getString('CEP');
  Result.cadastro.e_mail             := getString('E_MAIL');
  Result.cadastro.sexo               := getString('SEXO');
  Result.cadastro.estado_civil       := getString('ESTADO_CIVIL');
  Result.cadastro.data_nascimento    := getData('DATA_NASCIMENTO');
  Result.cadastro.data_cadastro      := GetData('DATA_CADASTRO');
  Result.cadastro.inscricao_estadual := getString('INSCRICAO_ESTADUAL');
  Result.cadastro.cnae               := getString('CNAE');
  Result.cadastro.rg                 := getString('RG');
  Result.cadastro.orgao_expedidor_rg := getString('ORGAO_EXPEDIDOR_RG');
  Result.cadastro.e_cliente          := getString('E_CLIENTE');
  Result.cadastro.e_fornecedor       := getString('E_FORNECEDOR');
  Result.cadastro.ativo              := getString('ATIVO');
  Result.cadastro.estado_id          := getString('ESTADO_ID');
end;

function AtualizarFornecedor(
  pConexao: TConexao;
  FPesquisouCadastro: Boolean;
  pCadastroId: Integer;
  pCadastro: RecCadastros;
  pTelefones: TArray<RecCadastrosTelefones>;
  pEnderecos: TArray<RecDiversosEnderecos>;
  pSuperSimples: string;
  pObservacoes: string;
  pTipoFornecedor: string;
  pAtivo: string;
  pRevenda: string;
  pUsoConsumo: string;
  pServico: string;
  pPlanoFinanceiroRevenda: string;
  pPlanoFinanceiroUsoConsumo: string;
  pPlanoFinanceiroServico: string;
  pTipoRateioFrete: string;
  pGrupoFornecedorId: Integer
): RecRetornoBD;
var
  t: TFornecedor;
  novo: Boolean;
  cad: TCadastro;
  seq: TSequencia;
  retorno: RecRetornoBD;
begin
  novo := pCadastroId = 0;
  Result.Iniciar;
  t := TFornecedor.Create(pConexao);
  cad := TCadastro.Create(pConexao);

  if novo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_CADASTRO_ID');
    pCadastroId := seq.GetProximaSequencia;
    result.AsInt := pCadastroId;
    pCadastro.cadastro_id := pCadastroId;
    seq.Free;
  end;

  try
    pConexao.IniciarTransacao;

    cad.setCadastro(pCadastro);
    if novo then
      cad.Inserir
    else
      cad.Atualizar;

    t.setInt('CADASTRO_ID', pCadastroId, True);
    t.SetString('SUPER_SIMPLES', pSuperSimples);
    t.SetString('OBSERVACOES', pObservacoes);
    t.setString('TIPO_FORNECEDOR', pTipoFornecedor);
    t.SetString('ATIVO', pAtivo);
    t.SetString('REVENDA', pRevenda);
    t.SetString('USO_CONSUMO', pUsoConsumo);
    t.SetString('SERVICO', pServico);
    t.SetString('PLANO_FINANCEIRO_REVENDA', pPlanoFinanceiroRevenda);
    t.SetString('PLANO_FINANCEIRO_USO_CONSUMO', pPlanoFinanceiroUsoConsumo);
    t.SetString('PLANO_FINANCEIRO_SERVICO', pPlanoFinanceiroServico);
    t.SetString('TIPO_RATEIO_FRETE', pTipoRateioFrete);
    t.setIntN('GRUPO_FORNECEDOR_ID', pGrupoFornecedorId);

    if novo or FPesquisouCadastro then
      t.Inserir
    else
      t.Atualizar;

    retorno := _CadastrosTelefones.AtualizarTelefones(pConexao, pCadastroId, pTelefones);
    if retorno.TeveErro then
      raise Exception.Create(retorno.MensagemErro);

    if pEnderecos <> nil then begin
      retorno := _DiversosEnderecos.AtualizarDiversosEndereco(pConexao, pCadastroId, pEnderecos);

      if retorno.TeveErro then
        raise Exception.Create(retorno.MensagemErro);
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  cad.Free;
  t.Free;
end;

function BuscarFornecedores(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant;
  pRevenda: Boolean;
  pUsoConsumo: Boolean;
  pServico: Boolean
): TArray<RecFornecedores>;
var
  i: Integer;
  t: TFornecedor;

  vSqlAuxiliar: string;
begin
  Result := nil;
  t := TFornecedor.Create(pConexao);

  vSqlAuxiliar := '';
  if pRevenda then
    _Biblioteca.AddOrSeNecessario(vSqlAuxiliar, 'FRN.REVENDA = ''S'' ');

  if pUsoConsumo then
    _Biblioteca.AddOrSeNecessario(vSqlAuxiliar, 'FRN.USO_CONSUMO = ''S'' ');

  if pServico then
    _Biblioteca.AddOrSeNecessario(vSqlAuxiliar, 'FRN.SERVICO = ''S'' ');

  // Esse select tem que vir dentro de par�nteses
  if vSqlAuxiliar <> '' then
    vSqlAuxiliar := 'and (' + vSqlAuxiliar + ') ';

  if t.Pesquisar(pIndice, pFiltros, vSqlAuxiliar) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordFornecedor;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirFornecedor(
  pConexao: TConexao;
  pCadastroId: Integer
): RecRetornoBD;
var
  ex: TExecucao;
  t: TFornecedor;
begin
  Result.TeveErro := False;
  t := TFornecedor.Create(pConexao);
  ex := TExecucao.Create(pConexao);

  t.SetInt('CADASTRO_ID', pCadastroId, True);

  try
    pConexao.IniciarTransacao;

    ex.SQL.Add('delete from DIVERSOS_CONTATOS where CADASTRO_ID = :P1');
    ex.Executar([pCadastroId]);

    ex.SQL.Clear;
    ex.SQL.Add('delete from DIVERSOS_ENDERECOS where CADASTRO_ID = :P1');
    ex.Executar([pCadastroId]);

    t.Excluir;
    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function EFornecedor(pConexao: TConexao; pCadastroId: Integer): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select count(*) from FORNECEDORES where CADASTRO_ID = :P1');
  vSql.Pesquisar([pCadastroId]);

  Result := vSql.GetInt(0) > 0;

  vSql.Free;
end;

function getCodigoNomeFantasiaFornecedor(pConexao: TConexao; pCPFCNPJ: string): RecCadastroIdRazaoSocial;
var
  vSql: TConsulta;
begin
  Result.CadastroId := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  CADASTRO_ID, ');
  vSql.Add('  RAZAO_SOCIAL ');
  vSql.Add('from ');
  vSql.Add('  CADASTROS ');
  vSql.Add('where CPF_CNPJ = :P1 ');
  vSql.Add('and E_FORNECEDOR = ''S'' ');

  if vSql.Pesquisar([pCPFCNPJ]) then begin
    Result.CadastroId  := vSql.GetInt(0);
    Result.RazaoSocial := vSql.GetString(1);
  end;

  vSql.Free;
end;


end.
