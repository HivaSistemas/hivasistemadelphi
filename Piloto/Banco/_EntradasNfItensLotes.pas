unit _EntradasNfItensLotes;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecEntradasNfItensLotes = record
    EntradaId: Integer;
    ProdutoId: Integer;
    ItemId: Integer;
    Lote: string;
    Quantidade: Currency;
    DataFabricacao: TDate;
    DataVencimento: TDate;
    TipoControleEstoque: string;
    ExigirDataFabricacaoLote: string;
    ExigirDataVencimentoLote: string;
  end;

  TEntradasNfItensLotes = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordEntradasNfItensLotes: RecEntradasNfItensLotes;
  end;

function BuscarEntradasNfItensLotes(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEntradasNfItensLotes>;

function BuscarEntradasNfItensLotesComando(pConexao: TConexao; pComando: string): TArray<RecEntradasNfItensLotes>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TEntradasNfItensLotes }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ITL.ENTRADA_ID = :P1'
    );
end;

constructor TEntradasNfItensLotes.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ENTRADAS_NF_ITENS_LOTES');

  FSql := 
    'select ' +
    '  ITL.ENTRADA_ID, ' +
    '  ITL.ITEM_ID, ' +
    '  ITL.LOTE, ' +
    '  ITL.QUANTIDADE, ' +
    '  ITE.PRODUTO_ID, ' +
    '  ITL.DATA_FABRICACAO, ' +
    '  ITL.DATA_VENCIMENTO, ' +
    '  PRO.TIPO_CONTROLE_ESTOQUE, ' +
    '  PRO.EXIGIR_DATA_FABRICACAO_LOTE, ' +
    '  PRO.EXIGIR_DATA_VENCIMENTO_LOTE ' +
    'from ' +
    '  ENTRADAS_NF_ITENS_LOTES ITL ' +

    'inner join ENTRADAS_NOTAS_FISCAIS_ITENS ITE ' +
    'on ITL.ENTRADA_ID = ITE.ENTRADA_ID ' +
    'and ITL.ITEM_ID = ITE.ITEM_ID ' +

    'inner join PRODUTOS PRO ' +
    'on ITE.PRODUTO_ID = PRO.PRODUTO_ID ';

  setFiltros(getFiltros);

  AddColuna('ENTRADA_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('LOTE', True);
  AddColuna('QUANTIDADE');
  AddColunaSL('PRODUTO_ID');
  AddColuna('DATA_FABRICACAO');
  AddColuna('DATA_VENCIMENTO');
  AddColunaSL('TIPO_CONTROLE_ESTOQUE');
  AddColunaSL('EXIGIR_DATA_FABRICACAO_LOTE');
  AddColunaSL('EXIGIR_DATA_VENCIMENTO_LOTE');
end;

function TEntradasNfItensLotes.getRecordEntradasNfItensLotes: RecEntradasNfItensLotes;
begin
  Result.EntradaId      := getInt('ENTRADA_ID', True);
  Result.ItemId         := getInt('ITEM_ID', True);
  Result.Lote           := getString('LOTE', True);
  Result.Quantidade     := getDouble('QUANTIDADE');
  Result.ProdutoId      := getInt('PRODUTO_ID');
  Result.DataFabricacao := getData('DATA_FABRICACAO');
  Result.DataVencimento := getData('DATA_VENCIMENTO');

  Result.TipoControleEstoque      := getString('TIPO_CONTROLE_ESTOQUE');
  Result.ExigirDataFabricacaoLote := getString('EXIGIR_DATA_FABRICACAO_LOTE');
  Result.ExigirDataVencimentoLote := getString('EXIGIR_DATA_VENCIMENTO_LOTE');
end;

function BuscarEntradasNfItensLotes(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEntradasNfItensLotes>;
var
  i: Integer;
  t: TEntradasNfItensLotes;
begin
  Result := nil;
  t := TEntradasNfItensLotes.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordEntradasNfItensLotes;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarEntradasNfItensLotesComando(pConexao: TConexao; pComando: string): TArray<RecEntradasNfItensLotes>;
var
  i: Integer;
  t: TEntradasNfItensLotes;
begin
  Result := nil;
  t := TEntradasNfItensLotes.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordEntradasNfItensLotes;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
