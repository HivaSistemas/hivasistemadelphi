unit _Relacao.CurvaABC;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsRelatorios, Types, Math;

function BuscarCurvaProdutos(
  pConexao: TConexao;
  pComando: string;
  pComandoPodutoId: string;
  percentuaisOrdenados: TIntegerDynArray;
  classesOrdenados: TArray<string>
): TArray<RecProdutoCurvaABC>;

function BuscarCurvaClientes(
  pConexao: TConexao;
  pComando: string;
  percentuaisOrdenados: TIntegerDynArray;
  classesOrdenados: TArray<string>
): TArray<RecClienteCurvaABC>;

implementation

function BuscarCurvaProdutos(
  pConexao: TConexao;
  pComando: string;
  pComandoPodutoId: string;
  percentuaisOrdenados: TIntegerDynArray;
  classesOrdenados: TArray<string>
): TArray<RecProdutoCurvaABC>;
var
  i: Integer;
  vSql: TConsulta;

  vTipo: string;
  vClassificacao: string;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  with vSql.SQL do begin
    Add('select ');
    Add('  ''PV'' as TIPO, ');
    Add('  RES.PRODUTO_ID, ');
    Add('  RES.NOME_PRODUTO, ');
    Add('  RES.PRECO_MEDIO_UNITARIO, ');
    Add('  RES.QUANTIDADE, ');
    Add('  RES.UNIDADE_VENDA, ');
    Add('  RES.CUSTO_TOTAL, ');
    Add('  RES.VALOR_TOTAL, ');
    Add('  RES.PERCENTUAL, ');
    Add('  sum(PERCENTUAL) over(order by PERCENTUAL desc) as ACUMULATIVO_PERCENTUAL ');
    Add('from( ');
    Add('  select ');
    Add('    ITE.PRODUTO_ID, ');
    Add('    PRO.NOME as NOME_PRODUTO, ');
    Add('    round(avg(ITE.PRECO_UNITARIO), 2) as PRECO_MEDIO_UNITARIO, ');
    Add('    sum(ITE.QUANTIDADE) as QUANTIDADE, ');
    Add('    PRO.UNIDADE_VENDA, ');
    Add('    sum(ITE.VALOR_TOTAL) as VALOR_TOTAL, ');
    Add('    sum(ITE.VALOR_CUSTO_FINAL) as CUSTO_TOTAL, ');
    Add('    round(sum(ITE.VALOR_TOTAL) / sum(sum(ITE.VALOR_TOTAL)) over () * 100, 2) as PERCENTUAL, ');
    Add('    MAR.MARCA_ID ');
    Add('  from ');
    Add('    ORCAMENTOS_ITENS ITE ');

    Add('  inner join PRODUTOS PRO ');
    Add('  on ITE.PRODUTO_ID = PRO.PRODUTO_ID ');

    Add('  inner join MARCAS MAR ');
    Add('  on PRO.MARCA_ID = MAR.MARCA_ID ');

    Add('  inner join ORCAMENTOS ORC ');
    Add('  on ITE.ORCAMENTO_ID = ORC.ORCAMENTO_ID ');
    Add('  and ORC.STATUS in(''RE'',''GN'',''EM'') ');

    Add(pComando);

    Add('  group by ');
    Add('    ITE.PRODUTO_ID, ');
    Add('    PRO.NOME, ');
    Add('    PRO.UNIDADE_VENDA, ');
    Add('    MAR.MARCA_ID ');

    Add('  order by ');
    Add('    sum(ITE.VALOR_TOTAL) desc ');
    Add(') RES ');

    Add(pComandoPodutoId);

    Add('union all ');

    Add('select ');
    Add('  ''PQ'' as TIPO, ');
    Add('  RES.PRODUTO_ID, ');
    Add('  RES.NOME_PRODUTO, ');
    Add('  0 as PRECO_MEDIO_UNITARIO, ');
    Add('  RES.QUANTIDADE, ');
    Add('  RES.UNIDADE_VENDA, ');
    Add('  0 as VALOR_TOTAL, ');
    Add('  0 AS CUSTO_TOTAL, ');
    Add('  RES.PERCENTUAL, ');
    Add('  sum(PERCENTUAL) over(order by PERCENTUAL desc) as ACUMULATIVO_PERCENTUAL ');
    Add('from( ');
    Add('  select ');
    Add('    ITE.PRODUTO_ID, ');
    Add('    PRO.NOME as NOME_PRODUTO, ');
    Add('    sum(ITE.QUANTIDADE) as QUANTIDADE, ');
    Add('    PRO.UNIDADE_VENDA, ');
    Add('    round(sum(ITE.QUANTIDADE) / sum(sum(ITE.QUANTIDADE)) over () * 100, 2) as PERCENTUAL, ');
    Add('    MAR.MARCA_ID ');
    Add('  from ');
    Add('    ORCAMENTOS_ITENS ITE ');

    Add('  inner join PRODUTOS PRO ');
    Add('  on ITE.PRODUTO_ID = PRO.PRODUTO_ID ');

    Add('  inner join MARCAS MAR ');
    Add('  on PRO.MARCA_ID = MAR.MARCA_ID ');

    Add('  inner join ORCAMENTOS ORC ');
    Add('  on ITE.ORCAMENTO_ID = ORC.ORCAMENTO_ID ');
    Add('  and ORC.STATUS in(''RE'',''GN'',''EM'') ');

    Add(pComando);

    Add('  group by ');
    Add('    ITE.PRODUTO_ID, ');
    Add('    PRO.NOME, ');
    Add('    PRO.UNIDADE_VENDA, ');
    Add('    MAR.MARCA_ID ');

    Add('  order by ');
    Add('    round(sum(ITE.QUANTIDADE) / sum(sum(ITE.QUANTIDADE)) over () * 100, 2) desc ');
    Add(') RES ');

    Add(pComandoPodutoId);
  end;

  if vSql.Pesquisar then begin
    vTipo := '';
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].Tipo                  := vSql.GetString('TIPO');
      Result[i].ProdutoId             := vSql.GetInt('PRODUTO_ID');
      Result[i].NomeProduto           := vSql.GetString('NOME_PRODUTO');
      Result[i].PrecoMedioUnitario    := vSql.GetDouble('PRECO_MEDIO_UNITARIO');
      Result[i].Quantidade            := vSql.GetDouble('QUANTIDADE');
      Result[i].UnidadeVenda          := vSql.GetString('UNIDADE_VENDA');
      Result[i].ValorTotal            := vSql.GetDouble('VALOR_TOTAL');
      Result[i].CustoTotal            := vSql.GetDouble('CUSTO_TOTAL');
      Result[i].Percentual            := vSql.GetDouble('PERCENTUAL');
      Result[i].AcumulativoPercentual := vSql.GetDouble('ACUMULATIVO_PERCENTUAL');

      if vTipo <> Result[i].Tipo then begin
        vTipo := Result[i].tipo;
        Result[i].Classificacao := 'A';
        vSql.Next;
        Continue;
      end;

      if Result[i - 1].AcumulativoPercentual < percentuaisOrdenados[3] then
        vClassificacao := classesOrdenados[3]
      else if Result[i - 1].AcumulativoPercentual < percentuaisOrdenados[3] + percentuaisOrdenados[2] then
        vClassificacao := classesOrdenados[2]
      else if Result[i - 1].AcumulativoPercentual < percentuaisOrdenados[3] + percentuaisOrdenados[2] + percentuaisOrdenados[1] then
        vClassificacao := classesOrdenados[1]
      else
        vClassificacao := classesOrdenados[0];


      Result[i].Classificacao := vClassificacao;

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function BuscarCurvaClientes(
  pConexao: TConexao;
  pComando: string;
  percentuaisOrdenados: TIntegerDynArray;
  classesOrdenados: TArray<string>
): TArray<RecClienteCurvaABC>;
var
  i: Integer;
  vSql: TConsulta;

  vTipo: string;
  vClassificacao: string;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  with vSql.SQL do begin
    Add('select ');
    Add('  ''CV'' as TIPO, ');
    Add('  CLIENTE_ID, ');
    Add('  NOME_CLIENTE, ');
    Add('  VALOR_TOTAL, ');
    Add('  QUANTIDADE_VENDAS, ');
    Add('  PERCENTUAL, ');
    Add('  ACUMULATIVO_PERCENTUAL ');
    Add('from ');
    Add('  VW_CURVA_ABC_CLI_VALOR_TOTAL ');

    Add('union all ');

    Add('select ');
    Add('  ''CQ'' as TIPO, ');
    Add('  CLIENTE_ID, ');
    Add('  NOME_CLIENTE, ');
    Add('  0 as VALOR_TOTAL, ');
    Add('  QUANTIDADE_VENDAS, ');
    Add('  PERCENTUAL, ');
    Add('  ACUMULATIVO_PERCENTUAL ');
    Add('from ');
    Add('  VW_CURVA_ABC_CLI_QUANTIDADE ');

    Add(pComando);
  end;

  if vSql.Pesquisar then begin
    vTipo := '';
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].Tipo                  := vSql.GetString('TIPO');
      Result[i].ClienteId             := vSql.GetInt('CLIENTE_ID');
      Result[i].NomeCliente           := vSql.GetString('NOME_CLIENTE');
      Result[i].ValorTotal            := vSql.GetDouble('VALOR_TOTAL');
      Result[i].QuantidadeVendas      := vSql.GetInt('QUANTIDADE_VENDAS');
      Result[i].Percentual            := vSql.GetDouble('PERCENTUAL');
      Result[i].AcumulativoPercentual := vSql.GetDouble('ACUMULATIVO_PERCENTUAL');

      if vTipo <> Result[i].Tipo then begin
        vTipo := Result[i].tipo;
        Result[i].Classificacao := 'A';
        vSql.Next;
        Continue;
      end;

      if Result[i - 1].AcumulativoPercentual < percentuaisOrdenados[3] then
        vClassificacao := classesOrdenados[3]
      else if Result[i - 1].AcumulativoPercentual < percentuaisOrdenados[3] + percentuaisOrdenados[2] then
        vClassificacao := classesOrdenados[2]
      else if Result[i - 1].AcumulativoPercentual < percentuaisOrdenados[3] + percentuaisOrdenados[2] + percentuaisOrdenados[1] then
        vClassificacao := classesOrdenados[1]
      else
        vClassificacao := classesOrdenados[0];

      Result[i].Classificacao := vClassificacao;

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

end.


