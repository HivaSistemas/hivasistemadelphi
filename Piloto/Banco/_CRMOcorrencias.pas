unit _CRMOcorrencias;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TCRMOcorrencias = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordCRMOcorrencias: RecCRMOcorrencias;
  end;

function AtualizarUsuarioOcorrencia(
  pConexao: TConexao;
  pOcorrenciaId: Integer;
  pUsuario: Integer
): RecRetornoBD;

function AtualizarStatusCRMOcorrencias(
  pConexao: TConexao;
  pOcorrenciaId: Integer;
  pStatus: string
): RecRetornoBD;

function AtualizarCRMPrioridades(
  pConexao: TConexao;
  pOcorrenciaId: Integer;
  pPrioridade: string
): RecRetornoBD;

function AtualizarCRMOcorrencias(
  pConexao: TConexao;
  pOcorrenciaId: Integer;
  pCadastroId: Integer;
  pDescricao: string;
  pSolucao: string;
  pPrioridade: string
): RecRetornoBD;

function BuscarCRMOcorrencias(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCRMOcorrencias>;

function AtualizarSolicitacaoOcorrencia(
  pConexao: TConexao;
  pOcorrenciaId: Integer;
  pSolicitacao: Integer
): RecRetornoBD;

function BuscarCRMOcorrenciasComando(pConexao: TConexao; pComando: string): TArray<RecCRMOcorrencias>;

function ExcluirCRMOcorrencias(
  pConexao: TConexao;
  pCRMOcorrenciasId: Integer
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TCRMOcorrencias }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where OCORRENCIA_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      1,
      'where CADASTRO_ID = :P1',
      'order by DATA_CADASTRO DESC'
    );
end;

constructor TCRMOcorrencias.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CRM_OCORRENCIAS');

  FSql :=
    'select ' +
    '  OCORRENCIA_ID, ' +
    '  CADASTRO_ID, ' +
    '  DATA_CADASTRO, ' +
    '  DATA_CONCLUSAO, ' +
    '  DESCRICAO, ' +
    '  SOLUCAO, ' +
    '  STATUS, ' +
    '  SOLICITACAO, ' +
    '  PRIORIDADE ' +
    'from ' +
    '  CRM_OCORRENCIAS';

  SetFiltros(GetFiltros);

  AddColuna('OCORRENCIA_ID', True);
  AddColuna('CADASTRO_ID', True);
  AddColunaSL('DATA_CADASTRO');
  AddColunaSL('DATA_CONCLUSAO');
  AddColuna('DESCRICAO');
  AddColuna('SOLUCAO');
  AddColuna('PRIORIDADE');
  AddColunaSL('STATUS');
  AddColunaSL('SOLICITACAO');
end;

function TCRMOcorrencias.GetRecordCRMOcorrencias: RecCRMOcorrencias;
begin
  Result.ocorrencia_id := GetInt('OCORRENCIA_ID', True);
  Result.cadastro_id    := GetInt('CADASTRO_ID', True);
  Result.dataCadastro   := getData('DATA_CADASTRO');
  Result.dataConclusao  := getData('DATA_CONCLUSAO');
  Result.descricao      := getString('DESCRICAO');
  Result.solucao        := getString('SOLUCAO');
  Result.solicitacao    := getInt('SOLICITACAO');
  Result.status         := getString('STATUS');
  Result.prioridade     := getString('PRIORIDADE');

  if Result.status = 'AB' then
    Result.status_analitico := 'Aberto'
  else if Result.status = 'CO' then
    Result.status_analitico := 'Concl�ido'
  else if Result.status = 'PR' then
    Result.status_analitico := 'Programa��o'
  else if Result.status = 'TE' then
    Result.status_analitico := 'Teste'
  else if Result.status = 'RE' then
    Result.status_analitico := 'Recusado';

  if Result.prioridade = 'BA' then
    Result.prioridade_analitico := 'Baixa'
  else if Result.prioridade = 'ME' then
    Result.prioridade_analitico := 'M�dia'
  else if Result.prioridade = 'AL' then
    Result.prioridade_analitico := 'Alta'
  else if Result.prioridade = 'UR' then
    Result.prioridade_analitico := 'Urgente'

end;

function AtualizarCRMOcorrencias(
  pConexao: TConexao;
  pOcorrenciaId: Integer;
  pCadastroId: Integer;
  pDescricao: string;
  pSolucao: string;
  pPrioridade: string
): RecRetornoBD;
var
  t: TCRMOcorrencias;
  vExec: TExecucao;
  novo: Boolean;
  seq: TSequencia;
  i: Integer;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_CRM_OCORRENCIAS');

  t := TCRMOcorrencias.Create(pConexao);
  vExec := TExecucao.Create(pConexao);

  novo := pOcorrenciaId = 0;
  if novo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_CRM_OCORRENCIAS_ID');
    pOcorrenciaId := seq.GetProximaSequencia;
    result.AsInt := pOcorrenciaId;
    seq.Free;
  end;

  t.SetInt('OCORRENCIA_ID', pOcorrenciaId, True);
  t.SetInt('CADASTRO_ID', pCadastroId, True);
  t.SetString('DESCRICAO', pDescricao);
  t.SetString('SOLUCAO', pSolucao);
  t.setString('PRIORIDADE', pPrioridade);

  try
    pConexao.IniciarTransacao;

    if novo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarCRMOcorrencias(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCRMOcorrencias>;
var
  i: Integer;
  t: TCRMOcorrencias;
begin
  Result := nil;
  t := TCRMOcorrencias.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordCRMOcorrencias;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirCRMOcorrencias(
  pConexao: TConexao;
  pCRMOcorrenciasId: Integer
): RecRetornoBD;
var
  t: TCRMOcorrencias;
begin
  Result.TeveErro := False;
  t := TCRMOcorrencias.Create(pConexao);

  t.SetInt('OCORRENCIA_ID', pCRMOcorrenciasId, True);

  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function BuscarCRMOcorrenciasComando(pConexao: TConexao; pComando: string): TArray<RecCRMOcorrencias>;
var
  i: Integer;
  t: TCRMOcorrencias;
begin
  Result := nil;
  t := TCRMOcorrencias.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordCRMOcorrencias;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function AtualizarStatusCRMOcorrencias(
  pConexao: TConexao;
  pOcorrenciaId: Integer;
  pStatus: string
): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  vExec := TExecucao.Create(pConexao);
  vExec.SQL.Text := 'update CRM_OCORRENCIAS set STATUS = :P1 where OCORRENCIA_ID = :P2';

  try
    pConexao.IniciarTransacao;

    vExec.Executar([pStatus, pOcorrenciaId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

end;

function AtualizarUsuarioOcorrencia(
  pConexao: TConexao;
  pOcorrenciaId: Integer;
  pUsuario: Integer
): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  vExec := TExecucao.Create(pConexao);
  vExec.SQL.Text := 'update CRM_OCORRENCIAS set USUARIO = :P1 where OCORRENCIA_ID = :P2';

  try
    pConexao.IniciarTransacao;

    vExec.Executar([pUsuario, pOcorrenciaId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
end;

function AtualizarSolicitacaoOcorrencia(
  pConexao: TConexao;
  pOcorrenciaId: Integer;
  pSolicitacao: Integer
): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  vExec := TExecucao.Create(pConexao);
  vExec.SQL.Text := 'update CRM_OCORRENCIAS set SOLICITACAO = :P1 where OCORRENCIA_ID = :P2';

  try
    pConexao.IniciarTransacao;

    vExec.Executar([pSolicitacao, pOcorrenciaId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
end;

function AtualizarCRMPrioridades(
  pConexao: TConexao;
  pOcorrenciaId: Integer;
  pPrioridade: string
): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  vExec := TExecucao.Create(pConexao);
  vExec.SQL.Text := 'update CRM_OCORRENCIAS set PRIORIDADE = :P1 where OCORRENCIA_ID = :P2';

  try
    pConexao.IniciarTransacao;

    vExec.Executar([pPrioridade, pOcorrenciaId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

end;

end.

