unit _ProdutosVenda;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsOrcamentosVendas, System.StrUtils;

{$M+}
type
  TProdutosVenda = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao; pPDV: Boolean; pSomentePromocao: Boolean);
  protected
    function getRecordProdutosVenda: RecProdutosVendas;
  end;

function BuscarProdutosVendas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pPDV: Boolean;
  pSomentePromocao: Boolean = False
): TArray<RecProdutosVendas>;

function BuscarProdutosVendasComando(pConexao: TConexao; pComando: string): TArray<RecProdutosVendas>;

function BuscarFotosProdutosVendas(pConexao: TConexao; pProdutosIds: TArray<Integer>): TArray<RecFotosProdutosVendas>;

function getProdutoIdViaCodigo(pConexao: TConexao; pCodigo: string; pTipo: string): Integer;

function getFiltros: TArray<RecFiltros>;

function BuscarEstoqueTodasEmpresaExcetoLogada(pConexao: TConexao; empresaId: Integer; produtoId: Integer): Double;

implementation

{ TProdutosVenda }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 10);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'and EMPRESA_ID = :P1 ' +
      'and PRODUTO_ID = :P2 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome do produto(Avan�ado)',
      False,
      0,
      'and EMPRESA_ID = :P1 ' +
      'and NOME like ''%'' || :P2 || ''%'' ',
      'order by ' +
      '  NOME',
      '',
      True
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Marca do produto',
      False,
      1,
      'and EMPRESA_ID = :P1 ' +
      'and NOME_MARCA like ''%'' || :P2 || ''%'' ' +
      'order by ' +
      '  NOME_MARCA '
    );

  Result[3] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'C�digo de barras',
      False,
      2,
      'and EMPRESA_ID = :P1 ' +
      'and (CODIGO_BARRAS = :P2 or PRODUTO_ID IN (select PRODUTO_ID from PRODUTOS_CODIGOS_BARRAS_AUX where CODIGO_BARRAS = :P2))' +
      'order by ' +
      '  NOME '
    );

  Result[4] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome/Marca produto (avan�.)',
      False,
      3,
      'and EMPRESA_ID = :P1 ' +
      'and NOME like ''%'' || :P2 || ''%'' ' +
      'and NOME_MARCA like ''%'' || :P3 || ''%'' ',
      'order by ' +
      '  NOME ',
      '',
      True
    );

  Result[5] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'C�digo do produto',
      False,
      4,
      'and EMPRESA_ID = :P1 ' +
      'and PRODUTO_ID = :P2 ' +
      'order by ' +
      '  NOME '
    );

  Result[6] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome/Marca produto',
      True,
      3,
      'and EMPRESA_ID = :P1 ' +
      'and NOME like :P2 || ''%'' ' +
      'and NOME_MARCA like :P3 || ''%'' ',
      'order by ' +
      '  NOME ',
      '',
      True
    );

  Result[7] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'C�digo Sistema Anterior',
      False,
      4,
      'and EMPRESA_ID = :P1 ' +
      'and CHAVE_IMPORTACAO = :P2 '
    );

  Result[8] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'C�digo Refer�ncia Ind�stria',
      False,
      4,
      'and EMPRESA_ID = :P1 ' +
      'and CODIGO_ORIGINAL_FABRICANTE like ''%'' || :P2 || ''%'' ',
      'order by ' +
      '  NOME '
    );

  Result[9] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome/C�digo Refer�ncia Ind�stria',
      False,
      3,
      'and EMPRESA_ID = :P1 ' +
      'and NOME like :P2 || ''%'' ' +
      'and CODIGO_ORIGINAL_FABRICANTE like ''%'' || :P3 || ''%'' ',
      'order by ' +
      '  NOME ',
      ''
    );
end;

constructor TProdutosVenda.Create(pConexao: TConexao; pPDV: Boolean; pSomentePromocao: Boolean);
begin
  inherited Create(pConexao, 'VW_PRODUTOS_VENDAS');

  FSql :=
    'select ' +
    '  EMPRESA_ID, ' +
    '  FORNECEDOR_ID, ' +
    '  PRODUTO_ID, ' +
    '  PESO, ' +
    '  MULTIPLO_VENDA, ' +
    '  0 as MULTIPLO_VENDA_2, ' +
    '  0 as MULTIPLO_VENDA_3, ' +
    '  PRECO_VAREJO, ' +
    '  QUANTIDADE_MIN_PRECO_VAREJO, ' +
    '  PRECO_PROMOCIONAL, ' +
    '  QUANTIDADE_MIN_PROMOCIONAL, ' +
    '  PRECO_ATACADO_1, ' +
    '  QUANTIDADE_MIN_PRECO_ATAC_1, ' +
    '  PRECO_ATACADO_2, ' +
    '  QUANTIDADE_MIN_PRECO_ATAC_2, ' +
    '  PRECO_ATACADO_3, ' +
    '  QUANTIDADE_MIN_PRECO_ATAC_3, ' +
    '  PRECO_PDV, ' +
    '  QUANTIDADE_MINIMA_PDV, ' +
    '  ESTOQUE, ' +
    '  DISPONIVEL, ' +
    '  NOME_MARCA, ' +
    '  CARACTERISTICAS, ' +
    '  UNIDADE_VENDA, ' +
    '  NOME, ' +
    '  TIPO_CONTROLE_ESTOQUE, ' +
    '  VALOR_ADICIONAL_FRETE, ' +
    '  CODIGO_BARRAS, ' +
    '  CODIGO_ORIGINAL_FABRICANTE, ' +
    '  FISICO, ' +
    '  CONTROLA_PESO, '+
    '  PRODUTO_DIVERSOS_PDV, ' +
    '  PRECO_PONTA_ESTOQUE ' +
    'from ' +
    '  VW_PRODUTOS_VENDAS ' +

    'where ' + IfThen(not pPDV, 'PRECO_VAREJO', 'PRECO_PDV') + ' > 0 ' +
    ' ' + IfThen(not pPDV, 'and PRODUTO_DIVERSOS_PDV = ''N'' ');

    if pSomentePromocao then
      FSql := FSql + ' and PRECO_PROMOCIONAL > 0 ';

  setFiltros(getFiltros);

  AddColunaSL('EMPRESA_ID');
  AddColunaSL('FORNECEDOR_ID');
  AddColunaSL('PRODUTO_ID');
  AddColunaSL('PESO');
  AddColunaSL('MULTIPLO_VENDA');
  AddColunaSL('MULTIPLO_VENDA_2');
  AddColunaSL('MULTIPLO_VENDA_3');
  AddColunaSL('PRECO_VAREJO');
  AddColunaSL('QUANTIDADE_MIN_PRECO_VAREJO');
  AddColunaSL('PRECO_PROMOCIONAL');
  AddColunaSL('QUANTIDADE_MIN_PROMOCIONAL');
  AddColunaSL('PRECO_ATACADO_1');
  AddColunaSL('QUANTIDADE_MIN_PRECO_ATAC_1');
  AddColunaSL('PRECO_ATACADO_2');
  AddColunaSL('QUANTIDADE_MIN_PRECO_ATAC_2');
  AddColunaSL('PRECO_ATACADO_3');
  AddColunaSL('QUANTIDADE_MIN_PRECO_ATAC_3');
  AddColunaSL('PRECO_PDV');
  AddColunaSL('QUANTIDADE_MINIMA_PDV');
  AddColunaSL('ESTOQUE');
  AddColunaSL('DISPONIVEL');
  AddColunaSL('NOME_MARCA');
  AddColunaSL('CARACTERISTICAS');
  AddColunaSL('UNIDADE_VENDA');
  AddColunaSL('NOME');
  AddColunaSL('TIPO_CONTROLE_ESTOQUE');
  AddColunaSL('VALOR_ADICIONAL_FRETE');
  AddColunaSL('CODIGO_BARRAS');
  AddColunaSL('CODIGO_ORIGINAL_FABRICANTE');
  AddColunaSL('FISICO');
  AddColunaSL('CONTROLA_PESO');
  AddColunaSL('PRODUTO_DIVERSOS_PDV');
  AddColunaSL('PRECO_PONTA_ESTOQUE');
end;

function TProdutosVenda.getRecordProdutosVenda: RecProdutosVendas;
begin
  Result.empresa_id              := getInt('EMPRESA_ID');
  Result.fornecedor_id           := getInt('FORNECEDOR_ID');
  Result.produto_id              := getInt('PRODUTO_ID');
  Result.peso                    := getDouble('PESO');
  Result.multiplo_venda          := getDouble('MULTIPLO_VENDA');
  Result.multiplo_venda_2        := getDouble('MULTIPLO_VENDA_2');
  Result.multiplo_venda_3        := getDouble('MULTIPLO_VENDA_3');
  Result.preco_varejo            := getDouble('PRECO_VAREJO');
  Result.QuantidadeMinPrecoVarejo:= getDouble('QUANTIDADE_MIN_PRECO_VAREJO');
  Result.PrecoPromocional        := getDouble('PRECO_PROMOCIONAL');
  Result.QuantidadeMinPrecoPromocional := getDouble('QUANTIDADE_MIN_PROMOCIONAL');
  Result.PrecoAtacado1           := getDouble('PRECO_ATACADO_1');
  Result.QuantidadeMinPrecoAtac1 := getDouble('QUANTIDADE_MIN_PRECO_ATAC_1');
  Result.PrecoAtacado2           := getDouble('PRECO_ATACADO_2');
  Result.QuantidadeMinPrecoAtac2 := getDouble('QUANTIDADE_MIN_PRECO_ATAC_2');
  Result.PrecoAtacado3           := getDouble('PRECO_ATACADO_3');
  Result.QuantidadeMinPrecoAtac3 := getDouble('QUANTIDADE_MIN_PRECO_ATAC_3');
  Result.preco_pdv               := getDouble('PRECO_PDV');
  Result.QuantidadeMinimaPDV     := getDouble('QUANTIDADE_MINIMA_PDV');
  Result.Estoque                 := getDouble('ESTOQUE');
  Result.disponivel              := getDouble('DISPONIVEL');
  Result.nome_marca              := getString('NOME_MARCA');
  Result.caracteristicas         := getString('CARACTERISTICAS');
  Result.unidade_venda           := getString('UNIDADE_VENDA');
  Result.nome                    := getString('NOME');
  Result.TipoControleEstoque     := getString('TIPO_CONTROLE_ESTOQUE');
  Result.ValorAdicionalFrete     := getDouble('VALOR_ADICIONAL_FRETE');
  Result.CodigoBarras            := getString('CODIGO_BARRAS');
  Result.CodigoOriginalFabricante:= getString('CODIGO_ORIGINAL_FABRICANTE');
  Result.Fisico                  := getDouble('FISICO');
  Result.ControlaPeso            := getString('CONTROLA_PESO');
  Result.produtoDiversosPDV      := getString('PRODUTO_DIVERSOS_PDV');
  Result.PrecoPontaEstoque       := getDouble('PRECO_PONTA_ESTOQUE');
end;

function BuscarProdutosVendas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pPDV: Boolean;
  pSomentePromocao: Boolean = False
): TArray<RecProdutosVendas>;
var
  i: Integer;
  t: TProdutosVenda;

  procedure CarregarFotos;
  var
    vSql: TConsulta;
  begin
    vSql := TConsulta.Create(pConexao);

    with vSql.SQL do begin
      Add('select ');
      Add('  FOTO_1, ');
      Add('  FOTO_2, ');
      Add('  FOTO_3 ');
      Add('from ');
      Add('  PRODUTOS ');
      Add('where PRODUTO_ID = :P1 ');
      Add('and FOTO_1 is not null ');
    end;

    if vSql.Pesquisar([Result[i].produto_id]) then begin
      Result[i].Foto1     := vSql.getFoto(0);
      Result[i].Foto2     := vSql.getFoto(1);
      Result[i].Foto3     := vSql.getFoto(2);
    end;
    vSql.Active := False;
    vSql.Free;
  end;

begin
  Result := nil;
  t := TProdutosVenda.Create(pConexao, pPDV, pSomentePromocao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordProdutosVenda;

      if pPDV then
        CarregarFotos;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarProdutosVendasComando(pConexao: TConexao; pComando: string): TArray<RecProdutosVendas>;
var
  i: Integer;
  t: TProdutosVenda;
begin
  Result := nil;
  t := TProdutosVenda.Create(pConexao, False, False);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordProdutosVenda;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarFotosProdutosVendas(pConexao: TConexao; pProdutosIds: TArray<Integer>): TArray<RecFotosProdutosVendas>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;

  if pProdutosIds = nil then
    Exit;

  vSql := TConsulta.Create(pConexao);

  with vSql.SQL do begin
    Add('select ');
    Add('  PRODUTO_ID, ');
    Add('  FOTO_1, ');
    Add('  FOTO_2, ');
    Add('  FOTO_3 ');
    Add('from ');
    Add('  PRODUTOS ');
    Add('where ' + FiltroInInt('PRODUTO_ID', pProdutosIds));
    Add('and FOTO_1 is not null or FOTO_2 is not null or FOTO_3 is not null ');
  end;

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].ProdutoId := vSql.GetInt(0);
      Result[i].Foto1     := vSql.getFoto(1);
      Result[i].Foto2     := vSql.getFoto(2);
      Result[i].Foto3     := vSql.getFoto(3);

      vSql.Next;
    end;
  end;

  vSql.Free;
end;

{
  pTipo
  C - C�digo de barras
  D - C�digo de balan�a
}
function getProdutoIdViaCodigo(pConexao: TConexao; pCodigo: string; pTipo: string): Integer;
var
  vSql: TConsulta;
begin
  Result := -1;
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  PRODUTO_ID ');
  vSql.SQL.Add('from ');
  vSql.SQL.Add('  VW_PRODUTOS_VENDA_PDV ');

  if pTipo = 'C' then
    vSql.SQL.Add('where CODIGO_BARRAS = :P1')
  else
    vSql.SQL.Add('where CODIGO_BALANCA = :P1');

  if vSql.Pesquisar([pCodigo]) then
    Result := vSql.GetInt(0);

  vSql.Free;
end;

function BuscarEstoqueTodasEmpresaExcetoLogada(pConexao: TConexao; empresaId: Integer; produtoId: Integer): Double;
var
  vSql: TConsulta;
begin
  Result := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  sum(DISPONIVEL) ');
  vSql.SQL.Add('from VW_ESTOQUES_DIVISAO DIV ');
  vSql.SQL.Add('where PRODUTO_ID = :P1 ');
  vSql.SQL.Add('and EMPRESA_ID <> :P2 ');

  if vSql.Pesquisar([produtoId, empresaId]) then
    Result := vSql.GetDouble(0);

  vSql.Free;
end;

end.
