unit _ContasPagarBaixasItens;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _Sessao, _RecordsFinanceiros;

{$M+}
type
  TContasPagarBaixasItens = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordContasPagarBaixasItens: RecContasPagarBaixasItens;
  end;

function AtualizarContasPagarBaixasItens(
  pConexao: TConexao;
  pBaixaId: Integer;
  pContaPagarId: Integer;
  pValorDinheiro: Double;
  pValorCheque: Double;
  pValorCartao: Double;
  pValorCobranca: Double;
  pValorCredito: Double;
  pValorDesconto: Double;
  pValorJuros: Double
): RecRetornoBD;

function BuscarContasPagarBaixasItenss(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecContasPagarBaixasItens>;

function ExcluirContasPagarBaixasItens(
  pConexao: TConexao
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TContasPagarBaixasItens }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CPI.BAIXA_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CPI.BAIXA_ID = :P1 ' +
      'order by ' +
      '  COP.DATA_VENCIMENTO '
    );
end;

constructor TContasPagarBaixasItens.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONTAS_PAGAR_BAIXAS_ITENS');

  FSql :=
    'select ' +
    '  CPI.BAIXA_ID, ' +
    '  CPI.PAGAR_ID, ' +
    '  CPI.VALOR_DINHEIRO, ' +
    '  CPI.VALOR_CHEQUE, ' +
    '  CPI.VALOR_CARTAO, ' +
    '  CPI.VALOR_COBRANCA, ' +
    '  CPI.VALOR_CREDITO, ' +
    '  CPI.VALOR_DESCONTO, ' +
    '  CPI.VALOR_JUROS, ' +
    '  COP.DOCUMENTO, ' +
    '  COP.CADASTRO_ID, ' +
    '  CAD.RAZAO_SOCIAL as NOME_FORNECEDOR, ' +
    '  COP.VALOR_DOCUMENTO, ' +
    '  COP.DATA_VENCIMENTO, ' +
    '  COP.COBRANCA_ID, ' +
    '  TPC.NOME as NOME_TIPO_COBRANCA ' +
    'from ' +
    '  CONTAS_PAGAR_BAIXAS_ITENS CPI ' +

    'inner join CONTAS_PAGAR COP ' +
    'on CPI.PAGAR_ID = COP.PAGAR_ID ' +

    'inner join CADASTROS CAD ' +
    'on COP.CADASTRO_ID = CAD.CADASTRO_ID ' +

    'inner join TIPOS_COBRANCA TPC ' +
    'on COP.COBRANCA_ID = TPC.COBRANCA_ID ';

  setFiltros(getFiltros);

  AddColuna('BAIXA_ID', True);
  AddColuna('PAGAR_ID', True);
  AddColuna('VALOR_DINHEIRO');
  AddColuna('VALOR_CHEQUE');
  AddColuna('VALOR_CARTAO');
  AddColuna('VALOR_COBRANCA');
  AddColuna('VALOR_CREDITO');
  AddColuna('VALOR_DESCONTO');
  AddColuna('VALOR_JUROS');
  AddColunaSL('DOCUMENTO');
  AddColunaSL('CADASTRO_ID');
  AddColunaSL('NOME_FORNECEDOR');
  AddColunaSL('VALOR_DOCUMENTO');
  AddColunaSL('DATA_VENCIMENTO');
  AddColunaSL('COBRANCA_ID');
  AddColunaSL('NOME_TIPO_COBRANCA');
end;

function TContasPagarBaixasItens.getRecordContasPagarBaixasItens: RecContasPagarBaixasItens;
begin
  Result.baixa_id                := getInt('BAIXA_ID', True);
  Result.PagarId                 := getInt('PAGAR_ID', True);
  Result.valor_dinheiro          := getDouble('VALOR_DINHEIRO');
  Result.valor_cheque            := getDouble('VALOR_CHEQUE');
  Result.valor_cartao            := getDouble('VALOR_CARTAO');
  Result.valor_cobranca          := getDouble('VALOR_COBRANCA');
  Result.valor_credito           := getDouble('VALOR_CREDITO');
  Result.valor_desconto          := getDouble('VALOR_DESCONTO');
  Result.ValorJuros              := getDouble('VALOR_JUROS');
  Result.CadastroId              := getInt('CADASTRO_ID');
  Result.NomeFornecedor          := getString('NOME_FORNECEDOR');
  Result.Documento               := getString('DOCUMENTO');
  Result.ValorDocumento          := getDouble('VALOR_DOCUMENTO');
  Result.DataVencimento          := getData('DATA_VENCIMENTO');
  Result.CobrancaId              := getInt('COBRANCA_ID');
  Result.NomeTipoCobranca        := getString('NOME_TIPO_COBRANCA');

  Result.ValorLiquido :=
    Result.ValorDocumento +
    Result.ValorJuros -
    Result.valor_desconto;
end;

function AtualizarContasPagarBaixasItens(
  pConexao: TConexao;
  pBaixaId: Integer;
  pContaPagarId: Integer;
  pValorDinheiro: Double;
  pValorCheque: Double;
  pValorCartao: Double;
  pValorCobranca: Double;
  pValorCredito: Double;
  pValorDesconto: Double;
  pValorJuros: Double
): RecRetornoBD;
var
  t: TContasPagarBaixasItens;
begin
  Result.Iniciar;
  t := TContasPagarBaixasItens.Create(pConexao);

  try
    t.setInt('BAIXA_ID', pBaixaId, True);
    t.setInt('PAGAR_ID', pContaPagarId, True);
    t.setDouble('VALOR_DINHEIRO', pValorDinheiro);
    t.setDouble('VALOR_CHEQUE', pValorCheque);
    t.setDouble('VALOR_CARTAO', pValorCartao);
    t.setDouble('VALOR_COBRANCA', pValorCobranca);
    t.setDouble('VALOR_CREDITO', pValorCredito);
    t.setDouble('VALOR_DESCONTO', pValorDesconto);
    t.setDouble('VALOR_JUROS', pValorJuros);

    t.Inserir;
  except
    on e: Exception do begin
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarContasPagarBaixasItenss(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecContasPagarBaixasItens>;
var
  i: Integer;
  t: TContasPagarBaixasItens;
begin
  Result := nil;
  t := TContasPagarBaixasItens.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordContasPagarBaixasItens;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirContasPagarBaixasItens(
  pConexao: TConexao
): RecRetornoBD;
var
  t: TContasPagarBaixasItens;
begin
  Result.TeveErro := False;
  t := TContasPagarBaixasItens.Create(pConexao);
  try

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
