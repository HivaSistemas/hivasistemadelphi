unit _TransferenciasLocais;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, _TransferenciasLocaisItens;

{$M+}
type
  RecTransferenciasLocais = record
    TransferenciaLocalId: Integer;
    EmpresaId: Integer;
    NomeEmpresa: string;
    LocalOrigemId: Integer;
    NomeLocal: string;
    UsuarioCadastroId: Integer;
    NomeUsuarioCadastro: string;
    DataHoraCadastro: TDateTime;
    Observacao: string;
  end;

  TTransferenciasLocais = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordTransferencia: RecTransferenciasLocais;
  end;

function AtualizarTransferenciaLocal(
  pConexao: TConexao;
  pAjusteEstoqueId: Integer;
  pEmpresaId: Integer;
  pLocalOrigemId: Integer;
  pObservacao: string;
  pItens: TArray<RecTransferenciasLocaisItens>
): RecRetornoBD;

function BuscarTransferenciasLocais(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTransferenciasLocais>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TTransferenciasLocais }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where TRANSFERENCIA_LOCAL_ID = :P1'
    );
end;

constructor TTransferenciasLocais.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'TRANSFERENCIAS_LOCAIS');

  FSql :=
    'select ' +
    '  TRA.TRANSFERENCIA_LOCAL_ID, ' +
    '  TRA.EMPRESA_ID, ' +
    '  EMP.NOME_FANTASIA, ' +
    '  TRA.USUARIO_CADASTRO_ID, ' +
    '  TRA.LOCAL_ORIGEM_ID, ' +
    '  LOC.NOME as NOME_LOCAL, ' +
    '  FUN.NOME as NOME_USUARIO_CADASTRO, ' +
    '  TRA.DATA_HORA_CADASTRO, ' +
    '  TRA.OBSERVACAO ' +
    'from ' +
    '  TRANSFERENCIAS_LOCAIS TRA ' +

    'inner join FUNCIONARIOS FUN ' +
    'on AJU.USUARIO_AJUSTE_ID = FUN.FUNCIONARIO_ID ' +

    'inner join EMPRESAS EMP ' +
    'on AJU.EMPRESA_ID = EMP.EMPRESA_ID ';

  setFiltros(getFiltros);

  AddColuna('TRANSFERENCIA_LOCAL_ID', True);
  AddColuna('EMPRESA_ID');
  AddColunaSL('NOME_FANTASIA');
  AddColuna('LOCAL_ORIGEM_ID');
  AddColunaSL('NOME_LOCAL');
  AddColunaSL('USUARIO_CADASTRO_ID');
  AddColunaSL('NOME_USUARIO_CADASTRO');
  AddColunaSL('DATA_HORA_CADASTRO');
  AddColuna('OBSERVACAO');
end;

function TTransferenciasLocais.getRecordTransferencia: RecTransferenciasLocais;
begin
  Result.TransferenciaLocalId := getInt('TRANSFERENCIA_LOCAL_ID', True);
  Result.EmpresaId            := getInt('EMPRESA_ID');
  Result.NomeEmpresa          := getString('NOME_FANTASIA');
  Result.LocalOrigemId        := getInt('LOCAL_ORIGEM_ID');
  Result.NomeLocal            := getString('MOTIVO_AJUSTE_ESTOQUE');
  Result.UsuarioCadastroId    := getInt('USUARIO_AJUSTE_ID');
  Result.NomeUsuarioCadastro  := getString('NOME_USUARIO_AJUSTE');
  Result.DataHoraCadastro     := getData('DATA_HORA_AJUSTE');
  Result.Observacao           := getString('OBSERVACAO');
end;

function AtualizarTransferenciaLocal(
  pConexao: TConexao;
  pAjusteEstoqueId: Integer;
  pEmpresaId: Integer;
  pLocalOrigemId: Integer;
  pObservacao: string;
  pItens: TArray<RecTransferenciasLocaisItens>
): RecRetornoBD;
var
  vTransf: TTransferenciasLocais;
  vItens: TTransferenciasLocaisItens;

  i: Integer;
  novo: Boolean;
  vExec: TExecucao;
begin
  Result.TeveErro := False;
  pConexao.SetRotina('REALIZAR_TRANSF_LOCAIS');

  vExec := TExecucao.Create(pConexao);
  vTransf := TTransferenciasLocais.Create(pConexao);
  vItens := TTransferenciasLocaisItens.Create(pConexao);

  novo := pAjusteEstoqueId = 0;
  if novo then begin
    pAjusteEstoqueId := TSequencia.Create(pConexao, 'SEQ_TRANSFERENCIA_LOCAL_ID').GetProximaSequencia;
    result.AsInt := pAjusteEstoqueId;
  end;

  try
    pConexao.IniciarTransacao;

    vTransf.setInt('TRANSFERENCIA_LOCAL_ID', pAjusteEstoqueId, True);
    vTransf.setInt('EMPRESA_ID', pEmpresaId);
    vTransf.setInt('LOCAL_ORIGEM_ID', pLocalOrigemId);
    vTransf.setStringN('OBSERVACAO', pObservacao);

    if novo then
      vTransf.Inserir
    else
      vTransf.Atualizar;

    if not novo then begin
      vExec.SQL.Clear;
      vExec.SQL.Add('delete from AJUSTES_ESTOQUE_ITENS where TRANSFERENCIA_LOCAL_ID = :P1');
      vExec.Executar([pAjusteEstoqueId]);
    end;

    vItens.setInt('TRANSFERENCIA_LOCAL_ID', pAjusteEstoqueId, True);
    for i := Low(pItens) to High(pItens) do begin
      vItens.setInt('LOCAL_DESTINO_ID', pItens[i].LocalDestinoId, True);
      vItens.setInt('ITEM_ID', pItens[i].ItemId, True);
      vItens.setString('LOTE', pItens[i].Lote, True);
      vItens.setInt('PRODUTO_ID', pItens[i].ProdutoId);
      vItens.setDouble('QUANTIDADE', pItens[i].quantidade);

      vItens.Inserir;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  vExec.Free;
  vItens.Free;
  vTransf.Free;
end;

function BuscarTransferenciasLocais(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTransferenciasLocais>;
var
  i: Integer;
  t: TTransferenciasLocais;
begin
  Result := nil;
  t := TTransferenciasLocais.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordTransferencia;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
