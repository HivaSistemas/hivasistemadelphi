unit _ContasFuncAutorizados;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecContasFuncAutorizados = record
    ContaId: string;
    FuncionarioId: Integer;
  end;

  TContasFuncAutorizados = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordContasFuncAutorizados: RecContasFuncAutorizados;
  end;

function BuscarContasFuncAutorizados(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecContasFuncAutorizados>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TContasFuncAutorizados }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CONTA_ID = :P1'
    );
end;

constructor TContasFuncAutorizados.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONTAS_FUNC_AUTORIZADOS');

  FSql := 
    'select ' +
    '  CONTA_ID, ' +
    '  FUNCIONARIO_ID ' +
    'from ' +
    '  CONTAS_FUNC_AUTORIZADOS';

  setFiltros(getFiltros);

  AddColuna('CONTA_ID', True);
  AddColuna('FUNCIONARIO_ID', True);
end;

function TContasFuncAutorizados.getRecordContasFuncAutorizados: RecContasFuncAutorizados;
begin
  Result.ContaId                    := getString('CONTA_ID', True);
  Result.FuncionarioId              := getInt('FUNCIONARIO_ID', True);
end;

function BuscarContasFuncAutorizados(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecContasFuncAutorizados>;
var
  i: Integer;
  t: TContasFuncAutorizados;
begin
  Result := nil;
  t := TContasFuncAutorizados.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordContasFuncAutorizados;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
