unit _Inventario;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils,
  _InventarioItens;

{$M+}
type
  TTipoData = (tdInicial, tdFinal);

  RecInventario = class
  public
    InventarioId: Integer;
    Nome: string;
    DataInicio: TDateTime;
    DataFinal: TDateTime;
    EmpresaId: integer;
    UltimaContagem: integer;
  end;

  TInventario = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordInventario: RecInventario;
  end;

function AtualizarInventario(
  pConexao: TConexao;
  pInventarioId: Integer;
  pEmpresaId: Integer;
  pNome: string;
  pDataInicio: TDateTime;
  pDataFinal: TDateTime;
  pUltimaContagem: Integer;
  pInventarioItens: TArray<RecInventarioItens>
): RecRetornoBD;

function BuscarInventario(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecInventario>;

function ExcluirInventario(
  pConexao: TConexao;
  pInventarioId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TInventario }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'C�digo',
      True,
      0,
      'where INVENTARIO_ID = :P1'
    );
  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome',
      True,
      1,
      'where NOME like :P1 || ''%'' '
    );
end;

constructor TInventario.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'INVENTARIO');

  FSql :=
    'select ' +
    '  INVENTARIO_ID, ' +
    '  NOME, ' +
    '  DATA_INICIO, ' +
    '  DATA_FINAL, ' +
    '  EMPRESA_ID, '+
    '  ULTIMA_CONTAGEM ' +
    'from ' +
    '  INVENTARIO';

  setFiltros(getFiltros);

  AddColuna('INVENTARIO_ID', True);
  AddColuna('NOME');
  AddColuna('DATA_INICIO');
  AddColuna('DATA_FINAL');
  AddColuna('EMPRESA_ID');
  AddColuna('ULTIMA_CONTAGEM');
end;

function TInventario.getRecordInventario: RecInventario;
begin
  Result := RecInventario.Create;

  Result.InventarioId   := getInt('INVENTARIO_ID', True);
  Result.Nome           := getString('NOME');
  Result.DataInicio     := getData('DATA_INICIO');
  Result.DataFinal      := getData('DATA_FINAL');
  Result.EmpresaId      := getInt('EMPRESA_ID');
  Result.UltimaContagem := getInt('ULTIMA_CONTAGEM');
end;

function AtualizarInventario(
  pConexao: TConexao;
  pInventarioId: Integer;
  pEmpresaId: Integer;
  pNome: string;
  pDataInicio: TDateTime;
  pDataFinal: TDateTime;
  pUltimaContagem: Integer;
  pInventarioItens: TArray<RecInventarioItens>
): RecRetornoBD;
var
  i: Integer;
  t: TInventario;
  vNovo: Boolean;
  vSeq: TSequencia;
  vItens: TInventarioItens;
  vExecucao: TExecucao;
begin
  Result.Iniciar;
  pConexao.setRotina('ATUALIZAR_INVENTARIO');

  vExecucao := TExecucao.Create(pConexao);
  t := TInventario.Create(pConexao);
  vItens := TInventarioItens.Create(pConexao);;
  vNovo := pInventarioId = 0;

  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_INVENTARIO_ID');
    pInventarioId := vSeq.getProximaSequencia;
    Result.AsInt := pInventarioId;
    vSeq.Free;
  end;

  try
    pConexao.IniciarTransacao;

    t.setInt('INVENTARIO_ID', pInventarioId, True);
    t.setString('NOME', pNome);
    t.setDataN('DATA_INICIO', pDataInicio);
    t.setDataN('DATA_FINAL', pDataFinal);
    t.setInt('EMPRESA_ID', pEmpresaId);
    t.setInt('ULTIMA_CONTAGEM', pUltimaContagem);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    if pInventarioItens <> nil then
    begin
      for i := Low(pInventarioItens) to High(pInventarioItens) do
      begin
        vItens.setInt('INVENTARIO_ID', pInventarioId, True);
        vItens.setInt('PRODUTO_ID', pInventarioItens[i].ProdutoId, True);
        vItens.setInt('LOCAL_ID', pInventarioItens[i].LocalId, True);
        vItens.setDouble('ESTOQUE', pInventarioItens[i].Estoque);
        vItens.setDouble('QUANTIDADE', pInventarioItens[i].Quantidade);
        vItens.setStringN('CONTOU', pInventarioItens[i].Contou);

        if vNovo then
          vItens.Inserir
        else
          vItens.Atualizar;

      end;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
  vItens.Free;
  vExecucao.Free;
end;

function BuscarInventario(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecInventario>;
var
  i: Integer;
  t: TInventario;
begin
  Result := nil;
  t := TInventario.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordInventario;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirInventario(
  pConexao: TConexao;
  pInventarioId: Integer
): RecRetornoBD;
var
  t: TInventario;
  vExecucao: TExecucao;
begin
  Result.Iniciar;
  t := TInventario.Create(pConexao);
  vExecucao := TExecucao.Create(pConexao);
  try
    pConexao.IniciarTransacao;

    vExecucao.Limpar;
    vExecucao.Add('delete from INVENTARIO_ITENS ');
    vExecucao.Add('where INVENTARIO_ID = :P1');
    vExecucao.Executar([pInventarioId]);

    t.setInt('INVENTARIO_ID', pInventarioId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
  vExecucao.Free;
end;


end.

