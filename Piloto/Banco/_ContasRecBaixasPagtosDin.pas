unit _ContasRecBaixasPagtosDin;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, _RecordsFinanceiros;

{$M+}
type
  TContasRecBaixasPagtosDin = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecContasReceberBaixasPagtosDin: RecTitulosBaixasPagtoDin;
  end;

function BuscarContasRecBaixasPagtoDin(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosBaixasPagtoDin>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TContasRecBaixasPagtosDin }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CRD.BAIXA_ID = :P1 '
    );
end;

constructor TContasRecBaixasPagtosDin.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONTAS_REC_BAIXAS_PAGTOS_DIN');

  FSql :=
    'select ' +
    '  CRD.BAIXA_ID, ' +
    '  CRD.CONTA_ID, ' +
    '  CON.NOME as DESCRICAO, ' +
    '  CRD.VALOR ' +
    'from ' +
    '  CONTAS_REC_BAIXAS_PAGTOS_DIN CRD ' +

    'inner join CONTAS CON ' +
    'on CRD.CONTA_ID = CON.CONTA ';

  setFiltros(getFiltros);

  AddColuna('BAIXA_ID', True);
  AddColuna('CONTA_ID', True);
  AddColuna('VALOR');
  AddColunaSL('DESCRICAO');
end;

function TContasRecBaixasPagtosDin.getRecContasReceberBaixasPagtosDin: RecTitulosBaixasPagtoDin;
begin
  Result.BaixaId   := getInt('BAIXA_ID', True);
  Result.ContaId   := getString('CONTA_ID', True);
  Result.Valor     := getDouble('VALOR');
  Result.Descricao := getString('DESCRICAO');
end;

function BuscarContasRecBaixasPagtoDin(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosBaixasPagtoDin>;
var
  i: Integer;
  t: TContasRecBaixasPagtosDin;
begin
  Result := nil;
  t := TContasRecBaixasPagtosDin.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecContasReceberBaixasPagtosDin;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;


end.
