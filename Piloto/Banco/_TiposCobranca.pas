unit _TiposCobranca;

Interface

uses
  _Conexao, _OperacoesBancoDados, _TiposCobrancaDiasPrazo, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros,
  System.Variants;

{$M+}
type
  TTiposCobranca = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordTiposCobranca: RecTiposCobranca;
  end;

function AtualizarTiposCobranca(
  pConexao: TConexao;
  pTipoCobrancaId: Integer;
  pNome: string;
  pTaxaRetencaoMes: Double;
  pRedeCartao: string;
  pTipoCartao: string;
  pFormaPagamento: string;
  pTipoRecebimentoCartao: string;
  pTipoPagamentoComissao: string;
  pPercentualComissao: Double;
  pUtilizarLimiteCredCliente: string;
  pBoletoBancario: string;
  pAtivo: string;
  pPortadorId: string;
  pIncidenciaFinanceira: string;
  pPermitirPrazoMedio: string;
  pPermitirVinculoAutTEF: string;
  pBandeiraCartaoNFe: string;
  pDiasPrazo: TArray<Integer>;
  pPrazoMedio: Double;
  pAdministradoraCartaoId: Integer;
  pEmitirDuplicMercantilVenda: string
): RecRetornoBD;

function BuscarTiposCobrancas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pNatureza: string
): TArray<RecTiposCobranca>;

function BuscarTiposCobrancasComando(
  pConexao: TConexao;
  pComando: string
): TArray<RecTiposCobranca>;

function ExcluirTiposCobranca(
  pConexao: TConexao;
  pTipoCobrancaId: Integer
): RecRetornoBD;

function TemTipoCobrancaBoletoBancario(pConexao: TConexao; pCobrancasIds: TArray<Integer>): Boolean;
function TemTipoDuplicataMercantil(pConexao: TConexao; pCobrancasIds: TArray<Integer>): Boolean;
function VincularCartaoAutomatico(pConexao: TConexao; pCobrancaId: Integer; pRede: string; pBandeira: string): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TTiposCobranca }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 5);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where TPC.COBRANCA_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome do tipo de cobran�a( avan�ado )',
      True,
      0,
      'where TPC.NOME like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  TPC.NOME '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where TPC.COBRANCA_ID = :P1 ' +
      'and TPC.FORMA_PAGAMENTO = :P2 ',
      'order by ' +
      '  TPC.NOME '
    );

  Result[3] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where TPC.FORMA_PAGAMENTO = ''CRT'' ' +
      'and PRZ.QTDE_PARCELAS = :P1 ' +
      'and (TPC.TIPO_CARTAO = :P2)',
      'order by ' +
      '  TPC.NOME '
    );

  Result[4] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where TPC.FORMA_PAGAMENTO = ''CRT'' ' +
      'and PRZ.QTDE_PARCELAS = :P1 ' +
      'and TPC.REDE_RETORNO_TEF = :P2 ' +
      'and TPC.BADEIRA_RETORNO_TEF = :P3 ' +
      'and TPC.PERMITIR_VINCULO_AUT_TEF = ''S'' ' +
      'and TPC.TIPO_CARTAO = :P4 ',
      'order by ' +
      '  TPC.NOME '
    );
end;

constructor TTiposCobranca.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'TIPOS_COBRANCA');

  FSql :=
    'select ' +
    '  TPC.COBRANCA_ID, ' +
    '  TPC.NOME, ' +
    '  TPC.TAXA_RETENCAO_MES, ' +
    '  TPC.REDE_CARTAO, ' +
    '  TPC.TIPO_CARTAO, ' +
    '  TPC.TIPO_RECEBIMENTO_CARTAO, ' +
    '  TPC.TIPO_PAGAMENTO_COMISSAO, ' +
    '  TPC.PERCENTUAL_COMISSAO, ' +
    '  TPC.ATIVO, ' +
    '  TPC.FORMA_PAGAMENTO, ' +
    '  TPC.UTILIZAR_LIMITE_CRED_CLIENTE, ' +
    '  TPC.BOLETO_BANCARIO, ' +
    '  TPC.PERMITIR_PRAZO_MEDIO, ' +
    '  TPC.PORTADOR_ID, ' +
    '  POR.DESCRICAO as NOME_PORTADOR, ' +
    '  TPC.INCIDENCIA_FINANCEIRA, ' +
    '  TPC.PERMITIR_VINCULO_AUT_TEF, ' +
    '  TPC.REDE_RETORNO_TEF, ' +
    '  TPC.BADEIRA_RETORNO_TEF, ' +
    '  TPC.PRAZO_MEDIO, ' +
    '  TPC.ADMINISTRADORA_CARTAO_ID, ' +
    '  TPC.EMITIR_DUPLIC_MERCANTIL_VENDA, ' +
    '  TPC.BANDEIRA_CARTAO_NFE, ' +
    '  PRZ.QTDE_PARCELAS ' +
    'from ' +
    '  TIPOS_COBRANCA TPC ' +

    'inner join PORTADORES POR ' +
    'on TPC.PORTADOR_ID = POR.PORTADOR_ID ' +

    'left join ( ' +
    '  select ' +
    '    COBRANCA_ID,' +
    '    count(*) as QTDE_PARCELAS ' +
    '  from ' +
    '    TIPO_COBRANCA_DIAS_PRAZO ' +
    '  group by ' +
    '    COBRANCA_ID ' +
    ') PRZ ' +
    'on TPC.COBRANCA_ID = PRZ.COBRANCA_ID ';

  SetFiltros(GetFiltros);

  AddColuna('COBRANCA_ID', True);
  AddColuna('NOME');
  AddColuna('TAXA_RETENCAO_MES');
  AddColuna('REDE_CARTAO');
  AddColuna('TIPO_CARTAO');
  AddColuna('TIPO_RECEBIMENTO_CARTAO');
  AddColuna('TIPO_PAGAMENTO_COMISSAO');
  AddColuna('PERCENTUAL_COMISSAO');
  AddColuna('FORMA_PAGAMENTO');
  AddColuna('UTILIZAR_LIMITE_CRED_CLIENTE');
  AddColuna('BOLETO_BANCARIO');
  AddColuna('PERMITIR_PRAZO_MEDIO');
  AddColuna('ATIVO');
  AddColuna('PORTADOR_ID');
  AddColunaSL('NOME_PORTADOR');
  AddColuna('INCIDENCIA_FINANCEIRA');
  AddColuna('PERMITIR_VINCULO_AUT_TEF');
  AddColunaSL('REDE_RETORNO_TEF');
  AddColunaSL('BADEIRA_RETORNO_TEF');
  AddColuna('PRAZO_MEDIO');
  AddColuna('ADMINISTRADORA_CARTAO_ID');
  AddColuna('BANDEIRA_CARTAO_NFE');
  AddColuna('EMITIR_DUPLIC_MERCANTIL_VENDA');
end;

function TTiposCobranca.GetRecordTiposCobranca: RecTiposCobranca;
begin
  Result := RecTiposCobranca.Create;

  Result.CobrancaId                 := GetInt('COBRANCA_ID', True);
  Result.Nome                       := getString('NOME');
  Result.taxa_retencao_mes          := GetDouble('TAXA_RETENCAO_MES');
  Result.rede_cartao                := GetString('REDE_CARTAO');
  Result.tipo_cartao                := GetString('TIPO_CARTAO');
  Result.forma_pagamento            := getString('FORMA_PAGAMENTO');
  Result.UtilizarLimiteCredCliente  := getString('UTILIZAR_LIMITE_CRED_CLIENTE');
  Result.tipo_recebimento_cartao    := getString('TIPO_RECEBIMENTO_CARTAO');
  Result.TipoPagamentoComissao      := getString('TIPO_PAGAMENTO_COMISSAO');
  Result.PercentualComissao         := getDouble('PERCENTUAL_COMISSAO');
  Result.BoletoBancario             := getString('BOLETO_BANCARIO');
  Result.PermitirPrazoMedio         := getString('PERMITIR_PRAZO_MEDIO');
  Result.ativo                      := GetString('ATIVO');
  Result.PortadorId                 := GetString('PORTADOR_ID');
  Result.NomePortador               := GetString('NOME_PORTADOR');
  Result.IncidenciaFinanceira       := GetString('INCIDENCIA_FINANCEIRA');
  Result.PermitirVinculoAutTEF      := GetString('PERMITIR_VINCULO_AUT_TEF');
  Result.RedeRetornoTEF             := GetString('REDE_RETORNO_TEF');
  Result.BandeiraRetornoTEF         := GetString('BADEIRA_RETORNO_TEF');
  Result.PrazoMedio                 := getDouble('PRAZO_MEDIO');
  Result.AdministradoraCartaoId     := getInt('ADMINISTRADORA_CARTAO_ID');
  Result.BandeiraCartaoNFe          := GetString('BANDEIRA_CARTAO_NFE');
  Result.EmitirDuplicMercantilVenda := GetString('EMITIR_DUPLIC_MERCANTIL_VENDA');
end;

function AtualizarTiposCobranca(
  pConexao: TConexao;
  pTipoCobrancaId: Integer;
  pNome: string;
  pTaxaRetencaoMes: Double;
  pRedeCartao: string;
  pTipoCartao: string;
  pFormaPagamento: string;
  pTipoRecebimentoCartao: string;
  pTipoPagamentoComissao: string;
  pPercentualComissao: Double;
  pUtilizarLimiteCredCliente: string;
  pBoletoBancario: string;
  pAtivo: string;
  pPortadorId: string;
  pIncidenciaFinanceira: string;
  pPermitirPrazoMedio: string;
  pPermitirVinculoAutTEF: string;
  pBandeiraCartaoNFe: string;
  pDiasPrazo: TArray<Integer>;
  pPrazoMedio: Double;
  pAdministradoraCartaoId: Integer;
  pEmitirDuplicMercantilVenda: string
): RecRetornoBD;
var
  vPrazos: TTipoCobrancaDiasPrazo;
  t: TTiposCobranca;
  vSeq: TSequencia;
  vExec: TExecucao;
  vSql: TConsulta;
  vNovo: Boolean;
  i: SmallInt;
begin
  vPrazos := TTipoCobrancaDiasPrazo.Create(pConexao);
  t := TTiposCobranca.Create(pConexao);
  vExec := TExecucao.Create(pConexao);
  Result.TeveErro := False;

  if pFormaPagamento = 'DIN' then begin
    vSql := TConsulta.Create(pConexao);
    vSql.SQL.Add('select count(*) ');
    vSql.SQL.Add('from TIPOS_COBRANCA ');
    vSql.SQL.Add('where FORMA_PAGAMENTO = ''DIN'' ');
    vSql.SQL.Add('and ATIVO = ''S'' ');
    vSql.SQL.Add('and COBRANCA_ID <> :P1 ');
    vSql.Pesquisar([pTipoCobrancaId]);

    if vSql.GetInt(0) > 0 then begin
      Result.TeveErro := True;
      Result.MensagemErro := 'S� pode existir um tipo de cobran�a ativo com a forma de pagamento "Dinheiro"!';
      vSql.Active := False;
      vSql.Free;
      Exit;
    end;
    vSql.Free;
  end;

  vNovo := pTipoCobrancaId = 0;
  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_COBRANCA_ID');
    pTipoCobrancaId := vSeq.GetProximaSequencia;
    result.AsInt := pTipoCobrancaId;
    vSeq.Free;
  end;

  t.setInt('COBRANCA_ID', pTipoCobrancaId, True);
  t.SetString('NOME', pNome);
  t.setDouble('TAXA_RETENCAO_MES', pTaxaRetencaoMes);
  t.SetString('REDE_CARTAO', pRedeCartao);
  t.SetString('TIPO_CARTAO', pTipoCartao);
  t.setString('FORMA_PAGAMENTO', pFormaPagamento);
  t.setStringN('TIPO_RECEBIMENTO_CARTAO', pTipoRecebimentoCartao);
  t.setString('TIPO_PAGAMENTO_COMISSAO', pTipoPagamentoComissao);
  t.setDouble('PERCENTUAL_COMISSAO', pPercentualComissao);
  t.setString('UTILIZAR_LIMITE_CRED_CLIENTE', pUtilizarLimiteCredCliente);
  t.setString('BOLETO_BANCARIO', pBoletoBancario);
  t.SetString('ATIVO', pAtivo);
  t.SetString('PORTADOR_ID', pPortadorId);
  t.SetString('INCIDENCIA_FINANCEIRA', pIncidenciaFinanceira);
  t.SetString('PERMITIR_PRAZO_MEDIO', pPermitirPrazoMedio);
  t.SetString('PERMITIR_VINCULO_AUT_TEF', pPermitirVinculoAutTEF);
  t.SetString('BANDEIRA_CARTAO_NFE', pBandeiraCartaoNFe);
  t.setDouble('PRAZO_MEDIO', pPrazoMedio);
  t.setIntN('ADMINISTRADORA_CARTAO_ID', pAdministradoraCartaoId);
  t.SetString('EMITIR_DUPLIC_MERCANTIL_VENDA', pEmitirDuplicMercantilVenda);

  try
    pConexao.IniciarTransacao;

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    vExec.Clear;
    vExec.Add('delete from TIPO_COBRANCA_DIAS_PRAZO where COBRANCA_ID = :P1');
    vExec.Executar([pTipoCobrancaId]);

    vPrazos.setInt('COBRANCA_ID', pTipoCobrancaId, True);
    for i := Low(pDiasPrazo) to High(pDiasPrazo) do begin
      vPrazos.setInt('DIAS', pDiasPrazo[i], True);

      vPrazos.Inserir;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vPrazos.Free;
  vExec.Free;
  t.Free;
end;

function BuscarTiposCobrancas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pNatureza: string
): TArray<RecTiposCobranca>;
var
  i: Integer;
  t: TTiposCobranca;
  vSql: string;
begin
  Result := nil;
  t := TTiposCobranca.Create(pConexao);

  vSql := '';
  if pNatureza <> '' then
    vSql := ' and INCIDENCIA_FINANCEIRA in(''A'', ''' + pNatureza + ''') ';

  if t.Pesquisar(pIndice, pFiltros, vSql) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordTiposCobranca;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarTiposCobrancasComando(
  pConexao: TConexao;
  pComando: string
): TArray<RecTiposCobranca>;
var
  i: Integer;
  t: TTiposCobranca;
begin
  Result := nil;
  t := TTiposCobranca.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordTiposCobranca;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirTiposCobranca(
  pConexao: TConexao;
  pTipoCobrancaId: Integer
): RecRetornoBD;
var
  t: TTiposCobranca;
begin
  Result.TeveErro := False;
  t := TTiposCobranca.Create(pConexao);

  t.SetInt('COBRANCA_ID', pTipoCobrancaId, True);

  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function TemTipoCobrancaBoletoBancario(pConexao: TConexao; pCobrancasIds: TArray<Integer>): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select count(*) ');
  vSql.Add('from TIPOS_COBRANCA ');
  vSql.Add('where ' + FiltroInInt('COBRANCA_ID', pCobrancasIds));
  vSql.Add('and BOLETO_BANCARIO = ''S'' ');

  vSql.Pesquisar;

  Result := (vSql.GetInt(0) > 0);

  vSql.Active := False;
  vSql.Free;
end;

function TemTipoDuplicataMercantil(pConexao: TConexao; pCobrancasIds: TArray<Integer>): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select count(*) ');
  vSql.Add('from TIPOS_COBRANCA ');
  vSql.Add('where ' + FiltroInInt('COBRANCA_ID', pCobrancasIds));
  vSql.Add('and EMITIR_DUPLIC_MERCANTIL_VENDA = ''S'' ');

  vSql.Pesquisar;

  Result := (vSql.GetInt(0) > 0);

  vSql.Active := False;
  vSql.Free;
end;

function VincularCartaoAutomatico(pConexao: TConexao; pCobrancaId: Integer; pRede: string; pBandeira: string): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('VINCULAR_TIPOS_COBR_TEF');

  vExec := TExecucao.Create(pConexao);

  vExec.Add('update TIPOS_COBRANCA set ');
  vExec.Add('  REDE_RETORNO_TEF = :P2, ');
  vExec.Add('  BADEIRA_RETORNO_TEF = :P3 ');
  vExec.Add('where COBRANCA_ID = :P1 ');
  vExec.Add('and PERMITIR_VINCULO_AUT_TEF = ''S'' ');

  try
    pConexao.IniciarTransacao;

    vExec.Executar([pCobrancaId, pRede, pBandeira]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
end;

end.
