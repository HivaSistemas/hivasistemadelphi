unit _AcumuladosItens;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecAcumuladosItens = record
    AcumuladoId: Integer;
    ProdutoId: Integer;
    NomeProduto: string;
    PrecoUnitario: Double;
    Quantidade: Double;
    ValorTotal: Double;
    ValorTotalDesconto: Double;
    ValorTotalOutrasDespesas: Double;
    ValorTotalFrete: Double;
    MarcaId: Integer;
    NomeMarca: string;
    UnidadeVenda: string;
    PrecoFinal: Double;
    PrecoFinalMedio: Double;
    PrecoLiquido: Double;
    PrecoLiquidoMedio: Double;
    Cmv: Double;
    CustoUltimoPedido: Double;
    CustoPedidoMedio: Double;
    ValorImpostos: Double;
    ValorEncargos: Double;
    ValorCustoVenda: Double;
    ValorCustoFinal: Double;
    ValorCustoEntrada: Double;
  end;

  TAcumuladosItens = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordAcumuladosItens: RecAcumuladosItens;
  end;

function BuscarAcumuladosItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecAcumuladosItens>;

function BuscarAcumuladosItensComando(pConexao: TConexao; pComando: string): TArray<RecAcumuladosItens>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TAcumuladosItens }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ITE.ACUMULADO_ID = :P1 '
    );
end;

constructor TAcumuladosItens.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ACUMULADOS_ITENS');

  FSql := 
    'select ' +
    '  ITE.ACUMULADO_ID, ' +
    '  ITE.PRODUTO_ID, ' +
    '  PRO.NOME as NOME_PRODUTO, ' +
    '  ITE.PRECO_UNITARIO, ' +
    '  ITE.QUANTIDADE, ' +
    '  ITE.VALOR_TOTAL, ' +
    '  ITE.VALOR_TOTAL_DESCONTO, ' +
    '  ITE.VALOR_TOTAL_OUTRAS_DESPESAS, ' +
    '  ITE.VALOR_TOTAL_FRETE, ' +
    '  PRO.MARCA_ID, ' +
    '  MAR.NOME as NOME_MARCA, ' +
    '  PRO.UNIDADE_VENDA, ' +

    '  ITE.PRECO_FINAL, ' +
    '  ITE.PRECO_FINAL_MEDIO, ' +
    '  ITE.PRECO_LIQUIDO, ' +
    '  ITE.PRECO_LIQUIDO_MEDIO, ' +
    '  ITE.CMV, ' +
    '  ITE.CUSTO_ULTIMO_PEDIDO, ' +
    '  ITE.CUSTO_PEDIDO_MEDIO, ' +
    '  ITE.VALOR_IMPOSTOS, ' +
    '  ITE.VALOR_ENCARGOS, ' +
    '  INN.VALOR_CUSTO_VENDA, ' +
    '  INN.VALOR_CUSTO_FINAL, ' +
    '  INN.VALOR_CUSTO_ENTRADA ' +
    'from ' +
    '  ACUMULADOS_ITENS ITE ' +

    'inner join PRODUTOS PRO ' +
    'on ITE.PRODUTO_ID = PRO.PRODUTO_ID ' +

    'inner join ( ' +
    'select ' +
    '  ORC.ACUMULADO_ID, ' +
    '  OIT.PRODUTO_ID, ' +
    '  SUM(OIT.VALOR_CUSTO_VENDA) AS VALOR_CUSTO_VENDA, ' +
    '  SUM(OIT.VALOR_CUSTO_FINAL) AS VALOR_CUSTO_FINAL, ' +
    '  SUM(case PAE.TIPO_CUSTO_VIS_LUCRO_VENDA ' +
    '    when ''FIN'' then zvl(OIT.PRECO_LIQUIDO, OIT.CUSTO_ULTIMO_PEDIDO) ' +
    '    when ''COM'' then zvl(OIT.CUSTO_ULTIMO_PEDIDO, OIT.PRECO_LIQUIDO) ' +
    '    else OIT.CMV ' +
    '  end) as VALOR_CUSTO_ENTRADA ' +
    'from ORCAMENTOS_ITENS OIT ' +

    'inner join ORCAMENTOS ORC ' +
    'on ORC.ORCAMENTO_ID = OIT.ORCAMENTO_ID ' +

    'inner join PARAMETROS_EMPRESA PAE ' +
    'on ORC.EMPRESA_ID = PAE.EMPRESA_ID ' +

    'group by ' +
    '  OIT.PRODUTO_ID, ' +
    '  ORC.ACUMULADO_ID ' +

    ') INN ' +
    'ON INN.PRODUTO_ID = ITE.PRODUTO_ID ' +
    'AND INN.ACUMULADO_ID = ITE.ACUMULADO_ID ' +

    'inner join MARCAS MAR ' +
    'on PRO.MARCA_ID = MAR.MARCA_ID ';

  setFiltros(getFiltros);

  AddColuna('ACUMULADO_ID', True);
  AddColuna('PRODUTO_ID', True);
  AddColunaSL('NOME_PRODUTO');
  AddColuna('PRECO_UNITARIO');
  AddColuna('QUANTIDADE');
  AddColuna('VALOR_TOTAL');
  AddColuna('VALOR_TOTAL_DESCONTO');
  AddColuna('VALOR_TOTAL_OUTRAS_DESPESAS');
  AddColuna('VALOR_TOTAL_FRETE');
  AddColunaSL('MARCA_ID');
  AddColunaSL('NOME_MARCA');
  AddColunaSL('UNIDADE_VENDA');
  AddColunaSL('PRECO_FINAL');
  AddColunaSL('PRECO_FINAL_MEDIO');
  AddColunaSL('PRECO_LIQUIDO');
  AddColunaSL('PRECO_LIQUIDO_MEDIO');
  AddColunaSL('CMV');
  AddColunaSL('CUSTO_ULTIMO_PEDIDO');
  AddColunaSL('CUSTO_PEDIDO_MEDIO');
  AddColuna('VALOR_IMPOSTOS');
  AddColuna('VALOR_ENCARGOS');
  AddColuna('VALOR_CUSTO_VENDA');
  AddColuna('VALOR_CUSTO_FINAL');
  AddColunaSL('VALOR_CUSTO_ENTRADA');
end;

function TAcumuladosItens.getRecordAcumuladosItens: RecAcumuladosItens;
begin
  Result.AcumuladoId              := getInt('ACUMULADO_ID', True);
  Result.ProdutoId                := getInt('PRODUTO_ID', True);
  Result.NomeProduto              := getString('NOME_PRODUTO');
  Result.PrecoUnitario            := getDouble('PRECO_UNITARIO');
  Result.Quantidade               := getDouble('QUANTIDADE');
  Result.ValorTotal               := getDouble('VALOR_TOTAL');
  Result.ValorTotalDesconto       := getDouble('VALOR_TOTAL_DESCONTO');
  Result.ValorTotalOutrasDespesas := getDouble('VALOR_TOTAL_OUTRAS_DESPESAS');
  Result.ValorTotalFrete          := getDouble('VALOR_TOTAL_FRETE');
  Result.MarcaId                  := getInt('MARCA_ID');
  Result.NomeMarca                := getString('NOME_MARCA');
  Result.UnidadeVenda             := getString('UNIDADE_VENDA');
  Result.PrecoFinal               := getDouble('PRECO_FINAL');
  Result.PrecoFinalMedio          := getDouble('PRECO_FINAL_MEDIO');
  Result.PrecoLiquido             := getDouble('PRECO_LIQUIDO');
  Result.PrecoLiquidoMedio        := getDouble('PRECO_LIQUIDO_MEDIO');
  Result.Cmv                      := getDouble('CMV');
  Result.CustoUltimoPedido        := getDouble('CUSTO_ULTIMO_PEDIDO');
  Result.CustoPedidoMedio         := getDouble('CUSTO_PEDIDO_MEDIO');
  Result.ValorImpostos            := getDouble('VALOR_IMPOSTOS');
  Result.ValorEncargos            := getDouble('VALOR_ENCARGOS');
  Result.ValorCustoVenda          := getDouble('VALOR_CUSTO_VENDA');
  Result.ValorCustoFinal          := getDouble('VALOR_CUSTO_FINAL');
  Result.ValorCustoEntrada        := getDouble('VALOR_CUSTO_ENTRADA');
end;

function BuscarAcumuladosItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecAcumuladosItens>;
var
  i: Integer;
  t: TAcumuladosItens;
begin
  Result := nil;
  t := TAcumuladosItens.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordAcumuladosItens;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarAcumuladosItensComando(pConexao: TConexao; pComando: string): TArray<RecAcumuladosItens>;
var
  i: Integer;
  t: TAcumuladosItens;
begin
  Result := nil;
  t := TAcumuladosItens.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordAcumuladosItens;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
