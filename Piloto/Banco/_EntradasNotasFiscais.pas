unit _EntradasNotasFiscais;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, _RecordsEstoques, _EntradasNotasFiscaisItens, _EntradasNotasFiscFinanceiro,
  System.Classes, _RecordsFinanceiros, Vcl.Graphics, _EntradasNFItensCompras, _EntradasNfItensLotes;

{$M+}
type
  TEntradaNotaFiscal = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordEntradaNotaFiscal: RecEntradaNotaFiscal;
  end;

function AtualizarEntradaNotaFiscal(
  pConexao: TConexao;
  pEntradaId: Integer;
  pEmpresaId: Integer;
  pFornecedorId: Integer;
  pStatusEntrada: string;
  pCfopId: string;
  pPlanoFinanceiroId: string;
  pCentroCustoId: Integer;
  pConhecimentoFreteId: Integer;
  pValorConhecimentoFrete: Double;
  pBaseCalculoIcms: Double;
  pNumeroNota: Integer;
  pPesoBruto: Double;
  pPesoLiquido: Double;
  pBaseCalculoCofins: Double;
  pValorCofins: Double;
  pValorIpi: Double;
  pBaseCalculoPis: Double;
  pValorPis: Double;
  pBaseCalculoIcmsSt: Double;
  pValorIcmsSt: Double;
  pValorIcms: Double;
  pValorTotal: Double;
  pValorTotalProdutos: Double;
  pValorDesconto: Double;
  pValorOutrasDespesas: Double;
  pValorOutrosCustosFinanceiros: Currency;
  pValorFrete: Double;
  pTipoRateioFrete: string;
  pModalidadeFrete: Integer;
  pSerieNota: string;
  pModeloNota: string;
  pDataHoraEmissao: TDateTime;
  pDataEntrada: TDateTime;
  pDataPrimeiraParcela: TDateTime;
  pChaveNfe: string;
  pAtualizarCustoCompra: string;
  pAtualizarCustoEntrada: string;
  pCaminhoArquivo: string;
  pXmlTexto: string;
  pEntradaItens: TArray<RecEntradaNotaFiscalItem>;
  pEntradaFinanceiro: TArray<RecTitulosFinanceiros>;
  pOutrosCustos: TArray<RecTitulosFinanceiros>;
  pItensCompras: TArray<RecEntradasNFItensCompras>;
  pLotes: TArray<RecEntradasNfItensLotes>
): RecRetornoBD;

function BuscarEntradaNotaFiscais(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEntradaNotaFiscal>;

function BuscarEntradaNotaFiscaisComando(
  pConexao: TConexao;
  pComando: string
): TArray<RecEntradaNotaFiscal>;

function ExcluirEntradaNotaFiscal(
  pConexao: TConexao;
  pEntradaId: Integer
): RecRetornoBD;

function ExisteEntradaNFe(pConexao: TConexao; pChaveNFe: string): Boolean; overload;
function ExisteEntradaNFe(pConexao: TConexao; pFornecedorId: Integer; pNumeroNota: Integer; pModeloNota: string; pSerieNota: string): Boolean; overload;
function ExisteEntradaConhecimentoVinculado(pConexao: TConexao; pConhecimentoId: Integer): Boolean;
function DesconsolidarEstoque(pConexao: TConexao; pEntradaId: Integer): RecRetornoBD;
function getCorStatus(pStatus: string): Integer;

function getFiltros: TArray<RecFiltros>;

implementation

{ TEntradaNotaFiscal }

uses _Sessao;

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ENTRADA_ID = :P1'
    );
end;

constructor TEntradaNotaFiscal.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ENTRADAS_NOTAS_FISCAIS');

  FSql :=
    'select ' +
    '  ENT.ENTRADA_ID, ' +
    '  ENT.EMPRESA_ID, ' +
    '  ENT.FORNECEDOR_ID, ' +
    '  ENT.STATUS, ' +
    '  ENT.ORIGEM_ENTRADA, ' +
    '  ENT.NOTA_TRANSF_PROD_ORIGEM_ID, ' +
    '  CAD.NOME_FANTASIA, ' +
    '  CAD.RAZAO_SOCIAL, ' +
    '  ENT.CFOP_ID, ' +
    '  ENT.PLANO_FINANCEIRO_ID, ' +
    '  ENT.CENTRO_CUSTO_ID, ' +
    '  ENT.CONHECIMENTO_FRETE_ID, ' +
    '  ENT.BASE_CALCULO_ICMS, ' +
    '  ENT.NUMERO_NOTA, ' +
    '  ENT.PESO_BRUTO, ' +
    '  ENT.PESO_LIQUIDO, ' +
    '  ENT.BASE_CALCULO_COFINS, ' +
    '  ENT.VALOR_COFINS, ' +
    '  ENT.VALOR_IPI, ' +
    '  ENT.BASE_CALCULO_PIS, ' +
    '  ENT.VALOR_PIS, ' +
    '  ENT.BASE_CALCULO_ICMS_ST, ' +
    '  ENT.VALOR_ICMS_ST, ' +
    '  ENT.VALOR_ICMS, ' +
    '  ENT.VALOR_TOTAL, ' +
    '  ENT.VALOR_TOTAL_PRODUTOS, ' +
    '  ENT.VALOR_DESCONTO, ' +
    '  ENT.VALOR_OUTRAS_DESPESAS, ' +
    '  ENT.VALOR_OUTROS_CUSTOS_FINANC, ' +
    '  ENT.VALOR_FRETE, ' +
    '  ENT.TIPO_RATEIO_FRETE, ' +
    '  ENT.MODALIDADE_FRETE, ' +
    '  ENT.SERIE_NOTA, ' +
    '  ENT.MODELO_NOTA, ' +
    '  ENT.DATA_HORA_CADASTRO, ' +
    '  ENT.DATA_HORA_EMISSAO, ' +
    '  ENT.DATA_ENTRADA, ' +
    '  ENT.DATA_PRIMEIRA_PARCELA, ' +
    '  ENT.VALOR_CONHECIMENTO_FRETE, ' +
    '  ENT.CHAVE_NFE, ' +
    '  ENT.ATUALIZAR_CUSTO_COMPRA, ' +
    '  ENT.ATUALIZAR_CUSTO_ENTRADA, ' +
    '  ENT.USUARIO_CADASTRO_ID, ' +
    '  FUN.NOME as NOME_USUARIO_CADASTRO, ' +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA ' +
    'from ' +
    '  ENTRADAS_NOTAS_FISCAIS ENT ' +

    'inner join FORNECEDORES FRN ' +
    'on ENT.FORNECEDOR_ID = FRN.CADASTRO_ID ' +

    'inner join CADASTROS CAD ' +
    'on FRN.CADASTRO_ID = CAD.CADASTRO_ID ' +

    'inner join FUNCIONARIOS FUN ' +
    'on ENT.USUARIO_CADASTRO_ID = FUN.FUNCIONARIO_ID ' +

    'inner join EMPRESAS EMP ' +
    'on ENT.EMPRESA_ID = EMP.EMPRESA_ID ';

  setFiltros(getFiltros);

  AddColuna('ENTRADA_ID', True);
  AddColuna('EMPRESA_ID');
  AddColuna('FORNECEDOR_ID');
  AddColuna('STATUS');
  AddColunaSL('ORIGEM_ENTRADA');
  AddColunaSL('NOTA_TRANSF_PROD_ORIGEM_ID');
  AddColunaSL('NOME_FANTASIA');
  AddColunaSL('RAZAO_SOCIAL');
  AddColuna('CFOP_ID');
  AddColuna('PLANO_FINANCEIRO_ID');
  AddColuna('CENTRO_CUSTO_ID');
  AddColuna('CONHECIMENTO_FRETE_ID');
  AddColuna('BASE_CALCULO_ICMS');
  AddColuna('NUMERO_NOTA');
  AddColuna('VALOR_CONHECIMENTO_FRETE');
  AddColuna('PESO_BRUTO');
  AddColuna('PESO_LIQUIDO');
  AddColuna('BASE_CALCULO_COFINS');
  AddColuna('VALOR_COFINS');
  AddColuna('VALOR_IPI');
  AddColuna('BASE_CALCULO_PIS');
  AddColuna('VALOR_PIS');
  AddColuna('BASE_CALCULO_ICMS_ST');
  AddColuna('VALOR_ICMS_ST');
  AddColuna('VALOR_ICMS');
  AddColuna('VALOR_TOTAL');
  AddColuna('VALOR_TOTAL_PRODUTOS');
  AddColuna('VALOR_DESCONTO');
  AddColuna('VALOR_OUTRAS_DESPESAS');
  AddColuna('VALOR_OUTROS_CUSTOS_FINANC');
  AddColuna('VALOR_FRETE');
  AddColuna('TIPO_RATEIO_FRETE');
  AddColuna('MODALIDADE_FRETE');
  AddColuna('SERIE_NOTA');
  AddColuna('MODELO_NOTA');
  AddColunaSL('DATA_HORA_CADASTRO');
  AddColuna('DATA_HORA_EMISSAO');
  AddColuna('DATA_ENTRADA');
  AddColuna('DATA_PRIMEIRA_PARCELA');
  AddColuna('CHAVE_NFE');
  AddColuna('ATUALIZAR_CUSTO_COMPRA');
  AddColuna('ATUALIZAR_CUSTO_ENTRADA');
  AddColunaSL('USUARIO_CADASTRO_ID');
  AddColunaSL('NOME_USUARIO_CADASTRO');
  AddColunaSL('NOME_EMPRESA');
end;

function TEntradaNotaFiscal.getRecordEntradaNotaFiscal: RecEntradaNotaFiscal;
begin
  Result.entrada_id              := getInt('ENTRADA_ID', True);
  Result.empresa_id              := getInt('EMPRESA_ID');
  Result.fornecedor_id           := getInt('FORNECEDOR_ID');
  Result.Status                  := getString('STATUS');
  Result.OrigemEntrada           := getString('ORIGEM_ENTRADA');
  Result.NotaTransfProdOrigemId  := getInt('NOTA_TRANSF_PROD_ORIGEM_ID');
  Result.Razao_Social            := getString('RAZAO_SOCIAL');
  Result.Nome_Fantasia           := getString('NOME_FANTASIA');
  Result.CfopId                  := getString('CFOP_ID');
  Result.PlanoFinanceiroId       := getString('PLANO_FINANCEIRO_ID');
  Result.CentroCustoId           := getInt('CENTRO_CUSTO_ID');
  Result.ConhecimentoFreteId     := getInt('CONHECIMENTO_FRETE_ID');
  Result.base_calculo_icms       := getDouble('BASE_CALCULO_ICMS');
  Result.numero_nota             := getInt('NUMERO_NOTA');
  Result.peso_bruto              := getDouble('PESO_BRUTO');
  Result.peso_liquido            := getDouble('PESO_LIQUIDO');
  Result.base_calculo_cofins     := getDouble('BASE_CALCULO_COFINS');
  Result.valor_cofins            := getDouble('VALOR_COFINS');
  Result.valor_ipi               := getDouble('VALOR_IPI');
  Result.ValorConhecimentoFrete  := getDouble('VALOR_CONHECIMENTO_FRETE');
  Result.base_calculo_pis        := getDouble('BASE_CALCULO_PIS');
  Result.valor_pis               := getDouble('VALOR_PIS');
  Result.base_calculo_icms_st    := getDouble('BASE_CALCULO_ICMS_ST');
  Result.valor_icms_st           := getDouble('VALOR_ICMS_ST');
  Result.valor_icms              := getDouble('VALOR_ICMS');
  Result.valor_total             := getDouble('VALOR_TOTAL');
  Result.valor_total_produtos    := getDouble('VALOR_TOTAL_PRODUTOS');
  Result.valor_desconto          := getDouble('VALOR_DESCONTO');
  Result.valor_outras_despesas   := getDouble('VALOR_OUTRAS_DESPESAS');
  Result.ValorOutrosCustosFinanc := getDouble('VALOR_OUTROS_CUSTOS_FINANC');
  Result.valor_frete             := getDouble('VALOR_FRETE');
  Result.TipoRateioFrete         := getString('TIPO_RATEIO_FRETE');
  Result.ModalidadeFrete         := getInt('MODALIDADE_FRETE');
  Result.serie_nota              := getString('SERIE_NOTA');
  Result.modelo_nota             := getString('MODELO_NOTA');
  Result.data_hora_cadastro      := getData('DATA_HORA_CADASTRO');
  Result.data_hora_emissao       := getData('DATA_HORA_EMISSAO');
  Result.DataEntrada             := getData('DATA_ENTRADA');
  Result.DataPrimeiraParcela     := getData('DATA_PRIMEIRA_PARCELA');
  Result.chave_nfe               := getString('CHAVE_NFE');
  Result.atualizar_custo_compra  := getString('ATUALIZAR_CUSTO_COMPRA');
  Result.atualizar_custo_entrada := getString('ATUALIZAR_CUSTO_ENTRADA');
  Result.UsuarioCadastroId       := getInt('USUARIO_CADASTRO_ID');
  Result.NomeUsuarioCadastro     := getString('NOME_USUARIO_CADASTRO');
  Result.NomeEmpresa             := getString('NOME_EMPRESA');

  Result.StatusAnalitico :=
   _BibliotecaGenerica.Decode(Result.Status, [
      'ACM', 'Aberto',
      'ACE', 'Ag.consol. estoque',
      'ECO', 'Estoque consolidado',
      'FCO', 'Financeiro consol.',
      'BAI', 'Finalizado'
   ]);

  Result.TipoRateioFreteAnalitico :=
   _BibliotecaGenerica.Decode(Result.TipoRateioFrete, [
      'Q', 'Quantidade',
      'V', 'Valor',
      'P', 'Peso',
      'Manual'
   ]);
end;

function AtualizarEntradaNotaFiscal(
  pConexao: TConexao;
  pEntradaId: Integer;
  pEmpresaId: Integer;
  pFornecedorId: Integer;
  pStatusEntrada: string;
  pCfopId: string;
  pPlanoFinanceiroId: string;
  pCentroCustoId: Integer;
  pConhecimentoFreteId: Integer;
  pValorConhecimentoFrete: Double;
  pBaseCalculoIcms: Double;
  pNumeroNota: Integer;
  pPesoBruto: Double;
  pPesoLiquido: Double;
  pBaseCalculoCofins: Double;
  pValorCofins: Double;
  pValorIpi: Double;
  pBaseCalculoPis: Double;
  pValorPis: Double;
  pBaseCalculoIcmsSt: Double;
  pValorIcmsSt: Double;
  pValorIcms: Double;
  pValorTotal: Double;
  pValorTotalProdutos: Double;
  pValorDesconto: Double;
  pValorOutrasDespesas: Double;
  pValorOutrosCustosFinanceiros: Currency;
  pValorFrete: Double;
  pTipoRateioFrete: string;
  pModalidadeFrete: Integer;
  pSerieNota: string;
  pModeloNota: string;
  pDataHoraEmissao: TDateTime;
  pDataEntrada: TDateTime;
  pDataPrimeiraParcela: TDateTime;
  pChaveNfe: string;
  pAtualizarCustoCompra: string;
  pAtualizarCustoEntrada: string;
  pCaminhoArquivo: string;
  pXmlTexto: string;
  pEntradaItens: TArray<RecEntradaNotaFiscalItem>;
  pEntradaFinanceiro: TArray<RecTitulosFinanceiros>;
  pOutrosCustos: TArray<RecTitulosFinanceiros>;
  pItensCompras: TArray<RecEntradasNFItensCompras>;
  pLotes: TArray<RecEntradasNfItensLotes>
): RecRetornoBD;
var
  t: TEntradaNotaFiscal;
  vItem: TEntradaNotaFiscalItem;
  vFinanc: TEntradaNotaFiscalFinanceiro;
  vItensCompra: TEntradasNFItensCompras;
  vLote: TEntradasNfItensLotes;

  vProc: TProcedimentoBanco;
  vProcFin: TProcedimentoBanco;
  vProcCustos: TProcedimentobanco;

  i: Integer;
  j: Integer;
  vNovo: Boolean;
  vExec: TExecucao;
  vArquivo: TMemoryStream;
begin
  Result.Iniciar;

  pConexao.SetRotina('ATUALIZAR_ENTRADA_NF');

  vExec := TExecucao.Create(pConexao);
  t := TEntradaNotaFiscal.Create(pConexao);
  vItem := TEntradaNotaFiscalItem.Create(pConexao);
  vLote := TEntradasNfItensLotes.Create(pConexao);
  vFinanc := TEntradaNotaFiscalFinanceiro.Create(pConexao);
  vItensCompra := TEntradasNFItensCompras.Create(pConexao);

  vProc := TProcedimentoBanco.Create(pConexao, 'CONS_ESTOQUE_ENTR_NOTA_FISCAL');
  vProcFin := TProcedimentoBanco.Create(pConexao, 'CONS_FINANC_ENTR_NOTA_FISCAL');
  vProcCustos := TProcedimentoBanco.Create(pConexao, 'ATUALIZAR_CUSTOS_ENTRADA');

  vNovo := pEntradaId = 0;
  if vNovo then begin
    pEntradaId := TSequencia.Create(pConexao, 'SEQ_ENTRADA_NOTAS_FISC_ENT_ID').getProximaSequencia;
    result.AsInt := pEntradaId;
  end;

  try
    pConexao.IniciarTransacao;

    t.setInt('ENTRADA_ID', pEntradaId, True);
    t.setInt('EMPRESA_ID', pEmpresaId);
    t.setInt('FORNECEDOR_ID', pFornecedorId);
    t.setString('STATUS', pStatusEntrada);
    t.setString('CFOP_ID', pCfopId);
    t.setDouble('BASE_CALCULO_ICMS', pBaseCalculoIcms);
    t.setInt('NUMERO_NOTA', pNumeroNota);
    t.setDouble('PESO_BRUTO', pPesoBruto);
    t.setDouble('PESO_LIQUIDO', pPesoLiquido);
    t.setDouble('BASE_CALCULO_COFINS', pBaseCalculoCofins);
    t.setDouble('VALOR_COFINS', pValorCofins);
    t.setDouble('VALOR_IPI', pValorIpi);
    t.setDouble('BASE_CALCULO_PIS', pBaseCalculoPis);
    t.setDouble('VALOR_PIS', pValorPis);
    t.setDouble('BASE_CALCULO_ICMS_ST', pBaseCalculoIcmsSt);
    t.setDouble('VALOR_ICMS_ST', pValorIcmsSt);
    t.setDouble('VALOR_ICMS', pValorIcms);
    t.setDouble('VALOR_TOTAL', pValorTotal);
    t.setDouble('VALOR_TOTAL_PRODUTOS', pValorTotalProdutos);
    t.setDouble('VALOR_DESCONTO', pValorDesconto);
    t.setDouble('VALOR_OUTRAS_DESPESAS', pValorOutrasDespesas);
    t.setDouble('VALOR_OUTROS_CUSTOS_FINANC', pValorOutrosCustosFinanceiros);
    t.setDouble('VALOR_FRETE', pValorFrete);
    t.setString('TIPO_RATEIO_FRETE', pTipoRateioFrete);
    t.setInt('MODALIDADE_FRETE', pModalidadeFrete);
    t.setString('SERIE_NOTA', pSerieNota);
    t.setString('MODELO_NOTA', pModeloNota);
    t.setData('DATA_HORA_EMISSAO', pDataHoraEmissao);
    t.setData('DATA_ENTRADA', pDataEntrada);
    t.setData('DATA_PRIMEIRA_PARCELA', pDataPrimeiraParcela);
    t.setStringN('CHAVE_NFE', pChaveNfe);
    t.setString('ATUALIZAR_CUSTO_COMPRA', pAtualizarCustoCompra);
    t.setString('ATUALIZAR_CUSTO_ENTRADA', pAtualizarCustoEntrada);
    t.setString('PLANO_FINANCEIRO_ID', pPlanoFinanceiroId);
    t.setIntN('CENTRO_CUSTO_ID', pCentroCustoId);
    t.setIntN('CONHECIMENTO_FRETE_ID', pConhecimentoFreteId);
    t.setDouble('VALOR_CONHECIMENTO_FRETE', pValorConhecimentoFrete);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    if not vNovo then begin
      vExec.Limpar;
      vExec.Add('delete from ENTRADAS_NF_ITENS_COMPRAS ');
      vExec.Add('where ENTRADA_ID = :P1 ');
      vExec.Executar([pEntradaId]);

      vExec.Limpar;
      vExec.Add('delete from ENTRADAS_NF_ITENS_LOTES ');
      vExec.Add('where ENTRADA_ID = :P1');
      vExec.Executar([pEntradaId]);

      vExec.Limpar;
      vExec.Add('delete from ENTRADAS_NOTAS_FISCAIS_ITENS ');
      vExec.Add('where ENTRADA_ID = :P1');
      vExec.Executar([pEntradaId]);
    end;

    for i := Low(pEntradaItens) to High(pEntradaItens) do begin
      vItem.setInt('ENTRADA_ID', pEntradaId, True);
      vItem.setInt('PRODUTO_ID', pEntradaItens[i].produto_id, True);
      vItem.setInt('ITEM_ID', pEntradaItens[i].item_id, True);
      vItem.setInt('LOCAL_ENTRADA_ID', pEntradaItens[i].LocalEntradaId);
      vItem.setString('CFOP_ID', pEntradaItens[i].CfopId);
      vItem.setString('CST', pEntradaItens[i].cst);
      vItem.setStringN('CODIGO_NCM', pEntradaItens[i].codigo_ncm);

      vItem.setDouble('PESO_UNITARIO', pEntradaItens[i].Peso);
      vItem.setDouble('VALOR_TOTAL', pEntradaItens[i].valor_total);
      vItem.setDouble('PRECO_UNITARIO', pEntradaItens[i].preco_unitario);
      vItem.setDouble('QUANTIDADE', pEntradaItens[i].quantidade);
      vItem.setDouble('VALOR_TOTAL_DESCONTO', pEntradaItens[i].valor_total_desconto);
      vItem.setDouble('VALOR_TOTAL_OUTRAS_DESPESAS', pEntradaItens[i].valor_total_outras_despesas);
      vItem.setString('CODIGO_BARRAS', pEntradaItens[i].codigo_barras);
      vItem.setInt('ORIGEM_PRODUTO', pEntradaItens[i].origem_produto);

      vItem.setDouble('INDICE_REDUCAO_BASE_ICMS', pEntradaItens[i].indice_reducao_base_icms);
      vItem.setDouble('BASE_CALCULO_ICMS', pEntradaItens[i].base_calculo_icms);
      vItem.setDouble('PERCENTUAL_ICMS', pEntradaItens[i].percentual_icms);
      vItem.setDouble('VALOR_ICMS', pEntradaItens[i].valor_icms);

      vItem.setDouble('INDICE_REDUCAO_BASE_ICMS_ST', pEntradaItens[i].indice_reducao_base_icms_st);
      vItem.setDouble('BASE_CALCULO_ICMS_ST', pEntradaItens[i].base_calculo_icms_st);
      vItem.setDouble('PERCENTUAL_ICMS_ST', pEntradaItens[i].percentual_icms_st);
      vItem.setDouble('VALOR_ICMS_ST', pEntradaItens[i].valor_icms_st);

      vItem.setString('CST_PIS', pEntradaItens[i].cst_pis);
      vItem.setDouble('BASE_CALCULO_PIS', pEntradaItens[i].base_calculo_pis);
      vItem.setDouble('PERCENTUAL_PIS', pEntradaItens[i].percentual_pis);
      vItem.setDouble('VALOR_PIS', pEntradaItens[i].valor_pis);

      vItem.setString('CST_COFINS', pEntradaItens[i].cst_cofins);
      vItem.setDouble('BASE_CALCULO_COFINS', pEntradaItens[i].base_calculo_cofins);
      vItem.setDouble('PERCENTUAL_COFINS', pEntradaItens[i].percentual_cofins);
      vItem.setDouble('VALOR_COFINS', pEntradaItens[i].valor_cofins);

      vItem.setDouble('VALOR_IPI', pEntradaItens[i].valor_ipi);
      vItem.setDouble('PERCENTUAL_IPI', pEntradaItens[i].percentual_ipi);

      vItem.setDouble('IVA', pEntradaItens[i].iva);
      vItem.setDouble('PRECO_PAUTA', pEntradaItens[i].preco_pauta);

      (* Forma��o do custo da entrada *)
      vItem.setDouble('VALOR_FRETE_CUSTO', pEntradaItens[i].ValorFreteCusto);
      vItem.setDouble('VALOR_IPI_CUSTO', pEntradaItens[i].ValorIPICusto);
      vItem.setDouble('VALOR_ICMS_ST_CUSTO', pEntradaItens[i].ValorICMSStCusto);
      vItem.setDouble('VALOR_OUTROS_CUSTO', 0);
      vItem.setDouble('VALOR_DIFAL_CUSTO', pEntradaItens[i].ValorDifalCusto);
      vItem.setDouble('VALOR_OUTROS_CUSTO', pEntradaItens[i].ValorOutrosCusto);
      vItem.setDouble('PRECO_FINAL', pEntradaItens[i].PrecoFinal);

      vItem.setDouble('VALOR_ICMS_CUSTO', pEntradaItens[i].ValorICMSCusto);
      vItem.setDouble('VALOR_ICMS_FRETE_CUSTO', pEntradaItens[i].ValorICMSFreteCusto);
      vItem.setDouble('VALOR_PIS_CUSTO', pEntradaItens[i].ValorPisCusto);
      vItem.setDouble('VALOR_COFINS_CUSTO', pEntradaItens[i].ValorCofinsCusto);
      vItem.setDouble('VALOR_PIS_FRETE_CUSTO', 0);
      vItem.setDouble('VALOR_COFINS_FRETE_CUSTO', 0);
      vItem.setDouble('PRECO_LIQUIDO', pEntradaItens[i].PrecoLiquido);
      (***************                                       ****************)

      vItem.setString('UNIDADE_ENTRADA_ID', pEntradaItens[i].UnidadeEntradaId);
      vItem.setString('UNIDADE_COMPRA_ID', pEntradaItens[i].UnidadeCompraId);
      vItem.setDouble('QUANTIDADE_EMBALAGEM', pEntradaItens[i].QuantidadeEmbalagem);
      vItem.setDouble('QUANTIDADE_ENTRADA_ALTIS', pEntradaItens[i].QuantidadeEntradaAltis);
      vItem.setDouble('MULTIPLO_COMPRA', pEntradaItens[i].MultiploCompra);

      vItem.setString('INFORMACOES_ADICIONAIS', pEntradaItens[i].informacoes_adicionais);

      vItem.Inserir;

      for j := Low(pLotes) to High(pLotes) do begin
        if pEntradaItens[i].produto_id <> pLotes[j].ProdutoId then
          Continue;

        vLote.setInt('ENTRADA_ID', pEntradaId, True);
        vLote.setInt('ITEM_ID', pEntradaItens[i].item_id, True);
        vLote.setString('LOTE', pLotes[j].Lote, True);
        vLote.setDouble('QUANTIDADE', pLotes[j].Quantidade);
        vLote.setDataN('DATA_FABRICACAO', pLotes[j].DataFabricacao);
        vLote.setDataN('DATA_VENCIMENTO', pLotes[j].DataVencimento);

        vLote.Inserir;
      end;
    end;

    if not vNovo then begin
      vExec.Limpar;
      vExec.Add('delete from ENTRADAS_NOTAS_FISC_FINANCEIRO ');
      vExec.Add('where ENTRADA_ID = :P1 ');
      vExec.Executar([pEntradaId]);
    end;

    for i := Low(pEntradaFinanceiro) to High(pEntradaFinanceiro) do begin
      vFinanc.setInt('ENTRADA_ID', pEntradaId, True);
      vFinanc.setInt('ITEM_ID', pEntradaFinanceiro[i].ItemId, True);
      vFinanc.setInt('COBRANCA_ID', pEntradaFinanceiro[i].CobrancaId);
      vFinanc.setData('DATA_VENCIMENTO', pEntradaFinanceiro[i].DataVencimento);
      vFinanc.setInt('PARCELA', pEntradaFinanceiro[i].Parcela);
      vFinanc.setInt('NUMERO_PARCELAS', pEntradaFinanceiro[i].NumeroParcelas);
      vFinanc.setDouble('VALOR_DOCUMENTO', pEntradaFinanceiro[i].Valor);
      vFinanc.setString('DOCUMENTO', pEntradaFinanceiro[i].Documento);
      vFinanc.setStringN('NOSSO_NUMERO', pEntradaFinanceiro[i].NossoNumero);
      vFinanc.setStringN('CODIGO_BARRAS', pEntradaFinanceiro[i].CodigoBarras);
      vFinanc.setStringN('TIPO', 'N');

      vFinanc.Inserir;
    end;

    for i := Low(pOutrosCustos) to High(pOutrosCustos) do begin
      vFinanc.setInt('ENTRADA_ID', pEntradaId, True);
      vFinanc.setInt('ITEM_ID', Length(pEntradaFinanceiro) + pOutrosCustos[i].ItemId, True);
      vFinanc.setInt('COBRANCA_ID', pOutrosCustos[i].CobrancaId);
      vFinanc.setData('DATA_VENCIMENTO', pOutrosCustos[i].DataVencimento);
      vFinanc.setInt('PARCELA', pOutrosCustos[i].Parcela);
      vFinanc.setInt('NUMERO_PARCELAS', pOutrosCustos[i].NumeroParcelas);
      vFinanc.setDouble('VALOR_DOCUMENTO', pOutrosCustos[i].Valor);
      vFinanc.setString('DOCUMENTO', pOutrosCustos[i].Documento);
      vFinanc.setStringN('NOSSO_NUMERO', pOutrosCustos[i].NossoNumero);
      vFinanc.setStringN('CODIGO_BARRAS', pOutrosCustos[i].CodigoBarras);
      vFinanc.setStringN('TIPO', 'O');
      vFinanc.setInt('FORNECEDOR_ID', pOutrosCustos[i].FornecedorId);
      vFinanc.setString('PLANO_FINANCEIRO_ID', pOutrosCustos[i].PlanoFinanceiroId);
      vFinanc.setInt('CENTRO_CUSTO_ID', pOutrosCustos[i].CentroCustoId);

      vFinanc.Inserir;
    end;

    for i := Low(pItensCompras) to High(pItensCompras) do begin
      vItensCompra.setInt('ENTRADA_ID', pEntradaId, True);
      vItensCompra.setInt('ITEM_ID', pItensCompras[i].ItemId, True);
      vItensCompra.setInt('COMPRA_ID', pItensCompras[i].CompraId, True);
      vItensCompra.setInt('ITEM_COMPRA_ID', pItensCompras[i].ItemCompraId);
      vItensCompra.setDouble('QUANTIDADE', pItensCompras[i].Quantidade);

      vItensCompra.Inserir;
    end;

    if (vNovo) and (pCaminhoArquivo <> '') then begin
      vArquivo := TMemoryStream.Create;
      vArquivo.LoadFromFile(pCaminhoArquivo);

      vExec.Clear;
      vExec.Add('insert into ENTRADAS_NOTAS_FISCAIS_XML(ENTRADA_ID, XML_TEXTO)values(:P1, :P2)');
      vExec.Executar([pEntradaId, pXmlTexto]);

      vExec.Clear;
      vExec.Add('update ENTRADAS_NOTAS_FISCAIS_XML set ');
      vExec.Add('  XML = :P2 ');
      vExec.Add('where ENTRADA_ID = :P1');

      vExec.CarregarBinario('P2', vArquivo);

      vExec.Executar([pEntradaId]);
    end;

    (* Se for para consolidar o estoque *)
    if pStatusEntrada = 'ECO' then begin
      vProc.Params[0].AsInteger := pEntradaId;
      vProc.Executar;

      vProcFin.Params[0].AsInteger := pEntradaId;
      vProcFin.Executar;
    end
    else if Sessao.getParametros.AtualizarCustoProdutosAguardandoChegada = 'S' then begin
      vProcCustos.Params[0].AsInteger := pEntradaId;
      vProcCustos.Executar;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vItensCompra.Free;
  vProcFin.Free;
  vFinanc.Free;
  vLote.Free;
  vProc.Free;
  VProcCustos.Free;
  vExec.Free;
  vItem.Free;
  t.Free;
end;

function BuscarEntradaNotaFiscais(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEntradaNotaFiscal>;
var
  i: Integer;
  t: TEntradaNotaFiscal;
begin
  Result := nil;
  t := TEntradaNotaFiscal.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordEntradaNotaFiscal;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarEntradaNotaFiscaisComando(
  pConexao: TConexao;
  pComando: string
): TArray<RecEntradaNotaFiscal>;
var
  i: Integer;
  t: TEntradaNotaFiscal;
begin
  Result := nil;
  t := TEntradaNotaFiscal.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordEntradaNotaFiscal;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirEntradaNotaFiscal(
  pConexao: TConexao;
  pEntradaId: Integer
): RecRetornoBD;
var
  t: TEntradaNotaFiscal;
  vExec: TExecucao;
begin
  Result.Iniciar;
  t := TEntradaNotaFiscal.Create(pConexao);
  vExec := TExecucao.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('ENTRADA_ID', pEntradaId, True);

    vExec.Clear;
    vExec.Add('delete from ENTRADAS_NF_ITENS_COMPRAS ');
    vExec.Add('where ENTRADA_ID = :P1');
    vExec.Executar([pEntradaId]);

    vExec.Clear;
    vExec.Add('delete from ENTRADAS_NOTAS_FISCAIS_ITENS ');
    vExec.Add('where ENTRADA_ID = :P1');
    vExec.Executar([pEntradaId]);

    vExec.Clear;
    vExec.Add('delete from ENTRADAS_NOTAS_FISC_FINANCEIRO ');
    vExec.Add('where ENTRADA_ID = :P1');
    vExec.Executar([pEntradaId]);

    vExec.Clear;
    vExec.Add('delete from ENTRADAS_NOTAS_FISCAIS_XML ');
    vExec.Add('where ENTRADA_ID = :P1');
    vExec.Executar([pEntradaId]);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function ExisteEntradaNFe(pConexao: TConexao; pChaveNFe: string): Boolean;
var
  p: TConsulta;
begin
  p := TConsulta.Create(pConexao);

  p.Add('select ');
  p.Add('  count(*) ');
  p.Add('from ');
  p.Add('  ENTRADAS_NOTAS_FISCAIS ');
  p.Add('where RETORNA_NUMEROS(CHAVE_NFE) = :P1 ');

  p.Pesquisar([ RetornaNumeros(pChaveNFe) ]);
  Result := p.GetInt(0) > 0;

  p.Free;
end;

function ExisteEntradaNFe(pConexao: TConexao; pFornecedorId: Integer; pNumeroNota: Integer; pModeloNota: string; pSerieNota: string): Boolean;
var
  p: TConsulta;
begin
  p := TConsulta.Create(pConexao);

  p.Add('select ');
  p.Add('  count(*) ');
  p.Add('from ');
  p.Add('  ENTRADAS_NOTAS_FISCAIS ');
  p.Add('where FORNECEDOR_ID = :P1 ');
  p.Add('and NUMERO_NOTA = :P2 ');
  p.Add('and MODELO_NOTA = :P3 ');
  p.Add('and SERIE_NOTA = :P4 ');

  p.Pesquisar([ pFornecedorId, pNumeroNota, pModeloNota, pSerieNota ]);
  Result := p.GetInt(0) > 0;

  p.Free;
end;

function ExisteEntradaConhecimentoVinculado(pConexao: TConexao; pConhecimentoId: Integer): Boolean;
var
  p: TConsulta;
begin
  p := TConsulta.Create(pConexao);

  p.Add('select ');
  p.Add('  count(*) ');
  p.Add('from ');
  p.Add('  ENTRADAS_NOTAS_FISCAIS ');
  p.Add('where CONHECIMENTO_FRETE_ID = :P1 ');
  p.Add('and STATUS not in(''ACM'', ''ACE'') ');

  p.Pesquisar([ pConhecimentoId ]);
  Result := p.GetInt(0) > 0;

  p.Free;
end;

function DesconsolidarEstoque(pConexao: TConexao; pEntradaId: Integer): RecRetornoBD;
var
  vDescFinanc: TProcedimentoBanco;
  vDescEstoq: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('DESCONSOLIDAR_EST_ENTRADA_NF');
  vDescFinanc := TProcedimentoBanco.Create(pConexao, 'DESCONS_FINANC_ENT_NOTA_FISCAL');
  vDescEstoq := TProcedimentoBanco.Create(pConexao, 'DESCONS_ESTOQUE_ENTR_NOTA_FISC');

  try
    pConexao.IniciarTransacao;

    vDescFinanc.Executar([pEntradaId]);
    vDescEstoq.Executar([pEntradaId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  vDescFinanc.Free;
  vDescEstoq.Free;
end;

function getCorStatus(pStatus: string): Integer;
begin
  Result :=
   _BibliotecaGenerica.Decode(pStatus, [
      'ACM', clBlue,
      'ACE', clTeal,
      'ECO', $000080FF,
      'FCO', clOlive,
      'BAI', clNavy,
      '', clBlack
   ]);
end;

end.
