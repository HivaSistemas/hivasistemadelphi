unit _AjustesEstoqueItens;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsEstoques, System.StrUtils;

{$M+}
type
  TAjusteEstoqueItens = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordAjusteEstoqueItens: RecAjusteEstoqueItens;
  end;

function BuscarAjusteEstoqueItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecAjusteEstoqueItens>;

function BuscarAjusteEstoqueItensComando(pConexao: TConexao; pComando: string): TArray<RecAjusteEstoqueItens>;

function ExcluirAjusteEstoqueItens(
  pConexao: TConexao;
  pAjusteEstoqueId: Integer;
  pItemId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TAjusteEstoqueItens }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where AJUSTE_ESTOQUE_ID = :P1 ' +
      'order by ' +
      '  PRODUTO_ID, ' +
      '  ITEM_ID '
    );
end;

constructor TAjusteEstoqueItens.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'AJUSTES_ESTOQUE_ITENS');

  FSql :=
    'select ' +
    '  ITE.AJUSTE_ESTOQUE_ID, ' +
    '  ITE.ITEM_ID, ' +
    '  ITE.LOCAL_ID, ' +
    '  LOC.NOME as NOME_LOCAL, ' +
    '  ITE.PRODUTO_ID, ' +
    '  PRO.NOME as NOME_PRODUTO, ' +
    '  PRO.MARCA_ID, ' +
    '  MAR.NOME as NOME_MARCA, ' +
    '  PRO.UNIDADE_VENDA as UNIDADE, ' +
    '  ITE.QUANTIDADE, ' +
    '  ITE.NATUREZA, ' +
    '  ITE.LOTE, ' +
    '  ITE.PRECO_UNITARIO, ' +
    '  ITE.VALOR_TOTAL, ' +
    '  ITE.DATA_FABRICACAO, ' +
    '  ITE.DATA_VENCIMENTO, ' +
    '  ITE.ESTOQUE_INFORMADO ' +
    'from ' +
    '  AJUSTES_ESTOQUE_ITENS ITE ' +

    'inner join PRODUTOS PRO ' +
    'on ITE.PRODUTO_ID = PRO.PRODUTO_ID ' +

    'inner join MARCAS MAR ' +
    'on PRO.MARCA_ID = MAR.MARCA_ID ' +

    'inner join LOCAIS_PRODUTOS LOC ' +
    'on ITE.LOCAL_ID = LOC.LOCAL_ID ';

  setFiltros(getFiltros);

  AddColuna('AJUSTE_ESTOQUE_ID', True);
  AddColuna('LOCAL_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('LOTE', True);
  AddColuna('PRODUTO_ID');
  AddColunaSL('NOME_PRODUTO');
  AddColunaSL('MARCA_ID');
  AddColunaSL('NOME_MARCA');
  AddColunaSL('UNIDADE');
  AddColuna('QUANTIDADE');
  AddColuna('NATUREZA');
  AddColunaSL('NOME_LOCAL');
  AddColuna('PRECO_UNITARIO');
  AddColuna('VALOR_TOTAL');
  AddColuna('DATA_FABRICACAO');
  AddColuna('DATA_VENCIMENTO');
  AddColuna('ESTOQUE_INFORMADO');
end;

function TAjusteEstoqueItens.getRecordAjusteEstoqueItens: RecAjusteEstoqueItens;
begin
  Result.ajuste_estoque_id := getInt('AJUSTE_ESTOQUE_ID', True);
  Result.LocalId           := getInt('LOCAL_ID', True);
  Result.item_id           := getInt('ITEM_ID', True);
  Result.Lote              := getString('LOTE', True);
  Result.produto_id        := getInt('PRODUTO_ID');
  Result.NomeProduto       := getString('NOME_PRODUTO');
  Result.MarcaId           := getInt('MARCA_ID');
  Result.NomeMarca         := getString('NOME_MARCA');
  Result.quantidade        := getDouble('QUANTIDADE');
  Result.Unidade           := getString('UNIDADE');
  Result.NomeLocal         := getString('NOME_LOCAL');
  Result.PrecoUnitario     := getDouble('PRECO_UNITARIO');
  Result.ValorTotal        := getDouble('VALOR_TOTAL');
  Result.DataFabricacao    := getData('DATA_FABRICACAO');
  Result.DataVencimento    := getData('DATA_VENCIMENTO');
  Result.quantidadeInformado := getDouble('ESTOQUE_INFORMADO');

  Result.Natureza          := getString('NATUREZA');
  Result.NaturezaAnalitico := IfThen(Result.Natureza = 'E', 'Entrada', 'Sa�da');
end;

function BuscarAjusteEstoqueItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecAjusteEstoqueItens>;
var
  i: Integer;
  t: TAjusteEstoqueItens;
begin
  Result := nil;
  t := TAjusteEstoqueItens.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordAjusteEstoqueItens;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarAjusteEstoqueItensComando(pConexao: TConexao; pComando: string): TArray<RecAjusteEstoqueItens>;
var
  i: Integer;
  t: TAjusteEstoqueItens;
begin
  Result := nil;
  t := TAjusteEstoqueItens.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordAjusteEstoqueItens;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirAjusteEstoqueItens(
  pConexao: TConexao;
  pAjusteEstoqueId: Integer;
  pItemId: Integer
): RecRetornoBD;
var
  t: TAjusteEstoqueItens;
begin
  Result.TeveErro := False;
  t := TAjusteEstoqueItens.Create(pConexao);

  try
    t.setInt('AJUSTE_ESTOQUE_ID', pAjusteEstoqueId, True);
    t.setInt('ITEM_ID', pItemId, True);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
