unit _CfopParametrosFiscaisEmpr;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecCfopParametrosFiscaisEmpr = record
    EmpresaId: Integer;
    CfopId: string;
    CalcularPisCofins: string;
  end;

  TCfopParametrosFiscaisEmpr = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordCfopParametrosFiscaisEmpr: RecCfopParametrosFiscaisEmpr;
  end;

function BuscarCfopParametrosFiscaisEmpr(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCfopParametrosFiscaisEmpr>;

implementation

{ TCfopParametrosFiscaisEmpr }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CFOP_ID = :P1'
    );
end;

constructor TCfopParametrosFiscaisEmpr.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CFOP_PARAMETROS_FISCAIS_EMPR');

  FSql := 
    'select ' +
    '  EMPRESA_ID, ' +
    '  CFOP_ID, ' +
    '  CALCULAR_PIS_COFINS ' +
    'from ' +
    '  CFOP_PARAMETROS_FISCAIS_EMPR ';

  setFiltros(getFiltros);

  AddColuna('EMPRESA_ID', True);
  AddColuna('CFOP_ID', True);
  AddColuna('CALCULAR_PIS_COFINS');
end;

function TCfopParametrosFiscaisEmpr.getRecordCfopParametrosFiscaisEmpr: RecCfopParametrosFiscaisEmpr;
begin
  Result.EmpresaId                  := getInt('EMPRESA_ID', True);
  Result.CfopId                     := getString('CFOP_ID', True);
  Result.CalcularPisCofins          := getString('CALCULAR_PIS_COFINS');
end;

function BuscarCfopParametrosFiscaisEmpr(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCfopParametrosFiscaisEmpr>;
var
  i: Integer;
  t: TCfopParametrosFiscaisEmpr;
begin
  Result := nil;
  t := TCfopParametrosFiscaisEmpr.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordCfopParametrosFiscaisEmpr;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
