unit _AcumuladosPagamentos;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsFinanceiros;

{$M+}
type
  RecAcumuladosPagamentos = record
    AcumuladoId: Integer;
    CobrancaId: Integer;
    ItemId: Integer;
    Tipo: string;
    DataVencimento: TDateTime;
    NsuTef: string;
    Valor: Double;
    Parcela: Integer;
    NumeroParcelas: Integer;
  end;

  TAcumuladosPagamentos = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordAcumuladosPagamentos: RecTitulosFinanceiros;
  end;

function BuscarAcumuladosPagamentos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TAcumuladosPagamentos }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 3);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where APG.ACUMULADO_ID = :P1 ' +
      'and TCO.TIPO_CARTAO = :P2 ' +
      'and APG.TIPO = ''CR'' '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where APG.ACUMULADO_ID = :P1 ' +
      'and APG.TIPO = ''CO'' ' +
      'order by ' +
      '  APG.COBRANCA_ID, ' +
      '  APG.DATA_VENCIMENTO '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where APG.ACUMULADO_ID = :P1 ' +
      'and APG.TIPO = ''CR'' '
    );
end;

constructor TAcumuladosPagamentos.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ACUMULADOS_PAGAMENTOS');

  FSql :=
    'select ' +
    '  APG.ACUMULADO_ID, ' +
    '  APG.COBRANCA_ID, ' +
    '  TCO.NOME as NOME_COBRANCA, ' +
    '  APG.ITEM_ID, ' +
    '  APG.TIPO, ' +
    '  APG.DATA_VENCIMENTO, ' +
    '  APG.NSU_TEF, ' +
    '  APG.CODIGO_AUTORIZACAO, ' +
    '  APG.TIPO_RECEB_CARTAO, ' +
    '  APG.NUMERO_CARTAO, ' +
    '  APG.VALOR, ' +
    '  APG.PARCELA, ' +
    '  APG.NUMERO_PARCELAS ' +
    'from ' +
    '  ACUMULADOS_PAGAMENTOS APG ' +

    'inner join TIPOS_COBRANCA TCO ' +
    'on APG.COBRANCA_ID = TCO.COBRANCA_ID ';

  setFiltros(getFiltros);

  AddColuna('ACUMULADO_ID', True);
  AddColuna('COBRANCA_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('TIPO');
  AddColuna('DATA_VENCIMENTO');
  AddColuna('NSU_TEF');
  AddColuna('NUMERO_CARTAO');
  AddColuna('CODIGO_AUTORIZACAO');
  AddColuna('TIPO_RECEB_CARTAO');
  AddColuna('VALOR');
  AddColuna('PARCELA');
  AddColuna('NUMERO_PARCELAS');
  AddColunaSL('NOME_COBRANCA');
end;

function TAcumuladosPagamentos.getRecordAcumuladosPagamentos: RecTitulosFinanceiros;
begin
  Result.Id                := getInt('ACUMULADO_ID', True);
  Result.CobrancaId        := getInt('COBRANCA_ID', True);
  Result.ItemId            := getInt('ITEM_ID', True);
  Result.Tipo              := getString('TIPO');
  Result.DataVencimento    := getData('DATA_VENCIMENTO');
  Result.NsuTef            := getString('NSU_TEF');
  Result.CodigoAutorizacao := getString('CODIGO_AUTORIZACAO');
  Result.TipoRecebCartao   := getString('TIPO_RECEB_CARTAO');
  Result.NumeroCartao      := getString('NUMERO_CARTAO');
  Result.Valor             := getDouble('VALOR');
  Result.Parcela           := getInt('PARCELA');
  Result.NumeroParcelas    := getInt('NUMERO_PARCELAS');
  Result.NomeCobranca      := getString('NOME_COBRANCA');
end;

function BuscarAcumuladosPagamentos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;
var
  i: Integer;
  t: TAcumuladosPagamentos;
begin
  Result := nil;
  t := TAcumuladosPagamentos.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordAcumuladosPagamentos;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.

