unit _ProdutosICMSCompra;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TProdutosICMSCompra = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordProdutosICMS: RecProdutosICMSCompra;
  end;

function AtualizarProdutosICMSCompra(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pProdutoid: Integer;
  pIva: Double;
  pPercentualIcms: Double;
  pCstRevenda: string;
  pCstDistribuidora: string;
  pCstIndustria: string;
  pPrecoPauta: Double;
  pEstadoId: string
): RecRetornoBD;

function BuscarProdutosICMSCompra(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecProdutosICMSCompra>;

function ExcluirProdutosICMSCompra(
  pConexao: TConexao;
  pProdutoId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TProdutosICMSCompra }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where PRODUTO_ID = :P1 ' +
      'and EMPRESA_ID = :P2 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where PRODUTO_ID = :P1 ' +
      'and ESTADO_ID = :P2 '
    );
end;

constructor TProdutosICMSCompra.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PRODUTOS_ICMS_COMPRA');

  FSql :=
    'select ' +
    '  EMPRESA_ID, ' +
    '  PRODUTO_ID, ' +
    '  ESTADO_ID, ' +
    '  IVA, ' +
    '  PERCENTUAL_ICMS, ' +
    '  INDICE_REDUCAO_BASE_ICMS, ' +
    '  PRECO_PAUTA, ' +
    '  CST_REVENDA, ' +
    '  CST_DISTRIBUIDORA, ' +
    '  CST_INDUSTRIA ' +
    'from ' +
    '  PRODUTOS_ICMS_COMPRA ';

  setFiltros(getFiltros);

  AddColuna('EMPRESA_ID', True);
  AddColuna('PRODUTO_ID', True);
  AddColuna('ESTADO_ID', True);
  AddColuna('IVA');
  AddColuna('PERCENTUAL_ICMS');
  AddColuna('INDICE_REDUCAO_BASE_ICMS');
  AddColuna('PRECO_PAUTA');
  AddColuna('CST_REVENDA');
  AddColuna('CST_DISTRIBUIDORA');
  AddColuna('CST_INDUSTRIA');
end;

function TProdutosICMSCompra.getRecordProdutosICMS: RecProdutosICMSCompra;
begin
  Result.EmpresaId                := getInt('EMPRESA_ID', True);
  Result.produto_id               := getInt('PRODUTO_ID', True);
  Result.estado_id                := getString('ESTADO_ID', True);
  Result.iva                      := getDouble('IVA');
  Result.percentual_icms          := getDouble('PERCENTUAL_ICMS');
  Result.preco_pauta              := getDouble('PRECO_PAUTA');
  Result.cst_revenda              := getString('CST_REVENDA');
  Result.cst_distribuidora        := getString('CST_DISTRIBUIDORA');
  Result.cst_industria            := getString('CST_INDUSTRIA');
  Result.indice_reducao_base_icms := getDouble('INDICE_REDUCAO_BASE_ICMS');
end;

function AtualizarProdutosICMSCompra(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pProdutoid: Integer;
  pIva: Double;
  pPercentualIcms: Double;
  pCstRevenda: string;
  pCstDistribuidora: string;
  pCstIndustria: string;
  pPrecoPauta: Double;
  pEstadoId: string
): RecRetornoBD;
var
  t: TProdutosICMSCompra;
  //novo: Boolean;
  //seq: TSequencia;
begin
  Result.TeveErro := False;
  t := TProdutosICMSCompra.Create(pConexao);

  t.setInt('EMPRESA_ID', pEmpresaId, True);
  t.setInt('PRODUTO_ID', pProdutoid, True);
  t.setString('ESTADO_ID', pEstadoId, True);
  t.setDouble('IVA', pIva);
  t.setDouble('PERCENTUAL_ICMS', pPercentualIcms);
  t.setDouble('PRECO_PAUTA', pPrecoPauta);
  t.setString('CST_REVENDA', pCstRevenda);
  t.setString('CST_DISTRIBUIDORA', pCstDistribuidora);
  t.setString('CST_INDUSTRIA', pCstIndustria);

  try
//    if novo then
//      t.Inserir
//    else
//      t.Atualizar;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function BuscarProdutosICMSCompra(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecProdutosICMSCompra>;
var
  i: Integer;
  t: TProdutosICMSCompra;
begin
  Result := nil;
  t := TProdutosICMSCompra.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordProdutosICMS;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirProdutosICMSCompra(
  pConexao: TConexao;
  pProdutoId: Integer
): RecRetornoBD;
var
  t: TProdutosICMSCompra;
begin
  Result.TeveErro := False;
  t := TProdutosICMSCompra.Create(pConexao);
  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
