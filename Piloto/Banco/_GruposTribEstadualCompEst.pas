unit _GruposTribEstadualCompEst;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecGruposTribEstadualCompEst = record
    GrupoTribEstadualCompraId: Integer;
    EstadoId: string;
    CstRevenda: string;
    CstDistribuidora: string;
    CstIndustria: string;
    IndiceReducaoBaseIcms: Double;
    Iva: Double;
    PrecoPauta: Double;
  end;

  TGruposTribEstadualCompEst = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordGruposTribEstadualCompEst: RecGruposTribEstadualCompEst;
  end;

function BuscarGruposTribEstadualCompEst(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecGruposTribEstadualCompEst>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TGruposTribEstadualCompEst }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where GRUPO_TRIB_ESTADUAL_COMPRA_ID = :P1'
    );
end;

constructor TGruposTribEstadualCompEst.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'GRUPOS_TRIB_ESTADUAL_COMP_EST');

  FSql := 
    'select ' +
    '  GRUPO_TRIB_ESTADUAL_COMPRA_ID, ' +
    '  ESTADO_ID, ' +
    '  CST_REVENDA, ' +
    '  CST_DISTRIBUIDORA, ' +
    '  CST_INDUSTRIA, ' +
    '  INDICE_REDUCAO_BASE_ICMS, ' +
    '  IVA, ' +
    '  PRECO_PAUTA ' +
    'from ' +
    '  GRUPOS_TRIB_ESTADUAL_COMP_EST GTE ';

  setFiltros(getFiltros);

  AddColuna('GRUPO_TRIB_ESTADUAL_COMPRA_ID', True);
  AddColuna('ESTADO_ID', True);
  AddColuna('CST_REVENDA');
  AddColuna('CST_DISTRIBUIDORA');
  AddColuna('CST_INDUSTRIA');
  AddColuna('INDICE_REDUCAO_BASE_ICMS');
  AddColuna('IVA');
  AddColuna('PRECO_PAUTA');
end;

function TGruposTribEstadualCompEst.getRecordGruposTribEstadualCompEst: RecGruposTribEstadualCompEst;
begin
  Result.GrupoTribEstadualCompraId  := getInt('GRUPO_TRIB_ESTADUAL_COMPRA_ID', True);
  Result.EstadoId                   := getString('ESTADO_ID', True);
  Result.CstRevenda                 := getString('CST_REVENDA');
  Result.CstDistribuidora           := getString('CST_DISTRIBUIDORA');
  Result.CstIndustria               := getString('CST_INDUSTRIA');
  Result.IndiceReducaoBaseIcms      := getDouble('INDICE_REDUCAO_BASE_ICMS');
  Result.Iva                        := getDouble('IVA');
  Result.PrecoPauta                 := getDouble('PRECO_PAUTA');
end;

function BuscarGruposTribEstadualCompEst(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecGruposTribEstadualCompEst>;
var
  i: Integer;
  t: TGruposTribEstadualCompEst;
begin
  Result := nil;
  t := TGruposTribEstadualCompEst.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordGruposTribEstadualCompEst;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
