unit _Arquivos;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, System.Classes, System.Variants, Data.DB;

{$M+}
type
  RecArquivos = record
    ArquivoId: Integer;
    UsuarioCadastroId: Integer;
    DataHoraCadastro: TDateTime;
    NomeArquivo: string;
    Origem: string;
    ClienteId: Integer;
    FornecedorId: Integer;
    CadastroId: Integer;
    OrcamentoId: Integer;
    OcorrenciaId: Integer;

    CaminhoArquivo: string;
  end;

  TArquivos = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordArquivos: RecArquivos;
  end;

function AtualizarArquivos(
  pConexao: TConexao;
  pOrigem: string;
  pId: Integer;
  pArquivos: TArray<RecArquivos>;
  pArquivosIdsExcluir: TArray<Integer>;
  pControlarTransacao: Boolean;
  pUsuarioCadastroId: Integer
): RecRetornoBD;

function BuscarArquivos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecArquivos>;

function getArquivos(pConexao: TConexao; pId: Integer; pOrigem: string; pNomeArquivo: string): TMemoryStream;

function getFiltros: TArray<RecFiltros>;

implementation

{ TArquivos }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 3);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ORCAMENTO_ID = :P1 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      1,
      'where CLIENTE_ID = :P1 '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      2,
      'where OCORRENCIA_ID = :P1 '
    );
end;

constructor TArquivos.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ARQUIVOS');

  FSql := 
    'select ' +
    '  ARQUIVO_ID, ' +
    '  USUARIO_CADASTRO_ID, ' +
    '  DATA_HORA_CADASTRO, ' +
    '  NOME_ARQUIVO, ' +
    '  ORIGEM, ' +
    '  CLIENTE_ID, ' +
    '  FORNECEDOR_ID, ' +
    '  CADASTRO_ID, ' +
    '  ORCAMENTO_ID, ' +
    '  OCORRENCIA_ID ' +
    'from ' +
    '  ARQUIVOS ';

  setFiltros(getFiltros);

  AddColuna('ARQUIVO_ID', True);
  AddColuna('USUARIO_CADASTRO_ID');
  AddColunaSL('DATA_HORA_CADASTRO');
  AddColuna('NOME_ARQUIVO');
  AddColuna('ORIGEM');
  AddColuna('CLIENTE_ID');
  AddColuna('FORNECEDOR_ID');
  AddColuna('CADASTRO_ID');
  AddColuna('ORCAMENTO_ID');
  AddColuna('OCORRENCIA_ID')
end;

function TArquivos.getRecordArquivos: RecArquivos;
begin
  Result.ArquivoId                  := getInt('ARQUIVO_ID', True);
  Result.UsuarioCadastroId          := getInt('USUARIO_CADASTRO_ID');
  Result.DataHoraCadastro           := getData('DATA_HORA_CADASTRO');
  Result.NomeArquivo                := getString('NOME_ARQUIVO');
  Result.Origem                     := getString('ORIGEM');
  Result.ClienteId                  := getInt('CLIENTE_ID');
  Result.FornecedorId               := getInt('FORNECEDOR_ID');
  Result.CadastroId                 := getInt('CADASTRO_ID');
  Result.OrcamentoId                := getInt('ORCAMENTO_ID');
  Result.OcorrenciaId               := getInt('OCORRENCIA_ID');
end;

function AtualizarArquivos(
  pConexao: TConexao;
  pOrigem: string;
  pId: Integer;
  pArquivos: TArray<RecArquivos>;
  pArquivosIdsExcluir: TArray<Integer>;
  pControlarTransacao: Boolean;
  pUsuarioCadastroId: Integer
): RecRetornoBD;
var
  i: Integer;
  t: TArquivos;
  vSeq: TSequencia;
  vExec: TExecucao;
begin
  Result.Iniciar;

  if pControlarTransacao then
    pConexao.setRotina('ATUALIZAR_ARQUIVO');

  t := TArquivos.Create(pConexao);
  vExec := TExecucao.Create(pConexao);
  vSeq := TSequencia.Create(pConexao, 'SEQ_ARQUIVOS');

  try
    if pControlarTransacao then
      pConexao.IniciarTransacao;

    t.setString('ORIGEM', pOrigem);
    if pOrigem = 'CLI' then
      t.setIntN('CLIENTE_ID', pId)
    else if pOrigem = 'FOR' then
      t.setIntN('FORNECEDOR_ID', pId)
    else if pOrigem = 'CAD' then
      t.setIntN('CADASTRO_ID', pId)
    else if pOrigem = 'CRM' then
      t.setIntN('OCORRENCIA_ID', pId)
    else
      t.setIntN('ORCAMENTO_ID', pId);

    if pArquivosIdsExcluir <> nil then begin
      vExec.Limpar;
      vExec.Add('delete from ARQUIVOS');
      vExec.Add('where ' + FiltroInInt('ARQUIVO_ID', pArquivosIdsExcluir));
      vExec.Executar;
    end;

    vExec.Limpar;
    vExec.Add('update ARQUIVOS set');
    vExec.Add('  ARQUIVO = :P2 ');
    vExec.Add('where ARQUIVO_ID = :P1 ');

    for i := Low(pArquivos) to High(pArquivos) do begin
      if pArquivos[i].CaminhoArquivo = '' then
        Continue;

      t.setInt('ARQUIVO_ID', vSeq.getProximaSequencia, True);
      t.setString('NOME_ARQUIVO', pArquivos[i].NomeArquivo);
      t.setInt('USUARIO_CADASTRO_ID', pUsuarioCadastroId);

      t.Inserir;

      vExec.ParamByName('P1').AsInteger := vSeq.SequenciaAtual;
      vExec.CarregarBinario('P2', pArquivos[i].CaminhoArquivo);
      vExec.Executar;
    end;

    if pControlarTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if pControlarTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;

  vSeq.Free;
  t.Free;
end;

function BuscarArquivos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecArquivos>;
var
  i: Integer;
  t: TArquivos;
begin
  Result := nil;
  t := TArquivos.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordArquivos;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function getArquivos(pConexao: TConexao; pId: Integer; pOrigem: string; pNomeArquivo: string): TMemoryStream;
var
  p: TConsulta;
  s: TStream;
begin
  Result := nil;

  p := TConsulta.Create(pConexao);
  p.SQL.Add('select ARQUIVO from ARQUIVOS where ORIGEM = :P1 and NOME_ARQUIVO = :P2 and ');
  if pOrigem = 'ORC' then
    p.SQL.Add('ORCAMENTO_ID = :P3 ')
  else if pOrigem = 'CLI' then
    p.SQL.Add('CLIENTE_ID = :P3 ')
  else if pOrigem = 'CRM' then
    p.SQL.Add('OCORRENCIA_ID = :P3 ');


  if p.Pesquisar([pOrigem, pNomeArquivo, PId]) then begin
    if not(p.Fields[0].Value = null) then begin
      Result := TMemoryStream.Create;
      s := p.CreateBlobStream(p.FieldByName('ARQUIVO'), bmRead);
      Result.LoadFromStream(s);
      s.Free;
    end;
  end;

  p.Free;
end;

end.
