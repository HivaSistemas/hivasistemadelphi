unit _DevolucoesItens;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsOrcamentosVendas;

{$M+}
type
  TDevolucoesItens = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordDevolucoesItens: RecDevolucoesItens;
  end;

function BuscarDevolucoesItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecDevolucoesItens>;

function BuscarDevolucoesItensComando(pConexao: TConexao; pComando: string): TArray<RecDevolucoesItens>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TTDevolucoesItens }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ITE.DEVOLUCAO_ID = :P1 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ITE.DEVOLUCAO_ID = :P1 ' +
      'and ITE.DEVOLVIDOS_ENTREGUES > 0'
    );
end;

constructor TDevolucoesItens.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'DEVOLUCOES_ITENS');

  FSql :=
    'select ' +
    '  ITE.DEVOLUCAO_ID, ' +
    '  ITE.ITEM_ID, ' +
    '  ITE.PRODUTO_ID, ' +
    '  PRO.NOME as NOME_PRODUTO, ' +
    '  PRO.MARCA_ID, ' +
    '  MAR.NOME as NOME_MARCA, ' +
    '  ITE.DEVOLVIDOS_ENTREGUES, ' +
    '  ITE.DEVOLVIDOS_PENDENCIAS, ' +
    '  ITE.DEVOLVIDOS_SEM_PREVISAO, ' +
    '  ITE.VALOR_LIQUIDO, ' +
    '  ITE.VALOR_BRUTO, ' +
    '  ITE.VALOR_DESCONTO, ' +
    '  ITE.VALOR_OUTRAS_DESPESAS, ' +
    '  ITE.VALOR_FRETE, ' +
    '  ITE.VALOR_ST, ' +
    '  ITE.VALOR_IPI, ' +
    '  ITE.PRECO_UNITARIO, ' +
    '  PRO.UNIDADE_VENDA, ' +
    '  PRO.CODIGO_BARRAS, ' +
    '  PRO.CODIGO_ORIGINAL_FABRICANTE, ' +
    '  PRO.UNIDADE_ENTREGA_ID, ' +
    '  PRO.MULTIPLO_VENDA, ' +
    '  ITE.SOMENTE_FISCAL, ' +
    '  (OIT.VALOR_CUSTO_FINAL/OIT.QUANTIDADE) AS VALOR_CUSTO_FINAL, ' +
    '  case PAE.TIPO_CUSTO_VIS_LUCRO_VENDA when ''FIN'' then zvl(OIT.PRECO_LIQUIDO, OIT.CUSTO_ULTIMO_PEDIDO)/OIT.QUANTIDADE when ''COM'' then zvl(OIT.CUSTO_ULTIMO_PEDIDO, OIT.PRECO_LIQUIDO)/OIT.QUANTIDADE else OIT.CMV/OIT.QUANTIDADE end as CUSTO_ENTRADA ' +
    'from ' +
    '  DEVOLUCOES_ITENS ITE ' +

    'inner join PRODUTOS PRO ' +
    'on ITE.PRODUTO_ID = PRO.PRODUTO_ID ' +

    'inner join DEVOLUCOES DEV ' +
    'on ITE.DEVOLUCAO_ID = DEV.DEVOLUCAO_ID ' +

    'inner join ORCAMENTOS ORC ' +
    'on DEV.ORCAMENTO_ID = ORC.ORCAMENTO_ID ' +

    'inner join ORCAMENTOS_ITENS OIT ' +
    'on OIT.PRODUTO_ID = ITE.PRODUTO_ID ' +
    'and OIT.ORCAMENTO_ID = DEV.ORCAMENTO_ID ' +

    'inner join PRECOS_PRODUTOS PRE ' +
    'on ITE.PRODUTO_ID = PRE.PRODUTO_ID ' +
    'and ORC.EMPRESA_ID = PRE.EMPRESA_ID ' +

    'inner join PARAMETROS_EMPRESA PAE ' +
    'on ORC.EMPRESA_ID = PAE.EMPRESA_ID ' +

    'inner join MARCAS MAR ' +
    'on PRO.MARCA_ID = MAR.MARCA_ID ';

  setFiltros(getFiltros);

  AddColuna('DEVOLUCAO_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('PRODUTO_ID');
  AddColunaSL('NOME_PRODUTO');
  AddColunaSL('MARCA_ID');
  AddColunaSL('NOME_MARCA');
  AddColuna('DEVOLVIDOS_ENTREGUES');
  AddColuna('DEVOLVIDOS_PENDENCIAS');
  AddColuna('DEVOLVIDOS_SEM_PREVISAO');
  AddColuna('VALOR_LIQUIDO');
  AddColuna('VALOR_BRUTO');
  AddColuna('VALOR_DESCONTO');
  AddColuna('VALOR_OUTRAS_DESPESAS');
  AddColuna('VALOR_FRETE');
  AddColuna('VALOR_ST');
  AddColuna('VALOR_IPI');
  AddColuna('PRECO_UNITARIO');
  AddColunaSL('UNIDADE_VENDA');
  AddColunaSL('CODIGO_BARRAS');
  AddColunaSL('CODIGO_ORIGINAL_FABRICANTE');
  AddColunaSL('UNIDADE_ENTREGA_ID');
  AddColunaSL('MULTIPLO_VENDA');
  AddColunaSL('CUSTO_ENTRADA');
  AddColunaSL('VALOR_CUSTO_FINAL');
  AddColuna('SOMENTE_FISCAL');
end;

function TDevolucoesItens.getRecordDevolucoesItens: RecDevolucoesItens;
begin
  Result.DevolucaoId         := getInt('DEVOLUCAO_ID', True);
  Result.ItemId              := getInt('ITEM_ID', True);
  Result.ProdutoId           := getInt('PRODUTO_ID');
  Result.NomeProduto         := getString('NOME_PRODUTO');
  Result.MarcaId             := getInt('MARCA_ID');
  Result.NomeMarca           := getString('NOME_MARCA');

  Result.QuantidadeDevolvidosEntregues  := getDouble('DEVOLVIDOS_ENTREGUES');
  Result.QuantidadeDevolvidosPendencias := getDouble('DEVOLVIDOS_PENDENCIAS');
  Result.QuantidadeDevolvSemPrevisao    := getDouble('DEVOLVIDOS_SEM_PREVISAO');

  Result.ValorLiquido        := getDouble('VALOR_LIQUIDO');
  Result.ValorBruto          := getDouble('VALOR_BRUTO');
  Result.ValorDesconto       := getDouble('VALOR_DESCONTO');
  Result.ValorOutrasDespesas := getDouble('VALOR_OUTRAS_DESPESAS');
  Result.ValorFrete               := getDouble('VALOR_FRETE');
  Result.ValorST                  := getDouble('VALOR_ST');
  Result.ValorIPI                 := getDouble('VALOR_IPI');
  Result.PrecoUnitario            := getDouble('PRECO_UNITARIO');
  Result.UnidadeVenda             := getString('UNIDADE_VENDA');
  Result.CodigoBarras             := getString('CODIGO_BARRAS');
  Result.CodigoOriginalFabricante := getString('CODIGO_ORIGINAL_FABRICANTE');
  Result.UnidadeEntregaId         := getString('UNIDADE_ENTREGA_ID');
  Result.MultiploVenda            := getDouble('MULTIPLO_VENDA');
  Result.CustoEntrada             := getDouble('CUSTO_ENTRADA');
  Result.ValorCustoFinal          := getDouble('VALOR_CUSTO_FINAL');
  Result.SomenteFiscal            := getString('SOMENTE_FISCAL');
end;

function BuscarDevolucoesItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecDevolucoesItens>;
var
  i: Integer;
  t: TDevolucoesItens;
begin
  Result := nil;
  t := TDevolucoesItens.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordDevolucoesItens;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarDevolucoesItensComando(pConexao: TConexao; pComando: string): TArray<RecDevolucoesItens>;
var
  i: Integer;
  t: TDevolucoesItens;
begin
  Result := nil;
  t := TDevolucoesItens.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordDevolucoesItens;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
