unit _ContasPagarBaixasPagamentos;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsFinanceiros;

{$M+}
type
  TContasPagarBaixasPagamento = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordContasPagarBaixasPagamentos: RecTitulosFinanceiros;
  end;

function AtualizarContasRecBaixasPagamentos(
  pConexao: TConexao;
  pBaixaId: Integer;
  pCobrancaId: Integer;
  pItemId: Integer;
  pTipo: string;
  pParcela: Integer;
  pNumeroParcelas: Integer;
  pDataVencimento: TDateTime;
  pNsuTef: string;
  pValor: Double
): RecRetornoBD;

function BuscarContasRecBaixasPagamentos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;

function ExcluirContasRecBaixasPagamentos(
  pConexao: TConexao
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TContasPagarBaixasPagamento }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 3);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CRP.BAIXA_ID = :P1 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CRP.BAIXA_ID = :P1 ' +
      'and CRP.TIPO = ''CR'' '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CRP.BAIXA_ID = :P1 ' +
      'and CRP.TIPO = ''CO'' '
    );
end;

constructor TContasPagarBaixasPagamento.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONTAS_PAGAR_BAIXAS_PAGAMENTOS');

  FSql :=
    'select ' +
    '  CPP.BAIXA_ID, ' +
    '  CPP.COBRANCA_ID, ' +
    '  CPP.ITEM_ID, ' +
    '  CPP.TIPO, ' +
    '  CPP.PARCELA, ' +
    '  CPP.NUMERO_PARCELAS, ' +
    '  CPP.DATA_VENCIMENTO, ' +
    '  CPP.NSU_TEF, ' +
    '  CPP.VALOR, ' +
    '  TCO.NOME as NOME_TIPO_COBRANCA, ' +
    '  TCO.TIPO_CARTAO, ' +
    '  TCO.TIPO_RECEBIMENTO_CARTAO ' +
    'from ' +
    '  CONTAS_PAGAR_BAIXAS_PAGAMENTOS CPP ' +

    'inner join TIPOS_COBRANCA TCO ' +
    'on CRP.COBRANCA_ID = TCO.COBRANCA_ID ';

  setFiltros(getFiltros);

  AddColuna('BAIXA_ID', True);
  AddColuna('COBRANCA_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('TIPO');
  AddColuna('PARCELA');
  AddColuna('NUMERO_PARCELAS');
  AddColuna('DATA_VENCIMENTO');
  AddColuna('NSU_TEF');
  AddColuna('VALOR');
  AddColunaSL('COBRANCA_ID');
  AddColunaSL('NOME_TIPO_COBRANCA');
  AddColunaSL('TIPO_CARTAO');
  AddColunaSL('TIPO_RECEBIMENTO_CARTAO');
end;

function TContasPagarBaixasPagamento.getRecordContasPagarBaixasPagamentos: RecTitulosFinanceiros;
begin
  Result.Id                    := getInt('BAIXA_ID', True);
  Result.CobrancaId            := getInt('COBRANCA_ID', True);
  Result.ItemId                := getInt('ITEM_ID', True);
  Result.Tipo                  := getString('TIPO');
  Result.Parcela               := getInt('PARCELA');
  Result.NumeroParcelas        := getInt('NUMERO_PARCELAS');
  Result.DataVencimento        := getData('DATA_VENCIMENTO');
  Result.NsuTef                := getString('NSU_TEF');
  Result.Valor                 := getDouble('VALOR');
  Result.NomeCobranca          := getString('NOME_TIPO_COBRANCA');
  Result.TipoCartao            := getString('TIPO_CARTAO');
  Result.TipoRecebimentoCartao := getString('TIPO_RECEBIMENTO_CARTAO');
end;

function AtualizarContasRecBaixasPagamentos(
  pConexao: TConexao;
  pBaixaId: Integer;
  pCobrancaId: Integer;
  pItemId: Integer;
  pTipo: string;
  pParcela: Integer;
  pNumeroParcelas: Integer;
  pDataVencimento: TDateTime;
  pNsuTef: string;
  pValor: Double
): RecRetornoBD;
var
  t: TContasPagarBaixasPagamento;
begin
  Result.TeveErro := False;
  t := TContasPagarBaixasPagamento.Create(pConexao);

  try
    t.setInt('BAIXA_ID', pBaixaId, True);
    t.setInt('COBRANCA_ID', pCobrancaId, True);
    t.setInt('ITEM_ID', pItemId, True);
    t.setString('TIPO', pTipo);
    t.setInt('PARCELA', pParcela);
    t.setInt('NUMERO_PARCELAS', pNumeroParcelas);
    t.setData('DATA_VENCIMENTO', pDataVencimento);
    t.setString('NSU_TEF', pNsuTef);
    t.setDouble('VALOR', pValor);

//    if vNovo then
//      t.Inserir
//    else
//      t.Atualizar;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function BuscarContasRecBaixasPagamentos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;
var
  i: Integer;
  t: TContasPagarBaixasPagamento;
begin
  Result := nil;
  t := TContasPagarBaixasPagamento.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordContasPagarBaixasPagamentos;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirContasRecBaixasPagamentos(
  pConexao: TConexao
): RecRetornoBD;
var
  t: TContasPagarBaixasPagamento;
begin
  Result.TeveErro := False;
  t := TContasPagarBaixasPagamento.Create(pConexao);

  try

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
