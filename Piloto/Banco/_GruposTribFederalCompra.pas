unit _GruposTribFederalCompra;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecGruposTribFederalCompra = class(TObject)
  public
    GrupoTribFederalCompraId: Integer;
    Descricao: string;
    OrigemProduto: Integer;
    CstPis: string;
    CstCofins: string;
    Ativo: string;
  end;

  TGruposTribFederalCompra = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordGruposTribFederalCompra: RecGruposTribFederalCompra;
  end;

function AtualizarGruposTribFederalCompra(
  pConexao: TConexao;
  pGrupoTribFederalCompraId: Integer;
  pDescricao: string;
  pOrigemProduto: Integer;
  pCstPis: string;
  pCstCofins: string;
  pAtivo: string
): RecRetornoBD;

function BuscarGruposTribFederalCompra(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecGruposTribFederalCompra>;

function ExcluirGruposTribFederalCompra(
  pConexao: TConexao;
  pGrupoTribFederalCompraId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TGruposTribFederalCompra }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where GRUPO_TRIB_FEDERAL_COMPRA_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Descri��o (Avan�ado)',
      True,
      0,
      'where DESCRICAO like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  GRUPO_TRIB_FEDERAL_COMPRA_ID',
      '',
      True
    );
end;

constructor TGruposTribFederalCompra.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'GRUPOS_TRIB_FEDERAL_COMPRA');

  FSql := 
    'select ' +
    '  GRUPO_TRIB_FEDERAL_COMPRA_ID, ' +
    '  DESCRICAO, ' +
    '  ORIGEM_PRODUTO, ' +
    '  CST_PIS, ' +
    '  CST_COFINS, ' +
    '  ATIVO ' +
    'from ' +
    '  GRUPOS_TRIB_FEDERAL_COMPRA';

  setFiltros(getFiltros);

  AddColuna('GRUPO_TRIB_FEDERAL_COMPRA_ID', True);
  AddColuna('DESCRICAO');
  AddColuna('ORIGEM_PRODUTO');
  AddColuna('CST_PIS');
  AddColuna('CST_COFINS');
  AddColuna('ATIVO');
end;

function TGruposTribFederalCompra.getRecordGruposTribFederalCompra: RecGruposTribFederalCompra;
begin
  Result := RecGruposTribFederalCompra.Create;

  Result.GrupoTribFederalCompraId   := getInt('GRUPO_TRIB_FEDERAL_COMPRA_ID', True);
  Result.Descricao                  := getString('DESCRICAO');
  Result.OrigemProduto              := getInt('ORIGEM_PRODUTO');
  Result.CstPis                     := getString('CST_PIS');
  Result.CstCofins                  := getString('CST_COFINS');
  Result.Ativo                      := getString('ATIVO');
end;

function AtualizarGruposTribFederalCompra(
  pConexao: TConexao;
  pGrupoTribFederalCompraId: Integer;
  pDescricao: string;
  pOrigemProduto: Integer;
  pCstPis: string;
  pCstCofins: string;
  pAtivo: string
): RecRetornoBD;
var
  t: TGruposTribFederalCompra;
  vNovo: Boolean;
  vSeq: TSequencia;
begin
  Result.Iniciar;
  pConexao.setRotina('ATUALIZAR_GRUPOS_TRIB_FED_COM');

  t := TGruposTribFederalCompra.Create(pConexao);

  vNovo := pGrupoTribFederalCompraId = 0;
  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_GRUPOS_TRIB_FEDERAL_COMPRA');
    pGrupoTribFederalCompraId := vSeq.getProximaSequencia;
    Result.AsInt := pGrupoTribFederalCompraId;
    vSeq.Free;
  end;

  try
    pConexao.IniciarTransacao; 

    t.setInt('GRUPO_TRIB_FEDERAL_COMPRA_ID', pGrupoTribFederalCompraId, True);
    t.setString('DESCRICAO', pDescricao);
    t.setInt('ORIGEM_PRODUTO', pOrigemProduto);
    t.setString('CST_PIS', pCstPis);
    t.setString('CST_COFINS', pCstCofins);
    t.setString('ATIVO', pAtivo);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao; 
  except
    on e: Exception do begin
      pConexao.VoltarTransacao; 
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarGruposTribFederalCompra(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecGruposTribFederalCompra>;
var
  i: Integer;
  t: TGruposTribFederalCompra;
begin
  Result := nil;
  t := TGruposTribFederalCompra.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordGruposTribFederalCompra;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirGruposTribFederalCompra(
  pConexao: TConexao;
  pGrupoTribFederalCompraId: Integer
): RecRetornoBD;
var
  t: TGruposTribFederalCompra;
begin
  Result.Iniciar;
  t := TGruposTribFederalCompra.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('GRUPO_TRIB_FEDERAL_COMPRA_ID', pGrupoTribFederalCompraId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
