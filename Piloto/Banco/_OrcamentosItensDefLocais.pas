unit _OrcamentosItensDefLocais;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, _RecordsCadastros;

type
  TOrcamentosItensDefLocais = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  end;

implementation

constructor TOrcamentosItensDefLocais.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ORCAMENTOS_ITENS_DEF_LOCAIS');

  FSql :=
    '';

  AddColuna('ORCAMENTO_ID', True);
  AddColuna('PRODUTO_ID', True);
  AddColuna('LOCAL_ID', True);
  AddColuna('EMPRESA_ID', True);
  AddColuna('QUANTIDADE_ATO');
  AddColuna('QUANTIDADE_RETIRAR');
  AddColuna('QUANTIDADE_ENTREGAR');
  AddColuna('ITEM_ID');
  AddColuna('STATUS');
  AddColuna('LOTE');
end;

end.
