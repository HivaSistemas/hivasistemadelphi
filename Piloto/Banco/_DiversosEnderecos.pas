unit _DiversosEnderecos;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TDiversosEndereco = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordDiversosEndereco: RecDiversosEnderecos;
  end;

function AtualizarDiversosEndereco(
  pConexao: TConexao;
  pCadastroId: Integer;
  pEnderecos: TArray<RecDiversosEnderecos>
): RecRetornoBD;

function BuscarDiversosEnderecos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteEnderecoEntrega: Boolean
): TArray<RecDiversosEnderecos>;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TDiversosEndereco }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where DEN.CADASTRO_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      1,
      'where DEN.CADASTRO_ID = :P1 ' +
      'and DEN.INSCRICAO_ESTADUAL = :P2 ',
      'order by ORDEM '
    );
end;

constructor TDiversosEndereco.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'DIVERSOS_ENDERECOS');

  FSql :=
    'select ' +
    '  DEN.CADASTRO_ID, ' +
    '  DEN.ORDEM, ' +
    '  DEN.TIPO_ENDERECO, ' +
    '  DEN.LOGRADOURO, ' +
    '  DEN.COMPLEMENTO, ' +
    '  DEN.NUMERO, ' +
    '  DEN.BAIRRO_ID, ' +
    '  DEN.PONTO_REFERENCIA, ' +
    '  DEN.CEP, ' +
    '  DEN.INSCRICAO_ESTADUAL, ' +
    '  BAI.NOME as NOME_BAIRRO, ' +
    '  CID.NOME as NOME_CIDADE ' +
    'from ' +
    '  DIVERSOS_ENDERECOS DEN ' +

    'inner join BAIRROS BAI ' +
    'on DEN.BAIRRO_ID = BAI.BAIRRO_ID ' +

    'inner join CIDADES CID ' +
    'on BAI.CIDADE_ID = CID.CIDADE_ID ';

  SetFiltros(GetFiltros);

  AddColuna('CADASTRO_ID', True);
  AddColuna('ORDEM', True);
  AddColuna('TIPO_ENDERECO');
  AddColuna('LOGRADOURO');
  AddColuna('COMPLEMENTO');
  AddColuna('NUMERO');
  AddColuna('BAIRRO_ID');
  AddColuna('PONTO_REFERENCIA');
  AddColuna('CEP');
  AddColuna('INSCRICAO_ESTADUAL');
  AddColunaSL('NOME_BAIRRO');
  AddColunaSL('NOME_CIDADE');
end;

function TDiversosEndereco.GetRecordDiversosEndereco: RecDiversosEnderecos;
begin
  Result.cadastro_id       := GetInt('CADASTRO_ID', True);
  Result.ordem             := GetInt('ORDEM', True);
  Result.tipo_endereco     := GetString('TIPO_ENDERECO');
  Result.logradouro        := GetString('LOGRADOURO');
  Result.complemento       := GetString('COMPLEMENTO');
  Result.numero            := getString('NUMERO');
  Result.bairro_id         := GetInt('BAIRRO_ID');
  Result.ponto_referencia  := GetString('PONTO_REFERENCIA');
  Result.cep               := GetString('CEP');
  Result.InscricaoEstadual := GetString('INSCRICAO_ESTADUAL');
  Result.nome_bairro       := GetString('NOME_BAIRRO');
  Result.nome_cidade       := GetString('NOME_CIDADE');

  Result.TipoEnderecoAnalitico := _Biblioteca.Decode(Result.tipo_endereco, ['E', 'Entrega', 'C', 'Cobran�a']);
end;

function AtualizarDiversosEndereco(
  pConexao: TConexao;
  pCadastroId: Integer;
  pEnderecos: TArray<RecDiversosEnderecos>
): RecRetornoBD;
var
  i: Integer;
  ex: TExecucao;
  t: TDiversosEndereco;
begin
  Result.TeveErro := False;
  ex := TExecucao.Create(pConexao);
  t := TDiversosEndereco.Create(pConexao);

  try
    ex.SQL.Add('delete from DIVERSOS_ENDERECOS where CADASTRO_ID = :P1');
    ex.Executar([pCadastroId]);

    t.SetInt('CADASTRO_ID', pCadastroId, True);
    for i := Low(pEnderecos) to High(pEnderecos) do begin
      t.SetInt('ORDEM', i, True);
      t.SetString('TIPO_ENDERECO', pEnderecos[i].tipo_endereco);
      t.SetString('LOGRADOURO', pEnderecos[i].logradouro);
      t.SetString('COMPLEMENTO', pEnderecos[i].complemento);
      t.setString('NUMERO', pEnderecos[i].numero);
      t.SetInt('BAIRRO_ID', pEnderecos[i].bairro_id);
      t.SetString('PONTO_REFERENCIA', pEnderecos[i].ponto_referencia);
      t.SetString('CEP', pEnderecos[i].cep);
      t.setString('NUMERO', pEnderecos[i].numero);
      t.setString('INSCRICAO_ESTADUAL', pEnderecos[i].InscricaoEstadual);

      t.Inserir;
    end;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function BuscarDiversosEnderecos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteEnderecoEntrega: Boolean
): TArray<RecDiversosEnderecos>;
var
  i: Integer;
  t: TDiversosEndereco;
  vSqlAuxiliar: string;
begin
  Result := nil;
  t := TDiversosEndereco.Create(pConexao);

  vSqlAuxiliar := '';
  if pSomenteEnderecoEntrega then
    vSqlAuxiliar :=  'and DEN.TIPO_ENDERECO = ''E'' ';

  if t.Pesquisar(pIndice, pFiltros, vSqlAuxiliar) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordDiversosEndereco;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
