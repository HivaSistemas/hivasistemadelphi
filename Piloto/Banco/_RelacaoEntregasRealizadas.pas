unit _RelacaoEntregasRealizadas;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsRelatorios, _BibliotecaGenerica;

type
  RecEntregasRealizadas = record
    TipoMovimento: string;
    MovimentoId: Integer;
    TipoMovimentoAnalitico: string;
    OrcamentoId: Integer;
    ClienteId: Integer;
    NomeCliente: string;
    EmpresaId: Integer;
    NomeEmpresa: string;
    LocalId: Integer;
    NomeLocal: string;
    VendedorId: Integer;
    NomeVendedor: string;
    DataHoraCadastro: TDateTime;
    QtdeProdutos: Double;
    PesoTotal: Double;
    UsuarioCadastroId: Integer;
    NomeUsuarioCadastro: string;
    UsuarioConfirmacaoId: Integer;
    NomeUsuarioConfirmacao: string;
    DataHoraEntrega: TDateTime;
    NomePessoaRecebeu: string;
  end;

  RecEntregasItens = record
    TipoMovimento: string;
    MovimentoId: Integer;
    ProdutoId: Integer;
    Nome: string;
    Quantidade: Double;
    Lote: string;
    MarcaId: Integer;
    NomeMarca: string;
    UnidadeVenda: string;
  end;

function BuscarEntregas(pConexao: TConexao; pComando: string): TArray<RecEntregasRealizadas>;
function BuscarItensEntregas(pConexao: TConexao; pComando: string): TArray<RecEntregasItens>;

implementation

function BuscarEntregas(pConexao: TConexao; pComando: string): TArray<RecEntregasRealizadas>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;

  vSql := TConsulta.Create( pConexao );
  vSql.Add('select ');
  vSql.Add('  TIPO_MOVIMENTO, ');
  vSql.Add('  MOVIMENTO_ID, ');
  vSql.Add('  DATA_HORA_CADASTRO, ');
  vSql.Add('  LOCAL_ID, ');
  vSql.Add('  NOME_LOCAL, ');
  vSql.Add('  EMPRESA_ID, ');
  vSql.Add('  NOME_EMPRESA, ');
  vSql.Add('  ORCAMENTO_ID, ');
  vSql.Add('  VENDEDOR_ID, ');
  vSql.Add('  NOME_VENDEDOR, ');
  vSql.Add('  QTDE_PRODUTOS, ');
  vSql.Add('  PESO_TOTAL, ');
  vSql.Add('  CLIENTE_ID, ');
  vSql.Add('  NOME_CLIENTE, ');
  vSql.Add('  USUARIO_CADASTRO_ID, ');
  vSql.Add('  NOME_USUARIO_CADASTRO, ');
  vSql.Add('  USUARIO_CONFIRMACAO_ID, ');
  vSql.Add('  NOME_USUARIO_CONFIRMACAO, ');
  vSql.Add('  DATA_HORA_ENTREGA, ');
  vSql.Add('  NOME_PESSOA_RECEBEU ');
  vSql.Add('from ');
  vSql.Add('  VW_ENTREGAS ');
  vSql.Add(pComando);

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros -1 do begin
      Result[i].TipoMovimento           := vSql.GetString('TIPO_MOVIMENTO');
      Result[i].MovimentoId             := vSql.GetInt('MOVIMENTO_ID');
      Result[i].OrcamentoId             := vSql.GetInt('ORCAMENTO_ID');
      Result[i].ClienteId               := vSql.GetInt('CLIENTE_ID');
      Result[i].NomeCliente             := vSql.GetString('NOME_CLIENTE');
      Result[i].EmpresaId               := vSql.GetInt('EMPRESA_ID');
      Result[i].NomeEmpresa             := vSql.GetString('NOME_EMPRESA');
      Result[i].LocalId                 := vSql.GetInt('LOCAL_ID');
      Result[i].NomeLocal               := vSql.GetString('NOME_LOCAL');
      Result[i].VendedorId              := vSql.GetInt('VENDEDOR_ID');
      Result[i].NomeVendedor            := vSql.GetString('NOME_VENDEDOR');
      Result[i].DataHoraCadastro        := vSql.getData('DATA_HORA_CADASTRO');
      Result[i].QtdeProdutos            := vSql.GetDouble('QTDE_PRODUTOS');
      Result[i].PesoTotal               := vSql.GetDouble('PESO_TOTAL');
      Result[i].UsuarioCadastroId       := vSql.GetInt('USUARIO_CADASTRO_ID');
      Result[i].NomeUsuarioCadastro     := vSql.GetString('NOME_USUARIO_CADASTRO');
      Result[i].UsuarioConfirmacaoId    := vSql.GetInt('USUARIO_CONFIRMACAO_ID');
      Result[i].NomeUsuarioConfirmacao  := vSql.GetString('NOME_USUARIO_CONFIRMACAO');
      Result[i].DataHoraEntrega         := vSql.getData('DATA_HORA_ENTREGA');
      Result[i].NomePessoaRecebeu       := vSql.GetString('NOME_PESSOA_RECEBEU');

      Result[i].TipoMovimentoAnalitico :=
        _BibliotecaGenerica.Decode(Result[i].TipoMovimento, ['R', 'Retirar', 'E', 'Entregar', 'A', 'Retirar ato']);

      vSql.Next;
    end;
  end;

  vSql.Free;
end;

function BuscarItensEntregas(pConexao: TConexao; pComando: string): TArray<RecEntregasItens>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;

  vSql := TConsulta.Create( pConexao );
  vSql.Add('select ');
  vSql.Add('  TIPO_MOVIMENTO, ');
  vSql.Add('  MOVIMENTO_ID, ');
  vSql.Add('  PRODUTO_ID, ');
  vSql.Add('  NOME_PRODUTO, ');
  vSql.Add('  QUANTIDADE, ');
  vSql.Add('  LOTE, ');
  vSql.Add('  MARCA_ID, ');
  vSql.Add('  NOME_MARCA, ');
  vSql.Add('  UNIDADE_VENDA ');
  vSql.Add('from ');
  vSql.Add('  VW_ENTREGAS_ITENS ');
  vSql.Add(pComando);

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros -1 do begin
      Result[i].TipoMovimento   := vSql.GetString('TIPO_MOVIMENTO');
      Result[i].MovimentoId     := vSql.GetInt('MOVIMENTO_ID');
      Result[i].ProdutoId       := vSql.GetInt('PRODUTO_ID');
      Result[i].Nome            := vSql.GetString('NOME_PRODUTO');
      Result[i].Quantidade      := vSql.GetDouble('QUANTIDADE');
      Result[i].Lote            := vSql.GetString('LOTE');
      Result[i].MarcaId         := vSql.GetInt('MARCA_ID');
      Result[i].NomeMarca       := vSql.GetString('NOME_MARCA');
      Result[i].UnidadeVenda    := vSql.GetString('UNIDADE_VENDA');

      vSql.Next;
    end;
  end;

  vSql.Free;
end;

end.
