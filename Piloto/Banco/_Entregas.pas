unit _Entregas;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsExpedicao, _EntregasItens,
  System.Variants, _BibliotecaGenerica, Vcl.Graphics;

{$M+}
type
  RecControleEntregasIds = record
    ControleEntregaId: Integer;
    EntregaId: Integer;
  end;

  TEntregas = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordEntrega: RecEntrega;
  end;

const
  coCorRetornoParcial = $000096DB;
  coCorRetornoTotal   = clRed;
  coCorEntregaTotal   = clBlue;

function BuscarEntregas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEntrega>;

function BuscarEntregasComando(pConexao: TConexao; pComando: string): TArray<RecEntrega>;

function ExcluirEntrega(
  pConexao: TConexao;
  pEntregaId: Integer
): RecRetornoBD;

function GerarNotaEntrega(
  pConexao: TConexao;
  const pEntregaId: Integer;
  const pControlarTransacao: Boolean
): RecRetornoBD;

function ConfirmarEntrega(
  pConexao: TConexao;
  const pEntregaId: Integer;
  const pNomePessoaRetirada: string;
  const pCPFPessoaRetirada: string;
  const pDataHoraRetirada: TDateTime;
  const pObservacoes: string
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

function getAgrupamentoId(pConexao: TConexao): Integer;
function CancelarEntrega(pConexao: TConexao; const pEntregaId: Integer): RecRetornoBD;

function BaixarEntrega(
  pConexao: TConexao;
  const pEntregaId: Integer;
  pStatus: string;
  pNomePessoaReceuEntrega: string;
  pCPFPessoaReceuEntrega: string;
  pDataHoraEntrega: TDateTime;
  pObservacoes: string;
  pItens: TArray<RecEntregaItem>
): RecRetornoBD;

function AlterarStatusSeparacao(pConexao: TConexao; pEntregaId: Integer; pStatus: string): RecRetornoBD;
function IncrementarQtdeViasListaSeparacao(pConexao: TConexao; pEntregaId: Integer): RecRetornoBD;
function AtualizarConfereciaProdutos(pConexao: TConexao; pEntregaId: Integer; pItens: TArray<RecItemConferencia>): RecRetornoBD;
function BuscarEntregasIdsPorControle(pConexao: TConexao; pEntregasIds: TArray<Integer>): TArray<RecControleEntregasIds>;
function getQtdeViasImpressasComprovanteEntrega(pConexao: TConexao; pEntregaId: Integer): Integer;
function IncrementarQtdeViasCompEntregaImpressos(
  pConexao: TConexao;
  pEntregaId: Integer;
  pUtilizaControleManifesto: Boolean
): RecRetornoBD;

implementation

{ TEntregas }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 3);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ENT.ORCAMENTO_ID = :P1 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      1,
      'where ENT.ENTREGA_ID = :P1 '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      2,
      'where ENT.CONTROLE_ENTREGA_ID = :P1 '
    );
end;

constructor TEntregas.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ENTREGAS');

  FSql :=
    'select ' +
    '  ENT.ENTREGA_ID, ' +
    '  ENT.ORCAMENTO_ID, ' +
    '  ENT.USUARIO_CADASTRO_ID, ' +
    '  FUC.NOME as NOME_USUARIO_CADASTRO, ' +
    '  ENT.PREVISAO_ENTREGA, ' +
    '  ENT.DATA_HORA_CADASTRO, ' +    
    '  ENT.STATUS, ' +
    '  ENT.USUARIO_CONFIRMACAO_ID, ' +
    '  FCO.NOME as NOME_USUARIO_CONFIRMACAO, ' +
    '  ENT.DATA_HORA_REALIZOU_ENTREGA, ' +
    '  ENT.NOME_PESSOA_RECEBEU_ENTREGA, ' +
    '  ENT.CPF_PESSOA_RECEBEU_ENTREGA, ' +
    '  ENT.OBSERVACOES, ' +
    '  ORC.CLIENTE_ID, ' +
    '  CAD.RAZAO_SOCIAL as NOME_CLIENTE, ' +
    '  CAD.NOME_FANTASIA as APELIDO, ' +
    '  ORC.VENDEDOR_ID, ' +
    '  CAD.TELEFONE_PRINCIPAL, ' +
    '  CAD.TELEFONE_CELULAR, ' +
    '  VEN.NOME as NOME_VENDEDOR, ' +
    '  ENT.EMPRESA_ENTREGA_ID, ' +
    '  EET.NOME_FANTASIA as NOME_EMPRESA_ENTREGA, ' +
    '  ENT.EMPRESA_GERACAO_ID, ' +
    '  ENT.CONTROLE_ENTREGA_ID, ' +
    '  ENT.USUARIO_INICIO_SEPARACAO_ID, ' +
    '  FIS.NOME as NOME_USUARIO_INICIO_SEP, ' +
    '  ENT.DATA_HORA_INICIO_SEPARACAO, ' +
    '  ENT.USUARIO_FINALIZOU_SEP_ID, ' +
    '  FFS.NOME as NOME_USUARIO_FIN_SEP, ' +
    '  ENT.DATA_HORA_FINALIZOU_SEP, ' +
    '  EEG.NOME_FANTASIA as NOME_EMPRESA_GERACAO, ' +
    '  ORC.LOGRADOURO, ' +
    '  ORC.COMPLEMENTO, ' +
    '  ORC.NUMERO, ' +
    '  ORC.VALOR_DINHEIRO + VALOR_CARTAO_CREDITO + VALOR_CARTAO_DEBITO + VALOR_CHEQUE as VALOR_RECEBER_ENTREGA, ' +
    '  ORC.PONTO_REFERENCIA, ' +
    '  ORC.BAIRRO_ID, ' +
    '  BAI.NOME_BAIRRO, ' +
    '  BAI.NOME_CIDADE, ' +
    '  BAI.ESTADO_ID, ' +
    '  ORC.CEP, ' +
    '  ENT.LOCAL_ID, ' +
    '  LOC.NOME as NOME_LOCAL, ' +
    '  BAI.ROTA_ID, ' +
    '  BAI.NOME_ROTA, ' +
    '  ITE.VALOR_ENTREGA, ' +
    '  ITE.QTDE_PRODUTOS, ' +
    '  ITE.PESO_TOTAL, ' +
    '  ORC.CONDICAO_ID, ' +
    '  CON.NOME as NOME_CONDICAO_PAGAMENTO, ' +
    '  ORC.RECEBER_NA_ENTREGA, ' +
    '  ORC.OBSERVACOES_EXPEDICAO ' +
    'from ' +
    '  ENTREGAS ENT ' +

    'inner join ORCAMENTOS ORC ' +
    'on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID ' +

    'inner join CADASTROS CAD ' +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID ' +

    'inner join FUNCIONARIOS FUC ' +
    'on ENT.USUARIO_CADASTRO_ID = FUC.FUNCIONARIO_ID ' +

    'inner join FUNCIONARIOS VEN ' +
    'on ORC.VENDEDOR_ID = VEN.FUNCIONARIO_ID ' +

    'inner join EMPRESAS EET ' +
    'on ENT.EMPRESA_ENTREGA_ID = EET.EMPRESA_ID ' +

    'inner join EMPRESAS EEG ' +
    'on ENT.EMPRESA_GERACAO_ID = EEG.EMPRESA_ID ' +

    'inner join VW_BAIRROS BAI ' +
    'on ORC.BAIRRO_ID = BAI.BAIRRO_ID ' +

    'inner join LOCAIS_PRODUTOS LOC ' +
    'on ENT.LOCAL_ID = LOC.LOCAL_ID ' +

    'inner join CONDICOES_PAGAMENTO CON ' +
    'on ORC.CONDICAO_ID = CON.CONDICAO_ID ' +

    'inner join ( ' +
    '  select ' +
    '    ENT.ENTREGA_ID, ' +
    '    round(ORC.VALOR_TOTAL * sum(OIT.VALOR_TOTAL / OIT.QUANTIDADE * ITE.QUANTIDADE) / ORC.VALOR_TOTAL_PRODUTOS, 2) as VALOR_ENTREGA, ' +
    '    count(*) as QTDE_PRODUTOS, ' +
    '    sum(ITE.QUANTIDADE * PRO.PESO) as PESO_TOTAL ' +
    '  from ' +
    '    ENTREGAS ENT ' +

    '  inner join ENTREGAS_ITENS ITE ' +
    '  on ENT.ENTREGA_ID = ITE.ENTREGA_ID ' +

    '  inner join ORCAMENTOS ORC ' +
    '  on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID ' +

    '  inner join ORCAMENTOS_ITENS OIT ' +
    '  on ORC.ORCAMENTO_ID = OIT.ORCAMENTO_ID ' +
    '  and ITE.ITEM_ID = OIT.ITEM_ID ' +

    '  inner join PRODUTOS PRO ' +
    '  on ITE.PRODUTO_ID = PRO.PRODUTO_ID ' +
    '  group by ' +
    '    ENT.ENTREGA_ID, ' +
    '    ORC.VALOR_TOTAL,  ' +
    '    ORC.VALOR_TOTAL_PRODUTOS ' +
    ') ITE ' +
    'on ENT.ENTREGA_ID = ITE.ENTREGA_ID ' +

    'left join FUNCIONARIOS FCO ' +
    'on ENT.USUARIO_CONFIRMACAO_ID = FCO.FUNCIONARIO_ID ' +

    'left join FUNCIONARIOS FIS ' +
    'on ENT.USUARIO_INICIO_SEPARACAO_ID = FIS.FUNCIONARIO_ID ' +

    'left join FUNCIONARIOS FFS ' +
    'on ENT.USUARIO_FINALIZOU_SEP_ID = FFS.FUNCIONARIO_ID ';

  setFiltros(getFiltros);

  AddColunaSL('ENTREGA_ID');
  AddColunaSL('ORCAMENTO_ID');
  AddColunaSL('USUARIO_CADASTRO_ID');
  AddColunaSL('NOME_USUARIO_CADASTRO');
  AddColunaSL('PREVISAO_ENTREGA');
  AddColunaSL('DATA_HORA_CADASTRO');
  AddColunaSL('STATUS');
  AddColunaSL('DATA_HORA_REALIZOU_ENTREGA');
  AddColunaSL('NOME_PESSOA_RECEBEU_ENTREGA');
  AddColunaSL('CPF_PESSOA_RECEBEU_ENTREGA');
  AddColunaSL('OBSERVACOES');
  AddColunaSL('CLIENTE_ID');
  AddColunaSL('NOME_CLIENTE');
  AddColunaSL('APELIDO');
  AddColunaSL('VENDEDOR_ID');
  AddColunaSL('TELEFONE_PRINCIPAL');
  AddColunaSL('TELEFONE_CELULAR');
  AddColunaSL('NOME_VENDEDOR');
  AddColunaSL('USUARIO_CONFIRMACAO_ID');
  AddColunaSL('NOME_USUARIO_CONFIRMACAO');
  AddColunaSL('EMPRESA_ENTREGA_ID');
  AddColunaSL('NOME_EMPRESA_ENTREGA');
  AddColunaSL('EMPRESA_GERACAO_ID');
  AddColunaSL('NOME_EMPRESA_GERACAO');
  AddColunaSL('LOGRADOURO');
  AddColunaSL('COMPLEMENTO');
  AddColunaSL('NUMERO');
  AddColunaSL('PONTO_REFERENCIA');
  AddColunaSL('VALOR_RECEBER_ENTREGA');
  AddColunaSL('BAIRRO_ID');
  AddColunaSL('NOME_BAIRRO');
  AddColunaSL('NOME_CIDADE');
  AddColunaSL('ESTADO_ID');
  AddColunaSL('LOCAL_ID');
  AddColunaSL('NOME_LOCAL');
  AddColunaSL('ROTA_ID');
  AddColunaSL('NOME_ROTA');
  AddColunaSL('VALOR_ENTREGA');
  AddColunaSL('QTDE_PRODUTOS');
  AddColunaSL('PESO_TOTAL');
  AddColunaSL('CONDICAO_ID');
  AddColunaSL('CONTROLE_ENTREGA_ID');
  AddColunaSL('USUARIO_INICIO_SEPARACAO_ID');
  AddColunaSL('NOME_USUARIO_INICIO_SEP');
  AddColunaSL('DATA_HORA_INICIO_SEPARACAO');
  AddColunaSL('USUARIO_FINALIZOU_SEP_ID');
  AddColunaSL('NOME_USUARIO_FIN_SEP');
  AddColunaSL('DATA_HORA_FINALIZOU_SEP');
  AddColunaSL('NOME_CONDICAO_PAGAMENTO');
  AddColunaSL('RECEBER_NA_ENTREGA');
  AddColunaSL('OBSERVACOES_EXPEDICAO');
end;

function TEntregas.getRecordEntrega: RecEntrega;
begin
  Result.EntregaId                := getInt('ENTREGA_ID');
  Result.OrcamentoId              := getInt('ORCAMENTO_ID');
  Result.UsuarioCadastroId        := getInt('USUARIO_CADASTRO_ID');
  Result.NomeUsuarioCadastro      := getString('NOME_USUARIO_CADASTRO');
  Result.DataHoraCadastro         := getData('DATA_HORA_CADASTRO');
  Result.PrevisaoEntrega          := getData('PREVISAO_ENTREGA');
  Result.Status                   := getString('STATUS');

  Result.StatusAnalitico :=
    _Biblioteca.Decode(
      Result.Status, [
      'AGS', 'Aguard. separa��o',
      'ESE', 'Em separa��o',
      'AGC', 'Aguar. carregamento',
      'ERO', 'Em rota de entrega',
      'ENT', 'Entregue',
      'RPA', 'Retorn. parcial',
      'RTO', 'Retornado total'
      ]
    );

  Result.DataHoraRealizouEntrega  := getData('DATA_HORA_REALIZOU_ENTREGA');
  Result.NomePessoaRecebeuEntrega := getString('NOME_PESSOA_RECEBEU_ENTREGA');
  Result.CpfPessoaRecebeuEntrega  := getString('CPF_PESSOA_RECEBEU_ENTREGA');
  Result.Observacoes              := getString('OBSERVACOES');
  Result.ClienteId                := getInt('CLIENTE_ID');
  Result.NomeCliente              := getString('NOME_CLIENTE');
  Result.Apelido                  := getString('APELIDO');
  Result.VendedorId               := getInt('VENDEDOR_ID');
  Result.TelefonePrincipal        := getString('TELEFONE_PRINCIPAL');
  Result.TelefoneCelular          := getString('TELEFONE_CELULAR');
  Result.NomeVendedor             := getString('NOME_VENDEDOR');
  Result.UsuarioConfirmacaoId     := getInt('USUARIO_CADASTRO_ID');
  Result.EmpresaId                := getInt('EMPRESA_ENTREGA_ID');
  Result.NomeEmpresa              := getString('NOME_EMPRESA_ENTREGA');
  Result.EmpresaGeracaoId         := getInt('EMPRESA_GERACAO_ID');
  Result.NomeEmpresaGeracao       := getString('NOME_EMPRESA_GERACAO');
  Result.Logradouro               := getString('LOGRADOURO');
  Result.Complemento              := getString('COMPLEMENTO');
  Result.Numero                   := getString('NUMERO');
  Result.PontoReferencia          := getString('PONTO_REFERENCIA');
  Result.ValorReceberEntrega      := getDouble('VALOR_RECEBER_ENTREGA');
  Result.BairroId                 := getInt('BAIRRO_ID');
  Result.NomeBairro               := getString('NOME_BAIRRO');
  Result.Cidade                   := getString('NOME_CIDADE');
  Result.UF                       := getString('ESTADO_ID');
  Result.LocalId                  := getInt('LOCAL_ID');
  Result.NomeLocal                := getString('NOME_LOCAL');
  Result.RotaId                   := getInt('ROTA_ID');
  Result.NomeRota                 := getString('NOME_ROTA');
  Result.ValorEntrega             := getDouble('VALOR_ENTREGA');
  Result.QtdeProdutos             := getInt('QTDE_PRODUTOS');
  Result.PesoTotal                := getDouble('PESO_TOTAL');
  Result.CondicaoId               := getInt('CONDICAO_ID');
  Result.ControleEntregaId        := getInt('CONTROLE_ENTREGA_ID');
  Result.UsuarioInicioSeparaoId   := getInt('USUARIO_INICIO_SEPARACAO_ID');
  Result.NomeUsuarioInicioSep     := getString('NOME_USUARIO_INICIO_SEP');
  Result.DataHoraInicioSeparacao  := getData('DATA_HORA_INICIO_SEPARACAO');
  Result.UsuarioFinalizouSepId    := getInt('USUARIO_FINALIZOU_SEP_ID');
  Result.NomeUsuarioFinSep        := getString('NOME_USUARIO_FIN_SEP');
  Result.DataHoraFinalizouSep     := getData('DATA_HORA_FINALIZOU_SEP');
  Result.NomeCondicaoPagamento    := getString('NOME_CONDICAO_PAGAMENTO');
  Result.ReceberNaEntrega         := getString('RECEBER_NA_ENTREGA');
  Result.ObservacoesExpedicao     := getString('OBSERVACOES_EXPEDICAO');
end;

function BuscarEntregas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEntrega>;
var
  i: Integer;
  t: TEntregas;
begin
  Result := nil;
  t := TEntregas.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordEntrega;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarEntregasComando(pConexao: TConexao; pComando: string): TArray<RecEntrega>;
var
  i: Integer;
  t: TEntregas;
begin
  Result := nil;
  t := TEntregas.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordEntrega;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirEntrega(
  pConexao: TConexao;
  pEntregaId: Integer
): RecRetornoBD;
var
  t: TEntregas;
begin
  Result.TeveErro := False;
  t := TEntregas.Create(pConexao);

  try
    t.setInt('ENTREGA_ID', pEntregaId, True);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function GerarNotaEntrega(
  pConexao: TConexao;
  const pEntregaId: Integer;
  const pControlarTransacao: Boolean
): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;

  try
    if pControlarTransacao then
      pConexao.IniciarTransacao;

    vProc := TProcedimentoBanco.Create(pConexao, 'GERAR_NOTA_ENTREGA');

    vProc.Params[0].AsInteger := pEntregaId;
    vProc.Executar;

    if pControlarTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if pControlarTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;
  FreeAndNil(vProc);
end;

function ConfirmarEntrega(
  pConexao: TConexao;
  const pEntregaId: Integer;
  const pNomePessoaRetirada: string;
  const pCPFPessoaRetirada: string;
  const pDataHoraRetirada: TDateTime;
  const pObservacoes: string
): RecRetornoBD;
var
  vExec: TExecucao;
  vProc: TProcedimentoBanco;
begin
  Result.TeveErro := False;
  vExec := TExecucao.Create(pConexao);
  pConexao.SetRotina('CONFIRMAR_ENTREGA');
  vProc := TProcedimentoBanco.Create(pConexao, 'CONFIRMAR_ENTREGA');
  try
    pConexao.IniciarTransacao;

    vExec.SQL.Add('update ENTREGAS set ');
    vExec.SQL.Add('  STATUS = ''ENT'', ');
    vExec.SQL.Add('  NOME_PESSOA_RECEBEU_ENTREGA = :P2, ');
    vExec.SQL.Add('  CPF_PESSOA_RECEBEU_ENTREGA = :P3, ');
    vExec.SQL.Add('  DATA_HORA_REALIZOU_ENTREGA = :P4, ');
    vExec.SQL.Add('  OBSERVACOES = :P5 ');
    vExec.SQL.Add('where ENTREGA_ID = :P1 ');

    vExec.Executar([pEntregaId, pNomePessoaRetirada, pCPFPessoaRetirada, _Biblioteca.nvl(pDataHoraRetirada, null), pObservacoes]);

    vProc.Params[0].AsInteger := pEntregaId;

    vProc.ExecProc;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vExec.Free;
  vProc.Free;
end;

function getAgrupamentoId(pConexao: TConexao): Integer;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select SEQ_AGRUP_ENTREGAS_ID.nextval from dual');
  vSql.Pesquisar;
  Result := vSql.GetInt(0);

  vSql.Free;
end;

function CancelarEntrega(pConexao: TConexao; const pEntregaId: Integer): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  pConexao.setRotina('CANCELAR_ENTREGA');
  Result.TeveErro := False;
  vProc := TProcedimentoBanco.Create(pConexao, 'CANCELAR_ENTREGA');
  try
    pConexao.IniciarTransacao;

    vProc.Params[0].AsInteger := pEntregaId;
    vProc.ExecProc;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  vProc.Free;
end;

function BaixarEntrega(
  pConexao: TConexao;
  const pEntregaId: Integer;
  pStatus: string;
  pNomePessoaReceuEntrega: string;
  pCPFPessoaReceuEntrega: string;
  pDataHoraEntrega: TDateTime;
  pObservacoes: string;
  pItens: TArray<RecEntregaItem>
): RecRetornoBD;
var
  i: Integer;
  vProc: TProcedimentoBanco;
  vExec: TExecucao;

  vProdutoIds: array of Integer;
  vItensIds: array of Integer;
  vQuantidades: array of Double;
  vLotes: array of string;
begin
  vExec := TExecucao.Create(pConexao);
  pConexao.SetRotina('BAIXAR_ENTREGA');
  vProc := TProcedimentoBanco.Create(pConexao, 'BAIXAR_ENTREGA');

  try
    SetLength(vProdutoIds, Length(pItens));
    SetLength(vItensIds, Length(pItens));
    SetLength(vQuantidades, Length(pItens));
    SetLength(vLotes, Length(pItens));

    for i := Low(pItens) to High(pItens) do begin
      vProdutoIds[i]  := pItens[i].ProdutoId;
      vItensIds[i]    := pItens[i].ItemId;
      vQuantidades[i] := pItens[i].QtdeRetornar;
      vLotes[i]       := pItens[i].Lote;
    end;

    if _Biblioteca.Em(pStatus, ['RPA', 'ENT']) then begin
      vExec.SQL.Add('update ENTREGAS set ');
      vExec.SQL.Add('  STATUS = :P2, ');
      vExec.SQL.Add('  NOME_PESSOA_RECEBEU_ENTREGA = :P3, ');
      vExec.SQL.Add('  CPF_PESSOA_RECEBEU_ENTREGA = :P4, ');
      vExec.SQL.Add('  DATA_HORA_REALIZOU_ENTREGA = :P5, ');
      vExec.SQL.Add('  OBSERVACOES = :P6 ');
      vExec.SQL.Add('where ENTREGA_ID = :P1 ');

      vExec.Executar([pEntregaId, pStatus, pNomePessoaReceuEntrega, pCPFPessoaReceuEntrega, _Biblioteca.nvl(pDataHoraEntrega, null), pObservacoes]);
    end
    else begin
      vExec.SQL.Add('update ENTREGAS set ');
      vExec.SQL.Add('  STATUS = :P2, ');
      vExec.SQL.Add('  OBSERVACOES = :P3 ');
      vExec.SQL.Add('where ENTREGA_ID = :P1 ');

      vExec.Executar([pEntregaId, pStatus, pObservacoes]);
    end;

    vProc.Params[0].AsInteger := pEntregaId;
    vProc.Params[1].AsString  := pStatus;

    (* Produtos *)
    vProc.Params[2].Value := vProdutoIds;
    vProc.Params[3].Value := vItensIds;
    vProc.Params[4].Value := vLotes;
    vProc.Params[5].Value := vQuantidades;

    vProc.Executar;
  except
    on e: Exception do
      Result.TratarErro(e);
  end;

  vExec.Free;
  vProc.Free;
end;

function AlterarStatusSeparacao(pConexao: TConexao; pEntregaId: Integer; pStatus: string): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ALTERAR_STATUS_SEP_ENTREGA');

  vExec := TExecucao.Create(pConexao);

  vExec.Add('update ENTREGAS set ');
  vExec.Add('  STATUS = :P2 ');
  vExec.Add('where ENTREGA_ID = :P1 ');

  try
    pConexao.IniciarTransacao;

    vExec.Executar([pEntregaId, pStatus]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
end;

function IncrementarQtdeViasListaSeparacao(pConexao: TConexao; pEntregaId: Integer): RecRetornoBD;
var
  vExe: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATU_QTDE_VIAS_IMP_LISTA_SEP');

  vExe := TExecucao.Create(pConexao);

  vExe.Add('update ENTREGAS set ');
  vExe.Add('  QTDE_VIAS_IMPRESSAS_LISTA_SEP = QTDE_VIAS_IMPRESSAS_LISTA_SEP + 1 ');
  vExe.Add('where ENTREGA_ID = :P1');

  try
    pConexao.IniciarTransacao;

    vExe.Executar([pEntregaId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  vExe.Free;
end;

function AtualizarConfereciaProdutos(pConexao: TConexao; pEntregaId: Integer; pItens: TArray<RecItemConferencia>): RecRetornoBD;
var
  i: Integer;
  vProc: TProcedimentoBanco;

  vProdutoIds: array of Integer;
  vItensIds: array of Integer;
  vQuantidades: array of Double;
  vLotes: array of string;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATU_CONF_SAIDA_ENTREGA');
  vProc := TProcedimentoBanco.Create(pConexao, 'CONSOLIDAR_CONF_SAIDA_ENTREGA');

  try
    SetLength(vProdutoIds, Length(pItens));
    SetLength(vItensIds, Length(pItens));
    SetLength(vQuantidades, Length(pItens));
    SetLength(vLotes, Length(pItens));

    for i := Low(pItens) to High(pItens) do begin
      vProdutoIds[i]  := pItens[i].ProdutoId;
      vItensIds[i]    := pItens[i].ItemId;
      vQuantidades[i] := pItens[i].NaoSeparados;
      vLotes[i]       := pItens[i].Lote;
    end;

    pConexao.IniciarTransacao;

    vProc.Params[0].AsInteger := pEntregaId;

    (* Produtos *)
    vProc.Params[1].Value := vProdutoIds;
    vProc.Params[2].Value := vItensIds;
    vProc.Params[3].Value := vLotes;
    vProc.Params[4].Value := vQuantidades;

    vProc.Executar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vProc.Free;
end;

function BuscarEntregasIdsPorControle(pConexao: TConexao; pEntregasIds: TArray<Integer>): TArray<RecControleEntregasIds>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  ENTREGA_ID, ');
  vSql.Add('  CONTROLE_ENTREGA_ID ');
  vSql.Add('from ');
  vSql.Add('  CONTROLES_ENTREGAS_ITENS ');
  vSql.Add('where ' + FiltroInInt('CONTROLE_ENTREGA_ID', pEntregasIds));

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros -1 do begin
      Result[i].EntregaId         := vSql.GetInt(0);
      Result[i].ControleEntregaId := vSql.GetInt(1);

      vSql.Next;
    end;
  end;

  vSql.Free;
end;

function getQtdeViasImpressasComprovanteEntrega(pConexao: TConexao; pEntregaId: Integer): Integer;
var
  vSql: TConsulta;
begin
  Result := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  QTDE_VIAS_IMPRESSAS_COMP_ENT ');
  vSql.Add('from ');
  vSql.Add('  ENTREGAS ');
  vSql.Add('where ENTREGA_ID = :P1');

  if vSql.Pesquisar([pEntregaId]) then
    Result := vSql.GetInt(0);

  vSql.Free;
end;

function IncrementarQtdeViasCompEntregaImpressos(
  pConexao: TConexao;
  pEntregaId: Integer;
  pUtilizaControleManifesto: Boolean
): RecRetornoBD;
var
  vExe: TExecucao;
begin
  Result.Iniciar;
  if pUtilizaControleManifesto then
    pConexao.SetRotina('ATU_QTDE_VIAS_IMP_COMP_ENTREGA')
  else
    pConexao.SetRotina('ATU_QTDE_VIAS_IMP_COMP_ENT_S_M');

  vExe := TExecucao.Create(pConexao);

  vExe.Add('update ENTREGAS set ');
  vExe.Add('  QTDE_VIAS_IMPRESSAS_COMP_ENT = QTDE_VIAS_IMPRESSAS_COMP_ENT + 1 ');
  vExe.Add('where ENTREGA_ID = :P1');

  try
    pConexao.IniciarTransacao;

    vExe.Executar([pEntregaId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  vExe.Free;
end;

end.
