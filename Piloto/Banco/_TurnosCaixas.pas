unit _TurnosCaixas;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCaixa, _BibliotecaGenerica;

{$M+}
type
  TTurnosCaixas = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
    function getRecordTurnosCaixas: RecTurnosCaixas;
  end;

function AtualizarTurnosCaixas(
  pConexao: TConexao;
  pTurnoId: Integer;
  pEmpresaId: Integer;
  pFuncionarioId: Integer;
  pValorInicial: Double;
  pContaOrigemDinIniId: string;
  pValorDinheiroInfFechamento: Double
): RecRetornoBD;

function BuscarTurnosCaixas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTurnosCaixas>;

function ExcluirTurnosCaixas(
  pConexao: TConexao;
  pTurnoId: Integer
): RecRetornoBD;

function BuscarInformacoesTurno(pConexao: TConexao; const pComando: string): TArray<RecInformacoesTurno>;

function LancarDiferencaFechamentoCaixa(pConexao: TConexao; const pTurnoId: Integer; const pValorDiferenca: Double): RecRetornoBD;
function ExisteCartoesTurno(pConexao: TConexao; pTurnoId: Integer; const pBancoCaixaId: Integer): Boolean;
function getTurnoId( pConexao: TConexao; pFuncionarioId: Integer ): Integer;

function getFiltros: TArray<RecFiltros>;

implementation

{ TTurnosCaixas }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 3);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where TUR.TURNO_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where TUR.FUNCIONARIO_ID = :P1 ' +
      'and TUR.STATUS = ''A'' '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where TUR.FUNCIONARIO_ID = :P1 ' +
      'and TUR.EMPRESA_ID = :P2 ' +
      'and TUR.STATUS = ''A'' '
    );
end;

constructor TTurnosCaixas.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'TURNOS_CAIXA');

  FSql :=
    'select ' +
    '  TUR.TURNO_ID, ' +
    '  TUR.EMPRESA_ID, ' +
    '  TUR.FUNCIONARIO_ID, ' +
    '  TUR.VALOR_INICIAL, ' +
    '  TUR.DATA_HORA_ABERTURA, ' +
    '  TUR.DATA_HORA_FECHAMENTO, ' +
    '  TUR.CONTA_ORIGEM_DIN_INI_ID, ' +
    '  MOV.VALOR_DINHEIRO, ' +
    '  MOV.VALOR_PIX, ' +
    '  MOV.VALOR_SAIDAS_DINHEIRO, ' +
    '  MOV.VALOR_CARTAO, ' +
    '  MOV.VALOR_CHEQUE, ' +
    '  MOV.VALOR_COBRANCA, ' +
    '  MOV.VALOR_CREDITO, ' +
    '  TUR.VALOR_DINHEIRO_INF_FECHAMENTO ' +
    'from ' +
    '  TURNOS_CAIXA TUR ' +

    'left join( ' +
    '  select ' +
    '    TURNO_ID, ' +
    '    sum(case when TIPO_MOVIMENTO in(''REV'', ''ABE'', ''SUP'', ''BXR'') then VALOR_DINHEIRO else 0 end) as VALOR_DINHEIRO, ' +
    '    sum(case when TIPO_MOVIMENTO in(''REV'') then VALOR_PIX else 0 end) as VALOR_PIX, ' +
    '    sum(case when TIPO_MOVIMENTO in(''BXP'', ''SAN'') then VALOR_DINHEIRO else 0 end) as VALOR_SAIDAS_DINHEIRO, ' +
    '    sum(case when TIPO_MOVIMENTO in(''REV'') then VALOR_CARTAO else 0 end) as VALOR_CARTAO, ' +
    '    sum(case when TIPO_MOVIMENTO in(''REV'') then VALOR_CHEQUE else 0 end) as VALOR_CHEQUE, ' +
    '    sum(case when TIPO_MOVIMENTO in(''REV'') then VALOR_COBRANCA else 0 end) as VALOR_COBRANCA, ' +
    '    sum(case when TIPO_MOVIMENTO in(''REV'') then VALOR_CREDITO else 0 end) as VALOR_CREDITO ' +
    '  from ' +
    '    VW_MOVIMENTOS_TURNOS ' +
    '  group by ' +
    '    TURNO_ID ' +
    ') MOV ' +
    'on TUR.TURNO_ID = MOV.TURNO_ID ';

  setFiltros(getFiltros);

  AddColuna('TURNO_ID', True);
  AddColuna('EMPRESA_ID');
  AddColuna('FUNCIONARIO_ID');
  AddColuna('VALOR_INICIAL');
  AddColunaSL('DATA_HORA_ABERTURA');
  AddColuna('DATA_HORA_FECHAMENTO');
  AddColuna('CONTA_ORIGEM_DIN_INI_ID');
  AddColunaSL('VALOR_DINHEIRO');
  AddColunaSL('VALOR_PIX');
  AddColunaSL('VALOR_SAIDAS_DINHEIRO');
  AddColunaSL('VALOR_CARTAO');
  AddColunaSL('VALOR_CHEQUE');
  AddColunaSL('VALOR_COBRANCA');
  AddColunaSL('VALOR_CREDITO');
  AddColuna('VALOR_DINHEIRO_INF_FECHAMENTO');
end;

function TTurnosCaixas.getRecordTurnosCaixas: RecTurnosCaixas;
begin
  Result := RecTurnosCaixas.Create;
  Result.TurnoId                       := getInt('TURNO_ID', True);
  Result.EmpresaId                     := getInt('EMPRESA_ID');
  Result.FuncionarioId                 := getInt('FUNCIONARIO_ID');
  Result.valor_inicial                 := getDouble('VALOR_INICIAL');
  Result.valor_dinheiro                := getDouble('VALOR_DINHEIRO');
  Result.valor_pix                     := getDouble('VALOR_PIX');
  Result.ValorSaidasDinheiro           := getDouble('VALOR_SAIDAS_DINHEIRO');
  Result.valor_cheque                  := getDouble('VALOR_CHEQUE');
  Result.valor_cartao                  := getDouble('VALOR_CARTAO');
  Result.valor_cobranca                := getDouble('VALOR_COBRANCA');
  Result.valor_credito                 := getDouble('VALOR_CREDITO');
  Result.data_hora_abertura            := getData('DATA_HORA_ABERTURA');
  Result.data_hora_fechamento          := getData('DATA_HORA_FECHAMENTO');
  Result.BancoCaixaOrigemDinIniId      := getString('CONTA_ORIGEM_DIN_INI_ID');
  Result.valor_dinheiro_inf_fechamento := getDouble('VALOR_DINHEIRO_INF_FECHAMENTO');
end;

function AtualizarTurnosCaixas(
  pConexao: TConexao;
  pTurnoId: Integer;
  pEmpresaId: Integer;
  pFuncionarioId: Integer;
  pValorInicial: Double;
  pContaOrigemDinIniId: string;
  pValorDinheiroInfFechamento: Double
): RecRetornoBD;
var
  t: TTurnosCaixas;
  novo: Boolean;
  seq: TSequencia;
begin
  Result.TeveErro := False;
  t := TTurnosCaixas.Create(pConexao);

  novo := pTurnoId = 0;

  if novo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_TURNO_ID');
    pTurnoId := seq.getProximaSequencia;
    result.AsInt := pTurnoId;
    seq.Free;
  end;

  try
    pConexao.IniciarTransacao;

    t.setInt('TURNO_ID', pTurnoId, True);
    t.setInt('EMPRESA_ID', pEmpresaId);
    t.setInt('FUNCIONARIO_ID', pFuncionarioId);
    t.setDouble('VALOR_INICIAL', pValorInicial);
    t.setStringN('CONTA_ORIGEM_DIN_INI_ID', pContaOrigemDinIniId);
    t.setDoubleN('VALOR_DINHEIRO_INF_FECHAMENTO', pValorDinheiroInfFechamento);

    if novo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function BuscarTurnosCaixas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTurnosCaixas>;
var
  i: Integer;
  t: TTurnosCaixas;
begin
  Result := nil;
  t := TTurnosCaixas.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordTurnosCaixas;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirTurnosCaixas(
  pConexao: TConexao;
  pTurnoId: Integer
): RecRetornoBD;
var
  t: TTurnosCaixas;
begin
  Result.TeveErro := False;
  t := TTurnosCaixas.Create(pConexao);

  t.setInt('TURNO_ID', pTurnoId, True);

  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  t.Free;
end;

function BuscarInformacoesTurno(pConexao: TConexao; const pComando: string): TArray<RecInformacoesTurno>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  TUR.TURNO_ID, ');
  vSql.Add('  TUR.EMPRESA_ID, ');
  vSql.Add('  TUR.STATUS, ');
  vSql.Add('  EMP.NOME_FANTASIA as NOME_EMPRESA, ');
  vSql.Add('  TUR.FUNCIONARIO_ID, ');
  vSql.Add('  FUN.NOME as NOME_CAIXA, ');
  vSql.Add('  TUR.VALOR_INICIAL, ');
  vSql.Add('  TUR.DATA_HORA_ABERTURA, ');
  vSql.Add('  TUR.USUARIO_ABERTURA_ID, ');
  vSql.Add('  USA.NOME as NOME_USUARIO_ABERTURA, ');
  vSql.Add('  TUR.DATA_HORA_FECHAMENTO, ');
  vSql.Add('  TUR.USUARIO_FECHAMENTO_ID, ');
  vSql.Add('  USF.NOME as NOME_USUARIO_FECHAMENTO, ');
  vSql.Add('  TUR.CONTA_ORIGEM_DIN_INI_ID, ');
  vSql.Add('  CON.NOME as NOME_CONTA_ORIGEM, ');

  vSql.Add('  MOV.VALOR_DINHEIRO, ');
  vSql.Add('  MOV.VALOR_PIX, ');
  vSql.Add('  MOV.VALOR_CARTAO, ');
  vSql.Add('  MOV.VALOR_CHEQUE, ');
  vSql.Add('  MOV.VALOR_COBRANCA, ');
  vSql.Add('  MOV.VALOR_ACUMULADO, ');
  vSql.Add('  MOV.VALOR_CREDITO, ');

  vSql.Add('  MOV.VALOR_PAGTOS_DINHEIRO, ');
  vSql.Add('  MOV.VALOR_PAGTOS_CREDITO, ');

  vSql.Add('  MOV.VALOR_SANGRIA_DINHEIRO, ');
  vSql.Add('  MOV.VALOR_SANGRIA_CARTAO, ');
  vSql.Add('  MOV.VALOR_SANGRIA_CHEQUE, ');
  vSql.Add('  MOV.VALOR_SANGRIA_COBRANCA, ');

  vSql.Add('  MOV.VALOR_SUPRIMENTO, ');
  vSql.Add('  TUR.VALOR_DINHEIRO_INF_FECHAMENTO, ');
  vSql.Add('  TUR.VALOR_CHEQUE_INF_FECHAMENTO, ');
  vSql.Add('  TUR.VALOR_CARTAO_INF_FECHAMENTO, ');
  vSql.Add('  TUR.VALOR_COBRANCA_INF_FECHAMENTO ');
  vSql.Add('from ');
  vSql.Add('  TURNOS_CAIXA TUR ');

  vSql.Add('inner join FUNCIONARIOS FUN ');
  vSql.Add('on TUR.FUNCIONARIO_ID = FUN.FUNCIONARIO_ID ');

  vSql.Add('inner join FUNCIONARIOS USA ');
  vSql.Add('on TUR.USUARIO_ABERTURA_ID = USA.FUNCIONARIO_ID ');

  vSql.Add('left join FUNCIONARIOS USF ');
  vSql.Add('on TUR.USUARIO_FECHAMENTO_ID = USF.FUNCIONARIO_ID ');

  vSql.Add('inner join EMPRESAS EMP ');
  vSql.Add('on TUR.EMPRESA_ID = EMP.EMPRESA_ID ');

  vSql.Add('left join CONTAS CON ');
  vSql.Add('on TUR.CONTA_ORIGEM_DIN_INI_ID = CON.CONTA ');

  vSql.Add('left join( ');
  vSql.Add('  select ');
  vSql.Add('    TURNO_ID, ');
  vSql.Add('    sum(case when TIPO_MOVIMENTO in(''REV'', ''BXR'', ''ACU'') then VALOR_DINHEIRO else 0 end) as VALOR_DINHEIRO, ');
  vSql.Add('    sum(case when TIPO_MOVIMENTO in(''REV'', ''BXR'', ''ACU'') then VALOR_PIX else 0 end) as VALOR_PIX, ');
  vSql.Add('    sum(case when TIPO_MOVIMENTO in(''REV'', ''BXR'', ''ACU'') then VALOR_CARTAO else 0 end) as VALOR_CARTAO, ');
  vSql.Add('    sum(case when TIPO_MOVIMENTO in(''REV'', ''BXR'', ''ACU'') then VALOR_CHEQUE else 0 end) as VALOR_CHEQUE, ');
  vSql.Add('    sum(case when TIPO_MOVIMENTO in(''REV'', ''BXR'', ''ACU'') then VALOR_COBRANCA else 0 end) as VALOR_COBRANCA, ');
  vSql.Add('    sum(case when TIPO_MOVIMENTO in(''REV'') then VALOR_ACUMULADO else 0 end) as VALOR_ACUMULADO, ');
  vSql.Add('    sum(case when TIPO_MOVIMENTO in(''REV'', ''BXR'', ''ACU'') then VALOR_CREDITO else 0 end) as VALOR_CREDITO, ');

  vSql.Add('    sum(case when TIPO_MOVIMENTO in(''BXP'') then VALOR_DINHEIRO else 0 end) as VALOR_PAGTOS_DINHEIRO, ');
  vSql.Add('    sum(case when TIPO_MOVIMENTO in(''BXP'') then VALOR_CREDITO else 0 end) as VALOR_PAGTOS_CREDITO, ');

  vSql.Add('    sum(case when TIPO_MOVIMENTO = ''SUP'' then VALOR_DINHEIRO else 0 end) as VALOR_SUPRIMENTO, ');
  vSql.Add('    sum(case when TIPO_MOVIMENTO = ''SAN'' then VALOR_DINHEIRO else 0 end) as VALOR_SANGRIA_DINHEIRO, ');
  vSql.Add('    sum(case when TIPO_MOVIMENTO = ''SAN'' then VALOR_CARTAO else 0 end) as VALOR_SANGRIA_CARTAO, ');
  vSql.Add('    sum(case when TIPO_MOVIMENTO = ''SAN'' then VALOR_CHEQUE else 0 end) as VALOR_SANGRIA_CHEQUE, ');
  vSql.Add('    sum(case when TIPO_MOVIMENTO = ''SAN'' then VALOR_COBRANCA else 0 end) as VALOR_SANGRIA_COBRANCA ');

  vSql.Add('  from ');
  vSql.Add('    VW_MOVIMENTOS_TURNOS ');
  vSql.Add('  group by ');
  vSql.Add('    TURNO_ID ');
  vSql.Add(') MOV ');
  vSql.Add('on TUR.TURNO_ID = MOV.TURNO_ID ');

  vSql.Add(pComando);

  if vSql.Pesquisar() then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].TurnoId                    := vSql.GetInt('TURNO_ID');
      Result[i].EmpresaId                  := vSql.GetInt('EMPRESA_ID');
      Result[i].Status                     := vSql.GetString('STATUS');
      Result[i].NomeEmpresa                := vSql.GetString('NOME_EMPRESA');
      Result[i].FuncionarioId              := vSql.GetInt('FUNCIONARIO_ID');
      Result[i].NomeCaixa                  := vSql.GetString('NOME_CAIXA');
      Result[i].ValorInicial               := vSql.GetDouble('VALOR_INICIAL');
      Result[i].DataHoraAbertura           := vSql.GetData('DATA_HORA_ABERTURA');
      Result[i].UsuarioAberturaId          := vSql.GetInt('USUARIO_ABERTURA_ID');
      Result[i].NomeUsuarioAbertura        := vSql.GetString('NOME_USUARIO_ABERTURA');
      Result[i].DataHoraFechamento         := vSql.GetData('DATA_HORA_FECHAMENTO');
      Result[i].UsuarioFechamentoId        := vSql.GetInt('USUARIO_FECHAMENTO_ID');
      Result[i].NomeUsuarioFechamento      := vSql.GetString('NOME_USUARIO_FECHAMENTO');
      Result[i].ContaOrigemDinIniId        := vSql.GetString('CONTA_ORIGEM_DIN_INI_ID');
      Result[i].NomeContaOrigem            := vSql.GetString('NOME_CONTA_ORIGEM');

      Result[i].ValorDinheiro              := vSql.GetDouble('VALOR_DINHEIRO');
      Result[i].ValorPix                   := vSql.GetDouble('VALOR_PIX');
      Result[i].ValorCheque                := vSql.GetDouble('VALOR_CHEQUE');
      Result[i].ValorCartao                := vSql.GetDouble('VALOR_CARTAO');
      Result[i].ValorCobranca              := vSql.GetDouble('VALOR_COBRANCA');
      Result[i].ValorAcumulado             := vSql.GetDouble('VALOR_ACUMULADO');
      Result[i].ValorCredito               := vSql.GetDouble('VALOR_CREDITO');
      Result[i].ValorSuprimento            := vSql.GetDouble('VALOR_SUPRIMENTO');

      Result[i].ValorPagtosDinheiro        := vSql.GetDouble('VALOR_PAGTOS_DINHEIRO');
      Result[i].ValorPagtosCredito         := vSql.GetDouble('VALOR_PAGTOS_CREDITO');

      Result[i].ValorSangriaDinheiro       := vSql.GetDouble('VALOR_SANGRIA_DINHEIRO');
      Result[i].ValorSangriaCartao         := vSql.GetDouble('VALOR_SANGRIA_CARTAO');
      Result[i].ValorSangriaCheque         := vSql.GetDouble('VALOR_SANGRIA_CHEQUE');
      Result[i].ValorSangriaCobranca       := vSql.GetDouble('VALOR_SANGRIA_COBRANCA');

      Result[i].ValorDinheiroInfFechamento := vSql.GetDouble('VALOR_DINHEIRO_INF_FECHAMENTO');
      Result[i].ValorChequeInfFechamento   := vSql.GetDouble('VALOR_CHEQUE_INF_FECHAMENTO');
      Result[i].ValorCartaoInfFechamento   := vSql.GetDouble('VALOR_CARTAO_INF_FECHAMENTO');
      Result[i].ValorCobrancaInfFechamento := vSql.GetDouble('VALOR_COBRANCA_INF_FECHAMENTO');

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function LancarDiferencaFechamentoCaixa(pConexao: TConexao; const pTurnoId: Integer; const pValorDiferenca: Double): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.TeveErro := False;
  vProc := TProcedimentoBanco.Create(pConexao, 'LANCAR_DIFERENCA_FECH_CAIXA');

  try
    vProc.Params[0].AsInteger := pTurnoId;
    vProc.Params[1].AsFloat := pValorDiferenca;
    vProc.Executar;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  vProc.Free;
end;

function ExisteCartoesTurno(pConexao: TConexao; pTurnoId: Integer; const pBancoCaixaId: Integer): Boolean;
var
  vSql: TConsulta;
begin
  Result := False;
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  count(COR.RECEBER_ID) ');
  vSql.SQL.Add('from ');
  vSql.SQL.Add('  CONTAS_RECEBER COR ');

  vSql.SQL.Add('inner join TIPOS_COBRANCA TCO ');
  vSql.SQL.Add('on COR.COBRANCA_ID = TCO.COBRANCA_ID ');

  vSql.SQL.Add('where COR.TURNO_ID = :P1 ');
  vSql.SQL.Add('and TCO.FORMA_PAGAMENTO = ''CRT'' ');
  vSql.SQL.Add('and COR.BANCO_CAIXA_ID = :P2 ');

  if vSql.Pesquisar([pTurnoId, pBancoCaixaId]) then
    Result := vSql.GetInt(0) > 0;

  vSql.Free;
end;

function getTurnoId( pConexao: TConexao; pFuncionarioId: Integer ): Integer;
var
  vSql: TConsulta;
begin
  Result := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  TURNO_ID ');
  vSql.Add('from ');
  vSql.Add('  TURNOS_CAIXA ');
  vSql.Add('where FUNCIONARIO_ID = :P1 ');
  vSql.Add('and STATUS = ''A'' ');

  if vSql.Pesquisar([pFuncionarioId]) then
    Result := vSql.GetInt(0);

  vSql.Free;
end;

end.
