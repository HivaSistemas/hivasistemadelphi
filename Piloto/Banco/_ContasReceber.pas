unit _ContasReceber;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, _RecordsFinanceiros, System.Variants, _Biblioteca, _CobrancaBancaria;

{$M+}
type
  RecBoletosEmitir = record
    ReceberId: Integer;
    OrcamentoId: Integer;
    Parcela: Integer;
    NumeroParcelas: Integer;
    PortadorId: string;
    NomePortador: string;
    CobrancaId: Integer;
    NomeTipoCobranca: string;
    EmpresaId: Integer;
    NomeEmpresa: string;
    CadastroId: Integer;
    NomeCliente: string;
    ValorDocumento: Double;
    NossoNumero: string;
    Documento: string;
    Status: string;
    DataVencimento: TDateTime;
    DataEmissao: TDateTime;
    Observacoes: string;
    Conta: string;
    DigitoConta: string;
    Carteira: string;
    InstrucaoBoleto: string;
    Agencia: string;
    DigitoAgencia: string;
    CpfCnpj: string;
    Logradouro: string;
    Complemento: string;
    Cep: string;
    NomeBairro: string;
    NomeCidade: string;
    EstadoId: string;
    CnpjEmpresa: string;
    LogradouroEmpresa: string;
    ComplementoEmpresa: string;
    NomeBairroEmpresa: string;
    NomeCidadeEmpresa: string;
    EstadoIdEmpresa: string;
    CepEmpresa: string;
  end;

  RecCartoesVinculacao = record
    ReceberId: Integer;

    MovimentoId: Integer;
    CobrancaId: Integer;
    ItemIdCartao: Integer;

    Origem: string;
    ClienteId: Integer;
    NomeCliente: string;
    TipoMovimento: string;

    Nsu: string;
    NumeroCartao: string;
    CodigoAutorizacao: string;

    DataCadastro: TDateTime;
    ValorCartao: Currency;
    QtdeParcelas: Integer;
  end;

  TContaReceber = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordContaReceber: RecContasReceber;
  end;

function AtualizarContaReceber(
  pConexao: TConexao;
  pReceberId: Integer;
  pEmpresaId: Integer;
  pCobrancaId: Integer;
  pClienteId: Integer;
  pParcela: Integer;
  pNumeroParcelas: Integer;
  pPortadorId: string;
  pPlanoFinanceiroId: string;
  pAgencia: string;
  pBanco: string;
  pNumeroCheque: Integer;
  pValorDocumento: Double;
  pNossoNumero: string;
  pDocumento: string;
  pStatus: string;
  pDataVencimentoOriginal: TDateTime;
  pDataVencimento: TDateTime;
  pDataEmissao: TDateTime;
  pTelefoneEmitente: string;
  pNomeEmitente: string;
  pCpfCnpjEmitente: string;
  pCodigoAutorizacaoTef: string;
  pContaCorrente: string;
  pCodigoBarras: string;
  pObservacoes: string;
  pCentroCustoId: Integer;
  pOrigem: string;
  pBaixaPagarOrigemId: Integer;
  pOrcamentoId: Integer;
  pDataContabil: TDateTime;
  pValorRetencao: Double;
  pEmTransacao: Boolean
): RecRetornoBD;

function BuscarContasReceber(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecContasReceber>;

function BuscarContasReceberComando(pConexao: TConexao; pComando: string): TArray<RecContasReceber>;

function ExcluirContasReceber(
  pConexao: TConexao;
  pReceberId: Integer
): RecRetornoBD;

function AtualizarBancoCaixaTitulos(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pBaixaOrigemId: Integer;
  pItemIdCrtOrcamento: Integer;
  pMovimentoId: Integer;
  pTipo: string;
  pReceberId: Integer
): RecRetornoBD;

function BuscarCartoesTurno(pConexao: TConexao; pTurnoId: Integer): TArray<RecCartoesTurno>;
function BuscarLimiteCreditoUtilizadoAtualmente(pConexao: TConexao; const pClienteId: Integer; const pNoMes: Boolean): Currency;
function GerarNossoNumero(pConexao: TConexao; pReceberId: Integer): RecRetornoBD;
function ExistemTitulosBaixados(pConexao: TConexao; pReceberIds: TArray<Integer>): Boolean;
function BuscarBoletosEmitir(pConexao: TConexao; pComando: string): TArray<RecBoletosEmitir>;
function BuscarBoletosGeracaoRemessa(pConexao: TConexao; pComando: string): TArray<RecBoletoGerarRemessa>;
function AtualizarInformacoes(
  pConexao: TConexao;
  pReceberId: Integer;
  pDataVencimento: TDateTime;
  pValorDocumento: Currency;
  pParcela: Integer;
  pQtdeParcelas: Integer
): RecRetornoBD;

function AtualizarObservacoes(
  pConexao: TConexao;
  pReceberId: Integer;
  pObservacoes: string
): RecRetornoBD;

function AtualizarVendedor(
  pConexao: TConexao;
  pReceberId: Integer;
  pVendedorId: Integer
): RecRetornoBD;

function buscarContasReceberVincularCartao(
  pConexao: TConexao;
  pDataTransacao: TDateTime;
  pValorTransacao: Currency;
  pQtdeParcelas: Integer
): RecCartoesVinculacao;

function AtualizarDadosCartoes(pConexao: TConexao; pCartoes: TArray<RecCartoesVinculacao>): RecRetornoBD;

function AtualizarContaCustodia(
  pConexao: TConexao;
  pReceberIds: TArray<Integer>;
  pContaCustodiaId: Integer;
  pObservacoesContaCustodia: string
): RecRetornoBD;

function BloquearVendaPorQtdeDiasTitulosVencidos(pConexao: TConexao; pClienteId: Integer; pQtdeDiasBloquear: Integer): Boolean;
function AtualizarCampoDocumento(pConexao: TConexao; pReceberId: Integer; pDocumento: string): RecRetornoBD;
function AtualizarValorRetencao(pConexao: TConexao; pReceberId: Integer; pValorRetencao: Double): RecRetornoBD;
function AtualizarCentroCusto(pConexao: TConexao; pReceberId: Integer; pCentroCustoId: Integer): RecRetornoBD;
function AtualizarNossoNumeroCodBarraLinhaDigitavel(pConexao: TConexao; pReceberId: Integer; pNossoNumero, pCodigoBarras, pLinhaDigitavel: String): RecRetornoBD;
function AtualizarEmitiuBoleto(pConexao: TConexao; pReceberId: Integer; pEmitiuBoleto: String): RecRetornoBD;
function AtualizarContaReceberArquivoRetorno(pConexao: TConexao; pReceberId: Integer; pDataContabil: TDateTime; pValorJuros, pValorMulta: Double): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TContaReceber }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 13);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COR.RECEBER_ID = :P1 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COR.TURNO_ID = :P1 ' +
      'and TCO.FORMA_PAGAMENTO = ''CHQ'' '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COR.TURNO_ID = :P1 ' +
      'and TCO.FORMA_PAGAMENTO = ''COB'' '
    );

  Result[3] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COR.RECEBER_ID = :P1 ' +
      'and COR.STATUS = ''A'' '
    );

  Result[4] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COR.ORCAMENTO_ID = :P1 ' +
      'and COR.STATUS = ''A'' ' +
      'and TCO.BOLETO_BANCARIO = ''S'' ' +
      'order by ' +
      '  TCO.COBRANCA_ID, ' +
      '  COR.PARCELA '
    );

  Result[5] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COR.BAIXA_ORIGEM_ID = :P1 ' +
      'order by ' +
      '  COR.RECEBER_ID '
    );

  Result[6] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COR.CADASTRO_ID = :P1 ' +
      'and COR.STATUS = ''A'' ' +
      'and (COR.COBRANCA_ID = :P2 or COR.COBRANCA_ID = :P3) ' +
      'order by ' +
      '  COR.RECEBER_ID '
    );

  Result[7] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COR.BAIXA_ID in(select BAIXA_ID from CONTAS_RECEBER_BAIXAS where BAIXA_PAGAR_ORIGEM_ID = :P1) ' +
      'and (COR.COBRANCA_ID = :P2 or COR.COBRANCA_ID = :P3) ' +
      'order by ' +
      '  COR.RECEBER_ID '
    );

  Result[8] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COR.ORCAMENTO_ID = :P1 ',
      'order by ' +
      '  COR.RECEBER_ID '
    );

  Result[9] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COR.ACUMULADO_ID = :P1 ',
      'order by ' +
      '  COR.RECEBER_ID '
    );

  Result[10] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COR.ORCAMENTO_ID = :P1 ' +
      'and TCO.EMITIR_DUPLIC_MERCANTIL_VENDA = ''S'' ' +
      'and COR.STATUS = ''A'' ',
      'order by ' +
      '  COR.RECEBER_ID '
    );

  Result[11] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COR.ACUMULADO_ID = :P1 ' +
      'and TCO.EMITIR_DUPLIC_MERCANTIL_VENDA = ''S'' ' +
      'and COR.STATUS = ''A'' ',
      'order by ' +
      '  COR.RECEBER_ID '
    );

  Result[12] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COR.RECEBER_ID = :P1 ' +
      'and TCO.EMITIR_DUPLIC_MERCANTIL_VENDA = ''S'' ' +
      'and COR.STATUS = ''A'' '
    );
end;

constructor TContaReceber.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONTAS_RECEBER');

  FSql :=
    'select ' +
    '  COR.RECEBER_ID, ' +
    '  COR.PARCELA, ' +
    '  COR.NUMERO_CARTAO_TRUNCADO, ' +
    '  COR.NUMERO_CHEQUE, ' +
    '  COR.AGENCIA, ' +
    '  COR.BANCO, ' +
    '  COR.NOTA_FISCAL_ID, ' +
    '  COR.VENDEDOR_ID, ' +
    '  nvl(FUN.APELIDO, FUN.NOME) as NOME_VENDEDOR, ' +
    '  COR.USUARIO_CADASTRO_ID, ' +
    '  FUC.NOME as NOME_USUARIO_CADASTRO, ' +
    '  COR.TURNO_ID, ' +
    '  COR.PORTADOR_ID, ' +
    '  POR.DESCRICAO as NOME_PORTADOR, ' +
    '  COR.COBRANCA_ID, ' +
    '  TCO.NOME as NOME_TIPO_COBRANCA, ' +
    '  COR.ORCAMENTO_ID, ' +
    '  COR.ACUMULADO_ID, ' +
    '  COR.EMPRESA_ID, ' +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA, ' +
    '  COR.CADASTRO_ID, ' +
    '  CAD.RAZAO_SOCIAL as NOME_CLIENTE, ' +
    '  COR.NUMERO_PARCELAS, ' +
    '  COR.VALOR_DOCUMENTO, ' +
    '  COR.VALOR_RETENCAO, ' +
    '  COR.VALOR_ADIANTADO, ' +
    '  COR.NOME_EMITENTE, ' +
    '  COR.CPF_CNPJ_EMITENTE, ' +
    '  COR.CODIGO_BARRAS, ' +
    '  COR.NSU, ' +
    '  COR.NOSSO_NUMERO, ' +
    '  COR.REMESSA_ID, ' +
    '  COR.NOME_ARQUIVO_REMESSA, ' +
    '  COR.DOCUMENTO, ' +
    '  COR.STATUS, ' +
    '  COR.TELEFONE_EMITENTE, ' +
    '  COR.DATA_VENCIMENTO_ORIGINAL, ' +
    '  COR.DATA_VENCIMENTO, ' +
    '  COR.DATA_EMISSAO, ' +
    '  COR.DATA_CONTABIL, ' +
    '  COR.DATA_CADASTRO, ' +
    '  COR.CODIGO_AUTORIZACAO_TEF, ' +
    '  COR.CONTA_CORRENTE, ' +
    '  COR.BAIXA_ORIGEM_ID, ' +
    '  COR.PLANO_FINANCEIRO_ID, ' +
    '  COR.ORIGEM, ' +
    '  COR.ITEM_ID_CRT_ORCAMENTO, ' +
    '  COR.BAIXA_PAGAR_ORIGEM_ID, ' +
    '  COR.CONTA_CUSTODIA_ID, ' +
    '  COR.OBSERVACOES_CONTA_CUSTODIA, ' +
    '  TCO.FORMA_PAGAMENTO, ' +
    '  COR.OBSERVACOES, ' +
    '  BAX.BAIXA_ID, ' +
    '  CRB.USUARIO_BAIXA_ID, ' +
    '  FBA.NOME as NOME_USUARIO_BAIXA, ' +
    '  CRB.DATA_PAGAMENTO, ' +
    '  CRB.DATA_HORA_BAIXA, ' +
    '  case when COR.STATUS = ''A'' then ' +
    '    case when trunc(sysdate) - COR.DATA_VENCIMENTO > 0 then trunc(sysdate) - COR.DATA_VENCIMENTO else 0 end ' +
    '  else ' +
    '    case when trunc(BAI.DATA_PAGAMENTO) - COR.DATA_VENCIMENTO > 0 then trunc(BAI.DATA_PAGAMENTO) - COR.DATA_VENCIMENTO else 0 end ' +
    '  end as DIAS_ATRASO, ' +
    '  COR.CENTRO_CUSTO_ID, ' +
    '  CEN.NOME as NOME_CENTRO_CUSTO ' +
    'from ' +
    '  CONTAS_RECEBER COR ' +

    'inner join PARAMETROS_EMPRESA PAE ' +
    'on COR.EMPRESA_ID = PAE.EMPRESA_ID ' +

    'inner join CADASTROS CAD ' +
    'on COR.CADASTRO_ID = CAD.CADASTRO_ID ' +

    'inner join TIPOS_COBRANCA TCO ' +
    'on COR.COBRANCA_ID = TCO.COBRANCA_ID ' +

    'left join FUNCIONARIOS FUN ' +
    'on COR.VENDEDOR_ID = FUN.FUNCIONARIO_ID ' +

    'inner join FUNCIONARIOS FUC ' +
    'on COR.USUARIO_CADASTRO_ID = FUC.FUNCIONARIO_ID ' +

    'inner join PORTADORES POR ' +
    'on COR.PORTADOR_ID = POR.PORTADOR_ID ' +

    'inner join EMPRESAS EMP ' +
    'on COR.EMPRESA_ID = EMP.EMPRESA_ID ' +

    'inner join CENTROS_CUSTOS CEN ' +
    'on COR.CENTRO_CUSTO_ID = CEN.CENTRO_CUSTO_ID ' +

    'left join CONTAS_RECEBER_BAIXAS_ITENS BAX ' +
    'on COR.RECEBER_ID = BAX.RECEBER_ID ' +

    'left join CONTAS_RECEBER_BAIXAS CRB ' +
    'on BAX.BAIXA_ID = CRB.BAIXA_ID ' +

    'left join FUNCIONARIOS FBA ' +
    'on CRB.USUARIO_BAIXA_ID = FBA.FUNCIONARIO_ID ' +

    'left join CONTAS_RECEBER_BAIXAS BAI ' +
    'on COR.BAIXA_ORIGEM_ID = BAI.BAIXA_ID ' +

    'left join CONTAS_RECEBER_BAIXAS_ITENS BXI ' +
    'on COR.RECEBER_ID = BXI.RECEBER_ID ' +
    'and BAI.BAIXA_ID = BXI.RECEBER_ID ';

  setFiltros(getFiltros);

  AddColuna('RECEBER_ID', True);
  AddColuna('PARCELA');
  AddColuna('NUMERO_CHEQUE');
  AddColuna('AGENCIA');
  AddColuna('BANCO');
  AddColuna('NOTA_FISCAL_ID');
  AddColuna('VENDEDOR_ID');
  AddColunaSL('NOME_VENDEDOR');
  AddColunaSL('USUARIO_CADASTRO_ID');
  AddColunaSL('NOME_USUARIO_CADASTRO');
  AddColuna('TURNO_ID');
  AddColuna('COBRANCA_ID');
  AddColunaSL('NOME_TIPO_COBRANCA');
  AddColuna('PORTADOR_ID');
  AddColunaSL('NOME_PORTADOR');
  AddColuna('ORCAMENTO_ID');
  AddColuna('EMPRESA_ID');
  AddColunaSL('NOME_EMPRESA');
  AddColuna('CADASTRO_ID');
  AddColunaSL('NOME_CLIENTE');
  AddColuna('NUMERO_PARCELAS');
  AddColuna('VALOR_DOCUMENTO');
  AddColuna('VALOR_RETENCAO');
  AddColunaSL('VALOR_ADIANTADO');
  AddColuna('NUMERO_CARTAO_TRUNCADO');
  AddColuna('NOME_EMITENTE');
  AddColuna('CPF_CNPJ_EMITENTE');
  AddColuna('CODIGO_BARRAS');
  AddColuna('NSU');
  AddColuna('NOSSO_NUMERO');
  AddColunaSL('REMESSA_ID');
  AddColunaSL('NOME_ARQUIVO_REMESSA');
  AddColuna('DOCUMENTO');
  AddColuna('STATUS');
  AddColuna('TELEFONE_EMITENTE');
  AddColuna('DATA_VENCIMENTO_ORIGINAL');
  AddColuna('DATA_VENCIMENTO');
  AddColuna('DATA_EMISSAO');
  AddColuna('DATA_CONTABIL');
  AddColuna('DATA_CADASTRO');
  AddColuna('CODIGO_AUTORIZACAO_TEF');
  AddColuna('CONTA_CORRENTE');
  AddColuna('BAIXA_ORIGEM_ID');
  AddColuna('PLANO_FINANCEIRO_ID');
  AddColuna('ORIGEM');
  AddColuna('BAIXA_PAGAR_ORIGEM_ID');
  AddColunaSL('ITEM_ID_CRT_ORCAMENTO');
  AddColunaSL('FORMA_PAGAMENTO');
  AddColunaSL('CONTA_CUSTODIA_ID');
  AddColunaSL('OBSERVACOES_CONTA_CUSTODIA');
  AddColuna('OBSERVACOES');
  AddColunaSL('BAIXA_ID');
  AddColunaSL('USUARIO_BAIXA_ID');
  AddColunaSL('NOME_USUARIO_BAIXA');
  AddColunaSL('DATA_HORA_BAIXA');
  AddColunaSL('DATA_PAGAMENTO');
  AddColunaSL('DIAS_ATRASO');
  AddColuna('CENTRO_CUSTO_ID');
  AddColunaSL('NOME_CENTRO_CUSTO');
  AddColunaSL('ACUMULADO_ID');
end;

function TContaReceber.getRecordContaReceber: RecContasReceber;
begin
  Result.ReceberId                := getInt('RECEBER_ID', True);
  Result.Parcela                  := getInt('PARCELA');
  Result.numero_cheque            := getInt('NUMERO_CHEQUE');
  Result.agencia                  := getString('AGENCIA');
  Result.banco                    := getInt('BANCO');
  Result.nota_fiscal_id           := getInt('NOTA_FISCAL_ID');
  Result.UsuarioCadastroId        := getInt('USUARIO_CADASTRO_ID');
  Result.NomeUsuarioCadastro      := getString('NOME_USUARIO_CADASTRO');
  Result.vendedor_id              := getInt('VENDEDOR_ID');
  Result.nome_vendedor            := getString('NOME_VENDEDOR');
  Result.turno_id                 := getInt('TURNO_ID');
  Result.cobranca_id              := getInt('COBRANCA_ID');
  Result.nome_tipo_cobranca       := getString('NOME_TIPO_COBRANCA');
  Result.PortadorId               := getString('PORTADOR_ID');
  Result.NomePortador             := getString('NOME_PORTADOR');
  Result.orcamento_id             := getInt('ORCAMENTO_ID');
  Result.AcumuladoId              := getInt('ACUMULADO_ID');
  Result.empresa_id               := getInt('EMPRESA_ID');
  Result.nome_empresa             := getString('NOME_EMPRESA');
  Result.CadastroId               := getInt('CADASTRO_ID');
  Result.NomeCliente              := getString('NOME_CLIENTE');
  Result.NumeroParcelas           := getInt('NUMERO_PARCELAS');
  Result.ValorDocumento           := getDouble('VALOR_DOCUMENTO');
  Result.ValorRetencao            := getDouble('VALOR_RETENCAO');
  Result.ValorAdiantado           := getDouble('VALOR_ADIANTADO');
  Result.nome_emitente            := getString('NOME_EMITENTE');
  Result.CpfCnpjEmitente          := getString('CPF_CNPJ_EMITENTE');
  Result.codigo_barras            := getString('CODIGO_BARRAS');
  Result.nsu                      := getString('NSU');
  Result.NossoNumero              := getString('NOSSO_NUMERO');
  Result.RemessaId                := getInt('REMESSA_ID');
  Result.NomeArquivoRemessa       := getString('NOME_ARQUIVO_REMESSA');
  Result.documento                := getString('DOCUMENTO');
  Result.status                   := getString('STATUS');
  Result.StatusAnalitico          := IIfStr(getString('STATUS') = 'A', 'Aberto', 'Baixado');
  Result.telefone_emitente        := getString('TELEFONE_EMITENTE');
  Result.data_vencimento_original := getData('DATA_VENCIMENTO_ORIGINAL');
  Result.data_vencimento          := getData('DATA_VENCIMENTO');
  Result.data_emissao             := getData('DATA_EMISSAO');
  Result.DataContabil             := getData('DATA_CONTABIL');
  Result.data_cadastro            := getData('DATA_CADASTRO');
  Result.codigo_autorizacao_tef   := getString('CODIGO_AUTORIZACAO_TEF');
  Result.conta_corrente           := getString('CONTA_CORRENTE');
  Result.baixa_origem_id          := getInt('BAIXA_ORIGEM_ID');
  Result.PlanoFinanceiroId        := getString('PLANO_FINANCEIRO_ID');
  Result.Origem                   := getString('ORIGEM');
  Result.BaixaPagarOrigemId       := getInt('BAIXA_PAGAR_ORIGEM_ID');
  Result.ItemIdCrtOrcamentoId     := getInt('ITEM_ID_CRT_ORCAMENTO');
  Result.forma_pagamento          := getString('FORMA_PAGAMENTO');
  Result.observacoes              := getString('OBSERVACOES');
  Result.ContaCustodiaId          := getInt('CONTA_CUSTODIA_ID');
  Result.ObservacoesContaCustodia := getString('OBSERVACOES_CONTA_CUSTODIA');
  Result.baixa_id                 := getInt('BAIXA_ID');
  Result.usuario_baixa_id         := getInt('USUARIO_BAIXA_ID');
  Result.nome_usuario_baixa       := getString('NOME_USUARIO_BAIXA');
  Result.data_pagamento           := getData('DATA_PAGAMENTO');
  Result.data_hora_baixa          := getData('DATA_HORA_BAIXA');
  Result.dias_atraso              := getInt('DIAS_ATRASO');
  Result.CentroCustoId            := getInt('CENTRO_CUSTO_ID');
  Result.NomeCentroCusto          := getString('NOME_CENTRO_CUSTO');
  Result.NumeroCartaoTruncado     := getString('NUMERO_CARTAO_TRUNCADO');
end;

function AtualizarContaReceber(
  pConexao: TConexao;
  pReceberId: Integer;
  pEmpresaId: Integer;
  pCobrancaId: Integer;
  pClienteId: Integer;
  pParcela: Integer;
  pNumeroParcelas: Integer;
  pPortadorId: string;
  pPlanoFinanceiroId: string;
  pAgencia: string;
  pBanco: string;
  pNumeroCheque: Integer;
  pValorDocumento: Double;
  pNossoNumero: string;
  pDocumento: string;
  pStatus: string;
  pDataVencimentoOriginal: TDateTime;
  pDataVencimento: TDateTime;
  pDataEmissao: TDateTime;
  pTelefoneEmitente: string;
  pNomeEmitente: string;
  pCpfCnpjEmitente: string;
  pCodigoAutorizacaoTef: string;
  pContaCorrente: string;
  pCodigoBarras: string;
  pObservacoes: string;
  pCentroCustoId: Integer;
  pOrigem: string;
  pBaixaPagarOrigemId: Integer;
  pOrcamentoId: Integer;
  pDataContabil: TDateTime;
  pValorRetencao: Double;
  pEmTransacao: Boolean
): RecRetornoBD;
var
  t: TContaReceber;
  vNovo: Boolean;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_CONTAS_RECEBER');

  t := TContaReceber.Create(pConexao);

  vNovo := pReceberId = 0;
  if vNovo then begin
    pReceberId := TSequencia.Create(pConexao, 'SEQ_RECEBER_ID').GetProximaSequencia;
    Result.AsInt := pReceberId;
  end;

  try
    if not pEmTransacao then
      pConexao.IniciarTransacao;

    t.setInt('RECEBER_ID', pReceberId, True);
    t.setInt('EMPRESA_ID', pEmpresaId);
    t.setInt('COBRANCA_ID', pCobrancaId);
    t.setInt('CADASTRO_ID', pClienteId);
    t.setInt('PARCELA', pParcela);
    t.setInt('NUMERO_PARCELAS', pNumeroParcelas);
    t.setInt('CENTRO_CUSTO_ID', pCentroCustoId);
    t.setString('PORTADOR_ID', pPortadorId);
    t.setString('PLANO_FINANCEIRO_ID', pPlanoFinanceiroId);
    t.setStringN('AGENCIA', pAgencia);
    t.setStringN('BANCO', pBanco);
    t.setIntN('NUMERO_CHEQUE', pNumeroCheque);
    t.setDouble('VALOR_DOCUMENTO', pValorDocumento);
    t.setStringN('NOSSO_NUMERO', pNossoNumero);
    t.setString('DOCUMENTO', pDocumento);
    t.setString('STATUS', pStatus);
    t.setDataN('DATA_VENCIMENTO_ORIGINAL', pDataVencimentoOriginal);
    t.setData('DATA_VENCIMENTO', pDataVencimento);
    t.setDataN('DATA_EMISSAO', pDataEmissao);
    t.setDataN('DATA_CONTABIL', pDataContabil);
    t.setStringN('TELEFONE_EMITENTE', pTelefoneEmitente);
    t.setStringN('NOME_EMITENTE', pNomeEmitente);
    t.setStringN('CPF_CNPJ_EMITENTE', pCpfCnpjEmitente);
    t.setStringN('CODIGO_AUTORIZACAO_TEF', pCodigoAutorizacaoTef);
    t.setStringN('CONTA_CORRENTE', pContaCorrente);
    t.setStringN('CODIGO_BARRAS', pCodigoBarras);
    t.setString('OBSERVACOES', pObservacoes);
    t.setString('ORIGEM', pOrigem);
    t.setIntN('BAIXA_PAGAR_ORIGEM_ID', pBaixaPagarOrigemId);
    t.setIntN('ORCAMENTO_ID', pOrcamentoId);
    t.setDouble('VALOR_RETENCAO', pValorRetencao);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    if not pEmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pEmTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarContasReceber(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecContasReceber>;
var
  i: Integer;
  t: TContaReceber;
begin
  Result := nil;
  t := TContaReceber.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordContaReceber;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarContasReceberComando(pConexao: TConexao; pComando: string): TArray<RecContasReceber>;
var
  i: Integer;
  t: TContaReceber;
begin
  Result := nil;
  t := TContaReceber.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordContaReceber;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirContasReceber(
  pConexao: TConexao;
  pReceberId: Integer
): RecRetornoBD;
var
  t: TContaReceber;
begin
  Result.Iniciar;
  t := TContaReceber.Create(pConexao);

  t.setInt('RECEBER_ID', pReceberId, True);

  try
    pConexao.IniciarTransacao;

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  t.Free;
end;

function AtualizarBancoCaixaTitulos(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pBaixaOrigemId: Integer;
  pItemIdCrtOrcamento: Integer;
  pMovimentoId: Integer;
  pTipo: string;
  pReceberId: Integer
): RecRetornoBD;
var
  vExec: TExecucao;
  vProcedimento: TProcedimentoBanco;
begin
  pConexao.SetRotina('ATUALIZAR_TITULOS_FINANCEIRO');
  vExec := TExecucao.Create(pConexao);
  vProcedimento := TProcedimentoBanco.Create(pConexao, 'GRAVAR_TITULOS_MOV_TURNOS_ITE');

  try
    if pTipo = 'CRT' then begin
      vExec.Add('update CONTAS_RECEBER set ');
      vExec.Add('  TURNO_ID = null ');
      vExec.Add('where ITEM_ID_CRT_ORCAMENTO = :P2 ');
      if pOrcamentoId > 0 then
        vExec.Add('and ORCAMENTO_ID = :P1 ')
      else
        vExec.Add('and BAIXA_ORIGEM_ID = :P1 ');

      vExec.Executar([IIf(pOrcamentoId > 0, pOrcamentoId, pBaixaOrigemId), pItemIdCrtOrcamento]);
    end
    else begin
      vExec.Add('update CONTAS_RECEBER set ');
      vExec.Add('  TURNO_ID = null ');
      vExec.Add('where RECEBER_ID = :P1 ');
      vExec.Executar([pReceberID]);
    end;

    if pTipo = 'CRT' then begin
      vProcedimento.Params[0].AsInteger := pOrcamentoId;   // iORCAMENTO_ID
      vProcedimento.Params[1].AsInteger := pItemIdCrtOrcamento;
      vProcedimento.Params[4].Value     := null;           // iRECEBER_ID
    end
    else begin
      vProcedimento.Params[0].Value     := null;           // iORCAMENTO_ID
      vProcedimento.Params[1].Value     := null;           // ITEM_ID_CRT_ORC
      vProcedimento.Params[4].AsInteger := pReceberId;     // iRECEBER_ID
    end;
    vProcedimento.Params[2].AsInteger := pMovimentoId;   // iMOVIMENTO_ID
    vProcedimento.Params[3].AsString  := pTipo;          // iTIPO
    vProcedimento.Executar;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vExec.Free;
end;

function BuscarCartoesTurno(pConexao: TConexao; pTurnoId: Integer): TArray<RecCartoesTurno>;
var
  i: Integer;
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  COR.NSU, ');
  vSql.Add('  COR.CADASTRO_ID, ');
  vSql.Add('  CAD.NOME_FANTASIA, ');
  vSql.Add('  SUM(VALOR_DOCUMENTO), ');
  vSql.Add('  TCO.COBRANCA_ID, ');
  vSql.Add('  TCO.NOME, ');
  vSql.Add('  COR.ORCAMENTO_ID, ');
  vSql.Add('  COR.BAIXA_ORIGEM_ID, ');
  vSql.Add('  COR.ITEM_ID_CRT_ORCAMENTO ');
  vSql.Add('from ');
  vSql.Add('  CONTAS_RECEBER COR ');

  vSql.Add('inner join TIPOS_COBRANCA TCO ');
  vSql.Add('on COR.COBRANCA_ID = TCO.COBRANCA_ID ');

  vSql.Add('inner join CADASTROS CAD ');
  vSql.Add('on COR.CADASTRO_ID = CAD.CADASTRO_ID ');

  vSql.Add('where COR.TURNO_ID = :P1 ');
  vSql.Add('and TCO.FORMA_PAGAMENTO = ''CRT'' ');

  vSql.Add('group by ');
  vSql.Add('  COR.NSU, ');
  vSql.Add('  COR.CADASTRO_ID, ');
  vSql.Add('  CAD.NOME_FANTASIA, ');
  vSql.Add('  TCO.COBRANCA_ID, ');
  vSql.Add('  TCO.NOME, ');
  vSql.Add('  COR.ORCAMENTO_ID, ');
  vSql.Add('  COR.BAIXA_ORIGEM_ID, ');
  vSql.Add('  COR.ITEM_ID_CRT_ORCAMENTO ');

  vSql.Add('order by ');
  vSql.Add('  COR.ORCAMENTO_ID desc, ');
  vSql.Add('  COR.ITEM_ID_CRT_ORCAMENTO ');

  if vSql.Pesquisar([pTurnoId]) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros -1 do begin
      Result[i].nsu           := vSql.GetString(0);
      Result[i].CadastroId    := vSql.GetInt(1);
      Result[i].nome_fantasia := vSql.GetString(2);
      Result[i].valor         := vSql.GetDouble(3);
      Result[i].cobranca_id   := vSql.GetInt(4);
      Result[i].nome_cobranca := vSql.GetString(5);
      Result[i].orcamento_id  := vSql.GetInt(6);
      Result[i].BaixaOrigemId      := vSql.GetInt(7);
      Result[i].itemIdCrtOrcamento := vSql.GetInt(8);

      vSql.Next();
    end;
  end;
  vSql.Active := False;
  vSql.Free;
end;

function BuscarLimiteCreditoUtilizadoAtualmente(pConexao: TConexao; const pClienteId: Integer; const pNoMes: Boolean): Currency;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  nvl(sum(COR.VALOR_DOCUMENTO), 0) as LIMITE_CREDITO_UTILIZADO ');
  vSql.Add('from ');
  vSql.Add('  CONTAS_RECEBER COR ');

  vSql.Add('inner join TIPOS_COBRANCA TCO ');
  vSql.Add('on COR.COBRANCA_ID = TCO.COBRANCA_ID ');

  vSql.Add('where COR.CADASTRO_ID = :P1 ');
  vSql.Add('and COR.STATUS = ''A'' ');
  vSql.Add('and TCO.UTILIZAR_LIMITE_CRED_CLIENTE = ''S'' ');

  if pNoMes then
  vSql.Add('and to_char(COR.DATA_EMISSAO, ''MM'') = to_char(sysdate, ''MM'') ');

  vSql.Pesquisar([pClienteId]);
  Result := vSql.GetDouble(0);

  vSql.Active := False;
  vSql.Free;
end;

function GerarNossoNumero(pConexao: TConexao; pReceberId: Integer): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  vProc := TProcedimentoBanco.Create(pConexao, 'GERAR_NOSSO_NUMERO');

  try
    pConexao.IniciarTransacao;

    vProc.Params[0].AsInteger := pReceberId;

    vProc.ExecProc;

    Result.AsString := vProc.Params[1].AsString;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  vProc.Free;
end;

function ExistemTitulosBaixados(pConexao: TConexao; pReceberIds: TArray<Integer>): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  count(*) ');
  vSql.Add('from ');
  vSql.Add('  CONTAS_RECEBER ');

  vSql.Add('where ' + FiltroInInt('RECEBER_ID', pReceberIds));
  vSql.Add('and STATUS = ''B'' ');

  vSql.Pesquisar;
  Result := vSql.GetInt(0) > 0;

  vSql.Active := False;
  vSql.Free;

end;

function BuscarBoletosEmitir(pConexao: TConexao; pComando: string): TArray<RecBoletosEmitir>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  RECEBER_ID, ');
  vSql.Add('  ORCAMENTO_ID, ');
  vSql.Add('  PARCELA, ');
  vSql.Add('  NUMERO_PARCELAS, ');
  vSql.Add('  PORTADOR_ID, ');
  vSql.Add('  NOME_PORTADOR, ');
  vSql.Add('  COBRANCA_ID, ');
  vSql.Add('  NOME_TIPO_COBRANCA, ');
  vSql.Add('  EMPRESA_ID, ');
  vSql.Add('  NOME_EMPRESA, ');
  vSql.Add('  CADASTRO_ID, ');
  vSql.Add('  NOME_CLIENTE, ');
  vSql.Add('  VALOR_DOCUMENTO, ');
  vSql.Add('  NOSSO_NUMERO, ');
  vSql.Add('  DOCUMENTO, ');
  vSql.Add('  STATUS, ');
  vSql.Add('  DATA_VENCIMENTO, ');
  vSql.Add('  DATA_EMISSAO, ');
  vSql.Add('  OBSERVACOES, ');
  vSql.Add('  CONTA, ');
  vSql.Add('  DIGITO_CONTA, ');
  vSql.Add('  CARTEIRA, ');
  vSql.Add('  INSTRUCAO_BOLETO, ');
  vSql.Add('  AGENCIA, ');
  vSql.Add('  DIGITO_AGENCIA, ');
  vSql.Add('  CPF_CNPJ, ');
  vSql.Add('  LOGRADOURO, ');
  vSql.Add('  COMPLEMENTO, ');
  vSql.Add('  CEP, ');
  vSql.Add('  NOME_BAIRRO, ');
  vSql.Add('  NOME_CIDADE, ');
  vSql.Add('  ESTADO_ID, ');
  vSql.Add('  CNPJ_EMPRESA, ');
  vSql.Add('  LOGRADOURO_EMPRESA, ');
  vSql.Add('  COMPLEMENTO_EMPRESA, ');
  vSql.Add('  NOME_BAIRRO_EMPRESA, ');
  vSql.Add('  NOME_CIDADE_EMPRESA, ');
  vSql.Add('  ESTADO_ID_EMPRESA, ');
  vSql.Add('  CEP_EMPRESA ');
  vSql.Add('from ');
  vSql.Add('  VW_BOLETOS_RECEBER ');

  vSql.Add(pComando);

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].ReceberId         := vSql.GetInt('RECEBER_ID');
      Result[i].OrcamentoId       := vSql.GetInt('ORCAMENTO_ID');
      Result[i].Parcela           := vSql.GetInt('PARCELA');
      Result[i].NumeroParcelas    := vSql.GetInt('NUMERO_PARCELAS');
      Result[i].PortadorId        := vSql.GetString('PORTADOR_ID');
      Result[i].NomePortador      := vSql.GetString('NOME_PORTADOR');
      Result[i].CobrancaId        := vSql.GetInt('COBRANCA_ID');
      Result[i].NomeTipoCobranca  := vSql.GetString('NOME_TIPO_COBRANCA');
      Result[i].EmpresaId         := vSql.GetInt('EMPRESA_ID');
      Result[i].NomeEmpresa       := vSql.GetString('NOME_EMPRESA');
      Result[i].CadastroId        := vSql.GetInt('CADASTRO_ID');
      Result[i].NomeCliente       := vSql.GetString('NOME_CLIENTE');
      Result[i].ValorDocumento    := vSql.getDouble('VALOR_DOCUMENTO');
      Result[i].NossoNumero       := vSql.GetString('NOSSO_NUMERO');
      Result[i].Documento         := vSql.GetString('DOCUMENTO');
      Result[i].Status            := vSql.GetString('STATUS');
      Result[i].DataVencimento    := vSql.GetData('DATA_VENCIMENTO');
      Result[i].DataEmissao       := vSql.GetData('DATA_EMISSAO');
      Result[i].Observacoes       := vSql.GetString('OBSERVACOES');
      Result[i].Conta             := vSql.GetString('CONTA');
      Result[i].DigitoConta       := vSql.GetString('DIGITO_CONTA');
      Result[i].Carteira          := vSql.GetString('CARTEIRA');
      Result[i].InstrucaoBoleto   := vSql.GetString('INSTRUCAO_BOLETO');
      Result[i].Agencia           := vSql.GetString('AGENCIA');
      Result[i].DigitoAgencia     := vSql.GetString('DIGITO_AGENCIA');
      Result[i].CpfCnpj           := vSql.GetString('CPF_CNPJ');
      Result[i].Logradouro        := vSql.GetString('LOGRADOURO');
      Result[i].Complemento       := vSql.GetString('COMPLEMENTO');
      Result[i].Cep               := vSql.GetString('CEP');
      Result[i].NomeBairro        := vSql.GetString('NOME_BAIRRO');
      Result[i].NomeCidade        := vSql.GetString('NOME_CIDADE');
      Result[i].EstadoId          := vSql.GetString('ESTADO_ID');
      Result[i].CnpjEmpresa       := vSql.GetString('CNPJ_EMPRESA');
      Result[i].LogradouroEmpresa := vSql.GetString('LOGRADOURO_EMPRESA');
      Result[i].ComplementoEmpresa := vSql.GetString('COMPLEMENTO_EMPRESA');
      Result[i].NomeBairroEmpresa := vSql.GetString('NOME_BAIRRO_EMPRESA');
      Result[i].NomeCidadeEmpresa := vSql.GetString('NOME_CIDADE_EMPRESA');
      Result[i].EstadoIdEmpresa   := vSql.GetString('ESTADO_ID_EMPRESA');
      Result[i].CepEmpresa       := vSql.GetString('CEP_EMPRESA');

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function BuscarBoletosGeracaoRemessa(pConexao: TConexao; pComando: string): TArray<RecBoletoGerarRemessa>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  COR.RECEBER_ID, ');
  vSql.Add('  COR.DOCUMENTO, ');
  vSql.Add('  COR.PARCELA, ');
  vSql.Add('  CON.CARTEIRA, ');
  vSql.Add('  CON.AGENCIA, ');
  vSql.Add('  CON.CONTA, ');
  vSql.Add('  CON.DIGITO_CONTA, ');
  vSql.Add('  COR.NOSSO_NUMERO, ');
  vSql.Add('  COR.DATA_VENCIMENTO, ');
  vSql.Add('  COR.VALOR_DOCUMENTO, ');
  vSql.Add('  COR.DATA_EMISSAO, ');
  vSql.Add('  round(COR.VALOR_DOCUMENTO * ( PAE.PERCENTUAL_JUROS_MENSAL / 30) * 0.01, 2) as VALOR_DIA_ATRASO, ');
  vSql.Add('  round(COR.VALOR_DOCUMENTO * PAE.PERCENTUAL_MULTA * 0.01, 2) as VALOR_MULTA, ');
  vSql.Add('  PAE.PERCENTUAL_MULTA, ');
  vSql.Add('  PAE.PERCENTUAL_JUROS_MENSAL, ');
  vSql.Add('  CAD.CPF_CNPJ, ');
  vSql.Add('  CAD.RAZAO_SOCIAL, ');
  vSql.Add('  CAD.LOGRADOURO, ');
  vSql.Add('  CAD.COMPLEMENTO, ');
  vSql.Add('  BAI.NOME_BAIRRO, ');
  vSql.Add('  BAI.NOME_CIDADE, ');
  vSql.Add('  BAI.ESTADO_ID, ');
  vSql.Add('  CAD.CEP ');
  vSql.Add('from ');
  vSql.Add('  CONTAS_RECEBER COR ');

  vSql.Add('inner join PARAMETROS_EMPRESA PAE ');
  vSql.Add('on COR.EMPRESA_ID = PAE.EMPRESA_ID ');

  vSql.Add('inner join CADASTROS CAD ');
  vSql.Add('on COR.CADASTRO_ID = CAD.CADASTRO_ID ');

  vSql.Add('inner join VW_BAIRROS BAI ');
  vSql.Add('on CAD.BAIRRO_ID = BAI.BAIRRO_ID ');

  vSql.Add('inner join PORTADORES POR ');
  vSql.Add('on COR.PORTADOR_ID = POR.PORTADOR_ID ');

  vSql.Add('inner join PORTADORES_CONTAS PCO ');
  vSql.Add('on POR.PORTADOR_ID = PCO.PORTADOR_ID ');
  vSql.Add('and COR.EMPRESA_ID = PCO.EMPRESA_ID ');

  vSql.Add('inner join CONTAS CON ');
  vSql.Add('on PCO.CONTA_ID = CON.CONTA ');

  vSql.Add('where COR.STATUS = ''A'' ');
  vSql.Add('and COR.NOSSO_NUMERO is not null ');
  vSql.Add(pComando);

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].ReceberId         := vSql.GetInt('RECEBER_ID');
      Result[i].Documento         := vSql.GetString('DOCUMENTO');
      Result[i].Parcela           := vSql.GetInt('PARCELA');
      Result[i].Carteira          := vSql.GetString('CARTEIRA');
      Result[i].Agencia           := vSql.GetString('AGENCIA');
      Result[i].Conta             := vSql.GetString('CONTA');
      Result[i].DigitoConta       := vSql.GetString('DIGITO_CONTA');
      Result[i].NossoNumero       := vSql.GetString('NOSSO_NUMERO');
      Result[i].DataVencimento    := vSql.GetData('DATA_VENCIMENTO');
      Result[i].ValorDocumento    := vSql.GetDouble('VALOR_DOCUMENTO');
      Result[i].DataEmissao       := vSql.GetData('DATA_EMISSAO');
      Result[i].ValorDiaAtraso    := vSql.GetDouble('VALOR_DIA_ATRASO');
      Result[i].ValorMulta        := vSql.GetDouble('VALOR_MULTA');
      Result[i].PercentualMulta   := vSql.GetDouble('PERCENTUAL_MULTA');
      Result[i].PercentualJurosMensal := vSql.GetDouble('PERCENTUAL_JUROS_MENSAL');
      Result[i].CpfCnpj           := vSql.GetString('CPF_CNPJ');
      Result[i].RazaoSocial       := vSql.GetString('RAZAO_SOCIAL');
      Result[i].Logradouro        := vSql.GetString('LOGRADOURO');
      Result[i].Complemento       := vSql.GetString('COMPLEMENTO');
      Result[i].NomeBairro        := vSql.GetString('NOME_BAIRRO');
      Result[i].NomeCidade        := vSql.GetString('NOME_CIDADE');
      Result[i].EstadoId          := vSql.GetString('ESTADO_ID');
      Result[i].Cep               := vSql.GetString('CEP');

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function AtualizarObservacoes(
  pConexao: TConexao;
  pReceberId: Integer;
  pObservacoes: string
): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_INFO_CONTAS_RECEBER');

  vExec := TExecucao.Create(pConexao);

  vExec.Add('update CONTAS_RECEBER set');
  vExec.Add('  OBSERVACOES = :P2 ');
  vExec.Add('where RECEBER_ID = :P1 ');

  try
    pConexao.IniciarTransacao;

    vExec.Executar([pReceberId, pObservacoes]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
end;

function AtualizarInformacoes(
  pConexao: TConexao;
  pReceberId: Integer;
  pDataVencimento: TDateTime;
  pValorDocumento: Currency;
  pParcela: Integer;
  pQtdeParcelas: Integer
): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_INFO_CONTAS_RECEBER');

  vExec := TExecucao.Create(pConexao);

  vExec.Add('update CONTAS_RECEBER set');
  vExec.Add('  DATA_VENCIMENTO = :P2, ');
  vExec.Add('  VALOR_DOCUMENTO = :P3, ');
  vExec.Add('  PARCELA = :P4, ');
  vExec.Add('  NUMERO_PARCELAS = :P5 ');
  vExec.Add('where RECEBER_ID = :P1 ');

  try
    pConexao.IniciarTransacao;

    vExec.Executar([pReceberId, pDataVencimento, pValorDocumento, pParcela, pQtdeParcelas]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
end;

function AtualizarVendedor(
  pConexao: TConexao;
  pReceberId: Integer;
  pVendedorId: Integer
): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_VENDEDOR_CONTAS_RECEBER');

  vExec := TExecucao.Create(pConexao);

  vExec.Add('update CONTAS_RECEBER set');
  vExec.Add('  VENDEDOR_ID = :P2 ');
  vExec.Add('where RECEBER_ID = :P1 ');

  try
    pConexao.IniciarTransacao;

    vExec.Executar([pReceberId, pVendedorId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
end;

function buscarContasReceberVincularCartao(
  pConexao: TConexao;
  pDataTransacao: TDateTime;
  pValorTransacao: Currency;
  pQtdeParcelas: Integer
): RecCartoesVinculacao;
var
  vSql: TConsulta;
begin
  Result.ClienteId := -1;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  COR.ORCAMENTO_ID, ');
  vSql.Add('  COR.ACUMULADO_ID, ');
  vSql.Add('  COR.CADASTRO_ID, ');
  vSql.Add('  COR.ITEM_ID_CRT_ORCAMENTO, ');
  vSql.Add('  CAD.RAZAO_SOCIAL as NOME_CLIENTE, ');
  vSql.Add('  COR.ORIGEM, ');
  vSql.Add('  COR.BAIXA_ORIGEM_ID, ');
  vSql.Add('  COR.COBRANCA_ID, ');
  vSql.Add('  COR.NUMERO_PARCELAS, ');
  vSql.Add('  COR.DATA_CADASTRO, ');
  vSql.Add('  sum(COR.VALOR_DOCUMENTO) as VALOR_TRANSACAO ');
  vSql.Add('from ');
  vSql.Add('  CONTAS_RECEBER COR ');

  vSql.Add('inner join CADASTROS CAD ');
  vSql.Add('on COR.CADASTRO_ID = CAD.CADASTRO_ID ');

  vSql.Add('where COR.COBRANCA_ID in(select COBRANCA_ID from TIPOS_COBRANCA where FORMA_PAGAMENTO = ''CRT'') ');
  vSql.Add('and COR.DATA_CADASTRO between :P1 and :P1 + 2 ');
  vSql.Add('and COR.NUMERO_PARCELAS = :P2 ');

  vSql.Add('group by ');
  vSql.Add('  COR.CADASTRO_ID, ');
  vSql.Add('  COR.ITEM_ID_CRT_ORCAMENTO, ');
  vSql.Add('  CAD.RAZAO_SOCIAL, ');
  vSql.Add('  COR.NUMERO_PARCELAS, ');
  vSql.Add('  COR.ORIGEM, ');
  vSql.Add('  COR.BAIXA_ORIGEM_ID, ');
  vSql.Add('  COR.COBRANCA_ID, ');
  vSql.Add('  COR.DATA_CADASTRO, ');
  vSql.Add('  COR.ORCAMENTO_ID, ');
  vSql.Add('  COR.ACUMULADO_ID ');

  vSql.Add('having sum(VALOR_DOCUMENTO) between :P3 - 0.05 and :P3 + 0.05 ');

  if vSql.Pesquisar([pDataTransacao, pQtdeParcelas, pValorTransacao]) then begin
    Result.ClienteId      := vSql.getInt('CADASTRO_ID');
    Result.NomeCliente    := vSql.GetString('NOME_CLIENTE');
    Result.Origem         := vSql.GetString('ORIGEM');
    Result.CobrancaId     := vSql.getInt('COBRANCA_ID');
    Result.ItemIdCartao   := vSql.getInt('ITEM_ID_CRT_ORCAMENTO');

    Result.DataCadastro   := vSql.getData('DATA_CADASTRO');
    Result.ValorCartao    := vSql.GetDouble('VALOR_TRANSACAO');
    Result.QtdeParcelas   := vSql.GetInt('NUMERO_PARCELAS');

    Result.TipoMovimento :=
      _Biblioteca.Decode(
        Result.Origem,[
        'ORC', 'Pedido',
        'BXR', 'Baixa contas rec.',
        'ACU', 'Acumulado']
      );

    Result.MovimentoId :=
      _Biblioteca.Decode(
        Result.Origem,[
        'ORC', vSql.GetInt('ORCAMENTO_ID'),
        'BXR', vSql.GetInt('BAIXA_ORIGEM_ID'),
        'ACU', vSql.GetInt('ACUMULADO_ID')
        ]
      );
  end;

  vSql.Active := False;
  vSql.Free;
end;

function AtualizarDadosCartoes(pConexao: TConexao; pCartoes: TArray<RecCartoesVinculacao>): RecRetornoBD;
var
  i: Integer;
  vExec: TExecucao;
begin
  Result.Iniciar;
  vExec := TExecucao.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    for i := Low(pCartoes) to High(pCartoes) do begin
      vExec.Limpar;

      if pCartoes[i].ReceberId > 0 then begin
        vExec.Add('update CONTAS_RECEBER set');
        vExec.Add('  NUMERO_CARTAO_TRUNCADO = :P2, ');
        vExec.Add('  NSU = :P3, ');
        vExec.Add('  CODIGO_AUTORIZACAO_TEF = :P4 ');
        vExec.Add('where RECEBER_ID = :P1');

        vExec.Executar([
          pCartoes[i].ReceberId,
          pCartoes[i].NumeroCartao,
          pCartoes[i].Nsu,
          pCartoes[i].CodigoAutorizacao
        ]);
      end
      else begin
        vExec.Add('update CONTAS_RECEBER set');
        vExec.Add('  NUMERO_CARTAO_TRUNCADO = :P6, ');
        vExec.Add('  NSU = :P7 ');

        if pCartoes[i].Origem = 'ORC' then
          vExec.Add('where ORCAMENTO_ID = :P1 ')
        else if pCartoes[i].Origem = 'ACU' then
          vExec.Add('where ACUMULADO_ID = :P1 ')
        else
          vExec.Add('where BAIXA_ORIGEM_ID = :P1 ');

        vExec.Add('and ITEM_ID_CRT_ORCAMENTO = :P2 ');
        vExec.Add('and ORIGEM = :P3 ');
        vExec.Add('and NUMERO_PARCELAS = :P4 ');
        vExec.Add('and COBRANCA_ID = :P5 ');

        vExec.Executar([
          pCartoes[i].MovimentoId,
          pCartoes[i].ItemIdCartao,
          pCartoes[i].Origem,
          pCartoes[i].QtdeParcelas,
          pCartoes[i].CobrancaId,
          pCartoes[i].NumeroCartao,
          pCartoes[i].Nsu
        ]);
      end;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
end;

function AtualizarContaCustodia(
  pConexao: TConexao;
  pReceberIds: TArray<Integer>;
  pContaCustodiaId: Integer;
  pObservacoesContaCustodia: string
): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_INFO_CONTAS_RECEBER');

  vExec := TExecucao.Create(pConexao);

  vExec.Add('update CONTAS_RECEBER set');
  vExec.Add('  CONTA_CUSTODIA_ID = :P1, ');
  vExec.Add('  OBSERVACOES_CONTA_CUSTODIA = :P2 ');
  vExec.Add('where ' + FiltroInInt('RECEBER_ID', pReceberIds));

  try
    pConexao.IniciarTransacao;

    if pContaCustodiaId = 0 then
      vExec.Executar([null, null])
    else
      vExec.Executar([pContaCustodiaId, pObservacoesContaCustodia]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
end;

function BloquearVendaPorQtdeDiasTitulosVencidos(pConexao: TConexao; pClienteId: Integer; pQtdeDiasBloquear: Integer): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  count(*) ');
  vSql.Add('from ');
  vSql.Add('  CONTAS_RECEBER COR ');

  vSql.Add('inner join TIPOS_COBRANCA TCO ');
  vSql.Add('on COR.COBRANCA_ID = TCO.COBRANCA_ID ');

  vSql.Add('where COR.CADASTRO_ID = :P1 ');
  vSql.Add('and trunc(sysdate) - COR.DATA_VENCIMENTO > :P2 ');
  vSql.Add('and COR.STATUS = ''A'' ');
  vSql.Add('and TCO.UTILIZAR_LIMITE_CRED_CLIENTE = ''S'' ');

  vSql.Pesquisar([pClienteId, pQtdeDiasBloquear]);
  Result := vSql.GetInt(0) > 0;

  vSql.Free;
end;

function AtualizarValorRetencao(pConexao: TConexao; pReceberId: Integer; pValorRetencao: Double): RecRetornoBD;
var
  vExecucao: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_VALOR_RETENCAO');
  vExecucao := TExecucao.Create(pConexao);

  vExecucao.Add('update CONTAS_RECEBER set ');
  vExecucao.Add('  VALOR_RETENCAO = :P2 ');
  vExecucao.Add('where RECEBER_ID = :P1');
  try
    pConexao.IniciarTransacao;

    vExecucao.Executar([pReceberId, pValorRetencao]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
end;

function AtualizarCampoDocumento(pConexao: TConexao; pReceberId: Integer; pDocumento: string): RecRetornoBD;
var
  vExecucao: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_DOCUMENTO_CONTAS_PAGAR');
  vExecucao := TExecucao.Create(pConexao);

  vExecucao.Add('update CONTAS_RECEBER set ');
  vExecucao.Add('  DOCUMENTO = :P2 ');
  vExecucao.Add('where RECEBER_ID = :P1');
  try
    pConexao.IniciarTransacao;

    vExecucao.Executar([pReceberId, pDocumento]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
end;

function AtualizarCentroCusto(pConexao: TConexao; pReceberId: Integer; pCentroCustoId: Integer): RecRetornoBD;
var
  vExecucao: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATU_CENTRO_CUSTO_CONTAS_PAGAR');
  vExecucao := TExecucao.Create(pConexao);

  vExecucao.Add('update CONTAS_RECEBER set ');
  vExecucao.Add('  CENTRO_CUSTO_ID = :P2 ');
  vExecucao.Add('where RECEBER_ID = :P1');
  try
    pConexao.IniciarTransacao;

    vExecucao.Executar([pReceberId, pCentroCustoId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
end;

function AtualizarNossoNumeroCodBarraLinhaDigitavel(pConexao: TConexao; pReceberId: Integer; pNossoNumero, pCodigoBarras, pLinhaDigitavel: String): RecRetornoBD;
var
  vExecucao: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_NOSSO_NUMERO_CODBARRAS_LINHADIG');
  vExecucao := TExecucao.Create(pConexao);

  vExecucao.Add('update CONTAS_RECEBER set ');
  vExecucao.Add('  NOSSO_NUMERO = :P2, ');
  vExecucao.Add('  CODIGO_BARRAS = :P3, ');
  vExecucao.Add('  LINHA_DIGITAVEL = :P4 ');
  vExecucao.Add('where RECEBER_ID = :P1');
  try
    if not pConexao.EmTransacao then
      pConexao.IniciarTransacao;

    vExecucao.Executar([pReceberId,
                        pNossoNumero,
                        pCodigoBarras,
                        pLinhaDigitavel]);

    if not pConexao.EmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pConexao.EmTransacao then
        pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
end;

function AtualizarEmitiuBoleto(pConexao: TConexao; pReceberId: Integer; pEmitiuBoleto: String): RecRetornoBD;
var
  vExecucao: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_EMITIU_BOLETO');
  vExecucao := TExecucao.Create(pConexao);

  vExecucao.Add('update CONTAS_RECEBER set ');
  vExecucao.Add('  EMITIU_BOLETO_DUPLICATA = :P2 ');
  vExecucao.Add('where RECEBER_ID = :P1');
  try
    if not pConexao.EmTransacao then
      pConexao.IniciarTransacao;

    vExecucao.Executar([pReceberId,
                        pEmitiuBoleto]);

    if not pConexao.EmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pConexao.EmTransacao then
        pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
end;

function AtualizarContaReceberArquivoRetorno(pConexao: TConexao; pReceberId: Integer; pDataContabil: TDateTime; pValorJuros, pValorMulta: Double): RecRetornoBD;
var
  vExecucao: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_EMITIU_BOLETO');
  vExecucao := TExecucao.Create(pConexao);

  vExecucao.Add('update CONTAS_RECEBER set ');
  vExecucao.Add('  DATA_CONTABIL = :P2, ');
  vExecucao.Add('  VALOR_JUROS   = :P3, ');
  vExecucao.Add('  VALOR_MULTA   = :P4 ');

  vExecucao.Add('where RECEBER_ID = :P1');
  try
    if not pConexao.EmTransacao then
      pConexao.IniciarTransacao;

    vExecucao.Executar([pReceberId,
                        pDataContabil,
                        pValorJuros,
                        pValorMulta]);

    if pConexao.EmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pConexao.EmTransacao then
        pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

end;

end.
