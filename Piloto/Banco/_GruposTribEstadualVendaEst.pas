unit _GruposTribEstadualVendaEst;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecGruposTribEstadualVendaEst = record
    GrupoTribEstadualVendaId: Integer;
    EstadoId: string;
    CstNaoContribuinte: string;
    CstContribuinte: string;
    CstOrgaoPublico: string;
    CstRevenda: string;
    CstConstrutora: string;
    CstClinicaHospital: string;
    CstProdutorRural: string;
    IndiceReducaoBaseIcms: Double;
    PercentualIcms: Double;
    PercentualIcmsInter: Double;
    Iva: Double;
    PrecoPauta: Double;
  end;

  TGruposTribEstadualVendaEst = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordGruposTribEstadualVendaEst: RecGruposTribEstadualVendaEst;
  end;

function BuscarGruposTribEstadualVendaEst(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecGruposTribEstadualVendaEst>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TGruposTribEstadualVendaEst }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where GRUPO_TRIB_ESTADUAL_VENDA_ID = :P1'
    );
end;

constructor TGruposTribEstadualVendaEst.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'GRUPOS_TRIB_ESTADUAL_VENDA_EST');

  FSql := 
    'select ' +
    '  GRUPO_TRIB_ESTADUAL_VENDA_ID, ' +
    '  ESTADO_ID, ' +
    '  CST_NAO_CONTRIBUINTE, ' +
    '  CST_CONTRIBUINTE, ' +
    '  CST_ORGAO_PUBLICO, ' +
    '  CST_REVENDA, ' +
    '  CST_CONSTRUTORA, ' +
    '  CST_CLINICA_HOSPITAL, ' +
    '  CST_PRODUTOR_RURAL, ' +
    '  INDICE_REDUCAO_BASE_ICMS, ' +
    '  PERCENTUAL_ICMS, ' +
    '  PERCENTUAL_ICMS_INTER, ' +
    '  IVA, ' +
    '  PRECO_PAUTA ' +
    'from ' +
    '  GRUPOS_TRIB_ESTADUAL_VENDA_EST';

  setFiltros(getFiltros);

  AddColuna('GRUPO_TRIB_ESTADUAL_VENDA_ID', True);
  AddColuna('ESTADO_ID', True);
  AddColuna('CST_NAO_CONTRIBUINTE');
  AddColuna('CST_CONTRIBUINTE');
  AddColuna('CST_ORGAO_PUBLICO');
  AddColuna('CST_REVENDA');
  AddColuna('CST_CONSTRUTORA');
  AddColuna('CST_CLINICA_HOSPITAL');
  AddColuna('CST_PRODUTOR_RURAL');
  AddColuna('INDICE_REDUCAO_BASE_ICMS');
  AddColuna('PERCENTUAL_ICMS');
  AddColuna('PERCENTUAL_ICMS_INTER');
  AddColuna('IVA');
  AddColuna('PRECO_PAUTA');
end;

function TGruposTribEstadualVendaEst.getRecordGruposTribEstadualVendaEst: RecGruposTribEstadualVendaEst;
begin
  Result.GrupoTribEstadualVendaId   := getInt('GRUPO_TRIB_ESTADUAL_VENDA_ID', True);
  Result.EstadoId                   := getString('ESTADO_ID', True);
  Result.CstNaoContribuinte         := getString('CST_NAO_CONTRIBUINTE');
  Result.CstContribuinte            := getString('CST_CONTRIBUINTE');
  Result.CstOrgaoPublico            := getString('CST_ORGAO_PUBLICO');
  Result.CstRevenda                 := getString('CST_REVENDA');
  Result.CstConstrutora             := getString('CST_CONSTRUTORA');
  Result.CstClinicaHospital         := getString('CST_CLINICA_HOSPITAL');
  Result.CstProdutorRural           := getString('CST_PRODUTOR_RURAL');
  Result.IndiceReducaoBaseIcms      := getDouble('INDICE_REDUCAO_BASE_ICMS');
  Result.PercentualIcms             := getDouble('PERCENTUAL_ICMS');
  Result.PercentualIcmsInter        := getDouble('PERCENTUAL_ICMS_INTER');
  Result.Iva                        := getDouble('IVA');
  Result.PrecoPauta                 := getDouble('PRECO_PAUTA');
end;

function BuscarGruposTribEstadualVendaEst(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecGruposTribEstadualVendaEst>;
var
  i: Integer;
  t: TGruposTribEstadualVendaEst;
begin
  Result := nil;
  t := TGruposTribEstadualVendaEst.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordGruposTribEstadualVendaEst;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.

// ALTER TABLE GRUPOS_TRIB_ESTADUAL_VENDA_EST ADD PERCENTUAL_ICMS_INTER NUMBER(4,2)
