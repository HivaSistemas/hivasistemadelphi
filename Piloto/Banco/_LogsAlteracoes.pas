unit _LogsAlteracoes;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsCadastros, System.SysUtils, _Biblioteca;

function BuscarLogs(
  pConexao: TConexao;
  pTabela: string;
  pColunaFiltro: TArray<string>;
  pView: string;
  pFiltro: TArray<Variant>;
  pLogsComEmpresa: Boolean
): TArray<RecLogsAlteracoes>;

implementation

function BuscarLogs(
  pConexao: TConexao;
  pTabela: string;
  pColunaFiltro: TArray<string>;
  pView: string;
  pFiltro: TArray<Variant>;
  pLogsComEmpresa: Boolean
): TArray<RecLogsAlteracoes>;
var
  i: Integer;
  vSql: TConsulta;
  vComando: string;
begin
  Result := nil;
  vComando := '';
  pConexao.SetRotina('BUSCAR_LOGS');
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  LOG.TIPO_ALTERACAO_LOG_ID, ');
  vSql.Add('  VW.CAMPO, ');
  vSql.Add('  LOG.VALOR_ANTERIOR, ');
  vSql.Add('  LOG.NOVO_VALOR, ');
  vSql.Add('  LOG.USUARIO_SESSAO_ID || '' - '' || nvl(FUN.APELIDO, FUN.NOME) as NOME_USUARIO_ALTERACAO, ');
  vSql.Add('  LOG.DATA_HORA_ALTERACAO, ');
  vSql.Add('  LOG.ROTINA_ALTERACAO ');

  if pLogsComEmpresa then begin
    vSql.Add('  ,LOG.EMPRESA_ID, ');
    vSql.Add('  EMP.NOME_FANTASIA as NOME_EMPRESA ');
  end;

  vSql.Add('from ');
  vSql.Add('  ' + pTabela + ' LOG ');

  vSql.Add('inner join ' + pView + ' VW');
  vSql.Add('on LOG.TIPO_ALTERACAO_LOG_ID = VW.TIPO_ALTERACAO_LOG_ID ');

  vSql.Add('inner join FUNCIONARIOS FUN ');
  vSql.Add('on LOG.USUARIO_SESSAO_ID = FUN.FUNCIONARIO_ID ');

  if pLogsComEmpresa then begin
    vSql.Add('inner join EMPRESAS EMP ');
    vSql.Add('on LOG.EMPRESA_ID = EMP.EMPRESA_ID ');
  end;

  for i := Low(pColunaFiltro) to High(pColunaFiltro) do
    _Biblioteca.WhereOuAnd(vComando, ' LOG.' + pColunaFiltro[i] + ' = :P' + IntToStr(i + 1));

  vSql.Add(vComando);

  vSql.Add('order by ');
  vSql.Add('  LOG.DATA_HORA_ALTERACAO desc ');

  if vSql.Pesquisar(pFiltro) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].tipoAlteracaoLogId := vSql.getInt('TIPO_ALTERACAO_LOG_ID');
      Result[i].campo              := vSql.GetString('CAMPO');
      Result[i].valorAnterior      := vSql.GetString('VALOR_ANTERIOR');
      Result[i].novoValor          := vSql.GetString('NOVO_VALOR');
      Result[i].usuarioAlteracao   := vSql.GetString('NOME_USUARIO_ALTERACAO');
      Result[i].dataHoraAlteracao  := vSql.GetData('DATA_HORA_ALTERACAO');
      Result[i].rotinaAlteracao    := vSql.GetString('ROTINA_ALTERACAO');
      if pLogsComEmpresa then begin
        Result[i].EmpresaId        := vSql.GetInt('EMPRESA_ID');
        Result[i].NomeEmpresa      := vSql.GetString('NOME_EMPRESA');
      end;

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

end.
