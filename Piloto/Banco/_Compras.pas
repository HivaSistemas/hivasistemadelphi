unit _Compras;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsOrcamentosVendas, _BibliotecaGenerica, _ComprasItens, _ComprasPrevisoes,
  _RecordsFinanceiros;

{$M+}
type
  RecCompras = record
    CompraId: Integer;
    FornecedorId: Integer;
    NomeFornecedor: string;
    EmpresaId: Integer;
    NomeEmpresa: string;
    CompradorId: Integer;
    NomeComprador: string;
    DataFaturamento: TDateTime;
    PrevisaoEntrega: TDateTime;
    DataHoraCompra: TDateTime;
    UsuarioCadastroId: Integer;
    Status: string;
    ValorLiquido: Currency;
    StatusAnalitico: string;
    DataHoraBaixa: TDateTime;
    UsuarioBaixaId: Integer;
    DataHoraCancelamento: TDateTime;
    UsuarioCancelamentoId: Integer;
    Observacoes: string;
    PlanoFinanceiroId: string;
    NomePlanoFinanceiro: string;
  end;

  TCompras = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordCompras: RecCompras;
  end;

function AtualizarCompras(
  pConexao: TConexao;
  pCompraId: Integer;
  pFornecedorId: Integer;
  pEmpresaId: Integer;
  pCompradorId: Integer;
  pDataFaturamento: TDateTime;
  pPrevisaoEntrega: TDateTime;
  pObservacoes: string;
  pPlanoFinanceiroId: string;
  pValorLiquido: Currency;
  pItens: TArray<RecComprasItens>;
  pTitulos: TArray<RecTitulosFinanceiros>
): RecRetornoBD;

function BuscarCompras(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCompras>;

function BuscarComprasComando(pConexao: TConexao; pComando: string): TArray<RecCompras>;

function ExcluirCompras(
  pConexao: TConexao;
  pDevolucaoId: Integer
): RecRetornoBD;

function ExistemProdutosEntreguesBaixadosCancelados(pConexao: TConexao; pCompraId: Integer): Boolean;

function getFiltros: TArray<RecFiltros>;

implementation

{ TCompras }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where COM.COMPRA_ID = :P1 '
    );
end;

constructor TCompras.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'COMPRAS');

  FSql :=
    'select ' +
    '  COM.COMPRA_ID, ' +
    '  COM.FORNECEDOR_ID, ' +
    '  CAD.RAZAO_SOCIAL as NOME_FORNECEDOR, ' +
    '  COM.EMPRESA_ID, ' +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA, ' +
    '  COM.COMPRADOR_ID, ' +
    '  FUN.NOME as NOME_COMPRADOR, ' +
    '  COM.DATA_FATURAMENTO, ' +
    '  COM.PREVISAO_ENTREGA, ' +
    '  COM.DATA_HORA_COMPRA, ' +
    '  COM.USUARIO_CADASTRO_ID, ' +
    '  COM.STATUS, ' +
    '  COM.VALOR_LIQUIDO, ' +
    '  COM.DATA_HORA_BAIXA, ' +
    '  COM.USUARIO_BAIXA_ID, ' +
    '  COM.DATA_HORA_CANCELAMENTO, ' +
    '  COM.USUARIO_CANCELAMENTO_ID, ' +
    '  COM.OBSERVACOES, ' +
    '  COM.PLANO_FINANCEIRO_ID, ' +
    '  PLF.DESCRICAO as NOME_PLANO_FINANCEIRO ' +
    'from ' +
    '  COMPRAS COM ' +

    'inner join EMPRESAS EMP ' +
    'on COM.EMPRESA_ID = EMP.EMPRESA_ID ' +

    'inner join FUNCIONARIOS FUN ' +
    'on COM.COMPRADOR_ID = FUN.FUNCIONARIO_ID ' +

    'inner join CADASTROS CAD ' +
    'on COM.FORNECEDOR_ID = CAD.CADASTRO_ID ' +

    'left join FORNECEDORES FRN ' +
    'on CAD.CADASTRO_ID = FRN.CADASTRO_ID ' +

    'left join PLANOS_FINANCEIROS PLF ' +
    'on COM.PLANO_FINANCEIRO_ID = PLF.PLANO_FINANCEIRO_ID ';

  setFiltros(getFiltros);

  AddColuna('COMPRA_ID', True);
  AddColuna('FORNECEDOR_ID');
  AddColunaSL('NOME_FORNECEDOR');
  AddColuna('EMPRESA_ID');
  AddColunaSL('NOME_EMPRESA');
  AddColuna('COMPRADOR_ID');
  AddColunaSL('NOME_COMPRADOR');
  AddColuna('DATA_FATURAMENTO');
  AddColuna('PREVISAO_ENTREGA');
  AddColunaSL('DATA_HORA_COMPRA');
  AddColunaSL('USUARIO_CADASTRO_ID');
  AddColunaSL('STATUS');
  AddColuna('VALOR_LIQUIDO');
  AddColunaSL('DATA_HORA_BAIXA');
  AddColunaSL('USUARIO_BAIXA_ID');
  AddColunaSL('DATA_HORA_CANCELAMENTO');
  AddColunaSL('USUARIO_CANCELAMENTO_ID');
  AddColuna('OBSERVACOES');
  AddColuna('PLANO_FINANCEIRO_ID');
  AddColunaSL('NOME_PLANO_FINANCEIRO');
end;

function TCompras.getRecordCompras: RecCompras;
begin
  Result.CompraId              := getInt('COMPRA_ID', True);
  Result.FornecedorId          := getInt('FORNECEDOR_ID');
  Result.NomeFornecedor        := getString('NOME_FORNECEDOR');
  Result.EmpresaId             := getInt('EMPRESA_ID');
  Result.NomeEmpresa           := getString('NOME_EMPRESA');
  Result.CompradorId           := getInt('COMPRADOR_ID');
  Result.NomeComprador         := getString('NOME_COMPRADOR');
  Result.DataFaturamento       := getData('DATA_FATURAMENTO');
  Result.PrevisaoEntrega       := getData('PREVISAO_ENTREGA');
  Result.DataHoraCompra        := getData('DATA_HORA_COMPRA');
  Result.UsuarioCadastroId     := getInt('USUARIO_CADASTRO_ID');
  Result.Status                := getString('STATUS');
  Result.ValorLiquido          := getDouble('VALOR_LIQUIDO');
  Result.StatusAnalitico       := _Biblioteca.Decode(Result.Status, ['A', 'Aberto', 'B', 'Baixado', 'C', 'Cancelado']);
  Result.DataHoraBaixa         := getData('DATA_HORA_BAIXA');
  Result.UsuarioBaixaId        := getInt('USUARIO_BAIXA_ID');
  Result.DataHoraCancelamento  := getData('DATA_HORA_CANCELAMENTO');
  Result.UsuarioCancelamentoId := getInt('USUARIO_CANCELAMENTO_ID');
  Result.Observacoes           := getString('OBSERVACOES');
  Result.PlanoFinanceiroId     := getString('PLANO_FINANCEIRO_ID');
  Result.NomePlanoFinanceiro   := getString('NOME_PLANO_FINANCEIRO');
end;

function AtualizarCompras(
  pConexao: TConexao;
  pCompraId: Integer;
  pFornecedorId: Integer;
  pEmpresaId: Integer;
  pCompradorId: Integer;
  pDataFaturamento: TDateTime;
  pPrevisaoEntrega: TDateTime;
  pObservacoes: string;
  pPlanoFinanceiroId: string;
  pValorLiquido: Currency;
  pItens: TArray<RecComprasItens>;
  pTitulos: TArray<RecTitulosFinanceiros>
): RecRetornoBD;
var
  i: Integer;
  t: TCompras;
  vNovo: Boolean;
  vSeq: TSequencia;
  vItem: TComprasItens;
  vTitulo: TComprasPrevisoes;

  vExec: TExecucao;
begin
  Result.TeveErro := False;
  t := TCompras.Create(pConexao);
  vItem := TComprasItens.Create(pConexao);
  vTitulo := TComprasPrevisoes.Create(pConexao);
  pConexao.SetRotina('ATUALIZAR_COMPRA');

  vExec := TExecucao.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    vNovo := pCompraId = 0;
    if vNovo then begin
      vSeq := TSequencia.Create(pConexao, 'SEQ_COMPRA_ID');
      pCompraId := vSeq.getProximaSequencia;
      Result.AsInt := pCompraId;
      vSeq.Free;
    end;

    // Deletando os itens aqui em cima antes de atualizar a tabela de compras por causa da pend�ncia de compras das empresas
    // Muito cuidado ao alterar para n�o gerar a pend�ncia de compra de maneira errada!!!
    if not vNovo then begin
      vExec.Clear;
      vExec.Add('delete from COMPRAS_ITENS');
      vExec.Add('where COMPRA_ID = :P1 ');
      vExec.Executar([pCompraId]);
    end;

    t.setInt('COMPRA_ID', pCompraId, True);
    t.setInt('FORNECEDOR_ID', pFornecedorId);
    t.setInt('EMPRESA_ID', pEmpresaId);
    t.setInt('COMPRADOR_ID', pCompradorId);
    t.setData('DATA_FATURAMENTO', pDataFaturamento);
    t.setData('PREVISAO_ENTREGA', pPrevisaoEntrega);
    t.setString('OBSERVACOES', pObservacoes);
    t.setString('PLANO_FINANCEIRO_ID', pPlanoFinanceiroId);
    t.setDouble('VALOR_LIQUIDO', pValorLiquido);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    vItem.SetInt('COMPRA_ID', pCompraId, True);
    for i := Low(pItens) to High(pItens) do begin
      vItem.setInt('ITEM_ID', i + 1, True);
      vItem.setInt('PRODUTO_ID', pItens[i].ProdutoId);
      vItem.setDouble('MULTIPLO_COMPRA', pItens[i].MultiploCompra);
      vItem.setString('UNIDADE_COMPRA_ID', pItens[i].UnidadeCompraId);
      vItem.setDouble('QUANTIDADE_EMBALAGEM', pItens[i].QuantidadeEmbalagem);
      vItem.setDouble('QUANTIDADE', pItens[i].Quantidade);
      vItem.setDouble('PRECO_UNITARIO', pItens[i].PrecoUnitario);
      vItem.setDouble('VALOR_TOTAL', pItens[i].ValorTotal);
      vItem.setDouble('VALOR_DESCONTO', pItens[i].ValorDesconto);
      vItem.setDouble('VALOR_OUTRAS_DESPESAS', pItens[i].ValorOutrasDespesas);
      vItem.setDouble('VALOR_LIQUIDO', pItens[i].ValorLiquido);

      vItem.Inserir;
    end;

    if not vNovo then begin
      vExec.Clear;
      vExec.Add('delete from COMPRAS_PREVISOES');
      vExec.Add('where COMPRA_ID = :P1 ');
      vExec.Executar([pCompraId]);
    end;

    vTitulo.SetInt('COMPRA_ID', pCompraId, True);
    for i := Low(pTitulos) to High(pTitulos) do begin
      vTitulo.setInt('COBRANCA_ID', pTitulos[i].CobrancaId, True);
      vTitulo.setInt('ITEM_ID', pTitulos[i].ItemId, True);
      vTitulo.setDouble('VALOR', pTitulos[i].Valor);
      vTitulo.setInt('PARCELA', pTitulos[i].Parcela);
      vTitulo.setInt('NUMERO_PARCELAS', pTitulos[i].NumeroParcelas);
      vTitulo.setData('DATA_VENCIMENTO', pTitulos[i].DataVencimento);
      vTitulo.setString('DOCUMENTO', pTitulos[i].Documento);

      vTitulo.Inserir;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vTitulo.Free;
  vExec.Free;
  vItem.Free;
  t.Free;
end;

function BuscarCompras(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCompras>;
var
  i: Integer;
  t: TCompras;
begin
  Result := nil;
  t := TCompras.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordCompras;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarComprasComando(pConexao: TConexao; pComando: string): TArray<RecCompras>;
var
  i: Integer;
  t: TCompras;
begin
  Result := nil;
  t := TCompras.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordCompras;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirCompras(
  pConexao: TConexao;
  pDevolucaoId: Integer
): RecRetornoBD;
var
  t: TCompras;
begin
  Result.TeveErro := False;
  t := TCompras.Create(pConexao);

  try
    t.setInt('DEVOLUCAO_ID', pDevolucaoId, True);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function ExistemProdutosEntreguesBaixadosCancelados(pConexao: TConexao; pCompraId: Integer): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  count(*) ');
  vSql.Add('from ');
  vSql.Add('  COMPRAS_ITENS ');
  vSql.Add('where COMPRA_ID = :P1 ');
  vSql.Add('and ENTREGUES + BAIXADOS + CANCELADOS > 0 ');
  vSql.Pesquisar([pCompraId]);

  Result := vSql.GetInt(0) > 0;

  vSql.Free;
end;

end.
