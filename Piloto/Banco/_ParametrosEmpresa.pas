unit _ParametrosEmpresa;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros, _RecordsNFE, System.Classes, System.Variants, Data.DB;

{$M+}
type
  RecParametrosComissao = record
    Faixa: Integer;
    ValorInicial: Double;
    ValorFinal: Double;
    Percentual: Double;
    TipoComissao: string;
  end;

  RecParametrosDevolucao = record
    Faixa: Integer;
    ValorInicial: Integer;
    ValorFinal: Integer;
    Percentual: Integer;
  end;

  TParametrosEmpresa = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  private
    function getRecordParametrosEmpresa: RecParametrosEmpresa;
  end;

function AtualizarParametrosEmpresa(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pUtilizaNFe: string;
  pValorMinDifTurnoContasRec: Double;
  pExigirNomeConsFinalVenda: string;
  pExirgirDadosCatao: string;
  pPermiteAltFormaPagtoCaixa: string;
  pPermAltFormaPagtoCxFinanc: string;
  pNumeroUltNFEEmitida: Integer;
  pCondicaoPagamentoPDV: Integer;
  pSerieNfe: Integer;
  pUltimoNsuConsultadoDistNFe: string;
  pUltimoNsuConsultadoDistCTe: string;
  pCorLucratividade1: Integer;
  pCorLucratividade2: Integer;
  pCorLucratividade3: Integer;
  pCorLucratividade4: Integer;
  pRegimeTributario: string;
  pAliquotaSimplesNacional: Double;
  pAliquotaPIS: Double;
  pAliquotaCOFINS: Double;
  pAliquotaCSLL: Double;
  pAliquotaIRPJ: Double;
  pCondPagtoPadraoPesqVenda: Integer;
  pUtilizaNfce: string;
  pNumeroUltNfceEmitida: Integer;
  pSerieNfce: Integer;
  pTipoAmbienteNFe: string;
  pTipoAmbienteNFCe: string;
  pInfComplDocsEletronicos: string;
  pTrabalhaRetirarAto: string;
  pTrabalhaRetirar: string;
  pTrabalhaEntregar: string;
  pTrabalhaSemPrevisao: string;
  pTrabalhaSemPrevisaoEntregar: string;
  pConfirmarSaidaProdutos: string;
  pTrabalharControleSeparacao: string;
  pCondicaoPagtoPadVendaAssis: Integer;
  pCondicaoPagamentoConsProd2: Integer;
  pCondicaoPagamentoConsProd3: Integer;
  pCondicaoPagamentoConsProd4: Integer;
  pIndiceSabado: Double;
  pIndiceDomingo: Double;
  pTipoPrecoUtilizarVenda: string;
  pLogoEmpresa: TMemoryStream;
  pImprimirLoteCompEntrega: string;
  pImprimirCodOriginalCompEnt: string;
  pImprimirCodBarrasCompEnt: string;
  pTipoCustoVisLucroVenda: string;
  pQtdeDiasValidadeOrcamento: Integer;
  pPercentualJurosMensal: Double;
  pPercentualMulta: Double;
  pTipoRedirecionamentoNota: string;
  pEmpresaRedNotaRetirarAtoId: Integer;
  pEmpresaRedNotaRetirarId: Integer;
  pEmpresaRedNotaEntregarId: Integer;
  pValorFretePadraoVenda: Double;
  pQtdeMinDiasEntregas: Integer;
  pObrigarVendedorSelTipoCobr: string;
  pTipoCustoAjusteEstoque: string;
  pTrabalharConferenciaRetAto: string;
  pTrabalharConferenciaRetirar: string;
  pTrabalharConferenciaEntrega: string;
  pPermBaixarEntRecPendente: string;
  pNumeroEstabelecimentCielo: string;
  pGerarNfAgrupada: string;
  pQtdeDiasBloqVendaTitVenc: Integer;
  pCalcularFreteVendaKM: string;
  pHabilitarRecebimentoPix: string;
  pBaixaContasReceberPixTurnoAberto: string;
  pControlarTransacao: Boolean;
  pInfComplCompPagamentos: string;
  pAlertaEntregaPendente: string;
  pSerieCertificado: string;
  pSenhaCertificado: string;
  pImpressoraNFe: string;
  pCscNFce: string;
  pIdToken: string;
  pModeloImpressora: string;
  pCertificado: TMemoryStream;
  pPermitirDevolucaoAposPrazo: string
): RecRetornoBD;

function BuscarParametrosEmpresas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecParametrosEmpresa>;

function getDadosProximaNfe(pConexao: TConexao; pNotaFiscalId: Integer): RecDadosGeracaoNFe;
function AtualizarUltimoNSUDistribuicaoNFeCTe(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pUltimoNSU: string;
  pTipo: string;
  pEmTransacao: Boolean
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

function AtualizarParametrosComissao(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pFaixas: TArray<RecParametrosComissao>;
  pTipoComissao: string
): RecRetornoBD;

function AtualizarParametrosComissaoMetaFuncionario(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pFaixas: TArray<RecParametrosComissao>;
  pFuncionarioId: Integer
): RecRetornoBD;

function AtualizarParametrosDevolucao(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pFaixas: TArray<RecParametrosDevolucao>
): RecRetornoBD;

function BuscarParametrosComissao(
  pConexao: TConexao;
  pEmpresaId: Integer
): TArray<RecParametrosComissao>;

function BuscarParametrosMetasFuncionario(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pFuncionarioId: Integer
): TArray<RecParametrosComissao>;

function BuscarParametrosDiasDevolucao(
  pConexao: TConexao;
  pEmpresaId: Integer
): TArray<RecParametrosDevolucao>;

implementation

{ TParametrosEmpresa }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where EMPRESA_ID = :P1'
    );
end;

constructor TParametrosEmpresa.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PARAMETROS_EMPRESA');

  FSql :=
    'select ' +
    '  PAE.EMPRESA_ID, ' +
    '  PAE.UTILIZA_NFE, ' +
    '  PAE.VALOR_MIN_DIF_TURNO_CONTAS_REC, ' +
    '  PAE.EXIGIR_NOME_CONS_FINAL_VENDA, ' +
    '  PAE.EXIRGIR_DADOS_CARTAO, ' +
    '  PAE.PERMITE_ALT_FORMA_PAGTO_CAIXA, ' +
    '  PAE.PERM_ALT_FORMA_PAGTO_CX_FINANC, ' +
    '  PAE.NUMERO_ULT_NFE_EMITIDA, ' +
    '  PAE.CONDICAO_PAGAMENTO_PDV, ' +
    '  PAE.SERIE_NFE, ' +
    '  PAE.ULTIMO_NSU_CONSULTADO_DIST_NFE, ' +
    '  PAE.ULTIMO_NSU_CONSULTADO_DIST_CTE, ' +
    '  PAE.COR_LUCRATIVIDADE_1, ' +
    '  PAE.COR_LUCRATIVIDADE_2, ' +
    '  PAE.COR_LUCRATIVIDADE_3, ' +
    '  PAE.COR_LUCRATIVIDADE_4, ' +
    '  PAE.REGIME_TRIBUTARIO, ' +
    '  PAE.ALIQUOTA_SIMPLES_NACIONAL, ' +
    '  PAE.ALIQUOTA_PIS, ' +
    '  PAE.ALIQUOTA_COFINS, ' +
    '  PAE.ALIQUOTA_CSLL, ' +
    '  PAE.ALIQUOTA_IRPJ, ' +
    '  PAE.COND_PAGTO_PADRAO_PESQ_VENDA, ' +
    '  PAE.UTILIZA_NFCE, ' +
    '  PAE.NUMERO_ULT_NFCE_EMITIDA, ' +
    '  PAE.SERIE_NFCE, ' +
    '  PAE.TIPO_AMBIENTE_NFE, ' +
    '  PAE.TIPO_AMBIENTE_NFCE, ' +
    '  PAE.INF_COMPL_DOCS_ELETRONICOS, ' +
    '  PAE.TRABALHA_RETIRAR_ATO, ' +
    '  PAE.TRABALHA_RETIRAR, ' +
    '  PAE.TRABALHA_ENTREGAR, ' +
    '  PAE.TRABALHA_SEM_PREVISAO, ' +
    '  PAE.TRABALHA_SEM_PREVISAO_ENTREGAR, ' +
    '  PAE.CONFIRMAR_SAIDA_PRODUTOS, ' +
    '  PAE.TRABALHAR_CONTROLE_SEPARACAO, ' +
    '  PAE.INF_COMPL_COMP_PAGAMENTOS, ' +
    '  PAE.TIPO_PRECO_UTILIZAR_VENDA, ' +
    '  PAE.CONDICAO_PAGTO_PAD_VENDA_ASSIS, ' +
    '  PAE.CONDICAO_PAGAMENTO_CONS_PROD_2, ' +
    '  CON2.NOME as NOME_COND_PAGTO_CONS2, ' +
    '  CON2.INDICE_ACRESCIMO as INDICE_COND_PAGTO_CONS2, ' +
    '  PAE.CONDICAO_PAGAMENTO_CONS_PROD_3, ' +
    '  CON3.NOME as NOME_COND_PAGTO_CONS3, ' +
    '  CON3.INDICE_ACRESCIMO as INDICE_COND_PAGTO_CONS3, ' +
    '  PAE.CONDICAO_PAGAMENTO_CONS_PROD_4, ' +
    '  CON4.NOME as NOME_COND_PAGTO_CONS4, ' +
    '  CON4.INDICE_ACRESCIMO as INDICE_COND_PAGTO_CONS4, ' +
    '  PAE.INDICE_SABADO, ' +
    '  PAE.INDICE_DOMINGO, ' +
    '  PAE.IMPRIMIR_LOTE_COMP_ENTREGA, ' +
    '  PAE.IMPRIMIR_COD_ORIGINAL_COMP_ENT, ' +
    '  PAE.IMPRIMIR_COD_BARRAS_COMP_ENT, ' +
    '  PAE.TIPO_CUSTO_VIS_LUCRO_VENDA, ' +
    '  PAE.QTDE_DIAS_VALIDADE_ORCAMENTO, ' +
    '  PAE.PERCENTUAL_JUROS_MENSAL, ' +
    '  PAE.PERCENTUAL_MULTA, ' +
    '  PAE.TIPO_REDIRECIONAMENTO_NOTA, ' +
    '  PAE.EMPRESA_RED_NOTA_RETIRA_ATO_ID, ' +
    '  PAE.EMPRESA_RED_NOTA_RETIRAR_ID, ' +
    '  PAE.EMPRESA_RED_NOTA_ENTREGAR_ID, ' +
    '  PAE.VALOR_FRETE_PADRAO_VENDA, ' +
    '  PAE.QUANTIDADE_MIN_DIAS_ENTREGA, ' +
    '  PAE.OBRIGAR_VENDEDOR_SEL_TIPO_COBR, ' +
    '  PAE.TIPO_CUSTO_AJUSTE_ESTOQUE, ' +
    '  PAE.TRABALHAR_CONFERENCIA_RET_ATO, ' +
    '  PAE.TRABALHAR_CONFERENCIA_RETIRAR, ' +
    '  PAE.TRABALHAR_CONFERENCIA_ENTREGA, ' +
    '  PAE.PERM_BAIXAR_ENT_REC_PENDENTE, ' +
    '  PAE.NUMERO_ESTABELECIMENTO_CIELO, ' +
    '  PAE.GERAR_NOTA_AGRP_ATO, ' +
    '  PAE.CALCULAR_FRETE_VENDA_KM, ' +
    '  PAE.QTDE_DIAS_BLOQ_VENDA_TIT_VENC, ' +
    '  PAE.HABILITAR_RECEBIMENTO_PIX, ' +
    '  PAE.BX_CON_REC_PIX_SOM_TURN_ABERTO, ' +
    '  PAE.ALERTA_ENTREGA_PENDENTE, ' +
    '  PAE.SERIE_CERTIFICADO, ' +
    '  PAE.SENHA_CERTIFICADO, ' +
    '  PAE.IMPRESSORA_NFE, ' +
    '  PAE.CERTIFICADO, ' +
    '  PAE.CSC_NFCE, ' +
    '  PAE.ID_TOKEN, ' +
    '  PAE.MODELO_IMPRESSORA, '+
    '  PAE.PERMITIR_DEV_APOS_PRAZO ' +


    'from ' +
    '  PARAMETROS_EMPRESA PAE ' +

    'left join CONDICOES_PAGAMENTO CON2 ' +
    'on PAE.CONDICAO_PAGAMENTO_CONS_PROD_2 = CON2.CONDICAO_ID ' +

    'left join CONDICOES_PAGAMENTO CON3 ' +
    'on PAE.CONDICAO_PAGAMENTO_CONS_PROD_3 = CON3.CONDICAO_ID ' +

    'left join CONDICOES_PAGAMENTO CON4 ' +
    'on PAE.CONDICAO_PAGAMENTO_CONS_PROD_4 = CON4.CONDICAO_ID ';

  setFiltros(getFiltros);

  AddColuna('EMPRESA_ID', True);
  AddColuna('UTILIZA_NFE');
  AddColuna('VALOR_MIN_DIF_TURNO_CONTAS_REC');
  AddColuna('EXIGIR_NOME_CONS_FINAL_VENDA');
  AddColuna('EXIRGIR_DADOS_CARTAO');
  AddColuna('PERMITE_ALT_FORMA_PAGTO_CAIXA');
  AddColuna('PERM_ALT_FORMA_PAGTO_CX_FINANC');
  AddColuna('NUMERO_ULT_NFE_EMITIDA');
  AddColuna('CONDICAO_PAGAMENTO_PDV');
  AddColuna('SERIE_NFE');
  AddColuna('ULTIMO_NSU_CONSULTADO_DIST_NFE');
  AddColuna('ULTIMO_NSU_CONSULTADO_DIST_CTE');
  AddColuna('COR_LUCRATIVIDADE_1');
  AddColuna('COR_LUCRATIVIDADE_2');
  AddColuna('COR_LUCRATIVIDADE_3');
  AddColuna('COR_LUCRATIVIDADE_4');
  AddColuna('REGIME_TRIBUTARIO');
  AddColuna('ALIQUOTA_SIMPLES_NACIONAL');
  AddColuna('ALIQUOTA_PIS');
  AddColuna('ALIQUOTA_COFINS');
  AddColuna('ALIQUOTA_CSLL');
  AddColuna('ALIQUOTA_IRPJ');
  AddColuna('COND_PAGTO_PADRAO_PESQ_VENDA');
  AddColuna('UTILIZA_NFCE');
  AddColuna('NUMERO_ULT_NFCE_EMITIDA');
  AddColuna('SERIE_NFCE');
  AddColuna('TIPO_AMBIENTE_NFE');
  AddColuna('TIPO_AMBIENTE_NFCE');
  AddColuna('INF_COMPL_DOCS_ELETRONICOS');
  AddColuna('TRABALHA_RETIRAR_ATO');
  AddColuna('TRABALHA_RETIRAR');
  AddColuna('TRABALHA_ENTREGAR');
  AddColuna('TRABALHA_SEM_PREVISAO');
  AddColuna('TRABALHA_SEM_PREVISAO_ENTREGAR');
  AddColuna('CONFIRMAR_SAIDA_PRODUTOS');
  AddColuna('TRABALHAR_CONTROLE_SEPARACAO');
  AddColuna('TIPO_PRECO_UTILIZAR_VENDA');
  AddColuna('CONDICAO_PAGTO_PAD_VENDA_ASSIS');
  AddColuna('CONDICAO_PAGAMENTO_CONS_PROD_2');
  AddColuna('INDICE_SABADO');
  AddColuna('INDICE_DOMINGO');
  AddColuna('IMPRIMIR_LOTE_COMP_ENTREGA');
  AddColuna('IMPRIMIR_COD_ORIGINAL_COMP_ENT');
  AddColuna('IMPRIMIR_COD_BARRAS_COMP_ENT');
  AddColuna('PERCENTUAL_JUROS_MENSAL');
  AddColuna('PERCENTUAL_MULTA');
  AddColuna('TIPO_REDIRECIONAMENTO_NOTA');
  AddColuna('EMPRESA_RED_NOTA_RETIRA_ATO_ID');
  AddColuna('EMPRESA_RED_NOTA_RETIRAR_ID');
  AddColuna('EMPRESA_RED_NOTA_ENTREGAR_ID');
  AddColuna('VALOR_FRETE_PADRAO_VENDA');
  AddColuna('QUANTIDADE_MIN_DIAS_ENTREGA');
  AddColuna('OBRIGAR_VENDEDOR_SEL_TIPO_COBR');
  AddColuna('TIPO_CUSTO_VIS_LUCRO_VENDA');
  AddColuna('QTDE_DIAS_VALIDADE_ORCAMENTO');
  AddColunaSL('NOME_COND_PAGTO_CONS2');
  AddColunaSL('INDICE_COND_PAGTO_CONS2');
  AddColuna('CONDICAO_PAGAMENTO_CONS_PROD_3');
  AddColunaSL('NOME_COND_PAGTO_CONS3');
  AddColunaSL('INDICE_COND_PAGTO_CONS3');
  AddColuna('CONDICAO_PAGAMENTO_CONS_PROD_4');
  AddColunaSL('NOME_COND_PAGTO_CONS4');
  AddColunaSL('INDICE_COND_PAGTO_CONS4');
  AddColuna('TIPO_CUSTO_AJUSTE_ESTOQUE');
  AddColuna('TRABALHAR_CONFERENCIA_RET_ATO');
  AddColuna('TRABALHAR_CONFERENCIA_RETIRAR');
  AddColuna('TRABALHAR_CONFERENCIA_ENTREGA');
  AddColuna('PERM_BAIXAR_ENT_REC_PENDENTE');
  AddColuna('NUMERO_ESTABELECIMENTO_CIELO');
  AddColuna('QTDE_DIAS_BLOQ_VENDA_TIT_VENC');
  AddColuna('GERAR_NOTA_AGRP_ATO');
  AddColuna('CALCULAR_FRETE_VENDA_KM');
  AddColuna('HABILITAR_RECEBIMENTO_PIX');
  AddColuna('INF_COMPL_COMP_PAGAMENTOS');
  AddColuna('BX_CON_REC_PIX_SOM_TURN_ABERTO');
  AddColuna('ALERTA_ENTREGA_PENDENTE');
  AddColuna('SERIE_CERTIFICADO');
  AddColuna('SENHA_CERTIFICADO');
  AddColuna('IMPRESSORA_NFE');
  AddColuna('CERTIFICADO');
  AddColuna('CSC_NFCE');
  AddColuna('ID_TOKEN');
  AddColuna('MODELO_IMPRESSORA');
  AddColuna('PERMITIR_DEV_APOS_PRAZO');
end;

function TParametrosEmpresa.getRecordParametrosEmpresa: RecParametrosEmpresa;
begin
  Result := RecParametrosEmpresa.Create;
  Result.EmpresaId                  := getInt('EMPRESA_ID', True);
  Result.UtilizaNfe                 := getString('UTILIZA_NFE');
  Result.ValorMinDifTurnoContasRec  := getDouble('VALOR_MIN_DIF_TURNO_CONTAS_REC');
  Result.ExigirNomeConsFinalVenda   := getString('EXIGIR_NOME_CONS_FINAL_VENDA');
  Result.PermiteAltFormaPagtoCaixa  := getString('PERMITE_ALT_FORMA_PAGTO_CAIXA');
  Result.PermAltFormaPagtoCxFinanc  := getString('PERM_ALT_FORMA_PAGTO_CX_FINANC');
  Result.NumeroUltNfeEmitida        := getInt('NUMERO_ULT_NFE_EMITIDA');
  Result.CondicaoPagamentoPDV       := getInt('CONDICAO_PAGAMENTO_PDV');
  Result.SerieNFe                   := getInt('SERIE_NFE');
  Result.UltimoNsuConsultadoDistNFe := getString('ULTIMO_NSU_CONSULTADO_DIST_NFE');
  Result.UltimoNsuConsultadoDistCTe := getString('ULTIMO_NSU_CONSULTADO_DIST_CTE');
  Result.CorLucratividade1          := getInt('COR_LUCRATIVIDADE_1');
  Result.CorLucratividade2          := getInt('COR_LUCRATIVIDADE_2');
  Result.CorLucratividade3          := getInt('COR_LUCRATIVIDADE_3');
  Result.CorLucratividade4          := getInt('COR_LUCRATIVIDADE_4');
  Result.RegimeTributario           := getString('REGIME_TRIBUTARIO');
  Result.AliquotaSimplesNacional    := getDouble('ALIQUOTA_SIMPLES_NACIONAL');
  Result.AliquotaPIS                := getDouble('ALIQUOTA_PIS');
  Result.AliquotaCOFINS             := getDouble('ALIQUOTA_COFINS');
  Result.AliquotaCSLL               := getDouble('ALIQUOTA_CSLL');
  Result.AliquotaIRPJ               := getDouble('ALIQUOTA_IRPJ');
  Result.CondPagtoPadraoPesqVenda   := getInt('COND_PAGTO_PADRAO_PESQ_VENDA');
  Result.UtilizaNfce                := getString('UTILIZA_NFCE');
  Result.NumeroUltNfceEmitida       := getInt('NUMERO_ULT_NFCE_EMITIDA');
  Result.SerieNfce                  := getInt('SERIE_NFCE');
  Result.tipoAmbienteNFe            := getString('TIPO_AMBIENTE_NFE');
  Result.tipoAmbienteNFCe           := getString('TIPO_AMBIENTE_NFCE');
  Result.infComplDocsEletronicos    := getString('INF_COMPL_DOCS_ELETRONICOS');
  Result.TrabalhaRetirarAto         := getString('TRABALHA_RETIRAR_ATO');
  Result.TrabalhaRetirar            := getString('TRABALHA_RETIRAR');
  Result.TrabalhaEntregar           := getString('TRABALHA_ENTREGAR');
  Result.TrabalhaSemPrevisao        := getString('TRABALHA_SEM_PREVISAO');
  Result.TrabalhaSemPrevisaoEntregar:= getString('TRABALHA_SEM_PREVISAO_ENTREGAR');
  Result.ConfirmarSaidaProdutos     := getString('CONFIRMAR_SAIDA_PRODUTOS');
  Result.TrabalharControleSeparacao := getString('TRABALHAR_CONTROLE_SEPARACAO');
  Result.TipoPrecoUtilizarVenda     := getString('TIPO_PRECO_UTILIZAR_VENDA');
  Result.CondicaoPagtoPadVendaAssis := getInt('CONDICAO_PAGTO_PAD_VENDA_ASSIS');
  Result.CondicaoPagamentoConsProd2 := getInt('CONDICAO_PAGAMENTO_CONS_PROD_2');
  Result.NomeCondPagamentoCons2     := getString('NOME_COND_PAGTO_CONS2');
  Result.IndiceCondPagtoCons2       := getDouble('INDICE_COND_PAGTO_CONS2');
  Result.CondicaoPagamentoConsProd3 := getInt('CONDICAO_PAGAMENTO_CONS_PROD_3');
  Result.NomeCondPagamentoCons3     := getString('NOME_COND_PAGTO_CONS3');
  Result.IndiceCondPagtoCons3       := getDouble('INDICE_COND_PAGTO_CONS3');
  Result.CondicaoPagamentoConsProd4 := getInt('CONDICAO_PAGAMENTO_CONS_PROD_4');
  Result.NomeCondPagamentoCons4     := getString('NOME_COND_PAGTO_CONS4');
  Result.IndiceCondPagtoCons4       := getDouble('INDICE_COND_PAGTO_CONS4');
  Result.IndiceSabado               := getDouble('INDICE_SABADO');
  Result.IndiceDomingo              := getDouble('INDICE_DOMINGO');
  Result.ImprimirLoteCompEntrega    := getString('IMPRIMIR_LOTE_COMP_ENTREGA');
  Result.ImprimirCodOriginalCompEnt := getString('IMPRIMIR_COD_ORIGINAL_COMP_ENT');
  Result.ImprimirCodBarrasCompEnt   := getString('IMPRIMIR_COD_BARRAS_COMP_ENT');
  Result.TipoCustoVisLucroVenda     := getString('TIPO_CUSTO_VIS_LUCRO_VENDA');
  Result.QtdeDiasValidadeOrcamento  := getInt('QTDE_DIAS_VALIDADE_ORCAMENTO');
  Result.PercentualJurosMensal      := getDouble('PERCENTUAL_JUROS_MENSAL');
  Result.PercentualMulta            := getDouble('PERCENTUAL_MULTA');
  Result.TipoRedirecionamentoNota   := getString('TIPO_REDIRECIONAMENTO_NOTA');
  Result.EmpresaRedNotaRetirarAtoId := getInt('EMPRESA_RED_NOTA_RETIRA_ATO_ID');
  Result.EmpresaRedNotaRetirarId    := getInt('EMPRESA_RED_NOTA_RETIRAR_ID');
  Result.EmpresaRedNotaEntregarId   := getInt('EMPRESA_RED_NOTA_ENTREGAR_ID');
  Result.ValorFretePadraoVenda      := getDouble('VALOR_FRETE_PADRAO_VENDA');
  Result.QuantidadeMinDiasEntrega   := getInt('QUANTIDADE_MIN_DIAS_ENTREGA');
  Result.ObrigarVendedorSelTipoCobr := getString('OBRIGAR_VENDEDOR_SEL_TIPO_COBR');
  Result.TipoCustoAjusteEstoque     := getString('TIPO_CUSTO_AJUSTE_ESTOQUE');
  Result.TrabalharConferenciaRetAto  := getString('TRABALHAR_CONFERENCIA_RET_ATO');
  Result.TrabalharConferenciaRetirar := getString('TRABALHAR_CONFERENCIA_RETIRAR');
  Result.TrabalharConferenciaEntrega := getString('TRABALHAR_CONFERENCIA_ENTREGA');
  Result.PermBaixarEntRecPendente    := getString('PERM_BAIXAR_ENT_REC_PENDENTE');
  Result.NumeroEstabelecimentoCielo  := getString('NUMERO_ESTABELECIMENTO_CIELO');
  Result.QtdeDiasBloqVendaTitVenc    := getInt('QTDE_DIAS_BLOQ_VENDA_TIT_VENC');
  Result.ExirgirDadosCatao           := getString('EXIRGIR_DADOS_CARTAO');
  Result.GerarNfAgrupada             := getString('GERAR_NOTA_AGRP_ATO');
  Result.CalcularFreteVendaPorKM     := getString('CALCULAR_FRETE_VENDA_KM');
  Result.HabilitarRecebimentoPix     := getString('HABILITAR_RECEBIMENTO_PIX');
  Result.InfComplCompPagamentos      := getString('INF_COMPL_COMP_PAGAMENTOS');
  Result.BaixaContasReceberPixTurnoAberto := getString('BX_CON_REC_PIX_SOM_TURN_ABERTO');
  Result.AlertaEntregaPendente       := getString('ALERTA_ENTREGA_PENDENTE');
  Result.SerieCertificado            := getString('SERIE_CERTIFICADO');
  Result.SenhaCertificado            := getString('SENHA_CERTIFICADO');
  Result.ImpressoraNFe               := getString('IMPRESSORA_NFE');
  Result.CscNFce                     := getString('CSC_NFCE');
  Result.IdToken                     := getString('ID_TOKEN');
  Result.ModeloImpressora            := getString('MODELO_IMPRESSORA');
  Result.PermitirDevolucaoAposPrazo  := getString('PERMITIR_DEV_APOS_PRAZO');
end;

function AtualizarParametrosEmpresa(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pUtilizaNFe: string;
  pValorMinDifTurnoContasRec: Double;
  pExigirNomeConsFinalVenda: string;
  pExirgirDadosCatao: string;
  pPermiteAltFormaPagtoCaixa: string;
  pPermAltFormaPagtoCxFinanc: string;
  pNumeroUltNFEEmitida: Integer;
  pCondicaoPagamentoPDV: Integer;
  pSerieNfe: Integer;
  pUltimoNsuConsultadoDistNFe: string;
  pUltimoNsuConsultadoDistCTe: string;
  pCorLucratividade1: Integer;
  pCorLucratividade2: Integer;
  pCorLucratividade3: Integer;
  pCorLucratividade4: Integer;
  pRegimeTributario: string;
  pAliquotaSimplesNacional: Double;
  pAliquotaPIS: Double;
  pAliquotaCOFINS: Double;
  pAliquotaCSLL: Double;
  pAliquotaIRPJ: Double;
  pCondPagtoPadraoPesqVenda: Integer;
  pUtilizaNfce: string;
  pNumeroUltNfceEmitida: Integer;
  pSerieNfce: Integer;
  pTipoAmbienteNFe: string;
  pTipoAmbienteNFCe: string;
  pInfComplDocsEletronicos: string;
  pTrabalhaRetirarAto: string;
  pTrabalhaRetirar: string;
  pTrabalhaEntregar: string;
  pTrabalhaSemPrevisao: string;
  pTrabalhaSemPrevisaoEntregar: string;
  pConfirmarSaidaProdutos: string;
  pTrabalharControleSeparacao: string;
  pCondicaoPagtoPadVendaAssis: Integer;
  pCondicaoPagamentoConsProd2: Integer;
  pCondicaoPagamentoConsProd3: Integer;
  pCondicaoPagamentoConsProd4: Integer;
  pIndiceSabado: Double;
  pIndiceDomingo: Double;
  pTipoPrecoUtilizarVenda: string;
  pLogoEmpresa: TMemoryStream;
  pImprimirLoteCompEntrega: string;
  pImprimirCodOriginalCompEnt: string;
  pImprimirCodBarrasCompEnt: string;
  pTipoCustoVisLucroVenda: string;
  pQtdeDiasValidadeOrcamento: Integer;
  pPercentualJurosMensal: Double;
  pPercentualMulta: Double;
  pTipoRedirecionamentoNota: string;
  pEmpresaRedNotaRetirarAtoId: Integer;
  pEmpresaRedNotaRetirarId: Integer;
  pEmpresaRedNotaEntregarId: Integer;
  pValorFretePadraoVenda: Double;
  pQtdeMinDiasEntregas: Integer;
  pObrigarVendedorSelTipoCobr: string;
  pTipoCustoAjusteEstoque: string;
  pTrabalharConferenciaRetAto: string;
  pTrabalharConferenciaRetirar: string;
  pTrabalharConferenciaEntrega: string;
  pPermBaixarEntRecPendente: string;
  pNumeroEstabelecimentCielo: string;
  pGerarNfAgrupada: string;
  pQtdeDiasBloqVendaTitVenc: Integer;
  pCalcularFreteVendaKM: string;
  pHabilitarRecebimentoPix: string;
  pBaixaContasReceberPixTurnoAberto: string;
  pControlarTransacao: Boolean;
  pInfComplCompPagamentos: string;
  pAlertaEntregaPendente: string;
  pSerieCertificado: string;
  pSenhaCertificado: string;
  pImpressoraNFe: string;
  pCscNFce: string;
  pIdToken: string;
  pModeloImpressora: string;
  pCertificado: TMemoryStream;
  pPermitirDevolucaoAposPrazo: string
): RecRetornoBD;
var
  t: TParametrosEmpresa;
  vExec: TExecucao;
begin
  Result.Iniciar;
  vExec := TExecucao.Create(pConexao);
  t := TParametrosEmpresa.Create(pConexao);
  pConexao.SetRotina('ATU_PARAMETROS_EMPRESA');

  try
    if pControlarTransacao then
      pConexao.IniciarTransacao;

    t.setInt('EMPRESA_ID', pEmpresaId, True);
    t.setString('UTILIZA_NFE', pUtilizaNFe);
    t.setDouble('VALOR_MIN_DIF_TURNO_CONTAS_REC', pValorMinDifTurnoContasRec);
    t.setString('EXIGIR_NOME_CONS_FINAL_VENDA', pExigirNomeConsFinalVenda);
    t.setString('EXIRGIR_DADOS_CARTAO', pExirgirDadosCatao);
    t.setString('PERMITE_ALT_FORMA_PAGTO_CAIXA', pPermiteAltFormaPagtoCaixa);
    t.setString('PERM_ALT_FORMA_PAGTO_CX_FINANC', pPermAltFormaPagtoCxFinanc);
    t.setInt('NUMERO_ULT_NFE_EMITIDA', pNumeroUltNFEEmitida);
    t.setIntN('CONDICAO_PAGAMENTO_PDV', pCondicaoPagamentoPDV);
    t.setIntN('CONDICAO_PAGTO_PAD_VENDA_ASSIS', pCondicaoPagtoPadVendaAssis);


    if pSerieNfe > -1 then
      t.setInt('SERIE_NFE', pSerieNfe);

    t.setString('ULTIMO_NSU_CONSULTADO_DIST_NFE', pUltimoNsuConsultadoDistNFe);
    t.setString('ULTIMO_NSU_CONSULTADO_DIST_CTE', pUltimoNsuConsultadoDistCTe);

    t.setInt('COR_LUCRATIVIDADE_1', pCorLucratividade1);
    t.setInt('COR_LUCRATIVIDADE_2', pCorLucratividade2);
    t.setInt('COR_LUCRATIVIDADE_3', pCorLucratividade3);
    t.setInt('COR_LUCRATIVIDADE_4', pCorLucratividade4);
    t.setString('REGIME_TRIBUTARIO', pRegimeTributario);
    t.setDoubleN('ALIQUOTA_SIMPLES_NACIONAL', pAliquotaSimplesNacional);
    t.setDouble('ALIQUOTA_PIS', pAliquotaPIS);
    t.setDouble('ALIQUOTA_COFINS', pAliquotaCOFINS);
    t.setDouble('ALIQUOTA_CSLL', pAliquotaCSLL);
    t.setDouble('ALIQUOTA_IRPJ', pAliquotaIRPJ);
    t.setIntN('COND_PAGTO_PADRAO_PESQ_VENDA', pCondPagtoPadraoPesqVenda);
    t.setString('UTILIZA_NFCE', pUtilizaNfce);

    if pSerieNfce > -1 then
      t.setInt('SERIE_NFCE', pSerieNfce);

    t.setInt('NUMERO_ULT_NFCE_EMITIDA', pNumeroUltNfceEmitida);
    t.setString('TIPO_AMBIENTE_NFE', pTipoAmbienteNFe);
    t.setString('TIPO_AMBIENTE_NFCE', pTipoAmbienteNFCe);
    t.setString('INF_COMPL_DOCS_ELETRONICOS', pInfComplDocsEletronicos);
    t.setString('TRABALHA_RETIRAR_ATO', pTrabalhaRetirarAto);
    t.setString('TRABALHA_RETIRAR', pTrabalhaRetirar);
    t.setString('TRABALHA_ENTREGAR', pTrabalhaEntregar);
    t.setString('TRABALHA_SEM_PREVISAO', pTrabalhaSemPrevisao);
    t.setString('TRABALHA_SEM_PREVISAO_ENTREGAR', pTrabalhaSemPrevisaoEntregar);
    t.setString('CONFIRMAR_SAIDA_PRODUTOS', pConfirmarSaidaProdutos);
    t.setString('TRABALHAR_CONTROLE_SEPARACAO', pTrabalharControleSeparacao);
    t.setString('TIPO_PRECO_UTILIZAR_VENDA', pTipoPrecoUtilizarVenda);
    t.setIntN('CONDICAO_PAGAMENTO_CONS_PROD_2', pCondicaoPagamentoConsProd2);
    t.setIntN('CONDICAO_PAGAMENTO_CONS_PROD_3', pCondicaoPagamentoConsProd3);
    t.setIntN('CONDICAO_PAGAMENTO_CONS_PROD_4', pCondicaoPagamentoConsProd4);
    t.setDouble('INDICE_SABADO', pIndiceSabado);
    t.setDouble('INDICE_DOMINGO', pIndiceDomingo);
    t.setString('IMPRIMIR_LOTE_COMP_ENTREGA', pImprimirLoteCompEntrega);
    t.setString('IMPRIMIR_COD_ORIGINAL_COMP_ENT', pImprimirCodOriginalCompEnt);
    t.setString('IMPRIMIR_COD_BARRAS_COMP_ENT', pImprimirCodBarrasCompEnt);
    t.setString('TIPO_CUSTO_VIS_LUCRO_VENDA', pTipoCustoVisLucroVenda);
    t.setInt('QTDE_DIAS_VALIDADE_ORCAMENTO', pQtdeDiasValidadeOrcamento);
    t.setDouble('PERCENTUAL_JUROS_MENSAL', pPercentualJurosMensal);
    t.setDouble('PERCENTUAL_MULTA', pPercentualMulta);
    t.setString('TIPO_REDIRECIONAMENTO_NOTA', pTipoRedirecionamentoNota);
    t.setIntN('EMPRESA_RED_NOTA_RETIRA_ATO_ID', pEmpresaRedNotaRetirarAtoId);
    t.setIntN('EMPRESA_RED_NOTA_RETIRAR_ID', pEmpresaRedNotaRetirarId);
    t.setIntN('EMPRESA_RED_NOTA_ENTREGAR_ID', pEmpresaRedNotaEntregarId);
    t.setDouble('VALOR_FRETE_PADRAO_VENDA', pValorFretePadraoVenda);
    t.setDouble('QUANTIDADE_MIN_DIAS_ENTREGA', pQtdeMinDiasEntregas);
    t.setString('OBRIGAR_VENDEDOR_SEL_TIPO_COBR', pObrigarVendedorSelTipoCobr);
    t.setString('TIPO_CUSTO_AJUSTE_ESTOQUE', pTipoCustoAjusteEstoque);
    t.setString('TRABALHAR_CONFERENCIA_RET_ATO', pTrabalharConferenciaRetAto);
    t.setString('TRABALHAR_CONFERENCIA_RETIRAR', pTrabalharConferenciaRetirar);
    t.setString('TRABALHAR_CONFERENCIA_ENTREGA', pTrabalharConferenciaEntrega);
    t.setString('PERM_BAIXAR_ENT_REC_PENDENTE', pPermBaixarEntRecPendente);
    t.setString('NUMERO_ESTABELECIMENTO_CIELO', pNumeroEstabelecimentCielo);
    t.setInt('QTDE_DIAS_BLOQ_VENDA_TIT_VENC', pQtdeDiasBloqVendaTitVenc);
    t.setString('GERAR_NOTA_AGRP_ATO', pGerarNfAgrupada);
    t.setString('CALCULAR_FRETE_VENDA_KM', pCalcularFreteVendaKM);
    t.setString('HABILITAR_RECEBIMENTO_PIX', pHabilitarRecebimentoPix);
    t.setString('INF_COMPL_COMP_PAGAMENTOS', pInfComplCompPagamentos);
    t.setString('BX_CON_REC_PIX_SOM_TURN_ABERTO', pBaixaContasReceberPixTurnoAberto);
    t.setString('ALERTA_ENTREGA_PENDENTE', pAlertaEntregaPendente);
    t.setString('SERIE_CERTIFICADO', pSerieCertificado);
    t.setString('SENHA_CERTIFICADO', pSenhaCertificado);
    t.setString('IMPRESSORA_NFE', pImpressoraNFe);
    t.setString('CSC_NFCE', pCscNFce);
    t.setString('ID_TOKEN', pIdToken);
    t.setString('MODELO_IMPRESSORA', pModeloImpressora);
    t.setString('PERMITIR_DEV_APOS_PRAZO', pPermitirDevolucaoAposPrazo);
    t.Atualizar;

    vExec.Clear;
    vExec.Add('update PARAMETROS_EMPRESA set ');
    vExec.Add('  LOGO_EMPRESA = null ');
    vExec.Add('where EMPRESA_ID = :P1');
    vExec.Executar([pEmpresaId]);

    if pLogoEmpresa <> nil then begin
      vExec.Clear;
      vExec.Add('update PARAMETROS_EMPRESA set ');
      vExec.Add('  LOGO_EMPRESA = :P1 ');
      vExec.Add('where EMPRESA_ID = :P2');

      vExec.CarregarBinario('P1', pLogoEmpresa);

      vExec.ParamByName('P2').AsInteger := pEmpresaId;
      vExec.Executar;
    end;

    vExec.Clear;
    vExec.Add('update PARAMETROS_EMPRESA set ');
    vExec.Add('  CERTIFICADO = null ');
    vExec.Add('where EMPRESA_ID = :P1');
    vExec.Executar([pEmpresaId]);

    if pCertificado <> nil then begin
      vExec.Clear;
      vExec.Add('update PARAMETROS_EMPRESA set ');
      vExec.Add('  CERTIFICADO = :P1 ');
      vExec.Add('where EMPRESA_ID = :P2');

      vExec.CarregarClob('P1', pCertificado);

      vExec.ParamByName('P2').AsInteger := pEmpresaId;
      vExec.Executar;
    end;


    if pControlarTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if pControlarTransacao then
        pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  vExec.Free;
  t.Free;
end;

function BuscarParametrosMetasFuncionario(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pFuncionarioId: Integer
): TArray<RecParametrosComissao>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  PCC.FAIXA, ');
  vSql.SQL.Add('  PCC.VALOR_INICIAL, ');
  vSql.SQL.Add('  PCC.VALOR_FINAL, ');
  vSql.SQL.Add('  PCC.PERCENTUAL, ');
  vSql.SQL.Add('  PAR.TIPO_COMISSAO_LUCRA ');
  vSql.SQL.Add('from ');
  vSql.SQL.Add('  FAIXAS_ACRESC_COMIS_META_FUNC PCC ');

  vSql.SQL.Add('left join PARAMETROS_EMPRESA PAR ');
  vSql.SQL.Add('on PCC.EMPRESA_ID = PAR.EMPRESA_ID ');

  vSql.SQL.Add('where PCC.EMPRESA_ID = :P1 ');
  vSql.SQL.Add('and PCC.FUNCIONARIO_ID = :P2 ');
  vSql.SQL.Add('order by PCC.FAIXA asc ');

  if vSql.Pesquisar([pEmpresaId, pFuncionarioId]) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);

    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].Faixa        := vSql.GetInt(0);
      Result[i].ValorInicial := vSql.GetDouble(1);
      Result[i].ValorFinal   := vSql.GetDouble(2);
      Result[i].Percentual   := vSql.GetDouble(3);
      Result[i].TipoComissao := vSql.GetString(4);

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function BuscarParametrosComissao(
  pConexao: TConexao;
  pEmpresaId: Integer
): TArray<RecParametrosComissao>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  PCC.FAIXA, ');
  vSql.SQL.Add('  PCC.VALOR_INICIAL, ');
  vSql.SQL.Add('  PCC.VALOR_FINAL, ');
  vSql.SQL.Add('  PCC.PERCENTUAL, ');
  vSql.SQL.Add('  PAR.TIPO_COMISSAO_LUCRA ');
  vSql.SQL.Add('from ');
  vSql.SQL.Add('  PARAMETROS_COMISSAO PCC ');

  vSql.SQL.Add('left join PARAMETROS_EMPRESA PAR ');
  vSql.SQL.Add('on PCC.EMPRESA_ID = PAR.EMPRESA_ID ');

  vSql.SQL.Add('where PCC.EMPRESA_ID = :P1 ');
  vSql.SQL.Add('order by PCC.FAIXA asc ');

  if vSql.Pesquisar([pEmpresaId]) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);

    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].Faixa        := vSql.GetInt(0);
      Result[i].ValorInicial := vSql.GetDouble(1);
      Result[i].ValorFinal   := vSql.GetDouble(2);
      Result[i].Percentual   := vSql.GetDouble(3);
      Result[i].TipoComissao := vSql.GetString(4);

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function BuscarParametrosDiasDevolucao(
  pConexao: TConexao;
  pEmpresaId: Integer
): TArray<RecParametrosDevolucao>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  PCC.FAIXA, ');
  vSql.SQL.Add('  PCC.VALOR_INICIAL, ');
  vSql.SQL.Add('  PCC.VALOR_FINAL, ');
  vSql.SQL.Add('  PCC.PERCENTUAL, ');
  vSql.SQL.Add('  PAR.TIPO_COMISSAO_LUCRA ');
  vSql.SQL.Add('from ');
  vSql.SQL.Add('  PARAMETROS_DIAS_DEVOLUCAO PCC ');

  vSql.SQL.Add('left join PARAMETROS_EMPRESA PAR ');
  vSql.SQL.Add('on PCC.EMPRESA_ID = PAR.EMPRESA_ID ');

  vSql.SQL.Add('where PCC.EMPRESA_ID = :P1 ');
  vSql.SQL.Add('order by PCC.FAIXA asc ');

  if vSql.Pesquisar([pEmpresaId]) then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);

    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].Faixa        := vSql.GetInt(0);
      Result[i].ValorInicial := vSql.GetInt(1);
      Result[i].ValorFinal   := vSql.GetInt(2);
      Result[i].Percentual   := vSql.GetInt(3);

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function BuscarParametrosEmpresas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecParametrosEmpresa>;
var
  i: Integer;
  t: TParametrosEmpresa;

  procedure getImagens(pConexao: TConexao; var vEmpresa: RecParametrosEmpresa);
  var
    p: TConsulta;
    s: TStream;
  begin
    vEmpresa.LogoEmpresa := nil;

    p := TConsulta.Create(pConexao);
    p.SQL.Add('select LOGO_EMPRESA from PARAMETROS_EMPRESA where EMPRESA_ID = :P1');

    if p.Pesquisar([vEmpresa.EmpresaId]) then begin
      if not(p.Fields[0].Value = null) then begin
        vEmpresa.LogoEmpresa := TMemoryStream.Create;
        s := p.CreateBlobStream(p.FieldByName('LOGO_EMPRESA'), bmRead);
        vEmpresa.LogoEmpresa.LoadFromStream(s);
        s.Free;
      end;
    end;

    p.Free;
  end;

  procedure getCertificado(pConexao: TConexao; var vEmpresa: RecParametrosEmpresa);
  var
    p: TConsulta;
    s: TStream;
  begin
    vEmpresa.Certificado := nil;

    p := TConsulta.Create(pConexao);
    p.SQL.Add('select CERTIFICADO from PARAMETROS_EMPRESA where EMPRESA_ID = :P1');

    if p.Pesquisar([vEmpresa.EmpresaId]) then begin
      if not(p.Fields[0].Value = null) then begin
        vEmpresa.Certificado := TMemoryStream.Create;
        s := p.CreateBlobStream(p.FieldByName('CERTIFICADO'), bmRead);
        vEmpresa.Certificado.LoadFromStream(s);
        s.Free;
      end;
    end;

    p.Free;
  end;
begin
  Result := nil;
  t := TParametrosEmpresa.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordParametrosEmpresa;
      getImagens(pConexao, Result[i]);
      getCertificado(pConexao, Result[i]);

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function getDadosProximaNfe(pConexao: TConexao; pNotaFiscalId: Integer): RecDadosGeracaoNFe;
var
  vProc: TProcedimentoBanco;
begin
  Result.numero_nota := 0;
  pConexao.SetRotina('BUSCAR_NUMERO_NOTA_EMISSAO');
  vProc := TProcedimentoBanco.Create(pConexao, 'BUSCAR_NUMERO_NOTA_EMISSAO');

  vProc.Params[0].AsInteger := pNotaFiscalID;
  vProc.Executar;

  Result.numero_nota := vProc.Params[1].AsInteger;
  Result.data_hora_emissao := vProc.Params[2].AsDate;

  FreeAndNil(vProc);
end;

function AtualizarParametrosComissaoMetaFuncionario(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pFaixas: TArray<RecParametrosComissao>;
  pFuncionarioId: Integer
): RecRetornoBD;
var
  vExec: TExecucao;
  i: Integer;
begin
  Result.TeveErro := False;

  try
    pConexao.IniciarTransacao;

    vExec := TExecucao.Create(pConexao);

    vExec.SQL.Text := 'delete from FAIXAS_ACRESC_COMIS_META_FUNC where EMPRESA_ID = :P1 and FUNCIONARIO_ID = :P2';
    vExec.Executar([pEmpresaId, pFuncionarioId]);
    vExec.Clear;

    vExec.SQL.Add('insert into FAIXAS_ACRESC_COMIS_META_FUNC( ');
    vExec.SQL.Add('  EMPRESA_ID, ');
    vExec.SQL.Add('  FAIXA, ');
    vExec.SQL.Add('  VALOR_FINAL, ');
    vExec.SQL.Add('  VALOR_INICIAL, ');
    vExec.SQL.Add('  PERCENTUAL, ');
    vExec.SQL.Add('  FUNCIONARIO_ID ');
    vExec.SQL.Add(') values ( ');
    vExec.SQL.Add('  :P1, ');
    vExec.SQL.Add('  :P2, ');
    vExec.SQL.Add('  :P3, ');
    vExec.SQL.Add('  :P4, ');
    vExec.SQL.Add('  :P5, ');
    vExec.SQL.Add('  :P6 ');
    vExec.SQL.Add(') ');

    for I := Low(pFaixas) to High(pFaixas) do begin
      vExec.Executar([
        pEmpresaId,
        i + 1,
        pFaixas[i].ValorFinal,
        pFaixas[i].ValorInicial,
        pFaixas[i].Percentual,
        pFuncionarioId
      ]);
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;

      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  vExec.Free;
end;

function AtualizarParametrosComissao(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pFaixas: TArray<RecParametrosComissao>;
  pTipoComissao: string
): RecRetornoBD;
var
  vExec: TExecucao;
  i: Integer;
begin
  Result.TeveErro := False;

  try
    pConexao.IniciarTransacao;

    vExec := TExecucao.Create(pConexao);

    vExec.SQL.Text := 'delete from PARAMETROS_COMISSAO where EMPRESA_ID = :P1';
    vExec.Executar([pEmpresaId]);
    vExec.Clear;

    vExec.SQL.Text := 'update PARAMETROS_EMPRESA set TIPO_COMISSAO_LUCRA = :P1 where EMPRESA_ID = :P2';
    vExec.Executar([pTipoComissao, pEmpresaId]);
    vExec.Clear;

    vExec.SQL.Add('insert into PARAMETROS_COMISSAO( ');
    vExec.SQL.Add('  EMPRESA_ID, ');
    vExec.SQL.Add('  FAIXA, ');
    vExec.SQL.Add('  VALOR_FINAL, ');
    vExec.SQL.Add('  VALOR_INICIAL, ');
    vExec.SQL.Add('  PERCENTUAL ');
    vExec.SQL.Add(') values ( ');
    vExec.SQL.Add('  :P1, ');
    vExec.SQL.Add('  :P2, ');
    vExec.SQL.Add('  :P3, ');
    vExec.SQL.Add('  :P4, ');
    vExec.SQL.Add('  :P5 ');
    vExec.SQL.Add(') ');

    for I := Low(pFaixas) to High(pFaixas) do begin
      vExec.Executar([
        pEmpresaId,
        i + 1,
        pFaixas[i].ValorFinal,
        pFaixas[i].ValorInicial,
        pFaixas[i].Percentual
      ]);
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;

      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  vExec.Free;
end;

function AtualizarParametrosDevolucao(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pFaixas: TArray<RecParametrosDevolucao>
): RecRetornoBD;
var
  vExec: TExecucao;
  i: Integer;
begin
  Result.TeveErro := False;

  try
    pConexao.IniciarTransacao;

    vExec := TExecucao.Create(pConexao);

    vExec.SQL.Text := 'delete from PARAMETROS_DIAS_DEVOLUCAO where EMPRESA_ID = :P1';
    vExec.Executar([pEmpresaId]);
    vExec.Clear;

    vExec.SQL.Add('insert into PARAMETROS_DIAS_DEVOLUCAO( ');
    vExec.SQL.Add('  EMPRESA_ID, ');
    vExec.SQL.Add('  FAIXA, ');
    vExec.SQL.Add('  VALOR_FINAL, ');
    vExec.SQL.Add('  VALOR_INICIAL, ');
    vExec.SQL.Add('  PERCENTUAL ');
    vExec.SQL.Add(') values ( ');
    vExec.SQL.Add('  :P1, ');
    vExec.SQL.Add('  :P2, ');
    vExec.SQL.Add('  :P3, ');
    vExec.SQL.Add('  :P4, ');
    vExec.SQL.Add('  :P5 ');
    vExec.SQL.Add(') ');

    for I := Low(pFaixas) to High(pFaixas) do begin
      vExec.Executar([
        pEmpresaId,
        i + 1,
        pFaixas[i].ValorFinal,
        pFaixas[i].ValorInicial,
        pFaixas[i].Percentual
      ]);

      pConexao.FinalizarTransacao;
    end;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;

      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  vExec.Free;
end;

function AtualizarUltimoNSUDistribuicaoNFeCTe(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pUltimoNSU: string;
  pTipo: string;
  pEmTransacao: Boolean
): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.TeveErro := False;
  vExec := TExecucao.Create(pConexao);

  try
    if not pEmTransacao then
      pConexao.IniciarTransacao;

    vExec.SQL.Add('update PARAMETROS_EMPRESA set ');

    // NFE
    if pTipo = 'N' then
      vExec.SQL.Add('  ULTIMO_NSU_CONSULTADO_DIST_NFE = :P2 ')
    else
      vExec.SQL.Add('  ULTIMO_NSU_CONSULTADO_DIST_CTE = :P2 ');

    vExec.SQL.Add('where EMPRESA_ID = :P1 ');

    vExec.Executar([pEmpresaId, pUltimoNSU]);

    if not pEmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pEmTransacao then
        pConexao.VoltarTransacao;

      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  vExec.Free;
end;

end.
