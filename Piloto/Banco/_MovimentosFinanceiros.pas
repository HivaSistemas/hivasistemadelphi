unit _MovimentosFinanceiros;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCaixa,
  System.Variants, System.StrUtils;

{$M+}
type
  RecMovimentosFinanceiros = record
    Id: Integer;
    ContaId: string;
    NomeContaOrigem: string;
    ClienteId: Integer;
    ClienteNome: string;
    UsuarioMovimentoId: Integer;
    ValorDinheiro: Double;
    ValorCheque: Double;
    ValorCredito: Double;
    ValorPix: Double;
    ValorCartao: Double;
    ValorCobranca: Double;
    ValorAcumulado: Double;
    DataHoraMovimento: TDateTime;
    TipoMovimento: string;
    TipoMovimentoAnalitico: string;
    Funcionario: string;
    Observacao: string;
  end;

  TMovimentosFinanceiros = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordMovimentoBancosCaixas: RecMovimentosFinanceiros;
  end;

function BuscarMovimentosFinanceiros(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecMovimentosFinanceiros>;

function BuscarMovimentosFinanceirosComando(pConexao: TConexao; pComando: string): TArray<RecMovimentosFinanceiros>;
function getFiltros: TArray<RecFiltros>;

implementation

{ TMovimentosFinanceiros }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where MOV.TURNO_ID = :P1 ' +
      'order by ' +
      '  MOV.DATA_HORA_MOVIMENTO '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where MOV.MOVIMENTO_TURNO_ID = :P1 '
    );
end;

constructor TMovimentosFinanceiros.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'MOVIMENTOS_TURNOS');

  FSql :=
    'select ' +
    '  MOV.MOVIMENTO_TURNO_ID, ' +
    '  MOV.ID, ' +
    '  MOV.CONTA_ID, ' +
    '  CON.NOME as NOME_CONTA_ORIGEM, ' +
    '  MOV.USUARIO_MOVIMENTO_ID, ' +
    '  MOV.VALOR_DINHEIRO, ' +
    '  MOV.VALOR_PIX, ' +
    '  MOV.VALOR_CHEQUE, ' +
    '  MOV.VALOR_CREDITO, ' +
    '  MOV.VALOR_CARTAO, ' +
    '  MOV.VALOR_COBRANCA, ' +
    '  MOV.VALOR_ACUMULADO, ' +
    '  MOV.DATA_HORA_MOVIMENTO, ' +
    '  MOV.TIPO_MOVIMENTO, ' +
    '  nvl(FUN.APELIDO, FUN.NOME) as FUNCIONARIO, ' +
    '  MOV.OBSERVACAO, ' +
    '  MOV.CLIENTE_ID, ' +
    '  MOV.CLIENTE_NOME ' +
    'from ' +
    '  VW_MOVIMENTOS_FINANCEIROS MOV ' +

    'left join CONTAS CON ' +
    'on MOV.CONTA_ID = CON.CONTA ' +

    'inner join FUNCIONARIOS FUN ' +
    'on MOV.USUARIO_MOVIMENTO_ID = FUN.FUNCIONARIO_ID ';

  setFiltros(getFiltros);

  AddColuna('MOVIMENTO_TURNO_ID', True);
  AddColuna('CONTA_ID');
  AddColuna('TIPO_MOVIMENTO');
  AddColuna('VALOR_DINHEIRO');
  AddColuna('VALOR_PIX');
  AddColuna('VALOR_CHEQUE');
  AddColuna('VALOR_CARTAO');
  AddColuna('VALOR_COBRANCA');
  AddColuna('OBSERVACAO');

  AddColunaSL('VALOR_ACUMULADO');
  AddColunaSL('DATA_HORA_MOVIMENTO');
  AddColunaSL('ID');
  AddColunaSL('NOME_CONTA_ORIGEM');
  AddColunaSL('USUARIO_MOVIMENTO_ID');
  AddColunaSL('VALOR_CREDITO');
  AddColunaSL('FUNCIONARIO');
  AddColunaSL('CLIENTE_ID');
  AddColunaSL('CLIENTE_NOME');
end;

function TMovimentosFinanceiros.getRecordMovimentoBancosCaixas: RecMovimentosFinanceiros;
begin
  Result.Id                 := getInt('ID');
  Result.ContaId            := getString('CONTA_ID');
  Result.NomeContaOrigem    := getString('NOME_CONTA_ORIGEM');
  Result.UsuarioMovimentoId := getInt('USUARIO_MOVIMENTO_ID');
  Result.ClienteId          := getInt('CLIENTE_ID');
  Result.ClienteNome        := getString('CLIENTE_NOME');
  Result.ValorDinheiro      := getDouble('VALOR_DINHEIRO');
  Result.ValorPix           := getDouble('VALOR_PIX');
  Result.ValorCheque        := getDouble('VALOR_CHEQUE');
  Result.ValorCredito       := getDouble('VALOR_CREDITO');
  Result.ValorCartao        := getDouble('VALOR_CARTAO');
  Result.ValorCobranca      := getDouble('VALOR_COBRANCA');
  Result.ValorAcumulado     := getDouble('VALOR_ACUMULADO');
  Result.DataHoraMovimento  := getData('DATA_HORA_MOVIMENTO');
  Result.Observacao         := getString('OBSERVACAO');

  Result.TipoMovimento      := getString('TIPO_MOVIMENTO');
  Result.TipoMovimentoAnalitico :=
    _Biblioteca.Decode(
      Result.TipoMovimento,[
        'ABE', 'Abertura',
        'SUP', 'Suprimento',
        'SAN', 'Sangria',
        'FEC', 'Fechamento',
        'REV', 'Recebimento de venda',
        'ACU', 'Recebimento de acumulados',
        'BXR', 'Baixa contas receber',
        'BXP', 'Baixa contas pagar'
      ]
    );

  Result.Funcionario        := getString('FUNCIONARIO');
end;

function BuscarMovimentosFinanceiros(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecMovimentosFinanceiros>;
var
  i: Integer;
  t: TMovimentosFinanceiros;
begin
  Result := nil;
  t := TMovimentosFinanceiros.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordMovimentoBancosCaixas;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarMovimentosFinanceirosComando(pConexao: TConexao; pComando: string): TArray<RecMovimentosFinanceiros>;
var
  i: Integer;
  t: TMovimentosFinanceiros;
begin
  Result := nil;
  t := TMovimentosFinanceiros.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordMovimentoBancosCaixas;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
