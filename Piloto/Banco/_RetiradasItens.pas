unit _RetiradasItens;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsExpedicao;

{$M+}
type
  TRetiradaItem = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordRetiradaItem: RecRetiradaItem;
  end;

function BuscarRetiradaItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecRetiradaItem>;

function BuscarRetiradaItensComando(pConexao: TConexao; pComando: string): TArray<RecRetiradaItem>;

function ExcluirRetiradaItem(
  pConexao: TConexao;
  pRetiradaId: Integer;
  pProdutoId: Integer;
  pItemId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TRetiradaItem }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'and ITE.RETIRADA_ID = :P1 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'and PRO.EXIGIR_SEPARACAO = ''S'' ' +
      'and ITE.RETIRADA_ID = :P1 '
    );
end;

constructor TRetiradaItem.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'RETIRADAS_ITENS');

  FSql :=
    'select ' +
    '  ITE.RETIRADA_ID, ' +
    '  ITE.PRODUTO_ID, ' +
    '  PRO.NOME as NOME_PRODUTO, ' +
    '  MAR.NOME as NOME_MARCA, ' +
    '  ITE.ITEM_ID, ' +
    '  ITE.QUANTIDADE, ' +
    '  ITE.DEVOLVIDOS, ' +
    '  ITE.LOTE, ' +
    '  PRO.PESO * ITE.QUANTIDADE AS PESO_TOTAL, ' +
    '  PRO.MULTIPLO_VENDA, ' +
    '  PRO.UNIDADE_VENDA as UNIDADE, ' +
    '  PRO.UNIDADE_ENTREGA_ID, ' +
    '  RET.DATA_HORA_CADASTRO, ' +
    '  PRO.CODIGO_ORIGINAL_FABRICANTE, ' +
    '  PRO.CODIGO_BARRAS, ' +
    '  PRO.CONF_SOMENTE_CODIGO_BARRAS, ' +
    '  OIT.TIPO_CONTROLE_ESTOQUE ' +
    'from ' +
    '  RETIRADAS_ITENS ITE ' +

    'inner join RETIRADAS RET ' +
    'on ITE.RETIRADA_ID = RET.RETIRADA_ID ' +

    'inner join PRODUTOS PRO ' +
    'on ITE.PRODUTO_ID = PRO.PRODUTO_ID ' +

    'inner join MARCAS MAR ' +
    'on PRO.MARCA_ID = MAR.MARCA_ID ' +

    'inner join ORCAMENTOS_ITENS OIT ' +
    'on RET.ORCAMENTO_ID = OIT.ORCAMENTO_ID ' +
    'and ITE.ITEM_ID = OIT.ITEM_ID ' +

    'where case when OIT.TIPO_CONTROLE_ESTOQUE = ''A'' or OIT.ITEM_KIT_ID is null then ''S'' else ''N'' end = ''S'' ';

  setFiltros(getFiltros);

  AddColuna('RETIRADA_ID', True);
  AddColuna('PRODUTO_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('LOTE', True);
  AddColuna('QUANTIDADE');
  AddColunaSL('DEVOLVIDOS');
  AddColunaSL('MULTIPLO_VENDA');
  AddColunaSL('NOME_PRODUTO');
  AddColunaSL('NOME_MARCA');
  AddColunaSL('UNIDADE');
  AddColunaSL('UNIDADE_ENTREGA_ID');
  AddColunaSL('DATA_HORA_CADASTRO');
  AddColunaSL('CODIGO_ORIGINAL_FABRICANTE');
  AddColunaSL('CODIGO_BARRAS');
  AddColunaSL('CONF_SOMENTE_CODIGO_BARRAS');
  AddColunaSL('TIPO_CONTROLE_ESTOQUE');
  AddColunaSL('PESO_TOTAL');
end;

function TRetiradaItem.getRecordRetiradaItem: RecRetiradaItem;
begin
  Result.RetiradaId               := getInt('RETIRADA_ID', True);
  Result.ProdutoId                := getInt('PRODUTO_ID', True);
  Result.ItemId                   := getInt('ITEM_ID', True);
  Result.Lote                     := getString('LOTE', True);
  Result.Quantidade               := getDouble('QUANTIDADE');
  Result.NomeProduto              := getString('NOME_PRODUTO');
  Result.NomeMarca                := getString('NOME_MARCA');
  Result.Unidade                  := getString('UNIDADE');
  Result.UnidadeEntregaId         := getString('UNIDADE_ENTREGA_ID');
  Result.MultiploVenda            := getDouble('MULTIPLO_VENDA');
  Result.Devolvidos               := getDouble('DEVOLVIDOS');
  Result.DataHoraCadastro         := getData('DATA_HORA_CADASTRO');
  Result.CodigoOriginalFabricante := getString('CODIGO_ORIGINAL_FABRICANTE');
  Result.CodigoBarras             := getString('CODIGO_BARRAS');
  Result.ConfSomenteCodigoBarras  := getString('CONF_SOMENTE_CODIGO_BARRAS');
  Result.TipoControleEstoque      := getString('TIPO_CONTROLE_ESTOQUE');
  Result.PesoTotal                := getDouble('PESO_TOTAL');
end;

function BuscarRetiradaItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecRetiradaItem>;
var
  i: Integer;
  t: TRetiradaItem;
begin
  Result := nil;
  t := TRetiradaItem.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordRetiradaItem;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarRetiradaItensComando(pConexao: TConexao; pComando: string): TArray<RecRetiradaItem>;
var
  i: Integer;
  t: TRetiradaItem;
begin
  Result := nil;
  t := TRetiradaItem.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordRetiradaItem;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirRetiradaItem(
  pConexao: TConexao;
  pRetiradaId: Integer;
  pProdutoId: Integer;
  pItemId: Integer
): RecRetornoBD;
var
  t: TRetiradaItem;
begin
  Result.TeveErro := False;
  t := TRetiradaItem.Create(pConexao);

  try
    t.setInt('RETIRADA_ID', pRetiradaId, True);
    t.setInt('PRODUTO_ID', pProdutoId, True);
    t.setInt('ITEM_ID', pItemId, True);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
