unit _ComprasItens;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, _RecordsOrcamentosVendas;

{$M+}
type
  RecComprasItens = record
    CompraId: Integer;
    ItemId: Integer;
    ProdutoId: Integer;
    NomeProduto: string;
    MarcaId: Integer;
    NomeMarca: string;
    CodigoOriginal: string;
    MultiploCompra: Double;
    QuantidadeEmbalagem: Double;
    UnidadeCompraId: string;
    Quantidade: Double;
    PrecoUnitario: Currency;
    ValorTotal: Currency;
    ValorDesconto: Double;
    ValorOutrasDespesas: Double;
    ValorLiquido: Double;
    Entregues: Double;
    Baixados: Double;
    Cancelados: Double;
    Saldo: Double;

    TipoControleEstoque: string;
    ExigirDataFabricacaoLote: string;
    ExigirDataVencimentoLote: string;

    QuantidadeCancelar: Double; // Utilizado para cancelamento de itens dentro de uma compra
  end;

  TComprasItens = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordComprasItens: RecComprasItens;
  end;

function BuscarComprasItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecComprasItens>;

function CancelarItensCompra(pConexao: TConexao; pCompraId: Integer; pItens: TArray<RecComprasItens>): RecRetornoBD;

function BuscarComprasItensComando(pConexao: TConexao; pComando: string): TArray<RecComprasItens>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TComprasItens }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ITE.COMPRA_ID = :P1 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ITE.COMPRA_ID = :P1 ' +
      'and ITE.SALDO > 0 ' +
      'order by ' +
      '  PRO.NOME '
    );
end;

constructor TComprasItens.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'COMPRAS_ITENS');

  FSql :=
    'select ' +
    '  ITE.COMPRA_ID, ' +
    '  ITE.ITEM_ID, ' +
    '  ITE.PRODUTO_ID, ' +
    '  PRO.NOME as NOME_PRODUTO, ' +
    '  PRO.MARCA_ID, ' +
    '  MAR.NOME as NOME_MARCA, ' +
    '  ITE.MULTIPLO_COMPRA, ' +
    '  ITE.QUANTIDADE_EMBALAGEM, ' +
    '  ITE.UNIDADE_COMPRA_ID, ' +
    '  ITE.QUANTIDADE, ' +
    '  ITE.PRECO_UNITARIO, ' +
    '  ITE.VALOR_TOTAL, ' +
    '  ITE.VALOR_DESCONTO, ' +
    '  ITE.VALOR_OUTRAS_DESPESAS, ' +
    '  ITE.VALOR_LIQUIDO, ' +
    '  ITE.ENTREGUES, ' +
    '  ITE.BAIXADOS, ' +
    '  ITE.CANCELADOS, ' +
    '  ITE.SALDO, ' +
    '  PRO.TIPO_CONTROLE_ESTOQUE, ' +
    '  PRO.EXIGIR_DATA_FABRICACAO_LOTE, ' +
    '  PRO.EXIGIR_DATA_VENCIMENTO_LOTE, ' +
    '  PRO.CODIGO_ORIGINAL_FABRICANTE ' +
    'from ' +
    '  COMPRAS_ITENS ITE ' +

    'inner join PRODUTOS PRO ' +
    'on ITE.PRODUTO_ID = PRO.PRODUTO_ID ' +

    'inner join MARCAS MAR ' +
    'on PRO.MARCA_ID = MAR.MARCA_ID ';

  setFiltros(getFiltros);

  AddColuna('COMPRA_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('PRODUTO_ID');
  AddColunaSL('NOME_PRODUTO');
  AddColunaSL('MARCA_ID');
  AddColunaSL('NOME_MARCA');
  AddColuna('MULTIPLO_COMPRA');
  AddColuna('QUANTIDADE_EMBALAGEM');
  AddColuna('UNIDADE_COMPRA_ID');
  AddColuna('QUANTIDADE');
  AddColuna('PRECO_UNITARIO');
  AddColuna('VALOR_TOTAL');
  AddColuna('VALOR_DESCONTO');
  AddColuna('VALOR_OUTRAS_DESPESAS');
  AddColuna('VALOR_LIQUIDO');
  AddColunaSL('ENTREGUES');
  AddColunaSL('BAIXADOS');
  AddColunaSL('CANCELADOS');
  AddColunaSL('SALDO');
  AddColunaSL('TIPO_CONTROLE_ESTOQUE');
  AddColunaSL('EXIGIR_DATA_FABRICACAO_LOTE');
  AddColunaSL('EXIGIR_DATA_VENCIMENTO_LOTE');
  AddColunaSL('CODIGO_ORIGINAL_FABRICANTE');
end;

function TComprasItens.getRecordComprasItens: RecComprasItens;
begin
  Result.CompraId            := getInt('COMPRA_ID', True);
  Result.ItemId              := getInt('ITEM_ID', True);
  Result.ProdutoId           := getInt('PRODUTO_ID');
  Result.NomeProduto         := getString('NOME_PRODUTO');
  Result.MarcaId             := getInt('MARCA_ID');
  Result.NomeMarca           := getString('NOME_MARCA');
  Result.MultiploCompra      := getDouble('MULTIPLO_COMPRA');
  Result.QuantidadeEmbalagem := getDouble('QUANTIDADE_EMBALAGEM');
  Result.UnidadeCompraId     := getString('UNIDADE_COMPRA_ID');
  Result.Quantidade          := getDouble('QUANTIDADE');
  Result.PrecoUnitario       := getDouble('PRECO_UNITARIO');
  Result.ValorTotal          := getDouble('VALOR_TOTAL');
  Result.ValorDesconto       := getDouble('VALOR_DESCONTO');
  Result.ValorOutrasDespesas := getDouble('VALOR_OUTRAS_DESPESAS');
  Result.ValorLiquido        := getDouble('VALOR_LIQUIDO');
  Result.Entregues           := getDouble('ENTREGUES');
  Result.Baixados            := getDouble('BAIXADOS');
  Result.Cancelados          := getDouble('CANCELADOS');
  Result.Saldo               := getDouble('SALDO');
  Result.TipoControleEstoque      := getString('TIPO_CONTROLE_ESTOQUE');
  Result.ExigirDataFabricacaoLote := getString('EXIGIR_DATA_FABRICACAO_LOTE');
  Result.ExigirDataVencimentoLote := getString('EXIGIR_DATA_VENCIMENTO_LOTE');
  Result.CodigoOriginal      := getString('CODIGO_ORIGINAL_FABRICANTE');
end;

function BuscarComprasItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecComprasItens>;
var
  i: Integer;
  t: TComprasItens;
begin
  Result := nil;
  t := TComprasItens.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordComprasItens;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarComprasItensComando(pConexao: TConexao; pComando: string): TArray<RecComprasItens>;
var
  i: Integer;
  t: TComprasItens;
begin
  Result := nil;
  t := TComprasItens.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordComprasItens;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function CancelarItensCompra(pConexao: TConexao; pCompraId: Integer; pItens: TArray<RecComprasItens>): RecRetornoBD;
var
  i: Integer;
  vExec: TExecucao;
  vProc: TProcedimentoBanco;
begin
  Result.TeveErro := False;
  pConexao.SetRotina('CANCELAR_ITENS_COMPRA');

  vExec := TExecucao.Create(pConexao);
  vProc := TProcedimentoBanco.Create(pConexao, 'VER_BAIXAR_CANCELAR_COMPRA');

  vExec.Add('update COMPRAS_ITENS set');
  vExec.Add('  CANCELADOS = CANCELADOS + :P3 ');
  vExec.Add('where COMPRA_ID = :P1 ');
  vExec.Add('and ITEM_ID = :P2 ');

  try
    pConexao.IniciarTransacao;

    for i := Low(pItens) to High(pItens) do begin
      vExec.Executar([
        pCompraId,
        pItens[i].ItemId,
        pItens[i].QuantidadeCancelar
      ]);
    end;

    vProc.Executar([pCompraId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
  vProc.Free;
end;

end.
