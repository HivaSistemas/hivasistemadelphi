unit _Mensagens;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TMensagem = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordMensagem: RecMensagem;
  end;

function AtualizarMensagem(
  pConexao: TConexao;
  pMensagemId: Integer;
  pAssunto: string;
  pMensagem: string;
  pUsuariosDestinoMensagemIds: TArray<Integer>;
  pRespondida: string;
  pPermitirResposta: string;
  pLida: string;
  pGrupoMensagem: string;
  pControlarTransacao: Boolean
): RecRetornoBD;

function BuscarMensagens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecMensagem>;

function ExcluirMensagem(
  pConexao: TConexao;
  pMensagemId: Integer
): RecRetornoBD;

function BuscarQuantidadeMensagensNaoLidas(pConexao: TConexao; pFuncionarioId: Integer): Integer;
function ConfirmarLeitura(pConexao: TConexao; pMensagemId: Integer): RecRetornoBD;
function SetarMensagemRespondida(pConexao: TConexao; pMensagemId: Integer; pControlarTransacao: Boolean): RecRetornoBD;
function ExisteMensagensNaoLidas(pConexao: TConexao; pFuncionarioId: Integer): Boolean;

function getFiltros: TArray<RecFiltros>;

implementation

{ TMensagem }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 3);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where MENSAGEM_ID = :P1 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where USUARIO_DESTINO_MENSAGEM_ID = :P1 ' +
      'and GRUPO_MENSAGEM = :P2 ' +
      'order by ' +
      '  MEN.MENSAGEM_ID desc '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where USUARIO_ENVIO_MENSAGEM_ID = :P1 ' +
      'and GRUPO_MENSAGEM = :P2 ' +
      'order by ' +
      '  MEN.MENSAGEM_ID desc '
    );
end;

constructor TMensagem.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'MENSAGENS');

  FSql :=
    'select ' +
    '  MEN.MENSAGEM_ID, ' +
    '  MEN.ASSUNTO, ' +
    '  MEN.AGRUPADOR_MENSAGEM_ID, ' +
    '  MEN.MENSAGEM, ' +
    '  MEN.USUARIO_ENVIO_MENSAGEM_ID, ' +
    '  FEN.NOME as NOME_USUARIO_ENVIO, ' +
    '  MEN.USUARIO_DESTINO_MENSAGEM_ID, ' +
    '  FDE.NOME as NOME_USUARIO_DESTINO, ' +
    '  MEN.RESPONDIDA, ' +
    '  MEN.PERMITIR_RESPOSTA, ' +
    '  MEN.GRUPO_MENSAGEM, ' +
    '  MEN.DATA_HORA_ENVIO, ' +
    '  MEN.LIDA ' +
    'from ' +
    '  MENSAGENS MEN ' +

    'inner join FUNCIONARIOS FEN ' +
    'on MEN.USUARIO_ENVIO_MENSAGEM_ID = FEN.FUNCIONARIO_ID ' +

    'inner join FUNCIONARIOS FDE ' +
    'on MEN.USUARIO_DESTINO_MENSAGEM_ID = FDE.FUNCIONARIO_ID ';

  setFiltros(getFiltros);

  AddColuna('MENSAGEM_ID', True);
  AddColuna('USUARIO_DESTINO_MENSAGEM_ID');
  AddColunaSL('NOME_USUARIO_DESTINO');
  AddColuna('ASSUNTO');
  AddColuna('AGRUPADOR_MENSAGEM_ID');
  AddColuna('MENSAGEM');
  AddColunaSL('USUARIO_ENVIO_MENSAGEM_ID');
  AddColunaSL('NOME_USUARIO_ENVIO');
  AddColuna('RESPONDIDA');
  AddColuna('PERMITIR_RESPOSTA');
  AddColuna('GRUPO_MENSAGEM');
  AddColunaSL('DATA_HORA_ENVIO');
  AddColunaSL('LIDA');
end;

function TMensagem.getRecordMensagem: RecMensagem;
begin
  Result.MensagemId               := getInt('MENSAGEM_ID', True);
  Result.AgrupadorMensagemId      := getInt('AGRUPADOR_MENSAGEM_ID');
  Result.Assunto                  := getString('ASSUNTO');
  Result.Mensagem                 := getString('MENSAGEM');
  Result.UsuarioEnvioMensagemId   := getInt('USUARIO_ENVIO_MENSAGEM_ID');
  Result.NomeUsuarioEnvio         := getString('NOME_USUARIO_ENVIO');
  Result.UsuarioDestinoMensagemId := getInt('USUARIO_DESTINO_MENSAGEM_ID');
  Result.NomeUsuarioDestino       := getString('NOME_USUARIO_DESTINO');
  Result.Respondida               := getString('RESPONDIDA');
  Result.PermitirResposta         := getString('PERMITIR_RESPOSTA');
  Result.GrupoMensagem            := getString('GRUPO_MENSAGEM');
  Result.DataHoraEnvio            := getData('DATA_HORA_ENVIO');
  Result.Lida                     := getString('LIDA');
end;

function AtualizarMensagem(
  pConexao: TConexao;
  pMensagemId: Integer;
  pAssunto: string;
  pMensagem: string;
  pUsuariosDestinoMensagemIds: TArray<Integer>;
  pRespondida: string;
  pPermitirResposta: string;
  pLida: string;
  pGrupoMensagem: string;
  pControlarTransacao: Boolean
): RecRetornoBD;
var
  i: Integer;
  t: TMensagem;
  vNovo: Boolean;
  vSeq: TSequencia;
  vSeqAgrupador: TSequencia;
  vAgrupadorMensagemId: Integer;
begin
  Result.Iniciar;

  t := TMensagem.Create(pConexao);
  vSeq := TSequencia.Create(pConexao, 'SEQ_MENSAGENS_ID');
  vSeqAgrupador := TSequencia.Create(pConexao, 'SEQ_AGRUPADOR_MENSAGENS');
  vNovo := pMensagemId = 0;

  try
    if pControlarTransacao then
      pConexao.IniciarTransacao;

    vAgrupadorMensagemId := vSeqAgrupador.GetProximaSequencia;

    t.setInt('AGRUPADOR_MENSAGEM_ID', vAgrupadorMensagemId);
    t.setString('ASSUNTO', pAssunto);
    t.setString('MENSAGEM', pMensagem);
    t.setString('RESPONDIDA', pRespondida);
    t.setString('PERMITIR_RESPOSTA', pPermitirResposta);
    t.setString('GRUPO_MENSAGEM', pGrupoMensagem);
    t.setString('LIDA', pLida);

    for i := Low(pUsuariosDestinoMensagemIds) to High(pUsuariosDestinoMensagemIds) do begin

      t.setInt('MENSAGEM_ID', vSeq.getProximaSequencia, True);
      t.setInt('USUARIO_DESTINO_MENSAGEM_ID', pUsuariosDestinoMensagemIds[i]);

      if vNovo then
        t.Inserir
      else
        t.Atualizar;
    end;

    if pControlarTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if pControlarTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro( e );
    end;
  end;

  vSeqAgrupador.Free;
  vSeq.Free;
  t.Free;
end;

function BuscarMensagens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecMensagem>;
var
  i: Integer;
  t: TMensagem;
begin
  Result := nil;
  t := TMensagem.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordMensagem;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirMensagem(
  pConexao: TConexao;
  pMensagemId: Integer
): RecRetornoBD;
var
  t: TMensagem;
begin
  Result.TeveErro := False;
  t := TMensagem.Create(pConexao);

  try
    t.setInt('MENSAGEM_ID', pMensagemId, True);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function BuscarQuantidadeMensagensNaoLidas(pConexao: TConexao; pFuncionarioId: Integer): Integer;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  count(*) ');
  vSql.Add('from ');
  vSql.Add('  MENSAGENS ');
  vSql.Add('where USUARIO_DESTINO_MENSAGEM_ID = :P1 ');
  vSql.Add('and LIDA = ''N'' ');
  vSql.Pesquisar([pFuncionarioId]);

  Result := vSql.GetInt(0);

  vSql.Active := False;
  vSql.Free;
end;

function ConfirmarLeitura(pConexao: TConexao; pMensagemId: Integer): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.TeveErro := False;
  vExec := TExecucao.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    vExec.SQL.Add('update MENSAGENS set ');
    vExec.SQL.Add('  LIDA = ''S'' ');
    vExec.SQL.Add('where MENSAGEM_ID = :P1');
    vExec.Executar([pMensagemId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  vExec.Free;
end;

function SetarMensagemRespondida(pConexao: TConexao; pMensagemId: Integer; pControlarTransacao: Boolean): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.TeveErro := False;
  vExec := TExecucao.Create(pConexao);

  try
    if pControlarTransacao then
      pConexao.IniciarTransacao;

    vExec.SQL.Add('update MENSAGENS set ');
    vExec.SQL.Add('  RESPONDIDA = ''S'' ');
    vExec.SQL.Add('where MENSAGEM_ID = :P1');
    vExec.Executar([pMensagemId]);

    if pControlarTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if pControlarTransacao then
        pConexao.VoltarTransacao;

      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  vExec.Free;
end;

function ExisteMensagensNaoLidas(pConexao: TConexao; pFuncionarioId: Integer): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  count(*) ');
  vSql.Add('from ');
  vSql.Add('  MENSAGENS MEN ');

  vSql.Add('cross join PARAMETROS PAR ');

  vSql.Add('where MEN.USUARIO_DESTINO_MENSAGEM_ID = :P1 ');
  vSql.Add('and MEN.LIDA = ''N'' ');
  vSql.Add('and PAR.QTDE_MENSAGENS_OBRIGAR_LEITURA > 0 ');

  vSql.Add('group by ');
  vSql.Add('  PAR.QTDE_MENSAGENS_OBRIGAR_LEITURA ');

  vSql.Add('having count(*) >= PAR.QTDE_MENSAGENS_OBRIGAR_LEITURA ');

  vSql.Pesquisar([pFuncionarioId]);

  Result := vSql.GetInt(0) > 0;

  vSql.Active := False;
  vSql.Free;
end;

end.
