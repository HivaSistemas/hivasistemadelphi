unit _ProdutosOrdensLocEntregas;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecProdutosOrdensLocEntregas = record
    EmpresaId: Integer;
    ProdutoId: Integer;
    LocalId: Integer;
    NomeLocal: string;
    Tipo: string;
    Ordem: Integer;
  end;

  TProdutosOrdensLocEntregas = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordOrdem: RecProdutosOrdensLocEntregas;
  end;

function BuscarOrdensLocais(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecProdutosOrdensLocEntregas>;

function AtualizarProdutosOrdensLocais(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pProdutoId: Integer;
  pTipo: string;
  pLocais: TArray<Integer>;
  pControlarTansacao: Boolean
): RecRetornoBD; overload;

function AtualizarProdutosOrdensLocais(
  pConexao: TConexao;
  pEmpresasIds: TArray<Integer>;
  pProdutosIds: TArray<Integer>;
  pTipo: string;
  pLocaisIds: TArray<Integer>;
  pControlarTansacao: Boolean
): RecRetornoBD; overload;

function getFiltros: TArray<RecFiltros>;

implementation

{ TProdutosOrdensLocEntregas }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ORD.EMPRESA_ID = :P1 ' +
      'and ORD.TIPO = :P2 ' +
      'and ORD.PRODUTO_ID = :P3 ' +
      'order by ' +
      '  ORD.ORDEM '
    );
end;

constructor TProdutosOrdensLocEntregas.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PRODUTOS_ORDENS_LOC_ENTREGAS');

  FSql :=
    'select ' +
    '  ORD.EMPRESA_ID, ' +
    '  ORD.PRODUTO_ID, ' +
    '  ORD.TIPO, ' +
    '  ORD.LOCAL_ID, ' +
    '  LOC.NOME as NOME_LOCAL, ' +
    '  ORD.ORDEM ' +
    'from ' +
    '  PRODUTOS_ORDENS_LOC_ENTREGAS ORD ' +

    'inner join LOCAIS_PRODUTOS LOC ' +
    'on ORD.LOCAL_ID = LOC.LOCAL_ID ';

  setFiltros(getFiltros);

  AddColuna('EMPRESA_ID', True);
  AddColuna('PRODUTO_ID', True);
  AddColuna('LOCAL_ID', True);
  AddColuna('ORDEM', True);
  AddColuna('TIPO', True);

  AddColunaSL('NOME_LOCAL');
end;

function TProdutosOrdensLocEntregas.getRecordOrdem: RecProdutosOrdensLocEntregas;
begin
  Result.LocalId    := getInt('LOCAL_ID', True);
  Result.EmpresaId  := getInt('EMPRESA_ID', True);
  Result.ProdutoId  := getInt('PRODUTO_ID', True);
  Result.Tipo       := getString('TIPO', True);
  Result.Ordem      := getInt('ORDEM', True);
  Result.NomeLocal  := getString('NOME_LOCAL');
end;

function BuscarOrdensLocais(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecProdutosOrdensLocEntregas>;
var
  i: Integer;
  t: TProdutosOrdensLocEntregas;
begin
  Result := nil;
  t := TProdutosOrdensLocEntregas.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordOrdem;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function AtualizarProdutosOrdensLocais(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pProdutoId: Integer;
  pTipo: string;
  pLocais: TArray<Integer>;
  pControlarTansacao: Boolean
): RecRetornoBD;
var
  t: TProdutosOrdensLocEntregas;
  vExec: TExecucao;
  i: Integer;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_PROD_ORDEM_LOCAIS');
  t := TProdutosOrdensLocEntregas.Create(pConexao);
  vExec := TExecucao.Create(pConexao);

  vExec.SQL.Add('delete from PRODUTOS_ORDENS_LOC_ENTREGAS');
  vExec.SQL.Add('where EMPRESA_ID = :P1 ');
  vExec.SQL.Add('and PRODUTO_ID = :P2 ');
  vExec.SQL.Add('and TIPO = :P3 ');

  try
    if pControlarTansacao then
      pConexao.IniciarTransacao;

    vExec.Executar([pEmpresaId, pProdutoId, pTipo]);

    for i := Low(pLocais) to High(pLocais) do begin
      t.setInt('EMPRESA_ID', pEmpresaId, True);
      t.setInt('PRODUTO_ID', pProdutoId, True);
      t.setInt('LOCAL_ID', pLocais[i], True);
      t.setInt('ORDEM', i + 1, True);
      t.setString('TIPO', pTipo, True);

      t.Inserir
    end;

    if pControlarTansacao then
      pConexao.FinalizarTransacao;
  except
    on e: exception do begin
      if pControlarTansacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;
  t.Free;
end;

function AtualizarProdutosOrdensLocais(
  pConexao: TConexao;
  pEmpresasIds: TArray<Integer>;
  pProdutosIds: TArray<Integer>;
  pTipo: string;
  pLocaisIds: TArray<Integer>;
  pControlarTansacao: Boolean
): RecRetornoBD;
var
  i: Integer;
  j: Integer;
  k: Integer;

  vExec: TExecucao;
  t: TProdutosOrdensLocEntregas;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_PROD_ORDEM_LOCAIS');
  t := TProdutosOrdensLocEntregas.Create(pConexao);
  vExec := TExecucao.Create(pConexao);

  try
    if pControlarTansacao then
      pConexao.IniciarTransacao;

    for i := Low(pEmpresasIds) to High(pEmpresasIds) do begin

      vExec.Limpar;
      vExec.Add('delete from PRODUTOS_ORDENS_LOC_ENTREGAS');
      vExec.Add('where EMPRESA_ID = :P1 ');
      vExec.Add('and TIPO = :P2 ');
      vExec.Add('and ' + FiltroInInt('PRODUTO_ID', pProdutosIds) );

      vExec.Executar([pEmpresasIds[i], pTipo]);

      for j := Low(pProdutosIds) to High(pProdutosIds) do begin
        for k := Low(pLocaisIds) to High(pLocaisIds) do begin
          t.setInt('EMPRESA_ID', pEmpresasIds[i], True);
          t.setInt('PRODUTO_ID', pProdutosIds[j], True);
          t.setInt('LOCAL_ID', pLocaisIds[k], True);
          t.setInt('ORDEM', k, True);
          t.setString('TIPO', pTipo, True);

          t.Inserir
        end;
      end;
    end;

    if pControlarTansacao then
      pConexao.FinalizarTransacao;
  except
    on e: exception do begin
      if pControlarTansacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;
  t.Free;
end;

end.
