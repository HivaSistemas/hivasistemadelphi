unit _EntradasNotasFiscaisItens;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsEstoques;

{$M+}
type
  TEntradaNotaFiscalItem = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordEntradaNotaFiscalItem: RecEntradaNotaFiscalItem;
  end;

function AtualizarEntradaNotaFiscalItem(
  pConexao: TConexao;
  pEntradaId: Integer;
  pProdutoId: Integer;
  pItemId: Integer;
  pCfopId: Integer;
  pOrigemProduto: Integer;
  pCodigoNcm: Integer;
  pValorTotalOutrasDespesas: Double;
  pValorTotalDesconto: Double;
  pQuantidade: Double;
  pPrecoUnitario: Double;
  pValorTotal: Double;
  pValorIcms: Double;
  pPercentualIpi: Double;
  pValorIpi: Double;
  pValorCofins: Double;
  pPercentualCofins: Double;
  pBaseCalculoIcms: Double;
  pPercentualIcms: Double;
  pBaseCalculoIcmsSt: Double;
  pPercentualIcmsSt: Double;
  pValorIcmsSt: Double;
  pBaseCalculoPis: Double;
  pPercentualPis: Double;
  pValorPis: Double;
  pBaseCalculoCofins: Double;
  pIndiceReducaoBaseIcmsSt: Double;
  pIndiceReducaoBaseIcms: Double;
  pInformacoesAdicionais: string;
  pCstPis: string;
  pCstCofins: string;
  pCst: string;
  pCodigoBarras: string;
  pIva: Double;
  pPrecoPauta: Double
): RecRetornoBD;

function BuscarEntradaNotaFiscalItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEntradaNotaFiscalItem>;

function BuscarEntradaNotaFiscalItensComando(pConexao: TConexao; pComando: string): TArray<RecEntradaNotaFiscalItem>;

function ExcluirEntradaNotaFiscalItem(
  pConexao: TConexao;
  pEntradaId: Integer;
  pProdutoId: Integer;
  pItemId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TEntradaNotaFiscalItem }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ENTRADA_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ENTRADA_ID = :P1 ' +
      'and QUANTIDADE - nvl(QUANTIDADE_DEVOLVIDOS, 0) > 0'
    );
end;

constructor TEntradaNotaFiscalItem.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ENTRADAS_NOTAS_FISCAIS_ITENS');

  FSql :=
    'select ' +
    '  ITE.ENTRADA_ID, ' +
    '  ITE.PRODUTO_ID, ' +
    '  PRO.NOME_COMPRA, ' +
    '  MAR.NOME as NOME_MARCA, ' +
    '  ITE.ITEM_ID, ' +
    '  ITE.CFOP_ID, ' +
    '  ITE.ORIGEM_PRODUTO, ' +
    '  ITE.CODIGO_NCM, ' +
    '  ITE.VALOR_TOTAL_OUTRAS_DESPESAS, ' +
    '  ITE.VALOR_TOTAL_DESCONTO, ' +
    '  ITE.QUANTIDADE, ' +
    '  ITE.QUANTIDADE_DEVOLVIDOS, ' +
    '  ITE.UNIDADE_ENTRADA_ID, ' +
    '  ITE.QUANTIDADE_ENTRADA_ALTIS, ' +
    '  ITE.PRECO_UNITARIO, ' +
    '  ITE.VALOR_TOTAL, ' +
    '  ITE.VALOR_ICMS, ' +
    '  ITE.PERCENTUAL_IPI, ' +
    '  ITE.VALOR_IPI, ' +
    '  ITE.VALOR_COFINS, ' +
    '  ITE.PERCENTUAL_COFINS, ' +
    '  ITE.BASE_CALCULO_ICMS, ' +
    '  ITE.PERCENTUAL_ICMS, ' +
    '  ITE.BASE_CALCULO_ICMS_ST, ' +
    '  ITE.PERCENTUAL_ICMS_ST, ' +
    '  ITE.VALOR_ICMS_ST, ' +
    '  ITE.BASE_CALCULO_PIS, ' +
    '  ITE.PERCENTUAL_PIS, ' +
    '  ITE.VALOR_PIS, ' +
    '  ITE.BASE_CALCULO_COFINS, ' +
    '  ITE.INDICE_REDUCAO_BASE_ICMS_ST, ' +
    '  ITE.INDICE_REDUCAO_BASE_ICMS, ' +
    '  ITE.INFORMACOES_ADICIONAIS, ' +
    '  ITE.CST_PIS, ' +
    '  ITE.CST_COFINS, ' +
    '  ITE.CST, ' +
    '  ITE.CODIGO_BARRAS, ' +
    '  ITE.IVA, ' +
    '  ITE.PRECO_PAUTA, ' +
    '  ITE.LOCAL_ENTRADA_ID, ' +
    '  LOC.NOME as NOME_LOCAL, ' +
    '  ITE.PESO_UNITARIO, ' +
    '  ITE.VALOR_FRETE_CUSTO, ' +
    '  ITE.VALOR_IPI_CUSTO, ' +
    '  ITE.VALOR_ICMS_ST_CUSTO, ' +
    '  ITE.VALOR_OUTROS_CUSTO, ' +
    '  ITE.PRECO_FINAL, ' +
    '  ITE.VALOR_DIFAL_CUSTO, ' +
    '  ITE.VALOR_ICMS_CUSTO, ' +
    '  ITE.VALOR_ICMS_FRETE_CUSTO, ' +
    '  ITE.VALOR_PIS_CUSTO, ' +
    '  ITE.VALOR_COFINS_CUSTO, ' +
    '  ITE.VALOR_PIS_FRETE_CUSTO, ' +
    '  ITE.VALOR_COFINS_FRETE_CUSTO, ' +
    '  ITE.PRECO_LIQUIDO, ' +
    '  ITE.QUANTIDADE_EMBALAGEM, ' +
    '  ITE.MULTIPLO_COMPRA, ' +
    '  ITE.UNIDADE_COMPRA_ID, ' +
    '  PRO.TIPO_CONTROLE_ESTOQUE, ' +
    '  PRO.EXIGIR_DATA_FABRICACAO_LOTE, ' +
    '  PRO.EXIGIR_DATA_VENCIMENTO_LOTE, ' +
    '  PRO.MULTIPLO_VENDA ' +
    'from ' +
    '  ENTRADAS_NOTAS_FISCAIS_ITENS ITE ' +

    'inner join PRODUTOS PRO ' +
    'on ITE.PRODUTO_ID = PRO.PRODUTO_ID ' +

    'inner join MARCAS MAR ' +
    'on PRO.MARCA_ID = MAR.MARCA_ID ' +

    'inner join LOCAIS_PRODUTOS LOC ' +
    'on ITE.LOCAL_ENTRADA_ID = LOC.LOCAL_ID ';

  setFiltros(getFiltros);

  AddColuna('ENTRADA_ID', True);
  AddColuna('PRODUTO_ID', True);
  AddColunaSL('NOME_COMPRA');
  AddColunaSL('NOME_MARCA');
  AddColuna('ITEM_ID', True);
  AddColuna('CFOP_ID');
  AddColuna('ORIGEM_PRODUTO');
  AddColuna('CODIGO_NCM');
  AddColuna('VALOR_TOTAL_OUTRAS_DESPESAS');
  AddColuna('VALOR_TOTAL_DESCONTO');
  AddColuna('QUANTIDADE');
  AddColunaSL('QUANTIDADE_DEVOLVIDOS');
  AddColuna('UNIDADE_ENTRADA_ID');
  AddColuna('QUANTIDADE_ENTRADA_ALTIS');
  AddColuna('PRECO_UNITARIO');
  AddColuna('VALOR_TOTAL');
  AddColuna('VALOR_ICMS');
  AddColuna('PERCENTUAL_IPI');
  AddColuna('VALOR_IPI');
  AddColuna('VALOR_COFINS');
  AddColuna('PERCENTUAL_COFINS');
  AddColuna('BASE_CALCULO_ICMS');
  AddColuna('PERCENTUAL_ICMS');
  AddColuna('BASE_CALCULO_ICMS_ST');
  AddColuna('PERCENTUAL_ICMS_ST');
  AddColuna('VALOR_ICMS_ST');
  AddColuna('BASE_CALCULO_PIS');
  AddColuna('PERCENTUAL_PIS');
  AddColuna('VALOR_PIS');
  AddColuna('BASE_CALCULO_COFINS');
  AddColuna('INDICE_REDUCAO_BASE_ICMS_ST');
  AddColuna('INDICE_REDUCAO_BASE_ICMS');
  AddColuna('INFORMACOES_ADICIONAIS');
  AddColuna('CST_PIS');
  AddColuna('CST_COFINS');
  AddColuna('CST');
  AddColuna('CODIGO_BARRAS');
  AddColuna('IVA');
  AddColuna('PRECO_PAUTA');
  AddColuna('LOCAL_ENTRADA_ID');
  AddColunaSL('NOME_LOCAL');
  AddColuna('PESO_UNITARIO');

  AddColuna('VALOR_FRETE_CUSTO');
  AddColuna('VALOR_IPI_CUSTO');
  AddColuna('VALOR_ICMS_ST_CUSTO');
  AddColuna('VALOR_OUTROS_CUSTO');
  AddColuna('PRECO_FINAL');

  AddColuna('VALOR_ICMS_FRETE_CUSTO');
  AddColuna('VALOR_PIS_CUSTO');
  AddColuna('VALOR_COFINS_CUSTO');
  AddColuna('VALOR_PIS_FRETE_CUSTO');
  AddColuna('VALOR_COFINS_FRETE_CUSTO');
  AddColuna('PRECO_LIQUIDO');
  AddColuna('VALOR_DIFAL_CUSTO');
  AddColuna('VALOR_ICMS_CUSTO');

  AddColuna('QUANTIDADE_EMBALAGEM');
  AddColuna('MULTIPLO_COMPRA');
  AddColuna('UNIDADE_COMPRA_ID');
  AddColunaSL('TIPO_CONTROLE_ESTOQUE');
  AddColunaSL('EXIGIR_DATA_FABRICACAO_LOTE');
  AddColunaSL('EXIGIR_DATA_VENCIMENTO_LOTE');

  AddColunaSL('MULTIPLO_VENDA');
end;

function TEntradaNotaFiscalItem.getRecordEntradaNotaFiscalItem: RecEntradaNotaFiscalItem;
begin
  Result.entrada_id :=                  getInt('ENTRADA_ID', True);
  Result.produto_id :=                  getInt('PRODUTO_ID', True);
  Result.nome_produto :=                getString('NOME_COMPRA');
  Result.nome_marca :=                  getString('NOME_MARCA');
  Result.item_id :=                     getInt('ITEM_ID', True);
  Result.CfopId :=                      getString('CFOP_ID');
  Result.origem_produto :=              getInt('ORIGEM_PRODUTO');
  Result.codigo_ncm :=                  getString('CODIGO_NCM');
  Result.valor_total_outras_despesas := getDouble('VALOR_TOTAL_OUTRAS_DESPESAS');
  Result.valor_total_desconto :=        getDouble('VALOR_TOTAL_DESCONTO');
  Result.quantidade :=                  getDouble('QUANTIDADE');
  Result.QuantidadeDevolvidos        := getDouble('QUANTIDADE_DEVOLVIDOS');
  Result.preco_unitario :=              getDouble('PRECO_UNITARIO');
  Result.valor_total :=                 getDouble('VALOR_TOTAL');
  Result.valor_icms :=                  getDouble('VALOR_ICMS');
  Result.percentual_ipi :=              getDouble('PERCENTUAL_IPI');
  Result.valor_ipi :=                   getDouble('VALOR_IPI');
  Result.valor_cofins :=                getDouble('VALOR_COFINS');
  Result.percentual_cofins :=           getDouble('PERCENTUAL_COFINS');
  Result.base_calculo_icms :=           getDouble('BASE_CALCULO_ICMS');
  Result.percentual_icms :=             getDouble('PERCENTUAL_ICMS');
  Result.base_calculo_icms_st :=        getDouble('BASE_CALCULO_ICMS_ST');
  Result.percentual_icms_st :=          getDouble('PERCENTUAL_ICMS_ST');
  Result.valor_icms_st :=               getDouble('VALOR_ICMS_ST');
  Result.base_calculo_pis :=            getDouble('BASE_CALCULO_PIS');
  Result.percentual_pis :=              getDouble('PERCENTUAL_PIS');
  Result.valor_pis :=                   getDouble('VALOR_PIS');
  Result.base_calculo_cofins :=         getDouble('BASE_CALCULO_COFINS');
  Result.indice_reducao_base_icms_st := getDouble('INDICE_REDUCAO_BASE_ICMS_ST');
  Result.indice_reducao_base_icms :=    getDouble('INDICE_REDUCAO_BASE_ICMS');
  Result.informacoes_adicionais :=      getString('INFORMACOES_ADICIONAIS');
  Result.cst_pis :=                     getString('CST_PIS');
  Result.cst_cofins :=                  getString('CST_COFINS');
  Result.cst :=                         getString('CST');
  Result.codigo_barras :=               getString('CODIGO_BARRAS');
  Result.iva :=                         getDouble('IVA');
  Result.preco_pauta :=                 getDouble('PRECO_PAUTA');
  Result.LocalEntradaId :=              getInt('LOCAL_ENTRADA_ID');
  Result.NomeLocal :=                   getString('NOME_LOCAL');
  Result.Peso :=                        getDouble('PESO_UNITARIO');

  Result.ValorFreteCusto             := getDouble('VALOR_FRETE_CUSTO');
  Result.ValorIPICusto               := getDouble('VALOR_IPI_CUSTO');
  Result.ValorICMSStCusto            := getDouble('VALOR_ICMS_ST_CUSTO');
  Result.ValorOutrosCusto            := getDouble('VALOR_OUTROS_CUSTO');
  Result.PrecoFinal                  := getDouble('PRECO_FINAL');
  Result.ValorDifalCusto             := getDouble('VALOR_DIFAL_CUSTO');
  Result.ValorICMSCusto              := getDouble('VALOR_ICMS_CUSTO');
  Result.ValorICMSFreteCusto         := getDouble('VALOR_ICMS_FRETE_CUSTO');
  Result.ValorPisCusto               := getDouble('VALOR_PIS_CUSTO');
  Result.ValorCofinsCusto            := getDouble('VALOR_COFINS_CUSTO');
  Result.ValorPisFreteCusto          := getDouble('VALOR_PIS_FRETE_CUSTO');
  Result.ValorCofinsFreteCusto       := getDouble('VALOR_COFINS_FRETE_CUSTO');
  Result.PrecoLiquido                := getDouble('PRECO_LIQUIDO');

  Result.QuantidadeEmbalagem         := getDouble('QUANTIDADE_EMBALAGEM');
  Result.MultiploCompra              := getDouble('MULTIPLO_COMPRA');
  Result.UnidadeCompraId             := getString('UNIDADE_COMPRA_ID');
  Result.UnidadeEntradaId            := getString('UNIDADE_ENTRADA_ID');
  Result.QuantidadeEntradaAltis      := getDouble('QUANTIDADE_ENTRADA_ALTIS');

  Result.TipoControleEstoque         := getString('TIPO_CONTROLE_ESTOQUE');
  Result.ExigirDataFabricacaoLote    := getString('EXIGIR_DATA_FABRICACAO_LOTE');
  Result.ExigirDataVencimentoLote    := getString('EXIGIR_DATA_VENCIMENTO_LOTE');
  Result.unidade                     := getString('UNIDADE_COMPRA_ID');

  Result.MultiploVenda               := getDouble('MULTIPLO_VENDA');
end;

function AtualizarEntradaNotaFiscalItem(
  pConexao: TConexao;
  pEntradaId: Integer;
  pProdutoId: Integer;
  pItemId: Integer;
  pCfopId: Integer;
  pOrigemProduto: Integer;
  pCodigoNcm: Integer;
  pValorTotalOutrasDespesas: Double;
  pValorTotalDesconto: Double;
  pQuantidade: Double;
  pPrecoUnitario: Double;
  pValorTotal: Double;
  pValorIcms: Double;
  pPercentualIpi: Double;
  pValorIpi: Double;
  pValorCofins: Double;
  pPercentualCofins: Double;
  pBaseCalculoIcms: Double;
  pPercentualIcms: Double;
  pBaseCalculoIcmsSt: Double;
  pPercentualIcmsSt: Double;
  pValorIcmsSt: Double;
  pBaseCalculoPis: Double;
  pPercentualPis: Double;
  pValorPis: Double;
  pBaseCalculoCofins: Double;
  pIndiceReducaoBaseIcmsSt: Double;
  pIndiceReducaoBaseIcms: Double;
  pInformacoesAdicionais: string;
  pCstPis: string;
  pCstCofins: string;
  pCst: string;
  pCodigoBarras: string;
  pIva: Double;
  pPrecoPauta: Double
): RecRetornoBD;
var
  t: TEntradaNotaFiscalItem;
  novo: Boolean;
  seq: TSequencia;
begin
  Result.TeveErro := False;
  t := TEntradaNotaFiscalItem.Create(pConexao);

//  novo := pXXX = 0;
//
//  if novo then begin
//    seq := TSequencia.Create(pConexao, 'SEQ_XXX');
//    pXXX := seq.getProximaSequencia;
//    result.retorno_integer := pXXX;
//    seq.Free;
//  end;

  try
    t.setInt('ENTRADA_ID', pEntradaId, True);
    t.setInt('PRODUTO_ID', pProdutoId, True);
    t.setInt('ITEM_ID', pItemId, True);
    t.setInt('CFOP_ID', pCfopId);
    t.setInt('ORIGEM_PRODUTO', pOrigemProduto);
    t.setInt('CODIGO_NCM', pCodigoNcm);
    t.setDouble('VALOR_TOTAL_OUTRAS_DESPESAS', pValorTotalOutrasDespesas);
    t.setDouble('VALOR_TOTAL_DESCONTO', pValorTotalDesconto);
    t.setDouble('QUANTIDADE', pQuantidade);
    t.setDouble('PRECO_UNITARIO', pPrecoUnitario);
    t.setDouble('VALOR_TOTAL', pValorTotal);
    t.setDouble('VALOR_ICMS', pValorIcms);
    t.setDouble('PERCENTUAL_IPI', pPercentualIpi);
    t.setDouble('VALOR_IPI', pValorIpi);
    t.setDouble('VALOR_COFINS', pValorCofins);
    t.setDouble('PERCENTUAL_COFINS', pPercentualCofins);
    t.setDouble('BASE_CALCULO_ICMS', pBaseCalculoIcms);
    t.setDouble('PERCENTUAL_ICMS', pPercentualIcms);
    t.setDouble('BASE_CALCULO_ICMS_ST', pBaseCalculoIcmsSt);
    t.setDouble('PERCENTUAL_ICMS_ST', pPercentualIcmsSt);
    t.setDouble('VALOR_ICMS_ST', pValorIcmsSt);
    t.setDouble('BASE_CALCULO_PIS', pBaseCalculoPis);
    t.setDouble('PERCENTUAL_PIS', pPercentualPis);
    t.setDouble('VALOR_PIS', pValorPis);
    t.setDouble('BASE_CALCULO_COFINS', pBaseCalculoCofins);
    t.setDouble('INDICE_REDUCAO_BASE_ICMS_ST', pIndiceReducaoBaseIcmsSt);
    t.setDouble('INDICE_REDUCAO_BASE_ICMS', pIndiceReducaoBaseIcms);
    t.setString('INFORMACOES_ADICIONAIS', pInformacoesAdicionais);
    t.setString('CST_PIS', pCstPis);
    t.setString('CST_COFINS', pCstCofins);
    t.setString('CST', pCst);
    t.setString('CODIGO_BARRAS', pCodigoBarras);
    t.setDouble('IVA', pIva);
    t.setDouble('PRECO_PAUTA', pPrecoPauta);

    if novo then
      t.Inserir
    else
      t.Atualizar;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function BuscarEntradaNotaFiscalItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEntradaNotaFiscalItem>;
var
  i: Integer;
  t: TEntradaNotaFiscalItem;
begin
  Result := nil;
  t := TEntradaNotaFiscalItem.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordEntradaNotaFiscalItem;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarEntradaNotaFiscalItensComando(pConexao: TConexao; pComando: string): TArray<RecEntradaNotaFiscalItem>;
var
  i: Integer;
  t: TEntradaNotaFiscalItem;
begin
  Result := nil;
  t := TEntradaNotaFiscalItem.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordEntradaNotaFiscalItem;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirEntradaNotaFiscalItem(
  pConexao: TConexao;
  pEntradaId: Integer;
  pProdutoId: Integer;
  pItemId: Integer
): RecRetornoBD;
var
  t: TEntradaNotaFiscalItem;
begin
  Result.TeveErro := False;
  t := TEntradaNotaFiscalItem.Create(pConexao);

  try
    t.setInt('ENTRADA_ID', pEntradaId, True);
    t.setInt('PRODUTO_ID', pProdutoId, True);
    t.setInt('ITEM_ID', pItemId, True);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
