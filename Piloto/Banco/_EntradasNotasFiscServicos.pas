unit _EntradasNotasFiscServicos;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsFinanceiros, _EntradasNfServicosFinanc;

{$M+}
type
  RecEntradasNotasFiscServicos = record
    EntradaId: Integer;
    EmpresaId: Integer;
    FornecedorId: Integer;
    NumeroNota: Integer;
    ModeloNota: string;
    SerieNota: string;
    DataEmissao: TDateTime;
    DataContabil: TDateTime;
    CfopId: string;
    CentroCustoId: Integer;
    ValorTotalNota: Double;
    PlanoFinanceiroId: string;
    UsuarioCadastroId: Integer;
    DataHoraCadastro: TDateTime;
  end;

  TEntradasNotasFiscServicos = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordEntradasNotasFiscServicos: RecEntradasNotasFiscServicos;
  end;

function AtualizarEntradasNotasFiscServicos(
  pConexao: TConexao;
  pEntradaId: Integer;
  pEmpresaId: Integer;
  pFornecedorId: Integer;
  pNumeroNota: Integer;
  pModeloNota: string;
  pSerieNota: string;
  pDataEmissao: TDateTime;
  pDataContabil: TDateTime;
  pCfopId: string;
  pValorTotalNota: Double;
  pPlanoFinanceiroId: string;
  pCentroCustoId: Integer;
  pEntradaFinanceiro: TArray<RecTitulosFinanceiros>
): RecRetornoBD;

function BuscarEntradasNotasFiscServicos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEntradasNotasFiscServicos>;

function ExcluirEntradasNotasFiscServicos(
  pConexao: TConexao;
  pEntradaId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TEntradasNotasFiscServicos }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ENTRADA_ID = :P1 '
    );
end;

constructor TEntradasNotasFiscServicos.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ENTRADAS_NOTAS_FISC_SERVICOS');

  FSql := 
    'select ' +
    '  ENTRADA_ID, ' +
    '  EMPRESA_ID, ' +
    '  FORNECEDOR_ID, ' +
    '  NUMERO_NOTA, ' +
    '  MODELO_NOTA, ' +
    '  SERIE_NOTA, ' +
    '  DATA_EMISSAO, ' +
    '  DATA_CONTABIL, ' +
    '  CFOP_ID, ' +
    '  VALOR_TOTAL_NOTA, ' +
    '  PLANO_FINANCEIRO_ID, ' +
    '  CENTRO_CUSTO_ID, ' +
    '  USUARIO_CADASTRO_ID, ' +
    '  DATA_HORA_CADASTRO ' +
    'from ' +
    '  ENTRADAS_NOTAS_FISC_SERVICOS';

  setFiltros(getFiltros);

  AddColuna('ENTRADA_ID', True);
  AddColuna('EMPRESA_ID');
  AddColuna('FORNECEDOR_ID');
  AddColuna('NUMERO_NOTA');
  AddColuna('MODELO_NOTA');
  AddColuna('SERIE_NOTA');
  AddColuna('DATA_EMISSAO');
  AddColuna('DATA_CONTABIL');
  AddColuna('CFOP_ID');
  AddColuna('VALOR_TOTAL_NOTA');
  AddColuna('PLANO_FINANCEIRO_ID');
  AddColuna('CENTRO_CUSTO_ID');
  AddColunaSL('USUARIO_CADASTRO_ID');
  AddColunaSL('DATA_HORA_CADASTRO');
end;

function TEntradasNotasFiscServicos.getRecordEntradasNotasFiscServicos: RecEntradasNotasFiscServicos;
begin
  Result.EntradaId             := getInt('ENTRADA_ID', True);
  Result.EmpresaId             := getInt('EMPRESA_ID');
  Result.FornecedorId          := getInt('FORNECEDOR_ID');
  Result.NumeroNota            := getInt('NUMERO_NOTA');
  Result.ModeloNota            := getString('MODELO_NOTA');
  Result.SerieNota             := getString('SERIE_NOTA');
  Result.DataEmissao           := getData('DATA_EMISSAO');
  Result.DataContabil          := getData('DATA_CONTABIL');
  Result.CfopId                := getString('CFOP_ID');
  Result.ValorTotalNota        := getDouble('VALOR_TOTAL_NOTA');
  Result.PlanoFinanceiroId     := getString('PLANO_FINANCEIRO_ID');
  Result.CentroCustoId         := getInt('CENTRO_CUSTO_ID');
  Result.UsuarioCadastroId     := getInt('USUARIO_CADASTRO_ID');
  Result.DataHoraCadastro      := getData('DATA_HORA_CADASTRO');
end;

function AtualizarEntradasNotasFiscServicos(
  pConexao: TConexao;
  pEntradaId: Integer;
  pEmpresaId: Integer;
  pFornecedorId: Integer;
  pNumeroNota: Integer;
  pModeloNota: string;
  pSerieNota: string;
  pDataEmissao: TDateTime;
  pDataContabil: TDateTime;
  pCfopId: string;
  pValorTotalNota: Double;
  pPlanoFinanceiroId: string;
  pCentroCustoId: Integer;
  pEntradaFinanceiro: TArray<RecTitulosFinanceiros>
): RecRetornoBD;
var
  t: TEntradasNotasFiscServicos;
  vNovo: Boolean;

  i: Integer;
  vSeq: TSequencia;
  vExec: TExecucao;

  vProc: TProcedimentoBanco;
  vFinanc: TEntradasNfServicosFinanc;
begin
  Result.Iniciar;
  pConexao.setRotina('ATUALIZAR_ENTRADA_NF_SERVICO');

  t := TEntradasNotasFiscServicos.Create(pConexao);
  vExec := TExecucao.Create(pConexao);
  vFinanc := TEntradasNfServicosFinanc.Create(pConexao);
  vProc := TProcedimentoBanco.Create(pConexao, 'CONS_FINANC_ENTR_NF_SERVICOS');

  vNovo := pEntradaId = 0;
  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_ENTRADAS_NF_SERVICOS');
    pEntradaId := vSeq.getProximaSequencia;
    Result.AsInt := pEntradaId;
    vSeq.Free;
  end;

  try
    pConexao.IniciarTransacao; 

    t.setInt('ENTRADA_ID', pEntradaId, True);
    t.setInt('EMPRESA_ID', pEmpresaId);
    t.setInt('FORNECEDOR_ID', pFornecedorId);
    t.setInt('NUMERO_NOTA', pNumeroNota);
    t.setString('MODELO_NOTA', pModeloNota);
    t.setString('SERIE_NOTA', pSerieNota);
    t.setData('DATA_EMISSAO', pDataEmissao);
    t.setData('DATA_CONTABIL', pDataContabil);
    t.setString('CFOP_ID', pCfopId);
    t.setDouble('VALOR_TOTAL_NOTA', pValorTotalNota);
    t.setString('PLANO_FINANCEIRO_ID', pPlanoFinanceiroId);
    t.setInt('CENTRO_CUSTO_ID', pCentroCustoId);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    if not vNovo then begin
      vExec.Limpar;
      vExec.Add('delete from CONTAS_PAGAR ');
      vExec.Add('where ENTRADA_SERVICO_ID = :P1 ');
      vExec.Executar([pEntradaId]);

      vExec.Limpar;
      vExec.Add('delete from ENTRADAS_NF_SERVICOS_FINANC ');
      vExec.Add('where ENTRADA_ID = :P1 ');
      vExec.Executar([pEntradaId]);
    end;

    for i := Low(pEntradaFinanceiro) to High(pEntradaFinanceiro) do begin
      vFinanc.setInt('ENTRADA_ID', pEntradaId, True);
      vFinanc.setInt('ITEM_ID', pEntradaFinanceiro[i].ItemId, True);
      vFinanc.setInt('COBRANCA_ID', pEntradaFinanceiro[i].CobrancaId);
      vFinanc.setData('DATA_VENCIMENTO', pEntradaFinanceiro[i].DataVencimento);
      vFinanc.setInt('PARCELA', pEntradaFinanceiro[i].Parcela);
      vFinanc.setInt('NUMERO_PARCELAS', pEntradaFinanceiro[i].NumeroParcelas);
      vFinanc.setDouble('VALOR_DOCUMENTO', pEntradaFinanceiro[i].Valor);
      vFinanc.setString('DOCUMENTO', pEntradaFinanceiro[i].Documento);
      vFinanc.setStringN('NOSSO_NUMERO', pEntradaFinanceiro[i].NossoNumero);
      vFinanc.setStringN('CODIGO_BARRAS', pEntradaFinanceiro[i].CodigoBarras);

      vFinanc.Inserir;
    end;

    vProc.Executar([pEntradaId]);

    pConexao.FinalizarTransacao; 
  except
    on e: Exception do begin
      pConexao.VoltarTransacao; 
      Result.TratarErro(e);
    end;
  end;

  vFinanc.Free;
  vExec.Free;
  vProc.Free;
  t.Free;
end;

function BuscarEntradasNotasFiscServicos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEntradasNotasFiscServicos>;
var
  i: Integer;
  t: TEntradasNotasFiscServicos;
begin
  Result := nil;
  t := TEntradasNotasFiscServicos.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordEntradasNotasFiscServicos;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirEntradasNotasFiscServicos(
  pConexao: TConexao;
  pEntradaId: Integer
): RecRetornoBD;
var
  t: TEntradasNotasFiscServicos;
begin
  Result.Iniciar;
  t := TEntradasNotasFiscServicos.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('ENTRADA_ID', pEntradaId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
