unit _Veiculos;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils;

{$M+}
type
  RecVeiculos = class
  public
    VeiculoId: Integer;
    MotoristaId: Integer;
    NomeMotorista: string;
    Nome: string;
    Ativo: string;
    Placa: string;
    Ano: Integer;
    TipoCombustivel: string;
    Chasssi: string;
    Cor: string;
    TipoVeiculo: string;
    PesoMaximo: Double;
    Tara: Double;
  end;

  TVeiculos = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordCentros: RecVeiculos;
  end;

function AtualizarVeiculo(
  pConexao: TConexao;
  pVeiculoId: Integer;
  pMotoristaId: Integer;
  pNome: string;
  pAtivo: string;
  pPlaca: string;
  pAno: Integer;
  pTipoCombustivel: string;
  pChasssi: string;
  pCor: string;
  pTipoVeiculo: string;
  pPesoMaximo: Double;
  pTara: Double
): RecRetornoBD;

function BuscarVeiculos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecVeiculos>;

function ExcluirVeiculo(
  pConexao: TConexao;
  pVeiculoId: Integer
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TVeiculos }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where VEICULO_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Ve�culos ( avan�ado )',
      True,
      0,
      'where NOME like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  NOME',
      '',
      True
    );
end;

constructor TVeiculos.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'VEICULOS');

  FSql :=
    'select ' +
    '  VEI.VEICULO_ID, ' +
    '  VEI.NOME, ' +
    '  VEI.MOTORISTA_ID, ' +
    '  VEI.PLACA, ' +
    '  VEI.ANO, ' +
    '  VEI.TIPO_COMBUSTIVEL, ' +
    '  VEI.CHASSSI, ' +
    '  VEI.COR, ' +
    '  VEI.TIPO_VEICULO, ' +
    '  VEI.PESO_MAXIMO, ' +
    '  VEI.ATIVO, ' +
    '  VEI.TARA, ' +
    '  CAD.NOME_FANTASIA as NOME_MOTORISTA ' +
    'from ' +
    '  VEICULOS VEI ' +

    'left join CADASTROS CAD ' +
    'on VEI.MOTORISTA_ID = CAD.CADASTRO_ID ';

  SetFiltros(GetFiltros);

  AddColuna('VEICULO_ID', True);
  AddColuna('NOME');
  AddColuna('MOTORISTA_ID');
  AddColunaSL('NOME_MOTORISTA');
  AddColuna('PLACA');
  AddColuna('ANO');
  AddColuna('TIPO_COMBUSTIVEL');
  AddColuna('CHASSSI');
  AddColuna('COR');
  AddColuna('TIPO_VEICULO');
  AddColuna('PESO_MAXIMO');
  AddColuna('ATIVO');
  AddColuna('TARA');
end;

function TVeiculos.GetRecordCentros: RecVeiculos;
begin
  Result := RecVeiculos.Create;

  Result.VeiculoId       := GetInt('VEICULO_ID', True);
  Result.Nome            := GetString('NOME');
  Result.Ativo           := GetString('ATIVO');
  Result.MotoristaId     := GetInt('MOTORISTA_ID');
  Result.NomeMotorista   := GetString('NOME_MOTORISTA');
  Result.Placa           := GetString('PLACA');
  Result.Ano             := GetInt('ANO');
  Result.TipoCombustivel := GetString('TIPO_COMBUSTIVEL');
  Result.Chasssi         := GetString('CHASSSI');
  Result.Cor             := GetString('COR');
  Result.TipoVeiculo     := GetString('TIPO_VEICULO');
  Result.PesoMaximo      := GetDouble('PESO_MAXIMO');
  Result.Tara            := GetDouble('TARA');
end;

function AtualizarVeiculo(
  pConexao: TConexao;
  pVeiculoId: Integer;
  pMotoristaId: Integer;
  pNome: string;
  pAtivo: string;
  pPlaca: string;
  pAno: Integer;
  pTipoCombustivel: string;
  pChasssi: string;
  pCor: string;
  pTipoVeiculo: string;
  pPesoMaximo: Double;
  pTara: Double
): RecRetornoBD;
var
  vVeiculo: TVeiculos;
  vNovo: Boolean;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_VEICULOS');

  vVeiculo := TVeiculos.Create(pConexao);

  vNovo := pVeiculoId = 0;
  if vNovo then begin
    pVeiculoId := TSequencia.Create(pConexao, 'SEQ_VEICULO_ID').GetProximaSequencia;
    Result.AsInt := pVeiculoId;
  end;

  vVeiculo.SetInt('VEICULO_ID', pVeiculoId, True);
  vVeiculo.setInt('MOTORISTA_ID', pMotoristaId);
  vVeiculo.SetString('NOME', pNome);
  vVeiculo.SetString('PLACA', pPlaca);
  vVeiculo.setIntN('ANO', pAno);
  vVeiculo.SetString('TIPO_COMBUSTIVEL', pTipoCombustivel);
  vVeiculo.SetString('CHASSSI', pChasssi);
  vVeiculo.SetString('COR', pCor);
  vVeiculo.SetString('TIPO_VEICULO', pTipoVeiculo);
  vVeiculo.setDouble('PESO_MAXIMO', pPesoMaximo);
  vVeiculo.SetString('ATIVO', pAtivo);
  vVeiculo.setDouble('TARA', pTara);

  try
    pConexao.IniciarTransacao;

    if vNovo then
      vVeiculo.Inserir
    else
      vVeiculo.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vVeiculo.Free;
end;

function BuscarVeiculos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecVeiculos>;
var
  i: Integer;
  t: TVeiculos;
  vSql: string;
begin
  Result := nil;
  t := TVeiculos.Create(pConexao);

  vSql := '';
  if pSomenteAtivos then
    vSql := vSql + 'and VEI.ATIVO = ''S'' ';

  if t.Pesquisar(pIndice, pFiltros, vSql) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordCentros;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirVeiculo(
  pConexao: TConexao;
  pVeiculoId: Integer
): RecRetornoBD;
var
  t: TVeiculos;
begin
  pConexao.SetRotina('EXCLUIR_ROTA');
  Result.TeveErro := False;
  t := TVeiculos.Create(pConexao);

  t.SetInt('VEICULO_ID', pVeiculoId, True);

  try
    pConexao.IniciarTransacao;

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
