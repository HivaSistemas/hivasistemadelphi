unit _ListaContagem;

interface

uses
  _Conexao, _OperacoesBancoDados;

type
  RecRelacaoEstoque = record
    ProdutoId: Integer;
    EmpresaId: Integer;
    NomeProduto: string;
    EstoqueFiscal: Double;
   // Fisico: Double;
    Disponivel: Double;
    Reservado: Double;
    CMV: Double;
    PrecoFinal: Double;
    PrecoCompra: Double;
    MarcaId: Integer;
    NomeMarca: string;
    LocalId: Integer;
    NomeLocal: string;
    UnidadeVenda: string;
    CodigoBarras: string;
    CodigoOriginal: string;
    Lote: string;
    EstoqueFisico: string;
  end;

function BuscarEstoque(
  pConexao: TConexao;
  pComando: string;
  pComandoEndereco: string;
  pAgruparLocais: Boolean;
  pAgruparMarca: Boolean;
  pComandOrderBy: string
): TArray<RecRelacaoEstoque>;

implementation

function BuscarEstoque(
  pConexao: TConexao;
  pComando: string;
  pComandoEndereco: string;
  pAgruparLocais: Boolean;
  pAgruparMarca: Boolean;
  pComandOrderBy: string
): TArray<RecRelacaoEstoque>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  if pAgruparLocais then begin
    vSql.Add('select ');
    vSql.Add('  EST.PRODUTO_ID, ');
    vSql.Add('  PRO.NOME as NOME_PRODUTO, ');
    vSql.Add('  sum(EST.ESTOQUE) as ESTOQUE_FISCAL, ');
    vSql.Add('  sum(DIV.FISICO) as FISICO, ');
    vSql.Add('  sum(EST.DISPONIVEL) as DISPONIVEL, ');
    vSql.Add('  sum(EST.RESERVADO) as RESERVADO, ');
    vSql.Add('  PRO.MARCA_ID, ');
    vSql.Add('  MAR.NOME as NOME_MARCA, ');
    vSql.Add('  DIV.LOCAL_ID, ');
    vSql.Add('  PRO.UNIDADE_VENDA, ');
    vSql.Add('  PRO.CODIGO_BARRAS, ');
    vSql.Add('  PRO.CODIGO_ORIGINAL_FABRICANTE, ');
    vSql.Add('  DIV.LOTE ');
    vSql.Add('from ');
    vSql.Add('  ESTOQUES EST ');

    vSql.Add('inner join PRODUTOS PRO ');
    vSql.Add('on EST.PRODUTO_ID = PRO.PRODUTO_ID ');

    vSql.Add('inner join CUSTOS_PRODUTOS CUS ');
    vSql.Add('on PRO.PRODUTO_ID = CUS.PRODUTO_ID ');
    vSql.Add('and EST.EMPRESA_ID = CUS.EMPRESA_ID ');

    vSql.Add('inner join MARCAS MAR ');
    vSql.Add('on PRO.MARCA_ID = MAR.MARCA_ID ');

    vSql.Add(pComandoEndereco);

    vSql.Add('inner join ESTOQUES_DIVISAO DIV ');
    vSql.Add('on DIV.PRODUTO_ID = PRO.PRODUTO_ID ');
    vSql.Add('and DIV.EMPRESA_ID = EST.EMPRESA_ID ');
    vSql.Add('and DIV.PRODUTO_ID = EST.PRODUTO_ID ');

    vSql.Add(pComando);

    vSql.Add(' group by ');
    vSql.Add('  EST.PRODUTO_ID, ');
    vSql.Add('  DIV.LOCAL_ID, ');
    vSql.Add('  PRO.NOME, ');
    vSql.Add('  PRO.MARCA_ID, ');
    vSql.Add('  MAR.NOME, ');
    vSql.Add('  DIV.LOCAL_ID, ');
    vSql.Add('  PRO.UNIDADE_VENDA, ');
    vSql.Add('  PRO.CODIGO_BARRAS, ');
    vSql.Add('  PRO.CODIGO_ORIGINAL_FABRICANTE, ');
    vSql.Add('  DIV.LOTE ');

    vSql.Add(pComandOrderBy);
  end
  else begin
    vSql.Add('select ');
    vSql.Add('  EST.PRODUTO_ID, ');
    vSql.Add('  EST.EMPRESA_ID, ');
    vSql.Add('  PRO.NOME as NOME_PRODUTO, ');
    vSql.Add('  EST.ESTOQUE as ESTOQUE_FISCAL, ');
    vSql.Add('  DIV.FISICO, ');
    vSql.Add('  EST.DISPONIVEL, ');
    vSql.Add('  EST.RESERVADO, ');
    vSql.Add('  PRO.MARCA_ID, ');
    vSql.Add('  MAR.NOME as NOME_MARCA, ');
    vSql.Add('  DIV.LOCAL_ID, ');
    //vSql.Add('  LOC.NOME as NOME_LOCAL, ');
    vSql.Add('  PRO.UNIDADE_VENDA, ');
    vSql.Add('  PRO.CODIGO_BARRAS, ');
    vSql.Add('  PRO.CODIGO_ORIGINAL_FABRICANTE, ');
    vSql.Add('  DIV.LOTE ');
    vSql.Add('from ');
    vSql.Add('  ESTOQUES EST ');

    vSql.Add('inner join PRODUTOS PRO ');
    vSql.Add('on EST.PRODUTO_ID = PRO.PRODUTO_ID ');

    vSql.Add('inner join CUSTOS_PRODUTOS CUS ');
    vSql.Add('on PRO.PRODUTO_ID = CUS.PRODUTO_ID ');
    vSql.Add('and EST.EMPRESA_ID = CUS.EMPRESA_ID ');

    vSql.Add('inner join MARCAS MAR ');
    vSql.Add('on PRO.MARCA_ID = MAR.MARCA_ID ');

    vSql.Add(pComandoEndereco);

   //  vSql.Add('inner join LOCAIS LOC ');
   // vSql.Add('on DIV.LOCAL_ID = LOC.LOCAL_ID ');

    vSql.Add('inner join ESTOQUES_DIVISAO DIV ');
    vSql.Add('on DIV.PRODUTO_ID = PRO.PRODUTO_ID ');
    vSql.Add('and DIV.EMPRESA_ID = EST.EMPRESA_ID ');
    vSql.Add('and DIV.PRODUTO_ID = EST.PRODUTO_ID ');

    vSql.Add(pComando);
  end;

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      if not pAgruparLocais then
        Result[i].EmpresaId     := vSql.GetInt('EMPRESA_ID');
      Result[i].ProdutoId     := vSql.GetInt('PRODUTO_ID');
      Result[i].NomeProduto   := vSql.GetString('NOME_PRODUTO');
      Result[i].EstoqueFiscal := vSql.GetDouble('ESTOQUE_FISCAL');
      //Result[i].Fisico        := vSql.GetDouble('FISICO');
      Result[i].Disponivel    := vSql.GetDouble('DISPONIVEL');
      Result[i].Reservado     := vSql.GetDouble('RESERVADO');
      Result[i].MarcaId       := vSql.getInt('MARCA_ID');
      Result[i].NomeMarca     := vSql.GetString('NOME_MARCA');
      Result[i].LocalId       := vSql.getInt('LOCAL_ID');
      //Result[i].NomeLocal     := vSql.GetString('NOME_LOCAL');
      Result[i].UnidadeVenda  := vSql.GetString('UNIDADE_VENDA');
      Result[i].CodigoBarras  := vSql.GetString('CODIGO_BARRAS');
      Result[i].CodigoOriginal  := vSql.GetString('CODIGO_ORIGINAL_FABRICANTE');
      Result[i].Lote          := vSql.GetString('LOTE');
      Result[i].EstoqueFisico  := vSql.GetString('FISICO');

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

end.


