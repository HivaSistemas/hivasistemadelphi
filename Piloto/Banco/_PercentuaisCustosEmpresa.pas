unit _PercentuaisCustosEmpresa;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, System.Math;

{$M+}
type
  RecPercentuaisCustosEmpresa = record
    EmpresaId: Integer;
    PercentualPis: Double;
    PercentualCofins: Double;
    PercentualCSLL: Double;
    PercentualIRPJ: Double;
    PercentualCustoFixo: Double;
    PercentualFreteVenda: Double;
    PercentualComissao: Double;
  end;

  TPercentuaisCustosEmpresa = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordPercentuais: RecPercentuaisCustosEmpresa;
  end;

function AtualizarPercentuaisCustos(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pPercentualPis: Double;
  pPercentualCofins: Double;
  pPercentualCSLL: Double;
  pPercentualIRPJ: Double;
  pPercentualComissao: Double;
  pPercentualFreteVenda: Double;
  pPercentualCustoFixo: Double
): RecRetornoBD;

function BuscarPercentuaisCustos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecPercentuaisCustosEmpresa>;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TPercentuaisCustosEmpresa }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where EMPRESA_ID = :P1 '
    );
end;

constructor TPercentuaisCustosEmpresa.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PERCENTUAIS_CUSTOS_EMPRESA');

  FSql :=
    'select ' +
    '  EMPRESA_ID, ' +
    '  PERCENTUAL_PIS, ' +
    '  PERCENTUAL_COFINS, ' +
    '  PERCENTUAL_CSLL, ' +
    '  PERCENTUAL_IRPJ, ' +
    '  PERCENTUAL_COMISSAO, ' +
    '  PERCENTUAL_CUSTO_FIXO, ' +
    '  PERCENTUAL_FRETE_VENDA ' +
    'from ' +
    '  PERCENTUAIS_CUSTOS_EMPRESA ';

  SetFiltros(GetFiltros);

  AddColuna('EMPRESA_ID', True);
  AddColuna('PERCENTUAL_PIS');
  AddColuna('PERCENTUAL_COFINS');
  AddColuna('PERCENTUAL_CSLL');
  AddColuna('PERCENTUAL_IRPJ');
  AddColuna('PERCENTUAL_COMISSAO');
  AddColuna('PERCENTUAL_CUSTO_FIXO');
  AddColuna('PERCENTUAL_FRETE_VENDA');
end;

function TPercentuaisCustosEmpresa.GetRecordPercentuais: RecPercentuaisCustosEmpresa;
begin
  Result.EmpresaId            := GetInt('EMPRESA_ID', True);
  Result.PercentualPis        := getDouble('PERCENTUAL_PIS');
  Result.PercentualCofins     := getDouble('PERCENTUAL_COFINS');
  Result.PercentualCSLL       := getDouble('PERCENTUAL_CSLL');
  Result.PercentualIRPJ       := getDouble('PERCENTUAL_IRPJ');
  Result.PercentualComissao   := getDouble('PERCENTUAL_COMISSAO');
  Result.PercentualCustoFixo  := getDouble('PERCENTUAL_CUSTO_FIXO');
  Result.PercentualFreteVenda := getDouble('PERCENTUAL_FRETE_VENDA');
end;

function AtualizarPercentuaisCustos(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pPercentualPis: Double;
  pPercentualCofins: Double;
  pPercentualCSLL: Double;
  pPercentualIRPJ: Double;
  pPercentualComissao: Double;
  pPercentualFreteVenda: Double;
  pPercentualCustoFixo: Double
): RecRetornoBD;
var
  vPerc: TPercentuaisCustosEmpresa;
begin
  Result.TeveErro := False;
  vPerc := TPercentuaisCustosEmpresa.Create(pConexao);
  pConexao.SetRotina('ATUALIZAR_PERC_CUSTOS_EMPRESA');

  vPerc.SetInt('EMPRESA_ID', pEmpresaId, True);
  vPerc.setDouble('PERCENTUAL_PIS', pPercentualPis);
  vPerc.setDouble('PERCENTUAL_COFINS', pPercentualCofins);
  vPerc.setDouble('PERCENTUAL_CSLL', pPercentualCSLL);
  vPerc.setDouble('PERCENTUAL_IRPJ', pPercentualIRPJ);
  vPerc.setDouble('PERCENTUAL_COMISSAO', pPercentualComissao);
  vPerc.setDouble('PERCENTUAL_CUSTO_FIXO', pPercentualCustoFixo);
  vPerc.setDouble('PERCENTUAL_FRETE_VENDA', pPercentualFreteVenda);

  try
    pConexao.IniciarTransacao;

    vPerc.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vPerc.Free;
end;

function BuscarPercentuaisCustos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecPercentuaisCustosEmpresa>;
var
  i: Integer;
  t: TPercentuaisCustosEmpresa;
begin
  Result := nil;
  t := TPercentuaisCustosEmpresa.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordPercentuais;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
