unit _Portadores;

interface

uses
  _OperacoesBancoDados, _RecordsEspeciais, _Conexao, System.SysUtils, System.Variants, _BibliotecaGenerica, _PortadoresContas;

{$M+}
type
  RecPortadores = class
  public
    PortadorId: string;
    Descricao: string;
    Tipo: string;
    IncidenciaFinanceiro: string;
    InstrucaoBoleto: string;
    ativo: string;
    ContasBoletoId: integer;
  end;

  TPortadores = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordPortador: RecPortadores;
  end;

function AtualizarPortador(
  pConexao: TConexao;
  pPortadorId: string;
  pDescricao: string;
  pTipo: string;
  pIncidenciaFinanceiro: string;
  pInstrucaoBoleto: string;
  pAtivo: string;
  pContasBoletosId: integer;
  pContasRecebimento: TArray<RecPortadoresContas>
): RecRetornoBD;

function BuscarPortadores(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant
): TArray<RecPortadores>;

function ExcluirPortador(
  pConexao: TConexao;
  pPortadorId: string
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TPortadores }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'C�d. Portador',
      False,
      0,
      'where PORTADOR_ID like ''%'' || :P1 || ''%'' '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Descri��o portador',
      True,
      1,
      'where DESCRICAO like ''%'' || :P1 || ''%'' '
    );
end;

constructor TPortadores.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PORTADORES');

  FSql :=
    'select ' +
    '  PORTADOR_ID, ' +
    '  DESCRICAO, ' +
    '  TIPO, ' +
    '  INCIDENCIA_FINANCEIRO, ' +
    '  INSTRUCAO_BOLETO, ' +
    '  ATIVO, ' +
    '  CONTABOLETO_ID ' +
    'from ' +
    '  PORTADORES ';

  SetFiltros(GetFiltros);

  AddColuna('PORTADOR_ID', True);
  AddColuna('DESCRICAO');
  AddColuna('TIPO');
  AddColuna('INCIDENCIA_FINANCEIRO');
  AddColuna('INSTRUCAO_BOLETO');
  AddColuna('ATIVO');
  AddColuna('CONTABOLETO_ID');
end;

function TPortadores.GetRecordPortador: RecPortadores;
begin
  Result := RecPortadores.Create;

  Result.PortadorId           := GetString('PORTADOR_ID', True);
  Result.Descricao            := GetString('DESCRICAO');
  Result.Tipo                 := GetString('TIPO');
  Result.IncidenciaFinanceiro := GetString('INCIDENCIA_FINANCEIRO');
  Result.InstrucaoBoleto      := GetString('INSTRUCAO_BOLETO');
  Result.ativo                := GetString('ATIVO');
  Result.ContasBoletoId       := getInt('CONTABOLETO_ID');
end;

function AtualizarPortador(
  pConexao: TConexao;
  pPortadorId: string;
  pDescricao: string;
  pTipo: string;
  pIncidenciaFinanceiro: string;
  pInstrucaoBoleto: string;
  pAtivo: string;
  pContasBoletosId: integer;
  pContasRecebimento: TArray<RecPortadoresContas>
): RecRetornoBD;
var
  i: Integer;
  vNovo: Boolean;
  vPort: TPortadores;
  vPortContas: TPortadoresContas;

  vExec: TExecucao;
begin
  Result.TeveErro := False;

  vExec := TExecucao.Create(pConexao);
  vPort := TPortadores.Create(pConexao);
  vPortContas := TPortadoresContas.Create(pConexao);

  vExec.Add('delete from PORTADORES_CONTAS ');
  vExec.Add('where PORTADOR_ID = :P1 ');

  try
    pConexao.IniciarTransacao;

    vNovo := not vPort.Pesquisar(0, [pPortadorId]);

    vPort.SetString('PORTADOR_ID', pPortadorId, True);
    vPort.SetString('DESCRICAO', pDescricao);
    vPort.SetString('TIPO', pTipo);
    vPort.SetString('INCIDENCIA_FINANCEIRO', pIncidenciaFinanceiro);
    vPort.SetString('INSTRUCAO_BOLETO', pInstrucaoBoleto);
    vPort.SetString('ATIVO', pAtivo);
    vPort.setInt('CONTABOLETO_ID', pContasBoletosId);

    if vNovo then
      vPort.Inserir
    else
      vPort.Atualizar;

    vExec.Executar([pPortadorId]);
    for i := Low(pContasRecebimento) to High(pContasRecebimento) do begin
      vPortContas.setInt('EMPRESA_ID', pContasRecebimento[i].EmpresaId, True);
      vPortContas.setString('PORTADOR_ID', pContasRecebimento[i].PortadorId, True);
      vPortContas.setString('CONTA_ID', pContasRecebimento[i].ContaId, True);

      vPortContas.Inserir;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vPortContas.Free;
  vPort.Free;
end;

function BuscarPortadores(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant
): TArray<RecPortadores>;
var
  i: Integer;
  t: TPortadores;
begin
  Result := nil;
  t := TPortadores.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordPortador;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirPortador(
  pConexao: TConexao;
  pPortadorId: string
): RecRetornoBD;
var
  t: TPortadores;
begin
  result.TeveErro := False;
  t := TPortadores.Create(pConexao);
  try
    pConexao.IniciarTransacao;

    t.setString('PORTADOR_ID', pPortadorId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
