unit _GruposFornecedores;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils;

{$M+}
type
  RecGruposFornecedores = class
  public
    GrupoId: Integer;
    Descricao: string;
    Ativo: string;
  end;

  TGruposFornecedores = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordRotas: RecGruposFornecedores;
  end;

function AtualizarGrupo(
  pConexao: TConexao;
  pGrupoId: Integer;
  pDescricao: string;
  pAtivo: string
): RecRetornoBD;

function BuscarGrupos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecGruposFornecedores>;

function ExcluirGrupo(
  pConexao: TConexao;
  pGrupoId: Integer
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TGruposFornecedores }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where GRUPO_ID = :P1 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Descri��o ( avan�ado )',
      True,
      0,
      'where DESCRICAO like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  DESCRICAO'
    );
end;

constructor TGruposFornecedores.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'GRUPOS_FORNECEDORES');

  FSql :=
    'select ' +
    '  GRUPO_ID, ' +
    '  DESCRICAO, ' +
    '  ATIVO ' +
    'from ' +
    '  GRUPOS_FORNECEDORES ';

  SetFiltros(GetFiltros);

  AddColuna('GRUPO_ID', True);
  AddColuna('DESCRICAO');
  AddColuna('ATIVO');
end;

function TGruposFornecedores.GetRecordRotas: RecGruposFornecedores;
begin
  Result := RecGruposFornecedores.Create;

  Result.GrupoId   := GetInt('GRUPO_ID', True);
  Result.Descricao := GetString('DESCRICAO');
  Result.Ativo     := GetString('ATIVO');
end;

function AtualizarGrupo(
  pConexao: TConexao;
  pGrupoId: Integer;
  pDescricao: string;
  pAtivo: string
): RecRetornoBD;
var
  vGrupo: TGruposFornecedores;
  vNovo: Boolean;
begin
  Result.Iniciar;
  vGrupo := TGruposFornecedores.Create(pConexao);

  vNovo := pGrupoId = 0;

  if vNovo then begin
    pGrupoId := TSequencia.Create(pConexao, 'SEQ_GRUPOS_FORNECEDORES').GetProximaSequencia;
    Result.AsInt := pGrupoId;
  end;

  vGrupo.SetInt('GRUPO_ID', pGrupoId, True);
  vGrupo.SetString('DESCRICAO', pDescricao);
  vGrupo.SetString('ATIVO', pAtivo);

  try
    pConexao.IniciarTransacao;

    if vNovo then
      vGrupo.Inserir
    else
      vGrupo.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vGrupo.Free;
end;

function BuscarGrupos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecGruposFornecedores>;
var
  i: Integer;
  t: TGruposFornecedores;

  vSqlAdicional: string;
begin
  Result := nil;
  t := TGruposFornecedores.Create(pConexao);

  vSqlAdicional := '';
  if pSomenteAtivos then
    vSqlAdicional := 'and ATIVO = ''S'' ';

  if t.Pesquisar(pIndice, pFiltros, vSqlAdicional) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordRotas;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirGrupo(
  pConexao: TConexao;
  pGrupoId: Integer
): RecRetornoBD;
var
  t: TGruposFornecedores;
begin
  pConexao.SetRotina('EXCLUIR_GRUPO_FORNECEDOR');
  Result.TeveErro := False;
  t := TGruposFornecedores.Create(pConexao);

  t.SetInt('GRUPO_ID', pGrupoId, True);

  try
    pConexao.IniciarTransacao;

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
