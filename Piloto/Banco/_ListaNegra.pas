unit _ListaNegra;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecListaNegra = class
  public
    ListaId: Integer;
    Nome: string;
    Ativo: string;
  end;

  TListaNegra = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordListaNegra: RecListaNegra;
  end;

function AtualizarListaNegra(
  pConexao: TConexao;
  pListaId: Integer;
  pNome: string;
  pAtivo: string
): RecRetornoBD;

function BuscarListaNegra(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecListaNegra>;

function ExcluirListaNegra(
  pConexao: TConexao;
  pListaId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TListaNegra }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where LISTA_ID = :P1 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome (Avan�ado)',
      True,
      0,
      'where NOME like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  NOME ',
      '',
      True
    );
end;

constructor TListaNegra.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'LISTA_NEGRA');

  FSql := 
    'select ' +
    '  LISTA_ID, ' +
    '  NOME, ' +
    '  ATIVO ' +
    'from ' +
    '  LISTA_NEGRA ';

  setFiltros(getFiltros);

  AddColuna('LISTA_ID', True);
  AddColuna('NOME');
  AddColuna('ATIVO');
end;

function TListaNegra.getRecordListaNegra: RecListaNegra;
begin
  Result := RecListaNegra.Create;

  Result.ListaId    := getInt('LISTA_ID', True);
  Result.Nome       := getString('NOME');
  Result.Ativo      := getString('ATIVO');
end;

function AtualizarListaNegra(
  pConexao: TConexao;
  pListaId: Integer;
  pNome: string;
  pAtivo: string
): RecRetornoBD;
var
  t: TListaNegra;
  vNovo: Boolean;
  vSeq: TSequencia;
begin
  Result.Iniciar;
  pConexao.setRotina('ATUALIZAR_LISTA_NEGRA');

  t := TListaNegra.Create(pConexao);

  vNovo := pListaId = 0;

  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_LISTA_NEGRA');
    pListaId := vSeq.getProximaSequencia;
    Result.AsInt := pListaId;
    vSeq.Free;
  end;

  try
    pConexao.IniciarTransacao; 

    t.setInt('LISTA_ID', pListaId, True);
    t.setString('NOME', pNome);
    t.setString('ATIVO', pAtivo);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao; 
  except
    on e: Exception do begin
      pConexao.VoltarTransacao; 
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarListaNegra(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteAtivos: Boolean
): TArray<RecListaNegra>;
var
  i: Integer;
  t: TListaNegra;

  vSql: string;
begin
  Result := nil;
  t := TListaNegra.Create(pConexao);

  vSql := '';
  if pSomenteAtivos then
    vSql := ' and ATIVO = ''S'' ';

  if t.Pesquisar(pIndice, pFiltros, vSql) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordListaNegra;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirListaNegra(
  pConexao: TConexao;
  pListaId: Integer
): RecRetornoBD;
var
  t: TListaNegra;
begin
  Result.Iniciar;
  t := TListaNegra.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('LISTA_ID', pListaId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
