unit _ProdutosPromocoes;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils;

type
  RecItensPromocoes = record
    EmpresaId: Integer;
    ProdutoId: Integer;
    Nome: string;
    PrecoPromocional: Double;
    QuantidadeMinima: Double;
    DataInicialPromocao: TDateTime;
    DataFinalPromocao: TDateTime;
    CustoCompraComercial: Double;
    PrecoVendaAtual: Double;
    PercCustoVenda: Double;
    DataAlteracao: TDateTime;
    UsuarioAlteracao: Integer;
    NomeUsuarioAlteracao: string;
  end;

  TProdutosPromocoes = class(TOperacoes)
  private
    function getRecordPromocoes: RecItensPromocoes;
  public
    constructor Create(pConexao: TConexao);
  end;

function BuscarPromocoesComando(pConexao: TConexao; pFiltro: string): TArray<RecItensPromocoes>;
function AtualizarPromocoes(pConexao: TConexao; pEmpresasIds: TArray<Integer>; pItens: TArray<RecItensPromocoes>): RecRetornoBD;
function ExcluirPromocoes(pConexao: TConexao; pEmpresasIds: TArray<Integer>; pItens: TArray<Integer>): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TProdutosPromocoes }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where PRE.EMPRSA_ID = :P1 '
    );
end;

constructor TProdutosPromocoes.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PRODUTOS_PROMOCOES');

  FSql :=
    'select ' +
    '  PPR.EMPRESA_ID, ' +
    '  PRO.PRODUTO_ID, ' +
    '  PRO.NOME, ' +
    '  PPR.PRECO_PROMOCIONAL, ' +
    '  PPR.QUANTIDADE_MINIMA, ' +
    '  PPR.DATA_INICIAL_PROMOCAO, ' +
    '  PPR.DATA_FINAL_PROMOCAO, ' +
    '  BUSCAR_CUSTO_COMPRA_PRODUTO(PRE.EMPRESA_ID, PRO.PRODUTO_ID) as CUSTO_COMPRA_COMERCIAL, ' +
    '  BUSCAR_PERC_CUSTO_VENDA_PROD(PRE.EMPRESA_ID, PRO.PRODUTO_ID) as PERC_CUSTO_VENDA, ' +
    '  round(PRE.PRECO_VAREJO * CON.INDICE_ACRESCIMO, 2) as PRECO_VAREJO, ' +
    '  TRUNC(PPR.DATA_HORA_ALTERACAO) as DATA_ALTERACAO, ' +
    '  PPR.USUARIO_SESSAO_ID as USUARIO_ALTERACAO, ' +
    '  VEN.NOME as NOME_USUARIO_ALTERACAO ' +
    'from ' +
    '  PRODUTOS PRO ' +

    'inner join PRECOS_PRODUTOS PRE ' +
    'on PRO.PRODUTO_ID = PRE.PRODUTO_ID ' +

    'inner join PARAMETROS_EMPRESA PAE ' +
    'on PRE.EMPRESA_ID = PAE.EMPRESA_ID ' +

    'inner join CONDICOES_PAGAMENTO CON ' +
    'on PAE.CONDICAO_PAGTO_PAD_VENDA_ASSIS = CON.CONDICAO_ID ' +

    'left join CUSTOS_PRODUTOS CUS ' +
    'on PRO.PRODUTO_PAI_ID = CUS.PRODUTO_ID ' +
    'and PRE.EMPRESA_ID = CUS.EMPRESA_ID ' +

    'left join PRODUTOS_PROMOCOES PPR ' +
    'on PRO.PRODUTO_ID = PPR.PRODUTO_ID ' +
    'and PRE.EMPRESA_ID = PPR.EMPRESA_ID ' +

    'left join FUNCIONARIOS VEN ' +
    'on PPR.USUARIO_SESSAO_ID = VEN.FUNCIONARIO_ID ' +

    'where PRO.ATIVO = ''S'' ';

  //SetFiltros(GetFiltros);

  AddColuna('PRODUTO_ID', True);
  AddColuna('EMPRESA_ID', True);
  AddColunaSL('NOME');
  AddColuna('PRECO_PROMOCIONAL');
  AddColuna('QUANTIDADE_MINIMA');
  AddColuna('DATA_INICIAL_PROMOCAO');
  AddColuna('DATA_FINAL_PROMOCAO');
  AddColunaSL('CUSTO_COMPRA_COMERCIAL');
  AddColunaSL('PERC_CUSTO_VENDA');
  AddColunaSL('PRECO_VAREJO');
  AddColunaSL('DATA_ALTERACAO');
  AddColunaSL('USUARIO_ALTERACAO');
  AddColunaSL('NOME_USUARIO_ALTERACAO');
end;

function TProdutosPromocoes.getRecordPromocoes: RecItensPromocoes;
begin
  Result.EmpresaId            := GetInt('EMPRESA_ID', True);
  Result.ProdutoId            := GetInt('PRODUTO_ID', True);
  Result.Nome                 := GetString('NOME');
  Result.PrecoPromocional     := GetDouble('PRECO_PROMOCIONAL');
  Result.QuantidadeMinima     := GetDouble('QUANTIDADE_MINIMA');
  Result.DataInicialPromocao  := GetData('DATA_INICIAL_PROMOCAO');
  Result.DataFinalPromocao    := GetData('DATA_FINAL_PROMOCAO');
  Result.CustoCompraComercial := GetDouble('CUSTO_COMPRA_COMERCIAL');
  Result.PercCustoVenda       := GetDouble('PERC_CUSTO_VENDA');
  Result.PrecoVendaAtual      := GetDouble('PRECO_VAREJO');
  Result.DataAlteracao        := GetData('DATA_ALTERACAO');
  Result.UsuarioAlteracao     := GetInt('USUARIO_ALTERACAO');
  Result.NomeUsuarioAlteracao := GetString('NOME_USUARIO_ALTERACAO');
end;

function BuscarPromocoesComando(
  pConexao: TConexao;
  pFiltro: string
): TArray<RecItensPromocoes>;
var
  i: Integer;
  vProm: TProdutosPromocoes;
begin
  Result := nil;
  vProm := TProdutosPromocoes.Create(pConexao);

  if vProm.PesquisarComando(pFiltro) then begin
    SetLength(Result, vProm.GetQuantidadeRegistros);
    for i := 0 to vProm.GetQuantidadeRegistros - 1 do begin
      Result[i] := vProm.getRecordPromocoes;

      vProm.ProximoRegistro;
    end;
  end;

  vProm.Free;
end;

function AtualizarPromocoes(
  pConexao: TConexao;
  pEmpresasIds: TArray<Integer>;
  pItens: TArray<RecItensPromocoes>
): RecRetornoBD;
var
  i: Integer;
  j: Integer;
  vSql: TConsulta;
  vNovo: Boolean;
  vProm: TProdutosPromocoes;
begin
  Result.TeveErro := False;
  vSql := TConsulta.Create(pConexao);
  pConexao.SetRotina('ATUALIZAR_PROMOCOES_PROD');
  vProm := TProdutosPromocoes.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  count(*) ');
  vSql.Add('from ');
  vSql.Add('  PRODUTOS_PROMOCOES ');
  vSql.Add('where EMPRESA_ID = :P1');
  vSql.Add('and PRODUTO_ID = :P2');

  try
    pConexao.IniciarTransacao;

    for i := Low(pEmpresasIds) to High(pEmpresasIds) do begin
      for j := Low(pItens) to High(pItens) do begin
        vSql.Pesquisar([pEmpresasIds[i], pItens[j].ProdutoId]);
        vNovo := vSql.GetInt(0) = 0;
        vSql.Active := False;

        vProm.setInt('EMPRESA_ID', pEmpresasIds[i], True);
        vProm.setInt('PRODUTO_ID', pItens[j].ProdutoId, True);
        vProm.setDouble('PRECO_PROMOCIONAL', pItens[j].PrecoPromocional);
        vProm.setDouble('QUANTIDADE_MINIMA', pItens[j].QuantidadeMinima);
        vProm.setData('DATA_INICIAL_PROMOCAO', pItens[j].DataInicialPromocao);
        vProm.setData('DATA_FINAL_PROMOCAO', pItens[j].DataFinalPromocao);

        if vNovo then
          vProm.Inserir
        else
          vProm.Atualizar;
      end;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  vSql.Free;
  vProm.Free;
end;

function ExcluirPromocoes(pConexao: TConexao; pEmpresasIds: TArray<Integer>; pItens: TArray<Integer>): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('EXCLUIR_PROMOCOES');
  vExec := TExecucao.Create( pConexao );

  vExec.Add('delete from PRODUTOS_PROMOCOES ');
  vExec.Add('where ' + FiltroInInt('EMPRESA_ID', pEmpresasIds));
  vExec.Add('and ' + FiltroInInt('PRODUTO_ID', pItens));

  try
    pConexao.IniciarTransacao;

    vExec.Executar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
end;

end.
