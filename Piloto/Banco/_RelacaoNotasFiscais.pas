unit _RelacaoNotasFiscais;

interface

uses
  _Conexao, _OperacoesBancoDados, _BibliotecaGenerica, _RecordsEspeciais, SysUtils, _RecordsCadastros;

type
  RecRelacaoNotasFiscais = record
    notaFiscalId: Integer;
    dataCadastro: TDateTime;
    tipoNota: string;
    tipoNotaAnalitico: string;
    razoSocial: string;
    numeroNota: Integer;
    valorTotal: Double;
    ValorTotalProdutos: Double;
    ValorOutrasDespesas: Double;
    ValorDesconto: Double;
    BaseCalculoIcms: Double;
    ValorIcms: Double;
    BaseCalculoIcmsSt: Double;
    ValorIcmsSt: Double;
    ValorIpi: Double;
    cfop: string;
    status: string;
    statusAnalitico: string;
    empresa: string;
    empresaId: Integer;
    tipoMovimento: string;
    cadastroId: Integer;
  end;

function BuscarNotasFiscais(pConexao: TConexao; pComando: string): TArray<RecRelacaoNotasFiscais>;
function AplicarIndiceDeducao(pConexao: TConexao; pNotaFiscalId: Integer; pIndiceDeducaoId: Integer): RecRetornoBD;
function AlterarEmpresaEmissaoNota(pConexao: TConexao; pNotaFiscalId: Integer; pEmpresa: RecEmpresas): RecRetornoBD;
function PodeCalcularIndiceDeducao(pConexao: TConexao; pCadastroId: Integer): Boolean;

implementation

function BuscarNotasFiscais(pConexao: TConexao; pComando: string): TArray<RecRelacaoNotasFiscais>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  NFI.NOTA_FISCAL_ID, ');
  vSql.Add('  trunc(NFI.DATA_HORA_CADASTRO) as DATA_CADASTRO, ');
  vSql.Add('  NFI.TIPO_NOTA, ');
  vSql.Add('  nvl(NFI.RAZAO_SOCIAL_DESTINATARIO, NFI.NOME_FANTASIA_DESTINATARIO) as RAZAO_SOCIAL, ');
  vSql.Add('  NFI.NUMERO_NOTA, ');
  vSql.Add('  NFI.VALOR_TOTAL, ');
  vSql.Add('  NFI.VALOR_TOTAL_PRODUTOS, ');
  vSql.Add('  NFI.VALOR_OUTRAS_DESPESAS, ');
  vSql.Add('  NFI.VALOR_DESCONTO, ');
  vSql.Add('  NFI.BASE_CALCULO_ICMS, ');
  vSql.Add('  NFI.VALOR_ICMS, ');
  vSql.Add('  NFI.BASE_CALCULO_ICMS_ST, ');
  vSql.Add('  NFI.VALOR_ICMS_ST, ');
  vSql.Add('  NFI.VALOR_IPI, ');
  vSql.Add('  NFI.CFOP_ID, ');
  vSql.Add('  NFI.STATUS, ');
  vSql.Add('  NFI.EMPRESA_ID || '' - '' || EMP.RAZAO_SOCIAL as EMPRESA, ');
  vSql.Add('  NFI.TIPO_MOVIMENTO, ');
  vSql.Add('  NFI.CADASTRO_ID, ');
  vSql.Add('  NFI.EMPRESA_ID ');
  vSql.Add('from ');
  vSql.Add('  NOTAS_FISCAIS NFI ');

  vSql.Add('inner join EMPRESAS EMP ');
  vSql.Add('on NFI.EMPRESA_ID = EMP.EMPRESA_ID ');

  vSql.Add('inner join CADASTROS CAD ');
  vSql.Add('on NFI.CADASTRO_ID = CAD.CADASTRO_ID ');

  vSql.Add('left join RETIRADAS REE ');
  vSql.Add('on REE.RETIRADA_ID = NFI.RETIRADA_ID ');

  vSql.Add('left join ENTREGAS ENN ');
  vSql.Add('on ENN.ENTREGA_ID = NFI.ENTREGA_ID ');

  vSql.Add(pComando);

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].notaFiscalId        := vSql.GetInt('NOTA_FISCAL_ID');
      Result[i].dataCadastro        := vSql.GetData('DATA_CADASTRO');
      Result[i].tipoNota            := vSql.GetString('TIPO_NOTA');
      Result[i].razoSocial          := vSql.GetString('RAZAO_SOCIAL');
      Result[i].numeroNota          := vSql.GetInt('NUMERO_NOTA');
      Result[i].valorTotal          := vSql.GetDouble('VALOR_TOTAL');
      Result[i].ValorTotalProdutos  := vSql.GetDouble('VALOR_TOTAL_PRODUTOS');
      Result[i].ValorOutrasDespesas := vSql.GetDouble('VALOR_OUTRAS_DESPESAS');
      Result[i].ValorDesconto       := vSql.GetDouble('VALOR_DESCONTO');
      Result[i].BaseCalculoIcms     := vSql.GetDouble('BASE_CALCULO_ICMS');
      Result[i].ValorIcms           := vSql.GetDouble('VALOR_ICMS');
      Result[i].BaseCalculoIcmsSt   := vSql.GetDouble('BASE_CALCULO_ICMS_ST');
      Result[i].ValorIcmsSt         := vSql.GetDouble('VALOR_ICMS_ST');
      Result[i].ValorIpi            := vSql.GetDouble('VALOR_IPI');
      Result[i].cfop                := vSql.GetString('CFOP_ID');
      Result[i].status              := vSql.GetString('STATUS');
      Result[i].empresa             := vSql.GetString('EMPRESA');
      Result[i].tipoMovimento       := vSql.GetString('TIPO_MOVIMENTO');
      Result[i].cadastroId          := vSql.GetInt('CADASTRO_ID');
      Result[i].empresaId           := vSql.GetInt('EMPRESA_ID');

      Result[i].tipoNotaAnalitico :=
        _BibliotecaGenerica.Decode(Result[i].tipoNota, ['C', 'NFCe', 'N', 'NF-e', 'N�o implementada']);

      Result[i].statusAnalitico :=
        _BibliotecaGenerica.Decode(Result[i].status, ['E', 'Emitida', 'N', 'N�o emitida', 'C', 'Cancelada', 'D', 'Denegada']);

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function AplicarIndiceDeducao(pConexao: TConexao; pNotaFiscalId: Integer; pIndiceDeducaoId: Integer): RecRetornoBD;
var
  procedimento: TProcedimentoBanco;
begin
  Result.TeveErro := False;
  procedimento := TProcedimentoBanco.Create(pConexao, 'CALCULAR_DESC_NOTA');

  try
    procedimento.Params[0].AsInteger := pNotaFiscalId;
    procedimento.Params[1].AsInteger := pIndiceDeducaoId;

    procedimento.Executar;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  FreeAndNil(procedimento);
end;

function AlterarEmpresaEmissaoNota(pConexao: TConexao; pNotaFiscalId: Integer; pEmpresa: RecEmpresas): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.TeveErro := False;
  vExec := TExecucao.Create(pConexao);
  try
    pConexao.IniciarTransacao;

    vExec.SQL.Add('update NOTAS_FISCAIS set ');
    vExec.SQL.Add('  EMPRESA_ID = :P2, ');
    vExec.SQL.Add('  CEP_EMITENTE = :P3, ');
    vExec.SQL.Add('  CNPJ_EMITENTE = :P4, ');
    vExec.SQL.Add('  COMPLEMENTO_EMITENTE = :P5, ');
    vExec.SQL.Add('  ESTADO_ID_EMITENTE = :P6, ');
    vExec.SQL.Add('  INSCRICAO_ESTADUAL_EMITENTE = :P7, ');
    vExec.SQL.Add('  LOGRADOURO_EMITENTE = :P8, ');
    vExec.SQL.Add('  NOME_BAIRRO_EMITENTE = :P9, ');
    vExec.SQL.Add('  NOME_CIDADE_EMITENTE = :P10, ');
    vExec.SQL.Add('  NOME_FANTASIA_EMITENTE = :P11, ');
    vExec.SQL.Add('  NUMERO_EMITENTE = :P12, ');
    vExec.SQL.Add('  RAZAO_SOCIAL_EMITENTE = :P13, ');
    vExec.SQL.Add('  TELEFONE_EMITENTE = :P14 ');
    vExec.SQL.Add('  where NOTA_FISCAL_ID = :P1 ');

    vExec.Executar([
      pNotaFiscalId,
      pEmpresa.EmpresaId,
      pEmpresa.Cep,
      pEmpresa.Cnpj,
      pEmpresa.Complemento,
      pEmpresa.EstadoId,
      pEmpresa.InscricaoEstadual,
      pEmpresa.Logradouro,
      pEmpresa.NomeBairro,
      pEmpresa.NomeCidade,
      pEmpresa.NomeFantasia,
      pEmpresa.Numero,
      pEmpresa.RazaoSocial,
      pEmpresa.TelefonePrincipal
    ]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  vExec.Free;
end;

function PodeCalcularIndiceDeducao(pConexao: TConexao; pCadastroId: Integer): Boolean;
var
  vSql: TConsulta;
begin
  Result := False;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  nvl(CALCULAR_INDICE_DED, ''N'') as CALCULAR_INDICE_DED ');
  vSql.Add('from ');
  vSql.Add('  CLIENTES ');
  vSql.Add('where CADASTRO_ID = :P1 ');

  if vSql.Pesquisar([pCadastroId]) then begin
    Result := vSql.GetString('CALCULAR_INDICE_DED') = 'S';
  end;

  vSql.Active := False;
  FreeAndNil(vSql);
end;

end.
