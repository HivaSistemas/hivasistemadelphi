unit _Orcamentos;

interface

uses
  System.Variants, _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsOrcamentosVendas,
  System.Math, _OrcamentosItens, _OrcamentosPagamentos, _OrcamentosPagamentosCheques, System.StrUtils, _BibliotecaGenerica,
  _RecordsExpedicao, _Retiradas, _OrcamentosItensDefLotes, _RecordsFinanceiros, _OrcamentosCreditos, _Arquivos, _ContasRecBaixasPagtosPix,
  _ContasReceberBaixas, _RecordsCadastros;

{$M+}
type
  RecItensPendentesEntrega = record
    OrcamentoId: Integer;
    EmpresaId: Integer;
    NomeEmpresa: string;
    CadastroId: Integer;
    NomeCliente: string;
    ProdutoId: Integer;
    NomeProduto: string;
    Saldo: Double;
    AguardarContatoCliente: string;
    PrevisaoEntrega: TDateTime;
    TipoMovimento: string;
    TipoMovimentoAnalitico: string;
  end;

  TOrcamento = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordOrcamento: RecOrcamentos;
  end;

function BuscarCpfConsumidorFinal(pConexao: TConexao; orcamento_id: Integer): string;
function AtualizarCpfConsumidorFinal(pConexao: TConexao; orcamento_id: Integer; cpf_consumidor_final: string): RecRetornoBD;
function AtualizarCustoProdutosOrcamento(pConexao: TConexao; vItens: TArray<RecOrcamentoItens>; orcamentoId: Integer): RecRetornoBD;

function AtualizarAguardandoContatoCliente(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pAguardandoContato: string
): RecRetornoBD;

function AtualizarOrcamentoPDV(
  pConexao: TConexao;
  pNomeConsumidorFinal: string;
  pTelefoneConsumidorFinal: string;
  pEmpresaId: Integer;
  pClienteId: Integer;
  pCondicaoId: Integer;
  pVendedorId: Integer;
  pIndiceCondicaoPagamento: Double;
  pValortotalProdutos: Double;
  pValortotalProdutosPromocao: Double;
  pValorTotal: Currency;
  pValorTotalOutrasDespesas: Currency;
  pValorDinheiro: Currency;
  pValorCartaoDebito: Currency;
  pValorCartaoCredito: Currency;
  pTurnoId: Integer;
  pFuncionarioId: Integer;
  pTipoAnaliseCusto: string;
  pIndiceDescontoVendaId: Integer;
  pOrcamentoItens: TArray<RecOrcamentoItens>;
  pPagamentosCartoes: TArray<RecTitulosFinanceiros>;
  pValorTroco: Double;
  pCreditos: TArray<Integer>;
  pDefinirLocalManual: string;
  pCpfConsumidorFinal: string;
  pUsuarioCadastroId: Integer
): RecRetornoBD;

function AtualizarOrcamento(
  pConexao: TConexao;
  pOrcamentoId: Cardinal;
  pNomeConsumidorFinal: string;
  pTelefoneConsumidorFinal: string;
  pEmpresaId: Integer;
  pClienteId: Integer;
  pCondicaoId: Integer;
  pVendedorId: Integer;
  pIndiceCondicaoPagamento: Double;
  pValortotalProdutos: Double;
  pValortotalProdutosPromocao: Double;
  pValorTotal: Currency;
  pValorOutrasDespesas: Currency;
  pValorDesconto: Currency;
  pValorDinheiro: Currency;
  pValorCheque: Currency;
  pValorCartaoDebito: Currency;
  pValorCartaoCredito: Currency;
  pValorCobranca: Currency;
  pValorAcumulativo: Currency;
  pValorFinanceira: Currency;
  pValorCredito: Currency;
  pValorFrete: Currency;
  pValorFreteItens: Currency;
  pTurnoId: Integer;
  pStatus: string;
  pIndiceOverPrice: Double;
  pTipoEntrega: string;
  pObservacoesCaixa: string;
  pObservacoesTitulo: string;
  pObservacoesExpedicao: string;
  pObservacoesNFe: string;
  pDataEntrega: TDateTime;
  pHoraEntrega: TDateTime;
  pOrigemVenda: string;
  pTipoAnaliseCusto: string;
  pLogradouro: string;
  pComplemento: string;
  pNumero: string;
  pPontoReferencia: string;
  pBairroId: Integer;
  pIndiceDescontoVendaId: Integer;
  pCep: string;
  pOrcamentoItens: TArray<RecOrcamentoItens>;
  pCreditos: TArray<Integer>;
  pProfissionalId: Integer;
  pArquivos: TArray<RecArquivos>;
  pInscricaoEstadual: string;
  pReceberNaEntrega: string;
  pDefinirLocalManual: string;
  pCpfConsumidorFinal: string;
  pValorPix: Currency;
  pUsuarioCadastroId: Integer;
  FProdutosAmbiente: TArray<RecAmbientes>;
  pAguardandoCliente: string
): RecRetornoBD;

function AtualizarOrcamentosPagamentos(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pPagamentosCheques: TArray<RecTitulosFinanceiros>;
  pPagamentosCartoes: TArray<RecTitulosFinanceiros>;
  pPagamentosCobrancas: TArray<RecTitulosFinanceiros>;
  pPagamentosFinanceiras: TArray<RecTitulosFinanceiros>;
  pCreditosNoMomentoDoRecebimento: TArray<Integer>;
  pGravandoOrcamento: Boolean;
  pPagamentosPix: TArray<RecPagamentosPix>
): RecRetornoBD;

function AtualizarOrcamentosPagamentosPix(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pPagamentosPix: TArray<RecPagamentosPix>;
  pEmpresaId: Integer;
  pCadastroId: Integer;
  pTurnoId: Integer;
  pDataPagamento: TDateTime
): RecRetornoBD;

function AtualizarValoresPagamentosOrcamento(
  pConexao: TConexao;
  pOrcamentoId: Cardinal;
  pValorOutrasDespesas: Double;
  pValorDesconto: Double;
  pValorDinheiro: Double;
  pValorCheque: Double;
  pValorCartaoDebito: Double;
  pValorCartaoCredito: Double;
  pValorCobranca: Double;
  pValorAcumulativo: Double;
  pValorFinanceira: Double;
  pValorCredito: Double;
  pValorPix: Double
): RecRetornoBD;

function GerarNotaRetiraAto(pConexao: TConexao; const pRetiradasIds: TArray<Integer>; const pControlarTransacao: Boolean): RecRetornoBD;
function GerarNotaRetiraAtoAgrupado(pConexao: TConexao; const pOrcamentoId: Integer; const pControlarTransacao: Boolean): RecRetornoBD;

function BuscarOrcamentos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecOrcamentos>;

function BuscarOrcamentosComando(pConexao: TConexao; const pComando: string): TArray<RecOrcamentos>;

function ExcluirOrcamento(
  pConexao: TConexao;
  pOrcamentoId: Cardinal
): RecRetornoBD;

function AtualizarStatusOrcamento(
  pConexao: TConexao;
  pOrcamentoId: Cardinal;
  pNovoStatus: string
): RecRetornoBD;

function AlterarParceiroVenda(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pParceiroId: Integer
): RecRetornoBD;

function ReceberPedido(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pTurnoId: Integer;
  pFuncionarioId: Integer;
  pGerarCreditoTroco: Boolean;
  pValorTroco: Currency;
  pTipoNotaGerar: string
): RecRetornoBD;

function GerarCartoesReceber(
  pConexao: TConexao;
  pOrcamentoID: Integer;
  pItemID: Integer;
  pNSU: string;
  pCodigoAutorizacaoTEF: string
): RecRetornoBD;

function CancelarFechamentoPedido(
  pConexao: TConexao;
  pOrcamentoId: Integer
): RecRetornoBD;

function CancelarRecebimentoPedido(
  pConexao: TConexao;
  pOrcamentoId: Integer
): RecRetornoBD;

function AtualizarObservacaoOrcamento(pConexao: TConexao; pOrcamentoId: Integer; pObservacao: string; pTipo: string): RecRetornoBD;
function ExistemVendasPendentesTurno(pConexao: TConexao; const pTurnoId: Integer): Boolean;
function BuscarInformacoesImpressao(pConexao: TConexao; const pOrcamentoId: Integer): RecOrcamentosImpressao;
function BuscarEntregasPendentes(pConexao: TConexao; pFiltro: string): TArray<RecItensPendentesEntrega>;
function PedidoPodeSerCancelado(pConexao: TConexao; pOrcamentoId: Integer): RecRetornoBD;
function ExistemPendenciasConfirmacaoEntrega(pConexao: TConexao; pOrcamentoId: Integer; pProdutosIds: TArray<Integer>): Boolean;
function ExistemEntregasRealizadas(pConexao: TConexao; pOrcamentoId: Integer; pProdutosIds: TArray<Integer>): Boolean;
function PedidoFazParteAcumuladoPendente(pConexao: TConexao; pOrcamentoId: Integer): Boolean;
function PedidoTemFinanceiroAbertoReceberNaEntrega(pConexao: TConexao; pOrcamentoId: Integer): Boolean;
function AtualizarIndiceDescontoVenda(pConexao: TConexao; pOrcamentoId: Integer; pIndiceDescontoVendaId: Integer): RecRetornoBD;
function AtualizarValorAdiantado(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pValorAdiantado: Double;
  pEmTransacao: Boolean
): RecRetornoBD;

function LiberarPedidoRecebimentoNaEntrega(pConexao: TConexao; pOrcamentoId: Integer): RecRetornoBD;

function AtualizarEnderecoEntrega(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pLogradouro: string;
  pComplemento: string;
  pNumero: string;
  pPontoReferencia: string;
  pBairroId: Integer;
  pCep: string;
  pInscricaoEstadual: string
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

uses
  _OrcamentosItensDefLocais;

{ TOrcamento }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 10);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ORC.ORCAMENTO_ID = :P1 and ORC.ORC_POR_AMBIENTE = ''N'' '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ORC.STATUS in(''VR'',''VB'') ' +
      'and ORC.EMPRESA_ID = :P1 ' +
      'order by ' +
      '  ORC.ORCAMENTO_ID desc '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ORC.STATUS = ''GN'' ' +
      'order by ORC.ORCAMENTO_ID desc '
    );

  Result[3] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ORC.ORCAMENTO_ID = :P1 ' +
      'and ORC.STATUS in(''RE'',''GN'',''EM'') '
    );

  Result[4] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ORC.ORCAMENTO_ID = :P1 ' +
      'and ORC.ORCAMENTO_ID in(select distinct ORCAMENTO_ID from ENTREGAS_ITENS_SEM_PREVISAO where SALDO > 0) '
    );

  Result[5] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ORC.ORCAMENTO_ID = :P1 ' +
      'and ORC.STATUS in(''RE'', ''VE'') '
    );

  Result[6] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ORC.CLIENTE_ID = :P1 ' +
      'and ORC.STATUS = ''RE'' ' +
      'and COD.ACUMULATIVO = ''S'' ' +
      'and ORC.VALOR_TOTAL - ORC.VALOR_DEV_ACUMULADO_ABERTO > 0 ' +
      'and ORC.ACUMULADO_ID is null ',
      'order by ' +
      '  ORC.INSCRICAO_ESTADUAL, ' +
      '  ORC.ORCAMENTO_ID '
    );

  Result[7] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ORC.ORCAMENTO_ID = :P1 ' +
      'and ORC.STATUS = ''RE'' ' +
      'and COD.ACUMULATIVO = ''S'' ' +
      'and ORC.VALOR_TOTAL - ORC.VALOR_DEV_ACUMULADO_ABERTO - ORC.VALOR_ADIANTADO_ACUMULADO_ABER > 0 ' +
      'and ORC.ACUMULADO_ID is null '
    );

  Result[8] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ORC.STATUS = ''VE'' ' +
      'and ORC.EMPRESA_ID = :P1 ' +
      'order by ' +
      '  ORC.ORCAMENTO_ID desc '
    );

  Result[9] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ORC.ORCAMENTO_ID = :P1 and ORC.ORC_POR_AMBIENTE = ''S'' '
    );
end;

constructor TOrcamento.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ORCAMENTOS');

  FSql :=
    'select ' +
    '  ORC.ORCAMENTO_ID, ' +
    '  ORC.NOME_CONSUMIDOR_FINAL, ' +
    '  ORC.CPF_CONSUMIDOR_FINAL, ' +
    '  ORC.EMPRESA_ID, ' +
    '  ORC.CLIENTE_ID, ' +
    '  ORC.CONDICAO_ID, ' +
    '  ORC.VENDEDOR_ID, ' +
    '  ORC.INDICE_CONDICAO_PAGAMENTO, ' +
    '  ORC.VALOR_TOTAL_PRODUTOS, ' +
    '  ORC.VALOR_TOTAL_PRODUTOS_PROMOCAO, ' +
    '  ORC.VALOR_COBRANCA, ' +
    '  ORC.VALOR_CREDITO, ' +
    '  ORC.VALOR_TOTAL, ' +
    '  ORC.VALOR_PIX, ' +
    '  ORC.VALOR_OUTRAS_DESPESAS, ' +
    '  ORC.VALOR_DESCONTO, ' +
    '  ORC.VALOR_DINHEIRO, ' +
    '  ORC.VALOR_CHEQUE, ' +
    '  ORC.VALOR_CARTAO_DEBITO, ' +
    '  ORC.VALOR_CARTAO_CREDITO, ' +
    '  ORC.VALOR_ACUMULATIVO, ' +
    '  ORC.VALOR_FINANCEIRA, ' +
    '  ORC.VALOR_TROCO, ' +
    '  ORC.VALOR_DEV_ACUMULADO_ABERTO, ' +
    '  ORC.STATUS, ' +
    '  ORC.TURNO_ID, ' +
    '  ORC.ACUMULADO_ID, ' +
    '  ORC.DATA_CADASTRO, ' +
    '  ORC.INDICE_OVER_PRICE, ' +
    '  ORC.DATA_HORA_RECEBIMENTO, ' +
    '  case when ORC.CLIENTE_ID = PAR.CADASTRO_CONSUMIDOR_FINAL_ID then ORC.NOME_CONSUMIDOR_FINAL else CAD.RAZAO_SOCIAL end as NOME_CLIENTE, ' +
    '  nvl(FUN.APELIDO, FUN.NOME) as NOME_VENDEDOR, ' +
    '  COD.NOME as NOME_CONDICAO_PAGTO, ' +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA, ' +
    '  ORC.USUARIO_CADASTRO_ID, ' +
    '  nvl(FCA.APELIDO, FCA.NOME) as NOME_USUARIO_CADASTRO, ' +
    '  TUR.FUNCIONARIO_ID as USUARIO_RECEBIMENTO_ID, ' +
    '  nvl(FTU.APELIDO, FTU.NOME) as NOME_USUARIO_RECEBIMENTO, ' +
    '  CAD.TELEFONE_PRINCIPAL, ' +
    '  CAD.TELEFONE_CELULAR, ' +
    '  ORC.TELEFONE_CONSUMIDOR_FINAL, ' +
    '  ORC.TIPO_ENTREGA, ' +
    '  ORC.DATA_ENTREGA, ' +
    '  ORC.HORA_ENTREGA, ' +
    '  ORC.OBSERVACOES_CAIXA, ' +
    '  ORC.OBSERVACOES_TITULOS, ' +
    '  ORC.OBSERVACOES_EXPEDICAO, ' +
    '  ORC.OBSERVACOES_NFE, ' +
    '  ORC.VALOR_FRETE, ' +
    '  ORC.VALOR_FRETE_ITENS, ' +
    '  ORC.ORIGEM_VENDA, ' +
    '  ORC.TIPO_ANALISE_CUSTO, ' +
    '  ORC.RECEBER_NA_ENTREGA, ' +
    '  ORC.LOGRADOURO, ' +
    '  ORC.COMPLEMENTO, ' +
    '  ORC.NUMERO, ' +
    '  ORC.PONTO_REFERENCIA, ' +
    '  ORC.BAIRRO_ID, ' +
    '  ORC.CEP, ' +
    '  CLI.QTDE_DIAS_VENCIMENTO_ACUMULADO, ' +
    '  nvl(ACU.INDICE_PEDIDOS, 1) as INDICE_PEDIDOS, ' +
    '  COD.ACUMULATIVO as ACUMULADO, ' +
    '  ORC.INDICE_DESCONTO_VENDA_ID, ' +
    '  IDV.DESCRICAO as NOME_IND_DESCONTO_VENDA, ' +
    '  ORC.PROFISSIONAL_ID, ' +
    '  ORC.VALOR_ADIANTADO_ACUMULADO_ABER, ' +
    '  COD.PRAZO_MEDIO, ' +
    '  nvl(ORC.INSCRICAO_ESTADUAL, CAD.INSCRICAO_ESTADUAL) as INSCRICAO_ESTADUAL, ' +
    '  TIP.TIPO_ACOMPANHAMENTO_ID, ' +
    '  PAE.INF_COMPL_COMP_PAGAMENTOS, ' +
    '  BAI.NOME as NOME_BAIRRO_CLIENTE, ' +
    '  CID.NOME as NOME_CIDADE_CLIENTE, ' +
    '  CID.ESTADO_ID as UF_CLIENTE, ' +
    '  CAD.LOGRADOURO as ENDERECO_CLIENTE, ' +
    '  CAD.COMPLEMENTO as COMPLEMENTO_CLIENTE, ' +
    '  CAD.NUMERO as NUMERO_CLIENTE, ' +
    '  CAD.CEP as CEP_CLIENTE, ' +
    '  CAD.CPF_CNPJ as CPF_CNPJ_CLIENTE, ' +
    '  CAD.INSCRICAO_ESTADUAL as INSCRICAO_ESTADUAL_CLIENTE, ' +
    '  ORC.AGUARDANDO_CLIENTE, ' +
    '  ORC.PEDIDO_LIBERADO, ' +
    '  ORC.PERCENTUAL_LIBERADO, ' +
    '  ORC.ORC_POR_AMBIENTE ' +
    'from ' +
    '  ORCAMENTOS ORC ' +

    'inner join CLIENTES CLI ' +
    'on ORC.CLIENTE_ID = CLI.CADASTRO_ID ' +

    'inner join CADASTROS CAD ' +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID ' +

    'inner join BAIRROS BAI ' +
    'on CAD.BAIRRO_ID = BAI.BAIRRO_ID ' +

    'inner join CIDADES CID ' +
    'on BAI.CIDADE_ID = CID.CIDADE_ID ' +

    'left join FUNCIONARIOS FUN ' +
    'on ORC.VENDEDOR_ID = FUN.FUNCIONARIO_ID ' +

    'inner join FUNCIONARIOS FCA ' +
    'on ORC.USUARIO_CADASTRO_ID = FCA.FUNCIONARIO_ID ' +

    'inner join CONDICOES_PAGAMENTO COD ' +
    'on ORC.CONDICAO_ID = COD.CONDICAO_ID ' +

    'inner join EMPRESAS EMP ' +
    'on ORC.EMPRESA_ID = EMP.EMPRESA_ID ' +

    'inner join PARAMETROS_EMPRESA PAE ' +
    'on ORC.EMPRESA_ID = PAE.EMPRESA_ID ' +

    'left join TURNOS_CAIXA TUR ' +
    'on ORC.TURNO_ID = TUR.TURNO_ID ' +

    'left join FUNCIONARIOS FTU ' +
    'on TUR.FUNCIONARIO_ID = FTU.FUNCIONARIO_ID ' +

    'left join ACUMULADOS ACU ' +
    'on ORC.ACUMULADO_ID = ACU.ACUMULADO_ID ' +

    'left join INDICES_DESCONTOS_VENDA IDV ' +
    'on ORC.INDICE_DESCONTO_VENDA_ID = IDV.INDICE_ID ' +

    'left join TIPO_ACOMPANHAMENTO_ORCAMENTO TIP ' +
    'on TIP.TIPO_ACOMPANHAMENTO_ID = ORC.TIPO_ACOMPANHAMENTO_ID ' +

    'cross join PARAMETROS PAR';

  setFiltros(getFiltros);

  AddColuna('ORCAMENTO_ID', True);
  AddColuna('NOME_CONSUMIDOR_FINAL');
  AddColuna('CPF_CONSUMIDOR_FINAL');
  AddColuna('EMPRESA_ID');
  AddColuna('CLIENTE_ID');
  AddColuna('CONDICAO_ID');
  AddColuna('VENDEDOR_ID');
  AddColuna('INDICE_CONDICAO_PAGAMENTO');
  AddColuna('VALOR_TOTAL_PRODUTOS');
  AddColuna('VALOR_TOTAL_PRODUTOS_PROMOCAO');
  AddColuna('VALOR_COBRANCA');
  AddColuna('VALOR_CREDITO');
  AddColuna('VALOR_TOTAL');
  AddColuna('VALOR_PIX');
  AddColuna('VALOR_OUTRAS_DESPESAS');
  AddColuna('VALOR_DESCONTO');
  AddColuna('VALOR_DINHEIRO');
  AddColuna('VALOR_CHEQUE');
  AddColuna('VALOR_CARTAO_DEBITO');
  AddColuna('VALOR_CARTAO_CREDITO');
  AddColuna('VALOR_ACUMULATIVO');
  AddColuna('VALOR_FINANCEIRA');
  AddColunaSL('VALOR_TROCO');
  AddColunaSL('VALOR_DEV_ACUMULADO_ABERTO');
  AddColuna('STATUS');
  AddColuna('TURNO_ID');
  AddColunaSL('ACUMULADO_ID');
  AddColuna('INDICE_DESCONTO_VENDA_ID');
  AddColunaSL('NOME_IND_DESCONTO_VENDA');
  AddColuna('TELEFONE_CONSUMIDOR_FINAL');
  AddColuna('TIPO_ENTREGA');
  AddColuna('DATA_ENTREGA');
  AddColuna('HORA_ENTREGA');
  AddColuna('OBSERVACOES_CAIXA');
  AddColuna('OBSERVACOES_TITULOS');
  AddColuna('OBSERVACOES_EXPEDICAO');
  AddColuna('OBSERVACOES_NFE');
  AddColuna('VALOR_FRETE');
  AddColuna('VALOR_FRETE_ITENS');
  AddColuna('ORIGEM_VENDA');
  AddColuna('TIPO_ANALISE_CUSTO');
  AddColuna('RECEBER_NA_ENTREGA');
  AddColuna('LOGRADOURO');
  AddColuna('COMPLEMENTO');
  AddColuna('NUMERO');
  AddColuna('PONTO_REFERENCIA');
  AddColuna('BAIRRO_ID');
  AddColuna('CEP');
  AddColuna('INSCRICAO_ESTADUAL');
  AddColuna('PROFISSIONAL_ID');
  AddColunaSL('VALOR_ADIANTADO_ACUMULADO_ABER');
  AddColunaSL('PRAZO_MEDIO');
  AddColunaSL('DATA_CADASTRO');
  AddColuna('INDICE_OVER_PRICE');
  AddColunaSL('DATA_HORA_RECEBIMENTO');
  AddColunaSL('NOME_CLIENTE');
  AddColunaSL('NOME_VENDEDOR');
  AddColunaSL('NOME_CONDICAO_PAGTO');
  AddColunaSL('NOME_EMPRESA');
  AddColunaSL('USUARIO_CADASTRO_ID');
  AddColunaSL('NOME_USUARIO_CADASTRO');
  AddColunaSL('USUARIO_RECEBIMENTO_ID');
  AddColunaSL('NOME_USUARIO_RECEBIMENTO');
  AddColunaSL('TELEFONE_PRINCIPAL');
  AddColunaSL('TELEFONE_CELULAR');
  AddColunaSL('QTDE_DIAS_VENCIMENTO_ACUMULADO');
  AddColunaSL('INDICE_PEDIDOS');
  AddColunaSL('ACUMULADO');
  AddColunaSL('TIPO_ACOMPANHAMENTO_ID');
  AddColunaSL('INF_COMPL_COMP_PAGAMENTOS');
  AddColunaSL('NOME_CIDADE_CLIENTE');
  AddColunaSL('NOME_BAIRRO_CLIENTE');
  AddColunaSL('UF_CLIENTE');
  AddColunaSL('ENDERECO_CLIENTE');
  AddColunaSL('COMPLEMENTO_CLIENTE');
  AddColunaSL('NUMERO_CLIENTE');
  AddColunaSL('CEP_CLIENTE');
  AddColunaSL('CPF_CNPJ_CLIENTE');
  AddColunaSL('INSCRICAO_ESTADUAL_CLIENTE');
  AddColunaSL('ORC_POR_AMBIENTE');
  AddColuna('AGUARDANDO_CLIENTE');
  AddColunaSL('PEDIDO_LIBERADO');
  AddColunaSL('PERCENTUAL_LIBERADO');
end;

function TOrcamento.getRecordOrcamento: RecOrcamentos;
begin
  Result.orcamento_id             := getInt('ORCAMENTO_ID', True);
  Result.nome_consumidor_final    := getString('NOME_CONSUMIDOR_FINAL');
  Result.cpf_consumidor_final     := getString('CPF_CONSUMIDOR_FINAL');
  Result.empresa_id               := getInt('EMPRESA_ID');
  Result.cliente_id               := getInt('CLIENTE_ID');
  Result.condicao_id              := getInt('CONDICAO_ID');
  Result.vendedor_id              := getInt('VENDEDOR_ID');
  Result.tipo_acompanhamento_id   := getInt('TIPO_ACOMPANHAMENTO_ID');
  Result.IndiceCondicaoPagamento  := getDouble('INDICE_CONDICAO_PAGAMENTO');
  Result.valor_total_produtos     := getDouble('VALOR_TOTAL_PRODUTOS');
  Result.ValorTotalProdutosPromocao := getDouble('VALOR_TOTAL_PRODUTOS_PROMOCAO');
  Result.valor_cobranca           := getDouble('VALOR_COBRANCA');
  Result.valor_credito            := getDouble('VALOR_CREDITO');
  Result.valor_total              := getDouble('VALOR_TOTAL');
  Result.valor_outras_despesas    := getDouble('VALOR_OUTRAS_DESPESAS');
  Result.valor_desconto           := getDouble('VALOR_DESCONTO');
  Result.valor_pix                 := getDouble('VALOR_PIX');
  Result.valor_dinheiro           := getDouble('VALOR_DINHEIRO');
  Result.valor_cheque             := getDouble('VALOR_CHEQUE');
  Result.ValorCartaoDebito        := getDouble('VALOR_CARTAO_DEBITO');
  Result.ValorCartaoCredito       := getDouble('VALOR_CARTAO_CREDITO');
  Result.ValorAcumulativo         := getDouble('VALOR_ACUMULATIVO');
  Result.ValorFinanceira          := getDouble('VALOR_FINANCEIRA');
  Result.valorTroco               := getDouble('VALOR_TROCO');
  Result.ValorDevAcumuladoAberto  := getDouble('VALOR_DEV_ACUMULADO_ABERTO');
  Result.status                   := getString('STATUS');
  Result.TelefoneConsumidorFinal  := getString('TELEFONE_CONSUMIDOR_FINAL');
  Result.TipoEntrega              := getString('TIPO_ENTREGA');
  Result.DataEntrega              := getData('DATA_ENTREGA');
  Result.HoraEntrega              := getData('HORA_ENTREGA');
  Result.ObservacoesCaixa         := getString('OBSERVACOES_CAIXA');
  Result.ObservacoesTitulo        := getString('OBSERVACOES_TITULOS');
  Result.ObservacoesExpedicao     := getString('OBSERVACOES_EXPEDICAO');
  Result.ObservacoesNFe           := getString('OBSERVACOES_NFE');
  Result.ValorFrete               := getDouble('VALOR_FRETE');
  Result.ValorFreteItens          := getDouble('VALOR_FRETE_ITENS');
  Result.OrigemVenda              := getString('ORIGEM_VENDA');
  Result.InfComplCompPagamentos   := getString('INF_COMPL_COMP_PAGAMENTOS');
  Result.NomeCidadeCliente        := getString('NOME_CIDADE_CLIENTE');
  Result.NomeBairroCliente        := getString('NOME_BAIRRO_CLIENTE');
  Result.UfCliente                := getString('UF_CLIENTE');
  Result.LogradouroCliente        := getString('ENDERECO_CLIENTE');
  Result.NumeroCliente            := getString('NUMERO_CLIENTE');
  Result.ComplementoCliente       := getString('COMPLEMENTO_CLIENTE');
  Result.CepCliente               := getString('CEP_CLIENTE');
  Result.cpfCnpjCliente           := getString('CPF_CNPJ_CLIENTE');
  Result.inscricaoEstadualCliente := getString('INSCRICAO_ESTADUAL_CLIENTE');
  Result.OrcPorAmbiente           := getString('ORC_POR_AMBIENTE');
  Result.AguardandoCliente        := getString('AGUARDANDO_CLIENTE');
  Result.PedidoLiberado           := getString('PEDIDO_LIBERADO');
  Result.PercentualLiberado       := getDouble('PERCENTUAL_LIBERADO');

  Result.status_analitico :=
    Decode(
      Result.status,[
        'OE', 'Em aberto',
        'OB', 'Bloqueado',
        'VB', 'Bloqueado',
        'VR', 'Aguardando recebimento',
        'VE', 'Ag.recebimento entrega',
        'RE', 'Recebido',
        'GN', 'Aguard. envio nota fiscal',
        'EM', 'Recebido e emitida nota',
        'CA', 'Cancelado']
    );

  Result.TipoEntregaAnalitico :=
    Decode(
      Result.TipoEntrega,
      [
        'RA', 'Retirar no ato',
        'RE', 'Retirar',
        'EN', 'Entregar',
        'SP', 'Sem previs�o',
        'SE', 'Sem previs�o - Entregar'
      ]
    );

  Result.OrigemVendaAnalitico :=
    Decode(
      Result.OrigemVenda,
      [
        'N', 'Normal',
        'P', 'PDV',
        'W', 'Web',
        'M', 'Mobile'
      ]
    );

  Result.turno_id                      := getInt('TURNO_ID');
  Result.AcumuladoId                   := getInt('ACUMULADO_ID');
  Result.IndiceDescontoVendaId         := getInt('INDICE_DESCONTO_VENDA_ID');
  Result.NomeIndDescontoVenda          := getString('NOME_IND_DESCONTO_VENDA');
  Result.data_cadastro                 := getData('DATA_CADASTRO');
  Result.indice_over_price             := getDouble('INDICE_OVER_PRICE');
  Result.nome_vendedor                 := getString('NOME_VENDEDOR');
  Result.nome_cliente                  := getString('NOME_CLIENTE');
  Result.nome_condicao_pagto           := getString('NOME_CONDICAO_PAGTO');
  Result.nome_empresa                  := getString('NOME_EMPRESA');
  Result.usuario_cadastro_id           := getInt('USUARIO_CADASTRO_ID');
  Result.nome_usuario_cadastro         := getString('NOME_USUARIO_CADASTRO');
  Result.data_hora_recebimento         := getData('DATA_HORA_RECEBIMENTO');
  Result.usuario_recebimento_id        := getInt('USUARIO_RECEBIMENTO_ID');
  Result.nome_usuario_recebimento      := getString('NOME_USUARIO_RECEBIMENTO');
  Result.telefone_principal            := getString('TELEFONE_PRINCIPAL');
  Result.telefone_celular              := getString('TELEFONE_CELULAR');
  Result.tipoAnaliseCusto              := getString('TIPO_ANALISE_CUSTO');
  Result.ReceberNaEntrega              := getString('RECEBER_NA_ENTREGA');
  Result.Logradouro                    := getString('LOGRADOURO');
  Result.Complemento                   := getString('COMPLEMENTO');
  Result.Numero                        := getString('NUMERO');
  Result.PontoReferencia               := getString('PONTO_REFERENCIA');
  Result.BairroId                      := getInt('BAIRRO_ID');
  Result.Cep                           := getString('CEP');
  Result.InscricaoEstadual             := getString('INSCRICAO_ESTADUAL');
  Result.QtdeDiasVencimentoAcumulado   := getInt('QTDE_DIAS_VENCIMENTO_ACUMULADO');
  Result.IndicePedidos                 := getDouble('INDICE_PEDIDOS');
  Result.Acumulado                     := getString('ACUMULADO');
  Result.ProfissionalId                := getInt('PROFISSIONAL_ID');
  Result.ValorAdiantadoAcumulatoAber   := getDouble('VALOR_ADIANTADO_ACUMULADO_ABER');
  Result.PrazoMedioCondicaoPagto       := getDouble('PRAZO_MEDIO');

  Result.SaldoDevedorAcumulado :=
    Result.valor_total -
    Result.ValorDevAcumuladoAberto -
    Result.ValorAdiantadoAcumulatoAber;
end;

function AtualizarOrcamentoPDV(
  pConexao: TConexao;
  pNomeConsumidorFinal: string;
  pTelefoneConsumidorFinal: string;
  pEmpresaId: Integer;
  pClienteId: Integer;
  pCondicaoId: Integer;
  pVendedorId: Integer;
  pIndiceCondicaoPagamento: Double;
  pValortotalProdutos: Double;
  pValortotalProdutosPromocao: Double;
  pValorTotal: Currency;
  pValorTotalOutrasDespesas: Currency;
  pValorDinheiro: Currency;
  pValorCartaoDebito: Currency;
  pValorCartaoCredito: Currency;
  pTurnoId: Integer;
  pFuncionarioId: Integer;
  pTipoAnaliseCusto: string;
  pIndiceDescontoVendaId: Integer;
  pOrcamentoItens: TArray<RecOrcamentoItens>;
  pPagamentosCartoes: TArray<RecTitulosFinanceiros>;
  pValorTroco: Double;
  pCreditos: TArray<Integer>;
  pDefinirLocalManual: string;
  pCpfConsumidorFinal: string;
  pUsuarioCadastroId: Integer
): RecRetornoBD;
var
  vOrcamentoId: Integer;
  vRetiradasIds: TArray<Integer>;
begin
  try
    pConexao.SetRotina('ATUA_ORCAMENTO_PDV');
    pConexao.StartTransaction;

    Result :=
      AtualizarOrcamento(
        pConexao,
        0,
        pNomeConsumidorFinal,
        pTelefoneConsumidorFinal,
        pEmpresaId,
        pClienteId,
        pCondicaoId,
        pVendedorId,
        pIndiceCondicaoPagamento,
        pValortotalProdutos,
        pValortotalProdutosPromocao,
        pValorTotal,
        pValorTotalOutrasDespesas,
        0,
        pValorDinheiro,
        0,
        pValorCartaoDebito,
        pValorCartaoCredito,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        'VR',
        1,
        'RA',
        '',
        '',
        '',
        '',
        0,
        0,
        'P',
        pTipoAnaliseCusto,
        '',
        '',
        '',
        '',
        0,
        pIndiceDescontoVendaId,
        '',
        pOrcamentoItens,
        pCreditos,
        0,
        nil,
        '',
        'N',
        pDefinirLocalManual,
        pCpfConsumidorFinal,
        0,
        pUsuarioCadastroId,
        nil,
        'N'
      );

    if Result.TeveErro then
      raise Exception.Create(Result.MensagemErro);

    vOrcamentoId := Result.AsInt;

    Result :=
      ReceberPedido(
        pConexao,
        vOrcamentoId,
        pTurnoId,
        pFuncionarioId,
        False,
        pValorTroco,
        'NE'
      );

    if Result.TeveErro then
      raise Exception.Create(Result.MensagemErro);

    Result :=
      AtualizarOrcamentosPagamentos(
        pConexao,
        vOrcamentoId,
        nil,
        pPagamentosCartoes,
        nil,
        nil,
        nil,
        False,
        nil
      );

    if Result.TeveErro then
      raise Exception.Create(Result.MensagemErro);

    if pTipoAnaliseCusto = 'C' then begin
      vRetiradasIds := _Retiradas.BuscarRetiradaIdAto(pConexao, vOrcamentoId);

      Result :=
        _Orcamentos.GerarNotaRetiraAto(
          pConexao,
          vRetiradasIds,
          False
        );

      if Result.TeveErro then
        raise Exception.Create(Result.MensagemErro);
    end;

    Result.AsInt := vOrcamentoId;
    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
end;

function AtualizarOrcamento(
  pConexao: TConexao;
  pOrcamentoId: Cardinal;
  pNomeConsumidorFinal: string;
  pTelefoneConsumidorFinal: string;
  pEmpresaId: Integer;
  pClienteId: Integer;
  pCondicaoId: Integer;
  pVendedorId: Integer;
  pIndiceCondicaoPagamento: Double;
  pValortotalProdutos: Double;
  pValortotalProdutosPromocao: Double;
  pValorTotal: Currency;
  pValorOutrasDespesas: Currency;
  pValorDesconto: Currency;
  pValorDinheiro: Currency;
  pValorCheque: Currency;
  pValorCartaoDebito: Currency;
  pValorCartaoCredito: Currency;
  pValorCobranca: Currency;
  pValorAcumulativo: Currency;
  pValorFinanceira: Currency;
  pValorCredito: Currency;
  pValorFrete: Currency;
  pValorFreteItens: Currency;
  pTurnoId: Integer;
  pStatus: string;
  pIndiceOverPrice: Double;
  pTipoEntrega: string;
  pObservacoesCaixa: string;
  pObservacoesTitulo: string;
  pObservacoesExpedicao: string;
  pObservacoesNFe: string;
  pDataEntrega: TDateTime;
  pHoraEntrega: TDateTime;
  pOrigemVenda: string;
  pTipoAnaliseCusto: string;
  pLogradouro: string;
  pComplemento: string;
  pNumero: string;
  pPontoReferencia: string;
  pBairroId: Integer;
  pIndiceDescontoVendaId: Integer;
  pCep: string;
  pOrcamentoItens: TArray<RecOrcamentoItens>;
  pCreditos: TArray<Integer>;
  pProfissionalId: Integer;
  pArquivos: TArray<RecArquivos>;
  pInscricaoEstadual: string;
  pReceberNaEntrega: string;
  pDefinirLocalManual: string;
  pCpfConsumidorFinal: string;
  pValorPix: Currency;
  pUsuarioCadastroId: Integer;
  FProdutosAmbiente: TArray<RecAmbientes>;
  pAguardandoCliente: string
): RecRetornoBD;
var
  t: TOrcamento;
  vNovo: Boolean;

  vRetBanco: RecRetornoBD;
  
  i: Integer;
  j: Integer;
  item_id: Integer;
  vExec: TExecucao;
  vUpdate: TExecucao;
  vSeq: TSequencia;
  vConsultaLote: TConsulta;
  vItens: TOrcamentoItens;
  vCreditos: TOrcamentosCreditos;
  vItensDefLotes: TOrcamentosItensDefLotes;
  vItensDefLocais: TOrcamentosItensDefLocais;

  vProc: TProcedimentoBanco;
begin
  pConexao.SetRotina('ATUALIZAR_ORCAMENTO');

  Result.Iniciar;

  t := TOrcamento.Create(pConexao);
  vExec := TExecucao.Create(pConexao);
  vUpdate := TExecucao.Create(pConexao);
  vConsultaLote := TConsulta.Create(pConexao);
  vItens := TOrcamentoItens.Create(pConexao);
  vItensDefLotes := TOrcamentosItensDefLotes.Create(pConexao);
  vItensDefLocais := TOrcamentosItensDefLocais.Create(pConexao);
  vCreditos := TOrcamentosCreditos.Create(pConexao);

  vProc := TProcedimentoBanco.Create(pConexao, 'FECHAR_PEDIDO');

  vConsultaLote.SQL.Add(
    'select ' +
    '  count(*) as CONTADOR ' +
    'from ' +
    '  ORCAMENTOS_ITENS_DEF_LOTES ' +
    'where ORCAMENTO_ID = :P1 ' +
    'and LOTE = :P2 ' +
    'and ITEM_ID = :P3'
  );

  vUpdate.Add(
    'update ORCAMENTOS_ITENS_DEF_LOTES set ' +
    '  QUANTIDADE_ENTREGAR = QUANTIDADE_ENTREGAR + :P1, ' +
    '  QUANTIDADE_RETIRAR = QUANTIDADE_RETIRAR + :P2, ' +
    '  QUANTIDADE_RETIRAR_ATO = QUANTIDADE_RETIRAR_ATO + :P3 ' +
    'where ORCAMENTO_ID = :P4 ' +
    'and ITEM_ID = :P5 ' +
    'and LOTE = :P6 '
  );

  item_id := 1;
  vNovo := pOrcamentoId = 0;
  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_ORCAMENTO_ID');
    pOrcamentoId := vSeq.getProximaSequencia;
    vSeq.Free;
  end;

  try
    result.AsInt := pOrcamentoId;

    t.setInt('ORCAMENTO_ID', pOrcamentoId, True);
    t.setString('NOME_CONSUMIDOR_FINAL', pNomeConsumidorFinal);
    t.setString('CPF_CONSUMIDOR_FINAL', pCpfConsumidorFinal);
    t.setString('TELEFONE_CONSUMIDOR_FINAL', pTelefoneConsumidorFinal);
    t.setInt('EMPRESA_ID', pEmpresaId);
    t.setInt('CLIENTE_ID', pClienteId);
    t.setInt('CONDICAO_ID', pCondicaoId);
    t.setIntN('VENDEDOR_ID', pVendedorId);
    t.setDouble('INDICE_CONDICAO_PAGAMENTO', pIndiceCondicaoPagamento);
    t.setDouble('VALOR_TOTAL_PRODUTOS', pValortotalProdutos);
    t.setDouble('VALOR_TOTAL_PRODUTOS_PROMOCAO', pValortotalProdutosPromocao);
    t.setDouble('VALOR_TOTAL', pValorTotal);
    t.setDouble('VALOR_OUTRAS_DESPESAS', pValorOutrasDespesas);
    t.setDouble('VALOR_DESCONTO', pValorDesconto);
    t.setDouble('VALOR_DINHEIRO', pValorDinheiro);
    t.setDouble('VALOR_CHEQUE', pValorCheque);
    t.setDouble('VALOR_CARTAO_DEBITO', pValorCartaoDebito);
    t.setDouble('VALOR_CARTAO_CREDITO', pValorCartaoCredito);
    t.setDouble('VALOR_COBRANCA', pValorCobranca);
    t.setDouble('VALOR_PIX', pValorPix);
    t.setDouble('VALOR_ACUMULATIVO', pValorAcumulativo);
    t.setDouble('VALOR_FINANCEIRA', pValorFinanceira);
    t.setDouble('VALOR_CREDITO', pValorCredito);
    t.setDouble('VALOR_FRETE', pValorFrete);
    t.setDouble('VALOR_FRETE_ITENS', pValorFreteItens);
    t.setIntN('TURNO_ID', pTurnoId);
    t.setString('STATUS', pStatus);
    t.setDouble('INDICE_OVER_PRICE', pIndiceOverPrice);
    t.setString('TIPO_ENTREGA', pTipoEntrega);
    t.setString('OBSERVACOES_CAIXA', pObservacoesCaixa);
    t.setString('OBSERVACOES_TITULOS', pObservacoesTitulo);
    t.setString('OBSERVACOES_EXPEDICAO', pObservacoesExpedicao);
    t.setString('OBSERVACOES_NFE', pObservacoesNFe);
    t.setDataN('DATA_ENTREGA', pDataEntrega);
    t.setDataN('HORA_ENTREGA', pHoraEntrega);
    t.setString('ORIGEM_VENDA', pOrigemVenda);
    t.setString('TIPO_ANALISE_CUSTO', pTipoAnaliseCusto);
    t.setString('LOGRADOURO', pLogradouro);
    t.setString('COMPLEMENTO', pComplemento);
    t.setString('NUMERO', pNumero);
    t.setString('PONTO_REFERENCIA', pPontoReferencia);
    t.setIntN('BAIRRO_ID', pBairroId);
    t.setIntN('INDICE_DESCONTO_VENDA_ID', pIndiceDescontoVendaId);
    t.setString('CEP', pCep);
    t.setIntN('PROFISSIONAL_ID', pProfissionalId);
    t.setString('INSCRICAO_ESTADUAL', pInscricaoEstadual);
    t.setString('RECEBER_NA_ENTREGA', pReceberNaEntrega);
    t.setString('AGUARDANDO_CLIENTE', pAguardandoCliente);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    vExec.Clear;
    vExec.Add('delete from ORCAMENTOS_ITENS ');
    vExec.Add('where ORCAMENTO_ID = :P1 ');
    vExec.Executar([pOrcamentoId]);

    vItens.setInt('ORCAMENTO_ID', pOrcamentoId, True);
    for i := Low(pOrcamentoItens) to High(pOrcamentoItens) do begin
      vItens.setInt('ITEM_ID', pOrcamentoItens[i].item_id, True);
      vItens.setInt('PRODUTO_ID', pOrcamentoItens[i].produto_id);

      vItens.setDouble('VALOR_TOTAL', pOrcamentoItens[i].valor_total);
      vItens.setDouble('VALOR_TOTAL_DESCONTO', pOrcamentoItens[i].valor_total_desconto);
      vItens.setDouble('VALOR_TOTAL_OUTRAS_DESPESAS', pOrcamentoItens[i].valor_total_outras_despesas);
      vItens.setDouble('VALOR_TOTAL_FRETE', pOrcamentoItens[i].ValorTotalFrete);

      vItens.setDouble('QUANTIDADE', pOrcamentoItens[i].quantidade);

      vItens.setDouble('QUANTIDADE_RETIRAR_ATO', pOrcamentoItens[i].QuantidadeRetirarAto);
      vItens.setDouble('QUANTIDADE_RETIRAR', pOrcamentoItens[i].QuantidadeRetirar);
      vItens.setDouble('QUANTIDADE_ENTREGAR', pOrcamentoItens[i].QuantidadeEntregar);
      vItens.setDouble('QUANTIDADE_SEM_PREVISAO', pOrcamentoItens[i].QuantidadeSemPrevisao);

      vItens.setDouble('QUANTIDADE_DEVOLV_ENTREGUES', 0);
      vItens.setDouble('QUANTIDADE_DEVOLV_PENDENTES', 0);
      vItens.setDouble('QUANTIDADE_DEVOLV_SEM_PREVISAO', 0);

      vItens.setDouble('QUANTIDADE_RETIRADOS', 0);
      vItens.setDouble('QUANTIDADE_ENTREGUES', 0);

      vItens.setDouble('PRECO_BASE', pOrcamentoItens[i].PrecoBase);
      vItens.setDouble('PRECO_UNITARIO', pOrcamentoItens[i].preco_unitario);
      vItens.setString('TIPO_CONTROLE_ESTOQUE', pOrcamentoItens[i].TipoControleEstoque);
      vItens.setString('TIPO_PRECO_UTILIZADO', pOrcamentoItens[i].TipoPrecoUtilizado);
      vItens.setDouble('PERC_DESCONTO_PRECO_MANUAL', pOrcamentoItens[i].PercDescontoPrecoManual);
      vItens.setDouble('VALOR_DESC_UNIT_PRECO_MANUAL', pOrcamentoItens[i].ValorDescUnitPrecoManual);

      vItens.setDouble('VALOR_IMPOSTOS', pOrcamentoItens[i].ValorImpostos);
      vItens.setDouble('VALOR_ENCARGOS', pOrcamentoItens[i].ValorEncargos);
      vItens.setDouble('VALOR_CUSTO_VENDA', pOrcamentoItens[i].ValorCustoVenda);
      vItens.setDouble('VALOR_CUSTO_FINAL', pOrcamentoItens[i].ValorCustoFinal);

      vItens.Inserir;

      for j := Low(pOrcamentoItens[i].Lotes) to High(pOrcamentoItens[i].Lotes) do begin

        vConsultaLote.Pesquisar([pOrcamentoId, pOrcamentoItens[i].Lotes[j].Lote, pOrcamentoItens[i].item_id]);

        if vConsultaLote.GetInt('CONTADOR') = 0 then begin
          vItensDefLotes.SetInt('ORCAMENTO_ID', pOrcamentoId, True);
          vItensDefLotes.SetInt('ITEM_ID', pOrcamentoItens[i].item_id, True);
          vItensDefLotes.setString('LOTE', pOrcamentoItens[i].Lotes[j].Lote, True);
          vItensDefLotes.setDouble('QUANTIDADE_RETIRAR_ATO', pOrcamentoItens[i].Lotes[j].QuantidadeRetirarAto);
          vItensDefLotes.setDouble('QUANTIDADE_RETIRAR', pOrcamentoItens[i].Lotes[j].QuantidadeRetirar);
          vItensDefLotes.setDouble('QUANTIDADE_ENTREGAR', pOrcamentoItens[i].Lotes[j].QuantidadeEntregar);

          vItensDefLotes.Inserir;
        end
        else begin
          vUpdate.Executar([
            pOrcamentoItens[i].Lotes[j].QuantidadeEntregar,
            pOrcamentoItens[i].Lotes[j].QuantidadeRetirar,
            pOrcamentoItens[i].Lotes[j].QuantidadeRetirarAto,
            pOrcamentoId,
            pOrcamentoItens[i].item_id,
            pOrcamentoItens[i].Lotes[j].Lote
          ]);
        end;

        if pDefinirLocalManual = 'S' then begin
          vItensDefLocais.SetInt('ORCAMENTO_ID', pOrcamentoId, True);
          vItensDefLocais.SetInt('EMPRESA_ID', pOrcamentoItens[i].Lotes[j].EmpresaId, True);
          vItensDefLocais.SetInt('PRODUTO_ID', pOrcamentoItens[i].produto_id, True);
          vItensDefLocais.SetInt('LOCAL_ID', pOrcamentoItens[i].Lotes[j].LocalId, True);
          vItensDefLocais.setString('LOTE', pOrcamentoItens[i].Lotes[j].Lote);
          vItensDefLocais.setDouble('QUANTIDADE_ATO', pOrcamentoItens[i].Lotes[j].QuantidadeRetirarAto);
          vItensDefLocais.setDouble('QUANTIDADE_ENTREGAR', pOrcamentoItens[i].Lotes[j].QuantidadeEntregar);
          vItensDefLocais.setDouble('QUANTIDADE_RETIRAR', pOrcamentoItens[i].Lotes[j].QuantidadeRetirar);
          vItensDefLocais.setInt('ITEM_ID', item_id);
          vItensDefLocais.setString('STATUS', 'PEN');

          vItensDefLocais.Inserir;
          Inc(item_id);
        end;
      end;

      if pOrcamentoItens[i].Locais = nil then
        Continue;

      for j := Low(pOrcamentoItens[i].Locais) to High(pOrcamentoItens[i].Locais) do begin
        vItensDefLocais.SetInt('ORCAMENTO_ID', pOrcamentoId, True);
        vItensDefLocais.SetInt('EMPRESA_ID', pOrcamentoItens[i].Locais[j].EmpresaId, True);
        vItensDefLocais.SetInt('PRODUTO_ID', pOrcamentoItens[i].produto_id, True);
        vItensDefLocais.SetInt('LOCAL_ID', pOrcamentoItens[i].Locais[j].LocalId, True);
        vItensDefLocais.setString('LOTE', '???');
        vItensDefLocais.setDouble('QUANTIDADE_ATO', pOrcamentoItens[i].Locais[j].QuantidadeAto);
        vItensDefLocais.setDouble('QUANTIDADE_ENTREGAR', pOrcamentoItens[i].Locais[j].QuantidadeEntregar);
        vItensDefLocais.setDouble('QUANTIDADE_RETIRAR', pOrcamentoItens[i].Locais[j].QuantidadeRetirar);
        vItensDefLocais.setInt('ITEM_ID', item_id);
        vItensDefLocais.setString('STATUS', 'PEN');

        vItensDefLocais.Inserir;
        Inc(item_id);
      end;
    end;

    for i := Low(pCreditos) to High(pCreditos) do begin
      vCreditos.setInt('ORCAMENTO_ID', pOrcamentoId, True);
      vCreditos.setInt('PAGAR_ID', pCreditos[i], True);
      vCreditos.setString('MOMENTO_USO', 'F');

      vCreditos.Inserir;
    end;

    // Gravar ambientes
    if FProdutosAmbiente <> nil then begin
      vExec.Clear;
      vExec.Add('update ORCAMENTOS set ORC_POR_AMBIENTE = ''S'' ');
      vExec.Add('where ORCAMENTO_ID = :P1 ');
      vExec.Executar([pOrcamentoId]);

      vExec.Clear;
      vExec.Add('delete from ORCAMENTOS_ITENS_AMBIENTE ');
      vExec.Add('where ORCAMENTO_ID = :P1 ');
      vExec.Executar([pOrcamentoId]);

      vExec.Clear;
      vExec.Add('insert into ORCAMENTOS_ITENS_AMBIENTE( ');
      vExec.Add('  AMBIENTE, ');
      vExec.Add('  ORCAMENTO_ID, ');
      vExec.Add('  PRODUTO_ID, ');
      vExec.Add('  QUANTIDADE ');
      vExec.Add(') values ( ');
      vExec.Add('  :P1, ');
      vExec.Add('  :P2, ');
      vExec.Add('  :P3, ');
      vExec.Add('  :P4 ');
      vExec.Add(') ');
      for i := Low(FProdutosAmbiente) to High(FProdutosAmbiente) do begin
        for j := Low(FProdutosAmbiente[i].Produtos) to High(FProdutosAmbiente[i].Produtos) do begin
          vExec.Executar([
            FProdutosAmbiente[i].NomeAmbiente,
            pOrcamentoId,
            FProdutosAmbiente[i].Produtos[j].ProdutoId,
            FProdutosAmbiente[i].Produtos[j].Quantidade
          ]);
        end;
      end;
    end;

    if pArquivos <> nil then begin
      vRetBanco := _Arquivos.AtualizarArquivos(pConexao, 'ORC', pOrcamentoId, pArquivos, nil, False, pUsuarioCadastroId);
      if vRetBanco.TeveErro then
        raise Exception.Create(vRetBanco.MensagemErroCompleta);
    end;

    vProc.Params[0].AsInteger := pOrcamentoId;

    if _BibliotecaGenerica.Em(pStatus, ['VB','VR']) then
      vProc.Executar;
  except
    on e: Exception do
      Result.TratarErro(e);
  end;
  vItensDefLotes.Free;
  vItensDefLocais.Free;
  vCreditos.Free;
  vProc.Free;
  vItens.Free;
  vExec.Free;
  vUpdate.Free;
  vConsultaLote.Active := False;
  vConsultaLote.Free;
  t.Free;
end;

function AtualizarOrcamentosPagamentosPix(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pPagamentosPix: TArray<RecPagamentosPix>;
  pEmpresaId: Integer;
  pCadastroId: Integer;
  pTurnoId: Integer;
  pDataPagamento: TDateTime
): RecRetornoBD;
var
  i: Integer;
  vExec: TExecucao;
  vPagPix: TContasRecBaixasPagtosPix;
  pBaixaId: Integer;
  t: TContaReceberBaixa;
  valorTitulos: Double;
begin
  Result.Iniciar;
  pConexao.SetRotina('BAIXA_PAG_PIX');

  try
    vExec := TExecucao.Create(pConexao);
    vPagPix := TContasRecBaixasPagtosPix.Create(pConexao);
    t := TContaReceberBaixa.Create(pConexao);
    pBaixaId := TSequencia.Create(pConexao, 'SEQ_CONTAS_RECEBER_BAIXAS').getProximaSequencia;

    valorTitulos := 0;
    for i := Low(pPagamentosPix) to High(pPagamentosPix) do
      valorTitulos := valorTitulos + pPagamentosPix[i].Valor;

    t.setInt('BAIXA_ID', pBaixaId, True);
    t.setInt('EMPRESA_ID', pEmpresaId);
    t.setIntN('CADASTRO_ID', pCadastroId);
    t.setIntN('TURNO_ID', pTurnoId);
    t.setDouble('VALOR_TITULOS', valorTitulos);
    t.setDouble('VALOR_MULTA', 0);
    t.setDouble('VALOR_JUROS', 0);
    t.setDouble('VALOR_RETENCAO', 0);
    t.setDouble('VALOR_DESCONTO', 0);
    t.setDouble('VALOR_ADIANTADO', 0);
    t.setDouble('VALOR_PIX', valorTitulos);
    t.setDouble('VALOR_DINHEIRO', 0);
    t.setDouble('VALOR_CHEQUE', 0);
    t.setDouble('VALOR_CARTAO_DEBITO', 0);
    t.setDouble('VALOR_CARTAO_CREDITO', 0);
    t.setDouble('VALOR_COBRANCA', 0);
    t.setDouble('VALOR_CREDITO', 0);
    t.setDataN('DATA_PAGAMENTO', pDataPagamento);
    t.setString('RECEBER_CAIXA', 'N');
    t.setString('TIPO', 'PIX');
    t.setString('OBSERVACOES', '');
    t.setDouble('VALOR_TROCO', 0);
    t.setString('RECEBIDO', 'S');
    t.setIntN('RECEBER_ADIANTADO_ID', 0);
    t.setString('BAIXA_PIX_REC_CX', 'S');

    t.Inserir;

    vExec.Add('delete from CONTAS_REC_BAIXAS_PAGTOS_PIX ');
    vExec.Add('where BAIXA_ID = :P1');
    vExec.Executar([pBaixaId]);

    for i := Low(pPagamentosPix) to High(pPagamentosPix) do begin
      vPagPix.setInt('BAIXA_ID', pBaixaId, True);
      vPagPix.setString('CONTA_ID', pPagamentosPix[i].ContaId, True);
      vPagPix.setDouble('VALOR', pPagamentosPix[i].Valor);

      vPagPix.Inserir;
    end;

    vExec.Limpar;
    vExec.Add('update ORCAMENTOS set BAIXA_RECEBER_PIX_ID = :P1 ');
    vExec.Add('where ORCAMENTO_ID = :P2');
    vExec.Executar([pBaixaId, pOrcamentoId]);
  except
    on e: Exception do
      Result.TratarErro(e);
  end;

  vPagPix.Free;
  vExec.Free;
  t.Free;
end;

function AtualizarOrcamentosPagamentos(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pPagamentosCheques: TArray<RecTitulosFinanceiros>;
  pPagamentosCartoes: TArray<RecTitulosFinanceiros>;
  pPagamentosCobrancas: TArray<RecTitulosFinanceiros>;
  pPagamentosFinanceiras: TArray<RecTitulosFinanceiros>;
  pCreditosNoMomentoDoRecebimento: TArray<Integer>;
  pGravandoOrcamento: Boolean;
  pPagamentosPix: TArray<RecPagamentosPix>
): RecRetornoBD;
var
  i: Integer;
  vExec: TExecucao;
  vCreditos: TOrcamentosCreditos;
  vCobrancas: TOrcamentosPagamentos;
  vCheques: TOrcamentosPagamentosCheques;
  vPagPix: TContasRecBaixasPagtosPix;
  pBaixaId: Integer;
  t: TContaReceberBaixa;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUA_FORMAS_PAGTO');

  try
    vExec := TExecucao.Create(pConexao);
    vCreditos := TOrcamentosCreditos.Create(pConexao);
    vCobrancas := TOrcamentosPagamentos.Create(pConexao);
    vCheques := TOrcamentosPagamentosCheques.Create(pConexao);
    vPagPix := TContasRecBaixasPagtosPix.Create(pConexao);
    t := TContaReceberBaixa.Create(pConexao);

    vExec.SQL.Text := 'delete from ORCAMENTOS_PAGAMENTOS where ORCAMENTO_ID = :P1';
    vExec.Executar([pOrcamentoId]);

    vExec.SQL.Text := 'delete from ORCAMENTOS_PAGAMENTOS_CHEQUES where ORCAMENTO_ID = :P1';
    vExec.Executar([pOrcamentoId]);

    vCobrancas.setInt('ORCAMENTO_ID', pOrcamentoId, True);
    for i := Low(pPagamentosCartoes) to High(pPagamentosCartoes) do begin
      vCobrancas.setInt('COBRANCA_ID', pPagamentosCartoes[i].CobrancaId, True);
      vCobrancas.setInt('ITEM_ID', i + 1, True);
      vCobrancas.setDouble('VALOR', pPagamentosCartoes[i].valor);
      vCobrancas.setDataN('DATA_VENCIMENTO', 0);
      vCobrancas.setString('TIPO', 'CR');
      vCobrancas.setInt('PARCELA', 1);
      vCobrancas.setInt('NUMERO_PARCELAS', 1);
      vCobrancas.setString('NUMERO_CARTAO', pPagamentosCartoes[i].NumeroCartao);
      vCobrancas.setString('NSU_TEF', pPagamentosCartoes[i].NsuTef);
      vCobrancas.setString('CODIGO_AUTORIZACAO', pPagamentosCartoes[i].CodigoAutorizacao);
      vCobrancas.setString('TIPO_RECEB_CARTAO', pPagamentosCartoes[i].TipoRecebCartao);

      vCobrancas.Inserir;
    end;

    vCobrancas.setStringN('TIPO_RECEB_CARTAO', '');
    for i := Low(pPagamentosCobrancas) to High(pPagamentosCobrancas) do begin
      vCobrancas.setInt('COBRANCA_ID', pPagamentosCobrancas[i].CobrancaId, True);
      vCobrancas.setInt('ITEM_ID', Length(pPagamentosCartoes) + i + 1, True);
      vCobrancas.setData('DATA_VENCIMENTO', pPagamentosCobrancas[i].DataVencimento);
      vCobrancas.setDouble('VALOR', pPagamentosCobrancas[i].valor);
      vCobrancas.setString('TIPO', 'CO');
      vCobrancas.setInt('PARCELA', pPagamentosCobrancas[i].Parcela);
      vCobrancas.setInt('NUMERO_PARCELAS', pPagamentosCobrancas[i].NumeroParcelas);

      vCobrancas.Inserir;
    end;

    for i := Low(pPagamentosFinanceiras) to High(pPagamentosFinanceiras) do begin
      vCobrancas.setInt('COBRANCA_ID', pPagamentosFinanceiras[i].CobrancaId, True);
      vCobrancas.setInt('ITEM_ID', Length(pPagamentosCartoes) + Length(pPagamentosCobrancas) + i + 1, True);
      vCobrancas.setData('DATA_VENCIMENTO', pPagamentosFinanceiras[i].DataVencimento);
      vCobrancas.setDouble('VALOR', pPagamentosFinanceiras[i].Valor);
      vCobrancas.setString('TIPO', 'FI');
      vCobrancas.setInt('PARCELA', pPagamentosFinanceiras[i].Parcela);
      vCobrancas.setInt('NUMERO_PARCELAS', pPagamentosFinanceiras[i].NumeroParcelas);

      vCobrancas.Inserir;
    end;

    vCheques.setInt('ORCAMENTO_ID', pOrcamentoId, True);
    for i := Low(pPagamentosCheques) to High(pPagamentosCheques) do begin
      vCheques.setInt('COBRANCA_ID', pPagamentosCheques[i].CobrancaId, True);
      vCheques.setInt('ITEM_ID', i + 1, True);
      vCheques.setString('BANCO', pPagamentosCheques[i].banco);
      vCheques.setString('AGENCIA', pPagamentosCheques[i].agencia);
      vCheques.setString('CONTA_CORRENTE', pPagamentosCheques[i].ContaCorrente);
      vCheques.setInt('NUMERO_CHEQUE', pPagamentosCheques[i].NumeroCheque);
      vCheques.setDouble('VALOR_CHEQUE', pPagamentosCheques[i].Valor);
      vCheques.setString('NOME_EMITENTE', pPagamentosCheques[i].NomeEmitente);
      vCheques.setString('CPF_CNPJ_EMITENTE', pPagamentosCheques[i].CpfCnpjEmitente);
      vCheques.setString('TELEFONE_EMITENTE', pPagamentosCheques[i].TelefoneEmitente);
      vCheques.setData('DATA_VENCIMENTO', pPagamentosCheques[i].DataVencimento);
      vCheques.setInt('PARCELA', pPagamentosCheques[i].parcela);
      vCheques.setInt('NUMERO_PARCELAS', pPagamentosCheques[i].NumeroParcelas);

      vCheques.Inserir;
    end;

    for i := Low(pCreditosNoMomentoDoRecebimento) to High(pCreditosNoMomentoDoRecebimento) do begin
      vCreditos.setInt('ORCAMENTO_ID', pOrcamentoId, True);
      vCreditos.setInt('PAGAR_ID', pCreditosNoMomentoDoRecebimento[i], True);
      vCreditos.setString('MOMENTO_USO', 'R');

      vCreditos.Inserir;
    end;
  except
    on e: Exception do
      Result.TratarErro(e);
  end;

  FreeAndNil(vCobrancas);
  FreeAndNil(vCreditos);
  FreeAndNil(vCheques);
  FreeAndNil(vExec);
end;

function BuscarOrcamentos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecOrcamentos>;
var
  i: Integer;
  t: TOrcamento;
begin
  Result := nil;
  t := TOrcamento.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordOrcamento;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarOrcamentosComando(pConexao: TConexao; const pComando: string): TArray<RecOrcamentos>;
var
  i: Integer;
  t: TOrcamento;
begin
  Result := nil;
  t := TOrcamento.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordOrcamento;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirOrcamento(
  pConexao: TConexao;
  pOrcamentoId: Cardinal
): RecRetornoBD;
var
  t: TOrcamento;
  vExec: TExecucao;
begin
  Result.TeveErro := False;
  t := TOrcamento.Create(pConexao);
  vExec := TExecucao.Create(pConexao);
  t.setInt('ORCAMENTO_ID', pOrcamentoId, True);

  try
    vExec.Clear;
    vExec.Add('delete from LOGS_ORCAMENTOS ');
    vExec.Add('where ORCAMENTO_ID = :P1 ');
    vExec.Executar([pOrcamentoId]);

    vExec.Clear;
    vExec.Add('delete from ORCAMENTOS_TRAMITES ');
    vExec.Add('where ORCAMENTO_ID = :P1 ');
    vExec.Executar([pOrcamentoId]);

    vExec.Clear;
    vExec.Add('delete from ORCAMENTOS_ITENS ');
    vExec.Add('where ORCAMENTO_ID = :P1 ');
    vExec.Executar([pOrcamentoId]);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vExec.Free;
  t.Free;
end;

function AtualizarValoresPagamentosOrcamento(
  pConexao: TConexao;
  pOrcamentoId: Cardinal;
  pValorOutrasDespesas: Double;
  pValorDesconto: Double;
  pValorDinheiro: Double;
  pValorCheque: Double;
  pValorCartaoDebito: Double;
  pValorCartaoCredito: Double;
  pValorCobranca: Double;
  pValorAcumulativo: Double;
  pValorFinanceira: Double;
  pValorCredito: Double;
  pValorPix: Double
): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.TeveErro := False;
  vExec := TExecucao.Create(pConexao);
  try
    vExec.SQL.Add('update ORCAMENTOS set ');
    vExec.SQL.Add('  VALOR_OUTRAS_DESPESAS = :P2, ');
    vExec.SQL.Add('  VALOR_DESCONTO = :P3, ');
    vExec.SQL.Add('  VALOR_DINHEIRO = :P4, ');
    vExec.SQL.Add('  VALOR_CHEQUE = :P5, ');
    vExec.SQL.Add('  VALOR_COBRANCA = :P6, ');
    vExec.SQL.Add('  VALOR_CARTAO_DEBITO = :P7, ');
    vExec.SQL.Add('  VALOR_CARTAO_CREDITO = :P8, ');
    vExec.SQL.Add('  VALOR_CREDITO = :P9, ');
    vExec.SQL.Add('  VALOR_ACUMULATIVO = :P10, ');
    vExec.SQL.Add('  VALOR_FINANCEIRA = :P11, ');
    vExec.SQL.Add('  VALOR_PIX = :P12 ');
    vExec.SQL.Add('where ORCAMENTO_ID = :P1 ');

    vExec.Executar([
      pOrcamentoId,
      pValorOutrasDespesas,
      pValorDesconto,
      pValorDinheiro,
      pValorCheque,
      pValorCobranca,
      pValorCartaoDebito,
      pValorCartaoCredito,
      pValorCredito,
      pValorAcumulativo,
      pValorFinanceira,
      pValorPix]
    );
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vExec.Free;
end;

function GerarNotaRetiraAto(pConexao: TConexao; const pRetiradasIds: TArray<Integer>; const pControlarTransacao: Boolean): RecRetornoBD;
var
  i: Integer;
  vProc: TProcedimentoBanco;
begin
  Result.TeveErro := False;
  pConexao.SetRotina('GERAR_NOTA_RET_ATO');

  try
    if pControlarTransacao then
      pConexao.IniciarTransacao;

    vProc := TProcedimentoBanco.Create(pConexao, 'GERAR_NOTA_RETIRA_ATO');

    for i := Low(pRetiradasIds) to High(pRetiradasIds) do begin
      vProc.Executar([pRetiradasIds[i]]);

      Result.AddInt(vProc.ParamByName('oNOTA_FISCAL_ID').AsInteger);
      Result.AddString(vProc.ParamByName('oTIPO_NOTA_GERAR').AsString);
    end;

    if pControlarTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if pControlarTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;
  FreeAndNil(vProc);
end;

function GerarNotaRetiraAtoAgrupado(pConexao: TConexao; const pOrcamentoId: Integer; const pControlarTransacao: Boolean): RecRetornoBD;
var
  i: Integer;
  vProc: TProcedimentoBanco;
begin
  Result.TeveErro := False;
  pConexao.SetRotina('GERAR_NOTA_RET_ATO_AGRUP');

  try
    if pControlarTransacao then
      pConexao.IniciarTransacao;

    vProc := TProcedimentoBanco.Create(pConexao, 'GERAR_NOTA_RETIRA_ATO_AGRUPADO');

    vProc.Executar([pOrcamentoId]);

    Result.AddInt(vProc.ParamByName('oNOTA_FISCAL_ID').AsInteger);
    Result.AddString(vProc.ParamByName('oTIPO_NOTA_GERAR').AsString);

    if pControlarTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if pControlarTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;
  FreeAndNil(vProc);
end;

function AlterarParceiroVenda(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pParceiroId: Integer
): RecRetornoBD;
var
  exe: TExecucao;
begin
  Result.TeveErro := False;
  exe := TExecucao.Create(pConexao);
  try
    exe.SQL.Add('update ORCAMENTOS set ');
    exe.SQL.Add('  PROFISSIONAL_ID = :P2 ');
    exe.SQL.Add('where ORCAMENTO_ID = :P1 ');

    exe.Executar([pOrcamentoId, pParceiroId]);
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  FreeAndNil(exe);
end;

function AtualizarStatusOrcamento(
  pConexao: TConexao;
  pOrcamentoId: Cardinal;
  pNovoStatus: string
): RecRetornoBD;
var
  exe: TExecucao;
begin
  Result.TeveErro := False;
  exe := TExecucao.Create(pConexao);
  try
    exe.SQL.Add('update ORCAMENTOS set ');
    exe.SQL.Add('  STATUS = :P2 ');
    exe.SQL.Add('where ORCAMENTO_ID = :P1 ');

    exe.Executar([pOrcamentoId, pNovoStatus]);
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  FreeAndNil(exe);
end;

function ReceberPedido(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pTurnoId: Integer;
  pFuncionarioId: Integer;
  pGerarCreditoTroco: Boolean;
  pValorTroco: Currency;
  pTipoNotaGerar: string
): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('RECEBER_PEDIDO');

  vProc := TProcedimentoBanco.Create(pConexao, 'RECEBER_PEDIDO');
  try
    vProc.Params[0].AsInteger  := pOrcamentoId;
    vProc.Params[1].AsInteger  := pTurnoId;
    vProc.Params[2].AsCurrency := pValorTroco;
    vProc.Params[3].AsString   := IfThen(pGerarCreditoTroco, 'S', 'N');
    vProc.Params[4].AsString   := pTipoNotaGerar;

    vProc.Executar;
  except
    on e: Exception do
      Result.TratarErro(e);
  end;
  FreeAndNil(vProc);
end;

function GerarCartoesReceber(
  pConexao: TConexao;
  pOrcamentoID: Integer;
  pItemID: Integer;
  pNSU: string;
  pCodigoAutorizacaoTEF: string
): RecRetornoBD;
var
  procedimento: TProcedimentoBanco;
begin
  Result.TeveErro := False;
  procedimento := TProcedimentoBanco.Create(pConexao, 'GERAR_CARTOES_RECEBER');
  try
    procedimento.Params[0].AsInteger := pOrcamentoId;
    procedimento.Params[1].AsInteger := pItemID;
    procedimento.Params[2].AsString  := pNSU;
    procedimento.Params[3].AsString  := pCodigoAutorizacaoTEF;

    procedimento.Executar;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  FreeAndNil(procedimento);
end;

function CancelarFechamentoPedido(
  pConexao: TConexao;
  pOrcamentoId: Integer
): RecRetornoBD;
var
  vProcedimento: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('CANCELAR_FECHAMENTO_PEDIDO');

  vProcedimento := TProcedimentoBanco.Create(pConexao, 'CANCELAR_FECHAMENTO_PEDIDO');
  try
    pConexao.IniciarTransacao;

    vProcedimento.Params[0].AsInteger := pOrcamentoId;
    vProcedimento.Executar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  FreeAndNil(vProcedimento);
end;

function CancelarRecebimentoPedido(
  pConexao: TConexao;
  pOrcamentoId: Integer
): RecRetornoBD;
var
  vProcedimento: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('CANCELAR_RECEBIMENTO_PEDIDO');
  vProcedimento := TProcedimentoBanco.Create(pConexao, 'CANCELAR_RECEBIMENTO_PEDIDO');
  try
    pConexao.IniciarTransacao;

    vProcedimento.Params[0].AsInteger := pOrcamentoId;
    vProcedimento.Executar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  FreeAndNil(vProcedimento);
end;

function ExistemVendasPendentesTurno(pConexao: TConexao; const pTurnoId: Integer): Boolean;
var
  vSql: TConsulta;
begin
  Result := False;
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  count(*) ');
  vSql.SQL.Add('from ');
  vSql.SQL.Add('  NOTAS_FISCAIS NFI ');

  vSql.SQL.Add('inner join ORCAMENTOS ORC ');
  vSql.SQL.Add('on NFI.ORCAMENTO_BASE_ID = ORC.ORCAMENTO_ID ');

  vSql.SQL.Add('where ORC.TURNO_ID = :P1 ');
  vSql.SQL.Add('and NFI.STATUS = ''N'' ');

  if vSql.Pesquisar([pTurnoId]) then
    Result := vSql.GetInt(0) > 0;

  vSql.Active := False;
  vSql.Free;
end;

function BuscarInformacoesImpressao(pConexao: TConexao; const pOrcamentoId: Integer): RecOrcamentosImpressao;
var
  vSql: TConsulta;
begin
  Result.OrcamentoId := 0;
  vSql := TConsulta.Create(pConexao);

  with vSql.SQL do begin
    Add('select ');
    Add('  ORCAMENTO_ID, ');
    Add('  EMPRESA_ID, ');
    Add('  CLIENTE_ID, ');
    Add('  NOME_CLIENTE, ');
    Add('  APELIDO, ');
    Add('  LOGRADOURO, ');
    Add('  COMPLEMENTO, ');
    Add('  NUMERO, ');
    Add('  BAIRRO_ID, ');
    Add('  NOME_BAIRRO, ');
    Add('  CEP, ');
    Add('  CIDADE_ID, ');
    Add('  NOME_CIDADE, ');
    Add('  ESTADO_ID, ');
    Add('  NOME_ESTADO, ');
    Add('  TELEFONE_PRINCIPAL, ');
    Add('  TELEFONE_CELULAR, ');
    Add('  CONDICAO_ID, ');
    Add('  NOME_CONDICAO_PAGAMENTO, ');
    Add('  VENDEDOR_ID, ');
    Add('  NOME_VENDEDOR, ');
    Add('  VALOR_TOTAL, ');
    Add('  VALOR_TOTAL_PRODUTOS, ');
    Add('  VALOR_TOTAL_PRODUTOS_PROMOCAO, ');
    Add('  VALOR_OUTRAS_DESPESAS, ');
    Add('  VALOR_FRETE, ');
    Add('  VALOR_FRETE_ITENS, ');
    Add('  VALOR_DESCONTO, ');
    Add('  VALOR_DINHEIRO, ');
    Add('  VALOR_CHEQUE, ');
    Add('  VALOR_CARTAO_DEBITO, ');
    Add('  VALOR_CARTAO_CREDITO, ');
    Add('  VALOR_COBRANCA, ');
    Add('  VALOR_CREDITO, ');
    Add('  STATUS, ');
    Add('  OBSERVACOES_CAIXA, ');
    Add('  E_MAIL, ');
    Add('  VALOR_ADIANTADO_ACUMULADO_ABER as VALOR_ADIANTAMENTO, ');
    Add('  CPF_CNPJ, ');
    Add('  INDICE_CONDICAO_PAGAMENTO ');
    Add('from ');
    Add('  VW_ORCAMENTOS_IMPRESSAO ');
    Add('where ORCAMENTO_ID = :P1 ');
  end;

  if vSql.Pesquisar([pOrcamentoId]) then begin
    Result.OrcamentoId           := vSql.GetInt('ORCAMENTO_ID');
    Result.EmpresaId             := vSql.GetInt('EMPRESA_ID');
    Result.ClienteId             := vSql.GetInt('CLIENTE_ID');
    Result.NomeCliente           := vSql.GetString('NOME_CLIENTE');
    Result.Apelido               := vSql.GetString('APELIDO');
    Result.Logradouro            := vSql.GetString('LOGRADOURO');
    Result.Complemento           := vSql.GetString('COMPLEMENTO');
    Result.Numero                := vSql.GetString('NUMERO');
    Result.BairroId              := vSql.GetInt('BAIRRO_ID');
    Result.NomeBairro            := vSql.GetString('NOME_BAIRRO');
    Result.Cep                   := vSql.GetString('CEP');
    Result.CidadeId              := vSql.GetInt('CIDADE_ID');
    Result.NomeCidade            := vSql.GetString('NOME_CIDADE');
    Result.EstadoId              := vSql.GetString('ESTADO_ID');
    Result.NomeEstado            := vSql.GetString('NOME_ESTADO');
    Result.TelefonePrincipal     := vSql.GetString('TELEFONE_PRINCIPAL');
    Result.TelefoneCelular       := vSql.GetString('TELEFONE_CELULAR');
    Result.CondicaoId            := vSql.GetInt('CONDICAO_ID');
    Result.NomeCondicaoPagamento := vSql.GetString('NOME_CONDICAO_PAGAMENTO');
    Result.VendedorId            := vSql.GetInt('VENDEDOR_ID');
    Result.NomeVendedor          := vSql.GetString('NOME_VENDEDOR');
    Result.ValorTotal            := vSql.GetDouble('VALOR_TOTAL');
    Result.ValorTotalProdutos    := vSql.GetDouble('VALOR_TOTAL_PRODUTOS');
    Result.ValorTotalProdutosPromocao := vSql.GetDouble('VALOR_TOTAL_PRODUTOS_PROMOCAO');
    Result.ValorOutrasDespesas   := vSql.GetDouble('VALOR_OUTRAS_DESPESAS');
    Result.ValorFrete            := vSql.GetDouble('VALOR_FRETE');
    Result.ValorFreteItens       := vSql.GetDouble('VALOR_FRETE_ITENS');
    Result.ValorDesconto         := vSql.GetDouble('VALOR_DESCONTO');
    Result.ValorDinheiro         := vSql.GetDouble('VALOR_DINHEIRO');
    Result.ValorAdiantamento     := vSql.GetDouble('VALOR_ADIANTAMENTO');
    Result.ValorCheque           := vSql.GetDouble('VALOR_CHEQUE');
    Result.ValorCartao           := vSql.GetDouble('VALOR_CARTAO_DEBITO') + vSql.GetDouble('VALOR_CARTAO_CREDITO');
    Result.ValorCobranca         := vSql.GetDouble('VALOR_COBRANCA');
    Result.ValorCredito          := vSql.GetDouble('VALOR_CREDITO');
    Result.Status                := vSql.GetString('STATUS');
    Result.ObservacoesCaixa      := vSql.GetString('OBSERVACOES_CAIXA');
    Result.EmailCliente          := vSql.GetString('E_MAIL');
    Result.CpfCnpj               := vSql.GetString('CPF_CNPJ');
    Result.Indice                := vSql.GetDouble('INDICE_CONDICAO_PAGAMENTO');

  end;

  vSql.Active := False;
  vSql.Free;
end;

function BuscarEntregasPendentes(pConexao: TConexao; pFiltro: string): TArray<RecItensPendentesEntrega>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  ORCAMENTO_ID, ');
  vSql.Add('  EMPRESA_ID, ');
  vSql.Add('  NOME_EMPRESA, ');
  vSql.Add('  CADASTRO_ID, ');
  vSql.Add('  NOME_CLIENTE, ');
  vSql.Add('  PRODUTO_ID, ');
  vSql.Add('  NOME_PRODUTO, ');
  vSql.Add('  SALDO, ');
  vSql.Add('  AGUARDAR_CONTATO_CLIENTE, ');
  vSql.Add('  PREVISAO_ENTREGA, ');
  vSql.Add('  TIPO_MOVIMENTO ');
  vSql.Add('from ');
  vSql.Add('  VW_ENTREGAS_PENDENTES ');
  vSql.Add(pFiltro);

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].OrcamentoId     := vSql.GetInt('ORCAMENTO_ID');
      Result[i].EmpresaId       := vSql.GetInt('EMPRESA_ID');
      Result[i].NomeEmpresa     := vSql.GetString('NOME_EMPRESA');
      Result[i].CadastroId      := vSql.GetInt('CADASTRO_ID');
      Result[i].NomeCliente     := vSql.GetString('NOME_CLIENTE');
      Result[i].ProdutoId       := vSql.GetInt('PRODUTO_ID');
      Result[i].NomeProduto     := vSql.GetString('NOME_PRODUTO');
      Result[i].Saldo           := vSql.GetDouble('SALDO');
      Result[i].PrevisaoEntrega := vSql.GetData('PREVISAO_ENTREGA');
      Result[i].TipoMovimento   := vSql.GetString('TIPO_MOVIMENTO');

      Result[i].TipoMovimentoAnalitico :=
        _Biblioteca.Decode(
          Result[i].TipoMovimento,[
            'SEP', 'Sem previs�o',
            'RPE', 'Retirada pendente',
            'ENP', 'Entrega pendente']
        );

      vSql.Next;
    end;
  end;

  vSql.Free;
end;

function PedidoPodeSerCancelado(pConexao: TConexao; pOrcamentoId: Integer): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('VER_PEDIDO_PODE_SER_CANC');
  vProc := TProcedimentoBanco.Create(pConexao, 'PEDIDO_PODE_SER_CANCELADO');

  try
    vProc.ParamByName('iORCAMENTO_ID').AsInteger := pOrcamentoId;
    vProc.Executar;
  except
    on e: Exception do
      Result.TratarErro(e);
  end;

  vProc.Free;
end;

function ExistemPendenciasConfirmacaoEntrega(pConexao: TConexao; pOrcamentoId: Integer; pProdutosIds: TArray<Integer>): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  sum(QUANTIDADE) as QUANTIDADE ');
  vSql.Add('from ( ');
  vSql.Add('  select ');
  vSql.Add('    count(ITE.ENTREGA_ID) as QUANTIDADE ');
  vSql.Add('  from ');
  vSql.Add('    ENTREGAS_ITENS ITE ');

  vSql.Add('  inner join ENTREGAS ENT ');
  vSql.Add('  on ITE.ENTREGA_ID = ENT.ENTREGA_ID ');

  vSql.Add('  where ENT.ORCAMENTO_ID = :P1 ');
  vSql.Add('  and ENT.STATUS in(''AGS'', ''ESE'', ''AGC'', ''ERO'') ');
  vSql.Add('  and ' + FiltroInInt('ITE.PRODUTO_ID', pProdutosIds));

  vSql.Add('  union all ');

  vSql.Add('  select ');
  vSql.Add('    count(ITE.RETIRADA_ID) as QUANTIDADE ');
  vSql.Add('  from ');
  vSql.Add('    RETIRADAS_ITENS ITE ');

  vSql.Add('  inner join RETIRADAS RET ');
  vSql.Add('  on ITE.RETIRADA_ID = RET.RETIRADA_ID ');

  vSql.Add('  where RET.ORCAMENTO_ID = :P1 ');
  vSql.Add('  and RET.CONFIRMADA = ''N'' ');
  vSql.Add('  and ' + FiltroInInt('ITE.PRODUTO_ID', pProdutosIds));
  vSql.Add(') ');

  vSql.Pesquisar([pOrcamentoId]);
  Result := vSql.GetInt(0) > 0;

  vSql.Free;
end;

function ExistemEntregasRealizadas(pConexao: TConexao; pOrcamentoId: Integer; pProdutosIds: TArray<Integer>): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  sum(QUANTIDADE) as QUANTIDADE ');
  vSql.Add('from ( ');
  vSql.Add('  select ');
  vSql.Add('    count(ITE.ENTREGA_ID) as QUANTIDADE ');
  vSql.Add('  from ');
  vSql.Add('    ENTREGAS_ITENS ITE ');

  vSql.Add('  inner join ENTREGAS ENT ');
  vSql.Add('  on ITE.ENTREGA_ID = ENT.ENTREGA_ID ');

  vSql.Add('  where ENT.ORCAMENTO_ID = :P1 ');
  vSql.Add('  and ' + FiltroInInt('ITE.PRODUTO_ID', pProdutosIds));

  vSql.Add('  union all ');

  vSql.Add('  select ');
  vSql.Add('    count(ITE.RETIRADA_ID) as QUANTIDADE ');
  vSql.Add('  from ');
  vSql.Add('    RETIRADAS_ITENS ITE ');

  vSql.Add('  inner join RETIRADAS RET ');
  vSql.Add('  on ITE.RETIRADA_ID = RET.RETIRADA_ID ');

  vSql.Add('  where RET.ORCAMENTO_ID = :P1 ');
  vSql.Add('  and ' + FiltroInInt('ITE.PRODUTO_ID', pProdutosIds));
  vSql.Add(') ');

  vSql.Pesquisar([pOrcamentoId]);
  Result := vSql.GetInt(0) > 0;

  vSql.Free;
end;

function AtualizarObservacaoOrcamento(pConexao: TConexao; pOrcamentoId: Integer; pObservacao: string; pTipo: string): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_OBSERVACAO');
  vExec := TExecucao.Create(pConexao);
  try
    pConexao.IniciarTransacao;

    vExec.Add('update ORCAMENTOS set ');

    if pTipo = 'C' then
      vExec.Add('  OBSERVACOES_CAIXA = :P2 ')
    else if pTipo = 'E' then
      vExec.Add('  OBSERVACOES_EXPEDICAO = :P2 ')
    else
      vExec.Add('  OBSERVACOES_NFE = :P2 ');

    vExec.Add('where ORCAMENTO_ID = :P1 ');

    vExec.Executar([pOrcamentoId, pObservacao]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  FreeAndNil(vExec);
end;

function PedidoFazParteAcumuladoPendente(pConexao: TConexao; pOrcamentoId: Integer): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  count(*) ');
  vSql.Add('from ');
  vSql.Add('  ORCAMENTOS ORC ');

  vSql.Add('inner join ACUMULADOS ACU ');
  vSql.Add('on ORC.ACUMULADO_ID = ACU.ACUMULADO_ID ');

  vSql.Add('where ACU.STATUS = ''AR'' ');
  vSql.Add('and ORC.ORCAMENTO_ID = :P1 ');

  vSql.Pesquisar([pOrcamentoId]);
  Result := vSql.GetInt(0) > 0;

  vSql.Free;
end;

function PedidoTemFinanceiroAbertoReceberNaEntrega(pConexao: TConexao; pOrcamentoId: Integer): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  count(*) ');
  vSql.Add('from ');
  vSql.Add('  CONTAS_RECEBER ');
  vSql.Add('where STATUS = ''A'' ');
  vSql.Add('and ORCAMENTO_ID = :P1 ');

  vSql.Pesquisar([pOrcamentoId]);
  Result := vSql.GetInt(0) > 0;

  vSql.Free;
end;

function AtualizarIndiceDescontoVenda(pConexao: TConexao; pOrcamentoId: Integer; pIndiceDescontoVendaId: Integer): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_IND_DESC_VENDA');

  vExec := TExecucao.Create(pConexao);

  vExec.Add('update ORCAMENTOS set');
  vExec.Add('  INDICE_DESCONTO_VENDA_ID = ' + IIfStr(pIndiceDescontoVendaId = 0, 'null', IntToStr(pIndiceDescontoVendaId)));
  vExec.Add('where ORCAMENTO_ID = :P1 ');

  try
    pConexao.IniciarTransacao;

    vExec.Executar([pOrcamentoId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
end;

function AtualizarValorAdiantado(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pValorAdiantado: Double;
  pEmTransacao: Boolean
): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_VALOR_ADIANT_ORC');
  vExec := TExecucao.Create(pConexao);
  try
    if not pEmTransacao then
      pConexao.IniciarTransacao;

    vExec.Add('update ORCAMENTOS set ');
    vExec.Add('  VALOR_ADIANTADO_ACUMULADO_ABER = VALOR_ADIANTADO_ACUMULADO_ABER + :P2 ');
    vExec.Add('where ORCAMENTO_ID = :P1 ');

    vExec.Executar([pOrcamentoId, pValorAdiantado]);

    if not pEmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pEmTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;
  FreeAndNil(vExec);
end;

function LiberarPedidoRecebimentoNaEntrega(pConexao: TConexao; pOrcamentoId: Integer): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('LIBERAR_PEDIDO_REC_NA_ENTREGA');
  vProc := TProcedimentoBanco.Create(pConexao, 'LIBERAR_PEDIDO_RECEB_ENTREGA');

  try
    pConexao.IniciarTransacao;

    vProc.Executar([pOrcamentoId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  FreeAndNil(vProc);
end;

function AtualizarEnderecoEntrega(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pLogradouro: string;
  pComplemento: string;
  pNumero: string;
  pPontoReferencia: string;
  pBairroId: Integer;
  pCep: string;
  pInscricaoEstadual: string
): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_END_ENT_ORC');

  vExec := TExecucao.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    vExec.Add('update ORCAMENTOS set ');
    vExec.Add('  COMPLEMENTO = :P2, ');
    vExec.Add('  LOGRADOURO = :P3, ');
    vExec.Add('  NUMERO = :P4, ');
    vExec.Add('  PONTO_REFERENCIA = :P5, ');
    vExec.Add('  BAIRRO_ID = :P6, ');
    vExec.Add('  CEP = :P7, ');
    vExec.Add('  INSCRICAO_ESTADUAL = :P8 ');
    vExec.Add('where ORCAMENTO_ID = :P1 ');

    vExec.Executar([
      pOrcamentoId,
      pComplemento,
      pLogradouro,
      pNumero,
      pPontoReferencia,
      pBairroId,
      pInscricaoEstadual
    ]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
end;

function BuscarCpfConsumidorFinal(pConexao: TConexao; orcamento_id: Integer): string;
var
  vConsulta: TConsulta;
begin
  Result := '';
  vConsulta := TConsulta.Create(pConexao);

  vConsulta.SQL.Add(
    'select ' +
    '  CPF_CONSUMIDOR_FINAL ' +
    'from ' +
    '  ORCAMENTOS ' +
    'where ORCAMENTO_ID = :P1 '
  );

  try
    if vConsulta.Pesquisar([orcamento_id]) then
      Result := vConsulta.FieldByName('CPF_CONSUMIDOR_FINAL').AsString;

  finally
    FreeAndNil(vConsulta);
  end;
end;

function AtualizarCpfConsumidorFinal(pConexao: TConexao; orcamento_id: Integer; cpf_consumidor_final: string): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_CPF_CONS');

  vExec := TExecucao.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    vExec.Add('update ORCAMENTOS set ');
    vExec.Add('  CPF_CONSUMIDOR_FINAL = :P2 ');
    vExec.Add('where ORCAMENTO_ID = :P1 ');

    vExec.Executar([
      orcamento_id,
      cpf_consumidor_final
    ]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
end;

function AtualizarCustoProdutosOrcamento(pConexao: TConexao; vItens: TArray<RecOrcamentoItens>; orcamentoId: Integer): RecRetornoBD;
var
  vExec: TExecucao;
  i: Integer;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_CUSTOS_ORCAMENTO');

  vExec := TExecucao.Create(pConexao);

  try
    pConexao.IniciarTransacao;

     //Esse reprocessamento s� funciona utilizando 'FIN' no tipo de custo do lucro
    vExec.Add('update ORCAMENTOS_ITENS set ');
    vExec.Add('  VALOR_CUSTO_FINAL = :P2, ');
    vExec.Add('  VALOR_IMPOSTOS = :P3, ');
    vExec.Add('  CUSTO_ULTIMO_PEDIDO = :P4, ');
    vExec.Add('  PRECO_LIQUIDO = :P5 ');
    vExec.Add('where ORCAMENTO_ID = :P1 ');
    vExec.Add('and PRODUTO_ID = :P6 ');

    for I := Low(vItens) to High(vItens) do begin
      vExec.Executar([
        orcamentoId,
        vItens[i].ValorCustoFinal,
        vItens[i].ValorImpostos,
        vItens[i].CustoEntrada,
        vItens[i].CustoEntrada,
        vItens[i].produto_id
      ]);
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
end;

function AtualizarAguardandoContatoCliente(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pAguardandoContato: string
): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_AGU_CON_CLI');

  vExec := TExecucao.Create(pConexao);
  try
    pConexao.IniciarTransacao;

    vExec.Add('update ORCAMENTOS set');
    vExec.Add('  AGUARDANDO_CLIENTE = :P2 ');
    vExec.Add('where ORCAMENTO_ID = :P1 ');
    vExec.Executar([pOrcamentoId, pAguardandoContato]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
end;

end.
