unit _RelacaoCadastros;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsCadastros,  Vcl.Graphics, _Biblioteca;

function getCorAtivo(pTipoAjuste: string): TColor;
function BuscarCadastros(pConexao: TConexao; pComando: string): TArray<RecRelacaoCadastros>;

implementation

function getCorAtivo(pTipoAjuste: string): TColor;
begin
  Result := _Biblioteca.Decode(
    pTipoAjuste,
    [
      'S', $000096DB,
      'N', clRed,
      clBlack
    ]
  );
end;

function BuscarCadastros(pConexao: TConexao; pComando: string): TArray<RecRelacaoCadastros>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select');
  vSql.Add('  CAD.CADASTRO_ID,');
  vSql.Add('  CAD.NOME_FANTASIA,');
  vSql.Add('  CAD.RAZAO_SOCIAL,');
  vSql.Add('  CAD.TIPO_PESSOA,');
  vSql.Add('  CAD.CPF_CNPJ,');
  vSql.Add('  CAD.LOGRADOURO,');
  vSql.Add('  CAD.COMPLEMENTO,');
  vSql.Add('  CAD.NUMERO,');
  vSql.Add('  CAD.BAIRRO_ID,');
  vSql.Add('  BAI.NOME_BAIRRO,');
  vSql.Add('  BAI.NOME_CIDADE,');
  vSql.Add('  CAD.PONTO_REFERENCIA,');
  vSql.Add('  CAD.CEP,');
  vSql.Add('  CAD.TELEFONE_PRINCIPAL,');
  vSql.Add('  CAD.TELEFONE_CELULAR,');
  vSql.Add('  CAD.TELEFONE_FAX,');
  vSql.Add('  CAD.E_MAIL,');
  vSql.Add('  CAD.SEXO,');
  vSql.Add('  CAD.ESTADO_CIVIL,');
  vSql.Add('  CAD.DATA_NASCIMENTO,');
  vSql.Add('  CAD.DATA_CADASTRO,');
  vSql.Add('  CAD.INSCRICAO_ESTADUAL,');
  vSql.Add('  CAD.CNAE,');
  vSql.Add('  CAD.RG,');
  vSql.Add('  CAD.ORGAO_EXPEDIDOR_RG,');
  vSql.Add('  case when CAD.E_CLIENTE = ''S'' then ''Sim'' else ''Nao'' end as E_CLIENTE,');
  vSql.Add('  case when CAD.E_FORNECEDOR = ''S'' then ''Sim'' else ''Nao'' end as E_FORNECEDOR,');
  vSql.Add('  case when CAD.E_MOTORISTA = ''S'' then ''Sim'' else ''Nao'' end as E_MOTORISTA,');
  vSql.Add('  case when CAD.E_TRANSPORTADORA = ''S'' then ''Sim'' else ''Nao'' end as E_TRANSPORTADORA,');
  vSql.Add('  case when CAD.E_PROFISSIONAL = ''S'' then ''Sim'' else ''Nao'' end as E_PROFISSIONAL,');
  vSql.Add('  case when CAD.ATIVO = ''S'' then ''Sim'' else ''Nao'' end as ATIVO_ANALITICO,');
  vSql.Add('  CAD.ATIVO,');
  vSql.Add('  CID.ESTADO_ID,');
  vSql.Add('  CAD.OBSERVACOES, ');
  vSql.Add('  CAD.DATA_CADASTRO, ');
  vSql.Add('  FUN.FUNCIONARIO_ID AS USUARIO_SESSAO_ID, ');
  vSql.Add('  FUN.NOME AS NOME_USUARIO ');
  vSql.Add('from');
  vSql.Add('  CADASTROS CAD');

  vSql.Add('inner join VW_BAIRROS BAI');
  vSql.Add('on CAD.BAIRRO_ID = BAI.BAIRRO_ID');

  vSql.Add('inner join CIDADES CID');
  vSql.Add('on BAI.CIDADE_ID = CID.CIDADE_ID');

  vSql.Add('left join CLIENTES CLI');
  vSql.Add('on CAD.CADASTRO_ID = CLI.CADASTRO_ID');

  vSql.Add('left join FUNCIONARIOS FUN');
  vSql.Add('on CLI.USUARIO_SESSAO_ID = FUN.FUNCIONARIO_ID');

  vSql.Add('left join FORNECEDORES FON');
  vSql.Add('on CAD.CADASTRO_ID = FON.CADASTRO_ID');

  vSql.Add(pComando);

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].CadastroId        := vSql.GetInt('CADASTRO_ID');
      Result[i].NomeFantasia      := vSql.GetString('NOME_FANTASIA');
      Result[i].RazaoSocial       := vSql.GetString('RAZAO_SOCIAL');
      Result[i].TipoPessoa        := vSql.GetString('TIPO_PESSOA');
      Result[i].CpfCnpj           := vSql.GetString('CPF_CNPJ');
      Result[i].Logradouro        := vSql.GetString('LOGRADOURO');
      Result[i].Complemento       := vSql.GetString('COMPLEMENTO');
      Result[i].Numero            := vSql.GetString('NUMERO');
      Result[i].BairroId          := vSql.GetInt('BAIRRO_ID');
      Result[i].NomeBairro        := vSql.GetString('NOME_BAIRRO');
      Result[i].NomeCidade        := vSql.GetString('NOME_CIDADE');
      Result[i].PontoReferencia   := vSql.GetString('PONTO_REFERENCIA');
      Result[i].Cep               := vSql.GetString('CEP');
      Result[i].TelefonePrincipal := vSql.GetString('TELEFONE_PRINCIPAL');
      Result[i].TelefoneCelular   := vSql.GetString('TELEFONE_CELULAR');
      Result[i].TelefoneFax       := vSql.GetString('TELEFONE_FAX');
      Result[i].Email             := vSql.GetString('E_MAIL');
      Result[i].Sexo              := vSql.GetString('SEXO');
      Result[i].EstadoCivil       := vSql.GetString('ESTADO_CIVIL');
      Result[i].DataNascimento    := vSql.GetData('DATA_NASCIMENTO');
      Result[i].DataCadastro      := vSql.GetData('DATA_CADASTRO');
      Result[i].InscricaoEstadual := vSql.GetString('INSCRICAO_ESTADUAL');
      Result[i].Cnae              := vSql.GetString('CNAE');
      Result[i].Rg                := vSql.GetString('RG');
      Result[i].OrgaoExpedidorRg  := vSql.GetString('ORGAO_EXPEDIDOR_RG');
      Result[i].ECliente          := vSql.GetString('E_CLIENTE');
      Result[i].EFornecedor       := vSql.GetString('E_FORNECEDOR');
      Result[i].EMotorista        := vSql.GetString('E_MOTORISTA');
      Result[i].ETransportadora   := vSql.GetString('E_TRANSPORTADORA');
      Result[i].EProfissional     := vSql.GetString('E_PROFISSIONAL');
      Result[i].AtivoSintetico    := vSql.GetString('ATIVO');
      Result[i].AtivoAnalitico    := vSql.GetString('ATIVO_ANALITICO');
      Result[i].EstadoId          := vSql.GetString('ESTADO_ID');
      Result[i].Observacoes       := vSql.GetString('OBSERVACOES');

      Result[i].DataCadastro      := vSql.GetData('DATA_CADASTRO');
      Result[i].UsuarioSessaoId   := vSql.GetInt('USUARIO_SESSAO_ID');
      Result[i].NomeUsuario       := vSql.GetString('NOME_USUARIO');

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

end.



