unit _GruposEstoques;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecGruposEstoques = record
    EmpresaId: Integer;
    EmpresaGrupoId: Integer;
    NomeFantasia: string;
    Ordem: Integer;
    Tipo: string;
  end;

  TGruposEstoques = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordGruposEstoques: RecGruposEstoques;
  end;

function BuscarGruposEstoques(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecGruposEstoques>;

function AtualizarGruposEstoques(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pTipo: string;
  pEmpresasGrupos: TArray<Integer>
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TMovimentosProdutos }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where GRU.EMPRESA_ID = :P1 ' +
      'and GRU.TIPO = :P2 ' +
      'order by ' +
      '  GRU.ORDEM '
    );
end;

constructor TGruposEstoques.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'GRUPOS_ESTOQUES');

  FSql :=
    'select ' +
    '  GRU.EMPRESA_ID, ' +
    '  GRU.EMPRESA_GRUPO_ID, ' +
    '  EMP.NOME_FANTASIA, ' +
    '  GRU.ORDEM, ' +
    '  GRU.TIPO ' +
    'from ' +
    '  GRUPOS_ESTOQUES GRU ' +

    'inner join EMPRESAS EMP ' +
    'on GRU.EMPRESA_GRUPO_ID = EMP.EMPRESA_ID ';

  setFiltros(getFiltros);

  AddColuna('EMPRESA_ID', True);
  AddColuna('EMPRESA_GRUPO_ID', True);
  AddColunaSL('NOME_FANTASIA');
  AddColuna('ORDEM', True);
  AddColuna('TIPO', True);
end;

function TGruposEstoques.getRecordGruposEstoques: RecGruposEstoques;
begin
  Result.EmpresaId      := getInt('EMPRESA_ID', True);
  Result.EmpresaGrupoId := getInt('EMPRESA_GRUPO_ID', True);
  Result.NomeFantasia   := getString('NOME_FANTASIA');
  Result.Ordem          := getInt('ORDEM', True);
  Result.Tipo           := getString('TIPO', True);
end;

function BuscarGruposEstoques(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecGruposEstoques>;
var
  i: Integer;
  t: TGruposEstoques;
begin
  Result := nil;
  t := TGruposEstoques.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordGruposEstoques;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function AtualizarGruposEstoques(
  pConexao: TConexao;
  pEmpresaId: Integer;
  pTipo: string;
  pEmpresasGrupos: TArray<Integer>
): RecRetornoBD;
var
  t: TGruposEstoques;
  vExec: TExecucao;
  i: Integer;
begin
  Result.TeveErro := False;
  pConexao.SetRotina('ATUALIZAR_GRUPOS_ESTOQUES');
  t := TGruposEstoques.Create(pConexao);
  vExec := TExecucao.Create(pConexao);

  vExec.SQL.Add('delete from GRUPOS_ESTOQUES');
  vExec.SQL.Add('where EMPRESA_ID = :P1 ');
  vExec.SQL.Add('and TIPO = :P2 ');

  try
    vExec.Executar([pEmpresaId, pTipo]);

    for i := Low(pEmpresasGrupos) to High(pEmpresasGrupos) do begin
      t.setInt('EMPRESA_ID', pEmpresaId, True);
      t.setInt('EMPRESA_GRUPO_ID', pEmpresasGrupos[i], True);
      t.setInt('ORDEM', i, True);
      t.setString('TIPO', pTipo, True);

      t.Inserir
    end;
  except
    on e: exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  t.Free;
end;

end.
