unit _Unidades;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TUnidade = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordUnidade: RecUnidades;
  end;

function AtualizarUnidade(
  pConexao: TConexao;
  pUnidadeId: string;
  pDescricao: string;
  pAtivo: string
): RecRetornoBD;

function BuscarUnidades(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecUnidades>;

function ExcluirUnidade(
  pConexao: TConexao;
  pUnidadeId: string
): RecRetornoBD;

function ExisteUnidadeDescricaoIgual(pConexao: TConexao; pUnidadeId: string; pDescricao: string): Boolean;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TUnidade }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 3);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where UNIDADE_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Descri��o',
      True,
      0,
      'where DESCRICAO like :P1 || ''%'' '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Descri��o',
      True,
      0,
      'where ATIVO = ''S'' '
    );
end;

constructor TUnidade.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'UNIDADES');

  FSql :=
    'select ' +
    '  UNIDADE_ID, ' +
    '  DESCRICAO, ' +
    '  ATIVO ' +
    'from ' +
    '  UNIDADES ';

  SetFiltros(GetFiltros);

  AddColuna('UNIDADE_ID', True);
  AddColuna('DESCRICAO');
  AddColuna('ATIVO');
end;

function TUnidade.GetRecordUnidade: RecUnidades;
begin
  Result := RecUnidades.Create;
  Result.unidade_id := getString('UNIDADE_ID', True);
  Result.descricao  := getString('DESCRICAO');
  Result.ativo      := getString('ATIVO');
end;

function AtualizarUnidade(
  pConexao: TConexao;
  pUnidadeId: string;
  pDescricao: string;
  pAtivo: string
): RecRetornoBD;
var
  vUnidade: TUnidade;
  vSql: TConsulta;
  vNovo: Boolean;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_UNIDADE');

  vUnidade := TUnidade.Create(pConexao);
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select count(UNIDADE_ID) from UNIDADES where UNIDADE_ID = :P1');
  vSql.Pesquisar([pUnidadeId]);

  vNovo := vSql.GetInt(0) = 0;

  vSql.Active := False;
  vSql.Free;

  vUnidade.setString('UNIDADE_ID', pUnidadeId, True);
  vUnidade.setString('DESCRICAO', pDescricao);
  vUnidade.setString('ATIVO', pAtivo);

  try
    pConexao.IniciarTransacao;

    if vNovo then
      vUnidade.Inserir
    else
      vUnidade.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vUnidade.Free;
end;

function BuscarUnidades(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecUnidades>;
var
  i: Integer;
  t: TUnidade;
begin
  Result := nil;
  t := TUnidade.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordUnidade;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirUnidade(
  pConexao: TConexao;
  pUnidadeId: string
): RecRetornoBD;
var
  t: TUnidade;
begin
  Result.Iniciar;
  t := TUnidade.Create(pConexao);

  t.SetString('UNIDADE_ID', pUnidadeId, True);

  try
    pConexao.IniciarTransacao;

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function ExisteUnidadeDescricaoIgual(pConexao: TConexao; pUnidadeId: string; pDescricao: string): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  count(*) ');
  vSql.Add('from ');
  vSql.Add('  UNIDADES ');
  vSql.Add('where DESCRICAO = :P1 ');
  vSql.Add('and UNIDADE_ID <> :P2 ');

  vSql.Pesquisar([pDescricao, pUnidadeId]);

  Result := vSql.GetInt(0) > 0;

  vSql.Free;
end;

end.
