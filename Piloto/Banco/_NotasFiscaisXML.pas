unit _NotasFiscaisXMLs;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  TNotasFiscaisXML = class(TOperacoes)
    function  getNOTA_FISCAL_ID: Integer;
    procedure setNOTA_FISCAL_ID(const pValor: Integer);
  public
    constructor Create(pConexao: TConexao);
   // function getRecordNotasFiscaisXML: RecNotasFiscaisXMLs;
  end;

function AtualizarNotasFiscaisXML(
  pConexao: TConexao;
  pnotafiscalid: Integer;
): RecRespostaBanco;

function BuscarNotasFiscaisXMLs(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecNotasFiscaisXMLs>;

function ExcluirNotasFiscaisXML(
  pConexao: TConexao;
): RecRespostaBanco;

function getFiltros: TArray<RecFiltros>;

implementation

{ TNotasFiscaisXML }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where XXX = :P1'
    );
end;

constructor TNotasFiscaisXML.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'NOTASFISCAISXMLS');

  _sql := 
    'select ' +
    '  NOTA_FISCAL_ID, ' +
    'from ' +
    '  NOTASFISCAISXMLS';

  setFiltros(getFiltros);

  _campos.setColuna('NOTA_FISCAL_ID');
end;

function TNotasFiscaisXML.getNOTA_FISCAL_ID: Integer;
begin
  Result := _campos.getValorInteger('NOTA_FISCAL_ID');
end;

//function TNotasFiscaisXML.getRecordNotasFiscaisXML: RecNotasFiscaisXMLs;
//begin
//  Result := RecNotasFiscaisXMLs.Create;
//  Result.nota_fiscal_id := getNOTA_FISCAL_ID;
//end;

procedure TNotasFiscaisXML.setNOTA_FISCAL_ID(const pValor: Integer);
begin
  _campos.setValor('NOTA_FISCAL_ID', pValor);
end;

function AtualizarNotasFiscaisXML(
  pConexao: TConexao;
  pnotafiscalid: Integer;
): RecRespostaBanco;
var
  t: TNotasFiscaisXML;
  novo: Boolean;
  seq: TSequencia;
begin
  Result.teve_erro := False;
  t := TNotasFiscaisXML.Create(pConexao);

  novo := pXXX = 0;

  if novo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_XXX');
    pXXX := seq.getProximaSequencia;
    result.retorno_integer := pXXX;
    seq.Free;
  end;

  t.setNOTA_FISCAL_ID(pnotafiscalid);

  try
    if novo then
      t.Inserir
    else
      t.Atualizar;
  except
    on e: Exception do begin
      Result.teve_erro := True;
      Result.mensagem_erro := e.Message;
    end;
  end;

  t.Free;
end;

function BuscarNotasFiscaisXMLs(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecNotasFiscaisXMLs>;
var
  i: Integer;
  t: TNotasFiscaisXML;
begin
  Result := nil;
  t := TNotasFiscaisXML.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordNotasFiscaisXML;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirNotasFiscaisXML(
  pConexao: TConexao;
): RecRespostaBanco;
var
  t: TNotasFiscaisXML;
begin
  Result.teve_erro := False;
  t := TNotasFiscaisXML.Create(pConexao);


  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.teve_erro := True;
      Result.mensagem_erro := e.Message;
    end;
  end;

  t.Free;
end;

end.
