unit _EntregasItensPendentes;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils;

{$M+}
type
  RecEntregasItensPendentes = record
    EmpresaId: Integer;
    NomeEmpresa: string;
    LocalId: Integer;
    NomeLocal: string;
    OrcamentoId: Integer;
    ProdutoId: Integer;
    Nome: string;
    ItemId: Integer;
    Quantidade: Double;
    Entregues: Double;
    Devolvidos: Double;
    QtdeEntregar: Double;
    Saldo: Double;
    Peso: Double;
    AguardarContatoCliente: string;
    PrevisaoEntrega: TDateTime;
    Unidade: string;
    Marca: string;
    Lote: string;
    MultiploVenda: Double;
    AceitarEstoqueNegativo: string;

    PesoTotal: Double; // Utilizado no Gerar Entregas
  end;

  TEntregasItensPendentes = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordEntregasItens: RecEntregasItensPendentes;
  end;

function BuscarEntregasItensPendentes(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEntregasItensPendentes>;

function BuscarEntregasItensPendentesComando(pConexao: TConexao; pComando: string): TArray<RecEntregasItensPendentes>;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TOrcamentoTramite }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where EIP.ORCAMENTO_ID = :P1 ' +
      'and case when IOR.TIPO_CONTROLE_ESTOQUE = ''A'' or IOR.ITEM_KIT_ID is null then ''S'' else ''N'' end = ''S'' ' +
      'and EIP.SALDO > 0 ',
      'order by ' +
      '  PRO.NOME '
    );
end;

constructor TEntregasItensPendentes.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ENTREGAS_ITENS_PENDENTES');

  FSql :=
    'select ' +
    '  EIP.ORCAMENTO_ID, ' +
    '  EIP.LOCAL_ID, ' +
    '  LOC.NOME as NOME_LOCAL, ' +
    '  EIP.EMPRESA_ID, ' +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA, ' +
    '  EIP.PRODUTO_ID, ' +
    '  PRO.NOME, ' +
    '  EIP.ITEM_ID, ' +
    '  EIP.QUANTIDADE, ' +
    '  EIP.ENTREGUES, ' +
    '  EIP.DEVOLVIDOS, ' +
    '  EIP.SALDO, ' +
    '  EIP.AGUARDAR_CONTATO_CLIENTE, ' +
    '  EIP.PREVISAO_ENTREGA, ' +
    '  PRO.UNIDADE_VENDA, ' +
    '  MAR.NOME as NOME_MARCA, ' +
    '  EIP.LOTE, ' +
    '  PRO.MULTIPLO_VENDA, ' +
    '  PRO.ACEITAR_ESTOQUE_NEGATIVO, ' +
    '  PRO.PESO ' +
    'from ' +
    '  ENTREGAS_ITENS_PENDENTES EIP ' +

    'inner join VW_ORC_ITENS_SEM_KITS_AGRUPADO IOR ' +
    'on EIP.ORCAMENTO_ID = IOR.ORCAMENTO_ID ' +
    'and EIP.PRODUTO_ID = IOR.PRODUTO_ID ' +
    'and EIP.ITEM_ID = IOR.ITEM_ID ' +

    'inner join PRODUTOS PRO ' +
    'on EIP.PRODUTO_ID = PRO.PRODUTO_ID ' +

    'inner join MARCAS MAR ' +
    'on PRO.MARCA_ID = MAR.MARCA_ID ' +

    'inner join LOCAIS_PRODUTOS LOC ' +
    'on EIP.LOCAL_ID = LOC.LOCAL_ID ' +

    'inner join EMPRESAS EMP ' +
    'on EIP.EMPRESA_ID = EMP.EMPRESA_ID ';

  SetFiltros(GetFiltros);

  AddColunaSL('ORCAMENTO_ID');
  AddColunaSL('LOCAL_ID');
  AddColunaSL('NOME_LOCAL');
  AddColunaSL('EMPRESA_ID');
  AddColunaSL('NOME_EMPRESA');
  AddColunaSL('PRODUTO_ID');
  AddColunaSL('NOME');
  AddColunaSL('ITEM_ID');
  AddColunaSL('QUANTIDADE');
  AddColunaSL('ENTREGUES');
  AddColunaSL('DEVOLVIDOS');
  AddColunaSL('SALDO');
  AddColunaSL('AGUARDAR_CONTATO_CLIENTE');
  AddColunaSL('PREVISAO_ENTREGA');
  AddColunaSL('UNIDADE_VENDA');
  AddColunaSL('NOME_MARCA');
  AddColunaSL('LOTE');
  AddColunaSL('MULTIPLO_VENDA');
  AddColunaSL('ACEITAR_ESTOQUE_NEGATIVO');
  AddColunaSL('PESO');
end;

function TEntregasItensPendentes.GetRecordEntregasItens: RecEntregasItensPendentes;
begin
  Result.OrcamentoId       := getInt('ORCAMENTO_ID');
  Result.LocalId           := getInt('LOCAL_ID');
  Result.NomeLocal         := getString('NOME_LOCAL');
  Result.EmpresaId         := getInt('EMPRESA_ID');
  Result.NomeEmpresa       := getString('NOME_EMPRESA');
  Result.ProdutoId         := getInt('PRODUTO_ID');
  Result.Nome              := getString('NOME');
  Result.ItemId            := getInt('ITEM_ID');
  Result.Quantidade        := getDouble('QUANTIDADE');
  Result.Entregues         := getDouble('ENTREGUES');
  Result.Devolvidos        := getDouble('DEVOLVIDOS');
  Result.Saldo             := getDouble('SALDO');
  Result.AguardarContatoCliente := getString('AGUARDAR_CONTATO_CLIENTE');
  Result.PrevisaoEntrega   := getData('PREVISAO_ENTREGA');
  Result.Unidade           := getString('UNIDADE_VENDA');
  Result.Marca             := getString('NOME_MARCA');
  Result.Lote              := getString('LOTE');
  Result.MultiploVenda     := getDouble('MULTIPLO_VENDA');
  Result.AceitarEstoqueNegativo := getString('ACEITAR_ESTOQUE_NEGATIVO');
  Result.Peso              := getDouble('PESO');
end;

function BuscarEntregasItensPendentes(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEntregasItensPendentes>;
var
  i: Integer;
  t: TEntregasItensPendentes;
begin
  Result := nil;
  t := TEntregasItensPendentes.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordEntregasItens;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarEntregasItensPendentesComando(pConexao: TConexao; pComando: string): TArray<RecEntregasItensPendentes>;
var
  i: Integer;
  t: TEntregasItensPendentes;
begin
  Result := nil;
  t := TEntregasItensPendentes.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordEntregasItens;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
