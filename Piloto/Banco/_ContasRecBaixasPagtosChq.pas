unit _ContasRecBaixasPagtosChq;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsFinanceiros;

{$M+}
type
  TContasRecBaixasPagtosChq = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordContasRecBaixasPagtosChq: RecTitulosFinanceiros;
  end;

function AtualizarContasRecBaixasPagtosChq(
  pConexao: TConexao;
  pBaixaId: Integer;
  pCobrancaId: Integer;
  pItemId: Integer;
  pDataVencimento: TDateTime;
  pBanco: Integer;
  pAgencia: Integer;
  pContaCorrente: string;
  pNumeroCheque: Integer;
  pValorCheque: Double;
  pNomeEmitente: string;
  pCpfCnpjEmitente: string;
  pTelefoneEmitente: string;
  pParcela: Integer;
  pNumeroParcelas: Integer
): RecRetornoBD;

function BuscarContasRecBaixasPagtosChqs(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;

function ExcluirContasRecBaixasPagtosChq(
  pConexao: TConexao;
  pBaixaId: Integer;
  pCobrancaId: Integer;
  pItemId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TContasRecBaixasPagtosChq }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where BAIXA_ID = :P1'
    );
end;

constructor TContasRecBaixasPagtosChq.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONTAS_REC_BAIXAS_PAGTOS_CHQ');

  FSql :=
    'select ' +
    '  BAIXA_ID, ' +
    '  COBRANCA_ID, ' +
    '  ITEM_ID, ' +
    '  DATA_VENCIMENTO, ' +
    '  BANCO, ' +
    '  AGENCIA, ' +
    '  CONTA_CORRENTE, ' +
    '  NUMERO_CHEQUE, ' +
    '  VALOR_CHEQUE, ' +
    '  NOME_EMITENTE, ' +
    '  CPF_CNPJ_EMITENTE, ' +
    '  TELEFONE_EMITENTE, ' +
    '  PARCELA, ' +
    '  NUMERO_PARCELAS ' +
    'from ' +
    '  CONTAS_REC_BAIXAS_PAGTOS_CHQ';

  setFiltros(getFiltros);

  AddColuna('BAIXA_ID', True);
  AddColuna('COBRANCA_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('DATA_VENCIMENTO');
  AddColuna('BANCO');
  AddColuna('AGENCIA');
  AddColuna('CONTA_CORRENTE');
  AddColuna('NUMERO_CHEQUE');
  AddColuna('VALOR_CHEQUE');
  AddColuna('NOME_EMITENTE');
  AddColuna('CPF_CNPJ_EMITENTE');
  AddColuna('TELEFONE_EMITENTE');
  AddColuna('PARCELA');
  AddColuna('NUMERO_PARCELAS');
end;

function TContasRecBaixasPagtosChq.getRecordContasRecBaixasPagtosChq: RecTitulosFinanceiros;
begin
  Result.Id               := getInt('BAIXA_ID', True);
  Result.CobrancaId       := getInt('COBRANCA_ID', True);
  Result.ItemId           := getInt('ITEM_ID', True);
  Result.DataVencimento   := getData('DATA_VENCIMENTO');
  Result.Banco            := getString('BANCO');
  Result.Agencia          := getString('AGENCIA');
  Result.ContaCorrente    := getString('CONTA_CORRENTE');
  Result.NumeroCheque     := getInt('NUMERO_CHEQUE');
  Result.Valor            := getDouble('VALOR_CHEQUE');
  Result.NomeEmitente     := getString('NOME_EMITENTE');
  Result.CpfCnpjEmitente  := getString('CPF_CNPJ_EMITENTE');
  Result.TelefoneEmitente := getString('TELEFONE_EMITENTE');
  Result.Parcela          := getInt('PARCELA');
  Result.NumeroParcelas   := getInt('NUMERO_PARCELAS');
end;

function AtualizarContasRecBaixasPagtosChq(
  pConexao: TConexao;
  pBaixaId: Integer;
  pCobrancaId: Integer;
  pItemId: Integer;
  pDataVencimento: TDateTime;
  pBanco: Integer;
  pAgencia: Integer;
  pContaCorrente: string;
  pNumeroCheque: Integer;
  pValorCheque: Double;
  pNomeEmitente: string;
  pCpfCnpjEmitente: string;
  pTelefoneEmitente: string;
  pParcela: Integer;
  pNumeroParcelas: Integer
): RecRetornoBD;
var
  t: TContasRecBaixasPagtosChq;
begin
  Result.TeveErro := False;
  t := TContasRecBaixasPagtosChq.Create(pConexao);

  try
    t.setInt('BAIXA_ID', pBaixaId, True);
    t.setInt('COBRANCA_ID', pCobrancaId, True);
    t.setInt('ITEM_ID', pItemId, True);
    t.setData('DATA_VENCIMENTO', pDataVencimento);
    t.setInt('BANCO', pBanco);
    t.setInt('AGENCIA', pAgencia);
    t.setString('CONTA_CORRENTE', pContaCorrente);
    t.setInt('NUMERO_CHEQUE', pNumeroCheque);
    t.setDouble('VALOR_CHEQUE', pValorCheque);
    t.setString('NOME_EMITENTE', pNomeEmitente);
    t.setString('CPF_CNPJ_EMITENTE', pCpfCnpjEmitente);
    t.setString('TELEFONE_EMITENTE', pTelefoneEmitente);
    t.setInt('PARCELA', pParcela);
    t.setInt('NUMERO_PARCELAS', pNumeroParcelas);
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function BuscarContasRecBaixasPagtosChqs(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;
var
  i: Integer;
  t: TContasRecBaixasPagtosChq;
begin
  Result := nil;
  t := TContasRecBaixasPagtosChq.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordContasRecBaixasPagtosChq;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirContasRecBaixasPagtosChq(
  pConexao: TConexao;
  pBaixaId: Integer;
  pCobrancaId: Integer;
  pItemId: Integer
): RecRetornoBD;
var
  t: TContasRecBaixasPagtosChq;
begin
  Result.TeveErro := False;
  t := TContasRecBaixasPagtosChq.Create(pConexao);

  try
    t.setInt('BAIXA_ID', pBaixaId, True);
    t.setInt('COBRANCA_ID', pCobrancaId, True);
    t.setInt('ITEM_ID', pItemId, True);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
