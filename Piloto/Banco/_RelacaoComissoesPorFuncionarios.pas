unit _RelacaoComissoesPorFuncionarios;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsOrcamentosVendas;

function BuscarComissoes(
  pConexao: TConexao;
  const pComando: string;
  pCalcularComissaoAcumuladoRecFin: string = 'N';
  pCalcularComissaoAcumuladoRecPedido: string = 'N'
  ): TArray<RecComissoesPorFuncionarios>;

function BuscarComissoesParceiros(pConexao: TConexao; const pComando: string; pCalcularComissaoAcumuladoRecFin: string = 'N'): TArray<RecComissoesPorFuncionarios>;

implementation

function BuscarComissoes(
  pConexao: TConexao;
  const pComando: string;
  pCalcularComissaoAcumuladoRecFin: string = 'N';
  pCalcularComissaoAcumuladoRecPedido: string = 'N'
  ): TArray<RecComissoesPorFuncionarios>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  with vSql.SQL do begin
    Add('select ');
    Add('  COM.ID, ');
    Add('  COM.TIPO, ');
    Add('  COM.FUNCIONARIO_ID, ');
    Add('  COM.NOME_FUNCIONARIO, ');
    Add('  COM.DATA, ');
    Add('  COM.CADASTRO_ID, ');
    Add('  COM.NOME_CLIENTE, ');
    Add('  COM.VALOR_VENDA, ');
    Add('  COM.VALOR_BASE_COMISSAO, ');
    Add('  COM.TIPO_COMISSAO, ');
    Add('  COM.VALOR_COMISSAO, ');
    Add('  COM.VALOR_CUSTO_FINAL ');
    Add('from ');

    if pCalcularComissaoAcumuladoRecPedido = 'S' then
      Add('  VW_COMISSAO_ACU_FEC_PEDIDO COM ')
    else if pCalcularComissaoAcumuladoRecFin = 'S' then
      Add('  VW_COMISSAO_POR_FUNC_ACU COM ')
    else
      Add('  VW_COMISSAO_POR_FUNCIONARIOS COM ');

    Add(pComando);
  end;

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].Id                 := vSql.GetInt('ID');
      Result[i].Tipo               := vSql.GetString('TIPO');
      Result[i].FuncionarioId      := vSql.GetInt('FUNCIONARIO_ID');
      Result[i].NomeFuncionario    := vSql.GetString('NOME_FUNCIONARIO');
      Result[i].Data               := vSql.GetData('DATA');
      Result[i].CadastroId         := vSql.GetInt('CADASTRO_ID');
      Result[i].NomeCliente        := vSql.GetString('NOME_CLIENTE');
      Result[i].ValorVenda         := vSql.GetDouble('VALOR_VENDA');
      Result[i].BaseComissao       := vSql.GetDouble('VALOR_BASE_COMISSAO');
      Result[i].TipoComissao       := vSql.GetString('TIPO_COMISSAO');
      Result[i].PercentualComissao := vSql.GetDouble('VALOR_COMISSAO') / vSql.GetDouble('VALOR_BASE_COMISSAO') * 100;
      Result[i].ValorComissao      := vSql.GetDouble('VALOR_COMISSAO');
      Result[i].ValorCustoFinal    := vSql.GetDouble('VALOR_CUSTO_FINAL');

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function BuscarComissoesParceiros(pConexao: TConexao; const pComando: string; pCalcularComissaoAcumuladoRecFin: string = 'N'): TArray<RecComissoesPorFuncionarios>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  with vSql.SQL do begin
    Add('select ');
    Add('  COM.ID, ');
    Add('  COM.TIPO, ');
    Add('  COM.FUNCIONARIO_ID, ');
    Add('  COM.NOME_FUNCIONARIO, ');
    Add('  COM.PARCEIRO_ID, ');
    Add('  COM.NOME_PARCEIRO, ');
    Add('  COM.DATA, ');
    Add('  COM.CADASTRO_ID, ');
    Add('  COM.NOME_CLIENTE, ');
    Add('  COM.VALOR_VENDA, ');
    Add('  COM.VALOR_BASE_COMISSAO, ');
    Add('  COM.TIPO_COMISSAO, ');
    Add('  COM.VALOR_COMISSAO, ');
    Add('  COM.VALOR_CUSTO_FINAL ');
    Add('from ');
    Add('  VW_COMISSAO_POR_PARCEIROS COM ');

    Add(pComando);
  end;

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].Id                 := vSql.GetInt('ID');
      Result[i].Tipo               := vSql.GetString('TIPO');
      Result[i].FuncionarioId      := vSql.GetInt('FUNCIONARIO_ID');
      Result[i].NomeFuncionario    := vSql.GetString('NOME_FUNCIONARIO');
      Result[i].ParceiroId         := vSql.GetInt('PARCEIRO_ID');
      Result[i].NomeParceiro       := vSql.GetString('NOME_PARCEIRO');
      Result[i].Data               := vSql.GetData('DATA');
      Result[i].CadastroId         := vSql.GetInt('CADASTRO_ID');
      Result[i].NomeCliente        := vSql.GetString('NOME_CLIENTE');
      Result[i].ValorVenda         := vSql.GetDouble('VALOR_VENDA');
      Result[i].BaseComissao       := vSql.GetDouble('VALOR_BASE_COMISSAO');
      Result[i].TipoComissao       := vSql.GetString('TIPO_COMISSAO');
      Result[i].PercentualComissao := vSql.GetDouble('VALOR_COMISSAO') / vSql.GetDouble('VALOR_BASE_COMISSAO') * 100;
      Result[i].ValorComissao      := vSql.GetDouble('VALOR_COMISSAO');
      Result[i].ValorCustoFinal    := vSql.GetDouble('VALOR_CUSTO_FINAL');

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

end.
