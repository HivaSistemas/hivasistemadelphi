unit _PreEntradasCte;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, System.Classes, _PreEntrCteNfeReferenciadas;

{$M+}
type
  RecPreEntradasCte = record
    PreEntradaId: Integer;
    EmpresaId: Integer;
    CadastroId: Integer;
    CFOP: Integer;
    CfopEntradaId: string;
    ChaveAcessoCte: string;
    CnpjRemetente: string;
    RazaoSocialRemetente: string;
    InscricaoEstadualRemetente: string;
    CnpjEmitente: string;
    RazaoSocialEmitente: string;
    InscricaoEstadualEmitente: string;
    DataHoraEmissao: TDateTime;
    NumeroCte: Integer;
    SerieCte: string;
    ValorTotalCte: Double;
    BaseCalculoIcms: Double;
    PercentualIcms: Double;
    ValorIcms: Double;
    Status: string;
    ConhecimentoId: Integer;
    XmlTexto: string;
    StatusSefaz: string;
    StatusAltisAnalitico: string;
    StatusSEFAZAnalitico: string;
  end;

  TPreEntradasCte = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordPreEntradasCte: RecPreEntradasCte;
  end;

function AtualizarPreEntradasCte(
  pConexao: TConexao;
  pPreEntradaId: Integer;
  pEmpresaId: Integer;
  pCFOP: Integer;
  pChaveAcessoCte: string;
  pCnpjRemetente: string;
  pRazaoSocialRemetente: string;
  pInscricaoEstadualRemetente: string;
  pCnpjEmitente: string;
  pRazaoSocialEmitente: string;
  pInscricaoEstadualEmitente: string;
  pDataHoraEmissao: TDateTime;
  pNumeroCte: Integer;
  pSerieCte: string;
  pValorTotalCte: Double;
  pBaseCalculoIcms: Double;
  pPercentualIcms: Double;
  pValorIcms: Double;
  pXmlTexto: string;
  pChavesNFesReferenciadas: TArray<string>;
  pEmTransacao: Boolean
): RecRetornoBD;

function BuscarPreEntradasCte(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecPreEntradasCte>;

function AtualizarStatuCTeSefaz(pConexao: TConexao; pChaveCTe: string; pStatus: string; pEmTransacao: Boolean): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TPreEntradasCte }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where PRE_ENTRADA_ID = :P1'
    );
end;

constructor TPreEntradasCte.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PRE_ENTRADAS_CTE');

  FSql := 
    'select ' +
    '  PRE.PRE_ENTRADA_ID, ' +
    '  PRE.EMPRESA_ID, ' +
    '  CAD.CADASTRO_ID, ' +
    '  PRE.CFOP, ' +
    '  CFO.CFOP_CORRESPONDENTE_ENTRADA_ID, ' +
    '  PRE.CHAVE_ACESSO_CTE, ' +
    '  PRE.CNPJ_REMETENTE, ' +
    '  PRE.RAZAO_SOCIAL_REMETENTE, ' +
    '  PRE.INSCRICAO_ESTADUAL_REMETENTE, ' +
    '  PRE.CNPJ_EMITENTE, ' +
    '  PRE.RAZAO_SOCIAL_EMITENTE, ' +
    '  PRE.INSCRICAO_ESTADUAL_EMITENTE, ' +
    '  PRE.DATA_HORA_EMISSAO, ' +
    '  PRE.NUMERO_CTE, ' +
    '  PRE.SERIE_CTE, ' +
    '  PRE.VALOR_TOTAL_CTE, ' +
    '  PRE.BASE_CALCULO_ICMS, ' +
    '  PRE.PERCENTUAL_ICMS, ' +
    '  PRE.VALOR_ICMS, ' +
    '  PRE.STATUS, ' +
    '  PRE.CONHECIMENTO_ID, ' +
    '  PRE.XML_TEXTO, ' +
    '  PRE.STATUS_SEFAZ ' +
    'from ' +
    '  PRE_ENTRADAS_CTE PRE ' +

    'left join CADASTROS CAD ' +
    'on PRE.CNPJ_EMITENTE = CAD.CPF_CNPJ ' +

    'left join CFOP CFO ' +
    'on to_char(PRE.CFOP) || ''-0'' = CFO.CFOP_PESQUISA_ID ';

  setFiltros(getFiltros);

  AddColuna('PRE_ENTRADA_ID', True);
  AddColuna('EMPRESA_ID');
  AddColunaSL('CADASTRO_ID');
  AddColuna('CFOP');
  AddColunaSL('CFOP_CORRESPONDENTE_ENTRADA_ID');
  AddColuna('CHAVE_ACESSO_CTE');
  AddColuna('CNPJ_REMETENTE');
  AddColuna('RAZAO_SOCIAL_REMETENTE');
  AddColuna('INSCRICAO_ESTADUAL_REMETENTE');
  AddColuna('CNPJ_EMITENTE');
  AddColuna('RAZAO_SOCIAL_EMITENTE');
  AddColuna('INSCRICAO_ESTADUAL_EMITENTE');
  AddColuna('DATA_HORA_EMISSAO');
  AddColuna('NUMERO_CTE');
  AddColuna('SERIE_CTE');
  AddColuna('VALOR_TOTAL_CTE');
  AddColuna('BASE_CALCULO_ICMS');
  AddColuna('PERCENTUAL_ICMS');
  AddColuna('VALOR_ICMS');
  AddColunaSL('STATUS');
  AddColunaSL('CONHECIMENTO_ID');
  AddColunaSL('XML_TEXTO');
  AddColunaSL('STATUS_SEFAZ');
end;

function TPreEntradasCte.getRecordPreEntradasCte: RecPreEntradasCte;
begin
  Result.PreEntradaId               := getInt('PRE_ENTRADA_ID', True);
  Result.EmpresaId                  := getInt('EMPRESA_ID');
  Result.CadastroId                 := getInt('CADASTRO_ID');
  Result.CFOP                       := getInt('CFOP');
  Result.CfopEntradaId              := getString('CFOP_CORRESPONDENTE_ENTRADA_ID');
  Result.ChaveAcessoCte             := getString('CHAVE_ACESSO_CTE');
  Result.CnpjRemetente              := getString('CNPJ_REMETENTE');
  Result.RazaoSocialRemetente       := getString('RAZAO_SOCIAL_REMETENTE');
  Result.InscricaoEstadualRemetente := getString('INSCRICAO_ESTADUAL_REMETENTE');
  Result.CnpjEmitente               := getString('CNPJ_EMITENTE');
  Result.RazaoSocialEmitente        := getString('RAZAO_SOCIAL_EMITENTE');
  Result.InscricaoEstadualEmitente  := getString('INSCRICAO_ESTADUAL_EMITENTE');
  Result.DataHoraEmissao            := getData('DATA_HORA_EMISSAO');
  Result.NumeroCte                  := getInt('NUMERO_CTE');
  Result.SerieCte                   := getString('SERIE_CTE');
  Result.ValorTotalCte              := getDouble('VALOR_TOTAL_CTE');
  Result.BaseCalculoIcms            := getDouble('BASE_CALCULO_ICMS');
  Result.PercentualIcms             := getDouble('PERCENTUAL_ICMS');
  Result.ValorIcms                  := getDouble('VALOR_ICMS');
  Result.Status                     := getString('STATUS');
  Result.ConhecimentoId             := getInt('CONHECIMENTO_ID');
  Result.XmlTexto                   := getString('XML_TEXTO');
  Result.StatusSefaz                := getString('STATUS_SEFAZ');

  Result.StatusAltisAnalitico       := IIfStr(Result.Status = 'AEN', 'Aguard.entrada', 'Entr.realizada');
  Result.StatusSefazAnalitico       := IIfStr(Result.StatusSefaz = '1', 'Autorizada', 'Cancelada');
end;

function AtualizarPreEntradasCte(
  pConexao: TConexao;
  pPreEntradaId: Integer;
  pEmpresaId: Integer;
  pCFOP: Integer;
  pChaveAcessoCte: string;
  pCnpjRemetente: string;
  pRazaoSocialRemetente: string;
  pInscricaoEstadualRemetente: string;
  pCnpjEmitente: string;
  pRazaoSocialEmitente: string;
  pInscricaoEstadualEmitente: string;
  pDataHoraEmissao: TDateTime;
  pNumeroCte: Integer;
  pSerieCte: string;
  pValorTotalCte: Double;
  pBaseCalculoIcms: Double;
  pPercentualIcms: Double;
  pValorIcms: Double;
  pXmlTexto: string;
  pChavesNFesReferenciadas: TArray<string>;
  pEmTransacao: Boolean
): RecRetornoBD;
var
  t: TPreEntradasCte;
  vNovo: Boolean;
  vSeq: TSequencia;
  vSql: TConsulta;
  vExec: TExecucao;

  i: Integer;
  vRerencias: TPreEntrCteNfeReferenciadas;
begin
  Result.Iniciar;
  pConexao.setRotina('ATUALIZAR_PRE_ENT_CTE');

  vSql := TConsulta.Create(pConexao);
  vSql.Add('select ');
  vSql.Add('  count(*) ');
  vSql.Add('from ');
  vSql.Add('  PRE_ENTRADAS_CTE ');
  vSql.Add('where CHAVE_ACESSO_CTE = :P1 ');
  vSql.Pesquisar([pChaveAcessoCte]);

  // Se j� existe o CTe cadastrado saindo fora
  vNovo := vSql.GetInt(0) = 0;
  if not vNovo then begin
    vSql.Free;
    Exit;
  end;

  t := TPreEntradasCte.Create(pConexao);
  vRerencias := TPreEntrCteNfeReferenciadas.Create(pConexao);

  vExec := TExecucao.Create(pConexao);
  vExec.Add('update PRE_ENTRADAS_CTE set');
  vExec.Add('  XML_TEXTO = :P2 ');
  vExec.Add('where PRE_ENTRADA_ID = :P1 ');

  try
    if not pEmTransacao then
      pConexao.IniciarTransacao;

    vSeq := TSequencia.Create(pConexao, 'SEQ_PRE_ENTRADAS_CTE');
    pPreEntradaId := vSeq.getProximaSequencia;
    Result.AsInt := pPreEntradaId;
    vSeq.Free;

    t.setInt('PRE_ENTRADA_ID', pPreEntradaId, True);
    t.setInt('EMPRESA_ID', pEmpresaId);
    t.setInt('CFOP', pCFOP);
    t.setString('CHAVE_ACESSO_CTE', pChaveAcessoCte);
    t.setString('CNPJ_REMETENTE', pCnpjRemetente);
    t.setString('RAZAO_SOCIAL_REMETENTE', pRazaoSocialRemetente);
    t.setString('INSCRICAO_ESTADUAL_REMETENTE', pInscricaoEstadualRemetente);
    t.setString('CNPJ_EMITENTE', pCnpjEmitente);
    t.setString('RAZAO_SOCIAL_EMITENTE', pRazaoSocialEmitente);
    t.setString('INSCRICAO_ESTADUAL_EMITENTE', pInscricaoEstadualEmitente);
    t.setData('DATA_HORA_EMISSAO', pDataHoraEmissao);
    t.setInt('NUMERO_CTE', pNumeroCte);
    t.setString('SERIE_CTE', pSerieCte);
    t.setDouble('VALOR_TOTAL_CTE', pValorTotalCte);
    t.setDouble('BASE_CALCULO_ICMS', pBaseCalculoIcms);
    t.setDouble('PERCENTUAL_ICMS', pPercentualIcms);
    t.setDouble('VALOR_ICMS', pValorIcms);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    vExec.CarregarClob('P2', TStringStream.Create(pXmlTexto));
    vExec.Executar([pPreEntradaId]);

    for i := Low(pChavesNFesReferenciadas) to High(pChavesNFesReferenciadas) do begin
      vRerencias.setInt('PRE_ENTRADA_ID', pPreEntradaId, True);
      vRerencias.setInt('ITEM_ID', i, True);
      vRerencias.setString('CHAVE_ACESSO_NFE', pChavesNFesReferenciadas[i]);

      vRerencias.Inserir;
    end;

    if not pEmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pEmTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;

  vSql.Active := False;
  vRerencias.Free;
  vExec.Free;
  vSql.Free;
  t.Free;
end;

function BuscarPreEntradasCte(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecPreEntradasCte>;
var
  i: Integer;
  t: TPreEntradasCte;
begin
  Result := nil;
  t := TPreEntradasCte.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordPreEntradasCte;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function AtualizarStatuCTeSefaz(pConexao: TConexao; pChaveCTe: string; pStatus: string; pEmTransacao: Boolean): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_STATUS_PRE_ENT_CTE');

  vExec := TExecucao.Create(pConexao);
  vExec.Add('update PRE_ENTRADAS_CTE set');
  vExec.Add('  STATUS = :P2 ');
  vExec.Add('where PRE_ENTRADA_ID = :P1 ');

  try
    if not pEmTransacao then
      pConexao.IniciarTransacao;

    vExec.Executar([pChaveCTe, pStatus]);

    if not pEmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pEmTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
end;

end.
