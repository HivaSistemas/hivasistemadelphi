unit _ContasPagarBaixasCreditos;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TContasPagarBaixasCreditos = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  end;

implementation

{ TOrcamentosCreditos }

constructor TContasPagarBaixasCreditos.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONTAS_PAGAR_BAIXAS_CREDITOS');

  FSql :=
    '';

  AddColuna('BAIXA_ID', True);
  AddColuna('RECEBER_ID', True);
end;

end.
