unit _DevolucoesEntradasNfItens;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecDevolucoesEntradasNfItens = record
    DevolucaoId: Integer;
    ProdutoId: Integer;
    NomeProduto: string;
    NomeMarca: string;
    ItemId: Integer;
    ValorTotal: Double;
    PrecoUnitario: Double;
    Quantidade: Double;
    ValorTotalDesconto: Double;
    ValorTotalOutrasDespesas: Double;
    BaseCalculoIcms: Double;
    PercentualIcms: Double;
    ValorIcms: Double;
    BaseCalculoIcmsSt: Double;
    PercentualIcmsSt: Double;
    ValorIcmsSt: Double;
    ValorIpi: Double;
    PercentualIpi: Double;
    PesoUnitario: Double;
    UnidadeEntradaId: string;
    LocalId: Integer;
  end;

  TDevolucoesEntradasNfItens = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordDevolucoesEntradasNfItens: RecDevolucoesEntradasNfItens;
  end;

function BuscarDevolucoesEntradasNfItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecDevolucoesEntradasNfItens>;

function BuscarDevolucoesEntradasNfItensComando(pConexao: TConexao; pComando: string): TArray<RecDevolucoesEntradasNfItens>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TDevolucoesEntradasNfItens }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where DEV.DEVOLUCAO_ID = :P1 '
    );
end;

constructor TDevolucoesEntradasNfItens.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'DEVOLUCOES_ENTRADAS_NF_ITENS');

  FSql := 
    'select ' +
    '  ITE.DEVOLUCAO_ID, ' +
    '  ITE.PRODUTO_ID, ' +
    '  PRO.NOME as NOME_PRODUTO, ' +
    '  MAR.NOME as NOME_MARCA, ' +
    '  ITE.ITEM_ID, ' +
    '  ITE.VALOR_TOTAL, ' +
    '  ITE.PRECO_UNITARIO, ' +
    '  ITE.QUANTIDADE, ' +
    '  ITE.VALOR_TOTAL_DESCONTO, ' +
    '  ITE.VALOR_TOTAL_OUTRAS_DESPESAS, ' +
    '  ITE.BASE_CALCULO_ICMS, ' +
    '  EIT.PERCENTUAL_ICMS, ' +
    '  ITE.VALOR_ICMS, ' +
    '  ITE.BASE_CALCULO_ICMS_ST, ' +
    '  EIT.PERCENTUAL_ICMS_ST, ' +
    '  ITE.VALOR_ICMS_ST, ' +
    '  ITE.VALOR_IPI, ' +
    '  EIT.PERCENTUAL_IPI, ' +
    '  ITE.PESO_UNITARIO, ' +
    '  EIT.UNIDADE_ENTRADA_ID, ' +
    '  EIT.LOCAL_ID ' +
    'from ' +
    '  DEVOLUCOES_ENTRADAS_NF_ITENS ITE ' +

    'inner join PRODUTOS PRO ' +
    'on ITE.PRODUTO_ID = PRO.PRODUTO_ID ' +

    'inner join MARCAS MAR ' +
    'on PRO.MARCA_ID = MAR.MARCA_ID ' +

    'inner join DEVOLUCOES_ENTRADAS_NOTAS_FISC DEV ' +
    'on ITE.DEVOLUCAO_ID = DEV.DEVOLUCAO_ID ' +

    'inner join ENTRADAS_NOTAS_FISCAIS_ITENS EIT ' +
    'on DEV.ENTRADA_ID = EIT.ENTRADA_ID ' +
    'and ITE.ITEM_ID = EIT.ITEM_ID ';

  setFiltros(getFiltros);

  AddColuna('DEVOLUCAO_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('PRODUTO_ID');
  AddColunaSL('NOME_PRODUTO');
  AddColunaSL('NOME_MARCA');
  AddColuna('VALOR_TOTAL');
  AddColuna('PRECO_UNITARIO');
  AddColuna('QUANTIDADE');
  AddColuna('VALOR_TOTAL_DESCONTO');
  AddColuna('VALOR_TOTAL_OUTRAS_DESPESAS');
  AddColuna('BASE_CALCULO_ICMS');
  AddColunaSL('PERCENTUAL_ICMS');
  AddColuna('VALOR_ICMS');
  AddColuna('BASE_CALCULO_ICMS_ST');
  AddColunaSL('PERCENTUAL_ICMS_ST');
  AddColuna('VALOR_ICMS_ST');
  AddColuna('VALOR_IPI');
  AddColunaSL('PERCENTUAL_IPI');
  AddColuna('PESO_UNITARIO');
  AddColunaSL('UNIDADE_ENTRADA_ID');
  AddColuna('LOCAL_ID');
end;

function TDevolucoesEntradasNfItens.getRecordDevolucoesEntradasNfItens: RecDevolucoesEntradasNfItens;
begin
  Result.DevolucaoId                := getInt('DEVOLUCAO_ID', True);
  Result.ProdutoId                  := getInt('PRODUTO_ID');
  Result.NomeProduto                := getString('NOME_PRODUTO');
  Result.NomeMarca                  := getString('NOME_MARCA');
  Result.ItemId                     := getInt('ITEM_ID', True);
  Result.LocalId                    := getInt('LOCAL_ID');
  Result.ValorTotal                 := getDouble('VALOR_TOTAL');
  Result.PrecoUnitario              := getDouble('PRECO_UNITARIO');
  Result.Quantidade                 := getDouble('QUANTIDADE');
  Result.ValorTotalDesconto         := getDouble('VALOR_TOTAL_DESCONTO');
  Result.ValorTotalOutrasDespesas   := getDouble('VALOR_TOTAL_OUTRAS_DESPESAS');
  Result.BaseCalculoIcms            := getDouble('BASE_CALCULO_ICMS');
  Result.PercentualIcms             := getDouble('PERCENTUAL_ICMS');
  Result.ValorIcms                  := getDouble('VALOR_ICMS');
  Result.BaseCalculoIcmsSt          := getDouble('BASE_CALCULO_ICMS_ST');
  Result.PercentualIcmsSt           := getDouble('PERCENTUAL_ICMS_ST');
  Result.ValorIcmsSt                := getDouble('VALOR_ICMS_ST');
  Result.ValorIpi                   := getDouble('VALOR_IPI');
  Result.PercentualIpi              := getDouble('PERCENTUAL_IPI');
  Result.PesoUnitario               := getDouble('PESO_UNITARIO');
  Result.UnidadeEntradaId           := getString('UNIDADE_ENTRADA_ID');
end;

function BuscarDevolucoesEntradasNfItens(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecDevolucoesEntradasNfItens>;
var
  i: Integer;
  t: TDevolucoesEntradasNfItens;
begin
  Result := nil;
  t := TDevolucoesEntradasNfItens.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordDevolucoesEntradasNfItens;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarDevolucoesEntradasNfItensComando(pConexao: TConexao; pComando: string): TArray<RecDevolucoesEntradasNfItens>;
var
  i: Integer;
  t: TDevolucoesEntradasNfItens;
begin
  Result := nil;
  t := TDevolucoesEntradasNfItens.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordDevolucoesEntradasNfItens;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
