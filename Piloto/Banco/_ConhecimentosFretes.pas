unit _ConhecimentosFretes;

interface

uses
  _OperacoesBancoDados, _RecordsEspeciais, _Conexao, System.SysUtils, System.Variants, _BibliotecaGenerica, _ConhecimentosFretesItens, _ConhecFretesFinanceiros,
  _RecordsFinanceiros;

{$M+}
type
  RecConhecimentosFretes = record
    ConhecimentoId: Integer;
    EmpresaId: Integer;
    NomeEmpresa: string;
    TransportadoraId: Integer;
    NomeTransportadora: string;
    Numero: Integer;
    Modelo: string;
    Serie: string;
    DataHoraCadastro: TDateTime;
    BaseCalculoIcms: Double;
    PercentualIcms: Double;
    ValorIcms: Double;
    ValorFrete: Double;
    ChaveConhecimento: string;
    CfopId: string;
    NomeCfop: string;
    DataEmissao: TDateTime;
    DataContabil: TDateTime;
    DataPrimeiroVencimento: TDateTime;
    UsuarioCadastroId: Integer;
    NomeUsuarioCadastro: string;
    TipoRateioConhecimento: string;
  end;

  TConhecimentosFretes = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordConhecimento: RecConhecimentosFretes;
  end;

function AtualizarConhecimento(
  pConexao: TConexao;
  pConhecimentoId: Integer;
  pEmpresaId: Integer;
  pTransportadoraId: Integer;
  pNumero: Integer;
  pModelo: string;
  pSerie: string;
  pBaseCalculoIcms: Double;
  pPercentualIcms: Double;
  pValorIcms: Double;
  pValorFrete: Double;
  pChaveConhecimento: string;
  pCfopId: string;
  pDataEmissao: TDateTime;
  pDataContabil: TDateTime;
  pDataPrimeiroVencimento: TDateTime;
  pTipoRateioConhecimento: string;
  pNotas: TArray<RecConhecimentosFretesItens>;
  pFinanceiros: TArray<RecTitulosFinanceiros>
): RecRetornoBD;

function BuscarConhecimento(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant
): TArray<RecConhecimentosFretes>;

function BuscarConhecimentoComando(pConexao: TConexao; pComando: string): TArray<RecConhecimentosFretes>;

function ExcluirConhecimento(
  pConexao: TConexao;
  pConhecimentoId: Integer
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TConhecimentosFretes }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CONHECIMENTO_ID = :P1 '
    );
end;

constructor TConhecimentosFretes.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONHECIMENTOS_FRETES');

  FSql :=
    'select ' +
    '  CON.CONHECIMENTO_ID, ' +
    '  CON.EMPRESA_ID, ' +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA, ' +
    '  CON.TRANSPORTADORA_ID, ' +
    '  CAD.NOME_FANTASIA as NOME_TRANSPORTADORA, ' +
    '  CON.NUMERO, ' +
    '  CON.MODELO, ' +
    '  CON.SERIE, ' +
    '  CON.DATA_HORA_CADASTRO, ' +
    '  CON.BASE_CALCULO_ICMS, ' +
    '  CON.PERCENTUAL_ICMS, ' +
    '  CON.VALOR_ICMS, ' +
    '  CON.VALOR_FRETE, ' +
    '  CON.CHAVE_CONHECIMENTO, ' +
    '  CON.CFOP_ID, ' +
    '  CFO.NOME as NOME_CFOP, ' +
    '  CON.DATA_EMISSAO, ' +
    '  CON.DATA_CONTABIL, ' +
    '  CON.DATA_PRIMEIRO_VENCIMENTO, ' +
    '  CON.TIPO_RATEIO_CONHECIMENTO, ' +
    '  CON.USUARIO_CADASTRO_ID, ' +
    '  FCA.NOME as NOME_USUARIO_CADASTRO ' +
    'from ' +
    '  CONHECIMENTOS_FRETES CON ' +

    'inner join EMPRESAS EMP ' +
    'on CON.EMPRESA_ID = EMP.EMPRESA_ID ' +

    'inner join CADASTROS CAD ' +
    'on CON.TRANSPORTADORA_ID = CAD.CADASTRO_ID ' +

    'inner join CFOP CFO ' +
    'on CON.CFOP_ID = CFO.CFOP_PESQUISA_ID ' +

    'inner join FUNCIONARIOS FCA ' +
    'on CON.USUARIO_CADASTRO_ID = FCA.FUNCIONARIO_ID ';

  SetFiltros(GetFiltros);

  AddColuna('CONHECIMENTO_ID', True);
  AddColuna('EMPRESA_ID');
  AddColunaSL('NOME_EMPRESA');
  AddColuna('TRANSPORTADORA_ID');
  AddColunaSL('NOME_TRANSPORTADORA');
  AddColuna('NUMERO');
  AddColuna('MODELO');
  AddColuna('SERIE');
  AddColunaSL('DATA_HORA_CADASTRO');
  AddColuna('BASE_CALCULO_ICMS');
  AddColuna('PERCENTUAL_ICMS');
  AddColuna('VALOR_ICMS');
  AddColuna('VALOR_FRETE');
  AddColuna('CHAVE_CONHECIMENTO');
  AddColuna('CFOP_ID');
  AddColunaSL('NOME_CFOP');
  AddColuna('DATA_EMISSAO');
  AddColuna('DATA_CONTABIL');
  AddColuna('DATA_PRIMEIRO_VENCIMENTO');
  AddColuna('TIPO_RATEIO_CONHECIMENTO');
  AddColunaSL('USUARIO_CADASTRO_ID');
  AddColunaSL('NOME_USUARIO_CADASTRO');
end;

function TConhecimentosFretes.GetRecordConhecimento: RecConhecimentosFretes;
begin
  Result.ConhecimentoId    := GetInt('CONHECIMENTO_ID', True);
  Result.EmpresaId         := GetInt('EMPRESA_ID');
  Result.NomeEmpresa       := GetString('NOME_EMPRESA');
  Result.TransportadoraId  := GetInt('TRANSPORTADORA_ID');
  Result.NomeTransportadora:= GetString('NOME_TRANSPORTADORA');
  Result.Numero            := GetInt('NUMERO');
  Result.Modelo            := GetString('MODELO');
  Result.Serie             := GetString('SERIE');
  Result.DataHoraCadastro  := GetData('DATA_HORA_CADASTRO');
  Result.BaseCalculoIcms   := getDouble('BASE_CALCULO_ICMS');
  Result.PercentualIcms    := getDouble('PERCENTUAL_ICMS');
  Result.ValorIcms         := getDouble('VALOR_ICMS');
  Result.ValorFrete        := getDouble('VALOR_FRETE');
  Result.ChaveConhecimento := GetString('CHAVE_CONHECIMENTO');
  Result.CfopId            := getString('CFOP_ID');
  Result.NomeCfop          := getString('NOME_CFOP');
  Result.DataEmissao       := GetData('DATA_EMISSAO');
  Result.DataContabil      := GetData('DATA_CONTABIL');
  Result.DataPrimeiroVencimento := GetData('DATA_PRIMEIRO_VENCIMENTO');
  Result.TipoRateioConhecimento := getString('TIPO_RATEIO_CONHECIMENTO');
  Result.UsuarioCadastroId      := getInt('USUARIO_CADASTRO_ID');
  Result.NomeUsuarioCadastro    := getString('NOME_USUARIO_CADASTRO');
end;

function AtualizarConhecimento(
  pConexao: TConexao;
  pConhecimentoId: Integer;
  pEmpresaId: Integer;
  pTransportadoraId: Integer;
  pNumero: Integer;
  pModelo: string;
  pSerie: string;
  pBaseCalculoIcms: Double;
  pPercentualIcms: Double;
  pValorIcms: Double;
  pValorFrete: Double;
  pChaveConhecimento: string;
  pCfopId: string;
  pDataEmissao: TDateTime;
  pDataContabil: TDateTime;
  pDataPrimeiroVencimento: TDateTime;
  pTipoRateioConhecimento: string;
  pNotas: TArray<RecConhecimentosFretesItens>;
  pFinanceiros: TArray<RecTitulosFinanceiros>
): RecRetornoBD;
var
  i: Integer;
  vNovo: Boolean;
  t: TConhecimentosFretes;

  vExec: TExecucao;
  vItens: TConhecimentosFretesItens;
  vFinanc: TConhecFretesFinanceiros;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_CONHEC_FRETE');

  vExec := TExecucao.Create(pConexao);
  t := TConhecimentosFretes.Create(pConexao);
  vItens := TConhecimentosFretesItens.Create(pConexao);
  vFinanc := TConhecFretesFinanceiros.Create(pConexao);

  vNovo := pConhecimentoId = 0;
  if vNovo then begin
    pConhecimentoId := TSequencia.Create(pConexao, 'SEQ_CONHECIMENTO_ID').GetProximaSequencia;
    result.AsInt := pConhecimentoId;
  end;

  try
    t.SetInt('CONHECIMENTO_ID', pConhecimentoId, True);
    t.setInt('EMPRESA_ID', pEmpresaId);
    t.setInt('TRANSPORTADORA_ID', pTransportadoraId);
    t.setInt('NUMERO', pNumero);
    t.SetString('MODELO', pModelo);
    t.SetString('SERIE', pSerie);
    t.setDouble('BASE_CALCULO_ICMS', pBaseCalculoIcms);
    t.setDouble('PERCENTUAL_ICMS', pPercentualIcms);
    t.setDouble('VALOR_ICMS', pValorIcms);
    t.setDouble('VALOR_FRETE', pValorFrete);
    t.SetString('CHAVE_CONHECIMENTO', pChaveConhecimento);
    t.SetString('TIPO_RATEIO_CONHECIMENTO', pTipoRateioConhecimento);
    t.setString('CFOP_ID', pCfopId);
    t.setData('DATA_EMISSAO', pDataEmissao);
    t.setData('DATA_CONTABIL', pDataContabil);
    t.setData('DATA_PRIMEIRO_VENCIMENTO', pDataPrimeiroVencimento);

    pConexao.IniciarTransacao;

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    if not vNovo then begin
      vExec.SQL.Clear;
      vExec.Add('delete from CONHECIMENTOS_FRETES_ITENS');
      vExec.Add('where CONHECIMENTO_ID = :P1 ');
      vExec.Executar([pConhecimentoId]);
    end;

    for i := Low(pNotas) to High(pNotas) do begin
      vItens.SetInt('CONHECIMENTO_ID', pConhecimentoId, True);
      vItens.setInt('ITEM_ID', i + 1, True);
      vItens.setInt('FORNECEDOR_NOTA_ID', pNotas[i].FornecedorNotaId);
      vItens.setInt('NUMERO_NOTA', pNotas[i].NumeroNota);
      vItens.SetString('MODELO_NOTA', pNotas[i].ModeloNota);
      vItens.SetString('SERIE_NOTA', pNotas[i].SerieNota);
      vItens.setData('DATA_EMISSAO_NOTA', pNotas[i].DataEmissaoNota);
      vItens.setDouble('PESO_NOTA', pNotas[i].PesoNota);
      vItens.setDouble('QUANTIDADADE_NOTA', pNotas[i].QuantidadeNota);
      vItens.setDouble('VALOR_TOTAL_NOTA', pNotas[i].ValorTotalNota);
      vItens.setDouble('INDICE', pNotas[i].Indice);

      vItens.Inserir;
    end;

    if not vNovo then begin
      vExec.SQL.Clear;
      vExec.Add('delete from CONHEC_FRETES_FINANCEIROS');
      vExec.Add('where CONHECIMENTO_ID = :P1 ');
      vExec.Executar([pConhecimentoId]);
    end;

    for i := Low(pFinanceiros) to High(pFinanceiros) do begin
      vFinanc.SetInt('CONHECIMENTO_ID', pConhecimentoId, True);
      vFinanc.setInt('ITEM_ID', pFinanceiros[i].ItemId, True);
      vFinanc.setInt('COBRANCA_ID', pFinanceiros[i].CobrancaId);
      vFinanc.setData('DATA_VENCIMENTO', pFinanceiros[i].DataVencimento);
      vFinanc.setInt('PARCELA', pFinanceiros[i].Parcela);
      vFinanc.setInt('NUMERO_PARCELAS', pFinanceiros[i].NumeroParcelas);
      vFinanc.setDouble('VALOR', pFinanceiros[i].Valor);
      vFinanc.setString('DOCUMENTO', pFinanceiros[i].Documento);
      vFinanc.setStringN('NOSSO_NUMERO', pFinanceiros[i].NossoNumero);
      vFinanc.setStringN('CODIGO_BARRAS', pFinanceiros[i].CodigoBarras);

      vFinanc.Inserir;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vFinanc.Free;
  vItens.Free;
  t.Free;
end;

function BuscarConhecimento(
  pConexao: TConexao;
  pIndice: Integer;
  pFiltros: array of Variant
): TArray<RecConhecimentosFretes>;
var
  i: Integer;
  t: TConhecimentosFretes;
begin
  Result := nil;
  t := TConhecimentosFretes.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordConhecimento;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarConhecimentoComando(pConexao: TConexao; pComando: string): TArray<RecConhecimentosFretes>;
var
  i: Integer;
  t: TConhecimentosFretes;
begin
  Result := nil;
  t := TConhecimentosFretes.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordConhecimento;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirConhecimento(
  pConexao: TConexao;
  pConhecimentoId: Integer
): RecRetornoBD;
var
  t: TConhecimentosFretes;
begin
  result.TeveErro := False;
  t := TConhecimentosFretes.Create(pConexao);
  try
    pConexao.IniciarTransacao;

    t.SetInt('CONHECIMENTO_ID', pConhecimentoId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
