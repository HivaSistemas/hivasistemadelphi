unit _ContasRecBaixasCreditos;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TContasRecBaixasCreditos = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  end;

implementation

{ TOrcamentosCreditos }

constructor TContasRecBaixasCreditos.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONTAS_REC_BAIXAS_CREDITOS');

  FSql :=
    '';

  AddColuna('BAIXA_ID', True);
  AddColuna('PAGAR_ID', True);
end;

end.
