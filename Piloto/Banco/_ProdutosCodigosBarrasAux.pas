unit _ProdutosCodigosBarrasAux;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecProdutosCodigosBarrasAux = record
    ProdutoId: Integer;
    CodigoBarras: string;
  end;

  TProdutosCodigosBarrasAux = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordProdutosCodigosBarrasAux: RecProdutosCodigosBarrasAux;
  end;

function BuscarProdutosCodigosBarrasAux(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecProdutosCodigosBarrasAux>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TProdutosCodigosBarrasAux }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where PRODUTO_ID = :P1 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CODIGO_BARRAS = :P1 '
    );
end;

constructor TProdutosCodigosBarrasAux.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PRODUTOS_CODIGOS_BARRAS_AUX');

  FSql := 
    'select ' +
    '  PRODUTO_ID, ' +
    '  CODIGO_BARRAS ' +
    'from ' +
    '  PRODUTOS_CODIGOS_BARRAS_AUX ';

  setFiltros(getFiltros);

  AddColuna('PRODUTO_ID', True);
  AddColuna('CODIGO_BARRAS', True);
end;

function TProdutosCodigosBarrasAux.getRecordProdutosCodigosBarrasAux: RecProdutosCodigosBarrasAux;
begin
  Result.ProdutoId                  := getInt('PRODUTO_ID', True);
  Result.CodigoBarras               := getString('CODIGO_BARRAS', True);
end;

function BuscarProdutosCodigosBarrasAux(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecProdutosCodigosBarrasAux>;
var
  i: Integer;
  t: TProdutosCodigosBarrasAux;
begin
  Result := nil;
  t := TProdutosCodigosBarrasAux.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordProdutosCodigosBarrasAux;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
