unit _DevolucoesItensRetiradas;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, _Biblioteca;

{$M+}
type
  RecDevolucoesItensRetiradas = record
    DevolucaoId: Integer;
    ItemId: Integer;
    Lote: string;
    LocalDevolucaoId: Integer;
    RetiradaId: Integer;
    Quantidade: Double;
  end;

  RecRetiradasEntregas = record
    Id: Integer;
    TipoMovimentoAnalitico: string;
    QuantidadeProdutos: Double;
  end;

  TDevolucoesItensRetiradas = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordDevolucoesItensRet: RecDevolucoesItensRetiradas;
  end;

function AtualizarDevolucoesItensRetiradas(
  pConexao: TConexao;
  pDevolucaoId: Integer;
  pLocalDevolucaoId: Integer;
  pItens: TArray<RecDevolucoesItensRetiradas>;
  pEmTransacao: Boolean
): RecRetornoBD;

function BuscarDevolucoesItensRet(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecDevolucoesItensRetiradas>;

function BuscarDevolucoesItensComandoEnt(pConexao: TConexao; pComando: string): TArray<RecDevolucoesItensRetiradas>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TTDevolucoesItens }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ITE.DEVOLUCAO_ID = :P1 '
    );
end;

constructor TDevolucoesItensRetiradas.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'DEVOLUCOES_ITENS_RETIRADAS');

  FSql :=
    'select ' +
    '  ITE.DEVOLUCAO_ID, ' +
    '  ITE.ITEM_ID, ' +
    '  ITE.LOTE, ' +
    '  ITE.LOCAL_DEVOLUCAO_ID, ' +
    '  ITE.RETIRADA_ID, ' +
    '  ITE.QUANTIDADE ' +
    'from ' +
    '  DEVOLUCOES_ITENS_RETIRADAS ITE ';

  setFiltros(getFiltros);

  AddColuna('DEVOLUCAO_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('LOTE', True);
  AddColuna('RETIRADA_ID', True);
  AddColuna('LOCAL_DEVOLUCAO_ID');
  AddColuna('QUANTIDADE');
end;

function TDevolucoesItensRetiradas.getRecordDevolucoesItensRet: RecDevolucoesItensRetiradas;
begin
  Result.DevolucaoId      := getInt('DEVOLUCAO_ID', True);
  Result.ItemId           := getInt('ITEM_ID', True);
  Result.Lote             := getString('LOTE', True);
  Result.RetiradaId       := getInt('RETIRADA_ID', True);
  Result.LocalDevolucaoId := getInt('LOCAL_DEVOLUCAO_ID');
  Result.Quantidade       := getDouble('QUANTIDADE');
end;

function AtualizarDevolucoesItensRetiradas(
  pConexao: TConexao;
  pDevolucaoId: Integer;
  pLocalDevolucaoId: Integer;
  pItens: TArray<RecDevolucoesItensRetiradas>;
  pEmTransacao: Boolean
): RecRetornoBD;
var
  i: Integer;
  vItem: TDevolucoesItensRetiradas;
begin
  Result.Iniciar;
  vItem := TDevolucoesItensRetiradas.Create(pConexao);
  pConexao.SetRotina('ATUALIZAR_DEVOLUCAO_ITENS_ENT');

  try
    if not pEmTransacao then
      pConexao.IniciarTransacao;

    for i := Low(pItens) to High(pItens) do begin
      vItem.SetInt('DEVOLUCAO_ID', pDevolucaoId, True);
      vItem.SetInt('ITEM_ID', pItens[i].ItemId, True);
      vItem.setIntN('RETIRADA_ID', pItens[i].RetiradaId, True);
      vItem.setString('LOTE', pItens[i].Lote, True);

      vItem.SetInt('LOCAL_DEVOLUCAO_ID', pLocalDevolucaoId);
      vItem.setDouble('QUANTIDADE', pItens[i].Quantidade);

      vItem.Inserir;
    end;

    if not pEmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pEmTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;
  vItem.Free;
end;

function BuscarDevolucoesItensRet(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecDevolucoesItensRetiradas>;
var
  i: Integer;
  t: TDevolucoesItensRetiradas;
begin
  Result := nil;
  t := TDevolucoesItensRetiradas.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordDevolucoesItensRet;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarDevolucoesItensComandoEnt(pConexao: TConexao; pComando: string): TArray<RecDevolucoesItensRetiradas>;
var
  i: Integer;
  t: TDevolucoesItensRetiradas;
begin
  Result := nil;
  t := TDevolucoesItensRetiradas.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordDevolucoesItensRet;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
