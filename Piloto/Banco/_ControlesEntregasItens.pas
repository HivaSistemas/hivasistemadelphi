unit _ControlesEntregasItens;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils;

{$M+}
type
  RecControlesEntregasItens = record
  public
    ControleEntregaId: Integer;
    EntregaId: Integer;
  end;

  TControlesEntregasItens = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordControlesItens: RecControlesEntregasItens;
  end;

function BuscarControles(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecControlesEntregasItens>;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TControlesEntregasItens }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CON.CONTROLE_ENTREGA_ID = :P1'
    );
end;

constructor TControlesEntregasItens.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONTROLES_ENTREGAS_ITENS');

  FSql :=
    'select ' +
    '  ITE.CONTROLE_ENTREGA_ID, ' +
    '  ITE.ENTREGA_ID ' +
    'from ' +
    '  CONTROLES_ENTREGAS_ITENS ITE ';

  SetFiltros(GetFiltros);

  AddColuna('CONTROLE_ENTREGA_ID', True);
  AddColuna('ENTREGA_ID', True);
end;

function TControlesEntregasItens.GetRecordControlesItens: RecControlesEntregasItens;
begin
  Result.ControleEntregaId := GetInt('CONTROLE_ENTREGA_ID', True);
  Result.EntregaId         := GetInt('EMPRESA_ID', True);
end;

function BuscarControles(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecControlesEntregasItens>;
var
  i: Integer;
  t: TControlesEntregasItens;
begin
  Result := nil;
  t := TControlesEntregasItens.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordControlesItens;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
