unit _Parametros;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TParametros = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordParametros: RecParametros;
  end;

function AtualizarParametros(
  pConexao: TConexao;
  pCadastroConsumidorFinalId: Integer;
  pIniciarVendaConsumidorFinal: string;
  pUtilizarAnaliseCusto: string;
  pQtdeMensagensObrigarLeitura: Integer;
  pQuantidadeDiasValidadeSenha: Integer;
  pMotivoAjuEstBxCompraId: Integer;
  pTipoCobrancaGeracaoCredId: Integer;
  pTipoCobrancaGerCredImpId: Integer;
  pTipoCobRecebEntregaId: Integer;
  pTipoCobAcumulativoId: Integer;
  pTipoCobAdiantamentoAcuId: Integer;
  pTipoCobAdiantamentoFinId: Integer;
  pTipoCobFechamentoTurnoId: Integer;
  pBloquearCreditoDevolucao: string;
  pBloquearCreditoPagarManual: string;
  pQtdeSegTravarAltisOcioso: Integer;
  pQtdeMaximaDiasDevolucao: Integer;
  pDefinirLocalManual: string;
  pPreencherLocaisAutomaticamente: string;
  pSomenteLocaisEstoquePositivo: string;
  pAplicarIndiceDeducao: string;
  pEntradaFutura : string;
  pVendaRapida : string;
  pUtilMarketPlace: string;
  pUtilEnderecoEstoque : string;
  pControleEntrega : string;
  pImprimeListaSeparacaoEntrega: string;
  pTipoCobEntradaNfeXmlId: Integer;
  pUtilizaDirecionamentoPorLocais: string;
  pQtdeViasReciboPagamento: Integer;
  pEmitirNotaAcumuladoFechamentoPedido: string;
  pUtilizaConfirmacaoTransferenciaLocais: string;
  pAtualizarPrecoVendaEntradaNota: string;
  pDeletarNFPendenteEmissao: string;
  pSmtpServidor: string;
  pSmtpUsuario: string;
  pSmtpSenha: string;
  pSmtpPorta: integer;
  pSmtpAutenticacao: string;
  pSmtpReqAutenticacao: string;
  pAtualizarCustoProdutosAguaradndoChegada: string;
  pCalcularComissaoAcumuladoRecFin: string;
  pDataInicioUtilizacao: TDateTime;
  pHabilitarDescontoItemImpresaoOrcamento: string;
  pAjusteEstoqueReal: string;
  pSenhaPedidosbloqueados: string;
  pOrcamentoAmbiente: string;
  pTipoComprovante: string;
  pUtilizaEmissaoBoleto: string;
  pUtilizaDRE: string;
  pCalcularComissaoAcumuladoRecPedido: string;
  pCalcularAdicionalComissaoPorMetaFuncionario: string;
  pUtilizaControleManifesto: string;
  pUrlIntegracaoWeb: string;
  pDataUltimaIntegracaoWeb: TDateTime;
  pIntervaloIntegracaoWeb: integer;
  pPortaIntegracaoWeb: integer
): RecRetornoBD;

function BuscarParametros(pConexao: TConexao): RecParametros;

function getCompilacaoSistemaBanco(pConexao: TConexao): string;
function atualizarCompilacaoSistemaBanco(pConexao: TConexao; pNovaCompilacao: string): RecRetornoBD;
function FinalizarAnoMes(pConexao: TConexao): RecRetornoBD;

implementation

{ TParametros }

constructor TParametros.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PARAMETROS');

  FSql :=
    'select ' +
    '  CADASTRO_CONSUMIDOR_FINAL_ID, ' +
    '  INICIAR_VENDA_CONSUMIDOR_FINAL, ' +
    '  UTILIZAR_ANALISE_CUSTO, ' +
    '  QTDE_MENSAGENS_OBRIGAR_LEITURA, ' +
    '  QUANTIDADE_DIAS_VALIDADE_SENHA, ' +
    '  MOTIVO_AJU_EST_BX_COMPRA_ID, ' +
    '  TIPO_COBRANCA_GERACAO_CRED_ID, ' +
    '  TIPO_COBRANCA_GER_CRED_IMP_ID, ' +
    '  TIPO_COB_RECEB_ENTREGA_ID, ' +
    '  TIPO_COB_ACUMULATIVO_ID, ' +
    '  TIPO_COB_ADIANTAMENTO_ACU_ID, ' +
    '  TIPO_COB_ADIANTAMENTO_FIN_ID, ' +
    '  TIPO_COB_FECHAMENTO_TURNO_ID, ' +
    '  BLOQUEAR_CREDITO_DEVOLUCAO, ' +
    '  BLOQUEAR_CREDITO_PAGAR_MANUAL, ' +
    '  UTIL_MODULO_PRE_ENTRADAS, ' +
    '  QTDE_SEG_TRAVAR_ALTIS_OCIOSO, ' +
    '  QTDE_MAXIMA_DIAS_DEVOLUCAO, ' +
    '  DEFINIR_LOCAL_MANUAL, ' +
    '  PREENCHER_LOCAIS_AUTO, ' +
    '  SOMENTE_LOCAL_EST_POSITIVO, ' +
    '  APLICAR_INDICE_DEDUCAO, ' +
    '  UTILIZA_VENDA_RAPIDA, ' +
    '  UTILIZA_MARKETPLACE, ' +
    '  UTILIZA_CONTROLE_ENTREGA, ' +
    '  UTILIZA_ENDERECO_ESTOQUE, ' +
    '  IMPRIME_LISTA_SEPARACAO_VEND, ' +
    '  TIPO_COB_ENTRADA_NFE_XML_ID, ' +
    '  EMITIR_NOTA_ACUMULADO_FECH_PED, ' +
    '  UTILIZA_DIRECIONAMENTO_POR_LOC, ' +
    '  QUANTIDADE_VIAS_RECIBO_PAG, ' +
    '  CONFIRMAR_TRANSF_LOCAIS, ' +
    '  ATUALIZAR_PRECO_VENDA_ENT_NOT, ' +
    '  DELETAR_NF_PENDENTE_EMISSAO, ' +
    '  SMTP_SERVIDOR, '+
    '  SMTP_USUARIO, ' +
    '  SMTP_SENHA, ' +
    '  SMTP_PORTA, ' +
    '  SMTP_AUTENTICACAO, '+
    '  SMTP_REQUER_AUTENTICACAO, '+
    '  ATU_CUSTO_PROD_AGUAR_CHE, ' +
    '  CAL_COMISS_ACU_REC_FIN_DATA, ' +
    '  CALC_COMISS_ACU_REC_FIN, ' +
    '  AJUSTE_ESTOQUE_REAL, ' +
    '  ORCAMENTO_AMBIENTE, ' +
    '  SENHA_PEDIDOS_BLOQUEADOS, ' +
    '  HABILITAR_DESC_ITEM_IMP_ORC, '+
    '  NVL(TIPO_COMPROVANTE,''Pagamento'') AS TIPO_COMPROVANTE, '+
    '  UTILIZA_EMISSAO_BOLETOS, ' +
    '  CALC_COM_ACU_FEC_PEDIDO, ' +
    '  CALC_ADI_COMISSAO_META_FUN, ' +
    '  UTILIZA_CONTROLE_MANIFESTO, ' +
    '  UTILIZA_DRE, '+
    '  URL_INTEGRACAO_WEB, '+
    '  DATA_ULTIMA_INTEGRACAO_WEB, '+
    '  TOKEN_INTEGRACAO_WEB, '+
    '  INTERVALO_INTEGRACAO_WEB, ' +
    '  PORTA_INTEGRACAO_WEB ' +
    'from ' +
    '  PARAMETROS';

  AddColuna('CADASTRO_CONSUMIDOR_FINAL_ID');
  AddColuna('INICIAR_VENDA_CONSUMIDOR_FINAL');
  AddColuna('UTILIZAR_ANALISE_CUSTO');
  AddColuna('QTDE_MENSAGENS_OBRIGAR_LEITURA');
  AddColuna('QUANTIDADE_DIAS_VALIDADE_SENHA');
  AddColuna('MOTIVO_AJU_EST_BX_COMPRA_ID');
  AddColuna('TIPO_COBRANCA_GERACAO_CRED_ID');
  AddColuna('TIPO_COBRANCA_GER_CRED_IMP_ID');
  AddColuna('TIPO_COB_RECEB_ENTREGA_ID');
  AddColuna('TIPO_COB_ACUMULATIVO_ID');
  AddColuna('TIPO_COB_ADIANTAMENTO_ACU_ID');
  AddColuna('TIPO_COB_ADIANTAMENTO_FIN_ID');
  AddColuna('TIPO_COB_FECHAMENTO_TURNO_ID');
  AddColuna('BLOQUEAR_CREDITO_DEVOLUCAO');
  AddColuna('BLOQUEAR_CREDITO_PAGAR_MANUAL');
  AddColuna('QTDE_SEG_TRAVAR_ALTIS_OCIOSO');
  AddColuna('QTDE_MAXIMA_DIAS_DEVOLUCAO');
  AddColuna('UTIL_MODULO_PRE_ENTRADAS');
  AddColuna('DEFINIR_LOCAL_MANUAL');
  AddColuna('PREENCHER_LOCAIS_AUTO');
  AddColuna('SOMENTE_LOCAL_EST_POSITIVO');
  AddColuna('APLICAR_INDICE_DEDUCAO');
  AddColuna('UTILIZA_VENDA_RAPIDA');
  AddColuna('UTILIZA_MARKETPLACE');
  AddColuna('UTILIZA_ENDERECO_ESTOQUE');
  AddColuna('UTILIZA_CONTROLE_ENTREGA');
  addColuna('IMPRIME_LISTA_SEPARACAO_VEND');
  AddColuna('EMITIR_NOTA_ACUMULADO_FECH_PED');
  AddColuna('TIPO_COB_ENTRADA_NFE_XML_ID');
  AddColuna('UTILIZA_DIRECIONAMENTO_POR_LOC');
  AddColuna('QUANTIDADE_VIAS_RECIBO_PAG');
  AddColuna('CONFIRMAR_TRANSF_LOCAIS');
  AddColuna('ATUALIZAR_PRECO_VENDA_ENT_NOT');
  AddColuna('DELETAR_NF_PENDENTE_EMISSAO');
  AddColuna('SMTP_SERVIDOR');
  AddColuna('SMTP_USUARIO');
  AddColuna('SMTP_SENHA');
  AddColuna('SMTP_PORTA');
  AddColuna('SMTP_AUTENTICACAO');
  AddColuna('SMTP_REQUER_AUTENTICACAO');
  AddColuna('ATU_CUSTO_PROD_AGUAR_CHE');
  AddColuna('CALC_COMISS_ACU_REC_FIN');
  AddColuna('CAL_COMISS_ACU_REC_FIN_DATA');
  AddColuna('HABILITAR_DESC_ITEM_IMP_ORC');
  AddColuna('TIPO_COMPROVANTE');
  AddColuna('AJUSTE_ESTOQUE_REAL');
  AddColuna('SENHA_PEDIDOS_BLOQUEADOS');
  AddColuna('ORCAMENTO_AMBIENTE');
  AddColuna('UTILIZA_EMISSAO_BOLETOS');
  AddColuna('UTILIZA_DRE');
  AddColuna('CALC_COM_ACU_FEC_PEDIDO');
  AddColuna('CALC_ADI_COMISSAO_META_FUN');
  AddColuna('UTILIZA_CONTROLE_MANIFESTO');
  AddColuna('URL_INTEGRACAO_WEB');
  AddColuna('DATA_ULTIMA_INTEGRACAO_WEB');
  AddColuna('TOKEN_INTEGRACAO_WEB');
  AddColuna('INTERVALO_INTEGRACAO_WEB');
  AddColuna('PORTA_INTEGRACAO_WEB');

end;

function TParametros.getRecordParametros: RecParametros;
begin
  Result.cadastro_consumidor_final_id   := getInt('CADASTRO_CONSUMIDOR_FINAL_ID');
  Result.iniciar_venda_consumidor_final := getString('INICIAR_VENDA_CONSUMIDOR_FINAL');
  Result.UtilizarAnaliseCusto           := getString('UTILIZAR_ANALISE_CUSTO');
  Result.QtdeMensagensObrigarLeitura    := getInt('QTDE_MENSAGENS_OBRIGAR_LEITURA');
  Result.QuantidadeDiasValidadeSenha    := getInt('QUANTIDADE_DIAS_VALIDADE_SENHA');
  Result.MotivoAjuEstBxCompraId         := getInt('MOTIVO_AJU_EST_BX_COMPRA_ID');
  Result.TipoCobrancaGeracaoCredId      := getInt('TIPO_COBRANCA_GERACAO_CRED_ID');
  Result.TipoCobrancaGerCredImpId       := getInt('TIPO_COBRANCA_GER_CRED_IMP_ID');
  Result.TipoCobRecebEntregaId          := getInt('TIPO_COB_RECEB_ENTREGA_ID');
  Result.TipoCobAcumulativoId           := getInt('TIPO_COB_ACUMULATIVO_ID');
  Result.TipoCobAdiantamentoAcuId       := getInt('TIPO_COB_ADIANTAMENTO_ACU_ID');
  Result.TipoCobAdiantamentoFinId       := getInt('TIPO_COB_ADIANTAMENTO_FIN_ID');
  Result.TipoCobFechamentoTurnoId       := getInt('TIPO_COB_FECHAMENTO_TURNO_ID');
  Result.BloquearCreditoDevolucao       := getString('BLOQUEAR_CREDITO_DEVOLUCAO');
  Result.BloquearCreditoPagarManual     := getString('BLOQUEAR_CREDITO_PAGAR_MANUAL');
  Result.UtilModuloPreEntradas          := getString('UTIL_MODULO_PRE_ENTRADAS');
  Result.QtdeSegTravarAltisOcioso       := getInt('QTDE_SEG_TRAVAR_ALTIS_OCIOSO');
  Result.QtdeMaximaDiasDevolucao        := getInt('QTDE_MAXIMA_DIAS_DEVOLUCAO');
  Result.DefinirLocalManual             := getString('DEFINIR_LOCAL_MANUAL');
  Result.PreencherLocaisAutomaticamente := getString('PREENCHER_LOCAIS_AUTO');
  Result.SomenteLocaisEstoquePositivo := getString('SOMENTE_LOCAL_EST_POSITIVO');
  Result.AplicarIndiceDeducao           := getString('APLICAR_INDICE_DEDUCAO');
  Result.UtilVendaRapida                := getString('UTILIZA_VENDA_RAPIDA');
  Result.UtilMarketPlace                := getString('UTILIZA_MARKETPLACE');
  Result.UtilEnderecoEstoque            := getString('UTILIZA_ENDERECO_ESTOQUE');
  Result.UtilControleEntrega            := getString('UTILIZA_CONTROLE_ENTREGA');
  Result.ImprimirListaSeparacaoVenda    := getString('IMPRIME_LISTA_SEPARACAO_VEND');
  Result.TipoCobEntradaNfeXmlId         := getInt('TIPO_COB_ENTRADA_NFE_XML_ID');
  Result.EmitirNotaAcumuladoFechamentoPedido := getString('EMITIR_NOTA_ACUMULADO_FECH_PED');
  Result.UtilizaDirecionamentoPorLocais := getString('UTILIZA_DIRECIONAMENTO_POR_LOC');
  Result.QuantidadeViasReciboPagamento  := getInt('QUANTIDADE_VIAS_RECIBO_PAG');
  Result.UtilConfirmacaoTransferenciaLocais := getString('CONFIRMAR_TRANSF_LOCAIS');
  Result.AtualizarPrecoVendaEntradaNota := getString('ATUALIZAR_PRECO_VENDA_ENT_NOT');
  Result.DeletarNFPendenteEmissao       := getString('DELETAR_NF_PENDENTE_EMISSAO');
  Result.SmtpServidor                   := getString('SMTP_SERVIDOR');
  Result.SmtpUsuario                    := getString('SMTP_USUARIO');
  Result.SmtpSenha                      := getString('SMTP_SENHA');
  Result.SmtpPorta                      := getInt('SMTP_PORTA');
  Result.SmtpAutenticacao               := getString('SMTP_AUTENTICACAO');
  Result.SmtpReqAutenticacao            := getSTring('SMTP_REQUER_AUTENTICACAO');
  Result.AtualizarCustoProdutosAguardandoChegada := getString('ATU_CUSTO_PROD_AGUAR_CHE');
  Result.CalcularComissaoAcumuladoRecFin := getString('CALC_COMISS_ACU_REC_FIN');
  Result.CalcularComissaoAcumuladoRecFinData := getData('CAL_COMISS_ACU_REC_FIN_DATA');
  Result.HabilitarDescontoItemImpresaoOrcamento := getString('HABILITAR_DESC_ITEM_IMP_ORC');
  Result.TipoComprovante                        := getSTring('TIPO_COMPROVANTE');
  Result.AjusteEstoqueReal                      := getString('AJUSTE_ESTOQUE_REAL');
  Result.OrcamentoAmbiente                      := getString('ORCAMENTO_AMBIENTE');
  Result.UtilizaEmissaoBoleto                   := getString('UTILIZA_EMISSAO_BOLETOS');
  Result.UtilizaDRE                             := getString('UTILIZA_DRE');
  Result.SenhaPedidosBloqueados                 := getString('SENHA_PEDIDOS_BLOQUEADOS');
  Result.CalcularComissaoAcumuladoRecPedido     := getString('CALC_COM_ACU_FEC_PEDIDO');
  Result.CalcularAdicionalComissaoPorMetaFuncionario := getString('CALC_ADI_COMISSAO_META_FUN');
  Result.UtilizaControleManifesto               := getString('UTILIZA_CONTROLE_MANIFESTO');
  Result.UrlIntegracaoWeb                       := getString('URL_INTEGRACAO_WEB');
  Result.DataUltimaIntegracaoWeb                := getData('DATA_ULTIMA_INTEGRACAO_WEB');
  Result.TokenIntegracaoWeb                     := getString('TOKEN_INTEGRACAO_WEB');
  Result.IntervalorIntegracaoWeb                := getInt('INTERVALO_INTEGRACAO_WEB');
  Result.PortaIntegracaoWeb                     := getInt('PORTA_INTEGRACAO_WEB');

end;

function AtualizarParametros(
  pConexao: TConexao;
  pCadastroConsumidorFinalId: Integer;
  pIniciarVendaConsumidorFinal: string;
  pUtilizarAnaliseCusto: string;
  pQtdeMensagensObrigarLeitura: Integer;
  pQuantidadeDiasValidadeSenha: Integer;
  pMotivoAjuEstBxCompraId: Integer;
  pTipoCobrancaGeracaoCredId: Integer;
  pTipoCobrancaGerCredImpId: Integer;
  pTipoCobRecebEntregaId: Integer;
  pTipoCobAcumulativoId: Integer;
  pTipoCobAdiantamentoAcuId: Integer;
  pTipoCobAdiantamentoFinId: Integer;
  pTipoCobFechamentoTurnoId: Integer;
  pBloquearCreditoDevolucao: string;
  pBloquearCreditoPagarManual: string;
  pQtdeSegTravarAltisOcioso: Integer;
  pQtdeMaximaDiasDevolucao: Integer;
  pDefinirLocalManual: string;
  pPreencherLocaisAutomaticamente: string;
  pSomenteLocaisEstoquePositivo: string;
  pAplicarIndiceDeducao: string;
  pEntradaFutura : string;
  pVendaRapida : string;
  pUtilMarketPlace : string;
  pUtilEnderecoEstoque : string;
  pControleEntrega : string;
  pImprimeListaSeparacaoEntrega: string;
  pTipoCobEntradaNfeXmlId: Integer;
  pUtilizaDirecionamentoPorLocais: string;
  pQtdeViasReciboPagamento: Integer;
  pEmitirNotaAcumuladoFechamentoPedido: string;
  pUtilizaConfirmacaoTransferenciaLocais: string;
  pAtualizarPrecoVendaEntradaNota: string;
  pDeletarNFPendenteEmissao: string;
  pSmtpServidor: string;
  pSmtpUsuario: string;
  pSmtpSenha: string;
  pSmtpPorta: integer;
  pSmtpAutenticacao: string;
  pSmtpReqAutenticacao: string;
  pAtualizarCustoProdutosAguaradndoChegada: string;
  pCalcularComissaoAcumuladoRecFin: string;
  pDataInicioUtilizacao: TDateTime;
  pHabilitarDescontoItemImpresaoOrcamento: string;
  pAjusteEstoqueReal: string;
  pSenhaPedidosbloqueados: string;
  pOrcamentoAmbiente: string;
  pTipoComprovante: string;
  pUtilizaEmissaoBoleto: string;
  pUtilizaDRE: string;
  pCalcularComissaoAcumuladoRecPedido: string;
  pCalcularAdicionalComissaoPorMetaFuncionario: string;
  pUtilizaControleManifesto: string;
  pUrlIntegracaoWeb: string;
  pDataUltimaIntegracaoWeb: TdateTime;
  pIntervaloIntegracaoWeb: integer;
  pPortaIntegracaoWeb: integer
): RecRetornoBD;
var
  t: TParametros;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_PARAMETROS');

  t := TParametros.Create(pConexao);

  try
    t.setInt('CADASTRO_CONSUMIDOR_FINAL_ID', pCadastroConsumidorFinalId);
    t.setString('INICIAR_VENDA_CONSUMIDOR_FINAL', pIniciarVendaConsumidorFinal);
    t.setString('UTILIZAR_ANALISE_CUSTO', pUtilizarAnaliseCusto);
    t.setInt('QTDE_MENSAGENS_OBRIGAR_LEITURA', pQtdeMensagensObrigarLeitura);
    t.setInt('QUANTIDADE_DIAS_VALIDADE_SENHA', pQuantidadeDiasValidadeSenha);
    t.setIntN('MOTIVO_AJU_EST_BX_COMPRA_ID', pMotivoAjuEstBxCompraId);

    t.setIntN('TIPO_COBRANCA_GERACAO_CRED_ID', pTipoCobrancaGeracaoCredId);
    t.setIntN('TIPO_COBRANCA_GER_CRED_IMP_ID', pTipoCobrancaGerCredImpId);
    t.setIntN('TIPO_COB_RECEB_ENTREGA_ID', pTipoCobRecebEntregaId);
    t.setIntN('TIPO_COB_ACUMULATIVO_ID', pTipoCobAcumulativoId);
    t.setIntN('TIPO_COB_ADIANTAMENTO_ACU_ID', pTipoCobAdiantamentoAcuId);
    t.setIntN('TIPO_COB_ADIANTAMENTO_FIN_ID', pTipoCobAdiantamentoFinId);
    t.setIntN('TIPO_COB_FECHAMENTO_TURNO_ID', pTipoCobFechamentoTurnoId);

    t.setString('BLOQUEAR_CREDITO_DEVOLUCAO', pBloquearCreditoDevolucao);
    t.setString('BLOQUEAR_CREDITO_PAGAR_MANUAL', pBloquearCreditoPagarManual);
    t.setInt('QTDE_SEG_TRAVAR_ALTIS_OCIOSO', pQtdeSegTravarAltisOcioso);
    t.setInt('QTDE_MAXIMA_DIAS_DEVOLUCAO', pQtdeMaximaDiasDevolucao);
    t.setString('DEFINIR_LOCAL_MANUAL', pDefinirLocalManual);
    t.setString('PREENCHER_LOCAIS_AUTO', pPreencherLocaisAutomaticamente);
    t.setString('SOMENTE_LOCAL_EST_POSITIVO', pSomenteLocaisEstoquePositivo);
    t.setString('APLICAR_INDICE_DEDUCAO', pAplicarIndiceDeducao);
    t.setString('UTIL_MODULO_PRE_ENTRADAS', pEntradaFutura);
    t.setString('UTILIZA_VENDA_RAPIDA', pVendaRapida);
    t.setString('UTILIZA_MARKETPLACE', pUtilMarketPlace);
    t.setString('UTILIZA_ENDERECO_ESTOQUE', pUtilEnderecoEstoque);
    t.setString('UTILIZA_CONTROLE_ENTREGA', pControleEntrega);
    t.setString('IMPRIME_LISTA_SEPARACAO_VEND', pImprimeListaSeparacaoEntrega);
    t.setIntN('TIPO_COB_ENTRADA_NFE_XML_ID', pTipoCobEntradaNfeXmlId);
    t.setString('UTILIZA_DIRECIONAMENTO_POR_LOC', pUtilizaDirecionamentoPorLocais);
    t.setString('EMITIR_NOTA_ACUMULADO_FECH_PED', pEmitirNotaAcumuladoFechamentoPedido);
    t.setInt('QUANTIDADE_VIAS_RECIBO_PAG', pQtdeViasReciboPagamento);
    t.setString('CONFIRMAR_TRANSF_LOCAIS', pUtilizaConfirmacaoTransferenciaLocais);
    t.setString('ATUALIZAR_PRECO_VENDA_ENT_NOT', pAtualizarPrecoVendaEntradaNota);
    t.setString('DELETAR_NF_PENDENTE_EMISSAO', pDeletarNFPendenteEmissao);
    t.setString('SMTP_SERVIDOR', pSmtpServidor);
    t.setString('SMTP_USUARIO', pSmtpUsuario);
    t.setString('SMTP_SENHA', pSmtpSenha);
    t.setInt('SMTP_PORTA', pSmtpPorta);
    t.setString('SMTP_AUTENTICACAO', pSmtpAutenticacao);
    t.setString('SMTP_REQUER_AUTENTICACAO', pSmtpReqAutenticacao);
    t.setString('ATU_CUSTO_PROD_AGUAR_CHE',   pAtualizarCustoProdutosAguaradndoChegada);
    t.setString('CALC_COMISS_ACU_REC_FIN', pCalcularComissaoAcumuladoRecFin);
    t.setDataN('CAL_COMISS_ACU_REC_FIN_DATA', pDataInicioUtilizacao);
    t.setString('HABILITAR_DESC_ITEM_IMP_ORC', pHabilitarDescontoItemImpresaoOrcamento);
    t.setString('TIPO_COMPROVANTE', pTipoComprovante);
    t.setString('AJUSTE_ESTOQUE_REAL', pAjusteEstoqueReal);
    t.setString('ORCAMENTO_AMBIENTE', pOrcamentoAmbiente);
    t.setString('SENHA_PEDIDOS_BLOQUEADOS', pSenhaPedidosbloqueados);
    t.setString('UTILIZA_EMISSAO_BOLETOS', pUtilizaEmissaoBoleto);
    t.setString('UTILIZA_DRE', pUtilizaDRE);
    t.setString('CALC_COM_ACU_FEC_PEDIDO', pCalcularComissaoAcumuladoRecPedido);
    t.setString('CALC_ADI_COMISSAO_META_FUN', pCalcularAdicionalComissaoPorMetaFuncionario);
    t.setString('UTILIZA_CONTROLE_MANIFESTO', pUtilizaControleManifesto);
    t.setString('URL_INTEGRACAO_WEB', pUrlIntegracaoWeb);
    t.setData('DATA_ULTIMA_INTEGRACAO_WEB', pDataUltimaIntegracaoWeb);
    t.setInt('INTERVALO_INTEGRACAO_WEB', pIntervaloIntegracaoWeb);
    t.setInt('PORTA_INTEGRACAO_WEB', pPortaIntegracaoWeb);
    t.Atualizar;
  except
    on e: Exception do
      Result.TratarErro(e);
  end;

  t.Free;
end;

function BuscarParametros(pConexao: TConexao): RecParametros;
var
  t: TParametros;
begin
  t := TParametros.Create(pConexao);

  t.PesquisarComando('');
  Result := t.getRecordParametros;

  t.Free;
end;

function getCompilacaoSistemaBanco(pConexao: TConexao): string;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.SQL.Add('select VERSAO_ATUAL from PARAMETROS');
  vSql.Pesquisar;

  Result := vSql.getString(0);
  
  vSql.Active := False;
  vSql.Free;
end;

function atualizarCompilacaoSistemaBanco(pConexao: TConexao; pNovaCompilacao: string): RecRetornoBD;
var
  vSql: TExecucao;
begin
  Result.TeveErro := False;
  vSql := TExecucao.Create(pConexao);

  try
    pConexao.IniciarTransacao;
    
    vSql.SQL.Add('update PARAMETROS set VERSAO_ATUAL = :P1');
    vSql.Executar([pNovaCompilacao]);
  
    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vSql.Free;
end;

function FinalizarAnoMes(pConexao: TConexao): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('FINALIZAR_ANO_MES');

  vProc := TProcedimentoBanco.Create(pConexao, 'FINALIZAR_ANO_MES');
  try
    pConexao.IniciarTransacao;

    vProc.Executar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  vProc.Free;
end;

end.
