unit _ProdutosRelacionados;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TProdutosRelacionado = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordProdutosRelacionado: RecProdutosRelacionados;
  end;

function AtualizarProdutosRelacionado(
  pConexao: TConexao;
  pProdutoPrincipalId: Integer;
  pProdutos: TArray<RecProdutosRelacionados>
): RecRetornoBD;

function BuscarProdutosRelacionados(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecProdutosRelacionados>;

function ExcluirProdutosRelacionado(
  pConexao: TConexao
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TProdutosRelacionado }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where PRE.PRODUTO_PRINCIPAL_ID = :P1 ' +
      'and PPR.EMPRESA_ID = :P2 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where PRE.PRODUTO_PRINCIPAL_ID = :P1 ' +
      'and PPR.EMPRESA_ID = :P2 ' +
      'and nvl(PPR.PRECO_VAREJO, 0) > 0 ' +
      'and PRO.ATIVO = ''S'' '
    );
end;

constructor TProdutosRelacionado.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PRODUTOS_RELACIONADOS');

  FSql :=
    'select ' +
    '  PRE.PRODUTO_PRINCIPAL_ID, ' +
    '  PRE.PRODUTO_RELACIONADO_ID, ' +
    '  PRE.QUANTIDADE_SUGESTAO, ' +
    '  PRO.NOME as NOME_PRODUTO_RELACIONADO ' +
    'from ' +
    '  PRODUTOS_RELACIONADOS PRE ' +

    'inner join PRODUTOS PRO ' +
    'on PRE.PRODUTO_RELACIONADO_ID = PRO.PRODUTO_ID ' +

    'left join PRECOS_PRODUTOS PPR ' +
    'on PRO.PRODUTO_ID = PPR.PRODUTO_ID ';

  setFiltros(getFiltros);

  AddColuna('PRODUTO_PRINCIPAL_ID', True);
  AddColuna('PRODUTO_RELACIONADO_ID', True);
  AddColuna('QUANTIDADE_SUGESTAO');
  AddColunaSL('NOME_PRODUTO_RELACIONADO')
end;

function TProdutosRelacionado.getRecordProdutosRelacionado: RecProdutosRelacionados;
begin
  Result.ProdutoPrincipalId     := getInt('PRODUTO_PRINCIPAL_ID', True);
  Result.ProdutoRelacionadoId   := getInt('PRODUTO_RELACIONADO_ID', True);
  Result.QuantidadeSugestao     := getDouble('QUANTIDADE_SUGESTAO');
  Result.NomeProdutoRelacionado := getString('NOME_PRODUTO_RELACIONADO');
end;

function AtualizarProdutosRelacionado(
  pConexao: TConexao;
  pProdutoPrincipalId: Integer;
  pProdutos: TArray<RecProdutosRelacionados>
): RecRetornoBD;
var
  i: Integer;
  vExec: TExecucao;
  t: TProdutosRelacionado;
begin
  Result.TeveErro := False;
  vExec := TExecucao.Create(pConexao);
  t := TProdutosRelacionado.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    vExec.SQL.Add('delete from PRODUTOS_RELACIONADOS where PRODUTO_PRINCIPAL_ID = :P1');
    vExec.Executar([pProdutoPrincipalId]);

    t.setInt('PRODUTO_PRINCIPAL_ID', pProdutoPrincipalId, True);
    for i := Low(pProdutos) to High(pProdutos) do begin
      t.setInt('PRODUTO_RELACIONADO_ID', pProdutos[i].ProdutoRelacionadoId, True);
      t.setDouble('QUANTIDADE_SUGESTAO', pProdutos[i].QuantidadeSugestao);

      t.Inserir;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vExec.Free;
  t.Free;
end;

function BuscarProdutosRelacionados(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecProdutosRelacionados>;
var
  i: Integer;
  t: TProdutosRelacionado;
begin
  Result := nil;
  t := TProdutosRelacionado.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordProdutosRelacionado;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirProdutosRelacionado(
  pConexao: TConexao
): RecRetornoBD;
var
  t: TProdutosRelacionado;
begin
  Result.TeveErro := False;
  t := TProdutosRelacionado.Create(pConexao);

  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
