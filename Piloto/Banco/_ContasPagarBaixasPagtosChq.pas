unit _ContasPagarBaixasPagtosChq;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsFinanceiros;

{$M+}
type
  TContasPagarBaixasPagtosChq = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordContasPagBaixasPagtosChq: RecTitulosFinanceiros;
  end;

function BuscarContasRecBaixasPagtosChqs(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TContasPagarBaixasPagtosChq }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where BAIXA_ID = :P1'
    );
end;

constructor TContasPagarBaixasPagtosChq.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONTAS_PAGAR_BAIXAS_PAGTOS_CHQ');

  FSql :=
    'select ' +
    '  BAIXA_ID, ' +
    '  COBRANCA_ID, ' +
    '  ITEM_ID, ' +
    '  DATA_VENCIMENTO, ' +
    '  NUMERO_CHEQUE, ' +
    '  VALOR_CHEQUE, ' +
    '  PARCELA, ' +
    '  NUMERO_PARCELAS ' +
    'from ' +
    '  CONTAS_PAGAR_BAIXAS_PAGTOS_CHQ';

  setFiltros(getFiltros);

  AddColuna('BAIXA_ID', True);
  AddColuna('COBRANCA_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('DATA_VENCIMENTO');
  AddColuna('NUMERO_CHEQUE');
  AddColuna('VALOR_CHEQUE');
  AddColuna('PARCELA');
  AddColuna('NUMERO_PARCELAS');
end;

function TContasPagarBaixasPagtosChq.getRecordContasPagBaixasPagtosChq: RecTitulosFinanceiros;
begin
  Result.Id               := getInt('BAIXA_ID', True);
  Result.CobrancaId       := getInt('COBRANCA_ID', True);
  Result.ItemId           := getInt('ITEM_ID', True);
  Result.DataVencimento   := getData('DATA_VENCIMENTO');
  Result.NumeroCheque     := getInt('NUMERO_CHEQUE');
  Result.Valor            := getDouble('VALOR_CHEQUE');
  Result.Parcela          := getInt('PARCELA');
  Result.NumeroParcelas   := getInt('NUMERO_PARCELAS');
end;

function BuscarContasRecBaixasPagtosChqs(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTitulosFinanceiros>;
var
  i: Integer;
  t: TContasPagarBaixasPagtosChq;
begin
  Result := nil;
  t := TContasPagarBaixasPagtosChq.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordContasPagBaixasPagtosChq;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
