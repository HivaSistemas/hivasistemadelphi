unit _PlanosFinanceiros;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils;

{$M+}
type
  RecPlanosFinanceiros = class
  public
    PlanoFinanceiroId: string;
    Descricao: string;
    PlanoFinanceiroPaiId: string;
    UltimoSubGrupo: string;
    Tipo: string;
    ExibirDre: string;
    Ativo: string;
    PermiteAlteracao: string;
  end;

  TPlanosFinanceiros = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordPlanos: RecPlanosFinanceiros;
  end;

function AtualizarPlanosFinanceiros(
  pConexao: TConexao;
  pPlanoFinanceiroId: string;
  pDescricao: string;
  pPlanoFinanceiroPaiId: string;
  pTipo: string;
  pExibirDre: string
): RecRetornoBD;

function BuscarPlanosFinanceiros(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteNivel3: Boolean
): TArray<RecPlanosFinanceiros>;

function ExcluirPlanoFinanceiro(
  pConexao: TConexao;
  pPlanoFinanceiroId: string
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TPlanosFinanceiros }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 4);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where PLANO_FINANCEIRO_ID = :P1 '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Descri��o ( avan�ado )',
      True,
      0,
      'where DESCRICAO like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  PLANO_FINANCEIRO_ID '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'order by ' +
      '  PLANO_FINANCEIRO_ID '
    );

  Result[3] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'C�digo plano fin.( Avan�ado )',
      False,
      1,
      'where PLANO_FINANCEIRO_ID like ''%'' || :P1 || ''%'' ',
      'order by ' +
      '  PLANO_FINANCEIRO_ID '
    );
end;

constructor TPlanosFinanceiros.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PLANOS_FINANCEIROS');

  FSql :=
    'select ' +
    '  PLANO_FINANCEIRO_ID, ' +
    '  DESCRICAO, ' +
    '  PLANO_FINANCEIRO_PAI_ID, ' +
    '  ULTIMO_SUB_GRUPO, ' +
    '  TIPO, ' +
    '  EXIBIR_DRE, ' +
    '  PERMITE_ALTERACAO ' +
    'from ' +
    '  PLANOS_FINANCEIROS ';

  SetFiltros(GetFiltros);

  AddColuna('PLANO_FINANCEIRO_ID', True);
  AddColuna('DESCRICAO');
  AddColunaSL('PLANO_FINANCEIRO_PAI_ID');
  AddColunaSL('ULTIMO_SUB_GRUPO');
  AddColuna('TIPO');
  AddColuna('EXIBIR_DRE');
  AddColunaSL('PERMITE_ALTERACAO');
end;

function TPlanosFinanceiros.GetRecordPlanos: RecPlanosFinanceiros;
begin
  Result := RecPlanosFinanceiros.Create;

  Result.PlanoFinanceiroId    := GetString('PLANO_FINANCEIRO_ID', True);
  Result.Descricao            := GetString('DESCRICAO');
  Result.PlanoFinanceiroPaiId := GetString('PLANO_FINANCEIRO_PAI_ID');
  Result.UltimoSubGrupo       := GetString('ULTIMO_SUB_GRUPO');
  Result.Tipo                 := GetString('TIPO');
  Result.ExibirDre            := GetString('EXIBIR_DRE');
  Result.PermiteAlteracao     := GetString('PERMITE_ALTERACAO');
end;

function AtualizarPlanosFinanceiros(
  pConexao: TConexao;
  pPlanoFinanceiroId: string;
  pDescricao: string;
  pPlanoFinanceiroPaiId: string;
  pTipo: string;
  pExibirDre: string
): RecRetornoBD;
var
  vId: Integer;
  vNovo: Boolean;
  vSql: TConsulta;
  t: TPlanosFinanceiros;
begin
  Result.Iniciar;

  vNovo := (Pos('???', pPlanoFinanceiroId) > 0) or (pPlanoFinanceiroId = '');
  if vNovo then begin
    vSql := TConsulta.Create(pConexao);

    if pPlanoFinanceiroId = '' then begin
      vSql.SQL.Add('select ');
      vSql.SQL.Add('    MAX(TO_NUMBER(CASE ');
      vSql.SQL.Add('        WHEN INSTR(PLANO_FINANCEIRO_ID, ''.'') > 0 THEN SUBSTR(PLANO_FINANCEIRO_ID, 1, INSTR(PLANO_FINANCEIRO_ID, ''.'') - 1) ');
      vSql.SQL.Add('        ELSE PLANO_FINANCEIRO_ID ');
      vSql.SQL.Add('    END)) + 1 AS subcadeia ');
      vSql.SQL.Add('from ');
      vSql.SQL.Add('  PLANOS_FINANCEIROS ');
      vSql.Pesquisar;

      pPlanoFinanceiroId := vSql.GetString(0);
    end
    else begin
      vSql.SQL.Add('select ');
      vSql.SQL.Add('  nvl( ');
      vSql.SQL.Add('    max( ');
      vSql.SQL.Add('      substr( ');
      vSql.SQL.Add('        PLANO_FINANCEIRO_ID, ');
      vSql.SQL.Add('        length(PLANO_FINANCEIRO_ID) - 2, ');
      vSql.SQL.Add('        length(PLANO_FINANCEIRO_ID) - 1 ');
      vSql.SQL.Add('      ) ');
      vSql.SQL.Add('    ), 0 ');
      vSql.SQL.Add('  ) + 1 ');
      vSql.SQL.Add('from ');
      vSql.SQL.Add('  PLANOS_FINANCEIROS ');
      vSql.SQL.Add('where substr(PLANO_FINANCEIRO_ID, 0, length(:P1)) = :P1 ');
      vSql.SQL.Add('and length(PLANO_FINANCEIRO_ID) < length(:P1) + 5 ');
      vSql.SQL.Add('and length(PLANO_FINANCEIRO_ID) > length(:P1) ');
      vSql.Pesquisar([pPlanoFinanceiroPaiId]);

      if (pPlanoFinanceiroPaiId = '1.001') and (vSql.GetInt(0) < 21) then
        vId := 21
      else if
        (Copy(pPlanoFinanceiroPaiId, 1, 4) = '1.00') and
        (SFormatInt(Copy(pPlanoFinanceiroPaiId, 5, 1)) in [2,3,5]) and
        (vSql.GetInt(0) < 6)
      then
        vId := 6
      else
        vId := vSql.GetInt(0);

      pPlanoFinanceiroId := pPlanoFinanceiroPaiId + '.' + LPad(NFormat(vId), 3, '0');
    end;

    Result.AsString := pPlanoFinanceiroId;
    vSql.Active := False;
    vSql.Free;
  end;

  t := TPlanosFinanceiros.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setString('PLANO_FINANCEIRO_ID', pPlanoFinanceiroId, True);
    t.setString('DESCRICAO', pDescricao);
    t.setStringN('PLANO_FINANCEIRO_PAI_ID', pPlanoFinanceiroPaiId);
    t.setString('TIPO', pTipo);
    t.setString('EXIBIR_DRE', pExibirDre);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarPlanosFinanceiros(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pSomenteNivel3: Boolean
): TArray<RecPlanosFinanceiros>;
var
  i: Integer;
  vSqlAuxiliar: string;
  t: TPlanosFinanceiros;
begin
  Result := nil;
  t := TPlanosFinanceiros.Create(pConexao);

  vSqlAuxiliar := '';
  if pSomenteNivel3 then
    vSqlAuxiliar :=  'and length(PLANO_FINANCEIRO_ID) = 9 ';

  if t.Pesquisar(pIndice, pFiltros, vSqlAuxiliar) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordPlanos;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirPlanoFinanceiro(
  pConexao: TConexao;
  pPlanoFinanceiroId: string
): RecRetornoBD;
var
  t: TPlanosFinanceiros;
begin
  pConexao.SetRotina('EXCLUIR_PLANO_FIN');
  Result.TeveErro := False;
  t := TPlanosFinanceiros.Create(pConexao);

  t.SetString('PLANO_FINANCEIRO_ID', pPlanoFinanceiroId, True);

  try
    pConexao.IniciarTransacao;

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
