unit _VidaProduto;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsEstoques;

{$M+}
type
  TVidaProduto = class(TOperacoes)
  public
    FTipoEstoque: string;
    constructor Create(pConexao: TConexao; pTipoEstoque: string);
  protected
    function getRecordMovimentosProduto: RecVidaProduto;
  end;

function VidaProdutoProdutos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pTipoEstoque: string
): TArray<RecVidaProduto>;

function BuscarVidaProdutoComando(
  pConexao: TConexao;
  pComando: string;
  pTipoEstoque: string
): TArray<RecVidaProduto>;

function BuscarSaldoInicialVidaProduto(pConexao: TConexao; pComando: string; pTipoEstoque: string): Double;

function getFiltros: TArray<RecFiltros>;

implementation

{ TMovimentosProdutos }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where XXX = :P1'
    );
end;

constructor TVidaProduto.Create(pConexao: TConexao; pTipoEstoque: string);
var
  vView: string;
begin
  inherited Create(pConexao, 'VW_VIDA_PRODUTO_FISICO');

  FTipoEstoque := pTipoEstoque;

  if pTipoEstoque = 'D' then
    vView := 'VW_VIDA_PRODUTO_DISPONIVEL'
  else if pTipoEstoque = 'F' then
    vView := 'VW_VIDA_PRODUTO_FISICO'
  else
    vView := 'VW_VIDA_PRODUTO_FISCAL';

  FSql :=
    'select ' +
    '  MOVIMENTO_ID, ' +
    '  PRODUTO_ID, ' +
    '  EMPRESA_ID, ' +
    '  QUANTIDADE, ' +
    '  DATA_HORA_MOVIMENTO, ' +
    '  TIPO_MOVIMENTO, ' +
    '  NATUREZA, ' +
    '  NUMERO_DOCUMENTO, ' +
    '  CADASTRO_ID, ' +
    '  NOME_CADASTRO ' +
    'from ' +
    '  ' + vView;

  setFiltros(getFiltros);

  AddColuna('MOVIMENTO_ID');
  AddColuna('PRODUTO_ID');
  AddColuna('EMPRESA_ID');
  AddColuna('QUANTIDADE');
  AddColuna('TIPO_MOVIMENTO');
  AddColuna('DATA_HORA_MOVIMENTO');
  AddColuna('NATUREZA');
  AddColuna('NUMERO_DOCUMENTO');
  AddColuna('CADASTRO_ID');
  AddColuna('NOME_CADASTRO');
end;

function TVidaProduto.getRecordMovimentosProduto: RecVidaProduto;
begin
  Result.movimento_id        := getInt('MOVIMENTO_ID');
  Result.produto_id          := getInt('PRODUTO_ID');
  Result.empresa_id          := getInt('EMPRESA_ID');
  Result.quantidade          := getDouble('QUANTIDADE');
  Result.data_hora_movimento := getData('DATA_HORA_MOVIMENTO');
  Result.tipo_movimento      := getString('TIPO_MOVIMENTO');
  Result.natureza            := getString('NATUREZA');
  Result.NumeroDocumento    := getString('NUMERO_DOCUMENTO');
  Result.CadastroId         := getInt('CADASTRO_ID');
  Result.NomeCadastro       := getString('NOME_CADASTRO');

  if FTipoEstoque = 'D' then begin
    Result.tipo_movimento_analitico :=
      Decode(
        Result.tipo_movimento, [
        'TPS', 'Transf.entre emp.(sa�da)',
        'RAT', 'Retirada no ato',
        'RET', 'Retirada futura',
        'AJU', 'Ajuste de estoque',
        'DEV', 'Devol. venda',
        'ENT', 'Entrada de produto',
        'VEN', 'Entrega',
        'ENG', 'Entrega futura',
        'DEN', 'Devolu��o de compra',
        'PRO', 'Produ��o',
        'BLO', 'Bloqueio de estoque']
      );
  end
  else if FTipoEstoque = 'F' then begin
    Result.tipo_movimento_analitico :=
      Decode(
        Result.tipo_movimento, [
        'TPS', 'Transf.entre emp.(sa�da)',
        'RAT', 'Retirada no ato',
        'RET', 'Retirada futura',
        'AJU', 'Ajuste de estoque',
        'DEV', 'Devol. venda',
        'DEN', 'Devolu��o de compra',
        'ENT', 'Entrada de produto',
        'VEN', 'Entrega',
        'ENG', 'Entrega futura',
        'REN', 'Retorno de entrega',
        'BLO', 'Bloqueio de estoque']
      );
  end
  else begin
    Result.tipo_movimento_analitico :=
      Decode(
        Result.tipo_movimento, [
        'ENT', 'Entrada de produtos',
        'VRA', 'Venda retirar ato',
        'VRE', 'Venda retirar',
        'VEN', 'Venda entregar',
        'DEV', 'Devolu��es de vendas',
        'DEN', 'Devolu��o de compra',
        'AJU', 'Ajuste de estoque',
        'OUT', 'Outras notas',
        'TFR', 'Transfer�ncia retirar',
        'TFE', 'Transfer�ncia entregar',
        'TPE', 'Transfer�ncia empresa']
      );
  end;
end;

function VidaProdutoProdutos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pTipoEstoque: string
): TArray<RecVidaProduto>;
var
  i: Integer;
  t: TVidaProduto;
begin
  Result := nil;
  t := TVidaProduto.Create(pConexao, pTipoEstoque);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordMovimentosProduto;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarVidaProdutoComando(
  pConexao: TConexao;
  pComando: string;
  pTipoEstoque: string
): TArray<RecVidaProduto>;
var
  i: Integer;
  t: TVidaProduto;
begin
  Result := nil;
  t := TVidaProduto.Create(pConexao, pTipoEstoque);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordMovimentosProduto;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarSaldoInicialVidaProduto(pConexao: TConexao; pComando: string; pTipoEstoque: string): Double;
var
  vSql: TConsulta;
  vView: string;
begin
  Result := 0;
  vSql := TConsulta.Create(pConexao);

  if pTipoEstoque = 'D' then
    vView := 'VW_VIDA_PRODUTO_DISPONIVEL'
  else if pTipoEstoque = 'F' then
    vView := 'VW_VIDA_PRODUTO_FISICO'
  else
    vView := 'VW_VIDA_PRODUTO_FISCAL';

  vSql.SQL.Add('select ');
  vSql.SQL.Add('  sum(QUANTIDADE * case when NATUREZA = ''E'' then 1 else -1 end) as QUANTIDADE ');
  vSql.SQL.Add('from ');
  vSql.SQL.Add('  ' + vView + ' ');
  vSql.SQL.Add(pComando);

  if vSql.Pesquisar() then
    Result := vSql.GetDouble(0);

  vSql.Free;
end;

end.
