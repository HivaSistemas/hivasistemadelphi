unit _PreEntradasNotasFiscais;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, Vcl.Graphics, System.Classes;

{$M+}
type
  RecPreEntradasNotasFiscais = record
    PreEntradaId: Integer;
    EmpresaId: Integer;
    ChaveAcessoNfe: string;
    Cnpj: string;
    RazaoSocial: string;
    InscricaoEstadual: string;
    DataHoraEmissao: TDateTime;
    NumeroNota: Integer;
    SerieNota: string;
    ValorTotalNota: Double;
    Status: string;
    StatusAnalitico: string;
    TipoNota: string;
    TipoNotaAnaltico: string;
    StatusNota: string;
    XmlTexto: string;
    StatusNotaAnalitico: string;
    EntradaId: Integer;
    UsuarioManifestacaoId: Integer;
    DataHoraManifestacao: TDateTime;
    UsuarioDesconhecimentoId: Integer;
    DataHoraDesconhecimento: TDateTime;
    UsuarioOperNaoRealizadaId: Integer;
    DataHoraOperNaoRealizada: TDateTime;
    MotivoOperacaoNaoRealzada: string;
    UsuarioConfirmacaoId: Integer;
    DataHoraConfirmacao: TDateTime;
  end;

  TPreEntradasNotasFiscais = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordPreEntradasNotasFiscais: RecPreEntradasNotasFiscais;
  end;

function AtualizarPreEntradasNotasFiscais(
  pConexao: TConexao;
  pPreEntradaId: Integer;
  pPreEmpresaId: Integer;
  pChaveAcessoNfe: string;
  pCnpj: string;
  pRazaoSocial: string;
  pInscricaoEstadual: string;
  pDataHoraEmissao: TDateTime;
  pNumeroNota: Integer;
  pSerieNota: string;
  pValorTotalNota: Double;
  pStatus: string;
  pTipoNota: string;
  pStatusNota: string;
  pEmTransacao: Boolean
): RecRetornoBD;

function BuscarPreEntradasNotasFiscais(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecPreEntradasNotasFiscais>;

function BuscarPreEntradasNotasFiscaisComando(pConexao: TConexao; pComando: string): TArray<RecPreEntradasNotasFiscais>;

function ExcluirPreEntradasNotasFiscais(
  pConexao: TConexao;
  pPreEntradaId: Integer
): RecRetornoBD;

function AtualizarStatusNotaPreEntrada(pConexao: TConexao; pChaveNFe: string; pSituacao: string; pEmTransacao: Boolean): RecRetornoBD;
function AtualizarStatusAltisPreEntrada(
  pConexao: TConexao;
  pPreEntradaId: Integer;
  pStatus: string;
  pXml: string;
  pMotivoOperacaoNaoRealizada: string
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TPreEntradasNotasFiscais }


function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where PRE_ENTRADA_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CHAVE_ACESSO_NFE in( ' +
      '  select ' +
      '    CHAVE_ACESSO_NFE ' +
      '  from ' +
      '    PRE_ENTR_CTE_NFE_REFERENCIADAS ' +
      '  where PRE_ENTRADA_ID = :P1 ' + // Esta PRE_ENTRADA_ID � a do CTe e n�o da nota fiscal
      ') ' +
      'and XML_TEXTO is not null'
    );
end;

constructor TPreEntradasNotasFiscais.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PRE_ENTRADAS_NOTAS_FISCAIS');

  FSql := 
    'select ' +
    '  PRE.PRE_ENTRADA_ID, ' +
    '  PRE.EMPRESA_ID, ' +
    '  PRE.CHAVE_ACESSO_NFE, ' +
    '  PRE.CNPJ, ' +
    '  PRE.RAZAO_SOCIAL, ' +
    '  PRE.INSCRICAO_ESTADUAL, ' +
    '  PRE.DATA_HORA_EMISSAO, ' +
    '  PRE.NUMERO_NOTA, ' +
    '  PRE.SERIE_NOTA, ' +
    '  PRE.VALOR_TOTAL_NOTA, ' +
    '  PRE.STATUS, ' +
    '  PRE.TIPO_NOTA, ' +
    '  PRE.STATUS_NOTA, ' +
    '  PRE.ENTRADA_ID, ' +
    '  PRE.USUARIO_MANIFESTACAO_ID, ' +
    '  PRE.DATA_HORA_MANIFESTACAO, ' +
    '  PRE.USUARIO_DESCONHECIMENTO_ID, ' +
    '  PRE.DATA_HORA_DESCONHECIMENTO, ' +
    '  PRE.USUARIO_OPER_NAO_REALIZADA_ID, ' +
    '  PRE.DATA_HORA_OPER_NAO_REALIZADA, ' +
    '  PRE.MOTIVO_OPERACAO_NAO_REALIZADA, ' +
    '  PRE.USUARIO_CONFIRMACAO_ID, ' +
    '  PRE.DATA_HORA_CONFIRMACAO, ' +
    '  PRE.XML_TEXTO ' +
    'from ' +
    '  PRE_ENTRADAS_NOTAS_FISCAIS PRE ' +

    'left join CADASTROS CAD ' +
    'on PRE.CNPJ = CAD.CPF_CNPJ ';

  setFiltros(getFiltros);

  AddColuna('PRE_ENTRADA_ID', True);
  AddColuna('EMPRESA_ID');
  AddColuna('CHAVE_ACESSO_NFE');
  AddColuna('CNPJ');
  AddColuna('RAZAO_SOCIAL');
  AddColuna('INSCRICAO_ESTADUAL');
  AddColuna('DATA_HORA_EMISSAO');
  AddColuna('NUMERO_NOTA');
  AddColuna('SERIE_NOTA');
  AddColuna('VALOR_TOTAL_NOTA');
  AddColuna('STATUS');
  AddColuna('TIPO_NOTA');
  AddColuna('STATUS_NOTA');
  AddColunaSL('ENTRADA_ID');
  AddColunaSL('XML_TEXTO');
  AddColunaSL('USUARIO_MANIFESTACAO_ID');
  AddColunaSL('DATA_HORA_MANIFESTACAO');
  AddColunaSL('USUARIO_DESCONHECIMENTO_ID');
  AddColunaSL('DATA_HORA_DESCONHECIMENTO');
  AddColunaSL('USUARIO_OPER_NAO_REALIZADA_ID');
  AddColunaSL('DATA_HORA_OPER_NAO_REALIZADA');
  AddColunaSL('MOTIVO_OPERACAO_NAO_REALIZADA');
  AddColunaSL('USUARIO_CONFIRMACAO_ID');
  AddColunaSL('DATA_HORA_CONFIRMACAO');
end;

function TPreEntradasNotasFiscais.getRecordPreEntradasNotasFiscais: RecPreEntradasNotasFiscais;
begin
  Result.PreEntradaId               := getInt('PRE_ENTRADA_ID', True);
  Result.EmpresaId                  := getInt('EMPRESA_ID');
  Result.ChaveAcessoNfe             := getString('CHAVE_ACESSO_NFE');
  Result.Cnpj                       := getString('CNPJ');
  Result.RazaoSocial                := getString('RAZAO_SOCIAL');
  Result.InscricaoEstadual          := getString('INSCRICAO_ESTADUAL');
  Result.DataHoraEmissao            := getData('DATA_HORA_EMISSAO');
  Result.NumeroNota                 := getInt('NUMERO_NOTA');
  Result.SerieNota                  := getString('SERIE_NOTA');
  Result.ValorTotalNota             := getDouble('VALOR_TOTAL_NOTA');
  Result.Status                     := getString('STATUS');
  Result.TipoNota                   := getString('TIPO_NOTA');
  Result.StatusNota                 := getString('STATUS_NOTA');
  Result.XmlTexto                   := getString('XML_TEXTO');
  Result.EntradaId                  := getInt('ENTRADA_ID');
  Result.UsuarioManifestacaoId      := getInt('USUARIO_MANIFESTACAO_ID');
  Result.DataHoraManifestacao       := getData('DATA_HORA_MANIFESTACAO');
  Result.UsuarioDesconhecimentoId   := getInt('USUARIO_DESCONHECIMENTO_ID');
  Result.DataHoraDesconhecimento    := getData('DATA_HORA_DESCONHECIMENTO');
  Result.UsuarioOperNaoRealizadaId  := getInt('USUARIO_OPER_NAO_REALIZADA_ID');
  Result.DataHoraOperNaoRealizada   := getData('DATA_HORA_OPER_NAO_REALIZADA');
  Result.MotivoOperacaoNaoRealzada  := getString('MOTIVO_OPERACAO_NAO_REALIZADA');
  Result.UsuarioConfirmacaoId       := getInt('USUARIO_CONFIRMACAO_ID');
  Result.DataHoraConfirmacao        := getData('DATA_HORA_CONFIRMACAO');

  Result.StatusAnalitico :=
   _Biblioteca.Decode(
      Result.Status,[
        'AMA', 'Ag.manifesta��o',
        'MAN', 'Manifestada',
        'ENT', 'Entrada realizada',
        'ONR', 'Oper. n�o realizada',
        'DES', 'Desconhecido',
        'CON', 'Oper.realizada'
      ]
   );

  Result.StatusNotaAnalitico :=
   _Biblioteca.Decode(
      Result.StatusNota,[
        '1', 'Uso autorizado',
        '2', 'Uso denegado',
        '3', 'Cancelada'
      ]
   );

  Result.TipoNotaAnaltico :=
   _Biblioteca.Decode(
      Result.StatusNota,[
        'C', 'NFCe',
        'N', 'NFe'
      ]
   );
end;

function AtualizarPreEntradasNotasFiscais(
  pConexao: TConexao;
  pPreEntradaId: Integer;
  pPreEmpresaId: Integer;
  pChaveAcessoNfe: string;
  pCnpj: string;
  pRazaoSocial: string;
  pInscricaoEstadual: string;
  pDataHoraEmissao: TDateTime;
  pNumeroNota: Integer;
  pSerieNota: string;
  pValorTotalNota: Double;
  pStatus: string;
  pTipoNota: string;
  pStatusNota: string;
  pEmTransacao: Boolean
): RecRetornoBD;
var
  t: TPreEntradasNotasFiscais;
  vNovo: Boolean;
  vSeq: TSequencia;

  vSql: TConsulta;
begin
  Result.Iniciar;
  pConexao.setRotina('ATUALIZAR_PRE_ENTRADA');

  t := TPreEntradasNotasFiscais.Create(pConexao);
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  count(*) ');
  vSql.Add('from ');
  vSql.Add('  PRE_ENTRADAS_NOTAS_FISCAIS ');
  vSql.Add('where CHAVE_ACESSO_NFE = :P1 ');

  try
    if not pEmTransacao then
      pConexao.IniciarTransacao;

    vSql.Pesquisar([pChaveAcessoNfe]);
    vNovo := vSql.GetInt(0) = 0;
    if vNovo then begin
      vSeq := TSequencia.Create(pConexao, 'SEQ_PRE_ENTRADAS');
      pPreEntradaId := vSeq.getProximaSequencia;
      Result.AsInt := pPreEntradaId;
      vSeq.Free;
    end;

    t.setInt('PRE_ENTRADA_ID', pPreEntradaId, True);
    t.setInt('EMPRESA_ID', pPreEmpresaId);
    t.setString('CHAVE_ACESSO_NFE', pChaveAcessoNfe);
    t.setString('CNPJ', pCnpj);
    t.setString('RAZAO_SOCIAL', pRazaoSocial);
    t.setString('INSCRICAO_ESTADUAL', pInscricaoEstadual);
    t.setData('DATA_HORA_EMISSAO', pDataHoraEmissao);
    t.setInt('NUMERO_NOTA', pNumeroNota);
    t.setString('SERIE_NOTA', pSerieNota);
    t.setDouble('VALOR_TOTAL_NOTA', pValorTotalNota);
    t.setString('STATUS', pStatus);
    t.setString('TIPO_NOTA', pTipoNota);
    t.setString('STATUS_NOTA', pStatusNota);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    if not pEmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pEmTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;

  vSql.Active := False;
  vSql.Free;
  t.Free;
end;

function BuscarPreEntradasNotasFiscais(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecPreEntradasNotasFiscais>;
var
  i: Integer;
  t: TPreEntradasNotasFiscais;
begin
  Result := nil;
  t := TPreEntradasNotasFiscais.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordPreEntradasNotasFiscais;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarPreEntradasNotasFiscaisComando(pConexao: TConexao; pComando: string): TArray<RecPreEntradasNotasFiscais>;
var
  i: Integer;
  t: TPreEntradasNotasFiscais;
begin
  Result := nil;
  t := TPreEntradasNotasFiscais.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordPreEntradasNotasFiscais;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirPreEntradasNotasFiscais(
  pConexao: TConexao;
  pPreEntradaId: Integer
): RecRetornoBD;
var
  t: TPreEntradasNotasFiscais;
begin
  Result.Iniciar;
  t := TPreEntradasNotasFiscais.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('PRE_ENTRADA_ID', pPreEntradaId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

// STATUS_NOTA � o status em que a nota se encontra na SEFAZ
function AtualizarStatusNotaPreEntrada(pConexao: TConexao; pChaveNFe: string; pSituacao: string; pEmTransacao: Boolean): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_STATUS_PRE_ENTRADA');

  vExec := TExecucao.Create(pConexao);
  vExec.Add('update PRE_ENTRADAS_NOTAS_FISCAIS set');
  vExec.Add(' STATUS_NOTA = :P2 ');
  vExec.Add('where CHAVE_ACESSO_NFE = :P1 ');

  try
    if not pEmTransacao then
      pConexao.IniciarTransacao;

    vExec.Executar([pChaveNFe, pSituacao]);

    if not pEmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pEmTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
end;

// STATUS_NOTA � o status em que a nota se encontra no Hiva
function AtualizarStatusAltisPreEntrada(
  pConexao: TConexao;
  pPreEntradaId: Integer;
  pStatus: string;
  pXml: string;
  pMotivoOperacaoNaoRealizada: string
): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_STATUS_PRE_ENTRADA');

  vExec := TExecucao.Create(pConexao);
  vExec.Add('update PRE_ENTRADAS_NOTAS_FISCAIS set');

  if pXml <> '' then begin
    vExec.Add(' STATUS = :P2, ');
    vExec.Add(' XML_TEXTO = :P3 ');
  end
  else if pMotivoOperacaoNaoRealizada <> '' then begin
    vExec.Add(' STATUS = :P2, ');
    vExec.Add(' MOTIVO_OPERACAO_NAO_REALIZADA = :P3 ');
  end
  else
    vExec.Add(' STATUS = :P2 ');

  vExec.Add('where PRE_ENTRADA_ID = :P1 ');

  try
     pConexao.IniciarTransacao;

    if pXml <> '' then begin
      vExec.ParamByName('P1').AsInteger := pPreEntradaId;
      vExec.ParamByName('P2').AsString  := pStatus;
      vExec.CarregarClob('P3', TStringStream.Create(pXml));

      vExec.Executar;
    end
    else if pMotivoOperacaoNaoRealizada <> '' then
      vExec.Executar([pPreEntradaId, pStatus, pMotivoOperacaoNaoRealizada])
    else
      vExec.Executar([pPreEntradaId, pStatus]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
end;

end.
