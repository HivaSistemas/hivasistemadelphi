unit _RelacaoOrcamentosBloqueios;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsRelatorios;

function BuscarOrcamentosBloqueios(pConexao: TConexao; pComando: string): TArray<RecOrcamentosBloqueados>;

implementation

function BuscarOrcamentosBloqueios(pConexao: TConexao; pComando: string): TArray<RecOrcamentosBloqueados>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  with vSql.SQL do begin
    Add('select ');
    Add('  ORC.ORCAMENTO_ID, ');
    Add('  ORC.DATA_CADASTRO, ');
    Add('  ORC.CLIENTE_ID, ');
    Add('  case when PAR.CADASTRO_CONSUMIDOR_FINAL_ID = ORC.CLIENTE_ID then ORC.NOME_CONSUMIDOR_FINAL else CAD.RAZAO_SOCIAL end as NOME_CLIENTE, ');
    Add('  ORC.VENDEDOR_ID, ');
    Add('  FUN.NOME as NOME_VENDEDOR, ');
    Add('  ORC.VALOR_TOTAL, ');
    Add('  ORC.VALOR_DESCONTO / zvl((ORC.VALOR_TOTAL_PRODUTOS - ORC.VALOR_TOTAL_PRODUTOS_PROMOCAO + ORC.VALOR_OUTRAS_DESPESAS), 1) * 100 as PERCENTUAL_DESCONTO, ');
    Add('  ORC.VALOR_DESCONTO, ');
    Add('  ORC.CONDICAO_ID, ');
    Add('  CON.NOME as NOME_CONDICAO, ');
    Add('  ORC.EMPRESA_ID, ');
    Add('  EMP.NOME_FANTASIA as NOME_EMPRESA, ');
    Add('  ORC.STATUS, ');
    Add('  CUS.VALOR_CUSTO_ENTRADA, ');
    Add('  CUS.VALOR_IMPOSTOS, ');
    Add('  CUS.VALOR_CUSTO_FINAL ');
    Add('from ');
    Add('  ORCAMENTOS ORC ');

    Add('inner join CLIENTES CLI ');
    Add('on ORC.CLIENTE_ID = CLI.CADASTRO_ID ');

    Add('inner join CADASTROS CAD ');
    Add('on CLI.CADASTRO_ID = CAD.CADASTRO_ID ');

    Add('inner join FUNCIONARIOS FUN ');
    Add('on ORC.VENDEDOR_ID = FUN.FUNCIONARIO_ID ');

    Add('inner join CONDICOES_PAGAMENTO CON ');
    Add('on ORC.CONDICAO_ID = CON.CONDICAO_ID ');

    Add('inner join EMPRESAS EMP ');
    Add('on ORC.EMPRESA_ID = EMP.EMPRESA_ID ');

    Add('inner join VW_CUSTOS_ORCAMENTOS CUS ');
    Add('on ORC.ORCAMENTO_ID = CUS.ORCAMENTO_ID ');

    Add('cross join PARAMETROS PAR ');

    Add('where ORC.ORCAMENTO_ID in(select ORCAMENTO_ID from ORCAMENTOS_BLOQUEIOS)');

    Add(pComando);
  end;

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].OrcamentoId        := vSql.GetInt('ORCAMENTO_ID');
      Result[i].DataCadastro       := vSql.GetData('DATA_CADASTRO');
      Result[i].ClienteId          := vSql.GetInt('CLIENTE_ID');
      Result[i].NomeCliente        := vSql.GetString('NOME_CLIENTE');
      Result[i].VendedorId         := vSql.GetInt('VENDEDOR_ID');
      Result[i].NomeVendedor       := vSql.GetString('NOME_VENDEDOR');
      Result[i].ValorTotal         := vSql.GetDouble('VALOR_TOTAL');
      Result[i].PercentualDesconto := vSql.GetDouble('PERCENTUAL_DESCONTO');
      Result[i].ValorDesconto      := vSql.GetDouble('VALOR_DESCONTO');
      Result[i].CondicaoId         := vSql.GetInt('CONDICAO_ID');
      Result[i].NomeCondicao       := vSql.GetString('NOME_CONDICAO');
      Result[i].EmpresaId          := vSql.GetInt('EMPRESA_ID');
      Result[i].NomeEmpresa        := vSql.GetString('NOME_EMPRESA');
      Result[i].Status             := vSql.GetString('STATUS');
      Result[i].ValorCustoEntrada  := vSql.GetDouble('VALOR_CUSTO_ENTRADA');
      Result[i].ValorImpostos      := vSql.GetDouble('VALOR_IMPOSTOS');
      Result[i].ValorCustoFinal    := vSql.GetDouble('VALOR_CUSTO_FINAL');

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

end.
