unit _MovimentosContas;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecMovimentosContas = record
    MovimentoId: Integer;
    ContaId: string;
    ContaDestinoId: string;
    ValorMovimento: Double;
    DataHoraMovimento: TDateTime;
    TipoMovimento: string;
    TipoMovimentoAnalitico: string;
    PlanoFinanceiroId: string;
    CentroCustoId: Integer;

    BaixaReceberId: Integer;
    BaixaPagarId: Integer;

    UsuarioMovimentoId: Integer;
    NomeUsuario: string;
    Conciliado: string;
    UsuarioConciliacaoId: Integer;
    NomeUsuarioConciliacao: string;
    DataHoraConciliacao: TDateTime;
    MovimentoIdBanco: string;

    Natureza: string;
  end;

  TMovimentosContas = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordMovimento: RecMovimentosContas;
  end;

function BuscarMovimentos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecMovimentosContas>;

function AtualizarMovimentos(
  pConexao: TConexao;
  pMovimentoId: Integer;
  pContaId: string;
  pDestinoContaId: string;
  pValorMovimento: Double;
  pDataHoraMovimento: TDateTime;
  pTipoMovimento: string;
  pPlanoFinanceiroId: string;
  pCentroCustoId: Integer;
  pEmTransacao: Boolean
): RecRetornoBD;

function BuscarSaldoInicialConta(pConexao: TConexao; pContaId: string; pDataInicial: TDateTime; pApenasConciliados: Boolean): Double;
function BuscarMovimentosContas(pConexao: TConexao; pComando: string): TArray<RecMovimentosContas>;
function AtualizarMovimentosConciliacao(
  pConexao: TConexao;
  pConciliar: Boolean;
  pMovimentosReceberIds: TArray<Integer>;
  pMovimentosPagarIds: TArray<Integer>
): RecRetornoBD;

function ExcluirMovimento(pConexao: TConexao; pMovimentoId: Integer): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TMovimentosContas }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where MOV.CONTA_ID = :P1 '
    );
end;

constructor TMovimentosContas.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'MOVIMENTOS_CONTAS');

  FSql :=
    'select ' +
    '  MOV.MOVIMENTO_ID, ' +
    '  MOV.CONTA_ID, ' +
    '  MOV.CONTA_DESTINO_ID, ' +
    '  MOV.VALOR_MOVIMENTO, ' +
    '  MOV.DATA_HORA_MOVIMENTO, ' +
    '  MOV.TIPO_MOVIMENTO, ' +
    '  MOV.PLANO_FINANCEIRO_ID, ' +
    '  MOV.CENTRO_CUSTO_ID, ' +
    '  MOV.BAIXA_RECEBER_ID, ' +
    '  MOV.BAIXA_PAGAR_ID, ' +
    '  MOV.MOVIMENTO_ID_BANCO ' +
    'from ' +
    '  MOVIMENTOS_CONTAS MOV ';

  setFiltros(getFiltros);

  AddColuna('MOVIMENTO_ID', True);
  AddColuna('CONTA_ID');
  AddColuna('CONTA_DESTINO_ID');
  AddColuna('VALOR_MOVIMENTO');
  AddColuna('DATA_HORA_MOVIMENTO');
  AddColuna('TIPO_MOVIMENTO');
  AddColuna('PLANO_FINANCEIRO_ID');
  AddColuna('CENTRO_CUSTO_ID');
  AddColunaSL('BAIXA_RECEBER_ID');
  AddColunaSL('BAIXA_PAGAR_ID');
  AddColunaSL('MOVIMENTO_ID_BANCO');
end;

function TMovimentosContas.getRecordMovimento: RecMovimentosContas;
begin
  Result.MovimentoId       := getInt('MOVIMENTO_ID', True);
  Result.ContaId           := getString('CONTA_ID');
  Result.ContaDestinoId    := getString('CONTA_DESTINO_ID');
  Result.ValorMovimento    := getDouble('VALOR_MOVIMENTO');
  Result.DataHoraMovimento := getDouble('DATA_HORA_MOVIMENTO');
  Result.TipoMovimento     := getString('TIPO_MOVIMENTO');
  Result.PlanoFinanceiroId := getString('PLANO_FINANCEIRO_ID');
  Result.CentroCustoId     := getInt('CENTRO_CUSTO_ID');
  Result.BaixaReceberId    := getInt('BAIXA_RECEBER_ID');
  Result.BaixaPagarId      := getInt('BAIXA_PAGAR_ID');
  Result.MovimentoIdBanco  := getString('MOVIMENTO_ID_BANCO');
end;

function BuscarMovimentos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecMovimentosContas>;
var
  i: Integer;
  t: TMovimentosContas;
begin
  Result := nil;
  t := TMovimentosContas.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordMovimento;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function AtualizarMovimentos(
  pConexao: TConexao;
  pMovimentoId: Integer;
  pContaId: string;
  pDestinoContaId: string;
  pValorMovimento: Double;
  pDataHoraMovimento: TDateTime;
  pTipoMovimento: string;
  pPlanoFinanceiroId: string;
  pCentroCustoId: Integer;
  pEmTransacao: Boolean
): RecRetornoBD;
var
  t: TMovimentosContas;
  vNovo: Boolean;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_MOVIMENTOS_CONTA');
  t := TMovimentosContas.Create(pConexao);

  vNovo := pMovimentoId = 0;
  if vNovo then begin
    pMovimentoId := TSequencia.Create(pConexao, 'SEQ_MOVIMENTOS_CONTAS').GetProximaSequencia;
    Result.AsInt := pMovimentoId;
  end;

  try
    if not pEmTransacao then
      pConexao.IniciarTransacao;

    t.setInt('MOVIMENTO_ID', pMovimentoId, True);
    t.setString('CONTA_ID', pContaId);
    t.setString('CONTA_DESTINO_ID', pDestinoContaId);
    t.setDouble('VALOR_MOVIMENTO', pValorMovimento);
    t.setData('DATA_HORA_MOVIMENTO', pDataHoraMovimento);
    t.setString('TIPO_MOVIMENTO', pTipoMovimento);
    t.setStringN('PLANO_FINANCEIRO_ID', pPlanoFinanceiroId);
    t.setIntN('CENTRO_CUSTO_ID', pCentroCustoId);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    if not pEmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: exception do begin
      if not pEmTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;
  t.Free;
end;

function BuscarSaldoInicialConta(pConexao: TConexao; pContaId: string; pDataInicial: TDateTime; pApenasConciliados: Boolean): Double;
var
  p: TConsulta;
begin
  Result := 0;
  p := TConsulta.Create(pConexao);

  p.Add('select ');
  p.Add('  sum(VALOR_MOVIMENTO * case when NATUREZA = ''E'' then 1 else -1 end) as SALDO ');
  p.Add('from ');
  p.Add('  VW_MOVIMENTOS_CONTAS ');
  p.Add('where CONTA_ID = :P1 ');
  p.Add('and trunc(DATA_HORA_MOVIMENTO) < :P2 ');

  if pApenasConciliados then
    p.Add('and CONCILIADO = ''S'' ');

  if p.Pesquisar([pContaId, pDataInicial]) then
    Result := p.GetDouble('SALDO');

  p.Free;
end;

function BuscarMovimentosContas(pConexao: TConexao; pComando: string): TArray<RecMovimentosContas>;
var
  i: Integer;
  p: TConsulta;
begin
  Result := nil;
  p := TConsulta.Create(pConexao);

  p.Add('select ');
  p.Add('  MOV.TIPO_MOVIMENTO, ');
  p.Add('  MOV.MOVIMENTO_ID, ');
  p.Add('  MOV.DATA_HORA_MOVIMENTO, ');
  p.Add('  MOV.CONTA_ID, ');
  p.Add('  MOV.VALOR_MOVIMENTO, ');
  p.Add('  MOV.NATUREZA, ');
  p.Add('  MOV.BAIXA_RECEBER_ID, ');
  p.Add('  MOV.BAIXA_PAGAR_ID, ');
  p.Add('  MOV.USUARIO_MOVIMENTO_ID, ');
  p.Add('  FUN.NOME as NOME_USUARIO, ');
  p.Add('  MOV.CONCILIADO, ');
  p.Add('  MOV.USUARIO_CONCILIACAO_ID, ');
  p.Add('  MOV.DATA_HORA_CONCILIACAO, ');
  p.Add('  FCO.NOME as NOME_USUARIO_CONCILIACAO, ');
  p.Add('  MOV.MOVIMENTO_ID_BANCO ');
  p.Add('from ');
  p.Add('  VW_MOVIMENTOS_CONTAS MOV ');

  p.Add('inner join FUNCIONARIOS FUN ');
  p.Add('on MOV.USUARIO_MOVIMENTO_ID = FUN.FUNCIONARIO_ID ');

  p.Add('left join FUNCIONARIOS FCO ');
  p.Add('on MOV.USUARIO_CONCILIACAO_ID = FCO.FUNCIONARIO_ID ');

  p.Add(pComando);

  if p.Pesquisar then begin
    SetLength(Result, p.GetQuantidadeRegistros);
    for i := 0 to p.GetQuantidadeRegistros - 1 do begin
      Result[i].TipoMovimento        := p.GetString('TIPO_MOVIMENTO');
      Result[i].MovimentoId          := p.GetInt('MOVIMENTO_ID');
      Result[i].DataHoraMovimento    := p.GetData('DATA_HORA_MOVIMENTO');
      Result[i].ContaId              := p.GetString('CONTA_ID');
      Result[i].ValorMovimento       := p.getDouble('VALOR_MOVIMENTO');
      Result[i].Natureza             := p.GetString('NATUREZA');
      Result[i].BaixaReceberId       := p.GetInt('BAIXA_RECEBER_ID');
      Result[i].BaixaPagarId         := p.GetInt('BAIXA_PAGAR_ID');
      Result[i].UsuarioMovimentoId   := p.GetInt('USUARIO_MOVIMENTO_ID');
      Result[i].NomeUsuario          := p.GetString('NOME_USUARIO');
      Result[i].Conciliado           := p.GetString('CONCILIADO');
      Result[i].UsuarioConciliacaoId := p.GetInt('USUARIO_CONCILIACAO_ID');
      Result[i].NomeUsuarioConciliacao := p.GetString('NOME_USUARIO_CONCILIACAO');
      Result[i].DataHoraConciliacao    := p.GetData('DATA_HORA_CONCILIACAO');
      Result[i].MovimentoIdBanco       := p.GetString('MOVIMENTO_ID_BANCO');

      Result[i].TipoMovimentoAnalitico :=
        _Biblioteca.Decode(Result[i].TipoMovimento, [
          'ENM', 'Entrada manual',
          'SAM', 'Sa�da manual',
          'TRA', 'Transfer�ncia',
          'BXR', 'Baixa contas receber',
          'BXP', 'Baixa contas pagar',
          'ATC', 'Abertura de turno',
          'SAN', 'Sangria de turno',
          'SUP', 'Suprimento de turno',
          'FEC', 'Fechamento de turno',
          'PIX', 'Pix']
        );

      p.Next;
    end;
  end;

  p.Free;
end;

function AtualizarMovimentosConciliacao(
  pConexao: TConexao;
  pConciliar: Boolean;
  pMovimentosReceberIds: TArray<Integer>;
  pMovimentosPagarIds: TArray<Integer>
): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_CONCILIACAO');
  vExec := TExecucao.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    if pMovimentosReceberIds <> nil then begin
      vExec.Limpar;
      vExec.Add('update MOVIMENTOS_CONTAS set');
      vExec.Add('  CONCILIADO = :P1 ');
      vExec.Add('where ' + FiltroInInt('MOVIMENTO_ID', pMovimentosReceberIds));

      vExec.Executar([ IIfStr(pConciliar, 'S', 'N') ]);
    end;

    if pMovimentosPagarIds <> nil then begin
      vExec.Limpar;
      vExec.Add('update MOVIMENTOS_CONTAS set');
      vExec.Add('  CONCILIADO = :P1 ');
      vExec.Add('where ' + FiltroInInt('MOVIMENTO_ID', pMovimentosPagarIds));

      vExec.Executar([ IIfStr(pConciliar, 'S', 'N') ]);
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
end;

function ExcluirMovimento(pConexao: TConexao; pMovimentoId: Integer): RecRetornoBD;
var
  t: TMovimentosContas;
begin
  Result.Iniciar;
  pConexao.SetRotina('EXCLUIR_MOVIMENTO_CONTA');
  t := TMovimentosContas.Create(pConexao);

  t.SetInt('MOVIMENTO_ID', pMovimentoId, True);

  try
    pConexao.IniciarTransacao;

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
