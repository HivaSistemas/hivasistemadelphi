unit _Acumulados;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _AcumuladosCreditos, _AcumuladosItens, _RecordsFinanceiros,
  _AcumuladosPagamentos, _AcumuladosPagamentosCheques, _AcumuladosOrcamentos;

{$M+}
type
  RecAcumulados = record
    AcumuladoId: Integer;
    EmpresaId: Integer;
    NomeEmpresa: string;
    ClienteId: Integer;
    NomeCliente: string;
    TelefonePrincipal: string;
    TelefoneCelular: string;
    CondicaoId: Integer;
    NomeCondicaoPagamento: string;
    TurnoId: Integer;
    IndiceDescontoVendaId: Integer;
    NomeIndDescontoVenda: string;
    DataHoraFechamento: TDateTime;
    UsuarioFechamentoId: Integer;
    NomeUsuarioFechamento: string;
    DataHoraRecebimento: TDateTime;
    UsuarioRecebimentoId: Integer;
    NomeUsuarioRecebimento: string;
    Status: string;
    StatusAnalitico: string;
    TipoNotaGerar: string;
    ValorTotalProdutos: Double;
    ValorTotal: Double;
    ValorCustoEntrada: Double;
    ValorCustoFinal: Double;
    ValorOutrasDespesas: Double;
    ValorDesconto: Double;
    ValorJuros: Double;
    ValorFrete: Double;
    IndicePedidos: Double;
    TemProdutos: string;
    ValorDinheiro: Double;
    ValorCheque: Double;
    ValorCartaoDebito: Double;
    ValorCartaoCredito: Double;
    ValorCobranca: Double;
    ValorFinanceira: Double;
    ValorCredito: Double;
    ValorPix: Double;
    ValorTroco: Double;
    ObservacoesNfe: string;
    Logradouro: string;
    Complemento: string;
    Numero: string;
    PontoReferencia: string;
    BairroId: Integer;
    Cep: string;
    InscricaoEstadual: string;
    PrazoMedioCondicaoPagto: Double;
  end;

  TAcumulados = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordAcumulados: RecAcumulados;
  end;

function AtualizarAcumulados(
  pConexao: TConexao;
  pAcumuladoId: Integer;
  pEmpresaId: Integer;
  pClienteId: Integer;
  pCondicaoId: Integer;
  pIndiceDescontoVendaId: Integer;
  pStatus: string;
  pValorTotalProdutos: Double;
  pValorTotal: Double;
  pValorOutrasDespesas: Double;
  pValorDesconto: Double;
  pValorFrete: Double;
  pValorJuros: Double;
  pIndicePedidos: Double;
  pTemProdutos: string;
  pValorDinheiro: Double;
  pValorCheque: Double;
  pValorCartaoDebito: Double;
  pValorCartaoCredito: Double;
  pValorCobranca: Double;
  pValorFinanceira: Double;
  pValorCredito: Double;
  pValorPix: Double;
  pValorTroco: Double;
  pObservacoesNfe: string;
  pLograuro: string;
  pComplemento: string;
  pNumero: string;
  pPontoReferencia: string;
  pBairroId: Integer;
  pCep: string;
  pInscricaoEstadual: string;
  pItens: TArray<RecAcumuladosItens>;
  pCreditos: TArray<Integer>;
  pPedidosIds: TArray<Integer>;
  pEmTransacao: Boolean
): RecRetornoBD;

function BuscarAcumulados(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecAcumulados>;

function AtualizarAcumuladosPagamentosPix(
  pConexao: TConexao;
  pAcumuladoId: Integer;
  pPagamentosPix: TArray<RecPagamentosPix>;
  pEmpresaId: Integer;
  pCadastroId: Integer;
  pTurnoId: Integer;
  pDataPagamento: TDateTime
): RecRetornoBD;

function BuscarAcumuladosComando(pConexao: TConexao; pComando: string): TArray<RecAcumulados>;

function ExcluirAcumulados(
  pConexao: TConexao;
  pAcumuladoId: Integer
): RecRetornoBD;

function CancelarFechamentoAcumulado(pConexao: TConexao; pAcumuladoId: Integer): RecRetornoBD;

function AtualizarValoresPagamentos(
  pConexao: TConexao;
  pAcumuladoId: Integer;
  pValorDinheiro: Double;
  pValorCheque: Double;
  pValorCartaoDebito: Double;
  pValorCartaoCredito: Double;
  pValorCobranca: Double;
  pValorFinanceira: Double;
  pValorCredito: Double
): RecRetornoBD;

function AtualizarAcumuladoPagamentos(
  pConexao: TConexao;
  pAcumuladoId: Integer;
  pPagamentosCheques: TArray<RecTitulosFinanceiros>;
  pPagamentosCartoes: TArray<RecTitulosFinanceiros>;
  pPagamentosCobrancas: TArray<RecTitulosFinanceiros>;
  pPagamentosFinanceiras: TArray<RecTitulosFinanceiros>;
  pEmTransacao: Boolean
): RecRetornoBD;

function ReceberAcumulado(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pTurnoId: Integer;
  pFuncionarioId: Integer;
  pGerarCreditoTroco: Boolean;
  pValorTroco: Currency;
  pTipoNotaGerar: string
): RecRetornoBD;

function PodeCancelarRecebimentoAcumulado(
  pConexao: TConexao;
  pAcumuladoId: Integer
): RecRetornoBD;

function CancelarRecebimentoAcumulado(pConexao: TConexao; pAcumuladoId: Integer): RecRetornoBD;
function AtualizarIndiceDescontoVenda(pConexao: TConexao; pAcumuladoId: Integer; pIndiceDescontoVendaId: Integer): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TAcumulados }

uses _ContasRecBaixasPagtosPix, _ContasReceberBaixas;

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 3);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ACU.ACUMULADO_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ACU.EMPRESA_ID = :P1 ' +
      'and ACU.STATUS = ''AR'' ',
      'order by ' +
      '  ACU.ACUMULADO_ID desc '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ACU.ACUMULADO_ID = :P1 ' +
      'and ACU.STATUS = ''RE'' '
    );
end;

constructor TAcumulados.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ACUMULADOS');

  //Coment�rio para demonstrar altera��o no arquivo
  FSql := 
    'select ' +
    '  ACU.ACUMULADO_ID, ' +
    '  ACU.EMPRESA_ID, ' +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA, ' +
    '  ACU.CLIENTE_ID, ' +
    '  CAD.NOME_FANTASIA as NOME_CLIENTE, ' +
    '  CAD.TELEFONE_PRINCIPAL, ' +
    '  CAD.TELEFONE_CELULAR, ' +
    '  ACU.CONDICAO_ID, ' +
    '  CON.NOME as NOME_CONDICAO_PAGAMENTO, ' +
    '  ACU.TURNO_ID, ' +
    '  ACU.INDICE_DESCONTO_VENDA_ID, ' +
    '  IDV.DESCRICAO as NOME_IND_DESCONTO_VENDA, ' +
    '  ACU.DATA_HORA_FECHAMENTO, ' +
    '  ACU.DATA_HORA_RECEBIMENTO, ' +
    '  ACU.USUARIO_FECHAMENTO_ID, ' +
    '  nvl(FFE.APELIDO, FFE.NOME) as NOME_USUARIO_FECHAMENTO, ' +
    '  ACU.DATA_HORA_RECEBIMENTO, ' +
    '  ACU.USUARIO_RECEBIMENTO_ID, ' +
    '  nvl(FRE.APELIDO, FRE.NOME) as NOME_USUARIO_RECEBIMENTO, ' +
    '  ACU.STATUS, ' +
    '  ACU.TIPO_NOTA_GERAR, ' +
    '  ACU.VALOR_TOTAL_PRODUTOS, ' +
    '  ACU.VALOR_TOTAL, ' +
    '  ACU.VALOR_OUTRAS_DESPESAS, ' +
    '  ACU.VALOR_DESCONTO, ' +
    '  ACU.VALOR_JUROS, ' +
    '  ACU.VALOR_FRETE, ' +
    '  ACU.INDICE_PEDIDOS, ' +
    '  ACU.TEM_PRODUTOS, ' +
    '  ACU.VALOR_DINHEIRO, ' +
    '  ACU.VALOR_CHEQUE, ' +
    '  ACU.VALOR_CARTAO_DEBITO, ' +
    '  ACU.VALOR_CARTAO_CREDITO, ' +
    '  ACU.VALOR_COBRANCA, ' +
    '  ACU.VALOR_FINANCEIRA, ' +
    '  ACU.VALOR_CREDITO, ' +
    '  ACU.VALOR_PIX, ' +
    '  ACU.VALOR_TROCO, ' +
    '  ACU.OBSERVACOES_NFE, ' +
    '  ACU.LOGRADOURO, ' +
    '  ACU.COMPLEMENTO, ' +
    '  ACU.NUMERO, ' +
    '  ACU.PONTO_REFERENCIA, ' +
    '  ACU.BAIRRO_ID, ' +
    '  ACU.CEP, ' +
    '  ACU.INSCRICAO_ESTADUAL, ' +
    '  CON.PRAZO_MEDIO, ' +
    '  CUS.VALOR_CUSTO_ENTRADA, ' +
    '  CUS.VALOR_CUSTO_FINAL ' +
    'from ' +
    '  ACUMULADOS ACU ' +

    'inner join CADASTROS CAD ' +
    'on ACU.CLIENTE_ID = CAD.CADASTRO_ID ' +

    'inner join FUNCIONARIOS FFE ' +
    'on ACU.USUARIO_FECHAMENTO_ID = FFE.FUNCIONARIO_ID ' +

    'inner join CONDICOES_PAGAMENTO CON ' +
    'on ACU.CONDICAO_ID = CON.CONDICAO_ID ' +

    'inner join VW_CUSTOS_ACUMULADOS CUS ' +
    'on ACU.ACUMULADO_ID = CUS.ACUMULADO_ID ' +

    'inner join EMPRESAS EMP ' +
    'on ACU.EMPRESA_ID = EMP.EMPRESA_ID ' +

    'left join FUNCIONARIOS FRE ' +
    'on ACU.USUARIO_RECEBIMENTO_ID = FRE.FUNCIONARIO_ID ' +

    'left join INDICES_DESCONTOS_VENDA IDV ' +
    'on ACU.INDICE_DESCONTO_VENDA_ID = IDV.INDICE_ID ';

  setFiltros(getFiltros);

  AddColuna('ACUMULADO_ID', True);
  AddColuna('EMPRESA_ID');
  AddColunaSL('NOME_EMPRESA');
  AddColuna('CLIENTE_ID');
  AddColunaSL('NOME_CLIENTE');
  AddColunaSL('TELEFONE_PRINCIPAL');
  AddColunaSL('TELEFONE_CELULAR');
  AddColuna('CONDICAO_ID');
  AddColunaSL('NOME_CONDICAO_PAGAMENTO');
  AddColunaSL('TURNO_ID');
  AddColuna('INDICE_DESCONTO_VENDA_ID');
  AddColunaSL('NOME_IND_DESCONTO_VENDA');
  AddColunaSL('DATA_HORA_FECHAMENTO');
  AddColunaSL('USUARIO_FECHAMENTO_ID');
  AddColunaSL('NOME_USUARIO_FECHAMENTO');
  AddColunaSL('DATA_HORA_RECEBIMENTO');
  AddColunaSL('USUARIO_RECEBIMENTO_ID');
  AddColunaSL('NOME_USUARIO_RECEBIMENTO');
  AddColuna('STATUS');
  AddColunaSL('TIPO_NOTA_GERAR');
  AddColunaSL('VALOR_CUSTO_ENTRADA');
  AddColunaSL('VALOR_CUSTO_FINAL');
  AddColuna('VALOR_TOTAL_PRODUTOS');
  AddColuna('VALOR_TOTAL');
  AddColuna('VALOR_OUTRAS_DESPESAS');
  AddColuna('VALOR_DESCONTO');
  AddColuna('VALOR_JUROS');
  AddColuna('VALOR_FRETE');
  AddColuna('INDICE_PEDIDOS');
  AddColuna('TEM_PRODUTOS');
  AddColuna('VALOR_DINHEIRO');
  AddColuna('VALOR_CHEQUE');
  AddColuna('VALOR_CARTAO_DEBITO');
  AddColuna('VALOR_CARTAO_CREDITO');
  AddColuna('VALOR_COBRANCA');
  AddColuna('VALOR_FINANCEIRA');
  AddColuna('VALOR_CREDITO');
  AddColuna('VALOR_PIX');
  AddColuna('VALOR_TROCO');
  AddColuna('OBSERVACOES_NFE');
  AddColuna('LOGRADOURO');
  AddColuna('COMPLEMENTO');
  AddColuna('NUMERO');
  AddColuna('PONTO_REFERENCIA');
  AddColuna('BAIRRO_ID');
  AddColuna('CEP');
  AddColuna('INSCRICAO_ESTADUAL');
  AddColunaSL('PRAZO_MEDIO');
  AddColunaSL('DATA_HORA_RECEBIMENTO');
end;

function TAcumulados.getRecordAcumulados: RecAcumulados;
begin
  Result.AcumuladoId            := getInt('ACUMULADO_ID', True);
  Result.EmpresaId              := getInt('EMPRESA_ID');
  Result.NomeEmpresa            := getString('NOME_EMPRESA');
  Result.ClienteId              := getInt('CLIENTE_ID');
  Result.NomeCliente            := getString('NOME_CLIENTE');
  Result.TelefonePrincipal      := getString('TELEFONE_PRINCIPAL');
  Result.TelefoneCelular        := getString('TELEFONE_CELULAR');
  Result.CondicaoId             := getInt('CONDICAO_ID');
  Result.NomeCondicaoPagamento  := getString('NOME_CONDICAO_PAGAMENTO');
  Result.TurnoId                := getInt('TURNO_ID');
  Result.IndiceDescontoVendaId  := getInt('INDICE_DESCONTO_VENDA_ID');
  Result.NomeIndDescontoVenda   := getString('NOME_IND_DESCONTO_VENDA');
  Result.DataHoraFechamento     := getData('DATA_HORA_FECHAMENTO');
  Result.UsuarioFechamentoId    := getInt('USUARIO_FECHAMENTO_ID');
  Result.NomeUsuarioFechamento  := getString('NOME_USUARIO_FECHAMENTO');
  Result.DataHoraRecebimento    := getData('DATA_HORA_RECEBIMENTO');
  Result.UsuarioRecebimentoId   := getInt('USUARIO_RECEBIMENTO_ID');
  Result.NomeUsuarioRecebimento := getString('NOME_USUARIO_RECEBIMENTO');
  Result.Status                 := getString('STATUS');
  Result.TipoNotaGerar          := getString('TIPO_NOTA_GERAR');
  Result.ValorTotalProdutos     := getDouble('VALOR_TOTAL_PRODUTOS');
  Result.ValorTotal             := getDouble('VALOR_TOTAL');
  Result.ValorCustoEntrada      := getDouble('VALOR_CUSTO_ENTRADA');
  Result.ValorCustoFinal        := getDouble('VALOR_CUSTO_FINAL');
  Result.ValorOutrasDespesas    := getDouble('VALOR_OUTRAS_DESPESAS');
  Result.ValorDesconto          := getDouble('VALOR_DESCONTO');
  Result.ValorJuros             := getDouble('VALOR_JUROS');
  Result.ValorFrete             := getDouble('VALOR_FRETE');
  Result.IndicePedidos          := getDouble('INDICE_PEDIDOS');
  Result.TemProdutos            := getString('TEM_PRODUTOS');
  Result.ValorDinheiro          := getDouble('VALOR_DINHEIRO');
  Result.ValorCheque            := getDouble('VALOR_CHEQUE');
  Result.ValorCartaoDebito      := getDouble('VALOR_CARTAO_DEBITO');
  Result.ValorCartaoCredito     := getDouble('VALOR_CARTAO_CREDITO');
  Result.ValorCobranca          := getDouble('VALOR_COBRANCA');
  Result.ValorFinanceira        := getDouble('VALOR_FINANCEIRA');
  Result.ValorCredito           := getDouble('VALOR_CREDITO');
  Result.ValorPix               := getDouble('VALOR_PIX');
  Result.ValorTroco             := getDouble('VALOR_TROCO');
  Result.ObservacoesNfe         := getString('OBSERVACOES_NFE');
  Result.Logradouro             := getString('LOGRADOURO');
  Result.Complemento            := getString('COMPLEMENTO');
  Result.Numero                 := getString('NUMERO');
  Result.PontoReferencia        := getString('PONTO_REFERENCIA');
  Result.BairroId               := getInt('BAIRRO_ID');
  Result.Cep                    := getString('CEP');
  Result.InscricaoEstadual      := getString('INSCRICAO_ESTADUAL');
  Result.PrazoMedioCondicaoPagto:= getDouble('PRAZO_MEDIO');
  Result.DataHoraRecebimento    := getData('DATA_HORA_RECEBIMENTO');

  Result.StatusAnalitico := IIfStr( Result.Status = 'AR', 'Aguardando recebimento', 'Recebido' );
end;

function AtualizarAcumulados(
  pConexao: TConexao;
  pAcumuladoId: Integer;
  pEmpresaId: Integer;
  pClienteId: Integer;
  pCondicaoId: Integer;
  pIndiceDescontoVendaId: Integer;
  pStatus: string;
  pValorTotalProdutos: Double;
  pValorTotal: Double;
  pValorOutrasDespesas: Double;
  pValorDesconto: Double;
  pValorFrete: Double;
  pValorJuros: Double;
  pIndicePedidos: Double;
  pTemProdutos: string;
  pValorDinheiro: Double;
  pValorCheque: Double;
  pValorCartaoDebito: Double;
  pValorCartaoCredito: Double;
  pValorCobranca: Double;
  pValorFinanceira: Double;
  pValorCredito: Double;
  pValorPix: Double;
  pValorTroco: Double;
  pObservacoesNfe: string;
  pLograuro: string;
  pComplemento: string;
  pNumero: string;
  pPontoReferencia: string;
  pBairroId: Integer;
  pCep: string;
  pInscricaoEstadual: string;
  pItens: TArray<RecAcumuladosItens>;
  pCreditos: TArray<Integer>;
  pPedidosIds: TArray<Integer>;
  pEmTransacao: Boolean
): RecRetornoBD;
var
  i: Integer;
  t: TAcumulados;
  vNovo: Boolean;
  vSeq: TSequencia;
  vExec: TExecucao;
  vItem: TAcumuladosItens;
  vProc: TProcedimentoBanco;
  vCreditos: TAcumuladosCreditos;
  vAcumuladosOrcs: TAcumuladosOrcamentos;
begin
  Result.Iniciar;
  pConexao.SetRotina('FECHAR_ACUMULADOS');

  t := TAcumulados.Create(pConexao);
  vExec := TExecucao.Create(pConexao);
  vItem := TAcumuladosItens.Create(pConexao);
  vCreditos := TAcumuladosCreditos.Create(pConexao);
  vAcumuladosOrcs := TAcumuladosOrcamentos.Create(pConexao);

  vProc := TProcedimentoBanco.Create(pConexao, 'FECHAR_ACUMULADO');

  vNovo := pAcumuladoId = 0;
  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_ACUMULADOS');
    pAcumuladoId := vSeq.getProximaSequencia;
    Result.AsInt := pAcumuladoId;
    vSeq.Free;
  end;

  try
    if not pEmTransacao then
      pConexao.IniciarTransacao;

    t.setInt('ACUMULADO_ID', pAcumuladoId, True);
    t.setInt('EMPRESA_ID', pEmpresaId);
    t.setInt('CLIENTE_ID', pClienteId);
    t.setInt('CONDICAO_ID', pCondicaoId);
    t.setIntN('INDICE_DESCONTO_VENDA_ID', pIndiceDescontoVendaId);
    t.setString('STATUS', pStatus);
    t.setDouble('VALOR_TOTAL_PRODUTOS', pValorTotalProdutos);
    t.setDouble('VALOR_TOTAL', pValorTotal);
    t.setDouble('VALOR_OUTRAS_DESPESAS', pValorOutrasDespesas);
    t.setDouble('VALOR_DESCONTO', pValorDesconto);
    t.setDouble('VALOR_JUROS', pValorJuros);
    t.setDouble('VALOR_FRETE', pValorFrete);
    t.setDouble('INDICE_PEDIDOS', pIndicePedidos);
    t.setString('TEM_PRODUTOS', pTemProdutos);
    t.setDouble('VALOR_DINHEIRO', pValorDinheiro);
    t.setDouble('VALOR_CHEQUE', pValorCheque);
    t.setDouble('VALOR_CARTAO_DEBITO', pValorCartaoDebito);
    t.setDouble('VALOR_CARTAO_CREDITO', pValorCartaoCredito);
    t.setDouble('VALOR_COBRANCA', pValorCobranca);
    t.setDouble('VALOR_FINANCEIRA', pValorFinanceira);
    t.setDouble('VALOR_CREDITO', pValorCredito);
    t.setDouble('VALOR_PIX', pValorPix);
    t.setDouble('VALOR_TROCO', pValorTroco);
    t.setString('OBSERVACOES_NFE', pObservacoesNfe);
    t.setString('LOGRADOURO', pLograuro);
    t.setString('COMPLEMENTO', pComplemento);
    t.setString('NUMERO', pNumero);
    t.setString('PONTO_REFERENCIA', pPontoReferencia);
    t.setInt('BAIRRO_ID', pBairroId);
    t.setString('CEP', pCep);
    t.setString('INSCRICAO_ESTADUAL', pInscricaoEstadual);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    vExec.Clear;
    vExec.Add('delete from ACUMULADOS_ITENS ');
    vExec.Add('where ACUMULADO_ID = :P1 ');
    vExec.Executar([pAcumuladoId]);

    vItem.setInt('ACUMULADO_ID', pAcumuladoId, True);
    for i := Low(pItens) to High(pItens) do begin
      vItem.setInt('PRODUTO_ID', pItens[i].ProdutoId, True);
      vItem.setDouble('PRECO_UNITARIO', pItens[i].PrecoUnitario);
      vItem.setDouble('QUANTIDADE', pItens[i].Quantidade);
      vItem.setDouble('VALOR_TOTAL', pItens[i].ValorTotal);
      vItem.setDouble('VALOR_TOTAL_DESCONTO', pItens[i].ValorTotalDesconto);
      vItem.setDouble('VALOR_TOTAL_OUTRAS_DESPESAS', pItens[i].ValorTotalOutrasDespesas);
      vItem.setDouble('VALOR_TOTAL_FRETE', pItens[i].ValorTotalFrete);

      vItem.setDouble('VALOR_IMPOSTOS', pItens[i].ValorImpostos);
      vItem.setDouble('VALOR_ENCARGOS', pItens[i].ValorEncargos);
      vItem.setDouble('VALOR_CUSTO_VENDA', pItens[i].ValorCustoVenda);
      vItem.setDouble('VALOR_CUSTO_FINAL', pItens[i].ValorCustoFinal);

      vItem.Inserir;
    end;

    for i := Low(pCreditos) to High(pCreditos) do begin
      vCreditos.setInt('ACUMULADO_ID', pAcumuladoId, True);
      vCreditos.setInt('PAGAR_ID', pCreditos[i], True);

      vCreditos.Inserir;
    end;

    vAcumuladosOrcs.setInt('ACUMULADO_ID', pAcumuladoId, True);
    for i := Low(pPedidosIds) to High(pPedidosIds) do begin
      vAcumuladosOrcs.setInt('ORCAMENTO_ID', pPedidosIds[i], True);
      vAcumuladosOrcs.Inserir;
    end;

    vProc.Params[0].AsInteger := pAcumuladoId;
    vProc.Executar;

    if not pEmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pEmTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;

  vAcumuladosOrcs.Free;
  vCreditos.Free;
  vProc.Free;
  vItem.Free;
  vExec.Free;
  t.Free;
end;

function BuscarAcumulados(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecAcumulados>;
var
  i: Integer;
  t: TAcumulados;
begin
  Result := nil;
  t := TAcumulados.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordAcumulados;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarAcumuladosComando(pConexao: TConexao; pComando: string): TArray<RecAcumulados>;
var
  i: Integer;
  t: TAcumulados;
begin
  Result := nil;
  t := TAcumulados.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordAcumulados;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirAcumulados(
  pConexao: TConexao;
  pAcumuladoId: Integer
): RecRetornoBD;
var
  t: TAcumulados;
begin
  Result.Iniciar;
  t := TAcumulados.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('ACUMULADO_ID', pAcumuladoId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function CancelarFechamentoAcumulado(pConexao: TConexao; pAcumuladoId: Integer): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('CANCELAR_FECHAMENTO_ACUMULADO');

  vProc := TProcedimentoBanco.Create(pConexao, 'CANCELAR_FECHAMENTO_ACUMULADO');
  try
    pConexao.IniciarTransacao;

    vProc.Executar([pAcumuladoId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vProc.Free;
end;

function AtualizarValoresPagamentos(
  pConexao: TConexao;
  pAcumuladoId: Integer;
  pValorDinheiro: Double;
  pValorCheque: Double;
  pValorCartaoDebito: Double;
  pValorCartaoCredito: Double;
  pValorCobranca: Double;
  pValorFinanceira: Double;
  pValorCredito: Double
): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  vExec := TExecucao.Create(pConexao);
  try
    vExec.Add('update ACUMULADOS set ');
    vExec.Add('  VALOR_DINHEIRO = :P2, ');
    vExec.Add('  VALOR_CHEQUE = :P3, ');
    vExec.Add('  VALOR_COBRANCA = :P4, ');
    vExec.Add('  VALOR_CARTAO_DEBITO = :P5, ');
    vExec.Add('  VALOR_CARTAO_CREDITO = :P6, ');
    vExec.Add('  VALOR_CREDITO = :P7, ');
    vExec.Add('  VALOR_FINANCEIRA = :P8 ');
    vExec.Add('where ACUMULADO_ID = :P1 ');

    vExec.Executar([
      pAcumuladoId,
      pValorDinheiro,
      pValorCheque,
      pValorCobranca,
      pValorCartaoDebito,
      pValorCartaoCredito,
      pValorCredito,
      pValorFinanceira]
    );
  except
    on e: Exception do
      Result.TratarErro(e);
  end;
  vExec.Free;
end;

function AtualizarAcumuladoPagamentos(
  pConexao: TConexao;
  pAcumuladoId: Integer;
  pPagamentosCheques: TArray<RecTitulosFinanceiros>;
  pPagamentosCartoes: TArray<RecTitulosFinanceiros>;
  pPagamentosCobrancas: TArray<RecTitulosFinanceiros>;
  pPagamentosFinanceiras: TArray<RecTitulosFinanceiros>;
  pEmTransacao: Boolean
): RecRetornoBD;
var
  i: Integer;
  vExec: TExecucao;
  vCobrancas: TAcumuladosPagamentos;
  vCheques: TAcumuladosPagamentosCheques;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUA_FORMAS_PAGTO_ACUM');

  try
    if not pEmTransacao then
      pConexao.IniciarTransacao;

    vExec := TExecucao.Create(pConexao);
    vCobrancas := TAcumuladosPagamentos.Create(pConexao);
    vCheques := TAcumuladosPagamentosCheques.Create(pConexao);

    vExec.SQL.Text := 'delete from ACUMULADOS_PAGAMENTOS where ACUMULADO_ID = :P1';
    vExec.Executar([pAcumuladoId]);

    vExec.SQL.Text := 'delete from ACUMULADOS_PAGAMENTOS_CHEQUES where ACUMULADO_ID = :P1';
    vExec.Executar([pAcumuladoId]);

    vCobrancas.setInt('ACUMULADO_ID', pAcumuladoId, True);
    for i := Low(pPagamentosCartoes) to High(pPagamentosCartoes) do begin
      vCobrancas.setInt('COBRANCA_ID', pPagamentosCartoes[i].CobrancaId, True);
      vCobrancas.setInt('ITEM_ID', i + 1, True);
      vCobrancas.setDouble('VALOR', pPagamentosCartoes[i].valor);
      vCobrancas.setDataN('DATA_VENCIMENTO', 0);
      vCobrancas.setString('TIPO', 'CR');
      vCobrancas.setInt('PARCELA', 1);
      vCobrancas.setInt('NUMERO_PARCELAS', 1);
      vCobrancas.setString('NSU_TEF', pPagamentosCartoes[i].NsuTef);
      vCobrancas.setString('CODIGO_AUTORIZACAO', pPagamentosCartoes[i].CodigoAutorizacao);
      vCobrancas.setString('NUMERO_CARTAO', pPagamentosCartoes[i].NumeroCartao);
      vCobrancas.setString('TIPO_RECEB_CARTAO', pPagamentosCartoes[i].TipoRecebCartao);

      vCobrancas.Inserir;
    end;

    for i := Low(pPagamentosCobrancas) to High(pPagamentosCobrancas) do begin
      vCobrancas.setInt('COBRANCA_ID', pPagamentosCobrancas[i].CobrancaId, True);
      vCobrancas.setInt('ITEM_ID', Length(pPagamentosCartoes) + i + 1, True);
      vCobrancas.setData('DATA_VENCIMENTO', pPagamentosCobrancas[i].DataVencimento);
      vCobrancas.setDouble('VALOR', pPagamentosCobrancas[i].valor);
      vCobrancas.setString('TIPO', 'CO');
      vCobrancas.setInt('PARCELA', pPagamentosCobrancas[i].Parcela);
      vCobrancas.setInt('NUMERO_PARCELAS', pPagamentosCobrancas[i].NumeroParcelas);
      vCobrancas.setString('TIPO_RECEB_CARTAO', pPagamentosCobrancas[i].TipoRecebCartao);

      vCobrancas.Inserir;
    end;

    for i := Low(pPagamentosFinanceiras) to High(pPagamentosFinanceiras) do begin
      vCobrancas.setInt('COBRANCA_ID', pPagamentosFinanceiras[i].CobrancaId, True);
      vCobrancas.setInt('ITEM_ID', Length(pPagamentosCartoes) + Length(pPagamentosCobrancas) + i + 1, True);
      vCobrancas.setData('DATA_VENCIMENTO', pPagamentosFinanceiras[i].DataVencimento);
      vCobrancas.setDouble('VALOR', pPagamentosFinanceiras[i].Valor);
      vCobrancas.setString('TIPO', 'FI');
      vCobrancas.setInt('PARCELA', pPagamentosFinanceiras[i].Parcela);
      vCobrancas.setInt('NUMERO_PARCELAS', pPagamentosFinanceiras[i].NumeroParcelas);

      vCobrancas.Inserir;
    end;

    vCheques.setInt('ACUMULADO_ID', pAcumuladoId, True);
    for i := Low(pPagamentosCheques) to High(pPagamentosCheques) do begin
      vCheques.setInt('COBRANCA_ID', pPagamentosCheques[i].CobrancaId, True);
      vCheques.setInt('ITEM_ID', i + 1, True);
      vCheques.setString('BANCO', pPagamentosCheques[i].banco);
      vCheques.setString('AGENCIA', pPagamentosCheques[i].agencia);
      vCheques.setString('CONTA_CORRENTE', pPagamentosCheques[i].ContaCorrente);
      vCheques.setInt('NUMERO_CHEQUE', pPagamentosCheques[i].NumeroCheque);
      vCheques.setDouble('VALOR_CHEQUE', pPagamentosCheques[i].Valor);
      vCheques.setString('NOME_EMITENTE', pPagamentosCheques[i].NomeEmitente);
      vCheques.setString('CPF_CNPJ_EMITENTE', pPagamentosCheques[i].CpfCnpjEmitente);
      vCheques.setString('TELEFONE_EMITENTE', pPagamentosCheques[i].TelefoneEmitente);
      vCheques.setData('DATA_VENCIMENTO', pPagamentosCheques[i].DataVencimento);
      vCheques.setInt('PARCELA', pPagamentosCheques[i].parcela);
      vCheques.setInt('NUMERO_PARCELAS', pPagamentosCheques[i].NumeroParcelas);

      vCheques.Inserir;
    end;

    if not pEmTransacao then
      pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      if not pEmTransacao then
        pConexao.VoltarTransacao;

      Result.TratarErro(e);
    end;
  end;
  FreeAndNil(vCobrancas);
  FreeAndNil(vCheques);
  FreeAndNil(vExec);
end;

function AtualizarAcumuladosPagamentosPix(
  pConexao: TConexao;
  pAcumuladoId: Integer;
  pPagamentosPix: TArray<RecPagamentosPix>;
  pEmpresaId: Integer;
  pCadastroId: Integer;
  pTurnoId: Integer;
  pDataPagamento: TDateTime
): RecRetornoBD;
var
  i: Integer;
  vExec: TExecucao;
  vPagPix: TContasRecBaixasPagtosPix;
  pBaixaId: Integer;
  t: TContaReceberBaixa;
  valorTitulos: Double;
begin
  Result.Iniciar;
  pConexao.SetRotina('BAIXA_PAG_PIX');

  try
    vExec := TExecucao.Create(pConexao);
    vPagPix := TContasRecBaixasPagtosPix.Create(pConexao);
    t := TContaReceberBaixa.Create(pConexao);
    pBaixaId := TSequencia.Create(pConexao, 'SEQ_CONTAS_RECEBER_BAIXAS').getProximaSequencia;

    valorTitulos := 0;
    for i := Low(pPagamentosPix) to High(pPagamentosPix) do
      valorTitulos := valorTitulos + pPagamentosPix[i].Valor;

    t.setInt('BAIXA_ID', pBaixaId, True);
    t.setInt('EMPRESA_ID', pEmpresaId);
    t.setIntN('CADASTRO_ID', pCadastroId);
    t.setIntN('TURNO_ID', pTurnoId);
    t.setDouble('VALOR_TITULOS', valorTitulos);
    t.setDouble('VALOR_MULTA', 0);
    t.setDouble('VALOR_JUROS', 0);
    t.setDouble('VALOR_RETENCAO', 0);
    t.setDouble('VALOR_DESCONTO', 0);
    t.setDouble('VALOR_ADIANTADO', 0);
    t.setDouble('VALOR_PIX', valorTitulos);
    t.setDouble('VALOR_DINHEIRO', 0);
    t.setDouble('VALOR_CHEQUE', 0);
    t.setDouble('VALOR_CARTAO_DEBITO', 0);
    t.setDouble('VALOR_CARTAO_CREDITO', 0);
    t.setDouble('VALOR_COBRANCA', 0);
    t.setDouble('VALOR_CREDITO', 0);
    t.setDataN('DATA_PAGAMENTO', pDataPagamento);
    t.setString('RECEBER_CAIXA', 'N');
    t.setString('TIPO', 'PIX');
    t.setString('OBSERVACOES', '');
    t.setDouble('VALOR_TROCO', 0);
    t.setString('RECEBIDO', 'S');
    t.setIntN('RECEBER_ADIANTADO_ID', 0);
    t.setString('BAIXA_PIX_REC_CX', 'S');

    t.Inserir;

    vExec.Add('delete from CONTAS_REC_BAIXAS_PAGTOS_PIX ');
    vExec.Add('where BAIXA_ID = :P1');
    vExec.Executar([pBaixaId]);

    for i := Low(pPagamentosPix) to High(pPagamentosPix) do begin
      vPagPix.setInt('BAIXA_ID', pBaixaId, True);
      vPagPix.setString('CONTA_ID', pPagamentosPix[i].ContaId, True);
      vPagPix.setDouble('VALOR', pPagamentosPix[i].Valor);

      vPagPix.Inserir;
    end;

    vExec.Limpar;
    vExec.Add('update ACUMULADOS set BAIXA_RECEBER_PIX_ID = :P1 ');
    vExec.Add('where ACUMULADO_ID = :P2');
    vExec.Executar([pBaixaId, pAcumuladoId]);
  except
    on e: Exception do
      Result.TratarErro(e);
  end;

  vPagPix.Free;
  vExec.Free;
  t.Free;
end;

function ReceberAcumulado(
  pConexao: TConexao;
  pOrcamentoId: Integer;
  pTurnoId: Integer;
  pFuncionarioId: Integer;
  pGerarCreditoTroco: Boolean;
  pValorTroco: Currency;
  pTipoNotaGerar: string
): RecRetornoBD;
var
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('RECEBER_ACUMULADO');

  vProc := TProcedimentoBanco.Create(pConexao, 'RECEBER_ACUMULADO');
  try
    vProc.Params[0].AsInteger  := pOrcamentoId;
    vProc.Params[1].AsInteger  := pTurnoId;
    vProc.Params[2].AsCurrency := pValorTroco;
    vProc.Params[3].AsString   := ToChar(pGerarCreditoTroco);
    vProc.Params[4].AsString   := pTipoNotaGerar;

    vProc.Executar;
  except
    on e: Exception do
      Result.TratarErro(e);
  end;
  FreeAndNil(vProc);
end;

function CancelarRecebimentoAcumulado(pConexao: TConexao; pAcumuladoId: Integer): RecRetornoBD;
var
  vProcedimento: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('CANCELAR_RECEBIMENTO_ACUMULADO');
  vProcedimento := TProcedimentoBanco.Create(pConexao, 'CANCELAR_RECEBIMENTO_ACUMULADO');
  try
    pConexao.IniciarTransacao;

    vProcedimento.Params[0].AsInteger := pAcumuladoId;
    vProcedimento.Executar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  FreeAndNil(vProcedimento);
end;

function PodeCancelarRecebimentoAcumulado(
  pConexao: TConexao;
  pAcumuladoId: Integer
): RecRetornoBD;
var
  vProcedimento: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('PODE_CANC_RECEB_ACUMULADO');
  vProcedimento := TProcedimentoBanco.Create(pConexao, 'ACUMULADO_PODE_SER_CANCELADO');
  try
    pConexao.IniciarTransacao;

    vProcedimento.Params[0].AsInteger := pAcumuladoId;
    vProcedimento.Executar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  FreeAndNil(vProcedimento);
end;

function AtualizarIndiceDescontoVenda(pConexao: TConexao; pAcumuladoId: Integer; pIndiceDescontoVendaId: Integer): RecRetornoBD;
var
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATU_IND_DESC_VENDA_ACU');

  vExec := TExecucao.Create(pConexao);

  vExec.Add('update ACUMULADOS set');
  vExec.Add('  INDICE_DESCONTO_VENDA_ID = ' + IIfStr(pIndiceDescontoVendaId = 0, 'null', IntToStr(pIndiceDescontoVendaId)));
  vExec.Add('where ACUMULADO_ID = :P1 ');

  try
    pConexao.IniciarTransacao;

    vExec.Executar([pAcumuladoId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
end;

end.
