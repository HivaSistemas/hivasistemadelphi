unit _DiversosContatos;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TDiversosContato = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordDiversosContato: RecDiversosContatos;
  end;

function AtualizarDiversosContato(
  pConexao: TConexao;
  pCadastroId: Integer;
  pContatos: TArray<RecDiversosContatos>
): RecRetornoBD;

function BuscarDiversosContatos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecDiversosContatos>;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TDiversosContato }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CADASTRO_ID = :P1'
    );
end;

constructor TDiversosContato.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'DIVERSOS_CONTATOS');

  FSql :=
    'select ' +
    '  CADASTRO_ID, ' +
    '  ORDEM, ' +
    '  DESCRICAO, ' +
    '  TELEFONE, ' +
    '  RAMAL, ' +
    '  CELULAR, ' +
    '  E_MAIL, ' +
    '  OBSERVACAO ' +
    'from ' +
    '  DIVERSOS_CONTATOS';

  SetFiltros(GetFiltros);

  AddColuna('CADASTRO_ID', True);
  AddColuna('ORDEM', True);
  AddColuna('DESCRICAO');
  AddColuna('TELEFONE');
  AddColuna('RAMAL');
  AddColuna('CELULAR');
  AddColuna('E_MAIL');
  AddColuna('OBSERVACAO');
end;

function TDiversosContato.GetRecordDiversosContato: RecDiversosContatos;
begin
  Result.cadastro_id := GetInt('CADASTRO_ID', True);
  Result.ordem       := GetInt('ORDEM', True);
  Result.descricao   := GetString('DESCRICAO');
  Result.telefone    := GetString('TELEFONE');
  Result.ramal       := GetInt('RAMAL');
  Result.celular     := GetString('CELULAR');
  Result.e_mail      := GetString('E_MAIL');
  Result.observacao  := GetString('OBSERVACAO');
end;

function AtualizarDiversosContato(
  pConexao: TConexao;
  pCadastroId: Integer;
  pContatos: TArray<RecDiversosContatos>
): RecRetornoBD;
var
  i: Integer;
  ex: TExecucao;
  t: TDiversosContato;
begin
  Result.TeveErro := False;
  ex := TExecucao.Create(pConexao);
  t := TDiversosContato.Create(pConexao);

  try
    ex.SQL.Add('delete from DIVERSOS_CONTATOS where CADASTRO_ID = :P1');
    ex.Executar([pCadastroId]);

    t.SetInt('CADASTRO_ID', pCadastroId, True);
    for i := Low(pContatos) to High(pContatos) do begin
      t.SetInt('ORDEM', i, True);
      t.SetString('DESCRICAO', pContatos[i].descricao);
      t.SetString('TELEFONE', pContatos[i].telefone);
      t.SetInt('RAMAL', pContatos[i].ramal);
      t.SetString('CELULAR', pContatos[i].celular);
      t.SetString('E_MAIL', pContatos[i].e_mail);
      t.SetString('OBSERVACAO', pContatos[i].observacao);

      t.Inserir;
    end;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  ex.Free;
  t.Free;
end;

function BuscarDiversosContatos(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecDiversosContatos>;
var
  i: Integer;
  t: TDiversosContato;
begin
  Result := nil;
  t := TDiversosContato.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordDiversosContato;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
