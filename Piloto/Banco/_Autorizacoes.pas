unit _Autorizacoes;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TAutorizacoes = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordAutorizacoes: RecAutorizacoes;
  end;

function AtualizarCamposObrigatorios(
  pConexao: TConexao;
  pIds: array of string
): RecRetornoBD;
function BuscarCamposClienteObrigatorios(pConexao: TConexao): TArray<RecCamposObrigatoriosClientes>;
function CamposClientes(pConexao: TConexao): TArray<RecCamposClientes>;
function AtualizarAutorizacoes(
  pConexao: TConexao;
  pFuncionarioId: Integer;
  pTipo: string;
  pIds: array of string
): RecRetornoBD;

function BuscarAutorizacoes(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecAutorizacoes>;

function ExcluirAutorizacoes(
  pConexao: TConexao;
  pFuncionarioId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TAutorizacoes }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where FUNCIONARIO_ID = :P1 ' +
      'and TIPO = :P2 '
    );
end;

constructor TAutorizacoes.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'AUTORIZACOES');

  FSql :=
    'select ' +
    '  FUNCIONARIO_ID, ' +
    '  TIPO, ' +
    '  ID ' +
    'from ' +
    '  AUTORIZACOES ';

  setFiltros(getFiltros);

  AddColuna('FUNCIONARIO_ID', True);
  AddColuna('TIPO');
  AddColuna('ID');
end;

function TAutorizacoes.getRecordAutorizacoes: RecAutorizacoes;
begin
  Result.funcionario_id := getInt('FUNCIONARIO_ID', True);
  Result.tipo           := getString('TIPO');
  Result.id             := getString('ID');
end;

function AtualizarAutorizacoes(
  pConexao: TConexao;
  pFuncionarioId: Integer;
  pTipo: string;
  pIds: array of string
): RecRetornoBD;
var
  t: TAutorizacoes;
  i: Integer;

  vExec: TExecucao;
begin
  Result.TeveErro := False;
  t := TAutorizacoes.Create(pConexao);
  vExec := TExecucao.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    vExec.SQL.Text := 'delete from AUTORIZACOES where TIPO = :P1 and FUNCIONARIO_ID = :P2';
    vExec.Executar([pTipo, pFuncionarioId]);

    t.setInt('FUNCIONARIO_ID', pFuncionarioId, True);
    t.setString('TIPO', pTipo);

    for i := Low(pIds) to High(pIds) do begin
      t.setString('ID', pIds[i]);
      t.Inserir;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vExec.Free;
  t.Free;
end;

function BuscarAutorizacoes(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecAutorizacoes>;
var
  i: Integer;
  t: TAutorizacoes;
begin
  Result := nil;
  t := TAutorizacoes.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordAutorizacoes;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirAutorizacoes(
  pConexao: TConexao;
  pFuncionarioId: Integer
): RecRetornoBD;
var
  t: TAutorizacoes;
begin
  Result.TeveErro := False;
  t := TAutorizacoes.Create(pConexao);

  try
    t.setInt('FUNCIONARIO_ID', pFuncionarioId, True);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

function CamposClientes(pConexao: TConexao): TArray<RecCamposClientes>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  with vSql.SQL do begin
    Add('select ');
    Add('  ID, ');
    Add('  DESCRICAO, ');
    Add('  TIPO_CLIENTE ');
    Add('from ');
    Add('  VW_CAMPOS_CADASTRO_CLIENTE ');
    Add('order by ID asc ');
  end;

  if vSql.Pesquisar() then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].id           := vSql.GetInt(0);
      Result[i].descricao    := vSql.GetString(1);
      Result[i].tipo_cliente := vSql.GetString(2);

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function BuscarCamposClienteObrigatorios(pConexao: TConexao): TArray<RecCamposObrigatoriosClientes>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  with vSql.SQL do begin
    Add('select ');
    Add('  COC.CAMPO_ID, ');
    Add('  VW.TIPO_CLIENTE, ');
    Add('  VW.DESCRICAO, ');
    Add('  VW.NOME ');
    Add('from ');
    Add('  CAMPOS_OBRIGATORIOS_CLIENTES COC ');

    Add('  inner join VW_CAMPOS_CADASTRO_CLIENTE VW ');
    Add('  on COC.CAMPO_ID = VW.ID  ');

    Add('order by COC.CAMPO_ID asc ');
  end;

  if vSql.Pesquisar() then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].id           := vSql.GetInt(0);
      Result[i].tipo_cliente := vSql.GetString(1);
      Result[i].descricao    := vSql.GetString(2);
      Result[i].nome         := vSql.GetString(3);

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function AtualizarCamposObrigatorios(
  pConexao: TConexao;
  pIds: array of string
): RecRetornoBD;
var
  i: Integer;
  vExec: TExecucao;
begin
  Result.TeveErro := False;
  vExec := TExecucao.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    vExec.SQL.Text := 'delete from CAMPOS_OBRIGATORIOS_CLIENTES';
    vExec.Executar();

    vExec.SQL.Text := 'insert into CAMPOS_OBRIGATORIOS_CLIENTES(CAMPO_ID) values (:P1)';

    for i := Low(pIds) to High(pIds) do
      vExec.Executar([StrToInt(pIds[i])]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;
  vExec.Free;
end;

end.
