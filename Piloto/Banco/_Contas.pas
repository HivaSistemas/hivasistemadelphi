unit _Contas;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros,
  System.Variants, System.Math, System.StrUtils, _ContasFuncAutorizados;

{$M+}
type
  TContas = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao; pTipo: string; pSomenteAtivos: Boolean);
  protected
    function GetRecordBanco: RecContas;
  end;

function AtualizarConta(
  pConexao: TConexao;

  pConta: string;
  pDigitoConta: string;
  pAgencia: string;
  pDigitoAgencia: string;

  pNome: string;
  pEmpresaId: Integer;
  pTipo: Integer;
  pCodigoBanco: string;
  pAceitaSaldoNegativo: string;
  pEmiteCheque: string;
  pNumeroUltimoChequeEmitido: Integer;
  pEmiteBoleto: string;
  pNossoNumeroInicial: Integer;
  pNossoNumeroFinal: Integer;
  pProximoNossoNumero: Integer;
  pCarteira: string;
  pNumeroRemessaBoleto: Integer;
  pCodigoEmpresaCobranca: string;
  pAtivo: string;
  pFuncionariosAutorizadosIds: TArray<Integer>
): RecRetornoBD;

function BuscarContas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pTipo: string;
  pSomenteAtivos: Boolean;
  pFuncionarioAutorizadoId: Integer
): TArray<RecContas>;

function ExcluirContas(pConexao: TConexao; pConta: string): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TContas }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CON.CONTA = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Nome da conta',
      True,
      0,
      'where CON.NOME like :P1 || ''%'' '
    );
end;

constructor TContas.Create(pConexao: TConexao; pTipo: string; pSomenteAtivos: Boolean);
begin
  inherited Create(pConexao, 'CONTAS');

  FSql :=
    'select ' +
    '  CON.CONTA, ' +
    '  CON.NOME, ' +
    '  CON.EMPRESA_ID, ' +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA, ' +
    '  CON.TIPO, ' +
    '  CON.CODIGO_BANCO, ' +
    '  CON.AGENCIA, ' +
    '  CON.DIGITO_AGENCIA, ' +
    '  CON.DIGITO_CONTA, ' +
    '  CON.ACEITA_SALDO_NEGATIVO, ' +
    '  CON.SALDO_ATUAL, ' +
    '  CON.SALDO_CONCILIADO_ATUAL, ' +
    '  CON.EMITE_CHEQUE, ' +
    '  CON.NUMERO_ULTIMO_CHEQUE_EMITIDO, ' +
    '  CON.EMITE_BOLETO, ' +
    '  CON.NOSSO_NUMERO_INICIAL, ' +
    '  CON.NOSSO_NUMERO_FINAL, ' +
    '  CON.PROXIMO_NOSSO_NUMERO, ' +
    '  CON.CARTEIRA, ' +
    '  CON.NUMERO_REMESSA_BOLETO, ' +
    '  CON.CODIGO_EMPRESA_COBRANCA, ' +
    '  CON.ATIVO ' +
    'from ' +
    '  CONTAS CON ' +

    'inner join EMPRESAS EMP ' +
    'on CON.EMPRESA_ID = EMP.EMPRESA_ID ';

  SetFiltros(GetFiltros);

  if pSomenteAtivos then
    SetFiltros('and CON.ATIVO = ''S'' ');

  if pTipo = 'B' then
    SetFiltros('and CON.TIPO = 1 ')
  else if pTipo = 'C' then
    SetFiltros('and CON.TIPO = 0');

  AddColuna('CONTA', True);
  AddColuna('NOME');
  AddColuna('EMPRESA_ID');
  AddColunaSL('NOME_EMPRESA');
  AddColuna('TIPO');
  AddColuna('CODIGO_BANCO');
  AddColuna('AGENCIA');
  AddColuna('DIGITO_AGENCIA');
  AddColuna('DIGITO_CONTA');
  AddColuna('ACEITA_SALDO_NEGATIVO');
  AddColuna('EMITE_CHEQUE');
  AddColuna('NUMERO_ULTIMO_CHEQUE_EMITIDO');
  AddColuna('EMITE_BOLETO');
  AddColuna('NOSSO_NUMERO_INICIAL');
  AddColuna('NOSSO_NUMERO_FINAL');
  AddColuna('PROXIMO_NOSSO_NUMERO');
  AddColuna('CARTEIRA');
  AddColuna('NUMERO_REMESSA_BOLETO');
  AddColuna('CODIGO_EMPRESA_COBRANCA');
  AddColuna('ATIVO');
  AddColunaSL('SALDO_ATUAL');
  AddColunaSL('SALDO_CONCILIADO_ATUAL');
end;

function TContas.GetRecordBanco: RecContas;
begin
  Result := RecContas.Create;

  Result.Conta                     := GetString('CONTA', True);
  Result.Nome                      := GetString('NOME');
  Result.EmpresaId                 := GetInt('EMPRESA_ID');
  Result.NomeEmpresa               := GetString('NOME_EMPRESA');
  Result.Tipo                      := GetInt('TIPO');
  Result.CodigoBanco               := GetString('CODIGO_BANCO');
  Result.Agencia                   := GetString('AGENCIA');
  Result.DigitoAgencia             := GetString('DIGITO_AGENCIA');
  Result.DigitoConta               := GetString('DIGITO_CONTA');
  Result.Ativo                     := GetString('ATIVO');
  Result.AceitaSaldoNegativo       := getString('ACEITA_SALDO_NEGATIVO');
  Result.SaldoAtual                := getDouble('SALDO_ATUAL');
  Result.SaldoConciliadoAtual      := getDouble('SALDO_CONCILIADO_ATUAL');
  Result.EmiteCheque               := getString('EMITE_CHEQUE');
  Result.EmiteBoleto               := getString('EMITE_BOLETO');
  Result.NossoNumeroInicial        := getInt('NOSSO_NUMERO_INICIAL');
  Result.NossoNumeroFinal          := getInt('NOSSO_NUMERO_FINAL');
  Result.ProximoNossoNumero        := getInt('PROXIMO_NOSSO_NUMERO');
  Result.Carteira                  := getString('CARTEIRA');
  Result.NumeroRemessaBoleto       := GetInt('NUMERO_REMESSA_BOLETO');
  Result.CodigoEmpresaCobranca     := getString('CODIGO_EMPRESA_COBRANCA');
  Result.NumeroUltimoChequeEmitido := getInt('NUMERO_ULTIMO_CHEQUE_EMITIDO');
end;

function AtualizarConta(
  pConexao: TConexao;

  pConta: string;
  pDigitoConta: string;
  pAgencia: string;
  pDigitoAgencia: string;

  pNome: string;
  pEmpresaId: Integer;
  pTipo: Integer;
  pCodigoBanco: string;
  pAceitaSaldoNegativo: string;
  pEmiteCheque: string;
  pNumeroUltimoChequeEmitido: Integer;
  pEmiteBoleto: string;
  pNossoNumeroInicial: Integer;
  pNossoNumeroFinal: Integer;
  pProximoNossoNumero: Integer;
  pCarteira: string;
  pNumeroRemessaBoleto: Integer;
  pCodigoEmpresaCobranca: string;
  pAtivo: string;
  pFuncionariosAutorizadosIds: TArray<Integer>

): RecRetornoBD;
var
  t: TContas;
  novo: Boolean;
  vAutorizados: TContasFuncAutorizados;
  i: Integer;
  vExec: TExecucao;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_CONTA');
  t := TContas.Create(pConexao, '', False);
  vAutorizados := TContasFuncAutorizados.Create(pConexao);
  vExec := TExecucao.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    novo := not t.Pesquisar(0, [pConta]);

    t.SetString('CONTA', pConta, True);
    t.SetStringN('DIGITO_CONTA', IfThen(pTipo = 1, pDigitoConta));
    t.SetStringN('AGENCIA', pAgencia);
    t.SetStringN('DIGITO_AGENCIA', pDigitoAgencia);
    t.SetString('NOME', pNome);
    t.setInt('EMPRESA_ID', pEmpresaId);
    t.SetInt('TIPO', pTipo);
    t.SetStringN('CODIGO_BANCO', pCodigoBanco);
    t.setString('ACEITA_SALDO_NEGATIVO', pAceitaSaldoNegativo);
    t.setString('EMITE_CHEQUE', pEmiteCheque);
    t.setInt('NUMERO_ULTIMO_CHEQUE_EMITIDO', pNumeroUltimoChequeEmitido);
    t.setString('EMITE_BOLETO', pEmiteBoleto);
    t.setIntN('NOSSO_NUMERO_INICIAL', pNossoNumeroInicial);
    t.setIntN('NOSSO_NUMERO_FINAL', pNossoNumeroFinal);
    t.setIntN('PROXIMO_NOSSO_NUMERO', pProximoNossoNumero);
    t.setStringN('CARTEIRA', pCarteira);
    t.setIntN('NUMERO_REMESSA_BOLETO', pNumeroRemessaBoleto);
    t.setStringN('CODIGO_EMPRESA_COBRANCA', pCodigoEmpresaCobranca);
    t.SetString('ATIVO', pAtivo);

    if novo then
      t.Inserir
    else
      t.Atualizar;

    vAutorizados.setString('CONTA_ID', pConta, True);
    vExec.Clear;
    vExec.Add('delete from CONTAS_FUNC_AUTORIZADOS ');
    vExec.Add('where CONTA_ID = :P1');
    vExec.Executar([pConta]);

    for i := Low(pFuncionariosAutorizadosIds) to High(pFuncionariosAutorizadosIds) do begin
      vAutorizados.setInt('FUNCIONARIO_ID', pFuncionariosAutorizadosIds[i], True);
      vAutorizados.Inserir;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  vExec.Free;
  t.Free;
end;

function BuscarContas(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant;
  pTipo: string;
  pSomenteAtivos: Boolean;
  pFuncionarioAutorizadoId: Integer
): TArray<RecContas>;
var
  i: Integer;
  t: TContas;
  vSql: string;
begin
  Result := nil;
  vSql := '';
  if pFuncionarioAutorizadoId > 0 then
    vSql := 'and CON.CONTA in (select CONTA_ID from CONTAS_FUNC_AUTORIZADOS where FUNCIONARIO_ID = ' + IntToStr(pFuncionarioAutorizadoId) + ')';

  t := TContas.Create(pConexao, pTipo, pSomenteAtivos);

  if t.Pesquisar(pIndice, pFiltros, vSql) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordBanco;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirContas(pConexao: TConexao; pConta: string): RecRetornoBD;
var
  t: TContas;
begin
  Result.TeveErro := False;
  t := TContas.Create(pConexao, '', False);

  t.setString('CONTA', pConta, True);

  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
