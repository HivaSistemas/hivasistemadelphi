unit _DevolucoesEntradasNotasFisc;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _DevolucoesEntradasNfItens, _DevolucoesEntrNfItensLotes;

{$M+}
type
  RecDevolucoesEntradasNotasFisc = record
    DevolucaoId: Integer;
    EntradaId: Integer;
    EmpresaId: Integer;
    DataHoraCadastro: TDateTime;
    UsuarioCadastroId: Integer;
    ValorTotal: Double;
    ValorTotalProdutos: Double;
    ValorDesconto: Double;
    ValorOutrasDespesas: Double;
    BaseCalculoIcms: Double;
    ValorIcms: Double;
    BaseCalculoIcmsSt: Double;
    ValorIcmsSt: Double;
    ValorIpi: Double;
    MotivoDevolucao: string;
    ObservacoesNfe: string;
    Status: string;
  end;

  TDevolucoesEntradasNotasFisc = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordDevolucoesEntradasNotasFisc: RecDevolucoesEntradasNotasFisc;
  end;

function AtualizarDevolucoesEntradasNotasFisc(
  pConexao: TConexao;
  pDevolucaoId: Integer;
  pEntradaId: Integer;
  pEmpresaId: Integer;
  pValorTotal: Double;
  pValorTotalProdutos: Double;
  pValorDesconto: Double;
  pValorOutrasDespesas: Double;
  pBaseCalculoIcms: Double;
  pValorIcms: Double;
  pBaseCalculoIcmsSt: Double;
  pValorIcmsSt: Double;
  pValorIpi: Double;
  pMotivoDevolucao: string;
  pObservacoesNfe: string;
  pStatus: string;
  pItens: TArray<RecDevolucoesEntradasNfItens>;
  pLotes: TArray<RecDevolucoesEntrNfItensLotes>
): RecRetornoBD;

function BuscarDevolucoesEntradasNotasFisc(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecDevolucoesEntradasNotasFisc>;

function ExcluirDevolucoesEntradasNotasFisc(
  pConexao: TConexao;
  pDevolucaoId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TDevolucoesEntradasNotasFisc }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where DEVOLUCAO_ID = :P1 '
    );
end;

constructor TDevolucoesEntradasNotasFisc.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'DEVOLUCOES_ENTRADAS_NOTAS_FISC');

  FSql := 
    'select ' +
    '  DEVOLUCAO_ID, ' +
    '  ENTRADA_ID, ' +
    '  EMPRESA_ID, ' +
    '  DATA_HORA_CADASTRO, ' +
    '  USUARIO_CADASTRO_ID, ' +
    '  VALOR_TOTAL, ' +
    '  VALOR_TOTAL_PRODUTOS, ' +
    '  VALOR_DESCONTO, ' +
    '  VALOR_OUTRAS_DESPESAS, ' +
    '  BASE_CALCULO_ICMS, ' +
    '  VALOR_ICMS, ' +
    '  BASE_CALCULO_ICMS_ST, ' +
    '  VALOR_ICMS_ST, ' +
    '  VALOR_IPI, ' +
    '  MOTIVO_DEVOLUCAO, ' +
    '  OBSERVACOES_NFE, ' +
    '  STATUS ' +
    'from ' +
    '  DEVOLUCOES_ENTRADAS_NOTAS_FISC ';

  setFiltros(getFiltros);

  AddColuna('DEVOLUCAO_ID', True);
  AddColuna('ENTRADA_ID');
  AddColuna('EMPRESA_ID');
  AddColunaSL('DATA_HORA_CADASTRO');
  AddColunaSL('USUARIO_CADASTRO_ID');
  AddColuna('VALOR_TOTAL');
  AddColuna('VALOR_TOTAL_PRODUTOS');
  AddColuna('VALOR_DESCONTO');
  AddColuna('VALOR_OUTRAS_DESPESAS');
  AddColuna('BASE_CALCULO_ICMS');
  AddColuna('VALOR_ICMS');
  AddColuna('BASE_CALCULO_ICMS_ST');
  AddColuna('VALOR_ICMS_ST');
  AddColuna('VALOR_IPI');
  AddColuna('MOTIVO_DEVOLUCAO');
  AddColuna('OBSERVACOES_NFE');
  AddColuna('STATUS');
end;

function TDevolucoesEntradasNotasFisc.getRecordDevolucoesEntradasNotasFisc: RecDevolucoesEntradasNotasFisc;
begin
  Result.DevolucaoId                := getInt('DEVOLUCAO_ID', True);
  Result.EntradaId                  := getInt('ENTRADA_ID');
  Result.EmpresaId                  := getInt('EMPRESA_ID');
  Result.DataHoraCadastro           := getData('DATA_HORA_CADASTRO');
  Result.UsuarioCadastroId          := getInt('USUARIO_CADASTRO_ID');
  Result.ValorTotal                 := getDouble('VALOR_TOTAL');
  Result.ValorTotalProdutos         := getDouble('VALOR_TOTAL_PRODUTOS');
  Result.ValorDesconto              := getDouble('VALOR_DESCONTO');
  Result.ValorOutrasDespesas        := getDouble('VALOR_OUTRAS_DESPESAS');
  Result.BaseCalculoIcms            := getDouble('BASE_CALCULO_ICMS');
  Result.ValorIcms                  := getDouble('VALOR_ICMS');
  Result.BaseCalculoIcmsSt          := getDouble('BASE_CALCULO_ICMS_ST');
  Result.ValorIcmsSt                := getDouble('VALOR_ICMS_ST');
  Result.ValorIpi                   := getDouble('VALOR_IPI');
  Result.MotivoDevolucao            := getString('MOTIVO_DEVOLUCAO');
  Result.ObservacoesNfe             := getString('OBSERVACOES_NFE');
  Result.Status                     := getString('STATUS');
end;

function AtualizarDevolucoesEntradasNotasFisc(
  pConexao: TConexao;
  pDevolucaoId: Integer;
  pEntradaId: Integer;
  pEmpresaId: Integer;
  pValorTotal: Double;
  pValorTotalProdutos: Double;
  pValorDesconto: Double;
  pValorOutrasDespesas: Double;
  pBaseCalculoIcms: Double;
  pValorIcms: Double;
  pBaseCalculoIcmsSt: Double;
  pValorIcmsSt: Double;
  pValorIpi: Double;
  pMotivoDevolucao: string;
  pObservacoesNfe: string;
  pStatus: string;
  pItens: TArray<RecDevolucoesEntradasNfItens>;
  pLotes: TArray<RecDevolucoesEntrNfItensLotes>
): RecRetornoBD;
var
  t: TDevolucoesEntradasNotasFisc;
  vItem: TDevolucoesEntradasNfItens;
  vLote: TDevolucoesEntrNfItensLotes;

  i: Integer;
  j: Integer;
  vNovo: Boolean;
  vSeq: TSequencia;
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.setRotina('ATUALIZAR_DEV_ENT_FISC');

  t := TDevolucoesEntradasNotasFisc.Create(pConexao);
  vItem := TDevolucoesEntradasNfItens.Create(pConexao);
  vLote := TDevolucoesEntrNfItensLotes.Create(pConexao);
  vProc := TProcedimentoBanco.Create(pConexao, 'CONSOLIDAR_DEVOLUC_ENTRADA_NF');

  vNovo := pDevolucaoId = 0;
  if vNovo then begin
    vSeq := TSequencia.Create(pConexao, 'SEQ_DEVOLUCOES_ENT_NOTAS_FISC');
    pDevolucaoId := vSeq.getProximaSequencia;
    Result.AsInt := pDevolucaoId;
    vSeq.Free;
  end;

  try
    pConexao.IniciarTransacao; 

    t.setInt('DEVOLUCAO_ID', pDevolucaoId, True);
    t.setInt('ENTRADA_ID', pEntradaId);
    t.setInt('EMPRESA_ID', pEmpresaId);
    t.setDouble('VALOR_TOTAL', pValorTotal);
    t.setDouble('VALOR_TOTAL_PRODUTOS', pValorTotalProdutos);
    t.setDouble('VALOR_DESCONTO', pValorDesconto);
    t.setDouble('VALOR_OUTRAS_DESPESAS', pValorOutrasDespesas);
    t.setDouble('BASE_CALCULO_ICMS', pBaseCalculoIcms);
    t.setDouble('VALOR_ICMS', pValorIcms);
    t.setDouble('BASE_CALCULO_ICMS_ST', pBaseCalculoIcmsSt);
    t.setDouble('VALOR_ICMS_ST', pValorIcmsSt);
    t.setDouble('VALOR_IPI', pValorIpi);
    t.setString('MOTIVO_DEVOLUCAO', pMotivoDevolucao);
    t.setString('OBSERVACOES_NFE', pObservacoesNfe);
    t.setString('STATUS', pStatus);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    vItem.setInt('DEVOLUCAO_ID', pDevolucaoId, True);
    for i := Low(pItens) to High(pItens) do begin
      vItem.setInt('ITEM_ID', pItens[i].ItemId, True);
      vItem.setInt('LOCAL_ID', pItens[i].LocalId);
      vItem.setInt('PRODUTO_ID', pItens[i].ProdutoId);
      vItem.setDouble('VALOR_TOTAL', pItens[i].ValorTotal);
      vItem.setDouble('PRECO_UNITARIO', pItens[i].PrecoUnitario);
      vItem.setDouble('QUANTIDADE', pItens[i].Quantidade);
      vItem.setDouble('VALOR_TOTAL_DESCONTO', pItens[i].ValorTotalDesconto);
      vItem.setDouble('VALOR_TOTAL_OUTRAS_DESPESAS', pItens[i].ValorTotalOutrasDespesas);
      vItem.setDouble('BASE_CALCULO_ICMS', pItens[i].BaseCalculoIcms);
      vItem.setDouble('VALOR_ICMS', pItens[i].ValorIcms);
      vItem.setDouble('BASE_CALCULO_ICMS_ST', pItens[i].BaseCalculoIcmsSt);
      vItem.setDouble('VALOR_ICMS_ST', pItens[i].ValorIcmsSt);
      vItem.setDouble('VALOR_IPI', pItens[i].ValorIpi);
      vItem.setDouble('PESO_UNITARIO', pItens[i].PesoUnitario);

      vItem.Inserir;

      for j := Low(pLotes) to High(pLotes) do begin
        if pItens[i].ItemId <> pLotes[j].ItemId then
          Continue;

        vLote.setInt('DEVOLUCAO_ID', pDevolucaoId, True);
        vLote.setInt('ITEM_ID', pLotes[j].ItemId, True);
        vLote.setString('LOTE', pLotes[j].Lote, True);
        vLote.setDouble('QUANTIDADE', pLotes[j].Quantidade);

        vLote.Inserir;
      end;
    end;

    vProc.Executar([pDevolucaoId]);

    pConexao.FinalizarTransacao; 
  except
    on e: Exception do begin
      pConexao.VoltarTransacao; 
      Result.TratarErro(e);
    end;
  end;

  vProc.Free;
  vLote.Free;
  vItem.Free;
  t.Free;
end;

function BuscarDevolucoesEntradasNotasFisc(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecDevolucoesEntradasNotasFisc>;
var
  i: Integer;
  t: TDevolucoesEntradasNotasFisc;
begin
  Result := nil;
  t := TDevolucoesEntradasNotasFisc.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordDevolucoesEntradasNotasFisc;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirDevolucoesEntradasNotasFisc(
  pConexao: TConexao;
  pDevolucaoId: Integer
): RecRetornoBD;
var
  t: TDevolucoesEntradasNotasFisc;
begin
  Result.Iniciar;
  t := TDevolucoesEntradasNotasFisc.Create(pConexao);

  try
    pConexao.IniciarTransacao;

    t.setInt('DEVOLUCAO_ID', pDevolucaoId, True);

    t.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

end.
