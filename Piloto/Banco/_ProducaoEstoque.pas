unit _ProducaoEstoque;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica,
  System.SysUtils, _RecordsEstoques, _ProducaoEstoqueItens, _Biblioteca,  Vcl.Graphics;

type
  TProducaoEstoque = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordProducaoEstoque: RecProducaoEstoque;
  end;

function AtualizarProducaoEstoque(
  pConexao: TConexao;
  pProducaoEstoqueId: Integer;
  pEmpresaId: Integer;
  pProducaoItens: TArray<RecProducaoEstoqueItens>
): RecRetornoBD;

function BuscarProducaoEstoque(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecProducaoEstoque>;

function BuscarProducaoEstoqueComando(pConexao: TConexao; pComando: string): TArray<RecProducaoEstoque>;
function GerarNotaProducao(pConexao: TConexao; producaoEstoqueId: Integer; natureza: string; pControlarTransacao: Boolean): RecRetornoBD;

function ExcluirProducaoEstoque(
  pConexao: TConexao;
  pProducaoEstoqueId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TAjusteEstoque }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where PRODUCAO_ESTOQUE_ID = :P1'
    );
end;

constructor TProducaoEstoque.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'PRODUCAO_ESTOQUE');

  FSql :=
    'select ' +
    '  PRO.PRODUCAO_ESTOQUE_ID, ' +
    '  PRO.EMPRESA_ID, ' +
    '  EMP.NOME_FANTASIA, ' +
    '  PRO.USUARIO_PRODUCAO_ID, ' +
    '  FUN.NOME as NOME_USUARIO_PRODUCAO, ' +
    '  PRO.DATA_HORA_PRODUCAO ' +
    'from ' +
    '  PRODUCAO_ESTOQUE PRO ' +

    'inner join FUNCIONARIOS FUN ' +
    'on PRO.USUARIO_PRODUCAO_ID = FUN.FUNCIONARIO_ID ' +

    'inner join EMPRESAS EMP ' +
    'on PRO.EMPRESA_ID = EMP.EMPRESA_ID ';

  setFiltros(getFiltros);

  AddColuna('PRODUCAO_ESTOQUE_ID', True);
  AddColuna('EMPRESA_ID');
  AddColunaSL('NOME_FANTASIA');
  AddColunaSL('USUARIO_PRODUCAO_ID');
  AddColunaSL('NOME_USUARIO_PRODUCAO');
  AddColunaSL('DATA_HORA_PRODUCAO');
end;

function TProducaoEstoque.getRecordProducaoEstoque: RecProducaoEstoque;
begin
  Result.producao_estoque_id   := getInt('PRODUCAO_ESTOQUE_ID', True);
  Result.EmpresaId             := getInt('EMPRESA_ID');
  Result.NomeFantasia          := getString('NOME_FANTASIA');
  Result.usuario_producao_id   := getInt('USUARIO_PRODUCAO_ID');
  Result.NomeUsuarioProducao   := getString('NOME_USUARIO_PRODUCAO');
  Result.data_hora_producao    := getData('DATA_HORA_PRODUCAO');
end;

function AtualizarProducaoEstoque(
  pConexao: TConexao;
  pProducaoEstoqueId: Integer;
  pEmpresaId: Integer;
  pProducaoItens: TArray<RecProducaoEstoqueItens>
): RecRetornoBD;
var
  t: TProducaoEstoque;
  vItem: TProducaoEstoqueItens;

  i: Integer;
  novo: Boolean;
  seq: TSequencia;
  vExec: TExecucao;
  vProc: TProcedimentoBanco;
begin
  Result.Iniciar;
  pConexao.SetRotina('REALIZAR_PRODUCAO_ESTOQUE');

  vExec := TExecucao.Create(pConexao);
  t := TProducaoEstoque.Create(pConexao);
  vItem := TProducaoEstoqueItens.Create(pConexao);

  novo := pProducaoEstoqueId = 0;
  if novo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_PRODUCAO_ESTOQUE_ID');
    pProducaoEstoqueId := seq.getProximaSequencia;
    result.AsInt := pProducaoEstoqueId;
    seq.Free;
  end;

  try
    pConexao.IniciarTransacao;

    Result.AsInt := pProducaoEstoqueId;
    t.setInt('PRODUCAO_ESTOQUE_ID', pProducaoEstoqueId, True);
    t.setInt('EMPRESA_ID', pEmpresaId);

    if novo then
      t.Inserir
    else
      t.Atualizar;

    if not novo then begin
      vExec.Limpar;
      vExec.Add('delete from PRODUCAO_ESTOQUE_ITENS ');
      vExec.Add('where PRODUCAO_ESTOQUE_ID = :P1');
      vExec.Executar([pProducaoEstoqueId]);
    end;

    vItem.setInt('PRODUCAO_ESTOQUE_ID', pProducaoEstoqueId, True);
    for i := Low(pProducaoItens) to High(pProducaoItens) do begin
      vItem.setInt('LOCAL_ID', pProducaoItens[i].LocalId, True);
      vItem.setInt('PRODUTO_ID', pProducaoItens[i].produto_id);
      vItem.setDouble('QUANTIDADE', pProducaoItens[i].quantidade);
      vItem.setString('NATUREZA', pProducaoItens[i].Natureza);
      vItem.setDouble('PRECO_UNITARIO', pProducaoItens[i].PrecoUnitario);

      vItem.Inserir;
    end;

    //Gerando nota de entrada
    vProc := TProcedimentoBanco.Create(pConexao, 'GERAR_NOTA_PRODUCAO');
    vProc.Executar([pProducaoEstoqueId, 'E']);
    Result.AddInt(vProc.ParamByName('oNOTA_FISCAL_ID').AsInteger);
    Result.AddString(vProc.ParamByName('oTIPO_NOTA_GERAR').AsString);

    //Gerando nota de sa�da
    vProc := TProcedimentoBanco.Create(pConexao, 'GERAR_NOTA_PRODUCAO');
    vProc.Executar([pProducaoEstoqueId, 'S']);
    Result.AddInt(vProc.ParamByName('oNOTA_FISCAL_ID').AsInteger);
    Result.AddString(vProc.ParamByName('oTIPO_NOTA_GERAR').AsString);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  vExec.Free;
  vItem.Free;
  t.Free;
end;

function BuscarProducaoEstoque(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecProducaoEstoque>;
var
  i: Integer;
  t: TProducaoEstoque;
begin
  Result := nil;
  t := TProducaoEstoque.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordProducaoEstoque;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarProducaoEstoqueComando(pConexao: TConexao; pComando: string): TArray<RecProducaoEstoque>;
var
  i: Integer;
  t: TProducaoEstoque;
begin
  Result := nil;
  t := TProducaoEstoque.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordProducaoEstoque;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;


function ExcluirProducaoEstoque(
  pConexao: TConexao;
  pProducaoEstoqueId: Integer
): RecRetornoBD;
var
  vAju: TProducaoEstoque;
  vExec: TExecucao;
begin
  Result.TeveErro := False;
  vAju := TProducaoEstoque.Create(pConexao);
  vExec := TExecucao.Create(pConexao);

  vExec.Add('delete from PRODUCAO_ESTOQUE_ITENS');
  vExec.Add('where PRODUCAO_ESTOQUE_ID = :P1');

  try
    pConexao.IniciarTransacao;

    vExec.Executar([pProducaoEstoqueId]);

    vAju.setInt('PRODUCAO_ESTOQUE_ID', pProducaoEstoqueId, True);
    vAju.Excluir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  vExec.Free;
  vAju.Free;
end;

function GerarNotaProducao(pConexao: TConexao; producaoEstoqueId: Integer; natureza: string; pControlarTransacao: Boolean): RecRetornoBD;
var
  i: Integer;
  vProc: TProcedimentoBanco;
begin
  Result.TeveErro := False;
  pConexao.SetRotina('GERAR_NOTA_PRODUCAO');

  try
    pConexao.IniciarTransacao;

    vProc := TProcedimentoBanco.Create(pConexao, 'GERAR_NOTA_PRODUCAO');

    vProc.Executar([producaoEstoqueId, natureza]);

    Result.AddInt(vProc.ParamByName('oNOTA_FISCAL_ID').AsInteger);
    Result.AddString(vProc.ParamByName('oTIPO_NOTA_GERAR').AsString);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  FreeAndNil(vProc);
end;

end.
