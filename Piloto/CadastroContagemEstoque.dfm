inherited FormCadastroContagemEstoque: TFormCadastroContagemEstoque
  Caption = 'Cadastro Contagem de Estoque'
  ClientHeight = 641
  ClientWidth = 936
  ExplicitWidth = 942
  ExplicitHeight = 670
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [1]
    Left = 211
    Top = 5
    Width = 32
    Height = 14
    Caption = 'Nome'
  end
  object Label2: TLabel [2]
    Left = 837
    Top = 5
    Width = 34
    Height = 14
    Caption = 'Status'
  end
  object lbDataNascimento: TLabel [3]
    Left = 599
    Top = 5
    Width = 65
    Height = 14
    Caption = 'Iniciado em'
  end
  object Label3: TLabel [4]
    Left = 719
    Top = 5
    Width = 78
    Height = 14
    Caption = 'Finalizado em'
  end
  inherited pnOpcoes: TPanel
    Height = 641
    TabOrder = 3
    ExplicitHeight = 641
    inherited sbExcluir: TSpeedButton
      Visible = False
    end
  end
  inherited eID: TEditLuka
    TabOrder = 0
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 881
    Top = 2
    TabOrder = 4
    Visible = False
    ExplicitLeft = 881
    ExplicitTop = 2
  end
  object pgMaster: TPageControl
    Left = 125
    Top = 96
    Width = 803
    Height = 538
    ActivePage = tsFiltros
    TabOrder = 5
    object tsFiltros: TTabSheet
      Caption = 'Filtros'
      ImageIndex = 1
      object lbTipoControleEstoque: TLabel
        Left = 380
        Top = 46
        Width = 154
        Height = 14
        Caption = 'Tipo de controle de estoque'
      end
      object lbAtivo: TLabel
        Left = 579
        Top = 2
        Width = 26
        Height = 14
        Caption = 'Ativo'
      end
      object lb3: TLabel
        Left = 381
        Top = 84
        Width = 112
        Height = 14
        Caption = 'Situa'#231#227'o do estoque'
      end
      object Label4: TLabel
        Left = 380
        Top = 125
        Width = 129
        Height = 14
        Caption = 'Quantidade de estoque'
      end
      object Label5: TLabel
        Left = 682
        Top = 2
        Width = 78
        Height = 14
        Caption = 'Agrupar locais'
      end
      object Label6: TLabel
        Left = 3
        Top = 340
        Width = 73
        Height = 14
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alCustom
        Caption = 'Agrupamento'
      end
      inline FrProdutos: TFrProdutos
        Left = 2
        Top = 91
        Width = 323
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 91
        ExplicitWidth = 323
        inherited sgPesquisa: TGridLuka
          Width = 298
          ExplicitWidth = 298
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 323
          ExplicitWidth = 323
          inherited lbNomePesquisa: TLabel
            Width = 48
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 218
            ExplicitLeft = 218
          end
        end
        inherited pnPesquisa: TPanel
          Left = 298
          ExplicitLeft = 298
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
        inherited ckFiltroExtra: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      inline FrDepartamentosSecoesLinhas: TFrameDepartamentosSecoesLinhas
        Left = 2
        Top = 276
        Width = 319
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 276
        ExplicitWidth = 319
        ExplicitHeight = 81
        inherited sgPesquisa: TGridLuka
          Width = 294
          Height = 64
          ExplicitWidth = 294
          ExplicitHeight = 64
        end
        inherited PnTitulos: TPanel
          Width = 319
          ExplicitWidth = 319
          inherited lbNomePesquisa: TLabel
            Width = 98
            Caption = 'Grupo de estoque'
            ExplicitWidth = 98
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 214
            ExplicitLeft = 214
          end
        end
        inherited pnPesquisa: TPanel
          Left = 294
          Height = 65
          ExplicitLeft = 294
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
      end
      inline FrMarcas: TFrMarcas
        Left = 2
        Top = 184
        Width = 321
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 184
        ExplicitWidth = 321
        inherited sgPesquisa: TGridLuka
          Width = 296
          ExplicitWidth = 296
        end
        inherited PnTitulos: TPanel
          Width = 321
          ExplicitWidth = 321
          inherited lbNomePesquisa: TLabel
            Width = 39
            ExplicitWidth = 39
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 216
            ExplicitLeft = 216
          end
        end
        inherited pnPesquisa: TPanel
          Left = 296
          ExplicitLeft = 296
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
      end
      inline FrDataCadastroProduto: TFrDataInicialFinal
        Left = 379
        Top = 1
        Width = 196
        Height = 42
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 5
        TabStop = True
        ExplicitLeft = 379
        ExplicitTop = 1
        ExplicitWidth = 196
        ExplicitHeight = 42
        inherited Label1: TLabel
          Width = 156
          Height = 14
          Caption = 'Data de cadastro do produto'
          ExplicitWidth = 156
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      object cbTipoControleEstoque: TComboBoxLuka
        Left = 380
        Top = 60
        Width = 157
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 5
        TabOrder = 8
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'Normal'
          'Lote'
          'Piso'
          'Grade'
          'Kit'
          'N'#227'o filtrar')
        Valores.Strings = (
          'N'
          'L'
          'P'
          'G'
          'K'
          'NaoFiltrar')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      object cbAtivo: TComboBoxLuka
        Left = 579
        Top = 16
        Width = 97
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 2
        TabOrder = 6
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'Sim'
          'N'#227'o'
          'N'#227'o filtrar')
        Valores.Strings = (
          'S'
          'N'
          'NaoFiltrar')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      object cbSituacaoDoEstoque: TComboBoxLuka
        Left = 381
        Top = 98
        Width = 156
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 3
        TabOrder = 10
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'Positivo'
          'Negativo'
          'Zerado'
          'N'#227'o filtrar')
        Valores.Strings = (
          'POS'
          'NEG'
          'ZERO'
          'NaoFiltrar')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      object pnEnderecos: TPanel
        Left = 372
        Top = 168
        Width = 341
        Height = 345
        BevelOuter = bvNone
        TabOrder = 13
        inline FrEnderecoEstoque: TFrEnderecoEstoque
          Left = 8
          Top = 0
          Width = 320
          Height = 61
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Color = clWhite
          ParentBackground = False
          ParentColor = False
          TabOrder = 0
          TabStop = True
          ExplicitLeft = 8
          ExplicitWidth = 320
          ExplicitHeight = 61
          inherited sgPesquisa: TGridLuka
            Width = 295
            Height = 44
            ExplicitWidth = 295
            ExplicitHeight = 44
          end
          inherited PnTitulos: TPanel
            Width = 320
            ExplicitWidth = 320
            inherited lbNomePesquisa: TLabel
              Width = 121
              ExplicitWidth = 121
              ExplicitHeight = 14
            end
            inherited CkChaveUnica: TCheckBox
              Checked = False
              State = cbUnchecked
            end
            inherited pnSuprimir: TPanel
              Left = 215
              ExplicitLeft = 215
              inherited ckSuprimir: TCheckBox
                Visible = False
              end
            end
          end
          inherited pnPesquisa: TPanel
            Left = 295
            Height = 45
            ExplicitLeft = 295
            ExplicitHeight = 45
          end
        end
        inline FrEnderecoEstoqueRua: TFrEnderecoEstoqueRua
          Left = 8
          Top = 64
          Width = 320
          Height = 61
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Color = clWhite
          ParentBackground = False
          ParentColor = False
          TabOrder = 1
          TabStop = True
          ExplicitLeft = 8
          ExplicitTop = 64
          ExplicitWidth = 320
          ExplicitHeight = 61
          inherited sgPesquisa: TGridLuka
            Width = 295
            Height = 44
            ExplicitWidth = 295
            ExplicitHeight = 44
          end
          inherited PnTitulos: TPanel
            Width = 320
            ExplicitWidth = 320
            inherited lbNomePesquisa: TLabel
              Width = 21
              ExplicitWidth = 21
              ExplicitHeight = 14
            end
            inherited CkChaveUnica: TCheckBox
              Checked = False
              State = cbUnchecked
            end
            inherited pnSuprimir: TPanel
              Left = 215
              ExplicitLeft = 215
              inherited ckSuprimir: TCheckBox
                Visible = False
              end
            end
          end
          inherited pnPesquisa: TPanel
            Left = 295
            Height = 45
            ExplicitLeft = 295
            ExplicitHeight = 45
          end
        end
        inline FrEnderecoEstoqueModulo: TFrEnderecoEstoqueModulo
          Left = 8
          Top = 129
          Width = 320
          Height = 61
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Color = clWhite
          ParentBackground = False
          ParentColor = False
          TabOrder = 2
          TabStop = True
          ExplicitLeft = 8
          ExplicitTop = 129
          ExplicitWidth = 320
          ExplicitHeight = 61
          inherited sgPesquisa: TGridLuka
            Width = 295
            Height = 44
            ExplicitWidth = 295
            ExplicitHeight = 44
          end
          inherited CkFiltroDuplo: TCheckBox
            Left = 3
            ExplicitLeft = 3
          end
          inherited PnTitulos: TPanel
            Width = 320
            ExplicitWidth = 320
            inherited lbNomePesquisa: TLabel
              Width = 42
              ExplicitWidth = 42
              ExplicitHeight = 14
            end
            inherited CkChaveUnica: TCheckBox
              Checked = False
              State = cbUnchecked
            end
            inherited pnSuprimir: TPanel
              Left = 215
              ExplicitLeft = 215
              inherited ckSuprimir: TCheckBox
                Visible = False
              end
            end
          end
          inherited pnPesquisa: TPanel
            Left = 295
            Height = 45
            ExplicitLeft = 295
            ExplicitHeight = 45
          end
        end
        inline FrEnderecoEstoqueNivel: TFrEnderecoEstoqueNivel
          Left = 8
          Top = 189
          Width = 320
          Height = 81
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Color = clWhite
          ParentBackground = False
          ParentColor = False
          TabOrder = 3
          TabStop = True
          ExplicitLeft = 8
          ExplicitTop = 189
          ExplicitWidth = 320
          inherited sgPesquisa: TGridLuka
            Width = 295
            ExplicitWidth = 295
          end
          inherited PnTitulos: TPanel
            Width = 320
            ExplicitWidth = 320
            inherited lbNomePesquisa: TLabel
              Width = 27
              ExplicitWidth = 27
              ExplicitHeight = 14
            end
            inherited CkChaveUnica: TCheckBox
              Checked = False
              State = cbUnchecked
            end
            inherited pnSuprimir: TPanel
              Left = 215
              ExplicitLeft = 215
              inherited ckSuprimir: TCheckBox
                Visible = False
              end
            end
          end
          inherited pnPesquisa: TPanel
            Left = 295
            ExplicitLeft = 295
          end
        end
        inline FrEnderecoEstoqueVao: TFrEnderecoEstoqueVao
          Left = 8
          Top = 272
          Width = 320
          Height = 61
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Color = clWhite
          ParentBackground = False
          ParentColor = False
          TabOrder = 4
          TabStop = True
          ExplicitLeft = 8
          ExplicitTop = 272
          ExplicitWidth = 320
          ExplicitHeight = 61
          inherited sgPesquisa: TGridLuka
            Width = 295
            Height = 44
            ExplicitWidth = 295
            ExplicitHeight = 44
          end
          inherited PnTitulos: TPanel
            Width = 320
            ExplicitWidth = 320
            inherited lbNomePesquisa: TLabel
              Width = 20
              ExplicitWidth = 20
              ExplicitHeight = 14
            end
            inherited CkChaveUnica: TCheckBox
              Checked = False
              State = cbUnchecked
            end
            inherited pnSuprimir: TPanel
              Left = 215
              ExplicitLeft = 215
              inherited ckSuprimir: TCheckBox
                Visible = False
              end
            end
          end
          inherited pnPesquisa: TPanel
            Left = 295
            Height = 45
            ExplicitLeft = 295
            ExplicitHeight = 45
          end
        end
      end
      object cbQuantidadeEstoque: TComboBoxLuka
        Left = 380
        Top = 140
        Width = 140
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 3
        TabOrder = 11
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'Entre'
          'Maior que'
          'Menor que'
          'N'#227'o filtrar')
        Valores.Strings = (
          'EN'
          'MA'
          'ME'
          'NaoFiltrar')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      object pnQuantidadeEstoque: TPanel
        Left = 528
        Top = 123
        Width = 185
        Height = 41
        BevelOuter = bvNone
        TabOrder = 12
        object Label7: TLabel
          Left = 0
          Top = 3
          Width = 60
          Height = 14
          Caption = 'Valor entre'
        end
        object lb2: TLabel
          Left = 79
          Top = 21
          Width = 7
          Height = 14
          Caption = 'e'
        end
        object eValor1: TEdit
          Left = 0
          Top = 17
          Width = 76
          Height = 22
          TabOrder = 0
          Text = '0'
        end
        object eValor2: TEdit
          Left = 92
          Top = 17
          Width = 76
          Height = 22
          TabOrder = 1
          Text = '0'
        end
      end
      object cbAgruparLocais: TComboBoxLuka
        Left = 682
        Top = 16
        Width = 105
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 7
        Text = 'N'#227'o'
        Items.Strings = (
          'N'#227'o'
          'Sim')
        Valores.Strings = (
          'N'
          'S')
        AsInt = 0
        AsString = 'N'
      end
      inline FrDataAjuste: TFrDataInicialFinal
        Left = 582
        Top = 58
        Width = 201
        Height = 41
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 9
        TabStop = True
        ExplicitLeft = 582
        ExplicitTop = 58
        ExplicitWidth = 201
        inherited Label1: TLabel
          Left = 1
          Width = 200
          Height = 14
          Caption = 'Produtos que n'#227'o tiveram corre'#231#245'es::'
          ExplicitLeft = 1
          ExplicitWidth = 200
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Left = 92
          Width = 18
          Height = 14
          ExplicitLeft = 92
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Left = 115
          Height = 22
          ExplicitLeft = 115
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      object cbAgrupamento: TComboBoxLuka
        Left = 3
        Top = 381
        Width = 141
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 4
        Text = 'Nenhum'
        Items.Strings = (
          'Nenhum'
          'Marca')
        Valores.Strings = (
          'NEN'
          'MAR')
        AsInt = 0
        AsString = 'NEN'
      end
      inline FrLocais: TFrLocais
        Left = 2
        Top = 0
        Width = 323
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 2
        ExplicitWidth = 323
        inherited sgPesquisa: TGridLuka
          Width = 298
          ExplicitWidth = 294
          ExplicitHeight = 24
        end
        inherited CkAspas: TCheckBox
          Top = 60
          ExplicitTop = 60
        end
        inherited CkFiltroDuplo: TCheckBox
          Top = 23
          ExplicitTop = 23
        end
        inherited CkPesquisaNumerica: TCheckBox
          Top = 40
          ExplicitTop = 40
        end
        inherited PnTitulos: TPanel
          Width = 323
          ExplicitWidth = 319
          inherited lbNomePesquisa: TLabel
            Width = 94
            Caption = 'Local do produto:'
            ExplicitWidth = 94
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 218
            ExplicitLeft = 214
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 298
          ExplicitLeft = 294
          ExplicitHeight = 63
        end
      end
    end
  end
  object edtNome: TEditLuka
    Left = 211
    Top = 19
    Width = 384
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 1
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object edtStatus: TEditLuka
    Left = 837
    Top = 19
    Width = 89
    Height = 22
    TabStop = False
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 6
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object edtDataInicio: TEditLukaData
    Left = 600
    Top = 19
    Width = 111
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    TabStop = False
    EditMask = '99/99/9999 99:99:99;1;_'
    MaxLength = 19
    ReadOnly = True
    TabOrder = 7
    Text = '  /  /       :  :  '
    TipoData = tdDataHora
  end
  object edtDataFinal: TEditLukaData
    Left = 719
    Top = 19
    Width = 112
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    TabStop = False
    EditMask = '99/99/9999 99:99:99;1;_'
    MaxLength = 19
    ReadOnly = True
    TabOrder = 8
    Text = '  /  /       :  :  '
    TipoData = tdDataHora
  end
  inline FrEmpresas: TFrEmpresas
    Left = 126
    Top = 48
    Width = 469
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 48
    ExplicitWidth = 469
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 444
      Height = 24
      ExplicitWidth = 444
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 469
      ExplicitWidth = 469
      inherited lbNomePesquisa: TLabel
        Width = 50
        Caption = 'Empresa:'
        ExplicitWidth = 50
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 364
        ExplicitLeft = 364
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 444
      Height = 25
      ExplicitLeft = 444
      ExplicitHeight = 25
    end
  end
end
