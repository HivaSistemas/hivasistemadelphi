inherited FormAlterarSenhaUsuario: TFormAlterarSenhaUsuario
  Caption = 'Nova senha'
  ClientHeight = 175
  ClientWidth = 226
  OnShow = FormShow
  ExplicitWidth = 232
  ExplicitHeight = 204
  PixelsPerInch = 96
  TextHeight = 14
  object Label1: TLabel [0]
    Left = 8
    Top = 5
    Width = 66
    Height = 14
    Caption = 'Senha atual'
  end
  object Label2: TLabel [1]
    Left = 8
    Top = 93
    Width = 102
    Height = 14
    Caption = 'Repita nova senha'
  end
  object Label3: TLabel [2]
    Left = 8
    Top = 49
    Width = 64
    Height = 14
    Caption = 'Nova senha'
  end
  inherited pnOpcoes: TPanel
    Top = 138
    Width = 226
    TabOrder = 3
    ExplicitTop = 138
    ExplicitWidth = 226
  end
  object eSenhaAtual: TEditLuka
    Left = 8
    Top = 20
    Width = 211
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 10
    PasswordChar = '*'
    TabOrder = 0
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eNovaSenhaRepetir: TEditLuka
    Left = 8
    Top = 108
    Width = 211
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 10
    PasswordChar = '*'
    TabOrder = 2
    OnKeyDown = eNovaSenhaRepetirKeyDown
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eNovaSenha: TEditLuka
    Left = 8
    Top = 64
    Width = 211
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 10
    PasswordChar = '*'
    TabOrder = 1
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
