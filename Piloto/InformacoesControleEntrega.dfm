inherited FormInformacoesControleEntrega: TFormInformacoesControleEntrega
  Caption = 'Informa'#231#245'es do controle de entrega'
  ClientHeight = 490
  ClientWidth = 823
  ExplicitWidth = 829
  ExplicitHeight = 519
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 5
    Top = 6
    Width = 46
    Height = 14
    Caption = 'Controle'
  end
  object lb2: TLabel [1]
    Left = 203
    Top = 6
    Width = 39
    Height = 14
    Caption = 'Ve'#237'culo'
  end
  object lb12: TLabel [2]
    Left = 407
    Top = 6
    Width = 53
    Height = 14
    Caption = 'Motorista'
  end
  object lb3: TLabel [3]
    Left = 85
    Top = 6
    Width = 102
    Height = 14
    Caption = 'Data/hora gera'#231#227'o'
  end
  inherited pnOpcoes: TPanel
    Top = 453
    Width = 823
    ExplicitTop = 453
    ExplicitWidth = 823
  end
  object eControle: TEditLuka
    Left = 5
    Top = 20
    Width = 76
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 1
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eVeiculo: TEditLuka
    Left = 203
    Top = 20
    Width = 200
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 2
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eMotorista: TEditLuka
    Left = 407
    Top = 20
    Width = 200
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 3
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eDataHoraGeracao: TEditLukaData
    Left = 85
    Top = 20
    Width = 114
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999 99:99:99;1;_'
    MaxLength = 19
    TabOrder = 4
    Text = '  /  /       :  :  '
    TipoData = tdDataHora
  end
  object pcDados: TPageControl
    Left = 0
    Top = 47
    Width = 820
    Height = 403
    ActivePage = tsPrincipais
    TabOrder = 5
    object tsPrincipais: TTabSheet
      Caption = 'Principais'
      ImageIndex = 2
      object lb6: TLabel
        Left = 345
        Top = -2
        Width = 55
        Height = 14
        Caption = 'Peso total'
      end
      object lb5: TLabel
        Left = 271
        Top = -2
        Width = 60
        Height = 14
        Caption = 'Qtde. prod.'
      end
      object lb4: TLabel
        Left = 197
        Top = -2
        Width = 56
        Height = 14
        Caption = 'Qtde. entr.'
      end
      object lb13: TLabel
        Left = 3
        Top = -1
        Width = 47
        Height = 14
        Caption = 'Empresa'
      end
      object Label1: TLabel
        Left = 3
        Top = 39
        Width = 110
        Height = 14
        Caption = 'Usu'#225'rio de cadastro'
      end
      object Label2: TLabel
        Left = 345
        Top = 40
        Width = 93
        Height = 14
        Caption = 'Usu'#225'rio de baixa'
      end
      object Label3: TLabel
        Left = 197
        Top = 39
        Width = 106
        Height = 14
        Caption = 'Data/hora cadastro'
      end
      object Label4: TLabel
        Left = 539
        Top = 39
        Width = 89
        Height = 14
        Caption = 'Data/hora baixa'
      end
      object eEmpresa: TEditLuka
        Left = 3
        Top = 12
        Width = 188
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 0
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object ePesoTotal: TEditLuka
        Left = 345
        Top = 12
        Width = 68
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 1
        Text = '0,000'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 3
        NaoAceitarEspaco = False
      end
      object eQuantidadeProdutos: TEditLuka
        Left = 271
        Top = 12
        Width = 68
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 2
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eQuantidadeEntregas: TEditLuka
        Left = 197
        Top = 12
        Width = 68
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 3
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eUsuarioCadastro: TEditLuka
        Left = 3
        Top = 52
        Width = 188
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 4
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eUsuarioBaixa: TEditLuka
        Left = 345
        Top = 52
        Width = 188
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 5
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eDataHoraCadastro: TEditLukaData
        Left = 197
        Top = 52
        Width = 142
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999 99:99:99;1;_'
        MaxLength = 19
        TabOrder = 6
        Text = '  /  /       :  :  '
        TipoData = tdDataHora
      end
      object eDataHoraBaixa: TEditLukaData
        Left = 539
        Top = 52
        Width = 142
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999 99:99:99;1;_'
        MaxLength = 19
        TabOrder = 7
        Text = '  /  /       :  :  '
        TipoData = tdDataHora
      end
      inline FrAjudantes: TFrFuncionarios
        Left = 3
        Top = 80
        Width = 306
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 8
        TabStop = True
        ExplicitLeft = 3
        ExplicitTop = 80
        ExplicitWidth = 306
        ExplicitHeight = 81
        inherited sgPesquisa: TGridLuka
          Width = 281
          Height = 64
          ExplicitWidth = 281
          ExplicitHeight = 64
        end
        inherited CkFiltroDuplo: TCheckBox
          Top = 32
          ExplicitTop = 32
        end
        inherited PnTitulos: TPanel
          Width = 306
          ExplicitWidth = 306
          inherited lbNomePesquisa: TLabel
            Width = 55
            Caption = 'Ajudantes'
            ExplicitWidth = 55
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 201
            ExplicitLeft = 201
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 281
          Height = 65
          Visible = False
          ExplicitLeft = 281
          ExplicitHeight = 65
        end
        inherited ckBuscarSomenteAtivos: TCheckBox
          Checked = False
          State = cbUnchecked
        end
        inherited ckAjudante: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
    end
    object tsEntregas: TTabSheet
      Caption = 'Entregas'
      object sgEntregas: TGridLuka
        Left = 0
        Top = 0
        Width = 812
        Height = 374
        Align = alClient
        ColCount = 11
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
        TabOrder = 0
        OnDblClick = sgEntregasDblClick
        OnDrawCell = sgEntregasDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Entrega'
          'Data gera'#231#227'o'
          'Pedido'
          'Cliente'
          'Prev. entrega'
          'Empresa'
          'Local'
          'Bairro'
          'Rota'
          'Peso total'
          'Qtde.prod.')
        Grid3D = False
        RealColCount = 15
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          52
          81
          50
          224
          95
          131
          113
          103
          124
          94
          65)
      end
    end
    object tsItens: TTabSheet
      Caption = 'Itens das entregas'
      ImageIndex = 1
      object sgItens: TGridLuka
        Left = 0
        Top = 0
        Width = 812
        Height = 374
        Align = alClient
        ColCount = 8
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 0
        OnDrawCell = sgItensDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Produto'
          'Nome'
          'Marca'
          'Lote'
          'Quantidade'
          'Und.'
          'Devolvidos'
          'Retornados')
        Grid3D = False
        RealColCount = 8
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          64
          236
          126
          103
          74
          39
          70
          75)
      end
    end
  end
  object st3: TStaticText
    Left = 611
    Top = 10
    Width = 206
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Status'
    Color = 38619
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 6
    Transparent = False
  end
  object stStatus: TStaticText
    Left = 611
    Top = 25
    Width = 206
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Para recebimento no caixa'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 7
    Transparent = False
  end
end
