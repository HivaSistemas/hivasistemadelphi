unit BuscarDadosTransacaoCartao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.StdCtrls, _Biblioteca, _Sessao,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls;

type
  RecDadosTransacao = record
    NumeroCartao: string;
    CodigoAutorizacao: string;
    Nsu: string;
  end;

  TFormBuscarDadosTransacaoCartao = class(TFormHerancaFinalizar)
    lbBusca: TLabel;
    eCodigoAutorizacao: TEditLuka;
    lb1: TLabel;
    eNSU: TEditLuka;
    lb2: TLabel;
    eDigitosCartao: TEditLuka;
    procedure eNSUKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Buscar(
  pNumeroCartao: string;
  pNSU: string;
  pCodigoAutorizacao: string
): TRetornoTelaFinalizar<RecDadosTransacao>;

implementation

{$R *.dfm}

function Buscar(
  pNumeroCartao: string;
  pNSU: string;
  pCodigoAutorizacao: string
): TRetornoTelaFinalizar<RecDadosTransacao>;
var
  vForm: TFormBuscarDadosTransacaoCartao;
begin
  vForm := TFormBuscarDadosTransacaoCartao.Create(Application);

  vForm.eDigitosCartao.Text     := pNumeroCartao;
  vForm.eCodigoAutorizacao.Text := pCodigoAutorizacao;
  vForm.eNSU.Text               := pNSU;

  if Result.Ok(vForm.ShowModal) then begin
    Result.Dados.NumeroCartao      := vForm.eDigitosCartao.Text;
    Result.Dados.CodigoAutorizacao := vForm.eCodigoAutorizacao.Text;
    Result.Dados.Nsu               := vForm.eNSU.Text;
  end;
end;

procedure TFormBuscarDadosTransacaoCartao.eNSUKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if (eCodigoAutorizacao.Text <> '') and (eNSU.Text <> '') and (eDigitosCartao.Text <> '') then
    ModalResult := mrOk
  else
    ProximoCampo(Sender, Key, Shift);
end;

procedure TFormBuscarDadosTransacaoCartao.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if Length(eDigitosCartao.Text) < 4 then begin
    _Biblioteca.Exclamar('� necess�rio informar os 4 �ltimos digitos do cart�o, verifique!');
    SetarFoco(eDigitosCartao);
    Abort;
  end;

  if eCodigoAutorizacao.Trim = '' then begin
    _Biblioteca.Exclamar('O c�digo de autoriza��o n�o foi informado corretamente, verifique');
    SetarFoco(eCodigoAutorizacao);
    Abort;
  end;

  if eNSU.Trim = '' then begin
    _Biblioteca.Exclamar('O NSU n�o foi informado corretamente, verifique');
    SetarFoco(eNSU);
    Abort;
  end;
end;

end.
