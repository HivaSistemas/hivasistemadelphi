object dmRelatorio: TdmRelatorio
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 326
  Width = 496
  object dstEmpresa: TfrxDBDataset
    UserName = 'frxdstEmpresa'
    CloseDataSource = False
    FieldAliases.Strings = (
      'EMPRESA_ID=EMPRESA_ID'
      'CADASTRO_ID=CADASTRO_ID'
      'BAIRRO_ID=BAIRRO_ID'
      'CIDADE_ID=CIDADE_ID'
      'NOME_BAIRRO=NOME_BAIRRO'
      'LOGRADOURO=LOGRADOURO'
      'COMPLEMENTO=COMPLEMENTO'
      'NUMERO=NUMERO'
      'PONTO_REFERENCIA=PONTO_REFERENCIA'
      'CEP=CEP'
      'TELEFONE_PRINCIPAL=TELEFONE_PRINCIPAL'
      'TELEFONE_FAX=TELEFONE_FAX'
      'E_MAIL=E_MAIL'
      'DATA_FUNDACAO=DATA_FUNDACAO'
      'INSCRICAO_ESTADUAL=INSCRICAO_ESTADUAL'
      'INSCRICAO_MUNICIPAL=INSCRICAO_MUNICIPAL'
      'CNAE=CNAE'
      'CNPJ=CNPJ'
      'NOME_FANTASIA=NOME_FANTASIA'
      'NOME_RESUMIDO_IMPRESSOES_NF=NOME_RESUMIDO_IMPRESSOES_NF'
      'ATIVO=ATIVO'
      'RAZAO_SOCIAL=RAZAO_SOCIAL'
      'TIPO_EMPRESA=TIPO_EMPRESA'
      'NOME_CIDADE=NOME_CIDADE'
      'ESTADO_ID=ESTADO_ID'
      'CODIGO_IBGE_CIDADE=CODIGO_IBGE_CIDADE'
      'CODIGO_IBGE_ESTADO=CODIGO_IBGE_ESTADO')
    DataSet = qryEmpresa
    BCDToCurrency = False
    Left = 23
    Top = 62
  end
  object qryEmpresa: TOraQuery
    SQL.Strings = (
      'SELECT'
      '     EMP.EMPRESA_ID, '
      '      EMP.CADASTRO_ID, '
      '      EMP.BAIRRO_ID, '
      '      BAI.CIDADE_ID, '
      '      BAI.NOME_BAIRRO, '
      '      EMP.LOGRADOURO, '
      '      EMP.COMPLEMENTO, '
      '      EMP.NUMERO, '
      '      EMP.PONTO_REFERENCIA, '
      '      EMP.CEP, '
      '      EMP.TELEFONE_PRINCIPAL, '
      '      EMP.TELEFONE_FAX, '
      '      EMP.E_MAIL, '
      '      EMP.DATA_FUNDACAO, '
      '      EMP.INSCRICAO_ESTADUAL, '
      '      EMP.INSCRICAO_MUNICIPAL, '
      '      EMP.CNAE, '
      '      EMP.CNPJ, '
      '      EMP.NOME_FANTASIA, '
      '      EMP.NOME_RESUMIDO_IMPRESSOES_NF, '
      '      EMP.ATIVO, '
      '      EMP.RAZAO_SOCIAL, '
      '      EMP.TIPO_EMPRESA, '
      '      CID.NOME as NOME_CIDADE, '
      '      CID.ESTADO_ID, '
      '      CID.CODIGO_IBGE as CODIGO_IBGE_CIDADE, '
      '      EST.CODIGO_IBGE as CODIGO_IBGE_ESTADO '
      '    from '
      '      EMPRESAS EMP '
      ''
      '    inner join VW_BAIRROS BAI '
      '    on EMP.BAIRRO_ID = BAI.BAIRRO_ID '
      ''
      '    inner join CIDADES CID '
      '    on BAI.CIDADE_ID = CID.CIDADE_ID '
      ''
      '    inner join ESTADOS EST '
      '    on CID.ESTADO_ID = EST.ESTADO_ID '
      'WHERE EMP.EMPRESA_ID = :EMPRESAID')
    FetchAll = True
    AutoCommit = False
    AutoCalcFields = False
    Left = 23
    Top = 14
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'EMPRESAID'
        Value = Null
      end>
    object qryEmpresaEMPRESA_ID: TIntegerField
      FieldName = 'EMPRESA_ID'
      Required = True
    end
    object qryEmpresaCADASTRO_ID: TFloatField
      FieldName = 'CADASTRO_ID'
    end
    object qryEmpresaBAIRRO_ID: TFloatField
      FieldName = 'BAIRRO_ID'
      Required = True
    end
    object qryEmpresaCIDADE_ID: TFloatField
      FieldName = 'CIDADE_ID'
    end
    object qryEmpresaNOME_BAIRRO: TStringField
      FieldName = 'NOME_BAIRRO'
      Size = 60
    end
    object qryEmpresaLOGRADOURO: TStringField
      FieldName = 'LOGRADOURO'
      Required = True
      Size = 100
    end
    object qryEmpresaCOMPLEMENTO: TStringField
      FieldName = 'COMPLEMENTO'
      Required = True
      Size = 100
    end
    object qryEmpresaNUMERO: TStringField
      FieldName = 'NUMERO'
      Required = True
      Size = 10
    end
    object qryEmpresaPONTO_REFERENCIA: TStringField
      FieldName = 'PONTO_REFERENCIA'
      Size = 100
    end
    object qryEmpresaCEP: TStringField
      FieldName = 'CEP'
      Required = True
      Size = 9
    end
    object qryEmpresaTELEFONE_PRINCIPAL: TStringField
      FieldName = 'TELEFONE_PRINCIPAL'
      Size = 15
    end
    object qryEmpresaTELEFONE_FAX: TStringField
      FieldName = 'TELEFONE_FAX'
      Size = 15
    end
    object qryEmpresaE_MAIL: TStringField
      FieldName = 'E_MAIL'
      Size = 30
    end
    object qryEmpresaDATA_FUNDACAO: TDateTimeField
      FieldName = 'DATA_FUNDACAO'
    end
    object qryEmpresaINSCRICAO_ESTADUAL: TStringField
      FieldName = 'INSCRICAO_ESTADUAL'
      Required = True
    end
    object qryEmpresaINSCRICAO_MUNICIPAL: TStringField
      FieldName = 'INSCRICAO_MUNICIPAL'
    end
    object qryEmpresaCNAE: TStringField
      FieldName = 'CNAE'
      Size = 12
    end
    object qryEmpresaCNPJ: TStringField
      FieldName = 'CNPJ'
      Required = True
      Size = 19
    end
    object qryEmpresaNOME_FANTASIA: TStringField
      FieldName = 'NOME_FANTASIA'
      Required = True
      Size = 60
    end
    object qryEmpresaNOME_RESUMIDO_IMPRESSOES_NF: TStringField
      FieldName = 'NOME_RESUMIDO_IMPRESSOES_NF'
      Size = 30
    end
    object qryEmpresaATIVO: TStringField
      FieldName = 'ATIVO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object qryEmpresaRAZAO_SOCIAL: TStringField
      FieldName = 'RAZAO_SOCIAL'
      Required = True
      Size = 60
    end
    object qryEmpresaTIPO_EMPRESA: TStringField
      FieldName = 'TIPO_EMPRESA'
      Required = True
      FixedChar = True
      Size = 1
    end
    object qryEmpresaNOME_CIDADE: TStringField
      FieldName = 'NOME_CIDADE'
      Size = 60
    end
    object qryEmpresaESTADO_ID: TStringField
      FieldName = 'ESTADO_ID'
      FixedChar = True
      Size = 2
    end
    object qryEmpresaCODIGO_IBGE_CIDADE: TIntegerField
      FieldName = 'CODIGO_IBGE_CIDADE'
    end
    object qryEmpresaCODIGO_IBGE_ESTADO: TIntegerField
      FieldName = 'CODIGO_IBGE_ESTADO'
    end
  end
end
