unit FrameDiversosContatos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.StdCtrls, _Biblioteca,
  ComboBoxLuka, _FrameHenrancaPesquisas, FrameBairros, EditLuka, Vcl.Mask, _RecordsCadastros,
  EditCEPLuka, Vcl.Grids, GridLuka, EditTelefoneLuka, System.Math;

type
  TFrDiversosContatos = class(TFrameHerancaPrincipal)
    lb14: TLabel;
    sgOutrosContatos: TGridLuka;
    eDescricao: TEditLuka;
    lb11: TLabel;
    eTelefone: TEditTelefoneLuka;
    eCelular: TEditTelefoneLuka;
    lb1: TLabel;
    lb9: TLabel;
    eRamal: TEditLuka;
    lb2: TLabel;
    lb3: TLabel;
    eEmail: TEditLuka;
    eObservacao: TMemo;
    procedure eObservacaoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgOutrosContatosDblClick(Sender: TObject);
    procedure sgOutrosContatosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgOutrosContatosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    _linha_alterando: Integer;
  public
    constructor Create(AOwner: TComponent); override;
    procedure setDiversosContatos(const pContatos: TArray<RecDiversosContatos>);
    function getRecDiversosContatos: TArray<RecDiversosContatos>;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
    procedure Clear; override;
  end;

implementation

{$R *.dfm}

const
  co_descricao   = 0;
  co_telefone    = 1;
  co_ramal       = 2;
  co_celular     = 3;
  co_email       = 4;
  co_observacao  = 5;

procedure TFrDiversosContatos.Clear;
begin
  inherited;
  _Biblioteca.LimparCampos([eDescricao, eTelefone, eRamal, eCelular, eEmail, eObservacao, sgOutrosContatos]);
end;

constructor TFrDiversosContatos.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  _linha_alterando := -1;
end;

procedure TFrDiversosContatos.eObservacaoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  adicionar: Boolean;
begin
  if Key <> VK_RETURN then
    Exit;

  adicionar :=
    (eDescricao.Text <> '') and
    (
      eTelefone.getTelefoneOk or
      eCelular.getTelefoneOk or
      (eEmail.Text <> '')
    );

  if not adicionar then begin
    ProximoCampo(Sender, Key, Shift);
    Exit;
  end;

  if (sgOutrosContatos.RowCount = 2) and (sgOutrosContatos.Cells[co_descricao, sgOutrosContatos.Row] = '') and (_linha_alterando = -1) then
    _linha_alterando := 1
  else if (_linha_alterando = -1) then begin
    sgOutrosContatos.RowCount := sgOutrosContatos.RowCount + 1;
    _linha_alterando := sgOutrosContatos.RowCount - 1;
  end;

  sgOutrosContatos.Cells[co_descricao, _linha_alterando] := eDescricao.Text;
  sgOutrosContatos.Cells[co_telefone, _linha_alterando] := eTelefone.Text;
  sgOutrosContatos.Cells[co_ramal, _linha_alterando] := eRamal.Text;
  sgOutrosContatos.Cells[co_celular, _linha_alterando] := eCelular.Text;
  sgOutrosContatos.Cells[co_email, _linha_alterando] := eEmail.Text;
  sgOutrosContatos.Cells[co_observacao, _linha_alterando] := eObservacao.Lines.Text;

  _linha_alterando := -1;
  _Biblioteca.LimparCampos([eDescricao, eTelefone, eRamal, eCelular, eEmail, eObservacao]);

  ProximoCampo(Sender, Key, Shift);
end;

function TFrDiversosContatos.getRecDiversosContatos: TArray<RecDiversosContatos>;
var
  i: Integer;
begin
  Result := nil;

  if sgOutrosContatos.Cells[co_descricao, 1] = '' then
    Exit;

  SetLength(Result, sgOutrosContatos.RowCount - 1);
  for i := sgOutrosContatos.FixedRows to sgOutrosContatos.RowCount - 1 do begin
    Result[i - sgOutrosContatos.FixedRows].descricao := sgOutrosContatos.Cells[co_descricao, i];
    Result[i - sgOutrosContatos.FixedRows].telefone := sgOutrosContatos.Cells[co_telefone, i];
    Result[i - sgOutrosContatos.FixedRows].ramal := StrToIntDef(sgOutrosContatos.Cells[co_ramal, i], 0);
    Result[i - sgOutrosContatos.FixedRows].celular := sgOutrosContatos.Cells[co_celular, i];
    Result[i - sgOutrosContatos.FixedRows].e_mail := sgOutrosContatos.Cells[co_email, i];
    Result[i - sgOutrosContatos.FixedRows].observacao := sgOutrosContatos.Cells[co_observacao, i];
  end;
end;

procedure TFrDiversosContatos.Modo(pEditando: Boolean; pLimpar: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    eDescricao,
    eTelefone,
    eRamal,
    eCelular,
    eEmail,
    eObservacao],
    pEditando,
    pLimpar
  );
end;

procedure TFrDiversosContatos.setDiversosContatos(const pContatos: TArray<RecDiversosContatos>);
var
  i: Integer;
begin
  for i := Low(pContatos) to High(pContatos) do begin
    sgOutrosContatos.Cells[co_descricao, i + 1] := pContatos[i].descricao;
    sgOutrosContatos.Cells[co_telefone, i + 1] := pContatos[i].telefone;
    sgOutrosContatos.Cells[co_ramal, i + 1] := IntToStr(pContatos[i].ramal);
    sgOutrosContatos.Cells[co_celular, i + 1] := pContatos[i].celular;
    sgOutrosContatos.Cells[co_email, i + 1] := pContatos[i].e_mail;
    sgOutrosContatos.Cells[co_observacao, i + 1] := pContatos[i].observacao;
  end;

  sgOutrosContatos.RowCount := IfThen(Length(pContatos) > 1, High(pContatos) + 2, 2);
end;

procedure TFrDiversosContatos.sgOutrosContatosDblClick(Sender: TObject);
begin
  inherited;

  if sgOutrosContatos.Cells[co_descricao, sgOutrosContatos.Row] = '' then
    Exit;

  _linha_alterando := sgOutrosContatos.Row;

  eDescricao.Text := sgOutrosContatos.Cells[co_descricao, _linha_alterando];
  eTelefone.Text := sgOutrosContatos.Cells[co_telefone, _linha_alterando];
  eRamal.Text := sgOutrosContatos.Cells[co_ramal, _linha_alterando];
  eCelular.Text := sgOutrosContatos.Cells[co_celular, _linha_alterando];
  eEmail.Text := sgOutrosContatos.Cells[co_email, _linha_alterando];
  eObservacao.Lines.Add(sgOutrosContatos.Cells[co_observacao, _linha_alterando]);

  eDescricao.SetFocus;
end;

procedure TFrDiversosContatos.sgOutrosContatosDrawCell(Sender: TObject;
  ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  inherited;
  sgOutrosContatos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taLeftJustify, Rect);
end;

procedure TFrDiversosContatos.sgOutrosContatosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;

  if Key = VK_DELETE then
    sgOutrosContatos.DeleteRow(sgOutrosContatos.Row);
end;

end.
