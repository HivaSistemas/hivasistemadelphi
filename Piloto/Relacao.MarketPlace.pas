unit Relacao.MarketPlace;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl,
  Vcl.ComCtrls, Vcl.StdCtrls, CheckBoxLuka, Vcl.Buttons, Vcl.ExtCtrls,
  FrameProdutos, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameEmpresas,
  Vcl.Mask, EditLukaData, Frame.Inteiros, Vcl.Grids, GridLuka, FrameLocais, _Biblioteca,
  FrameDataInicialFinal, _RelacaoMarketPlace, _Sessao, Vcl.Menus, StaticTextLuka,
  FrameEmpresasFaturamento;

type
  TFormRelacaoMarketPlace = class(TFormHerancaRelatoriosPageControl)
    FrEmpresas: TFrEmpresas;
    FrProdutos: TFrProdutos;
    FrCodigoOrcamento: TFrameInteiros;
    sgProdutos: TGridLuka;
    FrLocais: TFrLocais;
    FrDataCadastro: TFrDataInicialFinal;
    stValorTotalEstoque: TStaticTextLuka;
    st3: TStaticText;
    FrEmpresaFaturamento: TFrEmpresasFaturamento;
  private
    FVendasProdutos: TArray<RecRelacaoVendasMarketPlace>;
  public
    { Public declarations }
  protected
    procedure Carregar(Sender: TObject); override;
  end;

var
  FormRelacaoMarketPlace: TFormRelacaoMarketPlace;

implementation

const
  {Grid de produtos}
  coOrcamentoId   = 0;
  coProdutoId     = 1;
  coNomeProduto   = 2;
  coDataCadastro  = 3;
  coLocal         = 4;
  coQuantidade    = 5;
  coValorUnitario = 6;
  coValorTotal    = 7;

  //Ocultas
  coTipoMovimento = 8;

{$R *.dfm}

{ TFormRelacaoMarketPlace }

procedure TFormRelacaoMarketPlace.Carregar(Sender: TObject);
var
  vComando: string;
  i: Integer;
begin
  inherited;
  sgProdutos.ClearGrid;
  _Biblioteca.LimparCampos([stValorTotalEstoque]);

  if not FrEmpresas.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrEmpresas.getSqlFiltros('EMPRESA_ID'));

  if not FrEmpresaFaturamento.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrEmpresaFaturamento.getSqlFiltros('EMPRESA_FATURAMENTO'));

  if not FrProdutos.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrProdutos.getSqlFiltros('PRODUTO_ID'));

  if not FrLocais.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrLocais.getSqlFiltros('LOCAL_ID'));

  if not FrCodigoOrcamento.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrCodigoOrcamento.getSqlFiltros('ORCAMENTO_ID'));

  if not FrDataCadastro.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrDataCadastro.getSqlFiltros('trunc(DATA_HORA_CADASTRO)') );

  vComando := vComando + 'order by DATA_HORA_CADASTRO asc, ORCAMENTO_ID asc, NOME_PRODUTO asc ';

    if FrEmpresas.EstaVazio then begin
    Exclamar('A empresa de origem do estoque n�o foi definida!Por favor verifique!');
    SetarFoco(FrEmpresas);
    Abort;
    end;

     if FrEmpresaFaturamento.EstaVazio then begin
    Exclamar('A empresa de faturamento n�o foi definida!Por favor verifique!');
    SetarFoco(FrEmpresaFaturamento);
    Abort;
     end;

      if FrDataCadastro.EstaVazio then begin
    Exclamar('N�o foi definido uma data!Por favor verifique!');
    SetarFoco(FrDataCadastro);
    Abort;


end;
  FVendasProdutos := _RelacaoMarketPlace.BuscarVendasMarketPlace(Sessao.getConexaoBanco, vComando);
  if FVendasProdutos = nil then begin
    NenhumRegistro;
    Exit;
  end;

  for i := Low(FVendasProdutos) to High(FVendasProdutos) do begin
    sgProdutos.Cells[coOrcamentoId, i + 1]       := NFormat(FVendasProdutos[i].OrcamentoId);
    sgProdutos.Cells[coProdutoId, i + 1]         := NFormat(FVendasProdutos[i].ProdutoId);
    sgProdutos.Cells[coNomeProduto, i + 1]       := FVendasProdutos[i].NomeProduto;
    sgProdutos.Cells[coDataCadastro, i + 1]      := DFormatN(FVendasProdutos[i].DataCadastro);
    sgProdutos.Cells[coLocal, i + 1]             := NFormat(FVendasProdutos[i].LocalId) + ' - ' + FVendasProdutos[i].NomeLocal;
    sgProdutos.Cells[coQuantidade, i + 1]        := NFormatN(FVendasProdutos[i].Quantidade);
    sgProdutos.Cells[coValorUnitario, i + 1]     := NFormatN(FVendasProdutos[i].ValorUnitario);
    sgProdutos.Cells[coValorTotal, i + 1]        := NFormatN(FVendasProdutos[i].ValorTotal);
    sgProdutos.Cells[coTipoMovimento, i + 1]     := FVendasProdutos[i].TipoMovimento;

    stValorTotalEstoque.Somar( SFormatDouble(sgProdutos.Cells[coValorTotal, i + 1]) );
  end;
  sgProdutos.SetLinhasGridPorTamanhoVetor( Length(FVendasProdutos) );

  SetarFoco(sgProdutos);
end;

end.
