inherited FormInformacoesGruposTributacoesEstadualVenda: TFormInformacoesGruposTributacoesEstadualVenda
  Caption = 'Informa'#231#245'es do grupo de tributa'#231#245'es estadual de venda'
  ClientHeight = 438
  ClientWidth = 753
  ExplicitWidth = 759
  ExplicitHeight = 467
  PixelsPerInch = 96
  TextHeight = 14
  object Label1: TLabel [0]
    Left = 8
    Top = 8
    Width = 37
    Height = 14
    Caption = 'C'#243'digo'
  end
  object lb1: TLabel [1]
    Left = 94
    Top = 8
    Width = 53
    Height = 14
    Caption = 'Descri'#231#227'o'
  end
  inherited pnOpcoes: TPanel
    Top = 401
    Width = 753
    TabOrder = 4
    ExplicitTop = 401
    ExplicitWidth = 753
  end
  object eID: TEditLuka
    Left = 8
    Top = 22
    Width = 80
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 0
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object ckAtivo: TCheckBoxLuka
    Left = 699
    Top = 5
    Width = 46
    Height = 17
    Caption = 'Ativo'
    Enabled = False
    TabOrder = 2
    CheckedStr = 'N'
  end
  object eDescricao: TEditLuka
    Left = 94
    Top = 22
    Width = 599
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 1
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  inline FrICMS: TFrICMS
    Left = 8
    Top = 47
    Width = 737
    Height = 348
    Align = alCustom
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 8
    ExplicitTop = 47
    ExplicitWidth = 737
    ExplicitHeight = 348
    inherited pnGrid: TPanel
      Width = 737
      Height = 348
      ExplicitWidth = 737
      ExplicitHeight = 348
      inherited sgICMS: TGridLuka
        Width = 737
        Height = 348
        OnKeyDown = ProximoCampo
        ExplicitWidth = 737
        ExplicitHeight = 348
        ColWidths = (
          46
          60
          43
          56
          36
          43
          75
          62
          77
          107
          43
          79
          85)
      end
      inherited Panel1: TPanel
        Left = -100
        Height = 348
        Align = alNone
        Enabled = False
        Visible = False
        ExplicitLeft = -100
        ExplicitHeight = 348
        inherited eValor: TEditLuka
          Height = 22
          ExplicitHeight = 22
        end
      end
    end
  end
end
