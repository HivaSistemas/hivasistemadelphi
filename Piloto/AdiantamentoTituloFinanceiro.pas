unit AdiantamentoTituloFinanceiro;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Biblioteca, _Sessao,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, FramePagamentoFinanceiro, Vcl.StdCtrls, _ContasReceberBaixas,
  EditLuka, Vcl.Mask, EditLukaData, _ContasReceber, _RecordsEspeciais, _TurnosCaixas, _ContasReceberBaixasItens,
  Impressao.ComprovantePagamentoTituloReceberGrafico, _RecordsFinanceiros;

type
  TFormAdiantamentoTituloFinanceiro = class(TFormHerancaFinalizar)
    lb8: TLabel;
    lb22: TLabel;
    lb1: TLabel;
    lb4: TLabel;
    eDataPagamento: TEditLukaData;
    eValorTitulo: TEditLuka;
    eValorTotal: TEditLuka;
    eValorAdiantado: TEditLuka;
    FrPagamento: TFrPagamentoFinanceiro;
    eObservacoes: TEditLuka;
    lb2: TLabel;
    lb3: TLabel;
    eValorAdiantar: TEditLuka;
    procedure eValorAdiantarChange(Sender: TObject);
    procedure FrPagamentosbBuscarDadosCreditosClick(Sender: TObject);
  private
    FTitulo: TArray<RecContasReceber>;
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function AdiantarTitulo(pTituloId: Integer): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

function AdiantarTitulo(pTituloId: Integer): TRetornoTelaFinalizar;
var
  vTitulo: TArray<RecContasReceber>;
  vForm: TFormAdiantamentoTituloFinanceiro;
begin
  Result.RetTela := trCancelado;

  if pTituloId = 0 then
    Exit;

  vTitulo := _ContasReceber.BuscarContasReceber(Sessao.getConexaoBanco, 0, [pTituloId]);
  if vTitulo = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  if vTitulo[0].forma_pagamento = 'CRT' then begin
    _Biblioteca.Exclamar('N�o � permitido adiantamentos para cart�es!');
    Exit;
  end;

  if Sessao.TipoCobrancaNaoPermiteAdiantamento(vTitulo[0].cobranca_id) then begin
    _Biblioteca.Exclamar('Este financeiro n�o pode ser adiantado pois o tipo de cobran�a ao qual o mesmo est� ligado faz parte de um controle interno do Hiva!');
    Exit;
  end;

  vForm := TFormAdiantamentoTituloFinanceiro.Create(Application);

  vForm.eValorTitulo.AsCurr    := vTitulo[0].ValorDocumento;
  vForm.eValorAdiantado.AsCurr := vTitulo[0].ValorAdiantado;
  vForm.eValorTotal.AsCurr     := vTitulo[0].ValorDocumento - vTitulo[0].ValorAdiantado;

  vForm.FTitulo := vTitulo;

  Result.Ok(vForm.ShowModal, True);
end;

{ TFormAdiantamentoTituloFinanceiro }

procedure TFormAdiantamentoTituloFinanceiro.eValorAdiantarChange(Sender: TObject);
begin
  inherited;
  FrPagamento.ValorTitulos := eValorAdiantar.AsCurr;
end;

procedure TFormAdiantamentoTituloFinanceiro.Finalizar(Sender: TObject);
var
  vTurnoId: Integer;
  vBaixaId: Integer;
  vRetBanco: RecRetornoBD;
  vReceberAdiantamentoId: Integer;
begin

  vTurnoId := 0;
  if FrPagamento.eValorDinheiro.AsCurr > 0 then begin
    vTurnoId := _TurnosCaixas.getTurnoId( Sessao.getConexaoBanco, Sessao.getUsuarioLogado.funcionario_id );
    if (vTurnoId = 0) and (FrPagamento.Dinheiro = nil) then begin
      _Biblioteca.Exclamar('N�o foi encontrada a defini��o das contas para o recebimento em dinheiro!');
      SetarFoco(FrPagamento.eValorDinheiro);
      Abort;
    end;
  end;

  Sessao.getConexaoBanco.IniciarTransacao;

 { ******************* Baixa dos t�tulos ******************* }
  // Gerando a capa da baixa
  vRetBanco :=
    _ContasReceberBaixas.AtualizarContaReceberBaixa(
      Sessao.getConexaoBanco,
      0,
      Sessao.getEmpresaLogada.EmpresaId,
      FTitulo[0].CadastroId,
      IIfInt((vTurnoId > 0) and (FrPagamento.Dinheiro = nil), vTurnoId),
      eValorAdiantar.AsCurr,
      0,
      0,
      0,
      0,
      FrPagamento.eValorDesconto.AsCurr,
      FrPagamento.eValorDinheiro.AsCurr,
      FrPagamento.eValorCheque.AsCurr,
      FrPagamento.eValorCartaoDebito.AsCurr,
      FrPagamento.eValorCartaoCredito.AsCurr,
      FrPagamento.eValorCobranca.AsCurr,
      FrPagamento.eValorCredito.AsCurr,
      IIfData( FrPagamento.eValorCartaoDebito.AsCurr + FrPagamento.eValorCartaoCredito.AsCurr = 0, eDataPagamento.AsData),
      IIfStr( FrPagamento.eValorCartaoDebito.AsCurr + FrPagamento.eValorCartaoCredito.AsCurr > 0, 'S', 'N' ),
      eObservacoes.Text,
      IIfStr( FrPagamento.eValorCartaoDebito.AsCurr + FrPagamento.eValorCartaoCredito.AsCurr = 0 , 'S', 'N' ),
      'ADI',
      0,
      FrPagamento.Dinheiro,
      FrPagamento.CartoesDebito,
      FrPagamento.CartoesCredito,
      FrPagamento.Cobrancas,
      FrPagamento.Cheques,
      FrPagamento.Creditos,
      FTitulo[0].ReceberId,
      0,
      True,
      FrPagamento.eValorPix.AsCurr,
      FrPagamento.Pix,
      'N'
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  vBaixaId := vRetBanco.AsInt;

  vRetBanco := _ContasReceberBaixas.LancarAdiantamentoContasRceber(Sessao.getConexaoBanco, vBaixaId, True);
  Sessao.AbortarSeHouveErro(vRetBanco);

  vReceberAdiantamentoId := vRetBanco.AsInt;

  // Gravando os t�tulos da baixa
  vRetBanco :=
    _ContasReceberBaixasItens.AtualizarContasReceberBaixasItens(
      Sessao.getConexaoBanco,
      vBaixaId,
      vReceberAdiantamentoId,
      FrPagamento.eValorDinheiro.AsCurr,
      FrPagamento.eValorCartaoDebito.AsCurr,
      FrPagamento.eValorCartaoCredito.AsCurr,
      FrPagamento.eValorCheque.AsCurr,
      FrPagamento.eValorCobranca.AsCurr,
      FrPagamento.eValorCredito.AsCurr,
      0,
      0,
      0,
      FrPagamento.eValorPix.AsCurr,
      True
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  Sessao.getConexaoBanco.FinalizarTransacao;

  // O ccmprovante ser� impresso no caixa
  if FrPagamento.eValorCartaoDebito.AsCurr + FrPagamento.eValorCartaoCredito.AsCurr = 0 then begin
    RotinaSucesso;
    if Perguntar('Deseja imprimir o comprovante de baixa dos t�tulos?') then
      Impressao.ComprovantePagamentoTituloReceberGrafico.Imprimir(vBaixaId);
  end
  else
    _Biblioteca.Informar('Baixa realizada com sucesso, conclua a opera��o no caixa.');

  inherited;
end;

procedure TFormAdiantamentoTituloFinanceiro.FrPagamentosbBuscarDadosCreditosClick(
  Sender: TObject);
begin
  inherited;
  FrPagamento.setNatureza('R');
  if FTitulo[0].CadastroId > 0 then
    FrPagamento.setCadastroId(FTitulo[0].CadastroId);
  FrPagamento.sbBuscarDadosCreditosClick(Sender);
end;

procedure TFormAdiantamentoTituloFinanceiro.VerificarRegistro(Sender: TObject);
var
  i: Integer;
  vTitulosIds: TArray<Integer>;
begin
  inherited;
  if eValorAdiantar.AsCurr = 0 then begin
    _Biblioteca.Exclamar('O valor a ser adiantado n�o foi informado corretamente, verifique!');
    SetarFoco(eValorAdiantar);
    Abort;
  end;

  if eValorAdiantar.AsCurr >= eValorTotal.AsCurr then begin
    _Biblioteca.Exclamar('N�o � permitido realizar um adiantamento maior ou igual que o valor l�quido do t�tulo, verifique!');
    SetarFoco(eValorAdiantar);
    Abort;
  end;

  if not eDataPagamento.DataOk then begin
    Exclamar('A data do pagamento deve ser informada!');
    SetarFoco(eDataPagamento);
    Abort;
  end;

  FrPagamento.VerificarRegistro;

  vTitulosIds := nil;
  for i := Low(FTitulo) to High(FTitulo) do
    _Biblioteca.AddNoVetorSemRepetir( vTitulosIds, FTitulo[i].ReceberId );

  if _ContasReceber.ExistemTitulosBaixados( Sessao.getConexaoBanco, vTitulosIds ) then begin
    Exclamar('Existem t�tulos para esta baixa que j� foram baixados, verifique!');
    SetarFoco(FrPagamento);
    Abort;
  end;

  if FrPagamento.ExisteDiferenca then begin
    _Biblioteca.Exclamar('Existe diferen�a com o valor total a pagar e as formas de pagamento definidas, verifique!');
    SetarFoco(FrPagamento.eValorDinheiro);
    Abort;
  end;
end;

end.
