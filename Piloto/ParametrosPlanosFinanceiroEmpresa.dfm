inherited FormParametrosPlanoFinanceiroEmpresa: TFormParametrosPlanoFinanceiroEmpresa
  Caption = 'Par'#226'metros de planos financeiro por empresa'
  ClientHeight = 494
  ClientWidth = 907
  OnShow = FormShow
  ExplicitWidth = 913
  ExplicitHeight = 523
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 494
    ExplicitHeight = 494
    inherited sbExcluir: TSpeedButton
      Left = -136
      Visible = False
      ExplicitLeft = -136
    end
    inherited sbPesquisar: TSpeedButton
      Left = -136
      Visible = False
      ExplicitLeft = -136
    end
    inherited sbLogs: TSpeedButton
      Top = 84
      ExplicitTop = 84
    end
  end
  inline FrEmpresa: TFrEmpresas
    Left = 125
    Top = 7
    Width = 326
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 125
    ExplicitTop = 7
    ExplicitWidth = 326
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 301
      Height = 23
      ExplicitWidth = 301
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 326
      ExplicitWidth = 326
      inherited lbNomePesquisa: TLabel
        Width = 47
        Height = 14
        Caption = 'Empresa'
        ExplicitWidth = 47
        ExplicitHeight = 14
      end
    end
    inherited pnPesquisa: TPanel
      Left = 301
      Height = 24
      ExplicitLeft = 301
      ExplicitHeight = 24
    end
  end
  object pcPlanos: TPageControl
    Left = 125
    Top = 49
    Width = 780
    Height = 445
    ActivePage = tsEstoque
    TabOrder = 2
    object tsEstoque: TTabSheet
      Caption = 'Estoque'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      inline FrPlanoFinanceiroAjusteEstoqueNormal: TFrPlanosFinanceiros
        Left = 3
        Top = 3
        Width = 320
        Height = 41
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 3
        ExplicitTop = 3
        ExplicitHeight = 41
        inherited sgPesquisa: TGridLuka
          Height = 24
          ExplicitHeight = 24
        end
        inherited PnTitulos: TPanel
          inherited lbNomePesquisa: TLabel
            Width = 260
            Height = 14
            Caption = 'Plano financeiro para ajuste de estoque normal'
            ExplicitWidth = 260
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Top = 16
            ExplicitTop = 16
          end
        end
        inherited pnPesquisa: TPanel
          Height = 25
          ExplicitHeight = 25
        end
      end
    end
  end
end
