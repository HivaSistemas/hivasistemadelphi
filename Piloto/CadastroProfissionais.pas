unit CadastroProfissionais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Cadastros, Vcl.Menus, Vcl.StdCtrls,
  FrameDiversosContatos, FrameDiversosEnderecos, EditTelefoneLuka,
  _FrameHerancaPrincipal, FrameEndereco, EditLukaData, ComboBoxLuka,
  Vcl.ComCtrls, Vcl.ExtCtrls, RadioGroupLuka, Vcl.Mask, EditCpfCnpjLuka,
  CheckBoxLuka, EditLuka, Vcl.Buttons, _FrameHenrancaPesquisas,
  FrameCargosProfissionais, MemoAltis, _Profissionais, FrameCadastrosTelefones;

type
  TFormCadastroProfissionais = class(TFormCadastros)
    tsParametrosProfissional: TTabSheet;
    FrCargoProfissional: TFrCargosProfissionais;
    eNumeroCrea: TEditLuka;
    Label3: TLabel;
    ckRegraGeralComissaoGrupos: TCheckBoxLuka;
    Label4: TLabel;
    ePercentualComissao: TEditLuka;
    ckVip: TCheckBoxLuka;
    Label5: TLabel;
    meInformacoes: TMemoAltis;
    Label6: TLabel;
    ePercentualComissaoPrazo: TEditLuka;
    procedure ckRegraGeralComissaoGruposClick(Sender: TObject);
  private
    procedure PreencherRegistro(pProfissional: RecProfissionais);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

uses _Biblioteca, _RecordsEspeciais, _Sessao;

{ TFormCadastroProfissionais }

procedure TFormCadastroProfissionais.BuscarRegistro;
var
  vDados: TArray<RecProfissionais>;
begin
  inherited;
  vDados := _Profissionais.BuscarProfissionais(Sessao.getConexaoBanco, 0, [eID.AsInt], False);
  if vDados <> nil then
    PreencherRegistro(vDados[0]);
end;

procedure TFormCadastroProfissionais.ckRegraGeralComissaoGruposClick(
  Sender: TObject);
begin
  inherited;
  _Biblioteca.Habilitar([ePercentualComissao, ePercentualComissaoPrazo], not ckRegraGeralComissaoGrupos.Checked);
end;

procedure TFormCadastroProfissionais.ExcluirRegistro;
var
  retorno: RecRetornoBD;
begin
  retorno := _Profissionais.ExcluirProfissional(Sessao.getConexaoBanco, eId.AsInt);

  if retorno.TeveErro then begin
    _Biblioteca.Exclamar(retorno.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroProfissionais.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vRetBanco :=
    _Profissionais.AtualizarProfissional(
      Sessao.getConexaoBanco,
      FPesquisouCadastro,
      eID.AsInt,
      FCadastro,
      FTelefones,
      FEnderecos,
      ToChar(ckAtivo),
      FrCargoProfissional.GetCargoProfissional.CargoId,
      eNumeroCrea.Text,
      ePercentualComissao.AsDouble,
      ePercentualComissaoPrazo.AsDouble,
      ToChar(ckRegraGeralComissaoGrupos),
      ToChar(ckVip),
      Trim(meInformacoes.Text)
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormCadastroProfissionais.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    ckVip,
    eNumeroCrea,
    FrCargoProfissional,
    meInformacoes],
    pEditando
  );

  ckRegraGeralComissaoGrupos.Enabled := pEditando;
  ckRegraGeralComissaoGrupos.Checked := True;

  if not pEditando then begin
    ePercentualComissao.Clear;
    ePercentualComissaoPrazo.Clear;
  end;
end;

procedure TFormCadastroProfissionais.PreencherRegistro(pProfissional: RecProfissionais);
begin
  ckVip.Checked := (pProfissional.Vip = 'S');
  ckRegraGeralComissaoGrupos.Checked := (pProfissional.RegraGeralComissaoGrupos = 'S');
  ePercentualComissao.AsDouble := pProfissional.PercentualComissao;
  ePercentualComissaoPrazo.AsDouble := pProfissional.PercentualComissaoPrazo;
  eNumeroCrea.Text := pProfissional.NumeroCrea;
  FrCargoProfissional.InserirDadoPorChave(pProfissional.CargoId, False);
  meInformacoes.Lines.Text := pProfissional.Informacoes;

  ePercentualComissao.Enabled := not ckRegraGeralComissaoGrupos.Checked;
  ePercentualComissaoPrazo.Enabled := not ckRegraGeralComissaoGrupos.Checked;

  pProfissional.Free;
end;

procedure TFormCadastroProfissionais.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrCargoProfissional.EstaVazio then begin
    _Biblioteca.Exclamar('O cargo do profissional n�o foi informado corretamente, verifique!');
    SetarFoco(FrCargoProfissional);
    Abort;
  end;
end;

end.
