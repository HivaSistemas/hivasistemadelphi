unit RelacaoControlesEntregas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _Sessao, _Biblioteca,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, FrameClientes, FrameNumeros, _ControlesEntregas,
  FrameDataInicialFinal, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, InformacoesControleEntrega,
  FrameMotoristas, Vcl.Grids, GridLuka, Vcl.StdCtrls, StaticTextLuka, _Entregas, _RecordsExpedicao,
  Informacoes.Entrega, GroupBoxLuka, FrameEmpresas, CheckBoxLuka, ComboBoxLuka, ImpressaoRelacaoControleEntregasGrafico,
  FrameVeiculos, frxClass;

type
  TFormRelacaoControlesEntregas = class(TFormHerancaRelatoriosPageControl)
    FrMotoristas: TFrMotoristas;
    FrDataCadastro: TFrDataInicialFinal;
    FrCodigoControle: TFrNumeros;
    FrClientes: TFrClientes;
    StaticTextLuka1: TStaticTextLuka;
    sgControles: TGridLuka;
    splSeparador: TSplitter;
    StaticTextLuka2: TStaticTextLuka;
    sgEntregas: TGridLuka;
    gbStatus: TGroupBoxLuka;
    ckEmTransporte: TCheckBox;
    ckBaixados: TCheckBox;
    FrEmpresas: TFrEmpresas;
    lb1: TLabel;
    cbAgrupamento: TComboBoxLuka;
    FrVeiculos: TFrVeiculos;
    SpeedButton1: TSpeedButton;
    procedure sgControlesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgEntregasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure sgControlesDblClick(Sender: TObject);
    procedure sgControlesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgControlesClick(Sender: TObject);
    procedure sgEntregasDblClick(Sender: TObject);
    procedure FrVeiculossbPesquisaClick(Sender: TObject);
  private
    FEntregas: TArray<RecEntrega>;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

const
  coControle      = 0;
  coDataHoraCad   = 1;
  coUsuarioCad    = 2;
  coEmpresa       = 3;
  coMotorista     = 4;
  coVeiculo       = 5;
  coStatus        = 6;
  coValorTotalEnt = 7;
  coPesototal     = 8;
  coQtdeItens     = 9;
  coQtdeEntregas  = 10;
  coDataHoraBai   = 11;
  coUsuarioBai    = 12;
  coTipoLinha     = 13;

  (* Grid de entregas *)
  ceEntregaId    = 0;
  ceCliente      = 1;
  ceEmpresaEntr  = 2;
  ceValorEntrega = 3;
  cePesoTotal    = 4;
  ceQtdeItens    = 5;

  (* Tipos de linha *)
  clNormal          = 'NOR';
  clTotalizador     = 'TOT';
  clTotAgrupador    = 'TAGRU';
  clAgrupador       = 'AGR';

  caAgrupMotorista = 'MOT';
  caAgrupVeiculo   = 'VEI';

procedure TFormRelacaoControlesEntregas.Carregar(Sender: TObject);
var
  i: Integer;
  vComando: string;
  vControles: TArray<RecControlesEntregas>;

  vAgrupadorId: Integer;
  vControleIds: TArray<Integer>;

  vLinha: Integer;

  vTotalAgrupador: record
    TotalEntregas: Currency;
    TotalPeso: Currency;
    TotalItens: Integer;
    QtdeTotalEntregas: Integer;
  end;

  vTotais: record
    ValorTotalEntr: Currency;
    TotalPeso: Currency;
    TotalItens: Integer;
    QtdeTotalEntregas: Integer;
  end;

  procedure TotalizarAgrupador;
  begin
    Inc(vLinha);

    if cbAgrupamento.GetValor = caAgrupMotorista then
      sgControles.Cells[coVeiculo, vLinha]       := 'TOT.MOTORISTA: '
    else
      sgControles.Cells[coVeiculo, vLinha]       := 'TOT.VE�CULO: ';

    sgControles.Cells[coValorTotalEnt, vLinha] := NFormatN(vTotalAgrupador.TotalEntregas);
    sgControles.Cells[coPesototal, vLinha]     := NFormatNEstoque(vTotalAgrupador.TotalPeso);
    sgControles.Cells[coQtdeItens, vLinha]     := NFormatN(vTotalAgrupador.TotalItens);
    sgControles.Cells[coQtdeEntregas, vLinha]  := NFormatN(vTotalAgrupador.QtdeTotalEntregas);
    sgControles.Cells[coTipoLinha, vLinha]     := clTotAgrupador;

    vTotalAgrupador.TotalEntregas     := 0;
    vTotalAgrupador.TotalPeso         := 0;
    vTotalAgrupador.TotalItens        := 0;
    vTotalAgrupador.QtdeTotalEntregas := 0;
  end;

begin
  inherited;

  FEntregas := nil;
  vControleIds := nil;
  sgControles.ClearGrid();
  sgEntregas.ClearGrid();

  if not FrEmpresas.EstaVazio then
    _Biblioteca.WhereOuAnd( vComando, FrEmpresas.getSqlFiltros('CON.EMPRESA_ID') );

  if not FrMotoristas.EstaVazio then
    _Biblioteca.WhereOuAnd( vComando, FrMotoristas.getSqlFiltros('CON.MOTORISTA_ID') );

  if not FrClientes.EstaVazio then begin
    _Biblioteca.WhereOuAnd(
      vComando,
      'CON.CONTROLE_ENTREGA_ID in( ' +
      '  select distinct ' +
      '    ITE.CONTROLE_ENTREGA_ID ' +
      '  from ' +
      '    CONTROLES_ENTREGAS_ITENS ITE ' +

      '  inner join ENTREGAS ENT ' +
      '  on ITE.ENTREGA_ID = ENT.ENTREGA_ID ' +

      '  inner join ORCAMENTOS ORC ' +
      '  on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID ' +

      '  where ' + FrClientes.getSqlFiltros('CLIENTE_ID') + ' ' +
      ') '
    );
  end;

  if not FrDataCadastro.EstaVazio then
    _Biblioteca.WhereOuAnd( vComando, FrDataCadastro.getSqlFiltros('trunc(CON.DATA_HORA_CADASTRO)') );

  if not FrCodigoControle.EstaVazio then
    _Biblioteca.WhereOuAnd( vComando, FrCodigoControle.TrazerFiltros('CON.CONTROLE_ENTREGA_ID') );

  FrVeiculos.getSqlFiltros('CON.VEICULO_ID', vComando, True);

  if not gbStatus.TodosMarcados then begin
    if ckEmTransporte.Checked then
      _Biblioteca.WhereOuAnd(vComando, 'CON.STATUS = ''T'' ')
    else
      _Biblioteca.WhereOuAnd(vComando, 'CON.STATUS = ''B'' ');
  end;

  if cbAgrupamento.GetValor = caAgrupMotorista then
    vComando := vComando + ' order by CAD.NOME_FANTASIA, CON.CONTROLE_ENTREGA_ID '
  else
    vComando := vComando + ' order by CON.CONTROLE_ENTREGA_ID ';

  vControles := _ControlesEntregas.BuscarControlesComando(Sessao.getConexaoBanco, vComando);
  if vControles = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  vLinha := 0;
  vAgrupadorId := -1;

  vTotais.ValorTotalEntr    := 0;
  vTotais.TotalPeso         := 0;
  vTotais.TotalItens        := 0;
  vTotais.QtdeTotalEntregas := 0;

  for i := Low(vControles) to High(vControles) do begin
    if cbAgrupamento.GetValor = caAgrupMotorista then begin
      if vAgrupadorId <> vControles[i].MotoristaId then begin
        if i > 0 then
         TotalizarAgrupador;

        Inc(vLinha);

        vAgrupadorId := vControles[i].MotoristaId;
        sgControles.Cells[coControle, vLinha]  := getInformacao(vControles[i].MotoristaId, vControles[i].NomeMotorista);
        sgControles.Cells[coTipoLinha, vLinha] := clAgrupador;
      end;
    end
    else if cbAgrupamento.GetValor = caAgrupVeiculo then begin
      if vAgrupadorId <> vControles[i].VeiculoId then begin
        if i > 0 then
         TotalizarAgrupador;

        Inc(vLinha);

        vAgrupadorId := vControles[i].VeiculoId;
        sgControles.Cells[coControle, vLinha]  := getInformacao(vControles[i].VeiculoId, vControles[i].NomeVeiculo);
        sgControles.Cells[coTipoLinha, vLinha] := clAgrupador;
      end;
    end;

    Inc(vLinha);

    sgControles.Cells[coControle, vLinha]      := NFormat(vControles[i].ControleEntregaId);
    sgControles.Cells[coDataHoraCad, vLinha]   := DHFormat(vControles[i].DataHoraCadastro);
    sgControles.Cells[coUsuarioCad, vLinha]    := NFormat(vControles[i].UsuarioCadastroId) + ' - ' + vControles[i].NomeUsuarioCadastro;
    sgControles.Cells[coEmpresa, vLinha]       := NFormat(vControles[i].EmpresaId) + ' - ' + vControles[i].NomeEmpresa;
    sgControles.Cells[coMotorista, vLinha]     := NFormat(vControles[i].MotoristaId) + ' - ' + vControles[i].NomeMotorista;
    sgControles.Cells[coVeiculo, vLinha]       := NFormat(vControles[i].VeiculoId) + ' - ' + vControles[i].NomeVeiculo;
    sgControles.Cells[coStatus, vLinha]        := vControles[i].StatusAnalitico;
    sgControles.Cells[coValorTotalEnt, vLinha] := NFormat(vControles[i].ValorEntregas);
    sgControles.Cells[coPesototal, vLinha]     := NFormatNEstoque(vControles[i].PesoTotal);
    sgControles.Cells[coQtdeItens, vLinha]     := NFormatN(vControles[i].QtdeProdutos);
    sgControles.Cells[coQtdeEntregas, vLinha]  := NFormatN(vControles[i].QtdeEntregas);
    sgControles.Cells[coDataHoraBai, vLinha]   := DHFormatN(vControles[i].DataHoraBaixa);
    sgControles.Cells[coUsuarioBai, vLinha]    := getInformacao(vControles[i].UsuarioBaixaId, vControles[i].NomeUsuarioBaixa);
    sgControles.Cells[coTipoLinha, vLinha]     := clNormal;

    vTotais.ValorTotalEntr  := vTotais.ValorTotalEntr + vControles[i].ValorEntregas;
    vTotais.TotalPeso       := vTotais.TotalPeso + vControles[i].PesoTotal;
    vTotais.TotalItens      := vTotais.TotalItens + vControles[i].QtdeProdutos;
    vTotais.QtdeTotalEntregas := vTotais.QtdeTotalEntregas + vControles[i].QtdeEntregas;

    vTotalAgrupador.TotalEntregas        := vTotalAgrupador.TotalEntregas + vControles[i].ValorEntregas;
    vTotalAgrupador.TotalPeso            := vTotalAgrupador.TotalPeso + vControles[i].PesoTotal;
    vTotalAgrupador.TotalItens           := vTotalAgrupador.TotalItens + vControles[i].QtdeProdutos;
    vTotalAgrupador.QtdeTotalEntregas    := vTotalAgrupador.QtdeTotalEntregas + vControles[i].QtdeEntregas;

    _Biblioteca.AddNoVetorSemRepetir( vControleIds, vControles[i].ControleEntregaId );
  end;

  if cbAgrupamento.GetValor <> 'NEN' then
    TotalizarAgrupador;

  Inc(vLinha);

  sgControles.Cells[coDataHoraCad, vLinha]   := 'TOTAIS: ';
  sgControles.Cells[coValorTotalEnt, vLinha] := NFormatN(vTotais.ValorTotalEntr);
  sgControles.Cells[coPesototal, vLinha]     := NFormatNEstoque(vTotais.TotalPeso);
  sgControles.Cells[coQtdeItens, vLinha]     := NFormatN(vTotais.TotalItens);
  sgControles.Cells[coQtdeEntregas, vLinha]  := NFormatN(vTotais.QtdeTotalEntregas);
  sgControles.Cells[coTipoLinha, vLinha]     := clTotalizador;

  sgControles.SetLinhasGridPorTamanhoVetor( vLinha );

  FEntregas := _Entregas.BuscarEntregasComando( Sessao.getConexaoBanco, 'where ' + FiltroInInt('ENT.CONTROLE_ENTREGA_ID', vControleIds) );

  sgControlesClick(Sender);
  SetarFoco(sgControles);
end;

procedure TFormRelacaoControlesEntregas.FormShow(Sender: TObject);
begin
  inherited;
  FrEmpresas.InserirDadoPorChave( Sessao.getEmpresaLogada.EmpresaId, False );
  SetarFoco( FrEmpresas );
end;

procedure TFormRelacaoControlesEntregas.FrVeiculossbPesquisaClick(
  Sender: TObject);
begin
  inherited;
  FrVeiculos.sbPesquisaClick(Sender);

end;

procedure TFormRelacaoControlesEntregas.Imprimir(Sender: TObject);
var
  i: Integer;
  vDados: TArray<RecDadosImpressao>;
begin
  inherited;
//  if cbAgrupamento.GetValor <> caAgrupMotorista then begin
//    _Biblioteca.ImpressaoNaoDisponivel;
//    Exit;
//  end;

  vDados := nil;
  for i := 1 to sgControles.RowCount -1 do begin
    if sgControles.Cells[coTipoLinha, i] = clAgrupador then begin
      SetLength(vDados, Length(vDados) + 1);
      vDados[High(vDados)].Motorista := sgControles.Cells[coControle, i];

      Continue;
    end;

    // Se for a linha de totalizacao
    if sgControles.Cells[coTipoLinha, i] = clTotAgrupador then begin
      vDados[High(vDados)].QtdeEntregas       := SFormatInt(sgControles.Cells[coQtdeEntregas, i]);
      vDados[High(vDados)].QtdeTotalItens     := SFormatInt(sgControles.Cells[coQtdeItens, i]);
      vDados[High(vDados)].ValorTotalEntregas := SFormatDouble(sgControles.Cells[coValorTotalEnt, i]);
      vDados[High(vDados)].PesoTotal          := SFormatDouble(sgControles.Cells[coPesototal, i]);

      Continue;
    end;
  end;

  ImpressaoRelacaoControleEntregasGrafico.Imprimir(Self, vDados, FFiltrosUtilizados);
end;

procedure TFormRelacaoControlesEntregas.sgControlesClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vControleId: Integer;
begin
  inherited;

  vLinha := 0;
  sgEntregas.ClearGrid();

  vControleId := SFormatInt( sgControles.Cells[coControle, sgControles.Row] );
  for i := Low(FEntregas) to High(FEntregas) do begin
    if vControleId <> FEntregas[i].ControleEntregaId then
      Continue;

    Inc(vLinha);

    sgEntregas.Cells[ceEntregaId, vLinha]    := NFormat( FEntregas[i].EntregaId );
    sgEntregas.Cells[ceCliente, vLinha]      := NFormat( FEntregas[i].ClienteId ) + ' - ' + FEntregas[i].NomeCliente;
    sgEntregas.Cells[ceEmpresaEntr, vLinha]  := NFormat( FEntregas[i].EmpresaId ) + ' - ' + FEntregas[i].NomeEmpresa;
    sgEntregas.Cells[ceValorEntrega, vLinha] := NFormatN( FEntregas[i].ValorEntrega );
    sgEntregas.Cells[cePesoTotal, vLinha]    := NFormatNEstoque( FEntregas[i].PesoTotal );
    sgEntregas.Cells[ceQtdeItens, vLinha]    := NFormatNEstoque( FEntregas[i].QtdeProdutos );
  end;
  sgEntregas.SetLinhasGridPorTamanhoVetor( vLinha );
end;

procedure TFormRelacaoControlesEntregas.sgControlesDblClick(Sender: TObject);
begin
  inherited;
  InformacoesControleEntrega.Informar( SFormatInt(sgControles.Cells[coControle, sgControles.Row]) );
end;

procedure TFormRelacaoControlesEntregas.sgControlesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coControle, coValorTotalEnt, coPesototal, coQtdeItens, coQtdeEntregas] then
    vAlinhamento := taRightJustify
  else if ACol = coStatus then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  if sgControles.Cells[coTipoLinha, ARow] = clAgrupador then
    sgControles.MergeCells([ARow, ACol], [ARow, coControle], [ARow, coValorTotalEnt], taLeftJustify, Rect)
  else
    sgControles.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoControlesEntregas.sgControlesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgControles.Cells[coTipoLinha, ARow] = clTotalizador then begin
    ABrush.Color := _Biblioteca.getCorVerdePadraoAltis;
    AFont.Style  := [fsBold];
    AFont.Color  := clWhite;
  end
  else if sgControles.Cells[coTipoLinha, ARow] = clAgrupador then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end
  else if sgControles.Cells[coTipoLinha, ARow] = clTotAgrupador then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao1;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao1;
  end
  else begin
    if ACol = coStatus then begin
      AFont.Style := [fsBold];
      if sgControles.Cells[ACol, ARow] = 'Baixado' then
        AFont.Color := clBlue
      else
        AFont.Color := $000096DB;
    end;
  end;
end;

procedure TFormRelacaoControlesEntregas.sgEntregasDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Entrega.Informar( SFormatInt(sgEntregas.Cells[ceEntregaId, sgEntregas.Row]) );
end;

procedure TFormRelacaoControlesEntregas.sgEntregasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[ceEntregaId, ceValorEntrega, cePesoTotal, ceQtdeItens] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgEntregas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoControlesEntregas.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if gbStatus.NenhumMarcado then begin
    _Biblioteca.Exclamar('� neces�rio definir ao menos um status!');
    SetarFoco(ckEmTransporte);
    Abort;
  end;
end;

end.
