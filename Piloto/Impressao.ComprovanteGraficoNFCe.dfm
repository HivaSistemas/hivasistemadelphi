inherited FormComprovanteGraficoNFCe: TFormComprovanteGraficoNFCe
  Caption = 'FormComprovanteGraficoNFCe'
  ClientHeight = 749
  ClientWidth = 1370
  ExplicitWidth = 1386
  ExplicitHeight = 788
  PixelsPerInch = 96
  TextHeight = 13
  inherited qrRelatorioNF: TQuickRep
    Left = 330
    Top = 10
    Width = 624
    Height = 831
    BeforePrint = qrRelatorioNFBeforePrint
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    OnNeedData = qrRelatorioNFNeedData
    Page.Values = (
      0.000000000000000000
      999.403409090909100000
      0.000000000000000000
      750.454545454545500000
      0.000000000000000000
      0.000000000000000000
      0.000000000000000000)
    PrinterSettings.CustomPaperCode = 262
    Zoom = 220
    PrevShowThumbs = False
    PrevShowSearch = False
    PrevInitialZoom = qrZoomToWidth
    ExplicitLeft = 330
    ExplicitTop = 10
    ExplicitWidth = 624
    ExplicitHeight = 831
    inherited qrBandTitulo: TQRBand
      Width = 624
      Height = 215
      Size.Values = (
        258.570075757575800000
        750.454545454545500000)
      ExplicitWidth = 624
      ExplicitHeight = 215
      inherited qrlNFNomeEmpresa: TQRLabel
        Left = 31
        Top = 2
        Width = 564
        Height = 26
        Size.Values = (
          31.268939393939390000
          37.282196969696970000
          2.405303030303030000
          678.295454545454500000)
        Font.Charset = DEFAULT_CHARSET
        FontSize = 6
        ExplicitLeft = 31
        ExplicitTop = 2
        ExplicitWidth = 564
        ExplicitHeight = 26
      end
      inherited qrNFCidadeUf: TQRLabel [1]
        Left = 31
        Top = 56
        Width = 564
        Size.Values = (
          31.268939393939390000
          37.282196969696970000
          67.348484848484850000
          678.295454545454500000)
        Font.Height = -8
        Font.Style = [fsBold]
        FontSize = 6
        ExplicitLeft = 31
        ExplicitTop = 56
        ExplicitWidth = 564
      end
      inherited qrNFCnpj: TQRLabel [2]
        Left = 31
        Top = 83
        Width = 564
        Height = 23
        Size.Values = (
          27.660984848484850000
          37.282196969696970000
          99.820075757575760000
          678.295454545454500000)
        Font.Charset = DEFAULT_CHARSET
        Font.Height = -8
        FontSize = 6
        ExplicitLeft = 31
        ExplicitTop = 83
        ExplicitWidth = 564
        ExplicitHeight = 23
      end
      inherited qrlNFTelefoneEmpresa: TQRLabel [3]
        Left = 2
        Top = 109
        Width = 597
        Height = 23
        Size.Values = (
          27.660984848484850000
          2.405303030303031000
          131.089015151515200000
          717.982954545454500000)
        Alignment = taCenter
        Caption = 'DANFE NFC-e - Documento Auxiliar'
        Font.Height = -8
        Font.Style = [fsBold]
        FontSize = 6
        ExplicitLeft = 2
        ExplicitTop = 109
        ExplicitWidth = 597
        ExplicitHeight = 23
      end
      inherited qrNFEndereco: TQRLabel [4]
        Left = 31
        Top = 30
        Width = 564
        Height = 25
        Size.Values = (
          30.066287878787880000
          37.282196969696970000
          36.079545454545450000
          678.295454545454500000)
        Font.Charset = DEFAULT_CHARSET
        Font.Height = -8
        Font.Style = [fsBold]
        FontSize = 6
        ExplicitLeft = 31
        ExplicitTop = 30
        ExplicitWidth = 564
        ExplicitHeight = 25
      end
      object qr36: TQRLabel [5]
        Left = -21
        Top = 660
        Width = 66
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          -25.255681818181820000
          793.750000000000000000
          79.375000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Dt.cad.:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qr38: TQRLabel [6]
        Left = 209
        Top = 623
        Width = 106
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          251.354166666666700000
          749.251893939394000000
          127.481060606060600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Dt.hora rec.:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrl8: TQRLabel [7]
        Left = -21
        Top = 700
        Width = 172
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          -25.255681818181820000
          841.856060606060800000
          206.856060606060600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor frete.: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object QRLabel10: TQRLabel [8]
        Left = -21
        Top = 637
        Width = 66
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          -25.255681818181820000
          766.089015151515300000
          79.375000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Celular:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object QRLabel12: TQRLabel [9]
        Left = 202
        Top = 600
        Width = 110
        Height = 22
        Size.Values = (
          26.458333333333330000
          242.935606060606100000
          721.590909090909200000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cond.pagto:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object QRLabel14: TQRLabel [10]
        Left = -21
        Top = 679
        Width = 172
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          -25.255681818181820000
          816.600378787878800000
          206.856060606060600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total produtos: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object QRLabel15: TQRLabel [11]
        Left = -21
        Top = 722
        Width = 172
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          -25.255681818181820000
          868.314393939394000000
          206.856060606060600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor outras desp.: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object QRLabel16: TQRLabel [12]
        Left = -21
        Top = 744
        Width = 172
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          -25.255681818181820000
          894.772727272727300000
          206.856060606060600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor desconto: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object QRLabel17: TQRLabel [13]
        Left = 1
        Top = 729
        Width = 172
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          1.202651515151515000
          876.732954545454600000
          206.856060606060600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total ser pago: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object QRLabel2: TQRLabel [14]
        Left = -21
        Top = 591
        Width = 106
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          -25.255681818181820000
          710.767045454545400000
          127.481060606060600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Pedido: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object QRLabel22: TQRLabel [15]
        Left = -39
        Top = 771
        Width = 603
        Height = 26
        Enabled = False
        Size.Values = (
          31.268939393939390000
          -46.903409090909090000
          927.244318181818200000
          725.198863636363600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '------------------------- PRODUTOS ----------------------------'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRLabel4: TQRLabel [16]
        Left = 194
        Top = 426
        Width = 95
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          233.314393939394000000
          512.329545454545400000
          114.251893939393900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vendedor:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object QRLabel6: TQRLabel [17]
        Left = -21
        Top = 614
        Width = 66
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          -25.255681818181820000
          738.428030303030400000
          79.375000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cliente:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object QRLabel8: TQRLabel [18]
        Left = 310
        Top = 409
        Width = 88
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          372.821969696969700000
          491.884469696969800000
          105.833333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Telefone:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrlCelularNF: TQRLabel [19]
        Left = 66
        Top = 600
        Width = 134
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          79.375000000000000000
          721.590909090909200000
          161.155303030303000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(62) 98475-8550'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrlClienteNF: TQRLabel [20]
        Left = 45
        Top = 614
        Width = 271
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          54.119318181818180000
          738.428030303030400000
          325.918560606060600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'CLIENTE MASTER GOLD PLATINUM'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrlCondicaoPagamentoNF: TQRLabel [21]
        Left = 182
        Top = 637
        Width = 286
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          218.882575757575800000
          766.089015151515300000
          343.958333333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '15 - BOLETO 30/60/90/120/150'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      inherited qrlNFEmitidoEm: TQRLabel [22]
        Left = 175
        Top = 272
        Width = 282
        Height = 20
        Enabled = False
        Size.Values = (
          24.053030303030300000
          210.464015151515200000
          327.121212121212100000
          339.147727272727300000)
        FontSize = -6
        ExplicitLeft = 175
        ExplicitTop = 272
        ExplicitWidth = 282
        ExplicitHeight = 20
      end
      object qrlNFValorFrete: TQRLabel [23]
        Left = 152
        Top = 700
        Width = 238
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          182.803030303030300000
          841.856060606060800000
          286.231060606060600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor outras desp: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrlOrcamentoIdNF: TQRLabel [24]
        Left = 78
        Top = 591
        Width = 97
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          93.806818181818190000
          710.767045454545400000
          116.657196969697000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '146'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrlTelefoneNF: TQRLabel [25]
        Left = 248
        Top = 614
        Width = 141
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          298.257575757575800000
          738.428030303030400000
          169.573863636363600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(62) 30955-5850'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrlTotalDescontoNF: TQRLabel [26]
        Left = 152
        Top = 744
        Width = 238
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          182.803030303030300000
          894.772727272727300000
          286.231060606060600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor desconto: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrlTotalOutrasDespesasNF: TQRLabel [27]
        Left = 152
        Top = 722
        Width = 238
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          182.803030303030300000
          868.314393939394000000
          286.231060606060600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor outras desp: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrlTotalProdutosNF: TQRLabel [28]
        Left = 152
        Top = 679
        Width = 238
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          182.803030303030300000
          816.600378787878800000
          286.231060606060600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total produtos: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrlTotalSerPagoNF: TQRLabel [29]
        Left = 152
        Top = 766
        Width = 238
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          182.803030303030300000
          921.231060606060700000
          286.231060606060600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total ser pago: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrlVendedorNF: TQRLabel [30]
        Left = 175
        Top = 592
        Width = 308
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          210.464015151515200000
          711.969696969697000000
          370.416666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'VENDEDOR MASTER GOLD PLATINUM'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrNFDataCadastro: TQRLabel [31]
        Left = 66
        Top = 623
        Width = 134
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          79.375000000000000000
          749.251893939394000000
          161.155303030303000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '26/02/1991'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrNFDataHoraRecebimento: TQRLabel [32]
        Left = 227
        Top = 660
        Width = 187
        Height = 22
        Enabled = False
        Size.Values = (
          26.458333333333330000
          273.001893939394000000
          793.750000000000000000
          224.895833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '26/02/1991 15:55'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      inherited qrNFTipoDocumento: TQRLabel [33]
        Left = 18
        Top = 402
        Width = 598
        Height = 29
        Enabled = False
        Size.Values = (
          34.876893939393950000
          21.647727272727270000
          483.465909090909100000
          719.185606060606100000)
        Caption = 'Comprovante de pagamento'
        FontSize = 7
        ExplicitLeft = 18
        ExplicitTop = 402
        ExplicitWidth = 598
        ExplicitHeight = 29
      end
      inherited qrNFTitulo: TQRLabel [34]
        Left = 18
        Top = 469
        Width = 598
        Height = 29
        Enabled = False
        Size.Values = (
          34.876893939393950000
          21.647727272727270000
          564.043560606060700000
          719.185606060606100000)
        FontSize = 7
        ExplicitLeft = 18
        ExplicitTop = 469
        ExplicitWidth = 598
        ExplicitHeight = 29
      end
      inherited qrSeparador2: TQRShape [35]
        Left = 18
        Top = 264
        Width = 610
        Enabled = False
        Size.Values = (
          2.405303030303031000
          21.647727272727270000
          317.500000000000000000
          733.617424242424400000)
        ExplicitLeft = 18
        ExplicitTop = 264
        ExplicitWidth = 610
      end
      object qrl11: TQRLabel
        Left = 270
        Top = 184
        Width = 51
        Height = 25
        Size.Values = (
          30.066287878787880000
          324.715909090909100000
          221.287878787878800000
          61.335227272727280000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'UND'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrl16: TQRLabel
        Left = 487
        Top = 184
        Width = 95
        Height = 25
        Size.Values = (
          30.066287878787880000
          585.691287878787800000
          221.287878787878800000
          114.251893939393900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'TOTAL'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlCabProdutoId: TQRLabel
        Left = 206
        Top = 184
        Width = 45
        Height = 25
        Size.Values = (
          30.066287878787880000
          247.746212121212200000
          221.287878787878800000
          54.119318181818180000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'QTD'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlCodigo: TQRLabel
        Left = 5
        Top = 184
        Width = 68
        Height = 25
        Size.Values = (
          30.066287878787880000
          6.013257575757576000
          221.287878787878800000
          81.780303030303030000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'C'#211'D'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlDescricao: TQRLabel
        Left = 75
        Top = 184
        Width = 125
        Height = 25
        Size.Values = (
          30.066287878787880000
          90.198863636363640000
          221.287878787878800000
          150.331439393939400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'DESCRI'#199#195'O'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRLabel11: TQRLabel
        Left = 2
        Top = 131
        Width = 597
        Height = 23
        Size.Values = (
          27.660984848484850000
          2.405303030303031000
          157.547348484848500000
          717.982954545454500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'da Nota Fiscal do Consumidor Eletr'#244'nica'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel13: TQRLabel
        Left = 2
        Top = 153
        Width = 597
        Height = 23
        Size.Values = (
          27.660984848484850000
          2.405303030303031000
          184.005681818181800000
          717.982954545454500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'N'#227'o permite aproveitamento de cr'#233'dito de ICMS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel18: TQRLabel
        Left = 351
        Top = 184
        Width = 90
        Height = 25
        Size.Values = (
          30.066287878787880000
          422.130681818181900000
          221.287878787878800000
          108.238636363636400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'VL. UNIT'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRShape4: TQRShape
        Left = 0
        Top = 210
        Width = 599
        Height = 2
        Size.Values = (
          2.405303030303031000
          0.000000000000000000
          252.556818181818200000
          720.388257575757600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 15395562
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape3: TQRShape
        Left = 0
        Top = 183
        Width = 599
        Height = 2
        Size.Values = (
          2.405303030303031000
          0.000000000000000000
          220.085227272727300000
          720.388257575757600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 15395562
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape7: TQRShape
        Left = 1
        Top = 107
        Width = 598
        Height = 2
        Size.Values = (
          2.405303030303030000
          1.202651515151515000
          128.683712121212100000
          719.185606060606100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 15395562
        Shape = qrsRectangle
        VertAdjust = 0
      end
    end
    inherited PageFooterBand1: TQRBand
      Top = 274
      Width = 624
      Height = 815
      Size.Values = (
        980.160984848484800000
        750.454545454545500000)
      BandType = rbSummary
      ExplicitTop = 274
      ExplicitWidth = 624
      ExplicitHeight = 815
      inherited qrlNFSistema: TQRLabel
        Left = 458
        Top = 598
        Width = 157
        Height = 19
        Enabled = False
        Size.Values = (
          22.850378787878790000
          550.814393939394000000
          719.185606060606100000
          188.816287878787900000)
        Alignment = taRightJustify
        Caption = 'Hiva 2.0.0.1'
        FontSize = -6
        ExplicitLeft = 458
        ExplicitTop = 598
        ExplicitWidth = 157
        ExplicitHeight = 19
      end
      object qrl18: TQRLabel
        Left = 58
        Top = 8
        Width = 345
        Height = 25
        Size.Values = (
          30.066287878787880000
          69.753787878787880000
          9.621212121212121000
          414.914772727272700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'QTD ITENS'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRLabel1: TQRLabel
        Left = 58
        Top = 34
        Width = 345
        Height = 25
        Size.Values = (
          30.066287878787880000
          69.753787878787880000
          40.890151515151520000
          414.914772727272700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'VALOR TOTAL'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRLabel3: TQRLabel
        Left = 58
        Top = 61
        Width = 345
        Height = 25
        Size.Values = (
          30.066287878787880000
          69.753787878787880000
          73.361742424242420000
          414.914772727272700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'FORMA DE PAGAMENTO'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRLabel5: TQRLabel
        Left = 438
        Top = 61
        Width = 144
        Height = 25
        Size.Values = (
          30.066287878787880000
          526.761363636363600000
          73.361742424242420000
          173.181818181818200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'VALOR PAGO'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlFormaPagto: TQRLabel
        Left = 58
        Top = 86
        Width = 345
        Height = 25
        Size.Values = (
          30.066287878787880000
          69.753787878787880000
          103.428030303030300000
          414.914772727272700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'DINHEIRO/PIX'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlFormaPagtoValor: TQRLabel
        Left = 438
        Top = 86
        Width = 144
        Height = 25
        Size.Values = (
          30.066287878787880000
          526.761363636363600000
          103.428030303030300000
          173.181818181818200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '10,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRLabel7: TQRLabel
        Left = 58
        Top = 113
        Width = 345
        Height = 25
        Size.Values = (
          30.066287878787880000
          69.753787878787880000
          135.899621212121200000
          414.914772727272700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'TROCO'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlTroco: TQRLabel
        Left = 438
        Top = 113
        Width = 144
        Height = 25
        Size.Values = (
          30.066287878787880000
          526.761363636363600000
          135.899621212121200000
          173.181818181818200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = ''
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlqtdItens: TQRLabel
        Left = 438
        Top = 8
        Width = 144
        Height = 25
        Size.Values = (
          30.066287878787880000
          526.761363636363600000
          9.621212121212123000
          173.181818181818200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '1'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlValorTotal: TQRLabel
        Left = 438
        Top = 34
        Width = 144
        Height = 25
        Size.Values = (
          30.066287878787880000
          526.761363636363600000
          40.890151515151510000
          173.181818181818200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '10,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRLabel9: TQRLabel
        Left = 116
        Top = 268
        Width = 372
        Height = 25
        Size.Values = (
          30.066287878787880000
          139.507575757575800000
          322.310606060606100000
          447.386363636363600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Consulte pela chave de acesso em:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlChaveAcesso: TQRLabel
        Left = 5
        Top = 290
        Width = 593
        Height = 25
        Size.Values = (
          30.066287878787880000
          6.013257575757576000
          348.768939393939400000
          713.172348484848500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'sefaz.go'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrlNumeroChaveAcesso: TQRLabel
        Left = 5
        Top = 313
        Width = 593
        Height = 25
        Size.Values = (
          30.066287878787880000
          6.013257575757576000
          376.429924242424200000
          713.172348484848500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '529809000000000000000000123'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrlNomeConsumidor: TQRLabel
        Left = 5
        Top = 335
        Width = 593
        Height = 25
        Size.Values = (
          30.066287878787880000
          6.013257575757576000
          402.888257575757600000
          713.172348484848500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Consumidor final'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlDocConsumidor: TQRLabel
        Left = 5
        Top = 359
        Width = 593
        Height = 25
        Size.Values = (
          30.066287878787880000
          6.013257575757576000
          431.751893939394000000
          713.172348484848500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '500.843.920-02'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlDadosEmissao: TQRLabel
        Left = 31
        Top = 387
        Width = 567
        Height = 25
        Size.Values = (
          30.066287878787880000
          37.282196969696970000
          465.426136363636400000
          681.903409090909100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Numero 0000 Serie 000'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlProtocoloAutorizacao: TQRLabel
        Left = 31
        Top = 411
        Width = 567
        Height = 25
        Size.Values = (
          30.066287878787880000
          37.282196969696970000
          494.289772727272700000
          681.903409090909100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Protocolo'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlDataAutorizacao: TQRLabel
        Left = 31
        Top = 436
        Width = 567
        Height = 25
        Size.Values = (
          30.066287878787880000
          37.282196969696970000
          524.356060606060600000
          681.903409090909100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Data'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrIQrCode: TQRImage
        Left = 153
        Top = 470
        Width = 318
        Height = 325
        Size.Values = (
          390.861742424242400000
          184.005681818181800000
          565.246212121212200000
          382.443181818181800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
      end
      object qrlInformacoesComplementares: TQRLabel
        Left = 45
        Top = 144
        Width = 548
        Height = 105
        Size.Values = (
          126.278409090909100000
          54.119318181818180000
          173.181818181818200000
          659.053030303030300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Volte Sempre'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRShape5: TQRShape
        Left = 1
        Top = 252
        Width = 597
        Height = 2
        Size.Values = (
          2.405303030303031000
          1.202651515151515000
          303.068181818181800000
          717.982954545454500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 15395562
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape6: TQRShape
        Left = 0
        Top = 3
        Width = 608
        Height = 2
        Size.Values = (
          2.405303030303031000
          0.000000000000000000
          3.607954545454545000
          731.212121212121200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 15395562
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape8: TQRShape
        Left = 1
        Top = 140
        Width = 597
        Height = 2
        Size.Values = (
          2.405303030303031000
          1.202651515151515000
          168.371212121212100000
          717.982954545454500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 15395562
        Shape = qrsRectangle
        VertAdjust = 0
      end
    end
    object qrbndDetailBand2: TQRBand
      Left = 0
      Top = 215
      Width = 624
      Height = 59
      AlignToBottom = False
      BeforePrint = qrbndDetailBand2BeforePrint
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        70.956439393939400000
        750.454545454545500000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbDetail
      object qrlNFNomeProduto: TQRLabel
        Left = 87
        Top = 4
        Width = 519
        Height = 27
        Size.Values = (
          32.471590909090910000
          104.630681818181800000
          4.810606060606061000
          624.176136363636400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '7 - CIMENTO CIPLAN 50KG 7 - CIMENTO CIPLAN CIMENTO'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlNFQuantidade: TQRLabel
        Left = 169
        Top = 29
        Width = 82
        Height = 28
        Size.Values = (
          33.674242424242430000
          203.248106060606100000
          34.876893939393940000
          98.617424242424260000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '99.999,999'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlNFMarca: TQRLabel
        Left = 197
        Top = 202
        Width = 134
        Height = 28
        Enabled = False
        Size.Values = (
          33.674242424242430000
          236.922348484848500000
          242.935606060606100000
          161.155303030303000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '1 - AMIGO KENIM'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrl15: TQRLabel
        Left = 350
        Top = 202
        Width = 130
        Height = 26
        Enabled = False
        Size.Values = (
          31.268939393939390000
          420.928030303030400000
          242.935606060606100000
          156.344696969697000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = ''
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlNFUnidade: TQRLabel
        Left = 270
        Top = 30
        Width = 44
        Height = 28
        Size.Values = (
          33.674242424242430000
          324.715909090909100000
          36.079545454545450000
          52.916666666666660000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'CXA'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlNFPrecoUnitario: TQRLabel
        Left = 354
        Top = 28
        Width = 81
        Height = 28
        Size.Values = (
          33.674242424242430000
          425.738636363636400000
          33.674242424242430000
          97.414772727272720000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '9.999,99'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrl17: TQRLabel
        Left = 84
        Top = 224
        Width = 60
        Height = 28
        Enabled = False
        Size.Values = (
          33.674242424242430000
          101.022727272727300000
          269.393939393939400000
          72.159090909090910000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlNFValorTotal: TQRLabel
        Left = 445
        Top = 30
        Width = 137
        Height = 28
        Size.Values = (
          33.674242424242430000
          535.179924242424400000
          36.079545454545450000
          164.763257575757600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '9.999,99'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRShape1: TQRShape
        Left = 2
        Top = 172
        Width = 603
        Height = 2
        Enabled = False
        Size.Values = (
          2.405303030303031000
          2.405303030303031000
          206.856060606060600000
          725.198863636363600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 15395562
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrlCodigoValor: TQRLabel
        Left = 19
        Top = 4
        Width = 68
        Height = 27
        Size.Values = (
          32.471590909090910000
          22.850378787878790000
          4.810606060606061000
          81.780303030303030000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '123'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
    end
  end
  inherited qrRelatorioA4: TQuickRep
    Left = 990
    Top = 597
    Width = 635
    Height = 898
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    PrinterSettings.FirstPage = 1
    PrinterSettings.LastPage = 1
    PrinterSettings.UseCustomBinCode = True
    PrinterSettings.CustomBinCode = 15
    PrinterSettings.ExtendedDuplex = 1
    PrinterSettings.CustomPaperCode = 9
    PrinterSettings.PrintQuality = 600
    PrinterSettings.ColorOption = 2
    Zoom = 80
    ExplicitLeft = 990
    ExplicitTop = 597
    ExplicitWidth = 635
    ExplicitHeight = 898
    inherited qrCabecalho: TQRBand
      Left = 30
      Top = 30
      Width = 575
      Height = 69
      Size.Values = (
        228.203125000000000000
        1901.692708333333000000)
      ExplicitLeft = 30
      ExplicitTop = 30
      ExplicitWidth = 575
      ExplicitHeight = 69
      inherited qr13: TQRLabel [0]
        Left = 1
        Top = 50
        Width = 574
        Height = 16
        Size.Values = (
          52.916666666666670000
          2.645833333333333000
          164.041666666666700000
          1897.062500000000000000)
        Caption = 'Comprovante de pagamento'
        FontSize = 12
        ExplicitLeft = 1
        ExplicitTop = 50
        ExplicitWidth = 574
        ExplicitHeight = 16
      end
      inherited qrCaptionEndereco: TQRLabel [1]
        Left = 67
        Top = 14
        Width = 48
        Height = 10
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          44.979166666666670000
          158.750000000000000000)
        FontSize = 7
        ExplicitLeft = 67
        ExplicitTop = 14
        ExplicitWidth = 48
        ExplicitHeight = 10
      end
      inherited qrEmpresa: TQRLabel [2]
        Left = 117
        Top = 2
        Width = 172
        Height = 10
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          7.937500000000000000
          568.854166666666700000)
        FontSize = 7
        ExplicitLeft = 117
        ExplicitTop = 2
        ExplicitWidth = 172
        ExplicitHeight = 10
      end
      inherited qrEndereco: TQRLabel [3]
        Left = 117
        Top = 14
        Width = 458
        Height = 10
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          44.979166666666670000
          1513.416666666667000000)
        FontSize = 7
        ExplicitLeft = 117
        ExplicitTop = 14
        ExplicitWidth = 458
        ExplicitHeight = 10
      end
      inherited qrLogoEmpresa: TQRImage [4]
        Left = 4
        Width = 64
        Height = 64
        Size.Values = (
          211.666666666666700000
          13.229166666666670000
          2.645833333333333000
          211.666666666666700000)
        ExplicitLeft = 4
        ExplicitWidth = 64
        ExplicitHeight = 64
      end
      inherited qrCaptionEmpresa: TQRLabel [5]
        Left = 67
        Top = 2
        Width = 48
        Height = 10
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          7.937500000000000000
          158.750000000000000000)
        FontSize = 7
        ExplicitLeft = 67
        ExplicitTop = 2
        ExplicitWidth = 48
        ExplicitHeight = 10
      end
      inherited qrEmitidoEm: TQRLabel [6]
        Left = 462
        Width = 112
        Height = 9
        Size.Values = (
          29.104166666666670000
          1529.291666666667000000
          0.000000000000000000
          370.416666666666700000)
        FontSize = 5
        ExplicitLeft = 462
        ExplicitWidth = 112
        ExplicitHeight = 9
      end
      inherited qr1: TQRLabel [7]
        Left = 67
        Top = 25
        Width = 48
        Height = 10
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          82.020833333333320000
          158.750000000000000000)
        FontSize = 7
        ExplicitLeft = 67
        ExplicitTop = 25
        ExplicitWidth = 48
        ExplicitHeight = 10
      end
      inherited qrBairro: TQRLabel [8]
        Left = 117
        Top = 25
        Width = 172
        Height = 10
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          82.020833333333320000
          568.854166666666700000)
        FontSize = 7
        ExplicitLeft = 117
        ExplicitTop = 25
        ExplicitWidth = 172
        ExplicitHeight = 10
      end
      inherited qr3: TQRLabel [9]
        Left = 292
        Top = 25
        Width = 48
        Height = 10
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          82.020833333333320000
          158.750000000000000000)
        FontSize = 7
        ExplicitLeft = 292
        ExplicitTop = 25
        ExplicitWidth = 48
        ExplicitHeight = 10
      end
      inherited qrCidadeUf: TQRLabel [10]
        Left = 342
        Top = 25
        Width = 172
        Height = 10
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          82.020833333333320000
          568.854166666666700000)
        FontSize = 7
        ExplicitLeft = 342
        ExplicitTop = 25
        ExplicitWidth = 172
        ExplicitHeight = 10
      end
      inherited qr5: TQRLabel [11]
        Left = 292
        Top = 2
        Width = 48
        Height = 10
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          7.937500000000000000
          158.750000000000000000)
        FontSize = 7
        ExplicitLeft = 292
        ExplicitTop = 2
        ExplicitWidth = 48
        ExplicitHeight = 10
      end
      inherited qrCNPJ: TQRLabel [12]
        Left = 342
        Top = 2
        Width = 75
        Height = 10
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          7.937500000000000000
          248.708333333333300000)
        FontSize = 7
        ExplicitLeft = 342
        ExplicitTop = 2
        ExplicitWidth = 75
        ExplicitHeight = 10
      end
      inherited qr7: TQRLabel [13]
        Left = 67
        Top = 36
        Width = 48
        Height = 10
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
        ExplicitLeft = 67
        ExplicitTop = 36
        ExplicitWidth = 48
        ExplicitHeight = 10
      end
      inherited qrTelefone: TQRLabel [14]
        Left = 117
        Top = 36
        Width = 57
        Height = 10
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          119.062500000000000000
          187.854166666666700000)
        FontSize = 7
        ExplicitLeft = 117
        ExplicitTop = 36
        ExplicitWidth = 57
        ExplicitHeight = 10
      end
      inherited qrFax: TQRLabel [15]
        Left = 232
        Top = 36
        Width = 57
        Height = 10
        Size.Values = (
          34.395833333333330000
          767.291666666666800000
          119.062500000000000000
          187.854166666666700000)
        FontSize = 7
        ExplicitLeft = 232
        ExplicitTop = 36
        ExplicitWidth = 57
        ExplicitHeight = 10
      end
      inherited qr10: TQRLabel [16]
        Left = 182
        Top = 36
        Width = 48
        Height = 10
        Size.Values = (
          34.395833333333330000
          603.250000000000000000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
        ExplicitLeft = 182
        ExplicitTop = 36
        ExplicitWidth = 48
        ExplicitHeight = 10
      end
      inherited qr11: TQRLabel [17]
        Left = 292
        Top = 36
        Width = 48
        Height = 10
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
        ExplicitLeft = 292
        ExplicitTop = 36
        ExplicitWidth = 48
        ExplicitHeight = 10
      end
      inherited qrEmail: TQRLabel [18]
        Left = 342
        Top = 36
        Width = 233
        Height = 10
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          119.062500000000000000
          769.937500000000000000)
        FontSize = 7
        ExplicitLeft = 342
        ExplicitTop = 36
        ExplicitWidth = 233
        ExplicitHeight = 10
      end
    end
    inherited qrbndRodape: TQRBand
      Left = 30
      Top = 188
      Width = 575
      Height = 34
      Size.Values = (
        112.447916666666700000
        1901.692708333333000000)
      ExplicitLeft = 30
      ExplicitTop = 188
      ExplicitWidth = 575
      ExplicitHeight = 34
      inherited qrUsuarioImpressao: TQRLabel
        Left = 2
        Top = 22
        Width = 107
        Height = 10
        Size.Values = (
          31.750000000000000000
          7.937500000000000000
          74.083333333333340000
          354.541666666666700000)
        FontSize = 7
        ExplicitLeft = 2
        ExplicitTop = 22
        ExplicitWidth = 107
        ExplicitHeight = 10
      end
      inherited qrSistema: TQRLabel
        Left = 462
        Top = 22
        Width = 112
        Height = 10
        Size.Values = (
          31.750000000000000000
          1529.291666666667000000
          74.083333333333340000
          370.416666666666700000)
        FontSize = 7
        ExplicitLeft = 462
        ExplicitTop = 22
        ExplicitWidth = 112
        ExplicitHeight = 10
      end
    end
    object qrTitulo: TQRBand
      Left = 30
      Top = 99
      Width = 575
      Height = 89
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        294.348958333333300000
        1901.692708333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbTitle
      object QRShape2: TQRShape
        Left = 1
        Top = 87
        Width = 574
        Height = 1
        Size.Values = (
          2.645833333333333000
          2.645833333333333000
          288.395833333333300000
          1899.708333333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object qr2: TQRLabel
        Left = 4
        Top = 0
        Width = 44
        Height = 10
        Size.Values = (
          34.395833333333330000
          13.229166666666670000
          0.000000000000000000
          145.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Or'#231'amento: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrOrcamentoId: TQRLabel
        Left = 49
        Top = 0
        Width = 50
        Height = 10
        Size.Values = (
          34.395833333333330000
          161.395833333333300000
          0.000000000000000000
          164.041666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '146'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr8: TQRLabel
        Left = 4
        Top = 11
        Width = 44
        Height = 10
        Size.Values = (
          34.395833333333330000
          13.229166666666670000
          37.041666666666670000
          145.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cliente: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrCliente: TQRLabel
        Left = 49
        Top = 11
        Width = 126
        Height = 10
        Size.Values = (
          34.395833333333330000
          161.395833333333300000
          37.041666666666670000
          418.041666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '8 - TALITA C'#194'NDIA LIMA SILVA'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr4: TQRLabel
        Left = 178
        Top = 11
        Width = 40
        Height = 10
        Size.Values = (
          34.395833333333330000
          587.375000000000000000
          37.041666666666670000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Telefone: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrTelefoneCliente: TQRLabel
        Left = 218
        Top = 11
        Width = 54
        Height = 10
        Size.Values = (
          34.395833333333330000
          722.312500000000000000
          37.041666666666670000
          177.270833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(62) 3288-1655'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr14: TQRLabel
        Left = 273
        Top = 11
        Width = 40
        Height = 10
        Size.Values = (
          34.395833333333330000
          902.229166666666800000
          37.041666666666670000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Celular: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrCelularCliente: TQRLabel
        Left = 314
        Top = 11
        Width = 54
        Height = 10
        Size.Values = (
          34.395833333333330000
          1037.166666666667000000
          37.041666666666670000
          179.916666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(62) 9876-5432'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr9: TQRLabel
        Left = 103
        Top = 0
        Width = 48
        Height = 10
        Size.Values = (
          34.395833333333330000
          341.312500000000000000
          0.000000000000000000
          158.750000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vendedor: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrVendedor: TQRLabel
        Left = 152
        Top = 0
        Width = 126
        Height = 10
        Size.Values = (
          34.395833333333330000
          502.708333333333300000
          0.000000000000000000
          418.041666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '8 - TALITA C'#194'NDIA LIMA SILVA'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr17: TQRLabel
        Left = 279
        Top = 0
        Width = 74
        Height = 10
        Size.Values = (
          34.395833333333330000
          923.395833333333200000
          0.000000000000000000
          246.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Condi'#231#227'o pagto: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrCondicaoPagamento: TQRLabel
        Left = 354
        Top = 0
        Width = 215
        Height = 10
        Size.Values = (
          34.395833333333330000
          1172.104166666667000000
          0.000000000000000000
          711.729166666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '1 - Avista'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr28: TQRLabel
        Left = 4
        Top = 22
        Width = 76
        Height = 10
        Size.Values = (
          34.395833333333330000
          13.229166666666670000
          74.083333333333320000
          251.354166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total produtos: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr30: TQRLabel
        Left = 4
        Top = 34
        Width = 76
        Height = 10
        Size.Values = (
          34.395833333333330000
          13.229166666666670000
          111.125000000000000000
          251.354166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor outras desp.: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr31: TQRLabel
        Left = 4
        Top = 45
        Width = 76
        Height = 10
        Size.Values = (
          34.395833333333330000
          13.229166666666670000
          148.166666666666700000
          251.354166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor desconto: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr29: TQRLabel
        Left = 4
        Top = 56
        Width = 76
        Height = 10
        Size.Values = (
          34.395833333333330000
          13.229166666666670000
          185.208333333333300000
          251.354166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total ser pago: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrTotalSerPago: TQRLabel
        Left = 83
        Top = 56
        Width = 86
        Height = 10
        Size.Values = (
          34.395833333333330000
          275.166666666666700000
          185.208333333333300000
          285.750000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total ser pago: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrValorDesconto: TQRLabel
        Left = 83
        Top = 45
        Width = 86
        Height = 10
        Size.Values = (
          34.395833333333330000
          275.166666666666700000
          148.166666666666700000
          285.750000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor desconto: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrValorOutrasDespesas: TQRLabel
        Left = 83
        Top = 34
        Width = 86
        Height = 10
        Size.Values = (
          34.395833333333330000
          275.166666666666700000
          111.125000000000000000
          285.750000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor outras desp: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrTotalProdutos: TQRLabel
        Left = 83
        Top = 22
        Width = 86
        Height = 10
        Size.Values = (
          34.395833333333330000
          275.166666666666700000
          74.083333333333320000
          285.750000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total produtos: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr32: TQRLabel
        Left = 1
        Top = 70
        Width = 574
        Height = 16
        Size.Values = (
          52.916666666666670000
          2.645833333333333000
          232.833333333333300000
          1897.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Formas de pagamento'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
    end
  end
  object qrJuntar: TQRCompositeReport
    OnAddReports = qrJuntarAddReports
    Options = []
    PrinterSettings.Copies = 1
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.UseStandardprinter = False
    PrinterSettings.UseCustomBinCode = False
    PrinterSettings.CustomBinCode = 0
    PrinterSettings.ExtendedDuplex = 0
    PrinterSettings.UseCustomPaperCode = False
    PrinterSettings.CustomPaperCode = 0
    PrinterSettings.PrintMetaFile = False
    PrinterSettings.MemoryLimit = 1000000
    PrinterSettings.PrintQuality = 0
    PrinterSettings.Collate = 0
    PrinterSettings.ColorOption = 0
    PrinterSettings.Orientation = poPortrait
    PrinterSettings.PaperSize = Letter
    ReportTitle = 'Comprovante de pagamento'
    PageCount = 0
    Left = 16
    Top = 200
  end
end
