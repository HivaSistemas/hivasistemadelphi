unit BuscarProdutosZerarEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Grids, GridLuka,
  Vcl.Buttons, Vcl.ExtCtrls, FrameProdutos, _FrameHerancaPrincipal,
  _FrameHenrancaPesquisas, FrameLocais, FrameDataInicialFinal, _AjustesEstoque, _Biblioteca,
  Vcl.StdCtrls;

type
  TFormBuscarProdutosZerarEstoque = class(TFormHerancaFinalizar)
    sgProdutos: TGridLuka;
    FrLocalProduto: TFrLocais;
    FrProduto: TFrProdutos;
    sbZerarEstoque: TSpeedButton;
    FrDataAjuste: TFrDataInicialFinal;
    Label1: TLabel;
    procedure sbZerarEstoqueClick(Sender: TObject);
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgProdutosGetCellPicture(Sender: TObject; ARow, ACol: Integer;
      var APicture: TPicture);
    procedure sgProdutosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormBuscarProdutosZerarEstoque: TFormBuscarProdutosZerarEstoque;

function BuscarDadosProdutos: TRetornoTelaFinalizar<RecProdutosSelecionados>;

implementation

uses _Imagens, _Sessao;

const
  coSelecionado       = 0;
  coProduto           = 1;
  coNomeProduto       = 2;
  coUnidade           = 3;
  coMarca             = 4;
  coPrecoUnitario     = 5;
  coLocal             = 6;
  coQuantidade        = 7;
  coLote              = 8;
  coFabricacao        = 9;
  coVencimento        = 10;

  //Colunas ocultas
  coMarcaId           = 11;
  coMarcaNome         = 12;
  coLocalId           = 13;
  coLocalNome         = 14;

{$R *.dfm}

function BuscarDadosProdutos: TRetornoTelaFinalizar<RecProdutosSelecionados>;
var
  vForm: TFormBuscarProdutosZerarEstoque;
  i: Integer;
  produtosSelecionados: TArray<RecProdutosZerarEstoque>;
begin
  vForm := TFormBuscarProdutosZerarEstoque.Create(Application);

  if Result.Ok(vForm.ShowModal) then begin
    for i := vForm.sgProdutos.FixedRows to vForm.sgProdutos.RowCount -1 do begin
      if vForm.sgProdutos.Cells[coSelecionado, i] = charNaoSelecionado then
        Continue;

      SetLength(produtosSelecionados, Length(produtosSelecionados) + 1);
      produtosSelecionados[High(produtosSelecionados)].ProdutoId     := SFormatInt(vForm.sgProdutos.Cells[coProduto, i]);
      produtosSelecionados[High(produtosSelecionados)].ProdutoNome   := vForm.sgProdutos.Cells[coNomeProduto, i];
      produtosSelecionados[High(produtosSelecionados)].Unidade       := vForm.sgProdutos.Cells[coUnidade, i];
      produtosSelecionados[High(produtosSelecionados)].MarcaId       := SFormatInt(vForm.sgProdutos.Cells[coMarcaId, i]);
      produtosSelecionados[High(produtosSelecionados)].MarcaNome     := vForm.sgProdutos.Cells[coMarcaNome, i];
      produtosSelecionados[High(produtosSelecionados)].PrecoUnitario := SFormatDouble(vForm.sgProdutos.Cells[coPrecoUnitario, i]);
      produtosSelecionados[High(produtosSelecionados)].LocalId       := SFormatInt(vForm.sgProdutos.Cells[coLocalId, i]);
      produtosSelecionados[High(produtosSelecionados)].LocalNome     := vForm.sgProdutos.Cells[coLocalNome, i];
      produtosSelecionados[High(produtosSelecionados)].Quantidade    := SFormatDouble(vForm.sgProdutos.Cells[coQuantidade, i]);
      produtosSelecionados[High(produtosSelecionados)].Lote          := vForm.sgProdutos.Cells[coLote, i];
      produtosSelecionados[High(produtosSelecionados)].DataFabricacao := ToData(vForm.sgProdutos.Cells[coFabricacao, i]);
      produtosSelecionados[High(produtosSelecionados)].DataVencimento := ToData(vForm.sgProdutos.Cells[coVencimento, i]);
    end;

    Result.Dados.Itens := produtosSelecionados;
  end;

  FreeAndNil(vForm);
end;

procedure TFormBuscarProdutosZerarEstoque.sbZerarEstoqueClick(Sender: TObject);
var
  produtos: TArray<RecProdutosZerarEstoque>;
  ProdutosTodos: TArray<RecProdutosZerarEstoque>;
  ProdutosAjuste: TArray<RecProdutosZerarEstoque>;
  sqlAjuste: string;
  sqlProdutos: string;
  i: Integer;
  j: Integer;
  adicionarProduto: boolean;
begin
  inherited;

  if not FrDataAjuste.DatasValidas then begin
    Informar('� necess�rio informar um per�odo de ajuste de estoque v�lido');
    SetarFoco(FrDataAjuste);
    Abort;
  end;

  sqlProdutos := ' and DIV.EMPRESA_ID = ' + IntToStr(Sessao.getEmpresaLogada.EmpresaId);
  sqlAjuste := ' and AES.EMPRESA_ID = ' + IntToStr(Sessao.getEmpresaLogada.EmpresaId);

  if not FrProduto.EstaVazio then begin
    sqlProdutos := sqlProdutos + ' and ' + FrProduto.getSqlFiltros('PRO.PRODUTO_ID');
    sqlAjuste := sqlAjuste + ' and ' + FrProduto.getSqlFiltros('AEI.PRODUTO_ID');
  end;

  if not FrLocalProduto.EstaVazio then begin
    sqlProdutos := sqlProdutos + ' and ' + FrLocalProduto.getSqlFiltros('DIV.LOCAL_ID');
    sqlAjuste := sqlAjuste + ' and ' + FrLocalProduto.getSqlFiltros('AEI.LOCAL_ID');
  end;

  sqlAjuste := sqlAjuste + ' and ' + FrDataAjuste.getSqlFiltros('AES.DATA_HORA_AJUSTE');

  ProdutosTodos := _AjustesEstoque.BuscarTodosProdutos(Sessao.getConexaoBanco, sqlProdutos, Sessao.getEmpresaLogada.EmpresaId);
  ProdutosAjuste := _AjustesEstoque.BuscarProdutosComAjuste(Sessao.getConexaoBanco, sqlAjuste);

  for i := Low(ProdutosTodos) to High(ProdutosTodos) do begin
    if ProdutosTodos[i].PrecoUnitario = 0 then
      Continue;

    adicionarProduto := true;
    for j := Low(ProdutosAjuste) to High(ProdutosAjuste) do begin
      if
        (ProdutosAjuste[j].ProdutoId = ProdutosTodos[i].ProdutoId) and
        (ProdutosAjuste[j].LocalId = ProdutosTodos[i].LocalId) and
        (ProdutosAjuste[j].Lote = ProdutosTodos[i].Lote)
      then begin
        adicionarProduto := false;
        Break;
      end;
    end;

    if adicionarProduto then begin
      SetLength(produtos, Length(produtos) + 1);
      produtos[High(produtos)] := ProdutosTodos[i];
    end;
  end;

  if produtos = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  for i := Low(produtos) to High(produtos) do begin
    sgProdutos.Cells[coSelecionado, i + 1]   := charNaoSelecionado;
    sgProdutos.Cells[coProduto, i + 1]       := NFormat(produtos[i].ProdutoId);
    sgProdutos.Cells[coNomeProduto, i + 1]   := produtos[i].ProdutoNome;
    sgProdutos.Cells[coUnidade, i + 1]       := produtos[i].Unidade;
    sgProdutos.Cells[coMarca, i + 1]         := NFormatN(produtos[i].MarcaId) + ' - ' + produtos[i].MarcaNome;
    sgProdutos.Cells[coPrecoUnitario, i + 1] := NFormatN(produtos[i].PrecoUnitario);
    sgProdutos.Cells[coLocal, i + 1]         := NFormatN(produtos[i].LocalId) + ' - ' + produtos[i].LocalNome;
    sgProdutos.Cells[coQuantidade, i + 1]    := NFormatN(produtos[i].Quantidade);
    sgProdutos.Cells[coLote, i + 1]          := produtos[i].Lote;
    sgProdutos.Cells[coFabricacao, i + 1]    := DFormat(produtos[i].DataFabricacao);
    sgProdutos.Cells[coVencimento, i + 1]    := DFormat(produtos[i].DataVencimento);

    sgProdutos.Cells[coLocalId, i + 1]       := NFormat(produtos[i].LocalId);
    sgProdutos.Cells[coLocalNome, i + 1]     := produtos[i].LocalNome;
    sgProdutos.Cells[coMarcaId, i + 1]       := NFormat(produtos[i].MarcaId);
    sgProdutos.Cells[coMarcaNome, i + 1]     := produtos[i].MarcaNome;
  end;
  sgProdutos.SetLinhasGridPorTamanhoVetor( Length(produtos) );
end;

procedure TFormBuscarProdutosZerarEstoque.sgProdutosDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coPrecoUnitario, coQuantidade] then
    vAlinhamento := taRightJustify
  else if ACol in[coSelecionado] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormBuscarProdutosZerarEstoque.sgProdutosGetCellPicture(Sender: TObject; ARow,
  ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if (ACol <> coSelecionado) or (sgProdutos.Cells[ACol, ARow] = '') then
   Exit;

  if sgProdutos.Cells[ACol, ARow] = charNaoSelecionado then
    APicture := _Imagens.FormImagens.im1.Picture
  else
    APicture := _Imagens.FormImagens.im2.Picture;
end;

procedure TFormBuscarProdutosZerarEstoque.sgProdutosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  MarcarDesmarcarTodos(sgProdutos, coSelecionado, Key ,Shift);
end;

end.
