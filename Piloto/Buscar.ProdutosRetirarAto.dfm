inherited FormBuscarProdutosRetirarAto: TFormBuscarProdutosRetirarAto
  Caption = 'Buscar produtos para retirar no ato'
  ClientHeight = 438
  ClientWidth = 894
  OnShow = FormShow
  ExplicitWidth = 900
  ExplicitHeight = 467
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 401
    Width = 894
    ExplicitTop = 401
    ExplicitWidth = 894
  end
  object pnSuperior: TPanel
    Left = 0
    Top = 0
    Width = 894
    Height = 208
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object sgProdutos: TGridLuka
      Left = 0
      Top = 0
      Width = 894
      Height = 208
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      Align = alClient
      ColCount = 11
      DefaultRowHeight = 19
      DrawingStyle = gdsGradient
      FixedColor = 15395562
      FixedCols = 0
      RowCount = 2
      Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goEditing]
      TabOrder = 0
      OnClick = sgProdutosClick
      OnDrawCell = sgProdutosDrawCell
      OnKeyPress = NumerosVirgula
      OnSelectCell = sgProdutosSelectCell
      IncrementExpandCol = 0
      IncrementExpandRow = 0
      CorLinhaFoco = 38619
      CorFundoFoco = 16774625
      CorLinhaDesfoque = 14869218
      CorFundoDesfoque = 16382457
      CorSuperiorCabecalho = clWhite
      CorInferiorCabecalho = 13553358
      CorSeparadorLinhas = 12040119
      CorColunaFoco = clMenuHighlight
      HCol.Strings = (
        'Produto'
        'Nome do produto'
        'Marca'
        'Und.'
        'Quantidade'
        'Qtde. ret. ato'
        'Qtde. retirar'
        'Qtde. entregar'
        'Qtde. sem prev.'
        'Estoque'
        'Lotes def.?')
      OnGetCellColor = sgProdutosGetCellColor
      OnGetCellPicture = sgProdutosGetCellPicture
      OnArrumarGrid = sgProdutosArrumarGrid
      Grid3D = False
      RealColCount = 11
      Indicador = True
      AtivarPopUpSelecao = False
      ColWidths = (
        52
        169
        101
        33
        77
        83
        77
        89
        91
        89
        67)
    end
  end
  object pnMeio: TPanel
    Left = 0
    Top = 208
    Width = 894
    Height = 133
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object sgLocais: TGridLuka
      Left = 0
      Top = 0
      Width = 893
      Height = 133
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      ColCount = 8
      DefaultRowHeight = 19
      DrawingStyle = gdsGradient
      FixedColor = 15395562
      FixedCols = 0
      RowCount = 2
      Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goEditing]
      TabOrder = 0
      OnDrawCell = sgLocaisDrawCell
      OnKeyPress = NumerosVirgula
      OnSelectCell = sgLocaisSelectCell
      IncrementExpandCol = 0
      IncrementExpandRow = 0
      CorLinhaFoco = 38619
      CorFundoFoco = 16774625
      CorLinhaDesfoque = 14869218
      CorFundoDesfoque = 16382457
      CorSuperiorCabecalho = clWhite
      CorInferiorCabecalho = 13553358
      CorSeparadorLinhas = 12040119
      CorColunaFoco = clMenuHighlight
      HCol.Strings = (
        'Empresa'
        'Nome da empresa'
        'Local'
        'Nome do local'
        'Dispon'#237'vel'
        'Qtde. ret. ato'
        'Qtde. retirar'
        'Qtde. entregar')
      OnGetCellColor = sgLocaisGetCellColor
      OnArrumarGrid = sgLocaisArrumarGrid
      Grid3D = False
      RealColCount = 11
      Indicador = True
      AtivarPopUpSelecao = False
      ColWidths = (
        55
        185
        46
        213
        75
        88
        86
        93)
    end
  end
  object pnInferior: TPanel
    Left = 0
    Top = 341
    Width = 894
    Height = 60
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object lb12: TLabel
      Left = 3
      Top = 10
      Width = 107
      Height = 14
      Caption = 'Data / hora entrega'
    end
    object eDataEntrega: TEditLukaData
      Left = 3
      Top = 30
      Width = 72
      Height = 22
      Hint = 
        'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
        'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
      EditMask = '99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 0
      Text = '  /  /    '
    end
    object eHoraEntrega: TEditHoras
      Left = 74
      Top = 30
      Width = 41
      Height = 22
      EditMask = '!90:00;1;_'
      MaxLength = 5
      TabOrder = 1
      Text = '  :  '
    end
    object sbInformacoesEstoque: TPanel
      Left = 761
      Top = 30
      Width = 132
      Height = 22
      Cursor = crHandPoint
      Hint = 'Definir lote do produto focado'
      BevelOuter = bvNone
      Caption = 'Definir lotes'
      Color = 38619
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentBackground = False
      ParentFont = False
      TabOrder = 2
      OnClick = sbInformacoesEstoqueClick
    end
  end
end
