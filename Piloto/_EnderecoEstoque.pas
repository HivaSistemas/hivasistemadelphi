unit _EnderecoEstoque;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _RecordsCadastros;

{$M+}
type
  TEnderecoEstoque = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordEnderecoEstoque: RecEnderecosEstoque;
  end;

function AtualizarEnderecoEstoque(
  pConexao: TConexao;
  pEnderecoEstoqueId: Integer;
  pRuaId: Integer;
  pModuloId: Integer;
  pNivelId: Integer;
  pVaoId: Integer
): RecRetornoBD;

function BuscarEnderecoEstoque(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEnderecosEstoque>;

function ExcluirEnderecoEstoque(
  pConexao: TConexao;
  pEnderecoEstoqueId: Integer
): RecRetornoBD;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TEnderecoEstoque }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 6);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Endere�o',
      True,
      0,
      'and EDE.ENDERECO_ID = :P1 ',
      ' order by RUA.DESCRICAO, MOD.DESCRICAO, NIV.DESCRICAO, VAO.DESCRICAO '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      1,
      'and EDE.RUA_ID = :P1 and EDE.MODULO_ID = :P2 and EDE.NIVEL_ID = :P3 and EDE.VAO_ID = :P4 '
    );

  Result[2] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Rua',
      True,
      2,
      'and EDE.RUA_ID like ''%'' || :P1 || ''%'' ',
      ' order by RUA.DESCRICAO, MOD.DESCRICAO, NIV.DESCRICAO, VAO.DESCRICAO '
    );

  Result[3] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'M�dulo',
      True,
      3,
      'and EDE.MODULO_ID like ''%'' || :P1 || ''%'' ',
      ' order by RUA.DESCRICAO, MOD.DESCRICAO, NIV.DESCRICAO, VAO.DESCRICAO '
    );

  Result[4] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'N�vel',
      True,
      4,
      'and EDE.NIVEL_ID like ''%'' || :P1 || ''%'' ',
      ' order by RUA.DESCRICAO, MOD.DESCRICAO, NIV.DESCRICAO, VAO.DESCRICAO '
    );

  Result[5] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'V�o',
      True,
      5,
      'and EDE.VAO like ''%'' || :P1 || ''%'' ',
      ' order by RUA.DESCRICAO, MOD.DESCRICAO, NIV.DESCRICAO, VAO.DESCRICAO '
    );
end;

constructor TEnderecoEstoque.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ENDERECO_ESTOQUE');

  FSql :=
    'select ' +
    '  EDE.ENDERECO_ID, ' +
    '  EDE.RUA_ID, ' +
    '  EDE.MODULO_ID, ' +
    '  EDE.NIVEL_ID, ' +
    '  EDE.VAO_ID, ' +
    '  RUA.DESCRICAO AS NOME_RUA, ' +
    '  MOD.DESCRICAO AS NOME_MODULO, ' +
    '  NIV.DESCRICAO AS NOME_NIVEL, ' +
    '  VAO.DESCRICAO AS NOME_VAO ' +
    'from ' +
    '  ENDERECO_ESTOQUE EDE ' +

    'inner join ENDERECO_EST_RUA RUA ' +
    'on RUA.RUA_ID = EDE.RUA_ID ' +

    'inner join ENDERECO_EST_MODULO MOD ' +
    'on MOD.MODULO_ID = EDE.MODULO_ID ' +

    'inner join ENDERECO_EST_NIVEL NIV ' +
    'on NIV.NIVEL_ID = EDE.NIVEL_ID ' +

    'inner join ENDERECO_EST_VAO VAO ' +
    'on VAO.VAO_ID = EDE.VAO_ID ';

  SetFiltros(GetFiltros);

  AddColuna('ENDERECO_ID', True);
  AddColuna('RUA_ID', False);
  AddColuna('MODULO_ID', False);
  AddColuna('NIVEL_ID', False);
  AddColuna('VAO_ID', False);
  AddColunaSL('NOME_RUA');
  AddColunaSL('NOME_MODULO');
  AddColunaSL('NOME_NIVEL');
  AddColunaSL('NOME_VAO');
end;

function TEnderecoEstoque.GetRecordEnderecoEstoque: RecEnderecosEstoque;
begin
  Result := RecEnderecosEstoque.Create;
  Result.endereco_id                := GetInt('ENDERECO_ID', True);
  Result.rua_id                     := GetInt('RUA_ID');
  Result.modulo_id                  := GetInt('MODULO_ID');
  Result.nivel_id                   := GetInt('NIVEL_ID');
  Result.vao_id                     := GetInt('VAO_ID');
  Result.nome_rua                   := GetString('NOME_RUA');
  Result.nome_modulo                := GetString('NOME_MODULO');
  Result.nome_nivel                 := GetString('NOME_NIVEL');
  Result.nome_vao                   := GetString('NOME_VAO');
end;

function AtualizarEnderecoEstoque(
  pConexao: TConexao;
  pEnderecoEstoqueId: Integer;
  pRuaId: Integer;
  pModuloId: Integer;
  pNivelId: Integer;
  pVaoId: Integer
): RecRetornoBD;
var
  t: TEnderecoEstoque;
  novo: Boolean;
  seq: TSequencia;
  consultaExistencia: TConsulta;
begin
  Result.Iniciar;
  pConexao.SetRotina('ATUALIZAR_ENDERECO_ESTOQUE');

  t := TEnderecoEstoque.Create(pConexao);
  consultaExistencia := TConsulta.Create(pConexao);

  novo := pEnderecoEstoqueId = 0;
  if novo then begin
    try
      consultaExistencia.SQL.Text := 'select count(*) from ENDERECO_ESTOQUE where RUA_ID = :P1 and MODULO_ID = :P2 and NIVEL_ID = :P3 and VAO_ID = :P4';
      consultaExistencia.Pesquisar([pRuaId, pModuloId, pNivelId, pVaoId]);
      if consultaExistencia.GetInt(0) > 0 then begin
        Result.TeveErro := True;
        Result.MensagemErro := 'O Endere�o de Estoque informado j� existe!';
        t.Free;
        Exit;
      end;
    finally
      consultaExistencia.Free;
    end;

    seq := TSequencia.Create(pConexao, 'SEQ_END_ESTOQUE_ID');
    pEnderecoEstoqueId := seq.GetProximaSequencia;
    result.AsInt := pEnderecoEstoqueId;
    seq.Free;
  end;

  t.SetInt('ENDERECO_ID', pEnderecoEstoqueId, True);
  t.SetInt('RUA_ID', pRuaId);
  t.SetInt('MODULO_ID', pModuloId);
  t.SetInt('NIVEL_ID', pNivelId);
  t.SetInt('VAO_ID', pVaoId);

  try
    pConexao.IniciarTransacao;

    if novo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarEnderecoEstoque(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEnderecosEstoque>;
var
  i: Integer;
  t: TEnderecoEstoque;
begin
  Result := nil;
  t := TEnderecoEstoque.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordEnderecoEstoque;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirEnderecoEstoque(
  pConexao: TConexao;
  pEnderecoEstoqueId: Integer
): RecRetornoBD;
var
  t: TEnderecoEstoque;
begin
  Result.TeveErro := False;
  t := TEnderecoEstoque.Create(pConexao);

  t.SetInt('ENDERECO_ID', pEnderecoEstoqueId, True);

  try
    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
