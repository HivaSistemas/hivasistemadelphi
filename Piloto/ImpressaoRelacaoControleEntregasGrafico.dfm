inherited FormImpressaoRelacaoControleEntregasGrafico: TFormImpressaoRelacaoControleEntregasGrafico
  Caption = 'FormImpressaoRelacaoControleEntregasGrafico'
  PixelsPerInch = 96
  TextHeight = 13
  inherited qrRelatorioA4: TQuickRep
    Left = -48
    Top = 38
    BeforePrint = qrRelatorioA4BeforePrint
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    OnNeedData = qrRelatorioA4NeedData
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    ExplicitLeft = -48
    ExplicitTop = 38
    inherited qrbndCabecalho: TQRBand
      Size.Values = (
        102.053571428571400000
        1899.330357142857000000)
      inherited qrVersaoSistema: TQRLabel
        Size.Values = (
          34.017857142857150000
          9.449404761904763000
          5.669642857142857000
          733.273809523809500000)
        FontSize = 7
      end
      inherited qrEmitidoEm: TQRLabel
        Size.Values = (
          29.104166666666670000
          1529.291666666667000000
          0.000000000000000000
          370.416666666666700000)
        FontSize = 6
      end
      inherited qr13: TQRLabel
        Left = 104
        Width = 753
        Size.Values = (
          52.916666666666680000
          196.547619047619100000
          39.687500000000000000
          1423.080357142857000000)
        Caption = 
          'Rela'#231#227'o de controles de entregas sint'#233'tico - Agrupado por motori' +
          'sta'
        FontSize = 10
        ExplicitLeft = 104
        ExplicitWidth = 753
      end
      inherited qrUsuarioImpressao: TQRLabel
        Size.Values = (
          34.017857142857150000
          803.199404761904600000
          5.669642857142857000
          527.276785714285700000)
        FontSize = 7
      end
      inherited QRSysData1: TQRSysData
        Size.Values = (
          39.687500000000000000
          1785.937500000000000000
          26.458333333333340000
          52.916666666666680000)
        FontSize = 7
      end
      inherited qr2: TQRLabel
        Size.Values = (
          39.687500000000000000
          1674.434523809524000000
          26.458333333333340000
          105.833333333333400000)
        FontSize = 7
      end
      inherited qrQtdePaginas: TQRLabel
        Size.Values = (
          39.687500000000000000
          1838.854166666667000000
          26.458333333333340000
          62.366071428571420000)
        FontSize = 7
      end
    end
    inherited qrbndRodape: TQRBand
      Top = 150
      Size.Values = (
        130.401785714285700000
        1899.330357142857000000)
      ExplicitTop = 150
      inherited qr1: TQRLabel
        Size.Values = (
          41.892361111111120000
          8.819444444444444000
          8.819444444444444000
          901.788194444444500000)
        FontSize = 8
      end
      inherited qrFiltrosUtilizados: TQRPMemo
        Size.Values = (
          43.467261904761910000
          9.449404761904763000
          60.476190476190480000
          1880.431547619048000000)
        FontSize = 8
      end
    end
    object qrbndColumnHeaderBand1: TQRBand
      Left = 53
      Top = 107
      Width = 1005
      Height = 22
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        41.577380952380950000
        1899.330357142857000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbColumnHeader
      object qr8: TQRLabel
        Left = 5
        Top = 1
        Width = 340
        Height = 18
        Size.Values = (
          34.017857142857150000
          9.449404761904763000
          1.889880952380953000
          642.559523809523800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Motorista'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr18: TQRLabel
        Left = 349
        Top = 1
        Width = 103
        Height = 18
        Size.Values = (
          34.017857142857150000
          659.568452380952400000
          1.889880952380953000
          194.657738095238100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Qtde.entregas'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr19: TQRLabel
        Left = 456
        Top = 1
        Width = 89
        Height = 18
        Size.Values = (
          34.017857142857150000
          861.785714285714200000
          1.889880952380953000
          168.199404761904800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Qtde. itens'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr3: TQRLabel
        Left = 549
        Top = 1
        Width = 119
        Height = 18
        Size.Values = (
          34.017857142857150000
          1037.544642857143000000
          1.889880952380953000
          224.895833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vlr.total entregas'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr4: TQRLabel
        Left = 672
        Top = 1
        Width = 119
        Height = 18
        Size.Values = (
          34.017857142857150000
          1270.000000000000000000
          1.889880952380953000
          224.895833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Peso total'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
    end
    object qrbndDetailBand1: TQRBand
      Left = 53
      Top = 129
      Width = 1005
      Height = 21
      AlignToBottom = False
      BeforePrint = qrbndDetailBand1BeforePrint
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        39.687500000000000000
        1899.330357142857000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbDetail
      object qrMotorista: TQRLabel
        Left = 5
        Top = 1
        Width = 340
        Height = 18
        Size.Values = (
          34.017857142857150000
          9.449404761904763000
          1.889880952380953000
          642.559523809523800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Motorista'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrQtdeEntregas: TQRLabel
        Left = 349
        Top = 1
        Width = 103
        Height = 18
        Size.Values = (
          34.017857142857150000
          659.568452380952400000
          1.889880952380953000
          194.657738095238100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Qtde.entregas'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrQtdeItens: TQRLabel
        Left = 456
        Top = 1
        Width = 89
        Height = 18
        Size.Values = (
          34.017857142857150000
          861.785714285714200000
          1.889880952380953000
          168.199404761904800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Qtde. itens'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrValorTotalEntregas: TQRLabel
        Left = 549
        Top = 1
        Width = 119
        Height = 18
        Size.Values = (
          34.017857142857150000
          1037.544642857143000000
          1.889880952380953000
          224.895833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vlr.total entregas'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrPesoTotal: TQRLabel
        Left = 672
        Top = 1
        Width = 119
        Height = 18
        Size.Values = (
          34.017857142857150000
          1270.000000000000000000
          1.889880952380953000
          224.895833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Peso total'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
    end
  end
end
