inherited FormCadastroDesenvolvimento: TFormCadastroDesenvolvimento
  Caption = 'Cadastro de solicita'#231#245'es e desenvolvimentos'
  ClientHeight = 477
  ClientWidth = 872
  ExplicitWidth = 878
  ExplicitHeight = 506
  PixelsPerInch = 96
  TextHeight = 14
  object Label3: TLabel [0]
    Left = 424
    Top = 6
    Width = 57
    Height = 14
    Caption = 'Prioridade'
  end
  object Label4: TLabel [1]
    Left = 748
    Top = 5
    Width = 76
    Height = 14
    Caption = 'Data cadastro'
  end
  object lbl1: TLabel [3]
    Left = 211
    Top = 5
    Width = 104
    Height = 14
    Caption = 'Tipo de Solicita'#231#227'o'
  end
  object lbl3: TLabel [4]
    Left = 126
    Top = 101
    Width = 83
    Height = 14
    Caption = 'Cadastrado por'
  end
  object lbl4: TLabel [5]
    Left = 312
    Top = 101
    Width = 77
    Height = 14
    Caption = 'Analisado por'
  end
  object lbl5: TLabel [6]
    Left = 494
    Top = 101
    Width = 64
    Height = 14
    Caption = 'Testado por'
  end
  object lbl6: TLabel [7]
    Left = 678
    Top = 101
    Width = 116
    Height = 14
    Caption = 'Liberado/Negado por'
  end
  inherited pnOpcoes: TPanel
    Height = 477
    ExplicitHeight = 477
    inherited sbGravar: TSpeedButton
      Left = 0
      Top = 0
      Width = 118
      Height = 40
      Align = alTop
      ExplicitLeft = 0
      ExplicitTop = -1
      ExplicitWidth = 118
      ExplicitHeight = 40
    end
    inherited sbDesfazer: TSpeedButton
      Left = 0
      Top = 89
      Width = 118
      Height = 41
      Align = alCustom
      ExplicitLeft = 0
      ExplicitTop = 89
      ExplicitWidth = 118
      ExplicitHeight = 41
    end
    inherited sbExcluir: TSpeedButton
      Left = 0
      Top = 133
      Width = 118
      Height = 46
      Align = alCustom
      ExplicitLeft = 0
      ExplicitTop = 133
      ExplicitWidth = 118
      ExplicitHeight = 46
    end
    inherited sbPesquisar: TSpeedButton
      Left = 0
      Top = 45
      Width = 118
      Height = 41
      Align = alCustom
      ExplicitLeft = 0
      ExplicitTop = 45
      ExplicitWidth = 118
      ExplicitHeight = 41
    end
    inherited sbLogs: TSpeedButton
      Left = 0
      Top = 185
      Width = 118
      Height = 45
      Align = alCustom
      ExplicitLeft = 0
      ExplicitTop = 185
      ExplicitWidth = 118
      ExplicitHeight = 45
    end
    object reAnalise: TRichEdit
      Left = 5
      Top = 235
      Width = 105
      Height = 31
      Lines.Strings = (
        '')
      TabOrder = 0
      Visible = False
      Zoom = 100
    end
    object reDesenvolvimento: TRichEdit
      Left = 5
      Top = 272
      Width = 105
      Height = 31
      Lines.Strings = (
        '')
      TabOrder = 1
      Visible = False
      Zoom = 100
    end
    object reTeste: TRichEdit
      Left = 5
      Top = 309
      Width = 105
      Height = 31
      Lines.Strings = (
        '')
      TabOrder = 2
      Visible = False
      Zoom = 100
    end
    object reDocumentacao: TRichEdit
      Left = 5
      Top = 346
      Width = 105
      Height = 31
      Lines.Strings = (
        '')
      TabOrder = 3
      Visible = False
      Zoom = 100
    end
    object reLiberacao: TRichEdit
      Left = 4
      Top = 383
      Width = 105
      Height = 31
      Lines.Strings = (
        '')
      TabOrder = 4
      Visible = False
      Zoom = 100
    end
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 818
    Top = 98
    TabOrder = 12
    Visible = False
    ExplicitLeft = 818
    ExplicitTop = 98
  end
  object cbPrioridade: TComboBoxLuka
    Left = 424
    Top = 19
    Width = 156
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = 16645629
    TabOrder = 3
    OnKeyDown = ProximoCampo
    Items.Strings = (
      'Urgente'
      'Alta'
      'M'#233'dia'
      'Baixa')
    Valores.Strings = (
      'U'
      'A'
      'M'
      'B')
    AsInt = 0
  end
  object eDataCadastro: TEditLukaData
    Left = 748
    Top = 19
    Width = 105
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    Enabled = False
    EditMask = '!99/99/0000;1;_'
    MaxLength = 10
    TabOrder = 7
    Text = '  /  /    '
  end
  object cbTipoSolicitacao: TComboBoxLuka
    Left = 211
    Top = 19
    Width = 207
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = 16645629
    TabOrder = 2
    OnKeyDown = ProximoCampo
    Items.Strings = (
      'Corre'#231#227'o de Erro'
      'Novo Desenvolvimento'
      'Gerar Documenta'#231#227'o'
      'Analise de Procedimento'
      'Ordem de Servi'#231'o')
    Valores.Strings = (
      'CE'
      'ND'
      'GD'
      'AP'
      'OS')
    AsInt = 0
  end
  object stctxtlkcadastrado: TStaticTextLuka
    Left = 126
    Top = 115
    Width = 183
    Height = 23
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 8
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object stctxtlk2: TStaticTextLuka
    Left = 310
    Top = 115
    Width = 183
    Height = 23
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Aguardando An'#225'lise'
    Color = clSilver
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 9
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object stctxtlk3: TStaticTextLuka
    Left = 494
    Top = 115
    Width = 183
    Height = 23
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Aguardando'
    Color = clSilver
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 10
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object stctxtlk4: TStaticTextLuka
    Left = 678
    Top = 115
    Width = 183
    Height = 23
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Aguardando'
    Color = clSilver
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 11
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  inline frLeitura: TFrEdicaoTexto
    Left = 126
    Top = 328
    Width = 746
    Height = 147
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 13
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 328
    ExplicitWidth = 746
    ExplicitHeight = 147
    inherited pnFerramentas: TPanel
      Width = 746
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 746
      inherited cbFonte: TComboBox
        Height = 22
        ExplicitHeight = 22
      end
      inherited cbTamanho: TComboBox
        Height = 22
        ExplicitHeight = 22
      end
    end
    inherited rtTexto: TRichEdit
      Width = 735
      Height = 95
      ExplicitLeft = 10
      ExplicitWidth = 735
      ExplicitHeight = 95
    end
    inherited stbStatus: TStatusBar
      Top = 128
      Width = 746
      ExplicitLeft = 0
      ExplicitTop = 128
      ExplicitWidth = 746
    end
  end
  inline FrEdicaoTexto: TFrEdicaoTexto
    Left = 126
    Top = 144
    Width = 746
    Height = 180
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 6
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 144
    ExplicitWidth = 746
    ExplicitHeight = 180
    inherited pnFerramentas: TPanel
      Width = 746
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 746
      inherited cbFonte: TComboBox
        Height = 22
        ExplicitHeight = 22
      end
      inherited cbTamanho: TComboBox
        Height = 22
        ExplicitHeight = 22
      end
    end
    inherited rtTexto: TRichEdit
      Width = 735
      Height = 128
      ExplicitLeft = 10
      ExplicitWidth = 735
      ExplicitHeight = 128
    end
    inherited stbStatus: TStatusBar
      Top = 161
      Width = 746
      ExplicitLeft = 0
      ExplicitTop = 161
      ExplicitWidth = 746
    end
  end
  inline FrClientes: TFrClientes
    Left = 126
    Top = 53
    Width = 324
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 4
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 53
    ExplicitWidth = 324
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 299
      Height = 24
      ExplicitWidth = 299
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 324
      ExplicitWidth = 324
      inherited lbNomePesquisa: TLabel
        Width = 45
        ExplicitWidth = 45
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 219
        ExplicitLeft = 219
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 299
      Height = 25
      ExplicitLeft = 299
      ExplicitHeight = 25
    end
  end
  inline FrFuncionarios: TFrFuncionarios
    Left = 466
    Top = 54
    Width = 324
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 5
    TabStop = True
    ExplicitLeft = 466
    ExplicitTop = 54
    ExplicitWidth = 324
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 299
      Height = 24
      ExplicitWidth = 299
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 324
      ExplicitWidth = 324
      inherited lbNomePesquisa: TLabel
        Width = 49
        Caption = 'Usu'#225'rios'
        ExplicitWidth = 49
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 219
        ExplicitLeft = 219
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 299
      Height = 25
      ExplicitLeft = 299
      ExplicitHeight = 25
    end
    inherited ckSomenteAtivos: TCheckBox
      Left = 192
      ExplicitLeft = 192
    end
    inherited ckBuscarSomenteAtivos: TCheckBox
      Checked = False
      State = cbUnchecked
    end
  end
end
