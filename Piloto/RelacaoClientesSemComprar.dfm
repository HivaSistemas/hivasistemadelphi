inherited FormRelacaoClienteSemComprar: TFormRelacaoClienteSemComprar
  Caption = 'Rela'#231#227'o de clientes sem comprar'
  ExplicitLeft = -24
  PixelsPerInch = 96
  TextHeight = 14
  object Label2: TLabel [0]
    Left = 609
    Top = 124
    Width = 73
    Height = 14
    Margins.Left = 0
    Margins.Top = 1
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alCustom
    Caption = 'Agrupamento'
  end
  inherited pnOpcoes: TPanel
    inherited sbImprimir: TSpeedButton
      Visible = False
    end
    object SpeedButton1: TSpeedButton [2]
      Left = 4
      Top = 270
      Width = 110
      Height = 40
      BiDiMode = bdLeftToRight
      Caption = 'Gerar Planilha'
      Flat = True
      NumGlyphs = 2
      ParentBiDiMode = False
      OnClick = sbImprimirClick
    end
  end
  inherited pcDados: TPageControl
    ActivePage = tsResultado
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object Label3: TLabel
        Left = 470
        Top = 3
        Width = 73
        Height = 14
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alCustom
        Caption = 'Agrupamento'
      end
      inline FrEmpresas: TFrEmpresas
        Left = 0
        Top = 2
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitTop = 2
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 53
            ExplicitWidth = 53
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
      object gbStatus: TGroupBoxLuka
        Left = 3
        Top = 182
        Width = 182
        Height = 59
        Caption = 'Cliente sem comprar a mais de'
        TabOrder = 1
        OpcaoMarcarDesmarcar = True
        object Label1: TLabel
          Left = 56
          Top = 32
          Width = 24
          Height = 14
          Caption = 'dias'
        end
        object eQtdeDias: TEditLuka
          Left = 8
          Top = 24
          Width = 42
          Height = 22
          Alignment = taRightJustify
          CharCase = ecUpperCase
          TabOrder = 0
          Text = '30'
          TipoCampo = tcNumerico
          ApenasPositivo = True
          AsInt = 30
          AsDouble = 30.000000000000000000
          AsCurr = 30.000000000000000000
          CasasDecimais = 0
          NaoAceitarEspaco = False
        end
      end
      object ckClientesNuncaCompraram: TCheckBoxLuka
        Left = 3
        Top = 247
        Width = 193
        Height = 17
        Caption = 'Clientes que nunca compraram'
        TabOrder = 2
        CheckedStr = 'N'
      end
      object rgSituacao: TRadioGroupLuka
        Left = 191
        Top = 182
        Width = 185
        Height = 105
        Caption = 'Situa'#231#227'o'
        ItemIndex = 0
        Items.Strings = (
          'Todos'
          'Ativos'
          'Inativos')
        TabOrder = 3
        OnClick = rgSituacaoClick
        Valores.Strings = (
          'T'
          'S'
          'N')
      end
      inline FrVendedores: TFrVendedores
        Left = 0
        Top = 92
        Width = 404
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 4
        TabStop = True
        ExplicitTop = 92
        ExplicitWidth = 404
        inherited sgPesquisa: TGridLuka
          Width = 379
          ExplicitWidth = 379
        end
        inherited PnTitulos: TPanel
          Width = 404
          ExplicitWidth = 404
          inherited lbNomePesquisa: TLabel
            Width = 94
            Caption = 'Vendedor padr'#227'o'
            ExplicitWidth = 94
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Left = 120
            Checked = False
            State = cbUnchecked
            ExplicitLeft = 120
          end
          inherited pnSuprimir: TPanel
            Left = 299
            ExplicitLeft = 299
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 379
          ExplicitLeft = 379
        end
      end
      object cbAgrupamento: TComboBoxLuka
        Left = 470
        Top = 20
        Width = 183
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 5
        Text = 'Nenhum'
        Items.Strings = (
          'Nenhum'
          'Vendedor padr'#227'o')
        Valores.Strings = (
          'NEN'
          'VEN')
        AsInt = 0
        AsString = 'NEN'
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object sgClientes: TGridLuka
        Left = 0
        Top = 0
        Width = 884
        Height = 518
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 10
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
        PopupMenu = sgClientes.pmsgClientes
        TabOrder = 0
        OnDrawCell = sgClientesDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Cliente'
          'Nome'
          'Data ult. compra'
          'Qtde. dias'
          'Valor ult. compra'
          'Vendedor padr'#227'o'
          'Telefone'
          'Celular'
          'Email'
          'Ativo')
        OnGetCellColor = sgClientesGetCellColor
        Grid3D = False
        RealColCount = 10
        OrdenarOnClick = True
        Indicador = True
        AtivarPopUpSelecao = True
        ColWidths = (
          55
          275
          107
          75
          104
          175
          94
          81
          177
          49)
      end
    end
  end
end
