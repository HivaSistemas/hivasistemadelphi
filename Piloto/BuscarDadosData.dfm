inherited FormBuscarData: TFormBuscarData
  Caption = 'Buscar data'
  ClientHeight = 88
  ClientWidth = 192
  OnShow = FormShow
  ExplicitWidth = 198
  ExplicitHeight = 117
  PixelsPerInch = 96
  TextHeight = 14
  object lbData: TLabel [0]
    Left = 24
    Top = 4
    Width = 76
    Height = 14
    Caption = 'Data cadastro'
  end
  inherited pnOpcoes: TPanel
    Top = 51
    Width = 192
    TabOrder = 1
    ExplicitTop = 51
    ExplicitWidth = 192
  end
  object eData: TEditLukaData
    Left = 24
    Top = 18
    Width = 121
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    TabStop = False
    EditMask = '99/99/9999;1; '
    MaxLength = 10
    TabOrder = 0
    Text = '  /  /    '
  end
end
