inherited FormImpressaoPedidoCompraGrafico: TFormImpressaoPedidoCompraGrafico
  Caption = 'FormImpressaoPedidoCompraGrafico'
  ClientWidth = 1101
  ExplicitTop = -82
  ExplicitWidth = 1117
  ExplicitHeight = 754
  PixelsPerInch = 96
  TextHeight = 13
  inherited qrRelatorioNF: TQuickRep
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Page.Values = (
      0.000000000000000000
      509.300000000000000000
      0.000000000000000000
      750.100000000000000000
      0.000000000000000000
      0.000000000000000000
      0.000000000000000000)
    inherited qrBandTitulo: TQRBand
      Size.Values = (
        289.388020833333300000
        750.755208333333300000)
      inherited qrlNFNomeEmpresa: TQRLabel
        Size.Values = (
          34.726562500000000000
          6.614583333333332000
          16.536458333333330000
          744.140625000000000000)
        FontSize = 6
      end
      inherited qrNFCnpj: TQRLabel
        Top = 31
        Size.Values = (
          33.072916666666670000
          6.614583333333332000
          51.263020833333340000
          744.140625000000000000)
        FontSize = 7
        ExplicitTop = 31
      end
      inherited qrNFEndereco: TQRLabel
        Size.Values = (
          34.726562500000000000
          6.614583333333332000
          85.989583333333340000
          744.140625000000000000)
        FontSize = 7
      end
      inherited qrNFCidadeUf: TQRLabel
        Size.Values = (
          42.994791666666670000
          6.614583333333332000
          122.369791666666700000
          744.140625000000000000)
        FontSize = 7
      end
      inherited qrNFTipoDocumento: TQRLabel
        Size.Values = (
          33.072916666666670000
          1.653645833333333000
          211.666666666666700000
          719.335937500000000000)
        FontSize = 7
      end
      inherited qrSeparador2: TQRShape
        Size.Values = (
          3.307291666666666000
          3.307291666666666000
          281.119791666666700000
          724.296875000000000000)
      end
      inherited qrNFTitulo: TQRLabel
        Size.Values = (
          33.072916666666670000
          1.653645833333333000
          246.393229166666700000
          719.335937500000000000)
        FontSize = 7
      end
      inherited qrlNFEmitidoEm: TQRLabel
        Size.Values = (
          23.151041666666670000
          370.416666666666700000
          3.307291666666666000
          357.187500000000000000)
        FontSize = -6
      end
      inherited qrlNFTelefoneEmpresa: TQRLabel
        Top = 101
        Size.Values = (
          42.994791666666670000
          6.614583333333332000
          167.018229166666700000
          744.140625000000000000)
        FontSize = 7
        ExplicitTop = 101
      end
    end
    inherited PageFooterBand1: TQRBand
      Size.Values = (
        24.804687500000000000
        750.755208333333300000)
      inherited qrlNFSistema: TQRLabel
        Size.Values = (
          23.151041666666670000
          582.083333333333400000
          0.000000000000000000
          138.906250000000000000)
        Caption = 'Hiva 2.0.0.1'
        FontSize = -6
      end
    end
  end
  inherited qrRelatorioA4: TQuickRep
    Left = 81
    Width = 952
    Height = 1347
    BeforePrint = qrRelatorioA4BeforePrint
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    OnNeedData = qrRelatorioA4NeedData
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    Zoom = 120
    ExplicitLeft = 81
    ExplicitWidth = 952
    ExplicitHeight = 1347
    inherited qrCabecalho: TQRBand
      Left = 45
      Top = 45
      Width = 861
      Height = 107
      Size.Values = (
        235.920138888888900000
        1898.385416666667000000)
      ExplicitLeft = 45
      ExplicitTop = 45
      ExplicitWidth = 861
      ExplicitHeight = 107
      inherited qrCaptionEndereco: TQRLabel
        Left = 101
        Top = 20
        Width = 65
        Height = 17
        Size.Values = (
          37.482638888888890000
          222.690972222222200000
          44.097222222222220000
          143.315972222222200000)
        Alignment = taLeftJustify
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 101
        ExplicitTop = 20
        ExplicitWidth = 65
        ExplicitHeight = 17
      end
      inherited qrEmpresa: TQRLabel
        Left = 167
        Top = 4
        Width = 270
        Height = 17
        Size.Values = (
          37.482638888888890000
          368.211805555555600000
          8.819444444444444000
          595.312500000000000000)
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        FontSize = 9
        ExplicitLeft = 167
        ExplicitTop = 4
        ExplicitWidth = 270
        ExplicitHeight = 17
      end
      inherited qrEndereco: TQRLabel
        Left = 167
        Top = 20
        Width = 270
        Height = 17
        Size.Values = (
          37.482638888888890000
          368.211805555555600000
          44.097222222222220000
          595.312500000000000000)
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 167
        ExplicitTop = 20
        ExplicitWidth = 270
        ExplicitHeight = 17
      end
      inherited qrLogoEmpresa: TQRImage
        Left = 6
        Width = 96
        Height = 96
        Size.Values = (
          211.666666666666700000
          13.229166666666670000
          2.645833333333333000
          211.666666666666700000)
        ExplicitLeft = 6
        ExplicitWidth = 96
        ExplicitHeight = 96
      end
      inherited qrCaptionEmpresa: TQRLabel
        Left = 101
        Top = 4
        Width = 65
        Height = 17
        Size.Values = (
          37.482638888888890000
          222.690972222222200000
          8.819444444444444000
          143.315972222222200000)
        Alignment = taLeftJustify
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 101
        ExplicitTop = 4
        ExplicitWidth = 65
        ExplicitHeight = 17
      end
      inherited qrEmitidoEm: TQRLabel
        Left = 694
        Width = 168
        Height = 13
        Size.Values = (
          29.104166666666670000
          1529.291666666667000000
          0.000000000000000000
          370.416666666666700000)
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        FontSize = 5
        ExplicitLeft = 694
        ExplicitWidth = 168
        ExplicitHeight = 13
      end
      inherited qr1: TQRLabel
        Left = 438
        Top = 21
        Width = 53
        Height = 17
        Size.Values = (
          37.482638888888890000
          965.729166666666700000
          46.302083333333330000
          116.857638888888900000)
        Alignment = taLeftJustify
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 438
        ExplicitTop = 21
        ExplicitWidth = 53
        ExplicitHeight = 17
      end
      inherited qrBairro: TQRLabel
        Left = 489
        Top = 21
        Width = 368
        Height = 17
        Size.Values = (
          37.482638888888890000
          1078.177083333333000000
          46.302083333333330000
          811.388888888888900000)
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 489
        ExplicitTop = 21
        ExplicitWidth = 368
        ExplicitHeight = 17
      end
      inherited qr3: TQRLabel
        Left = 438
        Top = 37
        Width = 53
        Height = 17
        Size.Values = (
          37.482638888888890000
          965.729166666666700000
          81.579861111111110000
          116.857638888888900000)
        Alignment = taLeftJustify
        Caption = 'Cid./UF: '
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 438
        ExplicitTop = 37
        ExplicitWidth = 53
        ExplicitHeight = 17
      end
      inherited qrCidadeUf: TQRLabel
        Left = 490
        Top = 37
        Width = 368
        Height = 17
        Size.Values = (
          37.482638888888890000
          1080.381944444444000000
          81.579861111111110000
          811.388888888888900000)
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 490
        ExplicitTop = 37
        ExplicitWidth = 368
        ExplicitHeight = 17
      end
      inherited qr5: TQRLabel
        Left = 438
        Top = 4
        Width = 53
        Height = 17
        Size.Values = (
          37.482638888888890000
          965.729166666666700000
          8.819444444444444000
          116.857638888888900000)
        Alignment = taLeftJustify
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 438
        ExplicitTop = 4
        ExplicitWidth = 53
        ExplicitHeight = 17
      end
      inherited qrCNPJ: TQRLabel
        Left = 490
        Top = 4
        Width = 151
        Height = 17
        Size.Values = (
          37.482638888888890000
          1080.381944444444000000
          8.819444444444444000
          332.934027777777800000)
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 490
        ExplicitTop = 4
        ExplicitWidth = 151
        ExplicitHeight = 17
      end
      inherited qr7: TQRLabel
        Left = 101
        Top = 37
        Width = 65
        Height = 17
        Size.Values = (
          37.482638888888890000
          222.690972222222200000
          81.579861111111110000
          143.315972222222200000)
        Alignment = taLeftJustify
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 101
        ExplicitTop = 37
        ExplicitWidth = 65
        ExplicitHeight = 17
      end
      inherited qrTelefone: TQRLabel
        Left = 167
        Top = 37
        Width = 138
        Height = 17
        Size.Values = (
          37.482638888888890000
          368.211805555555600000
          81.579861111111110000
          304.270833333333300000)
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 167
        ExplicitTop = 37
        ExplicitWidth = 138
        ExplicitHeight = 17
      end
      inherited qrFax: TQRLabel
        Left = 167
        Top = 54
        Width = 138
        Height = 17
        Size.Values = (
          37.482638888888890000
          368.211805555555600000
          119.062500000000000000
          304.270833333333300000)
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 167
        ExplicitTop = 54
        ExplicitWidth = 138
        ExplicitHeight = 17
      end
      inherited qr10: TQRLabel
        Left = 101
        Top = 54
        Width = 65
        Height = 17
        Size.Values = (
          37.482638888888890000
          222.690972222222200000
          119.062500000000000000
          143.315972222222200000)
        Alignment = taLeftJustify
        Caption = 'Celular:'
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 101
        ExplicitTop = 54
        ExplicitWidth = 65
        ExplicitHeight = 17
      end
      inherited qr11: TQRLabel
        Left = 438
        Top = 54
        Width = 53
        Height = 17
        Size.Values = (
          37.482638888888890000
          965.729166666666700000
          119.062500000000000000
          116.857638888888900000)
        Alignment = taLeftJustify
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 438
        ExplicitTop = 54
        ExplicitWidth = 53
        ExplicitHeight = 17
      end
      inherited qrEmail: TQRLabel
        Left = 490
        Top = 54
        Width = 368
        Height = 17
        Size.Values = (
          37.482638888888890000
          1080.381944444444000000
          119.062500000000000000
          811.388888888888900000)
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 490
        ExplicitTop = 54
        ExplicitWidth = 368
        ExplicitHeight = 17
      end
      inherited qr13: TQRLabel
        Left = 323
        Top = 74
        Width = 240
        Height = 28
        Size.Values = (
          61.736111111111110000
          712.170138888888900000
          163.159722222222200000
          529.166666666666700000)
        Caption = 'PEDIDO DE COMPRA'
        Font.Charset = ANSI_CHARSET
        Font.Height = -20
        Font.Name = 'Cambria'
        FontSize = 15
        ExplicitLeft = 323
        ExplicitTop = 74
        ExplicitWidth = 240
        ExplicitHeight = 28
      end
      object QRShape7: TQRShape
        Left = -1
        Top = 100
        Width = 862
        Height = 2
        Size.Values = (
          4.409722222222222000
          -2.204861111111111000
          220.486111111111100000
          1900.590277777778000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape1: TQRShape
        Left = -1
        Top = 99
        Width = 862
        Height = 2
        Size.Values = (
          4.409722222222222000
          -2.204861111111111000
          218.281250000000000000
          1900.590277777778000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
    end
    inherited qrbndRodape: TQRBand
      Left = 45
      Top = 194
      Width = 861
      Height = 83
      Size.Values = (
        183.003472222222200000
        1898.385416666667000000)
      ExplicitLeft = 45
      ExplicitTop = 194
      ExplicitWidth = 861
      ExplicitHeight = 83
      inherited qrUsuarioImpressao: TQRLabel
        Left = 4
        Top = 67
        Width = 161
        Height = 14
        Size.Values = (
          30.868055555555560000
          8.819444444444444000
          147.725694444444400000
          354.982638888888900000)
        Font.Height = -8
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        FontSize = 6
        ExplicitLeft = 4
        ExplicitTop = 67
        ExplicitWidth = 161
        ExplicitHeight = 14
      end
      inherited qrSistema: TQRLabel
        Left = 694
        Top = 71
        Width = 168
        Height = 14
        Size.Values = (
          30.868055555555560000
          1530.173611111111000000
          156.545138888888900000
          370.416666666666700000)
        Caption = 'Hiva 3.0'
        Font.Height = -8
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        FontSize = 6
        ExplicitLeft = 694
        ExplicitTop = 71
        ExplicitWidth = 168
        ExplicitHeight = 14
      end
      object qr2: TQRLabel
        Left = 4
        Top = 3
        Width = 84
        Height = 16
        Size.Values = (
          35.277777777777780000
          8.819444444444444000
          6.614583333333333000
          185.208333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'C'#243'd.pedido:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrCompraId: TQRLabel
        Left = 88
        Top = 3
        Width = 76
        Height = 16
        Size.Values = (
          35.277777777777780000
          194.027777777777800000
          6.614583333333333000
          167.569444444444400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '25.500'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr8: TQRLabel
        Left = 538
        Top = 3
        Width = 74
        Height = 16
        Size.Values = (
          35.277777777777780000
          1186.215277777778000000
          6.614583333333333000
          163.159722222222200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Comprador:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrComprador: TQRLabel
        Left = 612
        Top = 3
        Width = 246
        Height = 16
        Size.Values = (
          35.277777777777780000
          1349.375000000000000000
          6.614583333333333000
          542.395833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'MARCELO FERREIRA SANTOS'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr12: TQRLabel
        Left = 4
        Top = 19
        Width = 84
        Height = 16
        Size.Values = (
          35.277777777777780000
          8.819444444444444000
          41.892361111111110000
          185.208333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Data fatur.:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrDataFaturamento: TQRLabel
        Left = 88
        Top = 20
        Width = 76
        Height = 16
        Size.Values = (
          35.277777777777780000
          194.027777777777800000
          44.097222222222220000
          167.569444444444400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '01/09/2018'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr15: TQRLabel
        Left = 170
        Top = 20
        Width = 95
        Height = 16
        Size.Values = (
          35.277777777777780000
          374.826388888888900000
          44.097222222222220000
          209.461805555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Prev.entrega:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrPrevisaoChegada: TQRLabel
        Left = 264
        Top = 20
        Width = 80
        Height = 16
        Size.Values = (
          35.277777777777780000
          582.083333333333300000
          44.097222222222220000
          176.388888888888900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '01/09/2018'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr17: TQRLabel
        Left = 170
        Top = 3
        Width = 95
        Height = 16
        Size.Values = (
          35.277777777777780000
          374.826388888888900000
          6.614583333333333000
          209.461805555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Qtde.produtos:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrQtdeProdutos: TQRLabel
        Left = 264
        Top = 3
        Width = 80
        Height = 16
        Size.Values = (
          35.277777777777780000
          582.083333333333300000
          6.614583333333333000
          176.388888888888900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '25.500'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrValorTotal: TQRLabel
        Left = 429
        Top = 3
        Width = 104
        Height = 16
        Size.Values = (
          35.277777777777780000
          945.885416666666700000
          6.614583333333333000
          229.305555555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '25.500'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr20: TQRLabel
        Left = 344
        Top = 3
        Width = 85
        Height = 16
        Size.Values = (
          35.277777777777780000
          758.472222222222300000
          6.614583333333332000
          187.413194444444400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor total:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr4: TQRLabel
        Left = 5
        Top = 35
        Width = 83
        Height = 16
        Size.Values = (
          35.277777777777780000
          11.024305555555560000
          77.170138888888890000
          183.003472222222200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Observa'#231#245'es:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrObservacoes: TQRMemo
        Left = 88
        Top = 35
        Width = 778
        Height = 30
        Size.Values = (
          66.145833333333330000
          194.027777777777800000
          77.170138888888890000
          1715.381944444444000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 8
      end
      object qr9: TQRLabel
        Left = 344
        Top = 20
        Width = 85
        Height = 16
        Size.Values = (
          35.277777777777780000
          758.472222222222200000
          44.097222222222220000
          187.413194444444400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Fornecedor:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrlFornecedor: TQRLabel
        Left = 429
        Top = 20
        Width = 387
        Height = 16
        Size.Values = (
          35.277777777777780000
          945.885416666666700000
          44.097222222222220000
          853.281250000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'MARCELO FERREIRA SANTOS'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
    end
    object qrbndColumnHeaderBand1: TQRBand
      Left = 45
      Top = 152
      Width = 861
      Height = 19
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        41.892361111111110000
        1898.385416666667000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbColumnHeader
      object qr23: TQRLabel
        Left = 4
        Top = 2
        Width = 52
        Height = 17
        Size.Values = (
          37.482638888888890000
          8.819444444444444000
          4.409722222222222000
          114.652777777777800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Produto'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr24: TQRLabel
        Left = 58
        Top = 2
        Width = 320
        Height = 17
        Size.Values = (
          37.482638888888890000
          127.881944444444400000
          4.409722222222222000
          705.555555555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nome'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr25: TQRLabel
        Left = 490
        Top = 2
        Width = 78
        Height = 17
        Size.Values = (
          37.482638888888890000
          1080.381944444444000000
          4.409722222222222000
          171.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Marca'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr6: TQRLabel
        Left = 574
        Top = 2
        Width = 69
        Height = 17
        Size.Values = (
          37.482638888888890000
          1265.590277777778000000
          4.409722222222222000
          152.135416666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Pre'#231'o unit.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr26: TQRLabel
        Left = 645
        Top = 2
        Width = 45
        Height = 17
        Size.Values = (
          37.482638888888890000
          1422.135416666667000000
          4.409722222222222000
          99.218750000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Qtde.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr28: TQRLabel
        Left = 750
        Top = 2
        Width = 109
        Height = 17
        Size.Values = (
          37.482638888888890000
          1653.645833333333000000
          4.409722222222222000
          240.329861111111100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total do produto'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr27: TQRLabel
        Left = 691
        Top = 2
        Width = 58
        Height = 17
        Size.Values = (
          37.482638888888890000
          1523.559027777778000000
          4.409722222222222000
          127.881944444444400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Und.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel1: TQRLabel
        Left = 384
        Top = 2
        Width = 102
        Height = 17
        Size.Values = (
          37.482638888888890000
          846.666666666666700000
          4.409722222222222000
          224.895833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'C'#243'd. Original'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
    end
    object qrbndDetailBand1: TQRBand
      Left = 45
      Top = 171
      Width = 861
      Height = 23
      AlignToBottom = False
      BeforePrint = qrbndDetailBand1BeforePrint
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        50.711805555555560000
        1898.385416666667000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbDetail
      object qrProdutoId: TQRLabel
        Left = 4
        Top = 2
        Width = 52
        Height = 16
        Size.Values = (
          35.277777777777780000
          8.819444444444444000
          4.409722222222222000
          114.652777777777800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        BiDiMode = bdRightToLeft
        ParentBiDiMode = False
        Caption = '123.456'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrNomeProduto: TQRLabel
        Left = 58
        Top = 2
        Width = 320
        Height = 16
        Size.Values = (
          35.277777777777780000
          127.881944444444400000
          4.409722222222222000
          705.555555555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nome'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrMarca: TQRLabel
        Left = 490
        Top = 2
        Width = 81
        Height = 16
        Size.Values = (
          35.277777777777780000
          1080.381944444444000000
          4.409722222222222000
          178.593750000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Marca'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrPrecoUnitario: TQRLabel
        Left = 574
        Top = 2
        Width = 67
        Height = 16
        Size.Values = (
          35.277777777777780000
          1265.590277777778000000
          4.409722222222222000
          147.725694444444400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Pre'#231'o unit.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrQtde: TQRLabel
        Left = 645
        Top = 2
        Width = 45
        Height = 16
        Size.Values = (
          35.277777777777780000
          1422.135416666667000000
          4.409722222222222000
          99.218750000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Qtde.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrUnidade: TQRLabel
        Left = 691
        Top = 2
        Width = 58
        Height = 16
        Size.Values = (
          35.277777777777780000
          1523.559027777778000000
          4.409722222222222000
          127.881944444444400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Und.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrValorTotalProduto: TQRLabel
        Left = 750
        Top = 2
        Width = 109
        Height = 16
        Size.Values = (
          35.277777777777780000
          1653.645833333333000000
          4.409722222222222000
          240.329861111111100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total do produto'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRShape2: TQRShape
        Left = -1
        Top = 18
        Width = 862
        Height = 2
        Size.Values = (
          4.409722222222222000
          -2.204861111111111000
          39.687500000000000000
          1900.590277777778000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object qrCodigoOriginal: TQRLabel
        Left = 384
        Top = 2
        Width = 102
        Height = 16
        Size.Values = (
          35.277777777777780000
          846.666666666666700000
          4.409722222222222000
          224.895833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cod. original'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
    end
  end
end
