inherited FormRelacaoMovimentacoesBancosCaixas: TFormRelacaoMovimentacoesBancosCaixas
  Caption = 'Rela'#231#227'o de movimenta'#231#245'es de contas'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    inherited sbImprimir: TSpeedButton
      Top = 60
      ExplicitTop = 60
    end
    object miConciliarMovimentosSelecionados: TSpeedButton [2]
      Left = 1
      Top = 125
      Width = 117
      Height = 35
      Caption = '&Conciliar'
      Flat = True
      OnClick = miConciliarMovimentosSelecionadosClick
    end
    object miDesconciliarMovimentosSelecionados: TSpeedButton [3]
      Left = 1
      Top = 179
      Width = 117
      Height = 31
      Caption = '&Desconciliar'
      Flat = True
      OnClick = miDesconciliarMovimentosSelecionadosClick
    end
    inherited ckOmitirFiltros: TCheckBoxLuka
      Top = 500
      ExplicitTop = 500
    end
  end
  inherited pcDados: TPageControl
    ActivePage = tsResultado
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object lb1: TLabel
        Left = 380
        Top = 54
        Width = 67
        Height = 14
        Caption = 'Movimentos'
      end
      inline FrDataInicialFinal: TFrDataInicialFinal
        Left = 378
        Top = 5
        Width = 217
        Height = 42
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 378
        ExplicitTop = 5
        ExplicitWidth = 217
        ExplicitHeight = 42
        inherited Label1: TLabel
          Width = 107
          Height = 14
          Caption = 'Data do movimento'
          ExplicitWidth = 107
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrConta: TFrContas
        Left = 0
        Top = 4
        Width = 368
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitTop = 4
        ExplicitWidth = 368
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 343
          Height = 23
          ExplicitWidth = 343
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 368
          ExplicitWidth = 368
          inherited lbNomePesquisa: TLabel
            Width = 31
            Caption = 'Conta'
            ExplicitWidth = 31
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 263
            ExplicitLeft = 263
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 343
          Height = 24
          ExplicitLeft = 343
          ExplicitHeight = 24
          inherited sbPesquisa: TSpeedButton
            Glyph.Data = {
              36180000424D3618000000000000360000002800000040000000200000000100
              18000000000000180000600F0000600F00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC9
              E9F726A5E00497DB53B8E6F0F9FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFC7
              C7C7C0C0C1D7D7D7FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9EFF914
              9DDD0095DA0095DA0095DA4DB5E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F2F3C2C2C3BF
              BFBFBFBFBFBFBFBFD7D7D8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8F5FC21A3DF00
              95DA0095DA0095DA0095DA0095DAECF7FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7C5C5C5BFBFBFBF
              BFBFBFBFBFBFBFBFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3FAFD32AAE10095DA00
              95DA0095DA0095DA0095DA149DDDFAFDFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFAC8C8C9BFBFBFBFBFBFBF
              BFBFBFBFBFBFBFBFC8C8C8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9FDFE47B3E40095DA0095DA00
              95DA0095DA0095DA0999DBB8E1F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFDCDCDCDBFBFBFBFBFBFBFBFBFBF
              BFBFBFBFBFC3C3C3F1F1F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFF5FBCE80095DA0095DA0095DA00
              95DA0095DA169EDDCBE9F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFED2D2D3BFBFBFBFBFBFBFBFBFBFBFBFBF
              BFBFC7C7C7F5F5F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFF3F45F808F1734420413
              19010609031118132F3C537787E4EAED7AC8EC0095DA0095DA0095DA0095DA00
              95DA27A5E0DEF1FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFDE5E5E5D2D2D2C8C8C9C3C3
              C3C7C7C7D2D2D2E6E6E6FCFCFDD9D9D9BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFCC
              CCCCF9F9F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF75929F030F150000000000000000
              00000000000000000000000000010A0F0058800095DA0095DA0095DA0095DA3C
              AEE3EDF8FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8E8E9C5C5C6BFBFBFBFBFBFBFBFBFBFBF
              BFBFBFBFBFBFBFBFBFBFC3C3C3BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFD2D2D2FC
              FCFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4969780000000000000000000000000000
              00000000000000000000000000000000000000003D590094D90095DA57B9E7F8
              FCFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFDEDEDFBFBFBFBFBFBFBFBFBFC5C5C6D6D6D6DBDB
              DBD6D6D6C5C5C6BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFD9D9D9FEFEFEFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF6F8C9A000000000000000000020304606162AFAF
              AFCBCBCBB5B5B56A6A6A05070800000000000000000000507576C6EBFEFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFE7E7E7BFBFBFBFBFBFC3C3C3E7E7E7FEFEFEFFFFFFFFFF
              FFFFFFFFFEFEFEE6E6E6C2C2C3BFBFBFBFBFBFBFBFBFE1E1E1FFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFE8EDEF020C1100000000000015191BCFCFCFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFDDDDDD232627000000000000000507CEDBE1FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFBFBFBC3C3C4BFBFBFC3C3C4F5F5F5FFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFF4F4F4C3C3C3BFBFBFC3C3C4FCFCFCFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFF577785000000000000030405D2D2D2FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFE0E0E00B0D0E000000000000385B6CFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFE2E2E2BFBFBFBFBFBFE9E9E9FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFE7E7E7BFBFBFBFBFBFE3E3E3FFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFF122A35000000000000656667FFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7E7E7E000000000000061820FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFCDCDCEBFBFBFC7C7C8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEC5C5C6BFBFBFCFCFCFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFF020B0F000000000000B5B5B5FFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCECECE000000000000000000F3F6F7FF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFC3C3C3BFBFBFD8D8D8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D6D6BFBFBFC3C3C4FFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFF000000000000000000CFCFCFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8E8E8000000000000000000D6DEE2FF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFBFBFBFBFBFBFDEDEDEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDBDBDBBFBFBFBFBFBFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFF020A0D000000000000B8B8B8FFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD1D1D1000000000000000000F6F7F7FF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFC3C3C3BFBFBFD7D7D8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D6D6BFBFBFC5C5C5FFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFF112834000000000000696B6BFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF82828300000000000006161DFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFCDCDCEBFBFBFC7C7C7FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEC5C5C5BFBFBFCFCFD0FFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFF547280000000000000040607D8D8D8FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFE6E6E60C1011000000000000375968FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFE2E2E2BFBFBFBFBFBFE7E7E7FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E5BFBFBFBFBFBFE4E4E4FFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFE6EAEC020A0E0000000000001A1F22D8D8D8FFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFE4E4E4272B2E000000000000000405CCD7DCFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFBFBFBC4C4C4BFBFBFC2C2C3F2F2F3FFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFF1F1F1C2C2C2BFBFBFC5C5C5FCFCFCFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF6B8692000000000000000000040607696B6DBCBC
              BCD7D7D7C1C1C1757677080C0D0000000000000000004B6A79FFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFE7E7E7BFBFBFBFBFBFC2C2C2E4E4E4FEFEFEFFFFFFFFFF
              FFFFFFFFFDFDFDE3E3E3C1C1C1BFBFBFBFBFBFE9E9E9FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF44606D0000000000000000000000000000
              00000000000000000000000000000000000000294654FDFDFDFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFDFDFDFBFBFBFBFBFBFBFBFBFC3C3C3D2D2D3D7D7
              D7D2D2D2C3C3C3BFBFBFBFBFBFBFBFBFE1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6C8793020B0F0000000000000000
              0000000000000000000000000001070A567482FEFEFEFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAEAEAC5C5C6BFBFBFBFBFBFBFBFBFBFBF
              BFBFBFBFBFBFBFBFBFBFC7C7C7EBEBEBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9EDEE597684132A35030B
              0F000000020A0D10262F4D6C7ADDE3E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDE8E8E8D5D5D5CCCCCCC7C7
              C8CCCCCCD6D6D6E8E8E9FDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          end
        end
        inherited rgTipo: TRadioGroupLuka
          Left = 212
          ExplicitLeft = 212
        end
        inherited ckAutorizados: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      object cbMovimentos: TComboBoxLuka
        Left = 380
        Top = 68
        Width = 185
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 2
        TabOrder = 2
        Text = 'Ambos'
        Items.Strings = (
          'N'#227'o conciliados'
          'Conciliados'
          'Ambos')
        Valores.Strings = (
          'S'
          'N'
          'NaoFiltrar')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      inline FrCaminhoArquivoOFX: TFrCaminhoArquivo
        Left = -4
        Top = 57
        Width = 372
        Height = 43
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        Visible = False
        ExplicitLeft = -4
        ExplicitTop = 57
        ExplicitWidth = 372
        ExplicitHeight = 43
        inherited lb1: TLabel
          Left = 3
          Top = -3
          Width = 259
          Height = 14
          Caption = 'Caminho arquivo OFX para concilia'#231#227'o banc'#225'ria'
          ExplicitLeft = 3
          ExplicitTop = -3
          ExplicitWidth = 259
          ExplicitHeight = 14
        end
        inherited sbPesquisarArquivo: TSpeedButton
          Left = 345
          Top = 15
          ExplicitLeft = 124
          ExplicitTop = 15
        end
        inherited eCaminhoArquivo: TEditLuka
          Top = 15
          Width = 340
          Height = 22
          ExplicitTop = 15
          ExplicitWidth = 340
          ExplicitHeight = 22
        end
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object splArquivoOFX: TSplitter
        Left = 0
        Top = 36
        Width = 884
        Height = 272
        Align = alNone
        MinSize = 6
        ResizeStyle = rsUpdate
      end
      object sbArquivoOFX: TSpeedButton
        Tag = 1155
        Left = 0
        Top = 312
        Width = 884
        Height = 11
        Hint = 'Visualizar/ocultar arquivo OFX'
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alBottom
        BiDiMode = bdLeftToRight
        Caption = '***'
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ParentBiDiMode = False
        Visible = False
        OnClick = sbArquivoOFXClick
        ExplicitTop = 326
      end
      object TPanel
        Left = 0
        Top = 0
        Width = 884
        Height = 1
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
      end
      object pnArquivoOFX: TPanel
        Left = 0
        Top = 323
        Width = 884
        Height = 195
        Align = alBottom
        BevelOuter = bvNone
        Caption = 'pnArquivoOFX'
        TabOrder = 1
        object sgArquivoOFX: TGridLuka
          Left = 0
          Top = 17
          Width = 884
          Height = 146
          Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
          Align = alClient
          ColCount = 8
          DefaultRowHeight = 19
          DrawingStyle = gdsGradient
          FixedColor = 15395562
          FixedCols = 0
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
          PopupMenu = pmArquivoOFX
          TabOrder = 0
          OnDragDrop = sgArquivoOFXDragDrop
          OnDragOver = sgArquivoOFXDragOver
          OnDrawCell = sgArquivoOFXDrawCell
          IncrementExpandCol = 0
          IncrementExpandRow = 0
          CorLinhaFoco = 38619
          CorFundoFoco = 16774625
          CorLinhaDesfoque = 14869218
          CorFundoDesfoque = 16382457
          CorSuperiorCabecalho = clWhite
          CorInferiorCabecalho = 13553358
          CorSeparadorLinhas = 12040119
          CorColunaFoco = clActiveCaption
          HCol.Strings = (
            'Ident.?'
            'Movimeto Hiva'
            'Data movimento'
            'Valor'
            'Opera'#231#227'o'
            'Natureza'
            'Conc.?'
            'Valor acum.')
          OnGetCellColor = sgArquivoOFXGetCellColor
          OnGetCellPicture = sgArquivoOFXGetCellPicture
          Grid3D = False
          RealColCount = 13
          Indicador = True
          AtivarPopUpSelecao = False
          ColWidths = (
            46
            90
            100
            99
            200
            109
            42
            86)
        end
        object st1: TStaticTextLuka
          Left = 0
          Top = 0
          Width = 884
          Height = 17
          Align = alTop
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          Caption = 'Movimentos do arquivo OFX'
          Color = 38619
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 1
          Transparent = False
          AsInt = 0
          TipoCampo = tcTexto
          CasasDecimais = 0
        end
        object pnDiferencas: TPanel
          Left = 0
          Top = 163
          Width = 884
          Height = 32
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 2
          object st5: TStaticText
            Left = 221
            Top = 1
            Width = 108
            Height = 16
            Alignment = taCenter
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'Qtde.mov. arq.'
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 1
            Transparent = False
          end
          object st6: TStaticText
            Left = 0
            Top = 1
            Width = 104
            Height = 16
            Alignment = taCenter
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'Qtde mov. Hiva'
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 2
            Transparent = False
          end
          object stQtdeMovAltis: TStaticTextLuka
            Left = 0
            Top = 16
            Width = 104
            Height = 16
            Alignment = taCenter
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGreen
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 3
            Transparent = False
            AsInt = 0
            TipoCampo = tcNumerico
            CasasDecimais = 0
          end
          object stQtdeMovArquivo: TStaticTextLuka
            Left = 221
            Top = 16
            Width = 108
            Height = 16
            Alignment = taCenter
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8421440
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 4
            Transparent = False
            AsInt = 0
            TipoCampo = tcNumerico
            CasasDecimais = 0
          end
          object st11: TStaticText
            Left = 103
            Top = 1
            Width = 120
            Height = 16
            Alignment = taCenter
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'Qtd.mov.conc.Hiva'
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 5
            Transparent = False
          end
          object stQtdeMovConcAltis: TStaticTextLuka
            Left = 103
            Top = 16
            Width = 120
            Height = 16
            Alignment = taCenter
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 33023
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 6
            Transparent = False
            AsInt = 0
            TipoCampo = tcNumerico
            CasasDecimais = 0
          end
          object stQtdeMovConcArquivo: TStaticTextLuka
            Left = 328
            Top = 16
            Width = 108
            Height = 16
            Alignment = taCenter
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clOlive
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 0
            Transparent = False
            AsInt = 0
            TipoCampo = tcNumerico
            CasasDecimais = 0
          end
          object st15: TStaticText
            Left = 328
            Top = 1
            Width = 108
            Height = 16
            Alignment = taCenter
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'Qtde. conc. arq.'
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 7
            Transparent = False
          end
        end
      end
      object pnl2: TPanel
        Left = 0
        Top = 1
        Width = 884
        Height = 311
        Align = alClient
        BevelOuter = bvNone
        Caption = 'pnl2'
        TabOrder = 2
        ExplicitTop = 33
        ExplicitHeight = 279
        object sgMovimentacoes: TGridLuka
          Left = 0
          Top = 0
          Width = 884
          Height = 288
          Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
          Align = alClient
          ColCount = 11
          DefaultRowHeight = 19
          DrawingStyle = gdsGradient
          FixedColor = 15395562
          FixedCols = 0
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
          PopupMenu = pmOpcoes
          TabOrder = 0
          OnDblClick = sgMovimentacoesDblClick
          OnDrawCell = sgMovimentacoesDrawCell
          OnKeyDown = sgMovimentacoesKeyDown
          OnMouseDown = sgMovimentacoesMouseDown
          IncrementExpandCol = 0
          IncrementExpandRow = 0
          CorLinhaFoco = 38619
          CorFundoFoco = 16774625
          CorLinhaDesfoque = 14869218
          CorFundoDesfoque = 16382457
          CorSuperiorCabecalho = clWhite
          CorInferiorCabecalho = 13553358
          CorSeparadorLinhas = 12040119
          CorColunaFoco = clActiveCaption
          HCol.Strings = (
            'Sel.?'
            'Movimento'
            'Opera'#231#227'o'
            'Valor'
            'Acumulado'
            'Data/hora movimento'
            'Funcion'#225'rio'
            'Conc.?'
            'Acu.conc.'
            'Data/hora conc.'
            'Usu'#225'rio concilia'#231#227'o')
          OnGetCellColor = sgMovimentacoesGetCellColor
          OnGetCellPicture = sgMovimentacoesGetCellPicture
          Grid3D = False
          RealColCount = 13
          Indicador = True
          AtivarPopUpSelecao = False
          ExplicitTop = -8
          ExplicitHeight = 264
          ColWidths = (
            34
            66
            166
            70
            72
            127
            189
            40
            68
            112
            187)
        end
        object pnl1: TPanel
          Left = 0
          Top = 288
          Width = 884
          Height = 23
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitTop = 287
          object StaticText1: TStaticText
            Left = 234
            Top = -1
            Width = 110
            Height = 20
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'Sa'#237'das por per'#237'odo'
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 0
            Transparent = False
          end
          object stTotalDebitos: TStaticTextLuka
            Left = 343
            Top = -1
            Width = 114
            Height = 20
            Alignment = taCenter
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = '0,00'
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 1
            Transparent = False
            AsInt = 0
            TipoCampo = tcNumerico
            CasasDecimais = 2
          end
          object StaticText2: TStaticText
            Left = 0
            Top = -1
            Width = 121
            Height = 20
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'Entradas por per'#237'odo'
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 2
            Transparent = False
          end
          object stTotalCreditos: TStaticTextLuka
            Left = 120
            Top = -1
            Width = 114
            Height = 20
            Alignment = taCenter
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = '0,00'
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 3
            Transparent = False
            AsInt = 0
            TipoCampo = tcNumerico
            CasasDecimais = 2
          end
          object st3: TStaticText
            Left = 457
            Top = -1
            Width = 73
            Height = 20
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'Saldo atual '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 4
            Transparent = False
          end
          object st8: TStaticText
            Left = 654
            Top = -1
            Width = 103
            Height = 20
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'Saldo conciliado '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 5
            Transparent = False
          end
          object stSaldoAtual: TStaticTextLuka
            Left = 528
            Top = -1
            Width = 126
            Height = 20
            Alignment = taCenter
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = '0,00'
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 6
            Transparent = False
            AsInt = 0
            TipoCampo = tcNumerico
            CasasDecimais = 2
          end
          object stSaldoAtualConciliado: TStaticTextLuka
            Left = 756
            Top = -1
            Width = 128
            Height = 20
            Alignment = taCenter
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = '0,00'
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 7
            Transparent = False
            AsInt = 0
            TipoCampo = tcNumerico
            CasasDecimais = 2
          end
        end
      end
    end
  end
  object pmOpcoes: TPopupMenu
    Left = 947
    Top = 203
  end
  object pmArquivoOFX: TPopupMenu
    Left = 947
    Top = 443
    object miLancarTransferencia: TMenuItem
      Caption = 'Lan'#231'ar transfer'#234'ncia'
      OnClick = miLancarTransferenciaClick
    end
    object miN3: TMenuItem
      Caption = '-'
    end
    object miLancarTarifa: TMenuItem
      Caption = 'Lan'#231'ar tarifa'
      OnClick = miLancarTarifaClick
    end
  end
  object frxReport: TfrxReport
    Version = '5.1.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44043.704685613400000000
    ReportOptions.LastChange = 45104.018866493060000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 360
    Top = 184
    Datasets = <
      item
        DataSet = dmRelatorio.dstEmpresa
        DataSetName = 'frxdstEmpresa'
      end
      item
        DataSet = dstMovimentos
        DataSetName = 'frxdstMovimentos'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 86.929190000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          Top = 70.811070000000000000
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
        object Memo1: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000001000
          Width = 68.031540000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 26.456710000000000000
          Top = 34.015770000000010000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 15.118120000000000000
          Top = 49.133889999999990000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 340.157700000000000000
          Top = 3.779530000000001000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 321.260050000000000000
          Top = 34.015770000000010000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade/UF:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 340.157700000000000000
          Top = 49.133889999999990000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 82.708720000000000000
          Top = 3.779530000000001000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 383.512060000000000000
          Top = 3.779530000000001000
          Width = 200.315090000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."CNPJ"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 84.929190000000000000
          Top = 18.897650000000000000
          Width = 551.811380000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            
              '[frxdstEmpresa."LOGRADOURO"] [frxdstEmpresa."COMPLEMENTO"] n'#176'[fr' +
              'xdstEmpresa."NUMERO"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 82.708720000000000000
          Top = 34.015770000000010000
          Width = 207.874150000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_BAIRRO"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 383.512060000000000000
          Top = 34.015770000000010000
          Width = 185.196970000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_CIDADE"] / [frxdstEmpresa."ESTADO_ID"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 82.708720000000000000
          Top = 49.133889999999990000
          Width = 75.590600000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 383.512060000000000000
          Top = 49.133889999999990000
          Width = 260.787570000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."E_MAIL"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 545.252320000000000000
          Top = 3.779530000000001000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Emitido em [Date] '#224's [Time]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 2.779530000000000000
          Top = 70.811070000000000000
          Width = 56.692950000000010000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Movimento')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 64.252010000000030000
          Top = 70.811070000000000000
          Width = 86.929190000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Opera'#231#227'o')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 451.102660000000000000
          Top = 70.811070000000000000
          Width = 90.708720000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Funcion'#225'rio')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 547.370440000000000000
          Top = 70.811070000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Conciliado?')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 545.252320000000000000
          Top = 18.897650000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pag. [Page] de [TotalPages#]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo34: TfrxMemoView
          Left = 212.031496062992100000
          Top = 70.811070000000000000
          Width = 68.031540000000010000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 359.614410000000000000
          Top = 70.811070000000000000
          Width = 86.929190000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Data movimento')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 287.425602049999900000
          Top = 70.811070000000000000
          Width = 68.031540000000010000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Acumulado')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 166.299320000000000000
        Width = 718.110700000000000000
        DataSet = dstMovimentos
        DataSetName = 'frxdstMovimentos'
        RowCount = 0
        object frxdstTranscodigoProd: TfrxMemoView
          Left = 2.779530000000000000
          Top = 1.000000000000000000
          Width = 56.692920710000000000
          Height = 11.338590000000000000
          DataField = 'Movimento'
          DataSet = dstMovimentos
          DataSetName = 'frxdstMovimentos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstMovimentos."Movimento"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo15: TfrxMemoView
          Left = 64.251978270000000000
          Top = 1.000000000000000000
          Width = 139.842580710000000000
          Height = 11.338590000000000000
          DataField = 'Operacao'
          DataSet = dstMovimentos
          DataSetName = 'frxdstMovimentos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstMovimentos."Operacao"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo23: TfrxMemoView
          Left = 212.031417950000000000
          Top = 1.000000000000000000
          Width = 68.031496060000000000
          Height = 11.338590000000000000
          DataField = 'ValorMovimento'
          DataSet = dstMovimentos
          DataSetName = 'frxdstMovimentos'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstMovimentos."ValorMovimento"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 359.433041570000000000
          Top = 1.000000000000000000
          Width = 86.929133859999990000
          Height = 11.338590000000000000
          DataField = 'DataMovimento'
          DataSet = dstMovimentos
          DataSetName = 'frxdstMovimentos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxdstMovimentos."DataMovimento"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo26: TfrxMemoView
          Left = 547.275624720000000000
          Top = 1.000000000000000000
          Width = 52.913385830000000000
          Height = 11.338590000000000000
          DataField = 'Conciliado'
          DataSet = dstMovimentos
          DataSetName = 'frxdstMovimentos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxdstMovimentos."Conciliado"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo28: TfrxMemoView
          Left = 451.275478270000000000
          Top = 1.000000000000000000
          Width = 90.708661420000000000
          Height = 11.338590000000000000
          DataField = 'Funcionario'
          DataSet = dstMovimentos
          DataSetName = 'frxdstMovimentos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstMovimentos."Funcionario"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo22: TfrxMemoView
          Left = 287.244280000000000000
          Top = 1.000000000000000000
          Width = 68.031496060000000000
          Height = 11.338590000000000000
          DataField = 'Acumulado'
          DataSet = dstMovimentos
          DataSetName = 'frxdstMovimentos'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstMovimentos."Acumulado"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 41.952782760000000000
        Top = 241.889920000000000000
        Width = 718.110700000000000000
        object Line1: TfrxLineView
          Top = 3.779529999999994000
          Width = 718.110700000000000000
          StretchMode = smMaxHeight
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 306.141930000000000000
        Width = 718.110700000000000000
        object Shape2: TfrxShapeView
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
      end
    end
  end
  object dstMovimentos: TfrxDBDataset
    UserName = 'frxdstMovimentos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Movimento=Movimento'
      'Operacao=Operacao'
      'ValorMovimento=ValorMovimento'
      'Acumulado=Acumulado'
      'DataMovimento=DataMovimento'
      'Funcionario=Funcionario'
      'Conciliado=Conciliado')
    DataSet = cdsMovimentos
    BCDToCurrency = False
    Left = 229
    Top = 212
  end
  object cdsMovimentos: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Movimento'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Operacao'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'ValorMovimento'
        DataType = ftFloat
      end
      item
        Name = 'Acumulado'
        DataType = ftFloat
      end
      item
        Name = 'DataMovimento'
        DataType = ftDate
      end
      item
        Name = 'Funcionario'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'Conciliado'
        DataType = ftString
        Size = 60
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 228
    Top = 267
    object cdsMovimentosMovimento: TStringField
      FieldName = 'Movimento'
    end
    object cdsMovimentosOperacao: TStringField
      FieldName = 'Operacao'
      Size = 60
    end
    object cdsMovimentosValorMovimento: TFloatField
      FieldName = 'ValorMovimento'
    end
    object cdsMovimentosAcumulado: TFloatField
      FieldName = 'Acumulado'
    end
    object cdsMovimentosDataMovimento: TDateField
      FieldName = 'DataMovimento'
    end
    object cdsMovimentosFuncionario: TStringField
      FieldName = 'Funcionario'
      Size = 60
    end
    object cdsMovimentosConciliado: TStringField
      FieldName = 'Conciliado'
    end
  end
end
