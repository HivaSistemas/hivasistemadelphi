inherited FormAlterarDadosPrecificacaoProdutos: TFormAlterarDadosPrecificacaoProdutos
  Caption = 'Alterar dados precifica'#231#227'o de produtos'
  ClientHeight = 128
  ClientWidth = 404
  ExplicitWidth = 410
  ExplicitHeight = 157
  PixelsPerInch = 96
  TextHeight = 14
  object lbColunaSelecionada: TLabel [0]
    Left = 8
    Top = 9
    Width = 156
    Height = 14
    Caption = 'Data de cadastro do produto'
  end
  inherited pnOpcoes: TPanel
    Top = 91
    Width = 404
    ExplicitTop = 91
    ExplicitWidth = 404
  end
  object eValor: TEditLuka
    Left = 8
    Top = 25
    Width = 200
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 1
    Text = '0,00'
    OnExit = eValorExit
    OnKeyPress = NumerosVirgula
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 2
    ValorMaximo = 100.000000000000000000
    NaoAceitarEspaco = True
  end
  object eValorData: TEditLukaData
    Left = 8
    Top = 25
    Width = 200
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '!99/99/0000;1;_'
    MaxLength = 10
    TabOrder = 2
    Text = '  /  /    '
  end
  object rgTipoAlteracao: TRadioGroupLuka
    Left = 214
    Top = 8
    Width = 186
    Height = 74
    Caption = ' Tipo de altera'#231#227'o'
    ItemIndex = 0
    Items.Strings = (
      'Valor '
      'Aumentar percentual'
      'Diminuir percentual')
    TabOrder = 3
    TabStop = True
    Valores.Strings = (
      'VLR'
      'AMP'
      'DMP')
  end
end
