unit FrameICMS;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.Grids, System.StrUtils,
  GridLuka, Vcl.ExtCtrls, Vcl.StdCtrls, _Biblioteca, EditLuka, System.Math,
  Vcl.Buttons, Vcl.Menus, _GruposTribEstadualVendaEst;

type
  TFrICMS = class(TFrameHerancaPrincipal)
    pnGrid: TPanel;
    sgICMS: TGridLuka;
    pmOpcoes: TPopupMenu;
    Panel1: TPanel;
    lb1: TLabel;
    lbEstadoSelecionado: TLabel;
    sbPreencher: TSpeedButton;
    eValor: TEditLuka;
    procedure sgICMSDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sbPreencherClick(Sender: TObject);
    procedure sgICMSSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure eValorKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    procedure setICMS(const pValor: TArray<RecGruposTribEstadualVendaEst>);
    function  getICMS: TArray<RecGruposTribEstadualVendaEst>;
    function getLinhaGrid(const pLinha: Integer): RecGruposTribEstadualVendaEst;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Clear; override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
    function TemEstadoComOperacaoST: Boolean;
    procedure VerificarRegistro;
  published
    property ICMS: TArray<RecGruposTribEstadualVendaEst> read getICMS write setICMS;
  end;

implementation

{$R *.dfm}

const
  coEstado                 = 0;
  coCstNaoContribuinte     = 1;
  coCstContribuinte        = 2;
  coCstOrgaoPublico        = 3;
  coCstRevenda             = 4;
  coCstConstrutora         = 5;
  coCstClinicaHospital     = 6;
  coCstProdutorRural       = 7;
  coPercentualAliquotaIcms = 8;
  coPercentualAliquotaIcmsInter = 9;
  coPercentualIva          = 10;
  coIndiceReducaoBaseIcms  = 11;
  coPrecoPauta             = 12;


procedure TFrICMS.Clear;
var
  vLinha: Integer;
  vColuna: Integer;
begin
  inherited;
  for vLinha := sgICMS.FixedRows to sgICMS.RowCount - 1 do begin
    for vColuna := sgICMS.FixedCols to sgICMS.ColCount do
      sgICMS.Cells[vColuna, vLinha] := '';
  end;
end;

constructor TFrICMS.Create(AOwner: TComponent);
begin
  inherited;
  Align := alClient;
  sgICMS.FixedCols := 1;

  sgICMS.OcultarColunas([coPercentualAliquotaIcmsInter]);

  sgICMS.Col := coCstNaoContribuinte;
  sgICMS.Invalidate;
  Invalidate;
end;

procedure TFrICMS.eValorKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key <> VK_RETURN) and (eValor.Text <> '') then
    Exit;

  sbPreencherClick(nil);
end;

function TFrICMS.getLinhaGrid(const pLinha: Integer): RecGruposTribEstadualVendaEst;
begin
  Result.EstadoId           := sgICMS.Cells[coEstado, pLinha];

  Result.CstNaoContribuinte := sgICMS.Cells[coCstNaoContribuinte, pLinha];
  Result.CstContribuinte    := sgICMS.Cells[coCstContribuinte, pLinha];
  Result.CstOrgaoPublico    := sgICMS.Cells[coCstOrgaoPublico, pLinha];
  Result.CstRevenda         := sgICMS.Cells[coCstRevenda, pLinha];
  Result.CstConstrutora     := sgICMS.Cells[coCstConstrutora, pLinha];
  Result.CstClinicaHospital := sgICMS.Cells[coCstClinicaHospital, pLinha];
  Result.CstProdutorRural   := sgICMS.Cells[coCstProdutorRural, pLinha];

  Result.PercentualIcms         := SFormatDouble(sgICMS.Cells[coPercentualAliquotaIcms, pLinha]);
  Result.PercentualIcmsInter    := SFormatDouble(sgICMS.Cells[coPercentualAliquotaIcmsInter, pLinha]);
  Result.IndiceReducaoBaseIcms  := SFormatDouble(sgICMS.Cells[coIndiceReducaoBaseIcms, pLinha]);
  Result.IVA                    := SFormatDouble(sgICMS.Cells[coPercentualIva, pLinha]);
  Result.PrecoPauta             := SFormatDouble(sgICMS.Cells[coPrecoPauta, pLinha]);
end;

function TFrICMS.getICMS: TArray<RecGruposTribEstadualVendaEst>;
var
  vLinha: Integer;
begin
  if sgICMS.Cells[coEstado, 1] = '' then
    Exit;

  SetLength(Result, sgICMS.RowCount - 1);
  for vLinha := sgICMS.FixedRows to sgICMS.RowCount - 1 do
    Result[vLinha - 1] := getLinhaGrid(vLinha);
end;

procedure TFrICMS.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;
  sgICMS.Enabled := pEditando;

  if pLimpar then
    Clear;
end;

procedure TFrICMS.sbPreencherClick(Sender: TObject);
var
  vLinha: Integer;
begin
  inherited;
  if (eValor.Text = '') then
    Exit;

  for vLinha := sgICMS.FixedRows to sgICMS.RowCount - 1 do
    sgICMS.Cells[sgICMS.Col, vLinha] := eValor.Text;

  if not(sgICMS.Col = coPrecoPauta) then
    sgICMS.Col := sgICMS.Col + 1
  else
    sgICMS.Col := coCstNaoContribuinte;

  eValor.Clear;
  SetarFoco(eValor);
end;

procedure TFrICMS.setICMS(const pValor: TArray<RecGruposTribEstadualVendaEst>);
var
  vLinha: Integer;
  vPosicGrid: Integer;
begin
  for vLinha := Low(pValor) to High(pValor) do begin
    vPosicGrid := sgICMS.Localizar([coEstado], [pValor[vLinha].EstadoId]);

    sgICMS.Cells[coCstNaoContribuinte, vPosicGrid]     := pValor[vLinha].CstNaoContribuinte;
    sgICMS.Cells[coCstContribuinte, vPosicGrid]        := pValor[vLinha].CstContribuinte;
    sgICMS.Cells[coCstOrgaoPublico, vPosicGrid]        := pValor[vLinha].CstOrgaoPublico;
    sgICMS.Cells[coCstRevenda, vPosicGrid]             := pValor[vLinha].CstRevenda;
    sgICMS.Cells[coCstConstrutora, vPosicGrid]         := pValor[vLinha].CstConstrutora;
    sgICMS.Cells[coCstClinicaHospital, vPosicGrid]     := pValor[vLinha].CstClinicaHospital;
    sgICMS.Cells[coCstProdutorRural, vPosicGrid]       := pValor[vLinha].CstProdutorRural;

    sgICMS.Cells[coPercentualAliquotaIcms, vPosicGrid] := NFormatN(pValor[vLinha].PercentualIcms);
    sgICMS.Cells[coPercentualAliquotaIcmsInter, vPosicGrid] := NFormatN(pValor[vLinha].PercentualIcmsInter);
    sgICMS.Cells[coIndiceReducaoBaseIcms, vPosicGrid]  := NFormatN(pValor[vLinha].IndiceReducaoBaseIcms, 4);
    sgICMS.Cells[coPercentualIva, vPosicGrid]          := NFormatN(pValor[vLinha].iva);
    sgICMS.Cells[coPrecoPauta, vPosicGrid]             := NFormatN(pValor[vLinha].PrecoPauta);
  end;
end;


procedure TFrICMS.sgICMSDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if
    ACol in[
      coCstNaoContribuinte,
      coCstContribuinte,
      coCstOrgaoPublico,
      coCstRevenda,
      coCstConstrutora,
      coCstClinicaHospital,
      coCstProdutorRural]
  then
    vAlinhamento := taCenter
  else if ACol = coEstado then
    vAlinhamento := taLeftJustify
  else
    vAlinhamento := taRightJustify;

  sgICMS.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFrICMS.sgICMSSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  if (ACol = coEstado) or FSomenteLeitura then
    sgICMS.Options := sgICMS.Options - [goEditing]
  else
    sgICMS.Options := sgICMS.Options + [goEditing];

  if ACol in[coPercentualAliquotaIcms, coPercentualAliquotaIcms, coPercentualIva, coPrecoPauta, coIndiceReducaoBaseIcms] then begin
    sgICMS.OnKeyPress := NumerosPonto;
    eValor.OnKeyPress := NumerosPonto;
    eValor.CasasDecimais := IfThen(ACol = coIndiceReducaoBaseIcms, 4, 2);
    eValor.Clear;
  end
  else begin
    sgICMS.OnKeyPress := Numeros;
    eValor.OnKeyPress := Numeros;
    eValor.CasasDecimais := 0;
  end;

  lbEstadoSelecionado.Caption :=
    _Biblioteca.Decode(
      sgICMS.Cells[coEstado, ARow],
      [
        'AC', 'Acre',
        'AL', 'Alagoas',
        'AP', 'Amap�',
        'AM', 'Amazonas',
        'BA', 'Bahia',
        'CE', 'Cear�',
        'DF', 'Distrito Federal',
        'ES', 'Espirito Santo',
        'GO', 'Goi�s',
        'MA', 'Maranh�o',
        'MT', 'Mato Grosso',
        'MS', 'Mato Grosso do Sul',
        'MG', 'Minas Gerais',
        'PA', 'Par�',
        'PB', 'Para�ba',
        'PR', 'Paran�',
        'PE', 'Pernambuco',
        'PI', 'Piau�',
        'RJ', 'Rio de Janeiro',
        'RN', 'Rio Grande do Norte',
        'RS', 'Rio Grande do Sul',
        'RO', 'Rond�nia',
        'RR', 'Roraima',
        'SC', 'Santa Catarina',
        'SP', 'S�o Paulo',
        'SE', 'Sergipe',
        'TO', 'Tocantins'
      ]
    );
end;

function TFrICMS.TemEstadoComOperacaoST: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := sgICMS.FixedRows to sgICMS.RowCount - sgICMS.FixedRows do begin
    Result :=
      Em(
        ['10', '60', '70'], [
        sgICMS.Cells[coCstNaoContribuinte, i],
        sgICMS.Cells[coCstContribuinte, i],
        sgICMS.Cells[coCstOrgaoPublico, i],
        sgICMS.Cells[coCstRevenda, i],
        sgICMS.Cells[coCstConstrutora, i],
        sgICMS.Cells[coCstClinicaHospital, i],
        sgICMS.Cells[coCstProdutorRural, i]]
      );
    if Result then
      Break;
  end;
end;

procedure TFrICMS.VerificarRegistro;
var
  i: Integer;

  procedure VerificarColuna(pColuna: Integer; pLinha: Integer; pColunaMensagem: string; pUF: string);
  begin

    if sgICMS.Cells[pColuna, pLinha] = '' then begin
      _Biblioteca.Exclamar('A coluna "' + pColunaMensagem + '" para o estado ' + pUF + ' n�o foi informada corretamente, verifique!');
      sgICMS.Row := pLinha;
      sgICMS.Col := pColuna;
      SetarFoco(sgICMS);
      Abort;
    end
    else if not Em(sgICMS.Cells[pColuna, pLinha], ['00','10','20','30','40','41','50','51','60','70','90']) then begin
      _Biblioteca.Exclamar(
        'A coluna "' + pColunaMensagem + '" para o estado ' + pUF + ' est� preenchida com um CST inexistente, verifique!' + chr(13) +
        'CST Preenchido: ' + sgICMS.Cells[pColuna, pLinha] + chr(13) +
        'CSTs existentes: ' + '00, 10, 20, 30, 40, 41, 50, 51, 60, 70, 90'
      );
      sgICMS.Row := pLinha;
      sgICMS.Col := pColuna;
      SetarFoco(sgICMS);
      Abort;
    end;

  end;

begin

  for i := 1 to sgICMS.RowCount -1 do begin
    VerificarColuna(coCstNaoContribuinte, i, '"N�o cont."', sgICMS.Cells[coEstado, i]);
    VerificarColuna(coCstContribuinte, i, '"Cont."', sgICMS.Cells[coEstado, i]);
    VerificarColuna(coCstOrgaoPublico, i, '"Org. p�b."', sgICMS.Cells[coEstado, i]);
    VerificarColuna(coCstRevenda, i, '"Rev."', sgICMS.Cells[coEstado, i]);
    VerificarColuna(coCstConstrutora, i, '"Const."', sgICMS.Cells[coEstado, i]);
    VerificarColuna(coCstClinicaHospital, i, '"Cl�n./Hosp."', sgICMS.Cells[coEstado, i]);
    VerificarColuna(coCstProdutorRural, i, '"Prod.rural"', sgICMS.Cells[coEstado, i]);
  end;

end;

end.
