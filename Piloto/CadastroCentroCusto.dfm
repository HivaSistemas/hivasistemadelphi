inherited FormCadastroCentroCustos: TFormCadastroCentroCustos
  Caption = 'Cadastro de centro de custos'
  ClientHeight = 206
  ClientWidth = 451
  ExplicitWidth = 457
  ExplicitHeight = 235
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label1: TLabel
    Left = 125
    Top = 38
    ExplicitLeft = 125
    ExplicitTop = 38
  end
  object lb1: TLabel [1]
    Left = 125
    Top = 93
    Width = 32
    Height = 14
    Caption = 'Nome'
  end
  inherited pnOpcoes: TPanel
    Height = 206
    ExplicitHeight = 271
    inherited sbDesfazer: TSpeedButton
      Top = 96
      ExplicitTop = 96
    end
    inherited sbExcluir: TSpeedButton
      Top = 157
      ExplicitTop = 157
    end
    inherited sbPesquisar: TSpeedButton
      Top = 48
      ExplicitTop = 48
    end
    inherited sbLogs: TSpeedButton
      Top = 220
      ExplicitTop = 220
    end
  end
  inherited eID: TEditLuka
    Left = 125
    Top = 52
    ExplicitLeft = 125
    ExplicitTop = 52
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 125
    Top = 8
    ExplicitLeft = 125
    ExplicitTop = 8
  end
  object eNome: TEditLuka
    Left = 125
    Top = 107
    Width = 317
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 3
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
