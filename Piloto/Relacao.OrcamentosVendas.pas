unit Relacao.OrcamentosVendas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _RecordsRelatorios,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, Vcl.StdCtrls, GroupBoxLuka, FrameDataInicialFinal, FrameEmpresas, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameClientes, Vcl.Grids,
  GridLuka, FrameCondicoesPagamento, _RelacaoOrcamentosVendas, _Biblioteca, _Sessao, System.Math, System.StrUtils, Informacoes.Orcamento, Frame.TipoAnaliseCusto, CheckBoxLuka, Frame.Inteiros,
  _Orcamentos, StaticTextLuka, Vcl.Menus, Impressao.ComprovantePagamentoGrafico, _RecordsEspeciais, FrameVendedores, FrameProdutos, _RecordsOrcamentosVendas, _OrcamentosItens,
  ImpressaoConfissaoDivida, BuscarIndiceDescontoVenda, Impressao.OrcamentoGrafico, ImpressaoMeiaPaginaDuplicataMercantil,
  FrameFaixaNumeros, ComboBoxLuka, FrameGruposClientes, Data.DB, _Profissionais, Impressao.OrcamentoGraficoAmbiente,
  Datasnap.DBClient, frxClass, frxDBSet, frxExportPDF, Informacoes.Devolucao, _RecordsCadastros,
  OraCall, DBAccess, Ora, MemDS, OraSmart, FrameProfissionais, BuscarDadosParceiro;

type
  TFormRelacaoOrcamentosVendas = class(TFormHerancaRelatoriosPageControl)
    FrClientes: TFrClientes;
    FrEmpresas: TFrEmpresas;
    FrDataCadastro: TFrDataInicialFinal;
    gbStatus: TGroupBoxLuka;
    ckVenda: TCheckBox;
    spSeparador: TSplitter;
    sgOrcamentos: TGridLuka;
    sgProdutos: TGridLuka;
    FrCondicoesPagamento: TFrCondicoesPagamento;
    gbFormasPagamento: TGroupBoxLuka;
    ckCartao: TCheckBox;
    ckFinanceira: TCheckBox;
    ckCheque: TCheckBox;
    ckCobranca: TCheckBox;
    ckAcumulado: TCheckBox;
    ckDinheiro: TCheckBoxLuka;
    FrCodigoOrcamento: TFrameInteiros;
    FrCodigoTurno: TFrameInteiros;
    StaticTextLuka1: TStaticTextLuka;
    StaticTextLuka2: TStaticTextLuka;
    pmOpcoesOrcamento: TPopupMenu;
    FrVendedores: TFrVendedores;
    FrPrevisaoEntrega: TFrDataInicialFinal;
    FrProdutos: TFrProdutos;
    miN1: TMenuItem;
    miN2: TMenuItem;
    ckDevolu��o: TCheckBox;
    FrValorTotal: TFrFaixaNumeros;
    FrDataRecebimento: TFrDataInicialFinal;
    lb1: TLabel;
    cbAgrupamento: TComboBoxLuka;
    miCancelarFechamentoPedido: TSpeedButton;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    miReimprimirOrcamento: TSpeedButton;
    miReimprimirComprovantePagamento: TSpeedButton;
    miReimprimirConfissaoDivida: TSpeedButton;
    miReimprimirDuplicataMercantil: TSpeedButton;
    miGerarPDFdooramentovendaFocada: TSpeedButton;
    miAlterarIndiceDescontoVenda: TSpeedButton;
    Panel1: TPanel;
    frxReport: TfrxReport;
    dstVendas: TfrxDBDataset;
    cdsVendas: TClientDataSet;
    cdsVendaspedido: TStringField;
    cdsVendasdata_recebimento: TDateField;
    cdsVendascliente: TStringField;
    cdsVendasvendedor: TStringField;
    cdsVendascond_pgto: TStringField;
    cdsVendasvalor_produto: TFloatField;
    cdsVendasvalor_total: TFloatField;
    cdsVendasoutras_despesas: TFloatField;
    cdsVendasdesconto: TFloatField;
    cdsVendasvalor_bruto: TFloatField;
    cdsVendasvalor_liquido: TFloatField;
    cdsVendasperc_lucro_bruto: TFloatField;
    cdsVendasperc_lucro_liquido: TFloatField;
    cdsVendasempresa: TStringField;
    ckPix: TCheckBox;
    FrCodigoDevolucao: TFrameInteiros;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    cdsVendasdevolucao: TStringField;
    dstProdutosVendasDevolucoes: TfrxDBDataset;
    dstVendasDevolucoes: TfrxDBDataset;
    qVendasDevolucoes: TOraQuery;
    dsVendasDevolucoes: TOraDataSource;
    qVendasDevolucoesProdutos: TOraQuery;
    qVendasDevolucoesProdutosPOSICAO: TFloatField;
    qVendasDevolucoesProdutosPRODUTO_ID: TFloatField;
    qVendasDevolucoesProdutosNOME: TStringField;
    qVendasDevolucoesProdutosMARCA: TStringField;
    qVendasDevolucoesProdutosPRECO_UNITARIO: TFloatField;
    qVendasDevolucoesProdutosQUANTIDADE: TFloatField;
    qVendasDevolucoesProdutosUNIDADE: TStringField;
    qVendasDevolucoesProdutosVALOR_TOTAL: TFloatField;
    qVendasDevolucoesProdutosVALOR_LUCRO_BRUTO: TFloatField;
    qVendasDevolucoesProdutosVALOR_LUCRO_LIQUIDO: TFloatField;
    qVendasDevolucoesProdutosPERC_LUCRO_BRUTO: TFloatField;
    qVendasDevolucoesProdutosPERC_LUCRO_LIQUIDO: TFloatField;
    frxReportAnalitico: TfrxReport;
    qVendasDevolucoesProdutosOUTRAS_DESPESAS: TFloatField;
    qVendasDevolucoesProdutosDESCONTO: TFloatField;
    frxReport1: TfrxReport;
    qVendasDevolucoesPEDIDO: TStringField;
    qVendasDevolucoesDATA_RECEBIMENTO: TStringField;
    qVendasDevolucoesCLIENTE: TStringField;
    qVendasDevolucoesVENDEDOR: TStringField;
    qVendasDevolucoesCOND_PGTO: TStringField;
    qVendasDevolucoesVALOR_PRODUTO: TFloatField;
    qVendasDevolucoesVALOR_TOTAL: TFloatField;
    qVendasDevolucoesOUTRAS_DESPESAS: TFloatField;
    qVendasDevolucoesDESCONTO: TFloatField;
    qVendasDevolucoesVALOR_BRUTO: TFloatField;
    qVendasDevolucoesVALOR_LIQUIDO: TFloatField;
    qVendasDevolucoesPERC_LUCRO_BRUTO: TFloatField;
    qVendasDevolucoesPERC_LUCRO_LIQUIDO: TFloatField;
    qVendasDevolucoesEMPRESA: TStringField;
    qVendasDevolucoesDEVOLUCAO: TStringField;
    qVendasDevolucoesPOSICAO: TFloatField;
    qVendasDevolucoesMOVIMENTO: TStringField;
    cdsVendasmovimento: TStringField;
    SpeedButton6: TSpeedButton;
    FrProfissional: TFrProfissionais;
    SpeedButton7: TSpeedButton;
    st2: TStaticText;
    stQtdeVendas: TStaticTextLuka;
    stQtdeDevolucoesLabel: TStaticText;
    stQtdeDevolucoes: TStaticTextLuka;
    SpeedButton8: TSpeedButton;
    cbTipoOrcamento: TComboBoxLuka;
    lbTipoOrcamento: TLabel;
    ckSomenteProducao: TCheckBoxLuka;
    SpeedButton9: TSpeedButton;
    procedure sgOrcamentosClick(Sender: TObject);
    procedure sgOrcamentosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgOrcamentosDblClick(Sender: TObject);
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure sgOrcamentosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure miReimprimirComprovantePagamentoClick(Sender: TObject);
    procedure miCancelarFechamentoPedidoClick(Sender: TObject);
    procedure miReimprimirConfissaoDividaClick(Sender: TObject);
    procedure miAlterarIndiceDescontoVendaClick(Sender: TObject);
    procedure miReimprimirOrcamentoClick(Sender: TObject);
    procedure miGerarPDFdooramentovendaFocadaClick(Sender: TObject);
    procedure miReimprimirDuplicataMercantilClick(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure SpeedButton9Click(Sender: TObject);
  private
    FProdutosAmbiente: TArray<RecAmbientes>;
    FProdutosOrcamentos: TArray<RecOrcamentoItens>;
    FItensDevolucoes: TArray<RecDevolucoesItens>;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

uses
  ImpressaoRelacaoVendasProdutosGrafico, SelecionarVariosOpcoes, untDmRelatorio,
  _RelacaoVendasDevolucoes, _DevolucoesItens, _Devolucoes,
  ImpressaoComprovanteDevolucaoGrafico, Impressao.ComprovanteFaturamentoVenda,
  MixVendas, _ProdutosVenda;

{$R *.dfm}

{ TFormRelacaoOrcamentosVendas }

const
  coOrcamentoId      = 0;
  coDevolucaoId      = 1;
  coTipoMovimentoAnalitico = 2;
  coCliente          = 3;
  coDataHoraReceb    = 4;
  coVendedor         = 5;
  coCondicaoPagto    = 6;
  coValorProdutos    = 7;
  coValorTotal       = 8;
  coValorOutDesp     = 9;
  coValorFrete       = 10;
  coValorDesconto    = 11;
  coValorLucroBruto  = 12;
  coPercLucroBruto   = 13;
  coValorLucroLiq    = 14;
  coPercLucroLiquido = 15;
  coEmpresa          = 16;
  coParceiro         = 17;
  coIndiceDescVenda  = 18;
  coTipoLinha        = 19;
  coTipoMovimento    = 20;
  coCondicaoPagamentoId = 21;
  coClienteId           = 22;
  coEmpresaId           = 23;

  (* Grid de produto *)
  cpProdutoId        = 0;
  cpNome             = 1;
  cpMarca            = 2;
  cpPrecoUnitario    = 3;
  cpQuantidade       = 4;
  cpUnidade          = 5;
  cpValorTotal       = 6;
  cpValorOutDesp     = 7;
  cpValorDesconto    = 8;
  cpValorLucroBruto  = 9;
  cpPercLucroBruto   = 10;
  cpValorLucroLiquido = 11;
  cpPercLucroLiquido  = 12;
  cpParceiroId       = 13;
  cpAmbiente         = 14;

  coLinhaDetalhe   = 'D';
  coLinhaCabAgrup  = 'CAGRU';
  coLinhaTotVenda  = 'TV';
  coTotalAgrupador = 'TAGRU';

procedure TFormRelacaoOrcamentosVendas.Carregar(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vAgrupadorId: Integer;
  vAgrupadorData: string;
  vTotalVendas: Integer;
  vTotalDevolucoes: Integer;

  vComando: string;
  vOrcamentosIds: TArray<Integer>;
  vDevolucoesIds: TArray<Integer>;
  vOrcamentosVendas: TArray<RecOrcamentosVendas>;

  vSqlFormasPagto: string;
  vSqlStatus: string;

  vTotais: record
    totalProdutos: Double;
    totalOutrasDespesas: Double;
    totalValorFrete: Double;
    totalDesconto: Double;
    valorTotal: Double;
    ValorLucroBruto: Double;
    PercLucroBruto: Double;
    ValorLucroLiquido: Double;
    PercLucroLiquido: Double;
  end;

  vTotaisAgrupador: record
    totalProdutos: Double;
    totalOutrasDespesas: Double;
    totalValorFrete: Double;
    totalDesconto: Double;
    valorTotal: Double;
    ValorLucroBruto: Double;
    PercLucroBruto: Double;
    ValorLucroLiquido: Double;
    PercLucroLiquido: Double;
    QtdeVendas: Integer;
    QtdeDevolucoes: Integer;
  end;

  procedure SqlFormaPagamento(pColuna: string; pCheckBox: TCheckBox);
  begin
    if pCheckBox.Checked then
      AddOrSeNecessario(vSqlFormasPagto, ' ' + pColuna + ' > 0');
  end;

  procedure SqlStatus(pValor: string; pCheckBox: TCheckBox);
  begin
    if pCheckBox.Checked then
      AddVirgulaSeNecessario(vSqlStatus, '''' + pValor + '''');
  end;

  procedure TotalizarGrupo;
  var
    vAgrupador: string;
  begin
    Inc(vLinha);

    vAgrupador := _Biblioteca.Decode(cbAgrupamento.GetValor, ['VEN', 'VENDEDOR', 'CLI', 'CLIENTE', 'GRUPO DE CLIENTE', 'DAT', 'Data recebimento']);

    if cbAgrupamento.GetValor = 'DAT' then
      sgOrcamentos.Cells[coCondicaoPagto, vLinha]    := 'TOTAL: '
    else
      sgOrcamentos.Cells[coCondicaoPagto, vLinha]    := 'TOTAL DO ' + vAgrupador + ': ';

    sgOrcamentos.Cells[coCliente, vLinha]          := 'QTDE. VENDAS: ' + IntToStr(vTotaisAgrupador.QtdeVendas);
    sgOrcamentos.Cells[coVendedor, vLinha]         := 'QTDE. DEVOLU��ES: ' + IntToStr(vTotaisAgrupador.QtdeDevolucoes);
    sgOrcamentos.Cells[coValorProdutos, vLinha]    := NFormatN(vTotaisAgrupador.totalProdutos);
    sgOrcamentos.Cells[coValorOutDesp, vLinha]     := NFormatN(vTotaisAgrupador.totalOutrasDespesas);
    sgOrcamentos.Cells[coValorFrete, vLinha]       := NFormatN(vTotaisAgrupador.totalValorFrete);
    sgOrcamentos.Cells[coValorDesconto, vLinha]    := NFormatN(vTotaisAgrupador.totalDesconto);
    sgOrcamentos.Cells[coValorTotal, vLinha]       := NFormatN(vTotaisAgrupador.valorTotal);
    sgOrcamentos.Cells[coValorLucroBruto, vLinha]  := NFormatN(vTotaisAgrupador.ValorLucroBruto);
    sgOrcamentos.Cells[coValorLucroLiq, vLinha]    := NFormatN(vTotaisAgrupador.ValorLucroLiquido);
    sgOrcamentos.Cells[coPercLucroBruto, vLinha]   := NFormatN( Sessao.getCalculosSistema.CalcOrcamento.getPercLucroBruto(vTotaisAgrupador.valorTotal, vTotaisAgrupador.ValorLucroBruto, 4), 4 );
    sgOrcamentos.Cells[coPercLucroLiquido, vLinha] := NFormatN( Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(vTotaisAgrupador.valorTotal, vTotaisAgrupador.ValorLucroLiquido,4 ), 4);
    sgOrcamentos.Cells[coTipoLinha, vLinha]        := coTotalAgrupador;

    vTotaisAgrupador.totalProdutos       := 0;
    vTotaisAgrupador.totalOutrasDespesas := 0;
    vTotaisAgrupador.totalValorFrete     := 0;
    vTotaisAgrupador.totalDesconto       := 0;
    vTotaisAgrupador.valorTotal          := 0;
    vTotaisAgrupador.ValorLucroBruto     := 0;
    vTotaisAgrupador.PercLucroBruto      := 0;
    vTotaisAgrupador.ValorLucroLiquido   := 0;
    vTotaisAgrupador.PercLucroLiquido    := 0;
    vTotaisAgrupador.QtdeVendas          := 0;
    vTotaisAgrupador.QtdeDevolucoes      := 0;
  end;

begin
  inherited;
  sgOrcamentos.ClearGrid();
  sgProdutos.ClearGrid();
  FProdutosOrcamentos := nil;
  FItensDevolucoes := nil;
  stQtdeVendas.Clear;
  stQtdeDevolucoes.Clear;
  vTotalVendas := 0;
  vTotalDevolucoes := 0;

  if not FrCodigoOrcamento.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrCodigoOrcamento.getSqlFiltros('ORCAMENTO_ID'));

  if not FrCodigoDevolucao.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrCodigoOrcamento.getSqlFiltros('DEVOLUCAO_ID'));

  if not FrCodigoTurno.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrCodigoTurno.getSqlFiltros('TURNO_ID'));

  if not FrEmpresas.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrEmpresas.getSqlFiltros('EMPRESA_ID'));

  if not FrClientes.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrClientes.getSqlFiltros('CLIENTE_ID'));

  if not FrCondicoesPagamento.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrCondicoesPagamento.getSqlFiltros('CONDICAO_ID'));

  if not FrDataCadastro.NenhumaDataValida then
    _Biblioteca.WhereOuAnd(vComando, FrDataCadastro.getSqlFiltros('DATA_MOVIMENTO'));

  if not FrVendedores.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrVendedores.getSqlFiltros('VENDEDOR_ID'));

  if not FrProfissional.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrProfissional.getSqlFiltros('PARCEIRO_ID'));

  if not FrPrevisaoEntrega.NenhumaDataValida then
    _Biblioteca.WhereOuAnd(vComando, FrPrevisaoEntrega.getSqlFiltros('DATA_ENTREGA'));

  FrDataRecebimento.getSqlFiltros('trunc(DATA_HORA_RECEBIMENTO)', vComando, True);

  _Biblioteca.WhereOuAnd(vComando, FrValorTotal.getSQL('VALOR_TOTAL'));

//  FrGruposCliente.getSqlFiltros('CLIENTE_GRUPO_ID', vComando, True);

  if not FrProdutos.EstaVazio then begin
    _Biblioteca.WhereOuAnd(
      vComando,
      'ORCAMENTO_ID in(select distinct ORCAMENTO_ID from ORCAMENTOS_ITENS where ' + FiltroInInt('PRODUTO_ID', FrProdutos.GetArrayOfInteger) + ') '
    );
  end;

  if ckSomenteProducao.Checked then begin
    _Biblioteca.WhereOuAnd(
      vComando,
      'ORCAMENTO_ID in(select distinct ORCAMENTO_ID from ORCAMENTOS_ITENS where PRODUTO_ID in (select PRODUTO_ID from PRODUTOS where TIPO_CONTROLE_ESTOQUE = ''I'') ) '
    );
  end;

  if (ckDinheiro.Checked) or (ckPix.Checked) or (ckCheque.Checked) or (ckCartao.Checked) or (ckFinanceira.Checked) or
     (ckCobranca.Checked) or (ckAcumulado.Checked) then
  begin
    SqlFormaPagamento('VALOR_DINHEIRO', ckDinheiro);
    SqlFormaPagamento('VALOR_PIX', ckPix);
    SqlFormaPagamento('VALOR_CHEQUE', ckCheque);
    SqlFormaPagamento('VALOR_CARTAO_DEBITO', ckCartao);
    SqlFormaPagamento('VALOR_CARTAO_CREDITO', ckCartao);
    SqlFormaPagamento('VALOR_FINANCEIRA', ckFinanceira);
    SqlFormaPagamento('VALOR_COBRANCA + VALOR_CREDITO', ckCobranca);
    SqlFormaPagamento('VALOR_ACUMULATIVO', ckAcumulado);
    vComando := vComando + ' and ( ' + vSqlFormasPagto + ' )';
  end;

  if cbTipoOrcamento.GetValor = 'AMB' then
    vComando := vComando + ' and ORC_POR_AMBIENTE = ''S'' ';

  vComando := vComando + ' and STATUS in(''RE'') ';

  SqlStatus('VEN', ckVenda);
  SqlStatus('DEV', ckDevolu��o);
  vComando := vComando + ' and TIPO_MOVIMENTO in( ' + vSqlStatus + ') ';

//  _Biblioteca.WhereOuAnd(vComando, 'TIPO_ANALISE_CUSTO = ''C'' ');

  if cbAgrupamento.GetValor = 'VEN' then
    vComando := vComando + ' order by NOME_FUNCIONARIO '
  else if cbAgrupamento.GetValor = 'CLI' then
    vComando := vComando + ' order by NOME_CLIENTE '
  else if cbAgrupamento.GetValor = 'GCL' then
    vComando := vComando + ' order by NOME_GRUPO_CLIENTE '
  else if cbAgrupamento.GetValor = 'DAT' then
    vComando := vComando + ' order by DATA_MOVIMENTO '
  else
    vComando := vComando + ' order by DATA_MOVIMENTO ';

  vOrcamentosVendas := _RelacaoVendasDevolucoes.BuscarVendasDevolucoes(Sessao.getConexaoBanco, vComando);
  if vOrcamentosVendas = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vAgrupadorId := -1;
  vAgrupadorData := '';

  vTotais.totalProdutos       := 0;
  vTotais.totalOutrasDespesas := 0;
  vTotais.totalValorFrete     := 0;
  vTotais.totalDesconto       := 0;
  vTotais.valorTotal          := 0;
  vTotais.ValorLucroBruto     := 0;
  vTotais.PercLucroBruto      := 0;
  vTotais.ValorLucroLiquido   := 0;
  vTotais.PercLucroLiquido    := 0;

  vTotaisAgrupador.totalProdutos       := 0;
  vTotaisAgrupador.totalOutrasDespesas := 0;
  vTotaisAgrupador.totalValorFrete     := 0;
  vTotaisAgrupador.totalDesconto       := 0;
  vTotaisAgrupador.valorTotal          := 0;
  vTotaisAgrupador.ValorLucroBruto     := 0;
  vTotaisAgrupador.PercLucroBruto      := 0;
  vTotaisAgrupador.ValorLucroLiquido   := 0;
  vTotaisAgrupador.PercLucroLiquido    := 0;
  vTotaisAgrupador.QtdeVendas          := 0;
  vTotaisAgrupador.QtdeDevolucoes      := 0;

  vLinha := 0;
  for i := Low(vOrcamentosVendas) to High(vOrcamentosVendas) do begin
    if cbAgrupamento.GetValor <> 'NEN' then begin
      if cbAgrupamento.GetValor = 'VEN' then begin
        if vAgrupadorId <> vOrcamentosVendas[i].VendedorId then begin
          if i > 0 then
            TotalizarGrupo;

          Inc(vLinha);

          sgOrcamentos.Cells[coCliente, vLinha]   := getInformacao(vOrcamentosVendas[i].VendedorId, vOrcamentosVendas[i].NomeVendedor);
          sgOrcamentos.Cells[coTipoLinha, vLinha] := coLinhaCabAgrup;

          vAgrupadorId := vOrcamentosVendas[i].VendedorId;
        end;
      end
      else if cbAgrupamento.GetValor = 'CLI' then begin
        if vAgrupadorId <> vOrcamentosVendas[i].ClienteId then begin
          if i > 0 then
            TotalizarGrupo;

          Inc(vLinha);

          sgOrcamentos.Cells[coCliente, vLinha]   := getInformacao(vOrcamentosVendas[i].ClienteId, vOrcamentosVendas[i].NomeCliente);
          sgOrcamentos.Cells[coTipoLinha, vLinha] := coLinhaCabAgrup;

          vAgrupadorId := vOrcamentosVendas[i].ClienteId;
        end;
      end
      else if cbAgrupamento.GetValor = 'GCL' then begin
        if vAgrupadorId <> vOrcamentosVendas[i].ClienteGrupoId then begin
          if i > 0 then
            TotalizarGrupo;

          Inc(vLinha);

          sgOrcamentos.Cells[coCliente, vLinha]   := getInformacao(vOrcamentosVendas[i].ClienteGrupoId, vOrcamentosVendas[i].NomeGrupoCliente);
          sgOrcamentos.Cells[coTipoLinha, vLinha] := coLinhaCabAgrup;

          vAgrupadorId := vOrcamentosVendas[i].ClienteGrupoId;
        end;
      end
      else if cbAgrupamento.GetValor = 'DAT' then begin
        if vAgrupadorData <> _Biblioteca.DFormatN(vOrcamentosVendas[i].DataAgrupamento) then begin
          if i > 0 then
            TotalizarGrupo;

          Inc(vLinha);

          sgOrcamentos.Cells[coCliente, vLinha]   := 'DATA RECEBIMENTO: ' + _Biblioteca.DFormatN(vOrcamentosVendas[i].DataAgrupamento);
          sgOrcamentos.Cells[coTipoLinha, vLinha] := coLinhaCabAgrup;

          vAgrupadorData := _Biblioteca.DFormatN(vOrcamentosVendas[i].DataAgrupamento);
        end;
      end;
    end;

    Inc(vLinha);

    sgOrcamentos.Cells[coOrcamentoId, vLinha]      := NFormat(vOrcamentosVendas[i].OrcamentoId);
    if vOrcamentosVendas[i].DevolucaoId > 0 then
      sgOrcamentos.Cells[coDevolucaoId, vLinha] := NFormat(vOrcamentosVendas[i].DevolucaoId);
    sgOrcamentos.Cells[coTipoMovimentoAnalitico, vLinha]    := vOrcamentosVendas[i].TipoMovimentoAnalitico;
    sgOrcamentos.Cells[coCliente, vLinha]          := NFormat(vOrcamentosVendas[i].ClienteId) + ' - ' + vOrcamentosVendas[i].NomeCliente;
//    sgOrcamentos.Cells[coDataCadastro, vLinha]     := DFormat(vOrcamentosVendas[i].DataCadastro);
    sgOrcamentos.Cells[coDataHoraReceb, vLinha]    := DHFormatN(vOrcamentosVendas[i].DataHoraRecebimento);
    sgOrcamentos.Cells[coVendedor, vLinha]         := getInformacao(vOrcamentosVendas[i].VendedorId, vOrcamentosVendas[i].NomeVendedor);
    sgOrcamentos.Cells[coCondicaoPagto, vLinha]    := NFormat(vOrcamentosVendas[i].CondicaoId) + ' - ' + vOrcamentosVendas[i].NomeCondicaoPagamento;
    sgOrcamentos.Cells[coValorProdutos, vLinha]    := NFormatN(vOrcamentosVendas[i].ValorTotalProdutos);
    sgOrcamentos.Cells[coValorOutDesp, vLinha]     := NFormatN(vOrcamentosVendas[i].ValorOutrasDespesas);
    sgOrcamentos.Cells[coValorFrete, vLinha]       := NFormatN(vOrcamentosVendas[i].ValorFrete);
    sgOrcamentos.Cells[coValorDesconto, vLinha]    := NFormatN(vOrcamentosVendas[i].ValorDesconto);
    sgOrcamentos.Cells[coValorTotal, vLinha]       := NFormatN(vOrcamentosVendas[i].ValorTotal);
    sgOrcamentos.Cells[coValorLucroBruto, vLinha]  := NFormatN(vOrcamentosVendas[i].ValorTotal - vOrcamentosVendas[i].ValorCustoEntrada);
    sgOrcamentos.Cells[coValorLucroLiq, vLinha]    := NFormatN(vOrcamentosVendas[i].ValorTotal - vOrcamentosVendas[i].ValorCustoFinal);
    sgOrcamentos.Cells[coPercLucroBruto, vLinha]   := NFormatN(Sessao.getCalculosSistema.CalcOrcamento.getPercLucroBruto(vOrcamentosVendas[i].ValorTotal, vOrcamentosVendas[i].ValorTotal - vOrcamentosVendas[i].ValorCustoEntrada, 4), 4);
    sgOrcamentos.Cells[coPercLucroLiquido, vLinha] := NFormatN(Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(vOrcamentosVendas[i].ValorTotal, vOrcamentosVendas[i].ValorTotal - vOrcamentosVendas[i].ValorCustoFinal, 4), 4);
    sgOrcamentos.Cells[coEmpresa, vLinha]          := NFormat(vOrcamentosVendas[i].EmpresaId) + ' - ' + vOrcamentosVendas[i].NomeEmpresa;
    sgOrcamentos.Cells[coCondicaoPagamentoId, vLinha] := IntToStr(vOrcamentosVendas[i].CondicaoId);
    sgOrcamentos.Cells[coClienteId, vLinha]           := IntToStr(vOrcamentosVendas[i].ClienteId);
    sgOrcamentos.Cells[coEmpresaId, vLinha]           := IntToStr(vOrcamentosVendas[i].EmpresaId);

    if vOrcamentosVendas[i].ParceiroId <> 0 then begin
      sgOrcamentos.Cells[coParceiro, vLinha]         := NFormat(vOrcamentosVendas[i].ParceiroId) + ' - ' + vOrcamentosVendas[i].ParceiroNome;
      sgOrcamentos.Cells[cpParceiroId, vLinha] := NFormat(vOrcamentosVendas[i].ParceiroId);
    end;

    sgOrcamentos.Cells[coIndiceDescVenda, vLinha]  := NFormat(vOrcamentosVendas[i].IndiceDescontoVendaId);
    sgOrcamentos.Cells[coTipoMovimento, vLinha]    := vOrcamentosVendas[i].TipoMovimento;
    sgOrcamentos.Cells[coTipoLinha, vLinha]        := coLinhaDetalhe;

    if vOrcamentosVendas[i].TipoMovimento = 'VEN' then begin
      vTotalVendas := vTotalVendas + 1;
      vTotais.totalProdutos       := vTotais.totalProdutos + vOrcamentosVendas[i].ValorTotalProdutos;
      vTotais.totalOutrasDespesas := vTotais.totalOutrasDespesas + vOrcamentosVendas[i].ValorOutrasDespesas;
      vTotais.totalValorFrete     := vTotais.totalValorFrete + vOrcamentosVendas[i].ValorFrete;
      vTotais.totalDesconto       := vTotais.totalDesconto + vOrcamentosVendas[i].ValorDesconto;
      vTotais.valorTotal          := vTotais.valorTotal + vOrcamentosVendas[i].ValorTotal;
      vTotais.ValorLucroBruto     := vTotais.ValorLucroBruto + SFormatDouble( sgOrcamentos.Cells[coValorLucroBruto, vLinha] );
      vTotais.ValorLucroLiquido   := vTotais.ValorLucroLiquido + SFormatDouble( sgOrcamentos.Cells[coValorLucroLiq, vLinha] );
      vTotais.PercLucroBruto      := vTotais.PercLucroBruto + SFormatDouble( sgOrcamentos.Cells[coPercLucroBruto, vLinha] );
      vTotais.PercLucroLiquido    := vTotais.PercLucroLiquido + SFormatDouble( sgOrcamentos.Cells[coPercLucroLiquido, vLinha] );

      vTotaisAgrupador.totalProdutos       := vTotaisAgrupador.totalProdutos + vOrcamentosVendas[i].ValorTotalProdutos;
      vTotaisAgrupador.totalOutrasDespesas := vTotaisAgrupador.totalOutrasDespesas + vOrcamentosVendas[i].ValorOutrasDespesas;
      vTotaisAgrupador.totalValorFrete     := vTotaisAgrupador.totalValorFrete + vOrcamentosVendas[i].ValorFrete;
      vTotaisAgrupador.totalDesconto       := vTotaisAgrupador.totalDesconto + vOrcamentosVendas[i].ValorDesconto;
      vTotaisAgrupador.valorTotal          := vTotaisAgrupador.valorTotal + vOrcamentosVendas[i].ValorTotal;
      vTotaisAgrupador.ValorLucroBruto     := vTotaisAgrupador.ValorLucroBruto + SFormatDouble( sgOrcamentos.Cells[coValorLucroBruto, vLinha] );
      vTotaisAgrupador.ValorLucroLiquido   := vTotaisAgrupador.ValorLucroLiquido + SFormatDouble( sgOrcamentos.Cells[coValorLucroLiq, vLinha] );
      vTotaisAgrupador.PercLucroBruto      := vTotaisAgrupador.PercLucroBruto + SFormatDouble( sgOrcamentos.Cells[coPercLucroBruto, vLinha] );
      vTotaisAgrupador.PercLucroLiquido    := vTotaisAgrupador.PercLucroLiquido + SFormatDouble( sgOrcamentos.Cells[coPercLucroLiquido, vLinha] );
      _Biblioteca.AddNoVetorSemRepetir(vOrcamentosIds, vOrcamentosVendas[i].OrcamentoId);
      vTotaisAgrupador.QtdeVendas          := vTotaisAgrupador.QtdeVendas + 1;
    end
    else begin
      vTotalDevolucoes := vTotalDevolucoes + 1;
      vTotais.totalProdutos       := vTotais.totalProdutos - vOrcamentosVendas[i].ValorTotalProdutos;
      vTotais.totalOutrasDespesas := vTotais.totalOutrasDespesas - vOrcamentosVendas[i].ValorOutrasDespesas;
      vTotais.totalValorFrete     := vTotais.totalValorFrete - vOrcamentosVendas[i].ValorFrete;
      vTotais.totalDesconto       := vTotais.totalDesconto - vOrcamentosVendas[i].ValorDesconto;
      vTotais.valorTotal          := vTotais.valorTotal - vOrcamentosVendas[i].ValorTotal;
      vTotais.ValorLucroBruto     := vTotais.ValorLucroBruto - SFormatDouble( sgOrcamentos.Cells[coValorLucroBruto, vLinha] );
      vTotais.ValorLucroLiquido   := vTotais.ValorLucroLiquido - SFormatDouble( sgOrcamentos.Cells[coValorLucroLiq, vLinha] );
      vTotais.PercLucroBruto      := vTotais.PercLucroBruto - SFormatDouble( sgOrcamentos.Cells[coPercLucroBruto, vLinha] );
      vTotais.PercLucroLiquido    := vTotais.PercLucroLiquido - SFormatDouble( sgOrcamentos.Cells[coPercLucroLiquido, vLinha] );

      vTotaisAgrupador.totalProdutos       := vTotaisAgrupador.totalProdutos - vOrcamentosVendas[i].ValorTotalProdutos;
      vTotaisAgrupador.totalOutrasDespesas := vTotaisAgrupador.totalOutrasDespesas - vOrcamentosVendas[i].ValorOutrasDespesas;
      vTotaisAgrupador.totalValorFrete     := vTotaisAgrupador.totalValorFrete - vOrcamentosVendas[i].ValorFrete;
      vTotaisAgrupador.totalDesconto       := vTotaisAgrupador.totalDesconto - vOrcamentosVendas[i].ValorDesconto;
      vTotaisAgrupador.valorTotal          := vTotaisAgrupador.valorTotal - vOrcamentosVendas[i].ValorTotal;
      vTotaisAgrupador.ValorLucroBruto     := vTotaisAgrupador.ValorLucroBruto - SFormatDouble( sgOrcamentos.Cells[coValorLucroBruto, vLinha] );
      vTotaisAgrupador.ValorLucroLiquido   := vTotaisAgrupador.ValorLucroLiquido - SFormatDouble( sgOrcamentos.Cells[coValorLucroLiq, vLinha] );
      vTotaisAgrupador.PercLucroBruto      := vTotaisAgrupador.PercLucroBruto - SFormatDouble( sgOrcamentos.Cells[coPercLucroBruto, vLinha] );
      vTotaisAgrupador.PercLucroLiquido    := vTotaisAgrupador.PercLucroLiquido - SFormatDouble( sgOrcamentos.Cells[coPercLucroLiquido, vLinha] );
      _Biblioteca.AddNoVetorSemRepetir(vDevolucoesIds, vOrcamentosVendas[i].DevolucaoId);
      vTotaisAgrupador.QtdeDevolucoes      := vTotaisAgrupador.QtdeDevolucoes + 1;
    end;
  end;

  if cbAgrupamento.GetValor <> 'NEN' then begin
    TotalizarGrupo;
    Inc(vLinha);
  end;

  Inc(vLinha);

  sgOrcamentos.Cells[coCondicaoPagto, vLinha]    := 'TOTAIS: ';
  sgOrcamentos.Cells[coValorProdutos, vLinha]    := NFormatN(vTotais.totalProdutos);
  sgOrcamentos.Cells[coValorOutDesp, vLinha]     := NFormatN(vTotais.totalOutrasDespesas);
  sgOrcamentos.Cells[coValorFrete, vLinha]       := NFormatN(vTotais.totalValorFrete);
  sgOrcamentos.Cells[coValorDesconto, vLinha]    := NFormatN(vTotais.totalDesconto);
  sgOrcamentos.Cells[coValorTotal, vLinha]       := NFormatN(vTotais.valorTotal);
  sgOrcamentos.Cells[coValorLucroBruto, vLinha]  := NFormatN(vTotais.ValorLucroBruto);
  sgOrcamentos.Cells[coValorLucroLiq, vLinha]    := NFormatN(vTotais.ValorLucroLiquido);
  sgOrcamentos.Cells[coPercLucroBruto, vLinha]   := NFormatN( Sessao.getCalculosSistema.CalcOrcamento.getPercLucroBruto(vTotais.valorTotal, vTotais.ValorLucroBruto, 4), 4 );
  sgOrcamentos.Cells[coPercLucroLiquido, vLinha] := NFormatN( Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(vTotais.valorTotal, vTotais.ValorLucroLiquido,4 ), 4);
  sgOrcamentos.Cells[coTipoLinha, vLinha]        := coLinhaTotVenda;

  Inc(vLinha);
  sgOrcamentos.RowCount := vLinha;

  if vOrcamentosIds <> nil then begin
    FProdutosOrcamentos := _OrcamentosItens.BuscarOrcamentosItensComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('ITE.ORCAMENTO_ID', vOrcamentosIds) + ' and ITE.ITEM_KIT_ID is null order by PRO.NOME ');
  end;

  if vDevolucoesIds <> nil then
    FItensDevolucoes := _DevolucoesItens.BuscarDevolucoesItensComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('ITE.DEVOLUCAO_ID', vDevolucoesIds) + ' order by ITE.DEVOLUCAO_ID, ITE.ITEM_ID');

  stQtdeVendas.AsInt := vTotalVendas;
  stQtdeDevolucoes.AsInt := vTotalDevolucoes;
  sgOrcamentosClick(sgOrcamentos);
  SetarFoco(sgOrcamentos);
end;

procedure TFormRelacaoOrcamentosVendas.FormCreate(Sender: TObject);
var
  vDataAtual: TDateTime;
begin
  inherited;
  AtivarAnaliseCusto(Sessao.getParametros.UtilizarAnaliseCusto);
  FProdutosAmbiente := nil;

  ActiveControl := FrEmpresas.sgPesquisa;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId, False);

  lbTipoOrcamento.Visible := Sessao.getParametros.OrcamentoAmbiente = 'S';
  cbTipoOrcamento.Visible := Sessao.getParametros.OrcamentoAmbiente = 'S';

  if Sessao.getUsuarioLogado.Vendedor = 'S' then begin
    vDataAtual := Sessao.getData;

    if not Sessao.AutorizadoRotina('visualizar_vendas_outras_vendedores') then begin
      FrVendedores.InserirDadoPorChave(Sessao.getUsuarioLogado.funcionario_id, False);
      FrVendedores.Modo(False, False);
    end;

    if not Sessao.AutorizadoRotina('visualizar_vendas_fora_dia_atual') then begin
      FrDataCadastro.eDataInicial.AsData := vDataAtual;
      FrDataCadastro.eDataFinal.AsData := vDataAtual;
      FrDataCadastro.Modo(False, False);
    end;

    if not Sessao.AutorizadoRotina('visualizar_lucro_venda') then begin
     sgOrcamentos.OcultarColunas([
       coValorLucroBruto, 
       coValorLucroLiq, 
       coPercLucroBruto, 
       coPercLucroLiquido
     ]);

     sgProdutos.OcultarColunas([
       cpValorLucroBruto, 
       cpValorLucroLiquido, 
       cpPercLucroBruto, 
       cpPercLucroLiquido
     ]);
    end;
  end;

  qVendasDevolucoes.Session := Sessao.getConexaoBanco;
  qVendasDevolucoesProdutos.Session := Sessao.getConexaoBanco;

  ckSomenteProducao.Visible := Sessao.getUsuarioLogado.funcionario_id = 1;
  SpeedButton9.Visible := Sessao.getUsuarioLogado.funcionario_id = 1;
end;

procedure TFormRelacaoOrcamentosVendas.Imprimir(Sender: TObject);
var
  i, j: Integer;
  vUltimoVendedorId: Integer;
  vDados: TArray<RecDadosImpressao>;
  vOpcao: TRetornoTelaFinalizar<Integer>;
  vProdutosVendDevol: TArray<RecDadosProdutoVendDevol>;
  vVendDevol: TArray<RecDadosVendDevol>;
  quantidade: Double;
  multiplicador: Integer;
begin
  inherited;

//  if cbAgrupamento.GetValor <> caAgrupadoVendedor then begin
//    _Biblioteca.ImpressaoNaoDisponivel;
//    Exit;
//  end;

  vOpcao := SelecionarVariosOpcoes.Selecionar('Tipo relat�rio', ['Sint�tico', 'An�litico'], 0);
  if vOpcao.BuscaCancelada then
    Exit;

  if vOpcao.Dados = 0 then
  begin
    cdsVendas.Close;
    cdsVendas.CreateDataSet;
    cdsVendas.Open;
    for i := sgOrcamentos.RowCount - 2 downto 1 do
    begin
      cdsVendas.Insert;
      cdsVendaspedido.AsString              := sgOrcamentos.Cells[coOrcamentoId,i];
      cdsVendasDevolucao.AsString           := sgOrcamentos.Cells[coDevolucaoId,i];
      cdsVendasmovimento.AsString           := sgOrcamentos.Cells[coTipoMovimento,i];
      cdsVendasdata_recebimento.AsDateTime  := ToData(sgOrcamentos.Cells[coDataHoraReceb,i]);
      cdsVendascliente.AsString             := sgOrcamentos.Cells[coCliente,i];
      cdsVendasvendedor.AsString            := sgOrcamentos.Cells[coVendedor,i];
      cdsVendascond_pgto.AsString           := sgOrcamentos.Cells[coCondicaoPagto,i];
      cdsVendasvalor_produto.AsFloat        := SFormatDouble(sgOrcamentos.Cells[coValorProdutos,i]);
      cdsVendasvalor_total.AsFloat          := SFormatDouble(sgOrcamentos.Cells[coValorTotal,i]);
      cdsVendasoutras_despesas.AsFloat      := SFormatDouble(sgOrcamentos.Cells[coValorOutDesp,i]);
      cdsVendasdesconto.AsFloat             := SFormatDouble(sgOrcamentos.Cells[coValorDesconto,i]);

      if Sessao.AutorizadoRotina('visualizar_lucro_venda') then begin
        cdsVendasvalor_bruto.AsFloat          := SFormatDouble(sgOrcamentos.Cells[coValorLucroBruto,i]);
        cdsVendasvalor_liquido.AsFloat        := SFormatDouble(sgOrcamentos.Cells[coValorLucroLiq,i]);
        cdsVendasperc_lucro_bruto.AsFloat     := SFormatDouble(sgOrcamentos.Cells[coPercLucroBruto,i]);
        cdsVendasperc_lucro_liquido.AsFloat   := SFormatDouble(sgOrcamentos.Cells[coPercLucroLiquido,i]);
      end;

      cdsVendasempresa.AsString             := sgOrcamentos.Cells[coEmpresa,i];
      cdsVendas.Post;
    end;

    TfrxMemoView(frxReport.FindComponent('mmValorProduto')).Text :=   'Valor Produto.....: ' + sgOrcamentos.Cells[coValorProdutos,sgOrcamentos.RowCount - 1];
    TfrxMemoView(frxReport.FindComponent('mmValorTotal')).Text   :=   'Valor Total.......: ' + sgOrcamentos.Cells[coValorTotal,sgOrcamentos.RowCount - 1];
    TfrxMemoView(frxReport.FindComponent('mmOutrasDespesas')).Text := 'Outras Despesas...: ' + sgOrcamentos.Cells[coValorOutDesp,sgOrcamentos.RowCount - 1];
    TfrxMemoView(frxReport.FindComponent('mmDescontos')).Text   :=    'Descontos.........: ' + sgOrcamentos.Cells[coValorDesconto,sgOrcamentos.RowCount - 1];
    
    if Sessao.AutorizadoRotina('visualizar_lucro_venda') then begin
      TfrxMemoView(frxReport.FindComponent('mmValorBruto')).Text :=     'Valor Bruto.......: ' + sgOrcamentos.Cells[coValorLucroBruto,sgOrcamentos.RowCount - 1];
      TfrxMemoView(frxReport.FindComponent('mmPercLucroBruto')).Text := '% Lucro Bruto.....: ' + sgOrcamentos.Cells[coPercLucroBruto,sgOrcamentos.RowCount - 1];
      TfrxMemoView(frxReport.FindComponent('mmValorLiquido')).Text   := 'Valor Liquido.....: ' + sgOrcamentos.Cells[coValorLucroLiq,sgOrcamentos.RowCount - 1];
      TfrxMemoView(frxReport.FindComponent('mmPercLucroLiq')).Text   := '% Lucro Liquido...: ' + sgOrcamentos.Cells[coPercLucroLiquido,sgOrcamentos.RowCount - 1];
    end;

    frxReport.ShowReport;
  end
  else if vOpcao.Dados = 1 then
  begin
    for i := sgOrcamentos.RowCount - 2 downto 1 do
    begin
      SetLength(vVendDevol, Length(vVendDevol) + 1);
      if sgOrcamentos.Cells[coTipoMovimento, i] = 'VEN' then
        multiplicador := 1
      else
        multiplicador := -1;

      vVendDevol[High(vVendDevol)].Pedido           := sgOrcamentos.Cells[coOrcamentoId,i];
      vVendDevol[High(vVendDevol)].Devolucao        := sgOrcamentos.Cells[coDevolucaoId,i];
      vVendDevol[High(vVendDevol)].Movimento        := sgOrcamentos.Cells[coTipoMovimento,i];
      vVendDevol[High(vVendDevol)].DataRecebimento  := ToData(sgOrcamentos.Cells[coDataHoraReceb,i]);
      vVendDevol[High(vVendDevol)].Cliente          := sgOrcamentos.Cells[coCliente,i];
      vVendDevol[High(vVendDevol)].Vendedor         := sgOrcamentos.Cells[coVendedor,i];
      vVendDevol[High(vVendDevol)].CondPagto        := sgOrcamentos.Cells[coCondicaoPagto,i];
      vVendDevol[High(vVendDevol)].ValorProduto     := SFormatDouble(sgOrcamentos.Cells[coValorProdutos,i]);
      vVendDevol[High(vVendDevol)].ValorTotal       := SFormatDouble(sgOrcamentos.Cells[coValorTotal,i]);
      vVendDevol[High(vVendDevol)].OutrasDespesas   := SFormatDouble(sgOrcamentos.Cells[coValorOutDesp,i]);
      vVendDevol[High(vVendDevol)].Desconto         := SFormatDouble(sgOrcamentos.Cells[coValorDesconto,i]);
      
      if Sessao.AutorizadoRotina('visualizar_lucro_venda') then begin
        vVendDevol[High(vVendDevol)].ValorBruto       := SFormatDouble(sgOrcamentos.Cells[coValorLucroBruto,i]);
        vVendDevol[High(vVendDevol)].ValorLiquido     := SFormatDouble(sgOrcamentos.Cells[coValorLucroLiq,i]);
        vVendDevol[High(vVendDevol)].PercLucroBruto   := SFormatDouble(sgOrcamentos.Cells[coPercLucroBruto,i]);
        vVendDevol[High(vVendDevol)].PercLucroLiquido := SFormatDouble(sgOrcamentos.Cells[coPercLucroLiquido,i]);
      end;
      
      vVendDevol[High(vVendDevol)].Empresa          := sgOrcamentos.Cells[coEmpresa,i];

      if sgOrcamentos.Cells[coOrcamentoId, i] = '' then
        Continue;

      vVendDevol[High(vVendDevol)].Posicao := i;

      if sgOrcamentos.Cells[coTipoMovimento, i] = 'VEN' then begin
        for j := Low(FProdutosOrcamentos) to High(FProdutosOrcamentos) do begin
          if SFormatInt(sgOrcamentos.Cells[coOrcamentoId, i]) <> FProdutosOrcamentos[j].orcamento_id then
            Continue;

          SetLength(vProdutosVendDevol, Length(vProdutosVendDevol) + 1);
          vProdutosVendDevol[High(vProdutosVendDevol)].Posicao := i;

          vProdutosVendDevol[High(vProdutosVendDevol)].ProdutoId         := FProdutosOrcamentos[j].produto_id;
          vProdutosVendDevol[High(vProdutosVendDevol)].Nome              := FProdutosOrcamentos[j].nome;
          vProdutosVendDevol[High(vProdutosVendDevol)].Marca             := FProdutosOrcamentos[j].nome_marca;
          vProdutosVendDevol[High(vProdutosVendDevol)].PrecoUnitario     := FProdutosOrcamentos[j].preco_unitario;
          vProdutosVendDevol[High(vProdutosVendDevol)].Quantidade        := FProdutosOrcamentos[j].Quantidade;
          vProdutosVendDevol[High(vProdutosVendDevol)].Unidade           := FProdutosOrcamentos[j].unidade_venda;
          vProdutosVendDevol[High(vProdutosVendDevol)].ValorTotal        := FProdutosOrcamentos[j].valor_total;
          vProdutosVendDevol[High(vProdutosVendDevol)].OutrasDespesas    := FProdutosOrcamentos[j].valor_total_outras_despesas;
          vProdutosVendDevol[High(vProdutosVendDevol)].Desconto          := FProdutosOrcamentos[j].valor_total_desconto;

          if Sessao.AutorizadoRotina('visualizar_lucro_venda') then begin
            vProdutosVendDevol[High(vProdutosVendDevol)].ValorLucroBruto   := FProdutosOrcamentos[j].ValorLiquido - FProdutosOrcamentos[j].CustoEntrada;
            vProdutosVendDevol[High(vProdutosVendDevol)].ValorLucroLiquido := FProdutosOrcamentos[j].ValorLiquido - FProdutosOrcamentos[j].ValorCustoFinal;
            vProdutosVendDevol[High(vProdutosVendDevol)].PercLucroBruto    := Sessao.getCalculosSistema.CalcOrcamento.getPercLucroBruto(FProdutosOrcamentos[j].ValorLiquido, FProdutosOrcamentos[j].ValorLiquido - FProdutosOrcamentos[j].CustoEntrada, 4);
            vProdutosVendDevol[High(vProdutosVendDevol)].PercLucroLiquido  := Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(FProdutosOrcamentos[j].ValorLiquido, FProdutosOrcamentos[j].ValorLiquido - FProdutosOrcamentos[j].ValorCustoFinal, 4);
          end;
          
        end;
      end
      else begin
        for j := Low(FItensDevolucoes) to High(FItensDevolucoes) do begin
          if SFormatInt(sgOrcamentos.Cells[coDevolucaoId, i]) <> FItensDevolucoes[j].DevolucaoId then
            Continue;

          SetLength(vProdutosVendDevol, Length(vProdutosVendDevol) + 1);
          vProdutosVendDevol[High(vProdutosVendDevol)].Posicao := i;

          quantidade := FItensDevolucoes[j].QuantidadeDevolvidosEntregues + FItensDevolucoes[j].QuantidadeDevolvidosPendencias + FItensDevolucoes[j].QuantidadeDevolvSemPrevisao;
          vProdutosVendDevol[High(vProdutosVendDevol)].ProdutoId         := FItensDevolucoes[j].ProdutoId;
          vProdutosVendDevol[High(vProdutosVendDevol)].Nome              := FItensDevolucoes[j].NomeProduto;
          vProdutosVendDevol[High(vProdutosVendDevol)].Marca             := FItensDevolucoes[j].NomeMarca;
          vProdutosVendDevol[High(vProdutosVendDevol)].PrecoUnitario     := FItensDevolucoes[j].PrecoUnitario;
          vProdutosVendDevol[High(vProdutosVendDevol)].Quantidade        := quantidade;
          vProdutosVendDevol[High(vProdutosVendDevol)].Unidade           := FItensDevolucoes[j].UnidadeVenda;
          vProdutosVendDevol[High(vProdutosVendDevol)].ValorTotal        := FItensDevolucoes[j].ValorBruto;
          vProdutosVendDevol[High(vProdutosVendDevol)].OutrasDespesas    := FItensDevolucoes[j].ValorOutrasDespesas;
          vProdutosVendDevol[High(vProdutosVendDevol)].Desconto          := FItensDevolucoes[j].ValorDesconto;
          
          if Sessao.AutorizadoRotina('visualizar_lucro_venda') then begin
            vProdutosVendDevol[High(vProdutosVendDevol)].ValorLucroBruto   := FItensDevolucoes[j].ValorLiquido - (FItensDevolucoes[j].CustoEntrada * quantidade);
            vProdutosVendDevol[High(vProdutosVendDevol)].ValorLucroLiquido := FItensDevolucoes[j].ValorLiquido - (FItensDevolucoes[j].ValorCustoFinal * quantidade);
            vProdutosVendDevol[High(vProdutosVendDevol)].PercLucroBruto    := Sessao.getCalculosSistema.CalcOrcamento.getPercLucroBruto(FItensDevolucoes[j].ValorLiquido, FItensDevolucoes[j].ValorLiquido - (FItensDevolucoes[j].CustoEntrada * quantidade), 4);
            vProdutosVendDevol[High(vProdutosVendDevol)].PercLucroLiquido  := Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(FItensDevolucoes[j].ValorLiquido, FItensDevolucoes[j].ValorLiquido - (FItensDevolucoes[j].ValorCustoFinal * quantidade), 4);
          end;
        end;
      end;
    end;

    _RelacaoOrcamentosVendas.GravarTabelaTemporariaItens(Sessao.getConexaoBanco, vVendDevol, vProdutosVendDevol);

    qVendasDevolucoes.Close;
    qVendasDevolucoes.Open;

    qVendasDevolucoesProdutos.Close;
    qVendasDevolucoesProdutos.Open;

    TfrxMemoView(frxReportAnalitico.FindComponent('mmValorProduto')).Text :=   'Valor Produto.....: ' + sgOrcamentos.Cells[coValorProdutos,sgOrcamentos.RowCount - 1];
    TfrxMemoView(frxReportAnalitico.FindComponent('mmValorTotal')).Text   :=   'Valor Total.......: ' + sgOrcamentos.Cells[coValorTotal,sgOrcamentos.RowCount - 1];
    TfrxMemoView(frxReportAnalitico.FindComponent('mmOutrasDespesas')).Text := 'Outras Despesas...: ' + sgOrcamentos.Cells[coValorOutDesp,sgOrcamentos.RowCount - 1];
    TfrxMemoView(frxReportAnalitico.FindComponent('mmDescontos')).Text   :=    'Descontos.........: ' + sgOrcamentos.Cells[coValorDesconto,sgOrcamentos.RowCount - 1];
    
    if Sessao.AutorizadoRotina('visualizar_lucro_venda') then begin
      TfrxMemoView(frxReportAnalitico.FindComponent('mmValorBruto')).Text :=     'Valor Bruto.......: ' + sgOrcamentos.Cells[coValorLucroBruto,sgOrcamentos.RowCount - 1];
      TfrxMemoView(frxReportAnalitico.FindComponent('mmPercLucroBruto')).Text := '% Lucro Bruto.....: ' + sgOrcamentos.Cells[coPercLucroBruto,sgOrcamentos.RowCount - 1];
      TfrxMemoView(frxReportAnalitico.FindComponent('mmValorLiquido')).Text   := 'Valor Liquido.....: ' + sgOrcamentos.Cells[coValorLucroLiq,sgOrcamentos.RowCount - 1];
      TfrxMemoView(frxReportAnalitico.FindComponent('mmPercLucroLiq')).Text   := '% Lucro Liquido...: ' + sgOrcamentos.Cells[coPercLucroLiquido,sgOrcamentos.RowCount - 1];
    end;
    
    frxReportAnalitico.ShowReport;
  end else
  begin
    vDados := nil;
    vUltimoVendedorId := -1;
    for i := 1 to sgProdutos.RowCount -1 do begin
  //    if sgProdutos.Cells[coTipoLinha, i] = clLinhaAgrupamento then begin
  //      if vUltimoVendedorId <> SFormatInt(sgProdutos.Cells[coVendedorId, i]) then begin
  //        SetLength(vDados, Length(vDados) + 1);
  //        vDados[High(vDados)].Vendedor := sgProdutos.Cells[coProdutoid, i];
  //
  //        vUltimoVendedorId := SFormatInt(sgProdutos.Cells[coVendedorId, i]);
  //
  //        Continue;
  //      end;
  //    end;

      // Se for a linha de totalizacao
  //    if sgProdutos.Cells[coTipoLinha, i] = clLinhaTotVendedor then begin
  //      vDados[High(vDados)].QtdeTotal    := SFormatDouble(sgProdutos.Cells[coQtdVendida, i]) - SFormatDouble(sgProdutos.Cells[coQtdDevolvida, i]);
  //      vDados[High(vDados)].TotalLiquido := SFormatDouble(sgProdutos.Cells[coValorVenda, i]) - SFormatDouble(sgProdutos.Cells[coValorDevolucoes, i]);
  //
  //      Continue;
  //    end;

  //    if sgProdutos.Cells[coTipoLinha, i] <> clLinhaDetalhe then
  //      Continue;

      SetLength(vDados[High(vDados)].Produtos, Length(vDados[High(vDados)].Produtos) + 1);
      vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].ProdutoId    := SFormatInt(sgProdutos.Cells[cpProdutoId, i]);
      vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].NomeProduto  := sgProdutos.Cells[cpNome, i];
      vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].NomeMarca    := sgProdutos.Cells[cpMarca, i];
      vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].Quantidade   := SFormatDouble(sgProdutos.Cells[cpQuantidade, i]) {- SFormatDouble(sgProdutos.Cells[coQtdDevolvida, i])};
      vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].Unidade      := sgProdutos.Cells[cpUnidade, i];
      vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].ValorLiquido := SFormatDouble(sgProdutos.Cells[cpValorTotal, i]) {- SFormatDouble(sgProdutos.Cells[coValorDevolucoes, i])};
    end;


    ImpressaoRelacaoVendasProdutosGrafico.Imprimir(Self, vOpcao.Dados, vDados, FFiltrosUtilizados);

  end;


end;

procedure TFormRelacaoOrcamentosVendas.miAlterarIndiceDescontoVendaClick(Sender: TObject);
var
  vOrcamentoId: Integer;
  vIndiceDescId: Integer;
  vRetTela: TRetornoTelaFinalizar<Integer>;
begin
  inherited;
  if not Sessao.AutorizadoRotina('definir_indice_desconto_venda') then begin
    _Biblioteca.NaoAutorizadoRotina;
    Exit;
  end;

  vOrcamentoId  := SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]);
  vIndiceDescId := SFormatInt(sgOrcamentos.Cells[coIndiceDescVenda, sgOrcamentos.Row]);

  vRetTela := BuscarIndiceDescontoVenda.Buscar(vOrcamentoId, 'ORC', vIndiceDescId);

  if vRetTela.BuscaCancelada then
    Exit;

  sgOrcamentos.Cells[coIndiceDescVenda, sgOrcamentos.Row] := NFormat(vIndiceDescId);
  _Biblioteca.RotinaSucesso;
end;

procedure TFormRelacaoOrcamentosVendas.miCancelarFechamentoPedidoClick(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if not Sessao.AutorizadoRotina('cancelar_fechamento_pedido') then begin
    _Biblioteca.NaoAutorizadoRotina;
    Exit;
  end;

  if not _Biblioteca.Perguntar('Deseja voltar esta venda para or�amento?') then
    Exit;

  vRetBanco := _Orcamentos.CancelarFechamentoPedido(Sessao.getConexaoBanco, SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]));
  Sessao.AbortarSeHouveErro( vRetBanco );

  _Biblioteca.Informar('Cancelamento de fechamento realizado com sucesso.');
  Carregar(Sender);
end;

procedure TFormRelacaoOrcamentosVendas.miGerarPDFdooramentovendaFocadaClick(Sender: TObject);
begin
  inherited;
  if cbTipoOrcamento.GetValor = 'AMB' then
      Impressao.OrcamentoGraficoAmbiente.Imprimir(
        SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]),
        FProdutosAmbiente,
        True,
        False
      )
  else
    Impressao.OrcamentoGrafico.Imprimir( SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]), True );
end;

procedure TFormRelacaoOrcamentosVendas.miReimprimirComprovantePagamentoClick(Sender: TObject);
begin
  inherited;
  Impressao.ComprovantePagamentoGrafico.Imprimir( SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]) );
end;

procedure TFormRelacaoOrcamentosVendas.miReimprimirConfissaoDividaClick(Sender: TObject);
begin
  inherited;
  ImpressaoConfissaoDivida.Imprimir( SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]) );
end;

procedure TFormRelacaoOrcamentosVendas.miReimprimirDuplicataMercantilClick(Sender: TObject);
begin
  inherited;
  ImpressaoMeiaPaginaDuplicataMercantil.Emitir( SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]), tpOrcamento );
end;

procedure TFormRelacaoOrcamentosVendas.miReimprimirOrcamentoClick(Sender: TObject);
begin
  inherited;
  if cbTipoOrcamento.GetValor = 'AMB' then
      Impressao.OrcamentoGraficoAmbiente.Imprimir(
        SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]),
        FProdutosAmbiente,
        False,
        False
      )
  else
    Impressao.OrcamentoGrafico.Imprimir( SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]) );
end;

procedure TFormRelacaoOrcamentosVendas.sgOrcamentosClick(Sender: TObject);
var
  i: Integer;
  j: Integer;
  k: Integer;
  vLinha: Integer;
  quantidade: Double;
  posProduto: Integer;
begin
  inherited;
  sgProdutos.ClearGrid;
  if sgOrcamentos.Cells[coOrcamentoId, 1] = '' then
    Exit;

  if cbTipoOrcamento.GetValor = 'AMB' then begin
    //OR�AMENTOS AMBIENTE
    FProdutosAmbiente := _OrcamentosItens.BuscarOrcamentosItensAmbiente(
      Sessao.getConexaoBanco,
      SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row])
    );

    vLinha := 0;
    for i := Low(FProdutosAmbiente) to High(FProdutosAmbiente) do begin
      Inc(vLinha);
      sgProdutos.Cells[cpNome, vLinha] := FProdutosAmbiente[i].NomeAmbiente;
      sgProdutos.Cells[cpAmbiente, vLinha] := 'S';

      for j := Low(FProdutosAmbiente[i].Produtos) to High(FProdutosAmbiente[i].Produtos) do begin
        Inc(vLinha);

        posProduto := -1;
        for k := Low(FProdutosOrcamentos) to High(FProdutosOrcamentos) do begin
          if FProdutosOrcamentos[k].produto_id = FProdutosAmbiente[i].Produtos[j].ProdutoId then begin
            posProduto := k;
            Break;
          end;
        end;

        sgProdutos.Cells[cpProdutoId, vLinha]         := NFormat(FProdutosOrcamentos[k].produto_id);
        sgProdutos.Cells[cpNome, vLinha]              := FProdutosOrcamentos[k].nome;
        sgProdutos.Cells[cpMarca, vLinha]             := FProdutosOrcamentos[k].nome_marca;
        sgProdutos.Cells[cpPrecoUnitario, vLinha]     := NFormat(FProdutosOrcamentos[k].preco_unitario);
        sgProdutos.Cells[cpQuantidade, vLinha]        := NFormatEstoque(FProdutosAmbiente[i].Produtos[j].Quantidade);
        sgProdutos.Cells[cpUnidade, vLinha]           := FProdutosOrcamentos[k].unidade_venda;
        sgProdutos.Cells[cpValorTotal, vLinha]        := NFormat(Arredondar(FProdutosAmbiente[i].Produtos[j].Quantidade * FProdutosOrcamentos[k].preco_unitario, 2));
        sgProdutos.Cells[cpValorLucroBruto, vLinha]   := NFormat(FProdutosOrcamentos[k].ValorLiquido - FProdutosOrcamentos[k].CustoEntrada);
        sgProdutos.Cells[cpValorLucroLiquido, vLinha] := NFormat(FProdutosOrcamentos[k].ValorLiquido - FProdutosOrcamentos[k].ValorCustoFinal);
        sgProdutos.Cells[cpPercLucroBruto, vLinha]    := NFormatN(Sessao.getCalculosSistema.CalcOrcamento.getPercLucroBruto(FProdutosOrcamentos[k].ValorLiquido, FProdutosOrcamentos[k].ValorLiquido - FProdutosOrcamentos[k].CustoEntrada, 4), 4);
        sgProdutos.Cells[cpPercLucroLiquido, vLinha]  := NFormatN(Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(FProdutosOrcamentos[k].ValorLiquido, FProdutosOrcamentos[k].ValorLiquido - FProdutosOrcamentos[k].ValorCustoFinal, 4), 4);
      end;
    end;
  end
  else begin
    //OR�AMENTOS NORMAIS
    vLinha := 0;
    if sgOrcamentos.Cells[coTipoMovimento, sgOrcamentos.Row] = 'VEN' then begin
      for i := Low(FProdutosOrcamentos) to High(FProdutosOrcamentos) do begin
        if SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]) <> FProdutosOrcamentos[i].orcamento_id then
          Continue;

        Inc(vLinha);

        sgProdutos.Cells[cpProdutoId, vLinha]         := NFormat(FProdutosOrcamentos[i].produto_id);
        sgProdutos.Cells[cpNome, vLinha]              := FProdutosOrcamentos[i].nome;
        sgProdutos.Cells[cpMarca, vLinha]             := FProdutosOrcamentos[i].nome_marca;
        sgProdutos.Cells[cpPrecoUnitario, vLinha]     := NFormat(FProdutosOrcamentos[i].preco_unitario);
        sgProdutos.Cells[cpQuantidade, vLinha]        := NFormatEstoque(FProdutosOrcamentos[i].Quantidade);
        sgProdutos.Cells[cpUnidade, vLinha]           := FProdutosOrcamentos[i].unidade_venda;
        sgProdutos.Cells[cpValorTotal, vLinha]        := NFormat(FProdutosOrcamentos[i].valor_total);
        sgProdutos.Cells[cpValorLucroBruto, vLinha]   := NFormat(FProdutosOrcamentos[i].ValorLiquido - FProdutosOrcamentos[i].CustoEntrada);
        sgProdutos.Cells[cpValorLucroLiquido, vLinha] := NFormat(FProdutosOrcamentos[i].ValorLiquido - FProdutosOrcamentos[i].ValorCustoFinal);
        sgProdutos.Cells[cpPercLucroBruto, vLinha]    := NFormatN(Sessao.getCalculosSistema.CalcOrcamento.getPercLucroBruto(FProdutosOrcamentos[i].ValorLiquido, FProdutosOrcamentos[i].ValorLiquido - FProdutosOrcamentos[i].CustoEntrada, 4), 4);
        sgProdutos.Cells[cpPercLucroLiquido, vLinha]  := NFormatN(Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(FProdutosOrcamentos[i].ValorLiquido, FProdutosOrcamentos[i].ValorLiquido - FProdutosOrcamentos[i].ValorCustoFinal, 4), 4);
      end;
    end
    else begin
      for i := Low(FItensDevolucoes) to High(FItensDevolucoes) do begin
        if SFormatInt(sgOrcamentos.Cells[coDevolucaoId, sgOrcamentos.Row]) <> FItensDevolucoes[i].DevolucaoId then
          Continue;

        Inc(vLinha);

        quantidade := FItensDevolucoes[i].QuantidadeDevolvidosEntregues + FItensDevolucoes[i].QuantidadeDevolvidosPendencias + FItensDevolucoes[i].QuantidadeDevolvSemPrevisao;
        sgProdutos.Cells[cpProdutoId, vLinha]         := NFormat(FItensDevolucoes[i].ProdutoId);
        sgProdutos.Cells[cpNome, vLinha]              := FItensDevolucoes[i].NomeProduto;
        sgProdutos.Cells[cpMarca, vLinha]             := FItensDevolucoes[i].NomeMarca;
        sgProdutos.Cells[cpPrecoUnitario, vLinha]     := NFormat(FItensDevolucoes[i].PrecoUnitario);
        sgProdutos.Cells[cpQuantidade, vLinha]        := NFormatEstoque(quantidade);
        sgProdutos.Cells[cpUnidade, vLinha]           := FItensDevolucoes[i].UnidadeVenda;
        sgProdutos.Cells[cpValorTotal, vLinha]        := NFormat(FItensDevolucoes[i].ValorBruto);
        sgProdutos.Cells[cpValorLucroBruto, vLinha]   := NFormat(FItensDevolucoes[i].ValorLiquido - (FItensDevolucoes[i].CustoEntrada * quantidade));
        sgProdutos.Cells[cpValorLucroLiquido, vLinha] := NFormat(FItensDevolucoes[i].ValorLiquido - (FItensDevolucoes[i].ValorCustoFinal * quantidade));
        sgProdutos.Cells[cpPercLucroBruto, vLinha]    := NFormatN(Sessao.getCalculosSistema.CalcOrcamento.getPercLucroBruto(FItensDevolucoes[i].ValorLiquido, FItensDevolucoes[i].ValorLiquido - (FItensDevolucoes[i].CustoEntrada * quantidade), 4), 4);
        sgProdutos.Cells[cpPercLucroLiquido, vLinha]  := NFormatN(Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(FItensDevolucoes[i].ValorLiquido, FItensDevolucoes[i].ValorLiquido - (FItensDevolucoes[i].ValorCustoFinal * quantidade), 4), 4);
      end;
    end;
  end;

  sgProdutos.SetLinhasGridPorTamanhoVetor( vLinha );
end;

procedure TFormRelacaoOrcamentosVendas.sgOrcamentosDblClick(Sender: TObject);
begin
  inherited;
  if sgOrcamentos.Cells[coTipoMovimento, sgOrcamentos.Row] = 'VEN' then
    Informacoes.Orcamento.Informar(SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]))
  else
    Informacoes.Devolucao.Informar(SFormatInt(sgOrcamentos.Cells[coDevolucaoId, sgOrcamentos.Row]));
end;

procedure TFormRelacaoOrcamentosVendas.sgOrcamentosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coOrcamentoId, coDevolucaoId, coValorProdutos, coValorOutDesp, coValorFrete, coValorDesconto, coValorTotal, coValorLucroBruto, coValorLucroLiq, coPercLucroBruto, coPercLucroLiquido] then
    vAlinhamento := taRightJustify
  else if ACol in[coTipoMovimentoAnalitico] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgOrcamentos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoOrcamentosVendas.sgOrcamentosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coPercLucroBruto then
    AFont.Color := _Sessao.Sessao.getCorLucro( SFormatDouble( sgOrcamentos.Cells[coPercLucroBruto, ARow] ) )
  else if ACol = coPercLucroLiquido then
    AFont.Color := _Sessao.Sessao.getCorLucro( SFormatDouble( sgOrcamentos.Cells[coPercLucroLiquido, ARow] ) )
  else if ACol = coTipoMovimentoAnalitico then begin
    if sgOrcamentos.Cells[coTipoMovimento, ARow] = 'DEV' then begin
      AFont.Color := clRed;
      AFont.Style  := [fsBold];
    end
    else begin
      AFont.Color := clGreen;
      AFont.Style  := [fsBold];
    end;
  end;

  if sgOrcamentos.Cells[coTipoLinha, ARow] = coLinhaTotVenda then begin
    AFont.Color := clWhite;
    AFont.Style := [fsBold];
    ABrush.Color := $000096DB;
  end
  else if sgOrcamentos.Cells[coTipoLinha, ARow] = coLinhaCabAgrup then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end
  else if sgOrcamentos.Cells[coTipoLinha, ARow] = coTotalAgrupador then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao3;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao3;
  end;
end;

procedure TFormRelacaoOrcamentosVendas.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[
    cpProdutoId,
    cpPrecoUnitario,
    cpQuantidade,
    cpValorTotal,
    cpValorLucroBruto,
    cpValorLucroLiquido,
    cpPercLucroBruto,
    cpPercLucroLiquido]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoOrcamentosVendas.sgProdutosGetCellColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgProdutos.Cells[cpAmbiente, ARow] = 'S' then begin
    AFont.Color := clWhite;
    AFont.Style := [fsBold];
    ABrush.Color := $000096DB;
  end;
end;

procedure TFormRelacaoOrcamentosVendas.SpeedButton4Click(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  vDevolucaoId: Integer;
begin
  inherited;
  vDevolucaoId := SFormatInt( sgOrcamentos.Cells[coDevolucaoId, sgOrcamentos.Row] );
  if vDevolucaoId = 0 then
    Exit;

  vRetBanco := _Devolucoes.CancelarDevolucao( Sessao.getConexaoBanco, vDevolucaoId );
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar( Sender );
end;

procedure TFormRelacaoOrcamentosVendas.SpeedButton5Click(Sender: TObject);
var
  vDevolucaoId: Integer;
begin
  inherited;
  vDevolucaoId := SFormatInt( sgOrcamentos.Cells[coDevolucaoId, sgOrcamentos.Row] );
  if vDevolucaoId = 0 then
    Exit;

  ImpressaoComprovanteDevolucaoGrafico.Imprimir(vDevolucaoId);
end;

procedure TFormRelacaoOrcamentosVendas.SpeedButton6Click(Sender: TObject);
begin
  inherited;
  Impressao.ComprovanteFaturamentoVenda.Imprimir(SFormatInt( sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row] ));
end;

procedure TFormRelacaoOrcamentosVendas.SpeedButton7Click(Sender: TObject);
var
  vDevolucaoId: Integer;
  vOrcamentoId: Integer;
  i: Integer;
  produtoId: Integer;
  vItens: TArray<RecOrcamentoItens>;
  vItensVenda: TArray<RecProdutosVendas>;
  vClienteId: Integer;
  vCondicaoId: Integer;
  vEmpresaId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if SFormatInt(sgOrcamentos.Cells[coDevolucaoId, sgOrcamentos.Row]) > 0 then begin
    vDevolucaoId := SFormatInt(sgOrcamentos.Cells[coDevolucaoId, sgOrcamentos.Row]);

    for i := Low(FItensDevolucoes) to High(FItensDevolucoes) do begin
      if SFormatInt(sgOrcamentos.Cells[coDevolucaoId, sgOrcamentos.Row]) <> FItensDevolucoes[i].DevolucaoId then
        Continue;


    end;

    Exit;
  end
  else if SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]) > 0 then begin
    vOrcamentoId := SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]);
    vClienteId := SFormatInt(sgOrcamentos.Cells[coClienteId, sgOrcamentos.Row]);
    vCondicaoId := SFormatInt(sgOrcamentos.Cells[coCondicaoPagamentoId, sgOrcamentos.Row]);
    vEmpresaId := SFormatInt(sgOrcamentos.Cells[coEmpresaId, sgOrcamentos.Row]);

    for i := Low(FProdutosOrcamentos) to High(FProdutosOrcamentos) do begin
      if SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]) <> FProdutosOrcamentos[i].orcamento_id then
        Continue;

      vItensVenda := _ProdutosVenda.BuscarProdutosVendas(Sessao.getConexaoBanco, 0, [vEmpresaId, FProdutosOrcamentos[i].produto_id], false);

      setLength(vItens, Length(vItens) + 1);
      vItens[High(vItens)].produto_id                  := FProdutosOrcamentos[i].produto_id;
      vItens[High(vItens)].quantidade                  := FProdutosOrcamentos[i].quantidade;
      vItens[High(vItens)].unidade_venda               := vItensVenda[0].unidade_venda;
      vItens[High(vItens)].PrecoManual                 := 0;
      vItens[High(vItens)].preco_unitario              := FProdutosOrcamentos[i].preco_unitario;
      vItens[High(vItens)].PrecoBase                   := FProdutosOrcamentos[i].preco_unitario;
      vItens[High(vItens)].ValorImpostos               := FProdutosOrcamentos[i].ValorImpostos;
      //vItens[High(vItens)].CustoEntrada                := FProdutosOrcamentos[i].CustoEntrada;

      vItens[High(vItens)].valor_total_outras_despesas := FProdutosOrcamentos[i].valor_total_outras_despesas;
      vItens[High(vItens)].valor_total_desconto        := FProdutosOrcamentos[i].valor_total_desconto;
      vItens[High(vItens)].ValorTotalFrete             := 0;
      vItens[High(vItens)].ValorDescUnitPrecoManual    := 0;

      vItens[High(vItens)].valor_total                 := FProdutosOrcamentos[i].valor_total;
      vItens[High(vItens)].Estoque                     := 1;

      vItens[High(vItens)].PrecoVarejoBasePrecoManual := vItens[High(vItens)].preco_unitario;
    end;


    MixVendas.Mix(
      coClienteId,
      '',
      vCondicaoId,
      '',
      vItens,
      False,
      True
    );

    vRetBanco := _Orcamentos.AtualizarCustoProdutosOrcamento(Sessao.getConexaoBanco, vItens, vOrcamentoId);

    Sessao.AbortarSeHouveErro( vRetBanco );
    _Biblioteca.RotinaSucesso;
    Carregar(Sender);
  end;
end;

procedure TFormRelacaoOrcamentosVendas.SpeedButton8Click(Sender: TObject);
var
  vOrcamentoId: Integer;
  vParceiroId: Integer;
  vRetornoRec: TRetornoTelaFinalizar<RecProfissionais>;
  vRetorno: RecRetornoBD;
begin
  inherited;

  vOrcamentoId := SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]);
  if vOrcamentoId = 0 then
    Exit;

  vParceiroId := SFormatInt(sgOrcamentos.Cells[cpParceiroId, sgOrcamentos.Row]);

  vRetornoRec := BuscarDadosParceiro.BuscarDadosParceiroVenda(vParceiroId);
  if vRetornoRec.RetTela = trCancelado then begin
    RotinaCanceladaUsuario;
    Exit;
  end;

  try
    vRetorno := _Orcamentos.AlterarParceiroVenda(
      Sessao.getConexaoBanco,
      vOrcamentoId,
      vRetornoRec.Dados.CadastroId
    );

    if vRetorno.TeveErro then
      Exclamar(vRetorno.MensagemErro)
    else
      RotinaSucesso;
  finally
    Carregar(Sender);
  end;
end;

procedure TFormRelacaoOrcamentosVendas.SpeedButton9Click(Sender: TObject);
var
  vDevolucaoId: Integer;
  vOrcamentoId: Integer;
  i: Integer;
  produtoId: Integer;
  vItens: TArray<RecOrcamentoItens>;
  vItensVenda: TArray<RecProdutosVendas>;
  vClienteId: Integer;
  vCondicaoId: Integer;
  vEmpresaId: Integer;
  vRetBanco: RecRetornoBD;
  j: Integer;
  l: Integer;
begin
  inherited;

  for j := 1 to sgOrcamentos.RowCount - 1 do begin
    if sgOrcamentos.Cells[coDevolucaoId, j] <> '' then
      Continue;

    if sgOrcamentos.Cells[coOrcamentoId, j] = '' then
      Continue;


    vOrcamentoId := SFormatInt(sgOrcamentos.Cells[coOrcamentoId, j]);
    vClienteId := SFormatInt(sgOrcamentos.Cells[coClienteId, j]);
    vCondicaoId := SFormatInt(sgOrcamentos.Cells[coCondicaoPagamentoId, j]);
    vEmpresaId := SFormatInt(sgOrcamentos.Cells[coEmpresaId, j]);

    vItens := nil;
    for i := Low(FProdutosOrcamentos) to High(FProdutosOrcamentos) do begin
      if vOrcamentoId <> FProdutosOrcamentos[i].orcamento_id then
        Continue;

      vItensVenda := _ProdutosVenda.BuscarProdutosVendas(Sessao.getConexaoBanco, 0, [vEmpresaId, FProdutosOrcamentos[i].produto_id], false);

      if vItensVenda = nil then
        Continue;

      setLength(vItens, Length(vItens) + 1);
      vItens[High(vItens)].produto_id                  := FProdutosOrcamentos[i].produto_id;
      vItens[High(vItens)].quantidade                  := FProdutosOrcamentos[i].quantidade;
      vItens[High(vItens)].unidade_venda               := vItensVenda[0].unidade_venda;
      vItens[High(vItens)].PrecoManual                 := 0;
      vItens[High(vItens)].preco_unitario              := FProdutosOrcamentos[i].preco_unitario;
      vItens[High(vItens)].PrecoBase                   := FProdutosOrcamentos[i].preco_unitario;
      vItens[High(vItens)].ValorImpostos               := FProdutosOrcamentos[i].ValorImpostos;
      //vItens[High(vItens)].CustoEntrada                := FProdutosOrcamentos[i].CustoEntrada;

      vItens[High(vItens)].valor_total_outras_despesas := FProdutosOrcamentos[i].valor_total_outras_despesas;
      vItens[High(vItens)].valor_total_desconto        := FProdutosOrcamentos[i].valor_total_desconto;
      vItens[High(vItens)].ValorTotalFrete             := 0;
      vItens[High(vItens)].ValorDescUnitPrecoManual    := 0;

      vItens[High(vItens)].valor_total                 := FProdutosOrcamentos[i].valor_total;
      vItens[High(vItens)].Estoque                     := 1;

      vItens[High(vItens)].PrecoVarejoBasePrecoManual := vItens[High(vItens)].preco_unitario;
    end;


    if vItens <> nil then begin
      MixVendas.Mix(
        coClienteId,
        '',
        vCondicaoId,
        '',
        vItens,
        False,
        True
      );

      vRetBanco := _Orcamentos.AtualizarCustoProdutosOrcamento(Sessao.getConexaoBanco, vItens, vOrcamentoId);

      Sessao.AbortarSeHouveErro( vRetBanco );
    end;
  end;

  _Biblioteca.RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoOrcamentosVendas.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if not (FrCodigoOrcamento.EstaVazio) or (not FrCodigoDevolucao.EstaVazio) then
    Exit;

  if gbFormasPagamento.NenhumMarcado then begin
    _Biblioteca.Exclamar('� neces�rio informar ao menos 1 forma de pagamento!');
    SetarFoco(gbFormasPagamento);
    Abort;
  end;

  if gbStatus.NenhumMarcado then begin
    _Biblioteca.Exclamar('� neces�rio informar ao menos 1 um status para pesquisa!');
    SetarFoco(ckVenda);
    Abort;
  end;
end;

end.
