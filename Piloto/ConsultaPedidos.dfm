inherited FormConsultaPedidos: TFormConsultaPedidos
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Consulta de pedidos'
  ClientHeight = 479
  ClientWidth = 740
  ExplicitWidth = 746
  ExplicitHeight = 508
  PixelsPerInch = 96
  TextHeight = 14
  object lb2: TLabel [1]
    Left = 125
    Top = 49
    Width = 39
    Height = 14
    Caption = 'Cliente'
  end
  object lb12: TLabel [2]
    Left = 424
    Top = 49
    Width = 52
    Height = 14
    Caption = 'Vendedor'
  end
  object lb13: TLabel [3]
    Left = 125
    Top = 96
    Width = 47
    Height = 14
    Caption = 'Empresa'
  end
  object lb3: TLabel [4]
    Left = 634
    Top = 47
    Width = 76
    Height = 14
    Caption = 'Data cadastro'
  end
  object lb4: TLabel [5]
    Left = 600
    Top = 98
    Width = 128
    Height = 14
    Caption = 'Data/hora recebimento'
  end
  object lb6: TLabel [6]
    Left = 129
    Top = 167
    Width = 79
    Height = 14
    Caption = 'Total produtos'
  end
  object Label2: TLabel [7]
    Left = 212
    Top = 185
    Width = 10
    Height = 14
    Caption = '+'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lb7: TLabel [8]
    Left = 225
    Top = 167
    Width = 92
    Height = 14
    Caption = 'Outras despesas'
  end
  object lb10: TLabel [9]
    Left = 321
    Top = 184
    Width = 5
    Height = 14
    Caption = '-'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lb8: TLabel [10]
    Left = 331
    Top = 167
    Width = 51
    Height = 14
    Caption = 'Desconto'
  end
  object lb9: TLabel [11]
    Left = 406
    Top = 185
    Width = 10
    Height = 14
    Caption = '+'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lb23: TLabel [12]
    Left = 418
    Top = 168
    Width = 57
    Height = 14
    Caption = 'Valor frete'
  end
  object lb11: TLabel [13]
    Left = 506
    Top = 184
    Width = 13
    Height = 19
    Caption = '='
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lb5: TLabel [14]
    Left = 525
    Top = 168
    Width = 27
    Height = 14
    Caption = 'Total'
  end
  object sbMixVenda: TSpeedButtonLuka [15]
    Left = 607
    Top = 232
    Width = 114
    Height = 34
    Caption = 'Lucratividade'
    Flat = True
    OnClick = sbMixVendaClick
    TagImagem = 14
    PedirCertificacao = False
    PermitirAutOutroUsuario = False
  end
  object sbInformacoesAcumulado: TSpeedButtonLuka [16]
    Left = 210
    Top = 22
    Width = 18
    Height = 18
    Hint = 'Abrir informa'#231#245'es do acumulado'
    Flat = True
    NumGlyphs = 2
    OnClick = sbInformacoesAcumuladoClick
    TagImagem = 13
    PedirCertificacao = False
    PermitirAutOutroUsuario = False
  end
  object lb18: TLabel [17]
    Left = 335
    Top = 96
    Width = 132
    Height = 14
    Caption = 'Condi'#231#227'o de pagamento'
  end
  object lb24: TLabel [18]
    Left = 247
    Top = 5
    Width = 61
    Height = 14
    Caption = 'Acumulado'
  end
  object SpeedButtonLuka1: TSpeedButtonLuka [19]
    Left = 313
    Top = 22
    Width = 18
    Height = 18
    Hint = 'Abrir informa'#231#245'es do acumulado'
    Flat = True
    NumGlyphs = 2
    OnClick = SpeedButtonLuka1Click
    TagImagem = 13
    PedirCertificacao = False
    PermitirAutOutroUsuario = False
  end
  object lb26: TLabel [20]
    Left = 639
    Top = 168
    Width = 93
    Height = 14
    Caption = 'Total em promoc.'
  end
  inherited pnOpcoes: TPanel
    Height = 479
    ExplicitHeight = 479
    inherited sbGravar: TSpeedButton
      Visible = False
    end
    inherited sbDesfazer: TSpeedButton
      Top = 49
      ExplicitTop = 49
    end
    inherited sbExcluir: TSpeedButton
      Top = 174
      Visible = False
      ExplicitTop = 174
    end
    inherited sbPesquisar: TSpeedButton
      Top = 1
      ExplicitTop = 1
    end
    inherited sbLogs: TSpeedButton
      Top = 234
      ExplicitTop = 234
    end
    object miCancelarFechamentoPedido: TSpeedButton
      Left = 0
      Top = 112
      Width = 117
      Height = 35
      Caption = 'Voltar p/ or'#231'.'
      Flat = True
      OnClick = miCancelarFechamentoPedidoClick
    end
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 667
    Visible = False
    ExplicitLeft = 667
  end
  object eCliente: TEditLuka
    Left = 125
    Top = 63
    Width = 293
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 3
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eVendedor: TEditLuka
    Left = 424
    Top = 63
    Width = 206
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 4
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eEmpresa: TEditLuka
    Left = 125
    Top = 112
    Width = 204
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 5
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object st3: TStaticText
    Left = 555
    Top = 11
    Width = 176
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Situa'#231#227'o do pedido'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 6
    Transparent = False
  end
  object stStatus: TStaticText
    Left = 555
    Top = 26
    Width = 176
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 7
    Transparent = False
  end
  object eDataCadastro: TEditLukaData
    Left = 634
    Top = 63
    Width = 98
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1; '
    MaxLength = 10
    ReadOnly = True
    TabOrder = 8
    Text = '  /  /    '
  end
  object eDataRecebimento: TEditLukaData
    Left = 600
    Top = 112
    Width = 133
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999 99:99:99;1;_'
    MaxLength = 19
    ReadOnly = True
    TabOrder = 9
    Text = '  /  /       :  :  '
    TipoData = tdDataHora
  end
  object stSPC: TStaticText
    Left = 121
    Top = 146
    Width = 616
    Height = 15
    Alignment = taCenter
    AutoSize = False
    Caption = 'Totais do pedido'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 10
    Transparent = False
  end
  object eTotalProdutos: TEditLuka
    Left = 129
    Top = 181
    Width = 79
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 11
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eOutrasDespesas: TEditLuka
    Left = 224
    Top = 181
    Width = 93
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 12
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorDesconto: TEditLuka
    Left = 331
    Top = 181
    Width = 72
    Height = 23
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = '|'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 13
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorFrete: TEditLuka
    Left = 418
    Top = 181
    Width = 85
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 14
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eTotalOrcamento: TEditLuka
    Left = 525
    Top = 181
    Width = 93
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 15
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object StaticText2: TStaticText
    Left = 121
    Top = 214
    Width = 616
    Height = 15
    Alignment = taCenter
    AutoSize = False
    Caption = 'Dados de entrega'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 16
    Transparent = False
  end
  object st1: TStaticText
    Left = 129
    Top = 233
    Width = 119
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Tipo de venda'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 17
    Transparent = False
  end
  object stOrigemVenda: TStaticText
    Left = 129
    Top = 248
    Width = 119
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = 33023
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 18
    Transparent = False
  end
  object StaticText1: TStaticText
    Left = 271
    Top = 233
    Width = 133
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Tipo de entrega'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 19
    Transparent = False
  end
  object stTipoEntrega: TStaticText
    Left = 271
    Top = 248
    Width = 133
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 20
    Transparent = False
  end
  object st2: TStaticText
    Left = 423
    Top = 233
    Width = 128
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Receber na entrega?'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 21
    Transparent = False
  end
  object stReceberNaEntrega: TStaticTextLuka
    Left = 423
    Top = 248
    Width = 128
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 22
    Transparent = False
    AsInt = 0
    TipoCampo = tcSimNao
    CasasDecimais = 0
  end
  object StaticText3: TStaticText
    Left = 121
    Top = 272
    Width = 616
    Height = 15
    Alignment = taCenter
    AutoSize = False
    Caption = 'Produtos'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 23
    Transparent = False
  end
  object sgProdutos: TGridLuka
    Left = 122
    Top = 288
    Width = 615
    Height = 191
    Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
    ColCount = 13
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 24
    OnDrawCell = sgProdutosDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Produto'
      'Nome'
      'Marca'
      'Tipo pr.utilizado'
      'Pre'#231'o unit.'
      'Quantidade'
      'Und.'
      'Valor desc.'
      '% Desc.manual'
      'Vlr.desc.manual'
      'Outras desp.'
      'Valor L'#237'quido'
      'Valor frete')
    OnGetCellColor = sgProdutosGetCellColor
    Grid3D = False
    RealColCount = 13
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      51
      244
      116
      133
      65
      71
      37
      67
      89
      94
      77
      80
      64)
  end
  object eCondicaoPagamento: TEditLuka
    Left = 335
    Top = 112
    Width = 259
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 25
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eAcumuladoId: TEditLuka
    Left = 247
    Top = 19
    Width = 61
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 26
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eTotalProdutosPromocao: TEditLuka
    Left = 639
    Top = 181
    Width = 93
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clGreen
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 27
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
end
