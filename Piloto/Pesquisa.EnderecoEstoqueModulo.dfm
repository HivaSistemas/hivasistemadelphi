inherited FormPesquisaEnderecoEstoqueModulo: TFormPesquisaEnderecoEstoqueModulo
  Caption = 'FormPesquisaEnderecoEstoqueModulo'
  ClientWidth = 458
  ExplicitWidth = 466
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 458
    OnDrawCell = sgPesquisaDrawCell
    OnGetCellColor = sgPesquisaGetCellColor
    ColWidths = (
      28
      55
      324)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Text = '0'
  end
  inherited pnFiltro1: TPanel
    Width = 458
    inherited eValorPesquisa: TEditLuka
      Width = 254
    end
  end
end
