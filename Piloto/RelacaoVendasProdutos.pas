unit RelacaoVendasProdutos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, Vcl.ComCtrls, Vcl.Buttons,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameEmpresas, FrameProdutos, FrameMarcas, Frame.DepartamentosSecoesLinhas, FrameDataInicialFinal, FrameGruposFornecedores,
  FrameFornecedores, Vcl.Grids, GridLuka, Vcl.StdCtrls, StaticTextLuka, _RelacaoVendasProdutos, ImpressaoRelacaoVendasProdutosGrafico, SelecionarVariosOpcoes,
  ComboBoxLuka, FrameVendedores, CheckBoxLuka, ComObj, Clipbrd, FileCtrl,
  FrameClientes;

type
  TFormRelacaoVendasProdutos = class(TFormHerancaRelatoriosPageControl)
    FrEmpresas: TFrEmpresas;
    FrProdutos: TFrProdutos;
    FrMarcas: TFrMarcas;
    FrDepartamentosSecoesLinhas: TFrameDepartamentosSecoesLinhas;
    FrFornecedores: TFrFornecedores;
    FrGruposFornecedores: TFrGruposFornecedores;
    FrDataRecebimento: TFrDataInicialFinal;
    StaticTextLuka1: TStaticTextLuka;
    StaticTextLuka2: TStaticTextLuka;
    sgProdutos: TGridLuka;
    spSeparador: TSplitter;
    sgVendasDevolucoes: TGridLuka;
    lb1: TLabel;
    cbAgrupamento: TComboBoxLuka;
    FrVendedores: TFrVendedores;
    SpeedButton1: TSpeedButton;
    FrClientes: TFrClientes;
    procedure FormCreate(Sender: TObject);
    procedure sgProdutosClick(Sender: TObject);
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgVendasDevolucoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgVendasDevolucoesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgVendasDevolucoesDblClick(Sender: TObject);
    procedure sbImprimirClick(Sender: TObject);
  private
    FVendasProdutos: TArray<RecRelacaoVendasProdutos>;
  protected
    procedure Carregar(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

uses
  _Biblioteca, _Sessao, Informacoes.Orcamento, Informacoes.Devolucao;

const
  {Grid de produtos}
  coProdutoId         = 0;
  coNome              = 1;
  coMarca             = 2;
  coQtdVendida        = 3;
  coQtdDevolvida      = 4;
  coUndVenda          = 5;
  coValorCustoEntrada = 6;
  coValorCustoVenda   = 7;
  coValorVenda        = 8;
  coValorDevolucoes   = 9;
  {Ocultas}
  coVendedorId        = 10;
  coTipoLinha         = 11;

  {Grid de vendas/devolu��es}
  cvTipoMovimento = 0;
  cvMovimentoId   = 1;
  cvDataMovimento = 2;
  cvQuantidade    = 3;
  cvValorCustoCompra = 4;
  cvValorCustoVenda = 5;
  cvValor         = 6;

  {Tipos de linha}
  clLinhaDetalhe     = 'D';
  clLinhaTotVenda    = 'TV';
  clLinhaTotVendedor = 'TVEN';
  clLinhaAgrupamento = 'AGR';

  caAgrupadoVendedor = 'VEN';

procedure TFormRelacaoVendasProdutos.Carregar(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vComando: string;

  vUltimoVendedorId: Integer;

  vProdutoId: Integer;
  vQtdVendida: Double;
  vQtdDevolvida: Double;
  vValorVenda: Double;
  vValorCustoEntrada: Double;
  vValorCustoVenda: Double;
  vValorDevolucoes: Double;
  vTrocouVendedor: Boolean;

  vTotais: record
    TotalQtdVendida: Double;
    TotalQtdDevolvida: Double;
    TotalValorVenda: Double;
    TotalValorDevolucoes: Double;
    TotalValorCustoEntrada: Double;
    TotalValorCustoVenda: Double;
  end;

  vTotaisVendedor: record
    TotalQtdVendida: Double;
    TotalQtdDevolvida: Double;
    TotalValorVenda: Double;
    TotalValorDevolucoes: Double;
    TotalValorCustoEntrada: Double;
    TotalValorCustoVenda: Double;
  end;

begin
  inherited;
  sgProdutos.ClearGrid;
  sgVendasDevolucoes.ClearGrid;

  if not FrEmpresas.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrEmpresas.getSqlFiltros('EMPRESA_ID'));

  if not FrProdutos.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrProdutos.getSqlFiltros('PRODUTO_ID'));

  if not FrMarcas.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrMarcas.getSqlFiltros('MARCA_ID'));

  if not FrDepartamentosSecoesLinhas.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrDepartamentosSecoesLinhas.getSqlFiltros('LINHA_PRODUTO_ID'));

  if not FrFornecedores.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrFornecedores.getSqlFiltros('FORNECEDOR_ID'));

  if not FrGruposFornecedores.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrGruposFornecedores.getSqlFiltros('GRUPO_FORNECEDOR_ID'));

  if not FrDataRecebimento.NenhumaDataValida then
    _Biblioteca.WhereOuAnd(vComando, FrDataRecebimento.getSqlFiltros('DATA_HORA_MOVIMENTO'));

  if not FrVendedores.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrVendedores.getSqlFiltros('VENDEDOR_ID'));

  if not FrClientes.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrClientes.getSqlFiltros('CLIENTE_ID'));

  if cbAgrupamento.GetValor = caAgrupadoVendedor then
    vComando := vComando + 'order by NOME_VENDEDOR, NOME_PRODUTO, DATA_HORA_MOVIMENTO '
  else
    vComando := vComando + 'order by NOME_PRODUTO, DATA_HORA_MOVIMENTO ';

  FVendasProdutos := _RelacaoVendasProdutos.BuscarVendasProdutos(Sessao.getConexaoBanco, vComando);
  if FVendasProdutos = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vLinha           := 0;
  vProdutoId       := 0;
  vQtdVendida      := 0;
  vQtdDevolvida    := 0;
  vValorVenda      := 0;
  vValorDevolucoes := 0;

  vUltimoVendedorId := -1;
  for i := Low(FVendasProdutos) to High(FVendasProdutos) do begin
    vTrocouVendedor := False;
    if cbAgrupamento.GetValor = caAgrupadoVendedor then begin
      if vUltimoVendedorId <> FVendasProdutos[i].VendedorId then begin
        if vUltimoVendedorId > -1 then begin
          sgProdutos.Cells[coQtdVendida, vLinha]      := NFormatN(vQtdVendida);
          sgProdutos.Cells[coQtdDevolvida, vLinha]    := NFormatN(vQtdDevolvida);
          sgProdutos.Cells[coValorCustoEntrada, vLinha] := NFormatN(vValorCustoEntrada);
          sgProdutos.Cells[coValorCustoVenda, vLinha] := NFormatN(vValorCustoVenda);
          sgProdutos.Cells[coValorVenda, vLinha]      := NFormatN(vValorVenda);
          sgProdutos.Cells[coValorDevolucoes, vLinha] := NFormatN(vValorDevolucoes);

          vTotais.TotalQtdVendida      := vTotais.TotalQtdVendida + vQtdVendida;
          vTotais.TotalQtdDevolvida    := vTotais.TotalQtdDevolvida + vQtdDevolvida;
          vTotais.TotalValorCustoEntrada := vTotais.TotalValorCustoEntrada + vValorCustoEntrada;
          vTotais.TotalValorCustoVenda := vTotais.TotalValorCustoVenda + vValorCustoVenda;
          vTotais.TotalValorVenda      := vTotais.TotalValorVenda + vValorVenda;
          vTotais.TotalValorDevolucoes := vTotais.TotalValorDevolucoes + vValorDevolucoes;

          vTotaisVendedor.TotalQtdVendida      := vTotaisVendedor.TotalQtdVendida + vQtdVendida;
          vTotaisVendedor.TotalQtdDevolvida    := vTotaisVendedor.TotalQtdDevolvida + vQtdDevolvida;
          vTotaisVendedor.TotalValorCustoEntrada := vTotaisVendedor.TotalValorCustoEntrada + vValorCustoEntrada;
          vTotaisVendedor.TotalValorCustoVenda := vTotaisVendedor.TotalValorCustoVenda + vValorCustoVenda;
          vTotaisVendedor.TotalValorVenda      := vTotaisVendedor.TotalValorVenda + vValorVenda;
          vTotaisVendedor.TotalValorDevolucoes := vTotaisVendedor.TotalValorDevolucoes + vValorDevolucoes;

          Inc(vLinha);

          sgProdutos.Cells[coMarca, vLinha]           := 'TOT.VENDEDOR:';
          sgProdutos.Cells[coQtdVendida, vLinha]      := NFormatN(vTotaisVendedor.TotalQtdVendida);
          sgProdutos.Cells[coQtdDevolvida, vLinha]    := NFormatN(vTotaisVendedor.TotalQtdDevolvida);
          sgProdutos.Cells[coValorCustoEntrada, vLinha] := NFormatN(vTotaisVendedor.TotalValorCustoEntrada);
          sgProdutos.Cells[coValorCustoVenda, vLinha] := NFormatN(vTotaisVendedor.TotalValorCustoVenda);
          sgProdutos.Cells[coValorVenda, vLinha]      := NFormatN(vTotaisVendedor.TotalValorVenda);
          sgProdutos.Cells[coValorDevolucoes, vLinha] := NFormatN(vTotaisVendedor.TotalValorDevolucoes);
          sgProdutos.Cells[coTipoLinha, vLinha]       := clLinhaTotVendedor;

          vQtdVendida      := 0;
          vQtdDevolvida    := 0;
          vValorCustoEntrada := 0;
          vValorVenda      := 0;
          vValorDevolucoes := 0;
          vValorCustoVenda := 0;

          vTotaisVendedor.TotalQtdVendida      := 0;
          vTotaisVendedor.TotalQtdDevolvida    := 0;
          vTotaisVendedor.TotalValorCustoEntrada := 0;
          vTotaisVendedor.TotalValorVenda      := 0;
          vTotaisVendedor.TotalValorDevolucoes := 0;
          vTotaisVendedor.TotalValorCustoVenda := 0;

          Inc(vLinha);
        end;

        Inc(vLinha);

        vTrocouVendedor   := True;
        vUltimoVendedorId := FVendasProdutos[i].VendedorId;

        sgProdutos.Cells[coProdutoId, vLinha]  := getInformacao(FVendasProdutos[i].VendedorId, FVendasProdutos[i].NomeVendedor);
        sgProdutos.Cells[coTipoLinha, vLinha]  := clLinhaAgrupamento;
        sgProdutos.Cells[coVendedorId, vLinha] := NFormat(FVendasProdutos[i].VendedorId);
      end;
    end;

    if
      (FVendasProdutos[i].ProdutoId <> vProdutoId) or
      ((cbAgrupamento.GetValor = caAgrupadoVendedor) and vTrocouVendedor)
    then begin
      vProdutoId := FVendasProdutos[i].ProdutoId;

      if i > 0 then begin
        sgProdutos.Cells[coQtdVendida, vLinha]      := NFormatN(vQtdVendida);
        sgProdutos.Cells[coQtdDevolvida, vLinha]    := NFormatN(vQtdDevolvida);
        sgProdutos.Cells[coValorCustoEntrada, vLinha] := NFormatN(vValorCustoEntrada);
        sgProdutos.Cells[coValorCustoVenda, vLinha] := NFormatN(vValorCustoVenda);
        sgProdutos.Cells[coValorVenda, vLinha]      := NFormatN(vValorVenda);
        sgProdutos.Cells[coValorDevolucoes, vLinha] := NFormatN(vValorDevolucoes);

        vTotais.TotalQtdVendida      := vTotais.TotalQtdVendida + vQtdVendida;
        vTotais.TotalQtdDevolvida    := vTotais.TotalQtdDevolvida + vQtdDevolvida;
        vTotais.TotalValorCustoEntrada := vTotais.TotalValorCustoEntrada + vValorCustoEntrada;
        vTotais.TotalValorCustoVenda := vTotais.TotalValorCustoVenda + vValorCustoVenda;
        vTotais.TotalValorVenda      := vTotais.TotalValorVenda + vValorVenda;
        vTotais.TotalValorDevolucoes := vTotais.TotalValorDevolucoes + vValorDevolucoes;

        vTotaisVendedor.TotalQtdVendida      := vTotaisVendedor.TotalQtdVendida + vQtdVendida;
        vTotaisVendedor.TotalQtdDevolvida    := vTotaisVendedor.TotalQtdDevolvida + vQtdDevolvida;
        vTotaisVendedor.TotalValorCustoEntrada := vTotaisVendedor.TotalValorCustoEntrada + vValorCustoEntrada;
        vTotaisVendedor.TotalValorCustoVenda := vTotaisVendedor.TotalValorCustoVenda + vValorCustoVenda;
        vTotaisVendedor.TotalValorVenda      := vTotaisVendedor.TotalValorVenda + vValorVenda;
        vTotaisVendedor.TotalValorDevolucoes := vTotaisVendedor.TotalValorDevolucoes + vValorDevolucoes;
      end;

      Inc(vLinha);

      sgProdutos.Cells[coProdutoId, vLinha] := NFormat(vProdutoId);
      sgProdutos.Cells[coNome, vLinha]      := FVendasProdutos[i].NomeProduto;
      sgProdutos.Cells[coMarca, vLinha]     := FVendasProdutos[i].NomeMarca;
      sgProdutos.Cells[coUndVenda, vLinha]  := FVendasProdutos[i].UnidadeVenda;
      sgProdutos.Cells[coTipoLinha, vLinha] := clLinhaDetalhe;

      vQtdVendida      := 0;
      vQtdDevolvida    := 0;
      vValorCustoEntrada := 0;
      vValorVenda      := 0;
      vValorDevolucoes := 0;
      vValorCustoVenda := 0;
    end;

    if FVendasProdutos[i].TipoMovimento = 'VEN' then begin
      vQtdVendida      := vQtdVendida + FVendasProdutos[i].Quantidade;
      vValorVenda      := vValorVenda + FVendasProdutos[i].ValorLiquido;
      vValorCustoEntrada := vValorCustoEntrada + FVendasProdutos[i].ValorCustoEntrada;
      vValorCustoVenda := vValorCustoVenda + FVendasProdutos[i].ValorCustoVenda;
    end
    else begin
      vQtdDevolvida    := vQtdDevolvida + FVendasProdutos[i].Quantidade;
      vValorDevolucoes := vValorDevolucoes + FVendasProdutos[i].ValorLiquido;
      vValorCustoEntrada := vValorCustoEntrada + FVendasProdutos[i].ValorCustoEntrada;
      vValorCustoVenda := vValorCustoVenda + FVendasProdutos[i].ValorCustoVenda;
    end;
  end;

  sgProdutos.Cells[coQtdVendida, vLinha]      := NFormatN(vQtdVendida);
  sgProdutos.Cells[coQtdDevolvida, vLinha]    := NFormatN(vQtdDevolvida);
  sgProdutos.Cells[coValorCustoEntrada, vLinha] := NFormatN(vValorCustoEntrada);
  sgProdutos.Cells[coValorCustoVenda, vLinha] := NFormatN(vValorCustoVenda);
  sgProdutos.Cells[coValorVenda, vLinha]      := NFormatN(vValorVenda);
  sgProdutos.Cells[coValorDevolucoes, vLinha] := NFormatN(vValorDevolucoes);

  Inc(vLinha);

  vTotais.TotalQtdVendida      := vTotais.TotalQtdVendida + vQtdVendida;
  vTotais.TotalQtdDevolvida    := vTotais.TotalQtdDevolvida + vQtdDevolvida;
  vTotais.TotalValorCustoEntrada := vTotais.TotalValorCustoEntrada + vValorCustoEntrada;
  vTotais.TotalValorCustoVenda := vTotais.TotalValorCustoVenda + vValorCustoVenda;
  vTotais.TotalValorVenda      := vTotais.TotalValorVenda + vValorVenda;
  vTotais.TotalValorDevolucoes := vTotais.TotalValorDevolucoes + vValorDevolucoes;

  if cbAgrupamento.GetValor = caAgrupadoVendedor then begin
    sgProdutos.Cells[coMarca, vLinha]           := 'TOT.VENDEDOR:';
    sgProdutos.Cells[coQtdVendida, vLinha]      := NFormatN(vTotaisVendedor.TotalQtdVendida + vQtdVendida);
    sgProdutos.Cells[coQtdDevolvida, vLinha]    := NFormatN(vTotaisVendedor.TotalQtdDevolvida + vQtdDevolvida);
    sgProdutos.Cells[coValorCustoEntrada, vLinha] := NFormatN(vTotaisVendedor.TotalValorCustoEntrada + vValorCustoEntrada);
    sgProdutos.Cells[coValorCustoVenda, vLinha] := NFormatN(vTotaisVendedor.TotalValorCustoVenda + vValorCustoVenda);
    sgProdutos.Cells[coValorVenda, vLinha]      := NFormatN(vTotaisVendedor.TotalValorVenda + vValorVenda);
    sgProdutos.Cells[coValorDevolucoes, vLinha] := NFormatN(vTotaisVendedor.TotalValorDevolucoes + vValorDevolucoes);
    sgProdutos.Cells[coTipoLinha, vLinha]       := clLinhaTotVendedor;


    vTotaisVendedor.TotalQtdVendida      := 0;
    vTotaisVendedor.TotalQtdDevolvida    := 0;
    vTotaisVendedor.TotalValorCustoEntrada := 0;
    vTotaisVendedor.TotalValorCustoVenda := 0;
    vTotaisVendedor.TotalValorVenda      := 0;
    vTotaisVendedor.TotalValorDevolucoes := 0;

    Inc(vLinha);
    Inc(vLinha);
  end;

  sgProdutos.Cells[coMarca, vLinha]           := 'TOTAIS:';
  sgProdutos.Cells[coQtdVendida, vLinha]      := NFormatN(vTotais.TotalQtdVendida);
  sgProdutos.Cells[coQtdDevolvida, vLinha]    := NFormatN(vTotais.TotalQtdDevolvida);
  sgProdutos.Cells[coValorCustoEntrada, vLinha] := NFormatN(vTotais.TotalValorCustoEntrada);
  sgProdutos.Cells[coValorCustoVenda, vLinha] := NFormatN(vTotais.TotalValorCustoVenda);
  sgProdutos.Cells[coValorVenda, vLinha]      := NFormatN(vTotais.TotalValorVenda);
  sgProdutos.Cells[coValorDevolucoes, vLinha] := NFormatN(vTotais.TotalValorDevolucoes);
  sgProdutos.Cells[coTipoLinha, vLinha]       := clLinhaTotVenda;

  Inc(vLinha);

  sgProdutos.RowCount := vLinha;
  sgProdutosClick(nil);
  SetarFoco(sgProdutos);
end;

procedure TFormRelacaoVendasProdutos.FormCreate(Sender: TObject);
begin
  inherited;
  ActiveControl := FrEmpresas.sgPesquisa;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId, False);
end;

procedure TFormRelacaoVendasProdutos.Imprimir(Sender: TObject);
var
  i: Integer;
  vUltimoVendedorId: Integer;
  vDados: TArray<RecDadosImpressao>;
  vOpcao: TRetornoTelaFinalizar<Integer>;
begin
  inherited;

  if cbAgrupamento.GetValor <> caAgrupadoVendedor then begin
    _Biblioteca.ImpressaoNaoDisponivel;
    Exit;
  end;

  vOpcao := SelecionarVariosOpcoes.Selecionar('Tipo relat�rio', ['Sint�tico por vendedor', 'Anal�tico'], 0);
  if vOpcao.BuscaCancelada then
    Exit;

  vDados := nil;
  vUltimoVendedorId := -1;
  for i := 1 to sgProdutos.RowCount -1 do begin
    if sgProdutos.Cells[coTipoLinha, i] = clLinhaAgrupamento then begin
      if vUltimoVendedorId <> SFormatInt(sgProdutos.Cells[coVendedorId, i]) then begin
        SetLength(vDados, Length(vDados) + 1);
        vDados[High(vDados)].Vendedor := sgProdutos.Cells[coProdutoid, i];

        vUltimoVendedorId := SFormatInt(sgProdutos.Cells[coVendedorId, i]);

        Continue;
      end;
    end;

    // Se for a linha de totalizacao
    if sgProdutos.Cells[coTipoLinha, i] = clLinhaTotVendedor then begin
      vDados[High(vDados)].QtdeTotal    := SFormatDouble(sgProdutos.Cells[coQtdVendida, i]) - SFormatDouble(sgProdutos.Cells[coQtdDevolvida, i]);
      vDados[High(vDados)].TotalLiquido := SFormatDouble(sgProdutos.Cells[coValorVenda, i]) - SFormatDouble(sgProdutos.Cells[coValorDevolucoes, i]);

      Continue;
    end;

    if sgProdutos.Cells[coTipoLinha, i] <> clLinhaDetalhe then
      Continue;

    SetLength(vDados[High(vDados)].Produtos, Length(vDados[High(vDados)].Produtos) + 1);
    vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].ProdutoId    := SFormatInt(sgProdutos.Cells[coProdutoId, i]);
    vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].NomeProduto  := sgProdutos.Cells[coNome, i];
    vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].NomeMarca    := sgProdutos.Cells[coMarca, i];
    vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].Quantidade   := SFormatDouble(sgProdutos.Cells[coQtdVendida, i]) - SFormatDouble(sgProdutos.Cells[coQtdDevolvida, i]);
    vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].Unidade      := sgProdutos.Cells[coUndVenda, i];
    vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].ValorLiquido := SFormatDouble(sgProdutos.Cells[coValorVenda, i]) - SFormatDouble(sgProdutos.Cells[coValorDevolucoes, i]);
  end;

  ImpressaoRelacaoVendasProdutosGrafico.Imprimir(Self, vOpcao.Dados, vDados, FFiltrosUtilizados);
end;

procedure TFormRelacaoVendasProdutos.sbImprimirClick(Sender: TObject);
var
 coluna, linha: integer;
 excel: variant;
 valor: string;
 i: Integer;
 dataHoraString: string;
 dataAtual: TDateTime;
 horaAtual: TDateTime;
 caminhoArquivo: string;
begin
  GridToExcel(sgProdutos);
//  try
//    excel := CreateOleObject('Excel.Application');
//    excel.Workbooks.add(1);
//  except
//    Application.MessageBox ('Vers�o do Ms-Excel'+
//    'Incompat�vel','Erro',MB_OK+MB_ICONEXCLAMATION);
//  end;
//
//  try
//    linha := 0;
//    for i := 1 to sgProdutos.RowCount - 1 do begin
//
//      excel.cells[linha + 2, 1] := SFormatInt(sgProdutos.Cells[coProdutoId, i]);
//      excel.cells[linha + 2, 2] := sgProdutos.Cells[coNome, i];
//      excel.cells[linha + 2, 3] := sgProdutos.Cells[coMarca, i];
//      excel.cells[linha + 2, 4].NumberFormat := '#.##0,0000';
//      excel.cells[linha + 2, 4] := SFormatDouble(sgProdutos.Cells[coQtdVendida, i]);
//      excel.cells[linha + 2, 5].NumberFormat := '#.##0,0000';
//      excel.cells[linha + 2, 5] := SFormatDouble(sgProdutos.Cells[coQtdDevolvida, i]);
//      excel.cells[linha + 2, 6] := sgProdutos.Cells[coUndVenda, i];
//      excel.cells[linha + 2, 7].NumberFormat := '#.##0,0000';
//      excel.cells[linha + 2, 7] := SFormatDouble(sgProdutos.Cells[coValorVenda, i]);
//      excel.cells[linha + 2, 8].NumberFormat := '#.##0,0000';
//      excel.cells[linha + 2, 8] := SFormatDouble(sgProdutos.Cells[coValorDevolucoes, i]);
//
//      linha := linha + 1;
//    end;
//
//   //Cabe�alho
//   excel.cells[1, 1] := 'PRODUTO';
//   excel.cells[1, 2] := 'NOME';
//   excel.cells[1, 3] := 'MARCA';
//   excel.cells[1, 4] := 'QTDE. VENDIDA';
//   excel.cells[1, 5] := 'QTDE. DEVOLVIDA';
//   excel.cells[1, 6] := 'UND. VENDA';
//   excel.cells[1, 7] := 'VALOR VENDA';
//   excel.cells[1, 8] := 'VALOR DEVOLU��ES';
//
//   //Cabe�alho negrito
//   for i := 1 to 8 do
//     excel.cells[1, i].Font.Bold := True;
//
//   //Linha abaixo do cabe�alho
//   for i := 1 to 8 do
//     excel.cells[1, i].Borders.LineStyle := 1;
//
//   //Cor de preenchimento cabe�alho
//   for i := 1 to 8 do
//     excel.cells[1, i].Interior.Color := RGB(210, 220, 255);
//
//    excel.columns.AutoFit; // esta linha � para fazer com que o Excel dimencione as c�lulas adequadamente.
//
//    SelectDirectory('Selecione onde ser� gerado o arquivo', '', caminhoArquivo);
//
//    if caminhoArquivo <> '' then begin
//      dataAtual := Date;
//      horaAtual := Time;
//
//      dataHoraString :=
//        StringReplace(DateToStr(dataAtual), '/', '', [rfReplaceAll]) +
//        StringReplace(TimeToStr(horaAtual), ':', '', [rfReplaceAll]);
//
//      caminhoArquivo := caminhoArquivo + '\RELACAO_VENDAS_PRODUTOS_' + dataHoraString + '.xlsx';
//
//      excel.Workbooks[1].SaveAs(caminhoArquivo);
//      Exclamar('Planilha gerada com sucesso!');
//    end;
//
//    excel.visible:=true;
//    excel := NULL;
//  except
//      Application.MessageBox ('Aconteceu um erro desconhecido durante a convers�o'+
//      'da tabela para o Ms-Excel','Erro',MB_OK+MB_ICONEXCLAMATION);
//  end;
end;

procedure TFormRelacaoVendasProdutos.sgProdutosClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
begin
  inherited;
  sgVendasDevolucoes.ClearGrid;

  vLinha := 0;
  for i := Low(FVendasProdutos) to High(FVendasProdutos) do begin
    if FVendasProdutos[i].ProdutoId <> SFormatInt(sgProdutos.Cells[coProdutoId, sgProdutos.Row]) then
      Continue;

    Inc(vLinha);

    sgVendasDevolucoes.Cells[cvTipoMovimento, vLinha] := FVendasProdutos[i].TipoMovimento;
    sgVendasDevolucoes.Cells[cvMovimentoId, vLinha]   := NFormat(FVendasProdutos[i].MovimentoId);
    sgVendasDevolucoes.Cells[cvDataMovimento, vLinha] := DFormatN(FVendasProdutos[i].DataMovimento);
    sgVendasDevolucoes.Cells[cvQuantidade, vLinha]    := NFormatN(FVendasProdutos[i].Quantidade);
    sgVendasDevolucoes.Cells[cvValorCustoCompra, vLinha] := NFormatN(FVendasProdutos[i].ValorCustoEntrada);
    sgVendasDevolucoes.Cells[cvValorCustoVenda, vLinha] := NFormatN(FVendasProdutos[i].ValorCustoVenda);
    sgVendasDevolucoes.Cells[cvValor, vLinha]         := NFormatN(FVendasProdutos[i].ValorLiquido);
  end;

  sgVendasDevolucoes.SetLinhasGridPorTamanhoVetor(vLinha);
end;

procedure TFormRelacaoVendasProdutos.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in [coProdutoId, coQtdVendida, coQtdDevolvida, coValorCustoEntrada, coValorCustoVenda, coValorVenda, coValorDevolucoes] then
    vAlinhamento := taRightJustify
  else if ACol = coUndVenda then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  if sgProdutos.Cells[coTipoLinha, ARow] = clLinhaAgrupamento then
    sgProdutos.MergeCells([ARow, ACol], [ARow, coProdutoId], [ARow, coValorDevolucoes], taLeftJustify, Rect)
  else
    sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoVendasProdutos.sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgProdutos.Cells[coTipoLinha, ARow] = clLinhaAgrupamento then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end
  else if sgProdutos.Cells[coTipoLinha, ARow] = clLinhaTotVendedor then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao1;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao1;
  end
  else if sgProdutos.Cells[coTipoLinha, ARow] = clLinhaTotVenda then begin
    AFont.Color := clWhite;
    AFont.Style := [fsBold];
    ABrush.Color := $000096DB;
  end
  else if ACol in [coQtdVendida, coValorVenda] then
    AFont.Color := clBlue
  else if ACol in [coQtdDevolvida, coValorDevolucoes] then
    AFont.Color := clRed;
end;

procedure TFormRelacaoVendasProdutos.sgVendasDevolucoesDblClick(Sender: TObject);
begin
  inherited;
  if sgVendasDevolucoes.Cells[cvTipoMovimento, sgVendasDevolucoes.Row] = 'VEN' then
    Informacoes.Orcamento.Informar(SFormatInt(sgVendasDevolucoes.Cells[cvMovimentoId, sgVendasDevolucoes.Row]))
  else
    Informacoes.Devolucao.Informar(SFormatInt(sgVendasDevolucoes.Cells[cvMovimentoId, sgVendasDevolucoes.Row]));
end;

procedure TFormRelacaoVendasProdutos.sgVendasDevolucoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in [cvMovimentoId, cvQuantidade, cvValor, cvValorCustoCompra, cvValorCustoVenda] then
    vAlinhamento := taRightJustify
  else if ACol = cvTipoMovimento then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgVendasDevolucoes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoVendasProdutos.sgVendasDevolucoesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = cvTipoMovimento then begin
    AFont.Style := [fsBold];
    AFont.Color := IIf(sgVendasDevolucoes.Cells[cvTipoMovimento, ARow] = 'VEN', clBlue, clRed);
  end;
end;

end.
