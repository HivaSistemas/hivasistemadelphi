unit BuscarDadosClienteEmissaoNota;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, Frame.Cadastros,
  _RecordsCadastros, _Biblioteca, Vcl.StdCtrls, FrameClientes;

type
  TFormBuscarDadosClienteEmissaoNota = class(TFormHerancaFinalizar)
    FrCliente: TFrClientes;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function BuscarDadosCliente(caption: string = ''): TRetornoTelaFinalizar<RecClientes>;

implementation

{$R *.dfm}

function BuscarDadosCliente(caption: string = ''): TRetornoTelaFinalizar<RecClientes>;
var
  vForm: TFormBuscarDadosClienteEmissaoNota;
begin
  vForm := TFormBuscarDadosClienteEmissaoNota.Create(Application);
  if (caption <> '') then
    vForm.Caption := caption;

  if Result.Ok(vForm.ShowModal) then
    Result.Dados := vForm.FrCliente.getCliente(0);

  FreeAndNil(vForm);
end;

end.
