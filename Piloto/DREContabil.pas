unit DREContabil;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatorios, Vcl.StdCtrls,
  CheckBoxLuka, Vcl.Buttons, Vcl.ExtCtrls, Data.DB, Vcl.Grids, Vcl.DBGrids,
  frxClass, frxDBSet, DBAccess, Ora, MemDS, _Sessao, OraCall, GridLuka,
  RadioGroupLuka, GroupBoxLuka, _FrameHerancaPrincipal, _FrameHenrancaPesquisas,
  FrameEmpresas, Vcl.Samples.Spin, FramePlanosFinanceiros, Vcl.ComCtrls;

type
  TFrmDREContabil = class(TFormHerancaRelatorios)
    qryDre: TOraQuery;
    frxDre: TfrxReport;
    frxDbDre: TfrxDBDataset;
    stgrDre: TGridLuka;
    gbMeses: TGroupBoxLuka;
    ckFevereiro: TCheckBoxLuka;
    ckMarco: TCheckBoxLuka;
    ckAbril: TCheckBoxLuka;
    speAno: TSpinEdit;
    ckMaio: TCheckBoxLuka;
    ckJunho: TCheckBoxLuka;
    ckJulho: TCheckBoxLuka;
    ckAgosto: TCheckBoxLuka;
    ckSetembro: TCheckBoxLuka;
    ckOutubro: TCheckBoxLuka;
    ckNovembro: TCheckBoxLuka;
    ckDezembro: TCheckBoxLuka;
    Label1: TLabel;
    FrEmpresas: TFrEmpresas;
    ckJaneiro: TCheckBoxLuka;
    qryGenerico: TOraQuery;
    SpeedButton2: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure sbCarregarClick(Sender: TObject);
    procedure stgrDreGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure FormShow(Sender: TObject);
    procedure stgrDreGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var AAlignment: TAlignment);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmDREContabil: TFrmDREContabil;

implementation

{$R *.dfm}

uses _Biblioteca;

const
  coTipoCampo = 0;
  coDescricao = 1;

procedure TFrmDREContabil.FormCreate(Sender: TObject);
begin
  inherited;
  qryDre.Session := Sessao.getConexaoBanco;
  qryGenerico.Session := Sessao.getConexaoBanco;
end;

procedure TFrmDREContabil.FormShow(Sender: TObject);
begin
  inherited;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId);
end;

procedure TFrmDREContabil.sbCarregarClick(Sender: TObject);
var
  vLinha,
  vColunaSoma,
  vCnt : integer;
  vIdSoma,
  vNomeSoma,
  vIdCabecalho : string;
  vTotal: Double;
  vNomeMes : String;

  procedure AdiconarSomatorio;
  begin
    stgrDre.Cells[coDescricao, vLinha] := vNomeSoma;
    vNomeSoma := qryDre.FieldByName('nomeSoma').AsString;
    stgrDre.Cells[coTipoCampo, vLinha] := 'Soma';
    vIdSoma := qryDre.FieldByName('idSoma').AsString;

    inc(vLinha);
    stgrDre.RowCount := stgrDre.RowCount + 1;
  end;

  procedure AdicionarMeses;
  var
    I,vColuna : Integer;
    vMesAno, vNome : String;
  begin
    for I := 0 to gbMeses.ControlCount - 1 do
    begin
      if gbMeses.Controls[I] is TCheckBoxLuka then
      begin
        if TCheckBoxLuka(gbMeses.Controls[I]).Checked then
        begin
          vColuna := stgrDre.ColCount + 1;
          stgrDre.ColCount := vColuna;
          stgrDre.Cells[vColuna -1,0] := TCheckBoxLuka(gbMeses.Controls[I]).Caption;
          stgrDre.ColWidths[vColuna - 1] := 80;
          vMesAno := (I+1).ToString.PadLeft(2,'0') + speAno.Text;
          vNome := TCheckBoxLuka(gbMeses.Controls[I]).Name;
          vNome := copy(vNome,3,length(vNome));

          qryDre.SQL.Add(' (select sum(c.valor_documento) * decode(pf.tipo, ''D'',-1,1) ');
          qryDre.SQL.Add('  from contas_pagar c, contas_pagar_baixas b  ');
          qryDre.SQL.Add('  where c.plano_financeiro_id = pf.plano_financeiro_id  ');
          qryDre.SQL.Add('    and c.empresa_id = ' + FrEmpresas.getEmpresa(0).EmpresaId.ToString.QuotedString);
          qryDre.SQL.Add('    and c.baixa_id = b.baixa_id ');
          qryDre.SQL.Add('    and to_char(c.data_contabil,''mmyyyy'') = '+ vMesAno.QuotedString );
          qryDre.SQL.Add('    and c.status = ''B''  ) as '+ vNome +',' );

        end;
      end;
    end;
  end;

  procedure MontarValoresLinha(pLinha: Integer);
  var
    I,vColuna : Integer;
    vNome : String;
  begin
    for I := 0 to gbMeses.ControlCount - 1 do
    begin
      if gbMeses.Controls[I] is TCheckBoxLuka then
      begin
        if TCheckBoxLuka(gbMeses.Controls[I]).Checked then
        begin
          vNome := TCheckBoxLuka(gbMeses.Controls[I]).Name;
          vNome := copy(vNome,3,length(vNome));

          vColuna := stgrDre.LocalizarColuna(0,TCheckBoxLuka(gbMeses.Controls[I]).Caption);

          stgrDre.Cells[vColuna, pLinha] := NFormat(qryDre.FieldByName(vNome).AsFloat);

        end;
      end;
    end;
  end;

  procedure AtualizarValoresLinha(pSQL: string);
  var
    I: Integer;
    vNome,vMesAno : String;
  begin
    for I := 0 to gbMeses.ControlCount - 1 do
    begin
      if gbMeses.Controls[I] is TCheckBoxLuka then
      begin
        if TCheckBoxLuka(gbMeses.Controls[I]).Checked then
        begin
          vNome := TCheckBoxLuka(gbMeses.Controls[I]).Name;
          vNome := copy(vNome,3,length(vNome));
          vMesAno := (I+1).ToString.PadLeft(2,'0') + speAno.Text;
          qryGenerico.Close;
          qryGenerico.SQL.Clear;
          qryGenerico.SQL.Add(pSql);
          qryGenerico.ParamByName('Empresa').AsString := FrEmpresas.getEmpresa(0).EmpresaId.ToString;
          qryGenerico.ParamByName('MesAno').AsString := vMesAno;
          qryGenerico.Open;

          qryDre.FieldByName(vNome).AsFloat := qryGenerico.Fields[0].AsFloat *
                                               Iif(qryDre.FieldByName('Tipo').AsString = 'D',-1,1);


        end;
      end;
    end;
  end;


begin
  inherited;
  stgrDre.ClearGrid;
  stgrDre.ColCount := 2;
  vIdSoma := '';
  vIdCabecalho:= '';

  stgrDre.Cells[coDescricao, 0] := 'Plano Financeiro';
  vLinha := 1;

  qryDre.Close;
  qryDre.SQL.Clear;
  qryDre.SQL.Add('select gs.grupo_financeiro_id as IdSoma, ');
  qryDre.SQL.Add('       gs.nome                as nomeSoma, ');
  qryDre.SQL.Add('       gf.grupo_financeiro_id as idCabecalho, ');
  qryDre.SQL.Add('       gf.nome                as nomeCabecalho, ');
  qryDre.SQL.Add('       ig.plano_financeiro_id as IdPlano, ');
  qryDre.SQL.Add('       pf.descricao           as nomePlano, ');
  qryDre.SQL.Add('       ig.sql, ');

  AdicionarMeses;

  qryDre.SQL.Add('       pf.tipo ');
  qryDre.SQL.Add('  from grupos_financeiros       gs, ');
  qryDre.SQL.Add('       grupos_financeiros       gf, ');
  qryDre.SQL.Add('       itens_grupos_financeiros ig, ');
  qryDre.SQL.Add('       planos_financeiros       pf ');
  qryDre.SQL.Add(' where gs.grupo_financeiro_id = gf.grupo_financeiro_pai_id ');
  qryDre.SQL.Add('   and gf.grupo_financeiro_id = ig.grupo_financeiro_id ');
  qryDre.SQL.Add('   and ig.plano_financeiro_id = pf.plano_financeiro_id ');
  qryDre.SQL.Add('   and pf.exibir_dre = ''S'' ');
  qryDre.SQL.Add('order by idsoma, idcabecalho, ig.ordem');

  qryDre.Open;
  qryDre.First;
  while not qryDre.Eof do
  begin
    if not qryDre.FieldByName('SQL').IsNull then
    begin
      qryDre.Edit;
      AtualizarValoresLinha(qryDre.FieldByName('SQL').AsString);
      qryDre.Post;
    end;

    qryDre.Next;
  end;
//


  stgrDre.FixedCols := 2;
  qryDre.First;
  vIdSoma := qryDre.FieldByName('idSoma').AsString;
  vNomeSoma := qryDre.FieldByName('nomeSoma').AsString;
  vTotal := 0;
  while not qryDre.Eof do
  begin
    if qryDre.FieldByName('idSoma').AsString <> vIdSoma then
    begin
      AdiconarSomatorio;
    end;

    if qryDre.FieldByName('idCabecalho').AsString <> vIdCabecalho then
    begin
      stgrDre.Cells[coDescricao, vLinha] := qryDre.FieldByName('nomeCabecalho').AsString;
      stgrDre.Cells[coTipoCampo, vLinha] := 'Cabecalho';
      vIdCabecalho := qryDre.FieldByName('idCabecalho').AsString;
      inc(vLinha);
      stgrDre.RowCount := stgrDre.RowCount + 1;
    end;

    stgrDre.Cells[coDescricao, vLinha] := '[ '+ Iif(qryDre.FieldByName('tipo').AsString = 'D','-','+')+' ]  '
                                          + qryDre.FieldByName('nomePlano').AsString;

    MontarValoresLinha(vLinha);

    inc(vLinha);
    stgrDre.RowCount := stgrDre.RowCount + 1;
    qryDre.Next;
  end;
  AdiconarSomatorio;

  stgrDre.FixedRows := 1;
  vLinha := stgrDre.RowCount + 2;
  stgrDre.RowCount := vLinha;
  stgrDre.Cells[coTipoCampo, vLinha-1] := 'Total';
  stgrDre.Cells[coDescricao, vLinha-1] := '(=) RESULTADO L�QUIDO DO EXERC�CIO';

  for vCnt := 0 to gbMeses.ControlCount - 1 do
  begin
    if gbMeses.Controls[vCnt] is TCheckBoxLuka then
    begin
      if TCheckBoxLuka(gbMeses.Controls[vCnt]).Checked then
      begin
        vNomeMes := TCheckBoxLuka(gbMeses.Controls[vCnt]).Name;
        vNomeMes := copy(vNomeMes,3,length(vNomeMes));

        vColunaSoma := stgrDre.LocalizarColuna(0,TCheckBoxLuka(gbMeses.Controls[vCnt]).Caption);
        vTotal := 0;
        for vLinha := 1 to stgrDre.RowCount - 1 do
        begin
          vTotal := vTotal + SFormatDouble(stgrDre.Cells[vColunaSoma,vLinha]);
          if Em(stgrDre.Cells[coTipoCampo,vLinha],['Total', 'Soma']) then
            stgrDre.Cells[vColunaSoma, vLinha] := NFormat(vTotal);
        end;
      end;
    end;
  end;

end;

procedure TFrmDREContabil.SpeedButton2Click(Sender: TObject);
begin
  inherited;
  GridToExcel(stgrDre);
end;

procedure TFrmDREContabil.stgrDreGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var AAlignment: TAlignment);
begin
  inherited;
  if Acol > 1 then
    AAlignment := taRightJustify;
end;

procedure TFrmDREContabil.stgrDreGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if stgrDre.Cells[coTipoCampo, ARow] = 'Cabecalho' then
  begin
    ABrush.Color := clGray;
    AFont.Color  := clBlack;
  end else
  if stgrDre.Cells[coTipoCampo, ARow] = 'Soma' then
  begin
    ABrush.Color := clHighlight;
    AFont.Color  := clWhite;
  end else
  if stgrDre.Cells[coTipoCampo, ARow] = 'Total' then
  begin
    ABrush.Color := $000096DB;
    AFont.Color  := clWhite;
  end else
  if ACol = coDescricao then
  begin
    ABrush.Color := clBtnFace;
    AFont.Color  := clBlack;
  end else
  begin
    ABrush.Color := clWhite;
    AFont.Color  := clBlack;
  end;

end;

end.

