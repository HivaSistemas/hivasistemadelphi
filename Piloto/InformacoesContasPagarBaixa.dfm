﻿inherited FormInformacoesContasPagarBaixa: TFormInformacoesContasPagarBaixa
  Caption = 'Informa'#231#245'es da baixa de contas a pagar'
  ClientHeight = 357
  ClientWidth = 714
  OnShow = FormShow
  ExplicitWidth = 720
  ExplicitHeight = 386
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 320
    Width = 714
    ExplicitTop = 320
    ExplicitWidth = 714
  end
  object pcDados: TPageControl
    Left = 0
    Top = 0
    Width = 714
    Height = 320
    ActivePage = tsGerais
    Align = alClient
    TabOrder = 1
    object tsGerais: TTabSheet
      Caption = 'Gerais'
      object lbllb7: TLabel
        Left = 1
        Top = 3
        Width = 30
        Height = 14
        Caption = 'Baixa'
      end
      object lbllb1: TLabel
        Left = 465
        Top = 3
        Width = 76
        Height = 14
        Caption = 'Usu'#225'rio baixa'
      end
      object lbllb2: TLabel
        Left = 263
        Top = 3
        Width = 91
        Height = 14
        Caption = 'Data pagamento'
      end
      object lbllb3: TLabel
        Left = 364
        Top = 3
        Width = 59
        Height = 14
        Caption = 'Data baixa'
      end
      object lbllb11: TLabel
        Left = 65
        Top = 101
        Width = 51
        Height = 14
        AutoSize = False
        Caption = 'Dinheiro'
      end
      object lbllb12: TLabel
        Left = 159
        Top = 101
        Width = 60
        Height = 14
        AutoSize = False
        Caption = 'Cheque'
      end
      object lbllb13: TLabel
        Left = 253
        Top = 101
        Width = 60
        Height = 14
        AutoSize = False
        Caption = 'Cart'#227'o'
      end
      object lbllb14: TLabel
        Left = 347
        Top = 101
        Width = 60
        Height = 14
        AutoSize = False
        Caption = 'Cobran'#231'a'
      end
      object lbllb5: TLabel
        Left = 441
        Top = 101
        Width = 105
        Height = 14
        AutoSize = False
        Caption = 'Cr'#233'ditos a receber'
      end
      object lbllb4: TLabel
        Left = 1
        Top = 142
        Width = 69
        Height = 14
        Caption = 'Observa'#231#245'es'
      end
      object lbllb8: TLabel
        Left = 59
        Top = 3
        Width = 97
        Height = 14
        Caption = 'Empresa da baixa'
      end
      object lbllb15: TLabel
        Left = 1
        Top = 43
        Width = 68
        Height = 14
        AutoSize = False
        Caption = 'Valor t'#237'tulos'
      end
      object lbllb16: TLabel
        Left = 126
        Top = 43
        Width = 60
        Height = 14
        AutoSize = False
        Caption = 'Valor juros'
        Font.Charset = ANSI_CHARSET
        Font.Color = 253
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lbllb17: TLabel
        Left = 251
        Top = 43
        Width = 88
        Height = 14
        AutoSize = False
        Caption = 'Valor desconto'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lbllb18: TLabel
        Left = 376
        Top = 43
        Width = 79
        Height = 14
        AutoSize = False
        Caption = 'Valor l'#237'quido'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object sbCreditos: TSpeedButton
        Left = 531
        Top = 119
        Width = 15
        Height = 18
        Hint = 'Abrir informa'#231#245'es dos cr'#233'ditos utilizados'
        Caption = '...'
        OnClick = sbCreditosClick
      end
      object lb19: TLabel
        Left = 550
        Top = 101
        Width = 51
        Height = 14
        AutoSize = False
        Caption = 'Troca'
      end
      object eBaixaId: TEditLuka
        Left = 1
        Top = 17
        Width = 54
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 0
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eUsuarioBaixa: TEditLuka
        Left = 465
        Top = 17
        Width = 240
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 1
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eDataPagamento: TEditLukaData
        Left = 263
        Top = 17
        Width = 97
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        TabOrder = 2
        Text = '  /  /    '
      end
      object eDataBaixa: TEditLukaData
        Left = 364
        Top = 17
        Width = 97
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        TabOrder = 3
        Text = '  /  /    '
      end
      object eValorDinheiro: TEditLuka
        Left = 65
        Top = 115
        Width = 90
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 4
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorCheque: TEditLuka
        Left = 159
        Top = 115
        Width = 90
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 5
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorCartao: TEditLuka
        Left = 253
        Top = 115
        Width = 90
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 6
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorCobranca: TEditLuka
        Left = 347
        Top = 115
        Width = 90
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 7
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object stValores: TStaticText
        Left = 63
        Top = 86
        Width = 583
        Height = 15
        Alignment = taCenter
        AutoSize = False
        Caption = 'Valores pagos na baixa'
        Color = 15790320
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 8
        Transparent = False
      end
      object eValorCredito: TEditLuka
        Left = 441
        Top = 115
        Width = 90
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 9
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eObservacoes: TMemo
        Left = 1
        Top = 157
        Width = 704
        Height = 133
        TabOrder = 10
      end
      object eEmpresaBaixa: TEditLuka
        Left = 59
        Top = 17
        Width = 200
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 11
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eValorBruto: TEditLuka
        Left = 1
        Top = 58
        Width = 120
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = -1
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 12
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorJuros: TEditLuka
        Left = 126
        Top = 58
        Width = 120
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 13
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorDesconto: TEditLuka
        Left = 251
        Top = 58
        Width = 120
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 14
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorLíquido: TEditLuka
        Left = 376
        Top = 58
        Width = 120
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 15
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object StaticTextLuka5: TStaticTextLuka
        Left = 499
        Top = 47
        Width = 206
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Tipo de movimento'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 16
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object stTipo: TStaticText
        Left = 499
        Top = 63
        Width = 206
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Retira no ato'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = 33023
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 17
        Transparent = False
      end
      object eValorTroca: TEditLuka
        Left = 550
        Top = 115
        Width = 96
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 18
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
    end
    object tsOrigem: TTabSheet
      Caption = 'Origem'
      ImageIndex = 4
      object lb1: TLabel
        Left = 2
        Top = -1
        Width = 105
        Height = 14
        AutoSize = False
        Caption = 'Baixa rec.origem'
      end
      object sbInformacoesBaixa: TSpeedButtonLuka
        Left = 106
        Top = 17
        Width = 18
        Height = 18
        Hint = 'Abrir informa'#231#245'es da baixa a receber que originou esta baixa'
        Flat = True
        NumGlyphs = 2
        OnClick = sbInformacoesBaixaClick
        TagImagem = 13
        PedirCertificacao = False
        PermitirAutOutroUsuario = False
      end
      object sbInformacoesPedido: TSpeedButtonLuka
        Left = 233
        Top = 17
        Width = 18
        Height = 18
        Hint = 'Abrir informa'#231#245'es do pedido'
        Flat = True
        NumGlyphs = 2
        OnClick = sbInformacoesPedidoClick
        TagImagem = 13
        PedirCertificacao = False
        PermitirAutOutroUsuario = False
      end
      object lb2: TLabel
        Left = 128
        Top = -1
        Width = 105
        Height = 14
        AutoSize = False
        Caption = 'Pedido'
      end
      object sbInformacoesAcumulado: TSpeedButtonLuka
        Left = 360
        Top = 17
        Width = 18
        Height = 18
        Hint = 'Abrir informa'#231#245'es doa acumulado'
        Flat = True
        NumGlyphs = 2
        OnClick = sbInformacoesAcumuladoClick
        TagImagem = 13
        PedirCertificacao = False
        PermitirAutOutroUsuario = False
      end
      object lb3: TLabel
        Left = 255
        Top = -1
        Width = 105
        Height = 14
        AutoSize = False
        Caption = 'Acumulado'
      end
      object eBaixaReceberOrigemId: TEditLuka
        Left = 1
        Top = 13
        Width = 105
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 0
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object ePedido: TEditLuka
        Left = 128
        Top = 13
        Width = 105
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 1
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eAcumulado: TEditLuka
        Left = 255
        Top = 13
        Width = 105
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 2
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
    end
    object tsContasPagamentoDinheiro: TTabSheet
      Caption = 'Contas pagamento dinheiro'
      ImageIndex = 3
      object sgContasDinheiro: TGridLuka
        Left = 0
        Top = 0
        Width = 706
        Height = 291
        Align = alClient
        ColCount = 3
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        TabOrder = 0
        OnDrawCell = sgContasDinheiroDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Conta'
          'Descricao'
          'Valor')
        Grid3D = False
        RealColCount = 10
        AtivarPopUpSelecao = False
        ColWidths = (
          54
          213
          161)
      end
    end
    object tsTitulosBaixados: TTabSheet
      Caption = 'T'#237'tulos baixados'
      ImageIndex = 1
      object sgTitulosBaixados: TGridLuka
        Left = 0
        Top = 0
        Width = 706
        Height = 291
        Align = alClient
        ColCount = 6
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        TabOrder = 0
        OnDblClick = sgTitulosBaixadosDblClick
        OnDrawCell = sgTitulosBaixadosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'C'#243'digo'
          'Fornecedor'
          'Documento'
          'Data vencto'
          'Valor'
          'Tipo cobran'#231'a')
        Grid3D = False
        RealColCount = 10
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          54
          210
          93
          73
          101
          170)
      end
    end
    object tsTitulosGerados: TTabSheet
      Caption = 'T'#237'tulos gerados'
      ImageIndex = 2
      object sgTitulosGerados: TGridLuka
        Left = 0
        Top = 0
        Width = 706
        Height = 291
        Align = alClient
        ColCount = 6
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        TabOrder = 0
        OnDblClick = sgTitulosGeradosDblClick
        OnDrawCell = sgTitulosGeradosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'C'#243'digo'
          'Cliente'
          'Documento'
          'Data vencto'
          'Valor'
          'Tipo cobran'#231'a'
          '')
        Grid3D = False
        RealColCount = 10
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          54
          213
          93
          73
          80
          159)
      end
    end
  end
end
