inherited FormPesquisaGruposTributacoesFederalCompra: TFormPesquisaGruposTributacoesFederalCompra
  Caption = 'Pesquisa grupos de tributa'#231#245'es federal de compra'
  ClientWidth = 534
  ExplicitWidth = 542
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 534
    ColCount = 4
    OnDrawCell = sgPesquisaDrawCell
    CorLinhaFoco = 38619
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Descri'#231#227'o'
      'Ativo?')
    RealColCount = 4
    ExplicitWidth = 534
    ColWidths = (
      28
      55
      362
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Text = '0'
  end
  inherited pnFiltro1: TPanel
    Width = 534
    ExplicitWidth = 534
    inherited lblPesquisa: TLabel
      Width = 158
      Caption = 'Grupo de tributacoes federal'
      ExplicitWidth = 158
    end
    inherited eValorPesquisa: TEditLuka
      Width = 326
      ExplicitWidth = 326
    end
  end
end
