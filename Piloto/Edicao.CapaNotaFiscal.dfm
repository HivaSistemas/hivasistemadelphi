inherited FormEdicaoCapaNotaFiscal: TFormEdicaoCapaNotaFiscal
  Caption = 'Edi'#231#227'o da capa da nota fiscal'
  ClientHeight = 507
  ClientWidth = 793
  ExplicitWidth = 799
  ExplicitHeight = 536
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 4
    Top = 3
    Width = 37
    Height = 14
    Caption = 'C'#243'digo'
  end
  object lbl1: TLabel [1]
    Left = 89
    Top = 3
    Width = 39
    Height = 14
    Caption = 'Cliente'
  end
  object lbl2: TLabel [2]
    Left = 617
    Top = 3
    Width = 42
    Height = 14
    Caption = 'Nr. nota'
  end
  object lbl3: TLabel [3]
    Left = 749
    Top = 3
    Width = 28
    Height = 14
    Caption = 'S'#233'rie'
  end
  object lbl27: TLabel [4]
    Left = 305
    Top = 3
    Width = 47
    Height = 14
    Caption = 'Empresa'
  end
  object lbl15: TLabel [5]
    Left = 4
    Top = 174
    Width = 83
    Height = 14
    Caption = 'Base c'#225'lc. ICMS'
  end
  object lbl16: TLabel [6]
    Left = 135
    Top = 174
    Width = 57
    Height = 14
    Caption = 'Valor ICMS'
  end
  object lbl17: TLabel [7]
    Left = 266
    Top = 174
    Width = 98
    Height = 14
    Caption = 'Base c'#225'lc. ICMS ST'
  end
  object lbl18: TLabel [8]
    Left = 397
    Top = 174
    Width = 72
    Height = 14
    Caption = 'Valor ICMS ST'
  end
  object lbl19: TLabel [9]
    Left = 659
    Top = 174
    Width = 132
    Height = 14
    Caption = 'Valor total dos produtos'
  end
  object lbl20: TLabel [10]
    Left = 4
    Top = 216
    Width = 57
    Height = 14
    Caption = 'Valor frete'
  end
  object lbl21: TLabel [11]
    Left = 135
    Top = 216
    Width = 68
    Height = 14
    Caption = 'Valor seguro'
  end
  object lbl22: TLabel [12]
    Left = 528
    Top = 174
    Width = 81
    Height = 14
    Caption = 'Valor desconto'
  end
  object lbl23: TLabel [13]
    Left = 266
    Top = 216
    Width = 85
    Height = 14
    Caption = 'Valor out. desp.'
  end
  object lbl24: TLabel [14]
    Left = 397
    Top = 216
    Width = 45
    Height = 14
    Caption = 'Valor IPI'
  end
  object lbl25: TLabel [15]
    Left = 528
    Top = 216
    Width = 117
    Height = 14
    Caption = 'Valor total da nota'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl26: TLabel [16]
    Left = 3
    Top = 365
    Width = 163
    Height = 14
    Caption = 'Informa'#231#245'es complementares'
  end
  object lbl7: TLabel [17]
    Left = 4
    Top = 90
    Width = 61
    Height = 14
    Caption = 'Logradouro'
  end
  object lbl8: TLabel [18]
    Left = 264
    Top = 90
    Width = 76
    Height = 14
    Caption = 'Complemento'
  end
  object lbl9: TLabel [19]
    Left = 485
    Top = 90
    Width = 43
    Height = 14
    Caption = 'Numero'
  end
  object lbl10: TLabel [20]
    Left = 4
    Top = 132
    Width = 33
    Height = 14
    Caption = 'Bairro'
  end
  object lbl11: TLabel [21]
    Left = 299
    Top = 132
    Width = 38
    Height = 14
    Caption = 'Cidade'
  end
  object lbl12: TLabel [22]
    Left = 485
    Top = 132
    Width = 14
    Height = 14
    Caption = 'UF'
  end
  object lbl13: TLabel [23]
    Left = 572
    Top = 132
    Width = 18
    Height = 14
    Caption = 'CEP'
  end
  object lb3: TLabel [24]
    Left = 471
    Top = 3
    Width = 52
    Height = 14
    Caption = 'Tipo nota'
  end
  object lb2: TLabel [25]
    Left = 684
    Top = 3
    Width = 42
    Height = 14
    Caption = 'Modelo'
  end
  object lb6: TLabel [26]
    Left = 305
    Top = 45
    Width = 101
    Height = 14
    Caption = 'Inscri'#231#227'o estadual'
  end
  object sbCarregarEnderecos: TSpeedButton [27]
    Left = 659
    Top = 145
    Width = 129
    Height = 22
    Caption = 'Carregar endere'#231'o'
    OnClick = sbCarregarEnderecosClick
  end
  object sbAlterarEmpresaEmitente: TSpeedButton [28]
    Left = 640
    Top = 101
    Width = 148
    Height = 22
    Caption = 'Alterar Empresa Emitente'
    OnClick = sbAlterarEmpresaEmitenteClick
  end
  object SpeedButton1: TSpeedButton [29]
    Left = 640
    Top = 73
    Width = 148
    Height = 22
    Caption = 'Alterar Cliente'
    OnClick = SpeedButton1Click
  end
  object Label1: TLabel [30]
    Left = 134
    Top = 277
    Width = 58
    Height = 14
    Caption = 'Peso bruto'
  end
  object Label2: TLabel [31]
    Left = 265
    Top = 277
    Width = 68
    Height = 14
    Caption = 'Peso l'#237'quido'
  end
  object Label3: TLabel [32]
    Left = 4
    Top = 277
    Width = 64
    Height = 14
    Caption = 'Quantidade'
  end
  object Label4: TLabel [33]
    Left = 4
    Top = 322
    Width = 70
    Height = 14
    Caption = 'Tipo de frete'
  end
  inherited pnOpcoes: TPanel
    Top = 470
    Width = 793
    TabOrder = 27
    ExplicitTop = 470
    ExplicitWidth = 793
  end
  object eNotaFiscalId: TEditLuka
    Left = 4
    Top = 17
    Width = 80
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 0
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eNomeCliente: TEditLuka
    Left = 89
    Top = 17
    Width = 211
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 1
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eNumeroNota: TEditLuka
    Left = 617
    Top = 17
    Width = 62
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 4
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eEmpresaNota: TEditLuka
    Left = 305
    Top = 17
    Width = 161
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 2
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eValorBaseCalculoICMS: TEditLuka
    Left = 4
    Top = 188
    Width = 125
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 16
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorICMS: TEditLuka
    Left = 135
    Top = 188
    Width = 125
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 17
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eBaseCalculoICMSST: TEditLuka
    Left = 266
    Top = 188
    Width = 125
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 18
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorICMSST: TEditLuka
    Left = 397
    Top = 188
    Width = 125
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 19
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorTotalProdutos: TEditLuka
    Left = 659
    Top = 188
    Width = 129
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 21
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorFrete: TEditLuka
    Left = 4
    Top = 230
    Width = 125
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 22
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorSeguro: TEditLuka
    Left = 135
    Top = 230
    Width = 125
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 23
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorDesconto: TEditLuka
    Left = 528
    Top = 188
    Width = 125
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 20
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorOutrasDespesas: TEditLuka
    Left = 266
    Top = 230
    Width = 125
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 24
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorIPI: TEditLuka
    Left = 397
    Top = 230
    Width = 125
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 25
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorTotalNota: TEditLuka
    Left = 528
    Top = 230
    Width = 260
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 26
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eInformacoesComplementares: TMemo
    Left = 1
    Top = 380
    Width = 784
    Height = 84
    TabOrder = 33
  end
  object eLogradouro: TEditLuka
    Left = 4
    Top = 104
    Width = 254
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 9
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eComplemento: TEditLuka
    Left = 264
    Top = 104
    Width = 215
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 10
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eNumero: TEditLuka
    Left = 485
    Top = 104
    Width = 81
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 11
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eBairro: TEditLuka
    Left = 4
    Top = 146
    Width = 289
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 12
    Text = 'SETOR PEDRO LUDOVICO TEIXEIRA'
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eCidade: TEditLuka
    Left = 299
    Top = 146
    Width = 180
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 13
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eEstadoId: TEditLuka
    Left = 485
    Top = 146
    Width = 81
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 14
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eCEP: TEditLuka
    Left = 572
    Top = 146
    Width = 81
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 15
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object cbTipoNota: TComboBoxLuka
    Left = 471
    Top = 17
    Width = 141
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    TabOrder = 3
    OnChange = cbTipoNotaChange
    OnKeyDown = ProximoCampo
    Items.Strings = (
      'NFCe'
      'NFe')
    Valores.Strings = (
      'C'
      'N')
    AsInt = 0
  end
  inline FrCFOP: TFrCFOPs
    Left = 4
    Top = 43
    Width = 297
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 7
    TabStop = True
    ExplicitLeft = 4
    ExplicitTop = 43
    ExplicitWidth = 297
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 272
      Height = 23
      TabOrder = 1
      ExplicitWidth = 272
      ExplicitHeight = 23
    end
    inherited CkAspas: TCheckBox
      TabOrder = 3
    end
    inherited CkFiltroDuplo: TCheckBox
      TabOrder = 5
    end
    inherited CkPesquisaNumerica: TCheckBox
      TabOrder = 7
    end
    inherited PnTitulos: TPanel
      Width = 297
      TabOrder = 0
      ExplicitWidth = 297
      inherited lbNomePesquisa: TLabel
        Width = 26
        Caption = 'CFOP'
        ExplicitWidth = 26
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 192
        ExplicitLeft = 192
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited cbTipoCFOPs: TComboBoxLuka
      Height = 22
      TabOrder = 8
      ExplicitHeight = 22
    end
    inherited pnPesquisa: TPanel
      Left = 272
      Height = 24
      TabOrder = 2
      ExplicitLeft = 272
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      TabOrder = 6
    end
  end
  object cbModelo: TComboBoxLuka
    Left = 684
    Top = 17
    Width = 60
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    Enabled = False
    TabOrder = 5
    OnKeyDown = ProximoCampo
    Items.Strings = (
      '55'
      '65')
    Valores.Strings = (
      '55'
      '65')
    AsInt = 0
  end
  object cbSerieNota: TComboBoxLuka
    Left = 749
    Top = 17
    Width = 39
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    TabOrder = 6
    OnKeyDown = ProximoCampo
    Items.Strings = (
      '1'
      '2'
      '3'
      '4'
      '5'
      '6'
      '7'
      '8'
      '9')
    Valores.Strings = (
      '1'
      '2'
      '3'
      '4'
      '5'
      '6'
      '7'
      '8'
      '9')
    AsInt = 0
  end
  object eInscricaoEstadual: TEditLuka
    Left = 305
    Top = 59
    Width = 161
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 13
    TabOrder = 8
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object ePesoBruto: TEditLuka
    Left = 134
    Top = 291
    Width = 125
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 29
    Text = '0,000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 3
    NaoAceitarEspaco = False
  end
  object ePesoLiquido: TEditLuka
    Left = 265
    Top = 291
    Width = 125
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 30
    Text = '0,000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 3
    NaoAceitarEspaco = False
  end
  object eQuantidade: TEditLuka
    Left = 4
    Top = 291
    Width = 125
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 28
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  inline FrUnidadeVenda: TFrUnidades
    Left = 400
    Top = 277
    Width = 344
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 31
    TabStop = True
    ExplicitLeft = 400
    ExplicitTop = 277
    ExplicitWidth = 344
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 319
      Height = 24
      TabOrder = 1
      ExplicitWidth = 319
      ExplicitHeight = 24
    end
    inherited CkAspas: TCheckBox
      TabOrder = 2
    end
    inherited CkFiltroDuplo: TCheckBox
      TabOrder = 4
    end
    inherited CkPesquisaNumerica: TCheckBox
      TabOrder = 5
    end
    inherited CkMultiSelecao: TCheckBox
      TabOrder = 3
    end
    inherited PnTitulos: TPanel
      Width = 344
      TabOrder = 0
      ExplicitWidth = 344
      inherited lbNomePesquisa: TLabel
        Width = 42
        Caption = 'Esp'#233'cie'
        ExplicitWidth = 42
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 239
        ExplicitLeft = 239
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 319
      Height = 25
      ExplicitLeft = 319
      ExplicitHeight = 25
      inherited sbPesquisa: TSpeedButton
        Visible = False
      end
    end
  end
  object stSPC: TStaticText
    Left = 4
    Top = 261
    Width = 781
    Height = 15
    Alignment = taCenter
    AutoSize = False
    Caption = 'Informa'#231#245'es do transporte'
    Color = clSilver
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 34
    Transparent = False
  end
  object cbTipoFrete: TComboBoxLuka
    Left = 4
    Top = 338
    Width = 236
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    ItemIndex = 0
    TabOrder = 32
    Text = 'Frete por conta do Remetente (CIF)'
    OnKeyDown = ProximoCampo
    Items.Strings = (
      'Frete por conta do Remetente (CIF)'
      'Frete por conta do Destinat'#225'rio (FOB)'
      'Sem ocorr'#234'ncia de transporte')
    Valores.Strings = (
      '0'
      '1'
      '9')
    AsInt = 0
    AsString = '0'
  end
  inline FrTransportadora: TFrTransportadora
    Left = 249
    Top = 322
    Width = 302
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 35
    TabStop = True
    ExplicitLeft = 249
    ExplicitTop = 322
    ExplicitWidth = 302
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 277
      Height = 23
      ExplicitWidth = 277
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 302
      ExplicitWidth = 302
      inherited lbNomePesquisa: TLabel
        Width = 83
        Caption = 'Transportadora'
        ExplicitWidth = 83
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 197
        ExplicitLeft = 197
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 277
      Height = 24
      ExplicitLeft = 277
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
end
