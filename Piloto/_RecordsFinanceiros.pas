unit _RecordsFinanceiros;

interface

type
  RecContasReceber = record
    ReceberId: Integer;
    Parcela: Integer;
    NumeroParcelas: Integer;
    numero_cheque: Integer;
    agencia: string;
    banco: Integer;
    nota_fiscal_id: Integer;
    vendedor_id: Integer;
    nome_vendedor: string;
    UsuarioCadastroId: Integer;
    NomeUsuarioCadastro: string;
    turno_id: Integer;
    cobranca_id: Integer;
    nome_tipo_cobranca: string;
    orcamento_id: Integer;
    AcumuladoId: Integer;
    empresa_id: Integer;
    nome_empresa: string;
    CadastroId: Integer;
    NomeCliente: string;
    ValorDocumento: Currency;
    ValorRetencao: Currency;
    ValorAdiantado: Currency;
    nome_emitente: string;
    CpfCnpjEmitente: string;
    codigo_barras: string;
    nsu: string;
    observacoes: string;
    NossoNumero: string;
    RemessaId: Integer;
    NomeArquivoRemessa: string;
    documento: string;
    status: string;
    StatusAnalitico: string;
    dias_atraso: Integer;
    CentroCustoId: Integer;
    telefone_emitente: string;
    NomeCentroCusto: string;
    NumeroCartaoTruncado: string;
    data_vencimento_original: TDateTime;
    data_vencimento: TDateTime;
    data_emissao: TDateTime;
    DataContabil: TDateTime;
    data_cadastro: TDateTime;
    codigo_autorizacao_tef: string;
    conta_corrente: string;
    baixa_origem_id: Integer;
    baixa_id: Integer;
    usuario_baixa_id: Integer;
    nome_usuario_baixa: string;
    ContaCustodiaId: Integer;
    ObservacoesContaCustodia: string;
    data_pagamento: TDateTime;
    data_hora_baixa: TDateTime;
    forma_pagamento: string;
    PortadorId: string;
    NomePortador: string;
    PlanoFinanceiroId: string;
    Origem: string;
    BaixaPagarOrigemId: Integer;
    ItemIdCrtOrcamentoId: Integer;
  end;

  RecCartoesTurno = record
    CadastroId: Integer;
    nome_fantasia: string;
    nsu: string;
    valor: Double;
    cobranca_id: Integer;
    nome_cobranca: string;
    orcamento_id: Integer;
    BaixaOrigemId: Integer;
    itemIdCrtOrcamento: Integer;
  end;

  RecContaReceberBaixa = record
    BaixaId: Integer;
    EmpresaId: Integer;
    CadastroId: Integer;
    nome_empresa_baixa: string;
    data_pagamento: TDateTime;
    ReceberCaixa: string;
    usuario_baixa_id: Integer;
    nome_usuario_baixa: string;
    ValorTitulos: Currency;
    ValorLiquido: Currency;
    ValorMulta: Currency;
    ValorJuros: Currency;
    valor_desconto: Double;
    ValorAdiantado: Currency;
    ValorRetencao: Double;
    valor_dinheiro: Double;
    valor_pix: Double;
    valor_cheque: Double;
    ValorCartaoDebito: Double;
    ValorCartaoCredito: Double;
    valor_cobranca: Double;
    valor_credito: Double;
    data_hora_baixa: TDateTime;
    TurnoId: Integer;
    BaixaPagarOrigemId: Integer;
    BaixaPixRecCx: string;
    ReceberAdiantadoId: Integer;
    Recebido: string;
    ValorTroco: Currency;
    observacoes: string;
    Tipo: string;
    TipoAnalitico: string;
    UsuarioEnvioCaixaId: Integer;
    NomeUsuarioEnvCaixa: string;
    NomeCliente: string;
  end;

  RecContaReceberBaixaItem = record
    baixa_id: Integer;
    receber_id: Integer;
    valor_dinheiro: Double;
    ValorCartaoDebito: Double;
    ValorCartaoCredito: Double;
    valor_cheque: Double;
    valor_cobranca: Double;
    valor_credito: Double;
    valor_desconto: Double;
    valor_pix: Double;
    ValorMulta: Double;
    ValorJuros: Double;

    CadastroId: Integer;
    nome_cliente: string;
    documento: string;
    data_vencimento: TDateTime;
    valor_documento: Currency;
    DiasAtraso: Integer;
    Banco: Integer;
    Agencia: Integer;
    ContaCorrente: string;
    NumeroCheque: Integer;
    NomeEmitente: string;
    TelefoneEmitente: string;
    CobrancaId: Integer;
    NomeTipoCobranca: string;
  end;

  RecContaPagar = record
    PagarId: Integer;
    CadastroId: Integer;
    nome_fornecedor: string;
    empresa_id: Integer;
    nome_empresa: string;
    entrada_id: Integer;
    cobranca_id: Integer;
    nome_tipo_cobranca: string;
    usuario_cadastro_id: Integer;
    nome_usuario_cadastro: string;
    documento: string;
    ValorDocumento: Double;
    ValorAdiantado: Currency;
    ValorDesconto: Double;
    ValorSaldo: Currency;
    parcela: Integer;
    numero_parcelas: Integer;
    numero_cheque: Integer;
    status: string;
    IndiceCondicaoPagamento: Double;
    StatusAnalitico: string;
    data_cadastro: TDateTime;
    data_vencimento: TDateTime;
    data_vencimento_original: TDateTime;
    nosso_numero: string;
    codigo_barras: string;
    Bloqueado: string;
    MotivoBloqueio: string;
    OrcamentoId: Integer;
    AcumuladoId: Integer;
    baixa_origem_id: Integer;
    DevolucaoId: Integer;
    BaixaReceberOrigemId: Integer;
    baixa_id: Integer;
    usuario_baixa_id: Integer;
    nome_usuario_baixa: string;
    data_pagamento: TDateTime;
    data_hora_baixa: TDateTime;
    dias_atraso: Integer;
    PlanoFinanceiroId: string;
    NomePlanoFinanceiro: string;
    CentroCustoId: Integer;
    NomeCentroCusto: string;
    PortadorId: Integer;
    NomePortador: string;
    DataContabil: TDateTime;
    DataEmissao: TDateTime;
    Observacoes: string;
    Origem: string;
    OrigemAnalitico: string;
  end;

  RecContasPagarBaixas = record
    empresa_id: Integer;
    NomeEmpresaBaixa: string;
    RazaoSocialEmpresaBaixa: string;
    CnpjEmpresaBaixa: string;
    CadastroId: Integer;
    baixa_id: Integer;
    usuario_baixa_id: Integer;
    NomeUsuarioBaixa: string;
    NomeFornecedor: string;
    CpfCpnjFornecedor: string;
    valor_cheque: Double;
    valor_dinheiro: Double;
    valor_cartao: Double;
    ValorTitulos: Double;
    ValorJuros: Double;
    valor_desconto: Double;
    valor_credito: Double;
    ValorTroca: Double;
    valor_cobranca: Double;
    data_hora_baixa: TDateTime;
    data_pagamento: TDateTime;
    observacoes: string;
    Tipo: string;
    TipoAnalitico: string;
    StatusCaixa: string;
    ValorLiquido: Double;
    BaixaReceberOrigemId: Integer;
    OrcamentoId: Integer;
    AcumuladoId: Integer;
    PagarAdiantadoId: Integer;
    TurnoId: Integer;
  end;

  RecContasPagarBaixasItens = record
    baixa_id: Integer;
    PagarId: Integer;
    valor_dinheiro: Double;
    valor_cheque: Double;
    valor_cartao: Double;
    valor_cobranca: Double;
    valor_credito: Double;
    valor_desconto: Double;
    ValorJuros: Double;
    ValorDocumento: Double;
    DataVencimento: TDateTime;
    CadastroId: Integer;
    NomeFornecedor: string;
    Documento: string;
    CobrancaId: Integer;
    NomeTipoCobranca: string;
    ValorLiquido: Double;
  end;

  RecTitulosBaixasPagtoDin = record
    BaixaId: Integer;
    ContaId: string;
    Descricao: string;
    Valor: Double;
  end;

  RecPagamentosPix = record
    BaixaId: Integer;
    ContaId: string;
    Valor: Double;
    DescricaoConta: string;
  end;

  RecTitulosFinanceiros = record
    Id: Integer;
    CobrancaId: Integer;
    NomeCobranca: string;
    Documento: string;
    ItemId: Integer;
    Valor: Double;
    ValorRetencao: Double;
    Tipo: string;
    DataVencimento: TDateTime;

    Parcela: Integer;
    NumeroParcelas: Integer;
    UtilizarLimiteCredCliente: string;

    NossoNumero: string;
    CodigoBarras: string;

    NsuTef: string;
    CodigoAutorizacao: string;
    NumeroCartao: string;
    TipoRecebCartao: string;  // Tipo de recebimento de fato que ocorreu no momento do recebimento

    TipoCartao: string;
    TipoCartaoAnalitico: string;
    tipoRecebimentoCartao: string;

    Banco: string;
    Agencia: string;
    ContaCorrente: string;
    NumeroCheque: Integer;
    NomeEmitente: string;
    CpfCnpjEmitente: string;
    TelefoneEmitente: string;

    PortadorId: string;
    PermitirPrazoMedio: string;

    FornecedorId: Integer;
    NomeFornecedor: string;

    PlanoFinanceiroId: string;
    CentroCustoId: Integer;
  end;

implementation

end.
