unit PesquisaContasBoletos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _Sessao,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _ContasBoletos, _Biblioteca, System.Math, System.StrUtils,
  Vcl.ExtCtrls;

type
  TFormPesquisaContasBoletos = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  protected
    procedure BuscarRegistros; override;
  end;

function PesquisarContasBoletos(pTipo: string; pSomenteAtivos: Boolean): TObject;

implementation

{$R *.dfm}

const
  cp_Descricao    = 2;
  cp_Banco    = 3;
  cp_agencia = 4;
  cp_conta   = 5;

var
  FTipo: string;
  FSomenteAtivos: Boolean;
  FfuncionarioAutorizadoID: Integer;

function PesquisarContasBoletos(pTipo: string; pSomenteAtivos: Boolean): TObject;
var
  r: TObject;
begin
  FTipo := pTipo;
  FSomenteAtivos := pSomenteAtivos;
  r := _HerancaPesquisas.Pesquisar(TFormPesquisaContasBoletos, _ContasBoletos.GetFiltros);
  if r = nil then
    Result := nil
  else
    Result := RecContasBoletos(r);
end;

procedure TFormPesquisaContasBoletos.BuscarRegistros;
var
  i: Integer;
  vContasBoletos: TArray<RecContasBoletos>;
begin
  inherited;

  vContasBoletos :=
    _ContasBoletos.BuscarContasBoletos(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text]);

  if vContasBoletos = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vContasBoletos);

  for i := Low(vContasBoletos) to High(vContasBoletos) do begin
    sgPesquisa.Cells[coSelecionado, i + 1] := 'N�o';
    sgPesquisa.Cells[coCodigo, i + 1]      := vContasBoletos[i].ContaboletoId.ToString;
    sgPesquisa.Cells[cp_Descricao, i + 1]  := vContasBoletos[i].Descricao;
    sgPesquisa.Cells[cp_Banco, i + 1]      := vContasBoletos[i].NumeroBanco;
    sgPesquisa.Cells[cp_agencia, i + 1]    := vContasBoletos[i].NumeroAgencia.ToString;
    sgPesquisa.Cells[cp_conta, i + 1]      := vContasBoletos[i].NumeroConta.ToString;
  end;

  sgPesquisa.RowCount := IfThen(Length(vContasBoletos) = 1, 2, High(vContasBoletos) + 2);
  SetarFoco(sgPesquisa);
end;

procedure TFormPesquisaContasBoletos.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  alinhamento: TAlignment;
begin
  inherited;

  if _Biblioteca.Em(ACol, [coSelecionado]) then
    alinhamento := taCenter
  else if ACol = coCodigo then
    alinhamento := taRightJustify
  else
    alinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], alinhamento, Rect);
end;

end.
