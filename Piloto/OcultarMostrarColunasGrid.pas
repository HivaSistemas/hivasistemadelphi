unit OcultarMostrarColunasGrid;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.StdCtrls, RadioGroupLuka, GridLuka, _Biblioteca,
  CheckBoxLuka, SpeedButtonLuka, Vcl.Menus;

type
  TFormOcultarMostrarColunasGrid = class(TFormHerancaFinalizar)
    pmOpcoes: TPopupMenu;
    miMarcaTodos: TMenuItem;
    miDesmarcarTodos: TMenuItem;
    procedure miMarcarTodosClick(Sender: TObject);
    procedure miDesmarcarTodosClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected

  end;

  function MostrarOcultarColunasGrid(
    var pGrid: TGridLuka;
    pTelaeGrid: string;
    pColunas: TArray<Integer> = nil
  ): TArray<Integer>;

implementation

{$R *.dfm}

function MostrarOcultarColunasGrid(
  var pGrid: TGridLuka;
  pTelaeGrid: string;
  pColunas: TArray<Integer> = nil
): TArray<Integer>;
var
  vForm: TFormOcultarMostrarColunasGrid;
  check: TCheckBoxLuka;
  vLinha1: Integer;
  vLinha2: Integer;
  vHeight: Integer;
  vLeft: Integer;
  vMaiorLargura: Integer;
  vTrocouLarguraTela: Boolean;
begin
  Result := nil;
  vLeft := 5;
  vHeight := 0;
  vMaiorLargura := 0;
  vTrocouLarguraTela := False;
  vForm := TFormOcultarMostrarColunasGrid.Create(nil);
  vForm.Caption := vForm.Caption + pTelaeGrid;

  if pGrid.ColCount < 20 then
    vForm.Height := pGrid.ColCount * 23;

  for vLinha1 := pGrid.FixedCols to pGrid.ColCount - 1 do begin

    if pColunas = nil then begin

      vHeight := vHeight + 16;

      if vHeight >= (vForm.Height - vForm.pnOpcoes.Height - 45) then begin
        vLeft         := vLeft + vMaiorLargura + 5;
        vHeight       := 16;
        vMaiorLargura := 0;

        vForm.ClientWidth := vForm.ClientWidth + vMaiorLargura + 30;
        vTrocouLarguraTela := True;
      end;

      check := TCheckBoxLuka.Create(vForm);
      check.Parent  := vForm;
      check.Width   := Length(Trim(pGrid.Cells[vLinha1, pGrid.FixedRows - 1])) * 10;
      check.Caption := Trim(pGrid.Cells[vLinha1, pGrid.FixedRows - 1]);
      check.Top     := vHeight;
      check.Left    := vLeft;
      check.Checked := not (pGrid.ColWidths[vLinha1] <= 0);

      if check.Width > vMaiorLargura then
        vMaiorLargura := check.Width;
    end
    else begin
      for vLinha2 := Low(pColunas) to High(pColunas) do begin

        if pColunas[vLinha2] = vLinha1 then begin

          vHeight := vHeight + 16;

          if vHeight >= (vForm.Height - vForm.pnOpcoes.Height - 45) then begin
            vLeft         := vLeft + vMaiorLargura + 5;
            vHeight       := 16;
            vMaiorLargura := 0;

            vForm.ClientWidth := vForm.ClientWidth + vMaiorLargura + 30;
            vTrocouLarguraTela := True;
          end;

          check := TCheckBoxLuka.Create(vForm);
          check.Parent  := vForm;
          check.Width   := Length(Trim(pGrid.Cells[vLinha1, pGrid.FixedRows - 1])) * 10;
          check.Caption := Trim(pGrid.Cells[vLinha1, pGrid.FixedRows - 1]);
          check.Top     := vHeight;
          check.Left    := vLeft;
          check.Checked := not (pGrid.ColWidths[vLinha1] <= 0);

          if check.Width > vMaiorLargura then
            vMaiorLargura := check.Width;

          Break;
        end;
      end;
    end;
  end;

  if not vTrocouLarguraTela then
    vForm.Width := vMaiorLargura + 15;

  if vForm.ShowModal = MrOk then begin

    for vLinha1 := 0 to vForm.ComponentCount - 1 do begin

      if not (vForm.Components[vLinha1].ClassType = TCheckBoxLuka) then
        Continue;

      for vLinha2 := pGrid.FixedCols to pGrid.ColCount - 1 do begin

        if TCheckBoxLuka(vForm.Components[vLinha1]).Caption = Trim(pGrid.Cells[vLinha2, pGrid.FixedRows - 1]) then begin

           if TCheckBoxLuka(vForm.Components[vLinha1]).Checked then begin
             pGrid.MostrarColunas(vLinha2);

             SetLength(Result, Length(Result) + 1);
             Result[High(Result)] := vLinha2;
           end
           else
             pGrid.OcultarColunas(vLinha2);

           Break;
        end;
      end;
    end;
  end;

end;

procedure TFormOcultarMostrarColunasGrid.miMarcarTodosClick(Sender: TObject);
var
  vLinha: Integer;
begin
  inherited;
  for vLinha := 0 to ComponentCount - 1 do begin

    if not (Components[vLinha].ClassType = TCheckBoxLuka) then
      Continue;

    TCheckBoxLuka(Components[vLinha]).Checked := True;
  end;
end;

procedure TFormOcultarMostrarColunasGrid.miDesmarcarTodosClick(Sender: TObject);
var
  vLinha: Integer;
begin
  inherited;
  for vLinha := 0 to ComponentCount - 1 do begin

    if not (Components[vLinha].ClassType = TCheckBoxLuka) then
      Continue;

    TCheckBoxLuka(Components[vLinha]).Checked := False;
  end;
end;

end.
