unit BuscarPlanoFinanceiro;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, _Biblioteca,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FramePlanosFinanceiros,
  Vcl.Buttons, Vcl.ExtCtrls;

type
  TFormBuscarPlanoFinanceiro = class(TFormHerancaFinalizar)
    FrPlanosFinanceiro: TFrPlanosFinanceiros;
    procedure FormShow(Sender: TObject);
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Buscar(pPlanoFinaneiroId: string): TRetornoTelaFinalizar<string>;

implementation

{$R *.dfm}

function Buscar(pPlanoFinaneiroId: string): TRetornoTelaFinalizar<string>;
var
  vForm: TFormBuscarPlanoFinanceiro;
begin
  vForm := TFormBuscarPlanoFinanceiro.Create(Application);

  vForm.FrPlanosFinanceiro.InserirDadoPorChave(pPlanoFinaneiroId, False);

  if Result.Ok(vForm.ShowModal, True) then
    Result.Dados := vForm.FrPlanosFinanceiro.getDados().PlanoFinanceiroId;
end;

{ TFormBuscarPlanoFinanceiro }

procedure TFormBuscarPlanoFinanceiro.Finalizar(Sender: TObject);
begin
  inherited;

end;

procedure TFormBuscarPlanoFinanceiro.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrPlanosFinanceiro);
end;

procedure TFormBuscarPlanoFinanceiro.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrPlanosFinanceiro.EstaVazio then begin
    _Biblioteca.Exclamar('O plano financeiro n�o foi informado corretamente, verifique!');
    SetarFoco(FrPlanosFinanceiro);
    Abort;
  end;
end;

end.
