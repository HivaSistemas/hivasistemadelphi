unit PesquisaEstados;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, _RecordsEspeciais,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _Estados, _RecordsCadastros,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _Sessao, _Biblioteca, System.Math,
  Vcl.ExtCtrls;

type
  TFormPesquisaEstados = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
  protected
    procedure BuscarRegistros; override;
  end;

function PesquisarEstado: TObject;

implementation

{$R *.dfm}

const
  cp_nome  = 2;
  cp_ativo = 3;

function PesquisarEstado: TObject;
var
  r: TObject;
begin
  r := _HerancaPesquisas.Pesquisar(TFormPesquisaEstados, _Estados.GetFiltros);
  if r = nil then
    Result := nil
  else
    Result := RecEstados(r);
end;

procedure TFormPesquisaEstados.BuscarRegistros;
var
  i: Integer;
  estados: TArray<RecEstados>;
begin
  inherited;

  estados :=
    _Estados.BuscarEstados(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text]
    );

  if estados = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(estados);

  for i := Low(estados) to High(estados) do begin
    sgPesquisa.Cells[coSelecionado, i + 1] := 'N�o';
    sgPesquisa.Cells[coCodigo, i + 1] := estados[i].estado_id;
    sgPesquisa.Cells[cp_nome, i + 1] := estados[i].nome;
    sgPesquisa.Cells[cp_ativo, i + 1] := estados[i].ativo;
  end;

  sgPesquisa.RowCount := IfThen(Length(estados) = 1, 2, High(estados) + 2);
  SetarFoco(sgPesquisa);
end;

procedure TFormPesquisaEstados.sgPesquisaDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  alinhamento: TAlignment;
begin
  inherited;

  if ACol = cp_ativo then
    alinhamento := taCenter
  else
    alinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], alinhamento, Rect);
end;

end.
