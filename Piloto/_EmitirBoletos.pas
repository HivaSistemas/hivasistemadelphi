unit _EmitirBoletos;

interface

uses
  Classes, ACBrBoleto, ACBrBoletoFCFR, ACBrBase, ACBrBoletoConversao, ACBrUtil.Base,
  _RecordsRelatorios, _RelacaoContasReceber,  _Sessao, System.SysUtils,
  _ContasBoletos, Vcl.ExtCtrls, _RecordsEspeciais, _ContasReceber,
  _RecordsCadastros, Vcl.Dialogs, _Email, DBClient, DB;

type
  TipoEmissao = (teBoleto, teArquivo);

  TContaReceber = record
    ReceberId: Integer;
  end;

  TParametroImpressao = class
    CaminhoBoletoPDF : String;
    Impressora: String;
    GerarNovoNossoNumero: Boolean;
    EnviarBoletoEmail: Boolean;
  end;


  TContasReceber = array of TContaReceber;

  TParametrosImpressao = class
  private
    FTitulos: TContasReceber;
    FItensImpressao: TParametroImpressao;
    FContasBoletosId: Integer;
    FCaminhoArqRetorno: string;
  public
    constructor Create;
    procedure AdicionarContasReceber(pReceberId : Integer);
    property ItensImpressao: TParametroImpressao read FItensImpressao write FItensImpressao;
    property ContasBoletosId: Integer read FContasBoletosId write FContasBoletosId;
    property CaminhoArqRetorno: string read FCaminhoArqRetorno write FCaminhoArqRetorno;
  end;

  TBoletosACBr = class(TObject)
  private
    ACBrBoletoComp: TACBrBoleto;
    ACBrBoletoFR: TACBrBoletoFCFR;
    ProximoNossoNumero : string;
    vContaBoletoID: Integer;
    vCarteira: String;
    vTextoLivre: string;
    vDiasProtesto: integer;
    vDiasMultaJuros: integer;
    vDiasBaixaDevolucao: integer;
    vDiasLimitePagamento: integer;
    vParametrosImpressao: TParametrosImpressao;
    vTipoNegativacao: TACBrCodigoNegativacao;
    vInstrucao1: string;
    vInstrucao2: string;
    vInstrucao3: string;
    vEspecie: string;
    vEspecieMoeda: string;
    vSequencialRemessa: integer;
    FCdsTitulos: TClientDataSet;
    FCdsMotivosOcorrencia: TClientDataSet;
    FCdsOcorrencias: TClientDataSet;
    FVlrTaxasTot: Double;
    FVlrJurosTot: Double;
    FVlrPagoTot: Double;
    FPortadorId: String;
    function  CarregarContaBoletos(pTipoEmissao: TipoEmissao; pEmpresaId: Integer; pArquivoRetorno: String = '' ): Boolean;
    procedure CarregarTitulos( var pTitulos: TArray<_RecordsRelatorios.RecContasReceber>);
    procedure AdicionaBoletoComponente(pTitulos: TArray<_RecordsRelatorios.RecContasReceber>);
    procedure AtualizarDadosTabelas(pReceberId : Integer; pNossoNumero, pCodigoBarras, pLinhaDigitavel: String);
    procedure Imprimir;
    procedure ExibirPreviewBoleto;
    procedure GerarPDFDiretorio;
    procedure ImprimirSemPreview;
    function  TravarProximoNossoNumero: Boolean;
    function  MontaCaminhoArquivo(pNomeArquivo: String): string;
    function  MontaNomeArquivo(pNomeArquivo, pExtensao: String): string;
    procedure EnviarBoletoAnexo;
    procedure ConfigurarCdsTitulos;
    procedure carregarTitulosRetorno;
    procedure ConfigurarCdsMotivosOcorrencia;
    procedure ConfigurarCdsOcorrencias;
    function DescricaoOcorrencia(pCodigoBanco, pCodigoOcorrencia: Integer): String;
    function retornaMensagemHiva(pValor: Double; pVencimento: TDateTime): string;
  public
    procedure EmitirBoletas(pParams: TParametrosImpressao);
    procedure GerarArquivoRemessa(pParams: TParametrosImpressao);
    procedure LerArquivoRetorno(pParams: TParametrosImpressao);
  published
    property CdsTitulos: TClientDataSet read FCdsTitulos write FCdsTitulos;
    property CdsMotivosOcorrencia: TClientDataSet read FCdsMotivosOcorrencia write FCdsMotivosOcorrencia;
    property CdsOcorrencias: TClientDataSet read FCdsOcorrencias write FCdsOcorrencias;
    property VlrTaxasTot: Double read FVlrTaxasTot write FVlrTaxasTot;
    property VlrJurosTot: Double read FVlrJurosTot write FVlrJurosTot;
    property VlrPagoTot: Double read FVlrPagoTot write FVlrPagoTot;
    property PortadorId: String read FPortadorId write FPortadorId;

  end;
implementation

{ TListaContasReceber }

uses _Biblioteca, _RemessasCobrancas;

procedure TParametrosImpressao.AdicionarContasReceber(pReceberId: Integer);
begin
  SetLength(FTitulos, Length(FTitulos) + 1);
  FTitulos[High(FTitulos)].ReceberId := pReceberId;
end;

{ TBoletosACBr }

procedure TBoletosACBr.AdicionaBoletoComponente(
  pTitulos: TArray<_RecordsRelatorios.RecContasReceber>);
var
  vTitulo: TACBrTitulo;
  vCnt: Integer;
begin
  inherited;
  for vCnt := Low(pTitulos) to High(pTitulos) do
  begin

    vTitulo                         := ACBrBoletoComp.CriarTituloNaLista;
    vTitulo.Vencimento              := pTitulos[vCnt].data_vencimento;
    vTitulo.DataDocumento           := pTitulos[vCnt].data_emissao;
    vTitulo.NumeroDocumento         := pTitulos[vCnt].documento;
    vTitulo.SeuNumero               := pTitulos[vCnt].receber_id.ToString;
    vTitulo.EspecieDoc              := vEspecie;
    vTitulo.EspecieMod              := vEspecieMoeda;
    vTitulo.Aceite                  := atNao;
    vTitulo.DataProcessamento       := Now;
    vTitulo.Carteira                := vCarteira;
    vTitulo.ValorDocumento          := pTitulos[vCnt].valor_documento;

    vTitulo.Sacado.NomeSacado       := pTitulos[vCnt].RazaoSocial;
    vTitulo.Sacado.CNPJCPF          := RetornaNumeros(pTitulos[vCnt].CpfCnpj);
    vTitulo.Sacado.Pessoa           := Iif(RetornaNumeros(pTitulos[vCnt].CpfCnpj).Length = 14, pJuridica, pFisica);
    vTitulo.Sacado.Logradouro       := pTitulos[vCnt].LogradouroCob;
    vTitulo.Sacado.Complemento      := pTitulos[vCnt].ComplementoEndCob;
    vTitulo.Sacado.Numero           := pTitulos[vCnt].NumeroEndCob;
    vTitulo.Sacado.Bairro           := pTitulos[vCnt].BairroEndCob;
    vTitulo.Sacado.Cidade           := pTitulos[vCnt].CidadeEndCob;
    vTitulo.Sacado.UF               := pTitulos[vCnt].UFEndCob;
    vTitulo.Sacado.CEP              := RetornaNumeros(pTitulos[vCnt].CepEndCob).PadRight(8,'0');
    vTitulo.Sacado.Email            := pTitulos[vCnt].Email;

    vTitulo.PercentualMulta         := Sessao.getParametrosEmpresa.PercentualMulta;

    vTitulo.ValorMoraJuros          := (Sessao.getParametrosEmpresa.PercentualJurosMensal / 30) *  pTitulos[vCnt].valor_documento *  0.01;


    vTitulo.Instrucao1              := vInstrucao1;
    vTitulo.Instrucao2              := vInstrucao2;
    vTitulo.Instrucao3              := vInstrucao3;
    vTitulo.Mensagem.Add(vTextoLivre);

//    vTitulo.ValorDesconto := pTitulos[vCnt].ValorDesconto;
    vTitulo.CodigoNegativacao       := vTipoNegativacao;
    vTitulo.DataMoraJuros           := pTitulos[vCnt].data_vencimento + vDiasMultaJuros;
    vTitulo.DataMulta               := pTitulos[vCnt].data_vencimento + vDiasMultaJuros;
    vTitulo.DataAbatimento          := 0;
//    vTitulo.DataDesconto            := pTitulos[vCnt].data_vencimento + vDiasDesconto;
    vTitulo.DataProtesto          := pTitulos[vCnt].data_vencimento + vDiasProtesto;
    vTitulo.DataBaixa             := pTitulos[vCnt].data_vencimento + vDiasBaixaDevolucao;
    //vTitulo.DataLimitePagto       := pTitulos[vCnt].data_vencimento + vDiasLimitePagamento;

    vTitulo.OcorrenciaOriginal.Tipo := toRemessaRegistrar;
    vTitulo.QtdePagamentoParcial    := 1;
    vTitulo.TipoPagamento           := tpNao_Aceita_Valor_Divergente;
    vTitulo.PercentualMinPagamento  := 0;
    vTitulo.PercentualMaxPagamento  := 0;
    vTitulo.ValorMinPagamento       := 0;
    vTitulo.ValorMaxPagamento       := 0;

    vTitulo.UsoBanco    := pTitulos[vCnt].orcamento_id.ToString;
    vTitulo.NossoNumero := pTitulos[vCnt].nosso_numero;
  end;
end;

procedure TBoletosACBr.AtualizarDadosTabelas(pReceberId: Integer; pNossoNumero, pCodigoBarras, pLinhaDigitavel: String);
var
  vRetBanco: RecRetornoBD;
  vComando: String;
  vTitulo: TArray<_RecordsRelatorios.RecContasReceber>;
begin
  if StrToFloat(ProximoNossoNumero) < 1 then
  begin
    Avisar('Boleto','Houve um problema ao buscar o pr�ximo nosso n�mero. ' +
            'Caso o boleto tenha sido emitido deve ser inutilizado. ' +
            'Entre em contato com o suporte.');
    if Sessao.getConexaoBanco.EmTransacao then
      Sessao.getConexaoBanco.FinalizarTransacao;
    Abort;
  end;
  vComando := '';
  WhereOuAnd(vComando, ' CON.RECEBER_ID = ' + pReceberId.ToString );
  vTitulo := _RelacaoContasReceber.BuscarContasReceberComando(
              Sessao.getConexaoBanco,
              vComando);

  vComando := '';
  WhereOuAnd(vComando, ' CON.PORTADOR_ID = ' + vTitulo[0].PortadorId.QuotedString);
  WhereOuAnd(vComando, ' CON.NOSSO_NUMERO = ' + pNossoNumero.QuotedString);
  WhereOuAnd(vComando, ' CON.RECEBER_ID <> ' + pReceberId.ToString);

  vTitulo := _RelacaoContasReceber.BuscarContasReceberComando(
              Sessao.getConexaoBanco,
              vComando);


  if length(vTitulo) > 0 then
  begin
    Avisar('Boleto','O nosso numero "'+pNossoNumero+
                    '" ja existe em outro titulo do banco "'+vTitulo[0].PortadorId+
                    '". Boleto n�o foi gerado.');
    Abort;
  end;

  vRetBanco := _ContasBoletos.AtualizarProximoNossoNumero(Sessao.getConexaoBanco,
                                                          vContaBoletoID,
                                                          ProximoNossoNumero );

  vRetBanco := _ContasReceber.AtualizarNossoNumeroCodBarraLinhaDigitavel(Sessao.getConexaoBanco,
                                                                         pReceberId,
                                                                         pNossoNumero,
                                                                         pCodigoBarras,
                                                                         pLinhaDigitavel );

  vRetBanco := _ContasReceber.AtualizarEmitiuBoleto(Sessao.getConexaoBanco,
                                                   pReceberId,
                                                   'S');

end;

function TBoletosACBr.CarregarContaBoletos(pTipoEmissao: TipoEmissao; pEmpresaId: Integer; pArquivoRetorno: String = ''): Boolean;
var
  vBancoBoletos: TArray<RecContasBoletos>;
  stlReport : TStringList;
  ImgFoto : TImage;
  vEmpresas: TArray<RecEmpresas>;
  vEmpresa: RecEmpresas;
  vDiretorioSistema: string;
  vCnt: Integer;
begin
  vEmpresas := Sessao.getEmpresas;
   for vCnt := Low(vEmpresas) to High(vEmpresas) do
   begin
     if vEmpresas[vCnt].EmpresaId = pEmpresaId then
     begin
       vEmpresa := vEmpresas[vCnt];
       Break;
     end;
   end;
  vBancoBoletos := _ContasBoletos.BuscarContasBoletos(Sessao.getConexaoBanco,0,[vContaBoletoID]);

  if length(vBancoBoletos) = 0 then
  begin
    Avisar('Boleto','Conta boleto n�o encontrada!');
    Result := False;
    Abort;
  end;

  if vBancoBoletos[0].TipoNegativacao = 'Nenhum' then
    vTipoNegativacao := cnNenhum
  else if vBancoBoletos[0].TipoNegativacao = 'Dias Corrido' then
    vTipoNegativacao := cnProtestarCorrido
  else if vBancoBoletos[0].TipoNegativacao = 'Dias Uteis' then
    vTipoNegativacao := cnProtestarUteis
  else if vBancoBoletos[0].TipoNegativacao = 'N�o Protestar' then
    vTipoNegativacao := cnNaoProtestar
  else if vBancoBoletos[0].TipoNegativacao = 'Negativar' then
    vTipoNegativacao := cnNegativar
  else if vBancoBoletos[0].TipoNegativacao = 'N�o Negativar' then
    vTipoNegativacao := cnNaoNegativar;

  vCarteira                          := vBancoBoletos[0].Carteira;
  vTextoLivre                        := vBancoBoletos[0].TextoLivre;
  vDiasProtesto                      := vBancoBoletos[0].DiasProtesto;
  vDiasBaixaDevolucao                := vBancoBoletos[0].DiasBaixaDevolucao;
  vDiasLimitePagamento               := vBancoBoletos[0].DiasLimitePagamento;
  vDiasMultaJuros                    := vBancoBoletos[0].DiasMultaJuros;
  vInstrucao1                        := vBancoBoletos[0].Instrucao1;
  vInstrucao2                        := vBancoBoletos[0].Instrucao2;
  vInstrucao3                        := vBancoBoletos[0].Instrucao3;
  vEspecie                           := vBancoBoletos[0].Especie;
  vEspecieMoeda                      := vBancoBoletos[0].EspecieMoeda;
  vSequencialRemessa                 := vBancoBoletos[0].SequencialRemessa;

  ACBrBoletoComp                    := TACBrBoleto.Create(nil);
  ACBrBoletoComp.Banco.TipoCobranca := ACBrBoletoComp.GetTipoCobranca(vBancoBoletos[0].NumeroBanco.ToInteger);

  if (not vBancoBoletos[0].VersaoLayout.IsEmpty) then
  begin
    ACBrBoletoComp.Banco.LayoutVersaoArquivo := StrToInt64Def(vBancoBoletos[0].VersaoLayout, 0);
  end;

  if (not vBancoBoletos[0].VersaoLote.IsEmpty) then
  begin
    ACBrBoletoComp.Banco.LayoutVersaoLote    := StrToInt64Def(vBancoBoletos[0].VersaoLote, 0);
  end;

  ACBrBoletoComp.Cedente.Agencia                       := vBancoBoletos[0].NumeroAgencia.ToString;
  ACBrBoletoComp.Cedente.AgenciaDigito                 := vBancoBoletos[0].DigitoAgencia;
  ACBrBoletoComp.Cedente.Conta                         := vBancoBoletos[0].NumeroConta.ToString;
  ACBrBoletoComp.Cedente.ContaDigito                   := vBancoBoletos[0].DigitoConta;
  ACBrBoletoComp.Cedente.CodigoCedente                 := vBancoBoletos[0].Cedente;
  ACBrBoletoComp.Cedente.CodigoTransmissao             := vBancoBoletos[0].Transmissao;
  ACBrBoletoComp.Cedente.Convenio                      := vBancoBoletos[0].Convenio;
  ACBrBoletoComp.Cedente.DigitoVerificadorAgenciaConta := vBancoBoletos[0].DvAgenciaConta;
  ACBrBoletoComp.Cedente.Modalidade                    := vBancoBoletos[0].Modalidade;
  ACBrBoletoComp.Cedente.Operacao                      := vBancoBoletos[0].Operacao;

  ACBrBoletoComp.LayoutRemessa                         := Iif(vBancoBoletos[0].Layout.Trim.ToUpper = 'CNAB240', c240, c400);

  ACBrBoletoComp.DirArqRetorno      := ExtractFilePath(pArquivoRetorno);
  ACBrBoletoComp.NomeArqRetorno     := ExtractFileName(pArquivoRetorno);
  ACBrBoletoComp.NomeArqRemessa     := MontaNomeArquivo(vBancoBoletos[0].NomeArquivoRemessa, vBancoBoletos[0].ExtArquivoRemessa);

  if not vBancoBoletos[0].LocalPagamento.Trim.IsEmpty then
  begin
    ACBrBoletoComp.Banco.LocalPagamento := vBancoBoletos[0].LocalPagamento;
  end;

  if (vBancoBoletos[0].TipoCarteira = 'Simples') then
  begin
    ACBrBoletoComp.Cedente.TipoCarteira := tctSimples;
  end
  else if (vBancoBoletos[0].TipoCarteira = 'Registrada') then
  begin
    ACBrBoletoComp.Cedente.TipoCarteira := tctRegistrada;
  end
  else if (vBancoBoletos[0].TipoCarteira = 'Eletronica') then
  begin
    ACBrBoletoComp.Cedente.TipoCarteira := tctEletronica;
  end;

  if (vBancoBoletos[0].Caracteristica  = 'Simples') then
  begin
    ACBrBoletoComp.Cedente.CaracTitulo := tcSimples;
  end
  else if (vBancoBoletos[0].Caracteristica = 'Vinculada') then
  begin
    ACBrBoletoComp.Cedente.CaracTitulo := tcVinculada;
  end
  else if (vBancoBoletos[0].Caracteristica = 'Caucionada') then
  begin
    ACBrBoletoComp.Cedente.CaracTitulo := tcCaucionada;
  end
  else if (vBancoBoletos[0].Caracteristica = 'Descontada') then
  begin
    ACBrBoletoComp.Cedente.CaracTitulo := tcDescontada;
  end
  else if (vBancoBoletos[0].Caracteristica = 'Vendor') then
  begin
    ACBrBoletoComp.Cedente.CaracTitulo := tcVendor;
  end
  else if (vBancoBoletos[0].Caracteristica = 'Direta') then
  begin
    ACBrBoletoComp.Cedente.CaracTitulo := tcDireta;
  end
  else if (vBancoBoletos[0].Caracteristica = 'Simples (R�pida com Registro)') then
  begin
    ACBrBoletoComp.Cedente.CaracTitulo := tcSimplesRapComReg;
  end
  else if (vBancoBoletos[0].Caracteristica = 'Caucionada (R�pida com Registro)') then
  begin
    ACBrBoletoComp.Cedente.CaracTitulo := tcCaucionadaRapComReg;
  end
  else if (vBancoBoletos[0].Caracteristica = 'Direta Especial') then
  begin
    ACBrBoletoComp.Cedente.CaracTitulo := tcDiretaEspecial;
  end;

  if (vBancoBoletos[0].RespEmissao = 'Cliente') then
  begin
    ACBrBoletoComp.Cedente.ResponEmissao := tbCliEmite;
  end
  else if (vBancoBoletos[0].RespEmissao = 'Banco') then
  begin
    ACBrBoletoComp.Cedente.ResponEmissao := tbBancoEmite;
  end
  else if (vBancoBoletos[0].RespEmissao = 'Banco reemite') then
  begin
    ACBrBoletoComp.Cedente.ResponEmissao := tbBancoReemite;
  end
  else if (vBancoBoletos[0].RespEmissao = 'Banco n�o reemite') then
  begin
    ACBrBoletoComp.Cedente.ResponEmissao := tbBancoNaoReemite;
  end
  else if (vBancoBoletos[0].RespEmissao = 'Banco pr� emite') then
  begin
    ACBrBoletoComp.Cedente.ResponEmissao := tbBancoPreEmite;
  end;

  ACBrBoletoComp.Cedente.CNPJCPF         := RetornaNumeros(vEmpresa.Cnpj);
  ACBrBoletoComp.Cedente.Nome            := vEmpresa.RazaoSocial;
  ACBrBoletoComp.Cedente.FantasiaCedente := vEmpresa.NomeFantasia;
  ACBrBoletoComp.Cedente.Logradouro      := vEmpresa.Logradouro;
  ACBrBoletoComp.Cedente.Complemento     := vEmpresa.Complemento;
  ACBrBoletoComp.Cedente.Bairro          := vEmpresa.NomeBairro;
  ACBrBoletoComp.Cedente.CEP             := RetornaNumeros(vEmpresa.CEP);
  ACBrBoletoComp.Cedente.Cidade          := vEmpresa.NomeCidade;
  ACBrBoletoComp.Cedente.Telefone        := RetornaNumeros(vEmpresa.TelefonePrincipal);
  ACBrBoletoComp.Cedente.UF              := vEmpresa.EstadoId;

  if pTipoEmissao = teBoleto then
  begin
    vDiretorioSistema := Sessao.getCaminhoPastaSistema;
    vDiretorioSistema := IncludeTrailingPathDelimiter(vDiretorioSistema);
    ForceDirectories(vDiretorioSistema + 'Boletos');

    stlReport := TStringList.Create;
    try
      stlReport.LoadFromStream(vBancoBoletos[0].LayoutImpressao);
      stlReport.Text := RetirarAcentos(stlReport.Text);
      stlReport.SaveToFile(vDiretorioSistema + 'Boletos\Boleto.fr3',TEncoding.UTF8);
    finally
      stlReport.Free;
    end;


    ForceDirectories(vDiretorioSistema + 'Boletos\Imagens');
    vBancoBoletos[0].Logo.SaveToFile(vDiretorioSistema +'Boletos\Imagens\' +  vBancoBoletos[0].NumeroBanco.PadLeft(3,'0') + '.bmp');

    ACBrBoletoFR                := TACBrBoletoFCFR.Create(nil);
    ACBrBoletoComp.ACBrBoletoFC := ACBrBoletoFR;
    ACBrBoletoFR.FastReportFile := vDiretorioSistema + 'Boletos\Boleto.fr3';
    ACBrBoletoFR.DirLogo := vDiretorioSistema + 'Boletos\Imagens';
    ACBrBoletoFR.CalcularNomeArquivoPDFIndividual := False;

    if (vBancoBoletos[0].ModeloImpressao = 'Padr�o') then
    begin
      ACBrBoletoFR.LayOut := lPadrao;
    end
    else if (vBancoBoletos[0].ModeloImpressao = 'Carn�') then
    begin
      ACBrBoletoFR.LayOut := lCarne;
    end
    else if (vBancoBoletos[0].ModeloImpressao = 'Fatura') then
    begin
      ACBrBoletoFR.LayOut := lFatura;
    end
    else if (vBancoBoletos[0].ModeloImpressao = 'Padr�o entrega') then
    begin
      ACBrBoletoFR.LayOut := lPadraoEntrega;
    end
    else if (vBancoBoletos[0].ModeloImpressao = 'Recibo topo') then
    begin
      ACBrBoletoFR.LayOut := lReciboTopo;
    end
    else if (vBancoBoletos[0].ModeloImpressao = 'Padr�o entrega 2') then
    begin
      ACBrBoletoFR.LayOut := lPadraoEntrega2;
    end
    else if (vBancoBoletos[0].ModeloImpressao = 'Fatura detalhe') then
    begin
      ACBrBoletoFR.LayOut := lFaturaDetal;
    end
    else if (vBancoBoletos[0].ModeloImpressao = 'T�rmica 80mm') then
    begin
      ACBrBoletoFR.LayOut := lTermica80mm;
    end;

  end;
  Result := True;
end;

procedure TBoletosACBr.CarregarTitulos(
  var pTitulos: TArray<_RecordsRelatorios.RecContasReceber>);
var
  vCnt: Integer;
  vVirgula: String;
  vComando: String;

begin
  vVirgula := '';

  WhereOuAnd(vComando, ' CON.RECEBER_ID in(');

  for vCnt := Low(vParametrosImpressao.FTitulos) to High(vParametrosImpressao.FTitulos) do
  begin
    vComando := vComando + vVirgula + vParametrosImpressao.FTitulos[vCnt].ReceberId.ToString ;
    vVirgula := ' , ';
  end;

  vComando := vComando + ') ';
  pTitulos := _RelacaoContasReceber.BuscarContasReceberComando(
              Sessao.getConexaoBanco,
              vComando);
end;

procedure TBoletosACBr.carregarTitulosRetorno;
var
  I: Integer;
  J: Integer;
  vValorTitulo: Double;
begin

  ACBrBoletoComp.LerRetorno();

  if (ACBrBoletoComp.ListadeBoletos.Count = 0) then
  begin
    avisar('Boleto','N�o h� ocorr�ncias para serem processadas!');
    Exit;
  end;

  ConfigurarCdsTitulos();

  for I := 0 to ACBrBoletoComp.ListadeBoletos.Count - 1 do
  begin
    if ACBrBoletoComp.ListadeBoletos[I].ValorDocumento > 0 then
      vValorTitulo := ACBrBoletoComp.ListadeBoletos[i].ValorDocumento
    else if ACBrBoletoComp.ListadeBoletos[i].ValorPago > 0 then
      vValorTitulo := ACBrBoletoComp.ListadeBoletos[i].ValorPago
    else
      vValorTitulo := ACBrBoletoComp.ListadeBoletos[i].ValorRecebido;

    CdsTitulos.Append;
    CdsTitulos.FieldByName('DOCUMENTO_ID').AsString      := ACBrBoletoComp.ListadeBoletos[I].SeuNumero;
    CdsTitulos.FieldByName('NOSSO_NUMERO').AsString      := ACBrBoletoComp.ListadeBoletos[I].NossoNumero;
    CdsTitulos.FieldByName('CODIGO_BANCO').AsString      := ACBrBoletoComp.Banco.Numero.ToString();
    CdsTitulos.FieldByName('VALOR_TITULO').AsFloat       := vValorTitulo;
    CdsTitulos.FieldByName('VLR_DESCONTO').AsFloat       := ACBrBoletoComp.ListadeBoletos[I].ValorDesconto;
    CdsTitulos.FieldByName('VLR_CREDITO').AsFloat        := ACBrBoletoComp.ListadeBoletos[I].ValorRecebido;
    CdsTitulos.FieldByName('VLR_TAXA_COBRANCA').AsFloat  := ACBrBoletoComp.ListadeBoletos[I].ValorDespesaCobranca;
    CdsTitulos.FieldByName('VLR_JUROS').AsFloat          := ACBrBoletoComp.ListadeBoletos[I].ValorMoraJuros;
    CdsTitulos.FieldByName('VLR_ACRESCIMO').AsFloat      := ACBrBoletoComp.ListadeBoletos[I].ValorOutrosCreditos;
    CdsTitulos.FieldByName('VLR_ABATIMENTO').AsFloat     := ACBrBoletoComp.ListadeBoletos[I].ValorAbatimento;
    CdsTitulos.FieldByName('CODIGO_OCORRENCIA').AsString := ACBrBoletoComp.ListadeBoletos[I].OcorrenciaOriginal.CodigoBanco;
    CdsTitulos.FieldByName('DT_CREDITO').AsDateTime      := ACBrBoletoComp.ListadeBoletos[I].DataCredito;
    CdsTitulos.FieldByName('DT_OCORRENCIA').AsDateTime   := ACBrBoletoComp.ListadeBoletos[I].DataOcorrencia;
    CdsTitulos.FieldByName('PAGAMENTO').AsString         := IIf(ACBrBoletoComp.ListadeBoletos[I].OcorrenciaOriginal.Tipo in
                                                                [toRetornoLiquidado,
                                                                toRetornoLiquidadoAposBaixaOuNaoRegistro,
                                                                toRetornoLiquidadoEmCartorio,
                                                                toRetornoLiquidadoPorConta,
                                                                toRetornoLiquidadoSaldoRestante,
                                                                toRetornoLiquidadoSemRegistro],
                                                                'Sim',
                                                                'N�o');
    CdsTitulos.FieldByName('CONTA_CORRENTE').AsString    := ACBrBoletoComp.Cedente.Conta + '-' + ACBrBoletoComp.Cedente.ContaDigito;
    CdsTitulos.FieldByName('BANCO').AsString             := ACBrBoletoComp.Banco.Nome;

    for J := 0 to ACBrBoletoComp.ListadeBoletos[I].DescricaoMotivoRejeicaoComando.Count - 1 do
    begin
      CdsMotivosOcorrencia.Append;
      CdsMotivosOcorrencia.FieldByName('NOSSO_NUMERO').AsString := ACBrBoletoComp.ListadeBoletos[I].NossoNumero;
      CdsMotivosOcorrencia.FieldByName('CODIGO').AsString       := ACBrBoletoComp.ListadeBoletos[I].MotivoRejeicaoComando[J];
      CdsMotivosOcorrencia.FieldByName('DESCRICAO').AsString    := ACBrBoletoComp.ListadeBoletos[I].DescricaoMotivoRejeicaoComando[J];
      CdsMotivosOcorrencia.Post;
    end;

    CdsTitulos.Post;
  end;
end;

procedure TBoletosACBr.ConfigurarCdsMotivosOcorrencia;
begin
  if Assigned(CdsMotivosOcorrencia) then
  begin
    CdsMotivosOcorrencia.Free;
  end;

  CdsMotivosOcorrencia := TClientDataSet.Create(nil);
  CdsMotivosOcorrencia.FieldDefs.Add('NOSSO_NUMERO', ftString, 100);
  CdsMotivosOcorrencia.FieldDefs.Add('CODIGO', ftString, 10);
  CdsMotivosOcorrencia.FieldDefs.Add('DESCRICAO', ftString, 2000);
  CdsMotivosOcorrencia.CreateDataSet;
end;

procedure TBoletosACBr.ConfigurarCdsOcorrencias;
begin
  if Assigned(CdsOcorrencias) then
  begin
    CdsOcorrencias.Free;
  end;

  CdsOcorrencias := TClientDataSet.Create(nil);
  CdsOcorrencias.FieldDefs.Add('RAZAO_SOCIAL', ftString, 100);
  CdsOcorrencias.FieldDefs.Add('DOCUMENTO_ID', ftString, 100);
  CdsOcorrencias.FieldDefs.Add('NOSSO_NUMERO', ftString, 100);
  CdsOcorrencias.FieldDefs.Add('VLR_ABATIMENTO', ftFloat);
  CdsOcorrencias.FieldDefs.Add('VLR_TITULO', ftFloat);
  CdsOcorrencias.FieldDefs.Add('PAGAMENTO', ftString, 4);
  CdsOcorrencias.FieldDefs.Add('DESC_OCORRENCIA', ftString, 100);
  CdsOcorrencias.FieldDefs.Add('DETALHES_OCORRENCIA', ftString, 500);
  CdsOcorrencias.FieldDefs.Add('COD_OCORRENCIA', ftString, 2);
  CdsOcorrencias.FieldDefs.Add('VLR_ORIGINAL', ftFloat);
  CdsOcorrencias.FieldDefs.Add('DIFERENCA', ftFloat);
  CdsOcorrencias.CreateDataSet;

end;

procedure TBoletosACBr.ConfigurarCdsTitulos;
begin
  if Assigned(CdsTitulos) then
  begin
    CdsTitulos.Free;
  end;

  CdsTitulos := TClientDataSet.Create(nil);
  CdsTitulos.FieldDefs.Add('CADASTRO_ID', ftString, 100);
  CdsTitulos.FieldDefs.Add('RAZAO_SOCIAL', ftString, 100);
  CdsTitulos.FieldDefs.Add('DOCUMENTO_ID', ftString, 100);
  CdsTitulos.FieldDefs.Add('NOSSO_NUMERO', ftString, 100);
  CdsTitulos.FieldDefs.Add('CODIGO_BANCO', ftString, 10);
  CdsTitulos.FieldDefs.Add('DT_EMISSAO', ftString, 20);
  CdsTitulos.FieldDefs.Add('DT_VENCIMENTO', ftString, 20);
  CdsTitulos.FieldDefs.Add('DT_CREDITO', ftDate);
  CdsTitulos.FieldDefs.Add('VALOR_TITULO', ftFloat);
  CdsTitulos.FieldDefs.Add('VLR_ADIANTAMENTO', ftFloat);
  CdsTitulos.FieldDefs.Add('VLR_DESCONTO', ftFloat);
  CdsTitulos.FieldDefs.Add('VLR_CREDITO', ftFloat);
  CdsTitulos.FieldDefs.Add('VLR_TAXA_COBRANCA', ftFloat);
  CdsTitulos.FieldDefs.Add('VLR_MULTA', ftFloat);
  CdsTitulos.FieldDefs.Add('VLR_JUROS', ftFloat);
  CdsTitulos.FieldDefs.Add('VLR_ACRESCIMO', ftFloat);
  CdsTitulos.FieldDefs.Add('VLR_ABATIMENTO', ftFloat);
  CdsTitulos.FieldDefs.Add('CODIGO_OCORRENCIA', ftString, 10);
  CdsTitulos.FieldDefs.Add('DT_OCORRENCIA', ftDate);
  CdsTitulos.FieldDefs.Add('PAGAMENTO', ftString, 10);
  CdsTitulos.FieldDefs.Add('CONTA_CORRENTE', ftString, 20);
  CdsTitulos.FieldDefs.Add('BANCO', ftString, 100);
  CdsTitulos.CreateDataSet;

  ConfigurarCdsMotivosOcorrencia;

end;

function TBoletosACBr.DescricaoOcorrencia(pCodigoBanco,
  pCodigoOcorrencia: Integer): String;
begin
  case StrToIntDef(pCodigoBanco.ToString.SubString(0, 3), 0) of
    001:
      {$REGION '===> OCORR�NCIAS BANCO DO BRASIL <==='}

      case pCodigoOcorrencia of
        02: Result := 'Confirma��o de Entrada de T�tulo';
        03: Result := 'Comando recusado';
        05: Result := 'Liquidado sem registro';
        06: Result := 'Liquida��o Normal';
        07: Result := 'Liquida��o por Conta';
        08: Result := 'Liquida��o por Saldo';
        09: Result := 'Baixa de Titulo';
        10: Result := 'Baixa Solicitada';
        11: Result := 'T�tulos em Ser';
        12: Result := 'Abatimento Concedido';
        13: Result := 'Abatimento Cancelado';
        14: Result := 'Altera��o de Vencimento do t�tulo';
        15: Result := 'Liquida��o em Cart�rio';
        16: Result := 'Confirma��o de altera��o de juros de mora';
        19: Result := 'Confirma��o de recebimento de instru��es para protesto';
        20: Result := 'Debito em Conta';
        21: Result := 'Altera��o do Nome do Sacado';
        22: Result := 'Altera��o do Endere�o do Sacado';
        23: Result := 'Indica��o de encaminhamento a cart�rio';
        24: Result := 'Sustar Protesto';
        25: Result := 'Dispensar Juros de mora';
        28: Result := 'Manuten��o de titulo vencido';
        31: Result := 'Conceder desconto';
        32: Result := 'N�o conceder desconto';
        33: Result := 'Retificar desconto';
        34: Result := 'Alterar data para desconto';
        35: Result := 'Cobrar Multa';
        36: Result := 'Dispensar Multa';
        37: Result := 'Dispensar Indexador';
        38: Result := 'Dispensar prazo limite para recebimento';
        39: Result := 'Alterar prazo limite para recebimento';
        44: Result := 'T�tulo pago com cheque devolvido';
        46: Result := 'T�tulo pago com cheque, aguardando  compensa��o';
        72: Result := 'Altera��o de tipo de cobran�a';
        85: Result := 'Inclus�o de Negativa��o';
        86: Result := 'Exclus�o de Negativa��o';
        96: Result := 'Despesas de Protesto';
        97: Result := 'Despesas de Susta��o de Protesto';
        98: Result := 'Debito de Custas Antecipadas';
      else
        Result := 'MOTIVO DESCONHECIDO';
      end;

      {$ENDREGION}

    104:
      {$REGION '===> OCORR�NCIAS CAIXA <==='}

      case pCodigoOcorrencia of
        01: Result := 'Solicita��o de Impress�o de T�tulos Confirmada';
        02: Result := 'Entrada Confirmada';
        03: Result := 'Entrada Rejeitada';
        04: Result := 'Transfer�ncia de Carteira/Entrada';
        05: Result := 'Transfer�ncia de Carteira/Baixa';
        06: Result := 'Liquida��o';
        07: Result := 'Confirma��o do Recebimento da Instru��o de Desconto';
        08: Result := 'Confirma��o do Recebimento do Cancelamento do Desconto';
        09: Result := 'Baixa';
        12: Result := 'Confirma��o Recebimento Instru��o de Abatimento';
        13: Result := 'Confirma��o Recebimento Instru��o de Cancelamento Abatimento';
        14: Result := 'Confirma��o Recebimento Instru��o Altera��o de Vencimento';
        19: Result := 'Confirma��o Recebimento Instru��o de Protesto';
        20: Result := 'Confirma��o Recebimento Instru��o de Susta��o/Cancelamento de Protesto';
        23: Result := 'Remessa a Cart�rio';
        24: Result := 'Retirada de Cart�rio';
        25: Result := 'Protestado e Baixado (Baixa por Ter Sido Protestado)';
        26: Result := 'Instru��o Rejeitada';
        27: Result := 'Confirma��o do Pedido de Altera��o de Outros Dados';
        28: Result := 'D�bito de Tarifas/Custas';
        30: Result := 'Altera��o de Dados Rejeitada';
        35: Result := 'Confirma��o de Inclus�o Banco de Sacado';
        36: Result := 'Confirma��o de Altera��o Banco de Sacado';
        37: Result := 'Confirma��o de Exclus�o Banco de Sacado';
        38: Result := 'Emiss�o de Bloquetos de Banco de Sacado';
        39: Result := 'Manuten��o de Sacado Rejeitada';
        40: Result := 'Entrada de T�tulo via Banco de Sacado Rejeitada';
        41: Result := 'Manuten��o de Banco de Sacado Rejeitada';
        44: Result := 'Estorno de Baixa / Liquida��o';
        45: Result := 'Altera��o de Dados';
      else
        Result := 'MOTIVO DESCONHECIDO';
      end;

      {$ENDREGION}

    341:
      {$REGION '===> OCORR�NCIAS ITA� <==='}

      case pCodigoOcorrencia of
        02: Result := 'ENTRADA CONFIRMADA';
        03: Result := 'ENTRADA REJEITADA';
        04: Result := 'ALTERA��O DE DADOS - NOVA ENTRADA';
        05: Result := 'ALTERA��O DE DADOS � BAIXA';
        06: Result := 'LIQUIDA��O NORMAL';
        07: Result := 'LIQUIDA��O PARCIAL � COBRAN�A INTELIGENTE (B2B)';
        08: Result := 'LIQUIDA��O EM CART�RIO';
        09: Result := 'BAIXA SIMPLES';
        10: Result := 'BAIXA POR TER SIDO LIQUIDADO';
        11: Result := 'EM SER (S� NO RETORNO MENSAL)';
        12: Result := 'ABATIMENTO CONCEDIDO';
        13: Result := 'ABATIMENTO CANCELADO';
        14: Result := 'VENCIMENTO ALTERADO';
        15: Result := 'BAIXAS REJEITADAS';
        16: Result := 'INSTRU��ES REJEITADAS';
        17: Result := 'ALTERA��O/EXCLUS�O DE DADOS REJEITADOS';
        18: Result := 'COBRAN�A CONTRATUAL';
        19: Result := 'CONFIRMA INSTRU��O DE PROTESTO';
        20: Result := 'CONFIRMA INSTRU��O DE SUSTA��O DE PROTESTO /TARIFA';
        21: Result := 'CONFIRMA INSTRU��O DE N�O PROTESTAR';
        23: Result := 'T�TULO ENVIADO A CART�RIO/TARIFA';
        24: Result := 'INSTRU��O DE PROTESTO REJEITADA / SUSTADA / PENDENTE';
        25: Result := 'ALEGA��ES DO SACADO';
        26: Result := 'TARIFA DE AVISO DE COBRAN�A';
        27: Result := 'TARIFA DE EXTRATO POSI��O (B40X)';
        28: Result := 'TARIFA DE RELA��O DAS LIQUIDA��ES';
        29: Result := 'TARIFA DE MANUTEN��O DE T�TULOS VENCIDOS';
        30: Result := 'D�BITO MENSAL DE TARIFAS (PARA ENTRADAS E BAIXAS)';
        32: Result := 'BAIXA POR TER SIDO PROTESTADO';
        33: Result := 'CUSTAS DE PROTESTO';
        34: Result := 'CUSTAS DE SUSTA��O';
        35: Result := 'CUSTAS DE CART�RIO DISTRIBUIDOR';
        36: Result := 'CUSTAS DE EDITAL';
        37: Result := 'TARIFA DE EMISS�O DE BOLETO/TARIFA DE ENVIO DE DUPLICATA';
        38: Result := 'TARIFA DE INSTRU��O';
        39: Result := 'TARIFA DE OCORR�NCIAS';
        40: Result := 'TARIFA MENSAL DE EMISS�O DE BOLETO/TARIFA MENSAL DE ENVIO DE DUPLICATA';
        41: Result := 'D�BITO MENSAL DE TARIFAS � EXTRATO DE POSI��O (B4EP/B4OX)';
        42: Result := 'D�BITO MENSAL DE TARIFAS � OUTRAS INSTRU��ES';
        43: Result := 'D�BITO MENSAL DE TARIFAS � MANUTEN��O DE T�TULOS VENCIDOS';
        44: Result := 'D�BITO MENSAL DE TARIFAS � OUTRAS OCORR�NCIAS';
        45: Result := 'D�BITO MENSAL DE TARIFAS � PROTESTO';
        46: Result := 'D�BITO MENSAL DE TARIFAS � SUSTA��O DE PROTESTO';
        47: Result := 'BAIXA COM TRANSFER�NCIA PARA DESCONTO';
        48: Result := 'CUSTAS DE SUSTA��O JUDICIAL';
        51: Result := 'TARIFA MENSAL REF A ENTRADAS BANCOS CORRESPONDENTES NA CARTEIRA';
        52: Result := 'TARIFA MENSAL BAIXAS NA CARTEIRA';
        53: Result := 'TARIFA MENSAL BAIXAS EM BANCOS CORRESPONDENTES NA CARTEIRA';
        54: Result := 'TARIFA MENSAL DE LIQUIDA��ES NA CARTEIRA';
        55: Result := 'TARIFA MENSAL DE LIQUIDA��ES EM BANCOS CORRESPONDENTES NA CARTEIRA';
        56: Result := 'CUSTAS DE IRREGULARIDADE';
        57: Result := 'INSTRU��O CANCELADA';
        59: Result := 'BAIXA POR CR�DITO EM C/C ATRAV�S DO SISPAG';
        60: Result := 'ENTRADA REJEITADA CARN�';
        61: Result := 'TARIFA EMISS�O AVISO DE MOVIMENTA��O DE T�TULOS';
        62: Result := 'D�BITO MENSAL DE TARIFA - AVISO DE MOVIMENTA��O DE T�TULOS';
        63: Result := 'T�TULO SUSTADO JUDICIALMENTE';
        64: Result := 'ENTRADA CONFIRMADA COM RATEIO DE CR�DITO';
        69: Result := 'CHEQUE DEVOLVIDO';
        71: Result := 'ENTRADA REGISTRADA, AGUARDANDO AVALIA��O';
        72: Result := 'BAIXA POR CR�DITO EM C/C ATRAV�S DO SISPAG';
        73: Result := 'CONFIRMA��O DE ENTRADA NA COBRAN�A SIMPLES';
        74: Result := 'INSTRU��O DE NEGATIVA��O EXPRESSA REJEITADA (NOTA 20 TABELA 11)';
        75: Result := 'CONFIRMA��O DE RECEBIMENTO DE INSTRU��O DE ENTRADA EM NEGATIVA��O EXPRESSA';
        76: Result := 'CHEQUE COMPENSADO';
        77: Result := 'CONFIRMA��O DE RECEBIMENTO DE INSTRU��O DE EXCLUS�O DE ENTRADA EM NEGATIVA��O EXPRESSA';
        78: Result := 'CONFIRMA��O DE RECEBIMENTO DE INSTRU��O DE CANCELAMENTO DE NEGATIVA��O EXPRESSA';
        79: Result := 'NEGATIVA��O EXPRESSA INFORMACIONAL (NOTA 20 TABELA 12)';
        80: Result := 'CONFIRMA��O DE ENTRADA EM NEGATIVA��O EXPRESSA TARIFA';
        82: Result := 'CONFIRMA��O DO CANCELAMENTO DE NEGATIVA��O EXPRESSA TARIFA';
        83: Result := 'CONFIRMA��O DE EXCLUS�O DE ENTRADA EM NEGATIVA��O EXPRESSA POR LIQUIDA��O TARIFA';
        85: Result := 'TARIFA POR BOLETO (AT� 03 ENVIOS) COBRAN�A ATIVA ELETR�NICA';
        86: Result := 'TARIA EMAIL COBRAN�A ATIVA ELETR�NICA';
        87: Result := 'TARIFA SMS COBRAN�A ATIVA ELETR�NICA';
        88: Result := 'TARIFA MENSAL POR BOLETO (AT� 03 ENVIOS) COBRAN�A ATIVA ELETR�NICA';
        89: Result := 'TARIFA MENSAL EMAIL COBRAN�A ATIVA ELETR�NICA';
        90: Result := 'TARIFA MENSAL SMS COBRAN�A ATIVA ELETR�NICA';
        91: Result := 'TARIFA MENSAL DE EXCLUS�O DE ENTRADA DE NEGATIVA��O EXPRESSA';
        92: Result := 'TARIFA MENSAL DE CANCELAMENTO DE NEGATIVA��O EXPRESSA';
        93: Result := 'TARIFA MENSAL DE EXCLUS�O DE NEGATIVA��O EXPRESSA POR LIQUIDA��O';
      else
        Result := 'MOTIVO DESCONHECIDO';
      end;

      {$ENDREGION}

    237:
      {$REGION '===> OCORR�NCIAS BRADESCO <==='}

      case pCodigoOcorrencia of
        02: Result := 'Entrada Confirmada';
        03: Result := 'Entrada Rejeitada';
        06: Result := 'Liquida��o  normal';
        09: Result := 'Baixado Automat. via Arquivo';
        10: Result := 'Baixado conforme instru��es da Ag�ncia';
        11: Result := 'Em Ser - Arquivo de T�tulos pendentes';
        12: Result := 'Abatimento Concedido';
        13: Result := 'Abatimento Cancelado';
        14: Result := 'Vencimento Alterado';
        15: Result := 'Liquida��o em Cart�rio';
        16: Result := 'T�tulo Pago em Cheque � Vinculado';
        17: Result := 'Liquida��o ap�s baixa ou T�tulo n�o registrado';
        18: Result := 'Acerto de Deposit�ria';
        19: Result := 'Confirma��o Receb. Inst. de Protesto';
        20: Result := 'Confirma��o Recebimento Instru��o Susta��o de Protesto';
        21: Result := 'Acerto do Controle do Participante';
        22: Result := 'T�tulo Com Pagamento Cancelado';
        23: Result := 'Entrada do T�tulo em Cart�rio';
        24: Result := 'Entrada rejeitada por CEP Irregular';
        25: Result := 'Confirma��o Receb.Inst.de Protesto Falimentar';
        27: Result := 'Baixa Rejeitada';
        28: Result := 'D�bito de tarifas/custas';
        29: Result := 'Ocorr�ncias do Pagador (NOVO)';
        30: Result := 'Altera��o de Outros Dados Rejeitados';
        32: Result := 'Instru��o Rejeitada';
        33: Result := 'Confirma��o Pedido Altera��o Outros Dados';
        34: Result := 'Retirado de Cart�rio e Manuten��o Carteira';
        35: Result := 'Desagendamento do d�bito autom�tico';
        40: Result := 'Estorno de pagamento';
        55: Result := 'Sustado judicial';
        68: Result := 'Acerto dos dados do rateio de Cr�dito';
        69: Result := 'Cancelamento dos dados do rateio';
      else
        Result := 'MOTIVO DESCONHECIDO';
      end;

      {$ENDREGION}

    422:
      {$REGION '===> OCORR�NCIAS SAFRA <==='}

      case pCodigoOcorrencia of
        02: Result := 'ENTRADA CONFIRMADA';
        03: Result := 'ENTRADA REJEITADA';
        04: Result := 'TRANSF. DE CARTEIRA (ENTRADA)';
        05: Result := 'TRANSF. DE CARTEIRA (BAIXA)';
        06: Result := 'LIQUIDA��O NORMAL';
        09: Result := 'BAIXADO AUTO.';
        10: Result := 'BAIXADO CONFORME INSTRU��ES';
        11: Result := 'T�TULOS EM SER (PARA ARQUIVO MENSAL)';
        12: Result := 'ABATIMENTO CONCEDIDO';
        13: Result := 'ABATIMENTO CANCELADO';
        14: Result := 'VENCIMENTO ALTERADO';
        15: Result := 'LIQUIDA��O EM CART�RIO';
        19: Result := 'CONFIRMA��O DE INSTRU��O DE PROTESTO';
        20: Result := 'CONFIRMA��O DE SUSTAR PROTESTO';
        21: Result := 'TRANSFER�NCIA DE BENEFICI�RIO';
        23: Result := 'T�TULO ENVIADO A CART�RIO';
        40: Result := 'BAIXA DE T�TULO PROTESTADO';
        41: Result := 'LIQUIDA��O DE T�TULO BAIXADO';
        42: Result := 'T�TULO RETIRADO DO CART�RIO';
        43: Result := 'DESPESA DE CART�RIO';
        44: Result := 'ACEITE DO T�TULO DDA PELO PAGADOR';
        45: Result := 'N�O ACEITE DO T�TULO DDA PELO PAGADOR';
        51: Result := 'VALOR DO T�TULO ALTERADO';
        52: Result := 'ACERTO DE DATA DE EMISSAO';
        53: Result := 'ACERTO DE COD ESPECIE DOCTO';
        54: Result := 'ALTERACAO DE SEU NUMERO';
        56: Result := 'INSTRU��O NEGATIVA��O ACEITA';
        57: Result := 'INSTRU��O BAIXA DE NEGATIVA��O ACEITA';
        58: Result := 'INSTRU��O N�O NEGATIVAR ACEITA';
      else
        Result := 'MOTIVO DESCONHECIDO';
      end;

      {$ENDREGION}

  else

    {$REGION '===> OCORR�NCIAS PADR�O FEBRABAN <==='}

    case pCodigoOcorrencia of
      02: Result := 'Entrada confirmada.';
      03: Result := 'Entrada rejeitada.';
      04: Result := 'Transfer�ncia de carteira/entrada.';
      05: Result := 'Transfer�ncia de carteira/baixa.';
      06: Result := 'Liquida��o.';
      09: Result := 'Baixa.';
      11: Result := 'T�tulos em carteira (em ser).';
      12: Result := 'Confirma��o recebimento instru��o de abatimento.';
      13: Result := 'Confirma��o recebimento instru��o de cancelamento abatimento.';
      14: Result := 'Confirma��o recebimento instru��o altera��o de vencimento.';
      15: Result := 'Franco de pagamento.';
      17: Result := 'Liquida��o ap�s baixa ou liquida��o t�tulo n�o registrado.';
      19: Result := 'Confirma��o recebimento instru��o de protesto.';
      20: Result := 'Confirma��o recebimento instru��o de susta��o/cancelamento de protesto.';
      23: Result := 'Remessa a cart�rio (aponte em cart�rio).';
      24: Result := 'Retirada de cart�rio e manuten��o em carteira.';
      25: Result := 'Protestado e baixado (baixa por ter sido protestado).';
      26: Result := 'Instru��o rejeitada.';
      27: Result := 'Confirma��o do pedido de altera��o de outros dados.';
      28: Result := 'D�bito de tarifas/custas.';
      29: Result := 'Ocorr�ncias do sacado.';
      30: Result := 'Altera��o de dados rejeitada.';
      48: Result := 'Transfer�ncia de carteira/modalidade de cobran�a';
    else
      Result := 'MOTIVO DESCONHECIDO';
    end;

    {$ENDREGION}

  end;
end;

procedure TBoletosACBr.EmitirBoletas(pParams: TParametrosImpressao);
var
  vCnt: Integer;
  vCodigoBarras: String;

  vTitulos: TArray<_RecordsRelatorios.RecContasReceber>;
begin
  inherited;
  vParametrosImpressao := pParams;
  CarregarTitulos(vTitulos);
  vContaBoletoID := vTitulos[0].ContasBoletosId;

  carregarContaBoletos(teBoleto, vTitulos[0].empresa_id);
  ACBrBoletoComp.ListadeBoletos.Clear;

  AdicionaBoletoComponente(vTitulos);

  if ACBrBoletoComp.ListadeBoletos.Count = 0 then
  begin
    Exit;
  end;

  try
    if not Sessao.getConexaoBanco.EmTransacao then
      Sessao.getConexaoBanco.IniciarTransacao;

    if not TravarProximoNossoNumero then
    begin
      Abort;
    end;

    for vCnt := 0 to ACBrBoletoComp.ListadeBoletos.Count - 1 do
    begin
      if (StrToInt64Def(ACBrBoletoComp.ListadeBoletos[vCnt].NossoNumero, 0) = 0) or (pParams.FItensImpressao.GerarNovoNossoNumero) then
      begin
        ACBrBoletoComp.ListadeBoletos[vCnt].NossoNumero := ProximoNossoNumero;
        ProximoNossoNumero := IntToStr(StrToInt64Def(ProximoNossoNumero, 0) + 1);
      end;
      vCodigoBarras := ACBrBoletoComp.Banco.MontarCodigoBarras(ACBrBoletoComp.ListadeBoletos[vCnt]);
      AtualizarDadosTabelas(ACBrBoletoComp.ListadeBoletos[vCnt].SeuNumero.ToInteger,
                            ACBrBoletoComp.ListadeBoletos[vCnt].NossoNumero,
                            vCodigoBarras,
                            ACBrBoletoComp.Banco.MontarLinhaDigitavel(vCodigoBarras, ACBrBoletoComp.ListadeBoletos[vCnt]));
    end;

    if Sessao.getConexaoBanco.EmTransacao then
    begin
      Sessao.getConexaoBanco.FinalizarTransacao;
    end;

    Imprimir;
    if pParams.FItensImpressao.EnviarBoletoEmail then
      EnviarBoletoAnexo;
  except
    on E: Exception do
    begin
      if Sessao.getConexaoBanco.EmTransacao then
        Sessao.getConexaoBanco.VoltarTransacao;
      if E.ClassName = 'EAbort' then
        Exit;
    end;
  end;
end;

procedure TBoletosACBr.EnviarBoletoAnexo;
var
  vDiretorioSistema: string;
  vAnexo: TStringList;
begin
  if ACBrBoletoComp.ListadeBoletos[0].Sacado.Email.IsEmpty then
  begin
    avisar('Boleto','Destinat�rio Inv�lido!');
    exit;
  end;
  vAnexo := TStringList.Create;
  try
    vDiretorioSistema := Sessao.getCaminhoPastaSistema;
    vDiretorioSistema := IncludeTrailingPathDelimiter(vDiretorioSistema)+'Boletos';
    ForceDirectories(vDiretorioSistema);

    ACBrBoletoComp.ACBrBoletoFC.MostrarSetup := False;
    ACBrBoletoComp.ACBrBoletoFC.NomeArquivo  := IncludeTrailingBackslash(vDiretorioSistema) + 'Boleta_'+ACBrBoletoComp.ListadeBoletos[0].SeuNumero + '.PDF';
    ACBrBoletoComp.ACBrBoletoFC.GerarPDF;

    vAnexo.Add(ACBrBoletoComp.ACBrBoletoFC.NomeArquivo);
    EnviarEmail(ACBrBoletoComp.ListadeBoletos[0].Sacado.Email,
                'Boleto: ',
                IIf(RetornaNumeros(Sessao.getEmpresaLogada.Cnpj) = '33433422000100',
                   retornaMensagemHiva(ACBrBoletoComp.ListadeBoletos[0].ValorDocumento,
                                       ACBrBoletoComp.ListadeBoletos[0].Vencimento)
                   ,'Segue em anexo seu boleto! Obrigado pela prefer�ncia!'),
                vAnexo);
    vAnexo.Free;
  except
    on E: Exception do
    begin
      avisar('Boleto', e.Message);
    end;
  end;
end;

procedure TBoletosACBr.ExibirPreviewBoleto;
begin
  ACBrBoletoComp.ACBrBoletoFC.Imprimir;
end;

procedure TBoletosACBr.GerarArquivoRemessa(pParams: TParametrosImpressao);
var
  vCnt: Integer;
  vTitulos: TArray<_RecordsRelatorios.RecContasReceber>;
  vRetBanco: RecRetornoBD;
  vRemessaId: Integer;
begin
  vParametrosImpressao := pParams;
  CarregarTitulos(vTitulos);
  vContaBoletoID := vTitulos[0].ContasBoletosId;
  CarregarContaBoletos(teArquivo, vTitulos[0].empresa_id);
  AdicionaBoletoComponente(vTitulos);

  if not Assigned(ACBrBoletoComp) then
    Exit;

  if (ACBrBoletoComp.ListadeBoletos.Count = 0) then
    Exit;

  ACBrBoletoComp.DirArqRemessa := ExtractFileDir(MontaCaminhoArquivo(ACBrBoletoComp.NomeArqRemessa));


  ACBrBoletoComp.GerarRemessa(vSequencialRemessa);

  vRetBanco := _RemessasCobrancas.AtualizarRemessasCobrancas(Sessao.getConexaoBanco,
                                                             0,
                                                             '',
                                                             vSequencialRemessa,
                                                             Length(vTitulos),
                                                             now,
                                                             now,
                                                             vContaBoletoID);
  vRemessaId := vRetBanco.AsInt;

  vRetBanco := _RemessasCobrancas.AtualizarDadosRemessaFinanceiro(Sessao.getConexaoBanco,
                                                                  vRemessaId,
                                                                  vTitulos,
                                                                  ACBrBoletoComp.NomeArqRemessa);

  vRetBanco := _ContasBoletos.AtualizarProximoNumeroRemessa(Sessao.getConexaoBanco,
                                                            vContaBoletoID,
                                                            vSequencialRemessa + 1);

end;

procedure TBoletosACBr.GerarPDFDiretorio;
var
  I: Integer;
  vNomePdf: string;
begin
  for I := 0 to ACBrBoletoComp.ListadeBoletos.Count - 1 do
  begin
    vNomePdf := 'Boleto_' + ACBrBoletoComp.ListadeBoletos[I].Sacado.NomeSacado;
    vNomePdf := Concat(vNomePdf,'_',ACBrBoletoComp.ListadeBoletos[I].SeuNumero,'.pdf');

    ACBrBoletoComp.ACBrBoletoFC.IndiceImprimirIndividual := I;
    ACBrBoletoComp.ACBrBoletoFC.NomeArquivo := Concat(IncludeTrailingPathDelimiter(vParametrosImpressao.FItensImpressao.CaminhoBoletoPDF),vNomePdf);
    ACBrBoletoComp.GerarPDF;
  end;

end;

procedure TBoletosACBr.Imprimir;
begin

  if (not vParametrosImpressao.FItensImpressao.Impressora.IsEmpty) and (vParametrosImpressao.FItensImpressao.CaminhoBoletoPDF.IsEmpty) then
  begin
    ImprimirSemPreview();
    Exit;
  end;

  if not vParametrosImpressao.FItensImpressao.CaminhoBoletoPDF.IsEmpty then
  begin
    GerarPDFDiretorio();
    Exit;
  end;

  ExibirPreviewBoleto();
end;

procedure TBoletosACBr.ImprimirSemPreview;
begin
  ACBrBoletoComp.ACBrBoletoFC.MostrarPreview := False;
  ACBrBoletoComp.ACBrBoletoFC.MostrarSetup := False;
  ACBrBoletoFR.Impressora := vParametrosImpressao.FItensImpressao.Impressora;
  ACBrBoletoComp.ACBrBoletoFC.Imprimir;
end;

function TBoletosACBr.MontaCaminhoArquivo(pNomeArquivo: String): string;
var
  SaveDialog : TSaveDialog;
  xArquivo:string;
  xExtensao:String;
begin
  result := pNomeArquivo;
  xExtensao  := ExtractFileExt( pNomeArquivo );
  xArquivo   := ChangeFileExt( pNomeArquivo,'');

  if Trim(xArquivo)='' then
    xArquivo := FormatDateTime('YYYYMMDD',Date());

  try
    SaveDialog := TSaveDialog.Create(nil);
    SaveDialog.FileName := xArquivo + xExtensao;
    if xExtensao <> '' then
    begin
      SaveDialog.DefaultExt := '';
      SaveDialog.Filter := '';
    end;

    if not SaveDialog.Execute() then
      Exit;

    result := SaveDialog.FileName;
  finally
    FreeAndNil(SaveDialog);
  end;

end;

function TBoletosACBr.MontaNomeArquivo(pNomeArquivo, pExtensao: String): string;
var
  vNomeRem : String;
begin
  vNomeRem := Trim(pNomeArquivo);
  if vNomeRem.IsEmpty then
    vNomeRem := 'DDMMAAAA';

  vNomeRem := StringReplace(vNomeRem, ' ', '', [rfReplaceAll,rfIgnoreCase]);
  vNomeRem := StringReplace(vNomeRem, 'DDMMAAAA' ,   FormatDateTime('DDMMYYYY' , Date),    [rfReplaceAll,rfIgnoreCase]);
  vNomeRem := StringReplace(vNomeRem, 'AAAAMMDD' ,   FormatDateTime('YYYYMMDD' , Date),    [rfReplaceAll,rfIgnoreCase]);
  vNomeRem := StringReplace(vNomeRem, 'DD_MM_AAAA' , FormatDateTime('DD_MM_YYYY' , Date),  [rfReplaceAll,rfIgnoreCase]);
  vNomeRem := StringReplace(vNomeRem, 'AAAA_MM_DD' , FormatDateTime('YYYY_MM_DD' , Date),  [rfReplaceAll,rfIgnoreCase]);
  result := vNomeRem+'.'+pExtensao;

end;

function TBoletosACBr.retornaMensagemHiva(pValor: Double; pVencimento: TDateTime): string;
begin
  Result :=  '<!DOCTYPE html>'
            +'<html lang="pt-BR">'
            +'<head>'
            +'<meta charset="UTF-8">'
            +'<meta name="viewport" content="width=device-width, initial-scale=1.0">'
            +'<title>Boleto Banc�rio</title>'
            +'<style>'
            +'body {'
            +'font-family: Arial, sans-serif;'
            +'background-color: #f4f4f4;'
            +'color: #333;'
            +'margin: 0;'
            +'padding: 20px;'
            +'}'
            +'.container {'
            +'background-color: #fff;'
            +'border-radius: 5px;'
            +'box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);'
            +'padding: 20px;'
            +'max-width: 600px;'
            +'margin: auto;'
            +'}'
            +'h1 {'
            +'color: #007bff;'
            +'}'
            +'.boleto {'
            +'background-color: #e9ecef;'
            +'padding: 10px;'
            +'border-radius: 5px;'
            +'margin-top: 20px;'
            +'}'
            +'.footer {'
            +'margin-top: 20px;'
            +'font-size: 0.9em;'
            +'text-align: center;'
            +'}'
            +'</style>'
            +'</head>'
            +'<body>'
            +'<div class="container">'
            +'<h1>Boleto Banc�rio</h1>'
            +'<p>Prezado(a) Cliente,</p>'
            +'<p>Segue em anexo o boleto banc�rio referente a sua mensalidade.</p>'
            +'<div class="boleto">'
            +'<p><strong>Valor:</strong> R$ '+ FormatFloat('#,##0.00',pValor)+' </p>'
            +'<p><strong>Vencimento:</strong> '+DateToStr(pVencimento)+'</p>'
//            +'<p><strong>Nosso N�mero:</strong> 123456789</p>'
//            +'<p><strong>C�digo de Barras:</strong> 12345.67890 12345.678901 23456.789012 3 12340000000000</p>'
            +'</div>'
            +'<p>Sr cliente, por gentileza efetuar o pagamento at� a data de vencimento para evitar juros e multas.</p>'
            +'<p>Atenciosamente,<br>Dep. Financeiro Hiva Sistema</p>'
           // +'<div class="footer">'
            +'<p><strong>Whatsapp:(62)98606-8188</strong></p>'
            +'</div>'
            +'</div>'
            +'</body>'
            +'</html>';
end;

procedure TBoletosACBr.LerArquivoRetorno(pParams: TParametrosImpressao);
var
  vComando: string;
  FTitulos: TArray<_RecordsRelatorios.RecContasReceber>;
  vCodOcorrencia: Integer;
  vlrPago, vlrJuros, vlrMulta: Double;
  vRetBanco: RecRetornoBD;
begin
  vParametrosImpressao := pParams;
  vContaBoletoID := pParams.ContasBoletosId;
  if not CarregarContaBoletos(teArquivo,Sessao.getEmpresaLogada.EmpresaId, pParams.CaminhoArqRetorno) then
  begin
    Abort;
  end;

  carregarTitulosRetorno;

  ConfigurarCdsOcorrencias;

  CdsTitulos.First;
  while not CdsTitulos.Eof do
  begin

    vComando :=
      'where CON.PORTADOR_ID =  ' + PortadorId + ' ' +
      'and CON.NOSSO_NUMERO = ' + CdsTitulos.FieldByName('NOSSO_NUMERO').AsString.QuotedString +
      'and CON.EMPRESA_ID = ' + IntToStr(Sessao.getEmpresaLogada.EmpresaId) + ' ' +
      'order by ' +
      '  CON.DATA_VENCIMENTO desc ';

    FTitulos := _RelacaoContasReceber.BuscarContasReceberComando(
      Sessao.getConexaoBanco,
      vComando
    );



    vlrPago       := Iif(CdsTitulos.FieldByName('VALOR_TITULO').AsFloat = 0,
                         CdsTitulos.FieldByName('VLR_CREDITO').AsFloat,
                         CdsTitulos.FieldByName('VALOR_TITULO').AsFloat);
    vlrTaxasTot   := vlrTaxasTot + CdsTitulos.FieldByName('VLR_TAXA_COBRANCA').AsFloat;
    vlrJuros      := CdsTitulos.FieldByName('VLR_JUROS').AsFloat;
    vlrMulta      := CdsTitulos.FieldByName('VLR_MULTA').AsFloat;

    if vlrJuros < 0 then
      vlrJuros := 0;

    if vlrMulta < 0 then
      vlrMulta := 0;

    if FTitulos <> nil then
    begin
      if TryStrToInt(CdsTitulos.FieldByName('CODIGO_OCORRENCIA').AsString, vCodOcorrencia) then
      begin
        cdsOcorrencias.Append;

        cdsOcorrencias.FieldByName('RAZAO_SOCIAL').AsString  := RPad(FTitulos[0].nome_cliente,30);
        cdsOcorrencias.FieldByName('DOCUMENTO_ID').AsString  := RPad(FTitulos[0].documento,12);
        cdsOcorrencias.FieldByName('NOSSO_NUMERO').AsString  := RPad(FTitulos[0].nosso_numero,21);
        cdsOcorrencias.FieldByName('VLR_ABATIMENTO').AsFloat := vlrJuros + CdsTitulos.FieldByName('VLR_ABATIMENTO').AsFloat;
        cdsOcorrencias.FieldByName('VLR_TITULO').AsFloat     := vlrPago;

        if cdsOcorrencias.FieldByName('VLR_TITULO').AsFloat = 0 then
        begin
          cdsOcorrencias.FieldByName('VLR_TITULO').AsFloat := FTitulos[0].valor_documento;
        end;

        cdsOcorrencias.FieldByName('PAGAMENTO').AsString := CdsTitulos.FieldByName('PAGAMENTO').AsString;
        cdsOcorrencias.FieldByName('COD_OCORRENCIA').AsString := CdsTitulos.FieldByName('CODIGO_OCORRENCIA').AsString;
        cdsOcorrencias.FieldByName('DESC_OCORRENCIA').AsString := RPad(DescricaoOcorrencia(StrToIntDef(RetornaNumeros(CdsTitulos.FieldByName('CODIGO_BANCO').AsString),0), vCodOcorrencia), 40);

        cdsOcorrencias.FieldByName('DETALHES_OCORRENCIA').AsString := '';
        CdsMotivosOcorrencia.Filtered := False;
        CdsMotivosOcorrencia.Filter   := 'NOSSO_NUMERO = ' + QuotedStr(CdsTitulos.FieldByName('NOSSO_NUMERO').AsString);
        CdsMotivosOcorrencia.Filtered := True;
        if (not CdsMotivosOcorrencia.IsEmpty) then
        begin
          CdsMotivosOcorrencia.First;
          while not CdsMotivosOcorrencia.Eof do
          begin
            if CdsMotivosOcorrencia.FieldByName('CODIGO').AsString = '00' then
            begin
              CdsMotivosOcorrencia.Next;
              Continue;
            end;

            cdsOcorrencias.FieldByName('DETALHES_OCORRENCIA').AsString := cdsOcorrencias.FieldByName('DETALHES_OCORRENCIA').AsString + '; ' +
              CdsMotivosOcorrencia.FieldByName('DESCRICAO').AsString;
            CdsMotivosOcorrencia.Next;
          end;
          cdsOcorrencias.FieldByName('DETALHES_OCORRENCIA').AsString := cdsOcorrencias.FieldByName('DETALHES_OCORRENCIA').AsString.SubString(2);
        end;

        cdsOcorrencias.Post;
      end;

      if (CdsTitulos.FieldByName('PAGAMENTO').AsString = 'Sim') then
      begin
        vlrJurosTot := vlrJurosTot + vlrJuros + vlrMulta;
        vlrTaxasTot := vlrTaxasTot + CdsTitulos.FieldByName('VLR_TAXA_COBRANCA').AsFloat;
        vlrPagoTot  := vlrPagoTot + vlrPago;

        if Arredondar(vlrPago,2) < Arredondar(FTitulos[0].valor_documento - FTitulos[0].ValorAdiantado, 2) then
        begin
          cdsOcorrencias.Edit;
          cdsOcorrencias.FieldByName('VLR_ORIGINAL').AsFloat := FTitulos[0].valor_documento - FTitulos[0].ValorAdiantado;
          cdsOcorrencias.FieldByName('DIFERENCA').AsFloat := cdsOcorrencias.FieldByName('VLR_ORIGINAL').AsFloat - vlrPago;
          cdsOcorrencias.Post;
        end;

        vRetBanco := _ContasReceber.AtualizarContaReceberArquivoRetorno(Sessao.getConexaoBanco,
                                                                        FTitulos[0].receber_id,
                                                                        CdsTitulos.FieldByName('DT_OCORRENCIA').AsDateTime,
                                                                        vlrJuros,
                                                                        vlrMulta);
      end;
    end else
    begin
      cdsOcorrencias.Append;

      cdsOcorrencias.FieldByName('RAZAO_SOCIAL').AsString    := RPad('T�TULO N�O ENCONTRADO',30);
      cdsOcorrencias.FieldByName('DOCUMENTO_ID').AsString    := RPad(CdsTitulos.FieldByName('DOCUMENTO_ID').AsString,12);
      cdsOcorrencias.FieldByName('NOSSO_NUMERO').AsString    := RPad(CdsTitulos.FieldByName('NOSSO_NUMERO').AsString,21);
      cdsOcorrencias.FieldByName('VLR_ABATIMENTO').AsFloat   := vlrJuros + vlrMulta;
      cdsOcorrencias.FieldByName('VLR_TITULO').AsFloat       := vlrPago;
      cdsOcorrencias.FieldByName('PAGAMENTO').AsString       := CPad('N�o', 4);
      cdsOcorrencias.FieldByName('COD_OCORRENCIA').AsString  := CdsTitulos.FieldByName('CODIGO_OCORRENCIA').AsString;
      cdsOcorrencias.FieldByName('DESC_OCORRENCIA').AsString := RPad('N�O ENCONTRADO', 40);

      CdsMotivosOcorrencia.Filtered := False;
      CdsMotivosOcorrencia.Filter   := 'NOSSO_NUMERO = ' + QuotedStr(CdsTitulos.FieldByName('NOSSO_NUMERO').AsString);
      CdsMotivosOcorrencia.Filtered := True;
      if (not CdsMotivosOcorrencia.IsEmpty) then
      begin
        CdsMotivosOcorrencia.First;
        while not CdsMotivosOcorrencia.Eof do
        begin
          if CdsMotivosOcorrencia.FieldByName('CODIGO').AsString = '00' then
          begin
            CdsMotivosOcorrencia.Next;
            Continue;
          end;

          if CdsMotivosOcorrencia.Bof then
          begin
            cdsOcorrencias.FieldByName('DETALHES_OCORRENCIA').AsString := '     Detalhes da ocorr�ncia: ' +
                                                                          CdsMotivosOcorrencia.FieldByName('DESCRICAO').AsString;
          end
          else
          begin
            cdsOcorrencias.FieldByName('DETALHES_OCORRENCIA').AsString := cdsOcorrencias.FieldByName('DETALHES_OCORRENCIA').AsString +
                                                                          '                             ' +
                                                                          CdsMotivosOcorrencia.FieldByName('DESCRICAO').AsString;
          end;
          CdsMotivosOcorrencia.Next;
        end;
      end;
    end;

    CdsTitulos.Next;
  end;

end;

function TBoletosACBr.TravarProximoNossoNumero: Boolean;
var
  vBancoBoletos: TArray<RecContasBoletos>;
begin
  Result := True;

  vBancoBoletos := _ContasBoletos.BuscarContasBoletos(Sessao.getConexaoBanco,0,[vContaBoletoID]);

  if ((vBancoBoletos[0].FimNossoNumero - vBancoBoletos[0].ProximoNossoNumero) <= 0) then
  begin
    Avisar('Boleto','A faixa de nosso n�mero informada na conta foi esgotada.' + sLineBreak +
            'N�o foi poss�vel imprimir os boletos.');
    Result := False;
    Exit;
  end;

  vBancoBoletos := _ContasBoletos.BuscarContasBoletos(Sessao.getConexaoBanco,2,[vContaBoletoID]);

  ProximoNossoNumero := vBancoBoletos[0].ProximoNossoNumero.ToString;

  if (Length(ProximoNossoNumero) = 1) and (StrToFloat(ProximoNossoNumero) < 1) then
  begin
    Avisar('Boleto','Houve um problema ao buscar o pr�ximo nosso n�mero. Entre em contato com o suporte.');
    if Sessao.getConexaoBanco.EmTransacao then
      Sessao.getConexaoBanco.FinalizarTransacao;
    Result := False;
    Exit;
  end;
end;

constructor TParametrosImpressao.Create;
begin
  FItensImpressao := TParametroImpressao.Create;
end;

end.
