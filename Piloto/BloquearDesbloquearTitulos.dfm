inherited FormBloquearDesbloquearTitulos: TFormBloquearDesbloquearTitulos
  Caption = 'Bloquear/Desbloquear t'#237'tulos'
  ClientHeight = 165
  ClientWidth = 403
  ExplicitWidth = 409
  ExplicitHeight = 194
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 128
    Width = 403
    ExplicitTop = 128
    ExplicitWidth = 403
  end
  object rgBloquearDesbloquear: TRadioGroupLuka
    Left = 8
    Top = 8
    Width = 193
    Height = 41
    Caption = ' Bloquear? '
    Columns = 2
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 1
    Valores.Strings = (
      'S'
      'N')
  end
  object meMotivoBloqueio: TMemoAltis
    Left = 8
    Top = 52
    Width = 385
    Height = 69
    CharCase = ecUpperCase
    TabOrder = 2
  end
end
