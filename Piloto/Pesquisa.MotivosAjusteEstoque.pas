unit Pesquisa.MotivosAjusteEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _RecordsCadastros,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _MotivosAjusteEstoque, System.StrUtils, System.Math,
  _Biblioteca, _Sessao;

type
  TFormPesquisaMotivosAjusteEstoque = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  protected
    procedure BuscarRegistros; override;
  end;

function Pesquisar(): TObject;

implementation

{$R *.dfm}

const
  coCodigo    = 1;
  coDescricao = 2;
  coAtivo     = 3;

function Pesquisar(): TObject;
var
  obj: TObject;
begin
  obj := _HerancaPesquisas.Pesquisar(TFormPesquisaMotivosAjusteEstoque, _MotivosAjusteEstoque.GetFiltros);
  if obj = nil then
    Result := nil
  else
    Result := RecMotivoAjusteEstoque(obj);
end;

{ TFormPesquisaMotivosAjusteEstoque }

procedure TFormPesquisaMotivosAjusteEstoque.BuscarRegistros;
var
  i: Integer;
  vMotivos: TArray<RecMotivoAjusteEstoque>;
begin
  inherited;

  vMotivos :=
    _MotivosAjusteEstoque.BuscarMotivoAjusteEstoques(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text]
    );

  if vMotivos = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vMotivos);

  for i := Low(vMotivos) to High(vMotivos) do begin
    sgPesquisa.Cells[coSelecionado, i + 1] := 'N�o';
    sgPesquisa.Cells[coCodigo, i + 1]   := IntToStr(vMotivos[i].motivo_ajuste_id);
    sgPesquisa.Cells[coDescricao, i + 1] := vMotivos[i].descricao;
    sgPesquisa.Cells[coAtivo, i + 1]     := SimNao(vMotivos[i].ativo);
  end;

  sgPesquisa.RowCount := IfThen(Length(vMotivos) = 1, 2, High(vMotivos) + 2);
  sgPesquisa.SetFocus;
end;

procedure TFormPesquisaMotivosAjusteEstoque.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = coCodigo then
    vAlinhamento := taRightJustify
  else if ACol = coDescricao then
    vAlinhamento := taLeftJustify
  else
    vAlinhamento := taCenter;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormPesquisaMotivosAjusteEstoque.sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;

  if ARow = 0 then
    Exit;

  if ACol = coAtivo then begin
    AFont.Style := [fsBold];
    AFont.Color := _Biblioteca.AzulVermelho(sgPesquisa.Cells[ACol, ARow]);
  end;
end;

end.
