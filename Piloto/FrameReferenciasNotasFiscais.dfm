inherited FrReferenciasNotasFiscais: TFrReferenciasNotasFiscais
  Width = 414
  Height = 102
  ExplicitWidth = 414
  ExplicitHeight = 102
  inherited sgValores: TGridLuka
    Width = 414
    Height = 85
    OnDrawCell = sgValoresDrawCell
    CorLinhaFoco = 38619
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Tipo refer'#234'ncia'
      'Chave NFe refer'#234'nciada')
    Indicador = True
    ExplicitWidth = 414
    ExplicitHeight = 85
    ColWidths = (
      82
      306)
  end
  inherited StaticTextLuka1: TStaticTextLuka
    Width = 414
    Visible = False
    ExplicitWidth = 414
  end
  inherited pmOpcoes: TPopupMenu
    Left = 360
    Top = 56
  end
end
