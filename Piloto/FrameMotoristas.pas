unit FrameMotoristas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Buttons, _Motoristas,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, PesquisaMotoristas, _Biblioteca, _Sessao,
  Vcl.Menus;

type
  TFrMotoristas = class(TFrameHenrancaPesquisas)
  public
    function getMotorista(pLinha: Integer = -1): RecMotoristas;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function AdicionarPesquisandoTodos: TArray<TObject>; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrMotoristas }

function TFrMotoristas.AdicionarDireto: TObject;
var
  vDados: TArray<RecMotoristas>;
begin
  vDados := _Motoristas.BuscarMotoristas(Sessao.getConexaoBanco, 0, [FChaveDigitada], ckSomenteAtivos.Checked);
  if vDados = nil then
    Result := nil
  else
    Result := vDados[0];
end;

function TFrMotoristas.AdicionarPesquisando: TObject;
begin
  Result := PesquisaMotoristas.Pesquisar(ckSomenteAtivos.Checked);
end;

function TFrMotoristas.AdicionarPesquisandoTodos: TArray<TObject>;
begin
  Result := PesquisaMotoristas.PesquisarVarios(ckSomenteAtivos.Checked);
end;

function TFrMotoristas.getMotorista(pLinha: Integer): RecMotoristas;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecMotoristas(FDados[IIfInt(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrMotoristas.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecMotoristas(FDados[i]).CadastroId = RecMotoristas(pSender).CadastroId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrMotoristas.MontarGrid;
var
  i: Integer;
  vSender: RecMotoristas;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecMotoristas(FDados[i]);
      AAdd([IntToStr(vSender.CadastroId), vSender.Cadastro.nome_fantasia]);
    end;
  end;
end;

end.
