unit OutrasOperacoes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, Vcl.ExtCtrls, EditLuka,
  Vcl.StdCtrls, Vcl.Menus, _Biblioteca, _ComunicacaoTEF, Winapi.ShellAPI, System.StrUtils,
  _GerenciadorCartaoTEF, _RecordsCadastros, _Sessao, Vcl.ComCtrls, _ComunicacaoNFE, _RecordsNFE,
  ImpressaoComprovanteCartao;

type
  TFormOutrasOperacoes = class(TFormHerancaFinalizar)
    pcOperacoes: TPageControl;
    tsGerais: TTabSheet;
    sbConsultarStatusServicoNFE: TSpeedButton;
    procedure sbGerenciadorTEFClick(Sender: TObject);
    procedure sbConsultarStatusServicoNFEClick(Sender: TObject);
  end;

procedure AbrirTela;

implementation

{$R *.dfm}

procedure AbrirTela;
var
  vForm: TFormOutrasOperacoes;
begin
  vForm := TFormOutrasOperacoes.Create(nil);
  vForm.ShowModal;
  vForm.Free;
end;

procedure TFormOutrasOperacoes.sbConsultarStatusServicoNFEClick(Sender: TObject);
var
  vNFe: TComunicacaoNFe;
  vRet: RecRespostaNFE;
begin
  inherited;
  if (Sessao.getParametrosEmpresa.UtilizaNfe = 'N') and (Sessao.getParametrosEmpresa.UtilizaNFCe = 'N') then begin
    _Biblioteca.Exclamar('A empresa n�o est� parametrizada para utilizar NFe!');
    Exit;
  end;

  vNFe := TComunicacaoNFe.Create(Application);
  vRet := vNFe.ConsultarStatusServico;
  if vRet.HouveErro then
    _Biblioteca.Exclamar(vRet.mensagem_erro)
  else begin
    _Biblioteca.Informar(
      'Ambiente NFE: ' + vRet.tipo_ambiente + Chr(13) + Chr(10) +
      LPad('UF: ', 13) +  vRet.uf + Chr(13) + Chr(10)+
      LPad('Status: ', 13) + vRet.motivo + Chr(13) + Chr(10) +
      LPad('Observa��o.: ', 13) + vRet.observacao
    );
  end;
  FreeAndNil(vNFe);
end;

procedure TFormOutrasOperacoes.sbGerenciadorTEFClick(Sender: TObject);
var
  vTef: TTEFDiscado;
  vTexto1Via: TArray<string>;
  vTexto2Via: TArray<string>;
begin
  inherited;
  vTef :=
    TTEFDiscado.Create(
      Self,
      Sessao.getParametrosEstacao.DiretorioArquivosRequisicao,
      Sessao.getParametrosEstacao.DiretorioArquivosResposta,
      '',
      0,
      0,
      '',
      0,
      0,
      '',
      0,
      0,
      '',
      0,
      0,
      '',
      0,
      0,
      '',
      0,
      0,
      '',
      0,
      0,
      '',
      0,
      0,
      '',
      0,
      0,
      '',
      0,
      0
    );

  while not vTef.VerificarTEFAtivo(True) do begin
    _Biblioteca.Informar('O gerenciador padr�o n�o est� ativo. Clique em OK para ativ�-lo.');
    vTef.AtivarTEF;
  end;

  vTef.OperacaoADM;

  if not vTef.Resposta.Aprovado then begin
    _Biblioteca.Exclamar(vTef.Resposta.Mensagem);
    Exit;
  end;

  if (vTef.Resposta.QtdeRegistros29 = 0) and (vTef.Resposta.Mensagem <> '') then begin
    _Biblioteca.Exclamar(vTef.Resposta.Mensagem);
    Exit;
  end;

  vTexto1Via := vTef.Resposta.Comprovante1Via;
  vTexto2Via := vTef.Resposta.Comprovante2Via;

  vTef.EnviarConfirmacao(0);
  if not vTef.Resposta.Aprovado then begin
    _Biblioteca.Exclamar(vTef.Resposta.Mensagem);
    Exit;
  end;

  if vTexto1Via <> nil then
    ImpressaoComprovanteCartao.Imprimir(vTexto1Via, vTexto2Via);

//  if vImpresso then begin
//    vRetorno := _ComunicacaoTEF.EnviarConfirmacao(_ComunicacaoTEF.getId001, _ComunicacaoTEF.getId001, 0, vRetorno.Rede, vRetorno.Nsu, vRetorno.finalizacao);
//    if not vRetorno.Aprovado then
//      _Biblioteca.Exclamar(vRetorno.Mensagem)
//    else
//      _ComunicacaoTEF.ApagarBackupArquivosTEF;
//  end
//  else begin
////    while not _ComunicacaoTEF.TEFAtivo(False) do begin
////      Informar('O gerenciador padr�o n�o est� ativo. Clique em OK para ativ�-lo.');
////      ShellExecute(Application.Handle, 'open', 'C:\tef_dial\tef_dial.exe', '', '', Sw_ShowNormal);
////      Sleep(5000);
////    end;
//
//    vRetorno := _ComunicacaoTEF.GerarArquivoNCN(_ComunicacaoTEF.getId001, _ComunicacaoTEF.getId001, vRetorno.Rede, vRetorno.Nsu, vRetorno.finalizacao, vRetorno.Valor);
//    if vRetorno.Aprovado then begin
//      Informar(
//        'Cancelada a transa��o: ' + Chr(13) + Chr(10) +
//        'Rede: ' + vRetorno.Rede + Chr(13) + Chr(10) +
//        'NSU: ' + vRetorno.Nsu + Chr(13) + Chr(10) +
//        IfThen(vRetorno.Valor > 0, #13 + 'Valor: R$ ' + _Biblioteca.NFormat(vRetorno.Valor))
//      );
//
//      // Limpando a transa��o pendente do TEF
//        _ComunicacaoTEF.ApagarBackupArquivosTEF;
//    end
//    else begin
//      _ComunicacaoTEF.FazerBackupArquivosTEF;
//      _Biblioteca.Exclamar(vRetorno.Mensagem);
//    end;
//  end;
end;

end.
