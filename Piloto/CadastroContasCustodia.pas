unit CadastroContasCustodia;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Sessao, _RecordsEspeciais,
  CheckBoxLuka, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _ContasCustodia, _Biblioteca, PesquisaContasCustodia;

type
  TFormCadastroContasCustodia = class(TFormHerancaCadastroCodigo)
    eNome: TEditLuka;
    lb1: TLabel;
  private
    procedure PreencherRegistro(pDados: RecContasCustodia);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroContasCustodia }

procedure TFormCadastroContasCustodia.BuscarRegistro;
var
  vDados: TArray<RecContasCustodia>;
begin
  vDados := _ContasCustodia.BuscarContasCustodia(Sessao.getConexaoBanco, 0, [eId.AsInt], False);
  if vDados = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    SetarFoco(eID);
    Exit;
  end;

  inherited;
  PreencherRegistro(vDados[0]);
end;

procedure TFormCadastroContasCustodia.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _ContasCustodia.ExcluirContasCustodia(Sessao.getConexaoBanco, eId.AsInt);
  Sessao.AbortarSeHouveErro( vRetBanco );

  inherited;
end;

procedure TFormCadastroContasCustodia.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vRetBanco :=
    _ContasCustodia.AtualizarContasCustodia(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eNome.Text,
      _Biblioteca.ToChar(ckAtivo)
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormCadastroContasCustodia.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    eNome,
    ckAtivo],
    pEditando
  );

  if pEditando then
    SetarFoco(eNome);
end;

procedure TFormCadastroContasCustodia.PesquisarRegistro;
var
  vDado: RecContasCustodia;
begin
  vDado := RecContasCustodia(PesquisaContasCustodia.Pesquisar(False));
  if vDado = nil then
    Exit;

  inherited;
  PreencherRegistro(vDado);
end;

procedure TFormCadastroContasCustodia.PreencherRegistro(pDados: RecContasCustodia);
begin
  eID.AsInt          := pDados.ContaCustodiaId;
  eNome.Text         := pDados.Nome;
  ckAtivo.CheckedStr := pDados.Ativo;

  pDados.Free;
end;

procedure TFormCadastroContasCustodia.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eNome.Trim = '' then begin
    _Biblioteca.Exclamar('O nome da conta cust�dia n�o foi informado corretamente, verifique!');
    SetarFoco(eNome);
    Abort;
  end;
end;

end.
