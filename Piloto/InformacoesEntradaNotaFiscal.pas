unit InformacoesEntradaNotaFiscal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar,
  Vcl.Buttons, _Sessao, _Biblioteca, Vcl.ExtCtrls, StaticTextLuka, Vcl.StdCtrls, EditLuka, Vcl.ComCtrls, _EntradasNotasFiscais, _EntradasNotasFiscaisItens,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameCFOPs, ComboBoxLuka, _EntradasNFItensCompras, RadioGroupLuka, Vcl.Mask, EditLukaData, SpeedButtonLuka,
  Vcl.Grids, GridLuka, _RecordsEstoques, FrameBuscarPedidosCompras, Frame.HerancaInsercaoExclusao, FrameBuscarLotes, Vcl.Menus, InformacoesPrecoLiquidoItemEntrada,
  FrameDadosCobranca, FrameCentroCustos, FramePlanosFinanceiros, _EntradasNotasFiscFinanceiro, _ContasPagar, _RecordsFinanceiros, Informacoes.TituloPagar,
  InformacoesConhecimentoFrete;

type
  TFormInformacoesEntradaNotaFiscal = class(TFormHerancaFinalizar)
    lb1: TLabel;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl27: TLabel;
    eEntradaId: TEditLuka;
    eFornecedor: TEditLuka;
    eNumeroNota: TEditLuka;
    eSerieNota: TEditLuka;
    eModeloNota: TEditLuka;
    eEmpresa: TEditLuka;
    stStatus: TStaticText;
    StaticTextLuka2: TStaticTextLuka;
    pcInfo: TPageControl;
    tsPrincipais: TTabSheet;
    tsProdutos: TTabSheet;
    tsFinanceiro: TTabSheet;
    lb4: TLabel;
    lb5: TLabel;
    lb6: TLabel;
    lb7: TLabel;
    lb8: TLabel;
    lb9: TLabel;
    lb10: TLabel;
    lb11: TLabel;
    lb12: TLabel;
    lb14: TLabel;
    lb15: TLabel;
    lb16: TLabel;
    lb17: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    lblTipoMovimento: TLabel;
    sbInformacoesConhecimento: TSpeedButtonLuka;
    eBaseCalculoICMS: TEditLuka;
    eValorICMS: TEditLuka;
    eBaseCalculoICMSST: TEditLuka;
    eValorICMSST: TEditLuka;
    eValorTotalProdutos: TEditLuka;
    eOutrasDespesas: TEditLuka;
    eValorIPI: TEditLuka;
    eValorFrete: TEditLuka;
    eDataContabil: TEditLukaData;
    eDataEmissaoNota: TEditLukaData;
    eValorDesconto: TEditLuka;
    eValorTotalNota: TEditLuka;
    eChaveAcessoNFe: TMaskEdit;
    eDataPrimeiraParcela: TEditLukaData;
    cbModalidadeFrete: TComboBoxLuka;
    eConhecimentoId: TEditLuka;
    eValorConhecimento: TEditLuka;
    FrCFOPCapa: TFrCFOPs;
    st2: TStaticText;
    stTipoEntrega: TStaticText;
    st3: TStaticText;
    stAtualizarCustoFinal: TStaticText;
    stTipoRateioFrete: TStaticText;
    st6: TStaticText;
    sgItens: TGridLuka;
    st7: TStaticText;
    stQtdeTotal: TStaticText;
    stValorTotal: TStaticText;
    st8: TStaticText;
    st9: TStaticText;
    stPesoTotal: TStaticText;
    FrBuscarLotes: TFrBuscarLotes;
    FrBuscarPedidosCompras: TFrBuscarPedidosCompras;
    sgFinanceiros: TGridLuka;
    pmOpcoes: TPopupMenu;
    miInformaesPrecoFinalLiquido: TMenuItem;
    StaticTextLuka1: TStaticTextLuka;
    FrPlanoFinanceiro: TFrPlanosFinanceiros;
    FrCentroCusto: TFrCentroCustos;
    FrDadosCobranca: TFrDadosCobranca;
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure miInformaesPrecoFinalLiquidoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure tsFinanceiroShow(Sender: TObject);
    procedure sgFinanceirosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgFinanceirosDblClick(Sender: TObject);
    procedure sbInformacoesConhecimentoClick(Sender: TObject);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgItensClick(Sender: TObject);
  private
    FEntradaId: Integer;
  public
    { Public declarations }
  end;

procedure Informar(pEntradaId: Integer);

implementation

{$R *.dfm}

const
  coLegenda                      = 0;
  coProdutoId                    = 1;
  coNomeProduto                  = 2;
  coMarca                        = 3;
  coCFOP                         = 4;
  coQuantidade                   = 5;
  coUnidade                      = 6;
  coQtdeEntradaAltis             = 7;
  coUnidadeEntradaAltis          = 8;
  coValorUnitario                = 9;
  coValorTotal                   = 10;
  coValorDesconto                = 11;
  coValorOutrasDespesas          = 12;
  coValorFrete                   = 13;
  coCST                          = 14;
  coBaseICMSUsuario              = 15;
  coPercentualICMSUsuario        = 16;
  coValorICMSUsuario             = 17;
  coBaseICMSSTUsuario            = 18;
  coPercentualICMSSTUsuario      = 19;
  coValorICMSSTUsuario           = 20;
  coPercentualIPIUsuario         = 21;
  coValorIPIUsuario              = 22;
  coLocalId                      = 23;
  coNomeLocal                    = 24;
  coPrecoFinal                   = 25;
  coPrecoLiquido                 = 26;
  (* Ocultas *)
  coCstPis                              = 27;
  coCstCofins                           = 28;
  coBasePis                             = 29;
  coPercentualPis                       = 30;
  coValorPis                            = 31;
  coBaseCofins                          = 32;
  coPercentualCofins                    = 33;
  coValorCofins                         = 34;
  coNCM                                 = 35;
  coCodigoBarras                        = 36;
  coIndiceReducaoBaseCalculoICMSUsuario = 37;
  coIndiceReducaoBaseCalcICMSSTUsuario  = 38;
  coIvaUsuario                          = 39;
  coPrecoPautaUsuario                   = 40;
  coInformacoesAdicionais               = 41;
  coPeso                                = 42;

  coValorDifalCusto                      = 43;
  coValorICMSCusto                       = 44;
  coValorICMSFreteCusto                  = 45;
  coValorPisCusto                        = 46;
  coValorCofinsCusto                     = 47;
  coValorPisFreteCusto                   = 48;
  coValorCofinsFreteCusto                = 49;
  coValorOutrosCusto                     = 50;
  coDivergenciaPedidoCompra              = 51;

  (* Grid financeiros *)
  cpPagarId        = 0;
  cpTipoCobranca   = 1;
  cpValor          = 2;
  cpStatus         = 3;
  cpDataVencimento = 4;

procedure Informar(pEntradaId: Integer);
var
  i: Integer;
  vForm: TFormInformacoesEntradaNotaFiscal;
  vEntrada: TArray<RecEntradaNotaFiscal>;
  vItens: TArray<RecEntradaNotaFiscalItem>;
  pProdutoId: Integer;
  pQuantidade: Double;
  vTemDivergencia: Boolean;
begin
  if pEntradaId = 0 then
    Exit;

  vEntrada := _EntradasNotasFiscais.BuscarEntradaNotaFiscais( Sessao.getConexaoBanco, 0, [pEntradaId] );
  if vEntrada = nil then begin
    _Biblioteca.Exclamar('Entrada n�o encontrada!');
    Exit;
  end;

  vItens := _EntradasNotasFiscaisItens.BuscarEntradaNotaFiscalItens( Sessao.getConexaoBanco, 0, [pEntradaId] );

  vForm := TFormInformacoesEntradaNotaFiscal.Create(nil);
  vForm.FEntradaId := pEntradaId;

  vForm.FrBuscarPedidosCompras.ItensPedidoCompra := _EntradasNFItensCompras.BuscarEntradasNFItensCompras(Sessao.getConexaoBanco, 0, [pEntradaId]);
  vForm.FrBuscarPedidosCompras.BuscarPedidosCompraCarregados();

  vForm.eEntradaId.AsInt := vEntrada[0].entrada_id;
  vForm.eEmpresa.SetInformacao(vEntrada[0].empresa_id, vEntrada[0].NomeEmpresa);
  vForm.eFornecedor.SetInformacao( vEntrada[0].fornecedor_id, vEntrada[0].nome_fantasia );
  vForm.eNumeroNota.AsInt := vEntrada[0].numero_nota;
  vForm.eModeloNota.Text  := vEntrada[0].modelo_nota;
  vForm.eSerieNota.Text   := vEntrada[0].serie_nota;
  vForm.stStatus.Caption  := vEntrada[0].StatusAnalitico;

  vForm.eChaveAcessoNFe.Text := vEntrada[0].chave_nfe;
  vForm.eDataContabil.AsData := vEntrada[0].DataEntrada;
  vForm.eDataEmissaoNota.AsData := vEntrada[0].data_hora_emissao;
  vForm.eDataPrimeiraParcela.AsData := vEntrada[0].DataPrimeiraParcela;
  vForm.FrCFOPCapa.InserirDadoPorChave(vEntrada[0].CfopId, False);
  vForm.eBaseCalculoICMS.AsDouble    := vEntrada[0].base_calculo_icms;
  vForm.eValorICMS.AsDouble          := vEntrada[0].valor_icms;
  vForm.eBaseCalculoICMSST.AsDouble  := vEntrada[0].valor_icms_st;
  vForm.eValorICMSST.AsDouble        := vEntrada[0].valor_icms_st;
  vForm.eValorTotalProdutos.AsDouble := vEntrada[0].valor_total_produtos;
  vForm.eValorDesconto.AsDouble      := vEntrada[0].valor_desconto;
  vForm.eOutrasDespesas.AsDouble     := vEntrada[0].valor_outras_despesas;
  vForm.eValorIPI.AsDouble           := vEntrada[0].valor_ipi;
  vForm.eValorFrete.AsDouble         := vEntrada[0].valor_frete;
  vForm.eValorTotalNota.AsDouble     := vEntrada[0].valor_total;
  vForm.cbModalidadeFrete.AsInt      := vEntrada[0].ModalidadeFrete;
  vForm.eConhecimentoId.AsInt        := vEntrada[0].ConhecimentoFreteId;
  vForm.eValorConhecimento.AsDouble  := vEntrada[0].ValorConhecimentoFrete;
  vForm.stTipoRateioFrete.Caption    := vEntrada[0].TipoRateioFreteAnalitico;

  vForm.stAtualizarCustoFinal.Caption := _Biblioteca.SimNao( vEntrada[0].atualizar_custo_entrada );

  for i := Low(vItens) to High(vItens) do begin
    vForm.sgItens.Cells[coPeso, i+1]                         := NFormatN(vItens[i].Peso, 3);
    vForm.sgItens.Cells[coProdutoId, i+1]                    := NFormat(vItens[i].produto_id);
    vForm.sgItens.Cells[coNomeProduto, i+1]                  := vItens[i].nome_produto;
    vForm.sgItens.Cells[coMarca, i+1]                        := vItens[i].nome_marca;
    vForm.sgItens.Cells[coCFOP, i+1]                         := vItens[i].CfopId;
    vForm.sgItens.Cells[coQuantidade, i+1]                   := NFormatEstoque(vItens[i].quantidade);
    vForm.sgItens.Cells[coUnidade, i+1]                      := vItens[i].unidade;
    vForm.sgItens.Cells[coQtdeEntradaAltis, i+1]             := NFormatEstoque(vItens[i].QuantidadeEntradaAltis);
    vForm.sgItens.Cells[coUnidadeEntradaAltis, i+1]          := vItens[i].UnidadeCompraId;
    vForm.sgItens.Cells[coValorUnitario, i+1]                := NFormat(vItens[i].preco_unitario);
    vForm.sgItens.Cells[coValorTotal, i+1]                   := NFormat(vItens[i].valor_total);

    vForm.sgItens.Cells[coBaseICMSUsuario, i+1]              := NFormat(vItens[i].base_calculo_icms);
    vForm.sgItens.Cells[coIndiceReducaoBaseCalculoICMSUsuario, i+1] := NFormat(vItens[i].indice_reducao_base_icms, 5);
    vForm.sgItens.Cells[coPercentualICMSUsuario, i+1]        := NFormat(vItens[i].percentual_icms);
    vForm.sgItens.Cells[coValorICMSUsuario, i+1]             := NFormat(vItens[i].valor_icms);

    vForm.sgItens.Cells[coBaseICMSSTUsuario, i+1]            := NFormat(vItens[i].base_calculo_icms_st);
    vForm.sgItens.Cells[coIndiceReducaoBaseCalcICMSSTUsuario, i+1]  := NFormat(vItens[i].indice_reducao_base_icms_st, 5);
    vForm.sgItens.Cells[coPercentualICMSSTUsuario, i+1]      := NFormat(vItens[i].percentual_icms_st);
    vForm.sgItens.Cells[coValorICMSSTUsuario, i+1]           := NFormat(vItens[i].valor_icms_st);

    vForm.sgItens.Cells[coValorIPIUsuario, i+1]              := NFormat(vItens[i].valor_ipi);
    vForm.sgItens.Cells[coPercentualIPIUsuario, i+1]         := NFormat(vItens[i].percentual_ipi);
    vForm.sgItens.Cells[coBasePis, i+1]                      := NFormat(vItens[i].base_calculo_pis);
    vForm.sgItens.Cells[coPercentualPis, i+1]                := NFormat(vItens[i].percentual_pis);
    vForm.sgItens.Cells[coValorPis, i+1]                     := NFormat(vItens[i].valor_pis);
    vForm.sgItens.Cells[coCstPis, i+1]                       := vItens[i].cst_pis;
    vForm.sgItens.Cells[coBaseCofins, i+1]                   := NFormat(vItens[i].base_calculo_cofins);
    vForm.sgItens.Cells[coPercentualCofins, i+1]             := NFormat(vItens[i].percentual_cofins);
    vForm.sgItens.Cells[coValorCofins, i+1]                  := NFormat(vItens[i].valor_cofins);
    vForm.sgItens.Cells[coLocalId, i+1]                      := NFormatN(vItens[i].LocalEntradaId);
    vForm.sgItens.Cells[coNomeLocal, i+1]                    := vItens[i].NomeLocal;
    vForm.sgItens.Cells[coCstCofins, i+1]                    := vItens[i].cst_cofins;
    vForm.sgItens.Cells[coNCM, i+1]                          := vItens[i].codigo_ncm;
    vForm.sgItens.Cells[coCodigoBarras, i+1]                 := vItens[i].codigo_barras;
    vForm.sgItens.Cells[coValorDesconto, i+1]                := NFormat(vItens[i].valor_total_desconto);
    vForm.sgItens.Cells[coValorFrete, i+1]                   := NFormat(vItens[i].ValorFreteCusto);
    vForm.sgItens.Cells[coValorOutrasDespesas, i+1]          := NFormat(vItens[i].valor_total_outras_despesas);
    vForm.sgItens.Cells[coCST, i+1]                          := vItens[i].cst;
    vForm.sgItens.Cells[coIvaUsuario, i+1]                   := NFormatN(vItens[i].iva);
    vForm.sgItens.Cells[coPrecoPautaUsuario, i+1]            := NFormatN(vItens[i].preco_pauta);
    vForm.sgItens.Cells[coInformacoesAdicionais, i+1]        := vItens[i].informacoes_adicionais;

    vForm.sgItens.Cells[coPrecoFinal, i+1]                   := NFormatN(vItens[i].PrecoFinal);
    vForm.sgItens.Cells[coPrecoLiquido, i+1]                 := NFormatN(vItens[i].PrecoLiquido);

    vForm.sgItens.Cells[coValorDifalCusto, i+1]              := NFormatN(vItens[i].ValorDifalCusto);
    vForm.sgItens.Cells[coValorICMSCusto, i+1]               := NFormatN(vItens[i].ValorICMSCusto);
    vForm.sgItens.Cells[coValorICMSFreteCusto, i+1]          := NFormatN(vItens[i].ValorICMSFreteCusto);
    vForm.sgItens.Cells[coValorPisCusto, i+1]                := NFormatN(vItens[i].ValorPisCusto);
    vForm.sgItens.Cells[coValorCofinsCusto, i+1]             := NFormatN(vItens[i].ValorCofinsCusto);
    vForm.sgItens.Cells[coValorPisFreteCusto, i+1]           := NFormatN(vItens[i].ValorPisFreteCusto);
    vForm.sgItens.Cells[coValorCofinsFreteCusto, i+1]        := NFormatN(vItens[i].ValorCofinsFreteCusto);
    vForm.sgItens.Cells[coValorOutrosCusto, i+1]             := NFormatN(vItens[i].ValorOutrosCusto);
  end;
  vForm.sgItens.SetLinhasGridPorTamanhoVetor( Length(vItens) );

  vForm.FrDadosCobranca.Titulos := _EntradasNotasFiscFinanceiro.BuscarEntradaNotaFiscalFinanceiro(Sessao.getConexaoBanco, 0, [pEntradaId]);
  vForm.FrBuscarPedidosCompras.ItensPedidoCompra := _EntradasNFItensCompras.BuscarEntradasNFItensCompras(Sessao.getConexaoBanco, 0, [pEntradaId]);

  for i := vForm.sgItens.FixedRows to vForm.sgItens.RowCount - 1 do begin
    pProdutoId := SFormatInt(vForm.sgItens.Cells[coProdutoId, i]);
    pQuantidade := SFormatDouble(vForm.sgItens.Cells[coQuantidade, i]);

    vTemDivergencia := vForm.FrBuscarPedidosCompras.VerificarDivergenciaAdicionandoNovoPedido(
      pProdutoId,
      pQuantidade
    );

    vForm.sgItens.Cells[coDivergenciaPedidoCompra, i] := IIfStr(vTemDivergencia, 'S', 'N');
  end;

  vForm.sgItens.Repaint;

  vForm.ShowModal;

  vForm.Free;
end;

procedure TFormInformacoesEntradaNotaFiscal.FormShow(Sender: TObject);
begin
  inherited;
  ReadOnlyTodosObjetos(True);
  SetarFoco(eChaveAcessoNFe);
end;

procedure TFormInformacoesEntradaNotaFiscal.miInformaesPrecoFinalLiquidoClick(Sender: TObject);
var
  vQuantidade: Double;
begin
  inherited;

  vQuantidade := SFormatDouble( sgItens.Cells[coQtdeEntradaAltis, sgItens.Row] );

  InformacoesPrecoLiquidoItemEntrada.Informar(
    SFormatDouble( sgItens.Cells[coValorTotal, sgItens.Row] ) / vQuantidade,
    SFormatDouble( sgItens.Cells[coValorOutrasDespesas, sgItens.Row] ) / vQuantidade,
    SFormatDouble( sgItens.Cells[coValorDesconto, sgItens.Row] ) / vQuantidade,
    SFormatDouble( sgItens.Cells[coValorFrete, sgItens.Row] ),
    SFormatDouble( sgItens.Cells[coValorIPIUsuario, sgItens.Row] ) / vQuantidade,
    SFormatDouble( sgItens.Cells[coValorICMSSTUsuario, sgItens.Row] ) / vQuantidade,
    SFormatDouble( sgItens.Cells[coValorOutrosCusto, sgItens.Row] ),
    SFormatDouble( sgItens.Cells[coValorDifalCusto, sgItens.Row] ),
    SFormatDouble( sgItens.Cells[coPrecoFinal, sgItens.Row] ),
    SFormatDouble( sgItens.Cells[coValorICMSCusto, sgItens.Row] ),
    SFormatDouble( sgItens.Cells[coValorICMSFreteCusto, sgItens.Row] ),
    SFormatDouble( sgItens.Cells[coValorPisCusto, sgItens.Row] ),
    SFormatDouble( sgItens.Cells[coValorCofinsCusto, sgItens.Row] ),
    SFormatDouble( sgItens.Cells[coPrecoLiquido, sgItens.Row] )
  );
end;

procedure TFormInformacoesEntradaNotaFiscal.sbInformacoesConhecimentoClick(Sender: TObject);
begin
  inherited;
  InformacoesConhecimentoFrete.Informar(eConhecimentoId.AsInt);
end;

procedure TFormInformacoesEntradaNotaFiscal.sgFinanceirosDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.TituloPagar.Informar( SFormatInt(sgFinanceiros.Cells[cpPagarId, sgFinanceiros.Row]) );
end;

procedure TFormInformacoesEntradaNotaFiscal.sgFinanceirosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[cpPagarId, cpValor] then
    vAlinhamento := taRightJustify
  else if ACol = cpStatus then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgFinanceiros.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesEntradaNotaFiscal.sgItensClick(Sender: TObject);
begin
  inherited;
  FrBuscarPedidosCompras.ProdutoId := SFormatInt( sgItens.Cells[coProdutoId, sgItens.Row] );
  FrBuscarPedidosCompras.ItemId    := sgItens.Row;
  FrBuscarPedidosCompras.QuantidadeBuscar := SFormatCurr( sgItens.Cells[coQuantidade, sgItens.Row] );
end;

procedure TFormInformacoesEntradaNotaFiscal.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if
    ACol in[
      coProdutoId,
      coQuantidade,
      coValorUnitario,
      coValorTotal,
      coValorDesconto,
      coValorOutrasDespesas,
      coBaseICMSUsuario,
      coPercentualICMSUsuario,
      coValorICMSUsuario,
      coBaseICMSSTUsuario,
      coPercentualICMSSTUsuario,
      coValorICMSSTUsuario,
      coValorIPIUsuario,
      coPercentualIPIUsuario,
      coQtdeEntradaAltis,
      coLocalId,
      coPrecoFinal,
      coPrecoLiquido]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesEntradaNotaFiscal.sgItensGetCellColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgItens.Cells[coDivergenciaPedidoCompra, ARow] = 'S' then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicaoPadrao;
    AFont.Color  := _Biblioteca.coCorFonteEdicao5;
  end
  else begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicaoPadrao;
    AFont.Color  := _Biblioteca.coCorFonteEdicaoPadrao;
  end;
end;

procedure TFormInformacoesEntradaNotaFiscal.tsFinanceiroShow(Sender: TObject);
var
  i: Integer;
  vFinanceiros: TArray<RecContaPagar>;
begin
  inherited;
  sgFinanceiros.ClearGrid();

  vFinanceiros := _ContasPagar.BuscarContasPagar(Sessao.getConexaoBanco, 4, [FEntradaId]);
  for i := Low(vFinanceiros) to High(vFinanceiros) do begin
    sgFinanceiros.Cells[cpPagarId, i + 1]        := NFormat(vFinanceiros[i].PagarId);
    sgFinanceiros.Cells[cpTipoCobranca, i + 1]   := NFormat(vFinanceiros[i].cobranca_id) + ' - ' + vFinanceiros[i].nome_tipo_cobranca;
    sgFinanceiros.Cells[cpValor, i + 1]          := NFormat(vFinanceiros[i].ValorDocumento);
    sgFinanceiros.Cells[cpStatus, i + 1]         := vFinanceiros[i].StatusAnalitico;
    sgFinanceiros.Cells[cpDataVencimento, i + 1] := DFormat(vFinanceiros[i].data_vencimento);
  end;
  sgFinanceiros.SetLinhasGridPorTamanhoVetor( Length(vFinanceiros) );
end;

end.
