unit FrameVeiculos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Buttons, _Sessao, _Biblioteca,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Veiculos, PesquisaVeiculos,
  Vcl.Menus;

type
  TFrVeiculos = class(TFrameHenrancaPesquisas)
  public
    function getDados(pLinha: Integer = -1): RecVeiculos;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function AdicionarPesquisandoTodos: TArray<TObject>; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrVeiculos }

function TFrVeiculos.AdicionarDireto: TObject;
var
  vDados: TArray<RecVeiculos>;
begin
  vDados := _Veiculos.BuscarVeiculos(Sessao.getConexaoBanco, 0, [FChaveDigitada], ckSomenteAtivos.Checked);
  if vDados = nil then
    Result := nil
  else
    Result := vDados[0];
end;

function TFrVeiculos.AdicionarPesquisando: TObject;
begin
  Result := PesquisaVeiculos.Pesquisar(ckSomenteAtivos.Checked);
end;

function TFrVeiculos.AdicionarPesquisandoTodos: TArray<TObject>;
begin

end;

function TFrVeiculos.getDados(pLinha: Integer): RecVeiculos;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecVeiculos(FDados[IIfInt(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrVeiculos.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecVeiculos(FDados[i]).VeiculoId = RecVeiculos(pSender).VeiculoId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrVeiculos.MontarGrid;
var
  i: Integer;
  vSender: RecVeiculos;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecVeiculos(FDados[i]);
      AAdd([IntToStr(vSender.VeiculoId), vSender.Nome]);
    end;
  end;
end;

end.
