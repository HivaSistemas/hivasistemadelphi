unit PesquisaAcumulados;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPrincipal, Vcl.Grids, GridLuka, _Sessao, _Biblioteca,
  FrameEmpresas, FrameVendedores, FrameDataInicialFinal, _FrameHerancaPrincipal, _Acumulados,
  _FrameHenrancaPesquisas, FrameClientes;

type
  TFormPesquisaAcumulados = class(TFormHerancaPrincipal)
    FrCliente: TFrClientes;
    FrDataFechamento: TFrDataInicialFinal;
    FrEmpresa: TFrEmpresas;
    sgAcumulados: TGridLuka;
    FrDataRecebimento: TFrDataInicialFinal;
    procedure FrDataRecebimentoeDataFinalKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgAcumuladosDblClick(Sender: TObject);
    procedure sgAcumuladosEnter(Sender: TObject);
    procedure sgAcumuladosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgAcumuladosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    FAcumulados: TArray<RecAcumulados>;

    procedure BuscarRegistros;
  end;

function Pesquisar: TRetornoTelaFinalizar<RecAcumulados>;

implementation

{$R *.dfm}

{ TFormPesquisaAcumulados }

const
  coAcumuladoId   = 0;
  coDataHoraReceb = 1;
  coCliente       = 2;
  coValor         = 3;
  coEmpresa       = 4;

function Pesquisar: TRetornoTelaFinalizar<RecAcumulados>;
var
  vForm: TFormPesquisaAcumulados;
begin
  vForm := TFormPesquisaAcumulados.Create(nil);

  if Result.Ok(vForm.ShowModal) then
    Result.Dados := vForm.FAcumulados[vForm.sgAcumulados.Row -1];

  vForm.Free;
end;

procedure TFormPesquisaAcumulados.BuscarRegistros;
var
  i: Integer;
  vComando: string;
begin
  FAcumulados := nil;
  sgAcumulados.ClearGrid;

  vComando := 'where ACU.STATUS in(''RE'') ';

  if not FrEmpresa.EstaVazio then
    vComando := vComando + ' and ' + FrEmpresa.getSqlFiltros('ACU.EMPRESA_ID');

  if not FrCliente.EstaVazio then
    vComando := vComando + ' and ' + FrCliente.getSqlFiltros('ACU.CLIENTE_ID');

  if not FrDataFechamento.NenhumaDataValida then
    vComando := vComando + ' and ' + FrDataFechamento.getSqlFiltros('trunc(ACU.DATA_HORA_FECHAMENTO)');

  if not FrDataRecebimento.NenhumaDataValida then
    vComando := vComando + ' and ' + FrDataRecebimento.getSqlFiltros('trunc(ACU.DATA_HORA_RECEBIMENTO)');

  vComando := vComando + ' order by ACU.ACUMULADO_ID';

  FAcumulados := _Acumulados.BuscarAcumuladosComando(Sessao.getConexaoBanco, vComando);
  if FAcumulados = nil then begin
    NenhumRegistro;
    SetarFoco(FrCliente);
    Exit;
  end;

  for i := Low(FAcumulados) to High(FAcumulados) do begin
    sgAcumulados.Cells[coAcumuladoId, i + 1]   := NFormat(FAcumulados[i].AcumuladoId);
    sgAcumulados.Cells[coDataHoraReceb, i + 1] := DHFormat(FAcumulados[i].DataHoraRecebimento);
    sgAcumulados.Cells[coCliente, i + 1]       := getInformacao(FAcumulados[i].ClienteId, FAcumulados[i].NomeCliente);
    sgAcumulados.Cells[coValor, i + 1]         := NFormat(FAcumulados[i].ValorTotal);
    sgAcumulados.Cells[coEmpresa, i + 1]       := getInformacao(FAcumulados[i].EmpresaId, FAcumulados[i].NomeEmpresa);
  end;
  sgAcumulados.SetLinhasGridPorTamanhoVetor( Length(FAcumulados) );

  SetarFoco(sgAcumulados);
end;

procedure TFormPesquisaAcumulados.FrDataRecebimentoeDataFinalKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    BuscarRegistros;
end;

procedure TFormPesquisaAcumulados.sgAcumuladosDblClick(Sender: TObject);
begin
  inherited;
  if sgAcumulados.Cells[coAcumuladoId, 1] = '' then
    Exit;

  ModalResult := mrOk;
end;

procedure TFormPesquisaAcumulados.sgAcumuladosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coAcumuladoId, coValor] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgAcumulados.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);  inherited;
end;

procedure TFormPesquisaAcumulados.sgAcumuladosEnter(Sender: TObject);
begin
  inherited;
  BuscarRegistros;
end;

procedure TFormPesquisaAcumulados.sgAcumuladosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    sgAcumuladosDblClick(Sender);
end;

end.
