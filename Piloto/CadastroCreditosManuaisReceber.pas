unit CadastroCreditosManuaisReceber;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CadastroCreditosManuais, _Sessao, _RecordsEspeciais,
  Frame.Cadastros, Vcl.StdCtrls, MemoAltis, Vcl.Mask, EditLukaData, _Biblioteca, _ContasPagarBaixas,
  FramePagamentoFinanceiro, FrameCentroCustos, FramePlanosFinanceiros, _ContasReceber, _TurnosCaixas,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameEmpresas, EditLuka, Vcl.Buttons, Vcl.ExtCtrls,
  CheckBoxLuka;

type
  TFormCadastroCreditoManualReceber = class(TFormCadastroCreditosManuais)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure GravarRegistro(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroCreditosManuais1 }

procedure TFormCadastroCreditoManualReceber.FormCreate(Sender: TObject);
begin
  inherited;
  FrPagamentoFinanceiro.Natureza := 'P';
end;

procedure TFormCadastroCreditoManualReceber.GravarRegistro(Sender: TObject);
var
  vTurnoId: Integer;
  vDataAtual: TDateTime;
  vBaixaPagarId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;

  vBaixaPagarId := 0;

  vTurnoId := _TurnosCaixas.getTurnoId( Sessao.getConexaoBanco, Sessao.getUsuarioLogado.funcionario_id );
  if (vTurnoId = 0) and (FrPagamentoFinanceiro.Dinheiro = nil) and (FrPagamentoFinanceiro.eValorDinheiro.AsCurr > 0) then begin
    _Biblioteca.Exclamar('N�o foi encontrada a defini��o das contas para o pagamento em dinheiro!');
    SetarFoco(FrPagamentoFinanceiro.eValorDinheiro);
    Abort;
  end;

  vDataAtual := Sessao.getData;

  Sessao.getConexaoBanco.IniciarTransacao;

  // Gerando a baixa
  // Se for importa��o( Virada de sistema ) n�o gera o financeiro
  if not ckImportacao.Checked then begin
    vRetBanco :=
      _ContasPagarBaixas.AtualizarContasPagarBaixas(
        Sessao.getConexaoBanco,
        0,
        FrEmpresa.getEmpresa().EmpresaId,
        FrCadastro.getCadastro().cadastro_id,
        IIfInt((vTurnoId > 0) and (FrPagamentoFinanceiro.Dinheiro = nil), vTurnoId),
        'PG',
        FrPagamentoFinanceiro.ValorDinheiro,
        FrPagamentoFinanceiro.ValorCheque,
        0,
        FrPagamentoFinanceiro.ValorCobranca,
        FrPagamentoFinanceiro.ValorCredito,
        eValorCreditoGerar.AsDouble,
        0,
        0,
        Sessao.getData,
        meObservacoes.Lines.Text,
        'CRE',
        0,
        FrPagamentoFinanceiro.Dinheiro,
        nil,
        FrPagamentoFinanceiro.Cobrancas,
        FrPagamentoFinanceiro.Cheques,
        nil,
        True
      );

    if vRetBanco.TeveErro then begin
      Sessao.getConexaoBanco.VoltarTransacao;
      _Biblioteca.Exclamar(vRetBanco.MensagemErro);
      Abort;
    end;

    vBaixaPagarId := vRetBanco.AsInt;
  end;

  // Lan�ando o cr�dito no contas a receber - D�bito
  vRetBanco :=
    _ContasReceber.AtualizarContaReceber(
      Sessao.getConexaoBanco,
      0,
      FrEmpresa.getEmpresa().EmpresaId,
      IIfInt(ckImportacao.Checked, Sessao.getParametros.TipoCobrancaGerCredImpId, Sessao.getParametros.TipoCobrancaGeracaoCredId),
      FrCadastro.getCadastro().cadastro_id,
      1,
      1,
      '9999',
      FrPlanoFinanceiro.getDados.PlanoFinanceiroId,
      '',
      '',
      0,
      eValorCreditoGerar.AsDouble,
      '',
      'CRE' + NFormat(FrCadastro.getCadastro().cadastro_id) + DFormat(eDataVencimento.AsData),
      'A',
      eDataVencimento.AsData,
      eDataVencimento.AsData,
      Sessao.getData,
      '',
      '',
      '',
      '',
      '',
      '',
      meObservacoes.Lines.Text,
      1,
      IIfStr(ckImportacao.Checked, 'MAN', 'BAP'),
      vBaixaPagarId,
      0,
      vDataAtual,
      0,
      True
    );

  if vRetBanco.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  Sessao.getConexaoBanco.FinalizarTransacao;

  if vBaixaPagarId > 0 then
    _Biblioteca.Informar('Nova baixa a pagar inserida com sucesso. C�digo: ' + NFormat(vBaixaPagarId))
  else
    _Biblioteca.Informar('Novo cr�dito inserido com sucesso.');
end;

procedure TFormCadastroCreditoManualReceber.VerificarRegistro(Sender: TObject);
begin
  if (ckImportacao.Checked) and (not _Sessao.Sessao.AutorizadoRotina('cadastrar_credito_importacao_receber')) then begin
    _Biblioteca.Exclamar('Voc� possui autoriza��o para lan�ar um cr�dito de importa��o!');
    SetarFoco(ckImportacao);
    Abort;
  end;

  if Sessao.getParametros.TipoCobrancaGeracaoCredId = 0 then begin
    _Biblioteca.Exclamar('O tipo de cobran�a para gera��o de cr�dito n�o foi parametrizado, fa�a a parametriza��o nos "Par�metros"!');
    SetarFoco(FrEmpresa);
    Abort;
  end;

  if Sessao.getParametros.TipoCobrancaGerCredImpId = 0 then begin
    _Biblioteca.Exclamar('O tipo de cobran�a para gera��o de cr�dito de importa��o n�o foi parametrizado, fa�a a parametriza��o nos "Par�metros"!');
    SetarFoco(FrEmpresa);
    Abort;
  end;

  inherited;
end;

end.
