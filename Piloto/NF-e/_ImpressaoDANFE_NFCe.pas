unit _ImpressaoDANFE_NFCe;

interface

uses
  System.SysUtils, System.StrUtils, System.Classes, _Biblioteca, System.DateUtils, _RecordsNotasFiscais, _NotasFiscais, _Sessao,
  _ImpressoraNaoFiscal, _NotasFiscaisItens, _RecordsOrcamentosVendas, _Orcamentos, _BibliotecaNFE;

function Imprimir(pNotaFiscalId: Integer): Boolean;

implementation

function Imprimir(pNotaFiscalId: Integer): Boolean;
var
  i: Integer;
  vImpressora: TImpressoraNaoFiscal;
  vDadosNota: TArray<RecNotaFiscal>;
  vItensNota: TArray<RecImpressaoDANFENFCe>;
  vDadosOrcamento: TArray<RecOrcamentos>;
  vXML: string;

  vConfigImpressora: RecImpressora;
begin
  Result := False;

  vDadosNota := _NotasFiscais.BuscarNotasFiscais(Sessao.getConexaoBanco, 0, [pNotaFiscalId]);
  if vDadosNota = nil then begin
    _Biblioteca.Exclamar('NFC-e n�o encontrado para impress�o!');
    Exit;
  end;

  vItensNota := _NotasFiscaisItens.BuscarProdutosImpressaoDANFENFCe(Sessao.getConexaoBanco, pNotaFiscalId);
  if vItensNota = nil then begin
    _Biblioteca.Exclamar('Itens da NFC-e n�o encontrado para impress�o!');
    Exit;
  end;

  if vDadosNota[0].status <> 'E' then begin
    _Biblioteca.Exclamar('A NFe deve estar emitida para a impress�o do DANFE!');
    Exit;
  end;

  vConfigImpressora := Sessao.getImpressora(toNFCe);
  if vConfigImpressora.CaminhoImpressora = '' then begin
    _Biblioteca.Exclamar('Impressora de NFCe n�o configurada!');
    Exit;
  end;

  if vConfigImpressora.TipoImpressora = 'G' then
    Exit;

  vDadosOrcamento := _Orcamentos.BuscarOrcamentos(Sessao.getConexaoBanco, 0, [vDadosNota[0].orcamento_id]);
  vXML := _NotasFiscais.BuscarXMLNFe(Sessao.getConexaoBanco, pNotaFiscalId);

  vImpressora := TImpressoraNaoFiscal.Create(Sessao.getParametrosEmpresa.ModeloImpressora, vConfigImpressora.caminhoImpressora);
  try
    vImpressora.SaltarLinha;

    vImpressora.Imprimir( _Biblioteca.CPad(Sessao.getEmpresaLogada.RazaoSocial, 48) );
    vImpressora.Imprimir( _Biblioteca.CPad('CNPJ-' + Sessao.getEmpresaLogada.Cnpj + ' ' + _Biblioteca.DFormat(vDadosNota[0].data_hora_emissao), 48) );
    vImpressora.Imprimir( _Biblioteca.CPad(Sessao.getEmpresaLogada.Logradouro + ' ' + Sessao.getEmpresaLogada.Complemento, 48) );
    vImpressora.Imprimir( _Biblioteca.CPad(Sessao.getEmpresaLogada.NomeBairro + '-' + Sessao.getEmpresaLogada.EstadoId, 48) );
    vImpressora.Imprimir( _Biblioteca.CPad('DANFE NFC-e Documento Auxiliar', 48) );
    vImpressora.Imprimir( _Biblioteca.CPad('da Nota Fiscal de Consumidor Eletronica', 48) );

    vImpressora.Imprimir('ITEM ' + LPad('CODIGO', 9) + ' ' + RPad('DESCRICAO', 33));
    vImpressora.Imprimir(LPad('QTD', 13) + ' ' + RPad('UND', 3) + ' ' + LPad('X VL ITEM (R$)', 14) + LPad('TOT ITEM(R$)', 16));

    for i := Low(vItensNota) to High(vItensNota) do begin
      vImpressora.Imprimir( LPad(NFormat(vItensNota[i].itemId), 4, '0') + ' ' + LPad(NFormat(vItensNota[i].produtoId), 9) + ' ' + RPad(vItensNota[i].nome, 33));
      vImpressora.Imprimir(
        LPad(NFormat(vItensNota[i].quantidade, 3), 13) + ' ' + RPad(vItensNota[i].unidade, 3) + ' ' +
        LPad(NFormat(vItensNota[i].precoUnitario), 14) + LPad(NFormat(vItensNota[i].precoUnitario * vItensNota[i].quantidade), 16)
      );
    end;

    vImpressora.Imprimir( _Biblioteca.RPad('QUANTIDADE TOTAL DE ITENS', 43) + ' ' + LPad(NFormat(Length(vItensNota)), 4) );
    vImpressora.Imprimir( _Biblioteca.RPad('VALOR TOTAL R$', 30) + ' ' + LPad(NFormat(vDadosNota[0].valor_total), 17) );
    vImpressora.Imprimir( _Biblioteca.RPad('FORMA DE PAGAMENTO', 30) + LPad('VALOR PAGO', 18) );

    if vDadosOrcamento[0].valor_dinheiro > 0 then
      vImpressora.Imprimir( _Biblioteca.RPad('Dinheiro', 30) + ' ' + LPad(NFormat(vDadosOrcamento[0].valor_dinheiro), 17) );

    if vDadosOrcamento[0].valor_cheque > 0 then
      vImpressora.Imprimir( _Biblioteca.RPad('Cheque', 30) + ' ' + LPad(NFormat(vDadosOrcamento[0].valor_cheque), 17) );

    if vDadosOrcamento[0].ValorCartaoDebito > 0 then
      vImpressora.Imprimir( _Biblioteca.RPad('Cartao', 30) + ' ' + LPad(NFormat(vDadosOrcamento[0].ValorCartaoDebito), 17) );

    if vDadosOrcamento[0].valor_cobranca > 0 then
      vImpressora.Imprimir( _Biblioteca.RPad('Cobran�a', 30) + ' ' + LPad(NFormat(vDadosOrcamento[0].valor_cobranca), 17) );

    if vDadosOrcamento[0].valor_credito > 0 then
      vImpressora.Imprimir( _Biblioteca.RPad('Credito', 30) + ' ' + LPad(NFormat(vDadosOrcamento[0].valor_credito), 17) );

    if vDadosOrcamento[0].valorTroco > 0 then
      vImpressora.Imprimir( _Biblioteca.RPad('Troco', 30) + ' ' + LPad(NFormat(vDadosOrcamento[0].valorTroco), 17) );

    vImpressora.Imprimir( _Biblioteca.CPad('Consulte pela Chave de aceso em', 48) );
    vImpressora.Imprimir(
      _BibliotecaNFE.GetURLQRCode(vDadosNota[0].estado_id_emitente, IIf(Sessao.getParametrosEmpresa.tipoAmbienteNFCe = 'P', tpProducao, tpHomologacao))
    );
    vImpressora.Imprimir( CPad(RetornaNumeros(vDadosNota[0].chave_nfe), 48) );

    if vDadosNota[0].cpf_cnpj_destinatario <> '' then begin
      vImpressora.Imprimir( CPad(vDadosNota[0].nome_fantasia_destinatario, 48) );
      vImpressora.Imprimir( CPad(vDadosNota[0].cpf_cnpj_destinatario, 48) );
    end;

    vImpressora.Imprimir(
      'Numero ' + LPad(NFormat(vDadosNota[0].numero_nota), 6, '0') + ' Serie' +
      LPad(vDadosNota[0].SerieNota, 2) + ' Emissao ' + DFormat(vDadosNota[0].data_hora_emissao)
    );

    vImpressora.SaltarLinha;

    vImpressora.Imprimir( CPad('Protocolo de autorizacao: ' + vDadosNota[0].protocolo_nfe, 48) );
    vImpressora.Imprimir( CPad('Data de autorizacao: ' + DFormat(vDadosNota[0].data_hora_emissao), 48) );

    vImpressora.SaltarLinha;

    vXML := Copy(vXML, Pos('<![CDATA[', vXML) + 9, Pos(']]>', vXML) - Pos('<![CDATA[', vXML) - 9);
    vImpressora.ImprimirQrCode( vXML );

    vImpressora.SaltarLinha;

    vImpressora.Imprimir( vDadosNota[0].informacoes_complementares );

    vImpressora.SaltarLinha;
    vImpressora.SaltarLinha;
    vImpressora.SaltarLinha;

    vImpressora.CortarPapel;
    Result := True;
  except
    on e: Exception do
      Exclamar('Erro ao imprimir o DANFE da NFCe, caso necess�rio reemita na tela "Consulta de notas fiscais"! ' + e.Message);
  end;
  vImpressora.Free;
end;

end.
