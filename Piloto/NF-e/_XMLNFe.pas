unit _XMLNFe;

interface

uses
  SysUtils, Classes, StrUtils, Variants, _BibliotecaNFE, _Biblioteca;

type
  TICMSItens = record
    cst: string;
    baseCalculoICMS: Double;
    valorICMS: Double;
  end;

  TDadosCartoes = record
    TipoIntegracao: string;
    CnpjAdminsitradora: string;
    Bandeira: string;
    CodigoAutorizacao: string;
    ValorCartao: string;
  end;

  {$M+}
  TXMLNFe = class
  private
    FICMS: TICMSItens;

    function getIcmsItens: string;
  public
    function getXMLNFe: string;
    function getTipoPagamento(pCodigo: string; pValor: string): string;
    function getTipoPagamentoCartao(
      pCodigo: string;
      pValor: string;
      pDadosCartoes: TArray<TDadosCartoes>
    ): string;
  published
    property ICMS: TICMSItens read FICMS write FICMS;
  end;

implementation

{ TXMLNFe }

function TXMLNFe.getIcmsItens: string;
begin



end;

function TXMLNFe.getTipoPagamento(pCodigo, pValor: string): string;
var
  descricaoOutros: string;
begin
  Result := '';
  if (pValor = '0.00') and (pCodigo <> '90') then
    Exit;

  descricaoOutros := '';
  if pCodigo = '99' then
    descricaoOutros := '<xPag>Crediario proprio</xPag>';

  Result :=
    '<detPag>' +
      '<tPag>' + pCodigo + '</tPag>' +
      descricaoOutros +
      '<vPag>' + pValor + '</vPag>' +
    '</detPag>';
end;

function TXMLNFe.getTipoPagamentoCartao(
  pCodigo: string;
  pValor: string;
  pDadosCartoes: TArray<TDadosCartoes>
): string;
var
  i: Integer;
begin
  Result := '';
  if pValor = '0.00' then
    Exit;

  if (pDadosCartoes = nil) and Em(pCodigo, ['03', '04']) then begin
    Result := getTipoPagamento('99', pValor);
    Exit;
  end;

  for i := Low(pDadosCartoes) to High(pDadosCartoes) do begin
    Result := Result +
      '<detPag>' +
      '<tPag>' + pCodigo + '</tPag>' +
      '<vPag>' + pDadosCartoes[i].ValorCartao + '</vPag>' +
        '<card>' +
          '<tpIntegra>' + pDadosCartoes[i].TipoIntegracao + '</tpIntegra>' +
          '<CNPJ>' + pDadosCartoes[i].CnpjAdminsitradora + '</CNPJ>' +
          '<tBand>99</tBand>' +
          '<cAut>' + pDadosCartoes[i].CodigoAutorizacao + '</cAut>' +
        '</card>' +
      '</detPag>';
  end;
end;

function TXMLNFe.getXMLNFe: string;
begin
  Result := getIcmsItens;
end;

end.
