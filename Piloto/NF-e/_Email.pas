unit _Email;

interface

uses
  Forms, SysUtils, IdSMTP, IdSSLOpenSSL, IdMessage, IdText, IdAttachmentFile,
  IdExplicitTLSClientServerBase, _Biblioteca, _Sessao, System.Classes;


procedure EnviarEmail(pDestinatario, pAssunto, pCorpoEmail: String; pListaCaminhoAnexo: TStringList);

implementation

procedure EnviarEmail(pDestinatario, pAssunto, pCorpoEmail: String; pListaCaminhoAnexo: TStringList);
var
  IdSSLIOHandlerSocket: TIdSSLIOHandlerSocketOpenSSL;
  IdSMTP: TIdSMTP;
  IdMessage: TIdMessage;
  IdText: TIdText;
  bolReqAut : Boolean;
  bolAut : Boolean;
  i: Integer;
begin
  // instancia��o dos objetos
  IdSSLIOHandlerSocket := TIdSSLIOHandlerSocketOpenSSL.Create();
  IdSMTP := TIdSMTP.Create(Application);
  IdMessage := TIdMessage.Create(Application);
  if Sessao.getParametros.SmtpUsuario.IsEmpty then
  begin
    Exclamar('Endere�o de e-mail de sua empresa n�o informado!');
    exit;
  end;

  if pDestinatario.IsEmpty then
  begin
//    Exclamar('Destinat�rio Inv�lido!');
    exit;
  end;

  if pAssunto.IsEmpty then
    pAssunto := 'Envio de NFe';

  if pCorpoEmail.IsEmpty then
    pCorpoEmail := 'Envio de NFe';

  bolReqAut := Sessao.getParametros.SmtpReqAutenticacao = 'S';
  bolAut := Sessao.getParametros.SmtpAutenticacao = 'SSL/TLS';

  IdSMTP.AuthType := satDefault;
  IdSMTP.Port := Sessao.getParametros.SmtpPorta;
  IdSMTP.Host := Sessao.getParametros.SmtpServidor;
  IdSMTP.Password := Sessao.getParametros.SmtpSenha;

  try
    if bolReqAut then
    begin
      IdSMTP.Username := Sessao.getParametros.SmtpUsuario;
      if bolAut then
      begin
        // Configura��o do protocolo SSL (TIdSSLIOHandlerSocketOpenSSL)
        IdSSLIOHandlerSocket.SSLOptions.Method := sslvSSLv23;
        IdSSLIOHandlerSocket.SSLOptions.Mode := sslmClient;
        // Configura��o do servidor SMTP (TIdSMTP)
        IdSMTP.IOHandler := IdSSLIOHandlerSocket;
        if (IdSMTP.Port = 465) then
          IdSMTP.UseTLS := utUseImplicitTLS
        else
          IdSMTP.UseTLS := utUseExplicitTLS;
      end;
    end else
      IdSMTP.Username := copy(Sessao.getParametros.SmtpUsuario,1,Pos('@',Sessao.getParametros.SmtpUsuario)-1);



    // Configura��o da mensagem (TIdMessage)
    IdMessage.From.Address := Sessao.getParametros.SmtpUsuario;
    IdMessage.From.Name := Sessao.getEmpresaLogada.RazaoSocial;

    IdMessage.Recipients.Add.Text := pDestinatario;
    IdMessage.Recipients.EMailAddresses := pDestinatario;
    IdMessage.Subject := pAssunto;
    IdMessage.Encoding := meMIME;

    // Configura��o do corpo do email (TIdText)
    IdText := TIdText.Create(IdMessage.MessageParts);
    IdText.Body.Add(pCorpoEmail);
    IdText.ContentType := 'text/html';//''text/plain; charset=iso-8859-1';

    // Opcional - Anexo da mensagem (TIdAttachmentFile)
    try
      for i := 0 to pListaCaminhoAnexo.Count-1 do
      begin
        if not FileExists(pListaCaminhoAnexo.Strings[i]) then
          Continue;
        TIdAttachmentFile.Create(IdMessage.MessageParts, pListaCaminhoAnexo.Strings[i]);
      end;
    except on E: Exception do
      raise Exception.Create('Falha ao anexar, Erro: '+e.Message);
    end;

    // Conex�o e autentica��o
    try
      IdSMTP.Connect;
      if bolAut then
        IdSMTP.Authenticate;
    except
      on E:Exception do
      begin
        Exclamar('Erro na conex�o ou autentica��o: ');
        Exit;
      end;
    end;

    // Envio da mensagem
    try
      IdSMTP.Send(IdMessage);
      Informar('E-mail enviado com sucesso!');
    except
      On E:Exception do
      begin
        Exclamar('Erro ao enviar o e-mail');
      end;
    end;
  finally
    // desconecta do servidor
    IdSMTP.Disconnect;
    // libera��o da DLL
    UnLoadOpenSSLLibrary;
    // libera��o dos objetos da mem�ria
    FreeAndNil(IdMessage);
    FreeAndNil(IdSSLIOHandlerSocket);
    FreeAndNil(IdSMTP);
  end;
end;

end.
