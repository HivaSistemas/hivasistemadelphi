unit _ImpressaoDANFE;

interface

uses
  _RecordsEspeciais, _Biblioteca, DanfePaisagem, _Sessao, _RecordsNotasFiscais, _NotasFiscais, _NotasFiscaisItens,
  SysUtils, System.Classes;

function Imprimir(
  const pNotaFiscalId: Integer;
  pCaminhoPDF: string = ''
): Boolean;

implementation

uses Impressao.Danfe, _Email;

function Imprimir(
  const pNotaFiscalId: Integer;
  pCaminhoPDF: string = ''
): Boolean;
var
  vDadosNota: TArray<RecNotaFiscal>;
  vDadosNotaItens: TArray<RecNotaFiscalItem>;
  vXmls: TArray<RecXMLs>;
  vRetorno: RecRetornoBD;
  vListaAnexo: TStringList;
  vCaminhoXml: string;
begin
  Result := False;
  vListaAnexo := TStringList.Create;
  vDadosNota := _NotasFiscais.BuscarNotasFiscais(Sessao.getConexaoBanco, 0, [pNotaFiscalId]);
  if vDadosNota = nil then
    Exit;


  // Se for para gerar PDF mas a nota n�o estiver emitida saindo fora
  // Se n�o for uma NFe saindo fora tamb�m
  //if (pCaminhoPDF <> '') and ((vDadosNota[0].status <> 'E') or (vDadosNota[0].tipo_nota <> 'N')) then
  //  Exit;

  if (pCaminhoPDF <> '') and (vDadosNota[0].tipo_nota <> 'N') then
    Exit;

  vDadosNotaItens := _NotasFiscaisItens.BuscarNotasFiscaisItens(Sessao.getConexaoBanco, 0, [pNotaFiscalId]);

  if pCaminhoPDF.IsEmpty then
  begin
    pCaminhoPDF := GetCurrentDir + '\NFE\PDF\';
    ForceDirectories(pCaminhoPdf);
  end;

  vXmls :=
    _NotasFiscais.BuscarXMLsNFe(
      Sessao.getConexaoBanco,
      ' where NFI.NOTA_FISCAL_ID = '+ pNotaFiscalId.ToString +
      '   and NFI.STATUS = ''E'' ');

  if vXmls <> nil then begin
    vCaminhoXml := Sessao.getParametrosEstacao2.CaminhoXMLs + 'NFe' + vXmls[0].chave + '-nfe.xml';
    if not FileExists(vCaminhoXml) then
      vXmls[0].xml.SaveToFile(vCaminhoXml);

    vListaAnexo.Add(vCaminhoXml);
    vListaAnexo.Add(pCaminhoPDF + 'nota_' + IntToStr(vDadosNota[0].numero_nota) + '.pdf');
  end;

  Result := Impressao.Danfe.ImprimirDanfe(vDadosNota, vDadosNotaItens, pCaminhoPDF);
  if vDadosNota[0].status = 'E' then begin
    if Result then begin
      vRetorno := _NotasFiscais.SetarDanfeImpresso(Sessao.getConexaoBanco, pNotaFiscalId);
      if vRetorno.TeveErro then begin
        Exclamar('Ocorreu um problema ao tentar atualizar status de impress�o do DANFE. ' + vRetorno.MensagemErro);
        Result := False;
      end;

     // if Perguntar('Deseja enviar danfe por email?','Envio de E-Mail') then
        EnviarEmail(vDadosNota[0].EmailCliente,
                    'Nota Fiscal: '+IntToStr(vDadosNota[0].numero_nota),
                    'Segue em anexo sua nota fiscal! Obrigado pela prefer�ncia!',
                    vListaAnexo);
    end;
  end;
  vListaAnexo.Free;
end;

end.
