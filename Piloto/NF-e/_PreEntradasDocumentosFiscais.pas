unit _PreEntradasDocumentosFiscais;

interface

uses
  _Conexao, _OperacoesBancoDados, _Biblioteca, Vcl.Graphics;

type
  RecDocumentosPreEntradas = record
    PreEntradaId: Integer;
    EmpresaId: Integer;
    ChaveAcesso: string;
    CnpjEmitente: string;
    RazaoSocialEmitente: string;
    InscricaoEstadualEmitente: string;
    DataHoraEmissao: TDateTime;
    NumeroDocumento: Integer;
    SerieDocumento: string;
    ValorDocumento: Double;
    StatusSefaz: string;
    TipoNota: string;
    TipoNotaAnaltico: string;
    StatusAltis: string;
    StatusAltisAnalitico: string;
    StatusSEFAZAnalitico: string;
  end;

function getCorStatusAltis(pStatus: string): TColor;
function getCorTipoNota(pTipoNota: string): TColor;

function BuscarDocumentos(pConexao: TConexao; pComandoNotas: string;  pComandoCTEs: string; pComandoFinal: string): TArray<RecDocumentosPreEntradas>;
function ExistemNotasNaoManifestadas(pConexao: TConexao; pPreEntradaCTeId: Integer): Boolean;

implementation

function getCorStatusAltis(pStatus: string): TColor;
begin
  Result :=
    _Biblioteca.Decode(
      pStatus, [
      'AMA', coCorFonteEdicao1,
      'MAN', coCorFonteEdicao2,
      'ENT', $000096DB,
      'ONR', coCorFonteEdicao3,
      'DES', coCorFonteEdicao4,
      'CON', clMaroon,
      'AEN', $000045FF,
      '', clBlack]
    );
end;

function getCorTipoNota(pTipoNota: string): TColor;
begin
  Result :=
    _Biblioteca.Decode(
      pTipoNota, [
      'NFe', coCorFonteEdicao4,
      'CTe', coCorFonteEdicao3,
      'NFCe', coCorFonteEdicao1,
      '', clBlack]
    );
end;

function BuscarDocumentos(pConexao: TConexao; pComandoNotas: string;  pComandoCTEs: string; pComandoFinal: string): TArray<RecDocumentosPreEntradas>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  * ');
  vSql.Add('from ( ');
  vSql.Add('  select ');
  vSql.Add('    PRE.PRE_ENTRADA_ID, ');
  vSql.Add('    PRE.EMPRESA_ID, ');
  vSql.Add('    PRE.CHAVE_ACESSO_NFE as CHAVE_ACESSO, ');
  vSql.Add('    PRE.CNPJ as CNPJ_EMITENTE, ');
  vSql.Add('    PRE.RAZAO_SOCIAL as RAZAO_SOCIAL_EMITENTE, ');
  vSql.Add('    PRE.INSCRICAO_ESTADUAL as INSCRICAO_ESTADUAL_EMITENTE, ');
  vSql.Add('    PRE.DATA_HORA_EMISSAO, ');
  vSql.Add('    PRE.NUMERO_NOTA as NUMERO_DOCUMENTO, ');
  vSql.Add('    PRE.SERIE_NOTA as SERIE_DOCUMENTO, ');
  vSql.Add('    PRE.VALOR_TOTAL_NOTA as VALOR_DOCUMENTO, ');
  vSql.Add('    PRE.STATUS_NOTA as STATUS_SEFAZ, ');
  vSql.Add('    PRE.TIPO_NOTA, ');
  vSql.Add('    PRE.STATUS as STATUS_ALTIS ');
  vSql.Add('  from ');
  vSql.Add('    PRE_ENTRADAS_NOTAS_FISCAIS PRE ');

  vSql.Add('  left join CADASTROS CAD ');
  vSql.Add('  on PRE.CNPJ = CAD.CPF_CNPJ ');

  vSql.Add(pComandoNotas);

  vSql.Add('  union all ');

  vSql.Add('  select ');
  vSql.Add('    PRE.PRE_ENTRADA_ID, ');
  vSql.Add('    PRE.EMPRESA_ID, ');
  vSql.Add('    PRE.CHAVE_ACESSO_CTE, ');
  vSql.Add('    PRE.CNPJ_EMITENTE, ');
  vSql.Add('    PRE.RAZAO_SOCIAL_EMITENTE, ');
  vSql.Add('    PRE.INSCRICAO_ESTADUAL_EMITENTE, ');
  vSql.Add('    PRE.DATA_HORA_EMISSAO, ');
  vSql.Add('    PRE.NUMERO_CTE as NUMERO_DOCUMENTO, ');
  vSql.Add('    PRE.SERIE_CTE as SERIE_DOCUMENTO, ');
  vSql.Add('    PRE.VALOR_TOTAL_CTE as VALOR_DOCUMENTO, ');
  vSql.Add('    PRE.STATUS_SEFAZ, ');
  vSql.Add('    ''T'' as TIPO_NOTA, ');
  vSql.Add('    PRE.STATUS as STATUS_ALTIS ');
  vSql.Add('  from ');
  vSql.Add('    PRE_ENTRADAS_CTE PRE ');

  vSql.Add('    left join CADASTROS CAD ');
  vSql.Add('    on PRE.CNPJ_EMITENTE = CAD.CPF_CNPJ ');

  vSql.Add(pComandoCTEs);

  vSql.Add(') ');
  vSql.Add(pComandoFinal);

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].PreEntradaId              := vSql.GetInt('PRE_ENTRADA_ID');
      Result[i].EmpresaId                 := vSql.GetInt('EMPRESA_ID');
      Result[i].ChaveAcesso               := vSql.GetString('CHAVE_ACESSO');
      Result[i].CnpjEmitente              := vSql.GetString('CNPJ_EMITENTE');
      Result[i].RazaoSocialEmitente       := vSql.GetString('RAZAO_SOCIAL_EMITENTE');
      Result[i].InscricaoEstadualEmitente := vSql.GetString('INSCRICAO_ESTADUAL_EMITENTE');
      Result[i].DataHoraEmissao           := vSql.getData('DATA_HORA_EMISSAO');
      Result[i].NumeroDocumento           := vSql.GetInt('NUMERO_DOCUMENTO');
      Result[i].SerieDocumento            := vSql.getString('SERIE_DOCUMENTO');
      Result[i].ValorDocumento            := vSql.GetDouble('VALOR_DOCUMENTO');
      Result[i].TipoNota                  := vSql.GetString('TIPO_NOTA');
      Result[i].StatusAltis               := vSql.GetString('STATUS_ALTIS');
      Result[i].StatusSefaz               := vSql.GetString('STATUS_SEFAZ');

      Result[i].StatusAltisAnalitico :=
       _Biblioteca.Decode(
          Result[i].StatusAltis, [
            'AEN', 'Aguardando entrada',
            'AMA', 'Ag.manifesta��o',
            'MAN', 'Manifestada',
            'ENT', 'Entrada realizada',
            'ONR', 'Oper. n�o realizada',
            'DES', 'Desconhecido',
            'CON', 'Oper.realizada'
          ]
       );

      Result[i].StatusSEFAZAnalitico :=
       _Biblioteca.Decode(
          Result[i].StatusSefaz,[
            '1', 'Uso autorizado',
            '2', 'Uso denegado',
            '3', 'Cancelada'
          ]
       );

      Result[i].TipoNotaAnaltico :=
       _Biblioteca.Decode(
          Result[i].TipoNota,[
            'C', 'NFCe',
            'N', 'NFe',
            'T', 'CTe'
          ]
       );

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function ExistemNotasNaoManifestadas(pConexao: TConexao; pPreEntradaCTeId: Integer): Boolean;
var
  vSql: TConsulta;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  count(*) ');
  vSql.Add('from ');
  vSql.Add('  PRE_ENTRADAS_NOTAS_FISCAIS ');
  vSql.Add('where CHAVE_ACESSO_NFE in( ');
  vSql.Add('  select ');
  vSql.Add('    CHAVE_ACESSO_NFE ');
  vSql.Add('  from ');
  vSql.Add('    PRE_ENTR_CTE_NFE_REFERENCIADAS ');
  vSql.Add('  where PRE_ENTRADA_ID = :P1 ');
  vSql.Add(') ');
  vSql.Add('and STATUS = ''AMA'' ');

  vsql.Pesquisar([pPreEntradaCTeId]);
  Result := vSql.GetInt(0) > 0;

  vSql.Free;
end;

end.

