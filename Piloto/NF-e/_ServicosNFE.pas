unit _ServicosNFE;

interface

uses
  System.SysUtils, System.StrUtils, System.Classes, _BibliotecaNFE, _RecordsNFE, _LeitorXML, _Biblioteca, System.DateUtils,
  Soap.SOAPHTTPTrans, _Capicom, Winapi.WinInet, Soap.SOAPConst, JwaWinCrypt, _XMLNFe, ClipBrd;

type
  TTipoManifestacao = (ttCienciaOperacao, ttConfirmacaOperacao, ttOperacaoNaoRealizada, ttDesconhecimentoOperacao);

  TDadosNFe = record
    chave_acesso: string;
    codigo_nota: string;
    codigo_uf_ibge: string;
    numero_nota: string;
    numero_pedido: Integer;
    serie_nota: string;
    natureza_operacao: string;
   // data_emissao: string;
   // hora_emissao: string;
    data_hora_emissao: string;
    tipo_nota: string;
    codigo_municipio_ibge: string;
    tipo_impressao: string;
    tipo_emissao: string;
    digito_verificador: string;
    versao_sistema: string;
    cnpj_emitente: string;
    // ***** Dados da empresa emitente da NFe ***** //
    razao_social_emitente: string;
    nome_fantasia: string;
    logradouro_emitente: string;
    numero_emitente: string;
    complemento_emitente: string;
    bairro_emitente: string;
    municipio_emitente: string;
    uf_emitente: string;
    cep_emitente: string;
    telefone_emitente: string;
    inscricao_estadual_emitente: string;
    cnae_emitente: string;
    codigo_regime_emitente: string;

    // ***** Dados do destinat�rio da NFe ***** //
    e_pessoa_fisica: Boolean;
    cpf_destinatario: string;
    cnpj_destinatario: string;
    razao_social_destinatario: string;
    nome_fantasia_destinatario: string;
    logradouro_destinatario: string;
    numero_destinatario: string;
    complemento_destinatario: string;
    bairro_destinatario: string;
    codigo_municipio_destinatario: string;
    municipio_destinatario: string;
    uf_destinatario: string;
    cep_destinatario: string;
    telefone_destinatario: string;
    inscricao_estadual_destinatario: string;
    email_destinatario: string;

    InscricaoEstEntrDestinat: string;
    LogradouroEntrDestinatario: string;
    ComplementoEntrDestinatario: string;
    NomeBairroEntrDestinatario: string;
    NomeCidadeEntrDestinatario: string;
    NumeroEntrDestinatario: string;
    EstadoIdEntrDestinatario: string;
    CepEntrDestinatario: string;
    CodigoIbgeMunicEntrDest: string;

    FinalidadeNFe: string;
    IndiceDestinatario: string;

    base_calculo_icms: string;
    valor_icms: string;

    valor_icms_inter: string;

    base_calculo_ipi: string;
    valor_ipi: string;
    base_calculo_st: string;
    total_tributos: string;
    valor_st: string;
    valor_total_produtos: string;
    valor_total_frete: string;
    valor_total_seguro: string;
    valor_total_desconto: string;
    valor_total_ipi: string;
    valor_total_pis: string;
    valor_total_cofins: string;
    valor_total_outras_despesas: string;
    valor_total_nota_fiscal: string;
    peso_liquido_total: string;
    peso_bruto_total: string;
    informacoes_complementares: string;
    TipoCliente: string;

    valor_dinheiro: string;
    valor_pix: string;
    valor_cartao_debito: string;
    valor_cartao_credito: string;
    valor_cheque: string;
    valor_credito: string;
    valor_outros: string;
    valor_acumulado: string;
    tipo_frete: string;
    sem_valor: boolean;
    transportadora_id: Integer;
    cnpj_transportadora: string;
    razao_transportadora: string;
    ie_transportadora: string;
    endereco_transportadora: string;
    cidade_transportadora: string;
    uf_transportadora: string;
    especie_nfe: string;
    quantidade_nfe: string;
  end;

  TDadosItensNFE = record
    codigo_produto: string;
    codigo_barras: string;
    descricao_produto: string;
    ncm: string;
    cfop: string;
    unidade_comercial: string;
    quantidade_comercial: string;
    valor_unitario_comercial: string;
    valor_bruto: string;
    codigo_barras_tributavel: string;
    unidade_tributavel: string;
    quantidade_tributavel: string;
    valor_unitario_tributavel: string;
    numero_item_pedido: string;
    valor_desconto: string;
    valor_outras_despesas: string;
    numero_pedido: string;
    total_tributos: string;
    cst: string;
    origem_produto: string;
    modalidade_bc_icms: string;
    valor_base_calculo_icms: string;
    percentual_aliquota_icms: string;
    valor_icms: string;

    base_calculo_icms_inter: string;
    valor_icms_inter: string;
    percentual_icms_inter: string;

    valor_base_calculo_ipi: string;
    percentual_aliquota_ipi: string;
    valor_ipi: string;
    percentual_margem_icms_st: string;
    percentual_reducao_bc_st: string;
    valor_base_calculo_st: string;
    percentual_aliquota_icms_st: string;
    valor_icms_st: string;
    percentual_reducao_bc_icms: string;
    motivo_desoneracao_icms: string;
    base_st_retida_anteriormente: string;
    valor_icms_st_retida_anteriormente: string;

    cst_pis: string;
    BaseCalculoPis: string;
    PercentualPis: string;
    ValorPis: string;

    cst_cofins: string;
    BaseCalculoCofins: string;
    PercentualCofins: string;
    ValorCofins: string;

    informacoes_adicionais_produto: string;
    cest: string;
  end;

  TServicosNFE = class
  private
    FURL: string;
    FConexao: THTTPReqResp;
    FNumeroSerieCertificado: string;
    FCertificado: TCertificadoDigital;

    //FXMLNFe: TXMLNFe;

    FCSC: string;
    FIdToken: string;

    FCodigoUFIBGE: Byte;
    FUFEmitente: string;
    FCaminhoXMLs: string;
    FLeitorXML: TLeitorXML;
    FAmbienteNFE: tpAmbienteNFE;

    FTipoDocumento: TTipoDocumento;
    FDocEletronico: TDadosNFe;
    FDocEletronicoItens: TArray<TDadosItensNFE>;
    FDocEletronicoReferencias: TArray<string>;
    FDocEletronicoCartoesCred: TArray<TDadosCartoes>;
    FDocEletronicoCartoesDeb: TArray<TDadosCartoes>;

    procedure OnAntesPost(const pHTTP: THTTPReqResp; pData: Pointer);

    function getXMLNFE(): string;
    function TratarRetornoEnvioNFe(pRetornoWS: string): RecRespostaNFE;

    function enviarEvento(
      const pLote: Integer;
      const pCodigoEvento: string;
      const pChaveNFe: string;
      const pSequencia: Integer;
      const pCNPJ: string;
      const pDetalheEvento: string;
      const pDataHora: TDateTime;
      const pCodigoUFAuxiliar: string = ''
    ): RecRespostaNFE;

  public
    constructor Create(
      pUF: string;
      pAmbienteNFE: tpAmbienteNFE;
      pCaminhoXMLs: string;
      pNumeroSerieCertificado: string;
      pCSC: string;
      pIdToken: string
    );

    destructor Destroy; override;

    procedure setTipoDocumento(pTipoNota: string);

    procedure setCabecalhoNFE(
      pChaveAcesso: string;
      pCNPJ: string;
      pNumeroNota: Integer;
      pNumeroPedido: Integer;
      pSerie: string;
      pNaturezaOperacao: string;
      pDataHoraEmissao: TDateTime;
      pNotaSaida: Boolean;
      pCodigoMunicipioIbge: Integer;

      pRazaoSocialEmitente: string;
      pNomeFantasia: string;
      pLogradouroEmitente: string;
      pNumeroEmitente: string;
      pComplementoEmitente: string;
      pBairroEmitente: string;
      pMunicipioEmitente: string;
      pUFEmitente: string;
      pCepEmitente: string;
      pTelefoneEmitente: string;
      pInscricaoEstadualEmitente: string;
      pCNAE: String;
      pCodigoRegimeEmitente: string;

      pCPF_CNPJDestinatario: string;
      pRazaoSocialDestinatario: string;
      pNomeFantasiaDestinatario: string;
      pLogradouroDestinatario: string;
      pNumeroDestinatario: string;
      pComplementoDestinatario: string;
      pBairroDestinatario: string;
      pCodigoMunicipioDestinatario: Integer;
      pMunicipioDestinatario: string;
      pUFDestinatario: string;
      pCepDestinatario: string;
      pTelefoneDestinatario: string;
      pInscricaoEstadualDestinatario: string;
      pEmailDestinatario: string;

      pInscricaoEstEntrDestinat: string;
      pLogradouroEntrDestinatario: string;
      pcomplementoEntrDestinatario: string;
      pNomeBairroEntrDestinatario: string;
      pNomeCidadeEntrDestinatario: string;
      pNumeroEntrDestinatario: string;
      pEstadoIdEntrDestinatario: string;
      pCepEntrDestinatario: string;
      pCodigoIbgeMunicEntrDest: Integer;

      pCFOPId: string;
      pBaseCalculoIcms: Currency;
      pValorIcms: Currency;
      pBaseCalculoST: Currency;
      pValorST: Currency;
      pValorTotalProdutos: Currency;
      pValorTotalFrete: Currency;
      pValorTotalSeguro: Currency;
      pValorTotalDesconto: Currency;
      pValorTotalIpi: Currency;
      pValorTotalPis: Currency;
      pValorTotalCofins: Currency;
      pValorTotalOutrasDespesas: Currency;
      pValorTotalNotaFiscal: Currency;
      pPesoLiquidoTotal: Currency;
      pPesoBrutoTotal: Currency;

      pValorDinheiro: Double;
      pValorCheque: Double;
      pValorCartaoCredito: Double;
      pValorCartaoDebito: Double;
      pValorCreditoLoja: Double;
      pValorOutros: Double;

      pInformacoes_complementares: string;

      pTipoCliente: string;
      pValorPix: Double;
      pValorAcumulado: Double;
      pEmissaoNotaAcumuladoFechamentoPedido: string;
      pTipoFrete: Integer;
      pTransportadoraId: Integer;
      pCnpjTransportadora: string;
      pRazaoTransportadora: string;
      pIeTransportadora: string;
      pEnderecoTransportadora: string;
      pCidadeTransportadora: string;
      pUfTransportadora: string;
      pEspecieNfe: string;
      pQuantidadeNfe: string;
      pValorIcmsInter: Currency
    );

    procedure setProdutoNFE(
      pCodigoProduto: string;
      pDescricaoProduto: string;
      pCodigoBarras: string;
      pNCM: string;
      pCFOPId: string;
      pUnidadeComercial: string;
      pQuantidadeComercial: Double;
      pValorUnitarioComercial: Double;
      pValorBruto: Double;
      pCodigoBarrasTributavel: string;
      pUnidadeTributavel: string;
      pQuantidadeTributavel: Double;
      pValorUnitarioTributavel: Double;
      pValorDesconto: Double;
      pValorOutrasDespesas: Double;
      pCST: string;
      pOrigemProduto: Byte;

      pModalidadeBCIcms: Byte;
      pPercentualReducaoBCIcms: Double;
      pValorBaseCalculoIcms: Double;
      pPercentualAliquotaIcms: Double;
      pValorIcms: Double;

      pPercentualMargemIcmsSt: Double;
      pPercentualReducaoBCSt: Double;
      pValorBaseCalculoST: Double;
      pPercentualAliquotaIcmsST: Double;
      pValorIcmsST: Double;

      pMotivoDesoneracaoIcms: string;
      pBaseSTRetidaAnteriormente: Double;
      pValorIcmsSTRetidaAnteriormente: Double;

      pCSTPis: string;
      pBaseCalculoPis: Currency;
      pPercentualPis: Double;
      pValorPis: Currency;

      pCSTCofins: string;
      pBaseCalculoCofins: Currency;
      pPercentualCofins: Double;
      pValorCofins: Currency;

      pInformacoesAdicionaisProduto: string;

      pCest: string;
      pPercentualAliquotaIpi: Double;
      pValorIpi: Double;
      pNumeroItemPedido: Integer;
      pNumeroPedido: string;
      pBaseCalculoIcmsInter: Double;
      pValorIcmsInter: Double;
      pPercentualIcmsInter: Double
    );

    procedure setReferencia(pChaveNFeReferenciada: string);
    procedure setDadosCartoes(
      pTipoIntegracao: string;
      pCnpjAdministradora: string;
      pBandeira: string;
      pCodigoAutorizacao: string;
      pTipoCartao: string;
      pValorCartao: Currency
    );

    procedure SalvarXML(pXML: string; pCaminho: string);

    function getStatusServico: RecRespostaNFE;

    function EnviarNFE(const pApenasGerarXML: Boolean): RecRespostaNFE;
    function ConsultarLoteNFE(const pChaveNFe: string): RecRespostaNFE;

    function CancelarNFe(
      const pLote: Integer;
      const pChaveNFe: string;
      const pCNPJ: string;
      const pDataHora: TDateTime;
      const pProtocolo: string;
      const pJustificativa: string
    ): RecRespostaNFE;

    function InutilizarNumeroNFe(
      const pCNPJ: string;
      const pAno: Integer;
      const pSerie: Integer;
      const pNumeracaoInicial: Integer;
      const pNumeracaoFinal: Integer;
      const pJustificativa: string;
      const pModeloNota: string
    ): RecRespostaNFE;

    function enviarCartaCorrecao(
      const pLote: Integer;
      const pChaveNFe: string;
      const pCNPJ: string;
      const pDataHora: TDateTime;
      const pSequencia: Integer;
      const pTextoCorrecao: string
    ): RecRespostaNFE;

    function manifestarOperacao(
      const pTipoManifestacao: TTipoManifestacao;
      const pLote: Integer;
      const pChaveNFe: string;
      const pCNPJ: string;
      const pDataHora: TDateTime;
      const pJustificativa: string
    ): RecRespostaNFE;  

    function BuscarNFeEntradas(pCNPJ: string; pUltimoNsuConsultadoDistNFe: string; pChaveNFe: string): RecRespostaNFe<RecResumoNFe>;
    function BuscarCTeEntradas(pCNPJ: string; pUltimoNsuConsultadoDistCTE: string): RecRespostaCTe;
  end;

implementation

{ TServicosNFE }

const
  teEventoCancelamento = '110111';

constructor TServicosNFE.Create(
  pUF: string;
  pAmbienteNFE: tpAmbienteNFE;
  pCaminhoXMLs: string;
  pNumeroSerieCertificado: string;
  pCSC: string;
  pIdToken: string
);
begin
  FDocEletronicoItens       := nil;
  FDocEletronicoReferencias := nil;
  FDocEletronicoCartoesCred := nil;
  FDocEletronicoCartoesDeb  := nil;

  FAmbienteNFE  := pAmbienteNFE;
  FCodigoUFIBGE := _BibliotecaNFE.getCodigoUFIBGE(pUF);
  FUFEmitente := pUF;
  FLeitorXML    := TLeitorXML.Create;

  FCaminhoXMLs            := pCaminhoXMLs;
  FNumeroSerieCertificado := pNumeroSerieCertificado;

  FCSC     := pCSC;
  FIdToken := pIdToken;

  FCertificado := TCertificadoDigital.Create(FNumeroSerieCertificado);

  FConexao := THTTPReqResp.Create(nil);

  FConexao.UseUTF8InHeader := True;
  FConexao.OnBeforePost    := Self.OnAntesPost;

  FConexao.Proxy := '';
  FConexao.UserName := '';
  FConexao.Password := '';

  //FXMLNFe := TXMLNFe.Create;
end;

destructor TServicosNFE.Destroy;
begin
  inherited;
  FLeitorXML.Free;
  _BibliotecaNFE.ShutDownXmlSec;
end;

procedure TServicosNFE.setTipoDocumento(pTipoNota: string);
begin
  if pTipoNota = 'N' then
    FTipoDocumento := tpNFe
  else
    FTipoDocumento := tpNFCe;
end;

function TServicosNFE.TratarRetornoEnvioNFe(pRetornoWS: string): RecRespostaNFE;
var
  vXML: string;
  vArquivo: TStringList;
begin
  FLeitorXML.Arquivo := pRetornoWS;
  FLeitorXML.Grupo := FLeitorXML.Arquivo;

  if (FLeitorXML.rExtrai(1, 'protNFe') <> '') or (FLeitorXML.rExtrai(1, 'retConsSitNFe') <> '') or (FLeitorXML.rExtrai(1, 'retEnviNFe') <> '') then begin
    Result.tipo_ambiente := IfThen(FAmbienteNFE = tpProducao, 'Produ��o', 'Homologa��o');
    Result.versao_aplicacao           := FLeitorXML.rCampo(tcStr, 'verAplic');
    Result.status                     := FLeitorXML.rCampo(tcInt, 'cStat');
    Result.motivo                     := FLeitorXML.rCampo(tcStr, 'xMotivo');
    Result.uf                         := FLeitorXML.rCampo(tcStr, 'cUF');
    Result.protocolo.chave_acesso_nfe := FLeitorXML.rCampo(tcStr, 'chNFe');
    Result.HouveErro                  := _BibliotecaNFE.HouveErroRetornoNFE(Result.status);
    Result.mensagem_erro              := Result.motivo;

    if (Result.status = '100') and (FLeitorXML.rExtrai(1, 'protNFe') <> '') then begin
      Result.protocolo.chave_acesso_nfe := FLeitorXML.rCampo(tcStr, 'chNFe');
      Result.protocolo.data_hora_recibo := FLeitorXML.rCampo(tcDatHor, 'dhRecbto');
      Result.protocolo.status := FLeitorXML.rCampo(tcInt, 'cStat');
      Result.protocolo.motivo := FLeitorXML.rCampo(tcStr, 'xMotivo');
      Result.protocolo.numero_protocolo := FLeitorXML.rCampo(tcInt64, 'nProt');

      if not FileExists(FCaminhoXMLs + 'NFe' + Result.protocolo.chave_acesso_nfe + '-nfe.xml') then
        raise Exception.Create('N�o foi encontrado o arquivo XML da NFE com a chave de acesso ' + Result.protocolo.chave_acesso_nfe);

      vArquivo := TStringList.Create;
      vArquivo.LoadFromFile(FCaminhoXMLs + 'NFe' + Result.protocolo.chave_acesso_nfe + '-nfe.xml');
      if vArquivo.Count <= 0 then // Erro
        raise Exception.Create('XML da NFe mal formatado');

      vXml :=
        '<nfeProc xmlns="http://www.portalfiscal.inf.br/nfe" versao="4.00">'  +
          StringReplace(vArquivo.Text, Chr(13) + chr(10), '', [rfReplaceAll]) +
          FLeitorXML.rExtrai(1, 'protNFe', 'retEnviNFe')                      +
          FLeitorXML.rExtrai(1, 'protNFe', 'retConsSitNFe')                   +
        '</nfeProc>';

      vArquivo.Free;

      SalvarXML(vXml, FCaminhoXMLs + 'NFe' + Result.protocolo.chave_acesso_nfe + '-nfe.xml');

      Result.caminho_xml_nfe := FCaminhoXMLs + 'NFe' + Result.protocolo.chave_acesso_nfe + '-nfe.xml';
      Result.textoXML := vXml;
    end;
  end;
end;

{
  M�todo desenvolvido para receber as informa��es do header da nota fiscal eletr�nica,
  converte-las de maneira que precisa ir para o XML
}
procedure TServicosNFE.setCabecalhoNFE(
  pChaveAcesso: string;
  pCNPJ: string;
  pNumeroNota: Integer;
  pNumeroPedido: Integer;
  pSerie: string;
  pNaturezaOperacao: string;
  pDataHoraEmissao: TDateTime;
  pNotaSaida: Boolean;
  pCodigoMunicipioIbge: Integer;

  pRazaoSocialEmitente: string;
  pNomeFantasia: string;
  pLogradouroEmitente: string;
  pNumeroEmitente: string;
  pComplementoEmitente: string;
  pBairroEmitente: string;
  pMunicipioEmitente: string;
  pUFEmitente: string;
  pCepEmitente: string;
  pTelefoneEmitente: string;
  pInscricaoEstadualEmitente: string;
  pCNAE: String;
  pCodigoRegimeEmitente: string;

  pCPF_CNPJDestinatario: string;
  pRazaoSocialDestinatario: string;
  pNomeFantasiaDestinatario: string;
  pLogradouroDestinatario: string;
  pNumeroDestinatario: string;
  pComplementoDestinatario: string;
  pBairroDestinatario: string;
  pCodigoMunicipioDestinatario: Integer;
  pMunicipioDestinatario: string;
  pUFDestinatario: string;
  pCepDestinatario: string;
  pTelefoneDestinatario: string;
  pInscricaoEstadualDestinatario: string;
  pEmailDestinatario: string;

  pInscricaoEstEntrDestinat: string;
  pLogradouroEntrDestinatario: string;
  pcomplementoEntrDestinatario: string;
  pNomeBairroEntrDestinatario: string;
  pNomeCidadeEntrDestinatario: string;
  pNumeroEntrDestinatario: string;
  pEstadoIdEntrDestinatario: string;
  pCepEntrDestinatario: string;
  pCodigoIbgeMunicEntrDest: Integer;

  pCFOPId: string;
  pBaseCalculoIcms: Currency;
  pValorIcms: Currency;
  pBaseCalculoST: Currency;
  pValorST: Currency;
  pValorTotalProdutos: Currency;
  pValorTotalFrete: Currency;
  pValorTotalSeguro: Currency;
  pValorTotalDesconto: Currency;
  pValorTotalIpi: Currency;
  pValorTotalPis: Currency;
  pValorTotalCofins: Currency;
  pValorTotalOutrasDespesas: Currency;
  pValorTotalNotaFiscal: Currency;
  pPesoLiquidoTotal: Currency;
  pPesoBrutoTotal: Currency;

  pValorDinheiro: Double;
  pValorCheque: Double;
  pValorCartaoCredito: Double;
  pValorCartaoDebito: Double;
  pValorCreditoLoja: Double;
  pValorOutros: Double;

  pInformacoes_complementares: string;
  pTipoCliente: string;
  pValorPix: Double;
  pValorAcumulado: Double;
  pEmissaoNotaAcumuladoFechamentoPedido: string;
  pTipoFrete: Integer;
  pTransportadoraId: Integer;
  pCnpjTransportadora: string;
  pRazaoTransportadora: string;
  pIeTransportadora: string;
  pEnderecoTransportadora: string;
  pCidadeTransportadora: string;
  pUfTransportadora: string;
  pEspecieNfe: string;
  pQuantidadeNfe: string;
  pValorIcmsInter: Currency
);
var
  codigo_nota: Integer;
  digito_verificador: Integer;
begin
//  if pChaveAcesso = '' then begin
    FDocEletronico.chave_acesso                := _BibliotecaNFE.GerarChaveNFE(FCodigoUFIBGE, StrToInt(pSerie), pNumeroNota, 1, pDataHoraEmissao, pCNPJ, FTipoDocumento, codigo_nota, digito_verificador);

    if FDocEletronico.chave_acesso = '' then
      raise Exception.Create('Erro ao gerar a chave de acesso da nfe!');

     //ajustado a linha 594 FCodigoUFIBGE Ezequiel
    FDocEletronico.codigo_nota                 := _Biblioteca.LPad(IntToStr(FCodigoUFIBGE), 8, '0');
    FDocEletronico.digito_verificador          := IntToStr(digito_verificador);
//  end
//  else begin
//    FDocEletronico.chave_acesso                := 'NFe' + pChaveAcesso;
//    FDocEletronico.codigo_nota                 := Copy(pChaveAcesso, 37, 8);
//    FDocEletronico.digito_verificador          := Copy(pChaveAcesso, 44, 1);
//  end;

  FDocEletronico.codigo_uf_ibge                := _Biblioteca.LPad(IntToStr(FCodigoUFIBGE), 2, '0');
  FDocEletronico.natureza_operacao             := _Biblioteca.RemoverCaracteresEspeciais( Copy(pNaturezaOperacao, 1, 60) );
  FDocEletronico.serie_nota                    := pSerie;
  FDocEletronico.numero_nota                   := IntToStr(pNumeroNota);
  FDocEletronico.numero_pedido                 := pNumeroPedido;
//  FDocEletronico.data_emissao                  := FormatDateTime('YYYY-MM-DD', pDataHoraEmissao);
//  FDocEletronico.hora_emissao                  := FormatDateTime('HH:NN:SS', pDataHoraEmissao);
  FDocEletronico.data_hora_emissao             := _BibliotecaNFE.FDataHoraXML(pDataHoraEmissao);
  FDocEletronico.tipo_nota                     := IfThen(pNotaSaida, '1', '0');
  FDocEletronico.codigo_municipio_ibge         := IntToStr(pCodigoMunicipioIbge);
  FDocEletronico.tipo_impressao                := '1';
  FDocEletronico.tipo_emissao                  := '1';
  FDocEletronico.versao_sistema                := 'Hiva';
  FDocEletronico.cnpj_emitente                 := _Biblioteca.RetornaNumeros(pCNPJ);
  FDocEletronico.razao_social_emitente         := _Biblioteca.RemoverCaracteresEspeciais(pRazaoSocialEmitente);
  FDocEletronico.nome_fantasia                 := _Biblioteca.RemoverCaracteresEspeciais(pNomeFantasia);
  FDocEletronico.logradouro_emitente           := _Biblioteca.RemoverCaracteresEspeciais(pLogradouroEmitente);
  FDocEletronico.numero_emitente               := pNumeroEmitente;
  FDocEletronico.complemento_emitente          := _Biblioteca.RemoverCaracteresEspeciais(pComplementoEmitente);
  FDocEletronico.bairro_emitente               := _Biblioteca.RemoverCaracteresEspeciais(pBairroEmitente);
  FDocEletronico.municipio_emitente            := _Biblioteca.RemoverCaracteresEspeciais(pMunicipioEmitente);
  FDocEletronico.uf_emitente                   := pUFEmitente;
  FDocEletronico.cep_emitente                  := _Biblioteca.LPad(_Biblioteca.RetornaNumeros(pCepEmitente), 8, '0');
  FDocEletronico.telefone_emitente             := _Biblioteca.RetornaNumeros(pTelefoneEmitente);
  FDocEletronico.inscricao_estadual_emitente   := _Biblioteca.RetornaNumeros(pInscricaoEstadualEmitente);
  FDocEletronico.cnae_emitente                 := _Biblioteca.RetornaNumeros(pCNAE);

  if pCodigoRegimeEmitente = 'SN' then // Se for Simples Nacional
    FDocEletronico.codigo_regime_emitente        := '1'
  else // Se for Lucro Presumido ou Real
    FDocEletronico.codigo_regime_emitente        := '3';

  if Length(_Biblioteca.RetornaNumeros(pCPF_CNPJDestinatario)) <= 11 then begin
    if FAmbienteNFE = tpHomologacao then begin
      FDocEletronico.cnpj_destinatario           := '99999999000191';
      FDocEletronico.nome_fantasia_destinatario  := 'NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL';
    end
    else
      FDocEletronico.nome_fantasia_destinatario  := Copy(_Biblioteca.RemoverCaracteresEspeciais(pNomeFantasiaDestinatario), 1, 60);

    FDocEletronico.e_pessoa_fisica             := True;
    FDocEletronico.cpf_destinatario            :=  _Biblioteca.RetornaNumeros(pCPF_CNPJDestinatario);
  end
  else begin
    if FAmbienteNFE = tpHomologacao then begin
      FDocEletronico.e_pessoa_fisica             := False;
      FDocEletronico.cnpj_destinatario           := '99999999000191';
      FDocEletronico.razao_social_destinatario   := 'NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL';
    end
    else begin
      FDocEletronico.e_pessoa_fisica             := False;
      FDocEletronico.cnpj_destinatario           := _Biblioteca.RetornaNumeros(pCPF_CNPJDestinatario);
      FDocEletronico.razao_social_destinatario   := Copy(_Biblioteca.RemoverCaracteresEspeciais(pRazaoSocialDestinatario), 1, 60);
    end;
  end;

  FDocEletronico.logradouro_destinatario       := _Biblioteca.RemoverCaracteresEspeciais(pLogradouroDestinatario);
  FDocEletronico.numero_destinatario           := Trim(pNumeroDestinatario);
  FDocEletronico.complemento_destinatario      := _Biblioteca.RemoverCaracteresEspeciais(pComplementoDestinatario);
  FDocEletronico.bairro_destinatario           := _Biblioteca.RemoverCaracteresEspeciais(pBairroDestinatario);
  FDocEletronico.codigo_municipio_destinatario := IntToStr(pCodigoMunicipioDestinatario);
  FDocEletronico.municipio_destinatario        := _Biblioteca.RemoverCaracteresEspeciais(pMunicipioDestinatario);
  FDocEletronico.uf_destinatario               := Trim(pUFDestinatario);
  FDocEletronico.cep_destinatario              := _Biblioteca.LPad(_Biblioteca.RetornaNumeros(pCepDestinatario), 8, '0');
  FDocEletronico.telefone_destinatario         := Trim(pTelefoneDestinatario);
  FDocEletronico.inscricao_estadual_destinatario:= pInscricaoEstadualDestinatario;
  FDocEletronico.email_destinatario             := Trim(pEmailDestinatario);

  FDocEletronico.InscricaoEstEntrDestinat       := pInscricaoEstEntrDestinat;
  FDocEletronico.LogradouroEntrDestinatario     := _Biblioteca.RemoverCaracteresEspeciais(pLogradouroEntrDestinatario);
  FDocEletronico.ComplementoEntrDestinatario    := _Biblioteca.RemoverCaracteresEspeciais(pComplementoEntrDestinatario);
  FDocEletronico.NomeBairroEntrDestinatario     := _Biblioteca.RemoverCaracteresEspeciais(pNomeBairroEntrDestinatario);
  FDocEletronico.NomeCidadeEntrDestinatario     := _Biblioteca.RemoverCaracteresEspeciais(pNomeCidadeEntrDestinatario);
  FDocEletronico.NumeroEntrDestinatario         := _Biblioteca.RemoverCaracteresEspeciais(pNumeroEntrDestinatario);
  FDocEletronico.EstadoIdEntrDestinatario       := pEstadoIdEntrDestinatario;
  FDocEletronico.CepEntrDestinatario            := pCepEntrDestinatario;
  FDocEletronico.CodigoIbgeMunicEntrDest        := IntToStr(pCodigoIbgeMunicEntrDest);

  pCFOPId := Copy(pCFOPId, 1, 4);

  if Em(pCFOPId, ['1202', '1204', '1209', '1410', '1411', '2202', '2204', '2209', '2410', '2411', '5202', '6202', '5209', '6209', '5411', '6411', '5413', '6413']) then
    FDocEletronico.FinalidadeNFe := '4' // Devolu��o
  else if Em(pCFOPId, ['5601', '5602']) then
    FDocEletronico.FinalidadeNFe := '3' // NFe de ajuste
  else
    FDocEletronico.FinalidadeNFe := '1';

  // 1 - Interna, 2 - Interestadual, 3 - exterior
  if Em(Copy(pCFOPId, 1, 1), ['1', '5']) then
    FDocEletronico.IndiceDestinatario := '1'
  else
    FDocEletronico.IndiceDestinatario := '2';

  FDocEletronico.base_calculo_icms             := FormatoValorNFe(pBaseCalculoIcms);
  FDocEletronico.valor_icms                    := FormatoValorNFe(pValorIcms);

  if pValorIcmsInter > 0 then
    FDocEletronico.valor_icms_inter            := FormatoValorNFe(pValorIcmsInter);

  FDocEletronico.valor_ipi                     := FormatoValorNFe(pValorTotalIpi);
  FDocEletronico.base_calculo_st               := FormatoValorNFe(pBaseCalculoST);
  FDocEletronico.valor_st                      := FormatoValorNFe(pValorST);
  FDocEletronico.valor_total_produtos          := FormatoValorNFe(pValorTotalProdutos);
  FDocEletronico.valor_total_frete             := FormatoValorNFe(pValorTotalFrete);
  FDocEletronico.valor_total_seguro            := FormatoValorNFe(pValorTotalSeguro);
  FDocEletronico.valor_total_desconto          := FormatoValorNFe(pValorTotalDesconto);
  FDocEletronico.valor_total_ipi               := FormatoValorNFe(pValorTotalIpi);
  FDocEletronico.valor_total_pis               := FormatoValorNFe(pValorTotalPis);
  FDocEletronico.valor_total_cofins            := FormatoValorNFe(pValorTotalCofins);
  FDocEletronico.valor_total_outras_despesas   := FormatoValorNFe(pValorTotalOutrasDespesas);
  FDocEletronico.valor_total_nota_fiscal       := FormatoValorNFe(pValorTotalNotaFiscal);
  FDocEletronico.peso_liquido_total            := FormatoValorNFe(pPesoLiquidoTotal, 3);
  FDocEletronico.peso_bruto_total              := FormatoValorNFe(pPesoBrutoTotal, 3);

  FDocEletronico.especie_nfe := pEspecieNfe;
  FDocEletronico.quantidade_nfe := pQuantidadeNfe;
  FDocEletronico.transportadora_id := pTransportadoraId;
  FDocEletronico.cnpj_transportadora := pCnpjTransportadora;
  FDocEletronico.razao_transportadora := pRazaoTransportadora;
  FDocEletronico.ie_transportadora := pIeTransportadora;
  FDocEletronico.endereco_transportadora := pEnderecoTransportadora;
  FDocEletronico.cidade_transportadora := pCidadeTransportadora;
  FDocEletronico.uf_transportadora := pUfTransportadora;

  FDocEletronico.total_tributos                := '0.00';

  FDocEletronico.valor_dinheiro                := FormatoValorNFe(pValorDinheiro);
  FDocEletronico.valor_cheque                  := FormatoValorNFe(pValorCheque);
  FDocEletronico.valor_cartao_debito           := FormatoValorNFe(pValorCartaoDebito);
  FDocEletronico.valor_cartao_credito          := FormatoValorNFe(pValorCartaoCredito);
  FDocEletronico.valor_credito                 := FormatoValorNFe(pValorCreditoLoja);
  FDocEletronico.valor_pix                     := FormatoValorNFe(pValorPix);
  //Caso utilize emiss�o da nota do acumulado no fechamento do pedido o valor do acumulado ser� adicionado ao pagamento 99 - outros
  if (pValorAcumulado > 0) and (pEmissaoNotaAcumuladoFechamentoPedido = 'S') then
    pValorOutros := pValorOutros + pValorAcumulado;

  FDocEletronico.sem_valor :=
    pValorDinheiro +
    pValorCheque +
    pValorCartaoDebito +
    pValorCartaoCredito +
    pValorCreditoLoja +
    pValorPix +
    pValorOutros = 0;

  FDocEletronico.valor_outros                  := FormatoValorNFe(pValorOutros);

  FDocEletronico.informacoes_complementares    := _BibliotecaNFE.trataTagTextoNFe(pInformacoes_complementares);
  FDocEletronico.TipoCliente                   := pTipoCliente;
  FDocEletronico.tipo_frete                    := pTipoFrete.ToString;
end;

procedure TServicosNFE.setProdutoNFE(
  pCodigoProduto: string;
  pDescricaoProduto: string;
  pCodigoBarras: string;
  pNCM: string;
  pCFOPId: string;
  pUnidadeComercial: string;
  pQuantidadeComercial: Double;
  pValorUnitarioComercial: Double;
  pValorBruto: Double;
  pCodigoBarrasTributavel: string;
  pUnidadeTributavel: string;
  pQuantidadeTributavel: Double;
  pValorUnitarioTributavel: Double;
  pValorDesconto: Double;
  pValorOutrasDespesas: Double;
  pCST: string;
  pOrigemProduto: Byte;

  pModalidadeBCIcms: Byte;
  pPercentualReducaoBCIcms: Double;
  pValorBaseCalculoIcms: Double;
  pPercentualAliquotaIcms: Double;
  pValorIcms: Double;

  pPercentualMargemIcmsSt: Double;
  pPercentualReducaoBCSt: Double;
  pValorBaseCalculoST: Double;
  pPercentualAliquotaIcmsST: Double;
  pValorIcmsST: Double;

  pMotivoDesoneracaoIcms: string;
  pBaseSTRetidaAnteriormente: Double;
  pValorIcmsSTRetidaAnteriormente: Double;

  pCSTPis: string;
  pBaseCalculoPis: Currency;
  pPercentualPis: Double;
  pValorPis: Currency;

  pCSTCofins: string;
  pBaseCalculoCofins: Currency;
  pPercentualCofins: Double;
  pValorCofins: Currency;

  pInformacoesAdicionaisProduto: string;

  pCest: string;
  pPercentualAliquotaIpi: Double;
  pValorIpi: Double;
  pNumeroItemPedido: Integer;
  pNumeroPedido: string;
  pBaseCalculoIcmsInter: Double;
  pValorIcmsInter: Double;
  pPercentualIcmsInter: Double
);
var
  posic: Word;
begin
  SetLength(FDocEletronicoItens, Length(FDocEletronicoItens) + 1);
  posic := High(FDocEletronicoItens);

  pCFOPId                                                       := Copy(pCFOPId, 1, 4);
  FDocEletronicoItens[posic].codigo_produto                     := pCodigoProduto;

  if FAmbienteNFE = tpProducao then
    FDocEletronicoItens[posic].descricao_produto                := _Biblioteca.RemoverCaracteresEspeciais(pDescricaoProduto)
  else
    FDocEletronicoItens[posic].descricao_produto                := 'NOTA FISCAL EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL';

  FDocEletronicoItens[posic].codigo_barras                      := IIfStr(pCodigoBarras <> '', pCodigoBarras, 'SEM GTIN');
  FDocEletronicoItens[posic].ncm                                := pNCM;
  FDocEletronicoItens[posic].cfop                               := pCFOPId;
  FDocEletronicoItens[posic].unidade_comercial                  := _Biblioteca.RemoverCaracteresEspeciais(pUnidadeComercial);
  FDocEletronicoItens[posic].quantidade_comercial               := FormatoValorNFe(pQuantidadeComercial, 4);
  FDocEletronicoItens[posic].valor_unitario_comercial           := FormatoValorNFe(pValorUnitarioComercial);
  FDocEletronicoItens[posic].valor_bruto                        := FormatoValorNFe(pValorBruto);
  FDocEletronicoItens[posic].codigo_barras_tributavel           := IIfStr(pCodigoBarras <> '', pCodigoBarrasTributavel, 'SEM GTIN');
  FDocEletronicoItens[posic].unidade_tributavel                 := _Biblioteca.RemoverCaracteresEspeciais(pUnidadeTributavel);
  FDocEletronicoItens[posic].quantidade_tributavel              := FormatoValorNFe(pQuantidadeTributavel, 4);
  FDocEletronicoItens[posic].valor_unitario_tributavel          := FormatoValorNFe(pValorUnitarioTributavel);
  FDocEletronicoItens[posic].valor_desconto                     := FormatoValorNFe(pValorDesconto);
  FDocEletronicoItens[posic].valor_outras_despesas              := FormatoValorNFe(pValorOutrasDespesas);
  FDocEletronicoItens[posic].numero_item_pedido                 := NFormatN(pNumeroItemPedido);
  FDocEletronicoItens[posic].numero_pedido                      := pNumeroPedido;

  // Se for do simples nacional
  if FDocEletronico.codigo_regime_emitente = '1' then begin
    if (Em(pCST, ['00', '20'])) and (pValorIcms > 0) then
  //  if (Em(pCST, ['00', '20'])) then
      FDocEletronicoItens[posic].cst                 := '101'
    else if Em(pCST, ['40', '41', '50', '51']) then
      FDocEletronicoItens[posic].cst                 := '400'
    else if (Em(pCST, ['00', '20'])) then
      FDocEletronicoItens[posic].cst                 := '102'
    else if (Em(pCST, ['10', '70'])) then
      FDocEletronicoItens[posic].cst                 := '201'
    else if pCST = '30' then
      FDocEletronicoItens[posic].cst                 := '202'
    else if pCST = '60' then
      FDocEletronicoItens[posic].cst                 := '500'
    else if pCST = '90' then
      FDocEletronicoItens[posic].cst                 := '900';
  end
  else
    FDocEletronicoItens[posic].cst := pCST;

  FDocEletronicoItens[posic].origem_produto                     := IntToStr(pOrigemProduto);
  FDocEletronicoItens[posic].modalidade_bc_icms                 := IntToStr(pModalidadeBCIcms);
  FDocEletronicoItens[posic].valor_base_calculo_icms            := FormatoValorNFe(pValorBaseCalculoIcms);
  FDocEletronicoItens[posic].percentual_aliquota_icms           := FormatoValorNFe(pPercentualAliquotaIcms);
  FDocEletronicoItens[posic].valor_icms                         := FormatoValorNFe(pValorIcms);

  if pValorIcmsInter > 0 then begin
    FDocEletronicoItens[posic].valor_icms_inter                   := FormatoValorNFe(pValorIcmsInter);
    FDocEletronicoItens[posic].base_calculo_icms_inter            := FormatoValorNFe(pBaseCalculoIcmsInter);
    FDocEletronicoItens[posic].percentual_icms_inter              := FormatoValorNFe(pPercentualIcmsInter);
  end;

  FDocEletronicoItens[posic].valor_ipi                          := FormatoValorNFe(pValorIpi);
  FDocEletronicoItens[posic].percentual_aliquota_ipi            := FormatoValorNFe(pPercentualAliquotaIpi);
  FDocEletronicoItens[posic].percentual_margem_icms_st          := FormatoValorNFe(pPercentualMargemIcmsSt);
  FDocEletronicoItens[posic].percentual_reducao_bc_st           := FormatoValorNFe(pPercentualReducaoBCSt);
  FDocEletronicoItens[posic].valor_base_calculo_st              := FormatoValorNFe(pValorBaseCalculoST);
  FDocEletronicoItens[posic].percentual_aliquota_icms_st        := FormatoValorNFe(pPercentualAliquotaIcmsST);
  FDocEletronicoItens[posic].valor_icms_st                      := FormatoValorNFe(pValorIcmsST);
  FDocEletronicoItens[posic].percentual_reducao_bc_icms         := FormatoValorNFe(pPercentualReducaoBCIcms);
  FDocEletronicoItens[posic].motivo_desoneracao_icms            := _Biblioteca.RemoverCaracteresEspeciais(pMotivoDesoneracaoIcms);
  FDocEletronicoItens[posic].base_st_retida_anteriormente       := FormatoValorNFe(pBaseSTRetidaAnteriormente);
  FDocEletronicoItens[posic].valor_icms_st_retida_anteriormente := FormatoValorNFe(pValorIcmsSTRetidaAnteriormente);

  FDocEletronicoItens[posic].cst_pis := _Biblioteca.LPad(pCSTPis, 2, '0');
  if Em(FDocEletronicoItens[posic].cst_pis, ['50', '51', '52', '54', '55', '56', '60', '61', '62', '63', '64', '65', '66', '67']) then
    FDocEletronicoItens[posic].cst_pis := '01'
  else if FDocEletronicoItens[posic].cst_pis = '70' then
    FDocEletronicoItens[posic].cst_pis := '06'
  else if FDocEletronicoItens[posic].cst_pis = '71' then
    FDocEletronicoItens[posic].cst_pis := '07'
  else if FDocEletronicoItens[posic].cst_pis = '72' then
    FDocEletronicoItens[posic].cst_pis := '09'
  else if FDocEletronicoItens[posic].cst_pis = '73' then
    FDocEletronicoItens[posic].cst_pis := '04'
  else if FDocEletronicoItens[posic].cst_pis = '74' then
    FDocEletronicoItens[posic].cst_pis := '08'
  else if FDocEletronicoItens[posic].cst_pis = '75' then
    FDocEletronicoItens[posic].cst_pis := '05';

  FDocEletronicoItens[posic].BaseCalculoPis                     := FormatoValorNFe(pBaseCalculoPis);
  FDocEletronicoItens[posic].PercentualPis                      := FormatoValorNFe(pPercentualPis);
  FDocEletronicoItens[posic].ValorPis                           := FormatoValorNFe(pValorPis);

  FDocEletronicoItens[posic].cst_cofins := _Biblioteca.LPad(pCSTCofins, 2, '0');
  if Em(FDocEletronicoItens[posic].cst_cofins, ['50', '51', '52', '54', '55', '56', '60', '61', '62', '63', '64', '65', '66', '67']) then
    FDocEletronicoItens[posic].cst_cofins := '01'
  else if FDocEletronicoItens[posic].cst_cofins = '70' then
    FDocEletronicoItens[posic].cst_cofins := '06'
  else if FDocEletronicoItens[posic].cst_cofins = '71' then
    FDocEletronicoItens[posic].cst_cofins := '07'
  else if FDocEletronicoItens[posic].cst_cofins = '72' then
    FDocEletronicoItens[posic].cst_cofins := '09'
  else if FDocEletronicoItens[posic].cst_cofins = '73' then
    FDocEletronicoItens[posic].cst_cofins := '04'
  else if FDocEletronicoItens[posic].cst_cofins = '74' then
    FDocEletronicoItens[posic].cst_cofins := '08'
  else if FDocEletronicoItens[posic].cst_cofins = '75' then
    FDocEletronicoItens[posic].cst_cofins := '05';

  FDocEletronicoItens[posic].BaseCalculoCofins                  := FormatoValorNFe(pBaseCalculoCofins);
  FDocEletronicoItens[posic].PercentualCofins                   := FormatoValorNFe(pPercentualCofins);
  FDocEletronicoItens[posic].ValorCofins                        := FormatoValorNFe(pValorCofins);
  FDocEletronicoItens[posic].total_tributos                     := '0.00';

  FDocEletronicoItens[posic].informacoes_adicionais_produto     := _Biblioteca.RemoverCaracteresEspeciais(pInformacoesAdicionaisProduto);
  FDocEletronicoItens[posic].cest                               := pCest;
end;

procedure TServicosNFE.setReferencia(pChaveNFeReferenciada: string);
begin
  SetLength(FDocEletronicoReferencias, Length(FDocEletronicoReferencias) + 1);
  FDocEletronicoReferencias[High(FDocEletronicoReferencias)] := _Biblioteca.RetornaNumeros(pChaveNFeReferenciada);
end;

procedure TServicosNFE.setDadosCartoes(
  pTipoIntegracao: string;
  pCnpjAdministradora: string;
  pBandeira: string;
  pCodigoAutorizacao: string;
  pTipoCartao: string;
  pValorCartao: Currency
);
begin
  if not Em(pTipoIntegracao, ['TEF', 'POS']) then
    raise Exception.Create('O tipo de integra��o do cart�o n�o foi informado corretamentes!');

  if Length(_Biblioteca.RetornaNumeros(pCnpjAdministradora)) < 14 then
    raise Exception.Create('O CNPJ da administradora do cart�o n�o foi informado corretamente!');

  if not Em(pBandeira, ['01', '02', '03', '04', '05', '06', '07', '08', '09', '99']) then
    raise Exception.Create('A bandeira do cart�o n�o foi informada corretamente!');

  if pCodigoAutorizacao = '' then
    raise Exception.Create('O c�digo de autoriza��o n�o foi informado corretamente, por favor informe os dados no movimento!');

  if pTipoCartao = 'C' then begin
    SetLength(FDocEletronicoCartoesCred, Length(FDocEletronicoCartoesCred) + 1);
    FDocEletronicoCartoesCred[High(FDocEletronicoCartoesCred)].TipoIntegracao     := IIfStr(pTipoIntegracao = 'TEF', '1', '2');
    FDocEletronicoCartoesCred[High(FDocEletronicoCartoesCred)].CnpjAdminsitradora := _Biblioteca.RetornaNumeros(pCnpjAdministradora);
    FDocEletronicoCartoesCred[High(FDocEletronicoCartoesCred)].Bandeira           := pBandeira;
    FDocEletronicoCartoesCred[High(FDocEletronicoCartoesCred)].CodigoAutorizacao  := pCodigoAutorizacao;
    FDocEletronicoCartoesCred[High(FDocEletronicoCartoesCred)].ValorCartao        := FormatoValorNFe(pValorCartao);
  end
  else begin
    SetLength(FDocEletronicoCartoesDeb, Length(FDocEletronicoCartoesDeb) + 1);
    FDocEletronicoCartoesDeb[High(FDocEletronicoCartoesDeb)].TipoIntegracao     := IIfStr(pTipoIntegracao = 'TEF', '1', '2');
    FDocEletronicoCartoesDeb[High(FDocEletronicoCartoesDeb)].CnpjAdminsitradora := _Biblioteca.RetornaNumeros(pCnpjAdministradora);
    FDocEletronicoCartoesDeb[High(FDocEletronicoCartoesDeb)].Bandeira           := pBandeira;
    FDocEletronicoCartoesDeb[High(FDocEletronicoCartoesDeb)].CodigoAutorizacao  := pCodigoAutorizacao;
    FDocEletronicoCartoesDeb[High(FDocEletronicoCartoesDeb)].ValorCartao        := FormatoValorNFe(pValorCartao);
  end;
end;

function TServicosNFE.EnviarNFE(const pApenasGerarXML: Boolean): RecRespostaNFE;
var
  vXml: string;
  vRetWS: string;
  vMensagem: string;
  vRetornoWS: string;
  vAcao : TStringList;
  vStream: TMemoryStream;
  vStrStream: TStringStream;
  vURLXmlns: string;

  procedure AdicionarNoXML(pAdicao: string);
  begin
    vXml := vXml + pAdicao;
  end;

begin
  vXml := '';
  Result.HouveErro := False;
  vAcao := TStringList.Create;
  vStream := TMemoryStream.Create;
  vStrStream := TStringStream.Create('');
  try
    try
      vMensagem :=
        '<?xml version="1.0" encoding="utf-8"?>'                               +
          '<enviNFe xmlns="http://www.portalfiscal.inf.br/nfe" versao="4.00">' +
            '<idLote>' + FDocEletronico.numero_nota + '</idLote>'              +
            '<indSinc>1</indSinc>'                                             +
             getXMLNFE() +
          '</enviNFe>';

      SalvarXML(vMensagem, FCaminhoXMLs + FDocEletronico.numero_nota + '-env-lot.xml');
      //_BibliotecaNFE.ValidarXML(vMensagem, FCaminhoSchemas, tsNfeAutorizacao);

      FURL := _BibliotecaNFE.getURLWebService(FDocEletronico.uf_emitente, FAmbienteNFE, tsNfeAutorizacao, FTipoDocumento = tpNFCe );
      vMensagem := StringReplace(vMensagem, '<?xml version="1.0" encoding="utf-8"?>', '', [rfReplaceAll]);

      if FUFEmitente = 'GO' then
        vURLXmlns := 'http://www.portalfiscal.inf.br/nfe/wsdl/NFeAutorizacao'
      else
        vURLXmlns := 'http://www.portalfiscal.inf.br/nfe/wsdl/NFeAutorizacao4';

      AdicionarNoXML('<?xml version="1.0" encoding="utf-8"?>'                                                                                                                                        );
      AdicionarNoXML(  '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">' );
      AdicionarNoXML(    '<soap12:Header>'                                                                                                                                                           );
      AdicionarNoXML(      '<nfeCabecMsg xmlns="' + vURLXmlns + '">'                                                                                                                                 );
      AdicionarNoXML(        '<versaoDados>4.00</versaoDados>'                                                                                                                                       );
      AdicionarNoXML(        '<cUF>' + IntToStr(FCodigoUFIBGE) + '</cUF>'                                                                                                                            );
      AdicionarNoXML(      '</nfeCabecMsg>'                                                                                                                                                          );
      AdicionarNoXML(    '</soap12:Header>'                                                                                                                                                          );
      AdicionarNoXML(    '<soap12:Body>'                                                                                                                                                             );
      AdicionarNoXML(      '<nfeDadosMsg xmlns="' + vURLXmlns + '">'                                                                                                                                 );
      AdicionarNoXML(       vMensagem                                                                                                                                                                );
      AdicionarNoXML(      '</nfeDadosMsg>'                                                                                                                                                          );
      AdicionarNoXML(    '</soap12:Body>'                                                                                                                                                            );
      AdicionarNoXML(  '</soap12:Envelope>'                                                                                                                                                          );

      if pApenasGerarXML then begin
        vXml := Copy(vMensagem, Pos('<NFe', vMensagem), Pos('</NFe>', vMensagem) - Pos('<NFe', vMensagem) + 6);
        SalvarXML(vXml, FCaminhoXMLs + FDocEletronico.chave_acesso + '-nfe.xml');

        Result.caminho_xml_nfe:= FCaminhoXMLs + FDocEletronico.chave_acesso + '-nfe.xml';
        Exit;
      end;

      if not _Biblioteca.TemConexaoInternet then begin
        Result.mensagem_erro := 'Sem conex�o com a internet, por favor, verifique!';
        Result.HouveErro := True;
        Exit;
      end;

      FConexao.URL := FURL + '?wsdl';
      FConexao.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/NFeAutorizacao4';

      FConexao.Execute(vXml, vStream);
      vStrStream.CopyFrom(vStream, 0);

      vRetornoWS := RetirarAcentos( string(ParseText(AnsiString(vStrStream.DataString), True, True)) );
      vRetWS := string(SepararDados( AnsiString(vRetornoWS),'nfeResultMsg') );

      if vRetWS = '' then
        raise Exception.Create('Nenhum dado retornado do WebService da SEFAZ!');

      vXml := Copy(vMensagem, Pos('<NFe', vMensagem), Pos('</NFe>', vMensagem) - Pos('<NFe', vMensagem) + 6);
      SalvarXML(vXml, FCaminhoXMLs + FDocEletronico.chave_acesso + '-nfe.xml');

      Result := TratarRetornoEnvioNFe(vRetWS);
      if Result.HouveErro then
        Clipboard.AsText := vXml;
    except
      on e: Exception do begin
        Result.HouveErro := True;
        Result.mensagem_erro := e.Message;
      end;
    end;
  finally
    vAcao.Free;
    vStream.Free;
    vStrStream.Free;
  end;
end;

function TServicosNFE.ConsultarLoteNFE(const pChaveNFe: string): RecRespostaNFE;
var
  vXml: string;
  vRetWS: string;
  mensagem: string;
  vRetornoWS: string;
  vAcao : TStringList;
  vStream: TMemoryStream;
  vStrStream: TStringStream;

  procedure AdicionarNoXML(pAdicao: string);
  begin
    vXml := vXml + pAdicao;
  end;

begin
  vXml := '';
  Result.HouveErro := False;
  vAcao := TStringList.Create;
  vStream := TMemoryStream.Create;
  vStrStream := TStringStream.Create('');
  try
    try
      mensagem :=
        '<?xml version="1.0" encoding="utf-8"?>'                                   +
          '<consSitNFe xmlns="http://www.portalfiscal.inf.br/nfe" versao="4.00">'  +
            '<tpAmb>' + GetCodigoAmbiente(FAmbienteNFE) + '</tpAmb>'               +
            '<xServ>CONSULTAR</xServ>'                                             +
            '<chNFe>' + pChaveNFe + '</chNFe>'                               +
          '</consSitNFe>';

      SalvarXML(mensagem, FCaminhoXMLs + pChaveNFe + '-ped-rec.xml');
 //     _BibliotecaNFE.ValidarXML(mensagem, FCaminhoSchemas, tsNfeRetAutorizacao);

      FURL := _BibliotecaNFE.getURLWebService(FUFEmitente, FAmbienteNFE, tsConsultaSituacao);
      mensagem := StringReplace(mensagem, '<?xml version="1.0" encoding="utf-8"?>', '', [rfReplaceAll]);

      AdicionarNoXML('<?xml version="1.0" encoding="utf-8"?>'                                                                                                                                       );
      AdicionarNoXML(  '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">');
      AdicionarNoXML(    '<soap12:Header>'                                                                                                                                                          );
      AdicionarNoXML(     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NFeConsultaProtocolo4">'                                                                                           );
      AdicionarNoXML(       '<cUF>' + IntToStr(FCodigoUFIBGE) + '</cUF>'                                                                                                                            );
      AdicionarNoXML(       '<versaoDados>4.00</versaoDados>'                                                                                                                                       );
      AdicionarNoXML(     '</nfeCabecMsg>'                                                                                                                                                          );
      AdicionarNoXML(   '</soap12:Header>'                                                                                                                                                          );
      AdicionarNoXML(   '<soap12:Body>'                                                                                                                                                             );
      AdicionarNoXML(     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NFeConsultaProtocolo4">'                                                                                           );
      AdicionarNoXML(      mensagem                                                                                                                                                                 );
      AdicionarNoXML(     '</nfeDadosMsg>'                                                                                                                                                          );
      AdicionarNoXML(   '</soap12:Body>'                                                                                                                                                            );
      AdicionarNoXML(  '</soap12:Envelope>'                                                                                                                                                         );

      if not _Biblioteca.TemConexaoInternet then begin
        Result.mensagem_erro := 'Sem conex�o com a internet, por favor, verifique!';
        Result.HouveErro := True;
        Exit;
      end;

      FConexao.URL := FURL + '?wsdl';
      FConexao.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/NFeConsultaProtocolo4';

      FConexao.Execute(vXml, vStream);
      vStrStream.CopyFrom(vStream, 0);

      vRetornoWS := RetirarAcentos( string(ParseText(AnsiString(vStrStream.DataString), True, True)) );
      vRetWS := string(SepararDados(AnsiString(vRetornoWS), 'retConsSitNFe', True));
      if vRetWS = '' then
        raise Exception.Create('Nenhum dado retornado do WebService da SEFAZ!');

      Result := TratarRetornoEnvioNFe(vRetWS);
    except
      on e: Exception do begin
        Result.HouveErro := True;
        Result.mensagem_erro := e.Message;
      end;
    end;
  finally
    vAcao.Free;
    vStream.Free;
    vStrStream.Free;
  end;
end;

function TServicosNFE.enviarEvento(
  const pLote: Integer;
  const pCodigoEvento: string;
  const pChaveNFe: string;
  const pSequencia: Integer;
  const pCNPJ: string;
  const pDetalheEvento: string;
  const pDataHora: TDateTime;
  const pCodigoUFAuxiliar: string = ''
): RecRespostaNFE;
const
  cVersao = '4.00';
  cXMLns = 'http://www.portalfiscal.inf.br/nfe/wsdl/NFeRecepcaoEvento4';
var
  vXml: string;
  vRetWS: string;
  vMensagem: string;
  vRetornoWS: string;
  vAcao : TStringList;
  vStream: TMemoryStream;
  vStrStream: TStringStream;

  vId: string;

  vDataHora: string;

  procedure AdicionarNoXML(pAdicao: string);
  begin
    vXml := vXml + pAdicao;
  end;

begin
  vXml := '';
  Result.HouveErro := False;
  vAcao := TStringList.Create;
  vStream := TMemoryStream.Create;
  vStrStream := TStringStream.Create('');
  try
    try
      vId := 'ID' + pCodigoEvento + pChaveNFe + LPad(IntToStr(pSequencia), 2, '0');

      vDataHora := FormatDateTime('yyyy-mm-dd"T"hh:nn:ss', pDataHora) + '-03:00';

      vMensagem :=
        '<evento xmlns="http://www.portalfiscal.inf.br/nfe" versao="1.00">'         +
          '<infEvento Id="' + vId + '">'                                            +
            '<cOrgao>' +  IIfStr(pCodigoUFAuxiliar <> '', pCodigoUFAuxiliar, IntToStr(FCodigoUFIBGE)) + '</cOrgao>' +
            '<tpAmb>' + GetCodigoAmbiente(FAmbienteNFE) + '</tpAmb>'                +
            '<CNPJ>' + _Biblioteca.RetornaNumeros(pCNPJ) + '</CNPJ>'                +
            '<chNFe>' + pChaveNFe + '</chNFe>'                                      +
            '<dhEvento>' + vDataHora +'</dhEvento>'                                 +
            '<tpEvento>' + pCodigoEvento + '</tpEvento>'                            +
            '<nSeqEvento>' + IntToStr(pSequencia) + '</nSeqEvento>'   +
            '<verEvento>1.00</verEvento>'                                           +
            '<detEvento versao="1.00">'                                             +
              pDetalheEvento                                                        +
            '</detEvento>'                                                          +
          '</infEvento>'                                                            +
        '</evento>';

      SalvarXML(vMensagem, FCaminhoXMLs + pChaveNFe + 'even.xml');
      vMensagem := _BibliotecaNFE.AssinarMsXML(vMensagem, FCertificado.GetCertificadoCapicom);

      vMensagem :=
        '<?xml version="1.0" encoding="utf-8"?>'                                 +
          '<envEvento xmlns="http://www.portalfiscal.inf.br/nfe" versao="1.00">' +
            '<idLote>' + IntToStr(pLote) + '</idLote>'                           +
            StringReplace(vMensagem, '<?xml version="1.0"?>', '', [rfReplaceAll])+
          '</envEvento>';

      //s_BibliotecaNFE.ValidarXML(vMensagem, FCaminhoSchemas, tsNfeEventoCanc);

      FURL := _BibliotecaNFE.getURLWebService( IIfStr(pCodigoUFAuxiliar <> '', pCodigoUFAuxiliar, FUFEmitente), FAmbienteNFE, tsNfeEvento );
      vMensagem := StringReplace(vMensagem, '<?xml version="1.0" encoding="utf-8"?>', '', [rfReplaceAll]);

      AdicionarNoXML('<?xml version="1.0" encoding="utf-8"?>'                                                                                                                                       );
      AdicionarNoXML(  '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">');
      AdicionarNoXML(    '<soap12:Header>'                                                                                                                                                          );
      AdicionarNoXML(     '<nfeCabecMsg xmlns="' + cXMLns + '">'                                                                                                                                    );
      AdicionarNoXML(       '<cUF>' + IntToStr(FCodigoUFIBGE) + '</cUF>'                                                                                                                            );
      AdicionarNoXML(       '<versaoDados>' + cVersao + '</versaoDados>'                                                                                                                            );
      AdicionarNoXML(     '</nfeCabecMsg>'                                                                                                                                                          );
      AdicionarNoXML(   '</soap12:Header>'                                                                                                                                                          );
      AdicionarNoXML(   '<soap12:Body>'                                                                                                                                                             );
      AdicionarNoXML(     '<nfeDadosMsg xmlns="' + cXMLns + '">'                                                                                                                                    );
      AdicionarNoXML(      vMensagem                                                                                                                                                                );
      AdicionarNoXML(     '</nfeDadosMsg>'                                                                                                                                                          );
      AdicionarNoXML(   '</soap12:Body>'                                                                                                                                                            );
      AdicionarNoXML(  '</soap12:Envelope>'                                                                                                                                                         );

      if not _Biblioteca.TemConexaoInternet then begin
        Result.mensagem_erro := 'Sem conex�o com a internet, por favor, verifique!';
        Result.HouveErro := True;
        Exit;
      end;

      FConexao.URL := FURL + '?wsdl';
      FConexao.SoapAction := cXMLns;

      FConexao.Execute(vXml, vStream);
      vStrStream.CopyFrom(vStream, 0);

      vRetornoWS := RetirarAcentos( string(ParseText(AnsiString(vStrStream.DataString), True, True)) );
      vRetWS := string(SepararDados(AnsiString(vRetornoWS), 'nfeResultMsg'));
      if vRetWS = '' then begin
        vRetWS := string(SepararDados(AnsiString(vRetornoWS), 'nfeRecepcaoEventoNFResult'));
        if vRetWS = '' then
          raise Exception.Create('Nenhum dado retornado do Web Service da SEFAZ!');
      end;

      FLeitorXML.Arquivo := vRetWS;
      FLeitorXML.Grupo := FLeitorXML.Arquivo;
      if FLeitorXML.rExtrai(1, 'retEnvEvento') <> '' then begin
        Result.tipo_ambiente := IfThen(FAmbienteNFE = tpProducao, 'Produ��o', 'Homologa��o');
        Result.versao_aplicacao := FLeitorXML.rCampo(tcStr, 'verAplic');
        Result.status := FLeitorXML.rCampo(tcInt, 'cStat');
        Result.motivo := FLeitorXML.rCampo(tcStr, 'xMotivo');
        Result.uf     := FLeitorXML.rCampo(tcStr, 'cUF');
        Result.HouveErro := Result.status <> '128';

        if Result.HouveErro then
          Result.mensagem_erro := FLeitorXML.rCampo(tcStr, 'xMotivo');

        if FLeitorXML.rExtrai(1, 'retEvento') <> '' then begin
          Result.protocolo.data_hora_recibo := FLeitorXML.rCampo(tcDatHor, 'dhRegEvento');
          Result.protocolo.numero_protocolo := FLeitorXML.rCampo(tcStr, 'nProt');
//          Result.protocolo.motivo           := pJustificativa;
          Result.status                     := FLeitorXML.rCampo(tcStr, 'cStat');
          Result.HouveErro                  := Result.status <> '135';

          if Result.HouveErro then begin
            Result.mensagem_erro := FLeitorXML.rCampo(tcStr, 'xMotivo');
            Exit;
          end;

          vXml := vRetWS;

          SalvarXML(vXml, FCaminhoXMLs + 'even-' + vId + '-evento.xml');

          Result.caminho_xml_nfe := FCaminhoXMLs + 'even-' + vId + '-evento.xml';
          Result.textoXML := vXml;
        end;
      end;
    except
      on e: Exception do begin
        Result.HouveErro := True;
        Result.mensagem_erro := e.Message;
      end;
    end;
  finally
    vAcao.Free;
    vStream.Free;
    vStrStream.Free;
  end;
end;

function TServicosNFE.CancelarNFe(
  const pLote: Integer;
  const pChaveNFe: string;
  const pCNPJ: string;
  const pDataHora: TDateTime;
  const pProtocolo: string;
  const pJustificativa: string
): RecRespostaNFE;
var
  vXml: string;
begin
  vXml :=
    '<descEvento>Cancelamento</descEvento>'                               +
    '<nProt>' + pProtocolo + '</nProt>'                                   +
    '<xJust>' + pJustificativa + '</xJust>';

  Result :=
    enviarEvento(
      pLote,
      '110111', // Cancelamento
      pChaveNFe,
      1,
      pCNPJ,
      vXml,
      pDataHora
    );
end;

function TServicosNFE.InutilizarNumeroNFe(
  const pCNPJ: string;
  const pAno: Integer;
  const pSerie: Integer;
  const pNumeracaoInicial: Integer;
  const pNumeracaoFinal: Integer;
  const pJustificativa: string;
  const pModeloNota: string
): RecRespostaNFE;
var
  vXml: string;
  retWS: string;
  vMensagem: string;
  retornoWS: string;
  Acao : TStringList;
  Stream: TMemoryStream;
  StrStream: TStringStream;

  vAnoStr: string;
  vNomeArquivo: string;

  procedure AdicionarNoXML(pAdicao: string);
  begin
    vXml := vXml + pAdicao;
  end;

begin
  vXml := '';
  Result.HouveErro := False;
  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;
  StrStream := TStringStream.Create('');
  try
    try
      vAnoStr := Copy(IntToStr(pAno), 3, 2);

      // Gerando o Id e j� aproveitando para montar o nome do arquivo xml que ser� salvo
      vNomeArquivo :=
        IntToStr(FCodigoUFIBGE) +
        vAnoStr +
        pCNPJ +
        pModeloNota +
        _Biblioteca.LPad(IntToStr(pSerie), 3, '0') +
        _Biblioteca.LPad(IntToStr(pNumeracaoInicial), 9, '0') +
        _Biblioteca.LPad(IntToStr(pNumeracaoFinal), 9, '0');

      vMensagem :=
        '<inutNFe xmlns="http://www.portalfiscal.inf.br/nfe" versao="4.00">' +
          '<infInut Id="' + 'ID' + vNomeArquivo + '">' +
            '<tpAmb>' + GetCodigoAmbiente(FAmbienteNFE) + '</tpAmb>' +
            '<xServ>INUTILIZAR</xServ>' +
            '<cUF>' + IntToStr(FCodigoUFIBGE) + '</cUF>' +
            '<ano>' + vAnoStr + '</ano>' +
            '<CNPJ>' + pCNPJ + '</CNPJ>' +
            '<mod>' + pModeloNota + '</mod>' +
            '<serie>' + IntToStr(pSerie) + '</serie>' +
            '<nNFIni>' + IntToStr(pNumeracaoInicial) + '</nNFIni>' +
            '<nNFFin>' + IntToStr(pNumeracaoFinal) + '</nNFFin>' +
            '<xJust>' + pJustificativa + '</xJust>' +
          '</infInut>' +
        '</inutNFe>';

      SalvarXML(vMensagem, FCaminhoXMLs + vNomeArquivo + '-inu.xml');
      vMensagem := _BibliotecaNFE.AssinarMsXML(vMensagem, FCertificado.GetCertificadoCapicom);
//      _BibliotecaNFE.ValidarXML(vMensagem, FCaminhoSchemas, tsNfeInutilizacao);

      FURL := _BibliotecaNFE.getURLWebService(FUFEmitente, FAmbienteNFE, tsNfeInutilizacao);
      vMensagem := StringReplace(vMensagem, '<?xml version="1.0"?>', '', [rfReplaceAll]);

      AdicionarNoXML('<?xml version="1.0" encoding="utf-8"?>'                                                                                                                                       );
      AdicionarNoXML(  '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">');
      AdicionarNoXML(    '<soap12:Header>'                                                                                                                                                          );
      AdicionarNoXML(     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NfeInutilizacao4">'                                                                                          );
      AdicionarNoXML(       '<cUF>' + IntToStr(FCodigoUFIBGE) + '</cUF>'                                                                                                                            );
      AdicionarNoXML(       '<versaoDados>4.00</versaoDados>'                                                                                                                                       );
      AdicionarNoXML(     '</nfeCabecMsg>'                                                                                                                                                          );
      AdicionarNoXML(   '</soap12:Header>'                                                                                                                                                          );
      AdicionarNoXML(   '<soap12:Body>'                                                                                                                                                             );
      AdicionarNoXML(     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/RecepcaoEvento">'                                                                                            );
      AdicionarNoXML(      vMensagem                                                                                                                                                                );
      AdicionarNoXML(     '</nfeDadosMsg>'                                                                                                                                                          );
      AdicionarNoXML(   '</soap12:Body>'                                                                                                                                                            );
      AdicionarNoXML(  '</soap12:Envelope>'                                                                                                                                                         );

      if not _Biblioteca.TemConexaoInternet then begin
        Result.mensagem_erro := 'Sem conex�o com a internet, por favor, verifique!';
        Result.HouveErro := True;
        Exit;
      end;

      FConexao.URL := FURL + '?wsdl';
      FConexao.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/NfeInutilizacao4';

      FConexao.Execute(vXml, Stream);
      StrStream.CopyFrom(Stream, 0);

      retornoWS := RetirarAcentos( string(ParseText(AnsiString(StrStream.DataString), True, True)) );
      retWS := string(SepararDados(AnsiString(retornoWS), 'nfeResultMsg'));
      if retWS = '' then
        raise Exception.Create('Nenhum dado retornado da SEFAZ!');
        
      FLeitorXML.Arquivo := retWS;
      FLeitorXML.Grupo := FLeitorXML.Arquivo;
      if (FLeitorXML.rExtrai(1, 'retInutNFe') <> '') or (FLeitorXML.rExtrai(1, 'infInut') <> '') then begin
        Result.tipo_ambiente    := IfThen(FAmbienteNFE = tpProducao, 'Produ��o', 'Homologa��o');
        Result.versao_aplicacao := FLeitorXML.rCampo(tcStr, 'verAplic');
        Result.status           := FLeitorXML.rCampo(tcInt, 'cStat');
        Result.motivo           := FLeitorXML.rCampo(tcStr, 'xMotivo');
        Result.uf               := FLeitorXML.rCampo(tcStr, 'cUF');
        Result.HouveErro        := Result.status <> '102';
        Result.mensagem_erro    := Result.motivo;
      end;
    except
      on e: Exception do begin
        Result.HouveErro := True;
        Result.mensagem_erro := e.Message;
      end;
    end;
  finally
    Acao.Free;
    Stream.Free;
    StrStream.Free;
  end;
end;

function TServicosNFE.enviarCartaCorrecao(
  const pLote: Integer;
  const pChaveNFe: string;
  const pCNPJ: string;
  const pDataHora: TDateTime;
  const pSequencia: Integer;
  const pTextoCorrecao: string
): RecRespostaNFE;
var
  vXml: string;
  vCondicaoUso: string;
begin
  vCondicaoUso :=
    _Biblioteca.RemoverCaracteresEspeciais(
      'A Carta de Correcao e disciplinada pelo paragrafo 1o-A do art. 7o do Convenio S/N, de 15 de dezembro de 1970 e pode ser utilizada para regularizacao de ' +
      'erro ocorrido na emissao de documento fiscal, desde que o erro nao esteja relacionado com: I - as variaveis que determinam o valor do imposto tais como: ' +
      'base de calculo, aliquota, diferenca de preco, quantidade, valor da operacao ou da prestacao; II - a correcao de dados cadastrais que implique mudanca do ' +
      'remetente ou do destinatario; III - a data de emissao ou de saida.'
    );

  vXml :=
    '<descEvento>Carta de Correcao</descEvento>' +
    '<xCorrecao>' + _BibliotecaNFE.trataTagTextoNFe(pTextoCorrecao) + '</xCorrecao>' +
    '<xCondUso>' + vCondicaoUso + '</xCondUso>';

  Result :=
    enviarEvento(
      pLote,
      '110110',
      pChaveNFe,
      pSequencia,
      pCNPJ,
      vXml,
      pDataHora
    );
end;

function TServicosNFE.manifestarOperacao(
  const pTipoManifestacao: TTipoManifestacao;
  const pLote: Integer;
  const pChaveNFe: string;
  const pCNPJ: string;
  const pDataHora: TDateTime;
  const pJustificativa: string  
): RecRespostaNFE;
var
  vXml: string;
  vCodigoEvento: string;
begin
  if pTipoManifestacao = ttCienciaOperacao then begin
    vCodigoEvento := '210210';
    vXml := '<descEvento>Ciencia da Operacao</descEvento>';
  end
  else if pTipoManifestacao = ttConfirmacaOperacao then begin
    vCodigoEvento := '210200';
    vXml := '<descEvento>Confirmacao da Operacao</descEvento>';
  end
  else if pTipoManifestacao = ttOperacaoNaoRealizada then begin
    vCodigoEvento := '210240';
    vXml := '<descEvento>Operacao nao Realizada</descEvento>';
    vXml := vXml + '<xJust>' + pJustificativa + '</xJust>';
  end
  else if pTipoManifestacao = ttDesconhecimentoOperacao then begin
    vCodigoEvento := '210220';
    vXml := '<descEvento>Desconhecimento da Operacao</descEvento>';
  end;

  Result :=
    enviarEvento(
      pLote,
      vCodigoEvento,
      pChaveNFe,
      1,
      pCNPJ,
      vXml,
      pDataHora,
      '91'
    );
end;

procedure TServicosNFE.OnAntesPost(const pHTTP: THTTPReqResp; pData: Pointer);
var
  vCert: ICertificate2;
  vCertContext: ICertContext;
  vPCertContext: Pointer;
  vContentHeader: string;
begin
  vCert := FCertificado.GetCertificadoCapicom;

  vCertContext :=  vCert as ICertContext;
  vCertContext.Get_CertContext(Integer(vPCertContext));

  if not InternetSetOption(pData, INTERNET_OPTION_CLIENT_CERT_CONTEXT, vPCertContext, SizeOf(CERT_CONTEXT)) then
    raise Exception.Create( 'Erro OnBeforePost: ' + IntToStr(GetLastError) );

  if Trim(FConexao.UserName) <> '' then begin
    if not InternetSetOption(pData, INTERNET_OPTION_PROXY_USERNAME, PChar(FConexao.UserName), Length(FConexao.UserName)) then
      raise Exception.Create( 'Erro OnBeforePost: ' + IntToStr(GetLastError) );
  end;

  if Trim(FConexao.UserName) <> '' then begin
    if not InternetSetOption(pData, INTERNET_OPTION_PROXY_PASSWORD, PChar(FConexao.Password), Length(FConexao.Password)) then
      raise Exception.Create( 'Erro OnBeforePost: ' + IntToStr(GetLastError) );
  end;

  if
    (pos('SCERECEPCAORFB',UpperCase(FURL)) <= 0) and
    (pos('SCECONSULTARFB',UpperCase(FURL)) <= 0)
  then begin
    vContentHeader := Format(ContentTypeTemplate, ['application/soap+xml; charset=utf-8']);
    HttpAddRequestHeaders(pData, PChar(vContentHeader), Length(vContentHeader), HTTP_ADDREQ_FLAG_REPLACE);
  end;
  FConexao.CheckContentType;
end;

function TServicosNFE.getXMLNFE(): string;
var
  i: Integer;
  xml: string;
  vUrlNFCe: string;
  vAuxiliar: string;
  vDigestValue: string;
  cfopDevolucao: Boolean;
  cfopProducao: Boolean;

  oXML: TXMLNFe;

  procedure Add(pAdicao: string; pAdicionar: Boolean = true);
  begin
    if pAdicionar then
      xml := xml + pAdicao;
  end;

begin
  xml := '';
  oXML := TXMLNFe.Create;

  Add(  '<NFe xmlns="http://www.portalfiscal.inf.br/nfe">'                                                               );
  Add(    '<infNFe Id="' + FDocEletronico.chave_acesso + '" versao="4.00">'                                              );   // Chave de Acesso da NFE
  Add(      '<ide>'                                                                                                      );
  Add(        '<cUF>' + FDocEletronico.codigo_uf_ibge + '</cUF>'                                                         );   // C�digo IBGE do estado
  Add(        '<cNF>' + FDocEletronico.codigo_nota + '</cNF>'                                                            );   // C�digo aleat�rio para compor a chave da NFe
  Add(        '<natOp>' + FDocEletronico.natureza_operacao + '</natOp>'                                                  );   // Descri��o do CFOP principal
  Add(        '<mod>' + IfThen(FTipoDocumento = tpNFe, '55', '65') + '</mod>'                                            );   // Modelo 55 para NFe, 65 NFCe
  Add(        '<serie>' + FDocEletronico.serie_nota + '</serie>'                                                         );   // S�ria da NFe
  Add(        '<nNF>' + FDocEletronico.numero_nota + '</nNF>'                                                            );   // Numero da nota fiscal
  Add(        '<dhEmi>' + FDocEletronico.data_hora_emissao + '</dhEmi>'                                                  );
  Add(        '<dhSaiEnt >' + FDocEletronico.data_hora_emissao + '</dhSaiEnt>', FTipoDocumento = tpNFe                   );
  Add(        '<tpNF>' + FDocEletronico.tipo_nota + '</tpNF>'                                                            );   // 0 - Entrada, 1 - Sa�da

  Add(        '<idDest>' + FDocEletronico.IndiceDestinatario + '</idDest>'                                               );   // 1 - Interna, 2 - Interestadual, 3 - exterior

  Add(        '<cMunFG>' + FDocEletronico.codigo_municipio_ibge + '</cMunFG>'                                            );
  Add(        '<tpImp>' + IfThen(FTipoDocumento = tpNFe, FDocEletronico.tipo_impressao, '4') + '</tpImp>'                );   // Tipo de impress�o. 1 - Retrato, 2 - Paisagem
  Add(        '<tpEmis>' + FDocEletronico.tipo_emissao + '</tpEmis>'                                                     );   // Tipo de emiss�o. 1 - Normal, 2 - Contig�ncia FS, 3 - SCAN, 4 - DPEC, 5 - FS-DA
  Add(        '<cDV>' + FDocEletronico.digito_verificador + '</cDV>'                                                     );   // Digito Verificador
  Add(        '<tpAmb>' + GetCodigoAmbiente(FAmbienteNFE) + '</tpAmb>'                                                   );   // 1 - Producao; 2 - Homologa��o
  Add(        '<finNFe>' + FDocEletronico.FinalidadeNFe + '</finNFe>'                                                    );   // 1 - NFe Normal; 2 - Nfe Complementar; 3 - NFe ajuste; 4 - Dev. Mercadoria
  Add(        '<indFinal>' + IIfStr(FDocEletronico.inscricao_estadual_destinatario = 'ISENTO', '1', '0') + '</indFinal>' );   // 0 - Normal; 1 - Consumidor final

  Add(        '<indPres>1</indPres>'                                                                                     );   // 0 = N�o se aplica (por exemplo, Nota Fiscal complementar ou de ajuste); 1 = Opera��o presencial; 2 = Opera��o n�o presencial, pela Internet; 3 = Opera��o n�o presencial, Teleatendimento; 4 = NFC-e em opera��o com entrega a domic�lio; 5 = Opera��o presencial, fora do estabelecimento; 9 = Opera��o n�o presencial, outros
//  Add(        '<indPres>' + IIfStr((FTipoDocumento = tpNFCe) and (FDocEletronico.InscricaoEstEntrDestinat <> ''), '4', '1') + '</indPres>' );   // 0 = N�o se aplica (por exemplo, Nota Fiscal complementar ou de ajuste); 1 = Opera��o presencial; 2 = Opera��o n�o presencial, pela Internet; 3 = Opera��o n�o presencial, Teleatendimento; 4 = NFC-e em opera��o com entrega a domic�lio; 5 = Opera��o presencial, fora do estabelecimento; 9 = Opera��o n�o presencial, outros

  Add(        '<procEmi>0</procEmi>'                                                                                     );
  Add(        '<verProc>' + FDocEletronico.versao_sistema + '</verProc>'                                                 );

  if FDocEletronicoReferencias <> nil then begin
    for i := Low(FDocEletronicoReferencias) to High(FDocEletronicoReferencias) do begin
      Add(        '<NFref>'                                                                                              );
      Add(          '<refNFe>' + FDocEletronicoReferencias[i] + '</refNFe>'                                              );
      Add(        '</NFref>'                                                                                             );
    end;
  end;

  // #ALTERAR_ESTE_PONTO
  {
    Aqui vem a conting�ncia
  }

  Add(      '</ide>'                                                                                                                     );
  Add(      '<emit>'                                                                                                                     );
  Add(        '<CNPJ>' + FDocEletronico.cnpj_emitente + '</CNPJ>'                                                                        );
  Add(        '<xNome>' + FDocEletronico.razao_social_emitente + '</xNome>'                                                              );
  Add(        '<xFant>' + FDocEletronico.nome_fantasia + '</xFant>'                                                                      );
  Add(        '<enderEmit>'                                                                                                              );
  Add(          '<xLgr>' + FDocEletronico.logradouro_emitente + '</xLgr>'                                                                );
  Add(          '<nro>' + FDocEletronico.numero_emitente + '</nro>'                                                                      );
  Add(          '<xCpl>' + FDocEletronico.complemento_emitente + '</xCpl>'                                                               );
  Add(          '<xBairro>' + FDocEletronico.bairro_emitente + '</xBairro>'                                                              );
  Add(          '<cMun>' + FDocEletronico.codigo_municipio_ibge + '</cMun>'                                                              );
  Add(          '<xMun>' + FDocEletronico.municipio_emitente + '</xMun>'                                                                 );
  Add(          '<UF>' + FDocEletronico.uf_emitente + '</UF>'                                                                            );
  Add(          '<CEP>' + FDocEletronico.cep_emitente + '</CEP>'                                                                         );
  Add(          '<cPais>1058</cPais>'                                                                                                    );
  Add(          '<xPais>BRASIL</xPais>'                                                                                                  );
  Add(          '<fone>' + FDocEletronico.telefone_emitente + '</fone>'                                                                  );
  Add(        '</enderEmit>'                                                                                                             );
  Add(        '<IE>' + FDocEletronico.inscricao_estadual_emitente + '</IE>'                                                              );
//  Add(        '<IEST>''</IEST>'                                                                                                        );   // Inscri��o estadual do Substituto Tribut�rio
//  Add(        '<IM>' + FDocEletronico.inscricao_municipal_emitente + '</IM>'                                                           );   // Inscri��o Municipal
//  Add(        '<CNAE>' + FDocEletronico.cnae_emitente + '</CNAE>'                                                                      );   // Quanto a Inscri��o Municipal for informada
  Add(        '<CRT>' + FDocEletronico.codigo_regime_emitente + '</CRT>'                                                                 );   // 1 - Simples Nacional; 2 - SN excesso de sublime receita bruta; 3 - Regime normal
  Add(      '</emit>'                                                                                                                    );

  if not Em(_Biblioteca.RetornaNumeros( nvl(FDocEletronico.cpf_destinatario, FDocEletronico.cnpj_destinatario) ), ['', '11111111111']) then begin
    Add(      '<dest>'                                                                                                                      );

    if FDocEletronico.e_pessoa_fisica then begin
      Add(        '<CPF>' + FDocEletronico.cpf_destinatario + '</CPF>'                                                                      );
      Add(        '<xNome>' + FDocEletronico.nome_fantasia_destinatario + '</xNome>'                                                        );
    end
    else begin
      Add(        '<CNPJ>' + FDocEletronico.cnpj_destinatario + '</CNPJ>'                                                                   );
      Add(        '<xNome>' + FDocEletronico.razao_social_destinatario + '</xNome>'                                                         );
    end;

    Add(        '<enderDest>'                                                                                                               );
    Add(          '<xLgr>' + FDocEletronico.logradouro_destinatario + '</xLgr>'                                                             );
    Add(          '<nro>' + FDocEletronico.numero_destinatario + '</nro>'                                                                   );
    Add(          '<xCpl>' + FDocEletronico.complemento_destinatario + '</xCpl>'                                                            );
    Add(          '<xBairro>' + FDocEletronico.bairro_destinatario + '</xBairro>'                                                           );
    Add(          '<cMun>' + FDocEletronico.codigo_municipio_destinatario + '</cMun>'                                                       );
    Add(          '<xMun>' + FDocEletronico.municipio_destinatario + '</xMun>'                                                              );
    Add(          '<UF>' + FDocEletronico.uf_destinatario + '</UF>'                                                                         );
    Add(          '<CEP>' + FDocEletronico.cep_destinatario + '</CEP>'                                                                      );
    Add(          '<cPais>1058</cPais>'                                                                                                     );
    Add(          '<xPais>BRASIL</xPais>'                                                                                                   );

//    if FDocEletronico.telefone_destinatario <> '' then
//      Add(        '<fone>' + FDocEletronico.telefone_destinatario + '</fone>'                                                               );

    Add(        '</enderDest>'                                                                                                              );

    if (FDocEletronico.uf_emitente = 'GO') or (FDocEletronico.uf_emitente = 'MA') then begin
      if (FDocEletronico.TipoCliente = 'NC') or (FTipoDocumento = tpNFCe) or (FDocEletronico.cnpj_destinatario = '99999999000191') then
        Add(        '<indIEDest>9</indIEDest>'                                                                       )
      else if FDocEletronico.inscricao_estadual_destinatario = 'ISENTO' then
        Add(        '<indIEDest>2</indIEDest>'                                                                       )
      else begin
        Add(        '<indIEDest>1</indIEDest>'                                                                       );
        Add(        '<IE>' + _Biblioteca.RetornaNumeros(FDocEletronico.inscricao_estadual_destinatario) + '</IE>'    );
      end;
    end;

    if FDocEletronico.email_destinatario <> '' then
      Add(      '<email>' + FDocEletronico.email_destinatario +'</email>'                                                                 );

    Add(      '</dest>'                                                                                                                   );

    if FDocEletronico.InscricaoEstEntrDestinat <> '' then begin
      Add(    '<entrega>'                                                                                                                 );
      if FDocEletronico.e_pessoa_fisica then
        Add(        '<CPF>' + FDocEletronico.cpf_destinatario + '</CPF>'                                                                  )
      else
        Add(        '<CNPJ>' + FDocEletronico.cnpj_destinatario + '</CNPJ>'                                                               );

      Add(      '<xLgr>'  + FDocEletronico.LogradouroEntrDestinatario + '</xLgr>'                                                         );
      Add(      '<nro>'  + FDocEletronico.NumeroEntrDestinatario + '</nro>'                                                               );
      Add(      '<xCpl>'  + FDocEletronico.ComplementoEntrDestinatario + '</xCpl>'                                                        );
      Add(      '<xBairro>'  + FDocEletronico.NomeBairroEntrDestinatario + '</xBairro>'                                                   );
      Add(      '<cMun>'  + FDocEletronico.CodigoIbgeMunicEntrDest + '</cMun>'                                                            );
      Add(      '<xMun>'  + FDocEletronico.NomeCidadeEntrDestinatario + '</xMun>'                                                         );
      Add(      '<UF>'  + FDocEletronico.EstadoIdEntrDestinatario + '</UF>'                                                               );
      Add(    '</entrega>'                                                                                                                );
    end;
  end;

  for i := Low(FDocEletronicoItens) to High(FDocEletronicoItens) do begin
    Add(      '<det nItem="' + IntToStr(i + 1) + '">'                                                                                     );
    Add(        '<prod>'                                                                                                                  );
    Add(          '<cProd>' + FDocEletronicoItens[i].codigo_produto + '</cProd>'                                                                    );
    Add(           '<cEAN>SEM GTIN</cEAN>'                                                                        );
    Add(          '<xProd>' + FDocEletronicoItens[i].descricao_produto + '</xProd>'                                                                 );

    if FDocEletronicoItens[i].ncm = '' then
      raise Exception.Create('O produto ' + FDocEletronicoItens[i].descricao_produto + ' n�o teve seu NCM informado corretamente, por favor fa�a a corre��o!');

    Add(          '<NCM>' + FDocEletronicoItens[i].ncm + '</NCM>'                                                                                   );

    if (FDocEletronicoItens[i].cest <> '') then
      Add(          '<CEST>' + FDocEletronicoItens[i].cest + '</CEST>'                                                                              );

    Add(          '<CFOP>' + FDocEletronicoItens[i].cfop + '</CFOP>'                                                                                );
    Add(          '<uCom>' + FDocEletronicoItens[i].unidade_comercial + '</uCom>'                                                                   );
    Add(          '<qCom>' + FDocEletronicoItens[i].quantidade_comercial + '</qCom>'                                                                );
    Add(          '<vUnCom>' + FDocEletronicoItens[i].valor_unitario_comercial + '</vUnCom>'                                                        );
    Add(          '<vProd>' + FDocEletronicoItens[i].valor_bruto + '</vProd>'                                                                       );
    Add(           '<cEANTrib>SEM GTIN</cEANTrib>'                                                               );
    Add(          '<uTrib>' + FDocEletronicoItens[i].unidade_tributavel + '</uTrib>'                                                                );
    Add(          '<qTrib>' + FDocEletronicoItens[i].quantidade_tributavel + '</qTrib>'                                                             );
    Add(          '<vUnTrib>' + FDocEletronicoItens[i].valor_unitario_tributavel + '</vUnTrib>'                                                     );
    //Add(          '<vFrete>' + FDocEletronicoItens. + '</vFrete>'                                                                                 );
    //Add(          '<vSeg>' + FDocEletronicoItens. + '</vSeg>'                                                                                     );

    if FDocEletronicoItens[i].valor_desconto <> '0.00' then
      Add(          '<vDesc>' + FDocEletronicoItens[i].valor_desconto + '</vDesc>'                                                                  );
    if FDocEletronicoItens[i].valor_outras_despesas <> '0.00' then
      Add(          '<vOutro>' + FDocEletronicoItens[i].valor_outras_despesas + '</vOutro>'                                                         );

    Add(          '<indTot>1</indTot>'                                                                                                    );   // 0 - O valor do item n�o comp�es o valor da NFe; 1 - O valor do item comp�e o valor total da NFe

    if FDocEletronicoItens[i].numero_pedido <> '' then
      Add(           '<xPed>' + FDocEletronicoItens[i].numero_pedido + '</xPed>' );


    if FDocEletronicoItens[i].numero_item_pedido <> '' then
      Add(          '<nItemPed>' + FDocEletronicoItens[i].numero_item_pedido + '</nItemPed>'                                                     );


    Add(        '</prod>'                                                                                                                 );
    Add(        '<imposto>'                                                                                                               );
//    Add(          '<vTotTrib>' + FDocEletronicoItens[i].total_tributos + '</vTotTrib>'                                                  );
    Add(          '<ICMS>'                                                                                                                );

    cfopDevolucao := (FDocEletronicoItens[i].cfop =  '6202') or  (FDocEletronicoItens[i].cfop =  '5202')
    or (FDocEletronicoItens[i].cfop =  '6949') or (FDocEletronicoItens[i].cfop =  '5949')
    or (FDocEletronicoItens[i].cfop =  '6411') or (FDocEletronicoItens[i].cfop =  '5411');

    cfopProducao := (FDocEletronicoItens[i].cfop =  '1151') or  (FDocEletronicoItens[i].cfop =  '5151');

    if FDocEletronico.codigo_regime_emitente = '1' then begin
      if cfopDevolucao then
        Add(            '<ICMSSN900>'                                                                        )
      else if FDocEletronicoItens[i].cst = '400' then
        Add(            '<ICMSSN102>'                                                                        )
      else
        Add(            '<ICMSSN' + FDocEletronicoItens[i].cst + '>'                                                                        );

      Add(              '<orig>' + FDocEletronicoItens[i].origem_produto + '</orig>'                                                      );

      if cfopDevolucao then
        Add(              '<CSOSN>900</CSOSN>'                                                               )
      else if FDocEletronicoItens[i].cst = '400' then
        Add('<CSOSN>400</CSOSN>')
      else
        Add(              '<CSOSN>' + FDocEletronicoItens[i].cst + '</CSOSN>'                                );

      if cfopDevolucao then begin
        Add(              '<modBC>' + FDocEletronicoItens[i].modalidade_bc_icms + '</modBC>'                        );
        Add(              '<vBC>' + FDocEletronicoItens[i].valor_base_calculo_icms +  '</vBC>'                      );
        Add(              '<pICMS>' + FDocEletronicoItens[i].percentual_aliquota_icms + '</pICMS>'                  );
        Add(              '<vICMS>' + FDocEletronicoItens[i].valor_icms + '</vICMS>'                                );
      end
      else if FDocEletronicoItens[i].cst = '101' then begin      //Ezequiel analisar regra de dev. do simples nacional

       // Add(              '<modBC>' + FDocEletronicoItens[i].modalidade_bc_icms + '</modBC>'                                              );
       // Add(              '<vBC>' + FDocEletronicoItens[i].valor_base_calculo_icms + '</vBC>'                                             );
        Add(              '<pCredSN>' + FDocEletronicoItens[i].percentual_aliquota_icms + '</pCredSN>'                                    );
        Add(              '<vCredICMSSN>' + FDocEletronicoItens[i].valor_icms + '</vCredICMSSN>'                                          );
      end
      else if FDocEletronicoItens[i].cst = '102' then begin
        // N�o preenche tag alguma
      end
      else if FDocEletronicoItens[i].cst = '201' then begin
        Add(              '<modBCST>' + FDocEletronicoItens[i].modalidade_bc_icms + '</modBCST>'                    );
        Add(              '<pMVAST>' + FDocEletronicoItens[i].percentual_margem_icms_st + '</pMVAST>'               );
        Add(              '<pRedBCST>' + FDocEletronicoItens[i].percentual_reducao_bc_st + '</pRedBCST>'            );
        Add(              '<vBCST>' + FDocEletronicoItens[i].valor_base_calculo_st + '</vBCST>'                     );
        Add(              '<pICMSST>' + FDocEletronicoItens[i].percentual_aliquota_icms_st + '</pICMSST>'           );
        Add(              '<vICMSST>' + FDocEletronicoItens[i].valor_icms_st + '</vICMSST>'                         );
        Add(              '<pCredSN>0.00</pCredSN>'                                                                 );
        Add(              '<vCredICMSSN>0.00</vCredICMSSN>'                                                         );
      end
      else if FDocEletronicoItens[i].cst = '500' then begin
        Add(              '<vBCSTRet>0.00</vBCSTRet>'                                                               );
        Add(              '<pST>0.00</pST>'                                                                         );
        Add(              '<vICMSSTRet>0.00</vICMSSTRet>'                                                           );
      end
      else if (FDocEletronicoItens[i].cst = '900') and (FDocEletronicoItens[i].valor_icms <> '0.00') then begin
        Add(              '<modBC>' + FDocEletronicoItens[i].modalidade_bc_icms + '</modBC>'                        );
        Add(              '<vBC>' + FDocEletronicoItens[i].valor_base_calculo_icms +  '</vBC>'                      );
        Add(              '<pICMS>' + FDocEletronicoItens[i].percentual_aliquota_icms + '</pICMS>'                  );
        Add(              '<vICMS>' + FDocEletronicoItens[i].valor_icms + '</vICMS>'                                );
      end;

      if cfopDevolucao then
        Add(            '</ICMSSN900>'                                                                                )
      else if FDocEletronicoItens[i].cst = '400' then
        Add(            '</ICMSSN102>'                                                 )
      else
        Add(            '</ICMSSN' + FDocEletronicoItens[i].cst + '>'                                                 );

    end
    else begin
      Add(            '<ICMS' + FDocEletronicoItens[i].cst + '>'                                                                                      );
      Add(              '<orig>' + FDocEletronicoItens[i].origem_produto + '</orig>'                                                                  );
      Add(              '<CST>' + FDocEletronicoItens[i].cst + '</CST>'                                                                               );

      if FDocEletronicoItens[i].cst = '00' then begin
        Add(              '<modBC>' + FDocEletronicoItens[i].modalidade_bc_icms + '</modBC>'                                                          );   // 0 - Margem Valor Agregado (%); 1 - Pauta (Valor); 2 - Pre�o Tabelado M�x. (valor); 3 - valor da opera��o.
        Add(              '<vBC>' + FDocEletronicoItens[i].valor_base_calculo_icms+ '</vBC>'                                                          );
        Add(              '<pICMS>' + FDocEletronicoItens[i].percentual_aliquota_icms + '</pICMS>'                                                    );
        Add(              '<vICMS>' + FDocEletronicoItens[i].valor_icms + '</vICMS>'                                                                  );
      end
      else if Em(FDocEletronicoItens[i].cst, ['10']) then begin
        Add(              '<modBC>' + FDocEletronicoItens[i].modalidade_bc_icms + '</modBC>'                                                          );   // 0 - Margem Valor Agregado (%); 1 - Pauta (Valor); 2 - Pre�o Tabelado M�x. (valor); 3 - valor da opera��o.
        Add(              '<vBC>' + FDocEletronicoItens[i].valor_base_calculo_icms+ '</vBC>'                                                          );
        Add(              '<pICMS>' + FDocEletronicoItens[i].percentual_aliquota_icms + '</pICMS>'                                                    );
        Add(              '<vICMS>' + FDocEletronicoItens[i].valor_icms + '</vICMS>'                                                                  );
        Add(              '<modBCST>' + FDocEletronicoItens[i].modalidade_bc_icms + '</modBCST>'                                                      );
        Add(              '<pMVAST>' + FDocEletronicoItens[i].percentual_margem_icms_st + '</pMVAST>'                                                 );
        Add(              '<pRedBCST>' + FDocEletronicoItens[i].percentual_reducao_bc_st + '</pRedBCST>'                                              );
        Add(              '<vBCST>' + FDocEletronicoItens[i].valor_base_calculo_st + '</vBCST>'                                                       );
        Add(              '<pICMSST>' + FDocEletronicoItens[i].percentual_aliquota_icms_st + '</pICMSST>'                                             );
        Add(              '<vICMSST>' + FDocEletronicoItens[i].valor_icms_st + '</vICMSST>'                                                           );
      end
      else if FDocEletronicoItens[i].cst = '20' then begin
        Add(              '<modBC>' + FDocEletronicoItens[i].modalidade_bc_icms + '</modBC>'                                                          );   // 0 - Margem Valor Agregado (%); 1 - Pauta (Valor); 2 - Pre�o Tabelado M�x. (valor); 3 - valor da opera��o.
        Add(              '<pRedBC>' + FDocEletronicoItens[i].percentual_reducao_bc_icms + '</pRedBC>'                                                );
        Add(              '<vBC>' + FDocEletronicoItens[i].valor_base_calculo_icms+ '</vBC>'                                                          );
        Add(              '<pICMS>' + FDocEletronicoItens[i].percentual_aliquota_icms + '</pICMS>'                                                    );
        Add(              '<vICMS>' + FDocEletronicoItens[i].valor_icms + '</vICMS>'                                                                  );
      end
      else if _Biblioteca.Em(FDocEletronicoItens[i].cst, ['40', '41', '50']) then begin
//        Add(              '<vICMS>' + FDocEletronicoItens[i].valor_icms + '</vICMS>'                                                                );
//        Add(              '<motDesICMS>' + FDocEletronicoItens[i].motivo_desoneracao_icms + '</motDesICMS>'                                         );   // 1 � T�xi; 2 � Deficiente F�sico; 3 � Produtor Agropecu�rio; 4 � Frotista/Locadora; 5 � Diplom�tico/Consular; 6 � Utilit�rios e Motocicletas da Amaz�nia Ocidental e �reas de Livre Com�rcio (Resolu��o 714/88 e 790/94 � CONTRAN e suas altera��es); 7 � SUFRAMA; 9 � outros. (v2.0)                                                                                                     );
      end
      else if FDocEletronicoItens[i].cst = '60' then begin
        Add(            '<vBCSTRet>' + FDocEletronicoItens[i].base_st_retida_anteriormente + '</vBCSTRet>'                                            );
        Add(            '<pST>' + FDocEletronicoItens[i].percentual_aliquota_icms_st + '</pST>'                                                       );
        Add(            '<vICMSSTRet>' + FDocEletronicoItens[i].valor_icms_st_retida_anteriormente + '</vICMSSTRet>'                                  );
//        Add(            '<vBCFCPSTRet>0.00</vBCFCPSTRet>'                                                                                           );
//        Add(            '<pFCPSTRet>0.00</pFCPSTRet>'                                                                                               );
//        Add(            '<vFCPSTRet>0.00</vFCPSTRet>'                                                                                               );
      end
      else if FDocEletronicoItens[i].cst = '70' then begin
        Add(              '<pRedBC>' + FDocEletronicoItens[i].percentual_reducao_bc_icms + '</pRedBC>'                                                );
        Add(              '<vBC>' + FDocEletronicoItens[i].valor_base_calculo_icms+ '</vBC>'                                                          );
        Add(              '<pICMS>' + FDocEletronicoItens[i].percentual_aliquota_icms + '</pICMS>'                                                    );
        Add(              '<vICMS>' + FDocEletronicoItens[i].valor_icms + '</vICMS>'                                                                  );
        Add(              '<modBCST>' + FDocEletronicoItens[i].modalidade_bc_icms + '</modBCST>'                                                      );
        Add(              '<pMVAST>' + FDocEletronicoItens[i].percentual_margem_icms_st + '</pMVAST>'                                                 );
        Add(              '<pRedBCST>' + FDocEletronicoItens[i].percentual_reducao_bc_st + '</pRedBCST>'                                              );
        Add(              '<vBCST>' + FDocEletronicoItens[i].valor_base_calculo_st + '</vBCST>'                                                       );
        Add(              '<pICMSST>' + FDocEletronicoItens[i].percentual_aliquota_icms_st + '</pICMSST>'                                             );
        Add(              '<vICMSST>' + FDocEletronicoItens[i].valor_icms_st + '</vICMSST>'                                                           );
      end;

      Add(            '</ICMS' + FDocEletronicoItens[i].cst + '>'                                                                                     );
    end;

    Add(          '</ICMS>'                                                                                                               );

    if (cfopDevolucao) and (_Biblioteca.SFormatDouble(FDocEletronicoItens[i].valor_ipi) > 0) then begin

    Add(          '<IPI>'                                                                                                                 );
    Add(             '<cEnq>999</cEnq>'                                                                                                   );
    Add(             '<IPITrib>'                                                                                                          );
    Add(                '<CST>99</CST>'                                                                                                   );
    Add(                '<vBC>' + FDocEletronicoItens[i].valor_base_calculo_icms+ '</vBC>'                                                );
    Add(                '<pIPI>' + FDocEletronicoItens[i].percentual_aliquota_ipi + '</pIPI>'                                                           );
    Add(                '<vIPI>' + FDocEletronicoItens[i].valor_ipi + '</vIPI>'                                                           );
    Add(             '</IPITrib>'                                                                                                         );
    Add(          '</IPI>'                                                                                                                );
    end;

    Add(          '<PIS>'                                                                                                                 );
    if Em(FDocEletronicoItens[i].cst_pis, ['01', '02']) then begin
      Add(            '<PISAliq>'                                                                                                         );
      Add(              '<CST>' + FDocEletronicoItens[i].cst_pis + '</CST>'                                                                         );
      Add(              '<vBC>' + FDocEletronicoItens[i].BaseCalculoPis + '</vBC>'                                                                  );
      Add(              '<pPIS>' + FDocEletronicoItens[i].PercentualPis + '</pPIS>'                                                                 );
      Add(              '<vPIS>' + FDocEletronicoItens[i].ValorPis + '</vPIS>'                                                                      );
      Add(            '</PISAliq>'                                                                                                        );
    end
    else if FDocEletronicoItens[i].cst_pis = '03' then begin
      Add(            '<PISQtde>'                                                                                                         );
      Add(              '<CST>' + FDocEletronicoItens[i].cst_pis + '</CST>'                                                                         );
      Add(              '<qBCProd>' + FDocEletronicoItens[i].BaseCalculoPis + '</qBCProd>'                                                          );
      Add(              '<vAliqProd>' + FDocEletronicoItens[i].PercentualPis + '</vAliqProd>'                                                       );
      Add(              '<vPIS>' + FDocEletronicoItens[i].ValorPis + '</vPIS>'                                                                      );
      Add(            '</PISQtde>'                                                                                                        );
    end
    else if Em(FDocEletronicoItens[i].cst_pis, ['04', '06', '07', '08', '09']) then begin
      Add(            '<PISNT>'                                                                                                           );
      Add(              '<CST>' + FDocEletronicoItens[i].cst_pis + '</CST>'                                                                         );
      Add(            '</PISNT>'                                                                                                          );
    end
    else if Em(FDocEletronicoItens[i].cst_pis, ['49', '98', '99']) then begin
      Add(            '<PISOutr>'                                                                                                         );
      Add(              '<CST>' + FDocEletronicoItens[i].cst_pis + '</CST>'                                                                         );
      Add(              '<vBC>' + FDocEletronicoItens[i].BaseCalculoPis + '</vBC>'                                                                  );
      Add(              '<pPIS>' + FDocEletronicoItens[i].PercentualPis + '</pPIS>'                                                                 );
      Add(              '<vPIS>' + FDocEletronicoItens[i].ValorPis + '</vPIS>'                                                                      );
      Add(            '</PISOutr>'                                                                                                        );
    end;
    Add(          '</PIS>'                                                                                                                );

    Add(          '<COFINS>'                                                                                                              );
    if Em(FDocEletronicoItens[i].cst_cofins, ['01', '02']) then begin
      Add(            '<COFINSAliq>'                                                                                                      );
      Add(              '<CST>' + FDocEletronicoItens[i].cst_cofins + '</CST>'                                                                      );
      Add(              '<vBC>' + FDocEletronicoItens[i].BaseCalculoCofins + '</vBC>'                                                               );
      Add(              '<pCOFINS>' + FDocEletronicoItens[i].PercentualCofins + '</pCOFINS>'                                                        );
      Add(              '<vCOFINS>' + FDocEletronicoItens[i].ValorCofins + '</vCOFINS>'                                                             );
      Add(            '</COFINSAliq>'                                                                                                     );
    end
    else if FDocEletronicoItens[i].cst_cofins = '03' then begin
      Add(            '<COFINSQtde>'                                                                                                      );
      Add(              '<CST>' + FDocEletronicoItens[i].cst_cofins + '</CST>'                                                                      );
      Add(              '<qBCProd>' + FDocEletronicoItens[i].BaseCalculoCofins + '</qBCProd>'                                                       );
      Add(              '<vAliqProd>' + FDocEletronicoItens[i].PercentualCofins + '</vAliqProd>'                                                    );
      Add(              '<vCOFINS>' + FDocEletronicoItens[i].ValorCofins + '</vCOFINS>'                                                             );
      Add(            '</COFINSQtde>'                                                                                                     );
    end
    else if Em(FDocEletronicoItens[i].cst_cofins, ['04', '06', '07', '08', '09']) then begin
      Add(            '<COFINSNT>'                                                                                                        );
      Add(              '<CST>' + FDocEletronicoItens[i].cst_cofins + '</CST>'                                                                      );
      Add(            '</COFINSNT>'                                                                                                       );
    end
    else if Em(FDocEletronicoItens[i].cst_cofins, ['49', '98', '99']) then begin
      Add(            '<COFINSOutr>'                                                                                                      );
      Add(              '<CST>' + FDocEletronicoItens[i].cst_cofins + '</CST>'                                                                      );
      Add(              '<vBC>' + FDocEletronicoItens[i].BaseCalculoCofins + '</vBC>'                                                               );
      Add(              '<pCOFINS>' + FDocEletronicoItens[i].PercentualCofins + '</pCOFINS>'                                                        );
      Add(              '<vCOFINS>' + FDocEletronicoItens[i].ValorCofins + '</vCOFINS>'                                                             );
      Add(            '</COFINSOutr>'                                                                                                     );
    end;
    Add(          '</COFINS>'                                                                                                             );

    if FDocEletronicoItens[i].valor_icms_inter <> '' then begin
      Add(          '<ICMSUFDest>'                                                                          );
      Add(            '<vBCUFDest>' + FDocEletronicoItens[i].base_calculo_icms_inter + '</vBCUFDest>'       );
      Add(            '<pICMSUFDest>' + FDocEletronicoItens[i].percentual_icms_inter + '</pICMSUFDest>'     );
      Add(            '<pICMSInter>' + FDocEletronicoItens[i].percentual_aliquota_icms + '</pICMSInter>'    );
      Add(            '<pICMSInterPart>100.0000</pICMSInterPart>'                                           );
      Add(            '<vICMSUFDest>' + FDocEletronicoItens[i].valor_icms_inter + '</vICMSUFDest>'          );
      Add(            '<vICMSUFRemet>0.00</vICMSUFRemet>'                                                   );
      Add(          '</ICMSUFDest>'                                                                         );
    end;

    Add(        '</imposto>'                                                                                                              );
//    Add(        '<infAdProd>' + FDocEletronicoItens[i].informacoes_adicionais_produto + '</infAdProd>'                                              );
    Add(      '</det>'                                                                                                                    );
  end;

  Add(      '<total>'                                                                                                                     );
  Add(        '<ICMSTot>'                                                                                                                 );
  Add(          '<vBC>' + FDocEletronico.base_calculo_icms + '</vBC>'                                                                     );
  Add(          '<vICMS>' + FDocEletronico.valor_icms + '</vICMS>'                                                                        );
  Add(          '<vICMSDeson>0.00</vICMSDeson>'                                                                                           );
  Add(          '<vFCPUFDest>0.00</vFCPUFDest>'                                                                                           );
  Add(          '<vICMSUFDest>' + IIfStr(FDocEletronico.valor_icms_inter <> '', FDocEletronico.valor_icms_inter, '0.00') + '</vICMSUFDest>');
  Add(          '<vICMSUFRemet>0.00</vICMSUFRemet>'                                                                                       );
  Add(          '<vFCP>0.00</vFCP>'                                                                                                       );
  Add(          '<vBCST>' + FDocEletronico.base_calculo_st + '</vBCST>'                                                                   );
  Add(          '<vST>' + FDocEletronico.valor_st + '</vST>'                                                                              );
  Add(          '<vFCPST>0.00</vFCPST>'                                                                                                   );
  Add(          '<vFCPSTRet>0.00</vFCPSTRet>'                                                                                             );
  Add(          '<vProd>' + FDocEletronico.valor_total_produtos + '</vProd>'                                                              );
  Add(          '<vFrete>' + FDocEletronico.valor_total_frete + '</vFrete>'                                                               );
  Add(          '<vSeg>' + FDocEletronico.valor_total_seguro + '</vSeg>'                                                                  );
  Add(          '<vDesc>' + FDocEletronico.valor_total_desconto + '</vDesc>'                                                              );
  Add(          '<vII>0.00</vII>'                                                                                                         );
  Add(          '<vIPI>' + FDocEletronico.valor_total_ipi + '</vIPI>'                                                                     );
  Add(          '<vIPIDevol>0.00</vIPIDevol>'                                                                                             );
  Add(          '<vPIS>' + FDocEletronico.valor_total_pis + '</vPIS>'                                                                     );
  Add(          '<vCOFINS>' + FDocEletronico.valor_total_cofins + '</vCOFINS>'                                                            );
  Add(          '<vOutro>' + FDocEletronico.valor_total_outras_despesas + '</vOutro>'                                                     );
  Add(          '<vNF>' + FDocEletronico.valor_total_nota_fiscal + '</vNF>'                                                               );
 // Add(          '<vTotTrib>' + FDocEletronico.total_tributos + '</vTotTrib>'                                                              );
  Add(        '</ICMSTot>'                                                                                                                );
  Add(      '</total>'                                                                                                                    );
  Add(      '<transp>'                                                                                                                    );
  Add(        '<modFrete>' + FDocEletronico.tipo_frete +'</modFrete>'                                                                     );   // 0- Por conta do emitente; 1- Por conta do destinat�rio/remetente; 2- Por conta de terceiros; 9- Sem frete. (V2.0)

  if (FDocEletronico.transportadora_id > 0) and (FDocEletronico.tipo_frete <> '9') then begin
    Add(      '<transporta>'                                                        );
    Add(        '<CNPJ>' + FDocEletronico.cnpj_transportadora + '</CNPJ>'           );
    Add(        '<xNome>' + FDocEletronico.razao_transportadora + '</xNome>'        );
    Add(        '<IE>' + FDocEletronico.ie_transportadora + '</IE>'                 );
    Add(        '<xEnder>' + FDocEletronico.endereco_transportadora + '</xEnder>'   );
    Add(        '<xMun>' + FDocEletronico.cidade_transportadora + '</xMun>'         );
    Add(        '<UF>' + FDocEletronico.uf_transportadora + '</UF>'                 );
    Add(      '</transporta>'                                                       );
  end;

  Add(        '<vol>'                                                                                                                     );
  Add(          '<qVol>' + FDocEletronico.quantidade_nfe + '</qVol>'                                                                        );

  if FDocEletronico.especie_nfe <> '' then
    Add(          '<esp>' + FDocEletronico.especie_nfe + '</esp>'                                                                        );

  Add(          '<pesoL>' + FDocEletronico.peso_liquido_total + '</pesoL>'                                                                );
  Add(          '<pesoB>' + FDocEletronico.peso_bruto_total + '</pesoB>'                                                                  );
  Add(        '</vol>'                                                                                                                    );

  Add(      '</transp>'                                                                                                                   );

  // Se for nota de ajuste ou devolu��o de mercadorias
  Add(      '<pag>'                                                                    );
  if Em(FDocEletronico.FinalidadeNFe, ['3', '4']) or (cfopProducao) then
    Add(        oXML.getTipoPagamento('90', '0.00')                                    )
  else if FDocEletronico.sem_valor then
    Add(        oXML.getTipoPagamento('90', FDocEletronico.valor_total_nota_fiscal)    )
  else begin
    Add(        oXML.getTipoPagamento('01', FDocEletronico.valor_dinheiro)             );
    Add(        oXML.getTipoPagamento('02', FDocEletronico.valor_cheque)               );

    Add(        oXML.getTipoPagamentoCartao('03', FDocEletronico.valor_cartao_credito, FDocEletronicoCartoesCred) );
    Add(        oXML.getTipoPagamentoCartao('04', FDocEletronico.valor_cartao_debito, FDocEletronicoCartoesDeb) );

    Add(        oXML.getTipoPagamento('05', FDocEletronico.valor_credito)              );
    Add(        oXML.getTipoPagamento('99', FDocEletronico.valor_pix)                  );
    Add(        oXML.getTipoPagamento('99', FDocEletronico.valor_outros)               );
  end;
  Add(      '</pag>'                                                                   );

  Add(      '<infAdic>'                                                                                                                   );
  Add(        '<infCpl>' + FDocEletronico.informacoes_complementares + '</infCpl>'                                                        );
  Add(      '</infAdic>'                                                                                                                  );
  Add(    '</infNFe>'                                                                                                                     );
  Add(  '</NFe>'                                                                                                                          );

  xml := _BibliotecaNFE.AssinarMsXML(xml, FCertificado.GetCertificadoCapicom);

  if FTipoDocumento = tpNFCe then begin
    FLeitorXML.Arquivo := xml;
    FLeitorXML.Grupo := FLeitorXML.Arquivo;
    vDigestValue := FLeitorXML.rCampo(tcStr, 'DigestValue');

    vUrlNFCe := _BibliotecaNFE.GetURLQRCode(FDocEletronico.uf_emitente, FAmbienteNFE);

    vAuxiliar := _BibliotecaNFE.GetURLQRCode(
      FDocEletronico.uf_emitente,
      FAmbienteNFE,
      FDocEletronico.chave_acesso,
      FDocEletronico.cpf_destinatario,
      FDocEletronico.data_hora_emissao,
      FDocEletronico.valor_total_nota_fiscal,
      FDocEletronico.valor_icms,
      vDigestValue,
      FCSC,
      FIdToken
    );

    vAuxiliar :=
      '<infNFeSupl>' +
        '<qrCode>' +
          '<![CDATA[' + vAuxiliar + ']]>' +
        '</qrCode>' +
        '<urlChave>' + vUrlNFCe + '</urlChave>' +
      '</infNFeSupl>';

    Insert(vAuxiliar, xml, Pos('</infNFe>', xml) + 9);
  end;

  //SalvarXML(xml, 'f:\xml.xml');

  Result := StringReplace(xml, '<?xml version="1.0"?>', '', [rfReplaceAll]);
end;

procedure TServicosNFE.SalvarXML(pXML, pCaminho: string);
var
  arquivo: TStringList;
begin
  arquivo := TStringList.Create;

  arquivo.Add(pXML);
  arquivo.SaveToFile(pCaminho, TEncoding.UTF8);

  arquivo.Free;
end;

function TServicosNFE.getStatusServico: RecRespostaNFE;
var
  vXml: string;
  vRetWS: string;
  vMensagem: string;
  vRetornoWS: string;
  vAcao: TStringList;
  vStream: TMemoryStream;
  vStrStream: TStringStream;

  procedure AdicionarNoXML(pAdicao: string);
  begin
    vXml := vXml + pAdicao;
  end;

begin
  vAcao := TStringList.Create;
  vStream := TMemoryStream.Create;
  vStrStream := TStringStream.Create('');
  try
    try
      vXml := '';
      Result.HouveErro := False;

      vMensagem :=
        '<?xml version="1.0" encoding="utf-8"?>'                                    +
          '<consStatServ xmlns="http://www.portalfiscal.inf.br/nfe" versao="4.00">' +
            '<tpAmb>' + IfThen(FAmbienteNFE = tpProducao, '1', '2') + '</tpAmb>'   +
            '<cUF>' + IntToStr(FCodigoUFIBGE) + '</cUF>'                            +
            '<xServ>STATUS</xServ>'                                                 +
          '</consStatServ>';

      SalvarXML(vMensagem, FCaminhoXMLs + FormatDateTime('yyyymmddhhnnss', Now) + '-ped-sta.xml');
      //_BibliotecaNFE.ValidarXML(vMensagem, FCaminhoSchemas, tsNfeStatusServico);

      FURL := _BibliotecaNFE.getURLWebService(FUFEmitente, FAmbienteNFE, tsNfeStatusServico);
      vMensagem := StringReplace(vMensagem, '<?xml version="1.0" encoding="utf-8"?>', '', [rfReplaceAll]);

      AdicionarNoXML('<?xml version="1.0" encoding="utf-8"?>'                                                                                                                                         );
      AdicionarNoXML(  '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">'  );
      AdicionarNoXML(    '<soap12:Header>'                                                                                                                                                            );
      AdicionarNoXML(      '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NFeStatusServico4">'                                                                                          );
      AdicionarNoXML(        '<cUF>' + IntToStr(FCodigoUFIBGE) + '</cUF>'                                                                                                                             );
      AdicionarNoXML(        '<versaoDados>4.00</versaoDados>'                                                                                                                                        );
      AdicionarNoXML(      '</nfeCabecMsg>'                                                                                                                                                           );
      AdicionarNoXML(    '</soap12:Header>'                                                                                                                                                           );
      AdicionarNoXML(    '<soap12:Body>'                                                                                                                                                              );
      AdicionarNoXML(      '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NFeStatusServico4">'                                                                                          );
      AdicionarNoXML(       vMensagem                                                                                                                                                                 );
      AdicionarNoXML(      '</nfeDadosMsg>'                                                                                                                                                           );
      AdicionarNoXML(    '</soap12:Body>'                                                                                                                                                             );
      AdicionarNoXML(  '</soap12:Envelope>'                                                                                                                                                           );

      if not _Biblioteca.TemConexaoInternet then begin
        Result.mensagem_erro := 'Sem conex�o com a internet, por favor, verifique!';
        Result.HouveErro := True;
        Exit;
      end;

      FConexao.URL := FURL + '?wsdl';
      FConexao.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/NFeStatusServico4';

      FConexao.Execute(vXml, vStream);
      vStrStream.CopyFrom(vStream, 0);

      vRetornoWS := _BibliotecaNFE.RetirarAcentos(string(ParseText(AnsiString(vStrStream.DataString), True, True)));
      vRetWS := string(SepararDados( AnsiString(vRetornoWS), 'retConsStatServ', True) );

      if vRetWS = '' then
        raise Exception.Create('Nenhum dado retornado do WebService da SEFAZ!');

      FLeitorXML.Arquivo := vRetWS;
      FLeitorXML.Grupo := FLeitorXML.Arquivo;
      if FLeitorXML.rExtrai(1, 'retConsStatServ') <> '' then begin
        SalvarXML(vMensagem, FCaminhoXMLs + FormatDateTime('yyyymmddhhnnss', Now) + '-ped-sta-ret.xml');

        Result.tipo_ambiente := IfThen(FLeitorXML.rCampo(tcInt, 'tpAmb') = 1, 'Produ��o', 'Homologa��o');
        Result.versao_aplicacao := FLeitorXML.rCampo(tcStr, 'verAplic');
        Result.status := FLeitorXML.rCampo(tcInt, 'cStat');
        Result.motivo := FLeitorXML.rCampo(tcStr, 'xMotivo');
        Result.uf := IntToStr(FCodigoUFIBGE);
        Result.data_hora_recebimento := FLeitorXML.rCampo(tcDatHor, 'dhRecbto');
        Result.tempo_medio := FLeitorXML.rCampo(tcInt, 'tMed');
        Result.data_hora_retorno := FLeitorXML.rCampo(tcDatHor, 'dhRetorno');
        Result.observacao := FLeitorXML.rCampo(tcStr, 'xObs');
      end;
    except
      on E: Exception do begin
        Result.HouveErro := True;
        Result.mensagem_erro := e.Message;
      end;
    end;
  finally
    vAcao.Free;
    vStream.Free;
    vStrStream.Free;
  end;
end;

function TServicosNFE.BuscarNFeEntradas(pCNPJ: string; pUltimoNsuConsultadoDistNFe: string; pChaveNFe: string): RecRespostaNFe<RecResumoNFe>;
var
  i: Integer;
  vXml: string;
  vRetWS: string;
  vMensagem: string;
  vRetornoWS: string;
  vCNPJ_CPF: string;
  vAcao: TStringList;
  vStream: TMemoryStream;
  vStrStream: TStringStream;

  vConsultaChave: Boolean;

  vTexto64Nfe: AnsiString;
  vLeitorXML: TNovoLeitorXML;
  vNomeArquivoXmlGeradoRetorno: string;

  procedure AdicionarNoXML(pAdicao: string);
  begin
    vXml := vXml + pAdicao;
  end;

begin
  vConsultaChave := pChaveNFe <> '';

  vAcao := TStringList.Create;
  vStream := TMemoryStream.Create;
  vStrStream := TStringStream.Create('');
  try
    try
      vXml := '';
      Result.HouveErro := False;

      vMensagem :=
        '<?xml version="1.0" encoding="utf-8"?>'                                     +
          '<distDFeInt xmlns="http://www.portalfiscal.inf.br/nfe" versao="' + IIfStr(vConsultaChave, '1.01', '1.00') + '">' +
            '<tpAmb>' + IfThen(FAmbienteNFE = tpHomologacao, '2', '1' ) + '</tpAmb>' +
            '<cUFAutor>' + IntToStr(FCodigoUFIBGE) + '</cUFAutor>'                   +
            '<CNPJ>' + RetornaNumeros(pCNPJ) + '</CNPJ>';

      if not vConsultaChave then begin
        vMensagem :=
          vMensagem +
            '<distNSU>'                                                              +
              '<ultNSU>' + _Biblioteca.LPad(pUltimoNsuConsultadoDistNFe, 15, '0') + '</ultNSU>' +
            '</distNSU>';

      end
      else begin
        vMensagem :=
          vMensagem +
            '<consChNFe>'                                                              +
              '<chNFe>' + _Biblioteca.RetornaNumeros(pChaveNFe) + '</chNFe>' +
            '</consChNFe>';
      end;

      vMensagem := vMensagem + '</distDFeInt>';

      SalvarXML(vMensagem, FCaminhoXMLs + FormatDateTime('yyyymmddhhnnss', Now) + '-con-dist-dfe.xml');
      //_BibliotecaNFE.ValidarXML(vMensagem, FCaminhoSchemas, tsNfeStatusServico);

      FURL := _BibliotecaNFE.getURLWebService(FUFEmitente, FAmbienteNFE, tsNfeDistDFeInteresse);
      vMensagem := StringReplace(vMensagem, '<?xml version="1.0" encoding="utf-8"?>', '', [rfReplaceAll]);

      AdicionarNoXML('<?xml version="1.0" encoding="utf-8"?>'                                                                                                                                         );
      AdicionarNoXML(  '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">'  );
      AdicionarNoXML(    '<soap12:Body>'                                                                                                                                                              );
      AdicionarNoXML(      '<nfeDistDFeInteresse xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NFeDistribuicaoDFe">'                   );
      AdicionarNoXML(        '<nfeDadosMsg>'                                                                                            );
      AdicionarNoXML(           vMensagem                                                                                               );
      AdicionarNoXML(        '</nfeDadosMsg>'                                                                                           );
      AdicionarNoXML(      '</nfeDistDFeInteresse>'                                                                                     );
      AdicionarNoXML(    '</soap12:Body>'                                                                                               );
      AdicionarNoXML(  '</soap12:Envelope>'                                                                                             );

      SalvarXML(vXml, FCaminhoXMLs + FormatDateTime('yyyymmddhhnnss', Now) + '-cons-dfe.xml');

      if not _Biblioteca.TemConexaoInternet then begin
        Result.mensagem_erro := 'Sem conex�o com a internet, por favor, verifique!';
        Result.HouveErro := True;
        Exit;
      end;

      FConexao.URL := FURL + '?wsdl';
      FConexao.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/NFeDistribuicaoDFe/nfeDistDFeInteresse';
      FConexao.Execute(vXml, vStream);

      vStrStream.CopyFrom(vStream, 0);

      vRetornoWS := _BibliotecaNFE.RetirarAcentos(string(ParseText(AnsiString(vStrStream.DataString), True, True)));
      vRetWS := string(SepararDados( AnsiString(vRetornoWS), 'nfeDistDFeInteresseResult') );

      if vRetWS = '' then
        raise Exception.Create('Nenhum dado retornado do WebService da SEFAZ!');

      FLeitorXML.Arquivo := vRetWS;
      FLeitorXML.Grupo := FLeitorXML.Arquivo;
      if FLeitorXML.rExtrai(1, 'retDistDFeInt') <> '' then begin
        vNomeArquivoXmlGeradoRetorno := FCaminhoXMLs + FormatDateTime('yyyymmddhhnnss', Now) + '-ret-dfe.xml';
        SalvarXML(vRetWS, vNomeArquivoXmlGeradoRetorno);

        Result.tipo_ambiente         := IfThen(FAmbienteNFE = tpProducao, 'Produ��o', 'Homologa��o');
        Result.versao_aplicacao      := FLeitorXML.rCampo(tcStr, 'verAplic');
        Result.status                := FLeitorXML.rCampo(tcInt, 'cStat');
        Result.motivo                := FLeitorXML.rCampo(tcStr, 'xMotivo');
        Result.uf                    := IntToStr(FCodigoUFIBGE);
        Result.data_hora_recebimento := FLeitorXML.rCampo(tcDatHor, 'dhRecbto');
        Result.data_hora_retorno     := FLeitorXML.rCampo(tcDatHor, 'dhRetorno');
        Result.ultimo_nsu            := FLeitorXML.rCampo(tcStr, 'ultNSU');

        Result.HouveErro := Result.status <> '138';
        if Result.HouveErro then
          Result.mensagem_erro := Result.motivo
        else begin
          Result.Dados := nil;
          vLeitorXML := TNovoLeitorXML.Create(vNomeArquivoXmlGeradoRetorno, '');

          vLeitorXML.TagAtual := 'loteDistDFeInt';
          SetLength(Result.Dados, vLeitorXML.getQtdeNos);

          vLeitorXML.TagAtual := 'docZip';
          for i := Low(Result.Dados) to High(Result.Dados) do begin
            vTexto64Nfe := AnsiString(vLeitorXML.getValorNoAtual);
            Result.Dados[i].XmlNFe := _BibliotecaNFe.getXMLBase64(vTexto64Nfe);

            FLeitorXML.Arquivo := Result.Dados[i].XmlNFe;
            FLeitorXML.Grupo := FLeitorXML.Arquivo;
            // Salvando os resumos das notas fiscais
            if FLeitorXML.rExtrai(1, 'resNFe') <> '' then begin
              vCNPJ_CPF := FLeitorXML.rCampo(tcStr, 'CNPJ');
              if vCNPJ_CPF = '' then
                vCNPJ_CPF := FLeitorXML.rCampo(tcStr, 'CPF');

              Result.Dados[i].CNPJ              := vCNPJ_CPF;
              Result.Dados[i].ChaveNFe          := FLeitorXML.rCampo(tcStr, 'chNFe');
              Result.Dados[i].RazaoSocial       := FLeitorXML.rCampo(tcStr, 'xNome');
              Result.Dados[i].InscricaoEstadual := FLeitorXML.rCampo(tcStr, 'IE');
              Result.Dados[i].DataEmissao       := FLeitorXML.rCampo(tcDatHor, 'dhEmi');
              Result.Dados[i].ValorNota         := FLeitorXML.rCampo(tcDe2, 'vNF');
              Result.Dados[i].StatusNota        := FLeitorXML.rCampo(tcStr, 'cSitNFe');
              Result.Dados[i].NumeroNota        := SFormatInt(Copy(Result.Dados[i].ChaveNFe, 26, 9));
              Result.Dados[i].SerieNota         := Copy(Result.Dados[i].ChaveNFe, 23, 3);
              Result.Dados[i].StatusNota        := '1';
            end
            // Salvando as notas fiscais canceladas            
            else if FLeitorXML.rExtrai(1, 'infEvento') <> '' then begin
              Result.Dados[i].ChaveNFe   := FLeitorXML.rCampo(tcStr, 'chNFe');
              Result.Dados[i].StatusNota := IIfStr(FLeitorXML.rCampo(tcStr, 'tpEvento') = teEventoCancelamento, '3');              
            end;
                                         
            vLeitorXML.ProximoNo;
          end;
        end;
      end;
    except
      on E: Exception do begin
        Result.HouveErro := True;
        Result.mensagem_erro := e.Message;
      end;
    end;
  finally
    vAcao.Free;
    vStream.Free;
    vStrStream.Free;
  end;
end;

function TServicosNFE.BuscarCTeEntradas(pCNPJ: string; pUltimoNsuConsultadoDistCTE: string): RecRespostaCTe;
var
  i: Integer;
  j: Integer;
  vXml: string;
  vRetWS: string;
  vMensagem: string;
  vRetornoWS: string;
  vAcao: TStringList;
  vStream: TMemoryStream;
  vStrStream: TStringStream;
  vTexto64Nfe: AnsiString;
  vLeitorXML: TNovoLeitorXML;
  vLeitorXMLReferencais: TNovoLeitorXML;
  vNomeArquivoXmlGeradoRetorno: string;

  procedure AdicionarNoXML(pAdicao: string);
  begin
    vXml := vXml + pAdicao;
  end;

begin
  vAcao := TStringList.Create;
  vStream := TMemoryStream.Create;
  vStrStream := TStringStream.Create('');
  try
    try
      vXml := '';
      Result.HouveErro := False;

      vMensagem :=
        '<?xml version="1.0" encoding="utf-8"?>'                                     +
          '<distDFeInt xmlns="http://www.portalfiscal.inf.br/cte" versao="1.00">'    +
            '<tpAmb>' + IfThen(FAmbienteNFE = tpHomologacao, '2', '1' ) + '</tpAmb>' +
            '<cUFAutor>' + IntToStr(FCodigoUFIBGE) + '</cUFAutor>'                   +
            '<CNPJ>' + RetornaNumeros(pCNPJ) + '</CNPJ>'                             +
            '<distNSU>'                                                              +
              '<ultNSU>' + _Biblioteca.LPad(pUltimoNsuConsultadoDistCTE, 15, '0') + '</ultNSU>' +
            '</distNSU>'                                                             +
          '</distDFeInt>';

      SalvarXML(vMensagem, FCaminhoXMLs + FormatDateTime('yyyymmddhhnnss', Now) + '-con-dist-dfe.xml');
      //_BibliotecaNFE.ValidarXML(vMensagem, FCaminhoSchemas, tsNfeStatusServico);

      FURL := _BibliotecaNFE.getURLWebService(FUFEmitente, FAmbienteNFE, tsNfeDistDFeInteresseCTe);
      vMensagem := StringReplace(vMensagem, '<?xml version="1.0" encoding="utf-8"?>', '', [rfReplaceAll]);

      AdicionarNoXML('<?xml version="1.0" encoding="utf-8"?>'                                                                                                                                         );
      AdicionarNoXML(  '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">'  );
      AdicionarNoXML(    '<soap12:Body>'                                                                                                                                                              );
      AdicionarNoXML(      '<cteDistDFeInteresse xmlns="http://www.portalfiscal.inf.br/cte/wsdl/CTeDistribuicaoDFe">'                   );
      AdicionarNoXML(        '<cteDadosMsg>'                                                                                            );
      AdicionarNoXML(           vMensagem                                                                                               );
      AdicionarNoXML(        '</cteDadosMsg>'                                                                                           );
      AdicionarNoXML(      '</cteDistDFeInteresse>'                                                                                     );
      AdicionarNoXML(    '</soap12:Body>'                                                                                               );
      AdicionarNoXML(  '</soap12:Envelope>'                                                                                             );

      SalvarXML(vXml, FCaminhoXMLs + FormatDateTime('yyyymmddhhnnss', Now) + '-cons-dist-cte.xml');

      if not _Biblioteca.TemConexaoInternet then begin
        Result.MensagemErro := 'Sem conex�o com a internet, por favor, verifique!';
        Result.HouveErro := True;
        Exit;
      end;

      FConexao.URL := FURL + '?wsdl';
      FConexao.SoapAction := 'http://www.portalfiscal.inf.br/cte/wsdl/CTeDistribuicaoDFe/cteDistDFeInteresse';
      FConexao.Execute(vXml, vStream);

      vStrStream.CopyFrom(vStream, 0);

      vRetornoWS := _BibliotecaNFE.RetirarAcentos(string(ParseText(AnsiString(vStrStream.DataString), True, True)));
      vRetWS := string(SepararDados( AnsiString(vRetornoWS), 'cteDistDFeInteresseResult') );

      if vRetWS = '' then
        raise Exception.Create('Nenhum dado retornado do WebService da SEFAZ!');

      FLeitorXML.Arquivo := vRetWS;
      FLeitorXML.Grupo := FLeitorXML.Arquivo;
      if FLeitorXML.rExtrai(1, 'retDistDFeInt') <> '' then begin
        vNomeArquivoXmlGeradoRetorno := FCaminhoXMLs + FormatDateTime('yyyymmddhhnnss', Now) + '-ret-dist-cte.xml';
        SalvarXML(vRetWS, vNomeArquivoXmlGeradoRetorno);

        Result.TipoAmbiente         := IfThen(FAmbienteNFE = tpProducao, 'Produ��o', 'Homologa��o');
        Result.VersaoAplicacao      := FLeitorXML.rCampo(tcStr, 'verAplic');
        Result.Status                := FLeitorXML.rCampo(tcInt, 'cStat');
        Result.motivo                := FLeitorXML.rCampo(tcStr, 'xMotivo');
        Result.uf                    := IntToStr(FCodigoUFIBGE);
        Result.UltimoNSU            := FLeitorXML.rCampo(tcStr, 'ultNSU');

        Result.HouveErro := Result.status <> '138';
        if Result.HouveErro then
          Result.MensagemErro := Result.motivo
        else begin
          Result.CTEs := nil;
          vLeitorXML := TNovoLeitorXML.Create(vNomeArquivoXmlGeradoRetorno, '');
          vLeitorXMLReferencais := TNovoLeitorXML.Create('', '');

          vLeitorXML.TagAtual := 'loteDistDFeInt';
          SetLength(Result.CTEs, vLeitorXML.getQtdeNos);

          vLeitorXML.TagAtual := 'docZip';
          for i := Low(Result.CTEs) to High(Result.CTEs) do begin
            vTexto64Nfe := AnsiString(vLeitorXML.getValorNoAtual);
            Result.CTEs[i].XmlCTe := _BibliotecaNFe.getXMLBase64(vTexto64Nfe);

            FLeitorXML.Arquivo := Result.CTEs[i].XmlCTe;
            FLeitorXML.Grupo := FLeitorXML.Arquivo;
            // Salvando os resumos das notas fiscais
            if FLeitorXML.rExtrai(1, 'infCte') <> '' then begin
              Result.CTEs[i].CFOP                       := FLeitorXML.rCampo(tcInt, 'CFOP');
              Result.CTEs[i].ChaveCTe                   := _Biblioteca.RetornaNumeros(FLeitorXML.rAtributo('Id'));
              Result.CTEs[i].DataEmissao                := FLeitorXML.rCampo(tcDatHor, 'dhEmi');
              Result.CTEs[i].ValorTotalCTe              := FLeitorXML.rCampo(tcDe2, 'vTPrest');
              Result.CTEs[i].BaseCalculoICMS            := FLeitorXML.rCampo(tcDe2, 'vBC');
              Result.CTEs[i].PercentualIcms             := FLeitorXML.rCampo(tcDe2, 'pICMS');
              Result.CTEs[i].ValorICMS                  := FLeitorXML.rCampo(tcDe2, 'vICMS');
              Result.CTEs[i].NumeroCTe                  := FLeitorXML.rCampo(tcInt, 'nCT');
              Result.CTEs[i].SerieCTe                   := IntToStr(FLeitorXML.rCampo(tcInt, 'serie'));
              Result.CTEs[i].StatusSefaz                := '1';

              if FLeitorXML.rExtrai(1, 'emit') <> '' then begin
                Result.CTEs[i].CNPJEmitente              := FLeitorXML.rCampo(tcStr, 'CNPJ');
                Result.CTEs[i].RazaoSocialEmitente       := FLeitorXML.rCampo(tcStr, 'xNome');
                Result.CTEs[i].InscricaoEstadualEmitente := FLeitorXML.rCampo(tcStr, 'IE');
              end;

              if FLeitorXML.rExtrai(1, 'rem') <> '' then begin
                Result.CTEs[i].CNPJRemetente              := FLeitorXML.rCampo(tcStr, 'CNPJ');
                Result.CTEs[i].RazaoSocialRemetente       := FLeitorXML.rCampo(tcStr, 'xNome');
                Result.CTEs[i].InscricaoEstadualRemetente := FLeitorXML.rCampo(tcStr, 'IE');
              end;

              // Se for transporte a n�o contribuinte n�o haver� NFe
              if _Biblioteca.Em(Result.CTEs[i].CFOP, [5357, 6357])  then
                Continue;

              vLeitorXMLReferencais.setXML( Result.CTEs[i].XmlCTe );
              vLeitorXMLReferencais.TagAtual := 'infDoc';
              SetLength(Result.CTEs[i].NFEsReferenciadas, vLeitorXMLReferencais.getQtdeNos);

              vLeitorXMLReferencais.TagAtual := 'infNFe';
              for j := Low(Result.CTEs[i].NFEsReferenciadas) to High(Result.CTEs[i].NFEsReferenciadas) do begin
                Result.CTEs[i].NFEsReferenciadas[j] := _Biblioteca.RetornaNumeros(vLeitorXMLReferencais.getValorTag('chave'));
                vLeitorXMLReferencais.ProximoNo;
              end;
            end
            // Salvando as notas fiscais canceladas
            else if FLeitorXML.rExtrai(1, 'infEvento') <> '' then begin
              Result.CTEs[i].ChaveCTe    := FLeitorXML.rCampo(tcStr, 'chCTe');
              Result.CTEs[i].StatusSefaz := IIfStr(FLeitorXML.rCampo(tcStr, 'tpEvento') = teEventoCancelamento, '3');
            end;

            vLeitorXML.ProximoNo;
          end;
        end;
      end;
    except
      on E: Exception do begin
        Result.HouveErro := True;
        Result.MensagemErro := e.Message;
      end;
    end;
  finally
    vAcao.Free;
    vStream.Free;
    vStrStream.Free;
  end;
end;

end.
