unit _LeitorXML;

interface

uses
  SysUtils, Classes, StrUtils, Variants, _BibliotecaNFE, _Biblioteca, Xml.xmldom, Xml.XMLIntf, Xml.Win.msxmldom, Xml.XMLDoc, Vcl.Forms;

type
  TLeitorXML = class(TPersistent)
  private
    FArquivo: string;
    FGrupo: string;
    FNivel: TStringList;
  public
    constructor Create;
    destructor Destroy; override;
    function rExtrai(const nivel: integer; const TagInicio: string; TagFim: string = ''; const item: integer = 1): string;
    function rCampo(const Tipo: tcTipoCampo; TAG: string; TAGparada: string = ''): Variant;
    function rCampoCNPJCPF(TAGparada: string = ''): string;
    function rAtributo(Atributo: string): variant;
    function CarregarArquivo(const CaminhoArquivo: string): boolean; overload;
    function CarregarArquivo(const Stream: TStringStream): boolean; overload;
    function PosLast(const SubStr, S: string ): Integer;
  published
    property Arquivo: string read FArquivo write FArquivo;
    property Grupo: string read FGrupo write FGrupo;
  end;

  TNovoLeitorXML = class(TPersistent)
  private
    FXML: TXMLDocument;
    FNoAtual: IXMLNode;
    FTagAtual: string;
    procedure setTagAtual(const pValue: string);
  public
    constructor Create(pCaminhoXML: string; pXml: string);

    procedure setXML(pXML: string);

    function getQtdeNos: Integer;
    function getValorTag(pTag: string): string;
    function getValorNoAtual: string;

    function ProximoNo: Boolean;

  published
    property TagAtual: string read FTagAtual write setTagAtual;
  end;

implementation

{ TLeitorXML }

constructor TLeitorXML.Create;
var
  i: integer;
begin
  inherited Create;
  FNivel := TStringList.Create;
  for i := 1 to 10 do
    FNivel.add('');
end;

destructor TLeitorXML.Destroy;
begin
  FNivel.Free;
  inherited;
end;

function TLeitorXML.CarregarArquivo(const CaminhoArquivo: string): boolean;
var
  ArquivoXML: TStringList;
begin
  //NOTA: Carrega o arquivo xml na mem�ria para posterior leitura de sua tag's
  ArquivoXML := TStringList.Create;
  try
    try
      ArquivoXML.LoadFromFile(CaminhoArquivo);
      FArquivo := ArquivoXML.Text;
      Result := True;
    except
      Result := False;
      raise;
    end;
  finally
    ArquivoXML.Free;
  end;
end;

function TLeitorXML.CarregarArquivo(const Stream: TStringStream): boolean;
begin
  //NOTA: Carrega o arquivo xml na mem�ria para posterior leitura de sua tag's
  try
    FArquivo := Stream.DataString;
    Result := True;
  except
    raise;
  end;
end;

function TLeitorXML.rExtrai(const nivel: integer; const TagInicio: string; TagFim: string = ''; const item: integer = 1): string;
var
  Texto: string;
  i, j: integer;
begin
  //NOTA: Extrai um grupo de dentro do nivel informado
  FNivel.strings[0] := string(FArquivo);
  if Trim(TagFim) = '' then
    TagFim := TagInicio;
  Texto := FNivel.Strings[nivel - 1];
  Result := '';
  FGrupo := '';
  for i := 1 to item do
    if i < item then
      Texto := copy(Texto, pos('</' + Trim(TagFim) + '>', Texto) + length(Trim(TagFim)) + 3, maxInt);

  Texto := copy(Texto, 1,pos('</' + Trim(TagFim) + '>', Texto) + length(Trim(TagFim)) + 3);   //Corre��o para leitura de tags em que a primeira � diferente da segunda Ex: <infProt id=XXX> e a segunda apenas <infProt>   
  i := pos('<' + Trim(TagInicio) + '>', Texto);
  if i = 0 then
    i := pos('<' + Trim(TagInicio) + ' ', Texto);
  if i = 0 then
    i := pos('<' + Trim(TagInicio) + ':', Texto); //corre��o para webservice do Cear�
  if i = 0 then
    exit;
  Texto := copy(Texto, i, maxInt);
  j:=pos('</' + Trim(TagFim) + '>',Texto);
  if j=0 then
   j:=pos('</' + Trim(TagFim) + ':',Texto); //corre��o para webservice do Cear�
  Result := TrimRight(copy(Texto, 1, j - 1));
  FNivel.strings[nivel] := Result;
  FGrupo := result;
end;

function TLeitorXML.rCampoCNPJCPF(TAGparada: string = ''): string;
begin
  result := rCampo(tcStr, 'CNPJ', TAGparada);
  if trim(result) = '' then
    result := rCampo(tcStr, 'CPF', TAGparada);
end;

function TLeitorXML.rCampo(const Tipo: tcTipoCampo; TAG: string; TAGparada: string = ''): Variant;
var
  ConteudoTag: string;
  inicio: Integer;
  fim: Integer;
  inicioTAGparada: Integer;
begin
  Tag := UpperCase(Trim(TAG));
  inicio := pos('<' + Tag + '>', UpperCase(FGrupo));

  if Trim(TAGparada) <> '' then begin
    inicioTAGparada := pos('<' + UpperCase(Trim(TAGparada)) + '>', UpperCase(FGrupo));
    if inicioTAGparada = 0 then
      inicioTAGparada := inicio;
  end
  else
    inicioTAGparada := inicio;

  if (inicio = 0) or (InicioTAGparada < inicio) then
    ConteudoTag := ''
  else begin
    inicio := inicio + Length(Tag) + 2;
    fim := pos('</' + Tag + '>', UpperCase(FGrupo)) - inicio;
    ConteudoTag := trim(copy(FGrupo, inicio, fim));
  end;

  case Tipo of
    tcStr: result := ReverterFiltroTextoXML(ConteudoTag);

    tcDat: begin
      if Length(ConteudoTag) > 0 then
        result := EncodeDate(StrToInt(copy(ConteudoTag, 01, 4)), StrToInt(copy(ConteudoTag, 06, 2)), StrToInt(copy(ConteudoTag, 09, 2)))
      else
        result:=0;
    end;

    tcDatCFe: begin
      if Length(ConteudoTag) > 0 then
        result := EncodeDate(StrToInt(copy(ConteudoTag, 01, 4)), StrToInt(copy(ConteudoTag, 05, 2)), StrToInt(copy(ConteudoTag, 07, 2)))
      else
        result:=0;
    end;

    tcDatHor: begin
      if Length(ConteudoTag) > 0 then
        Result := EncodeDate(StrToInt(copy(ConteudoTag, 01, 4)), StrToInt(copy(ConteudoTag, 06, 2)), StrToInt(copy(ConteudoTag, 09, 2))) +
        EncodeTime(StrToInt(copy(ConteudoTag, 12, 2)), StrToInt(copy(ConteudoTag, 15, 2)), StrToInt(copy(ConteudoTag, 18, 2)), 0)
      else
        result:=0;
    end;

    tcHor: begin
      if Length(ConteudoTag) > 0 then
        result := EncodeTime(StrToInt(copy(ConteudoTag, 1, 2)), StrToInt(copy(ConteudoTag, 4, 2)), StrToInt(copy(ConteudoTag, 7, 2)), 0)
      else
        result := 0;
    end;

    tcHorCFe: begin
      if Length(ConteudoTag) > 0 then
        result := EncodeTime(StrToInt(copy(ConteudoTag, 1, 2)), StrToInt(copy(ConteudoTag, 3, 2)), StrToInt(copy(ConteudoTag, 5, 2)), 0)
      else
        result := 0;
    end;

    tcDe2, tcDe3, tcDe4, tcDe6, tcDe10: result := StrToFloatDef(StringReplace(ConteudoTag, '.', FormatSettings.DecimalSeparator, []),0);

    tcEsp: result := ConteudoTag;

    tcInt: result := StrToIntDef(Trim(_Biblioteca.RetornaNumeros(ConteudoTag)), 0);

    tcInt64: result := StrToInt64Def(Trim(_Biblioteca.RetornaNumeros(ConteudoTag)), 0);
  else
    raise Exception.Create('Tag <' + Tag + '> com conte�do inv�lido. ' + ConteudoTag);
  end;
end;

function TLeitorXML.rAtributo(Atributo: string): variant;
var
  ConteudoTag, Aspas: string;
  inicio, fim: integer;
begin
  Atributo := Trim(Atributo);
  inicio := pos(Atributo, FGrupo) + Length(Atributo);
  ConteudoTag := trim(copy(FGrupo, inicio, maxInt));

  if Pos('"', ConteudoTag) <> 0 then
    Aspas := '"'
  else
    Aspas := '''';

  inicio := pos(Aspas, ConteudoTag) + 1;
  ConteudoTag := trim(copy(ConteudoTag, inicio, maxInt));
  fim := pos(Aspas, ConteudoTag) - 1;
  ConteudoTag := copy(ConteudoTag, 1, fim);
  result := ReverterFiltroTextoXML(ConteudoTag)
end;

function TLeitorXML.PosLast(const SubStr, S: string ): Integer;
var
  P: Integer ;
begin
  Result := 0 ;
  P := Pos( SubStr, S) ;
  while P <> 0 do begin
    Result := P ;
    P := PosEx(SubStr, S, P + 1) ;
  end;
end;

{ TNovoLeitorXML }

constructor TNovoLeitorXML.Create(pCaminhoXML: string; pXml: string);
begin
  FXML := TXMLDocument.Create(Application);

  if pCaminhoXML <> '' then begin
    FXML.LoadFromFile(pCaminhoXML);
    FXML.Active := True;
  end
  else if pXml <> '' then begin
    FXML.LoadFromFile(pXml);
    FXML.Active := True;
  end;
end;

function TNovoLeitorXML.getQtdeNos: Integer;
begin
  Result := FNoAtual.ChildNodes.Count;
end;

function TNovoLeitorXML.getValorNoAtual: string;
begin
  Result := FNoAtual.NodeValue;
end;

function TNovoLeitorXML.getValorTag(pTag: string): string;
begin
  Result := FNoAtual.ChildNodes[pTag].Text;
end;

function TNovoLeitorXML.ProximoNo: Boolean;
begin
  FNoAtual := FNoAtual.NextSibling;
  Result := FNoAtual <> nil;
end;

procedure TNovoLeitorXML.setTagAtual(const pValue: string);
begin
  FTagAtual := pValue;
  FNoAtual := getNoXMLDocument(pValue, FXML.Node);
  FNoAtual.ChildNodes.First;
end;

procedure TNovoLeitorXML.setXML(pXML: string);
begin
  FXML.Active := False;
  FXML.LoadFromXML( AnsiString(pXML) );
  FXML.Active := True;
end;

end.

