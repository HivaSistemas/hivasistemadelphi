unit _ComunicacaoNFE;

interface

uses
  System.Classes, Vcl.Forms, System.SysUtils, _ServicosNFE, _Sessao, _Biblioteca, _BibliotecaNFE, _RecordsNFE, _RecordsNotasFiscais, _NotasFiscais, _NotasFiscaisItens,
  _ParametrosEmpresa, _RecordsEspeciais, System.StrUtils, _Orcamentos, System.Math, MenuPrincipal, _ImpressaoDANFE, _Retiradas, _ImpressaoDANFE_NFCe,
  _NotasFiscaisCartasCorrecoes, _NotasFiscaisReferencias, _PreEntradasNotasFiscais, BuscaDados, _PreEntradasCte, _PreEntrCteNfeReferenciadas,
  _ConhecimentosFretesItens, _Fornecedores, Impressao.ComprovanteGraficoNFCe;

type
  TNotificacao = procedure(pNotificacao: string) of object;

  {$M+}
  TComunicacaoNFe = class
  private
    FServicos: TServicosNFE;
    FNotificacao: TNotificacao;
  public
    constructor Create(AOwner: TComponent);

    function InutilizarNFe(
      const pCNPJ: string;
      const pAno: Integer;
      const pSerie: Integer;
      const pNumeracaoInicial: Integer;
      const pNumeracaoFinal: Integer;
      const pJustificativa: string;
      const pModeloNota: string
    ): Boolean;

    function ConsultarStatusServico: RecRespostaNFE;
    function CancelarNFe(const pNotaFiscalId: Integer; const pJustificativa: string): Boolean;
    function EnviarCorrecao(const pNotaFiscalId: Integer; pChaveNFe: string; pSequencia: Integer; pTextoCorrecao: string): Boolean;
    function EmitirNFe(const pNotaFiscalId: Integer; pImprimirDanfe: Boolean): Boolean;

    function ConsultarNFeDistribuicao: Boolean;
    function ConsultarCTeDistribuicao: Boolean;

    function ManifestarCienciaNota(pPreEntradaId: Integer): Boolean;
    function ManifestarOperacaoNaoRealizada(pPreEntradaId: Integer): Boolean;
    function ManifestarDesconhecimento(pPreEntradaId: Integer): Boolean;

  published
    property OnNotificarStatus: TNotificacao read FNotificacao write FNotificacao;
  end;

function EnviarNFe(pNotaFiscalId: Integer; pImprimirDanfe: Boolean = True): Boolean;
function getNotasConhecimentoFrete(pPreEntradaId: Integer): TArray<RecConhecimentosFretesItens>;

implementation

function EnviarNFe(pNotaFiscalId: Integer; pImprimirDanfe: Boolean): Boolean;
var
  oNFe: TComunicacaoNFe;
begin
  try
    oNFe := TComunicacaoNFe.Create(Application);
    Result := oNFe.EmitirNFe(pNotaFiscalId, pImprimirDanfe);
  finally
    FreeAndNil(oNFe);
  end;
end;

function getNotasConhecimentoFrete(pPreEntradaId: Integer): TArray<RecConhecimentosFretesItens>;
var
  i: Integer;
  vCaminhoArquivoXml: string;
  vPreEntradasNotas: TArray<RecPreEntradasNotasFiscais>;

  vCapaNota: RecCapaNFe;
  vItens: TArray<RecItemNFe>;
  vFaturas: TArray<RecFaturamentoNFe>;

  vDadosForn: RecCadastroIdRazaoSocial;
begin
  Result := nil;

  vPreEntradasNotas := _PreEntradasNotasFiscais.BuscarPreEntradasNotasFiscais(Sessao.getConexaoBanco, 1, [pPreEntradaId]);
  if vPreEntradasNotas = nil then
    Exit;

  SetLength(Result, Length(vPreEntradasNotas));
  for i := Low(vPreEntradasNotas) to High(vPreEntradasNotas) do begin

    vCaminhoArquivoXml :=
      Sessao.getCaminhoPastaPreEntradas + '\pre_entrada_' + IntToStr(vPreEntradasNotas[0].PreEntradaId) + '_nota' + IntToStr(vPreEntradasNotas[0].NumeroNota) + '.xml';

    if
      not _Biblioteca.salvarStringEmAquivo(
        vPreEntradasNotas[i].XmlTexto,
        vCaminhoArquivoXml
      )
    then
      Exit;

    if not _BibliotecaNFE.BuscarDadosNFeEntrada(vCaminhoArquivoXml, vCapaNota, vItens, vFaturas) then begin
      _Biblioteca.Exclamar('N�o foi poss�vel carregar as notas que s�o referenciadas pelo CTe!');
      Result := nil;
      Exit;
    end;

    vDadosForn := _Fornecedores.getCodigoNomeFantasiaFornecedor(Sessao.getConexaoBanco, vPreEntradasNotas[0].Cnpj);
    if vDadosForn.CadastroId > 0 then begin
      Result[i].FornecedorNotaId := vDadosForn.CadastroId;
      Result[i].NomeFornecedor   := vDadosForn.RazaoSocial;
    end;

    Result[i].NumeroNota       := vCapaNota.NumeroNota;
    Result[i].ModeloNota       := vCapaNota.ModeloNota;
    Result[i].SerieNota        := vCapaNota.SerieNota;
    Result[i].DataEmissaoNota  := vCapaNota.DataHoraEmissao;
    Result[i].PesoNota         := vCapaNota.PesoLiquido;
    Result[i].QuantidadeNota   := Length(vItens);
    Result[i].ValorTotalNota   := vCapaNota.ValorTotal;
  end;
end;

{ TNFe }

constructor TComunicacaoNFe.Create(AOwner: TComponent);
var
  tipoAmbiente: _BibliotecaNFe.tpAmbienteNFE;
begin
  if Sessao.getParametrosEmpresa.tipoAmbienteNFCe = 'H' then
    tipoAmbiente := _BibliotecaNFE.tpHomologacao
  else
    tipoAmbiente := _BibliotecaNFE.tpProducao;

  FServicos :=
    TServicosNFE.Create(
      Sessao.getEmpresaLogada.EstadoId,
      tipoAmbiente,
      Sessao.getParametrosEstacao2.CaminhoXMLs,
      Sessao.getParametrosEmpresa.SerieCertificado,
      Sessao.getParametrosEmpresa.CscNFce,
      Sessao.getParametrosEmpresa.IdToken
   );

  OnNotificarStatus := MenuPrincipal.FormMenuPrincipal.NotificacaoEnvioNFe;
end;

function TComunicacaoNFe.EmitirNFe(const pNotaFiscalId: Integer; pImprimirDanfe: Boolean): Boolean;
var
  i: Integer;
  vNotaFiscalId: Integer;
  vRetorno: RecRespostaNFE;
  vRetornoBanco: RecRetornoBD;

  vDadosNota: TArray<RecNotaFiscal>;
  vDadosGeracaoNota: RecDadosGeracaoNFe;
  vNotaItens: TArray<RecNotaFiscalItem>;
  vReferencias: TArray<RecNotasFiscaisReferencias>;
  vDadosCartoes: TArray<RecDadosCartoes>;

  procedure Erro(const pMensagem: string);
  begin
    if FServicos <> nil then
      FServicos.Free;

    Exclamar(pMensagem);

    FNotificacao('Finalizar...');
  end;

begin
  Result := False;
  vNotaFiscalId := pNotaFiscalId;

  FNotificacao('�nicio do processamento da NFe...');

  FNotificacao('Buscando informa��es sobre a nota fiscal no SGBD...');
  vDadosNota := _NotasFiscais.BuscarNotasFiscais(Sessao.getConexaoBanco, 0, [vNotaFiscalId]);
  if vDadosNota = nil then begin
    Erro('N�o foram encontrado dados da nota fiscal a ser emitida!');
    Exit;
  end;

  if vDadosNota[0].status = 'E' then begin
    Erro('Esta nota fiscal j� foi emitida!');
    Exit;
  end;

  FNotificacao('Buscando informa��es dos itens da nota fiscal no SGBD...');
  vNotaItens := _NotasFiscaisItens.BuscarNotasFiscaisItens(Sessao.getConexaoBanco, 0, [vNotaFiscalId]);
  if vNotaItens = nil then begin
    Erro('Os itens da nota fiscal n�o foram encontrados!');
    Exit;
  end;

  FNotificacao('Buscando informa��es de refer�ncias...');
  vReferencias := _NotasFiscaisReferencias.BuscarNotasFiscaisReferencias(Sessao.getConexaoBanco, 0, [vNotaFiscalId]);

  vDadosCartoes := _NotasFiscais.BuscarDadosCartoesNFe(Sessao.getConexaoBanco, vDadosNota[0].OrcamentoBaseId, vDadosNota[0].TipoMovimento);

  FNotificacao('Buscando informa��es auxiliares nos par�metros sobre a nota fiscal...');
  vDadosGeracaoNota := _ParametrosEmpresa.getDadosProximaNfe(Sessao.getConexaoBanco, vDadosNota[0].nota_fiscal_id);
  if vDadosGeracaoNota.numero_nota = 0 then begin
    Erro('Numero da NF-e a ser gerada n�o encontrada!');
    Exit;
  end;

  try
    FNotificacao('Preenchendo cabe�alho XML da NFe...');

    FServicos.setTipoDocumento(vDadosNota[0].tipo_nota);

    FServicos.setCabecalhoNFE(
      _Biblioteca.RetornaNumeros(vDadosNota[0].chave_nfe),
      vDadosNota[0].cnpj_emitente,
      vDadosGeracaoNota.numero_nota,
      vDadosNota[0].numero_pedido,
      vDadosNota[0].SerieNota,
      vDadosNota[0].natureza_operacao,
      vDadosGeracaoNota.data_hora_emissao,
      Em(Copy(vDadosNota[0].CfopId, 1, 1), ['5', '6', '7']),
      vDadosNota[0].codigo_ibge_municipio_emit,
      vDadosNota[0].razao_social_emitente,
      vDadosNota[0].nome_fantasia_emitente,
      vDadosNota[0].logradouro_emitente,
      vDadosNota[0].numero_emitente,
      vDadosNota[0].complemento_emitente,
      vDadosNota[0].nome_bairro_emitente,
      vDadosNota[0].nome_cidade_emitente,
      vDadosNota[0].estado_id_emitente,
      vDadosNota[0].cep_emitente,
      vDadosNOta[0].telefone_emitente,
      vDadosNota[0].inscricao_estadual_emitente,
      '', // CNAE
      vDadosNota[0].regime_tributario,

      vDadosNota[0].cpf_cnpj_destinatario,
      vDadosNota[0].razao_social_destinatario,
      vDadosNota[0].nome_fantasia_destinatario,
      vDadosNota[0].logradouro_destinatario,
      vDadosNota[0].numero_destinatario,
      vDadosNota[0].complemento_destinatario,
      vDadosNota[0].nome_bairro_destinatario,

      vDadosNota[0].codigo_ibge_municipio_dest,
      vDadosNota[0].nome_cidade_destinatario,
      vDadosNota[0].estado_id_destinatario,
      vDadosNota[0].cep_destinatario,
      vDadosNota[0].telefone_consumidor_final,
      vDadosNota[0].inscricao_estadual_destinat,
      '',//vDadosNota[0].email,

      vDadosNota[0].InscricaoEstEntrDestinat,
      vDadosNota[0].LogradouroEntrDestinatario,
      vDadosNota[0].complementoEntrDestinatario,
      vDadosNota[0].NomeBairroEntrDestinatario,
      vDadosNota[0].NomeCidadeEntrDestinatario,
      vDadosNota[0].NumeroEntrDestinatario,
      vDadosNota[0].EstadoIdEntrDestinatario,
      vDadosNota[0].CepEntrDestinatario,
      vDadosNota[0].CodigoIbgeMunicEntrDest,

      vDadosNota[0].CfopId,
      vDadosNota[0].base_calculo_icms,
      vDadosNota[0].valor_icms,
      vDadosNota[0].base_calculo_icms_st,
      vDadosNota[0].valor_icms_st,
      vDadosNota[0].valor_total_produtos,
      vDadosNota[0].valor_frete,
      vDadosNota[0].valor_seguro,
      vDadosNota[0].valor_desconto,
      vDadosNota[0].valor_ipi,
      vDadosNota[0].valor_pis,
      vDadosNota[0].valor_cofins,
      vDadosNota[0].valor_outras_despesas,
      vDadosNota[0].valor_total,
      vDadosNota[0].peso_liquido,
      vDadosNota[0].peso_bruto,

      vDadosNota[0].valor_recebido_dinheiro,
      vDadosNota[0].valor_recebido_cheque,
      vDadosNota[0].valor_recebido_cartao_cred,
      vDadosNota[0].valor_recebido_cartao_deb,
      vDadosNota[0].valor_recebido_credito,
      vDadosNota[0].valor_recebido_cobranca + vDadosNota[0].valor_recebido_financeira,

      vDadosNota[0].informacoes_complementares,
      vDadosNota[0].TipoCliente,
      vDadosNota[0].valor_recebido_pix,
      vDadosNota[0].valor_recebido_acumulado,
      Sessao.getParametros.EmitirNotaAcumuladoFechamentoPedido,
      vDadosNota[0].tipo_frete,
      vDadosNota[0].transportadora_id,
      _Biblioteca.RetornaNumeros(vDadosNota[0].cnpj_transportadora),
      vDadosNota[0].razao_transportadora,
      _Biblioteca.RetornaNumeros(vDadosNota[0].ie_transportadora),
      vDadosNota[0].endereco_transportadora,
      vDadosNota[0].cidade_transportadora,
      vDadosNota[0].uf_transportadora,
      vDadosNota[0].EspecieNFe,
      FloatToStr(vDadosNota[0].QuantidadeNFe),
      vDadosNota[0].valor_icms_inter
    );

    FNotificacao('Preenchendo os itens da NFe');

    for i := Low(vNotaItens) to High(vNotaItens) do begin
      FNotificacao('Preenchendo item n� ' + IntToStr(i + 1) + ' da NFe...');

      FServicos.setProdutoNFE(
        IIfStr(vNotaItens[i].CodigoProdutoNFe <> '', vNotaItens[i].CodigoProdutoNFe, IntToStr(vNotaItens[i].produto_id)),
        vNotaItens[i].nome_produto,
        vNotaItens[i].codigo_barras,
        vNotaItens[i].codigo_ncm,
        vNotaItens[i].CfopId,
        vNotaItens[i].unidade,
        vNotaItens[i].quantidade,
        vNotaItens[i].preco_unitario,
        vNotaItens[i].valor_total,
        vNotaItens[i].codigo_barras,
        vNotaItens[i].unidade,
        vNotaItens[i].quantidade,
        vNotaItens[i].preco_unitario,
        vNotaItens[i].valor_total_desconto,
        vNotaItens[i].valor_total_outras_despesas,
        vNotaItens[i].cst,
        vNotaItens[i].origem_produto,
        3,                  //vNotaItens[i].modalidade_bc_icms,
        (1 - vNotaItens[i].indice_reducao_base_icms) * 100,
        vNotaItens[i].base_calculo_icms,
        vNotaItens[i].percentual_icms,
        vNotaItens[i].valor_icms,
        vNotaItens[i].iva,
        (1 - vNotaItens[i].indice_reducao_base_icms_st) * 100,
        vNotaItens[i].base_calculo_icms_st,
        vNotaItens[i].percentual_icms_st,
        vNotaItens[i].valor_icms_st,
        '',       // Motivo da desonera��o do ICMS
        0,
        0,
        vNotaItens[i].cst_pis,
        vNotaItens[i].base_calculo_pis,
        vNotaItens[i].percentual_pis,
        vNotaItens[i].valor_pis,
        vNotaItens[i].cst_cofins,
        vNotaItens[i].base_calculo_cofins,
        vNotaItens[i].percentual_cofins,
        vNotaItens[i].valor_cofins,
        vNotaItens[i].informacoes_adicionais,
        vNotaItens[i].cest,
        vNotaItens[i].percentual_ipi,
        vNotaItens[i].valor_ipi,
        vNotaItens[i].numero_item_pedido,
        vNotaItens[i].numero_pedido,
        vNotaItens[i].base_calculo_icms_inter,
        vNotaItens[i].valor_icms_inter,
        vNotaItens[i].percentual_icms_inter
      );
    end;
  except
    on e: Exception do begin
      Erro(e.Message);
      Exit;
    end;
  end;

  FNotificacao('Preenchendo refer�ncias...');

  for i := Low(vReferencias) to High(vReferencias) do
    FServicos.setReferencia(vReferencias[i].ChaveNfeReferenciada);

  FNotificacao('Preenchendo cart�es');

  for i := Low(vDadosCartoes) to High(vDadosCartoes) do begin
    try
      FServicos.setDadosCartoes(
        vDadosCartoes[i].TipoRecebCartao,
        vDadosCartoes[i].CnpjAdminsitradora,
        vDadosCartoes[i].Bandeira,
        vDadosCartoes[i].CodigoAutorizacao,
        vDadosCartoes[i].TipoCartao,
        Arredondar((vDadosCartoes[i].ValorCartao * vDadosNota[0].valor_total / vDadosCartoes[i].TotalMovimento), 2)
      );
    except
      on e: Exception do begin
        Erro(e.Message);
        Exit;
      end;
    end;
  end;

  FNotificacao('Enviando NFe para Sefaz...');

  vRetorno := FServicos.EnviarNFE(False);
  if vRetorno.status = '204' then begin
    FNotificacao('NFe em duplicidade, consultando dados da nota...');
    vRetorno := FServicos.ConsultarLoteNFE(vRetorno.protocolo.chave_acesso_nfe);
    if vRetorno.HouveErro then begin
      Erro(vRetorno.mensagem_erro);
      Exit;
    end;
  end
  else if vRetorno.HouveErro then begin
    Erro(vRetorno.mensagem_erro);
    Exit;
  end;

  vDadosNota[0].chave_nfe := vRetorno.protocolo.chave_acesso_nfe;

  if vRetorno.status = '100' then begin
    FNotificacao('Atualizando NFe como emitida no SGBD...');
    vRetornoBanco :=
      _NotasFiscais.SetarNFeEmitida(
        Sessao.getConexaoBanco,
        vNotaFiscalId,
        vRetorno.protocolo.numero_protocolo,
        vRetorno.protocolo.data_hora_recibo,
        vDadosNota[0].chave_nfe,
        vDadosGeracaoNota.data_hora_emissao
      );

    if vRetornoBanco.TeveErro then begin
      Erro('Erro ao atualizar status para emitida: ' + vRetornoBanco.MensagemErro);
      Exit;
    end;

    FNotificacao('Salvando XML da NFe...');
    vRetornoBanco := _NotasFiscais.SalvarXMLNFe(Sessao.getConexaoBanco, vNotaFiscalId, vRetorno.caminho_xml_nfe, vRetorno.textoXML);
    if vRetornoBanco.TeveErro then begin
      Erro('Erro ao salvar o xml da NFe no banco de dados: ' + vRetornoBanco.MensagemErro);
      Exit;
    end;

    vRetornoBanco := Sessao.AtualizarParametrosEmpresasBanco;
    if vRetornoBanco.TeveErro then begin
      Erro('Problema ao atulizar os parametros de empresa, feche o sistema e abra novamente: ' + vRetornoBanco.MensagemErro);
      Exit;
    end;

    FNotificacao('Imprimindo o DANFE...');

    if pImprimirDanfe then begin
      if vDadosNota[0].tipo_nota = 'C' then begin
        if not Impressao.ComprovanteGraficoNFCe.Imprimir(vNotaFiscalId) then
          _Biblioteca.Exclamar('N�o foi poss�vel emitir o DANFE da NFCe, fa�a a impress�o pela tela "Rela��o de notas fiscais"!');
      end
      else begin
        if not _ImpressaoDANFE.Imprimir(vNotaFiscalId) then
          _Biblioteca.Exclamar('N�o foi poss�vel emitir o DANFE da NFe, fa�a a impress�o pela tela "Rela��o de notas fiscais"!');
      end;
    end;
  end;

  FNotificacao('NFe enviada com sucesso...');

  Result := True;
  FServicos.Free;
end;

function TComunicacaoNFe.EnviarCorrecao(
  const pNotaFiscalId: Integer;
  pChaveNFe: string;
  pSequencia: Integer;
  pTextoCorrecao: string
): Boolean;
var
  vRetorno: RecRespostaNFE;
  vRetornoBanco: RecRetornoBD;
begin
  Result := False;

  vRetorno :=
    FServicos.enviarCartaCorrecao(
      Sessao.getNextSeq('SEQ_LOTE_EVENTOS_NFE_ID'),
      _Biblioteca.RetornaNumeros(pChaveNFe),
      Sessao.getEmpresaLogada.cnpj,
      Sessao.getDataHora,
      pSequencia,
      pTextoCorrecao
    );

  if vRetorno.HouveErro then begin
    _Biblioteca.Exclamar('Falha ao enviar carta de corre��o para a NFe! ' + vRetorno.mensagem_erro);
    Exit;
  end;

  vRetornoBanco :=
    _NotasFiscaisCartasCorrecoes.salvarXMLEmissaoCartaCorrecao(
      Sessao.getConexaoBanco,
      pNotaFiscalId,
      pSequencia,
      vRetorno.protocolo.numero_protocolo,
      vRetorno.textoXML,
      vRetorno.caminho_xml_nfe
    );

  if vRetornoBanco.TeveErro then begin
    Exclamar('Erro ao salvar o xml do evento da NFe no banco de dados: ' + vRetornoBanco.MensagemErro);
    Exit;
  end;

  Result := True;
end;

function TComunicacaoNFe.InutilizarNFe(
  const pCNPJ: string;
  const pAno: Integer;
  const pSerie: Integer;
  const pNumeracaoInicial: Integer;
  const pNumeracaoFinal: Integer;
  const pJustificativa: string;
  const pModeloNota: string
): Boolean;
var
  vRetorno: RecRespostaNFE;
begin
  vRetorno :=
    FServicos.InutilizarNumeroNFe(
      RetornaNumeros(pCNPJ),
      pAno,
      pSerie,
      pNumeracaoInicial,
      pNumeracaoFinal,
      pJustificativa,
      pModeloNota
    );

  Result := not vRetorno.HouveErro;
  if not Result then
    Exclamar(vRetorno.mensagem_erro);
end;

function TComunicacaoNFe.ManifestarCienciaNota(pPreEntradaId: Integer): Boolean;
var
  vRetBanco: RecRetornoBD;
  vRetorno: RecRespostaNFE;
  vPreEntrada: TArray<RecPreEntradasNotasFiscais>;

  vRetDownloadNFe: RecRespostaNFe<RecResumoNFe>;
begin
  Result := False;

  vPreEntrada := _PreEntradasNotasFiscais.BuscarPreEntradasNotasFiscais(Sessao.getConexaoBanco, 0, [pPreEntradaId]);
  if vPreEntrada = nil then begin
    _Biblioteca.Exclamar('N�o foi encontrados os dados da pr�-entrada!');
    Exit;
  end;

  vRetorno :=
    FServicos.manifestarOperacao(
      ttCienciaOperacao,
      Sessao.getNextSeq('SEQ_LOTE_EVENTOS_NFE_ID'),
      _Biblioteca.RetornaNumeros(vPreEntrada[0].ChaveAcessoNfe),
      Sessao.getEmpresaLogada.Cnpj,
      Sessao.getDataHora,
      ''
    );

  // Retorno 573 � nota ja manifestada, ent�o apenas setando como manifetada no banco
  if vRetorno.HouveErro and (vRetorno.status <> '573') then begin
    _Biblioteca.Exclamar('Falha ao tentar manifestar nota fiscal: ' + vRetorno.mensagem_erro);
    Exit;
  end;

  // Obtendo o XML da nota
  vRetDownloadNFe := FServicos.BuscarNFeEntradas(Sessao.getEmpresaLogada.Cnpj, '', _Biblioteca.RetornaNumeros(vPreEntrada[0].ChaveAcessoNfe));
  if vRetDownloadNFe.HouveErro then begin
    Exclamar(vRetDownloadNFe.mensagem_erro);
    Exit;
  end;

  vRetBanco :=
    _PreEntradasNotasFiscais.AtualizarStatusAltisPreEntrada(
      Sessao.getConexaoBanco,
      pPreEntradaId,
      'MAN',
      vRetDownloadNFe.Dados[0].XmlNFe,
      ''
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  Result := True;
end;

function TComunicacaoNFe.ManifestarDesconhecimento(pPreEntradaId: Integer): Boolean;
var
  vRetBanco: RecRetornoBD;
  vRetorno: RecRespostaNFE;
  vPreEntrada: TArray<RecPreEntradasNotasFiscais>;
begin
  Result := False;

  vPreEntrada := _PreEntradasNotasFiscais.BuscarPreEntradasNotasFiscais(Sessao.getConexaoBanco, 0, [pPreEntradaId]);
  if vPreEntrada = nil then begin
    _Biblioteca.Exclamar('N�o foi encontrados os dados da pr�-entrada!');
    Exit;
  end;

  if not _Biblioteca.Perguntar('Deseja realmente desconhecer esta opera��o?') then
    Exit;

  vRetorno :=
    FServicos.manifestarOperacao(
      ttDesconhecimentoOperacao,
      Sessao.getNextSeq('SEQ_LOTE_EVENTOS_NFE_ID'),
      _Biblioteca.RetornaNumeros(vPreEntrada[0].ChaveAcessoNfe),
      Sessao.getEmpresaLogada.Cnpj,
      Sessao.getDataHora,
      ''
    );

  // Retorno 573 � nota ja com o evento realizado, ent�o apenas setando como manifetada no banco
  if vRetorno.HouveErro and (vRetorno.status <> '573') then begin
    _Biblioteca.Exclamar('Falha ao tentar realizar o desconhecimento da nota fiscal: ' + vRetorno.mensagem_erro);
    Exit;
  end;

  vRetBanco :=
    _PreEntradasNotasFiscais.AtualizarStatusAltisPreEntrada(
      Sessao.getConexaoBanco,
      pPreEntradaId,
      'DES',
      '',
      ''
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  Result := True;
end;

function TComunicacaoNFe.ManifestarOperacaoNaoRealizada(pPreEntradaId: Integer): Boolean;
var
  vRetBanco: RecRetornoBD;
  vRetorno: RecRespostaNFE;
  vPreEntrada: TArray<RecPreEntradasNotasFiscais>;

  vMotivo: string;
begin
  Result := False;

  vPreEntrada := _PreEntradasNotasFiscais.BuscarPreEntradasNotasFiscais(Sessao.getConexaoBanco, 0, [pPreEntradaId]);
  if vPreEntrada = nil then begin
    _Biblioteca.Exclamar('N�o foi encontrados os dados da pr�-entrada!');
    Exit;
  end;

  if not _Biblioteca.Perguntar('Deseja realmente definir a opera��o como n�o realizada?') then
    Exit;

  vMotivo := BuscaDados.BuscarDados('Justificativa', '');
  if vMotivo = '' then begin
    Exclamar('� necess�rio informar a justificativa para cancelamento da NF-e!');
    Exit;
  end;

  if Length(Trim(vMotivo)) < 15 then begin
    Exclamar('A justificativa para cancelamento da NF-e deve ter no m�nimo 15 caracteres!');
    Exit;
  end;

  vRetorno :=
    FServicos.manifestarOperacao(
      ttOperacaoNaoRealizada,
      Sessao.getNextSeq('SEQ_LOTE_EVENTOS_NFE_ID'),
      _Biblioteca.RetornaNumeros(vPreEntrada[0].ChaveAcessoNfe),
      Sessao.getEmpresaLogada.Cnpj,
      Sessao.getDataHora,
      vMotivo
    );

  // Retorno 573 � nota ja com o evento realizado, ent�o apenas setando como manifetada no banco
  if vRetorno.HouveErro and (vRetorno.status <> '573') then begin
    _Biblioteca.Exclamar('Falha ao tentar manifestar nota fiscal: ' + vRetorno.mensagem_erro);
    Exit;
  end;

  vRetBanco :=
    _PreEntradasNotasFiscais.AtualizarStatusAltisPreEntrada(
      Sessao.getConexaoBanco,
      pPreEntradaId,
      'ONR',
      '',
      vMotivo
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  Result := True;
end;

function TComunicacaoNFe.CancelarNFe(const pNotaFiscalId: Integer; const pJustificativa: string): Boolean;
var
  vRetorno: RecRespostaNFE;
  oNota: TArray<RecNotaFiscal>;
  vRetornoBanco: RecRetornoBD;
begin
  Result := False;
  oNota := _NotasFiscais.BuscarNotasFiscais(Sessao.getConexaoBanco, 0, [pNotaFiscalId]);

  if oNota = nil then begin
    _Biblioteca.Exclamar('N�o foram encontrado dados da nota fiscal a ser emitida!');
    Abort;
  end;

  vRetorno :=
    FServicos.CancelarNFe(
      Sessao.getNextSeq('SEQ_LOTE_EVENTOS_NFE_ID'),
      _Biblioteca.RetornaNumeros(oNota[0].chave_nfe),
      oNota[0].cnpj_emitente,
      Sessao.getDataHora,
      oNota[0].protocolo_nfe,
      pJustificativa
    );

  if vRetorno.HouveErro then begin
    _Biblioteca.Exclamar('Falha ao cancelar NFe: ' + vRetorno.mensagem_erro);
    Exit;
  end;

  vRetornoBanco :=
    _NotasFiscais.SetarNFeCancelada(
      Sessao.getConexaoBanco,
      pNotaFiscalId,
      vRetorno.protocolo.numero_protocolo,
      pJustificativa,
      vRetorno.protocolo.data_hora_recibo
    );

  if vRetornoBanco.TeveErro then begin
    _Biblioteca.Exclamar('ENTRE EM CONTATO COM A Hiva!!!' + sLineBreak + 'Falha setar situa��o da nota fiscal como cancelada: ' + vRetornoBanco.MensagemErro);
    Exit;
  end;

  Result := True;
end;

function TComunicacaoNFe.ConsultarStatusServico: RecRespostaNFE;
begin
  Result := FServicos.getStatusServico;
end;

function TComunicacaoNFe.ConsultarCTeDistribuicao: Boolean;
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  vRetCTe: RecRespostaCTe;
begin
  Result := False;

  vRetCTe := FServicos.BuscarCTeEntradas(Sessao.getEmpresaLogada.Cnpj, Sessao.getParametrosEmpresa.UltimoNsuConsultadoDistCTe);
  if vRetCTe.HouveErro then begin
    Exclamar(vRetCTe.MensagemErro);
    Exit;
  end;

  if StrToInt64Def(vRetCTe.UltimoNSU, 0) > 0 then begin
    Sessao.getConexaoBanco.IniciarTransacao;
    for i := Low(vRetCTe.CTEs) to High(vRetCTe.CTEs) do begin
      // Se n�o ter chave de acesso
      if vRetCTe.CTEs[i].ChaveCTe = '' then
        Continue;

      // Se for uma nota autorizada
      if vRetCTe.CTEs[i].StatusSefaz = '1' then begin
        vRetBanco :=
          _PreEntradasCte.AtualizarPreEntradasCte(
            Sessao.getConexaoBanco,
            0,
            Sessao.getEmpresaLogada.EmpresaId,
            vRetCTe.CTEs[i].CFOP,
            vRetCTe.CTEs[i].ChaveCTe,
            FormatarCNPJ(vRetCTe.CTEs[i].CNPJRemetente),
            vRetCTe.CTEs[i].RazaoSocialRemetente,
            vRetCTe.CTEs[i].InscricaoEstadualRemetente,
            FormatarCNPJ(vRetCTe.CTEs[i].CNPJEmitente),
            vRetCTe.CTEs[i].RazaoSocialEmitente,
            vRetCTe.CTEs[i].InscricaoEstadualEmitente,
            vRetCTe.CTEs[i].DataEmissao,
            vRetCTe.CTEs[i].NumeroCTe,
            vRetCTe.CTEs[i].SerieCTe,
            vRetCTe.CTEs[i].ValorTotalCTe,
            vRetCTe.CTEs[i].BaseCalculoICMS,
            vRetCTe.CTEs[i].PercentualICMS,
            vRetCTe.CTEs[i].ValorICMS,
            vRetCTe.CTEs[i].XmlCTe,
            vRetCTe.CTEs[i].NFEsReferenciadas,
            True
          );
      end
      // Se for uma nota fiscal cancelada
      else if vRetCTe.CTEs[i].StatusSefaz = '3' then begin
        vRetBanco :=
          _PreEntradasCte.AtualizarStatuCTeSefaz(
            Sessao.getConexaoBanco,
            vRetCTe.CTEs[i].ChaveCTe,
            '3',
            True
          );
      end;

      Sessao.AbortarSeHouveErro(vRetBanco);
    end;

    vRetBanco := _ParametrosEmpresa.AtualizarUltimoNSUDistribuicaoNFeCTe(Sessao.getConexaoBanco, Sessao.getEmpresaLogada.EmpresaId, vRetCTe.UltimoNSU, 'C', True);
    Sessao.AbortarSeHouveErro(vRetBanco);

    Sessao.getConexaoBanco.FinalizarTransacao;

    Result := True;
    Sessao.AtualizarParametrosEmpresasBanco;
  end;
end;

function TComunicacaoNFe.ConsultarNFeDistribuicao: Boolean;
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  vRetNFe: RecRespostaNFe<RecResumoNFe>;
begin
  Result := False;

  vRetNFe := FServicos.BuscarNFeEntradas(Sessao.getEmpresaLogada.Cnpj, Sessao.getParametrosEmpresa.UltimoNsuConsultadoDistNFe, '');
  if vRetNFe.HouveErro then begin
    Exclamar(vRetNFe.mensagem_erro);
    Exit;
  end;

  if StrToInt64Def(vRetNFe.ultimo_nsu, 0) > 0 then begin
    Sessao.getConexaoBanco.IniciarTransacao;
    for i := Low(vRetNFe.Dados) to High(vRetNFe.Dados) do begin
      // Se n�o ter chave de acesso
      if vRetNFe.Dados[i].ChaveNFe = '' then
        Continue;

      vRetBanco.TeveErro := false;

      // Se for uma nota autorizada
      if vRetNFe.Dados[i].StatusNota = '1' then begin
        vRetBanco :=
          _PreEntradasNotasFiscais.AtualizarPreEntradasNotasFiscais(
            Sessao.getConexaoBanco,
            0,
            Sessao.getEmpresaLogada.EmpresaId,
            vRetNFe.Dados[i].ChaveNFe,
            FormatarCNPJ(vRetNFe.Dados[i].CNPJ),
            vRetNFe.Dados[i].RazaoSocial,
            vRetNFe.Dados[i].InscricaoEstadual,
            vRetNFe.Dados[i].DataEmissao,
            vRetNFe.Dados[i].NumeroNota,
            vRetNFe.Dados[i].SerieNota,
            vRetNFe.Dados[i].ValorNota,
            'AMA',
            'N',
            vRetNFe.Dados[i].StatusNota,
            True
          );
      end
      // Se for uma nota fiscal cancelada
      else if vRetNFe.Dados[i].StatusNota = '3' then begin
        vRetBanco :=
          _PreEntradasNotasFiscais.AtualizarStatusNotaPreEntrada(
            Sessao.getConexaoBanco,
            vRetNFe.Dados[i].ChaveNFe,
            vRetNFe.Dados[i].StatusNota,
            True
          );
      end;

      Sessao.AbortarSeHouveErro(vRetBanco);
    end;

    vRetBanco := _ParametrosEmpresa.AtualizarUltimoNSUDistribuicaoNFeCTe(Sessao.getConexaoBanco, Sessao.getEmpresaLogada.EmpresaId, vRetNFe.ultimo_nsu, 'N', True);
    Sessao.AbortarSeHouveErro(vRetBanco);

    Sessao.getConexaoBanco.FinalizarTransacao;

    Result := True;
    Sessao.AtualizarParametrosEmpresasBanco;
  end;
end;

end.
