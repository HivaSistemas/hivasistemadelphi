unit _BibliotecaNFE;

interface

uses
  Vcl.Forms, System.AnsiStrings, System.StrUtils, System.SysUtils, System.Classes, System.Math, System.Variants, libxml2, libxmlsec, libxslt, _Biblioteca,
  _RecordsEstoques, Xml.xmldom, Xml.XMLIntf, Xml.Win.msxmldom, Xml.XMLDoc, System.DateUtils, _Capicom, Winapi.ActiveX, _msxml2, JwaWinCrypt,
  Winapi.Windows, System.Win.ComObj, synacode, synautil, System.Zip, GZIPUtils;

type
  RecCapaNFe = record
    (* Dados do fornecedor *)
    RazaoSocial: string;
    NomeFantasia: string;
    Cnpj: string;
    InscricaoEstadual: string;
    Logradouro: string;
    Complemento: string;
    Bairro: string;
    Cidade: string;
    Cep: string;
    CodigoIBGeMunicipio: string;
    Estado: string;
    Telefone: string;
    SuperSimples: Boolean;
    NaturezaOperacao: String;

    BaseCalculoIcms: Double;
    NumeroNota: Integer;
    PesoBruto: Double;
    PesoLiquido: Double;
    BaseCalculoCofins: Double;
    ValorCofins: Double;
    ValorIpi: Double;
    BaseCalculoPis: Double;
    ValorPis: Double;
    BaseCalculoIcmsSt: Double;
    ValorIcmsSt: Double;
    ValorIcms: Double;
    ValorTotal: Double;
    ValorTotalProdutos: Double;
    ValorDesconto: Double;
    ValorOutrasDespesas: Double;
    ValorFrete: Double;
    ModeloNota: string;
    SerieNota: string;
    DataHoraEmissao: TDateTime;
    DataHoraSaida: TDateTime;
    ChaveNFe: string;
    TipoFrete: Integer;
    ModFrete: string;

    NomeDestinatario: string;
    CnpjDestinatario: string;
    InscEstadualDestinatario: string;
    InformacoesComplementares: string;

    XmlTexto: string;
  end;

  RecItemNFe = record
    ItemId: Integer;
    CfopId: string;

    CodigoOriginal: string;
    NomeProduto: string;
    NomeMarca: string;
    Unidade: string;
    OrigemProduto: Integer;
    CodigoNcm: string;
    Cest: string;
    ValorTotalOutrasDespesas: Double;
    ValorTotalDesconto: Double;
    Quantidade: Double;
    PrecoUnitario: Double;
    ValorTotal: Double;

    Cst: string;
    IndiceReducaoBaseIcms: Double;
    BaseCalculoIcms: Double;
    PercentualIcms: Double;
    ValorIcms: Double;

    IndiceReducaoBaseIcmsSt: Double;
    BaseCalculoIcmsSt: Double;
    PercentualIcmsSt: Double;
    ValorIcmsSt: Double;

    CstPis: string;
    BaseCalculoPis: Double;
    PercentualPis: Double;
    ValorPis: Double;

    CstCofins: string;
    BaseCalculoCofins: Double;
    PercentualCofins: Double;
    ValorCofins: Double;

    PercentualIpi: Double;
    ValorIpi: Double;

    ValorFrete: Double;
    Informacoes_adicionais: string;

    CodigoBarras: string;
    Iva: Double;
    PrecoPauta: Double;

    procedure Iniciar;
  end;

  RecFaturamentoNFe = record
    Documento: string;
    DataVencimento: TDateTime;
    Valor: Double;
    QuantParcelas: Integer;
    NumeroDaParcela: Integer;
  end;

  RecDadosNotaEntradaCTe = record
    NumeroNota: Integer;
    ModeloNota: string;
    SerieNota: string;
    DataEmissaoNota: TDateTime;
    PesoNota: Double;
    QuantidadeNota: Double;
    ValorTotalNota: Double;
  end;

  tpAmbienteNFE = (tpProducao = 1, tpHomologacao = 2);
  TTipoDocumento = (tpNFe, tpNFCe);

  tsServicosNFE = (
    tsNfeRecepcao,
    tsNfeRetRecepcao,
    tsNfeInutilizacao,
    tsNfeConsulta,
    tsNfeStatusServico,
    tsNfeCadastro,
    tsNfeEvento,
    tsNfeDistDFeInteresse,
    tsNfeDistDFeInteresseCTe,
    tsNfeAutorizacao,
    tsNfeRetAutorizacao,
    tsConsultaSituacao
  );

  tcTipoCampo = (tcStr, tcInt, tcInt64, tcDat, tcDatHor, tcEsp, tcDe2, tcDe3, tcDe4, tcDe6, tcDe10, tcHor,  tcDatCFe, tcHorCFe);

  {$M+}
  TCertificadoDigital = class(TObject)
  private
    FCNPJ: string;
    FNumeroSerie: string;
    FSenhaCertificado: string;
    FDataVencimento: TDateTime;
  public
    constructor Create(pNumeroSerieCertificado: string);
    function GetNumeroSerieCertificado: string;
    function GetCertificadoCapicom: ICertificate2;
  published
    property CNPJ: string read FCNPJ;
  end;

function getCodigoUFIBGE( const pUF: string ): Integer;
function PosLast(const SubStr, S: AnsiString): Integer;
function RetirarAcentos( const pTexto: string ): string;
function RetirarAcento( const pLetra: AnsiChar ): AnsiChar;
function ReverterFiltroTextoXML(aTexto: string): string;
function RetornarConteudoEntre(const Frase, Inicio, Fim: string): string;
function FormatoValorNFe(const pValor: Double; pQtdeCasasDecimais: Integer = 2): string;
function SepararDados( pTexto: AnsiString; pChave: String; pManterChave: Boolean = False ): AnsiString;
function ParseText( const pTexto: AnsiString; const pDecode: Boolean; const pIsUTF8: Boolean ): AnsiString;
function getURLWebService( const pUF: string; const pAmbienteNFE: tpAmbienteNFE; const pTipoServico: tsServicosNFE; pNFCe: Boolean = false): string;

function GerarDigito(var Digito: Integer; chave: string): Boolean;

function GerarChaveNFE(
  const pCodigoUFIBGE: Integer;
  const pNumeroSerie: Integer;
  const pNumeroNota: Integer;
  const pTipoEmissao: Integer;
  const pDataEmissao: TDateTime;
  const pCNPJ: string;
  const pTipoDocumento: TTipoDocumento;
  out pCodigoNota: Integer;
  out pDigitoVerificador: Integer
): string;

function AssinarXML(
  const pXML: AnsiString;
  const pCaminhoCertificado: AnsiString;
  const pSenhaCertificado: AnsiString
): string;

function AssinarMsXML(const pXML: string; pCertificado: ICertificate2): string;

procedure ValidarXML(const pXML: string; const pCaminhoSchemas: string; const pServicoNFe: tsServicosNFE);

function HouveErroRetornoNFE(const pCodigoRetorno: string): Boolean;

{  Valida��o e assinatura da NF-e }
procedure InitXmlSec;
procedure ShutDownXmlSec;
function SignFile(const Axml: PAnsiChar; const key_file: PAnsiChar; const senha: PAnsiChar): AnsiString;
function SignMemory(const Axml: PAnsiChar; const key_file: PAnsichar; const senha: PAnsiChar; Size: Integer; Ponteiro: Pointer): AnsiString;

function BuscarDadosNFeEntrada(
  const pCaminhoArquivo: string;
  var pCapaNota: RecCapaNFe;
  var pItensNota: TArray<RecItemNFe>;
  var pFaturamentos: TArray<RecFaturamentoNFe>
): Boolean;

function ValidarChaveNFe(pChaveNFe: string): Boolean;
function FDataHoraXML(pDataHoraEmissao: TDateTime): string;

function GetCodigoAmbiente(pAmbienteNFe: tpAmbienteNFE): string;

function GetURLQRCode(const pUF: string; const pAmbienteNFe: tpAmbienteNFE): string; overload;

function GetURLQRCode(
  const pUF: string;
  const pAmbienteNFe: tpAmbienteNFE;
  const pChaveNFe: string;
  const pCPFDestinatario: string;
  const pDataHoraEmissao: String;
  const pValorTotalNF: String;
  const pValorTotalICMS: String;
  const pDigestValue: string;
  const pCSC: string;
  const pIdToken: string
): string; overload;

function getNoXMLDocument(const pNome: string; pRoot: IXMLNode): IXMLNode;
function getXMLBase64(pTexto: AnsiString): string;
function trataTagTextoNFe(pTexto: string): string;

const
  CAPICOM_STORE_NAME = 'My';
  DSIGNS = 'xmlns:ds="http://www.w3.org/2000/09/xmldsig#"';

implementation

var
  PrivateKey: IPrivateKey;

const
 cDTD     = '<!DOCTYPE test [<!ATTLIST infNFe Id ID #IMPLIED>]>' ;
 cDTDCanc = '<!DOCTYPE test [<!ATTLIST infCanc Id ID #IMPLIED>]>' ;
 cDTDInut = '<!DOCTYPE test [<!ATTLIST infInut Id ID #IMPLIED>]>' ;
 cDTDDpec = '<!DOCTYPE test [<!ATTLIST infDPEC Id ID #IMPLIED>]>' ;
 cDTDCCe  = '<!DOCTYPE test [<!ATTLIST infEvento Id ID #IMPLIED>]>' ;
 cDTDEven = '<!DOCTYPE test [<!ATTLIST infEvento Id ID #IMPLIED>]>' ;

function FormatoValorNFe(const pValor: Double; pQtdeCasasDecimais: Integer = 2): string;
begin
  if pQtdeCasasDecimais = 2 then
    Result := StringReplace(FormatFloat('0.00', pValor), ',', '.', [rfReplaceAll])
  else if pQtdeCasasDecimais = 3 then
    Result := StringReplace(FormatFloat('0.000', pValor), ',', '.', [rfReplaceAll])
  else if pQtdeCasasDecimais = 4 then
    Result := StringReplace(FormatFloat('0.0000', pValor), ',', '.', [rfReplaceAll]);
end;

function getCodigoUFIBGE( const pUF: string ): Integer;
begin
  Result := 0;
  if pUF = 'RO' then
    Result := 11
  else if pUF = 'AC' then
    Result := 12
  else if pUF = 'AM' then
    Result := 13
  else if pUF = 'RR' then
    Result := 14
  else if pUF = 'PA' then
    Result := 15
  else if pUF = 'AP' then
    Result := 16
  else if pUF = 'TO' then
    Result := 17
  else if pUF = 'MA' then
    Result := 21
  else if pUF = 'PI' then
    Result := 22
  else if pUF = 'CE' then
    Result := 23
  else if pUF = 'RN' then
    Result := 24
  else if pUF = 'PB' then
    Result := 25
  else if pUF = 'PE' then
    Result := 26
  else if pUF = 'AL' then
    Result := 27
  else if pUF = 'SE' then
    Result := 28
  else if pUF = 'BA' then
    Result := 29
  else if pUF = 'MG' then
    Result := 31
  else if pUF = 'ES' then
    Result := 32
  else if pUF = 'RJ' then
    Result := 33
  else if pUF = 'SP' then
    Result := 35
  else if pUF = 'PR' then
    Result := 41
  else if pUF = 'SC' then
    Result := 42
  else if pUF = 'RS' then
    Result := 43
  else if pUF = 'MS' then
    Result := 50
  else if pUF = 'MT' then
    Result := 51
  else if pUF = 'GO' then
    Result := 52
  else if pUF = 'DF' then
    Result := 53;
end;

function getURLWebService( const pUF: string; const pAmbienteNFE: tpAmbienteNFE; const pTipoServico: tsServicosNFE; pNFCe: Boolean = false): string;
begin
  Result := '';
  if pTipoServico = tsNfeDistDFeInteresse then begin
    Result :=
      IfThen(
        pAmbienteNFE = tpProducao,
        'https://www1.nfe.fazenda.gov.br/NFeDistribuicaoDFe/NFeDistribuicaoDFe.asmx',
        'https://hom.nfe.fazenda.gov.br/NFeDistribuicaoDFe/NFeDistribuicaoDFe.asmx'
      );
  end
  else if pUF = '91' then begin
    Result :=
      IfThen(
        pAmbienteNFE = tpProducao,
        'https://www.nfe.fazenda.gov.br/NFeRecepcaoEvento4/NFeRecepcaoEvento4.asmx',
        'https://hom.nfe.fazenda.gov.br/NFeRecepcaoEvento4/NFeRecepcaoEvento4.asmx'
      );
  end
  else if pTipoServico = tsNfeDistDFeInteresseCTe then
    Result :=
      IfThen(
        pAmbienteNFE = tpProducao,
        'https://www1.cte.fazenda.gov.br/CTeDistribuicaoDFe/CTeDistribuicaoDFe.asmx',
        'https://hom.nfe.fazenda.gov.br/NFeRecepcaoEvento4/NFeRecepcaoEvento4.asmx'
      )
  else if pUF = 'RO' then begin

  end
  else if pUF = 'AC' then begin

  end
  else if pUF = 'AM' then begin

  end
  else if pUF = 'RR' then begin

  end
  else if pUF = 'PA' then begin

  end
  else if pUF = 'AP' then begin

  end
  else if pUF = 'TO' then begin

  end
  else if pUF = 'MA' then begin
    if not pNFCe then begin
      case pTipoServico of
        tsNfeRecepcao:
          Result :=
            IfThen(
              pAmbienteNFE = tpProducao,
              'https://www.sefazvirtual.fazenda.gov.br/NFeRecepcaoEvento4/NFeRecepcaoEvento4.asmx',
              'https://hom.sefazvirtual.fazenda.gov.br/NFeRecepcaoEvento4/NFeRecepcaoEvento4.asmx'
            );

        tsNfeAutorizacao:
          Result :=
            IfThen(
              pAmbienteNFE = tpProducao,
              'https://www.sefazvirtual.fazenda.gov.br/NFeAutorizacao4/NFeAutorizacao4.asmx',
              'https://hom.sefazvirtual.fazenda.gov.br/NFeAutorizacao4/NFeAutorizacao4.asmx'
            );

        tsNfeRetRecepcao:
          Result :=
            IfThen(
              pAmbienteNFE = tpProducao,
                '',
                '');

        tsNfeInutilizacao:
          Result :=
            IfThen(
              pAmbienteNFE = tpProducao,
              'https://www.sefazvirtual.fazenda.gov.br/NFeInutilizacao4/NFeInutilizacao4.asmx',
              'https://hom.sefazvirtual.fazenda.gov.br/NFeInutilizacao4/NFeInutilizacao4.asmx'
            );

        tsNfeConsulta: Result := IfThen( pAmbienteNFE = tpProducao, '', '' );

        tsNfeStatusServico:
          Result :=
            IfThen(
              pAmbienteNFE = tpProducao,
              'https://www.sefazvirtual.fazenda.gov.br/NFeStatusServico4/NFeStatusServico4.asmx',
              'https://hom.sefazvirtual.fazenda.gov.br/NFeStatusServico4/NFeStatusServico4.asmx'
            );

        tsNfeRetAutorizacao:
          Result :=
            IfThen(
              pAmbienteNFE = tpProducao,
              'https://www.sefazvirtual.fazenda.gov.br/NFeStatusServico4/NFeStatusServico4.asmx',
              'https://hom.sefazvirtual.fazenda.gov.br/NFeRetAutorizacao4/NFeRetAutorizacao4.asmx'
            );

        tsConsultaSituacao:
          Result :=
            IfThen(
              pAmbienteNFE = tpProducao,
              'https://www.sefazvirtual.fazenda.gov.br/NFeConsultaProtocolo4/NFeConsultaProtocolo4.asmx',
              'https://hom.sefazvirtual.fazenda.gov.br/NFeConsultaProtocolo4/NFeConsultaProtocolo4.asmx'
            );

        tsNfeCadastro:
          Result :=
            IfThen(
              pAmbienteNFE = tpProducao,
              '',
              ''
            );

        tsNfeEvento:
          Result :=
            IfThen(
              pAmbienteNFE = tpProducao,
              'https://www.sefazvirtual.fazenda.gov.br/NFeRecepcaoEvento4/NFeRecepcaoEvento4.asmx',
              'https://hom.sefazvirtual.fazenda.gov.br/NFeRecepcaoEvento4/NFeRecepcaoEvento4.asmx'
            );
      end;
    end
    else begin
      case pTipoServico of
        tsNfeRecepcao:
          Result :=
            IfThen(
              pAmbienteNFE = tpProducao,
              'https://nfce.svrs.rs.gov.br/ws/recepcaoevento/recepcaoevento4.asmx',
              'https://nfce-homologacao.svrs.rs.gov.br/ws/recepcaoevento/recepcaoevento4.asmx'
            );

        tsNfeAutorizacao:
          Result :=
            IfThen(
              pAmbienteNFE = tpProducao,
              'https://nfce.svrs.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao4.asmx',
              'https://nfce-homologacao.svrs.rs.gov.br/ws/NfeAutorizacao/NFeAutorizacao4.asmx'
            );

        tsNfeRetRecepcao:
          Result :=
            IfThen(
              pAmbienteNFE = tpProducao,
                '',
                '');

        tsNfeInutilizacao:
          Result :=
            IfThen(
              pAmbienteNFE = tpProducao,
              'https://nfce.svrs.rs.gov.br/ws/nfeinutilizacao/nfeinutilizacao4.asmx',
              'https://nfce-homologacao.svrs.rs.gov.br/ws/nfeinutilizacao/nfeinutilizacao4.asmx'
            );

        tsNfeConsulta: Result := IfThen( pAmbienteNFE = tpProducao, '', '' );

        tsNfeStatusServico:
          Result :=
            IfThen(
              pAmbienteNFE = tpProducao,
              'https://nfce.svrs.rs.gov.br/ws/NfeStatusServico/NfeStatusServico4.asmx',
              'https://hom.sefazvirtual.fazenda.gov.br/NFeStatusServico4/NFeStatusServico4.asmx'
            );

        tsNfeRetAutorizacao:
          Result :=
            IfThen(
              pAmbienteNFE = tpProducao,
              'https://nfce.svrs.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao4.asmx',
              'https://nfce-homologacao.svrs.rs.gov.br/ws/NfeRetAutorizacao/NFeRetAutorizacao4.asmx'
            );

        tsConsultaSituacao:
          Result :=
            IfThen(
              pAmbienteNFE = tpProducao,
              'https://nfce.svrs.rs.gov.br/ws/NfeConsulta/NfeConsulta4.asmx',
              'https://nfce-homologacao.svrs.rs.gov.br/ws/NfeConsulta/NfeConsulta4.asmx'
            );

        tsNfeCadastro:
          Result :=
            IfThen(
              pAmbienteNFE = tpProducao,
              '',
              ''
            );

        tsNfeEvento:
          Result :=
            IfThen(
              pAmbienteNFE = tpProducao,
              'https://www.sefazvirtual.fazenda.gov.br/NFeRecepcaoEvento4/NFeRecepcaoEvento4.asmx',
              'https://hom.sefazvirtual.fazenda.gov.br/NFeRecepcaoEvento4/NFeRecepcaoEvento4.asmx'
            );
      end;
    end;

  end
  else if pUF = 'PI' then begin

  end
  else if pUF = 'CE' then begin

  end
  else if pUF = 'RN' then begin

  end
  else if pUF = 'PB' then begin

  end
  else if pUF = 'PE' then begin

  end
  else if pUF = 'AL' then begin

  end
  else if pUF = 'SE' then begin

  end
  else if pUF = 'BA' then begin

  end
  else if pUF = 'MG' then begin

  end
  else if pUF = 'ES' then begin

  end
  else if pUF = 'RJ' then begin

  end
  else if pUF = 'SP' then begin

  end
  else if pUF = 'PR' then begin

  end
  else if pUF = 'SC' then begin

  end
  else if pUF = 'RS' then begin

  end
  else if pUF = 'MS' then begin

  end
  else if pUF = 'MT' then begin

  end
  else if pUF = 'GO' then begin
    case pTipoServico of
      tsNfeRecepcao:
        Result :=
          IfThen(
            pAmbienteNFE = tpProducao,
            'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeRecepcao2',
            'https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeRecepcao2'
          );

      tsNfeAutorizacao:
        Result :=
          IfThen(
            pAmbienteNFE = tpProducao,
            'https://nfe.sefaz.go.gov.br/nfe/services/NFeAutorizacao4',
            'https://homolog.sefaz.go.gov.br/nfe/services/NFeAutorizacao4'
          );

      tsNfeRetRecepcao   : Result := IfThen( pAmbienteNFE = tpProducao, 'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeRetRecepcao2', 'https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeRetRecepcao2' );

      tsNfeInutilizacao:
        Result :=
          IfThen(
            pAmbienteNFE = tpProducao,
            'https://nfe.sefaz.go.gov.br/nfe/services/NFeInutilizacao4',
            'https://homolog.sefaz.go.gov.br/nfe/services/NFeInutilizacao4'
          );

      tsNfeConsulta      : Result := IfThen( pAmbienteNFE = tpProducao, 'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeConsulta2', 'https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeConsulta2' );

      tsNfeStatusServico:
        Result :=
          IfThen(
            pAmbienteNFE = tpProducao,
            'https://nfe.sefaz.go.gov.br/nfe/services/NFeStatusServico4',
            'https://homolog.sefaz.go.gov.br/nfe/services/NFeStatusServico4'
          );

      tsNfeRetAutorizacao:
        Result :=
          IfThen(
            pAmbienteNFE = tpProducao,
            'https://nfe.sefaz.go.gov.br/nfe/services/v2/NfeRetAutorizacao',
            'https://homolog.sefaz.go.gov.br/nfe/services/v2/NfeRetAutorizacao'
          );

      tsConsultaSituacao:
        Result :=
          IfThen(
            pAmbienteNFE = tpProducao,
            'https://nfe.sefaz.go.gov.br/nfe/services/NFeConsultaProtocolo4',
            'https://homolog.sefaz.go.gov.br/nfe/services/NFeConsultaProtocolo4'
          );

      tsNfeCadastro:
        Result :=
          IfThen(
            pAmbienteNFE = tpProducao,
            'https://nfe.sefaz.go.gov.br/nfe/services/v2/CadConsultaCadastro2',
            'https://homolog.sefaz.go.gov.br/nfe/services/v2/CadConsultaCadastro2'
          );

      tsNfeEvento:
        Result :=
          IfThen(
            pAmbienteNFE = tpProducao,
            'https://nfe.sefaz.go.gov.br/nfe/services/NFeRecepcaoEvento4',
            'https://homolog.sefaz.go.gov.br/nfe/services/NFeRecepcaoEvento4'
          );
    end;
  end
  else if pUF = 'DF' then begin

  end;
end;

function ParseText( const pTexto: AnsiString; const pDecode: Boolean; const pIsUTF8: Boolean ) : AnsiString;
begin
  Result := '';
  if pDecode then
    begin
      Result := AnsiString(StringReplace(String(pTexto), '&amp;', '&', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&lt;', '<', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&gt;', '>', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&quot;', '"', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&#39;', #39, [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&aacute;', '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&Aacute;', '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&acirc;' , '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&Acirc;' , '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&atilde;', '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&Atilde;', '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&agrave;', '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&Agrave;', '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&eacute;', '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&Eacute;', '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&ecirc;' , '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&Ecirc;' , '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&iacute;', '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&Iacute;', '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&oacute;', '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&Oacute;', '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&otilde;', '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&Otilde;', '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&ocirc;' , '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&Ocirc;' , '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&uacute;', '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&Uacute;', '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&uuml;'  , '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&Uuml;'  , '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&ccedil;', '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&Ccedil;', '�', [rfReplaceAll]));
      Result := AnsiString(StringReplace(String(pTexto), '&apos;', '''', [rfReplaceAll]));
      if pIsUTF8 then
        Result := AnsiString(UTF8ToString(pTexto));
    end
  else begin
    Result := AnsiString(StringReplace(String(pTexto), '&', '&amp;', [rfReplaceAll]));
    Result := AnsiString(StringReplace(String(pTexto), '<', '&lt;', [rfReplaceAll]));
    Result := AnsiString(StringReplace(String(pTexto), '>', '&gt;', [rfReplaceAll]));
    Result := AnsiString(StringReplace(String(pTexto), '"', '&quot;', [rfReplaceAll]));
    Result := AnsiString(StringReplace(String(pTexto), #39, '&#39;', [rfReplaceAll]));
    if pIsUTF8 then
      Result := AnsiString(UTF8ToString(pTexto));
  end;
end;

function RetirarAcento( const pLetra: AnsiChar ): AnsiChar;
begin
  case pLetra of
    '�','�','�','�','�' : Result := 'a' ;
    '�','�','�','�','�' : Result := 'A' ;
    '�','�',    '�','�' : Result := 'e' ;
    '�','�',    '�','�' : Result := 'E' ;
    '�','�',    '�','�' : Result := 'i' ;
    '�','�',    '�','�' : Result := 'I' ;
    '�','�','�','�','�' : Result := 'o' ;
    '�','�','�','�','�' : Result := 'O' ;
    '�','�',    '�','�' : Result := 'u' ;
    '�','�',    '�','�' : Result := 'U' ;
    '�'                 : Result := 'c' ;
    '�'                 : Result := 'C' ;
    '�'                 : Result := 'n' ;
    '�'                 : Result := 'N' ;
  else
    Result := pLetra ;
  end;
end ;

function RetirarAcentos( const pTexto: string ): string;
Var
  i: Integer;
  letra: AnsiChar;
  ret: AnsiString;
  ansiStr: AnsiString;
begin
  Result  := '' ;
  Ret     := '' ;
  AnsiStr := AnsiString( pTexto );
  for i := 1 to Length( AnsiStr ) do begin
     Letra := RetirarAcento( AnsiStr[i] ) ;
     if not (Letra in [#32..#126,#13,#10,#8]) then    {Letras / numeros / pontos / sinais}
        Letra := ' ' ;
     Ret := Ret + Letra ;
  end;

  Result := string(Ret)
end;

function SepararDados( pTexto: AnsiString; pChave: String; pManterChave: Boolean = False ): AnsiString;
var
  PosIni: Integer;
  PosFim: Integer;
begin
  if pManterChave then begin
    PosIni := Pos( string(pChave), string(pTexto) ) - 1;
    PosFim := Pos( '/' + string(pChave), string(pTexto)) + Length(string(pChave) ) + 3;

    if (PosIni = 0) or (PosFim = 0) then begin
      PosIni := Pos( 'ns2:' + string(pChave),string(pTexto))-1;
      PosFim := Pos( '/ns2:' + string(pChave),string(pTexto))+Length(string(pChave))+3;
    end;
  end
  else begin
    PosIni := Pos( string(pChave), string(pTexto)) + Pos('>', Copy(string(pTexto), Pos(string(pChave), string(pTexto)), Length(string(pTexto))) );
    PosFim := Pos( '/' + string(pChave), string(pTexto) );

    if (PosIni = 0) or (PosFim = 0) then begin
      PosIni := Pos( 'ns2:' + string(pChave),string(pTexto)) + Pos( '>',Copy(string(pTexto),Pos('ns2:'+string(pChave), string(pTexto)), Length(string(pTexto))) );
      PosFim := Pos( '/ns2:' + string(pChave),string(pTexto) );
    end;
  end;
  Result := AnsiString( Copy(string(pTexto), PosIni, PosFim - (PosIni + 1)) );
end;

function HexToAscii(Texto: string): String;
var
  i: Integer;

  function HexToInt(Hex: string): integer;
  begin
    Result := StrToInt('$' + Hex);
  end;
begin
  result := '';
  for i := 1 to Length(texto) do begin
    if i mod 2 <> 0 then
       result := result + chr(HexToInt(copy(texto,i,2)));
  end;
end;

function RetornarConteudoEntre(const Frase, Inicio, Fim: string): string;
var
  i: Integer;
  s: string;
begin
  result := '';
  i := pos(Inicio, Frase);

  if i = 0 then
    Exit;

  s := Copy(Frase, i + length(Inicio), maxInt);
  result := Copy(s, 1, pos(Fim, s) - 1);
end;

function IdentificarSchema(const AXML: AnsiString; var I: integer): Integer;
var
  tipo_evento: string;
begin
  i := Pos('<infNFe', string(AXML));
  Result := 1;
  if i = 0  then begin
    i := Pos('<infCanc', string(AXML));
    if i > 0 then
      Result := 2
    else begin
      i := Pos('<infInut', string(AXML));
      if i > 0 then
         Result := 3
      else begin
        i := Pos('<infEvento', string(AXML));
        if i > 0 then begin
          tipo_evento := Trim(RetornarConteudoEntre(string(AXML),'<tpEvento>','</tpEvento>'));
          if tipo_evento = '110111' then
            Result := 6 // Cancelamento
          else if tipo_evento = '210200' then
            Result := 7 //Manif. Destinatario: Confirma��o da Opera��o
          else if tipo_evento = '210210' then
            Result := 8 //Manif. Destinatario: Ci�ncia da Opera��o Realizada
          else if tipo_evento = '210220' then
            Result := 9 //Manif. Destinatario: Desconhecimento da Opera��o
          else if tipo_evento = '210240' then
            Result := 10 //Manif. Destinatario: Opera��o n�o Realizada
          else
            Result := 5; //Carta de Corre��o Eletr�nica
        end
        else
          Result := 4; //DPEC
      end;
    end;
  end;
end;

function ReverterFiltroTextoXML(aTexto: string): string;
var
  p1: Integer;
  p2: Integer;
  vHex: string;
  vStr: string;
begin
  aTexto := StringReplace(aTexto, '&amp;', '&', [rfReplaceAll]);
  aTexto := StringReplace(aTexto, '&lt;', '<', [rfReplaceAll]);
  aTexto := StringReplace(aTexto, '&gt;', '>', [rfReplaceAll]);
  aTexto := StringReplace(aTexto, '&quot;', '"', [rfReplaceAll]);
  aTexto := StringReplace(aTexto, '&#39;', #39, [rfReplaceAll]);
  p1 := Pos('&#x', string(aTexto));
  while p1>0 do begin
    for p2:=p1 to Length(aTexto) do begin
      if aTexto[p2] = ';' then
        Break;
    end;
    vHex := Copy(aTexto, p1, p2 - p1 + 1);
    vStr := StringReplace(vHex,'&#x','',[rfReplaceAll]);
    vStr := StringReplace(vStr,';','',[rfReplaceAll]);
    vStr := HexToAscii(vStr);
    aTexto := StringReplace(aTexto, vHex, vStr,[rfReplaceAll]);
    p1 := Pos('&#x', aTexto);
  end;
  result := Trim(aTexto);
end;

function AssinarXML(
  const pXML: AnsiString;
  const pCaminhoCertificado: AnsiString;
  const pSenhaCertificado: AnsiString
): string;
var
  i: Integer;
  j: Integer;
  posic_inicial: Integer;
  Posic_final: Integer;

  uri: AnsiString;
  auxiliar: AnsiString;
  xml_assinado: AnsiString;

  tipo: Integer; // 1 - NFE 2 - Cancelamento 3 - Inutilizacao
  certificado: TMemoryStream;
  certificado_2: TStringStream;
begin
  Result := '';
  auxiliar := pXML;

  //// Encontrando o uri ////
  tipo := IdentificarSchema(auxiliar, i);

  i := PosEx('Id=', string(auxiliar), i + 6) ;
  if i = 0 then
    Exit;

  i := PosEx('"', string(auxiliar), i + 2) ;
  if i = 0 then
    Exit;

  j := PosEx('"', string(auxiliar), i + 1) ;
  if j = 0 then
    Exit;

  uri := Copy(auxiliar, i + 1, j - i -1) ;

  //// Adicionando Cabe�alho DTD, necess�rio para xmlsec encontrar o ID ////
  i := Pos('?>', string(auxiliar));

  case tipo of
    1: auxiliar     := Copy( auxiliar, 1, StrToInt(VarToStr(IfThen(i > 0, i + 1, i))) ) + cDTD      + Copy( auxiliar, StrToInt(VarToStr(IfThen(i > 0, i + 2, i))), Length(auxiliar) );
    2: auxiliar     := Copy( auxiliar, 1, StrToInt(VarToStr(IfThen(i > 0, i + 1, i))) ) + cDTDCanc  + Copy( auxiliar, StrToInt(VarToStr(IfThen(i > 0, i + 2, i))), Length(auxiliar) );
    3: auxiliar     := Copy( auxiliar, 1, StrToInt(VarToStr(IfThen(i > 0, i + 1, i))) ) + cDTDInut  + Copy( auxiliar, StrToInt(VarToStr(IfThen(i > 0, i + 2, i))), Length(auxiliar) );
    4: auxiliar     := Copy( auxiliar, 1, StrToInt(VarToStr(IfThen(i > 0, i + 1, i))) ) + cDTDDpec  + Copy( auxiliar, StrToInt(VarToStr(IfThen(i > 0, i + 2, i))), Length(auxiliar) );
    5: auxiliar     := Copy( auxiliar, 1, StrToInt(VarToStr(IfThen(i > 0, i + 1, i))) ) + cDTDCCe   + Copy( auxiliar, StrToInt(VarToStr(IfThen(i > 0, i + 2, i))), Length(auxiliar) );
    6..10: auxiliar := Copy( auxiliar, 1, StrToInt(VarToStr(IfThen(i > 0, i + 1, i))) ) + cDTDEven  + Copy( auxiliar, StrToInt(VarToStr(IfThen(i > 0, i + 2, i))), Length(auxiliar) );
  else
    auxiliar := '';
  end;

  //// Inserindo Template da Assinatura digital ////
  case tipo of
    1: begin
      i := Pos('</NFe>', string(auxiliar));
      if i = 0 then
        raise Exception.Create('N�o foi encontado o final do XML: </NFe>') ;
    end;

    2: begin
      i := Pos('</cancNFe>', string(auxiliar));
      if i = 0 then
        raise Exception.Create('N�o foi encontrado o final do XML: </cancNFe>') ;
    end;

    3: begin
      i := pos('</inutNFe>', string(auxiliar));
      if i = 0 then
        raise Exception.Create('N�o foi encontrado o final do XML: </inutNFe>') ;
    end;

    4: begin
      i := pos('</envDPEC>',string(auxiliar));
      if i = 0 then
        raise Exception.Create('N�o foi encontrado o final do XML: </envDPEC>') ;
    end;

    5..10: begin
      i := pos('</evento>',string(auxiliar));
      if i = 0 then
        raise Exception.Create('N�o foi encontrado o final do XML: </evento>') ;
    end;
  else
    raise Exception.Create('Template de Tipo n�o implementado.') ;
    Exit;
  end;

  auxiliar := Copy(auxiliar, 1, i - 1) +
    '<Signature xmlns="http://www.w3.org/2000/09/xmldsig#">'+
      '<SignedInfo>'+
        '<CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"/>'+
        '<SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1" />'+
        '<Reference URI="#' + uri + '">'+
          '<Transforms>'+
            '<Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature" />'+
            '<Transform Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" />'+
          '</Transforms>'+
          '<DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1" />'+
          '<DigestValue></DigestValue>'+
        '</Reference>'+
      '</SignedInfo>'+
      '<SignatureValue></SignatureValue>'+
      '<KeyInfo>'+
        '<X509Data>'+
          '<X509Certificate></X509Certificate>'+
        '</X509Data>'+
      '</KeyInfo>'+
    '</Signature>';

  case tipo of
    1: auxiliar := auxiliar + '</NFe>';
    2: auxiliar := auxiliar + '</cancNFe>';
    3: auxiliar := auxiliar + '</inutNFe>';
    4: auxiliar := auxiliar + '</envDPEC>';
    5..10: auxiliar := auxiliar + '</evento>';
  else
    auxiliar := '';
  end;

  if FileExists(string(pCaminhoCertificado)) then
    xml_assinado := SignFile(pAnsiChar(auxiliar), PAnsiChar(pCaminhoCertificado), PAnsiChar(pSenhaCertificado))
  else begin
    certificado := TMemoryStream.Create;
    certificado_2 := TStringStream.Create(pCaminhoCertificado);
    try
      certificado.LoadFromStream(certificado_2);
      xml_assinado := SignMemory(PAnsiChar(auxiliar), PAnsiChar(pCaminhoCertificado), PAnsiChar(pSenhaCertificado), certificado.Size, certificado.Memory) ;
    finally
      certificado_2.Free;
      certificado.Free;
    end;
  end;

  // Removendo quebras de linha //
  xml_assinado := AnsiString(StringReplace( string(xml_assinado), #10, '', [rfReplaceAll]));
  xml_assinado := AnsiString(StringReplace( string(xml_assinado), #13, '', [rfReplaceAll]));

  // Removendo DTD //
  case tipo of
    1: xml_assinado := AnsiString(StringReplace(string(xml_assinado), cDTD, '', []));
    2: xml_assinado := AnsiString(StringReplace( string(xml_assinado), cDTDCanc, '', []));
    3: xml_assinado := AnsiString(StringReplace( string(xml_assinado), cDTDInut, '', []));
    4: xml_assinado := AnsiString(StringReplace( string(xml_assinado), cDTDDpec, '', [] ));
    5: xml_assinado := AnsiString(StringReplace( string(xml_assinado), cDTDCCe, '', [] ));
    6..10: xml_assinado := AnsiString(StringReplace( string(xml_assinado), cDTDEven, '', []));
  else
    xml_assinado := '';
  end;

  posic_inicial := Pos('<X509Certificate>', string(xml_assinado)) - 1;
  Posic_final := PosLast('<X509Certificate>',xml_assinado);

  Result := Copy(string(xml_assinado), 1, posic_inicial) + Copy(string(xml_assinado), Posic_final, Length(xml_assinado));
end;

function AssinarMsXML(
  const pXML: string;
  pCertificado: ICertificate2
): string;
var
  i: Integer;
  j: Integer;
  vPosicInicial: Integer;
  vPosicFinal: Integer;

  vUri: string;
  vAuxiliar: string;
  vXmlAassinado: string;

  vTipo: Integer; // 1 - NFE 2 - Cancelamento 3 - Inutilizacao

  xmlHeaderAntes: string;
  xmlHeaderDepois: string;

  xmlDoc: IXMLDOMDocument3;
  xmldsig: IXMLDigitalSignature;
  dsigKey   : IXMLDSigKey;
  signedKey : IXMLDSigKey;

  Cert: ICertificate2;
  CertStore: IStore3;
  Certs: ICertificates2;
  CertStoreMem: IStore3;
begin
  CoInitialize(nil);
  try
    Result := '';
    vAuxiliar := pXML;

    //// Encontrando o vUri ////
    vTipo := IdentificarSchema(AnsiString(vAuxiliar), i);

    i := PosEx('Id=', vAuxiliar, i + 6) ;
    if i = 0 then
      Exit;

    i := PosEx('"', vAuxiliar, i + 2) ;
    if i = 0 then
      Exit;

    j := PosEx('"', vAuxiliar, i + 1) ;
    if j = 0 then
      Exit;

    vUri := Copy(vAuxiliar, i + 1, j - i -1) ;

    //// Adicionando Cabe�alho DTD, necess�rio para xmlsec encontrar o ID ////
    i := Pos('?>', string(vAuxiliar));

    case vTipo of
      1: vAuxiliar := Copy(vAuxiliar, 1, Pos('</NFe>', vAuxiliar) - 1);
      2: vAuxiliar := Copy(vAuxiliar, 1, Pos('</cancNFe>', vAuxiliar) - 1);
      3: vAuxiliar := Copy(vAuxiliar, 1, Pos('</inutNFe>', vAuxiliar) - 1);
      4: vAuxiliar := Copy(vAuxiliar, 1, Pos('</envDPEC>', vAuxiliar) - 1);
      5..11: vAuxiliar := Copy(vAuxiliar, 1, Pos('</evento>', vAuxiliar) - 1);
    else
      vAuxiliar := '';
    end;

    vAuxiliar := vAuxiliar +
      '<Signature xmlns="http://www.w3.org/2000/09/xmldsig#">' +
        '<SignedInfo>' +
          '<CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"/>' +
          '<SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1" />' +
          '<Reference URI="#' + vURI + '">' +
            '<Transforms>' +
              '<Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature" /><Transform Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" />' +
            '</Transforms>' +
            '<DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1" />' +
            '<DigestValue></DigestValue>' +
          '</Reference>' +
        '</SignedInfo>' +
        '<SignatureValue></SignatureValue>' +
        '<KeyInfo></KeyInfo>' +
      '</Signature>';

    case vTipo of
      1: vAuxiliar := vAuxiliar + '</NFe>';
      2: vAuxiliar := vAuxiliar + '</cancNFe>';
      3: vAuxiliar := vAuxiliar + '</inutNFe>';
      4: vAuxiliar := vAuxiliar + '</envDPEC>';
      5..11: vAuxiliar := vAuxiliar + '</evento>';
    else
      vAuxiliar := '';
    end;

    // Lendo Header antes de assinar //
    xmlHeaderAntes := '';
    i := Pos('?>', vAuxiliar);
    if i > 0 then
      xmlHeaderAntes := copy(vAuxiliar,1,I+1) ;

    xmldoc := CoDOMDocument50.Create;

    xmldoc.async              := False;
    xmldoc.validateOnParse    := False;
    xmldoc.preserveWhiteSpace := True;

    xmldsig := CoMXDigitalSignature50.Create;

    if (not xmldoc.loadXML(vAuxiliar) ) then
      raise Exception.Create('N�o foi poss�vel carregar o arquivo: ' + vAuxiliar + '!');

    xmldoc.setProperty('SelectionNamespaces', DSIGNS);
    xmldsig.signature := xmldoc.selectSingleNode('.//ds:Signature');

    if (xmldsig.signature = nil) then
      raise Exception.Create('� preciso carregar o template antes de assinar.');

    CertStore := CoStore.Create;
    CertStore.Open(CAPICOM_CURRENT_USER_STORE, 'My', CAPICOM_STORE_OPEN_READ_ONLY);

    CertStoreMem := CoStore.Create;
    CertStoreMem.Open(CAPICOM_MEMORY_STORE, 'Memoria', CAPICOM_STORE_OPEN_READ_ONLY);

    Cert := nil;
    Certs := CertStore.Certificates as ICertificates2;
    for i := 1 to Certs.Count do begin
      Cert := IInterface(Certs.Item[i]) as ICertificate2;
      if Cert.SerialNumber = pCertificado.SerialNumber then begin
        CertStoreMem.Add(Cert);
        Break;
      end;
    end;

    if Cert = nil then
      raise Exception.Create('Certificado digital n�o encontrado!');

    OleCheck(IDispatch(pCertificado.PrivateKey).QueryInterface(IPrivateKey, PrivateKey));
    xmldsig.store := CertStoreMem;

    dsigKey := xmldsig.createKeyFromCSP(PrivateKey.ProviderType, PrivateKey.ProviderName, PrivateKey.ContainerName, 0);
    if (dsigKey = nil) then
      raise Exception.Create('Erro ao criar a chave do CSP!');

    signedKey := xmldsig.sign(dsigKey, $00000002);
    if (signedKey <> nil) then begin
      vXmlAassinado := xmldoc.xml;
      vXmlAassinado := StringReplace( vXmlAassinado, #10, '', [rfReplaceAll] ) ;
      vXmlAassinado := StringReplace( vXmlAassinado, #13, '', [rfReplaceAll] ) ;

      vPosicInicial := Pos('<SignatureValue>',vXmlAassinado)+length('<SignatureValue>');
      vXmlAassinado := Copy(vXmlAassinado, 1, vPosicInicial-1) + StringReplace( Copy(vXmlAassinado, vPosicInicial, Length(vXmlAassinado)), ' ', '', [rfReplaceAll] ) ;
      vPosicInicial := Pos('<X509Certificate>',vXmlAassinado)-1;
      vPosicFinal   := PosLast('<X509Certificate>', AnsiString(vXmlAassinado));

      vXmlAassinado := Copy( vXmlAassinado, 1, vPosicInicial) + Copy(vXmlAassinado, vPosicFinal, Length(vXmlAassinado) );
    end
    else
      raise Exception.Create('Falha ao assinar NF-e!');

    if xmlHeaderAntes <> '' then begin
      i := pos('?>',vXmlAassinado) ;
      if i > 0 then begin
        xmlHeaderDepois := copy(vXmlAassinado,1,I+1) ;
        if xmlHeaderAntes <> xmlHeaderDepois then
          vXmlAassinado := StuffString(vXmlAassinado,1,length(xmlHeaderDepois),xmlHeaderAntes) ;
      end
      else
        vXmlAassinado := xmlHeaderAntes + vXmlAassinado ;
    end;

    Result := vXmlAassinado;
  finally
    xmldsig   := nil;
    dsigKey   := nil;
    signedKey := nil;
    xmldoc    := nil;

    CoUninitialize;
  end;
end;

procedure ValidarXML(const pXML: string; const pCaminhoSchemas: string; const pServicoNFe: tsServicosNFE);
var
  schema_xml: string;
  schema: xmlSchemaPtr;
  schemError: xmlErrorPtr;
  doc, schema_doc: xmlDocPtr;
  valid_ctxt: xmlSchemaValidCtxtPtr;
  parser_ctxt: xmlSchemaParserCtxtPtr;

  procedure GerarErro(pMensagemErro: string);
  begin
    raise Exception.Create(pMensagemErro);
  end;

begin
  //tipo := IdentificarSchema(pXML, i);

  if not DirectoryExists(ExtractFilePath(pCaminhoSchemas)) then
    raise Exception.Create('N�o foi encontrado diret�rio dos schemas de valida��o do xml da NF-e!');

  case pServicoNFe of
    tsNfeRecepcao      : schema_xml := pCaminhoSchemas + 'enviNFe_v4.00.xsd';
    tsNfeRetRecepcao   : schema_xml := pCaminhoSchemas + 'consReciNFe_v4.00.xsd';
    tsNfeInutilizacao  : schema_xml := pCaminhoSchemas + 'inutNFe_v4.00.xsd';
    tsNfeConsulta      : schema_xml := pCaminhoSchemas + 'consSitNFe_v4.00.xsd';
    tsNfeStatusServico : schema_xml := pCaminhoSchemas + 'consStatServ_v4.00.xsd';
//    tsNfeCadastro     : schema_xml := pCaminhoSchemas + 'envEventoCancNFe_v1.00.xsd';
//    tsNfeCCe          : schema_xml := pCaminhoSchemas + 'envConfRecebto_v1.00.xsd';
    tsNfeEvento    : schema_xml := pCaminhoSchemas + 'envEventoCancNFe_v1.00.xsd';
    tsNfeAutorizacao   : schema_xml := pCaminhoSchemas + 'enviNFe_v3.10.xsd';
    tsNfeRetAutorizacao: schema_xml := pCaminhoSchemas + 'consReciNFe_v3.10.xsd';
  else
    schema_xml := '';
  end;

  if not FileExists(schema_xml) then
    raise Exception.Create('Arquivo ' + schema_xml + ' n�o encontrado!');

  doc := xmlParseDoc(PAnsiChar(AnsiString(pXML)));

  if ((doc = nil) or (xmlDocGetRootElement(doc) = nil)) then begin
    GerarErro('Erro: unable to parse');
    Exit;
  end;

  schema_doc  := xmlReadFile(PAnsiChar(AnsiString(schema_xml)), nil, XML_DETECT_IDS);
  //  the schema cannot be loaded or is not well-formed
  if schema_doc = nil then begin
    GerarErro('Erro: Schema n�o pode ser carregado ou est� corrompido');
    Exit;
  end;

  parser_ctxt  := xmlSchemaNewDocParserCtxt(schema_doc);
// unable to create a parser context for the schema */
  if parser_ctxt = nil then begin
    xmlFreeDoc(schema_doc);
    GerarErro('Erro: unable to create a parser context for the schema');
    Exit;
  end;

   schema := xmlSchemaParse(parser_ctxt);
// the schema itself is not valid
  if schema = nil then begin
    xmlSchemaFreeParserCtxt(parser_ctxt);
    xmlFreeDoc(schema_doc);
    GerarErro('Error: the schema itself is not valid');
    Exit;
  end;

  valid_ctxt := xmlSchemaNewValidCtxt(schema);
//   unable to create a validation context for the schema */
  if (valid_ctxt = nil) then begin
    xmlSchemaFree(schema);
    xmlSchemaFreeParserCtxt(parser_ctxt);
    xmlFreeDoc(schema_doc);
    GerarErro('Error: unable to create a validation context for the schema');
    Exit;
  end;

  if (xmlSchemaValidateDoc(valid_ctxt, doc) <> 0) then begin
    schemError := xmlGetLastError();
    GerarErro(IntToStr(schemError^.code)+' - '+string(schemError^.message));
    Exit;
  end;

  xmlSchemaFreeValidCtxt(valid_ctxt);
  xmlSchemaFree(schema);
  xmlSchemaFreeParserCtxt(parser_ctxt);
  xmlFreeDoc(schema_doc);
end;

function HouveErroRetornoNFE(const pCodigoRetorno: string): Boolean;
begin
  Result := not _Biblioteca.Em(pCodigoRetorno, ['100', '103', '104', '105']);
end;

procedure InitXmlSec;
begin
  { Init libxml and libxslt libraries }
  xmlInitParser();
  __xmlLoadExtDtdDefaultValue^ := XML_DETECT_IDS or XML_COMPLETE_ATTRS;
  xmlSubstituteEntitiesDefault(1);
  __xmlIndentTreeOutput^ := 1;


  { Init xmlsec library }
  if (xmlSecInit() < 0) then
     raise Exception.Create('Error: xmlsec initialization failed.');

  { Check loaded library version }
  if (xmlSecCheckVersionExt(1, 2, 8, xmlSecCheckVersionABICompatible) <> 1) then
     raise Exception.Create('Error: loaded xmlsec library version is not compatible.');

  (* Load default crypto engine if we are supporting dynamic
   * loading for xmlsec-crypto libraries. Use the crypto library
   * name ("openssl", "nss", etc.) to load corresponding
   * xmlsec-crypto library.
   *)
  if (xmlSecCryptoDLLoadLibrary('openssl') < 0) then
     raise Exception.Create( 'Error: unable to load default xmlsec-crypto library. Make sure'#10 +
                              'that you have it installed and check shared libraries path'#10 +
                              '(LD_LIBRARY_PATH) environment variable.');

  { Init crypto library }
  if (xmlSecCryptoAppInit(nil) < 0) then
     raise Exception.Create('Error: crypto initialization failed.');

  { Init xmlsec-crypto library }
  if (xmlSecCryptoInit() < 0) then
     raise Exception.Create('Error: xmlsec-crypto initialization failed.');
end ;

procedure ShutDownXmlSec ;
begin
  { Shutdown xmlsec-crypto library }
  xmlSecCryptoShutdown();

  { Shutdown crypto library }
  xmlSecCryptoAppShutdown();

  { Shutdown xmlsec library }
  xmlSecShutdown();

  { Shutdown libxslt/libxml }
  xsltCleanupGlobals();
  xmlCleanupParser();
end ;

function SignFile(const Axml: PAnsiChar; const key_file: PAnsiChar; const senha: PAnsiChar): AnsiString;
var
  doc: xmlDocPtr;
  node: xmlNodePtr;
  dsigCtx: xmlSecDSigCtxPtr;
  buffer: PAnsiChar;
  bufSize: integer;
  label done;
begin
  doc := nil;
  Result := '';
  //node := nil;
  dsigCtx := nil;

  if (Axml = nil) or (key_file = nil) then
    Exit;

  try
     { load template }
     doc := xmlParseDoc( Axml );
     if ((doc = nil) or (xmlDocGetRootElement(doc) = nil)) then
       raise Exception.Create('Error: unable to parse');

     { find start node }
     node := xmlSecFindNode(xmlDocGetRootElement(doc), PAnsiChar(xmlSecNodeSignature), PAnsiChar(xmlSecDSigNs));
     if (node = nil) then
       raise Exception.Create('Error: start node not found');

     { create signature context, we don't need keys manager in this example }
     dsigCtx := xmlSecDSigCtxCreate(nil);
     if (dsigCtx = nil) then
       raise Exception.Create('Error :failed to create signature context');

     // { load private key}
     dsigCtx^.signKey := xmlSecCryptoAppKeyLoad(key_file, xmlSecKeyDataFormatPkcs12, senha, nil, nil);
     if (dsigCtx^.signKey = nil) then
        raise Exception.Create('Error: failed to load private pem key from "' + key_file + '"');

     { set key name to the file name, this is just an example! }
     if (xmlSecKeySetName(dsigCtx^.signKey, PAnsiChar(key_file)) < 0) then
       raise Exception.Create('Error: failed to set key name for key from "' + key_file + '"');

     { sign the template }
     if (xmlSecDSigCtxSign(dsigCtx, node) < 0) then
       raise Exception.Create('Error: signature failed');

     { print signed document to stdout }
     // xmlDocDump(stdout, doc);
     // Can't use "stdout" from Delphi, so we'll use xmlDocDumpMemory instead...
     buffer := nil;
     xmlDocDumpMemory(doc, @buffer, @bufSize);
     if (buffer <> nil) then
        { success }
        result := buffer ;
  finally
     { cleanup }
     if (dsigCtx <> nil) then
       xmlSecDSigCtxDestroy(dsigCtx);

     if (doc <> nil) then
       xmlFreeDoc(doc);
  end;
end;

function SignMemory(const Axml: PAnsiChar; const key_file: PAnsichar; const senha: PAnsiChar; Size: Integer; Ponteiro: Pointer): AnsiString;
var
  doc: xmlDocPtr;
  node: xmlNodePtr;
  buffer: PAnsiChar;
  bufSize: integer;
  dsigCtx: xmlSecDSigCtxPtr;
  label done;
begin
  doc := nil;
  //node := nil;
  dsigCtx := nil;
  result := '';

  if (Axml = nil) or (key_file = nil) then
    Exit;

  try
    { load template }
    doc := xmlParseDoc(Axml);
    if ((doc = nil) or (xmlDocGetRootElement(doc) = nil)) then
     raise Exception.Create('Error: unable to parse');

    { find start node }
    node := xmlSecFindNode(xmlDocGetRootElement(doc), PAnsiChar(xmlSecNodeSignature), PAnsiChar(xmlSecDSigNs));
    if (node = nil) then
     raise Exception.Create('Error: start node not found');

    { create signature context, we don't need keys manager in this example }
    dsigCtx := xmlSecDSigCtxCreate(nil);
    if (dsigCtx = nil) then
     raise Exception.Create('Error :failed to create signature context');

    // { load private key, assuming that there is not password }
    dsigCtx^.signKey := xmlSecCryptoAppKeyLoadMemory(Ponteiro, size, xmlSecKeyDataFormatPkcs12, senha, nil, nil);

    if (dsigCtx^.signKey = nil) then
      raise Exception.Create('Error: failed to load private pem key from "' + key_file + '"');

    { set key name to the file name, this is just an example! }
    if (xmlSecKeySetName(dsigCtx^.signKey, key_file) < 0) then
     raise Exception.Create('Error: failed to set key name for key from "' + key_file + '"');

    { sign the template }
    if (xmlSecDSigCtxSign(dsigCtx, node) < 0) then
     raise Exception.Create('Error: signature failed');

    { print signed document to stdout }
    // xmlDocDump(stdout, doc);
    // Can't use "stdout" from Delphi, so we'll use xmlDocDumpMemory instead...
    buffer := nil;
    xmlDocDumpMemory(doc, @buffer, @bufSize);

    { success }
    if (buffer <> nil) then
      result := buffer ;
  finally
    { cleanup }
    if dsigCtx <> nil then
      xmlSecDSigCtxDestroy(dsigCtx);

    if doc <> nil then
      xmlFreeDoc(doc);
  end ;
end;

function PosLast(const SubStr, S: AnsiString): Integer;
var
  P : Integer;
begin
  Result := 0;
  P := Pos(SubStr, S);

  while P <> 0 do begin
    Result := P;
    P := PosEx(SubStr, S, P + 1);
  end;
end;

function GerarDigito(var Digito: Integer; chave: string): Boolean;
var
  i: Integer;
  j: Integer;
const
  peso = '4329876543298765432987654329876543298765432';
begin
  // Manual Integracao Contribuinte v2.02a - P�gina: 70 //
  chave := RetornaNumeros(chave);
  j := 0;
  Digito := 0;
  result := True;
  try
    for i := 1 to 43 do
      j := j + StrToInt(copy(chave, i, 1)) * StrToInt(copy(peso, i, 1));

    Digito := 11 - (j mod 11);

    if (j mod 11) < 2 then
      Digito := 0;
  except
    result := False;
  end;

  if Length(chave) <> 43 then
    Result := False;
end;

function GerarChaveNFE(
  const pCodigoUFIBGE: Integer;
  const pNumeroSerie: Integer;
  const pNumeroNota: Integer;
  const pTipoEmissao: Integer;
  const pDataEmissao: TDateTime;
  const pCNPJ: string;
  const pTipoDocumento: TTipoDocumento;
  out pCodigoNota: Integer;
  out pDigitoVerificador: Integer
): string;
begin
  Result := '';
  try
    pCodigoNota := pCodigoUFIBGE;

    Result :=
      'NFe' +                                              // NFe            TAMANHO
      _Biblioteca.LPad(IntToStr(pCodigoUFIBGE), 2, '0') +  // C�digo da UF         2
      FormatDateTime('YY', pDataEmissao) +                 // Ano emiss�o YY       2
      FormatDateTime('MM', pDataEmissao) +                 // M�s emiss�o MM       2
      LPad(RetornaNumeros(pCNPJ), 14, '0') +               // CNPJ remetente      14
      IfThen(pTipoDocumento = tpNFe, '55', '65') +         // Modelo               2
      LPad(IntToStr(pNumeroSerie), 3, '0') +               // S�rie da nota        3
      LPad(IntToStr(pNumeroNota), 9, '0') +                // N�mero da NF-e       9
      LPad(IntToStr(pTipoEmissao), 1, '0') +               // Forma de emiss�o     1
      LPad(IntToStr(pCodigoNota), 8, '0');                 // C�digo num�rico      8

    GerarDigito(pDigitoVerificador, Result);
    Result := Result + IntToStr(pDigitoVerificador);
  except
    Result := '';
  end;
end;

function BuscarDadosNFeEntrada(
  const pCaminhoArquivo: string;
  var pCapaNota: RecCapaNFe;
  var pItensNota: TArray<RecItemNFe>;
  var pFaturamentos: TArray<RecFaturamentoNFe>
): Boolean;
var
  vXML: TXMLDocument;

  i: Integer;

  vNo: IXMLNode;
  vNoItem: IXMLNode;
  vNoItens: IXMLNode;
  vNoItemImposto: IXMLNode;
  vNoItemImpNome: IXMLNode;

  vNoICMS: IXMLNode;
  vNoPIS: IXMLNode;
  vNoCofins: IXMLNode;
  vNoIPI: IXMLNode;

  vItem: RecItemNFe;
  vCodigoProduto: string;

  function FormatarValoresXML(pValor: string; pTamanhoCampo: Integer; pDecimais: Integer): string;
  var
    vMascFinal, vMascara, vDecimais : string;
    i, x : Integer;
  begin
    //Valida��o dos Dados passados
    Result := '';
    if (Trim(pValor) = '') or (pTamanhoCampo = 0) then
      Exit;

    //Troca "." (padrao XML) por "," para poder usar a funcao "StrToFloat"
    pValor := StringReplace(pValor, '.', ',',[rfReplaceAll]);

    //Monta a m�scara para formata��o do resultado
    x := 0;
    vMascara := '';
    for i := 1 to pTamanhoCampo do begin
      if (x = 3) and (i <> pTamanhoCampo) then begin
        vMascara := vMascara + ',';
        x := 0;
      end;
      vMascara := vMascara + '#';
      x := x + 1;
    end;

    //Caso ocorra de formatar assim "###,###,##" ele inverte para "##,###,###" deixando sempre tres caracs "#" na ult pos.
    vMascara := ReverseString(vMascara);

    if Copy(vMascara, Length(vMascara), 1) = ',' then
      vMascara := Copy(vMascara, 1, Length(vMascara)-2) + '0'
    else
      vMascara := Copy(vMascara, 1, Length(vMascara)-1) + '0';

    //Monta a m�scara para formata��o dos decimais
    vDecimais := '';
    for i := 1 to pDecimais do
      vDecimais := vDecimais + '0';

    vDecimais := '.' + vDecimais;

    vMascFinal := vMascara + vDecimais;
    Result := FormatFloat(vMascFinal, StrToFloat(pValor));
  end;

  function FormatarData(pData: string): TDateTime;
  begin
    Result := StrToDate(Copy(pData, 9, 2) + '/' + Copy(pData, 6, 2) + '/' + Copy(pData, 1, 4));
  end;

begin
  Result := False;
  vXML := TXMLDocument.Create(Application);
  try
    vXML.LoadFromFile(pCaminhoArquivo);
    vXML.Active := True;

    pCapaNota.XmlTexto := vXML.XML.Text;

    // Chave de acesso
    vNo := getNoXMLDocument('InfNFe', vXML.Node);
    if vNo = nil then
      raise Exception.Create('N�o foi encontrada a chave de acesso da NFe, verifique se o arquivo carregado � um XML de NFe v�lido!');

    if vNo.HasAttribute('Id') then
      pCapaNota.ChaveNFe := _Biblioteca.FormatarChaveNFe(vNo.Attributes['Id']);;

    // Informa��es de Protocolo de Autoriza��o - (Se tiver)
//    vNo := getNoXMLDocument('infProt', vXML.Node);
//    if vNo <> nil then
//      pCapaNota[0].protocolo_nfe      :=  vNo.ChildNodes['nProt'].Text;

    // Identifica��o da NF-e - Ide
    vNo := getNoXMLDocument('ide', vXML.Node);
    if vNo <> nil then begin
      pCapaNota.DataHoraEmissao        := FormatarData(vNo.ChildNodes['dhEmi'].Text);
      pCapaNota.NumeroNota             := SFormatInt(vNo.ChildNodes['nNF'].Text);
      pCapaNota.ModeloNota             := vNo.ChildNodes['mod'].Text;
      pCapaNota.SerieNota              := vNo.ChildNodes['serie'].Text;
//      pCapaNota[0].codigo_ibge_municipio_emit := ParaInteger(vNo.ChildNodes['cMunFG'].Text);
    end;

    // Emitente - Emit
    vNo := getNoXMLDocument('emit', vXML.Node);
    if vNo <> nil then begin
      {Dados do n� dentro do n� pai EMITENTE}
      pCapaNota.RazaoSocial       := vNo.ChildNodes['xNome'].Text;
      pCapaNota.NomeFantasia      := vNo.ChildNodes['xFant'].Text;
      pCapaNota.Cnpj              := _Biblioteca.FormatarCNPJ(vNo.ChildNodes['CNPJ'].Text);
      pCapaNota.InscricaoEstadual := vNo.ChildNodes['IE'].Text;
      pCapaNota.SuperSimples      := Em(vNo.ChildNodes['CRT'].Text, ['1','2']);
    end;

    // Emitente - EnderEmit
    vNo := getNoXMLDocument('EnderEmit', vXML.Node);
    if vNo <> nil then begin
      pCapaNota.Logradouro          := vNo.ChildNodes['xLgr'].Text;
      pCapaNota.Complemento         := vNo.ChildNodes['xCpl'].Text;
      pCapaNota.Bairro              := vNo.ChildNodes['xBairro'].Text;
      pCapaNota.Cep                 := _Biblioteca.FormatarCEP(vNo.ChildNodes['CEP'].Text);
      pCapaNota.CodigoIBGeMunicipio := vNo.ChildNodes['cMun'].Text;
      pCapaNota.Cidade              := vNo.ChildNodes['xMun'].Text;
      pCapaNota.Telefone            := '(' + Copy(vNo.ChildNodes['fone'].Text, 1, 2) + ') ' + Copy(vNo.ChildNodes['fone'].Text, 3, 4) + '-' + Copy(vNo.ChildNodes['fone'].Text, 7, 4);
      pCapaNota.Estado              := vNo.ChildNodes['UF'].Text;
    end;

    // Destinat�rio
    vNo := getNoXMLDocument('dest', vXML.Node);
    if vNo <> nil then
      pCapaNota.CnpjDestinatario    :=  vNo.ChildNodes['CNPJ'].Text;

    //vVCredSN_Acum := 0;

    // Produtos
    pItensNota := nil;
    vNoItens := getNoXMLDocument('Det', vXML.Node);
    if vNoItens <> nil then begin
      repeat
        vNoItem  := vNoItens.ChildNodes['prod'];
        if vNoItem <> nil then begin
          vNoItem.ChildNodes.First;
          vCodigoProduto    := vNoItem.ChildNodes['cProd'].Text;
          if vCodigoProduto <> '' then begin
            vItem.Iniciar;

            if vNoItens.HasAttribute('nItem') then
              vItem.ItemId      := SFormatInt(vNoItens.Attributes['nItem']);

            vItem.CodigoOriginal             := vNoItem.ChildNodes['cProd'].Text;
            vItem.NomeProduto                := vNoItem.ChildNodes['xProd'].Text;
            vItem.Quantidade                 := SFormatDouble(FormatarValoresXML(vNoItem.ChildNodes['qCom'].Text, 15, 4));
            vItem.Unidade                    := vNoItem.ChildNodes['uCom'].Text;
            vItem.ValorTotal                 := SFormatDouble(FormatarValoresXML(vNoItem.ChildNodes['vProd'].Text, 15, 2));

            vItem.CodigoNcm                  := vNoItem.ChildNodes['NCM'].Text;
            vItem.Cest                       := vNoItem.ChildNodes['CEST'].Text;
            vItem.CfopId                     := vNoItem.ChildNodes['CFOP'].Text;
            vItem.CodigoBarras               := vNoItem.ChildNodes['cEAN'].Text;
//            vItem^.Prod_CodEANTrib          := vNoItem.ChildNodes['cEANTrib'].Text;
//            vItem^.Prod_UnidTrib            := vNoItem.ChildNodes['uTrib'].Text;
//            vItem^.Prod_QtdeTrib            := FormatarValoresXML(vNoItem.ChildNodes['qTrib'].Text, 15, 4);
            vItem.PrecoUnitario              := SFormatDouble(FormatarValoresXML(vNoItem.ChildNodes['vUnCom'].Text, 15, 10));
//            vItem^.Prod_ValorUnitTrib       := FormatarValoresXML(vNoItem.ChildNodes['vUnTrib'].Text, 15, 10);
            vItem.ValorFrete                 := SFormatDouble(FormatarValoresXML(vNoItem.ChildNodes['vFrete'].Text, 15, 2));
            vItem.ValorTotalDesconto         := SFormatDouble(FormatarValoresXML(vNoItem.ChildNodes['vDesc'].Text, 15, 2));
            vItem.ValorTotalOutrasDespesas   := SFormatDouble(FormatarValoresXML(vNoItem.ChildNodes['vOutro'].Text, 15, 2)) + SFormatDouble(FormatarValoresXML(vNoItem.ChildNodes['vSeg'].Text, 15, 2));

            vNoItemImposto  := vNoItens.ChildNodes['imposto'];
            if vNoItemImposto <> nil then begin
              vNoItemImpNome  := vNoItemImposto.ChildNodes['ICMS']; // CST 00
              if vNoItemImpNome <> nil then begin
                // Imposto - ICMS00
                vNoICMS  := vNoItemImpNome.ChildNodes['ICMS00']; // CST 00
                if vNoICMS.HasChildNodes then begin
                  vItem.OrigemProduto    := SFormatInt(Trim(vNoICMS.ChildNodes['orig'].Text));
                  vItem.Cst               := Trim(vNoICMS.ChildNodes['CST'].Text);
//                  vItem.ICMS_Mod_BC_Nor   := vNoICMS.ChildNodes['modBC'].Text;
//                  vItem.ICMS_Modal        := vNoICMS.ChildNodes['modBC'].Text;
                  vItem.BaseCalculoIcms := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['vBC'].Text, 15, 2));
                  vItem.PercentualIcms   := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pICMS'].Text, 4, 4));
                  vItem.ValorIcms        := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['vICMS'].Text, 15, 2));
                end;

                // Imposto - ICMS10
                vNoICMS  := vNoItemImpNome.ChildNodes['ICMS10'];
                if vNoICMS.HasChildNodes then begin
                  vItem.OrigemProduto              := SFormatInt(Trim(vNoICMS.ChildNodes['orig'].Text));
                  vItem.Cst                         := Trim(vNoICMS.ChildNodes['CST'].Text);
//                  vItem.ICMS_Mod_BC_Nor           := vNoICMS.ChildNodes['modBC'].Text;
//                  vItem.ICMS_Modal                := vNoICMS.ChildNodes['modBC'].Text;
                  vItem.BaseCalculoIcms           := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['vBC'].Text, 15, 2));
                  vItem.PercentualIcms             := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pICMS'].Text, 4, 4));
                  vItem.ValorIcms                  := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['vICMS'].Text, 15, 2));
//                  vItem.ICMSST_Mod_BC             := vNoICMS.ChildNodes['modBCST'].Text;
                  vItem.Iva                         := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pMVAST'].Text, 4, 4));
                  vItem.IndiceReducaoBaseIcmsSt := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pRedBCST'].Text, 4, 4));
                  vItem.BaseCalculoIcmsSt        := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['vBCST'].Text, 15, 2));
                  vItem.PercentualIcmsSt          := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pICMSST'].Text, 4, 4));
                  vItem.ValorIcmsSt               := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['vICMSST'].Text, 15, 2));
                end;

                // Imposto - ICMS20
                vNoICMS  := vNoItemImpNome.ChildNodes['ICMS20'];
                if vNoICMS.HasChildNodes then begin
                  vItem.OrigemProduto              := SFormatInt(Trim(vNoICMS.ChildNodes['orig'].Text));
                  vItem.Cst                         := Trim(vNoICMS.ChildNodes['CST'].Text);
//                  vItem.ICMS_Mod_BC_Nor           := vNoICMS.ChildNodes['modBC'].Text;
//                  vItem.ICMS_Modal                := vNoICMS.ChildNodes['modBC'].Text;
//                  vItem.indice_reducao_base_icms_st := FormatarValoresXML(vNoICMS.ChildNodes['pRedBC'].Text, 4, 4);
                  vItem.IndiceReducaoBaseIcms    := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pRedBC'].Text, 4, 4));
                  vItem.BaseCalculoIcms           := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['vBC'].Text, 15, 2));
                  vItem.PercentualIcms             := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pICMS'].Text, 4, 4));
                  vItem.ValorIcms                  := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['vICMS'].Text, 15, 2));
                end;

                // Imposto - ICMS30
                vNoICMS  := vNoItemImpNome.ChildNodes['ICMS30'];
                if vNoICMS.HasChildNodes then begin
                  vItem.OrigemProduto              := SFormatInt(Trim(vNoICMS.ChildNodes['orig'].Text));
                  vItem.Cst                         := Trim(vNoICMS.ChildNodes['CST'].Text);
//                  vItem.ICMSST_Mod_BC             := vNoICMS.ChildNodes['modBCST'].Text;
                  vItem.iva                         := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pMVAST'].Text, 4, 4));
                  vItem.IndiceReducaoBaseIcmsSt := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pRedBCST'].Text, 4, 4));
                  vItem.BaseCalculoIcmsSt        := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['vBCST'].Text, 15, 2));
                  vItem.PercentualIcmsSt          := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pICMSST'].Text, 4, 4));
                  vItem.ValorIcmsSt               := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['vICMSST'].Text, 15, 2));
                end;

                // Imposto - ICMS40
                vNoICMS  := vNoItemImpNome.ChildNodes['ICMS40'];
                if vNoICMS.HasChildNodes then begin
                  vItem.OrigemProduto := SFormatInt(Trim(vNoICMS.ChildNodes['orig'].Text));
                  vItem.Cst            := Trim(vNoICMS.ChildNodes['CST'].Text);
                  vItem.ValorIcms     := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['vICMS'].Text, 15, 2));
                  //motDesICMS         := ''; // Motivo da desonera��o do ICMS
                end;

                // Imposto - ICMS51
                vNoICMS  := vNoItemImpNome.ChildNodes['ICMS51'];
                if vNoICMS.HasChildNodes then begin
                  vItem.OrigemProduto           := SFormatInt(Trim(vNoICMS.ChildNodes['orig'].Text));
                  vItem.cst                      := Trim(vNoICMS.ChildNodes['CST'].Text);
//                  vItem.ICMS_Mod_BC_Nor        := vNoICMS.ChildNodes['modBC'].Text;
                  vItem.IndiceReducaoBaseIcms := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pRedBC'].Text, 4, 4));
                  vItem.BaseCalculoIcms        := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['vBC'].Text, 15, 2));
                  vItem.PercentualIcms          := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pICMS'].Text, 4, 4));
                  vItem.ValorIcms               := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['vICMS'].Text, 15, 2));
                end;

                // Imposto - ICMS60
                vNoICMS  := vNoItemImpNome.ChildNodes['ICMS60'];
                if vNoICMS.HasChildNodes then begin
                  vItem.OrigemProduto := SFormatInt(Trim(vNoICMS.ChildNodes['orig'].Text));
                  vItem.cst            := Trim(vNoICMS.ChildNodes['CST'].Text);
                  //vBCSTRet                      := ''; // Valor da BC do ICMS ST retido (Obrigat�rio)
                  //vICMSSTRet                    := ''; // Valor do ICMS ST retido (Obrigat�rio)
                end;

                // Imposto - ICMS70
                vNoICMS  := vNoItemImpNome.ChildNodes['ICMS70'];
                if vNoICMS.HasChildNodes then begin
                  vItem.OrigemProduto              := SFormatInt(Trim(vNoICMS.ChildNodes['orig'].Text));
                  vItem.cst                         := Trim(vNoICMS.ChildNodes['CST'].Text);
//                  vItem.ICMS_Mod_BC_Nor           := vNoICMS.ChildNodes['modBC'].Text;
//                  vItem.ICMS_Modal                := vNoICMS.ChildNodes['modBC'].Text;
                  vItem.IndiceReducaoBaseIcms    := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pRedBC'].Text, 4, 4));
                  vItem.BaseCalculoIcms           := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['vBC'].Text, 15, 2));
                  vItem.PercentualIcms             := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pICMS'].Text, 4, 4));
                  vItem.ValorIcms                  := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['vICMS'].Text, 15, 2));
//                  vItem.ICMSST_Mod_BC             := vNoICMS.ChildNodes['modBCST'].Text;
                  vItem.iva                         := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pMVAST'].Text, 4, 4));
                  vItem.IndiceReducaoBaseIcmsSt := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pRedBCST'].Text, 4, 4));
                  vItem.BaseCalculoIcmsSt        := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['vBCST'].Text, 15, 2));
                  vItem.PercentualIcmsSt          := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pICMSST'].Text, 4, 4));
                  vItem.ValorIcmsSt               := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['vICMSST'].Text, 15, 2));
                end;

                // Imposto - ICMS90
                vNoICMS  := vNoItemImpNome.ChildNodes['ICMS90'];
                if vNoICMS.HasChildNodes then begin
                  vItem.OrigemProduto              := SFormatInt(Trim(vNoICMS.ChildNodes['orig'].Text));
                  vItem.cst                         := Trim(vNoICMS.ChildNodes['CST'].Text);
//                  vItem.ICMS_Mod_BC_Nor           := vNoICMS.ChildNodes['modBC'].Text;
//                  vItem.ICMS_Modal                := vNoICMS.ChildNodes['modBC'].Text;
                  vItem.BaseCalculoIcms           := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['vBC'].Text, 15, 2));
                  vItem.IndiceReducaoBaseIcms    := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pRedBC'].Text, 4, 4));
                  vItem.PercentualIcms             := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pICMS'].Text, 4, 4));
                  vItem.ValorIcms                  := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['vICMS'].Text, 15, 2));
//                  vItem.ICMSST_Mod_BC             := vNoICMS.ChildNodes['modBCST'].Text;
                  vItem.Iva                         := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pMVAST'].Text, 4, 4));
                  vItem.IndiceReducaoBaseIcmsSt := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pRedBCST'].Text, 4, 4));
                  vItem.BaseCalculoIcmsSt        := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['vBCST'].Text, 15, 2));
                  vItem.PercentualIcmsSt          := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pICMSST'].Text, 4, 4));
                  vItem.ValorIcmsSt               := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['vICMSST'].Text, 15, 2));
                end;

                if vItem.IndiceReducaoBaseIcms = 0 then
                  vItem.IndiceReducaoBaseIcms := 1;

                if vItem.IndiceReducaoBaseIcmsSt = 0 then
                  vItem.IndiceReducaoBaseIcms := 1;

              //=========================================================================================================

                // Imposto - ICMSSN101
                vNoICMS  := vNoItemImpNome.ChildNodes['ICMSSN101'];
                if vNoICMS.HasChildNodes then begin
                  vItem.OrigemProduto   := SFormatInt(vNoICMS.ChildNodes['orig'].Text);
                  vItem.Cst             := '00';
                end;

                // Imposto - ICMSSN102
                vNoICMS  := vNoItemImpNome.ChildNodes['ICMSSN102'];
                if vNoICMS.HasChildNodes then begin
                  vItem.OrigemProduto   := SFormatInt(vNoICMS.ChildNodes['orig'].Text);
                  vItem.Cst             := '00';
                end;

                // Imposto - ICMSSN201
                vNoICMS  := vNoItemImpNome.ChildNodes['ICMSSN201'];
                if vNoICMS.HasChildNodes then begin
                  vItem.OrigemProduto   := SFormatInt(vNoICMS.ChildNodes['orig'].Text);
                  vItem.Cst             := '00';
                end;

                // Imposto - ICMSSN202
                vNoICMS  := vNoItemImpNome.ChildNodes['ICMSSN202'];
                if vNoICMS.HasChildNodes then begin
                  vItem.OrigemProduto           := SFormatInt(vNoICMS.ChildNodes['orig'].Text);
                  vItem.Cst                     := '10';
//                  vItem.ICMSSN_modBCST      := vNoICMS.ChildNodes['modBCST'].Text;
                  vItem.Iva                     := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pMVAST'].Text, 5, 2));
                  vItem.IndiceReducaoBaseIcmsSt := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pRedBCST'].Text, 5, 2));
                  vItem.BaseCalculoIcmsSt       := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['vBCST'].Text, 15, 2));
                  vItem.PercentualIcmsSt        := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['pICMSST'].Text, 5, 2));
                  vItem.ValorIcmsSt             := SFormatDouble(FormatarValoresXML(vNoICMS.ChildNodes['vICMSST'].Text, 15, 2));
                end;

                // Imposto - ICMSSN500
//                vNoICMS  := vNoItemImpNome.ChildNodes['ICMSSN500'];
//                if vNoICMS.HasChildNodes then begin
//                  vItem.ICMSSN_Orig         := vNoICMS.ChildNodes['orig'].Text;
//                  vItem.ICMSSN_CSOSN        := vNoICMS.ChildNodes['CSOSN'].Text;
//                  vItem.ICMSSN_vBCSTRet     := FormatarValoresXML(vNoICMS.ChildNodes['vBCSTRet'].Text, 15, 2);
//                  vItem.ICMSSN_vICMSSTRet   := FormatarValoresXML(vNoICMS.ChildNodes['vICMSSTRet'].Text, 15, 2);
//                end;

                // Imposto - ICMSSN900
//                vNoICMS  := vNoItemImpNome.ChildNodes['ICMSSN900'];
//                if vNoICMS.HasChildNodes then begin
//                  vItem.ICMSSN_Orig         := vNoICMS.ChildNodes['orig'].Text;
//                  vItem.ICMSSN_CSOSN        := vNoICMS.ChildNodes['CSOSN'].Text;
//                  vItem.ICMSSN_modBC        := vNoICMS.ChildNodes['modBC'].Text;
//                  vItem.ICMSSN_vBC          := FormatarValoresXML(vNoICMS.ChildNodes['vBC'].Text, 15, 2);
//                  vItem.ICMSSN_pRedBC       := FormatarValoresXML(vNoICMS.ChildNodes['pRedBC'].Text, 5, 2);
//                  vItem.ICMSSN_pICMS        := FormatarValoresXML(vNoICMS.ChildNodes['pICMS'].Text, 5, 2);
//                  vItem.ICMSSN_vICMS        := FormatarValoresXML(vNoICMS.ChildNodes['vICMS'].Text, 15, 2);
//                  vItem.ICMSSN_modBCST      := vNoICMS.ChildNodes['modBCST'].Text;
//                  vItem.ICMSSN_pMVAST       := FormatarValoresXML(vNoICMS.ChildNodes['pMVAST'].Text, 5, 2);
//                  vItem.ICMSSN_pRedBCST     := FormatarValoresXML(vNoICMS.ChildNodes['pRedBCST'].Text, 5, 2);
//                  vItem.ICMSSN_vBCST        := FormatarValoresXML(vNoICMS.ChildNodes['vBCST'].Text, 15, 2);
//                  vItem.ICMSSN_pICMSST      := FormatarValoresXML(vNoICMS.ChildNodes['pICMSST'].Text, 5, 2);
//                  vItem.ICMSSN_vICMSST      := FormatarValoresXML(vNoICMS.ChildNodes['vICMSST'].Text, 15, 2);
//                  vItem.ICMSSN_pCredSN      := FormatarValoresXML(vNoICMS.ChildNodes['pCredSN'].Text, 5, 2);
//                  vItem.ICMSSN_vCredICMSSN  := FormatarValoresXML(vNoICMS.ChildNodes['vCredICMSSN'].Text, 15, 2);
//                end;
              end;
              // Imposto - PIS
              vNoItemImpNome  := vNoItemImposto.ChildNodes['PIS'];
              if vNoItemImpNome <> nil then begin
                vNoPis  := vNoItemImpNome.ChildNodes['PISAliq'];
                if vNoPis.HasChildNodes then begin
                  vItem.CstPis            := vNoPis.ChildNodes['CST'].Text;
                  vItem.BaseCalculoPis    := SFormatDouble(FormatarValoresXML(vNoPis.ChildNodes['vBC'].Text, 15, 2));
                  vItem.PercentualPis     := SFormatDouble(FormatarValoresXML(vNoPis.ChildNodes['pPIS'].Text, 4, 2));
                  vItem.ValorPis          := SFormatDouble(FormatarValoresXML(vNoPis.ChildNodes['vPIS'].Text, 15, 2));
                end
                else
                  vItem.CstPis := '99';

//                vNoPis  := vNoItemImpNome.ChildNodes['PISQtde'];
//                if vNoPis.HasChildNodes then begin
//                  vItem.Pis_CST             := vNoPis.ChildNodes['CST'].Text;
//                  vItem.Pis_qBCProd         := vNoPis.ChildNodes['qBCProd'].Text;
//                  vItem.Pis_vAliqProd       := vNoPis.ChildNodes['vAliqProd'].Text;
//                  vItem.Pis_vPIS            := vNoPis.ChildNodes['vPIS'].Text;
//                end;

//                vNoPis  := vNoItemImpNome.ChildNodes['PISNT'];
//                if vNoPis.HasChildNodes then
//                  vItem.Pis_CST             := vNoPis.ChildNodes['CST'].Text;

//                vNoPis  := vNoItemImpNome.ChildNodes['PISOutr'];
//                if vNoPis.HasChildNodes then begin
//                  vItem.Pis_CST             := vNoPis.ChildNodes['CST'].Text;
//                  vItem.Pis_vBC             := vNoPis.ChildNodes['vBC'].Text;
//                  vItem.Pis_pPIS            := vNoPis.ChildNodes['pPIS'].Text;
//                  vItem.Pis_qBCProd         := vNoPis.ChildNodes['qBCProd'].Text;
//                  vItem.Pis_vAliqProd       := vNoPis.ChildNodes['vAliqProd'].Text;
//                  vItem.Pis_vPIS            := vNoPis.ChildNodes['vPIS'].Text;
//                end;
              end;

              // Imposto - PISST
//              vNoPis  := vNoItemImpNome.ChildNodes['PISST'];
//              if vNoPis.HasChildNodes then begin
//                vItem.Pis_vBC             := vNoPis.ChildNodes['vBC'].Text;
//                vItem.Pis_pPIS            := vNoPis.ChildNodes['pPIS'].Text;
//                vItem.Pis_qBCProd         := vNoPis.ChildNodes['qBCProd'].Text;
//                vItem.Pis_vAliqProd       := vNoPis.ChildNodes['vAliqProd'].Text;
//                vItem.Pis_vPIS            := vNoPis.ChildNodes['vPIS'].Text;
//              end;

              // Imposto - COFINS
              vNoItemImpNome  := vNoItemImposto.ChildNodes['COFINS'];
              if vNoItemImpNome <> nil then begin
                vNoCofins  := vNoItemImpNome.ChildNodes['COFINSAliq'];
                if vNoCofins.HasChildNodes then begin
                  vItem.CstCofins          := vNoCofins.ChildNodes['CST'].Text;
                  vItem.BaseCalculoCofins  := SFormatDouble(FormatarValoresXML(vNoCofins.ChildNodes['vBC'].Text, 15, 2));
                  vItem.PercentualCofins   := SFormatDouble(FormatarValoresXML(vNoCofins.ChildNodes['pCOFINS'].Text, 4, 2));
                  vItem.ValorCofins        := SFormatDouble(FormatarValoresXML(vNoCofins.ChildNodes['vCOFINS'].Text, 15, 2));
                end
                else
                  vItem.CstCofins := '99';

//                vNoCofins  := vNoItemImpNome.ChildNodes['COFINSQtde'];
//                if vNoCofins.HasChildNodes then begin
//                  vItem.Cof_CST             := vNoCofins.ChildNodes['CST'].Text;
//                  vItem.Cof_qBCProd         := vNoCofins.ChildNodes['qBCProd'].Text;
//                  vItem.Cof_vAliqProd       := vNoCofins.ChildNodes['vAliqProd'].Text;
//                  vItem.Cof_vCof            := vNoCofins.ChildNodes['vCOFINS'].Text;
//                end;
//
//                vNoCofins  := vNoItemImpNome.ChildNodes['COFINSNT'];
//                if vNoCofins.HasChildNodes then
//                  vItem.Cof_CST             := vNoCofins.ChildNodes['CST'].Text;
//
//                vNoCofins  := vNoItemImpNome.ChildNodes['COFINSOutr'];
//                if vNoCofins.HasChildNodes then begin
//                  vItem.Cof_CST             := vNoCofins.ChildNodes['CST'].Text;
//                  vItem.Cof_vBC             := vNoCofins.ChildNodes['vBC'].Text;
//                  vItem.Cof_pCof            := vNoCofins.ChildNodes['pCOFINS'].Text;
//                  vItem.Cof_qBCProd         := vNoCofins.ChildNodes['qBCProd'].Text;
//                  vItem.Cof_vAliqProd       := vNoCofins.ChildNodes['vAliqProd'].Text;
//                  vItem.Cof_vCof            := vNoCofins.ChildNodes['vCOFINS'].Text;
//                end;
              end;

//              // Imposto - CofST
//              vNoCofins  := vNoItemImpNome.ChildNodes['COFINSST'];
//              if vNoCofins.HasChildNodes then begin
//                vItem.Cof_vBC             := vNoCofins.ChildNodes['vBC'].Text;
//                vItem.Cof_pCof            := vNoCofins.ChildNodes['pCOFINS'].Text;
//                vItem.Cof_qBCProd         := vNoCofins.ChildNodes['qBCProd'].Text;
//                vItem.Cof_vAliqProd       := vNoCofins.ChildNodes['vAliqProd'].Text;
//                vItem.Cof_vCof            := vNoCofins.ChildNodes['vCOFINS'].Text;
//              end;

              // Imposto - IPI
              vNoItemImpNome  := vNoItemImposto.ChildNodes['IPI'];
              if vNoItemImpNome <> nil then begin
//                vItem.IPI_CodEnq        := vNoItemImpNome.ChildNodes['clEnq'].Text;
                vNoIPI  := vNoItemImpNome.ChildNodes['IPITrib'];
                if vNoIPI.HasChildNodes then begin
//                  vItem.IPI_BC              := FormatarValoresXML(vNoIPI.ChildNodes['vBC'].Text, 15, 2);
                  vItem.PercentualIpi       := SFormatDouble(FormatarValoresXML(vNoIPI.ChildNodes['pIPI'].Text, 4, 4));
//                  vItem.IPI_CST             := vNoIPI.ChildNodes['CST'].Text;
                  vItem.ValorIpi            := SFormatDouble(FormatarValoresXML(vNoIPI.ChildNodes['vIPI'].Text, 15, 2));
//                  vItem.IPI_QtdTotUnidPadr  := FormatarValoresXML(vNoIPI.ChildNodes['qUnid'].Text, 15, 4);
//                  vItem.IPI_ValorPorUnid    := FormatarValoresXML(vNoIPI.ChildNodes['vUnid'].Text, 15, 10);
                end;
              end;
            end;
            ////Acumula valores de permissoes dos Creditos de ICMS SN
            //if Trim(vItemAtual^.ICMSSN_vCredICMSSN) <> '' then
            //  if StrToFloat(vItemAtual^.ICMSSN_vCredICMSSN) > 0 then
            //    vVCredSN_Acum := vVCredSN_Acum + StrToFloat(vItemAtual^.ICMSSN_vCredICMSSN);
            SetLength(pItensNota, Length(pItensNota) + 1);
            pItensNota[High(pItensNota)] := vItem;
          end;
          vNoItem := vNoItem.NextSibling;
          vNoItens  := vNoItens.NextSibling;
        end;
      until vCodigoProduto = ''
    end;

//    pCapaNota.IcmsSN_VCREDSN := FormatarValoresXML(FloatToStr(vVCredSN_Acum),15,2);
//    pCapaNota.IcmsSN_PCREDSN := vItemAtual^.IcmsSN_PCREDSN;

    // Dados dos totais da NF-e
    vNo := getNoXMLDocument('ICMSTot', vXML.Node);
    if vNo <> nil then begin
      pCapaNota.ValorTotal          := SFormatDouble(FormatarValoresXML(vNo.ChildNodes['vNF'].Text, 15, 2));
      pCapaNota.BaseCalculoIcms    := SFormatDouble(FormatarValoresXML(vNo.ChildNodes['vBC'].Text, 15, 2));
      pCapaNota.ValorIcms           := SFormatDouble(FormatarValoresXML(vNo.ChildNodes['vICMS'].Text, 15, 2));
      pCapaNota.BaseCalculoIcmsSt := SFormatDouble(FormatarValoresXML(vNo.ChildNodes['vBCST'].Text, 15, 2));
      pCapaNota.ValorIcmsSt        := SFormatDouble(FormatarValoresXML(vNo.ChildNodes['vST'].Text, 15, 2));
      pCapaNota.ValorTotalProdutos := SFormatDouble(FormatarValoresXML(vNo.ChildNodes['vProd'].Text, 15, 2));
      pCapaNota.ValorFrete          := SFormatDouble(FormatarValoresXML(vNo.ChildNodes['vFrete'].Text, 15, 2));
      pCapaNota.ValorOutrasDespesas:= SFormatDouble(FormatarValoresXML(vNo.ChildNodes['vOutro'].Text, 15, 2)) + SFormatDouble(FormatarValoresXML(vNo.ChildNodes['vSeg'].Text, 15, 2));
      pCapaNota.ValorIpi            := SFormatDouble(FormatarValoresXML(vNo.ChildNodes['vIPI'].Text, 15, 2));
      pCapaNota.ValorDesconto       := SFormatDouble(FormatarValoresXML(vNo.ChildNodes['vDesc'].Text, 15, 2));
//      pCapaNota[0].valor_is        := FormatarValoresXML(vNo.ChildNodes['vII'].Text, 15, 2);
      pCapaNota.ValorPis            := SFormatDouble(FormatarValoresXML(vNo.ChildNodes['vPIS'].Text, 15, 2));
      pCapaNota.ValorCofins         := SFormatDouble(FormatarValoresXML(vNo.ChildNodes['vCOFINS'].Text, 15, 2));
    end;

    // Duplicatas
    vNo := getNoXMLDocument('cobr', vXML.Node);
    if vNo <> nil then begin
      for i := 0 to vNo.ChildNodes.Count - 1 do begin
        if vNo.ChildNodes[i].LocalName = 'dup' then begin
          SetLength(pFaturamentos, Length(pFaturamentos) + 1);

          pFaturamentos[High(pFaturamentos)].NumeroDaParcela  := i;
          pFaturamentos[High(pFaturamentos)].QuantParcelas    := vNo.ChildNodes.Count -1;
          pFaturamentos[High(pFaturamentos)].Documento        := vNo.ChildNodes[i].ChildNodes['nDup'].Text;
          pFaturamentos[High(pFaturamentos)].DataVencimento   := FormatarData(vNo.ChildNodes[i].ChildNodes['dVenc'].Text);
          pFaturamentos[High(pFaturamentos)].Valor            := SFormatDouble(FormatarValoresXML(vNo.ChildNodes[i].ChildNodes['vDup'].Text, 15, 2));
        end;
      end;
    end;

    // Dados da transportadora
    vNo := getNoXMLDocument('transp', vXML.Node);
    if vNo <> nil then begin
      pCapaNota.TipoFrete := SFormatInt(vNo.ChildNodes['modFrete'].Text);
      // Dados dos volumes
      vNo := getNoXMLDocument('vol', vXML.Node);
      if vNo <> nil then begin
        pCapaNota.PesoBruto   := SFormatDouble(FormatarValoresXML(vNo.ChildNodes['pesoB'].Text, 15, 3));
        pCapaNota.PesoLiquido := SFormatDouble(FormatarValoresXML(vNo.ChildNodes['pesoL'].Text, 15, 3));
      end;
    end;

    Result := True;
  except
    on e: exception do
      Exclamar('Problemas ao ler XML da NFE: ' + e.Message);
  end;
  vXML.Free;
end;

function ValidarChaveNFe(pChaveNFe: string): Boolean;
const
  cPeso: Array[0..43] of Integer = (4,3,2,9,8,7,6,5,4,3,2,9,8,7,6,5,4,3,2,9,8,7,6,5,4,3,2,9,8,7,6,5,4,3,2,9,8,7,6,5,4,3,2,0);
var
  vChave: Array[0..43] of Char;
  i: Integer;
  vSoma: Integer;
  vVerificador: Integer;
begin
  try
    pChaveNFe := RetornaNumeros(pChaveNFe);

    if not Length(pChaveNFe) = 44 then begin
      Exclamar('A chave de acesso deve conter 44 digitos!');
      Result := False;
      Exit;
    end;

    StrPCopy(vChave, StringReplace(pChaveNFe,' ', '',[rfReplaceAll]));

    vSoma := 0;
    for I := Low(vChave) to High(vChave) do
      vSoma := vSoma + (StrToInt(vChave[i]) * cPeso[i]);

    if vSoma = 0 then begin
      Exclamar('A chave da NFe � inv�lida, verifique!');
      Result := False;
      Exit;
    end;

    vSoma := vSoma - (11 * (Trunc(vSoma / 11)));
    if (vSoma = 0) or (vSoma = 1) then
      vVerificador := 0
    else
      vVerificador := 11 - vSoma;

    Result := vVerificador = StrToInt(vChave[43]);

    if not Result then
      Exclamar('A chave de acesso � inv�lida, verifique!');
  except
    Result := False;
  end;
end;

{ TCertificadoDigital }

constructor TCertificadoDigital.Create(pNumeroSerieCertificado: string);
begin
  FNumeroSerie := pNumeroSerieCertificado;
end;

function TCertificadoDigital.GetNumeroSerieCertificado: string;
var
  vStore: IStore3;
  vCerts: ICertificates2;
  vCerts2: ICertificates2;
  vCert: ICertificate2;
begin
  Result := '';
  vStore := CoStore.Create;
  vStore.Open(CAPICOM_CURRENT_USER_STORE, CAPICOM_STORE_NAME, CAPICOM_STORE_OPEN_READ_ONLY);

  vCerts := vStore.Certificates as ICertificates2;
  vCerts2 := vCerts.Select('Certificado(s) Digital(is) dispon�vel(is)', 'Selecione o Certificado Digital para uso no aplicativo', False);

  if not(vCerts2.Count = 0) then begin
    vCert := IInterface(vCerts2.Item[1]) as ICertificate2;
    FNumeroSerie    := string(vCert.SerialNumber);
    FDataVencimento := vCert.ValidToDate;
    FCNPJ           := vCert.SubjectName;
  end;

  Result := FNumeroSerie;
end;

function TCertificadoDigital.GetCertificadoCapicom: ICertificate2;
var
  vStore: IStore3;
  vCerts: ICertificates2;
  vCert: ICertificate2;
  Extension: IExtension;
  i: Integer;
  j: Integer;
  k: Integer;

  vXmldoc: IXMLDOMDocument3;
  vXmldsig: IXMLDigitalSignature;
  vDsigKey: IXMLDSigKey;
  vSigKey: IXMLDSigKeyEx;
  vPrivateKey: IPrivateKey;
  hCryptProvider: Cardinal;
  vXML: string;
  Propriedades: string;
  Lista: TStringList;

  vCertStoreMem: IStore3;
begin
  CoInitialize(nil); // PERMITE O USO DE THREAD
  try
    if FNumeroSerie = '' then
      raise Exception.Create('O n�mero de s�rie do certificado digital n�o configurado corretamente!');

    Result := nil;
    vStore := CoStore.Create;
    vStore.Open(CAPICOM_CURRENT_USER_STORE, CAPICOM_STORE_NAME, CAPICOM_STORE_OPEN_READ_ONLY);

    vCerts := vStore.Certificates as ICertificates2;
    for i := 1 to vCerts.Count do begin
      vCert := IInterface(vCerts.Item[i]) as ICertificate2;
      if vCert.SerialNumber <> FNumeroSerie then
        Continue;

      vPrivateKey := vCert.PrivateKey;
      if vCertStoreMem = nil then begin
        vCertStoreMem := CoStore.Create;
        vCertStoreMem.Open(CAPICOM_MEMORY_STORE, 'My', CAPICOM_STORE_OPEN_READ_ONLY);
        vCertStoreMem.Add(vCert);
        if (FSenhaCertificado <> '') and vPrivateKey.IsHardwareDevice then begin
          vXML := vXML + '<Signature xmlns="http://www.w3.org/2000/09/xmldsig#"><SignedInfo><CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"/><SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1" />';
          vXML := vXML + '<Reference URI="#">';
          vXML := vXML + '<Transforms><Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature" /><Transform Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" /></Transforms><DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1" />';
          vXML := vXML + '<DigestValue></DigestValue></Reference></SignedInfo><SignatureValue></SignatureValue><KeyInfo></KeyInfo></Signature>';

          vXmldoc                    := CoDOMDocument50.Create;
          vXmldoc.async              := False;
          vXmldoc.validateOnParse    := False;
          vXmldoc.preserveWhiteSpace := True;
          vXmldoc.loadXML(vXML);
          vXmldoc.setProperty('SelectionNamespaces', DSIGNS);

          vXmldsig           := CoMXDigitalSignature50.Create;
          vXmldsig.signature := vXmldoc.selectSingleNode('.//ds:Signature');
          vXmldsig.Store     := vCertStoreMem;

          vDsigKey := vXmldsig.createKeyFromCSP(vPrivateKey.ProviderType, vPrivateKey.ProviderName, vPrivateKey.ContainerName, 0);
          if (vDsigKey = nil) then
             raise Exception.Create('Erro ao criar a chave do CSP!');

          vSigKey := vDsigKey as IXMLDSigKeyEx;
          vSigKey.getCSPHandle( hCryptProvider );

          try
            CryptSetProvParam( hCryptProvider , PP_SIGNATURE_PIN, WinAPI.Windows.PBYTE(FSenhaCertificado), 0 );
          finally
            CryptReleaseContext(hCryptProvider, 0);
          end;

          vSigKey    := nil;
          vDsigKey   := nil;
          vXmldsig   := nil;
          vXmldoc    := nil;
        end;
      end;

      Result          := vCert;
      FDataVencimento := vCert.ValidToDate;
      for j := 1 to vCert.Extensions.Count do begin
        Extension := IInterface(vCert.Extensions.Item[J]) as IExtension;
        Propriedades := Extension.EncodedData.Format(True);
        if (Pos('2.16.76.1.3.3', Propriedades) > 0) then begin
          Lista := TStringList.Create;
          try
            Lista.Text := Propriedades;
            for K := 0 to Lista.Count - 1 do begin
              if Pos('2.16.76.1.3.3',Lista.Strings[K]) > 0 then begin
                FCNPJ := StringReplace(Lista.Strings[K],'2.16.76.1.3.3=','',[rfIgnoreCase]);
                FCNPJ := RetornaNumeros(HexToAscii(RemoverCaracter(FCNPJ, ' ')));

                Break;
              end;
            end;
          finally
            Lista.free;
          end;
          Break;
        end;
        Extension := nil;
      end;
      Break;
    end;

    if not(Assigned(Result)) then
      raise Exception.Create('O certificado digital n�o foi encontrado!');
  finally
    CoUninitialize;
  end;
end;

function AscIIToHex(const ABinaryString: AnsiString): string;
var
  I, L: Integer;
begin
  Result := '' ;
  L := Length(ABinaryString) ;
  for I := 1 to L do
     Result := Result + IntToHex(Ord(ABinaryString[I]), 2);
end;

function FDataHoraXML(pDataHoraEmissao: TDateTime): string;
begin
  Result := FormatDateTime('YYYY-MM-DD', pDataHoraEmissao) + 'T' + FormatDateTime('HH:NN:SS', pDataHoraEmissao) + '-03:00'
end;

function GetCodigoAmbiente(pAmbienteNFe: tpAmbienteNFE): string;
begin
  Result := IfThen(pAmbienteNFe = tpProducao, '1', '2');
end;

function GetURLQRCode(const pUF: string; const pAmbienteNFe: tpAmbienteNFE): string;
begin
  Result := '';
  if pUF = 'GO' then begin
    Result :=
      IfThen(
        pAmbienteNFe = tpHomologacao,
        'http://homolog.sefaz.go.gov.br/nfeweb/sites/nfce/danfeNFCe',
        'http://nfe.sefaz.go.gov.br/nfeweb/sites/nfce/danfeNFCe'
      );
  end
  else if pUF = 'MA' then begin
    Result :=
      IfThen(
        pAmbienteNFe = tpHomologacao,
        'http://www.nfce.sefaz.ma.gov.br/portal/consultarNFCe.jsp',
        'http://www.nfce.sefaz.ma.gov.br/portal/consultarNFCe.jsp'
      );
  end;
end;

(*
  Conforme manual de padr�es t�cnico QR Code Vers�o 2
*)
function GetURLQRCode(
  const pUF: string;
  const pAmbienteNFe: tpAmbienteNFE;
  const pChaveNFe: string;
  const pCPFDestinatario: String;
  const pDataHoraEmissao: String;
  const pValorTotalNF: String;
  const pValorTotalICMS: String;
  const pDigestValue: string;
  const pCSC: string;
  const pIdToken: string
): string;
var
  vUrl: string;
  vHashQRCode: string;
  vUrlAuxiliar: string;

  procedure Add(pTexto: string);
  begin
    vUrlAuxiliar := vUrlAuxiliar + pTexto;
  end;

begin
  vUrlAuxiliar := '';
  vUrl := GetURLQRCode(pUF, pAmbienteNFe);

  // Posi��o 1
  Add(_Biblioteca.RetornaNumeros(pChaveNFe));

  // Posi��o 2
  Add('|2');

  // Posi��o 3
  Add('|' + GetCodigoAmbiente(pAmbienteNFe));

  // Posi��o 4
  Add('|' + pIdToken);

  vHashQRCode := AscIIToHex( SHA1( AnsiString(vUrlAuxiliar + pCSC)) );

  // Posi��o 5
  Add('|' + UpperCase(vHashQRCode));

  Result := vUrl + '?p=' + vUrlAuxiliar;
end;

function getNoXMLDocument(const pNome: string; pRoot: IXMLNode): IXMLNode;
var
  i: Integer;
begin
  i := 0;
  Result := nil;
  while (i < pRoot.ChildNodes.Count) and (Result = nil) do begin
    if SameText(pRoot.ChildNodes.Nodes[i].NodeName, pNome) then
      Result := pRoot.ChildNodes.Nodes[i]
    else begin
      Result := getNoXMLDocument(pNome, pRoot.ChildNodes.Nodes[i]);
      Inc(i);
    end;
  end;
end;

function getXMLBase64(pTexto: AnsiString): string;
type
 TCompressType = ( ctUnknown, ctZLib, ctGZip, ctZipFile );

var
  vXml: AnsiString;

  function ZipFileDeCompress(inStream, outStream: TStream): Boolean;
  var
    Z: TZipFile;
    s: TStream;
    h: TZipHeader;
  begin
    z := TZipFile.Create;
    try
      z.Open(inStream, zmRead);
      z.Read(0, s, h);
      try
        outStream.CopyFrom(s, s.Size);
        Result := True;
      finally
        s.Free;
      end;
    finally
      z.Free;
    end;
  end;

  function DetectCompressType(AStream: TStream): TCompressType;
  var
    hdr: LongWord;
    OldPos: Int64;
  begin
    hdr := 0;
    OldPos := AStream.Position;
    AStream.Position := 0;
    AStream.ReadBuffer(hdr, 4);
    AStream.Position := OldPos;

    if (hdr and $88B1F) = $88B1F then
      Result := ctGZip
    else if (hdr and $9C78) = $9C78 then
      Result := ctZLib
    else if (hdr and $4034B50) = $4034B50 then
      Result := ctZipFile
    else
      Result := ctUnknown;
  end;

  function DeCompress(inStream, outStream: TStream): Boolean; overload;
  begin
    if (DetectCompressType(inStream) = ctZipFile) then
      Result := ZipFileDeCompress(inStream, outStream)
    else
      Result := GZIPUtils.unzipStream(inStream, outStream);
  end;

  function DeCompress(AStream: TStream): AnsiString; overload;
  var
    outMemStream: TMemoryStream;
  begin
    outMemStream := TMemoryStream.Create;
    try
      AStream.Position := 0;
      DeCompress(AStream, outMemStream);

      outMemStream.Position := 0;
      Result := ReadStrFromStream(outMemStream, outMemStream.Size);
    finally
      outMemStream.Free;
    end;
  end;

  function DeCompress(const ABinaryString: AnsiString): AnsiString; overload;
  var
    MS: TMemoryStream;
  begin
    MS := TMemoryStream.Create;
    try
      WriteStrToStream(MS, ABinaryString);
      MS.Position := 0;
      Result := DeCompress(MS);
    finally
      MS.Free;
    end;
  end;

begin
  vXml := synacode.DecodeBase64( pTexto );
  vXml := DeCompress(vXml);
  Result := string(vXml);
end;

function trataTagTextoNFe(pTexto: string): string;
begin
  Result := StringReplace(_Biblioteca.RemoverCaracteresEspeciais(pTexto), #13, ' ', [rfReplaceAll]);
  Result := StringReplace(_Biblioteca.RemoverCaracteresEspeciais(Result), #10, '', [rfReplaceAll]);
end;

{ RecItemNFe }

procedure RecItemNFe.Iniciar;
begin
  ItemId                    := 0;
  CfopId                    := '';
  CodigoOriginal            := '';
  NomeProduto               := '';
  NomeMarca                 := '';
  Unidade                   := '';
  OrigemProduto             := 0;
  CodigoNcm                 := '';
  Cest                      := '';
  ValorTotalOutrasDespesas  := 0;
  ValorTotalDesconto        := 0;
  Quantidade                := 0;
  PrecoUnitario             := 0;
  ValorTotal                := 0;

  Cst                       := '';
  IndiceReducaoBaseIcms     := 0;
  BaseCalculoIcms           := 0;
  PercentualIcms            := 0;
  ValorIcms                 := 0;

  IndiceReducaoBaseIcmsSt   := 0;
  BaseCalculoIcmsSt         := 0;
  PercentualIcmsSt          := 0;
  ValorIcmsSt               := 0;

  CstPis                    := '';
  BaseCalculoPis            := 0;
  PercentualPis             := 0;
  ValorPis                  := 0;

  CstCofins                 := '';
  BaseCalculoCofins         := 0;
  PercentualCofins          := 0;
  ValorCofins               := 0;

  PercentualIpi             := 0;
  ValorIpi                  := 0;

  ValorFrete                := 0;
  Informacoes_adicionais    := '';

  CodigoBarras              := '';
  Iva                       := 0;
  PrecoPauta                := 0;
end;

end.
