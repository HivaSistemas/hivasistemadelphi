unit _RecordsNFE;

interface

type
  RecResumoNFe = record
    ChaveNFe: string;
    CNPJ: string;
    RazaoSocial: string;
    InscricaoEstadual: string;
    ValorNota: Currency;
    StatusNota: string;
    DataEmissao: TDateTime;
    NumeroNota: Integer;
    SerieNota: string;
    XmlNFe: string;
  end;

  RecCTe = record
    ChaveCTe: string;
    CNPJRemetente: string;
    CFOP: Integer;
    RazaoSocialRemetente: string;
    InscricaoEstadualRemetente: string;
    CNPJEmitente: string;
    RazaoSocialEmitente: string;
    InscricaoEstadualEmitente: string;
    ValorTotalCTe: Currency;
    BaseCalculoICMS: Currency;
    PercentualIcms: Currency;
    ValorICMS: Currency;
    DataEmissao: TDateTime;
    NumeroCTe: Integer;
    SerieCTe: string;
    XmlCTe: string;

    StatusSefaz: string;

    NFEsReferenciadas: TArray<string>;
  end;

  RecProtocoloNFE = record
    chave_acesso_nfe: string;
    status: string;
    motivo: string;
    data_hora_recibo: TDateTime;
    numero_protocolo: string;
    digest_value: string;
    signature: string;
  end;

  RecRespostaNFE = record
    HouveErro: Boolean;
    mensagem_erro: string;
    tipo_ambiente: string;
    versao_aplicacao: string;
    status: string;
    motivo: string;
    uf: string;
    data_hora_recebimento: TDateTime;
    tempo_medio: string;
    data_hora_retorno: TDateTime;
    observacao: string;
    numero_recibo: string;

    protocolo: RecProtocoloNFE;
    caminho_xml_nfe: string;
    textoXML: string;
  end;

  RecRespostaNFE<T> = record
    HouveErro: Boolean;
    mensagem_erro: string;
    tipo_ambiente: string;
    versao_aplicacao: string;
    status: string;
    motivo: string;
    uf: string;
    data_hora_recebimento: TDateTime;
    data_hora_retorno: TDateTime;
    observacao: string;
    numero_recibo: string;
    ultimo_nsu: string;
    Dados: TArray<T>;
  end;

  RecRespostaCTe = record
    HouveErro: Boolean;
    MensagemErro: string;
    TipoAmbiente: string;
    VersaoAplicacao: string;
    Status: string;
    motivo: string;
    uf: string;
    Observacao: string;
    NumeroRecibo: string;
    UltimoNSU: string;
    CTEs: TArray<RecCTe>;
  end;

  RecDadosGeracaoNFe = record
    numero_nota: Integer;
    data_hora_emissao: TDateTime;
  end;

implementation

end.
