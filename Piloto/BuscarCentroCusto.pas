unit BuscarCentroCusto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, _Biblioteca,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameCentroCustos,
  Vcl.Buttons, Vcl.ExtCtrls;

type
  TFormBuscarCentroCusto = class(TFormHerancaFinalizar)
    FrCentrosCustos: TFrCentroCustos;
    procedure FormShow(Sender: TObject);
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Buscar(): TRetornoTelaFinalizar<Integer>;

implementation

{$R *.dfm}

function Buscar(): TRetornoTelaFinalizar<Integer>;
var
  vForm: TFormBuscarCentroCusto;
begin
  vForm := TFormBuscarCentroCusto.Create(Application);

  if Result.Ok(vForm.ShowModal, True) then
    Result.Dados := vForm.FrCentrosCustos.GetCentro().CentroCustoId;
end;

{ TFormBuscarPlanoFinanceiro }

procedure TFormBuscarCentroCusto.Finalizar(Sender: TObject);
begin
  inherited;

end;

procedure TFormBuscarCentroCusto.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrCentrosCustos);
end;

procedure TFormBuscarCentroCusto.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrCentrosCustos.EstaVazio then begin
    _Biblioteca.Exclamar('O centro de custos n�o foi informado corretamente, verifique!');
    SetarFoco(FrCentrosCustos);
    Abort;
  end;
end;

end.
