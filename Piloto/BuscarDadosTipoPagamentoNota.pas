unit BuscarDadosTipoPagamentoNota;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.StdCtrls, EditLuka, _NotasFiscais, _Biblioteca;

type
  TFormBuscarDadosTipoPagamentoNota = class(TFormHerancaFinalizar)
    lbllb11: TLabel;
    eTotalPago: TEditLuka;
    Label2: TLabel;
    eValorDinheiro: TEditLuka;
    Label3: TLabel;
    eValorCheque: TEditLuka;
    Label4: TLabel;
    eValorCartaoDebito: TEditLuka;
    Label5: TLabel;
    eValorCartaoCredito: TEditLuka;
    Label6: TLabel;
    eValorCobranca: TEditLuka;
    Label7: TLabel;
    eValorPix: TEditLuka;
    Label8: TLabel;
    eValorAcumulativo: TEditLuka;
    Label9: TLabel;
    eValorCredito: TEditLuka;
    Label10: TLabel;
    eValorTroco: TEditLuka;
    procedure eValorDinheiroDblClick(Sender: TObject);
    procedure eValorDinheiroChange(Sender: TObject);
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  private
    { Private declarations }
  public
    procedure TotalizarValorASerPago;
  end;

function BuscarDadosTipoPagamento(valorPago: Double): TRetornoTelaFinalizar<RecValoresNotaFiscal>;

implementation

{$R *.dfm}

function BuscarDadosTipoPagamento(valorPago: Double): TRetornoTelaFinalizar<RecValoresNotaFiscal>;
var
  vForm: TFormBuscarDadosTipoPagamentoNota;
begin
  vForm := TFormBuscarDadosTipoPagamentoNota.Create(Application);
  vForm.eTotalPago.AsDouble := valorPago;
  vForm.eValorTroco.AsDouble := valorPago;
  SetarFoco(vForm.eValorDinheiro);

  if Result.Ok(vForm.ShowModal) then begin
    Result.Dados.valorDinheiro      := vForm.eValorDinheiro.AsDouble;
    Result.Dados.valorAcumulado     := vForm.eValorAcumulativo.AsDouble;
    Result.Dados.valorCartaoCredito := vForm.eValorCartaoCredito.AsDouble;
    Result.Dados.valorCartaoDebito  := vForm.eValorCartaoCredito.AsDouble;
    Result.Dados.valorCheque        := vForm.eValorCheque.AsDouble;
    Result.Dados.valorCobranca      := vForm.eValorCobranca.AsDouble;
    Result.Dados.valorCredito       := vForm.eValorCredito.AsDouble;
    Result.Dados.valorPix           := vForm.eValorPix.AsDouble;
  end;

  FreeAndNil(vForm);
end;

procedure TFormBuscarDadosTipoPagamentoNota.eValorDinheiroChange(
  Sender: TObject);
begin
  inherited;
  TotalizarValorASerPago;
end;

procedure TFormBuscarDadosTipoPagamentoNota.eValorDinheiroDblClick(Sender: TObject);
begin
  inherited;
  if TEditLuka(Sender).AsCurr > 0 then
    TEditLuka(Sender).AsCurr := 0
  else
    TEditLuka(Sender).AsCurr := Abs(eValorTroco.AsCurr);
end;

procedure TFormBuscarDadosTipoPagamentoNota.TotalizarValorASerPago;
var
  vTotalFormasPagamento: Currency;
begin
  vTotalFormasPagamento      := eValorDinheiro.AsCurr + eValorCheque.AsCurr + eValorCartaoDebito.AsCurr + eValorCartaoCredito.AsCurr + eValorCobranca.AsCurr + eValorAcumulativo.AsCurr + eValorCredito.AsCurr + eValorPix.AsCurr;
  eValorTroco.AsCurr         := vTotalFormasPagamento - eTotalPago.AsCurr;

  inherited;
end;

procedure TFormBuscarDadosTipoPagamentoNota.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eValorTroco.AsDouble > 0 then begin
    Exclamar('� necess�rio que a soma dos tipos de pagamento seja igual ao valor total pago');
    SetarFoco(eValorDinheiro);
    Abort;
  end;
end;

end.
