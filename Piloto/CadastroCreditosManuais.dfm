inherited FormCadastroCreditosManuais: TFormCadastroCreditosManuais
  Caption = 'Cadastro de cr'#233'ditos manuais a pagar'
  ClientHeight = 313
  ClientWidth = 709
  ExplicitWidth = 715
  ExplicitHeight = 342
  PixelsPerInch = 96
  TextHeight = 14
  object lbl1: TLabel [1]
    Left = 126
    Top = 48
    Width = 86
    Height = 14
    Caption = 'Valor do cr'#233'dito'
  end
  object lbllb12: TLabel [2]
    Left = 126
    Top = 86
    Width = 81
    Height = 14
    Caption = 'Dt. vencimento'
  end
  object lbl2: TLabel [3]
    Left = 255
    Top = 86
    Width = 69
    Height = 14
    Caption = 'Observa'#231#245'es'
  end
  inherited pnOpcoes: TPanel
    Height = 313
    TabOrder = 9
    ExplicitHeight = 313
    inherited sbExcluir: TSpeedButton
      Visible = False
    end
    inherited sbPesquisar: TSpeedButton
      Visible = False
    end
  end
  inherited eID: TEditLuka
    TabOrder = 0
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 710
    TabOrder = 10
    Visible = False
    ExplicitLeft = 710
  end
  inline FrEmpresa: TFrEmpresas
    Left = 212
    Top = 3
    Width = 239
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 212
    ExplicitTop = 3
    ExplicitWidth = 239
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 214
      Height = 23
      ExplicitWidth = 214
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 239
      ExplicitWidth = 239
      inherited lbNomePesquisa: TLabel
        Width = 47
        Caption = 'Empresa'
        ExplicitWidth = 47
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 169
        ExplicitLeft = 169
      end
    end
    inherited pnPesquisa: TPanel
      Left = 214
      Height = 24
      ExplicitLeft = 214
      ExplicitHeight = 24
    end
  end
  object eValorCreditoGerar: TEditLuka
    Left = 126
    Top = 62
    Width = 123
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    Text = '0,00'
    OnChange = eValorCreditoGerarChange
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  inline FrPlanoFinanceiro: TFrPlanosFinanceiros
    Left = 256
    Top = 45
    Width = 248
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 4
    TabStop = True
    ExplicitLeft = 256
    ExplicitTop = 45
    ExplicitWidth = 248
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 223
      Height = 23
      ExplicitWidth = 223
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 248
      ExplicitWidth = 248
      inherited lbNomePesquisa: TLabel
        Width = 88
        Caption = 'Plano financeiro'
        ExplicitWidth = 88
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 178
        ExplicitLeft = 178
      end
    end
    inherited pnPesquisa: TPanel
      Left = 223
      Height = 24
      ExplicitLeft = 223
      ExplicitHeight = 24
    end
  end
  inline FrCentroCusto: TFrCentroCustos
    Left = 506
    Top = 45
    Width = 198
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 5
    TabStop = True
    ExplicitLeft = 506
    ExplicitTop = 45
    ExplicitWidth = 198
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 173
      Height = 23
      ExplicitWidth = 173
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 198
      ExplicitWidth = 198
      inherited lbNomePesquisa: TLabel
        Width = 90
        ExplicitWidth = 90
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 128
        ExplicitLeft = 128
      end
    end
    inherited pnPesquisa: TPanel
      Left = 173
      Height = 24
      ExplicitLeft = 173
      ExplicitHeight = 24
    end
  end
  object ckImportacao: TCheckBox
    Left = 150
    Top = 128
    Width = 85
    Height = 17
    Caption = 'Importa'#231#227'o'
    TabOrder = 8
    OnClick = ckImportacaoClick
    OnKeyDown = ProximoCampo
  end
  object eDataVencimento: TEditLukaData
    Left = 126
    Top = 100
    Width = 123
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 6
    Text = '  /  /    '
    OnKeyDown = ProximoCampo
  end
  object meObservacoes: TMemoAltis
    Left = 255
    Top = 100
    Width = 428
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 7
    OnKeyDown = ProximoCampo
  end
  inline FrCadastro: TFrameCadastros
    Left = 455
    Top = 3
    Width = 249
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 455
    ExplicitTop = 3
    ExplicitWidth = 249
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 224
      Height = 23
      ExplicitWidth = 224
      ExplicitHeight = 23
    end
    inherited CkAspas: TCheckBox
      Left = 9
      Top = 22
      ExplicitLeft = 9
      ExplicitTop = 22
    end
    inherited PnTitulos: TPanel
      Width = 249
      ExplicitWidth = 249
      inherited lbNomePesquisa: TLabel
        Width = 39
        Caption = 'Cliente'
        ExplicitWidth = 39
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 179
        ExplicitLeft = 179
      end
    end
    inherited pnPesquisa: TPanel
      Left = 224
      Height = 24
      ExplicitLeft = 224
      ExplicitHeight = 24
    end
  end
  inline FrPagamentoFinanceiro: TFrPagamentoFinanceiro
    Left = 240
    Top = 125
    Width = 303
    Height = 186
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 11
    TabStop = True
    ExplicitLeft = 240
    ExplicitTop = 125
    ExplicitHeight = 186
    inherited grpFechamento: TGroupBox
      Height = 186
      ExplicitHeight = 186
      inherited lbllb11: TLabel
        Top = 63
        ExplicitTop = 63
      end
      inherited lbllb12: TLabel
        Top = 83
        ExplicitTop = 83
      end
      inherited lbllb13: TLabel
        Left = 24
        Top = 103
        Width = 86
        ExplicitLeft = 24
        ExplicitTop = 103
        ExplicitWidth = 86
      end
      inherited lbllb14: TLabel
        Top = 143
        ExplicitTop = 143
      end
      inherited lbllb15: TLabel
        Left = -238
        Visible = False
        ExplicitLeft = -238
      end
      inherited sbBuscarDadosCheques: TSpeedButton
        Top = 79
        ExplicitTop = 79
      end
      inherited sbBuscarDadosCartoesDebito: TSpeedButton
        Top = 99
        ExplicitTop = 99
      end
      inherited sbBuscarDadosCobranca: TSpeedButton
        Top = 140
        ExplicitTop = 140
      end
      inherited lbllb10: TLabel
        Top = 164
        ExplicitTop = 164
      end
      inherited lbllb21: TLabel
        Left = 315
        Visible = False
        ExplicitLeft = 315
      end
      inherited lbllb7: TLabel
        Left = 553
        Width = 9
        Height = 14
        Visible = False
        ExplicitLeft = 553
        ExplicitWidth = 9
        ExplicitHeight = 14
      end
      inherited lbllb19: TLabel
        Top = 20
        Font.Color = clBlue
        ExplicitTop = 20
      end
      inherited lbllb1: TLabel
        Left = 315
        Visible = False
        ExplicitLeft = 315
      end
      inherited sbBuscarDadosDinheiro: TSpeedButton
        Top = 59
        ExplicitTop = 59
      end
      inherited sbBuscarDadosCreditos: TSpeedButton
        Left = -79
        Visible = False
        ExplicitLeft = -79
      end
      inherited lb4: TLabel
        Top = 123
        ExplicitTop = 123
      end
      inherited sbBuscarDadosCartoesCredito: TSpeedButton
        Top = 119
        ExplicitTop = 119
      end
      inherited lb1: TLabel
        Left = 307
        Visible = False
        ExplicitLeft = 307
      end
      inherited eValorDinheiro: TEditLuka
        Top = 56
        Height = 22
        ExplicitTop = 56
        ExplicitHeight = 22
      end
      inherited eValorCheque: TEditLuka
        Top = 76
        Height = 22
        ExplicitTop = 76
        ExplicitHeight = 22
      end
      inherited eValorCartaoDebito: TEditLuka
        Top = 96
        Height = 22
        ExplicitTop = 96
        ExplicitHeight = 22
      end
      inherited eValorCobranca: TEditLuka
        Top = 136
        Height = 22
        ExplicitTop = 136
        ExplicitHeight = 22
      end
      inherited eValorCredito: TEditLuka
        Left = -176
        Height = 22
        Visible = False
        ExplicitLeft = -176
        ExplicitHeight = 22
      end
      inherited stPagamento: TStaticText
        Top = 38
        ExplicitTop = 38
      end
      inherited eValorDiferencaPagamentos: TEditLuka
        Top = 157
        Height = 22
        ExplicitTop = 157
        ExplicitHeight = 22
      end
      inherited eValorDesconto: TEditLuka
        Left = 416
        Height = 22
        Visible = False
        ExplicitLeft = 416
        ExplicitHeight = 22
      end
      inherited ePercentualDesconto: TEditLuka
        Left = 512
        Height = 22
        Visible = False
        ExplicitLeft = 512
        ExplicitHeight = 22
      end
      inherited eValorTotalASerPago: TEditLuka
        Top = 12
        Font.Color = clBlue
        ExplicitTop = 12
      end
      inherited eValorJuros: TEditLuka
        Left = 416
        Height = 22
        Visible = False
        ExplicitLeft = 416
        ExplicitHeight = 22
      end
      inherited eValorCartaoCredito: TEditLuka
        Top = 116
        Height = 22
        ExplicitTop = 116
        ExplicitHeight = 22
      end
      inherited stValorTotal: TStaticText
        Top = 38
        ExplicitTop = 38
      end
      inherited stDinheiroDefinido: TStaticText
        Top = 60
        ExplicitTop = 60
      end
      inherited stChequeDefinido: TStaticText
        Top = 80
        ExplicitTop = 80
      end
      inherited stCartaoDebitoDefinido: TStaticText
        Top = 100
        ExplicitTop = 100
      end
      inherited stCobrancaDefinido: TStaticText
        Top = 140
        ExplicitTop = 140
      end
      inherited stCreditoDefinido: TStaticText
        Left = -60
        Visible = False
        ExplicitLeft = -60
      end
      inherited stCartaoCreditoDefinido: TStaticText
        Top = 120
        ExplicitTop = 120
      end
      inherited eValorMulta: TEditLuka
        Left = 408
        Height = 22
        Visible = False
        ExplicitLeft = 408
        ExplicitHeight = 22
      end
    end
  end
end
