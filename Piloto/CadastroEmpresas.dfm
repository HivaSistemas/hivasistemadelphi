inherited FormCadastroEmpresas: TFormCadastroEmpresas
  Caption = 'Cadastro de loja'
  ClientHeight = 310
  ClientWidth = 721
  ExplicitWidth = 727
  ExplicitHeight = 339
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 126
    Top = 48
    Width = 69
    Height = 14
    Caption = 'Raz'#227'o social'
  end
  object lb2: TLabel [1]
    Left = 404
    Top = 48
    Width = 81
    Height = 14
    Caption = 'Nome fantasia'
  end
  object lbCPF_CNPJ: TLabel [2]
    Left = 242
    Top = 89
    Width = 23
    Height = 14
    Caption = 'CNPJ'
  end
  object lb9: TLabel [3]
    Left = 126
    Top = 268
    Width = 35
    Height = 14
    Caption = 'E-mail'
  end
  object lb6: TLabel [4]
    Left = 383
    Top = 89
    Width = 101
    Height = 14
    Caption = 'Inscri'#231#227'o estadual'
  end
  object lb3: TLabel [5]
    Left = 499
    Top = 89
    Width = 107
    Height = 14
    Caption = 'Inscri'#231#227'o municipal'
  end
  object lb7: TLabel [6]
    Left = 624
    Top = 89
    Width = 27
    Height = 14
    Caption = 'CNAE'
  end
  object lb17: TLabel [7]
    Left = 126
    Top = 132
    Width = 80
    Height = 14
    Caption = 'Data funda'#231#227'o'
  end
  object lb11: TLabel [8]
    Left = 229
    Top = 132
    Width = 48
    Height = 14
    Caption = 'Telefone'
  end
  object lb13: TLabel [9]
    Left = 355
    Top = 132
    Width = 56
    Height = 14
    Caption = 'Whatsapp'
  end
  object lb4: TLabel [11]
    Left = 126
    Top = 89
    Width = 101
    Height = 14
    Caption = 'Nome res. impr. nf.'
  end
  inherited pnOpcoes: TPanel
    Height = 310
    TabOrder = 15
    ExplicitHeight = 310
  end
  inherited eID: TEditLuka
    TabOrder = 0
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 668
    Top = 8
    TabOrder = 16
    Visible = False
    ExplicitLeft = 668
    ExplicitTop = 8
  end
  object eRazaoSocial: TEditLuka
    Left = 126
    Top = 62
    Width = 273
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 2
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eNomeFantasia: TEditLuka
    Left = 404
    Top = 62
    Width = 273
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 3
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eCNPJ: TEditCPF_CNPJ_Luka
    Left = 242
    Top = 103
    Width = 136
    Height = 22
    EditMask = '99.999.999/9999-99'
    MaxLength = 18
    TabOrder = 5
    Text = '  .   .   /    -  '
    OnKeyDown = ProximoCampo
    Tipo = [tccCNPJ]
  end
  inline FrEndereco: TFrEndereco
    Left = 124
    Top = 174
    Width = 596
    Height = 95
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 12
    TabStop = True
    ExplicitLeft = 124
    ExplicitTop = 174
    ExplicitWidth = 596
    ExplicitHeight = 95
    inherited lb8: TLabel
      Left = 2
      Width = 61
      Height = 14
      ExplicitLeft = 2
      ExplicitWidth = 61
      ExplicitHeight = 14
    end
    inherited lb9: TLabel
      Left = 303
      Width = 76
      Height = 14
      ExplicitLeft = 303
      ExplicitWidth = 76
      ExplicitHeight = 14
    end
    inherited lb10: TLabel
      Left = 527
      Width = 20
      Height = 14
      ExplicitLeft = 527
      ExplicitWidth = 20
      ExplicitHeight = 14
    end
    inherited lb1: TLabel
      Left = 303
      Width = 107
      Height = 14
      ExplicitLeft = 303
      ExplicitWidth = 107
      ExplicitHeight = 14
    end
    inherited lb2: TLabel
      Left = 527
      Width = 43
      Height = 14
      ExplicitLeft = 527
      ExplicitWidth = 43
      ExplicitHeight = 14
    end
    inherited eCEP: TEditCEPLuka
      Left = 527
      Height = 22
      TabOrder = 10
      OnKeyDown = ProximoCampoForaFrame
      ExplicitLeft = 527
      ExplicitHeight = 22
    end
    inherited eLogradouro: TEditLuka
      Left = 0
      Width = 295
      Height = 22
      TabOrder = 1
      ExplicitLeft = 0
      ExplicitWidth = 295
      ExplicitHeight = 22
    end
    inherited eComplemento: TEditLuka
      Left = 303
      Height = 22
      TabOrder = 2
      ExplicitLeft = 303
      ExplicitHeight = 22
    end
    inherited FrBairro: TFrBairros
      Width = 292
      TabOrder = 7
      ExplicitWidth = 292
      inherited sgPesquisa: TGridLuka
        Width = 267
        ExplicitWidth = 267
      end
      inherited CkAspas: TCheckBox
        TabOrder = 3
      end
      inherited CkFiltroDuplo: TCheckBox
        TabOrder = 5
      end
      inherited CkPesquisaNumerica: TCheckBox
        TabOrder = 7
      end
      inherited CkMultiSelecao: TCheckBox
        TabOrder = 4
      end
      inherited PnTitulos: TPanel
        Width = 292
        ExplicitWidth = 292
        inherited lbNomePesquisa: TLabel
          Height = 15
        end
        inherited pnSuprimir: TPanel
          Left = 187
          ExplicitLeft = 187
        end
      end
      inherited pnPesquisa: TPanel
        Left = 267
        TabOrder = 2
        ExplicitLeft = 267
      end
      inherited ckSomenteAtivos: TCheckBox
        TabOrder = 6
      end
    end
    inherited ckValidarComplemento: TCheckBox
      Width = 147
      Checked = True
      State = cbChecked
      TabOrder = 0
      ExplicitWidth = 147
    end
    inherited ckValidarBairro: TCheckBox
      Checked = True
      State = cbChecked
      TabOrder = 6
    end
    inherited ckValidarCEP: TCheckBox
      Checked = True
      State = cbChecked
      TabOrder = 4
    end
    inherited ckValidarEndereco: TCheckBox
      Width = 147
      Checked = True
      State = cbChecked
      TabOrder = 8
      ExplicitWidth = 147
    end
    inherited ePontoReferencia: TEditLuka
      Left = 303
      Height = 22
      TabOrder = 9
      ExplicitLeft = 303
      ExplicitHeight = 22
    end
    inherited ckValidarPontoReferencia: TCheckBox
      Checked = True
      State = cbChecked
      TabOrder = 11
    end
    inherited eNumero: TEditLuka
      Left = 527
      Height = 22
      TabOrder = 3
      ExplicitLeft = 527
      ExplicitHeight = 22
    end
    inherited ckValidarNumero: TCheckBox
      Checked = True
      State = cbChecked
      TabOrder = 5
    end
  end
  object eEmail: TEditLuka
    Left = 126
    Top = 281
    Width = 293
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 13
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eInscricaoEstadual: TEditLuka
    Left = 383
    Top = 103
    Width = 112
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 6
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eInscricaoMunicipal: TEditLuka
    Left = 499
    Top = 103
    Width = 119
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 7
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eCNAE: TMaskEdit
    Left = 624
    Top = 103
    Width = 94
    Height = 22
    EditMask = '99.99-9-99;1;_'
    MaxLength = 10
    TabOrder = 8
    Text = '  .  - -  '
    OnKeyDown = ProximoCampo
  end
  object eDataFundacao: TEditLukaData
    Left = 126
    Top = 147
    Width = 98
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1; '
    MaxLength = 10
    TabOrder = 9
    Text = '  /  /    '
    OnKeyDown = ProximoCampo
  end
  object eTelefone: TEditTelefoneLuka
    Left = 229
    Top = 147
    Width = 121
    Height = 22
    EditMask = '(99) 9999-9999;1; '
    MaxLength = 14
    TabOrder = 10
    Text = '(  )     -    '
    OnKeyDown = ProximoCampo
  end
  object eFax: TEditTelefoneLuka
    Left = 355
    Top = 147
    Width = 117
    Height = 22
    EditMask = '(99) 99999-9999;1; '
    MaxLength = 15
    TabOrder = 11
    Text = '(  )      -    '
    OnKeyDown = ProximoCampo
  end
  inline FrCadastro: TFrameCadastros
    Left = 425
    Top = 265
    Width = 290
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 14
    TabStop = True
    ExplicitLeft = 425
    ExplicitTop = 265
    ExplicitWidth = 290
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 265
      Height = 24
      ExplicitWidth = 265
      ExplicitHeight = 24
    end
    inherited CkAspas: TCheckBox
      Left = 3
      Top = 22
      ExplicitLeft = 3
      ExplicitTop = 22
    end
    inherited PnTitulos: TPanel
      Width = 290
      ExplicitWidth = 290
      inherited lbNomePesquisa: TLabel
        Width = 184
        Caption = 'C'#243'digo da empresa nos cadastros'
        ExplicitWidth = 184
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 185
        ExplicitLeft = 185
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 265
      Height = 25
      ExplicitLeft = 265
      ExplicitHeight = 25
    end
  end
  object rgTipoEmpresa: TRadioGroupLuka
    Left = 211
    Top = 5
    Width = 504
    Height = 38
    Caption = ' Tipo de empresa '
    Columns = 4
    Items.Strings = (
      'Matriz'
      'Filial'
      'Dep'#243'sito fechado'
      'CD')
    TabOrder = 1
    Valores.Strings = (
      'M'
      'F'
      'D'
      'C')
  end
  object eNomeResumidoImpressoesNaoFiscais: TEditLuka
    Left = 126
    Top = 103
    Width = 110
    Height = 22
    Hint = 
      'Nome resumido que ir'#225' aparecer no cabe'#231'alho das impress'#245'es n'#227'o f' +
      'iscais'
    CharCase = ecUpperCase
    TabOrder = 4
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
