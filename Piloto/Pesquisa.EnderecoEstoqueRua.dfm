inherited FormPesquisaEnderecoEstoqueRua: TFormPesquisaEnderecoEstoqueRua
  Caption = 'Pesquisa endere'#231'o de estoque rua'
  ClientWidth = 458
  ExplicitWidth = 466
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 458
    OnDrawCell = sgPesquisaDrawCell
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Descri'#231#227'o')
    OnGetCellColor = sgPesquisaGetCellColor
    ColWidths = (
      28
      55
      336)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Text = '0'
  end
  inherited pnFiltro1: TPanel
    Width = 458
    inherited lblPesquisa: TLabel
      Left = 162
      ExplicitLeft = 162
    end
    inherited eValorPesquisa: TEditLuka
      Left = 162
      Width = 296
      ExplicitLeft = 162
      ExplicitWidth = 227
    end
    inherited cbOpcoesPesquisa: TComboBoxLuka
      Width = 151
      ExplicitWidth = 151
    end
  end
end
