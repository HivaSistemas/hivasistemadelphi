inherited FrFaixaNumeros: TFrFaixaNumeros
  Width = 190
  Height = 41
  ExplicitWidth = 190
  ExplicitHeight = 41
  object lb1: TLabel
    Left = 3
    Top = 1
    Width = 39
    Height = 13
    Caption = 'Valor de'
  end
  object lb2: TLabel
    Left = 89
    Top = 20
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object eValor1: TEditLuka
    Left = 1
    Top = 14
    Width = 86
    Height = 21
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 0
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValor2: TEditLuka
    Left = 106
    Top = 14
    Width = 84
    Height = 21
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 1
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
end
