inherited FormCadastroMotorista: TFormCadastroMotorista
  Caption = 'Cadastro de motorista'
  ClientHeight = 406
  ClientWidth = 800
  ExplicitWidth = 806
  ExplicitHeight = 435
  PixelsPerInch = 96
  TextHeight = 14
  inherited lbRazaoSocialNome: TLabel
    Left = 324
    ExplicitLeft = 324
  end
  inherited lbCPF_CNPJ: TLabel
    Left = 209
    ExplicitLeft = 209
  end
  inherited Label2: TLabel
    Left = 693
    ExplicitLeft = 693
  end
  inherited pnOpcoes: TPanel
    Height = 406
    ExplicitHeight = 406
    inherited sbLogs: TSpeedButton
      Top = 356
      ExplicitTop = 356
    end
  end
  inherited ckAtivo: TCheckBoxLuka
    Top = 20
    ExplicitTop = 20
  end
  inherited eRazaoSocial: TEditLuka
    Left = 324
    Width = 275
    ExplicitLeft = 324
    ExplicitWidth = 275
  end
  inherited eCPF_CNPJ: TEditCPF_CNPJ_Luka
    Left = 209
    ExplicitLeft = 209
  end
  inherited rgTipoPessoa: TRadioGroupLuka
    Width = 130
    ExplicitWidth = 130
  end
  inherited pcDados: TPageControl
    inherited tsPrincipais: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 670
      ExplicitHeight = 290
      inherited lb12: TLabel
        Left = 386
        ExplicitLeft = 386
      end
      object lbNumeroCNH: TLabel [7]
        Left = 340
        Top = 173
        Width = 68
        Height = 14
        Caption = 'N'#250'mero CNH'
      end
      object Label3: TLabel [8]
        Left = 565
        Top = 173
        Width = 73
        Height = 14
        Caption = 'Data val. CNH'
      end
      inherited lb13: TLabel
        Left = 518
        ExplicitLeft = 518
      end
      inherited rgSexo: TRadioGroupLuka
        Width = 173
        ExplicitWidth = 173
      end
      inherited FrEndereco: TFrEndereco
        Width = 660
        ExplicitWidth = 660
        inherited FrBairro: TFrBairros
          inherited PnTitulos: TPanel
            inherited lbNomePesquisa: TLabel
              Height = 15
            end
          end
          inherited pnPesquisa: TPanel
            ExplicitLeft = 325
          end
        end
      end
      inherited eEmail: TEditLuka
        Width = 325
        TabOrder = 11
        ExplicitWidth = 325
      end
      inherited eTelefone: TEditTelefoneLuka
        Width = 146
        ExplicitWidth = 146
      end
      inherited eCelular: TEditTelefoneLuka
        Left = 386
        Width = 127
        ExplicitLeft = 386
        ExplicitWidth = 127
      end
      inherited eFax: TEditTelefoneLuka
        Left = 517
        Width = 140
        ExplicitLeft = 517
        ExplicitWidth = 140
      end
      object eNumeroCNH: TEditLuka
        Left = 340
        Top = 187
        Width = 219
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 100
        TabOrder = 9
        OnKeyPress = Numeros
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eDataValidadeCNH: TEditLukaData
        Left = 565
        Top = 187
        Width = 91
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        TabOrder = 10
        Text = '  /  /    '
      end
    end
    inherited tsContatos: TTabSheet
      inherited FrTelefones: TFrCadastrosTelefones
        inherited sgTelefones: TGridLuka
          CorLinhaFoco = 38619
          CorColunaFoco = clActiveCaption
        end
        inherited eObservacao: TMemo
          Width = 316
          ExplicitWidth = 316
        end
      end
    end
    inherited tsEnderecos: TTabSheet
      inherited FrDiversosEnderecos: TFrDiversosEnderecos
        inherited sgOutrosEnderecos: TGridLuka
          CorLinhaFoco = 38619
          CorColunaFoco = clActiveCaption
        end
        inherited FrBairro: TFrBairros
          inherited PnTitulos: TPanel
            inherited lbNomePesquisa: TLabel
              Height = 15
            end
            inherited pnSuprimir: TPanel
              inherited ckSuprimir: TCheckBox
                Visible = False
              end
            end
          end
          inherited pnPesquisa: TPanel
            ExplicitLeft = 268
          end
        end
      end
    end
    inherited tsObservacoes: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 670
      ExplicitHeight = 290
    end
  end
  inherited eDataCadastro: TEditLukaData
    Left = 693
    ExplicitLeft = 693
  end
end
