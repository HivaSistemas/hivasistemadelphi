unit AlterarSenhaUsuario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Biblioteca,
  Vcl.ExtCtrls, Vcl.StdCtrls, EditLuka, _RecordsEspeciais, _Funcionarios;

type
  TFormAlterarSenhaUsuario = class(TFormHerancaFinalizar)
    Label1: TLabel;
    eSenhaAtual: TEditLuka;
    Label2: TLabel;
    eNovaSenhaRepetir: TEditLuka;
    Label3: TLabel;
    eNovaSenha: TEditLuka;
    procedure FormShow(Sender: TObject);
    procedure eNovaSenhaRepetirKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    FAlterandoSenhaAltisW: Boolean;
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function AtualizarSenha(pAlterandoSenhaAltisW: Boolean): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

{ TFormBuscarNovaSenhaFuncionario }

uses
  _Sessao;

function AtualizarSenha(pAlterandoSenhaAltisW: Boolean): TRetornoTelaFinalizar;
var
  vForm: TFormAlterarSenhaUsuario;
begin
  vForm := TFormAlterarSenhaUsuario.Create(Application);

  vForm.FAlterandoSenhaAltisW := pAlterandoSenhaAltisW;
  Result.Ok(vForm.ShowModal);

  FreeAndNil(vForm);
end;

procedure TFormAlterarSenhaUsuario.eNovaSenhaRepetirKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if key = VK_RETURN then
    sbFinalizarClick(self);
end;

procedure TFormAlterarSenhaUsuario.Finalizar(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin

  if not FAlterandoSenhaAltisW then begin
    vRetBanco :=
      _Funcionarios.AtualizarSenhaFuncionario(
        Sessao.getConexaoBanco,
        Sessao.getUsuarioLogado.funcionario_id,
        Sessao.getCriptografia.Criptografar( eNovaSenha.Text ),
        False
      );
  end
  else begin
    vRetBanco :=
      _Funcionarios.AtualizarSenhaFuncionario(
        Sessao.getConexaoBanco,
        Sessao.getUsuarioLogado.funcionario_id,
        eNovaSenha.Text,
        True
      );
  end;

  Sessao.AbortarSeHouveErro(vRetBanco);
  RotinaSucesso;
  inherited;
end;

procedure TFormAlterarSenhaUsuario.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(eSenhaAtual);
end;

procedure TFormAlterarSenhaUsuario.VerificarRegistro(Sender: TObject);
begin
  if eSenhaAtual.Text = '' then begin
    Exclamar('A senha atual n�o foi informada!');
    SetarFoco(eSenhaAtual);
    Abort;
  end;

  if Length(eNovaSenha.Text) < 6 then begin
    Exclamar('A nova senha deve ter 6 caracteres!');
    SetarFoco(eNovaSenha);
    Abort;
  end;

  if eNovaSenha.Text = '' then begin
    Exclamar('A nova senha n�o foi informada!');
    SetarFoco(eNovaSenha);
    Abort;
  end;

  if eNovaSenhaRepetir.Text = '' then begin
    Exclamar('A nova senha n�o foi informada!');
    SetarFoco(eNovaSenhaRepetir);
    Abort;
  end;

  if eSenhaAtual.Text = eNovaSenha.Text then begin
    Exclamar('A nova senha n�o pode ser igual a atual!');
    SetarFoco(eNovaSenha);
    Abort;
  end;

  if eNovaSenha.Text <> eNovaSenhaRepetir.Text then begin
    Exclamar('A nova senha com a senha de verifica��o n�o s�o iguais!');
    SetarFoco(eNovaSenhaRepetir);
    Abort;
  end;

  if FAlterandoSenhaAltisW then begin
    if Sessao.getUsuarioLogado.SenhaAltisW <> eSenhaAtual.Text then begin
      Exclamar('A senha atual est� incorreta!');
      SetarFoco(eSenhaAtual);
      Abort;
    end;
  end
  else begin
    if Sessao.getCriptografia.Descriptografar( (Sessao.getUsuarioLogado.senha_sistema) ) <> eSenhaAtual.Text then begin
      Exclamar('A senha atual est� incorreta!');
      SetarFoco(eSenhaAtual);
      Abort;
    end;
  end;

  inherited;
end;

end.
