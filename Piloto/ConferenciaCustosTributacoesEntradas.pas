unit ConferenciaCustosTributacoesEntradas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Sessao, _Biblioteca,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _FormacaoCustosPrecos, _RecordsEstoques;

type
  TFormConferenciaCustosTributacoesEntradas = class(TFormHerancaFinalizar)
    sgItens: TGridLuka;
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function Conferir(pEmpresaId: Integer; pItens: TArray<RecEntradaNotaFiscalItem>): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

const
  coProdutoId        = 0;
  coNomeProduto      = 1;
  coMarca            = 2;
  coQuantidade       = 3;
  coUnidade          = 4;
  coPrecoTabela      = 5;
  coPrecoTabelaAtual = 6;
  coVariacao         = 7;

function Conferir(pEmpresaId: Integer; pItens: TArray<RecEntradaNotaFiscalItem>): TRetornoTelaFinalizar;
var
  i: Integer;
  vPosic: Integer;
  vProdutosIds: TArray<Integer>;
  vItensFormacao: TArray<RecItensFormacaoCusto>;
  vForm: TFormConferenciaCustosTributacoesEntradas;
begin
  if pItens = nil then
    Exit;

  vForm := TFormConferenciaCustosTributacoesEntradas.Create(Application);

  vProdutosIds := nil;
  for i := Low(pItens) to High(pItens) do begin
    vForm.sgItens.Cells[coProdutoId, i + 1]        := NFormat(pItens[i].produto_id);
    vForm.sgItens.Cells[coNomeProduto, i + 1]      := pItens[i].nome_produto;
    vForm.sgItens.Cells[coMarca, i + 1]            := pItens[i].nome_marca;
    vForm.sgItens.Cells[coQuantidade, i + 1]       := NFormatNEstoque(pItens[i].QuantidadeEntradaAltis);
    vForm.sgItens.Cells[coUnidade, i + 1]          := pItens[i].UnidadeEntradaId;
    vForm.sgItens.Cells[coPrecoTabela, i + 1]      := NFormatN(pItens[i].preco_unitario);
    vForm.sgItens.Cells[coPrecoTabelaAtual, i + 1] := '';
    vForm.sgItens.Cells[coVariacao, i + 1]         := '';

    _Biblioteca.AddNoVetorSemRepetir(vProdutosIds, pItens[i].produto_id);
  end;
  vForm.sgItens.SetLinhasGridPorTamanhoVetor( Length(pItens) );

  vItensFormacao := _FormacaoCustosPrecos.BuscarProdutosFormacao(Sessao.getConexaoBanco, pEmpresaId, 'and ' + FiltroInInt('PCU.PRODUTO_ID', vProdutosIds));
  for i := Low(vItensFormacao) to High(vItensFormacao) do begin
    vPosic := vForm.sgItens.Localizar([coProdutoId], [NFormat(vItensFormacao[i].ProdutoId)]);
    vForm.sgItens.Cells[coPrecoTabelaAtual, vPosic] := NFormatN(vItensFormacao[i].PrecoTabela);

    vForm.sgItens.Cells[coVariacao, vPosic] :=
      NFormatN(
        Sessao.getCalculosSistema.CalcFormacaoCusto.getPercentualVariacao( vItensFormacao[i].PrecoTabela, SFormatDouble(vForm.sgItens.Cells[coPrecoTabela, vPosic]) ),
        5
      );
  end;

  Result.Ok(vForm.ShowModal);
end;

procedure TFormConferenciaCustosTributacoesEntradas.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[
    coNomeProduto,
    coMarca,
    coUnidade
  ] then
    vAlinhamento := taLeftJustify
  else
    vAlinhamento := taRightJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormConferenciaCustosTributacoesEntradas.sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;

  if ARow = 0 then
    Exit;

  if ACol = coVariacao then begin
    AFont.Style := [fsBold];
    if SFormatCurr(sgItens.Cells[coVariacao, ARow]) > 0 then
      AFont.Color := clRed
    else
      AFont.Color := clBlue;
  end;
end;

end.
