unit CadastrarNovoEnderecoVenda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, _Biblioteca, _Sessao, _DiversosEnderecos,
  _FrameHerancaPrincipal, FrameDiversosEnderecos, Vcl.Buttons, Vcl.ExtCtrls, _RecordsCadastros,
  _RecordsEspeciais;

type
  TFormCadastrarNovoEnderecoVenda = class(TFormHerancaFinalizar)
    FrDiversosEnderecos: TFrDiversosEnderecos;
    procedure FrDiversosEnderecossgOutrosEnderecosDblClick(Sender: TObject);
    procedure FrDiversosEnderecossgOutrosEnderecosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FrDiversosEnderecoseCEPKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    FCadastroId: Integer;
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Cadastrar( pCadastroId: Integer ): TRetornoTelaFinalizar<RecDiversosEnderecos>;

implementation

{$R *.dfm}

function Cadastrar( pCadastroId: Integer ): TRetornoTelaFinalizar<RecDiversosEnderecos>;
var
  vForm: TFormCadastrarNovoEnderecoVenda;
begin
  vForm := TFormCadastrarNovoEnderecoVenda.Create(nil);
  vForm.FCadastroId := pCadastroId;

  vForm.FrDiversosEnderecos.setDiversosEnderecos( _DiversosEnderecos.BuscarDiversosEnderecos(Sessao.getConexaoBanco, 0, [pCadastroId], False) );

  if Result.Ok(vForm.ShowModal, True) then
    Result.Dados := vForm.FrDiversosEnderecos.getRecDiversosEnderecos[ vForm.FrDiversosEnderecos.sgOutrosEnderecos.Row -1 ];
end;

procedure TFormCadastrarNovoEnderecoVenda.Finalizar(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin

  vRetBanco :=
    _DiversosEnderecos.AtualizarDiversosEndereco(
      Sessao.getConexaoBanco,
      FCadastroId,
      FrDiversosEnderecos.getRecDiversosEnderecos
    );

  inherited;
end;

procedure TFormCadastrarNovoEnderecoVenda.FormShow(Sender: TObject);
begin
  inherited;
  FrDiversosEnderecos.cbTipoEndereco.SetIndicePorValor('E');
  SetarFoco(FrDiversosEnderecos.eLogradouro);
end;

procedure TFormCadastrarNovoEnderecoVenda.FrDiversosEnderecoseCEPKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  FrDiversosEnderecos.eInscricaoEstadualKeyDown(Sender, Key, Shift);

  if Key = VK_RETURN then
    FrDiversosEnderecos.sgOutrosEnderecos.Row := FrDiversosEnderecos.sgOutrosEnderecos.RowCount -1;
end;

procedure TFormCadastrarNovoEnderecoVenda.FrDiversosEnderecossgOutrosEnderecosDblClick(Sender: TObject);
begin
//  inherited;
//  FrDiversosEnderecos.sgOutrosEnderecosDblClick(Sender);

  if FrDiversosEnderecos.sgOutrosEnderecos.Cells[0, FrDiversosEnderecos.sgOutrosEnderecos.Row] <> '' then
    Finalizar(Sender);
end;

procedure TFormCadastrarNovoEnderecoVenda.FrDiversosEnderecossgOutrosEnderecosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
//  inherited;
//  FrDiversosEnderecos.sgOutrosEnderecosKeyDown(Sender, Key, Shift);
  if Key = VK_RETURN then
    FrDiversosEnderecossgOutrosEnderecosDblClick(Sender);
end;

procedure TFormCadastrarNovoEnderecoVenda.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrDiversosEnderecos.getRecDiversosEnderecos = nil then begin
    _Biblioteca.Exclamar('Nenhum endere�o foi informado!');
    SetarFoco(FrDiversosEnderecos);
    Abort;
  end;

  if FrDiversosEnderecos.getRecDiversosEnderecos[ FrDiversosEnderecos.sgOutrosEnderecos.Row -1 ].tipo_endereco = 'C' then begin
    _Biblioteca.Exclamar('O endere�o n�o poder� ser do tipo cobran�a!');
    SetarFoco(FrDiversosEnderecos);
    Abort;
  end;
end;

end.
