inherited FormLancamentoManualCaixaBanco: TFormLancamentoManualCaixaBanco
  Caption = 'Lan'#231'amento manual em contas'
  ClientHeight = 234
  ClientWidth = 659
  OnShow = FormShow
  ExplicitWidth = 665
  ExplicitHeight = 263
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label1: TLabel
    Top = 21
    ExplicitTop = 21
  end
  object lbl7: TLabel [1]
    Left = 212
    Top = 21
    Width = 24
    Height = 14
    Caption = 'Tipo'
  end
  object lb17: TLabel [2]
    Left = 315
    Top = 21
    Width = 109
    Height = 14
    Caption = 'Valor do movimento'
  end
  object lb1: TLabel [3]
    Left = 450
    Top = 21
    Width = 120
    Height = 14
    Caption = 'Data/hora movimento'
  end
  inherited pnOpcoes: TPanel
    Height = 234
    TabOrder = 8
    ExplicitHeight = 234
    inherited sbDesfazer: TSpeedButton
      Top = 49
      ExplicitTop = 49
    end
    inherited sbExcluir: TSpeedButton
      Top = 110
      Height = 40
      ExplicitTop = 110
      ExplicitHeight = 40
    end
    inherited sbPesquisar: TSpeedButton
      Left = -120
      Visible = False
      ExplicitLeft = -120
    end
    inherited sbLogs: TSpeedButton
      Left = -119
      ExplicitLeft = -119
    end
  end
  inherited eID: TEditLuka
    Top = 35
    TabOrder = 0
    ExplicitTop = 35
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = -178
    Top = 358
    TabOrder = 9
    Visible = False
    ExplicitLeft = -178
    ExplicitTop = 358
  end
  inline FrConta: TFrContas
    Left = 126
    Top = 63
    Width = 259
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 4
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 63
    ExplicitWidth = 259
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 234
      Height = 24
      ExplicitWidth = 234
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 259
      ExplicitWidth = 259
      inherited lbNomePesquisa: TLabel
        Width = 31
        Caption = 'Conta'
        ExplicitWidth = 31
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 154
        ExplicitLeft = 154
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 234
      Height = 25
      ExplicitLeft = 234
      ExplicitHeight = 25
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
    inherited ckAutorizados: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  object cbTipoMovimento: TComboBoxLuka
    Left = 210
    Top = 35
    Width = 101
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = 16645629
    TabOrder = 1
    OnKeyDown = ProximoCampo
    Items.Strings = (
      'Entrada'
      'Sa'#237'da'
      'Transfer'#234'ncia')
    Valores.Strings = (
      'ENM'
      'SAM'
      'TRA')
    AsInt = 0
  end
  object eValorMovimento: TEditLuka
    Left = 315
    Top = 35
    Width = 129
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  inline FrContaDestino: TFrContas
    Left = 393
    Top = 63
    Width = 256
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 5
    TabStop = True
    ExplicitLeft = 393
    ExplicitTop = 63
    ExplicitWidth = 256
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 231
      Height = 24
      ExplicitWidth = 231
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 256
      ExplicitWidth = 256
      inherited lbNomePesquisa: TLabel
        Width = 92
        Caption = 'Conta de destino'
        ExplicitWidth = 92
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 151
        ExplicitLeft = 151
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 231
      Height = 25
      ExplicitLeft = 231
      ExplicitHeight = 25
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
    inherited ckAutorizados: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  inline FrPlanoFinanceiro: TFrPlanosFinanceiros
    Left = 393
    Top = 111
    Width = 256
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 7
    TabStop = True
    ExplicitLeft = 393
    ExplicitTop = 111
    ExplicitWidth = 256
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 231
      Height = 24
      ExplicitWidth = 231
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 256
      ExplicitWidth = 256
      inherited lbNomePesquisa: TLabel
        Width = 88
        Caption = 'Plano financeiro'
        ExplicitWidth = 88
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 151
        ExplicitLeft = 151
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 231
      Height = 25
      ExplicitLeft = 231
      ExplicitHeight = 25
    end
  end
  inline FrCentroCusto: TFrCentroCustos
    Left = 126
    Top = 111
    Width = 259
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 6
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 111
    ExplicitWidth = 259
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 234
      Height = 24
      ExplicitWidth = 234
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 259
      ExplicitWidth = 259
      inherited lbNomePesquisa: TLabel
        Width = 90
        ExplicitWidth = 90
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 154
        ExplicitLeft = 154
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 234
      Height = 25
      ExplicitLeft = 234
      ExplicitHeight = 25
    end
  end
  object eDataHoraMovimento: TEditLukaData
    Left = 450
    Top = 35
    Width = 135
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999 99:99:99;1;_'
    MaxLength = 19
    TabOrder = 3
    Text = '  /  /       :  :  '
    OnKeyDown = ProximoCampo
    TipoData = tdDataHora
  end
end
