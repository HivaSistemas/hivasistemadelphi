inherited FormCadastroGerenciadoresCartoesTEF: TFormCadastroGerenciadoresCartoesTEF
  Caption = 'Configura'#231#245'es TEF'
  ClientHeight = 251
  ClientWidth = 701
  ExplicitWidth = 707
  ExplicitHeight = 280
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 212
    Top = 5
    Width = 32
    Height = 14
    Caption = 'Nome'
  end
  inherited pnOpcoes: TPanel
    Height = 251
    ExplicitHeight = 251
    inherited sbGravar: TSpeedButton
      Top = 5
      ExplicitTop = 5
    end
    inherited sbDesfazer: TSpeedButton
      Top = 58
      ExplicitTop = 58
    end
    inherited sbExcluir: TSpeedButton
      Top = 114
      ExplicitTop = 114
    end
    inherited sbPesquisar: TSpeedButton
      Top = 166
      ExplicitTop = 166
    end
  end
  inherited eID: TEditLuka
    TabOrder = 4
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 651
    TabOrder = 5
    ExplicitLeft = 651
  end
  inline FrDiretorioRequisicoes: TFrDiretorio
    Left = 122
    Top = 47
    Width = 288
    Height = 45
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 122
    ExplicitTop = 47
    ExplicitWidth = 288
    inherited lb1: TLabel
      Width = 187
      Height = 14
      Caption = 'Caminho pasta de requisi'#231#245'es TEF'
      ExplicitWidth = 187
      ExplicitHeight = 14
    end
    inherited sbPesquisarDiretorio: TSpeedButton
      Left = 263
      Top = 15
      ExplicitLeft = 324
      ExplicitTop = 15
    end
    inherited eCaminhoDiretorio: TEditLuka
      Top = 15
      Width = 254
      Height = 22
      ExplicitTop = 15
      ExplicitWidth = 254
      ExplicitHeight = 22
    end
  end
  object eNome: TEditLuka
    Left = 212
    Top = 19
    Width = 433
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 1
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  inline FrDiretorioRespostas: TFrDiretorio
    Left = 413
    Top = 47
    Width = 284
    Height = 45
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 413
    ExplicitTop = 47
    ExplicitWidth = 284
    inherited lb1: TLabel
      Width = 183
      Height = 14
      Caption = 'Caminho pasta das respostas TEF'
      ExplicitWidth = 183
      ExplicitHeight = 14
    end
    inherited sbPesquisarDiretorio: TSpeedButton
      Left = 259
      Top = 15
      ExplicitLeft = 324
      ExplicitTop = 15
    end
    inherited eCaminhoDiretorio: TEditLuka
      Top = 15
      Width = 250
      Height = 22
      ExplicitTop = 15
      ExplicitWidth = 250
      ExplicitHeight = 22
    end
  end
  inline FrConfiguracoesCartoesTEF: TFrConfiguracoesCartoesTEF
    Left = 126
    Top = 106
    Width = 571
    Height = 142
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 6
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 106
    ExplicitWidth = 571
    ExplicitHeight = 142
    inherited sgConfig: TGridLuka
      Width = 571
      Height = 142
      ExplicitWidth = 571
      ExplicitHeight = 142
      ColWidths = (
        111
        107
        90
        107
        105
        88
        110)
    end
  end
  object st1: TStaticTextLuka
    Left = 125
    Top = 90
    Width = 571
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Conf. para busca da bandeira do cart'#227'o'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 7
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
end
