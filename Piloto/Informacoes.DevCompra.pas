unit Informacoes.DevCompra;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.StdCtrls, StaticTextLuka;

type
  TFormInformacoesDevCompra = class(TFormHerancaFinalizar)
    st1: TStaticTextLuka;
    stValorProdutos: TStaticTextLuka;
    st3: TStaticTextLuka;
    stOutrasDespesas: TStaticTextLuka;
    stDesconto: TStaticTextLuka;
    st4: TStaticTextLuka;
    st2: TStaticTextLuka;
    stBaseICMS: TStaticTextLuka;
    st9: TStaticTextLuka;
    stValorICMS: TStaticTextLuka;
    st12: TStaticTextLuka;
    stBaseIcmsSt: TStaticTextLuka;
    st6: TStaticTextLuka;
    stValorIcmsSt: TStaticTextLuka;
    st7: TStaticTextLuka;
    stIPI: TStaticTextLuka;
    st5: TStaticTextLuka;
    stTotalDevolucao: TStaticTextLuka;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormInformacoesDevCompra: TFormInformacoesDevCompra;

implementation

{$R *.dfm}

end.
