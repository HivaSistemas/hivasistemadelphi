unit _RecordsEstoques;

interface

type
  RecVidaProduto = record
  public
    produto_id: Integer;
    empresa_id: Integer;
    movimento_id: Integer;
    quantidade: Double;
    data_hora_movimento: TDateTime;
    tipo_movimento: string;
    tipo_movimento_analitico: string;
    natureza: string;
    NumeroDocumento: string;
    CadastroId: Integer;
    NomeCadastro: string;
  end;

  RecEstoque = record
    produto_id: Integer;
    empresa_id: Integer;
    Estoque: Double;
    Fisico: Double;
    Reservado: Double;
    Bloqueado: Double;
    Disponivel: Double;
    ComprasPendentes: Double;
    NomeEmpresa: string;
  end;

  RecEstoqueBkp2017 = record
    produto_id: Integer;
    empresa_id: Integer;
    fisico: Double;
    reservado: Double;
  end;

  RecAjusteEstoque = record
    ajuste_estoque_id: Integer;
    EmpresaId: Integer;
    NomeFantasia: string;
    motivo_ajuste_id: Integer;
    BaixaCompraId: Integer;
    PlanoFinanceiroId: string;
    MotivoAjusteEstoque: string;
    usuario_ajuste_id: Integer;
    NomeUsuarioAjuste: string;
    data_hora_ajuste: TDateTime;
    ValorTotal: Double;
    observacao: string;
    Tipo: string;
    TipoAnalitico: string;
    TipoEstoqueAjustado: string;
  end;

  RecProducaoEstoque = record
    producao_estoque_id: Integer;
    EmpresaId: Integer;
    NomeFantasia: string;
    usuario_producao_id: Integer;
    NomeUsuarioProducao: string;
    data_hora_producao: TDateTime;
    ValorTotal: Double;
    QuantidadeTotal: Double;
  end;

  RecMapaProducao = record
    MapaProducaoId: Integer;
    ProdutoId: Integer;
    NomeProduto: string;
    LocalId: Integer;
    Quantidade: Double;
    TipoMovimento: string;
  end;

  RecAjusteEstoqueItens = record
    ajuste_estoque_id: Integer;
    LocalId: Integer;
    item_id: Integer;
    produto_id: Integer;
    NomeProduto: string;
    MarcaId: Integer;
    NomeMarca: string;
    quantidade: Double;
    quantidadeInformado: Double;
    Unidade: string;
    Natureza: string;
    NaturezaAnalitico: string;
    Lote: string;
    NomeLocal: string;
    PrecoUnitario: Double;
    ValorTotal: Double;
    DataFabricacao: TDateTime;
    DataVencimento: TDateTime;
  end;

  RecProducaoEstoqueItens = record
    Producao_estoque_id: Integer;
    LocalId: Integer;
    Produto_id: Integer;
    NomeProduto: string;
    Quantidade: Double;
    Natureza: string;
    NaturezaAnalitico: string;
    NomeLocal: string;
    PrecoUnitario: Double;
    MarcaId: Integer;
    NomeMarca: string;
  end;

  RecTiposEntradas = record
    ordem: Integer;
    tipo: string;
    descricao: string;
  end;

  RecEntradaNotaFiscal = record
    entrada_id: Integer;
    empresa_id: Integer;
    fornecedor_id: Integer;
    Status: string;
    OrigemEntrada: string;
    NotaTransfProdOrigemId: Integer;
    StatusAnalitico: string;
    Razao_Social: string;
    Nome_Fantasia: string;
    CfopId: string;
    PlanoFinanceiroId: string;
    CentroCustoId: Integer;
    ConhecimentoFreteId: Integer;
    base_calculo_icms: Double;
    numero_nota: Integer;
    peso_bruto: Double;
    peso_liquido: Double;
    base_calculo_cofins: Double;
    valor_cofins: Double;
    valor_ipi: Double;
    ValorConhecimentoFrete: Double;
    base_calculo_pis: Double;
    valor_pis: Double;
    base_calculo_icms_st: Double;
    valor_icms_st: Double;
    valor_icms: Double;
    valor_total: Double;
    valor_total_produtos: Double;
    valor_desconto: Double;
    valor_outras_despesas: Double;
    ValorOutrosCustosFinanc: Currency;
    valor_frete: Double;
    TipoRateioFrete: string;
    TipoRateioFreteAnalitico: string;
    ModalidadeFrete: Integer;
    serie_nota: string;
    modelo_nota: string;
    data_hora_cadastro: TDateTime;
    data_hora_emissao: TDateTime;
    DataPrimeiraParcela: TDateTime;
    DataEntrada: TDateTime;
    chave_nfe: string;
    atualizar_custo_compra: string;
    atualizar_custo_entrada: string;
    cnpj_emitente: string;
    inscricao_estadual_emitente: string;
    cnpj_destinatario: string;
    UsuarioCadastroId: Integer;
    NomeUsuarioCadastro: string;
    NomeEmpresa: string;

    EntradaViaXML: Boolean;
    XmlTexto: string;
    CaminhoXml: string;
  end;

  RecEntradaNotaFiscalItem = record
    entrada_id: Integer;
    produto_id: Integer;
    item_id: Integer;
    CfopId: string;
    nome_produto: string;
    nome_marca: string;
    unidade: string;
    origem_produto: Integer;
    codigo_ncm: string;
    valor_total_outras_despesas: Double;
    valor_total_desconto: Double;
    quantidade: Double;
    QuantidadeDevolvidos: Currency;
    UnidadeEntradaId: string;
    UnidadeCompraId: string;
    QuantidadeEmbalagem: Double;
    MultiploCompra: Double;
    MultiploVenda: Double;
    preco_unitario: Double;
    QuantidadeEntradaAltis: Double;
    valor_total: Double;
    valor_icms: Double;
    percentual_ipi: Double;
    valor_ipi: Double;
    valor_cofins: Double;
    percentual_cofins: Double;
    base_calculo_icms: Double;
    percentual_icms: Double;
    base_calculo_icms_st: Double;
    percentual_icms_st: Double;
    valor_icms_st: Double;
    base_calculo_pis: Double;
    percentual_pis: Double;
    valor_pis: Double;
    base_calculo_cofins: Double;
    indice_reducao_base_icms_st: Double;
    indice_reducao_base_icms: Double;
    informacoes_adicionais: string;
    cst_pis: string;
    cst_cofins: string;
    cst: string;
    codigo_barras: string;
    iva: Double;
    Peso: Double;

    ValorFreteCusto: Double;
    ValorIPICusto: Double;
    ValorICMSStCusto: Double;
    ValorOutrosCusto: Double;
    PrecoFinal: Double;

    ValorDifalCusto: Double;
    ValorICMSCusto: Double;
    ValorICMSFreteCusto: Double;
    ValorPisCusto: Double;
    ValorCofinsCusto: Double;
    ValorPisFreteCusto: Double;
    ValorCofinsFreteCusto: Double;
    PrecoLiquido: Double;

    preco_pauta: Double;
    LocalEntradaId: Integer;
    NomeLocal: string;

    TipoControleEstoque: string;
    ExigirDataFabricacaoLote: string;
    ExigirDataVencimentoLote: string;
    EntradaMercadoriasBonific: string;
  end;

  RecEntradaNotaFiscalFinanceiro = record
    entrada_id: Integer;
    item_id: Integer;
    cobranca_id: Integer;
    parcela: Integer;
    data_vencimento: TDateTime;
    valor_documento: Double;
    nome_tipo_cobranca: string;
    nosso_numero: string;
    codigo_barras: string;
  end;

  RecProdutosAnoMesFinalizado = record
    produtoId: Integer;
    nomeProduto: string;
    nomeMarca: string;
    estoque: Double;
    cmv: Double;
    custoUltimoPedido: Double;
  end;

implementation

end.
