unit Frame.TipoAcompanhamentoOrcamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Menus,
  Vcl.Buttons, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _RecordsCadastros,
  _Sessao, _Biblioteca, _TipoAcompanhamentoOrcamento, Pesquisa.TipoAcompanhamentoOrcamento, Math;

type
  TFrTipoAcompanhamentoOrcamento = class(TFrameHenrancaPesquisas)
  public
    function GetTipoAcompanhamentoOrcamento(pLinha: Integer = -1): RecTipoAcompanhamentoOrcamento;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrameTipoAcompanhamentoOrcamento }

function TFrTipoAcompanhamentoOrcamento.AdicionarDireto: TObject;
var
  vMotivos: TArray<RecTipoAcompanhamentoOrcamento>;
begin
  vMotivos := _TipoAcompanhamentoOrcamento.BuscarTipoAcompanhamentoOrcamento(Sessao.getConexaoBanco, 0, [FChaveDigitada]);
  if vMotivos = nil then
    Result := nil
  else
    Result := vMotivos[0];
end;

function TFrTipoAcompanhamentoOrcamento.AdicionarPesquisando: TObject;
begin
  Result := Pesquisa.TipoAcompanhamentoOrcamento.Pesquisar();
end;

function TFrTipoAcompanhamentoOrcamento.GetTipoAcompanhamentoOrcamento(pLinha: Integer): RecTipoAcompanhamentoOrcamento;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecTipoAcompanhamentoOrcamento(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrTipoAcompanhamentoOrcamento.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecTipoAcompanhamentoOrcamento(FDados[i]).tipo_acompanhamento_id = RecTipoAcompanhamentoOrcamento(pSender).tipo_acompanhamento_id then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrTipoAcompanhamentoOrcamento.MontarGrid;
var
  i: Integer;
  vSender: RecTipoAcompanhamentoOrcamento;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecTipoAcompanhamentoOrcamento(FDados[i]);
      AAdd([IntToStr(vSender.tipo_acompanhamento_id), vSender.descricao]);
    end;
  end;
end;

end.
