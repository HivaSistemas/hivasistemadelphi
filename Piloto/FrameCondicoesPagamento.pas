unit FrameCondicoesPagamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _RecordsCadastros,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Sessao, _CondicoesPagamento, PesquisaCondicoesPagamento,
  System.Math, Vcl.Buttons, _Biblioteca, System.StrUtils, Vcl.Menus;

type
  TFrCondicoesPagamento = class(TFrameHenrancaPesquisas)
  public
    FFechamentoAcumulado: Boolean;
    FSomenteEmpresaLogada: Boolean;

    constructor Create(AOwner: TComponent); override;
    function getDados(pLinha: Integer = -1): RecCondicoesPagamento;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarDiretoTodos: TArray<TObject>; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  published
    property FechamentoAcumulado: Boolean read FFechamentoAcumulado write FFechamentoAcumulado default False;
    property SomenteEmpresaLogada: Boolean read FSomenteEmpresaLogada write FSomenteEmpresaLogada default False;
  end;

implementation

{$R *.dfm}

{ TFrCondicoesPagamento }

function TFrCondicoesPagamento.AdicionarDireto: TObject;
var
  vCondicoes: TArray<RecCondicoesPagamento>;
begin
  vCondicoes := _CondicoesPagamento.BuscarCondicoesPagamentos( Sessao.getConexaoBanco, 0, [FChaveDigitada], ckSomenteAtivos.Checked, FFechamentoAcumulado, FSomenteEmpresaLogada, Sessao.getEmpresaLogada.EmpresaId);
  if vCondicoes = nil then
    Result := nil
  else
    Result := vCondicoes[0];
end;

function TFrCondicoesPagamento.AdicionarDiretoTodos: TArray<TObject>;
begin
  Result := TArray<TObject>(_CondicoesPagamento.BuscarCondicoesPagamentosComando(Sessao.getConexaoBanco, 'where 1 = 1 ' + FChaveDigitada));
end;

function TFrCondicoesPagamento.AdicionarPesquisando: TObject;
begin
  Result := PesquisaCondicoesPagamento.PesquisarCondicaoPagamento(ckSomenteAtivos.Checked, FFechamentoAcumulado, FSomenteEmpresaLogada);
end;

constructor TFrCondicoesPagamento.Create(AOwner: TComponent);
begin
  inherited;
  FColunaPesquisa := 'CONDICAO_ID';
end;

function TFrCondicoesPagamento.getDados(pLinha: Integer): RecCondicoesPagamento;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecCondicoesPagamento(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrCondicoesPagamento.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecCondicoesPagamento(FDados[i]).condicao_id = RecCondicoesPagamento(pSender).condicao_id then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrCondicoesPagamento.MontarGrid;
var
  i: Integer;
  pSender: RecCondicoesPagamento;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      pSender := RecCondicoesPagamento(FDados[i]);
      AAdd([IntToStr(pSender.condicao_id), pSender.nome]);
    end;
  end;
end;

end.
