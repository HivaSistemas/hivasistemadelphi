inherited FormBaixarControleEntrega: TFormBaixarControleEntrega
  Caption = 'Baixa de controle de entrega'
  ClientHeight = 448
  ClientWidth = 903
  ExplicitWidth = 909
  ExplicitHeight = 477
  PixelsPerInch = 96
  TextHeight = 14
  object lb12: TLabel [0]
    Left = 84
    Top = 3
    Width = 47
    Height = 14
    Caption = 'Empresa'
  end
  object lb13: TLabel [1]
    Left = 404
    Top = 3
    Width = 39
    Height = 14
    Caption = 'Ve'#237'culo'
  end
  object Label1: TLabel [2]
    Left = 588
    Top = 3
    Width = 53
    Height = 14
    Caption = 'Motorista'
  end
  object Label2: TLabel [3]
    Left = 4
    Top = 3
    Width = 46
    Height = 14
    Caption = 'Controle'
  end
  inherited pnOpcoes: TPanel
    Top = 411
    Width = 903
    ExplicitTop = 411
    ExplicitWidth = 796
  end
  object sgEntregas: TGridLuka
    Left = 4
    Top = 56
    Width = 893
    Height = 122
    Align = alCustom
    ColCount = 9
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
    TabOrder = 1
    OnClick = sgEntregasClick
    OnDblClick = sgEntregasDblClick
    OnDrawCell = sgEntregasDrawCell
    OnKeyDown = sgEntregasKeyDown
    OnKeyPress = sgEntregasKeyPress
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Conf.?'
      'Entrega'
      'Status'
      'Cliente'
      'Empresa'
      'Local'
      'Peso total'
      'Nome pessoa recebeu'
      'CPF pessoa recebeu')
    OnGetCellColor = sgEntregasGetCellColor
    OnGetCellPicture = sgEntregasGetCellPicture
    Grid3D = False
    RealColCount = 15
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      40
      50
      85
      139
      127
      92
      65
      166
      118)
  end
  object StaticTextLuka2: TStaticTextLuka
    Left = 4
    Top = 178
    Width = 893
    Height = 17
    Align = alCustom
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Itens da entrega selecionada'
    Color = clSilver
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object sgItens: TGridLuka
    Left = 4
    Top = 194
    Width = 893
    Height = 216
    Align = alCustom
    ColCount = 6
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 2
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 3
    OnDrawCell = sgItensDrawCell
    OnSelectCell = sgItensSelectCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Produto'
      'Nome'
      'Marca'
      'Lote'
      'Quantidade'
      'Qtde.retorn.')
    OnGetCellColor = sgItensGetCellColor
    OnArrumarGrid = sgItensArrumarGrid
    Grid3D = False
    RealColCount = 10
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      54
      269
      154
      89
      77
      75)
  end
  object StaticTextLuka1: TStaticTextLuka
    Left = 4
    Top = 40
    Width = 893
    Height = 17
    Align = alCustom
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Entregas'
    Color = clSilver
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 4
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object eEmpresa: TEditLuka
    Left = 84
    Top = 17
    Width = 314
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 5
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eVeiculo: TEditLuka
    Left = 404
    Top = 17
    Width = 178
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 6
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eMotorista: TEditLuka
    Left = 588
    Top = 17
    Width = 308
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 7
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eControleEntregaId: TEditLuka
    Left = 4
    Top = 17
    Width = 73
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 8
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
