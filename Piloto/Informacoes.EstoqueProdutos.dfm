inherited FormInformacoesEstoqueProduto: TFormInformacoesEstoqueProduto
  Caption = 'Informa'#231#245'es de estoque de produto'
  ClientHeight = 332
  ClientWidth = 659
  ExplicitWidth = 665
  ExplicitHeight = 361
  PixelsPerInch = 96
  TextHeight = 14
  object spSeparador: TSplitter [0]
    Left = 0
    Top = 105
    Width = 659
    Height = 6
    Cursor = crVSplit
    Align = alTop
    ResizeStyle = rsUpdate
    ExplicitLeft = -225
    ExplicitTop = 129
    ExplicitWidth = 884
  end
  inherited pnOpcoes: TPanel
    Top = 295
    Width = 659
    ExplicitTop = 295
    ExplicitWidth = 659
  end
  object StaticTextLuka2: TStaticTextLuka
    Left = 0
    Top = 0
    Width = 659
    Height = 17
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Empresas'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object sgEmpresas: TGridLuka
    Left = 0
    Top = 17
    Width = 659
    Height = 88
    Align = alTop
    ColCount = 8
    Ctl3D = True
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    GradientEndColor = 15395562
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goRowSelect, goThumbTracking]
    ParentCtl3D = False
    TabOrder = 2
    OnClick = sgEmpresasClick
    OnDrawCell = sgEmpresasDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Empr.'
      'Nome'
      'F'#237'sico'
      'Reservado'
      'Bloq.'
      'Dispon'#237'vel'
      'Compras'
      'Entradas')
    Grid3D = False
    RealColCount = 54
    AtivarPopUpSelecao = False
    ColWidths = (
      42
      199
      59
      71
      60
      64
      67
      64)
  end
  object StaticTextLuka1: TStaticTextLuka
    Left = 0
    Top = 111
    Width = 659
    Height = 17
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Lotes'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 3
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object sgLotes: TGridLuka
    Left = 0
    Top = 128
    Width = 659
    Height = 167
    Align = alClient
    ColCount = 8
    Ctl3D = True
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    GradientEndColor = 15395562
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goRowSelect, goThumbTracking]
    ParentCtl3D = False
    TabOrder = 4
    OnDrawCell = sgLotesDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Local'
      'Lote'
      'Dt. fabrica'#231#227'o'
      'Dt. vencimento'
      'F'#237'sico'
      'Reservado'
      'Bloq.'
      'Dispon'#237'vel')
    Grid3D = False
    RealColCount = 54
    AtivarPopUpSelecao = False
    ColWidths = (
      104
      72
      83
      89
      63
      75
      59
      81)
  end
end
