unit MenuPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Vcl.StdCtrls, TFlatHintUnit, Vcl.Imaging.pngimage,
  Vcl.ExtCtrls, Vcl.ComCtrls, _Sessao, System.DateUtils, Vcl.Imaging.jpeg, Vcl.Buttons, _Notificacoes,
  _Mensagens, Relacao.CurvaABC, _RecordsEspeciais, EntradaNotasFiscaisServicos, Login, Winapi.ShellAPI,
  Vcl.ActnMan, Vcl.ActnColorMaps, System.ImageList, Vcl.ImgList, System.Actions, System.StrUtils,
  Vcl.ActnList, Vcl.XPStyleActnCtrls, Vcl.ToolWin, Vcl.ActnCtrls, Vcl.ActnMenus, DevolucaoEntradaNotaFiscal,
  Vcl.PlatformDefaultStyleActnCtrls, Relacao.MarketPlace, Cadastrar.EnderecoEstoque,
  FireDAC.Stan.StorageBin,  System.Generics.Collections, Vcl.Clipbrd, Relacao.Dashboard, RelacaoProducoesEstoque;

type
  TObjMenu = class
    Caminho: String;
    Menu: TMenuItem;
  end;

  TFormMenuPrincipal = class(TForm)
    Hints: TFlatHint;
    MenuPrincipal: TMainMenu;
    miCadastros: TMenuItem;
    miClientes: TMenuItem;
    miEmpresas: TMenuItem;
    miEnderecos: TMenuItem;
    miBairros: TMenuItem;
    miCidades: TMenuItem;
    miEstados: TMenuItem;
    miFinanceiro: TMenuItem;
    miContas: TMenuItem;
    miTiposCobranca: TMenuItem;
    miCondicoesPagamento: TMenuItem;
    miCadastrosFuncionarios: TMenuItem;
    miAutorizacoes: TMenuItem;
    miFuncoes: TMenuItem;
    miFuncionarios: TMenuItem;
    miProduto: TMenuItem;
    miCFOP: TMenuItem;
    miMarcas: TMenuItem;
    miProdutos: TMenuItem;
    miLocaisProdutos: TMenuItem;
    miUnidades: TMenuItem;
    miEstoque: TMenuItem;
    miEntradas: TMenuItem;
    miVendas1: TMenuItem;
    miOrcamentosVendas: TMenuItem;
    miCaixa: TMenuItem;
    miPedidos1: TMenuItem;
    miReceberPedidos: TMenuItem;
    miCancelarRecebimentoPedidos: TMenuItem;
    miFinanceiro2: TMenuItem;
    miContasReceber: TMenuItem;
    miContasPagar: TMenuItem;
    miDesenvolvimento: TMenuItem;
    miCadastrarNovoDesenvolvimento: TMenuItem;
    miAcompanhamentoDesenvolvimento: TMenuItem;
    miAjusteEstoque: TMenuItem;
    miVidaProduto: TMenuItem;
    miProdutosRelacionados: TMenuItem;
    miRotinas1: TMenuItem;
    miAbertura: TMenuItem;
    miSuprimento: TMenuItem;
    miSangria: TMenuItem;
    miFechamento: TMenuItem;
    miRelatrios1: TMenuItem;
    miMovimentacoesContas: TMenuItem;
    miParametrosSistema: TMenuItem;
    miParametros: TMenuItem;
    miParametrosEmpresa: TMenuItem;
    miFornecedores: TMenuItem;
    miSEFAZ1: TMenuItem;
    miConsultaNotasFiscais: TMenuItem;
    miGerenciadoresCartoesTEF: TMenuItem;
    miMotivosAjusteEstoque: TMenuItem;
    miDevolucaoProdutos: TMenuItem;
    miInclusaoTitulosReceber: TMenuItem;
    miRelatoriosFinanceiros: TMenuItem;
    miRelacaoContasReceber: TMenuItem;
    miRelaodecontasapagar1: TMenuItem;
    miRelacaoContasReceberBaixados: TMenuItem;
    miInclusaoTitulosPagar: TMenuItem;
    miN1: TMenuItem;
    miN2: TMenuItem;
    miN3: TMenuItem;
    miRelacaoContasPagarBaixados: TMenuItem;
    miRelatrios2: TMenuItem;
    miRelacaoOrcamentosBloqueios: TMenuItem;
    miAlterarSenha: TMenuItem;
    miComissoes: TMenuItem;
    miRelacaoComissoesFuncionario: TMenuItem;
    miRelacaoComissoesProduto: TMenuItem;
    miRelacaoComissesTipoCobranca: TMenuItem;
    miRelacaoOrcamentosVendas: TMenuItem;
    miRelacaoDevolucoes: TMenuItem;
    miCurvaABC: TMenuItem;
    miFiscal: TMenuItem;
    miOutrasnotas1: TMenuItem;
    miRelatoriosFiscais: TMenuItem;
    miExpedicao: TMenuItem;
    miConfirmarRetiradasAto: TMenuItem;
    miEntregas: TMenuItem;
    miGerarRetirada: TMenuItem;
    miRetiradasProdutos: TMenuItem;
    miGerarEntregas: TMenuItem;
    miMensagens: TMenuItem;
    tiMensagens: TTimer;
    lbNovasMensagens: TLabel;
    miRelacaoPreEntradas: TMenuItem;
    miEntradasdeNF: TMenuItem;
    miDepartamentosSecoesLinhas: TMenuItem;
    miSPEDICMS: TMenuItem;
    miSPEDPISCOFINS: TMenuItem;
    miPDV: TMenuItem;
    miRelatoriosEstoque: TMenuItem;
    miRelacaoTurnosCaixa: TMenuItem;
    GruposEstoque: TMenuItem;
    Produtopaifilho1: TMenuItem;
    miAgendarItensSemPrevisao: TMenuItem;
    Relatrios1: TMenuItem;
    miRelacaoCadastros: TMenuItem;
    miRelacaoProdutos: TMenuItem;
    Relatrios2: TMenuItem;
    miRelacaoEntregasPendentes: TMenuItem;
    miRelacaoEntregasRealizadas: TMenuItem;
    miControleEntregas: TMenuItem;
    miVeiculos: TMenuItem;
    AnaliseProdutosControlamlotes: TMenuItem;
    miFormacaoCustoPreco: TMenuItem;
    Percentuaisdecustosdaempresa1: TMenuItem;
    DefiniodeCFOPdeoperaes1: TMenuItem;
    miPromocoes: TMenuItem;
    ConsultaProdutos: TMenuItem;
    miControleGerencial: TMenuItem;
    Preos1: TMenuItem;
    miProdutosPaiFilho: TMenuItem;
    miRelacaoEstoque: TMenuItem;
    miTransferenciaProdutosEmpresas: TMenuItem;
    Compras1: TMenuItem;
    miCompras: TMenuItem;
    miSugestaoCompras: TMenuItem;
    miRelacaoCompras: TMenuItem;
    miRelacaoEntradas: TMenuItem;
    miConfirmarDevolucao: TMenuItem;
    miPortadores: TMenuItem;
    N1: TMenuItem;
    miRotas: TMenuItem;
    miCentroCustos: TMenuItem;
    miTransferenciaLocais: TMenuItem;
    miRelacaoTransferenciaLocais: TMenuItem;
    Fretes1: TMenuItem;
    miMotoristas: TMenuItem;
    miRelacaoControlesEntregas: TMenuItem;
    miRelacaoPlanosfinanceiros: TMenuItem;
    miConhecimentosFrete: TMenuItem;
    miTransportadoras: TMenuItem;
    miSeriesNotasFiscais: TMenuItem;
    miCreditosReceber: TMenuItem;
    miCreditosPagar: TMenuItem;
    tiPiscaNovasMensagens: TTimer;
    miLocais: TMenuItem;
    miOrdenaodelocaisparaentregaporproduto1: TMenuItem;
    miImpressoras: TMenuItem;
    miImpressorasEstacao: TMenuItem;
    miImpressorasUsuarios: TMenuItem;
    miLancamentoManualCaixaBanco: TMenuItem;
    miFornecedores1: TMenuItem;
    miGruposFornecedores: TMenuItem;
    miRelacaoAjustesEstoque: TMenuItem;
    miEntradas1: TMenuItem;
    miRelacaoConhecimentosFrete: TMenuItem;
    miAjuda: TMenuItem;
    miIPdoServidor: TMenuItem;
    SobreoAltis1: TMenuItem;
    miImportaes1: TMenuItem;
    miImpostosIBPT: TMenuItem;
    miSuporteRemoto: TMenuItem;
    miEstacao: TMenuItem;
    miCondicoesPagamentoCartoesPDV: TMenuItem;
    miGerarArquivosRemessaBoletos: TMenuItem;
    miBoletos: TMenuItem;
    miRetornoRemessaBoletos: TMenuItem;
    imTrocarUsuario: TImage;
    imMensagens: TImage;
    imConsultaProdutos: TImage;
    im4: TImage;
    im5: TImage;
    miFecharacumulado1: TMenuItem;
    miFecharAcumulado: TMenuItem;
    miRelacaoAcumulados: TMenuItem;
    miCancelarRecebimentoAcumulados: TMenuItem;
    miGruposdetributaes1: TMenuItem;
    miGruposTributacoesCompraEstadual: TMenuItem;
    miGruposTributacoesCompraFederal: TMenuItem;
    miCompra1: TMenuItem;
    miVenda1: TMenuItem;
    miGruposTributacoesVendaEstadual: TMenuItem;
    miGruposTributacoesVendaFederal: TMenuItem;
    miRelacaoVendasProdutos: TMenuItem;
    miIndicesdedescontosdevenda1: TMenuItem;
    miProfissionais1: TMenuItem;
    miProfissionais: TMenuItem;
    miCargosProfissionais: TMenuItem;
    miMovimentosPendentesEmissaoNotaFiscalTransferencia: TMenuItem;
    miBloqueioEstoque: TMenuItem;
    miRelacaoBloqueioEstoque: TMenuItem;
    miAdiantamentoAcumulado: TMenuItem;
    miEtiquetas: TMenuItem;
    miPlanosFinanceiros: TMenuItem;
    miEntradaNotasFiscaisServico: TMenuItem;
    miRelacaoTransferenciaEmpresas: TMenuItem;
    miVincularcartoesTEF: TMenuItem;
    miAdministradorasCartoes: TMenuItem;
    tiTempoInativo: TTimer;
    miCartes1: TMenuItem;
    miBaixasCartoesArquivo: TMenuItem;
    miVidaCliente: TMenuItem;
    miICMS1: TMenuItem;
    miApuracaoICMS: TMenuItem;
    miPisCofins1: TMenuItem;
    miListaNegra: TMenuItem;
    miClientes1: TMenuItem;
    miLimiteCreditoCliente: TMenuItem;
    miContasCustodia: TMenuItem;
    miGruposClientes: TMenuItem;
    miAlterarsenhas1: TMenuItem;
    miListaContagem: TMenuItem;
    miRelatoriosGerais: TMenuItem;
    N3: TMenuItem;
    mniN4: TMenuItem;
    miPrecificacao: TMenuItem;
    Clientes1: TMenuItem;
    miComprasEntradas: TMenuItem;
    miRelacaoComprasEntradas: TMenuItem;
    miValidadorNFe: TMenuItem;
    miConsultaNFeCompleta: TMenuItem;
    miSintegra: TMenuItem;
    miConsultaInutilizacoes: TMenuItem;
    IAtalhoVendaRapdia: TImage;
    ActionManager1: TActionManager;
    sairsistema: TAction;
    ImageList1: TImageList;
    XColorM: TXPColorMap;
    Anydesk: TImage;
    pnlDataHora: TPanel;
    AMMBar: TActionMainMenuBar;
    imPrincipal: TImage;
    lbMesAno: TLabel;
    lbDiaSemana: TLabel;
    miDevolucoesEntradaNf: TMenuItem;
    miwhatsapp: TMenuItem;
    WhatsApp: TImage;
    miMarketPlace: TMenuItem;
    Label1: TLabel;
    lbEmpresa: TLabel;
    Label4: TLabel;
    lbUsuario: TLabel;
    Image1: TImage;
    miOutrasOperacoes: TMenuItem;
    Sefaz1: TMenuItem;
    Diversos1: TMenuItem;
    miFsist: TMenuItem;
    miEnderecosDeEstoque: TMenuItem;
    Rua1: TMenuItem;
    Mdulo1: TMenuItem;
    Nvel1: TMenuItem;
    Vo1: TMenuItem;
    Endereo1: TMenuItem;
    miConfirmarTransferenciaLocais: TMenuItem;
    Mapadeproduo1: TMenuItem;
    miConsultaPedidos: TMenuItem;
    miGrupoPrecificacaoEntreLojas: TMenuItem;
    miMovimentacoes: TMenuItem;
    Centralderelacionamentodocliente1: TMenuItem;
    Relaodeocorrncias1: TMenuItem;
    Acompanhamentodeoramentos1: TMenuItem;
    ipodeacompanhamentooramento1: TMenuItem;
    lbEndereco: TListBox;
    edtBuscarMenu: TButtonedEdit;
    Label2: TLabel;
    miProdutosSemVender: TMenuItem;
    Dashboard1: TMenuItem;
    iposdebloqueiosdeestoque1: TMenuItem;
    Camposobrigatrios1: TMenuItem;
    Produesdeestoque1: TMenuItem;
    Relaodecomissesporparceiros1: TMenuItem;
    ClientesquenocompramaXdias1: TMenuItem;
    miGruposdeplanosfinanceiros: TMenuItem;
    miDREContabil: TMenuItem;
    miAjustarFinalizacaoMensal: TMenuItem;
    miCorrecaoEstoqueFiscal: TMenuItem;
    miVendaAmbiente: TMenuItem;
    ConfiguracoesContasBoletos: TMenuItem;
    miEmitirboletos: TMenuItem;
    Comissoporlucratividade1: TMenuItem;
    Parmetrosdecomisso1: TMenuItem;
    miParametrosDiasDevolucao: TMenuItem;
    CadastrodeContagem: TMenuItem;
    LanamentodeContagem1: TMenuItem;
    Listadecontagemsimplificada1: TMenuItem;
    Acompanhamentodecontagem1: TMenuItem;
    Listadecontagemavanada1: TMenuItem;
    procedure miCadastrarNovoDesenvolvimentoClick(Sender: TObject);
    procedure miAcompanhamentoDesenvolvimentoClick(Sender: TObject);
    procedure miEstadosClick(Sender: TObject);
    procedure miProdutosClick(Sender: TObject);
    procedure miCidadesClick(Sender: TObject);
    procedure miBairrosClick(Sender: TObject);
    procedure miContasClick(Sender: TObject);
    procedure miTiposCobrancaClick(Sender: TObject);
    procedure miCondicoesPagamentoClick(Sender: TObject);
    procedure miFuncoesClick(Sender: TObject);
    procedure miFuncionariosClick(Sender: TObject);
    procedure miCFOPClick(Sender: TObject);
    procedure miLocaisProdutosClick(Sender: TObject);
    procedure miClientesClick(Sender: TObject);
    procedure miMarcasClick(Sender: TObject);
    procedure miUnidadesClick(Sender: TObject);
    procedure miEmpresasClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure miAberturaClick(Sender: TObject);
    procedure miSuprimentoClick(Sender: TObject);
    procedure miMovimentacoesContasClick(Sender: TObject);
    procedure miOrcamentosVendasClick(Sender: TObject);
    procedure miFornecedoresClick(Sender: TObject);
    procedure miReceberPedidosClick(Sender: TObject);
    procedure miSintegraClick(Sender: TObject);
    procedure miParametrosEmpresaClick(Sender: TObject);
    procedure miVidaProdutoClick(Sender: TObject);
    procedure miSangriaClick(Sender: TObject);
    procedure miFechamentoClick(Sender: TObject);
    procedure miConsultaNotasFiscaisClick(Sender: TObject);
    procedure miGerenciadoresCartoesTEFClick(Sender: TObject);
    procedure miCancelarRecebimentoPedidosClick(Sender: TObject);
    procedure miMotivosAjusteEstoqueClick(Sender: TObject);
    procedure miAjusteEstoqueClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure miDevolucaoProdutosClick(Sender: TObject);
    procedure miParametrosClick(Sender: TObject);
    procedure sbTrocarUsuarioClick(Sender: TObject);
    procedure lbOrcamentosVendasMouseEnter(Sender: TObject);
    procedure lbOrcamentosVendasMouseLeave(Sender: TObject);
    procedure sbRecebimentoPedidosClick(Sender: TObject);
    procedure miRelacaoContasReceberClick(Sender: TObject);
    procedure miRelacaoContasReceberBaixadosClick(Sender: TObject);
    procedure miInclusaoTitulosReceberClick(Sender: TObject);
    procedure miInclusaoTitulosPagarClick(Sender: TObject);
    procedure miRelaodecontasapagar1Click(Sender: TObject);
    procedure miRelacaoContasPagarBaixadosClick(Sender: TObject);
    procedure miAutorizacoesClick(Sender: TObject);
    procedure miRelacaoOrcamentosBloqueiosClick(Sender: TObject);
    procedure miAlterarSenhaClick(Sender: TObject);
    procedure miProdutosRelacionadosClick(Sender: TObject);
    procedure miRelacaoComissoesFuncionarioClick(Sender: TObject);
    procedure miRelacaoComissoesProdutoClick(Sender: TObject);
    procedure miRelacaoOrcamentosVendasClick(Sender: TObject);
    procedure miRelacaoDevolucoesClick(Sender: TObject);
    procedure miOutrasnotas1Click(Sender: TObject);
    procedure miGerarRetiradaClick(Sender: TObject);
    procedure miConfirmarRetiradasAtoClick(Sender: TObject);
    procedure miMensagensClick(Sender: TObject);
    procedure lbMensagensClick(Sender: TObject);
    procedure sbMensagensClick(Sender: TObject);
    procedure tiMensagensTimer(Sender: TObject);
    procedure miEntradasdeNFClick(Sender: TObject);
    procedure miRelacaoPreEntradasClick(Sender: TObject);
    procedure miDepartamentosSecoesLinhasClick(Sender: TObject);
    procedure miGerarEntregasClick(Sender: TObject);
    procedure sbConsultaProdutosVendaClick(Sender: TObject);
    procedure miPDVClick(Sender: TObject);
    procedure sbPDVClick(Sender: TObject);
    procedure lbPDVClick(Sender: TObject);
    procedure miRelacaoTurnosCaixaClick(Sender: TObject);
    procedure GruposEstoqueClick(Sender: TObject);
    procedure Produtopaifilho1Click(Sender: TObject);
    procedure miAgendarItensSemPrevisaoClick(Sender: TObject);
    procedure AnaliseProdutosControlamlotesClick(Sender: TObject);
    procedure miFormacaoCustoPrecoClick(Sender: TObject);
    procedure Percentuaisdecustosdaempresa1Click(Sender: TObject);
    procedure DefiniodeCFOPdeoperaes1Click(Sender: TObject);
    procedure miPromocoesClick(Sender: TObject);
    procedure miControleGerencialClick(Sender: TObject);
    procedure miRelacaoEntregasPendentesClick(Sender: TObject);
    procedure Preos1Click(Sender: TObject);
    procedure miProdutosPaiFilhoClick(Sender: TObject);
    procedure miRelacaoProdutosClick(Sender: TObject);
    procedure miPortadoresClick(Sender: TObject);
    procedure miRotasClick(Sender: TObject);
    procedure miCentroCustosClick(Sender: TObject);
    procedure miTransferenciaLocaisClick(Sender: TObject);
    procedure miSugestaoComprasClick(Sender: TObject);
    procedure miVeiculosClick(Sender: TObject);
    procedure miMotoristasClick(Sender: TObject);
    procedure miControleEntregasClick(Sender: TObject);
    procedure miConfirmarDevolucaoClick(Sender: TObject);
    procedure miComprasClick(Sender: TObject);
    procedure miRelacaoComprasClick(Sender: TObject);
    procedure miRelacaoPlanosfinanceirosClick(Sender: TObject);
    procedure miTransportadorasClick(Sender: TObject);
    procedure miSeriesNotasFiscaisClick(Sender: TObject);
    procedure miConhecimentosFreteClick(Sender: TObject);
    procedure miRelacaoEntradasClick(Sender: TObject);
    procedure miCreditosPagarClick(Sender: TObject);
    procedure miCreditosReceberClick(Sender: TObject);
    procedure tiPiscaNovasMensagensTimer(Sender: TObject);
    procedure miOrdenaodelocaisparaentregaporproduto1Click(Sender: TObject);
    procedure miImpressorasEstacaoClick(Sender: TObject);
    procedure miImpressorasUsuariosClick(Sender: TObject);
    procedure miLancamentoManualCaixaBancoClick(Sender: TObject);
    procedure miGruposFornecedoresClick(Sender: TObject);
    procedure miRelacaoAjustesEstoqueClick(Sender: TObject);
    procedure miRelacaoEntregasRealizadasClick(Sender: TObject);
    procedure miIPdoServidorClick(Sender: TObject);
    procedure SobreoAltis1Click(Sender: TObject);
    procedure miRelacaoEstoqueClick(Sender: TObject);
    procedure miImpostosIBPTClick(Sender: TObject);
    procedure miRelacaoControlesEntregasClick(Sender: TObject);
    procedure miSuporteRemotoClick(Sender: TObject);
    procedure miEstacaoClick(Sender: TObject);
    procedure miCondicoesPagamentoCartoesPDVClick(Sender: TObject);
    procedure miGerarArquivosRemessaBoletosClick(Sender: TObject);
    procedure imTrocarUsuarioClick(Sender: TObject);
    procedure imMensagensClick(Sender: TObject);
    procedure lbNovasMensagensClick(Sender: TObject);
    procedure imConsultaProdutosClick(Sender: TObject);
    procedure im4Click(Sender: TObject);
    procedure ConsultastatusNFe1Click(Sender: TObject);
    procedure im6Click(Sender: TObject);
    procedure miRetornoRemessaBoletosClick(Sender: TObject);
    procedure miFecharAcumuladoClick(Sender: TObject);
    procedure miRelacaoAcumuladosClick(Sender: TObject);
    procedure miCancelarRecebimentoAcumuladosClick(Sender: TObject);
    procedure miGruposTributacoesCompraFederalClick(Sender: TObject);
    procedure miGruposTributacoesVendaFederalClick(Sender: TObject);
    procedure miGruposTributacoesCompraEstadualClick(Sender: TObject);
    procedure miGruposTributacoesVendaEstadualClick(Sender: TObject);
    procedure miRelacaoVendasProdutosClick(Sender: TObject);
    procedure miIndicesdedescontosdevenda1Click(Sender: TObject);
    procedure miBaixarNovaCompilacaoClick(Sender: TObject);
    procedure miCargosProfissionaisClick(Sender: TObject);
    procedure miProfissionaisClick(Sender: TObject);
    procedure miTransferenciaProdutosEmpresasClick(Sender: TObject);
    procedure miMovimentosPendentesEmissaoNotaFiscalTransferenciaClick(Sender: TObject);
    procedure miBloqueioEstoqueClick(Sender: TObject);
    procedure miAdiantamentoAcumuladoClick(Sender: TObject);
    procedure miEtiquetasClick(Sender: TObject);
    procedure miPlanosFinanceirosClick(Sender: TObject);
    procedure miEntradaNotasFiscaisServicoClick(Sender: TObject);
    procedure miRelacaoTransferenciaEmpresasClick(Sender: TObject);
    procedure miVincularcartoesTEFClick(Sender: TObject);
    procedure miAdministradorasCartoesClick(Sender: TObject);
    procedure tiTempoInativoTimer(Sender: TObject);

    procedure AppMessage(var Msg: TMsg; var Handled: Boolean);
    procedure AppIdle(Sender: TObject; var Done: Boolean);
    procedure imPrincipalMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure miBaixasCartoesArquivoClick(Sender: TObject);
    procedure miVidaClienteClick(Sender: TObject);
    procedure miRelacaoConhecimentosFreteClick(Sender: TObject);
    procedure miApuracaoICMSClick(Sender: TObject);
    procedure im7Click(Sender: TObject);
    procedure miLimiteCreditoClienteClick(Sender: TObject);
    procedure miListaNegraClick(Sender: TObject);
    procedure miContasCustodiaClick(Sender: TObject);
    procedure miRelacaoCadastrosClick(Sender: TObject);
    procedure miGruposClientesClick(Sender: TObject);
    procedure miRelacaoBloqueioEstoqueClick(Sender: TObject);
    procedure miAlterarsenhaAltisW1Click(Sender: TObject);
    procedure miListaContagemClick(Sender: TObject);
    procedure miValidadorNFeClick(Sender: TObject);
    procedure miConsultaNFeCompletaClick(Sender: TObject);
    procedure miConsultaInutilizacoesClick(Sender: TObject);
    procedure miCurvaABCClick(Sender: TObject);
    procedure sairsistemaExecute(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure miDevolucoesEntradaNfClick(Sender: TObject);
    procedure miwhatsappClick(Sender: TObject);
    procedure miMarketPlaceClick(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure miOutrasOperacoes12Click(Sender: TObject);
    procedure miOutrasOperacoesClick(Sender: TObject);
    procedure miFsistClick(Sender: TObject);
    procedure Rua1Click(Sender: TObject);
    procedure Mdulo1Click(Sender: TObject);
    procedure Nvel1Click(Sender: TObject);
    procedure Vo1Click(Sender: TObject);
    procedure Endereo1Click(Sender: TObject);
    procedure miConfirmarTransferenciaLocaisClick(Sender: TObject);
    procedure Mapadeproduo1Click(Sender: TObject);
    procedure miConsultaPedidosClick(Sender: TObject);
    procedure miGrupoPrecificacaoEntreLojasClick(Sender: TObject);
    procedure miMovimentacoesClick(Sender: TObject);
    procedure Centralderelacionamentodocliente1Click(Sender: TObject);
    procedure Relaodeocorrncias1Click(Sender: TObject);
    procedure Acompanhamentodeoramentos1Click(Sender: TObject);
    procedure ipodeacompanhamentooramento1Click(Sender: TObject);
    procedure edtBuscarMenuChange(Sender: TObject);
    procedure edtBuscarMenuEnter(Sender: TObject);
    procedure edtBuscarMenuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure lbEnderecoDblClick(Sender: TObject);
    procedure lbEnderecoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure lbEnderecoMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure miProdutosSemVenderClick(Sender: TObject);
    procedure Dashboard1Click(Sender: TObject);
    procedure iposdebloqueiosdeestoque1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Camposobrigatrios1Click(Sender: TObject);
    procedure Produesdeestoque1Click(Sender: TObject);
    procedure Relaodecomissesporparceiros1Click(Sender: TObject);
    procedure ClientesquenocompramaXdias1Click(Sender: TObject);
    procedure miGruposdeplanosfinanceirosClick(Sender: TObject);
    procedure miDREContabilClick(Sender: TObject);
    procedure miAjustarFinalizacaoMensalClick(Sender: TObject);
    procedure Ajustarfinalizaomensal1Click(Sender: TObject);
    procedure miCorrecaoEstoqueFiscalClick(Sender: TObject);
    procedure miVendaAmbienteClick(Sender: TObject);
    procedure ConfiguracoesContasBoletosClick(Sender: TObject);
    procedure miEmitirboletosClick(Sender: TObject);
    procedure Comissoporlucratividade1Click(Sender: TObject);
    procedure Parmetrosdecomisso1Click(Sender: TObject);
    procedure miParametrosDiasDevolucaoClick(Sender: TObject);
    procedure CadastrodeContagemClick(Sender: TObject);
    procedure LanamentodeContagem1Click(Sender: TObject);
    procedure Listadecontagemsimplificada1Click(Sender: TObject);
    procedure Acompanhamentodecontagem1Click(Sender: TObject);
  private
    FIconeMensagem: Integer;
    FNotificacoes: TFormNotificacoes;
    ListaMenu: TList<TObjMenu>;

    procedure LocalizaMenu(pTituloMenu: String);

  public
    procedure NotificacaoEnvioNFe(pNotificacao: string);
    procedure AtualizarAtalhosSistema;
  end;

var
  FormMenuPrincipal: TFormMenuPrincipal;

implementation

{$R *.dfm}
//{$R anyDesk.RES}

uses
  _Biblioteca, CadastroDesenvolvimento, AcompanhamentoDesenvolvimentos, CadastroEstados, CadastroProdutos, CadastroCidades, CadastroBairros,
  CadastroContas, CadastroTiposCobranca, CadastroCondicoesPagamento, CadastroFuncoesFuncionarios, CadastroFuncionarios, CadastroCFOP,
  CadastroLocaisProdutos, CadastroClientes, CadastroMarcas, CadastroUnidades, CadastroEmpresas, AberturaCaixa, SuprimentoCaixa, FechamentoAcumulados,
  OrcamentosVendas, CadastroFornecedor, RecebimentoPedidos, RelacaoContasReceber, RelacaoComissoesPorProduto, RelacaoPlanosFinanceiros,
  OutrasOperacoes, ParametrosEmpresa, EntradaNotasFiscais, SangriaCaixa, FechamentoCaixa, VidaProduto, ConsultaNotasFiscais, Parametros,
  Cadastro.GerenciadoresCartoesTEF, CancelarRecebimentoPedido, Cadastro.MotivosAjusteEstoque, AjusteEstoque, DevolucaoProdutos, ConhecimentosFretes,
  RelacaoContasReceberBaixas, InclusaoTitulosReceber, InclusaoTitulosPagar, RelacaoContasPagar, RelacaoContasPagarBaixas, Cadastro.Autorizacoes,
  RelacaoOrcamentosBloqueios, AlterarSenhaUsuario, Cadastro.ProdutosRelacionados, RelacaoComissoesPorFuncionarios, Relacao.MovimentacoesBancosCaixas,
  Relacao.OrcamentosVendas, Relacao.Devolucoes, OutrasNotas, GerarRetiradas, ConfirmarRetiradas, CentralMensagensInternas, PreEntradasDocumentosFiscais,
  Cadastro.DepartamentosSecoesLinhas, GerarEntregas, PesquisaProdutosVenda, PDV, Relacao.NotasFiscais, Relacao.TurnosCaixa, CadastroSeriesNotasFiscais,
  GrupoEstoques, Cadastro.ProdutoPaiFilho, AgendarItensSemPrevisao, AnaliseProdutoLotes, FormacaoCustoPrecos, PercentuaisCustosEmpresa,
  DefinirCFOPsOperacoes, DefinirPromocoes, ControleGerencial, Relacao.EntregasPendentes, DefinirPrecosVenda, Relacao.ProdutosPaiFilho, RelacaoControlesEntregas,
  Relacao.Produtos, CadastroPortadores, CadastroRotas, CadastroCentroCusto, TransferenciaLocais, SugestaoCompras, ImpressorasEstacao, _Parametros,
  CadastroVeiculos, CadastroMotoristas, ControleEntregas, ConfirmarDevolucoes, CadastroCompras, RelacaoCompras, CadastroTransportadoras, ParametrosEstacao,
  RelacaoEntradasNotasFiscais, CadastroCreditosManuaisPagar, CadastroCreditosManuaisReceber, _Imagens, OrdenacaoLocaisEntregaProduto, ImpressorasUsuarios,
  LancamentoManualCaixaBanco, CadastroGruposFornecedores, RelacaoAjustesEstoque, RelacaoEntregasRealizadas, SobreHiva, RelacaoEstoque, ImportacaoImpostosIBPT,
  CondicoesPagamentoCartaoCreditoPDV, GerarArquivoRemessaBoleto, RetornoRemessaBoletos, RelacaoAcumulados, CancelarRecebimentoAcumulado, GruposTributacoesFederalCompra,
  GruposTributacoesFederalVenda, GruposTributacoesEstadualCompra, GruposTributacoesEstadualVenda, RelacaoVendasProdutos, CadastroIndicesDescontosVenda,
  CadastroCargosProfissionais, CadastroProfissionais, TransferenciasProdutosEntreEmpresas, MovimentosPendentesEmissaoNotaFiscalTransferencia,
  BloqueioEstoque, AdiantamentoAcumulado, Etiquetas, CadastroPlanosFinanceiros, Relacao.TransferenciasEmpresas, VinculacaoCartaoesReceber, CadastroListaNegra,
  CadastroAdministradorasCartoes, BaixaAutomaticaCartaoCielo, VidaCliente, RelacaoConhecimentosFretes, ApuracaoICMS, RelacaoLimiteCreditoClientes,
  CadastroContasCustodia, RelacaoCadastros, CadastroGruposClientes, RelacaoBloqueiosEstoques, ListaContagem,
  _Empresas, _RecordsCadastros, _AtalhosSistema, Informacoes.AtalhosSistema,
  Cadastrar.EnderecoEstoque.Rua, Cadastrar.EnderecoEstoque.Modulo, AjusteAnoMesFinalizado,
  Cadastrar.EnderecoEstoque.Nivel, RelacaoClientesSemComprar, ParametrosComissao,
  Cadastrar.EnderecoEstoque.Vao, Pesquisa.EnderecoEstoqueVao, RelacaoComissoesPorLucratividade,
  ConfirmarTransferenciasDeLocais, MapaProducaoEstoque, ConsultaPedidos, OrcamentosVendasAmbientes,
  GrupoPrecificacao, Relacao.MovimentosFinanceiros, RelacaoComissoesPorParceiros, ParametroDevolucaoAposPrazo,
  CentralRelacionamentoCliente, Relacao.CRMOcorrencias, Relacao.Orcamentos, Cadastrar.TipoAcompanhamentoOrcamento,
  Relacao.ProdutosSemVender, Cadastrar.TipoBloqueioEstoque, Cadastro.ValidacoesCadastroCliente,
  AjusteEstoqueFiscal, GruposPlanosFinanceiros, DREContabil, CadastroContasBoletos, RelacaoBoletos,
  CadastroContagemEstoque, LancamentoContagem, ListaContagemAvancada,
  AcompanhamentoContagem;


procedure TFormMenuPrincipal.Acompanhamentodecontagem1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormAcompanhamentoContagem);
end;

procedure TFormMenuPrincipal.Acompanhamentodeoramentos1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoOrcamentos);
end;

procedure TFormMenuPrincipal.miAjustarFinalizacaoMensalClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormAjusteFinalizacaoMensal);
end;

procedure TFormMenuPrincipal.Ajustarfinalizaomensal1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormAjusteFinalizacaoMensal);
end;

procedure TFormMenuPrincipal.AnaliseProdutosControlamlotesClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormAnaliseProdutoLotes);
end;

procedure TFormMenuPrincipal.Dashboard1Click(Sender: TObject);
begin
  if not Sessao.AutorizadoTela('tformrelacaodashboard') then begin
    Exclamar('Voc� n�o possui permiss�es necess�rias para acessar esta tela!');
    Exit;
  end;
  Sessao.AbrirTela(TFormRelacaoDashboard);
end;

procedure TFormMenuPrincipal.DefiniodeCFOPdeoperaes1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormDefinirCFOPOperacoes);
end;

procedure TFormMenuPrincipal.miDREContabilClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFrmDREContabil);
end;

procedure TFormMenuPrincipal.edtBuscarMenuChange(Sender: TObject);
var
  I: Integer;
begin
  if (edtBuscarMenu.Text = '') then begin
    lbEndereco.Visible := False;
    Exit;
  end;

  if Length(edtBuscarMenu.Text) < 3 then
    Exit;

  LocalizaMenu(UpperCase(edtBuscarMenu.Text));
  lbEndereco.Items.Clear;
  for I := 0 to ListaMenu.Count - 1 do
    lbEndereco.Items.Add(ListaMenu[I].Caminho);
//
//  lbEndereco.top := pnlDataHora.Top - 150;
//  lbEndereco.left := pnlDataHora.left;

  lbEndereco.Visible := True;
  lbEndereco.BringToFront;
end;

procedure TFormMenuPrincipal.edtBuscarMenuEnter(Sender: TObject);
begin
  edtBuscarMenu.Clear;
  lbEndereco.Clear;

end;

procedure TFormMenuPrincipal.edtBuscarMenuKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_RETURN) or (Key = VK_DOWN) then begin
    if (lbEndereco.Count > 0) then begin
      lbEndereco.SetFocus;
      lbEndereco.ItemIndex := 0;
    end;
  end
  else if (Key = VK_ESCAPE) then begin
    edtBuscarMenu.Clear;
    lbEndereco.Visible := False;
  end;
end;

procedure TFormMenuPrincipal.miEmitirboletosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoBoletos);
end;

procedure TFormMenuPrincipal.Endereo1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormEnderecoEstoque);
end;

procedure TFormMenuPrincipal.miEtiquetasClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormEtiquetas);
end;

procedure TFormMenuPrincipal.miRelacaoEntregasPendentesClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoEntregasPendentes);
end;

procedure TFormMenuPrincipal.miRelacaoEntregasRealizadasClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoRetiradasEntregas);
end;

procedure TFormMenuPrincipal.miRelacaoEstoqueClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoEstoque);
end;

procedure TFormMenuPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if Screen.FormCount > 3 then begin
    _Biblioteca.Exclamar('Feche todas as telas antes de finalizar o sistema!');
    Abort;
  end;

  if not _Biblioteca.Perguntar('Deseja realmente finalizar o sistema?') then
    Abort;

  _Sessao.Sessao.Free;
  _Sessao.Sessao := _Sessao.Sessao.Create;
end;

procedure TFormMenuPrincipal.FormCreate(Sender: TObject);
var
  vData: TDateTime;
  vRespBanco: RecRetornoBD;

  function NewActionOnClick(EventoClick: TNotifyEvent; pImageIndex: Integer): TAction;
  var
    action : TAction;
  begin
    action := TAction.Create(nil);
    action.OnExecute := EventoClick;
    action.imageindex := pImageIndex;
    Result := action;
  end;

  procedure ListaItemMenu(pItemMenu : TMenuItem; pItem: TActionClientItem);
  var
    vCnt : Integer;
  begin
    for vCnt := 0 to pItemMenu.Count - 1 do
    begin
      if Assigned(pItemMenu[vCnt].OnClick) then
        pItem.Action := NewActionOnClick(pItemMenu[vCnt].OnClick,pItemMenu[vCnt].ImageIndex);


      pItem.Visible := pItemMenu[vCnt].Visible;
      pItem.Caption := pItemMenu[vCnt].Caption ;
      pItemMenu[vCnt].Visible := false;
      if pItemMenu[vCnt].Count > 0 then
        ListaItemMenu(pItemMenu[vCnt],pItem.Items.Add);

      pItem := pItem.ParentItem.Items.Add;
    end;
  end;

begin
  vRespBanco := _Parametros.FinalizarAnoMes(Sessao.getConexaoBanco);
  if vRespBanco.TeveErro then begin
    _Biblioteca.Exclamar('Falha ao gravar finaliza��o do m�s, entre em contato com a Hiva! ' + vRespBanco.MensagemErro);
    Halt;
  end;

  vData := Sessao.getDataHora;
  lbUsuario.Caption := Copy('' + IntToStr(Sessao.getUsuarioLogado.funcionario_id) + ' ' + Sessao.getUsuarioLogado.nome, 1, 40);
  lbEmpresa.Caption := '' + IntToStr(Sessao.getEmpresaLogada.EmpresaId) + ' ' + Sessao.getEmpresaLogada.NomeFantasia;

  lbDiaSemana.Caption := _Biblioteca.DiaDaSemana(vData);
  lbMesAno.Caption := DateToStr(vData);//_Biblioteca.MesDoAno(vData) + '' + NFormat(YearOf(vData));

  //pnMenu.Color := _Biblioteca.getCorVerdePadraoAltis;

  if Sender <> nil then
    Caption := Application.Title + ' - ' + Caption;

  tiMensagens.Enabled := True;
  tiMensagensTimer(Sender);

  // Fica ativo somente se for a base da Hiva
  //miRotinasHiva.Visible         := Sessao.getUsuarioLogado.funcionario_id = 1;
  miDesenvolvimento.Visible      := Sessao.getEmpresaLogada.Cnpj = '33.433.422/0001-00';
//  miDesenvolvimento.Visible      := True;
  //miBaixarNovaCompilacao.Visible := Sessao.AutorizadoRotina('baixar_nova_compilcao');
  miRelacaoPreEntradas.Visible                     := (Sessao.getParametros.UtilModuloPreEntradas = 'S') or (Sessao.getUsuarioLogado.funcionario_id = 1);
  //Habilitar venda rapida e configura��es

  //Venda Rapida
  miPDV.Visible                                    := (Sessao.getParametros.UtilVendaRapida = 'S') or (Sessao.getUsuarioLogado.funcionario_id = 1);
  IAtalhoVendaRapdia.Visible                       := (Sessao.getParametros.UtilVendaRapida = 'S') or (Sessao.getUsuarioLogado.funcionario_id = 1);
  miCondicoesPagamentoCartoesPDV.Visible           := (Sessao.getParametros.UtilVendaRapida = 'S') or (Sessao.getUsuarioLogado.funcionario_id = 1);

  miVendaAmbiente.Visible                          := Sessao.getParametros.OrcamentoAmbiente = 'S';

  //Habilitar controle de entrega
  miEntregas.Visible                               := (Sessao.getParametros.UtilControleEntrega = 'S') or (Sessao.getUsuarioLogado.funcionario_id = 1);
  miControleEntregas.Visible                       := (Sessao.getParametros.UtilizaControleManifesto = 'S');
  Relatrios2.Visible                               := (Sessao.getParametros.UtilControleEntrega = 'S') or (Sessao.getUsuarioLogado.funcionario_id = 1);
  Fretes1.Visible                                  := (Sessao.getParametros.UtilControleEntrega = 'S') or (Sessao.getUsuarioLogado.funcionario_id = 1);

  miParametrosDiasDevolucao.Visible                := (Sessao.getParametrosEmpresa.PermitirDevolucaoAposPrazo = 'S') and (Sessao.AutorizadoTela('tformparametrodevolucaoaposprazo'));

  miMarketPlace.Visible                            := (Sessao.getParametros.UtilMarketPlace = 'S') or (Sessao.getUsuarioLogado.funcionario_id = 1);

  miParametros.Visible                             := (Sessao.getUsuarioLogado.funcionario_id = 1);
  miParametrosEmpresa.Visible                      := (Sessao.getUsuarioLogado.funcionario_id = 1);
  miEmpresas.Visible                               := (Sessao.getUsuarioLogado.funcionario_id = 1);
  miIndicesdedescontosdevenda1.Visible             := (Sessao.getUsuarioLogado.funcionario_id = 1);
  miImportaes1.Visible                             := (Sessao.getUsuarioLogado.funcionario_id = 1);
  miEstacao.Visible                                := (Sessao.getUsuarioLogado.funcionario_id = 1);

  miConfirmarTransferenciaLocais.Visible           := (Sessao.getParametros.UtilConfirmacaoTransferenciaLocais = 'S');

  miEnderecosDeEstoque.Visible := Sessao.getParametros.UtilEnderecoEstoque = 'S';

  miAjustarFinalizacaoMensal.Visible := Sessao.AutorizadoTela('tformajustefinalizacaomensal');
  miMovimentacoes.Visible := Sessao.AutorizadoTela('tformmovimentosfinanceiros');
  miDREContabil.Visible := Sessao.AutorizadoTela('tfrmdrecontabil');

  miCorrecaoEstoqueFiscal.Visible := Sessao.getParametros.AjusteEstoqueReal = 'S';
  miBoletos.Visible  := (Sessao.getParametros.UtilizaEmissaoBoleto = 'S');
  miGruposdeplanosfinanceiros.Visible  := (Sessao.getParametros.UtilizaDRE = 'S');
  miDREContabil.Visible  := (Sessao.getParametros.UtilizaDRE = 'S');

  _Sessao.Sessao.setTimerSistemaOcioso(tiTempoInativo);

  Application.OnMessage := AppMessage;
  if sessao.getUsuarioLogado.FinalizarSessao = 'S' then
    Application.OnIdle    := AppIdle;

  ListaItemMenu(MenuPrincipal.Items,ActionManager1.ActionBars[0].Items[0].Items.Add);

  AtualizarAtalhosSistema;
end;

procedure TFormMenuPrincipal.FormResize(Sender: TObject);
begin
  //AMMBar.Width := self.Width;
 // AMMBar.Left := 0;
end;

procedure TFormMenuPrincipal.AppIdle(Sender: TObject; var Done: Boolean);
begin
  tiTempoInativo.Enabled := True;
end;

procedure TFormMenuPrincipal.AppMessage(var Msg: TMsg; var Handled: Boolean);
begin
  case Msg.message of
    WM_LBUTTONDOWN, WM_RBUTTONDOWN, WM_KEYDOWN: Sessao.setTimerSistemaOciosoHouveMovimento;
  end;
end;

procedure TFormMenuPrincipal.AtualizarAtalhosSistema;
var
  atalhos: TArray<RecAtalhosSistema>;
  i: Integer;
begin
  //Copiar essas 4 linhas e modificar s� os valores de atalho_sistema_id, atalho e descri��o
  SetLength(atalhos, Length(atalhos) + 1);
  atalhos[High(atalhos)].atalho_sistema_id := 1;
  atalhos[High(atalhos)].atalho            := 'F1';
  atalhos[High(atalhos)].descricao         := 'Cadastro de clientes';

  SetLength(atalhos, Length(atalhos) + 1);
  atalhos[High(atalhos)].atalho_sistema_id := 2;
  atalhos[High(atalhos)].atalho            := 'F2';
  atalhos[High(atalhos)].descricao         := 'Or�amento/Vendas';

  SetLength(atalhos, Length(atalhos) + 1);
  atalhos[High(atalhos)].atalho_sistema_id := 3;
  atalhos[High(atalhos)].atalho            := 'F3';
  atalhos[High(atalhos)].descricao         := 'Receber vendas';

  SetLength(atalhos, Length(atalhos) + 1);
  atalhos[High(atalhos)].atalho_sistema_id := 4;
  atalhos[High(atalhos)].atalho            := 'Ctrl + L';
  atalhos[High(atalhos)].descricao         := 'Liberar Or�amento/Vendas bloqueadas';

  SetLength(atalhos, Length(atalhos) + 1);
  atalhos[High(atalhos)].atalho_sistema_id := 5;
  atalhos[High(atalhos)].atalho            := 'Ctrl + P';
  atalhos[High(atalhos)].descricao         := 'Pesquisa de produtos/Pre�os';

  SetLength(atalhos, Length(atalhos) + 1);
  atalhos[High(atalhos)].atalho_sistema_id := 6;
  atalhos[High(atalhos)].atalho            := 'Ctrl + F';
  atalhos[High(atalhos)].descricao         := 'Precificacao de mercadorias';

  SetLength(atalhos, Length(atalhos) + 1);
  atalhos[High(atalhos)].atalho_sistema_id := 7;
  atalhos[High(atalhos)].atalho            := 'Ctrl + S';
  atalhos[High(atalhos)].descricao         := 'Precificacao simplificada';

  SetLength(atalhos, Length(atalhos) + 1);
  atalhos[High(atalhos)].atalho_sistema_id := 8;
  atalhos[High(atalhos)].atalho            := 'Ctrl + X';
  atalhos[High(atalhos)].descricao         := 'Sair do Sistema';

  SetLength(atalhos, Length(atalhos) + 1);
  atalhos[High(atalhos)].atalho_sistema_id := 9;
  atalhos[High(atalhos)].atalho            := 'Ctrl + M';
  atalhos[High(atalhos)].descricao         := 'Mensagens';

  SetLength(atalhos, Length(atalhos) + 1);
  atalhos[High(atalhos)].atalho_sistema_id := 10;
  atalhos[High(atalhos)].atalho            := 'Ctrl + N';
  atalhos[High(atalhos)].descricao         := 'Relacao de notas fiscais';

    SetLength(atalhos, Length(atalhos) + 1);
  atalhos[High(atalhos)].atalho_sistema_id := 11;
  atalhos[High(atalhos)].atalho            := 'Ctrl + O';
  atalhos[High(atalhos)].descricao         := 'Consulta de pedidos';

      SetLength(atalhos, Length(atalhos) + 1);
  atalhos[High(atalhos)].atalho_sistema_id := 12;
  atalhos[High(atalhos)].atalho            := 'Ctrl + E';
  atalhos[High(atalhos)].descricao         := 'Etiquetas';

  //Manter essa parte sempre no final
  for i := 0 to Length(atalhos) - 1 do begin
    _AtalhosSistema.AtualizarAtalhosSistema(
      Sessao.getConexaoBanco,
      atalhos[i].atalho_sistema_id,
      atalhos[i].atalho,
      atalhos[i].descricao
    );
  end;
end;

procedure TFormMenuPrincipal.Button1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroValidacoesCadastroCliente);
end;

procedure TFormMenuPrincipal.GruposEstoqueClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormGruposEstoques);
end;

procedure TFormMenuPrincipal.im4Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormOrcamentosVendas, False, 5);
end;


procedure TFormMenuPrincipal.miConfirmarTransferenciaLocaisClick(
  Sender: TObject);
begin
  Sessao.AbrirTela(TFormConfirmarTransferenciasDeLocais);
end;

procedure TFormMenuPrincipal.CadastrodeContagemClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroContagemEstoque);
end;

procedure TFormMenuPrincipal.Camposobrigatrios1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroValidacoesCadastroCliente);
end;

procedure TFormMenuPrincipal.Centralderelacionamentodocliente1Click(
  Sender: TObject);
begin
  Sessao.AbrirTela(TFormCentralRelacionamentoCliente);
end;

procedure TFormMenuPrincipal.ClientesquenocompramaXdias1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoClienteSemComprar);
end;

procedure TFormMenuPrincipal.ConsultastatusNFe1Click(Sender: TObject);
begin
  miReceberPedidos.Click;
end;

procedure TFormMenuPrincipal.miCorrecaoEstoqueFiscalClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormAjusteEstoqueFiscal);
end;

procedure TFormMenuPrincipal.Comissoporlucratividade1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoComissoesPorLucratividade);
end;

procedure TFormMenuPrincipal.ConfiguracoesContasBoletosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFrmCadastroContasBoletos);
end;

procedure TFormMenuPrincipal.im6Click(Sender: TObject);
begin
  miPDVClick(Sender);
end;

procedure TFormMenuPrincipal.im7Click(Sender: TObject);
begin
  miVidaClienteClick(Sender);
end;

procedure TFormMenuPrincipal.Image1Click(Sender: TObject);
var
  vForm: TFormAtalhoSistema;
begin
  vForm := TFormAtalhoSistema.Create(Application);
  vForm.Show;
//  Sessao.AbrirTela(TFormAtalhoSistema, False);
end;

procedure TFormMenuPrincipal.imConsultaProdutosClick(Sender: TObject);
begin
  if Sessao.getParametrosEmpresa.CondPagtoPadraoPesqVenda = 0 then begin
    _Biblioteca.Exclamar('A condi��o de pagamento padr�o para pesquisa de produtos n�o foi definida!');
    Exit;
  end;

  PesquisaProdutosVenda.PesquisarProdutosVendas(Sessao.getParametrosEmpresa.CondPagtoPadraoPesqVenda, True, False, False);
end;

procedure TFormMenuPrincipal.imMensagensClick(Sender: TObject);
begin
  miMensagensClick(Sender);
end;

procedure TFormMenuPrincipal.imPrincipalMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  Sessao.setTimerSistemaOciosoHouveMovimento;
end;

procedure TFormMenuPrincipal.imTrocarUsuarioClick(Sender: TObject);
begin
  tiMensagens.Enabled := False;

  _Sessao.Sessao.Finalizar;
  _Sessao.Sessao.Iniciar;
  FormCreate(nil);
end;

procedure TFormMenuPrincipal.ipodeacompanhamentooramento1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroTipoAcompanhamentoOrcamento);
end;

procedure TFormMenuPrincipal.iposdebloqueiosdeestoque1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroTipoBloqueioEstoque);
end;

procedure TFormMenuPrincipal.LanamentodeContagem1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormLancamentoContagem);
end;

procedure TFormMenuPrincipal.lbEnderecoDblClick(Sender: TObject);
var
  ItemMenu : TComponent;
begin
  try
    ItemMenu := ListaMenu[lbEndereco.ItemIndex].Menu;
    if (ItemMenu Is TMenuItem) then begin
      (ItemMenu As TMenuItem).OnClick(ItemMenu);
      lbEndereco.Clear;
      lbEndereco.Visible := False;
      edtBuscarMenu.Clear;
    end;
  except
  //
  end;
end;

procedure TFormMenuPrincipal.lbEnderecoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Shift = [ssCtrl]) and
     (Key = 67) and
     (lbEndereco.Items.Count > 0) then
    Clipboard.AsText := lbEndereco.Items[lbEndereco.ItemIndex]
  else if (Key = VK_RETURN) and
     (lbEndereco.Items.Count > 0) then
    lbEnderecoDblClick(Sender);
end;

procedure TFormMenuPrincipal.lbEnderecoMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  APoint: TPoint;
  Index: integer;
begin
  if Button = mbRight then
  begin
    APoint.X := X;
    APoint.Y := Y;
    Index := lbEndereco.ItemAtPos(APoint, True);
    if Index > -1 then
      lbEndereco.Selected[Index] := True;
  end;
end;

procedure TFormMenuPrincipal.lbMensagensClick(Sender: TObject);
begin
  miMensagensClick(Sender);
end;

procedure TFormMenuPrincipal.lbNovasMensagensClick(Sender: TObject);
begin
  miMensagensClick(Sender);
end;

procedure TFormMenuPrincipal.lbOrcamentosVendasMouseEnter(Sender: TObject);
begin
  TControl(Sender).Cursor := crHandPoint;
end;

procedure TFormMenuPrincipal.lbOrcamentosVendasMouseLeave(Sender: TObject);
begin
  TControl(Sender).Cursor := crDefault;
end;

procedure TFormMenuPrincipal.lbPDVClick(Sender: TObject);
begin
  miPDVClick(Sender);
end;

procedure TFormMenuPrincipal.Listadecontagemsimplificada1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormListaContagem);
end;

procedure TFormMenuPrincipal.LocalizaMenu(pTituloMenu: String);
var
  ItemMenu, ItemMenuParent: TMenuItem;
  I, J, K       : Integer;
  Caminho       : String;
  objMenu       : TObjMenu;
  Existe        : Boolean;
begin
  if Assigned(ListaMenu) then
  begin
    for I := ListaMenu.Count - 1 downto 0 do
    begin
      objMenu := ListaMenu[I];
      FreeAndNil(objMenu);
    end;
    FreeAndNil(ListaMenu);
  end;

  ListaMenu := TList<TObjMenu>.Create;

  for I := 0 to (ComponentCount - 1) do
  begin
    if (Components[I] is TMenuItem) then
    begin
      ItemMenu := TMenuItem(Components[I]);

//      if (not ItemMenu.Visible) then
//        Continue;

      if (not ItemMenu.Enabled) then
        Continue;

//      if (ItemMenu.Tag > 0) then
//        Continue;

      if (ItemMenu.Name = '') then
        Continue;

      if not (ContainsText(RetirarAcentos(UpperCase(ItemMenu.Caption.Replace('&',''))), RetirarAcentos(pTituloMenu))) and
         (Assigned(ItemMenu.OnClick) or
          (ItemMenu.Count > 0)) then
        Continue;

      objMenu := nil;
      objMenu := TObjMenu.Create;
      objMenu.Menu := ItemMenu;

      if Assigned(ItemMenu.OnClick) then
      begin
        Caminho := '';

        while (ItemMenu.Parent.Caption <> '') do begin
          Caminho := Caminho + IfThen(Caminho <> '',
                                   ' | ',
                                   '') + ItemMenu.Caption.Replace('&','');
          ItemMenu := ItemMenu.Parent;
        end;
        Caminho := Caminho + IfThen(Caminho <> '',
                                 ' | ',
                                 '') + ItemMenu.Caption.Replace('&','');

        Existe := False;
        for J := 0 to ListaMenu.Count - 1 do
        if Inverter(Caminho) = ListaMenu[J].Caminho then
        begin
          Existe := True;
          Break;
        end;

        if Existe then
        begin
          FreeAndNil(objMenu);
          Continue;
        end;

        objMenu.Caminho := Inverter(Caminho);
        ListaMenu.Add(objMenu);
      end
      else
      begin
        for J := 0 to ItemMenu.Count - 1 do
        begin
          if (ItemMenu[J].Visible) and
             (Assigned(ItemMenu[J].OnClick)) then
          begin
//            if (not ItemMenu[J].Visible) then
//              Continue;

            if (not ItemMenu[J].Enabled) then
              Continue;

//            if (ItemMenu[J].Tag > 0) then
//              Continue;

            if (ItemMenu[J].Name = '') then
              Continue;

            objMenu := nil;
            objMenu := TObjMenu.Create;
            objMenu.Menu := ItemMenu[J];

            Caminho := '';
            ItemMenuParent := ItemMenu[J];

            while (ItemMenuParent.Parent.Caption <> '') do begin
              Caminho := Caminho + IfThen(Caminho <> '',
                                       ' | ',
                                       '') + ItemMenuParent.Caption.Replace('&','');
              ItemMenuParent := ItemMenuParent.Parent;
            end;
            Caminho := Caminho + IfThen(Caminho <> '',
                                     ' | ',
                                     '') + ItemMenuParent.Caption.Replace('&','');

            Existe := False;
            for K := 0 to ListaMenu.Count - 1 do
            if Inverter(Caminho) = ListaMenu[K].Caminho then
            begin
              Existe := True;
              Break;
            end;
            if Existe then
            begin
              FreeAndNil(objMenu);
              Continue;
            end;

            objMenu.Caminho := Inverter(Caminho);
            ListaMenu.Add(objMenu);
          end;
        end;
      end;
    end;

  end;
end;

procedure TFormMenuPrincipal.Mapadeproduo1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormMapaProducao);
end;

procedure TFormMenuPrincipal.Mdulo1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormEnderecoEstoqueModulo);
end;

procedure TFormMenuPrincipal.miAberturaClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormAberturaCaixa);
end;

procedure TFormMenuPrincipal.miAcompanhamentoDesenvolvimentoClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormAcompanhamentoDesenvolvimentos);
end;

procedure TFormMenuPrincipal.miAdiantamentoAcumuladoClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormAdiantamentoAcumulado);
end;

procedure TFormMenuPrincipal.miAdministradorasCartoesClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroAdministradorasCartoes);
end;

procedure TFormMenuPrincipal.miAgendarItensSemPrevisaoClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormAgendarItensSemPrevisao);
end;

procedure TFormMenuPrincipal.miAjusteEstoqueClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormAjusteEstoque);
end;

procedure TFormMenuPrincipal.miAlterarsenhaAltisW1Click(Sender: TObject);
begin
  AlterarSenhaUsuario.AtualizarSenha(True);
end;

procedure TFormMenuPrincipal.miAlterarSenhaClick(Sender: TObject);
begin
  AlterarSenhaUsuario.AtualizarSenha(False);
end;

procedure TFormMenuPrincipal.miApuracaoICMSClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormApuracaoICMS);
end;

procedure TFormMenuPrincipal.miAutorizacoesClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroAutorizacoes);
end;

procedure TFormMenuPrincipal.miCadastrarNovoDesenvolvimentoClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroDesenvolvimento);
end;

procedure TFormMenuPrincipal.miCancelarRecebimentoAcumuladosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCancelarRecebimentoAcumulado);
end;

procedure TFormMenuPrincipal.miCancelarRecebimentoPedidosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCancelarRecebimentoPedido);
end;

procedure TFormMenuPrincipal.miCargosProfissionaisClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroCargosProfissionais);
end;

procedure TFormMenuPrincipal.miCentroCustosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroCentroCustos);
end;

procedure TFormMenuPrincipal.miBairrosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroBairros);
end;

procedure TFormMenuPrincipal.miBaixarNovaCompilacaoClick(Sender: TObject);
var
  vChamada: PAnsiChar;
begin
  if
    not _Biblioteca.Perguntar(
      'O Hiva ser� finalizado e aberto novamente ap�s a atualiza��o, ser�o executados comandos que pode ocasionar lentid�o ou at� mesmo travamento ' +
      'no banco de dados, deseja realmente continuar?'
    )
  then
    Exit;

  vChamada := PAnsiChar(AnsiString('Hivaconexoes.exe "BAIXAR_NOVA_COMPILACAO' + Sessao.getVersaoSistemaResumido + '"'));
  WinExec(vChamada, SW_HIDE);
end;

procedure TFormMenuPrincipal.miBaixasCartoesArquivoClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormBaixaAutomaticaCartaoCielo);
end;

procedure TFormMenuPrincipal.miBloqueioEstoqueClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormBloqueioEstoque);
end;

procedure TFormMenuPrincipal.miContasClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroContas);
end;

procedure TFormMenuPrincipal.miContasCustodiaClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroContasCustodia);
end;

procedure TFormMenuPrincipal.miTiposCobrancaClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroTiposCobranca);
end;

procedure TFormMenuPrincipal.miTransferenciaProdutosEmpresasClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormTransferenciasProdutosEntreEmpresas);
end;

procedure TFormMenuPrincipal.miTransferenciaLocaisClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormTransferenciaLocais);
end;

procedure TFormMenuPrincipal.miTransportadorasClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroTransportadoras);
end;

procedure TFormMenuPrincipal.miUnidadesClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroUnidades);
end;

procedure TFormMenuPrincipal.miVidaClienteClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormVidaCliente);
end;

procedure TFormMenuPrincipal.miVidaProdutoClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormVidaProduto);
end;

procedure TFormMenuPrincipal.miVincularcartoesTEFClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormVinculacaoCartaoesReceber);
end;

procedure TFormMenuPrincipal.miwhatsappClick(Sender: TObject);
begin
  inherited;
  ShellExecute(handle, 'open', PChar('https://api.whatsapp.com/send?phone=5562991476228'), '', '', SW_SHOWNORMAL);
end;

procedure TFormMenuPrincipal.miMovimentacoesClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormMovimentosFinanceiros);
end;

procedure TFormMenuPrincipal.sairsistemaExecute(Sender: TObject);
begin
  close;
end;

procedure TFormMenuPrincipal.sbConsultaProdutosVendaClick(Sender: TObject);
begin
  if Sessao.getParametrosEmpresa.CondPagtoPadraoPesqVenda = 0 then begin
    _Biblioteca.Exclamar('A condi��o de pagamento padr�o para pesquisa de produtos n�o foi definida!');
    Exit;
  end;

  PesquisaProdutosVenda.PesquisarProdutosVendas(Sessao.getParametrosEmpresa.CondPagtoPadraoPesqVenda, True, False, False);
end;

procedure TFormMenuPrincipal.sbMensagensClick(Sender: TObject);
begin
  miMensagensClick(Sender);
end;

procedure TFormMenuPrincipal.sbPDVClick(Sender: TObject);
begin
  miPDVClick(Sender);
end;

procedure TFormMenuPrincipal.sbRecebimentoPedidosClick(Sender: TObject);
begin
  miReceberPedidos.Click;
end;

procedure TFormMenuPrincipal.sbTrocarUsuarioClick(Sender: TObject);
begin
  tiMensagens.Enabled := False;

  _Sessao.Sessao.Finalizar;
  _Sessao.Sessao.Iniciar;
  FormCreate(nil);
end;

procedure TFormMenuPrincipal.SobreoAltis1Click(Sender: TObject);
begin
  SobreHiva.AbrirSobre;
end;

procedure TFormMenuPrincipal.tiMensagensTimer(Sender: TObject);
var
  vQtdeMensagens: Integer;
begin
  vQtdeMensagens := _Mensagens.BuscarQuantidadeMensagensNaoLidas(Sessao.getConexaoBanco, Sessao.getUsuarioLogado.funcionario_id);
  lbNovasMensagens.Caption := NFormat(vQtdeMensagens) + ' Mensagen(s)';
  lbNovasMensagens.Visible := (vQtdeMensagens > 0);

  tiPiscaNovasMensagens.Enabled := lbNovasMensagens.Visible;
end;

procedure TFormMenuPrincipal.tiPiscaNovasMensagensTimer(Sender: TObject);
begin
  FIconeMensagem := IIfInt(FIconeMensagem = 0, 1);
  if FIconeMensagem = 1 then
    Application.Icon := FormImagens.Im6.Picture.Icon
  else
    Application.Icon := FormImagens.Im7.Picture.Icon;
end;

procedure TFormMenuPrincipal.tiTempoInativoTimer(Sender: TObject);
var
  vFuncionarioId: Integer;
  i: Integer;
  vForm: TForm;
  pEmpresa: RecEmpresas;
begin
  tiMensagens.Enabled    := False;
  tiTempoInativo.Enabled := False;
  Application.OnIdle     := nil;
  Application.OnMessage  := nil;

  vFuncionarioId := Login.Logar(Sessao.getUsuarioLogado.funcionario_id, pEmpresa);
  if vFuncionarioId = 0 then begin
    Sessao.FinalizandoPorTempoInativo := True;

    i := 0;
    While i <= ( Application.ComponentCount - 1 ) do begin
      if ( Application.Components[i] is TForm ) then begin
        vForm := TForm(Application.Components[i]);
        If vForm <> Application.MainForm then begin
          i := -1;

          vForm.Close;
          vForm.Free;
        end;
      end;
      inc(i);
    end;

    Halt;
    Exit;
  end;

  tiTempoInativo.Enabled := True;
  tiMensagens.Enabled    := True;
  Application.OnIdle     := AppIdle;
  Application.OnMessage  := AppMessage;
end;

procedure TFormMenuPrincipal.miVendaAmbienteClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormOrcamentosVendasAmbientes, False, 5);
end;

procedure TFormMenuPrincipal.Vo1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormEnderecoEstoqueVao);
end;

procedure TFormMenuPrincipal.miVeiculosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroVeiculos);
end;

procedure TFormMenuPrincipal.miCFOPClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroCFOP);
end;

procedure TFormMenuPrincipal.miCidadesClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroCidades);
end;

procedure TFormMenuPrincipal.miClientesClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroClientes);
end;

procedure TFormMenuPrincipal.miRelacaoAcumuladosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoAcumulados);
end;

procedure TFormMenuPrincipal.miRelacaoAjustesEstoqueClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoAjusteEstoque);
end;

procedure TFormMenuPrincipal.miRelacaoBloqueioEstoqueClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoBloqueiosEstoques);
end;

procedure TFormMenuPrincipal.miRelacaoCadastrosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoCadastros);
end;

procedure TFormMenuPrincipal.miRelacaoComissoesFuncionarioClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoComissoesPorFuncionario);
end;

procedure TFormMenuPrincipal.miRelacaoComissoesProdutoClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoComissoesPorProduto);
end;

procedure TFormMenuPrincipal.miRelacaoComprasClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoCompras);
end;

procedure TFormMenuPrincipal.miComprasClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroCompras, False, 5);
end;

procedure TFormMenuPrincipal.miCondicoesPagamentoClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroCondicoesPagamento);
end;

procedure TFormMenuPrincipal.miCondicoesPagamentoCartoesPDVClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCondicoesPagamentoCartaoCreditoPDV);
end;

procedure TFormMenuPrincipal.miConfirmarDevolucaoClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormConfirmarDevolucoes);
end;

procedure TFormMenuPrincipal.miConfirmarRetiradasAtoClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormConfirmarRetiradas);
end;

procedure TFormMenuPrincipal.miConhecimentosFreteClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormConhecimentosFretes);
end;

procedure TFormMenuPrincipal.miDepartamentosSecoesLinhasClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroDepartamentosSecoesLinhas);
end;

procedure TFormMenuPrincipal.miDevolucaoProdutosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormDevolucaoProdutos);
end;

procedure TFormMenuPrincipal.miDevolucoesEntradaNfClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormDevolucaoEntradaNotaFiscal);
end;

procedure TFormMenuPrincipal.miEmpresasClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroEmpresas);
end;

procedure TFormMenuPrincipal.miEntradaNotasFiscaisServicoClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormEntradaNotasFiscaisServicos);
end;

procedure TFormMenuPrincipal.miEntradasdeNFClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormEntradaNotasFiscais);
end;

procedure TFormMenuPrincipal.miImpostosIBPTClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormImportacaoImpostosIBPT);
end;

procedure TFormMenuPrincipal.miImpressorasEstacaoClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormImpressorasEstacao);
end;

procedure TFormMenuPrincipal.miImpressorasUsuariosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormImpressorasUsuario);
end;

procedure TFormMenuPrincipal.miEstacaoClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormParametrosEstacao);
end;

procedure TFormMenuPrincipal.miEstadosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroEstados, True);
end;

procedure TFormMenuPrincipal.miFechamentoClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormFechamentoCaixa);
end;

procedure TFormMenuPrincipal.miFecharAcumuladoClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormFechamentoAcumulados);
end;

procedure TFormMenuPrincipal.miFormacaoCustoPrecoClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormFormacaoCustoPreco);
end;

procedure TFormMenuPrincipal.miFornecedoresClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroFornecedores);
end;


procedure TFormMenuPrincipal.miFuncionariosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroFuncionarios);
end;

procedure TFormMenuPrincipal.miFuncoesClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroCargosFuncionarios);
end;

procedure TFormMenuPrincipal.miGerarArquivosRemessaBoletosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormGerarArquivoRemessaBoleto);
end;

procedure TFormMenuPrincipal.miGerarEntregasClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormGerarEntregas);
end;

procedure TFormMenuPrincipal.miGerarRetiradaClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormGerarRetiradas);
end;

procedure TFormMenuPrincipal.miGerenciadoresCartoesTEFClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroGerenciadoresCartoesTEF);
end;

procedure TFormMenuPrincipal.miGrupoPrecificacaoEntreLojasClick(
  Sender: TObject);
begin
  Sessao.AbrirTela(TFormGrupoPrecificacao);
end;

procedure TFormMenuPrincipal.miGruposClientesClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroGruposClientes);
end;

procedure TFormMenuPrincipal.miGruposdeplanosfinanceirosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormGruposPlanosFinanceiros);
end;

procedure TFormMenuPrincipal.miGruposFornecedoresClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroGruposFornecedores);
end;

procedure TFormMenuPrincipal.miGruposTributacoesCompraEstadualClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormGruposTributacoesEstadualCompra);
end;

procedure TFormMenuPrincipal.miGruposTributacoesCompraFederalClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormGruposTributacoesFederalCompra);
end;

procedure TFormMenuPrincipal.miGruposTributacoesVendaEstadualClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormGruposTributacoesEstadualVenda);
end;

procedure TFormMenuPrincipal.miGruposTributacoesVendaFederalClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormGruposTributacoesFederalVenda);
end;

procedure TFormMenuPrincipal.miInclusaoTitulosPagarClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormInclusaoTitulosPagar);
end;

procedure TFormMenuPrincipal.miInclusaoTitulosReceberClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormInclusaoTitulosReceber);
end;

procedure TFormMenuPrincipal.miIndicesdedescontosdevenda1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroIndicesDescontosVenda, True);
end;

procedure TFormMenuPrincipal.miIPdoServidorClick(Sender: TObject);
begin
  _Biblioteca.Informar( Sessao.getConexaoBanco.getIPServidor );
end;

procedure TFormMenuPrincipal.miLancamentoManualCaixaBancoClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormLancamentoManualCaixaBanco);
end;

procedure TFormMenuPrincipal.miLimiteCreditoClienteClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoLimiteCreditoClientes);
end;

procedure TFormMenuPrincipal.miListaContagemClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormListaContagemAvancada);
end;

procedure TFormMenuPrincipal.miListaNegraClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroListaNegra);
end;

procedure TFormMenuPrincipal.miLocaisProdutosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroLocaisProdutos);
end;

procedure TFormMenuPrincipal.miOutrasOperacoesClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormOutrasOperacoes);
end;

procedure TFormMenuPrincipal.miMarcasClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroMarcas);
end;

procedure TFormMenuPrincipal.miMarketPlaceClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoMarketPlace);
end;

procedure TFormMenuPrincipal.miMensagensClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCentralMensagensInternas);
end;

procedure TFormMenuPrincipal.miMotivosAjusteEstoqueClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroMotivosAjusteEstoque);
end;

procedure TFormMenuPrincipal.miMotoristasClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroMotorista);
end;

procedure TFormMenuPrincipal.miMovimentacoesContasClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoMovimentacoesBancosCaixas);
end;

procedure TFormMenuPrincipal.miMovimentosPendentesEmissaoNotaFiscalTransferenciaClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormMovimentosPendentesEmissaoNotaFiscalTransferencia);
end;

procedure TFormMenuPrincipal.miConsultaNotasFiscaisClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoNotasFiscais);
end;

procedure TFormMenuPrincipal.miConsultaPedidosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormConsultaPedidos);
end;

procedure TFormMenuPrincipal.miControleEntregasClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormControleEntregas);
end;

procedure TFormMenuPrincipal.miControleGerencialClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormControleGerencial);
end;

procedure TFormMenuPrincipal.miCreditosPagarClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroCreditosManuaisPagar);
end;

procedure TFormMenuPrincipal.miCreditosReceberClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroCreditoManualReceber);
end;

procedure TFormMenuPrincipal.miCurvaABCClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoCurvaABC);
end;

procedure TFormMenuPrincipal.miOrcamentosVendasClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormOrcamentosVendas, False, 5);
end;

procedure TFormMenuPrincipal.miOrdenaodelocaisparaentregaporproduto1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormOrdenacaoLocaisEntregaProduto);
end;

procedure TFormMenuPrincipal.miOutrasnotas1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormOutrasNotas);
end;


procedure TFormMenuPrincipal.miOutrasOperacoes12Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormOutrasOperacoes);
end;


procedure TFormMenuPrincipal.miParametrosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormParametros, True);
end;

procedure TFormMenuPrincipal.miParametrosEmpresaClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormParametrosEmpresa, True);
end;

procedure TFormMenuPrincipal.miPDVClick(Sender: TObject);
var
  vPDV: TFormPDV;
begin
  if not Sessao.AutorizadoTela('tformpdv') then begin
    Exclamar('Voc� n�o possui permiss�es necess�rias para acessar esta tela!');
    Exit;
  end;

  if Sessao.getTurnoCaixaAberto = nil then begin
    Exclamar('N�o existe um turno em aberto para o usu�rio logado, por favor, fa�a a abertura de turno antes de realizar est� opera��o!');
    Exit;
  end;

  if Sessao.getParametrosEmpresa.CondicaoPagamentoPDV = 0 then begin
    Exclamar('N�o foi parametrizado a condi��o de pagamento da venda r�pida, verifique!');
    Exit;
  end;

  vPDV := TFormPDV.Create(Application);
  vPDV.ShowModal;
  vPDV.Free;
end;

procedure TFormMenuPrincipal.miPlanosFinanceirosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroPlanosFinanceiros);
end;

procedure TFormMenuPrincipal.miPortadoresClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroPortadores);
end;

procedure TFormMenuPrincipal.miRelacaoPlanosfinanceirosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoPlanosFinanceiros);
end;

procedure TFormMenuPrincipal.miRelacaoPreEntradasClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormPreEntradasDocumentosFiscais);
end;

procedure TFormMenuPrincipal.miProdutosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroProdutos);
end;

procedure TFormMenuPrincipal.miProdutosPaiFilhoClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoProdutosPaiFilho);
end;

procedure TFormMenuPrincipal.miProdutosRelacionadosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroProdutosRelacionados);
end;

procedure TFormMenuPrincipal.miProdutosSemVenderClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoProdutosSemVender);
end;

procedure TFormMenuPrincipal.miProfissionaisClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroProfissionais);
end;

procedure TFormMenuPrincipal.miPromocoesClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormDefinirPromocoes);
end;

procedure TFormMenuPrincipal.miReceberPedidosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRecebimentoPedidos);
end;

procedure TFormMenuPrincipal.miRelacaoConhecimentosFreteClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoConhecimentosFretes);
end;

procedure TFormMenuPrincipal.miRelacaoContasPagarBaixadosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoContasPagarBaixas);
end;

procedure TFormMenuPrincipal.miRelacaoContasReceberBaixadosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoContasReceberBaixas);
end;

procedure TFormMenuPrincipal.miRelacaoContasReceberClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoContasReceber);
end;

procedure TFormMenuPrincipal.miRelacaoControlesEntregasClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoControlesEntregas);
end;

procedure TFormMenuPrincipal.miRelacaoDevolucoesClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoDevolucoesVendas);
end;

procedure TFormMenuPrincipal.miRelacaoEntradasClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoEntradaNotasFiscais);
end;

procedure TFormMenuPrincipal.miRelacaoOrcamentosVendasClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoOrcamentosVendas);
end;

procedure TFormMenuPrincipal.miRelacaoProdutosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoProdutos);
end;

procedure TFormMenuPrincipal.miRelacaoOrcamentosBloqueiosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoBloqueiosOrcamentos);
end;

procedure TFormMenuPrincipal.miRelaodecontasapagar1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoContasPagar);
end;

procedure TFormMenuPrincipal.miRetornoRemessaBoletosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRetornoRemessaBoletos);
end;

procedure TFormMenuPrincipal.miRotasClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroRotas);
end;

procedure TFormMenuPrincipal.miSangriaClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormSangriaCaixa);
end;

procedure TFormMenuPrincipal.miValidadorNFeClick(Sender: TObject);
begin
  inherited;
  ShellExecute(handle, 'open', PChar('https://www.sefaz.rs.gov.br/nfe/NFE-VAL.aspx'), '', '', SW_SHOWNORMAL);
end;

procedure TFormMenuPrincipal.miConsultaNFeCompletaClick(Sender: TObject);
begin
  inherited;
  ShellExecute(
    handle,
    'open',
    PChar('http://www.nfe.fazenda.gov.br/portal/consultaRecaptcha.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8='),
    '',
    '',
    SW_SHOWNORMAL
  );
end;

procedure TFormMenuPrincipal.miConsultaInutilizacoesClick(Sender: TObject);
begin
  inherited;
  ShellExecute(
    handle,
    'open',
    PChar('http://www.nfe.fazenda.gov.br/portal/consulta.aspx?tipoConsulta=inutilizacao&tipoConteudo=YG1QjUXR4PY='),
    '',
    '',
    SW_SHOWNORMAL
  );
end;

procedure TFormMenuPrincipal.miSintegraClick(Sender: TObject);
begin
  inherited;
  ShellExecute(
    handle,
    'open',
    PChar('https://dfe-portal.svrs.rs.gov.br/nfe/ccc'),
    '',
    '',
    SW_SHOWNORMAL
  );
end;

procedure TFormMenuPrincipal.miFsistClick(Sender: TObject);
begin
  inherited;
  ShellExecute(
    handle,
    'open',
    PChar('https://www.fsist.com.br/'),
    '',
    '',
    SW_SHOWNORMAL
  );
end;

procedure TFormMenuPrincipal.miSeriesNotasFiscaisClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastrosSeriesNotasFiscais);
end;

procedure TFormMenuPrincipal.miSugestaoComprasClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormSugestaoCompras);
end;

procedure TFormMenuPrincipal.miSuporteRemotoClick(Sender: TObject);
var
  vRecursos: TResourceStream;
begin
  if not FileExists('Anydesk.exe') then begin
    vRecursos := TResourceStream.Create(Hinstance, 'util', 'Any');
    try
      vRecursos.SavetoFile('Anydesk.exe');
    finally
      vRecursos.Free;
    end;
  end;

  WinExec('Anydesk.exe', SW_SHOW);
end;

procedure TFormMenuPrincipal.miSuprimentoClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormSuprimentoCaixa);
end;

procedure TFormMenuPrincipal.NotificacaoEnvioNFe(pNotificacao: string);
begin
  if FNotificacoes = nil then begin
    FNotificacoes := TFormNotificacoes.Create(Self);
    FNotificacoes.SetCorNotificacao(clBlue);
    FNotificacoes.Show;
  end;

  FNotificacoes.Notificar(pNotificacao);

  if Em(pNotificacao, ['NFe enviada com sucesso...', 'Finalizar...']) then
    FreeAndNil(FNotificacoes);
end;

procedure TFormMenuPrincipal.Nvel1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormEnderecoEstoqueNivel);
end;

procedure TFormMenuPrincipal.Parmetrosdecomisso1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormParametrosComissao);
end;

procedure TFormMenuPrincipal.miParametrosDiasDevolucaoClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormParametroDevolucaoAposPrazo);
end;

procedure TFormMenuPrincipal.Percentuaisdecustosdaempresa1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormPercentuaisCustoEmpresa);
end;

procedure TFormMenuPrincipal.Preos1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormDefinirPrecosVenda);
end;

procedure TFormMenuPrincipal.Produesdeestoque1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoProducoesEstoque);
end;

procedure TFormMenuPrincipal.Produtopaifilho1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormCadastroProdutoPai);
end;

procedure TFormMenuPrincipal.Relaodecomissesporparceiros1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoComissoesPorParceiros);
end;

procedure TFormMenuPrincipal.Relaodeocorrncias1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoCRMOcorrencias);
end;

procedure TFormMenuPrincipal.Rua1Click(Sender: TObject);
begin
  Sessao.AbrirTela(TFormEnderecoEstoqueRua);
end;

procedure TFormMenuPrincipal.miRelacaoTransferenciaEmpresasClick(
  Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoTransferenciasEmpresas);
end;

procedure TFormMenuPrincipal.miRelacaoTurnosCaixaClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoTurnosCaixa);
end;

procedure TFormMenuPrincipal.miRelacaoVendasProdutosClick(Sender: TObject);
begin
  Sessao.AbrirTela(TFormRelacaoVendasProdutos);
end;

end.
