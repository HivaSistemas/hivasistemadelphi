inherited FormPesquisaBancosCaixas: TFormPesquisaBancosCaixas
  Caption = 'Pesquisa de bancos/caixas'
  ClientWidth = 604
  ExplicitWidth = 612
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 604
    ColCount = 7
    OnDrawCell = sgPesquisaDrawCell
    CorLinhaFoco = 38619
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Nome'
      'Tipo'
      'Agencia'
      'Conta'
      'Ativo')
    OnGetCellColor = sgPesquisaGetCellColor
    RealColCount = 7
    ExplicitWidth = 604
    ColWidths = (
      28
      55
      199
      64
      64
      87
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Top = 224
    Text = '0'
    ExplicitTop = 224
  end
  inherited pnFiltro1: TPanel
    Width = 604
    ExplicitWidth = 604
    inherited lblPesquisa: TLabel
      Left = 205
      Width = 34
      Caption = 'Conta:'
      ExplicitLeft = 205
      ExplicitWidth = 34
    end
    inherited eValorPesquisa: TEditLuka
      Left = 205
      Width = 380
      ExplicitLeft = 205
      ExplicitWidth = 380
    end
  end
end
