inherited FormVidaProduto: TFormVidaProduto
  ActiveControl = FrEmpresa
  Caption = 'Hist'#243'rico do produto'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  inherited pcDados: TPageControl
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      inline FrProduto: TFrProdutos
        Left = 7
        Top = 53
        Width = 331
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 7
        ExplicitTop = 53
        ExplicitWidth = 331
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 306
          Height = 23
          ExplicitWidth = 306
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 331
          ExplicitWidth = 331
          inherited lbNomePesquisa: TLabel
            Width = 42
            Caption = 'Produto'
            ExplicitWidth = 42
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 226
            ExplicitLeft = 226
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 306
          Height = 24
          ExplicitLeft = 306
          ExplicitHeight = 24
        end
      end
      inline FrDataMovimentoInicialFinal: TFrDataInicialFinal
        Left = 366
        Top = 0
        Width = 201
        Height = 63
        AutoScroll = True
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 366
        ExplicitWidth = 201
        ExplicitHeight = 63
        inherited Label1: TLabel
          Width = 107
          Height = 14
          Caption = 'Data de movimento'
          ExplicitWidth = 107
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrEmpresa: TFrEmpresas
        Left = 7
        Top = 3
        Width = 331
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 7
        ExplicitTop = 3
        ExplicitWidth = 331
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 306
          Height = 23
          ExplicitWidth = 306
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 331
          ExplicitWidth = 331
          inherited lbNomePesquisa: TLabel
            Width = 47
            Caption = 'Empresa'
            ExplicitWidth = 47
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 226
            ExplicitLeft = 226
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 306
          Height = 24
          ExplicitLeft = 306
          ExplicitHeight = 24
        end
      end
      object rgTipoEstoque: TRadioGroupLuka
        Left = 366
        Top = 45
        Width = 187
        Height = 76
        Caption = ' Estoque '
        Items.Strings = (
          'Dispon'#237'vel'
          'F'#237'sico'
          'Real')
        TabOrder = 3
        Valores.Strings = (
          'D'
          'F'
          'I')
      end
      object st1: TStaticText
        Left = 7
        Top = 105
        Width = 110
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Estoque Dispon'#237'vel'
        Color = 38619
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 4
        Transparent = False
      end
      object st2: TStaticText
        Left = 120
        Top = 105
        Width = 110
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Estoque F'#237'sico'
        Color = 38619
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        Transparent = False
      end
      object st4: TStaticText
        Left = 233
        Top = 105
        Width = 110
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Estoque Real'
        Color = 38619
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 6
        Transparent = False
      end
      object stEstoqueDisponivel: TStaticTextLuka
        Left = 7
        Top = 120
        Width = 110
        Height = 19
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = '0,000'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 7
        Transparent = False
        AsInt = 0
        TipoCampo = tcNumerico
        CasasDecimais = 3
      end
      object stEstoqueFisico: TStaticTextLuka
        Left = 120
        Top = 120
        Width = 110
        Height = 19
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = '0,000'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 8
        Transparent = False
        AsInt = 0
        TipoCampo = tcNumerico
        CasasDecimais = 3
      end
      object stEstoqueContabil: TStaticTextLuka
        Left = 233
        Top = 120
        Width = 110
        Height = 19
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = '0,000'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 9
        Transparent = False
        AsInt = 0
        TipoCampo = tcNumerico
        CasasDecimais = 3
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object spl1: TSplitter
        Left = 0
        Top = 352
        Width = 884
        Height = 6
        Cursor = crVSplit
        Align = alTop
        ResizeStyle = rsUpdate
      end
      object sgMovimentacoes: TGridLuka
        Left = 0
        Top = 0
        Width = 884
        Height = 352
        Align = alTop
        ColCount = 8
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        ParentFont = False
        TabOrder = 0
        OnDblClick = sgMovimentacoesDblClick
        OnDrawCell = sgMovimentacoesDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Data/Hora movimento'
          'Tipo movimento'
          'N'#186' Movimento'
          'Cliente/Fornecedor/Usuario'
          'N'#186' Documento'
          'Entrada'
          'Sa'#237'da'
          'Saldo')
        OnGetCellColor = sgMovimentacoesGetCellColor
        Grid3D = False
        RealColCount = 11
        AtivarPopUpSelecao = False
        ColWidths = (
          115
          124
          71
          221
          72
          77
          81
          80)
      end
      object sgResumo: TGridLuka
        Left = 0
        Top = 358
        Width = 884
        Height = 160
        Align = alClient
        ColCount = 4
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        ParentFont = False
        TabOrder = 1
        OnDrawCell = sgResumoDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Tipo movimento'
          'Entrada'
          'Sa'#237'da'
          'Saldo')
        OnGetCellColor = sgResumoGetCellColor
        Grid3D = False
        RealColCount = 4
        AtivarPopUpSelecao = False
        ColWidths = (
          130
          77
          86
          83)
      end
    end
  end
  object dsVidaProduto: TfrxUserDataSet
    RangeBegin = rbCurrent
    RangeEnd = reCurrent
    UserName = 'dsVidaProduto'
    OnCheckEOF = dsVidaProdutoCheckEOF
    OnFirst = dsVidaProdutoFirst
    OnNext = dsVidaProdutoNext
    OnPrior = dsVidaProdutoPrior
    Fields.Strings = (
      'DataHoraMovimento'
      'TipoMovimento'
      'NrMovimento'
      'ClienteFornecedor'
      'NrDocumento'
      'Entada'
      'Saida'
      'Saldo')
    OnGetValue = dsVidaProdutoGetValue
    Left = 736
    Top = 120
  end
  object frxReport: TfrxReport
    Version = '5.1.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44043.704685613400000000
    ReportOptions.LastChange = 45354.809220543980000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 736
    Top = 64
    Datasets = <
      item
        DataSet = dsVidaProduto
        DataSetName = 'dsVidaProduto'
      end
      item
        DataSet = dmRelatorio.dstEmpresa
        DataSetName = 'frxdstEmpresa'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 136.063080000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000000000
          Width = 68.031540000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 26.456710000000000000
          Top = 34.015770000000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 15.118120000000000000
          Top = 49.133890000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 340.157700000000000000
          Top = 3.779530000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 321.260050000000000000
          Top = 34.015770000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade/UF:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 340.157700000000000000
          Top = 49.133890000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 82.708720000000000000
          Top = 3.779530000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 383.512060000000000000
          Top = 3.779530000000000000
          Width = 200.315090000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."CNPJ"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 84.929190000000000000
          Top = 18.897650000000000000
          Width = 551.811380000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            
              '[frxdstEmpresa."LOGRADOURO"] [frxdstEmpresa."COMPLEMENTO"] n'#176'[fr' +
              'xdstEmpresa."NUMERO"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 82.708720000000000000
          Top = 34.015770000000000000
          Width = 207.874150000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_BAIRRO"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 383.512060000000000000
          Top = 34.015770000000000000
          Width = 185.196970000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_CIDADE"] / [frxdstEmpresa."ESTADO_ID"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 82.708720000000000000
          Top = 49.133890000000000000
          Width = 75.590600000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 383.512060000000000000
          Top = 49.133890000000000000
          Width = 260.787570000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."E_MAIL"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 545.252320000000000000
          Top = 3.779530000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Emitido em [Date] '#224's [Time]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 545.252320000000000000
          Top = 18.897650000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pag. [Page] de [TotalPages#]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Shape3: TfrxShapeView
          Top = 109.606370000000000000
          Width = 718.110700000000000000
          Height = 22.677180000000000000
          Fill.BackColor = cl3DLight
        end
        object Memo21: TfrxMemoView
          Top = 112.606370000000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Dt Mov.')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 82.708720000000000000
          Top = 112.606370000000000000
          Width = 86.929190000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Tipo Mov.')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 221.992270000000000000
          Top = 112.606370000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Nro Mov.')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 274.905690000000000000
          Top = 112.606370000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Cliente/Fornecedor/Usu'#225'rio')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 463.882190000000000000
          Top = 112.606370000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Nro Doc.')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 528.134200000000000000
          Top = 112.606370000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Entrada')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 588.606680000000000000
          Top = 112.606370000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Saida')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 683.094930000000000000
          Top = 112.606370000000000000
          Width = 34.015770000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          Top = 75.590600000000000000
          Width = 718.110700000000000000
          Height = 22.677180000000000000
          Fill.BackColor = cl3DLight
        end
        object Memo5: TfrxMemoView
          Top = 79.370130000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Produto: ')
          ParentFont = False
        end
        object mmProduto: TfrxMemoView
          Left = 52.913420000000000000
          Top = 79.370130000000000000
          Width = 396.850650000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '10001 - Abra'#231'adeira 1.x2')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 453.543600000000000000
          Top = 79.370130000000000000
          Width = 105.826840000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Data do movimento:')
          ParentFont = False
        end
        object mmDataInicio: TfrxMemoView
          Left = 563.149970000000000000
          Top = 79.370130000000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '01/01/2023')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 627.401980000000000000
          Top = 79.370130000000000000
          Width = 22.677180000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'at'#233)
          ParentFont = False
        end
        object mmDataFim: TfrxMemoView
          Left = 653.858690000000000000
          Top = 79.370130000000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '01/01/2024')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 343.937230000000000000
        Width = 718.110700000000000000
        object Shape2: TfrxShapeView
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 20.897650000000000000
        Top = 215.433210000000000000
        Width = 718.110700000000000000
        DataSet = dsVidaProduto
        DataSetName = 'dsVidaProduto'
        RowCount = 0
        object dsVidaProdutoDataHoraMovimento: TfrxMemoView
          Top = 3.779530000000000000
          Width = 86.929190000000000000
          Height = 15.118120000000000000
          DataField = 'DataHoraMovimento'
          DataSet = dsVidaProduto
          DataSetName = 'dsVidaProduto'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          Memo.UTF8W = (
            '[dsVidaProduto."DataHoraMovimento"]')
          ParentFont = False
        end
        object dsVidaProdutoTipoMovimento: TfrxMemoView
          Left = 82.708720000000000000
          Top = 3.779530000000000000
          Width = 139.842610000000000000
          Height = 15.118120000000000000
          DataField = 'TipoMovimento'
          DataSet = dsVidaProduto
          DataSetName = 'dsVidaProduto'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          Memo.UTF8W = (
            '[dsVidaProduto."TipoMovimento"]')
          ParentFont = False
        end
        object dsVidaProdutoNrDocumento: TfrxMemoView
          Left = 221.992270000000000000
          Top = 3.779530000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          DataField = 'NrDocumento'
          DataSet = dsVidaProduto
          DataSetName = 'dsVidaProduto'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          Memo.UTF8W = (
            '[dsVidaProduto."NrDocumento"]')
          ParentFont = False
        end
        object dsVidaProdutoClienteFornecedor: TfrxMemoView
          Left = 274.905690000000000000
          Top = 3.779530000000000000
          Width = 211.653680000000000000
          Height = 15.118120000000000000
          DataField = 'ClienteFornecedor'
          DataSet = dsVidaProduto
          DataSetName = 'dsVidaProduto'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          Memo.UTF8W = (
            '[dsVidaProduto."ClienteFornecedor"]')
          ParentFont = False
        end
        object dsVidaProdutoNrDocumento1: TfrxMemoView
          Left = 463.882190000000000000
          Top = 3.779530000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          DataField = 'NrDocumento'
          DataSet = dsVidaProduto
          DataSetName = 'dsVidaProduto'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          Memo.UTF8W = (
            '[dsVidaProduto."NrDocumento"]')
          ParentFont = False
        end
        object dsVidaProdutoEntada: TfrxMemoView
          Left = 505.457020000000000000
          Top = 3.779530000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          DataField = 'Entada'
          DataSet = dsVidaProduto
          DataSetName = 'dsVidaProduto'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[dsVidaProduto."Entada"]')
          ParentFont = False
        end
        object dsVidaProdutoSaida: TfrxMemoView
          Left = 577.268090000000000000
          Top = 3.779530000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          DataField = 'Saida'
          DataSet = dsVidaProduto
          DataSetName = 'dsVidaProduto'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[dsVidaProduto."Saida"]')
          ParentFont = False
        end
        object dsVidaProdutoSaldo: TfrxMemoView
          Left = 652.858690000000000000
          Top = 3.779530000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          DataField = 'Saldo'
          DataSet = dsVidaProduto
          DataSetName = 'dsVidaProduto'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[dsVidaProduto."Saldo"]')
          ParentFont = False
        end
      end
      object Memo15: TfrxMemoView
        Left = 370.393940000000000000
        Top = -49.133890000000000000
        Width = 75.590600000000000000
        Height = 11.338590000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
        ParentFont = False
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 298.582870000000000000
        Width = 718.110700000000000000
        object Line1: TfrxLineView
          Width = 714.331170000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
    end
  end
end
