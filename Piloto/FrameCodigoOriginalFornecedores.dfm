inherited FrCodigoOriginalFornecedores: TFrCodigoOriginalFornecedores
  Width = 364
  Height = 258
  ExplicitWidth = 364
  ExplicitHeight = 258
  inherited sgValores: TGridLuka
    Width = 364
    Height = 241
    OnDrawCell = sgValoresDrawCell
    HCol.Strings = (
      'Fabricante'
      'C'#243'digo da ind'#250'stria')
    ExplicitWidth = 364
    ExplicitHeight = 241
    ColWidths = (
      243
      101)
  end
  inherited StaticTextLuka1: TStaticTextLuka
    Width = 364
    Caption = 'C'#243'digo da ind'#250'stria'
    ExplicitWidth = 364
  end
  inherited pmOpcoes: TPopupMenu
    Left = 24
    Top = 200
    inherited miN1: TMenuItem
      Visible = False
    end
    inherited miSubir: TMenuItem
      Visible = False
    end
    inherited miDescer: TMenuItem
      Visible = False
    end
  end
end
