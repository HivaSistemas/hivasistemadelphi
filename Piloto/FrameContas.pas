unit FrameContas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _RecordsCadastros,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Contas, _Sessao, PesquisaBancosCaixas, System.Math,
  System.StrUtils, Vcl.Buttons, RadioGroupLuka, _Biblioteca, Vcl.Menus;

type
  TFrContas = class(TFrameHenrancaPesquisas)
    rgTipo: TRadioGroupLuka;
    ckAutorizados: TCheckBox;
  public
    function GetConta(pLinha: Integer = -1): RecContas;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrameHenrancaPesquisas1 }

function TFrContas.AdicionarDireto: TObject;
var
  vContas: TArray<RecContas>;
  vFuncionarioId: Integer;
begin
  vFuncionarioId := 0;
  if ckAutorizados.Checked and not Sessao.getUsuarioLogado.GerenteSistema then
    vFuncionarioId := Sessao.getUsuarioLogado.funcionario_id;

  vContas := _Contas.BuscarContas(Sessao.getConexaoBanco, 0, [FChaveDigitada], rgTipo.GetValor, ckSomenteAtivos.Checked, vFuncionarioId);
  if vContas = nil then
    Result := nil
  else
    Result := vContas[0];
end;

function TFrContas.AdicionarPesquisando: TObject;
var
  vFuncionarioAutotizadoId: Integer;
begin
  vFuncionarioAutotizadoId := 0;
  if ckAutorizados.Checked and not Sessao.getUsuarioLogado.GerenteSistema then
    vFuncionarioAutotizadoId := Sessao.getUsuarioLogado.funcionario_id;

  Result :=
    PesquisaBancosCaixas.PesquisarBancoCaixa(
      rgTipo.GetValor,
      ckSomenteAtivos.Checked,
      vFuncionarioAutotizadoId
    );
end;

function TFrContas.GetConta(pLinha: Integer): RecContas;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecContas(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrContas.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecContas(FDados[i]).Conta = RecContas(pSender).Conta then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrContas.MontarGrid;
var
  i: Integer;
  pSender: RecContas;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      pSender := RecContas(FDados[i]);
      AAdd([pSender.Conta, IfThen(pSender.Tipo = 1, pSender.CodigoBanco + ' ') + pSender.Nome]);
    end;
  end;
end;

end.
