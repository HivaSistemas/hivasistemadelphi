unit FrameRotas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Buttons, _Rotas, _Sessao,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Biblioteca, PesquisaRotas,
  Vcl.Menus;

type
  TFrRotas = class(TFrameHenrancaPesquisas)
  public
    function GetRota(pLinha: Integer = -1): RecRotas;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrRotas }

function TFrRotas.AdicionarDireto: TObject;
var
  vDados: TArray<RecRotas>;
begin
  vDados := _Rotas.BuscarRotas(Sessao.getConexaoBanco, 0, [FChaveDigitada], ckSomenteAtivos.Checked);
  if vDados = nil then
    Result := nil
  else
    Result := vDados[0];
end;

function TFrRotas.AdicionarPesquisando: TObject;
begin
  Result := PesquisaRotas.PesquisarRota(ckSomenteAtivos.Checked);
end;

function TFrRotas.GetRota(pLinha: Integer): RecRotas;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecRotas(FDados[IIfInt(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrRotas.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecRotas(FDados[i]).RotaId = RecRotas(pSender).RotaId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrRotas.MontarGrid;
var
  i: Integer;
  vSender: RecRotas;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecRotas(FDados[i]);
      AAdd([IntToStr(vSender.RotaId), vSender.nome]);
    end;
  end;
end;

end.
