inherited FormAgendarItensSemPrevisao: TFormAgendarItensSemPrevisao
  Caption = 'Agendamento de itens sem previs'#227'o'
  ClientHeight = 397
  ClientWidth = 774
  ExplicitWidth = 780
  ExplicitHeight = 426
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label1: TLabel
    Left = 123
    ExplicitLeft = 123
  end
  object lb2: TLabel [1]
    Left = 231
    Top = 4
    Width = 39
    Height = 14
    Caption = 'Cliente'
  end
  object lb12: TLabel [2]
    Left = 461
    Top = 4
    Width = 52
    Height = 14
    Caption = 'Vendedor'
  end
  object lb3: TLabel [3]
    Left = 690
    Top = 4
    Width = 76
    Height = 14
    Caption = 'Data cadastro'
  end
  object sbInfoPedido: TSpeedButton [4]
    Left = 204
    Top = 24
    Width = 17
    Height = 18
    Hint = 'Abrir informa'#231#245'es do cliente'
    Caption = '...'
    Flat = True
    OnClick = sbInfoPedidoClick
  end
  object sbInfoCliente: TSpeedButton [5]
    Left = 435
    Top = 24
    Width = 17
    Height = 18
    Hint = 'Abrir informa'#231#245'es do cliente'
    Caption = '...'
    Flat = True
  end
  object lb4: TLabel [6]
    Left = 122
    Top = 48
    Width = 48
    Height = 14
    Caption = 'Data rec.'
  end
  object lb1: TLabel [7]
    Left = 323
    Top = 48
    Width = 69
    Height = 14
    Caption = 'Observa'#231#245'es'
  end
  object Label2: TLabel [8]
    Left = 207
    Top = 48
    Width = 107
    Height = 14
    Caption = 'Data / hora entrega'
  end
  inherited pnOpcoes: TPanel
    Height = 397
    ExplicitHeight = 397
    inherited sbDesfazer: TSpeedButton
      Top = 96
      ExplicitTop = 96
    end
    inherited sbExcluir: TSpeedButton
      Top = 204
      Visible = False
      ExplicitTop = 204
    end
    inherited sbPesquisar: TSpeedButton
      Top = 45
      ExplicitTop = 45
    end
    inherited sbLogs: TSpeedButton
      Top = 350
      ExplicitTop = 350
    end
  end
  inherited eID: TEditLuka
    Left = 123
    ExplicitLeft = 123
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 690
    Top = 39
    Enabled = False
    TabOrder = 10
    Visible = False
    ExplicitLeft = 690
    ExplicitTop = 39
  end
  object eCliente: TEditLuka
    Left = 231
    Top = 19
    Width = 201
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 2
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eVendedor: TEditLuka
    Left = 461
    Top = 19
    Width = 223
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 3
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eDataCadastro: TEditLukaData
    Left = 690
    Top = 19
    Width = 79
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1; '
    MaxLength = 10
    ReadOnly = True
    TabOrder = 4
    Text = '  /  /    '
  end
  object eDataRecebimento: TEditLukaData
    Left = 123
    Top = 62
    Width = 80
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1; '
    MaxLength = 10
    ReadOnly = True
    TabOrder = 5
    Text = '  /  /    '
  end
  object eObservacoes: TEditLuka
    Left = 323
    Top = 62
    Width = 446
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 200
    ReadOnly = True
    TabOrder = 8
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object sgProdutos: TGridLuka
    Left = 123
    Top = 87
    Width = 646
    Height = 152
    Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
    ColCount = 7
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
    PopupMenu = pmItens
    TabOrder = 9
    OnClick = sgProdutosClick
    OnDrawCell = sgProdutosDrawCell
    OnEnter = sgProdutosEnter
    OnKeyPress = NumerosVirgula
    OnSelectCell = sgProdutosSelectCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Produto'
      'Nome'
      'Marca'
      'Und.'
      'Saldo'
      'Qtde. agendar'
      'Lotes def.?')
    OnGetCellColor = sgProdutosGetCellColor
    OnGetCellPicture = sgProdutosGetCellPicture
    OnArrumarGrid = sgProdutosArrumarGrid
    Grid3D = False
    RealColCount = 7
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      54
      203
      99
      28
      81
      82
      71)
  end
  object eDataEntrega: TEditLukaData
    Left = 207
    Top = 62
    Width = 72
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 6
    Text = '  /  /    '
    OnKeyDown = ProximoCampo
  end
  object eHoraEntrega: TEditHoras
    Left = 278
    Top = 62
    Width = 41
    Height = 22
    EditMask = '!90:00;1;_'
    MaxLength = 5
    TabOrder = 7
    Text = '  :  '
    OnKeyDown = ProximoCampo
  end
  object sgLocais: TGridLuka
    Left = 122
    Top = 239
    Width = 647
    Height = 129
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    ColCount = 6
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goEditing]
    TabOrder = 11
    OnDrawCell = sgLocaisDrawCell
    OnKeyPress = NumerosVirgula
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Empresa'
      'Nome da Empresa'
      'Local'
      'Nome do local'
      'Dispon'#237'vel'
      'Qtde. entregar')
    OnArrumarGrid = sgLocaisArrumarGrid
    Grid3D = False
    RealColCount = 11
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      54
      221
      36
      159
      64
      84)
  end
  object miDefinirLotes: TPanel
    Left = 640
    Top = 372
    Width = 129
    Height = 22
    Cursor = crHandPoint
    Hint = 'Definir lote do produto focado'
    BevelOuter = bvNone
    Caption = 'Definir lotes'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 12
    OnClick = miDefinirLotesClick
  end
  object pmItens: TPopupMenu
    Left = 687
    Top = 172
    object miDefinirQuantidadeTodosItens: TMenuItem
      Caption = 'Definir quantidade em todos os itens'
      ShortCut = 117
      OnClick = miDefinirQuantidadeTodosItensClick
    end
    object miRemoverQuantidadeTodosItensF8: TMenuItem
      Caption = 'Remover quantidade em todos os itens'
      ShortCut = 119
      OnClick = miRemoverQuantidadeTodosItensF8Click
    end
    object miN1: TMenuItem
      Caption = '-'
    end
  end
end
