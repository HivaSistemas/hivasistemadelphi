inherited FormImpressorasEstacao: TFormImpressorasEstacao
  Caption = 'Impressoras por esta'#231#227'o'
  ClientHeight = 384
  ClientWidth = 890
  OnShow = FormShow
  ExplicitTop = -4
  ExplicitWidth = 896
  ExplicitHeight = 413
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 384
    ExplicitHeight = 384
    inherited sbPesquisar: TSpeedButton
      Visible = False
    end
    inherited sbLogs: TSpeedButton
      Top = 80
      Height = 36
      ExplicitTop = 80
      ExplicitHeight = 36
    end
  end
  object eEstacao: TEditLuka [1]
    Left = 125
    Top = 13
    Width = 356
    Height = 22
    CharCase = ecUpperCase
    Enabled = False
    ReadOnly = True
    TabOrder = 1
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  inherited pcMaster: TPageControl
    Top = 41
    TabOrder = 2
    ExplicitTop = 41
    inherited tsImpressora: TTabSheet
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      inherited cbTipoImpressoraComproEntrega: TComboBoxLuka
        Items.Strings = (
          'Gr'#225'fica'
          'N'#227'o fiscal'
          'Matricial')
        Valores.Strings = (
          'G'
          'N'
          'M')
      end
    end
    inherited tsBalanca: TTabSheet
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
  end
end
