unit BuscarDadosUnidadeProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameUnidades, Vcl.Buttons,
  Vcl.ExtCtrls, _RecordsCadastros, _Biblioteca;

type
  TFormBuscarDadosUnidadeProduto = class(TFormHerancaFinalizar)
    FrUnidadeVenda: TFrUnidades;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function BuscarDadosUnidadeProdutos: TRetornoTelaFinalizar<RecUnidades>;

implementation

{$R *.dfm}

{ BuscarDadosUnidadeProduto }

function BuscarDadosUnidadeProdutos: TRetornoTelaFinalizar<RecUnidades>;
var
  vForm: TFormBuscarDadosUnidadeProduto;
begin
  vForm := TFormBuscarDadosUnidadeProduto.Create(Application);

  if Result.Ok(vForm.ShowModal) then
    Result.Dados := vForm.FrUnidadeVenda.getUnidade(0);

//  FreeAndNil(vForm);
end;

end.
