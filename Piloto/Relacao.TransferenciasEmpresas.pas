unit Relacao.TransferenciasEmpresas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _Biblioteca, _Sessao,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, FrameDataInicialFinal,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameEmpresas,
  FrameFuncionarios, Frame.Inteiros, FrameProdutos, Vcl.StdCtrls,
  StaticTextLuka, Vcl.Grids, GridLuka, RadioGroupLuka, _TransfProdutosEmpresas,
  _TransfProdutosEmpresasItens, CheckBoxLuka, GroupBoxLuka, Informacoes.TransferenciasEmpresas;

type
  TFormRelacaoTransferenciasEmpresas = class(TFormHerancaRelatoriosPageControl)
    FrEmpresasOrigem: TFrEmpresas;
    FrDataCadastro: TFrDataInicialFinal;
    FrCodigoTransferenciaEmpresas: TFrameInteiros;
    FrUsuarioInsercao: TFrFuncionarios;
    FrEmpresasDestino: TFrEmpresas;
    FrProdutos: TFrProdutos;
    FrDataBaixa: TFrDataInicialFinal;
    FrUsuarioBaixa: TFrFuncionarios;
    sgTransferencias: TGridLuka;
    lbTransferencia: TStaticTextLuka;
    spSeparador: TSplitter;
    StaticTextLuka2: TStaticTextLuka;
    sgItens: TGridLuka;
    FrDataCancelamento: TFrDataInicialFinal;
    FrUsuarioCancelamento: TFrFuncionarios;
    gbStatus: TGroupBoxLuka;
    ckAbertos: TCheckBoxLuka;
    ckBaixados: TCheckBoxLuka;
    ckCancelados: TCheckBoxLuka;
    CheckBoxLuka1: TCheckBoxLuka;
    procedure sgTransferenciasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgTransferenciasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgTransferenciasClick(Sender: TObject);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure sgTransferenciasDblClick(Sender: TObject);
  private
    FItens: TArray<RecTransfProdutosEmpresasItens>;
  public
    { Public declarations }

  protected
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}


const
  (* Grid de Transferências *)
  coTransferenciaID    = 0;
  coStatusAnalitico    = 1;
  coEmpresaOrigem      = 2;
  coEmpresaDestino     = 3;
  coMotorista          = 4;
  coValorTransferencia = 5;
  coUsuarioInsercao    = 6;
  coDataHoraCadastro   = 7;
  coUsuarioBaixa       = 8;
  coDataHoraBaixa      = 9;
  coUsuarioCanc        = 10;
  coDataHoraCanc       = 11;
  coStatus             = 12;

  (* Grid de Itens *)
  ciProduto      = 0;
  ciNome         = 1;
  ciMarca        = 2;
  ciLocalOrigem  = 3;
  ciLote         = 4;
  ciPrecUnitario = 5;
  ciQuantidade   = 6;
  ciValorTotal   = 7;

procedure TFormRelacaoTransferenciasEmpresas.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vTransfProdutosEmpresas: TArray<RecTransfProdutosEmpresas>;
  vTransfProdutosEmpresasIds: TArray<Integer>;
begin
  inherited;
  sgTransferencias.ClearGrid();
  sgItens.ClearGrid();

  FItens := nil;
  vTransfProdutosEmpresasIds := nil;

  if not FrCodigoTransferenciaEmpresas.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrCodigoTransferenciaEmpresas.getSqlFiltros('TRA.TRANSFERENCIA_ID'))
  else begin
    if not FrEmpresasOrigem.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrEmpresasOrigem.getSqlFiltros('TRA.EMPRESA_ORIGEM_ID'));

    if not FrEmpresasDestino.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrEmpresasDestino.getSqlFiltros('TRA.EMPRESA_DESTINO_ID'));

    if not FrProdutos.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, 'TRA.TRANSFERENCIA_ID in(select distinct TRANSFERENCIA_ID from TRANSF_PRODUTOS_EMPRESAS_ITENS where ' + FrProdutos.getSqlFiltros('PRODUTO_ID') + ')');

     if not FrUsuarioInsercao.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrUsuarioInsercao.getSqlFiltros('TRA.USUARIO_CADASTRO_ID'));

     if not FrUsuarioBaixa.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrUsuarioBaixa.getSqlFiltros('TRA.USUARIO_BAIXA_ID'));

     if not FrUsuarioCancelamento.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrUsuarioCancelamento.getSqlFiltros('TRA.USUARIO_CANCELAMENTO_ID'));

     if not FrDataCadastro.NenhumaDataValida then
      _Biblioteca.WhereOuAnd(vSql, FrDataCadastro.getSqlFiltros('trunc(TRA.DATA_HORA_CADASTRO)'));

     if not FrDataBaixa.NenhumaDataValida then
      _Biblioteca.WhereOuAnd(vSql, FrDataBaixa.getSqlFiltros('trunc(TRA.DATA_HORA_BAIXA)'));

     if not FrDataCadastro.NenhumaDataValida then
      _Biblioteca.WhereOuAnd(vSql, FrDataCancelamento.getSqlFiltros('trunc(TRA.DATA_HORA_CANCELAMENTO)'));

     _Biblioteca.WhereOuAnd(vSql, gbStatus.GetSql('TRA.STATUS'));
  end;

  vSql := vSql + 'order by TRA.TRANSFERENCIA_ID ';

  vTransfProdutosEmpresas := _TransfProdutosEmpresas.BuscarTransfProdutosEmpresasComando(Sessao.getConexaoBanco, vSql);

  if vTransfProdutosEmpresas = nil then begin
    NenhumRegistro;
    Exit;
  end;

  for i := Low(vTransfProdutosEmpresas) to High(vTransfProdutosEmpresas) do begin

    sgTransferencias.Cells[coTransferenciaID, i + 1]    := NFormatN( vTransfProdutosEmpresas[i].TransferenciaId );
    sgTransferencias.Cells[coEmpresaOrigem, i + 1]      := getInformacao( vTransfProdutosEmpresas[i].EmpresaOrigemId, vTransfProdutosEmpresas[i].NomeEmpresaOrigem);
    sgTransferencias.Cells[coEmpresaDestino, i + 1]     := getInformacao( vTransfProdutosEmpresas[i].EmpresaDestinoId, vTransfProdutosEmpresas[i].NomeEmpresaDestino);
    sgTransferencias.Cells[coMotorista, i + 1]          := getInformacao( vTransfProdutosEmpresas[i].MotoristaId, vTransfProdutosEmpresas[i].NomeMotorista);
    sgTransferencias.Cells[coValorTransferencia, i + 1] := NFormat( vTransfProdutosEmpresas[i].ValorTransferencia );
    sgTransferencias.Cells[coUsuarioInsercao, i + 1]    := getInformacao( vTransfProdutosEmpresas[i].UsuarioCadastroId, vTransfProdutosEmpresas[i].NomeUsuarioCadastro);
    sgTransferencias.Cells[coDataHoraCadastro, i + 1]   := DHFormat( vTransfProdutosEmpresas[i].DataHoraCadastro );
    sgTransferencias.Cells[coUsuarioBaixa, i + 1]       := getInformacao( vTransfProdutosEmpresas[i].UsuarioBaixaId, vTransfProdutosEmpresas[i].NomeUsuarioBaixa);
    sgTransferencias.Cells[coDataHoraBaixa, i + 1]      := DHFormat( vTransfProdutosEmpresas[i].DataHoraBaixa );
    sgTransferencias.Cells[coUsuarioCanc, i + 1]        := getInformacao( vTransfProdutosEmpresas[i].UsuarioCancelamentoId, vTransfProdutosEmpresas[i].NomeUsuarioCancelamento );
    sgTransferencias.Cells[coDataHoraCanc, i + 1]       := DHFormat( vTransfProdutosEmpresas[i].DataHoraCancelamento );

    sgTransferencias.Cells[coStatusAnalitico, i + 1]    := vTransfProdutosEmpresas[i].StatusAnalitico;
    sgTransferencias.Cells[coStatus, i + 1]             := vTransfProdutosEmpresas[i].Status;

    _Biblioteca.AddNoVetorSemRepetir(vTransfProdutosEmpresasIds, vTransfProdutosEmpresas[i].TransferenciaId);
  end;
  sgTransferencias.SetLinhasGridPorTamanhoVetor(Length(vTransfProdutosEmpresas));

  FItens := _TransfProdutosEmpresasItens.BuscarTransfProdutosEmpresasItensComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('ITE.TRANSFERENCIA_ID', vTransfProdutosEmpresasIds) + ' order by ITE.TRANSFERENCIA_ID, PRO.NOME');

  pcDados.ActivePage := tsResultado;
  sgTransferenciasClick(nil);
  SetarFoco(sgTransferencias);
end;

procedure TFormRelacaoTransferenciasEmpresas.FormCreate(Sender: TObject);
begin
  inherited;
  ActiveControl := FrEmpresasOrigem.sgPesquisa;
  FrEmpresasOrigem.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId, False);
end;

procedure TFormRelacaoTransferenciasEmpresas.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    ciProduto,
    ciPrecUnitario,
    ciQuantidade,
    ciValorTotal]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoTransferenciasEmpresas.sgTransferenciasClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vTransferenciaId: Integer;
begin
  inherited;
  vLinha := 0;
  sgItens.ClearGrid();
  vTransferenciaId := SFormatInt(sgTransferencias.Cells[coTransferenciaID, sgTransferencias.Row]);

  for i := Low(FItens) to High(FItens) do begin
    if vTransferenciaId <> FItens[i].TransferenciaId then
      Continue;

    Inc(vLinha);

    sgItens.Cells[ciProduto, vLinha]       := NFormat(FItens[i].ProdutoId);
    sgItens.Cells[ciNome, vLinha]          := FItens[i].NomeProduto;
    sgItens.Cells[ciMarca, vLinha]         := NFormat(FItens[i].MarcaId) + ' - ' + FItens[i].NomeMarca;
    sgItens.Cells[ciLocalOrigem, vLinha]   := NFormat(FItens[i].LocalOrigemId) + ' - ' + FItens[i].NomeLocalOrigem;
    sgItens.Cells[ciLote, vLinha]          := FItens[i].Lote;
    sgItens.Cells[ciPrecUnitario, vLinha]  := NFormat(FItens[i].PrecoUnitario);
    sgItens.Cells[ciQuantidade, vLinha]    := NFormatEstoque(FItens[i].Quantidade);
    sgItens.Cells[ciValorTotal, vLinha]    := NFormat(FItens[i].ValorTotal);
  end;
  sgItens.SetLinhasGridPorTamanhoVetor( vLinha );
end;

procedure TFormRelacaoTransferenciasEmpresas.sgTransferenciasDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.TransferenciasEmpresas.Informar( SFormatInt(sgTransferencias.Cells[coTransferenciaID, sgTransferencias.Row]) );
end;

procedure TFormRelacaoTransferenciasEmpresas.sgTransferenciasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    coTransferenciaID,
    coValorTransferencia]
  then
    vAlinhamento := taRightJustify
  else if ACol = coStatusAnalitico then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgTransferencias.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoTransferenciasEmpresas.sgTransferenciasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coStatusAnalitico then begin
    AFont.Style := [fsBold];
    AFont.Color := _TransfProdutosEmpresas.getCorStatus(sgTransferencias.Cells[coStatus, ARow]);
  end;
end;

end.
