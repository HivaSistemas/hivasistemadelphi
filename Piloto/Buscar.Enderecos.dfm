inherited FormBuscarEnderecos: TFormBuscarEnderecos
  Caption = 'Busca de endere'#231'os'
  ClientHeight = 279
  ClientWidth = 674
  OnShow = FormShow
  ExplicitWidth = 680
  ExplicitHeight = 308
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 242
    Width = 674
    ExplicitTop = 242
    ExplicitWidth = 674
  end
  object sgEnderecos: TGridLuka
    Left = 2
    Top = 2
    Width = 670
    Height = 239
    TabStop = False
    ColCount = 8
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 1
    OnDblClick = sgEnderecosDblClick
    OnDrawCell = sgEnderecosDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Tipo endere'#231'o'
      'Logradouro'
      'Complemento'
      'N'#250'mero'
      'Bairro'
      'Ponto de refer'#234'ncia'
      'Cep'
      'Inscri'#231#227'o estadual')
    Grid3D = False
    RealColCount = 8
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      91
      244
      178
      66
      126
      219
      78
      111)
  end
end
