unit FrameInventario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Menus, _Inventario,
  Vcl.Buttons, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, System.Math, _Sessao;

type
  TFrInventario = class(TFrameHenrancaPesquisas)
  private
    { Private declarations }
  public
    { Public declarations }
    function GetInventario(pLinha: Integer = -1): RecInventario;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

var
  FrInventario: TFrInventario;

implementation

{$R *.dfm}

uses PesquisaContagemEstoque;

{ TFrInventario }

function TFrInventario.AdicionarDireto: TObject;
var
  cidades: TArray<RecInventario>;
begin
  cidades := _Inventario.BuscarInventario(Sessao.getConexaoBanco, 0, [FChaveDigitada] );
  if cidades = nil then
    Result := nil
  else
    Result := cidades[0];
end;

function TFrInventario.AdicionarPesquisando: TObject;
begin
  Result := PesquisaContagemEstoque.PesquisarContagemEstoque;
end;

function TFrInventario.GetInventario(pLinha: Integer): RecInventario;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecInventario(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);

end;

function TFrInventario.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecInventario(FDados[i]).InventarioId = RecInventario(pSender).InventarioId then begin
      Result := i;
      Break;
    end;
  end;

end;

procedure TFrInventario.MontarGrid;
var
  i: Integer;
  vSender: RecInventario;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecInventario(FDados[i]);
      AAdd([IntToStr(vSender.InventarioId), vSender.nome]);
    end;
  end;

end;

end.
