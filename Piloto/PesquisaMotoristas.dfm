inherited FormPesquisaMotoristas: TFormPesquisaMotoristas
  Caption = 'Pesquisa de motoristas'
  ClientHeight = 416
  ClientWidth = 698
  ExplicitWidth = 704
  ExplicitHeight = 445
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 698
    Height = 370
    ColCount = 6
    OnDrawCell = sgPesquisaDrawCell
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Raz'#227'o Social'
      'Nome Fanstasia'
      'N'#250'mero CNH'
      'Dt. validade CNH')
    RealColCount = 6
    ExplicitLeft = 0
    ExplicitTop = 46
    ExplicitWidth = 698
    ExplicitHeight = 370
    ColWidths = (
      28
      55
      186
      161
      137
      95)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Top = 368
    Text = '0'
    ExplicitTop = 368
  end
  inherited pnFiltro1: TPanel
    Width = 698
    ExplicitWidth = 698
    inherited eValorPesquisa: TEditLuka
      Width = 491
      ExplicitWidth = 491
    end
  end
end
