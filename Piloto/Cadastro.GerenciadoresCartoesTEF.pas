unit Cadastro.GerenciadoresCartoesTEF;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms,
  Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Sessao, _Biblioteca, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _FrameHerancaPrincipal,
  Frame.Diretorio, _GerenciadorCartaoTEF, _RecordsCadastros, _RecordsEspeciais, Pesquisa.GerenciadoresCartoesTEF,
  _HerancaCadastro, CheckBoxLuka, StaticTextLuka, FrameConfiguracoesCartoesTEF;

type
  TFormCadastroGerenciadoresCartoesTEF = class(TFormHerancaCadastroCodigo)
    FrDiretorioRequisicoes: TFrDiretorio;
    lb1: TLabel;
    eNome: TEditLuka;
    FrDiretorioRespostas: TFrDiretorio;
    FrConfiguracoesCartoesTEF: TFrConfiguracoesCartoesTEF;
    st1: TStaticTextLuka;
  private
    procedure PreencherRegistro(pGerenciador: RecGerenciadorCartaoTEF);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroGerenciadoresCartoesTEF }

procedure TFormCadastroGerenciadoresCartoesTEF.BuscarRegistro;
var
  vGerenciador: TArray<RecGerenciadorCartaoTEF>;
begin
  vGerenciador := _GerenciadorCartaoTEF.BuscarGerenciadorCartaoTEF(Sessao.getConexaoBanco, 0, [eId.AsInt]);

  if vGerenciador = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    eID.SetFocus;
    Exit;
  end;

  inherited;
  PreencherRegistro(vGerenciador[0]);
end;

procedure TFormCadastroGerenciadoresCartoesTEF.ExcluirRegistro;
var
  vRetorno: RecRetornoBD;
begin
  vRetorno := _GerenciadorCartaoTEF.ExcluirGerenciadorCartaoTEF(Sessao.getConexaoBanco, eId.AsInt);

  if vRetorno.TeveErro then begin
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroGerenciadoresCartoesTEF.GravarRegistro(Sender: TObject);
var
  vRetorno: RecRetornoBD;
  vRecConfiguracoes: RecConfiguracoes;
begin
  inherited;

  vRecConfiguracoes := FrConfiguracoesCartoesTEF.getConfiguracoes;

  vRetorno :=
    _GerenciadorCartaoTEF.AtualizarGerenciadorCartaoTEF(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eNome.Text,
      FrDiretorioRequisicoes.getCaminhoDiretorio,
      FrDiretorioRespostas.getCaminhoDiretorio,
      vRecConfiguracoes.TextoProcurarBandeira1,
      vRecConfiguracoes.PosicaoInicialProcBand1,
      vRecConfiguracoes.QtdeCaracteresCopiarBand1,
      vRecConfiguracoes.TextoProcurarBandeira2,
      vRecConfiguracoes.PosicaoInicialProcBand2,
      vRecConfiguracoes.QtdeCaracteresCopiarBand2,
      vRecConfiguracoes.TextoProcurarRede1,
      vRecConfiguracoes.PosicaoInicialProcRede1,
      vRecConfiguracoes.QtdeCaracteresCopiarRede1,
      vRecConfiguracoes.TextoProcurarRede2,
      vRecConfiguracoes.PosicaoInicialProcRede2,
      vRecConfiguracoes.QtdeCaracteresCopiarRede2,
      vRecConfiguracoes.TextoProcurarNumeroCartao1,
      vRecConfiguracoes.PosicIniProcNumeroCartao1,
      vRecConfiguracoes.QtdeCaracCopiarNrCartao1,
      vRecConfiguracoes.TextoProcurarNumeroCartao2,
      vRecConfiguracoes.PosicIniProcNumeroCartao2,
      vRecConfiguracoes.QtdeCaracCopiarNrCartao2,
      vRecConfiguracoes.TextoProcurarNsu1,
      vRecConfiguracoes.PosicaoIniProcurarNsu1,
      vRecConfiguracoes.QtdeCaracCopiarNsu1,
      vRecConfiguracoes.TextoProcurarNsu2,
      vRecConfiguracoes.PosicaoIniProcurarNsu2,
      vRecConfiguracoes.QtdeCaracCopiarNsu2,
      vRecConfiguracoes.TextoProcurarCodAutoriz1,
      vRecConfiguracoes.PosicaoIniProCodAutoriz1,
      vRecConfiguracoes.QtdeCaracCopCodAutoriz1,
      vRecConfiguracoes.TextoProcurarCodAutoriz2,
      vRecConfiguracoes.PosicaoIniProCodAutoriz2,
      vRecConfiguracoes.QtdeCaracCopCodAutoriz2,
      _Biblioteca.ToChar(ckAtivo)
    );

  Sessao.AbortarSeHouveErro( vRetorno );

  _Biblioteca.InformarAlteracaoRegistro(vRetorno.AsInt);
end;

procedure TFormCadastroGerenciadoresCartoesTEF.Modo(pEditando: Boolean);
begin
  inherited;

  _Biblioteca.Habilitar([
    eNome,
    FrDiretorioRequisicoes,
    FrDiretorioRespostas,
    FrConfiguracoesCartoesTEF,
    ckAtivo],
    pEditando
  );

  if pEditando then begin
    ckAtivo.Checked := True;
    SetarFoco(eNome);
  end;
end;

procedure TFormCadastroGerenciadoresCartoesTEF.PesquisarRegistro;
var
  vGerenciador: RecGerenciadorCartaoTEF;
begin
  vGerenciador := RecGerenciadorCartaoTEF(Pesquisa.GerenciadoresCartoesTEF.Pesquisar());

  if vGerenciador = nil then
    Exit;

  inherited;
  PreencherRegistro(vGerenciador);
end;

procedure TFormCadastroGerenciadoresCartoesTEF.PreencherRegistro(pGerenciador: RecGerenciadorCartaoTEF);
begin
  eID.AsInt := pGerenciador.gerenciador_cartao_id;
  eNome.Text := pGerenciador.nome;
  FrDiretorioRequisicoes.eCaminhoDiretorio.Text := pGerenciador.diretorio_arquivos_requisicoes;
  FrDiretorioRespostas.eCaminhoDiretorio.Text := pGerenciador.diretorio_arquivos_respostas;

  FrConfiguracoesCartoesTEF.setConfiguracoes(
    pGerenciador.TextoProcurarBandeira1,
    pGerenciador.PosicaoInicialProcBand1,
    pGerenciador.QtdeCaracteresCopiarBand1,
    pGerenciador.TextoProcurarBandeira2,
    pGerenciador.PosicaoInicialProcBand2,
    pGerenciador.QtdeCaracteresCopiarBand2,
    pGerenciador.TextoProcurarRede1,
    pGerenciador.PosicaoInicialProcRede1,
    pGerenciador.QtdeCaracteresCopiarRede1,
    pGerenciador.TextoProcurarRede2,
    pGerenciador.PosicaoInicialProcRede2,
    pGerenciador.QtdeCaracteresCopiarRede2,
    pGerenciador.TextoProcurarNumeroCartao1,
    pGerenciador.PosicIniProcNumeroCartao1,
    pGerenciador.QtdeCaracCopiarNrCartao1,
    pGerenciador.TextoProcurarNumeroCartao2,
    pGerenciador.PosicIniProcNumeroCartao2,
    pGerenciador.QtdeCaracCopiarNrCartao2,
    pGerenciador.TextoProcurarNsu1,
    pGerenciador.PosicaoIniProcurarNsu1,
    pGerenciador.QtdeCaracCopiarNsu1,
    pGerenciador.TextoProcurarNsu2,
    pGerenciador.PosicaoIniProcurarNsu2,
    pGerenciador.QtdeCaracCopiarNsu2,
    pGerenciador.TextoProcurarCodAutoriz1,
    pGerenciador.PosicaoIniProCodAutoriz1,
    pGerenciador.QtdeCaracCopCodAutoriz1,
    pGerenciador.TextoProcurarCodAutoriz2,
    pGerenciador.PosicaoIniProCodAutoriz2,
    pGerenciador.QtdeCaracCopCodAutoriz2
  );

  ckAtivo.Checked := (pGerenciador.ativo = 'S');
end;

procedure TFormCadastroGerenciadoresCartoesTEF.VerificarRegistro(Sender: TObject);
begin
  inherited;

  eNome.Text := Trim(eNome.Text);
  if eNome.Text = '' then begin
    _Biblioteca.Exclamar('N�o foi informado o nome, verifique!');
    eNome.SetFocus;
    Abort;
  end;

  if FrDiretorioRequisicoes.EstaVazio then begin
    _Biblioteca.Exclamar('N�o foi informado o diret�rio dos arquivos de requisi��es, verifique!');
    FrDiretorioRequisicoes.SetFocus;
    Abort;
  end;

  if FrDiretorioRespostas.EstaVazio then begin
    _Biblioteca.Exclamar('N�o foi informado o diret�rio dos arquivos de respostas, verifique!');
    FrDiretorioRespostas.SetFocus;
    Abort;
  end;
end;

end.
