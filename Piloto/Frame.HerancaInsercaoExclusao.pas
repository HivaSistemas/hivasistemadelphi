unit Frame.HerancaInsercaoExclusao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.Buttons,
  SpeedButtonLuka, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Biblioteca, Vcl.Menus,
  Vcl.StdCtrls, StaticTextLuka;

type
  TFrameHerancaInsercaoExclusao = class(TFrameHerancaPrincipal)
    sgValores: TGridLuka;
    pmOpcoes: TPopupMenu;
    miInserir: TMenuItem;
    miExcluir: TMenuItem;
    miN1: TMenuItem;
    miSubir: TMenuItem;
    miDescer: TMenuItem;
    StaticTextLuka1: TStaticTextLuka;
    procedure sbExcluirClick(Sender: TObject);
    procedure sbInserirClick(Sender: TObject);
    procedure sgValoresKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure miInserirClick(Sender: TObject);
    procedure miExcluirClick(Sender: TObject);
    procedure miSubirClick(Sender: TObject);
    procedure miDescerClick(Sender: TObject);
  public
    procedure SetFocus; override;

    procedure Clear; override;
    procedure SomenteLeitura(pValue: Boolean); override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;

    function EstaVazio: Boolean; override;
  protected
    procedure Inserir; virtual; abstract;
    procedure Excluir; virtual;
  end;

implementation

{$R *.dfm}

{ TFrameHerancaInsercaoExclusao }

procedure TFrameHerancaInsercaoExclusao.Clear;
begin
  inherited;
  sgValores.ClearGrid;
end;

function TFrameHerancaInsercaoExclusao.EstaVazio: Boolean;
begin
  Result := sgValores.Cells[0, 1] = '';
end;

procedure TFrameHerancaInsercaoExclusao.Excluir;
begin
  sgValores.DeleteRow(sgValores.Row, sgValores.RealColCount);
end;

procedure TFrameHerancaInsercaoExclusao.miDescerClick(Sender: TObject);
begin
  inherited;
  if sgValores.Row + 1 < sgValores.RowCount then begin
    sgValores.TrocaRow(sgValores.Row + 1, sgValores.Row);
    sgValores.Row := sgValores.Row + 1;
  end;
end;

procedure TFrameHerancaInsercaoExclusao.miExcluirClick(Sender: TObject);
begin
  inherited;
  Excluir;
end;

procedure TFrameHerancaInsercaoExclusao.miInserirClick(Sender: TObject);
begin
  inherited;
  Inserir;
end;

procedure TFrameHerancaInsercaoExclusao.miSubirClick(Sender: TObject);
begin
  inherited;
  if sgValores.Row - 1 > -1 then begin
    sgValores.TrocaRow(sgValores.Row -1, sgValores.Row);
    sgValores.Row := sgValores.Row -1;
  end;
end;

procedure TFrameHerancaInsercaoExclusao.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([sgValores], pEditando, pLimpar);
end;

procedure TFrameHerancaInsercaoExclusao.sbExcluirClick(Sender: TObject);
begin
  inherited;
  Excluir;
end;

procedure TFrameHerancaInsercaoExclusao.sbInserirClick(Sender: TObject);
begin
  inherited;
  Inserir;
end;

procedure TFrameHerancaInsercaoExclusao.SetFocus;
begin
  inherited;
  sgValores.SetFocus;
end;

procedure TFrameHerancaInsercaoExclusao.sgValoresKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_DELETE then begin
    if miExcluir.Enabled then
      miExcluirClick(Sender);
  end;
end;

procedure TFrameHerancaInsercaoExclusao.SomenteLeitura(pValue: Boolean);
begin
  inherited;
  miExcluir.Enabled := not pValue;
  miInserir.Enabled := not pValue;
  miSubir.Enabled := not pValue;
  miDescer.Enabled := not pValue;
end;

end.
