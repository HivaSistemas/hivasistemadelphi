unit Pesquisa.TipoAcompanhamentoOrcamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls,
  ComboBoxLuka, EditLuka, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _TipoAcompanhamentoOrcamento,
  _RecordsCadastros, _Sessao, _Biblioteca, Math;

type
  TFormTipoAcompanhamentoOrcamento = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    procedure BuscarRegistros; override;
  end;

function Pesquisar(): TObject;

implementation

{$R *.dfm}

const
  coCodigo    = 1;
  coDescricao = 2;
  coAtivo     = 3;

function Pesquisar(): TObject;
var
  obj: TObject;
begin
  obj := _HerancaPesquisas.Pesquisar(TFormTipoAcompanhamentoOrcamento, _TipoAcompanhamentoOrcamento.GetFiltros);
  if obj = nil then
    Result := nil
  else
    Result := RecTipoAcompanhamentoOrcamento(obj);
end;

{ TFormPesquisaTipoAcompanhamentoOrcamento }

procedure TFormTipoAcompanhamentoOrcamento.BuscarRegistros;
var
  i: Integer;
  vTipo: TArray<RecTipoAcompanhamentoOrcamento>;
begin
  inherited;

  vTipo :=
    _TipoAcompanhamentoOrcamento.BuscarTipoAcompanhamentoOrcamento(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text]
    );

  if vTipo = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vTipo);

  for i := Low(vTipo) to High(vTipo) do begin
    sgPesquisa.Cells[coSelecionado, i + 1] := 'N�o';
    sgPesquisa.Cells[coCodigo, i + 1]   := IntToStr(vTipo[i].tipo_acompanhamento_id);
    sgPesquisa.Cells[coDescricao, i + 1] := vTipo[i].descricao;
    sgPesquisa.Cells[coAtivo, i + 1] := vTipo[i].ativo;
  end;

  sgPesquisa.RowCount := IfThen(Length(vTipo) = 1, 2, High(vTipo) + 2);
  sgPesquisa.SetFocus;
end;

procedure TFormTipoAcompanhamentoOrcamento.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = coCodigo then
    vAlinhamento := taRightJustify
  else if ACol = coDescricao then
    vAlinhamento := taLeftJustify
  else
    vAlinhamento := taCenter;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormTipoAcompanhamentoOrcamento.sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;

  if ARow = 0 then
    Exit;
end;

end.
