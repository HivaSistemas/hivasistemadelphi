unit CadastroEstados;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _RecordsEspeciais,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _Biblioteca, _RecordsCadastros, _Estados, _Sessao,
  PesquisaEstados, _HerancaCadastro, CheckBoxLuka;

type
  TFormCadastroEstados = class(TFormHerancaCadastroCodigo)
    Label2: TLabel;
    eNome: TEditLuka;
    lb2: TLabel;
    eCodigoEstadoIBGE: TEditLuka;
    lb1: TLabel;
    ePercICMSInterno: TEditLuka;
    procedure eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); reintroduce;
    procedure sbDesfazerClick(Sender: TObject);
    procedure sbGravarClick(Sender: TObject);
    procedure sbExcluirClick(Sender: TObject);
    procedure sbPesquisarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FCarregou: Boolean;
    procedure PreencherRegistro(pEstado: RecEstados);
  protected
    procedure BuscarRegistro; reintroduce;
    procedure GravarRegistro; reintroduce;
    procedure ExcluirRegistro; reintroduce;
    procedure VerificarRegistro; reintroduce;
    procedure PesquisarRegistro; reintroduce;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean); reintroduce;
  end;

implementation

{$R *.dfm}

{ TFormCadastroEstados }

procedure TFormCadastroEstados.BuscarRegistro;
var
  vEstado: TArray<RecEstados>;
begin
  FCarregou := False;

  vEstado := _Estados.BuscarEstados(Sessao.getConexaoBanco, 0, [eId.Text]);
  if vEstado = nil then
    Exit;

  PreencherRegistro(vEstado[0]);
end;

procedure TFormCadastroEstados.eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then begin
    if (eID.Text) = '' then begin
      _Biblioteca.Exclamar('� necess�rio inserir a UF de um estado!');
      SetarFoco(eID);
      Abort;
    end;

    BuscarRegistro;
    if eNome.Text = '' then
      Modo(True, False);
  end;
end;

procedure TFormCadastroEstados.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _Estados.ExcluirEstados(Sessao.getConexaoBanco, eID.Text);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  Modo(False, True);
end;

procedure TFormCadastroEstados.FormCreate(Sender: TObject);
begin
  inherited;
  Modo(False, True);
  eID.OnKeyDown := eIDKeyDown;
end;

procedure TFormCadastroEstados.GravarRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco :=
    _Estados.AtualizarEstado(
      Sessao.getConexaoBanco,
      eId.Text,
      eNome.Text,
      eCodigoEstadoIBGE.AsInt,
      ePercICMSInterno.AsDouble,
      _Biblioteca.ToChar(ckAtivo)
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.Informar(coAlteracaoRegistroSucesso);

  Modo(False, True);
end;

procedure TFormCadastroEstados.Modo(peditando: Boolean; pLimpar: Boolean);
begin

  _Biblioteca.Habilitar([
    eNome,
    eCodigoEstadoIBGE,
    ckAtivo,
    sbGravar,
    sbDesfazer,
    ePercICMSInterno],
    pEditando,
    pLimpar
  );

  _Biblioteca.Habilitar([eID, sbPesquisar], not pEditando, pLimpar);

  sbExcluir.Enabled := FCarregou and pEditando;

  if pEditando then begin
    SetarFoco(eNome);

    if not ckAtivo.Checked then
      ckAtivo.Checked := not FCarregou;
  end
  else begin
    SetarFoco(eID);
    ckAtivo.Checked := False;
  end;
end;

procedure TFormCadastroEstados.PesquisarRegistro;
var
  estado: RecEstados;
begin
  estado := RecEstados(PesquisaEstados.PesquisarEstado);

  if estado = nil then
    Exit;

  PreencherRegistro(estado);
end;

procedure TFormCadastroEstados.PreencherRegistro(pEstado: RecEstados);
begin
  FCarregou := True;
  Modo(True, True);

  eID.Text                  := pEstado.estado_id;
  eNome.Text                := pEstado.nome;
  eCodigoEstadoIBGE.AsInt   := pEstado.codigo_ibge;
  ePercICMSInterno.AsDouble := pEstado.PercIcmsInterno;
  ckAtivo.Checked           := (pEstado.ativo = 'S');

  pEstado.Free;
end;

procedure TFormCadastroEstados.sbDesfazerClick(Sender: TObject);
begin
  Modo(False, True);
end;

procedure TFormCadastroEstados.sbExcluirClick(Sender: TObject);
begin
  ExcluirRegistro;
end;

procedure TFormCadastroEstados.sbGravarClick(Sender: TObject);
begin
  VerificarRegistro;
  GravarRegistro;
end;

procedure TFormCadastroEstados.sbPesquisarClick(Sender: TObject);
begin
  PesquisarRegistro;
end;

procedure TFormCadastroEstados.VerificarRegistro;
begin
  inherited;

  if eNome.Trim = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar o nome do estado!');
    SetarFoco(eNome);
    Abort;
  end;

  if eCodigoEstadoIBGE.AsInt = 0 then begin
    if not _Biblioteca.Perguntar('Para emiss�o de nota fiscal eletr�nica � necess�rio o c�digo IBGE, deseja continuar sem inform�-lo?') then begin
      SetarFoco(eCodigoEstadoIBGE);
      Abort;
    end;
  end;
end;

end.
