inherited FormInformacoesDevolucao: TFormInformacoesDevolucao
  Caption = 'Informa'#231#245'es da devolu'#231#227'o'
  ClientHeight = 439
  ClientWidth = 759
  OnShow = FormShow
  ExplicitWidth = 765
  ExplicitHeight = 468
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 4
    Top = 3
    Width = 37
    Height = 14
    Caption = 'C'#243'digo'
  end
  object lbl6: TLabel [1]
    Left = 89
    Top = 3
    Width = 59
    Height = 14
    Caption = 'Or'#231'amento'
  end
  object sbInformacoesOrcamento: TSpeedButton [2]
    Left = 152
    Top = 21
    Width = 15
    Height = 18
    Caption = '...'
    OnClick = sbInformacoesOrcamentoClick
  end
  object lbl1: TLabel [3]
    Left = 175
    Top = 3
    Width = 39
    Height = 14
    Caption = 'Cliente'
  end
  object Label1: TLabel [4]
    Left = 379
    Top = 3
    Width = 47
    Height = 14
    Caption = 'Empresa'
  end
  inherited pnOpcoes: TPanel
    Top = 402
    Width = 759
    ExplicitTop = 402
    ExplicitWidth = 759
  end
  object eDevolucaoId: TEditLuka
    Left = 4
    Top = 17
    Width = 80
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 1
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eOrcamentoId: TEditLuka
    Left = 89
    Top = 17
    Width = 62
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 2
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eCliente: TEditLuka
    Left = 175
    Top = 17
    Width = 199
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 3
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object StaticTextLuka1: TStaticTextLuka
    Left = 583
    Top = 6
    Width = 173
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Status'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 4
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object stStatus: TStaticText
    Left = 583
    Top = 22
    Width = 173
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Aberto'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 5
    Transparent = False
  end
  object eEmpresa: TEditLuka
    Left = 379
    Top = 17
    Width = 199
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 6
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object pc1: TPageControl
    Left = 3
    Top = 41
    Width = 753
    Height = 360
    ActivePage = tsPrincipais
    TabOrder = 7
    object tsPrincipais: TTabSheet
      Caption = 'Principais'
      object lbllb3: TLabel
        Left = 207
        Top = 2
        Width = 92
        Height = 14
        Caption = 'Data/hora devol.'
      end
      object lb15: TLabel
        Left = 207
        Top = 43
        Width = 66
        Height = 14
        AutoSize = False
        Caption = 'Valor bruto'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lb16: TLabel
        Left = 313
        Top = 43
        Width = 98
        Height = 14
        AutoSize = False
        Caption = 'Valor out. desp.'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lb17: TLabel
        Left = 419
        Top = 43
        Width = 86
        Height = 14
        AutoSize = False
        Caption = 'Valor desconto'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lb18: TLabel
        Left = 631
        Top = 43
        Width = 77
        Height = 14
        AutoSize = False
        Caption = 'Valor l'#237'quido'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lb2: TLabel
        Left = 525
        Top = 43
        Width = 86
        Height = 14
        AutoSize = False
        Caption = 'Valor frete'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lb3: TLabel
        Left = 2
        Top = 2
        Width = 102
        Height = 14
        Caption = 'Usu'#225'rio devolu'#231#227'o'
      end
      object lb4: TLabel
        Left = 329
        Top = 2
        Width = 111
        Height = 14
        Caption = 'Usu'#225'rio confirma'#231#227'o'
      end
      object lb5: TLabel
        Left = 534
        Top = 2
        Width = 124
        Height = 14
        Caption = 'Data/hora confirma'#231#227'o'
      end
      object lbl26: TLabel
        Left = 2
        Top = 86
        Width = 69
        Height = 14
        Caption = 'Observa'#231#245'es'
      end
      object lb6: TLabel
        Left = 2
        Top = 44
        Width = 126
        Height = 14
        Caption = 'Cliente gera'#231#227'o cr'#233'dito'
      end
      object lb7: TLabel
        Left = 663
        Top = 2
        Width = 39
        Height = 14
        Caption = 'Cr'#233'dito'
      end
      object sbInformacoesCreditoPagar: TSpeedButton
        Left = 726
        Top = 20
        Width = 15
        Height = 18
        Hint = 'Informa'#231#245'es do cr'#233'dito a pagar gerado'
        Caption = '...'
        OnClick = sbInformacoesCreditoPagarClick
      end
      object eDataHoraDevolucao: TEditLukaData
        Left = 207
        Top = 16
        Width = 115
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999 99:99:99;1;_'
        MaxLength = 19
        ReadOnly = True
        TabOrder = 0
        Text = '  /  /       :  :  '
        TipoData = tdDataHora
      end
      object eValorBruto: TEditLuka
        Left = 207
        Top = 58
        Width = 99
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorOutrasDespesas: TEditLuka
        Left = 313
        Top = 58
        Width = 99
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 2
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorDesconto: TEditLuka
        Left = 419
        Top = 58
        Width = 99
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 3
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorLiquido: TEditLuka
        Left = 631
        Top = 58
        Width = 99
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object e1: TEditLuka
        Left = 525
        Top = 58
        Width = 99
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 5
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eUsuarioDevolucao: TEditLuka
        Left = 2
        Top = 16
        Width = 198
        Height = 22
        CharCase = ecUpperCase
        TabOrder = 6
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eUsuarioConfirmacao: TEditLuka
        Left = 329
        Top = 16
        Width = 198
        Height = 22
        CharCase = ecUpperCase
        TabOrder = 7
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eDataHoraConfirmacao: TEditLukaData
        Left = 534
        Top = 16
        Width = 124
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999 99:99:99;1;_'
        MaxLength = 19
        ReadOnly = True
        TabOrder = 8
        Text = '  /  /       :  :  '
        TipoData = tdDataHora
      end
      object eObservacoes: TMemo
        Left = 2
        Top = 100
        Width = 740
        Height = 230
        TabOrder = 9
      end
      object eClienteGeracaoCredito: TEditLuka
        Left = 2
        Top = 58
        Width = 198
        Height = 22
        CharCase = ecUpperCase
        TabOrder = 10
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eCreditoPagarId: TEditLuka
        Left = 663
        Top = 16
        Width = 62
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 11
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
    end
    object tsItens: TTabSheet
      Caption = 'Itens devolvidos'
      ImageIndex = 1
      object sgProdutos: TGridLuka
        Left = 0
        Top = 0
        Width = 745
        Height = 331
        Align = alClient
        ColCount = 12
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 0
        OnDrawCell = sgProdutosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Produto'
          'Nome'
          'Marca'
          'Qtde.entregues'
          'Qtde.pendentes'
          'Qtde.sem prev.'
          'Und.'
          'Pre'#231'o unit.'
          'Valor bruto'
          'Valor out. desp.'
          'Valor desc.'
          'Valor L'#237'quido'
          '')
        Grid3D = False
        RealColCount = 12
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          53
          189
          92
          91
          101
          87
          35
          67
          73
          92
          64
          81)
      end
    end
    object tsRetiradasEntregasDevolvidas: TTabSheet
      Caption = 'Retiradas/entregas devolvidas'
      ImageIndex = 2
      object sgRetiradasEntregas: TGridLuka
        Left = 0
        Top = 0
        Width = 745
        Height = 331
        Align = alClient
        ColCount = 3
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 0
        OnDblClick = sgRetiradasEntregasDblClick
        OnDrawCell = sgRetiradasEntregasDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'C'#243'digo'
          'Tipo'
          'Qtde.produtos')
        Grid3D = False
        RealColCount = 12
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          53
          159
          92)
      end
    end
    object tsNotasFiscais: TTabSheet
      Caption = 'Notas fiscais'
      ImageIndex = 3
      object sgNotasFiscais: TGridLuka
        Left = 0
        Top = 0
        Width = 745
        Height = 331
        Align = alClient
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        TabOrder = 0
        OnDblClick = sgNotasFiscaisDblClick
        OnDrawCell = sgNotasFiscaisDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Nota'
          'Tipo'
          'Data/hora emiss'#227'o'
          'Tipo movimento'
          'Valor total')
        Grid3D = False
        RealColCount = 10
        AtivarPopUpSelecao = False
        ColWidths = (
          69
          133
          124
          219
          110)
      end
    end
  end
end
