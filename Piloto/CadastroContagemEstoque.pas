unit CadastroContagemEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo,_RecordsEspeciais, _InventarioItens,
  Frame.EnderecoEstoqueVao, Frame.EnderecoEstoqueNivel,_Inventario, _Sessao, _ListaContagem,
  Frame.EnderecoEstoqueModulo, Frame.EnderecoEstoqueRua, Frame.EnderecoEstoque,
  FrameEmpresas, FrameLocais, Vcl.StdCtrls, ComboBoxLuka, FrameDataInicialFinal,
  FrameMarcas, Frame.DepartamentosSecoesLinhas, _FrameHerancaPrincipal,
  _FrameHenrancaPesquisas, FrameProdutos, Vcl.Mask, EditLukaData, Vcl.ComCtrls,
  CheckBoxLuka, EditLuka, Vcl.Buttons, Vcl.ExtCtrls;

type
  TFormCadastroContagemEstoque = class(TFormHerancaCadastroCodigo)
    pgMaster: TPageControl;
    tsFiltros: TTabSheet;
    lbTipoControleEstoque: TLabel;
    lbAtivo: TLabel;
    lb3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    FrProdutos: TFrProdutos;
    FrDepartamentosSecoesLinhas: TFrameDepartamentosSecoesLinhas;
    FrMarcas: TFrMarcas;
    FrDataCadastroProduto: TFrDataInicialFinal;
    cbTipoControleEstoque: TComboBoxLuka;
    cbAtivo: TComboBoxLuka;
    cbSituacaoDoEstoque: TComboBoxLuka;
    pnEnderecos: TPanel;
    FrEnderecoEstoque: TFrEnderecoEstoque;
    FrEnderecoEstoqueRua: TFrEnderecoEstoqueRua;
    FrEnderecoEstoqueModulo: TFrEnderecoEstoqueModulo;
    FrEnderecoEstoqueNivel: TFrEnderecoEstoqueNivel;
    FrEnderecoEstoqueVao: TFrEnderecoEstoqueVao;
    cbQuantidadeEstoque: TComboBoxLuka;
    pnQuantidadeEstoque: TPanel;
    Label7: TLabel;
    lb2: TLabel;
    eValor1: TEdit;
    eValor2: TEdit;
    cbAgruparLocais: TComboBoxLuka;
    FrDataAjuste: TFrDataInicialFinal;
    cbAgrupamento: TComboBoxLuka;
    edtNome: TEditLuka;
    lb1: TLabel;
    edtStatus: TEditLuka;
    Label2: TLabel;
    edtDataInicio: TEditLukaData;
    lbDataNascimento: TLabel;
    edtDataFinal: TEditLukaData;
    Label3: TLabel;
    FrEmpresas: TFrEmpresas;
    FrLocais: TFrLocais;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure PreencherRegistro(pInventario: RecInventario);
  public
    { Public declarations }
    function FiltrarProdutosContagem: TArray<RecInventarioItens>;
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

var
  FormCadastroContagemEstoque: TFormCadastroContagemEstoque;

implementation

{$R *.dfm}

uses _Biblioteca, PesquisaContagemEstoque ;

{ TFormCadastroContagemEstoque }

procedure TFormCadastroContagemEstoque.BuscarRegistro;
var
  vInventario: TArray<RecInventario>;
begin
  vInventario := _Inventario.BuscarInventario(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if vInventario = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    SetarFoco(eID);
    Exit;
  end;

  inherited;
  PreencherRegistro(vInventario[0]);
end;

procedure TFormCadastroContagemEstoque.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _Inventario.ExcluirInventario(Sessao.getConexaoBanco, eId.AsInt);
  Sessao.AbortarSeHouveErro(vRetBanco);
  inherited;

end;

function TFormCadastroContagemEstoque.FiltrarProdutosContagem: TArray<RecInventarioItens>;
var
  vSql: string;
  vSqlEnderecoEstoque: string;
  vEstoquesTotal: TArray<RecRelacaoEstoque>;
  i: Integer;
  vInventarioItens: TArray<RecInventarioItens>;
  vProdutos, vLocal, vVirgula: string;
begin
  inherited;
  Result := nil;
  vSql := '';

  if (FrDataAjuste.DatasValidas) and (cbAgruparLocais.GetValor = 'S') then begin
    Exclamar('O filtro "Produtos que n�o tiveram corre��es" n�o pode ser aplicado junto ao filtro "Agrupar locais"');
    SetarFoco(cbAgruparLocais);
    Abort;
  end;

  if not FrProdutos.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrProdutos.getSqlFiltros('PRO.PRODUTO_ID'));

  if not FrDepartamentosSecoesLinhas.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrDepartamentosSecoesLinhas.getSqlFiltros('PRO.LINHA_PRODUTO_ID'));

  if not FrLocais.EstaVazio then
   _Biblioteca.WhereOuAnd(vSql, FrLocais.getSqlFiltros('DIV.LOCAL_ID'));

  if not FrEmpresas.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, 'EST.EMPRESA_ID in (select EMPRESA_ID from ESTOQUES_DIVISAO where '+ FrEmpresas.getSqlFiltros('EMPRESA_ID') + ')');


  if not FrDataCadastroProduto.NenhumaDataValida then
    _Biblioteca.WhereOuAnd(vSql, FrDataCadastroProduto.getSqlFiltros('PRO.DATA_CADASTRO'));

  if not FrMarcas.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrMarcas.getSqlFiltros('PRO.MARCA_ID'));

  if cbTipoControleEstoque.GetValor <> 'NaoFiltrar' then
    _Biblioteca.WhereOuAnd(vSql, 'PRO.TIPO_CONTROLE_ESTOQUE = ''' + cbTipoControleEstoque.GetValor + ''' ');

  if cbAtivo.GetValor <> 'NaoFiltrar' then
    _Biblioteca.WhereOuAnd(vSql, 'PRO.ATIVO = ''' + cbAtivo.GetValor + ''' ');

  if FrLocais.EstaVazio then begin
    Exclamar('N�o foi definido um local de produtos!Por favor verifique!');
    SetarFoco(FrLocais);
    Abort;
  end;

  vSqlEnderecoEstoque := '';

  if not frEnderecoEstoque.EstaVazio then begin
    vSqlEnderecoEstoque :=
      ' inner join ENDERECOS_ESTOQUE_PRODUTO EPR ' +
      ' on EPR.PRODUTO_ID = PRO.PRODUTO_ID ' +
      ' and ' + FrEnderecoEstoque.getSqlFiltros('EPR.ENDERECO_ID');
  end
  else if (not FrEnderecoEstoqueRua.EstaVazio) or (not FrEnderecoEstoqueModulo.EstaVazio) or (not FrEnderecoEstoqueNivel.EstaVazio) or (not FrEnderecoEstoqueVao.EstaVazio) then begin
    vSqlEnderecoEstoque :=
      ' inner join ENDERECOS_ESTOQUE_PRODUTO EPR ' +
      ' on EPR.PRODUTO_ID = PRO.PRODUTO_ID ' +
      ' and EPR.ENDERECO_ID in ( ' +
      '   select ' +
      '     ENDERECO_ID ' +
      '   from ENDERECO_ESTOQUE EES ' +
      '   where 1 = 1 ';

    if not FrEnderecoEstoqueRua.EstaVazio then
      vSqlEnderecoEstoque := vSqlEnderecoEstoque + ' and ' + FrEnderecoEstoqueRua.getSqlFiltros('EES.RUA_ID');

    if not FrEnderecoEstoqueModulo.EstaVazio then
      vSqlEnderecoEstoque := vSqlEnderecoEstoque + ' and ' + FrEnderecoEstoqueModulo.getSqlFiltros('EES.MODULO_ID');

    if not FrEnderecoEstoqueNivel.EstaVazio then
      vSqlEnderecoEstoque := vSqlEnderecoEstoque + ' and ' + FrEnderecoEstoqueNivel.getSqlFiltros('EES.NIVEL_ID');

    if not FrEnderecoEstoqueVao.EstaVazio then
      vSqlEnderecoEstoque := vSqlEnderecoEstoque + ' and ' + FrEnderecoEstoqueVao.getSqlFiltros('EES.VAO_ID');

    vSqlEnderecoEstoque := vSqlEnderecoEstoque + ') ';
  end;

  if cbSituacaoDoEstoque.GetValor <> 'NaoFiltrar' then begin
    if cbSituacaoDoEstoque.GetValor = 'POS' then
      vSql := vSql + ' and ' + 'DIV.FISICO' + ' > 0 '
    else if cbSituacaoDoEstoque.GetValor = 'NEG' then
      vSql := vSql + ' and ' + 'DIV.FISICO' + ' < 0 '
    else
      vSql := vSql + ' and ' + 'DIV.FISICO' + ' = 0 ';
  end;

  if cbQuantidadeEstoque.GetValor <> 'NaoFiltrar' then begin
    if cbQuantidadeEstoque.GetValor = 'EN' then begin
      vSql := vSql + ' and ' + 'DIV.FISICO' + ' > ' + eValor1.Text + ' and DIV.FISICO < ' + eValor2.Text + ' ';
    end
    else if cbQuantidadeEstoque.GetValor = 'MA' then begin
      vSql := vSql + ' and ' + 'DIV.FISICO' + ' > ' + eValor1.Text + ' ';
    end
    else if cbQuantidadeEstoque.GetValor = 'ME' then begin
      vSql := vSql + ' and ' + 'DIV.FISICO' + ' < ' + eValor1.Text + ' ';
    end;
  end;

  if cbAgruparLocais.GetValor = 'N' then begin
    if cbAgrupamento.GetValor = 'MAR' then
      vSql := vSql + ' order by MAR.NOME ASC, PRO.NOME ASC '
    else
      vSql := vSql + ' order by PRO.NOME ';
  end;

  vEstoquesTotal := _ListaContagem.BuscarEstoque(
    Sessao.getConexaoBanco,
    vSql,
    vSqlEnderecoEstoque,
    cbAgruparLocais.GetValor = 'S',
    cbAgrupamento.GetValor = 'MAR',
    IIfStr(cbAgrupamento.GetValor = 'NEN', ' order by PRO.NOME ', ' order by MAR.NOME ASC, PRO.NOME ASC ')
  );

  if vEstoquesTotal = nil then
  begin
    Exclamar('Nenhum produto adicionado a contagem!');
    Abort;
  end;

  vProdutos := '';
  vLocal    := '';
  vVirgula  := '';

  SetLength(vInventarioItens, Length(vEstoquesTotal));
  for i := Low(vEstoquesTotal) to High(vEstoquesTotal) do
  begin
    vInventarioItens[i].ProdutoId := vEstoquesTotal[i].ProdutoId;
    vInventarioItens[i].LocalId   := vEstoquesTotal[i].LocalId;
    vInventarioItens[i].Estoque   := vEstoquesTotal[i].EstoqueFisico.ToDouble;
    vInventarioItens[i].Contou    := 'N';

    vProdutos := vProdutos + vVirgula + vEstoquesTotal[i].ProdutoId.ToString;
    vLocal    := vLocal    + vVirgula + vEstoquesTotal[i].LocalId.ToString;
    vVirgula := ', ';
  end;

  Result := vInventarioItens;

  if _InventarioItens.ProdutosEmContagem(Sessao.getConexaoBanco, vProdutos, vLocal, FrEmpresas.getEmpresa.EmpresaId.ToString) then
  begin
    Exclamar('Produtos desta contagem j� existe em outra contagem!'+#10+'Altere os filtros ou cancela.');
    Abort;
  end;

end;

procedure TFormCadastroContagemEstoque.FormCreate(Sender: TObject);
begin
  inherited;
  OnKeyDown := ProximoCampo;
end;

procedure TFormCadastroContagemEstoque.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  vListaProdutos: TArray<RecInventarioItens>;
begin
  inherited;

  vListaProdutos := FiltrarProdutosContagem;

  vRetBanco :=
    _Inventario.AtualizarInventario(
      Sessao.getConexaoBanco,
      eID.AsInt,
      FrEmpresas.getEmpresa(0).EmpresaId,
      edtNome.Text,
      0,
      0,
      0,
      vListaProdutos
    );

  Sessao.AbortarSeHouveErro(vRetBanco);
  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);

end;

procedure TFormCadastroContagemEstoque.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    pgMaster,
    edtNome,
    edtStatus,
    edtDataInicio,
    edtDataFinal,
    FrEmpresas,
    FrLocais,
    FrProdutos,
    FrMarcas,
    FrDepartamentosSecoesLinhas,
    ckAtivo],
    pEditando
  );

  if pEditando then begin
    SetarFoco(edtNome);
    ckAtivo.Checked := True;
  end;

  SomenteLeitura([eID,
                  edtNome,
                  edtStatus,
                  edtDataInicio,
                  edtDataFinal,
                  edtStatus,
                  FrEmpresas,
                  pgMaster],
                  (edtStatus.Text = 'FECHADO') or (eID.Text <> ''));


end;

procedure TFormCadastroContagemEstoque.PesquisarRegistro;
var
  vInventario: RecInventario;
begin
  vInventario := RecInventario(PesquisaContagemEstoque.PesquisarContagemEstoque);
  if vInventario = nil then
    Exit;

  inherited;
  PreencherRegistro(vInventario);
end;

procedure TFormCadastroContagemEstoque.PreencherRegistro(
  pInventario: RecInventario);
begin
  inherited;
  eID.AsInt := pInventario.InventarioId;
  edtNome.Text := pInventario.Nome;
  edtDataInicio.AsDataHora := pInventario.DataInicio;
  edtDataFinal.AsDataHora := pInventario.DataFinal;
  edtStatus.Text := IIfStr(edtDataFinal.AsDataHora = 0,'Aberto','Fechado');
  FrEmpresas.InserirDadoPorChave(pInventario.EmpresaId);
  if (edtStatus.Text = 'FECHADO') or (eID.Text <> '') then
  begin
    sbGravar.Enabled := False;
    sbExcluir.Enabled := False;
  end;

end;

procedure TFormCadastroContagemEstoque.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if edtNome.Trim = '' then begin
    _Biblioteca.Exclamar('Nome Inv�lido!');
    SetarFoco(edtNome);
    Abort;
  end;

  if FrEmpresas.EstaVazio then begin
    Exclamar('A empresa n�o foi definida!Por favor verifique!');
    SetarFoco(FrEmpresas);
    Abort;
  end;
end;

end.
