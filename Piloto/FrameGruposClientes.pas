unit FrameGruposClientes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Menus, _Biblioteca, _ClientesGrupos,
  Vcl.Buttons, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Sessao, PesquisaClientesGrupos;

type
  TFrGruposClientes = class(TFrameHenrancaPesquisas)
  public
    function getDados(pLinha: Integer = -1): RecClientesGrupos;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrGruposClientes }

function TFrGruposClientes.AdicionarDireto: TObject;
var
  vDados: TArray<RecClientesGrupos>;
begin
  vDados := _ClientesGrupos.BuscarClientesGrupos(Sessao.getConexaoBanco, 0, [FChaveDigitada], ckSomenteAtivos.Checked);
  if vDados = nil then
    Result := nil
  else
    Result := vDados[0];
end;

function TFrGruposClientes.AdicionarPesquisando: TObject;
begin
  Result := PesquisaClientesGrupos.Pesquisar(ckSomenteAtivos.Checked);
end;

function TFrGruposClientes.getDados(pLinha: Integer): RecClientesGrupos;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecClientesGrupos(FDados[IIfInt(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrGruposClientes.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecClientesGrupos(FDados[i]).ClienteGrupoId = RecClientesGrupos(pSender).ClienteGrupoId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrGruposClientes.MontarGrid;
var
  i: Integer;
  vSender: RecClientesGrupos;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecClientesGrupos(FDados[i]);
      AAdd([IntToStr(vSender.ClienteGrupoId), vSender.nome]);
    end;
  end;
end;

end.
