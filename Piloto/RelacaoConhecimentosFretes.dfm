inherited FormRelacaoConhecimentosFretes: TFormRelacaoConhecimentosFretes
  Caption = 'Rela'#231#227'o de conhecimentos de fretes'
  ExplicitTop = -19
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    ExplicitLeft = -2
    inherited sbImprimir: TSpeedButton
      Left = 4
      Top = 501
      ExplicitLeft = 4
      ExplicitTop = 501
    end
  end
  inherited pcDados: TPageControl
    ActivePage = tsResultado
    inherited tsFiltros: TTabSheet
      inline FrEmpresas: TFrEmpresas
        Left = 0
        Top = 2
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitTop = 2
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 53
            ExplicitWidth = 53
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
      inline FrCFOP: TFrCFOPs
        Left = 0
        Top = 171
        Width = 401
        Height = 82
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitTop = 171
        ExplicitWidth = 401
        inherited sgPesquisa: TGridLuka
          Width = 376
          ExplicitWidth = 376
        end
        inherited PnTitulos: TPanel
          Width = 401
          ExplicitWidth = 401
          inherited lbNomePesquisa: TLabel
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 296
            ExplicitLeft = 296
          end
        end
        inherited cbTipoCFOPs: TComboBoxLuka
          Height = 22
          ItemIndex = 2
          Text = 'Todos'
          ExplicitHeight = 22
        end
        inherited pnPesquisa: TPanel
          Left = 376
          ExplicitLeft = 376
        end
        inherited ckEntradaConhecimentoFrete: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      inline FrDatasCadastro: TFrDataInicialFinal
        Left = 409
        Top = 2
        Width = 190
        Height = 40
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 409
        ExplicitTop = 2
        ExplicitWidth = 190
        ExplicitHeight = 40
        inherited Label1: TLabel
          Width = 93
          Height = 14
          ExplicitWidth = 93
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrDatasEmissao: TFrDataInicialFinal
        Left = 409
        Top = 45
        Width = 190
        Height = 41
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 409
        ExplicitTop = 45
        ExplicitWidth = 190
        inherited Label1: TLabel
          Width = 93
          Height = 14
          Caption = 'Data de emiss'#227'o'
          ExplicitWidth = 93
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrCodigoConhecimento: TFrameInteiros
        Left = 605
        Top = 2
        Width = 135
        Height = 90
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 4
        TabStop = True
        ExplicitLeft = 605
        ExplicitTop = 2
        ExplicitWidth = 135
        ExplicitHeight = 90
        inherited sgInteiros: TGridLuka
          Width = 135
          Height = 72
          ExplicitWidth = 135
          ExplicitHeight = 72
        end
        inherited Panel1: TPanel
          Width = 135
          Caption = 'C'#243'digo do conhecimento'
          ExplicitWidth = 135
        end
      end
      inline FrNumeroConhecimento: TFrameInteiros
        Left = 605
        Top = 96
        Width = 135
        Height = 90
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 5
        TabStop = True
        ExplicitLeft = 605
        ExplicitTop = 96
        ExplicitWidth = 135
        ExplicitHeight = 90
        inherited sgInteiros: TGridLuka
          Width = 135
          Height = 72
          ExplicitWidth = 135
          ExplicitHeight = 72
        end
        inherited Panel1: TPanel
          Width = 135
          Caption = 'N'#250'mero do conhec.'
          ExplicitWidth = 135
        end
      end
      inline FrNumeroNota: TFrameInteiros
        Left = 605
        Top = 190
        Width = 135
        Height = 90
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 6
        TabStop = True
        ExplicitLeft = 605
        ExplicitTop = 190
        ExplicitWidth = 135
        ExplicitHeight = 90
        inherited sgInteiros: TGridLuka
          Width = 135
          Height = 72
          ExplicitWidth = 135
          ExplicitHeight = 72
        end
        inherited Panel1: TPanel
          Width = 135
          Caption = 'N'#250'mero da nota fiscal'
          ExplicitWidth = 135
        end
      end
      inline FrTransportadoras: TFrTransportadora
        Left = 0
        Top = 86
        Width = 401
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 7
        TabStop = True
        ExplicitTop = 86
        ExplicitWidth = 401
        inherited sgPesquisa: TGridLuka
          Width = 376
          ExplicitWidth = 376
        end
        inherited PnTitulos: TPanel
          Width = 401
          ExplicitWidth = 401
          inherited lbNomePesquisa: TLabel
            Width = 89
            ExplicitWidth = 89
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 296
            ExplicitLeft = 296
          end
        end
        inherited pnPesquisa: TPanel
          Left = 376
          ExplicitLeft = 376
        end
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object sgConhec: TGridLuka
        Left = 0
        Top = 0
        Width = 884
        Height = 518
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 12
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
        TabOrder = 0
        OnDblClick = sgConhecDblClick
        OnDrawCell = sgConhecDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Conhec.'
          'Transportadora'
          'N'#250'mero'
          'Modelo'
          'S'#233'rie'
          'Data emiss'#227'o'
          'Valor conhec.'
          'Base calc. ICMS'
          '% ICMS'
          'Valor ICMS'
          'Empresa'
          'Chave de acesso'
          '')
        OnGetCellColor = sgConhecGetCellColor
        Grid3D = False
        RealColCount = 15
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          53
          211
          53
          54
          38
          93
          83
          91
          43
          66
          144
          300)
      end
    end
  end
end
