unit RelacaoCadastros;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl,
  Vcl.ComCtrls, Vcl.StdCtrls, CheckBoxLuka, Vcl.Buttons, Vcl.ExtCtrls,
  Vcl.Grids, GridLuka, _FrameHerancaPrincipal, _FrameHenrancaPesquisas,
  FrameClientes, FrameBairros, FrameCidades, FrameDataInicialFinal,
  Frame.Inteiros, GroupBoxLuka, FrameEstados, Frame.Cadastros, FrameRotas,
  ComboBoxLuka, EditLuka, FrameVendedores, FrameGruposFornecedores, _RelacaoCadastros,
  _RecordsCadastros, _Biblioteca, _Sessao, InformacoesCadastro,
  FrameFuncionarios;

type
  TFormRelacaoCadastros = class(TFormHerancaRelatoriosPageControl)
    sgCadastros: TGridLuka;
    pcFiltros: TPageControl;
    tsGerais: TTabSheet;
    tsCliente: TTabSheet;
    FrCidades: TFrCidades;
    FrBairros: TFrBairros;
    FrDataCadastro: TFrDataInicialFinal;
    gbTipoCadastro: TGroupBoxLuka;
    ckECliente: TCheckBoxLuka;
    ckEConcorrente: TCheckBoxLuka;
    ckEFornecedor: TCheckBoxLuka;
    ckEFuncionario: TCheckBoxLuka;
    ckETransportadora: TCheckBoxLuka;
    ckEMotorista: TCheckBoxLuka;
    ckEProfissional: TCheckBoxLuka;
    FrCadastros: TFrameCadastros;
    FrEstados: TFrEstados;
    FrRotas: TFrRotas;
    cbTipoPessoa: TComboBoxLuka;
    lb2: TLabel;
    cbSexo: TComboBoxLuka;
    lbllb4: TLabel;
    FrDataInicialFinal: TFrDataInicialFinal;
    eNomeFantasia: TEditLuka;
    lbNomeFantasiaApelido: TLabel;
    tsFornecedor: TTabSheet;
    gbTipoCliente: TGroupBoxLuka;
    ckNaoContribuinte: TCheckBoxLuka;
    ckContribuinte: TCheckBoxLuka;
    ckOrgaoPublico: TCheckBoxLuka;
    ckRevendaCliente: TCheckBoxLuka;
    ckContrutora: TCheckBoxLuka;
    ckClinicaHospital: TCheckBoxLuka;
    ckProdutoRural: TCheckBoxLuka;
    FrVendedorPadrao: TFrVendedores;
    Label1: TLabel;
    cbNaoGerarComissao: TComboBoxLuka;
    Label2: TLabel;
    cbIgnorarRegraDirecNota: TComboBoxLuka;
    cbTipoPreco: TComboBoxLuka;
    Label3: TLabel;
    Label4: TLabel;
    cbEmitirSomenteNFE: TComboBoxLuka;
    cbEmitirNotaFiscalSimplesFaturamento: TComboBoxLuka;
    Label5: TLabel;
    cbEmitirModeloNotaFiscal: TComboBoxLuka;
    Label6: TLabel;
    PnTitulos: TPanel;
    FrGrupoFornecedor: TFrGruposFornecedores;
    gbTipoFornecedor: TGroupBoxLuka;
    ckRevendaFornecedor: TCheckBoxLuka;
    ckDistribuidora: TCheckBoxLuka;
    ckIndustria: TCheckBoxLuka;
    ckOutro: TCheckBoxLuka;
    Label7: TLabel;
    cbFornecedorRevenda: TComboBoxLuka;
    Label8: TLabel;
    cbFornecedorUsoConsumo: TComboBoxLuka;
    cbFornecedorServico: TComboBoxLuka;
    Label9: TLabel;
    eRazaoSocial: TEditLuka;
    Label10: TLabel;
    FrFuncionarios: TFrFuncionarios;
    SpeedButton2: TSpeedButton;
    procedure sgCadastrosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgCadastrosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgCadastrosDblClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Carregar(Sender: TObject); override;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

const
  coCodigo          = 0;
  coRazaoSocial     = 1;
  coNomeFantasia    = 2;
  coCPFCNPJ         = 3;
  coLogradouro      = 4;
  coComplemento     = 5;
  coBairro          = 6;
  coCidadeUf        = 7;
  coEmail           = 8;
  coECliente        = 9;
  coEFornecedor     = 10;
  coEMotorista      = 11;
  coETransportadora = 12;
  coEProfissional   = 13;
  coAtivoAnalitico  = 14;
  coDataCadastro    = 15;
  coUsuarioCadastro = 16;
  coAtivoSintetico  = 17;

{ TFormRelacaoCadastros }

procedure TFormRelacaoCadastros.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vCadastros: TArray<RecRelacaoCadastros>;
  vSqlTiposCadastros: string;
begin
  inherited;
  sgCadastros.ClearGrid();
  vCadastros := nil;

  if not FrCadastros.EstaVazio then begin
    _Biblioteca.WhereOuAnd(vSql, FrCadastros.getSqlFiltros('CAD.CADASTRO_ID'));
  end
  else begin
    if not FrEstados.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrEstados.getSqlFiltros('BAI.ESTADO_ID'));

    if not FrCidades.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrCidades.getSqlFiltros('BAI.CIDADE_ID'));

    if not FrBairros.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrBairros.getSqlFiltros('BAI.BAIRRO_ID'));

    if not FrRotas.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrRotas.getSqlFiltros('BAI.ROTA_ID'));

    if not FrFuncionarios.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrFuncionarios.getSqlFiltros('CLI.USUARIO_SESSAO_ID'));

    if
      (ckECliente.Checked) or (ckEConcorrente.Checked) or (ckEFornecedor.Checked) or
      (ckEFuncionario.Checked) or (ckETransportadora.Checked) or (ckEMotorista.Checked) or
      (ckEProfissional.Checked)
    then begin
      if ckECliente.Checked then
        _Biblioteca.AddOrSeNecessario(vSqlTiposCadastros, 'CAD.E_CLIENTE = ''S''');

      if ckEConcorrente.Checked then
        _Biblioteca.AddOrSeNecessario(vSqlTiposCadastros, 'CAD.E_CONCORRENTE = ''S''');

      if ckEFornecedor.Checked then
        _Biblioteca.AddOrSeNecessario(vSqlTiposCadastros, 'CAD.E_FORNECEDOR = ''S''');

      if ckEFuncionario.Checked then
        _Biblioteca.AddOrSeNecessario(vSqlTiposCadastros, 'CAD.E_FUNCIONARIO = ''S''');

      if ckETransportadora.Checked then
        _Biblioteca.AddOrSeNecessario(vSqlTiposCadastros, 'CAD.E_TRANSPORTADORA = ''S''');

      if ckEMotorista.Checked then
        _Biblioteca.AddOrSeNecessario(vSqlTiposCadastros, 'CAD.E_MOTORISTA = ''S''');

      if ckEProfissional.Checked then
        _Biblioteca.AddOrSeNecessario(vSqlTiposCadastros, 'CAD.E_PROFISSIONAL = ''S''');

      if vSqlTiposCadastros <> '' then begin
        vSqlTiposCadastros := ' (' + vSqlTiposCadastros + ') ';
        _Biblioteca.WhereOuAnd(vSql, vSqlTiposCadastros);
      end;
    end;

    if not gbTipoCliente.NenhumMarcado then
      _Biblioteca.WhereOuAnd(vSql, ' (' + gbTipoCliente.GetSql('CLI.TIPO_CLIENTE') + ') ');

    if cbTipoPessoa.GetValor <> _Biblioteca.cbNaoFiltrar then
      _Biblioteca.WhereOuAnd(vSql, 'CAD.TIPO_PESSOA = ''' + cbTipoPessoa.GetValor + '''');

    if cbSexo.GetValor <> _Biblioteca.cbNaoFiltrar then
      _Biblioteca.WhereOuAnd(vSql, 'CAD.SEXO = ''' + cbSexo.GetValor + '''');

    if not (Trim(eNomeFantasia.Text) = '') then
      _Biblioteca.WhereOuAnd(vSql, 'CAD.NOME_FANTASIA like ''%' + Trim(eNomeFantasia.Text) + '%''');

    if not (Trim(eRazaoSocial.Text) = '') then
      _Biblioteca.WhereOuAnd(vSql, 'CAD.RAZAO_SOCIAL like ''%' + Trim(eRazaoSocial.Text) + '%''');

    _Biblioteca.WhereOuAnd(vSql, gbTipoCliente.GetSql('CLI.TIPO_CLIENTE'));

    if cbNaoGerarComissao.GetValor <> _Biblioteca.cbNaoFiltrar then
      _Biblioteca.WhereOuAnd(vSql, 'CLI.NAO_GERAR_COMISSAO = ''' + cbNaoGerarComissao.GetValor + '''');

    if cbIgnorarRegraDirecNota.GetValor <> _Biblioteca.cbNaoFiltrar then
      _Biblioteca.WhereOuAnd(vSql, 'CLI.IGNORAR_REDIR_REGRA_NOTA_FISC = ''' + cbIgnorarRegraDirecNota.GetValor + '''');

    if cbTipoPreco.GetValor <> _Biblioteca.cbNaoFiltrar then
      _Biblioteca.WhereOuAnd(vSql, 'CLI.TIPO_PRECO = ''' + cbTipoPreco.GetValor + '''');

    if cbEmitirSomenteNFE.GetValor <> _Biblioteca.cbNaoFiltrar then
      _Biblioteca.WhereOuAnd(vSql, 'CLI.EMITIR_SOMENTE_NFE = ''' + cbEmitirSomenteNFE.GetValor + '''');

    if cbEmitirNotaFiscalSimplesFaturamento.GetValor <> _Biblioteca.cbNaoFiltrar then
      _Biblioteca.WhereOuAnd(vSql, 'CLI.EXIGIR_NOTA_SIMPLES_FAT = ''' + cbEmitirNotaFiscalSimplesFaturamento.GetValor + '''');

    if cbEmitirModeloNotaFiscal.GetValor <> _Biblioteca.cbNaoFiltrar then
      _Biblioteca.WhereOuAnd(vSql, 'CLI.EXIGIR_MODELO_NOTA_FISCAL = ''' + cbEmitirModeloNotaFiscal.GetValor + '''');

    if not FrVendedorPadrao.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrVendedorPadrao.getSqlFiltros('CLI.VENDEDOR_ID'));

    if not FrGrupoFornecedor.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrGrupoFornecedor.getSqlFiltros('FON.GRUPO_FORNECEDOR_ID'));

    _Biblioteca.WhereOuAnd(vSql, gbTipoFornecedor.GetSql('FON.TIPO_FORNECEDOR'));

    if cbFornecedorRevenda.GetValor <> _Biblioteca.cbNaoFiltrar then
      _Biblioteca.WhereOuAnd(vSql, 'FON.REVENDA = ''' + cbFornecedorRevenda.GetValor + '''');

    if cbFornecedorUsoConsumo.GetValor <> _Biblioteca.cbNaoFiltrar then
      _Biblioteca.WhereOuAnd(vSql, 'FON.USO_CONSUMO = ''' + cbFornecedorUsoConsumo.GetValor + '''');

    if cbFornecedorServico.GetValor <> _Biblioteca.cbNaoFiltrar then
      _Biblioteca.WhereOuAnd(vSql, 'FON.SERVICO = ''' + cbFornecedorServico.GetValor + '''');

    if FrDataCadastro.DatasValidas then
      _Biblioteca.WhereOuAnd(vSql, FrDataCadastro.getSqlFiltros('CAD.DATA_CADASTRO'));

    if FrDataInicialFinal.DatasValidas then
    begin
      _Biblioteca.WhereOuAnd(vSql,'extract(day from DATA_NASCIMENTO) between '
         +' extract(day from to_date('+ QuotedStr( FrDataInicialFinal.eDataInicial.Text+'/2000') + ', ''dd/mm/yyyy'')) and '
         +' extract(day from to_date('+ QuotedStr(FrDataInicialFinal.eDataFinal.Text+'/2000') +', ''dd/mm/yyyy'')) ');
     _Biblioteca.WhereOuAnd(vSql, 'extract(month from DATA_NASCIMENTO) between '
         +' extract(month from to_date('+ QuotedStr(FrDataInicialFinal.eDataInicial.Text+'/2000') +', ''dd/mm/yyyy'')) and '
         +' extract(month from to_date('+ QuotedStr(FrDataInicialFinal.eDataFinal.Text+'/2000') +', ''dd/mm/yyyy'')) ');

    end;
//      _Biblioteca.WhereOuAnd(vSql, FrDataInicialFinal.getSqlFiltros('CAD.DATA_NASCIMENTO'));
  end;

  vSql := vSql + 'order by CAD.NOME_FANTASIA ';

  vCadastros := _RelacaoCadastros.BuscarCadastros(Sessao.getConexaoBanco, vSql);

  if vCadastros = nil then begin
    NenhumRegistro;
    Exit;
  end;

   for i := Low(vCadastros) to High(vCadastros) do begin
    sgCadastros.Cells[coCodigo, i + 1]          := NFormat(vCadastros[i].CadastroId);
    sgCadastros.Cells[coNomeFantasia, i + 1]    := vCadastros[i].NomeFantasia;
    sgCadastros.Cells[coRazaoSocial, i + 1]     := vCadastros[i].RazaoSocial;
    sgCadastros.Cells[coCPFCNPJ, i + 1]         := vCadastros[i].CpfCnpj;
    sgCadastros.Cells[coLogradouro, i + 1]      := vCadastros[i].Logradouro;
    sgCadastros.Cells[coComplemento, i + 1]     := vCadastros[i].Complemento;
    sgCadastros.Cells[coBairro, i + 1]          := getInformacao(vCadastros[i].BairroId, vCadastros[i].NomeBairro);
    sgCadastros.Cells[coCidadeUF, i + 1]        := vCadastros[i].NomeCidade + '/' + vCadastros[i].EstadoId;
    sgCadastros.Cells[coEmail, i + 1]           := vCadastros[i].Email;
    sgCadastros.Cells[coECliente, i + 1]        := vCadastros[i].Ecliente;

    sgCadastros.Cells[coEFornecedor, i + 1]     := vCadastros[i].Efornecedor;
    sgCadastros.Cells[coEMotorista, i + 1]      := vCadastros[i].EMotorista;
    sgCadastros.Cells[coETransportadora, i + 1] := vCadastros[i].ETransportadora;
    sgCadastros.Cells[coEProfissional, i + 1]   := vCadastros[i].EProfissional;
    sgCadastros.Cells[coAtivoAnalitico, i + 1]  := vCadastros[i].AtivoAnalitico;
    sgCadastros.Cells[coAtivoSintetico, i + 1]  := vCadastros[i].AtivoSintetico;

    sgCadastros.Cells[coDataCadastro, i + 1]    := DFormat(vCadastros[i].DataCadastro);
    sgCadastros.Cells[coUsuarioCadastro, i + 1] := getInformacao(vCadastros[i].UsuarioSessaoId, vCadastros[i].NomeUsuario);
  end;
  sgCadastros.SetLinhasGridPorTamanhoVetor(Length(vCadastros));
  SetarFoco(sgCadastros);
end;

procedure TFormRelacaoCadastros.sgCadastrosDblClick(Sender: TObject);
begin
  inherited;
  InformacoesCadastro.Informar(SFormatInt(sgCadastros.Cells[coCodigo, sgCadastros.Row]));
end;

procedure TFormRelacaoCadastros.sgCadastrosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if _Biblioteca.Em(ACol, [coECliente, coEFornecedor, coEMotorista, coETransportadora, coEProfissional, coAtivoAnalitico]) then
    vAlinhamento := taCenter
  else if ACol = coCodigo then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgCadastros.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoCadastros.sgCadastrosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coAtivoAnalitico then begin
    AFont.Style := [fsBold];
    AFont.Color := _RelacaoCadastros.getCorAtivo(sgCadastros.Cells[coAtivoSintetico, ARow]);
  end;
end;

procedure TFormRelacaoCadastros.SpeedButton2Click(Sender: TObject);
begin
  inherited;
  GridToExcel(sgCadastros);
end;

end.
