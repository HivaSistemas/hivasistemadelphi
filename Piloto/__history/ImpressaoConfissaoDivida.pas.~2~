unit ImpressaoConfissaoDivida;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosGraficos, QRExport, _Biblioteca,
  QRPDFFilt, QRWebFilt, QRCtrls, QuickRpt, Vcl.ExtCtrls, _Sessao, _Orcamentos, _RecordsOrcamentosVendas,
  _RecordsCadastros, _Clientes, _ContasReceber, _RecordsFinanceiros;

type
  TFormImpressaoConfissaoDivida = class(TFormHerancaRelatoriosGraficos)
    QRShape4: TQRShape;
    qrbndDetailBand1: TQRBand;
    qrMeConfissao: TQRMemo;
    qrlCliente: TQRLabel;
    qrlCpfCnpjCliente: TQRLabel;
    QRShape2: TQRShape;
    QRShape1: TQRShape;
    qrEmpresaCredora: TQRLabel;
    qrCNPJEmpresaCredora: TQRLabel;
    qr8: TQRLabel;
    qrPedido: TQRLabel;
    qr9: TQRLabel;
    qrDataHoraRecebimento: TQRLabel;
    qr14: TQRLabel;
    qrVendedor: TQRLabel;
    qr2: TQRLabel;
    qrValorAcumulado: TQRLabel;
    qrCodigoCliente: TQRLabel;
    qr12: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure Imprimir(pPedidoId: Integer);

implementation

{$R *.dfm}

procedure Imprimir(pPedidoId: Integer);
var
  vForm: TFormImpressaoConfissaoDivida;
  vImpressora: RecImpressora;
  vOrcamento: TArray<RecOrcamentos>;
  vCondicaoPagamento: TArray<RecCondicoesPagamento>;

  vTexto: string;
  vEmpresa: RecEmpresas;
  vCadastro: RecClientes;

  vTitulos: TArray<RecContasReceber>;
begin
  if pPedidoId = 0 then
    Exit;

  vOrcamento := _Orcamentos.BuscarOrcamentos(Sessao.getConexaoBanco, 3, [pPedidoId]);
  if vOrcamento = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  if vOrcamento[0].Acumulado = 'N' then begin
    _Biblioteca.Exclamar('Este pedido n�o � um acumulado, impress�o da confiss�o de d�vida n�o permitida!');
    Exit;
  end;

  if vOrcamento[0].AcumuladoId > 0 then begin
    _Biblioteca.Exclamar('Este pedido � uma acumulado e j� teve seu fechamento realizado, impress�o da confiss�o de d�vida n�o permitida!');
    Exit;
  end;

  vImpressora := Sessao.getImpressora(toComprovantePagtoFinanceiro);
  if vImpressora.CaminhoImpressora = '' then begin
    _Biblioteca.ImpressoraNaoConfigurada;
    Exit;
  end;

  vForm := TFormImpressaoConfissaoDivida.Create(nil, vImpressora);

  vForm.PreencherCabecalho(vOrcamento[0].empresa_id, True);

  vEmpresa := Sessao.getEmpresa(vOrcamento[0].empresa_id);
  vForm.qrMeConfissao.Lines.Clear;

  vCadastro := _Clientes.BuscarClientes(Sessao.getConexaoBanco, 0, [vOrcamento[0].cliente_id], False)[0];
  vTitulos  := _ContasReceber.BuscarContasReceber(Sessao.getConexaoBanco, 8, [vOrcamento[0].orcamento_id]);

  // Credor
  vTexto :=
    'CREDOR: ' + vEmpresa.RazaoSocial + ', inscrita no CNPJ ' + vEmpresa.Cnpj + ', I.E ' + vEmpresa.InscricaoEstadual + ', estabelecida na ' + vEmpresa.Logradouro + ', ' +
    vEmpresa.Complemento + ', n� ' + vEmpresa.Numero + ', ' + vEmpresa.NomeBairro + ', ' + vEmpresa.NomeCidade + ' ' + vEmpresa.EstadoId + ';' + sLineBreak + sLineBreak;

  // Devedor
  if vCadastro.RecCadastro.tipo_pessoa = 'F' then begin
    vTexto := vTexto +
      'DEVEDOR: ' + vCadastro.RecCadastro.razao_social + ', brasileiro, ' + vCadastro.RecCadastro.estado_civil + ', portador do R.G. n� ' + vCadastro.RecCadastro.rg + ' e CPF/MF n� ' +
      vCadastro.RecCadastro.cpf_cnpj + ', residente e domiciliado no endere�o ' + vCadastro.RecCadastro.logradouro + ' ' + vCadastro.RecCadastro.complemento + ' n� ' + vCadastro.RecCadastro.numero + ', ' +
      vCadastro.RecCadastro.NomeBairro + ', ' + vCadastro.RecCadastro.cep + ', ' + vCadastro.RecCadastro.NomeCidade + ', ' + vCadastro.RecCadastro.estado_id;
  end
  else begin
    vTexto := vTexto +
      'DEVEDOR: ' + vCadastro.RecCadastro.razao_social + ', inscrita no CNPJ ' + vCadastro.RecCadastro.cpf_cnpj + ', I.E ' + vCadastro.RecCadastro.inscricao_estadual +
      ', estabelecida na ' + vCadastro.RecCadastro.logradouro + ' ' + vCadastro.RecCadastro.complemento + ' n� ' + vCadastro.RecCadastro.numero + ', ' +
      vCadastro.RecCadastro.NomeBairro + ', ' + vCadastro.RecCadastro.cep + ', ' + vCadastro.RecCadastro.NomeCidade + ', ' + vCadastro.RecCadastro.estado_id + ';';

  end;

  vTexto := vTexto + sLineBreak + sLineBreak +
    'Pelo presente instrumento e na melhor forma de direito, o DEVEDOR confessa e assume como l�quida e certa a d�vida a seguir descrita: ';

  vTexto := vTexto + sLineBreak + sLineBreak +
    'CL�USULA PRIMEIRA: Ressalvadas quaisquer outras obriga��es aqui n�o inclu�das, pelo presente instrumento e na melhor forma de direito, o DEVEDOR e AVALISTA confessam dever ao CREDOR a ' +
    'quantia l�quida, certa e exig�vel no valor de R$ ' + NFormat(vOrcamento[0].valor_total) + '( ' + _Biblioteca.getDinheiroPorExtenso(vOrcamento[0].valor_total) + ' ) ' + ', por ' +
    NFormat(Length(vTitulos)) + '( ' + getNumeroPorExtenso(Length(vTitulos)) + ' ) parcelas no valor de R$ ' + NFormat(vTitulos[0].ValorDocumento) + ' com o primeiro vencimento em ' +
    DFormat(vTitulos[0].data_vencimento) + ' e o �ltimo em ' + DFormat(vTitulos[0].data_vencimento) + '.';

  vTexto := vTexto + sLineBreak +
    'Par�grafo �nico: O n�o pagamento de qualquer parcela no seu vencimento, importar� no vencimento integral e antecipado do d�bito, sujeitando a DEVEDOR, al�m da execu��o do presente instrumento, ' +
    'ao pagamento do valor integral do d�bito, sobre o qual incidir� a aplica��o de multa de ' + NFormat(Sessao.getParametrosEmpresa.PercentualMulta) + '%, juros de mora de ' +
    NFormat(Sessao.getParametrosEmpresa.PercentualMulta) + ' % ao m�s e corre��o monet�ria e mais custas processuais e honor�rios advocat�cios na base de 20% sobre o valor total do d�bito.';

  vTexto := vTexto + sLineBreak + sLineBreak +
    'CL�USULA SEGUNDA: � D�VIDA ora reconhecida e assumida pelo DEVEDOR e AVALISTA, como l�quida, certa e exig�vel, no valor acima mencionado, aplica-se o disposto no artigo 585, II, do C�digo de ' +
    'Processo Civil Brasileiro,haja vista o car�ter de t�tulo executivo extrajudicial do presente instrumento de confiss�o de d�vida.';

  vTexto := vTexto + sLineBreak + sLineBreak +
    'CL�USULA TERCEIRA: A eventual toler�ncia � infring�ncia de qualquer das cl�usulas deste instrumento ou o n�o exerc�cio de qualquer direito nele previsto constituir� mera liberalidade, n�o implicando ' +
    'em nova��o ou transa��o de qualquer esp�cie.';

  vTexto := vTexto + sLineBreak + sLineBreak +
    'CL�USULA QUARTA: Para dirimir qualquer d�vida oriunda deste instrumento fica eleito o Foro de ' + vEmpresa.NomeCidade + ', com exclus�o de qualquer outro que seja.';

  vTexto := vTexto + sLineBreak + sLineBreak +
    'Isto posto, firma este instrumento em 2 (duas) vias de igual teor, na presen�a de duas testemunhas.';

  vForm.qrMeConfissao.Lines.Text := vTexto;

  vForm.qrEmpresaCredora.Caption := vEmpresa.RazaoSocial;
  vForm.qrCNPJEmpresaCredora.Caption := vEmpresa.Cnpj;

  vForm.qrlCliente.Caption        := vCadastro.RecCadastro.razao_social;
  vForm.qrlCpfCnpjCliente.Caption := vCadastro.RecCadastro.cpf_cnpj;
  vForm.qrPedido.Caption          := NFormatN(vOrcamento[0].orcamento_id);
  vForm.qrVendedor.Caption        := getInformacao(vOrcamento[0].vendedor_id, vOrcamento[0].nome_vendedor);
  vForm.qrCodigoCliente.Caption   := NFormat(vOrcamento[0].cliente_id);
  vForm.qrValorAcumulado.Caption  := NFormat(vOrcamento[0].ValorAcumulativo);
  vForm.qrDataHoraRecebimento.Caption := DHFormat(vOrcamento[0].data_hora_recebimento);

  vForm.Imprimir;

  vForm.Free;
end;

end.
