unit BuscarDadosRecebimentoPedido;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar,
  _Orcamentos, _RecordsOrcamentosVendas, _FrameHerancaPrincipal, FrameFechamento,
  Vcl.Buttons, Vcl.ExtCtrls, _Sessao, System.Math, _Biblioteca,
  _OrcamentosPagamentos, _OrcamentosPagamentosCheques, _RecordsEspeciais,
  _ComunicacaoTEF, Vcl.StdCtrls, EditLuka, Impressao.ComprovantePagamentoGrafico,
  _Retiradas, Emissao.BoletosBancario, CheckBoxLuka, _NotasFiscais,
  FrameTipoNotaGerar, ImpressaoComprovanteEntregaGrafico, BuscarDadosCartoesTEF,
  _RecordsCadastros, _CondicoesPagamento, _RecordsFinanceiros,
  ImpressaoComprovanteCartao, ImpressaoConfissaoDivida,
  ImpressaoMeiaPaginaDuplicataMercantil;

type
  TFormBuscarDadosRecebimentoPedido = class(TFormHerancaFinalizar)
    ckGerarCreditoTroco: TCheckBox;
    FrTipoNotaGerar: TFrTipoNotaGerar;
    FrDadosRecebimento: TFrFechamento;
    procedure FormShow(Sender: TObject);
    procedure FrDadosRecebimentosbBuscarDadosCartoesCreditoClick(Sender: TObject);
    procedure FrDadosRecebimentosbBuscarDadosCobrancaPixClick(Sender: TObject);
  private
    FOrcamento: RecOrcamentos;
    FClienteId: Integer;
    FQtdeMaximaParcelas: Integer;
    FCondicaoPagamento: RecCondicoesPagamento;
    FResultado: RecRetornoRecebimento;
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function BuscarDadosRecebimento(pOrcamento: RecOrcamentos): TRetornoTelaFinalizar<RecRetornoRecebimento>;

implementation

{$R *.dfm}

{ BuscarDadosRecebimentoPedido }

function BuscarDadosRecebimento(pOrcamento: RecOrcamentos): TRetornoTelaFinalizar<RecRetornoRecebimento>;
var
  vForm: TFormBuscarDadosRecebimentoPedido;
begin
  Result.Dados.EmitirDocumentoFiscal := False;
  if pOrcamento.orcamento_id = 0 then begin
    _Biblioteca.Exclamar('N�o foram encontrados os dados do or�amento!');
    Exit;
  end;

  vForm := TFormBuscarDadosRecebimentoPedido.Create(Application);

  vForm.FrDadosRecebimento.Modo(True);
  vForm.FOrcamento := pOrcamento;
  vForm.FClienteId := pOrcamento.cliente_id;
  vForm.FrDadosRecebimento.TelaChamada := ttRecebimento;
  vForm.FrDadosRecebimento.setUtilizaTef(Sessao.getParametrosEstacao.UtilizarTef = 'S');
  vForm.FrTipoNotaGerar.setDadosPrincipais(pOrcamento.cliente_id, pOrcamento.condicao_id, 0);

  vForm.FrDadosRecebimento.ValorFrete := pOrcamento.ValorFrete + pOrcamento.ValorFreteItens;

  vForm.FrDadosRecebimento.setCondicaoPagamento(pOrcamento.condicao_id, pOrcamento.nome_condicao_pagto, pOrcamento.PrazoMedioCondicaoPagto);

  // Se for para receber na entrega pode ser que uma parte j� tenha sido paga com cr�dito
  // ent�o vamos descontar o mesmo
  vForm.FrDadosRecebimento.setTotalProdutos(pOrcamento.valor_total_produtos - IIfDbl(pOrcamento.ReceberNaEntrega = 'S', pOrcamento.valor_credito));

  vForm.FrDadosRecebimento.eValorDesconto.AsCurr := pOrcamento.valor_desconto;
  vForm.FrDadosRecebimento.eValorDescontoChange(nil);

  vForm.FrDadosRecebimento.eValorOutrasDespesas.AsDouble := pOrcamento.valor_outras_despesas;
  vForm.FrDadosRecebimento.eValorOutrasDespesasChange(nil);

  vForm.FrDadosRecebimento.CartoesDebito  := _OrcamentosPagamentos.BuscarOrcamentosPagamentos(Sessao.getConexaoBanco, 0, [pOrcamento.orcamento_id, 'D']);
  vForm.FrDadosRecebimento.CartoesCredito := _OrcamentosPagamentos.BuscarOrcamentosPagamentos(Sessao.getConexaoBanco, 0, [pOrcamento.orcamento_id, 'C']);
  vForm.FrDadosRecebimento.Cobrancas      := _OrcamentosPagamentos.BuscarOrcamentosPagamentos(Sessao.getConexaoBanco, 1, [pOrcamento.orcamento_id]);
  vForm.FrDadosRecebimento.Cheques        := _OrcamentosPagamentosCheques.BuscarOrcamentosPagamentosCheques(Sessao.getConexaoBanco, 0, [pOrcamento.orcamento_id]);

  vForm.FrDadosRecebimento.eValorDinheiro.AsCurr      := pOrcamento.valor_dinheiro;
  vForm.FrDadosRecebimento.eValorCheque.AsCurr        := pOrcamento.valor_cheque;
  vForm.FrDadosRecebimento.eValorCartaoDebito.AsCurr  := pOrcamento.ValorCartaoDebito;
  vForm.FrDadosRecebimento.eValorCartaoCredito.AsCurr := pOrcamento.ValorCartaoCredito;
  vForm.FrDadosRecebimento.eValorCobranca.AsCurr      := pOrcamento.valor_cobranca;
  vForm.FrDadosRecebimento.eValorPix.AsCurr           := pOrcamento.valor_pix;
  vForm.FrDadosRecebimento.eValorAcumulativo.AsCurr   := pOrcamento.ValorAcumulativo;
  vForm.FrDadosRecebimento.eValorFinanceira.AsCurr    := pOrcamento.ValorFinanceira;
  vForm.FrDadosRecebimento.eValorCredito.AsCurr       := pOrcamento.valor_credito;

  vForm.FQtdeMaximaParcelas := _CondicoesPagamento.BuscarQtdePrazos(Sessao.getConexaoBanco, pOrcamento.condicao_id);
  vForm.FCondicaoPagamento  := _CondicoesPagamento.BuscarCondicoesPagamentos(Sessao.getConexaoBanco, 0, [pOrcamento.condicao_id], False, False)[0];

  if Result.Ok(vForm.ShowModal) then
    Result.Dados := vForm.FResultado;

  FreeAndNil(vForm);
end;

procedure TFormBuscarDadosRecebimentoPedido.Finalizar(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  vRetiradasIds: TArray<Integer>;
  vRetornoCartoes: TRetornoTelaFinalizar<TArray<RecRespostaTEF>>;
  vCartoes: TArray<RecTitulosFinanceiros>;
  vValorDinheiro: Double;
  vTemComprovanteTef: Boolean;

  vCreditosReceberNaEntrega: TArray<Integer>;
begin
  vCartoes := nil;
  vTemComprovanteTef := False;
  FResultado.EmitirDocumentoFiscal := False;

  vCreditosReceberNaEntrega := nil;
  if FOrcamento.ReceberNaEntrega = 'S' then
    vCreditosReceberNaEntrega := FrDadosRecebimento.Creditos;

  // Se houver TEF, chamando a tela que realizar� a passagem destes cart�es
  if (Sessao.getParametrosEstacao.UtilizarTef = 'S') and (FrDadosRecebimento.eValorCartaoDebito.AsCurr + FrDadosRecebimento.eValorCartaoCredito.AsCurr > 0) then begin
    vRetornoCartoes :=
      BuscarDadosCartoesTEF.BuscarPagamentosTEF(
        FrDadosRecebimento.eValorCartaoDebito.AsCurr,
        FrDadosRecebimento.eValorCartaoCredito.AsCurr,
        FQtdeMaximaParcelas,
        FrDadosRecebimento.eValorCartaoDebito.Enabled,
        FrDadosRecebimento.eValorCartaoCredito.Enabled
      );

    if vRetornoCartoes.BuscaCancelada then begin
      BuscarDadosCartoesTEF.CancelarCartoes(vRetornoCartoes.Dados);
      RotinaCanceladaUsuario;
      Abort;
    end;

    vCartoes := nil;
    SetLength(vCartoes, Length(vRetornoCartoes.Dados));
    for i := Low(vRetornoCartoes.Dados) to High(vRetornoCartoes.Dados) do begin
      vCartoes[i].CobrancaId        := vRetornoCartoes.Dados[i].CobrancaId;
      vCartoes[i].NsuTef            := vRetornoCartoes.Dados[i].Nsu;
      vCartoes[i].NumeroCartao      := vRetornoCartoes.Dados[i].NumeroCartao;
      vCartoes[i].CodigoAutorizacao := vRetornoCartoes.Dados[i].CodigoAutorizacao;
      vCartoes[i].ItemId            := i;
      vCartoes[i].Valor             := vRetornoCartoes.Dados[i].Valor;
      vCartoes[i].TipoRecebCartao   := vRetornoCartoes.Dados[i].TipoRecebimento;

      if not vTemComprovanteTef then
        vTemComprovanteTef := vRetornoCartoes.Dados[i].TipoRecebimento = 'TEF';
    end;
  end
  else
    vCartoes := FrDadosRecebimento.Cartoes;

  Sessao.getConexaoBanco.IniciarTransacao;

  vValorDinheiro := 0;
  if FrDadosRecebimento.eValorDinheiro.AsCurr > 0 then
    vValorDinheiro := IIfDbl(not ckGerarCreditoTroco.Checked, FrDadosRecebimento.eValorDinheiro.AsCurr - FrDadosRecebimento.eValorTroco.AsCurr, FrDadosRecebimento.eValorDinheiro.AsCurr);

  // Atualizando os pagamentos efetuados
  vRetBanco :=
    _Orcamentos.AtualizarValoresPagamentosOrcamento(
      Sessao.getConexaoBanco,
      FOrcamento.orcamento_id,
      FrDadosRecebimento.eValorOutrasDespesas.AsCurr,
      FrDadosRecebimento.eValorDesconto.AsCurr,
      vValorDinheiro,
      FrDadosRecebimento.eValorCheque.AsCurr,
      FrDadosRecebimento.eValorCartaoDebito.AsCurr,
      FrDadosRecebimento.eValorCartaoCredito.AsCurr,
      FrDadosRecebimento.eValorCobranca.AsCurr,
      FrDadosRecebimento.eValorAcumulativo.AsCurr,
      FrDadosRecebimento.eValorFinanceira.AsCurr,
      FrDadosRecebimento.eValorCredito.AsCurr + IIfDbl(FOrcamento.ReceberNaEntrega = 'S', FOrcamento.valor_credito),
      FrDadosRecebimento.eValorPix.AsCurr
    );

  Sessao.AbortarSeHouveErro(vRetBanco);
  // Atualizando os titulos a serem gerados no financeiro
  vRetBanco :=
    _Orcamentos.AtualizarOrcamentosPagamentos(
      Sessao.getConexaoBanco,
      FOrcamento.orcamento_id,
      FrDadosRecebimento.Cheques,
      vCartoes,
      FrDadosRecebimento.Cobrancas,
      nil,
      vCreditosReceberNaEntrega,
      True,
      nil
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  if Length(FrDadosRecebimento.FDadosPix) > 0 then begin
    vRetBanco := _Orcamentos.AtualizarOrcamentosPagamentosPix(
      Sessao.getConexaoBanco,
      FOrcamento.orcamento_id,
      FrDadosRecebimento.FDadosPix,
      FOrcamento.empresa_id,
      FOrcamento.cliente_id,
      Sessao.getTurnoCaixaAberto.TurnoId,
      Sessao.getData
    );

    Sessao.AbortarSeHouveErro(vRetBanco);
  end;

  // Gerando o financeiro, comiss�es e demais coisas pertinentes ao recebimento do pedido
  vRetBanco :=
    _Orcamentos.ReceberPedido(
      Sessao.getConexaoBanco,
      FOrcamento.orcamento_id,
      Sessao.getTurnoCaixaAberto.TurnoId,
      Sessao.getTurnoCaixaAberto.FuncionarioId,
      ckGerarCreditoTroco.Checked,
      IIfDbl(FrDadosRecebimento.eValorCredito.AsCurr > 0, 0, FrDadosRecebimento.eValorTroco.AsCurr),
      FrTipoNotaGerar.TipoNotaGerar
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  vRetiradasIds := _Retiradas.BuscarRetiradaIdAto(Sessao.getConexaoBanco, FOrcamento.orcamento_id);
  if FCondicaoPagamento.Acumulativo <> 'S' then begin
    if vRetiradasIds <> nil then begin
      vRetBanco := _Orcamentos.GerarNotaRetiraAto(Sessao.getConexaoBanco, vRetiradasIds, False);

      Sessao.AbortarSeHouveErro(vRetBanco);

      if FrTipoNotaGerar.ckEmitirNFCe.Checked or FrTipoNotaGerar.ckEmitirNFe.Checked then  begin
        FResultado.EmitirDocumentoFiscal := True;
        FResultado.TipoNota := FrTipoNotaGerar.TipoNotaGerar;
        FResultado.NotasFiscaisIds := vRetBanco.AsArrayInt;
      end;
    end;
  end;

  Sessao.getConexaoBanco.FinalizarTransacao;

  if FrDadosRecebimento.TemBoletoBancario then
    Emissao.BoletosBancario.Emitir(FOrcamento.orcamento_id, tbOrcamento);

  if FrDadosRecebimento.TemDuplicataMercantil then
    ImpressaoMeiaPaginaDuplicataMercantil.Emitir(FOrcamento.orcamento_id, tpOrcamento);

  if FrTipoNotaGerar.ckRecibo.Checked then
    Impressao.ComprovantePagamentoGrafico.Imprimir(FOrcamento.orcamento_id);

  if (Sessao.getParametros.ImprimirListaSeparacaoVenda = 'N') or (FOrcamento.TipoEntrega <> 'RA') then
    for i := Low(vRetiradasIds) to High(vRetiradasIds) do
      ImpressaoComprovanteEntregaGrafico.Imprimir( vRetiradasIds[i], tpRetirada );

//  if FOrcamento.Acumulado = 'S' then
//    ImpressaoConfissaoDivida.Imprimir(FOrcamento.orcamento_id);

  //Nova valida��o impress�o confiss�o d�vida
  if FCondicaoPagamento.ImprimirConfissaoDivida = 'S' then
    ImpressaoConfissaoDivida.Imprimir(FOrcamento.orcamento_id);

  // Imprimir as vias dos cart�es
  if (Sessao.getParametrosEstacao.UtilizarTef = 'S') and (vTemComprovanteTef) then begin
    for i := Low(vRetornoCartoes.Dados) to High(vRetornoCartoes.Dados) do
      ImpressaoComprovanteCartao.Imprimir(vRetornoCartoes.Dados[i].Comprovante1Via, vRetornoCartoes.Dados[i].Comprovante2Via);
  end;

  inherited;
end;

procedure TFormBuscarDadosRecebimentoPedido.FormShow(Sender: TObject);
begin
  inherited;
  FrDadosRecebimento.PermitirEditarValoresFormasPagamento(Sessao.getParametrosEmpresa.PermAltFormaPagtoCxFinanc = 'S');

  if FOrcamento.ReceberNaEntrega = 'S' then begin
    FrDadosRecebimento.CadastroId := FOrcamento.cliente_id;
    _Biblioteca.Habilitar([FrTipoNotaGerar.ckEmitirNFCe, FrTipoNotaGerar.ckEmitirNFe], False);
    FrTipoNotaGerar.ckRecibo.Checked := False;
  end
  else begin
    _Biblioteca.SomenteLeitura([FrDadosRecebimento.eValorCredito], True);
    _Biblioteca.Habilitar([FrDadosRecebimento.sbBuscarCreditos], False);
  end;

  SetarFoco(FrDadosRecebimento.eValorDinheiro);
end;

procedure TFormBuscarDadosRecebimentoPedido.FrDadosRecebimentosbBuscarDadosCartoesCreditoClick(Sender: TObject);
begin
  inherited;
  FrDadosRecebimento.sbBuscarDadosCartoesCreditoClick(Sender);
end;

procedure TFormBuscarDadosRecebimentoPedido.FrDadosRecebimentosbBuscarDadosCobrancaPixClick(
  Sender: TObject);
begin
  inherited;
  FrDadosRecebimento.sbBuscarDadosCobrancaPixClick(Sender);
end;

procedure TFormBuscarDadosRecebimentoPedido.VerificarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;

  if FrDadosRecebimento.eValorTroco.AsCurr < 0 then begin
    _Biblioteca.Exclamar('Existe diferen�a no valor a ser pago e as formas de pagamentos definidas!');
    SetarFoco(FrDadosRecebimento.eValorDinheiro);
    Abort;
  end;

  if (FrDadosRecebimento.eValorTroco.AsCurr > FrDadosRecebimento.eValorDinheiro.AsCurr) and (not ckGerarCreditoTroco.Checked) and (FrDadosRecebimento.eValorCredito.AsCurr = 0) then begin
    _Biblioteca.Exclamar('O valor do troco n�o pode ser maior que o valor do dinheiro sem que esteja marcado para gerar cr�dito!');
    SetarFoco(FrDadosRecebimento.eValorDinheiro);
    Abort;
  end;

  if (FrDadosRecebimento.eValorCheque.AsCurr > 0) and (FrDadosRecebimento.Cheques = nil) then begin
    _Biblioteca.Exclamar('Os cheques n�o foram definidos corretamente!');
    SetarFoco(FrDadosRecebimento.eValorCheque);
    Abort;
  end;

  FrDadosRecebimento.verificarCartoes;

  if (FrDadosRecebimento.eValorCobranca.AsCurr > 0) and (FrDadosRecebimento.Cobrancas = nil) then begin
    _Biblioteca.Exclamar('As cobran�as n�o foram definidas corretamente!');
    SetarFoco(FrDadosRecebimento.eValorCobranca);
    Abort;
  end;

  if (FrDadosRecebimento.eValorpIX.AsCurr > 0) and (FrDadosRecebimento.FDadosPix = nil) then begin
    _Biblioteca.Exclamar('As contas do PIX n�o foram definidas corretamente!');
    SetarFoco(FrDadosRecebimento.eValorpIX);
    Abort;
  end;

  if ckGerarCreditoTroco.Checked and (FClienteId = Sessao.getParametros.cadastro_consumidor_final_id) then begin
    _Biblioteca.Exclamar('N�o � permitido gerar cr�dito do troco para consumidor final!');
    SetarFoco(ckGerarCreditoTroco);
    Abort;
  end;

  if FrDadosRecebimento.getCreditoMaiorTotalPagarETemFormasPagamento then begin
    _Biblioteca.Exclamar('Os cr�ditos que est�o sendo utilizados j� cobrem o valor do pedido, n�o ser� poss�vel utilizar outras formas de pagamento!');
    SetarFoco(FrDadosRecebimento.eValorDinheiro);
    Abort;
  end;
//
//  if FrDadosRecebimento.getFormasPagamentoMaiorTotalPagarEUtilizandoCredito and (not ckGerarCreditoTroco.Checked) then begin
//    _Biblioteca.Exclamar('As formas de pagamento j� definidas � maior ou igual ao valor a ser pago pelo pedido, n�o ser� poss�vel utilizar cr�ditos!');
//    SetarFoco(FrDadosRecebimento.eValorCredito);
//    Abort;
//  end;

  FrTipoNotaGerar.VerificarRegistro;

  if FrTipoNotaGerar.ckEmitirNFe.Checked then begin
    vRetBanco := _NotasFiscais.PedidoPodeGerarNFe(Sessao.getConexaoBanco, FOrcamento.orcamento_id);
    if vRetBanco.TeveErro then begin
      _Biblioteca.Exclamar(vRetBanco.MensagemErro);
      Abort;
    end;
  end;
end;

end.

