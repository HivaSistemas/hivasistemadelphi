unit RelacaoVendasProdutos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, Vcl.ComCtrls, Vcl.Buttons,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameEmpresas, FrameProdutos, FrameMarcas, Frame.DepartamentosSecoesLinhas, FrameDataInicialFinal, FrameGruposFornecedores,
  FrameFornecedores, Vcl.Grids, GridLuka, Vcl.StdCtrls, StaticTextLuka, _RelacaoVendasProdutos, ImpressaoRelacaoVendasProdutosGrafico, SelecionarVariosOpcoes,
  ComboBoxLuka, FrameVendedores, CheckBoxLuka;

type
  TFormRelacaoVendasProdutos = class(TFormHerancaRelatoriosPageControl)
    FrEmpresas: TFrEmpresas;
    FrProdutos: TFrProdutos;
    FrMarcas: TFrMarcas;
    FrDepartamentosSecoesLinhas: TFrameDepartamentosSecoesLinhas;
    FrFornecedores: TFrFornecedores;
    FrGruposFornecedores: TFrGruposFornecedores;
    FrDataRecebimento: TFrDataInicialFinal;
    StaticTextLuka1: TStaticTextLuka;
    StaticTextLuka2: TStaticTextLuka;
    sgProdutos: TGridLuka;
    spSeparador: TSplitter;
    sgVendasDevolucoes: TGridLuka;
    lb1: TLabel;
    cbAgrupamento: TComboBoxLuka;
    FrVendedores: TFrVendedores;
    procedure FormCreate(Sender: TObject);
    procedure sgProdutosClick(Sender: TObject);
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgVendasDevolucoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgVendasDevolucoesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgVendasDevolucoesDblClick(Sender: TObject);
  private
    FVendasProdutos: TArray<RecRelacaoVendasProdutos>;
  protected
    procedure Carregar(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

uses
  _Biblioteca, _Sessao, Informacoes.Orcamento, Informacoes.Devolucao;

const
  {Grid de produtos}
  coProdutoId       = 0;
  coNome            = 1;
  coMarca           = 2;
  coQtdVendida      = 3;
  coQtdDevolvida    = 4;
  coUndVenda        = 5;
  coValorVenda      = 6;
  coValorDevolucoes = 7;
  {Ocultas}
  coVendedorId      = 8;
  coTipoLinha       = 9;

  {Grid de vendas/devolu��es}
  cvTipoMovimento = 0;
  cvMovimentoId   = 1;
  cvDataMovimento = 2;
  cvQuantidade    = 3;
  cvValor         = 4;

  {Tipos de linha}
  clLinhaDetalhe     = 'D';
  clLinhaTotVenda    = 'TV';
  clLinhaTotVendedor = 'TVEN';
  clLinhaAgrupamento = 'AGR';

  caAgrupadoVendedor = 'VEN';

procedure TFormRelacaoVendasProdutos.Carregar(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vComando: string;

  vUltimoVendedorId: Integer;

  vProdutoId: Integer;
  vQtdVendida: Double;
  vQtdDevolvida: Double;
  vValorVenda: Double;
  vValorDevolucoes: Double;
  vTrocouVendedor: Boolean;

  vTotais: record
    TotalQtdVendida: Double;
    TotalQtdDevolvida: Double;
    TotalValorVenda: Double;
    TotalValorDevolucoes: Double;
  end;

  vTotaisVendedor: record
    TotalQtdVendida: Double;
    TotalQtdDevolvida: Double;
    TotalValorVenda: Double;
    TotalValorDevolucoes: Double;
  end;

begin
  inherited;
  sgProdutos.ClearGrid;
  sgVendasDevolucoes.ClearGrid;

  if not FrEmpresas.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrEmpresas.getSqlFiltros('EMPRESA_ID'));

  if not FrProdutos.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrProdutos.getSqlFiltros('PRODUTO_ID'));

  if not FrMarcas.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrMarcas.getSqlFiltros('MARCA_ID'));

  if not FrDepartamentosSecoesLinhas.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrDepartamentosSecoesLinhas.getSqlFiltros('LINHA_PRODUTO_ID'));

  if not FrFornecedores.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrFornecedores.getSqlFiltros('FORNECEDOR_ID'));

  if not FrGruposFornecedores.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrGruposFornecedores.getSqlFiltros('GRUPO_FORNECEDOR_ID'));

  if not FrDataRecebimento.NenhumaDataValida then
    _Biblioteca.WhereOuAnd(vComando, FrDataRecebimento.getSqlFiltros('DATA_HORA_MOVIMENTO'));

  if not FrVendedores.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrVendedores.getSqlFiltros('VENDEDOR_ID'));

  if cbAgrupamento.GetValor = caAgrupadoVendedor then
    vComando := vComando + 'order by NOME_VENDEDOR, NOME_PRODUTO, DATA_HORA_MOVIMENTO '
  else
    vComando := vComando + 'order by NOME_PRODUTO, DATA_HORA_MOVIMENTO ';

  FVendasProdutos := _RelacaoVendasProdutos.BuscarVendasProdutos(Sessao.getConexaoBanco, vComando);
  if FVendasProdutos = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vLinha           := 0;
  vProdutoId       := 0;
  vQtdVendida      := 0;
  vQtdDevolvida    := 0;
  vValorVenda      := 0;
  vValorDevolucoes := 0;

  vUltimoVendedorId := -1;
  for i := Low(FVendasProdutos) to High(FVendasProdutos) do begin
    vTrocouVendedor := False;
    if cbAgrupamento.GetValor = caAgrupadoVendedor then begin
      if vUltimoVendedorId <> FVendasProdutos[i].VendedorId then begin
        if vUltimoVendedorId > -1 then begin
          sgProdutos.Cells[coQtdVendida, vLinha]      := NFormatN(vQtdVendida);
          sgProdutos.Cells[coQtdDevolvida, vLinha]    := NFormatN(vQtdDevolvida);
          sgProdutos.Cells[coValorVenda, vLinha]      := NFormatN(vValorVenda);
          sgProdutos.Cells[coValorDevolucoes, vLinha] := NFormatN(vValorDevolucoes);

          vTotais.TotalQtdVendida      := vTotais.TotalQtdVendida + vQtdVendida;
          vTotais.TotalQtdDevolvida    := vTotais.TotalQtdDevolvida + vQtdDevolvida;
          vTotais.TotalValorVenda      := vTotais.TotalValorVenda + vValorVenda;
          vTotais.TotalValorDevolucoes := vTotais.TotalValorDevolucoes + vValorDevolucoes;

          vTotaisVendedor.TotalQtdVendida      := vTotaisVendedor.TotalQtdVendida + vQtdVendida;
          vTotaisVendedor.TotalQtdDevolvida    := vTotaisVendedor.TotalQtdDevolvida + vQtdDevolvida;
          vTotaisVendedor.TotalValorVenda      := vTotaisVendedor.TotalValorVenda + vValorVenda;
          vTotaisVendedor.TotalValorDevolucoes := vTotaisVendedor.TotalValorDevolucoes + vValorDevolucoes;

          Inc(vLinha);

          sgProdutos.Cells[coMarca, vLinha]           := 'TOT.VENDEDOR:';
          sgProdutos.Cells[coQtdVendida, vLinha]      := NFormatN(vTotaisVendedor.TotalQtdVendida);
          sgProdutos.Cells[coQtdDevolvida, vLinha]    := NFormatN(vTotaisVendedor.TotalQtdDevolvida);
          sgProdutos.Cells[coValorVenda, vLinha]      := NFormatN(vTotaisVendedor.TotalValorVenda);
          sgProdutos.Cells[coValorDevolucoes, vLinha] := NFormatN(vTotaisVendedor.TotalValorDevolucoes);
          sgProdutos.Cells[coTipoLinha, vLinha]       := clLinhaTotVendedor;

          vQtdVendida      := 0;
          vQtdDevolvida    := 0;
          vValorVenda      := 0;
          vValorDevolucoes := 0;

          vTotaisVendedor.TotalQtdVendida      := 0;
          vTotaisVendedor.TotalQtdDevolvida    := 0;
          vTotaisVendedor.TotalValorVenda      := 0;
          vTotaisVendedor.TotalValorDevolucoes := 0;

          Inc(vLinha);
        end;

        Inc(vLinha);

        vTrocouVendedor   := True;
        vUltimoVendedorId := FVendasProdutos[i].VendedorId;

        sgProdutos.Cells[coProdutoId, vLinha]  := getInformacao(FVendasProdutos[i].VendedorId, FVendasProdutos[i].NomeVendedor);
        sgProdutos.Cells[coTipoLinha, vLinha]  := clLinhaAgrupamento;
        sgProdutos.Cells[coVendedorId, vLinha] := NFormat(FVendasProdutos[i].VendedorId);
      end;
    end;

    if
      (FVendasProdutos[i].ProdutoId <> vProdutoId) or
      ((cbAgrupamento.GetValor = caAgrupadoVendedor) and vTrocouVendedor)
    then begin
      vProdutoId := FVendasProdutos[i].ProdutoId;

      if i > 0 then begin
        sgProdutos.Cells[coQtdVendida, vLinha]      := NFormatN(vQtdVendida);
        sgProdutos.Cells[coQtdDevolvida, vLinha]    := NFormatN(vQtdDevolvida);
        sgProdutos.Cells[coValorVenda, vLinha]      := NFormatN(vValorVenda);
        sgProdutos.Cells[coValorDevolucoes, vLinha] := NFormatN(vValorDevolucoes);

        vTotais.TotalQtdVendida      := vTotais.TotalQtdVendida + vQtdVendida;
        vTotais.TotalQtdDevolvida    := vTotais.TotalQtdDevolvida + vQtdDevolvida;
        vTotais.TotalValorVenda      := vTotais.TotalValorVenda + vValorVenda;
        vTotais.TotalValorDevolucoes := vTotais.TotalValorDevolucoes + vValorDevolucoes;

        vTotaisVendedor.TotalQtdVendida      := vTotaisVendedor.TotalQtdVendida + vQtdVendida;
        vTotaisVendedor.TotalQtdDevolvida    := vTotaisVendedor.TotalQtdDevolvida + vQtdDevolvida;
        vTotaisVendedor.TotalValorVenda      := vTotaisVendedor.TotalValorVenda + vValorVenda;
        vTotaisVendedor.TotalValorDevolucoes := vTotaisVendedor.TotalValorDevolucoes + vValorDevolucoes;
      end;

      Inc(vLinha);

      sgProdutos.Cells[coProdutoId, vLinha] := NFormat(vProdutoId);
      sgProdutos.Cells[coNome, vLinha]      := FVendasProdutos[i].NomeProduto;
      sgProdutos.Cells[coMarca, vLinha]     := FVendasProdutos[i].NomeMarca;
      sgProdutos.Cells[coUndVenda, vLinha]  := FVendasProdutos[i].UnidadeVenda;
      sgProdutos.Cells[coTipoLinha, vLinha] := clLinhaDetalhe;

      vQtdVendida      := 0;
      vQtdDevolvida    := 0;
      vValorVenda      := 0;
      vValorDevolucoes := 0;
    end;

    if FVendasProdutos[i].TipoMovimento = 'VEN' then begin
      vQtdVendida      := vQtdVendida + FVendasProdutos[i].Quantidade;
      vValorVenda      := vValorVenda + FVendasProdutos[i].ValorLiquido;
    end
    else begin
      vQtdDevolvida    := vQtdDevolvida + FVendasProdutos[i].Quantidade;
      vValorDevolucoes := vValorDevolucoes + FVendasProdutos[i].ValorLiquido;
    end;
  end;

  sgProdutos.Cells[coQtdVendida, vLinha]      := NFormatN(vQtdVendida);
  sgProdutos.Cells[coQtdDevolvida, vLinha]    := NFormatN(vQtdDevolvida);
  sgProdutos.Cells[coValorVenda, vLinha]      := NFormatN(vValorVenda);
  sgProdutos.Cells[coValorDevolucoes, vLinha] := NFormatN(vValorDevolucoes);

  Inc(vLinha);

  vTotais.TotalQtdVendida      := vTotais.TotalQtdVendida + vQtdVendida;
  vTotais.TotalQtdDevolvida    := vTotais.TotalQtdDevolvida + vQtdDevolvida;
  vTotais.TotalValorVenda      := vTotais.TotalValorVenda + vValorVenda;
  vTotais.TotalValorDevolucoes := vTotais.TotalValorDevolucoes + vValorDevolucoes;

  if cbAgrupamento.GetValor = caAgrupadoVendedor then begin
    sgProdutos.Cells[coMarca, vLinha]           := 'TOT.VENDEDOR:';
    sgProdutos.Cells[coQtdVendida, vLinha]      := NFormatN(vTotaisVendedor.TotalQtdVendida + vQtdVendida);
    sgProdutos.Cells[coQtdDevolvida, vLinha]    := NFormatN(vTotaisVendedor.TotalQtdDevolvida + vQtdDevolvida);
    sgProdutos.Cells[coValorVenda, vLinha]      := NFormatN(vTotaisVendedor.TotalValorVenda + vValorVenda);
    sgProdutos.Cells[coValorDevolucoes, vLinha] := NFormatN(vTotaisVendedor.TotalValorDevolucoes + vValorDevolucoes);
    sgProdutos.Cells[coTipoLinha, vLinha]       := clLinhaTotVendedor;


    vTotaisVendedor.TotalQtdVendida      := 0;
    vTotaisVendedor.TotalQtdDevolvida    := 0;
    vTotaisVendedor.TotalValorVenda      := 0;
    vTotaisVendedor.TotalValorDevolucoes := 0;

    Inc(vLinha);
    Inc(vLinha);
  end;

  sgProdutos.Cells[coMarca, vLinha]           := 'TOTAIS:';
  sgProdutos.Cells[coQtdVendida, vLinha]      := NFormatN(vTotais.TotalQtdVendida);
  sgProdutos.Cells[coQtdDevolvida, vLinha]    := NFormatN(vTotais.TotalQtdDevolvida);
  sgProdutos.Cells[coValorVenda, vLinha]      := NFormatN(vTotais.TotalValorVenda);
  sgProdutos.Cells[coValorDevolucoes, vLinha] := NFormatN(vTotais.TotalValorDevolucoes);
  sgProdutos.Cells[coTipoLinha, vLinha]       := clLinhaTotVenda;

  Inc(vLinha);

  sgProdutos.RowCount := vLinha;
  sgProdutosClick(nil);
  SetarFoco(sgProdutos);
end;

procedure TFormRelacaoVendasProdutos.FormCreate(Sender: TObject);
begin
  inherited;
  ActiveControl := FrEmpresas.sgPesquisa;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId, False);
end;

procedure TFormRelacaoVendasProdutos.Imprimir(Sender: TObject);
var
  i: Integer;
  vUltimoVendedorId: Integer;
  vDados: TArray<RecDadosImpressao>;
  vOpcao: TRetornoTelaFinalizar<Integer>;
begin
  inherited;

  if cbAgrupamento.GetValor <> caAgrupadoVendedor then begin
    _Biblioteca.ImpressaoNaoDisponivel;
    Exit;
  end;

  vOpcao := SelecionarVariosOpcoes.Selecionar('Tipo relat�rio', ['Sint�tico por vendedor', 'Anal�tico'], 0);
  if vOpcao.BuscaCancelada then
    Exit;

  vDados := nil;
  vUltimoVendedorId := -1;
  for i := 1 to sgProdutos.RowCount -1 do begin
    if sgProdutos.Cells[coTipoLinha, i] = clLinhaAgrupamento then begin
      if vUltimoVendedorId <> SFormatInt(sgProdutos.Cells[coVendedorId, i]) then begin
        SetLength(vDados, Length(vDados) + 1);
        vDados[High(vDados)].Vendedor := sgProdutos.Cells[coProdutoid, i];

        vUltimoVendedorId := SFormatInt(sgProdutos.Cells[coVendedorId, i]);

        Continue;
      end;
    end;

    // Se for a linha de totalizacao
    if sgProdutos.Cells[coTipoLinha, i] = clLinhaTotVendedor then begin
      vDados[High(vDados)].QtdeTotal    := SFormatDouble(sgProdutos.Cells[coQtdVendida, i]) - SFormatDouble(sgProdutos.Cells[coQtdDevolvida, i]);
      vDados[High(vDados)].TotalLiquido := SFormatDouble(sgProdutos.Cells[coValorVenda, i]) - SFormatDouble(sgProdutos.Cells[coValorDevolucoes, i]);

      Continue;
    end;

    if sgProdutos.Cells[coTipoLinha, i] <> clLinhaDetalhe then
      Continue;

    SetLength(vDados[High(vDados)].Produtos, Length(vDados[High(vDados)].Produtos) + 1);
    vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].ProdutoId    := SFormatInt(sgProdutos.Cells[coProdutoId, i]);
    vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].NomeProduto  := sgProdutos.Cells[coNome, i];
    vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].NomeMarca    := sgProdutos.Cells[coMarca, i];
    vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].Quantidade   := SFormatDouble(sgProdutos.Cells[coQtdVendida, i]) - SFormatDouble(sgProdutos.Cells[coQtdDevolvida, i]);
    vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].Unidade      := sgProdutos.Cells[coUndVenda, i];
    vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].ValorLiquido := SFormatDouble(sgProdutos.Cells[coValorVenda, i]) - SFormatDouble(sgProdutos.Cells[coValorDevolucoes, i]);
  end;

  ImpressaoRelacaoVendasProdutosGrafico.Imprimir(Self, vOpcao.Dados, vDados, FFiltrosUtilizados);
end;

procedure TFormRelacaoVendasProdutos.sgProdutosClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
begin
  inherited;
  sgVendasDevolucoes.ClearGrid;

  vLinha := 0;
  for i := Low(FVendasProdutos) to High(FVendasProdutos) do begin
    if FVendasProdutos[i].ProdutoId <> SFormatInt(sgProdutos.Cells[coProdutoId, sgProdutos.Row]) then
      Continue;

    Inc(vLinha);

    sgVendasDevolucoes.Cells[cvTipoMovimento, vLinha] := FVendasProdutos[i].TipoMovimento;
    sgVendasDevolucoes.Cells[cvMovimentoId, vLinha]   := NFormat(FVendasProdutos[i].MovimentoId);
    sgVendasDevolucoes.Cells[cvDataMovimento, vLinha] := DFormatN(FVendasProdutos[i].DataMovimento);
    sgVendasDevolucoes.Cells[cvQuantidade, vLinha]    := NFormatN(FVendasProdutos[i].Quantidade);
    sgVendasDevolucoes.Cells[cvValor, vLinha]         := NFormatN(FVendasProdutos[i].ValorLiquido);
  end;

  sgVendasDevolucoes.SetLinhasGridPorTamanhoVetor(vLinha);
end;

procedure TFormRelacaoVendasProdutos.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in [coProdutoId, coQtdVendida, coQtdDevolvida, coValorVenda, coValorDevolucoes] then
    vAlinhamento := taRightJustify
  else if ACol = coUndVenda then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  if sgProdutos.Cells[coTipoLinha, ARow] = clLinhaAgrupamento then
    sgProdutos.MergeCells([ARow, ACol], [ARow, coProdutoId], [ARow, coValorDevolucoes], taLeftJustify, Rect)
  else
    sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoVendasProdutos.sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgProdutos.Cells[coTipoLinha, ARow] = clLinhaAgrupamento then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end
  else if sgProdutos.Cells[coTipoLinha, ARow] = clLinhaTotVendedor then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao1;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao1;
  end
  else if sgProdutos.Cells[coTipoLinha, ARow] = clLinhaTotVenda then begin
    AFont.Color := clWhite;
    AFont.Style := [fsBold];
    ABrush.Color := clTeal;
  end
  else if ACol in [coQtdVendida, coValorVenda] then
    AFont.Color := clBlue
  else if ACol in [coQtdDevolvida, coValorDevolucoes] then
    AFont.Color := clRed;
end;

procedure TFormRelacaoVendasProdutos.sgVendasDevolucoesDblClick(Sender: TObject);
begin
  inherited;
  if sgVendasDevolucoes.Cells[cvTipoMovimento, sgVendasDevolucoes.Row] = 'VEN' then
    Informacoes.Orcamento.Informar(SFormatInt(sgVendasDevolucoes.Cells[cvMovimentoId, sgVendasDevolucoes.Row]))
  else
    Informacoes.Devolucao.Informar(SFormatInt(sgVendasDevolucoes.Cells[cvMovimentoId, sgVendasDevolucoes.Row]));
end;

procedure TFormRelacaoVendasProdutos.sgVendasDevolucoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in [cvMovimentoId, cvQuantidade, cvValor] then
    vAlinhamento := taRightJustify
  else if ACol = cvTipoMovimento then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgVendasDevolucoes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoVendasProdutos.sgVendasDevolucoesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = cvTipoMovimento then begin
    AFont.Style := [fsBold];
    AFont.Color := IIf(sgVendasDevolucoes.Cells[cvTipoMovimento, ARow] = 'VEN', clBlue, clRed);
  end;
end;

end.
