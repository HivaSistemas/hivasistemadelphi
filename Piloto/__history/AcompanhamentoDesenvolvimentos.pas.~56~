unit AcompanhamentoDesenvolvimentos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatorios, Vcl.Buttons, _HerancaRelatoriosPageControl,
  Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.Grids, GridLuka, _Biblioteca, _RecordsCadastros, System.Math,
  _FrameHerancaPrincipal, FrameDataInicialFinal, _CadastroDesenvolvimento, _Sessao, Vcl.StdCtrls,
  VCLTee.TeCanvas, Vcl.Menus, _RecordsEspeciais, FrameTextoFormatado, FrameEdicaoTexto, GroupBoxLuka,
  FrameNumeros, Logs, CheckBoxLuka;

type
  TFormAcompanhamentoDesenvolvimentos = class(TFormHerancaRelatoriosPageControl)
    FrDataInicialFinal: TFrDataInicialFinal;
    pnDesenvolver: TPanel;
    sgDesenvolvimentos: TGridLuka;
    pcStatus: TPageControl;
    tsDesenvolvimento: TTabSheet;
    tsAnalise: TTabSheet;
    sp: TSplitter;
    pmOpcoesDesenvolvimento: TPopupMenu;
    miAnalisar: TMenuItem;
    miDesenvolver: TMenuItem;
    miTestar: TMenuItem;
    miDocumentar: TMenuItem;
    miFinalizar: TMenuItem;
    frDesenvolvimento: TFrEdicaoTexto;
    Panel2: TPanel;
    frAnalise: TFrEdicaoTexto;
    tsTestes: TTabSheet;
    tsDocumentacao: TTabSheet;
    frTestes: TFrEdicaoTexto;
    frDocumentacao: TFrEdicaoTexto;
    tsLiberacao: TTabSheet;
    frLiberacao: TFrEdicaoTexto;
    sbGravar: TSpeedButton;
    gbStatus: TGroupBoxLuka;
    ckEA: TCheckBox;
    ckED: TCheckBox;
    ckET: TCheckBox;
    FrCodigoSolicitacao: TFrNumeros;
    sbLogs: TSpeedButton;
    grp1: TGroupBoxLuka;
    ck1: TCheckBox;
    ck2: TCheckBox;
    ck3: TCheckBox;
    ck4: TCheckBox;
    ck6: TCheckBox;
    grp2: TGroupBoxLuka;
    ckUrgente: TCheckBox;
    ckAlta: TCheckBox;
    ckBaixa: TCheckBox;
    ckMedia: TCheckBox;
    ckEDO: TCheckBox;
    ckFI: TCheckBox;
    tsHistorico: TTabSheet;
    frHistorico: TFrEdicaoTexto;
    procedure sgDesenvolvimentosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgDesenvolvimentosClick(Sender: TObject);
    procedure sgDesenvolvimentosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure miAnalisarClick(Sender: TObject);
    procedure miDesenvolverClick(Sender: TObject);
    procedure miDocumentarClick(Sender: TObject);
    procedure miFinalizarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure miTestarClick(Sender: TObject);
    procedure sbGravarClick(Sender: TObject);
    procedure sbLogsClick(Sender: TObject);
    procedure frAnaliseExit(Sender: TObject);
    procedure frDesenvolvimentoExit(Sender: TObject);
    procedure frDocumentacaoExit(Sender: TObject);
    procedure frTestesExit(Sender: TObject);
    procedure frLiberacaoExit(Sender: TObject);
  private
    procedure MudarStatusDesenvolvimento(pNovoStatus: string);
  protected
    procedure Carregar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

const
  de_codigo                     = 0;
  de_data_cadastro              = 1;
  de_tipo_solicitacao           = 2;
  de_prioridade_analitica       = 3;
  de_status_analitico           = 4;

  de_prioridade                 = 5;
  de_status                     = 6;
  de_texto_historico            = 7;
  de_texto_analise              = 8;
  de_texto_desenvolvimento      = 9;
  de_texto_testes               = 10;
  de_texto_documentacao         = 11;
  de_texto_liberacao            = 12;

procedure TFormAcompanhamentoDesenvolvimentos.FormCreate(Sender: TObject);
begin
  inherited;
  frAnalise.Modo(False);
  frDesenvolvimento.Modo(False);
  frTestes.Modo(False);
  frDocumentacao.Modo(False);
  frLiberacao.Modo(False);
  frHistorico.Modo(False);

  pcStatus.ActivePage := tsHistorico;
end;


procedure TFormAcompanhamentoDesenvolvimentos.frAnaliseExit(Sender: TObject);
begin
  inherited;
  sgDesenvolvimentos.Cells[de_texto_analise, sgDesenvolvimentos.Row] := frAnalise.GetTexto;
end;


procedure TFormAcompanhamentoDesenvolvimentos.frDesenvolvimentoExit(
  Sender: TObject);
begin
  inherited;
  sgDesenvolvimentos.Cells[de_texto_desenvolvimento, sgDesenvolvimentos.Row] := frDesenvolvimento.GetTexto;
end;


procedure TFormAcompanhamentoDesenvolvimentos.frDocumentacaoExit(
  Sender: TObject);
begin
  inherited;
  sgDesenvolvimentos.Cells[de_texto_documentacao, sgDesenvolvimentos.Row] := FrDocumentacao.GetTexto;
end;


procedure TFormAcompanhamentoDesenvolvimentos.frLiberacaoExit(Sender: TObject);
begin
  inherited;
  sgDesenvolvimentos.Cells[de_texto_testes, sgDesenvolvimentos.Row] := frLiberacao.GetTexto;
end;

procedure TFormAcompanhamentoDesenvolvimentos.frTestesExit(Sender: TObject);
begin
  inherited;
  sgDesenvolvimentos.Cells[de_texto_testes, sgDesenvolvimentos.Row] := FrTestes.GetTexto;
end;

procedure TFormAcompanhamentoDesenvolvimentos.miAnalisarClick(Sender: TObject);
begin
  inherited;
  MudarStatusDesenvolvimento('EA');
end;

procedure TFormAcompanhamentoDesenvolvimentos.miDesenvolverClick(Sender: TObject);
begin
  inherited;
  MudarStatusDesenvolvimento('ED');
end;

procedure TFormAcompanhamentoDesenvolvimentos.miDocumentarClick(
  Sender: TObject);
begin
  inherited;
  MudarStatusDesenvolvimento('DC');
end;

procedure TFormAcompanhamentoDesenvolvimentos.miFinalizarClick(Sender: TObject);
begin
  inherited;
  MudarStatusDesenvolvimento('FI');
end;

procedure TFormAcompanhamentoDesenvolvimentos.miTestarClick(Sender: TObject);
begin
  inherited;
  MudarStatusDesenvolvimento('ET');
end;

procedure TFormAcompanhamentoDesenvolvimentos.MudarStatusDesenvolvimento(pNovoStatus: string);
var
  vDesenvolvimentoId: Integer;
  vRetorno: RecRetornoBD;
  vLinha: Integer;
begin
  vDesenvolvimentoId := StrToIntDef(sgDesenvolvimentos.Cells[de_codigo, sgDesenvolvimentos.Row], 0);
  if vDesenvolvimentoId = 0 then
    Exit;

  vRetorno :=
    _CadastroDesenvolvimento.AtualizarStatusDesenvolvimento(
      Sessao.getConexaoBanco,
      vDesenvolvimentoId,
      pNovoStatus
    );

  if vRetorno.TeveErro then begin
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Exit;
  end;

  vLinha := sgDesenvolvimentos.Row;

  Carregar(nil);

  sgDesenvolvimentos.Row := vLinha;
  sgDesenvolvimentosClick(nil);
end;

procedure TFormAcompanhamentoDesenvolvimentos.Carregar(Sender: TObject);
var
  i: Integer;
  vComando: string;
  vDesenvolvimentos: TArray<RecDesenvolvimento>;

begin
  inherited;
  sgDesenvolvimentos.ClearGrid;

  if not FrCodigoSolicitacao.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrCodigoSolicitacao.TrazerFiltros('DESENVOLVIMENTO_ID'));

  if FrDataInicialFinal.DatasValidas then
    vComando := FrDataInicialFinal.getSqlFiltros('DATA_CADASTRO');

  if (ckEA.checked) or (ckED.checked) or (ckET.checked) or (ckEDO.checked) or (ckFI.checked) then begin
    _Biblioteca.WhereOuAnd(vComando, ' STATUS in(');

    if ckEA.Checked then
      vComando := vComando  + '''EA'', ';

    if ckED.Checked then
      vComando := vComando + '''ED'', ';

    if ckET.Checked then
      vComando := vComando + '''ET'', ';

    if ckEDO.Checked then
      vComando := vComando + '''DC'', ';

    if ckFI.Checked then
      vComando := vComando + '''FI'', ';

    Delete(vComando, Length(vComando) - 1, 2);
    vComando := vComando + ') ';
  end;

  if (ckUrgente.checked) or (ckAlta.checked) or (ckMedia.checked) or (ckBaixa.checked) then begin
    _Biblioteca.WhereOuAnd(vComando, ' PRIORIDADE in(');

    if ckUrgente.Checked then
      vComando := vComando  + '''U'', ';

    if ckAlta.Checked then
      vComando := vComando + '''A'', ';

    if ckMedia.Checked then
      vComando := vComando + '''M'', ';

    if ckBaixa.Checked then
      vComando := vComando + '''B'', ';

    Delete(vComando, Length(vComando) - 1, 2);
    vComando := vComando + ') ';
  end;

  vComando := vComando + ' order by DESENVOLVIMENTO_ID ';

  vDesenvolvimentos := _CadastroDesenvolvimento.BuscarDesenvolvimentosComando(Sessao.getConexaoBanco, vComando);
  if vDesenvolvimentos = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    Exit;
  end;

  for i := Low(vDesenvolvimentos) to High(vDesenvolvimentos) do begin
    sgDesenvolvimentos.Cells[de_codigo, i + 1]        := NFormat(vDesenvolvimentos[i].desenvolvimento_id);
    sgDesenvolvimentos.Cells[de_data_cadastro, i + 1] := DateToStr(vDesenvolvimentos[i].data_cadastro);

    sgDesenvolvimentos.Cells[de_tipo_solicitacao, i + 1] :=
      _Biblioteca.Decode(
        vDesenvolvimentos[i].tipo_solicitacao,
        ['CE', 'Corre��o de Erro',
         'ND', 'Novo Desenvolvimento',
         'GD', 'Gerar Documenta��o',
         'AP', 'Analise de Procedimento',
         'OS', 'Ordem Servi�o']);

    sgDesenvolvimentos.Cells[de_prioridade_analitica, i + 1] := _Biblioteca.Decode(vDesenvolvimentos[i].prioridade, ['U', 'Urgente','A', 'Alta', 'M', 'M�dia', 'B', 'Baixa']);

    sgDesenvolvimentos.Cells[de_status_analitico, i + 1] :=
      _Biblioteca.Decode(
        vDesenvolvimentos[i].status,
        ['AA', 'Aguardando analise',
         'EA', 'Em analise',
         'AD', 'Aguardando Desenvolvimento',
         'ED', 'Em desenvolvimento',
         'AT', 'Aguardando Teste',
         'ET', 'Em testes',
         'AC', 'Aguardando Documenta��o',
         'DC', 'Em documenta��o',
         'AL', 'Aguardando Liberacao',
         'LI', 'Liberado',
         'NE', 'Negado',
         'FI', 'Finalizado']);

    sgDesenvolvimentos.Cells[de_status, i + 1] := vDesenvolvimentos[i].status;
    sgDesenvolvimentos.Cells[de_prioridade, i + 1] := vDesenvolvimentos[i].prioridade;
    sgDesenvolvimentos.Cells[de_texto_historico, i + 1] := vDesenvolvimentos[i].texto_historico;
    sgDesenvolvimentos.Cells[de_texto_analise, i + 1] := vDesenvolvimentos[i].texto_analise;
    sgDesenvolvimentos.Cells[de_texto_desenvolvimento, i + 1] := vDesenvolvimentos[i].texto_desenvolvimento;
    sgDesenvolvimentos.Cells[de_texto_testes, i + 1] := vDesenvolvimentos[i].texto_testes;
    sgDesenvolvimentos.Cells[de_texto_documentacao, i + 1] := vDesenvolvimentos[i].texto_documentacao;
    sgDesenvolvimentos.Cells[de_texto_liberacao, i + 1] := vDesenvolvimentos[i].texto_liberacao;
  end;

  sgDesenvolvimentos.SetLinhasGridPorTamanhoVetor( Length(vDesenvolvimentos) );

  SetarFoco(sgDesenvolvimentos);
  sgDesenvolvimentosClick(nil);
end;

procedure TFormAcompanhamentoDesenvolvimentos.sbGravarClick(Sender: TObject);
var
  vDesenvolvimentoId: Integer;
  vRetorno: RecRetornoBD;
  coluna: string;
  texto: string;
begin
  inherited;

  vDesenvolvimentoId := StrToIntDef(sgDesenvolvimentos.Cells[de_codigo, sgDesenvolvimentos.Row], 0);

  if
    (vDesenvolvimentoId = 0) or
    (sgDesenvolvimentos.Cells[de_status, sgDesenvolvimentos.Row] = 'FI')
  then
    Exit;

  if pcStatus.ActivePage = tsAnalise then begin
    coluna := 'TEXTO_ANALISE';
    texto := FrAnalise.GetTexto;

    vRetorno :=
      _CadastroDesenvolvimento.AtualizarTextos(
        Sessao.getConexaoBanco,
        vDesenvolvimentoId,
        coluna,
        texto
      );

    if vRetorno.TeveErro then begin
      _Biblioteca.Exclamar(vRetorno.MensagemErro);
      Exit;
    end;

    coluna := 'TEXTO_ANALISE';
    texto := frAnalise.GetTexto;
  end
  else if pcStatus.ActivePage = tsDesenvolvimento then begin
    coluna := 'TEXTO_DESENVOLVIMENTO';
    texto := FrDesenvolvimento.GetTexto;
  end
  else if pcStatus.ActivePage = tsTestes then begin
    coluna := 'TEXTO_TESTES';
    texto := FrTestes.GetTexto;
  end
  else if pcStatus.ActivePage = tsDocumentacao then begin
    coluna := 'TEXTO_DOCUMENTACAO';
    texto := FrDocumentacao.GetTexto;
  end
  else if pcStatus.ActivePage = tsLiberacao then begin
    coluna := 'TEXTO_LIBERACAO';
    texto := frLiberacao.GetTexto;
  end;

  vRetorno :=
    _CadastroDesenvolvimento.AtualizarTextos(
      Sessao.getConexaoBanco,
      vDesenvolvimentoId,
      coluna,
      texto
    );

  if vRetorno.TeveErro then begin
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Exit;
  end;
end;

procedure TFormAcompanhamentoDesenvolvimentos.sbLogsClick(Sender: TObject);
begin
  inherited;
  Logs.Iniciar('LOGS_DESENVOLVIMENTOS', ['DESENVOLVIMENTO_ID'], 'VW_TIPOS_ALTER_LOGS_DESENVOLV', [SFormatInt(sgDesenvolvimentos.Cells[de_codigo, sgDesenvolvimentos.Row])] );
end;

procedure TFormAcompanhamentoDesenvolvimentos.sgDesenvolvimentosClick(
  Sender: TObject);
begin
  inherited;
  frHistorico.SetTexto(sgDesenvolvimentos.Cells[de_texto_historico, sgDesenvolvimentos.Row]);

  frAnalise.Modo(sgDesenvolvimentos.Cells[de_status, sgDesenvolvimentos.Row] = 'AA');
  FrAnalise.SetTexto(sgDesenvolvimentos.Cells[de_texto_analise, sgDesenvolvimentos.Row]);

  FrDesenvolvimento.Modo(sgDesenvolvimentos.Cells[de_status, sgDesenvolvimentos.Row] = 'ED');
  FrDesenvolvimento.SetTexto(sgDesenvolvimentos.Cells[de_texto_desenvolvimento, sgDesenvolvimentos.Row]);

  FrTestes.Modo(sgDesenvolvimentos.Cells[de_status, sgDesenvolvimentos.Row] = 'ET');
  FrTestes.SetTexto(sgDesenvolvimentos.Cells[de_texto_testes, sgDesenvolvimentos.Row]);

  FrDocumentacao.Modo(sgDesenvolvimentos.Cells[de_status, sgDesenvolvimentos.Row] = 'DC');
  FrDocumentacao.SetTexto(sgDesenvolvimentos.Cells[de_texto_documentacao, sgDesenvolvimentos.Row]);

  frLiberacao.Modo(sgDesenvolvimentos.Cells[de_status, sgDesenvolvimentos.Row] = 'LI');
  frLiberacao.SetTexto(sgDesenvolvimentos.Cells[de_texto_liberacao, sgDesenvolvimentos.Row]);
end;

procedure TFormAcompanhamentoDesenvolvimentos.sgDesenvolvimentosDrawCell(
  Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  alinhamento: TAlignment;
begin
  inherited;

  if ACol = de_codigo then
    alinhamento := taRightJustify
  else if Em(ACol, [de_prioridade_analitica, de_status]) then
    alinhamento := taCenter
  else
    alinhamento := taLeftJustify;

  sgDesenvolvimentos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], alinhamento, Rect);
end;

procedure TFormAcompanhamentoDesenvolvimentos.sgDesenvolvimentosGetCellColor(
  Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush;
  AFont: TFont);
begin
  inherited;

  if
    (ARow = 0) or
    (sgDesenvolvimentos.Cells[de_codigo, 1] = '') or
    (not Em(ACol, [de_prioridade_analitica, de_status]))
  then
    Exit;

  if ACol = de_prioridade_analitica then begin
    ABrush.Color := _Biblioteca.Decode(
    sgDesenvolvimentos.Cells[de_prioridade, ARow],
    ['A', _Biblioteca.co_vermelho_prioridade, 'M', clYellow, 'B', clBlue, 'U', _Biblioteca.co_vermelho_prioridade]
    );
    AFont.Color := clWhite;
    AFont.Style := [fsBold];
  end
  else if ACol = de_status then begin
    AFont.Color :=
      _Biblioteca.Decode(
        sgDesenvolvimentos.Cells[de_status, ARow],
        ['AD', clMaroon,
         'EN', $000096DB,
         'ED', clBlue,
         'ET', clPurple,
         'DC', clOlive,
         'FI', clSilver]
     );
  end;
end;

procedure TFormAcompanhamentoDesenvolvimentos.VerificarRegistro(Sender: TObject);
var
  i: Integer;
  tem_itens_marcados: Boolean;
begin
  inherited;

  tem_itens_marcados := False;
  for i := 0 to gbStatus.ControlCount -1 do begin
    if gbStatus.Controls[i] is TCheckBox then begin
      if TCheckBox(gbStatus.Controls[i]).Checked then begin
        tem_itens_marcados := True;
        Break;
      end;
    end;
  end;

  if not tem_itens_marcados then begin
    _Biblioteca.Exclamar('Pelo menos um status do desenvolvimento deve ser marcado!');
    Abort;
  end;

end;

end.
