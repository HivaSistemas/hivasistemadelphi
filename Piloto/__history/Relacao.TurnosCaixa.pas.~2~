unit Relacao.TurnosCaixa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _Sessao, _TurnosCaixas,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, FrameFuncionarios, Frame.Inteiros,
  FrameDataInicialFinal, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, _Biblioteca,
  FrameEmpresas, Vcl.Grids, GridLuka, _RecordsCaixa, Informacoes.TurnoCaixa,
  Vcl.StdCtrls, System.StrUtils, GroupBoxLuka, CheckBoxLuka;

type
  TFormRelacaoTurnosCaixa = class(TFormHerancaRelatoriosPageControl)
    FrEmpresas: TFrEmpresas;
    FrDataAbertura: TFrDataInicialFinal;
    FrDataFechamento: TFrDataInicialFinal;
    FrCodigoTurno: TFrameInteiros;
    FrFuncionarios: TFrFuncionarios;
    sgTurnos: TGridLuka;
    Panel1: TPanel;
    gbStatus: TGroupBoxLuka;
    ckAbertos: TCheckBoxLuka;
    ckBaixados: TCheckBoxLuka;
    st3: TStaticText;
    stValorMovDinheiro: TStaticText;
    st1: TStaticText;
    stValorMovCheque: TStaticText;
    stValorMovCartao: TStaticText;
    st5: TStaticText;
    stValorMovCobranca: TStaticText;
    st7: TStaticText;
    stValorMovCredito: TStaticText;
    st9: TStaticText;
    st10: TStaticText;
    st11: TStaticText;
    st12: TStaticText;
    stValorInfDinheiro: TStaticText;
    stValorInfCheque: TStaticText;
    stValorInfCartao: TStaticText;
    stValorInfCobranca: TStaticText;
    stValorDiferencaDinheiro: TStaticText;
    stValorDiferencaCheque: TStaticText;
    stValorDiferencaCartao: TStaticText;
    stValorDiferencaCobranca: TStaticText;
    stValorAbertura: TStaticText;
    st13: TStaticText;
    st17: TStaticText;
    stValorSuprimentos: TStaticText;
    st18: TStaticText;
    stValorSangriasDinheiro: TStaticText;
    stValorSangriasCheque: TStaticText;
    stValorSangriasCartao: TStaticText;
    stValorSangriasCobranca: TStaticText;
    stTotalMovimentos: TStaticText;
    st19: TStaticText;
    stTotalAbertura: TStaticText;
    stTotalSuprimentos: TStaticText;
    stTotalSangrias: TStaticText;
    stTotalInformadoFechamento: TStaticText;
    stValorDiferencaCredito: TStaticText;
    st20: TStaticText;
    st21: TStaticText;
    st22: TStaticText;
    st23: TStaticText;
    st24: TStaticText;
    st2: TStaticText;
    stValorPagamentosDinheiro: TStaticText;
    stValorPagamentosCheques: TStaticText;
    ststValorPagamentosCobrancas: TStaticText;
    stTotalPagamentos: TStaticText;
    stValorPagamentosCreditos: TStaticText;
    stValorMovAcumulado: TStaticText;
    st6: TStaticText;
    stValorInfAcumulado: TStaticText;
    stValorDiferencaAcumulado: TStaticText;
    st14: TStaticText;
    st15: TStaticText;
    st4: TStaticText;
    st16: TStaticText;
    stValorInfCredito: TStaticText;
    stTotalDiferencaFechamento: TStaticText;
    StaticText1: TStaticText;
    stValorMovPix: TStaticText;
    stValorInfPix: TStaticText;
    stValorDiferencaPix: TStaticText;
    StaticText5: TStaticText;
    stValorPagamentosPix: TStaticText;
    StaticText3: TStaticText;
    procedure sgTurnosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure sgTurnosDblClick(Sender: TObject);
    procedure sgTurnosClick(Sender: TObject);
    procedure sgTurnosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    FTurnos: TArray<RecInformacoesTurno>;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

const
  coTurnoId                  = 0;
  coCaixa                    = 1;
  coStatus                   = 2;
  coContaOrigemDinIni        = 3;
  coDataHoraAbertura         = 4;
  coUsuarioAbertura          = 5;
  coDataHoraFechamento       = 6;
  coUsuarioFechamento        = 7;
  coEmpresa                  = 8;

procedure TFormRelacaoTurnosCaixa.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vLinha: Integer;
begin
  inherited;
  vLinha := 1;
  sgTurnos.ClearGrid();

  if not FrCodigoTurno.EstaVazio then
    vSql := 'where ' + FrCodigoTurno.getSqlFiltros('TUR.TURNO_ID')
  else begin
    if not FrEmpresas.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrEmpresas.getSqlFiltros('TUR.EMPRESA_ID'));

    if not FrFuncionarios.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrFuncionarios.getSqlFiltros('TUR.FUNCIONARIO_ID'));

    if not FrDataAbertura.NenhumaDataValida then
      _Biblioteca.WhereOuAnd(vSql, FrDataAbertura.getSqlFiltros('trunc(TUR.DATA_HORA_ABERTURA)'));

    if not FrDataFechamento.NenhumaDataValida then
      _Biblioteca.WhereOuAnd(vSql, FrDataFechamento.getSqlFiltros('trunc(TUR.DATA_HORA_FECHAMENTO)'));

    _Biblioteca.WhereOuAnd(vSql, gbStatus.GetSql('TUR.STATUS'));
  end;

  vSql := vSql + ' order by TUR.TURNO_ID ';

  FTurnos := _TurnosCaixas.BuscarInformacoesTurno(Sessao.getConexaoBanco, vSql);
  if FTurnos = nil then begin
    NenhumRegistro;
    Exit;
  end;

  for i := Low(FTurnos) to High(FTurnos) do begin
    sgTurnos.Cells[coTurnoId, vLinha]                  := NFormat(FTurnos[i].TurnoId);
    sgTurnos.Cells[coEmpresa, vLinha]                  := NFormat(FTurnos[i].EmpresaId) + ' - ' + FTurnos[i].NomeEmpresa;
    sgTurnos.Cells[coStatus, vLinha]                   := IfThen(FTurnos[i].Status = 'A', 'Aberto', 'Baixado');

    if FTurnos[i].NomeContaOrigem <> '' then
      sgTurnos.Cells[coContaOrigemDinIni, vLinha]      := FTurnos[i].ContaOrigemDinIniId + ' - ' + FTurnos[i].NomeContaOrigem;

    sgTurnos.Cells[coDataHoraAbertura, vLinha]         := DHFormat(FTurnos[i].DataHoraAbertura);
    sgTurnos.Cells[coCaixa, vLinha]                    := NFormat(FTurnos[i].FuncionarioId) + ' - ' + FTurnos[i].NomeCaixa;
    sgTurnos.Cells[coUsuarioAbertura, vLinha]          := NFormat(FTurnos[i].UsuarioAberturaId) + ' - ' + FTurnos[i].NomeUsuarioAbertura;

    if FTurnos[i].NomeUsuarioFechamento <> '' then
      sgTurnos.Cells[coUsuarioFechamento, vLinha]        := NFormat(FTurnos[i].UsuarioFechamentoId) + ' - ' + FTurnos[i].NomeUsuarioFechamento;

    sgTurnos.Cells[coDataHoraFechamento, vLinha]       := DHFormatN(FTurnos[i].DataHoraFechamento);

    Inc(vLinha);
  end;

  sgTurnos.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor(Length(FTurnos));
  sgTurnosClick(nil);
  SetarFoco(sgTurnos);
end;

procedure TFormRelacaoTurnosCaixa.FormShow(Sender: TObject);
begin
  inherited;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId);
end;

procedure TFormRelacaoTurnosCaixa.Imprimir(Sender: TObject);
begin
  inherited;

end;

procedure TFormRelacaoTurnosCaixa.sgTurnosClick(Sender: TObject);
var
  i: Integer;
  vPosic: Integer;

  procedure ColorirDiferenca(Sender: TStaticText);
  begin
    if SFormatCurr(Sender.Caption) = 0 then
      Exit;

    if SFormatCurr(Sender.Caption) < 0 then
      Sender.Font.Color := clRed
    else
      Sender.Font.Color := clNavy;
  end;

begin
  inherited;

  vPosic := -1;
  for i := Low(FTurnos) to High(FTurnos) do begin
    if FTurnos[i].TurnoId <> SFormatInt(sgTurnos.Cells[coTurnoId, sgTurnos.Row]) then
      Continue;

    vPosic := i;
    Break;
  end;

  if vPosic = -1 then
    Exit;

  stValorAbertura.Caption := NFormatN(FTurnos[vPosic].ValorInicial) + ' ';

  stValorMovDinheiro.Caption := NFormatN(FTurnos[vPosic].ValorDinheiro) + ' ';
  stValorMovCheque.Caption   := NFormatN(FTurnos[vPosic].ValorCheque) + ' ';
  stValorMovCartao.Caption   := NFormatN(FTurnos[vPosic].ValorCartao) + ' ';
  stValorMovCobranca.Caption := NFormatN(FTurnos[vPosic].ValorCobranca) + ' ';
  stValorMovAcumulado.Caption:= NFormatN(FTurnos[vPosic].ValorAcumulado) + ' ';
  stValorMovCredito.Caption  := NFormatN(FTurnos[vPosic].ValorCredito) + ' ';

  stValorPagamentosDinheiro.Caption  := NFormatN(FTurnos[vPosic].ValorPagtosDinheiro) + ' ';
  stValorPagamentosCreditos.Caption  := NFormatN(FTurnos[vPosic].ValorPagtosCredito) + ' ';

  stValorSuprimentos.Caption := NFormatN(FTurnos[vPosic].ValorSuprimento) + ' ';

  stValorSangriasDinheiro.Caption := NFormatN(FTurnos[vPosic].ValorSangriaDinheiro) + ' ';
  stValorSangriasCheque.Caption   := NFormatN(FTurnos[vPosic].ValorSangriaCheque) + ' ';
  stValorSangriasCartao.Caption   := NFormatN(FTurnos[vPosic].ValorSangriaCartao) + ' ';
  stValorSangriasCobranca.Caption := NFormatN(FTurnos[vPosic].ValorSangriaCobranca) + ' ';

  stValorInfDinheiro.Caption := NFormatN(FTurnos[vPosic].ValorDinheiroInfFechamento) + ' ';
  stValorInfCheque.Caption   := NFormatN(FTurnos[vPosic].ValorChequeInfFechamento) + ' ';
  stValorInfCartao.Caption   := NFormatN(FTurnos[vPosic].ValorCartaoInfFechamento) + ' ';
  stValorInfCobranca.Caption := NFormatN(FTurnos[vPosic].ValorCobrancaInfFechamento) + ' ';

  stValorDiferencaDinheiro.Caption :=
    _Biblioteca.NFormatN(
        FTurnos[vPosic].ValorDinheiroInfFechamento
      - FTurnos[vPosic].ValorInicial
      - FTurnos[vPosic].ValorDinheiro
      - FTurnos[vPosic].ValorSuprimento
      + FTurnos[vPosic].ValorPagtosDinheiro
      + FTurnos[vPosic].ValorSangriaDinheiro
                        ) + ' ';

  stValorDiferencaCheque.Caption :=
    NFormatN(FTurnos[vPosic].ValorChequeInfFechamento
           - FTurnos[vPosic].ValorCheque
           + FTurnos[vPosic].ValorSangriaCheque
            ) + ' ';

  stValorDiferencaCartao.Caption :=
    NFormatN(FTurnos[vPosic].ValorCartaoInfFechamento
           - FTurnos[vPosic].ValorCartao
           + FTurnos[vPosic].ValorSangriaCartao
            ) + ' ';

  stValorDiferencaCobranca.Caption :=
    NFormatN(FTurnos[vPosic].ValorCobrancaInfFechamento
           - FTurnos[vPosic].ValorCobranca
           + FTurnos[vPosic].ValorSangriaCobranca
            ) + ' ';

  // Diferenca de acumulado sempre sera 0 pois n�o ha validacao
  stValorInfAcumulado.Caption := NFormatN(FTurnos[vPosic].ValorAcumulado) + ' ';
  stValorDiferencaAcumulado.Caption := NFormatN(0) + ' ';

  // Diferenca de credito sempre sera 0 pois n�o ha validacao
  stValorInfCredito.Caption := NFormatN(FTurnos[vPosic].ValorCredito) + ' ';
  stValorDiferencaCredito.Caption := NFormatN(0) + ' ';

  ColorirDiferenca(stValorDiferencaDinheiro);
  ColorirDiferenca(stValorDiferencaCheque);
  ColorirDiferenca(stValorDiferencaCartao);
  ColorirDiferenca(stValorDiferencaCobranca);
  ColorirDiferenca(stValorDiferencaAcumulado);
  ColorirDiferenca(stValorDiferencaCredito);

  stTotalAbertura.Caption   := NFormatN(FTurnos[vPosic].ValorInicial) + ' ';
  stTotalMovimentos.Caption :=
    NFormatN(
      FTurnos[vPosic].ValorDinheiro +
      FTurnos[vPosic].ValorCheque +
      FTurnos[vPosic].ValorCartao +
      FTurnos[vPosic].ValorCobranca +
      FTurnos[vPosic].ValorCredito
    ) + ' ';

  stTotalPagamentos.Caption := NFormatN(FTurnos[vPosic].ValorPagtosDinheiro + FTurnos[vPosic].ValorPagtosCredito);

  stTotalSangrias.Caption :=
    NFormatN(
      FTurnos[vPosic].ValorSangriaDinheiro +
      FTurnos[vPosic].ValorSangriaCartao +
      FTurnos[vPosic].ValorSangriaCheque +
      FTurnos[vPosic].ValorSangriaCobranca
    ) + ' ';

  stTotalSuprimentos.Caption := NFormatN(FTurnos[vPosic].ValorSuprimento) + ' ';

  stTotalInformadoFechamento.Caption :=
    NFormatN(
      SFormatDouble(stValorInfDinheiro.Caption) +
      SFormatDouble(stValorInfCheque.Caption) +
      SFormatDouble(stValorInfCartao.Caption) +
      SFormatDouble(stValorInfCobranca.Caption)
    ) + ' ';

  stTotalDiferencaFechamento.Caption :=
    NFormatN(
      SFormatDouble(stValorDiferencaDinheiro.Caption) +
      SFormatDouble(stValorDiferencaCheque.Caption) +
      SFormatDouble(stValorDiferencaCartao.Caption) +
      SFormatDouble(stValorDiferencaCobranca.Caption)
    ) + ' ';
  ColorirDiferenca(stTotalDiferencaFechamento);
end;

procedure TFormRelacaoTurnosCaixa.sgTurnosDblClick(Sender: TObject);
begin
  inherited;
  if SFormatInt(sgTurnos.Cells[coTurnoId, sgTurnos.Row]) = 0 then
    Exit;

  Informacoes.TurnoCaixa.Informar(SFormatInt(sgTurnos.Cells[coTurnoId, sgTurnos.Row]));
end;

procedure TFormRelacaoTurnosCaixa.sgTurnosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[
    coEmpresa,
    coContaOrigemDinIni,
    coDataHoraAbertura,
    coCaixa,
    coUsuarioAbertura,
    coUsuarioFechamento,
    coDataHoraFechamento
  ] then
    vAlinhamento := taLeftJustify
  else if Acol = coStatus then
    vAlinhamento := taCenter
  else
    vAlinhamento := taRightJustify;

  sgTurnos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoTurnosCaixa.sgTurnosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coStatus then begin
    AFont.Style := [fsBold];
    if sgTurnos.Cells[coStatus, ARow] = 'Aberto' then
      AFont.Color := clRed
    else
      AFont.Color := clNavy;

  end;
end;

procedure TFormRelacaoTurnosCaixa.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if gbStatus.NenhumMarcado then begin
    _Biblioteca.Exclamar('� necess�rio definir pelo menos 1 status!');
    SetarFoco(ckAbertos);
    Abort;
  end;
end;

end.
