unit Relacao.EntregasPendentes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _Sessao, _Orcamentos,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.Buttons, _Biblioteca,
  StaticTextLuka, FrameDataInicialFinal, FrameNumeros, FrameProdutos, _FrameHerancaPrincipal,
  _FrameHenrancaPesquisas, FrameClientes, _RelacaoEntregasPendentes, FrameEmpresas, Informacoes.Orcamento,
  ComboBoxLuka, CheckBoxLuka;

type
  TFormRelacaoEntregasPendentes = class(TFormHerancaRelatoriosPageControl)
    sgEntregas: TGridLuka;
    spSeparador: TSplitter;
    sgItens: TGridLuka;
    StaticTextLuka1: TStaticTextLuka;
    StaticTextLuka2: TStaticTextLuka;
    FrClientes: TFrClientes;
    FrProdutos: TFrProdutos;
    FrOrcamentos: TFrNumeros;
    FrPrevisaoEntrega: TFrDataInicialFinal;
    FrDataRecebimento: TFrDataInicialFinal;
    FrEmpresas: TFrEmpresas;
    lb1: TLabel;
    cbTipoEntrega: TComboBoxLuka;
    procedure FormShow(Sender: TObject);
    procedure sgEntregasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgEntregasDblClick(Sender: TObject);
    procedure sgEntregasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgEntregasClick(Sender: TObject);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  private
    FItens: TArray<RecEntregasItensPendentes>;
  protected
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

const
  (* Grid Orcamentos *)
  coOrcamentoId     = 0;
  coCliente         = 1;
  coTipoMovimento   = 2;
  coDataCadastro    = 3;
  coVendedor        = 4;
  coPrevisaoEntrega = 5;
  coLocal           = 6;
  coLocalId         = 7;
  coEmpresaId       = 8;

  (* Grid de Itens *)
  ciProdutoId     = 0;
  ciNome          = 1;
  ciMarca         = 2;
  ciQuantidade    = 3;
  ciEntregues     = 4;
  ciDevolvidos    = 5;
  ciSaldo         = 6;
  ciUnidade       = 7;
  ciLote          = 8;

{ TFormRelacaoEntregasPendentes }

procedure TFormRelacaoEntregasPendentes.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vEntregas: TArray<RecEntregasPendentes>;

  vPedidosIds: TArray<Integer>;
begin
  sgEntregas.ClearGrid();
  sgItens.ClearGrid();
  FItens := nil;
  vPedidosIds := nil;

  if not FrEmpresas.EstaVazio then
    _Biblioteca.WhereOuAnd( vSql, '( ' + FrEmpresas.getSqlFiltros('EMPRESA_ID') + ' or EMPRESA_ID is null) ' );

  if not FrClientes.EstaVazio then
    _Biblioteca.WhereOuAnd( vSql, FrClientes.getSqlFiltros('CLIENTE_ID') );

  if not FrPrevisaoEntrega.NenhumaDataValida then
    _Biblioteca.WhereOuAnd( vSql, FrPrevisaoEntrega.getSqlFiltros('trunc(PREVISAO_ENTREGA)') );

  if not FrDataRecebimento.NenhumaDataValida then
    _Biblioteca.WhereOuAnd( vSql, FrDataRecebimento.getSqlFiltros('trunc(DATA_HORA_RECEBIMENTO)') );

  if not FrOrcamentos.EstaVazio then
    _Biblioteca.WhereOuAnd( vSql, FrOrcamentos.TrazerFiltros('ORCAMENTO_ID') );

  if not FrProdutos.EstaVazio then begin
    _Biblioteca.WhereOuAnd(
      vSql,
      '(ORCAMENTO_ID, trunc(nvl(PREVISAO_ENTREGA, sysdate))) in(select distinct ORCAMENTO_ID, trunc(nvl(PREVISAO_ENTREGA, sysdate)) from VW_ITENS_ENTREGAS_PENDENTES where ' + FrProdutos.getSqlFiltros('PRODUTO_ID') + ') '
    );
  end;

  if cbTipoEntrega.GetValor <> 'NaoFiltrar' then
    _Biblioteca.WhereOuAnd( vSql, 'TIPO_MOVIMENTO = ''' + cbTipoEntrega.GetValor + ''' ' );

  vSql := vSql + ' order by ORCAMENTO_ID ';

  vEntregas := _RelacaoEntregasPendentes.BuscarEntregas(Sessao.getConexaoBanco, vSql);
  if vEntregas = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  for i := Low(vEntregas) to High(vEntregas) do begin
    sgEntregas.Cells[coOrcamentoId, i + 1]     := _Biblioteca.NFormat(vEntregas[i].OrcamentoId);
    sgEntregas.Cells[coCliente, i + 1]         := _Biblioteca.NFormat(vEntregas[i].ClienteId) + ' - ' + vEntregas[i].NomeCliente;
    sgEntregas.Cells[coTipoMovimento, i + 1]   := vEntregas[i].TipoMovimentoAnalitico;
    sgEntregas.Cells[coDataCadastro, i + 1]    := _Biblioteca.DHFormatN(vEntregas[i].DataHoraCadastro);
    sgEntregas.Cells[coVendedor, i + 1]        := _Biblioteca.NFormat(vEntregas[i].VendedorId) + ' - ' + vEntregas[i].NomeVendedor;
    sgEntregas.Cells[coPrevisaoEntrega, i + 1] := _Biblioteca.DHFormatN(vEntregas[i].PrevisaoEntrega);

    if vEntregas[i].LocalId > 0 then
      sgEntregas.Cells[coLocal, i + 1]           := _Biblioteca.NFormat(vEntregas[i].LocalId) + ' - ' + vEntregas[i].NomeLocal;

    sgEntregas.Cells[coLocalId, i + 1]         := _Biblioteca.NFormatN(vEntregas[i].LocalId);
    sgEntregas.Cells[coEmpresaId, i + 1]       := _Biblioteca.NFormatN(vEntregas[i].EmpresaId);

    _Biblioteca.AddNoVetorSemRepetir( vPedidosIds, vEntregas[i].OrcamentoId);
  end;
  sgEntregas.SetLinhasGridPorTamanhoVetor( Length(vEntregas) );

  FItens := _RelacaoEntregasPendentes.BuscarItensEntregas( Sessao.getConexaoBanco, 'where ' + FiltroInInt('ORCAMENTO_ID', vPedidosIds) );

  sgEntregasClick(Sender);
  _Biblioteca.SetarFoco(sgEntregas);
end;

procedure TFormRelacaoEntregasPendentes.FormShow(Sender: TObject);
begin
  inherited;
  FrEmpresas.InserirDadoPorChave( Sessao.getParametrosEmpresa.EmpresaId, False );
  _Biblioteca.SetarFoco(FrEmpresas);
end;

procedure TFormRelacaoEntregasPendentes.sgEntregasClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;

  vPrevisaoEntrega: TDateTime;
  vEmpresaId: Integer;
  vPedidoId: Integer;
  vLocalId: Integer;
begin
  inherited;

  vLinha := 0;
  sgItens.ClearGrid();

  vPrevisaoEntrega := ToData( sgEntregas.Cells[coPrevisaoEntrega, sgEntregas.Row] );
  vEmpresaId := SFormatInt( sgEntregas.Cells[coEmpresaId, sgEntregas.Row] );
  vPedidoId := SFormatInt( sgEntregas.Cells[coOrcamentoId, sgEntregas.Row] );
  vLocalId := SFormatInt( sgEntregas.Cells[coLocalId, sgEntregas.Row] );

  for i := Low(FItens) to High(FItens) do begin
    if
      (vEmpresaId <> FItens[i].EmpresaId) or
      (vPedidoId <> FItens[i].OrcamentoId) or
      (vLocalId <> FItens[i].LocalId) or
      (vPrevisaoEntrega <> FItens[i].PrevisaoEntrega)
    then
      Continue;

    Inc(vLinha);

    sgItens.Cells[ciProdutoId, vLinha]  := NFormat(FItens[i].ProdutoId);
    sgItens.Cells[ciNome, vLinha]       := FItens[i].Nome;
    sgItens.Cells[ciMarca, vLinha]      := NFormat(FItens[i].MarcaId) + ' - ' + FItens[i].NomeMarca;
    sgItens.Cells[ciQuantidade, vLinha] := NFormatNEstoque(FItens[i].Quantidade);
    sgItens.Cells[ciEntregues, vLinha]  := NFormatNEstoque(FItens[i].Entregues);
    sgItens.Cells[ciDevolvidos, vLinha] := NFormatNEstoque(FItens[i].Devolvidos);
    sgItens.Cells[ciSaldo, vLinha]      := NFormatNEstoque(FItens[i].Saldo);
    sgItens.Cells[ciUnidade, vLinha]    := FItens[i].UnidadeVenda;
    sgItens.Cells[ciLote, vLinha]       := FItens[i].Lote;
  end;
  sgItens.SetLinhasGridPorTamanhoVetor( vLinha );
end;

procedure TFormRelacaoEntregasPendentes.sgEntregasDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar( SFormatInt(sgEntregas.Cells[coOrcamentoId, sgEntregas.Row]) );
end;

procedure TFormRelacaoEntregasPendentes.sgEntregasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coOrcamentoId] then
    vAlinhamento := taRightJustify
  else if ACol = coTipoMovimento then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgEntregas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoEntregasPendentes.sgEntregasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coTipoMovimento then begin
    AFont.Style := [fsBold];
    if sgEntregas.Cells[ACol, ARow] = 'Retirar' then
      AFont.Color := clBlue
    else if sgEntregas.Cells[ACol, ARow] = 'Entregar' then
      AFont.Color := clTeal
    else
      AFont.Color := clGreen;
  end;
end;

procedure TFormRelacaoEntregasPendentes.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    ciProdutoId,
    ciQuantidade,
    ciEntregues,
    ciDevolvidos,
    ciSaldo]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
