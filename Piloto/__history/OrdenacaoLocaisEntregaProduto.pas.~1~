unit OrdenacaoLocaisEntregaProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastro, _Biblioteca, _Sessao, _RecordsEspeciais,
  _FrameHenrancaPesquisas, FrameEmpresas, _FrameHerancaPrincipal, _ProdutosOrdensLocEntregas,
  FrameOrdenacaoLocais, Vcl.Buttons, Vcl.ExtCtrls, FrameProdutos;

type
  TFormOrdenacaoLocaisEntregaProduto = class(TFormHerancaCadastro)
    FrOrdemAto: TFrOrdenacaoLocais;
    FrOrdemRetirar: TFrOrdenacaoLocais;
    FrOrdemEntregar: TFrOrdenacaoLocais;
    FrEmpresa: TFrEmpresas;
    FrProduto: TFrProdutos;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sbDesfazerClick(Sender: TObject);
  private
    procedure FrEmpresaOnAposPesquisar(Sender: TObject);
    procedure FrEmpresaOnAposDeletar(Sender: TObject);

    procedure FrProdutoOnAposPesquisar(Sender: TObject);
    procedure FrProdutoOnAposDeletar(Sender: TObject);

    function getComponentes: TArray<TControl>;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure GravarRegistro(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormDefinirOrdemLocalEntregaProduto }

procedure TFormOrdenacaoLocaisEntregaProduto.FormCreate(Sender: TObject);
begin
  inherited;
  _Biblioteca.Habilitar(getComponentes, False);
  FrEmpresa.OnAposPesquisar := FrEmpresaOnAposPesquisar;
  FrEmpresa.OnAposDeletar := FrEmpresaOnAposDeletar;

  FrProduto.OnAposPesquisar := FrProdutoOnAposPesquisar;
  FrProduto.OnAposDeletar := FrProdutoOnAposDeletar;
end;

procedure TFormOrdenacaoLocaisEntregaProduto.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrEmpresa);
end;

procedure TFormOrdenacaoLocaisEntregaProduto.FrEmpresaOnAposDeletar(Sender: TObject);
begin
  _Biblioteca.Habilitar(getComponentes, False);
end;

procedure TFormOrdenacaoLocaisEntregaProduto.FrEmpresaOnAposPesquisar(Sender: TObject);
begin
  _Biblioteca.Habilitar([FrProduto, sbDesfazer], True);
  _Biblioteca.Habilitar([FrEmpresa], False, False);

  SetarFoco(FrProduto);
end;

procedure TFormOrdenacaoLocaisEntregaProduto.FrProdutoOnAposDeletar(Sender: TObject);
begin
  _Biblioteca.Habilitar([FrProduto], False);
  _Biblioteca.Habilitar([FrEmpresa], True, False);

  SetarFoco(FrEmpresa);
end;

procedure TFormOrdenacaoLocaisEntregaProduto.FrProdutoOnAposPesquisar(Sender: TObject);

  procedure PreencherLocal(pFrame: TFrOrdenacaoLocais; pArrayGrupos: TArray<RecProdutosOrdensLocEntregas>);
  var
    i: Integer;
  begin
    for i := Low(pArrayGrupos) to High(pArrayGrupos) do
      pFrame.AddLocal( pArrayGrupos[i].LocalId, pArrayGrupos[i].NomeLocal );
  end;

begin
  if FrProduto.getProduto.EProdutoFilho then begin
    _Biblioteca.Exclamar('Produtos filhos n�o podem ter a ordena��o de entrega definida!');
    SetarFoco(FrProduto);
    FrProduto.Clear;
    Exit;
  end;

  if FrProduto.getProduto.TipoControleEstoque = 'K' then begin
    _Biblioteca.Exclamar('Produtos com o tipo de controle de estoque igual a "Kit desmenbrado" n�o podem ter a ordena��o de entrega definida!');
    SetarFoco(FrProduto);
    FrProduto.Clear;
    Exit;
  end;

  if FrProduto.getProduto.TipoControleEstoque = 'L' then begin
    _Biblioteca.Exclamar('Produtos com o tipo de controle de estoque igual a "Lote" n�o podem ter a ordena��o de entrega definida!');
    SetarFoco(FrProduto);
    FrProduto.Clear;
    Exit;
  end;

  _Biblioteca.Habilitar(getComponentes, True);
  _Biblioteca.Habilitar([sbGravar], True);
  _Biblioteca.Habilitar([FrProduto], False, False);

  PreencherLocal(FrOrdemAto,      _ProdutosOrdensLocEntregas.BuscarOrdensLocais(Sessao.getConexaoBanco, 0, [FrEmpresa.getEmpresa.EmpresaId, 'A', FrProduto.getProduto.produto_id]));
  PreencherLocal(FrOrdemRetirar,  _ProdutosOrdensLocEntregas.BuscarOrdensLocais(Sessao.getConexaoBanco, 0, [FrEmpresa.getEmpresa.EmpresaId, 'R', FrProduto.getProduto.produto_id]));
  PreencherLocal(FrOrdemEntregar, _ProdutosOrdensLocEntregas.BuscarOrdensLocais(Sessao.getConexaoBanco, 0, [FrEmpresa.getEmpresa.EmpresaId, 'E', FrProduto.getProduto.produto_id]));

  SetarFoco(FrOrdemAto);
end;

function TFormOrdenacaoLocaisEntregaProduto.getComponentes: TArray<TControl>;
begin
  Result := [FrOrdemAto, FrOrdemRetirar, FrOrdemEntregar];
end;

procedure TFormOrdenacaoLocaisEntregaProduto.sbDesfazerClick(Sender: TObject);
begin
//  inherited; Matou a heran�a, PEIIII.
  if (not Sessao.FinalizandoPorTempoInativo) and (not _Biblioteca.Perguntar('Voc� est� prestes a desfazer as altera��es neste registro, deseja realmente continuar?')) then
    Exit;

  _Biblioteca.Habilitar(getComponentes, False);
  _Biblioteca.Habilitar([sbGravar, FrProduto], False);
  _Biblioteca.Habilitar([FrEmpresa], True);

  SetarFoco(FrEmpresa);
end;

procedure TFormOrdenacaoLocaisEntregaProduto.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

procedure TFormOrdenacaoLocaisEntregaProduto.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  Sessao.getConexaoBanco.IniciarTransacao;

  vRetBanco :=
    _ProdutosOrdensLocEntregas.AtualizarProdutosOrdensLocais(
      Sessao.getConexaoBanco,
      FrEmpresa.getEmpresa().EmpresaId,
      FrProduto.getProduto().produto_id,
      'A',
      FrOrdemAto.TrazerArrayLocais,
      False
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  vRetBanco :=
    _ProdutosOrdensLocEntregas.AtualizarProdutosOrdensLocais(
      Sessao.getConexaoBanco,
      FrEmpresa.getEmpresa().EmpresaId,
      FrProduto.getProduto().produto_id,
      'R',
      FrOrdemRetirar.TrazerArrayLocais,
      False
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  vRetBanco :=
    _ProdutosOrdensLocEntregas.AtualizarProdutosOrdensLocais(
      Sessao.getConexaoBanco,
      FrEmpresa.getEmpresa().EmpresaId,
      FrProduto.getProduto().produto_id,
      'E',
      FrOrdemEntregar.TrazerArrayLocais,
      False
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  Sessao.getConexaoBanco.FinalizarTransacao;

  RotinaSucesso;

  FrProduto.Clear;
  FrEmpresa.Clear;
  SetarFoco(FrEmpresa);

  inherited;
end;

end.
