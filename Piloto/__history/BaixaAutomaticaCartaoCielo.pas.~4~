unit BaixaAutomaticaCartaoCielo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatorios, Vcl.StdCtrls, _LeitorRetornoCartao,
  CheckBoxLuka, Vcl.Buttons, Vcl.ExtCtrls, _FrameHerancaPrincipal, _Biblioteca, _RecordsFinanceiros,
  FrameDiretorioArquivo, _ContasReceber, StaticTextLuka, Vcl.Grids, GridLuka, _Sessao, _Imagens,
  Informacoes.TituloReceber, Vcl.Menus, PesquisaContasReceber, _RecordsEspeciais,
  SpeedButtonLuka, _ContasReceberBaixas, _ContasReceberBaixasItens;

type
  RecValoresBaixa = record
    Conta: string;
    DigitoConta: string;

    ValorBrutoDebito: Currency;
    ValorRetencaoDebito: Currency;
    ValorLiquidoDebito: Currency;

    ValorBrutoCredito: Currency;
    ValorRetencaoCredito: Currency;
    ValorLiquidoCredito: Currency;

    DataPagamento: TDateTime;
  end;

  TFormBaixaAutomaticaCartaoCielo = class(TFormHerancaRelatorios)
    FrCaminhoArquivo: TFrCaminhoArquivo;
    pnPrincipal: TPanel;
    pn2: TPanel;
    pnComprovantes: TPanel;
    sgRO: TGridLuka;
    splSeparador: TSplitter;
    st1: TStaticTextLuka;
    sgCV: TGridLuka;
    st2: TStaticTextLuka;
    pmOpcoesCV: TPopupMenu;
    miIdentificarManualmente: TMenuItem;
    pn4: TPanel;
    st8: TStaticText;
    st4: TStaticText;
    stValorBrutoAltis: TStaticTextLuka;
    stValorLiquidoAltis: TStaticTextLuka;
    st3: TStaticText;
    stValorRetencaoAltis: TStaticTextLuka;
    st9: TStaticTextLuka;
    st5: TStaticText;
    st6: TStaticText;
    stValorBrutoArquivo: TStaticTextLuka;
    stValorLiquidoArquivo: TStaticTextLuka;
    st11: TStaticText;
    stValorRetencaoArquivo: TStaticTextLuka;
    st13: TStaticTextLuka;
    sbGravar: TSpeedButtonLuka;
    stQtdeCVsArquivo: TStaticTextLuka;
    st15: TStaticText;
    stQtdeCVsNaoIdentAltis: TStaticTextLuka;
    st17: TStaticText;
    sgValorBaixaConta: TGridLuka;
    st7: TStaticTextLuka;
    procedure sgRODrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgCVDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgROClick(Sender: TObject);
    procedure sgCVGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure FormCreate(Sender: TObject);
    procedure sgCVDblClick(Sender: TObject);
    procedure sgCVGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgROGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure miIdentificarManualmenteClick(Sender: TObject);
    procedure sbGravarClick(Sender: TObject);
    procedure sgValorBaixaContaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure OnAposPesquisarArquivoClick(Sender: TObject);
  private
    FComprovantesVendas: TArray<RecComprovanteVendas>;
    FValoresBaixa: TArray<RecValoresBaixa>;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormBaixaAutomaticaCartaoCielo }

const
  coResumoOperacao  = 0;
  coNumeroEstab     = 1;
  coStatusPagto     = 2;
  coValorBruto      = 3;
  coValorRetencao   = 4;
  coValorLiquido    = 5;
  coDataPagto       = 6;
  coQtdeCVs         = 7;
  coQtdeNaoIdent    = 8;
  coValorBrutoAltis = 9;
  coValorRetAltis   = 10;
  coValorLiqAltis   = 11;
  coConta           = 12;
  coDigitoConta     = 13;

  (* Grid de comprovantes das vendas *)
  cvIdentificado   = 0;
  cvReceberId      = 1;
  cvCliente        = 2;
  cvTipoTrasacao   = 3;
  cvNumeroCartao   = 4;
  cvNSU            = 5;
  cvCodAutorizacao = 6;
  cvDataVenda      = 7;
  cvValorBruto     = 8;
  cvPercRetencao   = 9;
  cvValorRetArq    = 10;
  cvParcela        = 11;
  cvQtdeParcelas   = 12;
  cvBandeira       = 13;
  cvValorDocumento = 14;
  cvValorRetencao  = 15;
  cvValorLiquido   = 16;
  cvStatus         = 17;

  (* Grid dos valores a baixar por contas *)
  cbConta               = 0;
  cbValorLiquidoDebito  = 1;
  cbValorLiquidoCredito = 2;

  coCorSuperiorAltis   = clWhite;
  coCorInferiorAltis   = clTeal;

  coCorSuperiorArquivo = clWhite;
  coCorInferiorArquivo = clBlue;

procedure TFormBaixaAutomaticaCartaoCielo.Carregar(Sender: TObject);
var
  i: Integer;
  j: Integer;
  vLeitor: TLeitorRetornoCartao;
  vResumosOperacoes: TArray<RecResumoOperacoes>;

  vContasReceber: TArray<RecContasReceber>;

  vNrCartoes: TArray<string>;
  vDataVendas: TArray<TDateTime>;
  vNumerosEstab: TArray<string>;
  vCodigosAut: TArray<string>;
  vNSUs: TArray<Integer>;
  vParcelas: TArray<Integer>;
  vQtdeParcelas: TArray<Integer>;

  vPosicConta: Integer;
begin
  inherited;

  _Biblioteca.LimparCampos([
    sgRO,
    sgCV,
    sgValorBaixaConta,
    stValorBrutoArquivo,
    stValorLiquidoArquivo,
    stValorRetencaoArquivo,
    stQtdeCVsArquivo,
    stValorBrutoAltis,
    stValorLiquidoAltis,
    stValorRetencaoAltis,
    stQtdeCVsNaoIdentAltis
  ]);

  FComprovantesVendas := nil;
  vNrCartoes          := nil;
  vDataVendas         := nil;
  vNumerosEstab       := nil;
  vCodigosAut         := nil;
  vNSUs               := nil;
  vContasReceber      := nil;
  vParcelas           := nil;
  vQtdeParcelas       := nil;
  FValoresBaixa       := nil;

  try
    vLeitor := TLeitorRetornoCartao.Create(FrCaminhoArquivo.getCaminhoArquivo);

    vResumosOperacoes := vLeitor.getRetornoCielo;

    vLeitor.Free;
  except
    on e: Exception do begin
      _Biblioteca.Exclamar(e.Message);
      Exit;
    end;
  end;

  (* Preenchendo os Resumo de opera��es *)
  for i := Low(vResumosOperacoes) to High(vResumosOperacoes) do begin
    sgRO.Cells[coResumoOperacao, i + 2] := vResumosOperacoes[i].CodigoRO;
    sgRO.Cells[coNumeroEstab, i + 2]    := vResumosOperacoes[i].NumeroEstabelecimento;
    sgRO.Cells[coStatusPagto, i + 2]    := vResumosOperacoes[i].StatusPagto;
    sgRO.Cells[coValorBruto, i + 2]     := NFormatN(vResumosOperacoes[i].ValorBruto);
    sgRO.Cells[coValorRetencao, i + 2]  := NFormatN(vResumosOperacoes[i].ValorRetencao);
    sgRO.Cells[coValorLiquido, i + 2]   := NFormatN(vResumosOperacoes[i].ValorLiquido);
    sgRO.Cells[coDataPagto, i + 2]      := DFormat(vResumosOperacoes[i].DataPagamento);
    sgRO.Cells[coQtdeCVs, i + 2]        := NFormatN(vResumosOperacoes[i].QtdeCVs);
    sgRO.Cells[coConta, i + 2]          := vResumosOperacoes[i].ContaCorrente;
    sgRO.Cells[coDigitoConta, i + 2]    := vResumosOperacoes[i].DigitoConta;

    FComprovantesVendas := FComprovantesVendas + vResumosOperacoes[i].ComprovantesVenda;

    _Biblioteca.AddNoVetorSemRepetir( vNumerosEstab, vResumosOperacoes[i].NumeroEstabelecimento );
    for j := Low(vResumosOperacoes[i].ComprovantesVenda) to High(vResumosOperacoes[i].ComprovantesVenda) do begin
      _Biblioteca.AddNoVetorSemRepetir( vNrCartoes, vResumosOperacoes[i].ComprovantesVenda[j].NumeroCartao );
      _Biblioteca.AddNoVetorSemRepetir( vCodigosAut, vResumosOperacoes[i].ComprovantesVenda[j].CodigoAutorizacao );
      _Biblioteca.AddNoVetorSemRepetir( vNSUs, SFormatInt(vResumosOperacoes[i].ComprovantesVenda[j].Nsu) );
      _Biblioteca.AddNoVetorSemRepetir( vDataVendas, vResumosOperacoes[i].ComprovantesVenda[j].DataVenda );
      _Biblioteca.AddNoVetorSemRepetir( vParcelas, vResumosOperacoes[i].ComprovantesVenda[j].Parcela );
      _Biblioteca.AddNoVetorSemRepetir( vQtdeParcelas, vResumosOperacoes[i].ComprovantesVenda[j].QtdeParcelas );
    end;

    vPosicConta := -1;
    for j := Low(FValoresBaixa) to High(FValoresBaixa) do begin
      if vResumosOperacoes[i].ContaCorrente = FValoresBaixa[j].Conta then begin
        vPosicConta := j;
        Break;
      end;
    end;

    if vPosicConta = -1 then begin
      SetLength(FValoresBaixa, Length(FValoresBaixa) + 1);
      vPosicConta := High(FValoresBaixa);

      FValoresBaixa[vPosicConta].Conta         := vResumosOperacoes[i].ContaCorrente;
      FValoresBaixa[vPosicConta].DataPagamento := vResumosOperacoes[i].DataPagamento;

      FValoresBaixa[vPosicConta].ValorBrutoDebito    := 0;
      FValoresBaixa[vPosicConta].ValorRetencaoDebito := 0;
      FValoresBaixa[vPosicConta].ValorLiquidoDebito  := 0;

      FValoresBaixa[vPosicConta].ValorBrutoCredito    := 0;
      FValoresBaixa[vPosicConta].ValorRetencaoCredito := 0;
      FValoresBaixa[vPosicConta].ValorLiquidoCredito  := 0;
    end;

    if vResumosOperacoes[i].TipoCartao = 'D' then begin
      FValoresBaixa[vPosicConta].ValorBrutoDebito    := FValoresBaixa[vPosicConta].ValorBrutoDebito + vResumosOperacoes[i].ValorBruto;
      FValoresBaixa[vPosicConta].ValorRetencaoDebito := FValoresBaixa[vPosicConta].ValorRetencaoDebito + vResumosOperacoes[i].ValorRetencao;
      FValoresBaixa[vPosicConta].ValorLiquidoDebito  := FValoresBaixa[vPosicConta].ValorLiquidoDebito + vResumosOperacoes[i].ValorLiquido;
    end
    else begin
      FValoresBaixa[vPosicConta].ValorBrutoCredito    := FValoresBaixa[vPosicConta].ValorBrutoCredito + vResumosOperacoes[i].ValorBruto;
      FValoresBaixa[vPosicConta].ValorRetencaoCredito := FValoresBaixa[vPosicConta].ValorRetencaoCredito + vResumosOperacoes[i].ValorRetencao;
      FValoresBaixa[vPosicConta].ValorLiquidoCredito  := FValoresBaixa[vPosicConta].ValorLiquidoCredito + vResumosOperacoes[i].ValorLiquido;
    end;
  end;
  sgRO.SetLinhasGridPorTamanhoVetor( Length(vResumosOperacoes) + 1 );

  (* Identificando os financeiros no Altis *)
  vContasReceber :=
    _ContasReceber.BuscarContasReceberComando(
      Sessao.getConexaoBanco,
      'where ' + FiltroInStr('PAE.NUMERO_ESTABELECIMENTO_CIELO', vNumerosEstab ) + ' ' +
      'and ' + FiltroInStr('substr(COR.NUMERO_CARTAO_TRUNCADO, length(COR.NUMERO_CARTAO_TRUNCADO) - 3, 4)', vNrCartoes ) + ' ' +
      'and ' + FiltroInInt('COR.NSU', vNSUs ) + ' ' +
      'and ' + FiltroInInt('COR.PARCELA', vParcelas ) + ' ' +
      'and ' + FiltroInInt('COR.NUMERO_PARCELAS', vQtdeParcelas ) + ' '
 //      'and ' + FiltroInData('COR.DATA_CADASTRO', vDataVendas)
    );

  for i := Low(FComprovantesVendas) to High(FComprovantesVendas) do begin
    FComprovantesVendas[i].Altis.ReceberId := 0;

    for j := Low(vContasReceber) to High(vContasReceber) do begin
      if
        (FComprovantesVendas[i].NumeroCartao <> Copy(vContasReceber[j].NumeroCartaoTruncado, Length(vContasReceber[j].NumeroCartaoTruncado) - 3, 4)) or
        (SFormatInt(FComprovantesVendas[i].Nsu) <> SFormatInt(vContasReceber[j].nsu)) or
        (FComprovantesVendas[i].Parcela <> vContasReceber[j].Parcela) or
        (FComprovantesVendas[i].QtdeParcelas <> vContasReceber[j].NumeroParcelas) or
        ((FComprovantesVendas[i].Valor <> vContasReceber[j].ValorDocumento) and (FComprovantesVendas[i].DataVenda <> vContasReceber[j].data_cadastro))
      then
        Continue;

      FComprovantesVendas[i].Altis.ReceberId   := vContasReceber[j].ReceberId;
      FComprovantesVendas[i].Altis.ClienteId   := vContasReceber[j].CadastroId;
      FComprovantesVendas[i].Altis.NomeCliente := vContasReceber[j].NomeCliente;

      FComprovantesVendas[i].Altis.ValorDocumento := vContasReceber[j].ValorDocumento;
      FComprovantesVendas[i].Altis.ValorRetencao  := vContasReceber[j].ValorRetencao;
      FComprovantesVendas[i].Altis.ValorLiquido   := vContasReceber[j].ValorDocumento - vContasReceber[j].ValorRetencao;
      FComprovantesVendas[i].Altis.Status         := vContasReceber[j].StatusAnalitico;
    end;

    for j := Low(vResumosOperacoes) to High(vResumosOperacoes) do begin
      if FComprovantesVendas[i].CodigoRO <> vResumosOperacoes[j].CodigoRO then
        Continue;

      if FComprovantesVendas[i].Altis.ReceberId = 0 then
        Continue;

      Inc(vResumosOperacoes[j].Altis.QtdeIdentificados);
      vResumosOperacoes[j].Altis.ValorBruto    := vResumosOperacoes[j].Altis.ValorBruto + FComprovantesVendas[i].Altis.ValorDocumento;
      vResumosOperacoes[j].Altis.ValorRetencao := vResumosOperacoes[j].Altis.ValorRetencao + FComprovantesVendas[i].Altis.ValorRetencao;
      vResumosOperacoes[j].Altis.ValorLiquido  := vResumosOperacoes[j].Altis.ValorLiquido + FComprovantesVendas[i].Altis.ValorLiquido;

      Break;
    end;
  end;

  (* Preenchendo as informa��es do Altis no cabe�alho *)
  for i := Low(vResumosOperacoes) to High(vResumosOperacoes) do begin
    sgRO.Cells[coQtdeNaoIdent, i + 2]    := NFormatN(vResumosOperacoes[i].QtdeCVs - vResumosOperacoes[i].Altis.QtdeIdentificados);
    sgRO.Cells[coValorBrutoAltis, i + 2] := NFormatN(vResumosOperacoes[i].Altis.ValorBruto);
    sgRO.Cells[coValorRetAltis, i + 2]   := NFormatN(vResumosOperacoes[i].Altis.ValorRetencao);
    sgRO.Cells[coValorLiqAltis, i + 2]   := NFormatN(vResumosOperacoes[i].Altis.ValorLiquido);

    stValorBrutoArquivo.Somar( SFormatDouble(sgRO.Cells[coValorBruto, i + 2]) );
    stValorRetencaoArquivo.Somar( SFormatDouble(sgRO.Cells[coValorRetencao, i + 2]) );
    stValorLiquidoArquivo.Somar( SFormatDouble(sgRO.Cells[coValorLiquido, i + 2]) );
    stQtdeCVsArquivo.SomarInt( SFormatInt(sgRO.Cells[coQtdeCVs, i + 2]) );

    stValorBrutoAltis.Somar( SFormatDouble(sgRO.Cells[coValorBrutoAltis, i + 2]) );
    stValorRetencaoAltis.Somar( SFormatDouble(sgRO.Cells[coValorRetAltis, i + 2]) );
    stValorLiquidoAltis.Somar( SFormatDouble(sgRO.Cells[coValorLiqAltis, i + 2]) );
    stQtdeCVsNaoIdentAltis.SomarInt( SFormatInt(sgRO.Cells[coQtdeNaoIdent, i + 2]) );
  end;

  (* Preenchendo o �ltimo grid que � as informa��es de como ficar� a baixa no extrato da conta *)
  for i := Low(FValoresBaixa) to High(FValoresBaixa) do begin
    sgValorBaixaConta.Cells[cbConta, i + 1]               := FValoresBaixa[i].Conta;
    sgValorBaixaConta.Cells[cbValorLiquidoDebito, i + 1]  := NFormatN( FValoresBaixa[i].ValorLiquidoDebito );
    sgValorBaixaConta.Cells[cbValorLiquidoCredito, i + 1] := NFormatN( FValoresBaixa[i].ValorLiquidoCredito );
  end;
  sgValorBaixaConta.SetLinhasGridPorTamanhoVetor( Length(FValoresBaixa) );

  sgROClick(Sender);
  SetarFoco(sgRO);
end;

procedure TFormBaixaAutomaticaCartaoCielo.FormCreate(Sender: TObject);
begin
  inherited;
  sgRO.Cells[coResumoOperacao, 0] := 'Arquivo';
  sgRO.Cells[coQtdeNaoIdent, 0]   := 'Altis';

  sgCV.Cells[cvIdentificado, 0]   := 'Altis';
  sgCV.Cells[cvTipoTrasacao, 0]   := 'Arquivo';
  sgCV.Cells[cvValorDocumento, 0] := 'Altis';

  _Biblioteca.LimparCampos([
    stValorBrutoArquivo,
    stValorLiquidoArquivo,
    stValorRetencaoArquivo,
    stQtdeCVsArquivo,
    stValorBrutoAltis,
    stValorLiquidoAltis,
    stValorRetencaoAltis,
    stQtdeCVsNaoIdentAltis
  ]);

  FrCaminhoArquivo.OnAposPesquisarArquivo := OnAposPesquisarArquivoClick;
end;

procedure TFormBaixaAutomaticaCartaoCielo.OnAposPesquisarArquivoClick(
  Sender: TObject);
begin
  inherited;
  sbCarregarClick(Sender);
end;

procedure TFormBaixaAutomaticaCartaoCielo.miIdentificarManualmenteClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar<RecContasReceber>;
  vCartoes: TArray<RecCartoesVinculacao>;
  vRetBanco: RecRetornoBD;

  vLinhaGridRO: Integer;
  vLinhaGridCV: Integer;
begin
  inherited;
  vRetTela :=
    PesquisaContasReceber.Pesquisar(
      True,
      SFormatInt(sgCV.Cells[cvParcela, sgCV.Row]),
      SFormatInt(sgCV.Cells[cvQtdeParcelas, sgCV.Row]),
      SFormatDouble(sgCV.Cells[cvValorBruto, sgCV.Row]) - 0.10,
      SFormatDouble(sgCV.Cells[cvValorBruto, sgCV.Row]) + 0.10,
      ToData(sgCV.Cells[cvDataVenda, sgCV.Row]) - 2,
      ToData(sgCV.Cells[cvDataVenda, sgCV.Row]) + 3,
      Copy(sgCV.Cells[cvTipoTrasacao, sgCV.Row], 1, 1),
      sgCV.Cells[cvNSU, sgCV.Row],
      sgCV.Cells[cvNumeroCartao, sgCV.Row],
      sgCV.Cells[cvCodAutorizacao, sgCV.Row]
    );

  if vRetTela.BuscaCancelada then
    Exit;

  SetLength(vCartoes, 1);
  vCartoes[0].ReceberId     := vRetTela.Dados.ReceberId;

  vCartoes[0].Nsu               := sgCV.Cells[cvNsu, sgCV.Row];
  vCartoes[0].NumeroCartao      := sgCV.Cells[cvNumeroCartao, sgCV.Row];
  vCartoes[0].CodigoAutorizacao := sgCV.Cells[cvCodAutorizacao, sgCV.Row];

  vRetBanco := _ContasReceber.AtualizarDadosCartoes(Sessao.getConexaoBanco, vCartoes);
  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.RotinaSucesso;

  vLinhaGridRO := sgRO.Row;
  vLinhaGridCV := sgCV.Row;

  Carregar(Sender);

  sgRO.Row := vLinhaGridRO;
  sgROClick(nil);

  sgCV.Row := vLinhaGridCV;
end;

procedure TFormBaixaAutomaticaCartaoCielo.sbGravarClick(Sender: TObject);
var
  i: Integer;
  vObservacao: string;

  procedure AddObservacao(pTexto: string);
  begin
    if vObservacao <> '' then
      vObservacao := vObservacao + sLineBreak + pTexto
    else
      vObservacao := vObservacao + pTexto;
  end;

  procedure GravarBaixa(
    pValorTitulos: Currency;
    pValorRetencoes: Currency;
    pValorLiquido: Currency;
    pDataPagamento: TDateTime;
    pTipoCartao: string;
    pConta: string
  );
  var
    i: Integer;
    vBaixaId: Integer;
    vObsBaixa: string;
    vRetBanco: RecRetornoBD;
    vContas: TArray<RecTitulosBaixasPagtoDin>;
  begin
    if pValorTitulos = 0 then
      Exit;

    vObsBaixa :=
      'Baixa realizada pela concilia��o de cart�es de ' + IIfStr(pTipoCartao = 'D', 'd�bito', 'cr�dito')  + ' com o arquivo ' +
      'arquivo ' + FrCaminhoArquivo.getNomeArquivo + sLineBreak + vObservacao;

    SetLength(vContas, 1);
    vContas[0].ContaId := pConta;
    vContas[0].Valor   := pValorLiquido;

    vRetBanco :=
      _ContasReceberBaixas.AtualizarContaReceberBaixa(
        Sessao.getConexaoBanco,
        0,
        Sessao.getEmpresaLogada.EmpresaId,
        0,
        0,
        pValorTitulos,
        0,
        0,
        pValorRetencoes,
        0,
        0,
        pValorLiquido,
        0,
        0,
        0,
        0,
        0,
        pDataPagamento,
        'N',
        vObsBaixa,
        'S',
        'BCA',
        0,
        vContas,
        nil,
        nil,
        nil,
        nil,
        nil,
        0,
        0,
        True
      );

    Sessao.AbortarSeHouveErro(vRetBanco);

    vBaixaId := vRetBanco.AsInt;
    for i := Low(FComprovantesVendas) to High(FComprovantesVendas) do begin
      if (pConta <> FComprovantesVendas[i].Conta) or (pTipoCartao <> FComprovantesVendas[i].TipoCartao) then
        Continue;

      if FComprovantesVendas[i].Altis.ReceberId = 0 then
        Continue;

      // Gravando os t�tulos da baixa
      vRetBanco :=
        _ContasReceberBaixasItens.AtualizarContasReceberBaixasItens(
          Sessao.getConexaoBanco,
          vBaixaId,
          FComprovantesVendas[i].Altis.ReceberId,
          FComprovantesVendas[i].Altis.ValorLiquido,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          True
        );

      Sessao.AbortarSeHouveErro(vRetBanco);
    end;
  end;

begin
  inherited;
  if stQtdeCVsNaoIdentAltis.AsInt > 0 then
    AddObservacao('Existem ' + stQtdeCVsNaoIdentAltis.Caption + ' vendas n�o identificados no Hiva.');

  if (stValorLiquidoAltis.AsCurr - stValorLiquidoArquivo.AsCurr <> 0) then
    AddObservacao('Existe uma diferen�a no valor l�quido. Valor arquivo R$ ' + stValorLiquidoArquivo.Caption + ', valor Hiva R$ ' + stValorLiquidoAltis.Caption + '.');

  if (stValorRetencaoArquivo.AsCurr - stValorRetencaoAltis.AsCurr <> 0) then
    AddObservacao('Existe uma diferen�a no valor de reten��o. Valor arquivo R$ ' + stValorRetencaoArquivo.Caption + ', valor Hiva R$ ' + stValorRetencaoAltis.Caption + '.');

  if vObservacao <> '' then begin
    if not _Biblioteca.Perguntar('Existem as seguintes observa��es para concilia��o com este arquivo: ' + sLineBreak + vObservacao + 'Deseja realmente continuar?' ) then
      Abort;
  end;

  for i := Low(FComprovantesVendas) to High(FComprovantesVendas) do begin
    if FComprovantesVendas[i].Altis.Status = 'B' then begin
      _Biblioteca.Exclamar('Existem CVs que j� est�o baixados no Hiva, a concilia��o n�o ser� permitida!');
      Abort;
    end;
  end;

  Sessao.getConexaoBanco.IniciarTransacao;

  { ******************* Baixa dos t�tulos ******************* }
  for i := Low(FValoresBaixa) to High(FValoresBaixa) do begin
    // Gerando a baixa dos d�bitos
    GravarBaixa(
      FValoresBaixa[i].ValorBrutoDebito,
      FValoresBaixa[i].ValorRetencaoDebito,
      FValoresBaixa[i].ValorLiquidoDebito,
      FValoresBaixa[i].DataPagamento,
      'D�bito',
      FValoresBaixa[i].Conta
    );

    // Gerando a baixa dos d�bitos
    GravarBaixa(
      FValoresBaixa[i].ValorBrutoCredito,
      FValoresBaixa[i].ValorRetencaoCredito,
      FValoresBaixa[i].ValorLiquidoCredito,
      FValoresBaixa[i].DataPagamento,
      'Cr�dito',
      FValoresBaixa[i].Conta
    );
  end;

  Sessao.getConexaoBanco.FinalizarTransacao;

  _Biblioteca.Informar('Concilia��o realizada com sucesso.');
  Carregar(Sender);
end;

procedure TFormBaixaAutomaticaCartaoCielo.sgCVDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.TituloReceber.Informar( SFormatInt(sgCV.Cells[cvReceberId, sgCV.Row]) );
end;

procedure TFormBaixaAutomaticaCartaoCielo.sgCVDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ARow = 0 then begin
    if
      ACol in[
        cvIdentificado,
        cvReceberId,
        cvCliente
      ]
    then
      sgCV.MergeCells([ARow, ACol], [ARow, cvIdentificado], [ARow, cvCliente], taCenter, Rect)
    else if ACol in[cvTipoTrasacao, cvNumeroCartao, cvNSU, cvCodAutorizacao, cvDataVenda, cvPercRetencao, cvValorRetArq, cvParcela, cvQtdeParcelas, cvBandeira] then
      sgCV.MergeCells([ARow, ACol], [ARow, cvTipoTrasacao], [ARow, cvBandeira], taCenter, Rect)
    else
      sgCV.MergeCells([ARow, ACol], [ARow, cvValorDocumento], [ARow, cvStatus], taCenter, Rect);
  end
  else begin
    if ACol in[cvIdentificado, cvStatus, cvTipoTrasacao, cvBandeira] then
      vAlinhamento := taCenter
    else if ACol in[
      cvReceberId,
      cvNumeroCartao,
      cvValorBruto,
      cvPercRetencao,
      cvValorRetArq,
      cvParcela,
      cvQtdeParcelas,
      cvValorDocumento,
      cvValorRetencao,
      cvValorLiquido]
    then
      vAlinhamento := taRightJustify
    else
      vAlinhamento := taLeftJustify;

    sgCV.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
  end;
end;

procedure TFormBaixaAutomaticaCartaoCielo.sgCVGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then begin
    AFont.Style := [fsBold];
    if (ACol in[cvIdentificado..cvCliente]) or (ACol in[cvValorDocumento..cvStatus]) then begin
      sgCV.CorSuperiorCabecalho := coCorSuperiorAltis;
      sgCV.CorInferiorCabecalho := coCorInferiorAltis;
    end
    else begin
      sgCV.CorSuperiorCabecalho := coCorSuperiorArquivo;
      sgCV.CorInferiorCabecalho := coCorInferiorArquivo;
    end;
  end
  else begin
    sgCV.CorSuperiorCabecalho := clWhite;
    sgCV.CorInferiorCabecalho := $00CECECE;

    if (ACol = cvStatus) and (ARow > 1) then begin
      AFont.Style := [fsBold];
      if sgCV.Cells[cvStatus, ARow] = 'Aberto' then
        AFont.Color := clTeal
      else
        AFont.Color := clBlue;
    end
    else if (ACol = cvValorRetencao) and (ARow > 1) then begin
      if SFormatCurr(sgCV.Cells[cvValorRetArq, ARow]) <> SFormatCurr(sgCV.Cells[cvValorRetencao, ARow]) then
        AFont.Color := clRed;
    end;
  end;
end;

procedure TFormBaixaAutomaticaCartaoCielo.sgCVGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow < 2 then
    Exit;

  if ACol = cvIdentificado then begin
    if sgCV.Cells[cvIdentificado, ARow] = _Biblioteca.charSelecionado then
      APicture := _Imagens.FormImagens.im1.Picture
    else if sgCV.Cells[ACol, ARow] = _Biblioteca.charNaoSelecionado then
      APicture := _Imagens.FormImagens.im2.Picture;
  end;
end;

procedure TFormBaixaAutomaticaCartaoCielo.sgROClick(Sender: TObject);
var
  vNumeroRO: string;

  i: Integer;
  vLinha: Integer;
begin
  inherited;
  sgCV.ClearGrid();

  vLinha    := 1;
  vNumeroRO := sgRO.Cells[coResumoOperacao, sgRO.Row];
  for i := Low(FComprovantesVendas) to High(FComprovantesVendas) do begin
    if vNumeroRO <> FComprovantesVendas[i].CodigoRO then
      Continue;

    Inc( vLinha );

    if FComprovantesVendas[i].Altis.ReceberId > 0 then
      sgCV.Cells[cvIdentificado, vLinha] := _Biblioteca.charNaoSelecionado
    else
      sgCV.Cells[cvIdentificado, vLinha] := _Biblioteca.charSelecionado;

    sgCV.Cells[cvReceberId, vLinha]      := NFormatN(FComprovantesVendas[i].Altis.ReceberId);
    sgCV.Cells[cvCliente, vLinha]        := getInformacao(FComprovantesVendas[i].Altis.ClienteId, FComprovantesVendas[i].Altis.NomeCliente);
    sgCV.Cells[cvTipoTrasacao, vLinha]   := FComprovantesVendas[i].TipoCartao;
    sgCV.Cells[cvNumeroCartao, vLinha]   := FComprovantesVendas[i].NumeroCartao;    
    sgCV.Cells[cvDataVenda, vLinha]      := DFormatN(FComprovantesVendas[i].DataVenda);
    sgCV.Cells[cvValorBruto, vLinha]     := NFormatN(FComprovantesVendas[i].Valor);
    sgCV.Cells[cvPercRetencao, vLinha]   := NFormatN(FComprovantesVendas[i].PercRetencao);
    sgCV.Cells[cvValorRetArq, vLinha]    := NFormatN(FComprovantesVendas[i].ValorRetencao);
    sgCV.Cells[cvParcela, vLinha]        := NFormatN(FComprovantesVendas[i].Parcela);
    sgCV.Cells[cvQtdeParcelas, vLinha]   := NFormatN(FComprovantesVendas[i].QtdeParcelas);
    sgCV.Cells[cvBandeira, vLinha]       := FComprovantesVendas[i].Bandeira;
    sgCV.Cells[cvNSU, vLinha]            := FComprovantesVendas[i].Nsu;
    sgCV.Cells[cvCodAutorizacao, vLinha] := FComprovantesVendas[i].CodigoAutorizacao;

    sgCV.Cells[cvValorDocumento, vLinha] := NFormatN(FComprovantesVendas[i].Altis.ValorDocumento);
    sgCV.Cells[cvValorRetencao, vLinha]  := NFormatN(FComprovantesVendas[i].Altis.ValorRetencao);
    sgCV.Cells[cvValorLiquido, vLinha]   := NFormatN(FComprovantesVendas[i].Altis.ValorLiquido);
    sgCV.Cells[cvStatus, vLinha]         := FComprovantesVendas[i].Altis.Status;
  end;
  sgCV.SetLinhasGridPorTamanhoVetor( vLinha );
end;

procedure TFormBaixaAutomaticaCartaoCielo.sgRODrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ARow = 0 then begin
    if
      ACol in[
        coResumoOperacao,
        coNumeroEstab,
        coStatusPagto,
        coValorBruto,
        coValorRetencao,
        coValorLiquido,
        coDataPagto,
        coQtdeCVs
      ]
    then
      sgRO.MergeCells([ARow, ACol], [ARow, coResumoOperacao], [ARow, coQtdeCVs], taCenter, Rect)
    else if ACol in[coQtdeNaoIdent, coValorBrutoAltis, coValorRetAltis, coValorLiqAltis] then
      sgRO.MergeCells([ARow, ACol], [ARow, coQtdeNaoIdent], [ARow, coValorLiqAltis], taCenter, Rect);
  end
  else begin
    if ACol in[coStatusPagto] then
      vAlinhamento := taCenter
    else if ACol in[coValorBruto, coValorRetencao, coValorLiquido, coQtdeCVs, coQtdeNaoIdent, coValorBrutoAltis, coValorRetAltis, coValorLiqAltis] then
      vAlinhamento := taRightJustify
    else
      vAlinhamento := taLeftJustify;

    sgRO.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
  end;
end;

procedure TFormBaixaAutomaticaCartaoCielo.sgROGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then begin
    AFont.Style := [fsBold];
    if ACol in[coResumoOperacao..coQtdeCVs] then begin
      sgRO.CorSuperiorCabecalho := coCorSuperiorArquivo;
      sgRO.CorInferiorCabecalho := coCorInferiorArquivo;
    end
    else begin
      sgRO.CorSuperiorCabecalho := coCorSuperiorAltis;
      sgRO.CorInferiorCabecalho := coCorInferiorAltis;
    end;
  end
  else begin
    sgRO.CorSuperiorCabecalho := clWhite;
    sgRO.CorInferiorCabecalho := $00CECECE;

    if (ACol = coValorRetAltis) and (ARow > 1) then begin
      if SFormatCurr(sgRO.Cells[coValorRetencao, ARow]) <> SFormatCurr(sgRO.Cells[coValorRetAltis, ARow]) then
        AFont.Color := clRed;
    end;
  end;
end;

procedure TFormBaixaAutomaticaCartaoCielo.sgValorBaixaContaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = cbConta then
    vAlinhamento := taLeftJustify
  else
    vAlinhamento := taRightJustify;

  sgValorBaixaConta.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormBaixaAutomaticaCartaoCielo.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
