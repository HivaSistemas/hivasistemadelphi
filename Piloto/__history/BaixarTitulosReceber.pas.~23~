unit BaixarTitulosReceber;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Grids, GridLuka, _ContasReceberBaixas,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, _RelacaoContasReceber,
  FrameContas, Vcl.StdCtrls, EditLuka, Vcl.Mask, EditLukaData, _Sessao, _RecordsRelatorios,
  Vcl.Buttons, Vcl.ExtCtrls, _Biblioteca, System.Math, System.StrUtils, _RecordsEspeciais,
  _ContasReceber, Informacoes.TituloReceber, _MovimentosTurnosItens, _RecordsCaixa,
  _ContasReceberBaixasItens, Impressao.ComprovantePagamentoTituloReceberGrafico,
  FramePagamentoFinanceiro, _TurnosCaixas, StaticTextLuka;

type
  TFormBaixarTitulosReceber = class(TFormHerancaFinalizar)
    lb8: TLabel;
    lb22: TLabel;
    lb23: TLabel;
    lb1: TLabel;
    lb2: TLabel;
    eDataPagamento: TEditLukaData;
    eValorTitulos: TEditLuka;
    eValorJuros: TEditLuka;
    eValorTotal: TEditLuka;
    eObservacoes: TEditLuka;
    sgTitulos: TGridLuka;
    FrPagamento: TFrPagamentoFinanceiro;
    eValorRetencao: TEditLuka;
    lb3: TLabel;
    eValorMulta: TEditLuka;
    lb4: TLabel;
    lb5: TLabel;
    eValorAdiantado: TEditLuka;
    ckGerarCreditoTroco: TCheckBox;
    procedure sgTitulosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgTitulosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure FormCreate(Sender: TObject);
    procedure sgTitulosDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure eDataPagamentoExit(Sender: TObject);
    procedure FrPagamentoeValorCreditoChange(Sender: TObject);
    procedure FrPagamentosbBuscarDadosChequesClick(Sender: TObject);
  private
    FClienteId: Integer;
    FBaixandoCartoes: Boolean;
    FTitulos: TArray<RecContasReceber>;

    procedure CalcularJuros(pDataPagamento: TDate);
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function BaixarTitulos(pClienteId: Integer; pTitulosIds: TArray<Integer>; pPodeHabilitarTodasFormasPagamento: Boolean; pBaixandoCartoes: Boolean): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

const
  coDocumento      = 0;
  coCliente        = 1;
  coParcela        = 2;
  coValor          = 3;
  coMulta          = 4;
  coJuros          = 5;
  coRetencao       = 6;
  coValorTotal     = 7;
  coDataVencimento = 8;
  coDiasAtraso     = 9;
  coTipoCobranca   = 10;
  coReceberId      = 11;

function BaixarTitulos(pClienteId: Integer; pTitulosIds: TArray<Integer>; pPodeHabilitarTodasFormasPagamento: Boolean; pBaixandoCartoes: Boolean): TRetornoTelaFinalizar;
var
  i: Integer;
  vForm: TFormBaixarTitulosReceber;
  vTitulos: TArray<RecContasReceber>;

  vValorTotalTitulos: Currency;
  vTotalAdiantados: Currency;
  vValorMultaTitulos: Currency;
  vValorRetencoes: Currency;
begin
  vTitulos := _RelacaoContasReceber.BuscarContasReceberComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('CON.RECEBER_ID', pTitulosIds));
  if vTitulos = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vForm := TFormBaixarTitulosReceber.Create(Application);

  vValorTotalTitulos := 0;
  vValorMultaTitulos := 0;
  vValorRetencoes    := 0;
  vTotalAdiantados   := 0;
  with vForm do begin
    for i := Low(vTitulos) to High(vTitulos) do begin
      sgTitulos.Cells[coDocumento, i + 1]      := vTitulos[i].documento;
      sgTitulos.Cells[coCliente, i + 1]        := NFormat(vTitulos[i].cliente_id) + ' - ' + vTitulos[i].nome_cliente;
      sgTitulos.Cells[coParcela, i + 1]        := NFormat(vTitulos[i].parcela);
      sgTitulos.Cells[coValor, i + 1]          := NFormat(vTitulos[i].valor_documento);
      sgTitulos.Cells[coRetencao, i + 1]       := NFormatN(vTitulos[i].ValorRetencao);

      if vTitulos[i].forma_pagamento <> 'CRT' then begin
        vTitulos[i].ValorMulta :=
          _Biblioteca.getValorMulta(
            vTitulos[i].valor_documento,
            Sessao.getParametrosEmpresa.PercentualMulta,
            vTitulos[i].dias_atraso
          );
      end;

      sgTitulos.Cells[coMulta, i + 1]          := NFormatN(vTitulos[i].ValorMulta);
      sgTitulos.Cells[coValorTotal, i + 1]     := NFormat(vTitulos[i].getValorLiquido);

      sgTitulos.Cells[coDataVencimento, i + 1] := DFormat(vTitulos[i].data_vencimento);
      sgTitulos.Cells[coDiasAtraso, i + 1]     := NFormatN(IfThen(vTitulos[i].dias_atraso > 0, vTitulos[i].dias_atraso));
      sgTitulos.Cells[coTipoCobranca, i + 1]   := NFormat(vTitulos[i].cobranca_id) + ' - ' + vTitulos[i].nome_tipo_cobranca;
      sgTitulos.Cells[coReceberId, i + 1]      := NFormat(vTitulos[i].receber_id);

      vValorTotalTitulos := vValorTotalTitulos + vTitulos[i].valor_documento;
      vValorMultaTitulos := vValorMultaTitulos + vTitulos[i].ValorMulta;
      vValorRetencoes    := vValorRetencoes + vTitulos[i].ValorRetencao;
      vTotalAdiantados   := vTotalAdiantados + vTitulos[i].ValorAdiantado;
    end;
    sgTitulos.SetLinhasGridPorTamanhoVetor( Length(vTitulos) );

    eValorTitulos.AsCurr    := vValorTotalTitulos;
    eValorAdiantado.AsCurr  := vTotalAdiantados;
    eValorMulta.AsCurr      := vValorMultaTitulos;
    eValorRetencao.AsCurr   := vValorRetencoes;
    eValorTotal.AsCurr      := vValorTotalTitulos - vTotalAdiantados + vValorMultaTitulos - vValorRetencoes;

    FrPagamento.CadastroId := pClienteId;
    FrPagamento.eValorMulta.AsCurr := eValorMulta.AsCurr;
    FrPagamento.eValorJuros.AsCurr := eValorJuros.AsCurr;
    FrPagamento.ValorTitulos       := vValorTotalTitulos - vValorRetencoes - vTotalAdiantados;
    FrPagamento.TotalizarValoresASerPago;

    if (not pPodeHabilitarTodasFormasPagamento) or pBaixandoCartoes then
      vForm.FrPagamento.ApenasDinheiro;

    FTitulos := vTitulos;
    FClienteId := pClienteId;
    FBaixandoCartoes := pBaixandoCartoes;

    Result.Ok(ShowModal, True);
  end;
end;

procedure TFormBaixarTitulosReceber.CalcularJuros(pDataPagamento: TDate);
var
  i: Integer;
  vValorJurosTitulos: Currency;
begin
  vValorJurosTitulos := 0;

  for i := Low(FTitulos) to High(FTitulos) do begin
    FTitulos[i].ValorJuros := 0;

    if FTitulos[i].forma_pagamento <> 'CRT' then begin
      FTitulos[i].dias_atraso := Floor(pDataPagamento - FTitulos[i].data_vencimento);

      FTitulos[i].ValorJuros :=
        _Biblioteca.getValorJuros(
          FTitulos[i].valor_documento,
          Sessao.getParametrosEmpresa.PercentualJurosMensal,
          FTitulos[i].dias_atraso
        );
    end;

    vValorJurosTitulos := vValorJurosTitulos + FTitulos[i].ValorJuros;

    sgTitulos.Cells[coJuros, i + 1]          := NFormatN(FTitulos[i].ValorJuros);
    sgTitulos.Cells[coValorTotal, i + 1]     := NFormat(FTitulos[i].getValorLiquido);
  end;

  eValorJuros.AsCurr := vValorJurosTitulos;
  eValorTotal.AsCurr := eValorTitulos.AsCurr - eValorAdiantado.AsCurr + eValorMulta.AsCurr + vValorJurosTitulos - eValorRetencao.AsCurr;

  if FrPagamento.eValorJuros.AsCurr = 0 then begin
    FrPagamento.eValorJuros.AsCurr := vValorJurosTitulos;
    FrPagamento.TotalizarValoresASerPago;
  end;
end;

procedure TFormBaixarTitulosReceber.eDataPagamentoExit(Sender: TObject);
begin
  inherited;
  CalcularJuros(eDataPagamento.AsData);
end;

procedure TFormBaixarTitulosReceber.Finalizar(Sender: TObject);
var
  i: Integer;
  vBaixaId: Integer;
  vValorTroco: Currency;
  vRetBanco: RecRetornoBD;

  vIndiceProporcional: Double;

  vValorDinheiroBxTit: Currency;
  vValorChequeBxTit: Currency;
  vValorCartaoDebBxTit: Currency;
  vValorCartaoCreBxTit: Currency;
  vValorCobrancaBxTit: Currency;
  vValorCreditoBxTit: Currency;

  vValorPropMulta: Currency;
  vValorPropJuros: Currency;
  vValorPropDesconto: Currency;

  vValorPropDinheiro: Currency;
  vValorPropCheque: Currency;
  vValorPropCartaoDeb: Currency;
  vValorPropCartaoCred: Currency;
  vValorPropCobranca: Currency;
  vValorPropCredito: Currency;

  vSaldoMulta: Currency;
  vSaldoJuros: Currency;
  vSaldoDesconto: Currency;

  vSaldoDinheiro: Currency;
  vSaldoCheque: Currency;
  vSaldoCartaoDeb: Currency;
  vSaldoCartaoCre: Currency;
  vSaldoCobranca: Currency;
  vSaldoCredito: Currency;

  vTurnoId: Integer;
begin
  vValorPropMulta      := 0;
  vValorPropJuros      := 0;
  vValorPropDesconto   := 0;

  vValorPropDinheiro   := 0;
  vValorPropCheque     := 0;
  vValorPropCartaoDeb  := 0;
  vValorPropCartaoCred := 0;
  vValorPropCobranca   := 0;
  vValorPropCredito    := 0;

  vSaldoMulta    := FrPagamento.eValorMulta.AsCurr;
  vSaldoJuros    := FrPagamento.eValorJuros.AsCurr;
  vSaldoDesconto := FrPagamento.eValorDesconto.AsCurr;

  vValorTroco := IIfDbl(ckGerarCreditoTroco.Checked, FrPagamento.eValorDiferencaPagamentos.AsCurr);

    if ckGerarCreditoTroco.Checked then
    vIndiceProporcional := FrPagamento.ValorPagar / FrPagamento.TotalPagamentos
  else
    vIndiceProporcional := 1;

  vSaldoDinheiro  := Arredondar(FrPagamento.eValorDinheiro.AsCurr * vIndiceProporcional, 2);
  vSaldoCheque    := Arredondar(FrPagamento.eValorCheque.AsCurr * vIndiceProporcional, 2);
  vSaldoCartaoDeb := Arredondar(FrPagamento.eValorCartaoDebito.AsCurr * vIndiceProporcional, 2);
  vSaldoCartaoCre := Arredondar(FrPagamento.eValorCartaoCredito.AsCurr * vIndiceProporcional, 2);
  vSaldoCobranca  := Arredondar(FrPagamento.eValorCobranca.AsCurr * vIndiceProporcional, 2);
  vSaldoCredito   := Arredondar(FrPagamento.eValorCredito.AsCurr * vIndiceProporcional, 2);

  vValorDinheiroBxTit  := vSaldoDinheiro;
  vValorChequeBxTit    := vSaldoCheque;
  vValorCartaoDebBxTit := vSaldoCartaoDeb;
  vValorCartaoCreBxTit := vSaldoCartaoCre;
  vValorCobrancaBxTit  := vSaldoCobranca;
  vValorCreditoBxTit   := vSaldoCredito;

  vTurnoId := 0;
  vTurnoId := _TurnosCaixas.getTurnoId( Sessao.getConexaoBanco, Sessao.getUsuarioLogado.funcionario_id );
  if (vTurnoId = 0) and ((FrPagamento.Dinheiro = nil) and (FrPagamento.Cheques <> nil)) then begin
    _Biblioteca.Exclamar('N�o foi encontrada turno aberto para o usu�rio logado!');
    SetarFoco(FrPagamento.eValorDinheiro);
    Abort;
  end;

  Sessao.getConexaoBanco.IniciarTransacao;

{ ******************* Baixa dos t�tulos ******************* }
  // Gerando a capa da baixa
  vRetBanco :=
    _ContasReceberBaixas.AtualizarContaReceberBaixa(
      Sessao.getConexaoBanco,
      0,
      Sessao.getEmpresaLogada.EmpresaId,
      FClienteId,
      IIfInt((vTurnoId > 0) and (FrPagamento.Dinheiro = nil) and (FrPagamento.eValorCartaoDebito.AsCurr + FrPagamento.eValorCartaoCredito.AsCurr = 0), vTurnoId),
      eValorTitulos.AsCurr,
      FrPagamento.eValorMulta.AsCurr,
      FrPagamento.eValorJuros.AsCurr,
      FrPagamento.eValorDesconto.AsCurr,
      eValorRetencao.AsCurr,
      eValorAdiantado.AsCurr,
      FrPagamento.eValorDinheiro.AsCurr,
      FrPagamento.eValorCheque.AsCurr,
      FrPagamento.eValorCartaoDebito.AsCurr,
      FrPagamento.eValorCartaoCredito.AsCurr,
      FrPagamento.eValorCobranca.AsCurr,
      FrPagamento.eValorCredito.AsCurr,
      IIfData( FrPagamento.eValorCartaoDebito.AsCurr + FrPagamento.eValorCartaoCredito.AsCurr = 0, eDataPagamento.AsData),
      IIfStr( FrPagamento.eValorCartaoDebito.AsCurr + FrPagamento.eValorCartaoCredito.AsCurr > 0, 'S', 'N' ),
      eObservacoes.Text,
      IIfStr( FrPagamento.eValorCartaoDebito.AsCurr + FrPagamento.eValorCartaoCredito.AsCurr = 0 , 'S', 'N' ),
      'BCR',
      IIf(ckGerarCreditoTroco.Checked, 0, vValorTroco),
      FrPagamento.Dinheiro,
      FrPagamento.CartoesDebito,
      FrPagamento.CartoesCredito,
      FrPagamento.Cobrancas,
      FrPagamento.Cheques,
      FrPagamento.Creditos,
      IIf(ckGerarCreditoTroco.Checked, vValorTroco, 0),
      0,
      True
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  vBaixaId := vRetBanco.AsInt;
  for i := Low(FTitulos) to High(FTitulos) do begin

    vIndiceProporcional := FTitulos[i].valor_documento / eValorTitulos.AsCurr;

    if FrPagamento.eValorMulta.AsCurr > 0 then
      vValorPropMulta := Arredondar(FrPagamento.eValorMulta.AsCurr * vIndiceProporcional, 2);

    if FrPagamento.eValorJuros.AsCurr > 0 then
      vValorPropJuros := Arredondar(FrPagamento.eValorJuros.AsCurr * vIndiceProporcional, 2);

    if FrPagamento.eValorDesconto.AsCurr > 0 then
      vValorPropDesconto := Arredondar(FrPagamento.eValorDesconto.AsCurr * vIndiceProporcional, 2);

    if vValorDinheiroBxTit > 0 then begin
      if FBaixandoCartoes then
        vValorPropDinheiro := FTitulos[i].valor_documento - FTitulos[i].ValorRetencao
      else
        vValorPropDinheiro := Arredondar(vValorDinheiroBxTit * vIndiceProporcional, 2);
    end;

    if vValorChequeBxTit > 0 then
      vValorPropCheque   := Arredondar(vValorChequeBxTit * vIndiceProporcional, 2);

    if vValorCartaoDebBxTit > 0 then
      vValorPropCartaoDeb   := Arredondar(vValorCartaoDebBxTit * vIndiceProporcional, 2);

    if vValorCartaoCreBxTit > 0 then
      vValorPropCartaoCred  := Arredondar(vValorCartaoCreBxTit * vIndiceProporcional, 2);

    if vValorCobrancaBxTit > 0 then
      vValorPropCobranca := Arredondar(vValorCobrancaBxTit * vIndiceProporcional, 2);

    if vValorCreditoBxTit > 0 then
      vValorPropCredito  := Arredondar(vValorCreditoBxTit * vIndiceProporcional, 2);

    vSaldoMulta      := vSaldoMulta - vValorPropMulta;
    vSaldoJuros      := vSaldoJuros - vValorPropJuros;
    vSaldoDesconto   := vSaldoDesconto - vValorPropDesconto;
    vSaldoDinheiro   := vSaldoDinheiro - vValorPropDinheiro;
    vSaldoCheque     := vSaldoCheque - vValorPropCheque;
    vSaldoCartaoDeb  := vSaldoCartaoDeb - vValorPropCartaoDeb;
    vSaldoCartaoCre  := vSaldoCartaoCre - vValorPropCartaoCred;
    vSaldoCobranca   := vSaldoCobranca - vValorPropCobranca;
    vSaldoCredito    := vSaldoCredito - vValorPropCredito;

    if i = High(FTitulos) then begin
      vValorPropMulta      := vValorPropMulta + vSaldoMulta;
      vValorPropJuros      := vValorPropJuros + vSaldoJuros;
      vValorPropDesconto   := vValorPropDesconto + vSaldoDesconto;
      vValorPropDinheiro   := vValorPropDinheiro + vSaldoDinheiro;
      vValorPropCheque     := vValorPropCheque + vSaldoCheque;
      vValorPropCartaoDeb  := vValorPropCartaoDeb + vSaldoCartaoDeb;
      vValorPropCartaoCred := vValorPropCartaoCred + vSaldoCartaoCre;
      vValorPropCobranca   := vValorPropCobranca + vSaldoCobranca;
      vValorPropCredito    := vValorPropCredito + vSaldoCredito;
    end;

    // Gravando os t�tulos da baixa
    vRetBanco :=
      _ContasReceberBaixasItens.AtualizarContasReceberBaixasItens(
        Sessao.getConexaoBanco,
        vBaixaId,
        FTitulos[i].receber_id,
        vValorPropDinheiro,
        vValorPropCartaoDeb,
        vValorPropCartaoCred,
        vValorPropCheque,
        vValorPropCobranca,
        vValorPropCredito,
        vValorPropDesconto,
        vValorPropMulta,
        vValorPropJuros,
        True
      );

    Sessao.AbortarSeHouveErro(vRetBanco);
  end;

  Sessao.AbortarSeHouveErro(vRetBanco);
  Sessao.getConexaoBanco.FinalizarTransacao;

  // O ccmprovante ser� impresso no caixa
  if FrPagamento.eValorCartaoDebito.AsCurr + FrPagamento.eValorCartaoCredito.AsCurr = 0 then begin
    RotinaSucesso;
    if Perguntar('Deseja imprimir o comprovante de baixa dos t�tulos?') then
      Impressao.ComprovantePagamentoTituloReceberGrafico.Imprimir(vBaixaId);
  end
  else
    _Biblioteca.Informar('Baixa realizada com sucesso, conclua a opera��o no caixa.');

  inherited;
end;

procedure TFormBaixarTitulosReceber.FormCreate(Sender: TObject);
begin
  inherited;
  FTitulos := nil;
  FrPagamento.Natureza := 'R';
end;

procedure TFormBaixarTitulosReceber.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(eDataPagamento);
end;

procedure TFormBaixarTitulosReceber.FrPagamentoeValorCreditoChange(
  Sender: TObject);
begin
  inherited;
  FrPagamento.checarDefinidos(Sender);

end;

procedure TFormBaixarTitulosReceber.FrPagamentosbBuscarDadosChequesClick(
  Sender: TObject);
begin
  inherited;
  FrPagamento.sbBuscarDadosChequesClick(Sender);

end;

procedure TFormBaixarTitulosReceber.sgTitulosDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.TituloReceber.Informar( SFormatInt(sgTitulos.Cells[coReceberId, sgTitulos.Row]) );
end;

procedure TFormBaixarTitulosReceber.sgTitulosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coParcela, coValor, coMulta, coJuros, coRetencao, coValorTotal, coDiasAtraso] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgTitulos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormBaixarTitulosReceber.sgTitulosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol in[coDiasAtraso, coRetencao] then
    AFont.Color := clRed
  else if ACol = coValorTotal then
    AFont.Color := $000096DB;
end;

procedure TFormBaixarTitulosReceber.VerificarRegistro(Sender: TObject);
var
  i: Integer;
  vTitulosIds: TArray<Integer>;
begin
  inherited;
  if not eDataPagamento.DataOk then begin
    Exclamar('A data do pagamento deve ser informada!');
    SetarFoco(eDataPagamento);
    Abort;
  end;

  FrPagamento.VerificarRegistro;

  if FBaixandoCartoes and FrPagamento.ExisteDiferenca then begin
    _Biblioteca.Exclamar('Existe diferen�a com o valor total a pagar e as formas de pagamento definidas, verifique!');
    SetarFoco(FrPagamento.eValorDinheiro);
    Abort;
  end;

  if FBaixandoCartoes and (FrPagamento.Dinheiro = nil) then begin
    _Biblioteca.Exclamar('A conta para recebimento dos cart�es n�o foi informada corretamente, verifique!');
    SetarFoco(FrPagamento.eValorDinheiro);
    Abort;
  end;

  vTitulosIds := nil;
  for i := Low(FTitulos) to High(FTitulos) do
    _Biblioteca.AddNoVetorSemRepetir( vTitulosIds, FTitulos[i].receber_id );

  if _ContasReceber.ExistemTitulosBaixados( Sessao.getConexaoBanco, vTitulosIds ) then begin
    Exclamar('Existem t�tulos para esta baixa que j� foram baixados, verifique!');
    SetarFoco(FrPagamento);
    Abort;
  end;

  if FrPagamento.ePercentualDesconto.AsCurr > Sessao.getUsuarioLogado.PercentualDescontoAdicFinan then begin
    Exclamar('Voc� n�o est� autorizado a conceder este percentual de desconto!');
    SetarFoco(FrPagamento.eValorDesconto);
    Abort;
  end;

  if FrPagamento.eValorCartaoDebito.AsCurr + FrPagamento.eValorCartaoCredito.AsCurr > 0 then begin
    if FrPagamento.CartoesDebito + FrPagamento.CartoesCredito = nil then begin
      Exclamar('� necess�rio selecionar os cart�es que ser�o passados!');

      if FrPagamento.eValorCartaoDebito.AsCurr > 0 then
        SetarFoco(FrPagamento.eValorCartaoDebito)
      else
        SetarFoco(FrPagamento.eValorCartaoCredito);

      Abort;
    end;
  end;

  if
    (FrPagamento.eValorDiferencaPagamentos.AsCurr > 0) and
    (FrPagamento.eValorCartaoDebito.AsCurr + FrPagamento.eValorCartaoCredito.AsCurr = 0) and
    (not ckGerarCreditoTroco.Checked)
  then begin
    _Biblioteca.Exclamar('N�o � permitido que as formas de pagamentos sejam maiores que o total a receber sem que a op��o "Gerar cr�dito do troco" esteja marcada!');
    Abort
  end;
end;

end.

