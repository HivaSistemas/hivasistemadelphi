unit CadastroCreditosManuaisPagar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CadastroCreditosManuais, _ContasPagar, _RecordsEspeciais,
  FramePlanosFinanceiros, FrameDadosCobranca, _FrameHerancaPrincipal, _Sessao, _Biblioteca,
  _FrameHenrancaPesquisas, FrameEmpresas, Vcl.StdCtrls, EditLuka, Vcl.Buttons, _ContasReceber,
  Vcl.ExtCtrls, Frame.Cadastros, FrameCentroCustos, StaticTextLuka, _ContasReceberBaixas,
  FramePagamentoFinanceiro, Vcl.Mask, EditLukaData, MemoAltis, CheckBoxLuka, _TurnosCaixas;

type
  TFormCadastroCreditosManuaisPagar = class(TFormCadastroCreditosManuais)
    procedure FormCreate(Sender: TObject);
  protected
    procedure Modo(pEditando: Boolean); override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure GravarRegistro(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroCreditosManuaisPagar }

procedure TFormCadastroCreditosManuaisPagar.FormCreate(Sender: TObject);
begin
  inherited;
  FrPagamentoFinanceiro.Natureza := 'R';
end;

procedure TFormCadastroCreditosManuaisPagar.GravarRegistro(Sender: TObject);
var
  vTurnoId: Integer;
  vTemCartao: Boolean;
  vDataAtual: TDateTime;
  vBaixaReceberId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;

  vBaixaReceberId := 0;

  vTurnoId := _TurnosCaixas.getTurnoId( Sessao.getConexaoBanco, Sessao.getUsuarioLogado.funcionario_id );
  if (vTurnoId = 0) and (FrPagamentoFinanceiro.Dinheiro = nil) and (FrPagamentoFinanceiro.eValorDinheiro.AsCurr > 0) then begin
    _Biblioteca.Exclamar('N�o foi encontrada a defini��o das contas para o pagamento em dinheiro!');
    SetarFoco(FrPagamentoFinanceiro.eValorDinheiro);
    Abort;
  end;

  vTemCartao := FrPagamentoFinanceiro.ValorCartaoDebito + FrPagamentoFinanceiro.ValorCartaoCredito > 0;
  vDataAtual := Sessao.getData;

  Sessao.getConexaoBanco.IniciarTransacao;

  // Gerando a baixa
  // Se for importa��o( Virada de sistema ) n�o gera o financeiro
  if not ckImportacao.Checked then begin
    vRetBanco :=
      _ContasReceberBaixas.AtualizarContaReceberBaixa(
        Sessao.getConexaoBanco,
        0,
        FrEmpresa.getEmpresa().EmpresaId,
        FrCadastro.getCadastro().cadastro_id,
        IIfInt((vTurnoId > 0) and (FrPagamentoFinanceiro.Dinheiro = nil) and (not vTemCartao), vTurnoId),
        eValorCreditoGerar.AsCurr,
        0,
        0,
        0,
        0,
        0,
        FrPagamentoFinanceiro.ValorDinheiro,
        FrPagamentoFinanceiro.ValorCheque,
        FrPagamentoFinanceiro.ValorCartaoDebito,
        FrPagamentoFinanceiro.ValorCartaoCredito,
        FrPagamentoFinanceiro.ValorCobranca,
        0,
        IIfDbl(vTemCartao, 0, Sessao.getData),
        IIfStr(vTemCartao, 'S', 'N'),
        meObservacoes.Lines.Text,
        IIfStr(vTemCartao, 'N', 'S'),
        'CRE',
        0,
        FrPagamentoFinanceiro.Dinheiro,
        FrPagamentoFinanceiro.CartoesDebito,
        FrPagamentoFinanceiro.CartoesCredito,
        FrPagamentoFinanceiro.Cobrancas,
        FrPagamentoFinanceiro.Cheques,
        nil,
        0,
        0,
        True,
      0,
      nil
      );

    if vRetBanco.TeveErro then begin
      Sessao.getConexaoBanco.VoltarTransacao;
      _Biblioteca.Exclamar(vRetBanco.MensagemErro);
      Abort;
    end;

    vBaixaReceberId := vRetBanco.AsInt;
  end;

  // Lan�ando o cr�dito no contas a pagar - D�bito
  vRetBanco :=
    _ContasPagar.AtualizarContaPagar(
      Sessao.getConexaoBanco,
      0,
      FrEmpresa.getEmpresa().EmpresaId,
      FrCadastro.getCadastro().cadastro_id,
      IIfInt(ckImportacao.Checked, Sessao.getParametros.TipoCobrancaGerCredImpId, Sessao.getParametros.TipoCobrancaGeracaoCredId),
      '9999',
      FrPlanoFinanceiro.getDados().PlanoFinanceiroId,
      'CRE' + NFormat(FrCadastro.getCadastro().cadastro_id) + DFormat(eDataVencimento.AsData),
      eValorCreditoGerar.AsDouble,
      0,
      IIfStr(ckImportacao.Checked, 'MAN', 'BCR'),
      vBaixaReceberId,
      1,
      1,
      0,
      'A',
      eDataVencimento.AsData,
      eDataVencimento.AsData,
      '',
      '',
      Sessao.getParametros.BloquearCreditoPagarManual,
      IIfStr(Sessao.getParametros.BloquearCreditoPagarManual = 'S', 'Bloqueio autom�tico de cr�dito a pagar.'),
      0,
      vDataAtual,
      vDataAtual,
      meObservacoes.Lines.Text,
      True
    );

  if vRetBanco.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  Sessao.getConexaoBanco.FinalizarTransacao;

  if vBaixaReceberId > 0 then
    _Biblioteca.Informar('Nova baixa a receber inserida com sucesso. C�digo: ' + NFormat(vBaixaReceberId))
  else
    _Biblioteca.Informar('Novo cr�dito inserido com sucesso.');
end;

procedure TFormCadastroCreditosManuaisPagar.Modo(pEditando: Boolean);
begin
  inherited;
  if pEditando then begin
    FrPlanoFinanceiro.InserirDadoPorChave('1.002.001', False);
    FrPlanoFinanceiro.Modo(False, False);
  end;
end;

procedure TFormCadastroCreditosManuaisPagar.VerificarRegistro(Sender: TObject);
begin

  if (ckImportacao.Checked) and (not _Sessao.Sessao.AutorizadoRotina('cadastrar_credito_importacao_pagar')) then begin
    _Biblioteca.Exclamar('Voc� possui autoriza��o para lan�ar um cr�dito de importa��o!');
    SetarFoco(ckImportacao);
    Abort;
  end;

  inherited;
end;

end.
