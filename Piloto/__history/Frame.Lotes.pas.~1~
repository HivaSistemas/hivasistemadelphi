unit Frame.Lotes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.Mask, _Biblioteca,
  EditLukaData, Vcl.StdCtrls, EditLuka;

type
  TFrameLotes = class(TFrameHerancaPrincipal)
    lbLote: TLabel;
    eLote: TEditLuka;
    eDataFabricacao: TEditLukaData;
    eDataVencimento: TEditLukaData;
    lbDataFabricacao: TLabel;
    lbDataVencimento: TLabel;
    eBitola: TEditLuka;
    lbBitola: TLabel;
    eTonalidade: TEditLuka;
    lbTonalidade: TLabel;
  private
    FTipoControleEstoque: string;
    function GetLote: string;
    procedure SetLote(pLote: string);
    function GetDataFabricacao: TDate;
    procedure SetDataFabricacao(pData: TDate);
    function GetDataVencimento: TDate;
    procedure SetDataVencimento(pData: TDate);
  public
    procedure SetFocus; override;
    procedure SetTipoControleEstoque(pTipoControleEstoque: string; pExigirDataFabricacao: string; pExigirDataVencimento: string);

    procedure Clear; override;
    procedure SomenteLeitura(pValue: Boolean); override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;

    function EstaVazio: Boolean; override;

    function LoteValido: Boolean;
  published
    property Lote: string read GetLote write SetLote;
    property DataFabricacao: TDate read GetDataFabricacao write SetDataFabricacao;
    property DataVencimento: TDate read GetDataVencimento write SetDataVencimento;
  end;

implementation

{$R *.dfm}

{ TFrameLotes }

procedure TFrameLotes.Clear;
begin
  inherited;
  SetTipoControleEstoque('N', 'N', 'N');
  _Biblioteca.LimparCampos([eLote, eDataFabricacao, eDataVencimento, eBitola, eTonalidade]);
end;

function TFrameLotes.EstaVazio: Boolean;
begin
  eLote.Text := Trim(eLote.Text);
  Result := eLote.Text = '';
end;

function TFrameLotes.GetDataFabricacao: TDate;
begin
  Result := eDataFabricacao.AsData;
end;

function TFrameLotes.GetDataVencimento: TDate;
begin
  Result := eDataVencimento.AsData;
end;

function TFrameLotes.GetLote: string;
begin
  if FTipoControleEstoque = 'P' then
    Result := eBitola.Text + '-' + eTonalidade.Text
  else
    Result := eLote.Text;
end;

function TFrameLotes.LoteValido: Boolean;
begin
  if Em(FTipoControleEstoque, ['L', 'G']) then
    Result := not Em(eLote.Text, ['', '???'])
  else if FTipoControleEstoque = 'P' then
    Result := (eBitola.Text <> '') and (eTonalidade.Text <> '')
  else
    Result := eLote.Text = '???';
end;

procedure TFrameLotes.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([eLote, eDataFabricacao, eDataVencimento, eBitola, eTonalidade], pEditando, pLimpar);
end;

procedure TFrameLotes.SetDataFabricacao(pData: TDate);
begin
  eDataFabricacao.AsData := pData;
end;

procedure TFrameLotes.SetDataVencimento(pData: TDate);
begin
  eDataVencimento.AsData := pData;
end;

procedure TFrameLotes.SetFocus;
begin
  inherited;
  if not FEditandoLocal then begin
    keybd_event(VK_TAB, 0, 0, 0);
    Exit;
  end;

  if FTipoControleEstoque <> 'P' then
    SetarFoco(eLote)
  else
    SetarFoco(eBitola);
end;

procedure TFrameLotes.SetLote(pLote: string);
begin
  if FTipoControleEstoque = 'P' then begin
    eBitola.Text     := Copy(pLote, 1, Pos('-', pLote) - 1);
    eTonalidade.Text := Copy(pLote, Pos('-', pLote) + 1, Length(pLote));
  end
  else
    eLote.Text := pLote;
end;

procedure TFrameLotes.SetTipoControleEstoque(pTipoControleEstoque: string; pExigirDataFabricacao: string; pExigirDataVencimento: string);
begin
  FEditandoLocal := True;
  FTipoControleEstoque := pTipoControleEstoque;

  _Biblioteca.Visibilidade([eBitola, eTonalidade, lbBitola, lbTonalidade], FTipoControleEstoque = 'P');
  _Biblioteca.Visibilidade([eLote, eDataFabricacao, eDataVencimento, lbLote, lbDataFabricacao, lbDataVencimento], FTipoControleEstoque <> 'P');
  if FTipoControleEstoque = 'P' then begin
    _Biblioteca.Habilitar([eBitola, eTonalidade], True, True);
    _Biblioteca.Habilitar([eLote, eDataFabricacao, eDataVencimento], False, True);
  end
  else if Em(FTipoControleEstoque, ['L', 'G']) then begin
    _Biblioteca.Habilitar([eLote], True, True);
    _Biblioteca.Habilitar([eDataFabricacao], pExigirDataFabricacao = 'S', True);
    _Biblioteca.Habilitar([eDataVencimento], pExigirDataVencimento = 'S', True);
  end
  else begin
    _Biblioteca.Habilitar([eLote, eDataFabricacao, eDataVencimento, eBitola, eTonalidade], False, True);
    eLote.Text := '???';
    FEditandoLocal := False;
  end;
end;

procedure TFrameLotes.SomenteLeitura(pValue: Boolean);
begin
  inherited;
  _Biblioteca.SomenteLeitura([eLote, eDataFabricacao, eDataVencimento], pValue);
end;

end.
