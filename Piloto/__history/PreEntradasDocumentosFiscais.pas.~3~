unit PreEntradasDocumentosFiscais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatorios, Vcl.Buttons, _RecordsNFE, _Biblioteca,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Grids, GridLuka, _ComunicacaoNFE, _Sessao, System.Math,
  Vcl.Menus, CheckBoxLuka, _PreEntradasNotasFiscais, EntradaNotasFiscais, _PreEntradasDocumentosFiscais,
  _HerancaRelatoriosPageControl, Vcl.ComCtrls, _FrameHerancaPrincipal, ConhecimentosFretes,
  _FrameHenrancaPesquisas, FrameFornecedores, FrameDataInicialFinal, InformacoesPreEntradaCTe,
  GroupBoxLuka, StaticTextLuka, Frame.Inteiros, InformacoesPreEntradaNotasFiscais;

type
  TFormPreEntradasDocumentosFiscais = class(TFormHerancaRelatoriosPageControl)
    pmOperacoesManifesto: TPopupMenu;
    miManifestarCienciaOperacao: TMenuItem;
    miConfirmacaoOperacao: TMenuItem;
    miDesconhecimentoOperacao: TMenuItem;
    miOperacaoNaoRealizada: TMenuItem;
    miN1: TMenuItem;
    miN2: TMenuItem;
    miRealizarEntradaViaXML: TMenuItem;
    miManifestaes1: TMenuItem;
    miN3: TMenuItem;
    miN4: TMenuItem;
    miN5: TMenuItem;
    sgDocs: TGridLuka;
    FrFornecedores: TFrFornecedores;
    FrDataEmissao: TFrDataInicialFinal;
    gbStatus: TGroupBoxLuka;
    ckAbertos: TCheckBoxLuka;
    ckBaixados: TCheckBoxLuka;
    ckCancelados: TCheckBoxLuka;
    ck1: TCheckBoxLuka;
    ck2: TCheckBoxLuka;
    ck3: TCheckBoxLuka;
    miBuscarnovosCTEsSEFAZ1: TMenuItem;
    gbStatusCTeAltis: TGroupBoxLuka;
    ck4: TCheckBoxLuka;
    ck6: TCheckBoxLuka;
    FrNumeroPreEntradasNotas: TFrameInteiros;
    FrNumeroPreEntradasCTEs: TFrameInteiros;
    FrNumeroNota: TFrameInteiros;
    FrNumeroCTe: TFrameInteiros;
    miBuscarNovasNotasFiscais: TSpeedButton;
    procedure sgDocsDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure miBuscarNovasNotasFiscaisClick(Sender: TObject);
    procedure sgDocsGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure miManifestarCienciaOperacaoClick(Sender: TObject);
    procedure miRealizarEntradaViaXMLClick(Sender: TObject);
    procedure miOperacaoNaoRealizadaClick(Sender: TObject);
    procedure miDesconhecimentoOperacaoClick(Sender: TObject);
    procedure miBuscarnovosCTEsSEFAZ1Click(Sender: TObject);
    procedure sgDocsDblClick(Sender: TObject);
  protected
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormRelacaoNotasFiscaisPreEntradas }

const

  coDataEmissao  = 0;//8;
  coTipoNota     = 1;
  coNumero       = 2;
  coSerie        = 3;
  coStatusSEFAZ  = 4;
  coCNPJEmitente = 5;
  coRazaoSocial  = 6;
  coStatusAltis  = 7;
  coValorTotal   = 8;//9;
  coChaveNFe     = 9;//10;

  (* Ocultas *)
  coPreEntradaId = 10;//0;
  coStatusSEFAZSintetico = 11;
  coStatusAltisSintetico = 12;

procedure TFormPreEntradasDocumentosFiscais.Carregar(Sender: TObject);
var
  i: Integer;
  vNotas: TArray<RecDocumentosPreEntradas>;

  vSqlNotas: string;
  vSqlCTes: string;
  vSqlFinal: string;
begin
  inherited;
  sgDocs.ClearGrid;

  if not FrFornecedores.EstaVazio then begin
    _Biblioteca.WhereOuAnd(vSqlNotas, FrFornecedores.getSqlFiltros('CAD.CADASTRO_ID'));
    _Biblioteca.WhereOuAnd(vSqlCTes, FrFornecedores.getSqlFiltros('CAD.CADASTRO_ID'));
  end;

  if not FrDataEmissao.NenhumaDataValida then begin
    _Biblioteca.WhereOuAnd(vSqlNotas, FrDataEmissao.getSqlFiltros('DATA_HORA_EMISSAO'));
    _Biblioteca.WhereOuAnd(vSqlCTes, FrFornecedores.getSqlFiltros('CAD.CADASTRO_ID'));
  end;

  if not FrNumeroPreEntradasNotas.EstaVazio then
    _Biblioteca.WhereOuAnd(vSqlNotas, FrNumeroPreEntradasNotas.getSqlFiltros('PRE_ENTRADA_ID'));

  if not FrNumeroPreEntradasCTEs.EstaVazio then
    _Biblioteca.WhereOuAnd(vSqlCTes, FrNumeroPreEntradasCTEs.getSqlFiltros('PRE_ENTRADA_ID'));

  if not FrNumeroNota.EstaVazio then
    _Biblioteca.WhereOuAnd(vSqlNotas, FrNumeroNota.getSqlFiltros('NUMERO_NOTA'));

  if not FrNumeroCTe.EstaVazio then
    _Biblioteca.WhereOuAnd(vSqlCTes, FrNumeroCTe.getSqlFiltros('NUMERO_CTE'));

  _Biblioteca.WhereOuAnd(vSqlNotas, gbStatus.GetSql('STATUS'));
  _Biblioteca.WhereOuAnd(vSqlCTes, gbStatusCTeAltis.GetSql('STATUS'));

  vSqlFinal := ' order by DATA_HORA_EMISSAO desc';

  vNotas := _PreEntradasDocumentosFiscais.BuscarDocumentos(Sessao.getConexaoBanco, vSqlNotas, vSqlCTes, vSqlFinal);
  if vNotas = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  for i := Low(vNotas) to High(vNotas) do begin
    sgDocs.Cells[coNumero, i + 1]       := NFormat(vNotas[i].NumeroDocumento);
    sgDocs.Cells[coTIpoNota, i + 1]     := vNotas[i].TipoNotaAnaltico;
    sgDocs.Cells[coSerie, i + 1]        := vNotas[i].SerieDocumento;
    sgDocs.Cells[coStatusSEFAZ, i + 1]  := vNotas[i].StatusSEFAZAnalitico;
    sgDocs.Cells[coCNPJEmitente, i + 1] := vNotas[i].CnpjEmitente;
    sgDocs.Cells[coRazaoSocial, i + 1]  := vNotas[i].RazaoSocialEmitente;
    sgDocs.Cells[coStatusAltis, i + 1]  := vNotas[i].StatusAltisAnalitico;
    sgDocs.Cells[coDataEmissao, i + 1]  := DHFormat(vNotas[i].DataHoraEmissao);
    sgDocs.Cells[coValorTotal, i + 1]   := NFormatN(vNotas[i].ValorDocumento);
    sgDocs.Cells[coChaveNFe, i + 1]     := vNotas[i].ChaveAcesso;

    sgDocs.Cells[coStatusSEFAZSintetico, i + 1] := vNotas[i].StatusSefaz;
    sgDocs.Cells[coStatusAltisSintetico, i + 1] := vNotas[i].StatusAltis;
    sgDocs.Cells[coPreEntradaid, i + 1]         := NFormat(vNotas[i].PreEntradaId);
  end;
  sgDocs.SetLinhasGridPorTamanhoVetor( Length(vNotas) );

  SetarFoco(sgDocs);
end;

procedure TFormPreEntradasDocumentosFiscais.miBuscarNovasNotasFiscaisClick(Sender: TObject);
var
  vNFe: TComunicacaoNFe;
begin
  inherited;
  vNFe := TComunicacaoNFe.Create(Self);

  if vNFe.ConsultarNFeDistribuicao then begin
    _Biblioteca.RotinaSucesso;
    Carregar(Sender);
  end;

  vNFe.Free;
end;

procedure TFormPreEntradasDocumentosFiscais.miBuscarnovosCTEsSEFAZ1Click(Sender: TObject);
var
  vNFe: TComunicacaoNFe;
begin
  inherited;
  vNFe := TComunicacaoNFe.Create(Self);

  if vNFe.ConsultarCTeDistribuicao then begin
    _Biblioteca.RotinaSucesso;
    Carregar(Sender);
  end;

  vNFe.Free;
end;

procedure TFormPreEntradasDocumentosFiscais.miDesconhecimentoOperacaoClick(Sender: TObject);
var
  vPreEntradaId: Integer;
  vNFe: TComunicacaoNFe;
begin
  inherited;

  if sgDocs.Cells[coStatusAltisSintetico, sgDocs.Row] <> 'AMA' then begin
    _Biblioteca.Exclamar('Apenas documentos no status "Ag.manifesta��o" podem ter o desconhecimento da opera��o realizado!');
    Exit;
  end;

  vPreEntradaId := SFormatInt(sgDocs.Cells[coPreEntradaid, sgDocs.Row]);
  if vPreEntradaId = 0 then
    Exit;

  vNFe := TComunicacaoNFe.Create(Self);

  if vNFe.ManifestarDesconhecimento(vPreEntradaId) then begin
    _Biblioteca.RotinaSucesso;
    Carregar(Sender);
  end;

  vNFe.Free;
end;

procedure TFormPreEntradasDocumentosFiscais.miManifestarCienciaOperacaoClick(Sender: TObject);
var
  vPreEntradaId: Integer;
  vNFe: TComunicacaoNFe;
begin
  inherited;

  if sgDocs.Cells[coStatusAltisSintetico, sgDocs.Row] <> 'AMA' then begin
    _Biblioteca.Exclamar('Apenas documentos no status "Ag.manifesta��o" podem ter a ci�ncia da opera��o realizada!');
    Exit;
  end;

  vPreEntradaId := SFormatInt(sgDocs.Cells[coPreEntradaid, sgDocs.Row]);
  if vPreEntradaId = 0 then
    Exit;

  vNFe := TComunicacaoNFe.Create(Self);

  if vNFe.ManifestarCienciaNota(vPreEntradaId) then begin
    _Biblioteca.RotinaSucesso;
    Carregar(Sender);
  end;

  vNFe.Free;
end;

procedure TFormPreEntradasDocumentosFiscais.miOperacaoNaoRealizadaClick(Sender: TObject);
var
  vPreEntradaId: Integer;
  vNFe: TComunicacaoNFe;
begin
  inherited;

  if sgDocs.Cells[coStatusAltisSintetico, sgDocs.Row] <> 'AMA' then begin
    _Biblioteca.Exclamar('Apenas documentos no status "Ag.manifesta��o" podem ter a ci�ncia da opera��o realizada!');
    Exit;
  end;

  vPreEntradaId := SFormatInt(sgDocs.Cells[coPreEntradaid, sgDocs.Row]);
  if vPreEntradaId = 0 then
    Exit;

  vNFe := TComunicacaoNFe.Create(Self);

  if vNFe.ManifestarOperacaoNaoRealizada(vPreEntradaId) then begin
    _Biblioteca.RotinaSucesso;
    Carregar(Sender);
  end;

  vNFe.Free;
end;

procedure TFormPreEntradasDocumentosFiscais.miRealizarEntradaViaXMLClick(Sender: TObject);
begin
  inherited;

  if sgDocs.Cells[coTipoNota, sgDocs.Row] = 'NFe' then begin
    if sgDocs.Cells[coStatusAltisSintetico, sgDocs.Row] <> 'MAN' then begin
      _Biblioteca.Exclamar('Apenas notas fiscais "Manifestada" podem ter a entrada de nota fiscal realizada!');
      Exit;
    end;

    EntradaNotasFiscais.RelizarEntradaViaPreEntrada(SFormatInt(sgDocs.Cells[coPreEntradaId, sgDocs.Row]));
  end
  else begin
    if sgDocs.Cells[coStatusAltisSintetico, sgDocs.Row] <> 'AEN' then begin
      _Biblioteca.Exclamar('Apenas CTEs "Ag.entrada" podem ter a entrada de CTe realizada!');
      Exit;
    end;

    if _PreEntradasDocumentosFiscais.ExistemNotasNaoManifestadas(Sessao.getConexaoBanco, SFormatInt(sgDocs.Cells[coPreEntradaId, sgDocs.Row])) then begin
      if
        not _Biblioteca.Perguntar(
          'Existem notas fiscais referenciadas por este CTe que n�o foram manifestadas no Hiva, n�o ser� poss�vel fazer o vinculado ' +
          'autom�tico, deseja realmente continuar?'
        )
      then
        Exit;
    end;

    ConhecimentosFretes.RelizarEntradaViaPreEntrada(SFormatInt(sgDocs.Cells[coPreEntradaId, sgDocs.Row]));
  end;
end;

procedure TFormPreEntradasDocumentosFiscais.sgDocsDblClick(Sender: TObject);
begin
  inherited;
  if sgDocs.Cells[coTipoNota, sgDocs.Row] = 'CTe' then
    InformacoesPreEntradaCTe.Informar( SFormatInt(sgDocs.Cells[coPreEntradaId, sgDocs.Row]) )
  else
    InformacoesPreEntradaNotasFiscais.Informar( SFormatInt(sgDocs.Cells[coPreEntradaId, sgDocs.Row]) );
end;

procedure TFormPreEntradasDocumentosFiscais.sgDocsDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coPreEntradaId, coValorTotal, coNumero, coSerie] then
    vAlinhamento := taRightJustify
  else if ACol in[coTipoNota, coStatusAltis, coStatusSEFAZ, coChaveNFe] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgDocs.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormPreEntradasDocumentosFiscais.sgDocsGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coStatusSEFAZ then begin
    AFont.Style := [fsBold];
    if sgDocs.Cells[coStatusSEFAZ, ARow] = 'Uso autorizado' then
      AFont.Color := clBlue
    else
      AFont.Color := clRed;
  end
  else if ACol = coStatusAltis then begin
    AFont.Style := [fsBold];
    AFont.Color := getCorStatusAltis(sgDocs.Cells[coStatusAltisSintetico, ARow]);
  end
  else if ACol = coTipoNota then begin
    AFont.Style := [fsBold];
    AFont.Color := getCorTipoNota(sgDocs.Cells[coTipoNota, ARow]);
  end;
end;

end.
