unit PDV;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPrincipal,
  Vcl.StdCtrls, _Biblioteca, _Sessao, EditLuka, Vcl.Grids, GridLuka, Vcl.Buttons, _ProdutosVendaPDV, _RecordsPDV, _ComunicacaoNFE, Vcl.Menus,
  _FrameHerancaPrincipal, FrameImagem, Vcl.ExtCtrls, _RecordsNFE, Buscar.DadosClientePDV, Buscar.Vendedor, Buscar.FechamentoPDV, _RecordsEspeciais, _Orcamentos,
  System.StrUtils, _RecordsOrcamentosVendas, OutrasOperacoes, _ComunicacaoTEF, _OrcamentosPagamentos, _NotasFiscais, PesquisaProdutosVenda, _RecordsCadastros,
  _CondicoesPagamento, Impressao.ComprovantePagamentoGrafico, ImpressaoComprovanteCartao,
  Vcl.Imaging.jpeg, _Balancas;

type
  TFormPDV = class(TFormHerancaPrincipal)
    sgItens: TGridLuka;
    eCodigoProduto: TEditLuka;
    lb1: TLabel;
    eQuantidade: TEditLuka;
    lb2: TLabel;
    lbNomeProduto: TLabel;
    lb4: TLabel;
    eValorUnitario: TEditLuka;
    lb5: TLabel;
    lb6: TLabel;
    eValorTotal: TEditLuka;
    sbIniciarVenda: TSpeedButton;
    sbDefinirVendedor: TSpeedButton;
    sbPesquisarProduto: TSpeedButton;
    sbDefinirCliente: TSpeedButton;
    sbCancelarItem: TSpeedButton;
    sbCancelarVenda: TSpeedButton;
    sbOutrasOperacoes: TSpeedButton;
    lbUnidade: TLabel;
    sbDefinirQuantidade: TSpeedButton;
    sbFinalizarVenda: TSpeedButton;
    lb8: TLabel;
    FrImagemProduto: TFrImagem;
    miProximafoto: TMenuItem;
    miAnterior: TMenuItem;
    lbValorTotal: TLabel;
    lbQuantidadeItens: TLabel;
    lbItens: TLabel;
    imgPrincipalpdv: TImage;
    procedure FormCreate(Sender: TObject);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sbIniciarVendaClick(Sender: TObject);
    procedure sbCancelarVendaClick(Sender: TObject);
    procedure eCodigoProdutoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbDefinirQuantidadeClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbDefinirVendedorClick(Sender: TObject);
    procedure sbDefinirClienteClick(Sender: TObject);
    procedure sbCancelarItemClick(Sender: TObject);
    procedure sbOutrasOperacoesClick(Sender: TObject);
    procedure sbFinalizarVendaClick(Sender: TObject);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgItensDblClick(Sender: TObject);
    procedure miProximafotoClick(Sender: TObject);
    procedure miAnteriorClick(Sender: TObject);
    procedure sgItensClick(Sender: TObject);
    procedure sgItensKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbPesquisarProdutoClick(Sender: TObject);
    procedure eValorUnitarioKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eValorUnitarioKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure eQuantidadeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    //vBalanca : TBalancas;
    FIndexFoto: Integer;
    FVendedorId: Integer;
    FEmModoCancelamento: Boolean;
    FDadosCliente: RecDadosCliente;

    FProduto: RecProdutosVendas;
    FCondicaoPadrao: RecCondicoesPagamento;

    procedure CalcularTotalItens;
    procedure Habilitar(Sender: TObject; pHabilitar: Boolean);
    procedure ControlarModoCancelamento;

    procedure AddNoGrid;
    procedure CancelarItem(pLinha: Integer);

    procedure RatearValoresItens(var pItens: TArray<RecOrcamentoItens>; pValorOutrasDespesas: Double);


  end;

  Function _AbrePortaSerialBal(Canal: AnsiString): Integer; stdcall; external 'LePeso.dll';
  Function _FechaPortaSerialBal(): Integer; stdcall; external 'LePeso.dll';
  Function _AlteraModeloBalanca(Modelo: Integer): Integer; stdcall; external 'LePeso.dll';
  Function _AlteraModoOperacao(Modo: Integer): Integer; stdcall; external 'LePeso.dll';
  Function _LePeso(): PAnsiChar; stdcall; external 'LePeso.dll';

implementation

{$R *.dfm}

uses
  BuscarDadosCartoesTEF, _CodigoBarras, _ImpressorasEstacoes,
  FrameCondicoesPagamento;

{ TFormPDV }

resourcestring
  stNormal    = 'NORMAL';
  stCancelado = 'CANCELADO';

const
  coProdutoId           = 0;
  coItemId              = 1;
  coNomeProduto         = 2;
  coQuantidade          = 3;
  coUnidade             = 4;
  coValorUnit           = 5;
  coValorTotal          = 6;
  coTipoControleEstoque = 7;

  (* Colunas ocultas *)
  coStatus              = 8;
  coPrecoVarejo         = 9;
  coPrecoPromocional    = 10;
  coTipoPreco           = 11;
  coPrecoBase           = 12;

  coFoto1 = 0;
  coFoto2 = 1;
  coFoto3 = 2;

procedure TFormPDV.CalcularTotalItens;
var
  i: Integer;
  vQtdeItens: Integer;
  vTotalItens: Double;
begin
  vQtdeItens := 0;
  vTotalItens := 0;

  for i := sgItens.FixedRows to sgItens.RowCount - 1 do begin
    Inc(vQtdeItens);
    vTotalItens := vTotalItens + SFormatDouble(sgItens.Cells[coValorTotal, i]);
  end;

  lbQuantidadeItens.Caption := NFormatN(vQtdeItens);
  lbValorTotal.Caption := NFormatN(vTotalItens);
end;

procedure TFormPDV.CancelarItem(pLinha: Integer);
begin
  if not FEmModoCancelamento then
    Exit;

  if sgItens.Cells[coItemId, pLinha] = '' then
    Exit;

  if not Perguntar('Deseja realmente cancelar este item?') then
    Exit;

  sgItens.EditorMode := False;
  sgItens.DeleteRow(pLinha);
  sgItens.Repaint;
  sgItens.EditorMode := True;

  CalcularTotalItens;
  ControlarModoCancelamento;
end;

procedure TFormPDV.ControlarModoCancelamento;
begin
  eValorTotal.Clear;
  eValorUnitario.Clear;
  FrImagemProduto.Clear;
  lbUnidade.Caption := '';
  FEmModoCancelamento := not FEmModoCancelamento;

  _Biblioteca.Habilitar([
    eQuantidade,
    eCodigoProduto,
    eValorUnitario,
    eValorTotal],
    not FEmModoCancelamento
  );

  if FEmModoCancelamento then begin
    lbNomeProduto.Caption := 'MODO CANCELAMENTO DE PRODUTO';
    lbNomeProduto.Font.Color := clRed;

    sbCancelarItem.Caption := 'F5 Para canc.';
    SetarFoco(sgItens);
    sgItensClick(nil);
  end
  else begin
    eQuantidade.AsDouble := 1;
    sbCancelarItem.Caption := 'F5 Excluir item';

    lbNomeProduto.Caption := '';
    lbNomeProduto.Font.Color := clBlack;

    SetarFoco(eCodigoProduto);
  end;
end;

procedure TFormPDV.eCodigoProdutoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  vProduto: TArray<RecProdutosVendas>;
  vCodigoProd: string;
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if eCodigoProduto.Text = '' then begin
    Exclamar('� necess�rio informar o c�digo do produto!');
    SetarFoco(eCodigoProduto);
    Exit;
  end;

//  vProduto := PesquisaProdutosVenda.PesquisarProdutosVendas(Sessao.getParametrosEmpresa.CondicaoPagamentoPDV, False, False, true);

//  if Length(eCodigoProduto.Text) > 10 then
//    vProduto[0].produto_id := _CodigoBarras.getProdutoId(eCodigoProduto.Text)
//  else
//    vProduto[0].produto_id := SFormatInt(eCodigoProduto.Text);


//  if vProduto[0].produto_id  0 then
  vProduto := _CodigoBarras.TratarCodigoBarras(eCodigoProduto.Text);
  if vProduto = nil then
  begin
    vCodigoProd :=  IntToStr(_CodigoBarras.getProdutoId(eCodigoProduto.Text));
    if StrToInt(vCodigoProd) < 0 then
      vProduto    := _CodigoBarras.TratarCodigoBarras(vCodigoProd)
    else if (vProduto = nil) and (length(vCodigoProd) < 11) then
      vProduto := PesquisaProdutosVenda.PesquisarProdutosVendas(Sessao.getParametrosEmpresa.CondicaoPagamentoPDV, False, False, true);


  end;


  if vProduto = nil then begin
    Exclamar('Produto n�o encontrado!');
    SetarFoco(eCodigoProduto);
    eCodigoProduto.Clear;
    Exit;
  end;

  FProduto := vProduto[0];

  if FProduto.ControlaPeso = 'S' then
      eQuantidade.AsDouble := StrToFloatDef( copy(_LePeso(),9,16),0);

  if FProduto.preco_pdv = 0 then begin
    Exclamar('Produto sem pre�o!');
    SetarFoco(eCodigoProduto);
    eCodigoProduto.Clear;
    Exit;
  end;

  if FProduto.TipoControleEstoque <> 'N' then begin
    Exclamar('N�o � poss�vel venda deste tipo de produtos pela venda r�pida!');
    SetarFoco(eCodigoProduto);
    eCodigoProduto.Clear;
    Exit;
  end;

  eValorUnitario.TabStop := FProduto.produtoDiversosPDV = 'S';

  if not ValidarMultiplo( eQuantidade.AsDouble, FProduto.multiplo_venda ) then begin
    SetarFoco(eQuantidade);
    Exit;
  end;

  if FProduto.ControlaPeso = 'S' then
  begin
    if eQuantidade.AsDouble <= 0 then
    begin
      Exclamar('Coloque o produto sobre a balan�a!');
      SetarFoco(eCodigoProduto);
      Exit;
    end;
    AddNoGrid;
  end else
    ProximoCampo(Sender, Key, Shift);

end;

procedure TFormPDV.eQuantidadeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) and (FProduto.ControlaPeso = 'N') and (FProduto.produtoDiversosPDV = 'N') then
    AddNoGrid
  else
    ProximoCampo(Sender, Key, Shift);

end;

procedure TFormPDV.eValorUnitarioKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if eValorUnitario.AsCurr = 0 then begin
    Exclamar('Insira o valor do item!');
    SetarFoco(eValorUnitario);
    Exit;
  end;

//  FProduto.ValorTotal := eValorTotal.AsCurr;
//  FProduto.PrecoPDV := eValorUnitario.AsCurr;
    FProduto.preco_pdv := eValorUnitario.AsCurr;

  AddNoGrid;
end;

procedure TFormPDV.eValorUnitarioKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  eValorTotal.AsCurr := _Biblioteca.Arredondar(eValorUnitario.AsCurr * eQuantidade.AsCurr, 2);
end;

procedure TFormPDV.AddNoGrid;
var
  vLinha: Integer;

  vTipoPreco: string;
  vPrecoBase: Double;
  vPrecoUnitario: Double;
begin
  if sgItens.Cells[coItemId, 1] = '' then
    vLinha := 1
  else begin
    vLinha := sgItens.RowCount;
    sgItens.RowCount := sgItens.RowCount + 1;
  end;

  sgItens.Cells[coProdutoId, vLinha]           := NFormat(FProduto.produto_id);
  sgItens.Cells[coItemId, vLinha]              := NFormat(vLinha);
  sgItens.Cells[coNomeProduto, vLinha]         := FProduto.nome;
  sgItens.Cells[coQuantidade, vLinha]          := NFormat(SFormatDouble(sgItens.Cells[coQuantidade, vLinha]) + eQuantidade.AsDouble, 4);
  sgItens.Cells[coUnidade, vLinha]             := FProduto.unidade_venda;
  sgItens.Cells[coTipoControleEstoque, vLinha] := FProduto.TipoControleEstoque;
  sgItens.Cells[coStatus, vLinha]              := stNormal;
  sgItens.Objects[coFoto1, vLinha]             := FProduto.Foto1;
  sgItens.Objects[coFoto2, vLinha]             := FProduto.Foto2;
  sgItens.Objects[coFoto3, vLinha]             := FProduto.Foto3;

  if (FCondicaoPadrao.PermitirPrecoPromocional = 'S') and (FProduto.PrecoPromocional > 0) then begin
    vTipoPreco := 'PRO';
    vPrecoBase := FProduto.PrecoPromocional;
    vPrecoUnitario := Arredondar(FProduto.PrecoPromocional * IIfDbl( FCondicaoPadrao.AplicarIndPrecoPromocional = 'S', FCondicaoPadrao.indice_acrescimo, 1 ), 2);
  end
  else begin
    vTipoPreco := 'PDV';
    vPrecoBase := FProduto.preco_pdv;
    vPrecoUnitario := Arredondar(FProduto.preco_pdv * FCondicaoPadrao.indice_acrescimo, 2);
  end;

  sgItens.Cells[coValorUnit, vLinha]           := NFormat(vPrecoUnitario);
  sgItens.Cells[coValorTotal, vLinha]          := NFormat(Arredondar(vPrecoUnitario * SFormatDouble(sgItens.Cells[coQuantidade, vLinha]), 2));
  sgItens.Cells[coPrecoVarejo, vLinha]         := NFormat(FProduto.preco_pdv);
  sgItens.Cells[coPrecoPromocional, vLinha]    := NFormat(FProduto.PrecoPromocional);
  sgItens.Cells[coPrecoBase, vLinha]           := NFormat(vPrecoBase);
  sgItens.Cells[coTipoPreco, vLinha]           := vTipoPreco;

  sgItens.Row := vLinha;
  sgItensClick(nil);

  eQuantidade.AsDouble := 1;

  CalcularTotalItens;
  eCodigoProduto.Clear;
  //eValorUnitario.ReadOnly := True;
  SetarFoco(eCodigoProduto);
end;

procedure TFormPDV.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
//  vBalanca.FechaPortaSerialBal;
//  vBalanca.Free;
  _FechaPortaSerialBal;
end;

procedure TFormPDV.FormCreate(Sender: TObject);
var
  vImpressoras: TArray<RecImpressorasEstacoes>;
begin
  inherited;
  if Sessao.getEmpresaLogada.TipoEmpresa = 'D' then begin
    _Biblioteca.RotinaNaoPermitidaDepositoFechado;
    Close;
  end;

  FCondicaoPadrao := _CondicoesPagamento.BuscarCondicoesPagamentos(Sessao.getConexaoBanco, 0, [Sessao.getParametrosEmpresa.CondicaoPagamentoPDV], False, False)[0];
  FrImagemProduto.SomenteLeitura(True);
  Habilitar(Sender, False);



//  if not Assigned(vBalanca)then
//    vBalanca := TBalancas.Create;

  vImpressoras := _ImpressorasEstacoes.BuscarImpressoras(Sessao.getConexaoBanco, 0, [_Biblioteca.NomeComputador]);
  if UpperCase(vImpressoras[0].ModeloBalanca).Contains('URANO 10 OU 11') then
    _AlteraModeloBalanca(0)
  else if UpperCase(vImpressoras[0].ModeloBalanca).Contains('URANO 6') then
    _AlteraModeloBalanca(1)
  else if UpperCase(vImpressoras[0].ModeloBalanca).Contains('URANO 12 OU UDC POP') then
    _AlteraModeloBalanca(2)
  else if UpperCase(vImpressoras[0].ModeloBalanca).Contains('US POP') then
    _AlteraModeloBalanca(3)
  else if UpperCase(vImpressoras[0].ModeloBalanca).Contains('CP POP L0') then
    _AlteraModeloBalanca(4)
  else if UpperCase(vImpressoras[0].ModeloBalanca).Contains('URANO CP POP1') then
    _AlteraModeloBalanca(5)
  else if UpperCase(vImpressoras[0].ModeloBalanca).Contains('URANO C') then
    _AlteraModeloBalanca(6)
  else if UpperCase(vImpressoras[0].ModeloBalanca).Contains('F-PZ-MF') then
    _AlteraModeloBalanca(7)
  else if UpperCase(vImpressoras[0].ModeloBalanca).Contains('T-PZ-P3') then
    _AlteraModeloBalanca(8)
  else if UpperCase(vImpressoras[0].ModeloBalanca).Contains('US POP L') then
    _AlteraModeloBalanca(9)
  else if UpperCase(vImpressoras[0].ModeloBalanca).Contains('US POP AF') then
    _AlteraModeloBalanca(10);

  _AlteraModoOperacao(0);

  if _AbrePortaSerialBal(vImpressoras[0].PortaBalanca) < 1 then
    Exclamar('Erro ao abrir a porta da balan�a: '+ vImpressoras[0].PortaBalanca);

end;

procedure TFormPDV.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;

  if Shift = [ssCtrl, ssShift] then
    Exit;

  case Key of
    VK_F1: begin
      if not sbIniciarVenda.Enabled then
        Exit;

      sbIniciarVendaClick(Sender);
    end;

    VK_F2: begin
      if not sbDefinirVendedor.Enabled then
        Exit;

      sbDefinirVendedorClick(Sender);
    end;

    VK_F3: begin
      if not sbDefinirCliente.Enabled then
        Exit;

      sbDefinirClienteClick(Sender);
    end;

//    VK_F4: begin
//      if not sbDefinirQuantidade.Enabled then
//        Exit;
//
//      sbDefinirQuantidadeClick(Sender);
//    end;

    VK_F4: begin
      if not sbPesquisarProduto.Enabled then
        Exit;

      sbPesquisarProdutoClick(Sender);
    end;

    VK_F5: begin
      if not sbCancelarItem.Enabled then
        Exit;

      sbCancelarItemClick(Sender);
    end;

    VK_F6: begin
      if not sbCancelarVenda.Enabled then
        Exit;

      sbCancelarVendaClick(Sender);
    end;

    VK_F7: begin
      if not sbFinalizarVenda.Enabled then
        Exit;

      sbFinalizarVendaClick(Sender);
    end;


//    VK_F12: begin
//      if not sbOutrasOperacoes.Enabled then
//     Exit;
//
//      sbOutrasOperacoesClick(Sender);
//    end;
  end;
end;

procedure TFormPDV.Habilitar(Sender: TObject; pHabilitar: Boolean);
begin
  _Biblioteca.Habilitar([
    eQuantidade,
    eCodigoProduto,
    eValorUnitario,
    eValorTotal,
    sgItens,
    sbDefinirVendedor,
    sbPesquisarProduto,
    sbDefinirCliente,
    sbFinalizarVenda,
    sbCancelarVenda,
    sbDefinirQuantidade,
    sbCancelarItem,
    lbQuantidadeItens,
    lbValorTotal,
    FrImagemProduto],
    pHabilitar
  );

  _Biblioteca.Habilitar([sbIniciarVenda, sbOutrasOperacoes], not pHabilitar);

  //FItensCupom := nil;
  lbUnidade.Caption := '';
  eQuantidade.AsDouble := 1;
  lbQuantidadeItens.Caption := '0';
  lbValorTotal.Caption := '0,00';

  if Sender <> sbIniciarVenda then begin
    FVendedorId := 0;
    FDadosCliente.NomeCliente := '';
    FDadosCliente.Cpf         := '';
    FDadosCliente.Endereco    := '';
    FDadosCliente.Telefone    := '';
  end;

  if pHabilitar then begin
    Self.BorderIcons := [];
    SetarFoco(eCodigoProduto);
    lbNomeProduto.Caption := '';
    SetForegroundWindow(Application.Handle);
  end
  else begin
    FDadosCliente.ClienteId := Sessao.getParametros.cadastro_consumidor_final_id;
    Self.BorderIcons := [biSystemMenu];
    lbNomeProduto.Caption := 'Caixa aberto';
  end;
end;

procedure TFormPDV.miAnteriorClick(Sender: TObject);
begin
  inherited;
  Dec(FIndexFoto);
  if FIndexFoto < 1 then begin
    FIndexFoto := 1;
    Exit;
  end;

  case FIndexFoto of
    2: FrImagemProduto.setFoto( TMemoryStream(sgItens.Objects[coFoto2, sgItens.Row]) );
    1: FrImagemProduto.setFoto( TMemoryStream(sgItens.Objects[coFoto1, sgItens.Row]) );
  end;
end;

procedure TFormPDV.miProximafotoClick(Sender: TObject);
begin
  inherited;
  Inc(FIndexFoto);
  if FIndexFoto > 3 then begin
    FIndexFoto := 3;
    Exit;
  end;

  case FIndexFoto of
    2: begin
      if sgItens.Objects[coFoto2, sgItens.Row] = nil then begin
        FIndexFoto := 1;
        Exit;
      end;

      FrImagemProduto.setFoto( TMemoryStream(sgItens.Objects[coFoto2, sgItens.Row]) );
    end;

    3: begin
      if sgItens.Objects[coFoto3, sgItens.Row] = nil then begin
        FIndexFoto := 2;
        Exit;
      end;

      FrImagemProduto.setFoto( TMemoryStream(sgItens.Objects[coFoto3, sgItens.Row]) );
    end;
  end;
end;

procedure TFormPDV.sbCancelarVendaClick(Sender: TObject);
begin
  inherited;
  if not Perguntar('Cancelar a venda?') then
    Exit;

  Habilitar(Sender, False);
end;

procedure TFormPDV.sbDefinirClienteClick(Sender: TObject);
var
  vDados: TRetornoTelaFinalizar<RecDadosCliente>;
begin
  inherited;
  vDados := Buscar.DadosClientePDV.Buscar(FDadosCliente);
  if vDados.RetTela = trCancelado then
    RotinaCanceladaUsuario
  else
    FDadosCliente := vDados.Dados;
end;

procedure TFormPDV.sbDefinirQuantidadeClick(Sender: TObject);
begin
  inherited;
  SetarFoco(eQuantidade);
  eQuantidade.AsDouble := 1;
end;

procedure TFormPDV.sbDefinirVendedorClick(Sender: TObject);
var
  vDados: TRetornoTelaFinalizar<Integer>;
begin
  inherited;
  vDados := Buscar.Vendedor.Buscar(FVendedorId);
  if vDados.RetTela = trCancelado then begin
    RotinaCanceladaUsuario;
    Exit;
  end;
  FVendedorId := vDados.Dados;
end;

procedure TFormPDV.RatearValoresItens(var pItens: TArray<RecOrcamentoItens>; pValorOutrasDespesas: Double);
var
  i: Integer;
  vIndiceRateio: Double;

  vValorFaltaRatearOutDespesas: Currency;

  vValorItemMaisSignificativo: Double;
  vLinhaItemMaisSignificativo: Integer;
begin
  vValorItemMaisSignificativo := 0;
  vLinhaItemMaisSignificativo := -1;

  vValorFaltaRatearOutDespesas := pValorOutrasDespesas;

  for i := Low(pItens) to High(pItens) do begin

    if pItens[i].valor_total > vValorItemMaisSignificativo then begin
      vLinhaItemMaisSignificativo := i;
      vValorItemMaisSignificativo := pItens[i].valor_total;
    end;

    vIndiceRateio := pItens[i].valor_total / SFormatDouble(lbValorTotal.Caption);

    if vValorFaltaRatearOutDespesas > 0 then begin
      pItens[i].valor_total_outras_despesas := _Biblioteca.Arredondar(pValorOutrasDespesas * vIndiceRateio, 2);
      vValorFaltaRatearOutDespesas := vValorFaltaRatearOutDespesas - pItens[i].valor_total_outras_despesas;
    end;
  end;

  if NFormatN(vValorFaltaRatearOutDespesas) <> '' then begin
    if vLinhaItemMaisSignificativo > -1 then
      pItens[vLinhaItemMaisSignificativo].valor_total_outras_despesas := pItens[vLinhaItemMaisSignificativo].valor_total_outras_despesas + vValorFaltaRatearOutDespesas;
  end;
end;

procedure TFormPDV.sbFinalizarVendaClick(Sender: TObject);
var
  i: Integer;
  j: Integer;
  vPosic: Integer;
  vEncontrou: Boolean;

  vTemProduto: Boolean;

  vRetProcessamentoTEF: RecRespostaTEF;

  vRetBanco: RecRetornoBD;
  vItens: TArray<RecOrcamentoItens>;
  vRetornoCartoes: TRetornoTelaFinalizar< TArray<RecRespostaTEF> >;
  vDadosFechamento: TRetornoTelaFinalizar<RecFechamento>;

  vOrcamentoId: Integer;

  vNFe: TComunicacaoNFe;
  vNotaFiscalId: Integer;

  procedure GerarErro(pMensagem: string; pHabilitarNovaVenda: Boolean);
  begin
    BlockInput(False);
    _Biblioteca.Exclamar(pMensagem);

    if pHabilitarNovaVenda then
      Habilitar(Sender, False);

    Abort;
  end;

begin
  inherited;
  vItens := nil;
  vTemProduto := False;
  for i := sgItens.FixedRows to sgItens.RowCount -1 do begin
    if sgItens.Cells[coProdutoId, i] = '' then
      Break;

    vPosic := -1;
    vTemProduto := True;
    vEncontrou := False;
    for j := Low(vItens) to High(vItens) do begin
      if SFormatInt(sgItens.Cells[coProdutoId, i]) = vItens[j].produto_id then begin
        vEncontrou := True;
        vPosic := j;
        Break;
      end;
    end;

    if vEncontrou then begin
      vItens[vPosic].quantidade           := vItens[vPosic].quantidade + SFormatDouble(sgItens.Cells[coQuantidade, i]);
      vItens[vPosic].QuantidadeRetirarAto := vItens[vPosic].quantidade;
      vItens[vPosic].valor_total          := vItens[vPosic].valor_total + SFormatDouble(sgItens.Cells[coValorTotal, i]);
    end
    else begin
      SetLength(vItens, Length(vItens) + 1);
      vItens[High(vItens)].produto_id                  := SFormatInt(sgItens.Cells[coProdutoId, i]);
      vItens[High(vItens)].item_id                     := Length(vItens);
      vItens[High(vItens)].quantidade                  := SFormatDouble(sgItens.Cells[coQuantidade, i]);
      vItens[High(vItens)].QuantidadeRetirarAto        := vItens[High(vItens)].quantidade;
      vItens[High(vItens)].preco_unitario              := SFormatDouble(sgItens.Cells[coValorUnit, i]);
      vItens[High(vItens)].valor_total_desconto        := 0;
      vItens[High(vItens)].valor_total                 := SFormatDouble(sgItens.Cells[coValorTotal, i]);
      vItens[High(vItens)].TipoControleEstoque         := sgItens.Cells[coTipoControleEstoque, i];
      vItens[High(vItens)].valor_total_outras_despesas := 0;
      vItens[High(vItens)].TipoPrecoUtilizado          := sgItens.Cells[coTipoPreco, i];

      vItens[High(vItens)].PrecoPromocional            := SFormatDouble(sgItens.Cells[coPrecoPromocional, i]);
      vItens[High(vItens)].preco_varejo                := SFormatDouble(sgItens.Cells[coPrecoVarejo, i]);
      vItens[High(vItens)].PrecoBase                   := SFormatDouble(sgItens.Cells[coPrecoBase, i]);
    end;
  end;

  if not vTemProduto then begin
    Exclamar('Nenhum produto foi adicionado na venda!');
    SetarFoco(eCodigoProduto);
    Exit;
  end;

  vDadosFechamento := Buscar.FechamentoPDV.Buscar(SFormatDouble(lbValorTotal.Caption), FDadosCliente.ClienteId, vItens);
  if vDadosFechamento.BuscaCancelada then
    Exit;

  RatearValoresItens(vItens, vDadosFechamento.Dados.ValorAcrescimoCartao);

  // Se houver TEF, chamando a tela que realizar� a passagem destes cart�es
  if (Sessao.getParametrosEstacao.UtilizarTef = 'S') and (vDadosFechamento.Dados.ValorCartaoDebito + vDadosFechamento.Dados.ValorCartaoCredito > 0) then begin
    vRetornoCartoes :=
      BuscarDadosCartoesTEF.BuscarPagamentosTEF(
        vDadosFechamento.Dados.ValorCartaoDebito,
        vDadosFechamento.Dados.ValorCartaoCredito,
        vDadosFechamento.Dados.QtdeMaximaParcelasCartao,
        False,
        False
      );

    if vRetornoCartoes.BuscaCancelada then begin
      BuscarDadosCartoesTEF.CancelarCartoes( vRetornoCartoes.Dados );
      RotinaCanceladaUsuario;
      Exit;
    end;

    vDadosFechamento.Dados.Cartoes := nil;
    SetLength(vDadosFechamento.Dados.Cartoes, Length(vRetornoCartoes.Dados));
    for i := Low(vRetornoCartoes.Dados) to High(vRetornoCartoes.Dados) do begin
      vDadosFechamento.Dados.Cartoes[i].CobrancaId := vRetornoCartoes.Dados[i].CobrancaId;
      vDadosFechamento.Dados.Cartoes[i].NsuTef     := vRetornoCartoes.Dados[i].Nsu;
      vDadosFechamento.Dados.Cartoes[i].ItemId     := i;
      vDadosFechamento.Dados.Cartoes[i].Valor      := vRetornoCartoes.Dados[i].Valor;
    end;
  end;

  vRetBanco :=
    _Orcamentos.AtualizarOrcamentoPDV(
      Sessao.getConexaoBanco,
      IfThen(FDadosCliente.ClienteId = Sessao.getParametros.cadastro_consumidor_final_id, FDadosCliente.NomeCliente, ''),
      '',
      Sessao.getEmpresaLogada.EmpresaId,
      FDadosCliente.ClienteId,
      IIfInt(vDadosFechamento.Dados.CondicaoCartaoId > 0, vDadosFechamento.Dados.CondicaoCartaoId, Sessao.getParametrosEmpresa.CondicaoPagamentoPDV),
      FVendedorId,
      1, // Indice da condi��o de pagamento
      SFormatDouble(lbValorTotal.Caption),
      SFormatDouble(lbValorTotal.Caption),
      SFormatDouble(lbValorTotal.Caption) + vDadosFechamento.Dados.ValorAcrescimoCartao, // Valor total
      vDadosFechamento.Dados.ValorAcrescimoCartao,
      vDadosFechamento.Dados.ValorDinheiro - vDadosFechamento.Dados.valorTroco,
      vDadosFechamento.Dados.ValorCartaoDebito,
      vDadosFechamento.Dados.ValorCartaoCredito,
      Sessao.getTurnoCaixaAberto.TurnoId,
      Sessao.getTurnoCaixaAberto.FuncionarioId,
      Self.tipoAnaliseCusto,
      0,
      vItens,
      vDadosFechamento.Dados.Cartoes,
      vDadosFechamento.Dados.valorTroco,
      nil,
      Sessao.getParametros.DefinirLocalManual,
      FDadosCliente.Cpf
    );

  if vRetBanco.TeveErro then
    GerarErro('Falha ao gravar venda no banco de dados!' + Chr(13) + Chr(10) + vRetBanco.MensagemErro, False);

  vOrcamentoId := vRetBanco.AsInt;

  // Emitindo o comprovante de pagamento
  if vDadosFechamento.Dados.EmitirReciboPagamento then
    Impressao.ComprovantePagamentoGrafico.Imprimir(vOrcamentoId);

  // Emitindo a NFCe
  if vDadosFechamento.Dados.TipoNotaGerar = 'NI' then
   // _Biblioteca.Exclamar('N�o foi poss�vel emitir a NFCe automaticamente, fa�a a emiss�o da mesma na tela "Rela��od e notas fiscais"!')
  else if vDadosFechamento.Dados.TipoNotaGerar = 'NE' then begin
    vNotaFiscalId := _NotasFiscais.BuscarNotaFiscalIdOrcamento(Sessao.getConexaoBanco, vOrcamentoId);

    vNFe := TComunicacaoNFe.Create(Application);
    if not vNFe.EmitirNFe(vNotaFiscalId) then
    //  _Biblioteca.Exclamar('N�o foi poss�vel emitir a NFCe automaticamente, fa�a a emiss�o da mesma na tela "Rela��od e notas fiscais"!');

    FreeAndNil(vNFe);
  end;

  // Imprimir as vias dos cart�es
  if Sessao.getParametrosEstacao.UtilizarTef = 'S' then begin
    for i := Low(vRetornoCartoes.Dados) to High(vRetornoCartoes.Dados) do
      ImpressaoComprovanteCartao.Imprimir( vRetornoCartoes.Dados[i].Comprovante1Via, vRetornoCartoes.Dados[i].Comprovante2Via );
  end;

  Habilitar(Sender, False);
end;

procedure TFormPDV.sbCancelarItemClick(Sender: TObject);
begin
  inherited;
  // if not xxx_paramentro then begin
  // Exit;

  ControlarModoCancelamento;
end;

procedure TFormPDV.sbIniciarVendaClick(Sender: TObject);
var
  vDadosCliente: TRetornoTelaFinalizar<RecDadosCliente>;
begin
  inherited;
  vDadosCliente.Dados.ClienteId := Sessao.getParametros.cadastro_consumidor_final_id;

  //if xxx.parametros then begin
//    vDadosCliente := Buscar.DadosClientePDV.Buscar(FDadosCliente);
//    if vDadosCliente.RetTela = trCancelado then begin
//      RotinaCanceladaUsuario;
//      Exit;
//    end;
//
//    FDadosCliente := vDadosCliente.Dados;

//  end;

  Habilitar(Sender, True);
end;

procedure TFormPDV.sbOutrasOperacoesClick(Sender: TObject);
begin
  inherited;
  OutrasOperacoes.AbrirTela;
end;

procedure TFormPDV.sbPesquisarProdutoClick(Sender: TObject);
var
  vProdutos: TArray<RecProdutosVendas>;
begin
  inherited;
  vProdutos := PesquisaProdutosVenda.PesquisarProdutosVendas(Sessao.getParametrosEmpresa.CondicaoPagamentoPDV, False, True, True);
  if vProdutos = nil then
    Exit;

  eCodigoProduto.Text := NFormatN(vProdutos[0].produto_id);
end;

procedure TFormPDV.sgItensClick(Sender: TObject);
begin
  inherited;
  if sgItens.Cells[coItemId, 1] = '' then
    Exit;

  lbNomeProduto.Caption   := sgItens.Cells[coProdutoId, sgItens.Row] + ' - ' + sgItens.Cells[coNomeProduto, sgItens.Row];
  eValorUnitario.AsDouble := SFormatDouble(sgItens.Cells[coValorUnit, sgItens.Row]);
  lbUnidade.Caption       := sgItens.Cells[coUnidade, sgItens.Row];
  eValorTotal.AsDouble    := SFormatDouble(sgItens.Cells[coValorTotal, sgItens.Row]);

  FrImagemProduto.Clear;

  if sgItens.Objects[coFoto1, sgItens.Row] = nil then
    Exit;

  FIndexFoto := 1;
  FrImagemProduto.setFoto( TMemoryStream(sgItens.Objects[coFoto1, sgItens.Row]) );
end;

procedure TFormPDV.sgItensDblClick(Sender: TObject);
begin
  inherited;
  CancelarItem(sgItens.Row);
end;

procedure TFormPDV.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coProdutoId, coItemId, coQuantidade, coValorUnit, coValorTotal] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormPDV.sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgItens.Cells[coStatus, ARow] = stCancelado then begin
    AFont.Color  := clRed;
    ABrush.Color := $00F4F4FF;
  end;
end;

procedure TFormPDV.sgItensKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    CancelarItem(sgItens.Row);
end;

end.
