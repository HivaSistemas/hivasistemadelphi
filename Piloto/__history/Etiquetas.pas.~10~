unit Etiquetas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, FrameProdutos, _FrameHenrancaPesquisas, Frame.DepartamentosSecoesLinhas, _FrameHerancaPrincipal, FrameDataInicialFinal,
  Vcl.Grids, GridLuka, Vcl.Menus, FrameMarcas, FrameFornecedores, FrameEmpresas, FrameCondicoesPagamento, Vcl.StdCtrls, RadioGroupLuka, ComboBoxLuka, EditLuka,
  _RecordsEspeciais, Vcl.Imaging.jpeg, CheckBoxLuka;

type
  TFormEtiquetas = class(TFormHerancaRelatoriosPageControl)
    FrDataCadastroProduto: TFrDataInicialFinal;
    FrDepartamentosSecoesLinhas: TFrameDepartamentosSecoesLinhas;
    FrProdutos: TFrProdutos;
    sgItens: TGridLuka;
    pmOpcoes: TPopupMenu;
    miMarcarDesmarcarSelecionado: TMenuItem;
    miMarcarDesmarcarTodos: TMenuItem;
    FrMarcas: TFrMarcas;
    FrFornecedores: TFrFornecedores;
    FrEmpresas: TFrEmpresas;
    FrCondicoesPagamento: TFrCondicoesPagamento;
    rgModelosEtiquetas: TRadioGroupLuka;
    cbTipoCodigoImpressao: TComboBoxLuka;
    Label1: TLabel;
    Label2: TLabel;
    eCaminhoImpressao: TEditLuka;
    imImagemEtiqueta: TImage;
    sbGravar: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure miMarcarDesmarcarSelecionadoClick(Sender: TObject);
    procedure miMarcarDesmarcarTodosClick(Sender: TObject);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgItensGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
    procedure sbGravarClick(Sender: TObject);
    procedure rgModelosEtiquetasClick(Sender: TObject);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    FNomeCondicaoPagamento: string;
  protected
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

uses _Biblioteca, _Sessao, _Imagens, _Etiquetas, _ImpressaoEtiquetasZebraEPL, _ImpressaoEtiquetas, _EtiquetasConfiguracoesTela, _ImagensEtiquetas;

{ TFormEtiquetas }

const
  coSelecionado    = 0;
  coProdutoId      = 1;
  coNome           = 2;
  coPrecoVarejo    = 3;
  coMarca          = 4;
  coUnidadeVenda   = 5;
  coCodigoBarras   = 6;
  coCodigoOriginal = 7;
  coQuantidade     = 8;

  //Ocultas
  coNomeMarca      = 9;

procedure TFormEtiquetas.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vProdutos: TArray<RecEtiquetas>;
begin
  inherited;
  vSql := '';
  sgItens.ClearGrid;
  FNomeCondicaoPagamento := FrCondicoesPagamento.getDados.nome;

  if not FrEmpresas.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrEmpresas.getSqlFiltros('PRE.EMPRESA_ID'));

  if not FrProdutos.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrProdutos.getSqlFiltros('PRO.PRODUTO_ID'));

  if not FrMarcas.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrMarcas.getSqlFiltros('PRO.MARCA_ID'));

  if not FrDepartamentosSecoesLinhas.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrDepartamentosSecoesLinhas.getSqlFiltros('PRO.LINHA_PRODUTO_ID'));

  if not FrFornecedores.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrFornecedores.getSqlFiltros('PRO.FORNECEDOR_ID'));

  if not FrDataCadastroProduto.NenhumaDataValida then
    _Biblioteca.WhereOuAnd(vSql, FrDataCadastroProduto.getSqlFiltros('PRO.DATA_CADASTRO'));

  _Biblioteca.WhereOuAnd(vSql, ' PRO.ATIVO = ''S'' ');
  vSql := vSql + ' order by PRO.NOME';

  vProdutos := _Etiquetas.BuscarProdutos(Sessao.getConexaoBanco, vSql);
  if vProdutos = nil then begin
    NenhumRegistro;
    Exit;
  end;

  for i := Low(vProdutos) to High(vProdutos) do begin
    sgItens.Cells[coSelecionado, i + 1]    := charNaoSelecionado;
    sgItens.Cells[coProdutoId, i + 1]      := _Biblioteca.NFormat(vProdutos[i].ProdutoId);
    sgItens.Cells[coNome, i + 1]           := vProdutos[i].NomeProduto;
    sgItens.Cells[coPrecoVarejo, i + 1]    := _Biblioteca.NFormat(vProdutos[i].PrecoVarejo * FrCondicoesPagamento.getDados.indice_acrescimo);
    sgItens.Cells[coMarca, i + 1]          := _Biblioteca.NFormat(vProdutos[i].MarcaId) + ' - ' + vProdutos[i].NomeMarca;
    sgItens.Cells[coUnidadeVenda, i + 1]   := vProdutos[i].UnidadeVenda;
    sgItens.Cells[coCodigoBarras, i + 1]   := vProdutos[i].CodigoBarras;
    sgItens.Cells[coCodigoOriginal, i + 1] := vProdutos[i].CodigoOriginal;
    sgItens.Cells[coQuantidade, i + 1]     := _Biblioteca.NFormat(1);
    sgItens.Cells[coNomeMarca, i + 1]      := vProdutos[i].NomeMarca;
  end;
  sgItens.SetLinhasGridPorTamanhoVetor(Length(vProdutos));
  SetarFoco(sgItens);
end;

procedure TFormEtiquetas.FormCreate(Sender: TObject);
var
  vEtiquetasConfiguracoesTela: TArray<RecEtiquetasConfiguracoesTela>;
begin
  inherited;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId);
  SetarFoco(FrCondicoesPagamento);
  FNomeCondicaoPagamento := '';

  vEtiquetasConfiguracoesTela := _EtiquetasConfiguracoesTela.BuscarEtiquetasConfiguracoesTela(Sessao.getConexaoBanco, 0, [Sessao.getEmpresaLogada.EmpresaId]);
  if vEtiquetasConfiguracoesTela <> nil then begin
    FrCondicoesPagamento.InserirDadoPorChave( vEtiquetasConfiguracoesTela[0].CondicaoId);
    cbTipoCodigoImpressao.SetIndicePorValor(vEtiquetasConfiguracoesTela[0].TipoCodigoImpressao);
    eCaminhoImpressao.Text := vEtiquetasConfiguracoesTela[0].CaminhoImpressao;
    rgModelosEtiquetas.ItemIndex := vEtiquetasConfiguracoesTela[0].ModeloId;
  end;

  rgModelosEtiquetasClick(nil);
end;

procedure TFormEtiquetas.Imprimir(Sender: TObject);
var
  i: Integer;
  vAchou: Boolean;
  vEtiqueta: TEtiqueta;
  vCodigoBarras: string;
  vDadosImpressao: TArray<RecImpressaoEtiqueta>;
begin
  inherited;
  vAchou := False;
  vEtiqueta := nil;
  vCodigoBarras := '';

  if sgItens.Cells[coProdutoId, sgItens.FixedRows] = '' then
    Exit;

  if Trim(eCaminhoImpressao.Text) = '' then begin
    _Biblioteca.Exclamar('� neces�rio informar o caminho de impress�o!');
    SetarFoco(eCaminhoImpressao);
    Exit;
  end;

  for i := sgItens.FixedRows to sgItens.RowCount - 1 do begin
    if
      (sgItens.Cells[coSelecionado, i] = charNaoSelecionado) or
      (SFormatInt(sgItens.Cells[coQuantidade, i]) < 1)
    then
      Continue;

    vAchou := True;

    if (cbTipoCodigoImpressao.GetValor = 'F') and (RetornaNumeros(sgItens.Cells[coCodigoBarras, i]) <> '') then
      vCodigoBarras := RetornaNumeros(sgItens.Cells[coCodigoBarras, i])
    else
      vCodigoBarras := LPad(RetornaNumeros(sgItens.Cells[coProdutoId, i]), 10, '0');

    SetLength(vDadosImpressao, Length(vDadosImpressao) + 1);
    vDadosImpressao[High(vDadosImpressao)].ProdutoId      := SFormatInt(sgItens.Cells[coProdutoId, i]);
    vDadosImpressao[High(vDadosImpressao)].NomeProduto    := sgItens.Cells[coNome, i];
    vDadosImpressao[High(vDadosImpressao)].CodigoOriginal := sgItens.Cells[coCodigoOriginal, i];
    vDadosImpressao[High(vDadosImpressao)].CodigoBarras   := vCodigoBarras;
    vDadosImpressao[High(vDadosImpressao)].UnidadeVenda   := sgItens.Cells[coUnidadeVenda, i];
    vDadosImpressao[High(vDadosImpressao)].NomeMarca      := sgItens.Cells[coNomeMarca, i];
    vDadosImpressao[High(vDadosImpressao)].NomeCondicao   := FNomeCondicaoPagamento;
    vDadosImpressao[High(vDadosImpressao)].PrecoVarejo    := SFormatDouble(sgItens.Cells[coPrecoVarejo, i]);
    vDadosImpressao[High(vDadosImpressao)].QtdEtiqueta    := SFormatInt(sgItens.Cells[coQuantidade, i]);
  end;

  if vAchou then begin
    //if rgModelosEtiquetas.ItemIndex = coModeloEtiqueta01 then
      vEtiqueta := TEtiquetaZebraEPL2.Create(rgModelosEtiquetas.ItemIndex, eCaminhoImpressao.Text, vDadosImpressao);

    vEtiqueta.Imprimir;
    RotinaSucesso;
  end
  else
    NenhumRegistroSelecionado;
end;

procedure TFormEtiquetas.miMarcarDesmarcarSelecionadoClick(Sender: TObject);
begin
  inherited;
  if sgItens.Cells[coProdutoId, sgItens.Row] = '' then
    Exit;

  sgItens.Cells[coSelecionado, sgItens.Row] := IIfStr(sgItens.Cells[coSelecionado, sgItens.Row] = charNaoSelecionado, charSelecionado, charNaoSelecionado);
end;

procedure TFormEtiquetas.miMarcarDesmarcarTodosClick(Sender: TObject);
var
  i: Integer;
  vMarcar: Boolean;
begin
  inherited;
  if sgItens.Cells[coProdutoId, sgItens.Row] = '' then
    Exit;

  vMarcar := sgItens.Cells[coSelecionado, sgItens.Row] = charNaoSelecionado;
  for i := 1 to sgItens.RowCount -1 do
    sgItens.Cells[coSelecionado, i] := IIfStr(vMarcar, charSelecionado, charNaoSelecionado);
end;

procedure TFormEtiquetas.rgModelosEtiquetasClick(Sender: TObject);
begin
  inherited;
  imImagemEtiqueta.Picture := _ImagensEtiquetas.BuscarImagemEtiqueta(Self, rgModelosEtiquetas.ItemIndex);

  case rgModelosEtiquetas.ItemIndex of
    coModeloEtiqueta01 : sgItens.HCol[8] := 'Quantidade';
    coModeloEtiqueta02 : sgItens.HCol[8] := 'Qtde Linha';
  end;
  sgItens.Refresh;
  Application.ProcessMessages;

end;

procedure TFormEtiquetas.sbGravarClick(Sender: TObject);
var
  condicao_id: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;

  condicao_id := 0;
  if not FrCondicoesPagamento.EstaVazio then
    condicao_id := FrCondicoesPagamento.getDados.condicao_id;

  vRetBanco :=
    _EtiquetasConfiguracoesTela.AtualizarEtiquetasConfiguracoesTela(
      Sessao.getConexaoBanco,
      Sessao.getEmpresaLogada.EmpresaId,
      condicao_id,
      cbTipoCodigoImpressao.GetValor,
      Trim(eCaminhoImpressao.Text),
      rgModelosEtiquetas.ItemIndex
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  RotinaSucesso;
end;

procedure TFormEtiquetas.sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
begin
  inherited;
  if (ARow = 0) or (ACol <> coQuantidade) then
    Exit;

  TextCell := NFormat(Abs(SFormatInt(sgItens.Cells[ACol, ARow])));
end;

procedure TFormEtiquetas.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in [coProdutoId, coPrecoVarejo, coQuantidade] then
    vAlinhamento := taRightJustify
  else if ACol in [coSelecionado, coUnidadeVenda] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormEtiquetas.sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coQuantidade then begin
    AFont.Color := _Biblioteca.coCorFonteEdicao2;
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
  end
end;

procedure TFormEtiquetas.sgItensGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if (ARow = 0) or (sgItens.Cells[coProdutoId, ARow] = '') then
    Exit;

  if ACol = coSelecionado then begin
    if sgItens.Cells[ACol, ARow] = charNaoSelecionado then
      APicture := _Imagens.FormImagens.im1.Picture
    else if sgItens.Cells[ACol, ARow] = charSelecionado then
      APicture := _Imagens.FormImagens.im2.Picture;
  end;
end;

procedure TFormEtiquetas.sgItensKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if sgItens.Cells[coProdutoId, sgItens.Row] = '' then
    Exit;

  if (Shift = [ssCtrl]) and (Key = VK_SPACE) then
    miMarcarDesmarcarTodosClick(Sender)
  else if (Key = VK_SPACE) then
    miMarcarDesmarcarSelecionadoClick(Sender);
end;

procedure TFormEtiquetas.sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coQuantidade then
    sgItens.Options := sgItens.Options + [goEditing]
  else
    sgItens.Options := sgItens.Options - [goEditing];
end;

procedure TFormEtiquetas.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrEmpresas.EstaVazio then begin
    _Biblioteca.Exclamar('� neces�rio informar a empresa!');
    SetarFoco(FrEmpresas);
    Abort;
  end;

  if FrCondicoesPagamento.EstaVazio then begin
    _Biblioteca.Exclamar('� neces�rio informar a condi��o de pagamento!');
    SetarFoco(FrCondicoesPagamento);
    Abort;
  end;
end;

end.
