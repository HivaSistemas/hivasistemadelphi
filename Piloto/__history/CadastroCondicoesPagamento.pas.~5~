unit CadastroCondicoesPagamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _RecordsCadastros, _Sessao,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, Vcl.ComCtrls, _Biblioteca, _FrameHerancaPrincipal, _FrameHenrancaPesquisas,
  FrameTiposCobranca, GroupBoxLuka, _RecordsEspeciais, _CondicoesPagamento, RadioGroupLuka, Logs,
  _TiposCobrancaCondicaoPagamento, PesquisaCondicoesPagamento, ComboBoxLuka, _CalculosSistema,
  FrameNumeros, _CondicoesPagamentoPrazos, _BibliotecaGenerica, CheckBoxLuka;

type
  TFormCadastroCondicoesPagamento = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eNome: TEditLuka;
    eIndiceAcrescimo: TEditLuka;
    lb2: TLabel;
    FrTiposCobranca: TFrTiposCobranca;
    rgTipoCondicao: TRadioGroupLuka;
    eValorMinimoVenda: TEditLuka;
    Label2: TLabel;
    ePercentualJuros: TEditLuka;
    Label3: TLabel;
    ePercentualDescontoComercial: TEditLuka;
    Label6: TLabel;
    Label8: TLabel;
    ckExigirNotaFiscal: TCheckBox;
    ckAplicarIndicePrecoPromocional: TCheckBox;
    ckPermitirUsoPontaEstoque: TCheckBox;
    ckPermitirUsoFechamentoAcum: TCheckBox;
    Label5: TLabel;
    cbTipoPreco: TComboBoxLuka;
    ePercEncargos: TEditLuka;
    lb4: TLabel;
    eValorMinimoParcela: TEditLuka;
    lb5: TLabel;
    FrDiasPrazo: TFrNumeros;
    ePrazoMedio: TEditLuka;
    lb6: TLabel;
    ePercentualDescontoPermitidoVenda: TEditLuka;
    lb3: TLabel;
    ePercCustoVenda: TEditLuka;
    lb7: TLabel;
    ckRestrita: TCheckBoxLuka;
    ckBloquearSempre: TCheckBoxLuka;
    ckPermitirPrecoProcional: TCheckBox;
    ckNaoPermitirConsFinal: TCheckBoxLuka;
    ckRecebimentoNaEntrega: TCheckBoxLuka;
    ckAcumulativa: TCheckBoxLuka;
    ckImprimirConfissaoDivida: TCheckBoxLuka;
    procedure ePercentualJurosChange(Sender: TObject);
    procedure ePercentualDescontoComercialChange(Sender: TObject);
    procedure cbQtdeMaximaParcelasChange(Sender: TObject);
    procedure ePercEncargosChange(Sender: TObject);
    procedure ePrazoMedioChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ckAcumulativaClick(Sender: TObject);
  private
    procedure PreencherRegistro(pCondicaoPagamento: RecCondicoesPagamento; pTiposCobrancaCondicaoPagamento: TArray<Integer>);
    procedure CalcularIndiceCondicao;
    procedure CalcularPrazoMedio;

    procedure FrDiasPrazoOnAposAdicionar(Sender: TObject);
    procedure FrDiasPrazoOnAposDeletar(Sender: TObject);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
    procedure ExibirLogs; override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroCondicoesPagamento }

procedure TFormCadastroCondicoesPagamento.BuscarRegistro;
var
  vCondicoesPagamento: TArray<RecCondicoesPagamento>;
  vTiposCobrCondicaoPagto: TArray<Integer>;
begin
  vCondicoesPagamento := _CondicoesPagamento.BuscarCondicoesPagamentos(Sessao.getConexaoBanco, 0, [eId.AsInt], False, False);
  if vCondicoesPagamento = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    _Biblioteca.SetarFoco(eID);
    Exit;
  end;

  vTiposCobrCondicaoPagto := _TiposCobrancaCondicaoPagamento.BuscarTiposCobrCondPagamentos(Sessao.getConexaoBanco, 0, [eId.AsInt]);

  inherited;
  PreencherRegistro(vCondicoesPagamento[0], vTiposCobrCondicaoPagto);
end;

procedure TFormCadastroCondicoesPagamento.CalcularIndiceCondicao;
var
  vIndice: Double;
  vPercJuros: Double;
begin
  inherited;
  vPercJuros := 0;

  (* Parte dos juros *)
  if (FrDiasPrazo.getValores <> nil) and (ePercentualJuros.AsCurr + ePercentualDescontoComercial.AsCurr > 0) and (rgTipoCondicao.GetValor = 'P') then begin
    vPercJuros :=
      Sessao.getCalculosSistema.RecCalculosJuros.CalcularJurosFuturos(
        1,
        FrDiasPrazo.getValores,
        Sessao.getCalculosSistema.RecCalculosJuros.ConverterJurosMensalParaDiario(Arredondar( ePercentualJuros.AsDouble * 0.01, 4 )) * 100
      ) - 1;
  end;

  try
    // Juros, Encargos e Percentual de Custo de Venda
    vIndice :=
      (1 + vPercJuros)
      /
      (1 - ((ePercEncargos.AsDouble + ePercCustoVenda.AsDouble) * 0.01));

    // Desconto
    vIndice := vIndice / (1 - ePercentualDescontoComercial.AsDouble * 0.01);
  except
    vIndice := 0;
  end;

  (* Parte do desconto comercial *)
  // Este calculo � muito simples, ele � simplismente o valor do produto ( 1 ) dividido pelo valor do desconto que ser� concedido na venda
  // Ex: O produto tem o valor de venda de 10 reais ( 1 ent�o � o valor cheio dele ), quero subir o pre�o de uma maneira que dando 10% de desconto
  // eu possa voltar aos 10, ent�o ficaria assim: 10 / ( 10 - ( 10 * 10%)) = 11,11
  // Tirando 10% de 11.11 chegamos novamente a 10 reais.
//  if (ePercentualDescontoComercial.AsCurr > 0) then
//    vIndiceDescontoComercial := (1 / (1 - ( ePercentualDescontoComercial.AsDouble * 0.01 ))) - 1;

  eIndiceAcrescimo.AsDouble := vIndice;
end;

procedure TFormCadastroCondicoesPagamento.CalcularPrazoMedio;
var
  i: Integer;
  vQtdeDias: Integer;
  vDias: TArray<Integer>;
begin
  vQtdeDias := 0;
  vDias := FrDiasPrazo.Valores;

  for i := Low(vDias) to High(vDias) do
    vQtdeDias := vQtdeDias + vDias[i];

  ePrazoMedio.AsInt := Round(vQtdeDias / _BibliotecaGenerica.zvl(Length(vDias), 1));
end;

procedure TFormCadastroCondicoesPagamento.cbQtdeMaximaParcelasChange(Sender: TObject);
begin
  inherited;
  CalcularIndiceCondicao;
end;

procedure TFormCadastroCondicoesPagamento.ckAcumulativaClick(Sender: TObject);
begin
  inherited;
  if ckAcumulativa.Checked then begin
    if Sessao.getParametros.TipoCobAcumulativoId = 0 then begin
      _Biblioteca.Exclamar('Para habilitar a op��o "10 - Acumulado" o tipo de cobran�a para acumulados deve ser informado nos par�metros!');
      ckAcumulativa.Checked := False;
      Exit;
    end;
  end;
end;

procedure TFormCadastroCondicoesPagamento.ePercEncargosChange(Sender: TObject);
begin
  inherited;
  CalcularIndiceCondicao;
end;

procedure TFormCadastroCondicoesPagamento.ePercentualDescontoComercialChange(Sender: TObject);
begin
  inherited;
  CalcularIndiceCondicao;
end;

procedure TFormCadastroCondicoesPagamento.ePercentualJurosChange(Sender: TObject);
begin
  inherited;
  CalcularIndiceCondicao;
end;

procedure TFormCadastroCondicoesPagamento.ePrazoMedioChange(Sender: TObject);
begin
  inherited;
  CalcularIndiceCondicao;
end;

procedure TFormCadastroCondicoesPagamento.ExcluirRegistro;
var
  retorno: RecRetornoBD;
begin
  Sessao.getConexaoBanco.IniciarTransacao;

  retorno := _TiposCobrancaCondicaoPagamento.ExcluirTiposCobrCondPagamento(Sessao.getConexaoBanco, eID.AsInt);

  if retorno.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    _Biblioteca.Exclamar(retorno.MensagemErro);
    Abort;
  end;

  retorno := _CondicoesPagamento.ExcluirCondicoesPagamento(Sessao.getConexaoBanco, eId.AsInt);

  if retorno.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    _Biblioteca.Exclamar(retorno.MensagemErro);
    Abort;
  end;

  Sessao.getConexaoBanco.FinalizarTransacao;

  inherited;
end;

procedure TFormCadastroCondicoesPagamento.ExibirLogs;
begin
  inherited;
  Logs.Iniciar('LOGS_CONDICOES_PAGAMENTO', ['CONDICAO_ID'], 'VW_TIPOS_ALTER_LOGS_COND_PAGTO', [eId.AsInt]);
end;

procedure TFormCadastroCondicoesPagamento.FormCreate(Sender: TObject);
begin
  inherited;
  FrDiasPrazo.OnAposAdicionar := FrDiasPrazoOnAposAdicionar;
  FrDiasPrazo.OnAposDeletar   := FrDiasPrazoOnAposDeletar;
end;

procedure TFormCadastroCondicoesPagamento.FrDiasPrazoOnAposAdicionar(Sender: TObject);
begin
  CalcularIndiceCondicao;
  CalcularPrazoMedio;
end;

procedure TFormCadastroCondicoesPagamento.FrDiasPrazoOnAposDeletar(Sender: TObject);
begin
  CalcularIndiceCondicao;
  CalcularPrazoMedio;
end;

procedure TFormCadastroCondicoesPagamento.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;

  Sessao.getConexaoBanco.IniciarTransacao;

  vRetBanco :=
    _CondicoesPagamento.AtualizarCondicoesPagamento(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eNome.Text,
      rgTipoCondicao.GetValor,
      eIndiceAcrescimo.AsDouble,
      eValorMinimoVenda.AsDouble,
      eValorMinimoParcela.AsDouble,
      ePercentualDescontoPermitidoVenda.AsDouble,
      _Biblioteca.ToChar(ckAtivo),
      ePrazoMedio.AsInt,
      ePercentualJuros.AsDouble,
      ePercEncargos.AsDouble,
      ePercentualDescontoComercial.AsDouble,
      _Biblioteca.ToChar(ckExigirNotaFiscal),
      _Biblioteca.ToChar(ckAplicarIndicePrecoPromocional),
      _Biblioteca.ToChar(ckPermitirUsoPontaEstoque),
      _Biblioteca.ToChar(ckPermitirUsoFechamentoAcum),
      cbTipoPreco.GetValor,
      _Biblioteca.ToChar(ckPermitirPrecoProcional),
      ckNaoPermitirConsFinal.CheckedStr,
      ckRestrita.CheckedStr,
      ckBloquearSempre.CheckedStr,
      ckRecebimentoNaEntrega.CheckedStr,
      ckAcumulativa.CheckedStr,
      ckImprimirConfissaoDivida.CheckedStr,
      FrDiasPrazo.getValores,
      ePercCustoVenda.AsDouble,
      True
    );

  if vRetBanco.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  vRetBanco := _TiposCobrancaCondicaoPagamento.AtualizarTiposCobrCondPagamento(Sessao.getConexaoBanco, vRetBanco.AsInt, FrTiposCobranca.GetArrayOfInteger);
  if vRetBanco.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  Sessao.getConexaoBanco.FinalizarTransacao;

  if eID.AsInt = 0 then
    _Biblioteca.Informar(coNovoRegistroSucessoCodigo + IntToStr(vRetBanco.AsInt))
  else
    _Biblioteca.Informar(coAlteracaoRegistroSucesso);
end;

procedure TFormCadastroCondicoesPagamento.Modo(pEditando: Boolean);
begin
  inherited;

  _Biblioteca.Habilitar([
    eNome,
    eIndiceAcrescimo,
    eValorMinimoVenda,
    eValorMinimoParcela,
    ckAtivo,
    rgTipoCondicao,
    ePercentualDescontoPermitidoVenda,
    ePercCustoVenda,
    FrTiposCobranca,
    ePrazoMedio,
    FrDiasPrazo,
    ePercEncargos,
    ePercentualJuros,
    ePercentualDescontoComercial,
    ckExigirNotaFiscal,
    ckAplicarIndicePrecoPromocional,
    ckPermitirUsoPontaEstoque,
    ckPermitirUsoFechamentoAcum,
    ckPermitirPrecoProcional,
    ckNaoPermitirConsFinal,
    cbTipoPreco,
    ckRestrita,
    ckBloquearSempre,
    ckRecebimentoNaEntrega,
    ckAcumulativa,
    ckImprimirConfissaoDivida],
    pEditando
  );

  if pEditando then begin
    ckAtivo.Checked := True;
    cbTipoPreco.ItemIndex := 0;
    eIndiceAcrescimo.AsCurr := 1;
    _Biblioteca.SomenteLeitura([eIndiceAcrescimo], True);
    _Biblioteca.SetarFoco(eNome);
  end;
end;

procedure TFormCadastroCondicoesPagamento.PesquisarRegistro;
var
  vCondicaoPagto: RecCondicoesPagamento;
begin
  vCondicaoPagto := RecCondicoesPagamento(PesquisaCondicoesPagamento.PesquisarCondicaoPagamento(False, False));
  if vCondicaoPagto = nil then
    Exit;

  inherited;
  PreencherRegistro(vCondicaoPagto, _TiposCobrancaCondicaoPagamento.BuscarTiposCobrCondPagamentos(Sessao.getConexaoBanco, 0, [vCondicaoPagto.condicao_id]));
end;

procedure TFormCadastroCondicoesPagamento.PreencherRegistro(pCondicaoPagamento: RecCondicoesPagamento; pTiposCobrancaCondicaoPagamento: TArray<Integer>);
var
  i: Integer;
  vDias: TArray<Integer>;
  vDiasCondicao: TArray<RecCondicoesPagamentoPrazos>;
begin
  eID.AsInt := pCondicaoPagamento.condicao_id;
  eNome.Text := pCondicaoPagamento.nome;
  rgTipoCondicao.SetIndicePorValor(pCondicaoPagamento.tipo);
  eIndiceAcrescimo.AsDouble  := pCondicaoPagamento.indice_acrescimo;
  eValorMinimoVenda.AsDouble := pCondicaoPagamento.valor_minimo_venda;
  eValorMinimoParcela.AsDouble := pCondicaoPagamento.ValorMinimoParcela;
  ePercentualDescontoPermitidoVenda.AsDouble := pCondicaoPagamento.PercentualDescontoPermVenda;
  ePercCustoVenda.AsDouble := pCondicaoPagamento.PercentualCustoVenda;

  if pTiposCobrancaCondicaoPagamento <> nil then
    FrTiposCobranca.InserirDadoPorChaveTodos(pTiposCobrancaCondicaoPagamento, False);

  ePrazoMedio.AsInt := pCondicaoPagamento.PrazoMedio;
  ePercEncargos.AsDouble := pCondicaoPagamento.PercentualEncagos;
  ePercentualJuros.AsDouble := pCondicaoPagamento.PercentualJuros;
  ePercentualDescontoComercial.AsDouble := pCondicaoPagamento.PercentualDescontoComercial;
  ckExigirNotaFiscal.Checked := pCondicaoPagamento.ExigirModeloNotaFiscal = 'S';
  ckAplicarIndicePrecoPromocional.Checked := pCondicaoPagamento.AplicarIndPrecoPromocional = 'S';
  ckPermitirUsoPontaEstoque.Checked := pCondicaoPagamento.PermitirUsoPontaEstoque = 'S';
  ckPermitirUsoFechamentoAcum.Checked := pCondicaoPagamento.PermitirUsoFechamentoAcumu = 'S';
  ckPermitirPrecoProcional.Checked := pCondicaoPagamento.PermitirPrecoPromocional = 'S';
  cbTipoPreco.SetIndicePorValor( pCondicaoPagamento.TipoPreco );
  ckNaoPermitirConsFinal.CheckedStr := pCondicaoPagamento.NaoPermitirConsFinal;
  ckRestrita.CheckedStr := pCondicaoPagamento.Restrita;
  ckBloquearSempre.CheckedStr := pCondicaoPagamento.BloquearVendaSempre;
  ckRecebimentoNaEntrega.CheckedStr := pCondicaoPagamento.RecebimentoNaEntrega;
  ckAcumulativa.CheckedStr := pCondicaoPagamento.Acumulativo;
  ckImprimirConfissaoDivida.CheckedStr := pCondicaoPagamento.ImprimirConfissaoDivida;

  ckAtivo.Checked := (pCondicaoPagamento.ativo = 'S');

  vDias := nil;
  vDiasCondicao := _CondicoesPagamentoPrazos.BuscarCondicoesPagamentoPrazos(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  for i := Low(vDiasCondicao) to High(vDiasCondicao) do
    _Biblioteca.AddNoVetorSemRepetir(vDias, vDiasCondicao[i].Dias);

  FrDiasPrazo.Valores := vDias;

  pCondicaoPagamento.Free;
end;

procedure TFormCadastroCondicoesPagamento.VerificarRegistro(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  if eNome.Text = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar o nome da condi��o de pagamento!');
    _Biblioteca.SetarFoco(eNome);
    Abort;
  end;

  if rgTipoCondicao.ItemIndex = -1 then begin
    _Biblioteca.Exclamar('� necess�rio informar o tipo de condi��o de pagamento!');
    _Biblioteca.SetarFoco(rgTipoCondicao);
    Abort;
  end;

  if eIndiceAcrescimo.AsCurr = 0 then begin
    _Biblioteca.Exclamar('Indice de acr�scimo inv�lido!');
    _Biblioteca.SetarFoco(eIndiceAcrescimo);
    Abort;
  end;

  if FrTiposCobranca.EstaVazio then begin
    _Biblioteca.Exclamar('Pelo menos 1 tipo de cobran�a deve estar ligada a esta condi��o de pagamento!');
    _Biblioteca.SetarFoco(FrTiposCobranca);
    Abort;
  end;

  if (rgTipoCondicao.GetValor = 'A') and (not FrDiasPrazo.EstaVazio) then begin
    _Biblioteca.Exclamar('A condi��o de pagamento quando � "a vista" n�o pode ter os dias de prazo informados!');
    _Biblioteca.SetarFoco(FrDiasPrazo);
    Abort;
  end;

  for i := 0 to FrTiposCobranca.getQuantidadeLinhas -1 do begin
    if FrTiposCobranca.GetTipoCobranca(i).forma_pagamento = 'ACU' then begin
      if not ckAcumulativa.Checked then begin
        _Biblioteca.Exclamar(
          'O tipo de cobran�a ' + FrTiposCobranca.GetTipoCobranca(i).Nome + ' est� configurado com a forma de pagamento ' +
          '"Acumulado" por�m a condi��o de pagamento n�o est� com o par�metro "10 - Acumulado" habilitado, por favor verifique!'
        );

        _Biblioteca.SetarFoco(ckAcumulativa);
        Abort;
      end;
    end;
  end;

  if ckAcumulativa.Checked then begin
    if rgTipoCondicao.GetValor = 'A' then begin
      _Biblioteca.Exclamar('Quando marcada a op��o "10 - Acumulado" o tipo de condi��o de pagamento s� poder� ser "A prazo"!');
      _Biblioteca.SetarFoco(rgTipoCondicao);
      Abort;
    end;

    if ckRecebimentoNaEntrega.Checked then begin
      _Biblioteca.Exclamar('Quando marcado a op��o "10 - Acumulado" a op��o "9 - Recebimento na entrega" n�o pode estar marcada!');
      _Biblioteca.SetarFoco(ckRecebimentoNaEntrega);
      Abort;
    end;

    if not ckNaoPermitirConsFinal.Checked then begin
      _Biblioteca.Exclamar('Quando marcado a op��o "10 - Acumulado" a op��o "6 - N�o permitir consumidor final" deve estar marcada!');
      _Biblioteca.SetarFoco(ckNaoPermitirConsFinal);
      Abort;
    end;

    if FrTiposCobranca.getQuantidadeLinhas > 1 then begin
      _Biblioteca.Exclamar('Quando marcado a op��o "10 - Acumulado", a condi��o de pagamento s� poder� ter 1 tipo cobran�a!');
      _Biblioteca.SetarFoco(FrTiposCobranca);
      Abort;
    end;

    if FrTiposCobranca.GetTipoCobranca.forma_pagamento <> 'ACU' then begin
      _Biblioteca.Exclamar('Quando marcado a op��o "10 - Acumulado", o tipo de cobran�a s� poder� ser com a forma de pagamento "Acumulado"!');
      _Biblioteca.SetarFoco(FrTiposCobranca);
      Abort;
    end;
  end;
end;

end.
