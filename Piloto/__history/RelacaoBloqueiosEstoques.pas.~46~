unit RelacaoBloqueiosEstoques;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _Biblioteca, _Sessao,
  Vcl.ComCtrls, Vcl.StdCtrls, CheckBoxLuka, Vcl.Buttons, Vcl.ExtCtrls, _BloqueiosEstoques,
  FrameMarcas, FrameEmpresas, FrameProdutos, _FrameHerancaPrincipal, _BloqueiosEstoquesItens,
  _FrameHenrancaPesquisas, Frame.DepartamentosSecoesLinhas, FrameLocais, BaixarCancelarItensBloqueioEstoque,
  FrameDataInicialFinal, FrameFuncionarios, Vcl.Grids, GridLuka, StaticTextLuka,
  Vcl.Menus;

type
  TFormRelacaoBloqueiosEstoques = class(TFormHerancaRelatoriosPageControl)
    FrProdutos: TFrProdutos;
    FrEmpresa: TFrEmpresas;
    FrDataCadastroProduto: TFrDataInicialFinal;
    FrLocaisBloqueio: TFrLocais;
    FrUsuariosCadastro: TFrFuncionarios;
    st1: TStaticTextLuka;
    sgBloqueios: TGridLuka;
    splSeparador: TSplitter;
    st2: TStaticTextLuka;
    sgItens: TGridLuka;
    pmOpcoes: TPopupMenu;
    miBaixarItensBloqueioSelecionado: TSpeedButton;
    miCancelarItensBloqueioFocado: TSpeedButton;
    StaticTextLuka1: TStaticTextLuka;
    StaticTextLuka2: TStaticTextLuka;
    FrDataIbxbloqueio: TFrDataInicialFinal;
    FrUsuariodaBaixa: TFrFuncionarios;
    procedure sgBloqueiosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgBloqueiosClick(Sender: TObject);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure miBaixarItensBloqueioSelecionadoClick(Sender: TObject);
    procedure sgBloqueiosGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    FItens: TArray<RecBloqueiosEstoquesItens>;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

const
  coBloqueioEstoqueId = 0;
  coEmpresa           = 1;
  coStatus            = 2;
  coCustoTotal        = 3;
  coDataHoraCadastro  = 4;
  coUsuarioCadastro   = 5;
  coDataHoraBaixa     = 6;
  coUsuarioBaixa      = 7;
  coObservacoes       = 8;
  coTipoLinha         = 9;

  (* Grid de produtos *)
  ciProdutoId         = 0;
  ciNome              = 1;
  ciMarca             = 2;
  ciCustoProduto      = 3;
  ciQuantidade        = 4;
  ciCustoProdutoTotal = 5;
  ciBaixados          = 6;
  ciCancelados        = 7;
  ciSaldo             = 8;
  ciUnidade           = 9;
  ciLote              = 10;
  ciLocal             = 11;

  coLinhaDetalhe   = 'D';
  coLinhaTotCusto  = 'TC';


procedure TFormRelacaoBloqueiosEstoques.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;

  vBloqueiosIds: TArray<Integer>;
  vBloqueios: TArray<RecBloqueiosEstoques>;
  vTotalCusto: Double;
begin
  inherited;
  vSql := '';
  vBloqueiosIds := nil;

  FrEmpresa.getSqlFiltros('BLO.EMPRESA_ID', vSql, True);
  FrProdutos.getSqlFiltros('BTI.PRODUTO_ID', vSql, True);
  FrLocaisBloqueio.getSqlFiltros('BTI.LOCAL_ID', vSql, True);
  FrUsuariosCadastro.getSqlFiltros('BLO.USUARIO_CADASTRO_ID', vSql, True);
  FrDataCadastroProduto.getSqlFiltros('BLO.DATA_HORA_CADASTRO', vSql, True);
  FrDataIbxbloqueio.getSqlFiltros('BLO.DATA_HORA_BAIXA', vSql, True);
  FrUsuariodaBaixa.getSqlFiltros('BLO.USUARIO_BAIXA_ID', vSql, True);
  vSql := vSql + ' order by BLO.BLOQUEIO_ID asc ';

  vBloqueios := _BloqueiosEstoques.BuscarBloqueiosEstoquesComando(Sessao.getConexaoBanco, vSql);
  if vBloqueios = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;  
  end;

  vTotalCusto := 0.0;
  for i := Low(vBloqueios) to High(vBloqueios) do begin
    sgBloqueios.Cells[coBloqueioEstoqueId, i + 1] := NFormat(vBloqueios[i].BloqueioId);
    sgBloqueios.Cells[coEmpresa, i + 1]           := getInformacao(vBloqueios[i].EmpresaId, vBloqueios[i].NomeEmpresa);
    sgBloqueios.Cells[coStatus, i + 1]            := vBloqueios[i].StatusAnalitico;
    sgBloqueios.Cells[coCustoTotal, i + 1]        := NFormatEstoque(vBloqueios[i].CustoTotal);
    sgBloqueios.Cells[coDataHoraCadastro, i + 1]  := DHFormat(vBloqueios[i].DataHoraCadastro);
    sgBloqueios.Cells[coUsuarioCadastro, i + 1]   := getInformacao(vBloqueios[i].UsuarioCadastroId, vBloqueios[i].NomeUsuarioCadastro);
    sgBloqueios.Cells[coDataHoraBaixa, i + 1]     := DHFormatN(vBloqueios[i].DataHoraBaixa);
    sgBloqueios.Cells[coUsuarioBaixa, i + 1]      := getInformacao(vBloqueios[i].UsuarioBaixaId, vBloqueios[i].NomeUsuarioBaixa);
    sgBloqueios.Cells[coObservacoes, i + 1]       := vBloqueios[i].Observacoes;
    sgBloqueios.Cells[coTipoLinha, i + 1]         := coLinhaDetalhe;

    _Biblioteca.AddNoVetorSemRepetir(vBloqueiosIds, vBloqueios[i].BloqueioId);
    vTotalCusto := vTotalCusto + vBloqueios[i].CustoTotal;
  end;

  sgBloqueios.Cells[coStatus, i + 1]       := 'TOTAL: ';
  sgBloqueios.Cells[coCustoTotal, i + 1]   := NFormatEstoque(vTotalCusto);
  sgBloqueios.Cells[coTipoLinha, i + 1]    := coLinhaTotCusto;

  sgBloqueios.SetLinhasGridPorTamanhoVetor( Length(vBloqueios) + 1 );

  FItens := _BloqueiosEstoquesItens.BuscarBloqueiosEstoquesItensComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('BEI.BLOQUEIO_ID', vBloqueiosIds));

  sgBloqueiosClick(Sender);
  SetarFoco(sgItens);
end;

procedure TFormRelacaoBloqueiosEstoques.miBaixarItensBloqueioSelecionadoClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar;
begin
  inherited;
  vRetTela :=
    BaixarCancelarItensBloqueioEstoque.BaixarCancelar(
      SFormatInt(sgBloqueios.Cells[coBloqueioEstoqueId, sgBloqueios.Row]),
      Sender = miBaixarItensBloqueioSelecionado
    );
  if vRetTela.BuscaCancelada then
    Exit;

  _Biblioteca.RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoBloqueiosEstoques.sgBloqueiosClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vBloqueioId: Integer;
begin
  inherited;

  sgItens.ClearGrid;

  vLinha := 0;
  vBloqueioId := SFormatInt(sgBloqueios.Cells[coBloqueioEstoqueId, sgBloqueios.Row]);
  for i := Low(FItens) to High(FItens) do begin
    if vBloqueioId <> FItens[i].BloqueioId then
      Continue;

    Inc(vLinha);
    
    sgItens.Cells[ciProdutoId, vLinha]         := NFormat(FItens[i].ProdutoId);
    sgItens.Cells[ciNome, vLinha]              := FItens[i].NomeProduto;
    sgItens.Cells[ciMarca, vLinha]             := FItens[i].NomeMarca;
    sgItens.Cells[ciCustoProduto, vLinha]      := NFormatNEstoque(FItens[i].CustoProduto);
    sgItens.Cells[ciQuantidade, vLinha]        := NFormatNEstoque(FItens[i].Quantidade);
    sgItens.Cells[ciCustoProdutoTotal, vLinha] := NFormatNEstoque(FItens[i].CustoProdutoTotal);
    sgItens.Cells[ciBaixados, vLinha]          := NFormatNEstoque(FItens[i].Baixados);
    sgItens.Cells[ciCancelados, vLinha]        := NFormatNEstoque(FItens[i].Cancelados);
    sgItens.Cells[ciSaldo, vLinha]             := NFormatNEstoque(FItens[i].Saldo);
    sgItens.Cells[ciUnidade, vLinha]           := FItens[i].UnidadeVenda;
    sgItens.Cells[ciLote, vLinha]              := FItens[i].Lote;
    sgItens.Cells[ciLocal, vLinha]             := getInformacao(FItens[i].LocalId, FItens[i].NomeLocal);
  end;
  sgItens.SetLinhasGridPorTamanhoVetor( vLinha );
end;

procedure TFormRelacaoBloqueiosEstoques.sgBloqueiosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coBloqueioEstoqueId, coCustoTotal] then
    vAlinhamento := taRightJustify
  else if ACol = coStatus then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgBloqueios.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoBloqueiosEstoques.sgBloqueiosGetCellColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coPercLucroBruto then
    AFont.Color := _Sessao.Sessao.getCorLucro( SFormatDouble( sgOrcamentos.Cells[coPercLucroBruto, ARow] ) )
  else if ACol = coPercLucroLiquido then
    AFont.Color := _Sessao.Sessao.getCorLucro( SFormatDouble( sgOrcamentos.Cells[coPercLucroLiquido, ARow] ) );

  if sgOrcamentos.Cells[coTipoLinha, ARow] = coLinhaTotVenda then begin
    AFont.Color := clWhite;
    AFont.Style := [fsBold];
    ABrush.Color := $000096DB;
  end
  else if sgOrcamentos.Cells[coTipoLinha, ARow] = coLinhaCabAgrup then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end
  else if sgOrcamentos.Cells[coTipoLinha, ARow] = coTotalAgrupador then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao3;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao3;
  end
end;

procedure TFormRelacaoBloqueiosEstoques.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[ciProdutoId, ciCustoProduto, ciQuantidade, ciCustoProdutoTotal, ciBaixados, ciCancelados, ciSaldo] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);  
end;

procedure TFormRelacaoBloqueiosEstoques.VerificarRegistro(Sender: TObject);
begin
  inherited;
  sgBloqueios.ClearGrid;
  sgItens.ClearGrid;
end;

end.
