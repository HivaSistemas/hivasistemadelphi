unit Impressao.OrcamentoGrafico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatorioSimples, QRCtrls, _Sessao, _Biblioteca,
  QuickRpt, Vcl.ExtCtrls, _Orcamentos, _OrcamentosItens, _RecordsOrcamentosVendas,
  QRExport, QRPDFFilt, QRWebFilt;

type
  TFormImpressaoOrcamentoGrafico = class(TFormHerancaRelatorioSimples)
    qr2: TQRLabel;
    qrOrcamentoId: TQRLabel;
    qr8: TQRLabel;
    qrCliente: TQRLabel;
    qr12: TQRLabel;
    qrEnderecoCliente: TQRLabel;
    qr15: TQRLabel;
    qrBairroCliente: TQRLabel;
    qrCidadeCliente: TQRLabel;
    qr18: TQRLabel;
    qr19: TQRLabel;
    qrEstadoCliente: TQRLabel;
    qrCepCliente: TQRLabel;
    qr21: TQRLabel;
    qr23: TQRLabel;
    qr24: TQRLabel;
    qr25: TQRLabel;
    qr26: TQRLabel;
    qr27: TQRLabel;
    qr6: TQRLabel;
    qr28: TQRLabel;
    qrProdutoId: TQRLabel;
    qrNomeProduto: TQRLabel;
    qrMarca: TQRLabel;
    qrValorUnitario: TQRLabel;
    qrQuantidade: TQRLabel;
    qrUnidade: TQRLabel;
    qrValorTotal: TQRLabel;
    qr4: TQRLabel;
    qrTelefoneCliente: TQRLabel;
    qr14: TQRLabel;
    qrCelularCliente: TQRLabel;
    qr9: TQRLabel;
    qrVendedor: TQRLabel;
    qr17: TQRLabel;
    qrCondicaoPagamento: TQRLabel;
    qrMensagensFinais: TQRMemo;
    qr20: TQRLabel;
    qr22: TQRLabel;
    qr29: TQRLabel;
    qrTotalSerPago: TQRLabel;
    qrValorDesconto: TQRLabel;
    qrValorOutrasDespesas: TQRLabel;
    qrl1: TQRLabel;
    qrlGrValorFrete: TQRLabel;
    qrTotalProdutos: TQRLabel;
    qr16: TQRLabel;
    QRLabel4: TQRLabel;
    qrOrcamentoIdTM: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel8: TQRLabel;
    qrClienteTM: TQRLabel;
    QRLabel12: TQRLabel;
    qrCelularClienteTM: TQRLabel;
    QRLabel5: TQRLabel;
    qrEnderecoClienteTM: TQRLabel;
    QRLabel9: TQRLabel;
    qrCidadeClienteTM: TQRLabel;
    QRLabel13: TQRLabel;
    qrEstadoClienteTM: TQRLabel;
    QRLabel7: TQRLabel;
    qrBairroClienteTM: TQRLabel;
    QRLabel11: TQRLabel;
    qrCepClienteTM: TQRLabel;
    QRBand1: TQRBand;
    qrProdutoIdTM: TQRLabel;
    qrVendedorTM: TQRLabel;
    QRShape4: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    qrNomeProdutoTM: TQRLabel;
    qrValorUnitarioTM: TQRLabel;
    qrQuantidadeTM: TQRLabel;
    qrUnidadeTM: TQRLabel;
    qrValorTotalTM: TQRLabel;
    procedure qrBandaDetalhesBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qrRelatorioBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure qrRelatorioNeedData(Sender: TObject; var MoreData: Boolean);

  private
    FPosicImp: Integer;
    FProdutos: TArray<RecOrcamentosItensImpressao>;
  end;

procedure Imprimir(
  const pOrcamentoId: Integer;
  pGerarPDF: Boolean = False
);

implementation

{$R *.dfm}

procedure Imprimir(
  const pOrcamentoId: Integer;
  pGerarPDF: Boolean = False
);
var
  vForm: TFormImpressaoOrcamentoGrafico;
  vOrcamento: RecOrcamentosImpressao;
  vItens: TArray<RecOrcamentosItensImpressao>;
  vImpressora: RecImpressora;
begin
  if pOrcamentoId = 0 then
    Exit;

  vOrcamento := _Orcamentos.BuscarInformacoesImpressao(Sessao.getConexaoBanco, pOrcamentoId);
  if vOrcamento.OrcamentoId = 0 then begin
    NenhumRegistro;
    Exit;
  end;

  vItens := _OrcamentosItens.BuscarInformacoesImpressao(Sessao.getConexaoBanco, pOrcamentoId);
  if vItens = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vImpressora := Sessao.getImpressora( toOrcamento );
  if not vImpressora.validaImpresssora then
    Exit;

  vForm := TFormImpressaoOrcamentoGrafico.Create(Application, vImpressora);


  vForm.FProdutos := vItens;
  vForm.PreencherCabecalho( vOrcamento.EmpresaId, True );

  if Em(vOrcamento.Status, ['VR', 'VE', 'RE']) then
    vForm.qr13.Caption := 'Pedido'
  else
    vForm.qr13.Caption := 'Or�amento';

  vForm.qrOrcamentoId.Caption         := NFormat(pOrcamentoId);
  vForm.qrOrcamentoIdTM.Caption       := NFormat(pOrcamentoId);
  vForm.qrVendedor.Caption            := NFormat(vOrcamento.VendedorId) + ' - ' + vOrcamento.NomeVendedor;
  vForm.qrVendedorTM.Caption          := NFormat(vOrcamento.VendedorId) + ' - ' + vOrcamento.NomeVendedor;
  vForm.qrCondicaoPagamento.Caption   := vOrcamento.NomeCondicaoPagamento;
  vForm.qrCliente.Caption             := NFormat(vOrcamento.ClienteId) + ' - ' + vOrcamento.NomeCliente;
  vForm.qrClienteTM.Caption           := NFormat(vOrcamento.ClienteId) + ' - ' + vOrcamento.NomeCliente;
  vForm.qrEnderecoCliente.Caption     := vOrcamento.Logradouro + ' ' + vOrcamento.Complemento + ' ' + vOrcamento.Numero;
  vForm.qrEnderecoClienteTM.Caption   := vOrcamento.Logradouro + ' ' + vOrcamento.Complemento + ' ' + vOrcamento.Numero;
  vForm.qrBairroCliente.Caption       := vOrcamento.NomeBairro;
  vForm.qrBairroClienteTM.Caption     := vOrcamento.NomeBairro;
  vForm.qrCidadeCliente.Caption       := vOrcamento.NomeCidade;
  vForm.qrCidadeClienteTM.Caption     := vOrcamento.NomeCidade;
  vForm.qrEstadoCliente.Caption       := vOrcamento.NomeEstado;
  vForm.qrEstadoClienteTM.Caption     := vOrcamento.NomeEstado;
  vForm.qrCepCliente.Caption          := vOrcamento.Cep;
  vForm.qrCepClienteTM.Caption        := vOrcamento.Cep;
  vForm.qrTelefoneCliente.Caption     := vOrcamento.TelefonePrincipal;
  vForm.qrCelularCliente.Caption      := vOrcamento.TelefoneCelular;
  vForm.qrCelularClienteTM.Caption    := vOrcamento.TelefoneCelular;

  vForm.qrTotalProdutos.Caption       := NFormat(vOrcamento.ValorTotalProdutos);
  vForm.qrValorOutrasDespesas.Caption := NFormat(vOrcamento.ValorOutrasDespesas);
  vForm.qrlGrValorFrete.Caption       := NFormat(vOrcamento.ValorFrete + vOrcamento.ValorFreteItens);
  vForm.qrValorDesconto.Caption       := NFormat(vOrcamento.ValorDesconto);
  vForm.qrTotalSerPago.Caption        := NFormat(vOrcamento.ValorTotal);

  if not Em(vOrcamento.Status, ['VR', 'VE', 'RE']) then begin
    vForm.qrMensagensFinais.Lines.Clear;
    vForm.qrMensagensFinais.Lines.Add('Or�amento v�lido pelo prazo de ' + NFormat(Sessao.getParametrosEmpresa.QtdeDiasValidadeOrcamento) + ' dia(s).' );
    vForm.qrMensagensFinais.Lines.Add(vOrcamento.ObservacoesCaixa);
  end;

  vForm.Imprimir(1, pGerarPDF);

  vForm.Free;
end;

procedure TFormImpressaoOrcamentoGrafico.qrBandaDetalhesBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrProdutoId.Caption     := NFormat(FProdutos[FPosicImp].ProdutoId);
  qrProdutoIdTM.Caption   := NFormat(FProdutos[FPosicImp].ProdutoId);
  qrNomeProduto.Caption   := FProdutos[FPosicImp].NomeProduto;
  qrNomeProdutoTM.Caption   := FProdutos[FPosicImp].NomeProduto;
  qrMarca.Caption         := FProdutos[FPosicImp].NomeMarca;
  qrValorUnitario.Caption := NFormat(FProdutos[FPosicImp].PrecoUnitario);
  qrValorUnitarioTM.Caption := NFormat(FProdutos[FPosicImp].PrecoUnitario);
  qrQuantidade.Caption    := NFormatEstoque(FProdutos[FPosicImp].Quantidade);
  qrQuantidadeTM.Caption    := NFormatEstoque(FProdutos[FPosicImp].Quantidade);
  qrUnidade.Caption       := FProdutos[FPosicImp].UnidadeVenda;
  qrUnidadeTM.Caption       := FProdutos[FPosicImp].UnidadeVenda;
  qrValorTotal.Caption    := NFormat(FProdutos[FPosicImp].ValorTotal);
  qrValorTotalTM.Caption    := NFormat(FProdutos[FPosicImp].ValorTotal);
end;


procedure TFormImpressaoOrcamentoGrafico.qrRelatorioBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicImp := -1;
end;

procedure TFormImpressaoOrcamentoGrafico.qrRelatorioNeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicImp);
  MoreData := (Length(FProdutos) > FPosicImp);
end;

end.
