unit Impressao.CartazPromocao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatorioSimples, QRExport,
  QRPDFFilt, QRWebFilt, QRCtrls, QuickRpt, Vcl.ExtCtrls, _Sessao;

type
  TFormImpressaoCartazPromocao = class(TFormHerancaRelatorioSimples)
    qrPromocao: TQuickRep;
    QRBand2: TQRBand;
    QRLabel12: TQRLabel;
    QRBand3: TQRBand;
    qrProduto: TQRLabel;
    qrPreco: TQRLabel;
    qrPrecoPromocao: TQRLabel;
    qrUnidade: TQRLabel;
    qrMarca: TQRLabel;
    qrCodigoOriginal: TQRLabel;
    qrCaracteristica: TQRLabel;
    qrValidadePromocao: TQRLabel;
  private
    { Private declarations }
  end;

procedure Imprimir(
  pProdutoId: string;
  pNomeProduto: string;
  pPreco: string;
  pPromocao: string;
  pUnidade: string;
  pMarca: string;
  pCodigoOriginal: string;
  pCaracteristica: string;
  pValidadePromocao: string
);

var
  FormImpressaoCartazPromocao: TFormImpressaoCartazPromocao;

implementation

{$R *.dfm}

procedure Imprimir(
  pProdutoId: string;
  pNomeProduto: string;
  pPreco: string;
  pPromocao: string;
  pUnidade: string;
  pMarca: string;
  pCodigoOriginal: string;
  pCaracteristica: string;
  pValidadePromocao: string
);
var
  vForm: TFormImpressaoCartazPromocao;
  vImpressora: RecImpressora;
begin

  vImpressora := Sessao.getImpressora( toCartazPromocional );
  if not vImpressora.validaImpresssora then
    Exit;

  vForm := TFormImpressaoCartazPromocao.Create(Application, vImpressora);

  vForm.qrProduto.Caption := pProdutoId + ' - ' + pNomeProduto;
  vForm.qrPreco.Caption := 'de ' + pPreco;
  vForm.qrPrecoPromocao.Caption := 'por ' + pPromocao;
  vForm.qrMarca.Caption := 'MARCA: ' + pMarca;
  vForm.qrCodigoOriginal.Caption := 'COD. DA FABRICA: ' + pCodigoOriginal;
  vForm.qrUnidade.Caption := pUnidade;
  vForm.qrCaracteristica.Caption := 'CARACTER�STICA: ' + pCaracteristica;
  vForm.qrValidadePromocao.Caption := 'PROMO��O V�LIDA AT� ' + pValidadePromocao;

  if vImpressora.AbrirPreview then
    vForm.qrPromocao.PreviewModal
  else
    vForm.qrPromocao.Print;

  vForm.Free;
end;

end.
