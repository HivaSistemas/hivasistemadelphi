inherited FormCadastroBairros: TFormCadastroBairros
  Caption = 'Cadastro de bairros'
  ClientHeight = 232
  ClientWidth = 521
  ExplicitWidth = 527
  ExplicitHeight = 261
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 126
    Top = 45
    Width = 32
    Height = 14
    Caption = 'Nome'
  end
  inherited pnOpcoes: TPanel
    Height = 232
    ExplicitHeight = 232
  end
  inherited eID: TEditLuka
    TabOrder = 2
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 468
    TabOrder = 1
    Visible = False
    ExplicitLeft = 468
  end
  object eNome: TEditLuka
    Left = 126
    Top = 59
    Width = 388
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 3
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  inline FrCidade: TFrCidades
    Left = 126
    Top = 89
    Width = 388
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 4
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 89
    ExplicitWidth = 388
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 363
      Height = 23
      TabOrder = 1
      ExplicitWidth = 363
      ExplicitHeight = 23
    end
    inherited CkAspas: TCheckBox
      TabOrder = 3
    end
    inherited CkFiltroDuplo: TCheckBox
      TabOrder = 5
    end
    inherited CkPesquisaNumerica: TCheckBox
      TabOrder = 7
    end
    inherited PnTitulos: TPanel
      Width = 388
      TabOrder = 0
      ExplicitWidth = 388
      inherited lbNomePesquisa: TLabel
        Caption = 'Cidade'
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 318
        ExplicitLeft = 318
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 363
      Height = 24
      TabOrder = 2
      ExplicitLeft = 363
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
      TabOrder = 6
    end
  end
  inline FrRota: TFrRotas
    Left = 126
    Top = 137
    Width = 386
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 5
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 137
    ExplicitWidth = 386
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 361
      Height = 24
      TabOrder = 1
      ExplicitWidth = 186
      ExplicitHeight = 24
    end
    inherited CkAspas: TCheckBox
      TabOrder = 3
    end
    inherited CkFiltroDuplo: TCheckBox
      TabOrder = 5
    end
    inherited CkPesquisaNumerica: TCheckBox
      TabOrder = 7
    end
    inherited PnTitulos: TPanel
      Width = 386
      TabOrder = 0
      ExplicitWidth = 211
      inherited lbNomePesquisa: TLabel
        Width = 25
        Caption = 'Rota'
        ExplicitWidth = 25
        ExplicitHeight = 14
      end
      inherited CkChaveUnica: TCheckBox
        TabOrder = 1
      end
      inherited pnSuprimir: TPanel
        Left = 316
        TabOrder = 0
        ExplicitLeft = 141
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 361
      Height = 25
      TabOrder = 2
      ExplicitLeft = 186
      ExplicitHeight = 25
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
      TabOrder = 6
    end
  end
end
