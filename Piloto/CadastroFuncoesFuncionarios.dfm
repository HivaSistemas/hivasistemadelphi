inherited FormCadastroCargosFuncionarios: TFormCadastroCargosFuncionarios
  Caption = 'Cargos de usu'#225'rios'
  ClientHeight = 204
  ClientWidth = 451
  ExplicitWidth = 457
  ExplicitHeight = 233
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 126
    Top = 99
    Width = 32
    Height = 14
    Caption = 'Nome'
  end
  inherited Label1: TLabel
    Top = 44
    ExplicitTop = 44
  end
  inherited pnOpcoes: TPanel
    Height = 204
    ExplicitLeft = -1
    ExplicitHeight = 204
  end
  inherited eID: TEditLuka
    Top = 58
    TabOrder = 3
    ExplicitTop = 58
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 126
    Top = 11
    ExplicitLeft = 126
    ExplicitTop = 11
  end
  object eNome: TEditLuka
    Left = 126
    Top = 114
    Width = 317
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 30
    TabOrder = 1
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
