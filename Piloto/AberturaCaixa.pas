unit AberturaCaixa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastro, Vcl.StdCtrls, _Biblioteca, _TurnosCaixas,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _FrameHerancaPrincipal, _RecordsEspeciais, _RecordsCaixa,
  _FrameHenrancaPesquisas, FrameFuncionarios, _Sessao, Vcl.Mask, EditLukaData,
  FrameContas;

type
  TFormAberturaCaixa = class(TFormHerancaCadastro)
    FrFuncionario: TFrFuncionarios;
    eValorDinheiroInicial: TEditLuka;
    lb11: TLabel;
    FrContaOrigem: TFrContas;
    procedure FormCreate(Sender: TObject);
  private
    procedure FrFuncionarioOnAposPesquisar(Sender: TObject);
  protected
    procedure GravarRegistro(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormAberturaCaixa }

procedure TFormAberturaCaixa.FormCreate(Sender: TObject);
begin
  inherited;
  FrFuncionario.OnAposPesquisar := FrFuncionarioOnAposPesquisar;
  FrFuncionario.Modo(True);
end;

procedure TFormAberturaCaixa.FrFuncionarioOnAposPesquisar(Sender: TObject);
var
  vTurno: TArray<RecTurnosCaixas>;
begin
  vTurno := _TurnosCaixas.BuscarTurnosCaixas(Sessao.getConexaoBanco, 1, [FrFuncionario.GetFuncionario.funcionario_id]);
  if vTurno <> nil then begin
    _Biblioteca.Exclamar('O funcion�rio selecionado j� possui turno em aberto, n�o ser� poss�vel abrir outro turno!');
    FrFuncionario.Clear;
    Exit;
  end;

  Modo(True);
  FrFuncionario.Modo(False, False);
end;

procedure TFormAberturaCaixa.GravarRegistro(Sender: TObject);
var
  vTurnoId: Integer;
  vContaOrigemId: string;
  vRetorno: RecRetornoBD;
begin
  inherited;

  vContaOrigemId := '';
  if eValorDinheiroInicial.AsCurr > 0 then
    vContaOrigemId := FrContaOrigem.GetConta.Conta;

  vRetorno :=
    _TurnosCaixas.AtualizarTurnosCaixas(
      Sessao.getConexaoBanco,
      0,
      Sessao.getEmpresaLogada.EmpresaId,
      FrFuncionario.GetFuncionario().funcionario_id,
      eValorDinheiroInicial.AsCurr,
      vContaOrigemId,
      0
    );

  if vRetorno.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  vTurnoId := vRetorno.AsInt;

  if Sessao.getUsuarioLogado.funcionario_id = FrFuncionario.GetFuncionario.funcionario_id then
    Sessao.AtualizarTurnoCaixaAberto;

  if vTurnoId > 0 then
    _Biblioteca.Informar(coNovoRegistroSucessoCodigo + IntToStr(vTurnoId));
end;

procedure TFormAberturaCaixa.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([FrContaOrigem, eValorDinheiroInicial], pEditando);

  if pEditando then
    SetarFoco(FrContaOrigem)
  else begin
    FrFuncionario.Modo(True);
    SetarFoco(FrFuncionario);
  end;
end;

procedure TFormAberturaCaixa.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrFuncionario.EstaVazio then begin
    _Biblioteca.Exclamar('N�o foi informado o funcion�rio do caixa a ser aberto, verifque!');
    FrFuncionario.SetFocus;
    Abort;
  end;

  if eValorDinheiroInicial.AsCurr > 0 then begin
    if FrContaOrigem.EstaVazio then begin
      _Biblioteca.Exclamar('N�o foi informado a origem do dinheiro para abertura do caixa, verifique!');
      FrContaOrigem.SetFocus;
      Abort;
    end;

    if
      (FrContaOrigem.GetConta().AceitaSaldoNegativo = 'N')
      and
      (FrContaOrigem.GetConta().SaldoAtual - eValorDinheiroInicial.AsCurr < 0)
    then begin
      _Biblioteca.Exclamar('Saldo atual conta de origem insuficiente para abertura do caixa, verique!');
      FrContaOrigem.SetFocus;
      Abort;
    end;
  end
  else begin
    if not FrContaOrigem.EstaVazio then begin
      _Biblioteca.Exclamar('N�o pode ser informado a origem do dinheiro quando o valor inicial est� zerado, verique!');
      FrContaOrigem.SetFocus;
      Abort;
    end;
  end;
end;

end.
