inherited FormCadastroContas: TFormCadastroContas
  Margins.Left = 1
  Margins.Top = 1
  Margins.Right = 1
  Margins.Bottom = 1
  Caption = 'Cadastro de contas'
  ClientHeight = 327
  ClientWidth = 681
  ExplicitWidth = 687
  ExplicitHeight = 356
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 197
    Top = 5
    Width = 32
    Height = 14
    Caption = 'Nome'
  end
  object lb2: TLabel [1]
    Left = 158
    Top = 45
    Width = 43
    Height = 14
    Caption = 'Ag'#234'ncia'
  end
  object lb3: TLabel [2]
    Left = 220
    Top = 45
    Width = 32
    Height = 14
    Caption = 'D'#237'gito'
  end
  object lb6: TLabel [3]
    Left = 418
    Top = 92
    Width = 63
    Height = 14
    Caption = 'Saldo atual'
  end
  object lb7: TLabel [4]
    Left = 126
    Top = 194
    Width = 123
    Height = 14
    Caption = 'Nr. ult. cheque emitido'
  end
  object lb5: TLabel [5]
    Left = 285
    Top = 45
    Width = 33
    Height = 14
    Caption = 'Banco'
  end
  object lb8: TLabel [6]
    Left = 252
    Top = 194
    Width = 88
    Height = 14
    Caption = 'Nosso nr. inicial'
  end
  object lb9: TLabel [7]
    Left = 354
    Top = 194
    Width = 95
    Height = 14
    Caption = 'Pr'#243'ximo nosso nr.'
  end
  object lb10: TLabel [8]
    Left = 460
    Top = 194
    Width = 77
    Height = 14
    Caption = 'Nosso nr. final'
  end
  inherited Label1: TLabel
    Width = 31
    Caption = 'Conta'
    ExplicitWidth = 31
  end
  object Label3: TLabel [10]
    Left = 126
    Top = 45
    Width = 20
    Height = 14
    Caption = 'D'#237'g.'
  end
  object lb4: TLabel [11]
    Left = 515
    Top = 45
    Width = 43
    Height = 14
    Caption = 'Carteira'
  end
  object lb11: TLabel [12]
    Left = 553
    Top = 195
    Width = 88
    Height = 14
    Caption = 'Nr. remessa bol.'
  end
  object lb12: TLabel [13]
    Left = 515
    Top = 91
    Width = 126
    Height = 14
    Caption = 'C'#243'd. empresa cobran'#231'a'
  end
  inherited pnOpcoes: TPanel
    Height = 327
    TabOrder = 16
    ExplicitHeight = 327
  end
  inherited eID: TEditLuka
    Width = 67
    Alignment = taLeftJustify
    TabOrder = 0
    TipoCampo = tcTexto
    ExplicitWidth = 67
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 632
    Top = 0
    TabOrder = 17
    ExplicitLeft = 632
    ExplicitTop = 0
  end
  object eAgencia: TEditLuka
    Left = 158
    Top = 59
    Width = 58
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 5
    TabOrder = 4
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eDigitoConta: TEditLuka
    Left = 126
    Top = 59
    Width = 28
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 3
    TabOrder = 3
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object ckAceitaSaldoNegativo: TCheckBox
    Left = 126
    Top = 136
    Width = 161
    Height = 17
    Caption = '1 - Aceita saldo negativo'
    TabOrder = 9
    OnKeyDown = ProximoCampo
  end
  object eSaldoAtual: TEditLuka
    Left = 418
    Top = 105
    Width = 91
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clTeal
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 8
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eUltimoChequeEmitido: TEditLuka
    Left = 126
    Top = 208
    Width = 123
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 4
    TabOrder = 12
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object ckEmiteCheque: TCheckBox
    Left = 126
    Top = 154
    Width = 161
    Height = 17
    Caption = '2 - Emite cheque'
    TabOrder = 10
    OnClick = ckEmiteChequeClick
    OnKeyDown = ProximoCampo
  end
  object rgTipo: TRadioGroupLuka
    Left = 462
    Top = 3
    Width = 164
    Height = 40
    Caption = ' Tipo '
    Columns = 2
    Items.Strings = (
      'Caixa'
      'Banco')
    TabOrder = 2
    TabStop = True
    OnClick = rgTipoClick
  end
  object cbBanco: TComboBoxLuka
    Left = 285
    Top = 59
    Width = 224
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    TabOrder = 6
    OnKeyDown = ProximoCampo
    Items.Strings = (
      '246 -> Banco ABC Brasil S.A.'
      '075 -> Banco ABN AMRO S.A.'
      '025 -> Banco Alfa S.A.'
      '641 -> Banco Alvorada S.A.'
      '024 -> Banco BANDEPE S.A.'
      '029 -> Banco Banerj S.A.'
      '000 -> Banco Bankpar S.A.'
      '740 -> Banco Barclays S.A.'
      '107 -> Banco BBM S.A.'
      '031 -> Banco Beg S.A.'
      
        '096 -> Banco BM&FBOVESPA de Servi'#231'os de Liquida'#231#227'o e Cust'#243'dia S.' +
        'A'
      '318 -> Banco BMG S.A.'
      '752 -> Banco BNP Paribas Brasil S.A.'
      '248 -> Banco Boavista Interatl'#226'ntico S.A.'
      '218 -> Banco Bonsucesso S.A.'
      '065 -> Banco Bracce S.A.'
      '036 -> Banco Bradesco BBI S.A.'
      '204 -> Banco Bradesco Cart'#245'es S.A.'
      '394 -> Banco Bradesco Financiamentos S.A.'
      '237 -> Banco Bradesco S.A.'
      '208 -> Banco BTG Pactual S.A.'
      '263 -> Banco Cacique S.A.'
      '473 -> Banco Caixa Geral - Brasil S.A.'
      '040 -> Banco Cargill S.A.'
      '739 -> Banco Cetelem S.A.'
      '233 -> Banco Cifra S.A.'
      '745 -> Banco Citibank S.A.'
      'M08 -> Banco Citicard S.A.'
      'M19 -> Banco CNH Industrial Capital S.A.'
      '215 -> Banco Comercial e de Investimento Sudameris S.A.'
      '095 -> Banco Confidence de C'#226'mbio S.A.'
      '756 -> Banco Cooperativo do Brasil S.A. - BANCOOB'
      '748 -> Banco Cooperativo Sicredi S.A.'
      '222 -> Banco Credit Agricole Brasil S.A.'
      '505 -> Banco Credit Suisse (Brasil) S.A.'
      '003 -> Banco da Amaz'#244'nia S.A.'
      '083-3 -> '#9'Banco da China Brasil S.A.'
      '707 -> Banco Daycoval S.A.'
      'M06 -> Banco de Lage Landen Brasil S.A.'
      '456 -> Banco de Tokyo-Mitsubishi UFJ Brasil S.A.'
      '214 -> Banco Dibens S.A.'
      '001 -> Banco do Brasil S.A.'
      '047 -> Banco do Estado de Sergipe S.A.'
      '037 -> Banco do Estado do Par'#225' S.A.'
      '041 -> Banco do Estado do Rio Grande do Sul S.A.'
      '004 -> Banco do Nordeste do Brasil S.A.'
      '265 -> Banco Fator S.A.'
      'M03 -> Banco Fiat S.A.'
      '224 -> Banco Fibra S.A.'
      '626 -> Banco Ficsa S.A.'
      '394 -> Banco Finasa BMC S.A.'
      'M18 -> Banco Ford S.A.'
      'M07 -> Banco GMAC S.A.'
      '612 -> Banco Guanabara S.A.'
      'M22 -> Banco Honda S.A.'
      '063 -> Banco Ibi S.A. Banco M'#250'ltiplo'
      'M11 -> Banco IBM S.A.'
      '604 -> Banco Industrial do Brasil S.A.'
      '320 -> Banco Industrial e Comercial S.A.'
      '653 -> Banco Indusval S.A.'
      '249 -> Banco Investcred Unibanco S.A.'
      '184 -> Banco Ita'#250' BBA S.A.'
      '479 -> Banco Ita'#250'Bank S.A'
      'M09 -> Banco Itaucred Financiamentos S.A.'
      '376 -> Banco J. P. Morgan S.A.'
      '074 -> Banco J. Safra S.A.'
      '217 -> Banco John Deere S.A.'
      '600 -> Banco Luso Brasileiro S.A.'
      '389 -> Banco Mercantil do Brasil S.A.'
      '370 -> Banco Mizuho do Brasil S.A.'
      '746 -> Banco Modal S.A.'
      '045 -> Banco Opportunity S.A.'
      '212 -> Banco Original S.A.'
      '623 -> Banco Panamericano S.A.'
      '611 -> Banco Paulista S.A.'
      '643 -> Banco Pine S.A.'
      'M24 -> Banco PSA Finance Brasil S.A.'
      '747 -> Banco Rabobank International Brasil S.A.'
      '356  -> Banco Real S.A.'
      '633  -> Banco Rendimento S.A.'
      'M16  -> Banco Rodobens S.A.'
      '422  -> Banco Safra S.A.'
      '033  -> Banco Santander (Brasil) S.A.'
      '366  -> Banco Soci'#233't'#233' G'#233'n'#233'rale Brasil S.A.'
      '012  -> Banco Standard de Investimentos S.A.'
      '464  -> Banco Sumitomo Mitsui Brasileiro S.A.'
      '082-5  -> Banco Top'#225'zio S.A.'
      'M20  -> Banco Toyota do Brasil S.A.'
      '634  -> Banco Tri'#226'ngulo S.A.'
      'M14  -> Banco Volkswagen S.A.'
      'M23  -> Banco Volvo (Brasil) S.A.'
      '655 -> Banco Votorantim S.A.'
      '610 -> Banco VR S.A.'
      '119 -> Banco Western Union do Brasil S.A.'
      '021 -> BANESTES S.A. Banco do Estado do Esp'#237'rito Santo'
      '719 -> Banif-Banco Internacional do Funchal (Brasil)S.A.'
      '755 -> Bank of America Merrill Lynch Banco M'#250'ltiplo S.A.'
      '073 -> BB Banco Popular do Brasil S.A.'
      '081-7 -> '#9'BBN Banco Brasileiro de Neg'#243'cios S.A.'
      '250 -> BCV - Banco de Cr'#233'dito e Varejo S.A.'
      '078 -> BES Investimento do Brasil S.A.-Banco de Investimento'
      '069 -> BPN Brasil Banco M'#250'ltiplo S.A.'
      '125 -> Brasil Plural S.A. - Banco M'#250'ltiplo'
      '070 -> BRB - Banco de Bras'#237'lia S.A.'
      '104 -> Caixa Econ'#244'mica Federal'
      '477 -> Citibank S.A.'
      '487 -> Deutsche Bank S.A. - Banco Alem'#227'o'
      '064 -> Goldman Sachs do Brasil Banco M'#250'ltiplo S.A.'
      '062 -> Hipercard Banco M'#250'ltiplo S.A.'
      '399 -> HSBC Bank Brasil S.A. - Banco M'#250'ltiplo'
      '492 -> ING Bank N.V.'
      '652 -> Ita'#250' Unibanco Holding S.A.'
      '341 -> Ita'#250' Unibanco S.A.'
      '488 -> JPMorgan Chase Bank'
      '254 -> Paran'#225' Banco S.A.'
      '751 -> Scotiabank Brasil S.A. Banco M'#250'ltiplo'
      '409 -> UNIBANCO - Uni'#227'o de Bancos Brasileiros S.A.')
    Valores.Strings = (
      '246'
      '075'
      '025'
      '641'
      '024'
      '029'
      '000'
      '740'
      '107'
      '031'
      '096'
      '318'
      '752'
      '248'
      '218'
      '065'
      '036'
      '204'
      '394'
      '237'
      '208'
      '263'
      '473'
      '040'
      '739'
      '233'
      '745'
      'M08'
      'M19'
      '215'
      '095'
      '756'
      '748'
      '222'
      '505'
      '003'
      '083-3'
      '707'
      'M06'
      '456'
      '214'
      '001'
      '047'
      '037'
      '041'
      '004'
      '265'
      'M03'
      '224'
      '626'
      '394'
      'M18'
      'M07'
      '612'
      'M22'
      '063'
      'M11'
      '604'
      '320'
      '653'
      '249'
      '184'
      '479'
      'M09'
      '376'
      '074'
      '217'
      '600'
      '389'
      '370'
      '746'
      '045'
      '212'
      '623'
      '611'
      '643'
      'M24'
      '747'
      '356'
      '633'
      'M16'
      '422'
      '033'
      '366'
      '012'
      '464'
      '082-5'
      'M20'
      '634'
      'M14'
      'M23'
      '655'
      '610'
      '119'
      '021'
      '719'
      '755'
      '073'
      '081-7'
      '250'
      '078'
      '069'
      '125'
      '070'
      '104'
      '477'
      '487'
      '064'
      '062'
      '399'
      '492'
      '652'
      '341'
      '488'
      '254'
      '751'
      '409')
    AsInt = 0
  end
  object eNome: TEditLuka
    Left = 197
    Top = 19
    Width = 259
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 1
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object ckEmiteBoletoBancario: TCheckBox
    Left = 126
    Top = 172
    Width = 161
    Height = 17
    Caption = '3 - Emite boleto banc'#225'rio'
    TabOrder = 11
    OnClick = ckEmiteBoletoBancarioClick
    OnKeyDown = ProximoCampo
  end
  object eNossoNumeroInicial: TEditLuka
    Left = 252
    Top = 208
    Width = 98
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 12
    TabOrder = 13
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eProximoNossoNumero: TEditLuka
    Left = 354
    Top = 208
    Width = 102
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 12
    TabOrder = 14
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eNossoNumeroFinal: TEditLuka
    Left = 460
    Top = 208
    Width = 87
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 12
    TabOrder = 15
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  inline FrEmpresa: TFrEmpresas
    Left = 126
    Top = 88
    Width = 290
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 7
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 88
    ExplicitWidth = 290
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 265
      Height = 23
      ExplicitWidth = 265
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 290
      ExplicitWidth = 290
      inherited lbNomePesquisa: TLabel
        Width = 47
        Height = 14
        Caption = 'Empresa'
        ExplicitWidth = 47
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 220
        ExplicitLeft = 220
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 265
      Height = 24
      ExplicitLeft = 265
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  object eDigitoAgencia: TEditLuka
    Left = 220
    Top = 59
    Width = 61
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 5
    TabOrder = 5
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eCarteira: TEditLuka
    Left = 515
    Top = 59
    Width = 67
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 18
    OnKeyDown = eIDKeyDown
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eNumeroRemessaBoleto: TEditLuka
    Left = 553
    Top = 208
    Width = 87
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 12
    TabOrder = 19
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eCodigoEmpresaCobranca: TEditLuka
    Left = 515
    Top = 105
    Width = 161
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 20
    TabOrder = 20
    OnKeyDown = eIDKeyDown
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  inline FrFuncionariosAutorizados: TFrFuncionarios
    Left = 126
    Top = 237
    Width = 320
    Height = 81
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 21
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 237
    ExplicitHeight = 81
    inherited sgPesquisa: TGridLuka
      Height = 64
      ExplicitHeight = 64
    end
    inherited PnTitulos: TPanel
      inherited lbNomePesquisa: TLabel
        Width = 139
        Height = 14
        Caption = 'Funcion'#225'rios autorizados'
        ExplicitWidth = 139
        ExplicitHeight = 14
      end
      inherited CkChaveUnica: TCheckBox
        Checked = False
        State = cbUnchecked
      end
      inherited pnSuprimir: TPanel
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Height = 65
      ExplicitHeight = 65
    end
  end
end
