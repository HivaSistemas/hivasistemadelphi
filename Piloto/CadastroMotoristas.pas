unit CadastroMotoristas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Sessao,
  FrameDiversosContatos, FrameDiversosEnderecos, EditTelefoneLuka, Cadastros, _RecordsCadastros,
  _FrameHerancaPrincipal, FrameEndereco, EditLukaData, ComboBoxLuka, _Cadastros, _RecordsEspeciais,
  Vcl.ComCtrls, Vcl.ExtCtrls, RadioGroupLuka, Vcl.Mask, EditCpfCnpjLuka, EditLuka, Vcl.Buttons,
  _Biblioteca, _Motoristas, Vcl.Menus, CheckBoxLuka, FrameCadastrosTelefones;

type
  TFormCadastroMotorista = class(TFormCadastros)
    lbNumeroCNH: TLabel;
    eNumeroCNH: TEditLuka;
    Label3: TLabel;
    eDataValidadeCNH: TEditLukaData;
    procedure sbPesquisarCadastrosClick(Sender: TObject);
  private
    procedure PreencherRegistro(vMotorista: RecMotoristas);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroMotorista }

procedure TFormCadastroMotorista.BuscarRegistro;
var
  vDados: TArray<RecMotoristas>;
begin
  inherited;

  if eID.AsInt = 0 then
    Exit;

  vDados := _Motoristas.BuscarMotoristas(Sessao.getConexaoBanco, 0, [eID.AsInt], False);
  if vDados <> nil then
    PreencherRegistro(vDados[0]);
end;

procedure TFormCadastroMotorista.ExcluirRegistro;
begin
  inherited;

end;

procedure TFormCadastroMotorista.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;

  vRetBanco :=
    _Motoristas.AtualizarMotorista(
      Sessao.getConexaoBanco,
      FPesquisouCadastro,
      eID.AsInt,
      FCadastro,
      FTelefones,
      FEnderecos,
      eNumeroCNH.Text,
      eDataValidadeCNH.AsData
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormCadastroMotorista.Modo(pEditando: Boolean);
begin
  inherited;

  _Biblioteca.Habilitar([
    eNumeroCNH,
    eDataValidadeCNH],
    pEditando
  );
end;

procedure TFormCadastroMotorista.PesquisarRegistro;
begin
  inherited;

end;

procedure TFormCadastroMotorista.PreencherRegistro(vMotorista: RecMotoristas);
begin
  eNumeroCNH.Text := vMotorista.NumeroCNH;
  eDataValidadeCNH.AsData := vMotorista.DataValidadeCNH;

  vMotorista.Free;
end;

procedure TFormCadastroMotorista.sbPesquisarCadastrosClick(Sender: TObject);
begin
  inherited;
  BuscarRegistro;
end;

procedure TFormCadastroMotorista.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if eNumeroCNH.Trim = '' then begin
    _Biblioteca.Exclamar('O n�mero da CNH n�o foi informada corretamente, verifique!');
    SetarFoco(eNumeroCNH);
    Abort;
  end;

  if not eDataValidadeCNH.DataOk then begin
    _Biblioteca.Exclamar('A data de validade da CNH n�o foi informada corretamente, verifique!');
    SetarFoco(eDataValidadeCNH);
    Abort;
  end;
end;

end.
