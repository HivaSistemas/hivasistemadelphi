unit InclusaoTitulosPagar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, FrameTiposCobranca, _RecordsEspeciais,
  Vcl.Mask, EditLukaData, FrameEmpresas, _ContasPagar, _Biblioteca, _Sessao,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameClientes, Vcl.StdCtrls,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, FrameFornecedores,
  FramePlanosFinanceiros, FrameDadosCobranca, FrameCentroCustos, CheckBoxLuka,
  MemoAltis;

type
  TFormInclusaoTitulosPagar = class(TFormHerancaCadastroCodigo)
    FrFornecedor: TFrFornecedores;
    FrEmpresa: TFrEmpresas;
    lb1: TLabel;
    eValorDocumento: TEditLuka;
    FrCentroCustos: TFrCentroCustos;
    FrDadosCobranca: TFrDadosCobranca;
    FrPlanoFinanceiro: TFrPlanosFinanceiros;
    lbllb12: TLabel;
    lbl2: TLabel;
    eDataEmissao: TEditLukaData;
    meObservacoes: TMemoAltis;
    lb2: TLabel;
    eDataContabil: TEditLukaData;
    procedure eValorDocumentoChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure eDataEmissaoChange(Sender: TObject);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormInclusaoTitulosPagar }

procedure TFormInclusaoTitulosPagar.BuscarRegistro;
begin
  inherited;

end;

procedure TFormInclusaoTitulosPagar.eDataEmissaoChange(Sender: TObject);
begin
  inherited;
  if eDataEmissao.DataOk and not eDataContabil.DataOk then
    eDataContabil.AsData := eDataEmissao.AsData;
end;

procedure TFormInclusaoTitulosPagar.eValorDocumentoChange(Sender: TObject);
begin
  inherited;
  FrDadosCobranca.ValorPagar := eValorDocumento.AsDouble;
end;

procedure TFormInclusaoTitulosPagar.ExcluirRegistro;
begin
  inherited;

end;

procedure TFormInclusaoTitulosPagar.FormCreate(Sender: TObject);
begin
  inherited;
  FrDadosCobranca.Natureza := 'P';
end;

procedure TFormInclusaoTitulosPagar.GravarRegistro(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  Sessao.getConexaoBanco.IniciarTransacao;

  for i := Low(FrDadosCobranca.Titulos) to High(FrDadosCobranca.Titulos) do begin
    vRetBanco :=
      _ContasPagar.AtualizarContaPagar(
        Sessao.getConexaoBanco,
        eID.AsInt,
        FrEmpresa.getEmpresa().EmpresaId,
        FrFornecedor.GetFornecedor.cadastro.cadastro_id,
        FrDadosCobranca.Titulos[i].CobrancaId,
        FrDadosCobranca.Titulos[i].PortadorId,
        FrPlanoFinanceiro.getDados().PlanoFinanceiroId,
        FrDadosCobranca.Titulos[i].Documento,
        FrDadosCobranca.Titulos[i].Valor,
        0,
        'MAN',
        0,
        FrDadosCobranca.Titulos[i].Parcela,
        FrDadosCobranca.Titulos[i].NumeroParcelas,
        FrDadosCobranca.Titulos[i].NumeroCheque,
        'A',
        FrDadosCobranca.Titulos[i].DataVencimento,
        FrDadosCobranca.Titulos[i].DataVencimento,
        FrDadosCobranca.Titulos[i].NossoNumero,
        FrDadosCobranca.Titulos[i].CodigoBarras,
        'N',
        '',
        0,
        eDataEmissao.AsData,
        eDataContabil.AsData,
        meObservacoes.Lines.Text,
        FrCentroCustos.GetCentro().CentroCustoId,
        True
      );

    Sessao.AbortarSeHouveErro( vRetBanco );
  end;

  Sessao.getConexaoBanco.FinalizarTransacao;

  _Biblioteca.Informar('Novo contas a pagar gerado com sucesso. C�digo: ' + NFormat(vRetBanco.AsInt) );
end;

procedure TFormInclusaoTitulosPagar.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    FrFornecedor,
    eValorDocumento,
    FrEmpresa,
    FrPlanoFinanceiro,
    FrCentroCustos,
    eDataEmissao,
    eDataContabil,
    meObservacoes,
    FrDadosCobranca],
    pEditando
  );

  if pEditando then
    SetarFoco(FrFornecedor);
end;

procedure TFormInclusaoTitulosPagar.PesquisarRegistro;
begin
  inherited;

end;

procedure TFormInclusaoTitulosPagar.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrFornecedor.EstaVazio then begin
    Exclamar('O fornecedor n�o foi informado corretamente, verifique!');
    SetarFoco(FrFornecedor);
    Abort;
  end;

  if FrEmpresa.EstaVazio then begin
    Exclamar('O tipo de cobran�a n�o foi informado corretamente, verifique!');
    SetarFoco(FrEmpresa);
    Abort;
  end;

  if FrCentroCustos.EstaVazio then begin
    Exclamar('O centro de custo n�o foi informado corretamente, verifique!');
    SetarFoco(FrCentroCustos);
    Abort;
  end;

  if eValorDocumento.AsCurr = 0 then begin
    Exclamar('O valor do t�tulo tem que ser maior que 0, verifique!');
    SetarFoco(eValorDocumento);
    Abort;
  end;

  if FrDadosCobranca.ExisteDiferenca then begin
    Exclamar('Existe diferen�a nos t�tulos lan�ados, verifique!');
    SetarFoco(FrDadosCobranca);
    Abort;
  end;

  if not eDataEmissao.DataOk then begin
    Exclamar('A data de emiss�o n�o foi definida corretamente, verifique!');
    SetarFoco(eDataEmissao);
    Abort;
  end;

  if not eDataContabil.DataOk then begin
    Exclamar('A data cont�bil n�o foi definida corretamente, verifique!');
    SetarFoco(eDataContabil);
    Abort;
  end;

end;

end.
