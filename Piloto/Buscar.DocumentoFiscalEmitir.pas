unit Buscar.DocumentoFiscalEmitir;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Biblioteca,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, Frame.DocumentoFiscalEmitir;

type
  TFormBuscarDocumentoFiscalEmitir = class(TFormHerancaFinalizar)
    FrDocumentoFiscalEmitir: TFrameDocumentoFiscalEmitir;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function Buscar(pEntregaComSeparacao: Boolean): TRetornoTelaFinalizar<RecDocumentoFiscalEmitir>;

implementation

{$R *.dfm}

function Buscar(pEntregaComSeparacao: Boolean): TRetornoTelaFinalizar<RecDocumentoFiscalEmitir>;
var
  vForm: TFormBuscarDocumentoFiscalEmitir;
begin
  vForm := TFormBuscarDocumentoFiscalEmitir.Create(nil);

  if pEntregaComSeparacao then
    vForm.FrDocumentoFiscalEmitir.ckRecibo.Enabled := False;

  if vForm.FrDocumentoFiscalEmitir.Enabled then
    vForm.FrDocumentoFiscalEmitir.ckRecibo.Checked := True;

  if Result.Ok(vForm.ShowModal, True) then
    Result.Dados := vForm.FrDocumentoFiscalEmitir.GetDocumentosEmitir;
end;

end.
