inherited FormImpressaoCartazPromocao: TFormImpressaoCartazPromocao
  Caption = 'FormImpressaoCartazPromocao'
  ClientWidth = 1229
  ExplicitWidth = 1245
  ExplicitHeight = 754
  PixelsPerInch = 96
  TextHeight = 13
  inherited qrRelatorioNF: TQuickRep
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Page.Values = (
      0.000000000000000000
      509.322916666666800000
      10.000000000000000000
      750.093750000000000000
      10.000000000000000000
      0.000000000000000000
      0.000000000000000000)
    inherited qrBandTitulo: TQRBand
      Size.Values = (
        198.437500000000000000
        740.833333333333400000)
      inherited qrlNFNomeEmpresa: TQRLabel
        Size.Values = (
          34.726562500000000000
          6.614583333333332000
          16.536458333333330000
          744.140625000000000000)
        FontSize = 6
      end
      inherited qrNFCnpj: TQRLabel
        Size.Values = (
          33.072916666666670000
          83.784722222222220000
          -4.409722222222222000
          641.614583333333200000)
        FontSize = 7
      end
      inherited qrNFEndereco: TQRLabel
        Size.Values = (
          34.395833333333340000
          84.666666666666680000
          21.166666666666670000
          640.291666666666800000)
        FontSize = 7
      end
      inherited qrNFCidadeUf: TQRLabel
        Size.Values = (
          34.395833333333340000
          84.666666666666680000
          47.625000000000000000
          277.812500000000000000)
        FontSize = 7
      end
      inherited qrNFTipoDocumento: TQRLabel
        Size.Values = (
          33.072916666666670000
          1.322916666666667000
          87.312500000000000000
          719.666666666666800000)
        FontSize = 7
      end
      inherited qrSeparador2: TQRShape
        Size.Values = (
          2.645833333333333000
          3.968750000000000000
          158.750000000000000000
          724.958333333333200000)
      end
      inherited qrNFTitulo: TQRLabel
        Size.Values = (
          33.072916666666670000
          1.322916666666667000
          121.708333333333300000
          719.666666666666800000)
        FontSize = 7
      end
      inherited qrlNFEmitidoEm: TQRLabel
        Size.Values = (
          23.151041666666670000
          370.416666666666700000
          3.307291666666667000
          357.187500000000000000)
        FontSize = -6
      end
      inherited qrlNFTelefoneEmpresa: TQRLabel
        Size.Values = (
          42.994791666666670000
          6.614583333333332000
          167.018229166666700000
          744.140625000000000000)
        FontSize = 7
      end
    end
    inherited PageFooterBand1: TQRBand
      Size.Values = (
        24.804687500000000000
        740.833333333333400000)
      inherited qrlNFSistema: TQRLabel
        Size.Values = (
          23.151041666666670000
          582.083333333333400000
          0.000000000000000000
          138.906250000000000000)
        FontSize = -6
      end
    end
  end
  inherited qrRelatorioA4: TQuickRep
    Left = 829
    Top = -6
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    ExplicitLeft = 829
    ExplicitTop = -6
    inherited qrCabecalho: TQRBand
      Size.Values = (
        227.541666666666700000
        1899.708333333333000000)
      inherited qrCaptionEndereco: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          44.979166666666670000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrEmpresa: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          7.937500000000000000
          568.854166666666700000)
        FontSize = 7
      end
      inherited qrEndereco: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          44.979166666666670000
          1513.416666666667000000)
        FontSize = 7
      end
      inherited qrCaptionEmpresa: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          7.937500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrEmitidoEm: TQRLabel
        Size.Values = (
          29.104166666666670000
          1529.291666666667000000
          0.000000000000000000
          370.416666666666700000)
        FontSize = 5
      end
      inherited qr1: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          82.020833333333320000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrBairro: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          82.020833333333320000
          568.854166666666700000)
        FontSize = 7
      end
      inherited qr3: TQRLabel
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          82.020833333333320000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrCidadeUf: TQRLabel
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          82.020833333333320000
          568.854166666666700000)
        FontSize = 7
      end
      inherited qr5: TQRLabel
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          7.937500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrCNPJ: TQRLabel
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          7.937500000000000000
          248.708333333333300000)
        FontSize = 7
      end
      inherited qr7: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrTelefone: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          119.062500000000000000
          187.854166666666700000)
        FontSize = 7
      end
      inherited qrFax: TQRLabel
        Size.Values = (
          34.395833333333330000
          767.291666666666800000
          119.062500000000000000
          187.854166666666700000)
        FontSize = 7
      end
      inherited qr10: TQRLabel
        Size.Values = (
          34.395833333333330000
          603.250000000000000000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qr11: TQRLabel
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrEmail: TQRLabel
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          119.062500000000000000
          769.937500000000000000)
        FontSize = 7
      end
      inherited qr13: TQRLabel
        Size.Values = (
          52.916666666666670000
          711.729166666666800000
          164.041666666666700000
          529.166666666666700000)
        FontSize = 12
      end
      inherited QRShape1: TQRShape
        Size.Values = (
          29.104166666666670000
          0.000000000000000000
          211.666666666666700000
          1899.708333333333000000)
      end
      inherited qrLogoEmpresa: TQRImage
        Size.Values = (
          211.666666666666700000
          13.229166666666670000
          2.645833333333333000
          211.666666666666700000)
      end
      inherited qrLabelCep: TQRLabel
        Size.Values = (
          39.687500000000000000
          1656.291666666667000000
          82.020833333333320000
          74.083333333333320000)
        FontSize = 9
      end
      inherited qrCEP: TQRLabel
        Size.Values = (
          39.687500000000000000
          1735.666666666667000000
          82.020833333333320000
          171.979166666666700000)
        FontSize = 9
      end
    end
    inherited qrTitulo: TQRBand
      Size.Values = (
        89.958333333333340000
        1899.708333333333000000)
      inherited QRShape2: TQRShape
        Size.Values = (
          29.104166666666670000
          0.000000000000000000
          71.437500000000000000
          1899.708333333333000000)
      end
    end
    inherited qrBandaCabecalhoColunas: TQRBand
      Size.Values = (
        52.916666666666670000
        1899.708333333333000000)
    end
    inherited qrBandaDetalhes: TQRBand
      Size.Values = (
        50.270833333333330000
        1899.708333333333000000)
    end
    inherited qrbndRodape: TQRBand
      Size.Values = (
        116.416666666666700000
        1899.708333333333000000)
      inherited qrUsuarioImpressao: TQRLabel
        Size.Values = (
          31.750000000000000000
          7.937500000000000000
          74.083333333333340000
          354.541666666666700000)
        FontSize = 7
      end
      inherited qrSistema: TQRLabel
        Size.Values = (
          31.750000000000000000
          1529.291666666667000000
          74.083333333333340000
          370.416666666666700000)
        FontSize = 7
      end
    end
  end
  object qrPromocao: TQuickRep [2]
    Left = 2
    Top = -499
    Width = 794
    Height = 1123
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE')
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Options = [FirstPageHeader, LastPageFooter]
    Page.Columns = 1
    Page.Orientation = poPortrait
    Page.PaperSize = A4
    Page.Continuous = False
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    PrinterSettings.Copies = 1
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.UseStandardprinter = False
    PrinterSettings.UseCustomBinCode = False
    PrinterSettings.CustomBinCode = 0
    PrinterSettings.ExtendedDuplex = 0
    PrinterSettings.UseCustomPaperCode = False
    PrinterSettings.CustomPaperCode = 0
    PrinterSettings.PrintMetaFile = False
    PrinterSettings.MemoryLimit = 1000000
    PrinterSettings.PrintQuality = 0
    PrinterSettings.Collate = 0
    PrinterSettings.ColorOption = 0
    PrintIfEmpty = True
    SnapToGrid = True
    Units = MM
    Zoom = 100
    PrevFormStyle = fsNormal
    PreviewInitialState = wsMaximized
    PrevShowThumbs = False
    PrevShowSearch = False
    PrevInitialZoom = qrZoomToWidth
    PreviewDefaultSaveType = stPDF
    PreviewLeft = 0
    PreviewTop = 0
    object QRBand2: TQRBand
      Left = 38
      Top = 38
      Width = 718
      Height = 158
      AlignToBottom = False
      Color = clRed
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        418.041666666666700000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbPageHeader
      object QRLabel12: TQRLabel
        Left = 128
        Top = 20
        Width = 487
        Height = 94
        Size.Values = (
          248.708333333333300000
          338.666666666666700000
          52.916666666666660000
          1288.520833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'PROMO'#199#195'O'
        Color = clRed
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clYellow
        Font.Height = -80
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 60
      end
    end
    object QRBand3: TQRBand
      Left = 38
      Top = 196
      Width = 718
      Height = 890
      AlignToBottom = False
      Color = clYellow
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        2354.791666666667000000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbSummary
      object qrProduto: TQRLabel
        Left = 51
        Top = 159
        Width = 615
        Height = 134
        Size.Values = (
          354.541666666666700000
          134.937500000000000000
          420.687500000000000000
          1627.187500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '1615 - CIMENTO TOCANTINS TODAS OBRAS - 50KG'
        Color = clYellow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -37
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 28
      end
      object qrPreco: TQRLabel
        Left = 138
        Top = 415
        Width = 432
        Height = 47
        Size.Values = (
          124.354166666666700000
          365.125000000000000000
          1098.020833333333000000
          1143.000000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '  de R$ 5.000,00  '
        Color = clYellow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = [fsStrikeOut]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 25
      end
      object qrPrecoPromocao: TQRLabel
        Left = 138
        Top = 468
        Width = 432
        Height = 61
        Size.Values = (
          161.395833333333300000
          365.125000000000000000
          1238.250000000000000000
          1143.000000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'por R$ 2.500,00'
        Color = clYellow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -53
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 40
      end
      object qrUnidade: TQRLabel
        Left = 438
        Top = 535
        Width = 133
        Height = 28
        Size.Values = (
          74.083333333333340000
          1158.875000000000000000
          1415.520833333333000000
          351.895833333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'SC'
        Color = clYellow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -20
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 15
      end
      object qrMarca: TQRLabel
        Left = 9
        Top = 663
        Width = 689
        Height = 24
        Size.Values = (
          63.500000000000000000
          23.812500000000000000
          1754.187500000000000000
          1822.979166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'MARCA: TOCANTINS'
        Color = clYellow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -20
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 15
      end
      object qrCodigoOriginal: TQRLabel
        Left = 9
        Top = 693
        Width = 689
        Height = 36
        Size.Values = (
          95.250000000000000000
          23.812500000000000000
          1833.562500000000000000
          1822.979166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'COD. DA FABRICA: 74980999BZ'
        Color = clYellow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -20
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 15
      end
      object qrCaracteristica: TQRLabel
        Left = 3
        Top = 743
        Width = 685
        Height = 100
        Size.Values = (
          264.583333333333400000
          7.937500000000000000
          1965.854166666667000000
          1812.395833333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 
          'CARACTER'#205'STICA: CaraLorem Ipsum is simply dummy text of the prin' +
          'ting and typesetting industry. Lorem Ipsum has been the industry' +
          #39's standard dummy text ever since the 1500s, when an unknown pri' +
          'nter took a galley of type and scrambled it to make a type speci' +
          'men book. It has survived not only five centuries, but also the ' +
          'leap into electronic typesetting, remaining essentially unchange' +
          'd. It was popularised in the 1960s with the release of Letraset '
        Color = clYellow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 12
      end
      object qrValidadePromocao: TQRLabel
        Left = 465
        Top = 862
        Width = 233
        Height = 17
        Size.Values = (
          44.979166666666670000
          1230.312500000000000000
          2280.708333333333000000
          616.479166666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'PROMO'#199#195'O V'#193'LIDA AT'#201' 05/09/2024'
        Color = clYellow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
    end
  end
end
