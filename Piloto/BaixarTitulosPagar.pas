unit BaixarTitulosPagar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Grids, GridLuka, _Biblioteca,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, _ContasPagar, _Sessao, ImpressaoReciboPagamentoContasPagar,
  Vcl.StdCtrls, EditLuka, Vcl.Mask, EditLukaData, _RecordsFinanceiros, _TurnosCaixas,
  Vcl.Buttons, Vcl.ExtCtrls, System.StrUtils, System.Math, _RecordsEspeciais, _ContasPagarBaixas,
  _MovimentosTurnosItens, _ContasPagarBaixasItens, Informacoes.TituloPagar,
  FramePagamentoFinanceiro;

type
  TFormBaixarTitulosPagar = class(TFormHerancaFinalizar)
    lb8: TLabel;
    lb22: TLabel;
    lb23: TLabel;
    lb1: TLabel;
    lb2: TLabel;
    eDataPagamento: TEditLukaData;
    eValorTitulos: TEditLuka;
    eValorDescontos: TEditLuka;
    eValorTotal: TEditLuka;
    eObservacoes: TEditLuka;
    sgTitulos: TGridLuka;
    FrPagamento: TFrPagamentoFinanceiro;
    procedure sgTitulosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgTitulosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgTitulosDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FFornecedorId: Integer;
    FTitulos: TArray<RecContaPagar>;
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function BaixarTitulos(const pFornecedorId: Integer; pTitulosIds: TArray<Integer>; pHabilitarTodasFormasPagto: Boolean): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

const
  coDocumento      = 0;
  coFornecedor     = 1;
  coParcela        = 2;
  coValor          = 3;
  coDesconto       = 4;
  coValorTotal     = 5;
  coDataVencimento = 6;
  coDiasAtraso     = 7;
  coTipoCobranca   = 8;
  coContaPagarId   = 9;

function BaixarTitulos(const pFornecedorId: Integer; pTitulosIds: TArray<Integer>; pHabilitarTodasFormasPagto: Boolean): TRetornoTelaFinalizar;
var
  i: Integer;

  vForm: TFormBaixarTitulosPagar;
  vTitulos: TArray<RecContaPagar>;

  vValorTotalTitulos: Currency;
  vValorTotalDescontos: Currency;
begin
  vTitulos := _ContasPagar.BuscarContasPagarComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('COP.PAGAR_ID', pTitulosIds));
  if vTitulos = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vForm := TFormBaixarTitulosPagar.Create(Application);
  vForm.FFornecedorId := pFornecedorId;

  vValorTotalTitulos := 0;
  vValorTotalDescontos := 0;
  with vForm do begin
    for i := Low(vTitulos) to High(vTitulos) do begin
      sgTitulos.Cells[coDocumento, i + 1]      := vTitulos[i].documento;
      sgTitulos.Cells[coFornecedor, i + 1]     := NFormat(vTitulos[i].CadastroId) + ' - ' + vTitulos[i].nome_fornecedor;
      sgTitulos.Cells[coParcela, i + 1]        := NFormat(vTitulos[i].parcela);
      sgTitulos.Cells[coValor, i + 1]          := NFormat(vTitulos[i].ValorDocumento);
      sgTitulos.Cells[coDesconto, i + 1]       := NFormatN(vTitulos[i].ValorDesconto);
      sgTitulos.Cells[coValorTotal, i + 1]     := NFormat(vTitulos[i].ValorSaldo);
      sgTitulos.Cells[coDataVencimento, i + 1] := DFormat(vTitulos[i].data_vencimento);
      sgTitulos.Cells[coDiasAtraso, i + 1]     := NFormat(IfThen(vTitulos[i].dias_atraso > 0, vTitulos[i].dias_atraso));
      sgTitulos.Cells[coTipoCobranca, i + 1]   := NFormat(vTitulos[i].cobranca_id) + ' - ' + vTitulos[i].nome_tipo_cobranca;
      sgTitulos.Cells[coContaPagarId, i + 1]   := NFormat(vTitulos[i].PagarId);

      vValorTotalTitulos := vValorTotalTitulos + vTitulos[i].ValorDocumento - vTitulos[i].ValorAdiantado;
      vValorTotalDescontos := vValorTotalDescontos + vTitulos[i].ValorDesconto;
    end;
    sgTitulos.RowCount := High(vTitulos) + 2;

    eValorTitulos.AsCurr   := vValorTotalTitulos;
    eValorDescontos.AsCurr := vValorTotalDescontos;
    eValorTotal.AsCurr     := vValorTotalTitulos - vValorTotalDescontos;

    FTitulos := vTitulos;
    FrPagamento.ValorPagar            := eValorTotal.AsCurr;
    FrPagamento.eValorDesconto.AsCurr := vValorTotalDescontos;
    FrPagamento.CadastroId            := pFornecedorId;

    if not pHabilitarTodasFormasPagto then
      vForm.FrPagamento.ApenasDinheiroCheque;

    Result.Ok(ShowModal, True);
  end;
end;

{ TFormBaixarTitulosPagar }

procedure TFormBaixarTitulosPagar.Finalizar(Sender: TObject);
var
  i: Integer;
  vBaixaId: Integer;
  vRetBanco: RecRetornoBD;

  vValorPropDesconto: Currency;
  vValorPropDinheiro: Currency;
  vValorPropCheque: Currency;
//  vValorPropCartao: Currency;
  vValorPropCobranca: Currency;
  vValorPropCredito: Currency;

  vSaldoDesconto: Currency;
  vSaldoDinheiro: Currency;
  vSaldoCheque: Currency;
//  vSaldoCartao: Currency;
  vSaldoCobranca: Currency;
  vSaldoCredito: Currency;

  vTurnoId: Integer;

begin
  vValorPropDesconto := 0;
  vValorPropDinheiro := 0;
  vValorPropCheque   := 0;
//  vValorPropCartao   := 0;
  vValorPropCobranca := 0;
  vValorPropCredito  := 0;

  vSaldoDesconto := FrPagamento.eValorDesconto.AsCurr;
  vSaldoDinheiro := FrPagamento.eValorDinheiro.AsCurr;
  vSaldoCheque   := FrPagamento.eValorCheque.AsCurr;

//  vSaldoCartao   := FrPagamento.eValorCartao.AsCurr;

  vSaldoCobranca := FrPagamento.eValorCobranca.AsCurr;
  vSaldoCredito  := FrPagamento.eValorCredito.AsCurr;

  vTurnoId := _TurnosCaixas.getTurnoId( Sessao.getConexaoBanco, Sessao.getUsuarioLogado.funcionario_id );
  if (vTurnoId = 0) and (FrPagamento.Dinheiro = nil) and (FrPagamento.eValorDinheiro.AsCurr > 0) then begin
    _Biblioteca.Exclamar('Seu turno n�o esta aberto! Por favor uma conta para efetuar o pagamento!');
    SetarFoco(FrPagamento.eValorDinheiro);
    Abort;
  end;

  Sessao.getConexaoBanco.IniciarTransacao;

{ ******************* Baixa dos t�tulos e sa�da do banco ******************* }
  // Gerando a capa da baixa
  vRetBanco :=
    _ContasPagarBaixas.AtualizarContasPagarBaixas(
      Sessao.getConexaoBanco,
      0,
      Sessao.getEmpresaLogada.EmpresaId,
      FFornecedorId,
      IIfInt((vTurnoId > 0) and (FrPagamento.Dinheiro = nil), vTurnoId),
      'PG',
      FrPagamento.eValorDinheiro.AsCurr,
      FrPagamento.eValorCheque.AsCurr,
      0, //FrPagamento.eValorCartao.AsCurr,
      FrPagamento.eValorCobranca.AsCurr,
      FrPagamento.eValorCredito.AsCurr,
      eValorTitulos.AsCurr,
      FrPagamento.eValorJuros.AsCurr,
      FrPagamento.eValorDesconto.AsCurr,
      eDataPagamento.AsData,
      eObservacoes.Text,
      'BCP',
      0,
      FrPagamento.Dinheiro,
      nil,
      FrPagamento.Cobrancas,
      FrPagamento.Cheques,
      FrPagamento.Creditos,
      True
    );

  Sessao.AbortarSeHouveErro( vRetBanco );

  vBaixaId := vRetBanco.AsInt;
  for i := Low(FTitulos) to High(FTitulos) do begin
    if FrPagamento.eValorDesconto.AsCurr > 0 then
      vValorPropDesconto := Arredondar(FrPagamento.eValorDesconto.AsCurr / Length(FTitulos), 2);

    if FrPagamento.eValorDinheiro.AsCurr > 0 then
      vValorPropDinheiro := Arredondar(FrPagamento.eValorDinheiro.AsCurr / Length(FTitulos), 2);

    if FrPagamento.eValorCheque.AsCurr > 0 then
      vValorPropCheque   := Arredondar(FrPagamento.eValorCheque.AsCurr / Length(FTitulos), 2);

//    if FrPagamento.eValorCartao.AsCurr > 0 then
//      vValorPropCartao   := Arredondar(FrPagamento.eValorCartao.AsCurr / Length(FTitulos), 2);

    if FrPagamento.eValorCobranca.AsCurr > 0 then
      vValorPropCobranca := Arredondar(FrPagamento.eValorCobranca.AsCurr / Length(FTitulos), 2);

    if FrPagamento.eValorCredito.AsCurr > 0 then
      vValorPropCredito  := Arredondar(FrPagamento.eValorCredito.AsCurr / Length(FTitulos), 2);

    vSaldoDesconto := vSaldoDesconto - vValorPropDesconto;
    vSaldoDinheiro := vSaldoDinheiro - vValorPropDinheiro;
    vSaldoCheque   := vSaldoCheque - vValorPropCheque;
//    vSaldoCartao   := vSaldoCartao - vValorPropCartao;
    vSaldoCobranca := vSaldoCobranca - vValorPropCobranca;
    vSaldoCredito  := vSaldoCredito - vValorPropCredito;

    if i = High(FTitulos) then begin
      vValorPropDesconto := vValorPropDesconto + vSaldoDesconto;
      vValorPropDinheiro := vValorPropDinheiro + vSaldoDinheiro;
      vValorPropCheque   := vValorPropCheque + vSaldoCheque;
//      vValorPropCartao   := vValorPropCartao + vSaldoCartao;
      vValorPropCobranca := vValorPropCobranca + vSaldoCobranca;
      vValorPropCredito  := vValorPropCredito + vSaldoCredito;
    end;

    // Gravando os t�tulos baixados
    vRetBanco :=
      _ContasPagarBaixasItens.AtualizarContasPagarBaixasItens(
        Sessao.getConexaoBanco,
        vBaixaId,
        FTitulos[i].PagarId,
        vValorPropDinheiro,
        vValorPropCheque,
        0,
        vValorPropCobranca,
        vValorPropCredito,
        vValorPropDesconto,
        0
      );

    Sessao.AbortarSeHouveErro( vRetBanco );;
  end;
{ ******************* ******************************** ******************* }

  Sessao.getConexaoBanco.FinalizarTransacao;

  RotinaSucesso;
  if Perguntar('Deseja imprimir o comprovante de baixa dos t�tulos?') then
    ImpressaoReciboPagamentoContasPagar.Imprimir(vBaixaId);

  inherited;
end;

procedure TFormBaixarTitulosPagar.FormCreate(Sender: TObject);
begin
  inherited;
  FrPagamento.Natureza := 'P';
end;

procedure TFormBaixarTitulosPagar.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(eDataPagamento);
end;

procedure TFormBaixarTitulosPagar.sgTitulosDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.TituloPagar.Informar( SFormatInt(sgTitulos.Cells[coContaPagarId, sgTitulos.Row]) );
end;

procedure TFormBaixarTitulosPagar.sgTitulosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coParcela, coValor, coDesconto, coValorTotal, coDiasAtraso] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgTitulos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormBaixarTitulosPagar.sgTitulosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coDiasAtraso then
    AFont.Color := clRed
  else if ACol = coValorTotal then
    AFont.Color := $000096DB;
end;

procedure TFormBaixarTitulosPagar.VerificarRegistro(Sender: TObject);
var
  i: Integer;
  vTitulosIds: TArray<Integer>;
begin
  inherited;
  if not eDataPagamento.DataOk then begin
    Exclamar('A data do pagamento deve ser informada!');
    SetarFoco(eDataPagamento);
    Abort;
  end;

  FrPagamento.VerificarRegistro;

  vTitulosIds := nil;
  for i := Low(FTitulos) to High(FTitulos) do
    _Biblioteca.AddNoVetorSemRepetir( vTitulosIds, FTitulos[i].PagarId );

  if _ContasPagar.ExistemTitulosBaixados( Sessao.getConexaoBanco, vTitulosIds ) then begin
    Exclamar('Existem t�tulos para esta baixa que j� foram baixados, verifique!');
    SetarFoco(FrPagamento);
    Abort;
  end;
end;

end.
