unit CadastroLocaisProdutos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _RecordsCadastros, _RecordsEspeciais,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, ComboBoxLuka, _Sessao, _LocaisProdutos, _Biblioteca, PesquisaLocaisProdutos,
  CheckBoxLuka, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameEmpresas;

type
  TFormCadastroLocaisProdutos = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eNome: TEditLuka;
    cbTipoLocal: TComboBoxLuka;
    lb2: TLabel;
    ckPermiteDevolucaoVenda: TCheckBox;
    FrEmpresaDirecionarNota: TFrEmpresas;
    procedure FormCreate(Sender: TObject);
  private
    procedure PreencherRegistro(pLocal: RecLocaisProdutos);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroLocaisProdutos }

procedure TFormCadastroLocaisProdutos.BuscarRegistro;
var
  locais: TArray<RecLocaisProdutos>;
begin
  locais := _LocaisProdutos.BuscarLocaisProdutos(Sessao.getConexaoBanco, 0, [eId.AsInt]);

  if locais = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    eID.SetFocus;
    Exit;
  end;

  inherited;
  PreencherRegistro(locais[0]);
end;

procedure TFormCadastroLocaisProdutos.ExcluirRegistro;
var
  retorno: RecRetornoBD;
begin
  retorno := _LocaisProdutos.ExcluirLocaisProduto(Sessao.getConexaoBanco, eId.AsInt);

  if retorno.TeveErro then begin
    _Biblioteca.Exclamar(retorno.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroLocaisProdutos.FormCreate(Sender: TObject);
begin
  inherited;
  FrEmpresaDirecionarNota.Visible := Sessao.getParametros.UtilizaDirecionamentoPorLocais = 'S';
end;

procedure TFormCadastroLocaisProdutos.GravarRegistro(Sender: TObject);
var
  retorno: RecRetornoBD;
  vEmpresaDirecionarNota: Integer;
begin
  inherited;
  vEmpresaDirecionarNota := 0;
  if not FrEmpresaDirecionarNota.EstaVazio then
    vEmpresaDirecionarNota := FrEmpresaDirecionarNota.getEmpresa(0).EmpresaId;

  retorno :=
    _LocaisProdutos.AtualizarLocaisProduto(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eNome.Text,
      cbTipoLocal.GetValor,
      _Biblioteca.ToChar(ckPermiteDevolucaoVenda),
      _Biblioteca.ToChar(ckAtivo),
      vEmpresaDirecionarNota
    );

  if retorno.TeveErro then begin
    _Biblioteca.Exclamar(retorno.MensagemErro);
    Abort;
  end;

  if retorno.AsInt > 0 then
    _Biblioteca.Informar(coNovoRegistroSucessoCodigo + IntToStr(retorno.AsInt));
end;

procedure TFormCadastroLocaisProdutos.Modo(pEditando: Boolean);
begin
  inherited;

  _Biblioteca.Habilitar(
    [
      eNome,
      cbTipoLocal,
      ckPermiteDevolucaoVenda,
      ckAtivo,
      FrEmpresaDirecionarNota
    ],
    pEditando
  );

  if pEditando then
    eNome.SetFocus;
end;

procedure TFormCadastroLocaisProdutos.PesquisarRegistro;
var
  local: RecLocaisProdutos;
begin
  local := RecLocaisProdutos(PesquisaLocaisProdutos.PesquisarLocalProduto);

  if local = nil then
    Exit;

  inherited;
  PreencherRegistro(local);
end;

procedure TFormCadastroLocaisProdutos.PreencherRegistro(pLocal: RecLocaisProdutos);
begin
  eID.AsInt := pLocal.local_id;
  eNome.Text := pLocal.nome;
  cbTipoLocal.SetIndicePorValor(pLocal.tipo_local);
  ckPermiteDevolucaoVenda.Checked := (pLocal.permite_devolucao_venda = 'S');
  ckAtivo.Checked := (pLocal.ativo = 'S');
  FrEmpresaDirecionarNota.InserirDadoPorChave(pLocal.empresa_direcionar_nota);
end;

procedure TFormCadastroLocaisProdutos.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if eNome.Text = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar o nome do local!');
    eNome.SetFocus;
    Abort;
  end;

  if cbTipoLocal.GetValor = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar o tipo de local!');
    cbTipoLocal.SetFocus;
    Abort;
  end;
end;

end.
