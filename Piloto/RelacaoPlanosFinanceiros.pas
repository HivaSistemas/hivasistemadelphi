unit RelacaoPlanosFinanceiros;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatorios, Vcl.Grids, GridLuka, _PlanosFinanceiros,
  Vcl.Buttons, Vcl.ExtCtrls, _Sessao, _Biblioteca, Vcl.StdCtrls, CheckBoxLuka;

type
  TFormRelacaoPlanosFinanceiros = class(TFormHerancaRelatorios)
    sgPlanos: TGridLuka;
    SpeedButton2: TSpeedButton;
    procedure sgPlanosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sbImprimirClick(Sender: TObject);
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormRelacaoPlanosFinanceiros }

const
  coPlanoFinanceiroId = 0;
  coDescricao         = 1;
  coTipo              = 2;
  coExibirDRE         = 3;

procedure TFormRelacaoPlanosFinanceiros.Carregar(Sender: TObject);
var
  i: Integer;
  vPlanos: TArray<RecPlanosFinanceiros>;
begin
  inherited;
  sgPlanos.ClearGrid();

  vPlanos := _PlanosFinanceiros.BuscarPlanosFinanceiros(Sessao.getConexaoBanco, 2, [], False);
  for i := Low(vPlanos) to High(vPlanos) do begin
    sgPlanos.Cells[coPlanoFinanceiroId, i + 1] := vPlanos[i].PlanoFinanceiroId;
    sgPlanos.Cells[coDescricao, i + 1]         := vPlanos[i].Descricao;
    sgPlanos.Cells[coTipo, i + 1]              := vPlanos[i].Tipo;
    sgPlanos.Cells[coExibirDRE, i + 1]         := vPlanos[i].ExibirDre;
  end;
  sgPlanos.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vPlanos) );
end;

procedure TFormRelacaoPlanosFinanceiros.Imprimir(Sender: TObject);
begin
  inherited;

end;

procedure TFormRelacaoPlanosFinanceiros.sbImprimirClick(Sender: TObject);
begin
GridToExcel(sgPlanos);
end;

procedure TFormRelacaoPlanosFinanceiros.sgPlanosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  inherited;
  sgPlanos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taLeftJustify, Rect);
end;

procedure TFormRelacaoPlanosFinanceiros.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
