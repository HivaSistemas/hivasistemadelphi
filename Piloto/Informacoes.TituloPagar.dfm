inherited FormInformacoesTituloPagar: TFormInformacoesTituloPagar
  Caption = 'Informa'#231#245'es do t'#237'tulo a pagar'
  ClientHeight = 363
  ClientWidth = 673
  OnShow = FormShow
  ExplicitWidth = 679
  ExplicitHeight = 392
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 326
    Width = 673
    ExplicitTop = 326
    ExplicitWidth = 673
    object sbLogs: TSpeedButtonLuka
      Left = 600
      Top = 0
      Width = 69
      Height = 35
      Caption = '&Logs'
      Flat = True
      OnClick = sbLogsClick
      TagImagem = 8
      PedirCertificacao = False
      PermitirAutOutroUsuario = False
    end
  end
  object pc1: TPageControl
    Left = 0
    Top = 0
    Width = 673
    Height = 326
    ActivePage = tsOrigem
    Align = alClient
    TabOrder = 1
    object tsPrincipais: TTabSheet
      Caption = 'Principais'
      object lb1: TLabel
        Left = 4
        Top = 2
        Width = 58
        Height = 14
        Caption = 'Financeiro'
      end
      object lb2: TLabel
        Left = 79
        Top = 2
        Width = 61
        Height = 14
        Caption = 'Fornecedor'
      end
      object lb13: TLabel
        Left = 455
        Top = 2
        Width = 47
        Height = 14
        Caption = 'Empresa'
      end
      object lb12: TLabel
        Left = 281
        Top = 2
        Width = 93
        Height = 14
        Caption = 'Usu'#225'rio cadastro'
      end
      object lb3: TLabel
        Left = 4
        Top = 43
        Width = 93
        Height = 14
        Caption = 'Tipo de cobran'#231'a'
      end
      object lb5: TLabel
        Left = 208
        Top = 86
        Width = 31
        Height = 14
        Caption = 'Saldo'
      end
      object lb6: TLabel
        Left = 432
        Top = 43
        Width = 76
        Height = 14
        Caption = 'Data cadastro'
      end
      object lb10: TLabel
        Left = 599
        Top = 44
        Width = 63
        Height = 14
        Caption = 'Dias atraso'
      end
      object lb11: TLabel
        Left = 512
        Top = 43
        Width = 84
        Height = 14
        Caption = 'Data de vencto.'
      end
      object lb14: TLabel
        Left = 557
        Top = 86
        Width = 101
        Height = 14
        Caption = 'Data de vecto orig.'
      end
      object lb18: TLabel
        Left = 304
        Top = 86
        Width = 71
        Height = 14
        Caption = 'Num. cheque'
      end
      object lb22: TLabel
        Left = 4
        Top = 86
        Width = 60
        Height = 14
        Caption = 'Valor t'#237'tulo'
      end
      object lb23: TLabel
        Left = 84
        Top = 86
        Width = 51
        Height = 14
        Caption = 'Desconto'
      end
      object lb26: TLabel
        Left = 477
        Top = 126
        Width = 46
        Height = 14
        Caption = 'Portador'
      end
      object lbl1: TLabel
        Left = 167
        Top = 126
        Width = 92
        Height = 14
        Caption = 'Plano Financeiro'
      end
      object lbl2: TLabel
        Left = 4
        Top = 126
        Width = 91
        Height = 14
        Caption = 'Centro de Custos'
      end
      object lb16: TLabel
        Left = 281
        Top = 43
        Width = 65
        Height = 14
        Caption = 'Dt. emiss'#227'o'
      end
      object lb17: TLabel
        Left = 357
        Top = 43
        Width = 60
        Height = 14
        Caption = 'Dt.cont'#225'bil'
      end
      object lb20: TLabel
        Left = 4
        Top = 167
        Width = 69
        Height = 14
        Caption = 'Observa'#231#245'es'
      end
      object lb28: TLabel
        Left = 144
        Top = 86
        Width = 56
        Height = 14
        Caption = 'Vlr.adiant.'
      end
      object ePagarId: TEditLuka
        Left = 4
        Top = 16
        Width = 71
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 0
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eFornecedor: TEditLuka
        Left = 79
        Top = 16
        Width = 198
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 1
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eEmpresa: TEditLuka
        Left = 455
        Top = 16
        Width = 207
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 2
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eUsuarioCadastro: TEditLuka
        Left = 281
        Top = 16
        Width = 170
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 3
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eTipoCobranca: TEditLuka
        Left = 4
        Top = 57
        Width = 273
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 4
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eValorTotalTitulo: TEditLuka
        Left = 208
        Top = 100
        Width = 90
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clTeal
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 5
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eDataCadastro: TEditLukaData
        Left = 432
        Top = 57
        Width = 76
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        ReadOnly = True
        TabOrder = 6
        Text = '  /  /    '
      end
      object eDiasAtraso: TEditLuka
        Left = 599
        Top = 57
        Width = 63
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 7
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eDataVencimento: TEditLukaData
        Left = 512
        Top = 57
        Width = 83
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        ReadOnly = True
        TabOrder = 8
        Text = '  /  /    '
      end
      object eDataVencimentoOriginal: TEditLukaData
        Left = 557
        Top = 100
        Width = 105
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        ReadOnly = True
        TabOrder = 9
        Text = '  /  /    '
      end
      object eNumeroCheque: TEditLuka
        Left = 304
        Top = 100
        Width = 71
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 10
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eValorTitulo: TEditLuka
        Left = 4
        Top = 100
        Width = 76
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 11
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorDesconto: TEditLuka
        Left = 84
        Top = 100
        Width = 56
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 12
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eNomePortador: TEditLuka
        Left = 477
        Top = 140
        Width = 185
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 13
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object ePlanoFinanceiro: TEditLuka
        Left = 167
        Top = 140
        Width = 303
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 14
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eCentroCustos: TEditLuka
        Left = 4
        Top = 140
        Width = 156
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 15
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object StaticTextLuka4: TStaticTextLuka
        Left = 472
        Top = 89
        Width = 82
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Bloqueado?'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 16
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object txtBloqueado: TStaticText
        Left = 472
        Top = 105
        Width = 82
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'N'#227'o'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 17
        Transparent = False
      end
      object txtStatus: TStaticText
        Left = 380
        Top = 105
        Width = 87
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Aberto'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 18
        Transparent = False
      end
      object StaticTextLuka2: TStaticTextLuka
        Left = 380
        Top = 89
        Width = 87
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Status'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 19
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object eDataEmissao: TEditLukaData
        Left = 281
        Top = 57
        Width = 71
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        ReadOnly = True
        TabOrder = 20
        Text = '  /  /    '
      end
      object eDataContabil: TEditLukaData
        Left = 357
        Top = 57
        Width = 71
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        ReadOnly = True
        TabOrder = 21
        Text = '  /  /    '
      end
      object meObservacoes: TMemoAltis
        Left = 4
        Top = 181
        Width = 658
        Height = 116
        CharCase = ecUpperCase
        Lines.Strings = (
          'MEMOALTIS1')
        TabOrder = 22
      end
      object eValorAdiantado: TEditLuka
        Left = 144
        Top = 100
        Width = 59
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 23
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
    end
    object tsOrigem: TTabSheet
      Caption = 'Baixa / Origem'
      ImageIndex = 1
      object lb19: TLabel
        Left = 3
        Top = 107
        Width = 30
        Height = 14
        Caption = 'Baixa'
      end
      object sbInformacoesBaixa: TSpeedButton
        Left = 79
        Top = 125
        Width = 14
        Height = 18
        Hint = 'Abrir informa'#231#245'es da baixa deste t'#237'tulo'
        Caption = '...'
        OnClick = sbInformacoesBaixaClick
      end
      object lb24: TLabel
        Left = 104
        Top = 107
        Width = 76
        Height = 14
        Caption = 'Usu'#225'rio baixa'
      end
      object lb25: TLabel
        Left = 3
        Top = 38
        Width = 73
        Height = 14
        Caption = 'Baixa pg orig.'
      end
      object sbInformacoesOrigemBaixa: TSpeedButton
        Left = 78
        Top = 56
        Width = 14
        Height = 18
        Hint = 'Abrir informa'#231#245'es da baixa que originou este t'#237'tulo'
        Caption = '...'
        OnClick = sbInformacoesOrigemBaixaClick
      end
      object lb9: TLabel
        Left = 201
        Top = 38
        Width = 42
        Height = 14
        Caption = 'Entrada'
      end
      object sbInfoEntradaId: TSpeedButton
        Left = 275
        Top = 56
        Width = 14
        Height = 18
        Hint = 'Abrir informa'#231#245'es da entrada'
        Caption = '...'
        OnClick = sbInfoEntradaIdClick
      end
      object lb4: TLabel
        Left = 104
        Top = 38
        Width = 76
        Height = 14
        Caption = 'Baixa rec.orig.'
      end
      object sbBaixaReceberOrigemId: TSpeedButton
        Left = 180
        Top = 56
        Width = 14
        Height = 18
        Hint = 'Abrir informa'#231#245'es da baixa que originou este t'#237'tulo'
        Caption = '...'
        OnClick = sbBaixaReceberOrigemIdClick
      end
      object lb7: TLabel
        Left = 384
        Top = 107
        Width = 61
        Height = 14
        Caption = 'Data da bx.'
      end
      object lb8: TLabel
        Left = 459
        Top = 107
        Width = 80
        Height = 14
        Caption = 'Data de pagto.'
      end
      object lb15: TLabel
        Left = 296
        Top = 38
        Width = 58
        Height = 14
        Caption = 'Dev. venda'
      end
      object sbInformacoesDevolucaoVenda: TSpeedButton
        Left = 370
        Top = 56
        Width = 14
        Height = 18
        Hint = 'Abrir informa'#231#245'es da devolu'#231#227'o de venda'
        Caption = '...'
        OnClick = sbInformacoesDevolucaoVendaClick
      end
      object lb21: TLabel
        Left = 391
        Top = 38
        Width = 38
        Height = 14
        Caption = 'Pedido'
      end
      object sbInformacoesPedido: TSpeedButton
        Left = 465
        Top = 56
        Width = 14
        Height = 18
        Hint = 'Abrir informa'#231#245'es do or'#231'amento'
        Caption = '...'
        OnClick = sbInformacoesPedidoClick
      end
      object lb27: TLabel
        Left = 486
        Top = 38
        Width = 61
        Height = 14
        Caption = 'Acumulado'
      end
      object sbInformacoesAcumulado: TSpeedButton
        Left = 561
        Top = 56
        Width = 14
        Height = 18
        Hint = 'Abrir informa'#231#245'es do acumulado'
        Caption = '...'
        OnClick = sbInformacoesAcumuladoClick
      end
      object eBaixaId: TEditLuka
        Left = 3
        Top = 121
        Width = 76
        Height = 22
        Hint = 'C'#243'digo da baixa ao qual este t'#237'tulo pertence'
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 0
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eNomeUsuarioBaixa: TEditLuka
        Left = 104
        Top = 121
        Width = 274
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 1
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eBaixaOrigemId: TEditLuka
        Left = 3
        Top = 52
        Width = 75
        Height = 22
        Hint = 'C'#243'digo da baixa que originou este t'#237'tulo'
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 2
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eEntradaId: TEditLuka
        Left = 200
        Top = 52
        Width = 75
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 3
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eBaixaReceberOrigemId: TEditLuka
        Left = 104
        Top = 52
        Width = 76
        Height = 22
        Hint = 'C'#243'digo da baixa que originou este t'#237'tulo'
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 4
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eDataBaixa: TEditLukaData
        Left = 384
        Top = 121
        Width = 70
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        ReadOnly = True
        TabOrder = 5
        Text = '  /  /    '
      end
      object eDataPagamento: TEditLukaData
        Left = 459
        Top = 121
        Width = 83
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        ReadOnly = True
        TabOrder = 6
        Text = '  /  /    '
      end
      object eDevolucaoVenda: TEditLuka
        Left = 295
        Top = 52
        Width = 75
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 7
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eOrcamentoId: TEditLuka
        Left = 390
        Top = 52
        Width = 75
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 8
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eAcumuladoId: TEditLuka
        Left = 485
        Top = 52
        Width = 75
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 9
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object st1: TStaticTextLuka
        Left = 3
        Top = 3
        Width = 272
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Origem'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 10
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object stOrigem: TStaticText
        Left = 3
        Top = 19
        Width = 272
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Aberto'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = 10485760
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 11
        Transparent = False
      end
      object st3: TStaticTextLuka
        Left = 3
        Top = 88
        Width = 539
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Dados da baixa'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 12
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
    end
    object tsAdiantamentos: TTabSheet
      Caption = 'Adiantamentos'
      ImageIndex = 2
      object sgAdiantamentos: TGridLuka
        Left = 0
        Top = 0
        Width = 665
        Height = 297
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 4
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 0
        OnDblClick = sgAdiantamentosDblClick
        OnDrawCell = sgAdiantamentosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'C'#243'digo'
          'Data/hora adiant.'
          'Valor adiantamento'
          'Usu'#225'rio adiantamento')
        Grid3D = False
        RealColCount = 5
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          57
          115
          124
          316)
      end
    end
  end
end
