unit AcompanhamentoContagem;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _InventarioItens,
  Vcl.ComCtrls, Vcl.StdCtrls, CheckBoxLuka, Vcl.Buttons, Vcl.ExtCtrls, System.Math,
  Vcl.Grids, GridLuka, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, _Sessao,
  FrameInventario;

type
  TFormAcompanhamentoContagem = class(TFormHerancaRelatoriosPageControl)
    FrInventario: TFrInventario;
    st2: TStaticText;
    st1: TStaticText;
    sgItens: TGridLuka;
    sgCapa: TGridLuka;
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure sgCapaClick(Sender: TObject);
  private
    { Private declarations }
    vContagem: TArray<RecHistoricoContagemItens>;
  public
    { Public declarations }
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;

  end;

var
  FormAcompanhamentoContagem: TFormAcompanhamentoContagem;

implementation

{$R *.dfm}

uses _Biblioteca;
const
  coCodigoCapa       = 0;
  coNomeCapa         = 1;
  coDataInicioCapa   = 2;
  coDataFimCapa      = 3;
  coNroContagemCapa  = 4;

  coProdutoIdItem   = 0;
  coNomeProdutoItem = 1;
  coUnidadeItem     = 2;
  coMarcaItem       = 3;
  coLocalItem       = 4;
  coEstoqueItem     = 5;
  coQuantidadeItem  = 6;
  coDiferencaItem   = 7;

procedure TFormAcompanhamentoContagem.Carregar(Sender: TObject);
var
  vSql: String;
  vLinha, i: Integer;
  vSeq: string;
begin
  inherited;
  WhereOuAnd(vSql, ' EMPRESA_ID = ' + FrInventario.GetInventario.EmpresaId.ToString);
  WhereOuAnd(vSql, 'hico.'+FrInventario.getSqlFiltros('INVENTARIO_ID'));

  vContagem := _InventarioItens.BuscarHistoricoContagemItens(
    Sessao.getConexaoBanco,
    vSql );

  if vContagem = nil then
  begin
    _Biblioteca.NenhumRegistro;
    sgCapa.ClearGrid;
    sgItens.ClearGrid;
    Exit;
  end;

  vLinha := 0;
  for i := Low(vContagem) to High(vContagem) do
  begin
    if vContagem[i].ContagemNumero.ToString = vSeq then
      continue;

    vSeq := vContagem[i].ContagemNumero.ToString;


    sgCapa.Cells[coCodigoCapa, vLinha + 1]      := NFormat(vContagem[i].InventarioId);
    sgCapa.Cells[coNomeCapa, vLinha + 1]        := vContagem[i].InventarioNome;
    sgCapa.Cells[coDataInicioCapa, vLinha + 1]  := DHFormat(vContagem[i].InventarioInico);
    sgCapa.Cells[coDataFimCapa, vLinha + 1]     := DHFormat(vContagem[i].InventarioFim);
    sgCapa.Cells[coNroContagemCapa, vLinha + 1] := vContagem[i].ContagemNumero.ToString;
    inc(vLinha);
  end;
  sgCapa.SetLinhasGridPorTamanhoVetor( vLinha);
  SetarFoco(sgCapa);
  sgCapaClick(sgCapa);
end;

procedure TFormAcompanhamentoContagem.Imprimir(Sender: TObject);
begin
  inherited;
{}
end;

procedure TFormAcompanhamentoContagem.sgCapaClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
begin
  inherited;
  vLinha := 0;
  sgItens.ClearGrid;
  for i := Low(vContagem) to High(vContagem) do begin
    if sgCapa.Cells[coNroContagemCapa, sgCapa.Row] <> vContagem[i].ContagemNumero.ToString then
      Continue;

    Inc(vLinha);

    sgItens.Cells[coProdutoIdItem, vLinha]    := NFormat(vContagem[i].ProdutoId);
    sgItens.Cells[coNomeProdutoItem, vLinha]  := vContagem[i].NomeProduto;
    sgItens.Cells[coUnidadeItem, vLinha]      := vContagem[i].UnidadeVenda;
    sgItens.Cells[coMarcaItem, vLinha]        := NFormatN(vContagem[i].MarcaId) + ' - ' + vContagem[i].NomeMarca;
    sgItens.Cells[coLocalItem, vLinha]        := vContagem[i].LocalId.ToString;
    sgItens.Cells[coEstoqueItem, vLinha]      := vContagem[i].Estoque.ToString;
    sgItens.Cells[coQuantidadeItem, vLinha]   := vContagem[i].Quantidade.ToString;
    sgItens.Cells[coDiferencaItem, vLinha]    := vContagem[i].ContagemDiferenca.ToString;

  end;
  sgItens.RowCount := IfThen(vLinha > 1, vLinha + 1, 2);
end;

procedure TFormAcompanhamentoContagem.sgItensDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);

var
  vAlinhamento: TAlignment;
begin
  if ACol in[coProdutoIdItem, coNomeProdutoItem, coUnidadeItem, coMarcaItem, coLocalItem] then
    vAlinhamento := taLeftJustify
  else
    vAlinhamento := taRightJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormAcompanhamentoContagem.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrInventario.EstaVazio then begin
    Exclamar('A contagem de estoque inv�lida!');
    SetarFoco(FrInventario);
    sgCapa.ClearGrid;
    sgItens.ClearGrid;
    Abort;
  end;
end;

end.
