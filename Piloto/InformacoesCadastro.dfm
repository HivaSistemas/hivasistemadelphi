inherited FormInformacoesCadastro: TFormInformacoesCadastro
  Caption = 'Informa'#231#245'es de cadastro'
  ClientHeight = 376
  ClientWidth = 791
  ExplicitWidth = 797
  ExplicitHeight = 405
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 339
    Width = 791
    ExplicitTop = 427
    ExplicitWidth = 791
  end
  object pcDados: TPageControl
    Left = 0
    Top = 89
    Width = 791
    Height = 250
    ActivePage = tsPrincipais
    Align = alClient
    TabOrder = 1
    TabStop = False
    ExplicitHeight = 338
    object tsPrincipais: TTabSheet
      Caption = 'Principais'
      ExplicitHeight = 309
      object lb4: TLabel
        Left = 2
        Top = 45
        Width = 62
        Height = 14
        Caption = 'Estado civil'
      end
      object lbDataNascimento: TLabel
        Left = 564
        Top = -3
        Width = 93
        Height = 14
        Caption = 'Data nascimento'
      end
      object lb6: TLabel
        Left = 235
        Top = 3
        Width = 101
        Height = 14
        Caption = 'Inscri'#231#227'o estadual'
      end
      object lb8: TLabel
        Left = 3
        Top = 3
        Width = 15
        Height = 14
        Caption = 'RG'
      end
      object lb10: TLabel
        Left = 128
        Top = 3
        Width = 57
        Height = 14
        Caption = 'Org'#227'o exp.'
      end
      object lb7: TLabel
        Left = 127
        Top = 45
        Width = 27
        Height = 14
        Caption = 'CNAE'
      end
      object lb9: TLabel
        Left = 3
        Top = 173
        Width = 35
        Height = 14
        Caption = 'E-mail'
      end
      object lb11: TLabel
        Left = 234
        Top = 45
        Width = 48
        Height = 14
        Caption = 'Telefone'
      end
      object lb12: TLabel
        Left = 360
        Top = 45
        Width = 39
        Height = 14
        Caption = 'Celular'
      end
      object lb13: TLabel
        Left = 474
        Top = 45
        Width = 18
        Height = 14
        Caption = 'Fax'
      end
      object cbEstadoCivil: TComboBoxLuka
        Left = 3
        Top = 59
        Width = 119
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = 16645629
        Enabled = False
        TabOrder = 5
        Items.Strings = (
          'Solteiro(a)'
          'Casado(a)'
          'Divorciado(a)'
          'Viuvo(a)')
        Valores.Strings = (
          'S'
          'C'
          'D'
          'V')
        AsInt = 0
      end
      object rgSexo: TRadioGroupLuka
        Left = 386
        Top = -1
        Width = 172
        Height = 41
        Caption = '  Sexo '
        Columns = 2
        Items.Strings = (
          'Masculino'
          'Feminino')
        TabOrder = 3
        TabStop = True
        Valores.Strings = (
          'M'
          'F')
      end
      object eDataNascimento: TEditLukaData
        Left = 564
        Top = 17
        Width = 93
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        ReadOnly = True
        TabOrder = 4
        Text = '  /  /    '
      end
      object eInscricaoEstadual: TEditLuka
        Left = 235
        Top = 17
        Width = 146
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 12
        ReadOnly = True
        TabOrder = 2
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eOrgaoExpedidor: TEditLuka
        Left = 128
        Top = 17
        Width = 102
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 1
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eCNAE: TMaskEdit
        Left = 127
        Top = 59
        Width = 102
        Height = 22
        EditMask = '99.99-9-99;1;_'
        MaxLength = 10
        ReadOnly = True
        TabOrder = 6
        Text = '  .  - -  '
      end
      object eRG: TEditLuka
        Left = 3
        Top = 17
        Width = 120
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 0
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      inline FrEndereco: TFrEndereco
        Left = 0
        Top = 87
        Width = 670
        Height = 85
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 10
        TabStop = True
        ExplicitTop = 87
        ExplicitWidth = 670
        ExplicitHeight = 85
        inherited lb8: TLabel
          Width = 61
          Height = 14
          ExplicitWidth = 61
          ExplicitHeight = 14
        end
        inherited lb9: TLabel
          Width = 76
          Height = 14
          ExplicitWidth = 76
          ExplicitHeight = 14
        end
        inherited lb10: TLabel
          Top = 43
          Width = 20
          Height = 14
          ExplicitTop = 43
          ExplicitWidth = 20
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Top = 43
          Width = 107
          Height = 14
          ExplicitTop = 43
          ExplicitWidth = 107
          ExplicitHeight = 14
        end
        inherited lb2: TLabel
          Width = 43
          Height = 14
          ExplicitWidth = 43
          ExplicitHeight = 14
        end
        inherited eCEP: TEditCEPLuka
          Top = 58
          Width = 72
          Height = 22
          ReadOnly = True
          ExplicitTop = 58
          ExplicitWidth = 72
          ExplicitHeight = 22
        end
        inherited eLogradouro: TEditLuka
          Height = 22
          ReadOnly = True
          ExplicitHeight = 22
        end
        inherited eComplemento: TEditLuka
          Height = 22
          ReadOnly = True
          ExplicitHeight = 22
        end
        inherited FrBairro: TFrBairros
          inherited CkAspas: TCheckBox
            TabOrder = 3
          end
          inherited CkFiltroDuplo: TCheckBox
            TabOrder = 5
          end
          inherited CkPesquisaNumerica: TCheckBox
            TabOrder = 7
          end
          inherited CkMultiSelecao: TCheckBox
            TabOrder = 4
          end
          inherited PnTitulos: TPanel
            inherited lbNomePesquisa: TLabel
              Height = 15
            end
          end
          inherited pnPesquisa: TPanel
            TabOrder = 2
            inherited sbPesquisa: TSpeedButton
              Left = 1
              ExplicitLeft = 1
            end
          end
          inherited ckSomenteAtivos: TCheckBox
            TabOrder = 6
          end
        end
        inherited ckValidarComplemento: TCheckBox
          Width = 137
          Checked = True
          State = cbChecked
          ExplicitWidth = 137
        end
        inherited ckValidarBairro: TCheckBox
          Checked = True
          State = cbChecked
          TabOrder = 9
        end
        inherited ckValidarCEP: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited ckValidarEndereco: TCheckBox
          Width = 127
          Checked = True
          State = cbChecked
          TabOrder = 10
          ExplicitWidth = 127
        end
        inherited ePontoReferencia: TEditLuka
          Top = 58
          Height = 22
          ReadOnly = True
          ExplicitTop = 58
          ExplicitHeight = 22
        end
        inherited ckValidarPontoReferencia: TCheckBox
          Width = 180
          TabOrder = 11
          ExplicitWidth = 180
        end
        inherited eNumero: TEditLuka
          Width = 72
          Height = 22
          ReadOnly = True
          ExplicitWidth = 72
          ExplicitHeight = 22
        end
        inherited ckValidarNumero: TCheckBox
          Checked = True
          State = cbChecked
          TabOrder = 8
        end
      end
      object eEmail: TEditLuka
        Left = 3
        Top = 187
        Width = 293
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 11
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eTelefone: TEditTelefoneLuka
        Left = 234
        Top = 59
        Width = 119
        Height = 22
        EditMask = '(99) 9999-9999;1; '
        MaxLength = 14
        ReadOnly = True
        TabOrder = 7
        Text = '(  )     -    '
      end
      object eCelular: TEditTelefoneLuka
        Left = 360
        Top = 59
        Width = 109
        Height = 22
        EditMask = '(99) 99999-9999;1; '
        MaxLength = 15
        ReadOnly = True
        TabOrder = 8
        Text = '(  )      -    '
        Celular = True
      end
      object eFax: TEditTelefoneLuka
        Left = 474
        Top = 59
        Width = 121
        Height = 22
        EditMask = '(99) 9999-9999;1; '
        MaxLength = 14
        ReadOnly = True
        TabOrder = 9
        Text = '(  )     -    '
      end
    end
    object tsContatos: TTabSheet
      Caption = 'Telefones'
      ImageIndex = 2
      ExplicitHeight = 309
      inline FrTelefones: TFrCadastrosTelefones
        Left = 0
        Top = 0
        Width = 783
        Height = 221
        Align = alClient
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitWidth = 783
        ExplicitHeight = 309
        inherited lb14: TLabel
          Width = 24
          Height = 14
          ExplicitWidth = 24
          ExplicitHeight = 14
        end
        inherited lb11: TLabel
          Width = 48
          Height = 14
          ExplicitWidth = 48
          ExplicitHeight = 14
        end
        inherited lb9: TLabel
          Width = 63
          Height = 14
          ExplicitWidth = 63
          ExplicitHeight = 14
        end
        inherited lb3: TLabel
          Width = 35
          Height = 14
          ExplicitWidth = 35
          ExplicitHeight = 14
        end
        inherited sgTelefones: TGridLuka
          Top = -33
          Width = 783
          Height = 254
          ExplicitTop = -29
          ExplicitWidth = 783
          ExplicitHeight = 254
        end
        inherited eTelefone: TEditTelefoneLuka
          Height = 22
          ExplicitHeight = 22
        end
        inherited eRamal: TEditLuka
          Height = 22
          ExplicitHeight = 22
        end
      end
    end
    object tsEnderecos: TTabSheet
      Caption = 'Endere'#231'os'
      ImageIndex = 1
      ExplicitHeight = 309
      inline FrDiversosEnderecos: TFrDiversosEnderecos
        Left = 0
        Top = 0
        Width = 783
        Height = 221
        Align = alClient
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        ExplicitWidth = 783
        ExplicitHeight = 309
        inherited lb14: TLabel
          Width = 61
          Height = 14
          ExplicitWidth = 61
          ExplicitHeight = 14
        end
        inherited lb15: TLabel
          Width = 76
          Height = 14
          ExplicitWidth = 76
          ExplicitHeight = 14
        end
        inherited lb16: TLabel
          Top = 44
          Width = 20
          Height = 14
          ExplicitTop = 44
          ExplicitWidth = 20
          ExplicitHeight = 14
        end
        inherited lb18: TLabel
          Top = 44
          Width = 107
          Height = 14
          ExplicitTop = 44
          ExplicitWidth = 107
          ExplicitHeight = 14
        end
        inherited lb19: TLabel
          Width = 68
          Height = 14
          ExplicitWidth = 68
          ExplicitHeight = 14
        end
        inherited lb2: TLabel
          Width = 43
          Height = 14
          ExplicitWidth = 43
          ExplicitHeight = 14
        end
        inherited lb6: TLabel
          Top = 44
          Width = 101
          Height = 14
          ExplicitTop = 44
          ExplicitWidth = 101
          ExplicitHeight = 14
        end
        inherited sgOutrosEnderecos: TGridLuka
          Top = 14
          Width = 783
          Height = 207
          ExplicitTop = 102
          ExplicitWidth = 783
          ExplicitHeight = 207
        end
        inherited eCEP: TEditCEPLuka
          Top = 59
          Height = 22
          ReadOnly = True
          ExplicitTop = 59
          ExplicitHeight = 22
        end
        inherited eLogradouro: TEditLuka
          Top = 17
          Height = 22
          ReadOnly = True
          ExplicitTop = 17
          ExplicitHeight = 22
        end
        inherited eComplemento: TEditLuka
          Top = 17
          Height = 22
          ReadOnly = True
          ExplicitTop = 17
          ExplicitHeight = 22
        end
        inherited FrBairro: TFrBairros
          Top = 42
          ExplicitTop = 42
          inherited PnTitulos: TPanel
            inherited lbNomePesquisa: TLabel
              Width = 33
              Height = 15
              ExplicitWidth = 33
              ExplicitHeight = 14
            end
            inherited pnSuprimir: TPanel
              Left = 223
              ExplicitLeft = 223
            end
          end
        end
        inherited ePontoReferencia: TEditLuka
          Top = 59
          Height = 22
          ReadOnly = True
          ExplicitTop = 59
          ExplicitHeight = 22
        end
        inherited cbTipoEndereco: TComboBoxLuka
          Top = 17
          ExplicitTop = 17
        end
        inherited eNumero: TEditLuka
          Top = 17
          Height = 22
          ReadOnly = True
          ExplicitTop = 17
          ExplicitHeight = 22
        end
        inherited eInscricaoEstadual: TEditLuka
          Top = 59
          Height = 22
          ReadOnly = True
          ExplicitTop = 59
          ExplicitHeight = 22
        end
      end
    end
    object tsObservacoes: TTabSheet
      Caption = 'Observa'#231#245'es'
      ImageIndex = 3
      ExplicitHeight = 309
      object eObservacoes: TMemo
        Left = 0
        Top = 0
        Width = 783
        Height = 221
        Align = alClient
        ReadOnly = True
        TabOrder = 0
        ExplicitHeight = 309
      end
    end
  end
  object pPanel: TPanel
    Left = 0
    Top = 0
    Width = 791
    Height = 89
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Label1: TLabel
      Left = 7
      Top = 1
      Width = 37
      Height = 14
      Caption = 'C'#243'digo'
    end
    object Label2: TLabel
      Left = 653
      Top = 42
      Width = 76
      Height = 14
      Caption = 'Data cadastro'
    end
    object lbCPF_CNPJ: TLabel
      Left = 88
      Top = 1
      Width = 52
      Height = 14
      Caption = 'CPF / CNPJ'
    end
    object lbNomeFantasiaApelido: TLabel
      Left = 3
      Top = 42
      Width = 81
      Height = 14
      Caption = 'Nome fantasia'
    end
    object lbRazaoSocialNome: TLabel
      Left = 204
      Top = 1
      Width = 69
      Height = 14
      Caption = 'Raz'#227'o social'
    end
    object eCPF_CNPJ: TEditCPF_CNPJ_Luka
      Left = 88
      Top = 15
      Width = 110
      Height = 22
      ReadOnly = True
      TabOrder = 0
      Text = ''
      Tipo = []
    end
    object eDataCadastro: TEditLukaData
      Left = 636
      Top = 57
      Width = 93
      Height = 22
      Hint = 
        'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
        'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
      TabStop = False
      EditMask = '99/99/9999;1; '
      MaxLength = 10
      ReadOnly = True
      TabOrder = 1
      Text = '  /  /    '
    end
    object eID: TEditLuka
      Left = 3
      Top = 15
      Width = 79
      Height = 22
      Alignment = taRightJustify
      CharCase = ecUpperCase
      ReadOnly = True
      TabOrder = 2
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 0
      NaoAceitarEspaco = False
    end
    object eNomeFantasia: TEditLuka
      Left = 3
      Top = 57
      Width = 552
      Height = 22
      CharCase = ecUpperCase
      ReadOnly = True
      TabOrder = 3
      TipoCampo = tcTexto
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 0
      NaoAceitarEspaco = False
    end
    object eRazaoSocial: TEditLuka
      Left = 204
      Top = 15
      Width = 350
      Height = 22
      CharCase = ecUpperCase
      ReadOnly = True
      TabOrder = 4
      TipoCampo = tcTexto
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 0
      NaoAceitarEspaco = False
    end
    object rgTipoPessoa: TRadioGroupLuka
      Left = 604
      Top = 3
      Width = 125
      Height = 37
      Caption = '  Tipo pessoa '
      Columns = 2
      Enabled = False
      Items.Strings = (
        'F'#237'sica'
        'Jur'#237'dica')
      TabOrder = 5
      Valores.Strings = (
        'F'
        'J')
    end
    object ckAtivo: TCheckBoxLuka
      Left = 739
      Top = 9
      Width = 46
      Height = 17
      Caption = 'Ativo'
      Enabled = False
      TabOrder = 6
      CheckedStr = 'N'
    end
  end
end
