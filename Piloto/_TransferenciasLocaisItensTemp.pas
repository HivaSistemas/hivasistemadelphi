unit _TransferenciasLocaisItensTemp;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, System.StrUtils;

{$M+}
type
  RecTransferenciasLocaisItensTemp = record
    TransferenciaLocalId: Integer;
    ItemId: Integer;
    LocalDestinoId: Integer;
    NomeLocal: string;
    ProdutoId: Integer;
    NomeProduto: string;
    MarcaId: Integer;
    NomeMarca: string;
    Unidade: string;
    Quantidade: Double;
    Lote: string;
  end;

  TTransferenciasLocaisItensTemp = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordTransferenciasLocaisItensTemp: RecTransferenciasLocaisItensTemp;
  end;

function BuscarTransferenciasLocaisItensTemp(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTransferenciasLocaisItensTemp>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TTransferenciasLocaisItensTemp }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where TRANSFERENCIA_LOCAL_ID = :P1 ' +
      'order by ' +
      '  PRODUTO_ID, ' +
      '  ITEM_ID '
    );
end;

constructor TTransferenciasLocaisItensTemp.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'TRANSFERENCIAS_LOC_ITENS_TMP');

  FSql :=
    'select ' +
    '  ITE.TRANSFERENCIA_LOCAL_ID, ' +
    '  ITE.ITEM_ID, ' +
    '  ITE.LOCAL_DESTINO_ID, ' +
    '  LOC.NOME as NOME_LOCAL, ' +
    '  ITE.PRODUTO_ID, ' +
    '  PRO.NOME as NOME_PRODUTO, ' +
    '  PRO.MARCA_ID, ' +
    '  MAR.NOME as NOME_MARCA, ' +
    '  PRO.UNIDADE_VENDA as UNIDADE, ' +
    '  ITE.QUANTIDADE, ' +
    '  ITE.LOTE ' +
    'from ' +
    '  TRANSFERENCIAS_LOC_ITENS_TMP ITE ' +

    'inner join PRODUTOS PRO ' +
    'on ITE.PRODUTO_ID = PRO.PRODUTO_ID ' +

    'inner join MARCAS MAR ' +
    'on PRO.MARCA_ID = MAR.MARCA_ID ' +

    'inner join LOCAIS_PRODUTOS LOC ' +
    'on ITE.LOCAL_DESTINO_ID = LOC.LOCAL_ID ';

  setFiltros(getFiltros);

  AddColuna('TRANSFERENCIA_LOCAL_ID', True);
  AddColuna('LOCAL_DESTINO_ID', True);
  AddColuna('ITEM_ID', True);
  AddColuna('LOTE', True);
  AddColuna('PRODUTO_ID');
  AddColunaSL('NOME_PRODUTO');
  AddColunaSL('MARCA_ID');
  AddColunaSL('NOME_MARCA');
  AddColunaSL('UNIDADE');
  AddColuna('QUANTIDADE');
  AddColunaSL('NOME_LOCAL');
end;

function TTransferenciasLocaisItensTemp.getRecordTransferenciasLocaisItensTemp: RecTransferenciasLocaisItensTemp;
begin
  Result.TransferenciaLocalId := getInt('TRANSFERENCIA_LOCAL_ID', True);
  Result.LocalDestinoId       := getInt('LOCAL_DESTINO_ID', True);
  Result.ItemId               := getInt('ITEM_ID', True);
  Result.Lote                 := getString('LOTE', True);
  Result.ProdutoId            := getInt('PRODUTO_ID');
  Result.NomeProduto          := getString('NOME_PRODUTO');
  Result.MarcaId              := getInt('MARCA_ID');
  Result.NomeMarca            := getString('NOME_MARCA');
  Result.quantidade           := getDouble('QUANTIDADE');
  Result.Unidade              := getString('UNIDADE');
  Result.NomeLocal            := getString('NOME_LOCAL');
end;

function BuscarTransferenciasLocaisItensTemp(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTransferenciasLocaisItensTemp>;
var
  i: Integer;
  t: TTransferenciasLocaisItensTemp;
begin
  Result := nil;
  t := TTransferenciasLocaisItensTemp.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordTransferenciasLocaisItensTemp;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
