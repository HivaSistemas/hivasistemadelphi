unit Cadastro.ValidacoesCadastroCliente;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastro, Vcl.ComCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, _Sessao, _Autorizacoes, _RecordsCadastros, System.Math,
  System.ImageList, Vcl.ImgList, Vcl.StdCtrls, _RecordsEspeciais;

type
  TNo = record
    No: TTreeNode;
    Autorizado: Boolean;
    Id: string;
  end;

  TFormCadastroValidacoesCadastroCliente = class(TFormHerancaCadastro)
    tvCampos: TTreeView;
    ilAutorizacoes: TImageList;
    stFormasPagamento: TStaticText;
    procedure FormCreate(Sender: TObject);
    procedure tvCamposDblClick(Sender: TObject);
    procedure tvCamposKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sbGravarClick(Sender: TObject);
  private
    FNosCampos: array of TNo;
    procedure PreencherCampos(campos: TArray<RecCamposClientes>);
    procedure AddNo(pCaminho: array of string; const pId: string = '');
    procedure PreencherCamposObrigatorios();
  public
    { Public declarations }
  end;

implementation

uses
  _Biblioteca;

const
  coSelecionado    = 0;
  coNaoSelecionado = 1;

{$R *.dfm}

procedure TFormCadastroValidacoesCadastroCliente.FormCreate(Sender: TObject);
var
  campos: TArray<RecCamposClientes>;
begin
  inherited;
  campos := _Autorizacoes.CamposClientes(Sessao.getConexaoBanco);
  PreencherCampos(campos);

  PreencherCamposObrigatorios();
  sbGravar.Enabled := true;
end;

procedure TFormCadastroValidacoesCadastroCliente.PreencherCamposObrigatorios();
var
  i: Integer;
  j: Integer;
  vObrigatorios: TArray<RecCamposObrigatoriosClientes>;
begin
  vObrigatorios := _Autorizacoes.BuscarCamposClienteObrigatorios(Sessao.getConexaoBanco);
  for i := Low(vObrigatorios) to High(vObrigatorios) do begin
    for j := Low(FNosCampos) to High(FNosCampos) do begin
      if IntToStr(vObrigatorios[i].id) = FNosCampos[j].Id then begin
        FNosCampos[j].Autorizado := True;

        FNosCampos[j].No.ImageIndex    := coSelecionado;
        FNosCampos[j].No.SelectedIndex := coSelecionado;
      end;
    end;
  end;
end;

procedure TFormCadastroValidacoesCadastroCliente.sbGravarClick(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  vCamposObrigatorios: array of string;
begin
  vCamposObrigatorios := nil;
  for i := Low(FNosCampos) to High(FNosCampos) do begin
    if (not FNosCampos[i].Autorizado) or (FNosCampos[i].Id = '') then
      Continue;

    SetLength(vCamposObrigatorios, Length(vCamposObrigatorios) + 1);
    vCamposObrigatorios[High(vCamposObrigatorios)] := FNosCampos[i].Id;
  end;

  vRetBanco :=
    _Autorizacoes.AtualizarCamposObrigatorios(
      Sessao.getConexaoBanco,
      vCamposObrigatorios
    );

  if vRetBanco.TeveErro then begin
    Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  _Biblioteca.Informar(coAlteracaoRegistroSucesso);
end;

procedure TFormCadastroValidacoesCadastroCliente.PreencherCampos(campos: TArray<RecCamposClientes>);
var
  i: Integer;
  tipoCliente: string;
begin
  for I := Low(campos) to High(campos) do begin
    if campos[i].tipo_cliente = 'F' then
      tipoCliente := 'Pessoa F�sica';

    if campos[i].tipo_cliente = 'J' then
      tipoCliente := 'Pessoa Jur�dica';

    if campos[i].tipo_cliente = 'A' then
      tipoCliente := 'Ambas';

    AddNo([campos[i].descricao + ' (' + tipoCliente + ')'], IntToStr(campos[i].id));
  end;
end;

procedure TFormCadastroValidacoesCadastroCliente.tvCamposDblClick(
  Sender: TObject);
begin
  inherited;

  if tvCampos.Selected.HasChildren then
    Exit;

  tvCampos.Selected.ImageIndex := IfThen(tvCampos.Selected.ImageIndex = coNaoSelecionado, coSelecionado, coNaoSelecionado);
  tvCampos.Selected.SelectedIndex := tvCampos.Selected.ImageIndex;

  FNosCampos[tvCampos.Selected.AbsoluteIndex].Autorizado := ( tvCampos.Selected.ImageIndex = coSelecionado )
end;

procedure TFormCadastroValidacoesCadastroCliente.tvCamposKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_SPACE then
    tvCamposDblClick(Sender);
end;

procedure TFormCadastroValidacoesCadastroCliente.AddNo(pCaminho: array of string; const pId: string = '');
var
  i: Integer;
  vPai: string;
  vNo: TTreeNode;
begin
  vNo := nil;

  vNo := tvCampos.Items.Add(nil, pCaminho[High(pCaminho)]);

  if pId = '' then begin
    vNo.ImageIndex := -1;
    vNo.SelectedIndex := -1;
  end
  else begin
    vNo.ImageIndex := coNaoSelecionado;
    vNo.SelectedIndex := coNaoSelecionado;
  end;

  SetLength(FNosCampos, Length(FNosCampos) + 1);

  FNosCampos[High(FNosCampos)].No  := vNo;
  FNosCampos[High(FNosCampos)].Id  := pId;
end;

end.
