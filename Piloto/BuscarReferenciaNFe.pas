unit BuscarReferenciaNFe;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.StdCtrls, _Biblioteca,
  Vcl.Mask, Vcl.ExtCtrls, RadioGroupLuka, Vcl.Buttons;

type
  RecReferenciaNFe = record
    TipoDocumento: string;
    ChaveNFe: string;
  end;

  TFormBuscarReferenciaNFe = class(TFormHerancaFinalizar)
    rgTipoReferencia: TRadioGroupLuka;
    lbl28: TLabel;
    eChaveAcessoNFe: TMaskEdit;
    procedure FormShow(Sender: TObject);
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function getDados: TRetornoTelaFinalizar<RecReferenciaNFe>;

implementation

{$R *.dfm}

{ TFormBuscarReferenciaNFe }

function getDados: TRetornoTelaFinalizar<RecReferenciaNFe>;
var
  vForm: TFormBuscarReferenciaNFe;
begin
  vForm := TFormBuscarReferenciaNFe.Create(nil);

  if Result.Ok(vForm.ShowModal) then begin
    Result.Dados.TipoDocumento := vForm.rgTipoReferencia.GetValor;
    Result.Dados.ChaveNFe      := vForm.eChaveAcessoNFe.Text;
  end;
end;

{ TFormBuscarReferenciaNFe }

procedure TFormBuscarReferenciaNFe.FormShow(Sender: TObject);
begin
  inherited;
  rgTipoReferencia.ItemIndex := 0;
  SetarFoco(rgTipoReferencia);
end;

procedure TFormBuscarReferenciaNFe.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if rgTipoReferencia.GetValor = '' then begin
    _Biblioteca.Exclamar('O tipo de documento n�o foi informado corretamente!');
    SetarFoco(rgTipoReferencia);
    Abort;
  end;

  if Length(_Biblioteca.RetornaNumeros(eChaveAcessoNFe.Text)) <> 44 then begin
    _Biblioteca.Exclamar('A chave informada � inv�lida!');
    SetarFoco(eChaveAcessoNFe);
    Abort;
  end;
end;

end.
