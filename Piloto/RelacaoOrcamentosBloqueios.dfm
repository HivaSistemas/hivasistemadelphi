inherited FormRelacaoBloqueiosOrcamentos: TFormRelacaoBloqueiosOrcamentos
  Caption = 'Libera'#231#227'o de pdidos bloqueados'
  ClientHeight = 542
  OnShow = FormShow
  ExplicitHeight = 571
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Width = 129
    Height = 542
    ExplicitWidth = 129
    ExplicitHeight = 542
    inherited sbCarregar: TSpeedButton
      Left = 7
      Top = 9
      ExplicitLeft = 7
      ExplicitTop = 9
    end
    inherited sbImprimir: TSpeedButton
      Left = 7
      Top = 384
      Visible = False
      ExplicitLeft = 7
      ExplicitTop = 384
    end
    object miVoltarOrcamentoFocado: TSpeedButton [2]
      Left = 3
      Top = 83
      Width = 117
      Height = 35
      Caption = 'Voltar pedido p/ orc.'
      Flat = True
      NumGlyphs = 2
      OnClick = miVoltarOrcamentoFocadoClick
    end
    object miDesbloquearSelecionados: TSpeedButton [3]
      Left = 0
      Top = 155
      Width = 117
      Height = 35
      Caption = 'Liberar pedido '
      Flat = True
      NumGlyphs = 2
      OnClick = miDesbloquearSelecionadosClick
    end
    inherited ckOmitirFiltros: TCheckBoxLuka
      Top = 425
      ExplicitTop = 425
    end
  end
  inherited pcDados: TPageControl
    Left = 129
    Width = 885
    Height = 542
    ExplicitLeft = 129
    ExplicitWidth = 885
    ExplicitHeight = 542
    inherited tsFiltros: TTabSheet
      ExplicitWidth = 877
      ExplicitHeight = 513
      inline FrClientes: TFrClientes
        Left = 1
        Top = 84
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 84
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 45
            ExplicitWidth = 45
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
      inline FrEmpresas: TFrEmpresas
        Left = 1
        Top = 0
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 1
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 53
            ExplicitWidth = 53
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
      inline FrDataCadastro: TFrDataInicialFinal
        Left = 424
        Top = 0
        Width = 217
        Height = 41
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 424
        ExplicitWidth = 217
        inherited Label1: TLabel
          Width = 93
          Height = 14
          ExplicitWidth = 93
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrCondicoesPagamentos: TFrCondicoesPagamento
        Left = 0
        Top = 171
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitTop = 171
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 138
            ExplicitWidth = 138
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
      object gbStatusBloqueio: TGroupBoxLuka
        Left = 424
        Top = 44
        Width = 134
        Height = 69
        Caption = '  Status  '
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        OpcaoMarcarDesmarcar = False
        object ckBloqueados: TCheckBox
          Left = 12
          Top = 17
          Width = 88
          Height = 17
          Caption = 'Bloqueados'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object ckDesbloqueados: TCheckBox
          Left = 12
          Top = 40
          Width = 103
          Height = 17
          Caption = 'Desbloqueados'
          TabOrder = 1
        end
      end
      inline FrCodigoOrcamento: TFrameInteiros
        Left = 426
        Top = 133
        Width = 119
        Height = 84
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        TabStop = True
        ExplicitLeft = 426
        ExplicitTop = 133
        ExplicitWidth = 119
        inherited sgInteiros: TGridLuka
          Width = 119
          ExplicitWidth = 113
        end
        inherited Panel1: TPanel
          Width = 119
          Caption = 'C'#243'digo do or'#231'amento'
        end
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitWidth = 877
      ExplicitHeight = 513
      object spSeparador: TSplitter
        Left = 0
        Top = 193
        Width = 877
        Height = 6
        Cursor = crVSplit
        Align = alTop
        ResizeStyle = rsUpdate
        ExplicitWidth = 801
      end
      object sgOrcamentos: TGridLuka
        Left = 0
        Top = 16
        Width = 877
        Height = 177
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alTop
        ColCount = 11
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
        PopupMenu = pmOpcoes
        TabOrder = 0
        OnClick = sgOrcamentosClick
        OnDblClick = sgOrcamentosDblClick
        OnDrawCell = sgOrcamentosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Orcamento'
          'Data cad.'
          'Cliente'
          'Vendedor'
          'Valor'
          '% Desc.'
          'Desconto'
          '% Lucro bruto'
          '% Lucro l'#237'quido'
          'Cond. pagamento'
          'Empresa')
        OnGetCellColor = sgOrcamentosGetCellColor
        Grid3D = False
        RealColCount = 15
        OrdenarOnClick = True
        Indicador = True
        AtivarPopUpSelecao = False
        ExplicitLeft = 2
        ColWidths = (
          69
          83
          176
          135
          75
          71
          72
          81
          90
          156
          157)
      end
      object st2: TStaticText
        Left = 0
        Top = 0
        Width = 877
        Height = 16
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Or'#231'amentos com bloqueios'
        Color = 15395562
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        Transparent = False
      end
      object st1: TStaticText
        Left = 0
        Top = 199
        Width = 877
        Height = 16
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Descri'#231#227'o dos bloqueios'
        Color = 15395562
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        Transparent = False
      end
      object sgItensBloqueios: TGridLuka
        Left = 0
        Top = 215
        Width = 877
        Height = 298
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 4
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect, goFixedRowClick]
        PopupMenu = pmRotinas
        TabOrder = 3
        OnDblClick = sgItensBloqueiosDblClick
        OnDrawCell = sgItensBloqueiosDrawCell
        OnKeyDown = sgItensBloqueiosKeyDown
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Sel.?'
          'Descri'#231#227'o'
          'Data libera'#231#227'o'
          'Usuario libera'#231#227'o')
        OnGetCellPicture = sgItensBloqueiosGetCellPicture
        Grid3D = False
        RealColCount = 15
        OrdenarOnClick = True
        AtivarPopUpSelecao = False
        ExplicitLeft = 2
        ColWidths = (
          36
          483
          93
          166)
      end
    end
  end
  object pmRotinas: TPopupMenu
    Left = 859
    Top = 459
  end
  object pmOpcoes: TPopupMenu
    Left = 843
    Top = 123
  end
end
