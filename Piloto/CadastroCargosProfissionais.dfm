inherited FormCadastroCargosProfissionais: TFormCadastroCargosProfissionais
  Caption = 'Cadastro de cargos de profissionais'
  ClientHeight = 271
  ClientWidth = 451
  ExplicitWidth = 457
  ExplicitHeight = 300
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label1: TLabel
    Left = 125
    Top = 28
    ExplicitLeft = 125
    ExplicitTop = 28
  end
  object lbllb1: TLabel [1]
    Left = 125
    Top = 82
    Width = 32
    Height = 14
    Caption = 'Nome'
  end
  inherited pnOpcoes: TPanel
    Height = 271
    TabOrder = 3
    ExplicitHeight = 177
  end
  inherited eID: TEditLuka
    Left = 125
    Top = 42
    MaxLength = 3
    TabOrder = 0
    ExplicitLeft = 125
    ExplicitTop = 42
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 125
    ExplicitLeft = 125
  end
  object eNome: TEditLuka
    Left = 125
    Top = 96
    Width = 255
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 30
    TabOrder = 1
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
