unit Cadastros;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Cadastros,
  FrameDiversosContatos, FrameDiversosEnderecos, EditTelefoneLuka, _RecordsCadastros, Logs,
  _FrameHerancaPrincipal, FrameEndereco, EditLukaData, ComboBoxLuka, _Sessao, Pesquisa.Cadastros,
  Vcl.ComCtrls, Vcl.ExtCtrls, RadioGroupLuka, Vcl.Mask, EditCpfCnpjLuka, _Biblioteca,
  EditLuka, Vcl.Buttons, _DiversosContatos, _DiversosEnderecos, _RecordsEspeciais,
  Vcl.Menus, CheckBoxLuka, FrameCadastrosTelefones, _CadastrosTelefones;

type
  TFormCadastros = class(TFormHerancaCadastroCodigo)
    lbRazaoSocialNome: TLabel;
    lbNomeFantasiaApelido: TLabel;
    lbCPF_CNPJ: TLabel;
    eRazaoSocial: TEditLuka;
    eNomeFantasia: TEditLuka;
    eCPF_CNPJ: TEditCPF_CNPJ_Luka;
    rgTipoPessoa: TRadioGroupLuka;
    pcDados: TPageControl;
    tsPrincipais: TTabSheet;
    lb4: TLabel;
    lbDataNascimento: TLabel;
    lb8: TLabel;
    lb10: TLabel;
    lb9: TLabel;
    cbEstadoCivil: TComboBoxLuka;
    rgSexo: TRadioGroupLuka;
    eDataNascimento: TEditLukaData;
    eOrgaoExpedidor: TEditLuka;
    eRG: TEditLuka;
    FrEndereco: TFrEndereco;
    eEmail: TEditLuka;
    tsEnderecos: TTabSheet;
    FrDiversosEnderecos: TFrDiversosEnderecos;
    tsContatos: TTabSheet;
    tsObservacoes: TTabSheet;
    eObservacoes: TMemo;
    eDataCadastro: TEditLukaData;
    Label2: TLabel;
    pmLogs: TPopupMenu;
    miLogsCadastro: TMenuItem;
    miHeranca: TMenuItem;
    FrTelefones: TFrCadastrosTelefones;
    lb11: TLabel;
    lb12: TLabel;
    lb13: TLabel;
    eTelefone: TEditTelefoneLuka;
    eCelular: TEditTelefoneLuka;
    eFax: TEditTelefoneLuka;
    eInscricaoEstadual: TEditLuka;
    lb6: TLabel;
    lb7: TLabel;
    eCNAE: TMaskEdit;
    procedure rgTipoPessoaClick(Sender: TObject);
    procedure sbPesquisarCadastrosClick(Sender: TObject);
    procedure eCPF_CNPJExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure miLogsCadastroClick(Sender: TObject);
    procedure miHerancaClick(Sender: TObject);
    procedure AjustarCampos;
  private
    procedure PreencherRegistro(
      pCadastro: RecCadastros;
      pTelefones: TArray<RecCadastrosTelefones>;
      pEnderecos: TArray<RecDiversosEnderecos>
    );
  public
    { Public declarations }
  protected
    FCadastro: RecCadastros;
    FPesquisouCadastro: Boolean;

    FTelefones: TArray<RecCadastrosTelefones>;
    FEnderecos: TArray<RecDiversosEnderecos>;

    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Modo(pEditando: Boolean); override;
    procedure ExibirLogs; override;
  end;

implementation

{$R *.dfm}

{ TFormCadastros }

uses
  CadastroMotoristas, CadastroClientes, CadastroFornecedor, CadastroTransportadoras, CadastroProfissionais;

procedure TFormCadastros.BuscarRegistro;
var
  vCadastros: TArray<RecCadastros>;
  vTelefones: TArray<RecCadastrosTelefones>;
  vEnderecos: TArray<RecDiversosEnderecos>;
begin
  vCadastros := _Cadastros.BuscarCadastros(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if vCadastros = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    eID.SetFocus;
    Exit;
  end;

  vTelefones := _CadastrosTelefones.BuscarCadastrosTelefones(Sessao.getConexaoBanco, 0, [eID.AsInt]);
  vEnderecos := _DiversosEnderecos.BuscarDiversosEnderecos(Sessao.getConexaoBanco, 0, [eID.AsInt], False);

  inherited;
  PreencherRegistro(vCadastros[0], vTelefones, vEnderecos);
end;

procedure TFormCadastros.eCPF_CNPJExit(Sender: TObject);
var
  vCadastro: TArray<RecCadastros>;
  vInvalido: Boolean;
  cpfCnpjNumeros: string;
begin
  inherited;
  if eID.Text <> '' then
    Exit;

  cpfCnpjNumeros := RetornaNumeros(eCPF_CNPJ.Text);
  //eCPF_CNPJ.Text := cpfCnpjNumeros;

  if Length(RetornaNumeros(cpfCnpjNumeros)) = 11 then
    rgTipoPessoa.ItemIndex := 0
  else begin
    if Length(RetornaNumeros(cpfCnpjNumeros)) = 14 then
      rgTipoPessoa.ItemIndex := 1
    else
      rgTipoPessoa.ItemIndex := -1;
  end;

  vInvalido := rgTipoPessoa.ItemIndex < 0;

  if not vInvalido then begin
    //AjustaCampos;
    if eCPF_CNPJ.getCPF_CNPJOk then begin
      vCadastro := _Cadastros.BuscarCadastros(Sessao.getConexaoBanco, 4, [eCPF_CNPJ.Text, eID.AsInt]);

      if vCadastro <> nil then begin
        eID.Text := NFormat(vCadastro[0].cadastro_id);
        BuscarRegistro;
      end
      else
        vInvalido := False;
    end
    else
      vInvalido := True;
  end;

  if vInvalido and FEditandoHeranca then begin
    _Biblioteca.Exclamar('Informe um CPF/CNPJ v�lido!');
    rgTipoPessoa.ItemIndex := -1;
    SetarFoco(eCPF_CNPJ);
    eCPF_CNPJ.EditMask := '';
    eCPF_CNPJ.Clear;
  end;
end;

procedure TFormCadastros.ExibirLogs;
var
  pt: TPoint;
begin
  inherited;
  GetCursorPos(pt);
  pmLogs.Popup( pt.X, pt.Y);
end;

procedure TFormCadastros.AjustarCampos;
begin
  lbCPF_CNPJ.Caption := IIfStr(rgTipoPessoa.ItemIndex = 0, 'CPF', 'CNPJ');

  _Biblioteca.Habilitar([eCNAE], rgTipoPessoa.ItemIndex = 1, True);
  _Biblioteca.Habilitar([eRG, rgSexo, eOrgaoExpedidor, cbEstadoCivil], rgTipoPessoa.ItemIndex = 0, True);

  lbRazaoSocialNome.Caption := IIfStr( rgTipoPessoa.GetValor = 'F', 'Nome', 'Raz�o social' );
  lbNomeFantasiaApelido.Caption := IIfStr( rgTipoPessoa.GetValor = 'F', 'Apelido', 'Nome fantasia' );
  lbDataNascimento.Caption := IIfStr( rgTipoPessoa.GetValor = 'F', 'Data nascimento', 'Data funda��o' );

  if rgTipoPessoa.GetValor = 'F' then begin
    eInscricaoEstadual.Clear;
    eCPF_CNPJ.Tipo := [tccCPF];
    eInscricaoEstadual.Text := 'ISENTO';
  end
  else begin
    if rgTipoPessoa.GetValor = 'J' then
      eCPF_CNPJ.Tipo := [tccCNPJ]
    else
      eCPF_CNPJ.Tipo := [];
  end;
end;

procedure TFormCadastros.FormCreate(Sender: TObject);
begin
  inherited;
  eCPF_CNPJ.OnExit := eCPF_CNPJExit;

  if Self.ClassType = TFormCadastroMotorista then
    miHeranca.Caption := 'Motorista'
  else if Self.ClassType = TFormCadastroClientes then
    miHeranca.Caption := 'Cliente'
  else if Self.ClassType = TFormCadastroFornecedores then
    miHeranca.Caption := 'Fornecedor'
  else if Self.ClassType = TFormCadastroTransportadoras then begin
    miHeranca.Caption := 'Transportadora';
    miHeranca.Enabled := False;
  end
  else if Self.ClassType = TFormCadastroProfissionais then begin
    miHeranca.Caption := 'Profissional';
    miHeranca.Enabled := False;
  end;
end;

procedure TFormCadastros.GravarRegistro(Sender: TObject);
begin
  inherited;
  FCadastro.cadastro_id := eID.AsInt;
  FCadastro.nome_fantasia := eNomeFantasia.Text;
  FCadastro.razao_social := eRazaoSocial.Text;
  FCadastro.tipo_pessoa := rgTipoPessoa.GetValor;
  FCadastro.cpf_cnpj := eCPF_CNPJ.Text;
  FCadastro.logradouro := FrEndereco.getLogradouro;
  FCadastro.complemento := FrEndereco.getComplemento;
  FCadastro.numero := FrEndereco.getNumero;
  FCadastro.ponto_referencia := FrEndereco.getPontoReferencia;

  if FrEndereco.FrBairro.EstaVazio then
    FCadastro.bairro_id := Sessao.getEmpresaLogada.BairroId
  else
    FCadastro.bairro_id := FrEndereco.getBairroId;

  FCadastro.cep := FrEndereco.getCep;
  FCadastro.TelefonePrincipal  := eTelefone.Text;
  FCadastro.TelefoneCelular    := eCelular.Text;
  FCadastro.TelefoneFax        := eFax.Text;
  FCadastro.e_mail             := eEmail.Text;
  FCadastro.estado_civil       := cbEstadoCivil.GetValor;
  FCadastro.sexo               := rgSexo.GetValor;
  FCadastro.data_nascimento    := eDataNascimento.AsData;
  FCadastro.inscricao_estadual := eInscricaoEstadual.Text;
  FCadastro.cnae               := eCNAE.Text;
  FCadastro.rg                 := eRG.Text;
  FCadastro.orgao_expedidor_rg := eOrgaoExpedidor.Text;
  FCadastro.Observacoes        := eObservacoes.Lines.Text;
  FCadastro.ativo              := ToChar(ckAtivo);

  if eID.AsInt = 0 then begin
    FCadastro.EMotorista      := 'N';
    FCadastro.e_cliente       := 'N';
    FCadastro.e_fornecedor    := 'N';
    FCadastro.ETransportadora := 'N';
    FCadastro.EProfissional   := 'N';
  end;

  if Self.ClassType = TFormCadastroMotorista then begin
    if eID.AsInt = 0 then
      FCadastro.EMotorista := 'S';
  end
  else if Self.ClassType = TFormCadastroClientes then begin
    if eID.AsInt = 0 then
      FCadastro.e_cliente := 'S';
  end
  else if Self.ClassType = TFormCadastroFornecedores then begin
    if eID.AsInt = 0 then
      FCadastro.e_fornecedor := 'S';
  end
  else if Self.ClassType = TFormCadastroTransportadoras then begin
    if eID.AsInt = 0 then
      FCadastro.ETransportadora := 'S';
  end
  else if Self.ClassType = TFormCadastroProfissionais then begin
    if eID.AsInt = 0 then
      FCadastro.EProfissional := 'S';
  end;

  FTelefones := FrTelefones.Telefones;
  FEnderecos := FrDiversosEnderecos.getRecDiversosEnderecos;
end;

procedure TFormCadastros.miHerancaClick(Sender: TObject);
begin
  inherited;
  if Self.ClassType = TFormCadastroMotorista then
    Logs.Iniciar('LOGS_MOTORISTAS', ['CADASTRO_ID'], 'VW_TIPOS_ALTER_LOGS_MOTORISTAS', [eId.AsInt])
  else if Self.ClassType = TFormCadastroClientes then
    Logs.Iniciar('LOGS_CLIENTES', ['CADASTRO_ID'], 'VW_TIPOS_ALTER_LOGS_CLIENTES', [eId.AsInt])
  else if Self.ClassType = TFormCadastroFornecedores then
    Logs.Iniciar('LOGS_FORNECEDORES', ['CADASTRO_ID'], 'VW_TIPOS_ALT_LOGS_FORNECEDORES', [eId.AsInt])
  else if Self.ClassType = TFormCadastroTransportadoras then
    Logs.Iniciar('LOGS_TRANSPORTADORAS', ['CADASTRO_ID'], 'VW_TIPOS_ALTER_LOGS_TRANPORTADORAS', [eId.AsInt])
  else if Self.ClassType = TFormCadastroProfissionais then
    Logs.Iniciar('LOGS_PROFISSIONAIS', ['CADASTRO_ID'], 'VW_TIPOS_ALTER_LOGS_PROFISSIONAIS', [eId.AsInt]);
end;

procedure TFormCadastros.miLogsCadastroClick(Sender: TObject);
begin
  inherited;
  Logs.Iniciar('LOGS_CADASTROS', ['CADASTRO_ID'], 'VW_TIPOS_ALTER_LOGS_CADASTROS', [eId.AsInt]);
end;

procedure TFormCadastros.Modo(pEditando: Boolean);
begin
  inherited;

  _Biblioteca.Habilitar([
    sbLogs,

    (* Principais *)
    eCPF_CNPJ,
    eDataCadastro,
    eNomeFantasia,
    eRazaoSocial,
    rgTipoPessoa,
    pcDados,
    ckAtivo,

    (* Aba principais *)
    eRG,
    eOrgaoExpedidor,
    eInscricaoEstadual,
    rgSexo,
    eDataNascimento,
    cbEstadoCivil,
    eCNAE,
    eTelefone,
    eCelular,
    eFax,
    eEmail,
    FrEndereco,

    (* Aba Endere�os *)
    FrDiversosEnderecos,

    (* Aba telefones *)
    FrTelefones,

    (* Observa��es *)
    eObservacoes],

    pEditando
  );

  FPesquisouCadastro := False;

  if pEditando then begin
    FCadastro := RecCadastros.Create;
    ckAtivo.Checked := True;
    SetarFoco(eCPF_CNPJ);
    eCPF_CNPJ.EditMask := '';
  end
  else
    FCadastro.Free;
end;

procedure TFormCadastros.PreencherRegistro(
  pCadastro: RecCadastros;
  pTelefones: TArray<RecCadastrosTelefones>;
  pEnderecos: TArray<RecDiversosEnderecos>
);
begin

  eID.AsInt := pCadastro.cadastro_id;
  eDataCadastro.AsData := pCadastro.data_cadastro;
  eRazaoSocial.Text    := pCadastro.razao_social;
  eNomeFantasia.Text   := pCadastro.nome_fantasia;

  if Length(RetornaNumeros(pCadastro.cpf_cnpj)) = 11 then
    eCPF_CNPJ.EditMask := '999.999.999-99'
  else
    eCPF_CNPJ.EditMask := '99.999.999/9999-99';

  eCPF_CNPJ.Text       := pCadastro.cpf_cnpj;

  rgTipoPessoa.SetIndicePorValor(pCadastro.tipo_pessoa);
  rgTipoPessoaClick(nil);
  rgTipoPessoa.Enabled := False;
  eRG.Text             := pCadastro.rg;
  eOrgaoExpedidor.Text := pCadastro.orgao_expedidor_rg;
  eInscricaoEstadual.Text := pCadastro.inscricao_estadual;
  rgSexo.SetIndicePorValor(pCadastro.sexo);
  eDataNascimento.AsData := pCadastro.data_nascimento;
  cbEstadoCivil.SetIndicePorValor(pCadastro.estado_civil);
  eCNAE.Text     := pCadastro.cnae;
  eTelefone.Text := pCadastro.TelefonePrincipal;
  eCelular.Text  := pCadastro.TelefoneCelular;
  eFax.Text      := pCadastro.TelefoneFax;
  FrEndereco.setLogradouro(pCadastro.logradouro);
  FrEndereco.setComplemento(pCadastro.complemento);
  FrEndereco.setNumero(pCadastro.numero);
  FrEndereco.setBairroId(pCadastro.bairro_id);
  FrEndereco.setPontoReferencia(pCadastro.ponto_referencia);
  FrEndereco.setCep(pCadastro.cep);
  eEmail.Text := pCadastro.e_mail;
  eObservacoes.Lines.Text := pCadastro.Observacoes;

  FrDiversosEnderecos.setDiversosEnderecos(pEnderecos);
  FrTelefones.Telefones := pTelefones;
  ckAtivo.Checked       := (pCadastro.ativo = 'S');

  FCadastro := pCadastro;

  if Self.ClassType = TFormCadastroMotorista then begin
    if FCadastro.EMotorista = 'N' then
      FPesquisouCadastro := True;
  end
  else if Self.ClassType = TFormCadastroClientes then begin
    if FCadastro.e_cliente = 'N' then
      FPesquisouCadastro := True;
  end
  else if Self.ClassType = TFormCadastroFornecedores then begin
    if FCadastro.e_fornecedor = 'N' then
      FPesquisouCadastro := True;
  end
  else if Self.ClassType = TFormCadastroTransportadoras then begin
    if FCadastro.ETransportadora = 'N' then
      FPesquisouCadastro := True;
  end
  else if Self.ClassType = TFormCadastroProfissionais then begin
    if FCadastro.EProfissional = 'N' then
      FPesquisouCadastro := True;
  end;
  SetarFoco(eRazaoSocial);
end;

procedure TFormCadastros.rgTipoPessoaClick(Sender: TObject);
begin
  inherited;
  AjustarCampos;
end;

procedure TFormCadastros.sbPesquisarCadastrosClick(Sender: TObject);
var
  vCadastro: RecCadastros;
  vTelefones: TArray<RecCadastrosTelefones>;
  vEnderecos: TArray<RecDiversosEnderecos>;
begin
  inherited;

  vCadastro := RecCadastros(Pesquisa.Cadastros.Pesquisar(True));
  if vCadastro = nil then
    Abort;

  vTelefones := _CadastrosTelefones.BuscarCadastrosTelefones(Sessao.getConexaoBanco, 0, [vCadastro.cadastro_id]);
  vEnderecos := _DiversosEnderecos.BuscarDiversosEnderecos(Sessao.getConexaoBanco, 0, [vCadastro.cadastro_id], False);

  Modo(True);
  PreencherRegistro(vCadastro, vTelefones, vEnderecos);
end;

procedure TFormCadastros.VerificarRegistro(Sender: TObject);
begin
  inherited;
  eInscricaoEstadual.Text := Trim(eInscricaoEstadual.Text);

  if not rgTipoPessoa.ItemIndex in[0, 1] then begin
    _Biblioteca.Exclamar('� necess�rio informar o tipo de pessoa!');
    SetarFoco(rgTipoPessoa);
    Abort;
  end;

  if not eCPF_CNPJ.getCPF_CNPJOk then begin
    _Biblioteca.Exclamar('� necess�rio informar um ' + IIfStr(rgTipoPessoa.GetValor = 'J', 'CNPJ', 'CPF') + ' v�lido!');
    SetarFoco(eCPF_CNPJ);
    Abort;
  end;

  if eRazaoSocial.Trim = '' then begin
    if rgTipoPessoa.GetValor = 'F' then
      _Biblioteca.Exclamar('� necess�rio informar o nome real da pessoa!')
    else
      _Biblioteca.Exclamar('� necess�rio informar a raz�o social!');

    SetarFoco(eRazaoSocial);
    Abort;
  end;

  if rgTipoPessoa.GetValor = 'J' then begin
    if eNomeFantasia.Trim = '' then begin
      _Biblioteca.Exclamar('� necess�rio informar o nome fantasia!');
      SetarFoco(eNomeFantasia);
      Abort;
    end;
  end;

  if eInscricaoEstadual.Text = '' then begin
    _Biblioteca.Exclamar('A inscri��o estadual n�o foi informada!');
    SetarFoco(eInscricaoEstadual);
    Abort;
  end;

  if TemTextoENumero(eInscricaoEstadual.Text) then begin
    _Biblioteca.Exclamar('Inscri�ao estadual inv�lida!');
    SetarFoco(eInscricaoEstadual);
    Abort;
  end;

  if RetornaTexto(eInscricaoEstadual.Text) <> '' then begin
    if eInscricaoEstadual.Text <> 'ISENTO' then begin
      _Biblioteca.Exclamar('Inscri�ao estadual inv�lida!');
      SetarFoco(eInscricaoEstadual);
      Abort;
    end;
  end;

  if
    (not eTelefone.getTelefoneOk) and
    (not eCelular.getTelefoneOk) and
    (not eFax.getTelefoneOk) and
    (FrTelefones.EstaVazio)
  then begin
    if not _Biblioteca.Perguntar('Nenhum telefone foi informado, deseja realmente continuar?') then begin
      SetarFoco(eTelefone);
      Abort;
    end;
  end;

  FrEndereco.VerificarDados;
end;

end.
