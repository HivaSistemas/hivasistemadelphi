inherited FormBuscarTiposCobrancaCartoes: TFormBuscarTiposCobrancaCartoes
  Caption = 'Busca de tipos de cobr. - Cart'#245'es'
  ClientHeight = 232
  ClientWidth = 267
  ExplicitWidth = 273
  ExplicitHeight = 261
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 195
    Width = 267
    ExplicitTop = 195
    ExplicitWidth = 267
  end
  object sgCobrancas: TGridLuka
    Left = 1
    Top = 1
    Width = 264
    Height = 193
    Align = alCustom
    ColCount = 2
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect]
    TabOrder = 1
    OnDblClick = sgCobrancasDblClick
    OnDrawCell = sgCobrancasDrawCell
    OnKeyDown = sgCobrancasKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Cobr.'
      'Nome')
    Grid3D = False
    RealColCount = 7
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      37
      208)
  end
end
