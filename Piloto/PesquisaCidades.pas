unit PesquisaCidades;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _RecordsCadastros,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _Cidades, _Biblioteca, System.StrUtils, _Sessao,
  System.Math, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameEstados,
  Vcl.ExtCtrls;

type
  TFormPesquisaCidades = class(TFormHerancaPesquisas)
    FrEstado: TFrEstados;
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
  protected
    procedure BuscarRegistros; override;
  end;

function PesquisarCidade(pPreencherEstado:Boolean): TObject;

var
  vPreencherEstado:Boolean;

implementation

{$R *.dfm}

const
  cp_nome   = 2;
  cp_estado = 3;
  cp_codigoibge = 4;
  cp_ativo  = 5;

function PesquisarCidade(pPreencherEstado:Boolean): TObject;
var
  obj: TObject;
begin
  vPreencherEstado := pPreencherEstado;
  obj := _HerancaPesquisas.Pesquisar(TFormPesquisaCidades, _Cidades.GetFiltros);
  if obj = nil then
    Result := nil
  else
    Result := RecEstados(obj);
end;

procedure TFormPesquisaCidades.BuscarRegistros;
var
  i: Integer;
  vCidades: TArray<RecCidades>;
begin
  inherited;

  if FrEstado.EstaVazio then begin
    vCidades :=
      _Cidades.BuscarCidades(
        Sessao.getConexaoBanco,
        cbOpcoesPesquisa.GetIndice,
        [eValorPesquisa.Text]
      );
  end
  else begin
    vCidades :=
      _Cidades.BuscarCidades(
        Sessao.getConexaoBanco,
        cbOpcoesPesquisa.GetIndice,
        [eValorPesquisa.Text],
        'and ESTADO_ID = ''' + FrEstado.getEstado().estado_id + ''' '
      );
  end;

  if vCidades = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vCidades);

  for i := Low(vCidades) to High(vCidades) do begin
    sgPesquisa.Cells[coSelecionado, i + 1] := charNaoSelecionado;
    sgPesquisa.Cells[coCodigo, i + 1]      := NFormat(vCidades[i].cidade_id);
    sgPesquisa.Cells[cp_nome, i + 1]        := vCidades[i].nome;
    sgPesquisa.Cells[cp_estado, i + 1]      := vCidades[i].estado_id;
    sgPesquisa.Cells[cp_codigoibge, i + 1]  := IntToStr(vCidades[i].codigo_ibge);
    sgPesquisa.Cells[cp_ativo, i + 1]       := vCidades[i].ativo;
  end;
  sgPesquisa.SetLinhasGridPorTamanhoVetor( Length(vCidades) );
  SetarFoco(sgPesquisa);
end;

procedure TFormPesquisaCidades.FormCreate(Sender: TObject);
begin
  inherited;
  if vPreencherEstado then
    FrEstado.InserirDadoPorChave( Sessao.getEmpresaLogada.EstadoId, False );
end;

procedure TFormPesquisaCidades.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if _Biblioteca.Em(ACol, [coSelecionado, cp_ativo]) then
    vAlinhamento := taCenter
  else if ACol = coCodigo then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
