inherited FormImpressaoRelacaoVendasProdutosGrafico: TFormImpressaoRelacaoVendasProdutosGrafico
  Caption = 'FormImpressaoRelacaoVendasProdutosGrafico'
  ClientWidth = 1166
  ExplicitWidth = 1182
  PixelsPerInch = 96
  TextHeight = 13
  inherited qrRelatorioA4: TQuickRep
    Left = 24
    Top = 30
    BeforePrint = qrRelatorioA4BeforePrint
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    OnNeedData = qrRelatorioA4NeedData
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    ExplicitLeft = 24
    ExplicitTop = 30
    inherited qrbndCabecalho: TQRBand
      Height = 53
      Size.Values = (
        100.163690476190500000
        1899.330357142857000000)
      ExplicitHeight = 53
      inherited qrVersaoSistema: TQRLabel
        Size.Values = (
          33.072916666666670000
          8.819444444444444000
          6.614583333333332000
          734.218750000000000000)
        FontSize = 7
      end
      inherited qrEmitidoEm: TQRLabel
        Size.Values = (
          29.104166666666670000
          1529.291666666667000000
          0.000000000000000000
          370.416666666666700000)
        FontSize = 6
      end
      inherited qr13: TQRLabel
        Left = 176
        Width = 617
        Size.Values = (
          52.916666666666680000
          332.619047619047700000
          39.687500000000000000
          1166.056547619048000000)
        Caption = 'Rela'#231#227'o de vendas por produto agrupado por vendedor'
        FontSize = 10
        ExplicitLeft = 176
        ExplicitWidth = 617
      end
      inherited qrUsuarioImpressao: TQRLabel
        Size.Values = (
          33.072916666666670000
          802.569444444444500000
          6.614583333333332000
          526.961805555555600000)
        FontSize = 7
      end
      inherited QRSysData1: TQRSysData
        Left = 936
        Width = 37
        Size.Values = (
          39.687500000000000000
          1768.928571428571000000
          26.458333333333340000
          69.925595238095240000)
        FontSize = 7
        ExplicitLeft = 936
        ExplicitWidth = 37
      end
      inherited qr2: TQRLabel
        Left = 880
        Size.Values = (
          39.687500000000000000
          1663.095238095238000000
          26.458333333333340000
          105.833333333333400000)
        FontSize = 7
        ExplicitLeft = 880
      end
      inherited qrQtdePaginas: TQRLabel
        Width = 32
        Size.Values = (
          39.687500000000000000
          1838.854166666667000000
          26.458333333333340000
          60.476190476190480000)
        FontSize = 7
        ExplicitWidth = 32
      end
    end
    object qrbndColumnHeaderBand1: TQRBand [1]
      Left = 53
      Top = 106
      Width = 1005
      Height = 19
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        35.907738095238100000
        1899.330357142857000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbColumnHeader
      object qr8: TQRLabel
        Left = 5
        Top = 1
        Width = 563
        Height = 18
        Size.Values = (
          34.017857142857150000
          9.449404761904763000
          1.889880952380953000
          1064.002976190476000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vendedor'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr18: TQRLabel
        Left = 570
        Top = 1
        Width = 92
        Height = 18
        Size.Values = (
          34.017857142857150000
          1077.232142857143000000
          1.889880952380953000
          173.869047619047600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Qtde.total'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr19: TQRLabel
        Left = 664
        Top = 1
        Width = 153
        Height = 18
        Size.Values = (
          34.017857142857150000
          1254.880952380952000000
          1.889880952380953000
          289.151785714285700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor total l'#237'quido'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
    end
    object qrbndDetailBand1: TQRBand [2]
      Left = 53
      Top = 125
      Width = 1005
      Height = 37
      AlignToBottom = False
      BeforePrint = qrbndDetailBand1BeforePrint
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        69.925595238095240000
        1899.330357142857000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbDetail
      object qrCabValorLiquido: TQRLabel
        Left = 715
        Top = 19
        Width = 102
        Height = 18
        Size.Values = (
          34.017857142857150000
          1351.264880952381000000
          35.907738095238090000
          192.767857142857200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor l'#237'quido'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrCabQuantidade: TQRLabel
        Left = 570
        Top = 19
        Width = 92
        Height = 18
        Size.Values = (
          34.017857142857150000
          1077.232142857143000000
          35.907738095238090000
          173.869047619047600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Quantidade'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrCabNomeProduto: TQRLabel
        Left = 111
        Top = 19
        Width = 288
        Height = 18
        Size.Values = (
          34.017857142857150000
          209.776785714285700000
          35.907738095238090000
          544.285714285714300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nome produto'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrCabProdutoId: TQRLabel
        Left = 51
        Top = 19
        Width = 58
        Height = 18
        Size.Values = (
          34.017857142857150000
          96.383928571428570000
          35.907738095238090000
          109.613095238095200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Produto'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrCabMarca: TQRLabel
        Left = 401
        Top = 19
        Width = 167
        Height = 18
        Size.Values = (
          34.017857142857150000
          757.842261904762000000
          35.907738095238090000
          315.610119047619000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Marca'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrCabUnidade: TQRLabel
        Left = 664
        Top = 19
        Width = 49
        Height = 18
        Size.Values = (
          34.017857142857150000
          1254.880952380952000000
          35.907738095238090000
          92.604166666666680000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Und.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrNomeVendedor: TQRLabel
        Left = 5
        Top = 2
        Width = 563
        Height = 17
        Size.Values = (
          32.127976190476190000
          9.449404761904763000
          3.779761904761905000
          1064.002976190476000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '10 - GLEIDSTONE DA SILVA'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrQuantidadeVendedor: TQRLabel
        Left = 570
        Top = 2
        Width = 92
        Height = 17
        Size.Values = (
          32.127976190476190000
          1077.232142857143000000
          3.779761904761905000
          173.869047619047600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Qtde.total'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrTotalLiquidoVendedor: TQRLabel
        Left = 664
        Top = 2
        Width = 153
        Height = 18
        Size.Values = (
          34.017857142857150000
          1254.880952380952000000
          3.779761904761905000
          289.151785714285700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor total l'#237'quido'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrSeparadorVendedores: TQRShape
        Left = 2
        Top = 1
        Width = 1005
        Height = 1
        Size.Values = (
          1.889880952380953000
          3.779761904761905000
          1.889880952380953000
          1899.330357142857000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 15395562
        Shape = qrsHorLine
        VertAdjust = 0
      end
    end
    inherited qrbndRodape: TQRBand
      Top = 183
      Height = 72
      Size.Values = (
        136.071428571428600000
        1899.330357142857000000)
      PreCaluculateBandHeight = True
      ExplicitTop = 183
      ExplicitHeight = 72
      inherited qr1: TQRLabel
        Size.Values = (
          41.577380952380950000
          9.449404761904763000
          9.449404761904763000
          901.473214285714300000)
        FontSize = 8
      end
      inherited qrFiltrosUtilizados: TQRPMemo
        Width = 999
        Height = 26
        Size.Values = (
          49.136904761904770000
          9.449404761904763000
          60.476190476190480000
          1887.991071428571000000)
        Lines.Strings = (
          
            'Filtros utilizados Filtros utilizados Filtros utilizados Filtros' +
            ' utilizados Filtros utilizados Filtros utilizados Filtros utiliz' +
            'ados Filtros utilizados 123456789')
        StretchHeightWithBand = False
        FontSize = 8
        ExplicitWidth = 999
        ExplicitHeight = 26
      end
    end
    object QRSubDetail1: TQRSubDetail
      Left = 53
      Top = 162
      Width = 1005
      Height = 21
      AlignToBottom = False
      BeforePrint = QRSubDetail1BeforePrint
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        39.687500000000000000
        1899.330357142857000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      Master = qrRelatorioA4
      OnNeedData = QRSubDetail1NeedData
      PrintBefore = False
      PrintIfEmpty = True
      object qrValorLiquidoProduto: TQRLabel
        Left = 715
        Top = 1
        Width = 102
        Height = 18
        Size.Values = (
          34.017857142857150000
          1351.264880952381000000
          1.889880952380953000
          192.767857142857200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor l'#237'quido'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrQuantidadeProduto: TQRLabel
        Left = 570
        Top = 1
        Width = 92
        Height = 18
        Size.Values = (
          34.017857142857150000
          1077.232142857143000000
          1.889880952380953000
          173.869047619047600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Quantidade'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrNomeProduto: TQRLabel
        Left = 111
        Top = 1
        Width = 288
        Height = 18
        Size.Values = (
          34.017857142857150000
          209.776785714285700000
          1.889880952380953000
          544.285714285714300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nome produto'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrProdutoId: TQRLabel
        Left = 51
        Top = 1
        Width = 58
        Height = 18
        Size.Values = (
          34.017857142857150000
          96.383928571428570000
          1.889880952380953000
          109.613095238095200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Produto'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrMarcaProduto: TQRLabel
        Left = 401
        Top = 1
        Width = 167
        Height = 18
        Size.Values = (
          34.017857142857150000
          757.842261904762000000
          1.889880952380953000
          315.610119047619000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Marca'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrUnidadeProduto: TQRLabel
        Left = 664
        Top = 1
        Width = 49
        Height = 18
        Size.Values = (
          34.017857142857150000
          1254.880952380952000000
          1.889880952380953000
          92.604166666666680000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Und.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
    end
  end
end
