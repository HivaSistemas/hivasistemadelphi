unit GruposTributacoesFederalCompra;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _GruposTribFederalCompra,
  ComboBoxLuka, CheckBoxLuka, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _Sessao, _Biblioteca,
  _RecordsEspeciais, PesquisaGruposTributacoesFederalCompra;

type
  TFormGruposTributacoesFederalCompra = class(TFormHerancaCadastroCodigo)
    lb25: TLabel;
    cbOrigemProduto: TComboBoxLuka;
    lb3: TLabel;
    cbCstEntradaPIS: TComboBoxLuka;
    lb5: TLabel;
    cbCstEntradaCOFINS: TComboBoxLuka;
    eDescricao: TEditLuka;
    lb1: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    procedure PreencherRegistro(pGrupo: RecGruposTribFederalCompra);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormGruposTributacoesFederalCompra }

procedure TFormGruposTributacoesFederalCompra.BuscarRegistro;
var
  vGrupo: TArray<RecGruposTribFederalCompra>;
begin
  vGrupo := _GruposTribFederalCompra.BuscarGruposTribFederalCompra(Sessao.getConexaoBanco, 0, [eID.AsInt]);
  if vGrupo = nil then begin
    _Biblioteca.NenhumRegistro;
    SetarFoco(eID);
    Abort;
  end;

  inherited;
  PreencherRegistro(vGrupo[0]);
end;

procedure TFormGruposTributacoesFederalCompra.ExcluirRegistro;
begin
  Sessao.AbortarSeHouveErro( _GruposTribFederalCompra.ExcluirGruposTribFederalCompra(Sessao.getConexaoBanco, eID.AsInt) );
  _Biblioteca.RegistroExcluidoSucesso;
  inherited;
end;

procedure TFormGruposTributacoesFederalCompra.FormCreate(Sender: TObject);
begin
  inherited;
  Sessao.SetComboBoxPisCofins(cbCstEntradaPIS);
  Sessao.SetComboBoxPisCofins(cbCstEntradaCOFINS);
end;

procedure TFormGruposTributacoesFederalCompra.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vRetBanco :=
    _GruposTribFederalCompra.AtualizarGruposTribFederalCompra(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eDescricao.Text,
      cbOrigemProduto.AsInt,
      cbCstEntradaPIS.AsString,
      cbCstEntradaCOFINS.AsString,
      ckAtivo.CheckedStr
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormGruposTributacoesFederalCompra.Modo(pEditando: Boolean);
begin
  inherited;

  _Biblioteca.Habilitar([
    eDescricao,
    cbOrigemProduto,
    cbCstEntradaPIS,
    cbCstEntradaCOFINS],
    pEditando
  );

  if pEditando then
    SetarFoco(eDescricao);
end;

procedure TFormGruposTributacoesFederalCompra.PesquisarRegistro;
var
  vGrupo: RecGruposTribFederalCompra;
begin
  vGrupo := RecGruposTribFederalCompra(PesquisaGruposTributacoesFederalCompra.Pesquisar);
  if vGrupo = nil then
    Exit;

  inherited;
  PreencherRegistro(vGrupo);
end;

procedure TFormGruposTributacoesFederalCompra.PreencherRegistro(pGrupo: RecGruposTribFederalCompra);
begin
  eID.AsInt                   := pGrupo.GrupoTribFederalCompraId;
  eDescricao.Text             := pGrupo.Descricao;
  ckAtivo.CheckedStr          := pGrupo.Ativo;
  cbOrigemProduto.AsInt       := pGrupo.OrigemProduto;
  cbCstEntradaPIS.AsString    := pGrupo.CstPis;
  cbCstEntradaCOFINS.AsString := pGrupo.CstCofins;
end;

procedure TFormGruposTributacoesFederalCompra.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if eDescricao.Trim = '' then begin
    _Biblioteca.Exclamar('A descri��o do grupo de tributa��o n�o foi informada corretamente, verifique!');
    SetarFoco(eDescricao);
    Abort;
  end;

  if cbOrigemProduto.AsString = '' then begin
    _Biblioteca.Exclamar('A origem do produto n�o foi informada corretamente, verifique!');
    SetarFoco(cbOrigemProduto);
    Abort;
  end;

  if cbCstEntradaPIS.AsString = '' then begin
    _Biblioteca.Exclamar('A CST do PIS do produto n�o foi informada corretamente, verifique!');
    SetarFoco(cbCstEntradaPIS);
    Abort;
  end;

  if cbCstEntradaCOFINS.AsString = '' then begin
    _Biblioteca.Exclamar('A CST do COFINS do produto n�o foi informada corretamente, verifique!');
    SetarFoco(cbCstEntradaCOFINS);
    Abort;
  end;
end;

end.
