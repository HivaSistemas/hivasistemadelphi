unit ReceberBaixaContasReceber;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _RecordsFinanceiros,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, _ContasReceber, _ContasReceberBaixas, _ComunicacaoTEF,
  _Biblioteca, _Sessao, _ContasRecBaixasPagamentos, _ContasRecBaixasPagtosChq, _RecordsEspeciais,
  Vcl.StdCtrls, EditLuka, System.Math, Impressao.ComprovantePagamentoTituloReceberGrafico,
  FramePagamentoFinanceiro, ImpressaoComprovanteCartao;

type
  TFormReceberBaixaContasReceber = class(TFormHerancaFinalizar)
    FrPagamentoFinanceiro: TFrPagamentoFinanceiro;
    procedure FormShow(Sender: TObject);
  private
    FBaixaId: Integer;
    FClienteId: Integer;
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Receber(const pBaixaId: Integer): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

uses BuscarDadosCartoesTEF;

{ TFormReceberBaixaContasReceber }

function Receber(const pBaixaId: Integer): TRetornoTelaFinalizar;
var
  vForm: TFormReceberBaixaContasReceber;
  vDadosBaixa: TArray<RecContaReceberBaixa>;
begin
  if pBaixaId = 0 then
    Exit;

  vDadosBaixa := _ContasReceberBaixas.BuscarContaReceberBaixas(Sessao.getConexaoBanco, 0, [pBaixaId]);
  if vDadosBaixa = nil then begin
    Exclamar('Nenhum registro encontrado!');
    Exit;
  end;

  vForm := TFormReceberBaixaContasReceber.Create(Application);

  vForm.FBaixaId   := pBaixaId;
  vForm.FClienteId := vDadosBaixa[0].CadastroId;
  vForm.FrPagamentoFinanceiro.ValorPagar            := vDadosBaixa[0].ValorCartaoDebito + vDadosBaixa[0].ValorCartaoCredito;
  vForm.FrPagamentoFinanceiro.eValorDesconto.AsCurr := vDadosBaixa[0].valor_desconto;

  vForm.FrPagamentoFinanceiro.ValorCartaoDebito  := vDadosBaixa[0].ValorCartaoDebito;
  vForm.FrPagamentoFinanceiro.ValorCartaoCredito := vDadosBaixa[0].ValorCartaoCredito;

  vForm.FrPagamentoFinanceiro.setUtilizaTef(Sessao.getParametrosEstacao.UtilizarTef = 'S');

  if Sessao.getParametrosEstacao.UtilizarTef <> 'S' then begin
    vForm.FrPagamentoFinanceiro.CartoesDebito  := _ContasRecBaixasPagamentos.BuscarContasRecBaixasPagamentos(Sessao.getConexaoBanco, 1, [pBaixaId]);
    vForm.FrPagamentoFinanceiro.CartoesCredito := _ContasRecBaixasPagamentos.BuscarContasRecBaixasPagamentos(Sessao.getConexaoBanco, 2, [pBaixaId]);
  end;

  vForm.FrPagamentoFinanceiro.TotalizarValoresASerPago;

  Result.Ok(vForm.ShowModal);

  vForm.Free;
end;

procedure TFormReceberBaixaContasReceber.Finalizar(Sender: TObject);
var
  i: Integer;
  vPosic: Integer;
  vTemComprovanteTef: Boolean;
  vCartoes: TArray<RecTitulosFinanceiros>;
  vRetornoCartoes: TRetornoTelaFinalizar< TArray<RecRespostaTEF> >;

  vRetBanco: RecRetornoBD;
  vQtdeMaximaParcelas: Integer;
begin
  vCartoes := nil;
  vTemComprovanteTef := False;

  // Se houver TEF, chamando a tela que realizar� a passagem destes cart�es
  if FrPagamentoFinanceiro.eValorCartaoDebito.AsCurr + FrPagamentoFinanceiro.eValorCartaoCredito.AsCurr > 0 then begin
    if Sessao.getParametrosEstacao.UtilizarTef = 'S' then begin
      vQtdeMaximaParcelas := _ContasReceberBaixas.getQtdeMaximaParcelas(Sessao.getConexaoBanco, FBaixaId);

      vRetornoCartoes :=
        BuscarDadosCartoesTEF.BuscarPagamentosTEF(
          FrPagamentoFinanceiro.eValorCartaoDebito.AsCurr,
          FrPagamentoFinanceiro.eValorCartaoCredito.AsCurr,
          vQtdeMaximaParcelas,
          False,
          False
        );

      if vRetornoCartoes.BuscaCancelada then begin
        BuscarDadosCartoesTEF.CancelarCartoes( vRetornoCartoes.Dados );
        RotinaCanceladaUsuario;
        Abort;
      end;

      vCartoes := nil;
      SetLength(vCartoes, Length(vRetornoCartoes.Dados));
      for i := Low(vRetornoCartoes.Dados) to High(vRetornoCartoes.Dados) do begin
        vCartoes[i].CobrancaId        := vRetornoCartoes.Dados[i].CobrancaId;
        vCartoes[i].ItemId            := i;
        vCartoes[i].Valor             := vRetornoCartoes.Dados[i].Valor;
        vCartoes[i].NsuTef            := vRetornoCartoes.Dados[i].Nsu;
        vCartoes[i].NumeroCartao      := vRetornoCartoes.Dados[i].NumeroCartao;
        vCartoes[i].CodigoAutorizacao := vRetornoCartoes.Dados[i].CodigoAutorizacao;
        vCartoes[i].TipoRecebCartao   := vRetornoCartoes.Dados[i].TipoRecebimento;

        if not vTemComprovanteTef then
          vTemComprovanteTef := vRetornoCartoes.Dados[i].TipoRecebimento = 'TEF';
      end;
    end
    else begin
      SetLength(vCartoes, Length(FrPagamentoFinanceiro.CartoesDebito) + Length(FrPagamentoFinanceiro.CartoesCredito));

      vPosic := -1;
      for i := Low(FrPagamentoFinanceiro.CartoesDebito) to High(FrPagamentoFinanceiro.CartoesDebito) do begin
        Inc(vPosic);
        vCartoes[vPosic].CobrancaId := FrPagamentoFinanceiro.CartoesDebito[i].CobrancaId;
        vCartoes[vPosic].ItemId     := vPosic + 1;
        vCartoes[vPosic].Valor      := FrPagamentoFinanceiro.CartoesDebito[i].Valor;
        vCartoes[vPosic].TipoRecebCartao := FrPagamentoFinanceiro.CartoesDebito[i].tipoRecebimentoCartao;
      end;

      for i := Low(FrPagamentoFinanceiro.CartoesCredito) to High(FrPagamentoFinanceiro.CartoesCredito) do begin
        Inc(vPosic);
        vCartoes[vPosic].CobrancaId := FrPagamentoFinanceiro.CartoesCredito[i].CobrancaId;
        vCartoes[vPosic].ItemId     := vPosic + 1;
        vCartoes[vPosic].Valor      := FrPagamentoFinanceiro.CartoesCredito[i].Valor;
        vCartoes[vPosic].TipoRecebCartao := FrPagamentoFinanceiro.CartoesCredito[i].tipoRecebimentoCartao;
      end;
    end;
  end;

  vRetBanco :=
    _ContasReceberBaixas.ReceberBaixaNoCaixa(
      Sessao.getConexaoBanco,
      FBaixaId,
      Sessao.getTurnoCaixaAberto.TurnoId,
      vCartoes
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  RotinaSucesso;

  // Imprimir as vias dos cart�es
  if (Sessao.getParametrosEstacao.UtilizarTef = 'S') and (vTemComprovanteTef) then begin
    for i := Low(vRetornoCartoes.Dados) to High(vRetornoCartoes.Dados) do
      ImpressaoComprovanteCartao.Imprimir(vRetornoCartoes.Dados[i].Comprovante1Via, vRetornoCartoes.Dados[i].Comprovante2Via);
  end;

  if FrPagamentoFinanceiro.eValorCartaoDebito.AsCurr + FrPagamentoFinanceiro.eValorCredito.AsCurr > 0 then begin
    if Perguntar('Deseja imprimir o comprovante de pagamento?') then
      Impressao.ComprovantePagamentoTituloReceberGrafico.Imprimir(FBaixaId);
  end;

  inherited;
end;

procedure TFormReceberBaixaContasReceber.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrPagamentoFinanceiro.eValorCartaoDebito);
end;

procedure TFormReceberBaixaContasReceber.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrPagamentoFinanceiro.ExisteFormasPagtoNaoDefinidas(Sessao.getParametrosEstacao.UtilizarTef <> 'S') then begin
    _Biblioteca.Exclamar('Os cart�es n�o foram bem definidos, verifique!');
    SetarFoco(FrPagamentoFinanceiro.eValorCartaoDebito);
    Abort;
  end;
end;

end.
