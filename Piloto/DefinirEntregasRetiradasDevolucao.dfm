inherited FormDefinirEntregasRetiradasEntregasDevolucao: TFormDefinirEntregasRetiradasEntregasDevolucao
  Caption = 'Definir entregas/retiradas na confirma'#231#227'o de devolu'#231#227'o'
  ClientHeight = 451
  ClientWidth = 635
  ExplicitWidth = 641
  ExplicitHeight = 480
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 4
    Top = 3
    Width = 57
    Height = 14
    Caption = 'Devolu'#231#227'o'
  end
  inherited pnOpcoes: TPanel
    Top = 414
    Width = 635
    ExplicitTop = 414
    ExplicitWidth = 635
  end
  object sgEntregas: TGridLuka
    Left = 2
    Top = 211
    Width = 631
    Height = 202
    ColCount = 6
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
    TabOrder = 1
    OnDrawCell = sgEntregasDrawCell
    OnEnter = sgEntregasEnter
    OnSelectCell = sgEntregasSelectCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'C'#243'digo'
      'Tipo entrega'
      'Data/hora ent./ret.'
      'Lote'
      'Qtde.dispon'#237'vel'
      'Qtde.utilizar')
    OnGetCellColor = sgEntregasGetCellColor
    OnArrumarGrid = sgEntregasArrumarGrid
    Grid3D = False
    RealColCount = 12
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      45
      125
      115
      121
      109
      94)
  end
  object StaticTextLuka1: TStaticTextLuka
    Left = 8
    Top = 200
    Width = 630
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Entregas / retiradas dispon'#237'veis para o item selecionado'
    Color = clSilver
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object sgItens: TGridLuka
    Left = 2
    Top = 59
    Width = 631
    Height = 135
    ColCount = 6
    Ctl3D = False
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
    ParentCtl3D = False
    TabOrder = 3
    OnClick = sgItensClick
    OnDrawCell = sgItensDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Produto'
      'Nome'
      'Marca'
      'Und.'
      'Quantidade'
      'Qtde.definida'
      '')
    Grid3D = False
    RealColCount = 12
    AtivarPopUpSelecao = False
    ColWidths = (
      51
      214
      108
      62
      74
      84)
  end
  object StaticTextLuka2: TStaticTextLuka
    Left = 2
    Top = 42
    Width = 631
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Itens da devolu'#231#227'o  a confirmar'
    Color = clSilver
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 4
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object eDevolucaoId: TEditLuka
    Left = 4
    Top = 17
    Width = 80
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 5
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  inline FrLocalDevolucao: TFrLocais
    Left = 90
    Top = 0
    Width = 303
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 6
    TabStop = True
    ExplicitLeft = 90
    ExplicitWidth = 303
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 278
      Height = 23
      ExplicitWidth = 278
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 303
      ExplicitWidth = 303
      inherited lbNomePesquisa: TLabel
        Width = 104
        Caption = 'Local da devolu'#231#227'o'
        ExplicitWidth = 104
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 198
        ExplicitLeft = 198
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 278
      Height = 24
      ExplicitLeft = 278
      ExplicitHeight = 24
    end
  end
end
