unit CadastroUnidades;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _RecordsCadastros, _Sessao,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _RecordsEspeciais, _Unidades, _Biblioteca, PesquisaUnidades,
  _HerancaCadastro, CheckBoxLuka;

type
  TFormCadastroUnidades = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eDescricao: TEditLuka;
    procedure eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); reintroduce;
    procedure sbGravarClick(Sender: TObject);
    procedure sbDesfazerClick(Sender: TObject);
    procedure sbExcluirClick(Sender: TObject);
    procedure sbPesquisarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FCarregou: Boolean;
    procedure PreencherRegistro(pUnidade: RecUnidades);
  protected
    procedure BuscarRegistro; reintroduce;
    procedure GravarRegistro; reintroduce;
    procedure ExcluirRegistro; reintroduce;
    procedure VerificarRegistro; reintroduce;
    procedure PesquisarRegistro; reintroduce;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean); reintroduce;
  end;

implementation

{$R *.dfm}

{ TFormCadastroUnidades }

procedure TFormCadastroUnidades.BuscarRegistro;
var
  vUnidades: TArray<RecUnidades>;
begin
  FCarregou := False;

  vUnidades := _Unidades.BuscarUnidades(Sessao.getConexaoBanco, 0, [eId.Text]);
  if vUnidades = nil then
    Exit;

  PreencherRegistro(vUnidades[0]);
end;

procedure TFormCadastroUnidades.eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then begin
    if (eID.Text) = '' then begin
      _Biblioteca.Exclamar('� necess�rio informar a unidade abreviada!');
      eID.SetFocus;
      Abort;
    end;

    BuscarRegistro;
    if eDescricao.Trim = '' then
      Modo(True, False);
  end;
end;

procedure TFormCadastroUnidades.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _Unidades.ExcluirUnidade(Sessao.getConexaoBanco, eID.Text);

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  Modo(False, True);
end;

procedure TFormCadastroUnidades.FormCreate(Sender: TObject);
begin
  inherited;
  eID.OnKeyDown := eIDKeyDown;
  Modo(False, True);
end;

procedure TFormCadastroUnidades.GravarRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _Unidades.AtualizarUnidade(Sessao.getConexaoBanco, eId.Text, eDescricao.Text, _Biblioteca.ToChar(ckAtivo));
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  _Biblioteca.Informar(coAlteracaoRegistroSucesso);

  Modo(False, True);
end;

procedure TFormCadastroUnidades.Modo(pEditando, pLimpar: Boolean);
begin
  _Biblioteca.Habilitar([
    eDescricao,
    ckAtivo,
    sbGravar,
    sbDesfazer,
    ckAtivo],
    pEditando,
    pLimpar
  );

  _Biblioteca.Habilitar([eID, sbPesquisar], not pEditando, pLimpar);
  sbExcluir.Enabled := FCarregou and pEditando;

  if pEditando then begin
    SetarFoco(eDescricao);

    if not ckAtivo.Checked then
      ckAtivo.Checked := not FCarregou;
  end
  else
    SetarFoco(eID);
end;

procedure TFormCadastroUnidades.PesquisarRegistro;
var
  vUnidades: RecUnidades;
begin
  vUnidades := RecUnidades(PesquisaUnidades.PesquisarUnidade);
  if vUnidades = nil then
    Exit;

  PreencherRegistro(vUnidades);
end;

procedure TFormCadastroUnidades.PreencherRegistro(pUnidade: RecUnidades);
begin
  FCarregou := True;
  Modo(True, True);

  eID.Text := pUnidade.unidade_id;
  eDescricao.Text := pUnidade.descricao;
  ckAtivo.Checked := (pUnidade.ativo = 'S');

  pUnidade.Free;
end;

procedure TFormCadastroUnidades.sbDesfazerClick(Sender: TObject);
begin
  if not Perguntar('Deseja realmente desfazer as altera��es deste registro?') then
    Exit;

  Modo(False, True);
end;

procedure TFormCadastroUnidades.sbExcluirClick(Sender: TObject);
begin
  if not Perguntar('Deseja realmente excluir este registro?') then
    Exit;

  ExcluirRegistro;
end;

procedure TFormCadastroUnidades.sbGravarClick(Sender: TObject);
begin
  VerificarRegistro;
  GravarRegistro;
end;

procedure TFormCadastroUnidades.sbPesquisarClick(Sender: TObject);
begin
  PesquisarRegistro;
end;

procedure TFormCadastroUnidades.VerificarRegistro;
begin

  if _Unidades.ExisteUnidadeDescricaoIgual(Sessao.getConexaoBanco, eId.Text, eDescricao.Trim) then begin
    if not _Biblioteca.Perguntar('J� existe uma unidade cadastrada com a mesma descri��o da que est� sendo gravada, deseja realmente continuar?') then
      Abort;
  end;

  if eDescricao.Trim = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar a descri��o da unidade!');
    eDescricao.SetFocus;
    Abort;
  end;
end;

end.
