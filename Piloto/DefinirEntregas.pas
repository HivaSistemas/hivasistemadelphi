unit DefinirEntregas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms,
  Vcl.Dialogs, _HerancaFinalizar, Vcl.Grids, GridLuka, _Sessao, _EntregasItensPendentes, Vcl.Buttons, Vcl.ExtCtrls, _Biblioteca,
  _RecordsEspeciais, _GruposEstoques, _Estoques, _RecordsEstoques;

type
  RecEmpresasGrid = record
    EmpresaId: Integer;
    PosicaoGrid: Integer;
    PosicaoEstFisico: Integer;
    PosicaoEstQtdeDefinida: Integer;
  end;

  TFormDefinirEntregas = class(TFormHerancaFinalizar)
    sgItens: TGridLuka;
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
  private
    FEmpresasGrid: TArray<RecEmpresasGrid>;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Finalizar(Sender: TObject); override;
  end;

function Entregar( pItens: TArray<RecEntregasItensPendentes> ): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

const
  clLinhaEmpresa   = 0;
  clLinhaCabecalho = 1;

  coProdutoId       = 0;
  coNomeProduto     = 1;
  coQuantidade      = 2;
  coUnidade         = 3;

  (* Ocultas *)
  coOrcamentoId     = 4;
  coEmpresaPendId   = 5;
  coItemId          = 6;
  coLocalId         = 7;
  coLote            = 8;
  coAceitarEstoqueNegativo = 9;

  // Sempre por �ltimo
  coInicioEmpresas  = 10;


function Entregar( pItens: TArray<RecEntregasItensPendentes> ): TRetornoTelaFinalizar;
var
  i: Integer;
  j: Integer;
  k: Integer;
  vForm: TFormDefinirEntregas;

  vEstoques: TArray<RecEstoque>;
  vProdutosIds: TArray<Integer>;
  vEmpresasIds: TArray<Integer>;
begin
  vForm := TFormDefinirEntregas.Create(nil);

  vProdutosIds := nil;
  vEmpresasIds := nil;
  for i := Low(pItens) to High(pItens) do begin
    vForm.sgItens.Cells[coProdutoId, i + 2]     := NFormat(pItens[i].ProdutoId);
    vForm.sgItens.Cells[coNomeProduto, i + 2]   := pItens[i].Nome;
    vForm.sgItens.Cells[coQuantidade, i + 2]    := NFormatEstoque(pItens[i].QtdeEntregar);
    vForm.sgItens.Cells[coUnidade, i + 2]       := pItens[i].Unidade;

    vForm.sgItens.Cells[coOrcamentoId, i + 2]   := NFormat(pItens[i].OrcamentoId);
    vForm.sgItens.Cells[coEmpresaPendId, i + 2] := NFormat(pItens[i].EmpresaId);
    vForm.sgItens.Cells[coItemId, i + 2]        := NFormat(pItens[i].ItemId);
    vForm.sgItens.Cells[coLocalId, i + 2]       := NFormat(pItens[i].LocalId);
    vForm.sgItens.Cells[coLote, i + 2]          := pItens[i].Lote;
    vForm.sgItens.Cells[coAceitarEstoqueNegativo, i + 2] := pItens[i].AceitarEstoqueNegativo;

    _Biblioteca.AddNoVetorSemRepetir(vProdutosIds, pItens[i].ProdutoId);
  end;
  vForm.sgItens.RowCount := Length(pItens) + 2;
  vForm.sgItens.OcultarColunas([coOrcamentoId, coEmpresaPendId, coItemId, coLocalId, coLote, coAceitarEstoqueNegativo]);

  (***************** BUSCANDO O ESTOQUE F�SICO PARA CADA PRODUTO *******************)
  for i := Low(vForm.FEmpresasGrid) to High(vForm.FEmpresasGrid) do
    _Biblioteca.AddNoVetorSemRepetir(vEmpresasIds, vForm.FEmpresasGrid[i].EmpresaId);

  vEstoques := _Estoques.BuscarEstoqueComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('EST.EMPRESA_ID', vEmpresasIds) + ' and ' + FiltroInInt('EST.PRODUTO_ID', vProdutosIds));
  for i := Low(vEstoques) to High(vEstoques) do begin
    for j := vForm.sgItens.FixedRows to vForm.sgItens.RowCount - 1 do begin
      if vEstoques[i].produto_id <> SFormatInt(vForm.sgItens.Cells[coProdutoId, j]) then
        Continue;

      for k := Low(vForm.FEmpresasGrid) to High(vForm.FEmpresasGrid) do begin
        if vEstoques[i].empresa_id <> vForm.FEmpresasGrid[k].EmpresaId then
          Continue;

        vForm.sgItens.Cells[vForm.FEmpresasGrid[k].PosicaoEstFisico, j] := _Biblioteca.NFormatNEstoque(vEstoques[i].Fisico);
      end;
    end;
  end;
  (* ****************************************************************************** *)


  Result.Ok(vForm.ShowModal, True);
end;

procedure TFormDefinirEntregas.FormCreate(Sender: TObject);
var
  i: Integer;
  vPosic: Integer;
  vEmpresas: TArray<RecGruposEstoques>;
begin
  inherited;

  vEmpresas := _GruposEstoques.BuscarGruposEstoques(Sessao.getConexaoBanco, 0, [Sessao.getEmpresaLogada.EmpresaId, 'E']);
  vPosic := coInicioEmpresas;
  SetLength(FEmpresasGrid, Length(vEmpresas));
  for i := Low(vEmpresas) to High(vEmpresas) do begin
    vPosic := vPosic + i;

    sgItens.Cells[vPosic, clLinhaEmpresa]   := NFormat(vEmpresas[i].EmpresaGrupoId) + ' - ' + vEmpresas[i].NomeFantasia;
    FEmpresasGrid[i].EmpresaId := vEmpresas[i].EmpresaGrupoId;
    FEmpresasGrid[i].PosicaoGrid := vPosic;

    sgItens.Cells[vPosic, clLinhaCabecalho] := 'Est.f�sico';
    FEmpresasGrid[i].PosicaoEstFisico := vPosic;

    sgItens.ColCount := sgItens.ColCount + 1;
    sgItens.ColWidths[vPosic] := 80;

    Inc(vPosic);
    sgItens.Cells[vPosic, clLinhaCabecalho] := 'Qtde ent.';
    FEmpresasGrid[i].PosicaoEstQtdeDefinida := vPosic;

    sgItens.ColCount := sgItens.ColCount + 1;
    sgItens.ColWidths[vPosic] := 80;
  end;
end;

procedure TFormDefinirEntregas.sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
var
  i: Integer;
begin
  inherited;
  if ARow < 2 then
    Exit;

  for i := Low(FEmpresasGrid) to High(FEmpresasGrid) do begin
    if ACol = FEmpresasGrid[i].PosicaoEstQtdeDefinida then
      TextCell := NFormatNEstoque(SFormatDouble(TextCell));
  end;
end;

procedure TFormDefinirEntregas.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  i: Integer;
  vAlinhamento: TAlignment;
begin
  inherited;

  if ARow = clLinhaEmpresa then begin
    for i := Low(FEmpresasGrid) to High(FEmpresasGrid) do begin
      if ACol in[FEmpresasGrid[i].PosicaoGrid, FEmpresasGrid[i].PosicaoEstQtdeDefinida] then begin
        sgItens.MergeCells([clLinhaEmpresa, ACol], [clLinhaEmpresa, FEmpresasGrid[i].PosicaoGrid], [clLinhaEmpresa, FEmpresasGrid[i].PosicaoEstQtdeDefinida], taCenter, Rect);
        Exit;
      end;
    end;

    sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taRightJustify, Rect);
    Exit;
  end;

  if ACol in[
    coNomeProduto,
    coUnidade]
  then
    vAlinhamento := taLeftJustify
  else
    vAlinhamento := taRightJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormDefinirEntregas.sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
var
  i: Integer;
begin
  inherited;
  if ARow > clLinhaCabecalho then begin
    for i := Low(FEmpresasGrid) to High(FEmpresasGrid) do begin
      if ACol in[FEmpresasGrid[i].PosicaoGrid, FEmpresasGrid[i].PosicaoEstQtdeDefinida] then begin
        if i = 0 then begin
          AFont.Color  := _Biblioteca.coCorFonteEdicao1;
          ABrush.Color := _Biblioteca.coCorCelulaEdicao1;
        end
        else if i = 1 then begin
          AFont.Color  := _Biblioteca.coCorFonteEdicao2;
          ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
        end
        else if i = 2 then begin
          AFont.Color  := _Biblioteca.coCorFonteEdicao3;
          ABrush.Color := _Biblioteca.coCorCelulaEdicao3;
        end
        else if i = 3 then begin
          AFont.Color  := _Biblioteca.coCorFonteEdicao4;
          ABrush.Color := _Biblioteca.coCorCelulaEdicao4;
        end;
      end;
    end;
  end;
end;

procedure TFormDefinirEntregas.sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
var
  i: Integer;
begin
  inherited;
  if ARow < 2 then
    Exit;

  sgItens.Options := sgItens.Options - [goEditing];

  for i := Low(FEmpresasGrid) to High(FEmpresasGrid) do begin
    if ACol = FEmpresasGrid[i].PosicaoEstQtdeDefinida then begin
      sgItens.Options := sgItens.Options + [goEditing];
      Exit;
    end;
  end;
end;

procedure TFormDefinirEntregas.VerificarRegistro(Sender: TObject);
begin
  inherited;
  // Nada, as valida��es ser�o feitas no finalizar para aproveitar o loop.
end;

procedure TFormDefinirEntregas.Finalizar(Sender: TObject);
var
  i: Integer;
  j: Integer;

  vQtdeDefinida: Double;
  vQtdeEstoque: Double;

  vItensBaixar: TArray<RecEntregasItensPendentes>;
  vItensEntregar: TArray<RecEntregasItensPendentes>;

//  vRetBanco: RecRespostaBanco;
begin
  inherited;
  vItensBaixar := nil;
  vItensEntregar := nil;

  for i := sgItens.FixedRows to sgItens.RowCount -1 do begin
    vQtdeDefinida := 0;
    vQtdeEstoque := 0;
    for j := Low(FEmpresasGrid) to High(FEmpresasGrid) do begin
      SetLength(vItensEntregar, Length(vItensEntregar) + 1);
      vItensEntregar[High(vItensEntregar)].OrcamentoId  := SFormatInt(sgItens.Cells[coOrcamentoId, i]);
      vItensEntregar[High(vItensEntregar)].LocalId      := 0;
      vItensEntregar[High(vItensEntregar)].EmpresaId    := FEmpresasGrid[j].EmpresaId;
      vItensEntregar[High(vItensEntregar)].ProdutoId    := SFormatInt(sgItens.Cells[coProdutoId, i]);
      vItensEntregar[High(vItensEntregar)].ItemId       := SFormatInt(sgItens.Cells[coItemId, i]);
      vItensEntregar[High(vItensEntregar)].Lote         := sgItens.Cells[coLote, i]; // Validar isso aqui, est� errado!!!
      vItensEntregar[High(vItensEntregar)].QtdeEntregar := SFormatDouble( sgItens.Cells[FEmpresasGrid[j].PosicaoEstQtdeDefinida, i] );

      vQtdeDefinida := vQtdeDefinida + SFormatDouble( sgItens.Cells[FEmpresasGrid[j].PosicaoEstQtdeDefinida, i] );
      vQtdeEstoque := vQtdeEstoque + SFormatDouble( sgItens.Cells[FEmpresasGrid[j].PosicaoEstFisico, i] );
    end;

    if sgItens.Cells[coQuantidade, i] <> NFormatEstoque(vQtdeDefinida) then begin
      _Biblioteca.Exclamar('A quantidade a entregar para o produto ' + sgItens.Cells[coNomeProduto, i] + ' n�o foi bem definida, verifique!');
      sgItens.Row := i;
      Abort;
    end;

    if
      (sgItens.Cells[coAceitarEstoqueNegativo, i] = 'N') and
      (_Biblioteca.Arredondar(vQtdeDefinida, getCasasDecimaisEstoque) > _Biblioteca.Arredondar( vQtdeEstoque, getCasasDecimaisEstoque))
    then begin
      _Biblioteca.Exclamar('O produto ' + sgItens.Cells[coNomeProduto, i] + ' n�o pode ter estoque negativo, verifique!');
      sgItens.Row := i;
      Abort;
    end;

    (* Setando o item para baixar a pend�ncia de estoque *)
    SetLength(vItensBaixar, Length(vItensBaixar) + 1);
    vItensBaixar[High(vItensBaixar)].OrcamentoId  := SFormatInt(sgItens.Cells[coOrcamentoId, i]);
    vItensBaixar[High(vItensBaixar)].EmpresaId    := SFormatInt(sgItens.Cells[coEmpresaPendId, i]);
    vItensBaixar[High(vItensBaixar)].LocalId      := SFormatInt(sgItens.Cells[coLocalId, i]);
    vItensBaixar[High(vItensBaixar)].ProdutoId    := SFormatInt(sgItens.Cells[coProdutoId, i]);
    vItensBaixar[High(vItensBaixar)].ItemId       := SFormatInt(sgItens.Cells[coItemId, i]);
    vItensBaixar[High(vItensBaixar)].Lote         := sgItens.Cells[coLote, i];
    vItensBaixar[High(vItensBaixar)].QtdeEntregar := vQtdeDefinida; // Validar isso aqui, est� errado!!!
  end;


end;

end.
