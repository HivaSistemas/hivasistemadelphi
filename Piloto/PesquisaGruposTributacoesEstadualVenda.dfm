inherited FormPesquisaGruposTributacoesEstadualVenda: TFormPesquisaGruposTributacoesEstadualVenda
  Caption = 'Pesquisa de grupos de tributa'#231#245'es de vendas'
  ClientHeight = 269
  ClientWidth = 505
  ExplicitWidth = 513
  ExplicitHeight = 300
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 505
    Height = 223
    ColCount = 4
    OnDrawCell = sgPesquisaDrawCell
    CorLinhaFoco = 38619
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Descri'#231#227'o'
      'Ativo?')
    RealColCount = 4
    ExplicitWidth = 505
    ExplicitHeight = 223
    ColWidths = (
      28
      55
      285
      66)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Text = '0'
  end
  inherited pnFiltro1: TPanel
    Width = 505
    ExplicitWidth = 505
    inherited lblPesquisa: TLabel
      Width = 167
      Caption = 'Grupo de tributacoes estadual'
      ExplicitWidth = 167
    end
    inherited eValorPesquisa: TEditLuka
      Width = 301
      ExplicitWidth = 301
    end
  end
end
