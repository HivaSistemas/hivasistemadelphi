unit RecebimentoPedidos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  _HerancaRelatorios, Vcl.Buttons, _RecordsOrcamentosVendas, Vcl.ExtCtrls, Vcl.Grids, GridLuka, Vcl.StdCtrls, _Sessao, System.Math,
  _RecordsEspeciais, _Orcamentos, Vcl.ComCtrls, _Biblioteca, _FrameHerancaPrincipal, BuscarDadosRecebimentoPedido,
  _RecordsNotasFiscais, _NotasFiscais, System.StrUtils, _ComunicacaoNFE, _RecordsNFE, _ImpressaoDANFE, ReceberBaixaContasReceber,
  Informacoes.Orcamento, SpeedButtonLuka, _RecordsFinanceiros, _ContasReceberBaixas, Informacoes.ContasReceberBaixa,
  Vcl.Menus, Legendas, _Acumulados, BuscarDadosRecebimentoAcumulado, InformacoesAcumulado, ImpressaoRecebimentoNaEntregaGrafico,
  CheckBoxLuka, Buscar.CpfConsumidorFinal;

type
  TFormRecebimentoPedidos = class(TFormHerancaRelatorios)
    pcDados: TPageControl;
    tsPedidos: TTabSheet;
    tsNotasAguardandoEmissao: TTabSheet;
    sgNotasAguardandoEmissao: TGridLuka;
    pmOpcoesPedidos: TPopupMenu;
    miConsultarStatusServicoNFe: TMenuItem;
    pmOpcoesTitulos: TPopupMenu;
    miVoltarFinanceiro: TMenuItem;
    tsAcumulados: TTabSheet;
    sgAcumulados: TGridLuka;
    pmOpcoesAcumulados: TPopupMenu;
    miCancelarFechamentoAcumulado: TMenuItem;
    miReceberPedidoSelecionado: TMenuItem;
    miN2: TMenuItem;
    miReceberBaixaSelecionada: TMenuItem;
    miN3: TMenuItem;
    pmOpcoesNotas: TPopupMenu;
    miEmitirDocumentoFiscalPendente: TMenuItem;
    tsTitulosFinanceiro: TTabSheet;
    sgBaixas: TGridLuka;
    miReceberAcumuladoSelecionado: TMenuItem;
    miN5: TMenuItem;
    PageControl1: TPageControl;
    tsAguardandoRecebimento: TTabSheet;
    sgOrcamentos: TGridLuka;
    tsAguardandoRecebimentoEntrega: TTabSheet;
    sgPedidosReceberEntrega: TGridLuka;
    pmOpcoesReceberNaEntrega: TPopupMenu;
    miReceberEntregaSelecionada: TMenuItem;
    miN6: TMenuItem;
    miReimprimirdocrecebimentonaentrega1: TMenuItem;
    miN7: TMenuItem;
    miCancelarfechamentodepedido1: TMenuItem;
    miCancelarFechamentoPedido: TSpeedButton;
    miLegendasPedidos: TSpeedButton;
    miGerarDocumentoRecebimentoNaEntrega: TSpeedButton;
    sbCpfNota: TSpeedButton;
    procedure sgOrcamentosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgNotasAguardandoEmissaoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgNotasAguardandoEmissaoGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure FormCreate(Sender: TObject);
    procedure sgOrcamentosDblClick(Sender: TObject);
    procedure sgOrcamentosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgNotasAguardandoEmissaoDblClick(Sender: TObject);
    procedure sgBaixasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgBaixasDblClick(Sender: TObject);
    procedure miConsultarStatusServicoNFeClick(Sender: TObject);
    procedure miVoltarFinanceiroClick(Sender: TObject);
    procedure miReceberPedidoSelecionadoClick(Sender: TObject);
    procedure miReceberBaixaSelecionadaClick(Sender: TObject);
    procedure miEmitirDocumentoFiscalPendenteClick(Sender: TObject);
    procedure sgOrcamentosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure miLegendasPedidosClick(Sender: TObject);
    procedure sgAcumuladosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure miCancelarFechamentoAcumuladoClick(Sender: TObject);
    procedure miReceberAcumuladoSelecionadoClick(Sender: TObject);
    procedure sgAcumuladosDblClick(Sender: TObject);
    procedure miGerarDocumentoRecebimentoNaEntregaClick(Sender: TObject);
    procedure sgPedidosReceberEntregaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgPedidosReceberEntregaDblClick(Sender: TObject);
    procedure miReceberEntregaSelecionadaClick(Sender: TObject);
    procedure miReimprimirdocrecebimentonaentrega1Click(Sender: TObject);
    procedure miCancelarfechamentodepedido1Click(Sender: TObject);
    procedure miCancelarFechamentoPedidoClick(Sender: TObject);
    procedure sbCpfNotaClick(Sender: TObject);
  private
    FPedidos: TArray<RecOrcamentos>;
    FPedidosReceberEntrega: TArray<RecOrcamentos>;

    FAcumulados: TArray<RecAcumulados>;

    FLegendaGridPedido: TImage;

    function LengendaGridPedidos(
      pPedidoBloqueado: Boolean;
      pReceberNaEntrega: Boolean
    ): TPicture;
  protected
    procedure Carregar(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

const
  // Grid de Pedidos
  coLegendas          = 0;
  coOrcamentoId       = 1;
  coCliente           = 2;
  coVendedor          = 3;
  coDataPedido        = 4;
  coTipoEntrega       = 5;
  coValorTotal        = 6;
  coCondicaoPagamento = 7;
  coStatus            = 8;

  // Grid de recebimentos na entrega
  cagPedidoId          = 0;
  cagCliente           = 1;
  cagVendedor          = 2;
  cagDataPedido        = 3;
  cagValorTotal        = 4;
  cagCondicaoPagamento = 5;

  // Grid de Acumulados
  caLegendas          = 0;
  caAcumuladoId       = 1;
  caCliente           = 2;
  caDataFechamento    = 3;
  caUsuarioFechamento = 4;
  caValorTotal        = 5;
  caCondicaoPagamento = 6;

  // Grid de Titulos vindos do financeiro
  ctBaixaId      = 0;
  ctTipo         = 1;
  ctCliente      = 2;
  ctValor        = 3;
  ctDataEnvio    = 4;
  ctUsuarioEnvio = 5;

  // Grid de notas de pedidos ainda n�o emitidas
  ceOrcamentoId       = 0;
  ceCliente           = 1;
  ceValorTotal        = 2;
  ceDataPedido        = 3;
  ceTipoNota          = 4;
  ceVendedor          = 5;
  ceCondicaoPagamento = 6;
  ceNotaFiscalId      = 7;
  ceStatusProcLoteNfe = 8;

procedure TFormRecebimentoPedidos.Carregar(Sender: TObject);
var
  i: Integer;
  vNotasEmitir: TArray<RecNotaFiscal>;
  vBaixas: TArray<RecContaReceberBaixa>;
begin
  inherited;
  FPedidos := nil;
  FAcumulados := nil;

  sgBaixas.ClearGrid;
  sgOrcamentos.ClearGrid;
  sgAcumulados.ClearGrid;
  sgPedidosReceberEntrega.ClearGrid;
  sgNotasAguardandoEmissao.ClearGrid;

  // Grid de or�amentos para serem recebidos
  FPedidos := _Orcamentos.BuscarOrcamentos(Sessao.getConexaoBanco, 1, [Sessao.getEmpresaLogada.EmpresaId]);
  for i := Low(FPedidos) to High(FPedidos) do begin
    sgOrcamentos.Cells[coOrcamentoId, i + 1]       := NFormat(FPedidos[i].orcamento_id);
    sgOrcamentos.Cells[coCliente, i + 1]           := NFormat(FPedidos[i].cliente_id) + ' - ' + FPedidos[i].nome_cliente;
    sgOrcamentos.Cells[coDataPedido, i + 1]        := DFormat(FPedidos[i].data_cadastro);
    sgOrcamentos.Cells[coTipoEntrega, i + 1]       := FPedidos[i].TipoEntregaAnalitico;
    sgOrcamentos.Cells[coValorTotal, i + 1]        := NFormat(FPedidos[i].valor_total);
    sgOrcamentos.Cells[coCondicaoPagamento, i + 1] := NFormat(FPedidos[i].condicao_id) + ' - ' + FPedidos[i].nome_condicao_pagto;
    sgOrcamentos.Cells[coVendedor, i + 1]          := NFormat(FPedidos[i].vendedor_id) + ' - ' + FPedidos[i].nome_vendedor;
    sgOrcamentos.Cells[coStatus, i + 1]            := FPedidos[i].status;
  end;
  sgOrcamentos.SetLinhasGridPorTamanhoVetor( Length(FPedidos) );

  FPedidosReceberEntrega := _Orcamentos.BuscarOrcamentos(Sessao.getConexaoBanco, 8, [Sessao.getEmpresaLogada.EmpresaId]);
  for i := Low(FPedidosReceberEntrega) to High(FPedidosReceberEntrega) do begin
    sgPedidosReceberEntrega.Cells[cagPedidoId, i + 1]          := NFormat(FPedidosReceberEntrega[i].orcamento_id);
    sgPedidosReceberEntrega.Cells[cagCliente, i + 1]           := getInformacao(FPedidosReceberEntrega[i].cliente_id, FPedidosReceberEntrega[i].nome_cliente);
    sgPedidosReceberEntrega.Cells[cagVendedor, i + 1]          := getInformacao(FPedidosReceberEntrega[i].vendedor_id, FPedidosReceberEntrega[i].nome_vendedor);
    sgPedidosReceberEntrega.Cells[cagDataPedido, i + 1]        := DFormat(FPedidosReceberEntrega[i].data_cadastro);
    sgPedidosReceberEntrega.Cells[cagValorTotal, i + 1]        := NFormat(FPedidosReceberEntrega[i].valor_total);
    sgPedidosReceberEntrega.Cells[cagCondicaoPagamento, i + 1] := getInformacao(FPedidosReceberEntrega[i].condicao_id, FPedidosReceberEntrega[i].nome_condicao_pagto);
  end;
  sgPedidosReceberEntrega.SetLinhasGridPorTamanhoVetor( Length(FPedidosReceberEntrega) );

  // Grid de acumulados
  FAcumulados := _Acumulados.BuscarAcumulados(Sessao.getConexaoBanco, 1, [Sessao.getEmpresaLogada.EmpresaId]);
  for i := Low(FAcumulados) to High(FAcumulados) do begin
    sgAcumulados.Cells[caAcumuladoId, i + 1]       := NFormat(FAcumulados[i].AcumuladoId);
    sgAcumulados.Cells[caCliente, i + 1]           := getInformacao(FAcumulados[i].ClienteId, FAcumulados[i].NomeCliente);
    sgAcumulados.Cells[caDataFechamento, i + 1]    := DFormat(FAcumulados[i].DataHoraFechamento);
    sgAcumulados.Cells[caUsuarioFechamento, i + 1] := getInformacao(FAcumulados[i].UsuarioFechamentoId, FAcumulados[i].NomeUsuarioFechamento);
    sgAcumulados.Cells[caValorTotal, i + 1]        := NFormat(FAcumulados[i].ValorTotal);
    sgAcumulados.Cells[caCondicaoPagamento, i + 1] := getInformacao(FAcumulados[i].CondicaoId, FAcumulados[i].NomeCondicaoPagamento);
  end;
  sgAcumulados.SetLinhasGridPorTamanhoVetor( Length(FAcumulados) );

  // Grid de baixas de t�tulos
  vBaixas := _ContasReceberBaixas.BuscarContaReceberBaixas(Sessao.getConexaoBanco, 1, [Sessao.getEmpresaLogada.EmpresaId]);
  for i := Low(vBaixas) to High(vBaixas) do begin
    sgBaixas.Cells[ctBaixaId, i + 1]      := NFormat(vBaixas[i].BaixaId);
    sgBaixas.Cells[ctCliente, i + 1]      := NFormat(vBaixas[i].CadastroId) + ' - ' + vBaixas[i].NomeCliente;
    sgBaixas.Cells[ctValor, i + 1]        := NFormat(vBaixas[i].ValorLiquido);
    sgBaixas.Cells[ctDataEnvio, i + 1]    := DFormat(vBaixas[i].data_hora_baixa);
    sgBaixas.Cells[ctUsuarioEnvio, i + 1] := NFormat(vBaixas[i].UsuarioEnvioCaixaId) + ' - ' + vBaixas[i].NomeUsuarioEnvCaixa;
  end;
  sgBaixas.SetLinhasGridPorTamanhoVetor( Length(vBaixas) );

  // Grid de Orcs aguardando emiss�o da NF
  vNotasEmitir := _NotasFiscais.BuscarNotasFiscais(Sessao.getConexaoBanco, 2, [Sessao.getEmpresaLogada.EmpresaId]);
  for i := Low(vNotasEmitir) to High(vNotasEmitir) do begin
    sgNotasAguardandoEmissao.Cells[ceOrcamentoId, i + 1]       := NFormat(vNotasEmitir[i].orcamento_id);
    sgNotasAguardandoEmissao.Cells[ceCliente, i + 1]           := vNotasEmitir[i].nome_fantasia_destinatario;
    sgNotasAguardandoEmissao.Cells[ceValorTotal, i + 1]        := NFormat(vNotasEmitir[i].valor_total);
    sgNotasAguardandoEmissao.Cells[ceDataPedido, i + 1]        := '';//DateToStr(vNotasEmitir[i].data_cadastro);
    sgNotasAguardandoEmissao.Cells[ceTipoNota, i + 1]          := vNotasEmitir[i].TipoNotaAnalitico;
    sgNotasAguardandoEmissao.Cells[ceVendedor, i + 1]          := '';
    sgNotasAguardandoEmissao.Cells[ceCondicaoPagamento, i + 1] := '';
    sgNotasAguardandoEmissao.Cells[ceNotaFiscalId, i + 1]      := NFormat(vNotasEmitir[i].nota_fiscal_id);
    sgNotasAguardandoEmissao.Cells[ceStatusProcLoteNfe, i + 1] := vNotasEmitir[i].status_nfe;
  end;
  sgNotasAguardandoEmissao.RowCount := IfThen(Length(vNotasEmitir) > 1, High(vNotasEmitir) + 2, 2);

  if FPedidos <> nil then
    SetarFoco(sgOrcamentos)
  else if FPedidosReceberEntrega <> nil then
    SetarFoco(sgPedidosReceberEntrega)
  else if FAcumulados <> nil then
    SetarFoco(sgAcumulados)
  else if vBaixas <> nil then
    SetarFoco(sgBaixas)
  else if vNotasEmitir <> nil then
    SetarFoco(sgNotasAguardandoEmissao)
  else
    SetarFoco(sgOrcamentos);
end;

procedure TFormRecebimentoPedidos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  FLegendaGridPedido.Free;
end;

procedure TFormRecebimentoPedidos.FormCreate(Sender: TObject);
begin
  inherited;
  if Sessao.getEmpresaLogada.TipoEmpresa = 'D' then begin
    _Biblioteca.RotinaNaoPermitidaDepositoFechado;
    Close;
  end;

  AtivarAnaliseCusto(Sessao.getParametros.UtilizarAnaliseCusto);

  pcDados.ActivePage := tsPedidos;
  miConsultarStatusServicoNFe.Enabled :=
    ( (Sessao.getParametrosEmpresa.UtilizaNfe = 'S') or (Sessao.getParametrosEmpresa.UtilizaNFCe = 'S') );

  FLegendaGridPedido := TImage.Create(Self);
  FLegendaGridPedido.Height := 15;

  sgAcumulados.OcultarColunas([caLegendas]);
end;

procedure TFormRecebimentoPedidos.Imprimir(Sender: TObject);
var
  vId: Integer;
  vNFe: TComunicacaoNFe;
begin
  inherited;
  if pcDados.ActivePage = tsNotasAguardandoEmissao then begin
    vId := SFormatInt(sgNotasAguardandoEmissao.Cells[ceNotaFiscalId, sgNotasAguardandoEmissao.Row]);
    if vId = 0 then
      Exit;

    vNFe := TComunicacaoNFe.Create(Self);

    if
      vNFe.EmitirNFe(vId, True)
    then
      Carregar(Sender);

    vNFe.Free;
  end;
end;

function TFormRecebimentoPedidos.LengendaGridPedidos(
  pPedidoBloqueado: Boolean;
  pReceberNaEntrega: Boolean
): TPicture;
var
	p: Integer;
  l: Integer;
begin
 	l := 0;

  if pPedidoBloqueado then
    Inc(l, 10);

  if pReceberNaEntrega then
    Inc(l, 10);

  p := 1;
  FLegendaGridPedido.Width := l + p;
  FLegendaGridPedido.Picture := nil;

  if pPedidoBloqueado then begin
  	FLegendaGridPedido.Canvas.Pen.Color := clMaroon;
  	FLegendaGridPedido.Canvas.Brush.Color := clRed;
  	FLegendaGridPedido.Canvas.Rectangle(p, 0, p + 5, 15);
    Inc(p, 10);
  end;

  if pReceberNaEntrega then begin
  	FLegendaGridPedido.Canvas.Pen.Color := $000096DB;
  	FLegendaGridPedido.Canvas.Brush.Color := clGreen;
  	FLegendaGridPedido.Canvas.Rectangle(p, 0, p + 5, 15);
//    Inc(p, 10);
  end;

  Result := FLegendaGridPedido.Picture;
end;

procedure TFormRecebimentoPedidos.miConsultarStatusServicoNFeClick(Sender: TObject);
var
  vNFe: TComunicacaoNFe;
  vRet: RecRespostaNFE;
begin
  inherited;
  if (Sessao.getParametrosEmpresa.UtilizaNfe = 'N') and (Sessao.getParametrosEmpresa.UtilizaNFCe = 'N') then begin
    _Biblioteca.Exclamar('A empresa n�o est� parametrizada para utilizar NFe!');
    Exit;
  end;

  vNFe := TComunicacaoNFe.Create(Application);
  vRet := vNFe.ConsultarStatusServico;
  if vRet.HouveErro then
    _Biblioteca.Exclamar(vRet.mensagem_erro)
  else begin
    _Biblioteca.Informar(
      'Ambiente NFE: ' + vRet.tipo_ambiente + Chr(13) + Chr(10) +
      LPad('UF: ', 13) +  vRet.uf + Chr(13) + Chr(10)+
      LPad('Status: ', 13) + vRet.motivo + Chr(13) + Chr(10) +
      LPad('Observa��o.: ', 13) + vRet.observacao
    );
  end;
  FreeAndNil(vNFe);
end;

procedure TFormRecebimentoPedidos.miEmitirDocumentoFiscalPendenteClick(Sender: TObject);
var
  vId: Integer;
  vNFe: TComunicacaoNFe;
begin
  inherited;
  if pcDados.ActivePage = tsNotasAguardandoEmissao then begin
    vId := SFormatInt(sgNotasAguardandoEmissao.Cells[ceNotaFiscalId, sgNotasAguardandoEmissao.Row]);
    if vId = 0 then
      Exit;

    vNFe := TComunicacaoNFe.Create(Self);

    if
      vNFe.EmitirNFe(vId, True)
    then
      Carregar(Sender);

    vNFe.Free;
  end;
end;

procedure TFormRecebimentoPedidos.miGerarDocumentoRecebimentoNaEntregaClick(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]) = 0 then
    Exit;

  if Sessao.getTurnoCaixaAberto = nil then begin
    _Biblioteca.Exclamar('N�o existe turno em aberto para este usu�rio!');
    Exit;
  end;

  if FPedidos[sgOrcamentos.Row - 1].status = 'VB' then begin
    _Biblioteca.Exclamar('Este or�amento est� bloqueado, � necess�rio primeiro fazer sua libera��o!');
    Exit;
  end;

  if FPedidos[sgOrcamentos.Row - 1].ReceberNaEntrega = 'N' then begin
    _Biblioteca.Exclamar('O pedido selecionado n�o � para recebimento na entrega!');
    Exit;
  end;

  vRetBanco := _Orcamentos.LiberarPedidoRecebimentoNaEntrega(Sessao.getConexaoBanco, SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]));
  Sessao.AbortarSeHouveErro(vRetBanco);

  ImpressaoRecebimentoNaEntregaGrafico.Imprimir(SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]));
  Carregar(Sender);
end;

procedure TFormRecebimentoPedidos.miLegendasPedidosClick(Sender: TObject);
var
  vLegendas: TFormLegendas;
begin
  inherited;
  vLegendas := TFormLegendas.Create(Self);

  vLegendas.AddLegenda(clRed, 'Venda bloqueada', clWhite);
  vLegendas.AddLegenda(clGreen, 'Receber na entrega', clWhite);

  vLegendas.Show;
end;

procedure TFormRecebimentoPedidos.miReceberAcumuladoSelecionadoClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar<RecRetornoRecebimento>;
begin
  inherited;
  if Sessao.getTurnoCaixaAberto = nil then begin
    _Biblioteca.Exclamar('N�o existe turno em aberto para este usu�rio!');
    Exit;
  end;

  if SFormatInt(sgAcumulados.Cells[caAcumuladoId, sgAcumulados.Row]) = 0 then
    Exit;

  vRetTela := BuscarDadosRecebimentoAcumulado.BuscarDadosRecebimento(FAcumulados[sgAcumulados.Row - 1]);
  if vRetTela.RetTela = trCancelado then
    Exit;

  if not vRetTela.Dados.EmitirDocumentoFiscal then begin
    Carregar(Sender);
    Exit;
  end;

  if Em(vRetTela.Dados.TipoNota, ['NE', 'NF']) then begin
    if not _ComunicacaoNFE.EnviarNFe(vRetTela.Dados.NotasFiscaisIds[0]) then begin
      Exclamar(
        'Ocorreu um problema ao tentar emitir a nota fiscal eletr�nica automaticamente!' + Chr(13) + Chr(10) +
        'Fa�a a emiss�o pela aba "Notas aguardando emiss�o"'
      );
    end;
  end;
  Carregar(Sender);
end;

procedure TFormRecebimentoPedidos.miReceberBaixaSelecionadaClick(Sender: TObject);
begin
  inherited;
  if Sessao.getTurnoCaixaAberto = nil then begin
    _Biblioteca.Exclamar('N�o existe turno em aberto para este usu�rio!');
    Exit;
  end;

  if ReceberBaixaContasReceber.Receber( SFormatInt(sgBaixas.Cells[ctBaixaId, sgBaixas.Row]) ).RetTela = trOk then
    Carregar(Sender);
end;

procedure TFormRecebimentoPedidos.miReceberPedidoSelecionadoClick(Sender: TObject);
var
  i: Integer;
  vRetornoRec: TRetornoTelaFinalizar<RecRetornoRecebimento>;
begin
  inherited;
  if SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]) = 0 then
    Exit;

  if Sessao.getTurnoCaixaAberto = nil then begin
    _Biblioteca.Exclamar('N�o existe turno em aberto para este usu�rio!');
    Exit;
  end;

  if FPedidos[sgOrcamentos.Row - 1].status = 'VB' then begin
    _Biblioteca.Exclamar('Este or�amento est� bloqueado, � necess�rio primeiro fazer sua libera��o!');
    Exit;
  end;

  if FPedidos[sgOrcamentos.Row - 1].ReceberNaEntrega = 'S' then begin
    _Biblioteca.Exclamar('O pedido selecionado � um recebimento na entrega, por favor fa�a a gera��o dos documentos de recebimento!');
    Exit;
  end;

  vRetornoRec := BuscarDadosRecebimentoPedido.BuscarDadosRecebimento(FPedidos[sgOrcamentos.Row - 1]);
  if vRetornoRec.RetTela = trCancelado then begin
    RotinaCanceladaUsuario;
    Exit;
  end;

  if not vRetornoRec.Dados.EmitirDocumentoFiscal then begin
    Carregar(Sender);
    Exit;
  end;

  if Em(vRetornoRec.Dados.TipoNota, ['NE', 'NF']) then begin
    for i := Low(vRetornoRec.Dados.NotasFiscaisIds) to High(vRetornoRec.Dados.NotasFiscaisIds) do begin
      if not _ComunicacaoNFE.EnviarNFe(vRetornoRec.Dados.NotasFiscaisIds[i]) then begin
        Exclamar(
          'Ocorreu um problema ao tentar emitir a nota fiscal eletr�nica automaticamente!' + Chr(13) + Chr(10) +
          'Fa�a a emiss�o pela aba "Notas aguardando emiss�o"'
        );

        Break;
      end;
    end;
  end;
  Carregar(Sender);
end;

procedure TFormRecebimentoPedidos.miReimprimirdocrecebimentonaentrega1Click(Sender: TObject);
begin
  inherited;
  ImpressaoRecebimentoNaEntregaGrafico.Imprimir(SFormatInt(sgPedidosReceberEntrega.Cells[cagPedidoId, sgPedidosReceberEntrega.Row]));
end;

procedure TFormRecebimentoPedidos.miVoltarFinanceiroClick(Sender: TObject);
var
  vId: Integer;
  vRetorno: RecRetornoBD;
begin
  inherited;
  vId := SFormatInt(sgBaixas.Cells[ctBaixaId, sgBaixas.Row]);
  if vId = 0 then
    Exit;

  if not Perguntar('Deseja realmente voltar esta baixa para o financeiro?') then
    Exit;

  vRetorno := _ContasReceberBaixas.CancelarBaixa(Sessao.getConexaoBanco, vId, True);
  if vRetorno.TeveErro then begin
    Exclamar(vRetorno.MensagemErro);
    Exit;
  end;

  Carregar(Sender);
end;

procedure TFormRecebimentoPedidos.miReceberEntregaSelecionadaClick(Sender: TObject);
var
  vRetornoRec: TRetornoTelaFinalizar<RecRetornoRecebimento>;
begin
  inherited;
  if SFormatInt(sgPedidosReceberEntrega.Cells[cagPedidoId, sgPedidosReceberEntrega.Row]) = 0 then
    Exit;

  if Sessao.getTurnoCaixaAberto = nil then begin
    _Biblioteca.Exclamar('N�o existe turno em aberto para este usu�rio!');
    Exit;
  end;

  vRetornoRec := BuscarDadosRecebimentoPedido.BuscarDadosRecebimento(FPedidosReceberEntrega[sgPedidosReceberEntrega.Row - 1]);
  if vRetornoRec.BuscaCancelada then begin
    RotinaCanceladaUsuario;
    Exit;

  end;

  Carregar(Sender);
end;

procedure TFormRecebimentoPedidos.miCancelarFechamentoAcumuladoClick(Sender: TObject);
var
  vAcumuladoId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vAcumuladoId := SFormatInt(sgAcumulados.Cells[caAcumuladoId, sgAcumulados.Row]);
  if vAcumuladoId = 0 then
    Exit;

  if not Perguntar('Deseja realmente cancelar o fechamento deste acumulado?') then
    Exit;

  vRetBanco := _Acumulados.CancelarFechamentoAcumulado(Sessao.getConexaoBanco, vAcumuladoId);
  Sessao.AbortarSeHouveErro(vRetBanco);

  Carregar(Sender);
end;

procedure TFormRecebimentoPedidos.miCancelarfechamentodepedido1Click(Sender: TObject);
var
  vId: Integer;
  vRetorno: RecRetornoBD;
begin
  inherited;
  vId := SFormatInt(sgPedidosReceberEntrega.Cells[cagPedidoId, sgPedidosReceberEntrega.Row]);
  if vId = 0 then
    Exit;

  if not Sessao.AutorizadoRotina('cancelar_fec_pedido_recimento_entrega') then begin
    _Biblioteca.NaoAutorizadoRotina;
    Exit;
  end;

  if not Perguntar('Deseja voltar esta venda para or�amento?') then
    Exit;

  vRetorno := _Orcamentos.CancelarFechamentoPedido(Sessao.getConexaoBanco, vId);
  if vRetorno.TeveErro then begin
    Exclamar(vRetorno.MensagemErro);
    Exit;
  end;

  Carregar(Sender);
end;

procedure TFormRecebimentoPedidos.miCancelarFechamentoPedidoClick(Sender: TObject);
var
  vId: Integer;
  vRetorno: RecRetornoBD;
begin
  inherited;
  vId := SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]);
  if vId = 0 then
    Exit;

  if not Sessao.AutorizadoRotina('voltar_pedido_para_orcamento') then begin
    _Biblioteca.NaoAutorizadoRotina;
    Exit;
  end;

  if not Perguntar('Deseja voltar esta venda para or�amento?') then
    Exit;

  vRetorno := _Orcamentos.CancelarFechamentoPedido(Sessao.getConexaoBanco, vId);
  if vRetorno.TeveErro then begin
    Exclamar(vRetorno.MensagemErro);
    Exit;
  end;

  Carregar(Sender);
end;

procedure TFormRecebimentoPedidos.sgNotasAguardandoEmissaoDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar( SFormatInt(sgNotasAguardandoEmissao.Cells[ceOrcamentoId, sgNotasAguardandoEmissao.Row]) );
end;

procedure TFormRecebimentoPedidos.sgNotasAguardandoEmissaoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  alinhamento: TAlignment;
begin
  inherited;

  if ACol in[ceOrcamentoId, ceValorTotal] then
    alinhamento := taRightJustify
  else if ACol = ceTipoNota then
    alinhamento := taCenter
  else
    alinhamento := taLeftJustify;

  sgNotasAguardandoEmissao.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], alinhamento, Rect);
end;

procedure TFormRecebimentoPedidos.sgNotasAguardandoEmissaoGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = ceTipoNota then begin
    AFont.Style := [fsBold];
    AFont.Color := IfThen(sgNotasAguardandoEmissao.Cells[ACol, ARow] = 'NF-e', clBlue, $000096DB);
  end;

  if sgNotasAguardandoEmissao.Cells[ceStatusProcLoteNfe, ARow] = '103' then
    ABrush.Color := $00FFFFDD;
end;

procedure TFormRecebimentoPedidos.sgOrcamentosDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar( SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]) );
end;

procedure TFormRecebimentoPedidos.sgOrcamentosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coOrcamentoId, coValorTotal] then
    vAlinhamento := taRightJustify
  else if ACol = coTipoEntrega then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgOrcamentos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRecebimentoPedidos.sgOrcamentosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgOrcamentos.Cells[coOrcamentoId, 1] = '' then
    Exit;

  if FPedidos[ARow - 1].ReceberNaEntrega = 'S' then begin
    ABrush.Color := $00ECFFEC;
  end;

  if ACol = coTipoEntrega then begin
    if sgOrcamentos.Cells[coTipoEntrega, ARow] = 'Retirar no ato' then
      AFont.Color := coCorFonteEdicao1
    else if sgOrcamentos.Cells[coTipoEntrega, ARow] = 'Retirar' then
      AFont.Color := coCorFonteEdicao2
    else if sgOrcamentos.Cells[coTipoEntrega, ARow] = 'Entregar' then
      AFont.Color := coCorFonteEdicao3
    else
      AFont.Color := coCorFonteEdicao4;
  end;
end;

procedure TFormRecebimentoPedidos.sgOrcamentosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgOrcamentos.Cells[coOrcamentoId, ARow] = '' then
    Exit;

  if ACol = coLegendas then begin
    APicture :=
      LengendaGridPedidos(
        FPedidos[ARow - 1].status = 'VB',
        FPedidos[ARow - 1].ReceberNaEntrega = 'S'
      );
  end;
end;

procedure TFormRecebimentoPedidos.sgPedidosReceberEntregaDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar( SFormatInt(sgPedidosReceberEntrega.Cells[cagPedidoId, sgPedidosReceberEntrega.Row]) );
end;

procedure TFormRecebimentoPedidos.sgPedidosReceberEntregaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[cagPedidoId, cagValorTotal] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgPedidosReceberEntrega.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRecebimentoPedidos.sbCpfNotaClick(Sender: TObject);
var
  cpf_consumidor_final: string;
  vRetBanco: RecRetornoBD;
begin
  inherited;

  if pcDados.ActivePage <> tsPedidos then begin
    Exclamar('Selecione um pedido na aba Pedidos -> Aguardando recebimento.');
    Exit;
  end;

  if PageControl1.ActivePage <> tsAguardandoRecebimento then begin
    Exclamar('Selecione um pedido na aba Pedidos -> Aguardando recebimento.');
    Exit;
  end;

  if FPedidos[sgOrcamentos.Row - 1].cliente_id <> Sessao.getParametros.cadastro_consumidor_final_id then begin
    Exclamar('S� � permitido informar CPF na nota para cliente consumidor final.');
    Exit;
  end;

  cpf_consumidor_final := _Orcamentos.BuscarCpfConsumidorFinal(
    Sessao.getConexaoBanco,
    FPedidos[sgOrcamentos.Row - 1].orcamento_id
  );

  cpf_consumidor_final := Buscar.CpfConsumidorFinal.Buscar(cpf_consumidor_final);

  vRetBanco := _Orcamentos.AtualizarCpfConsumidorFinal(
    Sessao.getConexaoBanco,
    FPedidos[sgOrcamentos.Row - 1].orcamento_id,
    cpf_consumidor_final
  );

  Sessao.AbortarSeHouveErro( vRetBanco );
end;

procedure TFormRecebimentoPedidos.sgAcumuladosDblClick(Sender: TObject);
begin
  inherited;
  InformacoesAcumulado.Informar( SFormatInt(sgAcumulados.Cells[caAcumuladoId, sgAcumulados.Row]) );
end;

procedure TFormRecebimentoPedidos.sgAcumuladosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[caAcumuladoId, caValorTotal] then
    vAlinhamento := taRightJustify
  else if ACol = caLegendas then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgAcumulados.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRecebimentoPedidos.sgBaixasDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.ContasReceberBaixa.Informar( SFormatInt(sgBaixas.Cells[ctBaixaId, sgBaixas.Row]) );
end;

procedure TFormRecebimentoPedidos.sgBaixasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[ctBaixaId, ctValor] then
    vAlinhamento := taRightJustify
  else if ACol = ctTipo then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgBaixas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
