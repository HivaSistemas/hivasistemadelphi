inherited FormEmissaoBoletoBancario: TFormEmissaoBoletoBancario
  Caption = 'Emiss'#227'o de boletos banc'#225'rio'
  ClientHeight = 338
  ClientWidth = 679
  Visible = True
  OnShow = FormShow
  ExplicitWidth = 685
  ExplicitHeight = 367
  PixelsPerInch = 96
  TextHeight = 14
  object sbImprimir: TSpeedButton [0]
    Left = 2
    Top = 274
    Width = 98
    Height = 26
    Caption = '&Imprimir'
    OnClick = sbImprimirClick
  end
  inherited pnOpcoes: TPanel
    Top = 301
    Width = 679
    ExplicitTop = 301
    ExplicitWidth = 679
  end
  object sgBoletos: TGridLuka
    Left = 2
    Top = 2
    Width = 674
    Height = 271
    ColCount = 10
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 1
    OnDrawCell = sgBoletosDrawCell
    OnKeyDown = sgBoletosKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Imp?'
      'Receber'
      'Documento'
      'Cliente'
      'Nome'
      'Data vencto.'
      'Valor'
      'Parcela'
      'Empresa'
      'Nome empresa')
    OnGetCellPicture = sgBoletosGetCellPicture
    Grid3D = False
    RealColCount = 20
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      46
      56
      89
      49
      164
      81
      73
      64
      54
      157)
  end
  object ckVisualizarAntesImprimir: TCheckBoxLuka
    Left = 103
    Top = 279
    Width = 176
    Height = 17
    Caption = 'Visualizar antes de imprimir'
    TabOrder = 2
    CheckedStr = 'N'
  end
end
