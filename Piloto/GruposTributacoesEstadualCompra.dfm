inherited FormGruposTributacoesEstadualCompra: TFormGruposTributacoesEstadualCompra
  Caption = 'Grupos de tributa'#231#245'es estadual de compra'
  ClientHeight = 438
  ClientWidth = 899
  ExplicitWidth = 905
  ExplicitHeight = 467
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [1]
    Left = 210
    Top = 5
    Width = 53
    Height = 14
    Caption = 'Descri'#231#227'o'
  end
  inherited pnOpcoes: TPanel
    Height = 438
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 852
    Top = 2
    OnKeyDown = ProximoCampo
    ExplicitLeft = 852
    ExplicitTop = 2
  end
  object eDescricao: TEditLuka
    Left = 210
    Top = 19
    Width = 640
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 3
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  inline FrICMS: TFrICMSCompra
    Left = 126
    Top = 44
    Width = 768
    Height = 391
    Align = alCustom
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 4
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 44
    inherited pnGrid: TPanel
      ExplicitTop = 8
      inherited sgICMS: TGridLuka
        Align = alCustom
      end
    end
  end
end
