unit LancamentoContagem;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, _Sessao, _RecordsEspeciais,
  Vcl.ExtCtrls, Vcl.StdCtrls, EditLuka, FrameProdutos, Vcl.Grids, GridLuka,
  _FrameHerancaPrincipal, _HerancaBarra, FrameInventario, _Biblioteca,
  _FrameHenrancaPesquisas, _HerancaCadastro, frxClass;

type
  TformLancamentoContagem = class(TFormHerancaBarra)
    FrInventario: TFrInventario;
    sgItens: TGridLuka;
    sbCarregar: TSpeedButton;
    sbGravar: TSpeedButton;
    sbDesfazer: TSpeedButton;
    sbFinalizar: TSpeedButton;
    sbImprimir: TSpeedButton;
    frxReport: TfrxReport;
    dsItens: TfrxUserDataSet;
    procedure FormCreate(Sender: TObject);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure sbCarregarClick(Sender: TObject);
    procedure sbGravarClick(Sender: TObject);
    procedure sbDesfazerClick(Sender: TObject);
    procedure sgItensEnter(Sender: TObject);
    procedure sgItensSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure sgItensKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sbFinalizarClick(Sender: TObject);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sbImprimirClick(Sender: TObject);
    procedure dsItensGetValue(const VarName: string; var Value: Variant);
    procedure dsItensFirst(Sender: TObject);
    procedure dsItensNext(Sender: TObject);
    procedure dsItensPrior(Sender: TObject);
  private
    { Private declarations }
    MasterNo: Integer;
    vOpcao: TRetornoTelaFinalizar<Integer>;
    procedure Modo(pEditando: Boolean);
  public
    { Public declarations }
  end;

var
  formLancamentoContagem: TformLancamentoContagem;

implementation

{$R *.dfm}

uses _InventarioItens, _Inventario, SelecionarVariosOpcoes;

const
  coProdutoId         = 0;
  coNome              = 1;
  coUnidade           = 2;
  coMarca             = 3;
  coLote              = 4;
  coLocal             = 5;
  coContou            = 6;
  coQuantidade        = 7;
  coInventarioId      = 8;
  coEstoque           = 9;
  coQtdeAntes         = 10;
  coCodigoBarra       = 11;


procedure TformLancamentoContagem.dsItensFirst(Sender: TObject);
begin
  inherited;
  MasterNo := 1;
end;

procedure TformLancamentoContagem.dsItensGetValue(const VarName: string;
  var Value: Variant);
begin
  inherited;
  if vOpcao.Dados = 0 then
  begin
    if sgItens.Cells[coEstoque, MasterNo] = sgItens.Cells[coQuantidade, MasterNo] then
      exit;
  end;

  if VarName = 'Produto' then
    value := sgItens.Cells[coProdutoId, MasterNo];
  if VarName = 'Nome' then
    value := sgItens.Cells[coNome, MasterNo];
  if VarName = 'Marca' then
    value := sgItens.Cells[coMarca, MasterNo];
  if VarName = 'Unidade' then
    value := sgItens.Cells[coUnidade, MasterNo];
  if VarName = 'Lote' then
    value := sgItens.Cells[coLote, MasterNo];
  if VarName = 'QuantidadeFisico' then
    value := sgItens.Cells[coEstoque, MasterNo];
  if VarName = 'QuantidadeContada' then
    value := sgItens.Cells[coQuantidade, MasterNo];
end;

procedure TformLancamentoContagem.dsItensNext(Sender: TObject);
begin
  inherited;
  Inc(MasterNo);
end;

procedure TformLancamentoContagem.dsItensPrior(Sender: TObject);
begin
  inherited;
  Dec(MasterNo);
end;

procedure TformLancamentoContagem.FormCreate(Sender: TObject);
begin
  inherited;
  OnKeyDown := ProximoCampo;
  Modo(False);
end;

procedure TformLancamentoContagem.Modo(pEditando: Boolean);
begin
  sbCarregar.Enabled := not pEditando;
  sbGravar.Enabled := (pEditando) and (FrInventario.GetInventario.DataFinal = 0);
  sbDesfazer.Enabled := pEditando;
  sbImprimir.Enabled := pEditando;
  sbFinalizar.Enabled := (pEditando) and (FrInventario.GetInventario.DataFinal = 0);

  FrInventario.Enabled := not pEditando;
  sgItens.Enabled := pEditando;

  if not pEditando then
  begin
    FrInventario.Clear;
    sgItens.ClearGrid;
  end;
end;

procedure TformLancamentoContagem.sbCarregarClick(Sender: TObject);
var
  vItens: TArray<RecInventarioLancamentoItens>;
  vSql: String;
  vLinha: Integer;
begin
  if FrInventario.EstaVazio then
  begin
    Exclamar('Contagem de estoque inv�lida!');
    SetarFoco(FrInventario);
    Abort;
  end;

  if FrInventario.GetInventario.DataInicio = 0 then
  begin
    Exclamar('Lista de contagem de estoque n�o iniciada!');
    SetarFoco(FrInventario);
    Abort;
  end;

  _Biblioteca.WhereOuAnd(vSql, ' EMPRESA_ID = ' + FrInventario.GetInventario.EmpresaId.ToString);
  _Biblioteca.WhereOuAnd(vSql, FrInventario.getSqlFiltros('INVENTARIO_ID'));

  vItens := _InventarioItens.BuscarLancamentoItens(
    Sessao.getConexaoBanco,
    vSql );

  if vItens = nil then
  begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  for vLinha := Low(vItens) to High(vItens) do
  begin
    sgItens.Cells[coProdutoId, vLinha + 1]      := NFormat(vItens[vLinha].ProdutoId);
    sgItens.Cells[coNome, vLinha + 1]           := vItens[vLinha].NomeProduto;
    sgItens.Cells[coUnidade, vLinha + 1]        := vItens[vLinha].UnidadeVenda;
    sgItens.Cells[coMarca, vLinha + 1]          := NFormatN(vItens[vLinha].MarcaId) + ' - ' + vItens[vLinha].NomeMarca;
    sgItens.Cells[coLote, vLinha + 1]           := vItens[vLinha].Lote;
    sgItens.Cells[coLocal, vLinha + 1]          := vItens[vLinha].LocalId.ToString;
    sgItens.Cells[coInventarioId, vLinha + 1]   := vItens[vLinha].InventarioId.ToString;
    sgItens.Cells[coEstoque, vLinha + 1]        := vItens[vLinha].Estoque.ToString;
    sgItens.Cells[coQuantidade, vLinha + 1]     := vItens[vLinha].Quantidade.ToString;
    sgItens.Cells[coQtdeAntes, vLinha + 1]      := vItens[vLinha].Quantidade.ToString;
    sgItens.Cells[coContou, vLinha + 1]         := vItens[vLinha].Contou;
    sgItens.Cells[coCodigoBarra, vLinha + 1]    := vItens[vLinha].CodigoBarra;

  end;
  dsItens.RangeEnd := reCount;
  dsItens.RangeEndCount := vLinha;

  sgItens.SetLinhasGridPorTamanhoVetor( vLinha);
  SetarFoco(sgItens);
  sgItensEnter(sgItens);
  Modo(True);
  Perform(40,0,0);
end;

procedure TformLancamentoContagem.sbDesfazerClick(Sender: TObject);
begin
  inherited;
  Modo(False);
end;

procedure TformLancamentoContagem.sbFinalizarClick(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;

  if not Perguntar('Ao finalizar a contagem n�o ser� poss�vel altera-la mais. Deseja realmente fazer isso?') then
    exit;

  vRetBanco :=
    _Inventario.AtualizarInventario(
      Sessao.getConexaoBanco,
      FrInventario.GetInventario.InventarioId,
      FrInventario.GetInventario.EmpresaId,
      FrInventario.GetInventario.Nome,
      FrInventario.GetInventario.DataInicio,
      Now,
      FrInventario.GetInventario.UltimaContagem,
      []
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  Modo(False);
end;

procedure TformLancamentoContagem.sbGravarClick(Sender: TObject);
var
  vListaProdutos: TArray<RecInventarioItens>;
  i: Integer;
  vRetBanco: RecRetornoBD;
  vContou: Boolean;
begin
  inherited;

  SetLength(vListaProdutos, sgItens.RowCount - 1);
  for i := 0 to sgItens.RowCount - 2 do
  begin
    if (sgItens.Cells[coContou, i + 1] = 'N')
      and (sgItens.Cells[coQuantidade, i + 1] <> sgItens.Cells[coQtdeAntes, i + 1]) then
      vListaProdutos[i].Contou := 'S'
    else
      vListaProdutos[i].Contou := sgItens.Cells[coContou, i + 1];

    vListaProdutos[i].InventarioId  := sgItens.Cells[coInventarioId, i + 1].ToInteger;
    vListaProdutos[i].ProdutoId     := SFormatInt(sgItens.Cells[coProdutoId, i + 1]);
    vListaProdutos[i].LocalId       := sgItens.Cells[coLocal, i + 1].ToInteger;
    vListaProdutos[i].Estoque       := SFormatInt(sgItens.Cells[coEstoque, i + 1]);
    vListaProdutos[i].Quantidade    := SFormatInt(sgItens.Cells[coQuantidade, i + 1]);
  end;

  vRetBanco :=
    _Inventario.AtualizarInventario(
      Sessao.getConexaoBanco,
      FrInventario.GetInventario.InventarioId,
      FrInventario.GetInventario.EmpresaId,
      FrInventario.GetInventario.Nome,
      FrInventario.GetInventario.DataInicio,
      0,
      FrInventario.GetInventario.UltimaContagem + 1,
      vListaProdutos
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  Modo(False);
end;

procedure TformLancamentoContagem.sbImprimirClick(Sender: TObject);
begin
  inherited;
  vOpcao := SelecionarVariosOpcoes.Selecionar('Tipo relat�rio', ['Contagem Divergente','Lista Completa'], 0);
  frxReport.ShowReport;
end;

procedure TformLancamentoContagem.sgItensDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  if ACol in[coProdutoId, coNome, coUnidade, coMarca, coLote, coContou] then
    vAlinhamento := taLeftJustify
  else
    vAlinhamento := taRightJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TformLancamentoContagem.sgItensEnter(Sender: TObject);
begin
  inherited;
  sgItens.Col := coQuantidade;
end;

procedure TformLancamentoContagem.sgItensGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
var
  bolQtdeDiferente: Boolean;
begin
  inherited;
  if Astate = [gdFixed] then
    exit;

  bolQtdeDiferente := sgItens.Cells[coEstoque, ARow] <> sgItens.Cells[coQuantidade, ARow];

  if ACol in[coProdutoId, coNome, coUnidade, coMarca, coLote] then
  begin
    ABrush.Color:= clBtnFace;
    AFont.Color:= clWindowText;
  end else
  begin
    ABrush.Color:= clWhite;
    AFont.Color:= clWindowText;
  end;

  if bolQtdeDiferente then
  begin
    ABrush.Color := clRed;
    AFont.Color  := clWhite;
  end;

end;

procedure TformLancamentoContagem.sgItensKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  vLinha: Integer;
  vCodigoBarra: string;
begin
  inherited;
  case key of
    VK_RETURN: //Enter muda de linha
    begin
      if sgItens.RowCount <> sgItens.Row +1 then
        sgItens.Row := sgItens.Row + 1;
    end;
    VK_MULTIPLY://Localiza produto
    begin
      vCodigoBarra :=  InputBox('Pesquisa de Produto','C�digo de Barras','');
      vLinha := sgItens.FindRow(coCodigoBarra,vCodigoBarra);

      vCodigoBarra := NFormat(StrToIntDef(vCodigoBarra,0));
      if vLinha < 0 then
        vLinha := sgItens.FindRow(coProdutoId,vCodigoBarra);

      if vLinha > 0 then
      begin
        sgItens.Row := vLinha;
        if sgItens.Cells[coContou,vLinha] = 'N' then
          sgItens.Cells[coQuantidade,vLinha] :=  (sgItens.Cells[coQuantidade,vLinha].ToInteger + 1).toString;
      end;
      if vLinha < 0 then
        Exclamar('Produto n�o encontrado!');
    end;
  end;

end;

procedure TformLancamentoContagem.sgItensSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  if (sgItens.Cells[coContou, ARow] = 'N') and (Acol = coQuantidade) then
    sgItens.Options := sgItens.Options + [goEditing]
  else
    sgItens.Options := sgItens.Options - [goEditing];
end;

end.
