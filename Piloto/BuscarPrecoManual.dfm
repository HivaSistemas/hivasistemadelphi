inherited FormBuscaPrecoManual: TFormBuscaPrecoManual
  Caption = 'Busca de pre'#231'o manual'
  ClientHeight = 393
  ClientWidth = 736
  ExplicitWidth = 742
  ExplicitHeight = 422
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 356
    Width = 736
    ExplicitTop = 356
    ExplicitWidth = 736
  end
  object sgItens: TGridLuka
    Left = 2
    Top = 1
    Width = 732
    Height = 322
    Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
    Align = alCustom
    ColCount = 9
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
    PopupMenu = pmOpcoesGrid
    TabOrder = 1
    OnDrawCell = sgItensDrawCell
    OnKeyPress = NumerosVirgula
    OnSelectCell = sgItensSelectCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Leg.'
      'Produto'
      'Nome'
      'Marca'
      'Qtde.'
      'Und.'
      'Pre'#231'o orig.'
      '% Desc.'
      'Novo pre'#231'o')
    OnGetCellColor = sgItensGetCellColor
    OnGetCellPicture = sgItensGetCellPicture
    OnArrumarGrid = sgItensArrumarGrid
    Grid3D = False
    RealColCount = 15
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      26
      50
      249
      125
      39
      32
      65
      51
      69)
  end
  object st5: TStaticText
    Left = 479
    Top = 324
    Width = 125
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '% Desconto m'#233'dio'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    Transparent = False
  end
  object st1: TStaticText
    Left = 355
    Top = 324
    Width = 125
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Total desconto'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 3
    Transparent = False
  end
  object st4: TStaticText
    Left = 107
    Top = 324
    Width = 125
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Total prod.original'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 4
    Transparent = False
  end
  object stTotalProdutosOriginal: TStaticTextLuka
    Left = 107
    Top = 339
    Width = 125
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGreen
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 5
    Transparent = False
    TipoCampo = tcNumerico
    CasasDecimais = 2
  end
  object stTotalDesconto: TStaticTextLuka
    Left = 355
    Top = 339
    Width = 125
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 6
    Transparent = False
    TipoCampo = tcNumerico
    CasasDecimais = 2
  end
  object stDescontoMedio: TStaticTextLuka
    Left = 479
    Top = 339
    Width = 125
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00000'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 33023
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 7
    Transparent = False
    TipoCampo = tcNumerico
    CasasDecimais = 5
  end
  object st2: TStaticText
    Left = 231
    Top = 324
    Width = 125
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Novo total prod.'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 8
    Transparent = False
  end
  object stNovoTotalProdutos: TStaticTextLuka
    Left = 231
    Top = 339
    Width = 125
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 9
    Transparent = False
    TipoCampo = tcNumerico
    CasasDecimais = 2
  end
  object pmOpcoesGrid: TPopupMenu
    Left = 668
    Top = 277
    object miLegendas: TMenuItem
      Caption = 'Legendas'
      OnClick = miLegendasClick
    end
  end
end
