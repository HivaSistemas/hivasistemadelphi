inherited FormImpressaoComprovanteCreditoPagarGrafico: TFormImpressaoComprovanteCreditoPagarGrafico
  Caption = 'FormImpressaoComprovanteCreditoPagarGrafico'
  ClientWidth = 1261
  ExplicitTop = -190
  ExplicitWidth = 1277
  ExplicitHeight = 754
  PixelsPerInch = 96
  TextHeight = 13
  inherited qrRelatorioNF: TQuickRep
    Left = 20
    Top = 81
    Width = 457
    Height = 576
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Page.Values = (
      0.000000000000000000
      952.500000000000000000
      0.000000000000000000
      755.716145833333200000
      0.000000000000000000
      0.000000000000000000
      0.000000000000000000)
    PrinterSettings.CustomPaperCode = 262
    ExplicitLeft = 20
    ExplicitTop = 81
    ExplicitWidth = 457
    ExplicitHeight = 576
    inherited qrBandTitulo: TQRBand
      Width = 457
      Height = 497
      Size.Values = (
        821.861979166666700000
        755.716145833333300000)
      ExplicitWidth = 457
      ExplicitHeight = 497
      inherited qrlNFNomeEmpresa: TQRLabel
        Size.Values = (
          34.726562500000000000
          6.614583333333332000
          16.536458333333330000
          744.140625000000000000)
        FontSize = 6
      end
      inherited qrNFCnpj: TQRLabel
        Left = 12
        Size.Values = (
          33.072916666666670000
          19.843750000000000000
          42.994791666666670000
          744.140625000000000000)
        FontSize = 7
        ExplicitLeft = 12
      end
      inherited qrNFEndereco: TQRLabel
        Left = 7
        Size.Values = (
          34.726562500000000000
          11.575520833333330000
          85.989583333333320000
          744.140625000000000000)
        FontSize = 7
        ExplicitLeft = 7
      end
      inherited qrNFCidadeUf: TQRLabel
        Size.Values = (
          42.994791666666670000
          6.614583333333332000
          122.369791666666700000
          744.140625000000000000)
        FontSize = 7
      end
      inherited qrNFTipoDocumento: TQRLabel
        Size.Values = (
          33.072916666666670000
          1.653645833333333000
          211.666666666666700000
          719.335937500000000000)
        Caption = 'COMPROVANTE DE CR'#201'DITO'
        FontSize = 7
      end
      inherited qrSeparador2: TQRShape
        Size.Values = (
          3.307291666666667000
          3.307291666666667000
          281.119791666666700000
          724.296875000000000000)
      end
      inherited qrNFTitulo: TQRLabel
        Size.Values = (
          33.072916666666670000
          1.653645833333333000
          246.393229166666700000
          719.335937500000000000)
        Font.Style = []
        FontSize = 7
      end
      inherited qrlNFEmitidoEm: TQRLabel
        Size.Values = (
          23.151041666666670000
          370.416666666666700000
          3.307291666666666000
          357.187500000000000000)
        FontSize = -6
      end
      inherited qrlNFTelefoneEmpresa: TQRLabel
        Top = 101
        Size.Values = (
          42.994791666666670000
          6.614583333333332000
          167.018229166666700000
          744.140625000000000000)
        FontSize = 7
        ExplicitTop = 101
      end
      object qrl1: TQRLabel
        Left = 4
        Top = 177
        Width = 93
        Height = 20
        Size.Values = (
          33.072916666666670000
          6.614583333333334000
          292.695312500000000000
          153.789062500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'C'#243'd.Pagar:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlNFFormasPagamento: TQRMemo
        Left = 4
        Top = 263
        Width = 432
        Height = 89
        Size.Values = (
          147.174479166666700000
          6.614583333333333000
          434.908854166666700000
          714.375000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        Lines.Strings = (
          'Dinheiro: R$ 1.500,00'
          'Cheque.: R$ 1.200,00'
          'Cart'#227'o..: R$ 5.000,00'
          'Cobran'#231'a: R$ 1.400,00'
          'Pix........: R$ 1.000,00')
        ParentFont = False
        Transparent = False
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 7
      end
      object qrlNFCodigoFinanceiro: TQRLabel
        Left = 98
        Top = 177
        Width = 93
        Height = 20
        Size.Values = (
          33.072916666666670000
          162.057291666666700000
          292.695312500000000000
          153.789062500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '15.000'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrl3: TQRLabel
        Left = 202
        Top = 177
        Width = 145
        Height = 20
        Size.Values = (
          33.072916666666670000
          334.036458333333300000
          292.695312500000000000
          239.778645833333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Bx.receb. origem:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlNFCodigoBaixaOrigem: TQRLabel
        Left = 348
        Top = 177
        Width = 88
        Height = 20
        Size.Values = (
          33.072916666666670000
          575.468750000000000000
          292.695312500000000000
          145.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '15.000'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrl5: TQRLabel
        Left = 4
        Top = 198
        Width = 69
        Height = 20
        Size.Values = (
          33.072916666666670000
          6.614583333333334000
          327.421875000000000000
          114.101562500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cliente:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlNFCliente: TQRLabel
        Left = 76
        Top = 198
        Width = 360
        Height = 20
        Size.Values = (
          33.072916666666670000
          125.677083333333300000
          327.421875000000000000
          595.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '15.000 - EZEQUIEL EUGENIO DO PRADO'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlNFValorCredito: TQRLabel
        Left = 4
        Top = 219
        Width = 432
        Height = 20
        Size.Values = (
          33.072916666666670000
          6.614583333333334000
          362.148437500000000000
          714.375000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor do cr'#233'dito: R$ 2.000,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = '\\'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrl8: TQRLabel
        Left = 2
        Top = 386
        Width = 438
        Height = 21
        Size.Values = (
          34.726562500000000000
          3.307291666666667000
          638.307291666666700000
          724.296875000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Assinatura do cliente'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRShape2: TQRShape
        Left = 38
        Top = 382
        Width = 353
        Height = 2
        Size.Values = (
          3.307291666666667000
          62.838541666666670000
          631.692708333333300000
          583.736979166666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 15395562
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrl9: TQRLabel
        Left = 2
        Top = 442
        Width = 438
        Height = 21
        Size.Values = (
          34.726562500000000000
          3.307291666666667000
          730.911458333333200000
          724.296875000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Assinatura da empresa'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRShape1: TQRShape
        Left = 38
        Top = 438
        Width = 353
        Height = 2
        Size.Values = (
          3.307291666666667000
          62.838541666666680000
          724.296875000000000000
          583.736979166666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 15395562
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrlNFCabecalhoFormasPagto: TQRLabel
        Left = 4
        Top = 242
        Width = 432
        Height = 20
        Enabled = False
        Size.Values = (
          33.072916666666670000
          6.614583333333332000
          400.182291666666700000
          714.375000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Formas de pagamento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = '\\'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
    end
    inherited PageFooterBand1: TQRBand
      Top = 497
      Width = 457
      Size.Values = (
        24.804687500000000000
        755.716145833333300000)
      ExplicitTop = 497
      ExplicitWidth = 457
      inherited qrlNFSistema: TQRLabel
        Size.Values = (
          23.151041666666670000
          582.083333333333400000
          0.000000000000000000
          138.906250000000000000)
        FontSize = -6
      end
    end
  end
  inherited qrRelatorioA4: TQuickRep
    Left = 445
    Top = 58
    Height = 472
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Page.PaperSize = Custom
    Page.Values = (
      100.000000000000000000
      1250.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    ExplicitLeft = 445
    ExplicitTop = 58
    ExplicitHeight = 472
    inherited qrCabecalho: TQRBand
      Height = 108
      Size.Values = (
        285.750000000000000000
        1899.708333333333000000)
      ExplicitHeight = 108
      inherited qrCaptionEndereco: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          44.979166666666670000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrEmpresa: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          7.937500000000000000
          568.854166666666700000)
        FontSize = 7
      end
      inherited qrEndereco: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          44.979166666666670000
          1513.416666666667000000)
        FontSize = 7
      end
      inherited qrLogoEmpresa: TQRImage
        Size.Values = (
          211.666666666666700000
          13.229166666666670000
          2.645833333333333000
          211.666666666666700000)
      end
      inherited qrCaptionEmpresa: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          7.937500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrEmitidoEm: TQRLabel
        Size.Values = (
          29.104166666666670000
          1529.291666666667000000
          0.000000000000000000
          370.416666666666700000)
        FontSize = 5
      end
      inherited qr1: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          82.020833333333320000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrBairro: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          82.020833333333320000
          568.854166666666700000)
        FontSize = 7
      end
      inherited qr3: TQRLabel
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          82.020833333333320000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrCidadeUf: TQRLabel
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          82.020833333333320000
          568.854166666666700000)
        FontSize = 7
      end
      inherited qr5: TQRLabel
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          7.937500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrCNPJ: TQRLabel
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          7.937500000000000000
          248.708333333333300000)
        FontSize = 7
      end
      inherited qr7: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrTelefone: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          119.062500000000000000
          187.854166666666700000)
        FontSize = 7
      end
      inherited qrFax: TQRLabel
        Size.Values = (
          34.395833333333330000
          767.291666666666800000
          119.062500000000000000
          187.854166666666700000)
        FontSize = 7
      end
      inherited qr10: TQRLabel
        Size.Values = (
          34.395833333333330000
          603.250000000000000000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qr11: TQRLabel
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrEmail: TQRLabel
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          119.062500000000000000
          769.937500000000000000)
        FontSize = 7
      end
      inherited qr13: TQRLabel
        Left = 223
        Width = 246
        Size.Values = (
          52.916666666666670000
          590.020833333333200000
          164.041666666666700000
          650.875000000000000000)
        Caption = 'COMPROVANTE DE CR'#201'DITO'
        FontSize = 12
        ExplicitLeft = 223
        ExplicitWidth = 246
      end
      object QRLabel1: TQRLabel
        Left = 209
        Top = 85
        Width = 272
        Height = 13
        Size.Values = (
          34.395833333333330000
          552.979166666666700000
          224.895833333333300000
          719.666666666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'N'#195'O '#201' V'#193'LIDO COMO DOCUMENTO FISCAL'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRShape3: TQRShape
        Left = 0
        Top = 104
        Width = 717
        Height = 1
        Size.Values = (
          2.645833333333333000
          0.000000000000000000
          275.166666666666700000
          1897.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 15395562
        Shape = qrsRectangle
        VertAdjust = 0
      end
    end
    inherited qrbndRodape: TQRBand
      Top = 401
      Height = 67
      Size.Values = (
        177.270833333333300000
        1899.708333333333000000)
      ExplicitTop = 401
      ExplicitHeight = 67
      inherited qrUsuarioImpressao: TQRLabel
        Size.Values = (
          31.750000000000000000
          7.937500000000000000
          74.083333333333320000
          354.541666666666700000)
        FontSize = 7
      end
      inherited qrSistema: TQRLabel
        Size.Values = (
          31.750000000000000000
          1529.291666666667000000
          74.083333333333340000
          370.416666666666700000)
        FontSize = 7
      end
    end
    object QRBand1: TQRBand
      Left = 38
      Top = 146
      Width = 718
      Height = 255
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        674.687500000000000000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbTitle
      object QRLabel2: TQRLabel
        Left = 3
        Top = 7
        Width = 92
        Height = 17
        Size.Values = (
          44.979166666666670000
          7.937500000000000000
          18.520833333333330000
          243.416666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'C'#243'digo Pagar:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qrNFCodigoFinanceiro: TQRLabel
        Left = 96
        Top = 7
        Width = 58
        Height = 17
        Size.Values = (
          44.979166666666670000
          254.000000000000000000
          18.520833333333330000
          153.458333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '15.000'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRLabel4: TQRLabel
        Left = 485
        Top = 6
        Width = 115
        Height = 17
        Size.Values = (
          44.979166666666670000
          1283.229166666667000000
          15.875000000000000000
          304.270833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Bx.receb. origem:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qrNFCodigoBaixaOrigem: TQRLabel
        Left = 602
        Top = 6
        Width = 55
        Height = 17
        Size.Values = (
          44.979166666666670000
          1592.791666666667000000
          15.875000000000000000
          145.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '15.000'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRLabel6: TQRLabel
        Left = 3
        Top = 36
        Width = 53
        Height = 17
        Size.Values = (
          44.979166666666670000
          7.937500000000000000
          95.250000000000000000
          140.229166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cliente:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qrNFCliente: TQRLabel
        Left = 57
        Top = 36
        Width = 368
        Height = 17
        Size.Values = (
          44.979166666666670000
          150.812500000000000000
          95.250000000000000000
          973.666666666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '15.000 - EZEQUIEL EUGENIO DO PRADO'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qrNFValorCredito: TQRLabel
        Left = 485
        Top = 36
        Width = 198
        Height = 17
        Size.Values = (
          44.979166666666670000
          1283.229166666667000000
          95.250000000000000000
          523.875000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor do cr'#233'dito: R$ 2.000,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = '\\'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qrNFCabecalhoFormasPagto: TQRLabel
        Left = 0
        Top = 70
        Width = 270
        Height = 17
        Enabled = False
        Size.Values = (
          44.979166666666670000
          0.000000000000000000
          185.208333333333300000
          714.375000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Formas de pagamento'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = '\\'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object qrNFFormasPagamento: TQRMemo
        Left = 0
        Top = 94
        Width = 270
        Height = 91
        Size.Values = (
          240.770833333333300000
          0.000000000000000000
          248.708333333333300000
          714.375000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        Lines.Strings = (
          'Dinheiro: R$ 1.500,00'
          'Cheque.: R$ 1.200,00'
          'Cart'#227'o..: R$ 5.000,00'
          'Cobran'#231'a: R$ 1.400,00'
          'Pix.......: R$ 1.400,00')
        ParentFont = False
        Transparent = False
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 10
      end
      object QRShape5: TQRShape
        Left = 232
        Top = 207
        Width = 333
        Height = 1
        Size.Values = (
          2.645833333333333000
          613.833333333333200000
          547.687500000000000000
          881.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 15395562
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel11: TQRLabel
        Left = 265
        Top = 211
        Width = 274
        Height = 16
        Size.Values = (
          42.333333333333330000
          701.145833333333200000
          558.270833333333300000
          724.958333333333200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Assinatura da empresa'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
    end
  end
end
