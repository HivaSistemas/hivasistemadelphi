unit Emissao.BoletosBancario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Grids, GridLuka, _RecordsFinanceiros,
  Vcl.Buttons, Vcl.ExtCtrls, _Sessao, System.Math, _Biblioteca, _ContasReceber, _Imagens, System.StrUtils,
  Vcl.StdCtrls, CheckBoxLuka, _BoletoItau, _BoletoBradesco, _CobrancaBancaria, _CobrancaBradesco,
  _RecordsEspeciais, DateUtils;

type
  TTipoMovimentoBoleto = (tbOrcamento, tbAcumulado);

  TFormEmissaoBoletoBancario = class(TFormHerancaFinalizar)
    sgBoletos: TGridLuka;
    sbImprimir: TSpeedButton;
    ckVisualizarAntesImprimir: TCheckBoxLuka;
    procedure FormShow(Sender: TObject);
    procedure sgBoletosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgBoletosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure sgBoletosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbImprimirClick(Sender: TObject);
  private
    FBoletos: TArray<RecBoletosEmitir>;
    function DigitoLinhaDigitavel(linhadigitavel: string): string;
    function DigitoCodigoBarra(codigobarra: string): string;
    function Multiplo10(numero: integer): integer;
  end;

procedure Emitir(pMovimentoId: Integer; pTipoMovimento: TTipoMovimentoBoleto); overload;
procedure Emitir(pReceberIds: TArray<Integer>); overload;

implementation

{$R *.dfm}

uses _BoletoSicoob;

const
  coImprimir       = 0;
  coReceberId      = 1;
  coDocumento      = 2;
  coClienteId      = 3;
  coNomeCliente    = 4;
  coDataVencimento = 5;
  coValor          = 6;
  coParcela        = 7;
  coEmpresaId      = 8;
  coNomeEmpresa    = 9;

  (* Ocultas *)
  coNossoNumero     = 10;
  coPortador        = 11;
  coAgencia         = 12;
  coDigitoAgencia   = 13;
  coConta           = 14;
  coDigitoConta     = 15;
  coCarteira        = 16;
  coInstrucao       = 17;
  coDataEmissao     = 18;
  coEnderecoEmpresa = 19;
  coLogradouro      = 20;
  coCep             = 21;
  coCpfCnpj         = 22;
  coNomeCidade      = 23;
  coEstadoId        = 24;
  coCNPJEmpresa     = 25;
  coNomePortador    = 26;

procedure Emitir(pMovimentoId: Integer; pTipoMovimento: TTipoMovimentoBoleto);
var
  vBoletos: TArray<RecBoletosEmitir>;
  vForm: TFormEmissaoBoletoBancario;
begin
  if pTipoMovimento = tbOrcamento then
    vBoletos := _ContasReceber.BuscarBoletosEmitir(Sessao.getConexaoBanco, 'where ORCAMENTO_ID = ' + IntToStr(pMovimentoId))
  else
    vBoletos := _ContasReceber.BuscarBoletosEmitir(Sessao.getConexaoBanco, 'where ACUMULADO_ID = ' + IntToStr(pMovimentoId));

  if vBoletos = nil then begin
    Exclamar('Nenhum boleto foi encontrado!');
    Exit;
  end;

  vForm := TFormEmissaoBoletoBancario.Create(Application);

  vForm.FBoletos := vBoletos;

  vForm.ShowModal;

  vForm.Free;
end;

procedure Emitir(pReceberIds: TArray<Integer>);
var
  vBoletos: TArray<RecBoletosEmitir>;
  vForm: TFormEmissaoBoletoBancario;
begin
  vBoletos := _ContasReceber.BuscarBoletosEmitir(Sessao.getConexaoBanco, 'where ' + FiltroInInt('RECEBER_ID', pReceberIds));
  if vBoletos = nil then begin
    Exclamar('Nenhum boleto foi encontrado!');
    Exit;
  end;

  vForm := TFormEmissaoBoletoBancario.Create(Application);

  vForm.FBoletos := vBoletos;

  vForm.ShowModal;

  vForm.Free;
end;

procedure TFormEmissaoBoletoBancario.FormShow(Sender: TObject);
var
  i: Integer;
begin
  inherited;

  for i := Low(FBoletos) to High(FBoletos) do begin
    sgBoletos.Cells[coImprimir, i + 1]       := charSelecionado;
    sgBoletos.Cells[coReceberId, i + 1]      := NFormat(FBoletos[i].ReceberId);
    sgBoletos.Cells[coDocumento, i + 1]      := FBoletos[i].Documento;
    sgBoletos.Cells[coClienteId, i + 1]      := NFormat(FBoletos[i].CadastroId);
    sgBoletos.Cells[coNomeCliente, i + 1]    := FBoletos[i].NomeCliente;
    sgBoletos.Cells[coDataVencimento, i + 1] := DFormat(FBoletos[i].DataVencimento);
    sgBoletos.Cells[coValor, i + 1]          := NFormat(FBoletos[i].ValorDocumento);
    sgBoletos.Cells[coParcela, i + 1]        := NFormat(FBoletos[i].Parcela) + ' / ' + NFormat(FBoletos[i].NumeroParcelas);
    sgBoletos.Cells[coEmpresaId, i + 1]      := NFormat(FBoletos[i].EmpresaId);
    sgBoletos.Cells[coNomeEmpresa, i + 1]    := FBoletos[i].NomeEmpresa;
    sgBoletos.Cells[coDataEmissao, i + 1]    := DFormat(FBoletos[i].DataEmissao);
    sgBoletos.Cells[coPortador, i + 1]       := FBoletos[i].PortadorId;
    sgBoletos.Cells[coConta, i + 1]          := FBoletos[i].Conta;
    sgBoletos.Cells[coDigitoConta, i + 1]    := FBoletos[i].DigitoConta;
    sgBoletos.Cells[coCarteira, i + 1]       := FBoletos[i].Carteira;
    sgBoletos.Cells[coAgencia, i + 1]        := FBoletos[i].Agencia;
    sgBoletos.Cells[coDigitoAgencia, i + 1]  := FBoletos[i].DigitoAgencia;
    sgBoletos.Cells[coInstrucao, i + 1]      := FBoletos[i].InstrucaoBoleto;

    sgBoletos.Cells[coEnderecoEmpresa, i + 1]:=
      FBoletos[i].LogradouroEmpresa + ' ' +
      FBoletos[i].ComplementoEmpresa + ' ' +
      FBoletos[i].NomeBairroEmpresa + ' ' +
      FBoletos[i].NomeCidadeEmpresa + ' ' +
      FBoletos[i].EstadoIdEmpresa + ' ' +
      'CEP: ' + FBoletos[i].CepEmpresa;

    sgBoletos.Cells[coLogradouro, i + 1]   := FBoletos[i].Logradouro + ' ' + FBoletos[i].Complemento + ' ' + FBoletos[i].NomeBairro;
    sgBoletos.Cells[coCep, i + 1]          := FBoletos[i].Cep;
    sgBoletos.Cells[coCpfCnpj, i + 1]      := FBoletos[i].CpfCnpj;
    sgBoletos.Cells[coNomeCidade, i + 1]   := FBoletos[i].NomeCidade;
    sgBoletos.Cells[coEstadoId, i + 1]     := FBoletos[i].EstadoId;
    sgBoletos.Cells[coCNPJEmpresa, i + 1]  := FBoletos[i].CnpjEmpresa;
    sgBoletos.Cells[coNomePortador, i + 1] := FBoletos[i].NomePortador;
  end;
  sgBoletos.RowCount := IfThen(Length(FBoletos) > 1, High(FBoletos) + 2, 2);
end;

procedure TFormEmissaoBoletoBancario.sbImprimirClick(Sender: TObject);
type
  TDadosBoleto = record
    Agencia: string;
    DigitoAgencia: string;
    Conta: string;
    DigitoConta: string;
    Carteira: string;
    NossoNumero: string;

    ValorDocumento: Double;
    ValorMulta: Double;
    ValorJuros: Double;

    LinhaDigitavel: string;
    DigitoCodigoBarras: string;
    CodigoBarras: string;
    Vencimento: TDateTime;
    FatorVencimento: string;
    Instrucao: string;
    LinhaDigitavelSemFormatar: string;
    PrimeiroCampo: string;
    SeguntoCampo: string;
    TerceiroCampo: string;
  end;

var
  i: Integer;
  vDados: TDadosBoleto;
  vBoletos: TArray<RecDadosBoletoBancario>;
  vCobranca: ICobrancaBancaria;
  vRetBanco: RecRetornoBD;
  linha: string;

  vDataBase: TDate;

  function getFatorVencimento(dataVencimento: TDateTime): string;
  begin
    Result := IntToStr(DaysBetween(dataVencimento, StrToDate('03/07/2000')) + 1000);
  end;

  function getDigitoVerifCodigoBarra(pCodigo: string): string;
  var
    i: integer;
    vSoma: integer;
    vDigito: integer;
    vMultiplicador: integer;
  begin
    vMultiplicador := 9;
    vSoma := 0;

    for i := Length(pCodigo) downto 1 do begin
      if pCodigo[i] = 'X' then
        Continue;

      Inc(vMultiplicador);
      if vMultiplicador = 10 then
        vMultiplicador := 2;

      vSoma := vSoma + ( SFormatInt(pCodigo[i]) * vMultiplicador );
    end;

    vDigito := 11 - (vSoma mod 11);

    if vDigito in [0, 1, 10, 11] then
      vDigito := 1;

    Result := IntToStr(vDigito);
  end;

  function getDigitoVerificador(pConteudo: string): string;
  var
    i,j: Integer;
    vDoisUm: Integer;
    vResto : Integer;
    vParcial: Integer;
    vParcialStr: string;
    vTotalGeral: Integer;
    vNovoParcial: Integer;
  begin
    vDoisUm := 1;
    vTotalGeral := 0;

    for i:= length(pConteudo) downto 1 do begin
      if vDoisUm = 1 then
        vDoisUm := 2
      else
        vDoisUm := 1;

      vParcial := vDoisUm * SFormatInt(pConteudo[i]);

      if vParcial > 9 then begin
        vNovoParcial := 0;
        vParcialStr := IntToStr(vParcial);
        for j := length(vParcialStr) downto 1 do
          vNovoParcial := vNovoParcial + SFormatInt(vParcialStr[j]);
        vParcial := vNovoParcial;
      end;

      vTotalGeral := vTotalGeral + vParcial;
    end;
    vResto := vTotalGeral mod 10;

    if vResto = 0 then
      Result := '0'
    else
      Result := NFormat(10 - vResto, 0);
  end;

begin
  inherited;

  vBoletos := nil;

  for i := 1 to sgBoletos.RowCount - 1 do begin
    if sgBoletos.Cells[coImprimir, i] = charNaoSelecionado then
      Continue;

    vRetBanco := _ContasReceber.GerarNossoNumero(Sessao.getConexaoBanco, SFormatInt(sgBoletos.Cells[coReceberId, i]));
    if vRetBanco.TeveErro then begin
      _Biblioteca.Exclamar('Falha ao gerar noss numero! ' + vRetBanco.MensagemErro);
      Continue;
    end;

    sgBoletos.Cells[coNossoNumero, i] := vRetBanco.AsString;

    vDataBase := StrToDate('07/10/1997');

    vDados.Agencia         := sgBoletos.Cells[coAgencia, i];
    vDados.DigitoAgencia   := Copy(sgBoletos.Cells[coDigitoAgencia, i], 1, 1);
    vDados.Conta           := sgBoletos.Cells[coConta, i];
    vDados.DigitoConta     := sgBoletos.Cells[coDigitoConta, i];
    vDados.NossoNumero     := sgBoletos.Cells[coNossoNumero, i];
    vDados.Carteira        := sgBoletos.Cells[coCarteira, i];
    vDados.Vencimento      := ToData(sgBoletos.Cells[coDataVencimento, i]);
    vDados.FatorVencimento := LPad(getFatorVencimento(vDados.Vencimento), 4, '0');
    vDados.ValorDocumento  := SFormatDouble(sgBoletos.Cells[coValor, i]);
    vDados.Instrucao       := sgBoletos.Cells[coInstrucao, i];
    vDados.ValorMulta      := 0;
    vDados.ValorJuros      := 0;

    // Formatando a instru��o
    if Sessao.getParametrosEmpresa.PercentualMulta > 0 then
      vDados.ValorMulta := vDados.ValorDocumento * Sessao.getParametrosEmpresa.PercentualMulta * 0.01;

    if Sessao.getParametrosEmpresa.PercentualJurosMensal > 0 then
      vDados.ValorJuros := vDados.ValorDocumento * Sessao.getParametrosEmpresa.PercentualJurosMensal * 0.01 / 100;

    vDados.Instrucao := AnsiReplaceStr(vDados.Instrucao,':BANCO', sgBoletos.Cells[coNomePortador, i]);
    vDados.Instrucao := AnsiReplaceStr(vDados.Instrucao,':MULTA', NFormatN(vDados.ValorMulta));
    vDados.Instrucao := AnsiReplaceStr(vDados.Instrucao,':JUROS_DIA', NFormatN(vDados.ValorJuros / 30));
    vDados.Instrucao := AnsiReplaceStr(vDados.Instrucao, 'AP�S :QT_DIAS DIA(S) VENCIDO(S) SER� ENVIADO PARA CART�RIO', '');

    if Sessao.getParametrosEmpresa.PercentualJurosMensal > 0 then
      vDados.Instrucao := AnsiReplaceStr(vDados.Instrucao, ':PERC_JUROS', NFormatN(Sessao.getParametrosEmpresa.PercentualJurosMensal))
    else
      vDados.Instrucao := AnsiReplaceStr(vDados.Instrucao, 'AP�S VENCIMENTO COBRAR :PERC_JUROS % AO M�S', '');

    // Bradesco
//    if sgBoletos.Cells[coPortador, i] = '237' then begin
//      vCobranca := TCobrancaBradesco.Create;
//
//      vDados.Conta := LPad(vDados.Conta, 7, '0');
//      vDados.NossoNumero := LPad(vDados.NossoNumero, 11, '0');
//
//      vDados.LinhaDigitavelSemFormatar :=
//        '237' + // Codigo banco
//        '9' + // Moeda
//        vDados.Agencia +
//        LPad(vDados.Carteira, 2, '0') +
//        vDados.NossoNumero +
//        vDados.Conta +
//        '0';
//
//      vDados.PrimeiroCampo :=
//        Copy(vDados.LinhaDigitavelSemFormatar, 1, 5) +
//        '.' +
//        Copy(vDados.LinhaDigitavelSemFormatar, 6, 4);
//
//      vDados.SeguntoCampo :=
//        Copy(vDados.LinhaDigitavelSemFormatar, 10, 5) +
//        '.' +
//        Copy(vDados.LinhaDigitavelSemFormatar, 15, 5);
//
//      vDados.TerceiroCampo :=
//        Copy(vDados.LinhaDigitavelSemFormatar, 20, 5) +
//        '.' +
//        Copy(vDados.LinhaDigitavelSemFormatar, 25, 5);
//
//
//      vDados.CodigoBarras :=
//        IIfStr( Em(vDados.Carteira, ['21','22']), '000', '237' ) +
//        '9'+
//        'X' +
//        vDados.FatorVencimento +
//        LPad(RetornaNumeros(sgBoletos.Cells[coValor, i]), 10, '0') +
//        vDados.Agencia +
//        vDados.Carteira +
//        vDados.NossoNumero +
//        vDados.Conta +
//        '0';
//
//      vDados.DigitoCodigoBarras := getDigitoVerifCodigoBarra(vDados.CodigoBarras);
//      vDados.CodigoBarras := AnsiReplaceStr(vDados.CodigoBarras, 'X', vDados.DigitoCodigoBarras);
//
//      SetLength(vBoletos, Length(vBoletos) + 1);
//
//      vBoletos[High(vBoletos)].DataVencimento               := vDados.Vencimento;
//      vBoletos[High(vBoletos)].Beneficiario                 := sgBoletos.Cells[coNomeEmpresa,i];
//      vBoletos[High(vBoletos)].DataEmissao                  := StrToDate(sgBoletos.Cells[coDataEmissao,i]) ;
//      vBoletos[High(vBoletos)].ReceberId                    := SFormatInt(sgBoletos.Cells[coReceberId,i]);
//      vBoletos[High(vBoletos)].NossoNumero                  := vDados.NossoNumero;
//      vBoletos[High(vBoletos)].ValorDocumento               := vDados.ValorDocumento;
//      vBoletos[High(vBoletos)].Logradouro                   := sgBoletos.Cells[coLogradouro, i];
//      vBoletos[High(vBoletos)].Cep                          := sgBoletos.Cells[coCep, i];
//      vBoletos[High(vBoletos)].CpfCnpj                      := sgBoletos.Cells[coCpfCnpj, i];
//      vBoletos[High(vBoletos)].Instrucao                    := vDados.Instrucao;
//      vBoletos[High(vBoletos)].Agencia                      := vDados.Agencia;
//      vBoletos[High(vBoletos)].DigitoAgencia                := vDados.DigitoAgencia;
//      vBoletos[High(vBoletos)].ContaCorrente                := vDados.Conta;
//      vBoletos[High(vBoletos)].DigitoContaCorrente          := vDados.DigitoConta;
//      vBoletos[High(vBoletos)].DigitoVerificadorNossoNumero := vCobranca.DigitoVerifNossoNumero(vDados.Carteira, vDados.NossoNumero, '', '');
//      vBoletos[High(vBoletos)].Cidade                       := sgBoletos.Cells[coNomeCidade, i];
//      vBoletos[High(vBoletos)].Estado                       := sgBoletos.Cells[coEstadoId, i];
//      vBoletos[High(vBoletos)].Pagador                      := sgBoletos.Cells[coNomeCliente, i];
//      vBoletos[High(vBoletos)].ClienteId                    := SFormatInt(sgBoletos.Cells[coClienteId, i]);
//      vBoletos[High(vBoletos)].Cliente                      := sgBoletos.Cells[coNomeCliente, i];
//      vBoletos[High(vBoletos)].CodigoBarras                 := vDados.CodigoBarras;
//      vBoletos[High(vBoletos)].Carteira                     := vDados.Carteira;
//      vBoletos[High(vBoletos)].EnderecoBeneficiario         := sgBoletos.Cells[coEnderecoEmpresa, i];
//      vBoletos[High(vBoletos)].CnpjBeneficiario             := sgBoletos.Cells[coCNPJEmpresa, i];
////      vBoletos[High(vBoletos)].NomeResumidoEmpresa := Cells[cNome_Resumido_Emp, i] +' - CNPJ: '+Cells[cCNPJ_Empresa,i]  ;
////      vBoletos[High(vBoletos)].TextoImpressao := Cells[cTexto,i];
////      vBoletos[High(vBoletos)].Observacao := Cells[cObservacao, i];
//
//      vBoletos[High(vBoletos)].LinhaDigitavel :=
//        vDados.PrimeiroCampo +
//        getDigitoVerificador(RemoverCaracter(vDados.PrimeiroCampo,'.')) + ' ' +
//        vDados.SeguntoCampo +
//        getDigitoVerificador(RemoverCaracter(vDados.SeguntoCampo,'.')) + ' ' +
//        vDados.TerceiroCampo +
//        getDigitoVerificador(RemoverCaracter(vDados.TerceiroCampo,'.')) + ' ' +
//        vDados.DigitoCodigoBarras + ' ' +
//        vDados.FatorVencimento +
//        LPad(RetornaNumeros(sgBoletos.Cells[coValor, i]), 10, '0');
//
//      vBoletos[High(vBoletos)].EmpresaId := SFormatInt(sgBoletos.Cells[coEmpresaId, i]);
//    end;

    if sgBoletos.Cells[coPortador, i] = '756' then begin
      vCobranca := TCobrancaBradesco.Create;

      vDados.Conta := LPad(vDados.Conta, 7, '0');
      vDados.NossoNumero := LPad(vDados.NossoNumero, 8, '0');

      vDados.NossoNumero := copy(vDados.NossoNumero, 1, 1) + 'X' + copy(vDados.NossoNumero, 2, 7);

      vDados.LinhaDigitavelSemFormatar :=
        '756' + // Codigo banco
        '9' + // Moeda
        vDados.Carteira +
        vDados.Agencia +
        'X' + //digito campo 1
        '01' + // C�digo da modalidade (necess�rio verificar o correto e colocar no cadastro da conta se necess�rio)
        vDados.Conta + // C�digo do beneficiario/cliente (necess�rio verificar o correto e colocar no cadastro da conta se necess�rio
        vDados.NossoNumero +  //digito campo 3 est� no meio do nosso n�mero
        '001' + //N�mero da parcela (verificar se tem parcelas e colocar corretamente)
        'X' +
        'X' + //digito c�digo barras
        vDados.FatorVencimento +
        LPad(RetornaNumeros(sgBoletos.Cells[coValor, i]), 10, '0');

      linha := DigitoLinhaDigitavel(vDados.LinhaDigitavelSemFormatar);

      vDados.PrimeiroCampo :=
        Copy(linha, 1, 5) +
        '.' +
        Copy(linha, 6, 4);

      vDados.SeguntoCampo :=
        Copy(linha, 10, 5) +
        '.' +
        Copy(linha, 15, 5);

      vDados.TerceiroCampo :=
        Copy(linha, 20, 5) +
        '.' +
        Copy(linha, 25, 5);
//
//
//      vDados.CodigoBarras :=
//        IIfStr( Em(vDados.Carteira, ['21','22']), '000', '756' ) +
//        '9'+
//        'X' +
//        vDados.FatorVencimento +
//        LPad(RetornaNumeros(sgBoletos.Cells[coValor, i]), 10, '0') +
//        vDados.Agencia +
//        vDados.Carteira +
//        vDados.NossoNumero +
//        vDados.Conta +
//        '0';

//      vDados.DigitoCodigoBarras := getDigitoVerifCodigoBarra(vDados.CodigoBarras);
//      vDados.CodigoBarras := AnsiReplaceStr(vDados.CodigoBarras, 'X', vDados.DigitoCodigoBarras);

      SetLength(vBoletos, Length(vBoletos) + 1);

      vBoletos[High(vBoletos)].DataVencimento               := vDados.Vencimento;
      vBoletos[High(vBoletos)].Beneficiario                 := sgBoletos.Cells[coNomeEmpresa,i];
      vBoletos[High(vBoletos)].DataEmissao                  := StrToDate(sgBoletos.Cells[coDataEmissao,i]) ;
      vBoletos[High(vBoletos)].ReceberId                    := SFormatInt(sgBoletos.Cells[coReceberId,i]);
      vBoletos[High(vBoletos)].NossoNumero                  := vDados.NossoNumero;
      vBoletos[High(vBoletos)].ValorDocumento               := vDados.ValorDocumento;
      vBoletos[High(vBoletos)].Logradouro                   := sgBoletos.Cells[coLogradouro, i];
      vBoletos[High(vBoletos)].Cep                          := sgBoletos.Cells[coCep, i];
      vBoletos[High(vBoletos)].CpfCnpj                      := sgBoletos.Cells[coCpfCnpj, i];
      vBoletos[High(vBoletos)].Instrucao                    := vDados.Instrucao;
      vBoletos[High(vBoletos)].Agencia                      := vDados.Agencia;
      vBoletos[High(vBoletos)].DigitoAgencia                := vDados.DigitoAgencia;
      vBoletos[High(vBoletos)].ContaCorrente                := vDados.Conta;
      vBoletos[High(vBoletos)].DigitoContaCorrente          := vDados.DigitoConta;
      vBoletos[High(vBoletos)].DigitoVerificadorNossoNumero := vCobranca.DigitoVerifNossoNumero(vDados.Carteira, vDados.NossoNumero, '', '');
      vBoletos[High(vBoletos)].Cidade                       := sgBoletos.Cells[coNomeCidade, i];
      vBoletos[High(vBoletos)].Estado                       := sgBoletos.Cells[coEstadoId, i];
      vBoletos[High(vBoletos)].Pagador                      := sgBoletos.Cells[coNomeCliente, i];
      vBoletos[High(vBoletos)].ClienteId                    := SFormatInt(sgBoletos.Cells[coClienteId, i]);
      vBoletos[High(vBoletos)].Cliente                      := sgBoletos.Cells[coNomeCliente, i];
      vBoletos[High(vBoletos)].CodigoBarras                 := linha;
      vBoletos[High(vBoletos)].Carteira                     := vDados.Carteira;
      vBoletos[High(vBoletos)].EnderecoBeneficiario         := sgBoletos.Cells[coEnderecoEmpresa, i];
      vBoletos[High(vBoletos)].CnpjBeneficiario             := sgBoletos.Cells[coCNPJEmpresa, i];

      vBoletos[High(vBoletos)].LinhaDigitavel :=
        Copy(linha, 1, 5) + '.' + Copy(linha, 6, 5) + ' ' +
        Copy(linha, 11, 5) + '.' + Copy(linha, 16, 6) + ' ' +
        Copy(linha, 22, 5) + '.' + Copy(linha, 27, 6) + ' ' +
        Copy(linha, 33, 1) + ' ' + Copy(linha, 34, 14);

      vBoletos[High(vBoletos)].EmpresaId := SFormatInt(sgBoletos.Cells[coEmpresaId, i]);
    end;
  end;

//  _BoletoBradesco.Emitir(vBoletos);
  _BoletoSicoob.Emitir(vBoletos);
end;

procedure TFormEmissaoBoletoBancario.sgBoletosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coImprimir, coParcela] then
    vAlinhamento := taCenter
  else if ACol in[coReceberId, coValor] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgBoletos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormEmissaoBoletoBancario.sgBoletosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if (ACol <> coImprimir) or (sgBoletos.Cells[ACol, ARow] = '') then
   Exit;

  if sgBoletos.Cells[ACol, ARow] = charNaoSelecionado then
    APicture := _Imagens.FormImagens.im1.Picture
  else if sgBoletos.Cells[ACol, ARow] = charSelecionado then
    APicture := _Imagens.FormImagens.im2.Picture;
end;

procedure TFormEmissaoBoletoBancario.sgBoletosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_SPACE then
    Exit;

  sgBoletos.Cells[coImprimir, sgBoletos.Row] := IfThen(sgBoletos.Cells[coImprimir, sgBoletos.Row] = charNaoSelecionado, charSelecionado, charNaoSelecionado);
end;

function TFormEmissaoBoletoBancario.DigitoCodigoBarra(codigobarra: string): string;
const
  indice = '43290876543298765432987654329876543298765432';
var
  soma, contador, digito: integer;
begin
  soma := 0;

  for contador := 1 to 44 do
    if contador <> 5 then
      soma := soma + (strtoint(codigobarra[contador]) * strtoint(indice[contador]));

  digito := 11 - (soma mod 11);

  if (digito <= 1) or (digito > 9) then digito := 1;

  //Colocar o digito no codigo barra

  codigobarra[5] := inttostr(digito)[1];

  result := codigobarra;

end;

function TFormEmissaoBoletoBancario.DigitoLinhaDigitavel(linhadigitavel: string): string;
const
  indice = '2121212120121212121201212121212';
var
  digito, soma, mult, contador: integer;
  codigobarra: string;
begin

  //c�lculo do primeiro d�gito

  soma := 0;

  for contador := 1 to 9 do

  begin

    mult := (strtoint(linhadigitavel[contador]) * strtoint(indice[contador]));

    if mult >= 10 then

      soma := soma + (strtoint(inttostr(mult)[1]) + strtoint(inttostr(mult)[2]))

    else

      soma := soma + mult;

  end;

  digito := multiplo10(soma) - soma;

  //Coloca o primeiro digito na linha digit�vel

  linhadigitavel[10] := inttostr(digito)[1];


  //c�lculo do segundo d�gito

  soma := 0;

  for contador := 11 to 20 do

  begin

    mult := (strtoint(linhadigitavel[contador]) * strtoint(indice[contador]));

    if mult >= 10 then

      soma := soma + (strtoint(inttostr(mult)[1]) + strtoint(inttostr(mult)[2]))

    else

      soma := soma + mult;

  end;

  digito := multiplo10(soma) - soma;

  //Coloca o segundo digito na linha digit�vel

  linhadigitavel[21] := inttostr(digito)[1];


  //c�lculo do terceiro d�gito

  soma := 0;

  for contador := 22 to 31 do

  begin

    mult := (strtoint(linhadigitavel[contador]) * strtoint(indice[contador]));

    if mult >= 10 then

      soma := soma + (strtoint(inttostr(mult)[1]) + strtoint(inttostr(mult)[2]))

    else

      soma := soma + mult;

  end;

  digito := multiplo10(soma) - soma;

  //Coloca o terceiro digito na linha digit�vel

  linhadigitavel[32] := inttostr(digito)[1];


  //Monta o codigo de barra para verificar o �ltimo d�gito

  codigobarra := copy(linhadigitavel,  1,  3) + //C�digo do Banco

                 copy(linhadigitavel,  4,  1) + //Moeda

                 copy(linhadigitavel, 33,  1) + //Digito Verificador

                 copy(linhadigitavel, 34,  4) + //fator de vencimento

                 copy(linhadigitavel, 38, 10) + //valor do documento

                 copy(linhadigitavel,  5,  1) + //Carteira

                 copy(linhadigitavel,  6,  4) + //Agencia

                 copy(linhadigitavel, 11,  2) + //Modalidade Cobranca

                 copy(linhadigitavel, 13,  7) + //C�digo do Cliente

                 copy(linhadigitavel, 20,  1) + copy(linhadigitavel, 22, 7) + //Nosso Numero

                 copy(linhadigitavel, 29,  3); //Parcela

  codigobarra := DigitoCodigoBarra(codigobarra);

  //Coloca o primeiro digito na linha digit�vel

  linhadigitavel[33] := codigobarra[5];

  result := linhadigitavel;
end;

function TFormEmissaoBoletoBancario.Multiplo10(numero: integer): integer;

begin

  while (numero mod 10) <> 0 do

    inc(numero);

  result := numero;

end;

end.

