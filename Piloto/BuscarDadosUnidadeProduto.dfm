inherited FormBuscarDadosUnidadeProduto: TFormBuscarDadosUnidadeProduto
  Caption = 'Buscar dados unidade produto'
  ClientHeight = 119
  ClientWidth = 406
  ExplicitWidth = 412
  ExplicitHeight = 148
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 82
    Width = 406
    ExplicitTop = 82
    ExplicitWidth = 406
    inherited sbFinalizar: TSpeedButton
      Left = 145
      ExplicitLeft = 145
    end
  end
  inline FrUnidadeVenda: TFrUnidades
    Left = 2
    Top = 4
    Width = 399
    Height = 75
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 2
    ExplicitTop = 4
    ExplicitWidth = 399
    ExplicitHeight = 75
    inherited sgPesquisa: TGridLuka
      Width = 374
      Height = 58
      TabOrder = 1
      ExplicitWidth = 374
      ExplicitHeight = 58
    end
    inherited CkAspas: TCheckBox
      TabOrder = 2
    end
    inherited CkFiltroDuplo: TCheckBox
      TabOrder = 4
    end
    inherited CkPesquisaNumerica: TCheckBox
      TabOrder = 5
    end
    inherited CkMultiSelecao: TCheckBox
      Left = 106
      TabOrder = 3
      ExplicitLeft = 106
    end
    inherited PnTitulos: TPanel
      Width = 399
      TabOrder = 0
      ExplicitWidth = 399
      inherited lbNomePesquisa: TLabel
        Width = 47
        Caption = 'Unidade'
        ExplicitWidth = 47
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 294
        ExplicitLeft = 294
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 374
      Height = 59
      ExplicitLeft = 374
      ExplicitHeight = 59
    end
  end
end
