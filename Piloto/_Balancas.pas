unit _Balancas;
interface
uses
  classes, system.AnsiStrings, system.SysUtils, WinApi.Windows, VCL.Forms;
type
  TBalancas = class
  public
    AbrePortaSerialBal: function(pCanal: ansistring): Integer;
    FechaPortaSerialBal: function(): Integer;
    AlteraModeloBalanca: function(Modelo: Integer): Integer;
    AlteraModoOperacao: function(Modo: Integer): Integer;
    LePeso: function(): PAnsiChar;
    EnviaPreco: function(p1: Integer; p2: Integer; p3: Integer; p4: Integer; p5: Integer; p6: Integer; cs1: Integer; cs2: Integer):Integer;
    CheckSum: function(p1: Integer; p2: Integer; p3: Integer; p4: Integer; p5: Integer; p6: Integer):Integer;
    constructor Create;
  end;
implementation
{ TBalancas }
constructor TBalancas.Create;
var
  FHandle : THandle;
  vLog : TStringList;
begin
  vLog := TStringList.Create;
  inherited Create;

  FHandle := LoadLibrary('LePeso.dll');
  if FHandle = 0 then
    raise Exception.Create('Erro ao carregar dll');

  try
    AbrePortaSerialBal := GetProcAddress(FHandle, '_AbrePortaSerialBal');
    FechaPortaSerialBal := GetProcAddress(FHandle, '_FechaPortaSerialBal');
    AlteraModeloBalanca := GetProcAddress(FHandle, '_AlteraModeloBalanca');
    AlteraModoOperacao := GetProcAddress(FHandle, '_AlteraModoOperacao');
    LePeso := GetProcAddress(FHandle, '_LePeso');
    EnviaPreco := GetProcAddress(FHandle, '_EnviaPreco');
    CheckSum := GetProcAddress(FHandle, '_CheckSum');
    if not Assigned(AbrePortaSerialBal) then
    begin
      raise Exception.Create('Dll n�o encontrada.');
      exit;
    end;
  except
    //FreeLibrary(FHandle);
  end;
end;
end.
