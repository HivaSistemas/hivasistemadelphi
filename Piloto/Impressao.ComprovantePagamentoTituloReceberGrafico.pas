unit Impressao.ComprovantePagamentoTituloReceberGrafico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosGraficos, QRExport, _Biblioteca,
  QRPDFFilt, QRWebFilt, QRCtrls, QuickRpt, Vcl.ExtCtrls, _Sessao, _ContasReceberBaixas,
  _RecordsFinanceiros, _ContasRecBaixasPagamentos, _ContasRecBaixasPagtosChq, _ContasReceberBaixasItens;

type
  RecCobrancas = record
    CobrancaId: Integer;
    NomeCobranca: string;
    Valor: Double;
    Dados: TArray<RecTitulosFinanceiros>;
  end;

  TFormImpressaoComprovantePagamentoTitulosReceberGrafico = class(TFormHerancaRelatoriosGraficos)
    TitleBand1: TQRBand;
    qr2: TQRLabel;
    qrBaixaId: TQRLabel;
    qr8: TQRLabel;
    qrCliente: TQRLabel;
    qr4: TQRLabel;
    qrTelefoneCliente: TQRLabel;
    qr14: TQRLabel;
    qrCelularCliente: TQRLabel;
    qr9: TQRLabel;
    qrDataBaixa: TQRLabel;
    qr28: TQRLabel;
    qr30: TQRLabel;
    qr31: TQRLabel;
    qr29: TQRLabel;
    qrTotalSerPago: TQRLabel;
    qrValorMulta: TQRLabel;
    qrValorAdiantado: TQRLabel;
    qrValorTitulos: TQRLabel;
    QRShape2: TQRShape;
    qr32: TQRLabel;
    qrReportDinheiro: TQuickRep;
    ColumnHeaderBand1: TQRBand;
    qr16: TQRLabel;
    qrValorDinheiro: TQRLabel;
    qrReportCheque: TQuickRep;
    QRBand1: TQRBand;
    qr12: TQRLabel;
    qr15: TQRLabel;
    qr18: TQRLabel;
    qr19: TQRLabel;
    qr20: TQRLabel;
    qr21: TQRLabel;
    qr22: TQRLabel;
    qr23: TQRLabel;
    DetailBand1: TQRBand;
    qrVencimentoCheque: TQRLabel;
    qrBancoCheque: TQRLabel;
    qrAgenciaCheque: TQRLabel;
    qrContaCheque: TQRLabel;
    qrNumeroCheque: TQRLabel;
    qrValorChq: TQRLabel;
    qrNomeDonoCheque: TQRLabel;
    qrTelefoneDonoCheque: TQRLabel;
    QRBand2: TQRBand;
    qr6: TQRLabel;
    qrValorCheque: TQRLabel;
    qrReportCartao: TQuickRep;
    QRBand3: TQRBand;
    qr24: TQRLabel;
    qrValorCrt: TQRLabel;
    qr26: TQRLabel;
    qr33: TQRLabel;
    QRBand4: TQRBand;
    qrTipoCobrancaCartao: TQRLabel;
    qrValorCartao: TQRLabel;
    qrReportCobranca: TQuickRep;
    QRBand5: TQRBand;
    qr25: TQRLabel;
    qrValorCob: TQRLabel;
    QRBand6: TQRBand;
    qrTipoCobranca: TQRLabel;
    qrValorTipoCobranca: TQRLabel;
    qr35: TQRLabel;
    qr34: TQRLabel;
    QRSubDetail1: TQRSubDetail;
    qrVencimentoCobranca: TQRLabel;
    qrValorCobranca: TQRLabel;
    qrReportCredito: TQuickRep;
    QRBand7: TQRBand;
    qr27: TQRLabel;
    qrValorCredito: TQRLabel;
    qrJuntar: TQRCompositeReport;
    qrReportFinal: TQuickRep;
    qrRodape: TQRBand;
    qrVersaoSistema: TQRLabel;
    qrChequesBaixados: TQuickRep;
    QRBand8: TQRBand;
    qr17: TQRLabel;
    qr36: TQRLabel;
    qr37: TQRLabel;
    qr38: TQRLabel;
    qr39: TQRLabel;
    qr40: TQRLabel;
    qr41: TQRLabel;
    qr42: TQRLabel;
    QRBand9: TQRBand;
    qrDataVencimentoChequeBaixado: TQRLabel;
    qrBancoChequeBaixado: TQRLabel;
    qrAgenciaChequeBaixado: TQRLabel;
    qrContaCorrenteChequeBaixado: TQRLabel;
    qrNumeroChequeBaixado: TQRLabel;
    qrValorChequeBaixado: TQRLabel;
    qrNomeDonoChequeBaixado: TQRLabel;
    qrTelefoneDonoChequeBaixado: TQRLabel;
    QRBand10: TQRBand;
    qr51: TQRLabel;
    qrValorTotalChequesBaixados: TQRLabel;
    qr53: TQRLabel;
    qr54: TQRLabel;
    qrJurosChequeBaixado: TQRLabel;
    qrValorTotalChequeBaixado: TQRLabel;
    qr57: TQRLabel;
    qrFinanceiroChequeBaixadoId: TQRLabel;
    qrFormasPagamento: TQuickRep;
    TitleBand2: TQRBand;
    QRShape1: TQRShape;
    qr43: TQRLabel;
    qr44: TQRLabel;
    qrValorJuros: TQRLabel;
    qr45: TQRLabel;
    qrValorRetencao: TQRLabel;
    qr47: TQRLabel;
    qrValorDesconto: TQRLabel;
    qrReportPix: TQuickRep;
    QRBand11: TQRBand;
    QRLabel1: TQRLabel;
    qrValorPix: TQRLabel;
    procedure qrJuntarAddReports(Sender: TObject);
    procedure qrChequesBaixadosBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure qrChequesBaixadosNeedData(Sender: TObject; var MoreData: Boolean);
    procedure QRBand9BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qrReportCartaoBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure qrReportCartaoNeedData(Sender: TObject; var MoreData: Boolean);
    procedure QRBand4BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qrReportChequeBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure qrReportChequeNeedData(Sender: TObject; var MoreData: Boolean);
    procedure DetailBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qrReportCobrancaBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure qrReportCobrancaNeedData(Sender: TObject; var MoreData: Boolean);
    procedure QRBand6BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure QRSubDetail1NeedData(Sender: TObject; var MoreData: Boolean);
    procedure QRSubDetail1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
  private
    FReceberBaixaId: Integer;

    FPosicCheque: Integer;
    FCheques: TArray<RecTitulosFinanceiros>;

    FPosicCartao: Integer;
    FCartoes: TArray<RecTitulosFinanceiros>;

    FPosicCobrancas: Integer;
    FPosicItensCobr: Integer;
    FPosicChequeBaixado: Integer;

    FCobrancas: TArray<RecCobrancas>;
    FChequesBaixados: TArray<RecContaReceberBaixaItem>;
  public
    { Public declarations }
  end;

function Imprimir(pReceberBaixaId: Integer): Boolean;

implementation

{$R *.dfm}

function Imprimir(pReceberBaixaId: Integer): Boolean;
var
  vImpressora: RecImpressora;
  vBaixa: TArray<RecContaReceberBaixa>;
  vForm: TFormImpressaoComprovantePagamentoTitulosReceberGrafico;

  function getValorBaixadoCheques: Currency;
  var
    i: Integer;
  begin
    Result := 0;
    for i := Low(vForm.FChequesBaixados) to High(vForm.FChequesBaixados) do
      Result := Result + vForm.FChequesBaixados[i].valor_documento;
  end;

begin
  Result := False;
  if pReceberBaixaId = 0 then
    Exit;

  vBaixa := _ContasReceberBaixas.BuscarContaReceberBaixas(Sessao.getConexaoBanco, 0, [pReceberBaixaId]);
  if vBaixa = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vImpressora := Sessao.getImpressora( toComprovantePagtoFinanceiro );
  if vImpressora.CaminhoImpressora = '' then begin
    _Biblioteca.ImpressoraNaoConfigurada;
    Exit;
  end;

  vForm := TFormImpressaoComprovantePagamentoTitulosReceberGrafico.Create(Application, vImpressora);
  vForm.PreencherCabecalho(Sessao.getEmpresaLogada.EmpresaId);

//  vForm.FChequesBaixados := _ContasReceberBaixasItens.BuscarContaReceberBaixaItens(Sessao.getConexaoBanco, 0, [pReceberBaixaId]);

  vForm.FReceberBaixaId := pReceberBaixaId;

  vForm.qrBaixaId.Caption             := NFormat(vBaixa[0].BaixaId);
  vForm.qrDataBaixa.Caption           := DHFormat(vBaixa[0].data_hora_baixa);
  vForm.qrCliente.Caption             := NFormat(vBaixa[0].CadastroId) + ' - ' + vBaixa[0].NomeCliente;
//  vForm.qrTelefoneCliente.Caption     := vBaixa[0].t;
//  vForm.qrCelularCliente.Caption      := vBaixa[0].telefone_celular;
  vForm.qrValorTitulos.Caption        := NFormat(vBaixa[0].ValorTitulos);
  vForm.qrValorMulta.Caption          := NFormat(vBaixa[0].ValorMulta);
  vForm.qrValorJuros.Caption          := NFormat(vBaixa[0].ValorJuros);
  vForm.qrValorRetencao.Caption       := NFormat(vBaixa[0].ValorRetencao);
  vForm.qrValorAdiantado.Caption      := NFormat(vBaixa[0].ValorAdiantado);
  vForm.qrValorDesconto.Caption       := NFormat(vBaixa[0].valor_desconto);
  vForm.qrTotalSerPago.Caption        := NFormat(vBaixa[0].ValorLiquido);
  vForm.qrValorDinheiro.Caption       := NFormatN(vBaixa[0].valor_dinheiro);
  vForm.qrValorCheque.Caption         := NFormatN(vBaixa[0].valor_cheque);
  vForm.qrValorCrt.Caption            := NFormatN(vBaixa[0].ValorCartaoDebito);
  vForm.qrValorCob.Caption            := NFormatN(vBaixa[0].valor_cobranca);
  vForm.qrValorCredito.Caption        := NFormatN(vBaixa[0].valor_credito);
  vForm.qrValorPix.Caption            := NFormatN(vBaixa[0].valor_pix);

  vForm.qrValorTotalChequesBaixados.Caption := NFormatN( getValorBaixadoCheques );
  vForm.qrVersaoSistema.Caption := Sessao.getVersaoSistema;
  vForm.qrUsuarioImpressao.Caption := Sessao.getUsuarioLogado.nome;

  if vForm.ImpressaoGrafica then begin
    vForm.qrJuntar.PrinterSettings.PrinterIndex := vImpressora.getIndex;
    if vImpressora.AbrirPreview then
      vForm.qrJuntar.Preview
    else
      vForm.qrJuntar.Print;
  end
  else begin

  end;

  vForm.Free;
end;

procedure TFormImpressaoComprovantePagamentoTitulosReceberGrafico.DetailBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrVencimentoCheque.Caption   := DFormat( FCheques[FPosicCheque].DataVencimento );
  qrBancoCheque.Caption        := FCheques[FPosicCheque].Banco;
  qrAgenciaCheque.Caption      := FCheques[FPosicCheque].Agencia;
  qrContaCheque.Caption        := FCheques[FPosicCheque].ContaCorrente;
  qrNumeroCheque.Caption       := NFormat( FCheques[FPosicCheque].NumeroCheque );
  qrValorChq.Caption           := NFormat( FCheques[FPosicCheque].Valor );
  qrNomeDonoCheque.Caption     := FCheques[FPosicCheque].NomeEmitente;
  qrTelefoneDonoCheque.Caption := FCheques[FPosicCheque].TelefoneEmitente;
end;

procedure TFormImpressaoComprovantePagamentoTitulosReceberGrafico.QRBand4BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrTipoCobrancaCartao.Caption := NFormat( FCartoes[FPosicCartao].CobrancaId ) + ' - ' + FCartoes[FPosicCartao].NomeCobranca;
  qrValorCartao.Caption        := NFormat( FCartoes[FPosicCartao].Valor );
end;

procedure TFormImpressaoComprovantePagamentoTitulosReceberGrafico.QRBand6BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  FPosicItensCobr := -1;
  qrTipoCobranca.Caption := NFormat(FCobrancas[FPosicCobrancas].CobrancaId) + ' - ' + FCobrancas[FPosicCobrancas].NomeCobranca;
  qrValorTipoCobranca.Caption := NFormat(FCobrancas[FPosicCobrancas].Valor);
end;

procedure TFormImpressaoComprovantePagamentoTitulosReceberGrafico.QRBand9BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrFinanceiroChequeBaixadoId.Caption   := NFormat( FChequesBaixados[FPosicChequeBaixado].receber_id );
  qrDataVencimentoChequeBaixado.Caption := DFormat( FChequesBaixados[FPosicChequeBaixado].data_vencimento );
  qrBancoChequeBaixado.Caption          := NFormat( FChequesBaixados[FPosicChequeBaixado].Banco );
  qrAgenciaChequeBaixado.Caption        := NFormat( FChequesBaixados[FPosicChequeBaixado].Agencia );
  qrContaCorrenteChequeBaixado.Caption  := FChequesBaixados[FPosicChequeBaixado].ContaCorrente;
  qrNumeroChequeBaixado.Caption         := NFormat( FChequesBaixados[FPosicChequeBaixado].NumeroCheque );
  qrValorChequeBaixado.Caption          := NFormatN( FChequesBaixados[FPosicChequeBaixado].valor_documento );
  qrJurosChequeBaixado.Caption          := '0,00'; //ChequesBaixados[FPosicChequeBaixado].ValorJuros;
  qrValorTotalChequeBaixado.Caption     := NFormat( FChequesBaixados[FPosicChequeBaixado].valor_documento );
  qrNomeDonoChequeBaixado.Caption       := FChequesBaixados[FPosicChequeBaixado].NomeEmitente;
  qrTelefoneDonoChequeBaixado.Caption   := FChequesBaixados[FPosicChequeBaixado].TelefoneEmitente;
end;

procedure TFormImpressaoComprovantePagamentoTitulosReceberGrafico.qrChequesBaixadosBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicChequeBaixado := -1;
end;

procedure TFormImpressaoComprovantePagamentoTitulosReceberGrafico.qrChequesBaixadosNeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicChequeBaixado);
  MoreData := FPosicChequeBaixado < Length(FChequesBaixados);
end;

procedure TFormImpressaoComprovantePagamentoTitulosReceberGrafico.qrJuntarAddReports(Sender: TObject);
var
  i: Integer;
  vCobrancaId: Integer;
  vCobrancas: TArray<RecTitulosFinanceiros>;
begin
  inherited;
  qrJuntar.Reports.Add(qrRelatorioA4);

  (* ----- Primeiro os t�tulos baixados ----- *)
  if qrValorTotalChequesBaixados.Caption <> '' then
    qrJuntar.Reports.Add(qrChequesBaixados);

  (* ----- Fim dos t�tulos baixados ----- *)

  qrJuntar.Reports.Add(qrFormasPagamento);

  if qrValorDinheiro.Caption <> '' then
    qrJuntar.Reports.Add(qrReportDinheiro);

  if qrValorPix.Caption <> '' then
    qrJuntar.Reports.Add(qrReportPix);

  if qrValorCheque.Caption <> '' then begin
    qrJuntar.Reports.Add(qrReportCheque);
    if FCheques = nil then
      FCheques := _ContasRecBaixasPagtosChq.BuscarContasRecBaixasPagtosChqs(Sessao.getConexaoBanco, 0, [FReceberBaixaId]);
  end;

  if qrValorCrt.Caption <> '' then begin
    qrJuntar.Reports.Add(qrReportCartao);
    if FCartoes = nil then
      FCartoes := _ContasRecBaixasPagamentos.BuscarContasRecBaixasPagamentos(Sessao.getConexaoBanco, 1, [FReceberBaixaId]);
  end;

  if qrValorCob.Caption <> '' then begin
    vCobrancaId := -1;
    qrJuntar.Reports.Add(qrReportCobranca);
    if FCobrancas = nil then begin
      vCobrancas := _ContasRecBaixasPagamentos.BuscarContasRecBaixasPagamentos(Sessao.getConexaoBanco, 3, [FReceberBaixaId]);
      for i := Low(vCobrancas) to High(vCobrancas) do begin
        if vCobrancaId <> vCobrancas[i].CobrancaId then begin
          vCobrancaId := vCobrancas[i].CobrancaId;
          SetLength(FCobrancas, Length(FCobrancas) + 1);
          FCobrancas[High(FCobrancas)].Valor        := vCobrancas[i].valor;
          FCobrancas[High(FCobrancas)].CobrancaId   := vCobrancas[i].CobrancaId;
          FCobrancas[High(FCobrancas)].NomeCobranca := vCobrancas[i].NomeCobranca;
        end
        else
          FCobrancas[High(FCobrancas)].Valor := vCobrancas[i].valor;

        SetLength(FCobrancas[High(FCobrancas)].Dados, Length(FCobrancas[High(FCobrancas)].Dados) + 1);
        FCobrancas[High(FCobrancas)].Dados[High(FCobrancas[High(FCobrancas)].Dados)] := vCobrancas[i];
      end;
    end;
  end;

  if qrValorCredito.Caption <> '' then
    qrJuntar.Reports.Add(qrReportCredito);

  qrJuntar.Reports.Add(qrReportFinal);
end;

procedure TFormImpressaoComprovantePagamentoTitulosReceberGrafico.qrReportCartaoBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicCartao := -1;
end;

procedure TFormImpressaoComprovantePagamentoTitulosReceberGrafico.qrReportCartaoNeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicCartao);
  MoreData := FPosicCartao < Length(FCartoes);
end;

procedure TFormImpressaoComprovantePagamentoTitulosReceberGrafico.qrReportChequeBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicCheque := -1;
end;

procedure TFormImpressaoComprovantePagamentoTitulosReceberGrafico.qrReportChequeNeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicCheque);
  MoreData := FPosicCheque < Length(FCheques);
end;

procedure TFormImpressaoComprovantePagamentoTitulosReceberGrafico.qrReportCobrancaBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicCobrancas := -1;
end;

procedure TFormImpressaoComprovantePagamentoTitulosReceberGrafico.qrReportCobrancaNeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicCobrancas);
  MoreData := FPosicCobrancas < Length(FCobrancas);
end;

procedure TFormImpressaoComprovantePagamentoTitulosReceberGrafico.QRSubDetail1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrVencimentoCobranca.Caption := DFormat(FCobrancas[FPosicCobrancas].Dados[FPosicItensCobr].DataVencimento);
  qrValorCobranca.Caption      := NFormat(FCobrancas[FPosicCobrancas].Dados[FPosicItensCobr].valor);
end;

procedure TFormImpressaoComprovantePagamentoTitulosReceberGrafico.QRSubDetail1NeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicItensCobr);
  MoreData := FPosicItensCobr < Length(FCobrancas[FPosicCobrancas].Dados);
end;

end.
