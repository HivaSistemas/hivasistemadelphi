unit Informacoes.TituloReceber;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Sessao, _Biblioteca,
  Vcl.ExtCtrls, EditTelefoneLuka, Vcl.Mask, EditLukaData, Vcl.StdCtrls, EditLuka, _ContasReceber,
  _RecordsFinanceiros, System.Math, System.StrUtils, System.DateUtils, Informacoes.Orcamento,
  Informacoes.ContasReceberBaixa, Informacoes.TurnoCaixa, Vcl.ComCtrls, SpeedButtonLuka,
  Vcl.Grids, GridLuka, _ContasReceberBaixas, Vcl.Menus, BuscaDados, _ContasRecHistoricoCobrancas,
  _RecordsEspeciais, MemoAltis, EditCpfCnpjLuka;

type
  TFormInformacoesTituloReceber = class(TFormHerancaFinalizar)
    pcDados: TPageControl;
    tsPrincipais: TTabSheet;
    tsBaixa: TTabSheet;
    lb1: TLabel;
    lb2: TLabel;
    lb13: TLabel;
    lb12: TLabel;
    lb3: TLabel;
    lb5: TLabel;
    lb6: TLabel;
    lb10: TLabel;
    lb11: TLabel;
    lb14: TLabel;
    lb22: TLabel;
    lb23: TLabel;
    lb26: TLabel;
    lb28: TLabel;
    lb30: TLabel;
    lb31: TLabel;
    eReceberId: TEditLuka;
    eCliente: TEditLuka;
    eEmpresa: TEditLuka;
    eVendedor: TEditLuka;
    eTipoCobranca: TEditLuka;
    eValorTotalTitulo: TEditLuka;
    eDataCadastro: TEditLukaData;
    eDiasAtraso: TEditLuka;
    eDataVencimento: TEditLukaData;
    eDataVencimentoOriginal: TEditLukaData;
    st3: TStaticText;
    stStatus: TStaticText;
    eValorTitulo: TEditLuka;
    eValorJuros: TEditLuka;
    eNomePortador: TEditLuka;
    eUsuarioCadastro: TEditLuka;
    eParcela: TEditLuka;
    eNumeroParcelas: TEditLuka;
    lb25: TLabel;
    eBaixaOrigemId: TEditLuka;
    sbInformacoesOrigemBaixa: TSpeedButton;
    eBaixaId: TEditLuka;
    lb19: TLabel;
    sbInformacoesBaixa: TSpeedButton;
    eNomeUsuarioBaixa: TEditLuka;
    lb24: TLabel;
    eDataBaixa: TEditLukaData;
    lb7: TLabel;
    eDataPagamento: TEditLukaData;
    lb8: TLabel;
    tsCheque: TTabSheet;
    lb15: TLabel;
    lb16: TLabel;
    lb17: TLabel;
    lb18: TLabel;
    lb20: TLabel;
    eBanco: TEditLuka;
    eAgencia: TEditLuka;
    eContaCorrente: TEditLuka;
    eNumeroCheque: TEditLuka;
    eNomeEmitente: TEditLuka;
    lb21: TLabel;
    eTelefoneEmitente: TEditTelefoneLuka;
    tsTEF: TTabSheet;
    lb4: TLabel;
    eNSU: TEditLuka;
    Label1: TLabel;
    meObservacao: TMemo;
    Label2: TLabel;
    eCentroCusto: TEditLuka;
    sbInformacoesTurno: TSpeedButton;
    eTurnoId: TEditLuka;
    lb27: TLabel;
    sbInformacoesNotaFiscal: TSpeedButton;
    eNotaFiscalId: TEditLuka;
    lb29: TLabel;
    sbInfoOrcamentoId: TSpeedButton;
    eOrcamentoId: TEditLuka;
    lb9: TLabel;
    eValorRetencao: TEditLuka;
    lb32: TLabel;
    sbLogs: TSpeedButtonLuka;
    lb33: TLabel;
    eNumeroCartaoTruncado: TEditLuka;
    tsBoleto: TTabSheet;
    lb34: TLabel;
    eNomeArquivoRemessa: TEditLuka;
    eNossoNumero: TEditLuka;
    lb35: TLabel;
    lb36: TLabel;
    lb37: TLabel;
    eDataEmissao: TEditLukaData;
    eDataContabil: TEditLukaData;
    lb38: TLabel;
    eRemessaId: TEditLuka;
    lb39: TLabel;
    eCodigoAutorizacao: TEditLuka;
    lb40: TLabel;
    eAcumuladoId: TEditLuka;
    sbInformacoesAcumulado: TSpeedButton;
    eValorAdiantado: TEditLuka;
    lb41: TLabel;
    tsAdiantamentos: TTabSheet;
    sgAdiantamentos: TGridLuka;
    tsHistoricoCobrancas: TTabSheet;
    sgHistoricoCob: TGridLuka;
    pmOpcoesHistorico: TPopupMenu;
    miInserirNovoHistorico: TMenuItem;
    pn1: TPanel;
    meHistorico: TMemoAltis;
    lb42: TLabel;
    eCPF_CNPJEmitente: TEditCPF_CNPJ_Luka;
    lbCPF_CNPJ: TLabel;
    procedure sbInfoOrcamentoIdClick(Sender: TObject);
    procedure sbInformacoesBaixaClick(Sender: TObject);
    procedure sbInformacoesOrigemBaixaClick(Sender: TObject);
    procedure sbInformacoesTurnoClick(Sender: TObject);
    procedure sbInformacoesNotaFiscalClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sbLogsClick(Sender: TObject);
    procedure sbInformacoesAcumuladoClick(Sender: TObject);
    procedure tsAdiantamentosShow(Sender: TObject);
    procedure sgAdiantamentosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgAdiantamentosClick(Sender: TObject);
    procedure miInserirNovoHistoricoClick(Sender: TObject);
    procedure tsHistoricoCobrancasShow(Sender: TObject);
    procedure sgHistoricoCobDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgHistoricoCobClick(Sender: TObject);
  end;

procedure Informar(pReceberId: Integer);

implementation

{$R *.dfm}

uses
  Logs, InformacoesAcumulado;

const
  coBaixaAdiantamentoId  = 0;
  coDataHoraAdiantamento = 1;
  coValorAdiantamento    = 2;
  coUsuarioAdiantamento  = 3;

  (* Grid de hist�rico de cobran�as *)
  chSequencia        = 0;
  chDataHoraCobranca = 1;
  chUsuarioCobranca  = 2;
  chDescricao        = 3;

procedure Informar(pReceberId: Integer);
var
  vTitulo: TArray<RecContasReceber>;
  vForm: TFormInformacoesTituloReceber;
begin
  if pReceberId = 0 then
    Exit;

  vTitulo := _ContasReceber.BuscarContasReceber(Sessao.getConexaoBanco, 0, [pReceberId]);
  if vTitulo = nil then begin
    Exclamar('Informa��es do t�tulo n�o foram encontradas!');
    Exit;
  end;

  vForm := TFormInformacoesTituloReceber.Create(Application);

  vForm.eReceberId.AsInt    := pReceberId;
  vForm.eOrcamentoId.SetInformacao(vTitulo[0].orcamento_id);
  vForm.eCliente.SetInformacao(vTitulo[0].CadastroId, vTitulo[0].NomeCliente);
  vForm.eVendedor.SetInformacao(vTitulo[0].vendedor_id, vTitulo[0].nome_vendedor);
  vForm.eEmpresa.Text       := NFormat(vTitulo[0].empresa_id) + ' - ' + vTitulo[0].nome_empresa;
  vForm.stStatus.Caption    := IfThen(vTitulo[0].status = 'A', 'Aberto', 'Baixado');
  vForm.stStatus.Font.Color := IfThen(vTitulo[0].status = 'A', $000096DB, clBlue);
  vForm.eTipoCobranca.Text  := NFormat(vTitulo[0].cobranca_id) + ' - ' + vTitulo[0].nome_tipo_cobranca;

  vForm.eDataCadastro.AsData           := vTitulo[0].data_cadastro;
  vForm.eDataEmissao.AsData            := vTitulo[0].data_emissao;
  vForm.eDataContabil.AsData           := vTitulo[0].DataContabil;
  vForm.eDataVencimento.AsData         := vTitulo[0].data_vencimento;
  vForm.eDataVencimentoOriginal.AsData := vTitulo[0].data_vencimento_original;
  vForm.eDiasAtraso.AsInt              := vTitulo[0].dias_atraso;
  vForm.eValorTitulo.AsDouble          := vTitulo[0].ValorDocumento;
  vForm.eValorAdiantado.AsDouble       := vTitulo[0].ValorAdiantado;
  vForm.eValorRetencao.AsDouble        := vTitulo[0].ValorRetencao;
  vForm.eValorTotalTitulo.AsDouble     := vTitulo[0].ValorDocumento - vTitulo[0].ValorRetencao - vTitulo[0].ValorAdiantado;
  vForm.eDataPagamento.AsData          := vTitulo[0].data_pagamento;
  vForm.eDataBaixa.AsData              := vTitulo[0].data_hora_baixa;
  vForm.eParcela.AsInt                 := vTitulo[0].Parcela;
  vForm.eNumeroParcelas.AsInt          := vTitulo[0].NumeroParcelas;
  vForm.eUsuarioCadastro.Text          := NFormat(vTitulo[0].UsuarioCadastroId) + ' - ' + vTitulo[0].NomeUsuarioCadastro;

  vForm.eTurnoId.SetInformacao(vTitulo[0].turno_id);
  vForm.eNotaFiscalId.SetInformacao(vTitulo[0].nota_fiscal_id);
  vForm.eBaixaOrigemId.SetInformacao(vTitulo[0].baixa_origem_id);
  vForm.eBaixaId.SetInformacao(vTitulo[0].baixa_id);
  vForm.eAcumuladoId.SetInformacao(vTitulo[0].AcumuladoId);
  vForm.eNomeUsuarioBaixa.SetInformacao(vTitulo[0].usuario_baixa_id, vTitulo[0].nome_usuario_baixa);

  vForm.eNomePortador.SetInformacao(vTitulo[0].PortadorId, vTitulo[0].NomePortador);
  vForm.eCentroCusto.SetInformacao(vTitulo[0].CentroCustoId, vTitulo[0].NomeCentroCusto);

  vForm.eBanco.AsInt           := vTitulo[0].banco;
  vForm.eAgencia.Text          := vTitulo[0].agencia;
  vForm.eContaCorrente.Text    := vTitulo[0].conta_corrente;
  vForm.eNumeroCheque.AsInt    := vTitulo[0].numero_cheque;
  vForm.eNomeEmitente.Text     := vTitulo[0].nome_emitente;
  vForm.eCPF_CNPJEmitente.Text := vTitulo[0].CpfCnpjEmitente;
  vForm.eTelefoneEmitente.Text := vTitulo[0].telefone_emitente;
  vForm.meObservacao.Text      := vTitulo[0].observacoes;

  vForm.eNSU.Text                  := vTitulo[0].nsu;
  vForm.eNumeroCartaoTruncado.Text := vTitulo[0].NumeroCartaoTruncado;
  vForm.eCodigoAutorizacao.Text    := vTitulo[0].codigo_autorizacao_tef;

  vForm.eRemessaId.AsInt         := vTitulo[0].RemessaId;
  vForm.eNossoNumero.Text        := vTitulo[0].NossoNumero;
  vForm.eNomeArquivoRemessa.Text := vTitulo[0].NomeArquivoRemessa;

  vForm.ShowModal;

  vForm.Free;
end;

procedure TFormInformacoesTituloReceber.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(eReceberId);
end;

procedure TFormInformacoesTituloReceber.miInserirNovoHistoricoClick(Sender: TObject);
var
  vTexto: string;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vTexto := BuscaDados.BuscarDados('Hist�rico', '', 800);
  if vTexto = '' then
    Exit;

  vTexto := Trim(vTexto);
  if Length(vTexto) < 15 then begin
    _Biblioteca.Exclamar('O hist�rico deve ser maior que 14 caracteres!');
    Exit;
  end;

  vRetBanco :=
    _ContasRecHistoricoCobrancas.AtualizarContasRecHistoricoCobrancas(
      Sessao.getConexaoBanco,
      eReceberId.AsInt,
      vTexto
    );

  Sessao.AbortarSeHouveErro(vRetBanco);
  _Biblioteca.RotinaSucesso;

  tsHistoricoCobrancasShow(Sender);
end;

procedure TFormInformacoesTituloReceber.sbInfoOrcamentoIdClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar(eOrcamentoId.IdInformacao);
end;

procedure TFormInformacoesTituloReceber.sbInformacoesAcumuladoClick(Sender: TObject);
begin
  inherited;
  InformacoesAcumulado.Informar(eAcumuladoId.IdInformacao);
end;

procedure TFormInformacoesTituloReceber.sbInformacoesBaixaClick(Sender: TObject);
begin
  inherited;
  Informacoes.ContasReceberBaixa.Informar(eBaixaId.IdInformacao);
end;

procedure TFormInformacoesTituloReceber.sbInformacoesNotaFiscalClick(Sender: TObject);
begin
  inherited;
//Informacoes.NotasFiscais.Inforamar(eNotaFiscalId.IdInformacao);
end;

procedure TFormInformacoesTituloReceber.sbInformacoesOrigemBaixaClick(Sender: TObject);
begin
  inherited;
  Informacoes.ContasReceberBaixa.Informar(eBaixaOrigemId.IdInformacao);
end;

procedure TFormInformacoesTituloReceber.sbInformacoesTurnoClick(Sender: TObject);
begin
  inherited;
  Informacoes.TurnoCaixa.Informar(eTurnoId.IdInformacao);
end;

procedure TFormInformacoesTituloReceber.sbLogsClick(Sender: TObject);
begin
  inherited;
  Logs.Iniciar('LOGS_CONTAS_RECEBER', ['RECEBER_ID'], 'VW_TIPOS_ALTER_LOGS_CT_RECEBER', [eReceberId.AsInt]);
end;

procedure TFormInformacoesTituloReceber.sgAdiantamentosClick(Sender: TObject);
begin
  inherited;
  Informacoes.ContasReceberBaixa.Informar( SFormatInt(sgAdiantamentos.Cells[coBaixaAdiantamentoId, sgAdiantamentos.Row]) );
end;

procedure TFormInformacoesTituloReceber.sgAdiantamentosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coBaixaAdiantamentoId, coValorAdiantamento] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgAdiantamentos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesTituloReceber.sgHistoricoCobClick(Sender: TObject);
begin
  inherited;
  meHistorico.Lines.Text := sgHistoricoCob.Cells[chDescricao, sgHistoricoCob.Row];
end;

procedure TFormInformacoesTituloReceber.sgHistoricoCobDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[chSequencia] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgHistoricoCob.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesTituloReceber.tsAdiantamentosShow(Sender: TObject);
var
  i: Integer;
  vBaixas: TArray<RecContaReceberBaixa>;
begin
  inherited;
  sgAdiantamentos.ClearGrid();

  vBaixas := _ContasReceberBaixas.BuscarContaReceberBaixas(Sessao.getConexaoBanco, 2, [eReceberId.AsInt]);
  if vBaixas = nil then
    Exit;

  for i := Low(vBaixas) to High(vBaixas) do begin
    sgAdiantamentos.Cells[coBaixaAdiantamentoId, i + 1]  := NFormat(vBaixas[i].BaixaId);
    sgAdiantamentos.Cells[coDataHoraAdiantamento, i + 1] := DHFormat(vBaixas[i].data_hora_baixa);
    sgAdiantamentos.Cells[coValorAdiantamento, i + 1]    := NFormat(vBaixas[i].ValorTitulos);
    sgAdiantamentos.Cells[coUsuarioAdiantamento, i + 1]  := getInformacao(vBaixas[i].usuario_baixa_id, vBaixas[i].nome_usuario_baixa);
  end;
  sgAdiantamentos.SetLinhasGridPorTamanhoVetor( Length(vBaixas) );
end;

procedure TFormInformacoesTituloReceber.tsHistoricoCobrancasShow(Sender: TObject);
var
  i: Integer;
  vHistoricos: TArray<RecContasRecHistoricoCobrancas>;
begin
  inherited;
  sgHistoricoCob.ClearGrid;

  vHistoricos := _ContasRecHistoricoCobrancas.BuscarContasRecHistoricoCobrancas(Sessao.getConexaoBanco, 0, [eReceberId.AsInt]);
  if vHistoricos = nil then
    Exit;

  for i := Low(vHistoricos) to High(vHistoricos) do begin
    sgHistoricoCob.Cells[chSequencia, i + 1]        := NFormat(vHistoricos[i].ItemId);
    sgHistoricoCob.Cells[chDataHoraCobranca, i + 1] := DHFormat(vHistoricos[i].DataHoraCobranca);
    sgHistoricoCob.Cells[chUsuarioCobranca, i + 1]  := getInformacao(vHistoricos[i].UsuarioCobrancaId, vHistoricos[i].NomeUsuarioCobranca);
    sgHistoricoCob.Cells[chDescricao, i + 1]        := vHistoricos[i].Historico;
  end;
  sgHistoricoCob.SetLinhasGridPorTamanhoVetor( Length(vHistoricos) );

  sgHistoricoCobClick(Sender);
end;

end.
