unit Pesquisa.Cadastros;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _Cadastros, _Sessao,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, System.Math, _RecordsCadastros,
  _Biblioteca, Vcl.ExtCtrls;

type
  TFormPesquisaCadastros = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  protected
    procedure BuscarRegistros; override;
  end;

function Pesquisar(pSomenteAtivos: Boolean): TObject;
function PesquisarVarios(pSomenteAtivos: Boolean): TArray<TObject>;

implementation

{$R *.dfm}

const
  coRazaoSocial     = 2;
  coNomeFantasia    = 3;
  coCPFCNPJ         = 4;
  coLogradouro      = 5;
  coComplemento     = 6;
  coBairro          = 7;
  coCidadeUf        = 8;
  coEmail           = 9;
  coECliente        = 10;
  coEFornecedor     = 11;
  coEMotorista      = 12;
  coETransportadora = 13;
  coEProfissional   = 14;
  coAtivo           = 15;

function Pesquisar(pSomenteAtivos: Boolean): TObject;
var
  vRet: TObject;
begin
  vRet := _HerancaPesquisas.Pesquisar(TFormPesquisaCadastros, _Cadastros.GetFiltros);
  if vRet = nil then
    Result := nil
  else
    Result := RecCadastros(vRet);
end;

function PesquisarVarios(pSomenteAtivos: Boolean): TArray<TObject>;
var
  vRet: TArray<TObject>;
begin
  vRet := _HerancaPesquisas.PesquisarVarios(TFormPesquisaCadastros, _Cadastros.GetFiltros);
  if vRet = nil then
    Result := nil
  else
    Result := TArray<TObject>(vRet);
end;

procedure TFormPesquisaCadastros.BuscarRegistros;
var
  i: Integer;
  vClientes: TArray<RecCadastros>;
begin
  inherited;
  sgPesquisa.ClearGrid();

  vClientes :=
    _Cadastros.BuscarCadastros(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text]
    );

  if vClientes = nil then begin
    _Biblioteca.NenhumRegistro;
    SetarFoco(eValorPesquisa);
    Exit;
  end;

  FDados := TArray<TObject>(vClientes);

  for i := Low(vClientes) to High(vClientes) do begin
    sgPesquisa.Cells[coSelecionado, i + 1]     := _Biblioteca.charNaoSelecionado;
    sgPesquisa.Cells[coCodigo, i + 1]          := NFormat(vClientes[i].cadastro_id);
    sgPesquisa.Cells[coNomeFantasia, i + 1]    := vClientes[i].nome_fantasia;
    sgPesquisa.Cells[coRazaoSocial, i + 1]     := vClientes[i].razao_social;
    sgPesquisa.Cells[coCPFCNPJ, i + 1]         := vClientes[i].cpf_cnpj;
    sgPesquisa.Cells[coLogradouro, i + 1]      := vClientes[i].logradouro;
    sgPesquisa.Cells[coComplemento, i + 1]     := vClientes[i].complemento;
    sgPesquisa.Cells[coBairro, i + 1]          := getInformacao(vClientes[i].bairro_id, vClientes[i].NomeBairro);
    sgPesquisa.Cells[coCidadeUF, i + 1]        := vClientes[i].NomeCidade + '/' + vClientes[i].estado_id;
    sgPesquisa.Cells[coEmail, i + 1]           := vClientes[i].e_mail;
    sgPesquisa.Cells[coECliente, i + 1]        := vClientes[i].e_cliente;

    sgPesquisa.Cells[coEFornecedor, i + 1]     := vClientes[i].e_fornecedor;
    sgPesquisa.Cells[coEMotorista, i + 1]      := vClientes[i].EMotorista;
    sgPesquisa.Cells[coETransportadora, i + 1] := vClientes[i].ETransportadora;
    sgPesquisa.Cells[coEProfissional, i + 1]   := vClientes[i].EProfissional;
    sgPesquisa.Cells[coAtivo, i + 1]           := vClientes[i].ativo;
  end;
  sgPesquisa.SetLinhasGridPorTamanhoVetor( Length(vClientes) );
  SetarFoco(sgPesquisa);
end;

procedure TFormPesquisaCadastros.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if _Biblioteca.Em(ACol, [coSelecionado, coECliente, coEFornecedor, coEMotorista, coETransportadora, coEProfissional, coAtivo]) then
    vAlinhamento := taCenter
  else if ACol = coCodigo then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
