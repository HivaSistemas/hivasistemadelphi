unit Informacoes.Orcamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  _Sessao, _Biblioteca, Vcl.ExtCtrls, Vcl.StdCtrls, EditLuka, Vcl.Mask, EditLukaData, _Orcamentos, _OrcamentosItens, _FrameHerancaPrincipal, FrameFechamento, Vcl.Grids,
  GridLuka, Vcl.ComCtrls, System.Math, _OrcamentosPagamentos, _OrcamentosPagamentosCheques, _RecordsOrcamentosVendas, Informacoes.NotaFiscal, _OrcamentosBloqueios,
  Informacoes.TurnoCaixa, Impressao.ComprovantePagamentoGrafico, _EntregasItensSemPrevisao, _RecordsExpedicao, _Retiradas, _RecordsNotasFiscais, _NotasFiscais, MixVendas,
  _OrcamentosTramites, _EntregasItensPendentes, Informacoes.Retiradas, _RetiradasItensPendentes, _Entregas, Informacoes.Entrega, Vcl.Menus, FrameEndereco, _Devolucoes,
  Informacoes.Devolucao, _RecordsEspeciais, BuscaDados, InformacoesAcumulado, SpeedButtonLuka, FrameArquivos, ImpressaoComprovanteEntregaGrafico, _RecordsFinanceiros,
  StaticTextLuka, _ContasReceber, _ContasPagar, Logs, PageControlAltis, PesquisaTiposCobranca, _RecordsCadastros, ImpressaoMeiaPaginaDuplicataMercantil,
  InformacoesProduto, BuscarDadosTransacaoCartao, CheckBoxLuka;

type
  TFormInformacoesOrcamento = class(TFormHerancaFinalizar)
    eOrcamentoId: TEditLuka;
    lb1: TLabel;
    eCliente: TEditLuka;
    lb2: TLabel;
    tsGerais: TTabSheet;
    tsItens: TTabSheet;
    eDataCadastro: TEditLukaData;
    lb3: TLabel;
    eDataRecebimento: TEditLukaData;
    lb4: TLabel;
    lb5: TLabel;
    eTotalOrcamento: TEditLuka;
    lb6: TLabel;
    eTotalProdutos: TEditLuka;
    lb7: TLabel;
    eOutrasDespesas: TEditLuka;
    lb8: TLabel;
    eValorDesconto: TEditLuka;
    lb9: TLabel;
    lb10: TLabel;
    lb11: TLabel;
    lb14: TLabel;
    eOverprice: TEditLuka;
    lb12: TLabel;
    eVendedor: TEditLuka;
    sgProdutos: TGridLuka;
    lb15: TLabel;
    eUsuarioCadastro: TEditLuka;
    lb16: TLabel;
    eUsuarioRecebimento: TEditLuka;
    lb17: TLabel;
    eTurnoId: TEditLuka;
    stSPC: TStaticText;
    lb18: TLabel;
    eCondicaoPagamento: TEditLuka;
    st3: TStaticText;
    stStatus: TStaticText;
    tsBloqueios: TTabSheet;
    sgItensBloqueios: TGridLuka;
    lb13: TLabel;
    eEmpresa: TEditLuka;
    tsEntregas: TTabSheet;
    tsNotasFiscais: TTabSheet;
    sgNotasFiscais: TGridLuka;
    tsTramites: TTabSheet;
    sgTramites: TGridLuka;
    StaticText1: TStaticText;
    stTipoEntrega: TStaticText;
    tsDevolucoes: TTabSheet;
    pmsgEntregas: TPopupMenu;
    miImprimirComprovanteEntrega: TMenuItem;
    FrEnderecoEntrega: TFrEndereco;
    sgDevolucoes: TGridLuka;
    tsObservacoes: TTabSheet;
    lb19: TLabel;
    lb20: TLabel;
    lb21: TLabel;
    eObservacoesCaixa: TMemo;
    eObservacoesExpedicao: TMemo;
    eObservacoesNotaFiscalEletronica: TMemo;
    sbAdicionarObservacaoCaixa: TSpeedButton;
    sbAdicionarObservacaoNFe: TSpeedButton;
    sbAdicionarObservacaoExpedicao: TSpeedButton;
    eValorFrete: TEditLuka;
    lb23: TLabel;
    pmsgRetiradas: TPopupMenu;
    miImprimirComprovanteEntregaRetirada: TMenuItem;
    sbAlterarEnderecoEntrega: TSpeedButton;
    st1: TStaticText;
    stOrigemVenda: TStaticText;
    FrFechamento: TFrFechamento;
    lb24: TLabel;
    eAcumuladoId: TEditLuka;
    sbInformacoesTurno: TSpeedButtonLuka;
    sbInformacoesAcumulado: TSpeedButtonLuka;
    sbMixVenda: TSpeedButtonLuka;
    lb25: TLabel;
    eIndiceDescontoVenda: TEditLuka;
    FrArquivos: TFrArquivos;
    lb26: TLabel;
    eTotalProdutosPromocao: TEditLuka;
    st2: TStaticText;
    stReceberNaEntrega: TStaticTextLuka;
    tsFinanceiro: TTabSheet;
    sgReceber: TGridLuka;
    sbLogs: TSpeedButtonLuka;
    pcDados: TPageControlAltis;
    tsCartoes: TTabSheet;
    sgCartoes: TGridLuka;
    pmSgCartoes: TPopupMenu;
    miAlterarTipoCobranca: TMenuItem;
    pmFinanceiro: TPopupMenu;
    miReemitirDuplicatasVenda: TMenuItem;
    pmProdutos: TPopupMenu;
    miInformacoesComposicaoKit: TMenuItem;
    pcEntregas: TPageControlAltis;
    tsRetirarAto: TTabSheet;
    tsEntregues: TTabSheet;
    tsRetirar: TTabSheet;
    tsEntregar: TTabSheet;
    sgRetirados: TGridLuka;
    sgEntregas: TGridLuka;
    sgARetirar: TGridLuka;
    sgProdutosPendentes: TGridLuka;
    tsEntregaSemPrevisao: TTabSheet;
    sgProdutosSemPrevisao: TGridLuka;
    miN1: TMenuItem;
    miAlterarDadosTransacao: TMenuItem;
    Label1: TLabel;
    StaticText2: TStaticText;
    ckAguardandoContatoCliente: TCheckBoxLuka;
    sbAtualizarContatoCliente: TSpeedButton;
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensBloqueiosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sbInformacoesTurnoClick(Sender: TObject);
    procedure sgRetiradosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgRetiradosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgARetirarDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgNotasFiscaisDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sbMixVendaClick(Sender: TObject);
    procedure sgNotasFiscaisDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sgTramitesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure tsTramitesShow(Sender: TObject);
    procedure sgProdutosSemPrevisaoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure tsEntregaSemPrevisaoShow(Sender: TObject);
    procedure tsEntregasShow(Sender: TObject);
    procedure sgProdutosSemPrevisaoGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgProdutosPendentesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure tsEntregarShow(Sender: TObject);
    procedure sgProdutosPendentesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure tsRetirarAtoShow(Sender: TObject);
    procedure tsRetirarShow(Sender: TObject);
    procedure sgRetiradosDblClick(Sender: TObject);
    procedure sgEntregasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure tsEntreguesShow(Sender: TObject);
    procedure sgEntregasDblClick(Sender: TObject);
    procedure miImprimirComprovanteEntregaClick(Sender: TObject);
    procedure tsDevolucoesShow(Sender: TObject);
    procedure sgDevolucoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgDevolucoesDblClick(Sender: TObject);
    procedure sbAdicionarObservacaoCaixaClick(Sender: TObject);
    procedure miImprimirComprovanteEntregaRetiradaClick(Sender: TObject);
    procedure sbAlterarEnderecoEntregaClick(Sender: TObject);
    procedure sbInformacoesAcumuladoClick(Sender: TObject);
    procedure sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure tsFinanceiroShow(Sender: TObject);
    procedure sgReceberDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgReceberDblClick(Sender: TObject);
    procedure sgReceberGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sbLogsClick(Sender: TObject);
    procedure sgCartoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure tsCartoesShow(Sender: TObject);
    procedure miAlterarTipoCobrancaClick(Sender: TObject);
    procedure miReemitirDuplicatasVendaClick(Sender: TObject);
    procedure miInformacoesComposicaoKitClick(Sender: TObject);
    procedure sgProdutosDblClick(Sender: TObject);
    procedure miAlterarDadosTransacaoClick(Sender: TObject);
    procedure sbAtualizarContatoClienteClick(Sender: TObject);
  private
    FOrcamentoId: Integer;
  end;

procedure Informar(pOrcamentoId: Integer);

implementation

{$R *.dfm}

uses
  Informacoes.TituloReceber, Informacoes.ProdutosComposicaoKit;

type
  coGridEntregas = record
  const
    EntregaId              = 0;
    Empresa                = 1;
    Local                  = 2;
    DataHoraCadastro       = 3;
    Status                 = 4;
    DataHoraEntrega        = 5;
  end;

  coGridProdutos = record
  const
    ProdutoId       = 0;
    Nome            = 1;
    Marca           = 2;
    TipoPrecoUtil   = 3;
    PrecoUnitario   = 4;
    Quantidade      = 5;
    Unidade         = 6;
    ValorDesconto   = 7;
    PercDescManual  = 8;
    ValorDescManual = 9;
    OutrasDespesas  = 10;
    ValorLiquido    = 11;
    ValorFrete      = 12;
  end;

  coGridFinanceiros = record
  const
    ProdutoId       = 0;
    Nome            = 1;
    Marca           = 2;
    TipoPrecoUtil   = 3;
    PrecoUnitario   = 4;
  end;

  coGridDevolucoes = record
  const
    DevolucaoId       = 0;
    DataHoraDevolucao = 1;
    Empresa           = 2;
    ValorTotal        = 3;
  end;

const
  (* Grid de transa��es de cart�o *)
  ccrCobranca          = 0;
  ccrValor             = 1;
  ccrCodigoAutorizacao = 2;
  ccrNSU               = 3;
  ccrNumeroCartao      = 4;
  ccrTipoRecebimento   = 5;
  ccrItemId            = 6;

  (* Grid de bloqueios *)
  cbItemId           = 0;
  cbDescricao        = 1;
  cbDataLiberacao    = 2;
  cbUsuarioLiberacao = 3;

  (* Grid de Retirados *)
  craRetiradaId             = 0;
  craEmpresa                = 1;
  craDataCadastro           = 2;
  craNomePessoaRetirou      = 3;
  craConfirmada             = 4;
  craUsuarioConfirmacao     = 5;
  craDataConfirmacao        = 6;

  (* Grid de produtos a retirar *)
  creEntregaPendEmpresa          = 0;
  creEntregaPendProdutoId        = 1;
  creEntregaPendNome             = 2;
  creEntregaPendMarca            = 3;
  creEntregaPendQuantidade       = 4;
  creEntregaPendUnd              = 5;
  creEntregaPendLocal            = 6;
  creEntregaPendLote             = 7;
  creEntregaPendEntregues        = 8;
  creEntregaPendDevolvidos       = 9;
  creEntregaPendSaldo            = 10;

  (* Grid de contas a receber *)
  crbReceberId      = 0;
  crbTipoCobranca   = 1;
  crbStatus         = 2;
  crbDocumento      = 3;
  crbDataVencimento = 4;
  crbValorDocumento = 5;
  crbParcela        = 6;
  crbNumeroParcelas = 7;

  (* Grid de itens pendentes de entrega *)
  coEntregaPendEmpresa          = 0;
  coEntregaPendProdutoId        = 1;
  coEntregaPendNome             = 2;
  coEntregaPendMarca            = 3;
  coEntregaPendQuantidade       = 4;
  coEntregaPendUnd              = 5;
  coEntregaPendLocal            = 6;
  coEntregaPendLote             = 7;
  coEntregaPendEntregues        = 8;
  coEntregaPendDevolvidos       = 8;
  coEntregaPendSaldo            = 10;
  coEntregaPendAgContatoCliente = 11;

  (* Grid de Entregas sem previs�o *)
  coSemPrevisaoProdutoId    = 0;
  coSemPrevisaoNome         = 1;
  coSemPrevisaoQuantidade   = 2;
  coSemPrevisaoEntregues    = 3;
  coSemPrevisaoDevolvidos   = 4;
  coSemPrevisaoSaldo        = 5;
  coSemPrevAgContatoCliente = 6;

  (* Grid de notas fiscais emitidas *)
  cnfNotaFiscalId  = 0;
  cnfNumeroNota    = 1;
  cnfTipoNota      = 2;
  cnfDataEmissao   = 3;
  cnfTipoMovimento = 4;
  cnfValorTotal    = 5;

  (* Grid de tr�mites *)
  ctrDataHoraTramite = 0;
  ctrTipoTramite     = 1;
  ctrValorTramite    = 2;
  ctrUsuarioTramite  = 3;
  ctrEstacaoTramite  = 4;
  ctrTipoTramiteSint = 5;

procedure Informar(pOrcamentoId: Integer);
var
  i: Integer;
  vForm: TFormInformacoesOrcamento;
  vOrcamento: TArray<RecOrcamentos>;
  vOrcamentoItens: TArray<RecOrcamentoItens>;
  vItensBloqueios: TArray<RecOrcamentosBloqueios>;

  vNotasFiscais: TArray<RecNotaFiscal>;
begin
  if pOrcamentoId = 0 then
    Exit;

  vOrcamento := _Orcamentos.BuscarOrcamentos(Sessao.getConexaoBanco, 0, [pOrcamentoID]);
  if vOrcamento = nil then begin
    Exclamar('Or�amento n�o encontrado, verifique!');
    Exit;
  end;

  vOrcamentoItens := _OrcamentosItens.BuscarOrcamentosItens(Sessao.getConexaoBanco, 0, [pOrcamentoId]);
  if vOrcamentoItens = nil then begin
    Exclamar('Itens do or�amento n�o encontrado, verifique!');
    Destruir(TArray<TObject>(vOrcamento));
    Exit;
  end;

  vForm := TFormInformacoesOrcamento.Create(Application);

  vForm.FOrcamentoId := pOrcamentoId;
  vForm.eOrcamentoId.AsInt := pOrcamentoId;
  vForm.eCondicaoPagamento.SetInformacao(vOrcamento[0].condicao_id, vOrcamento[0].nome_condicao_pagto);
  vForm.stTipoEntrega.Caption := vOrcamento[0].TipoEntregaAnalitico;
  vForm.stOrigemVenda.Caption := vOrcamento[0].OrigemVendaAnalitico;
  vForm.eCliente.SetInformacao(vOrcamento[0].cliente_id, vOrcamento[0].nome_cliente);
  vForm.eVendedor.SetInformacao(vOrcamento[0].vendedor_id, vOrcamento[0].nome_vendedor);
  vForm.eEmpresa.SetInformacao(vOrcamento[0].empresa_id, vOrcamento[0].nome_empresa);
  vForm.eIndiceDescontoVenda.SetInformacao(vOrcamento[0].IndiceDescontoVendaId, vOrcamento[0].NomeIndDescontoVenda);
  vForm.stReceberNaEntrega.Caption := vOrcamento[0].ReceberNaEntrega;

  vForm.stStatus.Caption := vOrcamento[0].status_analitico;

  if vOrcamento[0].status_analitico = 'Bloqueado' then
    vForm.stStatus.Font.Color := clRed;

  vForm.eDataCadastro.AsData := vOrcamento[0].data_cadastro;
  vForm.eUsuarioCadastro.Text := vOrcamento[0].nome_usuario_cadastro;

  vForm.eDataRecebimento.AsDataHora := vOrcamento[0].data_hora_recebimento;
  vForm.eUsuarioRecebimento.Text := vOrcamento[0].nome_usuario_recebimento;
  vForm.eTurnoId.SetInformacao(vOrcamento[0].turno_id);
  vForm.eAcumuladoId.SetInformacao(vOrcamento[0].AcumuladoId);

  vForm.eOverprice.AsDouble      := vOrcamento[0].indice_over_price;
  vForm.eTotalProdutos.AsDouble  := vOrcamento[0].valor_total_produtos;
  vForm.eTotalProdutosPromocao.AsDouble := vOrcamento[0].ValorTotalProdutosPromocao;
  vForm.eOutrasDespesas.AsDouble := vOrcamento[0].valor_outras_despesas;
  vForm.eValorDesconto.AsDouble  := vOrcamento[0].valor_desconto;
  vForm.eValorFrete.AsDouble     := vOrcamento[0].ValorFrete + vOrcamento[0].ValorFreteItens;

  vForm.eTotalOrcamento.AsDouble := vOrcamento[0].valor_total;

  vForm.eObservacoesCaixa.Text                := vOrcamento[0].ObservacoesCaixa;
  vForm.eObservacoesExpedicao.Text            := vOrcamento[0].ObservacoesExpedicao;
  vForm.eObservacoesNotaFiscalEletronica.Text := vOrcamento[0].ObservacoesNFe;

  vForm.FrEnderecoEntrega.SomenteLeitura(True);
  vForm.FrEnderecoEntrega.setLogradouro(vOrcamento[0].Logradouro);
  vForm.FrEnderecoEntrega.setComplemento(vOrcamento[0].Complemento);
  vForm.FrEnderecoEntrega.setNumero(vOrcamento[0].Numero);
  vForm.FrEnderecoEntrega.setBairroId(vOrcamento[0].BairroId);
  vForm.FrEnderecoEntrega.setPontoReferencia(vOrcamento[0].PontoReferencia);
  vForm.FrEnderecoEntrega.setCep(vOrcamento[0].Cep);
  vForm.ckAguardandoContatoCliente.CheckedStr := vOrcamento[0].AguardandoCliente;
  vForm.ckAguardandoContatoCliente.Visible := vOrcamento[0].TipoEntrega = 'EN';
  vForm.sbAtualizarContatoCliente.Visible  := vOrcamento[0].TipoEntrega = 'EN';
  vForm.FrFechamento.SomenteLeitura(True);
  vForm.FrFechamento.setCondicaoPagamento(vOrcamento[0].condicao_id, vOrcamento[0].nome_condicao_pagto, 0);
  vForm.FrFechamento.ValorFrete := vOrcamento[0].ValorFrete + vOrcamento[0].ValorFreteItens;
  vForm.FrFechamento.setTotalProdutos(vOrcamento[0].valor_total_produtos);
  vForm.FrFechamento.setTotalPromocional(vOrcamento[0].ValorTotalProdutosPromocao);
  vForm.FrFechamento.eValorOutrasDespesas.AsDouble := vOrcamento[0].valor_outras_despesas;
  vForm.FrFechamento.eValorOutrasDespesasChange(nil);

  vForm.FrFechamento.eValorDesconto.AsDouble := vOrcamento[0].valor_desconto;
  vForm.FrFechamento.eValorDescontoChange(nil);

  vForm.FrFechamento.CartoesDebito   := _OrcamentosPagamentos.BuscarOrcamentosPagamentos(Sessao.getConexaoBanco, 0, [pOrcamentoId, 'D']);
  vForm.FrFechamento.CartoesCredito  := _OrcamentosPagamentos.BuscarOrcamentosPagamentos(Sessao.getConexaoBanco, 0, [pOrcamentoId, 'C']);
  vForm.FrFechamento.Cobrancas := _OrcamentosPagamentos.BuscarOrcamentosPagamentos(Sessao.getConexaoBanco, 1, [pOrcamentoId]);
  vForm.FrFechamento.Cheques   := _OrcamentosPagamentosCheques.BuscarOrcamentosPagamentosCheques(Sessao.getConexaoBanco, 0, [pOrcamentoId]);

  vForm.FrFechamento.eValorDinheiro.AsDouble := vOrcamento[0].valor_dinheiro;
  vForm.FrFechamento.eValorCheque.AsDouble := vOrcamento[0].valor_cheque;
  vForm.FrFechamento.eValorCartaoDebito.AsDouble := vOrcamento[0].ValorCartaoDebito;
  vForm.FrFechamento.eValorCartaoCredito.AsDouble := vOrcamento[0].ValorCartaoCredito;
  vForm.FrFechamento.eValorCobranca.AsDouble := vOrcamento[0].valor_cobranca;
  vForm.FrFechamento.eValorAcumulativo.AsDouble := vOrcamento[0].ValorAcumulativo;
  vForm.FrFechamento.eValorCredito.AsDouble := vOrcamento[0].valor_credito;
  vForm.FrFechamento.eValorPix.AsDouble := vOrcamento[0].valor_pix;

  for i := Low(vOrcamentoItens) to High(vOrcamentoItens) do begin
    vForm.sgProdutos.Cells[coGridProdutos.ProdutoId, i + 1]      := NFormat(vOrcamentoItens[i].produto_id);
    vForm.sgProdutos.Cells[coGridProdutos.Nome, i + 1]           := vOrcamentoItens[i].nome;
    vForm.sgProdutos.Cells[coGridProdutos.Marca, i + 1]          := vOrcamentoItens[i].nome_marca;
    vForm.sgProdutos.Cells[coGridProdutos.TipoPrecoUtil, i + 1]  := vOrcamentoItens[i].TipoPrecoUtilizadoAnalitico;
    vForm.sgProdutos.Cells[coGridProdutos.PrecoUnitario, i + 1]  := NFormat(vOrcamentoItens[i].preco_unitario);
    vForm.sgProdutos.Cells[coGridProdutos.Quantidade, i + 1]     := NFormat(vOrcamentoItens[i].quantidade);
    vForm.sgProdutos.Cells[coGridProdutos.Unidade, i + 1]        := vOrcamentoItens[i].unidade_venda;
    vForm.sgProdutos.Cells[coGridProdutos.ValorDesconto, i + 1]  := NFormatN(vOrcamentoItens[i].valor_total_desconto);
    vForm.sgProdutos.Cells[coGridProdutos.PercDescManual, i + 1] := NFormatN(vOrcamentoItens[i].PercDescontoPrecoManual, 5);
    vForm.sgProdutos.Cells[coGridProdutos.ValorDescManual, i + 1] := NFormatN(vOrcamentoItens[i].ValorDescUnitPrecoManual);
    vForm.sgProdutos.Cells[coGridProdutos.OutrasDespesas, i + 1] := NFormatN(vOrcamentoItens[i].valor_total_outras_despesas);
    vForm.sgProdutos.Cells[coGridProdutos.ValorLiquido, i + 1]   := NFormat(vOrcamentoItens[i].valor_total + vOrcamentoItens[i].valor_total_outras_despesas - vOrcamentoItens[i].valor_total_desconto);
    vForm.sgProdutos.Cells[coGridProdutos.ValorFrete, i + 1]     := NFormatN(vOrcamentoItens[i].ValorTotalFrete);
  end;
  vForm.sgProdutos.SetLinhasGridPorTamanhoVetor( Length(vOrcamentoItens) );

  vItensBloqueios := _OrcamentosBloqueios.BuscarOrcamentosBloqueios(Sessao.getConexaoBanco, 0, [pOrcamentoId]);
  for i := Low(vItensBloqueios) to High(vItensBloqueios) do begin
    vForm.sgItensBloqueios.Cells[cbItemId, i + 1]           := NFormat(vItensBloqueios[i].ItemId);
    vForm.sgItensBloqueios.Cells[cbDescricao, i + 1]        := vItensBloqueios[i].Descricao;
    vForm.sgItensBloqueios.Cells[cbDataLiberacao, i + 1]    := DFormatN(vItensBloqueios[i].DataHoraLiberacao);

    if vItensBloqueios[i].UsuarioLiberacaoId > 0 then
      vForm.sgItensBloqueios.Cells[cbUsuarioLiberacao, i + 1] := NFormat(vItensBloqueios[i].UsuarioLiberacaoId) + ' - ' + vItensBloqueios[i].NomeUsuarioLiberacao;
  end;
  vForm.sgItensBloqueios.RowCount := IfThen(Length(vItensBloqueios) > 1, High(vItensBloqueios) + 2, 2);

  vNotasFiscais := _NotasFiscais.BuscarNotasFiscais(Sessao.getConexaoBanco, 1, [pOrcamentoId]);
  for i := Low(vNotasFiscais) to High(vNotasFiscais) do begin
    vForm.sgNotasFiscais.Cells[cnfNotaFiscalId, i + 1]  := NFormatN(vNotasFiscais[i].nota_fiscal_id);
    vForm.sgNotasFiscais.Cells[cnfNumeroNota, i + 1]    := NFormatN(vNotasFiscais[i].numero_nota);
    vForm.sgNotasFiscais.Cells[cnfTipoNota, i + 1]      := vNotasFiscais[i].TipoNotaAnalitico;
    vForm.sgNotasFiscais.Cells[cnfDataEmissao, i + 1]   := DFormatN(vNotasFiscais[i].data_hora_emissao);
    vForm.sgNotasFiscais.Cells[cnfTipoMovimento, i + 1] := vNotasFiscais[i].TipoMovimentoAnalitico;
    vForm.sgNotasFiscais.Cells[cnfValorTotal, i + 1]    := NFormat(vNotasFiscais[i].valor_total);
  end;
  vForm.sgNotasFiscais.SetLinhasGridPorTamanhoVetor( Length(vNotasFiscais) );

  vForm.FrArquivos.Origem := 'ORC';
  vForm.FrArquivos.Id     := pOrcamentoId;
  vForm.FrArquivos.buscarArquivos;

  vForm.ShowModal;

  Destruir(TArray<TObject>(vOrcamento));
  Destruir(TArray<TObject>(vOrcamentoItens));
  vForm.Free;
end;

{ TFormInformacoesOrcamento }

procedure TFormInformacoesOrcamento.FormShow(Sender: TObject);
begin
  inherited;
  pcDados.ActivePage := tsGerais;
end;

procedure TFormInformacoesOrcamento.miAlterarDadosTransacaoClick(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  vRetTela: TRetornoTelaFinalizar<RecDadosTransacao>;
begin
  inherited;
  if sgCartoes.Cells[ccrCobranca, sgCartoes.Row] = '' then
    Exit;

  vRetTela := BuscarDadosTransacaoCartao.Buscar(sgCartoes.Cells[ccrNumeroCartao, sgCartoes.Row], sgCartoes.Cells[ccrNSU, sgCartoes.Row], sgCartoes.Cells[ccrCodigoAutorizacao, sgCartoes.Row]);
  if vRetTela.BuscaCancelada then
    Exit;

  vRetBanco :=
    _OrcamentosPagamentos.AtualizarDadosCartoes(
      Sessao.getConexaoBanco,
      eOrcamentoId.AsInt,
      SFormatInt(sgCartoes.Cells[ccrItemId, sgCartoes.Row]),
      vRetTela.Dados.NumeroCartao,
      vRetTela.Dados.Nsu,
      vRetTela.Dados.CodigoAutorizacao
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.RotinaSucesso;
  tsCartoesShow(miAlterarDadosTransacao);
end;

procedure TFormInformacoesOrcamento.miAlterarTipoCobrancaClick(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  vTipoCobranca: RecTiposCobranca;
begin
  inherited;

  if
    not _Biblioteca.Perguntar(
      'Todos os financeiros em aberto para esta venda e com este tipo de cobran�a ser�o reprocessados no financeiro, deseja realmente continuar?'
    )
  then
    Exit;

  vTipoCobranca :=
    RecTiposCobranca(
      PesquisaTiposCobranca.PesquisarTiposCobranca(
        eCondicaoPagamento.IdInformacao,
        'CRT',
        'R'
      )
    );

  if vTipoCobranca = nil then
    Exit;

  vRetBanco :=
    _OrcamentosPagamentos.AtualizarTipoCobranca(
      Sessao.getConexaoBanco,
      eOrcamentoId.AsInt,
      vTipoCobranca.CobrancaId,
      SFormatInt(sgCartoes.Cells[ccrItemId, sgCartoes.Row])
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.RotinaSucesso;
  tsCartoesShow(miAlterarTipoCobranca);
end;

procedure TFormInformacoesOrcamento.miImprimirComprovanteEntregaClick(Sender: TObject);
begin
  inherited;
  ImpressaoComprovanteEntregaGrafico.Imprimir(SFormatInt(sgEntregas.Cells[coGridEntregas.EntregaId, sgEntregas.Row]), tpEntrega);
end;

procedure TFormInformacoesOrcamento.miImprimirComprovanteEntregaRetiradaClick(Sender: TObject);
begin
  inherited;
  ImpressaoComprovanteEntregaGrafico.Imprimir(SFormatInt(sgRetirados.Cells[craRetiradaId, sgRetirados.Row]), tpRetirada);
end;

procedure TFormInformacoesOrcamento.miInformacoesComposicaoKitClick(Sender: TObject);
begin
  inherited;
  Informacoes.ProdutosComposicaoKit.Informar( SFormatInt(sgProdutos.Cells[coGridProdutos.ProdutoId, sgProdutos.Row]), sgProdutos.Cells[coGridProdutos.Nome, sgProdutos.Row] );
end;

procedure TFormInformacoesOrcamento.miReemitirDuplicatasVendaClick(Sender: TObject);
begin
  inherited;
  ImpressaoMeiaPaginaDuplicataMercantil.Emitir( eOrcamentoId.AsInt, tpOrcamento );
end;

procedure TFormInformacoesOrcamento.sbAdicionarObservacaoCaixaClick(Sender: TObject);
var
  vTipo: string;
  vObservacao: string;
  vRetBanco: RecRetornoBD;

  vMemo: TMemo;
begin
  inherited;

  vObservacao := BuscaDados.BuscarDados('Observa��o', '', 800);
  if Trim(vObservacao) = '' then
    Exit;

  if Sender = sbAdicionarObservacaoCaixa then begin
    vTipo := 'C';
    vMemo := eObservacoesCaixa;
  end
  else if Sender = sbAdicionarObservacaoNFe then begin
    vTipo := 'N';
    vMemo := eObservacoesNotaFiscalEletronica;
  end
  else begin
    vTipo := 'E';
    vMemo := eObservacoesExpedicao;
  end;

  if vMemo.Lines.Text <> '' then
    vObservacao := vMemo.Lines.Text + chr(13) + chr(10) + vObservacao;

  vRetBanco := _Orcamentos.AtualizarObservacaoOrcamento(Sessao.getConexaoBanco, eOrcamentoId.AsInt, vObservacao, vTipo);
  Sessao.AbortarSeHouveErro( vRetBanco );

  vMemo.Lines.Text := vObservacao;
end;

procedure TFormInformacoesOrcamento.sbAlterarEnderecoEntregaClick(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if not Sessao.AutorizadoRotina('alterar_endereco_entrega') then begin
    _Biblioteca.NaoAutorizadoRotina;
    Exit;
  end;

  if not FrEnderecoEntrega.CarregarEnderecoCliente(eCliente.IdInformacao, True) then
    Exit;

  vRetBanco :=
    _Orcamentos.AtualizarEnderecoEntrega(
      Sessao.getConexaoBanco,
      eOrcamentoId.AsInt,
      FrEnderecoEntrega.getLogradouro,
      FrEnderecoEntrega.getComplemento,
      FrEnderecoEntrega.getNumero,
      FrEnderecoEntrega.getPontoReferencia,
      FrEnderecoEntrega.getBairroId,
      FrEnderecoEntrega.getCep,
      FrEnderecoEntrega.getCep
    );
  Sessao.AbortarSeHouveErro( vRetBanco );
end;

procedure TFormInformacoesOrcamento.sbAtualizarContatoClienteClick(
  Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vRetBanco :=
    _Orcamentos.AtualizarAguardandoContatoCliente(
       Sessao.getConexaoBanco,
       eOrcamentoId.AsInt,
       ckAguardandoContatoCliente.CheckedStr
    );

  Sessao.AbortarSeHouveErro(vRetBanco);
  _Biblioteca.RotinaSucesso;
end;

procedure TFormInformacoesOrcamento.sbInformacoesAcumuladoClick(Sender: TObject);
begin
  inherited;
  InformacoesAcumulado.Informar( eAcumuladoId.IdInformacao );
end;

procedure TFormInformacoesOrcamento.sbInformacoesTurnoClick(Sender: TObject);
begin
  inherited;
  Informacoes.TurnoCaixa.Informar(eTurnoId.IdInformacao);
end;

procedure TFormInformacoesOrcamento.sbLogsClick(Sender: TObject);
begin
  inherited;
  Logs.Iniciar('LOGS_ORCAMENTOS', ['ORCAMENTO_ID'], 'VW_TIPOS_ALTER_LOGS_ORCAMENTOS', [eOrcamentoId.AsInt]);
end;

procedure TFormInformacoesOrcamento.sbMixVendaClick(Sender: TObject);
begin
  inherited;
  MixVendas.Negociacao(eOrcamentoId.AsInt);
end;

procedure TFormInformacoesOrcamento.sgItensBloqueiosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = cbItemId then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItensBloqueios.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesOrcamento.sgProdutosPendentesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coEntregaPendProdutoId, coEntregaPendQuantidade, coEntregaPendEntregues, coEntregaPendDevolvidos, coEntregaPendSaldo] then
    vAlinhamento := taRightJustify
  else if ACol = coEntregaPendAgContatoCliente then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgProdutosPendentes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesOrcamento.sgProdutosPendentesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol <> coEntregaPendAgContatoCliente then
    Exit;

  _Biblioteca.SetarColunaGridSimNao(AFont, sgProdutosPendentes.Cells[ACol, ARow]);
end;

procedure TFormInformacoesOrcamento.sgNotasFiscaisDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.NotaFiscal.Informar(SFormatInt(sgNotasFiscais.Cells[cnfNotaFiscalId, sgNotasFiscais.Row]));
end;

procedure TFormInformacoesOrcamento.sgNotasFiscaisDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[cnfNotaFiscalId, cnfNumeroNota, cnfValorTotal] then
    vAlinhamento := taRightJustify
  else if ACol in[cnfTipoNota, cnfTipoMovimento] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgNotasFiscais.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesOrcamento.sgProdutosDblClick(Sender: TObject);
begin
  inherited;
  InformacoesProduto.Informar( SFormatInt(sgProdutos.Cells[coGridProdutos.ProdutoId, sgProdutos.Row]) );
end;

procedure TFormInformacoesOrcamento.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    coGridProdutos.ProdutoId,
    coGridProdutos.PrecoUnitario,
    coGridProdutos.Quantidade,
    coGridProdutos.ValorDesconto,
    coGridProdutos.OutrasDespesas,
    coGridProdutos.ValorLiquido,
    coGridProdutos.ValorFrete,
    coGridProdutos.PercDescManual,
    coGridProdutos.ValorDescManual
  ] then
    vAlinhamento := taRightJustify
  else if ACol = coGridProdutos.TipoPrecoUtil then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesOrcamento.sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coGridProdutos.TipoPrecoUtil then begin
    AFont.Style := [fsBold];
    AFont.Color := $000096DB;
  end;
end;

procedure TFormInformacoesOrcamento.sgProdutosSemPrevisaoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol = coSemPrevisaoNome then
    vAlinhamento := taLeftJustify
  else if ACol = coSemPrevAgContatoCliente then
    vAlinhamento := taCenter
  else
    vAlinhamento := taRightJustify;

  sgProdutosSemPrevisao.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesOrcamento.sgProdutosSemPrevisaoGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol <> coSemPrevAgContatoCliente then
    Exit;

  _Biblioteca.SetarColunaGridSimNao(AFont, sgProdutosSemPrevisao.Cells[ACol, ARow]);
end;

procedure TFormInformacoesOrcamento.sgReceberDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.TituloReceber.Informar( SFormatInt(sgReceber.Cells[crbReceberId, sgReceber.Row]) );
end;

procedure TFormInformacoesOrcamento.sgReceberDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[crbReceberId, crbValorDocumento, crbParcela, crbNumeroParcelas] then
    vAlinhamento := taRightJustify
  else if ACol = crbStatus then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgReceber.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesOrcamento.sgReceberGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol <> crbStatus then
    Exit;

  AFont.Style := [fsBold];
  if sgReceber.Cells[crbStatus, ARow] = 'Aberto' then
    AFont.Color := $000096DB
  else
    AFont.Color := clBlue;
end;

procedure TFormInformacoesOrcamento.sgRetiradosDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Retiradas.Informar( SFormatInt(sgRetirados.Cells[craRetiradaId, sgRetirados.Row]) );
end;

procedure TFormInformacoesOrcamento.sgRetiradosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = craRetiradaId then
    vAlinhamento := taRightJustify
  else if ACol = craConfirmada then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgRetirados.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesOrcamento.sgRetiradosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = craConfirmada then begin
    AFont.Style := [fsBold];
    AFont.Color := _Biblioteca.AzulVermelho(sgRetirados.Cells[ACol, ARow]);
  end;
end;

procedure TFormInformacoesOrcamento.sgARetirarDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[creEntregaPendProdutoId, creEntregaPendQuantidade, creEntregaPendEntregues, creEntregaPendDevolvidos, creEntregaPendSaldo] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgARetirar.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesOrcamento.sgCartoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[ccrValor] then
    vAlinhamento := taRightJustify
  else if ACol in[ccrTipoRecebimento] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgCartoes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesOrcamento.sgDevolucoesDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Devolucao.Informar( SFormatInt(sgDevolucoes.Cells[coGridDevolucoes.DevolucaoId, sgDevolucoes.Row]) );
end;

procedure TFormInformacoesOrcamento.sgDevolucoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coGridDevolucoes.DevolucaoId, coGridDevolucoes.ValorTotal] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgDevolucoes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesOrcamento.sgEntregasDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Entrega.Informar(SFormatInt(sgEntregas.Cells[coGridEntregas.EntregaId, sgEntregas.Row]));
end;

procedure TFormInformacoesOrcamento.sgEntregasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol = coGridEntregas.EntregaId then
    vAlinhamento := taRightJustify
  else if ACol = coGridEntregas.Status then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgEntregas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesOrcamento.sgTramitesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = ctrValorTramite then
    vAlinhamento := taRightJustify
  else if ACol in[ ctrDataHoraTramite, ctrTipoTramite ] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgTramites.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesOrcamento.tsCartoesShow(Sender: TObject);
var
  i: Integer;
  vPagtos: TArray<RecTitulosFinanceiros>;
begin
  inherited;
  if pcDados.CarregouAba and (Sender <> miAlterarTipoCobranca) and (Sender <> miAlterarDadosTransacao) then
    Exit;

  vPagtos := _OrcamentosPagamentos.BuscarOrcamentosPagamentos(Sessao.getConexaoBanco, 4, [eOrcamentoId.AsInt]);
  for i := Low(vPagtos) to High(vPagtos) do begin
    sgCartoes.Cells[ccrCobranca, i + 1]          := getInformacao( vPagtos[i].CobrancaId, vPagtos[i].NomeCobranca );
    sgCartoes.Cells[ccrValor, i + 1]             := NFormatN( vPagtos[i].Valor );
    sgCartoes.Cells[ccrCodigoAutorizacao, i + 1] := vPagtos[i].CodigoAutorizacao;
    sgCartoes.Cells[ccrNSU, i + 1]               := vPagtos[i].NsuTef;
    sgCartoes.Cells[ccrNumeroCartao, i + 1]      := vPagtos[i].NumeroCartao;
    sgCartoes.Cells[ccrTipoRecebimento, i + 1]   := vPagtos[i].TipoRecebCartao;
    sgCartoes.Cells[ccrItemId, i + 1]            := NFormat( vPagtos[i].ItemId );
  end;
  sgCartoes.SetLinhasGridPorTamanhoVetor( Length(vPagtos) );

  pcDados.CarregouAba := True;
end;

procedure TFormInformacoesOrcamento.tsDevolucoesShow(Sender: TObject);
var
  i: Integer;
  vDevolucoes: TArray<RecDevolucoes>;
begin
  inherited;
  if pcDados.CarregouAba then
    Exit;

  vDevolucoes := _Devolucoes.BuscarDevolucoes(Sessao.getConexaoBanco, 1, [eOrcamentoId.AsInt]);
  for i := Low(vDevolucoes) to High(vDevolucoes) do begin
    sgDevolucoes.Cells[coGridDevolucoes.DevolucaoId, i + 1]        := _Biblioteca.NFormat(vDevolucoes[i].devolucao_id);
    sgDevolucoes.Cells[coGridDevolucoes.DataHoraDevolucao, i + 1]  := _Biblioteca.DHFormat(vDevolucoes[i].data_hora_devolucao);
    sgDevolucoes.Cells[coGridDevolucoes.Empresa, i + 1]            := _Biblioteca.NFormat(vDevolucoes[i].EmpresaId) + ' - ' + vDevolucoes[i].NomeEmpresa;
    sgDevolucoes.Cells[coGridDevolucoes.ValorTotal, i + 1]         := _Biblioteca.NFormat(vDevolucoes[i].valor_liquido);
  end;
  sgDevolucoes.SetLinhasGridPorTamanhoVetor( Length(vDevolucoes) );

  pcDados.CarregouAba := True;
end;

procedure TFormInformacoesOrcamento.tsEntregarShow(Sender: TObject);
var
  i: Integer;
  vItens: TArray<RecEntregasItensPendentes>;
begin
  inherited;
  if pcEntregas.CarregouAba then
    Exit;

  vItens := _EntregasItensPendentes.BuscarEntregasItensPendentes(Sessao.getConexaoBanco, 0, [eOrcamentoId.AsInt]);
  for i := Low(vItens) to High(vItens) do begin
    sgProdutosPendentes.Cells[coEntregaPendEmpresa, i + 1]          := _Biblioteca.NFormat(vItens[i].EmpresaId) + ' - ' + vItens[i].NomeEmpresa;
    sgProdutosPendentes.Cells[coEntregaPendProdutoId, i + 1]        := _Biblioteca.NFormat(vItens[i].ProdutoId);
    sgProdutosPendentes.Cells[coEntregaPendNome, i + 1]             := vItens[i].Nome;
    sgProdutosPendentes.Cells[coEntregaPendMarca, i + 1]            := vItens[i].Marca;
    sgProdutosPendentes.Cells[coEntregaPendQuantidade, i + 1]       := _Biblioteca.NFormatNEstoque(vItens[i].Quantidade);
    sgProdutosPendentes.Cells[coEntregaPendLocal, i + 1]            := _Biblioteca.NFormatN(vItens[i].LocalId) + ' - ' + vItens[i].NomeLocal;
    sgProdutosPendentes.Cells[coEntregaPendUnd, i + 1]              := vItens[i].Unidade;
    sgProdutosPendentes.Cells[coEntregaPendLote, i + 1]             := vItens[i].Lote;
    sgProdutosPendentes.Cells[coEntregaPendEntregues, i + 1]        := _Biblioteca.NFormatNEstoque(vItens[i].Entregues);
    sgProdutosPendentes.Cells[coEntregaPendDevolvidos, i + 1]       := _Biblioteca.NFormatNEstoque(vItens[i].Devolvidos);
    sgProdutosPendentes.Cells[coEntregaPendSaldo, i + 1]            := _Biblioteca.NFormatNEstoque(vItens[i].Saldo);
    sgProdutosPendentes.Cells[coEntregaPendAgContatoCliente, i + 1] := _Biblioteca.SimNao(vItens[i].AguardarContatoCliente);
  end;
  sgProdutosPendentes.SetLinhasGridPorTamanhoVetor( Length(vItens) );

  pcEntregas.CarregouAba := True;
end;

procedure TFormInformacoesOrcamento.tsEntregaSemPrevisaoShow(Sender: TObject);
var
  i: Integer;
  vItens: TArray<RecEntregasItensSemPrevisao>;
begin
  inherited;
  if pcEntregas.CarregouAba then
    Exit;

  vItens := _EntregasItensSemPrevisao.BuscarEntregasItensSemPrevisao(Sessao.getConexaoBanco, 0, [eOrcamentoId.AsInt]);
  for i := Low(vItens) to High(vItens) do begin
    sgProdutosSemPrevisao.Cells[coSemPrevisaoProdutoId, i + 1]    := _Biblioteca.NFormat(vItens[i].ProdutoId);
    sgProdutosSemPrevisao.Cells[coSemPrevisaoNome, i + 1]         := vItens[i].Nome;
    sgProdutosSemPrevisao.Cells[coSemPrevisaoQuantidade, i + 1]   := _Biblioteca.NFormatNEstoque(vItens[i].Quantidade);
    sgProdutosSemPrevisao.Cells[coSemPrevisaoEntregues, i + 1]    := _Biblioteca.NFormatNEstoque(vItens[i].Entregues);
    sgProdutosSemPrevisao.Cells[coSemPrevisaoDevolvidos, i + 1]   := _Biblioteca.NFormatNEstoque(vItens[i].Devolvidos);
    sgProdutosSemPrevisao.Cells[coSemPrevisaoSaldo, i + 1]        := _Biblioteca.NFormatNEstoque(vItens[i].Saldo);
    sgProdutosSemPrevisao.Cells[coSemPrevAgContatoCliente, i + 1] := _Biblioteca.SimNao(vItens[i].AguardarContatoCliente);
  end;
  sgProdutosSemPrevisao.SetLinhasGridPorTamanhoVetor( Length(vItens) );

  pcEntregas.CarregouAba := True;
end;

procedure TFormInformacoesOrcamento.tsEntregasShow(Sender: TObject);
begin
  inherited;
  pcEntregas.ActivePageIndex := 0;
end;

procedure TFormInformacoesOrcamento.tsEntreguesShow(Sender: TObject);
var
  i: Integer;
  vEntregas: TArray<RecEntrega>;
begin
  if pcEntregas.CarregouAba then
    Exit;

  vEntregas := _Entregas.BuscarEntregas(Sessao.getConexaoBanco, 0, [eOrcamentoId.AsInt]);
  for i := Low(vEntregas) to High(vEntregas) do begin
    sgEntregas.Cells[coGridEntregas.EntregaId, i + 1]        := NFormat(vEntregas[i].EntregaId);
    sgEntregas.Cells[coGridEntregas.Empresa, i + 1]          := NFormat(vEntregas[i].EmpresaId) + ' - ' + vEntregas[i].NomeEmpresa;
    sgEntregas.Cells[coGridEntregas.Local, i + 1]            := NFormat(vEntregas[i].LocalId) + ' - ' + vEntregas[i].NomeLocal;
    sgEntregas.Cells[coGridEntregas.DataHoraCadastro, i + 1] := DHFormat(vEntregas[i].DataHoraCadastro);
    sgEntregas.Cells[coGridEntregas.Status, i + 1]           := vEntregas[i].StatusAnalitico;
    sgEntregas.Cells[coGridEntregas.DataHoraEntrega, i + 1]  := _Biblioteca.DHFormatN(vEntregas[i].DataHoraRealizouEntrega);
  end;
  sgEntregas.SetLinhasGridPorTamanhoVetor( Length(vEntregas) );

  pcEntregas.CarregouAba := True;
end;

procedure TFormInformacoesOrcamento.tsFinanceiroShow(Sender: TObject);
var
  i: Integer;
  vReceber: TArray<RecContasReceber>;
begin
  inherited;
  if pcDados.CarregouAba then
    Exit;

  sgReceber.ClearGrid();
  vReceber := _ContasReceber.BuscarContasReceber(Sessao.getConexaoBanco, 8, [FOrcamentoId]);
  for i := Low(vReceber) to High(vReceber) do begin
    sgReceber.Cells[crbReceberId, i + 1]      := NFormat(vReceber[i].ReceberId);
    sgReceber.Cells[crbTipoCobranca, i + 1]   := getInformacao(vReceber[i].cobranca_id, vReceber[i].nome_tipo_cobranca);
    sgReceber.Cells[crbStatus, i + 1]         := IIfStr(vReceber[i].status = 'A', 'Aberto', 'Baixado');
    sgReceber.Cells[crbDocumento, i + 1]      := vReceber[i].documento;
    sgReceber.Cells[crbDataVencimento, i + 1] := DFormat(vReceber[i].data_vencimento);
    sgReceber.Cells[crbValorDocumento, i + 1] := NFormat(vReceber[i].ValorDocumento);
    sgReceber.Cells[crbParcela, i + 1]        := NFormat(vReceber[i].Parcela);
    sgReceber.Cells[crbNumeroParcelas, i + 1] := NFormat(vReceber[i].NumeroParcelas);
  end;
  sgReceber.SetLinhasGridPorTamanhoVetor( Length(vReceber) );

  pcDados.CarregouAba := True;
end;

procedure TFormInformacoesOrcamento.tsRetirarAtoShow(Sender: TObject);
var
  i: Integer;
  vRetiradas: TArray<RecRetirada>;
begin
  if pcEntregas.CarregouAba then
    Exit;

  vRetiradas := _Retiradas.BuscarRetiradas(Sessao.getConexaoBanco, 1, [eOrcamentoId.AsInt]);
  for i := Low(vRetiradas) to High(vRetiradas) do begin
    sgRetirados.Cells[craRetiradaId, i + 1]         := NFormat(vRetiradas[i].RetiradaId);
    sgRetirados.Cells[craEmpresa, i + 1]            := NFormat(vRetiradas[i].EmpresaId) + ' - ' + vRetiradas[i].NomeEmpresa;
    sgRetirados.Cells[craDataCadastro, i + 1]       := DHFormat(vRetiradas[i].DataHoraCadastro);
    sgRetirados.Cells[craNomePessoaRetirou, i + 1]  := vRetiradas[i].NomePessoaRetirada;
    sgRetirados.Cells[craConfirmada, i + 1]         := SimNao(vRetiradas[i].Confirmada);
    sgRetirados.Cells[craUsuarioConfirmacao, i + 1] := vRetiradas[i].NomeUsuarioConfirmacao;
    sgRetirados.Cells[craDataConfirmacao, i + 1]    := DHFormatN(vRetiradas[i].DataHoraRetirada);
  end;
  sgRetirados.SetLinhasGridPorTamanhoVetor( Length(vRetiradas) );

  pcEntregas.CarregouAba := True;
end;

procedure TFormInformacoesOrcamento.tsRetirarShow(Sender: TObject);
var
  i: Integer;
  vItens: TArray<RecRetiradasItensPendentes>;
begin
  inherited;
  if pcEntregas.CarregouAba then
    Exit;

  vItens := _RetiradasItensPendentes.BuscarRetiradasItensPendentes(Sessao.getConexaoBanco, 0, [eOrcamentoId.AsInt]);
  for i := Low(vItens) to High(vItens) do begin
    sgARetirar.Cells[creEntregaPendEmpresa, i + 1]     := _Biblioteca.NFormat(vItens[i].EmpresaId) + ' - ' + vItens[i].NomeEmpresa;
    sgARetirar.Cells[creEntregaPendProdutoId, i + 1]   := _Biblioteca.NFormat(vItens[i].ProdutoId);
    sgARetirar.Cells[creEntregaPendNome, i + 1]        := vItens[i].Nome;
    sgARetirar.Cells[creEntregaPendMarca, i + 1]       := vItens[i].Marca;
    sgARetirar.Cells[creEntregaPendQuantidade, i + 1]  := _Biblioteca.NFormatNEstoque(vItens[i].Quantidade);
    sgARetirar.Cells[creEntregaPendUnd, i + 1]         := vItens[i].Unidade;
    sgARetirar.Cells[creEntregaPendLocal, i + 1]       := NFormat(vItens[i].LocalId) + ' - ' + vItens[i].NomeLocal;
    sgARetirar.Cells[creEntregaPendLote, i + 1]        := vItens[i].Lote;
    sgARetirar.Cells[creEntregaPendEntregues, i + 1]   := _Biblioteca.NFormatNEstoque(vItens[i].Entregues);
    sgARetirar.Cells[creEntregaPendDevolvidos, i + 1]  := _Biblioteca.NFormatNEstoque(vItens[i].Devolvidos);
    sgARetirar.Cells[creEntregaPendSaldo, i + 1]       := _Biblioteca.NFormatNEstoque(vItens[i].Saldo);
  end;
  sgARetirar.SetLinhasGridPorTamanhoVetor( Length(vItens) );

  pcEntregas.CarregouAba := True;
end;

procedure TFormInformacoesOrcamento.tsTramitesShow(Sender: TObject);
var
  i: Integer;
  vTramites: TArray<RecOrcamentoTramite>;
begin
  inherited;
  if pcDados.CarregouAba then
    Exit;

  sgTramites.ClearGrid();

  vTramites := _OrcamentosTramites.BuscarTramites(Sessao.getConexaoBanco, 0, [eOrcamentoId.AsInt]);
  if vTramites = nil then
    Exit;

  for i := Low(vTramites) to High(vTramites) do begin
    sgTramites.Cells[ctrDataHoraTramite, i + 1] := _Biblioteca.DHFormat(vTramites[i].dataHoraAlteracao);
    sgTramites.Cells[ctrTipoTramite, i + 1]     :=  vTramites[i].statusAnalitico;
    sgTramites.Cells[ctrValorTramite, i + 1]    := _Biblioteca.NFormat(vTramites[i].valorTotal);
    sgTramites.Cells[ctrUsuarioTramite, i + 1]  := vTramites[i].usuarioAlteracao;
    sgTramites.Cells[ctrEstacaoTramite, i + 1]  := vTramites[i].estacaoAlteracao;
    sgTramites.Cells[ctrTipoTramiteSint, i + 1] := vTramites[i].status;
  end;
  sgTramites.SetLinhasGridPorTamanhoVetor( Length(vTramites) );

  pcDados.CarregouAba := True;
end;

end.
