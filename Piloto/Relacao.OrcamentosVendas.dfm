﻿inherited FormRelacaoOrcamentosVendas: TFormRelacaoOrcamentosVendas
  Caption = 'Rela'#231#227'o de vendas e devolu'#231#245'es'
  ClientHeight = 566
  ClientWidth = 1015
  ExplicitWidth = 1021
  ExplicitHeight = 595
  PixelsPerInch = 96
  TextHeight = 14
  object SpeedButton1: TSpeedButton [0]
    Left = 19
    Top = 434
    Width = 158
    Height = 26
    Caption = 'Cancelar fecham. de pedido'
    Flat = True
    NumGlyphs = 2
    Visible = False
    OnClick = sbImprimirClick
  end
  object SpeedButton2: TSpeedButton [1]
    Left = 19
    Top = 469
    Width = 158
    Height = 26
    Caption = 'Cancelar fecham. de pedido'
    Flat = True
    NumGlyphs = 2
    Visible = False
    OnClick = sbImprimirClick
  end
  object SpeedButton3: TSpeedButton [2]
    Left = 19
    Top = 506
    Width = 158
    Height = 26
    Caption = 'Cancelar fecham. de pedido'
    Flat = True
    NumGlyphs = 2
    Visible = False
    OnClick = sbImprimirClick
  end
  inherited pnOpcoes: TPanel
    Height = 566
    ExplicitLeft = -2
    ExplicitHeight = 566
    inherited sbCarregar: TSpeedButton
      Top = 3
      Height = 42
      ExplicitTop = 3
      ExplicitHeight = 42
    end
    inherited sbImprimir: TSpeedButton
      Top = 67
      Height = 39
      ExplicitTop = 67
      ExplicitHeight = 39
    end
    object SpeedButton9: TSpeedButton [2]
      Left = 2
      Top = 483
      Width = 111
      Height = 33
      Caption = 'Rep. todos custos'
      Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      OnClick = SpeedButton9Click
    end
    inherited ckOmitirFiltros: TCheckBoxLuka
      Top = 160
      ExplicitTop = 160
    end
  end
  inherited pcDados: TPageControl
    Width = 893
    Height = 566
    ActivePage = tsResultado
    ExplicitWidth = 893
    ExplicitHeight = 566
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 885
      ExplicitHeight = 537
      object lb1: TLabel
        Left = 681
        Top = 1
        Width = 73
        Height = 14
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alCustom
        Caption = 'Agrupamento'
      end
      object lbTipoOrcamento: TLabel
        Left = 419
        Top = 489
        Width = 102
        Height = 14
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alCustom
        Caption = 'Tipo de or'#231'amento'
      end
      inline FrClientes: TFrClientes
        Left = 1
        Top = 85
        Width = 324
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 85
        ExplicitWidth = 324
        inherited sgPesquisa: TGridLuka
          Width = 299
          ExplicitWidth = 299
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 324
          ExplicitWidth = 324
          inherited lbNomePesquisa: TLabel
            Width = 45
            ExplicitWidth = 45
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 219
            ExplicitLeft = 219
          end
        end
        inherited pnPesquisa: TPanel
          Left = 299
          ExplicitLeft = 299
        end
      end
      inline FrEmpresas: TFrEmpresas
        Left = 1
        Top = 0
        Width = 322
        Height = 76
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 1
        ExplicitWidth = 322
        ExplicitHeight = 76
        inherited sgPesquisa: TGridLuka
          Width = 297
          Height = 59
          ExplicitWidth = 297
          ExplicitHeight = 59
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 322
          ExplicitWidth = 322
          inherited lbNomePesquisa: TLabel
            Width = 53
            ExplicitWidth = 53
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 217
            ExplicitLeft = 217
          end
        end
        inherited pnPesquisa: TPanel
          Left = 297
          Height = 60
          ExplicitLeft = 297
          ExplicitHeight = 60
        end
      end
      inline FrDataCadastro: TFrDataInicialFinal
        Left = 418
        Top = 212
        Width = 199
        Height = 41
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 418
        ExplicitTop = 212
        ExplicitWidth = 199
        inherited Label1: TLabel
          Width = 93
          Height = 14
          ExplicitWidth = 93
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      object gbStatus: TGroupBoxLuka
        Left = 418
        Top = 171
        Width = 220
        Height = 39
        Caption = 'Tipo de movimento'
        TabOrder = 4
        OpcaoMarcarDesmarcar = True
        object ckVenda: TCheckBox
          Left = 14
          Top = 16
          Width = 88
          Height = 17
          Caption = 'Venda'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object ckDevolução: TCheckBox
          Left = 124
          Top = 16
          Width = 100
          Height = 17
          Hint = 'Aguardando recebimento na entrega'
          Caption = 'Devolu'#231#227'o'
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
      end
      inline FrCondicoesPagamento: TFrCondicoesPagamento
        Left = 0
        Top = 170
        Width = 327
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        TabStop = True
        ExplicitTop = 170
        ExplicitWidth = 327
        inherited sgPesquisa: TGridLuka
          Width = 302
          ExplicitWidth = 302
        end
        inherited PnTitulos: TPanel
          Width = 327
          ExplicitWidth = 327
          inherited lbNomePesquisa: TLabel
            Width = 138
            ExplicitWidth = 138
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 222
            ExplicitLeft = 222
          end
        end
        inherited pnPesquisa: TPanel
          Left = 302
          ExplicitLeft = 302
        end
      end
      object gbFormasPagamento: TGroupBoxLuka
        Left = 418
        Top = 391
        Width = 225
        Height = 96
        Caption = '  Formas de pagamento    '
        TabOrder = 5
        OpcaoMarcarDesmarcar = True
        object ckCartao: TCheckBox
          Left = 14
          Top = 58
          Width = 88
          Height = 17
          Caption = 'Cart'#227'o'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object ckFinanceira: TCheckBox
          Left = 106
          Top = 17
          Width = 108
          Height = 17
          Caption = 'Financeira'
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
        object ckCheque: TCheckBox
          Left = 14
          Top = 37
          Width = 88
          Height = 17
          Caption = 'Cheque'
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
        object ckCobranca: TCheckBox
          Left = 106
          Top = 37
          Width = 88
          Height = 17
          Caption = 'Cobran'#231'a'
          Checked = True
          State = cbChecked
          TabOrder = 3
        end
        object ckAcumulado: TCheckBox
          Left = 106
          Top = 58
          Width = 88
          Height = 17
          Caption = 'Acumulado'
          Checked = True
          State = cbChecked
          TabOrder = 4
        end
        object ckDinheiro: TCheckBoxLuka
          Left = 14
          Top = 17
          Width = 85
          Height = 17
          Caption = 'Dinheiro'
          Checked = True
          State = cbChecked
          TabOrder = 5
          CheckedStr = 'S'
        end
        object ckPix: TCheckBox
          Left = 14
          Top = 77
          Width = 88
          Height = 17
          Caption = 'Pix'
          Checked = True
          State = cbChecked
          TabOrder = 6
        end
      end
      inline FrCodigoOrcamento: TFrameInteiros
        Left = 419
        Top = -4
        Width = 132
        Height = 84
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 6
        TabStop = True
        ExplicitLeft = 419
        ExplicitTop = -4
        inherited Panel1: TPanel
          Caption = 'C'#243'digo do pedido'
        end
      end
      inline FrCodigoTurno: TFrameInteiros
        Left = 734
        Top = 426
        Width = 132
        Height = 84
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 7
        TabStop = True
        Visible = False
        ExplicitLeft = 734
        ExplicitTop = 426
        inherited Panel1: TPanel
          Caption = 'C'#243'digo do turno'
        end
      end
      inline FrVendedores: TFrVendedores
        Left = 0
        Top = 255
        Width = 325
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 8
        TabStop = True
        ExplicitTop = 255
        ExplicitWidth = 325
        inherited sgPesquisa: TGridLuka
          Width = 300
          ExplicitWidth = 300
        end
        inherited PnTitulos: TPanel
          Width = 325
          ExplicitWidth = 325
          inherited lbNomePesquisa: TLabel
            Width = 65
            ExplicitWidth = 65
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 220
            ExplicitLeft = 220
          end
        end
        inherited pnPesquisa: TPanel
          Left = 300
          ExplicitLeft = 300
        end
      end
      inline FrPrevisaoEntrega: TFrDataInicialFinal
        Left = 419
        Top = 302
        Width = 199
        Height = 41
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 9
        TabStop = True
        ExplicitLeft = 419
        ExplicitTop = 302
        ExplicitWidth = 199
        inherited Label1: TLabel
          Width = 91
          Height = 14
          Caption = 'Data de entrega '
          ExplicitWidth = 91
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrProdutos: TFrProdutos
        Left = 0
        Top = 340
        Width = 325
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 10
        TabStop = True
        ExplicitTop = 340
        ExplicitWidth = 325
        inherited sgPesquisa: TGridLuka
          Width = 300
          ExplicitWidth = 300
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 325
          ExplicitWidth = 325
          inherited lbNomePesquisa: TLabel
            Width = 48
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 220
            ExplicitLeft = 220
          end
        end
        inherited pnPesquisa: TPanel
          Left = 300
          ExplicitLeft = 300
        end
        inherited ckFiltroExtra: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      inline FrValorTotal: TFrFaixaNumeros
        Left = 420
        Top = 348
        Width = 198
        Height = 41
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 11
        TabStop = True
        ExplicitLeft = 420
        ExplicitTop = 348
        ExplicitWidth = 198
        inherited lb1: TLabel
          Width = 60
          Height = 14
          Caption = 'Valor entre'
          ExplicitWidth = 60
          ExplicitHeight = 14
        end
        inherited lb2: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eValor1: TEditLuka
          Height = 22
          ExplicitHeight = 22
        end
        inherited eValor2: TEditLuka
          Left = 109
          Width = 82
          Height = 22
          ExplicitLeft = 109
          ExplicitWidth = 82
          ExplicitHeight = 22
        end
      end
      inline FrDataRecebimento: TFrDataInicialFinal
        Left = 419
        Top = 257
        Width = 199
        Height = 41
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 12
        TabStop = True
        ExplicitLeft = 419
        ExplicitTop = 257
        ExplicitWidth = 199
        inherited Label1: TLabel
          Width = 115
          Height = 14
          Caption = 'Data de recebimento'
          ExplicitWidth = 115
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      object cbAgrupamento: TComboBoxLuka
        Left = 681
        Top = 15
        Width = 141
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 13
        Text = 'Nenhum'
        Items.Strings = (
          'Nenhum'
          'Vendedor'
          'Cliente'
          'Data de recebimento')
        Valores.Strings = (
          'NEN'
          'VEN'
          'CLI'
          'DAT')
        AsInt = 0
        AsString = 'NEN'
      end
      inline FrCodigoDevolucao: TFrameInteiros
        Left = 419
        Top = 84
        Width = 132
        Height = 84
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 14
        TabStop = True
        ExplicitLeft = 419
        ExplicitTop = 84
        inherited Panel1: TPanel
          Caption = 'C'#243'digo da devolu'#231#227'o'
        end
      end
      inline FrProfissional: TFrProfissionais
        Left = 1
        Top = 438
        Width = 325
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 15
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 438
        ExplicitWidth = 325
        inherited sgPesquisa: TGridLuka
          Width = 300
          Align = alNone
          ExplicitWidth = 300
        end
        inherited CkAspas: TCheckBox
          Top = 20
          ExplicitTop = 20
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 325
          ExplicitWidth = 325
          inherited lbNomePesquisa: TLabel
            Width = 97
            Caption = 'Parceiro da venda'
            ExplicitWidth = 97
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 220
            ExplicitLeft = 220
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 300
          ExplicitLeft = 300
        end
        inherited poOpcoes: TPopupMenu
          Top = 32
        end
      end
      object cbTipoOrcamento: TComboBoxLuka
        Left = 419
        Top = 503
        Width = 141
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 16
        Text = 'Todos'
        Items.Strings = (
          'Todos'
          'Ambiente')
        Valores.Strings = (
          'TOD'
          'AMB')
        AsInt = 0
        AsString = 'TOD'
      end
      object ckSomenteProducao: TCheckBoxLuka
        Left = 630
        Top = 56
        Width = 243
        Height = 17
        Caption = 'Somente or'#231'amentos com produ'#231#227'o'
        TabOrder = 17
        CheckedStr = 'N'
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 885
      ExplicitHeight = 537
      object spSeparador: TSplitter
        Left = 0
        Top = 240
        Width = 885
        Height = 6
        Cursor = crVSplit
        Align = alBottom
        ResizeStyle = rsUpdate
        ExplicitLeft = -3
        ExplicitTop = 199
        ExplicitWidth = 884
      end
      object sgOrcamentos: TGridLuka
        Left = 0
        Top = 17
        Width = 885
        Height = 207
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        ColCount = 18
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
        PopupMenu = pmOpcoesOrcamento
        TabOrder = 0
        OnClick = sgOrcamentosClick
        OnDblClick = sgOrcamentosDblClick
        OnDrawCell = sgOrcamentosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Pedido'
          'Devolu'#231#227'o'
          'Movimento'
          'Cliente'
          'Data hr.movimento'
          'Vendedor'
          'Condi'#231#227'o de pagamento'
          'Valor produtos'
          'Valor total'
          'Valor out. desp.'
          'Valor frete'
          'Valor desconto'
          'Valor luc.bruto'
          '% Lucro bruto'
          'Valor luc.l'#237'quido'
          '% Lucro l'#237'quido'
          'Empresa'
          'Parceiro')
        OnGetCellColor = sgOrcamentosGetCellColor
        Grid3D = False
        RealColCount = 18
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          69
          70
          69
          253
          121
          187
          230
          108
          90
          104
          90
          98
          95
          91
          100
          89
          280
          387)
      end
      object sgProdutos: TGridLuka
        Left = 0
        Top = 263
        Width = 885
        Height = 171
        Align = alBottom
        ColCount = 13
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        TabOrder = 1
        OnDrawCell = sgProdutosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Produto'
          'Nome'
          'Marca'
          'Pre'#231'o unit.'
          'Quantidade'
          'Und.'
          'Valor total'
          'Valor out.desp.'
          'Valor desc.'
          'Valor luc.bruto'
          '% Lucro bruto'
          'Valor luc.l'#237'quido'
          '% Lucro l'#237'quido')
        OnGetCellColor = sgProdutosGetCellColor
        Grid3D = False
        RealColCount = 13
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          54
          269
          154
          69
          74
          33
          75
          89
          70
          87
          78
          98
          88)
      end
      object StaticTextLuka1: TStaticTextLuka
        Left = 0
        Top = 0
        Width = 885
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Pedidos de vendas'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object StaticTextLuka2: TStaticTextLuka
        Left = 0
        Top = 246
        Width = 885
        Height = 17
        Align = alBottom
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Itens dos pedidos'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object Panel1: TPanel
        Left = 0
        Top = 434
        Width = 885
        Height = 103
        Align = alBottom
        BevelKind = bkTile
        BevelOuter = bvNone
        Color = clWhite
        ParentBackground = False
        TabOrder = 4
        object miCancelarFechamentoPedido: TSpeedButton
          Left = 9
          Top = 1
          Width = 158
          Height = 26
          BiDiMode = bdRightToLeft
          Caption = 'Cancelar fecham. de pedido'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = miCancelarFechamentoPedidoClick
        end
        object miAlterarIndiceDescontoVenda: TSpeedButton
          Left = 699
          Top = 3
          Width = 158
          Height = 26
          Caption = 'Alterar '#237'ndice de dedu'#231#227'o'
          Flat = True
          NumGlyphs = 2
          OnClick = miAlterarIndiceDescontoVendaClick
        end
        object miGerarPDFdooramentovendaFocada: TSpeedButton
          Left = 9
          Top = 25
          Width = 158
          Height = 26
          BiDiMode = bdRightToLeft
          Caption = 'Gerar PDF do pedido'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = miGerarPDFdooramentovendaFocadaClick
        end
        object miReimprimirComprovantePagamento: TSpeedButton
          Left = 235
          Top = 1
          Width = 158
          Height = 26
          BiDiMode = bdRightToLeft
          Caption = 'Reimp. comp. de pagamento'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = miReimprimirComprovantePagamentoClick
        end
        object miReimprimirConfissaoDivida: TSpeedButton
          Left = 235
          Top = 51
          Width = 158
          Height = 26
          Caption = 'Reimp. confiss'#227'o de d'#237'vida'
          Flat = True
          NumGlyphs = 2
          OnClick = miReimprimirConfissaoDividaClick
        end
        object miReimprimirDuplicataMercantil: TSpeedButton
          Left = 230
          Top = 27
          Width = 158
          Height = 26
          BiDiMode = bdRightToLeft
          Caption = 'Reimprimir duplicata '
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = miReimprimirDuplicataMercantilClick
        end
        object miReimprimirOrcamento: TSpeedButton
          Left = 9
          Top = 51
          Width = 158
          Height = 26
          BiDiMode = bdRightToLeft
          Caption = 'Reimprimir pedido'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          Visible = False
          OnClick = miReimprimirOrcamentoClick
        end
        object SpeedButton4: TSpeedButton
          Left = 467
          Top = 1
          Width = 214
          Height = 26
          BiDiMode = bdRightToLeft
          Caption = 'Cancelar devolu'#231#227'o'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = SpeedButton4Click
        end
        object SpeedButton5: TSpeedButton
          Left = 467
          Top = 27
          Width = 214
          Height = 26
          BiDiMode = bdRightToLeft
          Caption = 'Reimprimir comprovante de devolu'#231#227'o'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = SpeedButton5Click
        end
        object SpeedButton6: TSpeedButton
          Left = 467
          Top = 51
          Width = 222
          Height = 26
          Caption = 'Reimp. comprovante  faturamento venda'
          Flat = True
          NumGlyphs = 2
          OnClick = SpeedButton6Click
        end
        object SpeedButton7: TSpeedButton
          Left = 695
          Top = 27
          Width = 158
          Height = 26
          Caption = 'Reprocessar custo'
          Flat = True
          NumGlyphs = 2
          OnClick = SpeedButton7Click
        end
        object SpeedButton8: TSpeedButton
          Left = 705
          Top = 51
          Width = 158
          Height = 26
          Caption = 'Alterar profissional'
          Flat = True
          NumGlyphs = 2
          OnClick = SpeedButton8Click
        end
      end
      object st2: TStaticText
        Left = 260
        Top = 223
        Width = 157
        Height = 16
        Alignment = taCenter
        Anchors = [akRight, akBottom]
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Qtde. vendas'
        Color = 15395562
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        Transparent = False
      end
      object stQtdeVendas: TStaticTextLuka
        Left = 416
        Top = 223
        Width = 157
        Height = 16
        Alignment = taCenter
        Anchors = [akRight, akBottom]
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clTeal
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 6
        Transparent = False
        AsInt = 0
        TipoCampo = tcNumerico
        CasasDecimais = 0
      end
      object stQtdeDevolucoesLabel: TStaticText
        Left = 572
        Top = 223
        Width = 157
        Height = 16
        Alignment = taCenter
        Anchors = [akRight, akBottom]
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Qtde. devolu'#231#245'es'
        Color = 15395562
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 7
        Transparent = False
      end
      object stQtdeDevolucoes: TStaticTextLuka
        Left = 728
        Top = 223
        Width = 157
        Height = 16
        Alignment = taCenter
        Anchors = [akRight, akBottom]
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clTeal
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 8
        Transparent = False
        AsInt = 0
        TipoCampo = tcNumerico
        CasasDecimais = 0
      end
    end
  end
  object pmOpcoesOrcamento: TPopupMenu
    Left = 896
    Top = 376
    object miN2: TMenuItem
      Caption = '-'
    end
    object miN1: TMenuItem
      Caption = '-'
    end
  end
  object frxReport: TfrxReport
    Version = '5.2.3'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44043.704685613400000000
    ReportOptions.LastChange = 44399.978094201400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 832
    Top = 320
    Datasets = <
      item
        DataSet = dmRelatorio.dstEmpresa
        DataSetName = 'frxdstEmpresa'
      end
      item
        DataSet = dstVendas
        DataSetName = 'frxdstVendas'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 86.929190000000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Shape1: TfrxShapeView
          Top = 70.811070000000000000
          Width = 1046.929810000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
        object Memo1: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000000000
          Width = 68.031540000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 26.456710000000000000
          Top = 34.015770000000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 15.118120000000000000
          Top = 49.133890000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 664.197280000002000000
          Top = 3.779530000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 645.299630000002000000
          Top = 34.015770000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade/UF:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 664.197280000002000000
          Top = 49.133890000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 82.708720000000000000
          Top = 3.779530000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 707.551640000002000000
          Top = 3.779530000000000000
          Width = 200.315090000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."CNPJ"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 84.929190000000000000
          Top = 18.897650000000000000
          Width = 551.811380000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            
              '[frxdstEmpresa."LOGRADOURO"] [frxdstEmpresa."COMPLEMENTO"] n'#176'[fr' +
              'xdstEmpresa."NUMERO"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 82.708720000000000000
          Top = 34.015770000000000000
          Width = 207.874150000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_BAIRRO"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 707.551640000002000000
          Top = 34.015770000000000000
          Width = 185.196970000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_CIDADE"] / [frxdstEmpresa."ESTADO_ID"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 82.708720000000000000
          Top = 49.133890000000000000
          Width = 75.590600000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 707.551640000002000000
          Top = 49.133890000000000000
          Width = 260.787570000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."E_MAIL"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 869.291900000002000000
          Top = 3.779530000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Emitido em [Date] '#224's [Time]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Top = 70.811070000000000000
          Width = 37.795300000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Pedido')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 119.055118110236000000
          Top = 70.811070000000000000
          Width = 86.929190000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 935.543910000000000000
          Top = 70.811070000000000000
          Width = 109.606370000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Empresa')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 303.700990000000000000
          Top = 70.811070000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Vendedor')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 422.086890000000000000
          Top = 70.811070000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Cond. Pgto')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 574.488560000000000000
          Top = 70.811070000000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Valor Total')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 869.291900000002000000
          Top = 18.897650000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pag. [Page] de [TotalPages#]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo32: TfrxMemoView
          Left = 499.677490000000000000
          Top = 70.811070000000000000
          Width = 79.370130000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Valor Produto')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 246.669450000000000000
          Top = 70.811070000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Dta Pgto')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 629.401980000000000000
          Top = 70.811070000000000000
          Width = 41.574830000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Desp.')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 672.756340000000000000
          Top = 70.811070000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Desconto')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 720.669760000000000000
          Top = 70.811070000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Luc. Bruto')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 774.024120000000000000
          Top = 70.811070000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '% Luc. Bruto')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 838.276130000000000000
          Top = 70.811070000000000000
          Width = 41.574830000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Luc. Liq')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 882.630490000000000000
          Top = 70.811070000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '%Luc. Liq')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 40.574830000000000000
          Top = 70.811070000000000000
          Width = 34.015770000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Devol.')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 78.370078740000000000
          Top = 70.811070000000000000
          Width = 37.795300000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Movim.')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 166.299320000000000000
        Width = 1046.929810000000000000
        DataSet = dstVendas
        DataSetName = 'frxdstVendas'
        RowCount = 0
        object frxdstTranscodigoProd: TfrxMemoView
          Top = 1.000000000000000000
          Width = 37.795275590551200000
          Height = 11.338590000000000000
          DataSet = dstVendas
          DataSetName = 'frxdstVendas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendas."pedido"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 118.944960000000000000
          Top = 1.000000000000000000
          Width = 124.724490000000000000
          Height = 11.338590000000000000
          DataSet = dstVendas
          DataSetName = 'frxdstVendas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendas."cliente"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 303.700990000000000000
          Top = 1.000000000000000000
          Width = 117.165430000000000000
          Height = 11.338590000000000000
          DataSet = dstVendas
          DataSetName = 'frxdstVendas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendas."vendedor"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 246.669450000000000000
          Top = 1.000000000000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          DataSet = dstVendas
          DataSetName = 'frxdstVendas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendas."data_recebimento"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 518.575140000000000000
          Top = 1.000000000000000000
          Width = 60.472480000000000000
          Height = 11.338590000000000000
          DataSet = dstVendas
          DataSetName = 'frxdstVendas'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendas."valor_produto"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 935.543910000000000000
          Top = 1.000000000000000000
          Width = 109.606370000000000000
          Height = 11.338590000000000000
          DataSet = dstVendas
          DataSetName = 'frxdstVendas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendas."Empresa"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 422.086890000000000000
          Top = 1.000000000000000000
          Width = 94.488250000000000000
          Height = 11.338590000000000000
          DataSet = dstVendas
          DataSetName = 'frxdstVendas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendas."cond_pgto"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 582.047620000000000000
          Top = 1.000000000000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          DataSet = dstVendas
          DataSetName = 'frxdstVendas'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendas."valor_total"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 636.961040000000000000
          Top = 1.000000000000000000
          Width = 34.015770000000000000
          Height = 11.338590000000000000
          DataSet = dstVendas
          DataSetName = 'frxdstVendas'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendas."outras_despesas"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 688.874460000000000000
          Top = 1.000000000000000000
          Width = 30.236240000000000000
          Height = 11.338590000000000000
          DataSet = dstVendas
          DataSetName = 'frxdstVendas'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendas."desconto"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 716.890230000000000000
          Top = 1.000000000000000000
          Width = 56.692950000000000000
          Height = 11.338590000000000000
          DataSet = dstVendas
          DataSetName = 'frxdstVendas'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendas."valor_bruto"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 842.055660000000000000
          Top = 1.000000000000000000
          Width = 37.795300000000000000
          Height = 11.338590000000000000
          DataSet = dstVendas
          DataSetName = 'frxdstVendas'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendas."valor_liquido"]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 777.803650000000000000
          Top = 1.000000000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          DataSet = dstVendas
          DataSetName = 'frxdstVendas'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendas."perc_lucro_bruto"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 882.630490000000000000
          Top = 1.000000000000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          DataSet = dstVendas
          DataSetName = 'frxdstVendas'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendas."perc_lucro_liquido"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 40.440944881889800000
          Top = 1.000000000000000000
          Width = 34.015770000000000000
          Height = 11.338590000000000000
          DataField = 'devolucao'
          DataSet = dstVendas
          DataSetName = 'frxdstVendas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendas."devolucao"]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 78.236220470000000000
          Top = 1.000000000000000000
          Width = 37.795275590000000000
          Height = 11.338590000000000000
          DataField = 'movimento'
          DataSet = dstVendas
          DataSetName = 'frxdstVendas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendas."movimento"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 434.645950000000000000
        Width = 1046.929810000000000000
        object Shape2: TfrxShapeView
          Width = 1046.929133860000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 170.456802760000000000
        Top = 241.889920000000000000
        Width = 1046.929810000000000000
        object Shape3: TfrxShapeView
          Width = 1046.929810000000000000
          Height = 166.299320000000000000
          Fill.BackColor = cl3DLight
        end
        object Memo44: TfrxMemoView
          Left = 283.685220000000000000
          Top = 3.779530000000000000
          Width = 211.653680000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Totais....................:')
          ParentFont = False
        end
        object mmPercLucroBruto: TfrxMemoView
          Left = 336.378170000000000000
          Top = 110.385900000000000000
          Width = 498.897960000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '% Lucro Bruto.....: ')
          ParentFont = False
        end
        object mmPercLucroLiq: TfrxMemoView
          Left = 336.378170000000000000
          Top = 141.842610000000000000
          Width = 498.897960000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '% Lucro Liquido...: ')
          ParentFont = False
        end
        object mmValorLiquido: TfrxMemoView
          Left = 336.378170000000000000
          Top = 126.724490000000000000
          Width = 498.897960000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Valor Liquido.....: ')
          ParentFont = False
        end
        object mmValorBruto: TfrxMemoView
          Left = 336.378170000000000000
          Top = 91.708720000000000000
          Width = 498.897960000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Valor Bruto.......: ')
          ParentFont = False
        end
        object mmDescontos: TfrxMemoView
          Left = 336.378170000000000000
          Top = 74.590600000000000000
          Width = 498.897960000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Descontos.........: ')
          ParentFont = False
        end
        object mmOutrasDespesas: TfrxMemoView
          Left = 336.378170000000000000
          Top = 56.692950000000000000
          Width = 498.897960000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Outras Despesas...: ')
          ParentFont = False
        end
        object mmValorTotal: TfrxMemoView
          Left = 336.378170000000000000
          Top = 39.795300000000000000
          Width = 498.897960000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Valor Total.......: ')
          ParentFont = False
        end
        object mmValorProduto: TfrxMemoView
          Left = 336.378170000000000000
          Top = 18.897650000000000000
          Width = 498.897960000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Valor Produto.....: ')
          ParentFont = False
        end
      end
    end
  end
  object dstVendas: TfrxDBDataset
    UserName = 'frxdstVendas'
    CloseDataSource = False
    FieldAliases.Strings = (
      'pedido=pedido'
      'data_recebimento=data_recebimento'
      'cliente=cliente'
      'vendedor=vendedor'
      'cond_pgto=cond_pgto'
      'valor_produto=valor_produto'
      'valor_total=valor_total'
      'outras_despesas=outras_despesas'
      'desconto=desconto'
      'valor_bruto=valor_bruto'
      'valor_liquido=valor_liquido'
      'perc_lucro_bruto=perc_lucro_bruto'
      'perc_lucro_liquido=perc_lucro_liquido'
      'empresa=Empresa'
      'devolucao=devolucao'
      'movimento=movimento')
    DataSet = cdsVendas
    BCDToCurrency = False
    Left = 856
    Top = 352
  end
  object cdsVendas: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'pedido'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'data_recebimento'
        DataType = ftDate
      end
      item
        Name = 'cliente'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'vendedor'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'cond_pgto'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'valor_produto'
        DataType = ftFloat
      end
      item
        Name = 'valor_total'
        DataType = ftFloat
      end
      item
        Name = 'outras_despesas'
        DataType = ftFloat
      end
      item
        Name = 'desconto'
        DataType = ftFloat
      end
      item
        Name = 'valor_bruto'
        DataType = ftFloat
      end
      item
        Name = 'valor_liquido'
        DataType = ftFloat
      end
      item
        Name = 'perc_lucro_bruto'
        DataType = ftFloat
      end
      item
        Name = 'perc_lucro_liquido'
        DataType = ftFloat
      end
      item
        Name = 'empresa'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'devolucao'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'movimento'
        DataType = ftString
        Size = 20
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 208
    Top = 96
    object cdsVendaspedido: TStringField
      FieldName = 'pedido'
    end
    object cdsVendasdata_recebimento: TDateField
      FieldName = 'data_recebimento'
    end
    object cdsVendascliente: TStringField
      FieldName = 'cliente'
      Size = 60
    end
    object cdsVendasvendedor: TStringField
      FieldName = 'vendedor'
      Size = 60
    end
    object cdsVendascond_pgto: TStringField
      FieldName = 'cond_pgto'
      Size = 60
    end
    object cdsVendasvalor_produto: TFloatField
      FieldName = 'valor_produto'
    end
    object cdsVendasvalor_total: TFloatField
      FieldName = 'valor_total'
    end
    object cdsVendasoutras_despesas: TFloatField
      FieldName = 'outras_despesas'
    end
    object cdsVendasdesconto: TFloatField
      FieldName = 'desconto'
    end
    object cdsVendasvalor_bruto: TFloatField
      FieldName = 'valor_bruto'
    end
    object cdsVendasvalor_liquido: TFloatField
      FieldName = 'valor_liquido'
    end
    object cdsVendasperc_lucro_bruto: TFloatField
      FieldName = 'perc_lucro_bruto'
    end
    object cdsVendasperc_lucro_liquido: TFloatField
      FieldName = 'perc_lucro_liquido'
    end
    object cdsVendasempresa: TStringField
      FieldName = 'empresa'
      Size = 60
    end
    object cdsVendasdevolucao: TStringField
      FieldName = 'devolucao'
    end
    object cdsVendasmovimento: TStringField
      FieldName = 'movimento'
    end
  end
  object dstProdutosVendasDevolucoes: TfrxDBDataset
    UserName = 'frxdstVendasDevolucoesProdutos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'POSICAO=POSICAO'
      'PRODUTO_ID=PRODUTO_ID'
      'NOME=NOME'
      'MARCA=MARCA'
      'PRECO_UNITARIO=PRECO_UNITARIO'
      'QUANTIDADE=QUANTIDADE'
      'UNIDADE=UNIDADE'
      'VALOR_TOTAL=valor_total'
      'VALOR_LUCRO_BRUTO=VALOR_LUCRO_BRUTO'
      'VALOR_LUCRO_LIQUIDO=VALOR_LUCRO_LIQUIDO'
      'PERC_LUCRO_BRUTO=perc_lucro_bruto'
      'PERC_LUCRO_LIQUIDO=perc_lucro_liquido'
      'OUTRAS_DESPESAS=OUTRAS_DESPESAS'
      'DESCONTO=DESCONTO')
    DataSet = qVendasDevolucoesProdutos
    BCDToCurrency = False
    Left = 928
    Top = 153
  end
  object dstVendasDevolucoes: TfrxDBDataset
    UserName = 'frxdstVendasDevolucoes'
    CloseDataSource = False
    FieldAliases.Strings = (
      'PEDIDO=pedido'
      'DATA_RECEBIMENTO=data_recebimento'
      'CLIENTE=cliente'
      'VENDEDOR=vendedor'
      'COND_PGTO=cond_pgto'
      'VALOR_PRODUTO=valor_produto'
      'VALOR_TOTAL=valor_total'
      'OUTRAS_DESPESAS=outras_despesas'
      'DESCONTO=desconto'
      'VALOR_BRUTO=valor_bruto'
      'VALOR_LIQUIDO=valor_liquido'
      'PERC_LUCRO_BRUTO=perc_lucro_bruto'
      'PERC_LUCRO_LIQUIDO=perc_lucro_liquido'
      'EMPRESA=empresa'
      'DEVOLUCAO=devolucao'
      'POSICAO=posicao'
      'MOVIMENTO=MOVIMENTO')
    DataSet = qVendasDevolucoes
    BCDToCurrency = False
    Left = 792
    Top = 145
  end
  object qVendasDevolucoes: TOraQuery
    SQL.Strings = (
      'select '
      'PEDIDO,'
      'MOVIMENTO,'
      'DATA_RECEBIMENTO,'
      'CLIENTE,'
      'VENDEDOR,'
      'COND_PGTO,'
      'VALOR_PRODUTO,'
      'VALOR_TOTAL,'
      'OUTRAS_DESPESAS,'
      'DESCONTO,'
      'VALOR_BRUTO,'
      'VALOR_LIQUIDO,'
      'PERC_LUCRO_BRUTO,'
      'PERC_LUCRO_LIQUIDO,'
      'EMPRESA,'
      'DEVOLUCAO,'
      'POSICAO'
      'from TEMP_VEND_DEVOL')
    Left = 792
    Top = 203
    object qVendasDevolucoesPEDIDO: TStringField
      FieldName = 'PEDIDO'
    end
    object qVendasDevolucoesDATA_RECEBIMENTO: TStringField
      FieldName = 'DATA_RECEBIMENTO'
    end
    object qVendasDevolucoesCLIENTE: TStringField
      FieldName = 'CLIENTE'
      Size = 200
    end
    object qVendasDevolucoesVENDEDOR: TStringField
      FieldName = 'VENDEDOR'
      Size = 200
    end
    object qVendasDevolucoesCOND_PGTO: TStringField
      FieldName = 'COND_PGTO'
      Size = 200
    end
    object qVendasDevolucoesVALOR_PRODUTO: TFloatField
      FieldName = 'VALOR_PRODUTO'
    end
    object qVendasDevolucoesVALOR_TOTAL: TFloatField
      FieldName = 'VALOR_TOTAL'
    end
    object qVendasDevolucoesOUTRAS_DESPESAS: TFloatField
      FieldName = 'OUTRAS_DESPESAS'
    end
    object qVendasDevolucoesDESCONTO: TFloatField
      FieldName = 'DESCONTO'
    end
    object qVendasDevolucoesVALOR_BRUTO: TFloatField
      FieldName = 'VALOR_BRUTO'
    end
    object qVendasDevolucoesVALOR_LIQUIDO: TFloatField
      FieldName = 'VALOR_LIQUIDO'
    end
    object qVendasDevolucoesPERC_LUCRO_BRUTO: TFloatField
      FieldName = 'PERC_LUCRO_BRUTO'
    end
    object qVendasDevolucoesPERC_LUCRO_LIQUIDO: TFloatField
      FieldName = 'PERC_LUCRO_LIQUIDO'
    end
    object qVendasDevolucoesEMPRESA: TStringField
      FieldName = 'EMPRESA'
      Size = 200
    end
    object qVendasDevolucoesDEVOLUCAO: TStringField
      FieldName = 'DEVOLUCAO'
      Size = 50
    end
    object qVendasDevolucoesPOSICAO: TFloatField
      FieldName = 'POSICAO'
    end
    object qVendasDevolucoesMOVIMENTO: TStringField
      FieldName = 'MOVIMENTO'
    end
  end
  object dsVendasDevolucoes: TOraDataSource
    DataSet = qVendasDevolucoes
    Left = 792
    Top = 264
  end
  object qVendasDevolucoesProdutos: TOraQuery
    SQL.Strings = (
      'select * from TEMP_PROD_VEND_DEVOL')
    MasterSource = dsVendasDevolucoes
    MasterFields = 'POSICAO'
    DetailFields = 'POSICAO'
    Left = 923
    Top = 218
    ParamData = <
      item
        DataType = ftFloat
        Name = 'POSICAO'
        ParamType = ptInput
        Value = 7.000000000000000000
      end>
    object qVendasDevolucoesProdutosPOSICAO: TFloatField
      FieldName = 'POSICAO'
    end
    object qVendasDevolucoesProdutosPRODUTO_ID: TFloatField
      FieldName = 'PRODUTO_ID'
    end
    object qVendasDevolucoesProdutosNOME: TStringField
      FieldName = 'NOME'
      Size = 60
    end
    object qVendasDevolucoesProdutosMARCA: TStringField
      FieldName = 'MARCA'
      Size = 60
    end
    object qVendasDevolucoesProdutosPRECO_UNITARIO: TFloatField
      FieldName = 'PRECO_UNITARIO'
    end
    object qVendasDevolucoesProdutosQUANTIDADE: TFloatField
      FieldName = 'QUANTIDADE'
    end
    object qVendasDevolucoesProdutosUNIDADE: TStringField
      FieldName = 'UNIDADE'
      Size = 10
    end
    object qVendasDevolucoesProdutosVALOR_TOTAL: TFloatField
      FieldName = 'VALOR_TOTAL'
    end
    object qVendasDevolucoesProdutosVALOR_LUCRO_BRUTO: TFloatField
      FieldName = 'VALOR_LUCRO_BRUTO'
    end
    object qVendasDevolucoesProdutosVALOR_LUCRO_LIQUIDO: TFloatField
      FieldName = 'VALOR_LUCRO_LIQUIDO'
    end
    object qVendasDevolucoesProdutosPERC_LUCRO_BRUTO: TFloatField
      FieldName = 'PERC_LUCRO_BRUTO'
    end
    object qVendasDevolucoesProdutosPERC_LUCRO_LIQUIDO: TFloatField
      FieldName = 'PERC_LUCRO_LIQUIDO'
    end
    object qVendasDevolucoesProdutosOUTRAS_DESPESAS: TFloatField
      FieldName = 'OUTRAS_DESPESAS'
    end
    object qVendasDevolucoesProdutosDESCONTO: TFloatField
      FieldName = 'DESCONTO'
    end
  end
  object frxReportAnalitico: TfrxReport
    Version = '5.2.3'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44043.704685613400000000
    ReportOptions.LastChange = 44404.792167442130000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 736
    Top = 329
    Datasets = <
      item
        DataSet = dmRelatorio.dstEmpresa
        DataSetName = 'frxdstEmpresa'
      end
      item
        DataSet = dstVendasDevolucoes
        DataSetName = 'frxdstVendasDevolucoes'
      end
      item
        DataSet = dstProdutosVendasDevolucoes
        DataSetName = 'frxdstVendasDevolucoesProdutos'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 86.929190000000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Shape1: TfrxShapeView
          Top = 70.811070000000000000
          Width = 1046.929810000000000000
          Height = 15.118120000000000000
          Fill.BackColor = clBtnShadow
        end
        object Memo1: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000001000
          Width = 68.031540000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 26.456710000000000000
          Top = 34.015770000000010000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 15.118120000000000000
          Top = 49.133889999999990000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 664.197280000002000000
          Top = 3.779530000000001000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 645.299630000002000000
          Top = 34.015770000000010000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade/UF:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 664.197280000002000000
          Top = 49.133890000000010000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 82.708720000000000000
          Top = 3.779530000000001000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 707.551640000001900000
          Top = 3.779530000000001000
          Width = 200.315090000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."CNPJ"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 84.929190000000000000
          Top = 18.897650000000000000
          Width = 551.811380000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            
              '[frxdstEmpresa."LOGRADOURO"] [frxdstEmpresa."COMPLEMENTO"] n'#176'[fr' +
              'xdstEmpresa."NUMERO"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 82.708720000000000000
          Top = 34.015770000000010000
          Width = 207.874150000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_BAIRRO"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 707.551640000001900000
          Top = 34.015770000000010000
          Width = 185.196970000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_CIDADE"] / [frxdstEmpresa."ESTADO_ID"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 82.708720000000000000
          Top = 49.133889999999990000
          Width = 75.590600000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 707.551640000001900000
          Top = 49.133890000000010000
          Width = 260.787570000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."E_MAIL"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 869.291900000002000000
          Top = 3.779530000000001000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Emitido em [Date] '#224's [Time]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Top = 70.811070000000000000
          Width = 41.574830000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Pedido')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 128.503951650000000000
          Top = 70.811070000000000000
          Width = 86.929190000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 914.646260000000000000
          Top = 70.811070000000000000
          Width = 132.283550000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Empresa')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 300.921460000000000000
          Top = 70.811070000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Vendedor')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 392.582677170000000000
          Top = 70.811070000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cond. Pgto')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 532.913730000000000000
          Top = 70.811070000000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Vlr Total')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 869.291900000002000000
          Top = 18.897650000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pag. [Page] de [TotalPages#]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo32: TfrxMemoView
          Left = 465.661720000000000000
          Top = 70.811070000000000000
          Width = 64.251970940000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Vlr Produto')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 238.110390000000000000
          Top = 70.811070000000000000
          Width = 60.472440940000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Dta Pgto')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 596.386210000000000000
          Top = 70.811070000000000000
          Width = 41.574830000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Desp.')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 640.520100000000000000
          Top = 70.811070000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Desconto')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 695.992580000000000000
          Top = 70.811070000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Luc. Bto')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 747.567410000000000000
          Top = 70.811070000000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '% Luc Bto')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 811.039890000000000000
          Top = 70.811070000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Luc Liq')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 859.173780000000000000
          Top = 70.811070000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '%Luc Liq')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 40.574830000000000000
          Top = 70.811070000000000000
          Width = 41.574830000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Devol.')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 84.149660000000000000
          Top = 70.811070000000000000
          Width = 41.574830000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Movim.')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 37.795300000000000000
        Top = 166.299320000000000000
        Width = 1046.929810000000000000
        DataSet = dstVendasDevolucoes
        DataSetName = 'frxdstVendasDevolucoes'
        RowCount = 0
        object Shape5: TfrxShapeView
          Top = 18.897650000000000000
          Width = 1046.929810000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
        object Shape4: TfrxShapeView
          Width = 1046.929810000000000000
          Height = 15.118120000000000000
          Fill.BackColor = clScrollBar
        end
        object frxdstTranscodigoProd: TfrxMemoView
          Top = 1.000000000000000000
          Width = 41.574830000000000000
          Height = 11.338590000000000000
          DataField = 'pedido'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."pedido"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 128.504020000000000000
          Top = 1.000000000000000000
          Width = 105.826840000000000000
          Height = 11.338590000000000000
          DataField = 'cliente'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."cliente"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 300.921460000000000000
          Top = 1.000000000000000000
          Width = 90.708720000000000000
          Height = 11.338590000000000000
          DataField = 'vendedor'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."vendedor"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 238.110390000000000000
          Top = 1.000000000000000000
          Width = 60.472480000000000000
          Height = 11.338590000000000000
          DataField = 'data_recebimento'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."data_recebimento"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 465.637795280000000000
          Top = 1.000000000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          DataField = 'valor_produto'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."valor_produto"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 914.645669290000000000
          Top = 1.000000000000000000
          Width = 132.283550000000000000
          Height = 11.338590000000000000
          DataField = 'empresa'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."empresa"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 392.630180000000000000
          Top = 1.000000000000000000
          Width = 71.811026060000000000
          Height = 11.338590000000000000
          DataField = 'cond_pgto'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."cond_pgto"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 532.913730000000000000
          Top = 1.000000000000000000
          Width = 60.472440944881890000
          Height = 11.338590000000000000
          DataField = 'valor_total'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."valor_total"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 596.409448820000000000
          Top = 1.000000000000000000
          Width = 41.574803149606300000
          Height = 11.338590000000000000
          DataField = 'outras_despesas'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."outras_despesas"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 640.629921260000000000
          Top = 1.000000000000000000
          Width = 52.913385826771650000
          Height = 11.338590000000000000
          DataField = 'desconto'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."desconto"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 695.811023620000000000
          Top = 1.000000000000000000
          Width = 49.133860710000000000
          Height = 11.338590000000000000
          DataField = 'valor_bruto'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."valor_bruto"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 811.086614170000000000
          Top = 1.000000000000000000
          Width = 45.354330708661420000
          Height = 11.338590000000000000
          DataField = 'valor_liquido'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."valor_liquido"]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 747.590551180000000000
          Top = 1.000000000000000000
          Width = 60.472440940000000000
          Height = 11.338590000000000000
          DataField = 'perc_lucro_bruto'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."perc_lucro_bruto"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 859.086614170000000000
          Top = 1.000000000000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          DataField = 'perc_lucro_liquido'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."perc_lucro_liquido"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 42.574830000000000000
          Top = 1.000000000000000000
          Width = 41.574830000000000000
          Height = 11.338590000000000000
          DataField = 'devolucao'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."devolucao"]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 4.559060000000000000
          Top = 18.897650000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 52.133890000000000000
          Top = 18.897650000000000000
          Width = 207.874150000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 264.212552050000000000
          Top = 18.897650000000000000
          Width = 128.504020000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 395.630180000000000000
          Top = 18.897650000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Pr'#231'. Unit.')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 451.323130000000000000
          Top = 18.897650000000000000
          Width = 30.236240000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Qtd.')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 483.338900000000000000
          Top = 18.897650000000000000
          Width = 26.456710000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Und.')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 514.015735830000000000
          Top = 18.897650000000000000
          Width = 60.472440940000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Vlr. Tot.')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 621.732271260000000000
          Top = 18.897650000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Desconto')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 678.315400000000000000
          Top = 18.897650000000000000
          Width = 52.913388270000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Luc. Bru.')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 733.228820000000000000
          Top = 18.897650000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '% Luc. Bru.')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 800.260360000000000000
          Top = 18.897650000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Luc. Liq.')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 868.291900000000000000
          Top = 18.897650000000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '% Luc. Liq.')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 577.268090000000000000
          Top = 18.897650000000000000
          Width = 41.574803150000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Desp.')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Left = 84.283464570000000000
          Top = 1.000000000000000000
          Width = 41.574830000000000000
          Height = 11.338590000000000000
          DataField = 'MOVIMENTO'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."MOVIMENTO"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 170.456802760000000000
        Top = 306.141930000000000000
        Width = 1046.929810000000000000
        object Shape3: TfrxShapeView
          Width = 1046.929810000000000000
          Height = 166.299320000000000000
          Fill.BackColor = cl3DLight
        end
        object Memo44: TfrxMemoView
          Left = 283.685220000000000000
          Top = 3.779530000000000000
          Width = 211.653680000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Totais....................:')
          ParentFont = False
        end
        object mmPercLucroBruto: TfrxMemoView
          Left = 336.378170000000000000
          Top = 110.385900000000000000
          Width = 498.897960000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '% Lucro Bruto.....: ')
          ParentFont = False
        end
        object mmPercLucroLiq: TfrxMemoView
          Left = 336.378170000000000000
          Top = 141.842610000000000000
          Width = 498.897960000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '% Lucro Liquido...: ')
          ParentFont = False
        end
        object mmValorLiquido: TfrxMemoView
          Left = 336.378170000000000000
          Top = 126.724490000000000000
          Width = 498.897960000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Valor Liquido.....: ')
          ParentFont = False
        end
        object mmValorBruto: TfrxMemoView
          Left = 336.378170000000000000
          Top = 91.708720000000000000
          Width = 498.897960000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Valor Bruto.......: ')
          ParentFont = False
        end
        object mmDescontos: TfrxMemoView
          Left = 336.378170000000000000
          Top = 74.590600000000000000
          Width = 498.897960000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Descontos.........: ')
          ParentFont = False
        end
        object mmOutrasDespesas: TfrxMemoView
          Left = 336.378170000000000000
          Top = 56.692950000000000000
          Width = 498.897960000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Outras Despesas...: ')
          ParentFont = False
        end
        object mmValorTotal: TfrxMemoView
          Left = 336.378170000000000000
          Top = 39.795300000000000000
          Width = 498.897960000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Valor Total.......: ')
          ParentFont = False
        end
        object mmValorProduto: TfrxMemoView
          Left = 336.378170000000000000
          Top = 18.897650000000000000
          Width = 498.897960000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Valor Produto.....: ')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 498.897960000000000000
        Width = 1046.929810000000000000
        object Shape2: TfrxShapeView
          Width = 1046.929133860000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 226.771800000000000000
        Width = 1046.929810000000000000
        DataSet = dstProdutosVendasDevolucoes
        DataSetName = 'frxdstVendasDevolucoesProdutos'
        RowCount = 0
        object Shape6: TfrxShapeView
          Left = -1.000000000000000000
          Width = 1046.929810000000000000
          Height = 15.118120000000000000
          Fill.BackColor = clBtnFace
        end
        object frxdstVendasDevolucoesProdutosPRODUTO_ID: TfrxMemoView
          Left = 4.559047800000000000
          Top = 0.220470000000000000
          Width = 45.354335590000000000
          Height = 15.118120000000000000
          DataField = 'PRODUTO_ID'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."PRODUTO_ID"]')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 52.133890000000000000
          Width = 207.873957170000000000
          Height = 15.118120000000000000
          DataField = 'NOME'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."NOME"]')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 264.346630000000000000
          Width = 128.503961420000000000
          Height = 15.118120000000000000
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."MARCA"]')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 395.716523230000000000
          Width = 52.913385830000000000
          Height = 15.118120000000000000
          DataField = 'PRECO_UNITARIO'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."PRECO_UNITARIO"]')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 451.323130000000000000
          Width = 30.236220470000000000
          Height = 15.118120000000000000
          DataField = 'QUANTIDADE'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."QUANTIDADE"]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Left = 483.338900000000000000
          Width = 26.456688030000000000
          Height = 15.118120000000000000
          DataField = 'UNIDADE'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."UNIDADE"]')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 514.015735830000000000
          Width = 60.472440940000000000
          Height = 15.118120000000000000
          DataField = 'valor_total'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."valor_total"]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 577.268090000000000000
          Width = 41.574803150000000000
          Height = 15.118120000000000000
          DataField = 'OUTRAS_DESPESAS'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."OUTRAS_DESPESAS"]')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 621.732271260000000000
          Width = 52.913390710000000000
          Height = 15.118120000000000000
          DataField = 'DESCONTO'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."DESCONTO"]')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 678.315400000000000000
          Width = 52.913388270000000000
          Height = 15.118120000000000000
          DataField = 'VALOR_LUCRO_BRUTO'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."VALOR_LUCRO_BRUTO"]')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 733.228820000000000000
          Width = 64.251968500000000000
          Height = 15.118120000000000000
          DataField = 'perc_lucro_bruto'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."perc_lucro_bruto"]')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Left = 800.260360000000000000
          Width = 64.251968500000000000
          Height = 15.118120000000000000
          DataField = 'VALOR_LUCRO_LIQUIDO'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."VALOR_LUCRO_LIQUIDO"]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 868.291900000000000000
          Width = 60.472440940000000000
          Height = 15.118120000000000000
          DataField = 'perc_lucro_liquido'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."perc_lucro_liquido"]')
          ParentFont = False
        end
      end
    end
  end
  object frxReport1: TfrxReport
    Version = '5.2.3'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44043.704685613400000000
    ReportOptions.LastChange = 44403.956589780100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 936
    Top = 320
    Datasets = <
      item
        DataSet = dmRelatorio.dstEmpresa
        DataSetName = 'frxdstEmpresa'
      end
      item
        DataSet = dstVendasDevolucoes
        DataSetName = 'frxdstVendasDevolucoes'
      end
      item
        DataSet = dstProdutosVendasDevolucoes
        DataSetName = 'frxdstVendasDevolucoesProdutos'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 86.929190000000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Shape1: TfrxShapeView
          Top = 70.811070000000000000
          Width = 1046.929810000000000000
          Height = 15.118120000000000000
          Fill.BackColor = clBtnShadow
        end
        object Memo1: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000001000
          Width = 68.031540000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 26.456710000000000000
          Top = 34.015770000000010000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 15.118120000000000000
          Top = 49.133889999999990000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 664.197280000002000000
          Top = 3.779530000000001000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 645.299630000002000000
          Top = 34.015770000000010000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade/UF:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 664.197280000002000000
          Top = 49.133890000000010000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 82.708720000000000000
          Top = 3.779530000000001000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 707.551640000001900000
          Top = 3.779530000000001000
          Width = 200.315090000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."CNPJ"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 84.929190000000000000
          Top = 18.897650000000000000
          Width = 551.811380000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            
              '[frxdstEmpresa."LOGRADOURO"] [frxdstEmpresa."COMPLEMENTO"] n'#176'[fr' +
              'xdstEmpresa."NUMERO"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 82.708720000000000000
          Top = 34.015770000000010000
          Width = 207.874150000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_BAIRRO"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 707.551640000001900000
          Top = 34.015770000000010000
          Width = 185.196970000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_CIDADE"] / [frxdstEmpresa."ESTADO_ID"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 82.708720000000000000
          Top = 49.133889999999990000
          Width = 75.590600000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 707.551640000001900000
          Top = 49.133890000000010000
          Width = 260.787570000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."E_MAIL"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 869.291900000002000000
          Top = 3.779530000000001000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Emitido em [Date] '#224's [Time]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Top = 70.811070000000000000
          Width = 37.795300000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Pedido')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 84.929190000000000000
          Top = 70.811070000000000000
          Width = 86.929190000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 912.866730000000000000
          Top = 70.811070000000000000
          Width = 132.283550000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Empresa')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 278.244280000000000000
          Top = 70.811070000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Vendedor')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 395.630180000000000000
          Top = 70.811070000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Cond. Pgto')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 548.031850000000000000
          Top = 70.811070000000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Valor Total')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 869.291900000002000000
          Top = 18.897650000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pag. [Page] de [TotalPages#]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo32: TfrxMemoView
          Left = 473.220780000000000000
          Top = 70.811070000000000000
          Width = 79.370130000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Valor Produto')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 215.433210000000000000
          Top = 70.811070000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Dta Pgto')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 602.945270000000000000
          Top = 70.811070000000000000
          Width = 41.574830000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Desp.')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 646.299630000000000000
          Top = 70.811070000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Desconto')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 694.213050000000000000
          Top = 70.811070000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Luc. Bruto')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 747.567410000000000000
          Top = 70.811070000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '% Luc. Bruto')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 811.819420000000000000
          Top = 70.811070000000000000
          Width = 41.574830000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Luc. Liq')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 856.173780000000000000
          Top = 70.811070000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '%Luc. Liq')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 40.574830000000000000
          Top = 70.811070000000000000
          Width = 41.574830000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Devol.')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 37.795300000000000000
        Top = 166.299320000000000000
        Width = 1046.929810000000000000
        DataSet = dstVendasDevolucoes
        DataSetName = 'frxdstVendasDevolucoes'
        RowCount = 0
        object frxdstTranscodigoProd: TfrxMemoView
          Top = 1.000000000000000000
          Width = 41.574830000000000000
          Height = 11.338590000000000000
          DataField = 'pedido'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."pedido"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 88.708720000000000000
          Top = 1.000000000000000000
          Width = 124.724490000000000000
          Height = 11.338590000000000000
          DataField = 'cliente'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."cliente"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 278.244280000000000000
          Top = 1.000000000000000000
          Width = 117.165430000000000000
          Height = 11.338590000000000000
          DataField = 'vendedor'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."vendedor"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 215.433210000000000000
          Top = 1.000000000000000000
          Width = 60.472480000000000000
          Height = 11.338590000000000000
          DataField = 'data_recebimento'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."data_recebimento"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 492.118430000000000000
          Top = 1.000000000000000000
          Width = 60.472480000000000000
          Height = 11.338590000000000000
          DataField = 'valor_produto'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."valor_produto"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 912.866730000000000000
          Top = 1.000000000000000000
          Width = 132.283550000000000000
          Height = 11.338590000000000000
          DataField = 'empresa'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."empresa"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 395.630180000000000000
          Top = 1.000000000000000000
          Width = 94.488250000000000000
          Height = 11.338590000000000000
          DataField = 'cond_pgto'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."cond_pgto"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 555.590910000000000000
          Top = 1.000000000000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          DataField = 'valor_total'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."valor_total"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 610.504330000000000000
          Top = 1.000000000000000000
          Width = 34.015770000000000000
          Height = 11.338590000000000000
          DataField = 'outras_despesas'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."outras_despesas"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 662.417750000000000000
          Top = 1.000000000000000000
          Width = 30.236240000000000000
          Height = 11.338590000000000000
          DataField = 'desconto'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."desconto"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 690.433520000000000000
          Top = 1.000000000000000000
          Width = 56.692950000000000000
          Height = 11.338590000000000000
          DataField = 'valor_bruto'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."valor_bruto"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 815.598950000000000000
          Top = 1.000000000000000000
          Width = 37.795300000000000000
          Height = 11.338590000000000000
          DataField = 'valor_liquido'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."valor_liquido"]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 751.346940000000000000
          Top = 1.000000000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          DataField = 'perc_lucro_bruto'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."perc_lucro_bruto"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 856.173780000000000000
          Top = 1.000000000000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          DataField = 'perc_lucro_liquido'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."perc_lucro_liquido"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 43.574830000000000000
          Top = 1.000000000000000000
          Width = 41.574830000000000000
          Height = 11.338590000000000000
          DataField = 'devolucao'
          DataSet = dstVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoes."devolucao"]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 16.897650000000000000
          Top = 18.897650000000000000
          Width = 41.574830000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 60.472480000000000000
          Top = 18.897650000000000000
          Width = 275.905690000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 339.023622047244100000
          Top = 18.897650000000000000
          Width = 90.708720000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 433.645950000000000000
          Top = 18.897650000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Pr'#231'. Unit.')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 491.338900000000000000
          Top = 18.897650000000000000
          Width = 41.574830000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Qtd.')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 536.693260000000000000
          Top = 18.897650000000000000
          Width = 26.456710000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Und.')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 566.929500000000000000
          Top = 18.897650000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Vlr. Tot.')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 702.992580000000000000
          Top = 18.897650000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Vlr. Desc.')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 759.685530000000000000
          Top = 18.897650000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Vlr. Luc. Bru.')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 831.496600000000000000
          Top = 18.897650000000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '% Luc. Bru.')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 895.748610000000000000
          Top = 18.897650000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Vlr. Luc. Liq.')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 963.780150000000000000
          Top = 18.897650000000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '% Luc. Liq.')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 623.622450000000000000
          Top = 18.897650000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Vlr. Out. Desp.')
          ParentFont = False
        end
        object Shape4: TfrxShapeView
          Width = 1046.929810000000000000
          Height = 15.118120000000000000
          Fill.BackColor = clScrollBar
        end
        object Shape5: TfrxShapeView
          Top = 18.897650000000000000
          Width = 1046.929810000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 170.456802760000000000
        Top = 306.141930000000000000
        Visible = False
        Width = 1046.929810000000000000
        object Shape3: TfrxShapeView
          Width = 1046.929810000000000000
          Height = 166.299320000000000000
          Fill.BackColor = cl3DLight
        end
        object Memo44: TfrxMemoView
          Left = 283.685220000000000000
          Top = 3.779529999999994000
          Width = 211.653680000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Totais....................:')
          ParentFont = False
        end
        object SysMemo1: TfrxSysMemoView
          Left = 336.378170000000000000
          Top = 22.456710000000000000
          Width = 464.882077720000000000
          Height = 15.118120000000000000
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            
              'Valor Produto.....: [SUM(<frxdstVendasDevolucoes."valor_produto"' +
              '>,MasterData1)]')
          ParentFont = False
        end
        object SysMemo2: TfrxSysMemoView
          Left = 336.378170000000000000
          Top = 41.354360000000000000
          Width = 464.882077720000000000
          Height = 15.118120000000000000
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            
              'Valor Total.......: [SUM(<frxdstVendasDevolucoes."valor_total">,' +
              'MasterData1)]')
          ParentFont = False
        end
        object SysMemo3: TfrxSysMemoView
          Left = 336.378170000000000000
          Top = 58.252010000000000000
          Width = 521.575027720000000000
          Height = 15.118120000000000000
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            
              'Outras Despesas...: [SUM(<frxdstVendasDevolucoes."outras_despesa' +
              's">,MasterData1)]')
          ParentFont = False
        end
        object SysMemo4: TfrxSysMemoView
          Left = 336.378170000000000000
          Top = 75.149660000000000000
          Width = 464.882077720000000000
          Height = 15.118120000000000000
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            
              'Descontos.........: [SUM(<frxdstVendasDevolucoes."desconto">,Mas' +
              'terData1)]')
          ParentFont = False
        end
        object SysMemo5: TfrxSysMemoView
          Left = 336.378170000000000000
          Top = 93.047310000000000000
          Width = 464.882077720000000000
          Height = 15.118120000000000000
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            
              'Valor Bruto.......: [SUM(<frxdstVendasDevolucoes."valor_bruto">,' +
              'MasterData1)]')
          ParentFont = False
        end
        object SysMemo7: TfrxSysMemoView
          Left = 336.378170000000000000
          Top = 126.063080000000000000
          Width = 464.882077720000000000
          Height = 15.118120000000000000
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            
              'Valor Liquido.....: [SUM(<frxdstVendasDevolucoes."valor_liquido"' +
              '>,MasterData1)]')
          ParentFont = False
        end
        object mmPercLucroBruto: TfrxMemoView
          Left = 336.378170000000000000
          Top = 110.385900000000000000
          Width = 498.897960000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '% Lucro Bruto.....: ')
          ParentFont = False
        end
        object mmPercLucroLiq: TfrxMemoView
          Left = 336.378170000000000000
          Top = 139.842610000000000000
          Width = 498.897960000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '% Lucro Liquido...: ')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 498.897960000000000000
        Width = 1046.929810000000000000
        object Shape2: TfrxShapeView
          Width = 1046.929133860000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 226.771800000000000000
        Width = 1046.929810000000000000
        DataSet = dstProdutosVendasDevolucoes
        DataSetName = 'frxdstVendasDevolucoesProdutos'
        RowCount = 0
        object frxdstVendasDevolucoesProdutosPRODUTO_ID: TfrxMemoView
          Left = 16.897637800000000000
          Top = 0.220470000000000000
          Width = 41.574805590000000000
          Height = 15.118120000000000000
          DataField = 'PRODUTO_ID'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."PRODUTO_ID"]')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 60.472480000000000000
          Width = 275.905497170000000000
          Height = 15.118120000000000000
          DataField = 'NOME'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."NOME"]')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 339.157700000000000000
          Width = 90.708661420000000000
          Height = 15.118120000000000000
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."MARCA"]')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 433.645950000000000000
          Width = 52.913385830000000000
          Height = 15.118120000000000000
          DataField = 'PRECO_UNITARIO'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."PRECO_UNITARIO"]')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 491.338900000000000000
          Width = 41.574810470000000000
          Height = 15.118120000000000000
          DataField = 'QUANTIDADE'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."QUANTIDADE"]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Left = 536.693260000000000000
          Width = 26.456688030000000000
          Height = 15.118120000000000000
          DataField = 'UNIDADE'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."UNIDADE"]')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 566.929500000000000000
          Width = 52.913390710000000000
          Height = 15.118120000000000000
          DataField = 'valor_total'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."valor_total"]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 623.622450000000000000
          Width = 75.590551180000000000
          Height = 15.118120000000000000
          DataField = 'OUTRAS_DESPESAS'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."OUTRAS_DESPESAS"]')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 702.992580000000000000
          Width = 52.913390710000000000
          Height = 15.118120000000000000
          DataField = 'DESCONTO'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."DESCONTO"]')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 759.685530000000000000
          Width = 68.031496062992130000
          Height = 15.118120000000000000
          DataField = 'VALOR_LUCRO_BRUTO'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."VALOR_LUCRO_BRUTO"]')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 831.496600000000000000
          Width = 60.472440940000000000
          Height = 15.118120000000000000
          DataField = 'perc_lucro_bruto'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."perc_lucro_bruto"]')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Left = 895.748610000000000000
          Width = 64.251968500000000000
          Height = 15.118120000000000000
          DataField = 'VALOR_LUCRO_LIQUIDO'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."VALOR_LUCRO_LIQUIDO"]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 963.780150000000000000
          Width = 60.472440940000000000
          Height = 15.118120000000000000
          DataField = 'perc_lucro_liquido'
          DataSet = dstProdutosVendasDevolucoes
          DataSetName = 'frxdstVendasDevolucoesProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstVendasDevolucoesProdutos."perc_lucro_liquido"]')
          ParentFont = False
        end
        object Shape6: TfrxShapeView
          Width = 1046.929810000000000000
          Height = 15.118120000000000000
          Fill.BackColor = clBtnFace
        end
      end
    end
  end
end
