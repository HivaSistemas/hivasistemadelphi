unit FrameProdutosGruposTributacoesEmpresas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.Grids, _Imagens, _RecordsCadastros,
  GridLuka, Vcl.StdCtrls, Vcl.Menus, StaticTextLuka, _Biblioteca, BuscarGruposTributacoesEmpresas,
  _Sessao, _ProdutosGruposTribEmpresas;

type
  TFrProdutosGruposTributacoesEmpresas = class(TFrameHerancaPrincipal)
    sgGrupos: TGridLuka;
    st1: TStaticTextLuka;
    pmOpcoes: TPopupMenu;
    miMarcarDesmarcarFocado: TMenuItem;
    miMarcarDesmarcarTodos: TMenuItem;
    miN1: TMenuItem;
    miAlterarGruposEmpresaFocada: TMenuItem;
    miN3: TMenuItem;
    miAlterarGruposEmpresasSelecionadas: TMenuItem;
    procedure sgGruposDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure miMarcarDesmarcarFocadoClick(Sender: TObject);
    procedure miMarcarDesmarcarTodosClick(Sender: TObject);
    procedure miAlterarGruposEmpresaFocadaClick(Sender: TObject);
    procedure sgGruposGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure miAlterarGruposEmpresasSelecionadasClick(Sender: TObject);
    procedure sgGruposKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgGruposDblClick(Sender: TObject);
  private
    function getRecProdutosGruposTribEmpresas: TArray<RecProdutosGruposTribEmpresas>;
    procedure setProdutosGruposTribEmpresas(const pGrupos: TArray<RecProdutosGruposTribEmpresas>);
  public
    constructor Create(AOwner: TComponent); override;

    procedure Clear; override;
    procedure SomenteLeitura(pValue: Boolean); override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;

    function EstaVazio: Boolean; override;

    procedure VerificarRegistro;
  published
    property ProdutosGruposTribEmpresas: TArray<RecProdutosGruposTribEmpresas> read getRecProdutosGruposTribEmpresas write setProdutosGruposTribEmpresas;
  end;

implementation

{$R *.dfm}

uses InformacoesGruposTributacoesEstadualCompra, InformacoesGruposTributacoesEstadualVenda, InformacoesGruposTributacoesFederalCompra, InformacoesGruposTributacoesFederalVenda;

const
  coSelecionado             = 0;
  coEmpresaId               = 1;
  coNomeEmpresa             = 2;
  coGrupoTribEstadualCompra = 3;
  coGrupoTribEstadualVenda  = 4;
  coGrupoTribFederalCompra  = 5;
  coGrupoTribFederalVenda   = 6;

  (* Ocultas *)
  coGrupoTribEstadualCompraId = 7;
  coGrupoTribEstadualVendaId  = 8;
  coGrupoTribFederalCompraId  = 9;
  coGrupoTribFederalVendaId   = 10;

procedure TFrProdutosGruposTributacoesEmpresas.Clear;
var
  i: Integer;
begin
  inherited;
  for i := 1 to sgGrupos.RowCount -1 do begin
    sgGrupos.Cells[coGrupoTribEstadualCompra, i] := '';
    sgGrupos.Cells[coGrupoTribEstadualVenda, i]  := '';
    sgGrupos.Cells[coGrupoTribFederalCompra, i]  := '';
    sgGrupos.Cells[coGrupoTribFederalVenda, i]   := '';

    sgGrupos.Cells[coGrupoTribEstadualCompraId, i] := '';
    sgGrupos.Cells[coGrupoTribEstadualVendaId, i]  := '';
    sgGrupos.Cells[coGrupoTribFederalCompraId, i]  := '';
    sgGrupos.Cells[coGrupoTribFederalVendaId, i]   := '';
  end;
end;

constructor TFrProdutosGruposTributacoesEmpresas.Create(AOwner: TComponent);
var
  i: Integer;
  vEmpresas: TArray<RecEmpresas>;
begin
  inherited;
  sgGrupos.Col := coGrupoTribEstadualCompra;

  vEmpresas := _Sessao.Sessao.getEmpresas;
  for i := Low(vEmpresas) to High(vEmpresas) do begin
    sgGrupos.Cells[coSelecionado, i + 1] := _Biblioteca.charNaoSelecionado;
    sgGrupos.Cells[coEmpresaId, i + 1]   := NFormat(vEmpresas[i].EmpresaId);
    sgGrupos.Cells[coNomeEmpresa, i + 1] := vEmpresas[i].NomeFantasia;
  end;
  sgGrupos.SetLinhasGridPorTamanhoVetor( Length(vEmpresas) );
  sgGrupos.Repaint;
end;

function TFrProdutosGruposTributacoesEmpresas.EstaVazio: Boolean;
begin
  Result := False;
end;

function TFrProdutosGruposTributacoesEmpresas.getRecProdutosGruposTribEmpresas: TArray<RecProdutosGruposTribEmpresas>;
var
  i: Integer;
begin
  SetLength(Result, sgGrupos.RowCount -1);
  for i := 1 to sgGrupos.RowCount -1 do begin
    Result[i - 1].EmpresaId                 := SFormatInt(sgGrupos.Cells[coEmpresaId, i]);
    Result[i - 1].GrupoTribEstadualCompraId := SFormatInt(sgGrupos.Cells[coGrupoTribEstadualCompraId, i]);
    Result[i - 1].GrupoTribEstadualVendaId  := SFormatInt(sgGrupos.Cells[coGrupoTribEstadualVendaId, i]);
    Result[i - 1].GrupoTribFederalCompraId  := SFormatInt(sgGrupos.Cells[coGrupoTribFederalCompraId, i]);
    Result[i - 1].GrupoTribFederalVendaId   := SFormatInt(sgGrupos.Cells[coGrupoTribFederalVendaId, i]);
  end;
end;

procedure TFrProdutosGruposTributacoesEmpresas.miAlterarGruposEmpresaFocadaClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar<RecGruposTributacoesEmpresas>;
begin
  inherited;

  vRetTela.Dados.GrupoTribEstadualCompraId := SFormatInt(sgGrupos.Cells[coGrupoTribEstadualCompraId, sgGrupos.Row]);
  vRetTela.Dados.GrupoTribEstadualVendaId  := SFormatInt(sgGrupos.Cells[coGrupoTribEstadualVendaId, sgGrupos.Row]);
  vRetTela.Dados.GrupoTribFederalCompraId  := SFormatInt(sgGrupos.Cells[coGrupoTribFederalCompraId, sgGrupos.Row]);
  vRetTela.Dados.GrupoTribFederalVendaId   := SFormatInt(sgGrupos.Cells[coGrupoTribFederalVendaId, sgGrupos.Row]);

  vRetTela := BuscarGruposTributacoesEmpresas.BuscarGrupos(vRetTela.Dados);
  if vRetTela.BuscaCancelada then
    Exit;

  sgGrupos.Cells[coGrupoTribEstadualCompra, sgGrupos.Row] := getInformacao(vRetTela.Dados.GrupoTribEstadualCompraId, vRetTela.Dados.NomeGrupoTribEstadualCompra);
  sgGrupos.Cells[coGrupoTribEstadualVenda, sgGrupos.Row]  := getInformacao(vRetTela.Dados.GrupoTribEstadualVendaId, vRetTela.Dados.NomeGrupoTribEstadualVenda);
  sgGrupos.Cells[coGrupoTribFederalCompra, sgGrupos.Row]  := getInformacao(vRetTela.Dados.GrupoTribFederalCompraId, vRetTela.Dados.NomeGrupoTribFederalCompra);
  sgGrupos.Cells[coGrupoTribFederalVenda, sgGrupos.Row]   := getInformacao(vRetTela.Dados.GrupoTribFederalVendaId, vRetTela.Dados.NomeGrupoTribFederalVenda);

  sgGrupos.Cells[coGrupoTribEstadualCompraId, sgGrupos.Row] := NFormat(vRetTela.Dados.GrupoTribEstadualCompraId);
  sgGrupos.Cells[coGrupoTribEstadualVendaId, sgGrupos.Row]  := NFormat(vRetTela.Dados.GrupoTribEstadualVendaId);
  sgGrupos.Cells[coGrupoTribFederalCompraId, sgGrupos.Row]  := NFormat(vRetTela.Dados.GrupoTribFederalCompraId);
  sgGrupos.Cells[coGrupoTribFederalVendaId, sgGrupos.Row]   := NFormat(vRetTela.Dados.GrupoTribFederalVendaId);
end;

procedure TFrProdutosGruposTributacoesEmpresas.miAlterarGruposEmpresasSelecionadasClick(Sender: TObject);
var
  i: Integer;
  vTemEmpresasSelecionadas: Boolean;
  vRetTela: TRetornoTelaFinalizar<RecGruposTributacoesEmpresas>;
begin
  inherited;
  vTemEmpresasSelecionadas := False;
  for i := 1 to sgGrupos.RowCount -1 do begin
    vTemEmpresasSelecionadas := sgGrupos.Cells[coSelecionado, i] = charSelecionado;
    if vTemEmpresasSelecionadas then
      Break;
  end;

  if not vTemEmpresasSelecionadas then begin
    _Biblioteca.NenhumRegistroSelecionado;
    SetarFoco(sgGrupos);
    Exit;
  end;

  vRetTela := BuscarGruposTributacoesEmpresas.BuscarGrupos(vRetTela.Dados);
  if vRetTela.BuscaCancelada then
    Exit;

  for i := 1 to sgGrupos.RowCount -1 do begin
    if sgGrupos.Cells[coSelecionado, i] <> charSelecionado then
      Continue;

    sgGrupos.Cells[coGrupoTribEstadualCompra, i] := getInformacao(vRetTela.Dados.GrupoTribEstadualCompraId, vRetTela.Dados.NomeGrupoTribEstadualCompra);
    sgGrupos.Cells[coGrupoTribEstadualVenda, i]  := getInformacao(vRetTela.Dados.GrupoTribEstadualVendaId, vRetTela.Dados.NomeGrupoTribEstadualVenda);
    sgGrupos.Cells[coGrupoTribFederalCompra, i]  := getInformacao(vRetTela.Dados.GrupoTribFederalCompraId, vRetTela.Dados.NomeGrupoTribFederalCompra);
    sgGrupos.Cells[coGrupoTribFederalVenda, i]   := getInformacao(vRetTela.Dados.GrupoTribFederalVendaId, vRetTela.Dados.NomeGrupoTribFederalVenda);

    sgGrupos.Cells[coGrupoTribEstadualCompraId, i] := NFormat(vRetTela.Dados.GrupoTribEstadualCompraId);
    sgGrupos.Cells[coGrupoTribEstadualVendaId, i]  := NFormat(vRetTela.Dados.GrupoTribEstadualVendaId);
    sgGrupos.Cells[coGrupoTribFederalCompraId, i]  := NFormat(vRetTela.Dados.GrupoTribFederalCompraId);
    sgGrupos.Cells[coGrupoTribFederalVendaId, i]   := NFormat(vRetTela.Dados.GrupoTribFederalVendaId);
  end;
end;

procedure TFrProdutosGruposTributacoesEmpresas.miMarcarDesmarcarFocadoClick(Sender: TObject);
begin
  inherited;
  sgGrupos.Cells[coSelecionado, sgGrupos.Row] := IIfStr(sgGrupos.Cells[coSelecionado, sgGrupos.Row] = charSelecionado, charNaoSelecionado, charSelecionado);
end;

procedure TFrProdutosGruposTributacoesEmpresas.miMarcarDesmarcarTodosClick(Sender: TObject);
var
  i: Integer;
  vMarcar: Boolean;
begin
  inherited;

  vMarcar := False;
  for i := 1 to sgGrupos.RowCount -1 do begin
    vMarcar := sgGrupos.Cells[coSelecionado, i] = charNaoSelecionado;
    Break;
  end;

  for i := 1 to sgGrupos.RowCount -1 do
    sgGrupos.Cells[coSelecionado, i] := IIfStr(vMarcar, charSelecionado, charNaoSelecionado);
end;

procedure TFrProdutosGruposTributacoesEmpresas.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;

end;

procedure TFrProdutosGruposTributacoesEmpresas.setProdutosGruposTribEmpresas(const pGrupos: TArray<RecProdutosGruposTribEmpresas>);
var
  i: Integer;
  vPosic: Integer;
begin
  for i := Low(pGrupos) to High(pGrupos) do begin
    vPosic := sgGrupos.Localizar([coEmpresaId], [NFormat(pGrupos[i].EmpresaId)]);

    sgGrupos.Cells[coGrupoTribEstadualCompra, vPosic] := getInformacao(pGrupos[i].GrupoTribEstadualCompraId, pGrupos[i].NomeGrupoTribEstadualCompra);
    sgGrupos.Cells[coGrupoTribEstadualVenda, vPosic]  := getInformacao(pGrupos[i].GrupoTribEstadualVendaId, pGrupos[i].NomeGrupoTribEstadualVenda);
    sgGrupos.Cells[coGrupoTribFederalCompra, vPosic]  := getInformacao(pGrupos[i].GrupoTribFederalCompraId, pGrupos[i].NomeGrupoTribFederalCompra);
    sgGrupos.Cells[coGrupoTribFederalVenda, vPosic]   := getInformacao(pGrupos[i].GrupoTribFederalVendaId, pGrupos[i].NomeGrupoTribFederalVenda);

    sgGrupos.Cells[coGrupoTribEstadualCompraId, vPosic] := NFormat(pGrupos[i].GrupoTribEstadualCompraId);
    sgGrupos.Cells[coGrupoTribEstadualVendaId, vPosic]  := NFormat(pGrupos[i].GrupoTribEstadualVendaId);
    sgGrupos.Cells[coGrupoTribFederalCompraId, vPosic]  := NFormat(pGrupos[i].GrupoTribFederalCompraId);
    sgGrupos.Cells[coGrupoTribFederalVendaId, vPosic]   := NFormat(pGrupos[i].GrupoTribFederalVendaId);
  end;
end;

procedure TFrProdutosGruposTributacoesEmpresas.sgGruposDblClick(Sender: TObject);
begin
  inherited;
  case sgGrupos.Col of
    coGrupoTribEstadualCompra: InformacoesGruposTributacoesEstadualCompra.Informar(SFormatInt(sgGrupos.Cells[coGrupoTribEstadualCompraId, sgGrupos.Row]));
    coGrupoTribEstadualVenda: InformacoesGruposTributacoesEstadualVenda.Informar(SFormatInt(sgGrupos.Cells[coGrupoTribEstadualVendaId, sgGrupos.Row]));
    coGrupoTribFederalCompra: InformacoesGruposTributacoesFederalCompra.Informar(SFormatInt(sgGrupos.Cells[coGrupoTribFederalCompraId, sgGrupos.Row]));
    coGrupoTribFederalVenda: InformacoesGruposTributacoesFederalVenda.Informar(SFormatInt(sgGrupos.Cells[coGrupoTribFederalVendaId, sgGrupos.Row]));
  end;
end;

procedure TFrProdutosGruposTributacoesEmpresas.sgGruposDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = coSelecionado then
    vAlinhamento := taCenter
  else if ACol in[coEmpresaId] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgGrupos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFrProdutosGruposTributacoesEmpresas.sgGruposGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol <> coSelecionado then
   Exit;

  if sgGrupos.Cells[ACol, ARow] = charNaoSelecionado then
    APicture := _Imagens.FormImagens.im1.Picture
  else if sgGrupos.Cells[ACol, ARow] = charSelecionado then
    APicture := _Imagens.FormImagens.im2.Picture;
end;

procedure TFrProdutosGruposTributacoesEmpresas.sgGruposKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Shift = [ssCtrl]) and (Key = VK_SPACE) then
    miMarcarDesmarcarTodosClick(Sender)
  else if Key = VK_SPACE then
    miMarcarDesmarcarFocadoClick(Sender);
end;

procedure TFrProdutosGruposTributacoesEmpresas.SomenteLeitura(pValue: Boolean);
begin
  inherited;

end;

procedure TFrProdutosGruposTributacoesEmpresas.VerificarRegistro;
var
  i: Integer;
begin
  for i := 1 to sgGrupos.RowCount -1 do begin
    if sgGrupos.Cells[coGrupoTribEstadualCompraId, i] = '' then begin
      _Biblioteca.Exclamar('O grupo de tributa��o estadual de compra para a empresa ' + sgGrupos.Cells[coNomeEmpresa, i] + ' n�o foi informado, verifique!');
      sgGrupos.Col := coGrupoTribEstadualCompra;
      sgGrupos.Row := i;
      SetarFoco(sgGrupos);
      Abort;
    end;

    if sgGrupos.Cells[coGrupoTribEstadualVendaId, i] = '' then begin
      _Biblioteca.Exclamar('O grupo de tributa��o estadual de venda para a empresa ' + sgGrupos.Cells[coNomeEmpresa, i] + ' n�o foi informado, verifique!');
      sgGrupos.Col := coGrupoTribEstadualVenda;
      sgGrupos.Row := i;
      SetarFoco(sgGrupos);
      Abort;
    end;

    if sgGrupos.Cells[coGrupoTribFederalCompraId, i] = '' then begin
      _Biblioteca.Exclamar('O grupo de tributa��o federal de compra para a empresa ' + sgGrupos.Cells[coNomeEmpresa, i] + ' n�o foi informado, verifique!');
      sgGrupos.Col := coGrupoTribFederalCompra;
      sgGrupos.Row := i;
      SetarFoco(sgGrupos);
      Abort;
    end;

    if sgGrupos.Cells[coGrupoTribFederalVendaId, i] = '' then begin
      _Biblioteca.Exclamar('O grupo de tributa��o federal de venda para a empresa ' + sgGrupos.Cells[coNomeEmpresa, i] + ' n�o foi informado, verifique!');
      sgGrupos.Col := coGrupoTribFederalVenda;
      sgGrupos.Row := i;
      SetarFoco(sgGrupos);
      Abort;
    end;
  end;
end;

end.
