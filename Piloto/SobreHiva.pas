unit SobreHiva;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPrincipal, Vcl.Imaging.pngimage, _Sessao,
  Vcl.ExtCtrls, Vcl.StdCtrls,Winapi.ShellAPI;

type
  TFormSobreHiva = class(TFormHerancaPrincipal)
    grpGbxSuporte: TGroupBox;
    lblNumTel: TLabel;
    lblLblFone: TLabel;
    lblLblCel: TLabel;
    lblEmail1: TLabel;
    lblemail: TLabel;
    lblcel2: TLabel;
    lblCopyright: TLabel;
    lblVersao: TLabel;
    lblProdLinc: TLabel;
    lblLicensa: TLabel;
    lblEspaco: TLabel;
    lblLicenciado: TLabel;
    imgAltis: TImage;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    lbl2: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    procedure lblEndSiteClick(Sender: TObject);
    procedure lblEndSiteMouseEnter(Sender: TObject);
    procedure lblEndSiteMouseLeave(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure AbrirSobre;

implementation

{$R *.dfm}

//-------------------(Public)--------------------------------------------------

procedure AbrirSobre;
var
  vForm: TFormSobreHiva;
begin
  vForm := TFormSobreHiva.Create(nil);
  vForm.lblVersao.Caption := 'Vers�o: ' + _Sessao.Sessao.getVersaoSistema;
  vForm.lblLicenciado.Caption:= Sessao.getEmpresaLogada.RazaoSocial;
  vForm.lblLicensa.Caption := 'Licen�a V�lida at�: ' + 'Data de Validade';
  vForm.lblCopyright.Caption := 'Copyright � 2016-' + FormatDateTime('yyyy',now) + '.';

  vForm.ShowModal;
  vForm.Free;
end;

procedure TFormSobreHiva.lblEndSiteClick(Sender: TObject);
begin
  inherited;
  ShellExecute(Application.Handle, nil, PChar('http://www.hivasolucoes.com.br'), nil, nil, SW_SHOWNORMAL);
end;

procedure TFormSobreHiva.lblEndSiteMouseEnter(Sender: TObject);
begin
  inherited;
  //LblEndSite.Font.Color := clNavy;
  //LblEndSite.Font.Size := 10;
 // LblEndSite.Top := LblSite.Top - 2;
end;

procedure TFormSobreHiva.lblEndSiteMouseLeave(Sender: TObject);
begin
  inherited;
  //LblEndSite.Font.Color := clBlue;
 // LblEndSite.Font.Size := 8;
 // LblEndSite.Top := LblSite.Top;
end;

end.
