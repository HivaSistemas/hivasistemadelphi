inherited FormDefinirEntregas: TFormDefinirEntregas
  Caption = 'Defini'#231#245'es de entrega'
  ClientHeight = 508
  ClientWidth = 994
  ExplicitWidth = 1000
  ExplicitHeight = 537
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 471
    Width = 994
    ExplicitTop = 471
    ExplicitWidth = 994
  end
  object sgItens: TGridLuka
    Left = 2
    Top = 2
    Width = 990
    Height = 467
    Align = alCustom
    ColCount = 8
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 3
    RowCount = 3
    FixedRows = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 1
    OnDrawCell = sgItensDrawCell
    OnSelectCell = sgItensSelectCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Produto'
      'Nome'
      'Qtde.'
      'Und.')
    OnGetCellColor = sgItensGetCellColor
    OnArrumarGrid = sgItensArrumarGrid
    Grid3D = False
    RealColCount = 8
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      54
      202
      53
      40
      23
      19
      25
      16)
  end
end
