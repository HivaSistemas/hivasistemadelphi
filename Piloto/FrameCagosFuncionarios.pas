unit FrameCagosFuncionarios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _RecordsCadastros,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Sessao, _RecordsEspeciais, _CargosFuncionario,
  PesquisaFuncoesFuncionario, System.Math, Vcl.Buttons, Vcl.Menus;

type
  TFrCargosFuncionarios = class(TFrameHenrancaPesquisas)
  public
    function getDados(pLinha: Integer = -1): RecCargosFuncionario;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrFuncoesFuncionarios }

function TFrCargosFuncionarios.AdicionarDireto: TObject;
var
  funcoes_funcionario: TArray<RecCargosFuncionario>;
begin
  funcoes_funcionario := _CargosFuncionario.BuscarCargosFuncionarios(Sessao.getConexaoBanco, 0, [FChaveDigitada] );
  if funcoes_funcionario = nil then
    Result := nil
  else
    Result := funcoes_funcionario[0];
end;

function TFrCargosFuncionarios.AdicionarPesquisando: TObject;
begin
  Result := PesquisaFuncoesFuncionario.PesquisarFuncaoFuncionario;
end;

function TFrCargosFuncionarios.getDados(pLinha: Integer): RecCargosFuncionario;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecCargosFuncionario(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrCargosFuncionarios.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecCargosFuncionario(FDados[i]).CargoId = RecCargosFuncionario(pSender).CargoId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrCargosFuncionarios.MontarGrid;
var
  i: Integer;
  funcao: RecCargosFuncionario;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      funcao := RecCargosFuncionario(FDados[i]);
      AAdd([IntToStr(funcao.CargoId), funcao.nome]);
    end;
  end;
end;

end.
