unit PesquisaAdministradorasCartoes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _Biblioteca, _Sessao,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _AdministradorasCartoes;

type
  TFormPesquisaAdministradorasCartoes = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  protected
    procedure BuscarRegistros; override;
  end;

function Pesquisar(pSomenteAtivos: Boolean): TObject;

implementation

{$R *.dfm}

{ TFormPesquisaAdministradorasCartoes }

const
  coDescricao   = 2;
  coCNPJ        = 3;
  coAtivo       = 4;

function Pesquisar(pSomenteAtivos: Boolean): TObject;
var
  vDado: TObject;
begin
  vDado := _HerancaPesquisas.Pesquisar(TFormPesquisaAdministradorasCartoes, _AdministradorasCartoes.GetFiltros, [pSomenteAtivos]);
  if vDado = nil then
    Result := nil
  else
    Result := RecAdministradorasCartoes(vDado);
end;

procedure TFormPesquisaAdministradorasCartoes.BuscarRegistros;
var
  i: Integer;
  vRotas: TArray<RecAdministradorasCartoes>;
begin
  inherited;

  vRotas :=
    _AdministradorasCartoes.BuscarAdministradorasCartoes(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text],
      FAuxiliares[0]
    );

  if vRotas = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vRotas);

  for i := Low(vRotas) to High(vRotas) do begin
    sgPesquisa.Cells[coSelecionado, i + 1]  := _Biblioteca.charNaoSelecionado;
    sgPesquisa.Cells[coCodigo, i + 1]       := NFormat(vRotas[i].AdministradoraId);
    sgPesquisa.Cells[coDescricao, i + 1]    := vRotas[i].Descricao;
    sgPesquisa.Cells[coCNPJ, i + 1]         := vRotas[i].Cnpj;
    sgPesquisa.Cells[coAtivo, i + 1]        := _Biblioteca.SimNao(vRotas[i].Ativo);
  end;
  sgPesquisa.SetLinhasGridPorTamanhoVetor( Length(vRotas) );
  SetarFoco(sgPesquisa);
end;

procedure TFormPesquisaAdministradorasCartoes.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coDescricao, coCNPJ] then
    vAlinhamento := taLeftJustify
  else if ACol = coAtivo then
    vAlinhamento := taCenter
  else
    vAlinhamento := taRightJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormPesquisaAdministradorasCartoes.sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coAtivo then begin
    AFont.Style := [fsBold];
    AFont.Color := _Biblioteca.AzulVermelho( sgPesquisa.Cells[coAtivo, ARow] );
  end;
end;

end.
