inherited FormCadastroEstados: TFormCadastroEstados
  Caption = 'Cadastro de estados'
  ClientHeight = 183
  ClientWidth = 495
  ExplicitWidth = 501
  ExplicitHeight = 212
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label1: TLabel
    Left = 125
    Width = 40
    Caption = 'C'#243'd. UF'
    ExplicitLeft = 125
    ExplicitWidth = 40
  end
  object Label2: TLabel [1]
    Left = 125
    Top = 49
    Width = 32
    Height = 14
    Caption = 'Nome'
  end
  object lb2: TLabel [2]
    Left = 125
    Top = 92
    Width = 65
    Height = 14
    Caption = 'C'#243'digo IBGE'
  end
  object lb1: TLabel [3]
    Left = 236
    Top = 92
    Width = 81
    Height = 14
    Caption = '% ICMS Interno'
  end
  inherited pnOpcoes: TPanel
    Height = 183
    ExplicitHeight = 183
    inherited sbGravar: TSpeedButton
      Top = 3
      ExplicitTop = 3
    end
    inherited sbDesfazer: TSpeedButton
      Top = 47
      ExplicitTop = 47
    end
    inherited sbExcluir: TSpeedButton
      Top = 91
      ExplicitTop = 91
    end
    inherited sbPesquisar: TSpeedButton
      Top = 135
      ExplicitTop = 135
    end
  end
  inherited eID: TEditLuka
    Left = 125
    Width = 65
    Alignment = taLeftJustify
    Anchors = [akLeft, akTop, akRight]
    MaxLength = 2
    TipoCampo = tcTexto
    ExplicitLeft = 125
    ExplicitWidth = 65
  end
  object eNome: TEditLuka [6]
    Left = 125
    Top = 64
    Width = 364
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 60
    TabOrder = 2
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 440
    TabOrder = 3
    OnKeyDown = ProximoCampo
    ExplicitLeft = 440
  end
  object eCodigoEstadoIBGE: TEditLuka
    Left = 125
    Top = 107
    Width = 107
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 2
    TabOrder = 4
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object ePercICMSInterno: TEditLuka
    Left = 236
    Top = 107
    Width = 93
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 5
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
end
