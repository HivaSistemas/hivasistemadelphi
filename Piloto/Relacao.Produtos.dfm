inherited FormRelacaoProdutos: TFormRelacaoProdutos
  Caption = 'Rela'#231#227'o de produtos'
  ClientHeight = 587
  OnShow = FormShow
  ExplicitTop = -180
  ExplicitHeight = 616
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Width = 125
    Height = 587
    ExplicitWidth = 125
    ExplicitHeight = 587
    inherited sbImprimir: TSpeedButton
      Visible = False
    end
    object SpeedButton1: TSpeedButton [2]
      Left = 4
      Top = 214
      Width = 110
      Height = 40
      BiDiMode = bdLeftToRight
      Caption = 'Gerar Planilha'
      Flat = True
      NumGlyphs = 2
      ParentBiDiMode = False
      OnClick = sbImprimirClick
    end
  end
  inherited pcDados: TPageControl
    Left = 125
    Width = 889
    Height = 587
    ExplicitLeft = 125
    ExplicitWidth = 889
    ExplicitHeight = 587
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 881
      ExplicitHeight = 558
      object Label1: TLabel
        Left = 330
        Top = 40
        Width = 154
        Height = 14
        Caption = 'Tipo de controle de estoque'
      end
      object Label2: TLabel
        Left = 330
        Top = 79
        Width = 26
        Height = 14
        Caption = 'Ativo'
      end
      object Label3: TLabel
        Left = 330
        Top = 120
        Width = 135
        Height = 14
        Caption = 'Aceitar estoque negativo'
      end
      object Label4: TLabel
        Left = 330
        Top = 162
        Width = 182
        Height = 14
        Caption = 'Inativar produto ao zerar estoque'
      end
      object Label5: TLabel
        Left = 330
        Top = 204
        Width = 137
        Height = 14
        Caption = 'Bloqueado para compras'
      end
      object Label6: TLabel
        Left = 328
        Top = 246
        Width = 130
        Height = 14
        Caption = 'Bloqueado para vendas'
      end
      object Label7: TLabel
        Left = 545
        Top = 245
        Width = 101
        Height = 14
        Caption = 'Permitir devolu'#231#227'o'
      end
      object Label9: TLabel
        Left = 545
        Top = 120
        Width = 87
        Height = 14
        Caption = 'Sob encomenda'
      end
      object Label10: TLabel
        Left = 545
        Top = 162
        Width = 209
        Height = 14
        Caption = 'Bloquear entrada sem ped. de compra'
      end
      object Label11: TLabel
        Left = 545
        Top = 204
        Width = 89
        Height = 14
        Caption = 'Exigir separa'#231#227'o'
      end
      object Label12: TLabel
        Left = 545
        Top = 40
        Width = 133
        Height = 14
        Caption = 'Exigir modelo nota fiscal'
      end
      object Label13: TLabel
        Left = 545
        Top = 79
        Width = 198
        Height = 14
        Caption = 'Separar somente com c'#243'd. de barras'
      end
      object Label14: TLabel
        Left = 545
        Top = 293
        Width = 119
        Height = 14
        Caption = 'Perc. comiss'#227'o a vista'
      end
      object Label17: TLabel
        Left = 545
        Top = 341
        Width = 123
        Height = 14
        Caption = 'Perc. comiss'#227'o a prazo'
      end
      object Label8: TLabel
        Left = 545
        Top = 389
        Width = 125
        Height = 14
        Caption = 'Produtos em promo'#231#227'o'
      end
      object Label18: TLabel
        Left = 545
        Top = 435
        Width = 73
        Height = 14
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alCustom
        Caption = 'Agrupamento'
      end
      inline FrDataCadastroProduto: TFrDataInicialFinal
        Left = 328
        Top = -1
        Width = 217
        Height = 42
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 5
        TabStop = True
        ExplicitLeft = 328
        ExplicitTop = -1
        ExplicitWidth = 217
        ExplicitHeight = 42
        inherited Label1: TLabel
          Width = 156
          Height = 14
          Caption = 'Data de cadastro do produto'
          ExplicitWidth = 156
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrDepartamentosSecoesLinhas: TFrameDepartamentosSecoesLinhas
        Left = 0
        Top = 86
        Width = 320
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitTop = 86
        ExplicitWidth = 320
        ExplicitHeight = 81
        inherited sgPesquisa: TGridLuka
          Width = 295
          Height = 64
          ExplicitWidth = 295
          ExplicitHeight = 64
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 102
            Caption = 'Grupo de produtos'
            ExplicitWidth = 102
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
            inherited ckSuprimir: TCheckBox
              Left = 1
              ExplicitLeft = 1
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          Height = 65
          ExplicitLeft = 295
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
      end
      object cbTipoControleEstoque: TComboBoxLuka
        Left = 330
        Top = 55
        Width = 185
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 6
        TabOrder = 6
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'Normal'
          'Lote'
          'Piso'
          'Grade'
          'Kit entr. desmembrada'
          'Kit entr. agrupada'
          'N'#227'o filtrar')
        Valores.Strings = (
          'N'
          'L'
          'P'
          'G'
          'K'
          'A'
          'NaoFiltrar')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      object cbAtivo: TComboBoxLuka
        Left = 330
        Top = 93
        Width = 185
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 2
        TabOrder = 7
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'Sim'
          'N'#227'o'
          'N'#227'o filtrar')
        Valores.Strings = (
          'S'
          'N'
          'NaoFiltrar')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      inline FrProdutos: TFrProdutos
        Left = 0
        Top = 0
        Width = 320
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitWidth = 320
        inherited sgPesquisa: TGridLuka
          Width = 295
          ExplicitWidth = 295
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 48
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
            inherited ckSuprimir: TCheckBox
              Left = 0
              ExplicitLeft = 0
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          ExplicitLeft = 295
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
        inherited ckFiltroExtra: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      object cbAceitarEstoqueNegativo: TComboBoxLuka
        Left = 330
        Top = 134
        Width = 185
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 8
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'N'#227'o filtrar'
          'Sim'
          'N'#227'o')
        Valores.Strings = (
          'NaoFiltrar'
          'S'
          'N')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      object cbInativarProdutoZerarEstoque: TComboBoxLuka
        Left = 330
        Top = 177
        Width = 185
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 9
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'N'#227'o filtrar'
          'Sim'
          'N'#227'o')
        Valores.Strings = (
          'NaoFiltrar'
          'S'
          'N')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      object cbBloqueadoCompra: TComboBoxLuka
        Left = 330
        Top = 218
        Width = 185
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 10
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'N'#227'o filtrar'
          'Sim'
          'N'#227'o')
        Valores.Strings = (
          'NaoFiltrar'
          'S'
          'N')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      object cbBloqueadoVenda: TComboBoxLuka
        Left = 328
        Top = 260
        Width = 185
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 11
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'N'#227'o filtrar'
          'Sim'
          'N'#227'o')
        Valores.Strings = (
          'NaoFiltrar'
          'S'
          'N')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      object cbPermitirDevolucao: TComboBoxLuka
        Left = 545
        Top = 257
        Width = 185
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 17
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'N'#227'o filtrar'
          'Sim'
          'N'#227'o')
        Valores.Strings = (
          'NaoFiltrar'
          'S'
          'N')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      object cbSobEncomenda: TComboBoxLuka
        Left = 545
        Top = 134
        Width = 185
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 14
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'N'#227'o filtrar'
          'Sim'
          'N'#227'o')
        Valores.Strings = (
          'NaoFiltrar'
          'S'
          'N')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      object cbBloquearEntradaSemPedidoCompra: TComboBoxLuka
        Left = 545
        Top = 177
        Width = 185
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 15
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'N'#227'o filtrar'
          'Sim'
          'N'#227'o')
        Valores.Strings = (
          'NaoFiltrar'
          'S'
          'N')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      object cbExigirSeparacao: TComboBoxLuka
        Left = 545
        Top = 218
        Width = 185
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 16
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'N'#227'o filtrar'
          'Sim'
          'N'#227'o')
        Valores.Strings = (
          'NaoFiltrar'
          'S'
          'N')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      object cbExigirModeloNotaFiscal: TComboBoxLuka
        Left = 545
        Top = 55
        Width = 185
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 12
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'N'#227'o filtrar'
          'Sim'
          'N'#227'o')
        Valores.Strings = (
          'NaoFiltrar'
          'S'
          'N')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      object cbSepararSomenteComCodigoBarras: TComboBoxLuka
        Left = 545
        Top = 93
        Width = 185
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 13
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'N'#227'o filtrar'
          'Sim'
          'N'#227'o')
        Valores.Strings = (
          'NaoFiltrar'
          'S'
          'N')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      inline FrMarca: TFrMarcas
        Left = 0
        Top = 173
        Width = 318
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        TabStop = True
        ExplicitTop = 173
        ExplicitWidth = 318
        inherited sgPesquisa: TGridLuka
          Width = 293
          TabOrder = 1
          ExplicitWidth = 293
        end
        inherited CkAspas: TCheckBox
          TabOrder = 2
        end
        inherited CkFiltroDuplo: TCheckBox
          TabOrder = 4
        end
        inherited CkPesquisaNumerica: TCheckBox
          TabOrder = 5
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
          TabOrder = 3
        end
        inherited PnTitulos: TPanel
          Width = 318
          TabOrder = 0
          ExplicitWidth = 318
          inherited lbNomePesquisa: TLabel
            Width = 33
            Caption = 'Marca'
            ExplicitWidth = 33
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 213
            ExplicitLeft = 213
            inherited ckSuprimir: TCheckBox
              Left = 2
              ExplicitLeft = 2
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 293
          ExplicitLeft = 293
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
      end
      inline FrFabricante: TFrFornecedores
        Left = 0
        Top = 259
        Width = 315
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        TabStop = True
        ExplicitTop = 259
        ExplicitWidth = 315
        inherited sgPesquisa: TGridLuka
          Width = 290
          TabOrder = 1
          ExplicitWidth = 290
        end
        inherited CkAspas: TCheckBox
          TabOrder = 2
        end
        inherited CkFiltroDuplo: TCheckBox
          TabOrder = 4
        end
        inherited CkPesquisaNumerica: TCheckBox
          TabOrder = 5
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
          TabOrder = 3
        end
        inherited PnTitulos: TPanel
          Width = 315
          TabOrder = 0
          ExplicitWidth = 315
          inherited lbNomePesquisa: TLabel
            Width = 58
            Caption = 'Fabricante'
            ExplicitWidth = 58
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 210
            ExplicitLeft = 210
          end
        end
        inherited pnPesquisa: TPanel
          Left = 290
          ExplicitLeft = 290
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
      end
      inline FrUnidadeVenda: TFrUnidades
        Left = 0
        Top = 347
        Width = 316
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 4
        TabStop = True
        ExplicitTop = 347
        ExplicitWidth = 316
        inherited sgPesquisa: TGridLuka
          Width = 291
          TabOrder = 1
          ExplicitWidth = 291
        end
        inherited CkAspas: TCheckBox
          TabOrder = 2
        end
        inherited CkFiltroDuplo: TCheckBox
          TabOrder = 4
        end
        inherited CkPesquisaNumerica: TCheckBox
          TabOrder = 5
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
          TabOrder = 3
        end
        inherited PnTitulos: TPanel
          Width = 316
          TabOrder = 0
          ExplicitWidth = 316
          inherited lbNomePesquisa: TLabel
            Width = 100
            Caption = 'Unidade de venda'
            ExplicitWidth = 100
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 211
            ExplicitLeft = 211
            inherited ckSuprimir: TCheckBox
              Left = 4
              ExplicitLeft = 4
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 291
          ExplicitLeft = 291
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
      end
      inline frNCM: TFrTextos
        Left = 328
        Top = 304
        Width = 187
        Height = 72
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 18
        TabStop = True
        ExplicitLeft = 328
        ExplicitTop = 304
        ExplicitWidth = 187
        inherited sgTextos: TGridLuka
          Width = 187
          ExplicitWidth = 187
        end
        inherited pnDescricao: TPanel
          Width = 187
          Caption = 'C'#243'digo NCM'
          ExplicitWidth = 187
        end
      end
      inline frCest: TFrTextos
        Left = 328
        Top = 384
        Width = 187
        Height = 72
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 19
        TabStop = True
        ExplicitLeft = 328
        ExplicitTop = 384
        ExplicitWidth = 187
        inherited sgTextos: TGridLuka
          Width = 187
          ExplicitWidth = 187
        end
        inherited pnDescricao: TPanel
          Width = 187
          Caption = 'CEST'
          ExplicitWidth = 187
        end
      end
      object pnComissaoVista: TPanel
        Left = 692
        Top = 291
        Width = 185
        Height = 41
        BevelOuter = bvNone
        TabOrder = 20
        Visible = False
        object lb1: TLabel
          Left = 0
          Top = 3
          Width = 60
          Height = 14
          Caption = 'Valor entre'
        end
        object lb2: TLabel
          Left = 79
          Top = 21
          Width = 7
          Height = 14
          Caption = 'e'
        end
        object eComissaoVista1: TEditLuka
          Left = 0
          Top = 17
          Width = 76
          Height = 22
          Alignment = taRightJustify
          CharCase = ecUpperCase
          TabOrder = 0
          Text = '0,00'
          TipoCampo = tcNumerico
          ApenasPositivo = False
          AsInt = 0
          CasasDecimais = 2
          NaoAceitarEspaco = False
        end
        object eComissaoVista2: TEditLuka
          Left = 92
          Top = 17
          Width = 76
          Height = 22
          Alignment = taRightJustify
          CharCase = ecUpperCase
          TabOrder = 1
          Text = '0,00'
          TipoCampo = tcNumerico
          ApenasPositivo = False
          AsInt = 0
          CasasDecimais = 2
          NaoAceitarEspaco = False
        end
      end
      object cbComissaoVista: TComboBoxLuka
        Left = 545
        Top = 308
        Width = 140
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 3
        TabOrder = 21
        Text = 'N'#227'o filtrar'
        OnChange = cbComissaoVistaChange
        Items.Strings = (
          'Entre'
          'Maior que'
          'Menor que'
          'N'#227'o filtrar')
        Valores.Strings = (
          'EN'
          'MA'
          'ME'
          'NaoFiltrar')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      object pnComissaoPrazo: TPanel
        Left = 692
        Top = 339
        Width = 185
        Height = 41
        BevelOuter = bvNone
        TabOrder = 22
        Visible = False
        object Label15: TLabel
          Left = 0
          Top = 3
          Width = 60
          Height = 14
          Caption = 'Valor entre'
        end
        object Label16: TLabel
          Left = 79
          Top = 21
          Width = 7
          Height = 14
          Caption = 'e'
        end
        object eComissaoPrazo1: TEditLuka
          Left = 0
          Top = 17
          Width = 76
          Height = 22
          Alignment = taRightJustify
          CharCase = ecUpperCase
          TabOrder = 0
          Text = '0,00'
          TipoCampo = tcNumerico
          ApenasPositivo = False
          AsInt = 0
          CasasDecimais = 2
          NaoAceitarEspaco = False
        end
        object eComissaoPrazo2: TEditLuka
          Left = 92
          Top = 17
          Width = 76
          Height = 22
          Alignment = taRightJustify
          CharCase = ecUpperCase
          TabOrder = 1
          Text = '0,00'
          TipoCampo = tcNumerico
          ApenasPositivo = False
          AsInt = 0
          CasasDecimais = 2
          NaoAceitarEspaco = False
        end
      end
      object cbComissaoPrazo: TComboBoxLuka
        Left = 545
        Top = 356
        Width = 140
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 3
        TabOrder = 23
        Text = 'N'#227'o filtrar'
        OnChange = cbComissaoPrazoChange
        Items.Strings = (
          'Entre'
          'Maior que'
          'Menor que'
          'N'#227'o filtrar')
        Valores.Strings = (
          'EN'
          'MA'
          'ME'
          'NaoFiltrar')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      object cbEmPromocao: TComboBoxLuka
        Left = 545
        Top = 404
        Width = 140
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 2
        TabOrder = 24
        Text = 'N'#227'o filtrar'
        OnChange = cbComissaoPrazoChange
        Items.Strings = (
          'Sim'
          'N'#227'o'
          'N'#227'o filtrar')
        Valores.Strings = (
          'S'
          'N'
          'NaoFiltrar')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      inline FrGrupoTributacoesEstadualVenda: TFrGruposTributacoesEstadualVenda
        Left = 0
        Top = 434
        Width = 316
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 25
        TabStop = True
        ExplicitTop = 434
        ExplicitWidth = 316
        inherited sgPesquisa: TGridLuka
          Width = 291
          ExplicitWidth = 291
        end
        inherited PnTitulos: TPanel
          Width = 316
          ExplicitWidth = 316
          inherited lbNomePesquisa: TLabel
            Width = 186
            ExplicitWidth = 186
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 211
            ExplicitLeft = 211
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 291
          ExplicitLeft = 291
        end
      end
      object cbAgrupamento: TComboBoxLuka
        Left = 545
        Top = 450
        Width = 201
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 26
        Text = 'Nenhum'
        Items.Strings = (
          'Nenhum'
          'C'#243'digo NCM'
          'Marca'
          'Grupo de tributa'#231#227'o'
          'Grupo de produtos')
        Valores.Strings = (
          'NEN'
          'NCM'
          'MAR'
          'TRI'
          'GPR')
        AsInt = 0
        AsString = 'NEN'
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 881
      ExplicitHeight = 558
      object sgItens: TGridLuka
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 875
        Height = 376
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 16
        Ctl3D = True
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 3
        RowCount = 2
        GradientEndColor = 15395562
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goThumbTracking]
        ParentCtl3D = False
        PopupMenu = pmOpcoes
        TabOrder = 0
        OnDblClick = sgItensDblClick
        OnDrawCell = sgItensDrawCell
        OnKeyDown = sgItensKeyDown
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Sel.?'
          'Produto'
          'Nome'
          'Nome compra'
          'Marca'
          'M'#250'lt. venda'
          'Und. venda'
          'Ativo'
          'Tipo cont. est.'
          'NCM'
          'Cest'
          'C'#243'd. barras'
          'C'#243'd. original'
          'Peso'
          '% Comiss'#227'o a vista'
          '% Comiss'#227'o a prazo')
        OnGetCellColor = sgItensGetCellColor
        OnGetCellPicture = sgItensGetCellPicture
        Grid3D = False
        RealColCount = 16
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          32
          55
          170
          152
          115
          74
          71
          37
          99
          77
          64
          110
          106
          64
          115
          119)
      end
      object Panel1: TPanel
        Left = 0
        Top = 382
        Width = 881
        Height = 176
        Align = alBottom
        BevelKind = bkTile
        BevelOuter = bvNone
        Color = clWhite
        ParentBackground = False
        TabOrder = 1
        object miMarcarDesmarcarSelecionado: TSpeedButton
          Left = 3
          Top = 8
          Width = 189
          Height = 25
          BiDiMode = bdLeftToRight
          Caption = 'Selcionar (Espa'#231'o)'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = miMarcarDesmarcarSelecionadoClick
        end
        object miMarcarDesmarcarTodos: TSpeedButton
          Left = 3
          Top = 39
          Width = 189
          Height = 25
          BiDiMode = bdLeftToRight
          Caption = 'Selecionar todos ( CTRL + Espa'#231'o)'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = miMarcarDesmarcarTodosClick
        end
        object miAlterarOrdemLocaisEntrega: TSpeedButton
          Left = 3
          Top = 70
          Width = 189
          Height = 25
          BiDiMode = bdLeftToRight
          Caption = 'Definir locais de produtos'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = miAlterarOrdemLocaisEntregaClick
        end
        object miPermitirNaoPermitirEstoqueNegativo: TSpeedButton
          Left = 206
          Top = 8
          Width = 207
          Height = 25
          BiDiMode = bdLeftToRight
          Caption = 'Definir estoque negativo (S / N)'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = miPermitirNaoPermitirEstoqueNegativoClick
        end
        object miAtivarDesativar: TSpeedButton
          Left = 206
          Top = 39
          Width = 207
          Height = 25
          BiDiMode = bdLeftToRight
          Caption = 'Definir produto ativo (S / N)'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = miAtivarDesativarClick
        end
        object miExigirSeparacao: TSpeedButton
          Left = 206
          Top = 70
          Width = 207
          Height = 25
          BiDiMode = bdLeftToRight
          Caption = 'Exirgir separa'#231#227'o de produtos (S / N)'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = miExigirSeparacaoClick
        end
        object miSepararSomenteCodigoBarras: TSpeedButton
          Left = 425
          Top = 8
          Width = 221
          Height = 25
          BiDiMode = bdLeftToRight
          Caption = 'Seperar soment. c/ c'#243'd. de barras (S / N)'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = miSepararSomenteCodigoBarrasClick
        end
        object miPermitirDevolucao: TSpeedButton
          Left = 425
          Top = 39
          Width = 218
          Height = 25
          BiDiMode = bdLeftToRight
          Caption = 'Aceita devolu'#231#227'o (S / N)'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = miPermitirDevolucaoClick
        end
        object miBloqueadoVendas: TSpeedButton
          Left = 425
          Top = 70
          Width = 218
          Height = 25
          BiDiMode = bdLeftToRight
          Caption = 'Bloquear produto p/ venda (S / N)'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = miBloqueadoVendasClick
        end
        object sbDefinirMarcaProdutos: TSpeedButton
          Left = 3
          Top = 102
          Width = 189
          Height = 25
          BiDiMode = bdLeftToRight
          Caption = 'Definir marca de produtos'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = sbDefinirMarcaProdutosClick
        end
        object sbDefinirUnidadeVenda: TSpeedButton
          Left = 206
          Top = 101
          Width = 207
          Height = 25
          BiDiMode = bdLeftToRight
          Caption = 'Definir unidade de venda'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = sbDefinirUnidadeVendaClick
        end
        object miDefinirGrupoProdutos: TSpeedButton
          Left = 425
          Top = 101
          Width = 218
          Height = 25
          BiDiMode = bdLeftToRight
          Caption = 'Definir grupo de produtos'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = miDefinirGrupoProdutosClick
        end
        object miDefinirEnderecosEstoque: TSpeedButton
          Left = 3
          Top = 133
          Width = 189
          Height = 25
          BiDiMode = bdLeftToRight
          Caption = 'Definir endere'#231'os de estoque'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = miDefinirEnderecosEstoqueClick
        end
        object sbZerarCustoVenda: TSpeedButton
          Left = 206
          Top = 132
          Width = 207
          Height = 25
          BiDiMode = bdLeftToRight
          Caption = 'Zerar % custo de venda total (S / N)'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = sbZerarCustoVendaClick
        end
        object SpeedButton3: TSpeedButton
          Left = 425
          Top = 134
          Width = 218
          Height = 23
          BiDiMode = bdLeftToRight
          Caption = 'Definir % custo de venda'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TSpeedButton
          Left = 662
          Top = 8
          Width = 207
          Height = 25
          BiDiMode = bdLeftToRight
          Caption = 'Definir % comiss'#227'o a vista'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = SpeedButton2Click
        end
        object SpeedButton4: TSpeedButton
          Left = 662
          Top = 39
          Width = 207
          Height = 25
          BiDiMode = bdLeftToRight
          Caption = 'Definir % comiss'#227'o a prazo'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = SpeedButton4Click
        end
        object SpeedButton5: TSpeedButton
          Left = 662
          Top = 70
          Width = 207
          Height = 25
          BiDiMode = bdLeftToRight
          Caption = 'Definir tributa'#231#227'o'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = SpeedButton5Click
        end
        object SpeedButton6: TSpeedButton
          Left = 662
          Top = 102
          Width = 207
          Height = 25
          BiDiMode = bdLeftToRight
          Caption = 'Definir CEST de produtos'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = SpeedButton6Click
        end
        object SpeedButton7: TSpeedButton
          Left = 662
          Top = 133
          Width = 207
          Height = 25
          BiDiMode = bdLeftToRight
          Caption = 'Outras op'#231#245'es'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = SpeedButton7Click
        end
      end
    end
  end
  object pmOpcoes: TPopupMenu
    Left = 472
    Top = 280
    object miN1: TMenuItem
      Caption = '-'
    end
    object miBloqueadoCompras: TMenuItem
      Caption = 'Bloquear/desbloquear para compras'
      Visible = False
      OnClick = miBloqueadoComprasClick
    end
    object miSobEncomenda: TMenuItem
      Caption = 'Sob encomenda'
      Visible = False
      OnClick = miSobEncomendaClick
    end
    object miBloquearEntradaSemPedidoCompra: TMenuItem
      Caption = 'Bloquear/desbloquear entrada sem ped. de compra'
      Visible = False
      OnClick = miBloquearEntradaSemPedidoCompraClick
    end
    object miExigirModeloNota: TMenuItem
      Caption = 'Exigir/n'#227'o exigir modelo nota fiscal'
      Visible = False
      OnClick = miExigirModeloNotaClick
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 610
    Top = 153
    object teste1: TMenuItem
      Caption = 'Sim'
      OnClick = teste1Click
    end
    object No1: TMenuItem
      Caption = 'N'#227'o'
      OnClick = No1Click
    end
  end
  object PopupMenu2: TPopupMenu
    Left = 286
    Top = 211
    object MenuItem1: TMenuItem
      Caption = 'Definir NCM de produtos'
      OnClick = MenuItem1Click
    end
    object MenuItem2: TMenuItem
      Caption = 'Definir unidade de entrega'
      OnClick = MenuItem2Click
    end
    object Limparunidadedeentrega1: TMenuItem
      Caption = 'Limpar unidade de entrega'
      OnClick = Limparunidadedeentrega1Click
    end
  end
end
