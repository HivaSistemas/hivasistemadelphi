unit ImpressaoComprovanteCreditoPagarGrafico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosGraficos, QRExport, _Sessao, _ContasPagar,
  QRPDFFilt, QRWebFilt, QRCtrls, QuickRpt, Vcl.ExtCtrls, _Biblioteca, _RecordsFinanceiros,
  _ContasReceberBaixas;

type
  TFormImpressaoComprovanteCreditoPagarGrafico = class(TFormHerancaRelatoriosGraficos)
    qrl1: TQRLabel;
    qrlNFFormasPagamento: TQRMemo;
    qrlNFCodigoFinanceiro: TQRLabel;
    qrl3: TQRLabel;
    qrlNFCodigoBaixaOrigem: TQRLabel;
    qrl5: TQRLabel;
    qrlNFCliente: TQRLabel;
    qrlNFValorCredito: TQRLabel;
    qrl8: TQRLabel;
    QRShape2: TQRShape;
    qrl9: TQRLabel;
    QRShape1: TQRShape;
    qrlNFCabecalhoFormasPagto: TQRLabel;
    QRLabel1: TQRLabel;
    QRShape3: TQRShape;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    qrNFCodigoFinanceiro: TQRLabel;
    QRLabel4: TQRLabel;
    qrNFCodigoBaixaOrigem: TQRLabel;
    QRLabel6: TQRLabel;
    qrNFCliente: TQRLabel;
    qrNFValorCredito: TQRLabel;
    qrNFCabecalhoFormasPagto: TQRLabel;
    qrNFFormasPagamento: TQRMemo;
    QRShape5: TQRShape;
    QRLabel11: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure Imprimir( pFinanceiroId: Integer );

implementation

{$R *.dfm}

procedure Imprimir( pFinanceiroId: Integer );
var
  vForm: TFormImpressaoComprovanteCreditoPagarGrafico;

  vPagar: TArray<RecContaPagar>;
  vBaixa: TArray<RecContaReceberBaixa>;

  vImpressora: RecImpressora;

  procedure AddFormaPagamento( pValor: Currency; pDescricao: string );
  begin
    if pValor = 0 then
      Exit;

    vForm.qrlNFFormasPagamento.Lines.Add(pDescricao + ' R$ ' + NFormat(pValor) );
    vForm.qrNFFormasPagamento.Lines.Add(pDescricao + ' R$ ' + NFormat(pValor) );
  end;

begin
  if pFinanceiroId = 0 then
    Exit;

  vImpressora := Sessao.getImpressora( toComprovantePagtoFinanceiro );
  if vImpressora.CaminhoImpressora = '' then begin
    _Biblioteca.ImpressoraNaoConfigurada;
    Exit;
  end;

  vPagar := _ContasPagar.BuscarContasPagar( Sessao.getConexaoBanco, 0, [pFinanceiroId] );
  if vPagar = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  if not Em(vPagar[0].cobranca_id, [Sessao.getParametros.TipoCobrancaGeracaoCredId, Sessao.getParametros.TipoCobrancaGerCredImpId]) then begin
    _Biblioteca.Exclamar('O t�tulo selecionado n�o � um cr�dito!');
    Exit;
  end;

  vBaixa := nil;
  if vPagar[0].BaixaReceberOrigemId > 0 then
    vBaixa := _ContasReceberBaixas.BuscarContaReceberBaixas( Sessao.getConexaoBanco, 0, [vPagar[0].BaixaReceberOrigemId] );

  vForm := TFormImpressaoComprovanteCreditoPagarGrafico.Create( nil, vImpressora );
  vForm.PreencherCabecalho( vPagar[0].empresa_id, False );

  //n�o fiscal
  vForm.qrlNFValorCredito.Caption      := 'Valor do cr�dito: R$ ' + NFormat(vPagar[0].ValorDocumento);
  vForm.qrlNFCodigoFinanceiro.Caption  := NFormat( vPagar[0].PagarId );
  vForm.qrlNFCodigoBaixaOrigem.Caption := NFormatN( vPagar[0].baixa_origem_id );
  vForm.qrlNFCliente.Caption           := NFormat( vPagar[0].CadastroId ) + ' - ' + vPagar[0].nome_fornecedor;

  vForm.qrlNFFormasPagamento.Lines.Clear;
  if (vPagar[0].BaixaReceberOrigemId > 0) then begin
    vForm.qrlNFCabecalhoFormasPagto.Enabled := True;
    AddFormaPagamento(vBaixa[0].valor_dinheiro, 'Dinheiro.: ');
    AddFormaPagamento(vBaixa[0].valor_cheque,   'Cheque...: ');
    AddFormaPagamento(vBaixa[0].ValorCartaoDebito,   'Cart�o d�bito...: ');
    AddFormaPagamento(vBaixa[0].ValorCartaoCredito,   'Cart�o cr�dito...: ');
    AddFormaPagamento(vBaixa[0].valor_cobranca, 'Cobran�a.: ');
    AddFormaPagamento(vBaixa[0].valor_pix, 'Pix......: ');
  end;

  //gr�fica
  vForm.qrNFValorCredito.Caption      := 'Valor do cr�dito: R$ ' + NFormat(vPagar[0].ValorDocumento);
  vForm.qrNFCodigoFinanceiro.Caption  := NFormat( vPagar[0].PagarId );
  vForm.qrNFCodigoBaixaOrigem.Caption := NFormatN( vPagar[0].BaixaReceberOrigemId );
  vForm.qrNFCliente.Caption           := NFormat( vPagar[0].CadastroId ) + ' - ' + vPagar[0].nome_fornecedor;

  vForm.qrNFFormasPagamento.Lines.Clear;
  if vPagar[0].BaixaReceberOrigemId > 0 then begin
    vForm.qrNFCabecalhoFormasPagto.Enabled := True;
    AddFormaPagamento(vBaixa[0].valor_dinheiro, 'Dinheiro.: ');
    AddFormaPagamento(vBaixa[0].valor_cheque,   'Cheque...: ');
    AddFormaPagamento(vBaixa[0].ValorCartaoDebito,   'Cart�o d�bito...: ');
    AddFormaPagamento(vBaixa[0].ValorCartaoCredito,   'Cart�o cr�dito...: ');
    AddFormaPagamento(vBaixa[0].valor_pix, 'Pix......: ');
  end;

  vForm.Imprimir;

  vForm.Free;
end;

end.
