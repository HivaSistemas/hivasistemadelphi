unit Frame.TipoBloqueioEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Menus,
  Vcl.Buttons, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _RecordsCadastros,
  _Sessao, _Biblioteca, _TipoBloqueioEstoque, Pesquisa.TipoBloqueioEstoque, Math;

type
  TFrTipoBloqueioEstoque = class(TFrameHenrancaPesquisas)
  public
    function GetTipoBloqueioEstoque(pLinha: Integer = -1): RecTipoBloqueioEstoque;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrameTipoBloqueioEstoque }

function TFrTipoBloqueioEstoque.AdicionarDireto: TObject;
var
  vMotivos: TArray<RecTipoBloqueioEstoque>;
begin
  vMotivos := _TipoBloqueioEstoque.BuscarTipoBloqueioEstoque(Sessao.getConexaoBanco, 0, [FChaveDigitada]);
  if vMotivos = nil then
    Result := nil
  else
    Result := vMotivos[0];
end;

function TFrTipoBloqueioEstoque.AdicionarPesquisando: TObject;
begin
  Result := Pesquisa.TipoBloqueioEstoque.Pesquisar();
end;

function TFrTipoBloqueioEstoque.GetTipoBloqueioEstoque(pLinha: Integer): RecTipoBloqueioEstoque;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecTipoBloqueioEstoque(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrTipoBloqueioEstoque.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecTipoBloqueioEstoque(FDados[i]).tipo_bloqueio_id = RecTipoBloqueioEstoque(pSender).tipo_bloqueio_id then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrTipoBloqueioEstoque.MontarGrid;
var
  i: Integer;
  vSender: RecTipoBloqueioEstoque;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecTipoBloqueioEstoque(FDados[i]);
      AAdd([IntToStr(vSender.tipo_bloqueio_id), vSender.descricao]);
    end;
  end;
end;

end.
