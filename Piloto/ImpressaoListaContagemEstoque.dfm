inherited FormImpressaoListaContagemEstoque: TFormImpressaoListaContagemEstoque
  Caption = 'FormImpressaoListaContagemEstoque'
  ExplicitWidth = 1030
  ExplicitHeight = 754
  PixelsPerInch = 96
  TextHeight = 13
  inherited qrRelatorioNF: TQuickRep
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Page.Values = (
      0.000000000000000000
      509.300000000000000000
      0.000000000000000000
      750.100000000000000000
      0.000000000000000000
      0.000000000000000000
      0.000000000000000000)
    inherited qrBandTitulo: TQRBand
      Size.Values = (
        289.388020833333300000
        750.755208333333300000)
      inherited qrlNFNomeEmpresa: TQRLabel
        Size.Values = (
          34.726562500000000000
          6.614583333333332000
          16.536458333333330000
          744.140625000000000000)
        FontSize = 6
      end
      inherited qrNFCnpj: TQRLabel
        Top = 31
        Size.Values = (
          33.072916666666670000
          6.614583333333332000
          51.263020833333340000
          744.140625000000000000)
        FontSize = 7
        ExplicitTop = 31
      end
      inherited qrNFEndereco: TQRLabel
        Size.Values = (
          34.726562500000000000
          6.614583333333332000
          85.989583333333340000
          744.140625000000000000)
        FontSize = 7
      end
      inherited qrNFCidadeUf: TQRLabel
        Size.Values = (
          42.994791666666670000
          6.614583333333332000
          122.369791666666700000
          744.140625000000000000)
        FontSize = 7
      end
      inherited qrNFTipoDocumento: TQRLabel
        Size.Values = (
          33.072916666666670000
          1.653645833333333000
          211.666666666666700000
          719.335937500000000000)
        FontSize = 7
      end
      inherited qrSeparador2: TQRShape
        Size.Values = (
          3.307291666666666000
          3.307291666666666000
          281.119791666666700000
          724.296875000000000000)
      end
      inherited qrNFTitulo: TQRLabel
        Size.Values = (
          33.072916666666670000
          1.653645833333333000
          246.393229166666700000
          719.335937500000000000)
        FontSize = 7
      end
      inherited qrlNFEmitidoEm: TQRLabel
        Size.Values = (
          23.151041666666670000
          370.416666666666700000
          3.307291666666666000
          357.187500000000000000)
        FontSize = -6
      end
      inherited qrlNFTelefoneEmpresa: TQRLabel
        Top = 101
        Size.Values = (
          42.994791666666670000
          6.614583333333332000
          167.018229166666700000
          744.140625000000000000)
        FontSize = 7
        ExplicitTop = 101
      end
    end
    inherited PageFooterBand1: TQRBand
      Size.Values = (
        24.804687500000000000
        750.755208333333300000)
      inherited qrlNFSistema: TQRLabel
        Size.Values = (
          23.151041666666670000
          582.083333333333400000
          0.000000000000000000
          138.906250000000000000)
        FontSize = -6
      end
    end
  end
  inherited qrRelatorioA4: TQuickRep
    Left = 43
    Top = 0
    BeforePrint = qrRelatorioA4BeforePrint
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    OnNeedData = qrRelatorioA4NeedData
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    ExplicitLeft = 43
    ExplicitTop = 0
    inherited qrCabecalho: TQRBand
      Height = 116
      Size.Values = (
        306.916666666666700000
        1899.708333333333000000)
      ExplicitHeight = 116
      inherited qrCaptionEndereco: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          44.979166666666670000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrEmpresa: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          7.937500000000000000
          568.854166666666700000)
        FontSize = 7
      end
      inherited qrEndereco: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          44.979166666666670000
          1513.416666666667000000)
        FontSize = 7
      end
      inherited qrLogoEmpresa: TQRImage
        Size.Values = (
          211.666666666666700000
          13.229166666666670000
          2.645833333333333000
          211.666666666666700000)
      end
      inherited qrCaptionEmpresa: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          7.937500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrEmitidoEm: TQRLabel
        Size.Values = (
          29.104166666666670000
          1529.291666666667000000
          0.000000000000000000
          370.416666666666700000)
        FontSize = 5
      end
      inherited qr1: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          82.020833333333320000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrBairro: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          82.020833333333320000
          568.854166666666700000)
        FontSize = 7
      end
      inherited qr3: TQRLabel
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          82.020833333333320000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrCidadeUf: TQRLabel
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          82.020833333333320000
          568.854166666666700000)
        FontSize = 7
      end
      inherited qr5: TQRLabel
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          7.937500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrCNPJ: TQRLabel
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          7.937500000000000000
          248.708333333333300000)
        FontSize = 7
      end
      inherited qr7: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrTelefone: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          119.062500000000000000
          187.854166666666700000)
        FontSize = 7
      end
      inherited qrFax: TQRLabel
        Size.Values = (
          34.395833333333330000
          767.291666666666800000
          119.062500000000000000
          187.854166666666700000)
        FontSize = 7
      end
      inherited qr10: TQRLabel
        Size.Values = (
          34.395833333333330000
          603.250000000000000000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qr11: TQRLabel
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrEmail: TQRLabel
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          119.062500000000000000
          769.937500000000000000)
        FontSize = 7
      end
      inherited qr13: TQRLabel
        Left = 271
        Top = 85
        Width = 179
        Size.Values = (
          52.916666666666670000
          717.020833333333400000
          224.895833333333300000
          473.604166666666700000)
        Caption = 'LISTA DE CONTAGEM'
        FontSize = 12
        ExplicitLeft = 271
        ExplicitTop = 85
        ExplicitWidth = 179
      end
    end
    inherited qrbndRodape: TQRBand
      Top = 204
      Height = 46
      Size.Values = (
        121.708333333333300000
        1899.708333333333000000)
      ExplicitTop = 204
      ExplicitHeight = 46
      inherited qrUsuarioImpressao: TQRLabel
        Top = 29
        Size.Values = (
          31.750000000000000000
          7.937500000000000000
          76.729166666666680000
          354.541666666666700000)
        Caption = 'EZEQUIEL EUGENIO DO PRADO'
        FontSize = 7
        ExplicitTop = 29
      end
      inherited qrSistema: TQRLabel
        Top = 27
        Size.Values = (
          31.750000000000000000
          1529.291666666667000000
          71.437500000000000000
          370.416666666666700000)
        Caption = 'Hiva 3.0'
        FontSize = 7
        ExplicitTop = 27
      end
    end
    object qrbndColumnHeaderBand1: TQRBand
      Left = 38
      Top = 154
      Width = 718
      Height = 16
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        42.333333333333330000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbColumnHeader
      object qr23: TQRLabel
        Left = 3
        Top = 2
        Width = 29
        Height = 13
        Size.Values = (
          34.395833333333330000
          7.937500000000000000
          5.291666666666667000
          76.729166666666680000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Prod.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr24: TQRLabel
        Left = 38
        Top = 2
        Width = 283
        Height = 13
        Size.Values = (
          34.395833333333340000
          100.541666666666700000
          5.291666666666667000
          748.770833333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nome'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr25: TQRLabel
        Left = 395
        Top = 2
        Width = 38
        Height = 13
        Size.Values = (
          34.395833333333340000
          1045.104166666667000000
          5.291666666666667000
          100.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Marca'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qr27: TQRLabel
        Left = 436
        Top = 2
        Width = 31
        Height = 13
        Size.Values = (
          34.395833333333340000
          1153.583333333333000000
          5.291666666666667000
          82.020833333333340000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Und.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRLabel1: TQRLabel
        Left = 637
        Top = 2
        Width = 76
        Height = 13
        Size.Values = (
          34.395833333333330000
          1685.395833333333000000
          5.291666666666667000
          201.083333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Qtde.contada'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRLabel3: TQRLabel
        Left = 324
        Top = 2
        Width = 68
        Height = 13
        Size.Values = (
          34.395833333333340000
          857.250000000000000000
          5.291666666666667000
          179.916666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'C'#243'd.barras'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRLabel4: TQRLabel
        Left = 471
        Top = 2
        Width = 69
        Height = 13
        Size.Values = (
          34.395833333333340000
          1246.187500000000000000
          5.291666666666667000
          182.562500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'C'#243'd.original'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRLabel5: TQRLabel
        Left = 543
        Top = 2
        Width = 34
        Height = 13
        Size.Values = (
          34.395833333333340000
          1436.687500000000000000
          5.291666666666667000
          89.958333333333340000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Lote'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRLabel6: TQRLabel
        Left = 580
        Top = 2
        Width = 54
        Height = 13
        Size.Values = (
          34.395833333333330000
          1534.583333333333000000
          5.291666666666667000
          142.875000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Qdt. Fisico'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
    end
    object qrbndDetailBand1: TQRBand
      Left = 38
      Top = 170
      Width = 718
      Height = 34
      AlignToBottom = False
      BeforePrint = qrbndDetailBand1BeforePrint
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        89.958333333333330000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbDetail
      object qrProdutoId: TQRLabel
        Left = 3
        Top = 2
        Width = 29
        Height = 13
        Size.Values = (
          34.395833333333330000
          7.937500000000000000
          5.291666666666667000
          76.729166666666680000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Prod.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrNomeProduto: TQRLabel
        Left = 38
        Top = 2
        Width = 283
        Height = 13
        Size.Values = (
          34.395833333333340000
          100.541666666666700000
          5.291666666666667000
          748.770833333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nome'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrMarca: TQRLabel
        Left = 395
        Top = 2
        Width = 38
        Height = 13
        Size.Values = (
          34.395833333333340000
          1045.104166666667000000
          5.291666666666667000
          100.541666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Marca'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrUnidade: TQRLabel
        Left = 436
        Top = 2
        Width = 31
        Height = 13
        Size.Values = (
          34.395833333333340000
          1153.583333333333000000
          5.291666666666667000
          82.020833333333340000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Und.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRLabel2: TQRLabel
        Left = 637
        Top = 2
        Width = 76
        Height = 13
        Size.Values = (
          34.395833333333330000
          1685.395833333333000000
          5.291666666666667000
          201.083333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '________'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrCodigoBarras: TQRLabel
        Left = 324
        Top = 2
        Width = 68
        Height = 13
        Size.Values = (
          34.395833333333340000
          857.250000000000000000
          5.291666666666667000
          179.916666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = ''
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrCodigoOriginal: TQRLabel
        Left = 471
        Top = 2
        Width = 69
        Height = 13
        Size.Values = (
          34.395833333333340000
          1246.187500000000000000
          5.291666666666667000
          182.562500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = ''
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrLote: TQRLabel
        Left = 543
        Top = 2
        Width = 34
        Height = 13
        Size.Values = (
          34.395833333333340000
          1436.687500000000000000
          5.291666666666667000
          89.958333333333340000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '12-B2'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrEstoqueFisico: TQRLabel
        Left = 580
        Top = 2
        Width = 54
        Height = 13
        Size.Values = (
          34.395833333333330000
          1534.583333333333000000
          5.291666666666667000
          142.875000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '1050'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object QRLabel7: TQRLabel
        Left = 3
        Top = 18
        Width = 46
        Height = 13
        Size.Values = (
          34.395833333333330000
          7.937500000000000000
          47.625000000000000000
          121.708333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'End. est.:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrNomeEnderecoEstoque: TQRLabel
        Left = 54
        Top = 18
        Width = 611
        Height = 13
        Size.Values = (
          34.395833333333330000
          142.875000000000000000
          47.625000000000000000
          1616.604166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nome'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
    end
  end
end
