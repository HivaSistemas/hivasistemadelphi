inherited FormBuscarCentroCusto: TFormBuscarCentroCusto
  Caption = 'Buscar centro de custo'
  ClientHeight = 105
  ClientWidth = 411
  OnShow = FormShow
  ExplicitWidth = 417
  ExplicitHeight = 134
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 68
    Width = 411
  end
  inline FrCentrosCustos: TFrCentroCustos
    Left = 1
    Top = 2
    Width = 403
    Height = 60
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 1
    ExplicitTop = 2
    ExplicitWidth = 403
    ExplicitHeight = 60
    inherited sgPesquisa: TGridLuka
      Width = 378
      Height = 43
      ExplicitWidth = 378
      ExplicitHeight = 43
    end
    inherited PnTitulos: TPanel
      Width = 403
      ExplicitWidth = 403
      inherited lbNomePesquisa: TLabel
        Width = 90
        ExplicitWidth = 90
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 298
        ExplicitLeft = 298
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 378
      Height = 44
      ExplicitLeft = 378
      ExplicitHeight = 44
    end
  end
end
