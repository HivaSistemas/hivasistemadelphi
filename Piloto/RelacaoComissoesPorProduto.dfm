inherited FormRelacaoComissoesPorProduto: TFormRelacaoComissoesPorProduto
  Caption = 'Rela'#231#227'o de comiss'#245'es produto'
  ClientHeight = 596
  ClientWidth = 1002
  ExplicitWidth = 1008
  ExplicitHeight = 625
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 596
    ExplicitHeight = 596
    inherited sbImprimir: TSpeedButton
      Visible = False
    end
  end
  inherited pcDados: TPageControl
    Width = 880
    Height = 596
    ActivePage = tsResultado
    ExplicitWidth = 880
    ExplicitHeight = 596
    inherited tsFiltros: TTabSheet
      ExplicitWidth = 872
      ExplicitHeight = 567
      inline FrFuncionarios: TFrFuncionarios
        Left = 2
        Top = 175
        Width = 403
        Height = 97
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 175
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 71
            ExplicitWidth = 71
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
      inline FrDataCadastro: TFrDataInicialFinal
        Left = 424
        Top = 2
        Width = 217
        Height = 68
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 424
        ExplicitTop = 2
        ExplicitWidth = 217
        ExplicitHeight = 68
        inherited Label1: TLabel
          Width = 93
          Height = 14
          ExplicitWidth = 93
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrProdutos: TFrProdutos
        Left = 2
        Top = 88
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 88
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 48
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
        inherited ckFiltroExtra: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      inline FrEmpresas: TFrEmpresas
        Left = 2
        Top = 1
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 1
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 53
            ExplicitWidth = 53
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 872
      ExplicitHeight = 567
      object sp1: TSplitter
        Left = 0
        Top = 249
        Width = 872
        Height = 6
        Cursor = crVSplit
        Align = alTop
        ResizeStyle = rsUpdate
        ExplicitTop = 176
      end
      object sgProdutos: TGridLuka
        Left = 0
        Top = 271
        Width = 872
        Height = 296
        Align = alClient
        ColCount = 6
        DefaultRowHeight = 19
        DrawingStyle = gdsClassic
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        TabOrder = 0
        OnDrawCell = sgProdutosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Produto'
          'Nome'
          'Marca'
          'Base comiss'#227'o'
          'Perc. comiss'#227'o'
          'Valor comiss'#227'o')
        Grid3D = False
        RealColCount = 9
        AtivarPopUpSelecao = False
        ColWidths = (
          57
          289
          120
          88
          94
          91)
      end
      object st1: TStaticText
        Left = 0
        Top = 255
        Width = 872
        Height = 16
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Produtos da venda / devolu'#231#227'o'
        Color = clSilver
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        Transparent = False
      end
      object sgPedidos: TGridLuka
        Left = 0
        Top = 16
        Width = 872
        Height = 233
        Align = alTop
        ColCount = 9
        DefaultRowHeight = 19
        DrawingStyle = gdsClassic
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
        TabOrder = 2
        OnClick = sgPedidosClick
        OnDblClick = sgPedidosDblClick
        OnDrawCell = sgPedidosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Tipo'
          'Orc. / dev.'
          'Data rec. / dev.'
          'Cliente'
          'Valor venda'
          'Base comiss'#227'o'
          'Tipo comiss'#227'o'
          'Perc. comiss'#227'o'
          'Valor comiss'#227'o')
        OnGetCellColor = sgPedidosGetCellColor
        Grid3D = False
        RealColCount = 15
        OrdenarOnClick = True
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          51
          68
          94
          215
          106
          112
          89
          87
          114)
      end
      object st2: TStaticText
        Left = 0
        Top = 0
        Width = 872
        Height = 16
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Vendas / devolu'#231#245'es'
        Color = clSilver
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        Transparent = False
      end
    end
  end
end
