unit Informacoes.TituloPagar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.StdCtrls, _Biblioteca, _Sessao,
  Vcl.Mask, EditLukaData, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _ContasPagar, _RecordsFinanceiros,
  System.StrUtils, System.Math, InformacoesContasPagarBaixa, Vcl.ComCtrls, Informacoes.ContasReceberBaixa,
  StaticTextLuka, SpeedButtonLuka, MemoAltis, Vcl.Grids, GridLuka, _ContasPagarBaixas;

type
  TFormInformacoesTituloPagar = class(TFormHerancaFinalizar)
    pc1: TPageControl;
    tsPrincipais: TTabSheet;
    lb1: TLabel;
    lb2: TLabel;
    lb13: TLabel;
    lb12: TLabel;
    lb3: TLabel;
    lb5: TLabel;
    lb6: TLabel;
    lb10: TLabel;
    lb11: TLabel;
    lb14: TLabel;
    lb18: TLabel;
    lb22: TLabel;
    lb23: TLabel;
    ePagarId: TEditLuka;
    eFornecedor: TEditLuka;
    eEmpresa: TEditLuka;
    eUsuarioCadastro: TEditLuka;
    eTipoCobranca: TEditLuka;
    eValorTotalTitulo: TEditLuka;
    eDataCadastro: TEditLukaData;
    eDiasAtraso: TEditLuka;
    eDataVencimento: TEditLukaData;
    eDataVencimentoOriginal: TEditLukaData;
    eNumeroCheque: TEditLuka;
    eValorTitulo: TEditLuka;
    eValorDesconto: TEditLuka;
    tsOrigem: TTabSheet;
    lb19: TLabel;
    sbInformacoesBaixa: TSpeedButton;
    lb24: TLabel;
    lb25: TLabel;
    sbInformacoesOrigemBaixa: TSpeedButton;
    eBaixaId: TEditLuka;
    eNomeUsuarioBaixa: TEditLuka;
    eBaixaOrigemId: TEditLuka;
    lb9: TLabel;
    eEntradaId: TEditLuka;
    sbInfoEntradaId: TSpeedButton;
    eBaixaReceberOrigemId: TEditLuka;
    lb4: TLabel;
    sbBaixaReceberOrigemId: TSpeedButton;
    lb7: TLabel;
    eDataBaixa: TEditLukaData;
    eDataPagamento: TEditLukaData;
    lb8: TLabel;
    lb26: TLabel;
    eNomePortador: TEditLuka;
    lbl1: TLabel;
    ePlanoFinanceiro: TEditLuka;
    lbl2: TLabel;
    eCentroCustos: TEditLuka;
    StaticTextLuka4: TStaticTextLuka;
    txtBloqueado: TStaticText;
    txtStatus: TStaticText;
    StaticTextLuka2: TStaticTextLuka;
    sbLogs: TSpeedButtonLuka;
    lb15: TLabel;
    eDevolucaoVenda: TEditLuka;
    sbInformacoesDevolucaoVenda: TSpeedButton;
    eDataEmissao: TEditLukaData;
    lb16: TLabel;
    eDataContabil: TEditLukaData;
    lb17: TLabel;
    meObservacoes: TMemoAltis;
    lb20: TLabel;
    lb21: TLabel;
    eOrcamentoId: TEditLuka;
    sbInformacoesPedido: TSpeedButton;
    lb27: TLabel;
    eAcumuladoId: TEditLuka;
    sbInformacoesAcumulado: TSpeedButton;
    st1: TStaticTextLuka;
    stOrigem: TStaticText;
    st3: TStaticTextLuka;
    tsAdiantamentos: TTabSheet;
    sgAdiantamentos: TGridLuka;
    eValorAdiantado: TEditLuka;
    lb28: TLabel;
    procedure sbInformacoesOrigemBaixaClick(Sender: TObject);
    procedure sbInformacoesBaixaClick(Sender: TObject);
    procedure sbInfoEntradaIdClick(Sender: TObject);
    procedure sbBaixaReceberOrigemIdClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sbLogsClick(Sender: TObject);
    procedure sbInformacoesDevolucaoVendaClick(Sender: TObject);
    procedure sbInformacoesPedidoClick(Sender: TObject);
    procedure sbInformacoesAcumuladoClick(Sender: TObject);
    procedure sgAdiantamentosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgAdiantamentosDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure Informar(pPagarId: Integer);

implementation

{$R *.dfm}

uses
  InformacoesEntradaNotaFiscal, Logs, Informacoes.Devolucao, Informacoes.Orcamento, InformacoesAcumulado;

const
  coBaixaAdiantamentoId  = 0;
  coDataHoraAdiantamento = 1;
  coValorAdiantamento    = 2;
  coUsuarioAdiantamento  = 3;

procedure Informar(pPagarId: Integer);
var
  i: Integer;
  vTitulo: TArray<RecContaPagar>;
  vForm: TFormInformacoesTituloPagar;
  vAdiantamentos: TArray<RecContasPagarBaixas>;
begin
  if pPagarId = 0 then
    Exit;

  vTitulo := _ContasPagar.BuscarContasPagar(Sessao.getConexaoBanco, 0, [pPagarId]);
  if vTitulo = nil then begin
    Exclamar('Informa��es do t�tulo n�o foram encontradas!');
    Exit;
  end;

  vAdiantamentos := _ContasPagarBaixas.BuscarContasPagarBaixas(Sessao.getConexaoBanco, 1, [pPagarId]);

  vForm := TFormInformacoesTituloPagar.Create(Application);

  vForm.ePagarId.AsInt                 := pPagarId;
  vForm.eFornecedor.Text               := NFormat(vTitulo[0].CadastroId) + ' - ' + vTitulo[0].nome_fornecedor;
  vForm.eUsuarioCadastro.Text          := NFormat(vTitulo[0].usuario_cadastro_id) + ' - ' + vTitulo[0].nome_usuario_cadastro;
  vForm.eEmpresa.Text                  := NFormat(vTitulo[0].empresa_id) + ' - ' + vTitulo[0].nome_empresa;
  vForm.eEntradaId.SetInformacao(vTitulo[0].entrada_id);
  vForm.eTipoCobranca.Text             := getInformacao(vTitulo[0].cobranca_id, vTitulo[0].nome_tipo_cobranca);
  vForm.txtStatus.Caption              := IIfStr(vTitulo[0].status = 'A', 'Aberto', 'Baixado');
  vForm.txtStatus.Font.Color           := IIfInt(vTitulo[0].status = 'A', $000096DB, clBlue);
  vForm.txtBloqueado.Caption           := _Biblioteca.SimNao(vTitulo[0].Bloqueado);
  vForm.txtBloqueado.Font.Color        := _Biblioteca.AzulVermelho(vTitulo[0].Bloqueado);
  vForm.eDataEmissao.AsData            := vTitulo[0].DataEmissao;
  vForm.eDataContabil.AsData           := vTitulo[0].DataContabil;
  vForm.eDataCadastro.AsData           := vTitulo[0].data_cadastro;
  vForm.eDataCadastro.AsData           := vTitulo[0].data_cadastro;
  vForm.eDataVencimento.AsData         := vTitulo[0].data_vencimento;
  vForm.eDataVencimentoOriginal.AsData := vTitulo[0].data_vencimento_original;
  vForm.eDiasAtraso.AsInt              := vTitulo[0].dias_atraso;
  vForm.eValorTitulo.AsCurr            := vTitulo[0].ValorDocumento;
  vForm.eValorDesconto.AsCurr          := vTitulo[0].ValorDesconto;
  vForm.eValorAdiantado.AsCurr         := vTitulo[0].ValorAdiantado;
  vForm.eValorTotalTitulo.AsCurr       := vTitulo[0].ValorSaldo;
  vForm.eDataPagamento.AsData          := vTitulo[0].data_pagamento;
  vForm.eDataBaixa.AsData              := vTitulo[0].data_hora_baixa;
  vForm.eNumeroCheque.AsInt            := vTitulo[0].numero_cheque;
  vForm.eOrcamentoId.SetInformacao(vTitulo[0].OrcamentoId);
  vForm.eAcumuladoId.SetInformacao(vTitulo[0].AcumuladoId);
  vForm.ePlanoFinanceiro.Text          := getInformacao(vTitulo[0].PlanoFinanceiroId, vTitulo[0].NomePlanoFinanceiro);
  vForm.eCentroCustos.Text             := getInformacao(vTitulo[0].CentroCustoId, vTitulo[0].NomeCentroCusto);
  vForm.eNomePortador.Text             := getInformacao(vTitulo[0].PortadorId, vTitulo[0].NomePortador);
  vForm.stOrigem.Caption               := vTitulo[0].OrigemAnalitico;

  vForm.eBaixaOrigemId.SetInformacao(vTitulo[0].baixa_origem_id);
  vForm.eBaixaId.SetInformacao(vTitulo[0].baixa_id);
  vForm.eBaixaReceberOrigemId.SetInformacao(vTitulo[0].BaixaReceberOrigemId);
  vForm.eDevolucaoVenda.SetInformacao(vTitulo[0].DevolucaoId);

  if vTitulo[0].usuario_baixa_id > 0 then
    vForm.eNomeUsuarioBaixa.Text         := NFormat(vTitulo[0].usuario_baixa_id) + ' - ' + vTitulo[0].nome_usuario_baixa;

  vForm.meObservacoes.Lines.Text := vTitulo[0].Observacoes;

  for i := Low(vAdiantamentos) to High(vAdiantamentos) do begin
    vForm.sgAdiantamentos.Cells[coBaixaAdiantamentoId, i + 1]  := NFormat(vAdiantamentos[i].baixa_id);
    vForm.sgAdiantamentos.Cells[coDataHoraAdiantamento, i + 1] := DHFormat(vAdiantamentos[i].data_hora_baixa);
    vForm.sgAdiantamentos.Cells[coValorAdiantamento, i + 1]    := NFormat(vAdiantamentos[i].ValorTitulos);
    vForm.sgAdiantamentos.Cells[coUsuarioAdiantamento, i + 1]  := getInformacao(vAdiantamentos[i].usuario_baixa_id, vAdiantamentos[i].NomeUsuarioBaixa);
  end;
  vForm.sgAdiantamentos.SetLinhasGridPorTamanhoVetor( Length(vAdiantamentos) );

  vForm.ShowModal;

  vForm.Free;
end;

procedure TFormInformacoesTituloPagar.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(ePagarId);
end;

procedure TFormInformacoesTituloPagar.sbInformacoesPedidoClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar(eOrcamentoId.AsInt);
end;

procedure TFormInformacoesTituloPagar.sbBaixaReceberOrigemIdClick(Sender: TObject);
begin
  inherited;
  Informacoes.ContasReceberBaixa.Informar(eBaixaReceberOrigemId.IdInformacao);
end;

procedure TFormInformacoesTituloPagar.sbInfoEntradaIdClick(Sender: TObject);
begin
  inherited;
  InformacoesEntradaNotaFiscal.Informar( eEntradaId.AsInt );
end;

procedure TFormInformacoesTituloPagar.sbInformacoesAcumuladoClick(Sender: TObject);
begin
  inherited;
  InformacoesAcumulado.Informar(eAcumuladoId.AsInt);
end;


procedure TFormInformacoesTituloPagar.sbInformacoesBaixaClick(Sender: TObject);
begin
  inherited;
  InformacoesContasPagarBaixa.Informar(eBaixaId.IdInformacao);
end;

procedure TFormInformacoesTituloPagar.sbInformacoesDevolucaoVendaClick(Sender: TObject);
begin
  inherited;
  Informacoes.Devolucao.Informar(eDevolucaoVenda.AsInt);
end;

procedure TFormInformacoesTituloPagar.sbInformacoesOrigemBaixaClick(Sender: TObject);
begin
  inherited;
  InformacoesContasPagarBaixa.Informar(eBaixaOrigemId.IdInformacao);
end;

procedure TFormInformacoesTituloPagar.sbLogsClick(Sender: TObject);
begin
  inherited;
  Logs.Iniciar('LOGS_CONTAS_PAGAR', ['PAGAR_ID'], 'VW_TIPOS_ALTER_LOGS_CT_PAGAR', [ePagarId.AsInt]);
end;

procedure TFormInformacoesTituloPagar.sgAdiantamentosDblClick(Sender: TObject);
begin
  inherited;
  InformacoesContasPagarBaixa.Informar( SFormatInt(sgAdiantamentos.Cells[coBaixaAdiantamentoId, sgAdiantamentos.Row]) );
end;

procedure TFormInformacoesTituloPagar.sgAdiantamentosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coBaixaAdiantamentoId, coValorAdiantamento] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgAdiantamentos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
