unit Frame.DepartamentosSecoesLinhas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Buttons, _RecordsCadastros,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Sessao, _Biblioteca, System.Math,
  Pesquisa.DepartamentosSecoesLinhas, _ProdutosDeptosSecoesLinhas, Vcl.Menus;
type
  TFrameDepartamentosSecoesLinhas = class(TFrameHenrancaPesquisas)
    rgNivel: TRadioGroup;
  public
    function GetProdutoDeptoSecaoLinha(pLinha: Integer = -1): RecProdutoDeptoSecaoLinha;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrameDepartamentosSecoesLinhas }

function TFrameDepartamentosSecoesLinhas.AdicionarDireto: TObject;
var
  vDepto: TArray<RecProdutoDeptoSecaoLinha>;
begin
  vDepto := _ProdutosDeptosSecoesLinhas.BuscarProdutoDeptoSecaoLinhas(Sessao.getConexaoBanco, 0, [FChaveDigitada], rgNivel.ItemIndex );
  if vDepto = nil then
    Result := nil
  else
    Result := vDepto[0];
end;

function TFrameDepartamentosSecoesLinhas.AdicionarPesquisando: TObject;
begin
  Result := Pesquisa.DepartamentosSecoesLinhas.Pesquisar(rgNivel.ItemIndex);
end;

function TFrameDepartamentosSecoesLinhas.GetProdutoDeptoSecaoLinha(pLinha: Integer): RecProdutoDeptoSecaoLinha;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecProdutoDeptoSecaoLinha(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrameDepartamentosSecoesLinhas.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecProdutoDeptoSecaoLinha(FDados[i]).DeptoSecaoLinhaId = RecProdutoDeptoSecaoLinha(pSender).DeptoSecaoLinhaId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrameDepartamentosSecoesLinhas.MontarGrid;
var
  i: Integer;
  vSender: RecProdutoDeptoSecaoLinha;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecProdutoDeptoSecaoLinha(FDados[i]);
      AAdd([vSender.DeptoSecaoLinhaId, vSender.Estrutura]);
    end;
  end;
end;

end.
