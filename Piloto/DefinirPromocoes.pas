unit DefinirPromocoes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _ProdutosPromocoes,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, FrameDataInicialFinal, _Biblioteca, _Sessao,
  Frame.DepartamentosSecoesLinhas, FrameProdutos, _FrameHerancaPrincipal, _RecordsEspeciais,
  _FrameHenrancaPesquisas, FrameEmpresas, Vcl.Grids, GridLuka, _Imagens,
  FrameMarcas, FrameNumeros, Vcl.Menus, SpeedButtonLuka, AlterarDadosPrecificacaoProdutos,
  Vcl.StdCtrls, CheckBoxLuka, Data.DB, Datasnap.DBClient, frxClass, frxDBSet;

type
  TFormDefinirPromocoes = class(TFormHerancaRelatoriosPageControl)
    FrEmpresas: TFrEmpresas;
    FrProdutos: TFrProdutos;
    FrDepartamentosSecoesLinhas: TFrameDepartamentosSecoesLinhas;
    FrDataCadastroProduto: TFrDataInicialFinal;
    sgItens: TGridLuka;
    FrMarcas: TFrMarcas;
    FrCodigoEntrada: TFrNumeros;
    pmOpcoes: TPopupMenu;
    miMarcarSelecionado: TMenuItem;
    miMarcarTodos: TMenuItem;
    sbLimparPromocoes: TSpeedButtonLuka;
    FrCodigoPedido: TFrNumeros;
    FrDataInicialPromocao: TFrDataInicialFinal;
    FrDataFinalPromocao: TFrDataInicialFinal;
    N1: TMenuItem;
    miAlterarDadosColunaSelecionada: TMenuItem;
    sbGravar: TSpeedButton;
    frxReport: TfrxReport;
    dstPromocoes: TfrxDBDataset;
    cdsPromocoes: TClientDataSet;
    cdsProduto: TStringField;
    cdsNome: TStringField;
    cdsDataAlteracao: TStringField;
    cdsUsuarioAlteracao: TStringField;
    cdsValorAnterior: TStringField;
    cdsNovoValor: TStringField;
    FrDataCadastroPromocao: TFrDataInicialFinal;
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sbGravarClick(Sender: TObject);
    procedure sgItensGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
    procedure sgItensKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgItensGetEditMask(Sender: TObject; ACol, ARow: Integer; var Value: string);
    procedure FormShow(Sender: TObject);
    procedure sbLimparPromocoesClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure miMarcarSelecionadoClick(Sender: TObject);
    procedure miMarcarTodosClick(Sender: TObject);
    procedure miAlterarDadosColunaSelecionadaClick(Sender: TObject);
    procedure sbImprimirClick(Sender: TObject);
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

const
  coSelecionado      = 0;
  coProdutoId        = 1;
  coNome             = 2;
  coCustoCompra      = 3;
  coPrecoVendaAtual  = 4;
  coPercMargLucPr    = 5;
  coPrecoPromocional = 6;
  coPercCustoVenda   = 7;
  coCustoComercial   = 8;
  coQtdeMinima       = 9;
  coDataInicial      = 10;
  coDataFinal        = 11;
  coDataAlteracao    = 12;
  coUsuarioAlteracao = 13;
  coNomeUsuarioAlteracao = 14;

procedure TFormDefinirPromocoes.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vItens: TArray<RecItensPromocoes>;
begin
  inherited;
  sgItens.ClearGrid();

  vSql := vSql + ' and PRE.EMPRESA_ID = ' + IntToStr(Sessao.getEmpresaLogada.EmpresaId);

  if not FrProdutos.EstaVazio then
    vSql := vSql + ' and ' + FrProdutos.getSqlFiltros('PRO.PRODUTO_ID');

  if not FrDepartamentosSecoesLinhas.EstaVazio then
    vSql := vSql + ' and ' + FrDepartamentosSecoesLinhas.getSqlFiltros('PRO.LINHA_PRODUTO_ID');

  if not FrDataCadastroProduto.NenhumaDataValida then
    vSql := vSql + ' and ' + FrDataCadastroProduto.getSqlFiltros('PRO.DATA_CADASTRO');

  if not FrDataInicialPromocao.NenhumaDataValida then
    vSql := vSql + ' and ' + FrDataInicialPromocao.getSqlFiltros('PPR.DATA_INICIAL_PROMOCAO');

  if not FrDataFinalPromocao.NenhumaDataValida then
    vSql := vSql + ' and ' + FrDataFinalPromocao.getSqlFiltros('PPR.DATA_FINAL_PROMOCAO');

  if not FrDataCadastroPromocao.NenhumaDataValida then begin
    vSql := vSql + ' and ' + FrDataCadastroPromocao.getSqlFiltros('PPR.DATA_HORA_ALTERACAO');
  end;

  if not FrMarcas.EstaVazio then begin
    vSql := vSql + ' and ' + FrMarcas.getSqlFiltros('PRO.MARCA_ID');
  end;

  if not FrCodigoEntrada.EstaVazio then
    vSql := vSql + ' and PRO.PRODUTO_ID in(select PRODUTO_ID from ENTRADAS_NOTAS_FISCAIS_ITENS where ' + FrCodigoEntrada.TrazerFiltros('ENTRADA_ID') + ') ';

  if not FrCodigoPedido.EstaVazio then
    vSql := vSql + ' and PRO.PRODUTO_ID in(select PRODUTO_ID from COMPRAS_ITENS where ' + FrCodigoPedido.TrazerFiltros('COMPRA_ID') + ') ';

  vSql := vSql + ' order by PRO.NOME ';

  vItens := _ProdutosPromocoes.BuscarPromocoesComando(Sessao.getConexaoBanco, vSql);
  if vItens = nil then begin
    NenhumRegistro;
    Exit;
  end;

  for i := Low(vItens) to High(vItens) do begin
    sgItens.Cells[coSelecionado, i + 1]      := charNaoSelecionado;
    sgItens.Cells[coProdutoId, i + 1]        := _Biblioteca.NFormat(vItens[i].ProdutoId);
    sgItens.Cells[coNome, i + 1]             :=  vItens[i].Nome;
    sgItens.Cells[coCustoCompra, i + 1]      :=  _Biblioteca.NFormatN( vItens[i].CustoCompraComercial, 4);
    sgItens.Cells[coPrecoVendaAtual, i + 1]  :=  _Biblioteca.NFormatN( vItens[i].PrecoVendaAtual, 4);
    sgItens.Cells[coPercMargLucPr, i + 1]    :=  '';
    sgItens.Cells[coPrecoPromocional, i + 1] :=  _Biblioteca.NFormatN( vItens[i].PrecoPromocional ) ;
    sgItens.Cells[coPercCustoVenda, i + 1]   :=  _Biblioteca.NFormatN( vItens[i].PercCustoVenda, 5);
    sgItens.Cells[coCustoComercial, i + 1]   :=  '';
    sgItens.Cells[coQtdeMinima, i + 1]       :=  _Biblioteca.NFormatN( vItens[i].QuantidadeMinima, 4);
    sgItens.Cells[coDataInicial, i + 1]      :=  _Biblioteca.DFormatN( vItens[i].DataInicialPromocao );
    sgItens.Cells[coDataFinal, i + 1]        :=  _Biblioteca.DFormatN( vItens[i].DataFinalPromocao );

    sgItens.Cells[coDataAlteracao, i + 1]    :=  _Biblioteca.DFormatN( vItens[i].DataAlteracao );
    sgItens.Cells[coUsuarioAlteracao, i + 1] := _Biblioteca.NFormat(vItens[i].UsuarioAlteracao);
    sgItens.Cells[coNomeUsuarioAlteracao, i + 1] := vItens[i].NomeUsuarioAlteracao;
  end;
  sgItens.SetLinhasGridPorTamanhoVetor( Length(vItens) );

  sgItens.Col := coPercMargLucPr;
  SetarFoco(sgItens);
end;

procedure TFormDefinirPromocoes.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_F2 then
    sbGravarClick(Sender)
  else if Key = VK_F4 then
   sbLimparPromocoesClick(Sender);
end;

procedure TFormDefinirPromocoes.FormShow(Sender: TObject);
begin
  inherited;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId, False);
  SetarFoco(FrProdutos);
end;

procedure TFormDefinirPromocoes.Imprimir(Sender: TObject);
begin
  inherited;

end;

procedure TFormDefinirPromocoes.miAlterarDadosColunaSelecionadaClick(
  Sender: TObject);
begin
  inherited;
  AlterarDadosPrecificacaoProdutos.AlterarDados(
    sgItens,
    coSelecionado,
    charSelecionado,
    'Alterar precifica��o dos produtos selecionados',
    [coPrecoPromocional],
    [coQtdeMinima],
    [coDataInicial, coDataFinal],
    [coPercMargLucPr]
  );
end;

procedure TFormDefinirPromocoes.miMarcarSelecionadoClick(Sender: TObject);
begin
  inherited;
  if sgItens.Cells[coProdutoId, sgItens.Row] = '' then
    Exit;

  sgItens.Cells[coSelecionado, sgItens.Row] := IIfStr(sgItens.Cells[coSelecionado, sgItens.Row] = charNaoSelecionado, charSelecionado, charNaoSelecionado);
end;

procedure TFormDefinirPromocoes.miMarcarTodosClick(Sender: TObject);
var
  i: Integer;
  vMarcar: Boolean;
begin
  if sgItens.Cells[coProdutoId, 1] = '' then
    Exit;

  vMarcar := sgItens.Cells[coSelecionado, 1] = charNaoSelecionado;
  for i := 1 to sgItens.RowCount -1 do
    sgItens.Cells[coSelecionado, i] := IIfStr(vMarcar, charSelecionado, charNaoSelecionado);
end;

procedure TFormDefinirPromocoes.sbGravarClick(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  vPromocoes: TArray<RecItensPromocoes>;

  vDataAtual: TDateTime;
begin
  inherited;

  vDataAtual := Sessao.getData;

  vPromocoes := nil;
  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    SetLength(vPromocoes, Length(vPromocoes) + 1);
    vPromocoes[High(vPromocoes)].ProdutoId           := SFormatInt( sgItens.Cells[coProdutoId, i] );
    vPromocoes[High(vPromocoes)].PrecoPromocional    := SFormatDouble( sgItens.Cells[coPrecoPromocional, i] );
    vPromocoes[High(vPromocoes)].DataInicialPromocao := ToData( sgItens.Cells[coDataInicial, i] );
    vPromocoes[High(vPromocoes)].DataFinalPromocao   := ToData( sgItens.Cells[coDataFinal, i] );
    vPromocoes[High(vPromocoes)].QuantidadeMinima    := SFormatDouble( sgItens.Cells[coQtdeMinima, i] );

    if NFormatN(vPromocoes[High(vPromocoes)].PrecoPromocional) = '' then begin
      _Biblioteca.Exclamar('O pre�o promocional n�o foi definido para o produto ' + sgItens.Cells[coNome, i] + '!');
      SgItens.Row := i;
      sgItens.Col := coPrecoPromocional;
      SetarFoco(sgItens);
      Exit;
    end;

    if vPromocoes[High(vPromocoes)].DataInicialPromocao > vPromocoes[High(vPromocoes)].DataFinalPromocao then begin
      _Biblioteca.Exclamar('A data inicical da promo��o n�o pode ser menor que a data final!');
      SgItens.Row := i;
      SetarFoco(sgItens);
      Exit;
    end;

    if SFormatDouble( sgItens.Cells[coPrecoVendaAtual, i] ) <= SFormatDouble( sgItens.Cells[coPrecoPromocional, i] ) then begin
      _Biblioteca.Exclamar('O pre�o promocional tem que ser menor que o pre�o de venda atual!');
      SgItens.Row := i;
      SetarFoco(sgItens);
      Exit;
    end;

    if vDataAtual > vPromocoes[High(vPromocoes)].DataFinalPromocao then begin
      _Biblioteca.Exclamar('A data final da promo��o n�o pode ser menor que a data atual para o produto ' + sgItens.Cells[coNome, i] + '!');
      SgItens.Row := i;
      SetarFoco(sgItens);
      Exit;
    end;
  end;

  if vPromocoes = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado para altera��o da promo��o!');
    SetarFoco(sgItens);
    Exit;
  end;

  vRetBanco := _ProdutosPromocoes.AtualizarPromocoes(Sessao.getConexaoBanco, FrEmpresas.GetArrayOfInteger, vPromocoes);
  if vRetBanco.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;
  RotinaSucesso;
  Carregar(nil);
  sbImprimirClick(nil);
end;

procedure TFormDefinirPromocoes.sbImprimirClick(Sender: TObject);
var
  vCnt: Integer;
begin
  inherited;
  if sgItens.IsRowEmpty(1) then
    exit;

  cdsPromocoes.Close;
  cdsPromocoes.CreateDataSet;
  cdsPromocoes.Open;

  for vCnt := sgItens.RowCount - 1 downto 1 do begin
    if sgItens.Cells[coPrecoPromocional, vCnt] = '' then
      Continue;

    cdsPromocoes.Insert;

    cdsProduto.AsString           := sgItens.Cells[coProdutoId, vCnt];
    cdsNome.AsString              := sgItens.Cells[coNome, vCnt];
    cdsValorAnterior.AsString     := sgItens.Cells[coPrecoVendaAtual, vCnt];
    cdsNovoValor.AsString         := sgItens.Cells[coPrecoPromocional, vCnt];
    cdsDataAlteracao.AsString     := sgItens.Cells[coDataAlteracao, vCnt];
    cdsUsuarioAlteracao.AsString  := sgItens.Cells[coUsuarioAlteracao, vCnt] + ' - ' + sgItens.Cells[coNomeUsuarioAlteracao, vCnt];

    cdsPromocoes.Post;
  end;

  frxReport.ShowReport;
end;

procedure TFormDefinirPromocoes.sbLimparPromocoesClick(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  vProdutosIds: TArray<Integer>;
begin
  inherited;

  vProdutosIds := nil;
  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt(sgItens.Cells[coProdutoId, i]) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado para exclus�o da promo��o!');
    SetarFoco(sgItens);
    Exit;
  end;

  if not _Biblioteca.Perguntar('Voc� deseja excluir os produtos selecionados da promo��o?') then
    Exit;

  vRetBanco := _ProdutosPromocoes.ExcluirPromocoes(Sessao.getConexaoBanco, FrEmpresas.GetArrayOfInteger, vProdutosIds);
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(nil);
end;

procedure TFormDefinirPromocoes.sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);

  procedure CalcularPrecoProduto(pMargem: Double; pPercCustoVenda: Double; pPrecoBase: Double);
  begin
    sgItens.Cells[coPrecoPromocional, ARow] :=
      _Biblioteca.NFormatN(
        Sessao.getCalculosSistema.CalcFormacaoCusto.getPrecoSugerido(
          pMargem,
          pPercCustoVenda,
          pPrecoBase
        )
      );
  end;

begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coPercMargLucPr then begin
    TextCell := _Biblioteca.NFormatN( SFormatDouble(TextCell), 5 );
    CalcularPrecoProduto(SFormatDouble(TextCell), SFormatDouble(sgItens.Cells[coPercCustoVenda, ARow]), SFormatDouble(sgItens.Cells[coCustoCompra, ARow]));
  end
  else if ACol = coPrecoPromocional then begin
    TextCell := _Biblioteca.NFormatN( SFormatDouble(TextCell) );

    sgItens.Cells[coCustoComercial, ARow] :=
      NFormatN(
        Sessao.getCalculosSistema.CalcFormacaoCusto.getCustoComercial( SFormatDouble(sgItens.Cells[coCustoCompra, ARow]),  SFormatDouble(TextCell), SFormatDouble(sgItens.Cells[coPercCustoVenda, ARow]) ), 4
      );

    sgItens.Cells[coPercMargLucPr, ARow] :=
      _Biblioteca.NFormatN(
        Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(
          SFormatDouble(TextCell),
          SFormatDouble(TextCell) - SFormatDouble(sgItens.Cells[coCustoComercial, ARow])
        ),5
      );
  end
  else if ACol = coQtdeMinima then
    TextCell := _Biblioteca.NFormatN( SFormatDouble(TextCell), 4 )
  else if ACol = coPrecoPromocional then
    TextCell :=_Biblioteca.NFormatN( SFormatDouble(TextCell), 2 )
  else if ACol in[coDataInicial, coDataFinal] then
    TextCell := _Biblioteca.FormatarData(TextCell);
end;

procedure TFormDefinirPromocoes.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[
    coProdutoId,
    coCustoCompra,
    coPrecoVendaAtual,
    coPercMargLucPr,
    coPrecoPromocional,
    coPercCustoVenda,
    coCustoComercial,
    coQtdeMinima]
  then
    vAlinhamento := taRightJustify
  else if ACol = coSelecionado then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormDefinirPromocoes.sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coPrecoPromocional then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao1;
    AFont.Color := _Biblioteca.coCorFonteEdicao1;
  end
  else if ACol in[coDataInicial, coDataFinal] then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Color := _Biblioteca.coCorFonteEdicao2;
  end;
end;

procedure TFormDefinirPromocoes.sgItensGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if (ACol <> coSelecionado) or (sgItens.Cells[ACol, ARow] = '') then
   Exit;

  if sgItens.Cells[ACol, ARow] = charNaoSelecionado then
    APicture := _Imagens.FormImagens.im1.Picture
  else
    APicture := _Imagens.FormImagens.im2.Picture;
end;

procedure TFormDefinirPromocoes.sgItensGetEditMask(Sender: TObject; ACol, ARow: Integer; var Value: string);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol in [coDataInicial, coDataFinal] then
    Value := '99/99/9999;1;_';
end;

procedure TFormDefinirPromocoes.sgItensKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (Key = Ord('A')) and (sgItens.Col in[coDataInicial, coDataFinal]) then
    sgItens.Cells[sgItens.Col, sgItens.Row] := _Biblioteca.getDataAtual
  else if (Key = Ord('I')) and (sgItens.Col in[coDataInicial, coDataFinal]) then
    sgItens.Cells[sgItens.Col, sgItens.Row] := _Biblioteca.getPrimeiroDiaMes
  else if (Key = Ord('F')) and (sgItens.Col in[coDataInicial, coDataFinal]) then
    sgItens.Cells[sgItens.Col, sgItens.Row] := _Biblioteca.getUltimoDiaMes
  else if (Shift = [ssCtrl]) and (Key = VK_SPACE) then
    miMarcarTodosClick(Sender)
  else if Key = VK_SPACE then begin
    miMarcarSelecionadoClick(Sender);

    if sgItens.Col in[coDataInicial, coDataFinal] then
      Exit; // Dando o exit para n�o apagar a data caso seja uma coluna de data focada
  end;
  inherited;
end;

procedure TFormDefinirPromocoes.sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  if ACol in[
    coPercMargLucPr,
    coPrecoPromocional,
    coQtdeMinima,
    coDataInicial,
    coDataFinal]
  then
    sgItens.Options := sgItens.Options + [goEditing]
  else
    sgItens.Options := sgItens.Options - [goEditing];
end;

procedure TFormDefinirPromocoes.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
