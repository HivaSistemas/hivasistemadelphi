unit PesquisaContasCustodia;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _Biblioteca, _ContasCustodia,
  ComboBoxLuka, EditLuka, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Sessao;

type
  TFormPesquisaContasCustodia = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
  protected
    procedure BuscarRegistros; override;
  end;

function Pesquisar(pSomenteAtivos: Boolean): TObject;

implementation

{$R *.dfm}

const
  coNome   = 2;
  coAtivo  = 3;

function Pesquisar(pSomenteAtivos: Boolean): TObject;
var
  vDado: TObject;
begin
  vDado := _HerancaPesquisas.Pesquisar(TFormPesquisaContasCustodia, _ContasCustodia.GetFiltros, [pSomenteAtivos]);
  if vDado = nil then
    Result := nil
  else
    Result := RecContasCustodia(vDado);
end;

{ TFormPesquisaContasCustodia }

procedure TFormPesquisaContasCustodia.BuscarRegistros;
var
  i: Integer;
  vDados: TArray<RecContasCustodia>;
begin
  inherited;

  vDados :=
    _ContasCustodia.BuscarContasCustodia(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text],
      FAuxiliares[0]
    );

  if vDados = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vDados);

  for i := Low(vDados) to High(vDados) do begin
    sgPesquisa.Cells[coSelecionado, i + 1]  := _Biblioteca.charNaoSelecionado;
    sgPesquisa.Cells[coCodigo, i + 1]       := NFormat(vDados[i].ContaCustodiaId);
    sgPesquisa.Cells[coNome, i + 1]         := vDados[i].Nome;
    sgPesquisa.Cells[coAtivo, i + 1]        := _Biblioteca.SimNao(vDados[i].Ativo);
  end;
  sgPesquisa.SetLinhasGridPorTamanhoVetor( Length(vDados) );
  SetarFoco(sgPesquisa);
end;

procedure TFormPesquisaContasCustodia.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coSelecionado, coAtivo] then
    vAlinhamento := taCenter
  else if ACol = coCodigo then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
