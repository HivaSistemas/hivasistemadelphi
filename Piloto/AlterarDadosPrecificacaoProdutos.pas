unit AlterarDadosPrecificacaoProdutos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, GridLuka, Vcl.StdCtrls, EditLuka, _Biblioteca, Vcl.Mask,
  EditLukaData, RadioGroupLuka;

type
  TFormAlterarDadosPrecificacaoProdutos = class(TFormHerancaFinalizar)
    lbColunaSelecionada: TLabel;
    eValor: TEditLuka;
    eValorData: TEditLukaData;
    rgTipoAlteracao: TRadioGroupLuka;
    procedure eValorExit(Sender: TObject);
  private
    vSomentePercMenor100: Boolean;
  public
    { Public declarations }
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function AlterarDados(
  var pGrid: TGridLuka;
  pCoSelecionado: Integer;
  pCharSelecionado: string;
  pCaption: string;
  pColunasEditaveisValorPercentual: TArray<Integer>;
  pColunasEditaveisValor: TArray<Integer>;
  pColunasEditaveisData: TArray<Integer>;
  pColunasPercMenor100: TArray<Integer> = nil
): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

function AlterarDados(
  var pGrid: TGridLuka;
  pCoSelecionado: Integer;
  pCharSelecionado: string;
  pCaption: string;
  pColunasEditaveisValorPercentual: TArray<Integer>;
  pColunasEditaveisValor: TArray<Integer>;
  pColunasEditaveisData: TArray<Integer>;
  pColunasPercMenor100: TArray<Integer> = nil
): TRetornoTelaFinalizar;

var
  vForm: TFormAlterarDadosPrecificacaoProdutos;
  vlinha: Integer;
  vTipoData: Boolean;
  vTipoValorPercentual: Boolean;
  vTipoValor: Boolean;
  vExisteLinhaSelecionada: Boolean;
begin
  vForm := TFormAlterarDadosPrecificacaoProdutos.Create(nil);
  vForm.lbColunaSelecionada.Caption := pGrid.Cells[pGrid.Col, pGrid.FixedRows - 1];

  vTipoValorPercentual := False;
  vTipoData := False;
  vTipoValor := False;
  vForm.vSomentePercMenor100 := False;

  vExisteLinhaSelecionada := False;
  for vlinha := pGrid.FixedRows to pGrid.RowCount - 1 do begin
    if pGrid.Cells[pCoSelecionado, vlinha] = pCharSelecionado then begin
      vExisteLinhaSelecionada := True;
      Break;
    end;
  end;

  if not vExisteLinhaSelecionada then begin
    _Biblioteca.NenhumRegistroSelecionado;
    vForm.Free;
    Abort;
  end;

  for vlinha := Low(pColunasEditaveisValorPercentual) to High(pColunasEditaveisValorPercentual) do begin
    if pGrid.Col = pColunasEditaveisValorPercentual[vLinha] then begin
      vTipoValorPercentual := True;
      Break;
    end;
  end;

  for vlinha := Low(pColunasEditaveisValor) to High(pColunasEditaveisValor) do begin
    if pGrid.Col = pColunasEditaveisValor[vLinha] then begin
      vTipoValor := True;
      Break;
    end;
  end;

  for vlinha := Low(pColunasEditaveisData) to High(pColunasEditaveisData) do begin
    if pGrid.Col = pColunasEditaveisData[vLinha] then begin
      vTipoData := True;
      Break;
    end;
  end;

  for vlinha := Low(pColunasPercMenor100) to High(pColunasPercMenor100) do begin
    if pGrid.Col = pColunasPercMenor100[vLinha] then begin
      vTipoValor := True;
      vForm.vSomentePercMenor100 := True;
      Break;
    end;
  end;

  if not vTipoValorPercentual and not vTipoValor and not vTipoData  then begin
    _Biblioteca.Exclamar('O campo ' + vForm.lbColunaSelecionada.Caption + ' n�o pode ser alterado!');
    vForm.Free;
    Abort;
  end;

  vForm.eValorData.Visible      := vTipoData;
  vForm.eValor.Visible          := vTipoValorPercentual or vTipoValor;
  vForm.rgTipoAlteracao.Visible := vTipoValorPercentual;

  if vTipoValor or vTipoData then begin
     vForm.Width  := 230;
     vForm.Height := 125;
  end;

  if Result.Ok(vForm.ShowModal, True) then begin


    for vlinha := pGrid.FixedRows to pGrid.RowCount - 1 do begin

      if pGrid.Cells[pCoSelecionado, vlinha] = pCharSelecionado then begin

        if vTipoValor or vTipoValorPercentual then begin
          if (vTipoValorPercentual and (vForm.rgTipoAlteracao.GetValor = 'VLR')) or vTipoValor then
            pGrid.Cells[pGrid.Col, vlinha] := _Biblioteca.NFormatN(vForm.eValor.AsDouble, 4)
          else if (vForm.rgTipoAlteracao.GetValor = 'AMP') and (pGrid.Cells[pGrid.Col, vlinha] <> '') then
            pGrid.Cells[pGrid.Col, vlinha] := _Biblioteca.NFormatN(SFormatDouble(pGrid.Cells[pGrid.Col, vlinha]) + (SFormatDouble(pGrid.Cells[pGrid.Col, vlinha]) * (vForm.eValor.AsDouble * 0.01)), 4)
          else if (vForm.rgTipoAlteracao.GetValor = 'DMP') and (pGrid.Cells[pGrid.Col, vlinha] <> '') then
            pGrid.Cells[pGrid.Col, vlinha] := _Biblioteca.NFormatN(SFormatDouble(pGrid.Cells[pGrid.Col, vlinha]) - (SFormatDouble(pGrid.Cells[pGrid.Col, vlinha]) * (vForm.eValor.AsDouble * 0.01)), 4)
        end
        else
          pGrid.Cells[pGrid.Col, vlinha] := _Biblioteca.DFormatN(vForm.eValorData.AsData);
      end;
    end;
  end;
end;

procedure TFormAlterarDadosPrecificacaoProdutos.eValorExit(Sender: TObject);
begin
  if (rgTipoAlteracao.GetValor = 'VLR') and vSomentePercMenor100 and (eValor.AsDouble >= 100) then begin
     _Biblioteca.Exclamar('N�o � permitido utilizar um valor maior que 99.99%, para o campo ' + lbColunaSelecionada.Caption);
     eValor.Clear;
     Abort;
  end;

  inherited;
end;

procedure TFormAlterarDadosPrecificacaoProdutos.VerificarRegistro(Sender: TObject);
begin
  inherited;
  eValorExit(nil);
end;

end.
