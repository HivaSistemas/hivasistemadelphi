unit PesquisaTiposCobranca;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _TiposCobranca,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _RecordsCadastros, _Biblioteca, System.Math,
  Vcl.ExtCtrls;

type
  TFormPesquisaTiposCobranca = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  private
    FCondicaoId: Integer;
    FFormaPagamento: string;

  protected
    procedure BuscarRegistros; override;
  end;

function PesquisarTiposCobranca(pNatureza: string): TObject; overload;
function PesquisarTiposCobranca(const pCondicaoId: Integer; const pFormaPagamento: string; pNatureza: string): TObject; overload;
function PesquisarVarios(pNatureza: string): TArray<TObject>;

implementation

{$R *.dfm}

uses
  _Sessao;

{ TFormPesquisaTiposCobranca }

const
  cp_nome              = 2;
  cp_taxa_retencao_mes = 3;
  cpPortador           = 4;
  cp_rede_cartao       = 5;
  cp_tipo_cartao       = 6;
  cp_ativo             = 7;

var
  FNatureza: string;

function PesquisarTiposCobranca(pNatureza: string): TObject;
var
  r: TObject;
begin
  FNatureza := pNatureza;
  r := _HerancaPesquisas.Pesquisar(TFormPesquisaTiposCobranca, _TiposCobranca.GetFiltros);
  if r = nil then
    Result := nil
  else
    Result := RecTiposCobranca(r);
end;

function PesquisarTiposCobranca(const pCondicaoId: Integer; const pFormaPagamento: string; pNatureza: string): TObject;
var
  t: TFormPesquisaTiposCobranca;
begin
  Result    := nil;
  FNatureza := pNatureza;

  if (pCondicaoId = 0) and (pFormaPagamento = '') then begin
    Result := PesquisarTiposCobranca(pNatureza);
    Exit;
  end;

  t := TFormPesquisaTiposCobranca.Create(Application);

  t.FCondicaoId := pCondicaoId;
  t.FFormaPagamento := pFormaPagamento;

  t.sgPesquisa.OcultarColunas([coSelecionado]);
  t.cbOpcoesPesquisa.Filtros := _TiposCobranca.getFiltros;

  if t.ShowModal = mrOk then
    Result := RecTiposCobranca(t.FDados[t.sgPesquisa.Row - 1]);

  t.Free;
end;

function PesquisarVarios(pNatureza: string): TArray<TObject>;
var
  vDados: TArray<TObject>;
begin
  FNatureza := pNatureza;
  vDados := _HerancaPesquisas.PesquisarVarios(TFormPesquisaTiposCobranca, _TiposCobranca.GetFiltros);
  if vDados = nil then
    Result := nil
  else
    Result := TArray<TObject>(vDados);
end;

procedure TFormPesquisaTiposCobranca.BuscarRegistros;
var
  i: Integer;
  vComando: string;
  vTiposCobranca: TArray<RecTiposCobranca>;
begin
  inherited;

  if (FCondicaoID = 0) and (FFormaPagamento = '') then begin
    vTiposCobranca :=
      _TiposCobranca.BuscarTiposCobrancas(
        Sessao.getConexaoBanco,
        cbOpcoesPesquisa.GetIndice,
        [eValorPesquisa.Text],
        FNatureza
      );
  end
  else begin
    _Biblioteca.WhereOuAnd(vComando, 'TPC.COBRANCA_ID in(select distinct COBRANCA_ID from TIPOS_COBR_COND_PAGAMENTO where NOME like ''' + eValorPesquisa.Text + ''' || ''%'') ');

    if FCondicaoID > 0 then
      _Biblioteca.WhereOuAnd(vComando, 'TPC.COBRANCA_ID in(select distinct COBRANCA_ID from TIPOS_COBR_COND_PAGAMENTO where CONDICAO_ID = ' + IntToStr(FCondicaoId) + ') ');

    if FFormaPagamento <> '' then
      _Biblioteca.WhereOuAnd(vComando, 'TPC.FORMA_PAGAMENTO = ' + QuotedStr(FFormaPagamento) + ' ');

    if FNatureza <> '' then
      _Biblioteca.WhereOuAnd(vComando, 'TPC.INCIDENCIA_FINANCEIRA in(''' + FNatureza + ''',''A'') ');

    vTiposCobranca :=
      _TiposCobranca.BuscarTiposCobrancasComando(
        Sessao.getConexaoBanco,
        vComando
      );
  end;

  if vTiposCobranca = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vTiposCobranca);
  for i := Low(vTiposCobranca) to High(vTiposCobranca) do begin
    sgPesquisa.Cells[coSelecionado, i + 1]        := _Biblioteca.charNaoSelecionado;
    sgPesquisa.Cells[coCodigo, i + 1]             := IntToStr(vTiposCobranca[i].CobrancaId);
    sgPesquisa.Cells[cp_nome, i + 1]              := vTiposCobranca[i].Nome;
    sgPesquisa.Cells[cp_taxa_retencao_mes, i + 1] := NFormatN(vTiposCobranca[i].taxa_retencao_mes);
    sgPesquisa.Cells[cpPortador, i + 1]           := getInformacao(vTiposCobranca[i].PortadorId, vTiposCobranca[i].NomePortador);
    sgPesquisa.Cells[cp_rede_cartao, i + 1]       := vTiposCobranca[i].rede_cartao;
    sgPesquisa.Cells[cp_tipo_cartao, i + 1]       := vTiposCobranca[i].tipo_cartao;
    sgPesquisa.Cells[cp_ativo, i + 1]             := vTiposCobranca[i].ativo;
  end;
  sgPesquisa.SetLinhasGridPorTamanhoVetor( Length(vTiposCobranca) );

  if sgPesquisa.ColWidths[coSelecionado] < 1 then
    sgPesquisa.Col := coCodigo;

  sgPesquisa.SetFocus;
end;

procedure TFormPesquisaTiposCobranca.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in [coCodigo, cp_taxa_retencao_mes] then
    vAlinhamento := taRightJustify
  else if ACol in [coSelecionado, cp_ativo] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
