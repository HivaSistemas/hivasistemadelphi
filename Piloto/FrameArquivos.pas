unit FrameArquivos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.Buttons, Arquivos,
  SpeedButtonLuka, _Biblioteca, _Arquivos, Vcl.ExtCtrls, _Sessao;

type
  TFrArquivos = class(TFrameHerancaPrincipal)
    sbArquivos: TSpeedButtonLuka;
    tiPiscar: TTimer;
    procedure sbArquivosClick(Sender: TObject);
    procedure tiPiscarTimer(Sender: TObject);
  private
    FOrigem: string;
    FId: Integer;

    FArquivos: TArray<RecArquivos>;

    procedure pararPisca;
  public
    procedure Clear; override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
    procedure buscarArquivos;
  published
    property Origem: string read FOrigem write FOrigem;
    property Id: Integer read FId write FId;
    property Arquivos: TArray<RecArquivos> read FArquivos;
  end;

implementation

{$R *.dfm}

procedure TFrArquivos.buscarArquivos;
var
  vIndice: Integer;
begin
  vIndice := -1;
  if FOrigem = 'ORC' then
    vIndice := 0
  else if FOrigem = 'CLI' then
    vIndice := 1
  else if FOrigem = 'CRM' then
    vIndice := 2;

  FArquivos := _Arquivos.BuscarArquivos(Sessao.getConexaoBanco, vIndice, [FId]);
end;

procedure TFrArquivos.Clear;
begin
  inherited;
  FArquivos := nil;
  pararPisca;
end;

procedure TFrArquivos.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([sbArquivos], pEditando);
  if pLimpar then
    FArquivos := nil;

  if not pEditando then
    pararPisca;
end;

procedure TFrArquivos.pararPisca;
begin
  tiPiscar.Enabled := False;
  sbArquivos.NumGlyphs := 2;
end;

procedure TFrArquivos.sbArquivosClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar< TArray<RecArquivos> >;
begin
  inherited;
  vRetTela := verArquivos(FId, FOrigem, FArquivos);
  if vRetTela.BuscaCancelada then
    Exit;

  if FId > 0 then begin
    buscarArquivos;
    Exit;
  end;

  FArquivos := vRetTela.Dados;

  if FArquivos = nil then
    pararPisca
  else
    tiPiscar.Enabled := True;
end;

procedure TFrArquivos.tiPiscarTimer(Sender: TObject);
begin
  inherited;
  sbArquivos.NumGlyphs := IIfInt(sbArquivos.NumGlyphs = 1, 2, 1);
end;

end.
