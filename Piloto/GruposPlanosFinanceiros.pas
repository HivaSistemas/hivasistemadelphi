unit GruposPlanosFinanceiros;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _GruposPlanosFinanceiros, CheckBoxLuka,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FramePlanosFinanceiros,
  Vcl.Grids, GridLuka, SpeedButtonLuka, _ItensGruposPlanosFinanceiros, Vcl.Menus,
  Vcl.ComCtrls;

type
  TFormGruposPlanosFinanceiros = class(TFormHerancaCadastroCodigo)
    lbDepartamento: TLabel;
    eDescricao: TEditLuka;
    edtSubGrupo: TEdit;
    FrPlanoFinanceiro: TFrPlanosFinanceiros;
    gdPlanoFinanceiro: TGridLuka;
    ppmSql: TPopupMenu;
    InserirSQL1: TMenuItem;
    pnlSQL: TPanel;
    Label2: TLabel;
    mmSQL: TMemo;
    SpeedButton1: TSpeedButton;
    btnNextPrev: TUpDown;
    procedure eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); override;
    procedure gdPlanoFinanceiroGetCellColor(Sender: TObject; ARow,
      ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sbDesfazerClick(Sender: TObject);
    procedure FrPlanoFinanceirosgPesquisaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure gdPlanoFinanceiroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sbGravarClick(Sender: TObject);
    procedure sbExcluirClick(Sender: TObject);
    procedure InserirSQL1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure gdPlanoFinanceiroSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure btnNextPrevClick(Sender: TObject; Button: TUDBtnType);
  private
    procedure PreencherRegistro(pRegistro: RecGruposPlanosFinanceiros);
    procedure InserirLinhaGrid(pId, pDescricao, pSQL: String; pTipoLinha: String = 'Normal' );
    function  getItens: TArray<RecItensGruposPlanosFinanceiros>;

  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

uses _Sessao, _Biblioteca,_RecordsEspeciais, PesquisaPlanosFinanceiros, System.Types, System.StrUtils,
  PesquisaGruposPlanosFinanceiros;

{ TFormCadastroPlanosFinanceiros }
const
  coDescricao         = 0;
  coTipoLinha         = 1;
  coPlanoFinanceiroId = 2;
  coSql               = 3;

procedure TFormGruposPlanosFinanceiros.eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  i: Integer;
  vMascara: TStringDynArray;
begin
  if Key <> VK_RETURN then
    Exit;

  eID.Trim;
  if eID.Text = '' then begin
    Modo(True);
    Exit;
  end;

  vMascara := SplitString(eID.Text, '.');
  if Length(vMascara) > 2 then begin
    Exclamar('A m�scara informada est� incorreta!');
    SetarFoco(eID);
    Abort;
  end;

  eID.Text := '';
  for i := Low(vMascara) to High(vMascara) do begin
    if
      (i <> High(vMascara)) and
      (
        (vMascara[i] = '') or
        (vMascara[i] <> RetornaNumeros(vMascara[i]))
      )
    then begin
      Exclamar('A m�scara informada est� incorreta!');
      SetarFoco(eID);
      Abort;
    end;

    if vMascara[i] = '' then
      eID.Text := eID.Text + '.???'
    else
      eID.Text := eID.Text + IfThen(i in[1,2], '.') + IIfStr(i = 0, vMascara[i], LPad(vMascara[i], 2, '0'));
  end;

  inherited;
end;

procedure TFormGruposPlanosFinanceiros.BuscarRegistro;
var
  vId: string;
  vNovo: Boolean;
  vGruposPlanosFinanceiros: TArray<RecGruposPlanosFinanceiros>;
begin
  vNovo := False;

  if Pos('???', eID.Text) > 0 then begin
    vGruposPlanosFinanceiros := _GruposPlanosFinanceiros.BuscarGruposPlanosFinanceiros(Sessao.getConexaoBanco, 0, [Copy(eID.Text, 1, Pos('???', eID.Text) - 2)]);
    vNovo := True;
  end
  else
    vGruposPlanosFinanceiros := _GruposPlanosFinanceiros.BuscarGruposPlanosFinanceiros(Sessao.getConexaoBanco, 0, [eID.Text]);

  if vGruposPlanosFinanceiros = nil then begin
    NenhumRegistro;
    SetarFoco(eID);
    Abort;
  end;

  if vNovo then begin
    vGruposPlanosFinanceiros[0].Descricao := '';
    vGruposPlanosFinanceiros[0].PermiteAlteracao := 'S';
  end;

  vId := eID.Text;
  inherited;

  PreencherRegistro(vGruposPlanosFinanceiros[0]);
  eID.Text := vId;

end;

procedure TFormGruposPlanosFinanceiros.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _GruposPlanosFinanceiros.ExcluirGrupoPlanoFinanceiro(Sessao.getConexaoBanco, eID.Text);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormGruposPlanosFinanceiros.FrPlanoFinanceirosgPesquisaKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  FrPlanoFinanceiro.sgPesquisaKeyDown(Sender, Key, Shift);

  if (key = VK_RETURN) and (not FrPlanoFinanceiro.EstaVazio) then
  begin
    InserirLinhaGrid(FrPlanoFinanceiro.getDados().PlanoFinanceiroId,
                     FrPlanoFinanceiro.getDados().Descricao,'');
    FrPlanoFinanceiro.Clear;
  end;
end;

procedure TFormGruposPlanosFinanceiros.gdPlanoFinanceiroGetCellColor(
  Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush;
  AFont: TFont);
begin
  if gdPlanoFinanceiro.Cells[coTipoLinha, ARow] = 'Cabecalho' then begin
    ABrush.Color := clGray;
    AFont.Color  := clBlack;
  end else
  if gdPlanoFinanceiro.Cells[coTipoLinha, ARow] = 'Soma' then begin
    ABrush.Color := clHighlight;
    AFont.Color  := clWhite;
  end else
  begin
    ABrush.Color := clWhite;
    AFont.Color  := clBlack;
  end;


end;

procedure TFormGruposPlanosFinanceiros.gdPlanoFinanceiroKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (key = VK_DELETE)
  and (gdPlanoFinanceiro.Row > 0)
  and (gdPlanoFinanceiro.Row <> gdPlanoFinanceiro.RowCount -1)
  and Perguntar('Deseja excluir esse plano financeiro: '+ gdPlanoFinanceiro.Cells[coDescricao, gdPlanoFinanceiro.Row]) then
    gdPlanoFinanceiro.DeleteRow(gdPlanoFinanceiro.Row);
end;

procedure TFormGruposPlanosFinanceiros.gdPlanoFinanceiroSelectCell(
  Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  InserirSQL1.Enabled := Em(gdPlanoFinanceiro.Cells[coTipoLinha,ARow ],['Normal','SQL']);
  btnNextPrev.Enabled := InserirSQL1.Enabled;
end;

function TFormGruposPlanosFinanceiros.getItens: TArray<RecItensGruposPlanosFinanceiros>;
var
  i, j: Integer;

begin
  Result := nil;

  for i := 0 to gdPlanoFinanceiro.RowCount - 1 do begin
    if not Em(gdPlanoFinanceiro.Cells[coTipoLinha, i],['Normal','SQL']) then
     Continue;

    SetLength(Result, Length(Result) + 1);
    j := Length(Result);

    Result[j-1].GrupoFinanceiroId   := eID.Text;
    Result[j-1].PlanoFinanceiroId   := gdPlanoFinanceiro.Cells[coPlanoFinanceiroId, i];
    Result[j-1].SQL                 := gdPlanoFinanceiro.Cells[coSql, i];

  end;
end;

procedure TFormGruposPlanosFinanceiros.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  vItens: TArray<RecItensGruposPlanosFinanceiros>;
begin
  inherited;
  vItens := getItens;
  vRetBanco :=
    _GruposPlanosFinanceiros.AtualizarGruposPlanosFinanceiros(
      Sessao.getConexaoBanco,
      eID.Text,
      eDescricao.Text,
      edtSubGrupo.Text,
      vItens);

  if vRetBanco.TeveErro then begin
    Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  if vRetBanco.AsString <> '' then
    Informar(coNovoRegistroSucessoCodigo + vRetBanco.AsString)
  else
    _Biblioteca.Informar(coAlteracaoRegistroSucesso);
end;

procedure TFormGruposPlanosFinanceiros.InserirLinhaGrid(pId,
  pDescricao, pSQL: String; pTipoLinha: String = 'Normal' );
var
  vLinha,
  vLinhaTroca: Integer;
begin
  vLinha := gdPlanoFinanceiro.Localizar([coPlanoFinanceiroId], [pId]);
  if vLinha = -1 then begin

    if gdPlanoFinanceiro.Cells[coPlanoFinanceiroId, 0] = '' then
      vLinha := 0
    else begin
      vLinha := gdPlanoFinanceiro.RowCount;
      gdPlanoFinanceiro.RowCount := gdPlanoFinanceiro.RowCount + 1;
    end;
  end else
  begin
    gdPlanoFinanceiro.Row := vLinha;
    Avisar('Aviso','Plano financeiro j� inserido!');
    exit;
  end;

  gdPlanoFinanceiro.Cells[coPlanoFinanceiroId, vLinha]        := pId;
  gdPlanoFinanceiro.Cells[coDescricao,         vLinha]        := pDescricao;
  gdPlanoFinanceiro.Cells[coSQL,               vLinha]        := pSQL;
  gdPlanoFinanceiro.Cells[coTipoLinha,         vLinha]        := Iif(pSql.IsEmpty,pTipoLinha,'SQL');

  vLinhaTroca := gdPlanoFinanceiro.Localizar([coTipoLinha], ['Soma']);
  if (vLinhaTroca > 0) and (pTipoLinha = 'Normal') then
  begin
    gdPlanoFinanceiro.TrocaRow(vLinha,vLinhaTroca);
  end;

end;

procedure TFormGruposPlanosFinanceiros.InserirSQL1Click(Sender: TObject);
begin
  inherited;
  pnlSQL.Left := gdPlanoFinanceiro.Left + 50;
  pnlSQL.Top := FrPlanoFinanceiro.Top;

  mmSQL.Lines.Clear;
  mmSQL.Lines.Text := gdPlanoFinanceiro.Cells[coSql, gdPlanoFinanceiro.Row];
  pnlSQL.Visible := True;
end;

procedure TFormGruposPlanosFinanceiros.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([eDescricao],pEditando);

  if pEditando then begin
    SetarFoco(eDescricao);
  end else
  begin
    edtSubGrupo.Clear;
    pnlSQL.Visible := False;
  end;
;
end;

procedure TFormGruposPlanosFinanceiros.PesquisarRegistro;
var
  vGruposPlanoFinanceiro: RecGruposPlanosFinanceiros;
begin
  vGruposPlanoFinanceiro := RecGruposPlanosFinanceiros(PesquisaGruposPlanosFinanceiros.Pesquisar(False, False));
  if vGruposPlanoFinanceiro = nil then
    Exit;

  inherited;
  PreencherRegistro(vGruposPlanoFinanceiro);
end;

procedure TFormGruposPlanosFinanceiros.PreencherRegistro(pRegistro: RecGruposPlanosFinanceiros);
var
  vMascara: TStringDynArray;
  vItensGruposPlanosFinanceiros: TArray<RecItensGruposPlanosFinanceiros>;
  vGruposPlanosFinanceiros: TArray<RecGruposPlanosFinanceiros>;
  vCnt: Integer;
begin
  eID.Text               := pRegistro.GrupoFinanceiroId;
  eDescricao.Text        := pRegistro.Descricao;
  edtSubGrupo.Text       := pRegistro.GrupoFinanceiroPaiId;
  if pRegistro.PermiteAlteracao = 'N' then begin
    eDescricao.Enabled := False;
  end;

  vMascara := SplitString(eID.Text, '.');
  if (Length(vMascara) = 1) and (Perguntar('Deseja incluir um subgrupo?')) then
  begin
    edtSubGrupo.Text := eID.Text;
    eID.Text := '';
    _Biblioteca.Habilitar([
      gdPlanoFinanceiro,
      FrPlanoFinanceiro],
      False
    );
    Modo(True);

    Abort;
  end else
  begin
    if (Length(vMascara) = 2) then
    begin
      _Biblioteca.Habilitar([
        gdPlanoFinanceiro,
        FrPlanoFinanceiro],
        True
      );

      InserirLinhaGrid(pRegistro.GrupoFinanceiroId, pRegistro.Descricao,'','Cabecalho');

      vItensGruposPlanosFinanceiros := _ItensGruposPlanosFinanceiros.BuscarItensGruposPlanosFinanceiros(Sessao.getConexaoBanco, 0, [eID.Text]);
      for vCnt := Low(vItensGruposPlanosFinanceiros) to High(vItensGruposPlanosFinanceiros) do
      begin
        InserirLinhaGrid(vItensGruposPlanosFinanceiros[vCnt].PlanoFinanceiroId,
                         vItensGruposPlanosFinanceiros[vCnt].Descricao,
                         vItensGruposPlanosFinanceiros[vCnt].SQL);
      end;
      vGruposPlanosFinanceiros := _GruposPlanosFinanceiros.BuscarGruposPlanosFinanceiros(Sessao.getConexaoBanco, 0, [pRegistro.GrupoFinanceiroPaiId]);
      InserirLinhaGrid(vGruposPlanosFinanceiros[0].GrupoFinanceiroId, vGruposPlanosFinanceiros[0].Descricao,'','Soma');

    end;
  end;

  pRegistro.Free;
end;

procedure TFormGruposPlanosFinanceiros.sbDesfazerClick(Sender: TObject);
begin
  inherited;
    _Biblioteca.Habilitar([
      gdPlanoFinanceiro,
      FrPlanoFinanceiro],
      False
    );

end;

procedure TFormGruposPlanosFinanceiros.sbExcluirClick(Sender: TObject);
begin
  if Assigned(_GruposPlanosFinanceiros.BuscarGruposPlanosFinanceiros(Sessao.getConexaoBanco, 4, [eID.Text])) then
  begin
    Exclamar('Este grupo tem subgrupo, apague-os primeiro!');
    exit;
  end;

  inherited;
    _Biblioteca.Habilitar([
      gdPlanoFinanceiro,
      FrPlanoFinanceiro],
      False
    );
end;

procedure TFormGruposPlanosFinanceiros.sbGravarClick(Sender: TObject);
begin
  inherited;
    _Biblioteca.Habilitar([
      gdPlanoFinanceiro,
      FrPlanoFinanceiro],
      False
    );

end;

procedure TFormGruposPlanosFinanceiros.SpeedButton1Click(Sender: TObject);
begin
  inherited;
  pnlSQL.Visible := False;
  gdPlanoFinanceiro.Cells[coSql, gdPlanoFinanceiro.Row] := mmSQL.Text;
  gdPlanoFinanceiro.Cells[coTipoLinha, gdPlanoFinanceiro.Row] := Iif(mmSQL.Lines.Text.IsEmpty,'Normal','SQL');
end;

procedure TFormGruposPlanosFinanceiros.btnNextPrevClick(Sender: TObject;
  Button: TUDBtnType);
begin
  inherited;

  if Button = btNext then
  begin
    if ((gdPlanoFinanceiro.Row - 1) = 0)then
      exit;
    gdPlanoFinanceiro.TrocaRow(gdPlanoFinanceiro.Row , gdPlanoFinanceiro.Row - 1);
    gdPlanoFinanceiro.Row := gdPlanoFinanceiro.Row - 1;
  end else
  begin
    if ((gdPlanoFinanceiro.Row + 1) = gdPlanoFinanceiro.RowCount-1) then
      exit;

    gdPlanoFinanceiro.TrocaRow(gdPlanoFinanceiro.Row,gdPlanoFinanceiro.Row + 1);
    gdPlanoFinanceiro.Row := gdPlanoFinanceiro.Row + 1;
  end;

end;

procedure TFormGruposPlanosFinanceiros.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eDescricao.Trim = '' then begin
    Exclamar('� necess�rio informar a descri��o!');
    SetarFoco(eDescricao);
    Abort;
  end;
end;

end.
