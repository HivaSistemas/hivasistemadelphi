inherited FormCadastroCondicoesPagamento: TFormCadastroCondicoesPagamento
  Caption = 'Tipos de pagamentos'
  ClientHeight = 541
  ClientWidth = 657
  ExplicitWidth = 663
  ExplicitHeight = 570
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 211
    Top = 4
    Width = 32
    Height = 14
    Caption = 'Nome'
  end
  object lb2: TLabel [1]
    Left = 593
    Top = 98
    Width = 57
    Height = 14
    Caption = 'Ind. venda'
  end
  object Label2: TLabel [2]
    Left = 278
    Top = 51
    Width = 104
    Height = 14
    Caption = 'Valor m'#237'n.da venda'
  end
  object Label3: TLabel [4]
    Left = 270
    Top = 98
    Width = 40
    Height = 14
    Caption = '% Juros'
  end
  object Label6: TLabel [5]
    Left = 480
    Top = 98
    Width = 63
    Height = 14
    Hint = '% Somente para calculo do ind'#237'ce da condi'#231#227'o de pagamento'
    Caption = '% Desconto'
  end
  object Label8: TLabel [6]
    Left = 570
    Top = 115
    Width = 8
    Height = 19
    Caption = '='
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel [7]
    Left = 126
    Top = 221
    Width = 74
    Height = 14
    Caption = 'Tipo de pre'#231'o'
  end
  object lb4: TLabel [8]
    Left = 201
    Top = 98
    Width = 36
    Height = 14
    Caption = '% Taxa'
  end
  object lb5: TLabel [9]
    Left = 395
    Top = 51
    Width = 84
    Height = 14
    Caption = 'Valor min. parc.'
  end
  object lb6: TLabel [10]
    Left = 126
    Top = 98
    Width = 67
    Height = 14
    Caption = 'Prazo m'#233'dio'
  end
  object lb3: TLabel [11]
    Left = 488
    Top = 51
    Width = 96
    Height = 14
    Caption = '% desc.m'#225'x.perm.'
  end
  object lb7: TLabel [12]
    Left = 320
    Top = 98
    Width = 137
    Height = 14
    Caption = '% Deprecia'#231#227'o financeira'
  end
  inherited pnOpcoes: TPanel
    Height = 541
    TabOrder = 19
    ExplicitHeight = 541
    inherited sbGravar: TSpeedButton
      Left = 1
      ExplicitLeft = 1
    end
    inherited sbDesfazer: TSpeedButton
      Left = 1
      ExplicitLeft = 1
    end
    inherited sbExcluir: TSpeedButton
      Left = 1
      Top = 156
      ExplicitLeft = 1
      ExplicitTop = 156
    end
    inherited sbPesquisar: TSpeedButton
      Left = 1
      Top = 47
      ExplicitLeft = 1
      ExplicitTop = 47
    end
    inherited sbLogs: TSpeedButton
      Left = 1
      Top = 490
      Visible = True
      ExplicitLeft = 1
      ExplicitTop = 490
    end
  end
  inherited eID: TEditLuka
    TabOrder = 0
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 592
    Top = 0
    TabOrder = 20
    ExplicitLeft = 592
    ExplicitTop = 0
  end
  object eNome: TEditLuka
    Left = 211
    Top = 19
    Width = 384
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 1
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eIndiceAcrescimo: TEditLuka
    Left = 593
    Top = 112
    Width = 57
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clTeal
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    MaxLength = 6
    ParentFont = False
    TabOrder = 11
    Text = '0,0000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 4
    NaoAceitarEspaco = False
  end
  inline FrTiposCobranca: TFrTiposCobranca
    Left = 126
    Top = 139
    Width = 347
    Height = 81
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 17
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 139
    ExplicitWidth = 347
    inherited sgPesquisa: TGridLuka
      Width = 322
      ExplicitWidth = 322
    end
    inherited CkPesquisaNumerica: TCheckBox
      Width = 129
      ExplicitWidth = 129
    end
    inherited PnTitulos: TPanel
      Width = 347
      ExplicitWidth = 347
      inherited lbNomePesquisa: TLabel
        Width = 267
        Caption = 'Tipos de cobran'#231'a habilitada para esta condi'#231#227'o'
        ExplicitWidth = 267
        ExplicitHeight = 14
      end
      inherited CkChaveUnica: TCheckBox
        Left = 296
        Checked = False
        State = cbUnchecked
        ExplicitLeft = 296
      end
      inherited pnSuprimir: TPanel
        Left = 242
        ExplicitLeft = 242
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 322
      ExplicitLeft = 322
    end
    inherited ckSomenteAtivos: TCheckBox
      Width = 105
      Checked = True
      State = cbChecked
      ExplicitWidth = 105
    end
  end
  object rgTipoCondicao: TRadioGroupLuka
    Left = 126
    Top = 48
    Width = 147
    Height = 40
    Caption = ' Tipo de condi'#231#227'o'
    Columns = 2
    Items.Strings = (
      'A vista'
      'A prazo')
    TabOrder = 2
    TabStop = True
    Valores.Strings = (
      'A'
      'P')
  end
  object eValorMinimoVenda: TEditLuka
    Left = 278
    Top = 65
    Width = 112
    Height = 22
    Hint = 'Valor m'#237'nimo da venda'
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 4
    TabOrder = 3
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object ePercentualJuros: TEditLuka
    Left = 270
    Top = 112
    Width = 42
    Height = 22
    Hint = 'Percentual de desconto permitido na venda'
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 4
    TabOrder = 8
    Text = '0,00'
    OnChange = ePercentualJurosChange
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 2
    ValorMaximo = 99.990000000000000000
    NaoAceitarEspaco = False
  end
  object ePercentualDescontoComercial: TEditLuka
    Left = 480
    Top = 112
    Width = 80
    Height = 22
    Hint = '% Somente para calculo do ind'#237'ce da condi'#231#227'o de pagamento'
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 4
    TabOrder = 9
    Text = '0,00'
    OnChange = ePercentualDescontoComercialChange
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 2
    ValorMaximo = 99.990000000000000000
    NaoAceitarEspaco = False
  end
  object ckExigirNotaFiscal: TCheckBox
    Left = 126
    Top = 263
    Width = 227
    Height = 17
    Caption = '1 - Exigir modelo nota fiscal'
    Enabled = False
    TabOrder = 12
    OnKeyDown = ProximoCampo
  end
  object ckAplicarIndicePrecoPromocional: TCheckBox
    Left = 126
    Top = 280
    Width = 227
    Height = 17
    Caption = '2 - Aplica '#237'ndice no pre'#231'o promocional'
    Enabled = False
    TabOrder = 13
    OnKeyDown = ProximoCampo
  end
  object ckPermitirUsoPontaEstoque: TCheckBox
    Left = 126
    Top = 297
    Width = 227
    Height = 17
    Caption = '3 - Permitir uso de ponta de estoque'
    Enabled = False
    TabOrder = 14
    OnKeyDown = ProximoCampo
  end
  object ckPermitirUsoFechamentoAcum: TCheckBox
    Left = 126
    Top = 314
    Width = 227
    Height = 17
    Caption = '4 - Permitir uso no fec. de acumulado'
    Enabled = False
    TabOrder = 15
    OnKeyDown = ProximoCampo
  end
  object cbTipoPreco: TComboBoxLuka
    Left = 126
    Top = 235
    Width = 163
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = 16645629
    TabOrder = 16
    OnKeyDown = ProximoCampo
    Items.Strings = (
      'Varejo'
      'Atacado'
      'Ambos'
      'Ambos ign.regra cliente')
    Valores.Strings = (
      'V'
      'A'
      'T'
      'I')
    AsInt = 0
  end
  object ePercEncargos: TEditLuka
    Left = 201
    Top = 112
    Width = 65
    Height = 22
    Hint = 'Percentual de desconto permitido na venda'
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 4
    TabOrder = 7
    Text = '0,00'
    OnChange = ePercEncargosChange
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 2
    ValorMaximo = 99.990000000000000000
    NaoAceitarEspaco = False
  end
  object eValorMinimoParcela: TEditLuka
    Left = 395
    Top = 65
    Width = 87
    Height = 22
    Hint = 'Valor m'#237'nimo de cada parcela'
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 4
    TabOrder = 4
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  inline FrDiasPrazo: TFrNumeros
    Left = 485
    Top = 139
    Width = 145
    Height = 81
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 18
    TabStop = True
    ExplicitLeft = 485
    ExplicitTop = 139
    ExplicitWidth = 145
    ExplicitHeight = 81
    inherited sgNumeros: TGridLuka
      Width = 145
      Height = 67
      ExplicitWidth = 145
      ExplicitHeight = 67
    end
    inherited pnDescricao: TPanel
      Width = 145
      Caption = ' Dias de prazo ( Calc. ind. )'
      ExplicitWidth = 145
    end
  end
  object ePrazoMedio: TEditLuka
    Left = 126
    Top = 112
    Width = 71
    Height = 22
    Hint = 'Prazo m'#233'dio calculado'
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 4
    ReadOnly = True
    TabOrder = 6
    OnChange = ePercEncargosChange
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
    ValorMaximo = 99.990000000000000000
    NaoAceitarEspaco = False
  end
  object ePercentualDescontoPermitidoVenda: TEditLuka
    Left = 488
    Top = 65
    Width = 107
    Height = 22
    Hint = 'Percentual de desconto m'#225'ximo permitido na venda'
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 4
    TabOrder = 5
    Text = '0,00'
    OnChange = ePercentualJurosChange
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 2
    ValorMaximo = 99.990000000000000000
    NaoAceitarEspaco = False
  end
  object ePercCustoVenda: TEditLuka
    Left = 318
    Top = 112
    Width = 145
    Height = 22
    Hint = 'Percentual de desconto permitido na venda'
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 4
    TabOrder = 10
    Text = '0,00'
    OnChange = ePercentualJurosChange
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 2
    ValorMaximo = 99.990000000000000000
    NaoAceitarEspaco = False
  end
  object ckRestrita: TCheckBoxLuka
    Left = 126
    Top = 364
    Width = 238
    Height = 17
    Caption = '7 - Especial'
    TabOrder = 21
    CheckedStr = 'N'
  end
  object ckBloquearSempre: TCheckBoxLuka
    Left = 357
    Top = 263
    Width = 238
    Height = 17
    Caption = '8 - Bloquear sempre'
    TabOrder = 22
    CheckedStr = 'N'
  end
  object ckPermitirPrecoProcional: TCheckBox
    Left = 126
    Top = 331
    Width = 228
    Height = 17
    Caption = '5 - Permitir pre'#231'o promocional'
    Enabled = False
    TabOrder = 23
    OnKeyDown = ProximoCampo
  end
  object ckNaoPermitirConsFinal: TCheckBoxLuka
    Left = 126
    Top = 347
    Width = 228
    Height = 17
    Caption = '6 - N'#227'o permitir consumidor final'
    TabOrder = 24
    CheckedStr = 'N'
  end
  object ckRecebimentoNaEntrega: TCheckBoxLuka
    Left = 357
    Top = 280
    Width = 238
    Height = 17
    Caption = '9 - Permitir recebimento na entrega'
    TabOrder = 25
    CheckedStr = 'N'
  end
  object ckAcumulativa: TCheckBoxLuka
    Left = 357
    Top = 297
    Width = 238
    Height = 17
    Caption = '10 - Acumulado'
    TabOrder = 26
    OnClick = ckAcumulativaClick
    CheckedStr = 'N'
  end
  object ckImprimirConfissaoDivida: TCheckBoxLuka
    Left = 357
    Top = 314
    Width = 238
    Height = 17
    Caption = '11 - Imprimir confiss'#227'o de d'#237'vida'
    TabOrder = 27
    OnClick = ckAcumulativaClick
    CheckedStr = 'N'
  end
  inline FrEmpresas: TFrEmpresas
    Left = 129
    Top = 392
    Width = 523
    Height = 143
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 28
    TabStop = True
    ExplicitLeft = 129
    ExplicitTop = 392
    ExplicitWidth = 523
    ExplicitHeight = 143
    inherited sgPesquisa: TGridLuka
      Width = 498
      Height = 126
      ExplicitWidth = 498
      ExplicitHeight = 143
    end
    inherited CkMultiSelecao: TCheckBox
      Checked = True
      State = cbChecked
    end
    inherited PnTitulos: TPanel
      Width = 523
      ExplicitWidth = 523
      inherited lbNomePesquisa: TLabel
        Width = 53
        ExplicitWidth = 53
        ExplicitHeight = 14
      end
      inherited CkChaveUnica: TCheckBox
        Checked = False
        State = cbUnchecked
      end
      inherited pnSuprimir: TPanel
        Left = 418
        ExplicitLeft = 418
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 498
      Height = 127
      ExplicitLeft = 498
      ExplicitHeight = 144
    end
  end
  object ckImprimirCompFaturamentoVenda: TCheckBoxLuka
    Left = 357
    Top = 330
    Width = 248
    Height = 17
    Caption = '12 - Imprimir comp. faturamento de venda'
    TabOrder = 29
    OnClick = ckAcumulativaClick
    CheckedStr = 'N'
  end
  object ckExportarWeb: TCheckBox
    Left = 357
    Top = 349
    Width = 159
    Height = 17
    Caption = '13 - Exportar para web'
    Enabled = False
    TabOrder = 30
    OnKeyDown = ProximoCampo
  end
end
