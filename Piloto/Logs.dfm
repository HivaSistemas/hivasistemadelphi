inherited FormLogs: TFormLogs
  Caption = 'Logs'
  ClientHeight = 451
  ClientWidth = 821
  ExplicitWidth = 827
  ExplicitHeight = 480
  PixelsPerInch = 96
  TextHeight = 14
  object Label1: TLabel [0]
    Left = 3
    Top = 3
    Width = 37
    Height = 14
    Caption = 'Campo'
  end
  object sbCarregar: TSpeedButtonLuka [1]
    Left = 619
    Top = 1
    Width = 117
    Height = 40
    Caption = '&Carregar'
    Flat = True
    OnClick = sbCarregarClick
    TagImagem = 10
    PedirCertificacao = False
    PermitirAutOutroUsuario = False
  end
  inherited pnOpcoes: TPanel
    Top = 414
    Width = 821
    ExplicitTop = 414
    ExplicitWidth = 821
  end
  object cbCampos: TComboBoxLuka
    Left = 1
    Top = 17
    Width = 291
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    TabOrder = 1
    OnChange = cbCamposChange
    AsInt = 0
  end
  inline FrEmpresa: TFrEmpresas
    Left = 300
    Top = -1
    Width = 320
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 300
    ExplicitTop = -1
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Height = 24
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      inherited lbNomePesquisa: TLabel
        Width = 47
        Caption = 'Empresa'
        ExplicitWidth = 47
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Height = 25
      ExplicitHeight = 25
    end
  end
  object sgLogs: TGridLuka
    Left = 1
    Top = 41
    Width = 818
    Height = 372
    ColCount = 7
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
    TabOrder = 3
    OnDrawCell = sgLogsDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Campo'
      'Valor anterior'
      'Novo valor'
      'Empresa'
      'Usuario altera'#231#227'o'
      'Data/hora altera'#231#227'o'
      'Rotina')
    Grid3D = False
    RealColCount = 7
    AtivarPopUpSelecao = False
    ColWidths = (
      208
      90
      72
      152
      134
      118
      126)
  end
end
