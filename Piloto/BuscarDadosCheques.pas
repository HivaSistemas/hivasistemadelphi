unit BuscarDadosCheques;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, EditTelefoneLuka, _Biblioteca,
  Vcl.Grids, GridLuka, Vcl.Mask, EditLukaData, _FrameHerancaPrincipal, System.Math,
  _FrameHenrancaPesquisas, FrameTiposCobranca, Vcl.StdCtrls, EditLuka, System.DateUtils,
  Vcl.Buttons, Vcl.ExtCtrls, _RecordsFinanceiros, _RecordsCadastros, _TiposCobrancaDiasPrazo,
  _Sessao, FrameDadosCobranca, StaticTextLuka;

type
  TFormBuscarDadosCheque = class(TFormHerancaFinalizar)
    stCondicaoPagamento: TStaticText;
    FrDadosCobranca: TFrDadosCobranca;
    stNomeCondicao: TStaticTextLuka;
    procedure FormShow(Sender: TObject);
  private
    FSomenteLeitura: Boolean;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function BuscarCheques(
  const pCondicaoId: Integer;
  const pNomeCondicao: string;
  const pValorABuscar: Double;
  const pCheques: TArray<RecTitulosFinanceiros>;
  const pSomenteLeitura: Boolean
): TRetornoTelaFinalizar< TArray<RecTitulosFinanceiros> >;

implementation

{$R *.dfm}

const
  co_data_vencimento      = 0;
  co_banco                = 1;
  co_agencia              = 2;
  co_conta_corrente       = 3;
  co_numero_cheque        = 4;
  co_valor_cheque         = 5;
  co_nome_emitente_cheque = 6;
  co_telefone_emitente    = 7;
  co_tipo_cobranca        = 8;
  co_parcela              = 9;
  co_qtde_parcelas        = 10;
  co_tipo_cobranca_id     = 11;
  coUtilizarLimiteCred    = 12;

function BuscarCheques(
  const pCondicaoId: Integer;
  const pNomeCondicao: string;
  const pValorABuscar: Double;
  const pCheques: TArray<RecTitulosFinanceiros>;
  const pSomenteLeitura: Boolean
): TRetornoTelaFinalizar< TArray<RecTitulosFinanceiros> >;
var
  vForm: TFormBuscarDadosCheque;
begin
  Result.Dados := nil;

  vForm := TFormBuscarDadosCheque.Create(Application);

  vForm.FSomenteLeitura := pSomenteLeitura;
  vForm.FrDadosCobranca.Modo(not pSomenteLeitura);

  vForm.FrDadosCobranca.Natureza := 'R';
  vForm.FrDadosCobranca.FormaPagamento := 'CHQ';
  vForm.FrDadosCobranca.FrTiposCobranca.setCondicaoId(pCondicaoId);
  vForm.FrDadosCobranca.ValorPagar := pValorABuscar;

  if pCondicaoId > 0 then
    vForm.stCondicaoPagamento.Caption := NFormat(pCondicaoId) + ' - ' + pNomeCondicao
  else if pNomeCondicao <> '' then
    vForm.stCondicaoPagamento.Caption := pNomeCondicao;

  vForm.FrDadosCobranca.Titulos := pCheques;

  if pSomenteLeitura then
    vForm.BorderIcons := [];

  if Result.Ok(vForm.ShowModal, False) then
    Result.Dados := vForm.FrDadosCobranca.Titulos;

  FreeAndNil(vForm);
end;

procedure TFormBuscarDadosCheque.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrDadosCobranca);
end;

procedure TFormBuscarDadosCheque.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrDadosCobranca.ExisteDiferenca then begin
    _Biblioteca.Exclamar('Existe uma diferenša nos valores, por favor verifique!');
    Abort;
  end;
end;

end.
