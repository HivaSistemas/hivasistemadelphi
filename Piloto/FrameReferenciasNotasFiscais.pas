unit FrameReferenciasNotasFiscais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Frame.HerancaInsercaoExclusao, _Biblioteca, _NotasFiscaisReferencias,
  Vcl.Menus, Vcl.StdCtrls, StaticTextLuka, Vcl.Grids, GridLuka, BuscarReferenciaNFe;

type
  TFrReferenciasNotasFiscais = class(TFrameHerancaInsercaoExclusao)
    procedure sgValoresDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  private
    function getReferencias: TArray<RecNotasFiscaisReferencias>;
    procedure setReferencias(pValores: TArray<RecNotasFiscaisReferencias>);
  public
    function EstaVazio: Boolean; override;
  protected
    procedure Inserir; override;
  published
    property Referencias: TArray<RecNotasFiscaisReferencias> read getReferencias write setReferencias;
  end;

implementation

{$R *.dfm}

{ TFrReferenciasNotasFiscais }

const
  coTipoDocumento     = 0;
  coChaveNFe          = 1;
  coTipoDocumentoSint = 2;

function TFrReferenciasNotasFiscais.EstaVazio: Boolean;
begin
  Result := sgValores.Cells[coTipoDocumento, sgValores.Row] = '';
end;

function TFrReferenciasNotasFiscais.getReferencias: TArray<RecNotasFiscaisReferencias>;
var
  i: Integer;
begin
  Result := nil;

  if sgValores.Cells[coTipoDocumento, 1] = '' then
    Exit;

  SetLength(Result, sgValores.RowCount -1);
  for i := 1 to sgValores.RowCount -1 do begin
    Result[i - 1].TipoReferencia       := 'M';
    Result[i - 1].TipoNota             := sgValores.Cells[coTipoDocumentoSint, i];
    Result[i - 1].ChaveNfeReferenciada := RetornaNumeros(sgValores.Cells[coChaveNFe, i]);
  end;
end;

procedure TFrReferenciasNotasFiscais.Inserir;
var
  vLinha: Integer;
  vRetTela: TRetornoTelaFinalizar<RecReferenciaNFe>;
begin
  inherited;

  vRetTela := BuscarReferenciaNFe.getDados;
  if vRetTela.BuscaCancelada then
    Exit;

  if sgValores.Cells[coTipoDocumento, 1] = '' then
    vLinha := 1
  else begin
    vLinha := sgValores.RowCount;
    sgValores.RowCount := sgValores.RowCount + 1;
  end;

  sgValores.Cells[coTipoDocumento, vLinha]     := IIfStr(vRetTela.Dados.TipoDocumento = 'N', 'NFe', 'NFCe');
  sgValores.Cells[coChaveNFe, vLinha]          := RetornaNumeros(vRetTela.Dados.ChaveNFe);
  sgValores.Cells[coTipoDocumentoSint, vLinha] := vRetTela.Dados.TipoDocumento;
end;

procedure TFrReferenciasNotasFiscais.setReferencias(pValores: TArray<RecNotasFiscaisReferencias>);
var
  i: Integer;
begin
  for i := Low(pValores) to High(pValores) do begin
    sgValores.Cells[coTipoDocumento, i + 1]     := IIfStr(pValores[i].TipoNota = 'N', 'NFe', 'NFCe');
    sgValores.Cells[coChaveNFe, i + 1]          := RetornaNumeros(pValores[i].ChaveNfeReferenciada);
    sgValores.Cells[coTipoDocumentoSint, i + 1] := pValores[i].TipoNota;
  end;
  sgValores.SetLinhasGridPorTamanhoVetor( Length(pValores) );
end;

procedure TFrReferenciasNotasFiscais.sgValoresDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  inherited;
  sgValores.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taCenter, Rect);
end;

end.
