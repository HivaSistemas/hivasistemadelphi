unit BuscarCreditosDisponiveis;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Grids, GridLuka, _Biblioteca, _ContasReceber,
  Vcl.Buttons, Vcl.ExtCtrls, _ContasPagar, _Sessao, _RecordsFinanceiros, _Imagens, Informacoes.TituloPagar;

type
  RecCreditos = record
    ValorTotal: Double;
    Creditos: TArray<Integer>;
  end;

  TFormBuscarCreditosDisponiveis = class(TFormHerancaFinalizar)
    sgCreditos: TGridLuka;
    procedure sgCreditosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgCreditosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure sgCreditosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgCreditosDblClick(Sender: TObject);
    procedure sgCreditosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  end;

function Buscar(
  pCadastroId: Integer;
  pCadastroIds: TArray<Integer>;
  pNatureza: string;
  pFechamentoAcumulado: Boolean;
  pCreditosAdiantamentosAcumuladosIds: TArray<Integer>
): TRetornoTelaFinalizar< RecCreditos >;

implementation

{$R *.dfm}

uses
  Informacoes.TituloReceber;

const
  coSelecionado     = 0;
  coCodigo          = 1;
  coTipo            = 2;
  coDataCadastro    = 3;
  coDocumento       = 4;
  coValor           = 5;
  coIndiceCondPagto = 6;
  coDataVencimento  = 7;
  coCliente         = 8;
  coBloqueado       = 9;

  (* Ocutlas *)
  coNatureza        = 10;
  coProvAdiantAcumu = 11;

function Buscar(
  pCadastroId: Integer;
  pCadastroIds: TArray<Integer>;
  pNatureza: string;
  pFechamentoAcumulado: Boolean;
  pCreditosAdiantamentosAcumuladosIds: TArray<Integer>
): TRetornoTelaFinalizar< RecCreditos >;
var
  i: Integer;
  vComando: string;

  vPagar: TArray<RecContaPagar>;
  vReceber: TArray<RecContasReceber>;

  vForm: TFormBuscarCreditosDisponiveis;
begin
  Result.Dados.Creditos := nil;

  if (pCadastroIds = nil) and (pCadastroId = 0) then
    Exit;

  (* Natureza, R - Recebimentos, P - Pagamentos *)

  if pNatureza = 'R' then begin
    if not pFechamentoAcumulado then begin
      if pCadastroIds = nil then begin
        vPagar :=
          _ContasPagar.BuscarContasPagar(
            Sessao.getConexaoBanco,
            2,
            [pCadastroId, Sessao.getParametros.TipoCobrancaGeracaoCredId, Sessao.getParametros.TipoCobrancaGerCredImpId]
          );
      end
      else begin
        vComando :=
          'where ' +  _Biblioteca.FiltroInInt('COP.CADASTRO_ID', pCadastroIds) + ' ' +
          'and COP.STATUS = ''A'' ' +
          'and (COP.COBRANCA_ID = ' + IntToStr(Sessao.getParametros.TipoCobrancaGeracaoCredId) + ' or COP.COBRANCA_ID = ' + IntToStr(Sessao.getParametros.TipoCobrancaGerCredImpId) + ') ' +
          'and COP.ORIGEM not in(''ADA'') ' + // N�o trazer os cr�ditos de adiantamentos de acumulados
          'order by ' +
          '  COP.PAGAR_ID ';

        vPagar :=
          _ContasPagar.BuscarContasPagarComando(
            Sessao.getConexaoBanco,
            vComando
          );
      end;
    end
    else begin
      vPagar :=
        _ContasPagar.BuscarContasPagar(
          Sessao.getConexaoBanco,
          7,
          [pCadastroId, Sessao.getParametros.TipoCobrancaGeracaoCredId, Sessao.getParametros.TipoCobrancaGerCredImpId]
        );
    end;

    if vPagar = nil then begin
      _Biblioteca.NenhumRegistro;
      Exit;
    end;
  end
  else begin
    vReceber := _ContasReceber.BuscarContasReceber(Sessao.getConexaoBanco, 6, [pCadastroId, Sessao.getParametros.TipoCobrancaGeracaoCredId, Sessao.getParametros.TipoCobrancaGerCredImpId]);
    if vReceber = nil then begin
      _Biblioteca.NenhumRegistro;
      Exit;
    end;
  end;

  vForm := TFormBuscarCreditosDisponiveis.Create(nil);

  if pNatureza = 'R' then begin
    for i := Low(vPagar) to High(vPagar) do begin
      vForm.sgCreditos.Cells[coSelecionado, i + 1]  := charNaoSelecionado;
      vForm.sgCreditos.Cells[coCodigo, i + 1]       := NFormat(vPagar[i].PagarId);
      vForm.sgCreditos.Cells[coTipo, i + 1]         := 'A pagar';
      vForm.sgCreditos.Cells[coDataCadastro, i + 1] := DFormat(vPagar[i].data_cadastro);
      vForm.sgCreditos.Cells[coDocumento, i + 1]    := vPagar[i].documento;
      vForm.sgCreditos.Cells[coValor, i + 1]        := NFormat(vPagar[i].ValorSaldo);

      vForm.sgCreditos.Cells[coIndiceCondPagto, i + 1] := NFormatN(vPagar[i].IndiceCondicaoPagamento, 5);
      vForm.sgCreditos.Cells[coDataVencimento, i + 1]  := DFormat(vPagar[i].data_vencimento);
      vForm.sgCreditos.Cells[coCliente, i + 1]         := NFormat(vPagar[i].CadastroId) + ' - ' + vPagar[i].nome_fornecedor;
      vForm.sgCreditos.Cells[coBloqueado, i + 1]       := _Biblioteca.SimNao(vPagar[i].bloqueado);

      vForm.sgCreditos.Cells[coNatureza, i + 1]     := pNatureza;

      if Em(vPagar[i].PagarId, pCreditosAdiantamentosAcumuladosIds) then begin
        vForm.sgCreditos.Cells[coProvAdiantAcumu, i + 1] := 'S';
        vForm.sgCreditos.Cells[coSelecionado, i + 1]     := charSelecionado;
      end;
    end;
    vForm.sgCreditos.SetLinhasGridPorTamanhoVetor( Length(vPagar) );
  end
  else begin
    vForm.sgCreditos.OcultarColunas([coIndiceCondPagto]);
    for i := Low(vReceber) to High(vReceber) do begin
      vForm.sgCreditos.Cells[coSelecionado, i + 1]  := charNaoSelecionado;
      vForm.sgCreditos.Cells[coCodigo, i + 1]       := NFormat(vReceber[i].ReceberId);
      vForm.sgCreditos.Cells[coTipo, i + 1]         := 'A receber';
      vForm.sgCreditos.Cells[coDataCadastro, i + 1] := DFormat(vReceber[i].data_cadastro);
      vForm.sgCreditos.Cells[coDocumento, i + 1]    := vReceber[i].documento;
      vForm.sgCreditos.Cells[coValor, i + 1]        := NFormat(vReceber[i].ValorDocumento);

      vForm.sgCreditos.Cells[coIndiceCondPagto, i + 1] := '1,00000';
      vForm.sgCreditos.Cells[coDataVencimento, i + 1]  := DFormat(vReceber[i].data_vencimento);
      vForm.sgCreditos.Cells[coCliente, i + 1]         := NFormat(vReceber[i].CadastroId) + ' - ' + vReceber[i].NomeCliente;
      vForm.sgCreditos.Cells[coBloqueado, i + 1]       := 'N�o';

      vForm.sgCreditos.Cells[coNatureza, i + 1]        := pNatureza;
      vForm.sgCreditos.Cells[coProvAdiantAcumu, i + 1] := 'N';
    end;
    vForm.sgCreditos.SetLinhasGridPorTamanhoVetor( Length(vReceber) );
  end;

  if Result.Ok(vForm.ShowModal) then begin
    Result.Dados.ValorTotal := 0;
    for i := 1 to vForm.sgCreditos.RowCount -1 do begin
      if vForm.sgCreditos.Cells[coSelecionado, i] = charSelecionado then begin
        SetLength(Result.Dados.Creditos, Length(Result.Dados.Creditos) + 1);
        Result.Dados.Creditos[High(Result.Dados.Creditos)] := SFormatInt( vForm.sgCreditos.Cells[coCodigo, i] );

        Result.Dados.ValorTotal := Result.Dados.ValorTotal + SFormatDouble(vForm.sgCreditos.Cells[coValor, i]);
      end;
    end;
  end;
end;

procedure TFormBuscarCreditosDisponiveis.sgCreditosDblClick(Sender: TObject);
begin
  inherited;
  if sgCreditos.Cells[coNatureza, sgCreditos.Row] = 'P' then
    Informacoes.TituloReceber.Informar( SFormatInt( sgCreditos.Cells[coCodigo, sgCreditos.Row] ) )
  else
    Informacoes.TituloPagar.Informar( SFormatInt( sgCreditos.Cells[coCodigo, sgCreditos.Row] ) );
end;

procedure TFormBuscarCreditosDisponiveis.sgCreditosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coSelecionado, coTipo, coBloqueado] then
    vAlinhamento := taCenter
  else if ACol in[coCodigo, coValor, coIndiceCondPagto] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgCreditos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormBuscarCreditosDisponiveis.sgCreditosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coBloqueado then begin
    AFont.Style := [fsBold];
    AFont.Color := _Biblioteca.VermelhoAzul(sgCreditos.Cells[ACol, ARow]);
  end;
end;

procedure TFormBuscarCreditosDisponiveis.sgCreditosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgCreditos.Cells[coCodigo, ARow] = '' then
    Exit;

  if ACol = coSelecionado then begin
    if sgCreditos.Cells[ACol, ARow] = charNaoSelecionado then
      APicture := _Imagens.FormImagens.im1.Picture
    else if sgCreditos.Cells[ACol, ARow] = charSelecionado then
      APicture := _Imagens.FormImagens.im2.Picture;
  end;
end;

procedure TFormBuscarCreditosDisponiveis.sgCreditosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  procedure MarcaDesmarcaGridTodos;
  var
    i: Integer;
    vMarcar: Boolean;
    vExisteBloqueado: Boolean;
  begin
    vMarcar := False;

    for i := 1 to sgCreditos.RowCount -1 do begin
      if not Em(sgCreditos.Cells[coSelecionado, sgCreditos.Row], [charSelecionado, charNaoSelecionado]) then
        Continue;

      vMarcar := sgCreditos.Cells[coSelecionado, sgCreditos.Row] = charNaoSelecionado;
      Break;
    end;

    vExisteBloqueado := False;
    for i := 1 to sgCreditos.RowCount -1 do begin
      if not Em(sgCreditos.Cells[coSelecionado, i], [charSelecionado, charNaoSelecionado]) then
        Continue;

      if (vMarcar) and (sgCreditos.Cells[coBloqueado, i] = 'Sim') then begin
        vExisteBloqueado := false;
        Continue;
      end;

      sgCreditos.Cells[coSelecionado, i] := IIfStr(vMarcar, charSelecionado, charNaoSelecionado);
    end;

    if vExisteBloqueado then begin
      Exclamar('Alguns cr�ditos n�o foram selecionados pois n�o � permitido a utiliza��o de cr�ditos bloqueados!');
      Exit
    end;
  end;
begin
  inherited;
  if sgCreditos.Cells[coCodigo, sgCreditos.Row] = '' then
    Exit;

  if sgCreditos.Cells[coSelecionado, sgCreditos.Row] = '' then
    Exit;

  if not Em(sgCreditos.Cells[coSelecionado, sgCreditos.Row], [charSelecionado, charNaoSelecionado]) then
    Exit;

  if (Shift = [ssCtrl]) and (Key = VK_SPACE) then
    MarcaDesmarcaGridTodos
  else if Key = VK_SPACE then begin

    if sgCreditos.Cells[coBloqueado, sgCreditos.Row] = 'Sim' then begin
      Exclamar('N�o � permitido a utiliza��o deste cr�dito pois ele se encontra bloqueado!');
      Exit
    end;

    sgCreditos.Cells[coSelecionado,sgCreditos.Row] :=
      IIfStr(sgCreditos.Cells[coSelecionado, sgCreditos.Row] = charNaoSelecionado, charSelecionado, charNaoSelecionado);
  end;
end;

end.
