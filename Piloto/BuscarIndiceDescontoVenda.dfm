inherited FormBuscarIndiceDescontoVenda: TFormBuscarIndiceDescontoVenda
  Caption = #205'ndice de dedu'#231#227'o'
  ClientHeight = 90
  ClientWidth = 213
  OnShow = FormShow
  ExplicitWidth = 219
  ExplicitHeight = 119
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 53
    Width = 213
    ExplicitTop = 53
    ExplicitWidth = 372
  end
  inline FrIndiceDescontoVenda: TFrIndicesDescontosVenda
    Left = 6
    Top = 7
    Width = 355
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 6
    ExplicitTop = 7
    ExplicitWidth = 355
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Top = 10
      Width = 193
      Height = 30
      Align = alNone
      ExplicitTop = 10
      ExplicitWidth = 193
      ExplicitHeight = 30
    end
    inherited CkFiltroDuplo: TCheckBox
      Top = 10
      ExplicitTop = 10
    end
    inherited PnTitulos: TPanel
      Width = 355
      ExplicitWidth = 355
      inherited lbNomePesquisa: TLabel
        Width = 101
        Caption = #205'ndice de dedu'#231#227'o'
        ExplicitWidth = 101
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 250
        ExplicitLeft = 250
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 330
      Height = 25
      ExplicitLeft = 322
      ExplicitHeight = 25
      inherited sbPesquisa: TSpeedButton
        Top = 2
        Visible = False
        ExplicitTop = 2
      end
    end
  end
end
