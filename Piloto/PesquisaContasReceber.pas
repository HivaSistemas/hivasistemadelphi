unit PesquisaContasReceber;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisasAvancadas, _Biblioteca,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameTiposCobranca, _Sessao,
  Vcl.Grids, GridLuka, FrameDataInicialFinal, FrameFaixaNumeros, FrameClientes,
  FrameEmpresas, _ContasReceber, _RecordsFinanceiros, Vcl.StdCtrls, EditLuka;

type
  TFormPesquisaContasReceber = class(TFormHerancaPesquisasAvancadas)
    FrTipoCobranca: TFrTiposCobranca;
    FrCliente: TFrClientes;
    FrValorDocumento: TFrFaixaNumeros;
    FrDataCadastro: TFrDataInicialFinal;
    FrEmpresa: TFrEmpresas;
    FrDataVencimento: TFrDataInicialFinal;
    eNSU: TEditLuka;
    lb1: TLabel;
    eNumCartaoTruncado: TEditLuka;
    lb2: TLabel;
    lb3: TLabel;
    eNumeroAutorizacao: TEditLuka;
    eParcela: TEditLuka;
    lb4: TLabel;
    lb5: TLabel;
    eQuantParcelas: TEditLuka;
    procedure FormShow(Sender: TObject);
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  private
    FDados: TArray<RecContasReceber>;
  protected
    procedure VerificarRegistro; override;
    procedure BuscarRegistros; override;
  end;

function Pesquisar(
  pApenasCartao: Boolean;
  pParcela: Integer;
  pQtdeParcelas: Integer;
  pValorDocumentoInicial: Currency;
  pValorDocumentoFinal: Currency;
  pDataCadastroInicial: TDateTime;
  pDataCadastroFinal: TDateTime;
  pTipoCartao: string;
  pNSU: string;
  pNumCartaoTruncado: string;
  pNumAutorizacao: string
): TRetornoTelaFinalizar<RecContasReceber>;

implementation

{$R *.dfm}

const
  coReceberId      = 0;
  coCliente        = 1;
  coDataCadastro   = 2;
  coDataVencimento = 3;
  coDocumento      = 4;
  coParcela        = 5;
  coValorDocumento = 6;
  coValorRetencao  = 7;
  coValorLiquido   = 8;
  coStatus         = 9;
  coTipoCobranca   = 10;
  coEmpresa        = 11;
  coNsu            = 12;
  coNCartaoTrunc   = 13;
  coNautorizacao   = 14;

var
  FApenasCartao: Boolean;
  FTipoCartao: string;

function Pesquisar(
  pApenasCartao: Boolean;
  pParcela: Integer;
  pQtdeParcelas: Integer;
  pValorDocumentoInicial: Currency;
  pValorDocumentoFinal: Currency;
  pDataCadastroInicial: TDateTime;
  pDataCadastroFinal: TDateTime;
  pTipoCartao: string;
  pNSU: string;
  pNumCartaoTruncado: string;
  pNumAutorizacao: string
): TRetornoTelaFinalizar<RecContasReceber>;
var
  vForm: TFormPesquisaContasReceber;
begin
  FApenasCartao := pApenasCartao;
  FTipoCartao   := pTipoCartao;

  vForm := TFormPesquisaContasReceber.Create(nil);

  if pValorDocumentoInicial > 0 then
    vForm.FrValorDocumento.eValor1.AsCurr := pValorDocumentoInicial;

  if pValorDocumentoFinal > 0 then
    vForm.FrValorDocumento.eValor2.AsCurr := pValorDocumentoFinal;

  if pDataCadastroInicial > 0 then
    vForm.FrDataCadastro.eDataInicial.AsData := pDataCadastroInicial;

  if pDataCadastroFinal > 0 then
    vForm.FrDataCadastro.eDataFinal.AsData := pDataCadastroFinal;

  if Trim(pNSU) <> '' then
    vForm.eNSU.Text := pNSU;

  if Trim(pNumCartaoTruncado) <> '' then
    vForm.eNumCartaoTruncado.Text := pNumCartaoTruncado;

  if Trim(pNumAutorizacao) <> '' then
    vForm.eNumeroAutorizacao.Text := pNumAutorizacao;

  if pQtdeParcelas > 0 then
    vForm.eQuantParcelas.AsInt := pQtdeParcelas;

  if pParcela > 0 then
    vForm.eParcela.AsInt := pParcela;

  Result.OkPesquisaAvancada(vForm.ShowModal, True, vForm.sgPesquisa, vForm.FDados);
end;

{ TFormPesquisaContasReceber }

procedure TFormPesquisaContasReceber.BuscarRegistros;
var
  i: Integer;
  vComando: string;
begin
  inherited;
  vComando := '';

  if not FrCliente.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrCliente.getSqlFiltros('COR.CADASTRO_ID'));

  if not FrTipoCobranca.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrTipoCobranca.getSqlFiltros('COR.COBRANCA_ID'));

  if not FrEmpresa.EstaVazio then
    WhereOuAnd(vComando, FrEmpresa.getSqlFiltros('COR.EMPRESA_ID'));

  if not FrValorDocumento.EstaVazio then
    WhereOuAnd(vComando, FrValorDocumento.getSQL('COR.VALOR_DOCUMENTO'));

  if FApenasCartao then
    WhereOuAnd(vComando, 'TCO.FORMA_PAGAMENTO = ''CRT'' ');

  if eParcela.AsInt > 0 then
    WhereOuAnd(vComando, 'COR.PARCELA = ' + IntToStr(eParcela.AsInt));

  if eQuantParcelas.AsInt > 0 then
    WhereOuAnd(vComando, 'COR.NUMERO_PARCELAS = ' + IntToStr(eQuantParcelas.AsInt));

  if FTipoCartao <> '' then
    WhereOuAnd(vComando, 'TCO.TIPO_CARTAO = ''' + FTipoCartao + '''');

  if eNSU.AsInt > 0 then
    WhereOuAnd(vComando, 'COR.NSU like  ''%'' || ''' + IntToStr(eNSU.AsInt) + ''' || ''%''');

  if Trim(eNumCartaoTruncado.Text) <> '' then
    WhereOuAnd(vComando, 'COR.NUMERO_CARTAO_TRUNCADO like ''%'' || ''' + eNumCartaoTruncado.Text + ''' || ''%''');

  if Trim(eNumeroAutorizacao.Text) <> '' then
    WhereOuAnd(vComando, 'COR.CODIGO_AUTORIZACAO_TEF like ''%'' || ''' + eNumeroAutorizacao.Text + ''' || ''%''');

  if not((eNSU.AsInt > 0) or (Trim(eNumCartaoTruncado.Text) <> '') or (Trim(eNumeroAutorizacao.Text) <> '')) then begin
    if not FrDataCadastro.EstaVazio then
      WhereOuAnd(vComando, FrDataCadastro.getSqlFiltros('COR.DATA_CADASTRO'));

    if not FrDataVencimento.EstaVazio then
      WhereOuAnd(vComando, FrDataVencimento.getSqlFiltros('COR.DATA_VENCIMENTO'));
  end;

  vComando := vComando + ' order by COR.DATA_VENCIMENTO ';

  FDados := _ContasReceber.BuscarContasReceberComando(Sessao.getConexaoBanco, vComando);
  if FDados = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := FDados;
  for i := Low(FDados) to High(FDados) do begin
    sgPesquisa.Cells[coReceberId, i + 1]      := NFormat(FDados[i].ReceberId);
    sgPesquisa.Cells[coCliente, i + 1]        := getInformacao(FDados[i].CadastroId, FDados[i].NomeCliente);
    sgPesquisa.Cells[coDataCadastro, i + 1]   := DFormat(FDados[i].data_cadastro);
    sgPesquisa.Cells[coDataVencimento, i + 1] := DFormat(FDados[i].data_vencimento);
    sgPesquisa.Cells[coDocumento, i + 1]      := FDados[i].documento;
    sgPesquisa.Cells[coParcela, i + 1]        := IntToStr(FDados[i].Parcela) + '/' + IntToStr(FDados[i].NumeroParcelas);
    sgPesquisa.Cells[coValorDocumento, i + 1] := NFormatN(FDados[i].ValorDocumento);
    sgPesquisa.Cells[coValorRetencao, i + 1]  := NFormatN(FDados[i].ValorRetencao);
    sgPesquisa.Cells[coValorLiquido, i + 1]   := NFormatN(FDados[i].ValorDocumento - FDados[i].ValorRetencao);
    sgPesquisa.Cells[coStatus, i + 1]         := FDados[i].StatusAnalitico;
    sgPesquisa.Cells[coTipoCobranca, i + 1]   := getInformacao(FDados[i].cobranca_id, FDados[i].nome_tipo_cobranca);
    sgPesquisa.Cells[coEmpresa, i + 1]        := getInformacao(FDados[i].empresa_id, FDados[i].nome_empresa);
    sgPesquisa.Cells[coNsu, i + 1]            := FDados[i].nsu;
    sgPesquisa.Cells[coNCartaoTrunc, i + 1]   := FDados[i].NumeroCartaoTruncado;
    sgPesquisa.Cells[coNautorizacao, i + 1]   := FDados[i].codigo_autorizacao_tef;

  end;
  sgPesquisa.SetLinhasGridPorTamanhoVetor( Length(FDados) );
  SetarFoco(sgPesquisa);
end;

procedure TFormPesquisaContasReceber.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrCliente);
end;

procedure TFormPesquisaContasReceber.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coReceberId, coValorDocumento, coValorRetencao, coValorLiquido] then
    vAlinhamento := taRightJustify
  else if ACol = coStatus then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormPesquisaContasReceber.VerificarRegistro;
begin
  inherited;
  FDados := nil;
  sgPesquisa.ClearGrid;
end;

end.
