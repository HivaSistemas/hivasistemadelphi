unit Frame.EnderecoEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _Sessao, _Biblioteca,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _EnderecoEstoque, Pesquisa.EnderecoEstoque,
  _RecordsCadastros, System.Math, Vcl.Buttons, Vcl.Menus;

type
  TFrEnderecoEstoque = class(TFrameHenrancaPesquisas)
  public
    function GetEnderecoEstoque(pLinha: Integer = -1): RecEnderecosEstoque;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrameEnderecoEstoque }

function TFrEnderecoEstoque.AdicionarDireto: TObject;
var
  vMotivos: TArray<RecEnderecosEstoque>;
begin
  vMotivos := _EnderecoEstoque.BuscarEnderecoEstoque(Sessao.getConexaoBanco, 0, [FChaveDigitada]);
  if vMotivos = nil then
    Result := nil
  else
    Result := vMotivos[0];
end;

function TFrEnderecoEstoque.AdicionarPesquisando: TObject;
begin
  Result := Pesquisa.EnderecoEstoque.Pesquisar();
end;

function TFrEnderecoEstoque.GetEnderecoEstoque(pLinha: Integer): RecEnderecosEstoque;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecEnderecosEstoque(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrEnderecoEstoque.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecEnderecosEstoque(FDados[i]).endereco_id = RecEnderecosEstoque(pSender).endereco_id then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrEnderecoEstoque.MontarGrid;
var
  i: Integer;
  vSender: RecEnderecosEstoque;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecEnderecosEstoque(FDados[i]);
      AAdd([IntToStr(vSender.endereco_id), 'Rua: ' + vSender.nome_rua + '| M�dulo: ' + vSender.nome_modulo + '| N�vel: ' + vSender.nome_nivel + '| V�o: ' + vSender.nome_vao]);
    end;
  end;
end;

end.
