unit FrameCidades;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _Sessao, _Biblioteca,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _RecordsCadastros, _Cidades, PesquisaCidades, System.StrUtils,
  System.Math, Vcl.Buttons, Vcl.Menus;

type
  TFrCidades = class(TFrameHenrancaPesquisas)
    ckPreencherEstado: TCheckBox;
  public
    function GetCidade(pLinha: Integer = -1): RecCidades;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrCidades }

function TFrCidades.AdicionarDireto: TObject;
var
  cidades: TArray<RecCidades>;
begin
  cidades := _Cidades.BuscarCidades(Sessao.getConexaoBanco, 0, [FChaveDigitada] );
  if cidades = nil then
    Result := nil
  else
    Result := cidades[0];
end;

function TFrCidades.AdicionarPesquisando: TObject;
begin
  Result := PesquisaCidades.PesquisarCidade(ckPreencherEstado.Checked);
end;

function TFrCidades.GetCidade(pLinha: Integer): RecCidades;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecCidades(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrCidades.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecCidades(FDados[i]).cidade_id = RecCidades(pSender).cidade_id then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrCidades.MontarGrid;
var
  i: Integer;
  vSender: RecCidades;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecCidades(FDados[i]);
      AAdd([IntToStr(vSender.cidade_id), vSender.nome + '   ' + vSender.estado_id]);
    end;
  end;
end;

end.
