inherited FormAtalhoSistema: TFormAtalhoSistema
  Caption = 'Atalhos do Sistema'
  ClientHeight = 432
  ClientWidth = 549
  OnShow = FormShow
  ExplicitWidth = 555
  ExplicitHeight = 461
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 395
    Width = 549
    ExplicitTop = 395
    ExplicitWidth = 549
  end
  object sgAtalhos: TGridLuka
    Left = 0
    Top = 0
    Width = 549
    Height = 395
    Align = alClient
    ColCount = 2
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goEditing, goRowSelect]
    TabOrder = 1
    OnDrawCell = sgAtalhosDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Atalho'
      'Descri'#231#227'o')
    Grid3D = False
    RealColCount = 10
    AtivarPopUpSelecao = False
    ColWidths = (
      93
      430)
  end
end
