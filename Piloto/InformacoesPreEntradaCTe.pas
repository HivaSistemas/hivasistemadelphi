unit InformacoesPreEntradaCTe;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.OleCtrls, _Biblioteca, _Sessao,
  SHDocVw, Vcl.Mask, EditLukaData, SpeedButtonLuka, Vcl.ComCtrls, _PreEntradasCte,
  StaticTextLuka, Vcl.StdCtrls, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Grids,
  GridLuka, _PreEntrCteNfeReferenciadas;

type
  TFormInformacoesPreEntradaCTe = class(TFormHerancaFinalizar)
    lb1: TLabel;
    ePreEntradaId: TEditLuka;
    eCNPJEmitente: TEditLuka;
    lb2: TLabel;
    lb13: TLabel;
    eRazaoSocialEmitente: TEditLuka;
    stStatusAltis: TStaticText;
    st1: TStaticTextLuka;
    lb11: TLabel;
    eNumeroNota: TEditLuka;
    lb12: TLabel;
    eSerieNota: TEditLuka;
    pcDados: TPageControl;
    tsPrincipais: TTabSheet;
    lb14: TLabel;
    sbInformaocoesEntrada: TSpeedButtonLuka;
    lb15: TLabel;
    lb16: TLabel;
    lb17: TLabel;
    lb18: TLabel;
    st2: TStaticTextLuka;
    stStatusSefaz: TStaticText;
    eConhecimentoId: TEditLuka;
    eEmpresa: TEditLuka;
    eDataHoraEmissao: TEditLukaData;
    eValorTotalNota: TEditLuka;
    eBaseCalculoICMS: TEditLuka;
    tsXML: TTabSheet;
    wbXML: TWebBrowser;
    lb19: TLabel;
    eChaveAcessoNFe: TEditLuka;
    lb5: TLabel;
    eCnpjRemetente: TEditLuka;
    eRazaoSocialRemetente: TEditLuka;
    lb6: TLabel;
    lb7: TLabel;
    ePercICMS: TEditLuka;
    lb8: TLabel;
    eValorICMS: TEditLuka;
    tsChavesNFe: TTabSheet;
    sgReferencias: TGridLuka;
    procedure FormShow(Sender: TObject);
    procedure sgReferenciasDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure Informar(pPreEntradaId: Integer);

implementation

{$R *.dfm}

const
  coPreEntradaId    = 0;
  coNumeroNota      = 1;
  coSerieNota       = 2;
  coCNPJ            = 3;
  coRazaoSocial     = 4;
  coDataHoraEmissao = 5;
  coValorTotalNota  = 6;
  coChaveAcessoNFe  = 7;

procedure Informar(pPreEntradaId: Integer);
var
  i: Integer;
  vForm: TFormInformacoesPreEntradaCTe;
  vPreEntrada: TArray<RecPreEntradasCte>;
  vReferencias: TArray<RecPreEntrCteNfeReferenciadas>;
begin
  if pPreEntradaId = 0 then
    Exit;

  vPreEntrada := _PreEntradasCte.BuscarPreEntradasCte(Sessao.getConexaoBanco, 0, [pPreEntradaId]);
  if vPreEntrada = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  vReferencias := _PreEntrCteNfeReferenciadas.BuscarPreEntrCteNfeReferenciadas(Sessao.getConexaoBanco, 0, [pPreEntradaId]);

  vForm := TFormInformacoesPreEntradaCTe.Create(Application);

  vForm.ePreEntradaId.AsInt         := vPreEntrada[0].PreEntradaId;
  vForm.eCNPJEmitente.Text          := vPreEntrada[0].CnpjEmitente;
  vForm.eRazaoSocialEmitente.Text   := vPreEntrada[0].RazaoSocialEmitente;
  vForm.eNumeroNota.AsInt           := vPreEntrada[0].NumeroCte;
  vForm.eSerieNota.Text             := vPreEntrada[0].SerieCte;
  vForm.stStatusAltis.Caption       := vPreEntrada[0].StatusAltisAnalitico;
  vForm.stStatusSefaz.Caption       := vPreEntrada[0].StatusSEFAZAnalitico;
  vForm.eConhecimentoId.SetInformacao(vPreEntrada[0].ConhecimentoId);

  vForm.eEmpresa.SetInformacao(vPreEntrada[0].EmpresaId, '');
  vForm.eDataHoraEmissao.AsDataHora := vPreEntrada[0].DataHoraEmissao;

  vForm.eValorTotalNota.AsDouble    := vPreEntrada[0].ValorTotalCte;

  vForm.eCnpjRemetente.Text          := vPreEntrada[0].CnpjRemetente;
  vForm.eRazaoSocialRemetente.Text   := vPreEntrada[0].RazaoSocialRemetente;

  vForm.eChaveAcessoNFe.Text := vPreEntrada[0].ChaveAcessoCte;

  for i := Low(vReferencias) to High(vReferencias) do begin
    vForm.sgReferencias.Cells[coPreEntradaId, i + 1]    := NFormat(vReferencias[i].PreEntradaIdNfeRef);
    vForm.sgReferencias.Cells[coNumeroNota, i + 1]      := NFormat(vReferencias[i].NumeroNotaNFeRef);
    vForm.sgReferencias.Cells[coSerieNota, i + 1]       := vReferencias[i].SerieNotaNfeRef;
    vForm.sgReferencias.Cells[coCNPJ, i + 1]            := vReferencias[i].CnpjEmitenteNFeRef;
    vForm.sgReferencias.Cells[coRazaoSocial, i + 1]     := vReferencias[i].RazaoSocialEmienteNfeRef;
    vForm.sgReferencias.Cells[coDataHoraEmissao, i + 1] := DHFormatN(vReferencias[i].DataHoraEmissaoNFeRef);
    vForm.sgReferencias.Cells[coValorTotalNota, i + 1]  := NFormatN(vReferencias[i].ValorTotalNotaNFeRef);
    vForm.sgReferencias.Cells[coChaveAcessoNFe, i + 1]  := vReferencias[i].ChaveAcessoNfe;
  end;
  vForm.sgReferencias.SetLinhasGridPorTamanhoVetor( Length(vReferencias) );

  _Biblioteca.salvarStringEmAquivo(vPreEntrada[0].XmlTexto, Sessao.getCaminhoPastaTemporaria + '\xml_temp_cte.xml');
  vForm.wbXml.Navigate(Sessao.getCaminhoPastaTemporaria + '\xml_temp_cte.xml');

  vForm.ShowModal;

  vForm.Free;
end;

procedure TFormInformacoesPreEntradaCTe.FormShow(Sender: TObject);
begin
  inherited;
  pcDados.ActivePageIndex := 0;
end;

procedure TFormInformacoesPreEntradaCTe.sgReferenciasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coPreEntradaId, coNumeroNota, coSerieNota, coValorTotalNota] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgReferencias.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
