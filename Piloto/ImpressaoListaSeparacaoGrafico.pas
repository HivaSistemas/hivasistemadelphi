unit ImpressaoListaSeparacaoGrafico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosGraficos, QRExport, _RecordsExpedicao,
  QRPDFFilt, QRWebFilt, QRCtrls, QuickRpt, Vcl.ExtCtrls, _Biblioteca, _EntregasItens, _Entregas,
  _Sessao, _RetiradasItens, _RecordsEspeciais, _Retiradas, QRBarcodeComum;

type
  TTipoMovimento = (tpEntrega, tpRetirada);

  TFormImpressaoListaSeparacaoGrafico = class(TFormHerancaRelatoriosGraficos)
    qrbndNFDetail: TQRBand;
    qrl2: TQRLabel;
    qrlNFNomeProduto: TQRLabel;
    qrlNFCabUnidade: TQRLabel;
    qrlNFLote: TQRLabel;
    qrlNFQuantidade: TQRLabel;
    qrlCabProdutoId: TQRLabel;
    qrl1: TQRLabel;
    qrlNFMarca: TQRLabel;
    qrl4: TQRLabel;
    qrlNFUnidade: TQRLabel;
    qrl3: TQRLabel;
    qrlNFCodigoBarras: TQRLabel;
    qrl7: TQRLabel;
    qrlNFCodigoOriginal: TQRLabel;
    qrlNFVendedor: TQRLabel;
    qrlNFCliente: TQRLabel;
    qrlNFPedido: TQRLabel;
    qrlNFCodigoMovimento: TQRLabel;
    qrlNFLocal: TQRLabel;
    QRShape1: TQRShape;
    qrlNFBarCodeEntrega: TQRBarcode;
    procedure qrbndNFDetailBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qrRelatorioNFNeedData(Sender: TObject; var MoreData: Boolean);
    procedure qrRelatorioNFBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
  private
    FPosicao: Integer;
    FTipoMovimento: TTipoMovimento;
    FItensEnt: TArray<RecEntregaItem>;
    FItensRet: TArray<RecRetiradaItem>;
  end;

procedure Imprimir(pId: Integer; pTipoMovimento: TTipoMovimento);

implementation

{$R *.dfm}

procedure Imprimir(pId: Integer; pTipoMovimento: TTipoMovimento);
type
  RecInformacoesLista = record
    Id: Integer;
    EmpresaId: Integer;
    OrcamentoId: Integer;
    ClienteId: Integer;
    NomeCliente: string;
    VendedorId: Integer;
    NomeVendedor: string;
    LocalId: Integer;
    NomeLocal: string;
    QuantidadeProdutos: Integer;
  end;

var
  vRetBanco: RecRetornoBD;

  vImpressora: RecImpressora;
  vEntregas: TArray<RecEntrega>;
  vInformacoes: RecInformacoesLista;
  vQtdeViasJaImpressasCompEntrega: Integer;

  vForm: TFormImpressaoListaSeparacaoGrafico;
begin
  vQtdeViasJaImpressasCompEntrega := 0;

  if pTipoMovimento = tpEntrega then
    vQtdeViasJaImpressasCompEntrega := _Entregas.getQtdeViasImpressasComprovanteEntrega(Sessao.getConexaoBanco, pId);

  if (vQtdeViasJaImpressasCompEntrega > 0) and (not Sessao.AutorizadoRotina('reimprimir_lista_separacao_entrega')) then begin
    _Biblioteca.Exclamar('Voc� n�o possui autoriza��o para reimprimir a lista de separa��o de entregas!');
    Exit;
  end;

  vImpressora := Sessao.getImpressora( toListaSeparacao );
  if vImpressora.CaminhoImpressora = '' then begin
    _Biblioteca.ImpressoraNaoConfigurada;
    Exit;
  end;

  if pTipoMovimento = tpEntrega then
    vRetBanco := _Entregas.IncrementarQtdeViasListaSeparacao(Sessao.getConexaoBanco, pId)
  else
    vRetBanco := _Retiradas.setImprimiuListaSeparacao(Sessao.getConexaoBanco, pId);

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  vEntregas := _Entregas.BuscarEntregas(Sessao.getConexaoBanco, 1, [pId]);
  if vEntregas = nil then begin
    _Biblioteca.Exclamar('Entrega n�o encontrada! Entrega: ' + _Biblioteca.NFormat(pId));
    Exit;
  end;
  vInformacoes.Id            := pId;
  vInformacoes.EmpresaId     := vEntregas[0].EmpresaId;
  vInformacoes.OrcamentoId   := vEntregas[0].OrcamentoId;
  vInformacoes.ClienteId     := vEntregas[0].ClienteId;
  vInformacoes.NomeCliente   := vEntregas[0].NomeCliente;
  vInformacoes.VendedorId    := vEntregas[0].VendedorId;
  vInformacoes.NomeVendedor  := vEntregas[0].NomeVendedor;
  vInformacoes.LocalId       := vEntregas[0].LocalId;
  vInformacoes.NomeLocal     := vEntregas[0].NomeLocal;

  vForm := TFormImpressaoListaSeparacaoGrafico.Create(nil, vImpressora);
  vForm.PreencherCabecalho( vInformacoes.EmpresaId, False );
  vForm.qrNFEndereco.Caption         := '*********************';
  vForm.qrlNFTelefoneEmpresa.Caption := '*********************';

  vForm.qrlNFSistema.Caption := _Sessao.Sessao.getVersaoSistema;

  if pTipoMovimento = tpEntrega then begin
    vForm.qrNFTipoDocumento.Caption := 'LISTA DE SEPARA��O DE ENTREGA';
    vForm.FItensEnt := _EntregasItens.BuscarEntregaItens(Sessao.getConexaoBanco, 1, [pId]);
    vInformacoes.QuantidadeProdutos := Length(vForm.FItensEnt);
  end
  else begin
    vForm.qrNFTipoDocumento.Caption := 'LISTA DE SEPARA��O DE RETIRADA';
    vForm.FItensRet := _RetiradasItens.BuscarRetiradaItens(Sessao.getConexaoBanco, 0, [pId]);
    vInformacoes.QuantidadeProdutos := Length(vForm.FItensRet);
  end;

  vForm.FTipoMovimento := pTipoMovimento;

  vForm.qrlNFCodigoMovimento.Caption := IIfStr(pTipoMovimento = tpEntrega, 'Entrega.: ', 'Retirada.:') + _Biblioteca.NFormat(vInformacoes.Id);
  vForm.qrlNFPedido.Caption          := 'Pedido..: ' + _Biblioteca.NFormat(vInformacoes.OrcamentoId);
  vForm.qrlNFCliente.Caption         := 'Cliente.: ' + _Biblioteca.NFormat(vInformacoes.ClienteId) + ' - ' + vInformacoes.NomeCliente;
  vForm.qrlNFVendedor.Caption        := 'Vendedor: ' + _Biblioteca.NFormat(vInformacoes.VendedorId) + ' - ' + vInformacoes.NomeVendedor;
  vForm.qrlNFBarCodeEntrega.BarText  := _Biblioteca.LPad(IntToStr(pId), 12, '0');

  try
    vForm.qrRelatorioNF.Height :=
      vForm.qrBandTitulo.Height + ( vForm.qrbndNFDetail.Height * vInformacoes.QuantidadeProdutos ) +
      vForm.PageFooterBand1.Height + 10;

    vForm.Imprimir;

  finally
    vForm.Free;
  end;
end;

procedure TFormImpressaoListaSeparacaoGrafico.qrbndNFDetailBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  if FTipoMovimento = tpEntrega then begin
    qrlNFNomeProduto.Caption    := _Biblioteca.NFormat(FItensEnt[FPosicao].ProdutoId) + ' - ' + FItensEnt[FPosicao].NomeProduto;
    if Length(qrlNFNomeProduto.Caption) > 38 then
      qrlNFNomeProduto.Font.Size := 6
    else
      qrlNFNomeProduto.Font.Size := 7;

    qrlNFMarca.Caption          := FItensEnt[FPosicao].NomeMarca;

    if FItensEnt[FPosicao].UnidadeEntregaId <> '' then begin
      qrlNFQuantidade.Caption     := _Biblioteca.NFormatEstoque(FItensEnt[FPosicao].Quantidade / FItensEnt[FPosicao].MultiploVenda);
      qrlNFUnidade.Caption        := FItensEnt[FPosicao].UnidadeEntregaId;
    end
    else begin
      qrlNFQuantidade.Caption     := _Biblioteca.NFormatEstoque(FItensEnt[FPosicao].Quantidade);
      qrlNFUnidade.Caption        := FItensEnt[FPosicao].Unidade;
    end;

    qrlNFLote.Caption           := FItensEnt[FPosicao].Lote;
    qrlNFCodigoBarras.Caption   := FItensEnt[FPosicao].CodigoBarras;
    qrlNFCodigoOriginal.Caption := FItensEnt[FPosicao].CodigoOriginalFabricante;
  end
  else begin
    qrlNFNomeProduto.Caption    := _Biblioteca.NFormat(FItensRet[FPosicao].ProdutoId) + ' - ' + FItensRet[FPosicao].NomeProduto;
    if Length(qrlNFNomeProduto.Caption) > 38 then
      qrlNFNomeProduto.Font.Size := 6
    else
      qrlNFNomeProduto.Font.Size := 7;

    qrlNFMarca.Caption          := FItensRet[FPosicao].NomeMarca;

    if FItensRet[FPosicao].UnidadeEntregaId <> '' then begin
      qrlNFQuantidade.Caption     := _Biblioteca.NFormatEstoque(FItensRet[FPosicao].Quantidade / FItensRet[FPosicao].MultiploVenda);
      qrlNFUnidade.Caption        := FItensRet[FPosicao].UnidadeEntregaId;
    end
    else begin
      qrlNFQuantidade.Caption     := _Biblioteca.NFormatEstoque(FItensRet[FPosicao].Quantidade);
      qrlNFUnidade.Caption        := FItensRet[FPosicao].Unidade;
    end;

    qrlNFLote.Caption           := FItensRet[FPosicao].Lote;
    qrlNFCodigoBarras.Caption   := FItensRet[FPosicao].CodigoBarras;
    qrlNFCodigoOriginal.Caption := FItensRet[FPosicao].CodigoOriginalFabricante;
  end;
end;

procedure TFormImpressaoListaSeparacaoGrafico.qrRelatorioNFBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicao := -1;
end;

procedure TFormImpressaoListaSeparacaoGrafico.qrRelatorioNFNeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicao);
  if FTipoMovimento = tpEntrega then
    MoreData := FPosicao < Length(FItensEnt)
  else
    MoreData := FPosicao < Length(FItensRet);
end;

end.
