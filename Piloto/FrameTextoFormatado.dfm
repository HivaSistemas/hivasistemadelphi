inherited FrTextoFormatado: TFrTextoFormatado
  Width = 745
  Height = 413
  ExplicitWidth = 745
  ExplicitHeight = 413
  object stbStatus: TStatusBar
    Left = 0
    Top = 394
    Width = 745
    Height = 19
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBtnText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Panels = <
      item
        Width = 100
      end
      item
        Width = 150
      end
      item
        Width = 70
      end>
    UseSystemFont = False
  end
  object rtTexto: TRichEdit
    AlignWithMargins = True
    Left = 10
    Top = 10
    Width = 735
    Height = 384
    Margins.Left = 10
    Margins.Top = 10
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alClient
    BorderStyle = bsNone
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    HideSelection = False
    ParentColor = True
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 1
    WantTabs = True
    Zoom = 100
  end
end
