inherited FormRelacaoContasReceber: TFormRelacaoContasReceber
  Caption = 'R. de contas a receber'
  ClientHeight = 664
  ClientWidth = 1049
  OnShow = FormShow
  ExplicitTop = -251
  ExplicitWidth = 1055
  ExplicitHeight = 693
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 664
    ExplicitHeight = 664
    inherited sbImprimir: TSpeedButton
      Top = 54
      ExplicitTop = 54
    end
    object miBaixarTitulo: TSpeedButton [2]
      Left = 0
      Top = 168
      Width = 117
      Height = 25
      Caption = 'Baixar t'#237'tulo focado'
      Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = miBaixarTituloClick
    end
    object miBaixarTitulosSelecionados: TSpeedButton [3]
      Left = 4
      Top = 216
      Width = 117
      Height = 25
      Caption = 'Baixa agrupada'
      Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = miAlterarInformacoes0
    end
    object lb1: TLabel [4]
      Left = 7
      Top = 542
      Width = 37
      Height = 14
      Caption = '% Juros'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb9: TLabel [5]
      Left = 5
      Top = 509
      Width = 41
      Height = 14
      Caption = '% Multa'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object SpeedButton1: TSpeedButton [6]
      Left = 4
      Top = 270
      Width = 110
      Height = 40
      BiDiMode = bdLeftToRight
      Caption = 'Gerar Planilha'
      Flat = True
      NumGlyphs = 2
      ParentBiDiMode = False
      Visible = False
      OnClick = sbImprimirClick
    end
    inherited ckOmitirFiltros: TCheckBoxLuka
      Left = 6
      Top = 624
      ExplicitLeft = 6
      ExplicitTop = 624
    end
    object ePercMulta: TEditLuka
      Left = 54
      Top = 500
      Width = 35
      Height = 22
      Alignment = taRightJustify
      CharCase = ecUpperCase
      Font.Charset = ANSI_CHARSET
      Font.Color = 33023
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      Text = '0,00'
      OnKeyDown = ProximoCampo
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 2
      NaoAceitarEspaco = False
    end
    object ePercJuros: TEditLuka
      Left = 54
      Top = 534
      Width = 35
      Height = 22
      Alignment = taRightJustify
      CharCase = ecUpperCase
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      Text = '0,00'
      OnKeyDown = ProximoCampo
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 2
      NaoAceitarEspaco = False
    end
  end
  inherited pcDados: TPageControl
    Width = 927
    Height = 664
    ExplicitWidth = 927
    ExplicitHeight = 664
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 919
      ExplicitHeight = 635
      object Label1: TLabel
        Left = 648
        Top = 452
        Width = 131
        Height = 14
        Caption = 'Nome emitente cheque:'
      end
      object Label2: TLabel
        Left = 648
        Top = 501
        Width = 73
        Height = 14
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alCustom
        Caption = 'Agrupamento'
      end
      inline FrEmpresas: TFrEmpresas
        Left = 1
        Top = 0
        Width = 403
        Height = 60
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 1
        ExplicitWidth = 403
        ExplicitHeight = 60
        inherited sgPesquisa: TGridLuka
          Width = 378
          Height = 43
          ExplicitWidth = 378
          ExplicitHeight = 43
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 53
            ExplicitWidth = 53
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
            inherited ckSuprimir: TCheckBox
              Width = 66
              ExplicitWidth = 66
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          Height = 44
          ExplicitLeft = 378
          ExplicitHeight = 44
        end
      end
      inline FrDataCadastro: TFrDataInicialFinal
        Left = 416
        Top = 144
        Width = 197
        Height = 41
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 5
        TabStop = True
        ExplicitLeft = 416
        ExplicitTop = 144
        ExplicitWidth = 197
        inherited Label1: TLabel
          Width = 93
          Height = 14
          ExplicitWidth = 93
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          OnKeyDown = ProximoCampoForaFrame
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrDataVencimento: TFrDataInicialFinal
        Left = 416
        Top = 0
        Width = 197
        Height = 42
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 6
        TabStop = True
        ExplicitLeft = 416
        ExplicitWidth = 197
        ExplicitHeight = 42
        inherited Label1: TLabel
          Width = 109
          Height = 14
          Caption = 'Data de vencimento'
          ExplicitWidth = 109
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          OnKeyDown = ProximoCampoForaFrame
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      object gbStatusFinanceiro: TGroupBoxLuka
        Left = 416
        Top = 271
        Width = 107
        Height = 60
        Caption = '  Status  '
        TabOrder = 9
        OpcaoMarcarDesmarcar = False
        object ckTitulosEmAberto: TCheckBox
          Left = 12
          Top = 16
          Width = 88
          Height = 17
          Caption = 'Aberto'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object ckTitulosBaixados: TCheckBox
          Left = 12
          Top = 37
          Width = 88
          Height = 17
          Caption = 'Baixado'
          TabOrder = 1
        end
      end
      object rgOrdenacao: TRadioGroup
        Left = 416
        Top = 455
        Width = 190
        Height = 163
        Caption = '  Ordernar por  '
        ItemIndex = 0
        Items.Strings = (
          'Cliente(C'#243'digo)'
          'Cliente(Nome)'
          'Data de cadastro'
          'Data de vencimento'
          'Tipo de cobran'#231'a'
          'Status')
        TabOrder = 11
      end
      object gbFormasPagamento: TGroupBoxLuka
        Left = 416
        Top = 340
        Width = 190
        Height = 80
        Caption = '  Formas de pagamento '
        TabOrder = 10
        OpcaoMarcarDesmarcar = True
        object ckTipoDinheiro: TCheckBox
          Left = 14
          Top = 16
          Width = 88
          Height = 17
          Caption = 'Dinheiro'
          TabOrder = 0
        end
        object ckTipoCartao: TCheckBox
          Left = 106
          Top = 16
          Width = 88
          Height = 17
          Caption = 'Cart'#227'o'
          TabOrder = 1
        end
        object ckTipoCheque: TCheckBox
          Left = 14
          Top = 36
          Width = 88
          Height = 17
          Caption = 'Cheque'
          TabOrder = 2
          OnClick = ckTipoChequeClick
        end
        object ckTipoCobranca: TCheckBox
          Left = 106
          Top = 36
          Width = 88
          Height = 17
          Caption = 'Cobran'#231'a'
          TabOrder = 3
        end
        object ckAcumulado: TCheckBox
          Left = 14
          Top = 56
          Width = 88
          Height = 17
          Caption = 'Acumulado'
          TabOrder = 4
        end
      end
      inline FrDataBaixa: TFrDataInicialFinal
        Left = 416
        Top = 49
        Width = 197
        Height = 42
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 7
        TabStop = True
        ExplicitLeft = 416
        ExplicitTop = 49
        ExplicitWidth = 197
        ExplicitHeight = 42
        inherited Label1: TLabel
          Width = 76
          Height = 14
          Caption = 'Data de baixa'
          ExplicitWidth = 76
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrDataPagamento: TFrDataInicialFinal
        Left = 416
        Top = 95
        Width = 197
        Height = 44
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 8
        TabStop = True
        ExplicitLeft = 416
        ExplicitTop = 95
        ExplicitWidth = 197
        ExplicitHeight = 44
        inherited Label1: TLabel
          Width = 108
          Height = 14
          Caption = 'Data de pagamento'
          ExplicitWidth = 108
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrCodigoPedido: TFrameInteiros
        Left = 648
        Top = 270
        Width = 117
        Height = 84
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 15
        TabStop = True
        ExplicitLeft = 648
        ExplicitTop = 270
        ExplicitWidth = 117
        inherited sgInteiros: TGridLuka
          Width = 117
          ExplicitWidth = 117
        end
        inherited Panel1: TPanel
          Width = 117
          Caption = 'C'#243'digo do pedido'
          ExplicitWidth = 117
        end
      end
      inline FrPortadores: TFrPortadores
        Left = 1
        Top = 185
        Width = 403
        Height = 60
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 185
        ExplicitWidth = 403
        ExplicitHeight = 60
        inherited sgPesquisa: TGridLuka
          Width = 378
          Height = 43
          ExplicitWidth = 378
          ExplicitHeight = 43
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 59
            ExplicitWidth = 59
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          Height = 44
          ExplicitLeft = 378
          ExplicitHeight = 44
        end
      end
      inline FrCentrosCustos: TFrCentroCustos
        Left = 1
        Top = 246
        Width = 403
        Height = 60
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 4
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 246
        ExplicitWidth = 403
        ExplicitHeight = 60
        inherited sgPesquisa: TGridLuka
          Width = 378
          Height = 43
          ExplicitWidth = 378
          ExplicitHeight = 43
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 90
            ExplicitWidth = 90
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          Height = 44
          ExplicitLeft = 378
          ExplicitHeight = 44
        end
      end
      inline FrTiposCobranca: TFrTiposCobranca
        Left = 1
        Top = 123
        Width = 402
        Height = 60
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 123
        ExplicitWidth = 402
        ExplicitHeight = 60
        inherited sgPesquisa: TGridLuka
          Width = 377
          Height = 43
          ExplicitWidth = 377
          ExplicitHeight = 43
        end
        inherited CkAspas: TCheckBox
          Top = 22
          ExplicitTop = 22
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 402
          ExplicitWidth = 402
          inherited lbNomePesquisa: TLabel
            Width = 99
            ExplicitWidth = 99
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 297
            ExplicitLeft = 297
          end
        end
        inherited pnPesquisa: TPanel
          Left = 377
          Height = 44
          ExplicitLeft = 377
          ExplicitHeight = 44
        end
      end
      inline FrClientes: TFrameCadastros
        Left = 0
        Top = 62
        Width = 403
        Height = 60
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitTop = 62
        ExplicitWidth = 403
        ExplicitHeight = 60
        inherited sgPesquisa: TGridLuka
          Width = 378
          Height = 43
          ExplicitWidth = 378
          ExplicitHeight = 43
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 54
            ExplicitWidth = 54
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          Height = 44
          ExplicitLeft = 378
          ExplicitHeight = 44
        end
      end
      inline FrCodigoTitulo: TFrameInteiros
        Left = 648
        Top = 90
        Width = 117
        Height = 84
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 13
        TabStop = True
        ExplicitLeft = 648
        ExplicitTop = 90
        ExplicitWidth = 117
        inherited sgInteiros: TGridLuka
          Width = 117
          ExplicitWidth = 117
        end
        inherited Panel1: TPanel
          Width = 117
          Caption = 'C'#243'digo do t'#237'tulo'
          ExplicitWidth = 117
        end
      end
      inline FrCodigoBaixa: TFrameInteiros
        Left = 648
        Top = 180
        Width = 117
        Height = 84
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 14
        TabStop = True
        ExplicitLeft = 648
        ExplicitTop = 180
        ExplicitWidth = 117
        inherited sgInteiros: TGridLuka
          Width = 117
          ExplicitWidth = 117
        end
        inherited Panel1: TPanel
          Width = 117
          Caption = 'C'#243'digo da Baixa'
          ExplicitWidth = 117
        end
      end
      inline FrCodigoAcumulado: TFrameInteiros
        Left = 648
        Top = 360
        Width = 117
        Height = 84
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 16
        TabStop = True
        ExplicitLeft = 648
        ExplicitTop = 360
        ExplicitWidth = 117
        inherited sgInteiros: TGridLuka
          Width = 117
          ExplicitWidth = 117
        end
        inherited Panel1: TPanel
          Width = 117
          Caption = 'C'#243'digo do Acumulado'
          ExplicitWidth = 117
        end
      end
      object ckTrazerCreditosPagar: TCheckBoxLuka
        Left = 428
        Top = 431
        Width = 178
        Height = 13
        Caption = 'Cr'#233'ditos a pagar de clientes'
        TabOrder = 17
        WordWrap = True
        CheckedStr = 'N'
      end
      inline FrDocumento: TFrTextos
        Left = 648
        Top = 0
        Width = 117
        Height = 84
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 12
        TabStop = True
        ExplicitLeft = 648
        ExplicitWidth = 117
        ExplicitHeight = 84
        inherited sgTextos: TGridLuka
          Width = 117
          Height = 70
          DefaultColWidth = 108
          ExplicitWidth = 117
          ExplicitHeight = 70
          ColWidths = (
            108)
        end
        inherited pnDescricao: TPanel
          Width = 117
          Caption = 'Documento'
          ExplicitWidth = 117
        end
      end
      inline FrVendedores: TFrVendedores
        Left = 0
        Top = 308
        Width = 404
        Height = 60
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 18
        TabStop = True
        ExplicitTop = 308
        ExplicitWidth = 404
        ExplicitHeight = 60
        inherited sgPesquisa: TGridLuka
          Width = 379
          Height = 43
          ExplicitWidth = 379
          ExplicitHeight = 43
        end
        inherited PnTitulos: TPanel
          Width = 404
          ExplicitWidth = 404
          inherited lbNomePesquisa: TLabel
            Width = 65
            ExplicitWidth = 65
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 299
            ExplicitLeft = 299
          end
        end
        inherited pnPesquisa: TPanel
          Left = 379
          Height = 44
          ExplicitLeft = 379
          ExplicitHeight = 44
        end
      end
      inline FrValorDocumento: TFrFaixaNumeros
        Left = 416
        Top = 231
        Width = 194
        Height = 41
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 19
        TabStop = True
        ExplicitLeft = 416
        ExplicitTop = 231
        ExplicitWidth = 194
        inherited lb1: TLabel
          Width = 124
          Height = 14
          Caption = 'Valor documento entre'
          ExplicitWidth = 124
          ExplicitHeight = 14
        end
        inherited lb2: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eValor1: TEditLuka
          Height = 22
          ExplicitHeight = 22
        end
        inherited eValor2: TEditLuka
          Left = 108
          Width = 82
          Height = 22
          ExplicitLeft = 108
          ExplicitWidth = 82
          ExplicitHeight = 22
        end
      end
      inline FrContasCustodia: TFrContasCustodia
        Left = 0
        Top = 369
        Width = 402
        Height = 60
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 20
        TabStop = True
        ExplicitTop = 369
        ExplicitWidth = 402
        ExplicitHeight = 60
        inherited sgPesquisa: TGridLuka
          Width = 377
          Height = 43
          ExplicitWidth = 377
          ExplicitHeight = 43
        end
        inherited PnTitulos: TPanel
          Width = 402
          ExplicitWidth = 402
          inherited lbNomePesquisa: TLabel
            Width = 87
            ExplicitWidth = 87
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 297
            ExplicitLeft = 297
          end
        end
        inherited pnPesquisa: TPanel
          Left = 377
          Height = 44
          ExplicitLeft = 377
          ExplicitHeight = 44
        end
      end
      inline FrListaNegra: TFrListaNegra
        Left = 0
        Top = 432
        Width = 401
        Height = 60
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 21
        TabStop = True
        ExplicitTop = 432
        ExplicitWidth = 401
        ExplicitHeight = 60
        inherited sgPesquisa: TGridLuka
          Width = 376
          Height = 43
          ExplicitWidth = 376
          ExplicitHeight = 43
        end
        inherited PnTitulos: TPanel
          Width = 401
          ExplicitWidth = 401
          inherited lbNomePesquisa: TLabel
            Width = 60
            ExplicitWidth = 60
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 296
            ExplicitLeft = 296
          end
        end
        inherited pnPesquisa: TPanel
          Left = 376
          Height = 44
          ExplicitLeft = 376
          ExplicitHeight = 44
        end
      end
      inline FrDataEmissao: TFrDataInicialFinal
        Left = 416
        Top = 191
        Width = 197
        Height = 42
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 22
        TabStop = True
        ExplicitLeft = 416
        ExplicitTop = 191
        ExplicitWidth = 197
        ExplicitHeight = 42
        inherited Label1: TLabel
          Width = 93
          Height = 14
          Caption = 'Data de emiss'#227'o'
          ExplicitWidth = 93
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrGruposClientes: TFrGruposClientes
        Left = 0
        Top = 495
        Width = 404
        Height = 60
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 23
        TabStop = True
        ExplicitTop = 495
        ExplicitWidth = 404
        ExplicitHeight = 60
        inherited sgPesquisa: TGridLuka
          Width = 379
          Height = 43
          ExplicitWidth = 379
          ExplicitHeight = 43
        end
        inherited PnTitulos: TPanel
          Width = 404
          ExplicitWidth = 404
          inherited lbNomePesquisa: TLabel
            Width = 103
            ExplicitWidth = 103
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 299
            ExplicitLeft = 299
          end
        end
        inherited pnPesquisa: TPanel
          Left = 379
          Height = 44
          ExplicitLeft = 379
          ExplicitHeight = 44
        end
      end
      object eNomeEmitenteCheque: TEditLuka
        Left = 648
        Top = 469
        Width = 229
        Height = 22
        CharCase = ecUpperCase
        TabOrder = 25
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object cbAgrupamento: TComboBoxLuka
        Left = 648
        Top = 515
        Width = 141
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 26
        Text = 'Nenhum'
        Items.Strings = (
          'Nenhum'
          'Cliente')
        Valores.Strings = (
          'NEN'
          'CLI')
        AsInt = 0
        AsString = 'NEN'
      end
      inline FrPlanosFinanceiros: TFrPlanosFinanceiros
        Left = 1
        Top = 557
        Width = 403
        Height = 60
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 24
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 557
        ExplicitWidth = 403
        ExplicitHeight = 60
        inherited sgPesquisa: TGridLuka
          Width = 378
          Height = 43
          ExplicitWidth = 378
          ExplicitHeight = 43
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 100
            ExplicitWidth = 100
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          Height = 44
          ExplicitLeft = 378
          ExplicitHeight = 44
        end
      end
    end
    object TabSheet1: TTabSheet [1]
      Caption = 'Outros filtros'
      ImageIndex = 2
      object lb2: TLabel
        Left = 620
        Top = 162
        Width = 97
        Height = 14
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alCustom
        Caption = 'Situa'#231#227'o remessa'
      end
      object Label3: TLabel
        Left = 620
        Top = 235
        Width = 92
        Height = 14
        Caption = 'Caminho arquivo'
        Enabled = False
      end
      object sbPesquisaCaminhoArquivo: TSpeedButtonLuka
        Left = 865
        Top = 249
        Width = 20
        Height = 22
        Enabled = False
        Flat = True
        OnClick = sbPesquisaCaminhoArquivoClick
        TagImagem = 5
        PedirCertificacao = False
        PermitirAutOutroUsuario = False
      end
      object gbOrigem: TGroupBoxLuka
        Left = 3
        Top = 3
        Width = 242
        Height = 147
        Caption = 'Origem'
        TabOrder = 0
        OpcaoMarcarDesmarcar = True
        object ckNfe: TCheckBoxLuka
          Left = 6
          Top = 18
          Width = 139
          Height = 17
          Caption = 'Manual'
          Checked = True
          State = cbChecked
          TabOrder = 0
          CheckedStr = 'S'
          Valor = 'MAN'
        end
        object ckNFCe: TCheckBoxLuka
          Left = 6
          Top = 33
          Width = 138
          Height = 17
          Caption = 'Pedidos'
          Checked = True
          State = cbChecked
          TabOrder = 1
          CheckedStr = 'S'
          Valor = 'ORC'
        end
        object CheckBoxLuka1: TCheckBoxLuka
          Left = 6
          Top = 48
          Width = 184
          Height = 17
          Caption = 'Baixa de contas a receber'
          Checked = True
          State = cbChecked
          TabOrder = 2
          CheckedStr = 'S'
          Valor = 'BXR'
        end
        object CheckBoxLuka3: TCheckBoxLuka
          Left = 6
          Top = 78
          Width = 195
          Height = 17
          Caption = 'Diferen'#231'a de turno'
          Checked = True
          State = cbChecked
          TabOrder = 3
          CheckedStr = 'S'
          Valor = 'DTU'
        end
        object CheckBoxLuka4: TCheckBoxLuka
          Left = 6
          Top = 93
          Width = 195
          Height = 17
          Caption = 'Acumulado'
          Checked = True
          State = cbChecked
          TabOrder = 4
          CheckedStr = 'S'
          Valor = 'ACU'
        end
        object CheckBoxLuka5: TCheckBoxLuka
          Left = 6
          Top = 108
          Width = 224
          Height = 17
          Caption = 'Adiantamento pedido acumulado'
          Checked = True
          State = cbChecked
          TabOrder = 5
          CheckedStr = 'S'
          Valor = 'ADA'
        end
        object CheckBoxLuka6: TCheckBoxLuka
          Left = 6
          Top = 123
          Width = 195
          Height = 17
          Caption = 'Devolu'#231#227'o de entrada de NF'
          Checked = True
          State = cbChecked
          TabOrder = 6
          CheckedStr = 'S'
          Valor = 'DEN'
        end
        object CheckBoxLuka10: TCheckBoxLuka
          Left = 6
          Top = 63
          Width = 233
          Height = 17
          Caption = 'Baixa de contas a pagar'
          Checked = True
          State = cbChecked
          TabOrder = 7
          CheckedStr = 'S'
          Valor = 'BAP'
        end
      end
      inline FrNossoNumero: TFrTextos
        Left = 620
        Top = 3
        Width = 117
        Height = 147
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 620
        ExplicitTop = 3
        ExplicitWidth = 117
        ExplicitHeight = 147
        inherited sgTextos: TGridLuka
          Width = 117
          Height = 133
          DefaultColWidth = 108
          ExplicitWidth = 117
          ExplicitHeight = 133
          ColWidths = (
            108)
        end
        inherited pnDescricao: TPanel
          Width = 117
          Caption = 'Nosso n'#250'mero'
          ExplicitWidth = 117
        end
      end
      object cbSituacaoRemessa: TComboBoxLuka
        Left = 620
        Top = 179
        Width = 185
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 2
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'N'#227'o filtrar'
          'Apenas n'#227'o gerados'
          'Gerados')
        Valores.Strings = (
          'NaoFiltrar'
          'NaoGerado'
          'Gerado')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      object ckProcessarArquivoRetorno: TCheckBoxLuka
        Left = 620
        Top = 217
        Width = 194
        Height = 13
        Caption = 'Processar arquivo retorno'
        TabOrder = 3
        WordWrap = True
        OnClick = ckProcessarArquivoRetornoClick
        CheckedStr = 'N'
      end
      object eCaminhoArquivo: TEditLuka
        Left = 620
        Top = 249
        Width = 238
        Height = 22
        CharCase = ecUpperCase
        Enabled = False
        ReadOnly = True
        TabOrder = 4
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 919
      ExplicitHeight = 635
      object pcResultado: TPageControl
        Left = 0
        Top = 0
        Width = 919
        Height = 635
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        ActivePage = tsAnalitico
        Align = alClient
        TabOrder = 0
        object tsAnalitico: TTabSheet
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Caption = 'Anal'#237'tico'
          DesignSize = (
            911
            606)
          object sgTitulos: TGridLuka
            Left = -4
            Top = 3
            Width = 912
            Height = 415
            Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
            Anchors = [akLeft, akTop, akRight]
            ColCount = 19
            DefaultRowHeight = 19
            DrawingStyle = gdsGradient
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
            PopupMenu = pmRotinas
            TabOrder = 0
            OnDblClick = sgTitulosDblClick
            OnDrawCell = sgTitulosDrawCell
            OnKeyDown = sgTitulosKeyDown
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 38619
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clActiveCaption
            HCol.Strings = (
              'Sel.?'
              'C'#243'digo'
              'Documento'
              'Cliente'
              'Sit.'
              'Vlr. Pedido/Acumul.'
              'Parc.'
              'Valor'
              'Adiant.'
              'Multa'
              'Juros'
              'Reten'#231#227'o'
              'Valor l'#237'quido'
              'Data Vencto.'
              'Data cad.'
              'Dias atra.'
              'Empresa'
              'Tipo de cobran'#231'a'
              'Portador')
            OnGetCellColor = sgTitulosGetCellColor
            OnGetCellPicture = sgTitulosGetCellPicture
            Grid3D = False
            RealColCount = 20
            OrdenarOnClick = True
            Indicador = True
            AtivarPopUpSelecao = False
            ColWidths = (
              32
              53
              108
              200
              24
              112
              30
              67
              52
              50
              50
              63
              79
              75
              75
              61
              159
              164
              169)
          end
          object pnAnalitico: TPanel
            Left = -1
            Top = 418
            Width = 913
            Height = 86
            Margins.Left = 0
            Margins.Top = 0
            Margins.Right = 0
            Margins.Bottom = 0
            Anchors = [akLeft, akTop, akRight, akBottom]
            BevelOuter = bvNone
            TabOrder = 1
            object st3: TStaticText
              Left = 0
              Top = 43
              Width = 876
              Height = 16
              Alignment = taCenter
              AutoSize = False
              BevelInner = bvNone
              BevelKind = bkFlat
              Caption = 'Total dos t'#237'tulos'
              Color = 15395562
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              TabOrder = 0
              Transparent = False
            end
            object st5: TStaticText
              Left = 229
              Top = 13
              Width = 126
              Height = 16
              Alignment = taCenter
              AutoSize = False
              BevelInner = bvNone
              BevelKind = bkFlat
              Caption = 'Cobran'#231'a'
              Color = 15395562
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              TabOrder = 1
              Transparent = False
            end
            object st1: TStaticText
              Left = 115
              Top = 13
              Width = 115
              Height = 16
              Alignment = taCenter
              AutoSize = False
              BevelInner = bvNone
              BevelKind = bkFlat
              Caption = 'Cart'#227'o'
              Color = 15395562
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              TabOrder = 2
              Transparent = False
            end
            object st4: TStaticText
              Left = 1
              Top = 13
              Width = 115
              Height = 16
              Alignment = taCenter
              AutoSize = False
              BevelInner = bvNone
              BevelKind = bkFlat
              Caption = 'Cheque'
              Color = 15395562
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              TabOrder = 3
              Transparent = False
            end
            object st6: TStaticText
              Left = 596
              Top = 13
              Width = 150
              Height = 16
              Alignment = taCenter
              AutoSize = False
              BevelInner = bvNone
              BevelKind = bkFlat
              Caption = 'Total'
              Color = 15395562
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              TabOrder = 4
              Transparent = False
            end
            object st7: TStaticText
              Left = 745
              Top = 13
              Width = 131
              Height = 16
              Alignment = taCenter
              AutoSize = False
              BevelInner = bvNone
              BevelKind = bkFlat
              Caption = 'Qtde. t'#237'tulos'
              Color = 15395562
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              TabOrder = 5
              Transparent = False
            end
            object st8: TStaticText
              Left = 354
              Top = 13
              Width = 129
              Height = 16
              Alignment = taCenter
              AutoSize = False
              BevelInner = bvNone
              BevelKind = bkFlat
              Caption = 'Acumulado'
              Color = 15395562
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              TabOrder = 6
              Transparent = False
            end
            object st2: TStaticText
              Left = 0
              Top = -2
              Width = 876
              Height = 16
              Alignment = taCenter
              AutoSize = False
              BevelInner = bvNone
              BevelKind = bkFlat
              Caption = 'Valores por formas de pagamento dos t'#237'tulos selecionados'
              Color = 15395562
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              TabOrder = 7
              Transparent = False
            end
            object stValorCheque: TStaticTextLuka
              Left = 1
              Top = 58
              Width = 115
              Height = 16
              Alignment = taCenter
              AutoSize = False
              BevelInner = bvNone
              BevelKind = bkFlat
              Caption = '0,00'
              Color = clWhite
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGreen
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              TabOrder = 8
              Transparent = False
              AsInt = 0
              TipoCampo = tcNumerico
              CasasDecimais = 2
            end
            object stValorCartao: TStaticTextLuka
              Left = 115
              Top = 58
              Width = 115
              Height = 16
              Alignment = taCenter
              AutoSize = False
              BevelInner = bvNone
              BevelKind = bkFlat
              Caption = '0,00'
              Color = clWhite
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clPurple
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              TabOrder = 9
              Transparent = False
              AsInt = 0
              TipoCampo = tcNumerico
              CasasDecimais = 2
            end
            object stValorCobranca: TStaticTextLuka
              Left = 229
              Top = 58
              Width = 126
              Height = 16
              Alignment = taCenter
              AutoSize = False
              BevelInner = bvNone
              BevelKind = bkFlat
              Caption = '0,00'
              Color = clWhite
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clTeal
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              TabOrder = 10
              Transparent = False
              AsInt = 0
              TipoCampo = tcNumerico
              CasasDecimais = 2
            end
            object stValorAcumulado: TStaticTextLuka
              Left = 354
              Top = 58
              Width = 129
              Height = 16
              Alignment = taCenter
              AutoSize = False
              BevelInner = bvNone
              BevelKind = bkFlat
              Caption = '0,00'
              Color = clWhite
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 16711808
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              TabOrder = 11
              Transparent = False
              AsInt = 0
              TipoCampo = tcNumerico
              CasasDecimais = 2
            end
            object stValorTotal: TStaticTextLuka
              Left = 596
              Top = 58
              Width = 150
              Height = 16
              Alignment = taCenter
              AutoSize = False
              BevelInner = bvNone
              BevelKind = bkFlat
              Caption = '0,00'
              Color = clWhite
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              TabOrder = 12
              Transparent = False
              AsInt = 0
              TipoCampo = tcNumerico
              CasasDecimais = 2
            end
            object stQuantidadeTitulos: TStaticTextLuka
              Left = 745
              Top = 58
              Width = 131
              Height = 16
              Alignment = taCenter
              AutoSize = False
              BevelInner = bvNone
              BevelKind = bkFlat
              Color = clWhite
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              TabOrder = 13
              Transparent = False
              AsInt = 0
              TipoCampo = tcNumerico
              CasasDecimais = 0
            end
            object stValorChequeSelecionados: TStaticTextLuka
              Left = 1
              Top = 28
              Width = 115
              Height = 16
              Alignment = taCenter
              AutoSize = False
              BevelInner = bvNone
              BevelKind = bkFlat
              Caption = '0,00'
              Color = clWhite
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGreen
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              TabOrder = 14
              Transparent = False
              AsInt = 0
              TipoCampo = tcNumerico
              CasasDecimais = 2
            end
            object stValorCartaoSelecionados: TStaticTextLuka
              Left = 115
              Top = 28
              Width = 115
              Height = 16
              Alignment = taCenter
              AutoSize = False
              BevelInner = bvNone
              BevelKind = bkFlat
              Caption = '0,00'
              Color = clWhite
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clPurple
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              TabOrder = 15
              Transparent = False
              AsInt = 0
              TipoCampo = tcNumerico
              CasasDecimais = 2
            end
            object stValorCobrancaSelecionados: TStaticTextLuka
              Left = 229
              Top = 28
              Width = 126
              Height = 16
              Alignment = taCenter
              AutoSize = False
              BevelInner = bvNone
              BevelKind = bkFlat
              Caption = '0,00'
              Color = clWhite
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clTeal
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              TabOrder = 16
              Transparent = False
              AsInt = 0
              TipoCampo = tcNumerico
              CasasDecimais = 2
            end
            object stValorAcumuladosSelecionados: TStaticTextLuka
              Left = 354
              Top = 28
              Width = 129
              Height = 16
              Alignment = taCenter
              AutoSize = False
              BevelInner = bvNone
              BevelKind = bkFlat
              Caption = '0,00'
              Color = clWhite
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 16711808
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              TabOrder = 17
              Transparent = False
              AsInt = 0
              TipoCampo = tcNumerico
              CasasDecimais = 2
            end
            object stValorTotalSelecionados: TStaticTextLuka
              Left = 596
              Top = 28
              Width = 150
              Height = 16
              Alignment = taCenter
              AutoSize = False
              BevelInner = bvNone
              BevelKind = bkFlat
              Caption = '0,00'
              Color = clWhite
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              TabOrder = 18
              Transparent = False
              AsInt = 0
              TipoCampo = tcNumerico
              CasasDecimais = 2
            end
            object stQtdeTitulosSelecionados: TStaticTextLuka
              Left = 745
              Top = 28
              Width = 131
              Height = 16
              Alignment = taCenter
              AutoSize = False
              BevelInner = bvNone
              BevelKind = bkFlat
              Color = clWhite
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              TabOrder = 19
              Transparent = False
              AsInt = 0
              TipoCampo = tcNumerico
              CasasDecimais = 0
            end
            object StaticText1: TStaticText
              Left = 482
              Top = 13
              Width = 115
              Height = 16
              Alignment = taCenter
              AutoSize = False
              BevelInner = bvNone
              BevelKind = bkFlat
              Caption = 'Reten'#231#227'o'
              Color = 15395562
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              TabOrder = 20
              Transparent = False
            end
            object stValorRetencaoSelecionados: TStaticTextLuka
              Left = 482
              Top = 28
              Width = 115
              Height = 16
              Alignment = taCenter
              AutoSize = False
              BevelInner = bvNone
              BevelKind = bkFlat
              Caption = '0,00'
              Color = clWhite
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              TabOrder = 21
              Transparent = False
              AsInt = 0
              TipoCampo = tcNumerico
              CasasDecimais = 2
            end
            object stValorRetencao: TStaticTextLuka
              Left = 482
              Top = 58
              Width = 115
              Height = 16
              Alignment = taCenter
              AutoSize = False
              BevelInner = bvNone
              BevelKind = bkFlat
              Caption = '0,00'
              Color = clWhite
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              TabOrder = 22
              Transparent = False
              AsInt = 0
              TipoCampo = tcNumerico
              CasasDecimais = 2
            end
          end
          object Panel1: TPanel
            Left = -2
            Top = 495
            Width = 920
            Height = 114
            Anchors = [akLeft, akTop, akRight, akBottom]
            BevelKind = bkTile
            BevelOuter = bvNone
            Color = 15395562
            ParentBackground = False
            TabOrder = 2
            object miMarcarDesmarcarTItuloFocado: TSpeedButton
              Left = 7
              Top = -1
              Width = 204
              Height = 25
              BiDiMode = bdRightToLeft
              Caption = 'Selecionar t'#237'tulos (Espa'#231'o)'
              Flat = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Calibri'
              Font.Style = [fsBold]
              ParentFont = False
              ParentBiDiMode = False
              OnClick = miMarcarDesmarcarTItuloFocadoClick
            end
            object miRealizarAdiantamentoTituloFocado: TSpeedButton
              Left = 7
              Top = 44
              Width = 204
              Height = 25
              BiDiMode = bdRightToLeft
              Caption = 'Adiantantamento de t'#237'tulo selecionado'
              Flat = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Calibri'
              Font.Style = [fsBold]
              ParentFont = False
              ParentBiDiMode = False
              OnClick = miRealizarAdiantamentoTituloFocadoClick
            end
            object miMarcarDesmarcarTodosTitulos: TSpeedButton
              Left = 7
              Top = 21
              Width = 204
              Height = 25
              BiDiMode = bdRightToLeft
              Caption = 'Selec. todos os t'#237'tulos (CRTL + Espa'#231'o)'
              Flat = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Calibri'
              Font.Style = [fsBold]
              ParentFont = False
              ParentBiDiMode = False
              OnClick = miMarcarDesmarcarTodosTitulosClick
            end
            object miAlterarInformacoes: TSpeedButton
              Left = 228
              Top = 24
              Width = 162
              Height = 25
              BiDiMode = bdRightToLeft
              Caption = 'Alterar data de vencimento'
              Flat = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Calibri'
              Font.Style = [fsBold]
              ParentFont = False
              ParentBiDiMode = False
              OnClick = miAlterarInformacoesClick
            end
            object miDefinirContaCustodia: TSpeedButton
              Left = 640
              Top = 21
              Width = 166
              Height = 25
              BiDiMode = bdLeftToRight
              Caption = 'Definir conta cust'#243'dia'
              Flat = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Calibri'
              Font.Style = [fsBold]
              ParentFont = False
              ParentBiDiMode = False
              OnClick = miDefinirContaCustodiaClick
            end
            object miExcluirTitulo: TSpeedButton
              Left = 446
              Top = 44
              Width = 162
              Height = 25
              BiDiMode = bdRightToLeft
              Caption = 'Excluir t'#237'tulo (Selecionado)'
              Flat = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Calibri'
              Font.Style = [fsBold]
              ParentFont = False
              ParentBiDiMode = False
              OnClick = miExcluirTituloClick
            end
            object miReimprimirDuplicataMercantilVenda: TSpeedButton
              Left = 435
              Top = -1
              Width = 179
              Height = 25
              BiDiMode = bdRightToLeft
              Caption = 'Reimprimir duplicata (Selecionda)'
              Flat = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Calibri'
              Font.Style = [fsBold]
              ParentFont = False
              ParentBiDiMode = False
              OnClick = miReimprimirDuplicataMercantilVendaClick
            end
            object miEmitirBoleto: TSpeedButton
              Left = 640
              Top = 68
              Width = 179
              Height = 25
              BiDiMode = bdRightToLeft
              Caption = 'Emitir boletos (Selecionados)'
              Flat = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Calibri'
              Font.Style = [fsBold]
              ParentFont = False
              ParentBiDiMode = False
              Visible = False
              OnClick = miEmitirBoletoClick
            end
            object miEnviarParaRecebimentoCaixa: TSpeedButton
              Left = 435
              Top = 72
              Width = 179
              Height = 25
              BiDiMode = bdRightToLeft
              Caption = 'Enviar p/ receb. no caixa'
              Flat = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Calibri'
              Font.Style = [fsBold]
              ParentFont = False
              ParentBiDiMode = False
              Visible = False
              OnClick = miEnviarParaRecebimentoCaixaClick
            end
            object miIncluirNovoTitulo: TSpeedButton
              Left = 435
              Top = 21
              Width = 179
              Height = 25
              BiDiMode = bdRightToLeft
              Caption = 'Incluir novo t'#237'tulo'
              Flat = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Calibri'
              Font.Style = [fsBold]
              ParentFont = False
              ParentBiDiMode = False
              OnClick = miIncluirNovoTituloClick
            end
            object miAlteraVendedor: TSpeedButton
              Left = 227
              Top = 44
              Width = 161
              Height = 25
              BiDiMode = bdRightToLeft
              Caption = 'Alterar Vendedor'
              Flat = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Calibri'
              Font.Style = [fsBold]
              ParentFont = False
              ParentBiDiMode = False
              OnClick = miAlteraVendedorClick
            end
            object miAlterarCampoDocumento: TSpeedButton
              Left = 224
              Top = -1
              Width = 166
              Height = 25
              BiDiMode = bdLeftToRight
              Caption = 'Alterar n'#250'mero do doc.'
              Flat = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Calibri'
              Font.Style = [fsBold]
              ParentFont = False
              ParentBiDiMode = False
              OnClick = miAlterarCampoDocumentoClick
            end
            object SpeedButton2: TSpeedButton
              Left = 640
              Top = -1
              Width = 166
              Height = 25
              BiDiMode = bdLeftToRight
              Caption = 'Alterar centro de custo'
              Flat = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Calibri'
              Font.Style = [fsBold]
              ParentFont = False
              ParentBiDiMode = False
              OnClick = SpeedButton2Click
            end
            object SpeedButton3: TSpeedButton
              Left = 640
              Top = 45
              Width = 166
              Height = 25
              BiDiMode = bdLeftToRight
              Caption = 'Alterar valor de reten'#231#227'o'
              Flat = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Calibri'
              Font.Style = [fsBold]
              ParentFont = False
              ParentBiDiMode = False
              OnClick = SpeedButton3Click
            end
            object SpeedButton4: TSpeedButton
              Left = 7
              Top = 68
              Width = 204
              Height = 25
              BiDiMode = bdRightToLeft
              Caption = 'Editar observa'#231#245'es de t'#237'tulo selecionado'
              Flat = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Calibri'
              Font.Style = [fsBold]
              ParentFont = False
              ParentBiDiMode = False
              OnClick = SpeedButton4Click
            end
          end
        end
        object tsSinteticoTipoCobranca: TTabSheet
          Caption = 'Sint'#233'tico por tipo de cobran'#231'a'
          ImageIndex = 1
          object sgTitulosTipoCobranca: TGridLuka
            Left = 0
            Top = 0
            Width = 911
            Height = 156
            Align = alClient
            ColCount = 7
            DefaultRowHeight = 19
            DrawingStyle = gdsGradient
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect, goFixedRowClick]
            TabOrder = 0
            OnDrawCell = sgTitulosTipoCobrancaDrawCell
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 38619
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clActiveCaption
            HCol.Strings = (
              'Tipo cobran'#231'a'
              'Valor'
              'Multa'
              'Juros'
              'Reten'#231#245'es'
              'Valor l'#237'quido'
              'Qtde. t'#237'tulos')
            OnGetCellColor = sgTitulosTipoCobrancaGetCellColor
            Grid3D = False
            RealColCount = 15
            OrdenarOnClick = True
            Indicador = True
            AtivarPopUpSelecao = False
            ColWidths = (
              301
              85
              71
              69
              76
              89
              86)
          end
          object pnGraficoTiposCobranca: TPanel
            Left = 0
            Top = 156
            Width = 911
            Height = 450
            Margins.Left = 0
            Margins.Top = 0
            Margins.Right = 0
            Margins.Bottom = 0
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            object chTiposCobranca: TChart
              Left = 185
              Top = -12
              Width = 691
              Height = 462
              BackImage.Mode = pbmCenter
              BackImageMode = pbmCenter
              Legend.FontSeriesColor = True
              Legend.TextStyle = ltsLeftPercent
              Legend.Title.Alignment = taRightJustify
              Legend.Visible = False
              PrintProportional = False
              Title.Font.Height = -12
              Title.Font.Style = [fsBold]
              Title.Text.Strings = (
                'Representa'#231#227'o gr'#225'fica(Valor total)')
              View3DOptions.Elevation = 315
              View3DOptions.Orthogonal = False
              View3DOptions.Perspective = 0
              View3DOptions.Rotation = 360
              View3DOptions.Zoom = 129
              BevelOuter = bvNone
              Color = clWhite
              TabOrder = 0
              DefaultCanvas = 'TGDIPlusCanvas'
              PrintMargins = (
                15
                26
                15
                26)
              ColorPaletteIndex = 13
              object psrsSeries1: TPieSeries
                XValues.Order = loAscending
                YValues.Name = 'Pie'
                YValues.Order = loNone
                Frame.InnerBrush.BackColor = clRed
                Frame.InnerBrush.Gradient.EndColor = clGray
                Frame.InnerBrush.Gradient.MidColor = clWhite
                Frame.InnerBrush.Gradient.StartColor = 4210752
                Frame.InnerBrush.Gradient.Visible = True
                Frame.MiddleBrush.BackColor = clYellow
                Frame.MiddleBrush.Gradient.EndColor = 8553090
                Frame.MiddleBrush.Gradient.MidColor = clWhite
                Frame.MiddleBrush.Gradient.StartColor = clGray
                Frame.MiddleBrush.Gradient.Visible = True
                Frame.OuterBrush.BackColor = clGreen
                Frame.OuterBrush.Gradient.EndColor = 4210752
                Frame.OuterBrush.Gradient.MidColor = clWhite
                Frame.OuterBrush.Gradient.StartColor = clSilver
                Frame.OuterBrush.Gradient.Visible = True
                Frame.Width = 4
                OtherSlice.Legend.Visible = False
              end
            end
            object rgTipoRepresentacaoGrafico: TRadioGroup
              Left = 0
              Top = 1
              Width = 185
              Height = 56
              Caption = ' Representa'#231#227'o por  '
              Color = clWhite
              ItemIndex = 0
              Items.Strings = (
                'Valor total'
                'Quantidade de t'#237'tulos')
              ParentBackground = False
              ParentColor = False
              TabOrder = 1
              OnClick = rgTipoRepresentacaoGraficoClick
            end
          end
        end
      end
    end
  end
  object pmRotinas: TPopupMenu
    OnPopup = pmRotinasPopup
    Left = 931
    Top = 211
  end
  object frxReport: TfrxReport
    Version = '5.2.3'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44043.704685613400000000
    ReportOptions.LastChange = 44105.845682627300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 936
    Top = 264
    Datasets = <
      item
        DataSet = dmRelatorio.dstEmpresa
        DataSetName = 'frxdstEmpresa'
      end
      item
        DataSet = dstReceber
        DataSetName = 'frxdstReceber'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 86.929190000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          Top = 70.811070000000000000
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
        object Memo1: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000001000
          Width = 68.031540000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 26.456710000000000000
          Top = 34.015770000000010000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 15.118120000000000000
          Top = 49.133889999999990000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 340.157700000000000000
          Top = 3.779530000000001000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 321.260050000000000000
          Top = 34.015770000000010000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade/UF:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 340.157700000000000000
          Top = 49.133889999999990000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 82.708720000000000000
          Top = 3.779530000000001000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 383.512060000000000000
          Top = 3.779530000000000000
          Width = 200.315090000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."CNPJ"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 84.929190000000000000
          Top = 18.897650000000000000
          Width = 551.811380000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            
              '[frxdstEmpresa."LOGRADOURO"] [frxdstEmpresa."COMPLEMENTO"] n'#176'[fr' +
              'xdstEmpresa."NUMERO"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 82.708720000000000000
          Top = 34.015770000000010000
          Width = 207.874150000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_BAIRRO"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 383.512060000000000000
          Top = 34.015770000000010000
          Width = 185.196970000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_CIDADE"] / [frxdstEmpresa."ESTADO_ID"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 82.708720000000000000
          Top = 49.133889999999990000
          Width = 75.590600000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 383.512060000000000000
          Top = 49.133889999999990000
          Width = 260.787570000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."E_MAIL"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 545.252320000000000000
          Top = 3.779530000000001000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Emitido em [Date] '#224's [Time]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 2.779530000000000000
          Top = 70.811070000000000000
          Width = 37.795300000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 44.354360000000000000
          Top = 70.811070000000000000
          Width = 86.929190000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 435.645950000000000000
          Top = 70.811070000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Empresa')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 600.724800000000000000
          Top = 70.811070000000000000
          Width = 98.267779999999990000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Cobran'#231'a')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 251.228510000000000000
          Top = 70.811070000000000000
          Width = 22.677180000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Sit.')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 267.346630000000000000
          Top = 70.811070000000000000
          Width = 68.031540000000010000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Valor Liquido')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 403.189240000000000000
          Top = 70.811070000000000000
          Width = 37.795300000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Atraso')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 545.252320000000000000
          Top = 18.897650000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pag. [Page] de [TotalPages#]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo32: TfrxMemoView
          Left = 341.157700000000000000
          Top = 70.811070000000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Dta Vencto')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 166.299320000000000000
        Width = 718.110700000000000000
        DataSet = dstReceber
        DataSetName = 'frxdstReceber'
        RowCount = 0
        object frxdstTranscodigoProd: TfrxMemoView
          Left = 2.779530000000000000
          Top = 1.000000000000000000
          Width = 37.795300000000000000
          Height = 11.338590000000000000
          DataSet = dstReceber
          DataSetName = 'frxdstReceber'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstReceber."Codigo"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 44.354360000000000000
          Top = 1.000000000000000000
          Width = 204.094620000000000000
          Height = 11.338590000000000000
          DataSet = dstReceber
          DataSetName = 'frxdstReceber'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstReceber."NomeCliente"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 251.228510000000000000
          Top = 1.000000000000000000
          Width = 18.897650000000000000
          Height = 11.338590000000000000
          DataSet = dstReceber
          DataSetName = 'frxdstReceber'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstReceber."situacao"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 600.724800000000000000
          Top = 1.000000000000000000
          Width = 117.165430000000000000
          Height = 11.338590000000000000
          DataSet = dstReceber
          DataSetName = 'frxdstReceber'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstReceber."TipoCobranca"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 267.346630000000000000
          Top = 1.000000000000000000
          Width = 68.031540000000010000
          Height = 11.338590000000000000
          DataSet = dstReceber
          DataSetName = 'frxdstReceber'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstReceber."ValorLiquido"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 406.189240000000000000
          Top = 1.000000000000000000
          Width = 22.677180000000000000
          Height = 11.338590000000000000
          DataSet = dstReceber
          DataSetName = 'frxdstReceber'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstReceber."DiasAtraso"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 435.645950000000000000
          Top = 1.000000000000000000
          Width = 162.519790000000000000
          Height = 11.338590000000000000
          DataSet = dstReceber
          DataSetName = 'frxdstReceber'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstReceber."Empresa"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 341.157700000000000000
          Top = 1.000000000000000000
          Width = 60.472480000000000000
          Height = 11.338590000000000000
          DataSet = dstReceber
          DataSetName = 'frxdstReceber'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstReceber."DtVencto"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 98.645732760000000000
        Top = 241.889920000000000000
        Width = 718.110700000000000000
        object Line1: TfrxLineView
          Top = 3.779529999999994000
          Width = 718.110700000000000000
          StretchMode = smMaxHeight
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object mmValorProduto: TfrxMemoView
          Left = 7.559060000000000000
          Top = 11.338590000000000000
          Width = 498.897960000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total Valor Liquido.....: ')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 362.834880000000000000
        Width = 718.110700000000000000
        object Shape2: TfrxShapeView
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
      end
    end
  end
  object dstReceber: TfrxDBDataset
    UserName = 'frxdstReceber'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Documento=Documento'
      'NomeCliente=NomeCliente'
      'situacao=situacao'
      'Parcela=Parcela'
      'Valor=Valor'
      'Multa=Multa'
      'Juros=Juros'
      'Adiantamento=Adiantamento'
      'ValorLiquido=ValorLiquido'
      'DtVencto=DtVencto'
      'DtCadastro=DtCadastro'
      'Empresa=Empresa'
      'TipoCobranca=TipoCobranca'
      'DiasAtraso=DiasAtraso')
    DataSet = cdsReceber
    BCDToCurrency = False
    Left = 1000
    Top = 464
  end
  object cdsReceber: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Codigo'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Documento'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NomeCliente'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'situacao'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Parcela'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Valor'
        DataType = ftFloat
      end
      item
        Name = 'Multa'
        DataType = ftFloat
      end
      item
        Name = 'Juros'
        DataType = ftFloat
      end
      item
        Name = 'Adiantamento'
        DataType = ftFloat
      end
      item
        Name = 'ValorLiquido'
        DataType = ftFloat
      end
      item
        Name = 'DtVencto'
        DataType = ftDate
      end
      item
        Name = 'DtCadastro'
        DataType = ftDate
      end
      item
        Name = 'Empresa'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'TipoCobranca'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'DiasAtraso'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 936
    Top = 40
    object cdsReceberCodigo: TStringField
      FieldName = 'Codigo'
    end
    object cdsReceberDocumento: TStringField
      FieldName = 'Documento'
    end
    object cdsReceberNomeCliente: TStringField
      FieldName = 'NomeCliente'
      Size = 60
    end
    object cdsRecebersituacao: TStringField
      FieldName = 'situacao'
    end
    object cdsReceberValor: TFloatField
      FieldName = 'Valor'
    end
    object cdsReceberMulta: TFloatField
      FieldName = 'Multa'
    end
    object cdsReceberJuros: TFloatField
      FieldName = 'Juros'
    end
    object cdsReceberAdiantamento: TFloatField
      FieldName = 'Adiantamento'
    end
    object cdsReceberValorLiquido: TFloatField
      FieldName = 'ValorLiquido'
    end
    object cdsReceberDtVencto: TDateField
      FieldName = 'DtVencto'
    end
    object cdsReceberDtCadastro: TDateField
      FieldName = 'DtCadastro'
    end
    object cdsReceberEmpresa: TStringField
      FieldName = 'Empresa'
      Size = 60
    end
    object cdsReceberTipoCobranca: TStringField
      FieldName = 'TipoCobranca'
      Size = 40
    end
    object cdsReceberDiasAtraso: TIntegerField
      FieldName = 'DiasAtraso'
    end
    object cdsReceberParcela: TStringField
      FieldName = 'Parcela'
    end
  end
  object cdsOcorrencias: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 936
    Top = 320
    object cdsOcorrenciasRAZAO_SOCIAL: TStringField
      FieldName = 'RAZAO_SOCIAL'
      Size = 100
    end
    object cdsOcorrenciasDOCUMENTO_ID: TStringField
      FieldName = 'DOCUMENTO_ID'
      Size = 100
    end
    object cdsOcorrenciasNOSSO_NUMERO: TStringField
      FieldName = 'NOSSO_NUMERO'
      Size = 100
    end
    object cdsOcorrenciasVLR_ABATIMENTO: TFloatField
      FieldName = 'VLR_ABATIMENTO'
    end
    object cdsOcorrenciasVLR_TITULO: TFloatField
      FieldName = 'VLR_TITULO'
    end
    object cdsOcorrenciasPAGAMENTO: TStringField
      FieldName = 'PAGAMENTO'
      Size = 4
    end
    object cdsOcorrenciasDESC_OCORRENCIA: TStringField
      FieldName = 'DESC_OCORRENCIA'
      Size = 100
    end
    object cdsOcorrenciasDETALHES_OCORRENCIA: TStringField
      FieldName = 'DETALHES_OCORRENCIA'
      Size = 500
    end
    object cdsOcorrenciasCOD_OCORRENCIA: TStringField
      FieldName = 'COD_OCORRENCIA'
      Size = 2
    end
    object cdsOcorrenciasDIFERENCA: TFloatField
      FieldName = 'DIFERENCA'
    end
    object cdsOcorrenciasVLR_ORIGINAL: TFloatField
      FieldName = 'VLR_ORIGINAL'
    end
  end
  object cdsSumario: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 931
    Top = 98
    object cdsSumarioCOD_OCORRENCIA: TIntegerField
      FieldName = 'COD_OCORRENCIA'
    end
    object cdsSumarioOCORRENCIA: TStringField
      FieldName = 'OCORRENCIA'
      Size = 80
    end
    object cdsSumarioQTD: TIntegerField
      FieldName = 'QTD'
    end
    object cdsSumarioTOTAL: TFloatField
      FieldName = 'TOTAL'
    end
  end
  object cdsPortador: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 936
    Top = 160
    object cdsPortadorPORTADOR_ID: TIntegerField
      FieldName = 'PORTADOR_ID'
    end
    object cdsPortadorPORTADOR_NOME: TStringField
      FieldName = 'PORTADOR_NOME'
      Size = 100
    end
    object cdsPortadorCONTA_CORRENTE_ID: TStringField
      FieldName = 'CONTA_CORRENTE_ID'
      Size = 40
    end
    object cdsPortadorCONTA_CORRENTE: TStringField
      FieldName = 'CONTA_CORRENTE'
      Size = 100
    end
    object cdsPortadorVLR_PAGO: TFloatField
      FieldName = 'VLR_PAGO'
    end
    object cdsPortadorVLR_JUROS: TFloatField
      FieldName = 'VLR_JUROS'
    end
    object cdsPortadorTOTAL_BRUTO: TFloatField
      FieldName = 'TOTAL_BRUTO'
    end
    object cdsPortadorVLR_TAXAS: TFloatField
      FieldName = 'VLR_TAXAS'
    end
    object cdsPortadorTOTAL_LIQUIDO: TFloatField
      FieldName = 'TOTAL_LIQUIDO'
    end
  end
  object frxDBOcorrencias: TfrxDBDataset
    UserName = 'frxDBOcorrencias'
    CloseDataSource = False
    FieldAliases.Strings = (
      'RAZAO_SOCIAL=RAZAO_SOCIAL'
      'DOCUMENTO_ID=DOCUMENTO_ID'
      'NOSSO_NUMERO=NOSSO_NUMERO'
      'VLR_ABATIMENTO=VLR_ABATIMENTO'
      'VLR_TITULO=VLR_TITULO'
      'PAGAMENTO=PAGAMENTO'
      'DESC_OCORRENCIA=DESC_OCORRENCIA'
      'DETALHES_OCORRENCIA=DETALHES_OCORRENCIA'
      'COD_OCORRENCIA=COD_OCORRENCIA'
      'DIFERENCA=DIFERENCA'
      'VLR_ORIGINAL=VLR_ORIGINAL')
    DataSet = cdsOcorrencias
    BCDToCurrency = False
    Left = 1008
    Top = 220
  end
  object frxDBTitulos: TfrxDBDataset
    UserName = 'frxDBTitulos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'CADASTRO_ID=CADASTRO_ID'
      'RAZAO_SOCIAL=RAZAO_SOCIAL'
      'DOCUMENTO_ID=DOCUMENTO_ID'
      'NOSSO_NUMERO=NOSSO_NUMERO'
      'CODIGO_BANCO=CODIGO_BANCO'
      'DT_EMISSAO=DT_EMISSAO'
      'DT_VENCIMENTO=DT_VENCIMENTO'
      'VALOR_TITULO=VALOR_TITULO'
      'VLR_ADIANTAMENTO=VLR_ADIANTAMENTO')
    DataSet = cdsTitulos
    BCDToCurrency = False
    Left = 1000
    Top = 404
  end
  object frxDBSumario: TfrxDBDataset
    UserName = 'frxDBSumario'
    CloseDataSource = False
    FieldAliases.Strings = (
      'COD_OCORRENCIA=COD_OCORRENCIA'
      'OCORRENCIA=OCORRENCIA'
      'QTD=QTD'
      'TOTAL=TOTAL')
    DataSet = cdsSumario
    BCDToCurrency = False
    Left = 1004
    Top = 288
  end
  object frxDBPortador: TfrxDBDataset
    UserName = 'frxDBPortador'
    CloseDataSource = False
    FieldAliases.Strings = (
      'PORTADOR_ID=PORTADOR_ID'
      'PORTADOR_NOME=PORTADOR_NOME'
      'CONTA_CORRENTE_ID=CONTA_CORRENTE_ID'
      'CONTA_CORRENTE=CONTA_CORRENTE'
      'VLR_PAGO=VLR_PAGO'
      'VLR_JUROS=VLR_JUROS'
      'TOTAL_BRUTO=TOTAL_BRUTO'
      'VLR_TAXAS=VLR_TAXAS'
      'TOTAL_LIQUIDO=TOTAL_LIQUIDO')
    DataSet = cdsPortador
    BCDToCurrency = False
    Left = 1008
    Top = 344
  end
  object frxOcorrencias: TfrxReport
    Version = '5.2.3'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Padr'#227'o'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 45321.716118831000000000
    ReportOptions.LastChange = 45321.716118831000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 1000
    Top = 40
    Datasets = <
      item
        DataSet = frxDBOcorrencias
        DataSetName = 'frxDBOcorrencias'
      end
      item
        DataSet = frxDBPortador
        DataSetName = 'frxDBPortador'
      end
      item
        DataSet = frxDBSumario
        DataSetName = 'frxDBSumario'
      end
      item
        DataSet = dmRelatorio.dstEmpresa
        DataSetName = 'frxdstEmpresa'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 121.834725000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo3: TfrxMemoView
          Left = 338.386055000000000000
          Top = 1.889765000000000000
          Width = 376.945115000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Data de Emiss'#227'o: [Date]      P'#225'gina [Page] de [TotalPages#]')
          ParentFont = False
        end
        object lbTitulo: TfrxMemoView
          Top = 19.787415000000000000
          Width = 715.213050000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'EXTRATO DO PROCESSAMENTO DE ARQUIVO DE RETORNO')
          ParentFont = False
        end
        object Shape4: TfrxShapeView
          Top = 42.464595000000000000
          Width = 714.110700000000000000
          Height = 71.811070000000000000
          Shape = skRoundRectangle
        end
        object Memo4: TfrxMemoView
          Left = 7.559060000000000000
          Top = 49.133890000000000000
          Width = 68.031540000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 3.779530000000000000
          Top = 64.252010000000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 22.677180000000000000
          Top = 79.370130000000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 11.338590000000000000
          Top = 94.488250000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 336.378170000000000000
          Top = 49.133890000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 317.480520000000000000
          Top = 79.370130000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade/UF:')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 336.378170000000000000
          Top = 94.488250000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 78.929190000000000000
          Top = 49.133890000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 379.732530000000000000
          Top = 49.133890000000000000
          Width = 200.315090000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."CNPJ"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 81.149660000000000000
          Top = 64.252010000000000000
          Width = 551.811380000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            
              '[frxdstEmpresa."LOGRADOURO"] [frxdstEmpresa."COMPLEMENTO"] n'#176'[fr' +
              'xdstEmpresa."NUMERO"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 78.929190000000000000
          Top = 79.370130000000000000
          Width = 207.874150000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_BAIRRO"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 379.732530000000000000
          Top = 79.370130000000000000
          Width = 185.196970000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_CIDADE"] / [frxdstEmpresa."ESTADO_ID"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 78.929190000000000000
          Top = 94.488250000000000000
          Width = 75.590600000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 379.732530000000000000
          Top = 94.488250000000000000
          Width = 260.787570000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."E_MAIL"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 541.472790000000000000
          Top = 49.133890000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Emitido em [Date] '#224's [Time]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 541.472790000000000000
          Top = 64.252010000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pag. [Page] de [TotalPages#]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 68.000000000000000000
        Top = 200.315090000000000000
        Width = 718.110700000000000000
        object frxDBPortadorPORTADOR_ID: TfrxMemoView
          Left = 8.000000000000000000
          Top = 2.000000000000000000
          Width = 704.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBPortador
          DataSetName = 'frxDBPortador'
          Memo.UTF8W = (
            
              'Portador: [frxDBPortador."PORTADOR_ID"] - [frxDBPortador."PORTAD' +
              'OR_NOME"]')
        end
        object frxDBPortadorCONTA_CORRENTE_ID: TfrxMemoView
          Left = 8.000000000000000000
          Top = 22.000000000000000000
          Width = 704.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBPortador
          DataSetName = 'frxDBPortador'
          Memo.UTF8W = (
            
              'Conta corrente: [frxDBPortador."CONTA_CORRENTE_ID"] - [frxDBPort' +
              'ador."CONTA_CORRENTE"]')
        end
        object Memo1: TfrxMemoView
          Left = 9.000000000000000000
          Top = 46.000000000000000000
          Width = 125.322820000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 135.000000000000000000
          Top = 46.000000000000000000
          Width = 92.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Documento')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 228.000000000000000000
          Top = 46.000000000000000000
          Width = 108.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Nosso n'#250'mero')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 337.000000000000000000
          Top = 46.000000000000000000
          Width = 64.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Juros/Abat.')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 403.000000000000000000
          Top = 46.000000000000000000
          Width = 76.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 481.000000000000000000
          Top = 46.000000000000000000
          Width = 32.661410000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 515.000000000000000000
          Top = 46.000000000000000000
          Width = 201.354360000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Motivo')
          ParentFont = False
        end
      end
      object mdTitulo: TfrxMasterData
        FillType = ftBrush
        Height = 43.559060000000000000
        Top = 291.023810000000000000
        Width = 718.110700000000000000
        DataSet = frxDBOcorrencias
        DataSetName = 'frxDBOcorrencias'
        PrintIfDetailEmpty = True
        RowCount = 0
        Stretched = True
        object Line4: TfrxLineView
          Left = -0.110235000000000000
          Top = 0.037795279999999990
          Width = 710.110700000000000000
          Color = clBlack
          Frame.Color = clSilver
          Frame.ShadowColor = clSilver
          Diagonal = True
        end
        object Memo11: TfrxMemoView
          ShiftMode = smWhenOverlapped
          Left = 9.000000000000000000
          Top = 2.000000000000000000
          Width = 125.322820000000000000
          Height = 16.000000000000000000
          StretchMode = smMaxHeight
          DataField = 'RAZAO_SOCIAL'
          DataSet = frxDBOcorrencias
          DataSetName = 'frxDBOcorrencias'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBOcorrencias."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          ShiftMode = smWhenOverlapped
          Left = 135.000000000000000000
          Top = 2.000000000000000000
          Width = 92.000000000000000000
          Height = 16.000000000000000000
          DataField = 'DOCUMENTO_ID'
          DataSet = frxDBOcorrencias
          DataSetName = 'frxDBOcorrencias'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBOcorrencias."DOCUMENTO_ID"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          ShiftMode = smWhenOverlapped
          Left = 228.000000000000000000
          Top = 2.000000000000000000
          Width = 108.000000000000000000
          Height = 16.000000000000000000
          DataField = 'NOSSO_NUMERO'
          DataSet = frxDBOcorrencias
          DataSetName = 'frxDBOcorrencias'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBOcorrencias."NOSSO_NUMERO"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          ShiftMode = smWhenOverlapped
          Left = 337.000000000000000000
          Top = 2.000000000000000000
          Width = 64.000000000000000000
          Height = 16.000000000000000000
          DataField = 'VLR_ABATIMENTO'
          DataSet = frxDBOcorrencias
          DataSetName = 'frxDBOcorrencias'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBOcorrencias."VLR_ABATIMENTO"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          ShiftMode = smWhenOverlapped
          Left = 403.000000000000000000
          Top = 2.000000000000000000
          Width = 76.000000000000000000
          Height = 16.000000000000000000
          DataField = 'VLR_TITULO'
          DataSet = frxDBOcorrencias
          DataSetName = 'frxDBOcorrencias'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBOcorrencias."VLR_TITULO"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          ShiftMode = smWhenOverlapped
          Left = 481.000000000000000000
          Top = 2.000000000000000000
          Width = 32.661410000000000000
          Height = 16.000000000000000000
          DataField = 'PAGAMENTO'
          DataSet = frxDBOcorrencias
          DataSetName = 'frxDBOcorrencias'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDBOcorrencias."PAGAMENTO"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          ShiftMode = smWhenOverlapped
          Left = 515.000000000000000000
          Top = 2.000000000000000000
          Width = 201.354360000000000000
          Height = 16.000000000000000000
          DataField = 'DESC_OCORRENCIA'
          DataSet = frxDBOcorrencias
          DataSetName = 'frxDBOcorrencias'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBOcorrencias."DESC_OCORRENCIA"]')
          ParentFont = False
        end
        object memDetalhes: TfrxMemoView
          ShiftMode = smWhenOverlapped
          Left = 20.000000000000000000
          Top = 21.000000000000000000
          Width = 692.000000000000000000
          Height = 16.000000000000000000
          StretchMode = smActualHeight
          DataSet = frxDBOcorrencias
          DataSetName = 'frxDBOcorrencias'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Detalhes da ocorr'#234'ncia: [frxDBOcorrencias."DETALHES_OCORRENCIA"]')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 208.000000000000000000
        Top = 359.055350000000000000
        Width = 718.110700000000000000
        object Memo19: TfrxMemoView
          Left = 65.000000000000000000
          Top = 183.000000000000000000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBSumario
          DataSetName = 'frxDBSumario'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 148.000000000000000000
          Top = 183.000000000000000000
          Width = 400.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBSumario
          DataSetName = 'frxDBSumario'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Ocorr'#234'ncia')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 552.000000000000000000
          Top = 183.000000000000000000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBSumario
          DataSetName = 'frxDBSumario'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 635.000000000000000000
          Top = 183.000000000000000000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBSumario
          DataSetName = 'frxDBSumario'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Vlr. Total')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 65.000000000000000000
          Top = 164.000000000000000000
          Width = 648.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBSumario
          DataSetName = 'frxDBSumario'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Resumo por ocorr'#234'ncia')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          Left = 466.000000000000000000
          Top = 4.000000000000000000
          Width = 244.000000000000000000
          Height = 144.000000000000000000
          Shape = skRoundRectangle
        end
        object Memo25: TfrxMemoView
          Left = 479.000000000000000000
          Top = 8.000000000000000000
          Width = 216.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Totais')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 479.000000000000000000
          Top = 28.000000000000000000
          Width = 104.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '+ Valor Pago:')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 479.000000000000000000
          Top = 48.000000000000000000
          Width = 104.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '+ Juros:')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 479.000000000000000000
          Top = 71.000000000000000000
          Width = 104.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '= Total Bruto:')
          ParentFont = False
        end
        object Line2: TfrxLineView
          Left = 479.000000000000000000
          Top = 92.000000000000000000
          Width = 216.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo29: TfrxMemoView
          Left = 479.000000000000000000
          Top = 100.000000000000000000
          Width = 104.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '- Taxas:')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 479.000000000000000000
          Top = 124.000000000000000000
          Width = 104.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '= Total L'#237'quido:')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 591.000000000000000000
          Top = 28.000000000000000000
          Width = 104.000000000000000000
          Height = 16.000000000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBPortador."VLR_PAGO"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 591.000000000000000000
          Top = 48.000000000000000000
          Width = 104.000000000000000000
          Height = 16.000000000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBPortador."VLR_JUROS"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 591.000000000000000000
          Top = 71.000000000000000000
          Width = 104.000000000000000000
          Height = 16.000000000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBPortador."TOTAL_BRUTO"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 591.000000000000000000
          Top = 100.000000000000000000
          Width = 104.000000000000000000
          Height = 16.000000000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBPortador."VLR_TAXAS"]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 591.000000000000000000
          Top = 124.000000000000000000
          Width = 104.000000000000000000
          Height = 16.000000000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBPortador."TOTAL_LIQUIDO"]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 20.000000000000000000
        Top = 589.606680000000000000
        Width = 718.110700000000000000
        DataSet = frxDBSumario
        DataSetName = 'frxDBSumario'
        RowCount = 0
        object frxDBSumarioCOD_OCORRENCIA: TfrxMemoView
          Left = 65.000000000000000000
          Top = 1.000000000000000000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          DataField = 'COD_OCORRENCIA'
          DataSet = frxDBSumario
          DataSetName = 'frxDBSumario'
          Memo.UTF8W = (
            '[frxDBSumario."COD_OCORRENCIA"]')
        end
        object frxDBSumarioOCORRENCIA: TfrxMemoView
          Left = 148.000000000000000000
          Top = 1.000000000000000000
          Width = 400.000000000000000000
          Height = 16.000000000000000000
          DataField = 'OCORRENCIA'
          DataSet = frxDBSumario
          DataSetName = 'frxDBSumario'
          Memo.UTF8W = (
            '[frxDBSumario."OCORRENCIA"]')
        end
        object frxDBSumarioQTD: TfrxMemoView
          Left = 552.000000000000000000
          Top = 1.000000000000000000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          DataField = 'QTD'
          DataSet = frxDBSumario
          DataSetName = 'frxDBSumario'
          Memo.UTF8W = (
            '[frxDBSumario."QTD"]')
        end
        object frxDBSumarioTOTAL: TfrxMemoView
          Left = 635.000000000000000000
          Top = 1.000000000000000000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          DataField = 'TOTAL'
          DataSet = frxDBSumario
          DataSetName = 'frxDBSumario'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBSumario."TOTAL"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDiferencas: TfrxReport
    Version = '5.2.3'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Padr'#227'o'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 45321.721123078710000000
    ReportOptions.LastChange = 45321.721123078710000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 1008
    Top = 96
    Datasets = <
      item
        DataSet = frxDBOcorrencias
        DataSetName = 'frxDBOcorrencias'
      end
      item
        DataSet = frxDBPortador
        DataSetName = 'frxDBPortador'
      end
      item
        DataSet = dmRelatorio.dstEmpresa
        DataSetName = 'frxdstEmpresa'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 114.275665000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo3: TfrxMemoView
          Left = 338.386055000000000000
          Top = 1.889765000000001000
          Width = 376.945115000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Data de Emiss'#227'o: [Date]      P'#225'gina [Page] de [TotalPages#]')
          ParentFont = False
        end
        object lbTitulo: TfrxMemoView
          Top = 19.787415000000000000
          Width = 715.213050000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            
              'PROCESSAMENTO DE ARQUIVO DE RETORNO - DIFEREN'#199'A ENTRE VALOR ORIG' +
              'INAL E VALOR PAGO')
          ParentFont = False
        end
        object Shape4: TfrxShapeView
          Top = 42.464595000000000000
          Width = 714.110700000000000000
          Height = 68.031540000000000000
          Shape = skRoundRectangle
        end
        object Memo4: TfrxMemoView
          Left = 7.559060000000000000
          Top = 45.354360000000000000
          Width = 68.031540000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 3.779530000000000000
          Top = 60.472480000000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 22.677180000000000000
          Top = 75.590600000000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 11.338590000000000000
          Top = 90.708720000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 336.378170000000000000
          Top = 45.354360000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 317.480520000000000000
          Top = 75.590600000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade/UF:')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 336.378170000000000000
          Top = 90.708720000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 78.929190000000000000
          Top = 45.354360000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 379.732530000000000000
          Top = 45.354360000000000000
          Width = 200.315090000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."CNPJ"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 81.149660000000000000
          Top = 60.472480000000000000
          Width = 551.811380000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            
              '[frxdstEmpresa."LOGRADOURO"] [frxdstEmpresa."COMPLEMENTO"] n'#176'[fr' +
              'xdstEmpresa."NUMERO"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 78.929190000000000000
          Top = 75.590600000000000000
          Width = 207.874150000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_BAIRRO"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 379.732530000000000000
          Top = 75.590600000000000000
          Width = 185.196970000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_CIDADE"] / [frxdstEmpresa."ESTADO_ID"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 78.929190000000000000
          Top = 90.708720000000000000
          Width = 75.590600000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 379.732530000000000000
          Top = 90.708720000000000000
          Width = 260.787570000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."E_MAIL"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 541.472790000000000000
          Top = 45.354360000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Emitido em [Date] '#224's [Time]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 541.472790000000000000
          Top = 60.472480000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pag. [Page] de [TotalPages#]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 68.000000000000000000
        Top = 192.756030000000000000
        Width = 718.110700000000000000
        object frxDBPortadorPORTADOR_ID: TfrxMemoView
          Left = 8.000000000000000000
          Top = 2.000000000000000000
          Width = 704.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBPortador
          DataSetName = 'frxDBPortador'
          Memo.UTF8W = (
            
              'Portador: [frxDBPortador."PORTADOR_ID"] - [frxDBPortador."PORTAD' +
              'OR_NOME"]')
        end
        object frxDBPortadorCONTA_CORRENTE_ID: TfrxMemoView
          Left = 8.000000000000000000
          Top = 22.000000000000000000
          Width = 704.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBPortador
          DataSetName = 'frxDBPortador'
          Memo.UTF8W = (
            
              'Conta corrente: [frxDBPortador."CONTA_CORRENTE_ID"] - [frxDBPort' +
              'ador."CONTA_CORRENTE"]')
        end
        object Memo1: TfrxMemoView
          Left = 9.000000000000000000
          Top = 46.000000000000000000
          Width = 172.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 186.000000000000000000
          Top = 46.000000000000000000
          Width = 92.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Documento')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 282.000000000000000000
          Top = 46.000000000000000000
          Width = 124.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Nosso n'#250'mero')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 409.000000000000000000
          Top = 46.000000000000000000
          Width = 64.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Juros/Abat.')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 476.000000000000000000
          Top = 46.000000000000000000
          Width = 76.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor Pago')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 556.000000000000000000
          Top = 46.000000000000000000
          Width = 76.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Vlr. Original')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 635.000000000000000000
          Top = 46.000000000000000000
          Width = 76.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Vlr. Diferen'#231'a')
          ParentFont = False
        end
      end
      object mdTitulo: TfrxMasterData
        FillType = ftBrush
        Height = 23.779530000000000000
        Top = 283.464750000000000000
        Width = 718.110700000000000000
        DataSet = frxDBOcorrencias
        DataSetName = 'frxDBOcorrencias'
        PrintIfDetailEmpty = True
        RowCount = 0
        object Line4: TfrxLineView
          Left = -0.110235000000000000
          Top = 0.037795280000011640
          Width = 710.110700000000000000
          Color = clBlack
          Frame.Color = clSilver
          Frame.ShadowColor = clSilver
          Diagonal = True
        end
        object Memo11: TfrxMemoView
          Left = 9.000000000000000000
          Top = 2.000000000000000000
          Width = 172.000000000000000000
          Height = 16.000000000000000000
          DataField = 'RAZAO_SOCIAL'
          DataSet = frxDBOcorrencias
          DataSetName = 'frxDBOcorrencias'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBOcorrencias."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 186.000000000000000000
          Top = 2.000000000000000000
          Width = 92.000000000000000000
          Height = 16.000000000000000000
          DataField = 'DOCUMENTO_ID'
          DataSet = frxDBOcorrencias
          DataSetName = 'frxDBOcorrencias'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBOcorrencias."DOCUMENTO_ID"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 282.000000000000000000
          Top = 2.000000000000000000
          Width = 124.000000000000000000
          Height = 16.000000000000000000
          DataField = 'NOSSO_NUMERO'
          DataSet = frxDBOcorrencias
          DataSetName = 'frxDBOcorrencias'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBOcorrencias."NOSSO_NUMERO"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 409.000000000000000000
          Top = 2.000000000000000000
          Width = 64.000000000000000000
          Height = 16.000000000000000000
          DataField = 'VLR_ABATIMENTO'
          DataSet = frxDBOcorrencias
          DataSetName = 'frxDBOcorrencias'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBOcorrencias."VLR_ABATIMENTO"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 476.000000000000000000
          Top = 2.000000000000000000
          Width = 76.000000000000000000
          Height = 16.000000000000000000
          DataField = 'VLR_TITULO'
          DataSet = frxDBOcorrencias
          DataSetName = 'frxDBOcorrencias'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBOcorrencias."VLR_TITULO"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 556.000000000000000000
          Top = 2.000000000000000000
          Width = 76.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBOcorrencias
          DataSetName = 'frxDBOcorrencias'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBOcorrencias."VLR_ORIGINAL"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 635.000000000000000000
          Top = 2.000000000000000000
          Width = 76.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBOcorrencias
          DataSetName = 'frxDBOcorrencias'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[<frxDBOcorrencias."VLR_ORIGINAL"> - <frxDBOcorrencias."VLR_TITU' +
              'LO">]')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 24.000000000000000000
        Top = 328.819110000000000000
        Width = 718.110700000000000000
      end
    end
  end
  object cdsTitulos: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 1008
    Top = 160
    object cdsTitulosCADASTRO_ID: TStringField
      FieldName = 'CADASTRO_ID'
    end
    object cdsTitulosRAZAO_SOCIAL: TStringField
      FieldName = 'RAZAO_SOCIAL'
      Size = 100
    end
    object cdsTitulosDOCUMENTO_ID: TStringField
      FieldName = 'DOCUMENTO_ID'
      Size = 30
    end
    object cdsTitulosNOSSO_NUMERO: TStringField
      FieldName = 'NOSSO_NUMERO'
      Size = 30
    end
    object cdsTitulosCODIGO_BANCO: TStringField
      FieldName = 'CODIGO_BANCO'
      Size = 10
    end
    object cdsTitulosDT_EMISSAO: TStringField
      FieldName = 'DT_EMISSAO'
    end
    object cdsTitulosDT_VENCIMENTO: TStringField
      FieldName = 'DT_VENCIMENTO'
    end
    object cdsTitulosVALOR_TITULO: TFloatField
      FieldName = 'VALOR_TITULO'
    end
    object cdsTitulosVLR_ADIANTAMENTO: TFloatField
      FieldName = 'VLR_ADIANTAMENTO'
    end
  end
end
