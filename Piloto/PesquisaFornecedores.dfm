inherited FormPesquisaFornecedores: TFormPesquisaFornecedores
  Caption = 'Pesquisa de fornecedores'
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    ColCount = 5
    OnDrawCell = sgPesquisaDrawCell
    CorLinhaFoco = 38619
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Raz'#227'o social'
      'Nome fantasia'
      'CPF/CNPJ')
    RealColCount = 5
    ColWidths = (
      28
      55
      191
      173
      141)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Text = '0'
  end
end
