unit FrameNomeComputador;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.Buttons, _Biblioteca,
  Vcl.StdCtrls, EditLuka;

type
  TFrNomeComputador = class(TFrameHerancaPrincipal)
    lb8: TLabel;
    eNomeComputador: TEditLuka;
    sbNomeComputador: TSpeedButton;
    procedure sbNomeComputadorClick(Sender: TObject);
  public
    procedure Clear; override;
    procedure SomenteLeitura(pValue: Boolean); override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;

    procedure SetFocus; override;
  end;

implementation

{$R *.dfm}

procedure TFrNomeComputador.Clear;
begin
  inherited;
  eNomeComputador.Clear;
end;

procedure TFrNomeComputador.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([eNomeComputador, sbNomeComputador], pEditando, pLimpar);
end;

procedure TFrNomeComputador.sbNomeComputadorClick(Sender: TObject);
begin
  inherited;
  eNomeComputador.Text := _Biblioteca.NomeComputador;
end;

procedure TFrNomeComputador.SetFocus;
begin
  inherited;
  SetarFoco(eNomeComputador);
end;

procedure TFrNomeComputador.SomenteLeitura(pValue: Boolean);
begin
  inherited;
  eNomeComputador.ReadOnly := pValue;
end;

end.
