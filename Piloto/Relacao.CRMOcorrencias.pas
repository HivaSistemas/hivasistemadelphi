unit Relacao.CRMOcorrencias;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _Biblioteca, _Sessao,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, FrameDataInicialFinal,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameEmpresas,
  FrameFuncionarios, Frame.Inteiros, FrameProdutos, Vcl.StdCtrls,
  StaticTextLuka, Vcl.Grids, GridLuka, RadioGroupLuka,
  _TransfProdutosEmpresasItens, CheckBoxLuka, GroupBoxLuka,
  FrameTextoFormatado, FrameEdicaoTexto, FrameClientes, _RelacaoCRMOcorrencias,
  FrameArquivos, _RecordsCadastros, BuscarDadosUsuarioOcorrencia, BuscarDadosSolicitacao,
  Vcl.Menus;

type
  TFormRelacaoCRMOcorrencias = class(TFormHerancaRelatoriosPageControl)
    frDataCadastro: TFrDataInicialFinal;
    frDataConclusao: TFrDataInicialFinal;
    gbStatus: TGroupBoxLuka;
    ckAbertas: TCheckBoxLuka;
    ckConcluidas: TCheckBoxLuka;
    ckRecusadas: TCheckBoxLuka;
    FrClientes: TFrClientes;
    FrCodigoOcorrencias: TFrameInteiros;
    Label1: TLabel;
    mmTextoDescricao: TMemo;
    Label2: TLabel;
    mmTextoSolucao: TMemo;
    lbTransferencia: TStaticTextLuka;
    sgOcorrencias: TGridLuka;
    pgDetalhesOcorrencia: TPageControl;
    tsDescricaoOcorrencia: TTabSheet;
    frDescricaoOcorrencia: TFrEdicaoTexto;
    tsSolucaoOcorrencia: TTabSheet;
    frSolucaoOcorrencia: TFrEdicaoTexto;
    sbGravarOcorrencia: TSpeedButton;
    FrArquivos: TFrArquivos;
    ckProgramacao: TCheckBoxLuka;
    FrFuncionarios: TFrFuncionarios;
    ckTeste: TCheckBoxLuka;
    pmOpcoesOcorrencias: TPopupMenu;
    miReceberPedidoSelecionado: TMenuItem;
    Concluda1: TMenuItem;
    Programao1: TMenuItem;
    Emteste1: TMenuItem;
    Recusada1: TMenuItem;
    Reabrir1: TMenuItem;
    miN2: TMenuItem;
    miConsultarStatusServicoNFe: TMenuItem;
    Mdia1: TMenuItem;
    Mdia2: TMenuItem;
    Alta1: TMenuItem;
    Urgente1: TMenuItem;
    N1: TMenuItem;
    Vincularsolicitao1: TMenuItem;
    N2: TMenuItem;
    Alterarusurio1: TMenuItem;
    gbPrioridades: TGroupBoxLuka;
    CheckBoxLuka1: TCheckBoxLuka;
    CheckBoxLuka2: TCheckBoxLuka;
    CheckBoxLuka3: TCheckBoxLuka;
    CheckBoxLuka4: TCheckBoxLuka;
    procedure sgTransferenciasClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sgOcorrenciasDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgOcorrenciasGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sbGravarOcorrenciaClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure sbAlterarStatusOcorrenciaClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FrArquivossbArquivosClick(Sender: TObject);
    procedure sgOcorrenciasClick(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure Concluda1Click(Sender: TObject);
    procedure Reabrir1Click(Sender: TObject);
    procedure Recusada1Click(Sender: TObject);
    procedure Programao1Click(Sender: TObject);
    procedure Emteste1Click(Sender: TObject);
    procedure Mdia1Click(Sender: TObject);
    procedure Mdia2Click(Sender: TObject);
    procedure Alta1Click(Sender: TObject);
    procedure Urgente1Click(Sender: TObject);
    procedure Vincularsolicitao1Click(Sender: TObject);
    procedure Alterarusurio1Click(Sender: TObject);
  private
    vOcorrencias: TArray<RecRelacaoCRMOcorrencias>;
  public
    { Public declarations }

  protected
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

uses _CRMOcorrencias, _RecordsEspeciais;

const
  (* Grid de Ocorr�ncias *)
  coOcorrenciaId     = 0;
  coCliente          = 1;
  coStatusAnalitico  = 2;
  coPrioridadeAnalitico = 3;
  coUsuario          = 4;
  coSolicitacaoVinc  = 5;
  coDataCadastro     = 6;
  coDataConclusao    = 7;
  coStatus           = 8;
  coPrioridade       = 9;
  coPosicaoArray     = 10;

procedure TFormRelacaoCRMOcorrencias.Alta1Click(Sender: TObject);
var
  ocorrenciaId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if sgOcorrencias.Cells[coOcorrenciaId, 1] = '' then
    Exit;

  ocorrenciaId := StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencias.Cells[coOcorrenciaId, sgOcorrencias.Row]));

  vRetBanco := _CRMOcorrencias.AtualizarCRMPrioridades(
    Sessao.getConexaoBanco,
    ocorrenciaId,
    'AL'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  Carregar(nil);
end;

procedure TFormRelacaoCRMOcorrencias.Alterarusurio1Click(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<RecFuncionarios>;
  vRetBanco: RecRetornoBD;
  ocorrenciaId: Integer;
begin
  inherited;

  if sgOcorrencias.Cells[coOcorrenciaId, 1] = '' then
    Exit;

  ocorrenciaId := StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencias.Cells[coOcorrenciaId, sgOcorrencias.Row]));

  vRetorno := BuscarDadosUsuarioOcorrencia.BuscarDadosUsuario;
  if vRetorno.BuscaCancelada or (vRetorno.Dados = nil) then
    Exit;

  vRetBanco := _CRMOcorrencias.AtualizarUsuarioOcorrencia(
    Sessao.getConexaoBanco,
    ocorrenciaId,
    vRetorno.Dados.funcionario_id
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar o usu�rio da ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Usu�rio da ocorr�ncia atualizada!');

  Carregar(nil);
end;

procedure TFormRelacaoCRMOcorrencias.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
begin
  inherited;
  vOcorrencias := nil;
  sgOcorrencias.ClearGrid();
  frSolucaoOcorrencia.Clear;
  frDescricaoOcorrencia.Clear;

  if not FrCodigoOcorrencias.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrCodigoOcorrencias.getSqlFiltros('CRM.OCORRENCIA_ID'))
  else begin
    if not FrClientes.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrClientes.getSqlFiltros('CRM.CADASTRO_ID'));

    if not FrFuncionarios.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrFuncionarios.getSqlFiltros('CRM.USUARIO'));

    if mmTextoDescricao.Text <> '' then
      _Biblioteca.WhereOuAnd(vSql, 'upper(CRM.DESCRICAO) like upper(''%' + mmTextoDescricao.Text + '%'')'  );

    if mmTextoSolucao.Text <> '' then
      _Biblioteca.WhereOuAnd(vSql, 'upper(CRM.SOLUCAO) like upper(''%' + mmTextoSolucao.Text + '%'')'  );

     if not FrDataCadastro.NenhumaDataValida then
      _Biblioteca.WhereOuAnd(vSql, FrDataCadastro.getSqlFiltros('trunc(CRM.DATA_CADASTRO)'));

     if not frDataConclusao.NenhumaDataValida then
      _Biblioteca.WhereOuAnd(vSql, frDataConclusao.getSqlFiltros('trunc(CRM.DATA_CONCLUSAO)'));

     _Biblioteca.WhereOuAnd(vSql, gbStatus.GetSql('CRM.STATUS'));

     _Biblioteca.WhereOuAnd(vSql, gbPrioridades.GetSql('CRM.PRIORIDADE'));
  end;

  vSql := vSql + ' order by CRM.OCORRENCIA_ID ';

  vOcorrencias := _RelacaoCRMOcorrencias.BuscarOcorrencias(Sessao.getConexaoBanco, vSql);

  if vOcorrencias = nil then begin
    NenhumRegistro;
    Exit;
  end;

  for i := Low(vOcorrencias) to High(vOcorrencias) do begin
    sgOcorrencias.Cells[coOcorrenciaId, i + 1]    := NFormatN(vOcorrencias[i].ocorrencia_id);
    sgOcorrencias.Cells[coCliente, i + 1]         := getInformacao(vOcorrencias[i].cadastro_id, vOcorrencias[i].nome_fantasia);
    sgOcorrencias.Cells[coStatusAnalitico, i + 1] := vOcorrencias[i].status_analitico;
    sgOcorrencias.Cells[coPrioridadeAnalitico, i + 1] := vOcorrencias[i].prioridade_analitico;
    sgOcorrencias.Cells[coUsuario, i + 1]         := getInformacao(vOcorrencias[i].usuario_id, vOcorrencias[i].usuario_nome);
    sgOcorrencias.Cells[coDataCadastro, i + 1]    := DFormatN(vOcorrencias[i].data_cadastro );
    sgOcorrencias.Cells[coDataConclusao, i + 1]   := DFormatN(vOcorrencias[i].data_conclusao);
    sgOcorrencias.Cells[coStatus, i + 1]          := vOcorrencias[i].status ;
    sgOcorrencias.Cells[coPrioridade, i + 1]      := vOcorrencias[i].prioridade ;
    sgOcorrencias.Cells[coSolicitacaoVinc, i + 1] := NFormatN(vOcorrencias[i].solicitacao_id);
    sgOcorrencias.Cells[coPosicaoArray, i + 1]    := intToStr(i);
  end;
  sgOcorrencias.SetLinhasGridPorTamanhoVetor(Length(vOcorrencias));

  pcDados.ActivePage := tsResultado;
  sgTransferenciasClick(nil);
  SetarFoco(sgOcorrencias);
end;

procedure TFormRelacaoCRMOcorrencias.Concluda1Click(Sender: TObject);
var
  ocorrenciaId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if sgOcorrencias.Cells[coOcorrenciaId, 1] = '' then
    Exit;

  ocorrenciaId := StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencias.Cells[coOcorrenciaId, sgOcorrencias.Row]));

  vRetBanco := _CRMOcorrencias.AtualizarStatusCRMOcorrencias(
    Sessao.getConexaoBanco,
    ocorrenciaId,
    'CO'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');


  Carregar(nil);
end;

procedure TFormRelacaoCRMOcorrencias.Emteste1Click(Sender: TObject);
var
  ocorrenciaId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if sgOcorrencias.Cells[coOcorrenciaId, 1] = '' then
    Exit;

  ocorrenciaId := StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencias.Cells[coOcorrenciaId, sgOcorrencias.Row]));

  vRetBanco := _CRMOcorrencias.AtualizarStatusCRMOcorrencias(
    Sessao.getConexaoBanco,
    ocorrenciaId,
    'TE'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  Carregar(nil);
end;

procedure TFormRelacaoCRMOcorrencias.FormCreate(Sender: TObject);
begin
  inherited;
  ActiveControl := FrClientes.sgPesquisa;
end;

procedure TFormRelacaoCRMOcorrencias.FrArquivossbArquivosClick(Sender: TObject);
var
  ocorrenciaId: Integer;
begin
  inherited;
  if sgOcorrencias.Cells[coOcorrenciaId, 1] = '' then
    Exit;

  ocorrenciaId := StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencias.Cells[coOcorrenciaId, sgOcorrencias.Row]));

  FrArquivos.Origem := 'CRM';
  FrArquivos.Id     := ocorrenciaId;
  FrArquivos.buscarArquivos;
  FrArquivos.sbArquivosClick(Sender);
end;

procedure TFormRelacaoCRMOcorrencias.Mdia1Click(Sender: TObject);
var
  ocorrenciaId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if sgOcorrencias.Cells[coOcorrenciaId, 1] = '' then
    Exit;

  ocorrenciaId := StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencias.Cells[coOcorrenciaId, sgOcorrencias.Row]));

  vRetBanco := _CRMOcorrencias.AtualizarCRMPrioridades(
    Sessao.getConexaoBanco,
    ocorrenciaId,
    'BA'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  Carregar(nil);
end;

procedure TFormRelacaoCRMOcorrencias.Mdia2Click(Sender: TObject);
var
  ocorrenciaId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if sgOcorrencias.Cells[coOcorrenciaId, 1] = '' then
    Exit;

  ocorrenciaId := StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencias.Cells[coOcorrenciaId, sgOcorrencias.Row]));

  vRetBanco := _CRMOcorrencias.AtualizarCRMPrioridades(
    Sessao.getConexaoBanco,
    ocorrenciaId,
    'ME'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  Carregar(nil);
end;

procedure TFormRelacaoCRMOcorrencias.Programao1Click(Sender: TObject);
var
  ocorrenciaId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if sgOcorrencias.Cells[coOcorrenciaId, 1] = '' then
    Exit;

  ocorrenciaId := StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencias.Cells[coOcorrenciaId, sgOcorrencias.Row]));

  vRetBanco := _CRMOcorrencias.AtualizarStatusCRMOcorrencias(
    Sessao.getConexaoBanco,
    ocorrenciaId,
    'PR'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  Carregar(nil);
end;

procedure TFormRelacaoCRMOcorrencias.Reabrir1Click(Sender: TObject);
var
  ocorrenciaId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if sgOcorrencias.Cells[coOcorrenciaId, 1] = '' then
    Exit;

  ocorrenciaId := StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencias.Cells[coOcorrenciaId, sgOcorrencias.Row]));

  vRetBanco := _CRMOcorrencias.AtualizarStatusCRMOcorrencias(
    Sessao.getConexaoBanco,
    ocorrenciaId,
    'AB'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  Carregar(nil);
end;

procedure TFormRelacaoCRMOcorrencias.Recusada1Click(Sender: TObject);
var
  ocorrenciaId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if sgOcorrencias.Cells[coOcorrenciaId, 1] = '' then
    Exit;

  ocorrenciaId := StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencias.Cells[coOcorrenciaId, sgOcorrencias.Row]));

  vRetBanco := _CRMOcorrencias.AtualizarStatusCRMOcorrencias(
    Sessao.getConexaoBanco,
    ocorrenciaId,
    'RE'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  Carregar(nil);
end;

procedure TFormRelacaoCRMOcorrencias.sbAlterarStatusOcorrenciaClick(
  Sender: TObject);
var
  ocorrenciaId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if sgOcorrencias.Cells[coOcorrenciaId, 1] = '' then
    Exit;

  ocorrenciaId := StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencias.Cells[coOcorrenciaId, sgOcorrencias.Row]));

  vRetBanco := _CRMOcorrencias.AtualizarStatusCRMOcorrencias(
    Sessao.getConexaoBanco,
    ocorrenciaId,
    'RE'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  Carregar(nil);
end;

procedure TFormRelacaoCRMOcorrencias.sbGravarOcorrenciaClick(Sender: TObject);
var
  ocorrenciaId: Integer;
  clienteId: Integer;
  descricao: string;
  solucao: string;
  vRetBanco: RecRetornoBD;
  posicaoArray: Integer;
begin
  inherited;
  if sgOcorrencias.Cells[coOcorrenciaId, 1] = '' then
    Exit;

  ocorrenciaId := StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencias.Cells[coOcorrenciaId, sgOcorrencias.Row]));
  posicaoArray := StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencias.Cells[coPosicaoArray, sgOcorrencias.Row]));
  clienteId := vOcorrencias[posicaoArray].cadastro_id;

  vRetBanco := _CRMOcorrencias.AtualizarCRMOcorrencias(
    Sessao.getConexaoBanco,
    ocorrenciaId,
    clienteId,
    frDescricaoOcorrencia.GetTexto,
    frSolucaoOcorrencia.GetTexto,
    'BA'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao gravar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end;

  vOcorrencias[posicaoArray].descricao := frDescricaoOcorrencia.GetTexto;
  vOcorrencias[posicaoArray].solucao := frSolucaoOcorrencia.GetTexto;

  Informar('Ocorr�ncia atualizada com sucesso!' + vRetBanco.MensagemErro);
end;

procedure TFormRelacaoCRMOcorrencias.sgOcorrenciasClick(Sender: TObject);
var
  posicaoArray: Integer;
begin
  inherited;
  if sgOcorrencias.Cells[coOcorrenciaId, 1] = '' then
    Exit;

  posicaoArray := StrToInt(sgOcorrencias.Cells[coPosicaoArray, sgOcorrencias.Row]);

  frDescricaoOcorrencia.SetTexto(vOcorrencias[posicaoArray].descricao);
  frSolucaoOcorrencia.SetTexto(vOcorrencias[posicaoArray].solucao);
end;

procedure TFormRelacaoCRMOcorrencias.sgOcorrenciasDrawCell(Sender: TObject;
  ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in [coOcorrenciaId, coSolicitacaoVinc]
  then
    vAlinhamento := taRightJustify
  else if ACol in [coStatusAnalitico, coPrioridadeAnalitico] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgOcorrencias.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoCRMOcorrencias.sgOcorrenciasGetCellColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coStatusAnalitico then begin
    AFont.Style := [fsBold];

    if sgOcorrencias.Cells[coStatus, ARow] = 'AB' then
      AFont.Color := clBlue
    else if sgOcorrencias.Cells[coStatus, ARow] = 'CO' then
      AFont.Color := clGreen
    else if sgOcorrencias.Cells[coStatus, ARow] = 'PR' then
      AFont.Color := clMaroon
    else if sgOcorrencias.Cells[coStatus, ARow] = 'TE' then
      AFont.Color := clOlive
    else if sgOcorrencias.Cells[coStatus, ARow] = 'RE' then
      AFont.Color := clRed;
  end
  else if ACol = coPrioridadeAnalitico then begin
    AFont.Style := [fsBold];

    if sgOcorrencias.Cells[coPrioridade, ARow] = 'BA' then
      AFont.Color := clBlue
    else if sgOcorrencias.Cells[coPrioridade, ARow] = 'ME' then
      AFont.Color := clOlive
    else if sgOcorrencias.Cells[coPrioridade, ARow] = 'AL' then
      AFont.Color := clGreen
    else if sgOcorrencias.Cells[coPrioridade, ARow] = 'UR' then
      AFont.Color := clOlive;
  end;

end;

procedure TFormRelacaoCRMOcorrencias.sgTransferenciasClick(Sender: TObject);
var
  vPosicaoArray: Integer;
begin
  inherited;
  frSolucaoOcorrencia.Clear;
  frDescricaoOcorrencia.Clear;
  vPosicaoArray := SFormatInt(sgOcorrencias.Cells[coPosicaoArray, sgOcorrencias.Row]);

  frSolucaoOcorrencia.SetTexto(vOcorrencias[vPosicaoArray].solucao);
  frDescricaoOcorrencia.SetTexto(vOcorrencias[vPosicaoArray].descricao);
end;

procedure TFormRelacaoCRMOcorrencias.SpeedButton1Click(Sender: TObject);
var
  ocorrenciaId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if sgOcorrencias.Cells[coOcorrenciaId, 1] = '' then
    Exit;

  ocorrenciaId := StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencias.Cells[coOcorrenciaId, sgOcorrencias.Row]));

  vRetBanco := _CRMOcorrencias.AtualizarStatusCRMOcorrencias(
    Sessao.getConexaoBanco,
    ocorrenciaId,
    'CO'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');


  Carregar(nil);
end;

procedure TFormRelacaoCRMOcorrencias.SpeedButton2Click(Sender: TObject);
var
  ocorrenciaId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if sgOcorrencias.Cells[coOcorrenciaId, 1] = '' then
    Exit;

  ocorrenciaId := StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencias.Cells[coOcorrenciaId, sgOcorrencias.Row]));

  vRetBanco := _CRMOcorrencias.AtualizarStatusCRMOcorrencias(
    Sessao.getConexaoBanco,
    ocorrenciaId,
    'AB'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  Carregar(nil);
end;

procedure TFormRelacaoCRMOcorrencias.SpeedButton3Click(Sender: TObject);
var
  ocorrenciaId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if sgOcorrencias.Cells[coOcorrenciaId, 1] = '' then
    Exit;

  ocorrenciaId := StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencias.Cells[coOcorrenciaId, sgOcorrencias.Row]));

  vRetBanco := _CRMOcorrencias.AtualizarStatusCRMOcorrencias(
    Sessao.getConexaoBanco,
    ocorrenciaId,
    'PR'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  Carregar(nil);
end;

procedure TFormRelacaoCRMOcorrencias.SpeedButton4Click(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<RecFuncionarios>;
  vRetBanco: RecRetornoBD;
  ocorrenciaId: Integer;
begin
  inherited;

  if sgOcorrencias.Cells[coOcorrenciaId, 1] = '' then
    Exit;

  ocorrenciaId := StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencias.Cells[coOcorrenciaId, sgOcorrencias.Row]));

  vRetorno := BuscarDadosUsuarioOcorrencia.BuscarDadosUsuario;
  if vRetorno.BuscaCancelada or (vRetorno.Dados = nil) then
    Exit;

  vRetBanco := _CRMOcorrencias.AtualizarUsuarioOcorrencia(
    Sessao.getConexaoBanco,
    ocorrenciaId,
    vRetorno.Dados.funcionario_id
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar o usu�rio da ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Usu�rio da ocorr�ncia atualizada!');

  Carregar(nil);
end;

procedure TFormRelacaoCRMOcorrencias.SpeedButton5Click(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<TArray<Integer>>;
  vRetBanco: RecRetornoBD;
  ocorrenciaId: Integer;
begin
  inherited;

  if sgOcorrencias.Cells[coOcorrenciaId, 1] = '' then
    Exit;

  ocorrenciaId := StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencias.Cells[coOcorrenciaId, sgOcorrencias.Row]));

  vRetorno := BuscarDadosSolicitacao.BuscarDadosSolicitacoes;
  if vRetorno.BuscaCancelada or (vRetorno.Dados = nil) then
    Exit;

  vRetBanco := _CRMOcorrencias.AtualizarSolicitacaoOcorrencia(
    Sessao.getConexaoBanco,
    ocorrenciaId,
    vRetorno.Dados[0]
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar o usu�rio da ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Solicita��o vinculada a ocorr�ncia atualizada!');

  Carregar(nil);
end;

procedure TFormRelacaoCRMOcorrencias.SpeedButton6Click(Sender: TObject);
var
  ocorrenciaId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if sgOcorrencias.Cells[coOcorrenciaId, 1] = '' then
    Exit;

  ocorrenciaId := StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencias.Cells[coOcorrenciaId, sgOcorrencias.Row]));

  vRetBanco := _CRMOcorrencias.AtualizarStatusCRMOcorrencias(
    Sessao.getConexaoBanco,
    ocorrenciaId,
    'TE'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  Carregar(nil);
end;

procedure TFormRelacaoCRMOcorrencias.Urgente1Click(Sender: TObject);
var
  ocorrenciaId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if sgOcorrencias.Cells[coOcorrenciaId, 1] = '' then
    Exit;

  ocorrenciaId := StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencias.Cells[coOcorrenciaId, sgOcorrencias.Row]));

  vRetBanco := _CRMOcorrencias.AtualizarCRMPrioridades(
    Sessao.getConexaoBanco,
    ocorrenciaId,
    'UR'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  Carregar(nil);
end;

procedure TFormRelacaoCRMOcorrencias.Vincularsolicitao1Click(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<TArray<Integer>>;
  vRetBanco: RecRetornoBD;
  ocorrenciaId: Integer;
begin
  inherited;

  if sgOcorrencias.Cells[coOcorrenciaId, 1] = '' then
    Exit;

  ocorrenciaId := StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencias.Cells[coOcorrenciaId, sgOcorrencias.Row]));

  vRetorno := BuscarDadosSolicitacao.BuscarDadosSolicitacoes;
  if vRetorno.BuscaCancelada or (vRetorno.Dados = nil) then
    Exit;

  vRetBanco := _CRMOcorrencias.AtualizarSolicitacaoOcorrencia(
    Sessao.getConexaoBanco,
    ocorrenciaId,
    vRetorno.Dados[0]
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar o usu�rio da ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Solicita��o vinculada a ocorr�ncia atualizada!');

  Carregar(nil);
end;

end.
