inherited FormBuscarDadosCartoesTEF: TFormBuscarDadosCartoesTEF
  Caption = 'Busca dados de cart'#245'es - TEF'
  ClientHeight = 321
  ClientWidth = 744
  ExplicitWidth = 750
  ExplicitHeight = 350
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 284
    Width = 744
    ExplicitTop = 284
    ExplicitWidth = 744
    inherited sbFinalizar: TSpeedButton
      Left = 194
      ExplicitLeft = 194
    end
  end
  object sgCartoesPassar: TGridLuka
    Left = 2
    Top = 15
    Width = 740
    Height = 236
    Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
    Align = alCustom
    ColCount = 7
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect]
    PopupMenu = pmOpcoes
    TabOrder = 1
    OnDrawCell = sgCartoesPassarDrawCell
    OnKeyPress = NumerosVirgula
    OnSelectCell = sgCartoesPassarSelectCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Tipo receb.'
      'Tipo cart'#227'o'
      'Qtde.parc.'
      'Cobr.'
      'Nome'
      'Valor'
      'Status')
    OnGetCellColor = sgCartoesPassarGetCellColor
    OnArrumarGrid = sgCartoesPassarArrumarGrid
    Grid3D = False
    RealColCount = 7
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      72
      82
      63
      37
      245
      99
      64)
  end
  object stCaracteristicas: TStaticText
    Left = 0
    Top = 0
    Width = 744
    Height = 15
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    Caption = 'Cart'#245'es a serem passados'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    Transparent = False
  end
  object st4: TStaticText
    Left = 74
    Top = 252
    Width = 150
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Total a receber'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 3
    Transparent = False
  end
  object stValorReceber: TStaticText
    Left = 74
    Top = 267
    Width = 150
    Height = 16
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clGreen
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 4
    Transparent = False
  end
  object st2: TStaticText
    Left = 521
    Top = 252
    Width = 150
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Total aprovado'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 5
    Transparent = False
  end
  object stValorAprovados: TStaticText
    Left = 521
    Top = 267
    Width = 150
    Height = 16
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clPurple
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 6
    Transparent = False
  end
  object stValorDiferenca: TStaticText
    Left = 372
    Top = 267
    Width = 150
    Height = 16
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clTeal
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 7
    Transparent = False
  end
  object st5: TStaticText
    Left = 372
    Top = 252
    Width = 150
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Diferen'#231'a'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 8
    Transparent = False
  end
  object stValorCartoesInseridos: TStaticText
    Left = 223
    Top = 267
    Width = 150
    Height = 16
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 9
    Transparent = False
  end
  object st3: TStaticText
    Left = 223
    Top = 252
    Width = 150
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Total cart'#245'es inseridos'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 10
    Transparent = False
  end
  object pmOpcoes: TPopupMenu
    Left = 688
    Top = 120
    object miAdicionarCartao: TMenuItem
      Caption = 'Adicionar cart'#227'o'
      ShortCut = 16496
      OnClick = miAdicionarCartaoClick
    end
    object miN4: TMenuItem
      Caption = '-'
    end
    object miAlterarFormaRecebimento: TMenuItem
      Caption = 'Passar cart'#227'o como POS'
      ShortCut = 16497
      OnClick = miAlterarFormaRecebimentoClick
    end
    object miN1: TMenuItem
      Caption = '-'
    end
    object miPassarCartao: TMenuItem
      Caption = 'Passar cart'#227'o'
      ShortCut = 16500
      OnClick = miPassarCartaoClick
    end
    object miN2: TMenuItem
      Caption = '-'
    end
    object miAlterarTipoCobranca: TMenuItem
      Caption = 'Alterar cart'#227'o'
      ShortCut = 16501
      OnClick = miAlterarTipoCobrancaClick
    end
    object miN3: TMenuItem
      Caption = '-'
    end
    object miDeletarCartao: TMenuItem
      Caption = 'Deletar cart'#227'o'
      ShortCut = 16430
      OnClick = miDeletarCartaoClick
    end
  end
end
