unit Frame.Diretorio;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.StdCtrls, _Biblioteca,
  EditLuka, Vcl.Buttons, Buscar.Diretorio;

type
  TFrDiretorio = class(TFrameHerancaPrincipal)
    lb1: TLabel;
    sbPesquisarDiretorio: TSpeedButton;
    eCaminhoDiretorio: TEditLuka;
    procedure eCaminhoDiretorioKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbPesquisarDiretorioClick(Sender: TObject);
  private
    FOnAposPesquisarDiretorio: TEventoObjeto;
  public
    function getCaminhoDiretorio: string;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
    procedure Clear; override;
    procedure SomenteLeitura(pValue: Boolean); override;
    procedure SetFocus; override;
    function EstaVazio: Boolean; override;
  published
    property OnAposPesquisarDiretorio: TEventoObjeto read FOnAposPesquisarDiretorio write FOnAposPesquisarDiretorio;
  end;

implementation

{$R *.dfm}

{ TFrameHerancaPrincipal1 }

procedure TFrDiretorio.Clear;
begin
  inherited;
  eCaminhoDiretorio.Clear;
end;

procedure TFrDiretorio.eCaminhoDiretorioKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (Key <> VK_RETURN) then
    Exit;

  if eCaminhoDiretorio.Text <> '' then begin // Caso j� tenha um caminho, passando para pr�ximo campo
    keybd_event(VK_TAB, 0, 0, 0);
    Exit;
  end;

  sbPesquisarDiretorioClick(nil);
end;

function TFrDiretorio.EstaVazio: Boolean;
begin
  Result := eCaminhoDiretorio.Text = '';
end;

function TFrDiretorio.getCaminhoDiretorio: string;
begin
  Result := Trim(eCaminhoDiretorio.Text);
end;

procedure TFrDiretorio.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;

  eCaminhoDiretorio.Enabled := pEditando;
  sbPesquisarDiretorio.Enabled := pEditando;

  if pLimpar then
    eCaminhoDiretorio.Clear;
end;

procedure TFrDiretorio.sbPesquisarDiretorioClick(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<string>;
begin
  inherited;
  vRetorno := Buscar.Diretorio.Buscar();
  if vRetorno.RetTela = trCancelado then
    RotinaCanceladaUsuario
  else
    eCaminhoDiretorio.Text := vRetorno.Dados;
end;

procedure TFrDiretorio.SetFocus;
begin
  inherited;
  eCaminhoDiretorio.SetFocus;
end;

procedure TFrDiretorio.SomenteLeitura(pValue: Boolean);
begin
  inherited;
  sbPesquisarDiretorio.Enabled := not pValue;
end;

end.
