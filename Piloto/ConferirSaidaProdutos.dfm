inherited FormConferirSaidaProdutos: TFormConferirSaidaProdutos
  Caption = 'Confer'#234'ncia de sa'#237'da de produtos'
  ClientHeight = 421
  ClientWidth = 794
  OnShow = FormShow
  ExplicitWidth = 800
  ExplicitHeight = 450
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 3
    Top = 2
    Width = 92
    Height = 14
    Caption = 'C'#243'digo de barras'
  end
  inherited pnOpcoes: TPanel
    Top = 384
    Width = 794
    ExplicitTop = 384
    ExplicitWidth = 794
  end
  object eCodigoBarras: TEditLuka
    Left = 2
    Top = 16
    Width = 286
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 1
    OnKeyDown = eCodigoBarrasKeyDown
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object sgItens: TGridLuka
    Left = 2
    Top = 40
    Width = 790
    Height = 343
    Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
    Align = alCustom
    ColCount = 8
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
    TabOrder = 2
    OnDrawCell = sgItensDrawCell
    OnSelectCell = sgItensSelectCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Produto'
      'Nome'
      'Marca'
      'Qtde.'
      'Und.'
      'Lote'
      'Qtde.conf.'
      'N'#227'o conf.')
    OnGetCellColor = sgItensGetCellColor
    OnArrumarGrid = sgItensArrumarGrid
    Grid3D = False
    RealColCount = 15
    AtivarPopUpSelecao = False
    ColWidths = (
      53
      268
      127
      63
      33
      75
      75
      70)
  end
end
