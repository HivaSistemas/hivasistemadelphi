unit FrameTextos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.ExtCtrls,
  Vcl.Grids, GridLuka, _Biblioteca;

type
  TFrTextos = class(TFrameHerancaPrincipal)
    sgTextos: TGridLuka;
    pnDescricao: TPanel;
    procedure sgTextosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgTextosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgTextosKeyPress(Sender: TObject; var Key: Char);
  private
    FOnAposAdicionar: TEventoObjeto;
    FOnAposDeletar: TEventoObjeto;

    procedure setValores(pValores: TArray<string>);
  public
    constructor Create(AOwner: TComponent); override;

    procedure Clear; override;
    procedure SetFocus; override;
    procedure Adicionar(pValor: string);
    procedure SomenteLeitura(pValue: Boolean); override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;

    function getValores: TArray<string>;
    function EstaVazio: Boolean; override;

    function TrazerFiltros(pNomeColuna: string): string;
  published
    property OnAposAdicionar: TEventoObjeto read FOnAposAdicionar write FOnAposAdicionar;
    property OnAposDeletar: TEventoObjeto read FOnAposDeletar write FOnAposDeletar;
    property Valores: TArray<string> read getValores write setValores;
  end;

implementation

{$R *.dfm}

uses BuscaDados;

const
  cTexto = 0;

procedure TFrTextos.Adicionar(pValor: string);
begin
  if sgTextos.Cells[cTexto, 0] = '' then
    sgTextos.Cells[cTexto, 0] := pValor
  else begin
    sgTextos.RowCount := sgTextos.RowCount + 1;
    sgTextos.Cells[cTexto, sgTextos.RowCount - 1] := pValor;
    sgTextos.Row := sgTextos.RowCount - 1;
  end;

  if Assigned(FOnAposAdicionar) then
    FOnAposAdicionar(Self);
end;

procedure TFrTextos.Clear;
begin
  inherited;
  sgTextos.ClearGrid;
end;

constructor TFrTextos.Create(AOwner: TComponent);
begin
  inherited;
  sgTextos.ColWidths[cTexto] := Self.Width - 45;
end;

function TFrTextos.EstaVazio: Boolean;
begin
  Result := ( sgTextos.Cells[cTexto, 0] = '' );
end;

function TFrTextos.getValores: TArray<string>;
var
  i: Integer;
begin
  if EstaVazio then
    Exit;

  SetLength(Result, sgTextos.RowCount);
  for i := 0 to sgTextos.RowCount - 1 do
    Result[i] := sgTextos.Cells[cTexto, i];
end;

procedure TFrTextos.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;
  sgTextos.Enabled := pEditando;

  if pLimpar then
    sgTextos.ClearGrid;
end;

procedure TFrTextos.SetFocus;
begin
  inherited;
  sgTextos.SetFocus;
end;

procedure TFrTextos.setValores(pValores: TArray<string>);
var
  i: Integer;
begin
  for i := Low(pValores) to High(pValores) do
    Adicionar(pValores[i]);
end;

procedure TFrTextos.sgTextosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  inherited;
  sgTextos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taLeftJustify, Rect);
end;

procedure TFrTextos.sgTextosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_DELETE then begin
    sgTextos.DeleteRow(sgTextos.Row);
    if Assigned(FOnAposDeletar) then
      FOnAposDeletar(Sender);
  end;
end;

procedure TFrTextos.sgTextosKeyPress(Sender: TObject; var Key: Char);
var
  i: Integer;
  valor: string;
begin
  inherited;
  if CharInSet(Key, ['0'..'9', 'A'..'Z', 'a'..'z']) then begin
    valor := BuscaDados.BuscarDados(tiTexto, 'Busca digitada', Key);

    for i := 0 to sgTextos.RowCount - 1 do begin
      if valor = sgTextos.Cells[cTexto, i] then
        Exit;
    end;

    if sgTextos.Cells[cTexto, 0] = '' then
      sgTextos.Cells[cTexto, 0] := valor
    else begin
      sgTextos.RowCount := sgTextos.RowCount + 1;
      sgTextos.Cells[cTexto, sgTextos.RowCount - 1] := valor;
      sgTextos.Row := sgTextos.RowCount - 1;
    end;

    if Assigned(FOnAposAdicionar) then
      FOnAposAdicionar(Sender);

    sgTextos.SetFocus;
  end;
end;

procedure TFrTextos.SomenteLeitura(pValue: Boolean);
begin
  inherited;
  sgTextos.EditorMode := not pValue;
end;

function TFrTextos.TrazerFiltros(pNomeColuna: string): string;
begin
  Result := _Biblioteca.FiltroInStr(pNomeColuna, getValores);
end;

end.
