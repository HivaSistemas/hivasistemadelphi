unit Impressao.OrcamentoGrafico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatorioSimples, QRCtrls, _Sessao, _Biblioteca,
  QuickRpt, Vcl.ExtCtrls, _Orcamentos, _OrcamentosItens, _RecordsOrcamentosVendas,
  QRExport, QRPDFFilt, QRWebFilt;

type
  TFormImpressaoOrcamentoGrafico = class(TFormHerancaRelatorioSimples)
    qr2: TQRLabel;
    qrOrcamentoId: TQRLabel;
    qr8: TQRLabel;
    qrCliente: TQRLabel;
    qr12: TQRLabel;
    qrEnderecoCliente: TQRLabel;
    qr15: TQRLabel;
    qrBairroCliente: TQRLabel;
    qrCidadeCliente: TQRLabel;
    qr18: TQRLabel;
    qrCepCliente: TQRLabel;
    qr21: TQRLabel;
    qr23: TQRLabel;
    qr24: TQRLabel;
    qr25: TQRLabel;
    qr26: TQRLabel;
    qr27: TQRLabel;
    qr6: TQRLabel;
    qr28: TQRLabel;
    qrProdutoId: TQRLabel;
    qrNomeProduto: TQRLabel;
    qrMarca: TQRLabel;
    qrValorUnitario: TQRLabel;
    qrQuantidade: TQRLabel;
    qrUnidade: TQRLabel;
    qrValorTotal: TQRLabel;
    qr4: TQRLabel;
    qrTelefoneCliente: TQRLabel;
    qr14: TQRLabel;
    qrCelularCliente: TQRLabel;
    qr9: TQRLabel;
    qrVendedor: TQRLabel;
    qr17: TQRLabel;
    qrCondicaoPagamento: TQRLabel;
    qrMensagensFinais: TQRMemo;
    qr20: TQRLabel;
    qr22: TQRLabel;
    qr29: TQRLabel;
    qrTotalSerPago: TQRLabel;
    qrValorDesconto: TQRLabel;
    qrValorOutrasDespesas: TQRLabel;
    qrl1: TQRLabel;
    qrlGrValorFrete: TQRLabel;
    qrTotalProdutos: TQRLabel;
    qr16: TQRLabel;
    QRLabel4: TQRLabel;
    qrOrcamentoIdTM: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel8: TQRLabel;
    qrClienteTM: TQRLabel;
    QRLabel12: TQRLabel;
    qrCelularClienteTM: TQRLabel;
    QRLabel5: TQRLabel;
    qrEnderecoClienteTM: TQRLabel;
    QRLabel9: TQRLabel;
    qrCidadeClienteTM: TQRLabel;
    QRLabel13: TQRLabel;
    qrEstadoClienteTM: TQRLabel;
    QRLabel7: TQRLabel;
    qrBairroClienteTM: TQRLabel;
    QRLabel11: TQRLabel;
    qrCepClienteTM: TQRLabel;
    QRBand1: TQRBand;
    qrProdutoIdTM: TQRLabel;
    qrVendedorTM: TQRLabel;
    QRShape4: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    qrNomeProdutoTM: TQRLabel;
    qrValorUnitarioTM: TQRLabel;
    qrQuantidadeTM: TQRLabel;
    qrUnidadeTM: TQRLabel;
    qrValorTotalTM: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    qrTotalProdutosTM: TQRLabel;
    qrValorOutrasDespesastm: TQRLabel;
    qrlGrValorFreteTM: TQRLabel;
    qrValorDescontoTM: TQRLabel;
    qrTotalSerPagoTM: TQRLabel;
    qrMensagensFinaisTM: TQRMemo;
    QRPDFFilter1: TQRPDFFilter;
    qlDescontoUnitario: TQRLabel;
    qrDescontoUnitario: TQRLabel;
    qrIndice: TQRLabel;
    QRShape3: TQRShape;
    qlPrcoUnitLiq: TQRLabel;
    qrPrcoUnitLiq: TQRLabel;
    procedure qrBandaDetalhesBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qrRelatorioBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure qrRelatorioNeedData(Sender: TObject; var MoreData: Boolean);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qrRelatorioNFBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure qrRelatorioNFNeedData(Sender: TObject; var MoreData: Boolean);

  private
    FPosicImp: Integer;
    FProdutos: TArray<RecOrcamentosItensImpressao>;
  end;

procedure Imprimir(
  const pOrcamentoId: Integer;
  pGerarPDF: Boolean = False;
  pEnviarEmail: Boolean = False;
  pExibirCustoUnitario: Boolean = False;
  pExibirCustoTotalItens: Boolean = False;
  pExibirDescontoItens: Boolean = False
);

procedure EnviarPorEmail(const pOrcamentoId: Integer; pOcultarCustoUnitario: Boolean = False; pOcultarCustoTotalItens: Boolean = False);

implementation

{$R *.dfm}

uses _Email;

procedure Imprimir(
  const pOrcamentoId: Integer;
  pGerarPDF: Boolean = False;
  pEnviarEmail: Boolean = False;
  pExibirCustoUnitario: Boolean = False;
  pExibirCustoTotalItens: Boolean = False;
  pExibirDescontoItens: Boolean = False
);
var
  vForm: TFormImpressaoOrcamentoGrafico;
  vOrcamento: RecOrcamentosImpressao;
  vItens: TArray<RecOrcamentosItensImpressao>;
  vImpressora: RecImpressora;
  pCaminhoPDF: string;
  vAnexo: TStringList;
  vcnt: Integer;
  vPrecoUnitario,
  vValotToal : Double;

begin
  if pOrcamentoId = 0 then
    Exit;

  vOrcamento := _Orcamentos.BuscarInformacoesImpressao(Sessao.getConexaoBanco, pOrcamentoId);
  if vOrcamento.OrcamentoId = 0 then begin
    NenhumRegistro;
    Exit;
  end;

  vItens := _OrcamentosItens.BuscarInformacoesImpressao(Sessao.getConexaoBanco, pOrcamentoId);
  if vItens = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vImpressora := Sessao.getImpressora( toOrcamento );
  if not vImpressora.validaImpresssora then
    Exit;

  vForm := TFormImpressaoOrcamentoGrafico.Create(Application, vImpressora);


  vForm.FProdutos := vItens;
  vForm.PreencherCabecalho( vOrcamento.EmpresaId, True );

  if Em(vOrcamento.Status, ['VR', 'VE', 'RE']) then
  begin
    pExibirCustoUnitario := True;
    pExibirCustoTotalItens := True;
    pExibirDescontoItens := True;

    vForm.qr13.Caption := 'Pedido de venda';
  end else
    vForm.qr13.Caption := 'Or�amento';

  vForm.qrOrcamentoId.Caption         := NFormat(pOrcamentoId);
  vForm.qrOrcamentoIdTM.Caption       := NFormat(pOrcamentoId);
  vForm.qrVendedor.Caption            := NFormat(vOrcamento.VendedorId) + ' - ' + vOrcamento.NomeVendedor;
  vForm.qrVendedorTM.Caption          := NFormat(vOrcamento.VendedorId) + ' - ' + vOrcamento.NomeVendedor;
  vForm.qrCondicaoPagamento.Caption   := vOrcamento.NomeCondicaoPagamento;
  vForm.qrCliente.Caption             := NFormat(vOrcamento.ClienteId) + ' - ' + vOrcamento.NomeCliente;
  vForm.qrClienteTM.Caption           := NFormat(vOrcamento.ClienteId) + ' - ' + vOrcamento.NomeCliente;
  vForm.qrEnderecoCliente.Caption     := vOrcamento.Logradouro + ' ' + vOrcamento.Complemento + ' ' + vOrcamento.Numero;
  vForm.qrEnderecoClienteTM.Caption   := vOrcamento.Logradouro + ' ' + vOrcamento.Complemento + ' ' + vOrcamento.Numero;
  vForm.qrBairroCliente.Caption       := vOrcamento.NomeBairro;
  vForm.qrBairroClienteTM.Caption     := vOrcamento.NomeBairro;
  vForm.qrCidadeCliente.Caption       := vOrcamento.NomeCidade + ' - ' + vOrcamento.NomeEstado;
  vForm.qrCidadeClienteTM.Caption     := vOrcamento.NomeCidade;
  //vForm.qrEstadoCliente.Caption       := vOrcamento.NomeEstado;
  vForm.qrEstadoClienteTM.Caption     := vOrcamento.NomeEstado;
  vForm.qrCepCliente.Caption          := vOrcamento.Cep;
  vForm.qrCepClienteTM.Caption        := vOrcamento.Cep;
  vForm.qrTelefoneCliente.Caption     := vOrcamento.TelefonePrincipal;
  vForm.qrCelularCliente.Caption      := vOrcamento.TelefoneCelular;
  vForm.qrCelularClienteTM.Caption    := vOrcamento.TelefoneCelular;

  vForm.qrTotalProdutos.Caption         := NFormat(vOrcamento.ValorTotalProdutos);
  vForm.qrTotalProdutosTM.Caption       := NFormat(vOrcamento.ValorTotalProdutos);
  vForm.qrValorOutrasDespesas.Caption   := NFormat(vOrcamento.ValorOutrasDespesas);
  vForm.qrValorOutrasDespesasTM.Caption := NFormat(vOrcamento.ValorOutrasDespesas);
  vForm.qrlGrValorFrete.Caption         := NFormat(vOrcamento.ValorFrete + vOrcamento.ValorFreteItens);
  vForm.qrlGrValorFreteTM.Caption       := NFormat(vOrcamento.ValorFrete + vOrcamento.ValorFreteItens);

  vForm.qrValorDescontoTM.Caption       := NFormat(vOrcamento.ValorDesconto);
  vForm.qrTotalSerPago.Caption          := NFormat(vOrcamento.ValorTotal);
  vForm.qrTotalSerPagoTM.Caption        := NFormat(vOrcamento.ValorTotal);
  vForm.qrIndice.Caption                := NFormat(vOrcamento.Indice,4);
  vForm.qrValorDesconto.Caption         := NFormat(vOrcamento.ValorDesconto);
  if pExibirDescontoItens then
  begin
    vForm.qrValorDesconto.Caption  := '0';
    vForm.qrTotalProdutos.Caption  := '0';
    vForm.qrTotalSerPago.Caption   := '0';
    vForm.qr16.Caption             := 'Total Bruto:    ';
    vForm.qr29.Caption             := 'Total Liquido:  ';


    for vcnt := 0 to length(vForm.FProdutos) - 1 do
    begin
      vForm.qrValorDesconto.Caption    := NFormat( StrToFloat(RemoverCaracter(vForm.qrValorDesconto.Caption,'.'))
                                        + vForm.FProdutos[vcnt].ValorTotalDesconto
                                        + vForm.FProdutos[vcnt].ValorDescUnitPrecoManual);

      if (vForm.qrDescontoUnitario.Enabled)
      //and(Em(vForm.FProdutos[vcnt].TipoPrecoUtilitario,['MAN','VAR']))
      then
      begin
        vPrecoUnitario :=  Arredondar(vForm.FProdutos[vcnt].ValorOriginal * StrToFloat(RemoverCaracter(vForm.qrIndice.Caption,'.')),2);
        vValotToal     := (vPrecoUnitario * vForm.FProdutos[vcnt].Quantidade) - StrToFloat(RemoverCaracter(vForm.qrValorDesconto.Caption,'.'));
      end;

      vForm.qrTotalProdutos.Caption    := NFormat(StrToFloat(RemoverCaracter(vForm.qrTotalProdutos.Caption,'.'))
                                        + (vPrecoUnitario * vForm.FProdutos[vCnt].Quantidade)) ;

      vForm.qrTotalSerPago.Caption    := NFormat( StrToFloat(RemoverCaracter(vForm.qrTotalProdutos.Caption,'.')) - StrToFloat(RemoverCaracter(vForm.qrValorDesconto.Caption,'.'))
                                       + (StrToFloat(RemoverCaracter(vForm.qrValorOutrasDespesas.Caption,'.')) + StrToFloat(RemoverCaracter(vForm.qrlGrValorFrete.Caption,'.')))) ;
    end;
  end;


  vForm.qr6.Enabled := True;
  vForm.qrValorUnitario.Enabled := True;
  if not pExibirCustoUnitario then begin
    vForm.qr6.Enabled := False;
    vForm.qrValorUnitario.Enabled := False;
  end;

  vForm.qlDescontoUnitario.Enabled := pExibirDescontoItens;
  vForm.qrDescontoUnitario.Enabled := pExibirDescontoItens;
  vForm.qlPrcoUnitLiq.Enabled := pExibirDescontoItens;
  vForm.qrPrcoUnitLiq.Enabled := pExibirDescontoItens;
  if not pExibirDescontoItens then begin
    vForm.qr24.width := 446;
    vForm.qrNomeProduto.width := 446;
    vForm.qr6.left := 689;
    vForm.qrValorUnitario.left := 689;
    vForm.qr27.left := 639;
    vForm.qrUnidade.left := 639;
    vForm.qr26.left := 584;
    vForm.qrQuantidade.left := 584;
    vForm.qr25.left := 506;
    vForm.qrMarca.left := 506;
  end;

  vForm.qr28.Enabled := True;
  vForm.qrValorTotal.Enabled := True;
  if not pExibirCustoTotalItens then begin
    vForm.qr28.Enabled := False;
    vForm.qrValorTotal.Enabled := False;
  end;

  if vOrcamento.Status = 'RE' then
  begin
    vForm.qrMensagensFinais.Lines.Clear;
    vForm.qrMensagensFinaisTM.Lines.Clear;
  end;

  if not Em(vOrcamento.Status, ['VR', 'VE', 'RE']) then begin
    vForm.qrMensagensFinais.Lines.Clear;
    vForm.qrMensagensFinaisTM.Lines.Clear;
    vForm.qrMensagensFinais.Lines.Add('Or�amento v�lido pelo prazo de ' + NFormat(Sessao.getParametrosEmpresa.QtdeDiasValidadeOrcamento) + ' dia(s).' );
    vForm.qrMensagensFinaisTM.Lines.Add('Or�amento v�lido pelo prazo de ' + NFormat(Sessao.getParametrosEmpresa.QtdeDiasValidadeOrcamento) + ' dia(s).' );
    vForm.qrMensagensFinais.Lines.Add(vOrcamento.ObservacoesCaixa);
    vForm.qrMensagensFinaisTM.Lines.Add(vOrcamento.ObservacoesCaixa);
  end;
  vAnexo := TStringList.Create;
  if pEnviarEmail then
  begin
    pCaminhoPDF := GetCurrentDir + '\Orcamento\PDF\';
    ForceDirectories(pCaminhoPdf);
    pCaminhoPDF := pCaminhoPDF + '\' +NFormat(pOrcamentoId) + '.pdf';
    vAnexo.Add(pCaminhoPDF);
    vForm.qrRelatorioA4.ExportToFilter(TQRPDFDocumentFilter.Create(pCaminhoPDF));
    EnviarEmail(vOrcamento.EmailCliente,
                'Or�amento: '+ NFormat(pOrcamentoId),
                'Segue em anexo seu or�amento! Obrigado pela prefer�ncia!',
                vAnexo);

  end else
    vForm.Imprimir(1, pGerarPDF);

  vAnexo.Free;
  vForm.Free;
end;

procedure EnviarPorEmail(const pOrcamentoId: Integer; pOcultarCustoUnitario: Boolean = False; pOcultarCustoTotalItens: Boolean = False);
begin
  Imprimir(pOrcamentoId,False,True, pOcultarCustoUnitario, pOcultarCustoTotalItens);
end;

procedure TFormImpressaoOrcamentoGrafico.QRBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  vQtdeEntrega: string;
begin
  inherited;
  qrProdutoIdTM.Caption     := NFormat(FProdutos[FPosicImp].ProdutoId);

  if FProdutos[FPosicImp].MultiploVenda > 1 then begin
    vQtdeEntrega := FloatToStr(FProdutos[FPosicImp].Quantidade / FProdutos[FPosicImp].MultiploVenda);
    vQtdeEntrega := '(' + vQtdeEntrega + ' ' + FProdutos[FPosicImp].UnidadeEntrega + ') ' + FProdutos[FPosicImp].NomeProduto;
    qrNomeProdutoTM.Caption := vQtdeEntrega;
  end
  else
    qrNomeProdutoTM.Caption   := FProdutos[FPosicImp].NomeProduto;

  qrValorUnitarioTM.Caption := NFormat(FProdutos[FPosicImp].PrecoUnitario);
  qrQuantidadeTM.Caption    := NFormatEstoque(FProdutos[FPosicImp].Quantidade);
  qrUnidadeTM.Caption       := FProdutos[FPosicImp].UnidadeVenda;
  qrValorTotalTM.Caption    := NFormat(FProdutos[FPosicImp].ValorTotal);
end;

procedure TFormImpressaoOrcamentoGrafico.qrBandaDetalhesBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
var
  vPrecoUnitario,
  vDescontoUnitario,
  vValotToal : Double;
  vQtdeEntrega: string;
begin
  inherited;
  vDescontoUnitario := FProdutos[FPosicImp].ValorTotalDesconto + FProdutos[FPosicImp].ValorDescUnitPrecoManual ;

  if (qrDescontoUnitario.Enabled)
    //and(Em(FProdutos[FPosicImp].TipoPrecoUtilitario,['MAN','VAR']))
    then
  begin
     vPrecoUnitario :=  Arredondar(FProdutos[FPosicImp].ValorOriginal * StrToFloat(qrIndice.Caption),2);
     vValotToal     := (vPrecoUnitario * FProdutos[FPosicImp].Quantidade) - vDescontoUnitario;
  end else
  begin
    vPrecoUnitario := FProdutos[FPosicImp].PrecoUnitario;
    vValotToal     := FProdutos[FPosicImp].ValorTotal;
  end;

  qrProdutoId.Caption        := NFormat(FProdutos[FPosicImp].ProdutoId);

  if FProdutos[FPosicImp].MultiploVenda > 1 then begin
    vQtdeEntrega := FloatToStr(FProdutos[FPosicImp].Quantidade / FProdutos[FPosicImp].MultiploVenda);
    vQtdeEntrega := '(' + vQtdeEntrega + ' ' + FProdutos[FPosicImp].UnidadeEntrega + ') ' + FProdutos[FPosicImp].NomeProduto;
    qrNomeProduto.Caption := vQtdeEntrega;
  end
  else
    qrNomeProduto.Caption      := FProdutos[FPosicImp].NomeProduto;

  qrMarca.Caption            := FProdutos[FPosicImp].NomeMarca;
  qrValorUnitario.Caption    := NFormat(vPrecoUnitario);
  qrDescontoUnitario.Caption := NFormat(vDescontoUnitario);
  qrQuantidade.Caption       := NFormatEstoque(FProdutos[FPosicImp].Quantidade);
  qrUnidade.Caption          := FProdutos[FPosicImp].UnidadeVenda;
  qrPrcoUnitLiq.Caption      := NFormat(vValotToal / FProdutos[FPosicImp].Quantidade);
  qrValorTotal.Caption       := NFormat(vValotToal);
end;

procedure TFormImpressaoOrcamentoGrafico.qrRelatorioBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicImp := -1;
end;

procedure TFormImpressaoOrcamentoGrafico.qrRelatorioNeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicImp);
  MoreData := (Length(FProdutos) > FPosicImp);
end;

procedure TFormImpressaoOrcamentoGrafico.qrRelatorioNFBeforePrint(
  Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicImp := -1;
end;

procedure TFormImpressaoOrcamentoGrafico.qrRelatorioNFNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicImp);
  MoreData := (Length(FProdutos) > FPosicImp);
end;

end.
