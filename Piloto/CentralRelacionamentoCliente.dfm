inherited FormCentralRelacionamentoCliente: TFormCentralRelacionamentoCliente
  Caption = 'Central de relacionamento do cliente'
  ClientHeight = 427
  ClientWidth = 787
  Visible = False
  OnShow = FormShow
  ExplicitWidth = 793
  ExplicitHeight = 456
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label1: TLabel
    Left = 699
    Top = 17
    Visible = False
    ExplicitLeft = 699
    ExplicitTop = 17
  end
  inherited pnOpcoes: TPanel
    Height = 427
    ExplicitHeight = 427
    inherited sbPesquisar: TSpeedButton
      Visible = False
    end
  end
  inherited eID: TEditLuka
    Left = 699
    Top = 31
    Visible = False
    ExplicitLeft = 699
    ExplicitTop = 31
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 733
    Top = 8
    Visible = False
    ExplicitLeft = 733
    ExplicitTop = 8
  end
  object pcDados: TPageControl
    Left = 122
    Top = 59
    Width = 665
    Height = 369
    ActivePage = tsGeral
    TabOrder = 3
    object tsGeral: TTabSheet
      Caption = 'Geral'
      object lbCPF_CNPJ: TLabel
        Left = 3
        Top = 5
        Width = 52
        Height = 14
        Caption = 'CPF / CNPJ'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object lbRazaoSocialNome: TLabel
        Left = 117
        Top = 5
        Width = 69
        Height = 14
        Caption = 'Raz'#227'o social'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object lbNomeFantasiaApelido: TLabel
        Left = 385
        Top = 5
        Width = 81
        Height = 14
        Caption = 'Nome fantasia'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object lb11: TLabel
        Left = 2
        Top = 46
        Width = 48
        Height = 14
        Caption = 'Telefone'
      end
      object lb12: TLabel
        Left = 153
        Top = 45
        Width = 39
        Height = 14
        Caption = 'Celular'
      end
      object lb9: TLabel
        Left = 291
        Top = 46
        Width = 35
        Height = 14
        Caption = 'E-mail'
      end
      object PageControl1: TPageControl
        Left = 0
        Top = 96
        Width = 657
        Height = 241
        ActivePage = TabSheet1
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = 'Acesso remoto'
          object Label7: TLabel
            Left = 229
            Top = 5
            Width = 10
            Height = 14
            Caption = 'IP'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = 'Calibri'
            Font.Style = []
            ParentFont = False
          end
          object Label8: TLabel
            Left = 455
            Top = 4
            Width = 34
            Height = 14
            Caption = 'Senha'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = 'Calibri'
            Font.Style = []
            ParentFont = False
          end
          object Label9: TLabel
            Left = 5
            Top = 44
            Width = 69
            Height = 14
            Caption = 'Observa'#231#245'es'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = 'Calibri'
            Font.Style = []
            ParentFont = False
          end
          object Label12: TLabel
            Left = 5
            Top = 5
            Width = 43
            Height = 14
            Caption = 'Usu'#225'rio'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = 'Calibri'
            Font.Style = []
            ParentFont = False
          end
          object eRemotoIP: TEditLuka
            Left = 229
            Top = 19
            Width = 220
            Height = 22
            CharCase = ecUpperCase
            TabOrder = 1
            OnKeyDown = ProximoCampo
            TipoCampo = tcTexto
            ApenasPositivo = False
            AsInt = 0
            CasasDecimais = 0
            NaoAceitarEspaco = False
          end
          object eRemotoSenha: TEditLuka
            Left = 455
            Top = 19
            Width = 190
            Height = 22
            CharCase = ecUpperCase
            TabOrder = 2
            OnKeyDown = ProximoCampo
            TipoCampo = tcTexto
            ApenasPositivo = False
            AsInt = 0
            CasasDecimais = 0
            NaoAceitarEspaco = False
          end
          object eRemotoObservacoes: TEditLuka
            Left = 5
            Top = 59
            Width = 640
            Height = 22
            CharCase = ecUpperCase
            TabOrder = 3
            OnKeyDown = eRemotoObservacoesKeyDown
            TipoCampo = tcTexto
            ApenasPositivo = False
            AsInt = 0
            CasasDecimais = 0
            NaoAceitarEspaco = False
          end
          object sgAcessoRemoto: TGridLuka
            Left = 6
            Top = 97
            Width = 640
            Height = 113
            ColCount = 4
            DefaultRowHeight = 19
            DrawingStyle = gdsGradient
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
            TabOrder = 4
            OnDblClick = sgAcessoRemotoDblClick
            OnDrawCell = sgAcessoRemotoDrawCell
            OnKeyDown = sgAcessoRemotoKeyDown
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 38619
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clActiveCaption
            HCol.Strings = (
              'IP'
              'Senha'
              'Usu'#225'rio'
              'Observa'#231#245'es')
            Grid3D = False
            RealColCount = 5
            Indicador = True
            AtivarPopUpSelecao = False
            ColWidths = (
              120
              106
              111
              275)
          end
          object eRemotoUsuario: TEditLuka
            Left = 5
            Top = 19
            Width = 218
            Height = 22
            CharCase = ecUpperCase
            TabOrder = 0
            OnKeyDown = ProximoCampo
            TipoCampo = tcTexto
            ApenasPositivo = False
            AsInt = 0
            CasasDecimais = 0
            NaoAceitarEspaco = False
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Acesso banco de dados'
          ImageIndex = 1
          object Label2: TLabel
            Left = 5
            Top = 5
            Width = 43
            Height = 14
            Caption = 'Usu'#225'rio'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = 'Calibri'
            Font.Style = []
            ParentFont = False
          end
          object Label3: TLabel
            Left = 113
            Top = 5
            Width = 34
            Height = 14
            Caption = 'Senha'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = 'Calibri'
            Font.Style = []
            ParentFont = False
          end
          object Label4: TLabel
            Left = 215
            Top = 5
            Width = 57
            Height = 14
            Caption = 'IP Servidor'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = 'Calibri'
            Font.Style = []
            ParentFont = False
          end
          object Label5: TLabel
            Left = 325
            Top = 5
            Width = 28
            Height = 14
            Caption = 'Porta'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = 'Calibri'
            Font.Style = []
            ParentFont = False
          end
          object Label6: TLabel
            Left = 435
            Top = 5
            Width = 38
            Height = 14
            Caption = 'Servi'#231'o'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = 'Calibri'
            Font.Style = []
            ParentFont = False
          end
          object eBancoUsuario: TEditLuka
            Left = 5
            Top = 19
            Width = 102
            Height = 22
            CharCase = ecUpperCase
            TabOrder = 0
            OnKeyUp = ProximoCampo
            TipoCampo = tcTexto
            ApenasPositivo = False
            AsInt = 0
            CasasDecimais = 0
            NaoAceitarEspaco = False
          end
          object eBancoSenha: TEditLuka
            Left = 113
            Top = 19
            Width = 96
            Height = 22
            CharCase = ecUpperCase
            TabOrder = 1
            OnKeyUp = ProximoCampo
            TipoCampo = tcTexto
            ApenasPositivo = False
            AsInt = 0
            CasasDecimais = 0
            NaoAceitarEspaco = False
          end
          object eBancoIpServidor: TEditLuka
            Left = 215
            Top = 19
            Width = 104
            Height = 22
            CharCase = ecUpperCase
            TabOrder = 2
            OnKeyUp = ProximoCampo
            TipoCampo = tcTexto
            ApenasPositivo = False
            AsInt = 0
            CasasDecimais = 0
            NaoAceitarEspaco = False
          end
          object sgBancoDados: TGridLuka
            Left = 5
            Top = 48
            Width = 640
            Height = 161
            DefaultRowHeight = 19
            DrawingStyle = gdsGradient
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
            TabOrder = 5
            OnDblClick = sgBancoDadosDblClick
            OnDrawCell = sgBancoDadosDrawCell
            OnKeyDown = sgBancoDadosKeyDown
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 38619
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clActiveCaption
            HCol.Strings = (
              'Usu'#225'rio'
              'Senha'
              'IP Servidor'
              'Porta'
              'Servi'#231'o')
            Grid3D = False
            RealColCount = 5
            Indicador = True
            AtivarPopUpSelecao = False
            ColWidths = (
              121
              130
              142
              108
              103)
          end
          object eBancoPorta: TEditLuka
            Left = 325
            Top = 19
            Width = 104
            Height = 22
            CharCase = ecUpperCase
            TabOrder = 3
            OnKeyUp = ProximoCampo
            TipoCampo = tcTexto
            ApenasPositivo = False
            AsInt = 0
            CasasDecimais = 0
            NaoAceitarEspaco = False
          end
          object eBancoServico: TEditLuka
            Left = 435
            Top = 19
            Width = 104
            Height = 22
            CharCase = ecUpperCase
            TabOrder = 4
            OnKeyUp = eBancoServicoKeyUp
            TipoCampo = tcTexto
            ApenasPositivo = False
            AsInt = 0
            CasasDecimais = 0
            NaoAceitarEspaco = False
          end
        end
      end
      object eCPF_CNPJ: TEditCPF_CNPJ_Luka
        Left = 3
        Top = 19
        Width = 108
        Height = 22
        ReadOnly = True
        TabOrder = 1
        Text = ''
        Tipo = []
      end
      object eRazaoSocial: TEditLuka
        Left = 117
        Top = 19
        Width = 262
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 2
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eNomeFantasia: TEditLuka
        Left = 385
        Top = 19
        Width = 269
        Height = 22
        CharCase = ecUpperCase
        Enabled = False
        TabOrder = 3
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eTelefone: TEditTelefoneLuka
        Left = 2
        Top = 60
        Width = 144
        Height = 22
        EditMask = '(99) 9999-9999;1; '
        MaxLength = 14
        ReadOnly = True
        TabOrder = 4
        Text = '(  )     -    '
      end
      object eCelular: TEditTelefoneLuka
        Left = 153
        Top = 60
        Width = 132
        Height = 22
        EditMask = '(99) 99999-9999;1; '
        MaxLength = 15
        ReadOnly = True
        TabOrder = 5
        Text = '(  )      -    '
        Celular = True
      end
      object eEmail: TEditLuka
        Left = 291
        Top = 60
        Width = 363
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 6
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
    end
    object tsOcorrencias: TTabSheet
      Caption = 'Ocorr'#234'ncias'
      ImageIndex = 1
      object Label11: TLabel
        Left = 400
        Top = 1
        Width = 34
        Height = 14
        Caption = 'Status'
      end
      object sbFiltrarOcorrencia: TSpeedButton
        Left = 527
        Top = 184
        Width = 127
        Height = 22
        Caption = 'Filtrar'
        Flat = True
        OnClick = sbFiltrarOcorrenciaClick
      end
      object sbGravarOcorrencia: TSpeedButton
        Left = 527
        Top = 240
        Width = 127
        Height = 22
        Caption = 'Gravar'
        Flat = True
        OnClick = sbGravarOcorrenciaClick
      end
      object sbNovaOcorrencia: TSpeedButton
        Left = 527
        Top = 212
        Width = 127
        Height = 22
        Caption = 'Nova ocorr'#234'ncia'
        Flat = True
        OnClick = sbNovaOcorrenciaClick
      end
      object sbCancelarOcorrencia: TSpeedButton
        Left = 527
        Top = 268
        Width = 127
        Height = 22
        Caption = 'Cancelar'
        Flat = True
        OnClick = sbCancelarOcorrenciaClick
      end
      object Label13: TLabel
        Left = 527
        Top = 0
        Width = 57
        Height = 14
        Caption = 'Prioridade'
      end
      object sgOcorrencia: TGridLuka
        Left = 3
        Top = 41
        Width = 651
        Height = 108
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        ColCount = 7
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        PopupMenu = pmOpcoesOcorrencias
        TabOrder = 3
        OnClick = sgOcorrenciaClick
        OnDrawCell = sgOcorrenciaDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'C'#243'digo'
          'Data cria'#231#227'o'
          'Data conclus'#227'o'
          'Status'
          'Prioridade'
          'Solicita'#231#227'o'
          'Anexo?')
        OnGetCellColor = sgOcorrenciaGetCellColor
        Grid3D = False
        RealColCount = 7
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          62
          83
          102
          117
          117
          73
          51)
      end
      object pgDetalhesOcorrencia: TPageControl
        Left = 3
        Top = 156
        Width = 518
        Height = 181
        ActivePage = tsDescricaoOcorrencia
        TabOrder = 4
        object tsDescricaoOcorrencia: TTabSheet
          Caption = 'Descri'#231#227'o'
          inline frDescricaoOcorrencia: TFrEdicaoTexto
            Left = 0
            Top = 0
            Width = 510
            Height = 152
            Align = alClient
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBackground = False
            ParentColor = False
            ParentFont = False
            TabOrder = 0
            TabStop = True
            ExplicitWidth = 510
            ExplicitHeight = 152
            inherited pnFerramentas: TPanel
              Width = 510
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 510
              inherited cbFonte: TComboBox
                Height = 22
                ExplicitHeight = 22
              end
              inherited cbTamanho: TComboBox
                Height = 22
                ExplicitHeight = 22
              end
            end
            inherited rtTexto: TRichEdit
              Width = 499
              Height = 100
              ExplicitLeft = 10
              ExplicitWidth = 499
              ExplicitHeight = 100
            end
            inherited stbStatus: TStatusBar
              Top = 133
              Width = 510
              ExplicitLeft = 0
              ExplicitTop = 133
              ExplicitWidth = 510
            end
          end
        end
        object tsSolucaoOcorrencia: TTabSheet
          Caption = 'Solu'#231#227'o'
          ImageIndex = 1
          inline frSolucaoOcorrencia: TFrEdicaoTexto
            Left = 0
            Top = 0
            Width = 510
            Height = 152
            Align = alClient
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBackground = False
            ParentColor = False
            ParentFont = False
            TabOrder = 0
            TabStop = True
            ExplicitWidth = 510
            ExplicitHeight = 152
            inherited pnFerramentas: TPanel
              Width = 510
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 510
              inherited cbFonte: TComboBox
                Height = 22
                ExplicitHeight = 22
              end
              inherited cbTamanho: TComboBox
                Height = 22
                ExplicitHeight = 22
              end
            end
            inherited rtTexto: TRichEdit
              Width = 499
              Height = 100
              ExplicitLeft = 10
              ExplicitWidth = 499
              ExplicitHeight = 100
            end
            inherited stbStatus: TStatusBar
              Top = 133
              Width = 510
              ExplicitLeft = 0
              ExplicitTop = 133
              ExplicitWidth = 510
            end
          end
        end
      end
      object cbStatusOcorrencia: TComboBoxLuka
        Left = 400
        Top = 16
        Width = 121
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = 16645629
        TabOrder = 2
        OnKeyDown = ProximoCampo
        Items.Strings = (
          'Aberto'
          'Conclu'#237'do'
          'Recusado'
          'Programa'#231#227'o'
          'Teste')
        Valores.Strings = (
          'AB'
          'CO'
          'RE'
          'PR'
          'TE')
        AsInt = 0
      end
      inline frDataCadastro: TFrDataInicialFinal
        Left = 3
        Top = 1
        Width = 195
        Height = 40
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 3
        ExplicitTop = 1
        ExplicitWidth = 195
        ExplicitHeight = 40
        inherited Label1: TLabel
          Width = 93
          Height = 14
          ExplicitWidth = 93
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrDataConclusao: TFrDataInicialFinal
        Left = 201
        Top = 1
        Width = 199
        Height = 40
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 201
        ExplicitTop = 1
        ExplicitWidth = 199
        ExplicitHeight = 40
        inherited Label1: TLabel
          Width = 101
          Height = 14
          Caption = 'Data de conclus'#227'o'
          ExplicitWidth = 101
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrArquivos: TFrArquivos
        Left = 526
        Top = 300
        Width = 127
        Height = 22
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 5
        TabStop = True
        ExplicitLeft = 526
        ExplicitTop = 300
        ExplicitWidth = 127
        ExplicitHeight = 22
        inherited sbArquivos: TSpeedButtonLuka
          Width = 127
          Height = 22
          Caption = 'Anexos'
          OnClick = FrArquivossbArquivosClick
          TagImagem = 0
          ExplicitTop = 8
          ExplicitWidth = 127
          ExplicitHeight = 22
        end
      end
      object cbPrioridadeOcorrencia: TComboBoxLuka
        Left = 527
        Top = 15
        Width = 121
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = 16645629
        TabOrder = 6
        OnKeyDown = ProximoCampo
        Items.Strings = (
          'Baixa'
          'M'#233'dia'
          'Alta'
          'Urgente')
        Valores.Strings = (
          'BA'
          'ME'
          'AL'
          'UR')
        AsInt = 0
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Observa'#231#245'es'
      ImageIndex = 2
      object Label10: TLabel
        Left = -1
        Top = 4
        Width = 69
        Height = 14
        Caption = 'Observa'#231#245'es'
      end
      object mmObservacoesCliente: TMemo
        Left = 3
        Top = 24
        Width = 651
        Height = 313
        TabOrder = 0
      end
    end
  end
  inline FrCliente: TFrClientes
    Left = 125
    Top = 9
    Width = 380
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 4
    TabStop = True
    ExplicitLeft = 125
    ExplicitTop = 9
    ExplicitWidth = 380
    ExplicitHeight = 41
    inherited CkAspas: TCheckBox [0]
    end
    inherited CkFiltroDuplo: TCheckBox [1]
    end
    inherited CkPesquisaNumerica: TCheckBox [2]
      Checked = False
      State = cbUnchecked
    end
    inherited CkMultiSelecao: TCheckBox [3]
      Top = 28
      ExplicitTop = 28
    end
    inherited PnTitulos: TPanel [4]
      Width = 380
      ExplicitWidth = 380
      inherited lbNomePesquisa: TLabel
        Width = 39
        Caption = 'Cliente'
        ExplicitWidth = 39
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 275
        ExplicitLeft = 275
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel [5]
      Left = 348
      Height = 25
      Align = alNone
      ExplicitLeft = 348
      ExplicitHeight = 25
      inherited sbPesquisa: TSpeedButton
        Top = 1
        ExplicitTop = 1
      end
    end
    inherited ckSomenteAtivos: TCheckBox [6]
      Checked = True
      State = cbChecked
    end
    inherited sgPesquisa: TGridLuka [7]
      Width = 350
      Height = 24
      Align = alNone
      ExplicitWidth = 350
      ExplicitHeight = 24
    end
  end
  object pmOpcoesOcorrencias: TPopupMenu
    Left = 568
    Top = 16
    object miReceberPedidoSelecionado: TMenuItem
      Caption = 'Alterar status'
      object Concluda1: TMenuItem
        Caption = 'Conclu'#237'da'
        OnClick = Concluda1Click
      end
      object Programao1: TMenuItem
        Caption = 'Programa'#231#227'o'
        OnClick = Programao1Click
      end
      object Emteste1: TMenuItem
        Caption = 'Teste'
        OnClick = Emteste1Click
      end
      object Recusada1: TMenuItem
        Caption = 'Recusada'
        OnClick = Recusada1Click
      end
      object Reabrir1: TMenuItem
        Caption = 'Reabrir'
        OnClick = Reabrir1Click
      end
    end
    object miN2: TMenuItem
      Caption = '-'
    end
    object miConsultarStatusServicoNFe: TMenuItem
      Caption = 'Alterar prioridade'
      object Mdia1: TMenuItem
        Caption = 'Baixa'
        OnClick = Mdia1Click
      end
      object Mdia2: TMenuItem
        Caption = 'M'#233'dia'
        OnClick = Mdia2Click
      end
      object Alta1: TMenuItem
        Caption = 'Alta'
        OnClick = Alta1Click
      end
      object Urgente1: TMenuItem
        Caption = 'Urgente'
        OnClick = Urgente1Click
      end
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Vincularsolicitao1: TMenuItem
      Caption = 'Vincular solicita'#231#227'o'
    end
  end
end
