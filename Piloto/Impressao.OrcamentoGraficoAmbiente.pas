unit Impressao.OrcamentoGraficoAmbiente;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatorioSimples, QRCtrls, _Sessao, _Biblioteca,
  QuickRpt, Vcl.ExtCtrls, _Orcamentos, _OrcamentosItens, _RecordsOrcamentosVendas, _Empresas,
  QRExport, QRPDFFilt, QRWebFilt, Data.DB, MemDS, DBAccess, Ora, OraCall, _RecordsCadastros,
  Vcl.Imaging.jpeg;

type
  TFormImpressaoOrcamentoGraficoAmbiente = class(TFormHerancaRelatorioSimples)
    qrMensagensFinais: TQRMemo;
    qr20: TQRLabel;
    qr22: TQRLabel;
    qr29: TQRLabel;
    qrTotalSerPago: TQRLabel;
    qrValorDesconto: TQRLabel;
    qrValorOutrasDespesas: TQRLabel;
    qrl1: TQRLabel;
    qrlGrValorFrete: TQRLabel;
    qrTotalProdutos: TQRLabel;
    qr16: TQRLabel;
    QRLabel4: TQRLabel;
    qrOrcamentoIdTM: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel8: TQRLabel;
    qrClienteTM: TQRLabel;
    QRLabel12: TQRLabel;
    qrCelularClienteTM: TQRLabel;
    QRLabel5: TQRLabel;
    qrEnderecoClienteTM: TQRLabel;
    QRLabel9: TQRLabel;
    qrCidadeClienteTM: TQRLabel;
    QRLabel13: TQRLabel;
    qrEstadoClienteTM: TQRLabel;
    QRLabel7: TQRLabel;
    qrBairroClienteTM: TQRLabel;
    QRLabel11: TQRLabel;
    qrCepClienteTM: TQRLabel;
    QRBand1: TQRBand;
    qrProdutoIdTM: TQRLabel;
    qrVendedorTM: TQRLabel;
    QRShape4: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    qrNomeProdutoTM: TQRLabel;
    qrValorUnitarioTM: TQRLabel;
    qrQuantidadeTM: TQRLabel;
    qrUnidadeTM: TQRLabel;
    qrValorTotalTM: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    qrTotalProdutosTM: TQRLabel;
    qrValorOutrasDespesastm: TQRLabel;
    qrlGrValorFreteTM: TQRLabel;
    qrValorDescontoTM: TQRLabel;
    qrTotalSerPagoTM: TQRLabel;
    qrMensagensFinaisTM: TQRMemo;
    QRPDFFilter1: TQRPDFFilter;
    QRShape3: TQRShape;
    qrOrcamentoAmbiente: TQuickRep;
    QRBand2: TQRBand;
    QRLabel28: TQRLabel;
    qrEmpresaAmbiente: TQRLabel;
    qrEnderecoEmpresaAmbiente: TQRLabel;
    QRLabel32: TQRLabel;
    qrEmitidoEmAmbiente: TQRLabel;
    QRLabel34: TQRLabel;
    qrBairroEmpresaAmbiente: TQRLabel;
    QRLabel36: TQRLabel;
    qrCidadeEmpresaAmbiente: TQRLabel;
    QRLabel38: TQRLabel;
    qrCNPJAmbiente: TQRLabel;
    QRLabel40: TQRLabel;
    qrTelefoneEmpresaAmbiente: TQRLabel;
    qrCelularEmpresaAmbiente: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    qrEmailEmpresaAmbiente: TQRLabel;
    QRLabel46: TQRLabel;
    QRShape5: TQRShape;
    qrLogoEmpresaAmbiente: TQRImage;
    QRLabel47: TQRLabel;
    qrCEPAmbiente: TQRLabel;
    qrGroup: TQRGroup;
    QRGroup2: TQRGroup;
    QRLabel21: TQRLabel;
    qrAmbiente: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    qlDescontoUnitario: TQRLabel;
    qlPrcoUnitLiq: TQRLabel;
    QRLabel30: TQRLabel;
    qrProdutoId: TQRLabel;
    qrNomeProduto: TQRLabel;
    qrMarca: TQRLabel;
    qrQuantidade: TQRLabel;
    qrUnidade: TQRLabel;
    qrValorUnitario: TQRLabel;
    qrDescontoUnitario: TQRLabel;
    qrPrcoUnitLiq: TQRLabel;
    qrValorTotal: TQRLabel;
    QRSubDetail1: TQRSubDetail;
    OraSession1: TOraSession;
    qMestre: TOraQuery;
    qDetalhe: TOraQuery;
    QRDBText1: TQRDBText;
    QRLabel49: TQRLabel;
    qrGroupHeader: TQRBand;
    QRLabel50: TQRLabel;
    qrSubdetail: TQRSubDetail;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    qrGroupFooter: TQRBand;
    QRLabel52: TQRLabel;
    qDetalheTotal: TOraQuery;
    QRDBText4: TQRDBText;
    QRLabel53: TQRLabel;
    QRDBText5: TQRDBText;
    qDetalhePRODUTO_ID: TFloatField;
    qDetalheNOME_PRODUTO: TStringField;
    qDetalheQUANTIDADE: TFloatField;
    qDetalhePRECO_UNITARIO: TFloatField;
    qDetalheNOME_MARCA: TStringField;
    qDetalheTOTAL_PRODUTO: TFloatField;
    qDetalheUNIDADE_VENDA: TStringField;
    QRLabel54: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    qMestreAMBIENTE: TStringField;
    qDetalheVALOR_TOTAL_DESCONTO: TFloatField;
    qDetalhePRECO_LIQUIDO: TFloatField;
    qDetalheTotalTOTAL_PRODUTO: TFloatField;
    QRBand3: TQRBand;
    qrUsuarioImpressaoAmbiente: TQRLabel;
    QRLabel31: TQRLabel;
    QRMemo1: TQRMemo;
    QRLabel33: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel37: TQRLabel;
    qrTotalGeralAmbiente: TQRLabel;
    qrValorDescontoAmbiente: TQRLabel;
    qrOutrasDespesasAmbiente: TQRLabel;
    QRLabel45: TQRLabel;
    qrValorFreteAmbiente: TQRLabel;
    qrTotalProdutosAmbiente: TQRLabel;
    QRLabel61: TQRLabel;
    qr2: TQRLabel;
    qr8: TQRLabel;
    qr12: TQRLabel;
    qr18: TQRLabel;
    qrCidadeCliente: TQRLabel;
    qrEnderecoCliente: TQRLabel;
    qrCliente: TQRLabel;
    qrOrcamentoId: TQRLabel;
    qr9: TQRLabel;
    qrVendedor: TQRLabel;
    qr17: TQRLabel;
    qrCondicaoPagamento: TQRLabel;
    qr4: TQRLabel;
    qrTelefoneCliente: TQRLabel;
    qr14: TQRLabel;
    qrCelularCliente: TQRLabel;
    qr15: TQRLabel;
    qrBairroCliente: TQRLabel;
    qr21: TQRLabel;
    qrCepCliente: TQRLabel;
    qrIndice: TQRLabel;
    QRShape6: TQRShape;
    procedure qrBandaDetalhesBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qrRelatorioBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure qrRelatorioNeedData(Sender: TObject; var MoreData: Boolean);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qrRelatorioNFBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure qrRelatorioNFNeedData(Sender: TObject; var MoreData: Boolean);
    procedure qrBandaSubDetalhesBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure qrGroupBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand3BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qMestreAfterScroll(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);

  private
    FPosicImp: Integer;
    FPosicImpProd: Integer;
    FProdutos: TArray<RecOrcamentosItensImpressao>;
    FProdutosAmbienteImpressao: TArray<RecAmbientes>;
    FOrcamentoId: Integer;
  end;

procedure Imprimir(
  const pOrcamentoId: Integer;
  FProdutosAmbiente: TArray<RecAmbientes>;
  pGerarPDF: Boolean = False;
  pEnviarEmail: Boolean = False
);

procedure EnviarPorEmail(const pOrcamentoId: Integer; FProdutosAmbiente: TArray<RecAmbientes>);

implementation

{$R *.dfm}

uses _Email;

procedure Imprimir(
  const pOrcamentoId: Integer;
  FProdutosAmbiente: TArray<RecAmbientes>;
  pGerarPDF: Boolean = False;
  pEnviarEmail: Boolean = False
);
var
  vForm: TFormImpressaoOrcamentoGraficoAmbiente;
  vOrcamento: RecOrcamentosImpressao;
  vItens: TArray<RecOrcamentosItensImpressao>;
  vImpressora: RecImpressora;
  pCaminhoPDF: String;
  vcnt: Integer;
  vPrecoUnitario,
  vValotToal : Double;
  vDadosEmpresa: TArray<RecEmpresas>;
  vArquivoPDFSalvar: string;
  vAnexo: TStringList;
begin
  if pOrcamentoId = 0 then
    Exit;

  vOrcamento := _Orcamentos.BuscarInformacoesImpressao(Sessao.getConexaoBanco, pOrcamentoId);
  if vOrcamento.OrcamentoId = 0 then begin
    NenhumRegistro;
    Exit;
  end;

  vItens := _OrcamentosItens.BuscarInformacoesImpressao(Sessao.getConexaoBanco, pOrcamentoId);
  if vItens = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vImpressora := Sessao.getImpressora( toOrcamento );
  if not vImpressora.validaImpresssora then
    Exit;

  vForm := TFormImpressaoOrcamentoGraficoAmbiente.Create(Application, vImpressora);


  vForm.FOrcamentoId := pOrcamentoId;
  vForm.FProdutosAmbienteImpressao := FProdutosAmbiente;
  vForm.FProdutos := vItens;

  // Preencher cabe�alho
  vDadosEmpresa := _Empresas.BuscarEmpresas(Sessao.getConexaoBanco, 0, [vOrcamento.EmpresaId]);
  vForm.qrEmpresaAmbiente.Caption         := vDadosEmpresa[0].RazaoSocial;
  vForm.qrCNPJAmbiente.Caption            := vDadosEmpresa[0].Cnpj;
  vForm.qrEnderecoEmpresaAmbiente.Caption := vDadosEmpresa[0].Logradouro + ' ' + vDadosEmpresa[0].Complemento + ' n� ' + vDadosEmpresa[0].Numero;
  vForm.qrBairroEmpresaAmbiente.Caption   := vDadosEmpresa[0].NomeBairro;
  vForm.qrCidadeEmpresaAmbiente.Caption   := vDadosEmpresa[0].NomeCidade + ' / ' + vDadosEmpresa[0].EstadoId;
  vForm.qrCEPAmbiente.Caption             := vDadosEmpresa[0].Cep;
  vForm.qrTelefoneEmpresaAmbiente.Caption := vDadosEmpresa[0].TelefonePrincipal;
  vForm.qrCelularEmpresaAmbiente.Caption  := vDadosEmpresa[0].TelefoneFax;
  vForm.qrEmailEmpresaAmbiente.Caption    := vDadosEmpresa[0].EMail;

//    qrSistema.Caption := 'Emitido por ' + Sessao.getVersaoSistema;
//  vForm.PreencherCabecalho( vOrcamento.EmpresaId, True );

  if Em(vOrcamento.Status, ['VR', 'VE', 'RE']) then
  begin
    vForm.qr13.Caption := 'Pedido de venda';
  end else
    vForm.qr13.Caption := 'Or�amento';

  vForm.qrOrcamentoId.Caption         := NFormat(pOrcamentoId);
  vForm.qrOrcamentoIdTM.Caption       := NFormat(pOrcamentoId);
  vForm.qrVendedor.Caption            := NFormat(vOrcamento.VendedorId) + ' - ' + vOrcamento.NomeVendedor;
  vForm.qrVendedorTM.Caption          := NFormat(vOrcamento.VendedorId) + ' - ' + vOrcamento.NomeVendedor;
  vForm.qrCondicaoPagamento.Caption   := vOrcamento.NomeCondicaoPagamento;
  vForm.qrCliente.Caption             := NFormat(vOrcamento.ClienteId) + ' - ' + vOrcamento.NomeCliente;
  vForm.qrClienteTM.Caption           := NFormat(vOrcamento.ClienteId) + ' - ' + vOrcamento.NomeCliente;
  vForm.qrEnderecoCliente.Caption     := vOrcamento.Logradouro + ' ' + vOrcamento.Complemento + ' ' + vOrcamento.Numero;
  vForm.qrEnderecoClienteTM.Caption   := vOrcamento.Logradouro + ' ' + vOrcamento.Complemento + ' ' + vOrcamento.Numero;
  vForm.qrBairroCliente.Caption       := vOrcamento.NomeBairro;
  vForm.qrBairroClienteTM.Caption     := vOrcamento.NomeBairro;
  vForm.qrCidadeCliente.Caption       := vOrcamento.NomeCidade + ' - ' + vOrcamento.NomeEstado;
  vForm.qrCidadeClienteTM.Caption     := vOrcamento.NomeCidade;
  //vForm.qrEstadoCliente.Caption       := vOrcamento.NomeEstado;
  vForm.qrEstadoClienteTM.Caption     := vOrcamento.NomeEstado;
  vForm.qrCepCliente.Caption          := vOrcamento.Cep;
  vForm.qrCepClienteTM.Caption        := vOrcamento.Cep;
  vForm.qrTelefoneCliente.Caption     := vOrcamento.TelefonePrincipal;
  vForm.qrCelularCliente.Caption      := vOrcamento.TelefoneCelular;
  vForm.qrCelularClienteTM.Caption    := vOrcamento.TelefoneCelular;

  vForm.qrTotalProdutosAmbiente.Caption  := NFormat(vOrcamento.ValorTotalProdutos);
  vForm.qrOutrasDespesasAmbiente.Caption := NFormat(vOrcamento.ValorOutrasDespesas);
  vForm.qrValorFreteAmbiente.Caption     := NFormat(vOrcamento.ValorFrete + vOrcamento.ValorFreteItens);

  vForm.qrValorDescontoAmbiente.Caption  := NFormat(vOrcamento.ValorDesconto);
  vForm.qrTotalGeralAmbiente.Caption     := NFormat(vOrcamento.ValorTotal);
  vForm.qrValorDesconto.Caption           := NFormat(vOrcamento.ValorDesconto);

  //ARRUMAR AQUI
  if vOrcamento.Status = 'RE' then
  begin
    vForm.qrMensagensFinais.Lines.Clear;
    vForm.qrMensagensFinaisTM.Lines.Clear;
  end;

  //ARRUMAR AQUI
  if not Em(vOrcamento.Status, ['VR', 'VE', 'RE']) then begin
    vForm.qrMensagensFinais.Lines.Clear;
    vForm.qrMensagensFinaisTM.Lines.Clear;
    vForm.qrMensagensFinais.Lines.Add('Or�amento v�lido pelo prazo de ' + NFormat(Sessao.getParametrosEmpresa.QtdeDiasValidadeOrcamento) + ' dia(s).' );
    vForm.qrMensagensFinaisTM.Lines.Add('Or�amento v�lido pelo prazo de ' + NFormat(Sessao.getParametrosEmpresa.QtdeDiasValidadeOrcamento) + ' dia(s).' );
    vForm.qrMensagensFinais.Lines.Add(vOrcamento.ObservacoesCaixa);
    vForm.qrMensagensFinaisTM.Lines.Add(vOrcamento.ObservacoesCaixa);
  end;

  vForm.qMestre.Close;
  vForm.qMestre.ParamByName('ORCAMENTO').Value := pOrcamentoId;
  vForm.qMestre.Open;

  if pEnviarEmail then
  begin
    pCaminhoPDF := GetCurrentDir + '\Orcamento\PDF\';
    ForceDirectories(pCaminhoPdf);
    pCaminhoPDF := pCaminhoPDF + '\' +NFormat(pOrcamentoId) + '.pdf';
    vForm.qrOrcamentoAmbiente.ExportToFilter(TQRPDFDocumentFilter.Create(pCaminhoPDF));
    vAnexo := TStringList.Create;
    vAnexo.Add(pCaminhoPDF);
    EnviarEmail(vOrcamento.EmailCliente,
                'Or�amento: '+ NFormat(pOrcamentoId),
                'Segue em anexo seu or�amento! Obrigado pela prefer�ncia!',
                vAnexo);

    vAnexo.Free;
  end else begin
    if pGerarPDF then begin
      vArquivoPDFSalvar := _Biblioteca.getNomeCaminhoArquivoSalvar('pdf', 'Arquivo PDF');
      vForm.qrOrcamentoAmbiente.ExportToFilter(TQRPDFDocumentFilter.Create(vArquivoPDFSalvar));
    end
    else begin
      vForm.qrOrcamentoAmbiente.Preview;
    end;
  end;

  vForm.Free;
end;

procedure EnviarPorEmail(const pOrcamentoId: Integer; FProdutosAmbiente: TArray<RecAmbientes>);
begin
  Imprimir(pOrcamentoId, FProdutosAmbiente, False, True);
end;

procedure TFormImpressaoOrcamentoGraficoAmbiente.FormCreate(Sender: TObject);
var
  vDataHora: TDateTime;
  vImagem: TJPEGImage;
begin
  inherited;

  qDetalheTotal.Session := Sessao.getConexaoBanco;
  qDetalhe.Session := Sessao.getConexaoBanco;
  qMestre.Session := Sessao.getConexaoBanco;

  vDataHora := Sessao.getDataHora;
  qrEmitidoEmAmbiente.Caption        := 'Emitido em ' + DFormat(vDataHora) + ' �s ' + HFormat(vDataHora);
  qrUsuarioImpressaoAmbiente.Caption := NFormat(Sessao.getUsuarioLogado.funcionario_id) + ' - ' + Sessao.getUsuarioLogado.nome;

  vImagem := nil;

  if Sessao.getParametrosEmpresa.LogoEmpresa = nil then
    Exit;

  try
    try
      Sessao.getParametrosEmpresa.LogoEmpresa.Position := 0;
      vImagem := TJPEGImage.Create;
      vImagem.LoadFromStream(Sessao.getParametrosEmpresa.LogoEmpresa);
      qrLogoEmpresaAmbiente.Picture.Graphic := vImagem;
    except

    end;
  finally
    vImagem.Free;
  end;
end;

procedure TFormImpressaoOrcamentoGraficoAmbiente.qMestreAfterScroll(
  DataSet: TDataSet);
begin
  inherited;
  if not qMestre.Eof then begin
    qDetalheTotal.Close;
    qDetalheTotal.ParamByName('AMBIENTE').Value := qMestre.FieldByName('AMBIENTE').AsString;
    qDetalheTotal.ParamByName('ORCAMENTO').Value := FOrcamentoId;
    qDetalheTotal.Open;

    qDetalhe.Close;
    qDetalhe.ParamByName('AMBIENTE').Value := qMestre.FieldByName('AMBIENTE').AsString;
    qDetalhe.ParamByName('ORCAMENTO').Value := FOrcamentoId;
    qDetalhe.Open;
  end;

end;

procedure TFormImpressaoOrcamentoGraficoAmbiente.QRBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrProdutoIdTM.Caption     := NFormat(FProdutos[FPosicImp].ProdutoId);
  qrNomeProdutoTM.Caption   := FProdutos[FPosicImp].NomeProduto;
  qrValorUnitarioTM.Caption := NFormat(FProdutos[FPosicImp].PrecoUnitario);
  qrQuantidadeTM.Caption    := NFormatEstoque(FProdutos[FPosicImp].Quantidade);
  qrUnidadeTM.Caption       := FProdutos[FPosicImp].UnidadeVenda;
  qrValorTotalTM.Caption    := NFormat(FProdutos[FPosicImp].ValorTotal);
end;

procedure TFormImpressaoOrcamentoGraficoAmbiente.QRBand3BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  //
end;

procedure TFormImpressaoOrcamentoGraficoAmbiente.qrBandaDetalhesBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
var
  vPrecoUnitario,
  vDescontoUnitario,
  vValotToal : Double;
begin
  inherited;
//  vDescontoUnitario := FProdutos[FPosicImp].ValorTotalDesconto + FProdutos[FPosicImp].ValorDescUnitPrecoManual ;
//
//  if (qrDescontoUnitario.Enabled)
//    //and(Em(FProdutos[FPosicImp].TipoPrecoUtilitario,['MAN','VAR']))
//    then
//  begin
//     vPrecoUnitario :=  Arredondar(FProdutos[FPosicImp].ValorOriginal * StrToFloat(qrIndice.Caption),2);
//     vValotToal     := (vPrecoUnitario * FProdutos[FPosicImp].Quantidade) - vDescontoUnitario;
//  end else
//  begin
//    vPrecoUnitario := FProdutos[FPosicImp].PrecoUnitario;
//    vValotToal     := FProdutos[FPosicImp].ValorTotal;
//  end;

  // Passar essa parte para outra brand
  qrAmbiente.Caption := FProdutosAmbienteImpressao[FPosicImp].NomeAmbiente;
//  FPosicImpProd := 0;

//  qrProdutoId.Caption        := NFormat(FProdutos[FPosicImp].ProdutoId);
//  qrNomeProduto.Caption      := FProdutos[FPosicImp].NomeProduto;
//  qrMarca.Caption            := FProdutos[FPosicImp].NomeMarca;
//  qrValorUnitario.Caption    := NFormat(vPrecoUnitario);
//  qrDescontoUnitario.Caption := NFormat(vDescontoUnitario);
//  qrQuantidade.Caption       := NFormatEstoque(FProdutos[FPosicImp].Quantidade);
//  qrUnidade.Caption          := FProdutos[FPosicImp].UnidadeVenda;
//  qrPrcoUnitLiq.Caption      := NFormat(vValotToal / FProdutos[FPosicImp].Quantidade);
//  qrValorTotal.Caption       := NFormat(vValotToal);

  while(FPosicImpProd < Length(FProdutosAmbienteImpressao[FPosicImp].Produtos)) do begin
    vDescontoUnitario := FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].ValorTotalDescProp;

    vPrecoUnitario := FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].PrecoUnitari;
    vValotToal     := FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].ValorTotalProduto;

    qrProdutoId.Caption        := NFormat(FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].ProdutoId);
    qrNomeProduto.Caption      := FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].NomeProduto;
    qrMarca.Caption            := FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].Marca;
    qrValorUnitario.Caption    := NFormat(vPrecoUnitario);
    qrDescontoUnitario.Caption := NFormat(vDescontoUnitario);
    qrQuantidade.Caption       := NFormatEstoque(FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].Quantidade);
    qrUnidade.Caption          := FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].UndVenda;
    qrPrcoUnitLiq.Caption      := NFormat(vValotToal / FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].Quantidade);
    qrValorTotal.Caption       := NFormat(vValotToal);

    Inc(FPosicImpProd);
  end;

//  if FPosicImpProd < Length(FProdutosAmbienteImpressao[FPosicImp].Produtos) then begin
//    vDescontoUnitario := FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].ValorTotalDescProp;
//
//    vPrecoUnitario := FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].PrecoUnitari;
//    vValotToal     := FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].ValorTotalProduto;
//
//    qrProdutoId.Caption        := NFormat(FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].ProdutoId);
//    qrNomeProduto.Caption      := FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].NomeProduto;
//    qrMarca.Caption            := FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].Marca;
//    qrValorUnitario.Caption    := NFormat(vPrecoUnitario);
//    qrDescontoUnitario.Caption := NFormat(vDescontoUnitario);
//    qrQuantidade.Caption       := NFormatEstoque(FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].Quantidade);
//    qrUnidade.Caption          := FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].UndVenda;
//    qrPrcoUnitLiq.Caption      := NFormat(vValotToal / FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].Quantidade);
//    qrValorTotal.Caption       := NFormat(vValotToal);
//
//    Inc(FPosicImpProd);
//    PrintBand := True;
//  end
//  else begin
//    FPosicImpProd := 0;
//    PrintBand := False;
//  end;
end;

procedure TFormImpressaoOrcamentoGraficoAmbiente.qrBandaSubDetalhesBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  vPrecoUnitario,
  vDescontoUnitario,
  vValotToal : Double;
begin
  inherited;
  vDescontoUnitario := FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].ValorTotalDescProp;

  vPrecoUnitario := FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].PrecoUnitari;
  vValotToal     := FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].ValorTotalProduto;

  qrProdutoId.Caption        := NFormat(FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].ProdutoId);
  qrNomeProduto.Caption      := FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].NomeProduto;
  qrMarca.Caption            := FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].Marca;
  qrValorUnitario.Caption    := NFormat(vPrecoUnitario);
  qrDescontoUnitario.Caption := NFormat(vDescontoUnitario);
  qrQuantidade.Caption       := NFormatEstoque(FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].Quantidade);
  qrUnidade.Caption          := FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].UndVenda;
  qrPrcoUnitLiq.Caption      := NFormat(vValotToal / FProdutosAmbienteImpressao[FPosicImp].Produtos[FPosicImpProd].Quantidade);
  qrValorTotal.Caption       := NFormat(vValotToal);

  Inc(FPosicImpProd);
  if FPosicImpProd < Length(FProdutosAmbienteImpressao[FPosicImp].Produtos) then
    PrintBand := True
  else
    PrintBand := True;
end;

procedure TFormImpressaoOrcamentoGraficoAmbiente.qrGroupBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  //
end;

procedure TFormImpressaoOrcamentoGraficoAmbiente.qrRelatorioBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicImp := -1;
  FPosicImpProd := 0;
end;

procedure TFormImpressaoOrcamentoGraficoAmbiente.qrRelatorioNeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicImp);
  MoreData := (Length(FProdutosAmbienteImpressao) > FPosicImp);
//  if FPosicImpProd = -1 then
//    Inc(FPosicImp);
//
//  Inc(FPosicImpProd);
//  if Length(FProdutosAmbienteImpressao[FPosicImp].Produtos) > FPosicImpProd then
//    MoreData := True
//  else begin
//    Inc(FPosicImp);
//    FPosicImpProd := -1;
//    MoreData := (Length(FProdutosAmbienteImpressao) > FPosicImp);
//  end;
end;

procedure TFormImpressaoOrcamentoGraficoAmbiente.qrRelatorioNFBeforePrint(
  Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicImp := -1;
end;

procedure TFormImpressaoOrcamentoGraficoAmbiente.qrRelatorioNFNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicImp);
  MoreData := (Length(FProdutos) > FPosicImp);
end;

end.
