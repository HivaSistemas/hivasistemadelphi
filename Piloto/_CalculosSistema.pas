unit _CalculosSistema;

interface

uses
  _Biblioteca, System.Math, _RecordsCadastros, _ParametrosEmpresa;

type
  RecCalculosFormacaoCusto = record
    function getPrecoSugerido(
      pPercMargem: Double;
      pPercCustoVenda: Double;
      pPrecoBase: Double
    ): Double;

    function getCustoComercial(
      pCustoCompra: Double;
      pPrecoVenda: Double;
      pPercCustoVenda: Double
    ): Double;

    function getPercentualVariacao(
      pValorAnterior: Double;
      pNovoValor: Double
    ): Double;
  end;

  RecCalculosOrcamento = record
    function getPercOutrasDespesas(pValorProdutos: Double; pValorOutrasDespeas: Double; pDecimais: Integer = 2): Double;
    function getPercJuros(pValorProdutos: Currency; pValorJuros: Currency; pDecimais: Integer = 2): Double;

    function getPercDesconto(
      pValorProdutos: Currency;
      pValorOutrasDespeas: Double;
      pValorDesconto: Double;
      pDecimais: Integer = 2
    ): Double;

    function getPercDescontoValorLiquido(
      pValorAnterior: Double;
      pNovoValor: Double;
      pDecimais: Integer = 2
    ): Double;

    function getPercLucroBruto(
      pValorTotalLiquido: Double;
      pValorTotalLucroBruto: Double;
      pDecimais: Integer = 2
    ): Double;

    function getPercLucroLiquido(
      pValorTotalLiquido: Double;
      pValorTotalLucroLiquido: Double;
      pDecimais: Integer = 2
    ): Double;

    function getJurosFechamentoAcumulado(
      pValorLiquido: Double;
      pPercentualJurosMensal: Double;
      pQtdeDiasAtraso: Integer
    ): Double;
  end;

  RecCalculosEntrada = record
    public
      function CalcularPrecoFinal(
        pValorTotal: Double;
        pValorOutrasDespesas: Double;
        pValorICMSSTUsuario: Double;
        pValorIPIUsuario: Double;
        pValorFrete: Double;
        pValorDesconto: Double;
        pValorDifal: Double;
        pValorOutrosCustos: Double;
        pQuantidade: Double
      ): Double;

      function CalcularPrecoLiquido(
        pPrecoFinal: Double;
        pValorICMS: Double;
        pValorPIS: Double;
        pValorCofins: Double;
        pValorICMSFrete: Double;
        pValorPisFrete: Double;
        pValorCofinsFrete: Double;
        pTemICMSSt: Boolean;
        pQuantidade: Double;
        pParametrosEmpresa: RecParametrosEmpresa
      ): Double;
  end;

  RecCalculosJuros = record
    function ConverterJurosMensalParaDiario(pPercentualJuros: Extended ): Double;
    function CalcularJurosFuturos( pValor: Double; pPrazos: TArray<Integer>; pPercentualJuros: Double ): Double;
  end;

  RecCalculosSistema = record
    CalcOrcamento: RecCalculosOrcamento;
    CalcFormacaoCusto: RecCalculosFormacaoCusto;
    RecCalculosJuros: RecCalculosJuros;
    CalcEntradas: RecCalculosEntrada;
  end;

implementation

uses
  _Sessao;

{ RecCalculosOrcamento }

(*
  M�todo utilizado para retornar o percentual de desconto de outras despesas de um produto ou venda
  F�rmula = OutrasDespesas / Total dos produtos * 100
*)
function RecCalculosOrcamento.getPercOutrasDespesas(pValorProdutos: Double; pValorOutrasDespeas: Double; pDecimais: Integer = 2): Double;
begin
  Result := Arredondar((pValorOutrasDespeas / pValorProdutos) * 100, pDecimais);
end;

(*
  M�todo utilizado para retornar o percentual de juros uma venda
  F�rmula = pValorJuros / Total * 100
*)
function RecCalculosOrcamento.getPercJuros(pValorProdutos: Currency; pValorJuros: Currency; pDecimais: Integer = 2): Double;
begin
  Result := 0;

  if pValorJuros = 0 then
    Exit;

  Result := Arredondar((pValorJuros / pValorProdutos) * 100, pDecimais);
end;

function RecCalculosFormacaoCusto.getCustoComercial(
  pCustoCompra,
  pPrecoVenda,
  pPercCustoVenda: Double
): Double;
begin
  Result := pCustoCompra + (pPrecoVenda * pPercCustoVenda * 0.01);
end;

function RecCalculosFormacaoCusto.getPrecoSugerido(
  pPercMargem: Double;
  pPercCustoVenda: Double;
  pPrecoBase: Double
): Double;
var
  vIndice: Double;
begin
  Result := 0;

  if _Biblioteca.NFormatN(pPercMargem) = '' then
    Exit;

  if _Biblioteca.NFormatN(pPrecoBase) = '' then
    Exit;

  vIndice := (100 - pPercCustoVenda - pPercMargem) * 0.01;
  Result := pPrecoBase / vIndice;
end;

function RecCalculosFormacaoCusto.getPercentualVariacao(pValorAnterior, pNovoValor: Double): Double;
begin
  Result := (1 - (pNovoValor / pValorAnterior)) * 100 * -1;
end;

(*
  M�todo utilizado para retornar o valor de juros a se cobrar no fechamento do acumulado
  F�rmula = Desconto / (Total dos produtos + Outras despesas) * 100
*)
function RecCalculosOrcamento.getJurosFechamentoAcumulado(
  pValorLiquido,
  pPercentualJurosMensal: Double;
  pQtdeDiasAtraso: Integer
): Double;
begin
  Result := 0;

  if pQtdeDiasAtraso <= 0 then
    Exit;

  Result := (pPercentualJurosMensal / 30 * pQtdeDiasAtraso * 0.01) * pValorLiquido;
end;

(*
  M�todo utilizado para retornar o percentual de desconto de um produto ou venda
  F�rmula = Desconto / (Total dos produtos + Outras despesas) * 100
*)
function RecCalculosOrcamento.getPercDesconto(
  pValorProdutos: Currency;
  pValorOutrasDespeas: Double;
  pValorDesconto: Double;
  pDecimais: Integer = 2
): Double;
begin
  Result := 0;

  if (pValorProdutos + pValorOutrasDespeas) = 0  then
    Exit;

  Result := Arredondar(pValorDesconto / (pValorProdutos + pValorOutrasDespeas) * 100, pDecimais);
end;

(*
  M�todo utilizado para retornar o percentual de desconto de um produto ou venda
  Este direfe do de cima pois aqui passamos o valor total do novo pre�o e n�o somente o desconto que foi dado
  F�rmula = ( Valor anterior - novo valor ) / (Total dos produtos + Outras despesas) * 100
*)
function RecCalculosOrcamento.getPercDescontoValorLiquido(
  pValorAnterior: Double;
  pNovoValor: Double;
  pDecimais: Integer = 2
): Double;
begin
  Result := Arredondar((pValorAnterior - pNovoValor) / pValorAnterior * 100, pDecimais);
end;

(*
  M�todo utilizado para retornar o percentual de lucro bruto
  F�rmula = Total liquido / Total Lucro Bruto
*)
function RecCalculosOrcamento.getPercLucroBruto(
  pValorTotalLiquido: Double;
  pValorTotalLucroBruto: Double;
  pDecimais: Integer = 2
): Double;
begin
  Result := 0;

  if _Biblioteca.NFormatN(pValorTotalLiquido) = '' then
    Exit;

  Result := Arredondar(pValorTotalLucroBruto / pValorTotalLiquido * 100, pDecimais);
end;

(*
  M�todo utilizado para retornar o percentual de lucro l�quido
  F�rmula = Total liquido / Total Lucro Liquido
*)
function RecCalculosOrcamento.getPercLucroLiquido(
  pValorTotalLiquido: Double;
  pValorTotalLucroLiquido: Double;
  pDecimais: Integer = 2
): Double;
begin
  Result := 0;

  if _Biblioteca.NFormatN(pValorTotalLiquido) = '' then
    Exit;

  Result := Arredondar(pValorTotalLucroLiquido / pValorTotalLiquido * 100, pDecimais);
end;

{ RecCalculosJuros }

function RecCalculosJuros.ConverterJurosMensalParaDiario(pPercentualJuros: Extended): Double;
begin
  Result := 0;

  if pPercentualJuros = 0 then
    Exit;

  Result := System.Math.Power( 1 + pPercentualJuros, 1 / 30 ) - 1;
end;

function RecCalculosJuros.CalcularJurosFuturos(pValor: Double; pPrazos: TArray<Integer>; pPercentualJuros: Double): Double;
var
  vIndiceParcela: extended;
  vDivisor: extended;

  function DiferencaPeriodos(pLinha: Integer): Integer;
  begin
    if pLinha = 0 then
      Result := pPrazos[pLinha]
    else
      Result := pPrazos[pLinha] - pPrazos[pLinha - 1];
  end;

  function Base(pLinha: integer): Double;
  var
    i: Integer;
  begin
    Result := 1;

    if pLinha > 2 then begin
      for i := pLinha downto 3 do begin
        if i = pLinha then
          Result := 1 + 1 / System.Math.Power( 1 + pPercentualJuros * 0.01, DiferencaPeriodos(i) )
        else
          Result := 1 + Result / System.Math.Power( 1 + pPercentualJuros * 0.01, DiferencaPeriodos(i) )
      end;
    end;
  end;

begin
  if pPrazos = nil then begin
    Result := 0;
    Exit;
  end;

  if Length(pPrazos) = 1 then
    vIndiceParcela := System.Math.Power( 1 + pPercentualJuros * 0.01, DiferencaPeriodos(0) )
  else begin
    vIndiceParcela := System.Math.Power( 1 + pPercentualJuros * 0.01, DiferencaPeriodos(0) ) * System.Math.Power( 1 + pPercentualJuros * 0.01, DiferencaPeriodos(1) );

    vDivisor := 1 + System.Math.Power( 1 + pPercentualJuros * 0.01, DiferencaPeriodos(1) );

    if Length(pPrazos) > 2 then
      vDivisor := vDivisor + Base( high(pPrazos) ) / System.Math.Power( 1 + pPercentualJuros * 0.01, DiferencaPeriodos(2) );

    vIndiceParcela := vIndiceParcela / vDivisor;
  end;

  Result := pValor * vIndiceParcela * Length(pPrazos);
end;

{ RecCalculosEntrada }
// Enviar sempre o valor total
function RecCalculosEntrada.CalcularPrecoFinal(
  pValorTotal,
  pValorOutrasDespesas,
  pValorICMSSTUsuario,
  pValorIPIUsuario,
  pValorFrete,
  pValorDesconto: Double;
  pValorDifal: Double;
  pValorOutrosCustos: Double;
  pQuantidade: Double
): Double;
begin
  Result :=
    _Biblioteca.Arredondar(
      (pValorTotal / pQuantidade) +
      (pValorOutrasDespesas / pQuantidade) +
      (pValorICMSSTUsuario / pQuantidade) +
      (pValorIPIUsuario / pQuantidade) +
      (pValorFrete / pQuantidade) +
      (pValorDifal / pQuantidade)+
      (pValorOutrosCustos / pQuantidade) -
      (pValorDesconto / pQuantidade),
      2
    );
end;

// Enviar sempre o valor total
function RecCalculosEntrada.CalcularPrecoLiquido(
  pPrecoFinal: Double;
  pValorICMS: Double;
  pValorPIS: Double;
  pValorCofins: Double;
  pValorICMSFrete: Double;
  pValorPisFrete: Double;
  pValorCofinsFrete: Double;
  pTemICMSSt: Boolean;
  pQuantidade: Double;
  pParametrosEmpresa: RecParametrosEmpresa
): Double;
var
  vValorCreditoIcms: Double;
  vValorCreditoPis: Double;
  vValorCreditoCofins: Double;
  vValorCreditoFreteIcms: Double;
begin
  if pParametrosEmpresa.RegimeTributario = 'SN' then begin
    vValorCreditoIcms      := 0;
    vValorCreditoPis       := 0;
    vValorCreditoCofins    := 0;
    vValorCreditoFreteIcms := 0;
  end
  else begin
    if pTemICMSSt then
      vValorCreditoIcms := 0
    else
      vValorCreditoIcms := pValorICMS / pQuantidade;

    vValorCreditoPis       := pValorPIS / pQuantidade;
    vValorCreditoCofins    := pValorCofins / pQuantidade;
    vValorCreditoFreteIcms := pValorICMSFrete / pQuantidade;
  end;

  Result :=
    _Biblioteca.Arredondar(
      pPrecoFinal -
      vValorCreditoIcms -
      vValorCreditoPis -
      vValorCreditoCofins -
      vValorCreditoFreteIcms,
      2
    );
end;

end.
