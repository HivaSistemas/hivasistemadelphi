inherited FormAberturaCaixa: TFormAberturaCaixa
  Caption = 'Abertura do caixa'
  ClientHeight = 202
  ClientWidth = 453
  ExplicitWidth = 459
  ExplicitHeight = 231
  PixelsPerInch = 96
  TextHeight = 14
  object lb11: TLabel [0]
    Left = 126
    Top = 122
    Width = 68
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Valor inicial'
  end
  inherited pnOpcoes: TPanel
    Height = 202
    ExplicitHeight = 202
    inherited sbDesfazer: TSpeedButton
      Top = 53
      ExplicitTop = 53
    end
    inherited sbExcluir: TSpeedButton
      Left = -113
      Visible = False
      ExplicitLeft = -113
    end
    inherited sbPesquisar: TSpeedButton
      Top = 138
      Enabled = False
      Visible = False
      ExplicitTop = 138
    end
  end
  inline FrFuncionario: TFrFuncionarios
    Left = 126
    Top = 28
    Width = 318
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 28
    ExplicitWidth = 318
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 293
      Height = 23
      ExplicitWidth = 293
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 318
      ExplicitWidth = 318
      inherited lbNomePesquisa: TLabel
        Width = 29
        Caption = 'Caixa'
        ExplicitWidth = 29
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 213
        ExplicitLeft = 213
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 293
      Height = 24
      ExplicitLeft = 293
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
    inherited ckCaixa: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  object eValorDinheiroInicial: TEditLuka
    Left = 126
    Top = 137
    Width = 97
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 3
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  inline FrContaOrigem: TFrContas
    Left = 126
    Top = 75
    Width = 322
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 75
    ExplicitWidth = 322
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 297
      Height = 23
      ExplicitWidth = 297
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 322
      ExplicitWidth = 322
      inherited lbNomePesquisa: TLabel
        Width = 122
        Caption = 'Conta origem dinheiro'
        ExplicitWidth = 122
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 217
        ExplicitLeft = 217
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 297
      Height = 24
      ExplicitLeft = 297
      ExplicitHeight = 24
    end
    inherited rgTipo: TRadioGroupLuka
      ItemIndex = 0
    end
  end
end
