unit PesquisaGruposTributacoesEstadualVenda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _Biblioteca,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _Sessao, _GruposTribEstadualVenda,
  Vcl.ExtCtrls;

type
  TFormPesquisaGruposTributacoesEstadualVenda = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  protected
    procedure BuscarRegistros; override;
  end;

function Pesquisar: TObject;

implementation

{$R *.dfm}

const
  coDescricao = 2;
  coAtivo     = 3;

function Pesquisar: TObject;
var
  vRetorno: TObject;
begin
  vRetorno := _HerancaPesquisas.Pesquisar(TFormPesquisaGruposTributacoesEstadualVenda, _GruposTribEstadualVenda.GetFiltros);
  if vRetorno = nil then
    Result := nil
  else
    Result := RecGruposTribEstadualVenda(vRetorno);
end;

{ TFormPesquisaGruposTributacoesEstadualVenda }

procedure TFormPesquisaGruposTributacoesEstadualVenda.BuscarRegistros;
var
  i: Integer;
  vGrupos: TArray<RecGruposTribEstadualVenda>;
begin
  inherited;

  vGrupos :=
    _GruposTribEstadualVenda.BuscarGruposTribEstadualVenda(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text]
    );

  if vGrupos = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vGrupos);

  for i := Low(vGrupos) to High(vGrupos) do begin
    sgPesquisa.Cells[coSelecionado, i + 1] := charNaoSelecionado;
    sgPesquisa.Cells[coCodigo, i + 1]      := NFormat(vGrupos[i].GrupoTribEstadualVendaId);
    sgPesquisa.Cells[coDescricao, i + 1]   := vGrupos[i].Descricao;
    sgPesquisa.Cells[coAtivo, i + 1]       := _Biblioteca.SimNao(vGrupos[i].Ativo);
  end;
  sgPesquisa.SetLinhasGridPorTamanhoVetor(Length(vGrupos));
  SetarFoco(sgPesquisa);
end;

procedure TFormPesquisaGruposTributacoesEstadualVenda.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = coCodigo then
    vAlinhamento := taRightJustify
  else if ACol in[coSelecionado, coAtivo] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
