inherited FormDadosGrupoProduto: TFormDadosGrupoProduto
  Caption = 'Buscar dados grupo de produtos'
  ClientHeight = 119
  ClientWidth = 406
  ExplicitWidth = 412
  ExplicitHeight = 148
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 82
    Width = 406
    inherited sbFinalizar: TSpeedButton
      Left = 137
      ExplicitLeft = 137
    end
  end
  inline FrLinhaProduto: TFrameDepartamentosSecoesLinhas
    Left = 3
    Top = 4
    Width = 398
    Height = 75
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 3
    ExplicitTop = 4
    ExplicitWidth = 398
    ExplicitHeight = 75
    inherited sgPesquisa: TGridLuka
      Width = 373
      Height = 58
      ExplicitWidth = 298
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 398
      ExplicitWidth = 323
      inherited lbNomePesquisa: TLabel
        Width = 102
        Caption = 'Grupo de produtos'
        ExplicitWidth = 102
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 293
        ExplicitLeft = 218
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 373
      Height = 59
      ExplicitLeft = 298
      ExplicitHeight = 24
    end
    inherited rgNivel: TRadioGroup
      ItemIndex = 3
    end
  end
end
