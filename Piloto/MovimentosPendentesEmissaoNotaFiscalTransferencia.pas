unit MovimentosPendentesEmissaoNotaFiscalTransferencia;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatorios, Vcl.ExtCtrls, _Sessao, _Biblioteca, _RecordsEspeciais,
  Vcl.Grids, GridLuka, Vcl.StdCtrls, StaticTextLuka, Vcl.Buttons, _MovimentosPendentesEmissaoNotaFiscalTransferencia,
  _RecordsExpedicao, _RetiradasItens, _EntregasItens, Vcl.Menus, Informacoes.Entrega, Informacoes.Retiradas,
  _ComunicacaoNFE, CheckBoxLuka, _FrameHerancaPrincipal, FrameDataInicialFinal,
  _FrameHenrancaPesquisas, FrameEmpresas;

type
  TFormMovimentosPendentesEmissaoNotaFiscalTransferencia = class(TFormHerancaRelatorios)
    pn1: TPanel;
    st1: TStaticTextLuka;
    sgPendencias: TGridLuka;
    pmOpcoes: TPopupMenu;
    miEmitirNFeTransferencia: TMenuItem;
    pn2: TPanel;
    FrDataEmissao: TFrDataInicialFinal;
    FrEmpresaOrigem: TFrEmpresas;
    FrEmpresaDestino: TFrEmpresas;
    procedure sgPendenciasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure miEmitirNFeTransferenciaClick(Sender: TObject);
    procedure sgPendenciasDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormMovimentosPendentesEmissaoNotaFiscalTransferencia }

const
  cpCodigo         = 0;
  cpTipoMovimento  = 1;
  cpDataHoraMov    = 2;
  cpCliente        = 3;
  cpEmpresaDestino = 4;
  (* Ocultas *)
  cpTipoMovSintet  = 5;


procedure TFormMovimentosPendentesEmissaoNotaFiscalTransferencia.Carregar(Sender: TObject);
var
  i: Integer;
  vMovimentos: TArray<RecMovimentosPendentes>;
begin
  inherited;

  vMovimentos :=
    _MovimentosPendentesEmissaoNotaFiscalTransferencia.buscarMovimentos(
      Sessao.getConexaoBanco,
      FrEmpresaOrigem.getEmpresa.EmpresaId,
      FrDataEmissao.getDataInicial,
      FrDataEmissao.getDataFinal
    );

  if vMovimentos = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  for i := Low(vMovimentos) to High(vMovimentos) do begin
    sgPendencias.Cells[cpCodigo, i + 1]         := NFormat(vMovimentos[i].MovimentoId);
    sgPendencias.Cells[cpTipoMovimento, i + 1]  := vMovimentos[i].TipoMovimentoAnalitico;
    sgPendencias.Cells[cpDataHoraMov, i + 1]    := DHFormat(vMovimentos[i].DataHoraEmissao);
    sgPendencias.Cells[cpCliente, i + 1]        := getInformacao(vMovimentos[i].ClienteId, vMovimentos[i].NomeCliente);
    sgPendencias.Cells[cpEmpresaDestino, i + 1] := getInformacao(vMovimentos[i].EmpresaDestinoId, vMovimentos[i].NomeEmpresaDestino);
    sgPendencias.Cells[cpTipoMovSintet, i + 1]  := vMovimentos[i].TipoMovimento;
  end;
  sgPendencias.SetLinhasGridPorTamanhoVetor( Length(vMovimentos) );

  SetarFoco(sgPendencias);
end;

procedure TFormMovimentosPendentesEmissaoNotaFiscalTransferencia.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrDataEmissao);
end;

procedure TFormMovimentosPendentesEmissaoNotaFiscalTransferencia.miEmitirNFeTransferenciaClick(Sender: TObject);
var
  vRetBanco: RecRetornoBD;

  vNfe: TComunicacaoNFe;
begin
  inherited;
  vRetBanco :=
    _MovimentosPendentesEmissaoNotaFiscalTransferencia.gerarNotaTransferencia(
      Sessao.getConexaoBanco,
      FrEmpresaOrigem.getEmpresa.EmpresaId,
      FrEmpresaDestino.getEmpresa.EmpresaId,
      FrDataEmissao.getDataInicial,
      FrDataEmissao.getDataFinal
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  vNfe := TComunicacaoNFe.Create(Application);
  try

    if not vNfe.EmitirNFe(vRetBanco.AsInt, True) then
      _Biblioteca.Exclamar('N�o foi poss�vel emitir a NFe automaticamente, por favor fa�a a emiss�o pela tela "Rela��o de notas fiscais"!')
    else
      _Biblioteca.RotinaSucesso;

  finally
    vNfe.Free;
  end;

  Carregar(Sender);
end;

procedure TFormMovimentosPendentesEmissaoNotaFiscalTransferencia.sgPendenciasDblClick(Sender: TObject);
begin
  inherited;
  if Em(sgPendencias.Cells[cpTipoMovSintet, sgPendencias.Row], ['VRA', 'VRE']) then
    Informacoes.Retiradas.Informar( SFormatInt(sgPendencias.Cells[cpCodigo, sgPendencias.Row]) )
  else
    Informacoes.Entrega.Informar( SFormatInt(sgPendencias.Cells[cpCodigo, sgPendencias.Row]) );
end;

procedure TFormMovimentosPendentesEmissaoNotaFiscalTransferencia.sgPendenciasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[cpTipoMovimento] then
    vAlinhamento := taCenter
  else if ACol in[cpCodigo] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgPendencias.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormMovimentosPendentesEmissaoNotaFiscalTransferencia.VerificarRegistro(Sender: TObject);
begin
  inherited;
  _Biblioteca.LimparCampos([sgPendencias]);
  if not FrDataEmissao.DatasValidas then begin
    _Biblioteca.Exclamar('As datas de emiss�o n�o foram informadas corretamente, verifique!');
    SetarFoco(FrDataEmissao);
    Abort;
  end;

  if FrEmpresaOrigem.EstaVazio then begin
    _Biblioteca.Exclamar('A empresa de origem n�o foi informada corretamente, verifique!');
    SetarFoco(FrEmpresaOrigem);
    Abort;
  end;

  if FrEmpresaDestino.EstaVazio then begin
    _Biblioteca.Exclamar('A empresa de destino n�o foi informada corretamente, verifique!');
    SetarFoco(FrEmpresaDestino);
    Abort;
  end;

  if FrEmpresaOrigem.getEmpresa.EmpresaId = FrEmpresaDestino.getEmpresa.EmpresaId then begin
    _Biblioteca.Exclamar('A empresa de origem e a de destino devem ser diferentes, verifique!');
    SetarFoco(FrEmpresaDestino);
    Abort;
  end;
end;

end.
