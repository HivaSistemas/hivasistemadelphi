unit Informacoes.AjustaEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Sessao, _Biblioteca,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Grids, GridLuka, Vcl.Mask, EditLukaData, _AjustesEstoque,
  EditLuka, _AjustesEstoqueItens, _RecordsEstoques, System.Math, StaticTextLuka;

type
  TFormInformacoesAjusteEstoque = class(TFormHerancaFinalizar)
    lb1: TLabel;
    lbl1: TLabel;
    lbllb3: TLabel;
    eAjusteId: TEditLuka;
    eUsuarioAjuste: TEditLuka;
    eDataAjuste: TEditLukaData;
    lb2: TLabel;
    eMotivoAjuste: TEditLuka;
    lb3: TLabel;
    eEmpresa: TEditLuka;
    sgProdutos: TGridLuka;
    lbl26: TLabel;
    eObservacoes: TMemo;
    stQuantidadeItensGrid: TStaticText;
    StaticTextLuka1: TStaticTextLuka;
    StaticTextLuka2: TStaticTextLuka;
    stValorTotalProdutosGrid: TStaticTextLuka;
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure Informar(const pAjusteEstoqueId: Integer);

implementation

{$R *.dfm}

const
  coProdutoId     = 0;
  coNome          = 1;
  coMarca         = 2;
  coLote          = 3;
  coQuantidade    = 4;
  coPrecoUnitario = 5;
  coUnidade       = 6;
  coValorTotal    = 7;

procedure Informar(const pAjusteEstoqueId: Integer);
var
  vAjuste: TArray<RecAjusteEstoque>;
  vItens:  TArray<RecAjusteEstoqueItens>;
  vForm: TFormInformacoesAjusteEstoque;
  i: Integer;
begin
  if pAjusteEstoqueId = 0 then
    Exit;

  vAjuste := _AjustesEstoque.BuscarAjustesEstoque(Sessao.getConexaoBanco, 0, [pAjusteEstoqueId]);
  if vAjuste = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vItens := _AjustesEstoqueItens.BuscarAjusteEstoqueItens(Sessao.getConexaoBanco, 0, [pAjusteEstoqueId]);
  if vItens = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vForm := TFormInformacoesAjusteEstoque.Create(Application);
  vForm.ReadOnlyTodosObjetos(True);

  vForm.eAjusteId.AsInt     := vAjuste[0].ajuste_estoque_id;
  vForm.eMotivoAjuste.Text  := NFormat(vAjuste[0].motivo_ajuste_id) + ' - ' + vAjuste[0].MotivoAjusteEstoque;
  vForm.eEmpresa.Text       := NFormat(vAjuste[0].EmpresaId) + ' - ' + vAjuste[0].NomeFantasia;
  vForm.eUsuarioAjuste.Text := NFormat(vAjuste[0].usuario_ajuste_id) + ' - ' + vAjuste[0].NomeUsuarioAjuste;
  vForm.eDataAjuste.AsData  := vAjuste[0].data_hora_ajuste;

  for i := Low(vItens) to High(vItens) do begin
    vForm.sgProdutos.Cells[coProdutoId, i + 1]     := NFormat(vItens[i].produto_id);
    vForm.sgProdutos.Cells[coNome, i + 1]          := vItens[i].NomeProduto;
    vForm.sgProdutos.Cells[coMarca, i + 1]         := NFormat(vItens[i].MarcaId) + ' - ' + vItens[i].NomeMarca;
    vForm.sgProdutos.Cells[coLote, i + 1]          := vItens[i].Lote;
    vForm.sgProdutos.Cells[coQuantidade, i + 1]    := NFormatEstoque(vItens[i].quantidade);
    vForm.sgProdutos.Cells[coPrecoUnitario, i + 1] := NFormatEstoque(vItens[i].PrecoUnitario);
    vForm.sgProdutos.Cells[coUnidade, i + 1]       := vItens[i].Unidade;
    vForm.sgProdutos.Cells[coValorTotal, i + 1]    := NFormatEstoque(vItens[i].ValorTotal);

    vForm.stValorTotalProdutosGrid.Somar(vItens[i].ValorTotal);
  end;
  vForm.sgProdutos.SetLinhasGridPorTamanhoVetor( Length(vItens) );

  vForm.stQuantidadeItensGrid.Caption := NFormat(vForm.sgProdutos.RowCount -1);

  vForm.eObservacoes.Text := vAjuste[0].observacao;

  vForm.ShowModal;

  FreeAndNil(vForm);
end;

procedure TFormInformacoesAjusteEstoque.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coProdutoId, coQuantidade, coPrecoUnitario, coValorTotal] then
    vAlinhamento := taRightJustify
  else if ACol = coUnidade then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
