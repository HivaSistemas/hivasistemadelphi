inherited FormDevolucaoEntradaNotaFiscal: TFormDevolucaoEntradaNotaFiscal
  BorderIcons = [biSystemMenu, biMinimize, biMaximize]
  Caption = 'Devolu'#231#227'o de entrada de nota fiscal'
  ClientHeight = 521
  ClientWidth = 979
  ExplicitWidth = 985
  ExplicitHeight = 550
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label1: TLabel
    Left = 123
    Top = 2
    Width = 42
    Caption = 'Entrada'
    ExplicitLeft = 123
    ExplicitTop = 2
    ExplicitWidth = 42
  end
  object sbInformacoesEntrada: TSpeedButtonLuka [1]
    Left = 185
    Top = 15
    Width = 18
    Height = 18
    Hint = 'Abrir informa'#231#245'es da entrada'
    Flat = True
    NumGlyphs = 2
    TagImagem = 13
    PedirCertificacao = False
    PermitirAutOutroUsuario = False
  end
  object lb2: TLabel [2]
    Left = 209
    Top = 2
    Width = 61
    Height = 14
    Caption = 'Fornecedor'
  end
  object lb1: TLabel [3]
    Left = 416
    Top = 2
    Width = 47
    Height = 14
    Caption = 'N'#186' da NF'
  end
  object lb3: TLabel [4]
    Left = 484
    Top = 2
    Width = 42
    Height = 14
    Caption = 'Modelo'
  end
  object lb4: TLabel [5]
    Left = 534
    Top = 2
    Width = 28
    Height = 14
    Caption = 'S'#233'rie'
  end
  object lb6: TLabel [6]
    Left = 578
    Top = 2
    Width = 76
    Height = 14
    Caption = 'Data emiss'#227'o'
  end
  object lb5: TLabel [7]
    Left = 123
    Top = 78
    Width = 121
    Height = 14
    Caption = 'Observa'#231#245'es para NFe'
  end
  object lb7: TLabel [8]
    Left = 123
    Top = 41
    Width = 95
    Height = 14
    Caption = 'Motivo devolu'#231#227'o'
  end
  inherited pnOpcoes: TPanel
    Height = 521
    TabOrder = 8
    ExplicitHeight = 521
    inherited sbDesfazer: TSpeedButton
      Top = 48
      ExplicitTop = 48
    end
    inherited sbExcluir: TSpeedButton
      Left = -144
      Visible = False
      ExplicitLeft = -144
    end
    inherited sbPesquisar: TSpeedButton
      Top = 91
      ExplicitTop = 91
    end
    inherited sbLogs: TSpeedButton
      Top = 474
      ExplicitTop = 474
    end
  end
  inherited eID: TEditLuka
    Left = 123
    Top = 16
    Width = 60
    TabOrder = 0
    ExplicitLeft = 123
    ExplicitTop = 16
    ExplicitWidth = 60
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 987
    Top = -10
    TabOrder = 9
    Visible = False
    ExplicitLeft = 987
    ExplicitTop = -10
  end
  object eFornecedor: TEditLuka
    Left = 209
    Top = 16
    Width = 203
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 1
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eNumeroNotaFiscal: TEditLuka
    Left = 416
    Top = 16
    Width = 64
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    NumbersOnly = True
    TabOrder = 2
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eModeloNota: TEditLuka
    Left = 484
    Top = 16
    Width = 44
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    NumbersOnly = True
    TabOrder = 3
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eSerieNota: TEditLuka
    Left = 534
    Top = 16
    Width = 38
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    NumbersOnly = True
    TabOrder = 4
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eDataEmissaoNota: TEditLukaData
    Left = 578
    Top = 16
    Width = 83
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1; '
    MaxLength = 10
    TabOrder = 5
    Text = '  /  /    '
  end
  object eObservacoes: TEditLuka
    Left = 123
    Top = 92
    Width = 449
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 400
    TabOrder = 7
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eMotivoDevolucao: TEditLuka
    Left = 123
    Top = 55
    Width = 310
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 400
    TabOrder = 6
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object sgItens: TGridLuka
    Left = 123
    Top = 116
    Width = 854
    Height = 301
    Anchors = [akLeft, akTop, akRight, akBottom]
    ColCount = 10
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 2
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
    TabOrder = 10
    OnClick = sgItensClick
    OnDrawCell = sgItensDrawCell
    OnKeyPress = NumerosVirgula
    OnSelectCell = sgItensSelectCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Produto'
      'Nome'
      'Marca'
      'Qtd.nota'
      'Und.nota'
      'Qtd.devolv.'
      'Und.Hiva'
      'Qtd.dev.Hiva'
      'Local'
      'Nome local')
    OnGetCellColor = sgItensGetCellColor
    OnArrumarGrid = sgItensArrumarGrid
    Grid3D = False
    RealColCount = 30
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      53
      189
      92
      62
      59
      69
      56
      76
      44
      142)
    RowHeights = (
      19
      19)
  end
  object st1: TStaticTextLuka
    Left = 640
    Top = 418
    Width = 110
    Height = 17
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    BevelOuter = bvNone
    Caption = 'Valor produtos'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 11
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object st3: TStaticTextLuka
    Left = 749
    Top = 418
    Width = 105
    Height = 17
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Outras desp.'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 12
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object st4: TStaticTextLuka
    Left = 853
    Top = 418
    Width = 127
    Height = 17
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Desconto'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 13
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object st5: TStaticTextLuka
    Left = 853
    Top = 488
    Width = 127
    Height = 17
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Total da devolu'#231#227'o'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 14
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object st6: TStaticTextLuka
    Left = 640
    Top = 488
    Width = 110
    Height = 17
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    Caption = 'ST'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 15
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object st7: TStaticTextLuka
    Left = 749
    Top = 488
    Width = 105
    Height = 17
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    Caption = 'IPI'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 16
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object stValorProdutos: TStaticTextLuka
    Left = 640
    Top = 435
    Width = 110
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 17
    Transparent = False
    AsInt = 0
    TipoCampo = tcNumerico
    CasasDecimais = 2
  end
  object stOutrasDespesas: TStaticTextLuka
    Left = 749
    Top = 435
    Width = 105
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGreen
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 18
    Transparent = False
    AsInt = 0
    TipoCampo = tcNumerico
    CasasDecimais = 2
  end
  object stDesconto: TStaticTextLuka
    Left = 853
    Top = 435
    Width = 127
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 19
    Transparent = False
    AsInt = 0
    TipoCampo = tcNumerico
    CasasDecimais = 2
  end
  object stValorIcmsSt: TStaticTextLuka
    Left = 640
    Top = 505
    Width = 110
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 20
    Transparent = False
    AsInt = 0
    TipoCampo = tcNumerico
    CasasDecimais = 2
  end
  object stIPI: TStaticTextLuka
    Left = 749
    Top = 505
    Width = 105
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 21
    Transparent = False
    AsInt = 0
    TipoCampo = tcNumerico
    CasasDecimais = 2
  end
  object stTotalDevolucao: TStaticTextLuka
    Left = 853
    Top = 505
    Width = 127
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clTeal
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 22
    Transparent = False
    AsInt = 0
    TipoCampo = tcNumerico
    CasasDecimais = 2
  end
  object st2: TStaticTextLuka
    Left = 640
    Top = 453
    Width = 110
    Height = 17
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Base calc. ICMS'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 23
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object stBaseICMS: TStaticTextLuka
    Left = 640
    Top = 470
    Width = 110
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 33023
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 24
    Transparent = False
    AsInt = 0
    TipoCampo = tcNumerico
    CasasDecimais = 2
  end
  object st9: TStaticTextLuka
    Left = 749
    Top = 453
    Width = 105
    Height = 17
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Valor ICMS'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 25
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object stValorICMS: TStaticTextLuka
    Left = 749
    Top = 470
    Width = 105
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 33023
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 26
    Transparent = False
    AsInt = 0
    TipoCampo = tcNumerico
    CasasDecimais = 2
  end
  object stBaseIcmsSt: TStaticTextLuka
    Left = 853
    Top = 470
    Width = 127
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 27
    Transparent = False
    AsInt = 0
    TipoCampo = tcNumerico
    CasasDecimais = 2
  end
  object st12: TStaticTextLuka
    Left = 853
    Top = 453
    Width = 127
    Height = 17
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Base ICMS ST'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 28
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  inline FrBuscarLotes: TFrBuscarLotes
    Left = 123
    Top = 418
    Width = 399
    Height = 103
    Anchors = [akLeft, akBottom]
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 29
    TabStop = True
    OnExit = FrBuscarLotesExit
    ExplicitLeft = 123
    ExplicitTop = 418
    ExplicitWidth = 399
    ExplicitHeight = 103
    inherited sgValores: TGridLuka
      Width = 399
      Height = 86
      ExplicitWidth = 399
      ExplicitHeight = 86
      ColWidths = (
        55
        65
        67
        72
        64
        64)
    end
    inherited StaticTextLuka1: TStaticTextLuka
      Width = 399
      Caption = 'Lotes ( Produto selecionado )'
      ExplicitWidth = 399
    end
  end
end
