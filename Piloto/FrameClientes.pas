unit FrameClientes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _RecordsCadastros,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Sessao, System.Math, PesquisaClientes, _Clientes,
  Vcl.Buttons, _Biblioteca, CadastroClientes, Vcl.Menus;

type
  TFrClientes = class(TFrameHenrancaPesquisas)
    miN1: TMenuItem;
    miVidaCliente: TMenuItem;
    procedure miVidaClienteClick(Sender: TObject);
  public
    function getCliente(pLinha: Integer = -1): RecClientes;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function AdicionarPesquisandoTodos: TArray<TObject>; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrClientes }

uses
  VidaCliente;

function TFrClientes.AdicionarDireto: TObject;
var
  vRetTela: TRetornoTelaFinalizar<Integer>;
  vClientes: TArray<RecClientes>;
begin
  Result := nil;
  FChaveDigitada := _Biblioteca.RetornaNumeros(FChaveDigitada);

//  // Se for um CPF ou CNPJ
  if Em(Length(FChaveDigitada), [11, 14]) then begin
    if not _Biblioteca.getCNPJ_CPFOk(FChaveDigitada) then begin
      _Biblioteca.Exclamar('O ' + IIfStr(Length(FChaveDigitada) = 11, 'CPF', 'CNPJ') + ' informado � inv�lido!');
      Exit;
    end;

    vClientes := _Clientes.BuscarClientes(Sessao.getConexaoBanco, 3, [FChaveDigitada], ckSomenteAtivos.Checked);
    if vClientes = nil then begin
      if not _Biblioteca.Perguntar('Cliente n�o encontrado com o ' + IIfStr(Length(FChaveDigitada) = 11, 'CPF', 'CNPJ') + ' digitado, deseja cadastra-lo?') then
        Exit;

      vRetTela := CadastroClientes.cadastrarNovoCliente(FChaveDigitada);
      if vRetTela.BuscaCancelada then
        Exit;

      FChaveDigitada := IntToStr(vRetTela.Dados);
    end;
  end;

  if vClientes = nil then
    vClientes := _Clientes.BuscarClientes(Sessao.getConexaoBanco, 0, [FChaveDigitada], ckSomenteAtivos.Checked);

  if vClientes = nil then
    Result := nil
  else
    Result := vClientes[0];
end;

function TFrClientes.AdicionarPesquisando: TObject;
begin
  Result := PesquisaClientes.Pesquisar(ckSomenteAtivos.Checked);
end;

function TFrClientes.AdicionarPesquisandoTodos: TArray<TObject>;
begin
  Result := PesquisaClientes.PesquisarVarios(ckSomenteAtivos.Checked);
end;

function TFrClientes.getCliente(pLinha: Integer): RecClientes;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecClientes(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrClientes.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecClientes(FDados[i]).cadastro_id = RecClientes(pSender).cadastro_id then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrClientes.miVidaClienteClick(Sender: TObject);
begin
  inherited;
  VidaCliente.AbrirVidaCliente( SFormatInt(sgPesquisa.Cells[0, sgPesquisa.Row]) );
end;

procedure TFrClientes.MontarGrid;
var
  i: Integer;
  vSender: RecClientes;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecClientes(FDados[i]);
      AAdd([IntToStr(vSender.cadastro_id), vSender.RecCadastro.nome_fantasia]);
    end;
  end;
end;

end.
