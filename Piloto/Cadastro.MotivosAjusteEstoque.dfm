inherited FormCadastroMotivosAjusteEstoque: TFormCadastroMotivosAjusteEstoque
  Caption = 'Motivos de ajuste de estoque'
  ClientHeight = 206
  ClientWidth = 451
  ExplicitWidth = 457
  ExplicitHeight = 235
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 126
    Top = 85
    Width = 53
    Height = 14
    Caption = 'Descri'#231#227'o'
  end
  inherited Label1: TLabel
    Top = 43
    ExplicitTop = 43
  end
  inherited pnOpcoes: TPanel
    Height = 206
    ExplicitHeight = 164
  end
  inherited eID: TEditLuka
    Top = 59
    Width = 59
    TabOrder = 3
    ExplicitTop = 59
    ExplicitWidth = 59
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 126
    Top = 8
    ExplicitLeft = 126
    ExplicitTop = 8
  end
  object eDescricao: TEditLuka
    Left = 126
    Top = 99
    Width = 317
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 30
    TabOrder = 1
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  inline FrPlanoFinanceiro: TFrPlanosFinanceiros
    Left = 126
    Top = 134
    Width = 317
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 4
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 134
    ExplicitWidth = 317
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 292
      Height = 24
      ExplicitWidth = 361
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 317
      ExplicitWidth = 386
      inherited lbNomePesquisa: TLabel
        Width = 88
        Caption = 'Plano financeiro'
        ExplicitTop = 9
        ExplicitWidth = 88
        ExplicitHeight = 15
      end
      inherited CkChaveUnica: TCheckBox
        Top = 16
        ExplicitTop = 16
      end
      inherited pnSuprimir: TPanel
        Left = 212
        ExplicitLeft = 281
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 292
      Height = 25
      ExplicitLeft = 361
      ExplicitHeight = 25
    end
  end
  object ckIncideFinanceiro: TCheckBoxLuka
    Left = 125
    Top = 182
    Width = 118
    Height = 17
    Caption = 'Incide financeiro'
    TabOrder = 5
    Visible = False
    CheckedStr = 'N'
  end
end
