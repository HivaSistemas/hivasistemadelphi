unit FrameIndicesDescontosVenda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Buttons, _Biblioteca,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Sessao, _IndicesDescontosVenda,
  PesquisaIndicesDescontosVenda, Vcl.Menus;

type
  TFrIndicesDescontosVenda = class(TFrameHenrancaPesquisas)
  public
    function getDados(pLinha: Integer = -1): RecIndicesDescontosVenda;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function AdicionarPesquisandoTodos: TArray<TObject>; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrIndicesDescontosVenda }

function TFrIndicesDescontosVenda.AdicionarDireto: TObject;
var
  vDados: TArray<RecIndicesDescontosVenda>;
begin
  vDados := _IndicesDescontosVenda.BuscarIndicesDescontosVenda(Sessao.getConexaoBanco, 0, [FChaveDigitada]);
  if vDados = nil then
    Result := nil
  else
    Result := vDados[0];
end;

function TFrIndicesDescontosVenda.AdicionarPesquisando: TObject;
begin
  Result := PesquisaIndicesDescontosVenda.Pesquisar;
end;

function TFrIndicesDescontosVenda.AdicionarPesquisandoTodos: TArray<TObject>;
begin

end;

function TFrIndicesDescontosVenda.getDados(pLinha: Integer): RecIndicesDescontosVenda;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecIndicesDescontosVenda(FDados[IIfInt(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrIndicesDescontosVenda.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecIndicesDescontosVenda(FDados[i]).IndiceId = RecIndicesDescontosVenda(pSender).IndiceId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrIndicesDescontosVenda.MontarGrid;
var
  i: Integer;
  vSender: RecIndicesDescontosVenda;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecIndicesDescontosVenda(FDados[i]);
      AAdd([IntToStr(vSender.IndiceId), vSender.Descricao]);
    end;
  end;
end;

end.
