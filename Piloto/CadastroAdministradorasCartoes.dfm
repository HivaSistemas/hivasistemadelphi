inherited FormCadastroAdministradorasCartoes: TFormCadastroAdministradorasCartoes
  Caption = 'Cadastro de administradoras de cart'#245'es'
  ClientHeight = 170
  ClientWidth = 509
  ExplicitWidth = 515
  ExplicitHeight = 199
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [1]
    Left = 126
    Top = 44
    Width = 53
    Height = 14
    Caption = 'Descri'#231#227'o'
  end
  object lbCPF_CNPJ: TLabel [2]
    Left = 386
    Top = 44
    Width = 23
    Height = 14
    Caption = 'CNPJ'
  end
  inherited pnOpcoes: TPanel
    Height = 170
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 458
    Top = 3
    OnKeyDown = ProximoCampo
    ExplicitLeft = 458
    ExplicitTop = 3
  end
  object eDescricao: TEditLuka
    Left = 126
    Top = 58
    Width = 255
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 3
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eCPF_CNPJ: TEditCPF_CNPJ_Luka
    Left = 386
    Top = 58
    Width = 119
    Height = 22
    EditMask = '99.999.999/9999-99'
    MaxLength = 18
    TabOrder = 4
    Text = '  .   .   /    -  '
    OnKeyDown = ProximoCampo
    Tipo = [tccCNPJ]
  end
end
