unit AjusteEstoqueFiscal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Sessao,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Grids, GridLuka, Vcl.Mask, _RecordsEstoques,
  EditLukaData, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameProdutos, _AjustesEstoqueItens,
  Frame.MotivosAjusteEstoque, _Biblioteca, _Estoques, _EstoquesDivisao, _CustosProdutos, _RecordsCadastros, _RecordsEspeciais, _AjustesEstoque,
  ComboBoxLuka, _HerancaCadastro, Frame.Lotes, FrameLocais, CheckBoxLuka,
  StaticTextLuka, BuscarProdutosZerarEstoque;

type
  TFormAjusteEstoqueFiscal = class(TFormHerancaCadastroCodigo)
    lb7: TLabel;
    lb1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    FrProduto: TFrProdutos;
    eQuantidade: TEditLuka;
    sgProdutos: TGridLuka;
    FrMotivoAjusteEstoque: TFrMotivosAjusteEstoque;
    eObservacao: TEditLuka;
    cbNatureza: TComboBoxLuka;
    FrLote: TFrameLotes;
    stEstoqueDisponivel: TStaticTextLuka;
    st4: TStaticText;
    stEstoqueFiscal: TStaticTextLuka;
    StaticText1: TStaticText;
    ePrecoUnitario: TEditLuka;
    eValorTotal: TEditLuka;
    ckCalcularEstoqueFinal: TCheckBoxLuka;
    st5: TStaticText;
    stQuantidadeItensGrid: TStaticText;
    stValorTotalProdutosGrid: TStaticTextLuka;
    StaticText5: TStaticText;
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure eQuantidadeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure sgProdutosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgProdutosClick(Sender: TObject);
    procedure sgProdutosDblClick(Sender: TObject);
    procedure sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure eQuantidadeChange(Sender: TObject);
    procedure ckCalcularEstoqueFinalClick(Sender: TObject);
    procedure sbZerarEstoqueClick(Sender: TObject);
  private
    FLinha: Integer;
    procedure PreencherRegistro(pAjuste: RecAjusteEstoque; pAjusteItens: TArray<RecAjusteEstoqueItens>);
    procedure AdicionarNoProdutoGrid(
      pProdutoId: Integer;
      pNomeProduto: string;
      pMarca: string;
      pUnidade: string;
      pCustoUnitario: Double;
      pValorTotal: Double;
      pQuantidade: Double;
      pNovoEstoqueFisico: Double;
      pLote: string;
      pDataFabricacao: TDate;
      pDataVencimento: TDate;
      pEstoqueFisico: Double;
      pNatureza: string;
      pNaturezaSintetico: string;
      pEstoqueDisponivel: Double;
      pEstoqueInformado: Double
    );

    procedure FrProdutoOnAposPesquisar(Sender: TObject);
    procedure FrProdutoOnAposDeletar(Sender: TObject);

    procedure FrMotivoAjusteOnAposPesquisar(Sender: TObject);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

const
  coProduto           = 0;
  coNomeProduto       = 1;
  coMarca             = 2;
  coUnidade           = 3;
  coPrecoUnitario     = 4;
  coNatureza          = 5;
  coQuantidade        = 6;
  coEstoqueInformado  = 7;
  coValorTotal        = 8;
  coNovoEstoqueFisico = 9;
  coLote              = 10;

  coDataFabricacao    = 11;
  coDataVencimento    = 12;

  coEstoqueFisico     = 13;
  coEstReservado      = 14;
  coNaturezaSint      = 15;
  coEstoqueDisponivel = 16;

procedure TFormAjusteEstoqueFiscal.AdicionarNoProdutoGrid(
  pProdutoId: Integer;
  pNomeProduto: string;
  pMarca: string;
  pUnidade: string;
  pCustoUnitario: Double;
  pValorTotal: Double;
  pQuantidade: Double;
  pNovoEstoqueFisico: Double;
  pLote: string;
  pDataFabricacao: TDate;
  pDataVencimento: TDate;
  pEstoqueFisico: Double;
  pNatureza: string;
  pNaturezaSintetico: string;
  pEstoqueDisponivel: Double;
  pEstoqueInformado: Double
);
var
  vLinha: Integer;
  i: Integer;
begin
  if FLinha = 0 then begin
    vLinha := sgProdutos.Localizar([coProduto, coLote], [NFormat(pProdutoId), pLote]);
    if vLinha > -1 then begin
      Exclamar('Este produto j� foi adicionado no grid!');
      sgProdutos.Row := vLinha;
      sgProdutos.SetFocus;
      Abort;
    end;

    if sgProdutos.Cells[coProduto, 1] = '' then
      FLinha := 1
    else begin
      FLinha := sgProdutos.RowCount;
      sgProdutos.RowCount := sgProdutos.RowCount + 1;
    end;
  end;

  sgProdutos.Cells[coProduto, FLinha]           := NFormat(pProdutoId);
  sgProdutos.Cells[coNomeProduto, FLinha]       := pNomeProduto;
  sgProdutos.Cells[coMarca, FLinha]             := pMarca;
  sgProdutos.Cells[coUnidade, FLinha]           := pUnidade;
  sgProdutos.Cells[coPrecoUnitario, FLinha]     := NFormat(pCustoUnitario);
  sgProdutos.Cells[coValorTotal, FLinha]        := NFormat(pValorTotal);
  sgProdutos.Cells[coQuantidade, FLinha]        := NFormatNEstoqueCasasDecimais(pQuantidade, 4);
  sgProdutos.Cells[coEstoqueInformado, FLinha]  := NFormatNEstoqueCasasDecimais(pEstoqueInformado, 4);
  sgProdutos.Cells[coNovoEstoqueFisico, FLinha] := NFormatNEstoqueCasasDecimais(pNovoEstoqueFisico, 4);
  sgProdutos.Cells[coLote, FLinha]              := pLote;
  sgProdutos.Cells[coDataFabricacao, FLinha]    := DFormatN(pDataFabricacao);
  sgProdutos.Cells[coDataVencimento, FLinha]    := DFormatN(pDataVencimento);
  sgProdutos.Cells[coEstoqueFisico, FLinha]     := NFormatNEstoqueCasasDecimais(pEstoqueFisico, 4);
  sgProdutos.Cells[coNatureza, FLinha]          := pNatureza;
  sgProdutos.Cells[coNaturezaSint, FLinha]      := pNaturezaSintetico;
  sgProdutos.Cells[coEstoqueDisponivel, FLinha] := NFormatNEstoqueCasasDecimais(pEstoqueDisponivel, 4);

  stQuantidadeItensGrid.Caption := NFormat(sgProdutos.RowCount -1);
  stValorTotalProdutosGrid.Clear;
  for i := 1 to sgProdutos.RowCount -1 do
    stValorTotalProdutosGrid.Somar(SFormatDouble(sgProdutos.Cells[coValorTotal, i]));

  sgProdutos.Row := FLinha;
  sgProdutosClick(nil);
  FLinha := 0;
end;

procedure TFormAjusteEstoqueFiscal.BuscarRegistro;
var
  vAjuste: TArray<RecAjusteEstoque>;
  vAjusteItens: TArray<RecAjusteEstoqueItens>;
begin
  vAjuste := _AjustesEstoque.BuscarAjustesEstoque(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if vAjuste = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    eID.SetFocus;
    Exit;
  end;

  vAjusteItens := _AjustesEstoqueItens.BuscarAjusteEstoqueItens(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if vAjusteItens = nil then begin
    _Biblioteca.Exclamar('Produtos do ajuste n�o encontrado!');
    eID.SetFocus;
    Exit;
  end;

  inherited;
  PreencherRegistro(vAjuste[0], vAjusteItens);
end;

procedure TFormAjusteEstoqueFiscal.ckCalcularEstoqueFinalClick(Sender: TObject);
begin
  inherited;
  if ckCalcularEstoqueFinal.Checked then begin
    cbNatureza.Enabled := False;
    cbNatureza.ItemIndex := -1;
  end
  else
    cbNatureza.Enabled := True;
end;

procedure TFormAjusteEstoqueFiscal.eQuantidadeChange(Sender: TObject);
begin
  inherited;
  eValorTotal.AsCurr := ePrecoUnitario.AsCurr * eQuantidade.AsDouble;
end;

procedure TFormAjusteEstoqueFiscal.eQuantidadeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  m1, m2, m3: Double;
  quantidadeDesejada: Double;
begin
  inherited;

  if (Key <> VK_RETURN) or (eID.AsInt > 0) then
    Exit;

  if FrProduto.EstaVazio then begin
    Exclamar('O produto n�o foi informado, verifique!');
    FrProduto.SetFocus;
    Exit;
  end;

  //Calcular se vai ser feito entrada ou saida com base na quantidade desejada de estoque
  quantidadeDesejada := eQuantidade.AsDouble;
  if ckCalcularEstoqueFinal.Checked then begin

    if stEstoqueFiscal.AsDouble > quantidadeDesejada then begin
      cbNatureza.SetIndicePorValor('S');
      eQuantidade.AsDouble := stEstoqueFiscal.AsDouble - quantidadeDesejada;
    end
    else if stEstoqueFiscal.AsDouble < quantidadeDesejada then begin
      cbNatureza.SetIndicePorValor('E');
      eQuantidade.AsDouble := quantidadeDesejada - (stEstoqueFiscal.AsDouble);
    end;

  end;

  if cbNatureza.Text = '' then begin
    Exclamar('A natureza do ajuste n�o foi informado, verifique!');
    SetarFoco(cbNatureza);
    Exit;
  end;

  m1 := FrProduto.getProduto().multiplo_venda;
  m2 := FrProduto.getProduto().multiplo_venda_2;
  m3 := FrProduto.getProduto().multiplo_venda_3;

  if
    not ValidarMultiplo(eQuantidade.AsDouble, m3, False, ((m1 = 0) and (m2 = 0))) and
    not ValidarMultiplo(eQuantidade.AsDouble, m2, False, (m1 = 0)) and
    not ValidarMultiplo(eQuantidade.AsDouble, m1, ((m2 = 0) and (m3 = 0)))
  then begin
    SetarFoco(eQuantidade);
    Exit;
  end;

  if eQuantidade.AsCurr = 0 then begin
    Exclamar('A quantidade n�o foi inforamada corretamente, verifique!');
    SetarFoco(eQuantidade);
    Exit;
  end;

  if not FrLote.LoteValido then begin
    Exclamar('O lote informado n�o � v�lido, verifique!');
    SetarFoco(FrLote);
    Exit;
  end;

  if eValorTotal.AsCurr = 0 then begin
    _Biblioteca.Exclamar('O produto n�o possui custo definido, por favor verique!');
    SetarFoco(FrProduto);
    Exit;
  end;

  AdicionarNoProdutoGrid(
    FrProduto.getProduto().produto_id,
    FrProduto.getProduto().nome,
    FrProduto.getProduto().nome_marca,
    FrProduto.getProduto().unidade_venda,
    ePrecoUnitario.AsDouble,
    (ePrecoUnitario.AsDouble * eQuantidade.AsDouble),
    eQuantidade.AsDouble,
    IIfDbl(cbNatureza.GetValor = 'E', stEstoqueFiscal.AsDouble + eQuantidade.AsDouble, stEstoqueFiscal.AsDouble - eQuantidade.AsDouble),
    FrLote.Lote,
    FrLote.DataFabricacao,
    FrLote.DataVencimento,
    stEstoqueFiscal.AsDouble,
    cbNatureza.Text,
    cbNatureza.GetValor,
    stEstoqueDisponivel.AsDouble,
    quantidadeDesejada
  );

  _Biblioteca.LimparCampos([FrProduto, eQuantidade, cbNatureza]);
  SetarFoco(FrProduto);
end;

procedure TFormAjusteEstoqueFiscal.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _AjustesEstoque.ExcluirAjustesEstoque(Sessao.getConexaoBanco, eID.AsInt);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormAjusteEstoqueFiscal.FormCreate(Sender: TObject);
begin
  inherited;
  FrProduto.OnAposDeletar := FrProdutoOnAposDeletar;
  FrProduto.OnAposPesquisar := FrProdutoOnAposPesquisar;

  FrMotivoAjusteEstoque.OnAposPesquisar := FrMotivoAjusteOnAposPesquisar;
  FrLote.Modo(False);

  eQuantidade.CasasDecimais := 4;
end;

procedure TFormAjusteEstoqueFiscal.FrMotivoAjusteOnAposPesquisar(Sender: TObject);
begin
  if FrMotivoAjusteEstoque.GetMotivoAjusteEstoque.PlanoFinanceiroId = '' then begin
    _Biblioteca.Exclamar('O plano financeiro para este motivo de ajuste n�o foi informado, verifique o cadastro de motivos de ajuste de estoque!');
    FrMotivoAjusteEstoque.Clear;
    SetarFoco(FrMotivoAjusteEstoque);
  end;
end;

procedure TFormAjusteEstoqueFiscal.FrProdutoOnAposDeletar(Sender: TObject);
begin
  _Biblioteca.LimparCampos([stEstoqueDisponivel, stEstoqueFiscal]);
  FrLote.SetTipoControleEstoque('N', 'N', 'N');
end;

procedure TFormAjusteEstoqueFiscal.FrProdutoOnAposPesquisar(Sender: TObject);
var
  vEstoques: TArray<RecEstoque>;
  vCustosProdutos: TArray<RecCustoProduto>;
begin
  _Biblioteca.LimparCampos([stEstoqueDisponivel, stEstoqueFiscal]);

  if Em(FrProduto.getProduto.TipoControleEstoque, ['K', 'A']) then begin
    _Biblioteca.Informar('Produtos que s�o cotrolados como "Kit" n�o pode ter seu estoque ajustado, ajuste os produtos que comp�e o kit.');
    SetarFoco(FrProduto);
    FrProduto.Clear;
    Exit;
  end;

  FrLote.SetTipoControleEstoque(FrProduto.getProduto.TipoControleEstoque, FrProduto.getProduto.ExigirDataFabricacaoLote, FrProduto.getProduto.ExigirDataVencimentoLote);

  vEstoques := _Estoques.BuscarEstoque(Sessao.getConexaoBanco, 0, [FrProduto.getProduto.produto_id, Sessao.getEmpresaLogada.EmpresaId]);
  if vEstoques <> nil then begin
    stEstoqueDisponivel.AsDouble := vEstoques[0].Disponivel;
    stEstoqueFiscal.AsDouble     := vEstoques[0].Estoque;
  end;

  vCustosProdutos := _CustosProdutos.BuscarCustoProdutos(Sessao.getConexaoBanco, 0, [FrProduto.getProduto.produto_id, Sessao.getEmpresaLogada.EmpresaId]);

  if vCustosProdutos <> nil then
    ePrecoUnitario.AsDouble := vCustosProdutos[0].CustoAjusteEstoque;
end;

procedure TFormAjusteEstoqueFiscal.GravarRegistro(Sender: TObject);
var
  i: Integer;
  vRetornoBanco: RecRetornoBD;
  vAjusteItens: TArray<RecAjusteEstoqueItens>;
begin
  inherited;

  SetLength(vAjusteItens, sgProdutos.RowCount -1);
  for i := 1 to sgProdutos.RowCount -1 do begin
    vAjusteItens[i - 1].ajuste_estoque_id := 0;
    vAjusteItens[i - 1].item_id           := i;
    vAjusteItens[i - 1].produto_id        := SFormatInt(sgProdutos.Cells[coProduto, i]);
    vAjusteItens[i - 1].quantidade        := SFormatDouble(sgProdutos.Cells[coQuantidade, i]);
    vAjusteItens[i - 1].quantidadeInformado := SFormatDouble(sgProdutos.Cells[coEstoqueInformado, i]);
    vAjusteItens[i - 1].Lote              := sgProdutos.Cells[coLote, i];
    vAjusteItens[i - 1].Natureza          := sgProdutos.Cells[coNaturezaSint, i];
    vAjusteItens[i - 1].PrecoUnitario     := SFormatDouble(sgProdutos.Cells[coPrecoUnitario, i]);
    vAjusteItens[i - 1].ValorTotal        := SFormatDouble(sgProdutos.Cells[coValorTotal, i]);
    vAjusteItens[i - 1].DataFabricacao    := ToData(sgProdutos.Cells[coDataFabricacao, i]);
    vAjusteItens[i - 1].DataVencimento    := ToData(sgProdutos.Cells[coDataVencimento, i]);
  end;

  vRetornoBanco :=
    _AjustesEstoque.AtualizarAjustesEstoque(
      Sessao.getConexaoBanco,
      eID.AsInt,
      Sessao.getEmpresaLogada.EmpresaId,
      FrMotivoAjusteEstoque.GetMotivoAjusteEstoque().motivo_ajuste_id,
      eObservacao.Text,
      FrMotivoAjusteEstoque.GetMotivoAjusteEstoque().PlanoFinanceiroId,
      'NOR',
      0,
      stValorTotalProdutosGrid.AsDouble,
      'FISCAL',
      vAjusteItens,
      False
    );

  if vRetornoBanco.TeveErro then begin
    Exclamar(vRetornoBanco.MensagemErro);
    Abort;
  end;

  if vRetornoBanco.AsInt > 0 then
    _Biblioteca.Informar('Novo ajuste de estoque efetuado com sucesso, c�digo: ' + NFormat(vRetornoBanco.AsInt));
end;

procedure TFormAjusteEstoqueFiscal.Modo(pEditando: Boolean);
begin
  inherited;
   _Biblioteca.Habilitar([
    //eDataCadastro,
    FrMotivoAjusteEstoque,
    eObservacao,
    FrProduto,
    eQuantidade,
    cbNatureza,
    sgProdutos,
    stQuantidadeItensGrid,
    stValorTotalProdutosGrid],
    pEditando
  );

  _Biblioteca.SomenteLeitura([
    //eDataCadastro,
    FrMotivoAjusteEstoque,
    eObservacao,
    FrProduto,
    eQuantidade,
    cbNatureza,
    sgProdutos,
    stQuantidadeItensGrid],
    False
  );

  cbNatureza.Enabled := (eID.AsInt = 0);

  if pEditando then begin
    SetarFoco(FrMotivoAjusteEstoque);
    sgProdutos.MostrarColunas([coNovoEstoqueFisico]);
  end;
end;

procedure TFormAjusteEstoqueFiscal.PesquisarRegistro;
begin
  inherited;

end;

procedure TFormAjusteEstoqueFiscal.PreencherRegistro(pAjuste: RecAjusteEstoque; pAjusteItens: TArray<RecAjusteEstoqueItens>);
var
  i: Integer;
begin
  eID.AsInt := pAjuste.ajuste_estoque_id;
  //eDataCadastro.AsData := pAjuste.data_hora_ajuste;
  FrMotivoAjusteEstoque.InserirDadoPorChave(pAjuste.motivo_ajuste_id);
  eObservacao.Text := pAjuste.observacao;
  sgProdutos.OcultarColunas([coNovoEstoqueFisico]);

  for i := Low(pAjusteItens) to High(pAjusteItens) do begin
    AdicionarNoProdutoGrid(
      pAjusteItens[i].produto_id,
      pAjusteItens[i].NomeProduto,
      pAjusteItens[i].NomeMarca,
      pAjusteItens[i].Unidade,
      pAjusteItens[i].PrecoUnitario,
      pAjusteItens[i].ValorTotal,
      pAjusteItens[i].Quantidade,
      0,
      pAjusteItens[i].Lote,
      0,
      0,
      0,
      pAjusteItens[i].NaturezaAnalitico,
      pAjusteItens[i].Natureza,
      0,
      10
    );

  end;

  sbGravar.Enabled := False;

  Informar('N�o � poss�vel editar este ajuste.');
  _Biblioteca.SomenteLeitura
  (
    [
      FrMotivoAjusteEstoque,
      FrProduto,
      FrLote,
      cbNatureza,
      eObservacao,
      FrProduto,
      eQuantidade
    ],
    True
  );

  FrMotivoAjusteEstoque.SetFocus;
end;

procedure TFormAjusteEstoqueFiscal.sbZerarEstoqueClick(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<RecProdutosSelecionados>;
  vProdutos: TArray<RecProdutosZerarEstoque>;
  i: Integer;
begin
  inherited;
  vRetorno := BuscarProdutosZerarEstoque.BuscarDadosProdutos;

  if vRetorno.BuscaCancelada then
    Exit;

  FrMotivoAjusteEstoque.InserirDadoPorChave(999);

  vProdutos := vRetorno.Dados.Itens;

  for i := Low(vProdutos) to High(vProdutos) do begin
    AdicionarNoProdutoGrid(
      vProdutos[i].ProdutoId,
      vProdutos[i].ProdutoNome,
      vProdutos[i].MarcaNome,
      vProdutos[i].Unidade,
      vProdutos[i].PrecoUnitario,
      (vProdutos[i].PrecoUnitario * IIfDbl(vProdutos[i].Quantidade < 0, vProdutos[i].Quantidade * -1, vProdutos[i].Quantidade)),
      IIfDbl(vProdutos[i].Quantidade < 0, vProdutos[i].Quantidade * -1, vProdutos[i].Quantidade),
      0, //novo estoque fisico
      vProdutos[i].Lote,
      vProdutos[i].DataFabricacao,
      vProdutos[i].DataVencimento,
      IIfDbl(vProdutos[i].Quantidade < 0, vProdutos[i].Quantidade * -1, vProdutos[i].Quantidade), //estoque fisico
      IIfStr(vProdutos[i].Quantidade > 0, 'Sa�da', 'Entrada'),
      IIf(vProdutos[i].Quantidade > 0, 'S', 'E'),
      IIfDbl(vProdutos[i].Quantidade < 0, vProdutos[i].Quantidade * -1, vProdutos[i].Quantidade), //estoque disponivel
      IIfDbl(vProdutos[i].Quantidade < 0, vProdutos[i].Quantidade * -1, vProdutos[i].Quantidade)  //estoque informado
    );
  end;
end;

procedure TFormAjusteEstoqueFiscal.sgProdutosClick(Sender: TObject);
begin
  inherited;
  if sgProdutos.Cells[coProduto, 1] = '' then
    Exit;
end;

procedure TFormAjusteEstoqueFiscal.sgProdutosDblClick(Sender: TObject);
begin
  inherited;
  if sgProdutos.Cells[coProduto, sgProdutos.Row] = '' then
    Exit;

  FrProduto.InserirDadoPorChave(SFormatInt(sgProdutos.Cells[coProduto, sgProdutos.Row]));

  if SFormatDouble(sgProdutos.Cells[coQuantidade, sgProdutos.Row]) > SFormatDouble(sgProdutos.Cells[coEstoqueInformado, sgProdutos.Row]) then
    eQuantidade.AsDouble := SFormatDouble(sgProdutos.Cells[coEstoqueInformado, sgProdutos.Row])
  else
    eQuantidade.AsDouble := SFormatDouble(sgProdutos.Cells[coQuantidade, sgProdutos.Row]);

  cbNatureza.SetIndicePorValor(sgProdutos.Cells[coNaturezaSint, sgProdutos.Row]);
  FrLote.Lote := sgProdutos.Cells[coLote, sgProdutos.Row];
  FrLote.DataFabricacao := ToData(sgProdutos.Cells[coDataFabricacao, sgProdutos.Row]);
  FrLote.DataVencimento := ToData(sgProdutos.Cells[coDataVencimento, sgProdutos.Row]);
  eValorTotal.AsDouble  := SFormatDouble(sgProdutos.Cells[coValorTotal, sgProdutos.Row]);
  ePrecoUnitario.AsDouble := SFormatDouble(sgProdutos.Cells[coPrecoUnitario, sgProdutos.Row]);

  FLinha := sgProdutos.Row;
  SetarFoco(FrProduto);
end;

procedure TFormAjusteEstoqueFiscal.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coProduto, coQuantidade, coEstoqueInformado, coPrecoUnitario, coNovoEstoqueFisico, coValorTotal] then
    vAlinhamento := taRightJustify
  else if ACol = coNatureza then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormAjusteEstoqueFiscal.sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if (ACol <> coNatureza) or (ARow = 0) then
    Exit;

  AFont.Style := [fsBold];

  if sgProdutos.Cells[coNaturezaSint, ARow] = 'E' then
    AFont.Color := clBlue
  else
    AFont.Color := clRed;
end;

procedure TFormAjusteEstoqueFiscal.sgProdutosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_DELETE) and (eID.AsInt = 0) then begin
    stValorTotalProdutosGrid.Somar(SFormatDouble(sgProdutos.Cells[coValorTotal, sgProdutos.Row]) * - 1);
    sgProdutos.DeleteRow(sgProdutos.Row);
    FLinha := 0;
    stQuantidadeItensGrid.Caption := NFormat(IIfDbl(sgProdutos.Cells[coProduto, sgProdutos.FixedRows] = '', 0, sgProdutos.RowCount - sgProdutos.FixedRows));
  end;
end;

procedure TFormAjusteEstoqueFiscal.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if FrMotivoAjusteEstoque.EstaVazio then begin
    Exclamar('N�o foi informado o motivo do ajuste do estoque, verifique!');
    FrMotivoAjusteEstoque.SetFocus;
    Abort;
  end;

  if SFormatInt(stQuantidadeItensGrid.Caption) = 0 then begin
    Exclamar('N�o foi informado nenhum produto para ajuste, verifique!');
    FrProduto.SetFocus;
    Abort;
  end;
end;

end.
