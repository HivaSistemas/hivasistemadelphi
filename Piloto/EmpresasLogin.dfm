inherited FormEmpresasLogin: TFormEmpresasLogin
  Caption = 'Empresas'
  ClientHeight = 166
  ClientWidth = 585
  Position = poScreenCenter
  Visible = False
  ExplicitWidth = 591
  ExplicitHeight = 195
  PixelsPerInch = 96
  TextHeight = 14
  object sgEmpresas: TGridLuka
    Left = 0
    Top = 0
    Width = 585
    Height = 169
    ColCount = 4
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
    TabOrder = 0
    OnDblClick = sgEmpresasDblClick
    OnDrawCell = sgEmpresasDrawCell
    OnKeyDown = sgEmpresasKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = clBlack
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'C'#243'digo'
      'Nome Fantasia'
      'Raz'#227'o social'
      'CNPJ')
    Grid3D = False
    RealColCount = 4
    AtivarPopUpSelecao = False
    ColWidths = (
      44
      212
      183
      134)
  end
end
