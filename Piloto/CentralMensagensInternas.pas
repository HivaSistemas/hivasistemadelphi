unit CentralMensagensInternas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatorios, Vcl.Grids, GridLuka,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Menus, EnviarMensagem,
  _RecordsCadastros, _Mensagens, _Sessao, _Biblioteca, System.Math, _RecordsEspeciais,
  CheckBoxLuka;

type
  TFormCentralMensagensInternas = class(TFormHerancaRelatorios)
    tvGrupoMensagens: TTreeView;
    pnMensagens: TPanel;
    sgMensagens: TGridLuka;
    eMensagem: TMemo;
    pmOpcoes: TPopupMenu;
    Panel1: TPanel;
    miEnviarNovaMensagem: TSpeedButton;
    miResponderMensagem: TSpeedButton;
    miEncaminharMensagem: TSpeedButton;
    procedure sgMensagensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure miEnviarNovaMensagemClick(Sender: TObject);
    procedure sgMensagensClick(Sender: TObject);
    procedure tvGrupoMensagensClick(Sender: TObject);
    procedure sgMensagensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure FormShow(Sender: TObject);
    procedure miEncaminharMensagemClick(Sender: TObject);
    procedure miResponderMensagemClick(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

const
  coMensagemId       = 0;
  coAssunto          = 1;
  coUsuarioEnvio     = 2;
  coDataHoraEnvio    = 3;
  coRespondida       = 4;
  coPermitirResposta = 5;
  coMensagem         = 6;
  coUsuarioEnvioId   = 7;
  coLida             = 8;

  cvEntradas    = 0;
  cvSistema     = 1;
  cvImportantes = 2;
  cvEnviadas    = 3;

procedure TFormCentralMensagensInternas.Carregar(Sender: TObject);
var
  i: Integer;
  vTipo: string;
  vMensagens: TArray<RecMensagem>;
begin
  inherited;
  sgMensagens.ClearGrid;
  eMensagem.Lines.Clear;

  case tvGrupoMensagens.Selected.Index of
    cvEntradas   : vTipo := 'ENV';
    cvSistema    : vTipo := 'SIS';
    cvImportantes: vTipo := 'IMP';
    cvEnviadas   : vTipo := 'ENV';
  end;

  if tvGrupoMensagens.Selected.Index = cvEnviadas then
    vMensagens := _Mensagens.BuscarMensagens(Sessao.getConexaoBanco, 2, [Sessao.getUsuarioLogado.funcionario_id, vTipo])
  else
    vMensagens := _Mensagens.BuscarMensagens(Sessao.getConexaoBanco, 1, [Sessao.getUsuarioLogado.funcionario_id, vTipo]);

  for i := Low(vMensagens) to High(vMensagens) do begin
    sgMensagens.Cells[coMensagemId, i + 1]       := NFormat(vMensagens[i].MensagemId);
    sgMensagens.Cells[coAssunto, i + 1]          := vMensagens[i].Assunto;
    sgMensagens.Cells[coUsuarioEnvio, i + 1]     := NFormat(vMensagens[i].UsuarioEnvioMensagemId) + ' - ' + vMensagens[i].NomeUsuarioEnvio;
    sgMensagens.Cells[coDataHoraEnvio, i + 1]    := DHFormat(vMensagens[i].DataHoraEnvio);
    sgMensagens.Cells[coRespondida, i + 1]       := SimNao(vMensagens[i].Respondida);
    sgMensagens.Cells[coPermitirResposta, i + 1] := vMensagens[i].PermitirResposta;
    sgMensagens.Cells[coMensagem, i + 1]         := vMensagens[i].Mensagem;
    sgMensagens.Cells[coUsuarioEnvioId, i + 1]   := NFormat(vMensagens[i].UsuarioEnvioMensagemId);
    sgMensagens.Cells[coLida, i + 1]             := vMensagens[i].Lida;
  end;
  sgMensagens.RowCount := IfThen(Length(vMensagens) > 1, High(vMensagens) + 2, 2);
  sgMensagensClick(Sender);
end;

procedure TFormCentralMensagensInternas.FormShow(Sender: TObject);
begin
  inherited;
  tvGrupoMensagens.Select(tvGrupoMensagens.Items[0]);
  tvGrupoMensagensClick(Sender);
end;

procedure TFormCentralMensagensInternas.miEncaminharMensagemClick(Sender: TObject);
begin
  inherited;
  if sgMensagens.Cells[coMensagemId, sgMensagens.Row] = '' then
    Exit;

  EnviarMensagem.Encaminhar(sgMensagens.Cells[coAssunto, sgMensagens.Row], sgMensagens.Cells[coMensagem, sgMensagens.Row]);
end;

procedure TFormCentralMensagensInternas.miEnviarNovaMensagemClick(Sender: TObject);
begin
  inherited;
  EnviarMensagem.Enviar;
end;

procedure TFormCentralMensagensInternas.miResponderMensagemClick(Sender: TObject);
begin
  inherited;
  EnviarMensagem.Responder(SFormatInt(sgMensagens.Cells[coMensagemId, sgMensagens.Row]), sgMensagens.Cells[coAssunto, sgMensagens.Row], SFormatInt(sgMensagens.Cells[coUsuarioEnvioId, sgMensagens.Row]));
end;

procedure TFormCentralMensagensInternas.sgMensagensClick(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  eMensagem.Lines.Clear;
  eMensagem.Lines.Text := sgMensagens.Cells[coMensagem, sgMensagens.Row];
  miResponderMensagem.Enabled := ( sgMensagens.Cells[coPermitirResposta, sgMensagens.Row] = 'S' ) and ( tvGrupoMensagens.Selected.Index <> cvEnviadas );

  if (sgMensagens.Cells[coLida, sgMensagens.Row] = 'N') and (tvGrupoMensagens.Selected.Index <> cvEnviadas) then begin
    vRetBanco := _Mensagens.ConfirmarLeitura(Sessao.getConexaoBanco, SFormatInt(sgMensagens.Cells[coMensagemId, sgMensagens.Row]));
    if vRetBanco.TeveErro then
      Exclamar(vRetBanco.MensagemErro)
    else
      sgMensagens.Cells[coLida, sgMensagens.Row] := 'S';
  end;
end;

procedure TFormCentralMensagensInternas.sgMensagensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = coMensagemId then
    vAlinhamento := taRightJustify
  else if ACol = coRespondida then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgMensagens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormCentralMensagensInternas.sgMensagensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgMensagens.Cells[coLida, ARow] = 'S' then
    AFont.Style := []
  else if tvGrupoMensagens.Selected.Index <> cvEnviadas then
    AFont.Style := [fsBold];

  if ACol = coRespondida then begin
    AFont.Style := [fsBold];
    AFont.Color := _Biblioteca.AzulVermelho(sgMensagens.Cells[coRespondida, ARow]);
  end;
end;

procedure TFormCentralMensagensInternas.tvGrupoMensagensClick(Sender: TObject);
begin
  inherited;
  Carregar(Sender);
end;

end.
