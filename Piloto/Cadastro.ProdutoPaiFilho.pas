unit Cadastro.ProdutoPaiFilho;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastro, _Biblioteca, _Produtos, _Sessao,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameProdutos, Vcl.StdCtrls,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _RecordsEspeciais;

type
  TFormCadastroProdutoPai = class(TFormHerancaCadastro)
    eID: TEditLuka;
    lb1: TLabel;
    FrProdutoFilho: TFrProdutos;
    FrProdutoPai: TFrProdutos;
    eQuantidadeVezesPai: TEditLuka;
    procedure FormCreate(Sender: TObject);
  private
    procedure FrProdutoFilhoOnAposPesquisar(Sender: TObject);
  protected
    procedure Modo(pEditando: Boolean); override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroProdutoPai }

procedure TFormCadastroProdutoPai.FormCreate(Sender: TObject);
begin
  inherited;
  FrProdutoFilho.OnAposPesquisar := FrProdutoFilhoOnAposPesquisar;
end;


procedure TFormCadastroProdutoPai.FrProdutoFilhoOnAposPesquisar(Sender: TObject);
var
  vRetorno: RecRetornoBD;
begin
  if FrProdutoFilho.getProduto.EProdutoFilho then begin
    Modo(True);
    FrProdutoPai.InserirDadoPorChave(FrProdutoFilho.getProduto().ProdutoPaiId);
    eQuantidadeVezesPai.AsDouble := FrProdutoFilho.getProduto().QuantidadeVezesPai;
  end
  else begin
    vRetorno := _Produtos.PodeVincularProdutoPaiFilho(Sessao.getConexaoBanco, FrProdutoFilho.getProduto().produto_id);
    if vRetorno.TeveErro then begin
      Exclamar(vRetorno.MensagemErro);
      Exit;
    end;

    Modo(True);
  end;
end;

procedure TFormCadastroProdutoPai.GravarRegistro(Sender: TObject);
var
  vRetorno: RecRetornoBD;
begin
  inherited;

  vRetorno :=
    _Produtos.AtualizarProdutoPaiFilho(
      Sessao.getConexaoBanco,
      FrProdutoFilho.getProduto().produto_id,
      FrProdutoPai.getProduto().produto_id,
      eQuantidadeVezesPai.AsDouble
    );

  if vRetorno.TeveErro then begin
    Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  _Biblioteca.Informar(coAlteracaoRegistroSucesso);
end;

procedure TFormCadastroProdutoPai.Modo(pEditando: Boolean);
begin
  inherited;

  _Biblioteca.Habilitar([
    FrProdutoPai,
    eQuantidadeVezesPai,
    sbGravar,
    sbDesfazer],
    pEditando
  );

  _Biblioteca.Habilitar([FrProdutoFilho], not pEditando, not pEditando);

  if not pEditando then
    SetarFoco(FrProdutoFilho)
  else
    SetarFoco(FrProdutoPai);
end;

procedure TFormCadastroProdutoPai.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
