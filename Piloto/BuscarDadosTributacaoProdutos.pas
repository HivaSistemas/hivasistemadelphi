unit BuscarDadosTributacaoProdutos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, FrameProdutosGruposTributacoesEmpresas, _Biblioteca,
  _ProdutosGruposTribEmpresas;

type
  TFormBuscarDadosTributacao = class(TFormHerancaFinalizar)
    FrProdutosGruposTributacoes: TFrProdutosGruposTributacoesEmpresas;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function BuscarDadosTributacao(caption: string = ''): TRetornoTelaFinalizar<TArray<RecProdutosGruposTribEmpresas>>;

implementation

{$R *.dfm}

function BuscarDadosTributacao(caption: string = ''): TRetornoTelaFinalizar<TArray<RecProdutosGruposTribEmpresas>>;
var
  vForm: TFormBuscarDadosTributacao;
begin
  vForm := TFormBuscarDadosTributacao.Create(Application);
  if (caption <> '') then
    vForm.Caption := caption;

  if Result.Ok(vForm.ShowModal) then
    Result.Dados := vForm.FrProdutosGruposTributacoes.ProdutosGruposTribEmpresas;

  FreeAndNil(vForm);
end;

end.
