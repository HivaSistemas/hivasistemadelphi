inherited FormCancelarRecebimentoAcumulado: TFormCancelarRecebimentoAcumulado
  Caption = 'Cancelar recebimento de acumulado'
  ClientHeight = 145
  ClientWidth = 482
  ExplicitWidth = 488
  ExplicitHeight = 174
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label1: TLabel
    Width = 61
    Caption = 'Acumulado'
    ExplicitWidth = 61
  end
  object lb12: TLabel [1]
    Left = 305
    Top = 53
    Width = 115
    Height = 14
    Caption = 'Usu'#225'rio recebimento'
  end
  object lb3: TLabel [2]
    Left = 355
    Top = 5
    Width = 128
    Height = 14
    Caption = 'Data\hora recebimento'
  end
  object lb5: TLabel [3]
    Left = 305
    Top = 97
    Width = 27
    Height = 14
    Caption = 'Total'
  end
  object lb18: TLabel [4]
    Left = 126
    Top = 97
    Width = 132
    Height = 14
    Caption = 'Condi'#231#227'o de pagamento'
  end
  object lb2: TLabel [5]
    Left = 126
    Top = 53
    Width = 39
    Height = 14
    Caption = 'Cliente'
  end
  object sbInformacoesAcumulado: TSpeedButtonLuka [6]
    Left = 206
    Top = 23
    Width = 18
    Height = 18
    Hint = 'Abrir informa'#231#245'es do acumulado'
    Flat = True
    NumGlyphs = 2
    OnClick = sbInformacoesAcumuladoClick
    TagImagem = 13
    PedirCertificacao = False
    PermitirAutOutroUsuario = False
  end
  inherited pnOpcoes: TPanel
    Height = 145
    ExplicitHeight = 145
    inherited sbDesfazer: TSpeedButton
      Top = 89
      ExplicitTop = 89
    end
    inherited sbExcluir: TSpeedButton
      Left = -136
      ExplicitLeft = -136
    end
    inherited sbPesquisar: TSpeedButton
      Top = 44
      ExplicitTop = 44
    end
    inherited sbLogs: TSpeedButton
      Left = -135
      ExplicitLeft = -135
    end
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 619
    Visible = False
    ExplicitLeft = 619
  end
  object eUsuarioRecebimento: TEditLuka
    Left = 305
    Top = 67
    Width = 174
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 3
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eDataHoraRecebimento: TEditLukaData
    Left = 355
    Top = 20
    Width = 124
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999 99:99:99;1;_'
    MaxLength = 19
    ReadOnly = True
    TabOrder = 4
    Text = '  /  /       :  :  '
    TipoData = tdDataHora
  end
  object eTotalAcumulado: TEditLuka
    Left = 303
    Top = 111
    Width = 175
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 5
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eCondicaoPagamento: TEditLuka
    Left = 126
    Top = 111
    Width = 171
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 6
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eCliente: TEditLuka
    Left = 126
    Top = 67
    Width = 171
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 7
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
