unit InformacoesGruposTributacoesFederalVenda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.StdCtrls, ComboBoxLuka, CheckBoxLuka, EditLuka, Vcl.Buttons, Vcl.ExtCtrls;

type
  TFormInformacoesGruposTributacoesFederalVenda = class(TFormHerancaFinalizar)
    Label1: TLabel;
    eID: TEditLuka;
    ckAtivo: TCheckBoxLuka;
    lb25: TLabel;
    lb3: TLabel;
    lb5: TLabel;
    lb1: TLabel;
    cbOrigemProduto: TComboBoxLuka;
    cbCstSaidaPIS: TComboBoxLuka;
    cbCstSaidaCOFINS: TComboBoxLuka;
    eDescricao: TEditLuka;
    procedure FormCreate(Sender: TObject);
  end;

procedure Informar(pGrupoTribFederalVendaId: Integer);

implementation

{$R *.dfm}

uses _GruposTribFederalVenda, _Biblioteca, _Sessao;

procedure Informar(pGrupoTribFederalVendaId: Integer);
var
  vGrupo: TArray<RecGruposTribFederalVenda>;
  vForm: TFormInformacoesGruposTributacoesFederalVenda;
begin
  if pGrupoTribFederalVendaId = 0 then
    Exit;

  vGrupo := _GruposTribFederalVenda.BuscarGruposTribFederalVenda(Sessao.getConexaoBanco, 0, [pGrupoTribFederalVendaId]);
  if vGrupo = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  vForm := TFormInformacoesGruposTributacoesFederalVenda.Create(nil);

  vForm.eID.SetInformacao(vGrupo[0].GrupoTribFederalVendaId);
  vForm.eDescricao.SetInformacao(vGrupo[0].Descricao);
  vForm.ckAtivo.CheckedStr        := vGrupo[0].Ativo;
  vForm.cbOrigemProduto.AsInt     := vGrupo[0].OrigemProduto;
  vForm.cbCstSaidaPIS.AsString    := vGrupo[0].CstPis;
  vForm.cbCstSaidaCOFINS.AsString := vGrupo[0].CstCofins;

  vForm.ShowModal;
  vForm.Free;
end;

procedure TFormInformacoesGruposTributacoesFederalVenda.FormCreate(Sender: TObject);
begin
  inherited;
  Sessao.SetComboBoxPisCofins(cbCstSaidaPIS);
  Sessao.SetComboBoxPisCofins(cbCstSaidaCOFINS);
end;

end.
