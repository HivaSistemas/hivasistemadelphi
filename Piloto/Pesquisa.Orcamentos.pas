unit Pesquisa.Orcamentos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPrincipal, Vcl.Grids, GridLuka, _RecordsEspeciais,
  FrameEmpresas, FrameVendedores, FrameDataInicialFinal, _FrameHerancaPrincipal, _Biblioteca,
  _FrameHenrancaPesquisas, FrameClientes, _Sessao, System.StrUtils, System.Math, _OrcamentosItens,
  _Orcamentos, _RecordsOrcamentosVendas, Vcl.StdCtrls, EditLuka, StaticTextLuka,
  FrameProdutos, Vcl.Buttons, SpeedButtonLuka, CheckBoxLuka, Vcl.ExtCtrls, System.Net.HttpClient,
  Vcl.ComCtrls, TFlatProgressBarUnit;

type
  TTipoPesquisa = (tpOrcamentos, tpVendasDevolucao, tpAgendarItensSemPrevisao, tpAcumuladosAberto, tpConsultaPedidos);

  TFormPesquisaOrcamentos = class(TFormHerancaPrincipal)
    sgOrcamentos: TGridLuka;
    st1: TStaticTextLuka;
    sgProdutos: TGridLuka;
    pnlTopo: TPanel;
    lbNomeFantasiaApelido: TLabel;
    FrCliente: TFrClientes;
    FrDataCadastro: TFrDataInicialFinal;
    FrVendedor: TFrVendedores;
    FrEmpresa: TFrEmpresas;
    eNomeCliente: TEditLuka;
    FrProduto: TFrProdutos;
    ckxPedidoWeb: TCheckBoxLuka;
    SpeedButton1: TSpeedButton;
    procedure FrDataCadastroeDataFinalKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgOrcamentosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgOrcamentosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgOrcamentosDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sgOrcamentosEnter(Sender: TObject);
    procedure sgOrcamentosClick(Sender: TObject);
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure SpeedButton1Click(Sender: TObject);
  private
    FOrcamentos: TArray<RecOrcamentos>;
    FItens: TArray<RecOrcamentoItens>;
    FTipoPesquisa: TTipoPesquisa;

    procedure BuscarRegistros;
  end;

function Pesquisar(pTipoPesquisa: TTipoPesquisa): TRetornoTelaFinalizar<RecOrcamentos>;

implementation

{$R *.dfm}

const
  coOrcamentoId  = 0;
  coDataCadastro = 1;
  coCliente      = 2;
  coVendedor     = 3;
  coValor        = 4;
  coEmpresa      = 5;

  (* Grid de produto *)
  cpProdutoId        = 0;
  cpNome             = 1;
  cpMarca            = 2;
  cpPrecoUnitario    = 3;
  cpQuantidade       = 4;
  cpUnidade          = 5;
  cpValorTotal       = 6;
  cpValorOutDesp     = 7;
  cpValorDesconto    = 8;

function Pesquisar(pTipoPesquisa: TTipoPesquisa): TRetornoTelaFinalizar<RecOrcamentos>;
var
  vForm: TFormPesquisaOrcamentos;
begin
  vForm := TFormPesquisaOrcamentos.Create(Application);

  vForm.FTipoPesquisa := pTipoPesquisa;

  if Result.Ok(vForm.ShowModal) then
    Result.Dados := vForm.FOrcamentos[vForm.sgOrcamentos.Row - 1];

  vForm.Free;
end;

procedure TFormPesquisaOrcamentos.BuscarRegistros;
var
  i: Integer;
  vComando: string;
  vOrcamentosIds: TArray<Integer>;
begin
  FOrcamentos := nil;
  vOrcamentosIds := nil;
  sgOrcamentos.ClearGrid;

  if FTipoPesquisa = tpConsultaPedidos then
    vComando := 'where 1 = 1 '
  else if FTipoPesquisa = tpOrcamentos then
    vComando := 'where ORC.STATUS in(''OE'', ''CA'') '
  else if FTipoPesquisa = tpAcumuladosAberto then begin
    vComando :=
      'where ORC.STATUS = ''RE'' ' +
      'and ORC.VALOR_TOTAL - ORC.VALOR_DEV_ACUMULADO_ABERTO > 0 ' +
      'and ORC.ACUMULADO_ID is null ' +
      'and COD.ACUMULATIVO = ''S'' ';
  end
  else if FTipoPesquisa = tpAgendarItensSemPrevisao then
    vComando := 'where ORC.STATUS in(''VE'', ''RE'') '
  else
    vComando := 'where ORC.STATUS in(''RE'') ';

  if not FrEmpresa.EstaVazio then
    vComando := vComando + ' and ' + FrEmpresa.getSqlFiltros('ORC.EMPRESA_ID');

  if not FrVendedor.EstaVazio then
    vComando := vComando + ' and ' + FrVendedor.getSqlFiltros('ORC.VENDEDOR_ID');

  if not FrCliente.EstaVazio then
    vComando := vComando + ' and ' + FrCliente.getSqlFiltros('ORC.CLIENTE_ID');

  if not FrDataCadastro.NenhumaDataValida then
    vComando := vComando + ' and ' + FrDataCadastro.getSqlFiltros('ORC.DATA_CADASTRO');

  if ckxPedidoWeb.Checked then
    vComando := vComando + ' and ORC.ORIGEM_VENDA = ''W'' ';

  if not FrProduto.EstaVazio then
    vComando := vComando + ' and ORC.ORCAMENTO_ID in(select distinct ORCAMENTO_ID from ORCAMENTOS_ITENS where ' + FrProduto.getSqlFiltros('PRODUTO_ID') + ')';

  if eNomeCliente.Trim <> '' then begin
    vComando := vComando +
      ' and ( ' +
      '  CAD.NOME_FANTASIA like ''%' + eNomeCliente.Text + '%'' or ' +
      '  CAD.RAZAO_SOCIAL like ''%' + eNomeCliente.Text + '%'' or ' +
      '  ORC.NOME_CONSUMIDOR_FINAL like ''%' + eNomeCliente.Text + '%'' ' +
      ' ) ';
  end;

  if FTipoPesquisa = tpAgendarItensSemPrevisao then
    vComando := vComando + ' and ORC.ORCAMENTO_ID in(select distinct ORCAMENTO_ID from ENTREGAS_ITENS_SEM_PREVISAO where SALDO > 0) ';

  vComando := vComando + ' order by ORC.ORCAMENTO_ID desc';

  FOrcamentos := _Orcamentos.BuscarOrcamentosComando(Sessao.getConexaoBanco, vComando);
  if FOrcamentos = nil then begin
    NenhumRegistro;
    SetarFoco(FrCliente);
    Exit;
  end;

  for i := Low(FOrcamentos) to High(FOrcamentos) do begin
    sgOrcamentos.Cells[coOrcamentoId, i + 1]  := NFormat(FOrcamentos[i].orcamento_id);
    sgOrcamentos.Cells[coDataCadastro, i + 1] := DFormat(FOrcamentos[i].data_cadastro);
    sgOrcamentos.Cells[coCliente, i + 1]      := NFormat(FOrcamentos[i].cliente_id) + ' - ' + FOrcamentos[i].nome_cliente;

    if FOrcamentos[i].nome_vendedor <> '' then
      sgOrcamentos.Cells[coVendedor, i + 1]     := NFormat(FOrcamentos[i].vendedor_id) + ' - ' + FOrcamentos[i].nome_vendedor;

    sgOrcamentos.Cells[coValor, i + 1]        := NFormat(FOrcamentos[i].valor_total);
    sgOrcamentos.Cells[coEmpresa, i + 1]      := NFormat(FOrcamentos[i].empresa_id) + ' - ' + FOrcamentos[i].nome_empresa;

    _Biblioteca.AddNoVetorSemRepetir(vOrcamentosIds, FOrcamentos[i].orcamento_id);
  end;
  sgOrcamentos.SetLinhasGridPorTamanhoVetor( Length(FOrcamentos) );

  FItens := _OrcamentosItens.BuscarOrcamentosItensComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('ORC.ORCAMENTO_ID', vOrcamentosIds) +  ' order by ITE.ITEM_ID asc');

  sgOrcamentosClick(nil);
  SetarFoco(sgOrcamentos);
end;

procedure TFormPesquisaOrcamentos.FormCreate(Sender: TObject);
begin
  inherited;
  FrEmpresa.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId, False);
end;

procedure TFormPesquisaOrcamentos.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrEmpresa);
  if FTipoPesquisa = tpVendasDevolucao then begin
    FrEmpresa.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId, False);
    FrEmpresa.Modo(False, False);
    SetarFoco(FrVendedor);
  end;
end;

procedure TFormPesquisaOrcamentos.FrDataCadastroeDataFinalKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  BuscarRegistros;
end;

procedure TFormPesquisaOrcamentos.sgOrcamentosClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
begin
  inherited;
  sgProdutos.ClearGrid;
  if sgOrcamentos.Cells[cpNome, 1] = '' then
    Exit;

  vLinha := 0;
  for i := Low(FItens) to High(FItens) do begin
    if SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]) <> FItens[i].orcamento_id then
      Continue;

    Inc(vLinha);

    sgProdutos.Cells[cpProdutoId, vLinha]         := NFormat(FItens[i].produto_id);
    sgProdutos.Cells[cpNome, vLinha]              := FItens[i].nome;
    sgProdutos.Cells[cpMarca, vLinha]             := FItens[i].nome_marca;
    sgProdutos.Cells[cpPrecoUnitario, vLinha]     := NFormat(FItens[i].preco_unitario);
    sgProdutos.Cells[cpQuantidade, vLinha]        := NFormatEstoque(FItens[i].Quantidade);
    sgProdutos.Cells[cpUnidade, vLinha]           := FItens[i].unidade_venda;
    sgProdutos.Cells[cpValorTotal, vLinha]        := NFormat(FItens[i].valor_total);
  end;
  sgProdutos.SetLinhasGridPorTamanhoVetor( vLinha );
end;

procedure TFormPesquisaOrcamentos.sgOrcamentosDblClick(Sender: TObject);
begin
  inherited;
  if sgOrcamentos.Cells[coOrcamentoId, 1] = '' then
    Exit;

  ModalResult := mrOk;
end;

procedure TFormPesquisaOrcamentos.sgOrcamentosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coOrcamentoId, coValor] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgOrcamentos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormPesquisaOrcamentos.sgOrcamentosEnter(Sender: TObject);
begin
  inherited;
  BuscarRegistros;
end;

procedure TFormPesquisaOrcamentos.sgOrcamentosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    sgOrcamentosDblClick(Sender);
end;

procedure TFormPesquisaOrcamentos.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[
    cpProdutoId,
    cpPrecoUnitario,
    cpQuantidade,
    cpValorTotal]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormPesquisaOrcamentos.SpeedButton1Click(Sender: TObject);
var
  HttpClient: THTTPClient;
  Response: IHTTPResponse;
  ST: TStream;
  vUrl: String;
begin
  vUrl := Sessao.getParametros.UrlIntegracaoWeb;

  if vUrl.IsEmpty then
  begin
    Exclamar('Url de ingra��o com a web inv�lida!');
    exit;
  end;

  HttpClient := THTTPClient.Create;

  if Copy(vUrl,vUrl.Length ,1) = '/' then
    vUrl := Copy(vUrl,1,vUrl.Length -1);

  vUrl := vUrl + ':' + Sessao.getParametros.PortaIntegracaoWeb.ToString;

  vUrl := vUrl + '/';

  try
    HttpClient.ContentType := 'application/json';
    ST := TStringStream.Create();
    try
      Response := HttpClient.Post(vUrl + '/getpedido',ST,nil);
      Informar('Pedido importado com sucesso!');

    except on E: Exception do
      Exclamar('API web indispon�vel!');
    end;
  finally
    HttpClient.Free;
    ST.Free;
  end;
end;

end.
