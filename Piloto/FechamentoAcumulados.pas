unit FechamentoAcumulados;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatorios, _Biblioteca, _Sessao, _Orcamentos,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameClientes, Vcl.Buttons, _RecordsOrcamentosVendas,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, Informacoes.Orcamento, _Imagens, Vcl.Menus, DefinirFechamentoAcumulados,
  Vcl.StdCtrls, StaticTextLuka, CheckBoxLuka;

type
  TFormFechamentoAcumulados = class(TFormHerancaRelatorios)
    FrCliente: TFrClientes;
    sgPedidos: TGridLuka;
    pmOpcoes: TPopupMenu;
    miN1: TMenuItem;
    stTotalPedidos: TStaticTextLuka;
    st4: TStaticText;
    stTotalJuros: TStaticTextLuka;
    st2: TStaticText;
    stValorReceber: TStaticTextLuka;
    st6: TStaticText;
    stTotalDevolucoes: TStaticTextLuka;
    st1: TStaticText;
    st3: TStaticText;
    miFecharacumuladosselecionados1: TSpeedButton;
    miMarcarDesmarcar: TSpeedButton;
    StaticText1: TStaticText;
    stValorAdiantado: TStaticTextLuka;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
    procedure sgPedidosDblClick(Sender: TObject);
    procedure sgPedidosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure sgPedidosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure sgPedidosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure miFecharacumuladosselecionados1Click(Sender: TObject);
    procedure miMarcarDesmarcarClick(Sender: TObject);
    procedure sgPedidosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    procedure FrClienteOnAposPesquisar(Sender: TObject);
    procedure FrClienteOnAposDeletar(Sender: TObject);

    procedure TotalizarSelecionados;
  protected
    procedure Modo(pEditando: Boolean);
  end;

implementation

{$R *.dfm}

const
  coSelecionado         = 0;
  coLegenda             = 1;
  coPedidoId            = 2;
  coDataHoraRecebimento = 3;
  coDataVencimento      = 4;
  coValorPedido         = 5;
  coValorAdiantado      = 6;
  coValorDevolvidos     = 7;
  coValorJuros          = 8;
  coTotalFechar         = 9;
  //Colunas ocultas
  coInscricaoEstadual   = 10;
  coTipoLinha           = 11;

  clCabecalho = 'C';
  clDetalhe   = 'D';

procedure TFormFechamentoAcumulados.FormCreate(Sender: TObject);
begin
  inherited;
  FrCliente.OnAposPesquisar := FrClienteOnAposPesquisar;
  FrCliente.OnAposDeletar   := FrClienteOnAposDeletar;
end;

procedure TFormFechamentoAcumulados.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrCliente);
end;

procedure TFormFechamentoAcumulados.FrClienteOnAposDeletar(Sender: TObject);
begin
  Modo(False);
end;

procedure TFormFechamentoAcumulados.FrClienteOnAposPesquisar(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vDataAtual: TDateTime;
  vPedidos: TArray<RecOrcamentos>;
  vInscricaoEstadualAnterior: string;
begin
  sgPedidos.ClearGrid;
  _Biblioteca.LimparCampos([stTotalPedidos, stTotalDevolucoes, stTotalJuros, stValorReceber, stValorAdiantado]);

  if FrCliente.getCliente.cadastro_id = Sessao.getParametros.cadastro_consumidor_final_id then begin
    _Biblioteca.NaoPermitidoConsumidorFinal;
    SetarFoco(FrCliente);
    FrCliente.Clear;
    Abort;
  end;

  vPedidos := _Orcamentos.BuscarOrcamentos(Sessao.getConexaoBanco, 6, [FrCliente.getCliente.cadastro_id]);
  if vPedidos = nil then begin
    _Biblioteca.NenhumRegistro;
    SetarFoco(FrCliente);
    Abort;
  end;

  Modo(True);

  vDataAtual := Sessao.getData;
  vLinha     := sgPedidos.FixedRows;

  for i := Low(vPedidos) to High(vPedidos) do begin
    if vInscricaoEstadualAnterior <> vPedidos[i].InscricaoEstadual then begin
      vInscricaoEstadualAnterior := vPedidos[i].InscricaoEstadual;
      sgPedidos.Cells[coDataHoraRecebimento, vLinha] := 'I.E.: ' + vPedidos[i].InscricaoEstadual;
      sgPedidos.Cells[coTipoLinha, vLinha]           := clCabecalho;
      Inc(vLinha);
    end;

    sgPedidos.Cells[coSelecionado, vLinha]         := charNaoSelecionado;
    sgPedidos.Cells[coPedidoId, vLinha]            := NFormat(vPedidos[i].orcamento_id);
    sgPedidos.Cells[coDataHoraRecebimento, vLinha] := DHFormat(vPedidos[i].data_hora_recebimento);
    sgPedidos.Cells[coDataVencimento, vLinha]      := DFormat(vPedidos[i].data_hora_recebimento + vPedidos[i].QtdeDiasVencimentoAcumulado);
    sgPedidos.Cells[coValorPedido, vLinha]         := NFormat(vPedidos[i].valor_total);
    sgPedidos.Cells[coValorAdiantado, vLinha]      := NFormat(vPedidos[i].ValorAdiantadoAcumulatoAber);
    sgPedidos.Cells[coValorDevolvidos, vLinha]     := NFormat(vPedidos[i].ValorDevAcumuladoAberto);
    sgPedidos.Cells[coInscricaoEstadual, vLinha]   := vPedidos[i].InscricaoEstadual;

    sgPedidos.Cells[coValorJuros, vLinha] :=
      NFormatN(Sessao.getCalculosSistema.CalcOrcamento.getJurosFechamentoAcumulado(
        vPedidos[i].valor_total - vPedidos[i].ValorDevAcumuladoAberto,
        Sessao.getParametrosEmpresa.PercentualJurosMensal,
        round(vDataAtual - (vPedidos[i].data_hora_recebimento + vPedidos[i].QtdeDiasVencimentoAcumulado))
      ));

    sgPedidos.Cells[coTotalFechar, vLinha] := NFormat(vPedidos[i].valor_total - vPedidos[i].ValorDevAcumuladoAberto + SFormatDouble(sgPedidos.Cells[coValorJuros, vLinha]));
    sgPedidos.Cells[coTipoLinha, vLinha]   := clDetalhe;
    Inc(vLinha);
  end;

  sgPedidos.RowCount := IIfInt(vLinha > sgPedidos.FixedRows, vLinha, sgPedidos.RowCount);
end;

procedure TFormFechamentoAcumulados.miFecharacumuladosselecionados1Click(Sender: TObject);
var
  i: Integer;
  vTotalLiquido: Double;
  vPedidosIds: TArray<Integer>;
  vRetTela: TRetornoTelaFinalizar;
  vInscricoesEstaduais: TArray<string>;
  vInscricaoEstadualFechamento: string;
begin
  inherited;
  vTotalLiquido := 0;
  vInscricaoEstadualFechamento := '';

  for i := 1 to sgPedidos.RowCount -1 do begin
    if sgPedidos.Cells[coSelecionado, i] <> charSelecionado then
      Continue;

    vTotalLiquido := vTotalLiquido + SFormatDouble(sgPedidos.Cells[coTotalFechar, i]);
    _Biblioteca.AddNoVetorSemRepetir(vPedidosIds, SFormatInt(sgPedidos.Cells[coPedidoId, i]));
    _Biblioteca.AddNoVetorSemRepetir(vInscricoesEstaduais, sgPedidos.Cells[coInscricaoEstadual, i]);
  end;

  if vPedidosIds = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  if Length(vInscricoesEstaduais) = 1 then
    vInscricaoEstadualFechamento := vInscricoesEstaduais[0];

  vRetTela := DefinirFechamentoAcumulados.Definir(
    FrCliente.getCliente.cadastro_id,
    vPedidosIds,
    vTotalLiquido,
    stTotalJuros.AsDouble,
    stValorAdiantado.AsDouble,
    vInscricaoEstadualFechamento
  );
  if vRetTela.BuscaCancelada then
    Exit;

  // Reprocessando a tela
  FrClienteOnAposPesquisar(Sender);
end;

procedure TFormFechamentoAcumulados.miMarcarDesmarcarClick(Sender: TObject);
var
  i: Integer;
  vMarcar: Boolean;
begin
  inherited;

  vMarcar := False;
  for i := 1 to sgPedidos.RowCount -1 do begin
    if sgPedidos.Cells[coTipoLinha, i] <> clDetalhe then
      Continue;

    vMarcar := sgPedidos.Cells[coSelecionado, i] = charNaoSelecionado;
    Break;
  end;

  for i := 1 to sgPedidos.RowCount -1 do begin
    if sgPedidos.Cells[coTipoLinha, i] <> clDetalhe then
      Continue;

    sgPedidos.Cells[coSelecionado, i] := IIfStr(vMarcar, charSelecionado, charNaoSelecionado);
  end;

  TotalizarSelecionados;
end;

procedure TFormFechamentoAcumulados.Modo(pEditando: Boolean);
begin
  _Biblioteca.Habilitar([sgPedidos], pEditando);

  if pEditando then
    SetarFoco(sgPedidos)
  else
    SetarFoco(FrCliente);
end;

procedure TFormFechamentoAcumulados.sgPedidosDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar( SFormatInt(sgPedidos.Cells[coPedidoId, sgPedidos.Row]) );
end;

procedure TFormFechamentoAcumulados.sgPedidosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[
    coPedidoId,
    coValorPedido,
    coValorAdiantado,
    coValorDevolvidos,
    coValorJuros,
    coTotalFechar]
  then
    vAlinhamento := taRightJustify
  else if ACol = coSelecionado then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgPedidos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormFechamentoAcumulados.sgPedidosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if (ACol = coDataHoraRecebimento) and (sgPedidos.Cells[coTipoLinha, ARow] = clCabecalho) then begin
    AFont.Color := coCorFonteEdicao1;
    AFont.Style := [fsBold];
  end;
end;

procedure TFormFechamentoAcumulados.sgPedidosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgPedidos.Cells[coPedidoId, ARow] = '' then
    Exit;

  if ACol = coSelecionado then begin
    if sgPedidos.Cells[ACol, ARow] = charNaoSelecionado then
      APicture := _Imagens.FormImagens.im1.Picture
    else if sgPedidos.Cells[ACol, ARow] = charSelecionado then
      APicture := _Imagens.FormImagens.im2.Picture;
  end;
end;

procedure TFormFechamentoAcumulados.sgPedidosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if sgPedidos.Cells[coPedidoId, sgPedidos.Row] = '' then
    Exit;

  if (Shift = [ssCtrl]) and (Key = VK_SPACE) then
    miMarcarDesmarcarClick(Sender)
  else if Key = VK_SPACE then begin
    sgPedidos.Cells[coSelecionado, sgPedidos.Row] := IIfStr(sgPedidos.Cells[coSelecionado, sgPedidos.Row] = charNaoSelecionado, charSelecionado, charNaoSelecionado);
    TotalizarSelecionados;
  end;
end;

procedure TFormFechamentoAcumulados.TotalizarSelecionados;
var
  i: Integer;
begin
  _Biblioteca.LimparCampos([stTotalPedidos, stTotalDevolucoes, stTotalJuros, stValorReceber, stValorAdiantado]);
  for i := 1 to sgPedidos.RowCount -1 do begin
    if sgPedidos.Cells[coSelecionado, i] <> charSelecionado then
      Continue;

    stTotalPedidos.Somar(SFormatDouble(sgPedidos.Cells[coValorPedido, i]));
    stValorAdiantado.Somar(SFormatDouble(sgPedidos.Cells[coValorAdiantado, i]));
    stTotalDevolucoes.Somar(SFormatDouble(sgPedidos.Cells[coValorDevolvidos, i]));
    stTotalJuros.Somar(SFormatDouble(sgPedidos.Cells[coValorJuros, i]));
  end;

  stValorReceber.AsDouble := stTotalPedidos.AsDouble + stTotalJuros.AsDouble - stTotalDevolucoes.AsDouble;
end;

end.
