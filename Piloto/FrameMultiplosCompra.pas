unit FrameMultiplosCompra;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.StdCtrls, _Sessao, DefinirMultiploCompra,
  StaticTextLuka, Vcl.ExtCtrls, _Biblioteca, _ProdutosMultiplosCompras, _Produtos, _RecordsCadastros,
  Vcl.Buttons;

type
  TFrMultiplosCompra = class(TFrameHerancaPrincipal)
    StaticTextLuka3: TStaticTextLuka;
    stMultiploCompra: TStaticText;
    StaticTextLuka1: TStaticTextLuka;
    stUnidadeCompra: TStaticText;
    sbAlterarMultiplo: TSpeedButton;
    StaticTextLuka2: TStaticTextLuka;
    stQuantidadeEmbalagem: TStaticText;
    procedure sbAlterarMultiploClick(Sender: TObject);
  private
    FProdutoId: Integer;
    FUnidadeVenda: string;
    FMultiplos: TArray<RecProdutoMultiploCompra>;

    function GetUnidadeId: string;
    function GetMultiplo: Double;
    function GetQuantidadeEmbalagem: Double;
  public
    procedure SetProdutoId(pProdutoId: Integer);
    procedure setUnidadeDefinida(pUnidadeId: string; pMultiplo: Double; pQtdeEmbalagem: Double);
    procedure Clear; override;
    procedure SomenteLeitura(pValue: Boolean); override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
    function EmEdicao: Boolean;

  published
    property UnidadeId: string read GetUnidadeId;
    property Multiplo: Double read GetMultiplo;
    property QuantidadeEmbalagem: Double read GetQuantidadeEmbalagem;
  end;

implementation

{$R *.dfm}

{ TFrMultiplosCompra }

procedure TFrMultiplosCompra.Clear;
begin
  inherited;
  _Biblioteca.LimparCampos([stMultiploCompra, stUnidadeCompra, stQuantidadeEmbalagem]);
end;

function TFrMultiplosCompra.EmEdicao: Boolean;
begin
  Result := sbAlterarMultiplo.Enabled;
end;

function TFrMultiplosCompra.GetMultiplo: Double;
begin
  Result := SFormatDouble(stMultiploCompra.Caption);
end;

function TFrMultiplosCompra.GetQuantidadeEmbalagem: Double;
begin
  Result := SFormatDouble(stQuantidadeEmbalagem.Caption);
end;

function TFrMultiplosCompra.GetUnidadeId: string;
begin
  Result := stUnidadeCompra.Caption;
end;

procedure TFrMultiplosCompra.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;
  if pLimpar then
    FProdutoId := 0;

  _Biblioteca.Habilitar([
    stMultiploCompra,
    stUnidadeCompra,
    stQuantidadeEmbalagem],
    pEditando,
    pLimpar
  );

  _Biblioteca.Habilitar([sbAlterarMultiplo], pEditando and (FProdutoId > 0), pLimpar);
end;

procedure TFrMultiplosCompra.sbAlterarMultiploClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar<RecProdutoMultiploCompra>;
begin
  vRetTela := DefinirMultiploCompra.Buscar(FMultiplos);
  if vRetTela.BuscaCancelada then
    Exit;

  stMultiploCompra.Caption := NFormat(vRetTela.Dados.Multiplo, 4);
  stQuantidadeEmbalagem.Caption := NFormat(vRetTela.Dados.QuantidadeEmbalagem, 4);
  stUnidadeCompra.Caption := vRetTela.Dados.UnidadeId;
end;

procedure TFrMultiplosCompra.SetProdutoId(pProdutoId: Integer);
begin
  FProdutoId := pProdutoId;
  FUnidadeVenda := _Produtos.BuscarUnidadeVenda(Sessao.getConexaoBanco, FProdutoId);
  FMultiplos := _ProdutosMultiplosCompras.BuscarProdutoMultiploCompras(Sessao.getConexaoBanco, 0, [FProdutoId]);

  stUnidadeCompra.Caption := FUnidadeVenda;
  stMultiploCompra.Caption := NFormatEstoque(1);
  stQuantidadeEmbalagem.Caption := NFormatEstoque(1);

  if FMultiplos = nil then
    _Biblioteca.Habilitar([sbAlterarMultiplo], False)
  else begin
    SetLength(FMultiplos, Length(FMultiplos) + 1);
    FMultiplos[High(FMultiplos)].ProdutoId := FProdutoId;
    FMultiplos[High(FMultiplos)].UnidadeId := FUnidadeVenda;
    FMultiplos[High(FMultiplos)].Multiplo  := FMultiplos[0].Multiplo;
    FMultiplos[High(FMultiplos)].QuantidadeEmbalagem := FMultiplos[0].QuantidadeEmbalagem;

    Modo(True, False);
  end;
  //stMultiploCompra.Caption := FloatToStr(FMultiplos[0].Multiplo);
  //stQuantidadeEmbalagem.Caption := FloatToStr(FMultiplos[0].QuantidadeEmbalagem);

end;

procedure TFrMultiplosCompra.setUnidadeDefinida(pUnidadeId: string; pMultiplo, pQtdeEmbalagem: Double);
var
  i: Integer;
  vAchou: Boolean;
begin
  vAchou := False;
  for i := Low(FMultiplos) to High(FMultiplos) do begin
    vAchou :=
      (pUnidadeId = FMultiplos[i].UnidadeId) and
      (NFormatNEstoque(pMultiplo) = NFormatNEstoque(FMultiplos[i].Multiplo)) and
      (NFormatNEstoque(pQtdeEmbalagem) = NFormatNEstoque(FMultiplos[i].QuantidadeEmbalagem));

    if vAchou then
      Break;
  end;

  if not vAchou then
    Exit;

  stMultiploCompra.Caption      := NFormat(pMultiplo, 4);
  stQuantidadeEmbalagem.Caption := NFormat(pQtdeEmbalagem, 4);
  stUnidadeCompra.Caption       := pUnidadeId;
end;

procedure TFrMultiplosCompra.SomenteLeitura(pValue: Boolean);
begin
  inherited;

end;
end.
