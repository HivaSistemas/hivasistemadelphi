inherited FormPesquisaIndicesDescontosVenda: TFormPesquisaIndicesDescontosVenda
  Caption = #205'ndice de dedu'#231#227'o'
  ClientHeight = 253
  ClientWidth = 505
  ExplicitWidth = 513
  ExplicitHeight = 284
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 505
    Height = 207
    Align = alNone
    ColCount = 7
    OnDrawCell = sgPesquisaDrawCell
    CorLinhaFoco = 38619
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Descri'#231#227'o'
      '% Desc.'
      'Pre'#231'o custo?'
      'Tipo desc.pre'#231'o custo'
      'Ativo?')
    OnGetCellColor = sgPesquisaGetCellColor
    Alignment = taCenter
    RealColCount = 7
    ExplicitWidth = 505
    ExplicitHeight = 207
    ColWidths = (
      28
      55
      98
      50
      76
      126
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Text = '0'
  end
  inherited pnFiltro1: TPanel
    Width = 505
    ExplicitWidth = 556
    inherited lblPesquisa: TLabel
      Width = 101
      Caption = #205'ndice de dedu'#231#227'o'
      ExplicitWidth = 101
    end
    inherited eValorPesquisa: TEditLuka
      Width = 92
      ExplicitWidth = 143
    end
  end
end
