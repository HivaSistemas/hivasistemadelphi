unit ConfirmarRetiradas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatorios, Vcl.Buttons, _RecordsExpedicao,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameClientes, _Retiradas,
  Vcl.Mask, EditLukaData, Vcl.StdCtrls, EditLuka, Vcl.Grids, GridLuka, Vcl.Menus, _RetiradasItens,
  _Sessao, _Biblioteca, System.Math, _RecordsEspeciais, Buscar.PessoaRetiradaMercadorias,
  Informacoes.Retiradas, FrameDataInicialFinal, StaticTextLuka, ImpressaoComprovanteEntregaGrafico,
  FrameLocais, _NotasFiscais, _RecordsNotasFiscais, _ComunicacaoNFe, BuscaDados, _ExibirMensagemMemo,
  ConferirSaidaProdutos, CheckBoxLuka;

type
  TFormConfirmarRetiradas = class(TFormHerancaRelatorios)
    pmRetiradas: TPopupMenu;
    pnCabecalho: TPanel;
    pn1: TPanel;
    lb2: TLabel;
    eOrcamentoId: TEditLuka;
    FrDataInicialFinal: TFrDataInicialFinal;
    FrLocal: TFrLocais;
    FrCliente: TFrClientes;
    sgItens: TGridLuka;
    StaticTextLuka2: TStaticTextLuka;
    sgRetiradas: TGridLuka;
    StaticTextLuka1: TStaticTextLuka;
    splSeparador: TSplitter;
    miN1: TMenuItem;
    miConfirmarRetirada: TSpeedButton;
    miCancelarRetirada: TSpeedButton;
    miImprimirReimprimirCompRetirada: TSpeedButton;
    procedure sgRetiradasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure eOrcamentoIdChange(Sender: TObject);
    procedure sgRetiradasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgRetiradasClick(Sender: TObject);
    procedure miConfirmarRetiradaClick(Sender: TObject);
    procedure sgRetiradasDblClick(Sender: TObject);
    procedure miCancelarRetiradaClick(Sender: TObject);
    procedure miImprimirReimprimirCompRetiradaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sgRetiradasKeyPress(Sender: TObject; var Key: Char);
  private
    FItens: TArray<RecRetiradaItem>;

    procedure FrClienteAposPesquisar(Sender: TObject);
    procedure FrClienteAposDeletar(Sender: TObject);
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormConfirmarRetiradas }

const
  coRetiradaId           = 0;
  coOrcamentoId          = 1;
  coCliente              = 2;
  coLocal                = 3;
  coDataCadastro         = 4;
  coVendedor             = 5;
  coTipoMovimento        = 6;
  coUsuarioGerouRetirada = 7;

  ciProdutoId  = 0;
  ciNome       = 1;
  ciMarca      = 2;
  ciQuantidade = 3;
  ciUnidade    = 4;
  ciLote       = 5;

procedure TFormConfirmarRetiradas.eOrcamentoIdChange(Sender: TObject);
begin
  inherited;
  _Biblioteca.Habilitar([FrCliente], eOrcamentoId.AsInt = 0, False);
end;

procedure TFormConfirmarRetiradas.FormCreate(Sender: TObject);
begin
  inherited;
  FrCliente.OnAposPesquisar := FrClienteAposPesquisar;
  FrCliente.OnAposDeletar := FrClienteAposDeletar;
end;

procedure TFormConfirmarRetiradas.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrCliente);
end;

procedure TFormConfirmarRetiradas.FrClienteAposDeletar(Sender: TObject);
begin
  _Biblioteca.Habilitar([eOrcamentoId], True);
end;

procedure TFormConfirmarRetiradas.FrClienteAposPesquisar(Sender: TObject);
begin
  _Biblioteca.Habilitar([eOrcamentoId], False);
end;

procedure TFormConfirmarRetiradas.miCancelarRetiradaClick(Sender: TObject);
var
  vRetiradaId: Integer;
  vRetBanco: RecRetornoBD;

  vNota: TArray<RecNotaFiscal>;

  oNFe: TComunicacaoNFe;
  vJustificativa: string;
begin
  inherited;
  if sgRetiradas.Cells[coTipoMovimento, sgRetiradas.Row] = 'Ato' then begin
    _Biblioteca.Exclamar('N�o � permitido o cancelamento de retiradas no ato!');
    Exit;
  end;

  if not Perguntar('Deseja realmente cancelar a retirada selecionada?') then
    Exit;

  vRetiradaId := SFormatInt(sgRetiradas.Cells[coRetiradaId, sgRetiradas.Row]);
  if vRetiradaId = 0 then begin
    Exclamar('Nenhuma retirada selecionada!');
    Exit;
  end;

  vNota := _NotasFiscais.BuscarNotasFiscais(Sessao.getConexaoBanco, 5, [vRetiradaId]);
  if vNota <> nil  then begin
    if (Em(vNota[0].tipo_nota, ['C', 'N'])) and (vNota[0].status = 'E') then begin
      if not _Biblioteca.Perguntar('Existe uma ' + vNota[0].TipoNotaAnalitico + ' emitida para esta retirada, a mesma ser� cancelada se poss�vel, deseja realmente continuar?' ) then
        Abort;

      vRetBanco := _NotasFiscais.PodeCancelarNFe(Sessao.getConexaoBanco, vNota[0].nota_fiscal_id);
      if vRetBanco.TeveErro then begin
        _Biblioteca.Exclamar(vRetBanco.MensagemErro);
        Abort;
      end;

      vJustificativa := BuscaDados.BuscarDados('Justificativa', '');
      if vJustificativa = '' then begin
        Exclamar('� necess�rio informar a justificativa para cancelamento da NF-e!');
        Abort;
      end;

      if Length(Trim(vJustificativa)) < 15 then begin
        Exclamar('A justificativa para cancelamento da NF-e deve ter no m�nimo 15 caracteres!');
        Abort;
      end;

      oNFe := TComunicacaoNFe.Create(Self);
      if not oNFe.CancelarNFe(vNota[0].nota_fiscal_id, vJustificativa) then begin
        oNFe.Free;
        Abort;
      end;

      oNFe.Free;
    end;
  end;

  vRetBanco := _Retiradas.CancelarRetirada(Sessao.getConexaoBanco, vRetiradaId);
  if vRetBanco.TeveErro then begin
    Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  _Biblioteca.Informar('Retirada cancelada com sucesso.');
  Carregar(Sender);
end;

procedure TFormConfirmarRetiradas.miConfirmarRetiradaClick(Sender: TObject);
var
  vPessoaRetirada: TRetornoTelaFinalizar<TInfoRetirada>;

  vRetiradaId: Integer;
  vRetBanco: RecRetornoBD;
  vRetConferencia: TRetornoTelaFinalizar;
begin
  inherited;
  vRetiradaId := SFormatInt(sgRetiradas.Cells[coRetiradaId, sgRetiradas.Row]);
  if vRetiradaId = 0 then begin
    Exclamar('Nenhuma retirada selecionada!');
    Exit;
  end;

  if
    ((sgRetiradas.Cells[coTipoMovimento, sgRetiradas.Row] = 'Ato') and (Sessao.getParametrosEmpresa.TrabalharConferenciaRetAto = 'S')) or
    ((sgRetiradas.Cells[coTipoMovimento, sgRetiradas.Row] = 'Retirar') and (Sessao.getParametrosEmpresa.TrabalharConferenciaRetirar = 'S'))
  then begin
    vRetConferencia := ConferirSaidaProdutos.Conferir( ConferirSaidaProdutos.tpRetirada, vRetiradaId );
    if vRetConferencia.BuscaCancelada then
      Exit;
  end;

  vPessoaRetirada := Buscar.PessoaRetiradaMercadorias.Buscar;
  if vPessoaRetirada.RetTela = trCancelado then
    Exit;

  vRetBanco :=
    _Retiradas.ConfirmarRetirada(
      Sessao.getConexaoBanco,
      vRetiradaId,
      vPessoaRetirada.Dados.NomePessoa,
      vPessoaRetirada.Dados.CPFPessoa,
      vPessoaRetirada.Dados.DataHoraRetirada,
      vPessoaRetirada.Dados.Observacoes
    );

  if vRetBanco.TeveErro then begin
    Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  _Biblioteca.Informar('Confirma��o realizada com sucesso.');
  Carregar(Sender);
end;

procedure TFormConfirmarRetiradas.miImprimirReimprimirCompRetiradaClick(Sender: TObject);
begin
  inherited;
  ImpressaoComprovanteEntregaGrafico.Imprimir( SFormatInt(sgRetiradas.Cells[coRetiradaId, sgRetiradas.Row]), ImpressaoComprovanteEntregaGrafico.tpRetirada );
end;

procedure TFormConfirmarRetiradas.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[ciProdutoId, ciQuantidade] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormConfirmarRetiradas.sgRetiradasClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vRetiradaId: Integer;
begin
  inherited;
  vLinha := 0;
  sgItens.ClearGrid;
  vRetiradaId := SFormatInt(sgRetiradas.Cells[coRetiradaId, sgRetiradas.Row]);
  for i := Low(FItens) to High(FItens) do begin
    if vRetiradaId <> FItens[i].RetiradaId then
      Continue;

    Inc(vLinha);

    sgItens.Cells[ciProdutoId, vLinha]  := NFormat(FItens[i].ProdutoId);
    sgItens.Cells[ciNome, vLinha]       := FItens[i].NomeProduto;
    sgItens.Cells[ciMarca, vLinha]      := FItens[i].NomeMarca;
    sgItens.Cells[ciQuantidade, vLinha] := NFormatEstoque(FItens[i].Quantidade);
    sgItens.Cells[ciUnidade, vLinha]    := FItens[i].Unidade;
    sgItens.Cells[ciLote, vLinha]       := FItens[i].Lote;
  end;
  sgItens.RowCount := IfThen(vLinha > 1, vLinha + 1, 2);
end;

procedure TFormConfirmarRetiradas.sgRetiradasDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Retiradas.Informar(SFormatInt(sgRetiradas.Cells[coRetiradaId, sgRetiradas.Row]));
end;

procedure TFormConfirmarRetiradas.sgRetiradasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coRetiradaId, coOrcamentoId] then
    vAlinhamento := taRightJustify
  else if ACol = coTipoMovimento then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgRetiradas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormConfirmarRetiradas.sgRetiradasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coTipoMovimento then begin
    AFont.Style := [fsBold];
    AFont.Color := IfThen(sgRetiradas.Cells[ACol, ARow] = 'Ato', clBlue, $000096DB);
  end;
end;

procedure TFormConfirmarRetiradas.sgRetiradasKeyPress(Sender: TObject; var Key: Char);
var
  vLinhaEntrega: Integer;
  vValorDigitado: string;
begin
  inherited;
  if CharInSet(Key, ['0'..'9', 'A'..'Z', 'a'..'z' , '.', '-' , '?']) then begin
    vValorDigitado := BuscaDados.BuscarDados(tiTexto, 'Busca digitada', Key);
    if vValorDigitado <> '' then begin
      vLinhaEntrega := sgRetiradas.Localizar([coRetiradaId], [ NFormat(SFormatInt(vValorDigitado)) ]);
      if vLinhaEntrega = -1 then
        _ExibirMensagemMemo.Mensagem('Aten��o', 'Retirada n�o encontrada!', [], True)
      else begin
        sgRetiradas.Row := vLinhaEntrega;
        miConfirmarRetiradaClick(Sender);
      end;
    end
    else
      SetFocus;
  end;
end;

procedure TFormConfirmarRetiradas.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

procedure TFormConfirmarRetiradas.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vRetiradasIds: TArray<Integer>;
  vRetiradas: TArray<RecRetirada>;
begin
  inherited;
  sgRetiradas.ClearGrid;
  vRetiradasIds := nil;
  sgItens.ClearGrid;

  vSql := ' where RET.CONFIRMADA = ''N'' ';
  vSql := vSql + ' and ORC.STATUS in(''RE'', ''VE'') ';
  vSql := vSql + ' and RET.EMPRESA_ID = ' + IntToStr(Sessao.getEmpresaLogada.EmpresaId) + ' ';

  if eOrcamentoId.AsInt > 0 then
    vSql := vSql + ' and RET.ORCAMENTO_ID = ' + IntToStr(eOrcamentoId.AsInt);

  if not FrCliente.EstaVazio then
    vSql := vSql + ' and ' + FrCliente.getSqlFiltros('ORC.CLIENTE_ID');

  if not FrDataInicialFinal.NenhumaDataValida then
    vSql := vSql + ' and ' + FrDataInicialFinal.getSqlFiltros('trunc(RET.DATA_HORA_CADASTRO)');

  if not FrLocal.EstaVazio then
    vSql := vSql + ' and ' + FrLocal.getSqlFiltros('RET.LOCAL_ID');

  vSql := vSql + ' order by RET.RETIRADA_ID desc';

  vRetiradas := _Retiradas.BuscarRetiradasComando(Sessao.getConexaoBanco, vSql);
  if vRetiradas = nil then begin
    NenhumRegistro;
    Exit;
  end;

  for i := Low(vRetiradas) to High(vRetiradas) do begin
    sgRetiradas.Cells[coRetiradaId, i + 1]           := NFormat(vRetiradas[i].RetiradaId);
    sgRetiradas.Cells[coOrcamentoId, i + 1]          := NFormat(vRetiradas[i].OrcamentoId);
    sgRetiradas.Cells[coCliente, i + 1]              := NFormat(vRetiradas[i].ClienteId) + ' - ' + vRetiradas[i].NomeCliente;
    sgRetiradas.Cells[coLocal, i + 1]                := NFormat(vRetiradas[i].LocalId) + ' - ' + vRetiradas[i].NomeLocal;
    sgRetiradas.Cells[coDataCadastro, i + 1]         := DFormat(vRetiradas[i].DataHoraCadastro);
    sgRetiradas.Cells[coVendedor, i + 1]             := NFormat(vRetiradas[i].VendedorId) + ' - ' + vRetiradas[i].NomeVendedor;
    sgRetiradas.Cells[coTipoMovimento, i + 1]        := vRetiradas[i].TipoMovimentoAnalitico;
    sgRetiradas.Cells[coUsuarioGerouRetirada, i + 1] := NFormatN(vRetiradas[i].UsuarioCadastroId) + ' - ' + vRetiradas[i].NomeUsuarioCadastro;

    _Biblioteca.AddNoVetorSemRepetir(vRetiradasIds, vRetiradas[i].RetiradaId);
  end;
  sgRetiradas.SetLinhasGridPorTamanhoVetor( Length(vRetiradas) );

  FItens := _RetiradasItens.BuscarRetiradaItensComando(Sessao.getConexaoBanco, ' and ' + _Biblioteca.FiltroInInt('ITE.RETIRADA_ID', vRetiradasIds) + ' order by PRO.NOME ');
  sgRetiradasClick(Sender);
  SetarFoco(sgRetiradas);
end;

end.
