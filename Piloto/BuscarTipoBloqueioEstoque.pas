unit BuscarTipoBloqueioEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, _FrameHenrancaPesquisas,
  _RecordsCadastros, _Biblioteca, Vcl.StdCtrls, Frame.TipoBloqueioEstoque;

type
  TFormBuscarTipoBloqueioEstoque = class(TFormHerancaFinalizar)
    FrTipoBloqueioEstoque: TFrTipoBloqueioEstoque;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function BuscarTipoBloqueio(tipoAcompanhamentoId: Integer): TRetornoTelaFinalizar<RecTipoBloqueioEstoque>;

implementation

{$R *.dfm}

function BuscarTipoBloqueio(tipoAcompanhamentoId: Integer): TRetornoTelaFinalizar<RecTipoBloqueioEstoque>;
var
  vForm: TFormBuscarTipoBloqueioEstoque;
begin
  vForm := TFormBuscarTipoBloqueioEstoque.Create(Application);

  vForm.FrTipoBloqueioEstoque.InserirDadoPorChave(tipoAcompanhamentoId);

  if Result.Ok(vForm.ShowModal) then
    Result.Dados := vForm.FrTipoBloqueioEstoque.GetTipoBloqueioEstoque(0);

  FreeAndNil(vForm);
end;

end.
