unit CadastroEmpresas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _RecordsCadastros,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, EditTelefoneLuka, EditLukaData, _Empresas, _Sessao,
  System.Math, _FrameHenrancaPesquisas, FrameClientes, _FrameHerancaPrincipal, FrameEndereco,
  Vcl.Mask, EditCpfCnpjLuka, _RecordsEspeciais, _Biblioteca, PesquisaEmpresas,
  FrameFornecedores, ComboBoxLuka, _HerancaCadastro, Frame.Cadastros,
  RadioGroupLuka, CheckBoxLuka;

type
  TFormCadastroEmpresas = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eRazaoSocial: TEditLuka;
    eNomeFantasia: TEditLuka;
    lb2: TLabel;
    lbCPF_CNPJ: TLabel;
    eCNPJ: TEditCPF_CNPJ_Luka;
    FrEndereco: TFrEndereco;
    lb9: TLabel;
    eEmail: TEditLuka;
    lb6: TLabel;
    eInscricaoEstadual: TEditLuka;
    lb3: TLabel;
    eInscricaoMunicipal: TEditLuka;
    lb7: TLabel;
    eCNAE: TMaskEdit;
    lb17: TLabel;
    eDataFundacao: TEditLukaData;
    eTelefone: TEditTelefoneLuka;
    lb11: TLabel;
    eFax: TEditTelefoneLuka;
    lb13: TLabel;
    FrCadastro: TFrameCadastros;
    rgTipoEmpresa: TRadioGroupLuka;
    eNomeResumidoImpressoesNaoFiscais: TEditLuka;
    lb4: TLabel;
  private
    procedure PreencherRegistro(pEmpresa: RecEmpresas);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroEmpresas }

procedure TFormCadastroEmpresas.BuscarRegistro;
var
  empresas: TArray<RecEmpresas>;
begin
  empresas := _Empresas.BuscarEmpresas(Sessao.getConexaoBanco, 0, [eId.AsInt]);

  if empresas = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    eID.SetFocus;
    Exit;
  end;

  inherited;
  PreencherRegistro(empresas[0]);
end;

procedure TFormCadastroEmpresas.ExcluirRegistro;
var
  retorno: RecRetornoBD;
begin
  retorno := _Empresas.ExcluirEmpresa(Sessao.getConexaoBanco, eID.AsInt);

  if retorno.TeveErro then begin
    _Biblioteca.Exclamar(retorno.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroEmpresas.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  vCadastroId: Integer;
begin
  inherited;

  vCadastroId := 0;
  if not FrCadastro.EstaVazio then
    vCadastroId := FrCadastro.getCadastro.cadastro_id;

  vRetBanco :=
    _Empresas.AtualizarEmpresa(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eNomeFantasia.Text,
      eRazaoSocial.Text,
      eNomeResumidoImpressoesNaoFiscais.Text,
      rgTipoEmpresa.GetValor,
      vCadastroId,
      FrEndereco.getBairroId,
      FrEndereco.getLogradouro,
      FrEndereco.getComplemento,
      FrEndereco.getNumero,
      FrEndereco.getPontoReferencia,
      FrEndereco.getCep,
      eTelefone.Text,
      eFax.Text,
      eEmail.Text,
      eDataFundacao.AsData,
      eInscricaoEstadual.Text,
      eInscricaoMunicipal.Text,
      eCNAE.Text,
      eCNPJ.Text,
      _Biblioteca.ToChar(ckAtivo)
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormCadastroEmpresas.Modo(pEditando: Boolean);
begin
  inherited;

  if pEditando and (eID.AsInt = 0) and (Sessao.getUsuarioLogado.funcionario_id <> 1) then begin
    _Biblioteca.Exclamar('Somente o usu�rio Hiva tem acesso ao cadastrar uma nova empresa!');
    Modo(False);
    Abort;
  end;

  _Biblioteca.Habilitar([
    rgTipoEmpresa,
    eNomeFantasia,
    eRazaoSocial,
    eNomeResumidoImpressoesNaoFiscais,
    eCNPJ,
    eInscricaoEstadual,
    eInscricaoMunicipal,
    eCNAE,
    eDataFundacao,
    eTelefone,
    eFax,
    FrEndereco,
    eEmail,
    FrCadastro,
    ckAtivo],
    pEditando
  );

  if pEditando then begin
    SetarFoco(rgTipoEmpresa);
    ckAtivo.Checked := True;
  end;
end;

procedure TFormCadastroEmpresas.PesquisarRegistro;
var
  vEmpresas: RecEmpresas;
begin
  vEmpresas := RecEmpresas(PesquisaEmpresas.PesquisarEmpresa);
  if vEmpresas = nil then
    Exit;

  inherited;
  PreencherRegistro(vEmpresas);
end;

procedure TFormCadastroEmpresas.PreencherRegistro(pEmpresa: RecEmpresas);
begin
  eID.AsInt                := pEmpresa.EmpresaId;
  rgTipoEmpresa.SetIndicePorValor(pEmpresa.TipoEmpresa);
  eRazaoSocial.Text                      := pEmpresa.RazaoSocial;
  eNomeFantasia.Text                     := pEmpresa.NomeFantasia;
  eNomeResumidoImpressoesNaoFiscais.Text := pEmpresa.NomeFantasia;
  eCNPJ.Text                             := pEmpresa.Cnpj;
  eInscricaoEstadual.Text                := pEmpresa.InscricaoEstadual;
  eInscricaoMunicipal.Text               := pEmpresa.InscricaoMunicipal;
  eCNAE.Text                             := pEmpresa.Cnae;
  eDataFundacao.AsData                   := pEmpresa.DataFundacao;
  eTelefone.Text                         := pEmpresa.TelefonePrincipal;
  eFax.Text                              := pEmpresa.TelefoneFax;
  FrEndereco.setLogradouro(pEmpresa.logradouro);
  FrEndereco.setComplemento(pEmpresa.complemento);
  FrEndereco.setNumero(pEmpresa.numero);
  FrEndereco.setBairroId(pEmpresa.BairroId);
  FrEndereco.setPontoReferencia(pEmpresa.PontoReferencia);
  FrEndereco.setCep(pEmpresa.cep);
  eEmail.Text := pEmpresa.EMail;
  FrCadastro.InserirDadoPorChave(pEmpresa.CadastroId, False);
  ckAtivo.Checked := (pEmpresa.ativo = 'S');

  pEmpresa.Free;
end;

procedure TFormCadastroEmpresas.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eRazaoSocial.Trim = '' then begin
    Exclamar('A raz�o social n�o foi informada, verifique!');
    SetarFoco(eRazaoSocial);
    Abort;
  end;

  if eNomeFantasia.Trim = '' then begin
    Exclamar('O nome fantasia n�o foi informado, verifique!');
    SetarFoco(eNomeFantasia);
    Abort;
  end;

  if not eCNPJ.getCPF_CNPJOk then begin
    Exclamar('O CNPJ n�o est� correto, verifique!');
    SetarFoco(eCNPJ);
    Abort;
  end;

  if FrCadastro.EstaVazio then begin
    Exclamar('O c�digo da empresa nos cadastros n�o foi informado corretamente, verifique!');
    SetarFoco(FrCadastro);
    Abort;
  end;

  if eCNPJ.Text <> FrCadastro.getCadastro.cpf_cnpj then begin
    Exclamar('O CNPJ informado para esta empresa � diferente do CNPJ do cadastro vinculado, verifique!');
    SetarFoco(FrCadastro);
    Abort;
  end;

  if rgTipoEmpresa.ItemIndex = -1 then begin
    Exclamar('O tipo de empresa n�o foi informado corretamente, verifique!');
    SetarFoco(rgTipoEmpresa);
    Abort;
  end;

  FrEndereco.VerificarDados;
end;

end.
