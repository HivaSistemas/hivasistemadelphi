inherited FormEnderecoEstoqueNivel: TFormEnderecoEstoqueNivel
  Caption = 'Cadastro Endere'#231'o de Estoque - N'#237'vel'
  ClientHeight = 206
  ClientWidth = 451
  ExplicitWidth = 457
  ExplicitHeight = 235
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [1]
    Left = 126
    Top = 58
    Width = 53
    Height = 14
    Caption = 'Descri'#231#227'o'
  end
  inherited pnOpcoes: TPanel
    Height = 206
    ExplicitHeight = 206
  end
  object eDescricao: TEditLuka
    Left = 126
    Top = 73
    Width = 317
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 3
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
