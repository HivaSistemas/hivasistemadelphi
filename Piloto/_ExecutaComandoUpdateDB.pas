unit _ExecutaComandoUpdateDB;

interface

uses
  UteisUpdateDB, _Biblioteca, _ComandosUpdateDB;

procedure ExecutarScripts;

implementation

procedure ExecutarScripts;
var
  i: Integer;
begin
  UteisUpdateDB.ExecutarScript(1, _ComandosUpdateDB.AdicionarColunaVALOR_ADIANTADO);
  UteisUpdateDB.ExecutarScript(2, _ComandosUpdateDB.AdicionarColunaPAGAR_ADIANTADO_ID);
  UteisUpdateDB.ExecutarScript(3, _ComandosUpdateDB.AjustarConstraintCK_CONTAS_PAGAR_BAIXAS_TIPO);
  UteisUpdateDB.ExecutarScript(4, _ComandosUpdateDB.AjustarConstraintCK_CONTAS_PAG_BX_TIPO_CAD_ID);
  UteisUpdateDB.ExecutarScript(5, _ComandosUpdateDB.AjustarProcedureLANCAR_ADIANT_CONTAS_PAGAR);
  UteisUpdateDB.ExecutarScript(6, _ComandosUpdateDB.AjustarProcedureFECHAR_PEDIDO);
  UteisUpdateDB.ExecutarScript(7, _ComandosUpdateDB.AjustarProcedureFECHAR_ACUMULADO);
  UteisUpdateDB.ExecutarScript(8, _ComandosUpdateDB.AjustarProcedureCONSOLIDAR_BAIXA_CONTAS_REC);
  UteisUpdateDB.ExecutarScript(9, _ComandosUpdateDB.AjustarProcedureRECEBER_PEDIDO);
  UteisUpdateDB.ExecutarScript(10, _ComandosUpdateDB.AjustarProcedureBAIXAR_CREDITO_PAGAR);
  UteisUpdateDB.ExecutarScript(11, _ComandosUpdateDB.AdicionarColunaCONTAS_RECEBER);
  UteisUpdateDB.ExecutarScript(12, _ComandosUpdateDB.AjustarConstraintCK_CONTAS_REC_COMISSIONAR);
  UteisUpdateDB.ExecutarScript(13, _ComandosUpdateDB.AjustarViewVW_TIPOS_COB_PEDIDOS_COM_BAIXA);
  UteisUpdateDB.ExecutarScript(14, _ComandosUpdateDB.AjustarViewVW_COMISSAO_POR_FUNCIONARIOS);
  UteisUpdateDB.ExecutarScript(15, _ComandosUpdateDB.AjustarComissionamentoTitulos);
  UteisUpdateDB.ExecutarScript(16, _ComandosUpdateDB.AjustarProcedureLANCAR_ADIANTAMENTO_CONTAS_REC);
  UteisUpdateDB.ExecutarScript(17, _ComandosUpdateDB.AdicionarColunaCPF_CNPJ_EMITENTETabelaORCAMENTOS_PAGAMENTOS_CHEQUES);
  UteisUpdateDB.ExecutarScript(18, _ComandosUpdateDB.AdicionarColunaCPF_CNPJ_EMITENTETabelaACUMULADOS_PAGAMENTOS_CHEQUES);
  UteisUpdateDB.ExecutarScript(19, _ComandosUpdateDB.AdicionarColunaCPF_CNPJ_EMITENTETabelaCONTAS_REC_BAIXAS_PAGTOS_CHQ);
  UteisUpdateDB.ExecutarScript(20, _ComandosUpdateDB.AdicionarColunaCPF_CNPJ_EMITENTETabelaCONTAS_RECEBER);
  UteisUpdateDB.ExecutarScript(21, _ComandosUpdateDB.AjustarTriggerCONTAS_RECEBER_D_BR);
  UteisUpdateDB.ExecutarScript(22, _ComandosUpdateDB.AjustarTriggerACUMULADOS_IU_BR);
  UteisUpdateDB.ExecutarScript(23, _ComandosUpdateDB.AjustarProcedureRECEBER_ACUMULADO);
  UteisUpdateDB.ExecutarScript(24, _ComandosUpdateDB.AjustarProcedurRECEBER_PEDIDO);
  UteisUpdateDB.ExecutarScript(25, _ComandosUpdateDB.AjustarProcedurCONSOLIDAR_BAIXA_CONTAS_REC);
  UteisUpdateDB.ExecutarScript(26, _ComandosUpdateDB.AjustarProcedureINICIAR_SESSAO);
  UteisUpdateDB.ExecutarScript(27, _ComandosUpdateDB.AjustarColunaPLANO_FINANCEIRO_ID);
  UteisUpdateDB.ExecutarScript(28, _ComandosUpdateDB.AtualizarTriggerCONTAS_PAGAR_U_AR);
  UteisUpdateDB.ExecutarScript(29, _ComandosUpdateDB.AtualizarViewVW_TIPOS_ALTER_LOGS_CT_PAGAR);
  UteisUpdateDB.ExecutarScript(30, _ComandosUpdateDB.AtualizarProcRECEBER_ACUMULADO);
  UteisUpdateDB.ExecutarScript(31, _ComandosUpdateDB.AtualizarProcGERAR_CREDITO_TROCO);
  UteisUpdateDB.ExecutarScript(32, _ComandosUpdateDB.AjustarProcedureGERAR_NOTA_TRANSF_MOVIMENTOS);
  UteisUpdateDB.ExecutarScript(33, _ComandosUpdateDB.AjustarViewVW_MOV_ITE_PEND_EMISSAO_NF_TR);
  UteisUpdateDB.ExecutarScript(34, _ComandosUpdateDB.AjustarTriggerNOTAS_FISCAIS_D_BR);
  UteisUpdateDB.ExecutarScript(35, _ComandosUpdateDB.AjustarTriggerENTREGAS_IU_BR);
  UteisUpdateDB.ExecutarScript(36, _ComandosUpdateDB.AdiconarColunaTIPO_RATEIO_CONHECIMENTO);
  UteisUpdateDB.ExecutarScript(37, _ComandosUpdateDB.AjustarViewVW_PRODUTOS_VENDAS);
  UteisUpdateDB.ExecutarScript(38, _ComandosUpdateDB.AjustarProcedureGERAR_ENTR_TRANS_PRODUTOS_EMP);
  UteisUpdateDB.ExecutarScript(39, _ComandosUpdateDB.AjustarViewVW_ORC_ITENS_SEM_KITS_AGRUPADO);
  UteisUpdateDB.ExecutarScript(40, _ComandosUpdateDB.AjustarProcViewVW_ORC_ITENS_SEM_KITS_AGRUPADO);
  UteisUpdateDB.ExecutarScript(41, _ComandosUpdateDB.AjustarProcGERAR_PENDENCIA_ENTREGAS);
  UteisUpdateDB.ExecutarScript(42, _ComandosUpdateDB.AjustarProcGERAR_ENTREGA_DE_PENDENCIA);
  UteisUpdateDB.ExecutarScript(43, _ComandosUpdateDB.AjustarProcDESAGENDAR_ENTREGA_PENDENTE);
  UteisUpdateDB.ExecutarScript(44, _ComandosUpdateDB.AjustarProcAGENDAR_ITENS_SEM_PREVISAO);
  UteisUpdateDB.ExecutarScript(45, _ComandosUpdateDB.AjustarProcVERIFICAR_LOTE_CADASTRADO);
  UteisUpdateDB.ExecutarScript(46, _ComandosUpdateDB.AjustarCK_CONTAS_RECEBER_BAIXAS_TIPO);
  UteisUpdateDB.ExecutarScript(47, _ComandosUpdateDB.AjustarCK_CONTAS_REC_BX_TIPO_CAD_ID);
  UteisUpdateDB.ExecutarScript(48, _ComandosUpdateDB.AjustarTriggerCONTAS_RECEBER_BX_ITENS_IU_BR);
  UteisUpdateDB.ExecutarScript(49, _ComandosUpdateDB.AjustarProcedureCANCELAR_FECHAMENTO_PEDIDO);
  UteisUpdateDB.ExecutarScript(50, _ComandosUpdateDB.AjustarProcedureCANCELAR_RECEBIMENTO_ACUMULADO);
  UteisUpdateDB.ExecutarScript(51, _ComandosUpdateDB.AjustarConstraintCK_CONTAS_PAGAR_ORIGEM);
  UteisUpdateDB.ExecutarScript(52, _ComandosUpdateDB.AjustarProcedureGERAR_CREDITO_TROCO);
  UteisUpdateDB.ExecutarScript(53, _ComandosUpdateDB.AjustarProcedureCONSOLIDAR_BAIXA_CONTAS_REC2);
  UteisUpdateDB.ExecutarScript(54, _ComandosUpdateDB.AjustarProcedureRECEBER_CONTAS_RECEBER_BAIXA);
  UteisUpdateDB.ExecutarScript(55, _ComandosUpdateDB.AjustarProcedureGERAR_PENDENCIA_ENTREGAS);
  UteisUpdateDB.ExecutarScript(56, _ComandosUpdateDB.AtualizarProcRECEBER_PEDIDO);
  UteisUpdateDB.ExecutarScript(57, _ComandosUpdateDB.AtualizarProcCONSOLIDAR_BAIXA_CONTAS_REC);

  for i := 0 to 9 do begin
    if UteisUpdateDB.ValidarObjetos then
      Exit;
  end;

  Exclamar('Falha ao validar objetos do banco de dados, entre em contato com o suporte da Hiva!');
end;


end.
