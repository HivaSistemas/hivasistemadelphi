unit BuscarIndiceDescontoVenda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, _Biblioteca, _Orcamentos, _Sessao,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameIndicesDescontosVenda,
  Vcl.Buttons, Vcl.ExtCtrls, _RecordsEspeciais, _Acumulados;

type
  TFormBuscarIndiceDescontoVenda = class(TFormHerancaFinalizar)
    FrIndiceDescontoVenda: TFrIndicesDescontosVenda;
    procedure FormShow(Sender: TObject);
  private
    FMovimentoId: Integer;
    FTipoMovimento: string;
    FIndiceDescontoVendaId: Integer;
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Buscar(pMovimentoId: Integer; pTipoMovimento: string; pIndiceDescontoVendaId: Integer): TRetornoTelaFinalizar<Integer>;

implementation

{$R *.dfm}

function Buscar(pMovimentoId: Integer; pTipoMovimento: string; pIndiceDescontoVendaId: Integer): TRetornoTelaFinalizar<Integer>;
var
  vForm: TFormBuscarIndiceDescontoVenda;
begin
  if pMovimentoId = 0 then
    Exit;

  vForm := TFormBuscarIndiceDescontoVenda.Create(nil);

  vForm.FMovimentoId           := pMovimentoId;
  vForm.FTipoMovimento         := pTipoMovimento;
  vForm.FIndiceDescontoVendaId := pIndiceDescontoVendaId;
  vForm.FrIndiceDescontoVenda.InserirDadoPorChave(pIndiceDescontoVendaId, False);

  if Result.Ok(vForm.ShowModal, True) then
    Result.Dados := vForm.FIndiceDescontoVendaId;
end;

{ TFormBuscarIndiceDescontoVenda }

procedure TFormBuscarIndiceDescontoVenda.Finalizar(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  FIndiceDescontoVendaId := 0;
  if not FrIndiceDescontoVenda.EstaVazio then
    FIndiceDescontoVendaId := FrIndiceDescontoVenda.getDados().IndiceId;

  if FTipoMovimento = 'ORC' then
    vRetBanco := _Orcamentos.AtualizarIndiceDescontoVenda(Sessao.getConexaoBanco, FMovimentoId, FIndiceDescontoVendaId)
  else
    vRetBanco := _Acumulados.AtualizarIndiceDescontoVenda(Sessao.getConexaoBanco, FMovimentoId, FIndiceDescontoVendaId);

  Sessao.AbortarSeHouveErro( vRetBanco );

  inherited;
end;

procedure TFormBuscarIndiceDescontoVenda.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrIndiceDescontoVenda);
end;

procedure TFormBuscarIndiceDescontoVenda.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if (FIndiceDescontoVendaId > 0) and FrIndiceDescontoVenda.EstaVazio then begin
    if not _Biblioteca.Perguntar('N�o foi informado o �ndice de desconto de venda, deseja realmente continuar?') then
      Abort;
  end;
end;

end.
