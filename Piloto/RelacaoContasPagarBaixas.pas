unit RelacaoContasPagarBaixas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _Sessao, System.StrUtils,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Grids, GridLuka, Vcl.ComCtrls, Vcl.Buttons, System.Math,
  _ContasPagar, _RecordsEspeciais, Informacoes.TituloPagar, _RecordsRelatorios, _RecordsFinanceiros,
  _Biblioteca, FrameDataInicialFinal, FrameTiposCobranca, _RelacaoContasPagarBaixas,
  FrameEmpresas, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameClientes, _ContasPagarBaixas,
  FrameFornecedores, Vcl.Menus, FrameNumeros, FrameFuncionarios, InformacoesContasPagarBaixa,
  Frame.Inteiros, ImpressaoReciboPagamentoContasPagar, Frame.Cadastros,
  CheckBoxLuka;

type
  TFormRelacaoContasPagarBaixas = class(TFormHerancaRelatoriosPageControl)
    sgBaixas: TGridLuka;
    FrEmpresas: TFrEmpresas;
    FrTiposCobranca: TFrTiposCobranca;
    pmBaixas: TPopupMenu;
    FrUsuarioBaixa: TFrFuncionarios;
    FrDataCadastro: TFrDataInicialFinal;
    FrDataVencimento: TFrDataInicialFinal;
    FrDataBaixa: TFrDataInicialFinal;
    FrDataPagamento: TFrDataInicialFinal;
    FrDataEmissao: TFrDataInicialFinal;
    FrDataContabil: TFrDataInicialFinal;
    FrPedidoCompra: TFrNumeros;
    FrContaPagar: TFrNumeros;
    FrCodigoEntrada: TFrNumeros;
    FrCodigoBaixa: TFrNumeros;
    miN1: TMenuItem;
    FrCadastro: TFrameCadastros;
    miCancelarBaixaSelecionada: TSpeedButton;
    miReimprimirReciboPagamento: TSpeedButton;
    SpeedButton1: TSpeedButton;
    procedure sgBaixasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure miCancelarBaixaSelecionadaClick(Sender: TObject);
    procedure sgBaixasDblClick(Sender: TObject);
    procedure miReimprimirReciboPagamentoClick(Sender: TObject);
    procedure sbImprimirClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    FBaixas: TArray<_RecordsRelatorios.RecContasPagarBaixas>;
  public
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

const
  coBaixa         = 0;
  coTipo          = 1;
  coDataPagamento = 2;
  coDataBaixa     = 3;
  coDinheiro      = 4;
  coCheque        = 5;
  coCartao        = 6;
  coCobranca      = 7;
  coCredito       = 8;
  coEmpresa       = 9;
  coUsuarioBaixa  = 10;

procedure TFormRelacaoContasPagarBaixas.Carregar(Sender: TObject);
var
  i: Integer;
  vComando: string;
begin
  inherited;
  vComando := '';
  sgBaixas.ClearGrid;

  if not FrCodigoBaixa.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrCodigoBaixa.TrazerFiltros('CPB.BAIXA_ID'))
  else begin
    if not FrCadastro.EstaVazio then begin
      WhereOuAnd(
        vComando,
        'CPB.BAIXA_ID in( ' +
        '  select CPI.BAIXA_ID ' +
        '  from CONTAS_PAGAR_BAIXAS_ITENS CPI ' +
        '  inner join CONTAS_PAGAR COP ' +
        '  on CPI.PAGAR_ID = COP.PAGAR_ID ' +
        '  where ' + FrCadastro.getSqlFiltros('COP.CADASTRO_ID') +
        ')'
      );
    end;

    if not FrTiposCobranca.EstaVazio then begin
      WhereOuAnd(
        vComando,
        'CPB.BAIXA_ID in( ' +
        '  select CPI.BAIXA_ID ' +
        '  from CONTAS_PAGAR_BAIXAS_ITENS CPI ' +
        '  inner join CONTAS_PAGAR COP ' +
        '  on CPI.PAGAR_ID = COP.PAGAR_ID ' +
        '  where ' + FrTiposCobranca.getSqlFiltros('COP.COBRANCA_ID') +
        ')'
      );
    end;

    if not FrContaPagar.EstaVazio then begin
      WhereOuAnd(
        vComando,
        'CPB.BAIXA_ID in( ' +
        '  select CPI.BAIXA_ID ' +
        '  from CONTAS_PAGAR_BAIXAS_ITENS CPI ' +
        '  inner join CONTAS_PAGAR COP ' +
        '  on CPI.PAGAR_ID = COP.PAGAR_ID ' +
        '  where ' + FrContaPagar.TrazerFiltros('COP.PAGAR_ID') +
        ')'
      );
    end;

    if not FrCodigoEntrada.EstaVazio then begin
      WhereOuAnd(
        vComando,
        'CPB.BAIXA_ID in( ' +
        '  select CPI.BAIXA_ID ' +
        '  from CONTAS_PAGAR_BAIXAS_ITENS CPI ' +
        '  inner join CONTAS_PAGAR COP ' +
        '  on CPI.PAGAR_ID = COP.PAGAR_ID ' +
        '  where ' + FrCodigoEntrada.TrazerFiltros('COP.ENTRADA_ID') +
        ')'
      );
    end;

    if not FrPedidoCompra.EstaVazio then begin
      WhereOuAnd(
        vComando,
        'CPB.BAIXA_ID in( ' +
        '  select CPI.BAIXA_ID ' +
        '  from CONTAS_PAGAR_BAIXAS_ITENS CPI ' +
        '  inner join CONTAS_PAGAR COP ' +
        '  on CPI.PAGAR_ID = COP.PAGAR_ID ' +
        '  where ' + FrPedidoCompra.TrazerFiltros('COP.COMPRA_ID') +
        ')'
      );
    end;

    if not FrDataCadastro.EstaVazio then begin
      WhereOuAnd(
        vComando,
        'CPB.BAIXA_ID in( ' +
        '  select CPI.BAIXA_ID ' +
        '  from CONTAS_PAGAR_BAIXAS_ITENS CPI ' +
        '  inner join CONTAS_PAGAR COP ' +
        '  on CPI.PAGAR_ID = COP.PAGAR_ID ' +
        '  where ' + FrDataCadastro.getSqlFiltros('COP.DATA_CADASTRO') +
        ')'
      );
    end;

    if not FrDataEmissao.EstaVazio then begin
      WhereOuAnd(
        vComando,
        'CPB.BAIXA_ID in( ' +
        '  select CPI.BAIXA_ID ' +
        '  from CONTAS_PAGAR_BAIXAS_ITENS CPI ' +
        '  inner join CONTAS_PAGAR COP ' +
        '  on CPI.PAGAR_ID = COP.PAGAR_ID ' +
        '  where ' + FrDataEmissao.getSqlFiltros('COP.DATA_EMISSAO') +
        ')'
      );
    end;

    if not FrDataContabil.EstaVazio then begin
      WhereOuAnd(
        vComando,
        'CPB.BAIXA_ID in( ' +
        '  select CPI.BAIXA_ID ' +
        '  from CONTAS_PAGAR_BAIXAS_ITENS CPI ' +
        '  inner join CONTAS_PAGAR COP ' +
        '  on CPI.PAGAR_ID = COP.PAGAR_ID ' +
        '  where ' + FrDataContabil.getSqlFiltros('COP.DATA_CONTABIL') +
        ')'
      );
    end;

    if not FrDataVencimento.EstaVazio then begin
      WhereOuAnd(
        vComando,
        'CPB.BAIXA_ID in( ' +
        '  select CPI.BAIXA_ID ' +
        '  from CONTAS_PAGAR_BAIXAS_ITENS CPI ' +
        '  inner join CONTAS_PAGAR COP ' +
        '  on CPI.PAGAR_ID = COP.PAGAR_ID ' +
        '  where ' + FrDataVencimento.getSqlFiltros('COP.DATA_VENCIMENTO') +
        ')'
      );
    end;

    if FrDataPagamento.DatasValidas then
      WhereOuAnd(vComando, FrDataPagamento.getSqlFiltros('CPB.DATA_PAGAMENTO'));

    if FrDataBaixa.DatasValidas then
      WhereOuAnd(vComando, FrDataBaixa.getSqlFiltros('trunc(CPB.DATA_HORA_BAIXA)'));

    if not FrEmpresas.EstaVazio then
      WhereOuAnd(vComando, FrEmpresas.getSqlFiltros('CPB.EMPRESA_ID'));

    if not FrUsuarioBaixa.EstaVazio then
      _Biblioteca.WhereOuAnd(vComando, FrUsuarioBaixa.getSqlFiltros('CPB.USUARIO_BAIXA_ID'));
  end;

  vComando := vComando + ' order by CPB.BAIXA_ID desc';

  FBaixas := _RelacaoContasPagarBaixas.BuscarContasPagarBaixas(Sessao.getConexaoBanco, vComando);
  if FBaixas = nil then begin
    NenhumRegistro;
    Exit;
  end;

  for i := Low(FBaixas) to High(FBaixas) do begin
    sgBaixas.Cells[coBaixa, i + 1]         := NFormat(FBaixas[i].baixa_id);
    sgBaixas.Cells[coTipo, i + 1]          := FBaixas[i].TipoAnalitico;
    sgBaixas.Cells[coDataPagamento, i + 1] := DFormat(FBaixas[i].data_pagamento);
    sgBaixas.Cells[coDataBaixa, i + 1]     := DFormat(FBaixas[i].data_baixa);
    sgBaixas.Cells[coDinheiro, i + 1]      := NFormatN(FBaixas[i].valor_dinheiro);
    sgBaixas.Cells[coCheque, i + 1]        := NFormatN(FBaixas[i].valor_cheque);
    sgBaixas.Cells[coCartao, i + 1]        := NFormatN(FBaixas[i].valor_cartao);
    sgBaixas.Cells[coCobranca, i + 1]      := NFormatN(FBaixas[i].valor_cobranca);
    sgBaixas.Cells[coCredito, i + 1]       := NFormatN(FBaixas[i].valor_credito);
    sgBaixas.Cells[coEmpresa, i + 1]       := NFormat(FBaixas[i].empresa_id) + ' - ' + FBaixas[i].nome_empresa;
    sgBaixas.Cells[coUsuarioBaixa, i + 1]  := NFormat(FBaixas[i].usuario_baixa_id) + ' - ' + FBaixas[i].nome_usuario_baixa;
  end;
  sgBaixas.SetLinhasGridPorTamanhoVetor( Length(FBaixas) );

  pcDados.ActivePage := tsResultado;
  SetarFoco(sgBaixas);
end;

procedure TFormRelacaoContasPagarBaixas.FormCreate(Sender: TObject);
begin
  inherited;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId, False);
end;

procedure TFormRelacaoContasPagarBaixas.Imprimir(Sender: TObject);
begin
  inherited;

end;

procedure TFormRelacaoContasPagarBaixas.miCancelarBaixaSelecionadaClick(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if not Perguntar('Deseja realmente cancelar esta baixa?') then
    Exit;

  vRetBanco := _ContasPagarBaixas.CancelarBaixaContasPagar(Sessao.getConexaoBanco, SFormatInt(sgBaixas.Cells[coBaixa, sgBaixas.Row]));
  if vRetBanco.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  _Biblioteca.Informar('Baixa cancelada com sucesso.');
  Carregar(Sender);
end;

procedure TFormRelacaoContasPagarBaixas.miReimprimirReciboPagamentoClick(Sender: TObject);
begin
  inherited;
  ImpressaoReciboPagamentoContasPagar.Imprimir(SFormatInt(sgBaixas.Cells[coBaixa, sgBaixas.Row]));
end;

procedure TFormRelacaoContasPagarBaixas.sbImprimirClick(Sender: TObject);
begin
   GridToExcel(sgBaixas);
end;

procedure TFormRelacaoContasPagarBaixas.sgBaixasDblClick(Sender: TObject);
begin
  inherited;
  InformacoesContasPagarBaixa.Informar( SFormatInt(sgBaixas.Cells[coBaixa, sgBaixas.Row]) );
end;

procedure TFormRelacaoContasPagarBaixas.sgBaixasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coBaixa, coDinheiro, coCheque, coCartao, coCobranca, coCredito] then
    vAlinhamento := taRightJustify
  else if ACol = coTipo then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgBaixas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoContasPagarBaixas.SpeedButton1Click(Sender: TObject);
begin
  inherited;
  GridToExcel(sgBaixas);
end;

procedure TFormRelacaoContasPagarBaixas.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
