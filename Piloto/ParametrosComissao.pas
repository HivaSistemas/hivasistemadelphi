unit ParametrosComissao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastro, Vcl.Buttons, _BibliotecaGenerica,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameEmpresas, _Sessao, _RecordsEspeciais,
  Vcl.StdCtrls, StaticTextLuka, Vcl.Grids, GridLuka, EditLuka, _Biblioteca, _ParametrosEmpresa,
  ComboBoxLuka;

type
  TFormParametrosComissao = class(TFormHerancaCadastro)
    FrEmpresa: TFrEmpresas;
    StaticTextLuka5: TStaticTextLuka;
    cbTipoComissao: TComboBoxLuka;
    lb10: TLabel;
    sgFaixas: TGridLuka;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure sbGravarClick(Sender: TObject);
    procedure sgFaixasSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure sgFaixasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sgFaixasDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
  private
    procedure EmpresaOnAposPesquisar(Sender: TObject);
  public
    { Public declarations }
  protected
    procedure Modo(pEditando: Boolean); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormParametrosComissao }

const
  faFaixa        = 0;
  faValorInicial = 1;
  faValorFinal   = 2;
  faPercentual   = 3;
  faColunaMaxima = 4;

procedure TFormParametrosComissao.EmpresaOnAposPesquisar(Sender: TObject);
var
  vFaixas: TArray<RecParametrosComissao>;
  i: Integer;
  vLinha: Integer;
begin
  Modo(True);
  FrEmpresa.Modo(False, False);

  vFaixas := _ParametrosEmpresa.BuscarParametrosComissao(
    Sessao.getConexaoBanco,
    FrEmpresa.getEmpresa().EmpresaId
  );

  vLinha := 0;
  if vFaixas = nil then begin
    sgFaixas.Cells[faFaixa, sgFaixas.row]        := '1';
    sgFaixas.Cells[faValorInicial, sgFaixas.row] := '0.01';
    sgFaixas.Col := faValorFinal;
  end
  else begin
    for i := Low(vFaixas) to High(vFaixas) do begin
      Inc(vLinha);
      sgFaixas.Cells[faFaixa, vLinha]        := IntToStr(vFaixas[i].Faixa);
      sgFaixas.Cells[faValorInicial, vLinha] := _Biblioteca.NFormat(vFaixas[i].ValorInicial);
      sgFaixas.Cells[faValorFinal, vLinha]   := _Biblioteca.NFormat(vFaixas[i].ValorFinal);
      sgFaixas.Cells[faPercentual, vLinha]   := _Biblioteca.NFormat(vFaixas[i].Percentual);
    end;

    Inc(vLinha);
    sgFaixas.RowCount := vLinha + 1;
    sgFaixas.Row := sgFaixas.RowCount - 1;
    sgFaixas.Cells[faFaixa, sgFaixas.Row] := IntToStr(sgFaixas.RowCount - 1);
    sgFaixas.Col := faValorInicial;

    cbTipoComissao.SetIndicePorValor(vFaixas[0].TipoComissao);
  end;
end;

procedure TFormParametrosComissao.FormCreate(Sender: TObject);
begin
  inherited;
  FrEmpresa.OnAposPesquisar := EmpresaOnAposPesquisar;
  FrEmpresa.Modo(True);
  sgFaixas.Col := faValorInicial;
  _Biblioteca.SetarFoco(FrEmpresa);
end;

procedure TFormParametrosComissao.Modo(pEditando: Boolean);
begin
  _Biblioteca.Habilitar([
    sgFaixas,
    cbTipoComissao],
    pEditando
  );

  sbGravar.Enabled := pEditando;
  sbDesfazer.Enabled := pEditando;
  if not pEditando then begin
    FrEmpresa.Modo(True);
    _BibliotecaGenerica.SetarFoco(FrEmpresa);
  end;
end;

procedure TFormParametrosComissao.sbGravarClick(Sender: TObject);
var
  vRetorno: RecRetornoBD;
  vFaixas: TArray<RecParametrosComissao>;
  i: Integer;
begin
  VerificarRegistro(Sender);

  for i := sgFaixas.FixedRows to sgFaixas.RowCount - 1 do begin
    if sgFaixas.Cells[faValorFinal, i] = '' then
      Continue;

    SetLength(vFaixas, Length(vFaixas) + 1);

    vFaixas[i - 1].Faixa          := SFormatInt(sgFaixas.Cells[faFaixa, i]);
    vFaixas[i - 1].ValorInicial   := SFormatDouble(sgFaixas.Cells[faValorInicial, i]);
    vFaixas[i - 1].ValorFinal     := SFormatDouble(sgFaixas.Cells[faValorFinal, i]);
    vFaixas[i - 1].Percentual     := SFormatDouble(sgFaixas.Cells[faPercentual, i]);
  end;

  vRetorno := _ParametrosEmpresa.AtualizarParametrosComissao(
    Sessao.getConexaoBanco,
    FrEmpresa.getEmpresa().EmpresaId,
    vFaixas,
    cbTipoComissao.GetValor
  );

  if vRetorno.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Exit;
  end;

  _Biblioteca.Informar(coAlteracaoRegistroSucesso);

  sbDesfazerClick(Sender);
  _BibliotecaGenerica.SetarFoco(FrEmpresa);
end;

procedure TFormParametrosComissao.sgFaixasDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[faFaixa, faValorInicial, faValorFinal, faPercentual] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgFaixas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormParametrosComissao.sgFaixasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  valorAtual: string;
  valorFinalAnterior: Double;
begin
  inherited;
  if Key = VK_RETURN then begin

    valorAtual := _Biblioteca.NFormat(SFormatDouble(sgFaixas.Cells[sgFaixas.Col, sgFaixas.Row]), 2);
    sgFaixas.Cells[sgFaixas.Col, sgFaixas.Row] := valorAtual;
    if valorAtual <> '0.00' then begin

      if sgFaixas.Col = faValorInicial then begin
        sgFaixas.Col := faValorFinal;
      end
      else if sgFaixas.Col = faValorFinal then begin
        sgFaixas.Col := faPercentual;
      end
      else if sgFaixas.Col = faPercentual then begin

        // Validando se n�o estou na ultima linha
        if sgFaixas.Row = sgFaixas.RowCount - 1 then begin
          // Pegando valor final da linha atual para setar valor inicial da proxima linha
          valorFinalAnterior := _Biblioteca.SFormatDouble(sgFaixas.Cells[faValorFinal, sgFaixas.Row]) + 0.01;

          // Acrescentando uma linha no grid e entrando nela
          sgFaixas.RowCount := sgFaixas.RowCount + 1;
          sgFaixas.Row := sgFaixas.RowCount - 1;

          // Adicionando valores na nova linha e setando posi��o
          sgFaixas.Cells[faFaixa, sgFaixas.RowCount - 1] := IntToStr(sgFaixas.RowCount - 1);
          sgFaixas.Cells[faValorInicial, sgFaixas.Row] := _Biblioteca.NFormat(valorFinalAnterior);
          sgFaixas.Col := faValorFinal;
        end
        else begin
          valorFinalAnterior := _Biblioteca.SFormatDouble(sgFaixas.Cells[faValorFinal, sgFaixas.Row]) + 0.01;
          sgFaixas.Cells[faValorInicial, sgFaixas.Row] := _Biblioteca.NFormat(valorFinalAnterior);
          sgFaixas.Row := sgFaixas.Row + 1;
          sgFaixas.Col := faValorFinal;
        end;
      end;
    end;
  end
  else if (Key = VK_DELETE) and (ssCtrl in Shift) then begin
    if sgFaixas.Row = sgFaixas.RowCount - 1 then begin
      if
        (sgFaixas.Row = 1) and
        ((sgFaixas.RowCount -1) = sgFaixas.Row)
      then begin
        sgFaixas.Col := faValorFinal;
        sgFaixas.Options := sgFaixas.Options + [goEditing];
      end;

      sgFaixas.DeleteRow(sgFaixas.Row, faColunaMaxima);
    end
    else
      Exclamar('S� � permitido remover a ultima linha da tabela.');

  end;
end;

procedure TFormParametrosComissao.sgFaixasSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  CanSelect := ACol in[faValorInicial, faValorFinal, faPercentual];

  if not CanSelect then
    Exit;

  if ACol = faValorInicial then begin
    sgFaixas.OnKeyPress := NumerosVirgula;
    sgFaixas.Options := sgFaixas.Options + [goEditing];
  end
  else if ACol = faValorInicial then begin
    sgFaixas.OnKeyPress := NumerosVirgula;
    sgFaixas.Options := sgFaixas.Options + [goEditing];
  end
  else if ACol = faPercentual then begin
    sgFaixas.OnKeyPress := NumerosVirgula;
    sgFaixas.Options := sgFaixas.Options + [goEditing];
  end;
end;

procedure TFormParametrosComissao.VerificarRegistro(Sender: TObject);
var
  i: Integer;
  vLinhaAtual: Integer;
  vLinhaAnterior: Integer;
begin
  inherited;

  if cbTipoComissao.GetValor = '' then begin
    Exclamar('� necess�rio selecionar o tipo da comiss�o.');
    _Biblioteca.SetarFoco(cbTipoComissao);
    Abort;
  end;

  for i := sgFaixas.FixedRows to sgFaixas.RowCount - 1 do begin
    if (sgFaixas.Cells[faValorFinal, i] = '') and (sgFaixas.Cells[faPercentual, i] = '')   then
      Continue;

    vLinhaAtual := i;
    vLinhaAnterior := i - 1;

    if SFormatDouble(sgFaixas.Cells[faValorInicial, vLinhaAtual]) = 0 then begin
      Exclamar('O valor inicial da faixa ' + sgFaixas.Cells[faFaixa, i] + ' � inv�lido');
      sgFaixas.Col := faValorInicial;
      sgFaixas.Row := vLinhaAtual;
      Abort;
    end;

    if SFormatDouble(sgFaixas.Cells[faValorInicial, vLinhaAtual]) >= SFormatDouble(sgFaixas.Cells[faValorFinal, vLinhaAtual]) then begin
      Exclamar('O valor inicial da faixa ' + sgFaixas.Cells[faFaixa, i] + ' � inv�lido');
      sgFaixas.Col := faValorInicial;
      sgFaixas.Row := vLinhaAtual;
      Abort;
    end;

    if SFormatDouble(sgFaixas.Cells[faValorFinal, vLinhaAtual]) = 0 then begin
      Exclamar('O valor final da faixa ' + sgFaixas.Cells[faFaixa, i] + ' � inv�lido');
      sgFaixas.Col := faValorFinal;
      sgFaixas.Row := vLinhaAtual;
      Abort;
    end;

    if SFormatDouble(sgFaixas.Cells[faValorInicial, vLinhaAtual]) <= SFormatDouble(sgFaixas.Cells[faValorFinal, vLinhaAnterior]) then begin
      if sgFaixas.Cells[faFaixa, vLinhaAtual] <> '1' then begin
        Exclamar('O valor inicial da faixa ' + sgFaixas.Cells[faFaixa, i] + ' � menor que o valor final da faixa anterior');
        sgFaixas.Col := faValorInicial;
        sgFaixas.Row := vLinhaAtual;
        Abort;
      end;
    end;

    if SFormatDouble(sgFaixas.Cells[faPercentual, vLinhaAtual]) = 0 then begin
      Exclamar('O percentual da faixa ' + sgFaixas.Cells[faFaixa, i] + ' � inv�lido');
      sgFaixas.Col := faValorInicial;
      sgFaixas.Row := vLinhaAtual;
      Abort;
    end;

    if SFormatDouble(sgFaixas.Cells[faPercentual, vLinhaAtual]) <= SFormatDouble(sgFaixas.Cells[faPercentual, vLinhaAnterior]) then begin
      if sgFaixas.Cells[faFaixa, vLinhaAtual] <> '1' then begin
        Exclamar('O percentual da faixa ' + sgFaixas.Cells[faFaixa, i] + ' � menor que o percentual da faixa anterior');
        sgFaixas.Col := faValorInicial;
        sgFaixas.Row := vLinhaAtual;
        Abort;
      end;
    end;
  end;
end;

end.
