unit Frame.GerenciadoresCartoesTEF;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _RecordsCadastros,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Sessao, _Biblioteca, System.Math, _GerenciadorCartaoTEF,
  Pesquisa.GerenciadoresCartoesTEF, Vcl.Buttons, Vcl.Menus;

type
  TFrGerenciadoresCartoesTEF = class(TFrameHenrancaPesquisas)
  public
    function GetGerenciadorCartaoTEF(pLinha: Integer = -1): RecGerenciadorCartaoTEF;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrGerenciadoresCartoesTEF }

function TFrGerenciadoresCartoesTEF.AdicionarDireto: TObject;
var
  vGerenciador: TArray<RecGerenciadorCartaoTEF>;
begin
  vGerenciador := _GerenciadorCartaoTEF.BuscarGerenciadorCartaoTEF(Sessao.getConexaoBanco, 0, [FChaveDigitada]);
  if vGerenciador = nil then
    Result := nil
  else
    Result := vGerenciador[0];
end;

function TFrGerenciadoresCartoesTEF.AdicionarPesquisando: TObject;
begin
  Result := Pesquisa.GerenciadoresCartoesTEF.Pesquisar();
end;

function TFrGerenciadoresCartoesTEF.GetGerenciadorCartaoTEF(pLinha: Integer): RecGerenciadorCartaoTEF;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecGerenciadorCartaoTEF(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrGerenciadoresCartoesTEF.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecGerenciadorCartaoTEF(FDados[i]).gerenciador_cartao_id = RecGerenciadorCartaoTEF(pSender).gerenciador_cartao_id then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrGerenciadoresCartoesTEF.MontarGrid;
var
  i: Integer;
  vSender: RecGerenciadorCartaoTEF;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecGerenciadorCartaoTEF(FDados[i]);
      AAdd([IntToStr(vSender.gerenciador_cartao_id), vSender.nome]);
    end;
  end;
end;

end.
