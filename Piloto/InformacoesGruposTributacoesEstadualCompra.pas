unit InformacoesGruposTributacoesEstadualCompra;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, Vcl.ExtCtrls, Vcl.StdCtrls, CheckBoxLuka, EditLuka, _FrameHerancaPrincipal, Frame.ICMSCompra;

type
  TFormInformacoesGruposTributacoesEstadualCompra = class(TFormHerancaFinalizar)
    FrICMS: TFrICMSCompra;
    eID: TEditLuka;
    Label1: TLabel;
    lb1: TLabel;
    eDescricao: TEditLuka;
    ckAtivo: TCheckBoxLuka;
  end;

procedure Informar(pGrupoTribEstadualCompraId: Integer);

implementation

{$R *.dfm}

uses _GruposTribEstadualCompra, _Biblioteca, _Sessao, _GruposTribEstadualCompEst;

procedure Informar(pGrupoTribEstadualCompraId: Integer);
var
  vGrupo: TArray<RecGruposTribEstadualCompra>;
  vForm: TFormInformacoesGruposTributacoesEstadualCompra;
begin
  if pGrupoTribEstadualCompraId = 0 then
    Exit;

  vGrupo := _GruposTribEstadualCompra.BuscarGruposTribEstadualCompra(Sessao.getConexaoBanco, 0, [pGrupoTribEstadualCompraId]);
  if vGrupo = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  vForm := TFormInformacoesGruposTributacoesEstadualCompra.Create(nil);

  vForm.eID.SetInformacao(vGrupo[0].GrupoTribEstadualCompraId);
  vForm.eDescricao.SetInformacao(vGrupo[0].Descricao);
  vForm.ckAtivo.CheckedStr := vGrupo[0].Ativo;
  vForm.FrICMS.ICMS        := _GruposTribEstadualCompEst.BuscarGruposTribEstadualCompEst(Sessao.getConexaoBanco, 0, [vGrupo[0].GrupoTribEstadualCompraId]);
  vForm.FrICMS.SomenteLeitura(True);

  vForm.ShowModal;
  vForm.Free;
end;

end.
