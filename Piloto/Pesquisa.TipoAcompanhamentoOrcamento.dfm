inherited FormTipoAcompanhamentoOrcamento: TFormTipoAcompanhamentoOrcamento
  Caption = 'Pesquisa tipo de acompanhamento de or'#231'amento'
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    ColCount = 4
    OnDrawCell = sgPesquisaDrawCell
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Descri'#231#227'o'
      'Ativo')
    OnGetCellColor = sgPesquisaGetCellColor
    RealColCount = 4
    ColWidths = (
      28
      55
      199
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Text = '0'
  end
end
