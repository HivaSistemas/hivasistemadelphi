unit ParametrosPlanosFinanceiroEmpresa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastro, Vcl.Buttons, _Biblioteca,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameEmpresas,
  Vcl.ComCtrls, FramePlanosFinanceiros, _RecordsEspeciais, _Sessao, _ParametrosPlanosFinancEmpre;

type
  TFormParametrosPlanoFinanceiroEmpresa = class(TFormHerancaCadastro)
    FrEmpresa: TFrEmpresas;
    pcPlanos: TPageControl;
    tsEstoque: TTabSheet;
    FrPlanoFinanceiroAjusteEstoqueNormal: TFrPlanosFinanceiros;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    procedure FrEmpresaOnAposPesquisar(Sender: TObject);
  protected
    procedure BuscarRegistro; override;
    procedure ExcluirRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure Modo(pEditando: Boolean); override;
    procedure ExibirLogs; override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormParametrosPlanoFinanceiroEmpresa }

procedure TFormParametrosPlanoFinanceiroEmpresa.BuscarRegistro;
var
  vParametro: TArray<RecParametrosPlanosFinancEmpre>;
begin
  inherited;
  vParametro := _ParametrosPlanosFinancEmpre.BuscarParametros(Sessao.getConexaoBanco, 0, [FrEmpresa.getEmpresa.EmpresaId]);

  FrPlanoFinanceiroAjusteEstoqueNormal.InserirDadoPorChave( vParametro[0].PlanoFinAjusteEstNormalId );
end;

procedure TFormParametrosPlanoFinanceiroEmpresa.ExcluirRegistro;
begin
  inherited;

end;

procedure TFormParametrosPlanoFinanceiroEmpresa.ExibirLogs;
begin
  inherited;

end;

procedure TFormParametrosPlanoFinanceiroEmpresa.FormCreate(Sender: TObject);
begin
  inherited;
  FrEmpresa.OnAposPesquisar := FrEmpresaOnAposPesquisar;
end;

procedure TFormParametrosPlanoFinanceiroEmpresa.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrEmpresa);
end;

procedure TFormParametrosPlanoFinanceiroEmpresa.FrEmpresaOnAposPesquisar(Sender: TObject);
begin
  Modo(True);
  BuscarRegistro;
end;

procedure TFormParametrosPlanoFinanceiroEmpresa.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;

  vRetBanco :=
    _ParametrosPlanosFinancEmpre.AtualizarParametros(
      Sessao.getConexaoBanco,
      FrEmpresa.getEmpresa().EmpresaId,
      FrPlanoFinanceiroAjusteEstoqueNormal.getDados().PlanoFinanceiroId
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  RotinaSucesso;
  Modo(False);
end;

procedure TFormParametrosPlanoFinanceiroEmpresa.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    pcPlanos,
    FrPlanoFinanceiroAjusteEstoqueNormal],
    pEditando
  );

  if not pEditando then begin
    _Biblioteca.Habilitar([FrEmpresa], True);
    _Biblioteca.SetarFoco(FrEmpresa);
  end
  else begin
    _Biblioteca.Habilitar([FrEmpresa], False, False);
    _Biblioteca.SetarFoco(FrPlanoFinanceiroAjusteEstoqueNormal);
  end;
end;

procedure TFormParametrosPlanoFinanceiroEmpresa.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrPlanoFinanceiroAjusteEstoqueNormal.EstaVazio then begin
    _Biblioteca.Exclamar('O plano financeiro para ajustes de estoques normais n�o foi bem definido, verifique!');
    SetarFoco(FrPlanoFinanceiroAjusteEstoqueNormal);
    Abort;
  end;
end;

end.
