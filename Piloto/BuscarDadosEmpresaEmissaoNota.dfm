inherited FormBuscarDadosEmpresaEmissaoNota: TFormBuscarDadosEmpresaEmissaoNota
  Caption = 'Bustar dados empresa de emiss'#227'o da nota'
  ClientHeight = 113
  ClientWidth = 406
  PrintScale = poNone
  ExplicitWidth = 412
  ExplicitHeight = 142
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 76
    Width = 406
    inherited sbFinalizar: TSpeedButton
      Left = 0
      Width = 402
      Height = 33
      Align = alClient
    end
  end
  inline FrEmpresas: TFrEmpresas
    Left = 0
    Top = 2
    Width = 403
    Height = 63
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitTop = 2
    ExplicitWidth = 403
    ExplicitHeight = 63
    inherited sgPesquisa: TGridLuka
      Width = 378
      Height = 46
      ExplicitWidth = 378
    end
    inherited PnTitulos: TPanel
      Width = 403
      ExplicitWidth = 403
      inherited lbNomePesquisa: TLabel
        Width = 53
        ExplicitWidth = 53
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 298
        ExplicitLeft = 298
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 378
      Height = 47
      ExplicitLeft = 378
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
end
