unit GrupoEstoques;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastro, _Biblioteca, _BibliotecaGenerica,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameEmpresas, Vcl.Buttons, _Sessao,
  Vcl.ExtCtrls, Frame.AdicionarEmpresas, _RecordsEspeciais, _GruposEstoques,
  Frame.HerancaInsercaoExclusao;

type
  TFormGruposEstoques = class(TFormHerancaCadastro)
    FrEmpresa: TFrEmpresas;
    FrEmpresasRetirar: TFrameAdicionarEmpresas;
    FrEmpresasEntregar: TFrameAdicionarEmpresas;
    FrEmpresasRetiraAto: TFrameAdicionarEmpresas;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure EmpresaOnAposPesquisar(Sender: TObject);
  protected
    procedure Modo(pEditando: Boolean); override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormGrupoEstoques }

procedure TFormGruposEstoques.EmpresaOnAposPesquisar(Sender: TObject);

  procedure PreencherGrupos(pFrame: TFrameAdicionarEmpresas; pArrayGrupos: TArray<RecGruposEstoques>);
  var
    i: Integer;
  begin
    for i := Low(pArrayGrupos) to High(pArrayGrupos) do
      pFrame.AddEmpresa( pArrayGrupos[i].EmpresaGrupoId, pArrayGrupos[i].NomeFantasia );
  end;

begin
  Modo(True);
  FrEmpresa.Modo(False, False);

  PreencherGrupos(FrEmpresasRetiraAto, _GruposEstoques.BuscarGruposEstoques(Sessao.getConexaoBanco, 0, [FrEmpresa.getEmpresa.EmpresaId, 'A']));
  PreencherGrupos(FrEmpresasRetirar,   _GruposEstoques.BuscarGruposEstoques(Sessao.getConexaoBanco, 0, [FrEmpresa.getEmpresa.EmpresaId, 'R']));
  PreencherGrupos(FrEmpresasEntregar,  _GruposEstoques.BuscarGruposEstoques(Sessao.getConexaoBanco, 0, [FrEmpresa.getEmpresa.EmpresaId, 'E']));
end;

procedure TFormGruposEstoques.FormCreate(Sender: TObject);
begin
  inherited;
  FrEmpresa.OnAposPesquisar := EmpresaOnAposPesquisar;
  FrEmpresa.Modo(True);
end;

procedure TFormGruposEstoques.FormShow(Sender: TObject);
begin
  inherited;
  _BibliotecaGenerica.SetarFoco(FrEmpresa.sgPesquisa);
end;

procedure TFormGruposEstoques.GravarRegistro(Sender: TObject);
var
  vRetorno: RecRetornoBD;
begin
  inherited;

  Sessao.getConexaoBanco.IniciarTransacao;

  vRetorno := _GruposEstoques.AtualizarGruposEstoques(Sessao.getConexaoBanco, FrEmpresa.getEmpresa().EmpresaId, 'A', FrEmpresasRetiraAto.TrazerArrayEmpresas);
  if vRetorno.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  vRetorno := _GruposEstoques.AtualizarGruposEstoques(Sessao.getConexaoBanco, FrEmpresa.getEmpresa().EmpresaId, 'R', FrEmpresasRetirar.TrazerArrayEmpresas);
  if vRetorno.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  vRetorno := _GruposEstoques.AtualizarGruposEstoques(Sessao.getConexaoBanco, FrEmpresa.getEmpresa().EmpresaId, 'E', FrEmpresasEntregar.TrazerArrayEmpresas);
  if vRetorno.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  Sessao.getConexaoBanco.FinalizarTransacao;

  _Biblioteca.Informar(coAlteracaoRegistroSucesso);
  Modo(False);
end;

procedure TFormGruposEstoques.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    FrEmpresasRetiraAto,
    FrEmpresasRetirar,
    FrEmpresasEntregar],
    pEditando
  );

  sbGravar.Enabled := pEditando;
  sbDesfazer.Enabled := pEditando;
  if not pEditando then begin
    FrEmpresa.Modo(True);
    _BibliotecaGenerica.SetarFoco(FrEmpresa);
  end;
end;

procedure TFormGruposEstoques.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if FrEmpresasRetiraAto.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio definir pelo menos 1 empresa para "Retira no ato"!');
    _Biblioteca.SetarFoco(FrEmpresasRetiraAto);
    Abort;
  end;

  if FrEmpresasRetirar.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio definir pelo menos 1 empresa para "A retirar lojas"!');
    _Biblioteca.SetarFoco(FrEmpresasRetirar);
    Abort;
  end;

  if FrEmpresasEntregar.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio definir pelo menos 1 empresa para "A entregar"!');
    _Biblioteca.SetarFoco(FrEmpresasEntregar);
    Abort;
  end;
end;

end.
