inherited FormBuscarDocumentoFiscalEmitir: TFormBuscarDocumentoFiscalEmitir
  Caption = 'Busca de doc. fisc. a emitir'
  ClientHeight = 186
  ClientWidth = 206
  ExplicitWidth = 212
  ExplicitHeight = 215
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 149
    Width = 206
    ExplicitTop = 105
    ExplicitWidth = 322
    inherited sbFinalizar: TSpeedButton
      Left = 0
      Width = 202
      Align = alTop
    end
  end
  inline FrDocumentoFiscalEmitir: TFrameDocumentoFiscalEmitir
    Left = 0
    Top = 3
    Width = 201
    Height = 137
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitTop = 3
    ExplicitHeight = 137
    inherited gbDocumentosEmitir: TGroupBox
      Left = 8
      Top = 0
      Height = 121
      Caption = 'Tipos de documento'
      ExplicitLeft = 8
      ExplicitTop = 0
      ExplicitHeight = 121
      inherited ckRecibo: TCheckBoxLuka
        Caption = 'Comprovante Pag.'
      end
      inherited ckEmitirNFCe: TCheckBoxLuka
        Top = 49
        Width = 145
        Caption = 'Cupom Fiscal (NFC-e)'
        ExplicitTop = 49
        ExplicitWidth = 145
      end
      inherited ckEmitirNFe: TCheckBoxLuka
        Top = 84
        Width = 129
        Caption = 'Nota Fiscal (NF-e)'
        ExplicitTop = 84
        ExplicitWidth = 129
      end
    end
  end
end
