﻿unit _ComandosUpdateDB;

interface

uses
  UteisUpdateDB;

function AdicionarColunaVALOR_ADIANTADO: RecScript;
function AdicionarColunaPAGAR_ADIANTADO_ID: RecScript;
function AjustarConstraintCK_CONTAS_PAGAR_BAIXAS_TIPO: RecScript;
function AjustarConstraintCK_CONTAS_PAG_BX_TIPO_CAD_ID: RecScript;
function AjustarProcedureLANCAR_ADIANT_CONTAS_PAGAR: RecScript;
function AjustarProcedureFECHAR_PEDIDO: RecScript;
function AjustarProcedureFECHAR_ACUMULADO: RecScript;
function AjustarProcedureCONSOLIDAR_BAIXA_CONTAS_REC: RecScript;
function AjustarProcedureRECEBER_PEDIDO: RecScript;
function AjustarProcedureBAIXAR_CREDITO_PAGAR: RecScript;
function AdicionarColunaCONTAS_RECEBER: RecScript;
function AjustarConstraintCK_CONTAS_REC_COMISSIONAR: RecScript;
function AjustarViewVW_TIPOS_COB_PEDIDOS_COM_BAIXA: RecScript;
function AjustarViewVW_COMISSAO_POR_FUNCIONARIOS: RecScript;
function AjustarComissionamentoTitulos: RecScript;
function AjustarProcedureLANCAR_ADIANTAMENTO_CONTAS_REC: RecScript;
function AdicionarColunaCPF_CNPJ_EMITENTETabelaORCAMENTOS_PAGAMENTOS_CHEQUES: RecScript;
function AdicionarColunaCPF_CNPJ_EMITENTETabelaACUMULADOS_PAGAMENTOS_CHEQUES: RecScript;
function AdicionarColunaCPF_CNPJ_EMITENTETabelaCONTAS_REC_BAIXAS_PAGTOS_CHQ: RecScript;
function AdicionarColunaCPF_CNPJ_EMITENTETabelaCONTAS_RECEBER: RecScript;
function AjustarTriggerCONTAS_RECEBER_D_BR: RecScript;
function AjustarTriggerACUMULADOS_IU_BR: RecScript;
function AjustarProcedureRECEBER_ACUMULADO: RecScript;
function AjustarProcedurRECEBER_PEDIDO: RecScript;
function AjustarProcedurCONSOLIDAR_BAIXA_CONTAS_REC: RecScript;
function AjustarProcedureINICIAR_SESSAO: RecScript;
function AjustarColunaPLANO_FINANCEIRO_ID: RecScript;
function AtualizarTriggerCONTAS_PAGAR_U_AR: RecScript;
function AtualizarViewVW_TIPOS_ALTER_LOGS_CT_PAGAR: RecScript;
function AtualizarProcRECEBER_ACUMULADO: RecScript;
function AtualizarProcGERAR_CREDITO_TROCO: RecScript;
function AjustarProcedureGERAR_NOTA_TRANSF_MOVIMENTOS: RecScript;
function AjustarViewVW_MOV_ITE_PEND_EMISSAO_NF_TR: RecScript;
function AjustarTriggerNOTAS_FISCAIS_D_BR: RecScript;
function AjustarTriggerENTREGAS_IU_BR: RecScript;
function AdiconarColunaTIPO_RATEIO_CONHECIMENTO: RecScript;
function AjustarViewVW_PRODUTOS_VENDAS: RecScript;
function AjustarProcedureGERAR_ENTR_TRANS_PRODUTOS_EMP: RecScript;
function AjustarViewVW_ORC_ITENS_SEM_KITS_AGRUPADO: RecScript;
function AjustarProcViewVW_ORC_ITENS_SEM_KITS_AGRUPADO: RecScript;
function AjustarProcGERAR_PENDENCIA_ENTREGAS: RecScript;
function AjustarProcGERAR_ENTREGA_DE_PENDENCIA: RecScript;
function AjustarProcDESAGENDAR_ENTREGA_PENDENTE: RecScript;
function AjustarProcAGENDAR_ITENS_SEM_PREVISAO: RecScript;
function AjustarProcVERIFICAR_LOTE_CADASTRADO: RecScript;
function AjustarCK_CONTAS_RECEBER_BAIXAS_TIPO: RecScript;
function AjustarCK_CONTAS_REC_BX_TIPO_CAD_ID: RecScript;
function AjustarTriggerCONTAS_RECEBER_BX_ITENS_IU_BR: RecScript;
function AjustarProcedureCANCELAR_FECHAMENTO_PEDIDO: RecScript;
function AjustarProcedureCANCELAR_RECEBIMENTO_ACUMULADO: RecScript;
function AjustarConstraintCK_CONTAS_PAGAR_ORIGEM: RecScript;
function AjustarProcedureGERAR_CREDITO_TROCO: RecScript;
function AjustarProcedureCONSOLIDAR_BAIXA_CONTAS_REC2: RecScript;
function AjustarProcedureRECEBER_CONTAS_RECEBER_BAIXA: RecScript;
function AjustarProcedureGERAR_PENDENCIA_ENTREGAS: RecScript;
function AtualizarProcRECEBER_PEDIDO: RecScript;
function AtualizarProcCONSOLIDAR_BAIXA_CONTAS_REC: RecScript;

implementation

function AdicionarColunaVALOR_ADIANTADO: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'alter table CONTAS_PAGAR'+ #13 +
      'add VALOR_ADIANTADO           number(8,2) default 0 not null;'+ #13 +
      #13 +
      'alter table CONTAS_PAGAR'+ #13 +
      'add constraint CK_CONTAS_PAGAR_VALOR_ADIANT'+ #13 +
      'check(VALOR_ADIANTADO >= 0 and VALOR_ADIANTADO < VALOR_DOCUMENTO - VALOR_DESCONTO);'+ #13
    );
end;

function AdicionarColunaPAGAR_ADIANTADO_ID: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'alter table CONTAS_PAGAR_BAIXAS'+ #13 +
      'add PAGAR_ADIANTADO_ID      number(12);'+ #13 +
      #13 +
      'alter table CONTAS_PAGAR_BAIXAS'+ #13 +
      'add constraint FK_CONTAS_PAG_BX_PAG_ADI_ID'+ #13 +
      'foreign key(PAGAR_ADIANTADO_ID)'+ #13 +
      'references CONTAS_PAGAR(PAGAR_ID);'+ #13
    );
end;

function AjustarConstraintCK_CONTAS_PAGAR_BAIXAS_TIPO: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'alter table CONTAS_PAGAR_BAIXAS'+ #13 +
      'drop constraint CK_CONTAS_PAGAR_BAIXAS_TIPO;'+ #13 +
      #13 +
      'alter table CONTAS_PAGAR_BAIXAS'+ #13 +
      'add constraint CK_CONTAS_PAGAR_BAIXAS_TIPO'+ #13 +
      'check('+ #13 +
      '  TIPO in(''CRE'', ''BCP'', ''BXN'')'+ #13 +
      '  or'+ #13 +
      '  (TIPO = ''ECP'') and BAIXA_RECEBER_ORIGEM_ID is not null'+ #13 +
      '  or'+ #13 +
      '  TIPO = ''FAC'' and ACUMULADO_ID is not null'+ #13 +
      '  or'+ #13 +
      '  TIPO = ''FPE'' and ORCAMENTO_ID is not null'+ #13 +
      '  or'+ #13 +
      '  TIPO = ''ADI'' and PAGAR_ADIANTADO_ID is not null'+ #13 +
      ');'+ #13
    );
end;

function AjustarConstraintCK_CONTAS_PAG_BX_TIPO_CAD_ID: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'alter table CONTAS_PAGAR_BAIXAS'+ #13 +
      'drop constraint CK_CONTAS_PAG_BX_TIPO_CAD_ID;'+ #13 +
      #13 +
      'alter table CONTAS_PAGAR_BAIXAS'+ #13 +
      'add constraint CK_CONTAS_PAG_BX_TIPO_CAD_ID'+ #13 +
      'check(TIPO in(''CRE'', ''BCP'', ''BXN'', ''ECP'', ''FAC'', ''FPE'', ''ADI'') and CADASTRO_ID is not null);'+ #13
    );
end;

function AjustarProcedureLANCAR_ADIANT_CONTAS_PAGAR: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure LANCAR_ADIANT_CONTAS_PAGAR(iBAIXA_ID in number, oPAGAR_ID out number)'+ #13 +
      'is'+ #13 +
      '  vEmpresaId          CONTAS_PAGAR_BAIXAS.EMPRESA_ID%type;'+ #13 +
      '  vClienteId          CONTAS_PAGAR_BAIXAS.CADASTRO_ID%type;'+ #13 +
      '  vValorBaixa         CONTAS_PAGAR_BAIXAS.VALOR_LIQUIDO%type;'+ #13 +
      '  vPagarAdiantadoId   CONTAS_PAGAR_BAIXAS.PAGAR_ADIANTADO_ID%type;'+ #13 +
      'begin'+ #13 +
      #13 +
      '  select'+ #13 +
      '    EMPRESA_ID,'+ #13 +
      '    CADASTRO_ID,'+ #13 +
      '    VALOR_LIQUIDO,'+ #13 +
      '    PAGAR_ADIANTADO_ID'+ #13 +
      '  into'+ #13 +
      '    vEmpresaId,'+ #13 +
      '    vClienteId,'+ #13 +
      '    vValorBaixa,'+ #13 +
      '    vPagarAdiantadoId'+ #13 +
      '  from'+ #13 +
      '    CONTAS_PAGAR_BAIXAS'+ #13 +
      '  where BAIXA_ID = iBAIXA_ID'+ #13 +
      '  and TIPO = ''ADI'';'+ #13 +
      #13 +
      '  update CONTAS_PAGAR set'+ #13 +
      '    VALOR_ADIANTADO = VALOR_ADIANTADO + vValorBaixa'+ #13 +
      '  where PAGAR_ID = vPagarAdiantadoId;'+ #13 +
      #13 +
      '  insert into CONTAS_PAGAR('+ #13 +
      '    PAGAR_ID,'+ #13 +
      '    CADASTRO_ID,'+ #13 +
      '    EMPRESA_ID,      '+ #13 +
      '    COBRANCA_ID,'+ #13 +
      '    PORTADOR_ID,'+ #13 +
      '    PLANO_FINANCEIRO_ID,      '+ #13 +
      '    DOCUMENTO,'+ #13 +
      '    DATA_EMISSAO,'+ #13 +
      '    DATA_VENCIMENTO,'+ #13 +
      '    DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '    VALOR_DOCUMENTO,'+ #13 +
      '    STATUS,'+ #13 +
      '    PARCELA,'+ #13 +
      '    NUMERO_PARCELAS,'+ #13 +
      '    ORIGEM,'+ #13 +
      '    BAIXA_ORIGEM_ID'+ #13 +
      '  )values('+ #13 +
      '    SEQ_PAGAR_ID.nextval,'+ #13 +
      '    vClienteId,'+ #13 +
      '    vEmpresaId,'+ #13 +
      '    SESSAO.PARAMETROS_GERAIS.TIPO_COB_ADIANTAMENTO_FIN_ID,'+ #13 +
      '    ''9999'','+ #13 +
      '    ''2.012.001'','+ #13 +
      '    ''BXP-'' || iBAIXA_ID || ''/ADI'','+ #13 +
      '    trunc(sysdate),'+ #13 +
      '    trunc(sysdate),'+ #13 +
      '    trunc(sysdate),'+ #13 +
      '    vValorBaixa,'+ #13 +
      '    ''A'','+ #13 +
      '    1,'+ #13 +
      '    1,'+ #13 +
      '    ''BCP'','+ #13 +
      '    iBAIXA_ID'+ #13 +
      '  );'+ #13 +
      #13 +
      '  oPAGAR_ID := SEQ_PAGAR_ID.currval;'+ #13 +
      #13 +
      'end LANCAR_ADIANT_CONTAS_PAGAR;'+ #13
    );
end;

function AjustarProcedureFECHAR_PEDIDO: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure FECHAR_PEDIDO('+ #13 +
      '  iORCAMENTO_ID in number'+ #13 +
      ')'+ #13 +
      'is  '+ #13 +
      '  vEmpresaId            ORCAMENTOS.EMPRESA_ID%type;  '+ #13 +
      '  vClienteId            CLIENTES.CADASTRO_ID%type;'+ #13 +
      '  vPrevisaoEntrega      date;'+ #13 +
      '  vValorBaixarCredito   number default 0;'+ #13 +
      '  vValorRestante        number default 0;'+ #13 +
      #13 +
      '  vBaixaCreditoPagarId  CONTAS_PAGAR_BAIXAS.BAIXA_ID%type;'+ #13 +
      '  vValorDinheiro        ORCAMENTOS.VALOR_DINHEIRO%type;'+ #13 +
      '  vValorCheque          ORCAMENTOS.VALOR_CHEQUE%type;'+ #13 +
      '  vValorCartaoDebito    ORCAMENTOS.VALOR_CARTAO_DEBITO%type;'+ #13 +
      '  vValorCartaoCredito   ORCAMENTOS.VALOR_CARTAO_CREDITO%type;'+ #13 +
      '  vValorCobranca        ORCAMENTOS.VALOR_COBRANCA%type;'+ #13 +
      '  vValorFinanceira      ORCAMENTOS.VALOR_FINANCEIRA%type;'+ #13 +
      '  vValorCredito         ORCAMENTOS.VALOR_CREDITO%type;'+ #13 +
      '  vValorTotal           ORCAMENTOS.VALOR_TOTAL%type;'+ #13 +
      #13 +
      '  cursor cItens is'+ #13 +
      '  select'+ #13 +
      '    PRODUTO_ID,'+ #13 +
      '    ITEM_ID,'+ #13 +
      '    QUANTIDADE,'+ #13 +
      '    QUANTIDADE_RETIRAR_ATO,'+ #13 +
      '    QUANTIDADE_RETIRAR,'+ #13 +
      '    QUANTIDADE_ENTREGAR,'+ #13 +
      '    QUANTIDADE_SEM_PREVISAO'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS_ITENS'+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '  order by '+ #13 +
      '    ITEM_ID;'+ #13 +
      #13 +
      #13 +
      '  cursor cCreditos is'+ #13 +
      '  select'+ #13 +
      '    ORC.PAGAR_ID,'+ #13 +
      '    PAG.VALOR_DOCUMENTO - PAG.VALOR_DESCONTO - PAG.VALOR_ADIANTADO as VALOR_LIQUIDO'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS_CREDITOS ORC'+ #13 +
      #13 +
      '  inner join CONTAS_PAGAR PAG'+ #13 +
      '  on ORC.PAGAR_ID = PAG.PAGAR_ID'+ #13 +
      #13 +
      '  where ORC.ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '  and ORC.MOMENTO_USO = ''F'';'+ #13 +
      #13 +
      #13 +
      '  procedure INSERIR_ITEM_GERAR_ENTREGA(  '+ #13 +
      '    pPRODUTO_ID in number,'+ #13 +
      '    pITEM_ID in number,'+ #13 +
      '    pQUANTIDADE in number,'+ #13 +
      '    pPREVISAO_ENTREGA in date,'+ #13 +
      '    pTIPO_ENTREGA in string'+ #13 +
      '  )'+ #13 +
      '  is begin'+ #13 +
      '    insert into ENTREGAS_ITENS_A_GERAR_TEMP('+ #13 +
      '      ORCAMENTO_ID,'+ #13 +
      '      PRODUTO_ID,'+ #13 +
      '      ITEM_ID,'+ #13 +
      '      QUANTIDADE,'+ #13 +
      '      TIPO_ENTREGA,'+ #13 +
      '      PREVISAO_ENTREGA'+ #13 +
      '    )values('+ #13 +
      '      iORCAMENTO_ID,'+ #13 +
      '      pPRODUTO_ID,'+ #13 +
      '      pITEM_ID,'+ #13 +
      '      pQUANTIDADE,'+ #13 +
      '      pTIPO_ENTREGA,'+ #13 +
      '      pPREVISAO_ENTREGA'+ #13 +
      '    );'+ #13 +
      '  end;'+ #13 +
      #13 +
      'begin'+ #13 +
      #13 +
      '  select'+ #13 +
      '    ORC.EMPRESA_ID,    '+ #13 +
      '    ORC.CLIENTE_ID,'+ #13 +
      '    ORC.DATA_ENTREGA,'+ #13 +
      '    ORC.VALOR_DINHEIRO,'+ #13 +
      '    ORC.VALOR_CHEQUE,'+ #13 +
      '    ORC.VALOR_CARTAO_DEBITO,'+ #13 +
      '    ORC.VALOR_CARTAO_CREDITO,'+ #13 +
      '    ORC.VALOR_COBRANCA,'+ #13 +
      '    ORC.VALOR_FINANCEIRA,'+ #13 +
      '    ORC.VALOR_CREDITO,'+ #13 +
      '    ORC.VALOR_TOTAL'+ #13 +
      '  into'+ #13 +
      '    vEmpresaId,'+ #13 +
      '    vClienteId,'+ #13 +
      '    vPrevisaoEntrega,'+ #13 +
      '    vValorDinheiro,'+ #13 +
      '    vValorCheque,'+ #13 +
      '    vValorCartaoDebito,'+ #13 +
      '    vValorCartaoCredito,'+ #13 +
      '    vValorCobranca,'+ #13 +
      '    vValorFinanceira,'+ #13 +
      '    vValorCredito,'+ #13 +
      '    vValorTotal'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS ORC'+ #13 +
      '  where ORC.ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      #13 +
      '  /* Criando os produtos que compôe o kit na tabela  ORCAMENTOS_ITENS */'+ #13 +
      '  DISTRIBUIR_KIT_FECHAMENTO_PED(iORCAMENTO_ID);'+ #13 +
      #13 +
      '  /* Adicionando na tabela temporária que faz a mágica dos agendamentos, simplismente o coração das entregas do sistema Hiva */'+ #13 +
      '  /* Aproveitando o mesmo loop já para gravar os custos dos itens */'+ #13 +
      '  for vItens in cItens loop'+ #13 +
      '    if vItens.QUANTIDADE_RETIRAR_ATO > 0 then'+ #13 +
      '      INSERIR_ITEM_GERAR_ENTREGA(  '+ #13 +
      '        vItens.PRODUTO_ID,'+ #13 +
      '        vItens.ITEM_ID,'+ #13 +
      '        vItens.QUANTIDADE_RETIRAR_ATO,'+ #13 +
      '        null,'+ #13 +
      '        ''RA'''+ #13 +
      '      );'+ #13 +
      '    end if;'+ #13 +
      #13 +
      '    if vItens.QUANTIDADE_RETIRAR > 0 then'+ #13 +
      '      INSERIR_ITEM_GERAR_ENTREGA('+ #13 +
      '        vItens.PRODUTO_ID,'+ #13 +
      '        vItens.ITEM_ID,'+ #13 +
      '        vItens.QUANTIDADE_RETIRAR,'+ #13 +
      '        null,'+ #13 +
      '        ''RE'''+ #13 +
      '      );'+ #13 +
      '    end if;'+ #13 +
      #13 +
      '    if vItens.QUANTIDADE_ENTREGAR > 0 then'+ #13 +
      '      INSERIR_ITEM_GERAR_ENTREGA('+ #13 +
      '        vItens.PRODUTO_ID,'+ #13 +
      '        vItens.ITEM_ID,'+ #13 +
      '        vItens.QUANTIDADE_ENTREGAR,'+ #13 +
      '        vPrevisaoEntrega,'+ #13 +
      '        ''EN'''+ #13 +
      '      );'+ #13 +
      '    end if;'+ #13 +
      #13 +
      '    if vItens.QUANTIDADE_SEM_PREVISAO > 0 then'+ #13 +
      '      INSERIR_ITEM_GERAR_ENTREGA('+ #13 +
      '        vItens.PRODUTO_ID,'+ #13 +
      '        vItens.ITEM_ID,'+ #13 +
      '        vItens.QUANTIDADE_SEM_PREVISAO,'+ #13 +
      '        null,'+ #13 +
      '        ''SP'''+ #13 +
      '      );'+ #13 +
      '    end if;'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  /* Gerando as retiradas no ato */'+ #13 +
      '  GERAR_RETIRADAS_ATO(iORCAMENTO_ID, vEmpresaId);'+ #13 +
      #13 +
      '  /* Gerando as retiradas pendentes */'+ #13 +
      '  GERAR_PENDENCIA_RETIRADAS(iORCAMENTO_ID, vEmpresaId);'+ #13 +
      #13 +
      '  /* Gerando as entregas pendentes */'+ #13 +
      '  GERAR_PENDENCIA_ENTREGAS(iORCAMENTO_ID, vEmpresaId);'+ #13 +
      #13 +
      '  /* Gerando os itens sem previsão */'+ #13 +
      '  GERAR_PENDENCIA_SEM_PREVISAO(iORCAMENTO_ID);'+ #13 +
      #13 +
      '  delete from ORCAMENTOS_ITENS_DEF_LOTES'+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      #13 +
      '  if vValorCredito > 0 then'+ #13 +
      '    vValorRestante := vValorTotal - (vValorDinheiro + vValorCheque + vValorCartaoDebito + vValorCartaoCredito + vValorCobranca + vValorFinanceira);'+ #13 +
      '    for xCreditos in cCreditos loop'+ #13 +
      '      if xCreditos.VALOR_LIQUIDO >= vValorRestante then'+ #13 +
      '        vValorBaixarCredito := vValorRestante;'+ #13 +
      '        vValorRestante := 0;'+ #13 +
      '      else'+ #13 +
      '        vValorBaixarCredito := xCreditos.VALOR_LIQUIDO;'+ #13 +
      '        vValorRestante := vValorRestante - vValorBaixarCredito;'+ #13 +
      '      end if;'+ #13 +
      #13 +
      '      BAIXAR_CREDITO_PAGAR(xCreditos.PAGAR_ID, iORCAMENTO_ID, ''ORC'', vValorBaixarCredito, vBaixaCreditoPagarId, vBaixaCreditoPagarId);'+ #13 +
      #13 +
      '      if vValorRestante = 0 then'+ #13 +
      '        exit;'+ #13 +
      '      end if;'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      'end FECHAR_PEDIDO;'+ #13
    );
end;

function AjustarProcedureFECHAR_ACUMULADO: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure FECHAR_ACUMULADO(iACUMULADO_ID in number)'+ #13 +
      'is'+ #13 +
      #13 +
      '  vValorBaixarCredito   number default 0;'+ #13 +
      '  vValorRestante        number default 0;'+ #13 +
      #13 +
      '  vBaixaCreditoPagarId  CONTAS_PAGAR_BAIXAS.BAIXA_ID%type;'+ #13 +
      '  vValorDinheiro        ACUMULADOS.VALOR_DINHEIRO%type;'+ #13 +
      '  vValorCheque          ACUMULADOS.VALOR_CHEQUE%type;'+ #13 +
      '  vValorCartaoDebito    ACUMULADOS.VALOR_CARTAO_DEBITO%type;'+ #13 +
      '  vValorCartaoCredito   ACUMULADOS.VALOR_CARTAO_CREDITO%type;'+ #13 +
      '  vValorCobranca        ACUMULADOS.VALOR_COBRANCA%type;'+ #13 +
      '  vValorFinanceira      ACUMULADOS.VALOR_FINANCEIRA%type;'+ #13 +
      '  vValorCredito         ACUMULADOS.VALOR_CREDITO%type;'+ #13 +
      '  vValorTotal           ACUMULADOS.VALOR_TOTAL%type;'+ #13 +
      #13 +
      '  cursor cCreditos is'+ #13 +
      '  select'+ #13 +
      '    ACU.PAGAR_ID,'+ #13 +
      '    PAG.VALOR_DOCUMENTO - PAG.VALOR_ADIANTADO - PAG.VALOR_DESCONTO as VALOR_LIQUIDO'+ #13 +
      '  from'+ #13 +
      '    ACUMULADOS_CREDITOS ACU'+ #13 +
      #13 +
      '  inner join CONTAS_PAGAR PAG'+ #13 +
      '  on ACU.PAGAR_ID = PAG.PAGAR_ID'+ #13 +
      #13 +
      '  where ACU.ACUMULADO_ID = iACUMULADO_ID'+ #13 +
      '  order by'+ #13 +
      '    case when PAG.ORIGEM = ''ADA'' then 0 else 1 end;'+ #13 +
      #13 +
      '  cursor cOrcs is'+ #13 +
      '  select'+ #13 +
      '    ORCAMENTO_ID'+ #13 +
      '  from'+ #13 +
      '    ACUMULADOS_ORCAMENTOS'+ #13 +
      '  where ACUMULADO_ID = iACUMULADO_ID;'+ #13 +
      #13 +
      'begin'+ #13 +
      #13 +
      '  select'+ #13 +
      '    ACU.VALOR_DINHEIRO,'+ #13 +
      '    ACU.VALOR_CHEQUE,'+ #13 +
      '    ACU.VALOR_CARTAO_DEBITO,'+ #13 +
      '    ACU.VALOR_CARTAO_CREDITO,'+ #13 +
      '    ACU.VALOR_COBRANCA,'+ #13 +
      '    ACU.VALOR_FINANCEIRA,'+ #13 +
      '    ACU.VALOR_CREDITO,'+ #13 +
      '    ACU.VALOR_TOTAL'+ #13 +
      '  into'+ #13 +
      '    vValorDinheiro,'+ #13 +
      '    vValorCheque,'+ #13 +
      '    vValorCartaoDebito,'+ #13 +
      '    vValorCartaoCredito,'+ #13 +
      '    vValorCobranca,'+ #13 +
      '    vValorFinanceira,'+ #13 +
      '    vValorCredito,'+ #13 +
      '    vValorTotal'+ #13 +
      '  from'+ #13 +
      '    ACUMULADOS ACU'+ #13 +
      '  where ACU.ACUMULADO_ID = iACUMULADO_ID;'+ #13 +
      #13 +
      '  if vValorCredito > 0 then'+ #13 +
      '    vValorRestante := vValorTotal - (vValorDinheiro + vValorCheque + vValorCartaoDebito + vValorCartaoCredito + vValorCobranca + vValorFinanceira);'+ #13 +
      '    for xCreditos in cCreditos loop'+ #13 +
      '      if xCreditos.VALOR_LIQUIDO >= vValorRestante then'+ #13 +
      '        vValorBaixarCredito := vValorRestante;'+ #13 +
      '        vValorRestante := 0;'+ #13 +
      '      else'+ #13 +
      '        vValorBaixarCredito := xCreditos.VALOR_LIQUIDO;'+ #13 +
      '        vValorRestante := vValorRestante - vValorBaixarCredito;'+ #13 +
      '      end if;'+ #13 +
      #13 +
      '      BAIXAR_CREDITO_PAGAR(xCreditos.PAGAR_ID, iACUMULADO_ID, ''ACU'', vValorBaixarCredito, vBaixaCreditoPagarId, vBaixaCreditoPagarId);'+ #13 +
      #13 +
      '      if vValorRestante = 0 then'+ #13 +
      '        exit;'+ #13 +
      '      end if;'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  for xOrcs in cOrcs loop'+ #13 +
      '    delete from CONTAS_RECEBER'+ #13 +
      '    where ORCAMENTO_ID = xOrcs.ORCAMENTO_ID'+ #13 +
      '    and COBRANCA_ID = (select TIPO_COB_ACUMULATIVO_ID from PARAMETROS);'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      'end FECHAR_ACUMULADO;'+ #13
    );
end;

function AjustarProcedureCONSOLIDAR_BAIXA_CONTAS_REC: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure CONSOLIDAR_BAIXA_CONTAS_REC(iBAIXA_RECEBER_ID in number)'+ #13 +
      'is'+ #13 +
      #13 +
      '  vReceberId     CONTAS_RECEBER.RECEBER_ID%type;'+ #13 +
      '  vCadastroId    CONTAS_RECEBER_BAIXAS.BAIXA_ID%type;'+ #13 +
      '  vEmpresaId     CONTAS_RECEBER_BAIXAS.EMPRESA_ID%type;'+ #13 +
      '  vValorRetencao CONTAS_RECEBER_BAIXAS.VALOR_RETENCAO%type;'+ #13 +
      #13 +
      '  vValorBaixarCredito   number default 0;'+ #13 +
      '  vValorRestante        number default 0;'+ #13 +
      #13 +
      '  vBaixaCreditoPagarId  CONTAS_PAGAR_BAIXAS.BAIXA_ID%type;'+ #13 +
      '  vValorDinheiro        CONTAS_RECEBER_BAIXAS.VALOR_DINHEIRO%type;'+ #13 +
      '  vValorCheque          CONTAS_RECEBER_BAIXAS.VALOR_CHEQUE%type;'+ #13 +
      '  vValorCartaoDeb       CONTAS_RECEBER_BAIXAS.VALOR_CARTAO_DEBITO%type;'+ #13 +
      '  vValorCartaoCred      CONTAS_RECEBER_BAIXAS.VALOR_CARTAO_CREDITO%type;'+ #13 +
      '  vValorCobranca        CONTAS_RECEBER_BAIXAS.VALOR_COBRANCA%type;'+ #13 +
      '  vValorCredito         CONTAS_RECEBER_BAIXAS.VALOR_CREDITO%type;'+ #13 +
      '  vValorTotal           CONTAS_RECEBER_BAIXAS.VALOR_LIQUIDO%type;'+ #13 +
      #13 +
      '  cursor cCheques is'+ #13 +
      '  select'+ #13 +
      '    CHQ.COBRANCA_ID,'+ #13 +
      '    CHQ.DATA_VENCIMENTO,'+ #13 +
      '    CHQ.PARCELA,'+ #13 +
      '    CHQ.NUMERO_PARCELAS,'+ #13 +
      '    CHQ.BANCO,'+ #13 +
      '    CHQ.AGENCIA,     '+ #13 +
      '    CHQ.CONTA_CORRENTE,'+ #13 +
      '    CHQ.NOME_EMITENTE,'+ #13 +
      '    CHQ.TELEFONE_EMITENTE,'+ #13 +
      '    CHQ.VALOR_CHEQUE,'+ #13 +
      '    CHQ.NUMERO_CHEQUE,'+ #13 +
      '    CHQ.ITEM_ID'+ #13 +
      '  from'+ #13 +
      '    CONTAS_REC_BAIXAS_PAGTOS_CHQ CHQ'+ #13 +
      '  where CHQ.BAIXA_ID = iBAIXA_RECEBER_ID;'+ #13 +
      #13 +
      #13 +
      '  cursor cCobrancas(pTIPO in string) is'+ #13 +
      '  select'+ #13 +
      '    PAG.COBRANCA_ID,'+ #13 +
      '    PAG.ITEM_ID,'+ #13 +
      '    PAG.VALOR,'+ #13 +
      '    PAG.DATA_VENCIMENTO,'+ #13 +
      '    PAG.PARCELA,'+ #13 +
      '    PAG.NUMERO_PARCELAS,'+ #13 +
      '    TIP.BOLETO_BANCARIO,'+ #13 +
      '    PAG.NSU_TEF,'+ #13 +
      '    TIP.PORTADOR_ID'+ #13 +
      '  from'+ #13 +
      '    CONTAS_REC_BAIXAS_PAGAMENTOS PAG'+ #13 +
      #13 +
      '  join TIPOS_COBRANCA TIP'+ #13 +
      '  on PAG.COBRANCA_ID = TIP.COBRANCA_ID    '+ #13 +
      '  and TIP.FORMA_PAGAMENTO = pTIPO'+ #13 +
      #13 +
      '  where PAG.BAIXA_ID = iBAIXA_RECEBER_ID  '+ #13 +
      '  order by '+ #13 +
      '		COBRANCA_ID,'+ #13 +
      '		PARCELA,'+ #13 +
      '		DATA_VENCIMENTO, '+ #13 +
      '		ITEM_ID;'+ #13 +
      #13 +
      #13 +
      '  cursor cCartoes is'+ #13 +
      '  select'+ #13 +
      '    ITEM_ID,'+ #13 +
      '    NSU_TEF,'+ #13 +
      '    NUMERO_CARTAO,'+ #13 +
      '    CODIGO_AUTORIZACAO'+ #13 +
      '  from'+ #13 +
      '    CONTAS_REC_BAIXAS_PAGAMENTOS'+ #13 +
      '  where BAIXA_ID = iBAIXA_RECEBER_ID'+ #13 +
      '  and TIPO = ''CR'';'+ #13 +
      #13 +
      '  cursor cCreditos is'+ #13 +
      '  select'+ #13 +
      '    BAI.PAGAR_ID,'+ #13 +
      '    PAG.VALOR_DOCUMENTO - PAG.VALOR_DESCONTO - PAG.VALOR_ADIANTADO as VALOR_LIQUIDO'+ #13 +
      '  from'+ #13 +
      '    CONTAS_REC_BAIXAS_CREDITOS BAI'+ #13 +
      #13 +
      '  inner join CONTAS_PAGAR PAG'+ #13 +
      '  on BAI.PAGAR_ID = PAG.PAGAR_ID'+ #13 +
      #13 +
      '  where BAI.BAIXA_ID = iBAIXA_RECEBER_ID;'+ #13 +
      #13 +
      'begin'+ #13 +
      #13 +
      '  select'+ #13 +
      '    EMPRESA_ID,'+ #13 +
      '    CADASTRO_ID,'+ #13 +
      '    VALOR_DINHEIRO,'+ #13 +
      '    VALOR_CHEQUE,'+ #13 +
      '    VALOR_CARTAO_DEBITO,'+ #13 +
      '    VALOR_CARTAO_CREDITO,'+ #13 +
      '    VALOR_COBRANCA,'+ #13 +
      '    VALOR_CREDITO,'+ #13 +
      '    VALOR_LIQUIDO,'+ #13 +
      '    VALOR_RETENCAO'+ #13 +
      '  into'+ #13 +
      '    vEmpresaId,'+ #13 +
      '    vCadastroId,'+ #13 +
      '    vValorDinheiro,'+ #13 +
      '    vValorCheque,'+ #13 +
      '    vValorCartaoDeb,'+ #13 +
      '    vValorCartaoCred,'+ #13 +
      '    vValorCobranca,'+ #13 +
      '    vValorCredito,'+ #13 +
      '    vValorTotal,'+ #13 +
      '    vValorRetencao'+ #13 +
      '  from'+ #13 +
      '    CONTAS_RECEBER_BAIXAS'+ #13 +
      '  where BAIXA_ID = iBAIXA_RECEBER_ID;'+ #13 +
      #13 +
      '  for xCheques in cCheques loop      '+ #13 +
      '    select SEQ_RECEBER_ID.nextval'+ #13 +
      '    into vReceberId'+ #13 +
      '    from dual;'+ #13 +
      #13 +
      '    insert into CONTAS_RECEBER('+ #13 +
      '      RECEBER_ID,'+ #13 +
      '      CADASTRO_ID,'+ #13 +
      '      EMPRESA_ID,      '+ #13 +
      '      COBRANCA_ID,'+ #13 +
      '      PORTADOR_ID,'+ #13 +
      '      PLANO_FINANCEIRO_ID,      '+ #13 +
      '      DOCUMENTO,'+ #13 +
      '      BANCO,'+ #13 +
      '      AGENCIA,'+ #13 +
      '      CONTA_CORRENTE,'+ #13 +
      '      NUMERO_CHEQUE,'+ #13 +
      '      NOME_EMITENTE,'+ #13 +
      '      TELEFONE_EMITENTE,'+ #13 +
      '      DATA_EMISSAO,'+ #13 +
      '      DATA_VENCIMENTO,'+ #13 +
      '      DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '      VALOR_DOCUMENTO,'+ #13 +
      '      STATUS,'+ #13 +
      '      PARCELA,'+ #13 +
      '      NUMERO_PARCELAS,'+ #13 +
      '      ORIGEM,'+ #13 +
      '      BAIXA_ORIGEM_ID'+ #13 +
      '    )values('+ #13 +
      '      vReceberId,'+ #13 +
      '      vCadastroId,'+ #13 +
      '      vEmpresaId,'+ #13 +
      '      xCheques.COBRANCA_ID,'+ #13 +
      '      ''9998'','+ #13 +
      '      ''1.001.002'',      '+ #13 +
      '      ''BXR-'' || iBAIXA_RECEBER_ID || xCheques.ITEM_ID || ''/CHQ'','+ #13 +
      '      xCheques.BANCO,'+ #13 +
      '      xCheques.AGENCIA,'+ #13 +
      '      xCheques.CONTA_CORRENTE,'+ #13 +
      '      xCheques.NUMERO_CHEQUE,'+ #13 +
      '      xCheques.NOME_EMITENTE,'+ #13 +
      '      xCheques.TELEFONE_EMITENTE,'+ #13 +
      '      trunc(sysdate),'+ #13 +
      '      xCheques.DATA_VENCIMENTO,'+ #13 +
      '      xCheques.DATA_VENCIMENTO,'+ #13 +
      '      xCheques.VALOR_CHEQUE,'+ #13 +
      '      ''A'','+ #13 +
      '      xCheques.PARCELA,'+ #13 +
      '      xCheques.NUMERO_PARCELAS,'+ #13 +
      '      ''BXR'','+ #13 +
      '      iBAIXA_RECEBER_ID'+ #13 +
      '    );    '+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  for xCobrancas in cCobrancas(''COB'') loop'+ #13 +
      '    select SEQ_RECEBER_ID.nextval'+ #13 +
      '    into vReceberId'+ #13 +
      '    from dual;'+ #13 +
      #13 +
      '    insert into CONTAS_RECEBER('+ #13 +
      '      RECEBER_ID,'+ #13 +
      '      CADASTRO_ID,'+ #13 +
      '      EMPRESA_ID,      '+ #13 +
      '      COBRANCA_ID,'+ #13 +
      '      PORTADOR_ID,'+ #13 +
      '      PLANO_FINANCEIRO_ID,        '+ #13 +
      '      DOCUMENTO,'+ #13 +
      '      DATA_EMISSAO,'+ #13 +
      '      DATA_VENCIMENTO,'+ #13 +
      '      DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '      VALOR_DOCUMENTO,'+ #13 +
      '      STATUS,'+ #13 +
      '      PARCELA,'+ #13 +
      '      NUMERO_PARCELAS,'+ #13 +
      '      ORIGEM,'+ #13 +
      '      BAIXA_ORIGEM_ID'+ #13 +
      '    )values('+ #13 +
      '      vReceberId,'+ #13 +
      '      vCadastroId,'+ #13 +
      '      vEmpresaId,'+ #13 +
      '      xCobrancas.COBRANCA_ID,'+ #13 +
      '      xCobrancas.PORTADOR_ID,'+ #13 +
      '      case when xCobrancas.BOLETO_BANCARIO = ''N'' then ''1.001.005'' else ''1.001.006'' end,            '+ #13 +
      '      ''BXR-'' || iBAIXA_RECEBER_ID || xCobrancas.ITEM_ID || case when xCobrancas.BOLETO_BANCARIO = ''S'' then ''/BOL'' else ''/COB'' end,'+ #13 +
      '      trunc(sysdate),'+ #13 +
      '      xCobrancas.DATA_VENCIMENTO,'+ #13 +
      '      xCobrancas.DATA_VENCIMENTO,'+ #13 +
      '      xCobrancas.VALOR,'+ #13 +
      '      ''A'','+ #13 +
      '      xCobrancas.PARCELA,'+ #13 +
      '      xCobrancas.NUMERO_PARCELAS,'+ #13 +
      '      ''BXR'','+ #13 +
      '      iBAIXA_RECEBER_ID'+ #13 +
      '    );'+ #13 +
      '  end loop;  '+ #13 +
      #13 +
      '  for xCartoes in cCartoes loop'+ #13 +
      '    GERAR_CARTOES_RECEBER_BAIXA(iBAIXA_RECEBER_ID, xCartoes.ITEM_ID, xCartoes.NSU_TEF, xCartoes.CODIGO_AUTORIZACAO, xCartoes.NUMERO_CARTAO);'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  ATUALIZAR_RETENCOES_CONTAS_REC(iBAIXA_RECEBER_ID, ''BXR'');'+ #13 +
      #13 +
      '  if vValorCredito > 0 then'+ #13 +
      '    vValorRestante := vValorTotal - (vValorDinheiro + vValorCheque + vValorCartaoDeb + vValorCartaoCred + vValorCobranca);'+ #13 +
      '    for xCreditos in cCreditos loop'+ #13 +
      '      if xCreditos.VALOR_LIQUIDO >= vValorRestante then'+ #13 +
      '        vValorBaixarCredito := vValorRestante;'+ #13 +
      '        vValorRestante := 0;'+ #13 +
      '      else'+ #13 +
      '        vValorBaixarCredito := xCreditos.VALOR_LIQUIDO;'+ #13 +
      '        vValorRestante := vValorRestante - vValorBaixarCredito;'+ #13 +
      '      end if;'+ #13 +
      #13 +
      '      BAIXAR_CREDITO_PAGAR(xCreditos.PAGAR_ID, iBAIXA_RECEBER_ID, ''BCR'', vValorBaixarCredito, vBaixaCreditoPagarId, vBaixaCreditoPagarId);'+ #13 +
      #13 +
      '      if vValorRestante = 0 then'+ #13 +
      '        exit;'+ #13 +
      '      end if;'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      'end CONSOLIDAR_BAIXA_CONTAS_REC;'+ #13
    );
end;

function AjustarProcedureRECEBER_PEDIDO: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure RECEBER_PEDIDO('+ #13 +
      '  iORCAMENTO_ID        in positiven,'+ #13 +
      '  iTURNO_ID            in positiven,'+ #13 +
      '  iVALOR_TROCO         in number,'+ #13 +
      '  iGERAR_CREDITO_TROCO in string,'+ #13 +
      '  iTIPO_NOTA_GERAR     in string'+ #13 +
      ')'+ #13 +
      'is'+ #13 +
      '  i                       naturaln default 0;'+ #13 +
      '  vRetiradaId             RETIRADAS.RETIRADA_ID%type;'+ #13 +
      '  vReceberId              CONTAS_RECEBER.RECEBER_ID%type;'+ #13 +
      '  vDadosPagamentoPedido   RecordsVendas.RecPedidosPagamentos;'+ #13 +
      #13 +
      '  vPagamentoInconsistente char(1);'+ #13 +
      '  vBaixaCreditoPagarId    CONTAS_PAGAR_BAIXAS.BAIXA_ID%type;'+ #13 +
      '  vValorTotal             ORCAMENTOS.VALOR_TOTAL%type;'+ #13 +
      '  vValorBaixarCredito     number default 0;'+ #13 +
      '  vValorRestante          number default 0;'+ #13 +
      '  vReceberNaEntrega       ORCAMENTOS.RECEBER_NA_ENTREGA%type;'+ #13 +
      #13 +
      '  cursor cCheques is'+ #13 +
      '  select'+ #13 +
      '    COBRANCA_ID,'+ #13 +
      '    ITEM_ID,'+ #13 +
      '    DATA_VENCIMENTO,'+ #13 +
      '    BANCO,'+ #13 +
      '    AGENCIA,'+ #13 +
      '    CONTA_CORRENTE,'+ #13 +
      '    NUMERO_CHEQUE,'+ #13 +
      '    VALOR_CHEQUE,'+ #13 +
      '    NOME_EMITENTE,'+ #13 +
      '    TELEFONE_EMITENTE,'+ #13 +
      '    PARCELA,'+ #13 +
      '    NUMERO_PARCELAS'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS_PAGAMENTOS_CHEQUES'+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '  order by '+ #13 +
      '		COBRANCA_ID, '+ #13 +
      '		PARCELA,'+ #13 +
      '		DATA_VENCIMENTO, '+ #13 +
      '		ITEM_ID;'+ #13 +
      #13 +
      '  cursor cCobrancas(pTIPO in string) is'+ #13 +
      '  select'+ #13 +
      '    PAG.COBRANCA_ID,'+ #13 +
      '    PAG.ITEM_ID,'+ #13 +
      '    PAG.VALOR,'+ #13 +
      '    PAG.DATA_VENCIMENTO,'+ #13 +
      '    PAG.PARCELA,'+ #13 +
      '    PAG.NUMERO_PARCELAS,'+ #13 +
      '    TIP.BOLETO_BANCARIO,'+ #13 +
      '    PAG.NSU_TEF,'+ #13 +
      '    TIP.PORTADOR_ID,'+ #13 +
      '    PAG.NUMERO_CARTAO,'+ #13 +
      '    PAG.CODIGO_AUTORIZACAO'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS_PAGAMENTOS PAG'+ #13 +
      #13 +
      '  join TIPOS_COBRANCA TIP'+ #13 +
      '  on PAG.COBRANCA_ID = TIP.COBRANCA_ID    '+ #13 +
      '  and TIP.FORMA_PAGAMENTO = pTIPO'+ #13 +
      #13 +
      '  where PAG.ORCAMENTO_ID = iORCAMENTO_ID  '+ #13 +
      '  order by '+ #13 +
      '		COBRANCA_ID,'+ #13 +
      '		PARCELA,'+ #13 +
      '		DATA_VENCIMENTO, '+ #13 +
      '		ITEM_ID;'+ #13 +
      #13 +
      '  cursor cCreditos is'+ #13 +
      '  select'+ #13 +
      '    ORC.PAGAR_ID,'+ #13 +
      '    PAG.VALOR_DOCUMENTO - PAG.VALOR_DESCONTO - PAG.VALOR_ADIANTADO as VALOR_LIQUIDO'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS_CREDITOS ORC'+ #13 +
      #13 +
      '  inner join CONTAS_PAGAR PAG'+ #13 +
      '  on ORC.PAGAR_ID = PAG.PAGAR_ID'+ #13 +
      #13 +
      '  where ORC.ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '  and ORC.MOMENTO_USO = ''R'';'+ #13 +
      #13 +
      'begin'+ #13 +
      #13 +
      '  begin'+ #13 +
      '    select'+ #13 +
      '      VALOR_DINHEIRO,'+ #13 +
      '      VALOR_CARTAO_DEBITO,'+ #13 +
      '      VALOR_CARTAO_CREDITO,'+ #13 +
      '      VALOR_CREDITO,'+ #13 +
      '      VALOR_COBRANCA,'+ #13 +
      '      VALOR_CHEQUE,'+ #13 +
      '      VALOR_FINANCEIRA,'+ #13 +
      '      VALOR_ACUMULATIVO,'+ #13 +
      '      STATUS,'+ #13 +
      '      VENDEDOR_ID,'+ #13 +
      '      EMPRESA_ID,'+ #13 +
      '      CLIENTE_ID,'+ #13 +
      '      RECEBER_NA_ENTREGA,'+ #13 +
      '      VALOR_TOTAL,'+ #13 +
      '      case when VALOR_TOTAL < VALOR_TOTAL_PRODUTOS + VALOR_OUTRAS_DESPESAS + VALOR_FRETE - VALOR_DESCONTO then ''S'' else ''N'' end'+ #13 +
      '    into'+ #13 +
      '      vDadosPagamentoPedido.VALOR_DINHEIRO,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_CARTAO_DEBITO,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_CARTAO_CREDITO,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_CREDITO,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_COBRANCA,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_CHEQUE,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_FINANCEIRA,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_ACUMULATIVO,'+ #13 +
      '      vDadosPagamentoPedido.STATUS,'+ #13 +
      '      vDadosPagamentoPedido.VENDEDOR_ID,'+ #13 +
      '      vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
      '      vDadosPagamentoPedido.CLIENTE_ID,'+ #13 +
      '      vReceberNaEntrega,'+ #13 +
      '      vValorTotal,'+ #13 +
      '      vPagamentoInconsistente'+ #13 +
      '    from'+ #13 +
      '      ORCAMENTOS'+ #13 +
      '    where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      '  exception'+ #13 +
      '    when others then'+ #13 +
      '      ERRO(''Não foi encontrado os valores de pagamento para o orçamento '' || iORCAMENTO_ID || ''!'' || chr(13) || chr(10) || sqlerrm);'+ #13 +
      '  end;'+ #13 +
      #13 +
      '  if vDadosPagamentoPedido.STATUS = ''RE'' then'+ #13 +
      '    ERRO(''Este orçamento já foi recebido!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if vDadosPagamentoPedido.STATUS not in(''VR'', ''VE'') then'+ #13 +
      '    ERRO(''Este pedido não está aguardando recebimento, verifique!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  /* Apagando as previsões dos recebimentos na entrega */'+ #13 +
      '  if vDadosPagamentoPedido.STATUS = ''VE'' then'+ #13 +
      '    delete from CONTAS_RECEBER'+ #13 +
      '    where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '    and COBRANCA_ID = SESSAO.PARAMETROS_GERAIS.TIPO_COB_RECEB_ENTREGA_ID;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  /* Inserindo cheques no financeiro */'+ #13 +
      '  if vDadosPagamentoPedido.VALOR_CHEQUE > 0 then'+ #13 +
      '    for vCheques in cCheques loop'+ #13 +
      '      select SEQ_RECEBER_ID.nextval'+ #13 +
      '      into vReceberId'+ #13 +
      '      from dual;'+ #13 +
      #13 +
      '      insert into CONTAS_RECEBER('+ #13 +
      '        RECEBER_ID,'+ #13 +
      '        CADASTRO_ID,'+ #13 +
      '        EMPRESA_ID,'+ #13 +
      '        ORCAMENTO_ID,'+ #13 +
      '        COBRANCA_ID,'+ #13 +
      '        PORTADOR_ID,'+ #13 +
      '        PLANO_FINANCEIRO_ID,'+ #13 +
      '        TURNO_ID,'+ #13 +
      '        VENDEDOR_ID,'+ #13 +
      '        DOCUMENTO,'+ #13 +
      '        BANCO,'+ #13 +
      '        AGENCIA,'+ #13 +
      '        CONTA_CORRENTE,'+ #13 +
      '        NUMERO_CHEQUE,'+ #13 +
      '        NOME_EMITENTE,'+ #13 +
      '        TELEFONE_EMITENTE,'+ #13 +
      '        DATA_EMISSAO,'+ #13 +
      '        DATA_VENCIMENTO,'+ #13 +
      '        DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '        VALOR_DOCUMENTO,'+ #13 +
      '        STATUS,'+ #13 +
      '        PARCELA,'+ #13 +
      '        NUMERO_PARCELAS,'+ #13 +
      '        ORIGEM'+ #13 +
      '      )values('+ #13 +
      '        vReceberId,'+ #13 +
      '        vDadosPagamentoPedido.CLIENTE_ID,'+ #13 +
      '        vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
      '        iORCAMENTO_ID,'+ #13 +
      '        vCheques.COBRANCA_ID,'+ #13 +
      '        ''9998'','+ #13 +
      '        ''1.001.002'','+ #13 +
      '        iTURNO_ID,'+ #13 +
      '        vDadosPagamentoPedido.VENDEDOR_ID,'+ #13 +
      '        ''PED-'' || iORCAMENTO_ID || vCheques.ITEM_ID || ''/CHQ'','+ #13 +
      '        vCheques.BANCO,'+ #13 +
      '        vCheques.AGENCIA,'+ #13 +
      '        vCheques.CONTA_CORRENTE,'+ #13 +
      '        vCheques.NUMERO_CHEQUE,'+ #13 +
      '        vCheques.NOME_EMITENTE,'+ #13 +
      '        vCheques.TELEFONE_EMITENTE,'+ #13 +
      '        trunc(sysdate),'+ #13 +
      '        vCheques.DATA_VENCIMENTO,'+ #13 +
      '        vCheques.DATA_VENCIMENTO,'+ #13 +
      '        vCheques.VALOR_CHEQUE,'+ #13 +
      '        ''A'','+ #13 +
      '        vCheques.PARCELA,'+ #13 +
      '        vCheques.NUMERO_PARCELAS,'+ #13 +
      '        ''ORC'''+ #13 +
      '      );'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if vDadosPagamentoPedido.VALOR_COBRANCA > 0 then'+ #13 +
      '    for vCobrancas in cCobrancas(''COB'') loop'+ #13 +
      '      select SEQ_RECEBER_ID.nextval'+ #13 +
      '      into vReceberId'+ #13 +
      '      from dual;'+ #13 +
      #13 +
      '      insert into CONTAS_RECEBER('+ #13 +
      '        RECEBER_ID,'+ #13 +
      '        CADASTRO_ID,'+ #13 +
      '        EMPRESA_ID,'+ #13 +
      '        ORCAMENTO_ID,'+ #13 +
      '        COBRANCA_ID,'+ #13 +
      '        PORTADOR_ID,'+ #13 +
      '        PLANO_FINANCEIRO_ID,'+ #13 +
      '        TURNO_ID,'+ #13 +
      '        VENDEDOR_ID,'+ #13 +
      '        DOCUMENTO,'+ #13 +
      '        DATA_EMISSAO,'+ #13 +
      '        DATA_VENCIMENTO,'+ #13 +
      '        DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '        VALOR_DOCUMENTO,'+ #13 +
      '        STATUS,'+ #13 +
      '        PARCELA,'+ #13 +
      '        NUMERO_PARCELAS,'+ #13 +
      '        ORIGEM'+ #13 +
      '      )values('+ #13 +
      '        vReceberId,'+ #13 +
      '        vDadosPagamentoPedido.CLIENTE_ID,'+ #13 +
      '        vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
      '        iORCAMENTO_ID,'+ #13 +
      '        vCobrancas.COBRANCA_ID,'+ #13 +
      '        vCobrancas.PORTADOR_ID,'+ #13 +
      '        case when vCobrancas.BOLETO_BANCARIO = ''N'' then ''1.001.005'' else ''1.001.006'' end,'+ #13 +
      '        iTURNO_ID,'+ #13 +
      '        vDadosPagamentoPedido.VENDEDOR_ID,'+ #13 +
      '        ''PED-'' || iORCAMENTO_ID || vCobrancas.ITEM_ID || case when vCobrancas.BOLETO_BANCARIO = ''S'' then ''/BOL'' else ''/COB'' end,'+ #13 +
      '        trunc(sysdate),'+ #13 +
      '        vCobrancas.DATA_VENCIMENTO,'+ #13 +
      '        vCobrancas.DATA_VENCIMENTO,'+ #13 +
      '        vCobrancas.VALOR,'+ #13 +
      '        ''A'','+ #13 +
      '        vCobrancas.PARCELA,'+ #13 +
      '        vCobrancas.NUMERO_PARCELAS,'+ #13 +
      '        ''ORC'''+ #13 +
      '      );'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if vDadosPagamentoPedido.VALOR_ACUMULATIVO > 0 then'+ #13 +
      '    AJUSTAR_CONTAS_REC_ACUMULATIVO(iORCAMENTO_ID);'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if iGERAR_CREDITO_TROCO = ''S'' and iVALOR_TROCO > 0 then'+ #13 +
      '    GERAR_CREDITO_TROCO(iORCAMENTO_ID, ''ORC'', vDadosPagamentoPedido.CLIENTE_ID, iVALOR_TROCO);'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if vReceberNaEntrega = ''S'' and vDadosPagamentoPedido.VALOR_CREDITO > 0 then'+ #13 +
      '    vValorRestante :='+ #13 +
      '      vValorTotal - ('+ #13 +
      '        vDadosPagamentoPedido.VALOR_DINHEIRO +'+ #13 +
      '        vDadosPagamentoPedido.VALOR_CHEQUE +'+ #13 +
      '        vDadosPagamentoPedido.VALOR_CARTAO_DEBITO +'+ #13 +
      '        vDadosPagamentoPedido.VALOR_CARTAO_CREDITO +'+ #13 +
      '        vDadosPagamentoPedido.VALOR_COBRANCA +'+ #13 +
      '        vDadosPagamentoPedido.VALOR_FINANCEIRA'+ #13 +
      '      );'+ #13 +
      #13 +
      '    for xCreditos in cCreditos loop'+ #13 +
      '      if xCreditos.VALOR_LIQUIDO >= vValorRestante then'+ #13 +
      '        vValorBaixarCredito := vValorRestante;'+ #13 +
      '        vValorRestante := 0;'+ #13 +
      '      else'+ #13 +
      '        vValorBaixarCredito := xCreditos.VALOR_LIQUIDO;'+ #13 +
      '        vValorRestante := vValorRestante - vValorBaixarCredito;'+ #13 +
      '      end if;'+ #13 +
      #13 +
      '      BAIXAR_CREDITO_PAGAR(xCreditos.PAGAR_ID, iORCAMENTO_ID, ''ORC'', vValorBaixarCredito, vBaixaCreditoPagarId, vBaixaCreditoPagarId);'+ #13 +
      #13 +
      '      if vValorRestante = 0 then'+ #13 +
      '        exit;'+ #13 +
      '      end if;'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  update ORCAMENTOS set'+ #13 +
      '    VALOR_TROCO = case when iGERAR_CREDITO_TROCO = ''N'' then iVALOR_TROCO else 0 end,'+ #13 +
      '    TURNO_ID = iTURNO_ID,'+ #13 +
      '    STATUS = ''RE'''+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      #13 +
      '  /* Não alterar este processo!!! Ele deve ser depois do recebimento */'+ #13 +
      '  if vDadosPagamentoPedido.VALOR_CARTAO_DEBITO + vDadosPagamentoPedido.VALOR_CARTAO_CREDITO > 0 then'+ #13 +
      '    for vCobrancas in cCobrancas(''CRT'') loop'+ #13 +
      '      GERAR_CARTOES_RECEBER(iORCAMENTO_ID, vCobrancas.ITEM_ID, vCobrancas.NSU_TEF, vCobrancas.CODIGO_AUTORIZACAO, vCobrancas.NUMERO_CARTAO);'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  select'+ #13 +
      '    max(RETIRADA_ID)'+ #13 +
      '  into'+ #13 +
      '    vRetiradaId'+ #13 +
      '  from'+ #13 +
      '    RETIRADAS'+ #13 +
      '  where TIPO_MOVIMENTO = ''A'''+ #13 +
      '  and ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      #13 +
      '  if vRetiradaId is not null then'+ #13 +
      '    update RETIRADAS set'+ #13 +
      '      TIPO_NOTA_GERAR = nvl(iTIPO_NOTA_GERAR, ''NI'')'+ #13 +
      '    where RETIRADA_ID = vRetiradaId;'+ #13 +
      #13 +
      '    /* Caso o cliente trabalhe com confirmação de saídas de mercadorias */'+ #13 +
      '    if SESSAO.PARAMETROS_EMPRESA_LOGADA.CONFIRMAR_SAIDA_PRODUTOS = ''N'' then'+ #13 +
      '      CONFIRMAR_RETIRADA(vRetiradaId, ''AUTOMATICO'');'+ #13 +
      '    end if;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  ATUALIZAR_RETENCOES_CONTAS_REC(iORCAMENTO_ID, ''ORC'');'+ #13 +
      #13 +
      'end RECEBER_PEDIDO;'+ #13
    );
end;

function AjustarProcedureBAIXAR_CREDITO_PAGAR: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure BAIXAR_CREDITO_PAGAR('+ #13 +
      '  iPAGAR_ID       in number,'+ #13 +
      '  iMOVIMENTO_ID   in number,'+ #13 +
      '  iTIPO_MOVIMENTO in string,'+ #13 +
      '  iVALOR_UTILIZAR in number,'+ #13 +
      '  iBAIXA_ID       in number, -- Este código vem deste próprio procedimento'+ #13 +
      '  oBAIXA_ID       out number'+ #13 +
      ')'+ #13 +
      'is'+ #13 +
      '  vPagarId       CONTAS_PAGAR.PAGAR_ID%type;'+ #13 +
      '  vBaixaId       CONTAS_PAGAR_BAIXAS.BAIXA_ID%type;'+ #13 +
      '  vValorCredito  CONTAS_PAGAR.VALOR_DOCUMENTO%type;'+ #13 +
      '  vCadastroId    CONTAS_PAGAR.CADASTRO_ID%type;'+ #13 +
      '  vEmpresaId     ORCAMENTOS.EMPRESA_ID%type;'+ #13 +
      '  vObservacoes   CONTAS_PAGAR_BAIXAS.OBSERVACOES%type;'+ #13 +
      #13 +
      '  vQtde          number;'+ #13 +
      '  vStatus        CONTAS_PAGAR.STATUS%type;'+ #13 +
      '  vStatusPedido  ORCAMENTOS.STATUS%type;'+ #13 +
      '  vTipoBaixa     CONTAS_PAGAR_BAIXAS.TIPO%type;'+ #13 +
      '  vTurnoId       CONTAS_RECEBER_BAIXAS.TURNO_ID%type;'+ #13 +
      'begin'+ #13 +
      #13 +
      '  if iTIPO_MOVIMENTO not in(''ORC'', ''BCR'', ''ACU'') then'+ #13 +
      '    ERRO(''O tipo de baixa só pode ser "ORC", "ACU" ou "BCR"!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if iTIPO_MOVIMENTO = ''ORC'' then'+ #13 +
      '    select'+ #13 +
      '      EMPRESA_ID,'+ #13 +
      '      STATUS'+ #13 +
      '    into'+ #13 +
      '      vEmpresaId,'+ #13 +
      '      vStatusPedido'+ #13 +
      '    from'+ #13 +
      '      ORCAMENTOS'+ #13 +
      '    where ORCAMENTO_ID = iMOVIMENTO_ID;'+ #13 +
      #13 +
      '    vTipoBaixa := ''FPE'';'+ #13 +
      '    if vStatusPedido = ''VE'' then'+ #13 +
      '      vObservacoes := ''Baixa automática proveniente de uso no recebimento do pedido(Receber na entrega) '' || NFORMAT(iMOVIMENTO_ID, 0);'+ #13 +
      '    else'+ #13 +
      '      vObservacoes := ''Baixa automática proveniente de uso no fechamento do pedido '' || NFORMAT(iMOVIMENTO_ID, 0);'+ #13 +
      '    end if;'+ #13 +
      '  elsif iTIPO_MOVIMENTO = ''ACU'' then'+ #13 +
      '    select'+ #13 +
      '      EMPRESA_ID'+ #13 +
      '    into'+ #13 +
      '      vEmpresaId'+ #13 +
      '    from'+ #13 +
      '      ACUMULADOS'+ #13 +
      '    where ACUMULADO_ID = iMOVIMENTO_ID;'+ #13 +
      #13 +
      '    vTipoBaixa := ''FAC'';'+ #13 +
      '    vObservacoes := ''Baixa automática proveniente de uso no fechamento do acumulado '' || NFORMAT(iMOVIMENTO_ID, 0);'+ #13 +
      '  elsif iTIPO_MOVIMENTO = ''BCR'' then'+ #13 +
      '    select'+ #13 +
      '      EMPRESA_ID,'+ #13 +
      '      TURNO_ID'+ #13 +
      '    into'+ #13 +
      '      vEmpresaId,'+ #13 +
      '      vTurnoId'+ #13 +
      '    from'+ #13 +
      '      CONTAS_RECEBER_BAIXAS'+ #13 +
      '    where BAIXA_ID = iMOVIMENTO_ID;'+ #13 +
      #13 +
      '    vTipoBaixa := ''ECP'';'+ #13 +
      '    vObservacoes := ''Baixa automática proveniente de uso na baixa de contas a receber '' || NFORMAT(iMOVIMENTO_ID, 0);'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  select'+ #13 +
      '    VALOR_DOCUMENTO - VALOR_DESCONTO - VALOR_ADIANTADO,'+ #13 +
      '    CADASTRO_ID,'+ #13 +
      '    STATUS'+ #13 +
      '  into'+ #13 +
      '    vValorCredito,'+ #13 +
      '    vCadastroId,'+ #13 +
      '    vStatus'+ #13 +
      '  from'+ #13 +
      '    CONTAS_PAGAR'+ #13 +
      '  where PAGAR_ID = iPAGAR_ID;'+ #13 +
      #13 +
      '  if vStatus = ''B'' then'+ #13 +
      '    ERRO(''O crédito a pagar que está sendo utilizado já está baixado! Crédito: '' || iPAGAR_ID);'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  /* Verificando se já existe uma baixa, pode ser que esteja sendo utilizado mais de um crédito */'+ #13 +
      '  if iBAIXA_ID is not null then'+ #13 +
      '    vBaixaId := iBAIXA_ID;'+ #13 +
      #13 +
      '    update CONTAS_PAGAR_BAIXAS set'+ #13 +
      '      VALOR_TITULOS = VALOR_TITULOS + iVALOR_UTILIZAR,'+ #13 +
      '      VALOR_CREDITO = VALOR_CREDITO + case when vTipoBaixa = ''ECP'' then iVALOR_UTILIZAR else 0 end,'+ #13 +
      '      VALOR_TROCA = VALOR_TROCA + case when vTipoBaixa in(''FPE'', ''FAC'') then iVALOR_UTILIZAR else 0 end'+ #13 +
      '    where BAIXA_ID = vBaixaId;'+ #13 +
      '  else'+ #13 +
      '    vBaixaId := SEQ_CONTAS_PAGAR_BAIXAS.nextval;'+ #13 +
      #13 +
      '    insert into CONTAS_PAGAR_BAIXAS('+ #13 +
      '      BAIXA_ID,'+ #13 +
      '      EMPRESA_ID,'+ #13 +
      '      CADASTRO_ID,'+ #13 +
      '      VALOR_TITULOS,'+ #13 +
      '      VALOR_CREDITO,'+ #13 +
      '      VALOR_TROCA,'+ #13 +
      '      OBSERVACOES,'+ #13 +
      '      DATA_PAGAMENTO,'+ #13 +
      '      TIPO,'+ #13 +
      '      BAIXA_RECEBER_ORIGEM_ID,'+ #13 +
      '      ACUMULADO_ID,'+ #13 +
      '      ORCAMENTO_ID,'+ #13 +
      '      TURNO_ID'+ #13 +
      '    )values('+ #13 +
      '      vBaixaId,'+ #13 +
      '      vEmpresaId,'+ #13 +
      '      vCadastroId,'+ #13 +
      '      iVALOR_UTILIZAR,'+ #13 +
      '      case when vTipoBaixa = ''ECP'' then iVALOR_UTILIZAR else 0 end,'+ #13 +
      '      case when vTipoBaixa in(''FPE'', ''FAC'') then iVALOR_UTILIZAR else 0 end,'+ #13 +
      '      vObservacoes,'+ #13 +
      '      trunc(sysdate),'+ #13 +
      '      vTipoBaixa,'+ #13 +
      '      case when vTipoBaixa = ''ECP'' then iMOVIMENTO_ID else null end,'+ #13 +
      '      case when vTipoBaixa = ''FAC'' then iMOVIMENTO_ID else null end,'+ #13 +
      '      case when vTipoBaixa = ''FPE'' then iMOVIMENTO_ID else null end,'+ #13 +
      '      vTurnoId'+ #13 +
      '    );'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  insert into CONTAS_PAGAR_BAIXAS_ITENS('+ #13 +
      '    BAIXA_ID,'+ #13 +
      '    PAGAR_ID,'+ #13 +
      '    VALOR_DINHEIRO,'+ #13 +
      '    VALOR_TROCA'+ #13 +
      '  )values('+ #13 +
      '    vBaixaId,'+ #13 +
      '    iPAGAR_ID,'+ #13 +
      '    case when vTipoBaixa = ''ECP'' then iVALOR_UTILIZAR else 0 end,'+ #13 +
      '    case when vTipoBaixa in(''FPE'', ''FAC'') then iVALOR_UTILIZAR else 0 end'+ #13 +
      '  );'+ #13 +
      #13 +
      '  /* Se ainda for sobrar um restante, gerando um novo crédigo a pagar */'+ #13 +
      '  if iVALOR_UTILIZAR < vValorCredito then'+ #13 +
      '    vPagarId := SEQ_PAGAR_ID.nextval;'+ #13 +
      #13 +
      '    insert into CONTAS_PAGAR('+ #13 +
      '      PAGAR_ID,'+ #13 +
      '      CADASTRO_ID,'+ #13 +
      '      EMPRESA_ID,'+ #13 +
      '      COBRANCA_ID,'+ #13 +
      '      PORTADOR_ID,      '+ #13 +
      '      DOCUMENTO,'+ #13 +
      '      NUMERO_CHEQUE,      '+ #13 +
      '      PLANO_FINANCEIRO_ID,'+ #13 +
      '      CENTRO_CUSTO_ID,      '+ #13 +
      '      DATA_VENCIMENTO,'+ #13 +
      '      DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '      VALOR_DOCUMENTO,'+ #13 +
      '      STATUS,'+ #13 +
      '      PARCELA,'+ #13 +
      '      NUMERO_PARCELAS,'+ #13 +
      '      ORIGEM,'+ #13 +
      '      BAIXA_ORIGEM_ID'+ #13 +
      '    )'+ #13 +
      '    select'+ #13 +
      '      vPagarId,'+ #13 +
      '      CADASTRO_ID,'+ #13 +
      '      EMPRESA_ID,'+ #13 +
      '      COBRANCA_ID,'+ #13 +
      '      PORTADOR_ID,      '+ #13 +
      '      ''CRE'' || vPagarId,'+ #13 +
      '      NUMERO_CHEQUE,      '+ #13 +
      '      PLANO_FINANCEIRO_ID,'+ #13 +
      '      CENTRO_CUSTO_ID,      '+ #13 +
      '      DATA_VENCIMENTO,'+ #13 +
      '      DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '      vValorCredito - iVALOR_UTILIZAR, -- Apenas o saldo restante'+ #13 +
      '      ''A'','+ #13 +
      '      PARCELA,'+ #13 +
      '      NUMERO_PARCELAS,'+ #13 +
      '      ''BCP'','+ #13 +
      '      vBaixaId'+ #13 +
      '    from'+ #13 +
      '      CONTAS_PAGAR'+ #13 +
      '    where PAGAR_ID = iPAGAR_ID;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  oBAIXA_ID := vBaixaId;'+ #13 +
      #13 +
      'end BAIXAR_CREDITO_PAGAR;'+ #13
    );
end;

function AdicionarColunaCONTAS_RECEBER: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'alter table CONTAS_RECEBER disable all triggers;'+ #13 +
      #13 +
      'alter table CONTAS_RECEBER'+ #13 +
      'add COMISSIONAR                char(1) default ''N'' not null;'+ #13 +
      #13 +
      'alter table CONTAS_RECEBER enable all triggers;'+ #13
    );
end;

function AjustarConstraintCK_CONTAS_REC_COMISSIONAR: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'alter table CONTAS_RECEBER'+ #13 +
      'add constraint CK_CONTAS_REC_COMISSIONAR'+ #13 +
      'check('+ #13 +
      '  COMISSIONAR = ''N'''+ #13 +
      '  or'+ #13 +
      '  COMISSIONAR = ''S'' and VENDEDOR_ID is not null'+ #13 +
      ');'+ #13
    );
end;

function AjustarViewVW_TIPOS_COB_PEDIDOS_COM_BAIXA: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace view VW_TIPOS_COB_PEDIDOS_COM_BAIXA'+ #13 +
      'as'+ #13 +
      'select'+ #13 +
      '  ORCAMENTO_ID,'+ #13 +
      '  sum(VALOR) as VALOR_SOMENTE_NA_BAIXA'+ #13 +
      'from'+ #13 +
      '  ('+ #13 +
      '  select'+ #13 +
      '    ORCAMENTO_ID,'+ #13 +
      '    COBRANCA_ID,'+ #13 +
      '    VALOR'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS_PAGAMENTOS'+ #13 +
      #13 +
      '  union all'+ #13 +
      #13 +
      '  select'+ #13 +
      '    ORCAMENTO_ID,'+ #13 +
      '    COBRANCA_ID,'+ #13 +
      '    VALOR_CHEQUE'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS_PAGAMENTOS_CHEQUES'+ #13 +
      ') MOV'+ #13 +
      #13 +
      'inner join TIPOS_COBRANCA TCO'+ #13 +
      'on MOV.COBRANCA_ID = TCO.COBRANCA_ID'+ #13 +
      #13 +
      'where TCO.TIPO_PAGAMENTO_COMISSAO = ''B'''+ #13 +
      #13 +
      'group by'+ #13 +
      '  ORCAMENTO_ID;'+ #13
    );
end;

function AjustarViewVW_COMISSAO_POR_FUNCIONARIOS: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace view VW_COMISSAO_POR_FUNCIONARIOS'+ #13 +
      'as'+ #13 +
      'select'+ #13 +
      '  ORC.ORCAMENTO_ID as ID,'+ #13 +
      '  ''ORC'' as TIPO,'+ #13 +
      '  FUN.FUNCIONARIO_ID,'+ #13 +
      '  FUN.NOME as NOME_FUNCIONARIO,'+ #13 +
      '  trunc(DATA_HORA_RECEBIMENTO) as DATA,'+ #13 +
      '  CAD.CADASTRO_ID,'+ #13 +
      '  CAD.NOME_FANTASIA as NOME_CLIENTE,'+ #13 +
      '  ORC.EMPRESA_ID,'+ #13 +
      '  ORC.VALOR_TOTAL as VALOR_VENDA,'+ #13 +
      #13 +
      '  sum(ITE.VALOR_TOTAL + ITE.VALOR_TOTAL_OUTRAS_DESPESAS - ITE.VALOR_TOTAL_DESCONTO) -'+ #13 +
      '  nvl(CBA.VALOR_SOMENTE_NA_BAIXA, 0) as VALOR_BASE_COMISSAO,'+ #13 +
      #13 +
      '  case when CON.TIPO = ''P'' then ''A prazo'' else ''A vista'' end as TIPO_COMISSAO,'+ #13 +
      #13 +
      '  sum('+ #13 +
      '    round('+ #13 +
      '      (ITE.VALOR_TOTAL + ITE.VALOR_TOTAL_OUTRAS_DESPESAS - ITE.VALOR_TOTAL_DESCONTO) *'+ #13 +
      '      case when CON.TIPO = ''A'' then'+ #13 +
      '        zvl(PRO.PERCENTUAL_COMISSAO_VISTA, FUN.PERCENTUAL_COMISSAO_A_VISTA)'+ #13 +
      '      else'+ #13 +
      '        zvl(PRO.PERCENTUAL_COMISSAO_PRAZO, FUN.PERCENTUAL_COMISSAO_A_PRAZO)'+ #13 +
      '      end * 0.01,'+ #13 +
      '      2'+ #13 +
      '    )'+ #13 +
      '  )'+ #13 +
      '  * zvl(nvl(CBA.VALOR_SOMENTE_NA_BAIXA, 0), sum(ITE.VALOR_TOTAL + ITE.VALOR_TOTAL_OUTRAS_DESPESAS - ITE.VALOR_TOTAL_DESCONTO)) / sum(ITE.VALOR_TOTAL + ITE.VALOR_TOTAL_OUTRAS_DESPESAS - ITE.VALOR_TOTAL_DESCONTO) as VALOR_COMISSAO'+ #13 +
      #13 +
      'from'+ #13 +
      '  FUNCIONARIOS FUN'+ #13 +
      #13 +
      'inner join ORCAMENTOS ORC'+ #13 +
      'on FUN.FUNCIONARIO_ID = ORC.VENDEDOR_ID'+ #13 +
      'and ORC.STATUS = ''RE'''+ #13 +
      #13 +
      'inner join ORCAMENTOS_ITENS ITE'+ #13 +
      'on ORC.ORCAMENTO_ID = ITE.ORCAMENTO_ID'+ #13 +
      #13 +
      'inner join PRODUTOS PRO'+ #13 +
      'on ITE.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
      'and PRO.SUJEITO_COMISSAO = ''S'''+ #13 +
      #13 +
      'inner join CADASTROS CAD'+ #13 +
      'on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
      #13 +
      'inner join CLIENTES CLI'+ #13 +
      'on CAD.CADASTRO_ID = CLI.CADASTRO_ID'+ #13 +
      'and CLI.NAO_GERAR_COMISSAO = ''N'''+ #13 +
      #13 +
      'inner join CONDICOES_PAGAMENTO CON'+ #13 +
      'on ORC.CONDICAO_ID = CON.CONDICAO_ID'+ #13 +
      'and CON.ACUMULATIVO = ''N'''+ #13 +
      #13 +
      'left join VW_TIPOS_COB_PEDIDOS_COM_BAIXA CBA'+ #13 +
      'on ORC.ORCAMENTO_ID = CBA.ORCAMENTO_ID'+ #13 +
      #13 +
      'group by'+ #13 +
      '  ORC.ORCAMENTO_ID,'+ #13 +
      '  FUN.FUNCIONARIO_ID,'+ #13 +
      '  FUN.NOME,'+ #13 +
      '  trunc(DATA_HORA_RECEBIMENTO),'+ #13 +
      '  CON.TIPO,'+ #13 +
      '  CAD.CADASTRO_ID,'+ #13 +
      '  CAD.NOME_FANTASIA,'+ #13 +
      '  ORC.EMPRESA_ID,'+ #13 +
      '  ORC.VALOR_TOTAL,'+ #13 +
      '  nvl(CBA.VALOR_SOMENTE_NA_BAIXA, 0)'+ #13 +
      #13 +
      'having sum(ITE.VALOR_TOTAL + ITE.VALOR_TOTAL_OUTRAS_DESPESAS - ITE.VALOR_TOTAL_DESCONTO) - nvl(CBA.VALOR_SOMENTE_NA_BAIXA, 0) > 0'+ #13 +
      #13 +
      'union all'+ #13 +
      #13 +
      'select'+ #13 +
      '  DEV.DEVOLUCAO_ID as ID,'+ #13 +
      '  ''DEV'' as TIPO,'+ #13 +
      '  FUN.FUNCIONARIO_ID,'+ #13 +
      '  FUN.NOME as NOME_FUNCIONARIO,'+ #13 +
      '  trunc(DEV.DATA_HORA_DEVOLUCAO) as DATA,'+ #13 +
      '  CAD.CADASTRO_ID,'+ #13 +
      '  CAD.NOME_FANTASIA as NOME_CLIENTE,'+ #13 +
      '  DEV.EMPRESA_ID,'+ #13 +
      '  ORC.VALOR_TOTAL as VALOR_VENDA,'+ #13 +
      '  sum((OIT.VALOR_TOTAL + OIT.VALOR_TOTAL_OUTRAS_DESPESAS - OIT.VALOR_TOTAL_DESCONTO) / OIT.QUANTIDADE * (ITE.DEVOLVIDOS_ENTREGUES + ITE.DEVOLVIDOS_PENDENCIAS + ITE.DEVOLVIDOS_SEM_PREVISAO)) as VALOR_BASE_COMISSAO,'+ #13 +
      '  decode(CON.TIPO, ''P'', ''A prazo'', ''A'', ''A vista'', '''') as TIPO_COMISSAO,'+ #13 +
      '  sum('+ #13 +
      '    round('+ #13 +
      '      (OIT.VALOR_TOTAL + OIT.VALOR_TOTAL_OUTRAS_DESPESAS - OIT.VALOR_TOTAL_DESCONTO) / OIT.QUANTIDADE * (ITE.DEVOLVIDOS_ENTREGUES + ITE.DEVOLVIDOS_PENDENCIAS + ITE.DEVOLVIDOS_SEM_PREVISAO) *'+ #13 +
      '      case when CON.TIPO = ''A'' then'+ #13 +
      '        zvl(PRO.PERCENTUAL_COMISSAO_VISTA, FUN.PERCENTUAL_COMISSAO_A_VISTA)'+ #13 +
      '      else'+ #13 +
      '        zvl(PRO.PERCENTUAL_COMISSAO_PRAZO, FUN.PERCENTUAL_COMISSAO_A_PRAZO)'+ #13 +
      '      end * 0.01,'+ #13 +
      '      2'+ #13 +
      '    )'+ #13 +
      '  ) as VALOR_COMISSAO'+ #13 +
      #13 +
      'from'+ #13 +
      '  DEVOLUCOES DEV'+ #13 +
      #13 +
      'inner join DEVOLUCOES_ITENS ITE'+ #13 +
      'on DEV.DEVOLUCAO_ID = ITE.DEVOLUCAO_ID'+ #13 +
      #13 +
      'inner join ORCAMENTOS ORC'+ #13 +
      'on DEV.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
      #13 +
      'inner join ORCAMENTOS_ITENS OIT'+ #13 +
      'on ORC.ORCAMENTO_ID = OIT.ORCAMENTO_ID'+ #13 +
      'and ITE.ITEM_ID = OIT.ITEM_ID'+ #13 +
      #13 +
      'inner join CONDICOES_PAGAMENTO CON'+ #13 +
      'on ORC.CONDICAO_ID = CON.CONDICAO_ID'+ #13 +
      #13 +
      'inner join FUNCIONARIOS FUN'+ #13 +
      'on ORC.VENDEDOR_ID = FUN.FUNCIONARIO_ID'+ #13 +
      #13 +
      'inner join CADASTROS CAD'+ #13 +
      'on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
      #13 +
      'inner join CLIENTES CLI'+ #13 +
      'on CAD.CADASTRO_ID = CLI.CADASTRO_ID'+ #13 +
      'and CLI.NAO_GERAR_COMISSAO = ''N'''+ #13 +
      #13 +
      'inner join PRODUTOS PRO'+ #13 +
      'on OIT.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
      #13 +
      'group by'+ #13 +
      '  DEV.DEVOLUCAO_ID,'+ #13 +
      '  FUN.FUNCIONARIO_ID,'+ #13 +
      '  FUN.NOME,'+ #13 +
      '  trunc(DATA_HORA_DEVOLUCAO),'+ #13 +
      '  CON.TIPO,'+ #13 +
      '  CAD.CADASTRO_ID,'+ #13 +
      '  CAD.NOME_FANTASIA,'+ #13 +
      '  DEV.EMPRESA_ID,'+ #13 +
      '  ORC.VALOR_TOTAL'+ #13 +
      #13 +
      'union all'+ #13 +
      #13 +
      'select'+ #13 +
      '  ACU.ACUMULADO_ID as ID,'+ #13 +
      '  ''ACU'' as TIPO,'+ #13 +
      '  FUN.FUNCIONARIO_ID,'+ #13 +
      '  FUN.NOME as NOME_FUNCIONARIO,'+ #13 +
      '  trunc(ACU.DATA_HORA_RECEBIMENTO) as DATA,'+ #13 +
      '  CAD.CADASTRO_ID,'+ #13 +
      '  CAD.NOME_FANTASIA as NOME_CLIENTE,'+ #13 +
      '  ORC.EMPRESA_ID,'+ #13 +
      '  ORC.VALOR_TOTAL as VALOR_VENDA,'+ #13 +
      '  trunc(sum(ITE.VALOR_TOTAL + ITE.VALOR_TOTAL_OUTRAS_DESPESAS - ITE.VALOR_TOTAL_DESCONTO) * ACU.INDICE_PEDIDOS, 2) as VALOR_BASE_COMISSAO,'+ #13 +
      #13 +
      '  case when CON.TIPO = ''P'' then ''A prazo'' else ''A vista'' end as TIPO_COMISSAO,'+ #13 +
      #13 +
      '  round('+ #13 +
      '    sum('+ #13 +
      '      round('+ #13 +
      '        (ITE.VALOR_TOTAL + ITE.VALOR_TOTAL_OUTRAS_DESPESAS - ITE.VALOR_TOTAL_DESCONTO) *'+ #13 +
      '        case when CON.TIPO = ''A'' then'+ #13 +
      '          zvl(PRO.PERCENTUAL_COMISSAO_VISTA, FUN.PERCENTUAL_COMISSAO_A_VISTA)'+ #13 +
      '        else'+ #13 +
      '          zvl(PRO.PERCENTUAL_COMISSAO_PRAZO, FUN.PERCENTUAL_COMISSAO_A_PRAZO)'+ #13 +
      '        end * 0.01,'+ #13 +
      '        2'+ #13 +
      '      )'+ #13 +
      '    ) * ACU.INDICE_PEDIDOS,'+ #13 +
      '    2'+ #13 +
      '  ) as VALOR_COMISSAO'+ #13 +
      #13 +
      'from'+ #13 +
      '  FUNCIONARIOS FUN'+ #13 +
      #13 +
      'inner join ORCAMENTOS ORC'+ #13 +
      'on FUN.FUNCIONARIO_ID = ORC.VENDEDOR_ID'+ #13 +
      'and ORC.STATUS = ''RE'''+ #13 +
      #13 +
      'inner join ORCAMENTOS_ITENS ITE'+ #13 +
      'on ORC.ORCAMENTO_ID = ITE.ORCAMENTO_ID'+ #13 +
      #13 +
      'inner join ACUMULADOS ACU'+ #13 +
      'on ORC.ACUMULADO_ID = ACU.ACUMULADO_ID'+ #13 +
      #13 +
      'inner join PRODUTOS PRO'+ #13 +
      'on ITE.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
      'and PRO.SUJEITO_COMISSAO = ''S'''+ #13 +
      #13 +
      'inner join CADASTROS CAD'+ #13 +
      'on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
      #13 +
      'inner join CLIENTES CLI'+ #13 +
      'on CAD.CADASTRO_ID = CLI.CADASTRO_ID'+ #13 +
      'and CLI.NAO_GERAR_COMISSAO = ''N'''+ #13 +
      #13 +
      'inner join CONDICOES_PAGAMENTO CON'+ #13 +
      'on ORC.CONDICAO_ID = CON.CONDICAO_ID'+ #13 +
      'and CON.ACUMULATIVO = ''S'''+ #13 +
      #13 +
      'group by'+ #13 +
      '  ACU.ACUMULADO_ID,'+ #13 +
      '  ACU.INDICE_PEDIDOS,'+ #13 +
      '  FUN.FUNCIONARIO_ID,'+ #13 +
      '  FUN.NOME,'+ #13 +
      '  trunc(ACU.DATA_HORA_RECEBIMENTO),'+ #13 +
      '  CON.TIPO,'+ #13 +
      '  CAD.CADASTRO_ID,'+ #13 +
      '  CAD.NOME_FANTASIA,'+ #13 +
      '  ORC.EMPRESA_ID,'+ #13 +
      '  ORC.VALOR_TOTAL'+ #13 +
      #13 +
      'union all'+ #13 +
      #13 +
      'select'+ #13 +
      '  COR.RECEBER_ID as ID,'+ #13 +
      '  ''FIN'' as TIPO,'+ #13 +
      '  FUN.FUNCIONARIO_ID,'+ #13 +
      '  FUN.NOME as NOME_FUNCIONARIO,'+ #13 +
      '  trunc(BXA.DATA_HORA_BAIXA) as DATA,'+ #13 +
      '  COR.CADASTRO_ID,'+ #13 +
      '  CAD.NOME_FANTASIA as NOME_CLIENTE,'+ #13 +
      '  COR.EMPRESA_ID,'+ #13 +
      '  ORC.VALOR_TOTAL as VALOR_VENDA,'+ #13 +
      '  COR.VALOR_DOCUMENTO as VALOR_BASE_COMISSAO,'+ #13 +
      #13 +
      '  case when CON.TIPO = ''P'' then ''A prazo'' else ''A vista'' end as TIPO_COMISSAO,'+ #13 +
      #13 +
      '  sum('+ #13 +
      '    round('+ #13 +
      '      (ITE.VALOR_TOTAL + ITE.VALOR_TOTAL_OUTRAS_DESPESAS - ITE.VALOR_TOTAL_DESCONTO) *'+ #13 +
      '      case when CON.TIPO = ''A'' then'+ #13 +
      '        zvl(PRO.PERCENTUAL_COMISSAO_VISTA, FUN.PERCENTUAL_COMISSAO_A_VISTA)'+ #13 +
      '      else'+ #13 +
      '        zvl(PRO.PERCENTUAL_COMISSAO_PRAZO, FUN.PERCENTUAL_COMISSAO_A_PRAZO)'+ #13 +
      '      end * 0.01,'+ #13 +
      '      2'+ #13 +
      '    )'+ #13 +
      '  )  * COR.VALOR_DOCUMENTO / ORC.VALOR_TOTAL as VALOR_COMISSAO'+ #13 +
      #13 +
      'from'+ #13 +
      '  CONTAS_RECEBER COR'+ #13 +
      #13 +
      'inner join FUNCIONARIOS FUN'+ #13 +
      'on COR.VENDEDOR_ID = FUN.FUNCIONARIO_ID'+ #13 +
      #13 +
      'inner join CADASTROS CAD'+ #13 +
      'on COR.CADASTRO_ID = CAD.CADASTRO_ID'+ #13 +
      #13 +
      'inner join CONTAS_RECEBER_BAIXAS BXA'+ #13 +
      'on COR.BAIXA_ID = BXA.BAIXA_ID'+ #13 +
      #13 +
      'inner join TIPOS_COBRANCA TCO'+ #13 +
      'on COR.COBRANCA_ID = TCO.COBRANCA_ID'+ #13 +
      #13 +
      'left join ORCAMENTOS ORC'+ #13 +
      'on COR.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
      #13 +
      'left join ORCAMENTOS_ITENS ITE'+ #13 +
      'on ORC.ORCAMENTO_ID = ITE.ORCAMENTO_ID'+ #13 +
      #13 +
      'left join PRODUTOS PRO'+ #13 +
      'on ITE.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
      #13 +
      'left join CONDICOES_PAGAMENTO CON'+ #13 +
      'on ORC.CONDICAO_ID = CON.CONDICAO_ID'+ #13 +
      #13 +
      'where COR.COMISSIONAR = ''S'''+ #13 +
      'and COR.STATUS = ''B'''+ #13 +
      'and COR.VENDEDOR_ID is not null'+ #13 +
      'and (TCO.TIPO_PAGAMENTO_COMISSAO = ''B'' or COR.ORIGEM = ''MAN'')'+ #13 +
      #13 +
      'group by'+ #13 +
      '  COR.RECEBER_ID,'+ #13 +
      '  FUN.FUNCIONARIO_ID,'+ #13 +
      '  FUN.NOME,'+ #13 +
      '  trunc(BXA.DATA_HORA_BAIXA),'+ #13 +
      '  CON.TIPO,'+ #13 +
      '  COR.CADASTRO_ID,'+ #13 +
      '  CAD.NOME_FANTASIA,'+ #13 +
      '  COR.EMPRESA_ID,'+ #13 +
      '  ORC.VALOR_TOTAL,'+ #13 +
      '  COR.VALOR_DOCUMENTO'+ #13 +
      #13 +
      'with read only;'+ #13
    );
end;

function AjustarComissionamentoTitulos: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'update CONTAS_RECEBER set'+ #13 +
      '  COMISSIONAR = ''S'''+ #13 +
      'where VENDEDOR_ID is not null;'+ #13
    );
end;

function AjustarProcedureLANCAR_ADIANTAMENTO_CONTAS_REC: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure LANCAR_ADIANTAMENTO_CONTAS_REC(iBAIXA_ID in number, oRECEBER_ID out number)'+ #13 +
      'is'+ #13 +
      '  vEmpresaId          CONTAS_RECEBER_BAIXAS.EMPRESA_ID%type;'+ #13 +
      '  vClienteId          CONTAS_RECEBER_BAIXAS.CADASTRO_ID%type;'+ #13 +
      '  vValorBaixa         CONTAS_RECEBER_BAIXAS.VALOR_LIQUIDO%type;'+ #13 +
      '  vReceberAdiantadoId CONTAS_RECEBER_BAIXAS.RECEBER_ADIANTADO_ID%type;'+ #13 +
      'begin'+ #13 +
      #13 +
      '  select'+ #13 +
      '    EMPRESA_ID,'+ #13 +
      '    CADASTRO_ID,'+ #13 +
      '    VALOR_LIQUIDO,'+ #13 +
      '    RECEBER_ADIANTADO_ID'+ #13 +
      '  into'+ #13 +
      '    vEmpresaId,'+ #13 +
      '    vClienteId,'+ #13 +
      '    vValorBaixa,'+ #13 +
      '    vReceberAdiantadoId'+ #13 +
      '  from'+ #13 +
      '    CONTAS_RECEBER_BAIXAS'+ #13 +
      '  where BAIXA_ID = iBAIXA_ID'+ #13 +
      '  and TIPO = ''ADI'';'+ #13 +
      #13 +
      '  update CONTAS_RECEBER set'+ #13 +
      '    VALOR_ADIANTADO = VALOR_ADIANTADO + vValorBaixa'+ #13 +
      '  where RECEBER_ID = vReceberAdiantadoId;'+ #13 +
      #13 +
      '  insert into CONTAS_RECEBER('+ #13 +
      '    RECEBER_ID,'+ #13 +
      '    CADASTRO_ID,'+ #13 +
      '    EMPRESA_ID,      '+ #13 +
      '    COBRANCA_ID,'+ #13 +
      '    PORTADOR_ID,'+ #13 +
      '    PLANO_FINANCEIRO_ID,      '+ #13 +
      '    DOCUMENTO,'+ #13 +
      '    DATA_EMISSAO,'+ #13 +
      '    DATA_VENCIMENTO,'+ #13 +
      '    DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '    VALOR_DOCUMENTO,'+ #13 +
      '    STATUS,'+ #13 +
      '    PARCELA,'+ #13 +
      '    NUMERO_PARCELAS,'+ #13 +
      '    ORIGEM,'+ #13 +
      '    BAIXA_ORIGEM_ID'+ #13 +
      '  )values('+ #13 +
      '    SEQ_RECEBER_ID.nextval,'+ #13 +
      '    vClienteId,'+ #13 +
      '    vEmpresaId,'+ #13 +
      '    SESSAO.PARAMETROS_GERAIS.TIPO_COB_ADIANTAMENTO_FIN_ID,'+ #13 +
      '    ''9999'','+ #13 +
      '    ''1.002.001'','+ #13 +
      '    ''BXR-'' || iBAIXA_ID || ''/ADI'','+ #13 +
      '    trunc(sysdate),'+ #13 +
      '    trunc(sysdate),'+ #13 +
      '    trunc(sysdate),'+ #13 +
      '    vValorBaixa,'+ #13 +
      '    ''A'','+ #13 +
      '    1,'+ #13 +
      '    1,'+ #13 +
      '    ''BXR'','+ #13 +
      '    iBAIXA_ID'+ #13 +
      '  );'+ #13 +
      #13 +
      '  oRECEBER_ID := SEQ_RECEBER_ID.currval;'+ #13 +
      #13 +
      'end LANCAR_ADIANTAMENTO_CONTAS_REC;'+ #13
    );
end;

function AdicionarColunaCPF_CNPJ_EMITENTETabelaORCAMENTOS_PAGAMENTOS_CHEQUES: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'alter table ORCAMENTOS_PAGAMENTOS_CHEQUES'+ #13 +
      'add CPF_CNPJ_EMITENTE varchar2(19);'+ #13
    );
end;

function AdicionarColunaCPF_CNPJ_EMITENTETabelaACUMULADOS_PAGAMENTOS_CHEQUES: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'alter table ACUMULADOS_PAGAMENTOS_CHEQUES'+ #13 +
      'add CPF_CNPJ_EMITENTE varchar2(19);'+ #13
    );
end;

function AdicionarColunaCPF_CNPJ_EMITENTETabelaCONTAS_REC_BAIXAS_PAGTOS_CHQ: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'alter table CONTAS_REC_BAIXAS_PAGTOS_CHQ'+ #13 +
      'add CPF_CNPJ_EMITENTE varchar2(19);'+ #13
    );
end;

function AdicionarColunaCPF_CNPJ_EMITENTETabelaCONTAS_RECEBER: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'alter table CONTAS_RECEBER'+ #13 +
      'add CPF_CNPJ_EMITENTE varchar2(19);'+ #13
    );
end;

function AjustarTriggerCONTAS_RECEBER_D_BR: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace trigger CONTAS_RECEBER_D_BR'+ #13 +
      'before delete'+ #13 +
      'on CONTAS_RECEBER'+ #13 +
      'for each row'+ #13 +
      'begin'+ #13 +
      #13 +
      '  delete from LOGS_CONTAS_RECEBER'+ #13 +
      '  where RECEBER_ID = :old.RECEBER_ID;'+ #13 +
      #13 +
      'end CONTAS_RECEBER_D_BR;'+ #13
    );
end;

function AjustarTriggerACUMULADOS_IU_BR: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace trigger ACUMULADOS_IU_BR'+ #13 +
      'before insert or update'+ #13 +
      'on ACUMULADOS'+ #13 +
      'for each row'+ #13 +
      'begin    '+ #13 +
      #13 +
      '  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
      '  :new.DATA_HORA_ALTERACAO := sysdate;'+ #13 +
      '  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;'+ #13 +
      '  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;'+ #13 +
      #13 +
      '  if inserting then'+ #13 +
      '    :new.USUARIO_FECHAMENTO_ID := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
      '    :new.DATA_HORA_FECHAMENTO  := sysdate;'+ #13 +
      '  else'+ #13 +
      '    if :old.STATUS = ''AR'' and :new.STATUS = ''RE'' then'+ #13 +
      '      :new.USUARIO_RECEBIMENTO_ID := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
      '      :new.DATA_HORA_RECEBIMENTO  := sysdate;'+ #13 +
      '    elsif :old.STATUS <> :new.STATUS and :new.STATUS = ''CA'' then'+ #13 +
      '      :new.USUARIO_CANCELAMENTO_ID := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
      '      :new.DATA_HORA_CANCELAMENTO  := sysdate;'+ #13 +
      '    end if;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      'end ACUMULADOS_IU_BR;'+ #13
    );
end;

function AjustarProcedureRECEBER_ACUMULADO: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure RECEBER_ACUMULADO('+ #13 +
      '  iACUMULADO_ID        in number,'+ #13 +
      '  iTURNO_ID            in number,'+ #13 +
      '  iVALOR_TROCO         in number,'+ #13 +
      '  iGERAR_CREDITO_TROCO in string,'+ #13 +
      '  iTIPO_NOTA_GERAR     in string'+ #13 +
      ')'+ #13 +
      'is'+ #13 +
      '  i                       naturaln default 0;  '+ #13 +
      '  vReceberId              CONTAS_RECEBER.RECEBER_ID%type;'+ #13 +
      '  vBaixaId                CONTAS_RECEBER_BAIXAS.BAIXA_ID%type;'+ #13 +
      '  vDadosPagamentoPedido   RecordsVendas.RecPedidosPagamentos;'+ #13 +
      #13 +
      '  cursor cCheques is'+ #13 +
      '  select'+ #13 +
      '    COBRANCA_ID,'+ #13 +
      '    ITEM_ID,'+ #13 +
      '    DATA_VENCIMENTO,'+ #13 +
      '    BANCO,'+ #13 +
      '    AGENCIA,'+ #13 +
      '    CONTA_CORRENTE,'+ #13 +
      '    NUMERO_CHEQUE,'+ #13 +
      '    VALOR_CHEQUE,'+ #13 +
      '    NOME_EMITENTE,'+ #13 +
      '    CPF_CNPJ_EMITENTE,'+ #13 +
      '    TELEFONE_EMITENTE,'+ #13 +
      '    PARCELA,'+ #13 +
      '    NUMERO_PARCELAS'+ #13 +
      '  from'+ #13 +
      '    ACUMULADOS_PAGAMENTOS_CHEQUES'+ #13 +
      '  where ACUMULADO_ID = iACUMULADO_ID'+ #13 +
      '  order by '+ #13 +
      '		COBRANCA_ID, '+ #13 +
      '		PARCELA,'+ #13 +
      '		DATA_VENCIMENTO, '+ #13 +
      '		ITEM_ID;'+ #13 +
      #13 +
      '  cursor cCobrancas(pTIPO in string) is'+ #13 +
      '  select'+ #13 +
      '    PAG.COBRANCA_ID,'+ #13 +
      '    PAG.ITEM_ID,'+ #13 +
      '    PAG.VALOR,'+ #13 +
      '    PAG.DATA_VENCIMENTO,'+ #13 +
      '    PAG.PARCELA,'+ #13 +
      '    PAG.NUMERO_PARCELAS,'+ #13 +
      '    TIP.BOLETO_BANCARIO,'+ #13 +
      '    PAG.NSU_TEF,'+ #13 +
      '    PAG.CODIGO_AUTORIZACAO,'+ #13 +
      '    PAG.NUMERO_CARTAO,'+ #13 +
      '    TIP.PORTADOR_ID'+ #13 +
      '  from'+ #13 +
      '    ACUMULADOS_PAGAMENTOS PAG'+ #13 +
      #13 +
      '  join TIPOS_COBRANCA TIP'+ #13 +
      '  on PAG.COBRANCA_ID = TIP.COBRANCA_ID    '+ #13 +
      '  and TIP.FORMA_PAGAMENTO = pTIPO'+ #13 +
      #13 +
      '  where PAG.ACUMULADO_ID = iACUMULADO_ID  '+ #13 +
      '  order by '+ #13 +
      '		COBRANCA_ID,'+ #13 +
      '		PARCELA,'+ #13 +
      '		DATA_VENCIMENTO, '+ #13 +
      '		ITEM_ID;'+ #13 +
      #13 +
      'begin'+ #13 +
      #13 +
      '  begin'+ #13 +
      '    select'+ #13 +
      '      VALOR_DINHEIRO,'+ #13 +
      '      VALOR_CARTAO_DEBITO,'+ #13 +
      '      VALOR_CARTAO_CREDITO,'+ #13 +
      '      VALOR_CREDITO,'+ #13 +
      '      VALOR_COBRANCA,'+ #13 +
      '      VALOR_CHEQUE,'+ #13 +
      '      VALOR_FINANCEIRA,      '+ #13 +
      '      STATUS,      '+ #13 +
      '      EMPRESA_ID,'+ #13 +
      '      CLIENTE_ID'+ #13 +
      '    into'+ #13 +
      '      vDadosPagamentoPedido.VALOR_DINHEIRO,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_CARTAO_DEBITO,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_CARTAO_CREDITO,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_CREDITO,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_COBRANCA,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_CHEQUE,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_FINANCEIRA,      '+ #13 +
      '      vDadosPagamentoPedido.STATUS,      '+ #13 +
      '      vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
      '      vDadosPagamentoPedido.CLIENTE_ID'+ #13 +
      '    from'+ #13 +
      '      ACUMULADOS'+ #13 +
      '    where ACUMULADO_ID = iACUMULADO_ID;'+ #13 +
      '  exception'+ #13 +
      '    when others then'+ #13 +
      '      ERRO(''Não foi encontrado os valores de pagamento para o acumulado '' || iACUMULADO_ID || ''!'' || chr(13) || chr(10) || sqlerrm);'+ #13 +
      '  end;'+ #13 +
      #13 +
      '  if vDadosPagamentoPedido.STATUS = ''RE'' then'+ #13 +
      '    ERRO(''Este acumulado já foi recebido!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if vDadosPagamentoPedido.STATUS not in(''AR'') then'+ #13 +
      '    ERRO(''Este acumulado não está aguardando recebimento, verifique!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  /* Inserindo cheques no financeiro */'+ #13 +
      '  if vDadosPagamentoPedido.VALOR_CHEQUE > 0 then'+ #13 +
      '    for vCheques in cCheques loop'+ #13 +
      '      select SEQ_RECEBER_ID.nextval'+ #13 +
      '      into vReceberId'+ #13 +
      '      from dual;'+ #13 +
      #13 +
      '      insert into CONTAS_RECEBER('+ #13 +
      '        RECEBER_ID,'+ #13 +
      '        CADASTRO_ID,'+ #13 +
      '        EMPRESA_ID,'+ #13 +
      '        ACUMULADO_ID,'+ #13 +
      '        COBRANCA_ID,'+ #13 +
      '        PORTADOR_ID,'+ #13 +
      '        PLANO_FINANCEIRO_ID,'+ #13 +
      '        TURNO_ID,        '+ #13 +
      '        DOCUMENTO,'+ #13 +
      '        BANCO,'+ #13 +
      '        AGENCIA,'+ #13 +
      '        CONTA_CORRENTE,'+ #13 +
      '        NUMERO_CHEQUE,'+ #13 +
      '        NOME_EMITENTE,'+ #13 +
      '        TELEFONE_EMITENTE,'+ #13 +
      '        CPF_CNPJ_EMITENTE,'+ #13 +
      '        DATA_EMISSAO,'+ #13 +
      '        DATA_VENCIMENTO,'+ #13 +
      '        DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '        VALOR_DOCUMENTO,'+ #13 +
      '        STATUS,'+ #13 +
      '        PARCELA,'+ #13 +
      '        NUMERO_PARCELAS,'+ #13 +
      '        ORIGEM'+ #13 +
      '      )values('+ #13 +
      '        vReceberId,'+ #13 +
      '        vDadosPagamentoPedido.CLIENTE_ID,'+ #13 +
      '        vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
      '        iACUMULADO_ID,'+ #13 +
      '        vCheques.COBRANCA_ID,'+ #13 +
      '        ''9998'','+ #13 +
      '        ''1.001.002'','+ #13 +
      '        iTURNO_ID,        '+ #13 +
      '        ''ACU-'' || iACUMULADO_ID || '' - '' || vCheques.ITEM_ID || ''/CHQ'','+ #13 +
      '        vCheques.BANCO,'+ #13 +
      '        vCheques.AGENCIA,'+ #13 +
      '        vCheques.CONTA_CORRENTE,'+ #13 +
      '        vCheques.NUMERO_CHEQUE,'+ #13 +
      '        vCheques.NOME_EMITENTE,'+ #13 +
      '        vCheques.CPF_CNPJ_EMITENTE,'+ #13 +
      '        vCheques.TELEFONE_EMITENTE,'+ #13 +
      '        trunc(sysdate),'+ #13 +
      '        vCheques.DATA_VENCIMENTO,'+ #13 +
      '        vCheques.DATA_VENCIMENTO,'+ #13 +
      '        vCheques.VALOR_CHEQUE,'+ #13 +
      '        ''A'','+ #13 +
      '        vCheques.PARCELA,'+ #13 +
      '        vCheques.NUMERO_PARCELAS,'+ #13 +
      '        ''ACU'''+ #13 +
      '      );'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if vDadosPagamentoPedido.VALOR_COBRANCA > 0 then'+ #13 +
      '    for vCobrancas in cCobrancas(''COB'') loop'+ #13 +
      '      select SEQ_RECEBER_ID.nextval'+ #13 +
      '      into vReceberId'+ #13 +
      '      from dual;'+ #13 +
      #13 +
      '      insert into CONTAS_RECEBER('+ #13 +
      '        RECEBER_ID,'+ #13 +
      '        CADASTRO_ID,'+ #13 +
      '        EMPRESA_ID,'+ #13 +
      '        ACUMULADO_ID,'+ #13 +
      '        COBRANCA_ID,'+ #13 +
      '        PORTADOR_ID,'+ #13 +
      '        PLANO_FINANCEIRO_ID,'+ #13 +
      '        TURNO_ID,        '+ #13 +
      '        DOCUMENTO,'+ #13 +
      '        DATA_EMISSAO,'+ #13 +
      '        DATA_VENCIMENTO,'+ #13 +
      '        DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '        VALOR_DOCUMENTO,'+ #13 +
      '        STATUS,'+ #13 +
      '        PARCELA,'+ #13 +
      '        NUMERO_PARCELAS,'+ #13 +
      '        ORIGEM'+ #13 +
      '      )values('+ #13 +
      '        vReceberId,'+ #13 +
      '        vDadosPagamentoPedido.CLIENTE_ID,'+ #13 +
      '        vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
      '        iACUMULADO_ID,'+ #13 +
      '        vCobrancas.COBRANCA_ID,'+ #13 +
      '        vCobrancas.PORTADOR_ID,'+ #13 +
      '        case when vCobrancas.BOLETO_BANCARIO = ''N'' then ''1.001.005'' else ''1.001.006'' end,'+ #13 +
      '        iTURNO_ID,        '+ #13 +
      '        ''ACU-'' || iACUMULADO_ID  || '' - '' || vCobrancas.ITEM_ID || case when vCobrancas.BOLETO_BANCARIO = ''S'' then ''/BOL'' else ''/COB'' end,'+ #13 +
      '        trunc(sysdate),'+ #13 +
      '        vCobrancas.DATA_VENCIMENTO,'+ #13 +
      '        vCobrancas.DATA_VENCIMENTO,'+ #13 +
      '        vCobrancas.VALOR,'+ #13 +
      '        ''A'','+ #13 +
      '        vCobrancas.PARCELA,'+ #13 +
      '        vCobrancas.NUMERO_PARCELAS,'+ #13 +
      '        ''ACU'''+ #13 +
      '      );'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if iGERAR_CREDITO_TROCO = ''S'' and iVALOR_TROCO > 0 then'+ #13 +
      '    GERAR_CREDITO_TROCO(iACUMULADO_ID, ''ACU'', vDadosPagamentoPedido.CLIENTE_ID, iVALOR_TROCO);'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  update ACUMULADOS set'+ #13 +
      '    VALOR_TROCO = case when iGERAR_CREDITO_TROCO = ''N'' then iVALOR_TROCO else 0 end,'+ #13 +
      '    TURNO_ID = iTURNO_ID,'+ #13 +
      '    STATUS = ''RE'''+ #13 +
      '  where ACUMULADO_ID = iACUMULADO_ID;'+ #13 +
      #13 +
      '  /* Não alterar este processo!!! Ele deve ser depois do recebimento */'+ #13 +
      '  if vDadosPagamentoPedido.VALOR_CARTAO_DEBITO + vDadosPagamentoPedido.VALOR_CARTAO_CREDITO > 0 then'+ #13 +
      '    for vCobrancas in cCobrancas(''CRT'') loop'+ #13 +
      '      GERAR_CARTOES_REC_ACUMULADO(iACUMULADO_ID, vCobrancas.ITEM_ID, vCobrancas.NSU_TEF, vCobrancas.CODIGO_AUTORIZACAO, vCobrancas.NUMERO_CARTAO);'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  ATUALIZAR_RETENCOES_CONTAS_REC(iACUMULADO_ID, ''ACU'');'+ #13 +
      #13 +
      '  GERAR_NOTA_ACUMULADO(iACUMULADO_ID);'+ #13 +
      #13 +
      'end RECEBER_ACUMULADO;'+ #13
    );
end;

function AjustarProcedurRECEBER_PEDIDO: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure RECEBER_PEDIDO('+ #13 +
      '  iORCAMENTO_ID        in positiven,'+ #13 +
      '  iTURNO_ID            in positiven,'+ #13 +
      '  iVALOR_TROCO         in number,'+ #13 +
      '  iGERAR_CREDITO_TROCO in string,'+ #13 +
      '  iTIPO_NOTA_GERAR     in string'+ #13 +
      ')'+ #13 +
      'is'+ #13 +
      '  i                       naturaln default 0;'+ #13 +
      '  vRetiradaId             RETIRADAS.RETIRADA_ID%type;'+ #13 +
      '  vReceberId              CONTAS_RECEBER.RECEBER_ID%type;'+ #13 +
      '  vDadosPagamentoPedido   RecordsVendas.RecPedidosPagamentos;'+ #13 +
      #13 +
      '  vPagamentoInconsistente char(1);'+ #13 +
      '  vBaixaCreditoPagarId    CONTAS_PAGAR_BAIXAS.BAIXA_ID%type;'+ #13 +
      '  vValorTotal             ORCAMENTOS.VALOR_TOTAL%type;'+ #13 +
      '  vValorBaixarCredito     number default 0;'+ #13 +
      '  vValorRestante          number default 0;'+ #13 +
      '  vReceberNaEntrega       ORCAMENTOS.RECEBER_NA_ENTREGA%type;'+ #13 +
      #13 +
      '  cursor cCheques is'+ #13 +
      '  select'+ #13 +
      '    COBRANCA_ID,'+ #13 +
      '    ITEM_ID,'+ #13 +
      '    DATA_VENCIMENTO,'+ #13 +
      '    BANCO,'+ #13 +
      '    AGENCIA,'+ #13 +
      '    CONTA_CORRENTE,'+ #13 +
      '    NUMERO_CHEQUE,'+ #13 +
      '    VALOR_CHEQUE,'+ #13 +
      '    NOME_EMITENTE,'+ #13 +
      '    CPF_CNPJ_EMITENTE,'+ #13 +
      '    TELEFONE_EMITENTE,'+ #13 +
      '    PARCELA,'+ #13 +
      '    NUMERO_PARCELAS'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS_PAGAMENTOS_CHEQUES'+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '  order by '+ #13 +
      '		COBRANCA_ID, '+ #13 +
      '		PARCELA,'+ #13 +
      '		DATA_VENCIMENTO, '+ #13 +
      '		ITEM_ID;'+ #13 +
      #13 +
      '  cursor cCobrancas(pTIPO in string) is'+ #13 +
      '  select'+ #13 +
      '    PAG.COBRANCA_ID,'+ #13 +
      '    PAG.ITEM_ID,'+ #13 +
      '    PAG.VALOR,'+ #13 +
      '    PAG.DATA_VENCIMENTO,'+ #13 +
      '    PAG.PARCELA,'+ #13 +
      '    PAG.NUMERO_PARCELAS,'+ #13 +
      '    TIP.BOLETO_BANCARIO,'+ #13 +
      '    PAG.NSU_TEF,'+ #13 +
      '    TIP.PORTADOR_ID,'+ #13 +
      '    PAG.NUMERO_CARTAO,'+ #13 +
      '    PAG.CODIGO_AUTORIZACAO'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS_PAGAMENTOS PAG'+ #13 +
      #13 +
      '  join TIPOS_COBRANCA TIP'+ #13 +
      '  on PAG.COBRANCA_ID = TIP.COBRANCA_ID    '+ #13 +
      '  and TIP.FORMA_PAGAMENTO = pTIPO'+ #13 +
      #13 +
      '  where PAG.ORCAMENTO_ID = iORCAMENTO_ID  '+ #13 +
      '  order by '+ #13 +
      '		COBRANCA_ID,'+ #13 +
      '		PARCELA,'+ #13 +
      '		DATA_VENCIMENTO, '+ #13 +
      '		ITEM_ID;'+ #13 +
      #13 +
      '  cursor cCreditos is'+ #13 +
      '  select'+ #13 +
      '    ORC.PAGAR_ID,'+ #13 +
      '    PAG.VALOR_DOCUMENTO - PAG.VALOR_DESCONTO - PAG.VALOR_ADIANTADO as VALOR_LIQUIDO'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS_CREDITOS ORC'+ #13 +
      #13 +
      '  inner join CONTAS_PAGAR PAG'+ #13 +
      '  on ORC.PAGAR_ID = PAG.PAGAR_ID'+ #13 +
      #13 +
      '  where ORC.ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '  and ORC.MOMENTO_USO = ''R'';'+ #13 +
      #13 +
      'begin'+ #13 +
      #13 +
      '  begin'+ #13 +
      '    select'+ #13 +
      '      VALOR_DINHEIRO,'+ #13 +
      '      VALOR_CARTAO_DEBITO,'+ #13 +
      '      VALOR_CARTAO_CREDITO,'+ #13 +
      '      VALOR_CREDITO,'+ #13 +
      '      VALOR_COBRANCA,'+ #13 +
      '      VALOR_CHEQUE,'+ #13 +
      '      VALOR_FINANCEIRA,'+ #13 +
      '      VALOR_ACUMULATIVO,'+ #13 +
      '      STATUS,'+ #13 +
      '      VENDEDOR_ID,'+ #13 +
      '      EMPRESA_ID,'+ #13 +
      '      CLIENTE_ID,'+ #13 +
      '      RECEBER_NA_ENTREGA,'+ #13 +
      '      VALOR_TOTAL,'+ #13 +
      '      case when VALOR_TOTAL < VALOR_TOTAL_PRODUTOS + VALOR_OUTRAS_DESPESAS + VALOR_FRETE - VALOR_DESCONTO then ''S'' else ''N'' end'+ #13 +
      '    into'+ #13 +
      '      vDadosPagamentoPedido.VALOR_DINHEIRO,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_CARTAO_DEBITO,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_CARTAO_CREDITO,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_CREDITO,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_COBRANCA,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_CHEQUE,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_FINANCEIRA,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_ACUMULATIVO,'+ #13 +
      '      vDadosPagamentoPedido.STATUS,'+ #13 +
      '      vDadosPagamentoPedido.VENDEDOR_ID,'+ #13 +
      '      vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
      '      vDadosPagamentoPedido.CLIENTE_ID,'+ #13 +
      '      vReceberNaEntrega,'+ #13 +
      '      vValorTotal,'+ #13 +
      '      vPagamentoInconsistente'+ #13 +
      '    from'+ #13 +
      '      ORCAMENTOS'+ #13 +
      '    where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      '  exception'+ #13 +
      '    when others then'+ #13 +
      '      ERRO(''Não foi encontrado os valores de pagamento para o orçamento '' || iORCAMENTO_ID || ''!'' || chr(13) || chr(10) || sqlerrm);'+ #13 +
      '  end;'+ #13 +
      #13 +
      '  if vDadosPagamentoPedido.STATUS = ''RE'' then'+ #13 +
      '    ERRO(''Este orçamento já foi recebido!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if vDadosPagamentoPedido.STATUS not in(''VR'', ''VE'') then'+ #13 +
      '    ERRO(''Este pedido não está aguardando recebimento, verifique!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  /* Apagando as previsões dos recebimentos na entrega */'+ #13 +
      '  if vDadosPagamentoPedido.STATUS = ''VE'' then'+ #13 +
      '    delete from CONTAS_RECEBER'+ #13 +
      '    where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '    and COBRANCA_ID = SESSAO.PARAMETROS_GERAIS.TIPO_COB_RECEB_ENTREGA_ID;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  /* Inserindo cheques no financeiro */'+ #13 +
      '  if vDadosPagamentoPedido.VALOR_CHEQUE > 0 then'+ #13 +
      '    for vCheques in cCheques loop'+ #13 +
      '      select SEQ_RECEBER_ID.nextval'+ #13 +
      '      into vReceberId'+ #13 +
      '      from dual;'+ #13 +
      #13 +
      '      insert into CONTAS_RECEBER('+ #13 +
      '        RECEBER_ID,'+ #13 +
      '        CADASTRO_ID,'+ #13 +
      '        EMPRESA_ID,'+ #13 +
      '        ORCAMENTO_ID,'+ #13 +
      '        COBRANCA_ID,'+ #13 +
      '        PORTADOR_ID,'+ #13 +
      '        PLANO_FINANCEIRO_ID,'+ #13 +
      '        TURNO_ID,'+ #13 +
      '        VENDEDOR_ID,'+ #13 +
      '        DOCUMENTO,'+ #13 +
      '        BANCO,'+ #13 +
      '        AGENCIA,'+ #13 +
      '        CONTA_CORRENTE,'+ #13 +
      '        NUMERO_CHEQUE,'+ #13 +
      '        NOME_EMITENTE,'+ #13 +
      '        CPF_CNPJ_EMITENTE,'+ #13 +
      '        TELEFONE_EMITENTE,'+ #13 +
      '        DATA_EMISSAO,'+ #13 +
      '        DATA_VENCIMENTO,'+ #13 +
      '        DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '        VALOR_DOCUMENTO,'+ #13 +
      '        STATUS,'+ #13 +
      '        PARCELA,'+ #13 +
      '        NUMERO_PARCELAS,'+ #13 +
      '        ORIGEM'+ #13 +
      '      )values('+ #13 +
      '        vReceberId,'+ #13 +
      '        vDadosPagamentoPedido.CLIENTE_ID,'+ #13 +
      '        vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
      '        iORCAMENTO_ID,'+ #13 +
      '        vCheques.COBRANCA_ID,'+ #13 +
      '        ''9998'','+ #13 +
      '        ''1.001.002'','+ #13 +
      '        iTURNO_ID,'+ #13 +
      '        vDadosPagamentoPedido.VENDEDOR_ID,'+ #13 +
      '        ''PED-'' || iORCAMENTO_ID || vCheques.ITEM_ID || ''/CHQ'','+ #13 +
      '        vCheques.BANCO,'+ #13 +
      '        vCheques.AGENCIA,'+ #13 +
      '        vCheques.CONTA_CORRENTE,'+ #13 +
      '        vCheques.NUMERO_CHEQUE,'+ #13 +
      '        vCheques.NOME_EMITENTE,'+ #13 +
      '        vCheques.CPF_CNPJ_EMITENTE,'+ #13 +
      '        vCheques.TELEFONE_EMITENTE,'+ #13 +
      '        trunc(sysdate),'+ #13 +
      '        vCheques.DATA_VENCIMENTO,'+ #13 +
      '        vCheques.DATA_VENCIMENTO,'+ #13 +
      '        vCheques.VALOR_CHEQUE,'+ #13 +
      '        ''A'','+ #13 +
      '        vCheques.PARCELA,'+ #13 +
      '        vCheques.NUMERO_PARCELAS,'+ #13 +
      '        ''ORC'''+ #13 +
      '      );'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if vDadosPagamentoPedido.VALOR_COBRANCA > 0 then'+ #13 +
      '    for vCobrancas in cCobrancas(''COB'') loop'+ #13 +
      '      select SEQ_RECEBER_ID.nextval'+ #13 +
      '      into vReceberId'+ #13 +
      '      from dual;'+ #13 +
      #13 +
      '      insert into CONTAS_RECEBER('+ #13 +
      '        RECEBER_ID,'+ #13 +
      '        CADASTRO_ID,'+ #13 +
      '        EMPRESA_ID,'+ #13 +
      '        ORCAMENTO_ID,'+ #13 +
      '        COBRANCA_ID,'+ #13 +
      '        PORTADOR_ID,'+ #13 +
      '        PLANO_FINANCEIRO_ID,'+ #13 +
      '        TURNO_ID,'+ #13 +
      '        VENDEDOR_ID,'+ #13 +
      '        DOCUMENTO,'+ #13 +
      '        DATA_EMISSAO,'+ #13 +
      '        DATA_VENCIMENTO,'+ #13 +
      '        DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '        VALOR_DOCUMENTO,'+ #13 +
      '        STATUS,'+ #13 +
      '        PARCELA,'+ #13 +
      '        NUMERO_PARCELAS,'+ #13 +
      '        ORIGEM'+ #13 +
      '      )values('+ #13 +
      '        vReceberId,'+ #13 +
      '        vDadosPagamentoPedido.CLIENTE_ID,'+ #13 +
      '        vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
      '        iORCAMENTO_ID,'+ #13 +
      '        vCobrancas.COBRANCA_ID,'+ #13 +
      '        vCobrancas.PORTADOR_ID,'+ #13 +
      '        case when vCobrancas.BOLETO_BANCARIO = ''N'' then ''1.001.005'' else ''1.001.006'' end,'+ #13 +
      '        iTURNO_ID,'+ #13 +
      '        vDadosPagamentoPedido.VENDEDOR_ID,'+ #13 +
      '        ''PED-'' || iORCAMENTO_ID || vCobrancas.ITEM_ID || case when vCobrancas.BOLETO_BANCARIO = ''S'' then ''/BOL'' else ''/COB'' end,'+ #13 +
      '        trunc(sysdate),'+ #13 +
      '        vCobrancas.DATA_VENCIMENTO,'+ #13 +
      '        vCobrancas.DATA_VENCIMENTO,'+ #13 +
      '        vCobrancas.VALOR,'+ #13 +
      '        ''A'','+ #13 +
      '        vCobrancas.PARCELA,'+ #13 +
      '        vCobrancas.NUMERO_PARCELAS,'+ #13 +
      '        ''ORC'''+ #13 +
      '      );'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if vDadosPagamentoPedido.VALOR_ACUMULATIVO > 0 then'+ #13 +
      '    AJUSTAR_CONTAS_REC_ACUMULATIVO(iORCAMENTO_ID);'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if iGERAR_CREDITO_TROCO = ''S'' and iVALOR_TROCO > 0 then'+ #13 +
      '    GERAR_CREDITO_TROCO(iORCAMENTO_ID, ''ORC'', vDadosPagamentoPedido.CLIENTE_ID, iVALOR_TROCO);'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if vReceberNaEntrega = ''S'' and vDadosPagamentoPedido.VALOR_CREDITO > 0 then'+ #13 +
      '    vValorRestante :='+ #13 +
      '      vValorTotal - ('+ #13 +
      '        vDadosPagamentoPedido.VALOR_DINHEIRO +'+ #13 +
      '        vDadosPagamentoPedido.VALOR_CHEQUE +'+ #13 +
      '        vDadosPagamentoPedido.VALOR_CARTAO_DEBITO +'+ #13 +
      '        vDadosPagamentoPedido.VALOR_CARTAO_CREDITO +'+ #13 +
      '        vDadosPagamentoPedido.VALOR_COBRANCA +'+ #13 +
      '        vDadosPagamentoPedido.VALOR_FINANCEIRA'+ #13 +
      '      );'+ #13 +
      #13 +
      '    for xCreditos in cCreditos loop'+ #13 +
      '      if xCreditos.VALOR_LIQUIDO >= vValorRestante then'+ #13 +
      '        vValorBaixarCredito := vValorRestante;'+ #13 +
      '        vValorRestante := 0;'+ #13 +
      '      else'+ #13 +
      '        vValorBaixarCredito := xCreditos.VALOR_LIQUIDO;'+ #13 +
      '        vValorRestante := vValorRestante - vValorBaixarCredito;'+ #13 +
      '      end if;'+ #13 +
      #13 +
      '      BAIXAR_CREDITO_PAGAR(xCreditos.PAGAR_ID, iORCAMENTO_ID, ''ORC'', vValorBaixarCredito, vBaixaCreditoPagarId, vBaixaCreditoPagarId);'+ #13 +
      #13 +
      '      if vValorRestante = 0 then'+ #13 +
      '        exit;'+ #13 +
      '      end if;'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  update ORCAMENTOS set'+ #13 +
      '    VALOR_TROCO = case when iGERAR_CREDITO_TROCO = ''N'' then iVALOR_TROCO else 0 end,'+ #13 +
      '    TURNO_ID = iTURNO_ID,'+ #13 +
      '    STATUS = ''RE'''+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      #13 +
      '  /* Não alterar este processo!!! Ele deve ser depois do recebimento */'+ #13 +
      '  if vDadosPagamentoPedido.VALOR_CARTAO_DEBITO + vDadosPagamentoPedido.VALOR_CARTAO_CREDITO > 0 then'+ #13 +
      '    for vCobrancas in cCobrancas(''CRT'') loop'+ #13 +
      '      GERAR_CARTOES_RECEBER(iORCAMENTO_ID, vCobrancas.ITEM_ID, vCobrancas.NSU_TEF, vCobrancas.CODIGO_AUTORIZACAO, vCobrancas.NUMERO_CARTAO);'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  select'+ #13 +
      '    max(RETIRADA_ID)'+ #13 +
      '  into'+ #13 +
      '    vRetiradaId'+ #13 +
      '  from'+ #13 +
      '    RETIRADAS'+ #13 +
      '  where TIPO_MOVIMENTO = ''A'''+ #13 +
      '  and ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      #13 +
      '  if vRetiradaId is not null then'+ #13 +
      '    update RETIRADAS set'+ #13 +
      '      TIPO_NOTA_GERAR = nvl(iTIPO_NOTA_GERAR, ''NI'')'+ #13 +
      '    where RETIRADA_ID = vRetiradaId;'+ #13 +
      #13 +
      '    /* Caso o cliente trabalhe com confirmação de saídas de mercadorias */'+ #13 +
      '    if SESSAO.PARAMETROS_EMPRESA_LOGADA.CONFIRMAR_SAIDA_PRODUTOS = ''N'' then'+ #13 +
      '      CONFIRMAR_RETIRADA(vRetiradaId, ''AUTOMATICO'');'+ #13 +
      '    end if;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  ATUALIZAR_RETENCOES_CONTAS_REC(iORCAMENTO_ID, ''ORC'');'+ #13 +
      #13 +
      'end RECEBER_PEDIDO;'+ #13
    );
end;

function AjustarProcedurCONSOLIDAR_BAIXA_CONTAS_REC: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure CONSOLIDAR_BAIXA_CONTAS_REC(iBAIXA_RECEBER_ID in number)'+ #13 +
      'is'+ #13 +
      #13 +
      '  vReceberId     CONTAS_RECEBER.RECEBER_ID%type;'+ #13 +
      '  vCadastroId    CONTAS_RECEBER_BAIXAS.BAIXA_ID%type;'+ #13 +
      '  vEmpresaId     CONTAS_RECEBER_BAIXAS.EMPRESA_ID%type;'+ #13 +
      '  vValorRetencao CONTAS_RECEBER_BAIXAS.VALOR_RETENCAO%type;'+ #13 +
      #13 +
      '  vValorBaixarCredito   number default 0;'+ #13 +
      '  vValorRestante        number default 0;'+ #13 +
      #13 +
      '  vBaixaCreditoPagarId  CONTAS_PAGAR_BAIXAS.BAIXA_ID%type;'+ #13 +
      '  vValorDinheiro        CONTAS_RECEBER_BAIXAS.VALOR_DINHEIRO%type;'+ #13 +
      '  vValorCheque          CONTAS_RECEBER_BAIXAS.VALOR_CHEQUE%type;'+ #13 +
      '  vValorCartaoDeb       CONTAS_RECEBER_BAIXAS.VALOR_CARTAO_DEBITO%type;'+ #13 +
      '  vValorCartaoCred      CONTAS_RECEBER_BAIXAS.VALOR_CARTAO_CREDITO%type;'+ #13 +
      '  vValorCobranca        CONTAS_RECEBER_BAIXAS.VALOR_COBRANCA%type;'+ #13 +
      '  vValorCredito         CONTAS_RECEBER_BAIXAS.VALOR_CREDITO%type;'+ #13 +
      '  vValorTotal           CONTAS_RECEBER_BAIXAS.VALOR_LIQUIDO%type;'+ #13 +
      #13 +
      '  cursor cCheques is'+ #13 +
      '  select'+ #13 +
      '    CHQ.COBRANCA_ID,'+ #13 +
      '    CHQ.DATA_VENCIMENTO,'+ #13 +
      '    CHQ.PARCELA,'+ #13 +
      '    CHQ.NUMERO_PARCELAS,'+ #13 +
      '    CHQ.BANCO,'+ #13 +
      '    CHQ.AGENCIA,     '+ #13 +
      '    CHQ.CONTA_CORRENTE,'+ #13 +
      '    CHQ.NOME_EMITENTE,'+ #13 +
      '    CHQ.CPF_CNPJ_EMITENTE,'+ #13 +
      '    CHQ.TELEFONE_EMITENTE,'+ #13 +
      '    CHQ.VALOR_CHEQUE,'+ #13 +
      '    CHQ.NUMERO_CHEQUE,'+ #13 +
      '    CHQ.ITEM_ID'+ #13 +
      '  from'+ #13 +
      '    CONTAS_REC_BAIXAS_PAGTOS_CHQ CHQ'+ #13 +
      '  where CHQ.BAIXA_ID = iBAIXA_RECEBER_ID;'+ #13 +
      #13 +
      #13 +
      '  cursor cCobrancas(pTIPO in string) is'+ #13 +
      '  select'+ #13 +
      '    PAG.COBRANCA_ID,'+ #13 +
      '    PAG.ITEM_ID,'+ #13 +
      '    PAG.VALOR,'+ #13 +
      '    PAG.DATA_VENCIMENTO,'+ #13 +
      '    PAG.PARCELA,'+ #13 +
      '    PAG.NUMERO_PARCELAS,'+ #13 +
      '    TIP.BOLETO_BANCARIO,'+ #13 +
      '    PAG.NSU_TEF,'+ #13 +
      '    TIP.PORTADOR_ID'+ #13 +
      '  from'+ #13 +
      '    CONTAS_REC_BAIXAS_PAGAMENTOS PAG'+ #13 +
      #13 +
      '  join TIPOS_COBRANCA TIP'+ #13 +
      '  on PAG.COBRANCA_ID = TIP.COBRANCA_ID    '+ #13 +
      '  and TIP.FORMA_PAGAMENTO = pTIPO'+ #13 +
      #13 +
      '  where PAG.BAIXA_ID = iBAIXA_RECEBER_ID  '+ #13 +
      '  order by '+ #13 +
      '		COBRANCA_ID,'+ #13 +
      '		PARCELA,'+ #13 +
      '		DATA_VENCIMENTO, '+ #13 +
      '		ITEM_ID;'+ #13 +
      #13 +
      #13 +
      '  cursor cCartoes is'+ #13 +
      '  select'+ #13 +
      '    ITEM_ID,'+ #13 +
      '    NSU_TEF,'+ #13 +
      '    NUMERO_CARTAO,'+ #13 +
      '    CODIGO_AUTORIZACAO'+ #13 +
      '  from'+ #13 +
      '    CONTAS_REC_BAIXAS_PAGAMENTOS'+ #13 +
      '  where BAIXA_ID = iBAIXA_RECEBER_ID'+ #13 +
      '  and TIPO = ''CR'';'+ #13 +
      #13 +
      '  cursor cCreditos is'+ #13 +
      '  select'+ #13 +
      '    BAI.PAGAR_ID,'+ #13 +
      '    PAG.VALOR_DOCUMENTO - PAG.VALOR_DESCONTO - PAG.VALOR_ADIANTADO as VALOR_LIQUIDO'+ #13 +
      '  from'+ #13 +
      '    CONTAS_REC_BAIXAS_CREDITOS BAI'+ #13 +
      #13 +
      '  inner join CONTAS_PAGAR PAG'+ #13 +
      '  on BAI.PAGAR_ID = PAG.PAGAR_ID'+ #13 +
      #13 +
      '  where BAI.BAIXA_ID = iBAIXA_RECEBER_ID;'+ #13 +
      #13 +
      'begin'+ #13 +
      #13 +
      '  select'+ #13 +
      '    EMPRESA_ID,'+ #13 +
      '    CADASTRO_ID,'+ #13 +
      '    VALOR_DINHEIRO,'+ #13 +
      '    VALOR_CHEQUE,'+ #13 +
      '    VALOR_CARTAO_DEBITO,'+ #13 +
      '    VALOR_CARTAO_CREDITO,'+ #13 +
      '    VALOR_COBRANCA,'+ #13 +
      '    VALOR_CREDITO,'+ #13 +
      '    VALOR_LIQUIDO,'+ #13 +
      '    VALOR_RETENCAO'+ #13 +
      '  into'+ #13 +
      '    vEmpresaId,'+ #13 +
      '    vCadastroId,'+ #13 +
      '    vValorDinheiro,'+ #13 +
      '    vValorCheque,'+ #13 +
      '    vValorCartaoDeb,'+ #13 +
      '    vValorCartaoCred,'+ #13 +
      '    vValorCobranca,'+ #13 +
      '    vValorCredito,'+ #13 +
      '    vValorTotal,'+ #13 +
      '    vValorRetencao'+ #13 +
      '  from'+ #13 +
      '    CONTAS_RECEBER_BAIXAS'+ #13 +
      '  where BAIXA_ID = iBAIXA_RECEBER_ID;'+ #13 +
      #13 +
      '  for xCheques in cCheques loop      '+ #13 +
      '    select SEQ_RECEBER_ID.nextval'+ #13 +
      '    into vReceberId'+ #13 +
      '    from dual;'+ #13 +
      #13 +
      '    insert into CONTAS_RECEBER('+ #13 +
      '      RECEBER_ID,'+ #13 +
      '      CADASTRO_ID,'+ #13 +
      '      EMPRESA_ID,      '+ #13 +
      '      COBRANCA_ID,'+ #13 +
      '      PORTADOR_ID,'+ #13 +
      '      PLANO_FINANCEIRO_ID,      '+ #13 +
      '      DOCUMENTO,'+ #13 +
      '      BANCO,'+ #13 +
      '      AGENCIA,'+ #13 +
      '      CONTA_CORRENTE,'+ #13 +
      '      NUMERO_CHEQUE,'+ #13 +
      '      NOME_EMITENTE,'+ #13 +
      '      CPF_CNPJ_EMITENTE,'+ #13 +
      '      TELEFONE_EMITENTE,'+ #13 +
      '      DATA_EMISSAO,'+ #13 +
      '      DATA_VENCIMENTO,'+ #13 +
      '      DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '      VALOR_DOCUMENTO,'+ #13 +
      '      STATUS,'+ #13 +
      '      PARCELA,'+ #13 +
      '      NUMERO_PARCELAS,'+ #13 +
      '      ORIGEM,'+ #13 +
      '      BAIXA_ORIGEM_ID'+ #13 +
      '    )values('+ #13 +
      '      vReceberId,'+ #13 +
      '      vCadastroId,'+ #13 +
      '      vEmpresaId,'+ #13 +
      '      xCheques.COBRANCA_ID,'+ #13 +
      '      ''9998'','+ #13 +
      '      ''1.001.002'',      '+ #13 +
      '      ''BXR-'' || iBAIXA_RECEBER_ID || xCheques.ITEM_ID || ''/CHQ'','+ #13 +
      '      xCheques.BANCO,'+ #13 +
      '      xCheques.AGENCIA,'+ #13 +
      '      xCheques.CONTA_CORRENTE,'+ #13 +
      '      xCheques.NUMERO_CHEQUE,'+ #13 +
      '      xCheques.NOME_EMITENTE,'+ #13 +
      '      xCheques.CPF_CNPJ_EMITENTE,'+ #13 +
      '      xCheques.TELEFONE_EMITENTE,'+ #13 +
      '      trunc(sysdate),'+ #13 +
      '      xCheques.DATA_VENCIMENTO,'+ #13 +
      '      xCheques.DATA_VENCIMENTO,'+ #13 +
      '      xCheques.VALOR_CHEQUE,'+ #13 +
      '      ''A'','+ #13 +
      '      xCheques.PARCELA,'+ #13 +
      '      xCheques.NUMERO_PARCELAS,'+ #13 +
      '      ''BXR'','+ #13 +
      '      iBAIXA_RECEBER_ID'+ #13 +
      '    );    '+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  for xCobrancas in cCobrancas(''COB'') loop'+ #13 +
      '    select SEQ_RECEBER_ID.nextval'+ #13 +
      '    into vReceberId'+ #13 +
      '    from dual;'+ #13 +
      #13 +
      '    insert into CONTAS_RECEBER('+ #13 +
      '      RECEBER_ID,'+ #13 +
      '      CADASTRO_ID,'+ #13 +
      '      EMPRESA_ID,      '+ #13 +
      '      COBRANCA_ID,'+ #13 +
      '      PORTADOR_ID,'+ #13 +
      '      PLANO_FINANCEIRO_ID,        '+ #13 +
      '      DOCUMENTO,'+ #13 +
      '      DATA_EMISSAO,'+ #13 +
      '      DATA_VENCIMENTO,'+ #13 +
      '      DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '      VALOR_DOCUMENTO,'+ #13 +
      '      STATUS,'+ #13 +
      '      PARCELA,'+ #13 +
      '      NUMERO_PARCELAS,'+ #13 +
      '      ORIGEM,'+ #13 +
      '      BAIXA_ORIGEM_ID'+ #13 +
      '    )values('+ #13 +
      '      vReceberId,'+ #13 +
      '      vCadastroId,'+ #13 +
      '      vEmpresaId,'+ #13 +
      '      xCobrancas.COBRANCA_ID,'+ #13 +
      '      xCobrancas.PORTADOR_ID,'+ #13 +
      '      case when xCobrancas.BOLETO_BANCARIO = ''N'' then ''1.001.005'' else ''1.001.006'' end,            '+ #13 +
      '      ''BXR-'' || iBAIXA_RECEBER_ID || xCobrancas.ITEM_ID || case when xCobrancas.BOLETO_BANCARIO = ''S'' then ''/BOL'' else ''/COB'' end,'+ #13 +
      '      trunc(sysdate),'+ #13 +
      '      xCobrancas.DATA_VENCIMENTO,'+ #13 +
      '      xCobrancas.DATA_VENCIMENTO,'+ #13 +
      '      xCobrancas.VALOR,'+ #13 +
      '      ''A'','+ #13 +
      '      xCobrancas.PARCELA,'+ #13 +
      '      xCobrancas.NUMERO_PARCELAS,'+ #13 +
      '      ''BXR'','+ #13 +
      '      iBAIXA_RECEBER_ID'+ #13 +
      '    );'+ #13 +
      '  end loop;  '+ #13 +
      #13 +
      '  for xCartoes in cCartoes loop'+ #13 +
      '    GERAR_CARTOES_RECEBER_BAIXA(iBAIXA_RECEBER_ID, xCartoes.ITEM_ID, xCartoes.NSU_TEF, xCartoes.CODIGO_AUTORIZACAO, xCartoes.NUMERO_CARTAO);'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  ATUALIZAR_RETENCOES_CONTAS_REC(iBAIXA_RECEBER_ID, ''BXR'');'+ #13 +
      #13 +
      '  if vValorCredito > 0 then'+ #13 +
      '    vValorRestante := vValorTotal - (vValorDinheiro + vValorCheque + vValorCartaoDeb + vValorCartaoCred + vValorCobranca);'+ #13 +
      '    for xCreditos in cCreditos loop'+ #13 +
      '      if xCreditos.VALOR_LIQUIDO >= vValorRestante then'+ #13 +
      '        vValorBaixarCredito := vValorRestante;'+ #13 +
      '        vValorRestante := 0;'+ #13 +
      '      else'+ #13 +
      '        vValorBaixarCredito := xCreditos.VALOR_LIQUIDO;'+ #13 +
      '        vValorRestante := vValorRestante - vValorBaixarCredito;'+ #13 +
      '      end if;'+ #13 +
      #13 +
      '      BAIXAR_CREDITO_PAGAR(xCreditos.PAGAR_ID, iBAIXA_RECEBER_ID, ''BCR'', vValorBaixarCredito, vBaixaCreditoPagarId, vBaixaCreditoPagarId);'+ #13 +
      #13 +
      '      if vValorRestante = 0 then'+ #13 +
      '        exit;'+ #13 +
      '      end if;'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      'end CONSOLIDAR_BAIXA_CONTAS_REC;'+ #13
    );
end;

function AjustarProcedureINICIAR_SESSAO: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure INICIAR_SESSAO('+ #13 +
      '	iEMPRESA_ID      in number,'+ #13 +
      '  iFUNCIONARIO_ID  in number,'+ #13 +
      '  iSISTEMA         in string,'+ #13 +
      '	iNOME_COMPUTADOR in string'+ #13 +
      ')'+ #13 +
      'is'+ #13 +
      #13 +
      '  vDataValidadeModPreEntr date;'+ #13 +
      #13 +
      '  procedure SETAR_CONFIGURACOES_ORACLE is'+ #13 +
      '  begin'+ #13 +
      '    begin'+ #13 +
      '      /* Força o separador decimal */'+ #13 +
      '      execute immediate'+ #13 +
      '        ''alter session set NLS_NUMERIC_CHARACTERS = '''',.'''' '';'+ #13 +
      #13 +
      '      /* Força o formato da data */'+ #13 +
      '      execute immediate'+ #13 +
      '        ''alter session set NLS_DATE_FORMAT = ''''DD/MM/YYYY'''' '';'+ #13 +
      #13 +
      '    exception'+ #13 +
      '      when others then'+ #13 +
      '        null;'+ #13 +
      '    end;'+ #13 +
      '  end;'+ #13 +
      #13 +
      'begin'+ #13 +
      #13 +
      '  SESSAO.USUARIO_SESSAO_ID      := iFUNCIONARIO_ID;'+ #13 +
      '  SESSAO.SISTEMA                := iSISTEMA;'+ #13 +
      '	SESSAO.NOME_COMPUTADOR_SESSAO := iNOME_COMPUTADOR;'+ #13 +
      #13 +
      '  SETAR_CONFIGURACOES_ORACLE;'+ #13 +
      #13 +
      '  select'+ #13 +
      '    DATA_VALIDADE_MOD_PRE_ENTR'+ #13 +
      '  into'+ #13 +
      '    vDataValidadeModPreEntr'+ #13 +
      '  from'+ #13 +
      '    PARAMETROS;'+ #13 +
      #13 +
      '  if vDataValidadeModPreEntr is not null then'+ #13 +
      '    if trunc(sysdate) > vDataValidadeModPreEntr then'+ #13 +
      '      update PARAMETROS set'+ #13 +
      '        UTIL_MODULO_PRE_ENTRADAS = ''N'';'+ #13 +
      '    end if;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  begin'+ #13 +
      '    select *'+ #13 +
      '    into SESSAO.PARAMETROS_GERAIS'+ #13 +
      '    from PARAMETROS;'+ #13 +
      #13 +
      '    select *'+ #13 +
      '    into SESSAO.PARAMETROS_EMPRESA_LOGADA'+ #13 +
      '    from PARAMETROS_EMPRESA'+ #13 +
      '    where EMPRESA_ID = iEMPRESA_ID;'+ #13 +
      '  exception'+ #13 +
      '    when others then'+ #13 +
      '      if iSISTEMA <> ''IMPORTADOR'' then'+ #13 +
      '        raise;'+ #13 +
      '      end if;'+ #13 +
      '  end;'+ #13 +
      #13 +
      '  delete from FILTROS_PADROES_TELAS_USUARIO'+ #13 +
      '  where TELA = ''tformpesquisavendas'''+ #13 +
      '  and CAMPO in (''eproduto'', ''emarca'')'+ #13 +
      '  and USUARIO_ID = iFUNCIONARIO_ID;'+ #13 +
      #13 +
      'end INICIAR_SESSAO;'+ #13
    );
end;

function AjustarColunaPLANO_FINANCEIRO_ID: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'alter table AJUSTES_ESTOQUE'+ #13 +
      'modify PLANO_FINANCEIRO_ID varchar2(9) null;'+ #13
    );
end;

function AtualizarTriggerCONTAS_PAGAR_U_AR: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace trigger CONTAS_PAGAR_U_AR'+ #13 +
      'after update'+ #13 +
      'on CONTAS_PAGAR'+ #13 +
      'for each row'+ #13 +
      'declare'+ #13 +
      #13 +
      '  procedure INSERIR_LOG(pTIPO_ALTERACAO_LOG_ID in number, pVALOR_ANTERIOR in string, pNOVO_VALOR in string)'+ #13 +
      '  is begin'+ #13 +
      '    if nvl(pVALOR_ANTERIOR, ''X'') <> nvl(pNOVO_VALOR, ''X'') then'+ #13 +
      '      insert into LOGS_CONTAS_PAGAR(TIPO_ALTERACAO_LOG_ID, PAGAR_ID, VALOR_ANTERIOR, NOVO_VALOR)'+ #13 +
      '      values(pTIPO_ALTERACAO_LOG_ID, :new.PAGAR_ID, pVALOR_ANTERIOR, pNOVO_VALOR);'+ #13 +
      '    end if;'+ #13 +
      '  end;'+ #13 +
      #13 +
      'begin'+ #13 +
      '  /* View VW_TIPOS_ALTER_LOGS_CT_PAGAR */'+ #13 +
      #13 +
      '  INSERIR_LOG(1, :old.BAIXA_ID, :new.BAIXA_ID);'+ #13 +
      '  INSERIR_LOG(2, :old.STATUS, :new.STATUS);'+ #13 +
      '  INSERIR_LOG(3, :old.DATA_VENCIMENTO, :new.DATA_VENCIMENTO);'+ #13 +
      '  INSERIR_LOG(4, :old.BLOQUEADO, :new.BLOQUEADO);'+ #13 +
      '  INSERIR_LOG(5, :old.MOTIVO_BLOQUEIO, :new.MOTIVO_BLOQUEIO);'+ #13 +
      '  INSERIR_LOG(6, :old.DOCUMENTO, :new.DOCUMENTO);'+ #13 +
      '  INSERIR_LOG(7, :old.VALOR_DESCONTO, :new.VALOR_DESCONTO);'+ #13 +
      '  INSERIR_LOG(8, :old.VALOR_DOCUMENTO, :new.VALOR_DOCUMENTO);'+ #13 +
      '  INSERIR_LOG(9, :old.PLANO_FINANCEIRO_ID, :new.PLANO_FINANCEIRO_ID);'+ #13 +
      #13 +
      'end CONTAS_PAGAR_U_AR;'+ #13
    );
end;

function AtualizarViewVW_TIPOS_ALTER_LOGS_CT_PAGAR: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace view VW_TIPOS_ALTER_LOGS_CT_PAGAR'+ #13 +
      'as'+ #13 +
      'select 1 as TIPO_ALTERACAO_LOG_ID, ''Código da baixa'' as CAMPO from dual union'+ #13 +
      'select 2 as TIPO_ALTERACAO_LOG_ID, ''Status'' as CAMPO from dual union'+ #13 +
      'select 3 as TIPO_ALTERACAO_LOG_ID, ''Data de vencimento'' as CAMPO from dual union'+ #13 +
      'select 4 as TIPO_ALTERACAO_LOG_ID, ''Bloqueado'' as CAMPO from dual union'+ #13 +
      'select 5 as TIPO_ALTERACAO_LOG_ID, ''Motivo do bloqueio'' as CAMPO from dual union'+ #13 +
      'select 6 as TIPO_ALTERACAO_LOG_ID, ''Documento'' as CAMPO from dual union'+ #13 +
      'select 7 as TIPO_ALTERACAO_LOG_ID, ''Valor do desconto'' as CAMPO from dual union'+ #13 +
      'select 8 as TIPO_ALTERACAO_LOG_ID, ''Valor do documento'' as CAMPO from dual union'+ #13 +
      'select 9 as TIPO_ALTERACAO_LOG_ID, ''Plano financeiro'' as CAMPO from dual;'+ #13
    );
end;

function AtualizarProcRECEBER_ACUMULADO: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure RECEBER_ACUMULADO('+ #13 +
      '  iACUMULADO_ID        in number,'+ #13 +
      '  iTURNO_ID            in number,'+ #13 +
      '  iVALOR_TROCO         in number,'+ #13 +
      '  iGERAR_CREDITO_TROCO in string,'+ #13 +
      '  iTIPO_NOTA_GERAR     in string'+ #13 +
      ')'+ #13 +
      'is'+ #13 +
      '  i                       naturaln default 0;  '+ #13 +
      '  vReceberId              CONTAS_RECEBER.RECEBER_ID%type;'+ #13 +
      '  vBaixaId                CONTAS_RECEBER_BAIXAS.BAIXA_ID%type;'+ #13 +
      '  vDadosPagamentoPedido   RecordsVendas.RecPedidosPagamentos;'+ #13 +
      #13 +
      '  cursor cCheques is'+ #13 +
      '  select'+ #13 +
      '    COBRANCA_ID,'+ #13 +
      '    ITEM_ID,'+ #13 +
      '    DATA_VENCIMENTO,'+ #13 +
      '    BANCO,'+ #13 +
      '    AGENCIA,'+ #13 +
      '    CONTA_CORRENTE,'+ #13 +
      '    NUMERO_CHEQUE,'+ #13 +
      '    VALOR_CHEQUE,'+ #13 +
      '    NOME_EMITENTE,'+ #13 +
      '    CPF_CNPJ_EMITENTE,'+ #13 +
      '    TELEFONE_EMITENTE,'+ #13 +
      '    PARCELA,'+ #13 +
      '    NUMERO_PARCELAS'+ #13 +
      '  from'+ #13 +
      '    ACUMULADOS_PAGAMENTOS_CHEQUES'+ #13 +
      '  where ACUMULADO_ID = iACUMULADO_ID'+ #13 +
      '  order by '+ #13 +
      '		COBRANCA_ID, '+ #13 +
      '		PARCELA,'+ #13 +
      '		DATA_VENCIMENTO, '+ #13 +
      '		ITEM_ID;'+ #13 +
      #13 +
      '  cursor cCobrancas(pTIPO in string) is'+ #13 +
      '  select'+ #13 +
      '    PAG.COBRANCA_ID,'+ #13 +
      '    PAG.ITEM_ID,'+ #13 +
      '    PAG.VALOR,'+ #13 +
      '    PAG.DATA_VENCIMENTO,'+ #13 +
      '    PAG.PARCELA,'+ #13 +
      '    PAG.NUMERO_PARCELAS,'+ #13 +
      '    TIP.BOLETO_BANCARIO,'+ #13 +
      '    PAG.NSU_TEF,'+ #13 +
      '    PAG.CODIGO_AUTORIZACAO,'+ #13 +
      '    PAG.NUMERO_CARTAO,'+ #13 +
      '    TIP.PORTADOR_ID'+ #13 +
      '  from'+ #13 +
      '    ACUMULADOS_PAGAMENTOS PAG'+ #13 +
      #13 +
      '  join TIPOS_COBRANCA TIP'+ #13 +
      '  on PAG.COBRANCA_ID = TIP.COBRANCA_ID    '+ #13 +
      '  and TIP.FORMA_PAGAMENTO = pTIPO'+ #13 +
      #13 +
      '  where PAG.ACUMULADO_ID = iACUMULADO_ID  '+ #13 +
      '  order by '+ #13 +
      '		COBRANCA_ID,'+ #13 +
      '		PARCELA,'+ #13 +
      '		DATA_VENCIMENTO, '+ #13 +
      '		ITEM_ID;'+ #13 +
      #13 +
      'begin'+ #13 +
      #13 +
      '  begin'+ #13 +
      '    select'+ #13 +
      '      VALOR_DINHEIRO,'+ #13 +
      '      VALOR_CARTAO_DEBITO,'+ #13 +
      '      VALOR_CARTAO_CREDITO,'+ #13 +
      '      VALOR_CREDITO,'+ #13 +
      '      VALOR_COBRANCA,'+ #13 +
      '      VALOR_CHEQUE,'+ #13 +
      '      VALOR_FINANCEIRA,      '+ #13 +
      '      STATUS,      '+ #13 +
      '      EMPRESA_ID,'+ #13 +
      '      CLIENTE_ID'+ #13 +
      '    into'+ #13 +
      '      vDadosPagamentoPedido.VALOR_DINHEIRO,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_CARTAO_DEBITO,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_CARTAO_CREDITO,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_CREDITO,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_COBRANCA,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_CHEQUE,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_FINANCEIRA,      '+ #13 +
      '      vDadosPagamentoPedido.STATUS,      '+ #13 +
      '      vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
      '      vDadosPagamentoPedido.CLIENTE_ID'+ #13 +
      '    from'+ #13 +
      '      ACUMULADOS'+ #13 +
      '    where ACUMULADO_ID = iACUMULADO_ID;'+ #13 +
      '  exception'+ #13 +
      '    when others then'+ #13 +
      '      ERRO(''Não foi encontrado os valores de pagamento para o acumulado '' || iACUMULADO_ID || ''!'' || chr(13) || chr(10) || sqlerrm);'+ #13 +
      '  end;'+ #13 +
      #13 +
      '  if vDadosPagamentoPedido.STATUS = ''RE'' then'+ #13 +
      '    ERRO(''Este acumulado já foi recebido!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if vDadosPagamentoPedido.STATUS not in(''AR'') then'+ #13 +
      '    ERRO(''Este acumulado não está aguardando recebimento, verifique!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  /* Inserindo cheques no financeiro */'+ #13 +
      '  if vDadosPagamentoPedido.VALOR_CHEQUE > 0 then'+ #13 +
      '    for vCheques in cCheques loop'+ #13 +
      '      select SEQ_RECEBER_ID.nextval'+ #13 +
      '      into vReceberId'+ #13 +
      '      from dual;'+ #13 +
      #13 +
      '      insert into CONTAS_RECEBER('+ #13 +
      '        RECEBER_ID,'+ #13 +
      '        CADASTRO_ID,'+ #13 +
      '        EMPRESA_ID,'+ #13 +
      '        ACUMULADO_ID,'+ #13 +
      '        COBRANCA_ID,'+ #13 +
      '        PORTADOR_ID,'+ #13 +
      '        PLANO_FINANCEIRO_ID,'+ #13 +
      '        TURNO_ID,        '+ #13 +
      '        DOCUMENTO,'+ #13 +
      '        BANCO,'+ #13 +
      '        AGENCIA,'+ #13 +
      '        CONTA_CORRENTE,'+ #13 +
      '        NUMERO_CHEQUE,'+ #13 +
      '        NOME_EMITENTE,'+ #13 +
      '        TELEFONE_EMITENTE,'+ #13 +
      '        CPF_CNPJ_EMITENTE,'+ #13 +
      '        DATA_EMISSAO,'+ #13 +
      '        DATA_VENCIMENTO,'+ #13 +
      '        DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '        VALOR_DOCUMENTO,'+ #13 +
      '        STATUS,'+ #13 +
      '        PARCELA,'+ #13 +
      '        NUMERO_PARCELAS,'+ #13 +
      '        ORIGEM'+ #13 +
      '      )values('+ #13 +
      '        vReceberId,'+ #13 +
      '        vDadosPagamentoPedido.CLIENTE_ID,'+ #13 +
      '        vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
      '        iACUMULADO_ID,'+ #13 +
      '        vCheques.COBRANCA_ID,'+ #13 +
      '        ''9998'','+ #13 +
      '        ''1.001.002'','+ #13 +
      '        iTURNO_ID,        '+ #13 +
      '        ''ACU-'' || iACUMULADO_ID || '' - '' || vCheques.ITEM_ID || ''/CHQ'','+ #13 +
      '        vCheques.BANCO,'+ #13 +
      '        vCheques.AGENCIA,'+ #13 +
      '        vCheques.CONTA_CORRENTE,'+ #13 +
      '        vCheques.NUMERO_CHEQUE,'+ #13 +
      '        vCheques.NOME_EMITENTE,'+ #13 +
      '        vCheques.CPF_CNPJ_EMITENTE,'+ #13 +
      '        vCheques.TELEFONE_EMITENTE,'+ #13 +
      '        trunc(sysdate),'+ #13 +
      '        vCheques.DATA_VENCIMENTO,'+ #13 +
      '        vCheques.DATA_VENCIMENTO,'+ #13 +
      '        vCheques.VALOR_CHEQUE,'+ #13 +
      '        ''A'','+ #13 +
      '        vCheques.PARCELA,'+ #13 +
      '        vCheques.NUMERO_PARCELAS,'+ #13 +
      '        ''ACU'''+ #13 +
      '      );'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if vDadosPagamentoPedido.VALOR_COBRANCA > 0 then'+ #13 +
      '    for vCobrancas in cCobrancas(''COB'') loop'+ #13 +
      '      select SEQ_RECEBER_ID.nextval'+ #13 +
      '      into vReceberId'+ #13 +
      '      from dual;'+ #13 +
      #13 +
      '      insert into CONTAS_RECEBER('+ #13 +
      '        RECEBER_ID,'+ #13 +
      '        CADASTRO_ID,'+ #13 +
      '        EMPRESA_ID,'+ #13 +
      '        ACUMULADO_ID,'+ #13 +
      '        COBRANCA_ID,'+ #13 +
      '        PORTADOR_ID,'+ #13 +
      '        PLANO_FINANCEIRO_ID,'+ #13 +
      '        TURNO_ID,        '+ #13 +
      '        DOCUMENTO,'+ #13 +
      '        DATA_EMISSAO,'+ #13 +
      '        DATA_VENCIMENTO,'+ #13 +
      '        DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '        VALOR_DOCUMENTO,'+ #13 +
      '        STATUS,'+ #13 +
      '        PARCELA,'+ #13 +
      '        NUMERO_PARCELAS,'+ #13 +
      '        ORIGEM'+ #13 +
      '      )values('+ #13 +
      '        vReceberId,'+ #13 +
      '        vDadosPagamentoPedido.CLIENTE_ID,'+ #13 +
      '        vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
      '        iACUMULADO_ID,'+ #13 +
      '        vCobrancas.COBRANCA_ID,'+ #13 +
      '        vCobrancas.PORTADOR_ID,'+ #13 +
      '        case when vCobrancas.BOLETO_BANCARIO = ''N'' then ''1.001.005'' else ''1.001.006'' end,'+ #13 +
      '        iTURNO_ID,        '+ #13 +
      '        ''ACU-'' || iACUMULADO_ID  || '' - '' || vCobrancas.ITEM_ID || case when vCobrancas.BOLETO_BANCARIO = ''S'' then ''/BOL'' else ''/COB'' end,'+ #13 +
      '        trunc(sysdate),'+ #13 +
      '        vCobrancas.DATA_VENCIMENTO,'+ #13 +
      '        vCobrancas.DATA_VENCIMENTO,'+ #13 +
      '        vCobrancas.VALOR,'+ #13 +
      '        ''A'','+ #13 +
      '        vCobrancas.PARCELA,'+ #13 +
      '        vCobrancas.NUMERO_PARCELAS,'+ #13 +
      '        ''ACU'''+ #13 +
      '      );'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if iGERAR_CREDITO_TROCO = ''S'' and iVALOR_TROCO > 0 then'+ #13 +
      '    GERAR_CREDITO_TROCO(iACUMULADO_ID, ''ACU'', vDadosPagamentoPedido.CLIENTE_ID, iVALOR_TROCO);'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  update ACUMULADOS set'+ #13 +
      '    VALOR_TROCO = case when iGERAR_CREDITO_TROCO = ''N'' then iVALOR_TROCO else 0 end,'+ #13 +
      '    TURNO_ID = iTURNO_ID,'+ #13 +
      '    STATUS = ''RE'','+ #13 +
      '    TIPO_NOTA_GERAR = iTIPO_NOTA_GERAR'+ #13 +
      '  where ACUMULADO_ID = iACUMULADO_ID;'+ #13 +
      #13 +
      '  /* Não alterar este processo!!! Ele deve ser depois do recebimento */'+ #13 +
      '  if vDadosPagamentoPedido.VALOR_CARTAO_DEBITO + vDadosPagamentoPedido.VALOR_CARTAO_CREDITO > 0 then'+ #13 +
      '    for vCobrancas in cCobrancas(''CRT'') loop'+ #13 +
      '      GERAR_CARTOES_REC_ACUMULADO(iACUMULADO_ID, vCobrancas.ITEM_ID, vCobrancas.NSU_TEF, vCobrancas.CODIGO_AUTORIZACAO, vCobrancas.NUMERO_CARTAO);'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  ATUALIZAR_RETENCOES_CONTAS_REC(iACUMULADO_ID, ''ACU'');'+ #13 +
      #13 +
      '  GERAR_NOTA_ACUMULADO(iACUMULADO_ID);'+ #13 +
      #13 +
      'end RECEBER_ACUMULADO;'+ #13
    );
end;

function AtualizarProcGERAR_CREDITO_TROCO: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure GERAR_CREDITO_TROCO('+ #13 +
      '  iMOVIMENTO_ID in number,'+ #13 +
      '  iTIPO_MOVIMENTO in string,'+ #13 +
      '  iCLIENTE_ID in number,'+ #13 +
      '  iVALOR_TROCO in number'+ #13 +
      ')'+ #13 +
      'is'+ #13 +
      '  vClienteId   CONTAS_PAGAR.CADASTRO_ID%type;'+ #13 +
      '  vEmpresaId   CONTAS_PAGAR.EMPRESA_ID%type;'+ #13 +
      '  vPagarId     CONTAS_PAGAR.PAGAR_ID%type;  '+ #13 +
      '  vCobrancaId  PARAMETROS.TIPO_COBRANCA_GERACAO_CRED_ID%type;  '+ #13 +
      '  vIndice      CONTAS_PAGAR.INDICE_CONDICAO_PAGAMENTO%type;  '+ #13 +
      'begin'+ #13 +
      #13 +
      '  if iTIPO_MOVIMENTO = ''ORC'' then'+ #13 +
      '    select'+ #13 +
      '      ORC.EMPRESA_ID,'+ #13 +
      '      ORC.CLIENTE_ID,'+ #13 +
      '      ORC.INDICE_CONDICAO_PAGAMENTO'+ #13 +
      '    into'+ #13 +
      '      vEmpresaId,'+ #13 +
      '      vClienteId,'+ #13 +
      '      vIndice'+ #13 +
      '    from'+ #13 +
      '      ORCAMENTOS ORC'+ #13 +
      '    where ORC.ORCAMENTO_ID = iMOVIMENTO_ID;'+ #13 +
      '  else'+ #13 +
      '    select'+ #13 +
      '      ACU.EMPRESA_ID,'+ #13 +
      '      ACU.CLIENTE_ID,'+ #13 +
      '      max(ORC.INDICE_CONDICAO_PAGAMENTO)'+ #13 +
      '    into'+ #13 +
      '      vEmpresaId,'+ #13 +
      '      vClienteId,'+ #13 +
      '      vIndice'+ #13 +
      '    from'+ #13 +
      '      ACUMULADOS ACU'+ #13 +
      #13 +
      '    inner join ORCAMENTOS ORC'+ #13 +
      '    on ACU.ACUMULADO_ID = ORC.ACUMULADO_ID'+ #13 +
      #13 +
      '    where ACU.ACUMULADO_ID = iMOVIMENTO_ID'+ #13 +
      '    group by'+ #13 +
      '      ACU.EMPRESA_ID,'+ #13 +
      '      ACU.CLIENTE_ID;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if vClienteId = SESSAO.PARAMETROS_GERAIS.CADASTRO_CONSUMIDOR_FINAL_ID then'+ #13 +
      '    ERRO(''Não é permitido gerar crédito para consumidor final!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  select'+ #13 +
      '    TIPO_COBRANCA_GERACAO_CRED_ID'+ #13 +
      '  into'+ #13 +
      '    vCobrancaId'+ #13 +
      '  from'+ #13 +
      '    PARAMETROS;'+ #13 +
      #13 +
      '  if vCobrancaId is null then'+ #13 +
      '    ERRO(''O tipo de cobrança para geração do crédito não foi parametrizado, faça a parametrização nos "Parâmetros"!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  select SEQ_PAGAR_ID.nextval'+ #13 +
      '  into vPagarId'+ #13 +
      '  from dual;'+ #13 +
      #13 +
      '  insert into CONTAS_PAGAR('+ #13 +
      '    PAGAR_ID,'+ #13 +
      '    CADASTRO_ID,'+ #13 +
      '    EMPRESA_ID,'+ #13 +
      '    DOCUMENTO,'+ #13 +
      '    ORIGEM,'+ #13 +
      '    ORCAMENTO_ID,'+ #13 +
      '    ACUMULADO_ID,'+ #13 +
      '    COBRANCA_ID,'+ #13 +
      '    PORTADOR_ID,'+ #13 +
      '    PLANO_FINANCEIRO_ID,'+ #13 +
      '    DATA_VENCIMENTO,'+ #13 +
      '    DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '    VALOR_DOCUMENTO,'+ #13 +
      '    STATUS,'+ #13 +
      '    PARCELA,'+ #13 +
      '    NUMERO_PARCELAS,'+ #13 +
      '    INDICE_CONDICAO_PAGAMENTO'+ #13 +
      '  )values('+ #13 +
      '    vPagarId,'+ #13 +
      '    vClienteId,'+ #13 +
      '    vEmpresaId,'+ #13 +
      '    case when iTIPO_MOVIMENTO = ''ORC'' then ''CVE-'' || NFORMAT(iMOVIMENTO_ID, 0) else ''CAC-'' || NFORMAT(iMOVIMENTO_ID, 0) end, -- Documento'+ #13 +
      '    case when iTIPO_MOVIMENTO = ''ORC'' then ''CVE'' else ''CAC'' end,'+ #13 +
      '    case when iTIPO_MOVIMENTO = ''ORC'' then iMOVIMENTO_ID else null end,'+ #13 +
      '    case when iTIPO_MOVIMENTO = ''ACU'' then iMOVIMENTO_ID else null end,'+ #13 +
      '    vCobrancaId,'+ #13 +
      '    ''9999'','+ #13 +
      '    ''1.002.001'','+ #13 +
      '    trunc(sysdate),'+ #13 +
      '    trunc(sysdate),'+ #13 +
      '    iVALOR_TROCO,'+ #13 +
      '    ''A'','+ #13 +
      '    1,'+ #13 +
      '    1,'+ #13 +
      '    vIndice'+ #13 +
      '  );'+ #13 +
      #13 +
      'end GERAR_CREDITO_TROCO;'+ #13
    );
end;

function AjustarProcedureGERAR_NOTA_TRANSF_MOVIMENTOS: RecScript;
  function GetComando1: string;
  begin
    Result :=
      'create or replace procedure GERAR_NOTA_TRANSF_MOVIMENTOS('+ #13 +
      '  iEMPRESA_ORIGEM_ID in number,'+ #13 +
      '  iEMPRESA_DESTINO_ID in number,'+ #13 +
      '  iDATA_INICIAL in date,'+ #13 +
      '  iDATA_FINAL in date,'+ #13 +
      '  oNOTA_FISCAL_ID out number'+ #13 +
      ')'+ #13 +
      'is'+ #13 +
      '  i                       number default 0;'+ #13 +
      '  vQtde                   number default 0;'+ #13 +
      '  vValorMaisSignificativo number default 0;'+ #13 +
      #13 +
      '  vNotaFiscalId           NOTAS_FISCAIS.NOTA_FISCAL_ID%type;'+ #13 +
      '  vSerieNota              NOTAS_FISCAIS.SERIE_NOTA%type;'+ #13 +
      #13 +
      '  vCfopIdCapa             NOTAS_FISCAIS.CFOP_ID%type;'+ #13 +
      '  vNaturezaOperacao       NOTAS_FISCAIS.NATUREZA_OPERACAO%type;'+ #13 +
      '  vDadosNota              RecordsNotasFiscais.RecNotaFiscal;'+ #13 +
      '  vDadosItens             RecordsNotasFiscais.ArrayOfRecNotasFiscaisItens;'+ #13 +
      #13 +
      '  vRegimeTributario       PARAMETROS_EMPRESA.REGIME_TRIBUTARIO%type;'+ #13 +
      '  vSerieNFe               PARAMETROS_EMPRESA.SERIE_NFE%type;'+ #13 +
      '  vTipoEmpresaOrigem      EMPRESAS.TIPO_EMPRESA%type;'+ #13 +
      #13 +
      '  cursor cItens is'+ #13 +
      '  select'+ #13 +
      '    PRODUTO_ID,'+ #13 +
      '    sum(QUANTIDADE) as QUANTIDADE,'+ #13 +
      '    NOME_PRODUTO,'+ #13 +
      '    PRECO_UNITARIO,'+ #13 +
      '    CODIGO_NCM,'+ #13 +
      '    UNIDADE_VENDA,'+ #13 +
      '    CODIGO_BARRAS,'+ #13 +
      '    CEST'+ #13 +
      '  from'+ #13 +
      '    VW_MOV_ITE_PEND_EMISSAO_NF_TR'+ #13 +
      '  where EMPRESA_ORIGEM_ID = iEMPRESA_ORIGEM_ID'+ #13 +
      '  and EMPRESA_DESTINO_ID = iEMPRESA_DESTINO_ID'+ #13 +
      '  and DATA_MOVIMENTO between iDATA_INICIAL and iDATA_FINAL'+ #13 +
      '  group by'+ #13 +
      '    PRODUTO_ID,'+ #13 +
      '    NOME_PRODUTO,'+ #13 +
      '    PRECO_UNITARIO,'+ #13 +
      '    CODIGO_NCM,'+ #13 +
      '    UNIDADE_VENDA,'+ #13 +
      '    CODIGO_BARRAS,'+ #13 +
      '    CEST;'+ #13 +
      #13 +
      'begin'+ #13 +
      '  vDadosNota.EMPRESA_ID := iEMPRESA_ORIGEM_ID;'+ #13 +
      #13 +
      '  begin'+ #13 +
      '    select'+ #13 +
      '      CAD.CADASTRO_ID,'+ #13 +
      #13 +
      '      /* Emitente */'+ #13 +
      '      EMP.RAZAO_SOCIAL,'+ #13 +
      '      EMP.NOME_FANTASIA,'+ #13 +
      '      EMP.CNPJ,'+ #13 +
      '      EMP.INSCRICAO_ESTADUAL,'+ #13 +
      '      EMP.LOGRADOURO,'+ #13 +
      '      EMP.COMPLEMENTO,'+ #13 +
      '      BAE.NOME as NOME_BAIRRO_EMITENTE,'+ #13 +
      '      CIE.NOME as NOME_CIDADE_EMITENTE,'+ #13 +
      '      EMP.NUMERO,'+ #13 +
      '      ESE.ESTADO_ID,'+ #13 +
      '      EMP.CEP,'+ #13 +
      '      EMP.TELEFONE_PRINCIPAL,'+ #13 +
      '      CIE.CODIGO_IBGE as CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '      ESE.CODIGO_IBGE as CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      #13 +
      '      /* Destinatario */'+ #13 +
      '      CAD.NOME_FANTASIA,'+ #13 +
      '      CAD.RAZAO_SOCIAL,'+ #13 +
      '      CAD.TIPO_PESSOA,'+ #13 +
      '      CAD.CPF_CNPJ,'+ #13 +
      '      CAD.INSCRICAO_ESTADUAL,'+ #13 +
      '      CAD.LOGRADOURO,'+ #13 +
      '      CAD.COMPLEMENTO,'+ #13 +
      '      CAD.NUMERO,'+ #13 +
      '      CAD.CEP,'+ #13 +
      '      BAI.NOME as NOME_BAIRRO,'+ #13 +
      '      CID.NOME as NOME_CIDADE,'+ #13 +
      '      CID.CODIGO_IBGE as CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '      CID.ESTADO_ID as ESTADO_ID_DESTINATARIO,'+ #13 +
      #13 +
      '      PAE.REGIME_TRIBUTARIO,'+ #13 +
      '      PAE.SERIE_NFE,'+ #13 +
      '      EMP.TIPO_EMPRESA'+ #13 +
      '    into'+ #13 +
      '      vDadosNota.CADASTRO_ID,'+ #13 +
      #13 +
      '      /* Emitente */'+ #13 +
      '      vDadosNota.RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_FANTASIA_EMITENTE,'+ #13 +
      '      vDadosNota.CNPJ_EMITENTE,'+ #13 +
      '      vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '      vDadosNota.LOGRADOURO_EMITENTE,'+ #13 +
      '      vDadosNota.COMPLEMENTO_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_BAIRRO_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_CIDADE_EMITENTE,'+ #13 +
      '      vDadosNota.NUMERO_EMITENTE,'+ #13 +
      '      vDadosNota.ESTADO_ID_EMITENTE,'+ #13 +
      '      vDadosNota.CEP_EMITENTE,'+ #13 +
      '      vDadosNota.TELEFONE_EMITENTE,'+ #13 +
      '      vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '      vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      #13 +
      '      /* Destinatario */'+ #13 +
      '      vDadosNota.NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '      vDadosNota.RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '      vDadosNota.TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '      vDadosNota.CPF_CNPJ_DESTINATARIO,'+ #13 +
      '      vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '      vDadosNota.LOGRADOURO_DESTINATARIO,'+ #13 +
      '      vDadosNota.COMPLEMENTO_DESTINATARIO,'+ #13 +
      '      vDadosNota.NUMERO_DESTINATARIO,'+ #13 +
      '      vDadosNota.CEP_DESTINATARIO,'+ #13 +
      '      vDadosNota.NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '      vDadosNota.NOME_CIDADE_DESTINATARIO,'+ #13 +
      '      vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '      vDadosNota.ESTADO_ID_DESTINATARIO,'+ #13 +
      #13 +
      '      /* Parâmetros da empresa emitente */'+ #13 +
      '      vRegimeTributario,'+ #13 +
      '      vSerieNFe,'+ #13 +
      '      vTipoEmpresaOrigem'+ #13 +
      '    from'+ #13 +
      '      EMPRESAS EMP'+ #13 +
      #13 +
      '    inner join EMPRESAS EMD'+ #13 +
      '    on EMD.EMPRESA_ID = iEMPRESA_DESTINO_ID'+ #13 +
      #13 +
      '    inner join CADASTROS CAD'+ #13 +
      '    on EMD.CADASTRO_ID = CAD.CADASTRO_ID'+ #13 +
      #13 +
      '    inner join BAIRROS BAI'+ #13 +
      '    on CAD.BAIRRO_ID = BAI.BAIRRO_ID'+ #13 +
      #13 +
      '    inner join CIDADES CID'+ #13 +
      '    on BAI.CIDADE_ID = CID.CIDADE_ID'+ #13 +
      #13 +
      '    inner join BAIRROS BAE'+ #13 +
      '    on EMP.BAIRRO_ID = BAE.BAIRRO_ID'+ #13 +
      #13 +
      '    inner join CIDADES CIE'+ #13 +
      '    on BAE.CIDADE_ID = CIE.CIDADE_ID'+ #13 +
      #13 +
      '    inner join ESTADOS ESE'+ #13 +
      '    on CIE.ESTADO_ID = ESE.ESTADO_ID'+ #13 +
      #13 +
      '    inner join PARAMETROS_EMPRESA PAE'+ #13 +
      '    on PAE.EMPRESA_ID = EMP.EMPRESA_ID'+ #13 +
      #13 +
      '    cross join PARAMETROS PAR'+ #13 +
      #13 +
      '    where EMP.EMPRESA_ID = iEMPRESA_ORIGEM_ID;'+ #13 +
      '  exception'+ #13 +
      '    when others then'+ #13 +
      '      ERRO(''Houve um problema ao buscar os dados das empresas para geração da NFe! '' + sqlerrm);'+ #13 +
      '  end;'+ #13 +
      #13 +
      '  vDadosNota.BASE_CALCULO_ICMS    := 0;'+ #13 +
      '  vDadosNota.VALOR_ICMS           := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_ICMS_ST := 0;'+ #13 +
      '  vDadosNota.VALOR_ICMS_ST        := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_PIS     := 0;'+ #13 +
      '  vDadosNota.VALOR_PIS            := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_COFINS  := 0;'+ #13 +
      '  vDadosNota.VALOR_COFINS         := 0;'+ #13 +
      '  vDadosNota.VALOR_IPI            := 0;'+ #13 +
      '  vDadosNota.VALOR_FRETE          := 0;'+ #13 +
      '  vDadosNota.VALOR_SEGURO         := 0;'+ #13 +
      '  vDadosNota.PESO_LIQUIDO         := 0;'+ #13 +
      '  vDadosNota.PESO_BRUTO           := 0;'+ #13 +
      #13 +
      '  for vItens in cItens loop'+ #13 +
      '    if length(vItens.CODIGO_NCM) < 8 then'+ #13 +
      '      Erro(''O código NCM do produto '' || vItens.PRODUTO_ID || '' - '' || vItens.NOME_PRODUTO || '' está incorreto, o NCM é composto de 8 digítos, verifique no cadastro de produtos!'');'+ #13 +
      '    end if;'+ #13 +
      #13 +
      '    vDadosItens(i).PRODUTO_ID    := vItens.PRODUTO_ID;'+ #13 +
      '    vDadosItens(i).ITEM_ID       := i + 1;'+ #13 +
      '    vDadosItens(i).NOME_PRODUTO  := vItens.NOME_PRODUTO;'+ #13 +
      '    vDadosItens(i).UNIDADE       := vItens.UNIDADE_VENDA;'+ #13 +
      '    vDadosItens(i).CODIGO_NCM    := vItens.CODIGO_NCM;'+ #13 +
      '    vDadosItens(i).QUANTIDADE    := vItens.QUANTIDADE;'+ #13 +
      '    vDadosItens(i).CODIGO_BARRAS := vItens.CODIGO_BARRAS;'+ #13 +
      '    vDadosItens(i).CEST          := vItens.CEST;'+ #13 +
      #13 +
      '    vDadosItens(i).PRECO_UNITARIO              := vItens.PRECO_UNITARIO;'+ #13 +
      '    vDadosItens(i).VALOR_TOTAL                 := round(vDadosItens(i).PRECO_UNITARIO * vItens.QUANTIDADE, 2);'+ #13 +
      '    vDadosItens(i).VALOR_TOTAL_DESCONTO        := 0;'+ #13 +
      '    vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS := 0;'+ #13 +
      #13 +
      '    /* Buscando os dados de impostos item a item da venda */'+ #13 +
      '    BUSCAR_CALC_IMPOSTOS_PRODUTO('+ #13 +
      '      vItens.PRODUTO_ID,'+ #13 +
      '      vDadosNota.EMPRESA_ID,'+ #13 +
      '      vDadosNota.CADASTRO_ID,'+ #13 +
      #13 +
      '      vDadosItens(i).VALOR_TOTAL,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO,'+ #13 +
      #13 +
      '      vDadosItens(i).CST,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS,'+ #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS,'+ #13 +
      #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS_ST,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS_ST,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST,'+ #13 +
      '      vDadosItens(i).PRECO_PAUTA,'+ #13 +
      '      vDadosItens(i).IVA,'+ #13 +
      #13 +
      '      vDadosItens(i).VALOR_IPI,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_IPI,'+ #13 +
      #13 +
      '      vDadosItens(i).CST_PIS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_PIS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_PIS,'+ #13 +
      '      vDadosItens(i).VALOR_PIS,'+ #13 +
      #13 +
      '      vDadosItens(i).CST_COFINS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_COFINS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_COFINS,'+ #13 +
      '      vDadosItens(i).VALOR_COFINS'+ #13 +
      '		);'+ #13 +
      #13 +
      '    /* Se for para deposito fechado, zerando os impostos */'+ #13 +
      '    if vTipoEmpresaOrigem = ''D'' then'+ #13 +
      '      vDadosItens(i).CST                  := ''41'';'+ #13 +
      '      vDadosItens(i).CST_PIS              := ''08'';'+ #13 +
      '      vDadosItens(i).CST_COFINS           := ''08'';'+ #13 +
      #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS    := 0;'+ #13 +
      '      vDadosItens(i).VALOR_ICMS           := 0;'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS_ST := 0;'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST        := 0;'+ #13 +
      #13 +
      '      vDadosItens(i).BASE_CALCULO_PIS     := 0;'+ #13 +
      '      vDadosItens(i).PERCENTUAL_PIS       := 0;'+ #13 +
      '      vDadosItens(i).VALOR_PIS            := 0;'+ #13 +
      #13 +
      '      vDadosItens(i).BASE_CALCULO_COFINS  := 0;'+ #13 +
      '      vDadosItens(i).PERCENTUAL_COFINS    := 0;'+ #13 +
      '      vDadosItens(i).VALOR_COFINS         := 0;'+ #13 +
      #13 +
      '      vDadosItens(i).CFOP_ID := BUSCAR_CFOP_ID_OPERACAO(vDadosNota.EMPRESA_ID, vDadosItens(i).CST, ''RRD'', ''I'');'+ #13 +
      '    else'+ #13 +
      '      vDadosItens(i).CFOP_ID := BUSCAR_CFOP_ID_OPERACAO(vDadosNota.EMPRESA_ID, vDadosItens(i).CST, ''TPE'', ''I'');'+ #13 +
      '    end if;'+ #13 +
      #13 +
      '		vDadosItens(i).NATUREZA_OPERACAO := BUSCAR_CFOP_NATUREZA_OPERACAO(vDadosItens(i).CFOP_ID);'+ #13 +
      #13 +
      '    /* --------------------------- Preenchendo os dados da capa da nota ------------------------------------- */'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS    := vDadosNota.BASE_CALCULO_ICMS + vDadosItens(i).BASE_CALCULO_ICMS;'+ #13 +
      '    vDadosNota.VALOR_ICMS           := vDadosNota.VALOR_ICMS + vDadosItens(i).VALOR_ICMS;'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS_ST := vDadosNota.BASE_CALCULO_ICMS_ST + vDadosItens(i).BASE_CALCULO_ICMS_ST;'+ #13 +
      '    vDadosNota.VALOR_ICMS_ST        := vDadosNota.VALOR_ICMS_ST + vDadosItens(i).VALOR_ICMS_ST;'+ #13 +
      '    vDadosNota.BASE_CALCULO_PIS     := vDadosNota.BASE_CALCULO_PIS + vDadosItens(i).BASE_CALCULO_PIS;'+ #13 +
      '    vDadosNota.VALOR_PIS            := vDadosNota.VALOR_PIS + vDadosItens(i).VALOR_PIS;'+ #13 +
      '    vDadosNota.VALOR_COFINS         := vDadosNota.VALOR_COFINS + vDadosItens(i).VALOR_COFINS;'+ #13 +
      '    vDadosNota.VALOR_IPI            := vDadosNota.VALOR_IPI + vDadosItens(i).VALOR_IPI;'+ #13 +
      #13 +
      '    vDadosNota.VALOR_TOTAL :='+ #13 +
      '      vDadosNota.VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '      vDadosItens(i).VALOR_IPI -'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      #13 +
      '    vDadosNota.VALOR_TOTAL_PRODUTOS  := vDadosNota.VALOR_TOTAL_PRODUTOS + vDadosItens(i).VALOR_TOTAL;'+ #13 +
      '    vDadosNota.VALOR_DESCONTO        := vDadosNota.VALOR_DESCONTO + vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      '    vDadosNota.VALOR_OUTRAS_DESPESAS := vDadosNota.VALOR_OUTRAS_DESPESAS + vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS;'+ #13 +
      '    /* ------------------------------------------------------------------------------------------------------ */'+ #13 +
      #13 +
      '		if'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '      vDadosItens(i).VALOR_IPI -'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO >'+ #13 +
      '      vValorMaisSignificativo'+ #13 +
      '    then'+ #13 +
      '			vValorMaisSignificativo :='+ #13 +
      '        vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '        vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '        vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '        vDadosItens(i).VALOR_IPI -'+ #13 +
      '        vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      #13 +
      '			vCfopIdCapa 	  	:= vDadosItens(i).CFOP_ID;'+ #13 +
      '			vNaturezaOperacao := vDadosItens(i).NATUREZA_OPERACAO;'+ #13 +
      '		end if;'+ #13 +
      #13 +
      '    i := i + 1;'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  if vDadosItens.count < 1 then'+ #13 +
      '    ERRO(''Os produtos da nota fiscal não foram encontrados!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '	if vCfopIdCapa is null then'+ #13 +
      '		vCfopIdCapa 			:= vDadosItens(0).CFOP_ID;'+ #13 +
      '		vNaturezaOperacao := vDadosItens(0).NATUREZA_OPERACAO;'+ #13 +
      '	end if;'+ #13 +
      #13 +
      '	vDadosNota.CFOP_ID           := vCfopIdCapa;'+ #13 +
      '  vDadosNota.NATUREZA_OPERACAO := vNaturezaOperacao;'+ #13 +
      #13 +
      '  vDadosNota.VALOR_RECEBIDO_COBRANCA := vDadosNota.VALOR_TOTAL;'+ #13 +
      #13 +
      '  vSerieNota  := to_char(nvl(vSerieNFe, 1));'+ #13 +
      #13 +
      '  if vTipoEmpresaOrigem = ''D'' then'+ #13 +
      '    vDadosNota.INFORMACOES_COMPLEMENTARES := ''Não incidencia do ICMS, nos termos do Art. 79, inciso I, "j" e Art. 19 do Anexo XII do RCTE-GO'';'+ #13 +
      '  else'+ #13 +
      '    vDadosNota.INFORMACOES_COMPLEMENTARES := ''Transferência de produtos para suprir ESTOQUE.'';'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  vNotaFiscalId := SEQ_NOTA_FISCAL_ID.nextval;'+ #13 +
      #13 +
      '	insert into NOTAS_FISCAIS('+ #13 +
      '    NOTA_FISCAL_ID,'+ #13 +
      '    CADASTRO_ID,'+ #13 +
      '    CFOP_ID,'+ #13 +
      '    EMPRESA_ID,'+ #13 +
      '		MODELO_NOTA,'+ #13 +
      '		SERIE_NOTA,'+ #13 +
      '    NATUREZA_OPERACAO,'+ #13 +
      '		STATUS,'+ #13 +
      '    TIPO_MOVIMENTO,'+ #13 +
      '    RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '    NOME_FANTASIA_EMITENTE,'+ #13 +
      '    REGIME_TRIBUTARIO,'+ #13 +
      '    CNPJ_EMITENTE,'+ #13 +
      '    INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '    LOGRADOURO_EMITENTE,'+ #13 +
      '    COMPLEMENTO_EMITENTE,'+ #13 +
      '    NOME_BAIRRO_EMITENTE,'+ #13 +
      '    NOME_CIDADE_EMITENTE,'+ #13 +
      '    NUMERO_EMITENTE,'+ #13 +
      '    ESTADO_ID_EMITENTE,'+ #13 +
      '    CEP_EMITENTE,'+ #13 +
      '    TELEFONE_EMITENTE,'+ #13 +
      '    CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '    CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      '    NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '    RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '    TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '    CPF_CNPJ_DESTINATARIO,'+ #13 +
      '    INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '    NOME_CONSUMIDOR_FINAL,'+ #13 +
      '    TELEFONE_CONSUMIDOR_FINAL,'+ #13 +
      '    TIPO_NOTA,'+ #13 +
      '    LOGRADOURO_DESTINATARIO,'+ #13 +
      '    COMPLEMENTO_DESTINATARIO,'+ #13 +
      '    NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '    NOME_CIDADE_DESTINATARIO,'+ #13 +
      '    ESTADO_ID_DESTINATARIO,'+ #13 +
      '    CEP_DESTINATARIO,'+ #13 +
      '    NUMERO_DESTINATARIO,'+ #13 +
      '    CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '    VALOR_TOTAL,'+ #13 +
      '    VALOR_TOTAL_PRODUTOS,'+ #13 +
      '    VALOR_DESCONTO,'+ #13 +
      '    VALOR_OUTRAS_DESPESAS,'+ #13 +
      '    BASE_CALCULO_ICMS,'+ #13 +
      '    VALOR_ICMS,'+ #13 +
      '    BASE_CALCULO_ICMS_ST,'+ #13 +
      '    VALOR_ICMS_ST,'+ #13 +
      '    BASE_CALCULO_PIS,'+ #13 +
      '    VALOR_PIS,'+ #13 +
      '    BASE_CALCULO_COFINS,'+ #13 +
      '    VALOR_COFINS,'+ #13 +
      '    VALOR_IPI,'+ #13 +
      '    VALOR_FRETE,'+ #13 +
      '    VALOR_SEGURO,'+ #13 +
      '    VALOR_RECEBIDO_COBRANCA,'+ #13 +
      '    PESO_LIQUIDO,'+ #13 +
      '    PESO_BRUTO,'+ #13 +
      '		INFORMACOES_COMPLEMENTARES'+ #13 +
      '  )values('+ #13 +
      '		vNotaFiscalId,'+ #13 +
      '    vDadosNota.CADASTRO_ID,'+ #13 +
      '    vDadosNota.CFOP_ID,'+ #13 +
      '    vDadosNota.EMPRESA_ID,'+ #13 +
      '		''55'','+ #13 +
      '		vSerieNota,'+ #13 +
      '    vDadosNota.NATUREZA_OPERACAO,'+ #13 +
      '		''N'','+ #13 +
      '    ''TFI'','+ #13 +
      '    vDadosNota.RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_FANTASIA_EMITENTE,'+ #13 +
      '    vRegimeTributario,'+ #13 +
      '    vDadosNota.CNPJ_EMITENTE,'+ #13 +
      '    vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '    vDadosNota.LOGRADOURO_EMITENTE,'+ #13 +
      '    vDadosNota.COMPLEMENTO_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_BAIRRO_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_CIDADE_EMITENTE,'+ #13 +
      '    vDadosNota.NUMERO_EMITENTE,'+ #13 +
      '    vDadosNota.ESTADO_ID_EMITENTE,'+ #13 +
      '    vDadosNota.CEP_EMITENTE,'+ #13 +
      '    vDadosNota.TELEFONE_EMITENTE,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      '    vDadosNota.NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '    vDadosNota.RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '    vDadosNota.TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '    vDadosNota.CPF_CNPJ_DESTINATARIO,'+ #13 +
      '    vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '    vDadosNota.NOME_CONSUMIDOR_FINAL,'+ #13 +
      '    vDadosNota.TELEFONE_CONSUMIDOR_FINAL,'+ #13 +
      '    ''N'','+ #13 +
      '    vDadosNota.LOGRADOURO_DESTINATARIO,'+ #13 +
      '    vDadosNota.COMPLEMENTO_DESTINATARIO,'+ #13 +
      '    vDadosNota.NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '    vDadosNota.NOME_CIDADE_DESTINATARIO,'+ #13 +
      '    vDadosNota.ESTADO_ID_DESTINATARIO,'+ #13 +
      '    vDadosNota.CEP_DESTINATARIO,'+ #13 +
      '    vDadosNota.NUMERO_DESTINATARIO,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '    vDadosNota.VALOR_TOTAL,'+ #13 +
      '    vDadosNota.VALOR_TOTAL_PRODUTOS,'+ #13 +
      '    vDadosNota.VALOR_DESCONTO,'+ #13 +
      '    vDadosNota.VALOR_OUTRAS_DESPESAS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS,'+ #13 +
      '    vDadosNota.VALOR_ICMS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS_ST,'+ #13 +
      '    vDadosNota.VALOR_ICMS_ST,'+ #13 +
      '    vDadosNota.BASE_CALCULO_PIS,'+ #13 +
      '    vDadosNota.VALOR_PIS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_COFINS,'+ #13 +
      '    vDadosNota.VALOR_COFINS,'+ #13 +
      '    vDadosNota.VALOR_IPI,'+ #13 +
      '    vDadosNota.VALOR_FRETE,'+ #13 +
      '    vDadosNota.VALOR_SEGURO,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_COBRANCA,'+ #13 +
      '    vDadosNota.PESO_LIQUIDO,'+ #13 +
      '    vDadosNota.PESO_BRUTO,'+ #13 +
      '		vDadosNota.INFORMACOES_COMPLEMENTARES'+ #13 +
      '  );'+ #13 +
      #13 +
      '  for i in 0..vDadosItens.count - 1 loop'+ #13 +
      '    insert into NOTAS_FISCAIS_ITENS('+ #13 +
      '      NOTA_FISCAL_ID,'+ #13 +
      '      PRODUTO_ID,'+ #13 +
      '      ITEM_ID,'+ #13 +
      '      NOME_PRODUTO,'+ #13 +
      '      UNIDADE,'+ #13 +
      '      CFOP_ID,'+ #13 +
      '      CST,'+ #13 +
      '      CODIGO_NCM,'+ #13 +
      '      VALOR_TOTAL,'+ #13 +
      '      PRECO_UNITARIO,'+ #13 +
      '      QUANTIDADE,'+ #13 +
      '      VALOR_TOTAL_DESCONTO,'+ #13 +
      '      VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      '      BASE_CALCULO_ICMS,'+ #13 +
      '      PERCENTUAL_ICMS,'+ #13 +
      '      VALOR_ICMS,'+ #13 +
      '      BASE_CALCULO_ICMS_ST,'+ #13 +
      '      VALOR_ICMS_ST,'+ #13 +
      '      CST_PIS,'+ #13 +
      '      BASE_CALCULO_PIS,'+ #13 +
      '      PERCENTUAL_PIS,'+ #13 +
      '      VALOR_PIS,'+ #13 +
      '      CST_COFINS,'+ #13 +
      '      BASE_CALCULO_COFINS,'+ #13 +
      '      PERCENTUAL_COFINS,'+ #13 +
      '      VALOR_COFINS,'+ #13 +
      '      INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      '      IVA,'+ #13 +
      '      PRECO_PAUTA,'+ #13 +
      '      CODIGO_BARRAS,'+ #13 +
      '      VALOR_IPI,'+ #13 +
      '      PERCENTUAL_IPI,'+ #13 +
      '      INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      '      PERCENTUAL_ICMS_ST,'+ #13 +
      '      CEST'+ #13 +
      '    )values('+ #13 +
      '      vNotaFiscalId,'+ #13 +
      '      vDadosItens(i).PRODUTO_ID,'+ #13 +
      '      vDadosItens(i).ITEM_ID,'+ #13 +
      '      vDadosItens(i).NOME_PRODUTO,'+ #13 +
      '      vDadosItens(i).UNIDADE,'+ #13 +
      '      vDadosItens(i).CFOP_ID,'+ #13 +
      '      vDadosItens(i).CST,'+ #13 +
      '      vDadosItens(i).CODIGO_NCM,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL,'+ #13 +
      '      vDadosItens(i).PRECO_UNITARIO,'+ #13 +
      '      vDadosItens(i).QUANTIDADE,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13;
  end;

  function GetComando2: string;
  begin
    Result :=
      '      vDadosItens(i).BASE_CALCULO_ICMS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS_ST,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST,'+ #13 +
      '      vDadosItens(i).CST_PIS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_PIS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_PIS,'+ #13 +
      '      vDadosItens(i).VALOR_PIS,'+ #13 +
      '      vDadosItens(i).CST_COFINS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_COFINS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_COFINS,'+ #13 +
      '      vDadosItens(i).VALOR_COFINS,'+ #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      '      vDadosItens(i).IVA,'+ #13 +
      '      vDadosItens(i).PRECO_PAUTA,'+ #13 +
      '      vDadosItens(i).CODIGO_BARRAS,'+ #13 +
      '      vDadosItens(i).VALOR_IPI,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_IPI,'+ #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS_ST,'+ #13 +
      '      vDadosItens(i).CEST'+ #13 +
      '    );'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  update RETIRADAS set'+ #13 +
      '    NOTA_TRANSF_ESTOQUE_FISCAL_ID = vNotaFiscalId'+ #13 +
      '  where RETIRADA_ID in('+ #13 +
      '    select'+ #13 +
      '      MOVIMENTO_ID'+ #13 +
      '    from'+ #13 +
      '      VW_MOV_PEND_EMISSAO_NF_TRANSF'+ #13 +
      '    where EMPRESA_ORIGEM_ID = iEMPRESA_ORIGEM_ID'+ #13 +
      '    and trunc(DATA_HORA_EMISSAO) between iDATA_INICIAL and iDATA_FINAL'+ #13 +
      '    and TIPO_MOVIMENTO in(''VRE'', ''VRA'')'+ #13 +
      '  );'+ #13 +
      #13 +
      '  update ENTREGAS set'+ #13 +
      '    NOTA_TRANSF_ESTOQUE_FISCAL_ID = vNotaFiscalId'+ #13 +
      '  where ENTREGA_ID in('+ #13 +
      '    select'+ #13 +
      '      MOVIMENTO_ID'+ #13 +
      '    from'+ #13 +
      '      VW_MOV_PEND_EMISSAO_NF_TRANSF'+ #13 +
      '    where EMPRESA_ORIGEM_ID = iEMPRESA_ORIGEM_ID'+ #13 +
      '    and trunc(DATA_HORA_EMISSAO) between iDATA_INICIAL and iDATA_FINAL'+ #13 +
      '    and TIPO_MOVIMENTO = ''VEN'''+ #13 +
      '  );'+ #13 +
      #13 +
      '  oNOTA_FISCAL_ID := vNotaFiscalId;'+ #13 +
      #13 +
      'end GERAR_NOTA_TRANSF_MOVIMENTOS;'+ #13;
  end;

begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      GetComando1 + GetComando2
    );
end;

function AjustarViewVW_MOV_ITE_PEND_EMISSAO_NF_TR: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace view VW_MOV_ITE_PEND_EMISSAO_NF_TR'+ #13 +
      'as'+ #13 +
      'select'+ #13 +
      '  RET.EMPRESA_ID as EMPRESA_ORIGEM_ID,'+ #13 +
      '  NOF.EMPRESA_ID as EMPRESA_DESTINO_ID,'+ #13 +
      '  trunc(NOF.DATA_HORA_EMISSAO) as DATA_MOVIMENTO,'+ #13 +
      #13 +
      '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
      '  ITE.QUANTIDADE * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
      '  PRO.NOME as NOME_PRODUTO,'+ #13 +
      #13 +
      '  round(zvl(zvl(zvl(CUS.PRECO_FINAL, CUS.PRECO_FINAL_MEDIO), CUS.PRECO_LIQUIDO), CUS.PRECO_LIQUIDO_MEDIO) * PRO.QUANTIDADE_VEZES_PAI, 2) as PRECO_UNITARIO,'+ #13 +
      #13 +
      '  PRO.CODIGO_NCM,'+ #13 +
      '  PRO.UNIDADE_VENDA,'+ #13 +
      '  PRO.CODIGO_BARRAS,'+ #13 +
      '  PRO.CEST'+ #13 +
      'from'+ #13 +
      '  RETIRADAS_ITENS ITE '+ #13 +
      #13 +
      'inner join RETIRADAS RET'+ #13 +
      'on ITE.RETIRADA_ID = RET.RETIRADA_ID'+ #13 +
      #13 +
      'inner join NOTAS_FISCAIS NOF'+ #13 +
      'on RET.RETIRADA_ID = NOF.RETIRADA_ID'+ #13 +
      'and NOF.STATUS = ''E'''+ #13 +
      'and RET.EMPRESA_ID <> NOF.EMPRESA_ID'+ #13 +
      'and RET.NOTA_TRANSF_ESTOQUE_FISCAL_ID is null'+ #13 +
      #13 +
      'inner join PRODUTOS PRO'+ #13 +
      'on ITE.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
      #13 +
      'inner join VW_CUSTOS_PRODUTOS CUS'+ #13 +
      'on PRO.PRODUTO_ID = CUS.PRODUTO_ID'+ #13 +
      'and RET.EMPRESA_ID = CUS.EMPRESA_ID'+ #13 +
      #13 +
      'union all'+ #13 +
      #13 +
      'select'+ #13 +
      '  ENT.EMPRESA_ENTREGA_ID as EMPRESA_ORIGEM_ID,'+ #13 +
      '  NOF.EMPRESA_ID as EMPRESA_DESTINO_ID,'+ #13 +
      '  trunc(NOF.DATA_HORA_EMISSAO) as DATA_MOVIMENTO,'+ #13 +
      #13 +
      '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
      '  ITE.QUANTIDADE * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
      '  PRO.NOME as NOME_PRODUTO,'+ #13 +
      #13 +
      '  round(zvl(zvl(zvl(CUS.PRECO_FINAL, CUS.PRECO_FINAL_MEDIO), CUS.PRECO_LIQUIDO), CUS.PRECO_LIQUIDO_MEDIO) * PRO.QUANTIDADE_VEZES_PAI, 2) as PRECO_UNITARIO,'+ #13 +
      #13 +
      '  PRO.CODIGO_NCM,'+ #13 +
      '  PRO.UNIDADE_VENDA,'+ #13 +
      '  PRO.CODIGO_BARRAS,'+ #13 +
      '  PRO.CEST'+ #13 +
      'from'+ #13 +
      '  ENTREGAS_ITENS ITE '+ #13 +
      #13 +
      'inner join ENTREGAS ENT'+ #13 +
      'on ITE.ENTREGA_ID = ENT.ENTREGA_ID'+ #13 +
      #13 +
      'inner join NOTAS_FISCAIS NOF'+ #13 +
      'on ENT.ENTREGA_ID = NOF.ENTREGA_ID'+ #13 +
      'and NOF.STATUS = ''E'''+ #13 +
      'and ENT.EMPRESA_ENTREGA_ID <> NOF.EMPRESA_ID'+ #13 +
      'and ENT.NOTA_TRANSF_ESTOQUE_FISCAL_ID is null'+ #13 +
      #13 +
      'inner join PRODUTOS PRO'+ #13 +
      'on ITE.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
      #13 +
      'inner join VW_CUSTOS_PRODUTOS CUS'+ #13 +
      'on PRO.PRODUTO_ID = CUS.PRODUTO_ID'+ #13 +
      'and ENT.EMPRESA_ENTREGA_ID = CUS.EMPRESA_ID;'+ #13
    );
end;

function AjustarTriggerNOTAS_FISCAIS_D_BR: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace trigger NOTAS_FISCAIS_D_BR'+ #13 +
      'before delete'+ #13 +
      'on NOTAS_FISCAIS'+ #13 +
      'for each row'+ #13 +
      'begin'+ #13 +
      #13 +
      '  if :old.STATUS in(''E'', ''C'') then'+ #13 +
      '    ERRO(''Não é permitido deletar uma nota fiscal já emitida ou cancelada!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if :old.NUMERO_NOTA is not null then'+ #13 +
      '    ERRO(''Não é permitido deletar uma nota fiscal que gerou numero!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  update RETIRADAS set'+ #13 +
      '    NOTA_TRANSF_ESTOQUE_FISCAL_ID = null'+ #13 +
      '  where NOTA_TRANSF_ESTOQUE_FISCAL_ID = :old.NOTA_FISCAL_ID;'+ #13 +
      #13 +
      '  update ENTREGAS set'+ #13 +
      '    NOTA_TRANSF_ESTOQUE_FISCAL_ID = null'+ #13 +
      '  where NOTA_TRANSF_ESTOQUE_FISCAL_ID = :old.NOTA_FISCAL_ID;'+ #13 +
      #13 +
      '  delete from LOGS_NOTAS_FISCAIS'+ #13 +
      '  where NOTA_FISCAL_ID = :old.NOTA_FISCAL_ID;'+ #13 +
      #13 +
      '  delete from LOGS_NOTAS_FISCAIS_ITENS'+ #13 +
      '  where NOTA_FISCAL_ID = :old.NOTA_FISCAL_ID;'+ #13 +
      #13 +
      '  delete from NOTAS_FISCAIS_REFERENCIAS'+ #13 +
      '  where NOTA_FISCAL_ID = :old.NOTA_FISCAL_ID;'+ #13 +
      #13 +
      '  delete from NOTAS_FISCAIS_ITENS'+ #13 +
      '  where NOTA_FISCAL_ID = :old.NOTA_FISCAL_ID;'+ #13 +
      #13 +
      'end NOTAS_FISCAIS_D_BR;'+ #13
    );
end;

function AjustarTriggerENTREGAS_IU_BR: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace trigger ENTREGAS_IU_BR'+ #13 +
      'before insert or update '+ #13 +
      'on ENTREGAS'+ #13 +
      'for each row'+ #13 +
      'declare'+ #13 +
      '  vStatusPedido   ORCAMENTOS.STATUS%type;'+ #13 +
      'begin'+ #13 +
      #13 +
      '  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
      '  :new.DATA_HORA_ALTERACAO := sysdate;'+ #13 +
      '  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;'+ #13 +
      '  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;'+ #13 +
      #13 +
      '  if SESSAO.ROTINA = ''UPDATE'' then'+ #13 +
      '    return;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  select'+ #13 +
      '    STATUS'+ #13 +
      '  into'+ #13 +
      '    vStatusPedido'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS'+ #13 +
      '  where ORCAMENTO_ID = :new.ORCAMENTO_ID;'+ #13 +
      #13 +
      '  if vStatusPedido not in(''RE'', ''VE'') then'+ #13 +
      '    ERRO(''Pedido ainda não recebido, por favor verifique!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if inserting then'+ #13 +
      '    :new.DATA_HORA_CADASTRO  := sysdate;'+ #13 +
      '    :new.USUARIO_CADASTRO_ID := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
      '  else'+ #13 +
      '    if :old.STATUS in(''ENT'', ''RPA'') and SESSAO.ROTINA not in(''GERAR_NF_TRANSF_MOVIMENTOS'') then'+ #13 +
      '      ERRO(''Não é permitido alterar uma entrega já finalizada!'');'+ #13 +
      '    end if;'+ #13 +
      #13 +
      '    if :old.STATUS = ''AGS'' and :new.STATUS = ''ESE'' then'+ #13 +
      '      :new.USUARIO_INICIO_SEPARACAO_ID := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
      '      :new.DATA_HORA_INICIO_SEPARACAO  := sysdate;'+ #13 +
      '    elsif :old.STATUS = ''ESE'' and :new.STATUS = ''AGC'' then'+ #13 +
      '      :new.USUARIO_FINALIZOU_SEP_ID := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
      '      :new.DATA_HORA_FINALIZOU_SEP  := sysdate;'+ #13 +
      '    elsif :old.STATUS not in(''ENT'', ''RPA'') and :new.STATUS in(''ENT'', ''RPA'') then'+ #13 +
      '      :new.USUARIO_CONFIRMACAO_ID := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
      '      if :new.DATA_HORA_REALIZOU_ENTREGA is null then'+ #13 +
      '        :new.DATA_HORA_REALIZOU_ENTREGA := sysdate;'+ #13 +
      '      end if;'+ #13 +
      '    elsif :old.STATUS = ''ESE'' and :new.STATUS = ''AGS'' then'+ #13 +
      '      :new.USUARIO_INICIO_SEPARACAO_ID := null;'+ #13 +
      '      :new.DATA_HORA_INICIO_SEPARACAO  := null;'+ #13 +
      '    end if;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      'end ENTREGAS_IU_BR;'+ #13
    );
end;

function AdiconarColunaTIPO_RATEIO_CONHECIMENTO: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'alter table CONHECIMENTOS_FRETES'+ #13 +
      'add TIPO_RATEIO_CONHECIMENTO  char(1) default ''V'' not null;'+ #13 +
      #13 +
      'alter table CONHECIMENTOS_FRETES'+ #13 +
      'add constraint CK_CONHEC_FRET_TP_RAT_CONHEC'+ #13 +
      'check(TIPO_RATEIO_CONHECIMENTO in(''Q'', ''V'', ''P''));'+ #13
    );
end;

function AjustarViewVW_PRODUTOS_VENDAS: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace view VW_PRODUTOS_VENDAS as'+ #13 +
      'select'+ #13 +
      '  PRO.PRODUTO_ID,'+ #13 +
      '  PRO.NOME,'+ #13 +
      '  PRO.MULTIPLO_VENDA,'+ #13 +
      '  PRO.UNIDADE_VENDA,'+ #13 +
      '  PRO.PESO,'+ #13 +
      '  PRO.CARACTERISTICAS,'+ #13 +
      '  PRO.FORNECEDOR_ID,'+ #13 +
      '  PRO.MARCA_ID,'+ #13 +
      '  MAR.NOME as NOME_MARCA,'+ #13 +
      '  PRE.PRECO_VAREJO,'+ #13 +
      '  PRE.QUANTIDADE_MIN_PRECO_VAREJO,'+ #13 +
      '  PRV.PRECO_PROMOCIONAL,'+ #13 +
      '  PRV.QUANTIDADE_MINIMA as QUANTIDADE_MIN_PROMOCIONAL,'+ #13 +
      '  PRE.PRECO_ATACADO_1,'+ #13 +
      '  PRE.QUANTIDADE_MIN_PRECO_ATAC_1,'+ #13 +
      '  PRE.PRECO_ATACADO_2,'+ #13 +
      '  PRE.QUANTIDADE_MIN_PRECO_ATAC_2,'+ #13 +
      '  PRE.PRECO_ATACADO_3,'+ #13 +
      '  PRE.QUANTIDADE_MIN_PRECO_ATAC_3,'+ #13 +
      '  PRE.PRECO_PDV,'+ #13 +
      '  PRE.QUANTIDADE_MINIMA_PDV,'+ #13 +
      '  EST.ESTOQUE,'+ #13 +
      '  case when PRO.TIPO_CONTROLE_ESTOQUE in(''K'', ''A'') then QUANTIDADE_KITS_DISP_EMPRESA(PRE.EMPRESA_ID, PRO.PRODUTO_ID) else EST.DISPONIVEL end as DISPONIVEL,'+ #13 +
      '  PRO.CODIGO_BARRAS,'+ #13 +
      '  PRE.EMPRESA_ID,'+ #13 +
      '  PRO.PRODUTO_DIVERSOS_PDV,'+ #13 +
      '  PRO.TIPO_CONTROLE_ESTOQUE,'+ #13 +
      '  PRO.VALOR_ADICIONAL_FRETE,'+ #13 +
      '  PRO.CODIGO_ORIGINAL_FABRICANTE'+ #13 +
      'from'+ #13 +
      '  PRODUTOS PRO'+ #13 +
      #13 +
      'inner join PRECOS_PRODUTOS PRE'+ #13 +
      'on PRO.PRODUTO_ID = PRE.PRODUTO_ID'+ #13 +
      #13 +
      'inner join MARCAS MAR'+ #13 +
      'on PRO.MARCA_ID = MAR.MARCA_ID'+ #13 +
      #13 +
      'left join VW_ESTOQUES EST'+ #13 +
      'on PRO.PRODUTO_ID = EST.PRODUTO_ID'+ #13 +
      'and PRE.EMPRESA_ID = EST.EMPRESA_ID'+ #13 +
      #13 +
      'left join PRODUTOS_PROMOCOES PRV'+ #13 +
      'on PRO.PRODUTO_ID = PRV.PRODUTO_ID'+ #13 +
      'and PRE.EMPRESA_ID = PRV.EMPRESA_ID'+ #13 +
      'and trunc(sysdate) between PRV.DATA_INICIAL_PROMOCAO and PRV.DATA_FINAL_PROMOCAO'+ #13 +
      #13 +
      'where PRO.ATIVO = ''S'''+ #13 +
      'and PRO.BLOQUEADO_VENDAS = ''N'';'+ #13
    );
end;

function AjustarProcedureGERAR_ENTR_TRANS_PRODUTOS_EMP: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure GERAR_ENTR_TRANS_PRODUTOS_EMP(iMOVIMENTO_ID in number, iTIPO_MOVIMENTO in string)'+ #13 +
      'is'+ #13 +
      '  vNotaFiscalSaidaId NOTAS_FISCAIS.NOTA_FISCAL_ID%type;'+ #13 +
      '  vEntradaId         ENTRADAS_NOTAS_FISCAIS.ENTRADA_ID%type;'+ #13 +
      '  vStatus            NOTAS_FISCAIS.STATUS%type;'+ #13 +
      '  vChaveNFe          ENTRADAS_NOTAS_FISCAIS.CHAVE_NFE%type;'+ #13 +
      '  vFornecedorId      FORNECEDORES.CADASTRO_ID%type;'+ #13 +
      '  vQtde              number;'+ #13 +
      'begin'+ #13 +
      #13 +
      '  if iTIPO_MOVIMENTO not in(''TPE'', ''TFP'') then'+ #13 +
      '    ERRO(''O tipo de movimento só pode ser "TPE" ou "TFP"!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  select'+ #13 +
      '    NFI.NOTA_FISCAL_ID,'+ #13 +
      '    NFI.STATUS,'+ #13 +
      '    NFI.CHAVE_NFE,'+ #13 +
      '    EMP.CADASTRO_ID'+ #13 +
      '  into'+ #13 +
      '    vNotaFiscalSaidaId,'+ #13 +
      '    vStatus,'+ #13 +
      '    vChaveNFe,'+ #13 +
      '    vFornecedorId'+ #13 +
      '  from'+ #13 +
      '    NOTAS_FISCAIS NFI'+ #13 +
      #13 +
      '  inner join EMPRESAS EMP'+ #13 +
      '  on NFI.EMPRESA_ID = EMP.EMPRESA_ID'+ #13 +
      #13 +
      '  where NFI.NOTA_FISCAL_ID = iMOVIMENTO_ID;'+ #13 +
      #13 +
      '  if vStatus <> ''E'' then'+ #13 +
      '    ERRO(''Apenas notas fiscais já emitidas podem gerar entrada de transferência de produtos entre empresas!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  select'+ #13 +
      '    count(*)'+ #13 +
      '  into'+ #13 +
      '    vQtde'+ #13 +
      '  from'+ #13 +
      '    FORNECEDORES'+ #13 +
      '  where CADASTRO_ID = vFornecedorId;'+ #13 +
      #13 +
      '  if vQtde = 0 then'+ #13 +
      '    ERRO(''O destinatário da NFe não está cadastrado como fornecedor, por favor faça o cadastro para que seja realizada a entrada da NFe na empresa de destino!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  vChaveNFe :='+ #13 +
      '    substr(vChaveNFe, 1, 4) || ''.'' ||'+ #13 +
      '    substr(vChaveNFe, 5, 4) || ''.'' ||'+ #13 +
      '    substr(vChaveNFe, 9, 4) || ''.'' ||'+ #13 +
      '    substr(vChaveNFe, 13, 4) || ''.'' ||'+ #13 +
      '    substr(vChaveNFe, 17, 4) || ''.'' ||'+ #13 +
      '    substr(vChaveNFe, 21, 4) || ''.'' ||'+ #13 +
      '    substr(vChaveNFe, 25, 4) || ''.'' ||'+ #13 +
      '    substr(vChaveNFe, 29, 4) || ''.'' ||'+ #13 +
      '    substr(vChaveNFe, 33, 4) || ''.'' ||'+ #13 +
      '    substr(vChaveNFe, 37, 4) || ''.'' ||'+ #13 +
      '    substr(vChaveNFe, 41, 4);'+ #13 +
      #13 +
      '  select'+ #13 +
      '    count(*)'+ #13 +
      '  into'+ #13 +
      '    vQtde'+ #13 +
      '  from'+ #13 +
      '    ENTRADAS_NOTAS_FISCAIS'+ #13 +
      '  where CHAVE_NFE = vChaveNFe;'+ #13 +
      #13 +
      '  if vQtde > 0 then'+ #13 +
      '    ERRO(''Já existe uma entrada cadastrada para esta NF-e, por favor verifique no "Relação de entradas de notas fiscais"!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  vEntradaId := SEQ_ENTRADA_NOTAS_FISC_ENT_ID.nextval;'+ #13 +
      #13 +
      '  insert into ENTRADAS_NOTAS_FISCAIS('+ #13 +
      '    ENTRADA_ID,'+ #13 +
      '    FORNECEDOR_ID,'+ #13 +
      '    CFOP_ID,'+ #13 +
      '    EMPRESA_ID,'+ #13 +
      '    ORIGEM_ENTRADA,'+ #13 +
      '    NOTA_TRANSF_PROD_ORIGEM_ID,'+ #13 +
      '    NUMERO_NOTA,'+ #13 +
      '    MODELO_NOTA,'+ #13 +
      '    SERIE_NOTA,    '+ #13 +
      '    TIPO_RATEIO_FRETE,    '+ #13 +
      '    VALOR_TOTAL,'+ #13 +
      '    VALOR_TOTAL_PRODUTOS,'+ #13 +
      '    BASE_CALCULO_ICMS,'+ #13 +
      '    VALOR_ICMS,'+ #13 +
      '    BASE_CALCULO_ICMS_ST,'+ #13 +
      '    VALOR_ICMS_ST,'+ #13 +
      '    BASE_CALCULO_PIS,'+ #13 +
      '    VALOR_PIS,'+ #13 +
      '    BASE_CALCULO_COFINS,'+ #13 +
      '    VALOR_COFINS,    '+ #13 +
      '    PESO_LIQUIDO,'+ #13 +
      '    PESO_BRUTO,'+ #13 +
      '    DATA_HORA_EMISSAO,'+ #13 +
      '    DATA_ENTRADA,'+ #13 +
      '    DATA_PRIMEIRA_PARCELA,'+ #13 +
      '    STATUS,'+ #13 +
      '    PLANO_FINANCEIRO_ID,    '+ #13 +
      '    CHAVE_NFE'+ #13 +
      '  )'+ #13 +
      '  select'+ #13 +
      '    vEntradaId,'+ #13 +
      '    vFornecedorId,'+ #13 +
      '    NFI.CFOP_ID,'+ #13 +
      '    EEN.EMPRESA_ID,'+ #13 +
      '    iTIPO_MOVIMENTO,'+ #13 +
      '    vNotaFiscalSaidaId,'+ #13 +
      '    NFI.NUMERO_NOTA,'+ #13 +
      '    ''55'','+ #13 +
      '    NFI.SERIE_NOTA,'+ #13 +
      '    ''V'','+ #13 +
      '    NFI.VALOR_TOTAL,'+ #13 +
      '    NFI.VALOR_TOTAL_PRODUTOS,'+ #13 +
      '    NFI.BASE_CALCULO_ICMS,'+ #13 +
      '    NFI.VALOR_ICMS,'+ #13 +
      '    NFI.BASE_CALCULO_ICMS_ST,'+ #13 +
      '    NFI.VALOR_ICMS_ST,'+ #13 +
      '    NFI.BASE_CALCULO_PIS,'+ #13 +
      '    NFI.VALOR_PIS,'+ #13 +
      '    NFI.BASE_CALCULO_COFINS,'+ #13 +
      '    NFI.VALOR_COFINS,'+ #13 +
      '    NFI.PESO_LIQUIDO,'+ #13 +
      '    NFI.PESO_BRUTO,'+ #13 +
      '    NFI.DATA_HORA_EMISSAO,'+ #13 +
      '    trunc(sysdate), -- DATA_ENTRADA'+ #13 +
      '    trunc(sysdate),'+ #13 +
      '    ''ACM'','+ #13 +
      '    ''2.001.001'','+ #13 +
      '    vChaveNFe'+ #13 +
      '  from'+ #13 +
      '    NOTAS_FISCAIS NFI'+ #13 +
      #13 +
      '  /* Trazendo a empresa da saída */'+ #13 +
      '  inner join EMPRESAS EMP'+ #13 +
      '  on NFI.EMPRESA_ID = EMP.EMPRESA_ID'+ #13 +
      #13 +
      '  /* Trazendo a empresa que será realizada a entrada pelo código do cadastro */'+ #13 +
      '  inner join EMPRESAS EEN'+ #13 +
      '  on NFI.CADASTRO_ID = EEN.CADASTRO_ID'+ #13 +
      #13 +
      '  where NFI.NOTA_FISCAL_ID = vNotaFiscalSaidaId;'+ #13 +
      #13 +
      '  insert into ENTRADAS_NOTAS_FISCAIS_ITENS('+ #13 +
      '    ENTRADA_ID,'+ #13 +
      '    PRODUTO_ID,'+ #13 +
      '    ITEM_ID,'+ #13 +
      '    CFOP_ID,'+ #13 +
      '    LOCAL_ENTRADA_ID,'+ #13 +
      '    CST,'+ #13 +
      '    CODIGO_NCM,'+ #13 +
      '    VALOR_TOTAL,'+ #13 +
      '    PRECO_UNITARIO,'+ #13 +
      '    QUANTIDADE,'+ #13 +
      '    UNIDADE_ENTRADA_ID,'+ #13 +
      '    VALOR_TOTAL_DESCONTO,'+ #13 +
      '    VALOR_TOTAL_OUTRAS_DESPESAS,    '+ #13 +
      '    ORIGEM_PRODUTO,'+ #13 +
      #13 +
      '    QUANTIDADE_EMBALAGEM,'+ #13 +
      '    MULTIPLO_COMPRA,'+ #13 +
      '    UNIDADE_COMPRA_ID,'+ #13 +
      #13 +
      '    QUANTIDADE_ENTRADA_ALTIS,'+ #13 +
      #13 +
      '    /* ICMS NORMAL */'+ #13 +
      '    INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      '    BASE_CALCULO_ICMS,'+ #13 +
      '    PERCENTUAL_ICMS,'+ #13 +
      '    VALOR_ICMS,'+ #13 +
      '    /* ------------------------------------------------ */'+ #13 +
      #13 +
      '    /* ST */'+ #13 +
      '    INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      '    BASE_CALCULO_ICMS_ST,'+ #13 +
      '    PERCENTUAL_ICMS_ST,'+ #13 +
      '    VALOR_ICMS_ST,'+ #13 +
      '    IVA,'+ #13 +
      '    /* ------------------------------------------------ */'+ #13 +
      #13 +
      '    /* PIS */'+ #13 +
      '    CST_PIS,'+ #13 +
      '    BASE_CALCULO_PIS,'+ #13 +
      '    PERCENTUAL_PIS,'+ #13 +
      '    VALOR_PIS,'+ #13 +
      '    /* ------------------------------------------------ */'+ #13 +
      #13 +
      '    /* COFINS */'+ #13 +
      '    CST_COFINS,'+ #13 +
      '    BASE_CALCULO_COFINS,'+ #13 +
      '    PERCENTUAL_COFINS,'+ #13 +
      '    VALOR_COFINS,'+ #13 +
      '    /* ----------------------------------------------- */'+ #13 +
      #13 +
      '    /* IPI */'+ #13 +
      '    VALOR_IPI,'+ #13 +
      '    PERCENTUAL_IPI,'+ #13 +
      '    /* ----------------------------------------------- */'+ #13 +
      #13 +
      '    PRECO_FINAL,'+ #13 +
      '    PRECO_LIQUIDO,'+ #13 +
      '    PRECO_PAUTA,'+ #13 +
      '    INFORMACOES_ADICIONAIS'+ #13 +
      '  )'+ #13 +
      '  select'+ #13 +
      '    vEntradaId,'+ #13 +
      '    ITE.PRODUTO_ID,'+ #13 +
      '    ITE.ITEM_ID,'+ #13 +
      '    ITE.CFOP_ID,'+ #13 +
      '    1,'+ #13 +
      '    ITE.CST,'+ #13 +
      '    ITE.CODIGO_NCM,'+ #13 +
      '    ITE.VALOR_TOTAL,'+ #13 +
      '    ITE.PRECO_UNITARIO,'+ #13 +
      '    ITE.QUANTIDADE,'+ #13 +
      '    PRO.UNIDADE_VENDA,'+ #13 +
      '    0,'+ #13 +
      '    0,'+ #13 +
      '    ITE.ORIGEM_PRODUTO,'+ #13 +
      #13 +
      '    1,'+ #13 +
      '    1,'+ #13 +
      '    PRO.UNIDADE_VENDA,'+ #13 +
      #13 +
      '    ITE.QUANTIDADE,'+ #13 +
      #13 +
      '    /* ICMS NORMAL */'+ #13 +
      '    ITE.INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      '    ITE.BASE_CALCULO_ICMS,'+ #13 +
      '    ITE.PERCENTUAL_ICMS,'+ #13 +
      '    ITE.VALOR_ICMS,'+ #13 +
      '    /* ------------------------------------------------ */'+ #13 +
      #13 +
      '    /* ST */'+ #13 +
      '    ITE.INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      '    ITE.BASE_CALCULO_ICMS_ST,'+ #13 +
      '    ITE.PERCENTUAL_ICMS_ST,'+ #13 +
      '    ITE.VALOR_ICMS_ST,'+ #13 +
      '    ITE.IVA,'+ #13 +
      '    /* ------------------------------------------------ */'+ #13 +
      #13 +
      '    /* PIS */'+ #13 +
      '    ITE.CST_PIS,'+ #13 +
      '    ITE.BASE_CALCULO_PIS,'+ #13 +
      '    ITE.PERCENTUAL_PIS,'+ #13 +
      '    ITE.VALOR_PIS,'+ #13 +
      '    /* ------------------------------------------------ */'+ #13 +
      #13 +
      '    /* COFINS */'+ #13 +
      '    ITE.CST_COFINS,'+ #13 +
      '    ITE.BASE_CALCULO_COFINS,'+ #13 +
      '    ITE.PERCENTUAL_COFINS,'+ #13 +
      '    ITE.VALOR_COFINS,'+ #13 +
      '    /* ----------------------------------------------- */'+ #13 +
      #13 +
      '    /* IPI */'+ #13 +
      '    ITE.VALOR_IPI,'+ #13 +
      '    ITE.PERCENTUAL_IPI,'+ #13 +
      '    /* ----------------------------------------------- */'+ #13 +
      #13 +
      '    0, -- PRECO_FINAL'+ #13 +
      '    0, -- PRECO_LIQUIDO'+ #13 +
      '    ITE.PRECO_PAUTA,'+ #13 +
      '    ITE.INFORMACOES_ADICIONAIS'+ #13 +
      '  from'+ #13 +
      '    NOTAS_FISCAIS_ITENS ITE'+ #13 +
      #13 +
      '  inner join PRODUTOS PRO'+ #13 +
      '  on ITE.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
      #13 +
      '  where ITE.NOTA_FISCAL_ID = vNotaFiscalSaidaId;'+ #13 +
      #13 +
      'end GERAR_ENTR_TRANS_PRODUTOS_EMP;'+ #13
    );
end;

function AjustarViewVW_ORC_ITENS_SEM_KITS_AGRUPADO: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace view VW_ORC_ITENS_SEM_KITS_AGRUPADO'+ #13 +
      'as'+ #13 +
      'select'+ #13 +
      '  OIT.ORCAMENTO_ID,'+ #13 +
      '  OIT.PRODUTO_ID,'+ #13 +
      '  OIT.ITEM_ID,'+ #13 +
      '  OIT.ITEM_KIT_ID,'+ #13 +
      '  OIT.TIPO_CONTROLE_ESTOQUE'+ #13 +
      'from'+ #13 +
      '  ORCAMENTOS_ITENS OIT'+ #13 +
      #13 +
      'left join ORCAMENTOS_ITENS OIK'+ #13 +
      'on OIT.ORCAMENTO_ID = OIK.ORCAMENTO_ID'+ #13 +
      'and OIT.ITEM_KIT_ID = OIK.ITEM_ID'+ #13 +
      #13 +
      'where nvl(OIK.TIPO_CONTROLE_ESTOQUE, ''X'') <> ''A''; '+ #13
    );
end;

function AjustarProcViewVW_ORC_ITENS_SEM_KITS_AGRUPADO: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure GERAR_RETIRADA_ENT_PENDENTE('+ #13 +
      '  iORCAMENTO_ID     in number,'+ #13 +
      '  iEMPRESA_PEND_ID  in number,'+ #13 +
      '  iLOCAL_ID         in number,'+ #13 +
      '  iPREVISAO_ENTREGA in date,'+ #13 +
      '  iTIPO_NOTA_GERAR  in string,'+ #13 +
      '  iAGRUPADOR_ID     in number,'+ #13 +
      '  iPRODUTOS_IDS     in TIPOS.ArrayOfNumeros,'+ #13 +
      '  iITENS_IDS        in TIPOS.ArrayOfNumeros,'+ #13 +
      '  iLOCAIS_IDS       in TIPOS.ArrayOfNumeros,'+ #13 +
      '  iQUANTIDADES      in TIPOS.ArrayOfNumeros,'+ #13 +
      '  iLOTES            in TIPOS.ArrayOfString,'+ #13 +
      '  oRETIRADA_ID      out number'+ #13 +
      #13 +
      ')'+ #13 +
      'is'+ #13 +
      '  i            number;'+ #13 +
      '  vQtde        number;'+ #13 +
      '  vRetiradaId  number;'+ #13 +
      '  vEProdutoKit char(1);'+ #13 +
      #13 +
      #13 +
      '  cursor cItensPendKit( pPRODUTO_KIT_ID in number, pITEM_KIT_ID in number, pLOCAL_ID in number, pQUANTIDADE_PRODUTO_KIT in number ) is'+ #13 +
      '  select'+ #13 +
      '    EIP.PRODUTO_ID,'+ #13 +
      '    EIP.ITEM_ID,'+ #13 +
      '    EIP.LOTE,'+ #13 +
      '    KIT.QUANTIDADE * pQUANTIDADE_PRODUTO_KIT as QUANTIDADE'+ #13 +
      '  from'+ #13 +
      '    ENTREGAS_ITENS_PENDENTES EIP'+ #13 +
      #13 +
      '  inner join ORCAMENTOS_ITENS OIT'+ #13 +
      '  on EIP.ORCAMENTO_ID = OIT.ORCAMENTO_ID'+ #13 +
      '  and EIP.ITEM_ID = OIT.ITEM_ID'+ #13 +
      #13 +
      '  inner join PRODUTOS_KIT KIT'+ #13 +
      '  on KIT.PRODUTO_KIT_ID = pPRODUTO_KIT_ID'+ #13 +
      '  and OIT.PRODUTO_ID = KIT.PRODUTO_ID'+ #13 +
      #13 +
      '  where EIP.ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '  and EIP.EMPRESA_ID = iEMPRESA_PEND_ID'+ #13 +
      '  and OIT.ITEM_KIT_ID = pITEM_KIT_ID'+ #13 +
      '  and EIP.LOCAL_ID = pLOCAL_ID'+ #13 +
      '  and EIP.PREVISAO_ENTREGA = iPREVISAO_ENTREGA;'+ #13 +
      #13 +
      #13 +
      '  procedure VERIFICAR_PENDENCIA( pITEM_ID in number, pLOCAL_ID in number, pLOTE in string, pQUANTIDADE in number )'+ #13 +
      '  is'+ #13 +
      '    vPodeDeletarPendencia char(1);'+ #13 +
      '  begin'+ #13 +
      #13 +
      '    select'+ #13 +
      '      case when QUANTIDADE = pQUANTIDADE then ''S'' else ''N'' end as PODE_DELETAR_PENDENCIA'+ #13 +
      '    into'+ #13 +
      '      vPodeDeletarPendencia'+ #13 +
      '    from'+ #13 +
      '      ENTREGAS_ITENS_PENDENTES'+ #13 +
      '    where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '    and EMPRESA_ID = iEMPRESA_PEND_ID'+ #13 +
      '    and LOCAL_ID = pLOCAL_ID'+ #13 +
      '    and ITEM_ID = pITEM_ID'+ #13 +
      '    and LOTE = pLOTE'+ #13 +
      '    and PREVISAO_ENTREGA = iPREVISAO_ENTREGA;'+ #13 +
      #13 +
      '    /* Caso a quantidade sendo desagendada seja a mesma quantidade da pendencia, quer dizer que o usuário está desagendando toda a pendência, */'+ #13 +
      '    /* Neste caso vamos deleta-la */'+ #13 +
      '    if vPodeDeletarPendencia = ''S'' then'+ #13 +
      '      delete from ENTREGAS_ITENS_PENDENTES'+ #13 +
      '      where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '      and EMPRESA_ID = iEMPRESA_PEND_ID'+ #13 +
      '      and LOCAL_ID = pLOCAL_ID'+ #13 +
      '      and ITEM_ID = pITEM_ID'+ #13 +
      '      and LOTE = pLOTE'+ #13 +
      '      and PREVISAO_ENTREGA = iPREVISAO_ENTREGA;'+ #13 +
      '    else'+ #13 +
      '      /* Baixando a pendência */'+ #13 +
      '      update ENTREGAS_ITENS_PENDENTES set'+ #13 +
      '        QUANTIDADE = QUANTIDADE - pQUANTIDADE'+ #13 +
      '      where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '      and EMPRESA_ID = iEMPRESA_PEND_ID'+ #13 +
      '      and LOCAL_ID = pLOCAL_ID'+ #13 +
      '      and ITEM_ID = pITEM_ID'+ #13 +
      '      and LOTE = pLOTE'+ #13 +
      '      and PREVISAO_ENTREGA = iPREVISAO_ENTREGA;'+ #13 +
      '    end if;'+ #13 +
      #13 +
      '  end;'+ #13 +
      #13 +
      #13 +
      '  procedure INSERIR_ITEM( pPRODUTO_ID in number, pITEM_ID in number, pLOTE in string, pQUANTIDADES in number )'+ #13 +
      '  is'+ #13 +
      '    vQtde number;'+ #13 +
      '  begin'+ #13 +
      '    select'+ #13 +
      '      count(*)'+ #13 +
      '    into'+ #13 +
      '      vQtde'+ #13 +
      '    from'+ #13 +
      '      RETIRADAS_ITENS_PENDENTES'+ #13 +
      '    where EMPRESA_ID = iEMPRESA_PEND_ID'+ #13 +
      '    and LOCAL_ID = iLOCAL_ID'+ #13 +
      '    and ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '    and PRODUTO_ID = pPRODUTO_ID'+ #13 +
      '    and ITEM_ID = pITEM_ID'+ #13 +
      '    and LOTE = pLOTE;'+ #13 +
      #13 +
      '    if vQtde = 0 then'+ #13 +
      '      insert into RETIRADAS_ITENS_PENDENTES('+ #13 +
      '        EMPRESA_ID,'+ #13 +
      '        LOCAL_ID,'+ #13 +
      '        ORCAMENTO_ID,'+ #13 +
      '        PRODUTO_ID,'+ #13 +
      '        ITEM_ID,'+ #13 +
      '        LOTE,'+ #13 +
      '        QUANTIDADE'+ #13 +
      '      )values('+ #13 +
      '        iEMPRESA_PEND_ID,'+ #13 +
      '        iLOCAL_ID,'+ #13 +
      '        iORCAMENTO_ID,'+ #13 +
      '        pPRODUTO_ID,'+ #13 +
      '        pITEM_ID,'+ #13 +
      '        pLOTE,'+ #13 +
      '        pQUANTIDADES'+ #13 +
      '      );'+ #13 +
      '    else'+ #13 +
      '      update RETIRADAS_ITENS_PENDENTES set'+ #13 +
      '        QUANTIDADE = QUANTIDADE + pQUANTIDADES'+ #13 +
      '      where EMPRESA_ID = iEMPRESA_PEND_ID'+ #13 +
      '      and LOCAL_ID = iLOCAL_ID'+ #13 +
      '      and ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '      and PRODUTO_ID = pPRODUTO_ID'+ #13 +
      '      and ITEM_ID = pITEM_ID'+ #13 +
      '      and LOTE = pLOTE;'+ #13 +
      '    end if;'+ #13 +
      '  end;'+ #13 +
      #13 +
      #13 +
      'begin'+ #13 +
      #13 +
      '  select'+ #13 +
      '    count(*)'+ #13 +
      '  into'+ #13 +
      '    vQtde'+ #13 +
      '  from'+ #13 +
      '    RETIRADAS_PENDENTES'+ #13 +
      '  where EMPRESA_ID = iEMPRESA_PEND_ID'+ #13 +
      '  and LOCAL_ID = iLOCAL_ID'+ #13 +
      '  and ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      #13 +
      '  if vQtde = 0 then'+ #13 +
      '    insert into RETIRADAS_PENDENTES('+ #13 +
      '      EMPRESA_ID,'+ #13 +
      '      LOCAL_ID,'+ #13 +
      '      ORCAMENTO_ID'+ #13 +
      '    )values('+ #13 +
      '      iEMPRESA_PEND_ID,'+ #13 +
      '      iLOCAL_ID,'+ #13 +
      '      iORCAMENTO_ID'+ #13 +
      '    );'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  for i in iPRODUTOS_IDS.first..iPRODUTOS_IDS.last loop'+ #13 +
      #13 +
      '    /* Baixando a pendência */'+ #13 +
      '    VERIFICAR_PENDENCIA( iITENS_IDS(i), iLOCAIS_IDS(i), iLOTES(i), iQUANTIDADES(i) );'+ #13 +
      #13 +
      '    /* Inserindo o item na retirada */'+ #13 +
      '    INSERIR_ITEM( iPRODUTOS_IDS(i), iITENS_IDS(i), iLOTES(i), iQUANTIDADES(i) );'+ #13 +
      '    select'+ #13 +
      '      case when TIPO_CONTROLE_ESTOQUE in(''K'', ''A'') then ''S'' else ''N'' end'+ #13 +
      '    into'+ #13 +
      '      vEProdutoKit'+ #13 +
      '    from'+ #13 +
      '      ORCAMENTOS_ITENS'+ #13 +
      '    where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '    and ITEM_ID = iITENS_IDS(i);'+ #13 +
      #13 +
      '    /* Se for o kit, desagendando os componentes do kit */'+ #13 +
      '    if vEProdutoKit = ''S'' then'+ #13 +
      '      for xItensPendKit in cItensPendKit( iPRODUTOS_IDS(i), iITENS_IDS(i), iLOCAIS_IDS(i), iQUANTIDADES(i) ) loop'+ #13 +
      '        /* Baixando a pendência do produto que compõe o kit */'+ #13 +
      '        VERIFICAR_PENDENCIA( xItensPendKit.ITEM_ID, iLOCAIS_IDS(i), xItensPendKit.LOTE, xItensPendKit.QUANTIDADE );'+ #13 +
      #13 +
      '        /* Inserindo o item na retirada */'+ #13 +
      '        INSERIR_ITEM( xItensPendKit.PRODUTO_ID, xItensPendKit.ITEM_ID, xItensPendKit.LOTE, xItensPendKit.QUANTIDADE );'+ #13 +
      '      end loop;'+ #13 +
      '    end if;'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  select'+ #13 +
      '    count(*)'+ #13 +
      '  into'+ #13 +
      '    vQtde'+ #13 +
      '  from'+ #13 +
      '    ENTREGAS_ITENS_PENDENTES'+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '  and EMPRESA_ID = iEMPRESA_PEND_ID'+ #13 +
      '  and LOCAL_ID = iLOCAL_ID'+ #13 +
      '  and PREVISAO_ENTREGA = iPREVISAO_ENTREGA;'+ #13 +
      #13 +
      '  if vQtde = 0 then'+ #13 +
      '    delete from ENTREGAS_PENDENTES'+ #13 +
      '    where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '    and EMPRESA_ID = iEMPRESA_PEND_ID'+ #13 +
      '    and LOCAL_ID = iLOCAL_ID'+ #13 +
      '    and PREVISAO_ENTREGA = iPREVISAO_ENTREGA;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  GERAR_RETIRADA_RETIRA_PENDENTE('+ #13 +
      '    iORCAMENTO_ID,'+ #13 +
      '    iEMPRESA_PEND_ID,'+ #13 +
      '    iLOCAL_ID,'+ #13 +
      '    iTIPO_NOTA_GERAR,'+ #13 +
      '    iAGRUPADOR_ID,'+ #13 +
      '    iPRODUTOS_IDS,'+ #13 +
      '    iITENS_IDS,'+ #13 +
      '    iLOCAIS_IDS,'+ #13 +
      '    iQUANTIDADES,'+ #13 +
      '    iLOTES,'+ #13 +
      '    vRetiradaId'+ #13 +
      '  );'+ #13 +
      #13 +
      '  oRETIRADA_ID := vRetiradaId;'+ #13 +
      #13 +
      'end GERAR_RETIRADA_ENT_PENDENTE;'+ #13
    );
end;

function AjustarProcGERAR_PENDENCIA_ENTREGAS: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure GERAR_PENDENCIA_ENTREGAS('+ #13 +
      '  iORCAMENTO_ID       in number,'+ #13 +
      '  iEMPRESA_GERACAO_ID in number'+ #13 +
      ')'+ #13 +
      'is    '+ #13 +
      '  vQtdeAgendar             number;'+ #13 +
      '  vQtdeFaltaAgendar        number;'+ #13 +
      #13 +
      '  vQtdeAgendarLoteItemKit  number;'+ #13 +
      '  vQtdeFaltaAgendarItemKit number;'+ #13 +
      '  vQtdeAgendarKitEmpresa   number;'+ #13 +
      #13 +
      '  cursor cItens is'+ #13 +
      '  select'+ #13 +
      '    ITE.PRODUTO_ID,'+ #13 +
      '    ITE.ITEM_ID,'+ #13 +
      '    ITE.QUANTIDADE,    '+ #13 +
      '    PRO.ACEITAR_ESTOQUE_NEGATIVO,'+ #13 +
      '    ITE.PREVISAO_ENTREGA,'+ #13 +
      '    OIT.TIPO_CONTROLE_ESTOQUE'+ #13 +
      '  from'+ #13 +
      '    ENTREGAS_ITENS_A_GERAR_TEMP ITE    '+ #13 +
      #13 +
      '  inner join PRODUTOS PRO'+ #13 +
      '  on ITE.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
      #13 +
      '  inner join ORCAMENTOS_ITENS OIT'+ #13 +
      '  on ITE.ORCAMENTO_ID = OIT.ORCAMENTO_ID'+ #13 +
      '  and ITE.PRODUTO_ID = OIT.PRODUTO_ID'+ #13 +
      '  and ITE.ITEM_ID = OIT.ITEM_ID'+ #13 +
      '  and OIT.ITEM_KIT_ID is null'+ #13 +
      #13 +
      '  where ITE.ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '  and ITE.TIPO_ENTREGA = ''EN'''+ #13 +
      #13 +
      '  order by'+ #13 +
      '    ITE.TIPO_ENTREGA;  '+ #13 +
      #13 +
      '  cursor cEstoques(pPRODUTO_ID in number) is'+ #13 +
      '  select'+ #13 +
      '    GES.EMPRESA_GRUPO_ID as EMPRESA_ID,'+ #13 +
      '    DIV.PRODUTO_ID,'+ #13 +
      '    DIV.DISPONIVEL,    '+ #13 +
      '    DIV.LOTE,'+ #13 +
      '    DIV.LOCAL_ID'+ #13 +
      '  from'+ #13 +
      '    VW_ESTOQUES_DIVISAO DIV'+ #13 +
      #13 +
      '  inner join GRUPOS_ESTOQUES GES'+ #13 +
      '  on DIV.EMPRESA_ID = GES.EMPRESA_GRUPO_ID'+ #13 +
      '  and GES.TIPO = ''E'''+ #13 +
      '  and GES.EMPRESA_ID = iEMPRESA_GERACAO_ID'+ #13 +
      #13 +
      '  inner join PRODUTOS_ORDENS_LOC_ENTREGAS POL'+ #13 +
      '  on DIV.EMPRESA_ID = POL.EMPRESA_ID'+ #13 +
      '  and DIV.PRODUTO_PAI_ID = POL.PRODUTO_ID'+ #13 +
      '  and DIV.LOCAL_ID = POL.LOCAL_ID'+ #13 +
      '  and POL.TIPO = ''E'''+ #13 +
      #13 +
      '  where DIV.PRODUTO_ID = pPRODUTO_ID'+ #13 +
      '  and case when DIV.ACEITAR_ESTOQUE_NEGATIVO = ''S'' then 1 else DIV.DISPONIVEL end > 0'+ #13 +
      '  order by'+ #13 +
      '    GES.ORDEM,'+ #13 +
      '    POL.ORDEM;'+ #13 +
      #13 +
      #13 +
      '  cursor cEmpresasEstoqueKit is'+ #13 +
      '  select'+ #13 +
      '    EMPRESA_GRUPO_ID as EMPRESA_ID'+ #13 +
      '  from'+ #13 +
      '    GRUPOS_ESTOQUES'+ #13 +
      '  where TIPO = ''E'''+ #13 +
      '  and EMPRESA_ID = iEMPRESA_GERACAO_ID'+ #13 +
      '  order by'+ #13 +
      '    ORDEM;    '+ #13 +
      #13 +
      #13 +
      '  cursor cItensKit(pPRODUTO_KIT_ID in number, pITEM_KIT_ID in number, pQUANTIDADE_KITS in number) is'+ #13 +
      '  select'+ #13 +
      '    KIT.PRODUTO_ID,'+ #13 +
      '    ITE.ITEM_ID,'+ #13 +
      '    KIT.QUANTIDADE * pQUANTIDADE_KITS as QUANTIDADE,'+ #13 +
      '    PRO.ACEITAR_ESTOQUE_NEGATIVO'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS_ITENS ITE'+ #13 +
      #13 +
      '  inner join PRODUTOS_KIT KIT'+ #13 +
      '  on ITE.PRODUTO_ID = KIT.PRODUTO_ID'+ #13 +
      '  and KIT.PRODUTO_KIT_ID = pPRODUTO_KIT_ID'+ #13 +
      #13 +
      '  inner join PRODUTOS PRO'+ #13 +
      '  on KIT.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
      #13 +
      '  where ITE.ITEM_KIT_ID = pITEM_KIT_ID'+ #13 +
      '  and ORCAMENTO_ID = iORCAMENTO_ID;    '+ #13 +
      #13 +
      #13 +
      '  cursor cEstoqueItemKit(pEMPRESA_ID in number, pPRODUTO_ID in number, pLOCAL_ID in number) is'+ #13 +
      '  select'+ #13 +
      '    DIV.LOCAL_ID,'+ #13 +
      '    DIV.DISPONIVEL,'+ #13 +
      '    DIV.LOTE'+ #13 +
      '  from'+ #13 +
      '    VW_ESTOQUES_DIVISAO DIV'+ #13 +
      #13 +
      '  where DIV.PRODUTO_ID = pPRODUTO_ID'+ #13 +
      '  and DIV.EMPRESA_ID = pEMPRESA_ID'+ #13 +
      '  and DIV.LOCAL_ID = pLOCAL_ID'+ #13 +
      #13 +
      '  order by'+ #13 +
      '    DIV.DISPONIVEL desc;'+ #13 +
      #13 +
      #13 +
      '  cursor cItensDefLotes( pITEM_ID in number ) is'+ #13 +
      '  select'+ #13 +
      '    ITE.PRODUTO_ID,'+ #13 +
      '    OIL.LOTE,'+ #13 +
      '    OIL.QUANTIDADE_ENTREGAR as QUANTIDADE_AGENDAR'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS_ITENS_DEF_LOTES OIL'+ #13 +
      #13 +
      '  inner join ORCAMENTOS_ITENS ITE'+ #13 +
      '  on OIL.ORCAMENTO_ID = ITE.ORCAMENTO_ID'+ #13 +
      '  and OIL.ITEM_ID = ITE.ITEM_ID'+ #13 +
      #13 +
      '  where OIL.ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '  and OIL.ITEM_ID = pITEM_ID;'+ #13 +
      #13 +
      #13 +
      '  cursor cItensDefLotesEstoques( pPRODUTO_ID in number, pLOTE in string ) is'+ #13 +
      '  select'+ #13 +
      '    DIV.EMPRESA_ID,'+ #13 +
      '    DIV.LOCAL_ID,'+ #13 +
      '    DIV.DISPONIVEL'+ #13 +
      '  from'+ #13 +
      '    VW_ESTOQUES_DIVISAO DIV'+ #13 +
      #13 +
      '  inner join GRUPOS_ESTOQUES GRU'+ #13 +
      '  on GRU.TIPO = ''E'''+ #13 +
      '  and GRU.EMPRESA_GRUPO_ID = iEMPRESA_GERACAO_ID'+ #13 +
      '  and DIV.EMPRESA_ID = GRU.EMPRESA_ID'+ #13 +
      #13 +
      '  inner join PRODUTOS_ORDENS_LOC_ENTREGAS POL'+ #13 +
      '  on DIV.EMPRESA_ID = POL.EMPRESA_ID'+ #13 +
      '  and DIV.PRODUTO_ID = POL.PRODUTO_ID'+ #13 +
      '  and DIV.LOCAL_ID = POL.LOCAL_ID'+ #13 +
      '  and POL.TIPO = ''E'''+ #13 +
      #13 +
      '  where DIV.PRODUTO_ID = pPRODUTO_ID'+ #13 +
      '  and DIV.LOTE = pLOTE'+ #13 +
      '  order by'+ #13 +
      '    case when DIV.DISPONIVEL <= 0 then 1 else 0 end,'+ #13 +
      '    POL.ORDEM;'+ #13 +
      #13 +
      '  cursor cOrdemLocaisProdutoKit( pPRODUTO_KIT_ID in number, pEMPRESA_ESTOQUE_ID in number )'+ #13 +
      '  is'+ #13 +
      '  select'+ #13 +
      '    LOCAL_ID'+ #13 +
      '  from'+ #13 +
      '    PRODUTOS_ORDENS_LOC_ENTREGAS'+ #13 +
      '  where PRODUTO_ID = pPRODUTO_KIT_ID'+ #13 +
      '  and EMPRESA_ID = pEMPRESA_ESTOQUE_ID'+ #13 +
      '  and TIPO = ''E'''+ #13 +
      '  order by'+ #13 +
      '    ORDEM;'+ #13 +
      #13 +
      'begin'+ #13 +
      #13 +
      '  for xItens in cItens loop'+ #13 +
      '     vQtdeFaltaAgendar := xItens.QUANTIDADE;'+ #13 +
      '    /* Verificando se o usuário já selecionou os lotes */'+ #13 +
      '    /* Se sim apenas agendando e passando pra frente */'+ #13 +
      '    if xItens.TIPO_CONTROLE_ESTOQUE in(''L'', ''G'', ''P'') then'+ #13 +
      '      for xItensDefLotes in cItensDefLotes( xItens.ITEM_ID ) loop'+ #13 +
      '        vQtdeFaltaAgendar := xItensDefLotes.QUANTIDADE_AGENDAR;'+ #13 +
      #13 +
      '        for xItensDefLoteEstoq in cItensDefLotesEstoques( xItens.PRODUTO_ID, xItensDefLotes.LOTE ) loop'+ #13 +
      '          VERIFICAR_PENDENCIA_ENTREGA(xItensDefLoteEstoq.EMPRESA_ID, xItensDefLoteEstoq.LOCAL_ID, iORCAMENTO_ID, xItens.PREVISAO_ENTREGA, ''NHR'');'+ #13 +
      #13 +
      '          if xItensDefLoteEstoq.DISPONIVEL >= vQtdeFaltaAgendar then'+ #13 +
      '            vQtdeAgendar := vQtdeFaltaAgendar;'+ #13 +
      '            vQtdeFaltaAgendar := 0;'+ #13 +
      '          else'+ #13 +
      '            vQtdeAgendar := xItensDefLoteEstoq.DISPONIVEL;'+ #13 +
      '            vQtdeFaltaAgendar := vQtdeFaltaAgendar - vQtdeAgendar;'+ #13 +
      '          end if;'+ #13 +
      #13 +
      '          vQtdeFaltaAgendar := vQtdeFaltaAgendar - vQtdeAgendar;'+ #13 +
      #13 +
      '          begin'+ #13 +
      '            INSERIR_ENTREGAS_ITENS_PEND('+ #13 +
      '              xItensDefLoteEstoq.EMPRESA_ID,'+ #13 +
      '              xItensDefLoteEstoq.LOCAL_ID,'+ #13 +
      '              iORCAMENTO_ID,'+ #13 +
      '              xItens.PREVISAO_ENTREGA,'+ #13 +
      '              xItens.PRODUTO_ID,'+ #13 +
      '              xItens.ITEM_ID,'+ #13 +
      '              xItensDefLotes.LOTE,'+ #13 +
      '              vQtdeAgendar'+ #13 +
      '            );'+ #13 +
      '          exception'+ #13 +
      '            when others then'+ #13 +
      '              ERRO(vQtdeAgendar);'+ #13 +
      '          end;'+ #13 +
      #13 +
      '          if vQtdeFaltaAgendar = 0 then'+ #13 +
      '            exit;'+ #13 +
      '          end if;'+ #13 +
      '        end loop;'+ #13 +
      #13 +
      '        if vQtdeFaltaAgendar > 0 then'+ #13 +
      '          ERRO(''Estoque insuficiente para o produto '' || xItens.PRODUTO_ID || '', lote '' || xItensDefLotes.LOTE  || ''!'');'+ #13 +
      '        end if;'+ #13 +
      '      end loop;'+ #13 +
      #13 +
      '      if vQtdeFaltaAgendar > 0 then'+ #13 +
      '        ERRO(''Estoque insuficiente para o produto '' || xItens.PRODUTO_ID || ''!'');'+ #13 +
      '      end if;'+ #13 +
      '    /* Se for produto com controle de estoque por kit agrupado ou desmembrado */'+ #13 +
      '    elsif xItens.TIPO_CONTROLE_ESTOQUE in(''K'', ''A'') then'+ #13 +
      #13 +
      '      vQtdeFaltaAgendar := xItens.QUANTIDADE;'+ #13 +
      #13 +
      '      if xItens.TIPO_CONTROLE_ESTOQUE = ''K'' then'+ #13 +
      '        for xItensCompoeKit in cItensKit(xItens.PRODUTO_ID, xItens.ITEM_ID, xItens.QUANTIDADE) loop'+ #13 +
      #13 +
      '          vQtdeFaltaAgendarItemKit := xItensCompoeKit.QUANTIDADE;'+ #13 +
      #13 +
      '          for xEstoques in cEstoques(xItensCompoeKit.PRODUTO_ID) loop'+ #13 +
      '            if xItensCompoeKit.ACEITAR_ESTOQUE_NEGATIVO = ''S'' then'+ #13 +
      '              vQtdeAgendar := vQtdeFaltaAgendarItemKit;'+ #13 +
      '              vQtdeFaltaAgendarItemKit := 0;'+ #13 +
      '            elsif xEstoques.DISPONIVEL > 0 then'+ #13 +
      '              if xEstoques.DISPONIVEL >= vQtdeFaltaAgendar  then'+ #13 +
      '                vQtdeAgendar := vQtdeFaltaAgendarItemKit;'+ #13 +
      '                vQtdeFaltaAgendarItemKit := 0;'+ #13 +
      '              else'+ #13 +
      '                vQtdeAgendar := xEstoques.DISPONIVEL;'+ #13 +
      '                vQtdeFaltaAgendarItemKit := vQtdeFaltaAgendarItemKit - vQtdeAgendar;'+ #13 +
      '              end if;'+ #13 +
      '            else'+ #13 +
      '              /* Se não aceita estoque negativo, passando para o próximo local. */'+ #13 +
      '              continue;'+ #13 +
      '            end if;'+ #13 +
      #13 +
      '            VERIFICAR_PENDENCIA_ENTREGA(xEstoques.EMPRESA_ID, xEstoques.LOCAL_ID, iORCAMENTO_ID, xItens.PREVISAO_ENTREGA, ''NHR'');'+ #13 +
      #13 +
      '            INSERIR_ENTREGAS_ITENS_PEND('+ #13 +
      '              xEstoques.EMPRESA_ID,'+ #13 +
      '              xEstoques.LOCAL_ID,'+ #13 +
      '              iORCAMENTO_ID,'+ #13 +
      '              xItens.PREVISAO_ENTREGA,'+ #13 +
      '              xItensCompoeKit.PRODUTO_ID,'+ #13 +
      '              xItensCompoeKit.ITEM_ID,'+ #13 +
      '              xEstoques.LOTE,'+ #13 +
      '              vQtdeAgendar'+ #13 +
      '            );'+ #13 +
      #13 +
      '            if vQtdeFaltaAgendarItemKit = 0 then'+ #13 +
      '              vQtdeFaltaAgendar := 0;'+ #13 +
      '              exit;'+ #13 +
      '            end if;'+ #13 +
      '          end loop;'+ #13 +
      #13 +
      '          if vQtdeFaltaAgendarItemKit > 0 then'+ #13 +
      '            ERRO('+ #13 +
      '              ''Estoque insuficiente para o produto '' || xItensCompoeKit.PRODUTO_ID || '' que compõe o kit '' ||  xItens.PRODUTO_ID || ''!'' || chr(13) ||'+ #13 +
      '              ''Faltam: '' || NFORMAT(vQtdeFaltaAgendarItemKit, 0)'+ #13 +
      '            );'+ #13 +
      '          end if;'+ #13 +
      #13 +
      '        end loop;'+ #13 +
      #13 +
      '      /* Se kit entrega agrupada, detalhe é que neste todos os itens devem estar no mesmo local */'+ #13 +
      '      else'+ #13 +
      '        for xEmpresasEstoqueKit in cEmpresasEstoqueKit loop'+ #13 +
      '          vQtdeAgendarKitEmpresa := QUANTIDADE_KITS_DISP_EMPRESA(xEmpresasEstoqueKit.EMPRESA_ID, xItens.PRODUTO_ID);'+ #13 +
      #13 +
      '          -- Se não pode agendar nada, passa pra frente'+ #13 +
      '          if xItens.ACEITAR_ESTOQUE_NEGATIVO = ''S'' then'+ #13 +
      '            vQtdeAgendarKitEmpresa := xItens.QUANTIDADE;'+ #13 +
      '          elsif vQtdeAgendarKitEmpresa = 0 then'+ #13 +
      '            continue;'+ #13 +
      '          end if;'+ #13 +
      #13 +
      '          if vQtdeAgendarKitEmpresa >= vQtdeFaltaAgendar then'+ #13 +
      '            vQtdeAgendarKitEmpresa := vQtdeFaltaAgendar;'+ #13 +
      '          end if;'+ #13 +
      #13 +
      '          for xOrdemLocaisProdutoKit in cOrdemLocaisProdutoKit(xItens.PRODUTO_ID, xEmpresasEstoqueKit.EMPRESA_ID) loop'+ #13 +
      #13 +
      '            if xItens.ACEITAR_ESTOQUE_NEGATIVO = ''S'' then'+ #13 +
      '              vQtdeAgendar := vQtdeAgendarKitEmpresa;'+ #13 +
      '            else'+ #13 +
      '              vQtdeAgendar := QUANTIDADE_KITS_DISP_LOCAL(xEmpresasEstoqueKit.EMPRESA_ID, xOrdemLocaisProdutoKit.LOCAL_ID, xItens.PRODUTO_ID);'+ #13 +
      '              if vQtdeAgendar >= vQtdeAgendarKitEmpresa then'+ #13 +
      '                vQtdeAgendar := vQtdeAgendarKitEmpresa;'+ #13 +
      '              end if;'+ #13 +
      '            end if;'+ #13 +
      #13 +
      '            /* Se não pode agendar no local passando para o próximo */'+ #13 +
      '            if vQtdeAgendar = 0 then'+ #13 +
      '              continue;'+ #13 +
      '            end if;'+ #13 +
      #13 +
      '            /* Verificando o cabeçalho da pendência */'+ #13 +
      '            VERIFICAR_PENDENCIA_ENTREGA(xEmpresasEstoqueKit.EMPRESA_ID, xOrdemLocaisProdutoKit.LOCAL_ID, iORCAMENTO_ID, xItens.PREVISAO_ENTREGA, ''NHR'');'+ #13 +
      #13 +
      '            for xItensKit in cItensKit(xItens.PRODUTO_ID, xItens.ITEM_ID, vQtdeAgendar) loop'+ #13 +
      #13 +
      '              vQtdeFaltaAgendarItemKit := xItens.QUANTIDADE;'+ #13 +
      #13 +
      '              for xEstoqueItemKit in cEstoqueItemKit(xEmpresasEstoqueKit.EMPRESA_ID, xItensKit.PRODUTO_ID, xOrdemLocaisProdutoKit.LOCAL_ID) loop'+ #13 +
      #13 +
      '                if xEstoqueItemKit.DISPONIVEL >= vQtdeFaltaAgendarItemKit then'+ #13 +
      '                  vQtdeAgendarLoteItemKit := vQtdeFaltaAgendarItemKit;'+ #13 +
      '                  vQtdeFaltaAgendarItemKit := 0;'+ #13 +
      '                else'+ #13 +
      '                  vQtdeAgendarLoteItemKit := xEstoqueItemKit.DISPONIVEL;'+ #13 +
      '                  vQtdeFaltaAgendarItemKit := vQtdeFaltaAgendarItemKit - vQtdeAgendarLoteItemKit;'+ #13 +
      '                end if;'+ #13 +
      #13 +
      '                begin'+ #13 +
      '                  INSERIR_ENTREGAS_ITENS_PEND('+ #13 +
      '                    xEmpresasEstoqueKit.EMPRESA_ID,'+ #13 +
      '                    xOrdemLocaisProdutoKit.LOCAL_ID,'+ #13 +
      '                    iORCAMENTO_ID,'+ #13 +
      '                    xItens.PREVISAO_ENTREGA,'+ #13 +
      '                    xItensKit.PRODUTO_ID,'+ #13 +
      '                    xItensKit.ITEM_ID,'+ #13 +
      '                    xEstoqueItemKit.LOTE,'+ #13 +
      '                    vQtdeAgendarLoteItemKit'+ #13 +
      '                  );'+ #13 +
      '                exception'+ #13 +
      '                  when others then'+ #13 +
      '                    erro(vQtdeAgendarLoteItemKit || ''  '' || xItensKit.PRODUTO_ID || ''  '' || xEmpresasEstoqueKit.EMPRESA_ID || ''  '' || vQtdeAgendarLoteItemKit || ''  '' || sqlerrm);'+ #13 +
      '                end;'+ #13 +
      #13 +
      '                if vQtdeFaltaAgendarItemKit = 0 then'+ #13 +
      '                  exit;'+ #13 +
      '                end if;'+ #13 +
      '              end loop;'+ #13 +
      '            end loop;'+ #13 +
      #13 +
      '            /* Inserindo o próprio item kit */'+ #13 +
      '            INSERIR_ENTREGAS_ITENS_PEND('+ #13 +
      '              xEmpresasEstoqueKit.EMPRESA_ID,'+ #13 +
      '              xOrdemLocaisProdutoKit.LOCAL_ID,'+ #13 +
      '              iORCAMENTO_ID,'+ #13 +
      '              xItens.PREVISAO_ENTREGA,'+ #13 +
      '              xItens.PRODUTO_ID,'+ #13 +
      '              xItens.ITEM_ID,'+ #13 +
      '              ''???'','+ #13 +
      '              vQtdeAgendar'+ #13 +
      '            );'+ #13 +
      #13 +
      '            vQtdeAgendarKitEmpresa := vQtdeAgendarKitEmpresa - vQtdeAgendar;'+ #13 +
      '            vQtdeFaltaAgendar      := vQtdeFaltaAgendar - vQtdeAgendar;'+ #13 +
      #13 +
      '            if vQtdeAgendarKitEmpresa = 0 or vQtdeFaltaAgendar = 0 then'+ #13 +
      '              exit;'+ #13 +
      '            end if;'+ #13 +
      '          end loop;'+ #13 +
      #13 +
      '          if vQtdeAgendarKitEmpresa > 0 then'+ #13 +
      '            ERRO('+ #13 +
      '              ''Estoque insuficiente para o produto kit '' || xItens.PRODUTO_ID || ''!'' || chr(13) ||'+ #13 +
      '              ''Faltam: '' || NFORMAT(vQtdeAgendarKitEmpresa, 0) || chr(13) ||'+ #13 +
      '              ''Verifique os locais dos produtos que compõe este kit!'''+ #13 +
      '            );'+ #13 +
      '          end if;'+ #13 +
      #13 +
      '        end loop;'+ #13 +
      '      end if;'+ #13 +
      #13 +
      '      if vQtdeFaltaAgendar > 0 then'+ #13 +
      '        ERRO('+ #13 +
      '          ''Estoque insuficiente para o produto kit '' || xItens.PRODUTO_ID || ''!'' || chr(13) ||'+ #13 +
      '          ''Faltam: '' || NFORMAT(vQtdeFaltaAgendar, 0)'+ #13 +
      '        );'+ #13 +
      '      end if;'+ #13 +
      #13 +
      '    /* Se for produto normal */'+ #13 +
      '    else'+ #13 +
      '      vQtdeFaltaAgendar := xItens.QUANTIDADE;'+ #13 +
      #13 +
      '      for xEstoques in cEstoques(xItens.PRODUTO_ID) loop'+ #13 +
      '        if xItens.ACEITAR_ESTOQUE_NEGATIVO = ''S'' then'+ #13 +
      '          vQtdeAgendar := vQtdeFaltaAgendar;'+ #13 +
      '          vQtdeFaltaAgendar := 0;'+ #13 +
      '        elsif xEstoques.DISPONIVEL > 0 then'+ #13 +
      '          if xEstoques.DISPONIVEL >= vQtdeFaltaAgendar then'+ #13 +
      '            vQtdeAgendar := vQtdeFaltaAgendar;'+ #13 +
      '            vQtdeFaltaAgendar := 0;'+ #13 +
      '          else'+ #13 +
      '            vQtdeAgendar := xEstoques.DISPONIVEL;'+ #13 +
      '            vQtdeFaltaAgendar := vQtdeFaltaAgendar - vQtdeAgendar;'+ #13 +
      '          end if;'+ #13 +
      '        else'+ #13 +
      '          /* Se não aceita estoque negativo, passando para o próximo local. */'+ #13 +
      '          continue;'+ #13 +
      '        end if;'+ #13 +
      #13 +
      '        VERIFICAR_PENDENCIA_ENTREGA(xEstoques.EMPRESA_ID, xEstoques.LOCAL_ID, iORCAMENTO_ID, xItens.PREVISAO_ENTREGA, ''NHR'');'+ #13 +
      #13 +
      '        INSERIR_ENTREGAS_ITENS_PEND('+ #13 +
      '          xEstoques.EMPRESA_ID,'+ #13 +
      '          xEstoques.LOCAL_ID,'+ #13 +
      '          iORCAMENTO_ID,'+ #13 +
      '          xItens.PREVISAO_ENTREGA,'+ #13 +
      '          xItens.PRODUTO_ID,'+ #13 +
      '          xItens.ITEM_ID,'+ #13 +
      '          xEstoques.LOTE,'+ #13 +
      '          vQtdeAgendar'+ #13 +
      '        );'+ #13 +
      #13 +
      '        if vQtdeFaltaAgendar = 0 then'+ #13 +
      '          exit;'+ #13 +
      '        end if;'+ #13 +
      '      end loop;'+ #13 +
      #13 +
      '      if vQtdeFaltaAgendar > 0 then'+ #13 +
      '        ERRO( ''Estoque insuficiente para geração da entrega! Produto: '' || xItens.PRODUTO_ID );'+ #13 +
      '      end if;'+ #13 +
      #13 +
      '    end if;'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  /* Limpando a tabela no final do processo */'+ #13 +
      '  delete from ENTREGAS_ITENS_A_GERAR_TEMP'+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '  and TIPO_ENTREGA = ''EN'';'+ #13 +
      #13 +
      'end GERAR_PENDENCIA_ENTREGAS;'+ #13
    );
end;

function AjustarProcGERAR_ENTREGA_DE_PENDENCIA: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure GERAR_ENTREGA_DE_PENDENCIA('+ #13 +
      '  iORCAMENTO_ID     in number,'+ #13 +
      '  iEMPRESA_PEND_ID  in number,'+ #13 +
      '  iLOCAL_ID         in number,'+ #13 +
      '  iPREVISAO_ENTREGA in date,'+ #13 +
      '  iAGRUPADOR_ID     in number,'+ #13 +
      '  iTIPO_NOTA_GERAR  in string,'+ #13 +
      #13 +
      '  iPRODUTOS_IDS    in TIPOS.ArrayOfNumeros,'+ #13 +
      '  iITENS_IDS       in TIPOS.ArrayOfNumeros,'+ #13 +
      '  iLOCAIS_IDS      in TIPOS.ArrayOfNumeros,'+ #13 +
      '  iQUANTIDADES     in TIPOS.ArrayOfNumeros,'+ #13 +
      '  iLOTES           in TIPOS.ArrayOfString,'+ #13 +
      '  oENTREGA_ID     out number'+ #13 +
      ')'+ #13 +
      'is'+ #13 +
      '  i                           number;'+ #13 +
      '  vQtde                       number default 0;'+ #13 +
      '  vEntregaId                  number;'+ #13 +
      '  vEProdutoKit                char(1);'+ #13 +
      '  vTemItensExigirSeparacao    PRODUTOS.EXIGIR_SEPARACAO%type default ''N'';'+ #13 +
      '  vTrabalharControleSeparacao PARAMETROS_EMPRESA.TRABALHAR_CONTROLE_SEPARACAO%type;'+ #13 +
      #13 +
      '  cursor cItensKit(pPRODUTO_KIT_ID in number, pITEM_KIT_ID in number, pQUANTIDADE_KITS in number) is'+ #13 +
      '  select'+ #13 +
      '    KIT.PRODUTO_ID,'+ #13 +
      '    ITE.ITEM_ID,'+ #13 +
      '    KIT.QUANTIDADE * pQUANTIDADE_KITS as QUANTIDADE'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS_ITENS ITE'+ #13 +
      #13 +
      '  inner join PRODUTOS_KIT KIT'+ #13 +
      '  on ITE.PRODUTO_ID = KIT.PRODUTO_ID'+ #13 +
      '  and KIT.PRODUTO_KIT_ID = pPRODUTO_KIT_ID'+ #13 +
      #13 +
      '  where ITE.ITEM_KIT_ID = pITEM_KIT_ID'+ #13 +
      '  and ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      #13 +
      '  cursor cItensPendKit( pITEM_ID in number, pLOCAL_ID in number ) is'+ #13 +
      '  select'+ #13 +
      '    PRODUTO_ID,'+ #13 +
      '    LOTE,'+ #13 +
      '    SALDO'+ #13 +
      '  from'+ #13 +
      '    ENTREGAS_ITENS_PENDENTES'+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '  and EMPRESA_ID = iEMPRESA_PEND_ID'+ #13 +
      '  and LOCAL_ID = pLOCAL_ID'+ #13 +
      '  and ITEM_ID = pITEM_ID'+ #13 +
      '  and PREVISAO_ENTREGA = iPREVISAO_ENTREGA'+ #13 +
      '  order by'+ #13 +
      '    SALDO desc;'+ #13 +
      #13 +
      'begin'+ #13 +
      '  select'+ #13 +
      '    TRABALHAR_CONTROLE_SEPARACAO'+ #13 +
      '  into'+ #13 +
      '    vTrabalharControleSeparacao'+ #13 +
      '  from'+ #13 +
      '    PARAMETROS_EMPRESA'+ #13 +
      '  where EMPRESA_ID = iEMPRESA_PEND_ID;'+ #13 +
      #13 +
      '  select SEQ_ENTREGAS_ID.nextval'+ #13 +
      '  into vEntregaId'+ #13 +
      '  from dual;'+ #13 +
      #13 +
      '  insert into ENTREGAS('+ #13 +
      '    ENTREGA_ID,'+ #13 +
      '    EMPRESA_ENTREGA_ID,          '+ #13 +
      '    EMPRESA_GERACAO_ID,'+ #13 +
      '    LOCAL_ID,'+ #13 +
      '    ORCAMENTO_ID,'+ #13 +
      '    PREVISAO_ENTREGA,'+ #13 +
      '    AGRUPADOR_ID,'+ #13 +
      '    STATUS,'+ #13 +
      '    TIPO_NOTA_GERAR'+ #13 +
      '  )values('+ #13 +
      '    vEntregaId,'+ #13 +
      '    iEMPRESA_PEND_ID,'+ #13 +
      '    iEMPRESA_PEND_ID,'+ #13 +
      '    iLOCAL_ID,'+ #13 +
      '    iORCAMENTO_ID,'+ #13 +
      '    iPREVISAO_ENTREGA,'+ #13 +
      '    iAGRUPADOR_ID,'+ #13 +
      '    ''AGC'','+ #13 +
      '    iTIPO_NOTA_GERAR'+ #13 +
      '  );'+ #13 +
      #13 +
      '  for i in iPRODUTOS_IDS.first..iPRODUTOS_IDS.last loop'+ #13 +
      #13 +
      '    /* Baixando a pendência */'+ #13 +
      '    update ENTREGAS_ITENS_PENDENTES set'+ #13 +
      '      ENTREGUES = ENTREGUES + iQUANTIDADES(i)'+ #13 +
      '    where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '    and EMPRESA_ID = iEMPRESA_PEND_ID'+ #13 +
      '    and PREVISAO_ENTREGA = iPREVISAO_ENTREGA'+ #13 +
      '    and LOCAL_ID = iLOCAIS_IDS(i)'+ #13 +
      '    and ITEM_ID = iITENS_IDS(i)'+ #13 +
      '    and LOTE = iLOTES(i);'+ #13 +
      #13 +
      '    /* Inserindo o produto na entrega */'+ #13 +
      '    insert into ENTREGAS_ITENS('+ #13 +
      '      ENTREGA_ID,'+ #13 +
      '      PRODUTO_ID,'+ #13 +
      '      ITEM_ID,'+ #13 +
      '      LOTE,'+ #13 +
      '      QUANTIDADE'+ #13 +
      '    )values('+ #13 +
      '      vEntregaId,'+ #13 +
      '      iPRODUTOS_IDS(i),'+ #13 +
      '      iITENS_IDS(i),'+ #13 +
      '      iLOTES(i),'+ #13 +
      '      iQUANTIDADES(i)'+ #13 +
      '    );'+ #13 +
      #13 +
      '    select'+ #13 +
      '      case when TIPO_CONTROLE_ESTOQUE in(''K'', ''A'') then ''S'' else ''N'' end'+ #13 +
      '    into'+ #13 +
      '      vEProdutoKit'+ #13 +
      '    from'+ #13 +
      '      ORCAMENTOS_ITENS'+ #13 +
      '    where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '    and ITEM_ID = iITENS_IDS(i);'+ #13 +
      #13 +
      '    /* Se for o kit, baixando os componentes do kit */'+ #13 +
      '    if vEProdutoKit = ''S'' then'+ #13 +
      '      for xItensKit in cItensKit(iPRODUTOS_IDS(i), iITENS_IDS(i), iQUANTIDADES(i)) loop'+ #13 +
      #13 +
      '        for xItensPendKit in cItensPendKit( xItensKit.ITEM_ID, iLOCAIS_IDS(i) ) loop'+ #13 +
      '          update ENTREGAS_ITENS_PENDENTES set'+ #13 +
      '            ENTREGUES = ENTREGUES + xItensKit.QUANTIDADE'+ #13 +
      '          where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '          and EMPRESA_ID = iEMPRESA_PEND_ID'+ #13 +
      '          and LOCAL_ID = iLOCAIS_IDS(i)'+ #13 +
      '          and PREVISAO_ENTREGA = iPREVISAO_ENTREGA'+ #13 +
      '          and ITEM_ID = xItensKit.ITEM_ID'+ #13 +
      '          and LOTE = xItensPendKit.LOTE;'+ #13 +
      #13 +
      '          /* Inserindo de fato a retirada pelo cliente */'+ #13 +
      '          insert into ENTREGAS_ITENS('+ #13 +
      '            ENTREGA_ID,'+ #13 +
      '            PRODUTO_ID,'+ #13 +
      '            ITEM_ID,'+ #13 +
      '            LOTE,'+ #13 +
      '            QUANTIDADE'+ #13 +
      '          )values('+ #13 +
      '            vEntregaId,'+ #13 +
      '            xItensPendKit.PRODUTO_ID,'+ #13 +
      '            xItensKit.ITEM_ID,'+ #13 +
      '            xItensPendKit.LOTE,'+ #13 +
      '            xItensKit.QUANTIDADE'+ #13 +
      '          );'+ #13 +
      '        end loop;'+ #13 +
      '      end loop;'+ #13 +
      #13 +
      '    end if;'+ #13 +
      #13 +
      '    if vTrabalharControleSeparacao = ''S'' and vTemItensExigirSeparacao = ''N'' then'+ #13 +
      '      select'+ #13 +
      '        EXIGIR_SEPARACAO'+ #13 +
      '      into'+ #13 +
      '        vTemItensExigirSeparacao'+ #13 +
      '      from'+ #13 +
      '        PRODUTOS'+ #13 +
      '      where PRODUTO_ID = iPRODUTOS_IDS(i);'+ #13 +
      '    end if;'+ #13 +
      #13 +
      '  end loop;'+ #13 +
      #13 +
      '  if vTemItensExigirSeparacao = ''S'' then'+ #13 +
      '    update ENTREGAS set'+ #13 +
      '      STATUS = ''AGS'''+ #13 +
      '    where ENTREGA_ID = vEntregaId;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  oENTREGA_ID := vEntregaId;'+ #13 +
      #13 +
      'end GERAR_ENTREGA_DE_PENDENCIA;'+ #13
    );
end;

function AjustarProcDESAGENDAR_ENTREGA_PENDENTE: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure DESAGENDAR_ENTREGA_PENDENTE('+ #13 +
      '  iEMPRESA_PEND_ID  in number,'+ #13 +
      '  iORCAMENTO_ID     in number,'+ #13 +
      '  iPREVISAO_ENTREGA in date,'+ #13 +
      '  iLOCAL_ID         in number,'+ #13 +
      '  iPRODUTOS_IDS     in TIPOS.ArrayOfNumeros,'+ #13 +
      '  iITENS_IDS        in TIPOS.ArrayOfNumeros,'+ #13 +
      '  iQUANTIDADES      in TIPOS.ArrayOfNumeros,'+ #13 +
      '  iLOTES            in TIPOS.ArrayOfString'+ #13 +
      ')'+ #13 +
      'is'+ #13 +
      '  i            number;'+ #13 +
      '  vQtde        number;'+ #13 +
      '  vEProdutoKit char(1);'+ #13 +
      #13 +
      '  cursor cItensPendKit( pPRODUTO_KIT_ID in number, pITEM_KIT_ID in number, pQUANTIDADE_PRODUTO_KIT in number ) is'+ #13 +
      '  select'+ #13 +
      '    EIP.PRODUTO_ID,'+ #13 +
      '    EIP.ITEM_ID,'+ #13 +
      '    EIP.LOTE,'+ #13 +
      '    KIT.QUANTIDADE * pQUANTIDADE_PRODUTO_KIT as QUANTIDADE'+ #13 +
      '  from'+ #13 +
      '    ENTREGAS_ITENS_PENDENTES EIP'+ #13 +
      #13 +
      '  inner join ORCAMENTOS_ITENS OIT'+ #13 +
      '  on EIP.ORCAMENTO_ID = OIT.ORCAMENTO_ID'+ #13 +
      '  and EIP.ITEM_ID = OIT.ITEM_ID'+ #13 +
      #13 +
      '  inner join PRODUTOS_KIT KIT'+ #13 +
      '  on KIT.PRODUTO_KIT_ID = pPRODUTO_KIT_ID'+ #13 +
      '  and OIT.PRODUTO_ID = KIT.PRODUTO_ID'+ #13 +
      #13 +
      '  where EIP.ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '  and EIP.EMPRESA_ID = iEMPRESA_PEND_ID'+ #13 +
      '  and OIT.ITEM_KIT_ID = pITEM_KIT_ID'+ #13 +
      '  and EIP.PREVISAO_ENTREGA = iPREVISAO_ENTREGA'+ #13 +
      '  and EIP.LOCAL_ID = iLOCAL_ID;'+ #13 +
      #13 +
      '  procedure VERIFICAR_PENDENCIA( pITEM_ID in number, pLOTE in string, pQUANTIDADE in number ) '+ #13 +
      '  is'+ #13 +
      '    vPodeDeletarPendencia char(1);'+ #13 +
      '  begin'+ #13 +
      #13 +
      '   select'+ #13 +
      '      case when QUANTIDADE = pQUANTIDADE then ''S'' else ''N'' end as PODE_DELETAR_PENDENCIA'+ #13 +
      '    into'+ #13 +
      '      vPodeDeletarPendencia'+ #13 +
      '    from'+ #13 +
      '      ENTREGAS_ITENS_PENDENTES'+ #13 +
      '    where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '    and EMPRESA_ID = iEMPRESA_PEND_ID'+ #13 +
      '    and PREVISAO_ENTREGA = iPREVISAO_ENTREGA'+ #13 +
      '    and LOCAL_ID = iLOCAL_ID'+ #13 +
      '    and ITEM_ID = pITEM_ID'+ #13 +
      '    and LOTE = pLOTE;  '+ #13 +
      #13 +
      '    /* Caso a quantidade sendo desagendada seja a mesma quantidade da pendencia, quer dizer que o usuário está desagendando toda a pendência, */'+ #13 +
      '    /* Neste caso vamos deleta-la */'+ #13 +
      '    if vPodeDeletarPendencia = ''S'' then'+ #13 +
      '      delete from ENTREGAS_ITENS_PENDENTES'+ #13 +
      '      where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '      and EMPRESA_ID = iEMPRESA_PEND_ID'+ #13 +
      '      and PREVISAO_ENTREGA = iPREVISAO_ENTREGA'+ #13 +
      '      and LOCAL_ID = iLOCAL_ID'+ #13 +
      '      and ITEM_ID = pITEM_ID'+ #13 +
      '      and LOTE = pLOTE;      '+ #13 +
      '    else'+ #13 +
      '      /* Baixando a pendência */'+ #13 +
      '      update ENTREGAS_ITENS_PENDENTES set'+ #13 +
      '        QUANTIDADE = QUANTIDADE - pQUANTIDADE'+ #13 +
      '      where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '      and EMPRESA_ID = iEMPRESA_PEND_ID'+ #13 +
      '      and PREVISAO_ENTREGA = iPREVISAO_ENTREGA'+ #13 +
      '      and LOCAL_ID = iLOCAL_ID'+ #13 +
      '      and ITEM_ID = pITEM_ID'+ #13 +
      '      and LOTE = pLOTE;'+ #13 +
      '    end if;'+ #13 +
      #13 +
      '  end;'+ #13 +
      #13 +
      '  procedure VERIFICAR_SEM_PREVISAO( pPRODUTO_ID in number, pITEM_ID in number, pQUANTIDADE in number ) '+ #13 +
      '  is'+ #13 +
      '    vQtde number;  '+ #13 +
      '  begin'+ #13 +
      '    select'+ #13 +
      '      count(*)'+ #13 +
      '    into'+ #13 +
      '      vQtde'+ #13 +
      '    from'+ #13 +
      '      ENTREGAS_ITENS_SEM_PREVISAO'+ #13 +
      '    where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '    and ITEM_ID = pITEM_ID;'+ #13 +
      #13 +
      '    if vQtde = 0 then'+ #13 +
      '      /* Inserindo a entrega sem previsão caso não exista */'+ #13 +
      '      insert into ENTREGAS_ITENS_SEM_PREVISAO(        '+ #13 +
      '        ORCAMENTO_ID,'+ #13 +
      '        PRODUTO_ID,'+ #13 +
      '        ITEM_ID,'+ #13 +
      '        QUANTIDADE,'+ #13 +
      '        AGUARDAR_CONTATO_CLIENTE'+ #13 +
      '      )values('+ #13 +
      '        iORCAMENTO_ID,'+ #13 +
      '        pPRODUTO_ID,'+ #13 +
      '        pITEM_ID,        '+ #13 +
      '        pQUANTIDADE,'+ #13 +
      '        ''N'''+ #13 +
      '      );'+ #13 +
      '    else      '+ #13 +
      '      update ENTREGAS_ITENS_SEM_PREVISAO set        '+ #13 +
      '        QUANTIDADE = QUANTIDADE + pQUANTIDADE'+ #13 +
      '      where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '      and PRODUTO_ID = pPRODUTO_ID'+ #13 +
      '      and ITEM_ID = pITEM_ID;'+ #13 +
      '    end if;   '+ #13 +
      #13 +
      '  end;'+ #13 +
      #13 +
      'begin'+ #13 +
      #13 +
      '  for i in iPRODUTOS_IDS.first..iPRODUTOS_IDS.last loop'+ #13 +
      #13 +
      '    /* Baixando a pendência */'+ #13 +
      '    VERIFICAR_PENDENCIA( iITENS_IDS(i), iLOTES(i), iQUANTIDADES(i) );'+ #13 +
      #13 +
      '    /* Criando o sem previsão */'+ #13 +
      '    VERIFICAR_SEM_PREVISAO( iPRODUTOS_IDS(i), iITENS_IDS(i), iQUANTIDADES(i) );'+ #13 +
      #13 +
      '    select'+ #13 +
      '      case when TIPO_CONTROLE_ESTOQUE in(''K'', ''A'') then ''S'' else ''N'' end'+ #13 +
      '    into'+ #13 +
      '      vEProdutoKit'+ #13 +
      '    from'+ #13 +
      '      ORCAMENTOS_ITENS'+ #13 +
      '    where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '    and ITEM_ID = iITENS_IDS(i);  '+ #13 +
      #13 +
      '    /* Se for o kit, desagendando os componentes do kit */'+ #13 +
      '    if vEProdutoKit = ''S'' then                  '+ #13 +
      '      for xItensPendKit in cItensPendKit( iPRODUTOS_IDS(i), iITENS_IDS(i), iQUANTIDADES(i) ) loop'+ #13 +
      '        /* Baixando a pendência */'+ #13 +
      '        VERIFICAR_PENDENCIA( xItensPendKit.ITEM_ID, xItensPendKit.LOTE, xItensPendKit.QUANTIDADE );'+ #13 +
      #13 +
      '        /* Criando o sem previsão */'+ #13 +
      '        VERIFICAR_SEM_PREVISAO( xItensPendKit.PRODUTO_ID, xItensPendKit.ITEM_ID, xItensPendKit.QUANTIDADE );'+ #13 +
      '      end loop;'+ #13 +
      '    end if;    '+ #13 +
      '  end loop;'+ #13 +
      #13 +
      'end DESAGENDAR_ENTREGA_PENDENTE;'+ #13
    );
end;

function AjustarProcAGENDAR_ITENS_SEM_PREVISAO: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure AGENDAR_ITENS_SEM_PREVISAO('+ #13 +
      '  iORCAMENTO_ID       in number,'+ #13 +
      '  iEMPRESA_GERACAO_ID in number,'+ #13 +
      '  iPREVISAO_ENTREGA   in date,'+ #13 +
      '  iVetPRODUTOS_IDS    in TIPOS.ArrayOfNumeros,'+ #13 +
      '  iVetITENS_IDS       in TIPOS.ArrayOfNumeros,'+ #13 +
      '  iVetQUANTIDADES     in TIPOS.ArrayOfNumeros'+ #13 +
      ')'+ #13 +
      'is'+ #13 +
      '  i            number;'+ #13 +
      '  vStatus      ORCAMENTOS.STATUS%type;'+ #13 +
      '  vEProdutoKit char(1) default ''N'';'+ #13 +
      #13 +
      '  cursor cItensKit(pPRODUTO_KIT_ID in number, pITEM_KIT_ID in number, pQUANTIDADE_KITS in number) is'+ #13 +
      '  select'+ #13 +
      '    KIT.PRODUTO_ID,'+ #13 +
      '    ITE.ITEM_ID,'+ #13 +
      '    KIT.QUANTIDADE * pQUANTIDADE_KITS as QUANTIDADE'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS_ITENS ITE'+ #13 +
      #13 +
      '  inner join PRODUTOS_KIT KIT'+ #13 +
      '  on ITE.PRODUTO_ID = KIT.PRODUTO_ID'+ #13 +
      '  and KIT.PRODUTO_KIT_ID = pPRODUTO_KIT_ID'+ #13 +
      #13 +
      '  where ITE.ITEM_KIT_ID = pITEM_KIT_ID'+ #13 +
      '  and ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      #13 +
      'begin'+ #13 +
      '  select'+ #13 +
      '    STATUS'+ #13 +
      '  into'+ #13 +
      '    vStatus'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS'+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      #13 +
      '  if vStatus not in(''VE'', ''RE'') then'+ #13 +
      '    ERRO(''O orçamento ainda não foi recebido, verifique!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  for i in iVetPRODUTOS_IDS.first..iVetPRODUTOS_IDS.last loop'+ #13 +
      #13 +
      '    select'+ #13 +
      '      case when TIPO_CONTROLE_ESTOQUE in(''K'', ''A'') then ''S'' else ''N'' end'+ #13 +
      '    into'+ #13 +
      '      vEProdutoKit'+ #13 +
      '    from'+ #13 +
      '      ORCAMENTOS_ITENS'+ #13 +
      '    where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '    and ITEM_ID = iVetITENS_IDS(i);'+ #13 +
      #13 +
      '    if vEProdutoKit = ''S'' then'+ #13 +
      '      for xItensKit in cItensKit(iVetPRODUTOS_IDS(i), iVetITENS_IDS(i), iVetQUANTIDADES(i)) loop'+ #13 +
      '        update ENTREGAS_ITENS_SEM_PREVISAO set'+ #13 +
      '          ENTREGUES = ENTREGUES + xItensKit.QUANTIDADE'+ #13 +
      '        where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '        and PRODUTO_ID = xItensKit.PRODUTO_ID'+ #13 +
      '        and ITEM_ID = xItensKit.ITEM_ID;'+ #13 +
      '      end loop;'+ #13 +
      '    end if;'+ #13 +
      #13 +
      '    update ENTREGAS_ITENS_SEM_PREVISAO set'+ #13 +
      '      ENTREGUES = ENTREGUES + iVetQUANTIDADES(i)'+ #13 +
      '    where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '    and PRODUTO_ID = iVetPRODUTOS_IDS(i)'+ #13 +
      '    and ITEM_ID = iVetITENS_IDS(i);'+ #13 +
      #13 +
      '    insert into ENTREGAS_ITENS_A_GERAR_TEMP('+ #13 +
      '      ORCAMENTO_ID,'+ #13 +
      '      PREVISAO_ENTREGA,'+ #13 +
      '      PRODUTO_ID,'+ #13 +
      '      ITEM_ID,'+ #13 +
      '      QUANTIDADE,'+ #13 +
      '      TIPO_ENTREGA'+ #13 +
      '    )values('+ #13 +
      '      iORCAMENTO_ID,'+ #13 +
      '      iPREVISAO_ENTREGA,'+ #13 +
      '      iVetPRODUTOS_IDS(i),'+ #13 +
      '      iVetITENS_IDS(i),'+ #13 +
      '      iVetQUANTIDADES(i),'+ #13 +
      '      ''EN'''+ #13 +
      '    );'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  GERAR_PENDENCIA_ENTREGAS(iORCAMENTO_ID, iEMPRESA_GERACAO_ID);'+ #13 +
      #13 +
      '  /* Removendo as pendência já que não existem mais */'+ #13 +
      '  delete from ENTREGAS_ITENS_SEM_PREVISAO'+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '  and SALDO = 0;'+ #13 +
      #13 +
      'end AGENDAR_ITENS_SEM_PREVISAO;'+ #13
    );
end;

function AjustarProcVERIFICAR_LOTE_CADASTRADO: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure VERIFICAR_LOTE_CADASTRADO('+ #13 +
      '  iEMPRESA_ID       in number,'+ #13 +
      '  iPRODUTO_ID       in number,'+ #13 +
      '  iLOCAL_ID         in number,'+ #13 +
      '  iLOTE             in string,'+ #13 +
      '  iDATA_FABRICACAO  in date,'+ #13 +
      '  iDATA_VENCIMENTO  in date'+ #13 +
      ')'+ #13 +
      'is'+ #13 +
      '  vQtde  number;'+ #13 +
      'begin'+ #13 +
      #13 +
      '  select'+ #13 +
      '    count(*)'+ #13 +
      '  into'+ #13 +
      '    vQtde'+ #13 +
      '  from'+ #13 +
      '    PRODUTOS'+ #13 +
      '  where TIPO_CONTROLE_ESTOQUE in(''K'', ''A'')'+ #13 +
      '  and PRODUTO_ID = iPRODUTO_ID;'+ #13 +
      #13 +
      '  if vQtde > 0 then'+ #13 +
      '    return;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  select'+ #13 +
      '    count(*)'+ #13 +
      '  into'+ #13 +
      '    vQtde'+ #13 +
      '  from'+ #13 +
      '    PRODUTOS_LOTES'+ #13 +
      '  where PRODUTO_ID = iPRODUTO_ID'+ #13 +
      '  and LOTE = iLOTE;'+ #13 +
      #13 +
      '  if vQtde = 0 then'+ #13 +
      '    insert into PRODUTOS_LOTES('+ #13 +
      '      PRODUTO_ID,'+ #13 +
      '      LOTE,'+ #13 +
      '      DATA_FABRICACAO,'+ #13 +
      '      DATA_VENCIMENTO'+ #13 +
      '    )values('+ #13 +
      '      iPRODUTO_ID,'+ #13 +
      '      iLOTE,'+ #13 +
      '      iDATA_FABRICACAO,'+ #13 +
      '      iDATA_VENCIMENTO'+ #13 +
      '    );'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  select'+ #13 +
      '    count(*)'+ #13 +
      '  into'+ #13 +
      '    vQtde'+ #13 +
      '  from'+ #13 +
      '    ESTOQUES_DIVISAO'+ #13 +
      '  where EMPRESA_ID = iEMPRESA_ID'+ #13 +
      '  and LOCAL_ID = iLOCAL_ID'+ #13 +
      '  and PRODUTO_ID = iPRODUTO_ID'+ #13 +
      '  and LOTE = iLOTE;'+ #13 +
      #13 +
      '  if vQtde = 0 then'+ #13 +
      '    insert into ESTOQUES_DIVISAO('+ #13 +
      '      EMPRESA_ID,'+ #13 +
      '      PRODUTO_ID,'+ #13 +
      '      LOCAL_ID,'+ #13 +
      '      LOTE'+ #13 +
      '    )values('+ #13 +
      '      iEMPRESA_ID,'+ #13 +
      '      iPRODUTO_ID,'+ #13 +
      '      iLOCAL_ID,'+ #13 +
      '      iLOTE'+ #13 +
      '    );'+ #13 +
      '  end if;'+ #13 +
      #13 +
      'end VERIFICAR_LOTE_CADASTRADO;'+ #13
    );
end;

function AjustarCK_CONTAS_RECEBER_BAIXAS_TIPO: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'alter table CONTAS_RECEBER_BAIXAS'+ #13 +
      'drop constraint CK_CONTAS_RECEBER_BAIXAS_TIPO;'+ #13 +
      #13 +
      'alter table CONTAS_RECEBER_BAIXAS'+ #13 +
      'add constraint CK_CONTAS_RECEBER_BAIXAS_TIPO'+ #13 +
      'check('+ #13 +
      '  TIPO in(''CRE'', ''BCR'', ''BCA'', ''BBO'')'+ #13 +
      '  or'+ #13 +
      '  (TIPO = ''ECR'' and BAIXA_PAGAR_ORIGEM_ID is not null)'+ #13 +
      '  or'+ #13 +
      '  (TIPO = ''ADI'' and RECEBER_ADIANTADO_ID is not null)'+ #13 +
      ');'+ #13
    );
end;

function AjustarCK_CONTAS_REC_BX_TIPO_CAD_ID: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'alter table CONTAS_RECEBER_BAIXAS'+ #13 +
      'drop constraint CK_CONTAS_REC_BX_TIPO_CAD_ID;'+ #13 +
      #13 +
      'alter table CONTAS_RECEBER_BAIXAS'+ #13 +
      'add constraint CK_CONTAS_REC_BX_TIPO_CAD_ID'+ #13 +
      'check('+ #13 +
      '  (TIPO in(''CRE'', ''BCR'', ''ECR'', ''ADI'') and CADASTRO_ID is not null)'+ #13 +
      '  or'+ #13 +
      '  (TIPO in(''BCA'', ''BBO'') and CADASTRO_ID is null)'+ #13 +
      ');'+ #13
    );
end;

function AjustarTriggerCONTAS_RECEBER_BX_ITENS_IU_BR: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace trigger CONTAS_RECEBER_BX_ITENS_IU_BR'+ #13 +
      'before insert or update'+ #13 +
      'on CONTAS_RECEBER_BAIXAS_ITENS'+ #13 +
      'for each row'+ #13 +
      'declare'+ #13 +
      '  vStatus CONTAS_RECEBER.STATUS%type;'+ #13 +
      'begin'+ #13 +
      #13 +
      '  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
      '  :new.DATA_HORA_ALTERACAO := sysdate;'+ #13 +
      '  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;'+ #13 +
      '  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;'+ #13 +
      #13 +
      '  if inserting then'+ #13 +
      '    select'+ #13 +
      '      STATUS'+ #13 +
      '    into'+ #13 +
      '      vStatus'+ #13 +
      '    from'+ #13 +
      '      CONTAS_RECEBER'+ #13 +
      '    where RECEBER_ID = :new.RECEBER_ID;'+ #13 +
      #13 +
      '    if vStatus = ''B'' then'+ #13 +
      '      ERRO(''O contas a receber '' || :new.RECEBER_ID || '' já está com o status baixado, baixa não permitida!'');'+ #13 +
      '    end if;'+ #13 +
      #13 +
      '    update CONTAS_RECEBER set'+ #13 +
      '      STATUS = ''B'','+ #13 +
      '      BAIXA_ID = :new.BAIXA_ID'+ #13 +
      '    where RECEBER_ID = :new.RECEBER_ID;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      'end CONTAS_RECEBER_BX_ITENS_IU_BR;'+ #13
    );
end;

function AjustarProcedureCANCELAR_FECHAMENTO_PEDIDO: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure CANCELAR_FECHAMENTO_PEDIDO(iORCAMENTO_ID in number)'+ #13 +
      'is'+ #13 +
      '  vQtde         number;'+ #13 +
      '  vValorCredito ORCAMENTOS.VALOR_CREDITO%type;'+ #13 +
      '  vClienteId    ORCAMENTOS.CLIENTE_ID%type;'+ #13 +
      '  vStatus       ORCAMENTOS.STATUS%type;'+ #13 +
      #13 +
      '  cursor cProdutos is'+ #13 +
      '  select'+ #13 +
      '    PRODUTO_ID,'+ #13 +
      '    QUANTIDADE'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS_ITENS'+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '  order by PRODUTO_ID, ITEM_ID;'+ #13 +
      #13 +
      #13 +
      '  cursor cCreditos is'+ #13 +
      '  select distinct'+ #13 +
      '    PAG.BAIXA_ID'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS_CREDITOS ORC'+ #13 +
      #13 +
      '  inner join CONTAS_PAGAR PAG'+ #13 +
      '  on ORC.PAGAR_ID = PAG.PAGAR_ID'+ #13 +
      #13 +
      '  where ORC.ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '  and PAG.BAIXA_ID is not null;'+ #13 +
      #13 +
      'begin'+ #13 +
      #13 +
      '  select'+ #13 +
      '    CLIENTE_ID,'+ #13 +
      '    VALOR_CREDITO,'+ #13 +
      '    STATUS'+ #13 +
      '  into'+ #13 +
      '    vClienteId,'+ #13 +
      '    vValorCredito,'+ #13 +
      '    vStatus'+ #13 +
      '  from ORCAMENTOS'+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      #13 +
      '  if vStatus not in(''VR'', ''VB'', ''VE'', ''OB'') then'+ #13 +
      '    ERRO(''A venda não está no status de "Ag. recebimento", "venda bloqueada", "Ag.recebimento entrega" ou "Orçamento bloqueado", por favor verifique!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if vStatus = ''VE'' then'+ #13 +
      '    select'+ #13 +
      '      count(*)'+ #13 +
      '    into'+ #13 +
      '      vQtde'+ #13 +
      '    from'+ #13 +
      '      ENTREGAS'+ #13 +
      '    where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      #13 +
      '    if vQtde > 0 then'+ #13 +
      '      ERRO(''Não é permitido cancelar o fechamento de um pedido para recebimento na entrega que já possui entregas geradas!'');'+ #13 +
      '    end if;'+ #13 +
      #13 +
      '    select'+ #13 +
      '      count(*)'+ #13 +
      '    into'+ #13 +
      '      vQtde'+ #13 +
      '    from'+ #13 +
      '      DEVOLUCOES'+ #13 +
      '    where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      #13 +
      '    if vQtde > 0 then'+ #13 +
      '      ERRO(''Não é permitido cancelar o fechamento de um pedido para recebimento na entrega que já possui devoluções!'');'+ #13 +
      '    end if;'+ #13 +
      #13 +
      '    /* Apagando as previsões dos recebimentos na entrega */'+ #13 +
      '    delete from CONTAS_RECEBER'+ #13 +
      '    where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '    and COBRANCA_ID = SESSAO.PARAMETROS_GERAIS.TIPO_COB_RECEB_ENTREGA_ID;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  /* ----- Deletando as pendências de retiradas e entregas ------ */'+ #13 +
      '  delete from RETIRADAS_ITENS'+ #13 +
      '  where RETIRADA_ID in('+ #13 +
      '    select'+ #13 +
      '      RETIRADA_ID'+ #13 +
      '    from'+ #13 +
      '      RETIRADAS'+ #13 +
      '    where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '  );'+ #13 +
      #13 +
      '  delete from RETIRADAS'+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      #13 +
      '  delete from RETIRADAS_ITENS_PENDENTES'+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      #13 +
      '  delete from RETIRADAS_PENDENTES'+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      #13 +
      '  delete from ENTREGAS_ITENS_PENDENTES'+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      #13 +
      '  delete from ENTREGAS_PENDENTES'+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      #13 +
      '  delete from ENTREGAS_ITENS_SEM_PREVISAO'+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      '  /* ----------------------------------------------------------- */'+ #13 +
      #13 +
      '  /* Deletando os produtos que compôe os kits */'+ #13 +
      '  delete from ORCAMENTOS_ITENS'+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '  and ITEM_KIT_ID is not null;'+ #13 +
      #13 +
      '  if vValorCredito > 0 then'+ #13 +
      '    for xCreditos in cCreditos loop'+ #13 +
      '      CANCELAR_BAIXA_CONTAS_PAGAR(xCreditos.BAIXA_ID);'+ #13 +
      '    end loop;'+ #13 +
      #13 +
      '    delete from ORCAMENTOS_CREDITOS'+ #13 +
      '    where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  delete from ORCAMENTOS_BLOQUEIOS'+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      #13 +
      '  update ORCAMENTOS set'+ #13 +
      '    STATUS = ''OE'','+ #13 +
      '    VALOR_DINHEIRO = 0,'+ #13 +
      '    VALOR_CHEQUE = 0,'+ #13 +
      '    VALOR_CARTAO_DEBITO = 0,'+ #13 +
      '    VALOR_CARTAO_CREDITO = 0,'+ #13 +
      '    VALOR_COBRANCA = 0,'+ #13 +
      '    VALOR_ACUMULATIVO = 0,'+ #13 +
      '    VALOR_CREDITO = 0'+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      #13 +
      'end CANCELAR_FECHAMENTO_PEDIDO;'+ #13
    );
end;

function AjustarProcedureCANCELAR_RECEBIMENTO_ACUMULADO: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure CANCELAR_RECEBIMENTO_ACUMULADO(iACUMULADO_ID in number)'+ #13 +
      'is'+ #13 +
      '  vQtde number;'+ #13 +
      'begin'+ #13 +
      #13 +
      '  select'+ #13 +
      '    count(*)'+ #13 +
      '  into'+ #13 +
      '    vQtde'+ #13 +
      '  from'+ #13 +
      '    NOTAS_FISCAIS'+ #13 +
      '  where ACUMULADO_ID = iACUMULADO_ID'+ #13 +
      '  and STATUS = ''N'''+ #13 +
      '  and NUMERO_NOTA is not null;'+ #13 +
      #13 +
      '  if vQtde > 0 then'+ #13 +
      '    ERRO(''Este acumulado gerou um numero de nota fiscal e ainda não foi emitido, por favor verifique antes de continuar!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  delete from CONTAS_RECEBER'+ #13 +
      '  where ACUMULADO_ID = iACUMULADO_ID;'+ #13 +
      #13 +
      '  delete from CONTAS_PAGAR'+ #13 +
      '  where ACUMULADO_ID = iACUMULADO_ID;'+ #13 +
      #13 +
      '  delete from NOTAS_FISCAIS'+ #13 +
      '  where ACUMULADO_ID = iACUMULADO_ID'+ #13 +
      '  and STATUS not in(''E'', ''C'')'+ #13 +
      '  and NUMERO_NOTA is null;'+ #13 +
      #13 +
      '  update ACUMULADOS set'+ #13 +
      '    STATUS = ''AR'','+ #13 +
      '    TURNO_ID = null,'+ #13 +
      '    DATA_HORA_RECEBIMENTO = null,'+ #13 +
      '    USUARIO_RECEBIMENTO_ID = null'+ #13 +
      '  where ACUMULADO_ID = iACUMULADO_ID;'+ #13 +
      #13 +
      '  CANCELAR_FECHAMENTO_ACUMULADO(iACUMULADO_ID);  '+ #13 +
      #13 +
      'end CANCELAR_RECEBIMENTO_ACUMULADO;'+ #13
    );
end;

function AjustarConstraintCK_CONTAS_PAGAR_ORIGEM: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'alter table CONTAS_PAGAR'+ #13 +
      'drop constraint CK_CONTAS_PAGAR_ORIGEM;'+ #13 +
      #13 +
      'alter table CONTAS_PAGAR'+ #13 +
      'add constraint CK_CONTAS_PAGAR_ORIGEM'+ #13 +
      'check('+ #13 +
      '  (ORIGEM = ''COM'' and COMPRA_ID is not null)'+ #13 +
      '  or'+ #13 +
      '  (ORIGEM = ''ENT'' and ENTRADA_ID is not null)'+ #13 +
      '  or'+ #13 +
      '  (ORIGEM = ''ENS'' and ENTRADA_SERVICO_ID is not null)'+ #13 +
      '  or'+ #13 +
      '  (ORIGEM = ''CBX'' and COMPRA_BAIXA_ID is not null)'+ #13 +
      '  or'+ #13 +
      '  (ORIGEM = ''CON'' and CONHECIMENTO_FRETE_ID is not null)'+ #13 +
      '  or'+ #13 +
      '  (ORIGEM = ''BCP'' and BAIXA_ORIGEM_ID is not null)'+ #13 +
      '  or'+ #13 +
      '  (ORIGEM = ''BCR'' and BAIXA_RECEBER_ORIGEM_ID is not null)'+ #13 +
      '  or'+ #13 +
      '  (ORIGEM = ''DEV'' and DEVOLUCAO_ID is not null and INDICE_CONDICAO_PAGAMENTO is not null)'+ #13 +
      '  or'+ #13 +
      '  (ORIGEM = ''CVE'' and ORCAMENTO_ID is not null and INDICE_CONDICAO_PAGAMENTO is not null)'+ #13 +
      '  or'+ #13 +
      '  (ORIGEM = ''CAC'' and ACUMULADO_ID is not null and INDICE_CONDICAO_PAGAMENTO is not null)'+ #13 +
      '  or'+ #13 +
      '  (ORIGEM = ''CBR'' and BAIXA_RECEBER_ORIGEM_ID is not null and INDICE_CONDICAO_PAGAMENTO is not null)'+ #13 +
      '  or'+ #13 +
      '  (ORIGEM = ''ADA'' and BAIXA_RECEBER_ORIGEM_ID is not null and ORCAMENTO_ID is not null)'+ #13 +
      '  or'+ #13 +
      '  (ORIGEM = ''MAN'')'+ #13 +
      ');'+ #13
    );
end;

function AjustarProcedureGERAR_CREDITO_TROCO: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure GERAR_CREDITO_TROCO('+ #13 +
      '  iMOVIMENTO_ID in number,'+ #13 +
      '  iTIPO_MOVIMENTO in string,'+ #13 +
      '  iCLIENTE_ID in number,'+ #13 +
      '  iVALOR_TROCO in number'+ #13 +
      ')'+ #13 +
      'is'+ #13 +
      '  vClienteId   CONTAS_PAGAR.CADASTRO_ID%type;'+ #13 +
      '  vEmpresaId   CONTAS_PAGAR.EMPRESA_ID%type;'+ #13 +
      '  vPagarId     CONTAS_PAGAR.PAGAR_ID%type;'+ #13 +
      '  vCobrancaId  PARAMETROS.TIPO_COBRANCA_GERACAO_CRED_ID%type;'+ #13 +
      '  vIndice      CONTAS_PAGAR.INDICE_CONDICAO_PAGAMENTO%type;'+ #13 +
      'begin'+ #13 +
      #13 +
      '  if iTIPO_MOVIMENTO not in(''ORC'', ''ACU'', ''BXR'') then'+ #13 +
      '    ERRO(''O tipo de movimento só pode ser "ORC", "ACU" ou "BXR"!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if iTIPO_MOVIMENTO = ''ORC'' then'+ #13 +
      '    select'+ #13 +
      '      ORC.EMPRESA_ID,'+ #13 +
      '      ORC.CLIENTE_ID,'+ #13 +
      '      ORC.INDICE_CONDICAO_PAGAMENTO'+ #13 +
      '    into'+ #13 +
      '      vEmpresaId,'+ #13 +
      '      vClienteId,'+ #13 +
      '      vIndice'+ #13 +
      '    from'+ #13 +
      '      ORCAMENTOS ORC'+ #13 +
      '    where ORC.ORCAMENTO_ID = iMOVIMENTO_ID;'+ #13 +
      '  elsif iTIPO_MOVIMENTO = ''ACU'' then'+ #13 +
      '    select'+ #13 +
      '      ACU.EMPRESA_ID,'+ #13 +
      '      ACU.CLIENTE_ID,'+ #13 +
      '      max(ORC.INDICE_CONDICAO_PAGAMENTO)'+ #13 +
      '    into'+ #13 +
      '      vEmpresaId,'+ #13 +
      '      vClienteId,'+ #13 +
      '      vIndice'+ #13 +
      '    from'+ #13 +
      '      ACUMULADOS ACU'+ #13 +
      #13 +
      '    inner join ORCAMENTOS ORC'+ #13 +
      '    on ACU.ACUMULADO_ID = ORC.ACUMULADO_ID'+ #13 +
      #13 +
      '    where ACU.ACUMULADO_ID = iMOVIMENTO_ID'+ #13 +
      '    group by'+ #13 +
      '      ACU.EMPRESA_ID,'+ #13 +
      '      ACU.CLIENTE_ID;'+ #13 +
      '  else'+ #13 +
      '    select'+ #13 +
      '      EMPRESA_ID'+ #13 +
      '    into'+ #13 +
      '      vEmpresaId'+ #13 +
      '    from'+ #13 +
      '      CONTAS_RECEBER_BAIXAS'+ #13 +
      #13 +
      '    where BAIXA_ID = iMOVIMENTO_ID;'+ #13 +
      #13 +
      '    vIndice := 1;'+ #13 +
      '    vClienteId := iCLIENTE_ID;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if vClienteId = SESSAO.PARAMETROS_GERAIS.CADASTRO_CONSUMIDOR_FINAL_ID then'+ #13 +
      '    ERRO(''Não é permitido gerar crédito para consumidor final!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  select'+ #13 +
      '    TIPO_COBRANCA_GERACAO_CRED_ID'+ #13 +
      '  into'+ #13 +
      '    vCobrancaId'+ #13 +
      '  from'+ #13 +
      '    PARAMETROS;'+ #13 +
      #13 +
      '  if vCobrancaId is null then'+ #13 +
      '    ERRO(''O tipo de cobrança para geração do crédito não foi parametrizado, faça a parametrização nos "Parâmetros"!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  select SEQ_PAGAR_ID.nextval'+ #13 +
      '  into vPagarId'+ #13 +
      '  from dual;'+ #13 +
      #13 +
      '  insert into CONTAS_PAGAR('+ #13 +
      '    PAGAR_ID,'+ #13 +
      '    CADASTRO_ID,'+ #13 +
      '    EMPRESA_ID,'+ #13 +
      '    DOCUMENTO,'+ #13 +
      '    ORIGEM,'+ #13 +
      '    ORCAMENTO_ID,'+ #13 +
      '    ACUMULADO_ID,'+ #13 +
      '    BAIXA_RECEBER_ORIGEM_ID,'+ #13 +
      '    COBRANCA_ID,'+ #13 +
      '    PORTADOR_ID,'+ #13 +
      '    PLANO_FINANCEIRO_ID,'+ #13 +
      '    DATA_VENCIMENTO,'+ #13 +
      '    DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '    VALOR_DOCUMENTO,'+ #13 +
      '    STATUS,'+ #13 +
      '    PARCELA,'+ #13 +
      '    NUMERO_PARCELAS,'+ #13 +
      '    INDICE_CONDICAO_PAGAMENTO'+ #13 +
      '  )values('+ #13 +
      '    vPagarId,'+ #13 +
      '    vClienteId,'+ #13 +
      '    vEmpresaId,'+ #13 +
      '    decode(iTIPO_MOVIMENTO, ''ORC'', ''CVE'', ''ACU'', ''CAC'', ''BXR'', ''CBR'') || ''-'' || NFORMAT(iMOVIMENTO_ID, 0), -- Documento'+ #13 +
      '    decode(iTIPO_MOVIMENTO, ''ORC'', ''CVE'', ''ACU'', ''CAC'', ''BXR'', ''CBR''),'+ #13 +
      '    case when iTIPO_MOVIMENTO = ''ORC'' then iMOVIMENTO_ID else null end,'+ #13 +
      '    case when iTIPO_MOVIMENTO = ''ACU'' then iMOVIMENTO_ID else null end,'+ #13 +
      '    case when iTIPO_MOVIMENTO = ''BXR'' then iMOVIMENTO_ID else null end,'+ #13 +
      '    vCobrancaId,'+ #13 +
      '    ''9999'','+ #13 +
      '    ''1.002.001'','+ #13 +
      '    trunc(sysdate),'+ #13 +
      '    trunc(sysdate),'+ #13 +
      '    iVALOR_TROCO,'+ #13 +
      '    ''A'','+ #13 +
      '    1,'+ #13 +
      '    1,'+ #13 +
      '    vIndice'+ #13 +
      '  );'+ #13 +
      #13 +
      'end GERAR_CREDITO_TROCO;'+ #13
    );
end;

function AjustarProcedureCONSOLIDAR_BAIXA_CONTAS_REC2: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure CONSOLIDAR_BAIXA_CONTAS_REC('+ #13 +
      '  iBAIXA_RECEBER_ID in number,'+ #13 +
      '  iCLIENTE_ID in number,'+ #13 +
      '  iVALOR_TROCO in number'+ #13 +
      ')'+ #13 +
      'is'+ #13 +
      #13 +
      '  vReceberId     CONTAS_RECEBER.RECEBER_ID%type;'+ #13 +
      '  vCadastroId    CONTAS_RECEBER_BAIXAS.BAIXA_ID%type;'+ #13 +
      '  vEmpresaId     CONTAS_RECEBER_BAIXAS.EMPRESA_ID%type;'+ #13 +
      '  vValorRetencao CONTAS_RECEBER_BAIXAS.VALOR_RETENCAO%type;'+ #13 +
      #13 +
      '  vValorBaixarCredito   number default 0;'+ #13 +
      '  vValorRestante        number default 0;'+ #13 +
      #13 +
      '  vBaixaCreditoPagarId  CONTAS_PAGAR_BAIXAS.BAIXA_ID%type;'+ #13 +
      '  vValorDinheiro        CONTAS_RECEBER_BAIXAS.VALOR_DINHEIRO%type;'+ #13 +
      '  vValorCheque          CONTAS_RECEBER_BAIXAS.VALOR_CHEQUE%type;'+ #13 +
      '  vValorCartaoDeb       CONTAS_RECEBER_BAIXAS.VALOR_CARTAO_DEBITO%type;'+ #13 +
      '  vValorCartaoCred      CONTAS_RECEBER_BAIXAS.VALOR_CARTAO_CREDITO%type;'+ #13 +
      '  vValorCobranca        CONTAS_RECEBER_BAIXAS.VALOR_COBRANCA%type;'+ #13 +
      '  vValorCredito         CONTAS_RECEBER_BAIXAS.VALOR_CREDITO%type;'+ #13 +
      '  vValorTotal           CONTAS_RECEBER_BAIXAS.VALOR_LIQUIDO%type;'+ #13 +
      #13 +
      '  cursor cCheques is'+ #13 +
      '  select'+ #13 +
      '    CHQ.COBRANCA_ID,'+ #13 +
      '    CHQ.DATA_VENCIMENTO,'+ #13 +
      '    CHQ.PARCELA,'+ #13 +
      '    CHQ.NUMERO_PARCELAS,'+ #13 +
      '    CHQ.BANCO,'+ #13 +
      '    CHQ.AGENCIA,'+ #13 +
      '    CHQ.CONTA_CORRENTE,'+ #13 +
      '    CHQ.NOME_EMITENTE,'+ #13 +
      '    CHQ.CPF_CNPJ_EMITENTE,'+ #13 +
      '    CHQ.TELEFONE_EMITENTE,'+ #13 +
      '    CHQ.VALOR_CHEQUE,'+ #13 +
      '    CHQ.NUMERO_CHEQUE,'+ #13 +
      '    CHQ.ITEM_ID'+ #13 +
      '  from'+ #13 +
      '    CONTAS_REC_BAIXAS_PAGTOS_CHQ CHQ'+ #13 +
      '  where CHQ.BAIXA_ID = iBAIXA_RECEBER_ID;'+ #13 +
      #13 +
      #13 +
      '  cursor cCobrancas(pTIPO in string) is'+ #13 +
      '  select'+ #13 +
      '    PAG.COBRANCA_ID,'+ #13 +
      '    PAG.ITEM_ID,'+ #13 +
      '    PAG.VALOR,'+ #13 +
      '    PAG.DATA_VENCIMENTO,'+ #13 +
      '    PAG.PARCELA,'+ #13 +
      '    PAG.NUMERO_PARCELAS,'+ #13 +
      '    TIP.BOLETO_BANCARIO,'+ #13 +
      '    PAG.NSU_TEF,'+ #13 +
      '    TIP.PORTADOR_ID'+ #13 +
      '  from'+ #13 +
      '    CONTAS_REC_BAIXAS_PAGAMENTOS PAG'+ #13 +
      #13 +
      '  join TIPOS_COBRANCA TIP'+ #13 +
      '  on PAG.COBRANCA_ID = TIP.COBRANCA_ID'+ #13 +
      '  and TIP.FORMA_PAGAMENTO = pTIPO'+ #13 +
      #13 +
      '  where PAG.BAIXA_ID = iBAIXA_RECEBER_ID'+ #13 +
      '  order by'+ #13 +
      '		COBRANCA_ID,'+ #13 +
      '		PARCELA,'+ #13 +
      '		DATA_VENCIMENTO,'+ #13 +
      '		ITEM_ID;'+ #13 +
      #13 +
      #13 +
      '  cursor cCartoes is'+ #13 +
      '  select'+ #13 +
      '    ITEM_ID,'+ #13 +
      '    NSU_TEF,'+ #13 +
      '    NUMERO_CARTAO,'+ #13 +
      '    CODIGO_AUTORIZACAO'+ #13 +
      '  from'+ #13 +
      '    CONTAS_REC_BAIXAS_PAGAMENTOS'+ #13 +
      '  where BAIXA_ID = iBAIXA_RECEBER_ID'+ #13 +
      '  and TIPO = ''CR'';'+ #13 +
      #13 +
      '  cursor cCreditos is'+ #13 +
      '  select'+ #13 +
      '    BAI.PAGAR_ID,'+ #13 +
      '    PAG.VALOR_DOCUMENTO - PAG.VALOR_DESCONTO - PAG.VALOR_ADIANTADO as VALOR_LIQUIDO'+ #13 +
      '  from'+ #13 +
      '    CONTAS_REC_BAIXAS_CREDITOS BAI'+ #13 +
      #13 +
      '  inner join CONTAS_PAGAR PAG'+ #13 +
      '  on BAI.PAGAR_ID = PAG.PAGAR_ID'+ #13 +
      #13 +
      '  where BAI.BAIXA_ID = iBAIXA_RECEBER_ID;'+ #13 +
      #13 +
      'begin'+ #13 +
      #13 +
      '  select'+ #13 +
      '    EMPRESA_ID,'+ #13 +
      '    CADASTRO_ID,'+ #13 +
      '    VALOR_DINHEIRO,'+ #13 +
      '    VALOR_CHEQUE,'+ #13 +
      '    VALOR_CARTAO_DEBITO,'+ #13 +
      '    VALOR_CARTAO_CREDITO,'+ #13 +
      '    VALOR_COBRANCA,'+ #13 +
      '    VALOR_CREDITO,'+ #13 +
      '    VALOR_LIQUIDO,'+ #13 +
      '    VALOR_RETENCAO'+ #13 +
      '  into'+ #13 +
      '    vEmpresaId,'+ #13 +
      '    vCadastroId,'+ #13 +
      '    vValorDinheiro,'+ #13 +
      '    vValorCheque,'+ #13 +
      '    vValorCartaoDeb,'+ #13 +
      '    vValorCartaoCred,'+ #13 +
      '    vValorCobranca,'+ #13 +
      '    vValorCredito,'+ #13 +
      '    vValorTotal,'+ #13 +
      '    vValorRetencao'+ #13 +
      '  from'+ #13 +
      '    CONTAS_RECEBER_BAIXAS'+ #13 +
      '  where BAIXA_ID = iBAIXA_RECEBER_ID;'+ #13 +
      #13 +
      '  for xCheques in cCheques loop'+ #13 +
      '    select SEQ_RECEBER_ID.nextval'+ #13 +
      '    into vReceberId'+ #13 +
      '    from dual;'+ #13 +
      #13 +
      '    insert into CONTAS_RECEBER('+ #13 +
      '      RECEBER_ID,'+ #13 +
      '      CADASTRO_ID,'+ #13 +
      '      EMPRESA_ID,'+ #13 +
      '      COBRANCA_ID,'+ #13 +
      '      PORTADOR_ID,'+ #13 +
      '      PLANO_FINANCEIRO_ID,'+ #13 +
      '      DOCUMENTO,'+ #13 +
      '      BANCO,'+ #13 +
      '      AGENCIA,'+ #13 +
      '      CONTA_CORRENTE,'+ #13 +
      '      NUMERO_CHEQUE,'+ #13 +
      '      NOME_EMITENTE,'+ #13 +
      '      CPF_CNPJ_EMITENTE,'+ #13 +
      '      TELEFONE_EMITENTE,'+ #13 +
      '      DATA_EMISSAO,'+ #13 +
      '      DATA_VENCIMENTO,'+ #13 +
      '      DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '      VALOR_DOCUMENTO,'+ #13 +
      '      STATUS,'+ #13 +
      '      PARCELA,'+ #13 +
      '      NUMERO_PARCELAS,'+ #13 +
      '      ORIGEM,'+ #13 +
      '      BAIXA_ORIGEM_ID'+ #13 +
      '    )values('+ #13 +
      '      vReceberId,'+ #13 +
      '      vCadastroId,'+ #13 +
      '      vEmpresaId,'+ #13 +
      '      xCheques.COBRANCA_ID,'+ #13 +
      '      ''9998'','+ #13 +
      '      ''1.001.002'','+ #13 +
      '      ''BXR-'' || iBAIXA_RECEBER_ID || xCheques.ITEM_ID || ''/CHQ'','+ #13 +
      '      xCheques.BANCO,'+ #13 +
      '      xCheques.AGENCIA,'+ #13 +
      '      xCheques.CONTA_CORRENTE,'+ #13 +
      '      xCheques.NUMERO_CHEQUE,'+ #13 +
      '      xCheques.NOME_EMITENTE,'+ #13 +
      '      xCheques.CPF_CNPJ_EMITENTE,'+ #13 +
      '      xCheques.TELEFONE_EMITENTE,'+ #13 +
      '      trunc(sysdate),'+ #13 +
      '      xCheques.DATA_VENCIMENTO,'+ #13 +
      '      xCheques.DATA_VENCIMENTO,'+ #13 +
      '      xCheques.VALOR_CHEQUE,'+ #13 +
      '      ''A'','+ #13 +
      '      xCheques.PARCELA,'+ #13 +
      '      xCheques.NUMERO_PARCELAS,'+ #13 +
      '      ''BXR'','+ #13 +
      '      iBAIXA_RECEBER_ID'+ #13 +
      '    );'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  for xCobrancas in cCobrancas(''COB'') loop'+ #13 +
      '    select SEQ_RECEBER_ID.nextval'+ #13 +
      '    into vReceberId'+ #13 +
      '    from dual;'+ #13 +
      #13 +
      '    insert into CONTAS_RECEBER('+ #13 +
      '      RECEBER_ID,'+ #13 +
      '      CADASTRO_ID,'+ #13 +
      '      EMPRESA_ID,'+ #13 +
      '      COBRANCA_ID,'+ #13 +
      '      PORTADOR_ID,'+ #13 +
      '      PLANO_FINANCEIRO_ID,'+ #13 +
      '      DOCUMENTO,'+ #13 +
      '      DATA_EMISSAO,'+ #13 +
      '      DATA_VENCIMENTO,'+ #13 +
      '      DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '      VALOR_DOCUMENTO,'+ #13 +
      '      STATUS,'+ #13 +
      '      PARCELA,'+ #13 +
      '      NUMERO_PARCELAS,'+ #13 +
      '      ORIGEM,'+ #13 +
      '      BAIXA_ORIGEM_ID'+ #13 +
      '    )values('+ #13 +
      '      vReceberId,'+ #13 +
      '      vCadastroId,'+ #13 +
      '      vEmpresaId,'+ #13 +
      '      xCobrancas.COBRANCA_ID,'+ #13 +
      '      xCobrancas.PORTADOR_ID,'+ #13 +
      '      case when xCobrancas.BOLETO_BANCARIO = ''N'' then ''1.001.005'' else ''1.001.006'' end,'+ #13 +
      '      ''BXR-'' || iBAIXA_RECEBER_ID || xCobrancas.ITEM_ID || case when xCobrancas.BOLETO_BANCARIO = ''S'' then ''/BOL'' else ''/COB'' end,'+ #13 +
      '      trunc(sysdate),'+ #13 +
      '      xCobrancas.DATA_VENCIMENTO,'+ #13 +
      '      xCobrancas.DATA_VENCIMENTO,'+ #13 +
      '      xCobrancas.VALOR,'+ #13 +
      '      ''A'','+ #13 +
      '      xCobrancas.PARCELA,'+ #13 +
      '      xCobrancas.NUMERO_PARCELAS,'+ #13 +
      '      ''BXR'','+ #13 +
      '      iBAIXA_RECEBER_ID'+ #13 +
      '    );'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  for xCartoes in cCartoes loop'+ #13 +
      '    GERAR_CARTOES_RECEBER_BAIXA(iBAIXA_RECEBER_ID, xCartoes.ITEM_ID, xCartoes.NSU_TEF, xCartoes.CODIGO_AUTORIZACAO, xCartoes.NUMERO_CARTAO);'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  ATUALIZAR_RETENCOES_CONTAS_REC(iBAIXA_RECEBER_ID, ''BXR'');'+ #13 +
      #13 +
      '  if vValorCredito > 0 then'+ #13 +
      '    vValorRestante := vValorTotal - (vValorDinheiro + vValorCheque + vValorCartaoDeb + vValorCartaoCred + vValorCobranca);'+ #13 +
      '    for xCreditos in cCreditos loop'+ #13 +
      '      if xCreditos.VALOR_LIQUIDO >= vValorRestante then'+ #13 +
      '        vValorBaixarCredito := vValorRestante;'+ #13 +
      '        vValorRestante := 0;'+ #13 +
      '      else'+ #13 +
      '        vValorBaixarCredito := xCreditos.VALOR_LIQUIDO;'+ #13 +
      '        vValorRestante := vValorRestante - vValorBaixarCredito;'+ #13 +
      '      end if;'+ #13 +
      #13 +
      '      BAIXAR_CREDITO_PAGAR(xCreditos.PAGAR_ID, iBAIXA_RECEBER_ID, ''BCR'', vValorBaixarCredito, vBaixaCreditoPagarId, vBaixaCreditoPagarId);'+ #13 +
      #13 +
      '      if vValorRestante = 0 then'+ #13 +
      '        exit;'+ #13 +
      '      end if;'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if iVALOR_TROCO > 0 then'+ #13 +
      '    GERAR_CREDITO_TROCO(iBAIXA_RECEBER_ID, ''BXR'', iCLIENTE_ID, iVALOR_TROCO);'+ #13 +
      '  end if;'+ #13 +
      #13 +
      'end CONSOLIDAR_BAIXA_CONTAS_REC;'+ #13
    );
end;

function AjustarProcedureRECEBER_CONTAS_RECEBER_BAIXA: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure RECEBER_CONTAS_RECEBER_BAIXA('+ #13 +
      '  iBAIXA_ID in number,'+ #13 +
      '  iTURNO_ID in number,'+ #13 +
      '  iCLIENTE_ID in number,'+ #13 +
      '  iVALOR_TROCO in number'+ #13 +
      ')'+ #13 +
      'is'+ #13 +
      'begin'+ #13 +
      #13 +
      '	update CONTAS_RECEBER_BAIXAS set'+ #13 +
      '	  DATA_PAGAMENTO = trunc(sysdate),'+ #13 +
      '		RECEBIDO = ''S'','+ #13 +
      '		TURNO_ID = iTURNO_ID'+ #13 +
      '	where BAIXA_ID = iBAIXA_ID;'+ #13 +
      #13 +
      '  CONSOLIDAR_BAIXA_CONTAS_REC(iBAIXA_ID, iCLIENTE_ID, iVALOR_TROCO);'+ #13 +
      #13 +
      'end RECEBER_CONTAS_RECEBER_BAIXA;'+ #13
    );
end;

function AjustarProcedureGERAR_PENDENCIA_ENTREGAS: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure GERAR_PENDENCIA_ENTREGAS('+ #13 +
      '  iORCAMENTO_ID       in number,'+ #13 +
      '  iEMPRESA_GERACAO_ID in number'+ #13 +
      ')'+ #13 +
      'is    '+ #13 +
      '  vQtdeAgendar             number;'+ #13 +
      '  vQtdeFaltaAgendar        number;'+ #13 +
      '  vQtdeFaltaAgendarLote    number;'+ #13 +
      #13 +
      '  vQtdeAgendarLoteItemKit  number;'+ #13 +
      '  vQtdeFaltaAgendarItemKit number;'+ #13 +
      '  vQtdeAgendarKitEmpresa   number;'+ #13 +
      #13 +
      '  cursor cItens is'+ #13 +
      '  select'+ #13 +
      '    ITE.PRODUTO_ID,'+ #13 +
      '    ITE.ITEM_ID,'+ #13 +
      '    ITE.QUANTIDADE,    '+ #13 +
      '    PRO.ACEITAR_ESTOQUE_NEGATIVO,'+ #13 +
      '    ITE.PREVISAO_ENTREGA,'+ #13 +
      '    OIT.TIPO_CONTROLE_ESTOQUE'+ #13 +
      '  from'+ #13 +
      '    ENTREGAS_ITENS_A_GERAR_TEMP ITE    '+ #13 +
      #13 +
      '  inner join PRODUTOS PRO'+ #13 +
      '  on ITE.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
      #13 +
      '  inner join ORCAMENTOS_ITENS OIT'+ #13 +
      '  on ITE.ORCAMENTO_ID = OIT.ORCAMENTO_ID'+ #13 +
      '  and ITE.PRODUTO_ID = OIT.PRODUTO_ID'+ #13 +
      '  and ITE.ITEM_ID = OIT.ITEM_ID'+ #13 +
      '  and OIT.ITEM_KIT_ID is null'+ #13 +
      #13 +
      '  where ITE.ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '  and ITE.TIPO_ENTREGA = ''EN'''+ #13 +
      #13 +
      '  order by'+ #13 +
      '    ITE.TIPO_ENTREGA;  '+ #13 +
      #13 +
      '  cursor cEstoques(pPRODUTO_ID in number) is'+ #13 +
      '  select'+ #13 +
      '    GES.EMPRESA_GRUPO_ID as EMPRESA_ID,'+ #13 +
      '    DIV.PRODUTO_ID,'+ #13 +
      '    DIV.DISPONIVEL,    '+ #13 +
      '    DIV.LOTE,'+ #13 +
      '    DIV.LOCAL_ID'+ #13 +
      '  from'+ #13 +
      '    VW_ESTOQUES_DIVISAO DIV'+ #13 +
      #13 +
      '  inner join GRUPOS_ESTOQUES GES'+ #13 +
      '  on DIV.EMPRESA_ID = GES.EMPRESA_GRUPO_ID'+ #13 +
      '  and GES.TIPO = ''E'''+ #13 +
      '  and GES.EMPRESA_ID = iEMPRESA_GERACAO_ID'+ #13 +
      #13 +
      '  inner join PRODUTOS_ORDENS_LOC_ENTREGAS POL'+ #13 +
      '  on DIV.EMPRESA_ID = POL.EMPRESA_ID'+ #13 +
      '  and DIV.PRODUTO_PAI_ID = POL.PRODUTO_ID'+ #13 +
      '  and DIV.LOCAL_ID = POL.LOCAL_ID'+ #13 +
      '  and POL.TIPO = ''E'''+ #13 +
      #13 +
      '  where DIV.PRODUTO_ID = pPRODUTO_ID'+ #13 +
      '  and case when DIV.ACEITAR_ESTOQUE_NEGATIVO = ''S'' then 1 else DIV.DISPONIVEL end > 0'+ #13 +
      '  order by'+ #13 +
      '    GES.ORDEM,'+ #13 +
      '    POL.ORDEM;'+ #13 +
      #13 +
      #13 +
      '  cursor cEmpresasEstoqueKit is'+ #13 +
      '  select'+ #13 +
      '    EMPRESA_GRUPO_ID as EMPRESA_ID'+ #13 +
      '  from'+ #13 +
      '    GRUPOS_ESTOQUES'+ #13 +
      '  where TIPO = ''E'''+ #13 +
      '  and EMPRESA_ID = iEMPRESA_GERACAO_ID'+ #13 +
      '  order by'+ #13 +
      '    ORDEM;    '+ #13 +
      #13 +
      #13 +
      '  cursor cItensKit(pPRODUTO_KIT_ID in number, pITEM_KIT_ID in number, pQUANTIDADE_KITS in number) is'+ #13 +
      '  select'+ #13 +
      '    KIT.PRODUTO_ID,'+ #13 +
      '    ITE.ITEM_ID,'+ #13 +
      '    KIT.QUANTIDADE * pQUANTIDADE_KITS as QUANTIDADE,'+ #13 +
      '    PRO.ACEITAR_ESTOQUE_NEGATIVO'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS_ITENS ITE'+ #13 +
      #13 +
      '  inner join PRODUTOS_KIT KIT'+ #13 +
      '  on ITE.PRODUTO_ID = KIT.PRODUTO_ID'+ #13 +
      '  and KIT.PRODUTO_KIT_ID = pPRODUTO_KIT_ID'+ #13 +
      #13 +
      '  inner join PRODUTOS PRO'+ #13 +
      '  on KIT.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
      #13 +
      '  where ITE.ITEM_KIT_ID = pITEM_KIT_ID'+ #13 +
      '  and ORCAMENTO_ID = iORCAMENTO_ID;    '+ #13 +
      #13 +
      #13 +
      '  cursor cEstoqueItemKit(pEMPRESA_ID in number, pPRODUTO_ID in number, pLOCAL_ID in number) is'+ #13 +
      '  select'+ #13 +
      '    DIV.LOCAL_ID,'+ #13 +
      '    DIV.DISPONIVEL,'+ #13 +
      '    DIV.LOTE'+ #13 +
      '  from'+ #13 +
      '    VW_ESTOQUES_DIVISAO DIV'+ #13 +
      #13 +
      '  where DIV.PRODUTO_ID = pPRODUTO_ID'+ #13 +
      '  and DIV.EMPRESA_ID = pEMPRESA_ID'+ #13 +
      '  and DIV.LOCAL_ID = pLOCAL_ID'+ #13 +
      #13 +
      '  order by'+ #13 +
      '    DIV.DISPONIVEL desc;'+ #13 +
      #13 +
      #13 +
      '  cursor cItensDefLotes( pITEM_ID in number ) is'+ #13 +
      '  select'+ #13 +
      '    ITE.PRODUTO_ID,'+ #13 +
      '    OIL.LOTE,'+ #13 +
      '    OIL.QUANTIDADE_ENTREGAR as QUANTIDADE_AGENDAR'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS_ITENS_DEF_LOTES OIL'+ #13 +
      #13 +
      '  inner join ORCAMENTOS_ITENS ITE'+ #13 +
      '  on OIL.ORCAMENTO_ID = ITE.ORCAMENTO_ID'+ #13 +
      '  and OIL.ITEM_ID = ITE.ITEM_ID'+ #13 +
      #13 +
      '  where OIL.ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '  and OIL.ITEM_ID = pITEM_ID;'+ #13 +
      #13 +
      #13 +
      '  cursor cItensDefLotesEstoques( pPRODUTO_ID in number, pLOTE in string ) is'+ #13 +
      '  select'+ #13 +
      '    DIV.EMPRESA_ID,'+ #13 +
      '    DIV.LOCAL_ID,'+ #13 +
      '    DIV.DISPONIVEL'+ #13 +
      '  from'+ #13 +
      '    VW_ESTOQUES_DIVISAO DIV'+ #13 +
      #13 +
      '  inner join GRUPOS_ESTOQUES GRU'+ #13 +
      '  on GRU.TIPO = ''E'''+ #13 +
      '  and GRU.EMPRESA_GRUPO_ID = iEMPRESA_GERACAO_ID'+ #13 +
      '  and DIV.EMPRESA_ID = GRU.EMPRESA_ID'+ #13 +
      #13 +
      '  inner join PRODUTOS_ORDENS_LOC_ENTREGAS POL'+ #13 +
      '  on DIV.EMPRESA_ID = POL.EMPRESA_ID'+ #13 +
      '  and DIV.PRODUTO_ID = POL.PRODUTO_ID'+ #13 +
      '  and DIV.LOCAL_ID = POL.LOCAL_ID'+ #13 +
      '  and POL.TIPO = ''E'''+ #13 +
      #13 +
      '  where DIV.PRODUTO_ID = pPRODUTO_ID'+ #13 +
      '  and DIV.LOTE = pLOTE'+ #13 +
      '  order by'+ #13 +
      '    case when DIV.DISPONIVEL <= 0 then 1 else 0 end,'+ #13 +
      '    POL.ORDEM;'+ #13 +
      #13 +
      '  cursor cOrdemLocaisProdutoKit( pPRODUTO_KIT_ID in number, pEMPRESA_ESTOQUE_ID in number )'+ #13 +
      '  is'+ #13 +
      '  select'+ #13 +
      '    LOCAL_ID'+ #13 +
      '  from'+ #13 +
      '    PRODUTOS_ORDENS_LOC_ENTREGAS'+ #13 +
      '  where PRODUTO_ID = pPRODUTO_KIT_ID'+ #13 +
      '  and EMPRESA_ID = pEMPRESA_ESTOQUE_ID'+ #13 +
      '  and TIPO = ''E'''+ #13 +
      '  order by'+ #13 +
      '    ORDEM;'+ #13 +
      #13 +
      'begin'+ #13 +
      #13 +
      '  for xItens in cItens loop'+ #13 +
      '     vQtdeFaltaAgendar := xItens.QUANTIDADE;'+ #13 +
      '    /* Verificando se o usuário já selecionou os lotes */'+ #13 +
      '    /* Se sim apenas agendando e passando pra frente */'+ #13 +
      '    if xItens.TIPO_CONTROLE_ESTOQUE in(''L'', ''G'', ''P'') then'+ #13 +
      '      for xItensDefLotes in cItensDefLotes( xItens.ITEM_ID ) loop'+ #13 +
      '        vQtdeFaltaAgendarLote := xItensDefLotes.QUANTIDADE_AGENDAR;'+ #13 +
      #13 +
      '        for xItensDefLoteEstoq in cItensDefLotesEstoques( xItens.PRODUTO_ID, xItensDefLotes.LOTE ) loop'+ #13 +
      '          VERIFICAR_PENDENCIA_ENTREGA(xItensDefLoteEstoq.EMPRESA_ID, xItensDefLoteEstoq.LOCAL_ID, iORCAMENTO_ID, xItens.PREVISAO_ENTREGA, ''NHR'');'+ #13 +
      #13 +
      '          if xItensDefLoteEstoq.DISPONIVEL >= vQtdeFaltaAgendarLote then'+ #13 +
      '            vQtdeAgendar := vQtdeFaltaAgendarLote;'+ #13 +
      '            vQtdeFaltaAgendarLote := 0;'+ #13 +
      '          else'+ #13 +
      '            vQtdeAgendar := xItensDefLoteEstoq.DISPONIVEL;'+ #13 +
      '            vQtdeFaltaAgendarLote := vQtdeFaltaAgendarLote - vQtdeAgendar;'+ #13 +
      '          end if;'+ #13 +
      #13 +
      '          begin'+ #13 +
      '            INSERIR_ENTREGAS_ITENS_PEND('+ #13 +
      '              xItensDefLoteEstoq.EMPRESA_ID,'+ #13 +
      '              xItensDefLoteEstoq.LOCAL_ID,'+ #13 +
      '              iORCAMENTO_ID,'+ #13 +
      '              xItens.PREVISAO_ENTREGA,'+ #13 +
      '              xItens.PRODUTO_ID,'+ #13 +
      '              xItens.ITEM_ID,'+ #13 +
      '              xItensDefLotes.LOTE,'+ #13 +
      '              vQtdeAgendar'+ #13 +
      '            );'+ #13 +
      '          exception'+ #13 +
      '            when others then'+ #13 +
      '              ERRO(vQtdeAgendar || ''  '' || sqlerrm);'+ #13 +
      '          end;'+ #13 +
      #13 +
      '          vQtdeFaltaAgendar := vQtdeFaltaAgendar - vQtdeAgendar;'+ #13 +
      #13 +
      '          if vQtdeFaltaAgendarLote = 0 then'+ #13 +
      '            exit;'+ #13 +
      '          end if;'+ #13 +
      '        end loop;'+ #13 +
      #13 +
      '        if vQtdeFaltaAgendarLote > 0 then'+ #13 +
      '          ERRO(''Estoque insuficiente para o produto '' || xItens.PRODUTO_ID || '', lote '' || xItensDefLotes.LOTE  || ''!'');'+ #13 +
      '        end if;'+ #13 +
      '      end loop;'+ #13 +
      #13 +
      '      if vQtdeFaltaAgendar > 0 then'+ #13 +
      '        ERRO(''Estoque insuficiente para o produto '' || xItens.PRODUTO_ID || ''!'');'+ #13 +
      '      end if;'+ #13 +
      '    /* Se for produto com controle de estoque por kit agrupado ou desmembrado */'+ #13 +
      '    elsif xItens.TIPO_CONTROLE_ESTOQUE in(''K'', ''A'') then'+ #13 +
      #13 +
      '      vQtdeFaltaAgendar := xItens.QUANTIDADE;'+ #13 +
      #13 +
      '      if xItens.TIPO_CONTROLE_ESTOQUE = ''K'' then'+ #13 +
      '        for xItensCompoeKit in cItensKit(xItens.PRODUTO_ID, xItens.ITEM_ID, xItens.QUANTIDADE) loop'+ #13 +
      #13 +
      '          vQtdeFaltaAgendarItemKit := xItensCompoeKit.QUANTIDADE;'+ #13 +
      #13 +
      '          for xEstoques in cEstoques(xItensCompoeKit.PRODUTO_ID) loop'+ #13 +
      '            if xItensCompoeKit.ACEITAR_ESTOQUE_NEGATIVO = ''S'' then'+ #13 +
      '              vQtdeAgendar := vQtdeFaltaAgendarItemKit;'+ #13 +
      '              vQtdeFaltaAgendarItemKit := 0;'+ #13 +
      '            elsif xEstoques.DISPONIVEL > 0 then'+ #13 +
      '              if xEstoques.DISPONIVEL >= vQtdeFaltaAgendar  then'+ #13 +
      '                vQtdeAgendar := vQtdeFaltaAgendarItemKit;'+ #13 +
      '                vQtdeFaltaAgendarItemKit := 0;'+ #13 +
      '              else'+ #13 +
      '                vQtdeAgendar := xEstoques.DISPONIVEL;'+ #13 +
      '                vQtdeFaltaAgendarItemKit := vQtdeFaltaAgendarItemKit - vQtdeAgendar;'+ #13 +
      '              end if;'+ #13 +
      '            else'+ #13 +
      '              /* Se não aceita estoque negativo, passando para o próximo local. */'+ #13 +
      '              continue;'+ #13 +
      '            end if;'+ #13 +
      #13 +
      '            VERIFICAR_PENDENCIA_ENTREGA(xEstoques.EMPRESA_ID, xEstoques.LOCAL_ID, iORCAMENTO_ID, xItens.PREVISAO_ENTREGA, ''NHR'');'+ #13 +
      #13 +
      '            INSERIR_ENTREGAS_ITENS_PEND('+ #13 +
      '              xEstoques.EMPRESA_ID,'+ #13 +
      '              xEstoques.LOCAL_ID,'+ #13 +
      '              iORCAMENTO_ID,'+ #13 +
      '              xItens.PREVISAO_ENTREGA,'+ #13 +
      '              xItensCompoeKit.PRODUTO_ID,'+ #13 +
      '              xItensCompoeKit.ITEM_ID,'+ #13 +
      '              xEstoques.LOTE,'+ #13 +
      '              vQtdeAgendar'+ #13 +
      '            );'+ #13 +
      #13 +
      '            if vQtdeFaltaAgendarItemKit = 0 then'+ #13 +
      '              vQtdeFaltaAgendar := 0;'+ #13 +
      '              exit;'+ #13 +
      '            end if;'+ #13 +
      '          end loop;'+ #13 +
      #13 +
      '          if vQtdeFaltaAgendarItemKit > 0 then'+ #13 +
      '            ERRO('+ #13 +
      '              ''Estoque insuficiente para o produto '' || xItensCompoeKit.PRODUTO_ID || '' que compõe o kit '' ||  xItens.PRODUTO_ID || ''!'' || chr(13) ||'+ #13 +
      '              ''Faltam: '' || NFORMAT(vQtdeFaltaAgendarItemKit, 0)'+ #13 +
      '            );'+ #13 +
      '          end if;'+ #13 +
      #13 +
      '        end loop;'+ #13 +
      #13 +
      '      /* Se kit entrega agrupada, detalhe é que neste todos os itens devem estar no mesmo local */'+ #13 +
      '      else'+ #13 +
      '        for xEmpresasEstoqueKit in cEmpresasEstoqueKit loop'+ #13 +
      '          vQtdeAgendarKitEmpresa := QUANTIDADE_KITS_DISP_EMPRESA(xEmpresasEstoqueKit.EMPRESA_ID, xItens.PRODUTO_ID);'+ #13 +
      #13 +
      '          -- Se não pode agendar nada, passa pra frente'+ #13 +
      '          if xItens.ACEITAR_ESTOQUE_NEGATIVO = ''S'' then'+ #13 +
      '            vQtdeAgendarKitEmpresa := xItens.QUANTIDADE;'+ #13 +
      '          elsif vQtdeAgendarKitEmpresa = 0 then'+ #13 +
      '            continue;'+ #13 +
      '          end if;'+ #13 +
      #13 +
      '          if vQtdeAgendarKitEmpresa >= vQtdeFaltaAgendar then'+ #13 +
      '            vQtdeAgendarKitEmpresa := vQtdeFaltaAgendar;'+ #13 +
      '          end if;'+ #13 +
      #13 +
      '          for xOrdemLocaisProdutoKit in cOrdemLocaisProdutoKit(xItens.PRODUTO_ID, xEmpresasEstoqueKit.EMPRESA_ID) loop'+ #13 +
      #13 +
      '            if xItens.ACEITAR_ESTOQUE_NEGATIVO = ''S'' then'+ #13 +
      '              vQtdeAgendar := vQtdeAgendarKitEmpresa;'+ #13 +
      '            else'+ #13 +
      '              vQtdeAgendar := QUANTIDADE_KITS_DISP_LOCAL(xEmpresasEstoqueKit.EMPRESA_ID, xOrdemLocaisProdutoKit.LOCAL_ID, xItens.PRODUTO_ID);'+ #13 +
      '              if vQtdeAgendar >= vQtdeAgendarKitEmpresa then'+ #13 +
      '                vQtdeAgendar := vQtdeAgendarKitEmpresa;'+ #13 +
      '              end if;'+ #13 +
      '            end if;'+ #13 +
      #13 +
      '            /* Se não pode agendar no local passando para o próximo */'+ #13 +
      '            if vQtdeAgendar = 0 then'+ #13 +
      '              continue;'+ #13 +
      '            end if;'+ #13 +
      #13 +
      '            /* Verificando o cabeçalho da pendência */'+ #13 +
      '            VERIFICAR_PENDENCIA_ENTREGA(xEmpresasEstoqueKit.EMPRESA_ID, xOrdemLocaisProdutoKit.LOCAL_ID, iORCAMENTO_ID, xItens.PREVISAO_ENTREGA, ''NHR'');'+ #13 +
      #13 +
      '            for xItensKit in cItensKit(xItens.PRODUTO_ID, xItens.ITEM_ID, vQtdeAgendar) loop'+ #13 +
      #13 +
      '              vQtdeFaltaAgendarItemKit := xItens.QUANTIDADE;'+ #13 +
      #13 +
      '              for xEstoqueItemKit in cEstoqueItemKit(xEmpresasEstoqueKit.EMPRESA_ID, xItensKit.PRODUTO_ID, xOrdemLocaisProdutoKit.LOCAL_ID) loop'+ #13 +
      #13 +
      '                if xEstoqueItemKit.DISPONIVEL >= vQtdeFaltaAgendarItemKit then'+ #13 +
      '                  vQtdeAgendarLoteItemKit := vQtdeFaltaAgendarItemKit;'+ #13 +
      '                  vQtdeFaltaAgendarItemKit := 0;'+ #13 +
      '                else'+ #13 +
      '                  vQtdeAgendarLoteItemKit := xEstoqueItemKit.DISPONIVEL;'+ #13 +
      '                  vQtdeFaltaAgendarItemKit := vQtdeFaltaAgendarItemKit - vQtdeAgendarLoteItemKit;'+ #13 +
      '                end if;'+ #13 +
      #13 +
      '                begin'+ #13 +
      '                  INSERIR_ENTREGAS_ITENS_PEND('+ #13 +
      '                    xEmpresasEstoqueKit.EMPRESA_ID,'+ #13 +
      '                    xOrdemLocaisProdutoKit.LOCAL_ID,'+ #13 +
      '                    iORCAMENTO_ID,'+ #13 +
      '                    xItens.PREVISAO_ENTREGA,'+ #13 +
      '                    xItensKit.PRODUTO_ID,'+ #13 +
      '                    xItensKit.ITEM_ID,'+ #13 +
      '                    xEstoqueItemKit.LOTE,'+ #13 +
      '                    vQtdeAgendarLoteItemKit'+ #13 +
      '                  );'+ #13 +
      '                exception'+ #13 +
      '                  when others then'+ #13 +
      '                    erro(vQtdeAgendarLoteItemKit || ''  '' || xItensKit.PRODUTO_ID || ''  '' || xEmpresasEstoqueKit.EMPRESA_ID || ''  '' || vQtdeAgendarLoteItemKit || ''  '' || sqlerrm);'+ #13 +
      '                end;'+ #13 +
      #13 +
      '                if vQtdeFaltaAgendarItemKit = 0 then'+ #13 +
      '                  exit;'+ #13 +
      '                end if;'+ #13 +
      '              end loop;'+ #13 +
      '            end loop;'+ #13 +
      #13 +
      '            /* Inserindo o próprio item kit */'+ #13 +
      '            INSERIR_ENTREGAS_ITENS_PEND('+ #13 +
      '              xEmpresasEstoqueKit.EMPRESA_ID,'+ #13 +
      '              xOrdemLocaisProdutoKit.LOCAL_ID,'+ #13 +
      '              iORCAMENTO_ID,'+ #13 +
      '              xItens.PREVISAO_ENTREGA,'+ #13 +
      '              xItens.PRODUTO_ID,'+ #13 +
      '              xItens.ITEM_ID,'+ #13 +
      '              ''???'','+ #13 +
      '              vQtdeAgendar'+ #13 +
      '            );'+ #13 +
      #13 +
      '            vQtdeAgendarKitEmpresa := vQtdeAgendarKitEmpresa - vQtdeAgendar;'+ #13 +
      '            vQtdeFaltaAgendar      := vQtdeFaltaAgendar - vQtdeAgendar;'+ #13 +
      #13 +
      '            if vQtdeAgendarKitEmpresa = 0 or vQtdeFaltaAgendar = 0 then'+ #13 +
      '              exit;'+ #13 +
      '            end if;'+ #13 +
      '          end loop;'+ #13 +
      #13 +
      '          if vQtdeAgendarKitEmpresa > 0 then'+ #13 +
      '            ERRO('+ #13 +
      '              ''Estoque insuficiente para o produto kit '' || xItens.PRODUTO_ID || ''!'' || chr(13) ||'+ #13 +
      '              ''Faltam: '' || NFORMAT(vQtdeAgendarKitEmpresa, 0) || chr(13) ||'+ #13 +
      '              ''Verifique os locais dos produtos que compõe este kit!'''+ #13 +
      '            );'+ #13 +
      '          end if;'+ #13 +
      #13 +
      '        end loop;'+ #13 +
      '      end if;'+ #13 +
      #13 +
      '      if vQtdeFaltaAgendar > 0 then'+ #13 +
      '        ERRO('+ #13 +
      '          ''Estoque insuficiente para o produto kit '' || xItens.PRODUTO_ID || ''!'' || chr(13) ||'+ #13 +
      '          ''Faltam: '' || NFORMAT(vQtdeFaltaAgendar, 0)'+ #13 +
      '        );'+ #13 +
      '      end if;'+ #13 +
      #13 +
      '    /* Se for produto normal */'+ #13 +
      '    else'+ #13 +
      '      vQtdeFaltaAgendar := xItens.QUANTIDADE;'+ #13 +
      #13 +
      '      for xEstoques in cEstoques(xItens.PRODUTO_ID) loop'+ #13 +
      '        if xItens.ACEITAR_ESTOQUE_NEGATIVO = ''S'' then'+ #13 +
      '          vQtdeAgendar := vQtdeFaltaAgendar;'+ #13 +
      '          vQtdeFaltaAgendar := 0;'+ #13 +
      '        elsif xEstoques.DISPONIVEL > 0 then'+ #13 +
      '          if xEstoques.DISPONIVEL >= vQtdeFaltaAgendar then'+ #13 +
      '            vQtdeAgendar := vQtdeFaltaAgendar;'+ #13 +
      '            vQtdeFaltaAgendar := 0;'+ #13 +
      '          else'+ #13 +
      '            vQtdeAgendar := xEstoques.DISPONIVEL;'+ #13 +
      '            vQtdeFaltaAgendar := vQtdeFaltaAgendar - vQtdeAgendar;'+ #13 +
      '          end if;'+ #13 +
      '        else'+ #13 +
      '          /* Se não aceita estoque negativo, passando para o próximo local. */'+ #13 +
      '          continue;'+ #13 +
      '        end if;'+ #13 +
      #13 +
      '        VERIFICAR_PENDENCIA_ENTREGA(xEstoques.EMPRESA_ID, xEstoques.LOCAL_ID, iORCAMENTO_ID, xItens.PREVISAO_ENTREGA, ''NHR'');'+ #13 +
      #13 +
      '        INSERIR_ENTREGAS_ITENS_PEND('+ #13 +
      '          xEstoques.EMPRESA_ID,'+ #13 +
      '          xEstoques.LOCAL_ID,'+ #13 +
      '          iORCAMENTO_ID,'+ #13 +
      '          xItens.PREVISAO_ENTREGA,'+ #13 +
      '          xItens.PRODUTO_ID,'+ #13 +
      '          xItens.ITEM_ID,'+ #13 +
      '          xEstoques.LOTE,'+ #13 +
      '          vQtdeAgendar'+ #13 +
      '        );'+ #13 +
      #13 +
      '        if vQtdeFaltaAgendar = 0 then'+ #13 +
      '          exit;'+ #13 +
      '        end if;'+ #13 +
      '      end loop;'+ #13 +
      #13 +
      '      if vQtdeFaltaAgendar > 0 then'+ #13 +
      '        ERRO( ''Estoque insuficiente para geração da entrega! Produto: '' || xItens.PRODUTO_ID );'+ #13 +
      '      end if;'+ #13 +
      #13 +
      '    end if;'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  /* Limpando a tabela no final do processo */'+ #13 +
      '  delete from ENTREGAS_ITENS_A_GERAR_TEMP'+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '  and TIPO_ENTREGA = ''EN'';'+ #13 +
      #13 +
      'end GERAR_PENDENCIA_ENTREGAS;'+ #13
    );
end;

function AtualizarProcRECEBER_PEDIDO: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure RECEBER_PEDIDO('+ #13 +
      '  iORCAMENTO_ID        in positiven,'+ #13 +
      '  iTURNO_ID            in positiven,'+ #13 +
      '  iVALOR_TROCO         in number,'+ #13 +
      '  iGERAR_CREDITO_TROCO in string,'+ #13 +
      '  iTIPO_NOTA_GERAR     in string'+ #13 +
      ')'+ #13 +
      'is'+ #13 +
      '  i                       naturaln default 0;'+ #13 +
      '  vRetiradaId             RETIRADAS.RETIRADA_ID%type;'+ #13 +
      '  vReceberId              CONTAS_RECEBER.RECEBER_ID%type;'+ #13 +
      '  vDadosPagamentoPedido   RecordsVendas.RecPedidosPagamentos;'+ #13 +
      #13 +
      '  vPagamentoInconsistente char(1);'+ #13 +
      '  vBaixaCreditoPagarId    CONTAS_PAGAR_BAIXAS.BAIXA_ID%type;'+ #13 +
      '  vValorTotal             ORCAMENTOS.VALOR_TOTAL%type;'+ #13 +
      '  vValorBaixarCredito     number default 0;'+ #13 +
      '  vValorRestante          number default 0;'+ #13 +
      '  vReceberNaEntrega       ORCAMENTOS.RECEBER_NA_ENTREGA%type;'+ #13 +
      #13 +
      '  cursor cCheques is'+ #13 +
      '  select'+ #13 +
      '    COBRANCA_ID,'+ #13 +
      '    ITEM_ID,'+ #13 +
      '    DATA_VENCIMENTO,'+ #13 +
      '    BANCO,'+ #13 +
      '    AGENCIA,'+ #13 +
      '    CONTA_CORRENTE,'+ #13 +
      '    NUMERO_CHEQUE,'+ #13 +
      '    VALOR_CHEQUE,'+ #13 +
      '    NOME_EMITENTE,'+ #13 +
      '    CPF_CNPJ_EMITENTE,'+ #13 +
      '    TELEFONE_EMITENTE,'+ #13 +
      '    PARCELA,'+ #13 +
      '    NUMERO_PARCELAS'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS_PAGAMENTOS_CHEQUES'+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '  order by '+ #13 +
      '		COBRANCA_ID, '+ #13 +
      '		PARCELA,'+ #13 +
      '		DATA_VENCIMENTO, '+ #13 +
      '		ITEM_ID;'+ #13 +
      #13 +
      '  cursor cCobrancas(pTIPO in string) is'+ #13 +
      '  select'+ #13 +
      '    PAG.COBRANCA_ID,'+ #13 +
      '    PAG.ITEM_ID,'+ #13 +
      '    PAG.VALOR,'+ #13 +
      '    PAG.DATA_VENCIMENTO,'+ #13 +
      '    PAG.PARCELA,'+ #13 +
      '    PAG.NUMERO_PARCELAS,'+ #13 +
      '    TIP.BOLETO_BANCARIO,'+ #13 +
      '    PAG.NSU_TEF,'+ #13 +
      '    TIP.PORTADOR_ID,'+ #13 +
      '    PAG.NUMERO_CARTAO,'+ #13 +
      '    PAG.CODIGO_AUTORIZACAO'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS_PAGAMENTOS PAG'+ #13 +
      #13 +
      '  join TIPOS_COBRANCA TIP'+ #13 +
      '  on PAG.COBRANCA_ID = TIP.COBRANCA_ID    '+ #13 +
      '  and TIP.FORMA_PAGAMENTO = pTIPO'+ #13 +
      #13 +
      '  where PAG.ORCAMENTO_ID = iORCAMENTO_ID  '+ #13 +
      '  order by '+ #13 +
      '		COBRANCA_ID,'+ #13 +
      '		PARCELA,'+ #13 +
      '		DATA_VENCIMENTO, '+ #13 +
      '		ITEM_ID;'+ #13 +
      #13 +
      '  cursor cCreditos is'+ #13 +
      '  select'+ #13 +
      '    ORC.PAGAR_ID,'+ #13 +
      '    PAG.VALOR_DOCUMENTO - PAG.VALOR_DESCONTO - PAG.VALOR_ADIANTADO as VALOR_LIQUIDO'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS_CREDITOS ORC'+ #13 +
      #13 +
      '  inner join CONTAS_PAGAR PAG'+ #13 +
      '  on ORC.PAGAR_ID = PAG.PAGAR_ID'+ #13 +
      #13 +
      '  where ORC.ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '  and ORC.MOMENTO_USO = ''R'';'+ #13 +
      #13 +
      'begin'+ #13 +
      #13 +
      '  begin'+ #13 +
      '    select'+ #13 +
      '      VALOR_DINHEIRO,'+ #13 +
      '      VALOR_CARTAO_DEBITO,'+ #13 +
      '      VALOR_CARTAO_CREDITO,'+ #13 +
      '      VALOR_CREDITO,'+ #13 +
      '      VALOR_COBRANCA,'+ #13 +
      '      VALOR_CHEQUE,'+ #13 +
      '      VALOR_FINANCEIRA,'+ #13 +
      '      VALOR_ACUMULATIVO,'+ #13 +
      '      STATUS,'+ #13 +
      '      VENDEDOR_ID,'+ #13 +
      '      EMPRESA_ID,'+ #13 +
      '      CLIENTE_ID,'+ #13 +
      '      RECEBER_NA_ENTREGA,'+ #13 +
      '      VALOR_TOTAL,'+ #13 +
      '      case when VALOR_TOTAL < VALOR_TOTAL_PRODUTOS + VALOR_OUTRAS_DESPESAS + VALOR_FRETE - VALOR_DESCONTO then ''S'' else ''N'' end'+ #13 +
      '    into'+ #13 +
      '      vDadosPagamentoPedido.VALOR_DINHEIRO,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_CARTAO_DEBITO,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_CARTAO_CREDITO,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_CREDITO,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_COBRANCA,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_CHEQUE,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_FINANCEIRA,'+ #13 +
      '      vDadosPagamentoPedido.VALOR_ACUMULATIVO,'+ #13 +
      '      vDadosPagamentoPedido.STATUS,'+ #13 +
      '      vDadosPagamentoPedido.VENDEDOR_ID,'+ #13 +
      '      vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
      '      vDadosPagamentoPedido.CLIENTE_ID,'+ #13 +
      '      vReceberNaEntrega,'+ #13 +
      '      vValorTotal,'+ #13 +
      '      vPagamentoInconsistente'+ #13 +
      '    from'+ #13 +
      '      ORCAMENTOS'+ #13 +
      '    where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      '  exception'+ #13 +
      '    when others then'+ #13 +
      '      ERRO(''Não foi encontrado os valores de pagamento para o orçamento '' || iORCAMENTO_ID || ''!'' || chr(13) || chr(10) || sqlerrm);'+ #13 +
      '  end;'+ #13 +
      #13 +
      '  if vDadosPagamentoPedido.STATUS = ''RE'' then'+ #13 +
      '    ERRO(''Este orçamento já foi recebido!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if vDadosPagamentoPedido.STATUS not in(''VR'', ''VE'') then'+ #13 +
      '    ERRO(''Este pedido não está aguardando recebimento, verifique!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  /* Apagando as previsões dos recebimentos na entrega */'+ #13 +
      '  if vDadosPagamentoPedido.STATUS = ''VE'' then'+ #13 +
      '    delete from CONTAS_RECEBER'+ #13 +
      '    where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
      '    and COBRANCA_ID = SESSAO.PARAMETROS_GERAIS.TIPO_COB_RECEB_ENTREGA_ID;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  /* Inserindo cheques no financeiro */'+ #13 +
      '  if vDadosPagamentoPedido.VALOR_CHEQUE > 0 then'+ #13 +
      '    for vCheques in cCheques loop'+ #13 +
      '      select SEQ_RECEBER_ID.nextval'+ #13 +
      '      into vReceberId'+ #13 +
      '      from dual;'+ #13 +
      #13 +
      '      insert into CONTAS_RECEBER('+ #13 +
      '        RECEBER_ID,'+ #13 +
      '        CADASTRO_ID,'+ #13 +
      '        EMPRESA_ID,'+ #13 +
      '        ORCAMENTO_ID,'+ #13 +
      '        COBRANCA_ID,'+ #13 +
      '        PORTADOR_ID,'+ #13 +
      '        PLANO_FINANCEIRO_ID,'+ #13 +
      '        TURNO_ID,'+ #13 +
      '        VENDEDOR_ID,'+ #13 +
      '        DOCUMENTO,'+ #13 +
      '        BANCO,'+ #13 +
      '        AGENCIA,'+ #13 +
      '        CONTA_CORRENTE,'+ #13 +
      '        NUMERO_CHEQUE,'+ #13 +
      '        NOME_EMITENTE,'+ #13 +
      '        CPF_CNPJ_EMITENTE,'+ #13 +
      '        TELEFONE_EMITENTE,'+ #13 +
      '        DATA_EMISSAO,'+ #13 +
      '        DATA_VENCIMENTO,'+ #13 +
      '        DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '        VALOR_DOCUMENTO,'+ #13 +
      '        STATUS,'+ #13 +
      '        PARCELA,'+ #13 +
      '        NUMERO_PARCELAS,'+ #13 +
      '        ORIGEM'+ #13 +
      '      )values('+ #13 +
      '        vReceberId,'+ #13 +
      '        vDadosPagamentoPedido.CLIENTE_ID,'+ #13 +
      '        vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
      '        iORCAMENTO_ID,'+ #13 +
      '        vCheques.COBRANCA_ID,'+ #13 +
      '        ''9998'','+ #13 +
      '        ''1.001.002'','+ #13 +
      '        iTURNO_ID,'+ #13 +
      '        vDadosPagamentoPedido.VENDEDOR_ID,'+ #13 +
      '        ''PED-'' || iORCAMENTO_ID || vCheques.ITEM_ID || ''/CHQ'','+ #13 +
      '        vCheques.BANCO,'+ #13 +
      '        vCheques.AGENCIA,'+ #13 +
      '        vCheques.CONTA_CORRENTE,'+ #13 +
      '        vCheques.NUMERO_CHEQUE,'+ #13 +
      '        vCheques.NOME_EMITENTE,'+ #13 +
      '        vCheques.CPF_CNPJ_EMITENTE,'+ #13 +
      '        vCheques.TELEFONE_EMITENTE,'+ #13 +
      '        trunc(sysdate),'+ #13 +
      '        vCheques.DATA_VENCIMENTO,'+ #13 +
      '        vCheques.DATA_VENCIMENTO,'+ #13 +
      '        vCheques.VALOR_CHEQUE,'+ #13 +
      '        ''A'','+ #13 +
      '        vCheques.PARCELA,'+ #13 +
      '        vCheques.NUMERO_PARCELAS,'+ #13 +
      '        ''ORC'''+ #13 +
      '      );'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if vDadosPagamentoPedido.VALOR_COBRANCA > 0 then'+ #13 +
      '    for vCobrancas in cCobrancas(''COB'') loop'+ #13 +
      '      select SEQ_RECEBER_ID.nextval'+ #13 +
      '      into vReceberId'+ #13 +
      '      from dual;'+ #13 +
      #13 +
      '      insert into CONTAS_RECEBER('+ #13 +
      '        RECEBER_ID,'+ #13 +
      '        CADASTRO_ID,'+ #13 +
      '        EMPRESA_ID,'+ #13 +
      '        ORCAMENTO_ID,'+ #13 +
      '        COBRANCA_ID,'+ #13 +
      '        PORTADOR_ID,'+ #13 +
      '        PLANO_FINANCEIRO_ID,'+ #13 +
      '        TURNO_ID,'+ #13 +
      '        VENDEDOR_ID,'+ #13 +
      '        DOCUMENTO,'+ #13 +
      '        DATA_EMISSAO,'+ #13 +
      '        DATA_VENCIMENTO,'+ #13 +
      '        DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '        VALOR_DOCUMENTO,'+ #13 +
      '        STATUS,'+ #13 +
      '        PARCELA,'+ #13 +
      '        NUMERO_PARCELAS,'+ #13 +
      '        ORIGEM'+ #13 +
      '      )values('+ #13 +
      '        vReceberId,'+ #13 +
      '        vDadosPagamentoPedido.CLIENTE_ID,'+ #13 +
      '        vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
      '        iORCAMENTO_ID,'+ #13 +
      '        vCobrancas.COBRANCA_ID,'+ #13 +
      '        vCobrancas.PORTADOR_ID,'+ #13 +
      '        case when vCobrancas.BOLETO_BANCARIO = ''N'' then ''1.001.005'' else ''1.001.006'' end,'+ #13 +
      '        iTURNO_ID,'+ #13 +
      '        vDadosPagamentoPedido.VENDEDOR_ID,'+ #13 +
      '        ''PED-'' || iORCAMENTO_ID || vCobrancas.ITEM_ID || case when vCobrancas.BOLETO_BANCARIO = ''S'' then ''/BOL'' else ''/COB'' end,'+ #13 +
      '        trunc(sysdate),'+ #13 +
      '        vCobrancas.DATA_VENCIMENTO,'+ #13 +
      '        vCobrancas.DATA_VENCIMENTO,'+ #13 +
      '        vCobrancas.VALOR,'+ #13 +
      '        ''A'','+ #13 +
      '        vCobrancas.PARCELA,'+ #13 +
      '        vCobrancas.NUMERO_PARCELAS,'+ #13 +
      '        ''ORC'''+ #13 +
      '      );'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if vDadosPagamentoPedido.VALOR_ACUMULATIVO > 0 then'+ #13 +
      '    AJUSTAR_CONTAS_REC_ACUMULATIVO(iORCAMENTO_ID);'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if iGERAR_CREDITO_TROCO = ''S'' and iVALOR_TROCO > 0 then'+ #13 +
      '    GERAR_CREDITO_TROCO(iORCAMENTO_ID, ''ORC'', vDadosPagamentoPedido.CLIENTE_ID, iVALOR_TROCO);'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if vReceberNaEntrega = ''S'' and vDadosPagamentoPedido.VALOR_CREDITO > 0 then'+ #13 +
      '    vValorRestante :='+ #13 +
      '      vValorTotal + iVALOR_TROCO - ('+ #13 +
      '        vDadosPagamentoPedido.VALOR_DINHEIRO +'+ #13 +
      '        vDadosPagamentoPedido.VALOR_CHEQUE +'+ #13 +
      '        vDadosPagamentoPedido.VALOR_CARTAO_DEBITO +'+ #13 +
      '        vDadosPagamentoPedido.VALOR_CARTAO_CREDITO +'+ #13 +
      '        vDadosPagamentoPedido.VALOR_COBRANCA +'+ #13 +
      '        vDadosPagamentoPedido.VALOR_FINANCEIRA'+ #13 +
      '      );'+ #13 +
      #13 +
      '    for xCreditos in cCreditos loop'+ #13 +
      '      if xCreditos.VALOR_LIQUIDO >= vValorRestante then'+ #13 +
      '        vValorBaixarCredito := vValorRestante;'+ #13 +
      '        vValorRestante := 0;'+ #13 +
      '      else'+ #13 +
      '        vValorBaixarCredito := xCreditos.VALOR_LIQUIDO;'+ #13 +
      '        vValorRestante := vValorRestante - vValorBaixarCredito;'+ #13 +
      '      end if;'+ #13 +
      #13 +
      '      BAIXAR_CREDITO_PAGAR(xCreditos.PAGAR_ID, iORCAMENTO_ID, ''ORC'', vValorBaixarCredito, vBaixaCreditoPagarId, vBaixaCreditoPagarId);'+ #13 +
      #13 +
      '      if vValorRestante = 0 then'+ #13 +
      '        exit;'+ #13 +
      '      end if;'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  update ORCAMENTOS set'+ #13 +
      '    VALOR_TROCO = case when iGERAR_CREDITO_TROCO = ''N'' then iVALOR_TROCO else 0 end,'+ #13 +
      '    TURNO_ID = iTURNO_ID,'+ #13 +
      '    STATUS = ''RE'''+ #13 +
      '  where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      #13 +
      '  /* Não alterar este processo!!! Ele deve ser depois do recebimento */'+ #13 +
      '  if vDadosPagamentoPedido.VALOR_CARTAO_DEBITO + vDadosPagamentoPedido.VALOR_CARTAO_CREDITO > 0 then'+ #13 +
      '    for vCobrancas in cCobrancas(''CRT'') loop'+ #13 +
      '      GERAR_CARTOES_RECEBER(iORCAMENTO_ID, vCobrancas.ITEM_ID, vCobrancas.NSU_TEF, vCobrancas.CODIGO_AUTORIZACAO, vCobrancas.NUMERO_CARTAO);'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  select'+ #13 +
      '    max(RETIRADA_ID)'+ #13 +
      '  into'+ #13 +
      '    vRetiradaId'+ #13 +
      '  from'+ #13 +
      '    RETIRADAS'+ #13 +
      '  where TIPO_MOVIMENTO = ''A'''+ #13 +
      '  and ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
      #13 +
      '  if vRetiradaId is not null then'+ #13 +
      '    update RETIRADAS set'+ #13 +
      '      TIPO_NOTA_GERAR = nvl(iTIPO_NOTA_GERAR, ''NI'')'+ #13 +
      '    where RETIRADA_ID = vRetiradaId;'+ #13 +
      #13 +
      '    /* Caso o cliente trabalhe com confirmação de saídas de mercadorias */'+ #13 +
      '    if SESSAO.PARAMETROS_EMPRESA_LOGADA.CONFIRMAR_SAIDA_PRODUTOS = ''N'' then'+ #13 +
      '      CONFIRMAR_RETIRADA(vRetiradaId, ''AUTOMATICO'');'+ #13 +
      '    end if;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  ATUALIZAR_RETENCOES_CONTAS_REC(iORCAMENTO_ID, ''ORC'');'+ #13 +
      #13 +
      'end RECEBER_PEDIDO;'+ #13
    );
end;

function AtualizarProcCONSOLIDAR_BAIXA_CONTAS_REC: RecScript;
begin
  Result :=
    UteisUpdateDB.MontarScript(
      False,
      'create or replace procedure CONSOLIDAR_BAIXA_CONTAS_REC('+ #13 +
      '  iBAIXA_RECEBER_ID in number,'+ #13 +
      '  iCLIENTE_ID in number,'+ #13 +
      '  iVALOR_TROCO in number'+ #13 +
      ')'+ #13 +
      'is'+ #13 +
      #13 +
      '  vReceberId     CONTAS_RECEBER.RECEBER_ID%type;'+ #13 +
      '  vCadastroId    CONTAS_RECEBER_BAIXAS.BAIXA_ID%type;'+ #13 +
      '  vEmpresaId     CONTAS_RECEBER_BAIXAS.EMPRESA_ID%type;'+ #13 +
      '  vValorRetencao CONTAS_RECEBER_BAIXAS.VALOR_RETENCAO%type;'+ #13 +
      #13 +
      '  vValorBaixarCredito   number default 0;'+ #13 +
      '  vValorRestante        number default 0;'+ #13 +
      #13 +
      '  vBaixaCreditoPagarId  CONTAS_PAGAR_BAIXAS.BAIXA_ID%type;'+ #13 +
      '  vValorDinheiro        CONTAS_RECEBER_BAIXAS.VALOR_DINHEIRO%type;'+ #13 +
      '  vValorCheque          CONTAS_RECEBER_BAIXAS.VALOR_CHEQUE%type;'+ #13 +
      '  vValorCartaoDeb       CONTAS_RECEBER_BAIXAS.VALOR_CARTAO_DEBITO%type;'+ #13 +
      '  vValorCartaoCred      CONTAS_RECEBER_BAIXAS.VALOR_CARTAO_CREDITO%type;'+ #13 +
      '  vValorCobranca        CONTAS_RECEBER_BAIXAS.VALOR_COBRANCA%type;'+ #13 +
      '  vValorCredito         CONTAS_RECEBER_BAIXAS.VALOR_CREDITO%type;'+ #13 +
      '  vValorTotal           CONTAS_RECEBER_BAIXAS.VALOR_LIQUIDO%type;'+ #13 +
      #13 +
      '  cursor cCheques is'+ #13 +
      '  select'+ #13 +
      '    CHQ.COBRANCA_ID,'+ #13 +
      '    CHQ.DATA_VENCIMENTO,'+ #13 +
      '    CHQ.PARCELA,'+ #13 +
      '    CHQ.NUMERO_PARCELAS,'+ #13 +
      '    CHQ.BANCO,'+ #13 +
      '    CHQ.AGENCIA,'+ #13 +
      '    CHQ.CONTA_CORRENTE,'+ #13 +
      '    CHQ.NOME_EMITENTE,'+ #13 +
      '    CHQ.CPF_CNPJ_EMITENTE,'+ #13 +
      '    CHQ.TELEFONE_EMITENTE,'+ #13 +
      '    CHQ.VALOR_CHEQUE,'+ #13 +
      '    CHQ.NUMERO_CHEQUE,'+ #13 +
      '    CHQ.ITEM_ID'+ #13 +
      '  from'+ #13 +
      '    CONTAS_REC_BAIXAS_PAGTOS_CHQ CHQ'+ #13 +
      '  where CHQ.BAIXA_ID = iBAIXA_RECEBER_ID;'+ #13 +
      #13 +
      #13 +
      '  cursor cCobrancas(pTIPO in string) is'+ #13 +
      '  select'+ #13 +
      '    PAG.COBRANCA_ID,'+ #13 +
      '    PAG.ITEM_ID,'+ #13 +
      '    PAG.VALOR,'+ #13 +
      '    PAG.DATA_VENCIMENTO,'+ #13 +
      '    PAG.PARCELA,'+ #13 +
      '    PAG.NUMERO_PARCELAS,'+ #13 +
      '    TIP.BOLETO_BANCARIO,'+ #13 +
      '    PAG.NSU_TEF,'+ #13 +
      '    TIP.PORTADOR_ID'+ #13 +
      '  from'+ #13 +
      '    CONTAS_REC_BAIXAS_PAGAMENTOS PAG'+ #13 +
      #13 +
      '  join TIPOS_COBRANCA TIP'+ #13 +
      '  on PAG.COBRANCA_ID = TIP.COBRANCA_ID'+ #13 +
      '  and TIP.FORMA_PAGAMENTO = pTIPO'+ #13 +
      #13 +
      '  where PAG.BAIXA_ID = iBAIXA_RECEBER_ID'+ #13 +
      '  order by'+ #13 +
      '		COBRANCA_ID,'+ #13 +
      '		PARCELA,'+ #13 +
      '		DATA_VENCIMENTO,'+ #13 +
      '		ITEM_ID;'+ #13 +
      #13 +
      #13 +
      '  cursor cCartoes is'+ #13 +
      '  select'+ #13 +
      '    ITEM_ID,'+ #13 +
      '    NSU_TEF,'+ #13 +
      '    NUMERO_CARTAO,'+ #13 +
      '    CODIGO_AUTORIZACAO'+ #13 +
      '  from'+ #13 +
      '    CONTAS_REC_BAIXAS_PAGAMENTOS'+ #13 +
      '  where BAIXA_ID = iBAIXA_RECEBER_ID'+ #13 +
      '  and TIPO = ''CR'';'+ #13 +
      #13 +
      '  cursor cCreditos is'+ #13 +
      '  select'+ #13 +
      '    BAI.PAGAR_ID,'+ #13 +
      '    PAG.VALOR_DOCUMENTO - PAG.VALOR_DESCONTO - PAG.VALOR_ADIANTADO as VALOR_LIQUIDO'+ #13 +
      '  from'+ #13 +
      '    CONTAS_REC_BAIXAS_CREDITOS BAI'+ #13 +
      #13 +
      '  inner join CONTAS_PAGAR PAG'+ #13 +
      '  on BAI.PAGAR_ID = PAG.PAGAR_ID'+ #13 +
      #13 +
      '  where BAI.BAIXA_ID = iBAIXA_RECEBER_ID;'+ #13 +
      #13 +
      'begin'+ #13 +
      #13 +
      '  select'+ #13 +
      '    EMPRESA_ID,'+ #13 +
      '    CADASTRO_ID,'+ #13 +
      '    VALOR_DINHEIRO,'+ #13 +
      '    VALOR_CHEQUE,'+ #13 +
      '    VALOR_CARTAO_DEBITO,'+ #13 +
      '    VALOR_CARTAO_CREDITO,'+ #13 +
      '    VALOR_COBRANCA,'+ #13 +
      '    VALOR_CREDITO,'+ #13 +
      '    VALOR_LIQUIDO,'+ #13 +
      '    VALOR_RETENCAO'+ #13 +
      '  into'+ #13 +
      '    vEmpresaId,'+ #13 +
      '    vCadastroId,'+ #13 +
      '    vValorDinheiro,'+ #13 +
      '    vValorCheque,'+ #13 +
      '    vValorCartaoDeb,'+ #13 +
      '    vValorCartaoCred,'+ #13 +
      '    vValorCobranca,'+ #13 +
      '    vValorCredito,'+ #13 +
      '    vValorTotal,'+ #13 +
      '    vValorRetencao'+ #13 +
      '  from'+ #13 +
      '    CONTAS_RECEBER_BAIXAS'+ #13 +
      '  where BAIXA_ID = iBAIXA_RECEBER_ID;'+ #13 +
      #13 +
      '  for xCheques in cCheques loop'+ #13 +
      '    select SEQ_RECEBER_ID.nextval'+ #13 +
      '    into vReceberId'+ #13 +
      '    from dual;'+ #13 +
      #13 +
      '    insert into CONTAS_RECEBER('+ #13 +
      '      RECEBER_ID,'+ #13 +
      '      CADASTRO_ID,'+ #13 +
      '      EMPRESA_ID,'+ #13 +
      '      COBRANCA_ID,'+ #13 +
      '      PORTADOR_ID,'+ #13 +
      '      PLANO_FINANCEIRO_ID,'+ #13 +
      '      DOCUMENTO,'+ #13 +
      '      BANCO,'+ #13 +
      '      AGENCIA,'+ #13 +
      '      CONTA_CORRENTE,'+ #13 +
      '      NUMERO_CHEQUE,'+ #13 +
      '      NOME_EMITENTE,'+ #13 +
      '      CPF_CNPJ_EMITENTE,'+ #13 +
      '      TELEFONE_EMITENTE,'+ #13 +
      '      DATA_EMISSAO,'+ #13 +
      '      DATA_VENCIMENTO,'+ #13 +
      '      DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '      VALOR_DOCUMENTO,'+ #13 +
      '      STATUS,'+ #13 +
      '      PARCELA,'+ #13 +
      '      NUMERO_PARCELAS,'+ #13 +
      '      ORIGEM,'+ #13 +
      '      BAIXA_ORIGEM_ID'+ #13 +
      '    )values('+ #13 +
      '      vReceberId,'+ #13 +
      '      vCadastroId,'+ #13 +
      '      vEmpresaId,'+ #13 +
      '      xCheques.COBRANCA_ID,'+ #13 +
      '      ''9998'','+ #13 +
      '      ''1.001.002'','+ #13 +
      '      ''BXR-'' || iBAIXA_RECEBER_ID || xCheques.ITEM_ID || ''/CHQ'','+ #13 +
      '      xCheques.BANCO,'+ #13 +
      '      xCheques.AGENCIA,'+ #13 +
      '      xCheques.CONTA_CORRENTE,'+ #13 +
      '      xCheques.NUMERO_CHEQUE,'+ #13 +
      '      xCheques.NOME_EMITENTE,'+ #13 +
      '      xCheques.CPF_CNPJ_EMITENTE,'+ #13 +
      '      xCheques.TELEFONE_EMITENTE,'+ #13 +
      '      trunc(sysdate),'+ #13 +
      '      xCheques.DATA_VENCIMENTO,'+ #13 +
      '      xCheques.DATA_VENCIMENTO,'+ #13 +
      '      xCheques.VALOR_CHEQUE,'+ #13 +
      '      ''A'','+ #13 +
      '      xCheques.PARCELA,'+ #13 +
      '      xCheques.NUMERO_PARCELAS,'+ #13 +
      '      ''BXR'','+ #13 +
      '      iBAIXA_RECEBER_ID'+ #13 +
      '    );'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  for xCobrancas in cCobrancas(''COB'') loop'+ #13 +
      '    select SEQ_RECEBER_ID.nextval'+ #13 +
      '    into vReceberId'+ #13 +
      '    from dual;'+ #13 +
      #13 +
      '    insert into CONTAS_RECEBER('+ #13 +
      '      RECEBER_ID,'+ #13 +
      '      CADASTRO_ID,'+ #13 +
      '      EMPRESA_ID,'+ #13 +
      '      COBRANCA_ID,'+ #13 +
      '      PORTADOR_ID,'+ #13 +
      '      PLANO_FINANCEIRO_ID,'+ #13 +
      '      DOCUMENTO,'+ #13 +
      '      DATA_EMISSAO,'+ #13 +
      '      DATA_VENCIMENTO,'+ #13 +
      '      DATA_VENCIMENTO_ORIGINAL,'+ #13 +
      '      VALOR_DOCUMENTO,'+ #13 +
      '      STATUS,'+ #13 +
      '      PARCELA,'+ #13 +
      '      NUMERO_PARCELAS,'+ #13 +
      '      ORIGEM,'+ #13 +
      '      BAIXA_ORIGEM_ID'+ #13 +
      '    )values('+ #13 +
      '      vReceberId,'+ #13 +
      '      vCadastroId,'+ #13 +
      '      vEmpresaId,'+ #13 +
      '      xCobrancas.COBRANCA_ID,'+ #13 +
      '      xCobrancas.PORTADOR_ID,'+ #13 +
      '      case when xCobrancas.BOLETO_BANCARIO = ''N'' then ''1.001.005'' else ''1.001.006'' end,'+ #13 +
      '      ''BXR-'' || iBAIXA_RECEBER_ID || xCobrancas.ITEM_ID || case when xCobrancas.BOLETO_BANCARIO = ''S'' then ''/BOL'' else ''/COB'' end,'+ #13 +
      '      trunc(sysdate),'+ #13 +
      '      xCobrancas.DATA_VENCIMENTO,'+ #13 +
      '      xCobrancas.DATA_VENCIMENTO,'+ #13 +
      '      xCobrancas.VALOR,'+ #13 +
      '      ''A'','+ #13 +
      '      xCobrancas.PARCELA,'+ #13 +
      '      xCobrancas.NUMERO_PARCELAS,'+ #13 +
      '      ''BXR'','+ #13 +
      '      iBAIXA_RECEBER_ID'+ #13 +
      '    );'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  for xCartoes in cCartoes loop'+ #13 +
      '    GERAR_CARTOES_RECEBER_BAIXA(iBAIXA_RECEBER_ID, xCartoes.ITEM_ID, xCartoes.NSU_TEF, xCartoes.CODIGO_AUTORIZACAO, xCartoes.NUMERO_CARTAO);'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  ATUALIZAR_RETENCOES_CONTAS_REC(iBAIXA_RECEBER_ID, ''BXR'');'+ #13 +
      #13 +
      '  if vValorCredito > 0 then'+ #13 +
      '    vValorRestante := vValorTotal + iVALOR_TROCO - (vValorDinheiro + vValorCheque + vValorCartaoDeb + vValorCartaoCred + vValorCobranca);'+ #13 +
      '    for xCreditos in cCreditos loop'+ #13 +
      '      if xCreditos.VALOR_LIQUIDO >= vValorRestante then'+ #13 +
      '        vValorBaixarCredito := vValorRestante;'+ #13 +
      '        vValorRestante := 0;'+ #13 +
      '      else'+ #13 +
      '        vValorBaixarCredito := xCreditos.VALOR_LIQUIDO;'+ #13 +
      '        vValorRestante := vValorRestante - vValorBaixarCredito;'+ #13 +
      '      end if;'+ #13 +
      #13 +
      '      BAIXAR_CREDITO_PAGAR(xCreditos.PAGAR_ID, iBAIXA_RECEBER_ID, ''BCR'', vValorBaixarCredito, vBaixaCreditoPagarId, vBaixaCreditoPagarId);'+ #13 +
      #13 +
      '      if vValorRestante = 0 then'+ #13 +
      '        exit;'+ #13 +
      '      end if;'+ #13 +
      '    end loop;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if iVALOR_TROCO > 0 then'+ #13 +
      '    GERAR_CREDITO_TROCO(iBAIXA_RECEBER_ID, ''BXR'', iCLIENTE_ID, iVALOR_TROCO);'+ #13 +
      '  end if;'+ #13 +
      #13 +
      'end CONSOLIDAR_BAIXA_CONTAS_REC;'+ #13
    );
end;


end.
