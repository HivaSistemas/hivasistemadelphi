unit AjusteAnoMesFinalizado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _RecordsEstoques,
  Vcl.ComCtrls, Vcl.StdCtrls, CheckBoxLuka, Vcl.Buttons, Vcl.ExtCtrls, EditLuka, _Estoques,
  FrameEmpresas, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameProdutos, _Sessao,
  _Biblioteca, Vcl.Grids, GridLuka, _Imagens, _RecordsEspeciais;

type
  TFormAjusteFinalizacaoMensal = class(TFormHerancaRelatoriosPageControl)
    sbGravar: TSpeedButton;
    FrProdutos: TFrProdutos;
    FrEmpresas: TFrEmpresas;
    Label1: TLabel;
    eMes: TEditLuka;
    Label2: TLabel;
    eAno: TEditLuka;
    sgProdutos: TGridLuka;
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgProdutosGetCellPicture(Sender: TObject; ARow, ACol: Integer;
      var APicture: TPicture);
    procedure sgProdutosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sgProdutosSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sbGravarClick(Sender: TObject);
  private
    { Private declarations }
    FEmpresaId: Integer;
    FAno: string;
    FMes: string;
  public
    { Public declarations }
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

var
  FormAjusteFinalizacaoMensal: TFormAjusteFinalizacaoMensal;

implementation

const
  cSelecionado       = 0;
  cProduto           = 1;
  cNome              = 2;
  cNomeMarca         = 3;
  cEstoque           = 4;
  cCMV               = 5;
  cCustoUltimoPedido = 6;

{$R *.dfm}

{ TFormHerancaRelatoriosPageControl1 }

procedure TFormAjusteFinalizacaoMensal.Carregar(Sender: TObject);
var
  vSQL: string;
  i: Integer;
  produtos: TArray<RecProdutosAnoMesFinalizado>;
  vLinha: Integer;
begin
  inherited;

  sgProdutos.ClearGrid();

  FEmpresaId := FrEmpresas.getEmpresa().EmpresaId;
  FAno := eAno.Text;
  FMes := eMes.Text;

  if not FrProdutos.EstaVazio then
    vSql := vSql + ' and ' + FrProdutos.getSqlFiltros('PRO.PRODUTO_ID');

  vSql := vSql + ' order by PRO.NOME ';

  produtos := _Estoques.BuscarMesFinalizado(
    Sessao.getConexaoBanco,
    vSql,
    FrEmpresas.getEmpresa().EmpresaId,
    StrToInt(eAno.Text + eMes.Text)
  );

  if produtos = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vLinha := 0;
  for I := Low(produtos) to High(produtos) do begin
    Inc(vLinha);

    sgProdutos.Cells[cSelecionado, vLinha]       := charNaoSelecionado;
    sgProdutos.Cells[cProduto, vLinha]           := NFormat(produtos[i].produtoId);
    sgProdutos.Cells[cNome, vLinha]              := produtos[i].nomeProduto;
    sgProdutos.Cells[cNomeMarca, vLinha]         := produtos[i].nomeMarca;
    sgProdutos.Cells[cEstoque, vLinha]           := NFormatEstoque(produtos[i].estoque);
    sgProdutos.Cells[cCMV, vLinha]               := NFormatN(produtos[i].cmv);
    sgProdutos.Cells[cCustoUltimoPedido, vLinha] := NFormatN(produtos[i].custoUltimoPedido);
  end;

  sgProdutos.SetLinhasGridPorTamanhoVetor(Length(produtos));
  SetarFoco(sgProdutos);
end;

procedure TFormAjusteFinalizacaoMensal.FormCreate(Sender: TObject);
begin
  inherited;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId);
end;

procedure TFormAjusteFinalizacaoMensal.sbGravarClick(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  vProdutosSelecionados: TArray<RecProdutosAnoMesFinalizado>;
begin
  inherited;

   for i := 1 to sgProdutos.RowCount -1 do begin
     if sgProdutos.Cells[cSelecionado, i] = charNaoSelecionado then
      Continue;

     SetLength(vProdutosSelecionados, Length(vProdutosSelecionados) + 1);
     vProdutosSelecionados[High(vProdutosSelecionados)].produtoId         := SFormatInt(sgProdutos.Cells[cProduto, i]);
     vProdutosSelecionados[High(vProdutosSelecionados)].estoque           := SFormatDouble(sgProdutos.Cells[cEstoque, i]);
     vProdutosSelecionados[High(vProdutosSelecionados)].cmv               := SFormatDouble(sgProdutos.Cells[cCMV, i]);
     vProdutosSelecionados[High(vProdutosSelecionados)].custoUltimoPedido := SFormatDouble(sgProdutos.Cells[cCustoUltimoPedido, i]);
   end;

  if vProdutosSelecionados = nil then begin
    _Biblioteca.NenhumRegistroSelecionado;
    SetarFoco(sgProdutos);
    Abort;
  end;

  vRetBanco := _Estoques.AtualizarFinalizacaoMensal(
    Sessao.getConexaoBanco,
    vProdutosSelecionados,
    FEmpresaId,
    StrToInt(FAno + FMes)
  );

  if vRetBanco.TeveErro then begin
    Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  RotinaSucesso;
  Carregar(nil);
end;

procedure TFormAjusteFinalizacaoMensal.sgProdutosDrawCell(Sender: TObject;
  ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  alinhamento: TAlignment;
begin
  inherited;

  if ACol in[cProduto, cEstoque, cCMV, cCustoUltimoPedido] then
    alinhamento := taRightJustify
  else if ACol in[cSelecionado] then
    alinhamento := taCenter
  else
    alinhamento := taLeftJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], alinhamento, Rect);
end;

procedure TFormAjusteFinalizacaoMensal.sgProdutosGetCellColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol in[
    cEstoque,
    cCMV,
    cCustoUltimoPedido]
  then begin
    AFont.Color := _Biblioteca.coCorFonteEdicao1;
    ABrush.Color := _Biblioteca.coCorCelulaEdicao1;
  end
end;

procedure TFormAjusteFinalizacaoMensal.sgProdutosGetCellPicture(Sender: TObject;
  ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgProdutos.Cells[ACol, ARow] = '' then
   Exit;

  if ACol = cSelecionado then begin
    if sgProdutos.Cells[ACol, ARow] = charNaoSelecionado then
      APicture := _Imagens.FormImagens.im1.Picture
    else if sgProdutos.Cells[ACol, ARow] = charSelecionado then
      APicture := _Imagens.FormImagens.im2.Picture;
  end;
end;

procedure TFormAjusteFinalizacaoMensal.sgProdutosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_SPACE then
    sgProdutos.Cells[cSelecionado, sgProdutos.Row] := IIfStr(sgProdutos.Cells[cSelecionado, sgProdutos.Row] = charNaoSelecionado, charSelecionado, charNaoSelecionado);

  inherited;
end;

procedure TFormAjusteFinalizacaoMensal.sgProdutosSelectCell(Sender: TObject;
  ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol in[
    cEstoque,
    cCMV,
    cCustoUltimoPedido]
  then
    sgProdutos.Options := sgProdutos.Options + [goEditing]
  else
    sgProdutos.Options := sgProdutos.Options - [goEditing];
end;

procedure TFormAjusteFinalizacaoMensal.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrEmpresas.EstaVazio then begin
    Exclamar('� necess�rio informar uma empresa.');
    SetarFoco(FrEmpresas);
    Abort;
  end;

  if (eMes.AsInt = 0) or (eMes.AsInt > 12) or (eAno.AsInt < 0) then begin
    Exclamar('� necess�rio informar um m�s de finaliza��o v�lido.');
    SetarFoco(eMes);
    Abort;
  end;

  if (eAno.AsInt = 0) or (eAno.AsInt < 0) then begin
    Exclamar('� necess�rio informar um ano de finaliza��o v�lido.');
    SetarFoco(eAno);
    Abort;
  end;
end;

end.
