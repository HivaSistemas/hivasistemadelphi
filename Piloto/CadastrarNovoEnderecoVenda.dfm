inherited FormCadastrarNovoEnderecoVenda: TFormCadastrarNovoEnderecoVenda
  Caption = 'Cadastro de novo endere'#231'o - Venda'
  ClientHeight = 275
  ClientWidth = 665
  OnShow = FormShow
  ExplicitWidth = 671
  ExplicitHeight = 304
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 238
    Width = 665
    ExplicitTop = 238
    ExplicitWidth = 665
  end
  inline FrDiversosEnderecos: TFrDiversosEnderecos
    Left = 0
    Top = 1
    Width = 661
    Height = 234
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitTop = 1
    ExplicitWidth = 661
    ExplicitHeight = 234
    inherited lb19: TLabel
      Width = 68
      Height = 14
      ExplicitWidth = 68
      ExplicitHeight = 14
    end
    inherited sgOutrosEnderecos: TGridLuka
      Width = 661
      Height = 153
      OnDblClick = FrDiversosEnderecossgOutrosEnderecosDblClick
      OnKeyDown = FrDiversosEnderecossgOutrosEnderecosKeyDown
      ExplicitWidth = 661
      ExplicitHeight = 153
    end
    inherited eCEP: TEditCEPLuka
      Height = 22
      ExplicitHeight = 22
    end
    inherited eLogradouro: TEditLuka
      Height = 22
      ExplicitHeight = 22
    end
    inherited eComplemento: TEditLuka
      Height = 22
      MaxLength = 55
      ExplicitHeight = 22
    end
    inherited FrBairro: TFrBairros
      inherited PnTitulos: TPanel
        inherited lbNomePesquisa: TLabel
          Width = 33
          Height = 15
          Font.Charset = ANSI_CHARSET
          Font.Height = -12
          Font.Name = 'Calibri'
          ExplicitWidth = 33
          ExplicitHeight = 14
        end
        inherited pnSuprimir: TPanel
          inherited ckSuprimir: TCheckBox
            Visible = False
          end
        end
      end
      inherited pnPesquisa: TPanel
        ExplicitLeft = 268
      end
    end
    inherited ePontoReferencia: TEditLuka
      Height = 22
      MaxLength = 20
      ExplicitHeight = 22
    end
    inherited eNumero: TEditLuka
      Height = 22
      ExplicitHeight = 22
    end
    inherited eInscricaoEstadual: TEditLuka
      Height = 22
      OnKeyDown = FrDiversosEnderecoseCEPKeyDown
      ExplicitHeight = 22
    end
  end
end
