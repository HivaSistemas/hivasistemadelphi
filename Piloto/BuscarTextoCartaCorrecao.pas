unit BuscarTextoCartaCorrecao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.StdCtrls, _Biblioteca, _Sessao,
  MemoAltis, Vcl.Buttons, Vcl.ExtCtrls, _RecordsEspeciais, _NotasFiscaisCartasCorrecoes;

type
  TFormBuscarTextoCartaCorrecao = class(TFormHerancaFinalizar)
    meTextoCorrecao: TMemoAltis;
    lb1: TLabel;
  private
    FNotaFiscalId: Integer;
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function buscarCorrecao(pNotaFiscalId: Integer): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

function buscarCorrecao(pNotaFiscalId: Integer): TRetornoTelaFinalizar;
var
  vForm: TFormBuscarTextoCartaCorrecao;
begin
  vForm := TFormBuscarTextoCartaCorrecao.Create(nil);

  vForm.FNotaFiscalId := pNotaFiscalId;

  Result.Ok(vForm.ShowModal, True);
end;

{ TFormBuscarTextoCartaCorrecao }

procedure TFormBuscarTextoCartaCorrecao.Finalizar(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  vTexto: string;
begin
  vTexto := Trim(meTextoCorrecao.Lines.Text);
  vTexto := _Biblioteca.RemoverCaracteresEspeciais(vTexto);

  vRetBanco :=
    _NotasFiscaisCartasCorrecoes.AtualizarNotasFiscaisCartasCorrecoes(
      Sessao.getConexaoBanco,
      FNotaFiscalId,
      0,
      vTexto,
      'N'
    );

  Sessao.AbortarSeHouveErro( vRetBanco );

  inherited;
end;

procedure TFormBuscarTextoCartaCorrecao.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if Trim(meTextoCorrecao.Lines.Text) = '' then begin
    _Biblioteca.Exclamar('Nenhum texto v�lido foi informado para corre��o!');
    SetarFoco(meTextoCorrecao);
    Abort;
  end;

end;

end.
