inherited FormPesquisaMarcas: TFormPesquisaMarcas
  Caption = 'Pesquisa de marcas'
  ClientHeight = 232
  ClientWidth = 408
  ExplicitWidth = 414
  ExplicitHeight = 261
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 408
    Height = 186
    ColCount = 4
    OnDrawCell = sgPesquisaDrawCell
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Nome'
      'Ativo')
    RealColCount = 4
    ExplicitLeft = 0
    ExplicitTop = 46
    ExplicitWidth = 408
    ExplicitHeight = 186
    ColWidths = (
      28
      55
      211
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Top = 208
    Text = '0'
    ExplicitTop = 208
  end
  inherited pnFiltro1: TPanel
    Width = 408
    ExplicitWidth = 408
    inherited lblPesquisa: TLabel
      Left = 123
      Width = 37
      ExplicitLeft = 123
      ExplicitWidth = 37
    end
    inherited eValorPesquisa: TEditLuka
      Left = 123
      Width = 280
      ExplicitLeft = 123
      ExplicitWidth = 280
    end
    inherited cbOpcoesPesquisa: TComboBoxLuka
      Width = 117
      ExplicitWidth = 117
    end
  end
end
