inherited FormPesquisaGruposTributacoesFederalVenda: TFormPesquisaGruposTributacoesFederalVenda
  Caption = 'Pesquisa grupos de tributa'#231#245'es federal de venda'
  ClientWidth = 530
  ExplicitWidth = 538
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 530
    ColCount = 4
    OnDrawCell = sgPesquisaDrawCell
    CorLinhaFoco = 38619
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Descri'#231#227'o'
      'Ativo?')
    RealColCount = 4
    ExplicitWidth = 530
    ColWidths = (
      28
      48
      363
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Text = '0'
  end
  inherited pnFiltro1: TPanel
    Width = 530
    ExplicitWidth = 530
    inherited lblPesquisa: TLabel
      Width = 158
      Caption = 'Grupo de tributacoes federal'
      ExplicitWidth = 158
    end
    inherited eValorPesquisa: TEditLuka
      Width = 323
      ExplicitWidth = 323
    end
  end
end
