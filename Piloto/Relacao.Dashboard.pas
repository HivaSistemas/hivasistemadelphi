unit Relacao.Dashboard;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl,
  Vcl.ComCtrls, Vcl.StdCtrls, CheckBoxLuka, Vcl.Buttons, Vcl.ExtCtrls,
  Vcl.Grids, GridLuka, StaticTextLuka, EditLuka, _RelacaoDashboard, _Sessao, DateUtils, _Biblioteca,
  VclTee.TeeGDIPlus, VCLTee.TeEngine, VCLTee.Series, VCLTee.TeeProcs,
  VCLTee.Chart, Vcl.Imaging.pngimage, Vcl.Imaging.jpeg;

type
  TFormRelacaoDashboard = class(TFormHerancaRelatoriosPageControl)
    StaticTextLuka1: TStaticTextLuka;
    StaticTextLuka2: TStaticTextLuka;
    StaticTextLuka3: TStaticTextLuka;
    StaticTextLuka4: TStaticTextLuka;
    lb1: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    eValorLucratividadeDiaAnterior: TEditLuka;
    eValorLucratividadeMesAtual: TEditLuka;
    eValorLucratividadeMesAnterior: TEditLuka;
    Label3: TLabel;
    Label5: TLabel;
    eQtdeOrcamentosAbertosMesAtual: TEditLuka;
    eValorOrcamentosAbertoMesAtual: TEditLuka;
    eQtdeOrcamentoAbertoMesAnterior: TEditLuka;
    eValorOrcamentoAbertoMesAnterior: TEditLuka;
    Label7: TLabel;
    Label9: TLabel;
    eQtdeTitulosPagarVencendoHoje: TEditLuka;
    eValorTituloPagarVencendoHoje: TEditLuka;
    eQtdeTitulosPagarAberto: TEditLuka;
    eValorTotalTitulosPagarAberto: TEditLuka;
    Label11: TLabel;
    Label13: TLabel;
    eQuantidadeTitulosReceberVencendoHoje: TEditLuka;
    eValorTitulosReceberVencendoHoje: TEditLuka;
    eQtdeTitutlosReceberAberto: TEditLuka;
    eValorTotalTitulosReceberAberto: TEditLuka;
    Label15: TLabel;
    eQtdeOrcamentosBloqueados: TEditLuka;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    StaticTextLuka5: TStaticTextLuka;
    eVendaDiaAtual: TEditLuka;
    eVendaDiaAnterior: TEditLuka;
    eVendaMesAtual: TEditLuka;
    eVendaMesAnterior: TEditLuka;
    Label20: TLabel;
    eVendaMesAnteriorAnoAnterior: TEditLuka;
    eValorLucratividadeDiaAtual: TEditLuka;
    Label21: TLabel;
    ePercLucratividadeDiaAtual: TEditLuka;
    ePercLucratividadeDiaAnterior: TEditLuka;
    Label22: TLabel;
    Label23: TLabel;
    ePercLucratividadeMesAtual: TEditLuka;
    ePercLucratividadeMesAnterior: TEditLuka;
    Label24: TLabel;
    eValorLucratividadeAnoAnterior: TEditLuka;
    ePercLucratividadeAnoAnterior: TEditLuka;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    cLucratividade: TChart;
    Series1: TBarSeries;
    cVendas: TChart;
    BarSeries1: TBarSeries;
    StaticTextLuka6: TStaticTextLuka;
    Label4: TLabel;
    Label6: TLabel;
    eQtdeDevolucoesMesAtual: TEditLuka;
    eValorDevolucoesMesAtual: TEditLuka;
    eQtdeDevolucoesMesAnterior: TEditLuka;
    eValorDevolucoesMesAnterior: TEditLuka;
    Label8: TLabel;
    eQtdeDevolucoesMesAnoAnterior: TEditLuka;
    eValorDevolucoesMesAnoAnterior: TEditLuka;
    Label10: TLabel;
    iLogo: TImage;
    iLogoPadrao: TImage;
    Label29: TLabel;
    Label31: TLabel;
    Label39: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label33: TLabel;
    Label30: TLabel;
    Label32: TLabel;
    Label34: TLabel;
    eVendaDiariaMesAtual: TEditLuka;
    eVendaDiariaMesAnterior: TEditLuka;
    eVendaDiariaMesAnoAnterior: TEditLuka;
    procedure FormCreate(Sender: TObject);
    function getDiaAnterior(): TDate;
    function getInicioMesAnterior(): TDate;
    function getFimMesAnterior(): TDate;
    function getInicioMesAnoAnterior(): TDate;
    function getFimMesAnoAnterior(): TDate;
    procedure carregarLogo();
  private

  public
    { Public declarations }
  end;

var
  FormRelacaoDashboard: TFormRelacaoDashboard;

implementation

{$R *.dfm}

procedure TFormRelacaoDashboard.carregarLogo;
var
  vImagem: TJPEGImage;
  pFoto: TMemoryStream;
begin
  pFoto := Sessao.getParametrosEmpresa.LogoEmpresa;
  if pFoto <> nil then begin
    vImagem := nil;

    iLogo.Visible := True;

    try
      try
        pFoto.Position := 0;
        vImagem := TJPEGImage.Create;
        vImagem.LoadFromStream(pFoto);
        iLogo.Picture.Graphic := vImagem;
      except

      end;
    finally
      vImagem.Free;
    end;
  end
  else begin
    iLogoPadrao.Visible := True;
  end;
end;

procedure TFormRelacaoDashboard.FormCreate(Sender: TObject);
var
  diaAnterior: TDateTime;
  mesAnteriorInicio: TDateTime;
  mesAnteriorFim: TDateTime;
  mesAnteriorAnoAnteriorInicio: TDateTime;
  mesAnteriorAnoAnteriorFim: TDateTime;
  sqlDate: string;
  vendas: RecVendas;
  devolucoesDiaAtual: RecVendas;
  devolucoesDiaAnterior: RecVendas;
  devolucoesMesAtual: RecVendas;
  devolucoesMesAnterior: RecVendas;
  devolucoesMesAnoAnterior: RecVendas;
  vendasMesAtual: RecVendas;
  vendasMesAnterior: RecVendas;
  vendasMesAnoAntrior: RecVendas;
begin
  inherited;
  carregarLogo();
  diaAnterior := getDiaAnterior();
  mesAnteriorInicio := getInicioMesAnterior();
  mesAnteriorFim := getFimMesAnterior();
  mesAnteriorAnoAnteriorInicio := getInicioMesAnoAnterior();
  mesAnteriorAnoAnteriorFim := getFimMesAnoAnterior();

  tsFiltros.TabVisible := false;
  eQtdeOrcamentosBloqueados.AsInt := _RelacaoDashboard.QuantidadeOrcamentosBloqueados(Sessao.getConexaoBanco, Sessao.getEmpresaLogada.EmpresaId);
  eQtdeTitulosPagarVencendoHoje.asInt := _RelacaoDashboard.QuantidadeTitulosPagarVencendoHoje(Sessao.getConexaoBanco, Sessao.getEmpresaLogada.EmpresaId);
  eQuantidadeTitulosReceberVencendoHoje.AsInt := _RelacaoDashboard.QuantidadeTitulosReceberVencendoHoje(Sessao.getConexaoBanco, Sessao.getEmpresaLogada.EmpresaId);
  eValorTituloPagarVencendoHoje.AsDouble := _RelacaoDashboard.ValorTitulosPagarVencendoHoje(Sessao.getConexaoBanco, Sessao.getEmpresaLogada.EmpresaId);
  eValorTitulosReceberVencendoHoje.AsDouble := _RelacaoDashboard.ValorTitulosReceberVencendoHoje(Sessao.getConexaoBanco, Sessao.getEmpresaLogada.EmpresaId);
  eQtdeTitulosPagarAberto.AsInt := _RelacaoDashboard.QuantidadeTitulosPagarAberto(Sessao.getConexaoBanco, Sessao.getEmpresaLogada.EmpresaId);
  eValorTotalTitulosPagarAberto.AsDouble := _RelacaoDashboard.ValorTitulosPagarAberto(Sessao.getConexaoBanco, Sessao.getEmpresaLogada.EmpresaId);
  eQtdeTitutlosReceberAberto.AsInt := _RelacaoDashboard.QuantidadeTitulosReceberAberto(Sessao.getConexaoBanco, Sessao.getEmpresaLogada.EmpresaId);
  eValorTotalTitulosReceberAberto.AsDouble := _RelacaoDashboard.ValorTitulosReceberAberto(Sessao.getConexaoBanco, Sessao.getParametrosEmpresa.PercentualMulta, Sessao.getParametrosEmpresa.PercentualJurosMensal, Sessao.getEmpresaLogada.EmpresaId);

  eVendaDiaAtual.AsDouble := _RelacaoDashboard.ValorVendasDiaAtual(Sessao.getConexaoBanco, Sessao.getEmpresaLogada.EmpresaId);
  sqlDate := ' and trunc(data_hora_recebimento) = trunc(sysdate)';
  devolucoesDiaAtual := _RelacaoDashboard.ValorDevolucoesMesAtual(Sessao.getConexaoBanco, sqlDate, Sessao.getEmpresaLogada.EmpresaId);

  eVendaDiaAnterior.AsDouble := _RelacaoDashboard.ValorVendasDiaAnterior(Sessao.getConexaoBanco, Sessao.getEmpresaLogada.EmpresaId);
  sqlDate := ' and trunc(data_hora_recebimento) = trunc(sysdate) - 1 ';
  devolucoesDiaAnterior := _RelacaoDashboard.ValorDevolucoesMesAtual(Sessao.getConexaoBanco, sqlDate, Sessao.getEmpresaLogada.EmpresaId);

  cVendas.Series[0].Clear;
  sqlDate := ' and trunc(data_hora_recebimento) between ' + ToDateOracle(StartOfTheMonth(date)) + ' and ' + ToDateOracle(date);
  vendasMesAtual := _RelacaoDashboard.ValorVendasMes(Sessao.getConexaoBanco, sqlDate, Sessao.getEmpresaLogada.EmpresaId);
  eVendaMesAtual.AsDouble := vendasMesAtual.ValorTotal;
  if vendasMesAtual.ValorTotal > 0 then
    eVendaDiariaMesAtual.AsDouble := vendasMesAtual.ValorTotal / vendasMesAtual.Quantidade;
  cVendas.Series[0].Add(eVendaMesAtual.AsDouble, 'M�s atual', RGB(0,0,255));

  sqlDate := ' and trunc(data_hora_recebimento) between ' + ToDateOracle(StartOfTheMonth(date)) + ' and ' + ToDateOracle(date);
  devolucoesMesAtual := _RelacaoDashboard.ValorDevolucoesMesAtual(Sessao.getConexaoBanco, sqlDate, Sessao.getEmpresaLogada.EmpresaId);
  eValorDevolucoesMesAtual.AsDouble := devolucoesMesAtual.ValorTotal;
  eQtdeDevolucoesMesAtual.AsInt := devolucoesMesAtual.Quantidade;

  sqlDate := ' and trunc(data_hora_recebimento) between ' + ToDateOracle(mesAnteriorInicio) + ' and ' + ToDateOracle(mesAnteriorFim);
  vendasMesAnterior := _RelacaoDashboard.ValorVendasMes(Sessao.getConexaoBanco, sqlDate, Sessao.getEmpresaLogada.EmpresaId);
  eVendaMesAnterior.AsDouble := vendasMesAnterior.ValorTotal;
  if vendasMesAnterior.ValorTotal > 0 then
    eVendaDiariaMesAnterior.AsDouble := vendasMesAnterior.ValorTotal / vendasMesAnterior.Quantidade;
  cVendas.Series[0].Add(eVendaMesAnterior.AsDouble, 'M�s anterior', RGB(0,100,0));

  sqlDate := ' and trunc(data_hora_recebimento) between ' + ToDateOracle(mesAnteriorInicio) + ' and ' + ToDateOracle(mesAnteriorFim);
  devolucoesMesAnterior := _RelacaoDashboard.ValorDevolucoesMesAtual(Sessao.getConexaoBanco, sqlDate, Sessao.getEmpresaLogada.EmpresaId);
  eValorDevolucoesMesAnterior.AsDouble := devolucoesMesAnterior.ValorTotal;
  eQtdeDevolucoesMesAnterior.AsInt := devolucoesMesAnterior.Quantidade;

  sqlDate := ' and trunc(data_hora_recebimento) between ' + ToDateOracle(mesAnteriorAnoAnteriorInicio) + ' and ' + ToDateOracle(mesAnteriorAnoAnteriorFim);
  vendasMesAnoAntrior := _RelacaoDashboard.ValorVendasMes(Sessao.getConexaoBanco, sqlDate, Sessao.getEmpresaLogada.EmpresaId);
  eVendaMesAnteriorAnoAnterior.AsDouble := vendasMesAnoAntrior.ValorTotal;
  if vendasMesAnoAntrior.ValorTotal > 0 then
    eVendaDiariaMesAnoAnterior.AsDouble := vendasMesAnoAntrior.ValorTotal / vendasMesAnoAntrior.Quantidade;
  cVendas.Series[0].Add(eVendaMesAnteriorAnoAnterior.AsDouble, 'M�s/Ano anterior', RGB(255,0,0));

  sqlDate := ' and trunc(data_hora_recebimento) between ' + ToDateOracle(mesAnteriorAnoAnteriorInicio) + ' and ' + ToDateOracle(mesAnteriorAnoAnteriorFim);
  devolucoesMesAnoAnterior := _RelacaoDashboard.ValorDevolucoesMesAtual(Sessao.getConexaoBanco, sqlDate, Sessao.getEmpresaLogada.EmpresaId);
  eValorDevolucoesMesAnoAnterior.AsDouble := devolucoesMesAnoAnterior.ValorTotal;
  eQtdeDevolucoesMesAnoAnterior.AsInt := devolucoesMesAnoAnterior.Quantidade;

  sqlDate := ' and trunc(data_cadastro) between ' + ToDateOracle(StartOfTheMonth(date)) + ' and ' + ToDateOracle(date);
  eQtdeOrcamentosAbertosMesAtual.AsInt := _RelacaoDashboard.QtdeOrcamentosAbertosMesAtual(Sessao.getConexaoBanco, sqlDate, Sessao.getEmpresaLogada.EmpresaId);
  eValorOrcamentosAbertoMesAtual.AsDouble := _RelacaoDashboard.ValorOrcamentosAbertosMesAtual(Sessao.getConexaoBanco, sqlDate, Sessao.getEmpresaLogada.EmpresaId);

  sqlDate := ' and trunc(data_cadastro) between ' + ToDateOracle(mesAnteriorInicio) + ' and ' + ToDateOracle(mesAnteriorFim);
  eQtdeOrcamentoAbertoMesAnterior.AsInt := _RelacaoDashboard.QtdeOrcamentosAbertosMesAnterior(Sessao.getConexaoBanco, sqlDate, Sessao.getEmpresaLogada.EmpresaId);
  eValorOrcamentoAbertoMesAnterior.AsDouble := _RelacaoDashboard.ValorOrcamentosAbertosMesAnterior(Sessao.getConexaoBanco, sqlDate, Sessao.getEmpresaLogada.EmpresaId);

  sqlDate := ' and trunc(sysdate) = trunc(ORC.DATA_HORA_RECEBIMENTO) ';
  vendas := _RelacaoDashboard.VendasDiaAtual(Sessao.getConexaoBanco, sqlDate, Sessao.getEmpresaLogada.EmpresaId);
  eValorLucratividadeDiaAtual.AsDouble := vendas.ValorLucro - devolucoesDiaAtual.ValorLucro;
  ePercLucratividadeDiaAnterior.AsDouble := Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(vendas.ValorTotal - devolucoesDiaAtual.ValorTotal, vendas.ValorLucro - devolucoesDiaAtual.ValorLucro,4 );;

  sqlDate := ' and trunc(sysdate) - 1 = trunc(ORC.DATA_HORA_RECEBIMENTO) ';
  vendas := _RelacaoDashboard.VendasDiaAtual(Sessao.getConexaoBanco, sqlDate, Sessao.getEmpresaLogada.EmpresaId);
  eValorLucratividadeDiaAnterior.AsDouble := vendas.ValorLucro - devolucoesDiaAnterior.ValorLucro;
  ePercLucratividadeDiaAnterior.AsDouble := Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(vendas.ValorTotal - devolucoesDiaAnterior.ValorTotal, vendas.ValorLucro - devolucoesDiaAnterior.ValorLucro,4 );;

  cLucratividade.Series[0].Clear;
  sqlDate := ' and trunc(ORC.DATA_HORA_RECEBIMENTO) between ' + ToDateOracle(StartOfTheMonth(date)) + ' and ' + ToDateOracle(date);
  vendas := _RelacaoDashboard.VendasDiaAtual(Sessao.getConexaoBanco, sqlDate, Sessao.getEmpresaLogada.EmpresaId);
  eValorLucratividadeMesAtual.AsDouble := vendas.ValorLucro - devolucoesMesAtual.ValorLucro;
  ePercLucratividadeMesAtual.AsDouble := Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(vendas.ValorTotal - devolucoesMesAtual.ValorTotal, vendas.ValorLucro - devolucoesMesAtual.ValorLucro,4 );
  cLucratividade.Series[0].Add(vendas.ValorLucro - devolucoesMesAtual.ValorLucro, 'M�s atual', RGB(0,0,255));

  sqlDate := ' and trunc(ORC.DATA_HORA_RECEBIMENTO) between ' + ToDateOracle(mesAnteriorInicio) + ' and ' + ToDateOracle(mesAnteriorFim);
  vendas := _RelacaoDashboard.VendasDiaAtual(Sessao.getConexaoBanco, sqlDate, Sessao.getEmpresaLogada.EmpresaId);
  eValorLucratividadeMesAnterior.AsDouble := vendas.ValorLucro - devolucoesMesAnterior.ValorLucro;
  ePercLucratividadeMesAnterior.AsDouble := Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(vendas.ValorTotal - devolucoesMesAnterior.ValorTotal, vendas.ValorLucro - devolucoesMesAnterior.ValorLucro,4 );
  cLucratividade.Series[0].Add(vendas.ValorLucro - devolucoesMesAnterior.ValorTotal, 'M�s anterior', RGB(0,100,0));

  sqlDate := ' and trunc(ORC.DATA_HORA_RECEBIMENTO) between ' + ToDateOracle(mesAnteriorAnoAnteriorInicio) + ' and ' + ToDateOracle(mesAnteriorAnoAnteriorFim);
  vendas := _RelacaoDashboard.VendasDiaAtual(Sessao.getConexaoBanco, sqlDate, Sessao.getEmpresaLogada.EmpresaId);
  eValorLucratividadeAnoAnterior.AsDouble := vendas.ValorLucro - devolucoesMesAnterior.ValorTotal;
  ePercLucratividadeAnoAnterior.AsDouble := Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(vendas.ValorTotal - devolucoesMesAnterior.ValorTotal, vendas.ValorLucro - devolucoesMesAnterior.ValorLucro,4 );
  cLucratividade.Series[0].Add(vendas.ValorLucro - devolucoesMesAnterior.ValorTotal, 'M�s/Ano anterior', RGB(255,0,0));

  pcDados.ActivePage := tsResultado;
end;

function TFormRelacaoDashboard.getDiaAnterior(): TDate;
begin
  result := IncDay(Date, -1);
end;

function TFormRelacaoDashboard.getInicioMesAnterior(): TDate;
var
  data: TDate;
begin
  data := Date;
  data := IncMonth(data, -1);
  data := StartOfTheMonth(data);
  Result := data;
end;

function TFormRelacaoDashboard.getFimMesAnterior(): TDate;
var
  data: TDate;
begin
  data := Date;
  data := IncMonth(data, -1);
  data := EndOfTheMonth(data);
  Result := data;
end;

function TFormRelacaoDashboard.getInicioMesAnoAnterior(): TDate;
var
  data: TDate;
begin
  data := Date;
  data := IncMonth(data, -1);
  data := IncYear(data, -1);
  data := StartOfTheMonth(data);
  Result := data;
end;

function TFormRelacaoDashboard.getFimMesAnoAnterior(): TDate;
var
  data: TDate;
begin
  data := Date;
  data := IncMonth(data, -1);
  data := IncYear(data, -1);
  data := EndOfTheMonth(data);
  Result := data;
end;

end.
