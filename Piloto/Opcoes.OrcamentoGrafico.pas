unit Opcoes.OrcamentoGrafico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Sessao,
  Vcl.ExtCtrls, Vcl.StdCtrls, RadioGroupLuka;

type
  TFormOpcoesOrcamentoGrafico = class(TFormHerancaFinalizar)
    rgExibirPrecoUnitario: TRadioGroupLuka;
    rgExibirPrecoTotal: TRadioGroupLuka;
    rgExibirDescontoProduto: TRadioGroupLuka;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure OpcçõesImpressaoOrcamentoGrafico(var pExibirPrecoUnitarioProdutos, pExibirPrecoTotalProdutos, pExibirDescontoProdutos : Boolean);

var
  FormOpcoesOrcamentoGrafico: TFormOpcoesOrcamentoGrafico;

implementation

{$R *.dfm}

uses _Biblioteca;

procedure OpcçõesImpressaoOrcamentoGrafico(var pExibirPrecoUnitarioProdutos, pExibirPrecoTotalProdutos, pExibirDescontoProdutos: Boolean);
var
  vForm: TFormOpcoesOrcamentoGrafico;
begin
  vForm := TFormOpcoesOrcamentoGrafico.Create(Application);
  vForm.rgExibirPrecoUnitario.SetIndicePorValor(IIfStr(pExibirPrecoUnitarioProdutos, 'S', 'N'));
  vForm.rgExibirPrecoTotal.SetIndicePorValor(IIfStr(pExibirPrecoTotalProdutos, 'S', 'N'));
  vForm.rgExibirDescontoProduto.SetIndicePorValor(IIfStr(Sessao.getParametros.HabilitarDescontoItemImpresaoOrcamento = 'S', 'S', 'N'));
  vForm.rgExibirDescontoProduto.Visible := Sessao.getParametros.HabilitarDescontoItemImpresaoOrcamento = 'S';
  vForm.Width := IIfInt(Sessao.getParametros.HabilitarDescontoItemImpresaoOrcamento = 'S',610,415);
  vForm.sbFinalizar.Left := IIfInt(Sessao.getParametros.HabilitarDescontoItemImpresaoOrcamento = 'S',241,138);
  vForm.ShowModal;

  if vForm.ModalResult = mrOK then begin
    pExibirPrecoUnitarioProdutos := vForm.rgExibirPrecoUnitario.GetValor = 'S';
    pExibirPrecoTotalProdutos := vForm.rgExibirPrecoTotal.GetValor = 'S';
    pExibirDescontoProdutos := vForm.rgExibirDescontoProduto.GetValor = 'S';
  end;
end;

procedure TFormOpcoesOrcamentoGrafico.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  if Key = #13 then begin
    sbFinalizarClick(nil);
  end;
end;

end.
