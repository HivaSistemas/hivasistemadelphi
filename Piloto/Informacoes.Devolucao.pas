unit Informacoes.Devolucao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Devolucoes, _DevolucoesItens,
  Vcl.ExtCtrls, Vcl.StdCtrls, EditLuka, Vcl.Mask, EditLukaData, Vcl.Grids, _RecordsOrcamentosVendas,
  GridLuka, _Biblioteca, _Sessao, System.Math, StaticTextLuka, Vcl.ComCtrls, _DevolucoesItensEntregas,
  Informacoes.Retiradas, Informacoes.Entrega, _DevolucoesItensRetiradas, Informacoes.TituloPagar,
  _NotasFiscais, _RecordsNotasFiscais, Informacoes.NotaFiscal;

type
  TFormInformacoesDevolucao = class(TFormHerancaFinalizar)
    lb1: TLabel;
    eDevolucaoId: TEditLuka;
    lbl6: TLabel;
    eOrcamentoId: TEditLuka;
    sbInformacoesOrcamento: TSpeedButton;
    lbl1: TLabel;
    eCliente: TEditLuka;
    StaticTextLuka1: TStaticTextLuka;
    stStatus: TStaticText;
    eEmpresa: TEditLuka;
    Label1: TLabel;
    pc1: TPageControl;
    tsPrincipais: TTabSheet;
    lbllb3: TLabel;
    lb15: TLabel;
    lb16: TLabel;
    lb17: TLabel;
    lb18: TLabel;
    lb2: TLabel;
    lb3: TLabel;
    lb4: TLabel;
    lb5: TLabel;
    eDataHoraDevolucao: TEditLukaData;
    eValorBruto: TEditLuka;
    eValorOutrasDespesas: TEditLuka;
    eValorDesconto: TEditLuka;
    eValorLiquido: TEditLuka;
    e1: TEditLuka;
    eUsuarioDevolucao: TEditLuka;
    eUsuarioConfirmacao: TEditLuka;
    eDataHoraConfirmacao: TEditLukaData;
    eObservacoes: TMemo;
    lbl26: TLabel;
    tsItens: TTabSheet;
    sgProdutos: TGridLuka;
    tsRetiradasEntregasDevolvidas: TTabSheet;
    tsNotasFiscais: TTabSheet;
    sgRetiradasEntregas: TGridLuka;
    sgNotasFiscais: TGridLuka;
    eClienteGeracaoCredito: TEditLuka;
    lb6: TLabel;
    lb7: TLabel;
    eCreditoPagarId: TEditLuka;
    sbInformacoesCreditoPagar: TSpeedButton;
    procedure sbInformacoesOrcamentoClick(Sender: TObject);
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure sgRetiradasEntregasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgRetiradasEntregasDblClick(Sender: TObject);
    procedure sgNotasFiscaisDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sbInformacoesCreditoPagarClick(Sender: TObject);
    procedure sgNotasFiscaisDblClick(Sender: TObject);
  end;

procedure Informar(const pDevolucaoId: Integer);

implementation

{$R *.dfm}

uses
  Informacoes.Orcamento;

const
  coProdutoId        = 0;
  coNome             = 1;
  coMarca            = 2;
  coQtdeEntregues    = 3;
  coQtdePendentes    = 4;
  coQtdeSemPrevisao  = 5;
  coUnidade          = 6;
  coPrecoUnit        = 7;
  coValorBruto       = 8;
  coValorOutDesp     = 9;
  coValorDescont     = 10;
  coValorLiquido     = 11;

  (* Grid de entregas *)
  ceCodigo             = 0;
  ceTipoMovimento      = 1;
  ceQuantidadeProdutos = 2;

  (* Grid de notas fiscais *)
  cnfNotaFiscalId  = 0;
  cnfTipoNota      = 1;
  cnfDataEmissao   = 2;
  cnfTipoMovimento = 3;
  cnfValorTotal    = 4;

procedure Informar(const pDevolucaoId: Integer);
var
  i: Integer;
  vForm: TFormInformacoesDevolucao;
  vDevolucao: TArray<RecDevolucoes>;
  vItens: TArray<RecDevolucoesItens>;

  vDevRetiradas: TArray<RecDevolucoesItensRetiradas>;
  vDevEntregas: TArray<RecDevolucoesItensEntregas>;
  vNotas: TArray<RecNotaFiscal>;
begin
  if pDevolucaoId = 0 then
    Exit;

  vDevolucao := _Devolucoes.BuscarDevolucoes(Sessao.getConexaoBanco, 0, [pDevolucaoId]);
  if vDevolucao = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vItens := _DevolucoesItens.BuscarDevolucoesItens(Sessao.getConexaoBanco, 0, [pDevolucaoId]);
  if vDevolucao = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vForm := TFormInformacoesDevolucao.Create(Application);
  vForm.ReadOnlyTodosObjetos(True);

  vForm.eDevolucaoId.AsInt := vDevolucao[0].devolucao_id;
  vForm.eOrcamentoId.SetInformacao( vDevolucao[0].orcamento_id );
  vForm.eCliente.SetInformacao( vDevolucao[0].ClienteId, vDevolucao[0].NomeCliente );
  vForm.eEmpresa.SetInformacao( vDevolucao[0].EmpresaId, vDevolucao[0].NomeEmpresa );
  vForm.eClienteGeracaoCredito.SetInformacao( vDevolucao[0].ClienteCreditoId, vDevolucao[0].NomeClienteCredito );
  vForm.eCreditoPagarId.SetInformacao( vDevolucao[0].CreditoPagarId );

  vForm.eUsuarioDevolucao.SetInformacao( vDevolucao[0].usuario_cadastro_id, vDevolucao[0].NomeUsuarioDevolucao );
  vForm.eDataHoraDevolucao.AsDataHora := vDevolucao[0].data_hora_devolucao;

  vForm.eUsuarioConfirmacao.SetInformacao( vDevolucao[0].UsuarioConfirmacaoId, vDevolucao[0].NomeUsuarioConfirmacao );
  vForm.eDataHoraConfirmacao.AsDataHora := vDevolucao[0].DataHoraConfirmacao;
  vForm.stStatus.Caption := IIfStr(vDevolucao[0].Status = 'A', 'Aberto', 'Baixado');

  vForm.eValorBruto.AsCurr          := vDevolucao[0].valor_bruto;
  vForm.eValorOutrasDespesas.AsCurr := vDevolucao[0].ValorOutrasDespesas;
  vForm.eValorDesconto.AsCurr       := vDevolucao[0].valor_desconto;
  vForm.eValorLiquido.AsCurr        := vDevolucao[0].valor_liquido;

  for i := Low(vItens) to High(vItens) do begin
    vForm.sgProdutos.Cells[coProdutoId, i + 1]        := NFormat(vItens[i].ProdutoId);
    vForm.sgProdutos.Cells[coNome, i + 1]             := vItens[i].NomeProduto;
    vForm.sgProdutos.Cells[coMarca, i + 1]            := vItens[i].NomeMarca;
    vForm.sgProdutos.Cells[coQtdeEntregues, i + 1]    := NFormatEstoque(vItens[i].QuantidadeDevolvidosEntregues);
    vForm.sgProdutos.Cells[coQtdePendentes, i + 1]    := NFormatEstoque(vItens[i].QuantidadeDevolvidosPendencias);
    vForm.sgProdutos.Cells[coQtdeSemPrevisao, i + 1]  := NFormatEstoque(vItens[i].QuantidadeDevolvSemPrevisao);
    vForm.sgProdutos.Cells[coUnidade, i + 1]          := vItens[i].UnidadeVenda;
    vForm.sgProdutos.Cells[coPrecoUnit, i + 1]        := NFormat(vItens[i].PrecoUnitario);
    vForm.sgProdutos.Cells[coValorBruto, i + 1]       := NFormat(vItens[i].ValorBruto);
    vForm.sgProdutos.Cells[coValorOutDesp, i + 1]     := NFormatN(vItens[i].ValorOutrasDespesas);
    vForm.sgProdutos.Cells[coValorDescont, i + 1]     := NFormatN(vItens[i].ValorDesconto);
    vForm.sgProdutos.Cells[coValorLiquido, i + 1]     := NFormat(vItens[i].ValorLiquido);
  end;
  vForm.sgProdutos.SetLinhasGridPorTamanhoVetor( Length(vItens) );

  vDevRetiradas := _DevolucoesItensRetiradas.BuscarDevolucoesItensRet(Sessao.getConexaoBanco, 0, [pDevolucaoId]);
  for i := Low(vDevRetiradas) to High(vDevRetiradas) do begin
    vForm.sgRetiradasEntregas.Cells[ceCodigo, i + 1]             := NFormat(vDevRetiradas[i].RetiradaId);
    vForm.sgRetiradasEntregas.Cells[ceTipoMovimento, i + 1]      := 'Retirada';
    vForm.sgRetiradasEntregas.Cells[ceQuantidadeProdutos, i + 1] := NFormatNEstoque(vDevRetiradas[i].Quantidade);
  end;

  vDevEntregas := _DevolucoesItensEntregas.BuscarDevolucoesItensEnt(Sessao.getConexaoBanco, 0, [pDevolucaoId]);
  for i := Low(vDevEntregas) to High(vDevEntregas) do begin
    vForm.sgRetiradasEntregas.Cells[ceCodigo, i + 1]             := NFormat(vDevEntregas[i].EntregaId);
    vForm.sgRetiradasEntregas.Cells[ceTipoMovimento, i + 1]      := 'Entrega';
    vForm.sgRetiradasEntregas.Cells[ceQuantidadeProdutos, i + 1] := NFormatNEstoque(vDevEntregas[i].Quantidade);
  end;
  vForm.sgRetiradasEntregas.SetLinhasGridPorTamanhoVetor( Length(vDevRetiradas) + Length(vDevEntregas) );

  vNotas := _NotasFiscais.BuscarNotasFiscais(Sessao.getConexaoBanco, 6, [pDevolucaoId]);
  for i := Low(vNotas) to High(vNotas) do begin
    vForm.sgNotasFiscais.Cells[cnfNotaFiscalId, i + 1]  := NFormat(vNotas[i].nota_fiscal_id);
    vForm.sgNotasFiscais.Cells[cnfTipoNota, i + 1]      := vNotas[i].TipoNotaAnalitico;
    vForm.sgNotasFiscais.Cells[cnfDataEmissao, i + 1]   := DHFormat(vNotas[i].data_hora_emissao);
    vForm.sgNotasFiscais.Cells[cnfTipoMovimento, i + 1] := vNotas[i].TipoMovimentoAnalitico;
    vForm.sgNotasFiscais.Cells[cnfValorTotal, i + 1]    := NFormatN(vNotas[i].valor_total);
  end;
  vForm.sgNotasFiscais.SetLinhasGridPorTamanhoVetor( Length( vNotas ) );

  vForm.ShowModal;

  vForm.Free;
end;

procedure TFormInformacoesDevolucao.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(eUsuarioDevolucao);
end;

procedure TFormInformacoesDevolucao.sbInformacoesCreditoPagarClick(Sender: TObject);
begin
  inherited;
  Informacoes.TituloPagar.Informar( eCreditoPagarId.AsInt );
end;

procedure TFormInformacoesDevolucao.sbInformacoesOrcamentoClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar(eOrcamentoId.IdInformacao);
end;

procedure TFormInformacoesDevolucao.sgNotasFiscaisDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.NotaFiscal.Informar( SFormatInt(sgNotasFiscais.Cells[cnfNotaFiscalId, sgNotasFiscais.Row]) );
end;

procedure TFormInformacoesDevolucao.sgNotasFiscaisDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[cnfNotaFiscalId, cnfValorTotal] then
    vAlinhamento := taRightJustify
  else if ACol in[cnfTipoNota, cnfTipoMovimento] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgNotasFiscais.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesDevolucao.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[
    coProdutoId,
    coQtdeEntregues,
    coQtdePendentes,
    coQtdeSemPrevisao,
    coPrecoUnit,
    coValorBruto,
    coValorOutDesp,
    coValorDescont,
    coValorLiquido]
  then
    vAlinhamento := taRightJustify
  else if ACol = coUnidade then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesDevolucao.sgRetiradasEntregasDblClick(Sender: TObject);
begin
  inherited;
  if sgRetiradasEntregas.Cells[ceTipoMovimento, sgRetiradasEntregas.Row] = 'Retirar' then
    Informacoes.Retiradas.Informar( SFormatInt( sgRetiradasEntregas.Cells[ceCodigo, sgRetiradasEntregas.Row] ) )
  else
    Informacoes.Entrega.Informar( SFormatInt( sgRetiradasEntregas.Cells[ceCodigo, sgRetiradasEntregas.Row] ) );
end;

procedure TFormInformacoesDevolucao.sgRetiradasEntregasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  vAlinhamento := taLeftJustify;

  if ACol in[ceCodigo, ceQuantidadeProdutos] then
    vAlinhamento := taRightJustify
  else if ACol = ceTipoMovimento then
    vAlinhamento := taCenter;   
  
  sgRetiradasEntregas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
