unit BuscarDadosCobrancas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.StdCtrls, System.Math,
  EditLuka, Vcl.Grids, GridLuka, Vcl.Mask, EditLukaData, _FrameHerancaPrincipal, _Biblioteca,
  _FrameHenrancaPesquisas, FrameTiposCobranca, Vcl.Buttons, Vcl.ExtCtrls, System.DateUtils,
  _RecordsFinanceiros, _RecordsCadastros, StaticTextLuka, FrameDadosCobranca;

type
  TFormBuscarDadosCobranca = class(TFormHerancaFinalizar)
    stCondicaoPagamento: TStaticText;
    stNomeCondicao: TStaticTextLuka;
    FrDadosCobranca: TFrDadosCobranca;
    procedure FormShow(Sender: TObject);
  private
    FSomenteLeitura: Boolean;
    FPrazoMedioCondicaoPagto: Currency;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function BuscarCobrancas(
  const pCondicaoId: Integer;
  const pNomeCondicao: string;
  const pTipoFormaPagto: string;
  const pValorABuscar: Double;
  const pCobrancas: TArray<RecTitulosFinanceiros>;
  const pSomenteLeitura: Boolean;
  const pPrazoMedioCondicaoPagto: Double
): TRetornoTelaFinalizar< TArray<RecTitulosFinanceiros> >;

implementation

{$R *.dfm}

uses
  _TiposCobrancaDiasPrazo, _Sessao;

function BuscarCobrancas(
  const pCondicaoId: Integer;
  const pNomeCondicao: string;
  const pTipoFormaPagto: string;
  const pValorABuscar: Double;
  const pCobrancas: TArray<RecTitulosFinanceiros>;
  const pSomenteLeitura: Boolean;
  const pPrazoMedioCondicaoPagto: Double
): TRetornoTelaFinalizar< TArray<RecTitulosFinanceiros> >;
var
  vForm: TFormBuscarDadosCobranca;
begin
  Result.Dados := nil;

  vForm := TFormBuscarDadosCobranca.Create(Application);

  vForm.FSomenteLeitura := pSomenteLeitura;
  vForm.FrDadosCobranca.Modo(not pSomenteLeitura);

  vForm.FrDadosCobranca.Natureza := 'R';
  vForm.FrDadosCobranca.FormaPagamento := 'COB';
  vForm.FrDadosCobranca.FrTiposCobranca.setCondicaoId(pCondicaoId);
  vForm.FrDadosCobranca.ValorPagar          := pValorABuscar;
  vForm.FrDadosCobranca.PrazoMedioPermitido := pPrazoMedioCondicaoPagto;
  vForm.FPrazoMedioCondicaoPagto            := pPrazoMedioCondicaoPagto;

  if pCondicaoId > 0 then
    vForm.stCondicaoPagamento.Caption := IntToStr(pCondicaoId) + ' - ' + pNomeCondicao
  else begin
    vForm.stNomeCondicao.Visible := False;
    vForm.stCondicaoPagamento.Visible := False;
    vForm.Height := 370;
  end;

  vForm.FrDadosCobranca.Titulos := pCobrancas;

  if Result.Ok(vForm.ShowModal, False) then
    Result.Dados := vForm.FrDadosCobranca.Titulos;

  FreeAndNil(vForm);
end;

procedure TFormBuscarDadosCobranca.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco( FrDadosCobranca );
end;

procedure TFormBuscarDadosCobranca.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrDadosCobranca.ExisteDiferenca then begin
    _Biblioteca.Exclamar('Existe uma diferenša nos valores, por favor verifique!');
    Abort;
  end;

  FrDadosCobranca.verificarPrazoMedio;
end;

end.
