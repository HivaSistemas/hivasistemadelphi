unit SugestaoCompras;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _Sessao, _ComprasItens,
  FrameProdutos, FrameMarcas, FrameFornecedores, _FrameHerancaPrincipal, _SugestaoCompras,
  _FrameHenrancaPesquisas, Frame.DepartamentosSecoesLinhas, Vcl.ComCtrls, _CalculosSugestaoCompras,
  Vcl.Buttons, Vcl.ExtCtrls, FrameEmpresas, Vcl.Grids, GridLuka, _Biblioteca, System.DateUtils,
  FrameDataInicialFinal, Vcl.StdCtrls, EditLuka, GroupBoxLuka, CheckBoxLuka, CadastroCompras,
  Vcl.Menus, _ProdutosMultiplosCompras, _RecordsCadastros, FrameLocais;

type
  TFormSugestaoCompras = class(TFormHerancaRelatoriosPageControl)
    FrFornecedores: TFrFornecedores;
    FrMarcas: TFrMarcas;
    FrProdutos: TFrProdutos;
    sgItens: TGridLuka;
    FrEmpresas: TFrEmpresas;
    ckTrazerApenasItensSugestao: TCheckBoxLuka;
    ckTrazerSugestaoBaseadaMovimentacoesFisicas: TCheckBoxLuka;
    GroupBoxLuka1: TGroupBoxLuka;
    eDiasSuprimento: TEditLuka;
    Label1: TLabel;
    ePercentualCrescimento: TEditLuka;
    Label2: TLabel;
    gbMeses: TGroupBoxLuka;
    ckMesAtual: TCheckBoxLuka;
    ckMes1: TCheckBoxLuka;
    ckMes2: TCheckBoxLuka;
    ckMes3: TCheckBoxLuka;
    ckMes4: TCheckBoxLuka;
    ckMes5: TCheckBoxLuka;
    ckMes6: TCheckBoxLuka;
    ckMes7: TCheckBoxLuka;
    ckMes8: TCheckBoxLuka;
    ckMes9: TCheckBoxLuka;
    ckMes10: TCheckBoxLuka;
    ckMes11: TCheckBoxLuka;
    pmOpcoes: TPopupMenu;
    pcInformacoes: TPageControl;
    tsInformacoesProduto: TTabSheet;
    tsMovimentacoes: TTabSheet;
    sgMovimentacoesMensais: TGridLuka;
    st4: TStaticText;
    stNomeCompra: TStaticText;
    st1: TStaticText;
    stCodigoBarras: TStaticText;
    st2: TStaticText;
    stCodigoOriginal: TStaticText;
    st5: TStaticText;
    stUnidadeVenda: TStaticText;
    stMultiploVenda: TStaticText;
    st6: TStaticText;
    pn1: TPanel;
    st3: TStaticText;
    stDiasUteisPeridoSelecionado: TStaticText;
    st8: TStaticText;
    stDiasUteisCalculoSuprimento: TStaticText;
    sgMultiplosCompra: TGridLuka;
    st7: TStaticText;
    stFabricante: TStaticText;
    st10: TStaticText;
    stMarca: TStaticText;
    miRealizarPedidoCompra: TSpeedButton;
    TabSheet1: TTabSheet;
    sgSugestaoLocais: TGridLuka;
    FrLocais: TFrLocais;
    procedure FormCreate(Sender: TObject);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure miRealizarPedidoCompraClick(Sender: TObject);
    procedure sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
    procedure sgMovimentacoesMensaisDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensClick(Sender: TObject);
    procedure sgMultiplosCompraDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgMultiplosCompraDblClick(Sender: TObject);
    procedure sgSugestaoLocaisDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
  private
    FMovimentacoesMesAno: TArray<RecMovimentacoes>;
    FMultiplosCompraProdutos: TArray<RecProdutoMultiploCompra>;

    procedure CalcularTotalCompra(pLinha: Integer);
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormSugestaoCompras }

const
  (* Grid de sugest�es *)
  coLegenda          = 0;
  coProdutoId        = 1;
  coNome             = 2;
  coMediaDiaria      = 3;
  coPrevVenMensal    = 4;
  coEstoque          = 5;
  coDiasEstoque      = 6;
  coPendencias       = 7;
  coSugestaoCompra   = 8;
  coUnidadeVenda     = 9;
  coPrecoTabela      = 10;
  coQtdeComprar      = 11;
  coUnidadeComprar   = 12;
  coPrecoComprar     = 13;
  coValorTotComprar  = 14;
  coDtUltEntrada     = 15;
  coFornUltEntrada   = 16;

  (* Ocultas *)
  coNomeCompra      = 17;
  coCodigoBarras    = 18;
  coCodigoOrigFabr  = 19;
  coMultiploVenda   = 20;
  coMultiploCompra  = 21;
  coQtdeEmbalagem   = 22;
  coFornecedorId    = 23;
  coNomeFornecedor  = 24;
  coNomeMarca       = 25;

  (* Grid de movimentacoes mensais *)
  cmMesAno = 0;
  cmMes1   = 1;
  cmMes2   = 2;
  cmMes3   = 3;
  cmMes4   = 4;
  cmMes5   = 5;
  cmMes6   = 6;
  cmMes7   = 7;
  cmMes8   = 8;
  cmMes9   = 9;
  cmMes10  = 10;
  cmMes11  = 11;
  cmMes12  = 12;
  cmMes13  = 13;

  (* Grid de multiplos de compra *)
  cmUnidadeCompra  = 0;
  cmMultiploCompra = 1;
  cmQtdeEmbalagem  = 2;

  cmLinhaMesAno         = 0;
  cmLinhaQtdeVendidos   = 1;
  cmLinhaQtdeDevolvidos = 2;
  cmLinhaQtdeCompras    = 3;

  cuLocalId       = 0;
  cuEstoque       = 1;
  cuVendasPeriodo = 2;
  cuSugestao      = 3;

procedure TFormSugestaoCompras.FormCreate(Sender: TObject);

  procedure preencherMeses(pCheckBox: array of TCheckBoxLuka);
  var
    i: Integer;
    vData: string;
    vDataBase: TDateTime;
  begin
    vDataBase := Sessao.getDataHora;
    for i := Low(pCheckBox) to High(pCheckBox) do begin
      vData := _Biblioteca.MesAno( IncMonth( vDataBase, i * -1 ) );
      pCheckBox[i].Caption := vData;
      pCheckBox[i].Valor := _Biblioteca.RetornaNumeros(vData);
    end;
  end;

  procedure preencherMesesMovimentacoes(pColunasGrid: TArray<Integer>);
  var
    i: Integer;
    vData: string;
    vDataBase: TDateTime;
  begin
    vDataBase := Sessao.getDataHora;
    for i := Low(pColunasGrid) to High(pColunasGrid) do begin
      vData := _Biblioteca.MesAno( IncMonth( vDataBase, i * -1 ) );
      sgMovimentacoesMensais.Cells[pColunasGrid[i], cmLinhaMesAno] := vData;
    end;
  end;

begin
  inherited;

  preencherMeses([
    ckMesAtual,
    ckMes1,
    ckMes2,
    ckMes3,
    ckMes4,
    ckMes5,
    ckMes6,
    ckMes7,
    ckMes8,
    ckMes9,
    ckMes10,
    ckMes11
    //ckMes12,
    //ckMes13
    //ckMes14
  ]);

  preencherMesesMovimentacoes([
    cmMes1,
    cmMes2,
    cmMes3,
    cmMes4,
    cmMes5,
    cmMes6,
    cmMes7,
    cmMes8,
    cmMes9,
    cmMes10,
    cmMes11,
    cmMes12,
    cmMes13
  ]);

  FrEmpresas.InserirDadoPorChave( Sessao.getEmpresaLogada.EmpresaId, False );

  sgMovimentacoesMensais.Col := cmMes1;
end;

procedure TFormSugestaoCompras.sgSugestaoLocaisDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = cuLocalId then
    vAlinhamento := taLeftJustify
  else
    vAlinhamento := taRightJustify;

  sgSugestaoLocais.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormSugestaoCompras.Imprimir(Sender: TObject);
begin
  inherited;

end;

procedure TFormSugestaoCompras.miRealizarPedidoCompraClick(Sender: TObject);
var
  i: Integer;
  vItens: TArray<RecComprasItens>;

  vRetTela: TRetornoTelaFinalizar;
begin
  inherited;

  vItens := nil;
  for i := 1 to sgItens.RowCount -1 do begin
    if SFormatCurr(sgItens.Cells[coQtdeComprar, i]) = 0 then
      Continue;

    SetLength(vItens, Length(vItens) + 1);

    vItens[High(vItens)].ProdutoId       := SFormatInt(sgItens.Cells[coProdutoId, i]);
    vItens[High(vItens)].NomeProduto     := sgItens.Cells[coNome, i];
    vItens[High(vItens)].Quantidade      := SFormatDouble(sgItens.Cells[coQtdeComprar, i]);
    vItens[High(vItens)].PrecoUnitario   := SFormatDouble(sgItens.Cells[coPrecoComprar, i]);
    vItens[High(vItens)].ValorLiquido    := SFormatDouble(sgItens.Cells[coValorTotComprar, i]);
    vItens[High(vItens)].ValorTotal      := SFormatCurr(sgItens.Cells[coValorTotComprar, i]);

    vItens[High(vItens)].NomeMarca           := sgItens.Cells[coNomeMarca, i];
    vItens[High(vItens)].UnidadeCompraId     := sgItens.Cells[coUnidadeComprar, i];
    vItens[High(vItens)].MultiploCompra      := SFormatDouble(sgItens.Cells[coMultiploCompra, i]);
    vItens[High(vItens)].QuantidadeEmbalagem := SFormatDouble(sgItens.Cells[coQtdeEmbalagem, i]);

    if vItens[High(vItens)].ValorTotal = 0 then begin
      _Biblioteca.Exclamar('O pre�o do produto ' + sgItens.Cells[coNome, i] + ' n�o foi definido, verifique!');
      sgItens.Row := i;
      sgItens.Col := coPrecoComprar;
      SetarFoco(sgItens);
      Abort;
    end;
  end;

  if vItens = nil then begin
    _Biblioteca.Exclamar('Nenhum produto teve a quantidade a comprar informada!');
    Exit;
  end;

  vRetTela := CadastroCompras.GerarCompraViaSugestaoCompras(vItens);
  if vRetTela.BuscaCancelada then
    Exit;

  Carregar(Sender);
end;

procedure TFormSugestaoCompras.sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coQtdeComprar then begin
    TextCell := NFormatNEstoque(SFormatDouble(TextCell));
    CalcularTotalCompra(ARow);
  end
  else if ACol = coPrecoComprar then begin
    TextCell := NFormatN(SFormatDouble(TextCell));
    CalcularTotalCompra(ARow);
  end;
end;

procedure TFormSugestaoCompras.sgItensClick(Sender: TObject);
var
  i: Integer;
  vPosicCol: Integer;
  vProdutoId: Integer;
  vSql: string;

  vLinha: Integer;

  vItens: TArray<RecItensSugestao>;
  vQtdeDiasUteisPeriodoSelecionado: Integer;
  vQtdeDiasUteisSuprimento: Integer;

  function getData(pInicial: Boolean): TArray<TDate>;
  var
    i: Integer;
  begin
    Result := nil;
    for i := 0 to gbMeses.ControlCount -1 do begin
      if TCheckBoxLuka(gbMeses.Controls[i]).Checked then begin
        SetLength(Result, Length(Result) + 1);

        if pInicial then
          Result[i] := ToData('01/' + TCheckBoxLuka(gbMeses.Controls[i]).Caption)
        else
          Result[i] := EndOfTheMonth( ToData('01/' + TCheckBoxLuka(gbMeses.Controls[i]).Caption) );
      end;
    end;
  end;
begin
  inherited;

  sgMultiplosCompra.ClearGrid;

  stNomeCompra.Caption      := ' ' + sgItens.Cells[coNomeCompra, sgItens.Row];
  stFabricante.Caption      := ' ' + getInformacao(sgItens.Cells[coFornecedorId, sgItens.Row], sgItens.Cells[coNomeFornecedor, sgItens.Row]);
  stMarca.Caption           := ' ' + sgItens.Cells[coNomeMarca, sgItens.Row];
  stCodigoBarras.Caption    := ' ' + sgItens.Cells[coCodigoBarras, sgItens.Row];
  stCodigoOriginal.Caption  := ' ' + sgItens.Cells[coCodigoOrigFabr, sgItens.Row];
  stUnidadeVenda.Caption    := ' ' + sgItens.Cells[coUnidadeVenda, sgItens.Row];
  stMultiploVenda.Caption   := ' ' + sgItens.Cells[coMultiploVenda, sgItens.Row];  

  for i := 1 to sgMovimentacoesMensais.ColCount do begin
    sgMovimentacoesMensais.Cells[i, cmLinhaQtdeVendidos]   := '';
    sgMovimentacoesMensais.Cells[i, cmLinhaQtdeDevolvidos] := '';    
    sgMovimentacoesMensais.Cells[i, cmLinhaQtdeCompras]    := '';
  end;  

  vProdutoId := SFormatInt(sgItens.Cells[coProdutoId, sgItens.Row]);
  if vProdutoId = 0 then
    Exit;

  for i := Low(FMovimentacoesMesAno) to High(FMovimentacoesMesAno) do begin
    if vProdutoId <> FMovimentacoesMesAno[i].ProdutoId then
      Continue;

    vPosicCol := sgMovimentacoesMensais.LocalizarColuna(cmLinhaMesAno, FMovimentacoesMesAno[i].AnoMes);
    if vPosicCol = -1 then
      Continue;

    sgMovimentacoesMensais.Cells[vPosicCol, cmLinhaQtdeVendidos]   := NFormatNEstoque(FMovimentacoesMesAno[i].QtdeVendida);
    sgMovimentacoesMensais.Cells[vPosicCol, cmLinhaQtdeDevolvidos] := NFormatNEstoque(FMovimentacoesMesAno[i].QtdeDevolvida);
    sgMovimentacoesMensais.Cells[vPosicCol, cmLinhaQtdeCompras]    := NFormatNEstoque(FMovimentacoesMesAno[i].QtdeComprada);
  end;

  vLinha := 0;
  for i := Low(FMultiplosCompraProdutos) to High(FMultiplosCompraProdutos) do begin
    if vProdutoId <> FMultiplosCompraProdutos[i].ProdutoId then
      Continue;

    Inc(vLinha);
  
    sgMultiplosCompra.Cells[cmUnidadeCompra, vLinha]  := FMultiplosCompraProdutos[i].UnidadeId;
    sgMultiplosCompra.Cells[cmMultiploCompra, vLinha] := NFormatNEstoque(FMultiplosCompraProdutos[i].Multiplo);
    sgMultiplosCompra.Cells[cmQtdeEmbalagem, vLinha]  := NFormatNEstoque(FMultiplosCompraProdutos[i].QuantidadeEmbalagem);
  end;
  sgMultiplosCompra.SetLinhasGridPorTamanhoVetor( vLinha );

  vSql := ' where ' + gbMeses.GetSql('to_char(VWS.DATA_HORA_MOVIMENTO, ''mmyyyy'')');

  vSql := vSql + ' and VWS.PRODUTO_ID = ' + IntToStr(vProdutoId);

  vSql := vSql + ' and ' + FrEmpresas.getSqlFiltros('VWS.EMPRESA_ID');

  if not FrLocais.EstaVazio then
    FrLocais.getSqlFiltros('VWS.LOCAL_ID', vSql, True);

  vItens := _SugestaoCompras.BuscarDadosPorLocais(Sessao.getConexaoBanco, vSql);
  if vItens = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vItens :=
    _CalculosSugestaoCompras.CalcularSugestao(
      FrEmpresas.getEmpresa(0).EmpresaId,
      ckTrazerApenasItensSugestao.Checked,
      eDiasSuprimento.AsInt,
      ePercentualCrescimento.AsDouble,
      getData(True),
      getData(False),
      vItens,
      vQtdeDiasUteisPeriodoSelecionado,
      vQtdeDiasUteisSuprimento
    );

  stDiasUteisPeridoSelecionado.Caption := '  ' + NFormat(vQtdeDiasUteisPeriodoSelecionado);
  stDiasUteisCalculoSuprimento.Caption := '  ' + NFormat(vQtdeDiasUteisSuprimento);

  if vItens = nil then begin
    NenhumRegistro;
    Exit;
  end;

  sgSugestaoLocais.ClearGrid;
  for i := 0 to Length(vItens) - 1 do begin
    sgSugestaoLocais.Cells[cuLocalId, i + 1]       := NFormat(vItens[i].LocalId) + ' - ' + vItens[i].NomeLocal;
    sgSugestaoLocais.Cells[cuEstoque, i + 1]       := NFormat(vItens[i].Estoque);
    sgSugestaoLocais.Cells[cuVendasPeriodo, i + 1] := NFormat(vItens[i].VendasPeriodo);
    sgSugestaoLocais.Cells[cuSugestao, i + 1]      := NFormat(vItens[i].SugestaoCompra);
  end;
  sgSugestaoLocais.SetLinhasGridPorTamanhoVetor( Length(vItens) );
end;

procedure TFormSugestaoCompras.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coLegenda, coUnidadeVenda, coUnidadeComprar] then
    vAlinhamento := taCenter
  else if ACol in[
    coProdutoId,
    coMediaDiaria,
    coPrevVenMensal,
    coEstoque,
    coDiasEstoque,
    coPendencias,
    coSugestaoCompra,
    coQtdeComprar,
    coPrecoTabela,
    coPrecoComprar,
    coValorTotComprar]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormSugestaoCompras.sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coQtdeComprar then begin
    AFont.Color := _Biblioteca.coCorFonteEdicao2;
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
  end
  else if ACol = coPrecoComprar then begin
    AFont.Color := _Biblioteca.coCorFonteEdicao3;
    ABrush.Color := _Biblioteca.coCorCelulaEdicao3;
  end;
end;

procedure TFormSugestaoCompras.sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol in[coQtdeComprar, coPrecoComprar] then
    sgItens.Options := sgItens.Options + [goEditing]
  else
    sgItens.Options := sgItens.Options - [goEditing];
end;

procedure TFormSugestaoCompras.sgMovimentacoesMensaisDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  vAlinhamento := taRightJustify;

  sgMovimentacoesMensais.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormSugestaoCompras.sgMultiplosCompraDblClick(Sender: TObject);
var
  vQtdeComprarNovoMultiplo: Currency;
begin
  inherited;

  if SFormatCurr(sgItens.Cells[coSugestaoCompra, sgItens.Row]) = 0 then
    Exit;

  vQtdeComprarNovoMultiplo := 
    SFormatDouble(sgItens.Cells[coSugestaoCompra, sgItens.Row]) / SFormatDouble(sgMultiplosCompra.Cells[cmQtdeEmbalagem, sgMultiplosCompra.Row]);

  // Ajustando ao multiplo de compra da unidade que esta sendo selecionada
  vQtdeComprarNovoMultiplo := _Biblioteca.RetornaValorDeAcordoMultiplo( vQtdeComprarNovoMultiplo, SFormatDouble(sgMultiplosCompra.Cells[cmMultiploCompra, sgMultiplosCompra.Row]) );

  if vQtdeComprarNovoMultiplo = 0 then
    Exit;

  sgItens.Cells[coQtdeComprar, sgItens.Row]    := NFormatNEstoque(vQtdeComprarNovoMultiplo);
  sgItens.Cells[coUnidadeComprar, sgItens.Row] := sgMultiplosCompra.Cells[cmUnidadeCompra, sgMultiplosCompra.Row];
  sgItens.Cells[coMultiploCompra, sgItens.Row] := sgMultiplosCompra.Cells[cmMultiploCompra, sgMultiplosCompra.Row];
  sgItens.Cells[coQtdeEmbalagem, sgItens.Row]  := sgMultiplosCompra.Cells[cmQtdeEmbalagem, sgMultiplosCompra.Row];

  // Convertendo o pre�o para o pre�o da nova unidade
  sgItens.Cells[coPrecoComprar, sgItens.Row]   := 
    NFormatN(SFormatDouble(sgItens.Cells[coPrecoTabela, sgItens.Row]) * SFormatDouble(sgMultiplosCompra.Cells[cmQtdeEmbalagem, sgMultiplosCompra.Row]));

  CalcularTotalCompra(sgItens.Row);
end;

procedure TFormSugestaoCompras.sgMultiplosCompraDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[cmMultiploCompra, cmQtdeEmbalagem] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgMultiplosCompra.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormSugestaoCompras.VerificarRegistro(Sender: TObject);
begin
  inherited;
  FMultiplosCompraProdutos := nil;
  
  _Biblioteca.LimparCampos([
    stNomeCompra, 
    stCodigoBarras, 
    stCodigoOriginal, 
    stUnidadeVenda,
    stDiasUteisPeridoSelecionado,
    stDiasUteisCalculoSuprimento,
    sgItens,
    sgMovimentacoesMensais
  ]);

  if FrEmpresas.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio informar ao menos uma empresa para sugest�o de compras!');
    SetarFoco(FrEmpresas);
    Abort;
  end;

  if eDiasSuprimento.AsInt = 0 then begin
    _Biblioteca.Exclamar('� necess�rio informar o n�mero de dias de suprimento para c�culo da sugest�o!');
    SetarFoco(eDiasSuprimento);
    Abort;
  end;

  if gbMeses.NenhumMarcado then begin
    _Biblioteca.Exclamar('� necess�rio selecionar ao menos 1 m�s para m�dia de vendas!');
    SetarFoco(gbMeses);
    Abort;
  end;
end;

procedure TFormSugestaoCompras.CalcularTotalCompra(pLinha: Integer);
begin
  sgItens.Cells[coValorTotComprar, pLinha] := NFormatN( SFormatDouble(sgItens.Cells[coQtdeComprar, pLinha]) * SFormatDouble(sgItens.Cells[coPrecoComprar, pLinha]) );
end;

procedure TFormSugestaoCompras.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vProdutosIds: TArray<Integer>;
  vItens: TArray<RecItensSugestao>;

  vQtdeDiasUteisPeriodoSelecionado: Integer;
  vQtdeDiasUteisSuprimento: Integer;
  MyClass: TComponent;

  function getData(pInicial: Boolean): TArray<TDate>;
  var
    i: Integer;
  begin
    Result := nil;
    for i := 0 to gbMeses.ControlCount -1 do begin
      if TCheckBoxLuka(gbMeses.Controls[i]).Checked then begin
        SetLength(Result, Length(Result) + 1);

        if pInicial then
          Result[i] := ToData('01/' + TCheckBoxLuka(gbMeses.Controls[i]).Caption)
        else
          Result[i] := EndOfTheMonth( ToData('01/' + TCheckBoxLuka(gbMeses.Controls[i]).Caption) );
      end;
    end;
  end;

begin
  inherited;
  vProdutosIds := nil;

  vSql := ' and ' + FrEmpresas.getSqlFiltros('EST.EMPRESA_ID');

  if not FrProdutos.EstaVazio then
    vSql := vSql + ' and ' + FrProdutos.getSqlFiltros('PRO.PRODUTO_ID');

  if not FrFornecedores.EstaVazio then
    vSql := vSql + ' and ' + FrProdutos.getSqlFiltros('PRO.FORNECEDOR_ID');

  if not FrMarcas.EstaVazio then
    vSql := vSql + ' and ' + FrMarcas.getSqlFiltros('PRO.MARCA_ID');

  vSql := vSql + ' and ' + gbMeses.GetSql('to_char(VEN.DATA_HORA_MOVIMENTO, ''mmyyyy'')');

  vItens := _SugestaoCompras.BuscarDados(Sessao.getConexaoBanco, vSql);
  if vItens = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vItens :=
    _CalculosSugestaoCompras.CalcularSugestao(
      FrEmpresas.getEmpresa(0).EmpresaId,
      ckTrazerApenasItensSugestao.Checked,
      eDiasSuprimento.AsInt,
      ePercentualCrescimento.AsDouble,
      getData(True),
      getData(False),
      vItens,
      vQtdeDiasUteisPeriodoSelecionado,
      vQtdeDiasUteisSuprimento
    );

  stDiasUteisPeridoSelecionado.Caption := '  ' + NFormat(vQtdeDiasUteisPeriodoSelecionado);
  stDiasUteisCalculoSuprimento.Caption := '  ' + NFormat(vQtdeDiasUteisSuprimento);  

  if vItens = nil then begin
    NenhumRegistro;
    Exit;
  end;


  for i := Low(vItens) to High(vItens) do begin
    sgItens.Cells[coProdutoId, i + 1]           := NFormat(vItens[i].ProdutoId);
    sgItens.Cells[coNome, i + 1]                := vItens[i].Nome;
    sgItens.Cells[coMediaDiaria, i + 1]         := NFormat(vItens[i].MediaVendasDiaria, 4);
    sgItens.Cells[coPrevVenMensal, i + 1]       := '';
    sgItens.Cells[coEstoque, i + 1]             := NFormatNEstoque(vItens[i].Estoque);
    sgItens.Cells[coDiasEstoque, i + 1]         := NFormatN(vItens[i].DiasEstoque);    
    sgItens.Cells[coPendencias, i + 1]          := NFormatNEstoque(vItens[i].ComprasPendentes);
    sgItens.Cells[coSugestaoCompra, i + 1]      := NFormatNEstoque(vItens[i].SugestaoCompra);
    sgItens.Cells[coQtdeComprar, i + 1]         := NFormatNEstoque(vItens[i].SugestaoCompra);
    sgItens.Cells[coUnidadeComprar    , i + 1]  := vItens[i].UnidadeVenda;
    sgItens.Cells[coPrecoTabela, i + 1]         := NFormat(vItens[i].PrecoFinal);
    sgItens.Cells[coPrecoComprar, i + 1]        := NFormat(vItens[i].PrecoFinal);
    sgItens.Cells[coNomeCompra, i + 1]          := vItens[i].NomeCompra;
    sgItens.Cells[coCodigoBarras, i + 1]        := vItens[i].CodigoBarras;
    sgItens.Cells[coCodigoOrigFabr, i + 1]      := vItens[i].CodigoOriginalFabricante;
    sgItens.Cells[coUnidadeVenda, i + 1]        := vItens[i].UnidadeVenda;
    sgItens.Cells[coMultiploVenda, i + 1]       := NFormatNEstoque(vItens[i].MultiploVenda);
    sgItens.Cells[coMultiploCompra, i + 1]      := NFormatNEstoque(1);
    sgItens.Cells[coQtdeEmbalagem, i + 1]       := NFormatNEstoque(1);
    sgItens.Cells[coFornecedorId, i + 1]        := NFormatN(vItens[i].FornecedorId);
    sgItens.Cells[coNomeFornecedor, i + 1]      := vItens[i].NomeFornecedor;
    sgItens.Cells[coNomeMarca, i + 1]           := vItens[i].NomeMarca;

    CalcularTotalCompra(i + 1);

    _Biblioteca.AddNoVetorSemRepetir(vProdutosIds, vItens[i].ProdutoId);
  end;
  sgItens.SetLinhasGridPorTamanhoVetor( Length(vItens) );
  
  FMovimentacoesMesAno     := _SugestaoCompras.BuscarMovimentacoesProdutos(Sessao.getConexaoBanco, FrEmpresas.GetArrayOfInteger, vProdutosIds);

  FMultiplosCompraProdutos := 
    _ProdutosMultiplosCompras.BuscarProdutoMultiploComprasComando(
      Sessao.getConexaoBanco, 
      'where ' + FiltroInInt('PRODUTO_ID', vProdutosIds) + ' ' +
      'order by ' +
      '  ORDEM '
    );

  SetarFoco(sgItens);
  sgItensClick(nil);
end;

end.
