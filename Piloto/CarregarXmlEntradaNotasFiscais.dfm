inherited FormCarregarXmlEntradaNotasFiscais: TFormCarregarXmlEntradaNotasFiscais
  Caption = 'Carregar XML para entrada de notas fiscais'
  ClientHeight = 671
  ClientWidth = 1010
  OnKeyDown = nil
  OnShow = FormShow
  ExplicitWidth = 1016
  ExplicitHeight = 700
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 634
    Width = 1010
    TabOrder = 4
    ExplicitTop = 634
    ExplicitWidth = 1010
  end
  inline FrCaminhoXML: TFrCaminhoArquivo
    Left = -2
    Top = 1
    Width = 443
    Height = 43
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = -2
    ExplicitTop = 1
    ExplicitWidth = 443
    ExplicitHeight = 43
    inherited lb1: TLabel
      Top = 0
      Width = 69
      Height = 14
      Caption = 'Carregar XML'
      ExplicitTop = 0
      ExplicitWidth = 69
      ExplicitHeight = 14
    end
    inherited sbPesquisarArquivo: TSpeedButton
      Left = 417
      Top = 15
      OnClick = FrCaminhoXMLsbPesquisarArquivoClick
      ExplicitLeft = 124
      ExplicitTop = 15
    end
    inherited eCaminhoArquivo: TEditLuka
      Top = 15
      Width = 411
      Height = 22
      ExplicitTop = 15
      ExplicitWidth = 411
      ExplicitHeight = 22
    end
  end
  inline FrFornecedor: TFrFornecedores
    Left = 453
    Top = -1
    Width = 370
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 0
    TabStop = True
    ExplicitLeft = 453
    ExplicitTop = -1
    ExplicitWidth = 370
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 345
      Height = 23
      TabOrder = 1
      ExplicitWidth = 345
      ExplicitHeight = 23
    end
    inherited CkAspas: TCheckBox
      TabOrder = 3
    end
    inherited CkFiltroDuplo: TCheckBox
      TabOrder = 6
    end
    inherited CkPesquisaNumerica: TCheckBox
      TabOrder = 9
    end
    inherited PnTitulos: TPanel
      Width = 370
      TabOrder = 0
      ExplicitWidth = 370
      inherited lbNomePesquisa: TLabel
        Width = 61
        Height = 14
        Caption = 'Fornecedor'
        ExplicitWidth = 61
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 265
        ExplicitLeft = 265
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 345
      Height = 24
      TabOrder = 2
      ExplicitLeft = 345
      ExplicitHeight = 24
    end
    inherited ckRevenda: TCheckBox
      TabOrder = 10
    end
    inherited ckUsoConsumo: TCheckBox
      TabOrder = 5
    end
    inherited ckServico: TCheckBox
      TabOrder = 8
    end
  end
  object ckNaoEscriturarItens: TCheckBoxLuka
    Left = 832
    Top = 15
    Width = 175
    Height = 17
    Caption = 'N'#227'o escriturar itens'
    TabOrder = 2
    Visible = False
    CheckedStr = 'N'
  end
  object pcDados: TPageControlAltis
    Left = 0
    Top = 50
    Width = 1011
    Height = 585
    ActivePage = tsDadosNota
    Align = alCustom
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    CarregouAba = False
    object tsDadosNota: TTabSheet
      Caption = 'Dados da Nota'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      DesignSize = (
        1003
        557)
      object lbl2: TLabel
        Left = 475
        Top = 84
        Width = 59
        Height = 13
        Caption = 'Raz'#227'o social'
      end
      object lb1: TLabel
        Left = 475
        Top = 125
        Width = 25
        Height = 13
        Caption = 'CNPJ'
      end
      object lb2: TLabel
        Left = 651
        Top = 125
        Width = 10
        Height = 13
        Caption = 'IE'
      end
      object lb3: TLabel
        Left = 475
        Top = 168
        Width = 45
        Height = 13
        Caption = 'Endere'#231'o'
      end
      object lb4: TLabel
        Left = 827
        Top = 125
        Width = 42
        Height = 13
        Caption = 'Telefone'
      end
      object lb5: TLabel
        Left = 0
        Top = 264
        Width = 44
        Height = 13
        Caption = 'N'#250'mero'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbMod: TLabel
        Left = 176
        Top = 264
        Width = 29
        Height = 13
        Caption = 'S'#233'rie'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lb7: TLabel
        Left = 298
        Top = 264
        Width = 79
        Height = 13
        Caption = 'Data de emiss'#227'o'
      end
      object lb6: TLabel
        Left = 383
        Top = 264
        Width = 66
        Height = 13
        Caption = 'Data de sa'#237'da'
      end
      object lb8: TLabel
        Left = 237
        Top = 264
        Width = 34
        Height = 13
        Caption = 'Modelo'
      end
      object lb9: TLabel
        Left = 1
        Top = 306
        Width = 82
        Height = 13
        Caption = 'Chave de acesso'
      end
      object lb10: TLabel
        Left = 327
        Top = 306
        Width = 86
        Height = 13
        Caption = 'Base c'#225'lculo ICMS'
      end
      object lb11: TLabel
        Left = 423
        Top = 306
        Width = 52
        Height = 13
        Caption = 'Valor ICMS'
      end
      object lb12: TLabel
        Left = 517
        Top = 306
        Width = 73
        Height = 13
        Caption = 'Base c'#225'lculo ST'
      end
      object lb13: TLabel
        Left = 614
        Top = 306
        Width = 39
        Height = 13
        Caption = 'Valor ST'
      end
      object lb15: TLabel
        Left = 710
        Top = 306
        Width = 41
        Height = 13
        Caption = 'Valor IPI'
      end
      object lb14: TLabel
        Left = 3
        Top = 349
        Width = 70
        Height = 13
        Caption = 'Valor produtos'
      end
      object lb16: TLabel
        Left = 98
        Top = 349
        Width = 51
        Height = 13
        Caption = 'Valor frete'
      end
      object lb17: TLabel
        Left = 193
        Top = 349
        Width = 58
        Height = 13
        Caption = 'Valor outras'
      end
      object lb18: TLabel
        Left = 289
        Top = 349
        Width = 71
        Height = 13
        Caption = 'Valor desconto'
      end
      object lb19: TLabel
        Left = 383
        Top = 349
        Width = 89
        Height = 13
        Caption = 'Valor total da nota'
      end
      object lb20: TLabel
        Left = 481
        Top = 349
        Width = 52
        Height = 13
        Caption = 'Peso bruto'
      end
      object lb21: TLabel
        Left = 576
        Top = 349
        Width = 56
        Height = 13
        Caption = 'Peso liquido'
      end
      object lb22: TLabel
        Left = 806
        Top = 306
        Width = 40
        Height = 13
        Caption = 'Valor pis'
      end
      object lb23: TLabel
        Left = 902
        Top = 306
        Width = 57
        Height = 13
        Caption = 'Valor Cofins'
      end
      object lb24: TLabel
        Left = 466
        Top = 264
        Width = 107
        Height = 13
        Caption = 'Natureza da opera'#231#227'o'
      end
      object lb25: TLabel
        Left = 3
        Top = 17
        Width = 59
        Height = 13
        Caption = 'Raz'#227'o social'
      end
      object lb26: TLabel
        Left = 703
        Top = 16
        Width = 10
        Height = 13
        Caption = 'IE'
      end
      object lb27: TLabel
        Left = 528
        Top = 16
        Width = 25
        Height = 13
        Caption = 'CNPJ'
      end
      object lb28: TLabel
        Left = 676
        Top = 349
        Width = 62
        Height = 13
        Caption = 'Tipo de frete'
      end
      object st13: TStaticTextLuka
        Left = 0
        Top = 63
        Width = 474
        Height = 15
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Financeiro'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 4
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object st14: TStaticTextLuka
        Left = 472
        Top = 63
        Width = 537
        Height = 15
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Dados do emitente'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object st16: TStaticTextLuka
        Left = -2
        Top = 243
        Width = 1004
        Height = 15
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Dados da Nota'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 13
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object stInformacoesComplementares: TStaticTextLuka
        Left = 0
        Top = 497
        Width = 1003
        Height = 60
        Align = alBottom
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Informacoes Complementares'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 36
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object sgCobrancas: TGridLuka
        Left = 0
        Top = 81
        Width = 466
        Height = 162
        Anchors = [akLeft, akTop, akBottom]
        ColCount = 15
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 6
        OnDrawCell = sgCobrancasDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Cobr.'
          'Descri'#231#227'o'
          'Valor'
          'Documento'
          'Parc.'
          'Nr.parc.'
          'Vencimento'
          'C'#243'd. barras'
          'Nosso n'#250'mero'
          'Banco'
          'Ag'#234'ncia'
          'Conta corrente'
          'Num.cheque'
          'Nome emitente'
          'CPF/CNPJ emitente'
          'Telefone')
        Grid3D = False
        RealColCount = 15
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          34
          148
          53
          77
          31
          46
          71
          95
          127
          54
          56
          88
          82
          139
          115)
      end
      object mskChaveAcessoNFe: TMaskEdit
        Left = 0
        Top = 322
        Width = 321
        Height = 21
        EditMask = '9999.9999.9999.9999.9999.9999.9999.9999.9999.9999.9999;1;_'
        MaxLength = 54
        ReadOnly = True
        TabOrder = 20
        Text = '    .    .    .    .    .    .    .    .    .    .    '
      end
      object eCNPJ: TEditLuka
        Left = 475
        Top = 142
        Width = 170
        Height = 21
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 8
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eInscricaoEstadual: TEditLuka
        Left = 651
        Top = 142
        Width = 170
        Height = 21
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 9
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eTelefone: TEditLuka
        Left = 827
        Top = 142
        Width = 171
        Height = 21
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 10
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eEndereco: TEditLuka
        Left = 475
        Top = 184
        Width = 523
        Height = 21
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 11
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eComplemento: TEditLuka
        Left = 475
        Top = 204
        Width = 523
        Height = 21
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 12
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eRazaoSocial: TEditLuka
        Left = 475
        Top = 103
        Width = 523
        Height = 21
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 7
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eNumNota: TEditLuka
        Left = 0
        Top = 280
        Width = 170
        Height = 21
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 14
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eSerieNF: TEditLuka
        Left = 174
        Top = 280
        Width = 57
        Height = 21
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 15
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eModeloNF: TEditLuka
        Left = 235
        Top = 280
        Width = 57
        Height = 21
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 16
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eDataEmissao: TEditLuka
        Left = 296
        Top = 280
        Width = 80
        Height = 21
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 17
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eDataSaida: TEditLuka
        Left = 380
        Top = 280
        Width = 80
        Height = 21
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 18
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eNatuezaOperacao: TEditLuka
        Left = 464
        Top = 280
        Width = 534
        Height = 21
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 19
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eBaseCalcIcms: TEditLuka
        Left = 326
        Top = 322
        Width = 93
        Height = 21
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 21
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eValorIcms: TEditLuka
        Left = 421
        Top = 322
        Width = 93
        Height = 21
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 22
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eBaseCalcST: TEditLuka
        Left = 516
        Top = 322
        Width = 93
        Height = 21
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 23
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eValorST: TEditLuka
        Left = 612
        Top = 322
        Width = 93
        Height = 21
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 24
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eValorIPI: TEditLuka
        Left = 708
        Top = 322
        Width = 93
        Height = 21
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 25
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eValorPis: TEditLuka
        Left = 804
        Top = 322
        Width = 93
        Height = 21
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 26
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eValorCofins: TEditLuka
        Left = 901
        Top = 322
        Width = 97
        Height = 21
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 27
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eValorProdutos: TEditLuka
        Left = 0
        Top = 366
        Width = 93
        Height = 21
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 28
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eValorFrete: TEditLuka
        Left = 95
        Top = 366
        Width = 93
        Height = 21
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 29
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eValorOutras: TEditLuka
        Left = 190
        Top = 366
        Width = 93
        Height = 21
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 30
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eValorDesconto: TEditLuka
        Left = 286
        Top = 366
        Width = 93
        Height = 21
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 31
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eValorTotal: TEditLuka
        Left = 382
        Top = 366
        Width = 93
        Height = 21
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 32
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object ePesoBruto: TEditLuka
        Left = 478
        Top = 366
        Width = 93
        Height = 21
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 33
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object ePesoLiquido: TEditLuka
        Left = 574
        Top = 366
        Width = 97
        Height = 21
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 34
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object st18: TStaticTextLuka
        Left = 0
        Top = 0
        Width = 1003
        Height = 15
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Destinat'#225'rio'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object eRazaoSocialDestinatario: TEditLuka
        Left = 1
        Top = 33
        Width = 523
        Height = 21
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 3
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eCnpjDestinatario: TEditLuka
        Left = 528
        Top = 32
        Width = 170
        Height = 21
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 1
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eInscEstadualDestinatario: TEditLuka
        Left = 703
        Top = 32
        Width = 170
        Height = 21
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 2
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eTipoFrete: TEditLuka
        Left = 675
        Top = 366
        Width = 323
        Height = 21
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 35
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
    end
    object tsItensNota: TTabSheet
      Caption = 'Itens da Nota'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object sgItens: TGridLuka
        Left = 0
        Top = 0
        Width = 1003
        Height = 418
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 35
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 2
        RowCount = 2
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        ParentFont = False
        PopupMenu = pmOpcoes
        TabOrder = 0
        OnClick = sgItensClick
        OnDrawCell = sgItensDrawCell
        OnKeyDown = sgItensKeyDown
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Leg.'
          'Sel.?'
          'Nome XML'
          'Produto'
          'Nome Hiva'
          'CST'
          'Pre'#231'o unit.'
          'Quantidade'
          'Und.'
          'Und.'
          'Valor desc.'
          'Outras desp.'
          'Valor total'
          'CFOP'
          'CFOP entrada'
          'Base c'#225'lc. ICMS'
          #205'nd.red.base c'#225'lc.ICMS'
          '% ICMS'
          'Valor ICMS'
          'Base c'#225'lc.ICMS ST'
          #205'nd.red.base c'#225'lc.ICMS ST'
          '% ICMS ST'
          'Valor ICMS ST'
          'Base c'#225'lc.PIS'
          '% PIS'
          'Valor PIS'
          'Base c'#225'lc.COFINS'
          '% COFINS'
          'Valor COFINS'
          '% IVA'
          'Pre'#231'o pauta'
          '% IPI'
          'Valor IPI'
          '% Frete'
          'Valor frete')
        OnGetCellPicture = sgItensGetCellPicture
        Grid3D = False
        RealColCount = 45
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          29
          29
          231
          86
          286
          32
          63
          75
          31
          73
          66
          73
          70
          42
          119
          90
          130
          48
          64
          102
          145
          59
          78
          76
          36
          56
          98
          58
          76
          45
          68
          36
          53
          65
          64)
      end
      object pnInformacoesItem: TPanel
        Left = 0
        Top = 418
        Width = 1003
        Height = 139
        Align = alBottom
        TabOrder = 1
        object pn1: TPanel
          Left = 1
          Top = 1
          Width = 498
          Height = 137
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object st5: TStaticTextLuka
            Left = 0
            Top = 0
            Width = 498
            Height = 17
            Align = alTop
            Alignment = taCenter
            AutoSize = False
            BevelEdges = [beLeft, beBottom]
            BevelInner = bvNone
            Caption = 'Informa'#231#245'es do produtos selecionado no XML'
            Color = 38619
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            TabOrder = 0
            Transparent = False
            AsInt = 0
            TipoCampo = tcTexto
            CasasDecimais = 0
          end
          object st10: TStaticText
            Left = 0
            Top = 17
            Width = 150
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'Nome de compra '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 1
            Transparent = False
          end
          object stNomeCompraXML: TStaticText
            Left = 150
            Top = 17
            Width = 350
            Height = 16
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = ' Caixa de acomp'
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 2
            Transparent = False
          end
          object stCodigoNCMXml: TStaticText
            Left = 150
            Top = 32
            Width = 350
            Height = 16
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = ' 0012457'
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 4
            Transparent = False
          end
          object st2: TStaticText
            Left = 0
            Top = 32
            Width = 150
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'C'#243'digo NCM '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 3
            Transparent = False
          end
          object stCestXml: TStaticText
            Left = 150
            Top = 47
            Width = 350
            Height = 16
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = ' 1001700'
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 6
            Transparent = False
          end
          object st3: TStaticText
            Left = 0
            Top = 47
            Width = 150
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'CEST '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 5
            Transparent = False
          end
          object stUnidadeXml: TStaticText
            Left = 150
            Top = 62
            Width = 350
            Height = 16
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = ' LTA'
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 8
            Transparent = False
          end
          object st4: TStaticText
            Left = 0
            Top = 62
            Width = 150
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'Unidade '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 7
            Transparent = False
          end
          object st1: TStaticText
            Left = 0
            Top = 77
            Width = 150
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'Fornecedor '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 9
            Transparent = False
          end
          object stFornecedorXML: TStaticText
            Left = 150
            Top = 77
            Width = 50
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = '3154 '
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 10
            Transparent = False
          end
          object stNomeFabricanteXML: TStaticText
            Left = 194
            Top = 77
            Width = 306
            Height = 16
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = ' TIGRE TUBOS E CONEX'#213'ES LTDA'
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 11
            Transparent = False
          end
          object st6: TStaticText
            Left = 0
            Top = 92
            Width = 150
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'C'#243'digo original '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 12
            Transparent = False
          end
          object stCodigoOriginalXml: TStaticText
            Left = 150
            Top = 92
            Width = 350
            Height = 16
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = ' ABC123'
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 13
            Transparent = False
          end
          object st8: TStaticText
            Left = 0
            Top = 107
            Width = 150
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'C'#243'digo de barras '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 14
            Transparent = False
          end
          object stCodigoBarrasXml: TStaticText
            Left = 150
            Top = 107
            Width = 350
            Height = 16
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = ' 789123123123'
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 15
            Transparent = False
          end
          object st11: TStaticText
            Left = 0
            Top = 122
            Width = 150
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'C'#243'digo de origem '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 16
            Transparent = False
          end
          object stCodigoOrigemXml: TStaticText
            Left = 150
            Top = 122
            Width = 350
            Height = 16
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = ' 0 - Nacional'
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 17
            Transparent = False
          end
        end
        object pn2: TPanel
          Left = 499
          Top = 1
          Width = 502
          Height = 137
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object sbPesquisaProduto: TSpeedButtonLuka
            Left = 484
            Top = 17
            Width = 16
            Height = 16
            Hint = 'Pesquisar produto'
            Flat = True
            OnClick = sbPesquisaProdutoClick
            TagImagem = 5
            PedirCertificacao = False
            PermitirAutOutroUsuario = False
          end
          object st12: TStaticTextLuka
            Left = 0
            Top = 0
            Width = 502
            Height = 17
            Align = alTop
            Alignment = taCenter
            AutoSize = False
            BevelEdges = [beLeft, beBottom]
            BevelInner = bvNone
            Caption = 
              'Informa'#231#245'es de cadastro do produto selecionado no Hiva. ( Duplo ' +
              'click para copiar inf. do XML ) '
            Color = 38619
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            TabOrder = 0
            Transparent = False
            AsInt = 0
            TipoCampo = tcTexto
            CasasDecimais = 0
          end
          object stNomeCompraAltis: TStaticText
            Left = 150
            Top = 17
            Width = 336
            Height = 16
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = ' Caixa de acomp'
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 2
            Transparent = False
            OnDblClick = stNomeCompraAltisDblClick
          end
          object st15: TStaticText
            Left = 1
            Top = 17
            Width = 149
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'Nome de compra '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 1
            Transparent = False
          end
          object st17: TStaticText
            Left = 1
            Top = 32
            Width = 149
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'C'#243'digo NCM '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 3
            Transparent = False
          end
          object stCodigoNcmAltis: TStaticText
            Left = 150
            Top = 32
            Width = 354
            Height = 16
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = ' 0012457'
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 4
            Transparent = False
            OnDblClick = stCodigoNcmAltisDblClick
          end
          object st7: TStaticText
            Left = 1
            Top = 47
            Width = 149
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'CEST '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 5
            Transparent = False
          end
          object st19: TStaticText
            Left = 1
            Top = 62
            Width = 149
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'Unidade '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 7
            Transparent = False
          end
          object st9: TStaticText
            Left = 1
            Top = 77
            Width = 149
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'Fornecedor '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 9
            Transparent = False
          end
          object st21: TStaticText
            Left = 1
            Top = 92
            Width = 149
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'C'#243'digo original '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 12
            Transparent = False
          end
          object st23: TStaticText
            Left = 1
            Top = 107
            Width = 149
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'C'#243'digo de barras '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 14
            Transparent = False
          end
          object st25: TStaticText
            Left = 1
            Top = 122
            Width = 149
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'C'#243'digo de origem '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 16
            Transparent = False
          end
          object stCestAltis: TStaticText
            Left = 150
            Top = 47
            Width = 354
            Height = 16
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = ' 1001700'
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 6
            Transparent = False
            OnDblClick = stCestAltisDblClick
          end
          object stUnidadeAltis: TStaticText
            Left = 150
            Top = 62
            Width = 354
            Height = 16
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = ' LTA'
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 8
            Transparent = False
            OnDblClick = stUnidadeAltisDblClick
          end
          object stFabricanteAltis: TStaticText
            Left = 150
            Top = 77
            Width = 50
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = ' 3166 '
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 10
            Transparent = False
            OnDblClick = stFabricanteAltisDblClick
          end
          object stNomeFabricanteAltis: TStaticText
            Left = 199
            Top = 77
            Width = 305
            Height = 16
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = ' TIGRE TUBOS E CONEX'#213'ES LTDA'
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 11
            Transparent = False
          end
          object stCodigoOriginalAltis: TStaticText
            Left = 150
            Top = 92
            Width = 354
            Height = 16
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = ' ABC123'
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 13
            Transparent = False
            OnDblClick = stCodigoOriginalAltisDblClick
          end
          object stCodigoBarrasAltis: TStaticText
            Left = 150
            Top = 107
            Width = 354
            Height = 16
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = ' 789123123123'
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 15
            Transparent = False
            OnDblClick = stCodigoBarrasAltisDblClick
          end
          object stCodigoOrigemAltis: TStaticText
            Left = 150
            Top = 122
            Width = 354
            Height = 16
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = ' 0 - Nacional'
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 17
            Transparent = False
          end
        end
      end
    end
  end
  object pmOpcoes: TPopupMenu
    Left = 16
    Top = 496
    object miMarcarSelecionado: TMenuItem
      Caption = 'Marcar selecionado ( Espa'#231'o )'
      OnClick = miMarcarSelecionadoClick
    end
    object miDesmarcarSelecionado: TMenuItem
      Caption = 'Desmarcar selecionado ( Espa'#231'o )'
      OnClick = miDesmarcarSelecionadoClick
    end
    object miN2: TMenuItem
      Caption = '-'
    end
    object miMarcarTodos: TMenuItem
      Caption = 'Marcar todos'
      OnClick = miMarcarTodosClick
    end
    object miDesmarcarTodos: TMenuItem
      Caption = 'Desmarcar todos'
      OnClick = miDesmarcarTodosClick
    end
    object miN1: TMenuItem
      Caption = '-'
    end
    object miCadastrarProdutoSelecionado: TMenuItem
      Caption = 'Cadastrar produto selecionado'
      OnClick = miCadastrarProdutoSelecionadoClick
    end
    object miAlterarCFOPEntrada: TMenuItem
      Caption = 'Alterar CFOP de entrada'
      OnClick = miAlterarCFOPEntradaClick
    end
  end
end
