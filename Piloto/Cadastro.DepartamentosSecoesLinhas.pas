unit Cadastro.DepartamentosSecoesLinhas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Sessao, _Biblioteca,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _RecordsCadastros, _ProdutosDeptosSecoesLinhas,
  _RecordsEspeciais, System.StrUtils, System.Types, Pesquisa.DepartamentosSecoesLinhas,
  CheckBoxLuka;

type
  TFormCadastroDepartamentosSecoesLinhas = class(TFormHerancaCadastroCodigo)
    lbDepartamento: TLabel;
    eDepartamento: TEditLuka;
    lb1: TLabel;
    eSecao: TEditLuka;
    lb2: TLabel;
    eLinha: TEditLuka;
    procedure eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); override;
  private
    FTipoCadastro: TTipoCadastro;

    procedure PreencherRegistro(pRegistro: RecProdutoDeptoSecaoLinha);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroDepartamentosSecoesLinhas }

procedure TFormCadastroDepartamentosSecoesLinhas.BuscarRegistro;
var
  vId: string;
  vDeptoSecaoLinha: TArray<RecProdutoDeptoSecaoLinha>;
begin
  if Pos('???', eID.Text) > 0 then
    vDeptoSecaoLinha := _ProdutosDeptosSecoesLinhas.BuscarProdutoDeptoSecaoLinhas(Sessao.getConexaoBanco, 0, [Copy(eID.Text, 1, Pos('???', eID.Text) - 2)], 0)
  else
    vDeptoSecaoLinha := _ProdutosDeptosSecoesLinhas.BuscarProdutoDeptoSecaoLinhas(Sessao.getConexaoBanco, 0, [eID.Text], 0);

  if vDeptoSecaoLinha = nil then begin
    NenhumRegistro;
    SetarFoco(eID);
    Abort;
  end;

  vId := eID.Text;
  inherited;

  PreencherRegistro(vDeptoSecaoLinha[0]);
  eID.Text := vId;
end;

procedure TFormCadastroDepartamentosSecoesLinhas.eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  i: Integer;
  vMascara: TStringDynArray;
begin
  if Key <> VK_RETURN then
    Exit;

  eID.Trim;
  FTipoCadastro := ttNenhum;
  if eID.Text = '' then begin
    FTipoCadastro := ttDepartamento;
    Modo(True);
    eID.Text := '???';
    Exit;
  end;

  vMascara := SplitString(eID.Text, '.');
  if Length(vMascara) > 3 then begin
    Exclamar('A m�scara informada est� incorreta!');
    eID.Clear;
    SetarFoco(eID);
    Abort;
  end;

  eID.Text := '';
  for i := Low(vMascara) to High(vMascara) do begin
    if vMascara[i] = '' then
      eID.Text := eID.Text + '.???'
    else
      eID.Text := eID.Text + IfThen(i in[1,2], '.') + LPad(vMascara[i], 3, '0');
  end;

  case Length(vMascara) of
    1: FTipoCadastro := ttDepartamento;
    2: FTipoCadastro := ttSecao;
    3: FTipoCadastro := ttLinha;
  else
    FTipoCadastro := ttNenhum;
  end;

  BuscarRegistro;
end;

procedure TFormCadastroDepartamentosSecoesLinhas.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _ProdutosDeptosSecoesLinhas.ExcluirProdutoDeptoSecaoLinha(Sessao.getConexaoBanco, eID.Text);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroDepartamentosSecoesLinhas.GravarRegistro(Sender: TObject);
var
  vNomeSalvar: string;
  vRetBanco: RecRetornoBD;
begin
  inherited;

  if FTipoCadastro = ttDepartamento then
    vNomeSalvar := eDepartamento.Text
  else if FTipoCadastro = ttSecao then
    vNomeSalvar := eSecao.Text
  else
    vNomeSalvar := eLinha.Text;

  vRetBanco :=
    _ProdutosDeptosSecoesLinhas.AtualizarProdutoDeptoSecaoLinha(
      Sessao.getConexaoBanco,
      eID.Text,
      vNomeSalvar,
      RetornaMascaraPai(eID.Text),
      ToChar(ckAtivo),
      FTipoCadastro
    );

  if vRetBanco.TeveErro then begin
    Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  if vRetBanco.AsString <> '' then
    Informar(coNovoRegistroSucessoCodigo + vRetBanco.AsString)
  else
    _Biblioteca.Informar(coAlteracaoRegistroSucesso);
end;

procedure TFormCadastroDepartamentosSecoesLinhas.Modo(pEditando: Boolean);
var
  vEditHabilitar: TEditLuka;
begin
  inherited;
  vEditHabilitar := nil;

  if FTipoCadastro = ttDepartamento then
    vEditHabilitar := eDepartamento
  else if FTipoCadastro = ttSecao then
    vEditHabilitar := eSecao
  else if FTipoCadastro = ttLinha then
    vEditHabilitar := eLinha;

  _Biblioteca.Habilitar([
    eDepartamento,
    eSecao,
    eLinha],
    False
  );

  _Biblioteca.Habilitar([ckAtivo], pEditando);

  if vEditHabilitar <> nil then
    _Biblioteca.Habilitar([vEditHabilitar], pEditando);

  if pEditando then begin
    SetarFoco(vEditHabilitar);
    ckAtivo.Checked := True;
  end;
end;

procedure TFormCadastroDepartamentosSecoesLinhas.PesquisarRegistro;
var
  vDepto: RecProdutoDeptoSecaoLinha;
begin
  vDepto := RecProdutoDeptoSecaoLinha(Pesquisa.DepartamentosSecoesLinhas.Pesquisar(0));
  if vDepto = nil then
    Exit;

  case Length(vDepto.DeptoSecaoLinhaId) of
    3: FTipoCadastro := ttDepartamento;
    7: FTipoCadastro := ttSecao;
    11: FTipoCadastro := ttLinha;
  else
    FTipoCadastro := ttNenhum;
  end;

  inherited;
  PreencherRegistro(vDepto);
end;

procedure TFormCadastroDepartamentosSecoesLinhas.PreencherRegistro(pRegistro: RecProdutoDeptoSecaoLinha);
begin
  eID.Text           := pRegistro.DeptoSecaoLinhaId;
  eDepartamento.Text := pRegistro.NomeDepartamento;
  eSecao.Text        := pRegistro.NomeSecao;
  eLinha.Text        := pRegistro.NomeLinha;
  ckAtivo.Checked    := (pRegistro.Ativo = 'S');

  pRegistro.Free;
end;

procedure TFormCadastroDepartamentosSecoesLinhas.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if FTipoCadastro = ttDepartamento then begin
    if eDepartamento.Trim = '' then begin
      Exclamar('� necess�rio informar o nome do departamento!');
      SetarFoco(eDepartamento);
      Abort;
    end;
  end
  else if FTipoCadastro = ttSecao then begin
    if eSecao.Trim = '' then begin
      Exclamar('� necess�rio informar o nome da se��o!');
      SetarFoco(eDepartamento);
      Abort;
    end;
  end
  else begin
    if eLinha.Trim = '' then begin
      Exclamar('� necess�rio informar o nome da linha!');
      SetarFoco(eDepartamento);
      Abort;
    end;
  end;
end;

end.
