inherited FrImagem: TFrImagem
  Width = 128
  Height = 128
  ExplicitWidth = 128
  ExplicitHeight = 128
  object pnImagem: TPanel
    Left = 0
    Top = 0
    Width = 128
    Height = 128
    Align = alClient
    BevelInner = bvSpace
    BevelOuter = bvLowered
    Caption = 'Foto'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    object imFoto: TImage
      Left = 2
      Top = 2
      Width = 124
      Height = 124
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      Align = alClient
      ParentShowHint = False
      PopupMenu = pmOpcoes
      ShowHint = False
      Stretch = True
      OnClick = imFotoClick
      ExplicitLeft = 23
      ExplicitTop = 23
      ExplicitWidth = 105
      ExplicitHeight = 105
    end
  end
  object pmOpcoes: TPopupMenu
    OnPopup = pmOpcoesPopup
    Left = 16
    Top = 72
    object miCarregarfoto: TMenuItem
      Caption = 'Carregar foto'
      OnClick = miCarregarfotoClick
    end
    object miApagarfoto: TMenuItem
      Caption = 'Apagar foto'
      OnClick = miApagarfotoClick
    end
  end
end
