inherited FormGerarArquivoRemessaBoleto: TFormGerarArquivoRemessaBoleto
  Caption = 'Gerar arquivo de remessa de boletos'
  ClientWidth = 997
  OnShow = FormShow
  ExplicitWidth = 1003
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    inherited sbImprimir: TSpeedButton
      Left = 4
      Top = 221
      Visible = False
      ExplicitLeft = 4
      ExplicitTop = 221
    end
    object SpeedButton1: TSpeedButton [2]
      Left = 3
      Top = 110
      Width = 110
      Height = 40
      Caption = 'Gerar Remessa'
      OnClick = SpeedButton1Click
    end
  end
  inherited pcDados: TPageControl
    Width = 875
    ExplicitWidth = 875
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 867
      ExplicitHeight = 518
      object lb2: TLabel
        Left = 322
        Top = 89
        Width = 97
        Height = 14
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alCustom
        Caption = 'Situa'#231#227'o remessa'
      end
      inline FrPortador: TFrPortadores
        Left = 2
        Top = 2
        Width = 320
        Height = 41
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 2
        ExplicitWidth = 320
        ExplicitHeight = 41
        inherited sgPesquisa: TGridLuka
          Width = 295
          Height = 24
          ExplicitWidth = 295
          ExplicitHeight = 24
        end
        inherited CkAspas: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 46
            Caption = 'Portador'
            ExplicitWidth = 46
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          Height = 25
          ExplicitLeft = 295
          ExplicitHeight = 25
        end
      end
      inline FrDataEmissao: TFrDataInicialFinal
        Left = 320
        Top = 0
        Width = 197
        Height = 41
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 320
        ExplicitWidth = 197
        inherited Label1: TLabel
          Width = 93
          Height = 14
          Caption = 'Data de emiss'#227'o'
          ExplicitWidth = 93
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrDataVencimento: TFrDataInicialFinal
        Left = 320
        Top = 45
        Width = 197
        Height = 42
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 320
        ExplicitTop = 45
        ExplicitWidth = 197
        ExplicitHeight = 42
        inherited Label1: TLabel
          Width = 109
          Height = 14
          Caption = 'Data de vencimento'
          ExplicitWidth = 109
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      object cbSituacaoRemessa: TComboBoxLuka
        Left = 322
        Top = 103
        Width = 181
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 1
        TabOrder = 3
        Text = 'Apenas n'#227'o gerados'
        Items.Strings = (
          'N'#227'o filtrar'
          'Apenas n'#227'o gerados'
          'Gerados')
        Valores.Strings = (
          'NaoFiltrar'
          'NaoGerado'
          'Gerado')
        AsInt = 0
        AsString = 'NaoGerado'
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 867
      ExplicitHeight = 518
      object sgBoletos: TGridLuka
        Left = 0
        Top = 0
        Width = 867
        Height = 518
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 8
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
        TabOrder = 0
        OnDrawCell = sgBoletosDrawCell
        OnKeyDown = sgBoletosKeyDown
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Sel.?'
          'C'#243'digo'
          'Documento'
          'Data emiss'#227'o'
          'Data vencimento'
          'Valor documento'
          'Nome cliente'
          'Nosso n'#250'mero')
        OnGetCellPicture = sgBoletosGetCellPicture
        Grid3D = False
        RealColCount = 10
        OrdenarOnClick = True
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          32
          46
          73
          84
          102
          100
          290
          113)
      end
    end
  end
end
