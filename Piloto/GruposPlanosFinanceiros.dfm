inherited FormGruposPlanosFinanceiros: TFormGruposPlanosFinanceiros
  Caption = 'Grupos de planos financeiros'
  ClientHeight = 269
  ClientWidth = 655
  ExplicitWidth = 661
  ExplicitHeight = 298
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label1: TLabel
    Left = 125
    Top = 2
    ExplicitLeft = 125
    ExplicitTop = 2
  end
  object lbDepartamento: TLabel [1]
    Left = 211
    Top = 2
    Width = 53
    Height = 14
    Caption = 'Descri'#231#227'o'
  end
  inherited pnOpcoes: TPanel
    Height = 269
    TabOrder = 3
    ExplicitHeight = 271
    inherited sbGravar: TSpeedButton
      Left = 1
      ExplicitLeft = 1
    end
    inherited sbDesfazer: TSpeedButton
      Left = 1
      Top = 98
      ExplicitLeft = 1
      ExplicitTop = 98
    end
    inherited sbExcluir: TSpeedButton
      Left = 1
      Top = 156
      ExplicitLeft = 1
      ExplicitTop = 156
    end
    inherited sbPesquisar: TSpeedButton
      Left = 1
      Top = 47
      ExplicitLeft = 1
      ExplicitTop = 47
    end
    inherited sbLogs: TSpeedButton
      Left = 1
      Top = 218
      ExplicitLeft = 1
      ExplicitTop = 218
    end
  end
  inherited eID: TEditLuka
    Left = 125
    Top = 18
    Alignment = taLeftJustify
    TabOrder = 0
    OnKeyPress = NumerosPonto
    TipoCampo = tcTexto
    ExplicitLeft = 125
    ExplicitTop = 18
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 1000
    Top = 8
    Enabled = False
    TabOrder = 4
    Visible = False
    ExplicitLeft = 1000
    ExplicitTop = 8
  end
  object eDescricao: TEditLuka
    Left = 211
    Top = 19
    Width = 400
    Height = 22
    CharCase = ecUpperCase
    Enabled = False
    TabOrder = 1
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object edtSubGrupo: TEdit
    Left = 276
    Top = 8
    Width = 25
    Height = 22
    TabStop = False
    TabOrder = 5
    Visible = False
  end
  inline FrPlanoFinanceiro: TFrPlanosFinanceiros
    Left = 125
    Top = 47
    Width = 482
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Enabled = False
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 125
    ExplicitTop = 47
    ExplicitWidth = 482
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 457
      Height = 23
      OnKeyDown = FrPlanoFinanceirosgPesquisaKeyDown
      ExplicitWidth = 457
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 482
      ExplicitWidth = 482
      inherited lbNomePesquisa: TLabel
        Width = 88
        Caption = 'Plano financeiro'
        ExplicitWidth = 88
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 377
        ExplicitLeft = 377
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 457
      Height = 24
      ExplicitLeft = 457
      ExplicitHeight = 24
    end
  end
  object gdPlanoFinanceiro: TGridLuka
    Left = 125
    Top = 94
    Width = 486
    Height = 170
    Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
    TabStop = False
    ColCount = 2
    DefaultRowHeight = 19
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    PopupMenu = ppmSql
    TabOrder = 6
    OnKeyDown = gdPlanoFinanceiroKeyDown
    OnSelectCell = gdPlanoFinanceiroSelectCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = clWhite
    CorFundoFoco = clWhite
    CorLinhaDesfoque = clWhite
    CorFundoDesfoque = clWhite
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = clWhite
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    OnGetCellColor = gdPlanoFinanceiroGetCellColor
    Grid3D = False
    RealColCount = 4
    AtivarPopUpSelecao = False
    ColWidths = (
      400
      64)
  end
  object pnlSQL: TPanel
    Left = 160
    Top = 104
    Width = 417
    Height = 187
    BevelInner = bvLowered
    TabOrder = 7
    Visible = False
    object Label2: TLabel
      Left = 2
      Top = 2
      Width = 413
      Height = 14
      Align = alTop
      Caption = 'SQL'
      ExplicitWidth = 19
    end
    object SpeedButton1: TSpeedButton
      Left = 2
      Top = 163
      Width = 413
      Height = 22
      Align = alBottom
      Caption = '&Ok'
      OnClick = SpeedButton1Click
      ExplicitTop = 169
    end
    object mmSQL: TMemo
      Left = 2
      Top = 16
      Width = 413
      Height = 147
      Align = alClient
      ScrollBars = ssBoth
      TabOrder = 0
    end
  end
  object btnNextPrev: TUpDown
    Left = 610
    Top = 94
    Width = 39
    Height = 170
    DoubleBuffered = False
    Enabled = False
    Min = -100
    ParentDoubleBuffered = False
    TabOrder = 8
    OnClick = btnNextPrevClick
  end
  object ppmSql: TPopupMenu
    Left = 304
    Top = 136
    object InserirSQL1: TMenuItem
      Caption = 'Inserir SQL'
      OnClick = InserirSQL1Click
    end
  end
end
