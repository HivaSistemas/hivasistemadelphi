unit FramePlanosFinanceiros;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Buttons, _PlanosFinanceiros,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Sessao, _Biblioteca, PesquisaPlanosFinanceiros,
  Vcl.Menus;

type
  TFrPlanosFinanceiros = class(TFrameHenrancaPesquisas)
    ckSomenteNivel3: TCheckBox;
  public
    function getDados(pLinha: Integer = -1): RecPlanosFinanceiros;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function AdicionarPesquisandoTodos: TArray<TObject>; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

uses System.StrUtils, System.Types;

{$R *.dfm}

{ TFrPlanosFinanceiros }

function TFrPlanosFinanceiros.AdicionarDireto: TObject;
var
  i: Integer;
  x: TStringDynArray;
  vDados: TArray<RecPlanosFinanceiros>;
begin
  x := SplitString(FChaveDigitada, '.');
  FChaveDigitada := '';
  for i := Low(x) to High(x) do
    FChaveDigitada := FChaveDigitada + IfThen(i in[1,2], '.') + IIfStr(i = 0, x[i], LPad(x[i], 3, '0'));

  vDados := _PlanosFinanceiros.BuscarPlanosFinanceiros(Sessao.getConexaoBanco, 0, [FChaveDigitada], ckSomenteNivel3.Checked);
  if vDados = nil then
    Result := nil
  else
    Result := vDados[0];
end;

function TFrPlanosFinanceiros.AdicionarPesquisando: TObject;
begin
  Result := PesquisaPlanosFinanceiros.Pesquisar(ckSomenteAtivos.Checked, ckSomenteNivel3.Checked);
end;

function TFrPlanosFinanceiros.AdicionarPesquisandoTodos: TArray<TObject>;
begin
//  Result := PesquisaMotoristas.PesquisarVarios(ckSomenteAtivos.Checked);
end;

function TFrPlanosFinanceiros.getDados(pLinha: Integer): RecPlanosFinanceiros;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecPlanosFinanceiros(FDados[IIfInt(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrPlanosFinanceiros.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecPlanosFinanceiros(FDados[i]).PlanoFinanceiroId = RecPlanosFinanceiros(pSender).PlanoFinanceiroId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrPlanosFinanceiros.MontarGrid;
var
  i: Integer;
  vSender: RecPlanosFinanceiros;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecPlanosFinanceiros(FDados[i]);
      AAdd([vSender.PlanoFinanceiroId, vSender.Descricao]);
    end;
  end;
end;

end.
