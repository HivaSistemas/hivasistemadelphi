unit InformacoesContasPagarBaixa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _RecordsFinanceiros,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, StaticTextLuka, Vcl.StdCtrls, Vcl.Mask, _Sessao,
  EditLukaData, EditLuka, Vcl.ComCtrls, _Biblioteca, _ContasPagarBaixas, _ContasPagarBaixasItens,
  _ContasPagarBaixasPagtosDin, _ContasPagar, SpeedButtonLuka;

type
  TFormInformacoesContasPagarBaixa = class(TFormHerancaFinalizar)
    pcDados: TPageControl;
    tsGerais: TTabSheet;
    lbllb7: TLabel;
    lbllb1: TLabel;
    lbllb2: TLabel;
    lbllb3: TLabel;
    lbllb11: TLabel;
    lbllb12: TLabel;
    lbllb13: TLabel;
    lbllb14: TLabel;
    lbllb5: TLabel;
    lbllb4: TLabel;
    lbllb8: TLabel;
    lbllb15: TLabel;
    lbllb16: TLabel;
    lbllb17: TLabel;
    lbllb18: TLabel;
    eBaixaId: TEditLuka;
    eUsuarioBaixa: TEditLuka;
    eDataPagamento: TEditLukaData;
    eDataBaixa: TEditLukaData;
    eValorDinheiro: TEditLuka;
    eValorCheque: TEditLuka;
    eValorCartao: TEditLuka;
    eValorCobranca: TEditLuka;
    stValores: TStaticText;
    eValorCredito: TEditLuka;
    eObservacoes: TMemo;
    eEmpresaBaixa: TEditLuka;
    eValorBruto: TEditLuka;
    eValorJuros: TEditLuka;
    eValorDesconto: TEditLuka;
    eValorL�quido: TEditLuka;
    StaticTextLuka5: TStaticTextLuka;
    stTipo: TStaticText;
    tsContasPagamentoDinheiro: TTabSheet;
    sgContasDinheiro: TGridLuka;
    tsTitulosBaixados: TTabSheet;
    sgTitulosBaixados: TGridLuka;
    tsTitulosGerados: TTabSheet;
    sgTitulosGerados: TGridLuka;
    sbCreditos: TSpeedButton;
    lb19: TLabel;
    eValorTroca: TEditLuka;
    tsOrigem: TTabSheet;
    lb1: TLabel;
    eBaixaReceberOrigemId: TEditLuka;
    sbInformacoesBaixa: TSpeedButtonLuka;
    sbInformacoesPedido: TSpeedButtonLuka;
    ePedido: TEditLuka;
    lb2: TLabel;
    sbInformacoesAcumulado: TSpeedButtonLuka;
    eAcumulado: TEditLuka;
    lb3: TLabel;
    procedure sgContasDinheiroDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure sgTitulosBaixadosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgTitulosBaixadosDblClick(Sender: TObject);
    procedure sgTitulosGeradosDblClick(Sender: TObject);
    procedure sgTitulosGeradosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sbInformacoesBaixaClick(Sender: TObject);
    procedure sbCreditosClick(Sender: TObject);
    procedure sbInformacoesPedidoClick(Sender: TObject);
    procedure sbInformacoesAcumuladoClick(Sender: TObject);
  end;

procedure Informar(pBaixaId: Integer);

implementation

{$R *.dfm}

uses
  Informacoes.TituloPagar, Informacoes.ContasReceberBaixa, InformacoesCreditosUtilizados, Informacoes.Orcamento, InformacoesAcumulado;

const
  coPagarId        = 0;
  coFornecedor     = 1;
  coDocumento      = 2;
  coDataVencimento = 3;
  coValor          = 4;
  coTipoCobranca   = 5;

  (* Grid de contas com os recebimentos em dinheiro *)
  cdContaId      = 0;
  cdDescricao    = 1;
  cdValor        = 2;

  (* Grid de t�tulos gerados *)
  cgPagarId        = 0;
  cgFornecedor     = 1;
  cgDocumento      = 2;
  cgDataVencimento = 3;
  cgValor          = 4;
  cgTipoCobranca   = 5;

procedure Informar(pBaixaId: Integer);
var
  i: Integer;
  vForm: TFormInformacoesContasPagarBaixa;
  vDadosBaixa: TArray<RecContasPagarBaixas>;
  vItensBaixa: TArray<RecContasPagarBaixasItens>;
  vPagamentosDin: TArray<RecTitulosBaixasPagtoDin>;

  vFinancGerados: TArray<RecContaPagar>;
begin
  if pBaixaId = 0 then
    Exit;

  vDadosBaixa := _ContasPagarBaixas.BuscarContasPagarBaixas(Sessao.getConexaoBanco, 0, [pBaixaId]);
  if vDadosBaixa = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vItensBaixa := _ContasPagarBaixasItens.BuscarContasPagarBaixasItenss(Sessao.getConexaoBanco, 0, [pBaixaId]);
  if vDadosBaixa = nil then begin
    Exclamar('Os itens da baixa n�o foram encontrados!');
    Exit;
  end;

  vForm := TFormInformacoesContasPagarBaixa.Create(Application);

  vForm.ReadOnlyTodosObjetos(True);
  vForm.eBaixaId.AsInt         := vDadosBaixa[0].baixa_id;
  vForm.eUsuarioBaixa.SetInformacao(vDadosBaixa[0].usuario_baixa_id, vDadosBaixa[0].NomeUsuarioBaixa);
  vForm.eDataPagamento.AsData  := vDadosBaixa[0].data_pagamento;
  vForm.eDataBaixa.AsData      := vDadosBaixa[0].data_hora_baixa;
  vForm.eEmpresaBaixa.Text     := NFormat(vDadosBaixa[0].empresa_id) + ' - ' + vDadosBaixa[0].NomeEmpresaBaixa;
  vForm.stTipo.Caption         := vDadosBaixa[0].TipoAnalitico;

  vForm.eValorBruto.AsCurr    := vDadosBaixa[0].ValorTitulos;
  vForm.eValorJuros.AsCurr    := vDadosBaixa[0].ValorJuros;
  vForm.eValorDesconto.AsCurr := vDadosBaixa[0].valor_desconto;
  vForm.eValorL�quido.AsCurr  := vDadosBaixa[0].ValorLiquido;
  vForm.eValorDinheiro.AsCurr := vDadosBaixa[0].valor_dinheiro;
  vForm.eValorCheque.AsCurr   := vDadosBaixa[0].valor_cheque;
  vForm.eValorCartao.AsCurr   := vDadosBaixa[0].valor_cartao;
  vForm.eValorCobranca.AsCurr := vDadosBaixa[0].valor_cobranca;
  vForm.eValorCredito.AsCurr  := vDadosBaixa[0].valor_credito;
  vForm.eValorTroca.AsCurr    := vDadosBaixa[0].ValorTroca;
  vForm.eObservacoes.Text     := vDadosBaixa[0].observacoes;

  vForm.eBaixaReceberOrigemId.SetInformacao(vDadosBaixa[0].BaixaReceberOrigemId);
  vForm.ePedido.SetInformacao(vDadosBaixa[0].OrcamentoId);
  vForm.eAcumulado.SetInformacao(vDadosBaixa[0].AcumuladoId);

  vPagamentosDin := _ContasPagarBaixasPagtosDin.BuscarContasRecBaixasPagtoDin(Sessao.getConexaoBanco, 0, [pBaixaId]);
  for i := Low(vPagamentosDin) to High(vPagamentosDin) do begin
    vForm.sgContasDinheiro.Cells[cdContaId, i + 1]   := vPagamentosDin[i].ContaId;
    vForm.sgContasDinheiro.Cells[cdDescricao, i + 1] := vPagamentosDin[i].Descricao;
    vForm.sgContasDinheiro.Cells[cdValor, i + 1]     := NFormat(vPagamentosDin[i].Valor);
  end;
  vForm.sgContasDinheiro.SetLinhasGridPorTamanhoVetor( Length(vPagamentosDin) );

  for i := Low(vItensBaixa) to High(vItensBaixa) do begin
    vForm.sgTitulosBaixados.Cells[coPagarId, i + 1]        := NFormat(vItensBaixa[i].PagarId);
    vForm.sgTitulosBaixados.Cells[coFornecedor, i + 1]     := NFormat(vItensBaixa[i].CadastroId) + ' - ' + vItensBaixa[i].NomeFornecedor;
    vForm.sgTitulosBaixados.Cells[coDocumento, i + 1]      := vItensBaixa[i].Documento;
    vForm.sgTitulosBaixados.Cells[coDataVencimento, i + 1] := DFormat(vItensBaixa[i].DataVencimento);
    vForm.sgTitulosBaixados.Cells[coValor, i + 1]          := NFormat(vItensBaixa[i].ValorDocumento);
    vForm.sgTitulosBaixados.Cells[coTipoCobranca, i + 1]   := NFormat(vItensBaixa[i].CobrancaId) + ' - ' + vItensBaixa[i].NomeTipoCobranca;
  end;
  vForm.sgTitulosBaixados.SetLinhasGridPorTamanhoVetor( Length(vItensBaixa) );

  vFinancGerados := _ContasPagar.BuscarContasPagar(Sessao.getConexaoBanco, 3, [pBaixaId]);
  for i := Low(vFinancGerados) to High(vFinancGerados) do begin
    vForm.sgTitulosGerados.Cells[cgPagarId, i + 1]        := NFormat(vFinancGerados[i].PagarId);
    vForm.sgTitulosGerados.Cells[cgFornecedor, i + 1]     := NFormat(vFinancGerados[i].CadastroId) + ' - ' + vFinancGerados[i].nome_fornecedor;
    vForm.sgTitulosGerados.Cells[cgDocumento, i + 1]      := vFinancGerados[i].documento;
    vForm.sgTitulosGerados.Cells[cgDataVencimento, i + 1] := DFormat(vFinancGerados[i].data_vencimento);
    vForm.sgTitulosGerados.Cells[cgValor, i + 1]          := NFormat(vFinancGerados[i].ValorDocumento);
    vForm.sgTitulosGerados.Cells[cgTipoCobranca, i + 1]   := NFormat(vFinancGerados[i].cobranca_id) + ' - ' + vFinancGerados[i].nome_tipo_cobranca;
  end;
  vForm.sgTitulosGerados.SetLinhasGridPorTamanhoVetor( Length(vFinancGerados) );

  vForm.ShowModal;

  vForm.Free;
end;

procedure TFormInformacoesContasPagarBaixa.FormShow(Sender: TObject);
begin
  inherited;
  pcDados.ActivePageIndex := 0;
end;

procedure TFormInformacoesContasPagarBaixa.sbCreditosClick(Sender: TObject);
begin
  inherited;
  InformacoesCreditosUtilizados.Informar( eBaixaId.AsInt, 'P' );
end;

procedure TFormInformacoesContasPagarBaixa.sbInformacoesAcumuladoClick(Sender: TObject);
begin
  inherited;
  InformacoesAcumulado.Informar(eAcumulado.IdInformacao);
end;

procedure TFormInformacoesContasPagarBaixa.sbInformacoesBaixaClick(Sender: TObject);
begin
  inherited;
  Informacoes.ContasReceberBaixa.Informar( eBaixaReceberOrigemId.IdInformacao );
end;

procedure TFormInformacoesContasPagarBaixa.sbInformacoesPedidoClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar(ePedido.IdInformacao);
end;

procedure TFormInformacoesContasPagarBaixa.sgContasDinheiroDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol = cdValor then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgContasDinheiro.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesContasPagarBaixa.sgTitulosBaixadosDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.TituloPagar.Informar( SFormatInt(sgTitulosBaixados.Cells[coPagarId, sgTitulosBaixados.Row]) );
end;

procedure TFormInformacoesContasPagarBaixa.sgTitulosBaixadosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coPagarId, coValor] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgTitulosBaixados.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesContasPagarBaixa.sgTitulosGeradosDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.TituloPagar.Informar( SFormatInt(sgTitulosGerados.Cells[cgPagarId, sgTitulosGerados.Row]) );
end;

procedure TFormInformacoesContasPagarBaixa.sgTitulosGeradosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[cgPagarId, cgValor] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgTitulosGerados.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
