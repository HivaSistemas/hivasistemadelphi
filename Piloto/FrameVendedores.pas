unit FrameVendedores;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _RecordsCadastros,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Funcionarios, PesquisaFuncionarios, _Sessao, System.Math,
  Vcl.Buttons, Vcl.Menus;

type
  TFrVendedores = class(TFrameHenrancaPesquisas)
  public
    function getVendedor(pLinha: Integer = -1): RecFuncionarios;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrVendedores }

function TFrVendedores.AdicionarDireto: TObject;
var
  vFuncionarios: TArray<RecFuncionarios>;
begin
  vFuncionarios := _Funcionarios.BuscarFuncionarios(Sessao.getConexaoBanco, 0, [FChaveDigitada], ckSomenteAtivos.Checked, True, False, False, False);
  if vFuncionarios = nil then
    Result := nil
  else
    Result := vFuncionarios[0];
end;

function TFrVendedores.AdicionarPesquisando: TObject;
begin
  Result := PesquisaFuncionarios.PesquisarFuncionario(True, False, False, False, Sessao.getEmpresaLogada.EmpresaId, True, True, False);
end;

function TFrVendedores.getVendedor(pLinha: Integer): RecFuncionarios;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecFuncionarios(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrVendedores.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecFuncionarios(FDados[i]).funcionario_id = RecFuncionarios(pSender).funcionario_id then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrVendedores.MontarGrid;
var
  i: Integer;
  pSender: RecFuncionarios;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      pSender := RecFuncionarios(FDados[i]);
      AAdd([IntToStr(pSender.funcionario_id), pSender.nome]);
    end;
  end;
end;


end.
