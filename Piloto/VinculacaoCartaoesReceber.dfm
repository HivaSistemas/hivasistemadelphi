inherited FormVinculacaoCartaoesReceber: TFormVinculacaoCartaoesReceber
  Caption = 'Vincula'#231#227'o de cart'#245'es a receber'
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    inherited sbImprimir: TSpeedButton
      Top = 168
      ExplicitTop = 168
    end
    object sbGravar: TSpeedButtonLuka [2]
      Left = 1
      Top = 78
      Width = 117
      Height = 35
      Caption = 'Gravar (F2)'
      Flat = True
      OnClick = sbGravarClick
      TagImagem = 9
      PedirCertificacao = False
      PermitirAutOutroUsuario = False
    end
    inherited ckOmitirFiltros: TCheckBoxLuka
      Top = 200
      ExplicitTop = 200
    end
  end
  inherited pcDados: TPageControl
    ActivePage = tsFiltros
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      inline FrCaminhoArquivo: TFrCaminhoArquivo
        Left = 0
        Top = 3
        Width = 433
        Height = 45
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitTop = 3
        ExplicitWidth = 433
        inherited lb1: TLabel
          Width = 92
          Height = 14
          Caption = 'Caminho arquivo'
          ExplicitWidth = 92
          ExplicitHeight = 14
        end
        inherited sbPesquisarArquivo: TSpeedButton
          Left = 406
          ExplicitLeft = 406
        end
        inherited eCaminhoArquivo: TEditLuka
          Width = 401
          Height = 22
          ExplicitWidth = 401
          ExplicitHeight = 22
        end
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object sgCartoes: TGridLuka
        Left = 0
        Top = 0
        Width = 884
        Height = 518
        Align = alClient
        ColCount = 11
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
        TabOrder = 0
        OnDblClick = sgCartoesDblClick
        OnDrawCell = sgCartoesDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Ident.?'
          'C'#243'd.mov.Altis'
          'Tipo mov.Altis'
          'Cliente Altis'
          'NSU'
          'Numero cart'#227'o'
          'Rede'
          'Bandeira'
          'Valor arq.'
          'Data arq.'
          'Qtde.parc.')
        OnGetCellColor = sgCartoesGetCellColor
        OnGetCellPicture = sgCartoesGetCellPicture
        Grid3D = False
        RealColCount = 11
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          44
          80
          129
          194
          56
          141
          63
          89
          73
          69
          64)
      end
    end
  end
end
