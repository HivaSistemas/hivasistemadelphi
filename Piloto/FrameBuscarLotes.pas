unit FrameBuscarLotes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Frame.HerancaInsercaoExclusao, _Biblioteca,
  Vcl.Menus, Vcl.StdCtrls, StaticTextLuka, Vcl.Grids, GridLuka, _EntradasNfItensLotes;

type
  TTela = (ttDevolucaEntradas, ttEntradas, ttInformacoesEntradaNF);

  TFrBuscarLotes = class(TFrameHerancaInsercaoExclusao)
    procedure sgValoresDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgValoresKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgValoresSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure sgValoresArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
    procedure sgValoresGetEditMask(Sender: TObject; ACol, ARow: Integer; var Value: string);
  private
    FTela: TTela;
    FProdutoId: Integer;
    FMultiploCompra: Double;
    FTipoControleEstoque: string;
    FLotes: TArray<RecEntradasNfItensLotes>;

    FExigirDataFabricao: Boolean;
    FExigirDataVencimento: Boolean;

    function getLotes: TArray<RecEntradasNfItensLotes>;
    procedure setLotes(pValor: TArray<RecEntradasNfItensLotes>);
  public
    procedure setTipoControleEstoque(
      pTipoControleLote: string;
      pExigirDataFabricao: Boolean;
      pExigirDataVencimento: Boolean
    );
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
    procedure setProdutoId(pProdutoId: Integer);
  published
    property ProdutoId: Integer read FProdutoId write FProdutoId;
    property MultiploCompra: Double write FMultiploCompra;
    property Lotes: TArray<RecEntradasNfItensLotes> read getLotes write setLotes;
    property Tela: TTela read FTela write FTela;
  end;

implementation

{$R *.dfm}

const
  coQuantidade = 0;
  coLote       = 1;
  coBitola     = 2;
  coTonalidade = 3;
  coDataFabric = 4;
  coDataVencto = 5;
  coItemId     = 6;

function TFrBuscarLotes.getLotes: TArray<RecEntradasNfItensLotes>;
var
  i: Integer;
  vAuxiliar: TArray<RecEntradasNfItensLotes>;
begin
  vAuxiliar := nil;
  // Refazendo o vetor de lotes que chegou ao frame
  for i := Low(FLotes) to High(FLotes) do begin
    if FProdutoId = FLotes[i].ProdutoId then
      Continue;

    SetLength(vAuxiliar, Length(vAuxiliar) + 1);
    vAuxiliar[High(vAuxiliar)] := FLotes[i];
  end;

  for i := 1 to sgValores.RowCount -1 do begin
    if
      (FTipoControleEstoque = 'N')
      or
      (Em(FTipoControleEstoque, ['L', 'G']) and (sgValores.Cells[coLote, sgValores.Row] = ''))
      or
      ((FTipoControleEstoque = 'P') and (sgValores.Cells[coBitola, sgValores.Row] = '') and (sgValores.Cells[coTonalidade, sgValores.Row] = ''))
    then
      Continue;

    SetLength(vAuxiliar, Length(vAuxiliar) + 1);

    vAuxiliar[High(vAuxiliar)].ProdutoId      := FProdutoId;

    if Em(FTipoControleEstoque, ['G', 'L']) then
      vAuxiliar[High(vAuxiliar)].Lote := sgValores.Cells[coLote, i]
    else
      vAuxiliar[High(vAuxiliar)].Lote := sgValores.Cells[coBitola, i] + '-' + sgValores.Cells[coTonalidade, i];

    vAuxiliar[High(vAuxiliar)].Quantidade     := SFormatDouble(sgValores.Cells[coQuantidade, i]);
    vAuxiliar[High(vAuxiliar)].DataFabricacao := ToData(sgValores.Cells[coDataFabric, i]);
    vAuxiliar[High(vAuxiliar)].DataVencimento := ToData(sgValores.Cells[coDataVencto, i]);
    vAuxiliar[High(vAuxiliar)].ItemId         := SFormatInt(sgValores.Cells[coItemId, i]);
  end;

  Result := vAuxiliar;
end;

procedure TFrBuscarLotes.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;
  sgValores.MostrarColunas([coLote]);
  sgValores.OcultarColunas([coBitola, coTonalidade, coDataFabric, coDataVencto]);
end;

procedure TFrBuscarLotes.setLotes(pValor: TArray<RecEntradasNfItensLotes>);
var
  i: Integer;
  vLinha: Integer;
begin
  vLinha := 0;
  FLotes := pValor;

  for i := Low(pValor) to High(pValor) do begin
    if pValor[i].ProdutoId <> FProdutoId then
      Continue;

    Inc(vLinha);

    if Em(FTipoControleEstoque, ['L', 'G']) then
      sgValores.Cells[coLote, vLinha] := pValor[i].Lote
    else begin
      sgValores.Cells[coBitola, vLinha]     := Copy(pValor[i].Lote, 1, Pos('-', pValor[i].Lote) -1);
      sgValores.Cells[coTonalidade, vLinha] := Copy(pValor[i].Lote, Pos('-', pValor[i].Lote) + 1, Length(pValor[i].Lote));
    end;

    sgValores.Cells[coQuantidade, vLinha] := NFormatNEstoque(pValor[i].Quantidade);
    sgValores.Cells[coDataFabric, vLinha] := DFormatN(pValor[i].DataFabricacao);
    sgValores.Cells[coDataVencto, vLinha] := DFormatN(pValor[i].DataVencimento);
    sgValores.Cells[coItemId, vLinha]     := NFormat(pValor[i].ItemId);
  end;
  sgValores.SetLinhasGridPorTamanhoVetor( vLinha );
end;

procedure TFrBuscarLotes.setProdutoId(pProdutoId: Integer);
var
  i: Integer;
begin
  for i := Low(FLotes) to High(FLotes) do begin
    if FLotes[i].ProdutoId <> pProdutoId then
      Continue;

    setTipoControleEstoque( FLotes[i].TipoControleEstoque, FLotes[i].ExigirDataFabricacaoLote = 'S', FLotes[i].ExigirDataVencimentoLote = 'S' );
    Break;
  end;

  FProdutoId := pProdutoId;
  setLotes( FLotes );
end;

procedure TFrBuscarLotes.sgValoresArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
begin
  inherited;

  if ARow = 0 then
    Exit;

  if ACol = coQuantidade then begin
    TextCell := NFormatNEstoque( SFormatDouble(TextCell) );
    if TextCell = '' then
      Exit;

    if not ValidarMultiplo(SFormatDouble(sgValores.Cells[ACol, sgValores.Row]), FMultiploCompra) then begin
      TextCell := '';
      Exit;
    end;
  end
  else if ACol in[coDataFabric, coDataVencto] then
    TextCell :=_Biblioteca.FormatarData(TextCell);
end;

procedure TFrBuscarLotes.sgValoresDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = coQuantidade then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgValores.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFrBuscarLotes.sgValoresGetEditMask(Sender: TObject; ACol, ARow: Integer; var Value: string);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol in [coDataFabric, coDataVencto] then
    Value := '99/99/9999;1;_';
end;

procedure TFrBuscarLotes.sgValoresKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  inherited;

  if Key = VK_DOWN then begin
    if
      (sgValores.Row = sgValores.RowCount -1) and
      (
        (Em(FTipoControleEstoque, ['L', 'G']) and (sgValores.Cells[coLote, sgValores.Row] <> ''))
        or
        ((FTipoControleEstoque = 'P') and (sgValores.Cells[coBitola, sgValores.Row] <> '') and (sgValores.Cells[coTonalidade, sgValores.Row] <> ''))
      )
    then
      sgValores.RowCount := sgValores.RowCount + 1;
  end;
end;

procedure TFrBuscarLotes.sgValoresSelectCell(Sender: TObject; ACol,  ARow: Integer; var CanSelect: Boolean);
begin
  inherited;

  if FTela = ttDevolucaEntradas then begin
    sgValores.Options := sgValores.Options - [goEditing];
    if ACol = coQuantidade then
      sgValores.Options := sgValores.Options + [goEditing];
  end
  else begin

    sgValores.Options := sgValores.Options + [goEditing];

    if ACol in[coLote, coBitola, coTonalidade] then
      sgValores.OnKeyPress := TextoSemEspacoSemTracos
    else if ACol in[coDataFabric, coDataVencto] then begin
      sgValores.OnKeyPress := NumerosPonto;
      sgValores.Options := sgValores.Options - [goEditing];
      if (ACol = coDataFabric) and FExigirDataFabricao then
        sgValores.Options := sgValores.Options + [goEditing]
      else if (ACol = coDataVencto) and FExigirDataVencimento then
        sgValores.Options := sgValores.Options + [goEditing];
    end;
  end;
end;

procedure TFrBuscarLotes.setTipoControleEstoque(
  pTipoControleLote: string;
  pExigirDataFabricao: Boolean;
  pExigirDataVencimento: Boolean
);
begin
  Modo( Em(pTipoControleLote, ['L', 'G', 'P']) );
  FTipoControleEstoque  := pTipoControleLote;
  FExigirDataFabricao   := pExigirDataFabricao;
  FExigirDataVencimento := pExigirDataVencimento;

  if FExigirDataFabricao then
    sgValores.MostrarColunas([coDataFabric]);

  if FExigirDataVencimento then
    sgValores.MostrarColunas([coDataVencto]);

  if pTipoControleLote = 'L' then begin

  end
  else if pTipoControleLote = 'G' then begin

  end
  else if pTipoControleLote = 'P' then begin
    sgValores.MostrarColunas([coBitola, coTonalidade]);
    sgValores.OcultarColunas([coLote]);
  end;
end;

end.
