unit CadastroIndicesDescontosVenda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Biblioteca,
  CheckBoxLuka, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, ComboBoxLuka, _IndicesDescontosVenda,
  _Sessao, _RecordsEspeciais, PesquisaIndicesDescontosVenda;

type
  TFormCadastroIndicesDescontosVenda = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eDescricao: TEditLuka;
    lb2: TLabel;
    ePercentualDesconto: TEditLuka;
    ckPrecoCusto: TCheckBoxLuka;
    cbTipoDescontoPrecoCusto: TComboBoxLuka;
    lb3: TLabel;
    cbTipoCusto: TComboBoxLuka;
    lb4: TLabel;
  private
    procedure PreencherRegistro(pIndices: RecIndicesDescontosVenda);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroIndicesDescontosVenda }

procedure TFormCadastroIndicesDescontosVenda.BuscarRegistro;
var
  vDados: TArray<RecIndicesDescontosVenda>;
begin
  vDados := _IndicesDescontosVenda.BuscarIndicesDescontosVenda(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if vDados = nil then begin
    _Biblioteca.NenhumRegistro;
    SetarFoco(eID);
    Exit;
  end;

  inherited;
  PreencherRegistro(vDados[0]);
end;

procedure TFormCadastroIndicesDescontosVenda.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _IndicesDescontosVenda.ExcluirIndicesDescontosVenda(Sessao.getConexaoBanco, eId.AsInt);
  Sessao.AbortarSeHouveErro(vRetBanco);

  inherited;
end;

procedure TFormCadastroIndicesDescontosVenda.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;

  vRetBanco :=
    _IndicesDescontosVenda.AtualizarIndicesDescontosVenda(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eDescricao.Text,
      ckPrecoCusto.CheckedStr,
      ePercentualDesconto.AsInt,
      ckAtivo.CheckedStr,
      cbTipoCusto.AsString,
      cbTipoDescontoPrecoCusto.AsString
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormCadastroIndicesDescontosVenda.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    eDescricao,
    ePercentualDesconto,
    ckPrecoCusto,
    cbTipoCusto,
    cbTipoDescontoPrecoCusto],
    pEditando
  );

  if pEditando then begin
    SetarFoco(eDescricao);
    cbTipoDescontoPrecoCusto.ItemIndex := 0;
  end;
end;

procedure TFormCadastroIndicesDescontosVenda.PesquisarRegistro;
var
  vDado: RecIndicesDescontosVenda;
begin
  vDado := RecIndicesDescontosVenda(PesquisaIndicesDescontosVenda.Pesquisar);
  if vDado = nil then
    Exit;

  inherited;
  PreencherRegistro(vDado);
end;

procedure TFormCadastroIndicesDescontosVenda.PreencherRegistro(pIndices: RecIndicesDescontosVenda);
begin
  eID.AsInt                         := pIndices.IndiceId;
  eDescricao.Text                   := pIndices.Descricao;
  ckPrecoCusto.CheckedStr           := pIndices.PrecoCusto;
  ePercentualDesconto.AsInt         := pIndices.PercentualDesconto;
  ckAtivo.CheckedStr                := pIndices.Ativo;
  cbTipoCusto.AsString              := pIndices.TipoCusto;
  cbTipoDescontoPrecoCusto.AsString := pIndices.TipoDescontoPrecoCusto;
end;

procedure TFormCadastroIndicesDescontosVenda.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if eDescricao.Trim = '' then begin
    _Biblioteca.Exclamar('A descri��o n�o foi informada corretamente, verifique!');
    SetarFoco(eDescricao);
    Abort;
  end;

  if not EMultiplo(ePercentualDesconto.AsInt, 2) then begin
    _Biblioteca.Exclamar('O percentual de desconto s� pode ser um n�mero m�ltiplo de 2!');
    SetarFoco(ePercentualDesconto);
    Abort;
  end;

  if ePercentualDesconto.AsInt > 60 then begin
    _Biblioteca.Exclamar('O percentual de desconto n�o pode ser maior que 50%!');
    SetarFoco(ePercentualDesconto);
    Abort;
  end;

  if ckPrecoCusto.Checked then begin
    if cbTipoCusto.ItemIndex < 1 then begin
      _Biblioteca.Exclamar('Quando a op��o "Pre�o de custo" estiver marcada o "Tipo de custo" deve ser diferente de "Nenhum"!');
      SetarFoco(cbTipoCusto);
      Abort;
    end;

    if cbTipoDescontoPrecoCusto.AsString <> 'N' then begin
      if ePercentualDesconto.AsInt = 0 then begin
        _Biblioteca.Exclamar('O tipo de desconto no pre�o de custo est� diferente de "Nenhum" por�m n�o foi informado nenhum percentual, verifique!');
        SetarFoco(ePercentualDesconto);
        Abort;
      end;
    end
    else begin
      if ePercentualDesconto.AsInt > 0 then begin
        _Biblioteca.Exclamar('O tipo de desconto no pre�o de custo est� igual a "Nenhum" por�m foi informado percentual de desconto, verifique!');
        SetarFoco(cbTipoDescontoPrecoCusto);
        Abort;
      end;
    end;
  end
  else begin
    if cbTipoCusto.AsString <> 'N' then begin
      _Biblioteca.Exclamar('Quando a op��o "Pre�o de custo" estiver desmarcada o "Tipo de custo" deve ser igual a "Nenhum"!');
      SetarFoco(cbTipoCusto);
      Abort;
    end;

    if cbTipoDescontoPrecoCusto.AsString <> 'N' then begin
      _Biblioteca.Exclamar('O tipo de desconto no pre�o de custo s� pode ser informado se a op��o "Pre�o custo" estiver marcada, verifique!');
      SetarFoco(eDescricao);
      Abort;
    end;

    if ePercentualDesconto.AsInt = 0 then begin
      _Biblioteca.Exclamar('Nenhum percentual de desconto foi informado, verifique!');
      SetarFoco(ePercentualDesconto);
      Abort;
    end;
  end;
end;

end.
