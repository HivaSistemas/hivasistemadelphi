unit Buscar.CfopCstOperacoes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.StdCtrls, _Biblioteca,
  ComboBoxLuka, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameCFOPs,
  Vcl.Buttons, Vcl.ExtCtrls;

type
  RecOperacoesCST = record
    CfopId: string;
    TipoOperacao: string;
    TipoOperacaoAnalitico: string;
    TipoMovimento: string;
    TipoMovimentoAnalitico: string;
    Cst: string;
  end;

  TFormBuscarCfopCstOperacoes = class(TFormHerancaFinalizar)
    FrCFOP: TFrCFOPs;
    cbTipoMovimento: TComboBoxLuka;
    Label1: TLabel;
    Label2: TLabel;
    cbTipoOperacao: TComboBoxLuka;
    cbCST: TComboBoxLuka;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Buscar(pCFOPId: Integer; pTipoMovimento: string; pTipoOperacao: string; pCST: string): TRetornoTelaFinalizar<RecOperacoesCST>;

implementation

{$R *.dfm}

function Buscar(pCFOPId: Integer; pTipoMovimento: string; pTipoOperacao: string; pCST: string): TRetornoTelaFinalizar<RecOperacoesCST>;
var
  vForm: TFormBuscarCfopCstOperacoes;
begin
  vForm := TFormBuscarCfopCstOperacoes.Create(nil);

  if pCFOPId > 0 then begin
    vForm.FrCFOP.InserirDadoPorChave(pCFOPId);
    vForm.cbTipoMovimento.SetIndicePorValor(pTipoMovimento);
    vForm.cbTipoOperacao.SetIndicePorValor(pTipoOperacao);
    vForm.cbCST.SetIndicePorValor(pCst);
  end;

  if Result.Ok(vForm.ShowModal, True) then begin
    Result.Dados.CfopId                 := vForm.FrCFOP.GetCFOP().CfopPesquisaId;
    Result.Dados.TipoOperacao           := vForm.cbTipoOperacao.GetValor;
    Result.Dados.TipoOperacaoAnalitico  := vForm.cbTipoOperacao.Text;
    Result.Dados.TipoMovimento          := vForm.cbTipoMovimento.GetValor;
    Result.Dados.TipoMovimentoAnalitico := vForm.cbTipoMovimento.Text;
    Result.Dados.Cst                    := vForm.cbCST.Text;
  end;
end;

{ TFormBuscarCfopCstOperacoes }

procedure TFormBuscarCfopCstOperacoes.FormCreate(Sender: TObject);
begin
  inherited;
  SetarFoco(FrCFOP);
end;

procedure TFormBuscarCfopCstOperacoes.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrCFOP.EstaVazio then begin
    _Biblioteca.Exclamar('O CFOP n�o foi informado corretamente, verifique!');
    SetarFoco(FrCFOP);
    Abort;
  end;

  if cbTipoMovimento.ItemIndex < 0 then begin
    _Biblioteca.Exclamar('O Tipo de movimento n�o foi informado corretamente, verifique!');
    SetarFoco(cbTipoMovimento);
    Abort;
  end;

  if cbTipoOperacao.ItemIndex < 0 then begin
    _Biblioteca.Exclamar('O Tipo de opera��o n�o foi informado corretamente, verifique!');
    SetarFoco(cbTipoOperacao);
    Abort;
  end;

  if cbCST.ItemIndex < 0 then begin
    _Biblioteca.Exclamar('A CST n�o foi informado corretamente, verifique!');
    SetarFoco(cbCST);
    Abort;
  end;
end;

end.
