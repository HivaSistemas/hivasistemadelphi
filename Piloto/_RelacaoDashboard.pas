unit _RelacaoDashboard;

interface

uses
_Conexao, _OperacoesBancoDados, _Biblioteca;

type
  RecVendas = record
    ValorTotal: Double;
    PercentualLucro: Double;
    ValorLucro: Double;
    Quantidade: Integer;
  end;

function QuantidadeOrcamentosBloqueados(pConexao: TConexao; pEmpresaId: Integer): Integer;

function QuantidadeTitulosPagarVencendoHoje(pConexao: TConexao; pEmpresaId: Integer): Integer;
function ValorTitulosPagarVencendoHoje(pConexao: TConexao; pEmpresaId: Integer): Double;
function QuantidadeTitulosPagarAberto(pConexao: TConexao; pEmpresaId: Integer): Integer;
function ValorTitulosPagarAberto(pConexao: TConexao; pEmpresaId: Integer): Double;

function QuantidadeTitulosReceberVencendoHoje(pConexao: TConexao; pEmpresaId: Integer): Integer;
function ValorTitulosReceberVencendoHoje(pConexao: TConexao; pEmpresaId: Integer): Double;
function QuantidadeTitulosReceberAberto(pConexao: TConexao; pEmpresaId: Integer): Integer;
function ValorTitulosReceberAberto(pConexao: TConexao; pPercentualMulta: Double; pPercentualJuros: Double; pEmpresaId: Integer): Double;

function ValorVendasDiaAtual(pConexao: TConexao; pEmpresaId: Integer): Double;
function ValorVendasDiaAnterior(pConexao: TConexao; pEmpresaId: Integer): Double;
function ValorVendasMes(pConexao: TConexao; pSqlDate: string; pEmpresaId: Integer): RecVendas;

function QtdeOrcamentosAbertosMesAtual(pConexao: TConexao; pSqlDate: string; pEmpresaId: Integer): Integer;
function ValorOrcamentosAbertosMesAtual(pConexao: TConexao; pSqlDate: string; pEmpresaId: Integer): Double;

function QtdeOrcamentosAbertosMesAnterior(pConexao: TConexao; pSqlDate: string; pEmpresaId: Integer): Integer;
function ValorOrcamentosAbertosMesAnterior(pConexao: TConexao; pSqlDate: string; pEmpresaId: Integer): Double;

function QtdeDevolucoesMesAtual(pConexao: TConexao; pSqlDate: string; pEmpresaId: Integer): Integer;
function ValorDevolucoesMesAtual(pConexao: TConexao; pSqlDate: string; pEmpresaId: Integer): RecVendas;

function VendasDiaAtual(pConexao: TConexao; vSqlPeriodo: string; pEmpresaId: Integer): RecVendas;

function getPercLucroLiquido(
  pValorTotalLiquido: Double;
  pValorTotalLucroLiquido: Double;
  pDecimais: Integer = 2
): Double;

implementation

function QuantidadeOrcamentosBloqueados(pConexao: TConexao; pEmpresaId: Integer): Integer;
var
  vSql: TConsulta;
begin
  Result := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('  select');
  vSql.Add('    count(*)');
  vSql.Add('  from ORCAMENTOS');
  vSql.Add('  where STATUS in (''VB'', ''OB'')');
  vSql.Add('  and EMPRESA_ID = :P1');

  vSql.Pesquisar([pEmpresaId]);
  Result := vSql.GetInt(0);
  vSql.Active := False;
  vSql.Free;
end;

function QuantidadeTitulosPagarVencendoHoje(pConexao: TConexao; pEmpresaId: Integer): Integer;
var
  vSql: TConsulta;
begin
  Result := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('  select ');
  vSql.Add('    count(*) ');
  vSql.Add('  from CONTAS_PAGAR ');
  vSql.Add('  where DATA_VENCIMENTO = trunc(sysdate) and STATUS = ''A'' ');
  vSql.Add('  and EMPRESA_ID = :P1');

  vSql.Pesquisar([pEmpresaId]);
  Result := vSql.GetInt(0);
  vSql.Active := False;
  vSql.Free;
end;

function ValorTitulosPagarVencendoHoje(pConexao: TConexao; pEmpresaId: Integer): Double;
var
  vSql: TConsulta;
begin
  Result := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('  select ');
  vSql.Add('    SUM(VALOR_DOCUMENTO - VALOR_ADIANTADO - VALOR_DESCONTO) ');
  vSql.Add('  from CONTAS_PAGAR ');
  vSql.Add('  where DATA_VENCIMENTO = trunc(sysdate) ');
  vSql.Add('  and status = ''A'' ');
  vSql.Add('  and EMPRESA_ID = :P1');

  vSql.Pesquisar([pEmpresaId]);
  Result := vSql.GetDouble(0);
  vSql.Active := False;
  vSql.Free;
end;

function QuantidadeTitulosPagarAberto(pConexao: TConexao; pEmpresaId: Integer): Integer;
var
  vSql: TConsulta;
begin
  Result := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('  select ');
  vSql.Add('    count(*) ');
  vSql.Add('  from CONTAS_PAGAR ');
  vSql.Add('  where STATUS = ''A'' ');
  vSql.Add('  and EMPRESA_ID = :P1');

  vSql.Pesquisar([pEmpresaId]);
  Result := vSql.GetInt(0);
  vSql.Active := False;
  vSql.Free;
end;

function ValorTitulosPagarAberto(pConexao: TConexao; pEmpresaId: Integer): Double;
var
  vSql: TConsulta;
begin
  Result := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('  select ');
  vSql.Add('    SUM(VALOR_DOCUMENTO - VALOR_ADIANTADO - VALOR_DESCONTO) ');
  vSql.Add('  from CONTAS_PAGAR ');
  vSql.Add('  where status = ''A'' ');
  vSql.Add('  and EMPRESA_ID = :P1');

  vSql.Pesquisar([pEmpresaId]);
  Result := vSql.GetDouble(0);
  vSql.Active := False;
  vSql.Free;
end;

function QuantidadeTitulosReceberVencendoHoje(pConexao: TConexao; pEmpresaId: Integer): Integer;
var
  vSql: TConsulta;
begin
  Result := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('  select ');
  vSql.Add('    count(*) ');
  vSql.Add('  from CONTAS_RECEBER ');
  vSql.Add('  where DATA_VENCIMENTO = trunc(sysdate) and STATUS = ''A'' ');
  vSql.Add('  and EMPRESA_ID = :P1');

  vSql.Pesquisar([pEmpresaId]);
  Result := vSql.GetInt(0);
  vSql.Active := False;
  vSql.Free;
end;

function ValorTitulosReceberVencendoHoje(pConexao: TConexao; pEmpresaId: Integer): Double;
var
  vSql: TConsulta;
begin
  Result := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('  select ');
  vSql.Add('    SUM(VALOR_DOCUMENTO - VALOR_ADIANTADO - VALOR_RETENCAO) ');
  vSql.Add('  from CONTAS_RECEBER ');
  vSql.Add('  where DATA_VENCIMENTO = trunc(sysdate) ');
  vSql.Add('  and status = ''A'' ');
  vSql.Add('  and EMPRESA_ID = :P1');

  vSql.Pesquisar([pEmpresaId]);
  Result := vSql.GetDouble(0);
  vSql.Active := False;
  vSql.Free;
end;

function QuantidadeTitulosReceberAberto(pConexao: TConexao; pEmpresaId: Integer): Integer;
var
  vSql: TConsulta;
begin
  Result := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('  select ');
  vSql.Add('    count(*) ');
  vSql.Add('  from CONTAS_RECEBER ');
  vSql.Add('  where STATUS = ''A'' ');
  vSql.Add('  and EMPRESA_ID = :P1');

  vSql.Pesquisar([pEmpresaId]);
  Result := vSql.GetInt(0);
  vSql.Active := False;
  vSql.Free;
end;

function ValorTitulosReceberAberto(pConexao: TConexao; pPercentualMulta: Double; pPercentualJuros: Double; pEmpresaId: Integer): Double;
var
  vSql: TConsulta;
  valor_cauculado: Double;
  tipo_cobranca: string;
  i: Integer;
begin
  Result := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  CON.VALOR_DOCUMENTO, ');
  vSql.Add('  CON.VALOR_ADIANTADO, ');
  vSql.Add('  CON.VALOR_RETENCAO, ');
  vSql.Add('  case when trunc(sysdate) - CON.DATA_VENCIMENTO > 0 then trunc(sysdate) - CON.DATA_VENCIMENTO else 0 end as DIAS_ATRASO, ');
  vSql.Add('  TCO.FORMA_PAGAMENTO ');
  vSql.Add('from CONTAS_RECEBER CON ');

  vSql.Add('inner join TIPOS_COBRANCA TCO ');
  vSql.Add('on CON.COBRANCA_ID = TCO.COBRANCA_ID ');

  vSql.Add('where CON.STATUS = ''A'' ');
  vSql.Add('and CON.EMPRESA_ID = :P1 ');

  vSql.Pesquisar([pEmpresaId]);
  valor_cauculado := 0;
  for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
    tipo_cobranca := vSql.GetString('FORMA_PAGAMENTO');
    valor_cauculado :=
      valor_cauculado +
      vSql.GetDouble('VALOR_DOCUMENTO') -
      vSql.GetDouble('VALOR_ADIANTADO') -
      vSql.GetDouble('VALOR_RETENCAO') +
      getValorMulta(IIfDbl(tipo_cobranca <> 'CRT', vSql.GetDouble('VALOR_DOCUMENTO'), 0), pPercentualMulta, vSql.getInt('DIAS_ATRASO')) +
      getValorJuros(IIfDbl(tipo_cobranca <> 'CRT', vSql.GetDouble('VALOR_DOCUMENTO'), 0), pPercentualJuros, vSql.getInt('DIAS_ATRASO'));
      vSql.Next;
  end;
  Result := valor_cauculado;
  vSql.Active := False;
  vSql.Free;
end;

function ValorVendasDiaAtual(pConexao: TConexao; pEmpresaId: Integer): Double;
var
  vSql: TConsulta;
begin
  Result := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('  SELECT ');
  vSql.Add('    SUM(valor_total) ');
  vSql.Add('  FROM orcamentos ');
  vSql.Add('  WHERE empresa_id = :P1 ');
  vSql.Add('  AND status = ''RE'' ');
  vSql.Add('  AND TRUNC(data_hora_recebimento) = TRUNC(sysdate) ');

  vSql.Pesquisar([pEmpresaId]);
  Result := vSql.GetDouble(0);
  vSql.Active := False;
  vSql.Free;
end;

function ValorVendasDiaAnterior(pConexao: TConexao; pEmpresaId: Integer): Double;
var
  vSql: TConsulta;
begin
  Result := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('  SELECT ');
  vSql.Add('    SUM(valor_total) ');
  vSql.Add('  FROM orcamentos ');
  vSql.Add('  WHERE empresa_id = :P1 ');
  vSql.Add('  AND status = ''RE'' ');
  vSql.Add('  AND TRUNC(data_hora_recebimento) = TRUNC(sysdate) - 1 ');

  vSql.Pesquisar([pEmpresaId]);
  Result := vSql.GetDouble(0);
  vSql.Active := False;
  vSql.Free;
end;

function ValorVendasMes(pConexao: TConexao; pSqlDate: string; pEmpresaId: Integer): RecVendas;
var
  vSql: TConsulta;
  i: Integer;
begin
  Result.ValorTotal := 0;
  Result.Quantidade := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('SELECT ');
  vSql.Add('  SUM(VALOR_TOTAL) as VALOR_TOTAL, ');
  vSql.Add('  trunc(DATA_HORA_RECEBIMENTO) as DATA_RECEBIMENTO ');
  vSql.Add('FROM ORCAMENTOS ');
  vSql.Add('WHERE EMPRESA_ID = :P1 ');
  vSql.Add('AND STATUS = ''RE'' ');
  vSql.Add(pSqlDate);
  vSql.Add('GROUP BY trunc(DATA_HORA_RECEBIMENTO) ');

  vSql.Pesquisar([pEmpresaId]);

  for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
    Result.ValorTotal := Result.ValorTotal + vSql.GetDouble('VALOR_TOTAL');
    vSql.Next;
  end;

  Result.Quantidade := vSql.GetQuantidadeRegistros;

  vSql.Active := False;
  vSql.Free;
end;

function QtdeOrcamentosAbertosMesAtual(pConexao: TConexao; pSqlDate: string; pEmpresaId: Integer): Integer;
var
  vSql: TConsulta;
begin
  Result := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('  SELECT ');
  vSql.Add('    count(*) ');
  vSql.Add('  FROM orcamentos ');
  vSql.Add('  WHERE empresa_id = :P1 ');
  vSql.Add('  AND status = ''OE'' ');
  vSql.Add(pSqlDate);

  vSql.Pesquisar([pEmpresaId]);
  Result := vSql.GetInt(0);
  vSql.Active := False;
  vSql.Free;
end;

function ValorOrcamentosAbertosMesAtual(pConexao: TConexao; pSqlDate: string; pEmpresaId: Integer): Double;
var
  vSql: TConsulta;
begin
  Result := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('  SELECT ');
  vSql.Add('    SUM(valor_total) ');
  vSql.Add('  FROM orcamentos ');
  vSql.Add('  WHERE empresa_id = :P1 ');
  vSql.Add('  AND status = ''OE'' ');
  vSql.Add(pSqlDate);

  vSql.Pesquisar([pEmpresaId]);
  Result := vSql.GetDouble(0);
  vSql.Active := False;
  vSql.Free;
end;

function QtdeOrcamentosAbertosMesAnterior(pConexao: TConexao; pSqlDate: string; pEmpresaId: Integer): Integer;
var
  vSql: TConsulta;
begin
  Result := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('  SELECT ');
  vSql.Add('    count(*) ');
  vSql.Add('  FROM orcamentos ');
  vSql.Add('  WHERE empresa_id = :P1 ');
  vSql.Add('  AND status = ''OE'' ');
  vSql.Add(pSqlDate);

  vSql.Pesquisar([pEmpresaId]);
  Result := vSql.GetInt(0);
  vSql.Active := False;
  vSql.Free;
end;

function ValorOrcamentosAbertosMesAnterior(pConexao: TConexao; pSqlDate: string; pEmpresaId: Integer): Double;
var
  vSql: TConsulta;
begin
  Result := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('  SELECT ');
  vSql.Add('    SUM(valor_total) ');
  vSql.Add('  FROM orcamentos ');
  vSql.Add('  WHERE empresa_id = :P1 ');
  vSql.Add('  AND status = ''OE'' ');
  vSql.Add(pSqlDate);

  vSql.Pesquisar([pEmpresaId]);
  Result := vSql.GetDouble(0);
  vSql.Active := False;
  vSql.Free;
end;

function VendasDiaAtual(pConexao: TConexao; vSqlPeriodo: string; pEmpresaId: Integer): RecVendas;
var
  vSql: TConsulta;
  valor_cauculado: Double;
  i: Integer;
begin
  //Result := 0;
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  ORC.VALOR_TOTAL, ');
  vSql.Add('  CUS.VALOR_CUSTO_ENTRADA, ');
  vSql.Add('  CUS.VALOR_CUSTO_FINAL ');
  vSql.Add('from ORCAMENTOS ORC ');

  vSql.Add('inner join VW_CUSTOS_ORCAMENTOS CUS ');
  vSql.Add('on CUS.ORCAMENTO_ID = ORC.ORCAMENTO_ID ');

  vSql.Add('where ORC.EMPRESA_ID = :P1 and ORC.STATUS = ''RE''');
  vSql.Add(vSqlPeriodo);

  vSql.Pesquisar([pEmpresaId]);
  Result.ValorTotal := 0;
  Result.PercentualLucro := 0;
  Result.ValorLucro := 0;
  for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
    Result.ValorTotal := Result.ValorTotal + vSql.GetDouble('VALOR_TOTAL');
    Result.ValorLucro := Result.ValorLucro + vSql.GetDouble('VALOR_TOTAL') - vSql.GetDouble('VALOR_CUSTO_FINAL');
    Result.PercentualLucro := getPercLucroLiquido(vSql.GetDouble('VALOR_TOTAL'), vSql.GetDouble('VALOR_TOTAL') - vSql.GetDouble('VALOR_CUSTO_FINAL'), 4);
    vSql.Next;
  end;

  Result.PercentualLucro := getPercLucroLiquido(Result.ValorTotal, Result.ValorLucro, 4);
  vSql.Active := False;
  vSql.Free;
end;

function getPercLucroLiquido(
  pValorTotalLiquido: Double;
  pValorTotalLucroLiquido: Double;
  pDecimais: Integer = 2
): Double;
begin
  Result := 0;

  if _Biblioteca.NFormatN(pValorTotalLiquido) = '' then
    Exit;

  Result := Arredondar(pValorTotalLucroLiquido / pValorTotalLiquido * 100, pDecimais);
end;

function QtdeDevolucoesMesAtual(pConexao: TConexao; pSqlDate: string; pEmpresaId: Integer): Integer;
begin

end;

function ValorDevolucoesMesAtual(pConexao: TConexao; pSqlDate: string; pEmpresaId: Integer): RecVendas;
var
  vSql: TConsulta;
  valor_cauculado: Double;
  i: Integer;
begin
  vSql := TConsulta.Create(pConexao);

  vSql.Add('select ');
  vSql.Add('  VALOR_LIQUIDO AS VALOR_TOTAL, ');
  vSql.Add('  VALOR_CUSTO_ENTRADA, ');
  vSql.Add('  VALOR_CUSTO_FINAL ');
  vSql.Add('from VW_RELACAO_DEVOLUCOES ');

  vSql.Add('where EMPRESA_ID = :P1 AND STATUS in(''RE'')');
  vSql.Add(pSqlDate);

  vSql.Pesquisar([pEmpresaId]);
  Result.ValorTotal := 0;
  Result.PercentualLucro := 0;
  Result.ValorLucro := 0;
  for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
    Result.ValorTotal := Result.ValorTotal + vSql.GetDouble('VALOR_TOTAL');
    Result.ValorLucro := Result.ValorLucro + vSql.GetDouble('VALOR_TOTAL') - vSql.GetDouble('VALOR_CUSTO_FINAL');
    Result.PercentualLucro := getPercLucroLiquido(vSql.GetDouble('VALOR_TOTAL'), vSql.GetDouble('VALOR_TOTAL') - vSql.GetDouble('VALOR_CUSTO_FINAL'), 4);
    vSql.Next;
  end;
  Result.Quantidade := vSql.GetQuantidadeRegistros;
  Result.PercentualLucro := getPercLucroLiquido(Result.ValorTotal, Result.ValorLucro, 4);
  vSql.Active := False;
  vSql.Free;
end;

end.
