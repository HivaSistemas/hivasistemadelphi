unit Buscar.UnidadeMultiplo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.StdCtrls, _Biblioteca,
  EditLuka, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameUnidades,
  Vcl.Buttons, Vcl.ExtCtrls;

type
  TUnidadeMultiplo = record
    Unidade: string;
    Multiplo: Double;
    QuantidadeEmbalagem: Double;
  end;

  TFormBuscarUnidadeMultiplo = class(TFormHerancaFinalizar)
    FrUnidade: TFrUnidades;
    eMultiplo: TEditLuka;
    lb1: TLabel;
    eQtdeEmbalagem: TEditLuka;
    lbl1: TLabel;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Buscar(): TRetornoTelaFinalizar<TUnidadeMultiplo>;

implementation

{$R *.dfm}

function Buscar(): TRetornoTelaFinalizar<TUnidadeMultiplo>;
var
  vForm: TFormBuscarUnidadeMultiplo;
begin
  vForm := TFormBuscarUnidadeMultiplo.Create(Application);

  if Result.Ok(vForm.ShowModal, True) then begin
    Result.Dados.Unidade  := vForm.FrUnidade.getUnidade().unidade_id;
    Result.Dados.Multiplo := vForm.eMultiplo.AsDouble;
    Result.Dados.QuantidadeEmbalagem := vForm.eQtdeEmbalagem.AsDouble;
  end;
end;

{ TFormBuscarUnidadeMultiplo }

procedure TFormBuscarUnidadeMultiplo.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrUnidade.EstaVazio then begin
    Exclamar('A unidade n�o foi informada corretamente, verifique!');
    SetarFoco(FrUnidade);
    Abort;
  end;

  if eMultiplo.AsCurr = 0 then begin
    Exclamar('O m�ltiplo n�o foi informado corretamente, verifique!');
    SetarFoco(eMultiplo);
    Abort;
  end;

  if eQtdeEmbalagem.AsCurr = 0 then begin
    Exclamar('A quantidade por embalagem n�o foi informado corretamente, verifique!');
    SetarFoco(eQtdeEmbalagem);
    Abort;
  end;
end;

end.
