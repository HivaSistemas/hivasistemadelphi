inherited FrICMS: TFrICMS
  Width = 836
  Height = 394
  ExplicitWidth = 836
  ExplicitHeight = 394
  object pnGrid: TPanel
    Left = 0
    Top = 0
    Width = 836
    Height = 394
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object sgICMS: TGridLuka
      Left = 0
      Top = 0
      Width = 746
      Height = 394
      Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
      Align = alClient
      ColCount = 13
      DefaultRowHeight = 19
      DrawingStyle = gdsGradient
      FixedColor = 15395562
      FixedCols = 0
      RowCount = 28
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
      PopupMenu = pmOpcoes
      TabOrder = 0
      OnDrawCell = sgICMSDrawCell
      OnKeyPress = Numeros
      OnSelectCell = sgICMSSelectCell
      IncrementExpandCol = 0
      IncrementExpandRow = 0
      CorLinhaFoco = 38619
      CorFundoFoco = 16774625
      CorLinhaDesfoque = 14869218
      CorFundoDesfoque = 16382457
      CorSuperiorCabecalho = clWhite
      CorInferiorCabecalho = 13553358
      CorSeparadorLinhas = 12040119
      CorColunaFoco = clActiveCaption
      HCol.Strings = (
        'Estado'
        'N'#227'o cont.'
        'Cont.'
        'Org.p'#250'bl.'
        'Reven.'
        'Constr.'
        'Cl'#237'n./Hosp.  '
        'Prod.rural'
        '% Aliq.ICMS'
        '% Aliq. ICMS Inter.'
        '% IVA'
        'Ind.reducao'
        'Pre'#231'o pauta')
      HRow.Strings = (
        'AC'
        'AL'
        'AP'
        'AM'
        'BA'
        'CE'
        'DF'
        'ES'
        'GO'
        'MA'
        'MT'
        'MS'
        'MG'
        'PA'
        'PB'
        'PR'
        'PE'
        'PI'
        'RJ'
        'RN'
        'RS'
        'RO'
        'RR'
        'SC'
        'SP'
        'SE'
        'TO')
      Grid3D = False
      RealColCount = 19
      Indicador = True
      AtivarPopUpSelecao = False
      ColWidths = (
        46
        59
        44
        56
        47
        45
        69
        59
        75
        111
        48
        82
        64)
    end
    object Panel1: TPanel
      Left = 746
      Top = 0
      Width = 90
      Height = 394
      Align = alRight
      Anchors = []
      BevelOuter = bvNone
      TabOrder = 1
      object lb1: TLabel
        Left = 5
        Top = 4
        Width = 66
        Height = 14
        Caption = 'Estado sel.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbEstadoSelecionado: TLabel
        Left = 5
        Top = 19
        Width = 26
        Height = 13
        Caption = 'Goi'#225's'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 38619
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object sbPreencher: TSpeedButton
        Left = 8
        Top = 78
        Width = 75
        Height = 26
        Caption = 'Pre. todos'
        Flat = True
        OnClick = sbPreencherClick
      end
      object eValor: TEditLuka
        Left = 3
        Top = 55
        Width = 85
        Height = 21
        Hint = 'Valor a ser preenchido em todos os estados'
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 0
        OnKeyDown = eValorKeyDown
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
    end
  end
  object pmOpcoes: TPopupMenu
    Left = 624
    Top = 296
  end
end
