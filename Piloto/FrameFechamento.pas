unit FrameFechamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.StdCtrls, _Biblioteca,
  EditLuka, Vcl.Buttons, BuscarDadosCheques, BuscarDadosCartoes, _RecordsFinanceiros, _ContasPagar,
  _TiposCobrancaCondicaoPagamento, _Sessao, BuscarDadosCobrancas, _TiposCobranca, BuscarCreditosDisponiveis,
  _BibliotecaGenerica, FramePagamentoFinanceiro, BuscarDadosDinheiro;

type
  TNotificarTroco = procedure(pValorTroco: Currency) of object;
  TTelaChamada = (ttNaoDefinida, ttOrcamentosVendas, ttRecebimento, ttAcumulado);
  TNotificarValorAlterado = procedure(pValorOutrasDespesas: Currency; pValorDesconto: Currency; pValorTotal: Currency) of object;

  TFrFechamento = class(TFrameHerancaPrincipal)
    gbFechamento: TGroupBox;
    lb19: TLabel;
    lb20: TLabel;
    lb21: TLabel;
    lb22: TLabel;
    lb7: TLabel;
    lb11: TLabel;
    lb12: TLabel;
    lb13: TLabel;
    lb14: TLabel;
    lb15: TLabel;
    sbBuscarDadosCheques: TSpeedButton;
    sbBuscarDadosCartoesDebito: TSpeedButton;
    sbBuscarDadosCobranca: TSpeedButton;
    lb10: TLabel;
    eValorTotalASerPago: TEditLuka;
    ePercentualOutrasDespesas: TEditLuka;
    eValorDesconto: TEditLuka;
    eValorOutrasDespesas: TEditLuka;
    ePercentualDesconto: TEditLuka;
    eValorDinheiro: TEditLuka;
    eValorCheque: TEditLuka;
    eValorCartaoDebito: TEditLuka;
    eValorCobranca: TEditLuka;
    eValorCredito: TEditLuka;
    stSPC: TStaticText;
    eValorTroco: TEditLuka;
    lb1: TLabel;
    eValorAcumulativo: TEditLuka;
    lb2: TLabel;
    eValorFinanceira: TEditLuka;
    sbBuscarDadosFinanceira: TSpeedButton;
    sbBuscarCreditos: TSpeedButton;
    lb3: TLabel;
    eValorFrete: TEditLuka;
    lb4: TLabel;
    eValorCartaoCredito: TEditLuka;
    sbBuscarDadosCartoesCredito: TSpeedButton;
    Label1: TLabel;
    eValorPix: TEditLuka;
    sbBuscarDadosCobrancaPix: TSpeedButton;
    procedure ePercentualOutrasDespesasChange(Sender: TObject);
    procedure ePercentualDescontoChange(Sender: TObject);
    procedure eValorOutrasDespesasChange(Sender: TObject);
    procedure eValorDinheiroChange(Sender: TObject);
    procedure eValorDescontoChange(Sender: TObject);
    procedure sbBuscarDadosCartoesDebitoClick(Sender: TObject);
    procedure sbBuscarDadosChequesClick(Sender: TObject);
    procedure eValorChequeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eValorCartaoDebitoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbBuscarDadosCobrancaClick(Sender: TObject);
    procedure eValorCobrancaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eValorChequeExit(Sender: TObject);
    procedure eValorCartaoDebitoExit(Sender: TObject);
    procedure eValorCobrancaExit(Sender: TObject);
    procedure eValorTotalASerPagoChange(Sender: TObject);
    procedure sbBuscarCreditosClick(Sender: TObject);
    procedure sbBuscarDadosCartoesCreditoClick(Sender: TObject);
    procedure eValorCartaoCreditoExit(Sender: TObject);
    procedure eValorCartaoCreditoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eValorDinheiroDblClick(Sender: TObject);
    procedure sbBuscarDadosCobrancaPixClick(Sender: TObject);
    procedure eValorPixExit(Sender: TObject);
    procedure eValorPixKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  protected
    FTotalProdutos: Double;
    FTotalPromocional: Double;
    FTotalAtacado: Double;
  private
    FCadastroId: Integer;
    FSomenteLeitura: Boolean;

    FCondicaoPagamentoId: Integer;
    FNomeCondicao: string;
    FPrazoMedio: Double;

    FTelaChamada: TTelaChamada;
    FNotificarValorAlterado: TNotificarValorAlterado;

    FDadosCartoesDebito: TArray<RecTitulosFinanceiros>;
    FDadosCartoesCredito: TArray<RecTitulosFinanceiros>;
    FDadosCobranca: TArray<RecTitulosFinanceiros>;
    FDadosCheques: TArray<RecTitulosFinanceiros>;
    FCreditos: TArray<Integer>;
    FCreditosAcumuladosIds: TArray<Integer>;

    procedure setTelaChamada(const pValor: TTelaChamada);
    procedure setCadastroId(pCadastroId: Integer);
    procedure setTotalFrete(const pValor: Double);
    procedure setPedidosAcumuladosIds(pValor: TArray<Integer>);
    function  getTotalFrete: Double;
    function  getCartoes: TArray<RecTitulosFinanceiros>;
  public
    FDadosPix: TArray<RecPagamentosPix>;
    constructor Create(AOwner: TComponent); override;
    procedure setTotalProdutos(const pValor: Double);
    procedure setTotalPromocional(const pValor: Double);
    procedure setTotalAtacado(const pValor: Double);
    procedure setUtilizaTef(pUtilizarTEF: Boolean);

    procedure setCondicaoPagamento(
      const pCondicaoId: Integer;
      const pNomeCondicao: string;
      const pPrazoMedio: Double
    );

    procedure Clear; override;
    procedure LimparDadosTiposCobranca(pLimparCondicaoPagto: Boolean);
    procedure SomenteLeitura(pValue: Boolean); override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;

    procedure PermitirEditarTotalPagar(const pPermitir: Boolean);
    procedure PermitirEditarDesconto(const pPermitir: Boolean);
    procedure PermitirEditarValoresFormasPagamento(const pPermitir: Boolean);

    function TemBoletoBancario: Boolean;
    function TemDuplicataMercantil: Boolean;
    procedure setRecebimentoNaEntrega;

    function getFormasPagamentoMaiorTotalPagarEUtilizandoCredito: Boolean;
    function getCreditoMaiorTotalPagarETemFormasPagamento: Boolean;
    procedure TotalizarValorASerPago; virtual;
    procedure verificarCartoes;
    function TemValoresComprometemCreditoCliente: Boolean;
    function getValoresComprometemCreditoCliente: Currency;
  published
    property Cartoes: TArray<RecTitulosFinanceiros> read getCartoes;  // Cart�es de cr�dito e d�bito juntos

    property CartoesDebito: TArray<RecTitulosFinanceiros> read FDadosCartoesDebito write FDadosCartoesDebito;
    property CartoesCredito: TArray<RecTitulosFinanceiros> read FDadosCartoesCredito write FDadosCartoesCredito;
    property Cobrancas: TArray<RecTitulosFinanceiros> read FDadosCobranca write FDadosCobranca;
    property Cheques: TArray<RecTitulosFinanceiros> read FDadosCheques write FDadosCheques;
    property Creditos: TArray<Integer> read FCreditos write FCreditos;

    property CadastroId: Integer read FCadastroId write setCadastroId;
    property ValorFrete: Double read getTotalFrete write setTotalFrete;
    property TelaChamada: TTelaChamada write setTelaChamada default ttNaoDefinida;
    property PedidosAcumuladosIds: TArray<Integer> write setPedidosAcumuladosIds;
    property OnValorAlteracao: TNotificarValorAlterado read FNotificarValorAlterado write FNotificarValorAlterado;
  end;

implementation

{$R *.dfm}

procedure TFrFechamento.Clear;
begin
  inherited;

  _Biblioteca.LimparCampos([
    eValorOutrasDespesas,
    ePercentualOutrasDespesas,
    eValorDesconto,
    ePercentualDesconto,
    eValorTotalASerPago,
    eValorDinheiro,
    eValorPix,
    eValorCheque,
    eValorCartaoDebito,
    eValorCartaoCredito,
    eValorCobranca,
    eValorFinanceira,
    eValorAcumulativo,
    eValorCredito,
    eValorTroco]
  );
end;

constructor TFrFechamento.Create(AOwner: TComponent);
begin
  inherited;
  FTotalProdutos := 0;
  FTotalAtacado := 0;

  FDadosCheques        := nil;
  FDadosCartoesDebito  := nil;
  FDadosCartoesCredito := nil;
  FDadosCobranca       := nil;
  FCreditosAcumuladosIds := nil;

  FSomenteLeitura := False;
end;

procedure TFrFechamento.ePercentualDescontoChange(Sender: TObject);
begin
  inherited;
  if ePercentualOutrasDespesas.Focused and (FTotalProdutos > 0) then begin
    eValorOutrasDespesas.AsDouble := FTotalProdutos * ePercentualOutrasDespesas.AsDouble * 0.01;
    TotalizarValorASerPago;
  end
  else if ePercentualDesconto.Focused and (FTotalProdutos > 0) then begin
    eValorDesconto.AsDouble := _BibliotecaGenerica.ZeroSeNegativo((FTotalProdutos - FTotalPromocional - FTotalAtacado + eValorOutrasDespesas.AsDouble)* ePercentualDesconto.AsDouble * 0.01);
    TotalizarValorASerPago;
  end;
end;

procedure TFrFechamento.ePercentualOutrasDespesasChange(Sender: TObject);
begin
  inherited;
  if ePercentualDesconto.Focused and (FTotalProdutos > 0) then begin
    eValorDesconto.AsDouble := _BibliotecaGenerica.ZeroSeNegativo((FTotalProdutos - FTotalPromocional - FTotalAtacado + eValorOutrasDespesas.AsDouble)* ePercentualDesconto.AsDouble * 0.01);
    TotalizarValorASerPago;
  end
  else if ePercentualOutrasDespesas.Focused and (FTotalProdutos > 0) then begin
    eValorOutrasDespesas.AsDouble := FTotalProdutos * ePercentualOutrasDespesas.AsDouble * 0.01;
    eValorDesconto.AsDouble := _BibliotecaGenerica.ZeroSeNegativo((FTotalProdutos - FTotalPromocional - FTotalAtacado + eValorOutrasDespesas.AsDouble)* ePercentualDesconto.AsDouble * 0.01);
    TotalizarValorASerPago;
  end;
end;

procedure TFrFechamento.eValorCartaoCreditoExit(Sender: TObject);
begin
  inherited;
  if (eValorCartaoCredito.AsDouble = 0) and (FDadosCartoesCredito <> nil) then
    FDadosCartoesCredito := nil;
end;

procedure TFrFechamento.eValorCartaoCreditoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) and (FDadosCartoesCredito = nil) and (not (FTelaChamada = ttOrcamentosVendas)) and (sbBuscarDadosCartoesCredito.Visible) then
    sbBuscarDadosCartoesCreditoClick(nil);

  ProximoCampo(Sender, Key, Shift);
end;

procedure TFrFechamento.eValorCartaoDebitoExit(Sender: TObject);
begin
  inherited;
  if (eValorCartaoDebito.AsDouble = 0) and (FDadosCartoesDebito <> nil) then
    FDadosCartoesDebito := nil;
end;

procedure TFrFechamento.eValorCartaoDebitoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) and (FDadosCartoesDebito = nil) and (not (FTelaChamada = ttOrcamentosVendas)) and (sbBuscarDadosCartoesDebito.Visible) then
    sbBuscarDadosCartoesDebitoClick(nil);

  ProximoCampo(Sender, Key, Shift);
end;

procedure TFrFechamento.eValorChequeExit(Sender: TObject);
begin
  inherited;
  if (eValorCheque.AsCurr = 0) and (FDadosCheques <> nil) then
    FDadosCheques := nil;
end;

procedure TFrFechamento.eValorChequeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) and (FDadosCheques = nil) and (not (FTelaChamada = ttOrcamentosVendas)) then
    sbBuscarDadosChequesClick(nil);

  ProximoCampo(Sender, Key, Shift);
end;

procedure TFrFechamento.eValorCobrancaExit(Sender: TObject);
begin
  inherited;
  if (eValorCobranca.AsCurr = 0) and (FDadosCobranca <> nil) then
    FDadosCobranca := nil;
end;

procedure TFrFechamento.eValorCobrancaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) and (FDadosCobranca = nil) and (not (FTelaChamada = ttOrcamentosVendas)) then
    sbBuscarDadosCobrancaClick(nil);

  ProximoCampo(Sender, Key, Shift);
end;

procedure TFrFechamento.eValorDescontoChange(Sender: TObject);
begin
  inherited;
  if (eValorDesconto.Focused or (Sender = nil)) and(FTotalProdutos > 0) then begin
    ePercentualDesconto.AsDouble := Sessao.getCalculosSistema.CalcOrcamento.getPercDesconto(FTotalProdutos - FTotalPromocional - FTotalAtacado, eValorOutrasDespesas.AsDouble, eValorDesconto.AsDouble);

    if eValorDesconto.Focused then
      TotalizarValorASerPago;
  end;
end;

procedure TFrFechamento.eValorDinheiroChange(Sender: TObject);
begin
  inherited;
  TotalizarValorASerPago;
end;

procedure TFrFechamento.eValorDinheiroDblClick(Sender: TObject);
begin
  inherited;
  if TEditLuka(Sender).AsCurr > 0 then
    TEditLuka(Sender).AsCurr := 0
  else
    TEditLuka(Sender).AsCurr := Abs(eValorTroco.AsCurr);
end;

procedure TFrFechamento.eValorOutrasDespesasChange(Sender: TObject);
begin
  inherited;
  if (eValorOutrasDespesas.Focused or (Sender = nil)) and (FTotalProdutos > 0) then begin
    ePercentualOutrasDespesas.AsDouble := Sessao.getCalculosSistema.CalcOrcamento.getPercOutrasDespesas(FTotalProdutos, eValorOutrasDespesas.AsDouble);
    eValorDescontoChange(nil);

    if eValorOutrasDespesas.Focused then
      TotalizarValorASerPago;
  end;
end;

procedure TFrFechamento.eValorPixExit(Sender: TObject);
begin
  inherited;
  if (eValorPix.AsCurr = 0) and (FDadosPix <> nil) then
    FDadosCobranca := nil;
end;

procedure TFrFechamento.eValorPixKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) and (FDadosPix = nil) and (not (FTelaChamada = ttOrcamentosVendas)) then
    sbBuscarDadosCobrancaPixClick(nil);

  ProximoCampo(Sender, Key, Shift);
end;

procedure TFrFechamento.eValorTotalASerPagoChange(Sender: TObject);
var
  vTotalFormasPagamento: Currency;
  vJuros: Double;
  vEditJuros: TComponent;
begin
  inherited;

  if eValorTotalASerPago.Focused then begin
    _Biblioteca.LimparCampos([eValorDesconto, eValorOutrasDespesas, ePercentualDesconto, ePercentualOutrasDespesas]);

    vJuros := 0;
    vEditJuros := FindComponent('eValorJuros');
    if vEditJuros <> nil then
      vJuros := TEditLuka(vEditJuros).AsCurr;

    if eValorTotalASerPago.AsCurr > FTotalProdutos + vJuros + eValorFrete.AsDouble then begin
      eValorOutrasDespesas.AsDouble := eValorTotalASerPago.AsDouble - (FTotalProdutos + vJuros + eValorFrete.AsDouble);
      eValorOutrasDespesasChange(nil);
    end
    else begin
      eValorDesconto.AsDouble := (FTotalProdutos + vJuros + eValorFrete.AsDouble) - eValorTotalASerPago.AsDouble;
      eValorDescontoChange(nil);
    end;

    vTotalFormasPagamento := eValorDinheiro.AsCurr + eValorCheque.AsCurr + eValorCartaoDebito.AsCurr + eValorCartaoCredito.AsCurr + eValorCobranca.AsCurr + eValorFinanceira.AsCurr + eValorAcumulativo.AsCurr + eValorCredito.AsCurr + eValorPix.AsCurr;
    eValorTroco.AsCurr    := vTotalFormasPagamento - eValorTotalASerPago.AsCurr;
  end;

  if Assigned(FNotificarValorAlterado) then
    FNotificarValorAlterado(eValorOutrasDespesas.AsCurr, eValorDesconto.AsCurr, eValorTotalASerPago.AsCurr);
end;

function TFrFechamento.getCartoes: TArray<RecTitulosFinanceiros>;
begin
  Result := nil;
  if eValorCartaoDebito.AsCurr + eValorCartaoCredito.AsCurr > 0 then
    Result := FDadosCartoesDebito + FDadosCartoesCredito;
end;

function TFrFechamento.getCreditoMaiorTotalPagarETemFormasPagamento: Boolean;
begin
  Result := False;
  if eValorCredito.AsCurr = 0 then
    Exit;

  Result :=
    (eValorCredito.AsCurr >= eValorTotalASerPago.AsCurr) and
    (eValorDinheiro.AsCurr + eValorPix.AsCurr + eValorCheque.AsCurr + eValorCartaoDebito.AsCurr + eValorCartaoCredito.AsCurr + eValorCobranca.AsCurr > 0);
end;

function TFrFechamento.getFormasPagamentoMaiorTotalPagarEUtilizandoCredito: Boolean;
begin
  Result := False;
  if eValorCredito.AsCurr = 0 then
    Exit;

  Result := (eValorDinheiro.AsCurr + eValorPix.AsCurr + eValorCheque.AsCurr + eValorCartaoDebito.AsCurr + eValorCartaoCredito.AsCurr + eValorCobranca.AsCurr) >= eValorTotalASerPago.AsCurr;
end;

function TFrFechamento.TemValoresComprometemCreditoCliente: Boolean;
begin
  Result := (eValorCheque.AsCurr + eValorCobranca.AsCurr + eValorAcumulativo.AsCurr) > 0;
end;

function TFrFechamento.getTotalFrete: Double;
begin
  Result := eValorFrete.AsDouble;
end;

function TFrFechamento.getValoresComprometemCreditoCliente: Currency;
begin
  Result := eValorCheque.AsCurr + eValorCobranca.AsCurr + eValorAcumulativo.AsCurr;
end;

procedure TFrFechamento.LimparDadosTiposCobranca(pLimparCondicaoPagto: Boolean);
begin
  if pLimparCondicaoPagto then
    setCondicaoPagamento(0, '', 0);

  _Biblioteca.LimparCampos([
    eValorDinheiro,
    eValorPix,
    eValorCheque,
    eValorCartaoDebito,
    eValorCartaoCredito,
    eValorCobranca,
    eValorFinanceira,
    eValorAcumulativo,
    eValorCredito]
  );

  FDadosCheques       := nil;
  FDadosCartoesDebito := nil;
  FDadosCobranca      := nil;
end;

procedure TFrFechamento.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;

  _Biblioteca.Habilitar([
    eValorOutrasDespesas,
    ePercentualOutrasDespesas,
    eValorDesconto,
    ePercentualDesconto,
    eValorTotalASerPago,
    eValorDinheiro,
    eValorCheque,
    eValorCartaoDebito,
    eValorCartaoCredito,
    eValorCobranca,
    eValorFinanceira,
    eValorAcumulativo,
    eValorCredito,
    eValorTroco],
    pEditando,
    pLimpar
  );

  eValorPix.Enabled := eValorDinheiro.Enabled and (Sessao.getParametrosEmpresa.HabilitarRecebimentoPix = 'S');

  if pLimpar then begin
    FDadosCheques        := nil;
    FDadosCartoesDebito  := nil;
    FDadosCartoesCredito := nil;
    FDadosCobranca       := nil;
    FCreditos            := nil;
  end;

  _Biblioteca.Habilitar([sbBuscarDadosCartoesDebito, sbBuscarDadosCartoesCredito, sbBuscarDadosCheques, sbBuscarDadosCobranca, sbBuscarDadosFinanceira], False);
  _Biblioteca.SomenteLeitura([eValorTotalASerPago, eValorTroco], True);
end;

procedure TFrFechamento.PermitirEditarDesconto(const pPermitir: Boolean);
begin
  _Biblioteca.SomenteLeitura([eValorDesconto, ePercentualDesconto], not pPermitir);
end;

procedure TFrFechamento.PermitirEditarTotalPagar(const pPermitir: Boolean);
begin
  _Biblioteca.SomenteLeitura([eValorTotalASerPago], not pPermitir);
end;

procedure TFrFechamento.PermitirEditarValoresFormasPagamento(const pPermitir: Boolean);
begin
  _Biblioteca.SomenteLeitura([
    eValorCheque,
    eValorCartaoDebito,
    eValorCartaoCredito,
    eValorCobranca,
    eValorFinanceira,
    eValorAcumulativo],
    not pPermitir
  );
end;

procedure TFrFechamento.sbBuscarCreditosClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar< RecCreditos >;
begin
  inherited;
  vRetTela := BuscarCreditosDisponiveis.Buscar(
    FCadastroId,
    nil,
    'R',
    FTelaChamada = ttAcumulado,
    FCreditosAcumuladosIds
  );

  FCreditos := vRetTela.Dados.Creditos;
  eValorCredito.AsDouble := vRetTela.Dados.ValorTotal;
end;

procedure TFrFechamento.sbBuscarDadosCartoesCreditoClick(Sender: TObject);
begin
  inherited;
  if eValorCartaoCredito.AsCurr > 0 then begin
    FDadosCartoesCredito :=
      BuscarDadosCartoes.BuscarCartoes(
        FCondicaoPagamentoId,
        FNomeCondicao,
        eValorCartaoCredito.AsDouble,
        FDadosCartoesCredito,
        FSomenteLeitura,
        0,
        'C'
      ).Dados;
  end;
end;

procedure TFrFechamento.sbBuscarDadosCartoesDebitoClick(Sender: TObject);
begin
  inherited;
  if eValorCartaoDebito.AsCurr > 0 then
    FDadosCartoesDebito := BuscarDadosCartoes.BuscarCartoes(FCondicaoPagamentoId, FNomeCondicao, eValorCartaoDebito.AsDouble, FDadosCartoesDebito, FSomenteLeitura, 0, 'D').Dados;
end;

procedure TFrFechamento.sbBuscarDadosChequesClick(Sender: TObject);
begin
  inherited;
  if eValorCheque.AsCurr > 0 then
    FDadosCheques := BuscarDadosCheques.BuscarCheques(FCondicaoPagamentoId, FNomeCondicao, eValorCheque.AsDouble, FDadosCheques, FSomenteLeitura).Dados;
end;

procedure TFrFechamento.sbBuscarDadosCobrancaClick(Sender: TObject);
begin
  inherited;
  if eValorCobranca.AsCurr > 0 then begin
    FDadosCobranca :=
      BuscarDadosCobrancas.BuscarCobrancas(
        FCondicaoPagamentoId,
        FNomeCondicao,
        'COB',
        eValorCobranca.AsDouble,
        FDadosCobranca,
        FSomenteLeitura,
        FPrazoMedio
      ).Dados;
  end;
end;

procedure TFrFechamento.sbBuscarDadosCobrancaPixClick(Sender: TObject);
begin
  inherited;
  if not sbBuscarDadosCobrancaPix.Enabled then
    Exit;

  if eValorPix.AsCurr > 0 then
    FDadosPix := BuscarDadosDinheiro.BuscarDadosPix(eValorPix.AsCurr, FDadosPix).Dados;

//  checarDefinidos(Sender);
end;

procedure TFrFechamento.setCadastroId(pCadastroId: Integer);
begin
  if (pCadastroId = 0) or (pCadastroId <> FCadastroId) then begin
    eValorCredito.AsCurr := 0;
    FCreditos := nil;
  end;

  FCadastroId := pCadastroId;
end;

procedure TFrFechamento.setCondicaoPagamento(
  const pCondicaoId: Integer;
  const pNomeCondicao: string;
  const pPrazoMedio: Double
);
var
  vFormasLiberadas: TArray<string>;
begin
  FCondicaoPagamentoId := pCondicaoId;
  FNomeCondicao        := pNomeCondicao;
  FPrazoMedio          := pPrazoMedio;

  if pCondicaoId > 0 then
    vFormasLiberadas := _TiposCobrancaCondicaoPagamento.BuscarFormasPagamentoLiberadas(Sessao.getConexaoBanco, FCondicaoPagamentoId);

  _Biblioteca.Habilitar([
    eValorDinheiro,
    sbBuscarCreditos,
    eValorCheque,
    eValorCartaoDebito,
    eValorCartaoCredito,
    eValorCobranca,
    eValorFinanceira,
    eValorAcumulativo,
    eValorPix],
    False
  );

  eValorPix.Enabled := eValorDinheiro.Enabled and (Sessao.getParametrosEmpresa.HabilitarRecebimentoPix = 'S');

  if FTelaChamada <> ttAcumulado then
    eValorCredito.Clear;

  if _Biblioteca.Em('ACU', vFormasLiberadas) then begin
    eValorAcumulativo.Enabled := True;
    _Biblioteca.Habilitar([
      eValorDinheiro,
      sbBuscarDadosCartoesDebito,
      sbBuscarDadosCartoesCredito,
      eValorCartaoDebito,
      eValorCartaoCredito,
      eValorPix],
      true
    );
  end
  else begin
    _Biblioteca.Habilitar([
      eValorDinheiro,
      sbBuscarCreditos],
      True
    );

    eValorPix.Enabled := eValorDinheiro.Enabled and (Sessao.getParametrosEmpresa.HabilitarRecebimentoPix = 'S');

    // Est� aqui para n�o ser limpo no met�do habilitar acima. *** FECHAMENTO DE ACUMULADOS ***
    eValorCredito.Enabled               := True;

    eValorCheque.Enabled                := _Biblioteca.Em('CHQ', vFormasLiberadas);
    sbBuscarDadosCheques.Enabled        := _Biblioteca.Em('CHQ', vFormasLiberadas);

    eValorCartaoDebito.Enabled          := _Biblioteca.Em('CRD', vFormasLiberadas);
    eValorCartaoCredito.Enabled         := _Biblioteca.Em('CRC', vFormasLiberadas);
    sbBuscarDadosCartoesDebito.Enabled  := _Biblioteca.Em('CRD', vFormasLiberadas);
    sbBuscarDadosCartoesCredito.Enabled := _Biblioteca.Em('CRC', vFormasLiberadas);

    eValorCobranca.Enabled              := _Biblioteca.Em('COB', vFormasLiberadas);
    sbBuscarDadosCobranca.Enabled       := _Biblioteca.Em('COB', vFormasLiberadas);

    eValorFinanceira.Enabled            := _Biblioteca.Em('FIN', vFormasLiberadas);
    sbBuscarDadosFinanceira.Enabled     := _Biblioteca.Em('FIN', vFormasLiberadas);
  end;
end;

procedure TFrFechamento.setPedidosAcumuladosIds(pValor: TArray<Integer>);
var
  i: Integer;
  vCreditos: TArray<RecCreditosAdiantamentosAcumulados>;
begin
  vCreditos := _ContasPagar.BuscarCreditosAdiantamentosAcumulados(Sessao.getConexaoBanco, CadastroId);
  if vCreditos = nil then
    Exit;

  SetLength(FCreditosAcumuladosIds, Length(vCreditos));
  for i := Low(vCreditos) to High(vCreditos) do begin
    FCreditosAcumuladosIds[i] := vCreditos[i].PagarId;
    eValorCredito.AsCurr := eValorCredito.AsCurr + vCreditos[i].Valor;
  end;

  FCreditos := FCreditosAcumuladosIds;
end;

procedure TFrFechamento.setRecebimentoNaEntrega;
var
  vCondicaoId: Integer;
  vNomeCondicao: string;
  vPrazoMedio: Double;
begin
  vCondicaoId   := FCondicaoPagamentoId;
  vNomeCondicao := FNomeCondicao;
  vPrazoMedio   := FPrazoMedio;

  LimparDadosTiposCobranca(True);

  setCondicaoPagamento(vCondicaoId, vNomeCondicao, vPrazoMedio);
  _Biblioteca.Habilitar([eValorCobranca], False);
end;

procedure TFrFechamento.setTelaChamada(const pValor: TTelaChamada);
begin
  FTelaChamada := pValor;

  if FTelaChamada = ttOrcamentosVendas then
    _Biblioteca.Visibilidade([sbBuscarDadosCartoesDebito, sbBuscarDadosCartoesCredito], False)
  else if FTelaChamada = ttRecebimento then
    eValorCredito.ReadOnly := True;
end;

procedure TFrFechamento.setTotalFrete(const pValor: Double);
begin
  eValorFrete.AsDouble := pValor;
  TotalizarValorASerPago;
end;

procedure TFrFechamento.setTotalProdutos(const pValor: Double);
begin
  FTotalProdutos := pValor;
  TotalizarValorASerPago;
end;

procedure TFrFechamento.setTotalPromocional(const pValor: Double);
begin
  FTotalPromocional := pValor;
end;

procedure TFrFechamento.setTotalAtacado(const pValor: Double);
begin
  FTotalAtacado := pValor;
end;

procedure TFrFechamento.setUtilizaTef(pUtilizarTEF: Boolean);
begin
  _Biblioteca.Visibilidade([sbBuscarDadosCartoesDebito, sbBuscarDadosCartoesCredito], not pUtilizarTEF);
end;

procedure TFrFechamento.SomenteLeitura(pValue: Boolean);
begin
  inherited;
  _Biblioteca.SomenteLeitura([
    eValorOutrasDespesas,
    eValorDesconto,
    eValorDinheiro,
    eValorCheque,
    eValorCartaoDebito,
    eValorCartaoCredito,
    eValorCobranca,
    eValorFinanceira,
    eValorAcumulativo],
    pValue
  );

  eValorPix.ReadOnly := eValorDinheiro.ReadOnly;

  FSomenteLeitura := pValue;
end;

function TFrFechamento.TemBoletoBancario: Boolean;
var
  i: Integer;
  vCobrancasIds: TArray<Integer>;
begin
  Result := False;

  if FDadosCobranca = nil then
    Exit;

  vCobrancasIds := nil;
  for i := Low(FDadosCobranca) to High(FDadosCobranca) do
    AddNoVetorSemRepetir(vCobrancasIds, FDadosCobranca[i].CobrancaId);

  Result := _TiposCobranca.TemTipoCobrancaBoletoBancario(Sessao.getConexaoBanco, vCobrancasIds);
end;

function TFrFechamento.TemDuplicataMercantil: Boolean;
var
  i: Integer;
  vCobrancasIds: TArray<Integer>;
begin
  Result := False;

  if FDadosCobranca = nil then
    Exit;

  vCobrancasIds := nil;
  for i := Low(FDadosCobranca) to High(FDadosCobranca) do
    AddNoVetorSemRepetir(vCobrancasIds, FDadosCobranca[i].CobrancaId);

  Result := _TiposCobranca.TemTipoDuplicataMercantil(Sessao.getConexaoBanco, vCobrancasIds);
end;

procedure TFrFechamento.TotalizarValorASerPago;
var
  vTotalFormasPagamento: Currency;
begin
  eValorTotalASerPago.AsCurr := FTotalProdutos + eValorOutrasDespesas.AsCurr - eValorDesconto.AsCurr + eValorFrete.AsDouble;
  vTotalFormasPagamento      := eValorDinheiro.AsCurr + eValorCheque.AsCurr + eValorCartaoDebito.AsCurr + eValorCartaoCredito.AsCurr + eValorCobranca.AsCurr + eValorFinanceira.AsCurr + eValorAcumulativo.AsCurr + eValorCredito.AsCurr + eValorPix.AsCurr;
  eValorTroco.AsCurr         := vTotalFormasPagamento - eValorTotalASerPago.AsCurr;

  inherited;
end;

procedure TFrFechamento.verificarCartoes;
var
  i: Integer;
  vValorCartao: Currency;
begin
  if (eValorCartaoDebito.AsCurr > 0) and (Sessao.getParametrosEstacao.UtilizarTef <> 'S') then begin
    if CartoesDebito = nil then begin
      _Biblioteca.Exclamar('Os cart�es de d�bito n�o foram definidos corretamente!');
      _Biblioteca.SetarFoco(eValorCartaoDebito);
      Abort;
    end;

    vValorCartao := 0;
    for i := Low(FDadosCartoesDebito) to High(FDadosCartoesDebito) do
      vValorCartao := vValorCartao + FDadosCartoesDebito[i].Valor;

    if vValorCartao <> eValorCartaoDebito.AsCurr then begin
      _Biblioteca.Exclamar('Existe diferen�a nos valores dos cart�es de d�bito!');
      _Biblioteca.SetarFoco(eValorCartaoDebito);
      Abort;
    end;
  end;

  if (eValorCartaoCredito.AsCurr > 0) and (Sessao.getParametrosEstacao.UtilizarTef <> 'S') then begin
    if CartoesCredito = nil then begin
      _Biblioteca.Exclamar('Os cart�es de cr�dito n�o foram definidos corretamente!');
      _Biblioteca.SetarFoco(eValorCartaoCredito);
      Abort;
    end;

    vValorCartao := 0;
    for i := Low(FDadosCartoesCredito) to High(FDadosCartoesCredito) do
      vValorCartao := vValorCartao + FDadosCartoesCredito[i].Valor;

    if vValorCartao <> eValorCartaoCredito.AsCurr then begin
      _Biblioteca.Exclamar('Existe diferen�a nos valores dos cart�es de cr�dito!');
      _Biblioteca.SetarFoco(eValorCartaoCredito);
      Abort;
    end;
  end;

end;

end.
