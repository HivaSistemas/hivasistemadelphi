unit RelacaoContasReceberBaixas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _Biblioteca, _Sessao,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, FrameDataInicialFinal, System.StrUtils, System.Math,
  FrameTiposCobranca, FrameEmpresas, _FrameHerancaPrincipal, _ContasReceber,
  _FrameHenrancaPesquisas, FrameClientes, Vcl.Grids, GridLuka, _RelacaoContasReceberBaixas,
  _RecordsRelatorios, Vcl.StdCtrls, Informacoes.ContasReceberBaixa, Informacoes.TituloReceber,
  _RecordsFinanceiros, Vcl.Menus, _RecordsEspeciais, _ContasReceberBaixas, Impressao.ComprovantePagamentoTituloReceberGrafico,
  FrameNumeros, FrameFuncionarios, CheckBoxLuka, Data.DB, Datasnap.DBClient,
  frxClass, frxDBSet, OraCall, DBAccess, Ora, MemDS;

type
  TFormRelacaoContasReceberBaixas = class(TFormHerancaRelatoriosPageControl)
    FrClientes: TFrClientes;
    FrEmpresas: TFrEmpresas;
    FrDataBaixa: TFrDataInicialFinal;
    FrDataPagamento: TFrDataInicialFinal;
    sgBaixas: TGridLuka;
    pmBaixas: TPopupMenu;
    FrBaixas: TFrNumeros;
    FrTiposCobranca: TFrTiposCobranca;
    FrCodigoTitulo: TFrNumeros;
    FrCodigoPedido: TFrNumeros;
    FrCodigoAcumulado: TFrNumeros;
    FrFuncionarios: TFrFuncionarios;
    miCancelarBaixaSelecionada: TSpeedButton;
    miImprimirComprovantePagamento: TSpeedButton;
    SpeedButton1: TSpeedButton;
    frxReport: TfrxReport;
    frxBaixas: TfrxDBDataset;
    qBaixas: TOraQuery;
    dsBaixas: TDataSource;
    dstReceber: TfrxDBDataset;
    qReceber: TOraQuery;
    qBaixasBAIXA_ID: TFloatField;
    qBaixasEMPRESA_ID: TIntegerField;
    qBaixasVALOR_TITULOS: TFloatField;
    qBaixasVALOR_LIQUIDO: TFloatField;
    qBaixasVALOR_JUROS: TFloatField;
    qBaixasVALOR_DESCONTO: TFloatField;
    qBaixasVALOR_DINHEIRO: TFloatField;
    qBaixasVALOR_CHEQUE: TFloatField;
    qBaixasVALOR_CARTAO_DEBITO: TFloatField;
    qBaixasVALOR_COBRANCA: TFloatField;
    qBaixasVALOR_CREDITO: TFloatField;
    qBaixasVALOR_TROCO: TFloatField;
    qBaixasDATA_HORA_BAIXA: TDateTimeField;
    qBaixasOBSERVACOES: TStringField;
    qBaixasCADASTRO_ID: TFloatField;
    qBaixasVALOR_CARTAO_CREDITO: TFloatField;
    qBaixasVALOR_MULTA: TFloatField;
    qBaixasVALOR_PIX: TFloatField;
    frxReport1: TfrxReport;
    qBaixasCLIENTE: TStringField;
    qBaixasCPF_CNPJ: TStringField;
    qBaixasVALOR_ADIANTADO: TFloatField;
    qBaixasVALOR_RETENCAO: TFloatField;
    qReceberRECEBER_ID: TFloatField;
    qReceberEMPRESA_ID: TIntegerField;
    qReceberORIGEM: TStringField;
    qReceberORIGEM_ID: TFloatField;
    qReceberDOCUMENTO: TStringField;
    qReceberDATA_CADASTRO: TDateTimeField;
    qReceberDATA_EMISSAO: TDateTimeField;
    qReceberDATA_VENCIMENTO: TDateTimeField;
    qReceberVALOR_DOCUMENTO: TFloatField;
    qReceberPARCELA: TIntegerField;
    qReceberNUMERO_PARCELAS: TIntegerField;
    qReceberOBSERVACOES: TStringField;
    qReceberCLIENTE: TStringField;
    qReceberNOME_FANTASIA: TStringField;
    qReceberVALOR_RETENCAO: TFloatField;
    qReceberBAIXA_ID: TFloatField;
    qReceberCPF_CNPJ: TStringField;
    qReceberVALOR_ADIANTADO: TFloatField;
    qReceberVALOR_DESCONTO: TFloatField;
    qReceberVALOR_JUROS: TFloatField;
    qReceberVALOR_MULTA: TFloatField;
    qReceberVALOR_LIQUIDO: TFloatField;
    qBaixasTIPO_BAIXA: TStringField;
    qCreditoGerado: TOraQuery;
    qCreditoGeradoVALOR_DOCUMENTO: TFloatField;
    qCreditoGeradoPAGAR_ID: TFloatField;
    procedure sgBaixasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure sgBaixasDblClick(Sender: TObject);
    procedure miCancelarBaixaSelecionadaClick(Sender: TObject);
    procedure miImprimirComprovantePagamentoClick(Sender: TObject);
    procedure sbImprimirClick(Sender: TObject);
    procedure sbImprimirCompPagClick(Sender: TObject);
  private
    FBaixas: TArray<RecContasReceberBaixas>;
    vComandoBaixasPadrao: string;
    vComandoReceberPadrao: string;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

uses _ContasReceberBaixasItens;

const
  coBaixa         = 0;
  coTipoAnalitico = 1;
  coDataPagamento = 2;
  coDataBaixa     = 3;
  coCliente       = 4;
  coDinheiro      = 5;
  coCheque        = 6;
  coCartao        = 7;
  coCobranca      = 8;
  coCredito       = 9;
  coPix           = 10;
  coEmpresa       = 11;
  coUsuarioBaixa  = 12;



procedure TFormRelacaoContasReceberBaixas.Carregar(Sender: TObject);
var
  i, j: Integer;
  vComando: string;
  vItensBaixa: TArray<RecContaReceberBaixaItem>;
  clienteId: Integer;
  clienteNome: string;
  clienteIdAnterior: Integer;
  multiplosClientes: boolean;
begin
  inherited;
  vComando := '';
  sgBaixas.ClearGrid;

  if not FrBaixas.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrBaixas.TrazerFiltros('CRB.BAIXA_ID'))
  else begin
    if not FrClientes.EstaVazio then begin
      WhereOuAnd(
        vComando,
        'CRB.BAIXA_ID in( ' +
        '  select CRI.BAIXA_ID ' +
        '  from CONTAS_RECEBER_BAIXAS_ITENS CRI ' +
        '  inner join CONTAS_RECEBER COR ' +
        '  on CRI.RECEBER_ID = COR.RECEBER_ID ' +
        '  where ' + FrClientes.getSqlFiltros('COR.CADASTRO_ID') +
        ')'
      );
    end;

    if not FrEmpresas.EstaVazio then
      WhereOuAnd(vComando, FrEmpresas.getSqlFiltros('CRB.EMPRESA_ID'));

    if not FrTiposCobranca.EstaVazio then begin
      WhereOuAnd(
        vComando,
        'CRB.BAIXA_ID in( ' +
        '  select CRI.BAIXA_ID ' +
        '  from CONTAS_RECEBER_BAIXAS_ITENS CRI ' +
        '  inner join CONTAS_RECEBER COR ' +
        '  on CRI.RECEBER_ID = COR.RECEBER_ID ' +
        '  where ' + FrTiposCobranca.getSqlFiltros('COR.COBRANCA_ID') +
        ')'
      );
    end;

    if FrDataBaixa.DatasValidas then
      WhereOuAnd(vComando, FrDataBaixa.getSqlFiltros('CRB.DATA_HORA_BAIXA'));

    if FrDataPagamento.DatasValidas then
      WhereOuAnd(vComando, FrDataPagamento.getSqlFiltros('CRB.DATA_PAGAMENTO'));

    if not FrCodigoTitulo.EstaVazio then begin
      WhereOuAnd(
        vComando,
        'CRB.BAIXA_ID in( ' +
        '  select CRI.BAIXA_ID ' +
        '  from CONTAS_RECEBER_BAIXAS_ITENS CRI ' +
        '  inner join CONTAS_RECEBER COR ' +
        '  on CRI.RECEBER_ID = COR.RECEBER_ID ' +
        '  where ' + FrCodigoTitulo.TrazerFiltros('COR.RECEBER_ID') +
        ')'
      );
    end;

    if not FrCodigoPedido.EstaVazio then begin
      WhereOuAnd(
        vComando,
        'CRB.BAIXA_ID in( ' +
        '  select CRI.BAIXA_ID ' +
        '  from CONTAS_RECEBER_BAIXAS_ITENS CRI ' +
        '  inner join CONTAS_RECEBER COR ' +
        '  on CRI.RECEBER_ID = COR.RECEBER_ID ' +
        '  where ' + FrCodigoPedido.TrazerFiltros('COR.ORCAMENTO_ID') +
        ')'
      );
    end;

    if not FrCodigoAcumulado.EstaVazio then begin
      WhereOuAnd(
        vComando,
        'CRB.BAIXA_ID in( ' +
        '  select CRI.BAIXA_ID ' +
        '  from CONTAS_RECEBER_BAIXAS_ITENS CRI ' +
        '  inner join CONTAS_RECEBER COR ' +
        '  on CRI.RECEBER_ID = COR.RECEBER_ID ' +
        '  where ' + FrCodigoAcumulado.TrazerFiltros('COR.ACUMULADO_ID') +
        ')'
      );
    end;

    if not FrFuncionarios.EstaVazio then
      WhereOuAnd(vComando, FrFuncionarios.getSqlFiltros('CRB.USUARIO_BAIXA_ID'));
  end;

  vComando := vComando + ' order by CRB.BAIXA_ID desc';

  FBaixas := _RelacaoContasReceberBaixas.BuscarContasReceberBaixas(Sessao.getConexaoBanco, vComando);
  if FBaixas = nil then begin
    NenhumRegistro;
    Exit;
  end;

  for i := Low(FBaixas) to High(FBaixas) do begin
    sgBaixas.Cells[coBaixa, i + 1]         := NFormat(FBaixas[i].baixa_id);
    sgBaixas.Cells[coTipoAnalitico, i + 1] := FBaixas[i].TipoAnalitico;
    sgBaixas.Cells[coDataPagamento, i + 1] := DFormat(FBaixas[i].data_pagamento);
    sgBaixas.Cells[coDataBaixa, i + 1]     := DFormat(FBaixas[i].data_baixa);
    sgBaixas.Cells[coDinheiro, i + 1]      := NFormatN(FBaixas[i].valor_dinheiro);
    sgBaixas.Cells[coCheque, i + 1]        := NFormatN(FBaixas[i].valor_cheque);
    sgBaixas.Cells[coCartao, i + 1]        := NFormatN(FBaixas[i].valor_cartao);
    sgBaixas.Cells[coCobranca, i + 1]      := NFormatN(FBaixas[i].valor_cobranca);
    sgBaixas.Cells[coCredito, i + 1]       := NFormatN(FBaixas[i].valor_credito);
    sgBaixas.Cells[coEmpresa, i + 1]       := NFormat(FBaixas[i].empresa_id) + ' - ' + FBaixas[i].nome_empresa;
    sgBaixas.Cells[coUsuarioBaixa, i + 1]  := NFormat(FBaixas[i].usuario_baixa_id) + ' - ' + FBaixas[i].nome_usuario_baixa;
    sgBaixas.Cells[coPix, i + 1]           := NFormatN(FBaixas[i].valor_pix);

    vItensBaixa := _ContasReceberBaixasItens.BuscarContaReceberBaixaItens(Sessao.getConexaoBanco, 0, [FBaixas[i].baixa_id]);
    if vItensBaixa <> nil then begin

      clienteIdAnterior := 0;              
      multiplosClientes := false;
      for j := Low(vItensBaixa) to High(vItensBaixa) do begin
        if (clienteIdAnterior <> 0) and (clienteIdAnterior <> vItensBaixa[j].CadastroId) then begin
          multiplosClientes := True;
          clienteNome := 'BAIXA AGRUPADA';
          Break;
        end;
          
        clienteIdAnterior := vItensBaixa[j].CadastroId;
        clienteId := vItensBaixa[j].CadastroId;
        clienteNome := vItensBaixa[j].nome_cliente;
      end;

      if multiplosClientes then
        sgBaixas.Cells[coCliente, i + 1]  := 'BAIXA AGRUPADA'
      else
        sgBaixas.Cells[coCliente, i + 1]  := NFormat(clienteId) + ' - ' + clienteNome;
      
    end;
  end;
  sgBaixas.SetLinhasGridPorTamanhoVetor( Length(FBaixas) );
  SetarFoco(sgBaixas);
end;

procedure TFormRelacaoContasReceberBaixas.FormCreate(Sender: TObject);
begin
  inherited;
  pcDados.ActivePageIndex := 0;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId, False);

  qBaixas.Session := Sessao.getConexaoBanco;
  qReceber.Session := Sessao.getConexaoBanco;
  qCreditoGerado.Session := Sessao.getConexaoBanco;
  vComandoBaixasPadrao := qBaixas.SQL.Text;
  vComandoReceberPadrao := qReceber.SQL.Text;
end;

procedure TFormRelacaoContasReceberBaixas.Imprimir(Sender: TObject);
begin
  inherited;

end;

procedure TFormRelacaoContasReceberBaixas.miCancelarBaixaSelecionadaClick(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if not Perguntar('Deseja realmente cancelar esta baixa?') then
    Exit;

  vRetBanco := _ContasReceberBaixas.CancelarBaixa(Sessao.getConexaoBanco, SFormatInt(sgBaixas.Cells[coBaixa, sgBaixas.Row]), False);
  if vRetBanco.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  _Biblioteca.Informar('Baixa cancelada com sucesso.');
  Carregar(Sender);
end;

procedure TFormRelacaoContasReceberBaixas.miImprimirComprovantePagamentoClick(Sender: TObject);
var
  baixaId: Integer;
begin
  inherited;
  baixaId := SFormatInt(sgBaixas.Cells[coBaixa, sgBaixas.Row]);
  if baixaId = 0 then
    Exit;

  qBaixas.SQL.Text := vComandoBaixasPadrao;
  qBaixas.SQL.Add(' ' +  ' AND BAI.BAIXA_ID = ' + IntToStr(baixaId));
  qBaixas.Open;

  qReceber.SQL.Text := vComandoReceberPadrao;
  qReceber.Close;
  qReceber.Open;

  qCreditoGerado.Close;
  qCreditoGerado.ParamByName('BAIXA_RECEBER_ORIGEM_ID').AsInteger := baixaId;
  qCreditoGerado.Open;

  TfrxMemoView(frxReport1.FindComponent('mmDinheiro')).Text      := 'R$ ' + NFormat(qBaixasVALOR_DINHEIRO.AsFloat);
  TfrxMemoView(frxReport1.FindComponent('mmCartaoCredito')).Text := 'R$ ' + NFormat(qBaixasVALOR_CARTAO_CREDITO.AsFloat);
  TfrxMemoView(frxReport1.FindComponent('mmCartaoDebito')).Text  := 'R$ ' + NFormat(qBaixasVALOR_CARTAO_DEBITO.AsFloat);
  TfrxMemoView(frxReport1.FindComponent('mmCheque')).Text        := 'R$ ' + NFormat(qBaixasVALOR_CHEQUE.AsFloat);
  TfrxMemoView(frxReport1.FindComponent('mmCobranca')).Text      := 'R$ ' + NFormat(qBaixasVALOR_COBRANCA.AsFloat);
  TfrxMemoView(frxReport1.FindComponent('mmCredito')).Text       := 'R$ ' + NFormat(qBaixasVALOR_CREDITO.AsFloat);
  TfrxMemoView(frxReport1.FindComponent('mmPix')).Text           := 'R$ ' + NFormat(qBaixasVALOR_PIX.AsFloat);

  if not qCreditoGerado.IsEmpty then begin
    TfrxMemoView(frxReport1.FindComponent('mmLabelCodigoNovoCredito')).Visible := True;
    TfrxMemoView(frxReport1.FindComponent('mmLabelCodigoNovoCredito')).Text :=
      'C�d. novo cr�dito (' + NFormat(qCreditoGeradoPAGAR_ID.AsInteger) + '): R$ ' + NFormat(qCreditoGeradoVALOR_DOCUMENTO.AsFloat);
  end
  else begin
    TfrxMemoView(frxReport1.FindComponent('mmLabelCodigoNovoCredito')).Visible := False;
  end;

  frxReport1.ShowReport;
  //Impressao.ComprovantePagamentoTituloReceberGrafico.Imprimir( SFormatInt(sgBaixas.Cells[coBaixa, sgBaixas.Row]) );
end;

procedure TFormRelacaoContasReceberBaixas.sbImprimirClick(Sender: TObject);
begin
   GridToExcel(sgBaixas);
end;

procedure TFormRelacaoContasReceberBaixas.sbImprimirCompPagClick(
  Sender: TObject);
var
  vCnt: Integer;
  vReceberComandoFinal: string;
  baixaId: Integer;
  i: Integer;
begin
  inherited;
  baixaId := SFormatInt(sgBaixas.Cells[coBaixa, sgBaixas.Row]);
  if baixaId = 0 then
    Exit;

  qBaixas.SQL.Text := vComandoBaixasPadrao;
  qBaixas.SQL.Add(' ' +  ' AND BAIXA_ID = ' + IntToStr(baixaId));
  qBaixas.Open;

  qReceber.SQL.Text := vComandoReceberPadrao;
  qReceber.Close;
  qReceber.Open;

  TfrxMemoView(frxReport1.FindComponent('mmDinheiro')).Text      := 'R$ ' + NFormat(qBaixasVALOR_DINHEIRO.AsFloat);
  TfrxMemoView(frxReport1.FindComponent('mmCartaoCredito')).Text := 'R$ ' + NFormat(qBaixasVALOR_CARTAO_CREDITO.AsFloat);
  TfrxMemoView(frxReport1.FindComponent('mmCartaoDebito')).Text  := 'R$ ' + NFormat(qBaixasVALOR_CARTAO_DEBITO.AsFloat);
  TfrxMemoView(frxReport1.FindComponent('mmCheque')).Text        := 'R$ ' + NFormat(qBaixasVALOR_CHEQUE.AsFloat);
  TfrxMemoView(frxReport1.FindComponent('mmCobranca')).Text      := 'R$ ' + NFormat(qBaixasVALOR_COBRANCA.AsFloat);
  TfrxMemoView(frxReport1.FindComponent('mmCredito')).Text       := 'R$ ' + NFormat(qBaixasVALOR_CREDITO.AsFloat);

  frxReport1.ShowReport;
end;

procedure TFormRelacaoContasReceberBaixas.sgBaixasDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.ContasReceberBaixa.Informar(SFormatInt(sgBaixas.Cells[coBaixa, sgBaixas.Row]));
end;

procedure TFormRelacaoContasReceberBaixas.sgBaixasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coBaixa, coDinheiro, coCheque, coCartao, coCobranca, coCredito, coPix] then
    vAlinhamento := taRightJustify
  else if ACol = coTipoAnalitico then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgBaixas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoContasReceberBaixas.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
