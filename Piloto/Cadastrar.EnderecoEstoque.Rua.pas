unit Cadastrar.EnderecoEstoque.Rua;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _Sessao, _Biblioteca, _RecordsEspeciais, _RecordsCadastros,
  Pesquisa.MotivosAjusteEstoque, _HerancaCadastro, _FrameHerancaPrincipal,
  _FrameHenrancaPesquisas, FramePlanosFinanceiros, CheckBoxLuka, _EnderecoEstoqueRua;

type
  TFormEnderecoEstoqueRua = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eDescricao: TEditLuka;
  private
    procedure PreencherRegistro(pRuaId: RecEnderecoEstoqueRua);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

uses Pesquisa.EnderecoEstoqueRua;

{ TFormEnderecoEstoqueRua }

procedure TFormEnderecoEstoqueRua.BuscarRegistro;
var
  vEnderecoEstoqueRua: TArray<RecEnderecoEstoqueRua>;
begin
  vEnderecoEstoqueRua := _EnderecoEstoqueRua.BuscarEnderecoEstoqueRua(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if vEnderecoEstoqueRua = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    eID.SetFocus;
    Exit;
  end;

  inherited;
  PreencherRegistro(vEnderecoEstoqueRua[0]);
end;

procedure TFormEnderecoEstoqueRua.ExcluirRegistro;
var
  vRetorno: RecRetornoBD;
begin
  vRetorno := _EnderecoEstoqueRua.ExcluirEnderecoEstoqueRua(Sessao.getConexaoBanco, eId.AsInt);
  if vRetorno.TeveErro then begin
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormEnderecoEstoqueRua.GravarRegistro(Sender: TObject);
var
  vRetorno: RecRetornoBD;
begin
  inherited;
  vRetorno :=
    _EnderecoEstoqueRua.AtualizarEnderecoEstoqueRua(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eDescricao.Text
    );

  if vRetorno.TeveErro then begin
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetorno.AsInt);
end;

procedure TFormEnderecoEstoqueRua.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([eDescricao, ckAtivo], pEditando);

  if pEditando then begin
    SetarFoco(eDescricao);
    ckAtivo.Checked := True;
  end;
end;

procedure TFormEnderecoEstoqueRua.PesquisarRegistro;
var
  vEnderecoEstoqueRua: RecEnderecoEstoqueRua;
begin
  vEnderecoEstoqueRua := RecEnderecoEstoqueRua(Pesquisa.EnderecoEstoqueRua.Pesquisar());  //AquiS
  if vEnderecoEstoqueRua = nil then
    Exit;

  inherited;
  PreencherRegistro(vEnderecoEstoqueRua);
end;

procedure TFormEnderecoEstoqueRua.PreencherRegistro(pRuaId: RecEnderecoEstoqueRua);
begin
  eID.AsInt                     := pRuaId.rua_id;
  eDescricao.Text               := pRuaId.descricao;
  pRuaId.Free;
end;

procedure TFormEnderecoEstoqueRua.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eDescricao.Trim = '' then begin
    _Biblioteca.Exclamar('A descri��o n�o foi informado corretamente, verifique!');
    SetarFoco(eDescricao);
    Abort;
  end;
end;

end.
