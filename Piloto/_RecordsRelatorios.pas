unit _RecordsRelatorios;

interface

type
  RecContasReceber = record
    receber_id: Integer;
    cliente_id: Integer;
    nome_cliente: string;
    numero_cheque: Integer;
    agencia: string;
    banco: string;
    nota_fiscal_id: Integer;
    vendedor_id: Word;
    nome_vendedor: string;
    turno_id: Integer;
    cobranca_id: Word;
    nome_tipo_cobranca: string;
    forma_pagamento: string;
    BoletoBancario: string;
    orcamento_id: Integer;
    empresa_id: Integer;
    dias_atraso: Integer;
    nome_empresa: string;
    ValorRetencao: Double;
    valor_documento: Double;
    ValorAdiantado: Currency;
    nome_emitente: string;
    telefone_emitente: string;
    nosso_numero: string;
    status: string;
    documento: string;
    Origem: string;
    data_pagamento: TDateTime;
    data_vencimento_original: TDateTime;
    data_vencimento: TDateTime;
    data_emissao: TDateTime;
    data_cadastro: TDateTime;
    codigo_barras: string;
    conta_corrente: string;
    nsu: string;
    parcela: Integer;
    numero_parcelas: Integer;
    NoCaixa: string;
    PortadorId: string;
    NomePortador: string;
    ValorMulta: Double;
    ValorJuros: Double;
    ValorPedido: Double;
    CpfCnpj: string;
    LogradouroCob: string;
    ComplementoEndCob: string;
    NumeroEndCob: string;
    BairroEndCob: string;
    CidadeEndCob: string;
    UFEndCob: string;
    CepEndCob: string;
    Email: string;
    RazaoSocial: string;
    ContasBoletosId: integer;
    observacoes: string;

    function getValorLiquido: Double;
  end;

  RecContasReceberBaixas = record
    baixa_id: Integer;
    data_pagamento: TDateTime;
    data_baixa: TDateTime;
    valor_dinheiro: Currency;
    valor_cheque: Currency;
    valor_cartao: Currency;
    valor_cobranca: Currency;
    valor_credito: Currency;
    valor_pix: Currency;
    empresa_id: Integer;
    nome_empresa: string;
    usuario_baixa_id: Integer;
    nome_usuario_baixa: string;
    observacoes: string;
    Tipo: string;
    TipoAnalitico: string;
  end;

  RecDadosCobrancas = record
    cobranca_id: Integer;
    nome_tipo_cobranca: string;
    valor_titulos: Currency;
    valor_juros: Currency;
    valor_total: Currency;
    qtde_titulos: Integer;
  end;

  RecContasPagar = record
    PagarId: Integer;
    fornecedor_id: Integer;
    nome_fornecedor: string;
    numero_cheque: Integer;
    entrada_id: Integer;
    cobranca_id: Word;
    nome_tipo_cobranca: string;
    forma_pagamento: string;
    empresa_id: Integer;
    dias_atraso: Integer;
    nome_empresa: string;
    valor_documento: Double;
    ValorAdiantado: Currency;
    ValorDesconto: Double;
    ValorSaldo: Double;
    nosso_numero: string;
    status: string;
    origem: string;
    documento: string;
    data_pagamento: TDateTime;
    data_vencimento_original: TDateTime;
    data_vencimento: TDateTime;
    Data_Contabil: TDateTime;
    data_cadastro: TDateTime;
    codigo_barras: string;
    parcela: Integer;
    numero_parcelas: Integer;
    bloqueado: string;
    PlanoFinanceiroId: string;
    NomePlanoFinanceiro: string;
    CentroCustoId: Integer;
    NomeCentroCusto: string;
    BaixaReceberOrigemId: Integer;
    BaixaId: Integer;
  end;

  RecContasPagarBaixas = record
    baixa_id: Integer;
    data_pagamento: TDateTime;
    data_baixa: TDateTime;
    valor_dinheiro: Currency;
    valor_cheque: Currency;
    valor_cartao: Currency;
    valor_cobranca: Currency;
    valor_credito: Currency;
    valor_pix: Currency;
    empresa_id: Integer;
    nome_empresa: string;
    usuario_baixa_id: Integer;
    nome_usuario_baixa: string;
    observacoes: string;
    Tipo: string;
    TipoAnalitico: string;
  end;

  RecOrcamentosBloqueados = record
    OrcamentoId: Integer;
    DataCadastro: TDateTime;
    ClienteId: Integer;
    NomeCliente: string;
    VendedorId: Integer;
    NomeVendedor: string;
    ValorTotal: Double;
    PercentualDesconto: Double;
    ValorDesconto: Double;
    CondicaoId: Integer;
    NomeCondicao: string;
    EmpresaId: Integer;
    NomeEmpresa: string;
    Status: string;
    ValorCustoEntrada: Double;
    ValorImpostos: Double;
    ValorCustoFinal: Double;
  end;

  RecOrcamentosVendas = record
    OrcamentoId: Integer;
    DevolucaoId: Integer;
    ClienteId: Integer;
    NomeCliente: string;
    DataCadastro: TDateTime;
    VendedorId: Integer;
    NomeVendedor: string;
    CondicaoId: Integer;
    NomeCondicaoPagamento: string;
    ValorTotalProdutos: Double;
    ValorOutrasDespesas: Double;
    ValorFrete : Double;
    ValorDesconto: Double;
    ValorTotal: Double;
    EmpresaId: Integer;
    NomeEmpresa: string;
    tipoAnaliseCusto: string;
    IndiceDescontoVendaId: Integer;
    ValorCustoEntrada: Double;
    ValorCustoFinal: Double;
    Status: string;
    StatusAnalitico: string;
    DataHoraRecebimento: TDateTime;
    ClienteGrupoId: Integer;
    NomeGrupoCliente: string;
    TipoMovimento: string;
    TipoMovimentoAnalitico: string;
    TipoAcompanhamentoId: Integer;
    TipoAcompanhamentoDesc: string;
    ParceiroId: Integer;
    ParceiroNome: string;
    DataAgrupamento: TDate;
  end;

  RecProdutoOrcamento = record
    ProdutoId: Integer;
    ItemId: Integer;
    NomeProduto: string;
    MarcaId: Integer;
    NomeMarca: string;
    PrecoUnitario: Double;
    Quantidade: Double;
    UnidadeVenda: string;
    ValorTotal: Double;
    OrcamentoId: Integer;
  end;

  RecDevolucoes = record
    DevolucaoId: Integer;
    OrcamentoId: Integer;
    ClienteId: Integer;
    NomeCliente: string;
    ValorBruto: Double;
    ValorOutrasDepesas: Double;
    ValorDesconto: Double;
    ValorLiquido: Double;
    DataHoraDevolucao: TDateTime;
    UsuarioCadastroId: Integer;
    NomeUsuarioDevolucao: string;
    EmpresaId: Integer;
    NomeEmpresa: string;
  end;

  RecProdutoCurvaABC = record
    Tipo: string;
    ProdutoId: Integer;
    NomeProduto: string;
    PrecoMedioUnitario: Double;
    Quantidade: Double;
    UnidadeVenda: string;
    ValorTotal: Double;
    CustoTotal: Double;
    Percentual: Double;
    AcumulativoPercentual: Double;
    Classificacao: string;
  end;

  RecClienteCurvaABC = record
    Tipo: string;
    ClienteId: Integer;
    NomeCliente: string;
    ValorTotal: Double;
    QuantidadeVendas: Integer;
    Percentual: Double;
    AcumulativoPercentual: Double;
    Classificacao: string;
  end;

  RecDadosVidaCliente = record
    CadastroId: Integer;
    DataNascimento: TDateTime;
    TelefonePrincipal: string;
    TelefoneCelular: string;
    DataCadastro: TDateTime;
    LimiteCredito: Double;
    DataAprovacaoLimiteCredito: TDateTime;
    DataValidadeLimiteCredito: TDateTime;
    ValorUtilizadoLimiteCredito: Double;
    SaldoRestante: Double;
    ValorFinanceiroAberto: Double;
    QtdeFinanceirosAbertos: Integer;
    ValorFinanceiroBaixado: Double;
    QtdeFinanceirosBaixados: Integer;
    QtdeOrcamentosAbertos: Integer;
    QtdePedidos: Integer;
    QtdeDevolucoes: Integer;
    DataUltimaVenda: TDateTime;
    QtdeDiasSemComprar: Integer;
    DataUltimaEntrega: TDateTime;
    ValorUltimaEntrega: Double;
    QtdeEntregas: Integer;
    QtdeEntregasRetornadas: Integer;
    DataUltimaRetirada: TDateTime;
    ValorUltimaRetirada: Double;
    QtdeRetiradas: Integer;
  end;

    RecDadosLimiteCreditoCliente = record
    CadastroId: Integer;
    NomeCliente: string;
    LimiteCredito: Double;
    DataAprovacaoLimiteCredito: TDateTime;
    DataValidadeLimiteCredito: TDateTime;
    ValorUtilizadoLimiteCredito: Double;
    SaldoRestante: Double;
  end;


implementation

{ RecContasReceber }

function RecContasReceber.getValorLiquido: Double;
begin
  Result := valor_documento + ValorMulta + ValorJuros - ValorRetencao - ValorAdiantado;
end;

end.
