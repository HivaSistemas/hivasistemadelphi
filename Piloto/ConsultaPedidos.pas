unit ConsultaPedidos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls,
  CheckBoxLuka, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Mask, EditLukaData,
  StaticTextLuka, SpeedButtonLuka, Vcl.Grids, GridLuka, _RecordsOrcamentosVendas;

type
  TFormConsultaPedidos = class(TFormHerancaCadastroCodigo)
    lb2: TLabel;
    eCliente: TEditLuka;
    lb12: TLabel;
    eVendedor: TEditLuka;
    lb13: TLabel;
    eEmpresa: TEditLuka;
    st3: TStaticText;
    stStatus: TStaticText;
    eDataCadastro: TEditLukaData;
    lb3: TLabel;
    lb4: TLabel;
    eDataRecebimento: TEditLukaData;
    stSPC: TStaticText;
    lb6: TLabel;
    eTotalProdutos: TEditLuka;
    Label2: TLabel;
    lb7: TLabel;
    eOutrasDespesas: TEditLuka;
    lb10: TLabel;
    lb8: TLabel;
    eValorDesconto: TEditLuka;
    lb9: TLabel;
    lb23: TLabel;
    eValorFrete: TEditLuka;
    lb11: TLabel;
    lb5: TLabel;
    eTotalOrcamento: TEditLuka;
    StaticText2: TStaticText;
    st1: TStaticText;
    stOrigemVenda: TStaticText;
    StaticText1: TStaticText;
    stTipoEntrega: TStaticText;
    st2: TStaticText;
    stReceberNaEntrega: TStaticTextLuka;
    sbMixVenda: TSpeedButtonLuka;
    StaticText3: TStaticText;
    sgProdutos: TGridLuka;
    sbInformacoesAcumulado: TSpeedButtonLuka;
    lb18: TLabel;
    eCondicaoPagamento: TEditLuka;
    lb24: TLabel;
    eAcumuladoId: TEditLuka;
    SpeedButtonLuka1: TSpeedButtonLuka;
    lb26: TLabel;
    eTotalProdutosPromocao: TEditLuka;
    miCancelarFechamentoPedido: TSpeedButton;
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sbInformacoesAcumuladoClick(Sender: TObject);
    procedure sbMixVendaClick(Sender: TObject);
    procedure SpeedButtonLuka1Click(Sender: TObject);
    procedure miCancelarFechamentoPedidoClick(Sender: TObject);
  private
    procedure PreencherRegistro(vOrcamento: RecOrcamentos);
  public
    { Public declarations }
  protected
    procedure BuscarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
    procedure PesquisarRegistro; override;
  end;

var
  FormConsultaPedidos: TFormConsultaPedidos;

implementation

{$R *.dfm}

uses _RecordsCadastros, _OrcamentosItens, _Sessao, _Biblioteca, _Orcamentos,
  Informacoes.Orcamento, MixVendas, InformacoesAcumulado, Pesquisa.Orcamentos,
  _RecordsEspeciais;

type
  coGridProdutos = record
  const
    ProdutoId       = 0;
    Nome            = 1;
    Marca           = 2;
    TipoPrecoUtil   = 3;
    PrecoUnitario   = 4;
    Quantidade      = 5;
    Unidade         = 6;
    ValorDesconto   = 7;
    PercDescManual  = 8;
    ValorDescManual = 9;
    OutrasDespesas  = 10;
    ValorLiquido    = 11;
    ValorFrete      = 12;
  end;

{ TFormConsultaPedidos }

procedure TFormConsultaPedidos.BuscarRegistro;
var
  vOrcamento: TArray<RecOrcamentos>;
begin
  vOrcamento := _Orcamentos.BuscarOrcamentos(Sessao.getConexaoBanco, 0, [eID.AsInt]);

  if vOrcamento = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    SetarFoco(eID);
    Exit;
  end;

  inherited;
  PreencherRegistro(vOrcamento[0]);
end;

procedure TFormConsultaPedidos.miCancelarFechamentoPedidoClick(Sender: TObject);
var
  vId: Integer;
  vRetorno: RecRetornoBD;
begin
  inherited;
  vId := eId.AsInt;
  if vId = 0 then
    Exit;

  if stStatus.Caption <>  'Aguardando recebimento' then begin
    Exclamar('S� � permitido voltar para or�amento pedidos com status "Aguardando recebimento".');
    Exit;
  end;

  if not Perguntar('Deseja voltar esta venda para or�amento?') then
    Exit;

  vRetorno := _Orcamentos.CancelarFechamentoPedido(Sessao.getConexaoBanco, vId);
  if vRetorno.TeveErro then begin
    Exclamar(vRetorno.MensagemErro);
    Exit;
  end;

  Modo(False);
end;

procedure TFormConsultaPedidos.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    eCondicaoPagamento,
    eCliente,
    eVendedor,
    eEmpresa,
    eDataCadastro,
    eDataRecebimento,
    eAcumuladoId,
    eTotalProdutos,
    eTotalProdutosPromocao,
    eOutrasDespesas,
    eValorDesconto,
    eValorFrete,
    eTotalOrcamento],
    pEditando
  );

  sgProdutos.ClearGrid();

  _Biblioteca.LimparCampos([
    stTipoEntrega,
    stOrigemVenda,
    stReceberNaEntrega,
    stStatus
  ]);
end;

procedure TFormConsultaPedidos.PesquisarRegistro;
var
  vRetTela: TRetornoTelaFinalizar<RecOrcamentos>;
begin
  vRetTela := Pesquisa.Orcamentos.Pesquisar(tpConsultaPedidos);

  if vRetTela.BuscaCancelada then
    Exit;

  inherited;
  PreencherRegistro(vRetTela.Dados);
end;

procedure TFormConsultaPedidos.PreencherRegistro(vOrcamento: RecOrcamentos);
var
  i: Integer;
  vOrcamentoItens: TArray<RecOrcamentoItens>;
begin
  eID.AsInt := vOrcamento.orcamento_id;

  vOrcamentoItens := _OrcamentosItens.BuscarOrcamentosItens(Sessao.getConexaoBanco, 0, [eID.AsInt]);
  if vOrcamentoItens = nil then begin
    Exclamar('Itens do or�amento n�o encontrado, verifique!');
    Exit;
  end;

  eCondicaoPagamento.SetInformacao(vOrcamento.condicao_id, vOrcamento.nome_condicao_pagto);
  stTipoEntrega.Caption := vOrcamento.TipoEntregaAnalitico;
  stOrigemVenda.Caption := vOrcamento.OrigemVendaAnalitico;
  eCliente.SetInformacao(vOrcamento.cliente_id, vOrcamento.nome_cliente);
  eVendedor.SetInformacao(vOrcamento.vendedor_id, vOrcamento.nome_vendedor);
  eEmpresa.SetInformacao(vOrcamento.empresa_id, vOrcamento.nome_empresa);
  stReceberNaEntrega.Caption := vOrcamento.ReceberNaEntrega;

  stStatus.Caption := vOrcamento.status_analitico;

  if vOrcamento.status_analitico = 'Bloqueado' then
    stStatus.Font.Color := clRed
  else
    stStatus.Font.Color := clBlue;

  eDataCadastro.AsData := vOrcamento.data_cadastro;
  eDataRecebimento.AsDataHora := vOrcamento.data_hora_recebimento;
  eAcumuladoId.SetInformacao(vOrcamento.AcumuladoId);

  eTotalProdutos.AsDouble  := vOrcamento.valor_total_produtos;
  eTotalProdutosPromocao.AsDouble := vOrcamento.ValorTotalProdutosPromocao;
  eOutrasDespesas.AsDouble := vOrcamento.valor_outras_despesas;
  eValorDesconto.AsDouble  := vOrcamento.valor_desconto;
  eValorFrete.AsDouble     := vOrcamento.ValorFrete + vOrcamento.ValorFreteItens;

  eTotalOrcamento.AsDouble := vOrcamento.valor_total;

  for i := Low(vOrcamentoItens) to High(vOrcamentoItens) do begin
    sgProdutos.Cells[coGridProdutos.ProdutoId, i + 1]      := NFormat(vOrcamentoItens[i].produto_id);
    sgProdutos.Cells[coGridProdutos.Nome, i + 1]           := vOrcamentoItens[i].nome;
    sgProdutos.Cells[coGridProdutos.Marca, i + 1]          := vOrcamentoItens[i].nome_marca;
    sgProdutos.Cells[coGridProdutos.TipoPrecoUtil, i + 1]  := vOrcamentoItens[i].TipoPrecoUtilizadoAnalitico;
    sgProdutos.Cells[coGridProdutos.PrecoUnitario, i + 1]  := NFormat(vOrcamentoItens[i].preco_unitario);
    sgProdutos.Cells[coGridProdutos.Quantidade, i + 1]     := NFormat(vOrcamentoItens[i].quantidade);
    sgProdutos.Cells[coGridProdutos.Unidade, i + 1]        := vOrcamentoItens[i].unidade_venda;
    sgProdutos.Cells[coGridProdutos.ValorDesconto, i + 1]  := NFormatN(vOrcamentoItens[i].valor_total_desconto);
    sgProdutos.Cells[coGridProdutos.PercDescManual, i + 1] := NFormatN(vOrcamentoItens[i].PercDescontoPrecoManual, 5);
    sgProdutos.Cells[coGridProdutos.ValorDescManual, i + 1] := NFormatN(vOrcamentoItens[i].ValorDescUnitPrecoManual);
    sgProdutos.Cells[coGridProdutos.OutrasDespesas, i + 1] := NFormatN(vOrcamentoItens[i].valor_total_outras_despesas);
    sgProdutos.Cells[coGridProdutos.ValorLiquido, i + 1]   := NFormat(vOrcamentoItens[i].valor_total + vOrcamentoItens[i].valor_total_outras_despesas - vOrcamentoItens[i].valor_total_desconto);
    sgProdutos.Cells[coGridProdutos.ValorFrete, i + 1]     := NFormatN(vOrcamentoItens[i].ValorTotalFrete);
  end;
  sgProdutos.SetLinhasGridPorTamanhoVetor( Length(vOrcamentoItens) );
end;

procedure TFormConsultaPedidos.sbInformacoesAcumuladoClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar(eID.AsInt);
end;

procedure TFormConsultaPedidos.sbMixVendaClick(Sender: TObject);
begin
  inherited;
  MixVendas.Negociacao(eId.AsInt);
end;

procedure TFormConsultaPedidos.sgProdutosDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    coGridProdutos.ProdutoId,
    coGridProdutos.PrecoUnitario,
    coGridProdutos.Quantidade,
    coGridProdutos.ValorDesconto,
    coGridProdutos.OutrasDespesas,
    coGridProdutos.ValorLiquido,
    coGridProdutos.ValorFrete,
    coGridProdutos.PercDescManual,
    coGridProdutos.ValorDescManual
  ] then
    vAlinhamento := taRightJustify
  else if ACol = coGridProdutos.TipoPrecoUtil then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormConsultaPedidos.sgProdutosGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coGridProdutos.TipoPrecoUtil then begin
    AFont.Style := [fsBold];
    AFont.Color := $000096DB;
  end;
end;

procedure TFormConsultaPedidos.SpeedButtonLuka1Click(Sender: TObject);
begin
  inherited;
  InformacoesAcumulado.Informar( eAcumuladoId.IdInformacao );
end;

end.
