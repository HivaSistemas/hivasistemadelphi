inherited FormBuscarCondicoesCartaoCreditoPDV: TFormBuscarCondicoesCartaoCreditoPDV
  Caption = 'Buscar condi'#231#245'es cart'#227'o cr'#233'dito PDV'
  ClientHeight = 272
  ClientWidth = 628
  ExplicitWidth = 634
  ExplicitHeight = 301
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 235
    Width = 628
    ExplicitTop = 235
    ExplicitWidth = 628
  end
  object sgCondicoes: TGridLuka
    Left = 1
    Top = 2
    Width = 625
    Height = 232
    ColCount = 2
    DefaultRowHeight = 35
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goRowSelect]
    ParentFont = False
    TabOrder = 1
    OnDblClick = sgCondicoesDblClick
    OnDrawCell = sgCondicoesDrawCell
    OnKeyDown = sgCondicoesKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HRow.Strings = (
      'CART'#195'O 1X 2X R$ 50,00')
    Grid3D = False
    RealColCount = 2
    AtivarPopUpSelecao = False
    ColWidths = (
      390
      205)
  end
end
