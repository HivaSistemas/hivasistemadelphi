unit RelacaoBloqueiosEstoques;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _Biblioteca, _Sessao,
  Vcl.ComCtrls, Vcl.StdCtrls, CheckBoxLuka, Vcl.Buttons, Vcl.ExtCtrls, _BloqueiosEstoques,
  FrameMarcas, FrameEmpresas, FrameProdutos, _FrameHerancaPrincipal, _BloqueiosEstoquesItens,
  _FrameHenrancaPesquisas, Frame.DepartamentosSecoesLinhas, FrameLocais, BaixarCancelarItensBloqueioEstoque,
  FrameDataInicialFinal, FrameFuncionarios, Vcl.Grids, GridLuka, StaticTextLuka,
  Vcl.Menus, Frame.TipoBloqueioEstoque, frxClass, GroupBoxLuka;

type
  TFormRelacaoBloqueiosEstoques = class(TFormHerancaRelatoriosPageControl)
    FrProdutos: TFrProdutos;
    FrEmpresa: TFrEmpresas;
    FrDataCadastroProduto: TFrDataInicialFinal;
    FrLocaisBloqueio: TFrLocais;
    FrUsuariosCadastro: TFrFuncionarios;
    st1: TStaticTextLuka;
    sgBloqueios: TGridLuka;
    splSeparador: TSplitter;
    st2: TStaticTextLuka;
    sgItens: TGridLuka;
    pmOpcoes: TPopupMenu;
    miBaixarItensBloqueioSelecionado: TSpeedButton;
    miCancelarItensBloqueioFocado: TSpeedButton;
    StaticTextLuka1: TStaticTextLuka;
    StaticTextLuka2: TStaticTextLuka;
    FrDataIbxbloqueio: TFrDataInicialFinal;
    FrUsuariodaBaixa: TFrFuncionarios;
    Panel1: TPanel;
    sbAlterarTipoAcompanhamento: TSpeedButton;
    FrTipoBloqueioEstoque1: TFrTipoBloqueioEstoque;
    frxReport: TfrxReport;
    dsCapaBloqueio: TfrxUserDataSet;
    dsItensBloqueio: TfrxUserDataSet;
    gbStatus: TGroupBoxLuka;
    ckAbertos: TCheckBoxLuka;
    ckBaixados: TCheckBoxLuka;
    procedure sgBloqueiosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgBloqueiosClick(Sender: TObject);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure miBaixarItensBloqueioSelecionadoClick(Sender: TObject);
    procedure sgBloqueiosGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sbAlterarTipoAcompanhamentoClick(Sender: TObject);
    procedure dsCapaBloqueioFirst(Sender: TObject);
    procedure dsCapaBloqueioGetValue(const VarName: string; var Value: Variant);
    procedure dsCapaBloqueioNext(Sender: TObject);
    procedure dsCapaBloqueioPrior(Sender: TObject);
    procedure dsItensBloqueioCheckEOF(Sender: TObject; var Eof: Boolean);
    procedure dsItensBloqueioFirst(Sender: TObject);
    procedure dsItensBloqueioGetValue(const VarName: string;
      var Value: Variant);
    procedure dsItensBloqueioNext(Sender: TObject);
    procedure dsItensBloqueioPrior(Sender: TObject);
  private
    FItens: TArray<RecBloqueiosEstoquesItens>;
    MasterNo: Integer;
    DetailNo: Integer;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
  end;

implementation

uses
  _RecordsEspeciais, _RecordsCadastros, BuscarTipoBloqueioEstoque;

{$R *.dfm}

const
  coBloqueioEstoqueId = 0;
  coEmpresa           = 1;
  coStatus            = 2;
  coCustoTotal        = 3;
  coTipoBloqueio      = 4;
  coDataHoraCadastro  = 5;
  coUsuarioCadastro   = 6;
  coDataHoraBaixa     = 7;
  coUsuarioBaixa      = 8;
  coObservacoes       = 9;
  coTipoBloqueioId    = 10;
  coTipoLinha         = 11;

  (* Grid de produtos *)
  ciProdutoId         = 0;
  ciNome              = 1;
  ciMarca             = 2;
  ciCustoProduto      = 3;
  ciQuantidade        = 4;
  ciCustoProdutoTotal = 5;
  ciBaixados          = 6;
  ciCancelados        = 7;
  ciSaldo             = 8;
  ciUnidade           = 9;
  ciLote              = 10;
  ciLocal             = 11;

  coLinhaDetalhe   = 'D';
  coLinhaTotCusto  = 'TC';


procedure TFormRelacaoBloqueiosEstoques.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;

  vBloqueiosIds: TArray<Integer>;
  vBloqueios: TArray<RecBloqueiosEstoques>;
  vTotalCusto: Double;
begin
  inherited;

  if gbStatus.NenhumMarcado then begin
    Exclamar('� necess�rio selecionar pelo menos 1 status');
    gbStatus.SetFocus;
    Abort;
  end;

  vSql := '';
  vBloqueiosIds := nil;

  if ckAbertos.Checked then begin
    _Biblioteca.WhereOuAnd(vSql, ' BLO.STATUS = ''A'' ');
  end
  else if ckBaixados.Checked then begin
    _Biblioteca.WhereOuAnd(vSql, ' BLO.STATUS = ''B'' ');
  end;

  FrEmpresa.getSqlFiltros('BLO.EMPRESA_ID', vSql, True);
  FrProdutos.getSqlFiltros('BTI.PRODUTO_ID', vSql, True);
  FrLocaisBloqueio.getSqlFiltros('BTI.LOCAL_ID', vSql, True);
  FrUsuariosCadastro.getSqlFiltros('BLO.USUARIO_CADASTRO_ID', vSql, True);
  FrDataCadastroProduto.getSqlFiltros('BLO.DATA_HORA_CADASTRO', vSql, True);
  FrDataIbxbloqueio.getSqlFiltros('BLO.DATA_HORA_BAIXA', vSql, True);
  FrUsuariodaBaixa.getSqlFiltros('BLO.USUARIO_BAIXA_ID', vSql, True);
  FrTipoBloqueioEstoque1.getSqlFiltros('BLO.TIPO_BLOQUEIO_ID', vSql, True);
  vSql := vSql + ' order by BLO.BLOQUEIO_ID asc ';

  vBloqueios := _BloqueiosEstoques.BuscarBloqueiosEstoquesComando(Sessao.getConexaoBanco, vSql);
  if vBloqueios = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;  
  end;

  vTotalCusto := 0.0;
  for i := Low(vBloqueios) to High(vBloqueios) do begin
    sgBloqueios.Cells[coBloqueioEstoqueId, i + 1] := NFormat(vBloqueios[i].BloqueioId);
    sgBloqueios.Cells[coEmpresa, i + 1]           := getInformacao(vBloqueios[i].EmpresaId, vBloqueios[i].NomeEmpresa);
    sgBloqueios.Cells[coStatus, i + 1]            := vBloqueios[i].StatusAnalitico;
    sgBloqueios.Cells[coCustoTotal, i + 1]        := NFormatEstoque(vBloqueios[i].CustoTotal);
    sgBloqueios.Cells[coTipoBloqueio, i + 1]      := getInformacao(vBloqueios[i].TipoBloqueioId, vBloqueios[i].TipoBloqueioDescricao);
    sgBloqueios.Cells[coDataHoraCadastro, i + 1]  := DHFormat(vBloqueios[i].DataHoraCadastro);
    sgBloqueios.Cells[coUsuarioCadastro, i + 1]   := getInformacao(vBloqueios[i].UsuarioCadastroId, vBloqueios[i].NomeUsuarioCadastro);
    sgBloqueios.Cells[coDataHoraBaixa, i + 1]     := DHFormatN(vBloqueios[i].DataHoraBaixa);
    sgBloqueios.Cells[coUsuarioBaixa, i + 1]      := getInformacao(vBloqueios[i].UsuarioBaixaId, vBloqueios[i].NomeUsuarioBaixa);
    sgBloqueios.Cells[coObservacoes, i + 1]       := vBloqueios[i].Observacoes;
    sgBloqueios.Cells[coTipoBloqueioId, i + 1]    := NFormat(vBloqueios[i].TipoBloqueioId);
    sgBloqueios.Cells[coTipoLinha, i + 1]         := coLinhaDetalhe;

    _Biblioteca.AddNoVetorSemRepetir(vBloqueiosIds, vBloqueios[i].BloqueioId);
    vTotalCusto := vTotalCusto + vBloqueios[i].CustoTotal;
  end;

  sgBloqueios.Cells[coStatus, i + 1]       := 'TOTAL: ';
  sgBloqueios.Cells[coCustoTotal, i + 1]   := NFormatEstoque(vTotalCusto);
  sgBloqueios.Cells[coTipoLinha, i + 1]    := coLinhaTotCusto;

  sgBloqueios.SetLinhasGridPorTamanhoVetor( Length(vBloqueios) + 1 );

  FItens := _BloqueiosEstoquesItens.BuscarBloqueiosEstoquesItensComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('BEI.BLOQUEIO_ID', vBloqueiosIds));

  dsCapaBloqueio.RangeEnd := reCount;
  dsCapaBloqueio.RangeEndCount := i;

  dsItensBloqueio.RangeEnd := reCount;
  dsItensBloqueio.RangeEndCount := High(FItens);

  sgBloqueiosClick(Sender);
  SetarFoco(sgItens);
end;

procedure TFormRelacaoBloqueiosEstoques.dsCapaBloqueioFirst(Sender: TObject);
begin
  inherited;
  MasterNo := 1;
end;

procedure TFormRelacaoBloqueiosEstoques.dsCapaBloqueioGetValue(
  const VarName: string; var Value: Variant);
begin
  inherited;
  Value := sgBloqueios.Cells[dsCapaBloqueio.Fields.IndexOf(VarName) ,MasterNo];
  if VarName = 'custo' then
    Value := StrToFloat(StringReplace(Value,'.','',[rfReplaceAll]));

end;

procedure TFormRelacaoBloqueiosEstoques.dsCapaBloqueioNext(Sender: TObject);
begin
  inherited;
  Inc(MasterNo);
end;

procedure TFormRelacaoBloqueiosEstoques.dsCapaBloqueioPrior(Sender: TObject);
begin
  inherited;
  Dec(MasterNo);
end;

procedure TFormRelacaoBloqueiosEstoques.dsItensBloqueioCheckEOF(Sender: TObject;
  var Eof: Boolean);
var
  vTotalItens : integer;
begin
  vTotalItens := High(FItens);
  if not Eof then
    Eof := DetailNo > vTotalItens;
end;

procedure TFormRelacaoBloqueiosEstoques.dsItensBloqueioFirst(Sender: TObject);
begin
  inherited;
  DetailNo := 1;
  while (not dsItensBloqueio.Eof)
    and (FItens[DetailNo].BloqueioId <> SFormatInt(sgBloqueios.Cells[coBloqueioEstoqueId ,MasterNo])) do
    Inc(DetailNo);
end;

procedure TFormRelacaoBloqueiosEstoques.dsItensBloqueioGetValue(
  const VarName: string; var Value: Variant);
begin
  inherited;
 inherited;
  if VarName = 'codigo' then
    Value := FItens[DetailNo].BloqueioId;
  if VarName = 'produto' then
    Value := FItens[DetailNo].ProdutoId;
  if VarName = 'nome' then
    Value := FItens[DetailNo].NomeProduto;
  if VarName = 'marca' then
    Value := FItens[DetailNo].NomeMarca;
  if VarName = 'custo' then
    Value := FItens[DetailNo].CustoProduto;
  if VarName = 'qtde' then
    Value := FItens[DetailNo].Quantidade;
  if VarName = 'custos_total' then
    Value := FItens[DetailNo].CustoProdutoTotal;
  if VarName = 'baixados' then
    Value := FItens[DetailNo].Baixados;
  if VarName = 'cancelados' then
    Value := FItens[DetailNo].Cancelados;
  if VarName = 'saldos' then
    Value := FItens[DetailNo].Saldo;
  if VarName = 'unid' then
    Value := FItens[DetailNo].UnidadeVenda;
  if VarName = 'lote' then
    Value := FItens[DetailNo].Lote;
  if VarName = 'local' then
    Value := FItens[DetailNo].LocalId;

end;

procedure TFormRelacaoBloqueiosEstoques.dsItensBloqueioNext(Sender: TObject);
begin
  inherited;
  Inc(DetailNo);
  while (not dsItensBloqueio.Eof)
    and (FItens[DetailNo].BloqueioId <> SFormatInt(sgBloqueios.Cells[coBloqueioEstoqueId ,MasterNo])) do
  begin
    Inc(DetailNo);
  end;
end;

procedure TFormRelacaoBloqueiosEstoques.dsItensBloqueioPrior(Sender: TObject);
begin
  inherited;
  Dec(DetailNo);
  while (not dsItensBloqueio.Eof)
    and (FItens[DetailNo].BloqueioId <> SFormatInt(sgBloqueios.Cells[coBloqueioEstoqueId ,MasterNo])) do
  begin
    Dec(DetailNo);
  end;
end;

procedure TFormRelacaoBloqueiosEstoques.Imprimir(Sender: TObject);
begin
  inherited;
  frxReport.ShowReport();
end;

procedure TFormRelacaoBloqueiosEstoques.miBaixarItensBloqueioSelecionadoClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar;
begin
  inherited;
  vRetTela :=
    BaixarCancelarItensBloqueioEstoque.BaixarCancelar(
      SFormatInt(sgBloqueios.Cells[coBloqueioEstoqueId, sgBloqueios.Row]),
      Sender = miBaixarItensBloqueioSelecionado
    );
  if vRetTela.BuscaCancelada then
    Exit;

  _Biblioteca.RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoBloqueiosEstoques.sbAlterarTipoAcompanhamentoClick(
  Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<RecTipoBloqueioEstoque>;
  vRetBanco: RecRetornoBD;
  vTipoBloqueioId: Integer;
  vBloqueioEstoqueId: Integer;
begin
  inherited;

  if sgBloqueios.Cells[coBloqueioEstoqueId, 1] = '' then
    Exit;

  vTipoBloqueioId := StrToInt(sgBloqueios.Cells[coTipoBloqueioId, sgBloqueios.Row]);
  vBloqueioEstoqueId := SFormatInt(sgBloqueios.Cells[coBloqueioEstoqueId, sgBloqueios.Row]);

  vRetorno := BuscarTipoBloqueioEstoque.BuscarTipoBloqueio(vTipoBloqueioId);
  if vRetorno.BuscaCancelada then
    Exit;

  vRetBanco := _BloqueiosEstoques.AtualizarTipoBloqueioEstoque(
    Sessao.getConexaoBanco,
    vRetorno.Dados.tipo_bloqueio_id,
    vBloqueioEstoqueId
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Erro ao atualizar o tipo de bloqueio de estoque.');
    Exit;
  end;

  _Biblioteca.Informar('Tipo de bloqueio de atualizado!');
  Carregar(nil);
end;

procedure TFormRelacaoBloqueiosEstoques.sgBloqueiosClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vBloqueioId: Integer;
begin
  inherited;

  sgItens.ClearGrid;

  vLinha := 0;
  vBloqueioId := SFormatInt(sgBloqueios.Cells[coBloqueioEstoqueId, sgBloqueios.Row]);
  for i := Low(FItens) to High(FItens) do begin
    if vBloqueioId <> FItens[i].BloqueioId then
      Continue;

    Inc(vLinha);
    
    sgItens.Cells[ciProdutoId, vLinha]         := NFormat(FItens[i].ProdutoId);
    sgItens.Cells[ciNome, vLinha]              := FItens[i].NomeProduto;
    sgItens.Cells[ciMarca, vLinha]             := FItens[i].NomeMarca;
    sgItens.Cells[ciCustoProduto, vLinha]      := NFormatNEstoque(FItens[i].CustoProduto);
    sgItens.Cells[ciQuantidade, vLinha]        := NFormatNEstoque(FItens[i].Quantidade);
    sgItens.Cells[ciCustoProdutoTotal, vLinha] := NFormatNEstoque(FItens[i].CustoProdutoTotal);
    sgItens.Cells[ciBaixados, vLinha]          := NFormatNEstoque(FItens[i].Baixados);
    sgItens.Cells[ciCancelados, vLinha]        := NFormatNEstoque(FItens[i].Cancelados);
    sgItens.Cells[ciSaldo, vLinha]             := NFormatNEstoque(FItens[i].Saldo);
    sgItens.Cells[ciUnidade, vLinha]           := FItens[i].UnidadeVenda;
    sgItens.Cells[ciLote, vLinha]              := FItens[i].Lote;
    sgItens.Cells[ciLocal, vLinha]             := getInformacao(FItens[i].LocalId, FItens[i].NomeLocal);
  end;
  sgItens.SetLinhasGridPorTamanhoVetor( vLinha );
end;

procedure TFormRelacaoBloqueiosEstoques.sgBloqueiosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coBloqueioEstoqueId, coCustoTotal] then
    vAlinhamento := taRightJustify
  else if ACol = coStatus then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgBloqueios.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoBloqueiosEstoques.sgBloqueiosGetCellColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgBloqueios.Cells[coTipoLinha, ARow] = coLinhaTotCusto then begin
    AFont.Color := clWhite;
    AFont.Style := [fsBold];
    ABrush.Color := $000096DB;
  end;
end;

procedure TFormRelacaoBloqueiosEstoques.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[ciProdutoId, ciCustoProduto, ciQuantidade, ciCustoProdutoTotal, ciBaixados, ciCancelados, ciSaldo] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);  
end;

procedure TFormRelacaoBloqueiosEstoques.VerificarRegistro(Sender: TObject);
begin
  inherited;
  sgBloqueios.ClearGrid;
  sgItens.ClearGrid;
end;

end.
