inherited FormDefinirPrecosVenda: TFormDefinirPrecosVenda
  Caption = 'Precifica'#231#227'o simplificada'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    inherited sbCarregar: TSpeedButton
      ParentCustomHint = False
    end
    inherited sbImprimir: TSpeedButton
      Top = 224
      Visible = False
      ExplicitTop = 224
    end
    object sbGravar: TSpeedButtonLuka [2]
      Left = 3
      Top = 63
      Width = 110
      Height = 40
      Flat = True
      NumGlyphs = 2
      OnClick = sbGravarClick
      TagImagem = 9
      PedirCertificacao = False
      PermitirAutOutroUsuario = False
    end
  end
  inherited pcDados: TPageControl
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      inline FrEmpresas: TFrEmpresas
        Left = 0
        Top = 168
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitTop = 168
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 105
            Caption = 'Empresas atualizar'
            ExplicitWidth = 105
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
        inherited ckSomenteAtivos: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      inline FrProdutos: TFrProdutos
        Left = 0
        Top = -2
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitTop = -2
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 48
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
        inherited ckSomenteAtivos: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited ckFiltroExtra: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      inline FrDepartamentosSecoesLinhas: TFrameDepartamentosSecoesLinhas
        Left = 0
        Top = 83
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitTop = 83
        ExplicitWidth = 403
        ExplicitHeight = 81
        inherited sgPesquisa: TGridLuka
          Width = 378
          Height = 64
          ExplicitWidth = 378
          ExplicitHeight = 64
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 128
            Caption = 'Departamentos/se'#231#245'es'
            ExplicitWidth = 128
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          Height = 65
          ExplicitLeft = 378
          ExplicitHeight = 65
        end
      end
      inline FrDataCadastroProduto: TFrDataInicialFinal
        Left = 424
        Top = 0
        Width = 217
        Height = 46
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 424
        ExplicitWidth = 217
        ExplicitHeight = 46
        inherited Label1: TLabel
          Width = 156
          Height = 14
          Caption = 'Data de cadastro do produto'
          ExplicitWidth = 156
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      object rgTipoCustoFormacao: TRadioGroupLuka
        Left = 426
        Top = 44
        Width = 185
        Height = 74
        Caption = ' Tipo de custo para forma'#231#227'o '
        ItemIndex = 0
        Items.Strings = (
          'Custo comercial'
          'CMV')
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        Valores.Strings = (
          'FIN'
          'COM'
          'CMV')
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object sgItens: TGridLuka
        Left = 0
        Top = 0
        Width = 884
        Height = 518
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 13
        Ctl3D = True
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 4
        RowCount = 2
        GradientEndColor = 15395562
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goThumbTracking]
        ParentCtl3D = False
        PopupMenu = pmOpcoes
        TabOrder = 0
        OnDrawCell = sgItensDrawCell
        OnGetEditMask = sgItensGetEditMask
        OnKeyDown = sgItensKeyDown
        OnSelectCell = sgItensSelectCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Sel.'
          'Leg.'
          'Produto'
          'Nome'
          'Cust.Compra'
          '% Custo venda'
          '% Marg. varejo'
          'Pre'#231'o varejo'
          'Custo comerc.'
          'Qtde.min.varej.'
          'Data inic. pre'#231'o'
          'Pre'#231'o atual'
          'Data pre.atual')
        OnGetCellColor = sgItensGetCellColor
        OnGetCellPicture = sgItensGetCellPicture
        OnArrumarGrid = sgItensArrumarGrid
        Grid3D = False
        RealColCount = 54
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          31
          33
          52
          289
          80
          88
          82
          75
          81
          87
          94
          80
          86)
      end
    end
  end
  object pmOpcoes: TPopupMenu
    Left = 952
    Top = 471
    object miMarcarSelecionado: TMenuItem
      Caption = 'Marcar/Desmarcar selecionado       ( Espa'#231'o )'
      OnClick = miMarcarSelecionadoClick
    end
    object miMarcarTodos: TMenuItem
      Caption = 'Marcar/Desmarcar todos        ( CTRL + Espa'#231'o )'
      OnClick = miMarcarTodosClick
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object miAlterarDadosColunaSelecionados: TMenuItem
      Caption = 'Alterar dados coluna selecionada'
      OnClick = miAlterarDadosColunaSelecionadosClick
    end
  end
end
