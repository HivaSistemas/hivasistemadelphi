inherited FormGruposEstoques: TFormGruposEstoques
  Caption = 'Grupo de estoques entre lojas'
  ClientHeight = 318
  ClientWidth = 823
  OnShow = FormShow
  ExplicitWidth = 829
  ExplicitHeight = 347
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 318
    ExplicitHeight = 318
    inherited sbExcluir: TSpeedButton
      Visible = False
    end
    inherited sbPesquisar: TSpeedButton
      Visible = False
    end
  end
  inline FrEmpresa: TFrEmpresas
    Left = 127
    Top = 7
    Width = 418
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 127
    ExplicitTop = 7
    ExplicitWidth = 418
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 393
      Height = 23
      ExplicitWidth = 393
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 418
      ExplicitWidth = 418
      inherited lbNomePesquisa: TLabel
        Width = 47
        Caption = 'Empresa'
        ExplicitWidth = 47
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 348
        ExplicitLeft = 348
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 393
      Height = 24
      ExplicitLeft = 393
      ExplicitHeight = 24
    end
  end
  inline FrEmpresasRetirar: TFrameAdicionarEmpresas
    Left = 365
    Top = 58
    Width = 218
    Height = 256
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 365
    ExplicitTop = 58
    inherited StaticTextLuka1: TStaticTextLuka
      Caption = 'A retirar'
    end
  end
  inline FrEmpresasEntregar: TFrameAdicionarEmpresas
    Left = 599
    Top = 58
    Width = 218
    Height = 256
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 599
    ExplicitTop = 58
    inherited StaticTextLuka1: TStaticTextLuka
      Caption = 'A entregar'
    end
  end
  inline FrEmpresasRetiraAto: TFrameAdicionarEmpresas
    Left = 127
    Top = 58
    Width = 218
    Height = 256
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 4
    TabStop = True
    ExplicitLeft = 127
    ExplicitTop = 58
    inherited StaticTextLuka1: TStaticTextLuka
      Caption = 'Retira no ato'
    end
  end
end
