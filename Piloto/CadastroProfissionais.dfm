inherited FormCadastroProfissionais: TFormCadastroProfissionais
  Caption = 'Cadastro de parceiros'
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    TabOrder = 8
  end
  inherited eNomeFantasia: TEditLuka
    TabOrder = 6
  end
  object ckVip: TCheckBoxLuka [14]
    Left = 756
    Top = 24
    Width = 46
    Height = 17
    Caption = 'Vip'
    TabOrder = 5
    Visible = False
    OnKeyDown = ProximoCampo
    CheckedStr = 'N'
  end
  inherited pcDados: TPageControl
    ActivePage = tsParametrosProfissional
    TabOrder = 7
    inherited tsPrincipais: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 670
      ExplicitHeight = 290
      inherited FrEndereco: TFrEndereco
        inherited FrBairro: TFrBairros
          inherited PnTitulos: TPanel
            inherited lbNomePesquisa: TLabel
              Height = 15
            end
          end
        end
      end
    end
    inherited tsEnderecos: TTabSheet
      inherited FrDiversosEnderecos: TFrDiversosEnderecos
        inherited FrBairro: TFrBairros
          inherited PnTitulos: TPanel
            inherited lbNomePesquisa: TLabel
              Height = 15
            end
            inherited pnSuprimir: TPanel
              inherited ckSuprimir: TCheckBox
                Visible = False
              end
            end
          end
        end
      end
    end
    inherited tsObservacoes: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 670
      ExplicitHeight = 290
    end
    object tsParametrosProfissional: TTabSheet
      Caption = 'Par'#226'metros do profissional'
      ImageIndex = 4
      object Label3: TLabel
        Left = 3
        Top = 68
        Width = 70
        Height = 14
        Caption = 'N'#250'mero Crea'
      end
      object Label4: TLabel
        Left = 3
        Top = 26
        Width = 104
        Height = 14
        Caption = '% Comiss'#227'o a vista'
      end
      object Label5: TLabel
        Left = 3
        Top = 157
        Width = 68
        Height = 14
        Caption = 'Informa'#231#245'es'
      end
      object Label6: TLabel
        Left = 142
        Top = 26
        Width = 108
        Height = 14
        Caption = '% Comiss'#227'o a prazo'
      end
      inline FrCargoProfissional: TFrCargosProfissionais
        Left = 3
        Top = 110
        Width = 320
        Height = 41
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 3
        ExplicitTop = 110
        ExplicitWidth = 320
        ExplicitHeight = 41
        inherited sgPesquisa: TGridLuka
          Width = 295
          Height = 24
          ExplicitWidth = 295
          ExplicitHeight = 24
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 30
            Caption = 'Cargo'
            ExplicitWidth = 30
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          Height = 25
          ExplicitLeft = 295
          ExplicitHeight = 25
        end
        inherited ckSomenteAtivos: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      object eNumeroCrea: TEditLuka
        Left = 3
        Top = 82
        Width = 256
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 30
        TabOrder = 2
        OnKeyDown = ProximoCampo
        TipoCampo = tcTexto
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object ckRegraGeralComissaoGrupos: TCheckBoxLuka
        Left = 3
        Top = 3
        Width = 150
        Height = 17
        Caption = 'Regra geral de comiss'#227'o'
        Checked = True
        State = cbChecked
        TabOrder = 0
        OnClick = ckRegraGeralComissaoGruposClick
        OnKeyDown = ProximoCampo
        CheckedStr = 'S'
      end
      object ePercentualComissao: TEditLuka
        Left = 3
        Top = 40
        Width = 129
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Enabled = False
        TabOrder = 1
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 2
        ValorMaximo = 999.990000000000000000
        NaoAceitarEspaco = False
      end
      object meInformacoes: TMemoAltis
        Left = 3
        Top = 171
        Width = 598
        Height = 116
        CharCase = ecUpperCase
        MaxLength = 400
        TabOrder = 4
      end
      object ePercentualComissaoPrazo: TEditLuka
        Left = 142
        Top = 40
        Width = 129
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Enabled = False
        TabOrder = 5
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 2
        ValorMaximo = 999.990000000000000000
        NaoAceitarEspaco = False
      end
    end
  end
  inherited eDataCadastro: TEditLukaData
    TabOrder = 11
  end
end
