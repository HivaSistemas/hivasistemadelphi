unit BuscarDadosCheques;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, EditTelefoneLuka, _Biblioteca,
  Vcl.Grids, GridLuka, Vcl.Mask, EditLukaData, _FrameHerancaPrincipal, System.Math,
  _FrameHenrancaPesquisas, FrameTiposCobranca, Vcl.StdCtrls, EditLuka, System.DateUtils,
  Vcl.Buttons, Vcl.ExtCtrls, _RecordsFinanceiros, _RecordsCadastros, _TiposCobrancaDiasPrazo,
  _Sessao;

type
  TFormBuscarDadosCheque = class(TFormHerancaFinalizar)
    eBanco: TEditLuka;
    lb1: TLabel;
    FrTiposCobranca: TFrTiposCobranca;
    eDataVencimento: TEditLukaData;
    lb2: TLabel;
    sgChequesLancados: TGridLuka;
    lb3: TLabel;
    eAgencia: TEditLuka;
    lb4: TLabel;
    eContaCorrente: TEditLuka;
    lb5: TLabel;
    eNumeroCheque: TEditLuka;
    lb6: TLabel;
    eValorCheque: TEditLuka;
    lb7: TLabel;
    eNomeEmitente: TEditLuka;
    eTelefoneEmitente: TEditTelefoneLuka;
    lb8: TLabel;
    stCondicaoPagamento: TStaticText;
    st3: TStaticText;
    stValorTotalASerPago: TStaticText;
    stValorTotal: TStaticText;
    stTotalDefinido: TStaticText;
    stValorTotalDefinido: TStaticText;
    st2: TStaticText;
    stDiferenca: TStaticText;
    lb9: TLabel;
    ePrazo: TEditLuka;
    eRepetir: TEditLuka;
    lb10: TLabel;
    procedure sgChequesLancadosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure eTelefoneEmitenteKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgChequesLancadosDblClick(Sender: TObject);
    procedure sgChequesLancadosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ePrazoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
  private
    FLinhaEditando: Integer;
    FSomenteLeitura: Boolean;
    FDiasPrazo: TArray<RecTipoCobrancaDiasPrazo>;

    procedure CalculaDiferencaPagamento;

    procedure FrTiposCobrancaOnAposPesquisar(Sender: TObject);
    procedure FrTiposCobrancaOnAposDeletar(Sender: TObject);
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function BuscarCheques(
  const pCondicaoId: Integer;
  const pNomeCondicao: string;
  const pValorABuscar: Double;
  const pCheques: TArray<RecTitulosFinanceiros>;
  const pSomenteLeitura: Boolean
): TRetornoTelaFinalizar< TArray<RecTitulosFinanceiros> >;

implementation

{$R *.dfm}

const
  co_data_vencimento      = 0;
  co_banco                = 1;
  co_agencia              = 2;
  co_conta_corrente       = 3;
  co_numero_cheque        = 4;
  co_valor_cheque         = 5;
  co_nome_emitente_cheque = 6;
  co_telefone_emitente    = 7;
  co_tipo_cobranca        = 8;
  co_parcela              = 9;
  co_qtde_parcelas        = 10;
  co_tipo_cobranca_id     = 11;
  coUtilizarLimiteCred    = 12;

function BuscarCheques(
  const pCondicaoId: Integer;
  const pNomeCondicao: string;
  const pValorABuscar: Double;
  const pCheques: TArray<RecTitulosFinanceiros>;
  const pSomenteLeitura: Boolean
): TRetornoTelaFinalizar< TArray<RecTitulosFinanceiros> >;
var
  i: SmallInt;
  vForm: TFormBuscarDadosCheque;
begin
  Result.Dados := nil;

  vForm := TFormBuscarDadosCheque.Create(Application);

  vForm.FrTiposCobranca.setFormaPagamento('CHQ');
  vForm.FrTiposCobranca.setCondicaoId(pCondicaoId);
  vForm.stValorTotalASerPago.Caption := NFormat(pValorABuscar);

  if pCondicaoId > 0 then
    vForm.stCondicaoPagamento.Caption := NFormat(pCondicaoId) + ' - ' + pNomeCondicao
  else if pNomeCondicao <> '' then
    vForm.stCondicaoPagamento.Caption := pNomeCondicao;

  for i := Low(pCheques) to High(pCheques) do begin
    vForm.sgChequesLancados.Cells[co_data_vencimento, i + 1]      := DateToStr(pCheques[i].DataVencimento);
    vForm.sgChequesLancados.Cells[co_banco, i + 1]                := pCheques[i].banco;
    vForm.sgChequesLancados.Cells[co_agencia, i + 1]              := pCheques[i].Agencia;
    vForm.sgChequesLancados.Cells[co_conta_corrente, i + 1]       := pCheques[i].ContaCorrente;
    vForm.sgChequesLancados.Cells[co_numero_cheque, i + 1]        := IntToStr(pCheques[i].NumeroCheque);
    vForm.sgChequesLancados.Cells[co_valor_cheque, i + 1]         := NFormat(pCheques[i].Valor);
    vForm.sgChequesLancados.Cells[co_nome_emitente_cheque, i + 1] := pCheques[i].NomeEmitente;
    vForm.sgChequesLancados.Cells[co_telefone_emitente, i + 1]    := pCheques[i].TelefoneEmitente;
    vForm.sgChequesLancados.Cells[co_tipo_cobranca_id, i + 1]     := IntToStr(pCheques[i].CobrancaId);
    vForm.sgChequesLancados.Cells[co_parcela, i + 1]              := IntToStr(pCheques[i].parcela);
    vForm.sgChequesLancados.Cells[co_qtde_parcelas, i + 1]        := IntToStr(pCheques[i].NumeroParcelas);
    vForm.sgChequesLancados.Cells[co_tipo_cobranca, i + 1]        := pCheques[i].Tipo;
    vForm.sgChequesLancados.Cells[coUtilizarLimiteCred, i + 1]    := pCheques[i].UtilizarLimiteCredCliente;
  end;
  vForm.sgChequesLancados.RowCount := IfThen(Length(pCheques) > 1, High(pCheques) + 2, 2);
  vForm.CalculaDiferencaPagamento;

  vForm.FSomenteLeitura := pSomenteLeitura;
  vForm.FrTiposCobranca.Modo(not pSomenteLeitura);
  _Biblioteca.SomenteLeitura([
    vForm.ePrazo,
    vForm.eRepetir,
    vForm.eBanco,
    vForm.eAgencia,
    vForm.eContaCorrente,
    vForm.eNumeroCheque,
    vForm.eValorCheque,
    vForm.eNomeEmitente,
    vForm.eTelefoneEmitente,
    vForm.eDataVencimento],
    pSomenteLeitura
  );

  if pSomenteLeitura then
    vForm.BorderIcons := [];

  if vForm.ShowModal = mrOk then begin
    SetLength(Result.Dados, vForm.sgChequesLancados.RowCount - 1);
    for i := 1 to vForm.sgChequesLancados.RowCount - 1 do begin
//      Result[i - 1].data_vencimento   := StrToDate(vForm.sgChequesLancados.Cells[co_data_vencimento, i]);
//      Result[i - 1].banco             := vForm.sgChequesLancados.Cells[co_banco, i];
//      Result[i - 1].agencia           := vForm.sgChequesLancados.Cells[co_agencia, i];
//      Result[i - 1].conta_corrente    := vForm.sgChequesLancados.Cells[co_conta_corrente, i];
//      Result[i - 1].numero_cheque     := StrToInt(vForm.sgChequesLancados.Cells[co_numero_cheque, i]);
//      Result[i - 1].valor_cheque      := SFormatDouble(vForm.sgChequesLancados.Cells[co_valor_cheque, i]);
//      Result[i - 1].nome_emitente     := vForm.sgChequesLancados.Cells[co_nome_emitente_cheque, i];
//      Result[i - 1].telefone_emitente := vForm.sgChequesLancados.Cells[co_telefone_emitente, i];
//      Result[i - 1].cobranca_id       := StrToInt(vForm.sgChequesLancados.Cells[co_tipo_cobranca_id, i]);
//      Result[i - 1].parcela           := StrToInt(vForm.sgChequesLancados.Cells[co_parcela, i]);
//      Result[i - 1].NumeroParcelas    := StrToInt(vForm.sgChequesLancados.Cells[co_qtde_parcelas, i]);
//      Result[i - 1].tipo_cobranca     := vForm.sgChequesLancados.Cells[co_tipo_cobranca, i];
//      Result[i - 1].UtilizarLimiteCredCliente := vForm.sgChequesLancados.Cells[coUtilizarLimiteCred, i];
    end;
  end;
  FreeAndNil(vForm);
end;

procedure TFormBuscarDadosCheque.CalculaDiferencaPagamento;
var
  i: Integer;
begin
  stValorTotalDefinido.Caption := '0,00';
  for i := 1 to sgChequesLancados.RowCount do
    stValorTotalDefinido.Caption := NFormat(SFormatDouble(stValorTotalDefinido.Caption) + SFormatDouble(sgChequesLancados.Cells[co_valor_cheque, i]));

  stDiferenca.Caption := NFormatN( SFormatDouble(stValorTotalASerPago.Caption) - SFormatDouble(stValorTotalDefinido.Caption) );
end;

procedure TFormBuscarDadosCheque.ePrazoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) or (Key = VK_TAB) then
    eRepetir.SetFocus;
end;

procedure TFormBuscarDadosCheque.eTelefoneEmitenteKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  i: Integer;
  vLinha: Integer;
  vRepeticao: Integer;

  vDataAtual: TDateTime;
  vDataVencimento: TDateTime;

  vValorProporcional: Currency;
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  vDataAtual := 0;
  if FDiasPrazo <> nil then begin
    vRepeticao := Length(FDiasPrazo);
    vDataAtual := Sessao.getDataHora;
    vDataVencimento := IncDay( vDataAtual, FDiasPrazo[0].Dias );
  end
  else begin
    vRepeticao := IfThen(eRepetir.AsInt = 0, 1, eRepetir.AsInt);
    vDataVencimento := eDataVencimento.AsData;
  end;

  if FLinhaEditando > 0 then
    vRepeticao := 1;

  vValorProporcional := Arredondar( eValorCheque.AsDouble / vRepeticao, 2 );

  for i := 1 to vRepeticao do begin
    if FLinhaEditando > 0 then
      vLinha := FLinhaEditando
    else if sgChequesLancados.Cells[co_data_vencimento, 1] = '' then
      vLinha := 1
    else begin
      vLinha := sgChequesLancados.RowCount - 1;
      Inc(vLinha);
    end;

    sgChequesLancados.Cells[co_data_vencimento, vLinha]      := DFormat(vDataVencimento);
    sgChequesLancados.Cells[co_banco, vLinha]                := eBanco.Text;
    sgChequesLancados.Cells[co_agencia, vLinha]              := eAgencia.Text;
    sgChequesLancados.Cells[co_conta_corrente, vLinha]       := eContaCorrente.Text;
    sgChequesLancados.Cells[co_numero_cheque, vLinha]        := _Biblioteca.NFormat(eNumeroCheque.AsInt + i - 1);
    sgChequesLancados.Cells[co_nome_emitente_cheque, vLinha] := eNomeEmitente.Text;
    sgChequesLancados.Cells[co_telefone_emitente, vLinha]    := eTelefoneEmitente.Text;
    sgChequesLancados.Cells[co_valor_cheque, vLinha]         := NFormat(vValorProporcional);
    sgChequesLancados.Cells[co_tipo_cobranca, vLinha]        := NFormat(FrTiposCobranca.GetTipoCobranca.CobrancaId) + ' - ' + FrTiposCobranca.GetTipoCobranca().Nome;
    sgChequesLancados.Cells[co_tipo_cobranca_id, vLinha]     := NFormat(FrTiposCobranca.GetTipoCobranca.CobrancaId);
    sgChequesLancados.Cells[coUtilizarLimiteCred, vLinha]    := FrTiposCobranca.GetTipoCobranca.UtilizarLimiteCredCliente;

    if FLinhaEditando = 0 then begin
      sgChequesLancados.Cells[co_parcela, vLinha]       := NFormat(i);
      sgChequesLancados.Cells[co_qtde_parcelas, vLinha] := NFormat(vRepeticao);
    end;

    if (FDiasPrazo <> nil) and (i <= High(FDiasPrazo)) then
      vDataVencimento := IncDay(vDataAtual, FDiasPrazo[i].Dias)
    else if ePrazo.AsInt > 0 then
      vDataVencimento := IncDay(vDataVencimento, ePrazo.AsInt);

    if FLinhaEditando = 0 then
      sgChequesLancados.RowCount := vLinha + 1;

    FLinhaEditando := 0;
  end;

  _Biblioteca.LimparCampos([
    FrTiposCobranca,
    eDataVencimento,
    eBanco,
    eAgencia,
    eContaCorrente,
    eNumeroCheque,
    eValorCheque,
    eNomeEmitente,
    eTelefoneEmitente,
    eRepetir,
    ePrazo]
  );

  SetarFoco(FrTiposCobranca);
  CalculaDiferencaPagamento;
end;

procedure TFormBuscarDadosCheque.FormCreate(Sender: TObject);
begin
  inherited;
  FLinhaEditando := 0;
  FrTiposCobranca.OnAposDeletar := FrTiposCobrancaOnAposDeletar;
  FrTiposCobranca.OnAposPesquisar := FrTiposCobrancaOnAposPesquisar;
end;

procedure TFormBuscarDadosCheque.FrTiposCobrancaOnAposDeletar(Sender: TObject);
begin
  LimparCampos([ePrazo, eRepetir]);
  FDiasPrazo := nil;
end;

procedure TFormBuscarDadosCheque.FrTiposCobrancaOnAposPesquisar(Sender: TObject);
begin
  FDiasPrazo := _TiposCobrancaDiasPrazo.BuscarTipoCobrancaDiasPrazos(Sessao.getConexaoBanco, 0, [FrTiposCobranca.GetTipoCobranca().CobrancaId]);
  Habilitar([ePrazo, eRepetir, eDataVencimento], FDiasPrazo = nil);
end;

procedure TFormBuscarDadosCheque.sgChequesLancadosDblClick(Sender: TObject);
begin
  inherited;
  if (sgChequesLancados.Cells[co_data_vencimento, sgChequesLancados.Row] = '') or FSomenteLeitura then
    Exit;

  FLinhaEditando := sgChequesLancados.Row;

  eDataVencimento.Text := sgChequesLancados.Cells[co_data_vencimento, FLinhaEditando];
  FrTiposCobranca.InserirDadoPorChave(StrToInt(sgChequesLancados.Cells[co_tipo_cobranca_id, FLinhaEditando]));
  eBanco.Text            := sgChequesLancados.Cells[co_banco, FLinhaEditando];
  eAgencia.Text          := sgChequesLancados.Cells[co_agencia, FLinhaEditando];
  eContaCorrente.Text    := sgChequesLancados.Cells[co_conta_corrente, FLinhaEditando];
  eNumeroCheque.Text     := sgChequesLancados.Cells[co_numero_cheque, FLinhaEditando];
  eValorCheque.Text      := sgChequesLancados.Cells[co_valor_cheque, FLinhaEditando];
  eNomeEmitente.Text     := sgChequesLancados.Cells[co_nome_emitente_cheque, FLinhaEditando];
  eTelefoneEmitente.Text := sgChequesLancados.Cells[co_telefone_emitente, FLinhaEditando];

  FrTiposCobranca.SetFocus;
end;

procedure TFormBuscarDadosCheque.sgChequesLancadosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  alinhamento: TAlignment;
begin
  inherited;

  if ACol in[co_banco, co_agencia, co_conta_corrente, co_numero_cheque, co_valor_cheque, co_parcela, co_qtde_parcelas] then
    alinhamento := taRightJustify
  else
    alinhamento := taLeftJustify;

  sgChequesLancados.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], alinhamento, Rect);
end;

procedure TFormBuscarDadosCheque.sgChequesLancadosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_DELETE) or FSomenteLeitura then begin
    sgChequesLancados.DeleteRow(sgChequesLancados.Row);
    CalculaDiferencaPagamento;
  end;
end;

procedure TFormBuscarDadosCheque.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if stDiferenca.Caption <> '' then begin
    _Biblioteca.Exclamar('Existe uma diferenša nos valores, por favor verifique!');
    Abort;
  end;
end;

end.
