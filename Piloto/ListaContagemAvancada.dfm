inherited FormListaContagemAvancada: TFormListaContagemAvancada
  Caption = 'Lista de contagem'
  ClientHeight = 440
  ClientWidth = 835
  ExplicitWidth = 841
  ExplicitHeight = 469
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 440
    ExplicitHeight = 440
    object sbIniciar: TSpeedButton [2]
      Left = 3
      Top = 181
      Width = 110
      Height = 40
      BiDiMode = bdLeftToRight
      Caption = 'Iniciar contagem'
      Enabled = False
      Flat = True
      NumGlyphs = 2
      ParentBiDiMode = False
      OnClick = sbIniciarClick
    end
    inherited ckOmitirFiltros: TCheckBoxLuka
      Top = 352
      ExplicitTop = 352
    end
  end
  inherited pcDados: TPageControl
    Width = 713
    Height = 440
    ExplicitWidth = 713
    ExplicitHeight = 440
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 705
      ExplicitHeight = 411
      inline FrInventario: TFrInventario
        Left = 4
        Top = 7
        Width = 637
        Height = 41
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 4
        ExplicitTop = 7
        ExplicitWidth = 637
        ExplicitHeight = 41
        inherited sgPesquisa: TGridLuka
          Width = 612
          Height = 24
          ExplicitWidth = 612
          ExplicitHeight = 24
        end
        inherited PnTitulos: TPanel
          Width = 637
          ExplicitWidth = 637
          inherited lbNomePesquisa: TLabel
            Width = 101
            ExplicitWidth = 101
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 532
            ExplicitLeft = 532
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 612
          Height = 25
          ExplicitLeft = 612
          ExplicitHeight = 25
        end
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 705
      ExplicitHeight = 411
      object sgItens: TGridLuka
        Left = 0
        Top = 0
        Width = 705
        Height = 411
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 6
        Ctl3D = True
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        GradientEndColor = 15395562
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goThumbTracking]
        ParentCtl3D = False
        TabOrder = 0
        OnDrawCell = sgItensDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Produto'
          'Nome'
          'Und.'
          'Marca'
          'Lote'
          'Local')
        OnGetCellColor = sgItensGetCellColor
        Grid3D = False
        RealColCount = 6
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          52
          320
          38
          127
          64
          64)
      end
    end
  end
  object frxReport: TfrxReport
    Version = '5.2.3'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44043.704685613400000000
    ReportOptions.LastChange = 44362.890600868100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 360
    Top = 184
    Datasets = <
      item
        DataSet = dmRelatorio.dstEmpresa
        DataSetName = 'frxdstEmpresa'
      end
      item
        DataSet = dstPagar
        DataSetName = 'frxdstPagar'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 90.708720000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          Top = 70.811070000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          Fill.BackColor = cl3DLight
        end
        object Memo1: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000000000
          Width = 68.031540000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 26.456710000000000000
          Top = 34.015770000000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 15.118120000000000000
          Top = 49.133890000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 340.157700000000000000
          Top = 3.779530000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 321.260050000000000000
          Top = 34.015770000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade/UF:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 340.157700000000000000
          Top = 49.133890000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 82.708720000000000000
          Top = 3.779530000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 383.512060000000000000
          Top = 3.779530000000000000
          Width = 200.315090000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."CNPJ"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 84.929190000000000000
          Top = 18.897650000000000000
          Width = 551.811380000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            
              '[frxdstEmpresa."LOGRADOURO"] [frxdstEmpresa."COMPLEMENTO"] n'#176'[fr' +
              'xdstEmpresa."NUMERO"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 82.708720000000000000
          Top = 34.015770000000000000
          Width = 207.874150000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_BAIRRO"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 383.512060000000000000
          Top = 34.015770000000000000
          Width = 185.196970000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_CIDADE"] / [frxdstEmpresa."ESTADO_ID"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 82.708720000000000000
          Top = 49.133890000000000000
          Width = 75.590600000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 383.512060000000000000
          Top = 49.133890000000000000
          Width = 260.787570000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."E_MAIL"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 545.252320000000000000
          Top = 3.779530000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Emitido em [Date] '#224's [Time]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 1.779530000000000000
          Top = 72.811070000000000000
          Width = 41.574830000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 46.913420000000000000
          Top = 72.811070000000000000
          Width = 192.756030000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 635.961040000000000000
          Top = 72.811070000000000000
          Width = 79.370130000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Qtde. contada')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 425.086890000000000000
          Top = 72.811070000000000000
          Width = 79.370130000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'C'#243'd. original')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 565.709030000000000000
          Top = 72.811070000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde f'#237'sico')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 545.252320000000000000
          Top = 18.897650000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pag. [Page] de [TotalPages#]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo32: TfrxMemoView
          Left = 396.850650000000000000
          Top = 72.811070000000000000
          Width = 26.456710000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Und.')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 245.448980000000000000
          Top = 72.811070000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'C'#243'd. barras')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 323.039580000000000000
          Top = 72.811070000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 506.457020000000000000
          Top = 72.811070000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Lote')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 16.832405710000000000
        Top = 170.078850000000000000
        Width = 718.110700000000000000
        DataSet = dstPagar
        DataSetName = 'frxdstPagar'
        RowCount = 0
        object frxdstTranscodigoProd: TfrxMemoView
          Left = 1.779530000000000000
          Top = 1.000000000000000000
          Width = 41.574803150000000000
          Height = 15.118120000000000000
          DataField = 'Produto'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstPagar."Produto"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo25: TfrxMemoView
          Left = 46.913420000000000000
          Top = 1.000000000000000000
          Width = 192.755905510000000000
          Height = 15.118120000000000000
          DataField = 'Nome'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstPagar."Nome"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 245.448980000000000000
          Top = 1.000000000000000000
          Width = 75.590551180000000000
          Height = 15.118120000000000000
          DataField = 'CodigoBarras'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstPagar."CodigoBarras"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 506.457020000000000000
          Top = 1.000000000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          DataField = 'Lote'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstPagar."Lote"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 425.086890000000000000
          Top = 1.000000000000000000
          Width = 79.370078740000000000
          Height = 15.118120000000000000
          DataField = 'CodigoOriginal'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstPagar."CodigoOriginal"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo15: TfrxMemoView
          Left = 323.039580000000000000
          Top = 1.000000000000000000
          Width = 71.811023620000000000
          Height = 15.118120000000000000
          DataField = 'Marca'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstPagar."Marca"]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 396.850650000000000000
          Top = 1.000000000000000000
          Width = 26.456692910000000000
          Height = 15.118120000000000000
          DataField = 'Unidade'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstPagar."Unidade"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 565.709069060000000000
          Top = 1.000000000000000000
          Width = 64.251970940000000000
          Height = 15.118120000000000000
          DataField = 'QuantidadeFisico'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstPagar."QuantidadeFisico"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 635.961040000000000000
          Top = 2.220470000000000000
          Width = 79.370078740000000000
          Height = 13.228348900000000000
          DataField = 'QuantidadeContada'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstPagar."QuantidadeContada"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 249.448980000000000000
        Width = 718.110700000000000000
        object Shape2: TfrxShapeView
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
      end
    end
  end
  object dstPagar: TfrxDBDataset
    UserName = 'frxdstPagar'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Produto=Produto'
      'Nome=Nome'
      'CodigoBarras=CodigoBarras'
      'Marca=Marca'
      'Unidade=Unidade'
      'CodigoOriginal=CodigoOriginal'
      'Lote=Lote'
      'QuantidadeFisico=QuantidadeFisico'
      'Agrupador=Agrupador'
      'QuantidadeContada=QuantidadeContada')
    DataSet = cdsPagar
    BCDToCurrency = False
    Left = 360
    Top = 248
  end
  object cdsPagar: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Produto'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Nome'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'CodigoBarras'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'Marca'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'Unidade'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CodigoOriginal'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'Lote'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'QuantidadeFisico'
        DataType = ftFloat
      end
      item
        Name = 'QuantidadeContada'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Agrupador'
        DataType = ftString
        Size = 200
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 360
    Top = 312
    object cdsProduto: TStringField
      FieldName = 'Produto'
    end
    object cdsNome: TStringField
      FieldName = 'Nome'
      Size = 200
    end
    object cdsCodigoBarras: TStringField
      FieldName = 'CodigoBarras'
      Size = 200
    end
    object cdsMarca: TStringField
      FieldName = 'Marca'
      Size = 200
    end
    object cdsUnidade: TStringField
      FieldName = 'Unidade'
    end
    object cdsCodigoOriginal: TStringField
      FieldName = 'CodigoOriginal'
      Size = 200
    end
    object cdsLote: TStringField
      FieldName = 'Lote'
      Size = 200
    end
    object cdsQuantidadeFisico: TFloatField
      FieldName = 'QuantidadeFisico'
    end
    object cdsAgrupador: TStringField
      FieldName = 'Agrupador'
      Size = 200
    end
    object cdsQuantidadeContada: TStringField
      FieldName = 'QuantidadeContada'
    end
  end
end
