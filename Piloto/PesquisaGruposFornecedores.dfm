inherited FormPesquisaGruposFornecedores: TFormPesquisaGruposFornecedores
  Caption = 'Pesquisa de grupos de fornecedores'
  ClientHeight = 215
  ClientWidth = 392
  ExplicitWidth = 400
  ExplicitHeight = 246
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 392
    Height = 169
    ColCount = 4
    OnDrawCell = sgPesquisaDrawCell
    CorLinhaFoco = 38619
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Descri'#231#227'o'
      'Ativo?')
    RealColCount = 4
    ExplicitWidth = 392
    ExplicitHeight = 169
    ColWidths = (
      28
      55
      217
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Text = '0'
  end
  inherited pnFiltro1: TPanel
    Width = 392
    ExplicitWidth = 392
    inherited lblPesquisa: TLabel
      Left = 201
      ExplicitLeft = 201
    end
    inherited lblOpcoesPesquisa: TLabel
      Left = 1
      ExplicitLeft = 1
    end
    inherited eValorPesquisa: TEditLuka
      Left = 201
      Top = 19
      Width = 189
      ExplicitLeft = 201
      ExplicitTop = 19
      ExplicitWidth = 189
    end
    inherited cbOpcoesPesquisa: TComboBoxLuka
      Left = 1
      Top = 19
      ExplicitLeft = 1
      ExplicitTop = 19
    end
  end
end
