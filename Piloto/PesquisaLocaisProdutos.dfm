inherited FormPesquisaLocaisProdutos: TFormPesquisaLocaisProdutos
  Caption = 'Pesquisa de locais de produtos'
  ClientHeight = 261
  ClientWidth = 504
  ExplicitWidth = 512
  ExplicitHeight = 292
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 504
    Height = 215
    ColCount = 5
    OnDrawCell = sgPesquisaDrawCell
    CorLinhaFoco = 38619
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Nome'
      'Tipo'
      'Ativo')
    RealColCount = 5
    ExplicitWidth = 504
    ExplicitHeight = 215
    ColWidths = (
      28
      55
      199
      122
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Top = 232
    Text = '0'
    ExplicitTop = 232
  end
  inherited pnFiltro1: TPanel
    Width = 504
    ExplicitWidth = 504
    inherited eValorPesquisa: TEditLuka
      Width = 296
      ExplicitWidth = 296
    end
  end
end
