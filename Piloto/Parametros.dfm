inherited FormParametros: TFormParametros
  Caption = 'Par'#226'metros Hiva'
  ClientHeight = 521
  ClientWidth = 738
  ExplicitWidth = 744
  ExplicitHeight = 550
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 521
    ExplicitHeight = 521
    inherited sbGravar: TSpeedButton
      Enabled = True
    end
    inherited sbDesfazer: TSpeedButton
      Left = -112
      Visible = False
      ExplicitLeft = -112
    end
    inherited sbExcluir: TSpeedButton
      Left = -112
      Visible = False
      ExplicitLeft = -112
    end
    inherited sbPesquisar: TSpeedButton
      Left = -112
      Enabled = False
      Visible = False
      ExplicitLeft = -112
    end
    object Label9: TLabel
      Left = 2
      Top = 466
      Width = 107
      Height = 14
      Alignment = taCenter
      Caption = 'Busca de par'#226'metros:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtBuscarMenu: TButtonedEdit
      Tag = 3
      Left = 2
      Top = 486
      Width = 112
      Height = 26
      ParentCustomHint = False
      TabStop = False
      Align = alCustom
      AutoSelect = False
      AutoSize = False
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      Color = clInfoBk
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      LeftButton.ImageIndex = 11
      ParentFont = False
      ParentShowHint = False
      RightButton.ImageIndex = 11
      RightButton.Visible = True
      ShowHint = True
      TabOrder = 0
      OnChange = edtBuscarMenuChange
      OnEnter = edtBuscarMenuEnter
      OnKeyDown = edtBuscarMenuKeyDown
    end
  end
  object pgMaster: TPageControl
    Left = 122
    Top = 0
    Width = 616
    Height = 521
    ActivePage = tsWeb
    Align = alClient
    TabOrder = 1
    object tsPrincipal: TTabSheet
      Caption = 'Principal'
      object Shape1: TShape
        Left = 0
        Top = 0
        Width = 608
        Height = 1
        Align = alTop
        ExplicitWidth = 850
      end
      object SpeedButton1: TSpeedButton
        Left = 282
        Top = 172
        Width = 14
        Height = 15
        Caption = '?'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = SpeedButton1Click
      end
      object SpeedButton2: TSpeedButton
        Left = 306
        Top = 228
        Width = 14
        Height = 15
        Caption = '?'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = SpeedButton2Click
      end
      object SpeedButton3: TSpeedButton
        Left = 398
        Top = 249
        Width = 14
        Height = 15
        Caption = '?'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = SpeedButton3Click
      end
      object Label2: TLabel
        Left = 165
        Top = 405
        Width = 138
        Height = 14
        Caption = 'Qtde. de vias recibo pgto.'
        FocusControl = eQtdeViasReciboPagamento
      end
      object lbllb5: TLabel
        Left = 14
        Top = 405
        Width = 129
        Height = 14
        Hint = 'Qtde.mens. obrigar leit.'
        Caption = 'Qtde.mens. obrigar leit.'
        FocusControl = eQtdeMensagensObrigarLeitura
      end
      object Label1: TLabel
        Left = 14
        Top = 443
        Width = 87
        Height = 14
        Caption = 'Qtd dias de dev.'
        FocusControl = eQuantidadeMaximaDiasDevolucao
      end
      object lb2: TLabel
        Left = 319
        Top = 445
        Width = 109
        Height = 14
        Caption = 'Qtde.seg.travar Hiva'
        FocusControl = eQtdeSegundosTravarAltisOcioso
      end
      object lb1: TLabel
        Left = 165
        Top = 445
        Width = 141
        Height = 14
        Caption = 'Qtde.dias validade senha'
        FocusControl = eQtdeDiasValidadeSenha
      end
      object SpeedButton4: TSpeedButton
        Left = 343
        Top = 269
        Width = 14
        Height = 15
        Caption = '?'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = SpeedButton4Click
      end
      object lb12: TLabel
        Left = 410
        Top = 278
        Width = 104
        Height = 14
        Hint = 'Inicio da utiliza'#231#227'o'
        Caption = 'Inicio da utiliza'#231#227'o'
        FocusControl = eDataInicioUtilizacao
      end
      object SpeedButton5: TSpeedButton
        Left = 391
        Top = 293
        Width = 14
        Height = 15
        Caption = '?'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = SpeedButton5Click
      end
      object Label8: TLabel
        Left = 319
        Top = 403
        Width = 150
        Height = 14
        Caption = 'Tipo comprovante de venda'
        FocusControl = cbxTipoComprovante
      end
      object SpeedButton6: TSpeedButton
        Left = 473
        Top = 402
        Width = 14
        Height = 15
        Caption = '?'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = SpeedButton6Click
      end
      object SpeedButton7: TSpeedButton
        Left = 455
        Top = 365
        Width = 14
        Height = 15
        Caption = '?'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = SpeedButton7Click
      end
      object SpeedButton8: TSpeedButton
        Left = 363
        Top = 382
        Width = 14
        Height = 15
        Caption = '?'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = SpeedButton8Click
      end
      object ckIniciarVendaConsumidorFinal: TCheckBox
        Left = 14
        Top = 9
        Width = 225
        Height = 17
        Caption = '1- Add. consumidor final na venda'
        Color = clWhite
        ParentColor = False
        TabOrder = 0
      end
      object ckUtilizarAnaliseCusto: TCheckBox
        Left = 14
        Top = 27
        Width = 172
        Height = 17
        Caption = '2 - Utiliza an'#225'lise de custo'
        Enabled = False
        TabOrder = 1
      end
      object ckBloquearCreditoPagarManual: TCheckBoxLuka
        Left = 14
        Top = 46
        Width = 225
        Height = 16
        Caption = '3 - Bloquear cr'#233'dito a pagar manual '
        TabOrder = 2
        CheckedStr = 'N'
      end
      object ckBloquearCreditoDevolucao: TCheckBoxLuka
        Left = 14
        Top = 64
        Width = 225
        Height = 17
        Caption = '4 - Bloquear cr'#233'dito de devolu'#231#227'o'
        TabOrder = 3
        CheckedStr = 'N'
      end
      object ckDefinirLocalProdutoManual: TCheckBoxLuka
        Left = 14
        Top = 83
        Width = 225
        Height = 17
        Caption = '5 - Definir local de produtos manual'
        TabOrder = 4
        OnClick = ckDefinirLocalProdutoManualClick
        CheckedStr = 'N'
      end
      object ckAplicarIndiceDecucao: TCheckBoxLuka
        Left = 14
        Top = 134
        Width = 225
        Height = 17
        Caption = '6 - Aplicar indice de dedu'#231#227'o'
        TabOrder = 5
        CheckedStr = 'N'
      end
      object ckImprimirListaSeparacaoNaVenda: TCheckBoxLuka
        Left = 14
        Top = 154
        Width = 287
        Height = 17
        Caption = '7 - Imprimir lista de separa'#231#227'o no ato da venda'
        TabOrder = 6
        CheckedStr = 'N'
      end
      object ckUtilizaDirecionamentoNotasPorLocais: TCheckBoxLuka
        Left = 14
        Top = 172
        Width = 269
        Height = 17
        Caption = '8 - Utiliza direcionamento de notas por locais'
        TabOrder = 7
        CheckedStr = 'N'
      end
      object ckEmitirNotaAcumuladoFechamentoPedido: TCheckBoxLuka
        Left = 14
        Top = 191
        Width = 325
        Height = 17
        Caption = '9 - Emitir nota de acumulado no fechamento do pedido'
        TabOrder = 8
        CheckedStr = 'N'
      end
      object ckUtilizaConfirmacaoTransferenciaLocais: TCheckBoxLuka
        Left = 14
        Top = 209
        Width = 325
        Height = 17
        Caption = '10 - Utiliza confirma'#231#227'o de transfer'#234'ncia de locais'
        TabOrder = 9
        CheckedStr = 'N'
      end
      object ckAtualizarPrecoVendaEntradaNota: TCheckBoxLuka
        Left = 14
        Top = 228
        Width = 293
        Height = 17
        Caption = '11 - Atualizar pre'#231'o de venda com entrada de nota'
        TabOrder = 10
        CheckedStr = 'N'
      end
      object ckDeletarNFPendenteEmissaoAoGerarDevolucaoVenda: TCheckBoxLuka
        Left = 14
        Top = 243
        Width = 384
        Height = 26
        Caption = '12 - Deletar NF pendente de emiss'#227'o ao gerar devolu'#231#227'o de venda'
        TabOrder = 11
        WordWrap = True
        CheckedStr = 'N'
      end
      object eQtdeSegundosTravarAltisOcioso: TEditLuka
        Left = 319
        Top = 458
        Width = 129
        Height = 22
        Hint = 'Qtde.seg.travar Hiva'
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 4
        TabOrder = 12
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eQtdeViasReciboPagamento: TEditLuka
        Left = 165
        Top = 419
        Width = 142
        Height = 22
        Hint = 'Qtde. de vias recibo pgto.'
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 19
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eQuantidadeMaximaDiasDevolucao: TEditLuka
        Left = 14
        Top = 458
        Width = 129
        Height = 22
        Hint = 'Quantidade dias de devolu'#231#227'o'
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 5
        TabOrder = 21
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eQtdeDiasValidadeSenha: TEditLuka
        Left = 165
        Top = 458
        Width = 142
        Height = 22
        Hint = 'Quantidade de dias validade senha'
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 3
        TabOrder = 22
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eQtdeMensagensObrigarLeitura: TEditLuka
        Left = 14
        Top = 419
        Width = 129
        Height = 22
        Hint = 
          'Quantidade de mensagens n'#227'o lidas para que seja obrigat'#243'rio a le' +
          'itura antes de se entrar em uma nova tela.'
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 3
        TabOrder = 18
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object ckPreencherLocaisAutomaticamente: TCheckBoxLuka
        Left = 45
        Top = 99
        Width = 258
        Height = 17
        Caption = '5.1 - Preencher locais automaticamente'
        TabOrder = 13
        OnClick = ckPreencherLocaisAutomaticamenteClick
        CheckedStr = 'N'
      end
      object ckAtualizarCustoProdutosAguardandoChegada: TCheckBoxLuka
        Left = 14
        Top = 268
        Width = 330
        Height = 17
        Caption = '13 - Atualizar custo de mercadorias aguardando chegada'
        TabOrder = 14
        CheckedStr = 'N'
      end
      object ckCalcularComissaoAcumuladoRecebimentoFinanceiro: TCheckBoxLuka
        Left = 14
        Top = 287
        Width = 371
        Height = 26
        Caption = 
          '14 - Calcular comiss'#227'o de venda no acumulado recebido com condi'#231 +
          #227'o de pagamento a prazo somente depois do recebimento'
        TabOrder = 15
        WordWrap = True
        CheckedStr = 'N'
      end
      object eDataInicioUtilizacao: TEditLukaData
        Left = 411
        Top = 291
        Width = 118
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        Color = 14019327
        EditMask = '99/99/9999;1;_'
        MaxLength = 10
        TabOrder = 16
        Text = '  /  /    '
      end
      object ckHabilitarDescontoItemImpresaoOrcamento: TCheckBoxLuka
        Left = 14
        Top = 316
        Width = 363
        Height = 17
        Caption = '15 - Habilitar desconto por item na impress'#227'o do or'#231'amento'
        TabOrder = 17
        CheckedStr = 'N'
      end
      object cbxTipoComprovante: TComboBox
        Left = 319
        Top = 419
        Width = 97
        Height = 22
        Hint = 'Tipo comprovante de venda'
        Style = csDropDownList
        ItemIndex = 0
        TabOrder = 20
        Text = 'Pagamento'
        Items.Strings = (
          'Pagamento'
          'Pedido')
      end
      object ckAjusteEstoqueReal: TCheckBoxLuka
        Left = 14
        Top = 333
        Width = 172
        Height = 17
        Caption = '16 - Ajuste de estoque real'
        TabOrder = 23
        CheckedStr = 'N'
      end
      object ckSenhaPedidosBloqueados: TCheckBoxLuka
        Left = 14
        Top = 350
        Width = 384
        Height = 17
        Caption = '17 - Solicitar senha para libera'#231#227'o de pedidos bloqueados'
        TabOrder = 24
        CheckedStr = 'N'
      end
      object ckCalcularComissaoAcumuladoFechamentoPedido: TCheckBoxLuka
        Left = 14
        Top = 366
        Width = 444
        Height = 19
        Caption = 
          '18 - Calcular comiss'#227'o de venda no acumulado no recebimento do o' +
          'r'#231'amento'
        TabOrder = 25
        WordWrap = True
        CheckedStr = 'N'
      end
      object ckSomenteLocaisEstoquePositivo: TCheckBoxLuka
        Left = 45
        Top = 115
        Width = 340
        Height = 17
        Caption = '5.2 - Exibir somente locais com estoque maior que zero'
        TabOrder = 26
        OnClick = ckSomenteLocaisEstoquePositivoClick
        CheckedStr = 'N'
      end
      object ckCalcularAdicionalComissaoPorMetaFuncionario: TCheckBoxLuka
        Left = 14
        Top = 383
        Width = 353
        Height = 19
        Caption = '19 - Calcular adicional de comiss'#227'o por meta por funcion'#225'rio'
        TabOrder = 27
        WordWrap = True
        CheckedStr = 'N'
      end
    end
    object tsModulos: TTabSheet
      Caption = 'Modulos'
      ImageIndex = 1
      object Shape2: TShape
        Left = 0
        Top = 0
        Width = 608
        Height = 1
        Align = alTop
        ExplicitTop = 8
        ExplicitWidth = 850
      end
      object ckHabilitarEntradaFutura: TCheckBoxLuka
        Left = 3
        Top = 7
        Width = 230
        Height = 17
        Caption = '1 - Habilitar Entrada Futura'
        TabOrder = 0
        CheckedStr = 'N'
      end
      object ckHabilitarVendaRapida: TCheckBoxLuka
        Left = 3
        Top = 31
        Width = 230
        Height = 17
        Caption = '2 - Habilitar Venda R'#225'pida'
        TabOrder = 1
        CheckedStr = 'N'
      end
      object ckHabilitarControleEntrega: TCheckBoxLuka
        Left = 3
        Top = 55
        Width = 230
        Height = 17
        Caption = '3 - Utiliza Controle de Entrega'
        TabOrder = 2
        CheckedStr = 'N'
      end
      object ckHabilitarMarketPlace: TCheckBoxLuka
        Left = 3
        Top = 79
        Width = 230
        Height = 17
        Caption = '4 - Utiliza MarketPlace (por local)'
        TabOrder = 3
        CheckedStr = 'N'
      end
      object ckHabilitarEnderecoEstoque: TCheckBoxLuka
        Left = 3
        Top = 103
        Width = 230
        Height = 17
        Caption = '5 - Utilizar endere'#231'o de estoque'
        TabOrder = 4
        CheckedStr = 'N'
      end
      object ckUtilizaEmissaoBoletos: TCheckBoxLuka
        Left = 3
        Top = 125
        Width = 230
        Height = 17
        Caption = '6 - Utilizar emiss'#227'o de boletos'
        TabOrder = 5
        CheckedStr = 'N'
      end
      object ckUtilizarDRE: TCheckBoxLuka
        Left = 3
        Top = 147
        Width = 230
        Height = 17
        Caption = '7 - Utilizar DRE'
        TabOrder = 6
        CheckedStr = 'N'
      end
      object ckOrcamentoAmbiente: TCheckBoxLuka
        Left = 3
        Top = 170
        Width = 172
        Height = 17
        Caption = '8 - Or'#231'amento por ambiente'
        TabOrder = 7
        CheckedStr = 'N'
      end
      object ckUtilizaControleManifesto: TCheckBoxLuka
        Left = 3
        Top = 193
        Width = 230
        Height = 17
        Caption = '9 - Utiliza Controle de Manifesto'
        TabOrder = 8
        CheckedStr = 'N'
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Outros parametros'
      ImageIndex = 2
      inline FrConsumidorFinal: TFrClientes
        Left = 8
        Top = 12
        Width = 281
        Height = 40
        Hint = 'Cadastro consumidor final'
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 8
        ExplicitTop = 12
        ExplicitWidth = 281
        ExplicitHeight = 40
        inherited CkAspas: TCheckBox [0]
        end
        inherited CkFiltroDuplo: TCheckBox [1]
        end
        inherited CkPesquisaNumerica: TCheckBox [2]
        end
        inherited CkMultiSelecao: TCheckBox [3]
        end
        inherited PnTitulos: TPanel [4]
          Width = 281
          ExplicitWidth = 281
          inherited lbNomePesquisa: TLabel
            Width = 142
            Caption = 'Cadastro consumidor final'
            ExplicitWidth = 142
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 176
            ExplicitLeft = 176
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel [5]
          Left = 256
          Height = 24
          ExplicitLeft = 256
          ExplicitHeight = 24
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
        inherited ckSomenteAtivos: TCheckBox [6]
        end
        inherited sgPesquisa: TGridLuka [7]
          Width = 273
          Height = 23
          Align = alNone
          ExplicitWidth = 273
          ExplicitHeight = 23
        end
        inherited poOpcoes: TPopupMenu
          Left = 232
        end
      end
      inline FrMotivoAjuEstBxCompraId: TFrMotivosAjusteEstoque
        Left = 8
        Top = 225
        Width = 279
        Height = 41
        Hint = 'Motivo de ajuste de estoque ( Baixa pedido de compra )'
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 8
        ExplicitTop = 225
        ExplicitWidth = 279
        ExplicitHeight = 41
        inherited CkAspas: TCheckBox [0]
        end
        inherited CkFiltroDuplo: TCheckBox [1]
        end
        inherited CkPesquisaNumerica: TCheckBox [2]
        end
        inherited CkMultiSelecao: TCheckBox [3]
        end
        inherited PnTitulos: TPanel [4]
          Width = 279
          ExplicitWidth = 279
          inherited lbNomePesquisa: TLabel
            Width = 304
            Caption = 'Motivo de ajuste de estoque ( Baixa pedido de compra )'
            ExplicitWidth = 304
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 174
            ExplicitLeft = 174
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel [5]
          Left = 254
          Height = 25
          ExplicitLeft = 254
          ExplicitHeight = 25
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
        inherited ckSomenteAtivos: TCheckBox [6]
        end
        inherited sgPesquisa: TGridLuka [7]
          Width = 273
          Height = 24
          Align = alNone
          ExplicitWidth = 273
          ExplicitHeight = 24
        end
      end
      inline FrTipoCobrancaGeracaoCredId: TFrTiposCobranca
        Left = 8
        Top = 66
        Width = 279
        Height = 41
        Hint = 'Cob. p/ gerar cr'#233'dito (troca e devolu'#231#227'o)'
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 8
        ExplicitTop = 66
        ExplicitWidth = 279
        ExplicitHeight = 41
        inherited CkAspas: TCheckBox [0]
        end
        inherited CkFiltroDuplo: TCheckBox [1]
        end
        inherited CkPesquisaNumerica: TCheckBox [2]
        end
        inherited CkMultiSelecao: TCheckBox [3]
        end
        inherited PnTitulos: TPanel [4]
          Width = 279
          ExplicitWidth = 279
          inherited lbNomePesquisa: TLabel
            Width = 217
            Caption = 'Cob. p/ gerar cr'#233'dito (troca e devolu'#231#227'o)'
            ExplicitWidth = 217
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 174
            ExplicitLeft = 174
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel [5]
          Left = 254
          Height = 25
          ExplicitLeft = 254
          ExplicitHeight = 25
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
        inherited ckSomenteAtivos: TCheckBox [6]
        end
        inherited sgPesquisa: TGridLuka [7]
          Width = 273
          Height = 24
          Align = alNone
          ExplicitWidth = 273
          ExplicitHeight = 24
        end
      end
      inline FrTipoCobrancaGerCredImpId: TFrTiposCobranca
        Left = 8
        Top = 117
        Width = 281
        Height = 41
        Hint = 'Cob. p/ gerar cr'#233'ditos de importa'#231#227'o'
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 8
        ExplicitTop = 117
        ExplicitWidth = 281
        ExplicitHeight = 41
        inherited CkAspas: TCheckBox [0]
        end
        inherited CkFiltroDuplo: TCheckBox [1]
        end
        inherited CkPesquisaNumerica: TCheckBox [2]
        end
        inherited CkMultiSelecao: TCheckBox [3]
        end
        inherited PnTitulos: TPanel [4]
          Width = 281
          ExplicitWidth = 281
          inherited lbNomePesquisa: TLabel
            Width = 198
            Caption = 'Cob. p/ gerar cr'#233'ditos de importa'#231#227'o'
            ExplicitWidth = 198
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 176
            ExplicitLeft = 176
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel [5]
          Left = 256
          Height = 25
          ExplicitLeft = 256
          ExplicitHeight = 25
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
        inherited ckSomenteAtivos: TCheckBox [6]
        end
        inherited sgPesquisa: TGridLuka [7]
          Width = 273
          Height = 24
          Align = alNone
          ExplicitWidth = 273
          ExplicitHeight = 24
        end
      end
      inline FrTipoCobrancaRecebimentoEntrega: TFrTiposCobranca
        Left = 8
        Top = 172
        Width = 277
        Height = 41
        Hint = 'Cob. p/ recebimento na entrega'
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 4
        TabStop = True
        ExplicitLeft = 8
        ExplicitTop = 172
        ExplicitWidth = 277
        ExplicitHeight = 41
        inherited CkAspas: TCheckBox [0]
        end
        inherited CkFiltroDuplo: TCheckBox [1]
        end
        inherited CkPesquisaNumerica: TCheckBox [2]
        end
        inherited CkMultiSelecao: TCheckBox [3]
        end
        inherited PnTitulos: TPanel [4]
          Width = 277
          ExplicitWidth = 277
          inherited lbNomePesquisa: TLabel
            Width = 172
            Caption = 'Cob. p/ recebimento na entrega'
            ExplicitWidth = 172
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 172
            ExplicitLeft = 172
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel [5]
          Left = 252
          Height = 25
          ExplicitLeft = 252
          ExplicitHeight = 25
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
        inherited ckSomenteAtivos: TCheckBox [6]
        end
        inherited sgPesquisa: TGridLuka [7]
          Width = 273
          Height = 24
          Align = alNone
          ExplicitWidth = 273
          ExplicitHeight = 24
        end
      end
      inline FrTipoCobAdiantamentoAcuId: TFrTiposCobranca
        Left = 317
        Top = 172
        Width = 275
        Height = 41
        Hint = 'Cob. p/ adiantamento acumulativo'
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 5
        TabStop = True
        ExplicitLeft = 317
        ExplicitTop = 172
        ExplicitWidth = 275
        ExplicitHeight = 41
        inherited CkAspas: TCheckBox [0]
        end
        inherited CkFiltroDuplo: TCheckBox [1]
        end
        inherited CkPesquisaNumerica: TCheckBox [2]
        end
        inherited CkMultiSelecao: TCheckBox [3]
        end
        inherited PnTitulos: TPanel [4]
          Width = 275
          ExplicitWidth = 275
          inherited lbNomePesquisa: TLabel
            Width = 188
            Caption = 'Cob. p/ adiantamento acumulativo'
            ExplicitWidth = 188
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 170
            ExplicitLeft = 170
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel [5]
          Left = 250
          Height = 25
          ExplicitLeft = 250
          ExplicitHeight = 25
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
        inherited ckSomenteAtivos: TCheckBox [6]
        end
        inherited sgPesquisa: TGridLuka [7]
          Width = 273
          Height = 24
          Align = alNone
          ExplicitWidth = 273
          ExplicitHeight = 24
        end
      end
      inline FrTipoCobrancaAcumulativoId: TFrTiposCobranca
        Left = 317
        Top = 65
        Width = 279
        Height = 41
        Hint = 'Cob. p/ acumulativo'
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 6
        TabStop = True
        ExplicitLeft = 317
        ExplicitTop = 65
        ExplicitWidth = 279
        ExplicitHeight = 41
        inherited CkAspas: TCheckBox [0]
        end
        inherited CkFiltroDuplo: TCheckBox [1]
        end
        inherited CkPesquisaNumerica: TCheckBox [2]
        end
        inherited CkMultiSelecao: TCheckBox [3]
        end
        inherited PnTitulos: TPanel [4]
          Width = 279
          ExplicitWidth = 279
          inherited lbNomePesquisa: TLabel
            Width = 107
            Caption = 'Cob. p/ acumulativo'
            ExplicitWidth = 107
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Left = 120
            ExplicitLeft = 120
          end
          inherited pnSuprimir: TPanel
            Left = 174
            ExplicitLeft = 174
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel [5]
          Left = 254
          Height = 25
          ExplicitLeft = 254
          ExplicitHeight = 25
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
        inherited ckSomenteAtivos: TCheckBox [6]
        end
        inherited sgPesquisa: TGridLuka [7]
          Width = 273
          Height = 24
          Align = alNone
          ExplicitWidth = 273
          ExplicitHeight = 24
        end
      end
      inline FrTipoCobAdiantamentoFinId: TFrTiposCobranca
        Left = 317
        Top = 115
        Width = 277
        Height = 41
        Hint = 'Cob. p/ adiantamento de financeiros'
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 7
        TabStop = True
        ExplicitLeft = 317
        ExplicitTop = 115
        ExplicitWidth = 277
        ExplicitHeight = 41
        inherited CkAspas: TCheckBox [0]
        end
        inherited CkFiltroDuplo: TCheckBox [1]
        end
        inherited CkPesquisaNumerica: TCheckBox [2]
        end
        inherited CkMultiSelecao: TCheckBox [3]
        end
        inherited PnTitulos: TPanel [4]
          Width = 277
          ExplicitWidth = 277
          inherited lbNomePesquisa: TLabel
            Width = 199
            Caption = 'Cob. p/ adiantamento de financeiros'
            ExplicitWidth = 199
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 172
            ExplicitLeft = 172
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel [5]
          Left = 252
          Height = 25
          ExplicitLeft = 252
          ExplicitHeight = 25
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
        inherited ckSomenteAtivos: TCheckBox [6]
        end
        inherited sgPesquisa: TGridLuka [7]
          Width = 273
          Height = 24
          Align = alNone
          ExplicitWidth = 273
          ExplicitHeight = 24
        end
      end
      inline FrTipoCobFechamentoTurnoId: TFrTiposCobranca
        Left = 317
        Top = 12
        Width = 277
        Height = 41
        Hint = 'Cob. p/ gerar diferen'#231'a fechamento de caixa'
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 8
        TabStop = True
        ExplicitLeft = 317
        ExplicitTop = 12
        ExplicitWidth = 277
        ExplicitHeight = 41
        inherited CkAspas: TCheckBox [0]
        end
        inherited CkFiltroDuplo: TCheckBox [1]
        end
        inherited CkPesquisaNumerica: TCheckBox [2]
        end
        inherited CkMultiSelecao: TCheckBox [3]
        end
        inherited PnTitulos: TPanel [4]
          Width = 277
          ExplicitWidth = 277
          inherited lbNomePesquisa: TLabel
            Width = 240
            Caption = 'Cob. p/ gerar diferen'#231'a fechamento de caixa'
            ExplicitWidth = 240
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Left = 99
            ExplicitLeft = 99
          end
          inherited pnSuprimir: TPanel
            Left = 172
            ExplicitLeft = 172
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel [5]
          Left = 252
          Height = 25
          ExplicitLeft = 252
          ExplicitHeight = 25
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
        inherited ckSomenteAtivos: TCheckBox [6]
        end
        inherited sgPesquisa: TGridLuka [7]
          Left = 4
          Width = 273
          Height = 24
          Align = alNone
          ExplicitLeft = 4
          ExplicitWidth = 273
          ExplicitHeight = 24
        end
      end
      inline frTipoCobEntradaNfe: TFrTiposCobranca
        Left = 317
        Top = 225
        Width = 275
        Height = 41
        Hint = 'Cob. p/ entrada de notas fiscais'
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 9
        TabStop = True
        ExplicitLeft = 317
        ExplicitTop = 225
        ExplicitWidth = 275
        ExplicitHeight = 41
        inherited CkAspas: TCheckBox [0]
        end
        inherited CkFiltroDuplo: TCheckBox [1]
        end
        inherited CkPesquisaNumerica: TCheckBox [2]
        end
        inherited CkMultiSelecao: TCheckBox [3]
        end
        inherited PnTitulos: TPanel [4]
          Width = 275
          ExplicitWidth = 275
          inherited lbNomePesquisa: TLabel
            Width = 172
            Caption = 'Cob. p/ entrada de notas fiscais'
            ExplicitWidth = 172
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 170
            ExplicitLeft = 170
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel [5]
          Left = 250
          Height = 25
          ExplicitLeft = 250
          ExplicitHeight = 25
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
        inherited ckSomenteAtivos: TCheckBox [6]
        end
        inherited sgPesquisa: TGridLuka [7]
          Width = 273
          Height = 24
          Align = alNone
          ExplicitWidth = 273
          ExplicitHeight = 24
        end
      end
      object GroupBox1: TGroupBox
        Left = -1
        Top = 272
        Width = 586
        Height = 177
        Caption = 'Configura'#231#227'o SMTP'
        TabOrder = 10
        object Label3: TLabel
          Left = 14
          Top = 20
          Width = 44
          Height = 14
          Caption = 'Servidor'
        end
        object Label4: TLabel
          Left = 14
          Top = 63
          Width = 43
          Height = 14
          Caption = 'Usu'#225'rio'
        end
        object Label5: TLabel
          Left = 14
          Top = 104
          Width = 34
          Height = 14
          Caption = 'Senha'
        end
        object Label6: TLabel
          Left = 261
          Top = 20
          Width = 28
          Height = 14
          Caption = 'Porta'
        end
        object Label7: TLabel
          Left = 321
          Top = 63
          Width = 97
          Height = 14
          Caption = 'Tipo autentica'#231#227'o'
        end
        object edtSmtpServidor: TEditLuka
          Left = 14
          Top = 37
          Width = 241
          Height = 22
          Hint = 'Servidor'
          CharCase = ecLowerCase
          MaxLength = 40
          TabOrder = 0
          TipoCampo = tcTexto
          ApenasPositivo = False
          AsInt = 0
          CasasDecimais = 0
          NaoAceitarEspaco = False
        end
        object edtSmtpUsuario: TEditLuka
          Left = 14
          Top = 79
          Width = 290
          Height = 22
          Hint = 'Usu'#225'rio'
          CharCase = ecLowerCase
          MaxLength = 40
          TabOrder = 2
          TipoCampo = tcTexto
          ApenasPositivo = False
          AsInt = 0
          CasasDecimais = 0
          NaoAceitarEspaco = False
        end
        object edtSmtpPorta: TEditLuka
          Left = 261
          Top = 37
          Width = 43
          Height = 22
          Hint = 'Porta'
          Alignment = taRightJustify
          CharCase = ecUpperCase
          MaxLength = 5
          TabOrder = 1
          TipoCampo = tcNumerico
          ApenasPositivo = False
          AsInt = 0
          CasasDecimais = 0
          NaoAceitarEspaco = False
        end
        object cbxSmtpTipoAutenticacao: TComboBox
          Left = 318
          Top = 79
          Width = 145
          Height = 22
          Hint = 'Tipo autentica'#231#227'o'
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 5
          Text = 'Nenhuma'
          Items.Strings = (
            'Nenhuma'
            'SSL/TLS')
        end
        object edtSmtpSenha: TEdit
          Left = 14
          Top = 122
          Width = 290
          Height = 22
          Hint = 'Senha'
          PasswordChar = '*'
          TabOrder = 3
          Text = 'edtSmtpSenha'
        end
        object ckSmtRequerAutenticacao: TCheckBoxLuka
          Left = 321
          Top = 32
          Width = 230
          Height = 17
          Caption = 'Requer autentica'#231'ao'
          TabOrder = 4
          CheckedStr = 'N'
        end
      end
    end
    object tsWeb: TTabSheet
      Caption = 'Integra'#231#227'o Web'
      ImageIndex = 3
      ExplicitLeft = 36
      ExplicitTop = 22
      object Label10: TLabel
        Left = 2
        Top = 5
        Width = 68
        Height = 14
        Hint = 'Qtde.mens. obrigar leit.'
        Caption = 'URL API Web'
        FocusControl = edtUrlApi
      end
      object Label11: TLabel
        Left = 2
        Top = 49
        Width = 153
        Height = 14
        Caption = 'Data hora '#250'ltima integra'#231#227'o'
      end
      object Label12: TLabel
        Left = 179
        Top = 49
        Width = 178
        Height = 14
        Caption = 'Intervelo integra'#231#227'o em minutos'
        FocusControl = edtIntervaloIntegracao
      end
      object Label13: TLabel
        Left = 358
        Top = 5
        Width = 89
        Height = 14
        Caption = 'Porta integra'#231#227'o'
        FocusControl = edtPortaIntegracaoWeb
      end
      object edtUrlApi: TEditLuka
        Left = 2
        Top = 19
        Width = 350
        Height = 22
        CharCase = ecLowerCase
        MaxLength = 200
        TabOrder = 0
        TipoCampo = tcTexto
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object edtDataUltimaIntegracaoWeb: TEditLukaData
        Left = 2
        Top = 63
        Width = 153
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999 99:99:99;1;_'
        MaxLength = 19
        TabOrder = 2
        Text = '  /  /       :  :  '
        TipoData = tdDataHora
      end
      object edtIntervaloIntegracao: TEditLuka
        Left = 179
        Top = 63
        Width = 142
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 3
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object edtPortaIntegracaoWeb: TEditLuka
        Left = 358
        Top = 19
        Width = 142
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 1
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
    end
  end
  object lbParametros: TListBox
    Left = 4
    Top = 341
    Width = 112
    Height = 121
    Anchors = [akLeft, akBottom]
    ItemHeight = 14
    TabOrder = 2
    Visible = False
    OnDblClick = lbParametrosDblClick
    OnKeyDown = lbParametrosKeyDown
  end
end
