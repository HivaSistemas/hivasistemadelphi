inherited FormOpcoesOrcamentoGrafico: TFormOpcoesOrcamentoGrafico
  Caption = 'Op'#231#245'es de impress'#227'o or'#231'amento gr'#225'fico'
  ClientHeight = 129
  ClientWidth = 609
  OnKeyPress = FormKeyPress
  ExplicitWidth = 615
  ExplicitHeight = 158
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 92
    Width = 609
    ExplicitTop = 92
    ExplicitWidth = 615
    inherited sbFinalizar: TSpeedButton
      Left = 241
      Top = 4
      ExplicitLeft = 241
      ExplicitTop = 4
    end
  end
  object rgExibirPrecoUnitario: TRadioGroupLuka
    Left = 8
    Top = 8
    Width = 201
    Height = 74
    Caption = 'Exibir pre'#231'o unit'#225'rio do produto?'
    ItemIndex = 0
    Items.Strings = (
      'Sim'
      'N'#227'o')
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    Valores.Strings = (
      'S'
      'N')
  end
  object rgExibirPrecoTotal: TRadioGroupLuka
    Left = 217
    Top = 8
    Width = 186
    Height = 74
    Caption = 'Exibir pre'#231'o total do produto?'
    ItemIndex = 0
    Items.Strings = (
      'Sim'
      'N'#227'o')
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    Valores.Strings = (
      'S'
      'N')
  end
  object rgExibirDescontoProduto: TRadioGroupLuka
    Left = 414
    Top = 8
    Width = 179
    Height = 74
    Caption = 'Exibir desconto do produto?'
    ItemIndex = 0
    Items.Strings = (
      'Sim'
      'N'#227'o')
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    Valores.Strings = (
      'S'
      'N')
  end
end
