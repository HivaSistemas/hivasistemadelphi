unit Parametros;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastro, Vcl.StdCtrls, _Parametros, _Sessao,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _FrameHerancaPrincipal, _Biblioteca, _RecordsEspeciais,
  _FrameHenrancaPesquisas, FrameClientes, _RecordsCadastros,
  Frame.MotivosAjusteEstoque, FrameTiposCobranca, CheckBoxLuka, Vcl.ComCtrls,
  Vcl.Mask, EditLukaData;

type
  TFormParametros = class(TFormHerancaCadastro)
    pgMaster: TPageControl;
    tsPrincipal: TTabSheet;
    lbllb5: TLabel;
    lb1: TLabel;
    lb2: TLabel;
    Label1: TLabel;
    FrConsumidorFinal: TFrClientes;
    ckIniciarVendaConsumidorFinal: TCheckBox;
    ckUtilizarAnaliseCusto: TCheckBox;
    eQtdeMensagensObrigarLeitura: TEditLuka;
    eQtdeDiasValidadeSenha: TEditLuka;
    FrMotivoAjuEstBxCompraId: TFrMotivosAjusteEstoque;
    FrTipoCobrancaGeracaoCredId: TFrTiposCobranca;
    FrTipoCobrancaGerCredImpId: TFrTiposCobranca;
    FrTipoCobrancaRecebimentoEntrega: TFrTiposCobranca;
    FrTipoCobrancaAcumulativoId: TFrTiposCobranca;
    ckBloquearCreditoPagarManual: TCheckBoxLuka;
    ckBloquearCreditoDevolucao: TCheckBoxLuka;
    FrTipoCobAdiantamentoAcuId: TFrTiposCobranca;
    eQtdeSegundosTravarAltisOcioso: TEditLuka;
    eQuantidadeMaximaDiasDevolucao: TEditLuka;
    FrTipoCobAdiantamentoFinId: TFrTiposCobranca;
    FrTipoCobFechamentoTurnoId: TFrTiposCobranca;
    ckDefinirLocalProdutoManual: TCheckBoxLuka;
    ckAplicarIndiceDecucao: TCheckBoxLuka;
    ckEmitirNotaAcumuladoFechamentoPedido: TCheckBoxLuka;
    tsModulos: TTabSheet;
    Shape1: TShape;
    Shape2: TShape;
    ckHabilitarEntradaFutura: TCheckBoxLuka;
    ckHabilitarVendaRapida: TCheckBoxLuka;
    ckHabilitarControleEntrega: TCheckBoxLuka;
    ckImprimirListaSeparacaoNaVenda: TCheckBoxLuka;
    frTipoCobEntradaNfe: TFrTiposCobranca;
    ckHabilitarMarketPlace: TCheckBoxLuka;
    ckUtilizaDirecionamentoNotasPorLocais: TCheckBoxLuka;
    SpeedButton1: TSpeedButton;
    Label2: TLabel;
    eQtdeViasReciboPagamento: TEditLuka;
    ckHabilitarEnderecoEstoque: TCheckBoxLuka;
    ckUtilizaConfirmacaoTransferenciaLocais: TCheckBoxLuka;
    ckAtualizarPrecoVendaEntradaNota: TCheckBoxLuka;
    SpeedButton2: TSpeedButton;
    ckDeletarNFPendenteEmissaoAoGerarDevolucaoVenda: TCheckBoxLuka;
    SpeedButton3: TSpeedButton;
    TabSheet1: TTabSheet;
    ckPreencherLocaisAutomaticamente: TCheckBoxLuka;
    GroupBox1: TGroupBox;
    edtSmtpServidor: TEditLuka;
    Label3: TLabel;
    Label4: TLabel;
    edtSmtpUsuario: TEditLuka;
    Label5: TLabel;
    Label6: TLabel;
    edtSmtpPorta: TEditLuka;
    cbxSmtpTipoAutenticacao: TComboBox;
    Label7: TLabel;
    edtSmtpSenha: TEdit;
    ckSmtRequerAutenticacao: TCheckBoxLuka;
    ckAtualizarCustoProdutosAguardandoChegada: TCheckBoxLuka;
    SpeedButton4: TSpeedButton;
    ckCalcularComissaoAcumuladoRecebimentoFinanceiro: TCheckBoxLuka;
    eDataInicioUtilizacao: TEditLukaData;
    lb12: TLabel;
    SpeedButton5: TSpeedButton;
    ckHabilitarDescontoItemImpresaoOrcamento: TCheckBoxLuka;
    cbxTipoComprovante: TComboBox;
    Label8: TLabel;
    SpeedButton6: TSpeedButton;
    ckAjusteEstoqueReal: TCheckBoxLuka;
    ckUtilizaEmissaoBoletos: TCheckBoxLuka;
    ckUtilizarDRE: TCheckBoxLuka;
    ckOrcamentoAmbiente: TCheckBoxLuka;
    ckSenhaPedidosBloqueados: TCheckBoxLuka;
    ckCalcularComissaoAcumuladoFechamentoPedido: TCheckBoxLuka;
    SpeedButton7: TSpeedButton;
    ckSomenteLocaisEstoquePositivo: TCheckBoxLuka;
    ckCalcularAdicionalComissaoPorMetaFuncionario: TCheckBoxLuka;
    SpeedButton8: TSpeedButton;
    Label9: TLabel;
    edtBuscarMenu: TButtonedEdit;
    lbParametros: TListBox;
    ckUtilizaControleManifesto: TCheckBoxLuka;
    tsWeb: TTabSheet;
    edtUrlApi: TEditLuka;
    Label10: TLabel;
    edtDataUltimaIntegracaoWeb: TEditLukaData;
    Label11: TLabel;
    edtIntervaloIntegracao: TEditLuka;
    Label12: TLabel;
    Label13: TLabel;
    edtPortaIntegracaoWeb: TEditLuka;
    procedure FormCreate(Sender: TObject);
    procedure sbGravarClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure ckDefinirLocalProdutoManualClick(Sender: TObject);
    procedure ckPreencherLocaisAutomaticamenteClick(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure ckSomenteLocaisEstoquePositivoClick(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure edtBuscarMenuChange(Sender: TObject);
    procedure edtBuscarMenuEnter(Sender: TObject);
    procedure edtBuscarMenuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure lbParametrosDblClick(Sender: TObject);
    procedure lbParametrosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    procedure VerificarRegistro; reintroduce;
    procedure FindComponentsByHint(const SearchText: string; ComponentList: TStrings);
    procedure FindComponentsByHintRecursive(ParentControl: TWinControl; const SearchText: string; ComponentList: TStrings);
    procedure FocusComponentByHintRecursive(ParentControl: TWinControl; const SearchText: string);
  end;

implementation

{$R *.dfm}

procedure TFormParametros.ckDefinirLocalProdutoManualClick(Sender: TObject);
begin
  inherited;
  if not ckDefinirLocalProdutoManual.Checked then begin
    ckPreencherLocaisAutomaticamente.Checked := False;
    ckSomenteLocaisEstoquePositivo.Checked := False;
  end;
end;

procedure TFormParametros.ckPreencherLocaisAutomaticamenteClick(
  Sender: TObject);
begin
  inherited;
  if (ckPreencherLocaisAutomaticamente.Checked) and (not ckDefinirLocalProdutoManual.Checked) then begin
    Informar('Para ativar "Preencher locais automaticamente" � necess�rio que o par�metro "Definir local de produtos manual" esteja ativo!');
    ckPreencherLocaisAutomaticamente.Checked := False;
  end;
end;

procedure TFormParametros.ckSomenteLocaisEstoquePositivoClick(Sender: TObject);
begin
  inherited;
  if (ckSomenteLocaisEstoquePositivo.Checked) and (not ckDefinirLocalProdutoManual.Checked) then begin
    Informar('Para ativar "Exibir somente locais com estoque maior que zero" � necess�rio que o par�metro "Definir local de produtos manual" esteja ativo!');
    ckSomenteLocaisEstoquePositivo.Checked := False;
  end;
end;

procedure TFormParametros.edtBuscarMenuChange(Sender: TObject);
var
  ComponentsFound: TStringList;
begin
  if (edtBuscarMenu.Text = '') then begin
    lbParametros.Visible := False;
    Exit;
  end;

  if Length(edtBuscarMenu.Text) < 3 then
    Exit;

  ComponentsFound := TStringList.Create;
  try
    // Busca todos os componentes com o Hint correspondente
    FindComponentsByHint(edtBuscarMenu.Text, ComponentsFound);

    lbParametros.Items.Clear;
    // Adiciona os componentes encontrados ao ListBox
    lbParametros.Items.Assign(ComponentsFound);

    lbParametros.Visible := True;
    lbParametros.BringToFront;
  finally
    ComponentsFound.Free;
  end;
end;

procedure TFormParametros.edtBuscarMenuEnter(Sender: TObject);
begin
  inherited;
  edtBuscarMenu.Clear;
  lbParametros.Clear;
end;

procedure TFormParametros.edtBuscarMenuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) or (Key = VK_DOWN) then begin
    if (lbParametros.Count > 0) then begin
      lbParametros.SetFocus;
      lbParametros.ItemIndex := 0;
    end;
  end
  else if (Key = VK_ESCAPE) then begin
    edtBuscarMenu.Clear;
    lbParametros.Visible := False;
  end;
end;

procedure TFormParametros.FormCreate(Sender: TObject);
begin
  (* Ao inserir um novo frame que seja de tipo de cobran�a e tambem seja para controle de alguma parte financeira *)
  (* verificar a necessidade de se add este nova coluna na fun��o TipoCobrancaFazParteControleInternoHiva do objeto Sessao *)
  (* Ezequil E. 25/08/2018 18:40 *)

  FrConsumidorFinal.InserirDadoPorChave(Sessao.getParametros.cadastro_consumidor_final_id, False);
  ckIniciarVendaConsumidorFinal.Checked := (Sessao.getParametros.iniciar_venda_consumidor_final = 'S');
  ckUtilizarAnaliseCusto.Checked        := (Sessao.getParametros.UtilizarAnaliseCusto = 'S');
  eQtdeMensagensObrigarLeitura.AsInt    := Sessao.getParametros.QtdeMensagensObrigarLeitura;
  eQtdeDiasValidadeSenha.AsInt          := Sessao.getParametros.QuantidadeDiasValidadeSenha;
  eQtdeSegundosTravarAltisOcioso.AsInt  := Sessao.getParametros.QtdeSegTravarAltisOcioso;
  eQtdeViasReciboPagamento.AsInt        := Sessao.getParametros.QuantidadeViasReciboPagamento;
  eQuantidadeMaximaDiasDevolucao.AsInt  := Sessao.getParametros.QtdeMaximaDiasDevolucao;

  FrMotivoAjuEstBxCompraId.InserirDadoPorChave( Sessao.getParametros.MotivoAjuEstBxCompraId, False );
  FrTipoCobrancaGeracaoCredId.InserirDadoPorChave( Sessao.getParametros.TipoCobrancaGeracaoCredId, False );
  FrTipoCobrancaGerCredImpId.InserirDadoPorChave( Sessao.getParametros.TipoCobrancaGerCredImpId, False );
  FrTipoCobrancaRecebimentoEntrega.InserirDadoPorChave( Sessao.getParametros.TipoCobRecebEntregaId, False );
  FrTipoCobrancaAcumulativoId.InserirDadoPorChave( Sessao.getParametros.TipoCobAcumulativoId, False );
  FrTipoCobAdiantamentoAcuId.InserirDadoPorChave( Sessao.getParametros.TipoCobAdiantamentoAcuId, False );
  FrTipoCobAdiantamentoFinId.InserirDadoPorChave( Sessao.getParametros.TipoCobAdiantamentoFinId, False );
  FrTipoCobFechamentoTurnoId.InserirDadoPorChave( Sessao.getParametros.TipoCobFechamentoTurnoId, False );
  FrTipoCobEntradaNfe.InserirDadoPorChave( Sessao.getParametros.TipoCobEntradaNfeXmlId, False );

  ckBloquearCreditoDevolucao.CheckedStr := Sessao.getParametros.BloquearCreditoDevolucao;
  ckBloquearCreditoPagarManual.CheckedStr := Sessao.getParametros.BloquearCreditoPagarManual;
  ckDefinirLocalProdutoManual.CheckedStr := Sessao.getParametros.DefinirLocalManual;
  ckPreencherLocaisAutomaticamente.CheckedStr := Sessao.getParametros.PreencherLocaisAutomaticamente;
  ckSomenteLocaisEstoquePositivo.CheckedStr := Sessao.getParametros.SomenteLocaisEstoquePositivo;
  ckAplicarIndiceDecucao.CheckedStr := Sessao.getParametros.AplicarIndiceDeducao;
  ckImprimirListaSeparacaoNaVenda.CheckedStr := Sessao.getParametros.ImprimirListaSeparacaoVenda;
  ckUtilizaDirecionamentoNotasPorLocais.CheckedStr := Sessao.getParametros.UtilizaDirecionamentoPorLocais;
  ckEmitirNotaAcumuladoFechamentoPedido.CheckedStr := Sessao.getParametros.EmitirNotaAcumuladoFechamentoPedido;
  ckHabilitarEntradaFutura.CheckedStr := Sessao.getParametros.UtilModuloPreEntradas;
  ckHabilitarVendaRapida.CheckedStr := Sessao.getParametros.UtilVendaRapida;
  ckHabilitarMarketPlace.CheckedStr := Sessao.getParametros.UtilMarketPlace;
  ckHabilitarEnderecoEstoque.CheckedStr := Sessao.getParametros.UtilEnderecoEstoque;
  ckHabilitarControleEntrega.CheckedStr := Sessao.getParametros.UtilControleEntrega;
  ckUtilizaConfirmacaoTransferenciaLocais.CheckedStr := Sessao.getParametros.UtilConfirmacaoTransferenciaLocais;
  ckAtualizarPrecoVendaEntradaNota.CheckedStr := Sessao.getParametros.AtualizarPrecoVendaEntradaNota;
  ckDeletarNFPendenteEmissaoAoGerarDevolucaoVenda.CheckedStr := Sessao.getParametros.DeletarNFPendenteEmissao;
  ckAtualizarCustoProdutosAguardandoChegada.CheckedStr := Sessao.getParametros.AtualizarCustoProdutosAguardandoChegada;
  ckCalcularComissaoAcumuladoRecebimentoFinanceiro.CheckedStr := Sessao.getParametros.CalcularComissaoAcumuladoRecFin;
  ckHabilitarDescontoItemImpresaoOrcamento.CheckedStr := Sessao.getParametros.HabilitarDescontoItemImpresaoOrcamento;
  ckAjusteEstoqueReal.CheckedStr := Sessao.getParametros.AjusteEstoqueReal;
  ckSenhaPedidosBloqueados.CheckedStr := Sessao.getParametros.SenhaPedidosBloqueados;
  ckCalcularComissaoAcumuladoFechamentoPedido.CheckedStr := Sessao.getParametros.CalcularComissaoAcumuladoRecPedido;
  ckCalcularAdicionalComissaoPorMetaFuncionario.CheckedStr := Sessao.getParametros.CalcularAdicionalComissaoPorMetaFuncionario;
  ckOrcamentoAmbiente.CheckedStr := Sessao.getParametros.OrcamentoAmbiente;
  eDataInicioUtilizacao.AsData := Sessao.getParametros.CalcularComissaoAcumuladoRecFinData;
  cbxTipoComprovante.ItemIndex := cbxTipoComprovante.Items.IndexOf(Sessao.getParametros.TipoComprovante);
  ckUtilizaControleManifesto.CheckedStr := Sessao.getParametros.UtilizaControleManifesto;

  edtSmtpServidor.Text    := Sessao.getParametros.SmtpServidor;
  edtSmtpUsuario.Text     := Sessao.getParametros.SmtpUsuario;
  edtSmtpSenha.Text       := Sessao.getParametros.SmtpSenha;
  edtSmtpPorta.AsInt      := Sessao.getParametros.SmtpPorta;
  cbxSmtpTipoAutenticacao.ItemIndex := cbxSmtpTipoAutenticacao.Items.IndexOf(Sessao.getParametros.SmtpAutenticacao);
  ckSmtRequerAutenticacao.CheckedStr := Sessao.getParametros.SmtpReqAutenticacao;
  ckUtilizaEmissaoBoletos.CheckedStr := Sessao.getParametros.UtilizaEmissaoBoleto;
  ckUtilizarDRE.CheckedStr := Sessao.getParametros.UtilizaDRE;
  edtUrlApi.Text           := Sessao.getParametros.UrlIntegracaoWeb;
  edtDataUltimaIntegracaoWeb.AsDataHora := Sessao.getParametros.DataUltimaIntegracaoWeb;
  edtIntervaloIntegracao.AsInt := Sessao.getParametros.IntervalorIntegracaoWeb;
  edtPortaIntegracaoWeb.AsInt := Sessao.getParametros.PortaIntegracaoWeb;

  pgMaster.ActivePage := tsPrincipal;

  lbParametros.Top := 341;
  lbParametros.Height := 121;
  lbParametros.Width := 593;
end;

procedure TFormParametros.FocusComponentByHintRecursive(ParentControl: TWinControl; const SearchText: string);
var
  i: Integer;
  ChildControl: TControl;
begin
  for i := 0 to ParentControl.ControlCount - 1 do
  begin
    ChildControl := ParentControl.Controls[i];

    // Se for checkbox, busca pelo Caption
    if (ChildControl is TCheckBox) and (TCheckBox(ChildControl).Caption = SearchText) then
    begin
      // Se o componente est� em uma aba, ativa a aba correta
      if (ChildControl.Parent is TTabSheet) and (ChildControl.Parent.Parent is TPageControl) then
      begin
        TPageControl(ChildControl.Parent.Parent).ActivePage := TTabSheet(ChildControl.Parent);
      end;

      SetarFoco(TWinControl(ChildControl));
      lbParametros.Visible := False;
      Exit;
    end
    else if (ChildControl is TCheckBoxLuka) and (TCheckBoxLuka(ChildControl).Caption = SearchText) then
    begin
      // Se o componente est� em uma aba, ativa a aba correta
      if (ChildControl.Parent is TTabSheet) and (ChildControl.Parent.Parent is TPageControl) then
      begin
        TPageControl(ChildControl.Parent.Parent).ActivePage := TTabSheet(ChildControl.Parent);
      end;

      SetarFoco(TWinControl(ChildControl));
      lbParametros.Visible := False;
      Exit;
    end
    // Caso contr�rio, busca pelo Hint
    else if (ChildControl.Hint = SearchText) then
    begin
      // Se o componente est� em uma aba, ativa a aba correta
      if (ChildControl.Parent is TTabSheet) and (ChildControl.Parent.Parent is TPageControl) then
      begin
        TPageControl(ChildControl.Parent.Parent).ActivePage := TTabSheet(ChildControl.Parent);
      end;

      SetarFoco(TWinControl(ChildControl));
      lbParametros.Visible := False;
      Exit;
    end;

    if ChildControl is TWinControl then
      FocusComponentByHintRecursive(TWinControl(ChildControl), SearchText);
  end;
end;

procedure TFormParametros.lbParametrosDblClick(Sender: TObject);
var
  i: Integer;
  SelectedText: string;
  Component: TControl;
begin
  // Obt�m o texto selecionado no ListBox (pode ser Hint ou Caption)
  SelectedText := lbParametros.Items[lbParametros.ItemIndex];

  // Busca o componente correspondente ao texto selecionado
  for i := 0 to Self.ControlCount - 1 do
  begin
    Component := Self.Controls[i];

    // Se o componente for um checkbox, compara pelo Caption
    if (Component is TCheckBox) and (TCheckBox(Component).Caption = SelectedText) then
    begin
      // Se o componente est� em uma aba, ativa a aba correspondente
      if (Component.Parent is TTabSheet) and (Component.Parent.Parent is TPageControl) then
      begin
        TPageControl(Component.Parent.Parent).ActivePage := TTabSheet(Component.Parent);
      end;

      SetarFoco(TWinControl(Component));
      lbParametros.Visible := False;
      Exit;  // Sai ap�s focar no primeiro componente encontrado
    end
    else if (Component is TCheckBoxLuka) and (TCheckBoxLuka(Component).Caption = SelectedText) then
    begin
      // Se o componente est� em uma aba, ativa a aba correspondente
      if (Component.Parent is TTabSheet) and (Component.Parent.Parent is TPageControl) then
      begin
        TPageControl(Component.Parent.Parent).ActivePage := TTabSheet(Component.Parent);
      end;

      SetarFoco(TWinControl(Component));
      lbParametros.Visible := False;
      Exit;  // Sai ap�s focar no primeiro componente encontrado
    end
    // Se n�o for checkbox, compara pelo Hint
    else if (Component.Hint = SelectedText) then
    begin
      // Se o componente est� em uma aba, ativa a aba correspondente
      if (Component.Parent is TTabSheet) and (Component.Parent.Parent is TPageControl) then
      begin
        TPageControl(Component.Parent.Parent).ActivePage := TTabSheet(Component.Parent);
      end;

      SetarFoco(TWinControl(Component));
      lbParametros.Visible := False;
      Exit;  // Sai ap�s focar no primeiro componente encontrado
    end;

    // Busca recursiva para encontrar o componente
    if Component is TWinControl then
      FocusComponentByHintRecursive(TWinControl(Component), SelectedText);
  end;
end;

procedure TFormParametros.lbParametrosKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) and (lbParametros.Items.Count > 0) then
    lbParametrosDblClick(Sender);
end;

procedure TFormParametros.FindComponentsByHint(const SearchText: string; ComponentList: TStrings);
var
  i: Integer;
  Component: TControl;
begin
  for i := 0 to Self.ControlCount - 1 do
  begin
    Component := Self.Controls[i];

    // Se o componente for um checkbox, busca pelo Caption
    if (Component is TCheckBox) and (Pos(LowerCase(SearchText), LowerCase(TCheckBox(Component).Caption)) > 0) then
    begin
      ComponentList.Add(TCheckBox(Component).Caption); // Adiciona o Caption do checkbox
    end
    else if (Component is TCheckBoxLuka) and (Pos(LowerCase(SearchText), LowerCase(TCheckBoxLuka(Component).Caption)) > 0) then
    begin
      ComponentList.Add(TCheckBoxLuka(Component).Caption); // Adiciona o Caption do checkbox
    end
    // Se n�o for checkbox, busca pelo Hint normalmente
    else if (Component.Hint <> '') and (Pos(LowerCase(SearchText), LowerCase(Component.Hint)) > 0) then
    begin
      ComponentList.Add(Component.Hint); // Adiciona o Hint dos outros componentes
    end;

    // Se o componente for um container, fa�a busca recursiva
    if Component is TWinControl then
      FindComponentsByHintRecursive(TWinControl(Component), SearchText, ComponentList);
  end;
end;

procedure TFormParametros.FindComponentsByHintRecursive(ParentControl: TWinControl; const SearchText: string; ComponentList: TStrings);
var
  i: Integer;
  ChildControl: TControl;
begin
  for i := 0 to ParentControl.ControlCount - 1 do
  begin
    ChildControl := ParentControl.Controls[i];

    // Se o componente for um checkbox, busca pelo Caption
    if (ChildControl is TCheckBox) and (Pos(LowerCase(SearchText), LowerCase(TCheckBox(ChildControl).Caption)) > 0) then
    begin
      ComponentList.Add(TCheckBox(ChildControl).Caption); // Adiciona o Caption do checkbox
    end
    // Se n�o for checkbox, busca pelo Hint normalmente
    else if (ChildControl.Hint <> '') and (Pos(LowerCase(SearchText), LowerCase(ChildControl.Hint)) > 0) then
    begin
      ComponentList.Add(ChildControl.Hint); // Adiciona o Hint dos outros componentes
    end;

    // Busca recursiva em containers filhos
    if ChildControl is TWinControl then
      FindComponentsByHintRecursive(TWinControl(ChildControl), SearchText, ComponentList);
  end;
end;

procedure TFormParametros.sbGravarClick(Sender: TObject);
var
  vMotivoAjuEstBxCompraId: Integer;
  vRetBanco: RecRetornoBD;
begin
  VerificarRegistro;

  vMotivoAjuEstBxCompraId := 0;
  if not FrMotivoAjuEstBxCompraId.EstaVazio then
    vMotivoAjuEstBxCompraId := FrMotivoAjuEstBxCompraId.GetMotivoAjusteEstoque().motivo_ajuste_id;

  if not ckDefinirLocalProdutoManual.Checked then begin
    ckPreencherLocaisAutomaticamente.Checked := False;
    ckSomenteLocaisEstoquePositivo.Checked := False;
  end;

  vRetBanco :=
    _Parametros.AtualizarParametros(
      Sessao.getConexaoBanco,
      FrConsumidorFinal.getCliente().RecCadastro.cadastro_id,
      ToChar(ckIniciarVendaConsumidorFinal),
      ToChar(ckUtilizarAnaliseCusto),
      eQtdeMensagensObrigarLeitura.AsInt,
      eQtdeDiasValidadeSenha.AsInt,
      vMotivoAjuEstBxCompraId,
      FrTipoCobrancaGeracaoCredId.GetTipoCobranca().CobrancaId,
      FrTipoCobrancaGerCredImpId.GetTipoCobranca().CobrancaId,
      FrTipoCobrancaRecebimentoEntrega.GetTipoCobranca().CobrancaId,
      FrTipoCobrancaAcumulativoId.GetTipoCobranca().CobrancaId,
      FrTipoCobAdiantamentoAcuId.GetTipoCobranca().CobrancaId,
      FrTipoCobAdiantamentoFinId.GetTipoCobranca().CobrancaId,
      FrTipoCobFechamentoTurnoId.GetTipoCobranca().CobrancaId,
      ckBloquearCreditoDevolucao.CheckedStr,
      ckBloquearCreditoPagarManual.CheckedStr,
      eQtdeSegundosTravarAltisOcioso.AsInt,
      eQuantidadeMaximaDiasDevolucao.AsInt,
      ckDefinirLocalProdutoManual.CheckedStr,
      ckPreencherLocaisAutomaticamente.CheckedStr,
      ckSomenteLocaisEstoquePositivo.CheckedStr,
      ckAplicarIndiceDecucao.CheckedStr,
      ckHabilitarEntradaFutura.CheckedStr,
      ckHabilitarVendaRapida.CheckedStr,
      ckHabilitarMarketPlace.CheckedStr,
      ckhabilitarEnderecoEstoque.CheckedStr,
      ckHabilitarControleEntrega.CheckedStr,
      ckImprimirListaSeparacaoNaVenda.CheckedStr,
      frTipoCobEntradaNfe.GetTipoCobranca().CobrancaId,
      ckUtilizaDirecionamentoNotasPorLocais.CheckedStr,
      eQtdeViasReciboPagamento.AsInt,
      ckEmitirNotaAcumuladoFechamentoPedido.CheckedStr,
      ckUtilizaConfirmacaoTransferenciaLocais.CheckedStr,
      ckAtualizarPrecoVendaEntradaNota.CheckedStr,
      ckDeletarNFPendenteEmissaoAoGerarDevolucaoVenda.CheckedStr,
      edtSmtpServidor.Text,
      edtSmtpUsuario.Text,
      edtSmtpSenha.Text,
      edtSmtpPorta.AsInt,
      cbxSmtpTipoAutenticacao.Text,
      ckSmtRequerAutenticacao.CheckedStr,
      ckAtualizarCustoProdutosAguardandoChegada.CheckedStr,
      ckCalcularComissaoAcumuladoRecebimentoFinanceiro.CheckedStr,
      eDataInicioUtilizacao.AsData,
      ckHabilitarDescontoItemImpresaoOrcamento.CheckedStr,
      ckAjusteEstoqueReal.CheckedStr,
      ckSenhaPedidosBloqueados.CheckedStr,
      ckOrcamentoAmbiente.CheckedStr,
      cbxTipoComprovante.Text,
      ckUtilizaEmissaoBoletos.CheckedStr,
      ckUtilizarDRE.CheckedStr,
      ckCalcularComissaoAcumuladoFechamentoPedido.CheckedStr,
      ckCalcularAdicionalComissaoPorMetaFuncionario.CheckedStr,
      ckUtilizaControleManifesto.CheckedStr,
      edtUrlApi.Text,
      edtDataUltimaIntegracaoWeb.AsDataHora,
      edtIntervaloIntegracao.AsInt,
      edtPortaIntegracaoWeb.AsInt
      );

  if vRetBanco.TeveErro then begin
    Exclamar(vRetBanco.MensagemErro);
    Exit;
  end
  else begin
    Informar('Par�metros atualizados com sucesso');

    vRetBanco := Sessao.AtualizarParametros;
    if vRetBanco.TeveErro then
      Exclamar('Falha ao atualizar os par�metros da sess�o, feche o sistema e abra novamente!');
  end;
end;

procedure TFormParametros.SpeedButton1Click(Sender: TObject);
begin
  inherited;
  Informar('Ao marcar essa op��o � necess�rio definir as empresas para direcionamento de notas no cadastro de locais');
end;

procedure TFormParametros.SpeedButton2Click(Sender: TObject);
begin
  inherited;
  Informar('Ao marcar essa op��o se o pre�o de tabela da entrada for maior que o pre�o de tabela anterior ser� recalculado o valor de venda mantendo o mesmo lucro usado anteriormente.');
end;

procedure TFormParametros.SpeedButton3Click(Sender: TObject);
begin
  inherited;
  Informar('Ao gerar devolu��o de venda que cont�m uma nota fiscal n�o emitida o sistema ir� deletar a nota fiscal.');
end;

procedure TFormParametros.SpeedButton4Click(Sender: TObject);
begin
  inherited;
  Informar('Este par�metro far� com que as entradas com o status "Aguardando chegada de mercadoria" atualizem os custos dos produtos.');
end;

procedure TFormParametros.SpeedButton5Click(Sender: TObject);
begin
  inherited;
  Informar(
    'Ao fechar um acumulado utilizando uma condi��o de pagamento a prazo somente ir� ser gerado a comiss�o ' +
    'quando for efetuado o pagamento do financeiro gerado.' +
    'Obs: Somente ser� aplicado a comiss�es de acumulados fechados posteriormente a data de inicio da utiliza��o.'
  );
end;

procedure TFormParametros.SpeedButton6Click(Sender: TObject);
begin
  inherited;
  Informar(
    'Definir qual o layout vai ser gerado como comprovante de venda ao receber os pedidos de vendas.'
  );
end;

procedure TFormParametros.SpeedButton7Click(Sender: TObject);
begin
  inherited;
  Informar(
    'Ao selecionar essa op��o ir� sobrepor o par�metro 14 e o c�lculo da comiss�o de pedidos ' +
    'recebidos com condi��o de pagamento "acumulativo" ser� feito no fechamento do pedido'
  );
end;

procedure TFormParametros.SpeedButton8Click(Sender: TObject);
begin
  inherited;
  Informar('');
end;

procedure TFormParametros.VerificarRegistro;
begin
  if FrConsumidorFinal.EstaVazio then begin
    Exclamar('O consumidor final deve ser informado, verifique!');
    SetarFoco(FrConsumidorFinal);
    Abort;
  end;

  if not eQtdeDiasValidadeSenha.AsInt in[30..180] then begin
    Exclamar('A quantidade de dias da validade da senha tem que estar entre 30 e 180 dias!');
    SetarFoco(eQtdeDiasValidadeSenha);
    Abort;
  end;

  if FrTipoCobrancaGeracaoCredId.EstaVazio then begin
    Exclamar('� necess�rio informar o tipo de cobran�a para gera��o de cr�dito!');
    SetarFoco(FrTipoCobrancaGeracaoCredId);
    Abort;
  end;

  if FrTipoCobrancaGeracaoCredId.GetTipoCobranca.forma_pagamento <> 'COB' then begin
    Exclamar('O tipo de cobran�a para gera��o do cr�dito s� pode ser do tipo "Cobran�a", fa�a a valida��o na tela de cadastro de tipos de cobran�a!');
    SetarFoco(FrTipoCobrancaGeracaoCredId);
    Abort;
  end;

  if FrTipoCobrancaGerCredImpId.EstaVazio then begin
    Exclamar('� necess�rio informar o tipo de cobran�a para gera��o de cr�dito de importa��o!');
    SetarFoco(FrTipoCobrancaGerCredImpId);
    Abort;
  end;

  if FrTipoCobrancaGerCredImpId.GetTipoCobranca.forma_pagamento <> 'COB' then begin
    Exclamar('O tipo de cobran�a para gera��o do cr�dito de importa��o s� pode ser do tipo "Cobran�a", fa�a a valida��o na tela de cadastro de tipos de cobran�a!');
    SetarFoco(FrTipoCobrancaGerCredImpId);
    Abort;
  end;

  if FrTipoCobrancaRecebimentoEntrega.EstaVazio then begin
    Exclamar('� necess�rio informar o tipo de cobran�a para recebimento na entrega!');
    SetarFoco(FrTipoCobrancaRecebimentoEntrega);
    Abort;
  end;

  if FrTipoCobrancaRecebimentoEntrega.GetTipoCobranca.forma_pagamento <> 'COB' then begin
    Exclamar('O tipo de cobran�a para recebimento na entrega s� pode ser do tipo "Cobran�a", fa�a a valida��o na tela de cadastro de tipos de cobran�a!');
    SetarFoco(FrTipoCobrancaRecebimentoEntrega);
    Abort;
  end;

  if FrTipoCobrancaAcumulativoId.EstaVazio then begin
    Exclamar('� necess�rio informar o tipo de cobran�a para acumulados!');
    SetarFoco(FrTipoCobrancaAcumulativoId);
    Abort;
  end;

  if FrTipoCobrancaAcumulativoId.GetTipoCobranca.forma_pagamento <> 'ACU' then begin
    Exclamar('O tipo de cobran�a para acumulados s� pode ser do tipo "Acumulativo", fa�a a valida��o na tela de cadastro de tipos de cobran�a!');
    SetarFoco(FrTipoCobrancaAcumulativoId);
    Abort;
  end;

  if FrTipoCobAdiantamentoAcuId.EstaVazio then begin
    Exclamar('� necess�rio informar o tipo de cobran�a para adiantamento de acumulados!');
    SetarFoco(FrTipoCobAdiantamentoAcuId);
    Abort;
  end;

  if FrTipoCobAdiantamentoAcuId.GetTipoCobranca.forma_pagamento <> 'COB' then begin
    Exclamar('O tipo de cobran�a para adiantamento de acumulados s� pode ser do tipo "Cobran�a", fa�a a valida��o na tela de cadastro de tipos de cobran�a!');
    SetarFoco(FrTipoCobAdiantamentoAcuId);
    Abort;
  end;

  if FrTipoCobAdiantamentoFinId.EstaVazio then begin
    Exclamar('� necess�rio informar o tipo de cobran�a para adiantamento de financeiros!');
    SetarFoco(FrTipoCobAdiantamentoFinId);
    Abort;
  end;

  if FrTipoCobAdiantamentoFinId.GetTipoCobranca.forma_pagamento <> 'COB' then begin
    Exclamar('O tipo de cobran�a para adiantamento de financeiros s� pode ser do tipo "Cobran�a", fa�a a valida��o na tela de cadastro de tipos de cobran�a!');
    SetarFoco(FrTipoCobAdiantamentoFinId);
    Abort;
  end;

  if FrTipoCobFechamentoTurnoId.EstaVazio then begin
    Exclamar('� necess�rio informar o tipo de cobran�a para fechamento de turno com diferen�a!');
    SetarFoco(FrTipoCobFechamentoTurnoId);
    Abort;
  end;

  if FrTipoCobFechamentoTurnoId.GetTipoCobranca.forma_pagamento <> 'COB' then begin
    Exclamar('O tipo de cobran�a para fechamento de turno com diferen�a s� pode ser do tipo "Cobran�a", fa�a a valida��o na tela de cadastro de tipos de cobran�a!');
    SetarFoco(FrTipoCobFechamentoTurnoId);
    Abort;
  end;

  if (ckCalcularComissaoAcumuladoRecebimentoFinanceiro.Checked) and (not eDataInicioUtilizacao.DataOk)then begin
    Exclamar('Ao marcar o par�metro "14" � necess�rio definir a data de inicio da utiliza��o');
    SetarFoco(eDataInicioUtilizacao);
    Abort;
  end;

end;

end.
