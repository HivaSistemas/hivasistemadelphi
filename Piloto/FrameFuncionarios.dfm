inherited FrFuncionarios: TFrFuncionarios
  Height = 97
  ExplicitHeight = 97
  inherited sgPesquisa: TGridLuka
    Height = 80
    ExplicitHeight = 80
  end
  inherited CkAspas: TCheckBox
    Top = 17
    ExplicitTop = 17
  end
  inherited CkFiltroDuplo: TCheckBox
    Top = 33
    ExplicitTop = 33
  end
  inherited CkPesquisaNumerica: TCheckBox
    Top = 49
    ExplicitTop = 49
  end
  inherited CkMultiSelecao: TCheckBox
    Left = 120
    Top = 17
    ExplicitLeft = 120
    ExplicitTop = 17
  end
  inherited PnTitulos: TPanel
    inherited lbNomePesquisa: TLabel
      Width = 60
      Caption = 'Funcion'#225'rios'
      ExplicitWidth = 60
    end
  end
  inherited pnPesquisa: TPanel
    Height = 81
    TabOrder = 8
    ExplicitHeight = 81
    DesignSize = (
      25
      81)
  end
  inherited ckSomenteAtivos: TCheckBox
    Left = 216
    Top = 17
    TabOrder = 9
    ExplicitLeft = 216
    ExplicitTop = 17
  end
  object ckBuscarSomenteAtivos: TCheckBox [8]
    Left = 120
    Top = 33
    Width = 97
    Height = 17
    Caption = 'Ativos'
    Checked = True
    State = cbChecked
    TabOrder = 6
    Visible = False
  end
  object ckBuscarTodasEmpresas: TCheckBox [9]
    Left = 120
    Top = 49
    Width = 97
    Height = 17
    Caption = 'Todas empresas '
    TabOrder = 7
    Visible = False
  end
  object ckVendedor: TCheckBox [10]
    Left = 8
    Top = 64
    Width = 121
    Height = 17
    Caption = 'Vendedor'
    TabOrder = 10
    Visible = False
  end
  object ckMotorista: TCheckBox [11]
    Left = 8
    Top = 79
    Width = 121
    Height = 17
    Caption = 'Motorista'
    TabOrder = 11
    Visible = False
  end
  object ckComprador: TCheckBox [12]
    Left = 120
    Top = 64
    Width = 121
    Height = 17
    Caption = 'Comprador'
    TabOrder = 12
    Visible = False
  end
  object ckCaixa: TCheckBox [13]
    Left = 120
    Top = 79
    Width = 121
    Height = 17
    Caption = 'Caixa'
    TabOrder = 13
    Visible = False
  end
  object ckAjudante: TCheckBox [14]
    Left = 216
    Top = 79
    Width = 97
    Height = 17
    Caption = 'Ajudante'
    TabOrder = 14
    Visible = False
  end
  inherited poOpcoes: TPopupMenu
    Left = 304
    Top = 40
  end
end
