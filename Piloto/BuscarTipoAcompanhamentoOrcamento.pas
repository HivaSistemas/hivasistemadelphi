unit BuscarTipoAcompanhamentoOrcamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, _FrameHenrancaPesquisas,
  Frame.TipoAcompanhamentoOrcamento, _RecordsCadastros, _Biblioteca, Vcl.StdCtrls;

type
  TFormBuscarTiposAcompanhamentoOrcamentos = class(TFormHerancaFinalizar)
    FrTipoAcompanhamento: TFrTipoAcompanhamentoOrcamento;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function BuscarDadosCliente(tipoAcompanhamentoId: Integer): TRetornoTelaFinalizar<RecTipoAcompanhamentoOrcamento>;

implementation

{$R *.dfm}

function BuscarDadosCliente(tipoAcompanhamentoId: Integer): TRetornoTelaFinalizar<RecTipoAcompanhamentoOrcamento>;
var
  vForm: TFormBuscarTiposAcompanhamentoOrcamentos;
begin
  vForm := TFormBuscarTiposAcompanhamentoOrcamentos.Create(Application);

  vForm.FrTipoAcompanhamento.InserirDadoPorChave(tipoAcompanhamentoId);

  if Result.Ok(vForm.ShowModal) then
    Result.Dados := vForm.FrTipoAcompanhamento.GetTipoAcompanhamentoOrcamento(0);

  FreeAndNil(vForm);
end;

end.
