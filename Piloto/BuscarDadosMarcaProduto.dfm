inherited FormBuscarDadosMarcaProduto: TFormBuscarDadosMarcaProduto
  Caption = 'Buscar dados marca produto'
  ClientHeight = 119
  ClientWidth = 406
  ExplicitWidth = 412
  ExplicitHeight = 148
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 82
    Width = 406
    inherited sbFinalizar: TSpeedButton
      Left = 137
      ExplicitLeft = 137
    end
  end
  inline FrMarca: TFrMarcas
    Left = 0
    Top = 0
    Width = 401
    Height = 81
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    TabStop = True
    ExplicitWidth = 401
    inherited sgPesquisa: TGridLuka
      Width = 376
      TabOrder = 1
      ExplicitWidth = 293
    end
    inherited CkAspas: TCheckBox
      TabOrder = 2
    end
    inherited CkFiltroDuplo: TCheckBox
      TabOrder = 4
    end
    inherited CkPesquisaNumerica: TCheckBox
      TabOrder = 5
    end
    inherited CkMultiSelecao: TCheckBox
      TabOrder = 3
    end
    inherited PnTitulos: TPanel
      Width = 401
      TabOrder = 0
      ExplicitWidth = 318
      inherited lbNomePesquisa: TLabel
        Width = 33
        Caption = 'Marca'
        ExplicitWidth = 33
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 296
        ExplicitLeft = 213
        inherited ckSuprimir: TCheckBox
          ExplicitLeft = 2
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 376
      ExplicitLeft = 293
    end
  end
end
