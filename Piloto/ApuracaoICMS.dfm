inherited FormApuracaoICMS: TFormApuracaoICMS
  Caption = 'Apura'#231#227'o de ICMS'
  ClientHeight = 409
  ClientWidth = 855
  ExplicitWidth = 861
  ExplicitHeight = 438
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 409
    ExplicitHeight = 409
  end
  inline FrEmpresas: TFrEmpresas
    Left = 124
    Top = 0
    Width = 320
    Height = 81
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 124
    inherited PnTitulos: TPanel
      inherited lbNomePesquisa: TLabel
        Width = 53
        Height = 14
        ExplicitWidth = 53
        ExplicitHeight = 14
      end
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  inline FrDataContabil: TFrDataInicialFinal
    Left = 450
    Top = 8
    Width = 190
    Height = 41
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 450
    ExplicitTop = 8
    inherited Label1: TLabel
      Width = 74
      Height = 14
      Caption = 'Data cont'#225'bil'
      ExplicitWidth = 74
      ExplicitHeight = 14
    end
    inherited lb1: TLabel
      Width = 7
      Height = 14
      ExplicitWidth = 7
      ExplicitHeight = 14
    end
    inherited eDataFinal: TEditLukaData
      Height = 22
      ExplicitHeight = 22
    end
    inherited eDataInicial: TEditLukaData
      Height = 22
      ExplicitHeight = 22
    end
  end
  object sgApuracao: TGridLuka
    Left = 124
    Top = 82
    Width = 728
    Height = 326
    Anchors = [akLeft, akTop, akRight, akBottom]
    ColCount = 9
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
    TabOrder = 3
    OnDblClick = sgApuracaoDblClick
    OnDrawCell = sgApuracaoDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Id'
      'Tipo mov.'
      'Data cont.'
      'CFOP'
      'Valor cont'#225'bil'
      'Base c'#225'lculo'
      'Valor ICMS'
      'Isentas/N'#227'o trib.'
      'Outras')
    OnGetCellColor = sgApuracaoGetCellColor
    Grid3D = False
    RealColCount = 10
    AtivarPopUpSelecao = False
    ColWidths = (
      56
      63
      67
      59
      102
      97
      86
      107
      64)
  end
end
