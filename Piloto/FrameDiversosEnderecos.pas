unit FrameDiversosEnderecos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.StdCtrls, _Biblioteca,
  ComboBoxLuka, _FrameHenrancaPesquisas, FrameBairros, EditLuka, Vcl.Mask, _RecordsCadastros,
  EditCEPLuka, Vcl.Grids, GridLuka, System.Math;

type
  TFrDiversosEnderecos = class(TFrameHerancaPrincipal)
    sgOutrosEnderecos: TGridLuka;
    lb14: TLabel;
    lb15: TLabel;
    lb16: TLabel;
    lb18: TLabel;
    eCEP: TEditCEPLuka;
    eLogradouro: TEditLuka;
    eComplemento: TEditLuka;
    FrBairro: TFrBairros;
    ePontoReferencia: TEditLuka;
    lb19: TLabel;
    cbTipoEndereco: TComboBoxLuka;
    lb2: TLabel;
    eNumero: TEditLuka;
    eInscricaoEstadual: TEditLuka;
    lb6: TLabel;
    procedure sgOutrosEnderecosDblClick(Sender: TObject);
    procedure sgOutrosEnderecosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgOutrosEnderecosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eInscricaoEstadualKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eCEPKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cbTipoEnderecoChange(Sender: TObject);
  private
    _linha_alterando: Integer;
  public
    constructor Create(AOwner: TComponent); override;
    function  getRecDiversosEnderecos: TArray<RecDiversosEnderecos>;
    procedure setDiversosEnderecos(const pEnderecos: TArray<RecDiversosEnderecos>);
    procedure SetFocus; override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
    procedure Clear; override;

  end;

implementation

{$R *.dfm}

const
  coTipoEndereco          = 0;
  coLogradouro            = 1;
  coComplemento           = 2;
  coNumero                = 3;
  coBairro                = 4;
  coPontoReferencia       = 5;
  coCep                   = 6;
  coInscricaoEstadual     = 7;
  coTipoEnderecoSintetico = 8;

procedure TFrDiversosEnderecos.cbTipoEnderecoChange(Sender: TObject);
begin
  inherited;
  eInscricaoEstadual.Enabled := cbTipoEndereco.GetValor = 'E';

  if cbTipoEndereco.GetValor = 'C' then
    eInscricaoEstadual.Clear;
end;

procedure TFrDiversosEnderecos.Clear;
begin
  inherited;
  _Biblioteca.LimparCampos([cbTipoEndereco, eLogradouro, eComplemento, eNumero, FrBairro, ePontoReferencia, eCEP, eInscricaoEstadual]);
end;

constructor TFrDiversosEnderecos.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  _linha_alterando := -1;
end;

procedure TFrDiversosEnderecos.eCEPKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) and not eInscricaoEstadual.Enabled then
    eInscricaoEstadualKeyDown(Sender, Key, Shift)
  else
    ProximoCampo(Sender, Key, Shift);
end;

procedure TFrDiversosEnderecos.eInscricaoEstadualKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if eLogradouro.Trim = '' then begin
    _Biblioteca.Exclamar('O logradouro n�o foi informado corretamente!');
    SetarFoco(eLogradouro);
    Abort;
  end;

  if eComplemento.Trim = '' then begin
    _Biblioteca.Exclamar('O complemento n�o foi informado corretamente!');
    SetarFoco(eComplemento);
    Abort;
  end;

  if eNumero.Trim = '' then begin
    _Biblioteca.Exclamar('O n�mero n�o foi informado corretamente!');
    SetarFoco(eNumero);
    Abort;
  end;

  if FrBairro.EstaVazio then begin
    _Biblioteca.Exclamar('O bairro n�o foi informado corretamente!');
    SetarFoco(FrBairro);
    Abort;
  end;

  if ePontoReferencia.Trim = '' then begin
    _Biblioteca.Exclamar('O ponto de refer�ncia n�o foi informado corretamente!');
    SetarFoco(ePontoReferencia);
    Abort;
  end;

  if not eCEP.getCepOk then begin
    _Biblioteca.Exclamar('O CEP n�o foi informado corretamente!');
    SetarFoco(eCEP);
    Abort;
  end;

  if (sgOutrosEnderecos.RowCount = 2) and (sgOutrosEnderecos.Cells[coTipoEndereco, sgOutrosEnderecos.Row] = '') and (_linha_alterando = -1) then
    _linha_alterando := 1
  else if (_linha_alterando = -1) then begin
    sgOutrosEnderecos.RowCount := sgOutrosEnderecos.RowCount + 1;
    _linha_alterando := sgOutrosEnderecos.RowCount - 1;
  end;

  sgOutrosEnderecos.Cells[coTipoEndereco, _linha_alterando] := cbTipoEndereco.Text;
  sgOutrosEnderecos.Cells[coTipoEnderecoSintetico, _linha_alterando] := cbTipoEndereco.GetValor;
  sgOutrosEnderecos.Cells[coLogradouro, _linha_alterando] := eLogradouro.Text;
  sgOutrosEnderecos.Cells[coComplemento, _linha_alterando] := eComplemento.Text;
  sgOutrosEnderecos.Cells[coNumero, _linha_alterando] := eNumero.Text;
  sgOutrosEnderecos.Cells[coBairro, _linha_alterando] := IntToStr(FrBairro.GetBairro.bairro_id) + ' - ' + FrBairro.GetBairro.nome + ' / ' + FrBairro.GetBairro.nome_cidade;
  sgOutrosEnderecos.Cells[coPontoReferencia, _linha_alterando] := ePontoReferencia.Text;
  sgOutrosEnderecos.Cells[coCep, _linha_alterando] := eCEP.Text;
  sgOutrosEnderecos.Cells[coInscricaoEstadual, _linha_alterando] := IIfStr((cbTipoEndereco.GetValor = 'E') and (eInscricaoEstadual.Text = ''), 'ISENTO', eInscricaoEstadual.Text);

  _linha_alterando := -1;
  _Biblioteca.LimparCampos([eLogradouro, eComplemento, eNumero, FrBairro, ePontoReferencia, eCEP, eInscricaoEstadual]);

  ProximoCampo(Sender, Key, Shift);
end;

function TFrDiversosEnderecos.getRecDiversosEnderecos: TArray<RecDiversosEnderecos>;
var
  i: Integer;
begin
  Result := nil;

  if sgOutrosEnderecos.Cells[coTipoEndereco, 1] = '' then
    Exit;

  SetLength(Result, sgOutrosEnderecos.RowCount - 1);
  for i := sgOutrosEnderecos.FixedRows to sgOutrosEnderecos.RowCount - 1 do begin
    Result[i - sgOutrosEnderecos.FixedRows].tipo_endereco := sgOutrosEnderecos.Cells[coTipoEnderecoSintetico, i];
    Result[i - sgOutrosEnderecos.FixedRows].logradouro := sgOutrosEnderecos.Cells[coLogradouro, i];
    Result[i - sgOutrosEnderecos.FixedRows].complemento := sgOutrosEnderecos.Cells[coComplemento, i];
    Result[i - sgOutrosEnderecos.FixedRows].numero := sgOutrosEnderecos.Cells[coNumero, i];
    Result[i - sgOutrosEnderecos.FixedRows].bairro_id := StrToInt(Trim(Copy(sgOutrosEnderecos.Cells[coBairro, i], 1, Pos(' -', sgOutrosEnderecos.Cells[coBairro, i]))));
    Result[i - sgOutrosEnderecos.FixedRows].ponto_referencia := sgOutrosEnderecos.Cells[coPontoReferencia, i];
    Result[i - sgOutrosEnderecos.FixedRows].cep := sgOutrosEnderecos.Cells[coCep, i];
    Result[i - sgOutrosEnderecos.FixedRows].InscricaoEstadual := sgOutrosEnderecos.Cells[coInscricaoEstadual, i];
  end;
end;

procedure TFrDiversosEnderecos.Modo(pEditando: Boolean; pLimpar: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    cbTipoEndereco,
    eLogradouro,
    eComplemento,
    eNumero,
    FrBairro,
    ePontoReferencia,
    eCEP,
    eInscricaoEstadual,
    sgOutrosEnderecos],
    pEditando
  );
end;

procedure TFrDiversosEnderecos.setDiversosEnderecos(const pEnderecos: TArray<RecDiversosEnderecos>);
var
  i: Integer;
begin
  for i := Low(pEnderecos) to High(pEnderecos) do begin
    sgOutrosEnderecos.Cells[coTipoEndereco, i + 1] := _Biblioteca.Decode(pEnderecos[i].tipo_endereco, ['E', 'Endere�o', 'C', 'Cobran�a']);
    sgOutrosEnderecos.Cells[coTipoEnderecoSintetico, i + 1] := pEnderecos[i].tipo_endereco;
    sgOutrosEnderecos.Cells[coLogradouro, i + 1] := pEnderecos[i].logradouro;
    sgOutrosEnderecos.Cells[coComplemento, i + 1] := pEnderecos[i].complemento;
    sgOutrosEnderecos.Cells[coNumero, i + 1] := pEnderecos[i].numero;
    sgOutrosEnderecos.Cells[coBairro, i + 1] := IntToStr(pEnderecos[i].bairro_id) + ' - ' + pEnderecos[i].nome_bairro + ' / ' +pEnderecos[i].nome_cidade;
    sgOutrosEnderecos.Cells[coPontoReferencia, i + 1] := pEnderecos[i].ponto_referencia;
    sgOutrosEnderecos.Cells[coCep, i + 1] := pEnderecos[i].cep;
    sgOutrosEnderecos.Cells[coInscricaoEstadual, i + 1] := pEnderecos[i].InscricaoEstadual;
  end;
  sgOutrosEnderecos.RowCount := IfThen(Length(pEnderecos) > 1, High(pEnderecos) + 2, 2);
end;

procedure TFrDiversosEnderecos.SetFocus;
begin
  inherited;
  SetarFoco(cbTipoEndereco);
end;

procedure TFrDiversosEnderecos.sgOutrosEnderecosDblClick(Sender: TObject);
var
  bairro_id: Integer;
begin
  inherited;

  if sgOutrosEnderecos.Cells[coLogradouro, sgOutrosEnderecos.Row] = '' then
    Exit;

  _linha_alterando := sgOutrosEnderecos.Row;

  cbTipoEndereco.SetIndicePorValor(sgOutrosEnderecos.Cells[coTipoEnderecoSintetico, _linha_alterando]);
  eLogradouro.Text := sgOutrosEnderecos.Cells[coLogradouro, _linha_alterando];
  eComplemento.Text := sgOutrosEnderecos.Cells[coComplemento, _linha_alterando];
  eNumero.Text := sgOutrosEnderecos.Cells[coNumero, _linha_alterando];
  ePontoReferencia.Text := sgOutrosEnderecos.Cells[coPontoReferencia, _linha_alterando];
  eCEP.Text := sgOutrosEnderecos.Cells[coCep, _linha_alterando];
  eInscricaoEstadual.Text := sgOutrosEnderecos.Cells[coInscricaoEstadual, _linha_alterando];

  bairro_id := StrToInt(Trim(Copy(sgOutrosEnderecos.Cells[coBairro, _linha_alterando], 1, Pos(' -', sgOutrosEnderecos.Cells[coBairro, _linha_alterando]))));
  FrBairro.InserirDadoPorChave(bairro_id);

  cbTipoEnderecoChange(nil);
  eLogradouro.SetFocus;
end;

procedure TFrDiversosEnderecos.sgOutrosEnderecosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  inherited;
  sgOutrosEnderecos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taLeftJustify, Rect);
end;

procedure TFrDiversosEnderecos.sgOutrosEnderecosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_DELETE then
    sgOutrosEnderecos.DeleteRow(sgOutrosEnderecos.Row);
end;

end.
