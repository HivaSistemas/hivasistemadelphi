unit AdiantamentoContasPagar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, _Biblioteca, _Sessao, _ContasPagar,
  _FrameHerancaPrincipal, FramePagamentoFinanceiro, Vcl.StdCtrls, EditLuka, _RecordsFinanceiros,
  Vcl.Mask, EditLukaData, Vcl.Buttons, Vcl.ExtCtrls, _RecordsEspeciais, _TurnosCaixas,
  _ContasPagarBaixas, ImpressaoReciboPagamentoContasPagar, _ContasPagarBaixasItens;

type
  TFormAdiantamentoContasPagar = class(TFormHerancaFinalizar)
    lb8: TLabel;
    lb22: TLabel;
    lb1: TLabel;
    lb4: TLabel;
    lb2: TLabel;
    lb3: TLabel;
    eDataPagamento: TEditLukaData;
    eValorTitulo: TEditLuka;
    eValorTotal: TEditLuka;
    eValorAdiantado: TEditLuka;
    FrPagamento: TFrPagamentoFinanceiro;
    eObservacoes: TEditLuka;
    eValorAdiantar: TEditLuka;
    procedure eValorAdiantarChange(Sender: TObject);
  private
    FTitulo: TArray<RecContaPagar>;
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function AdiantarTitulo(pTituloId: Integer): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

function AdiantarTitulo(pTituloId: Integer): TRetornoTelaFinalizar;
var
  vTitulo: TArray<RecContaPagar>;
  vForm: TFormAdiantamentoContasPagar;
begin
  Result.RetTela := trCancelado;

  if pTituloId = 0 then
    Exit;

  vTitulo := _ContasPagar.BuscarContasPagar(Sessao.getConexaoBanco, 0, [pTituloId]);
  if vTitulo = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  if Sessao.TipoCobrancaNaoPermiteAdiantamento(vTitulo[0].cobranca_id) then begin
    _Biblioteca.Exclamar('Este financeiro n�o pode ser adiantado pois o tipo de cobran�a ao qual o mesmo est� ligado faz parte de um controle interno do Hiva!');
    Exit;
  end;

  vForm := TFormAdiantamentoContasPagar.Create(Application);

  vForm.eValorTitulo.AsCurr    := vTitulo[0].ValorDocumento;
  vForm.eValorAdiantado.AsCurr := vTitulo[0].ValorAdiantado;
  vForm.eValorTotal.AsCurr     := vTitulo[0].ValorSaldo;

  vForm.FTitulo := vTitulo;

  Result.Ok(vForm.ShowModal, True)
end;

{ TFormAdiantamentoContasPagar }

procedure TFormAdiantamentoContasPagar.eValorAdiantarChange(Sender: TObject);
begin
  inherited;
  FrPagamento.ValorTitulos := eValorAdiantar.AsCurr;
end;

procedure TFormAdiantamentoContasPagar.Finalizar(Sender: TObject);
var
  vTurnoId: Integer;
  vBaixaId: Integer;
  vRetBanco: RecRetornoBD;
  vPagarAdiantamentoId: Integer;
begin

  vTurnoId := 0;
  if FrPagamento.eValorDinheiro.AsCurr > 0 then begin
    vTurnoId := _TurnosCaixas.getTurnoId( Sessao.getConexaoBanco, Sessao.getUsuarioLogado.funcionario_id );
    if (vTurnoId = 0) and (FrPagamento.Dinheiro = nil) then begin
      _Biblioteca.Exclamar('N�o foi encontrada a defini��o das contas para o recebimento em dinheiro!');
      SetarFoco(FrPagamento.eValorDinheiro);
      Abort;
    end;
  end;

  Sessao.getConexaoBanco.IniciarTransacao;

 { ******************* Baixa dos t�tulos ******************* }
  // Gerando a capa da baixa
  vRetBanco :=
    _ContasPagarBaixas.AtualizarContasPagarBaixas(
      Sessao.getConexaoBanco,
      0,
      Sessao.getEmpresaLogada.EmpresaId,
      FTitulo[0].CadastroId,
      IIfInt((vTurnoId > 0) and (FrPagamento.Dinheiro = nil), vTurnoId),
      'PG',
      FrPagamento.eValorDinheiro.AsCurr,
      FrPagamento.eValorCheque.AsCurr,
      FrPagamento.eValorCartaoDebito.AsCurr + FrPagamento.eValorCartaoCredito.AsCurr,
      FrPagamento.eValorCobranca.AsCurr,
      FrPagamento.eValorCredito.AsCurr,
      eValorAdiantar.AsCurr,
      0,
      0,
      eDataPagamento.AsData,
      eObservacoes.Text,
      'ADI',
      FTitulo[0].PagarId,
      FrPagamento.Dinheiro,
      FrPagamento.CartoesDebito + FrPagamento.CartoesCredito,
      FrPagamento.Cobrancas,
      FrPagamento.Cheques,
      FrPagamento.Creditos,
      True
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  vBaixaId := vRetBanco.AsInt;

  vRetBanco := _ContasPagar.LancarAdiantamentoContasPagar(Sessao.getConexaoBanco, vBaixaId, True);
  Sessao.AbortarSeHouveErro(vRetBanco);

  vPagarAdiantamentoId := vRetBanco.AsInt;

  // Gravando os t�tulos da baixa
  vRetBanco :=
    _ContasPagarBaixasItens.AtualizarContasPagarBaixasItens(
      Sessao.getConexaoBanco,
      vBaixaId,
      vPagarAdiantamentoId,
      FrPagamento.eValorDinheiro.AsCurr,
      FrPagamento.eValorCheque.AsCurr,
      FrPagamento.eValorCartaoDebito.AsCurr + FrPagamento.eValorCartaoCredito.AsCurr,
      FrPagamento.eValorCobranca.AsCurr,
      FrPagamento.eValorCredito.AsCurr,
      0,
      0
    );

  Sessao.AbortarSeHouveErro(vRetBanco);
  Sessao.getConexaoBanco.FinalizarTransacao;

  _Biblioteca.RotinaSucesso;

  if Perguntar('Deseja imprimir o comprovante de baixa dos t�tulos?') then
    ImpressaoReciboPagamentoContasPagar.Imprimir(vBaixaId);

  inherited;
end;

procedure TFormAdiantamentoContasPagar.VerificarRegistro(Sender: TObject);
var
  i: Integer;
  vTitulosIds: TArray<Integer>;
begin
  inherited;
  if eValorAdiantar.AsCurr = 0 then begin
    _Biblioteca.Exclamar('O valor a ser adiantado n�o foi informado corretamente, verifique!');
    SetarFoco(eValorAdiantar);
    Abort;
  end;

  if eValorAdiantar.AsCurr >= eValorTotal.AsCurr then begin
    _Biblioteca.Exclamar('N�o � permitido realizar um adiantamento maior ou igual que o valor l�quido do t�tulo, verifique!');
    SetarFoco(eValorAdiantar);
    Abort;
  end;

  if not eDataPagamento.DataOk then begin
    Exclamar('A data do pagamento deve ser informada!');
    SetarFoco(eDataPagamento);
    Abort;
  end;

  FrPagamento.VerificarRegistro;

  vTitulosIds := nil;
  for i := Low(FTitulo) to High(FTitulo) do
    _Biblioteca.AddNoVetorSemRepetir( vTitulosIds, FTitulo[i].PagarId );

  if _ContasPagar.ExistemTitulosBaixados( Sessao.getConexaoBanco, vTitulosIds ) then begin
    Exclamar('Existem t�tulos para esta baixa que j� foram baixados, verifique!');
    SetarFoco(FrPagamento);
    Abort;
  end;
end;

end.
