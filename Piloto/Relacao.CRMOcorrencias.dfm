inherited FormRelacaoCRMOcorrencias: TFormRelacaoCRMOcorrencias
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Rela'#231#227'o de ocorr'#234'ncias CRM'
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    inherited sbImprimir: TSpeedButton
      Visible = False
    end
  end
  inherited pcDados: TPageControl
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object Label1: TLabel
        Left = 3
        Top = 89
        Width = 83
        Height = 14
        Caption = 'Texto descri'#231#227'o'
      end
      object Label2: TLabel
        Left = 3
        Top = 173
        Width = 74
        Height = 14
        Caption = 'Texto solu'#231#227'o'
      end
      inline frDataCadastro: TFrDataInicialFinal
        Left = 416
        Top = 0
        Width = 217
        Height = 37
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 416
        ExplicitWidth = 217
        ExplicitHeight = 37
        inherited Label1: TLabel
          Width = 93
          Height = 14
          ExplicitWidth = 93
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline frDataConclusao: TFrDataInicialFinal
        Left = 416
        Top = 48
        Width = 217
        Height = 37
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 416
        ExplicitTop = 48
        ExplicitWidth = 217
        ExplicitHeight = 37
        inherited Label1: TLabel
          Width = 101
          Height = 14
          Caption = 'Data de conclus'#227'o'
          ExplicitWidth = 101
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      object gbStatus: TGroupBoxLuka
        Left = 416
        Top = 100
        Width = 187
        Height = 133
        Caption = 'Status  '
        TabOrder = 2
        OpcaoMarcarDesmarcar = False
        object ckAbertas: TCheckBoxLuka
          Left = 10
          Top = 16
          Width = 111
          Height = 17
          Caption = 'Abertas'
          Checked = True
          State = cbChecked
          TabOrder = 0
          CheckedStr = 'S'
          Valor = 'AB'
        end
        object ckConcluidas: TCheckBoxLuka
          Left = 10
          Top = 39
          Width = 84
          Height = 17
          Caption = 'Conclu'#237'das'
          TabOrder = 1
          CheckedStr = 'N'
          Valor = 'CO'
        end
        object ckRecusadas: TCheckBoxLuka
          Left = 10
          Top = 62
          Width = 84
          Height = 17
          Caption = 'Recusadas'
          TabOrder = 2
          CheckedStr = 'N'
          Valor = 'RE'
        end
        object ckProgramacao: TCheckBoxLuka
          Left = 10
          Top = 86
          Width = 95
          Height = 17
          Caption = 'Programa'#231#227'o'
          TabOrder = 3
          CheckedStr = 'N'
          Valor = 'PR'
        end
        object ckTeste: TCheckBoxLuka
          Left = 10
          Top = 110
          Width = 95
          Height = 17
          Caption = 'Teste'
          TabOrder = 4
          CheckedStr = 'N'
          Valor = 'TE'
        end
      end
      inline FrClientes: TFrClientes
        Left = 1
        Top = 4
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 4
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 45
            ExplicitWidth = 45
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
      inline FrCodigoOcorrencias: TFrameInteiros
        Left = 416
        Top = 358
        Width = 187
        Height = 68
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 4
        TabStop = True
        ExplicitLeft = 416
        ExplicitTop = 358
        ExplicitWidth = 187
        ExplicitHeight = 68
        inherited sgInteiros: TGridLuka
          Top = 19
          Width = 187
          Height = 49
          ExplicitTop = 19
          ExplicitWidth = 187
          ExplicitHeight = 49
          ColWidths = (
            64)
        end
        inherited Panel1: TPanel
          Width = 187
          Height = 19
          Caption = 'C'#243'digo da ocorr'#234'ncia'
          ExplicitWidth = 187
          ExplicitHeight = 19
        end
      end
      object mmTextoDescricao: TMemo
        Left = 3
        Top = 104
        Width = 376
        Height = 66
        TabOrder = 5
      end
      object mmTextoSolucao: TMemo
        Left = 3
        Top = 188
        Width = 376
        Height = 70
        TabOrder = 6
      end
      inline FrFuncionarios: TFrFuncionarios
        Left = 3
        Top = 268
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 7
        TabStop = True
        ExplicitLeft = 3
        ExplicitTop = 268
        ExplicitWidth = 403
        ExplicitHeight = 81
        inherited sgPesquisa: TGridLuka
          Width = 378
          Height = 64
          ExplicitWidth = 378
          ExplicitHeight = 64
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 49
            Caption = 'Usu'#225'rios'
            ExplicitWidth = 49
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          Height = 65
          ExplicitLeft = 378
          ExplicitHeight = 65
        end
        inherited ckSomenteAtivos: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited ckBuscarSomenteAtivos: TCheckBox
          Checked = False
          State = cbUnchecked
        end
      end
      object gbPrioridades: TGroupBoxLuka
        Left = 416
        Top = 236
        Width = 187
        Height = 113
        Caption = 'Prioridade'
        TabOrder = 8
        OpcaoMarcarDesmarcar = False
        object CheckBoxLuka1: TCheckBoxLuka
          Left = 10
          Top = 16
          Width = 111
          Height = 17
          Caption = 'Baixa'
          Checked = True
          State = cbChecked
          TabOrder = 0
          CheckedStr = 'S'
          Valor = 'BA'
        end
        object CheckBoxLuka2: TCheckBoxLuka
          Left = 10
          Top = 39
          Width = 84
          Height = 17
          Caption = 'M'#233'dia'
          Checked = True
          State = cbChecked
          TabOrder = 1
          CheckedStr = 'S'
          Valor = 'ME'
        end
        object CheckBoxLuka3: TCheckBoxLuka
          Left = 10
          Top = 62
          Width = 84
          Height = 17
          Caption = 'Alta'
          Checked = True
          State = cbChecked
          TabOrder = 2
          CheckedStr = 'S'
          Valor = 'AL'
        end
        object CheckBoxLuka4: TCheckBoxLuka
          Left = 10
          Top = 86
          Width = 95
          Height = 17
          Caption = 'Urgente'
          Checked = True
          State = cbChecked
          TabOrder = 3
          CheckedStr = 'S'
          Valor = 'UR'
        end
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object sbGravarOcorrencia: TSpeedButton
        Left = 7
        Top = 464
        Width = 127
        Height = 22
        Caption = 'Gravar altera'#231#245'es'
        Flat = True
        OnClick = sbGravarOcorrenciaClick
      end
      object lbTransferencia: TStaticTextLuka
        Left = 0
        Top = 0
        Width = 884
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Ocorr'#234'ncias'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object sgOcorrencias: TGridLuka
        Left = 0
        Top = 17
        Width = 884
        Height = 257
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alTop
        ColCount = 8
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
        PopupMenu = pmOpcoesOcorrencias
        TabOrder = 1
        OnClick = sgOcorrenciasClick
        OnDrawCell = sgOcorrenciasDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Ocorr'#234'ncia'
          'Cliente'
          'Status'
          'Prioridade'
          'Usu'#225'rio'
          'Solicita'#231#227'o'
          'Data de cadastro'
          'Data de conclus'#227'o')
        OnGetCellColor = sgOcorrenciasGetCellColor
        Grid3D = False
        RealColCount = 12
        OrdenarOnClick = True
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          75
          178
          80
          88
          135
          67
          114
          118)
      end
      object pgDetalhesOcorrencia: TPageControl
        Left = 0
        Top = 280
        Width = 884
        Height = 177
        ActivePage = tsDescricaoOcorrencia
        TabOrder = 2
        object tsDescricaoOcorrencia: TTabSheet
          Caption = 'Descri'#231#227'o'
          inline frDescricaoOcorrencia: TFrEdicaoTexto
            Left = 0
            Top = 0
            Width = 876
            Height = 148
            Align = alClient
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBackground = False
            ParentColor = False
            ParentFont = False
            TabOrder = 0
            TabStop = True
            ExplicitWidth = 876
            ExplicitHeight = 148
            inherited pnFerramentas: TPanel
              Width = 876
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 876
              inherited cbFonte: TComboBox
                Height = 22
                ExplicitHeight = 22
              end
              inherited cbTamanho: TComboBox
                Height = 22
                ExplicitHeight = 22
              end
            end
            inherited rtTexto: TRichEdit
              Width = 865
              Height = 96
              ExplicitLeft = 10
              ExplicitWidth = 865
              ExplicitHeight = 96
            end
            inherited stbStatus: TStatusBar
              Top = 129
              Width = 876
              ExplicitLeft = 0
              ExplicitTop = 129
              ExplicitWidth = 876
            end
          end
        end
        object tsSolucaoOcorrencia: TTabSheet
          Caption = 'Solu'#231#227'o'
          ImageIndex = 1
          inline frSolucaoOcorrencia: TFrEdicaoTexto
            Left = 0
            Top = 0
            Width = 876
            Height = 148
            Align = alClient
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBackground = False
            ParentColor = False
            ParentFont = False
            TabOrder = 0
            TabStop = True
            ExplicitWidth = 876
            ExplicitHeight = 148
            inherited pnFerramentas: TPanel
              Width = 876
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 876
              inherited cbFonte: TComboBox
                Height = 22
                ExplicitHeight = 22
              end
              inherited cbTamanho: TComboBox
                Height = 22
                ExplicitHeight = 22
              end
            end
            inherited rtTexto: TRichEdit
              Width = 865
              Height = 96
              ExplicitLeft = 10
              ExplicitWidth = 865
              ExplicitHeight = 96
            end
            inherited stbStatus: TStatusBar
              Top = 129
              Width = 876
              ExplicitLeft = 0
              ExplicitTop = 129
              ExplicitWidth = 876
            end
          end
        end
      end
      inline FrArquivos: TFrArquivos
        Left = -2
        Top = 488
        Width = 127
        Height = 22
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = -2
        ExplicitTop = 488
        ExplicitWidth = 127
        ExplicitHeight = 22
        inherited sbArquivos: TSpeedButtonLuka
          Width = 127
          Height = 22
          Caption = 'Anexos'
          OnClick = FrArquivossbArquivosClick
          TagImagem = 0
          ExplicitLeft = -8
          ExplicitTop = 0
          ExplicitWidth = 127
          ExplicitHeight = 22
        end
      end
    end
  end
  object pmOpcoesOcorrencias: TPopupMenu
    Left = 832
    Top = 152
    object miReceberPedidoSelecionado: TMenuItem
      Caption = 'Alterar status'
      object Concluda1: TMenuItem
        Caption = 'Conclu'#237'da'
        OnClick = Concluda1Click
      end
      object Programao1: TMenuItem
        Caption = 'Programa'#231#227'o'
        OnClick = Programao1Click
      end
      object Emteste1: TMenuItem
        Caption = 'Teste'
        OnClick = Emteste1Click
      end
      object Recusada1: TMenuItem
        Caption = 'Recusada'
        OnClick = Recusada1Click
      end
      object Reabrir1: TMenuItem
        Caption = 'Reabrir'
        OnClick = Reabrir1Click
      end
    end
    object miN2: TMenuItem
      Caption = '-'
    end
    object miConsultarStatusServicoNFe: TMenuItem
      Caption = 'Alterar prioridade'
      object Mdia1: TMenuItem
        Caption = 'Baixa'
        OnClick = Mdia1Click
      end
      object Mdia2: TMenuItem
        Caption = 'M'#233'dia'
        OnClick = Mdia2Click
      end
      object Alta1: TMenuItem
        Caption = 'Alta'
        OnClick = Alta1Click
      end
      object Urgente1: TMenuItem
        Caption = 'Urgente'
        OnClick = Urgente1Click
      end
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Vincularsolicitao1: TMenuItem
      Caption = 'Vincular solicita'#231#227'o'
      OnClick = Vincularsolicitao1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Alterarusurio1: TMenuItem
      Caption = 'Alterar usu'#225'rio'
      OnClick = Alterarusurio1Click
    end
  end
end
