unit DefinirTransporte;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.StdCtrls, _RecordsEspeciais,
  StaticTextLuka, FrameMotoristas, _FrameHerancaPrincipal, _Biblioteca, _Sessao, _ControlesEntregas,
  _FrameHenrancaPesquisas, FrameVeiculos, Vcl.Buttons, Vcl.ExtCtrls, _RecordsExpedicao,
  _ControlesEntregasItens, FrameFuncionarios, Data.DB, OraCall, DBAccess, Ora,
  MemDS, frxClass, frxDBSet;

type
  TFormDefinirTransporte = class(TFormHerancaFinalizar)
    FrVeiculos: TFrVeiculos;
    FrMotorista: TFrMotoristas;
    StaticTextLuka1: TStaticTextLuka;
    stQuantidadeProdutos: TStaticText;
    stPesoTotal: TStaticText;
    StaticTextLuka2: TStaticTextLuka;
    StaticTextLuka3: TStaticTextLuka;
    stQuantidadeEntregas: TStaticText;
    StaticTextLuka4: TStaticTextLuka;
    stPesoMaximoVeiculo: TStaticText;
    FrAjudantes: TFrFuncionarios;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FEntregasIds: TArray<Integer>;

    procedure FrVeiculoOnAposPesquisar(Sender: TObject);
    procedure FrVeiculoOnAposDeletar(Sender: TObject);
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Finalizar(Sender: TObject); override;
  end;

function Definir(pEntregas: TArray<RecEntrega>): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

function Definir(pEntregas: TArray<RecEntrega>): TRetornoTelaFinalizar;
var
  i: Integer;
  vForm: TFormDefinirTransporte;
  vQtdeProdutos: Integer;
  vPesoTotal: Double;
begin
  vForm := TFormDefinirTransporte.Create(nil);
  vForm.FEntregasIds := nil;

  vQtdeProdutos := 0;
  vPesoTotal := 0;
  for i := Low(pEntregas) to High(pEntregas) do begin
    vQtdeProdutos := vQtdeProdutos + pEntregas[i].QtdeProdutos;
    vPesoTotal := vPesoTotal + pEntregas[i].PesoTotal;

    _Biblioteca.AddNoVetorSemRepetir(vForm.FEntregasIds, pEntregas[i].EntregaId);
  end;
  vForm.stQuantidadeProdutos.Caption := NFormat(vQtdeProdutos);
  vForm.stPesoTotal.Caption := NFormatN(vPesoTotal, 3);
  vForm.stQuantidadeEntregas.Caption := NFormat(Length(pEntregas));
  vForm.stPesoMaximoVeiculo.Caption := '';

  Result.Ok(vForm.ShowModal, True);
end;

procedure TFormDefinirTransporte.Finalizar(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco :=
    _ControlesEntregas.AtualizarControles(
      Sessao.getConexaoBanco,
      0,
      Sessao.getEmpresaLogada.EmpresaId,
      FrVeiculos.getDados().VeiculoId,
      FrMotorista.getMotorista().Cadastro.cadastro_id,
      FEntregasIds,
      FrAjudantes.GetArrayOfInteger
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  _Biblioteca.Informar( coNovoRegistroSucessoCodigo + NFormat(vRetBanco.AsInt) );
  inherited;
end;

procedure TFormDefinirTransporte.FormCreate(Sender: TObject);
begin
  inherited;
  FrVeiculos.OnAposPesquisar := FrVeiculoOnAposPesquisar;
  FrVeiculos.OnAposDeletar := FrVeiculoOnAposDeletar;

  _Biblioteca.LimparCampos([stPesoMaximoVeiculo, stQuantidadeProdutos, stQuantidadeEntregas, stPesoTotal]);
end;

procedure TFormDefinirTransporte.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrVeiculos);
end;

procedure TFormDefinirTransporte.FrVeiculoOnAposDeletar(Sender: TObject);
begin
  stPesoMaximoVeiculo.Caption := '';
end;

procedure TFormDefinirTransporte.FrVeiculoOnAposPesquisar(Sender: TObject);
begin
  stPesoMaximoVeiculo.Caption := NFormat( FrVeiculos.getDados.PesoMaximo, 3 );
  if FrVeiculos.getDados.MotoristaId > 0 then
    FrMotorista.InserirDadoPorChave( FrVeiculos.getDados.MotoristaId );
end;

procedure TFormDefinirTransporte.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if FrVeiculos.EstaVazio then begin
    _Biblioteca.Exclamar('O ve�culo n�o foi informado corretamente, verifique!');
    SetarFoco(FrVeiculos);
    Abort;
  end;

  if FrMotorista.EstaVazio then begin
    _Biblioteca.Exclamar('O motorista n�o foi informado corretamente, verifique!');
    SetarFoco(FrMotorista);
    Abort;
  end;

  if _Biblioteca.SFormatCurr(stPesoTotal.Caption) > _Biblioteca.SFormatCurr(stPesoMaximoVeiculo.Caption) then begin
    if not _Biblioteca.Perguntar('O peso total das entregas � maior que o peso m�ximo suportado pelo ve�culo, deseja continuar?') then begin
      SetarFoco(FrVeiculos);
      Abort;
    end;
  end;
end;

end.
