unit FrameBairros;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _Sessao, _RecordsCadastros,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Bairros, System.Math,
  Vcl.Buttons, Vcl.Menus;

type
  TFrBairros = class(TFrameHenrancaPesquisas)
    ckPreencherCidade: TCheckBox;
  public
    function GetBairro(pLinha: Integer = -1): RecBairros;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrBairros }

uses
  PesquisaBairros;

function TFrBairros.AdicionarDireto: TObject;
var
  bairros: TArray<RecBairros>;
begin
  bairros := _Bairros.BuscarBairros(Sessao.getConexaoBanco, 0, [FChaveDigitada], 0);
  if bairros = nil then
    Result := nil
  else
    Result := bairros[0];
end;

function TFrBairros.AdicionarPesquisando: TObject;
begin
  Result := PesquisaBairros.PesquisarBairro(ckPreencherCidade.Checked);
end;

function TFrBairros.GetBairro(pLinha: Integer): RecBairros;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecBairros(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrBairros.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecBairros(FDados[i]).bairro_id = RecBairros(pSender).bairro_id then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrBairros.MontarGrid;
var
  i: Integer;
  pSender: RecBairros;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      pSender := RecBairros(FDados[i]);
      AAdd([IntToStr(pSender.bairro_id), pSender.nome + '-' + pSender.nome_cidade + '/' + pSender.EstadoId]);
    end;
  end;
end;

end.
