unit Cadastrar.TipoAcompanhamentoOrcamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls,
  CheckBoxLuka, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _RecordsCadastros, _Biblioteca,
  _Sessao, _RecordsEspeciais;

type
  TFormCadastroTipoAcompanhamentoOrcamento = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eDescricao: TEditLuka;
  private
    procedure PreencherRegistro(pTipoAcompanhamentoOrcamento: RecTipoAcompanhamentoOrcamento);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

uses Pesquisa.TipoAcompanhamentoOrcamento, _TipoAcompanhamentoOrcamento;

{ TFormTipoAcompanhamentoOrcamento }

procedure TFormCadastroTipoAcompanhamentoOrcamento.BuscarRegistro;
var
  vTipoAcompanhamentoOrcamento: TArray<RecTipoAcompanhamentoOrcamento>;
begin
  vTipoAcompanhamentoOrcamento := _TipoAcompanhamentoOrcamento.BuscarTipoAcompanhamentoOrcamento(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if vTipoAcompanhamentoOrcamento = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    eID.SetFocus;
    Exit;
  end;

  inherited;
  PreencherRegistro(vTipoAcompanhamentoOrcamento[0]);
end;

procedure TFormCadastroTipoAcompanhamentoOrcamento.ExcluirRegistro;
var
  vRetorno: RecRetornoBD;
begin
  vRetorno := _TipoAcompanhamentoOrcamento.ExcluirTipoAcompanhamentoOrcamento(Sessao.getConexaoBanco, eId.AsInt);
  if vRetorno.TeveErro then begin
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroTipoAcompanhamentoOrcamento.GravarRegistro(Sender: TObject);
var
  vRetorno: RecRetornoBD;
begin
  inherited;
  vRetorno :=
    _TipoAcompanhamentoOrcamento.AtualizarTipoAcompanhamentoOrcamento(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eDescricao.Text,
      ckAtivo.CheckedStr
    );

  if vRetorno.TeveErro then begin
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetorno.AsInt);
end;

procedure TFormCadastroTipoAcompanhamentoOrcamento.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([eDescricao, ckAtivo], pEditando);

  if pEditando then begin
    SetarFoco(eDescricao);
    ckAtivo.Checked := True;
  end;
end;

procedure TFormCadastroTipoAcompanhamentoOrcamento.PesquisarRegistro;
var
  vTipoAcompanhamentoOrcamento: RecTipoAcompanhamentoOrcamento;
begin
  vTipoAcompanhamentoOrcamento := RecTipoAcompanhamentoOrcamento(Pesquisa.TipoAcompanhamentoOrcamento.Pesquisar());  //AquiS
  if vTipoAcompanhamentoOrcamento = nil then
    Exit;

  inherited;
  PreencherRegistro(vTipoAcompanhamentoOrcamento);
end;

procedure TFormCadastroTipoAcompanhamentoOrcamento.PreencherRegistro(pTipoAcompanhamentoOrcamento: RecTipoAcompanhamentoOrcamento);
begin
  eID.AsInt                     := pTipoAcompanhamentoOrcamento.tipo_acompanhamento_id;
  eDescricao.Text               := pTipoAcompanhamentoOrcamento.descricao;
  ckAtivo.CheckedStr            := pTipoAcompanhamentoOrcamento.ativo;
  pTipoAcompanhamentoOrcamento.Free;
end;

procedure TFormCadastroTipoAcompanhamentoOrcamento.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eDescricao.Trim = '' then begin
    _Biblioteca.Exclamar('A descri��o n�o foi informado corretamente, verifique!');
    SetarFoco(eDescricao);
    Abort;
  end;
end;

end.
