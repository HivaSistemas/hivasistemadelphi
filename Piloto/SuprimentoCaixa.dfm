inherited FormSuprimentoCaixa: TFormSuprimentoCaixa
  Caption = 'Suprimento de caixa'
  ClientHeight = 230
  ClientWidth = 459
  ExplicitWidth = 465
  ExplicitHeight = 259
  PixelsPerInch = 96
  TextHeight = 14
  object lb11: TLabel [0]
    Left = 128
    Top = 114
    Width = 27
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Valor'
  end
  object lb1: TLabel [1]
    Left = 229
    Top = 114
    Width = 69
    Height = 14
    Caption = 'Observa'#231#245'es'
  end
  inherited pnOpcoes: TPanel
    Height = 230
    ExplicitHeight = 194
    inherited sbDesfazer: TSpeedButton
      Top = 50
      ExplicitTop = 50
    end
    inherited sbExcluir: TSpeedButton
      Left = -113
      ExplicitLeft = -113
    end
    inherited sbPesquisar: TSpeedButton
      Top = 138
      Visible = False
      ExplicitTop = 138
    end
  end
  inline FrCaixaAberto: TFrFuncionarios
    Left = 128
    Top = 22
    Width = 318
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 128
    ExplicitTop = 22
    ExplicitWidth = 318
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 293
      Height = 23
      TabOrder = 1
      ExplicitWidth = 293
      ExplicitHeight = 23
    end
    inherited CkAspas: TCheckBox
      TabOrder = 2
    end
    inherited CkFiltroDuplo: TCheckBox
      TabOrder = 4
    end
    inherited CkPesquisaNumerica: TCheckBox
      TabOrder = 5
    end
    inherited CkMultiSelecao: TCheckBox
      TabOrder = 3
    end
    inherited PnTitulos: TPanel
      Width = 318
      TabOrder = 0
      ExplicitWidth = 318
      inherited lbNomePesquisa: TLabel
        Width = 29
        Caption = 'Caixa'
        ExplicitWidth = 29
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 213
        ExplicitLeft = 213
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 293
      Height = 24
      ExplicitLeft = 293
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
    inherited ckCaixa: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  object eValorDinheiro: TEditLuka
    Left = 128
    Top = 130
    Width = 95
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 1
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  inline FrContaOrigem: TFrContas
    Left = 126
    Top = 67
    Width = 320
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 67
    ExplicitWidth = 320
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 295
      Height = 23
      ExplicitWidth = 295
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 320
      ExplicitWidth = 320
      inherited lbNomePesquisa: TLabel
        Width = 122
        Caption = 'Conta origem dinheiro'
        ExplicitWidth = 122
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 215
        ExplicitLeft = 215
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 295
      Height = 24
      ExplicitLeft = 295
      ExplicitHeight = 24
    end
    inherited rgTipo: TRadioGroupLuka
      ItemIndex = 0
    end
  end
  object eObservacoes: TMemo
    Left = 229
    Top = 130
    Width = 219
    Height = 93
    TabOrder = 4
  end
end
