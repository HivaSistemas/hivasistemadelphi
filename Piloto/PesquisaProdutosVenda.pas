unit PesquisaProdutosVenda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _ProdutosVenda, System.Math,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _FrameHerancaPrincipal, _Sessao, _RecordsOrcamentosVendas,
  _FrameHenrancaPesquisas, FrameCondicoesPagamento, Vcl.ExtCtrls, FrameImagem, _Biblioteca, TabelaPrecosProduto,
  _Imagens, Vcl.Menus, Vcl.ComCtrls, _HerancaFinalizar, Vcl.Buttons, Legendas, Informacoes.EstoqueProdutos,
  SpeedButtonLuka, CheckBoxLuka;

type
  TFormPesquisaVendas = class(TFormHerancaFinalizar)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    FrImagemProduto: TFrImagem;
    miProximafoto: TMenuItem;
    miAnterior: TMenuItem;
    eCaracteristicas: TMemo;
    lb2: TLabel;
    sgItens: TGridLuka;
    lbNomeCondicaoPagto2: TLabel;
    lbNomeCondicaoPagto3: TLabel;
    lbNomeCondicaoPagto4: TLabel;
    pmOpcoesGrid: TPopupMenu;
    lbPrecoCondicaoPagto2: TLabel;
    lbPrecoCondicaoPagto3: TLabel;
    lbPrecoCondicaoPagto4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    lbEstoqueGrupo: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    lbEstoqueTodasEmpresas: TLabel;
    sbTabelaPrecos: TSpeedButtonLuka;
    sbInformacoesEstoque: TSpeedButtonLuka;
    lb1: TLabel;
    lb3: TLabel;
    lbCodigoBarras: TLabel;
    lbCodigoOriginal: TLabel;
    lblOpcoesPesquisa: TLabel;
    lblPesquisa: TLabel;
    lbMarca: TLabel;
    FrCondicaoPagamento: TFrCondicoesPagamento;
    cbOpcoesPesquisa: TComboBoxLuka;
    eProduto: TEditLuka;
    eMarca: TEditLuka;
    Label3: TLabel;
    miLegendas: TSpeedButton;
    ckSomenteProdutosPromocao: TCheckBoxLuka;
    procedure cbOpcoesPesquisaClick(Sender: TObject);
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgPesquisaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FrImagemProdutoimFotoClick(Sender: TObject);
    procedure miProximafotoClick(Sender: TObject);
    procedure miAnteriorClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure cbOpcoesPesquisaChange(Sender: TObject);
    procedure eProdutoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure miLegendasClick(Sender: TObject);
    procedure eMarcaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgItensClick(Sender: TObject);
    procedure pnInformacoesEstoque2Click(Sender: TObject);
    procedure sgItensDblClick(Sender: TObject);
    procedure sbTabelaPrecosClick(Sender: TObject);
    procedure sbInformacoesEstoqueClick(Sender: TObject);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    FPDV: Boolean;
    FIndexFoto: Integer;
    FApenasConsulta: Boolean;
    FItens: TArray<RecProdutosVendas>;
    FIndexFotosProdutoVetor: Integer;
    FFotos: TArray<RecFotosProdutosVendas>;

    FLegendaGrid: TImage;

    procedure FrCondicaoPagamentoAposPesquisar(Sender: TObject);
    function LengendaGrid(pSemEstoque: Boolean; pPromocao: Boolean; pFisicoMaiorReal: Boolean; pFisicoMenorReal: Boolean; pProducao: Boolean): TPicture;
  protected
    procedure BuscarRegistros;
    procedure BuscarFiltrosTela; override;
    procedure AjustaTela;
  end;

function PesquisarProdutosVendas(
  pCondicaoId: Integer;
  pApenasConsulta: Boolean;
  pTrazerVariosProdutos: Boolean;
  pPDV: Boolean
): TArray<RecProdutosVendas>;

implementation

{$R *.dfm}

{ TFormPesquisaVendas }

const
  coSelecionado      = 0;
  coLegenda          = 1;
  coProdutoId        = 2;
  coNome             = 3;
  coMarca            = 4;
  coEstoque          = 5;
  coUnidadeVenda     = 6;
  cpPrecoUnitario    = 7;
  coPrecoPromocional = 8;

  (* Colunas ocultas *)
  coCaracteristicas   = 9;
  coPrecoBase         = 10;
  coPrecoPromocaoBase = 11;
  coCodigoBarras      = 12;
  coCodigoOriginalFabr= 13;
  coFisicoMaiorReal   = 14;
  coFisicoMenorReal   = 15;
  coProducao          = 16;

  cbNomeMarcaAvancado = 3;
  cbNomeMarca         = 5;
  cbNomeCodigoFabricante = 8;

  corCorSemEstoque = clRed;
  coCorPromocao    = clGreen;
  coCorKit         = $000096DB;
  coCorFisicoMaiorReal = clYellow;
  coCorFisicoMenorReal = clGray;
  coCorProducao    = clBlue;

function PesquisarProdutosVendas(
  pCondicaoId: Integer;
  pApenasConsulta: Boolean;
  pTrazerVariosProdutos: Boolean;
  pPDV: Boolean
): TArray<RecProdutosVendas>;
var
  i: Integer;
  t: TFormPesquisaVendas;
begin
  Result := nil;
  t := TFormPesquisaVendas.Create(Application);

  t.FrCondicaoPagamento.InserirDadoPorChave(pCondicaoId, False);
  t.cbOpcoesPesquisa.Filtros := _ProdutosVenda.getFiltros;
  t.FApenasConsulta := pApenasConsulta;
  t.FPDV := pPDV;
  t.cbOpcoesPesquisaChange(nil);

  if pApenasConsulta or pTrazerVariosProdutos then
    t.sgItens.OcultarColunas([0]);

  t.sgItens.Col := coNome;

  if t.ShowModal = mrOk then begin
    for i := t.sgItens.FixedRows to t.sgItens.RowCount - 1 do begin
      if not Em(t.sgItens.Cells[coSelecionado, i], [charSelecionado, charNaoSelecionado]) then
        Continue;

      if t.sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
        Continue;

      SetLength(Result, Length(Result) + 1);
      Result[High(Result)] := t.FItens[i - 1];
    end;
  end;
  FreeAndNil(t);
end;

procedure TFormPesquisaVendas.BuscarFiltrosTela;
begin
  inherited;
  AjustaTela;

  if (eProduto.Text <> '') or (eMarca.Text <> '') then begin
    BuscarRegistros;
    SetarFoco(eProduto);
  end;
end;

procedure TFormPesquisaVendas.AjustaTela;
begin
  _Biblioteca.Visibilidade([eMarca, lbMarca], cbOpcoesPesquisa.ItemIndex in [cbNomeMarcaAvancado,cbNomeMarca, cbNomeCodigoFabricante]);
  _Biblioteca.Habilitar([eMarca, lbMarca], cbOpcoesPesquisa.ItemIndex in [cbNomeMarcaAvancado,cbNomeMarca, cbNomeCodigoFabricante], False);
  eProduto.Width := IfThen(eMarca.Visible, 216, 335);
  if cbOpcoesPesquisa.ItemIndex = cbNomeCodigoFabricante then
    lbMarca.Caption := 'C�d. ref. da ind�stria'
  else
    lbMarca.Caption := 'Marca';
end;

procedure TFormPesquisaVendas.BuscarRegistros;
var
  i: Integer;
  vProdutosIds: TArray<Integer>;
  linha: Integer;
begin
  inherited;
  FFotos := nil;
  sgItens.ClearGrid();

  if FrCondicaoPagamento.EstaVazio then begin
    Exclamar('A condi��o de pagamento � necess�ria antes de pesquisar os produtos!');
    SetarFoco(FrCondicaoPagamento);
    Exit;
  end;

  if (cbOpcoesPesquisa.ItemIndex in [cbNomeMarcaAvancado,cbNomeMarca, cbNomeCodigoFabricante]) then begin
    FItens :=
      _ProdutosVenda.BuscarProdutosVendas(
        Sessao.getConexaoBanco,
        cbOpcoesPesquisa.GetIndice,
        [Sessao.getEmpresaLogada.EmpresaId, eProduto.Text, eMarca.Text],
        FPDV,
        ckSomenteProdutosPromocao.Checked
      );
  end
  else begin
    FItens :=
      _ProdutosVenda.BuscarProdutosVendas(
        Sessao.getConexaoBanco,
        cbOpcoesPesquisa.GetIndice,
        [Sessao.getEmpresaLogada.EmpresaId, eProduto.Text],
        FPDV,
        ckSomenteProdutosPromocao.Checked
      );
  end;

  if FItens = nil then begin
    _Biblioteca.NenhumRegistro;
    SetarFoco(eProduto);
    Exit;
  end;

  linha := 0;
  for i := Low(FItens) to High(FItens) do begin
    SetLength(vProdutosIds, Length(vProdutosIds) + 1);
    vProdutosIds[High(vProdutosIds)] := FItens[i].produto_id;

    sgItens.Cells[coSelecionado, linha + 1]     := charNaoSelecionado;
    sgItens.Cells[coProdutoId, linha + 1]       := NFormat(FItens[i].produto_id);
    sgItens.Cells[coNome, linha + 1]            := FItens[i].nome;
    sgItens.Cells[coUnidadeVenda, linha + 1]    := FItens[i].unidade_venda;
    sgItens.Cells[coMarca, linha + 1]           := FItens[i].nome_marca;

    sgItens.Cells[cpPrecoUnitario, linha + 1]   :=  NFormatN(IIfDbl(FPDV, FItens[i].preco_pdv, FItens[i].preco_varejo) * FrCondicaoPagamento.getDados().indice_acrescimo);

    sgItens.Cells[coEstoque, linha + 1]            := NFormatNEstoque(FItens[i].disponivel);
    sgItens.Cells[coCaracteristicas, linha + 1]    := FItens[i].caracteristicas;
    sgItens.Cells[coPrecoBase, linha + 1]          := NFormatN(FItens[i].preco_varejo);

    sgItens.Cells[coPrecoPromocional, linha + 1]   := _Biblioteca.NFormatN(FItens[i].PrecoPromocional * FrCondicaoPagamento.getDados.getIndiceAcrescimoPrecoPromocional);
    sgItens.Cells[coPrecoPromocaoBase, linha + 1]  := _Biblioteca.NFormatN(FItens[i].PrecoPromocional);
    sgItens.Cells[coCodigoBarras, linha + 1]       := FItens[i].CodigoBarras;
    sgItens.Cells[coCodigoOriginalFabr, linha + 1] := FItens[i].CodigoOriginalFabricante;
    sgItens.Cells[coFisicoMaiorReal, linha + 1]    := _Biblioteca.IIfStr(FItens[i].Fisico > FItens[i].estoque, 'S', 'N');
    sgItens.Cells[coFisicoMenorReal, linha + 1]    := _Biblioteca.IIfStr(FItens[i].Fisico < FItens[i].estoque, 'S', 'N');
    sgItens.Cells[coProducao, linha + 1]           := _Biblioteca.IIfStr(FItens[i].TipoControleEstoque = 'I', 'S', 'N'); //Industrializa��o
    linha := linha + 1;
  end;
  sgItens.SetLinhasGridPorTamanhoVetor( Length(vProdutosIds) );
  sgItens.Col := coNome;
  SetarFoco(sgItens);

  FFotos := _ProdutosVenda.BuscarFotosProdutosVendas(Sessao.getConexaoBanco, vProdutosIds);
  sgItensClick(nil);
end;

procedure TFormPesquisaVendas.cbOpcoesPesquisaChange(Sender: TObject);
begin
  inherited;
  AjustaTela;
end;

procedure TFormPesquisaVendas.cbOpcoesPesquisaClick(Sender: TObject);
begin
  inherited;
  cbOpcoesPesquisa.Hint := cbOpcoesPesquisa.Text;
end;

procedure TFormPesquisaVendas.eMarcaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    BuscarRegistros;
end;

procedure TFormPesquisaVendas.eProdutoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if eMarca.Enabled then begin
    SetarFoco(eMarca);
    Exit;
  end;

  BuscarRegistros;
end;

procedure TFormPesquisaVendas.FormClose(Sender: TObject; var Action: TCloseAction);
var
  i: Integer;
begin
  SalvarFiltrosTela;
  inherited;

  for i := High(FFotos) downto Low(FFotos) do begin
    if FFotos[i].Foto3 <> nil then begin
      FFotos[i].Foto3.Destroy;
      FFotos[i].Foto3 := nil;
    end;

    if FFotos[i].Foto2 <> nil then begin
      FFotos[i].Foto2.Destroy;
      FFotos[i].Foto2 := nil;
    end;

    if FFotos[i].Foto1 <> nil then begin
      FFotos[i].Foto1.Destroy;
      FFotos[i].Foto1 := nil;
    end;
  end;

  FFotos := nil;
  FLegendaGrid.Free;
end;

procedure TFormPesquisaVendas.FormCreate(Sender: TObject);
begin
  inherited;
  AtivarAnaliseCusto(Sessao.getParametros.UtilizarAnaliseCusto);
  FrCondicaoPagamento.OnAposPesquisar := FrCondicaoPagamentoAposPesquisar;

  _Biblioteca.LimparCampos([lbCodigoBarras, lbCodigoOriginal]);

  FLegendaGrid := TImage.Create(Self);
  FLegendaGrid.Height := 15;
  FrCondicaoPagamento.SomenteEmpresaLogada := True;
end;

procedure TFormPesquisaVendas.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(eProduto);
  cbOpcoesPesquisa.ItemIndex := cbNomeMarcaAvancado;
  cbOpcoesPesquisaChange(nil);
  BuscarFiltrosTela;

  lbNomeCondicaoPagto2.Caption := NFormat(Sessao.getParametrosEmpresa.CondicaoPagamentoConsProd2) + ' - ' + Sessao.getParametrosEmpresa.NomeCondPagamentoCons2;
  lbNomeCondicaoPagto3.Caption := NFormat(Sessao.getParametrosEmpresa.CondicaoPagamentoConsProd3) + ' - ' + Sessao.getParametrosEmpresa.NomeCondPagamentoCons3;
  lbNomeCondicaoPagto4.Caption := NFormat(Sessao.getParametrosEmpresa.CondicaoPagamentoConsProd4) + ' - ' + Sessao.getParametrosEmpresa.NomeCondPagamentoCons4;

  lbPrecoCondicaoPagto2.Caption := '';
  lbPrecoCondicaoPagto3.Caption := '';
  lbPrecoCondicaoPagto4.Caption := '';
end;

procedure TFormPesquisaVendas.FrCondicaoPagamentoAposPesquisar(Sender: TObject);
var
  i: Integer;
begin
  for i := 1 to sgItens.RowCount - 1 do begin
    sgItens.Cells[cpPrecoUnitario, i]    := NFormatN(SFormatDouble(sgItens.Cells[coPrecoBase, i]) * FrCondicaoPagamento.getDados().indice_acrescimo);
    sgItens.Cells[coPrecoPromocional, i] := NFormatN(SFormatDouble(sgItens.Cells[coPrecoPromocaoBase, i]) * FrCondicaoPagamento.getDados().getIndiceAcrescimoPrecoPromocional);
  end;
end;

procedure TFormPesquisaVendas.FrImagemProdutoimFotoClick(Sender: TObject);
begin
//  inherited;
end;

function TFormPesquisaVendas.LengendaGrid(
  pSemEstoque: Boolean;
  pPromocao: Boolean;
  pFisicoMaiorReal: Boolean;
  pFisicoMenorReal: Boolean;
  pProducao: Boolean
): TPicture;
var
	p: Integer;
  l: Integer;
begin
 	l := 0;

  if pSemEstoque then
    Inc(l, 10);

  if pPromocao then
    Inc(l, 10);

  if pFisicoMaiorReal then
    Inc(l, 10);

  if pFisicoMenorReal then
    Inc(l, 10);

  if pProducao then
    Inc(l, 10);

  p := 1;
  FLegendaGrid.Width := l + p;
  FLegendaGrid.Picture := nil;

  if pSemEstoque then begin
  	FLegendaGrid.Canvas.Pen.Color := clRed;
  	FLegendaGrid.Canvas.Brush.Color := clRed;
  	FLegendaGrid.Canvas.Rectangle(p, 0, p + 5, 15);
    Inc(p, 10);
  end;

  if pPromocao then begin
  	FLegendaGrid.Canvas.Pen.Color := clGreen;
  	FLegendaGrid.Canvas.Brush.Color := clGreen;
  	FLegendaGrid.Canvas.Rectangle(p, 0, p + 5, 15);
    Inc(p, 10);
  end;

  if pFisicoMaiorReal then begin
  	FLegendaGrid.Canvas.Pen.Color := coCorFisicoMaiorReal;
  	FLegendaGrid.Canvas.Brush.Color := coCorFisicoMaiorReal;
  	FLegendaGrid.Canvas.Rectangle(p, 0, p + 5, 15);
    Inc(p, 10);
  end;

  if pFisicoMenorReal then begin
  	FLegendaGrid.Canvas.Pen.Color := coCorFisicoMenorReal;
  	FLegendaGrid.Canvas.Brush.Color := coCorFisicoMenorReal;
  	FLegendaGrid.Canvas.Rectangle(p, 0, p + 5, 15);
    Inc(p, 10);
  end;

  if pProducao then begin
  	FLegendaGrid.Canvas.Pen.Color := coCorProducao;
  	FLegendaGrid.Canvas.Brush.Color := coCorProducao;
  	FLegendaGrid.Canvas.Rectangle(p, 0, p + 5, 15);
    Inc(p, 10);
  end;

  Result := FLegendaGrid.Picture;
end;

procedure TFormPesquisaVendas.miProximafotoClick(Sender: TObject);
begin
  Inc(FIndexFoto);
  if FIndexFoto > 3 then begin
    FIndexFoto := 3;
    Exit;
  end;

  case FIndexFoto of
    2: begin
      if FFotos[FIndexFotosProdutoVetor].Foto2 = nil then begin
        FIndexFoto := 1;
        Exit;
      end;

      FrImagemProduto.setFoto(FFotos[FIndexFotosProdutoVetor].Foto2);
    end;

    3: begin
      if FFotos[FIndexFotosProdutoVetor].Foto3 = nil then begin
        FIndexFoto := 2;
        Exit;
      end;

      FrImagemProduto.setFoto(FFotos[FIndexFotosProdutoVetor].Foto3);
    end;
  end;
end;

procedure TFormPesquisaVendas.miAnteriorClick(Sender: TObject);
begin
  Dec(FIndexFoto);
  if FIndexFoto < 1 then begin
    FIndexFoto := 1;
    Exit;
  end;

  case FIndexFoto of
    2: FrImagemProduto.setFoto(FFotos[FIndexFotosProdutoVetor].Foto2);
    1: FrImagemProduto.setFoto(FFotos[FIndexFotosProdutoVetor].Foto1);
  end;
end;

procedure TFormPesquisaVendas.miLegendasClick(Sender: TObject);
var
  vLegendas: TFormLegendas;
begin
  inherited;
  vLegendas := TFormLegendas.Create(Self);

  vLegendas.AddLegenda(coCorPromocao, 'Em promo��o', clWhite);
  vLegendas.AddLegenda(corCorSemEstoque, 'Sem estoque', clWhite);
  vLegendas.AddLegenda(coCorProducao, 'Produ��o', clWhite);
  vLegendas.AddLegenda(coCorFisicoMaiorReal, 'Fisico > Real', clBlack);
  vLegendas.AddLegenda(coCorFisicoMenorReal, 'Fisico < Real', clWhite);

  vLegendas.Show;
end;

procedure TFormPesquisaVendas.pnInformacoesEstoque2Click(Sender: TObject);
begin
  inherited;
  Informacoes.EstoqueProdutos.Informar( SFormatInt(sgItens.Cells[coProdutoId, sgItens.Row]) );
end;

procedure TFormPesquisaVendas.sbInformacoesEstoqueClick(Sender: TObject);
begin
  inherited;
  Informacoes.EstoqueProdutos.Informar( SFormatInt(sgItens.Cells[coProdutoId, sgItens.Row]) );
end;

procedure TFormPesquisaVendas.sbTabelaPrecosClick(Sender: TObject);
begin
  inherited;
  if FrCondicaoPagamento.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio definir primeiramente uma condi��o de pagamento antes de continuar!');
    SetarFoco(FrCondicaoPagamento);
    Exit;
  end;

  TabelaPrecosProduto.Informar(
    SFormatInt(sgItens.Cells[coProdutoId, sgItens.Row]),
    Sessao.getEmpresaLogada.EmpresaId,
    FrCondicaoPagamento.getDados.condicao_id
  );
end;

procedure TFormPesquisaVendas.sgItensClick(Sender: TObject);
var
  i: Integer;
  quantidadeEmpresas: Double;

  procedure VerCondicoesPagamento(pCondicao: Integer; pIndice: Double; pLabelPreco: TLabel);
  begin
    pLabelPreco.Caption := '';
    if pCondicao > 0 then
      pLabelPreco.Caption := 'R$ ' + _Biblioteca.NFormatN( SFormatDouble(sgItens.Cells[coPrecoBase, sgItens.Row]) * pIndice );
  end;

begin
  inherited;
  if sgItens.Cells[coProdutoId, 1] = '' then
    Exit;

  lbCodigoBarras.Caption   := sgItens.Cells[coCodigoBarras, sgItens.Row];
  lbCodigoOriginal.Caption := sgItens.Cells[coCodigoOriginalFabr, sgItens.Row];

  VerCondicoesPagamento(Sessao.getParametrosEmpresa.CondicaoPagamentoConsProd2, Sessao.getParametrosEmpresa.IndiceCondPagtoCons2, lbPrecoCondicaoPagto2);
  VerCondicoesPagamento(Sessao.getParametrosEmpresa.CondicaoPagamentoConsProd2, Sessao.getParametrosEmpresa.IndiceCondPagtoCons3, lbPrecoCondicaoPagto3);
  VerCondicoesPagamento(Sessao.getParametrosEmpresa.CondicaoPagamentoConsProd2, Sessao.getParametrosEmpresa.IndiceCondPagtoCons4, lbPrecoCondicaoPagto4);

  FrImagemProduto.Clear;
  eCaracteristicas.Lines.Clear;
  eCaracteristicas.Lines.Add(sgItens.Cells[coCaracteristicas, sgItens.Row]);

  FIndexFoto := 1;
  FIndexFotosProdutoVetor := -1;
  for i := Low(FFotos) to High(FFotos) do begin
    if SFormatInt(sgItens.Cells[coProdutoId, sgItens.Row]) = FFotos[i].ProdutoId then begin
      FIndexFotosProdutoVetor := i;
      Break;
    end;
  end;

  if FIndexFotosProdutoVetor > -1 then
    FrImagemProduto.setFoto(FFotos[FIndexFotosProdutoVetor].Foto1);

   quantidadeEmpresas := _ProdutosVenda.BuscarEstoqueTodasEmpresaExcetoLogada(
     Sessao.getConexaoBanco,
     Sessao.getEmpresaLogada.EmpresaId,
     SFormatInt(sgItens.Cells[coProdutoId, sgItens.Row])
   );

   lbEstoqueTodasEmpresas.Caption := NFormatEstoque(quantidadeEmpresas)
end;

procedure TFormPesquisaVendas.sgItensDblClick(Sender: TObject);
begin
  inherited;
  if sgItens.Cells[coProdutoId, sgItens.Row] = '' then
    Exit;

  sgItens.Cells[coSelecionado, sgItens.Row] := _Biblioteca.charSelecionado;
  ModalResult := mrOk;
end;

procedure TFormPesquisaVendas.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    coProdutoId,
    cpPrecoUnitario,
    coEstoque,
    coPrecoPromocional]
  then
    vAlinhamento := taRightJustify
  else if ACol = coSelecionado then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormPesquisaVendas.sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgItens.Cells[coPrecoPromocional, ARow] <> '' then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end;

  if sgItens.Cells[coProducao, ARow] = 'S' then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao1;
    AFont.Color  := _Biblioteca.coCorFonteEdicao1;
  end;
end;

procedure TFormPesquisaVendas.sgItensGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;

  if ARow = 0 then
    Exit;

  if sgItens.Cells[coProdutoId, ARow] = '' then
    Exit;

  if ACol = coLegenda then begin
    APicture :=
      LengendaGrid(
        SFormatCurr(sgItens.Cells[coEstoque, ARow]) <= 0,
        sgItens.Cells[coPrecoPromocional, ARow] <> '',
        sgItens.Cells[coFisicoMaiorReal, ARow] = 'S',
        sgItens.Cells[coFisicoMenorReal, ARow] = 'S',
        sgItens.Cells[coProducao, ARow] = 'S'
      );
  end
  else if ACol = coSelecionado then begin
    if sgItens.Cells[ACol, ARow] = charNaoSelecionado then
      APicture := _Imagens.FormImagens.im1.Picture
    else if sgItens.Cells[ACol, ARow] = _Biblioteca.charSelecionado then
      APicture := _Imagens.FormImagens.im2.Picture;
  end;
end;

procedure TFormPesquisaVendas.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coProdutoId, cpPrecoUnitario, coEstoque] then
    vAlinhamento := taRightJustify
  else if ACOl = coSelecionado then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormPesquisaVendas.sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if SFormatCurr(sgItens.Cells[coEstoque, ARow]) <= 0 then
    AFont.Color := clRed;
end;

procedure TFormPesquisaVendas.sgPesquisaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if FApenasConsulta then
    Exit;

  if sgItens.Cells[coProdutoId, sgItens.Row] = '' then
    Exit;

  if Key = VK_RETURN then
    sgItensDblClick(Sender)
  else if Key = VK_SPACE then
    sgItens.Cells[coSelecionado, sgItens.Row] := IIfStr(sgItens.Cells[coSelecionado, sgItens.Row] = _Biblioteca.charSelecionado, charNaoSelecionado, _Biblioteca.charSelecionado);
end;

end.
