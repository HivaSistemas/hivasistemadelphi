unit ParametrosEstacao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  _HerancaCadastro, Vcl.Buttons, _ParametrosEstacoes, _Sessao, Vcl.ExtCtrls, Vcl.StdCtrls, EditLuka, _FrameHerancaPrincipal, _Biblioteca,
  _FrameHenrancaPesquisas, Frame.GerenciadoresCartoesTEF, CheckBoxLuka, _RecordsEspeciais;

type
  TFormParametrosEstacao = class(TFormHerancaCadastro)
    eEstacao: TEditLuka;
    lb1: TLabel;
    ckUtilizarTEF: TCheckBoxLuka;
    FrGerenciadorCartaoTEF: TFrGerenciadoresCartoesTEF;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ckUtilizarTEFClick(Sender: TObject);
  protected
    procedure BuscarRegistro; override;
    procedure ExcluirRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExibirLogs; override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

procedure TFormParametrosEstacao.BuscarRegistro;
var
  vPar: TArray<RecParametrosEstacoes>;
begin
  inherited;

  vPar := _ParametrosEstacoes.BuscarParametros(Sessao.getConexaoBanco, 0, [eEstacao.Text]);
  if vPar = nil then
    Exit;

  ckUtilizarTEF.CheckedStr := vPar[0].UtilizarTef;
  ckUtilizarTEFClick(nil);

  FrGerenciadorCartaoTEF.InserirDadoPorChave( vPar[0].GerenciadorCartaoId, False );
end;

procedure TFormParametrosEstacao.ckUtilizarTEFClick(Sender: TObject);
begin
  inherited;
  _Biblioteca.Habilitar([FrGerenciadorCartaoTEF], ckUtilizarTEF.Checked);
end;

procedure TFormParametrosEstacao.ExcluirRegistro;
begin
  inherited;

end;

procedure TFormParametrosEstacao.ExibirLogs;
begin
  inherited;

end;

procedure TFormParametrosEstacao.FormCreate(Sender: TObject);
begin
  inherited;
  eEstacao.Text := _Biblioteca.NomeComputador;
  BuscarRegistro;
end;

procedure TFormParametrosEstacao.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(ckUtilizarTEF);
end;

procedure TFormParametrosEstacao.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  vGerenciadorCartaoId: Integer;
begin
  inherited;

  vGerenciadorCartaoId := 0;
  if not FrGerenciadorCartaoTEF.EstaVazio then
    vGerenciadorCartaoId := FrGerenciadorCartaoTEF.GetGerenciadorCartaoTEF().gerenciador_cartao_id;

  vRetBanco := _ParametrosEstacoes.AtualizarParametro(Sessao.getConexaoBanco, eEstacao.Text, ckUtilizarTEF.CheckedStr, vGerenciadorCartaoId);
  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.RotinaSucesso;
  Abort;
end;

procedure TFormParametrosEstacao.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
