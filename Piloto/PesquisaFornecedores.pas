unit PesquisaFornecedores;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _Sessao, _Fornecedores,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _RecordsCadastros, _Biblioteca, System.Math,
  Vcl.ExtCtrls;

type
  TFormPesquisaFornecedores = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  protected
    procedure BuscarRegistros; override;
  end;

function PesquisarFornecedor(pRevenda: Boolean; pUsoConsumo: Boolean; pServico: Boolean): TObject;

implementation

{$R *.dfm}

{ TFormPesquisaFornecedores }

const
  cpRazaoSocial  = 2;
  cpNomeFantasia = 3;
  cpCPF_CNPJ     = 4;

var
  FRevenda: Boolean;
  FUsoConsumo: Boolean;
  FServico: Boolean;

function PesquisarFornecedor(pRevenda: Boolean; pUsoConsumo: Boolean; pServico: Boolean): TObject;
var
  r: TObject;
begin
  FRevenda    := pRevenda;
  FUsoConsumo := pUsoConsumo;
  FServico    := pServico;

  r := _HerancaPesquisas.Pesquisar(TFormPesquisaFornecedores, _Fornecedores.GetFiltros);
  if r = nil then
    Result := nil
  else
    Result := RecFornecedores(r);
end;

procedure TFormPesquisaFornecedores.BuscarRegistros;
var
  i: Integer;
  vFornecedores: TArray<RecFornecedores>;
begin
  inherited;

  vFornecedores :=
    _Fornecedores.BuscarFornecedores(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text],
      FRevenda,
      FUsoConsumo,
      FServico
    );

  if vFornecedores = nil then begin
    _Biblioteca.NenhumRegistro;
    SetarFoco(eValorPesquisa);
    Exit;
  end;

  FDados := TArray<TObject>(vFornecedores);

  for i := Low(vFornecedores) to High(vFornecedores) do begin
    sgPesquisa.Cells[coSelecionado, i + 1] := 'N�o';
    sgPesquisa.Cells[coCodigo, i + 1]      := NFormat(vFornecedores[i].cadastro.cadastro_id);
    sgPesquisa.Cells[cpNomeFantasia, i + 1] := vFornecedores[i].cadastro.nome_fantasia;
    sgPesquisa.Cells[cpRazaoSocial, i + 1]  := vFornecedores[i].cadastro.razao_social;
    sgPesquisa.Cells[cpCPF_CNPJ, i + 1]     := vFornecedores[i].cadastro.cpf_cnpj;
  end;

  sgPesquisa.RowCount := IfThen(Length(vFornecedores) = 1, 2, High(vFornecedores) + 2);
  sgPesquisa.SetFocus;
end;

procedure TFormPesquisaFornecedores.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = coCodigo then
    vAlinhamento := taRightJustify
  else if ACol = coSelecionado then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
