unit Impressao.Danfe;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, QuickRpt, QRCtrls,
  QRPDFFilt, grimgctrl, Data.DB, Datasnap.DBClient, qrBarCode, _RecordsNotasFiscais, Vcl.Imaging.jpeg;

type
  TFormImpressaoDanfe = class(TForm)
    QRPDFFilter1: TQRPDFFilter;
    qrDanfe: TQuickRep;
    qrbRecibo: TQRBand;
    QRPDFShape1: TQRPDFShape;
    QRLabel10: TQRLabel;
    QRShape101: TQRShape;
    qrlRecebemosDe1: TQRLabel;
    qrNumeroNotaDestacar: TQRLabel;
    qrSerieNotaDestacar: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel11: TQRLabel;
    QRPDFShape2: TQRPDFShape;
    QRPDFShape3: TQRPDFShape;
    QRShape1: TQRShape;
    qrNomeRazaoSocialDestcabecalho: TQRLabel;
    QRLabel3: TQRLabel;
    QRShape2: TQRShape;
    QRLabel7: TQRLabel;
    qrlbl1: TQRLabel;
    qrTotalNotaFiscalcapa: TQRLabel;
    qrbDadosDanfe: TQRChildBand;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRLabel17: TQRLabel;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape11: TQRShape;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    qrNaturezaOperacao: TQRLabel;
    qrInscricaoEstadualEmitente: TQRLabel;
    qrInscricaoEstadualSubstitutoEmitente: TQRLabel;
    qrCNPJEmitente: TQRLabel;
    qrLogoEmpresa: TQRImage;
    qriBarCode: TQRImage;
    QRLabel2: TQRLabel;
    qrChaveAcesso: TQRLabel;
    qrlMsgAutorizado: TQRLabel;
    qrlDescricao: TQRLabel;
    qrProtocoloNFe: TQRLabel;
    qrSerieNota: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    qrNumeroNota: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRShape102: TQRShape;
    qrEntradaSaida: TQRLabel;
    QRShape4: TQRShape;
    QRShape66: TQRShape;
    qriBarCodeContingencia: TQRImage;
    QRShape69: TQRShape;
    qrRazaoSocialEmpresa: TQRLabel;
    qrEnderecoEmpresa: TQRLabel;
    qrCidadeUF: TQRLabel;
    qrTelefoneEmail: TQRLabel;
    qrQtdePaginas: TQRLabel;
    qrNumeroFolha: TQRSysData;
    QRLabel6: TQRLabel;
    qrbEmitenteDestinatario: TQRChildBand;
    QRShape36: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel4: TQRLabel;
    QRShape12: TQRShape;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRShape21: TQRShape;
    QRShape22: TQRShape;
    QRShape23: TQRShape;
    QRShape25: TQRShape;
    QRShape26: TQRShape;
    QRShape27: TQRShape;
    QRShape28: TQRShape;
    QRShape29: TQRShape;
    QRShape30: TQRShape;
    QRShape31: TQRShape;
    QRShape32: TQRShape;
    QRShape33: TQRShape;
    QRShape34: TQRShape;
    QRShape35: TQRShape;
    QRShape37: TQRShape;
    QRShape38: TQRShape;
    QRShape39: TQRShape;
    QRShape42: TQRShape;
    QRShape43: TQRShape;
    QRShape44: TQRShape;
    QRShape45: TQRShape;
    QRShape46: TQRShape;
    QRShape47: TQRShape;
    QRShape48: TQRShape;
    QRShape49: TQRShape;
    QRShape50: TQRShape;
    QRShape51: TQRShape;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel61: TQRLabel;
    QRLabel62: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel64: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabel67: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel69: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel71: TQRLabel;
    QRLabel72: TQRLabel;
    qrNomeRazaoSocialDestinatario: TQRLabel;
    qrCPFCNPJDestinatario: TQRLabel;
    qrEnderecoDestinatario: TQRLabel;
    qrBairroDestinatario: TQRLabel;
    qrCEPDestinatario: TQRLabel;
    qrCidadeDestinatario: TQRLabel;
    qrTelefoneDestinatario: TQRLabel;
    qrUFDestinatario: TQRLabel;
    qrInscricaoEstadualDestinatario: TQRLabel;
    qrDataEmissao: TQRLabel;
    qrDataSaidaEntrada: TQRLabel;
    qrBaseICMS: TQRLabel;
    qrValorICMS: TQRLabel;
    qrBaseICMSST: TQRLabel;
    qrValorICMSST: TQRLabel;
    qrTotalProdutos: TQRLabel;
    qrValorFrete: TQRLabel;
    qrValorSeguro: TQRLabel;
    qrValorDesconto: TQRLabel;
    qrOutrasDespesas: TQRLabel;
    qrValorIPI: TQRLabel;
    qrTotalNotaFiscal: TQRLabel;
    qrNomeTransportadora: TQRLabel;
    qrCodigoANTT: TQRLabel;
    qrPlacaVeiculo: TQRLabel;
    qrUFVeiculo: TQRLabel;
    qrCPFCNPJTransportadora: TQRLabel;
    qrEnderecoTransportadora: TQRLabel;
    qrCidadeTransportadora: TQRLabel;
    qrUFTransportadora: TQRLabel;
    qrInscricaoEstadualTransp: TQRLabel;
    qrQuantidadeTransp: TQRLabel;
    qrEspecieTransportadora: TQRLabel;
    qrMarcaTransportadora: TQRLabel;
    qrNumeracaoTransportadora: TQRLabel;
    qrPesoBrutoTransp: TQRLabel;
    qrPesoLiquidoTransp: TQRLabel;
    qrHoraSaida: TQRLabel;
    qr6: TQRLabel;
    QRShape68: TQRShape;
    qr1: TQRLabel;
    qr2: TQRLabel;
    qr3: TQRLabel;
    qrbHeaderItens: TQRBand;
    QRLabel142: TQRLabel;
    QRLabel143: TQRLabel;
    QRLabel144: TQRLabel;
    lblCST: TQRLabel;
    QRLabel146: TQRLabel;
    QRLabel147: TQRLabel;
    QRLabel148: TQRLabel;
    QRLabel149: TQRLabel;
    QRLabel150: TQRLabel;
    QRLabel151: TQRLabel;
    QRLabel152: TQRLabel;
    QRLabel153: TQRLabel;
    QRLabel156: TQRLabel;
    QRLabel154: TQRLabel;
    QRLabel157: TQRLabel;
    QRShape10: TQRShape;
    QRShape24: TQRShape;
    QRShape40: TQRShape;
    QRShape41: TQRShape;
    QRShape57: TQRShape;
    QRShape58: TQRShape;
    QRShape59: TQRShape;
    QRShape60: TQRShape;
    QRShape61: TQRShape;
    QRShape62: TQRShape;
    QRShape63: TQRShape;
    QRShape64: TQRShape;
    QRShape65: TQRShape;
    QRShape67: TQRShape;
    qrbISSQN: TQRBand;
    QRShape56: TQRShape;
    qr11: TQRLabel;
    qr12: TQRLabel;
    qrDadosAdicionais: TQRMemo;
    rbDadosAdicionais: TQRShape;
    qrDataHoraImpressao: TQRLabel;
    qr4: TQRLabel;
    qrItensNota: TQRBand;
    qrs2: TQRShape;
    qrs3: TQRShape;
    qrs4: TQRShape;
    qrs5: TQRShape;
    qrs6: TQRShape;
    qrs7: TQRShape;
    qrs8: TQRShape;
    qrs9: TQRShape;
    qrs10: TQRShape;
    qrs11: TQRShape;
    qrs12: TQRShape;
    qrs13: TQRShape;
    qrs14: TQRShape;
    qrProdutoId: TQRLabel;
    qrNomeProduto: TQRLabel;
    qrNCM: TQRLabel;
    qrCST: TQRLabel;
    qrCFOP: TQRLabel;
    qrUnidade: TQRLabel;
    qrQuantidade: TQRLabel;
    qrValorUnitario: TQRLabel;
    qrValorTotalProduto: TQRLabel;
    qrBaseICMSProduto: TQRLabel;
    qrValorICMSProduto: TQRLabel;
    qrValorIPIProduto: TQRLabel;
    qrAliquotaICMS: TQRLabel;
    qrAliquotaIPI: TQRLabel;
    QRChildBand1: TQRChildBand;
    QRShape3: TQRShape;
    QRLabel9: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRShape52: TQRShape;
    QRShape53: TQRShape;
    QRShape54: TQRShape;
    QRShape55: TQRShape;
    QRShape70: TQRShape;
    QRShape71: TQRShape;
    QRShape72: TQRShape;
    QRShape73: TQRShape;
    QRShape74: TQRShape;
    QRShape75: TQRShape;
    QRShape76: TQRShape;
    QRShape77: TQRShape;
    QRShape78: TQRShape;
    QRShape79: TQRShape;
    QRShape80: TQRShape;
    QRShape81: TQRShape;
    QRShape82: TQRShape;
    QRShape83: TQRShape;
    QRShape84: TQRShape;
    QRShape85: TQRShape;
    QRShape86: TQRShape;
    QRShape87: TQRShape;
    QRShape88: TQRShape;
    QRShape89: TQRShape;
    QRShape90: TQRShape;
    QRShape91: TQRShape;
    QRShape92: TQRShape;
    QRShape93: TQRShape;
    QRShape94: TQRShape;
    QRShape95: TQRShape;
    QRShape96: TQRShape;
    QRShape97: TQRShape;
    QRShape98: TQRShape;
    QRShape99: TQRShape;
    QRShape100: TQRShape;
    QRShape103: TQRShape;
    QRLabel21: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel73: TQRLabel;
    QRLabel74: TQRLabel;
    QRLabel75: TQRLabel;
    QRLabel76: TQRLabel;
    QRLabel77: TQRLabel;
    QRLabel78: TQRLabel;
    QRLabel79: TQRLabel;
    QRLabel80: TQRLabel;
    QRLabel81: TQRLabel;
    QRLabel82: TQRLabel;
    QRLabel83: TQRLabel;
    QRLabel84: TQRLabel;
    QRLabel85: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabel89: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabel91: TQRLabel;
    QRLabel92: TQRLabel;
    QRLabel93: TQRLabel;
    QRLabel94: TQRLabel;
    QRLabel95: TQRLabel;
    QRLabel96: TQRLabel;
    QRLabel97: TQRLabel;
    QRLabel98: TQRLabel;
    QRLabel99: TQRLabel;
    QRLabel100: TQRLabel;
    QRLabel101: TQRLabel;
    QRLabel102: TQRLabel;
    QRLabel103: TQRLabel;
    QRLabel104: TQRLabel;
    QRLabel105: TQRLabel;
    QRLabel106: TQRLabel;
    QRLabel107: TQRLabel;
    QRLabel108: TQRLabel;
    QRLabel109: TQRLabel;
    QRLabel110: TQRLabel;
    QRLabel111: TQRLabel;
    QRLabel112: TQRLabel;
    QRLabel113: TQRLabel;
    QRLabel114: TQRLabel;
    QRLabel115: TQRLabel;
    QRLabel116: TQRLabel;
    QRLabel117: TQRLabel;
    QRLabel118: TQRLabel;
    QRLabel119: TQRLabel;
    QRLabel120: TQRLabel;
    QRLabel121: TQRLabel;
    QRLabel122: TQRLabel;
    QRLabel123: TQRLabel;
    QRLabel124: TQRLabel;
    QRLabel125: TQRLabel;
    QRLabel126: TQRLabel;
    QRLabel127: TQRLabel;
    QRLabel128: TQRLabel;
    QRLabel129: TQRLabel;
    QRLabel130: TQRLabel;
    QRLabel131: TQRLabel;
    QRLabel132: TQRLabel;
    QRLabel133: TQRLabel;
    QRLabel134: TQRLabel;
    QRLabel135: TQRLabel;
    QRLabel136: TQRLabel;
    QRLabel137: TQRLabel;
    QRLabel138: TQRLabel;
    QRLabel139: TQRLabel;
    QRLabel140: TQRLabel;
    QRLabel141: TQRLabel;
    QRLabel145: TQRLabel;
    QRLabel155: TQRLabel;
    QRLabel158: TQRLabel;
    QRLabel159: TQRLabel;
    QRShape104: TQRShape;
    QRLabel160: TQRLabel;
    QRLabel161: TQRLabel;
    QRLabel162: TQRLabel;
    qrSemValorFiscal: TQRLabel;
    procedure qrDanfeNeedData(Sender: TObject; var MoreData: Boolean);
    procedure qrDanfeBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure qrbDadosDanfeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure qrItensNotaBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure qrNumeroFolhaPrint(sender: TObject; var Value: string);
  private
    FPosicao: Integer;
    FItensNota: TArray<RecNotaFiscalItem>;
    FRegimeTributario: string;
  public
    { Public declarations }
  end;

function ImprimirDanfe(
  pDadosNota: TArray<RecNotaFiscal>;
  pItensNota: TArray<RecNotaFiscalItem>;
  pCaminhoPDF: string = ''
): Boolean;

implementation

{$R *.dfm}

uses _Sessao, _NotasFiscais, _RecordsCadastros,
  _Biblioteca, _RecordsFinanceiros, _ContasReceber;

function ImprimirDanfe(
  pDadosNota: TArray<RecNotaFiscal>;
  pItensNota: TArray<RecNotaFiscalItem>;
  pCaminhoPDF: string = ''
): Boolean;
var
  vForm: TFormImpressaoDanfe;
  vImpressora: RecImpressora;
  vImagem: TJPEGImage;
  vTelefone: string;

  procedure PreencherCabecalho;

    procedure setBarCode;
    var
      vBarCode :TBarCode128c;
    begin
      vBarCode := TBarCode128c.Create;
      vBarCode.Code := _Biblioteca.RetornaNumeros(pDadosNota[0].chave_nfe);
      vBarCode.PaintCodeToCanvas(vBarCode.Code, vForm.qriBarCode.Canvas, vForm.qriBarCode.ClientRect);
      vBarCode.free;
    end;

    procedure setChaveAcesso;
    var
      i: Integer;
      chave: string;
      posic: Integer;
    begin
      posic := 0;
      vForm.qrChaveAcesso.Caption := '';
      chave := _Biblioteca.RetornaNumeros(pDadosNota[0].chave_nfe);
      for i := 1 to Length(chave) do begin
        vForm.qrChaveAcesso.Caption := vForm.qrChaveAcesso.Caption + chave[i];
        Inc(posic);

        if posic = 4 then begin
          vForm.qrChaveAcesso.Caption := vForm.qrChaveAcesso.Caption + ' ';
          posic := 0;
        end;
      end;
    end;

  begin
    if Sessao.getParametrosEmpresa.LogoEmpresa <> nil then begin
      try
        try
          vImagem := nil;
          Sessao.getParametrosEmpresa.LogoEmpresa.Position := 0;
          vImagem := TJPEGImage.Create;
          vImagem.LoadFromStream(Sessao.getParametrosEmpresa.LogoEmpresa);
          vForm.qrLogoEmpresa.Picture.Graphic := vImagem;
        except

        end;
      finally
        vImagem.Free;
      end;
    end;

    vForm.qrlRecebemosDe1.Caption                       := Format(vForm.qrlRecebemosDe1.Caption, [pDadosNota[0].razao_social_emitente]);
    vForm.qrRazaoSocialEmpresa.Caption                  := pDadosNota[0].razao_social_emitente;
    vForm.qrEnderecoEmpresa.Caption                     := pDadosNota[0].logradouro_emitente + ' ' + pDadosNota[0].complemento_emitente + ' ' + pDadosNota[0].numero_emitente + ' BAIRRO ' + pDadosNota[0].NOME_BAIRRO_EMITENTE;
    vForm.qrCidadeUF.Caption                            := pDadosNota[0].nome_cidade_emitente + ' / ' + pDadosNota[0].estado_id_emitente;

    vTelefone := 'Telefone: ' + pDadosNota[0].telefone_emitente;
    if (pDadosNota[0].TelefoneWhatsapp <> '(  )     -    ') and (pDadosNota[0].TelefoneWhatsapp <> '(  )      -    ') then
      vTelefone := vTelefone + #13 + 'Whatsapp: ' + pDadosNota[0].TelefoneWhatsapp;

    vForm.qrTelefoneEmail.Caption                       := vTelefone;
    vForm.qrNumeroNotaDestacar.Caption                  := FormatFloat('000,000,000', pDadosNota[0].numero_nota);
    vForm.qrSerieNotaDestacar.Caption                   := pDadosNota[0].SerieNota;
    vForm.qrNumeroNota.Caption                          := FormatFloat('000,000,000', pDadosNota[0].numero_nota);
    vForm.qrSerieNota.Caption                           := pDadosNota[0].SerieNota;

    if pDadosNota[0].status = 'E' then begin
      SetBarCode;
      setChaveAcesso;
    end;

    if (Copy(pDadosNota[0].CfopId, 1, 1) = '1') or (Copy(pDadosNota[0].CfopId, 1, 1) = '2') then
      vForm.qrEntradaSaida.Caption                        := '0'  //Entrada
    else
      vForm.qrEntradaSaida.Caption                        := '1'; //Sa�da

    vForm.qrNaturezaOperacao.Caption                    := pDadosNota[0].natureza_operacao;
    if pDadosNota[0].status = 'E' then
      vForm.qrProtocoloNFe.Caption                        := pDadosNota[0].protocolo_nfe + ' ' + FormatDateTime('dd/mm/yyyy hh:nn:ss', pDadosNota[0].data_hora_protocolo_nfe)
    else
      vForm.qrProtocoloNFe.Caption := '';

    vForm.qrInscricaoEstadualEmitente.Caption           := pDadosNota[0].inscricao_estadual_emitente;
    vForm.qrInscricaoEstadualSubstitutoEmitente.Caption := ''; // IE Substituto Tribut�rio
    vForm.qrCNPJEmitente.Caption                        := pDadosNota[0].cnpj_emitente;

    vForm.qrNomeRazaoSocialDestinatario.Caption := pDadosNota[0].razao_social_destinatario;
    vForm.qrNomeRazaoSocialDestcabecalho.Caption := pDadosNota[0].razao_social_destinatario;

    if pDadosNota[0].status = 'E' then begin
      vForm.qrDataEmissao.Caption                 := FormatDateTime('dd/mm/yyyy', pDadosNota[0].data_hora_emissao);
      vForm.qrDataSaidaEntrada.Caption            := FormatDateTime('dd/mm/yyyy', pDadosNota[0].data_hora_emissao);
      vForm.qrHoraSaida.Caption                   := FormatDateTime('hh:nn:ss', pDadosNota[0].data_hora_emissao);
    end
    else begin
      vForm.qrDataEmissao.Caption                 := FormatDateTime('dd/mm/yyyy', pDadosNota[0].data_hora_cadastro);
      vForm.qrDataSaidaEntrada.Caption            := FormatDateTime('dd/mm/yyyy', pDadosNota[0].data_hora_cadastro);
      vForm.qrHoraSaida.Caption                   := FormatDateTime('hh:nn:ss', pDadosNota[0].data_hora_cadastro);
    end;
  end;

  procedure PreencherDadosDestinatario;
  begin
    vForm.qrCPFCNPJDestinatario.Caption           := pDadosNota[0].cpf_cnpj_destinatario;
    vForm.qrEnderecoDestinatario.Caption          := pDadosNota[0].logradouro_destinatario + ' ' + pDadosNota[0].complemento_destinatario;
    vForm.qrBairroDestinatario.Caption            := pDadosNota[0].nome_bairro_destinatario;
    vForm.qrCepDestinatario.Caption               := pDadosNota[0].cep_destinatario;
    vForm.qrCidadeDestinatario.Caption            := pDadosNota[0].nome_cidade_destinatario;
    vForm.qrTelefoneDestinatario.Caption          := pDadosNota[0].TelefoneDestinatario;
    vForm.qrUFDestinatario.Caption                := pDadosNota[0].estado_id_destinatario;
    vForm.qrInscricaoEstadualDestinatario.Caption := pDadosNota[0].inscricao_estadual_destinat;
  end;

  procedure PreencherImpostos;
  begin
    vForm.qrBaseICMS.Caption        := FormatFloat('#0.00', pDadosNota[0].base_calculo_icms);
    vForm.qrValorICMS.Caption       := FormatFloat('#0.00', pDadosNota[0].valor_icms);
    vForm.qrBaseICMSST.Caption      := FormatFloat('#0.00', pDadosNota[0].base_calculo_icms_st);
    vForm.qrValorICMSST.Caption     := FormatFloat('#0.00', pDadosNota[0].valor_icms_st);
    vForm.qrTotalProdutos.Caption   := FormatFloat('#0.00', pDadosNota[0].valor_total_produtos);
    vForm.qrValorFrete.Caption      := FormatFloat('#0.00', pDadosNota[0].valor_frete);
    vForm.qrValorSeguro.Caption     := FormatFloat('#0.00', pDadosNota[0].valor_seguro);
    vForm.qrValorDesconto.Caption   := FormatFloat('#0.00', pDadosNota[0].valor_desconto);
    vForm.qrOutrasDespesas.Caption  := FormatFloat('#0.00', pDadosNota[0].valor_outras_despesas);
    vForm.qrValorIPI.Caption        := FormatFloat('#0.00', pDadosNota[0].valor_ipi);
    vForm.qrTotalNotaFiscal.Caption := FormatFloat('#0.00', pDadosNota[0].valor_total);
    vForm.qrTotalNotaFiscalcapa.Caption := FormatFloat('#0.00', pDadosNota[0].valor_total);
  end;

  procedure PreencherTransporte;
  begin
    vForm.qrNomeTransportadora.Caption      := pDadosNota[0].razao_transportadora;
    vForm.qrCodigoANTT.Caption              := '';
    vForm.qrPlacaVeiculo.Caption            := '';
    vForm.qrEnderecoTransportadora.Caption  := pDadosNota[0].endereco_transportadora;
    vForm.qrCidadeTransportadora.Caption    := pDadosNota[0].cidade_transportadora;
    vForm.qrUFTransportadora.Caption        := pDadosNota[0].uf_transportadora;
    vForm.qrInscricaoEstadualTransp.Caption := pDadosNota[0].ie_transportadora;
    vForm.qrUFVeiculo.Caption               := '';
    vForm.qrCPFCNPJTransportadora.Caption   := pDadosNota[0].cnpj_transportadora;
    vForm.qrQuantidadeTransp.Caption        := FormatFloat('#0.00', pDadosNota[0].QuantidadeNFe);
    vForm.qrEspecieTransportadora.Caption   := pDadosNota[0].EspecieNFe;
    vForm.qrMarcaTransportadora.Caption     := '';
    vForm.qrNumeracaoTransportadora.Caption := '';
    vForm.qrPesoBrutoTransp.Caption         := FormatFloat('#0.000', pDadosNota[0].peso_bruto);
    vForm.qrPesoLiquidoTransp.Caption       := FormatFloat('#0.000', pDadosNota[0].peso_liquido);
    vForm.qr3.Caption                       := IIf((pDadosNota[0].tipo_frete = 0) or (pDadosNota[0].tipo_frete = 9),'1','2');
  end;

  procedure PreencherISSQN;
  begin
 //   vForm.qrInscricaoMunicipal.Caption := '';
  //  vForm.qrTotalServicos.Caption      := '';
 //   vForm.qrBaseISSQN.Caption          := '';
  //  vForm.qrValorISSQN.Caption         := '';
  end;

  procedure PreencherInformacoesAdicionais;
  var
    vReceber: TArray<RecContasReceber>;
    i: Integer;
    complementoFinanceiro: string;
    comandoFinal: string;
  begin
    if (pDadosNota[0].AcumuladoId > 0) then begin
      comandoFinal :=
        '  where COR.ACUMULADO_ID = ' + IntToStr(pDadosNota[0].AcumuladoId) + '' +
        '  and TCO.FORMA_PAGAMENTO <> ''CRT'' ' +
        '  order by COR.RECEBER_ID ';
    end
    else begin
      comandoFinal :=
        '  where COR.ORCAMENTO_ID = ' + IntToStr(pDadosNota[0].orcamento_id) + '' +
        '  and TCO.FORMA_PAGAMENTO <> ''CRT'' ' +
        '  order by COR.RECEBER_ID ';
    end;

    complementoFinanceiro := '';
    vReceber := _ContasReceber.BuscarContasReceberComando(Sessao.getConexaoBanco, comandoFinal);
    for i := Low(vReceber) to High(vReceber) do begin
      if complementoFinanceiro <> '' then
        complementoFinanceiro := complementoFinanceiro + ' - ';

      complementoFinanceiro :=
        complementoFinanceiro + ' Parc.: ' +
        NFormat(vReceber[i].Parcela) + '/' +
        NFormat(vReceber[i].NumeroParcelas) + ' Venc.: ' +
        DFormat(vReceber[i].data_vencimento) + ' R$ ' +
        NFormat(vReceber[i].ValorDocumento);
    end;

    vForm.qrDadosAdicionais.Lines.Text := pDadosNota[0].informacoes_complementares + IIfStr(complementoFinanceiro <> '', ''#$D#$A'T�tulos:' + complementoFinanceiro);
    vForm.qrDataHoraImpressao.Caption  := FormatDateTime('dd/mm/yyyy hh:nn:ss', Now);
   // vForm.qrSistema.Caption            := Application.Title;
  end;

begin
  Result := False;
  if pDadosNota = nil then begin
    _Biblioteca.Exclamar('Dados da nota fiscal n�o encontrado!');
    Exit;
  end;

  vImpressora := Sessao.getImpressora( toNFe );
  if vImpressora.CaminhoImpressora = '' then begin
    _Biblioteca.ImpressoraNaoConfigurada;
    Exit;
  end;
  vForm := TFormImpressaoDanfe.Create(Application);

  vForm.qrSemValorFiscal.Enabled := pDadosNota[0].status <> 'E';

  PreencherCabecalho;
  PreencherDadosDestinatario;
  PreencherImpostos;
  PreencherTransporte;
  PreencherISSQN;
  PreencherInformacoesAdicionais;

  vForm.FItensNota := pItensNota;
  vForm.FRegimeTributario := pDadosNota[0].regime_tributario;

  vForm.qrDanfe.Prepare;
  vForm.qrQtdePaginas.Caption := '/' + LPad(NFormat(vForm.qrDanfe.QRPrinter.PageCount), 2, '0');

  if pCaminhoPDF <> '' then
    vForm.qrDanfe.ExportToFilter(TQRPDFDocumentFilter.Create(pCaminhoPDF + 'nota_' + IntToStr(pDadosNota[0].numero_nota) + '.pdf'));

  vForm.qrDanfe.PrinterSettings.PrinterIndex := vImpressora.getIndex;
  if vImpressora.AbrirPreview then
    vForm.qrDanfe.PreviewModal
  else
    vForm.qrDanfe.Print;

  FreeAndNil(vForm);
  Result := True;
end;

procedure TFormImpressaoDanfe.qrbDadosDanfeBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  qrNumeroFolha.Caption := _Biblioteca.LPad(IntToStr(qrDanfe.PageNumber), 2, '0') + '/' + _Biblioteca.LPad(IntToStr(qrDanfe.QRPrinter.PageCount), 2, '0');
end;

procedure TFormImpressaoDanfe.qrDanfeBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  FPosicao := -1;
end;

procedure TFormImpressaoDanfe.qrDanfeNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  Inc(FPosicao);
  MoreData := FPosicao < Length(FItensNota);
end;

procedure TFormImpressaoDanfe.qrItensNotaBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  qrProdutoId.Caption         := IIfStr(FItensNota[FPosicao].CodigoProdutoNFe <> '', FItensNota[FPosicao].CodigoProdutoNFe, IntToStr(FItensNota[FPosicao].produto_id));
  qrNomeProduto.Caption       := FItensNota[FPosicao].nome_produto;
  qrNCM.Caption               := FItensNota[FPosicao].codigo_ncm;

  if FRegimeTributario = 'SN' then begin
    if (Em(FItensNota[FPosicao].cst, ['00', '20'])) and (FItensNota[FPosicao].valor_icms > 0) then
      qrCST.Caption := '101'
    else if Em(FItensNota[FPosicao].cst, ['40', '41', '50', '51']) then
      qrCST.Caption := '400'
    else if (Em(FItensNota[FPosicao].cst, ['00', '20'])) then
      qrCST.Caption := '102'
    else if (Em(FItensNota[FPosicao].cst, ['10', '70'])) then
      qrCST.Caption := '201'
    else if FItensNota[FPosicao].cst = '30' then
      qrCST.Caption := '202'
    else if FItensNota[FPosicao].cst = '60' then
      qrCST.Caption := '500'
    else if FItensNota[FPosicao].cst = '90' then
      qrCST.Caption := '900';
  end
  else
    qrCST.Caption               := FItensNota[FPosicao].cst;

  qrCFOP.Caption              := Copy(FItensNota[FPosicao].CfopId, 1, 4);
  qrUnidade.Caption           := FItensNota[FPosicao].unidade;
  qrQuantidade.Caption        := _Biblioteca.NFormat(FItensNota[FPosicao].quantidade);
  qrValorUnitario.Caption     := _Biblioteca.NFormat(FItensNota[FPosicao].preco_unitario);
  qrValorTotalProduto.Caption := _Biblioteca.NFormat(FItensNota[FPosicao].valor_total);
  qrBaseICMSProduto.Caption   := _Biblioteca.NFormat(FItensNota[FPosicao].base_calculo_icms);
  qrValorICMSProduto.Caption  := _Biblioteca.NFormat(FItensNota[FPosicao].valor_icms);
  qrValorIPIProduto.Caption   := _Biblioteca.NFormat(FItensNota[FPosicao].valor_ipi);
  qrAliquotaICMS.Caption      := _Biblioteca.NFormat(FItensNota[FPosicao].percentual_icms);
  qrAliquotaIPI.Caption       := _Biblioteca.NFormat(FItensNota[FPosicao].percentual_ipi);
end;

procedure TFormImpressaoDanfe.qrNumeroFolhaPrint(sender: TObject;
  var Value: string);
begin
  Value := LPad(Value, 2, '0');
end;

end.
