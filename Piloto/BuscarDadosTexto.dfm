inherited FormBuscarDadosTexto: TFormBuscarDadosTexto
  Caption = 'T'#237'tulo'
  ClientHeight = 89
  ClientWidth = 167
  ExplicitWidth = 173
  ExplicitHeight = 118
  PixelsPerInch = 96
  TextHeight = 14
  object lbDados: TLabel [0]
    Left = 8
    Top = 8
    Width = 64
    Height = 14
    Caption = 'C'#243'digo NCM'
  end
  inherited pnOpcoes: TPanel
    Top = 52
    Width = 167
    inherited sbFinalizar: TSpeedButton
      Width = 156
      ExplicitWidth = 156
    end
  end
  object eDados: TEditLuka
    Left = 8
    Top = 22
    Width = 151
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 8
    NumbersOnly = True
    TabOrder = 1
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
