unit Relacao.MovimentosFinanceiros;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl,
  Vcl.ComCtrls, Vcl.StdCtrls, CheckBoxLuka, Vcl.Buttons, Vcl.ExtCtrls,
  FrameDataInicialFinal, _FrameHerancaPrincipal, _FrameHenrancaPesquisas,
  FrameEmpresas, Vcl.Grids, GridLuka, _Biblioteca, ComboBoxLuka, GroupBoxLuka,
  Frame.Cadastros;

type
  TFormMovimentosFinanceiros = class(TFormHerancaRelatoriosPageControl)
    FrEmpresas: TFrEmpresas;
    FrDataAbertura: TFrDataInicialFinal;
    sgMovimentacoes: TGridLuka;
    lb1: TLabel;
    cbAgrupamento: TComboBoxLuka;
    gbFormasPagamento: TGroupBoxLuka;
    ckCartao: TCheckBox;
    ckFinanceira: TCheckBox;
    ckCheque: TCheckBox;
    ckCobranca: TCheckBox;
    ckAcumulado: TCheckBox;
    ckDinheiro: TCheckBoxLuka;
    ckPix: TCheckBox;
    SpeedButton2: TSpeedButton;
    FrCadastros: TFrameCadastros;
    gbTipoMovimento: TGroupBoxLuka;
    ckRecebimentoAcumulados: TCheckBoxLuka;
    ckBaixaContasReceber: TCheckBoxLuka;
    ckRecebimentoVenda: TCheckBoxLuka;
    ckBaixaContasPagar: TCheckBoxLuka;
    procedure sgMovimentacoesDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgMovimentacoesGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure FormShow(Sender: TObject);
    procedure sbImprimirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    procedure Carregar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

var
  FormMovimentosFinanceiros: TFormMovimentosFinanceiros;

implementation

{$R *.dfm}

uses _MovimentosFinanceiros, _Conexao, _Sessao, _ContasReceberBaixasItens,
  _RecordsCadastros, _RecordsEspeciais, _RecordsFinanceiros,
  _ContasPagarBaixasItens;

const
  coId                 =  0;
  coTipoMovimentoAna   =  1;  // Anal�tico
  coCliente            =  2;
  coValorDinheiro      =  3;
  coValorCheque        =  4;
  coValorCartao        =  5;
  coValorCobranca      =  6;
  coValorAcumulado     =  7;
  coValorCredito       =  8;
  coValorPix           =  9;
  coValorTotal         =  10;
  coContaOrigDestino   =  11;
  coDataHoraMovimento  =  12;
  coTipoMovimentoSint  =  13;

{ TFormMovimentosFinanceiros }

procedure TFormMovimentosFinanceiros.Carregar(Sender: TObject);
var
  i: Integer;
  j: Integer;
  vLinha: Integer;
  vSql, vSqlFormasPagto: string;
  vMovimentos: TArray<RecMovimentosFinanceiros>;
  vMovimentosProvisorio: TArray<RecMovimentosFinanceiros>;
  vClienteIdAtual: Integer;
  vClienteIdsUtilizados: TArray<Integer>;
  vItensBaixa: TArray<RecContaReceberBaixaItem>;
  vItensBaixaPagar: TArray<RecContasPagarBaixasItens>;
  clienteIdAnterior: Integer;
  multiplosClientes: Boolean;
  clienteId: Integer;
  clienteNome: string;
  cadastros: TArray<Integer>;
  idxCadastros: Integer;
  pularMovimento: Boolean;

  vValorTotalDinheiro: Currency;
  vValorTotalCheque: Currency;
  vValorTotalCartao: Currency;
  vValorTotalCobranca: Currency;
  vValorTotalAcumulado: Currency;
  vValorTotalCredito: Currency;
  vValorTotalPix: Currency;

  vValorTotalDinheiroEnt: Currency;
  vValorTotalChequeEnt: Currency;
  vValorTotalCartaoEnt: Currency;
  vValorTotalCobrancaEnt: Currency;
  vValorTotalAcumuladoEnt: Currency;
  vValorTotalCreditoEnt: Currency;
  vValorTotalPixEnt: Currency;

  vValorTotalDinheiroSai: Currency;
  vValorTotalChequeSai: Currency;
  vValorTotalCartaoSai: Currency;
  vValorTotalCobrancaSai: Currency;
  vValorTotalAcumuladoSai: Currency;
  vValorTotalCreditoSai: Currency;
  vValorTotalPixSai: Currency;


  procedure SqlFormaPagamento(pColuna: string; pCheckBox: TCheckBox);
  begin
    if pCheckBox.Checked then
      AddOrSeNecessario(vSqlFormasPagto, ' ' + pColuna + ' > 0');
  end;
begin
  inherited;
  vSql := '';
  vLinha := 1;
  sgMovimentacoes.ClearGrid();

  if not FrEmpresas.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrEmpresas.getSqlFiltros('MOV.EMPRESA_ID'));

  if not FrDataAbertura.NenhumaDataValida then
    _Biblioteca.WhereOuAnd(vSql, FrDataAbertura.getSqlFiltros('trunc(MOV.DATA_HORA_MOVIMENTO)'));

  _Biblioteca.WhereOuAnd(vSql, gbTipoMovimento.GetSql('MOV.TIPO_MOVIMENTO'));

  vSql := vSql + ' and MOV.TIPO_MOVIMENTO not in (''ABE'',''SUP'',''FEC'',''SAN'') ';

  if (ckDinheiro.Checked) or (ckPix.Checked) or (ckCheque.Checked) or (ckCartao.Checked) or (ckFinanceira.Checked) or
     (ckCobranca.Checked) or (ckAcumulado.Checked) then
  begin
    SqlFormaPagamento('MOV.VALOR_DINHEIRO', ckDinheiro);
    SqlFormaPagamento('MOV.VALOR_PIX', ckPix);
    SqlFormaPagamento('MOV.VALOR_CHEQUE', ckCheque);
    SqlFormaPagamento('MOV.VALOR_CARTAO', ckCartao);
    SqlFormaPagamento('MOV.VALOR_CARTAO', ckCartao);
    SqlFormaPagamento('MOV.VALOR_FINANCEIRA', ckFinanceira);
    SqlFormaPagamento('MOV.VALOR_COBRANCA + MOV.VALOR_CREDITO', ckCobranca);
    SqlFormaPagamento('MOV.VALOR_ACUMULADO', ckAcumulado);
    vSql := vSql + ' and ( ' + vSqlFormasPagto + ' )';
  end;

  if cbAgrupamento.GetValor <> 'NEN' then
    vSql := vSql + ' order by MOV.TIPO_MOVIMENTO, MOV.DATA_HORA_MOVIMENTO '
  else
    vSql := vSql + ' order by MOV.DATA_HORA_MOVIMENTO ';

  vMovimentos := _MovimentosFinanceiros.BuscarMovimentosFinanceirosComando(Sessao.getConexaoBanco, vSql);
  if vMovimentos = nil then begin
    NenhumRegistro;
    Exit;
  end;

  if not FrCadastros.EstaVazio then
    cadastros := FrCadastros.GetArrayOfInteger;

  for I := Low(vMovimentos) to High(vMovimentos) do begin
    multiplosClientes := false;
    clienteId := 0;
    if vMovimentos[i].TipoMovimento = 'BXR' then begin

      vItensBaixa := _ContasReceberBaixasItens.BuscarContaReceberBaixaItens(Sessao.getConexaoBanco, 0, [vMovimentos[i].id]);
      if vItensBaixa <> nil then begin

        clienteIdAnterior := 0;
        multiplosClientes := false;
        for j := Low(vItensBaixa) to High(vItensBaixa) do begin
          if (clienteIdAnterior <> 0) and (clienteIdAnterior <> vItensBaixa[j].CadastroId) then begin
            multiplosClientes := True;
            clienteNome := 'BAIXA AGRUPADA';
            Break;
          end;

          clienteIdAnterior := vItensBaixa[j].CadastroId;
          clienteId := vItensBaixa[j].CadastroId;
          clienteNome := vItensBaixa[j].nome_cliente;
        end;

        if multiplosClientes then begin
          vMovimentos[i].ClienteId  := 0;
          vMovimentos[i].ClienteNome := 'BAIXA AGRUPADA';
        end
        else begin
          vMovimentos[i].ClienteId  := clienteId;
          vMovimentos[i].ClienteNome := clienteNome;
        end;
      end;
    end
    else if vMovimentos[i].TipoMovimento = 'BXP' then begin

      vItensBaixaPagar := _ContasPagarBaixasItens.BuscarContasPagarBaixasItenss(Sessao.getConexaoBanco, 0, [vMovimentos[i].id]);
      if vItensBaixaPagar <> nil then begin

        clienteIdAnterior := 0;
        multiplosClientes := false;
        for j := Low(vItensBaixaPagar) to High(vItensBaixaPagar) do begin
          if (clienteIdAnterior <> 0) and (clienteIdAnterior <> vItensBaixaPagar[j].CadastroId) then begin
            multiplosClientes := True;
            clienteNome := 'BAIXA AGRUPADA';
            Break;
          end;

          clienteIdAnterior := vItensBaixaPagar[j].CadastroId;
          clienteId := vItensBaixaPagar[j].CadastroId;
          clienteNome := vItensBaixaPagar[j].NomeFornecedor;
        end;

        if multiplosClientes then begin
          vMovimentos[i].ClienteId  := 0;
          vMovimentos[i].ClienteNome := 'BAIXA AGRUPADA';
        end
        else begin
          vMovimentos[i].ClienteId  := clienteId;
          vMovimentos[i].ClienteNome := clienteNome;
        end;
      end;
    end;
  end;

  if cbAgrupamento.GetValor = 'CAD' then begin
    vClienteIdAtual := -1;
    for i := Low(vMovimentos) to High(vMovimentos) do begin
      vClienteIdAtual := vMovimentos[i].ClienteId;
      if (vClienteIdsUtilizados <> nil) then begin
        for j := Low(vClienteIdsUtilizados) to High(vClienteIdsUtilizados) do begin
          if vClienteIdAtual = vClienteIdsUtilizados[j] then begin
            vClienteIdAtual := -1;
            Break;
          end;
        end;
      end;

      if vClienteIdAtual = -1 then
        Continue;

      SetLength(vClienteIdsUtilizados, Length(vClienteIdsUtilizados) + 1);
      vClienteIdsUtilizados[High(vClienteIdsUtilizados)] := vClienteIdAtual;
      for j := Low(vMovimentos) to High(vMovimentos) do begin
        if vMovimentos[j].ClienteId = vClienteIdAtual then begin
          SetLength(vMovimentosProvisorio, Length(vMovimentosProvisorio) + 1);
          vMovimentosProvisorio[High(vMovimentosProvisorio)] := vMovimentos[j];
        end;
      end;
    end;

    vMovimentos := vMovimentosProvisorio;
  end;

  vLinha := 0;
  for i := Low(vMovimentos) to High(vMovimentos) do begin

    if (vMovimentos[i].ClienteId = 0) and (not FrCadastros.EstaVazio) then
      Continue;

    pularMovimento := False;
    if (cadastros <> nil) then begin
      pularMovimento := true;
      for idxCadastros := Low(cadastros) to High(cadastros) do begin
        if vMovimentos[i].ClienteId = cadastros[idxCadastros] then
          pularMovimento := false;
      end;
    end;

    if pularMovimento then
      Continue;

    vLinha := vLinha + 1;

    sgMovimentacoes.Cells[coId, vLinha]                := NFormat(vMovimentos[i].Id);
    sgMovimentacoes.Cells[coTipoMovimentoAna, vLinha]  := vMovimentos[i].TipoMovimentoAnalitico;
    sgMovimentacoes.Cells[coTipoMovimentoSint, vLinha] := vMovimentos[i].TipoMovimento;
    sgMovimentacoes.Cells[coCliente, vLinha]           := IntToStr(vMovimentos[i].ClienteId) + ' - ' + vMovimentos[i].ClienteNome;
    sgMovimentacoes.Cells[coValorDinheiro, vLinha]     := NFormatN(vMovimentos[i].ValorDinheiro);
    sgMovimentacoes.Cells[coValorCheque, vLinha]       := NFormatN(vMovimentos[i].ValorCheque);
    sgMovimentacoes.Cells[coValorCartao, vLinha]       := NFormatN(vMovimentos[i].ValorCartao);
    sgMovimentacoes.Cells[coValorCobranca, vLinha]     := NFormatN(vMovimentos[i].ValorCobranca);
    sgMovimentacoes.Cells[coValorAcumulado, vLinha]    := NFormatN(vMovimentos[i].ValorAcumulado);
    sgMovimentacoes.Cells[coValorCredito, vLinha]      := NFormatN(vMovimentos[i].ValorCredito);
    sgMovimentacoes.Cells[coValorPix, vLinha]          := NFormatN(vMovimentos[i].ValorPix);

    sgMovimentacoes.Cells[coValorTotal, vLinha] :=
      NFormatN(
        vMovimentos[i].ValorDinheiro +
        vMovimentos[i].ValorCheque +
        vMovimentos[i].ValorCartao +
        vMovimentos[i].ValorCobranca +
        vMovimentos[i].ValorAcumulado +
        vMovimentos[i].ValorCredito +
        vMovimentos[i].ValorPix
      );

    sgMovimentacoes.Cells[coContaOrigDestino, vLinha]  := getInformacao(vMovimentos[i].ContaId, vMovimentos[i].NomeContaOrigem);
    sgMovimentacoes.Cells[coDataHoraMovimento, vLinha] := FormatDateTime('dd/mm/yyyy hh:nn:ss', vMovimentos[i].DataHoraMovimento);

    sgMovimentacoes.Cells[coId, vLinha]                := NFormat(vMovimentos[i].Id);

    if Em(vMovimentos[i].TipoMovimento, ['ABE', 'SUP', 'REV', 'ACU', 'BXR']) then begin
      vValorTotalDinheiroEnt := vValorTotalDinheiroEnt + vMovimentos[i].ValorDinheiro;
      vValorTotalChequeEnt   := vValorTotalChequeEnt + vMovimentos[i].ValorCheque;
      vValorTotalCartaoEnt   := vValorTotalCartaoEnt + vMovimentos[i].ValorCartao;
      vValorTotalCobrancaEnt := vValorTotalCobrancaEnt + vMovimentos[i].ValorCobranca;
      vValorTotalAcumuladoEnt:= vValorTotalAcumuladoEnt + vMovimentos[i].ValorAcumulado;
      vValorTotalCreditoEnt  := vValorTotalCreditoEnt + vMovimentos[i].ValorCredito;
      vValorTotalPixEnt      := vValorTotalPixEnt + vMovimentos[i].ValorPix;
    end
    else begin
      vValorTotalDinheiroSai := vValorTotalDinheiroSai + vMovimentos[i].ValorDinheiro;
      vValorTotalChequeSai   := vValorTotalChequeSai + vMovimentos[i].ValorCheque;
      vValorTotalCartaoSai   := vValorTotalCartaoSai + vMovimentos[i].ValorCartao;
      vValorTotalCobrancaSai := vValorTotalCobrancaSai + vMovimentos[i].ValorCobranca;
      vValorTotalAcumuladoSai:= vValorTotalAcumuladoSai + vMovimentos[i].ValorAcumulado;
      vValorTotalCreditoSai  := vValorTotalCreditoSai + vMovimentos[i].ValorCredito;
    end;
  end;

  if vLinha = 0 then begin
    NenhumRegistro;
    Exit;
  end;

//  vValorTotalDinheiro := -1*vValorTotalDinheiro;
//  vValorTotalCheque   := -1*vValorTotalCheque;
//  vValorTotalCartao   := -1*vValorTotalCartao;
//  vValorTotalCobranca := -1*vValorTotalCobranca;
//  vValorTotalAcumulado:= 0;//-1*vValorTotalAcumulado;
//  vValorTotalCredito  := 0;//-1*vValorTotalCredito;

  sgMovimentacoes.RowCount := vLinha + 6;

  sgMovimentacoes.Cells[coTipoMovimentoAna, sgMovimentacoes.RowCount - 3] := 'Total Entradas: ';
  sgMovimentacoes.Cells[coValorDinheiro, sgMovimentacoes.RowCount - 3]    := NFormatN(vValorTotalDinheiroEnt);
  sgMovimentacoes.Cells[coValorCheque, sgMovimentacoes.RowCount - 3]      := NFormatN(vValorTotalChequeEnt);
  sgMovimentacoes.Cells[coValorCartao, sgMovimentacoes.RowCount - 3]      := NFormatN(vValorTotalCartaoEnt);
  sgMovimentacoes.Cells[coValorCobranca, sgMovimentacoes.RowCount - 3]    := NFormatN(vValorTotalCobrancaEnt);
  sgMovimentacoes.Cells[coValorAcumulado, sgMovimentacoes.RowCount - 3]   := NFormatN(vValorTotalAcumuladoEnt);
  sgMovimentacoes.Cells[coValorCredito, sgMovimentacoes.RowCount - 3]     := NFormatN(vValorTotalCreditoEnt);
  sgMovimentacoes.Cells[coValorPix, sgMovimentacoes.RowCount -3]         := NFormatN(vValorTotalPixEnt);
  sgMovimentacoes.Cells[coValorTotal, sgMovimentacoes.RowCount - 3] :=
    NFormatN(
      vValorTotalDinheiroEnt +
      vValorTotalChequeEnt +
      vValorTotalCartaoEnt +
      vValorTotalCobrancaEnt +
      vValorTotalAcumuladoEnt +
      vValorTotalCreditoEnt +
      vValorTotalPixEnt
    );

  sgMovimentacoes.Cells[coTipoMovimentoAna, sgMovimentacoes.RowCount - 2] := 'Total Sa�das: ';
  sgMovimentacoes.Cells[coValorDinheiro, sgMovimentacoes.RowCount - 2]    := NFormatN(vValorTotalDinheiroSai);
  sgMovimentacoes.Cells[coValorCheque, sgMovimentacoes.RowCount - 2]      := NFormatN(vValorTotalChequeSai);
  sgMovimentacoes.Cells[coValorCartao, sgMovimentacoes.RowCount - 2]      := NFormatN(vValorTotalCartaoSai);
  sgMovimentacoes.Cells[coValorCobranca, sgMovimentacoes.RowCount - 2]    := NFormatN(vValorTotalCobrancaSai);
  sgMovimentacoes.Cells[coValorAcumulado, sgMovimentacoes.RowCount - 2]   := NFormatN(vValorTotalAcumuladoSai);
  sgMovimentacoes.Cells[coValorCredito, sgMovimentacoes.RowCount - 2]     := NFormatN(vValorTotalCreditoSai);
  sgMovimentacoes.Cells[coValorPix, sgMovimentacoes.RowCount - 2]         := NFormatN(vValorTotalPixSai);
  sgMovimentacoes.Cells[coValorTotal, sgMovimentacoes.RowCount - 2] :=
    NFormatN(
      vValorTotalDinheiroSai +
      vValorTotalChequeSai +
      vValorTotalCartaoSai +
      vValorTotalCobrancaSai +
      vValorTotalAcumuladoSai +
      vValorTotalCreditoSai +
      vValorTotalPixSai
    );

  sgMovimentacoes.Cells[coTipoMovimentoAna, sgMovimentacoes.RowCount - 1] := 'Resultado: ';
  sgMovimentacoes.Cells[coValorDinheiro, sgMovimentacoes.RowCount - 1]    := NFormatN(vValorTotalDinheiroEnt - vValorTotalDinheiroSai);
  sgMovimentacoes.Cells[coValorCheque, sgMovimentacoes.RowCount - 1]      := NFormatN(vValorTotalChequeEnt - vValorTotalChequeSai);
  sgMovimentacoes.Cells[coValorCartao, sgMovimentacoes.RowCount - 1]      := NFormatN(vValorTotalCartaoEnt - vValorTotalCartaoSai);
  sgMovimentacoes.Cells[coValorCobranca, sgMovimentacoes.RowCount - 1]    := NFormatN(vValorTotalCobrancaEnt - vValorTotalCobrancaSai);
  sgMovimentacoes.Cells[coValorAcumulado, sgMovimentacoes.RowCount - 1]   := NFormatN(vValorTotalAcumuladoEnt - vValorTotalAcumuladoSai);
  sgMovimentacoes.Cells[coValorCredito, sgMovimentacoes.RowCount - 1]     := NFormatN(vValorTotalCreditoEnt- vValorTotalCreditoSai);
  sgMovimentacoes.Cells[coValorPix, sgMovimentacoes.RowCount - 1]         := NFormatN(vValorTotalPixEnt - vValorTotalPixSai);
  sgMovimentacoes.Cells[coValorTotal, sgMovimentacoes.RowCount - 1] :=
    NFormatN(
      vValorTotalDinheiroEnt +
      vValorTotalChequeEnt +
      vValorTotalCartaoEnt +
      vValorTotalCobrancaEnt +
      vValorTotalAcumuladoEnt +
      vValorTotalPixEnt +
      vValorTotalCreditoEnt -
      vValorTotalDinheiroSai -
      vValorTotalChequeSai -
      vValorTotalCartaoSai -
      vValorTotalCobrancaSai -
      vValorTotalAcumuladoSai -
      vValorTotalCreditoSai -
      vValorTotalPixSai
    );

  SetarFoco(sgMovimentacoes);
end;

procedure TFormMovimentosFinanceiros.FormShow(Sender: TObject);
begin
  inherited;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId);
end;

procedure TFormMovimentosFinanceiros.sbImprimirClick(Sender: TObject);
begin
  inherited;
    GridToExcel(sgMovimentacoes);
end;

procedure TFormMovimentosFinanceiros.sgMovimentacoesDrawCell(Sender: TObject;
  ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in [
      coId,
      coValorDinheiro,
      coValorCheque,
      coValorCartao,
      coValorCobranca,
      coValorAcumulado,
      coValorCredito,
      coValorPix,
      coValorTotal,
      coId
    ]
  then
    vAlinhamento := taRightJustify
  else if ACol = coTipoMovimentoAna then begin
    if (ARow = sgMovimentacoes.RowCount - 1) or (ARow = sgMovimentacoes.RowCount - 2) or (ARow = sgMovimentacoes.RowCount - 3) then
      vAlinhamento := taRightJustify
    else
      vAlinhamento := taCenter;
  end
  else
    vAlinhamento := taLeftJustify;

  sgMovimentacoes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormMovimentosFinanceiros.sgMovimentacoesGetCellColor(
  Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush;
  AFont: TFont);
begin
  inherited;
  if (ARow = 0) or (sgMovimentacoes.Cells[coId, sgMovimentacoes.FixedRows] = '') then
    Exit;

  if ARow = sgMovimentacoes.RowCount - 1 then begin
    AFont.Style := [fsBold];
    AFont.Color := clWhite;
    ABrush.Color := clGreen;
  end
  else if ARow = sgMovimentacoes.RowCount - 2 then begin
    AFont.Style := [fsBold];
    AFont.Color := clWhite;
    ABrush.Color := clRed;
  end
  else if ARow = sgMovimentacoes.RowCount - 3 then begin
    AFont.Style := [fsBold];
    AFont.Color := clWhite;
    ABrush.Color := $000096DB;
  end
  else begin
    if ACol = coTipoMovimentoAna then begin
      AFont.Style := [fsBold];
      AFont.Color :=
        _Biblioteca.Decode(
          sgMovimentacoes.Cells[coTipoMovimentoSint, ARow],
          [
            'ABE', cor_abertura,
            'SUP', cor_suprimento,
            'SAN', cor_sangria,
            'ESC', cor_entrada_sang,
            'FEC', cor_fechamento,
            'REV', cor_recebimento,
            'ACU', cor_receb_acumu,
            'SSC', cor_saida_supri,
            'CRV', cor_canc_venda,
            'EFC', cor_ent_fec_cai,
            'BXP', cor_saida_bx_tit,
            'BXR', cor_ent_bx_tit,
            clBlack
          ]
        );
    end
    else if ACol = coValorTotal then begin
      AFont.Style := [fsBold];
      if Em(sgMovimentacoes.Cells[coTipoMovimentoSint, ARow], ['ABC', 'SUC', 'ESC', 'REV', 'ACU', 'BXR']) then
        AFont.Color := clBlue
      else
        AFont.Color := clRed;
    end;
  end;
end;

procedure TFormMovimentosFinanceiros.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrEmpresas.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio definir a empresa!');
    SetarFoco(FrEmpresas);
    Abort;
  end;

  if FrDataAbertura.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio o intervalo de datas');
    SetarFoco(FrDataAbertura);
    Abort;
  end;

  if gbTipoMovimento.NenhumMarcado then begin
    _Biblioteca.Exclamar('� necess�rio um tipo de movimento');
    SetarFoco(ckRecebimentoVenda);
    Abort;
  end;
end;

end.
