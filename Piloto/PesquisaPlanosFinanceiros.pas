unit PesquisaPlanosFinanceiros;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _PlanosFinanceiros,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _Sessao, _Biblioteca,
  Vcl.ExtCtrls;

type
  TFormPesquisaPlanosFinanceiros = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  protected
    procedure BuscarRegistros; override;
  end;

function Pesquisar(pSomenteAtivos: Boolean; pSomenteNivel3: Boolean): TObject;

implementation

{$R *.dfm}

const
  coDescricao = 2;
  coTipo      = 3;
  coExibirDre = 4;
  coAtivo     = 5;

var
  FSomenteNivel3: Boolean;

function Pesquisar(pSomenteAtivos: Boolean; pSomenteNivel3: Boolean): TObject;
var
  vObj: TObject;
begin
  FSomenteNivel3 := pSomenteNivel3;
  vObj := _HerancaPesquisas.Pesquisar(TFormPesquisaPlanosFinanceiros, _PlanosFinanceiros.GetFiltros, [pSomenteAtivos]);
  if vObj = nil then
    Result := nil
  else
    Result := RecPlanosFinanceiros(vObj);
end;

{ TFormPesquisaPlanosFinanceiros }

procedure TFormPesquisaPlanosFinanceiros.BuscarRegistros;
var
  i: Integer;
  vDados: TArray<RecPlanosFinanceiros>;
begin
  inherited;

  vDados :=
    _PlanosFinanceiros.BuscarPlanosFinanceiros(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text],
      FSomenteNivel3
    );

  if vDados = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vDados);

  for i := Low(vDados) to High(vDados) do begin
    sgPesquisa.Cells[coSelecionado, i + 1]  := _Biblioteca.charNaoSelecionado;
    sgPesquisa.Cells[coCodigo, i + 1]       := vDados[i].PlanoFinanceiroId;
    sgPesquisa.Cells[coDescricao, i + 1]    := vDados[i].Descricao;
    sgPesquisa.Cells[coTipo, i + 1]         := vDados[i].Tipo;
    sgPesquisa.Cells[coExibirDre, i + 1]    := vDados[i].ExibirDre;
    sgPesquisa.Cells[coAtivo, i + 1]        := vDados[i].Ativo;
  end;
  sgPesquisa.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vDados) );
  sgPesquisa.SetFocus;
end;

procedure TFormPesquisaPlanosFinanceiros.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  inherited;
  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taLeftJustify, Rect);
end;

end.
