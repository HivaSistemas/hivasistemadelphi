unit CadastroCargosProfissionais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, CheckBoxLuka, EditLuka, Vcl.Buttons, Vcl.ExtCtrls,
  _CargosProfissionais;

type
  TFormCadastroCargosProfissionais = class(TFormHerancaCadastroCodigo)
    lbllb1: TLabel;
    eNome: TEditLuka;
  private
    procedure PreencherRegistro(pCargoProfissional: RecCargosProfissionais);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

uses _Biblioteca, _Sessao, _RecordsEspeciais, PesquisaCargosProfissionais;

{ TFormCadastroCargosProfissionais }

procedure TFormCadastroCargosProfissionais.BuscarRegistro;
var
  vCargosProfissionais: TArray<RecCargosProfissionais>;
begin
  vCargosProfissionais := _CargosProfissionais.BuscarCargosProfissionais(Sessao.getConexaoBanco, 0, [eId.AsInt], False);
  if vCargosProfissionais = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    SetarFoco(eID);
    Exit;
  end;

  inherited;
  PreencherRegistro(vCargosProfissionais[0]);
end;

procedure TFormCadastroCargosProfissionais.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _CargosProfissionais.ExcluirCargoProfissional(Sessao.getConexaoBanco, eId.AsInt);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroCargosProfissionais.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vRetBanco :=
    _CargosProfissionais.AtualizarCargoProfissional(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eNome.Text,
      _Biblioteca.ToChar(ckAtivo)
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormCadastroCargosProfissionais.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    eNome,
    ckAtivo],
    pEditando
  );

  if pEditando then
    SetarFoco(eNome);
end;

procedure TFormCadastroCargosProfissionais.PesquisarRegistro;
var
  vCargoProfissional: RecCargosProfissionais;
begin
  vCargoProfissional := RecCargosProfissionais(PesquisaCargosProfissionais.PesquisarCargoProfissional(False));
  if vCargoProfissional = nil then
    Exit;

  inherited;
  PreencherRegistro(vCargoProfissional);
end;

procedure TFormCadastroCargosProfissionais.PreencherRegistro(pCargoProfissional: RecCargosProfissionais);
begin
  eID.AsInt       := pCargoProfissional.CargoId;
  eNome.Text      := pCargoProfissional.nome;
  ckAtivo.Checked := (pCargoProfissional.ativo = 'S');

  pCargoProfissional.Free;
end;

procedure TFormCadastroCargosProfissionais.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eNome.Trim = '' then begin
    _Biblioteca.Exclamar('O nome do cargo n�o foi informada corretamente, verifique!');
    SetarFoco(eNome);
    Abort;
  end;
end;

end.
