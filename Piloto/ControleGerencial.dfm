inherited FormControleGerencial: TFormControleGerencial
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'Controle gerencial'
  ClientHeight = 327
  ClientWidth = 882
  ExplicitWidth = 882
  ExplicitHeight = 327
  PixelsPerInch = 96
  TextHeight = 14
  object Label1: TLabel
    Left = 578
    Top = 53
    Width = 160
    Height = 29
    Alignment = taRightJustify
    Caption = 'R$ 12.000,00'
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 482
    Top = 18
    Width = 160
    Height = 29
    Alignment = taRightJustify
    Caption = 'R$ 13.500,00'
    Font.Charset = ANSI_CHARSET
    Font.Color = clTeal
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 713
    Top = 18
    Width = 145
    Height = 29
    Alignment = taRightJustify
    Caption = 'R$ 1.500,00'
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 23
    Top = 18
    Width = 175
    Height = 29
    Alignment = taRightJustify
    Caption = 'R$ 130.500,00'
    Font.Charset = ANSI_CHARSET
    Font.Color = clTeal
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 262
    Top = 18
    Width = 160
    Height = 29
    Alignment = taRightJustify
    Caption = 'R$ 11.500,00'
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 135
    Top = 53
    Width = 175
    Height = 29
    Alignment = taRightJustify
    Caption = 'R$ 119.000,00'
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label7: TLabel
    Left = 180
    Top = 88
    Width = 122
    Height = 29
    Alignment = taRightJustify
    Caption = 'R$ 170,00'
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label8: TLabel
    Left = 596
    Top = 88
    Width = 122
    Height = 29
    Alignment = taRightJustify
    Caption = 'R$ 133,33'
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label9: TLabel
    Left = 350
    Top = 205
    Width = 160
    Height = 29
    Alignment = taRightJustify
    Caption = 'R$ 25.700,00'
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label10: TLabel
    Left = 254
    Top = 170
    Width = 160
    Height = 29
    Alignment = taRightJustify
    Caption = 'R$ 35.500,00'
    Font.Charset = ANSI_CHARSET
    Font.Color = clTeal
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label11: TLabel
    Left = 485
    Top = 170
    Width = 145
    Height = 29
    Alignment = taRightJustify
    Caption = 'R$ 9.800,00'
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label12: TLabel
    Left = 388
    Top = 240
    Width = 122
    Height = 29
    Alignment = taRightJustify
    Caption = 'R$ 128,50'
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label13: TLabel
    Left = 189
    Top = 123
    Width = 97
    Height = 29
    Alignment = taRightJustify
    Caption = '22,00%'
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label14: TLabel
    Left = 609
    Top = 123
    Width = 97
    Height = 29
    Alignment = taRightJustify
    Caption = '18,89%'
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label15: TLabel
    Left = 413
    Top = 275
    Width = 97
    Height = 29
    Alignment = taRightJustify
    Caption = '22,00%'
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -24
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
end
