unit PesquisaCentroCustos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _Sessao, _Biblioteca,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _CentroCustos, Vcl.ExtCtrls;

type
  TFormPesquisaCentroCustos = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  protected
    procedure BuscarRegistros; override;
  end;

function PesquisarCentro: TObject;

implementation

{$R *.dfm}

const
  coNome   = 2;
  coAtivo  = 3;

function PesquisarCentro: TObject;
var
  obj: TObject;
begin
  obj := _HerancaPesquisas.Pesquisar(TFormPesquisaCentroCustos, _CentroCustos.GetFiltros);
  if obj = nil then
    Result := nil
  else
    Result := RecCentrosCustos(obj);
end;

{ TFormPesquisaCentroCustos }

procedure TFormPesquisaCentroCustos.BuscarRegistros;
var
  i: Integer;
  vDados: TArray<RecCentrosCustos>;
begin
  inherited;

  vDados :=
    _CentroCustos.BuscarCentroCustos(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text]
    );

  if vDados = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vDados);

  for i := Low(vDados) to High(vDados) do begin
    sgPesquisa.Cells[coSelecionado, i + 1]  := _Biblioteca.charNaoSelecionado;
    sgPesquisa.Cells[coCodigo, i + 1]       := NFormat(vDados[i].CentroCustoId);
    sgPesquisa.Cells[coNome, i + 1]         := vDados[i].Nome;
    sgPesquisa.Cells[coAtivo, i + 1]        := vDados[i].Ativo;
  end;
  sgPesquisa.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vDados) );
  SetarFoco(sgPesquisa);
end;

procedure TFormPesquisaCentroCustos.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coSelecionado, coAtivo] then
    vAlinhamento := taCenter
  else if ACol = coCodigo then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
