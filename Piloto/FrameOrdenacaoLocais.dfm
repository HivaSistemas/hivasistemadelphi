inherited FrOrdenacaoLocais: TFrOrdenacaoLocais
  Width = 222
  Height = 258
  ExplicitWidth = 222
  ExplicitHeight = 258
  object sbSubir: TSpeedButton
    Left = 193
    Top = 47
    Width = 23
    Height = 22
    Glyph.Data = {
      42040000424D4204000000000000420000002800000010000000100000000100
      200003000000000400008D4E02008D4E020000000000000000000000FF0000FF
      0000FF00000000000000B9802900B9802900B87F2900BF842B00B27B2711B77F
      283FB9802960B9802960B77F283FB27B2711BF842B00B87F2900B9802900B980
      29000000000000000000B9802900B87F2900AE782507BB822A5BC3872CC2CB8C
      2FEFCF8F30FBCF8F30FBCB8C2FEFC3872CC2BB822A5BAE782507B87F2900B980
      290000000000B9802900B77F2900B07A260AC1862C8ACF8F30F8D99633FFDB98
      34FFD49332FFD49332FFDB9834FFD99633FFCF8F30F8C1862C8BB07A260AB77F
      2900B9802900B17A2600FFC14700C3872C73D39231FCDB9834FFDB9834FFDB98
      35FFDCB880FFDCB880FFDB9835FFDB9834FFDB9834FFD39231FCC3872C73FFC1
      4700B17A2600C1852B00BE842B25D19131DFDB9834FFDB9834FFDB9834FFDB98
      35FFEAD4B0FFEAD4B0FFDB9835FFDB9834FFDB9834FFDB9834FFD19131DFBE84
      2B25C1852B00D6943200CC8E2F68D99733FFDB9834FFDA9733FFD99632FFDB98
      35FFE9D2AEFFE9D2AEFFDB9835FFD99632FFDA9733FFDB9834FFD99733FFCC8E
      2F68D6943200FFCA4B00D5943296DB9834FFDA9834FFD09844FFCF9A4BFFD997
      33FFEAD2AEFFEAD2AEFFD99733FFCF9A4BFFD09844FFDA9834FFDB9834FFD594
      3296FFCA4B00B8802901DA9734A3DB9834FFDB9937FFE5C99FFFE6DBC7FFD39D
      4BFFE9D1ACFFE9D1ACFFD39D4BFFE6DBC7FFE5C99FFFDB9937FFDB9834FFDA97
      34A3B8802901C5892D00DB98349DDB9834FFDB9733FFDFA856FFEEE2CEFFE1CD
      ADFFE5D2B5FFE5D2B5FFE1CDADFFEEE2CEFFDFA856FFDB9733FFDB9834FFDB98
      349DC5892D00DB983400DB98347BDB9834FFDB9834FFDB9732FFE1AF63FFEEE5
      D5FFEEEBE4FFEEEBE4FFEEE5D5FFE1AF63FFDB9732FFDB9834FFDB9834FFDB98
      347BDB983400DB983400DB98343CDB9834F1DB9834FFDB9834FFDB9732FFE3B7
      74FFF0ECE3FFF0ECE3FFE3B774FFDB9732FFDB9834FFDB9834FFDB9834F1DB98
      343CDB983400DB983400DB983406DB98349FDB9834FFDB9834FFDB9834FFDB98
      35FFE5BE84FFE5BE84FFDB9835FFDB9834FFDB9834FFDB9834FFDB98349FDB98
      3406DB983400DB983400DB983400DB983421DB9834C1DB9834FFDB9834FFDB98
      34FFDB9936FFDB9936FFDB9834FFDB9834FFDB9834FFDB9834C1DB983421DB98
      3400DB983400DB983400DB983400DB983400DB983421DB98349FDB9834F1DB98
      34FFDB9834FFDB9834FFDB9834FFDB9834F1DB98349FDB983421DB983400DB98
      3400DB98340000000000DB983400DB983400DB983400DB983406DB98343CDB98
      347BDB98349DDB98349DDB98347BDB98343CDB983406DB983400DB983400DB98
      3400000000000000000000000000DB983400DB983400DB983400DB983400DB98
      3400DB983400DB983400DB983400DB983400DB983400DB983400DB9834000000
      000000000000}
    OnClick = sbSubirClick
  end
  object sbDescer: TSpeedButton
    Left = 193
    Top = 71
    Width = 23
    Height = 22
    Glyph.Data = {
      42040000424D4204000000000000420000002800000010000000100000000100
      200003000000000400008D4E02008D4E020000000000000000000000FF0000FF
      0000FF00000000000000B9802900B9802900B87F2900BF842B00B27B2711B77F
      283FB9802960B9802960B77F283FB27B2711BF842B00B87F2900B9802900B980
      29000000000000000000B9802900B87F2900AE782507BB822A5BC3872CC2CB8C
      2FEFCF8F30FBCF8F30FBCB8C2FEFC3872CC2BB822A5BAE782507B87F2900B980
      290000000000B9802900B77F2900B07A260AC1862C8ACF8F30F8D99633FFDC98
      34FFD89632FFD89632FFDC9834FFD99633FFCF8F30F8C1862C8BB07A260AB77F
      2900B9802900B17A2600FFC14700C3872C73D39231FCDB9834FFDB9834FFD895
      31FFD1A259FFD1A259FFD89531FFDB9834FFDB9834FFD39231FCC3872C73FFC1
      4700B17A2600C1852B00BE842B25D19131DFDB9834FFDB9834FFDA9631FFD29E
      50FFE8DDCCFFE8DDCCFFD29E50FFDA9631FFDB9834FFDB9834FFD19131DFBE84
      2B25C1852B00D6943200CC8E2F68D99733FFDB9834FFDA9732FFD29944FFE4D4
      BBFFF1F1EDFFF1F1EDFFE4D4BBFFD29944FFDA9732FFDB9834FFD99733FFCC8E
      2F68D6943200FFCA4B00D5943296DB9834FFDB9733FFD3963CFFE0CCABFFEDE1
      CCFFECDDC5FFECDDC5FFEDE1CCFFE0CCABFFD3963CFFDB9733FFDB9834FFD594
      3296FFCA4B00B8802901DA9734A3DB9834FFDA9835FFE0C293FFEFE9DFFFE1B2
      6AFFE9D1ACFFE9D1ACFFE1B26AFFEFE9DFFFE0C293FFDA9835FFDB9834FFDA97
      34A3B8802901C5892D00DB98349DDB9834FFDB9936FFE2B26BFFE3B776FFDB99
      36FFE9D2AEFFE9D2AEFFDB9936FFE3B776FFE2B26BFFDB9936FFDB9834FFDB98
      349DC5892D00DB983400DB98347BDB9834FFDB9834FFDB9732FFDB9732FFDB98
      35FFE9D2AEFFE9D2AEFFDB9835FFDB9732FFDB9732FFDB9834FFDB9834FFDB98
      347BDB983400DB983400DB98343CDB9834F1DB9834FFDB9834FFDB9834FFDB98
      35FFEAD3AFFFEAD3AFFFDB9835FFDB9834FFDB9834FFDB9834FFDB9834F1DB98
      343CDB983400DB983400DB983406DB98349FDB9834FFDB9834FFDB9834FFDB98
      35FFE8CB9FFFE8CB9FFFDB9835FFDB9834FFDB9834FFDB9834FFDB98349FDB98
      3406DB983400DB983400DB983400DB983421DB9834C1DB9834FFDB9834FFDB98
      34FFDD9F43FFDD9F43FFDB9834FFDB9834FFDB9834FFDB9834C1DB983421DB98
      3400DB983400DB983400DB983400DB983400DB983421DB98349FDB9834F1DB98
      34FFDB9833FFDB9833FFDB9834FFDB9834F1DB98349FDB983421DB983400DB98
      3400DB98340000000000DB983400DB983400DB983400DB983406DB98343CDB98
      347BDB98349DDB98349DDB98347BDB98343CDB983406DB983400DB983400DB98
      3400000000000000000000000000DB983400DB983400DB983400DB983400DB98
      3400DB983400DB983400DB983400DB983400DB983400DB983400DB9834000000
      000000000000}
    OnClick = sbDescerClick
  end
  object sbAdicionarLocais: TSpeedButton
    Left = 193
    Top = 23
    Width = 23
    Height = 22
    Glyph.Data = {
      0A170000424D0A170000000000004200000028000000360000001B0000000100
      200003000000C8160000C30E0000C30E000000000000000000000000FF0000FF
      0000FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFF6CC4FFFF15A1FFFF6DC5FFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD1CEE4FFB6B1
      D5FFD1CEE5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF65C1
      FFFF0099FFFF0099FFFF0099FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCCE3FFAFAAD1FFAFAAD1FFAFAAD1FFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFF3FAFFFFFDFE
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF65C1FFFF0099FFFF0099FFFF0099
      FFFF6BC4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFBFBFDFFFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFCFCCE3FFAFAAD1FFAFAAD1FFAFAAD1FFD1CEE4FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFF9ED8FFFF49B6FFFF1AA3FFFF0A9DFFFF19A3FFFF48B6FFFFA1D9
      FFFFF4FBFFFF6DC5FFFF0099FFFF0099FFFF0099FFFF65C1FFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1DFEEFFC6C2DEFFB7B3
      D6FFB2ADD3FFB7B2D6FFC6C2DEFFE2E0EEFFFCFBFDFFD1CEE5FFAFAAD1FFAFAA
      D1FFAFAAD1FFCFCCE3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCF1FFFF34AEFFFF0099
      FFFF0099FFFF0099FFFF0099FFFF0099FFFF0099FFFF0099FFFF15A1FFFF0099
      FFFF0099FFFF0099FFFF65C1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFF4F3F9FFBFBBDAFFAFAAD1FFAFAAD1FFAFAAD1FFAFAAD1FFAFAA
      D1FFAFAAD1FFAFAAD1FFB6B1D5FFAFAAD1FFAFAAD1FFAFAAD1FFCFCCE3FFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFE2F3FFFF0C9EFFFF0099FFFF0099FFFF5EBFFFFFA4DB
      FFFFBAE3FFFFA5DBFFFF60BFFFFF0099FFFF0099FFFF0099FFFF059BFFFF6DC5
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F5FAFFB3AE
      D3FFAFAAD1FFAFAAD1FFCCC9E2FFE2E1EFFFE9E8F3FFE3E1EFFFCDCAE2FFAFAA
      D1FFAFAAD1FFAFAAD1FFB1ACD2FFD1CEE5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFF29A9FFFF0099FFFF0B9DFFFFC2E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFC7E9FFFF0F9FFFFF0099FFFF16A2FFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBCB8D8FFAFAAD1FFB2AED3FFECEB
      F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDECF5FFB4AFD4FFAFAA
      D1FFB6B1D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA7DCFFFF0099FFFF0099
      FFFFCAEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFCFECFFFF0099FFFF0099FFFFA9DDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFE3E2EFFFAFAAD1FFAFAAD1FFEEEDF5FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0EFF6FFAFAAD1FFAFAAD1FFE4E2
      EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFF46B5FFFF0099FFFF54BBFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5DBE
      FFFF0099FFFF3DB1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5C1
      DEFFAFAAD1FFC9C6E0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFCCC9E2FFAFAAD1FFC2BEDCFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFF13A1FFFF0099FFFFAFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB3E1FFFF0099FFFF109F
      FFFFFBFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB5B0D4FFAFAAD1FFE6E4
      F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFE7E6F1FFAFAAD1FFB4AFD4FFFEFEFEFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0099
      FFFF0099FFFFC9E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFC8E9FFFF0099FFFF0199FFFFF0F9FFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAAD1FFAFAAD1FFEEEDF5FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEED
      F5FFAFAAD1FFAFAAD1FFFAFAFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF14A1FFFF0099FFFFADDE
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFB2E0FFFF0099FFFF11A0FFFFFBFDFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFB5B1D5FFAFAAD1FFE5E4F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E5F1FFAFAAD1FFB4B0
      D4FFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFF47B5FFFF0099FFFF51B9FFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5ABD
      FFFF0099FFFF3EB2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5C2
      DEFFAFAAD1FFC8C5E0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFCBC8E1FFAFAAD1FFC2BFDCFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFAADDFFFF0099FFFF0099FFFFC5E8FFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCBEAFFFF0099FFFF0099FFFFA3DA
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4E3F0FFAFAAD1FFAFAA
      D1FFEDECF5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFEFEEF6FFAFAAD1FFAFAAD1FFE2E0EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFF2DABFFFF0099FFFF089CFFFFBDE5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFC1E6FFFF0C9EFFFF0099FFFF26A8FFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDB9D9FFAFAAD1FFB2ADD2FFEAE9
      F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECEAF4FFB3AED3FFAFAA
      D1FFBBB7D8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE6F5FFFF109F
      FFFF0099FFFF0099FFFF56BBFFFFA1D9FFFFBAE3FFFFA2DAFFFF58BCFFFF0099
      FFFF0099FFFF0C9EFFFFE1F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFF7F7FAFFB4AFD4FFAFAAD1FFAFAAD1FFCAC7E1FFE2E0
      EEFFE9E8F3FFE2E0EEFFCBC7E1FFAFAAD1FFAFAAD1FFB3AED3FFF6F5FAFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0F3FFFF39B0FFFF0099
      FFFF0099FFFF0099FFFF0099FFFF0099FFFF0099FFFF0099FFFF36AFFFFFDCF1
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFF5F5F9FFC1BDDBFFAFAAD1FFAFAAD1FFAFAAD1FFAFAAD1FFAFAA
      D1FFAFAAD1FFAFAAD1FFC0BCDBFFF4F3F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA4DBFFFF51B9FFFF1DA5
      FFFF0A9DFFFF1CA4FFFF4FB9FFFFA1D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFE2E1EFFFC8C5E0FFB8B4D6FFB2ADD3FFB8B3D6FFC8C4DFFFE2E0
      EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    NumGlyphs = 2
    OnClick = sbAdicionarLocaisClick
  end
  object Panel1: TPanel
    Left = 0
    Top = 17
    Width = 191
    Height = 241
    Align = alLeft
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 0
    object sgLocais: TGridLuka
      Left = 0
      Top = 0
      Width = 191
      Height = 241
      Align = alClient
      ColCount = 2
      DefaultRowHeight = 19
      DrawingStyle = gdsGradient
      FixedColor = 15395562
      FixedCols = 0
      RowCount = 1
      FixedRows = 0
      Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
      TabOrder = 0
      OnDrawCell = sgLocaisDrawCell
      OnKeyDown = sgLocaisKeyDown
      OnKeyPress = sgLocaisKeyPress
      IncrementExpandCol = 0
      IncrementExpandRow = 0
      CorLinhaFoco = 16768407
      CorFundoFoco = 16774625
      CorLinhaDesfoque = 14869218
      CorFundoDesfoque = 16382457
      CorSuperiorCabecalho = clWhite
      CorInferiorCabecalho = 13553358
      CorSeparadorLinhas = 12040119
      CorColunaFoco = clMenuHighlight
      Grid3D = False
      RealColCount = 2
      AtivarPopUpSelecao = False
      ColWidths = (
        39
        126)
    end
  end
  object StaticTextLuka1: TStaticTextLuka
    Left = 0
    Top = 0
    Width = 222
    Height = 17
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Retira no ato'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
end
