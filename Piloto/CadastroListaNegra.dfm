inherited FormCadastroListaNegra: TFormCadastroListaNegra
  Caption = 'Cadastro de lista negra'
  ClientHeight = 206
  ClientWidth = 451
  ExplicitWidth = 457
  ExplicitHeight = 235
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label1: TLabel
    Top = 37
    ExplicitTop = 37
  end
  object lb1: TLabel [1]
    Left = 126
    Top = 87
    Width = 32
    Height = 14
    Caption = 'Nome'
  end
  inherited pnOpcoes: TPanel
    Height = 206
    ExplicitHeight = 170
  end
  inherited eID: TEditLuka
    Top = 51
    Width = 59
    ExplicitTop = 51
    ExplicitWidth = 59
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 126
    Top = 6
    ExplicitLeft = 126
    ExplicitTop = 6
  end
  object eNome: TEditLuka
    Left = 126
    Top = 101
    Width = 317
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 3
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
