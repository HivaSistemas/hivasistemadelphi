inherited FormImpressorasUsuario: TFormImpressorasUsuario
  Caption = 'Impressoras por usu'#225'rio'
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    inherited sbDesfazer: TSpeedButton
      Left = 0
      Visible = True
      ExplicitLeft = 0
    end
  end
  inherited pcMaster: TPageControl
    Top = 51
    ExplicitTop = 51
  end
  inline FrEmpresa: TFrEmpresas
    Left = 389
    Top = 3
    Width = 271
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 389
    ExplicitTop = 3
    ExplicitWidth = 271
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 246
      Height = 24
      ExplicitWidth = 246
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 271
      ExplicitWidth = 271
      inherited lbNomePesquisa: TLabel
        Width = 47
        Caption = 'Empresa'
        ExplicitWidth = 47
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 166
        ExplicitLeft = 166
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 246
      Height = 25
      ExplicitLeft = 246
      ExplicitHeight = 25
    end
  end
  inline FrFuncionario: TFrFuncionarios
    Left = 125
    Top = 3
    Width = 261
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 125
    ExplicitTop = 3
    ExplicitWidth = 261
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 236
      Height = 24
      ExplicitWidth = 236
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 261
      ExplicitWidth = 261
      inherited lbNomePesquisa: TLabel
        Width = 65
        Caption = 'Funcion'#225'rio'
        ExplicitWidth = 65
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 156
        ExplicitLeft = 156
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 236
      Height = 25
      ExplicitLeft = 236
      ExplicitHeight = 25
    end
  end
end
