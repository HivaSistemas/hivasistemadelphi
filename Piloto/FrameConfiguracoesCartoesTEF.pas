unit FrameConfiguracoesCartoesTEF;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.Grids, _Biblioteca,
  GridLuka;

type
  RecConfiguracoes = record
    TextoProcurarBandeira1: string;
    PosicaoInicialProcBand1: Integer;
    QtdeCaracteresCopiarBand1: Integer;
    TextoProcurarBandeira2: string;
    PosicaoInicialProcBand2: Integer;
    QtdeCaracteresCopiarBand2: Integer;
    TextoProcurarRede1: string;
    PosicaoInicialProcRede1: Integer;
    QtdeCaracteresCopiarRede1: Integer;
    TextoProcurarRede2: string;
    PosicaoInicialProcRede2: Integer;
    QtdeCaracteresCopiarRede2: Integer;
    TextoProcurarNumeroCartao1: string;
    PosicIniProcNumeroCartao1: Integer;
    QtdeCaracCopiarNrCartao1: Integer;
    TextoProcurarNumeroCartao2: string;
    PosicIniProcNumeroCartao2: Integer;
    QtdeCaracCopiarNrCartao2: Integer;
    TextoProcurarNsu1: string;
    PosicaoIniProcurarNsu1: Integer;
    QtdeCaracCopiarNsu1: Integer;
    TextoProcurarNsu2: string;
    PosicaoIniProcurarNsu2: Integer;
    QtdeCaracCopiarNsu2: Integer;
    TextoProcurarCodAutoriz1: string;
    PosicaoIniProCodAutoriz1: Integer;
    QtdeCaracCopCodAutoriz1: Integer;
    TextoProcurarCodAutoriz2: string;
    PosicaoIniProCodAutoriz2: Integer;
    QtdeCaracCopCodAutoriz2: Integer
  end;

  TFrConfiguracoesCartoesTEF = class(TFrameHerancaPrincipal)
    sgConfig: TGridLuka;
    procedure sgConfigDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgConfigGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgConfigSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
  public
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
    procedure Clear; override;

    function getConfiguracoes: RecConfiguracoes;

    procedure setConfiguracoes(
      pTextoProcurarBandeira1: string;
      pPosicaoInicialProcBand1: Integer;
      pQtdeCaracteresCopiarBand1: Integer;
      pTextoProcurarBandeira2: string;
      pPosicaoInicialProcBand2: Integer;
      pQtdeCaracteresCopiarBand2: Integer;
      pTextoProcurarRede1: string;
      pPosicaoInicialProcRede1: Integer;
      pQtdeCaracteresCopiarRede1: Integer;
      pTextoProcurarRede2: string;
      pPosicaoInicialProcRede2: Integer;
      pQtdeCaracteresCopiarRede2: Integer;
      pTextoProcurarNumeroCartao1: string;
      pPosicIniProcNumeroCartao1: Integer;
      pQtdeCaracCopiarNrCartao1: Integer;
      pTextoProcurarNumeroCartao2: string;
      pPosicIniProcNumeroCartao2: Integer;
      pQtdeCaracCopiarNrCartao2: Integer;
      pTextoProcurarNsu1: string;
      pPosicaoIniProcurarNsu1: Integer;
      pQtdeCaracCopiarNsu1: Integer;
      pTextoProcurarNsu2: string;
      pPosicaoIniProcurarNsu2: Integer;
      pQtdeCaracCopiarNsu2: Integer;
      pTextoProcurarCodAutoriz1: string;
      pPosicaoIniProCodAutoriz1: Integer;
      pQtdeCaracCopCodAutoriz1: Integer;
      pTextoProcurarCodAutoriz2: string;
      pPosicaoIniProCodAutoriz2: Integer;
      pQtdeCaracCopCodAutoriz2: Integer
    );
  protected
    procedure Loaded; override;
  end;

implementation

{$R *.dfm}

const
  coCampo           = 0;
  coTexto1          = 1;
  coInicioTexto1    = 2;
  coQtdeCopiaTexto1 = 3;
  coTexto2          = 4;
  coInicioTexto2    = 5;
  coQtdeCopiaTexto2 = 6;


  (* Linhas *)
  clBandeira         = 1;
  clRede             = 2;
  clNrCartaoTruncado = 3;
  clNSU              = 4;
  clCodAutorizacao   = 5;

procedure TFrConfiguracoesCartoesTEF.Loaded;
begin
  inherited;
  sgConfig.Col := coTexto1;
end;


procedure TFrConfiguracoesCartoesTEF.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;

  sgConfig.Cells[coTexto1, clBandeira]          := '';
  sgConfig.Cells[coInicioTexto1, clBandeira]    := '';
  sgConfig.Cells[coQtdeCopiaTexto1, clBandeira] := '';
  sgConfig.Cells[coTexto2, clBandeira]          := '';
  sgConfig.Cells[coInicioTexto2, clBandeira]    := '';
  sgConfig.Cells[coQtdeCopiaTexto2, clBandeira] := '';

  sgConfig.Cells[coTexto1, clRede]          := '';
  sgConfig.Cells[coInicioTexto1, clRede]    := '';
  sgConfig.Cells[coQtdeCopiaTexto1, clRede] := '';
  sgConfig.Cells[coTexto2, clRede]          := '';
  sgConfig.Cells[coInicioTexto2, clRede]    := '';
  sgConfig.Cells[coQtdeCopiaTexto2, clRede] := '';

  sgConfig.Cells[coTexto1, clNrCartaoTruncado]          := '';
  sgConfig.Cells[coInicioTexto1, clNrCartaoTruncado]    := '';
  sgConfig.Cells[coQtdeCopiaTexto1, clNrCartaoTruncado] := '';
  sgConfig.Cells[coTexto2, clNrCartaoTruncado]          := '';
  sgConfig.Cells[coInicioTexto2, clNrCartaoTruncado]    := '';
  sgConfig.Cells[coQtdeCopiaTexto2, clNrCartaoTruncado] := '';

  sgConfig.Cells[coTexto1, clNSU]          := '';
  sgConfig.Cells[coInicioTexto1, clNSU]    := '';
  sgConfig.Cells[coQtdeCopiaTexto1, clNSU] := '';
  sgConfig.Cells[coTexto2, clNSU]          := '';
  sgConfig.Cells[coInicioTexto2, clNSU]    := '';
  sgConfig.Cells[coQtdeCopiaTexto2, clNSU] := '';

  sgConfig.Cells[coTexto1, clCodAutorizacao]          := '';
  sgConfig.Cells[coInicioTexto1, clCodAutorizacao]    := '';
  sgConfig.Cells[coQtdeCopiaTexto1, clCodAutorizacao] := '';

  sgConfig.Cells[coTexto2, clCodAutorizacao]          := '';
  sgConfig.Cells[coInicioTexto2, clCodAutorizacao]    := '';
  sgConfig.Cells[coQtdeCopiaTexto2, clCodAutorizacao] := '';

  if pEditando then begin
    sgConfig.Col := coTexto1;
    sgConfig.Row := 1;
  end;
end;

procedure TFrConfiguracoesCartoesTEF.Clear;
begin
  inherited;

end;

function TFrConfiguracoesCartoesTEF.getConfiguracoes: RecConfiguracoes;
begin
  Result.TextoProcurarBandeira1    := sgConfig.Cells[coTexto1, clBandeira];
  Result.PosicaoInicialProcBand1   := SFormatInt(sgConfig.Cells[coInicioTexto1, clBandeira]);
  Result.QtdeCaracteresCopiarBand1 := SFormatInt(sgConfig.Cells[coQtdeCopiaTexto1, clBandeira]);

  Result.TextoProcurarBandeira2    := sgConfig.Cells[coTexto2, clBandeira];
  Result.PosicaoInicialProcBand2   := SFormatInt(sgConfig.Cells[coInicioTexto2, clBandeira]);
  Result.QtdeCaracteresCopiarBand2 := SFormatInt(sgConfig.Cells[coQtdeCopiaTexto2, clBandeira]);

  Result.TextoProcurarRede1        := sgConfig.Cells[coTexto1, clRede];
  Result.PosicaoInicialProcRede1   := SFormatInt(sgConfig.Cells[coInicioTexto1, clRede]);
  Result.QtdeCaracteresCopiarRede1 := SFormatInt(sgConfig.Cells[coQtdeCopiaTexto1, clRede]);

  Result.TextoProcurarRede2        := sgConfig.Cells[coTexto2, clRede];
  Result.PosicaoInicialProcRede2   := SFormatInt(sgConfig.Cells[coInicioTexto2, clRede]);
  Result.QtdeCaracteresCopiarRede2 := SFormatInt(sgConfig.Cells[coQtdeCopiaTexto2, clRede]);

  Result.TextoProcurarNumeroCartao1 := sgConfig.Cells[coTexto1, clNrCartaoTruncado];
  Result.PosicIniProcNumeroCartao1  := SFormatInt(sgConfig.Cells[coInicioTexto1, clNrCartaoTruncado]);
  Result.QtdeCaracCopiarNrCartao1   := SFormatInt(sgConfig.Cells[coQtdeCopiaTexto1, clNrCartaoTruncado]);

  Result.TextoProcurarNumeroCartao2 := sgConfig.Cells[coTexto2, clNrCartaoTruncado];
  Result.PosicIniProcNumeroCartao2  := SFormatInt(sgConfig.Cells[coInicioTexto2, clNrCartaoTruncado]);
  Result.QtdeCaracCopiarNrCartao2   := SFormatInt(sgConfig.Cells[coQtdeCopiaTexto2, clNrCartaoTruncado]);

  Result.TextoProcurarNsu1      := sgConfig.Cells[coTexto1, clNSU];
  Result.PosicaoIniProcurarNsu1 := SFormatInt(sgConfig.Cells[coInicioTexto1, clNSU]);
  Result.QtdeCaracCopiarNsu1    := SFormatInt(sgConfig.Cells[coQtdeCopiaTexto1, clNSU]);

  Result.TextoProcurarNsu2      := sgConfig.Cells[coTexto2, clNSU];
  Result.PosicaoIniProcurarNsu2 := SFormatInt(sgConfig.Cells[coInicioTexto2, clNSU]);
  Result.QtdeCaracCopiarNsu2    := SFormatInt(sgConfig.Cells[coQtdeCopiaTexto2, clNSU]);

  Result.TextoProcurarCodAutoriz1 := sgConfig.Cells[coTexto1, clCodAutorizacao];
  Result.PosicaoIniProCodAutoriz1 := SFormatInt(sgConfig.Cells[coInicioTexto1, clCodAutorizacao]);
  Result.QtdeCaracCopCodAutoriz1  := SFormatInt(sgConfig.Cells[coQtdeCopiaTexto1, clCodAutorizacao]);

  Result.TextoProcurarCodAutoriz2 := sgConfig.Cells[coTexto2, clCodAutorizacao];
  Result.PosicaoIniProCodAutoriz2 := SFormatInt(sgConfig.Cells[coInicioTexto2, clCodAutorizacao]);
  Result.QtdeCaracCopCodAutoriz2  := SFormatInt(sgConfig.Cells[coQtdeCopiaTexto2, clCodAutorizacao]);
end;

procedure TFrConfiguracoesCartoesTEF.setConfiguracoes(
  pTextoProcurarBandeira1: string;
  pPosicaoInicialProcBand1: Integer;
  pQtdeCaracteresCopiarBand1: Integer;
  pTextoProcurarBandeira2: string;
  pPosicaoInicialProcBand2: Integer;
  pQtdeCaracteresCopiarBand2: Integer;
  pTextoProcurarRede1: string;
  pPosicaoInicialProcRede1: Integer;
  pQtdeCaracteresCopiarRede1: Integer;
  pTextoProcurarRede2: string;
  pPosicaoInicialProcRede2: Integer;
  pQtdeCaracteresCopiarRede2: Integer;
  pTextoProcurarNumeroCartao1: string;
  pPosicIniProcNumeroCartao1: Integer;
  pQtdeCaracCopiarNrCartao1: Integer;
  pTextoProcurarNumeroCartao2: string;
  pPosicIniProcNumeroCartao2: Integer;
  pQtdeCaracCopiarNrCartao2: Integer;
  pTextoProcurarNsu1: string;
  pPosicaoIniProcurarNsu1: Integer;
  pQtdeCaracCopiarNsu1: Integer;
  pTextoProcurarNsu2: string;
  pPosicaoIniProcurarNsu2: Integer;
  pQtdeCaracCopiarNsu2: Integer;
  pTextoProcurarCodAutoriz1: string;
  pPosicaoIniProCodAutoriz1: Integer;
  pQtdeCaracCopCodAutoriz1: Integer;
  pTextoProcurarCodAutoriz2: string;
  pPosicaoIniProCodAutoriz2: Integer;
  pQtdeCaracCopCodAutoriz2: Integer
);
begin
  sgConfig.Cells[coTexto1, clBandeira]          := pTextoProcurarBandeira1;
  sgConfig.Cells[coInicioTexto1, clBandeira]    := IntToStr(pPosicaoInicialProcBand1);
  sgConfig.Cells[coQtdeCopiaTexto1, clBandeira] := IntToStr(pQtdeCaracteresCopiarBand1);
  sgConfig.Cells[coTexto2, clBandeira]          := pTextoProcurarBandeira2;
  sgConfig.Cells[coInicioTexto2, clBandeira]    := IntToStr(pPosicaoInicialProcBand2);
  sgConfig.Cells[coQtdeCopiaTexto2, clBandeira] := IntToStr(pQtdeCaracteresCopiarBand2);

  sgConfig.Cells[coTexto1, clRede]          := pTextoProcurarRede1;
  sgConfig.Cells[coInicioTexto1, clRede]    := IntToStr(pPosicaoInicialProcRede1);
  sgConfig.Cells[coQtdeCopiaTexto1, clRede] := IntToStr(pQtdeCaracteresCopiarRede1);
  sgConfig.Cells[coTexto2, clRede]          := pTextoProcurarRede2;
  sgConfig.Cells[coInicioTexto2, clRede]    := IntToStr(pPosicaoInicialProcRede2);
  sgConfig.Cells[coQtdeCopiaTexto2, clRede] := IntToStr(pQtdeCaracteresCopiarRede2);

  sgConfig.Cells[coTexto1, clNrCartaoTruncado]          := pTextoProcurarNumeroCartao1;
  sgConfig.Cells[coInicioTexto1, clNrCartaoTruncado]    := IntToStr(pPosicIniProcNumeroCartao1);
  sgConfig.Cells[coQtdeCopiaTexto1, clNrCartaoTruncado] := IntToStr(pQtdeCaracCopiarNrCartao1);
  sgConfig.Cells[coTexto2, clNrCartaoTruncado]          := pTextoProcurarNumeroCartao2;
  sgConfig.Cells[coInicioTexto2, clNrCartaoTruncado]    := IntToStr(pPosicIniProcNumeroCartao2);
  sgConfig.Cells[coQtdeCopiaTexto2, clNrCartaoTruncado] := IntToStr(pQtdeCaracCopiarNrCartao2);

  sgConfig.Cells[coTexto1, clNSU]          := pTextoProcurarNsu1;
  sgConfig.Cells[coInicioTexto1, clNSU]    := IntToStr(pPosicaoIniProcurarNsu1);
  sgConfig.Cells[coQtdeCopiaTexto1, clNSU] := IntToStr(pQtdeCaracCopiarNsu1);
  sgConfig.Cells[coTexto2, clNSU]          := pTextoProcurarNsu2;
  sgConfig.Cells[coInicioTexto2, clNSU]    := IntToStr(pPosicaoIniProcurarNsu2);
  sgConfig.Cells[coQtdeCopiaTexto2, clNSU] := IntToStr(pQtdeCaracCopiarNsu2);

  sgConfig.Cells[coTexto1, clCodAutorizacao]          := pTextoProcurarCodAutoriz1;
  sgConfig.Cells[coInicioTexto1, clCodAutorizacao]    := IntToStr(pPosicaoIniProCodAutoriz1);
  sgConfig.Cells[coQtdeCopiaTexto1, clCodAutorizacao] := IntToStr(pQtdeCaracCopCodAutoriz1);

  sgConfig.Cells[coTexto2, clCodAutorizacao]          := pTextoProcurarCodAutoriz2;
  sgConfig.Cells[coInicioTexto2, clCodAutorizacao]    := IntToStr(pPosicaoIniProCodAutoriz2);
  sgConfig.Cells[coQtdeCopiaTexto2, clCodAutorizacao] := IntToStr(pQtdeCaracCopCodAutoriz2);
end;

procedure TFrConfiguracoesCartoesTEF.sgConfigDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coCampo, coTexto1, coTexto2] then
    vAlinhamento := taLeftJustify
  else
    vAlinhamento := taRightJustify;

  sgConfig.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFrConfiguracoesCartoesTEF.sgConfigGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol in[coTexto1, coInicioTexto1, coQtdeCopiaTexto1] then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end
  else if ACol in[coTexto2, coInicioTexto2, coQtdeCopiaTexto2] then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao3;
    AFont.Color  := _Biblioteca.coCorFonteEdicao3;
  end;
end;

procedure TFrConfiguracoesCartoesTEF.sgConfigSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  sgConfig.Options := sgConfig.Options + [goEditing];

  if ACol in[coInicioTexto1, coQtdeCopiaTexto1, coInicioTexto2, coQtdeCopiaTexto2] then
    sgConfig.OnKeyPress := Numeros
  else if ACol in[coTexto1, coTexto2] then
    sgConfig.OnKeyPress := nil
  else
    sgConfig.Options := sgConfig.Options - [goEditing];
end;

end.
