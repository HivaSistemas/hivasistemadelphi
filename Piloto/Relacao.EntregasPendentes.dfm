inherited FormRelacaoEntregasPendentes: TFormRelacaoEntregasPendentes
  Caption = 'Rela'#231#227'o de entregas pendentes'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    inherited sbImprimir: TSpeedButton
      Top = 57
      ExplicitTop = 57
    end
  end
  inherited pcDados: TPageControl
    ActivePage = tsResultado
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object lb1: TLabel
        Left = 420
        Top = 86
        Width = 86
        Height = 14
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alCustom
        Caption = 'Tipo de entrega'
      end
      inline FrClientes: TFrClientes
        Left = 1
        Top = 85
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 85
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 45
            ExplicitWidth = 45
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
      inline FrProdutos: TFrProdutos
        Left = 0
        Top = 170
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitTop = 170
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 48
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
        inherited ckFiltroExtra: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      inline FrOrcamentos: TFrNumeros
        Left = 623
        Top = 3
        Width = 134
        Height = 76
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 623
        ExplicitTop = 3
        ExplicitHeight = 76
        inherited sgNumeros: TGridLuka
          Height = 62
          ExplicitHeight = 62
        end
        inherited pnDescricao: TPanel
          Caption = ' Or'#231'amentos'
        end
      end
      inline FrPrevisaoEntrega: TFrDataInicialFinal
        Left = 416
        Top = 0
        Width = 201
        Height = 41
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 416
        ExplicitWidth = 201
        inherited Label1: TLabel
          Width = 108
          Height = 14
          Caption = 'Previs'#227'o de entrega'
          ExplicitWidth = 108
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrDataRecebimento: TFrDataInicialFinal
        Left = 418
        Top = 42
        Width = 199
        Height = 41
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 4
        TabStop = True
        ExplicitLeft = 418
        ExplicitTop = 42
        ExplicitWidth = 199
        inherited Label1: TLabel
          Width = 90
          Height = 14
          Caption = 'Data rec. pedido'
          ExplicitWidth = 90
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrEmpresas: TFrEmpresas
        Left = 1
        Top = 0
        Width = 401
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 5
        TabStop = True
        ExplicitLeft = 1
        ExplicitWidth = 401
        inherited sgPesquisa: TGridLuka
          Width = 376
          ExplicitWidth = 376
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 401
          ExplicitWidth = 401
          inherited lbNomePesquisa: TLabel
            Width = 53
            ExplicitWidth = 53
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 296
            ExplicitLeft = 296
          end
        end
        inherited pnPesquisa: TPanel
          Left = 376
          ExplicitLeft = 376
        end
      end
      object cbTipoEntrega: TComboBoxLuka
        Left = 420
        Top = 100
        Width = 185
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 6
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'N'#227'o filtrar'
          'A retirar'
          'A entregar'
          'Sem previs'#227'o')
        Valores.Strings = (
          'NaoFiltrar'
          'R'
          'E'
          'S')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      inline FrVendedores: TFrVendedores
        Left = 0
        Top = 340
        Width = 402
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 7
        TabStop = True
        ExplicitTop = 340
        ExplicitWidth = 402
        inherited sgPesquisa: TGridLuka
          Width = 377
          ExplicitWidth = 377
        end
        inherited PnTitulos: TPanel
          Width = 402
          ExplicitWidth = 402
          inherited lbNomePesquisa: TLabel
            Width = 65
            ExplicitWidth = 65
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 297
            ExplicitLeft = 297
          end
        end
        inherited pnPesquisa: TPanel
          Left = 377
          ExplicitLeft = 377
        end
      end
      inline FrLocalProduto: TFrLocais
        Left = 0
        Top = 253
        Width = 402
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 8
        TabStop = True
        ExplicitTop = 253
        ExplicitWidth = 402
        inherited sgPesquisa: TGridLuka
          Width = 377
          TabOrder = 5
          ExplicitWidth = 377
        end
        inherited CkAspas: TCheckBox
          Top = 56
          Height = 16
          ExplicitTop = 56
          ExplicitHeight = 16
        end
        inherited CkFiltroDuplo: TCheckBox
          Top = 24
          Height = 16
          ExplicitTop = 24
          ExplicitHeight = 16
        end
        inherited CkPesquisaNumerica: TCheckBox
          Top = 40
          Height = 16
          ExplicitTop = 40
          ExplicitHeight = 16
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
          TabOrder = 3
        end
        inherited PnTitulos: TPanel
          Width = 402
          TabOrder = 4
          ExplicitWidth = 402
          inherited lbNomePesquisa: TLabel
            Width = 28
            Caption = 'Local'
            ExplicitWidth = 28
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 297
            ExplicitLeft = 297
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 377
          ExplicitLeft = 377
        end
      end
      object ckSomenteRecebimentoNaEntrega: TCheckBoxLuka
        Left = 420
        Top = 134
        Width = 229
        Height = 17
        Caption = 'Somente recebimento na entrega'
        TabOrder = 9
        CheckedStr = 'N'
        Valor = 'DAC'
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object spSeparador: TSplitter
        Left = 2
        Top = 248
        Width = 884
        Height = 6
        Align = alNone
        ResizeStyle = rsUpdate
      end
      object sgEntregas: TGridLuka
        Left = 0
        Top = 17
        Width = 884
        Height = 210
        Align = alTop
        ColCount = 7
        DefaultRowHeight = 19
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        TabOrder = 0
        OnClick = sgEntregasClick
        OnDblClick = sgEntregasDblClick
        OnDrawCell = sgEntregasDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Pedido'
          'Cliente'
          'Tipo'
          'Data cadastro'
          'Vendedor'
          'Data/hora prev. entrega'
          'Local')
        OnGetCellColor = sgEntregasGetCellColor
        Grid3D = False
        RealColCount = 10
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          45
          248
          126
          162
          179
          137
          147)
      end
      object sgItens: TGridLuka
        Left = 0
        Top = 280
        Width = 884
        Height = 238
        Align = alBottom
        Anchors = [akLeft, akTop, akRight, akBottom]
        ColCount = 10
        DefaultRowHeight = 19
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goEditing, goRowSelect, goFixedRowClick]
        TabOrder = 1
        OnDrawCell = sgItensDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Produto'
          'Nome'
          'Marca'
          'Qtde.'
          'Entr.'
          'Devolv.'
          'Saldo'
          'Vlr. Total'
          'Und.'
          'Lote')
        OnGetCellColor = sgItensGetCellColor
        Grid3D = False
        RealColCount = 15
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          56
          216
          171
          54
          50
          58
          57
          68
          46
          132)
      end
      object StaticTextLuka1: TStaticTextLuka
        Left = 0
        Top = 0
        Width = 884
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Pend'#234'ncias'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object StaticTextLuka2: TStaticTextLuka
        Left = 0
        Top = 257
        Width = 884
        Height = 17
        Alignment = taCenter
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Itens da pend'#234'ncia'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object st4: TStaticText
        Left = 0
        Top = 226
        Width = 144
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Total a retirar'
        Color = 15395562
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 4
        Transparent = False
      end
      object stTotalRetirar: TStaticTextLuka
        Left = 143
        Top = 226
        Width = 137
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = '0'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        Transparent = False
        AsInt = 0
        TipoCampo = tcNumerico
        CasasDecimais = 0
      end
      object StaticText1: TStaticText
        Left = 279
        Top = 226
        Width = 153
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Total a entregar'
        Color = 15395562
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 6
        Transparent = False
      end
      object stTotalEntregar: TStaticTextLuka
        Left = 430
        Top = 226
        Width = 155
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = '0'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 7
        Transparent = False
        AsInt = 0
        TipoCampo = tcNumerico
        CasasDecimais = 0
      end
      object StaticText2: TStaticText
        Left = 583
        Top = 226
        Width = 154
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Total sem previs'#227'o'
        Color = 15395562
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 8
        Transparent = False
      end
      object stTotalSemPrevisao: TStaticTextLuka
        Left = 736
        Top = 226
        Width = 148
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = '0'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 9
        Transparent = False
        AsInt = 0
        TipoCampo = tcNumerico
        CasasDecimais = 0
      end
    end
  end
  object dsEntrega: TfrxUserDataSet
    RangeBegin = rbCurrent
    RangeEnd = reCurrent
    UserName = 'dsEntrega'
    OnFirst = dsEntregaFirst
    OnNext = dsEntregaNext
    OnPrior = dsEntregaPrior
    Fields.Strings = (
      'pedido'
      'cliente'
      'tipo'
      'DtCadastro')
    OnGetValue = dsEntregaGetValue
    Left = 480
    Top = 144
  end
  object dsItens: TfrxUserDataSet
    RangeBegin = rbCurrent
    RangeEnd = reCurrent
    UserName = 'dsItens'
    OnCheckEOF = dsItensCheckEOF
    OnFirst = dsItensFirst
    OnNext = dsItensNext
    OnPrior = dsItensPrior
    Fields.Strings = (
      'produto'
      'nome'
      'marca'
      'qtde'
      'entr'
      'devolu'
      'saldo'
      'unid'
      'lote'
      'pedido')
    OnGetValue = dsItensGetValue
    Left = 480
    Top = 200
  end
  object frxReport: TfrxReport
    Version = '5.1.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44043.704685613400000000
    ReportOptions.LastChange = 44362.890600868100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 480
    Top = 96
    Datasets = <
      item
        DataSet = dsEntrega
        DataSetName = 'dsEntrega'
      end
      item
        DataSet = dsItens
        DataSetName = 'dsItens'
      end
      item
        DataSet = dmRelatorio.dstEmpresa
        DataSetName = 'frxdstEmpresa'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 71.811070000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000001000
          Width = 68.031540000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 26.456710000000000000
          Top = 34.015770000000010000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 15.118120000000000000
          Top = 49.133889999999990000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 340.157700000000000000
          Top = 3.779530000000001000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 321.260050000000000000
          Top = 34.015770000000010000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade/UF:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 340.157700000000000000
          Top = 49.133889999999990000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 82.708720000000000000
          Top = 3.779530000000001000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 383.512060000000000000
          Top = 3.779530000000001000
          Width = 200.315090000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."CNPJ"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 84.929190000000000000
          Top = 18.897650000000000000
          Width = 551.811380000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            
              '[frxdstEmpresa."LOGRADOURO"] [frxdstEmpresa."COMPLEMENTO"] n'#176'[fr' +
              'xdstEmpresa."NUMERO"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 82.708720000000000000
          Top = 34.015770000000010000
          Width = 207.874150000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_BAIRRO"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 383.512060000000000000
          Top = 34.015770000000010000
          Width = 185.196970000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_CIDADE"] / [frxdstEmpresa."ESTADO_ID"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 82.708720000000000000
          Top = 49.133889999999990000
          Width = 75.590600000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 383.512060000000000000
          Top = 49.133889999999990000
          Width = 260.787570000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."E_MAIL"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 545.252320000000000000
          Top = 3.779530000000001000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Emitido em [Date] '#224's [Time]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 545.252320000000000000
          Top = 18.897650000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pag. [Page] de [TotalPages#]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 294.803340000000000000
        Width = 718.110700000000000000
        object Shape2: TfrxShapeView
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 41.574830000000000000
        Top = 151.181200000000000000
        Width = 718.110700000000000000
        DataSet = dsEntrega
        DataSetName = 'dsEntrega'
        RowCount = 0
        object Memo18: TfrxMemoView
          Left = 2.779530000000000000
          Top = 2.000000000000000000
          Width = 124.724490000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Pedido: [dsEntrega."pedido"]'
            '')
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          Top = 22.677179999999990000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          Fill.BackColor = cl3DLight
        end
        object dsEntregacliente: TfrxMemoView
          Left = 131.283550000000000000
          Top = 2.000000000000000000
          Width = 268.346630000000000000
          Height = 18.897650000000000000
          DataSet = dsEntrega
          DataSetName = 'dsEntrega'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cliente: [dsEntrega."cliente"]')
          ParentFont = False
        end
        object dsEntregatipo: TfrxMemoView
          Left = 401.409710000000000000
          Top = 2.000000000000000000
          Width = 109.606370000000000000
          Height = 18.897650000000000000
          DataSet = dsEntrega
          DataSetName = 'dsEntrega'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Tipo: [dsEntrega."tipo"]')
          ParentFont = False
        end
        object dsEntregaDtCadastro: TfrxMemoView
          Left = 518.795610000000000000
          Top = 2.000000000000000000
          Width = 192.756030000000000000
          Height = 18.897650000000000000
          DataSet = dsEntrega
          DataSetName = 'dsEntrega'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Dt Cadastro: [dsEntrega."DtCadastro"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 6.559060000000000000
          Top = 23.677179999999990000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 59.472480000000000000
          Top = 23.677179999999990000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 282.464750000000000000
          Top = 23.677179999999990000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 399.630180000000000000
          Top = 23.677180000000000000
          Width = 41.574830000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 444.984540000000000000
          Top = 23.677180000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Entregue')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 505.457020000000000000
          Top = 23.677179999999990000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Devolu'#231#227'o')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 569.709030000000000000
          Top = 23.677179999999990000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 626.401980000000000000
          Top = 23.677179999999990000
          Width = 34.015770000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Unid')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 665.197280000000000000
          Top = 23.677179999999990000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Lote')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Width = 718.110236220472400000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 215.433210000000000000
        Width = 718.110700000000000000
        DataSet = dsItens
        DataSetName = 'dsItens'
        RowCount = 0
        object dsItensproduto: TfrxMemoView
          Left = 6.559060000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DataField = 'produto'
          DataSet = dsItens
          DataSetName = 'dsItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[dsItens."produto"]')
          ParentFont = False
        end
        object dsItensnome: TfrxMemoView
          Left = 59.472480000000000000
          Width = 219.212740000000000000
          Height = 18.897650000000000000
          DataField = 'nome'
          DataSet = dsItens
          DataSetName = 'dsItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[dsItens."nome"]')
          ParentFont = False
        end
        object dsItensmarca: TfrxMemoView
          Left = 282.464750000000000000
          Width = 105.826840000000000000
          Height = 18.897650000000000000
          DataField = 'marca'
          DataSet = dsItens
          DataSetName = 'dsItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[dsItens."marca"]')
          ParentFont = False
        end
        object dsItensqtde: TfrxMemoView
          Left = 392.071120000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataField = 'qtde'
          DataSet = dsItens
          DataSetName = 'dsItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[dsItens."qtde"]')
          ParentFont = False
        end
        object dsItensentr: TfrxMemoView
          Left = 444.984540000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DataField = 'entr'
          DataSet = dsItens
          DataSetName = 'dsItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[dsItens."entr"]')
          ParentFont = False
        end
        object dsItensdevolu: TfrxMemoView
          Left = 505.457020000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          DataField = 'devolu'
          DataSet = dsItens
          DataSetName = 'dsItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[dsItens."devolu"]')
          ParentFont = False
        end
        object dsItenssaldo: TfrxMemoView
          Left = 573.488560000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataField = 'saldo'
          DataSet = dsItens
          DataSetName = 'dsItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[dsItens."saldo"]')
          ParentFont = False
        end
        object dsItensunid: TfrxMemoView
          Left = 626.401980000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          DataField = 'unid'
          DataSet = dsItens
          DataSetName = 'dsItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[dsItens."unid"]')
          ParentFont = False
        end
        object dsItenslote: TfrxMemoView
          Left = 665.197280000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DataField = 'lote'
          DataSet = dsItens
          DataSetName = 'dsItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[dsItens."lote"]')
          ParentFont = False
        end
      end
      object Memo15: TfrxMemoView
        Left = 370.393940000000000000
        Top = -49.133890000000000000
        Width = 75.590600000000000000
        Height = 11.338590000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
        ParentFont = False
      end
    end
  end
end
