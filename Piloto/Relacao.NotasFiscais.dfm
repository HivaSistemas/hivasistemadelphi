inherited FormRelacaoNotasFiscais: TFormRelacaoNotasFiscais
  Caption = 'Rela'#231#227'o de notas fiscais'
  ClientHeight = 587
  ClientWidth = 995
  ExplicitTop = -180
  ExplicitWidth = 1001
  ExplicitHeight = 616
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 587
    ExplicitHeight = 587
    inherited sbImprimir: TSpeedButton
      Visible = False
    end
    object SpeedButton1: TSpeedButton [2]
      Left = 4
      Top = 214
      Width = 110
      Height = 40
      BiDiMode = bdLeftToRight
      Caption = 'Gerar Planilha'
      Flat = True
      NumGlyphs = 2
      ParentBiDiMode = False
      OnClick = sbImprimirClick
    end
  end
  inherited pcDados: TPageControl
    Width = 873
    Height = 587
    ActivePage = tsResultado
    ExplicitWidth = 873
    ExplicitHeight = 587
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 865
      ExplicitHeight = 558
      inline FrEmpresas: TFrEmpresas
        Left = 0
        Top = 2
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitTop = 2
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 53
            ExplicitWidth = 53
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
      inline FrCadastros: TFrameCadastros
        Left = 0
        Top = 87
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitTop = 87
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 117
            Caption = 'Clintes/Fornecedores'
            ExplicitWidth = 117
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
      inline FrProdutos: TFrProdutos
        Left = 0
        Top = 170
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitTop = 170
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 48
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
        inherited ckFiltroExtra: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      inline FrDatasCadastro: TFrDataInicialFinal
        Left = 430
        Top = 45
        Width = 201
        Height = 40
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 6
        TabStop = True
        ExplicitLeft = 430
        ExplicitTop = 45
        ExplicitWidth = 201
        ExplicitHeight = 40
        inherited Label1: TLabel
          Left = 1
          Width = 93
          Height = 14
          ExplicitLeft = 1
          ExplicitWidth = 93
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Left = 92
          Width = 18
          Height = 14
          ExplicitLeft = 92
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Left = 115
          Height = 22
          ExplicitLeft = 115
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrDatasEmissao: TFrDataInicialFinal
        Left = 430
        Top = 2
        Width = 201
        Height = 41
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 7
        TabStop = True
        ExplicitLeft = 430
        ExplicitTop = 2
        ExplicitWidth = 201
        inherited Label1: TLabel
          Left = 1
          Width = 93
          Height = 14
          Caption = 'Data de emiss'#227'o'
          ExplicitLeft = 1
          ExplicitWidth = 93
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Left = 92
          Width = 18
          Height = 14
          ExplicitLeft = 92
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Left = 115
          Height = 22
          ExplicitLeft = 115
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrDatasCancelamento: TFrDataInicialFinal
        Left = 430
        Top = 90
        Width = 201
        Height = 40
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 8
        TabStop = True
        ExplicitLeft = 430
        ExplicitTop = 90
        ExplicitWidth = 201
        ExplicitHeight = 40
        inherited Label1: TLabel
          Left = 1
          Width = 123
          Height = 14
          Caption = 'Data de cancelamento'
          ExplicitLeft = 1
          ExplicitWidth = 123
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Left = 92
          Width = 18
          Height = 14
          ExplicitLeft = 92
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Left = 115
          Height = 22
          ExplicitLeft = 115
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      object gbTiposNota: TGroupBoxLuka
        Left = 535
        Top = 132
        Width = 142
        Height = 59
        Caption = 'Nota ou Cupom'
        TabOrder = 9
        OpcaoMarcarDesmarcar = True
        object ckNfe: TCheckBoxLuka
          Left = 14
          Top = 18
          Width = 139
          Height = 17
          Caption = 'Nota Fsical (NFe)'
          Checked = True
          State = cbChecked
          TabOrder = 0
          CheckedStr = 'S'
          Valor = 'N'
        end
        object ckNFCe: TCheckBoxLuka
          Left = 14
          Top = 38
          Width = 138
          Height = 17
          Caption = 'Cupom fiscal (NFC-e)'
          Checked = True
          State = cbChecked
          TabOrder = 1
          CheckedStr = 'S'
          Valor = 'C'
        end
      end
      object gbStatus: TGroupBoxLuka
        Left = 430
        Top = 132
        Width = 100
        Height = 79
        Caption = 'Status'
        TabOrder = 10
        OpcaoMarcarDesmarcar = True
        object ckNaoEmitida: TCheckBoxLuka
          Left = 14
          Top = 40
          Width = 85
          Height = 17
          Caption = 'N'#227'o emitida'
          Checked = True
          State = cbChecked
          TabOrder = 0
          CheckedStr = 'S'
          Valor = 'N'
        end
        object ckCancelada: TCheckBoxLuka
          Left = 14
          Top = 60
          Width = 85
          Height = 17
          Caption = 'Cancelada'
          TabOrder = 1
          CheckedStr = 'N'
          Valor = 'C'
        end
        object ckEmitida: TCheckBoxLuka
          Left = 14
          Top = 19
          Width = 76
          Height = 17
          Caption = 'Emitida'
          Checked = True
          State = cbChecked
          TabOrder = 2
          CheckedStr = 'S'
          Valor = 'E'
        end
        object ckDenegada: TCheckBoxLuka
          Left = 10
          Top = 76
          Width = 76
          Height = 17
          Caption = 'Denegada'
          TabOrder = 3
          Visible = False
          CheckedStr = 'N'
          Valor = 'D'
        end
      end
      inline FrCodigoNota: TFrameInteiros
        Left = 684
        Top = 319
        Width = 116
        Height = 76
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 12
        TabStop = True
        ExplicitLeft = 684
        ExplicitTop = 319
        ExplicitWidth = 116
        ExplicitHeight = 76
        inherited sgInteiros: TGridLuka
          Width = 116
          Height = 58
          ExplicitWidth = 116
          ExplicitHeight = 58
        end
        inherited Panel1: TPanel
          Width = 116
          Caption = 'C'#243'digo da nota'
          ExplicitWidth = 116
        end
      end
      inline FrNumeroNota: TFrameInteiros
        Left = 684
        Top = 2
        Width = 116
        Height = 76
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 13
        TabStop = True
        ExplicitLeft = 684
        ExplicitTop = 2
        ExplicitWidth = 116
        ExplicitHeight = 76
        inherited sgInteiros: TGridLuka
          Width = 116
          Height = 58
          ExplicitWidth = 116
          ExplicitHeight = 58
        end
        inherited Panel1: TPanel
          Width = 116
          Caption = 'N'#250'mero da nota'
          ExplicitWidth = 116
        end
      end
      inline FrCodigoOrcamento: TFrameInteiros
        Left = 684
        Top = 80
        Width = 116
        Height = 76
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 14
        TabStop = True
        ExplicitLeft = 684
        ExplicitTop = 80
        ExplicitWidth = 116
        ExplicitHeight = 76
        inherited sgInteiros: TGridLuka
          Width = 116
          Height = 58
          ExplicitWidth = 116
          ExplicitHeight = 58
        end
        inherited Panel1: TPanel
          Width = 116
          Caption = 'N'#250'mero do pedido'
          ExplicitWidth = 116
        end
      end
      object gbTipoMovimento: TGroupBoxLuka
        Left = 432
        Top = 256
        Width = 209
        Height = 260
        Caption = 'Tipo de movimento'
        TabOrder = 11
        OpcaoMarcarDesmarcar = True
        object ckVendaRetirarAto: TCheckBoxLuka
          Left = 14
          Top = 17
          Width = 107
          Height = 17
          Caption = 'Venda retirar ato'
          Checked = True
          State = cbChecked
          TabOrder = 0
          CheckedStr = 'S'
          Valor = 'VRA'
        end
        object ckVendaRetirar: TCheckBoxLuka
          Left = 14
          Top = 35
          Width = 107
          Height = 17
          Caption = 'Venda retirar'
          Checked = True
          State = cbChecked
          TabOrder = 1
          CheckedStr = 'S'
          Valor = 'VRE'
        end
        object ckVendaEntregar: TCheckBoxLuka
          Left = 14
          Top = 53
          Width = 107
          Height = 17
          Caption = 'Venda entregar'
          Checked = True
          State = cbChecked
          TabOrder = 2
          CheckedStr = 'S'
          Valor = 'VEN'
        end
        object ckDevolucoesRetiradas: TCheckBoxLuka
          Left = 14
          Top = 71
          Width = 171
          Height = 17
          Hint = 'Devolu'#231#227'o de venda retirar'
          Caption = 'Devolu'#231#227'o de venda retirar'
          Checked = True
          State = cbChecked
          TabOrder = 3
          CheckedStr = 'S'
          Valor = 'DRE'
        end
        object ckDevolucoesEntregas: TCheckBoxLuka
          Left = 14
          Top = 89
          Width = 163
          Height = 17
          Hint = 'Devolu'#231#227'o de venda entregar'
          Caption = 'Devolu'#231#227'o venda entregar'
          Checked = True
          State = cbChecked
          TabOrder = 4
          CheckedStr = 'S'
          Valor = 'DEN'
        end
        object ckTransfProdEmp: TCheckBoxLuka
          Left = 135
          Top = 19
          Width = 140
          Height = 17
          Hint = 'Transfer'#234'ncia de produtos entre empresas'
          Caption = 'Transf. produtos'
          TabOrder = 8
          Visible = False
          CheckedStr = 'N'
          Valor = 'TPE'
        end
        object ckDevolucaoAcumulados: TCheckBoxLuka
          Left = 14
          Top = 139
          Width = 140
          Height = 17
          Hint = 'Devolu'#231#227'o de acumulado'
          Caption = 'Devolu'#231#227'o acumulado'
          TabOrder = 7
          CheckedStr = 'N'
          Valor = 'DAC'
        end
        object ckAcumulados: TCheckBoxLuka
          Left = 14
          Top = 123
          Width = 92
          Height = 17
          Caption = 'Acumulado'
          Checked = True
          State = cbChecked
          TabOrder = 6
          CheckedStr = 'S'
          Valor = 'ACU'
        end
        object ckOutrasNotas: TCheckBoxLuka
          Left = 14
          Top = 107
          Width = 92
          Height = 17
          Caption = 'Nota manual'
          TabOrder = 5
          CheckedStr = 'N'
          Valor = 'OUT'
        end
        object ckNfeNfce: TCheckBoxLuka
          Left = 14
          Top = 156
          Width = 140
          Height = 17
          Hint = 'Nota de cupom'
          Caption = 'Nota de cupom'
          TabOrder = 9
          CheckedStr = 'N'
          Valor = 'NNE'
        end
        object ckRetornoParcialTotalEntrega: TCheckBoxLuka
          Left = 135
          Top = 56
          Width = 140
          Height = 17
          Hint = 'Retorno parcial/total de entrega'
          Caption = 'Retorno parc/tot. entr.'
          TabOrder = 10
          Visible = False
          CheckedStr = 'N'
          Valor = 'REN'
        end
        object ckTransferenciaFiscal: TCheckBoxLuka
          Left = 135
          Top = 37
          Width = 140
          Height = 17
          Caption = 'Transfer'#234'ncia fiscal'
          TabOrder = 11
          Visible = False
          CheckedStr = 'N'
          Valor = 'TFI'
        end
        object ckDevolucaoEntradaNF: TCheckBoxLuka
          Left = 14
          Top = 172
          Width = 139
          Height = 17
          Hint = 'Nota de devolu'#231#227'o de entrada'
          Caption = 'Dev. de compras'
          TabOrder = 12
          CheckedStr = 'N'
          Valor = 'DEF'
        end
        object ckProducaoEstoqueEntrada: TCheckBoxLuka
          Left = 14
          Top = 188
          Width = 187
          Height = 17
          Hint = 'Produ'#231#227'o de estoque entrada'
          Caption = 'Produ'#231#227'o de estoque entrada'
          TabOrder = 13
          CheckedStr = 'N'
          Valor = 'PEN'
        end
        object ckProducaoEstoqueSaida: TCheckBoxLuka
          Left = 14
          Top = 203
          Width = 182
          Height = 17
          Hint = 'Produ'#231#227'o de estoque sa'#237'da'
          Caption = 'Produ'#231#227'o de estoque sa'#237'da'
          TabOrder = 14
          CheckedStr = 'N'
          Valor = 'PSA'
        end
        object ckAjusteEstoqueEntrada: TCheckBoxLuka
          Left = 14
          Top = 218
          Width = 182
          Height = 17
          Hint = 'Ajuste de estoque entrada'
          Caption = 'Ajuste de estoque entrada'
          TabOrder = 15
          CheckedStr = 'N'
          Valor = 'AEE'
        end
        object ckAjusteEstoqueSaida: TCheckBoxLuka
          Left = 14
          Top = 234
          Width = 182
          Height = 17
          Hint = 'Ajuste de estoque sa'#237'da'
          Caption = 'Ajuste de estoque sa'#237'da'
          TabOrder = 16
          CheckedStr = 'N'
          Valor = 'AES'
        end
      end
      inline FrCodigoAcumulado: TFrNumeros
        Left = 684
        Top = 158
        Width = 116
        Height = 76
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 15
        TabStop = True
        ExplicitLeft = 684
        ExplicitTop = 158
        ExplicitWidth = 116
        ExplicitHeight = 76
        inherited sgNumeros: TGridLuka
          Top = 18
          Width = 116
          DefaultColWidth = 108
          ExplicitTop = 18
          ExplicitWidth = 116
          ColWidths = (
            108)
        end
        inherited pnDescricao: TPanel
          Width = 116
          Height = 18
          Caption = 'N'#250'm. do acumulado'
          ExplicitWidth = 116
          ExplicitHeight = 18
        end
      end
      inline FrCodigoRetirada: TFrNumeros
        Left = 0
        Top = 478
        Width = 112
        Height = 80
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 4
        TabStop = True
        Visible = False
        ExplicitTop = 478
        ExplicitWidth = 112
        ExplicitHeight = 80
        inherited sgNumeros: TGridLuka
          Top = 18
          Width = 112
          Height = 62
          DefaultColWidth = 108
          Visible = False
          ExplicitTop = 18
          ExplicitWidth = 112
          ExplicitHeight = 62
          ColWidths = (
            108)
        end
        inherited pnDescricao: TPanel
          Width = 112
          Height = 18
          Caption = 'C'#243'digo da retirada'
          Visible = False
          ExplicitWidth = 112
          ExplicitHeight = 18
        end
      end
      inline FrCodigoEntrega: TFrNumeros
        Left = 144
        Top = 478
        Width = 132
        Height = 80
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 5
        TabStop = True
        Visible = False
        ExplicitLeft = 144
        ExplicitTop = 478
        ExplicitWidth = 132
        ExplicitHeight = 80
        inherited sgNumeros: TGridLuka
          Top = 18
          Width = 132
          Height = 62
          DefaultColWidth = 108
          ExplicitTop = 18
          ExplicitWidth = 132
          ExplicitHeight = 62
          ColWidths = (
            108)
        end
        inherited pnDescricao: TPanel
          Width = 132
          Height = 18
          Caption = 'C'#243'digo da entrega'
          ExplicitWidth = 132
          ExplicitHeight = 18
        end
      end
      inline FrCodigoDevolucao: TFrNumeros
        Left = 684
        Top = 237
        Width = 116
        Height = 76
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 16
        TabStop = True
        ExplicitLeft = 684
        ExplicitTop = 237
        ExplicitWidth = 116
        ExplicitHeight = 76
        inherited sgNumeros: TGridLuka
          Top = 18
          Width = 116
          DefaultColWidth = 108
          ExplicitTop = 18
          ExplicitWidth = 116
          ColWidths = (
            108)
        end
        inherited pnDescricao: TPanel
          Width = 116
          Height = 18
          Caption = 'C'#243'digo da devolu'#231#227'o'
          ExplicitWidth = 116
          ExplicitHeight = 18
        end
      end
      inline FrCFOP: TFrCFOPs
        Left = 0
        Top = 355
        Width = 401
        Height = 82
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitTop = 355
        ExplicitWidth = 401
        inherited sgPesquisa: TGridLuka
          Width = 376
          ExplicitWidth = 376
        end
        inherited PnTitulos: TPanel
          Width = 401
          ExplicitWidth = 401
          inherited lbNomePesquisa: TLabel
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 296
            ExplicitLeft = 296
          end
        end
        inherited cbTipoCFOPs: TComboBoxLuka
          Height = 22
          ItemIndex = 2
          Text = 'Todos'
          ExplicitHeight = 22
        end
        inherited pnPesquisa: TPanel
          Left = 376
          ExplicitLeft = 376
          inherited sbPesquisa: TSpeedButton
            Top = 6
            ExplicitTop = 6
          end
        end
      end
      inline FrValorTotal: TFrFaixaNumeros
        Left = 431
        Top = 216
        Width = 200
        Height = 41
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 17
        TabStop = True
        ExplicitLeft = 431
        ExplicitTop = 216
        ExplicitWidth = 200
        inherited lb1: TLabel
          Width = 60
          Height = 14
          Caption = 'Valor entre'
          ExplicitWidth = 60
          ExplicitHeight = 14
        end
        inherited lb2: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eValor1: TEditLuka
          Height = 22
          ExplicitHeight = 22
        end
        inherited eValor2: TEditLuka
          Left = 109
          Width = 82
          Height = 22
          ExplicitLeft = 109
          ExplicitWidth = 82
          ExplicitHeight = 22
        end
      end
      inline FrLocalProduto: TFrLocais
        Left = 0
        Top = 256
        Width = 401
        Height = 93
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 18
        TabStop = True
        ExplicitTop = 256
        ExplicitWidth = 401
        ExplicitHeight = 93
        inherited sgPesquisa: TGridLuka
          Width = 376
          Height = 76
          ExplicitWidth = 376
          ExplicitHeight = 76
        end
        inherited PnTitulos: TPanel
          Width = 401
          ExplicitWidth = 401
          inherited lbNomePesquisa: TLabel
            Width = 97
            ExplicitWidth = 97
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 296
            ExplicitLeft = 296
          end
        end
        inherited pnPesquisa: TPanel
          Left = 376
          Height = 77
          ExplicitLeft = 376
          ExplicitHeight = 77
        end
      end
      inline frCodigoProducao: TFrameInteiros
        Left = 684
        Top = 407
        Width = 116
        Height = 76
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 19
        TabStop = True
        ExplicitLeft = 684
        ExplicitTop = 407
        ExplicitWidth = 116
        ExplicitHeight = 76
        inherited sgInteiros: TGridLuka
          Width = 116
          Height = 58
          ExplicitWidth = 116
          ExplicitHeight = 58
        end
        inherited Panel1: TPanel
          Width = 116
          Caption = 'C'#243'digo da produ'#231#227'o'
          ExplicitWidth = 116
        end
      end
    end
    object TabSheet1: TTabSheet [1]
      Caption = 'Outros filtros'
      ImageIndex = 2
      object Label1: TLabel
        Left = 272
        Top = 5
        Width = 129
        Height = 14
        Caption = 'N'#250'mero de nota gerado'
      end
      object lb1: TLabel
        Left = 272
        Top = 48
        Width = 59
        Height = 14
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alCustom
        Caption = 'Ordena'#231#227'o'
      end
      object gbClasseFiscal: TGroupBoxLuka
        Left = 3
        Top = 161
        Width = 184
        Height = 106
        Caption = 'Classe fiscal'
        TabOrder = 0
        OpcaoMarcarDesmarcar = True
        object CheckBoxLuka1: TCheckBoxLuka
          Left = 6
          Top = 18
          Width = 46
          Height = 17
          Caption = '00'
          Checked = True
          State = cbChecked
          TabOrder = 0
          CheckedStr = 'S'
          Valor = '00'
        end
        object CheckBoxLuka2: TCheckBoxLuka
          Left = 70
          Top = 17
          Width = 46
          Height = 17
          Caption = '10'
          Checked = True
          State = cbChecked
          TabOrder = 1
          CheckedStr = 'S'
          Valor = '10'
        end
        object CheckBoxLuka3: TCheckBoxLuka
          Left = 134
          Top = 16
          Width = 46
          Height = 17
          Caption = '20'
          Checked = True
          State = cbChecked
          TabOrder = 2
          CheckedStr = 'S'
          Valor = '20'
        end
        object CheckBoxLuka4: TCheckBoxLuka
          Left = 6
          Top = 186
          Width = 195
          Height = 17
          Caption = 'Entradas de notas de servi'#231'os'
          Checked = True
          State = cbChecked
          TabOrder = 3
          Visible = False
          CheckedStr = 'S'
          Valor = 'ENS'
        end
        object CheckBoxLuka5: TCheckBoxLuka
          Left = 70
          Top = 38
          Width = 46
          Height = 17
          Caption = '40'
          Checked = True
          State = cbChecked
          TabOrder = 4
          CheckedStr = 'S'
          Valor = '40'
        end
        object CheckBoxLuka6: TCheckBoxLuka
          Left = 134
          Top = 37
          Width = 46
          Height = 17
          Caption = '41'
          Checked = True
          State = cbChecked
          TabOrder = 5
          CheckedStr = 'S'
          Valor = '41'
        end
        object CheckBoxLuka7: TCheckBoxLuka
          Left = 6
          Top = 60
          Width = 46
          Height = 17
          Caption = '50'
          Checked = True
          State = cbChecked
          TabOrder = 6
          CheckedStr = 'S'
          Valor = '50'
        end
        object CheckBoxLuka8: TCheckBoxLuka
          Left = 70
          Top = 59
          Width = 46
          Height = 17
          Caption = '51'
          Checked = True
          State = cbChecked
          TabOrder = 7
          CheckedStr = 'S'
          Valor = '51'
        end
        object CheckBoxLuka9: TCheckBoxLuka
          Left = 134
          Top = 60
          Width = 46
          Height = 17
          Caption = '60'
          Checked = True
          State = cbChecked
          TabOrder = 8
          CheckedStr = 'S'
          Valor = '60'
        end
        object CheckBoxLuka10: TCheckBoxLuka
          Left = 6
          Top = 81
          Width = 46
          Height = 17
          Caption = '70'
          Checked = True
          State = cbChecked
          TabOrder = 9
          CheckedStr = 'S'
          Valor = '70'
        end
        object CheckBoxLuka11: TCheckBoxLuka
          Left = 70
          Top = 80
          Width = 46
          Height = 17
          Caption = '90'
          Checked = True
          State = cbChecked
          TabOrder = 10
          CheckedStr = 'S'
          Valor = '90'
        end
        object CheckBoxLuka12: TCheckBoxLuka
          Left = 6
          Top = 39
          Width = 46
          Height = 17
          Caption = '30'
          Checked = True
          State = cbChecked
          TabOrder = 11
          CheckedStr = 'S'
          Valor = '30'
        end
      end
      object rgTipoPessoa: TRadioGroupLuka
        Left = 3
        Top = 3
        Width = 209
        Height = 38
        Caption = 'Tipo de pessoa'
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Todas'
          'F'#237'sica'
          'Jur'#237'dica')
        TabOrder = 1
        Valores.Strings = (
          'T'
          'F'
          'J')
      end
      object gbFormasPagamento: TGroupBoxLuka
        Left = 2
        Top = 52
        Width = 185
        Height = 98
        Caption = 'Formas de pagamento    '
        TabOrder = 2
        OpcaoMarcarDesmarcar = True
        object ckCartao: TCheckBox
          Left = 14
          Top = 58
          Width = 88
          Height = 17
          Caption = 'Cart'#227'o'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object ckFinanceira: TCheckBox
          Left = 86
          Top = 17
          Width = 81
          Height = 17
          Caption = 'Financeira'
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
        object ckCheque: TCheckBox
          Left = 14
          Top = 37
          Width = 88
          Height = 17
          Caption = 'Cheque'
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
        object ckCobranca: TCheckBox
          Left = 86
          Top = 37
          Width = 80
          Height = 17
          Caption = 'Cobran'#231'a'
          Checked = True
          State = cbChecked
          TabOrder = 3
        end
        object ckAcumulado: TCheckBox
          Left = 86
          Top = 58
          Width = 82
          Height = 17
          Caption = 'Acumulado'
          Checked = True
          State = cbChecked
          TabOrder = 4
        end
        object ckDinheiro: TCheckBoxLuka
          Left = 14
          Top = 17
          Width = 71
          Height = 17
          Caption = 'Dinheiro'
          Checked = True
          State = cbChecked
          TabOrder = 5
          CheckedStr = 'S'
        end
        object ckPix: TCheckBox
          Left = 14
          Top = 77
          Width = 88
          Height = 17
          Caption = 'Pix'
          Checked = True
          State = cbChecked
          TabOrder = 6
        end
      end
      object cbNumeroNotaGerado: TComboBoxLuka
        Left = 272
        Top = 19
        Width = 185
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 3
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'N'#227'o filtrar'
          'Sim'
          'N'#227'o')
        Valores.Strings = (
          'NaoFiltrar'
          'S'
          'N')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      object cbOrdenacao: TComboBoxLuka
        Left = 272
        Top = 61
        Width = 185
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 4
        Text = 'Data de cadastro'
        Items.Strings = (
          'Data de cadastro'
          'N'#250'mero da nota')
        Valores.Strings = (
          'DAT'
          'NUM')
        AsInt = 0
        AsString = 'DAT'
      end
      inline frProducaoEstoque: TFrameInteiros
        Left = 3
        Top = 274
        Width = 138
        Height = 90
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 5
        TabStop = True
        ExplicitLeft = 3
        ExplicitTop = 274
        ExplicitWidth = 138
        ExplicitHeight = 90
        inherited sgInteiros: TGridLuka
          Width = 138
          Height = 72
          ExplicitWidth = 138
          ExplicitHeight = 72
        end
        inherited Panel1: TPanel
          Width = 138
          Caption = 'Produ'#231#227'o de estoque'
          ExplicitWidth = 138
        end
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 865
      ExplicitHeight = 558
      object spSeparador: TSplitter
        Left = 0
        Top = 233
        Width = 865
        Height = 6
        Cursor = crVSplit
        Align = alTop
        ResizeStyle = rsUpdate
        ExplicitTop = 246
        ExplicitWidth = 884
      end
      object sgNotasFiscais: TGridLuka
        Left = 0
        Top = 0
        Width = 865
        Height = 216
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alTop
        ColCount = 18
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        PopupMenu = pmRotinas
        TabOrder = 0
        OnClick = sgNotasFiscaisClick
        OnDblClick = sgNotasFiscaisDblClick
        OnDrawCell = sgNotasFiscaisDrawCell
        OnKeyDown = sgNotasFiscaisKeyDown
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Sel.?'
          'C'#243'digo'
          'Data cadastro'
          'Tipo nota'
          'Tipo movimento'
          'Nome / Raz'#227'o social'
          'Numero nota'
          'Valor total nota'
          'Valor tot. produtos'
          'Valor out. despesas'
          'Valor desconto'
          'Base calc. ICMS'
          'Valor ICMS'
          'Base calc. ICMS ST'
          'Valor ICMS ST'
          'Valor IPI'
          'CFOP'
          'Status'
          'Empresa')
        OnGetCellColor = sgNotasFiscaisGetCellColor
        OnGetCellPicture = sgNotasFiscaisGetCellPicture
        Grid3D = False
        RealColCount = 21
        OrdenarOnClick = True
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          35
          66
          87
          66
          195
          143
          83
          95
          115
          117
          90
          93
          69
          104
          80
          58
          65
          111)
      end
      object sgProdutos: TGridLuka
        Left = 0
        Top = 233
        Width = 865
        Height = 176
        Anchors = [akLeft, akTop, akRight]
        ColCount = 26
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 1
        OnDrawCell = sgProdutosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Produto'
          'Nome'
          'CST'
          'Pre'#231'o unit.'
          'Quantidade'
          'Und.'
          'Valor desc.'
          'Outras desp.'
          'Valor total'
          'Base c'#225'lc. ICMS'
          #205'nd. red. base c'#225'lc. ICMS'
          '% ICMS'
          'Valor ICMS'
          'Base c'#225'lc. ICMS ST'
          #205'nd. red. base c'#225'lc. ICMS ST'
          'Valor ICMS ST'
          'Base c'#225'lc. PIS'
          '% PIS'
          'Valor PIS'
          'Base c'#225'lc. COFINS'
          '% COFINS'
          'Valor COFINS'
          '% IVA'
          'Pre'#231'o pauta'
          '% IPI'
          'Valor IPI')
        Grid3D = False
        RealColCount = 26
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          54
          190
          40
          75
          72
          43
          71
          83
          91
          97
          141
          57
          86
          112
          160
          103
          84
          49
          69
          107
          81
          83
          62
          77
          49
          73)
      end
      object StaticTextLuka1: TStaticTextLuka
        Left = 0
        Top = 216
        Width = 865
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Produtos'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object Panel1: TPanel
        Left = 0
        Top = 408
        Width = 865
        Height = 152
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelKind = bkTile
        BevelOuter = bvNone
        Color = 15395562
        ParentBackground = False
        TabOrder = 3
        object miInutilizarNumercaoNFe: TSpeedButton
          Left = 0
          Top = 86
          Width = 201
          Height = 25
          BiDiMode = bdRightToLeft
          Caption = 'Inutilizar n'#250'meros de notas '
          Flat = True
          ParentBiDiMode = False
          OnClick = miInutilizarNumercaoNFeClick
        end
        object miEditarCapaNotaFiscal: TSpeedButton
          Left = 373
          Top = 2
          Width = 208
          Height = 25
          Caption = 'Edit. capa da NF selecionada'
          Flat = True
          OnClick = miEditarCapaNotaFiscalClick
        end
        object miEditarItensNotaFiscal: TSpeedButton
          Left = 373
          Top = 30
          Width = 208
          Height = 25
          Caption = 'Edit. itens da NF selecionada'
          Flat = True
          OnClick = miEditarItensNotaFiscalClick
        end
        object miCancelarNFe: TSpeedButton
          Left = 0
          Top = 58
          Width = 201
          Height = 25
          BiDiMode = bdRightToLeft
          Caption = 'Canc. doc. fiscal selecionado'
          Flat = True
          ParentBiDiMode = False
          OnClick = miCancelarNFeClick
        end
        object miEmitirNFePendente: TSpeedButton
          Left = 207
          Top = 2
          Width = 160
          Height = 25
          BiDiMode = bdLeftToRight
          Caption = 'Emitir NF individual'
          Flat = True
          ParentBiDiMode = False
          OnClick = miEmitirNFePendenteClick
        end
        object miImprimirDANFE: TSpeedButton
          Left = 207
          Top = 30
          Width = 160
          Height = 25
          Caption = 'Reimprimir NF selecionada'
          Flat = True
          OnClick = miImprimirDANFEClick
        end
        object miGerarXMLs: TSpeedButton
          Left = 373
          Top = 58
          Width = 208
          Height = 25
          Caption = 'Gerar XMLs das NF selecionadas'
          Flat = True
          OnClick = miGerarXMLsClick
        end
        object miGerarPDFDanfe: TSpeedButton
          Left = 587
          Top = 2
          Width = 208
          Height = 25
          Caption = 'Gerar PDF das NF selecionadas'
          Flat = True
          OnClick = miGerarPDFDanfeClick
        end
        object miMarcarDesmarcarFocado: TSpeedButton
          Left = 0
          Top = 2
          Width = 201
          Height = 25
          BiDiMode = bdRightToLeft
          Caption = 'Selec. nota fiscal (Espa'#231'o)'
          Flat = True
          ParentBiDiMode = False
          OnClick = miMarcarDesmarcarFocadoClick
        end
        object miMarcarDesmarcarTodos: TSpeedButton
          Left = 0
          Top = 30
          Width = 201
          Height = 25
          BiDiMode = bdRightToLeft
          Caption = 'Selec. todas as NF (CRTL + Espa'#231'o)'
          Flat = True
          ParentBiDiMode = False
          OnClick = miMarcarDesmarcarTodosClick
        end
        object miEmitirNFeNFCe: TSpeedButton
          Left = 587
          Top = 30
          Width = 208
          Height = 25
          Caption = 'Emitir nota de cupom'
          Flat = True
          OnClick = miEmitirNFeNFCeClick
        end
        object sbAplicarIndiceDeducao: TSpeedButton
          Left = 834
          Top = 99
          Width = 23
          Height = 22
          OnClick = sbAplicarIndiceDeducaoClick
        end
        object miAlterarEmpresaEmitente: TSpeedButton
          Left = 207
          Top = 86
          Width = 160
          Height = 25
          Caption = 'Alterar Empresa Emitente'
          Flat = True
          OnClick = miAlterarEmpresaEmitenteClick
        end
        object miEmitirNfsSelecionadas: TSpeedButton
          Left = 207
          Top = 58
          Width = 160
          Height = 25
          BiDiMode = bdLeftToRight
          Caption = 'Emitir NF'#39's selecionadas'
          Flat = True
          ParentBiDiMode = False
          OnClick = miEmitirNfsSelecionadasClick
        end
        object SpeedButton2: TSpeedButton
          Left = 373
          Top = 86
          Width = 208
          Height = 25
          Caption = 'Alt. Emp. Emit. de NFs selecionadas'
          Flat = True
          OnClick = SpeedButton2Click
        end
        object SpeedButton3: TSpeedButton
          Left = 587
          Top = 58
          Width = 208
          Height = 25
          Caption = 'Alt. Dest. de NFs selecionadas'
          Flat = True
          OnClick = SpeedButton3Click
        end
        object SpeedButton4: TSpeedButton
          Left = 19
          Top = 117
          Width = 160
          Height = 25
          Caption = 'Imprimir espelho da NFe'
          Flat = True
          OnClick = SpeedButton4Click
        end
        object sbAlterarTiposPagamento: TSpeedButton
          Left = 207
          Top = 117
          Width = 160
          Height = 25
          Caption = 'Alterar tipos de pagamento'
          Flat = True
          OnClick = sbAlterarTiposPagamentoClick
        end
        inline FrIndiceDescontoVenda: TFrIndicesDescontosVenda
          Left = 699
          Top = 82
          Width = 133
          Height = 41
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Color = 15395562
          ParentBackground = False
          ParentColor = False
          TabOrder = 0
          TabStop = True
          ExplicitLeft = 699
          ExplicitTop = 82
          ExplicitWidth = 133
          ExplicitHeight = 41
          inherited sgPesquisa: TGridLuka
            Width = 108
            Height = 24
            ExplicitWidth = 108
            ExplicitHeight = 24
          end
          inherited PnTitulos: TPanel
            Width = 133
            ExplicitWidth = 133
            inherited lbNomePesquisa: TLabel
              Width = 101
              Caption = #205'ndice de dedu'#231#227'o'
              ExplicitWidth = 101
              ExplicitHeight = 14
            end
            inherited pnSuprimir: TPanel
              Left = 28
              ExplicitLeft = 28
              inherited ckSuprimir: TCheckBox
                Visible = False
              end
            end
          end
          inherited pnPesquisa: TPanel
            Left = 108
            Height = 25
            ExplicitLeft = 108
            ExplicitHeight = 25
          end
        end
      end
    end
  end
  object pmRotinas: TPopupMenu
    Left = 954
    Top = 243
  end
end
