unit Frame.EnderecoEstoqueNivel;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _Sessao, _Biblioteca,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _EnderecoEstoqueNivel, Pesquisa.EnderecoEstoqueNivel,
  _RecordsCadastros, System.Math, Vcl.Buttons, Vcl.Menus;

type
  TFrEnderecoEstoqueNivel = class(TFrameHenrancaPesquisas)
  public
    function GetEnderecoEstoqueNivel(pLinha: Integer = -1): RecEnderecoEstoqueNivel;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrameEnderecoEstoqueNivel }

function TFrEnderecoEstoqueNivel.AdicionarDireto: TObject;
var
  vMotivos: TArray<RecEnderecoEstoqueNivel>;
begin
  vMotivos := _EnderecoEstoqueNivel.BuscarEnderecoEstoqueNivel(Sessao.getConexaoBanco, 0, [FChaveDigitada]);
  if vMotivos = nil then
    Result := nil
  else
    Result := vMotivos[0];
end;

function TFrEnderecoEstoqueNivel.AdicionarPesquisando: TObject;
begin
  Result := Pesquisa.EnderecoEstoqueNivel.Pesquisar();
end;

function TFrEnderecoEstoqueNivel.GetEnderecoEstoqueNivel(pLinha: Integer): RecEnderecoEstoqueNivel;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecEnderecoEstoqueNivel(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrEnderecoEstoqueNivel.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecEnderecoEstoqueNivel(FDados[i]).Nivel_id = RecEnderecoEstoqueNivel(pSender).Nivel_id then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrEnderecoEstoqueNivel.MontarGrid;
var
  i: Integer;
  vSender: RecEnderecoEstoqueNivel;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecEnderecoEstoqueNivel(FDados[i]);
      AAdd([IntToStr(vSender.Nivel_id), vSender.descricao]);
    end;
  end;
end;

end.
