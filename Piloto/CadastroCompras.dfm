inherited FormCadastroCompras: TFormCadastroCompras
  Caption = 'Pedidos de compras'
  ClientHeight = 463
  ClientWidth = 976
  ExplicitWidth = 982
  ExplicitHeight = 492
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label1: TLabel
    Left = 123
    Top = 4
    ExplicitLeft = 123
    ExplicitTop = 4
  end
  inherited pnOpcoes: TPanel
    Height = 463
    TabOrder = 4
    ExplicitHeight = 463
    inherited sbDesfazer: TSpeedButton
      Top = 45
      ExplicitTop = 45
    end
    inherited sbExcluir: TSpeedButton
      Left = -112
      Top = 164
      Visible = False
      ExplicitLeft = -112
      ExplicitTop = 164
    end
    inherited sbPesquisar: TSpeedButton
      Top = 88
      ExplicitTop = 88
    end
  end
  inherited eID: TEditLuka
    Left = 123
    Top = 18
    TabOrder = 0
    ExplicitLeft = 123
    ExplicitTop = 18
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 993
    TabOrder = 5
    Visible = False
    ExplicitLeft = 993
  end
  inline FrFornecedor: TFrFornecedores
    Left = 207
    Top = 0
    Width = 244
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 207
    ExplicitWidth = 244
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 219
      Height = 23
      ExplicitWidth = 219
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 244
      ExplicitWidth = 244
      inherited lbNomePesquisa: TLabel
        Width = 61
        Caption = 'Fornecedor'
        ExplicitWidth = 61
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 139
        ExplicitLeft = 139
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 219
      Height = 24
      ExplicitLeft = 219
      ExplicitHeight = 24
    end
  end
  inline FrComprador: TFrFuncionarios
    Left = 707
    Top = 1
    Width = 266
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 707
    ExplicitTop = 1
    ExplicitWidth = 266
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 241
      Height = 23
      ExplicitWidth = 241
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 266
      ExplicitWidth = 266
      inherited lbNomePesquisa: TLabel
        Width = 59
        Caption = 'Comprador'
        ExplicitWidth = 59
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 161
        ExplicitLeft = 161
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 241
      Height = 24
      ExplicitLeft = 241
      ExplicitHeight = 24
    end
    inherited ckComprador: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  inline FrEmpresa: TFrEmpresas
    Left = 455
    Top = -1
    Width = 248
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 455
    ExplicitTop = -1
    ExplicitWidth = 248
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 223
      Height = 23
      ExplicitWidth = 223
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 248
      ExplicitWidth = 248
      inherited lbNomePesquisa: TLabel
        Width = 47
        Caption = 'Empresa'
        ExplicitWidth = 47
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 143
        ExplicitLeft = 143
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 223
      Height = 24
      ExplicitLeft = 223
      ExplicitHeight = 24
    end
  end
  object pcDados: TPageControl
    Left = 123
    Top = 42
    Width = 850
    Height = 421
    ActivePage = tsPrincipais
    TabOrder = 6
    object tsPrincipais: TTabSheet
      Caption = 'Principais'
      ExplicitLeft = 2
      ExplicitTop = 21
      object lbl1: TLabel
        Left = 1
        Top = -1
        Width = 97
        Height = 14
        Caption = 'Data faturamento'
      end
      object lbl2: TLabel
        Left = 101
        Top = -1
        Width = 91
        Height = 14
        Caption = 'Previs'#227'o entrega'
      end
      object lbl8: TLabel
        Left = 197
        Top = -2
        Width = 69
        Height = 14
        Caption = 'Observa'#231#245'es'
      end
      object eDataFaturamento: TEditLukaData
        Left = 1
        Top = 13
        Width = 96
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1;_'
        MaxLength = 10
        TabOrder = 0
        Text = '  /  /    '
        OnKeyDown = ProximoCampo
      end
      object ePrevisaoEntrega: TEditLukaData
        Left = 101
        Top = 13
        Width = 92
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1;_'
        MaxLength = 10
        TabOrder = 1
        Text = '  /  /    '
        OnKeyDown = ProximoCampo
      end
      inline FrPlanoFinanceiro: TFrPlanosFinanceiros
        Left = 1
        Top = 57
        Width = 270
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 57
        ExplicitWidth = 270
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 245
          Height = 23
          ExplicitWidth = 245
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 270
          ExplicitWidth = 270
          inherited lbNomePesquisa: TLabel
            Width = 88
            Caption = 'Plano financeiro'
            ExplicitWidth = 88
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 165
            ExplicitLeft = 165
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 245
          Height = 24
          ExplicitLeft = 245
          ExplicitHeight = 24
        end
      end
      object meObservacoes: TMemoAltis
        Left = 197
        Top = 13
        Width = 642
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 800
        TabOrder = 2
        OnKeyDown = ProximoCampo
      end
      inline FrDadosCobranca: TFrDadosCobranca
        Left = 0
        Top = 103
        Width = 578
        Height = 243
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 4
        TabStop = True
        ExplicitTop = 103
        ExplicitHeight = 243
        inherited pnInformacoesPrincipais: TPanel
          inherited lbllb10: TLabel
            Width = 39
            Height = 14
            ExplicitWidth = 39
            ExplicitHeight = 14
          end
          inherited lbllb9: TLabel
            Width = 29
            Height = 14
            ExplicitWidth = 29
            ExplicitHeight = 14
          end
          inherited lbllb6: TLabel
            Width = 28
            Height = 14
            ExplicitWidth = 28
            ExplicitHeight = 14
          end
          inherited lbllb2: TLabel
            Width = 64
            Height = 14
            ExplicitWidth = 64
            ExplicitHeight = 14
          end
          inherited lbl2: TLabel
            Width = 62
            Height = 14
            ExplicitWidth = 62
            ExplicitHeight = 14
          end
          inherited eRepetir: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited ePrazo: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eValorCobranca: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eDataVencimento: TEditLukaData
            Height = 22
            ExplicitHeight = 22
          end
          inherited FrTiposCobranca: TFrTiposCobranca
            inherited PnTitulos: TPanel
              inherited lbNomePesquisa: TLabel
                Height = 15
              end
            end
          end
          inherited eDocumento: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
        end
        inherited pnInformacoesBoleto: TPanel
          inherited lbllb31: TLabel
            Width = 79
            Height = 14
            ExplicitWidth = 79
            ExplicitHeight = 14
          end
          inherited lbl1: TLabel
            Width = 92
            Height = 14
            ExplicitWidth = 92
            ExplicitHeight = 14
          end
          inherited eNossoNumero: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eCodigoBarras: TMaskEditLuka
            Height = 22
            ExplicitHeight = 22
          end
        end
        inherited pn1: TPanel
          Height = 130
          ExplicitHeight = 130
          inherited sgCobrancas: TGridLuka
            Top = 3
            Height = 126
            ExplicitTop = 3
            ExplicitHeight = 126
          end
          inherited ckObrigarDataBase: TCheckBoxLuka
            Checked = True
            State = cbChecked
            CheckedStr = 'S'
          end
        end
        inherited pnInformacoesCheque: TPanel
          inherited lbllb1: TLabel
            Width = 33
            Height = 14
            ExplicitWidth = 33
            ExplicitHeight = 14
          end
          inherited lbllb3: TLabel
            Width = 43
            Height = 14
            ExplicitWidth = 43
            ExplicitHeight = 14
          end
          inherited lbllb4: TLabel
            Width = 79
            Height = 14
            ExplicitWidth = 79
            ExplicitHeight = 14
          end
          inherited lbllb5: TLabel
            Width = 71
            Height = 14
            ExplicitWidth = 71
            ExplicitHeight = 14
          end
          inherited lbl3: TLabel
            Width = 85
            Height = 14
            ExplicitWidth = 85
            ExplicitHeight = 14
          end
          inherited lbl4: TLabel
            Width = 48
            Height = 14
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited lbCPF_CNPJ: TLabel
            Width = 52
            Height = 14
            ExplicitWidth = 52
            ExplicitHeight = 14
          end
          inherited eBanco: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eAgencia: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eContaCorrente: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eNumeroCheque: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eNomeEmitente: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eTelefoneEmitente: TEditTelefoneLuka
            Height = 22
            OnKeyDown = nil
            ExplicitHeight = 22
          end
          inherited eCPF_CNPJ: TEditCPF_CNPJ_Luka
            Height = 22
            ExplicitHeight = 22
          end
        end
      end
      object StaticTextLuka3: TStaticTextLuka
        Left = -2
        Top = 41
        Width = 580
        Height = 15
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
    end
    object tsProdutos: TTabSheet
      Caption = 'Produtos'
      ImageIndex = 1
      object lbl5: TLabel
        Left = 591
        Top = 38
        Width = 70
        Height = 14
        Caption = 'Vlr. desconto'
      end
      object lbl6: TLabel
        Left = 675
        Top = 38
        Width = 68
        Height = 14
        Caption = 'Vlr.out.desp.'
      end
      object lbl7: TLabel
        Left = 759
        Top = 38
        Width = 59
        Height = 14
        Caption = 'Vlr. l'#237'quido'
      end
      object lbl4: TLabel
        Left = 759
        Top = -2
        Width = 27
        Height = 14
        Caption = 'Total'
      end
      object lbl3: TLabel
        Left = 675
        Top = -2
        Width = 57
        Height = 14
        Caption = 'Pre'#231'o unit.'
      end
      object lbllb7: TLabel
        Left = 591
        Top = -2
        Width = 64
        Height = 14
        Caption = 'Quantidade'
      end
      inline FrProduto: TFrProdutos
        Left = 0
        Top = -3
        Width = 319
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitTop = -3
        ExplicitWidth = 319
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 294
          Height = 23
          ExplicitWidth = 294
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 319
          ExplicitWidth = 319
          inherited lbNomePesquisa: TLabel
            Width = 42
            Caption = 'Produto'
            ExplicitWidth = 42
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 214
            ExplicitLeft = 214
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 294
          Height = 24
          ExplicitLeft = 294
          ExplicitHeight = 24
        end
        inherited ckSomenteAtivos: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited ckFiltroExtra: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      object eValorDesconto: TEditLuka
        Left = 591
        Top = 52
        Width = 80
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        Text = '0,00'
        OnChange = eValorOutrasDespesasChange
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorOutrasDespesas: TEditLuka
        Left = 675
        Top = 52
        Width = 80
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        Text = '0,00'
        OnChange = eValorOutrasDespesasChange
        OnKeyDown = eValorOutrasDespesasKeyDown
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorLiquido: TEditLuka
        Left = 759
        Top = 52
        Width = 80
        Height = 22
        TabStop = False
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object sgItens: TGridLuka
        Left = 0
        Top = 78
        Width = 842
        Height = 281
        Align = alCustom
        ColCount = 10
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 7
        OnDblClick = sgItensDblClick
        OnDrawCell = sgItensDrawCell
        OnKeyDown = sgItensKeyDown
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Produto'
          'Nome'
          'Marca'
          'Und.'
          'Quantidade'
          'Pre'#231'o unit.'
          'Total'
          'Vlr. desconto'
          'Vlr.out.desp.'
          'Vlr. l'#237'quido')
        Grid3D = False
        RealColCount = 10
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          49
          203
          109
          32
          72
          68
          73
          83
          78
          77)
      end
      inline FrMultiplosCompra: TFrMultiplosCompra
        Left = 321
        Top = 3
        Width = 263
        Height = 35
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 8
        TabStop = True
        ExplicitLeft = 321
        ExplicitTop = 3
        inherited sbAlterarMultiplo: TSpeedButton
          Left = 242
          Top = -1
          Height = 35
          Caption = '?'
          Font.Color = clRed
          Font.Name = 'Calibri'
          ParentFont = False
          ExplicitLeft = 242
          ExplicitTop = -1
          ExplicitHeight = 35
        end
      end
      object eValorTotal: TEditLuka
        Left = 759
        Top = 12
        Width = 80
        Height = 22
        TabStop = False
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object ePrecoUnitario: TEditLuka
        Left = 675
        Top = 12
        Width = 80
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        Text = '0,00'
        OnChange = eValorOutrasDespesasChange
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eQuantidade: TEditLuka
        Left = 591
        Top = 12
        Width = 80
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        Text = '0,0000'
        OnChange = eValorOutrasDespesasChange
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 4
        NaoAceitarEspaco = False
      end
      object StaticTextLuka1: TStaticTextLuka
        Left = 0
        Top = 360
        Width = 129
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Qtde. produtos'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 9
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object stQuantidadeItensGrid: TStaticText
        Left = 0
        Top = 376
        Width = 129
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        TabOrder = 10
        Transparent = False
      end
      object stValorTotal: TStaticText
        Left = 128
        Top = 376
        Width = 129
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        TabOrder = 11
        Transparent = False
      end
      object StaticTextLuka2: TStaticTextLuka
        Left = 128
        Top = 360
        Width = 129
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Valor total l'#237'quido'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 12
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
    end
  end
end
