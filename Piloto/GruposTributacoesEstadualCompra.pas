unit GruposTributacoesEstadualCompra;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Sessao,
  CheckBoxLuka, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _FrameHerancaPrincipal,
  Frame.ICMSCompra, _Biblioteca, _RecordsEspeciais, _GruposTribEstadualCompra,
  _GruposTribEstadualCompEst, PesquisaGruposTributacoesEstadualCompra;

type
  TFormGruposTributacoesEstadualCompra = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eDescricao: TEditLuka;
    FrICMS: TFrICMSCompra;
  private
    procedure PreencherRegistro(pGrupo: RecGruposTribEstadualCompra);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormGruposTributacoesEstadualCompra }

procedure TFormGruposTributacoesEstadualCompra.BuscarRegistro;
var
  vGrupo: TArray<RecGruposTribEstadualCompra>;
begin
  vGrupo := _GruposTribEstadualCompra.BuscarGruposTribEstadualCompra(Sessao.getConexaoBanco, 0, [eID.AsInt]);
  if vGrupo = nil then begin
    _Biblioteca.NenhumRegistro;
    SetarFoco(eID);
    Abort;
  end;

  inherited;
  PreencherRegistro(vGrupo[0]);
end;

procedure TFormGruposTributacoesEstadualCompra.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _GruposTribEstadualCompra.ExcluirGruposTribEstadualCompra(Sessao.getConexaoBanco, eID.AsInt);
  Sessao.AbortarSeHouveErro(vRetBanco);

  inherited;
end;

procedure TFormGruposTributacoesEstadualCompra.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;

  vRetBanco :=
    _GruposTribEstadualCompra.AtualizarGruposTribEstadualCompra(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eDescricao.Text,
      ckAtivo.CheckedStr,
      FrICMS.ICMS
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormGruposTributacoesEstadualCompra.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    eDescricao,
    FrICMS],
    pEditando
  );

  if pEditando then
    SetarFoco(eDescricao);
end;

procedure TFormGruposTributacoesEstadualCompra.PesquisarRegistro;
var
  vGrupo: RecGruposTribEstadualCompra;
begin
  vGrupo := RecGruposTribEstadualCompra(PesquisaGruposTributacoesEstadualCompra.Pesquisar);
  if vGrupo = nil then
    Exit;

  inherited;
  PreencherRegistro(vGrupo);
end;

procedure TFormGruposTributacoesEstadualCompra.PreencherRegistro(pGrupo: RecGruposTribEstadualCompra);
begin
  eID.AsInt                   := pGrupo.GrupoTribEstadualCompraId;
  eDescricao.Text             := pGrupo.Descricao;
  ckAtivo.CheckedStr          := pGrupo.Ativo;
  FrICMS.ICMS                 := _GruposTribEstadualCompEst.BuscarGruposTribEstadualCompEst(Sessao.getConexaoBanco, 0, [pGrupo.GrupoTribEstadualCompraId]);
end;

procedure TFormGruposTributacoesEstadualCompra.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if eDescricao.Trim = '' then begin
    _Biblioteca.Exclamar('A descri��o do grupo de tributa��o n�o foi informada corretamente, verifique!');
    SetarFoco(eDescricao);
    Abort;
  end;

  FrICMS.VerificarRegistro;
end;

end.
