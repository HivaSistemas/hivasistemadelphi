unit RelacaoCompras;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _Sessao, _Biblioteca,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, FrameDataInicialFinal, _Compras, _ComprasItens,
  Frame.DepartamentosSecoesLinhas, FrameMarcas, FrameProdutos, CancelarItensCompras,
  FrameFornecedores, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, BaixarItensCompras,
  FrameEmpresas, Vcl.StdCtrls, GroupBoxLuka, Vcl.Grids, GridLuka, StaticTextLuka,
  CheckBoxLuka, Vcl.Menus, FrameNumeros, InformacoesPedidoCompra, ImpressaoPedidoCompraGrafico,
  FrameGruposFornecedores;

type
  TFormRelacaoCompras = class(TFormHerancaRelatoriosPageControl)
    FrEmpresas: TFrEmpresas;
    FrFornecedores: TFrFornecedores;
    FrProdutos: TFrProdutos;
    FrDataCadastro: TFrDataInicialFinal;
    FrPrevisaoEntrega: TFrDataInicialFinal;
    gbStatus: TGroupBoxLuka;
    StaticTextLuka1: TStaticTextLuka;
    sgCompras: TGridLuka;
    splSeparador: TSplitter;
    StaticTextLuka2: TStaticTextLuka;
    sgItens: TGridLuka;
    ckAbertos: TCheckBoxLuka;
    ckBaixados: TCheckBoxLuka;
    pmOpcoes: TPopupMenu;
    ckCancelados: TCheckBoxLuka;
    FrCodigoCompra: TFrNumeros;
    miN1: TMenuItem;
    FrGruposFornecedores: TFrGruposFornecedores;
    miBaixarItensCompraSelecionada: TSpeedButton;
    miCancelarItensCompra: TSpeedButton;
    miReimprimirPedidoCompra: TSpeedButton;
    procedure sgComprasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgComprasClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure miCancelarItensCompraClick(Sender: TObject);
    procedure miBaixarItensCompraSelecionadaClick(Sender: TObject);
    procedure sgComprasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgComprasDblClick(Sender: TObject);
    procedure miReimprimirPedidoCompraClick(Sender: TObject);
  private
    FItens: TArray<RecComprasItens>;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

const
  (* Grid de Compras *)
  coCompraId             = 0;
  coFornecedor           = 1;
  coStatusAnalitico      = 2;
  coEmpresa              = 3;
  coComprador            = 4;
  coTotalFaturamento     = 5;
  coDataFaturamento      = 6;
  coPrevisaoEntrega      = 7;
  coDataHoraCompra       = 8;
  coUsuarioCadastro      = 9;
  coDataHoraBaixa        = 10;
  coUsuarioBaixa         = 11;
  coDataHoraCancelamento = 12;
  coUsuarioCancelamento  = 13;
  coStatus               = 14;
  coEmpresaId            = 15;
  coTipoLinha            = 16;


  (* Grid de itens *)
  ciProdutoId     = 0;
  ciNome          = 1;
  ciMarca         = 2;
  ciUnidade       = 3;
  ciQuantidade    = 4;
  ciPrecoUnit     = 5;
  ciValorTotal    = 6;
  ciValorDesconto = 7;
  ciValorOutDesp  = 8;
  ciValorLiquido  = 9;
  ciEntregues     = 10;
  ciBaixados      = 11;
  ciCancelados    = 12;
  ciSaldo         = 13;

  ctLinhaNorm   = 'LIN';
  ctTotalizador = 'TOT';

procedure TFormRelacaoCompras.sgComprasClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vCompraId: Integer;
begin
  inherited;
  vLinha := 0;
  sgItens.ClearGrid();
  vCompraId := SFormatInt(sgCompras.Cells[coCompraId, sgCompras.Row]);
  for i := Low(FItens) to High(FItens) do begin
    if vCompraId <> FItens[i].CompraId then
      Continue;

    Inc(vLinha);

    sgItens.Cells[ciProdutoId, vLinha]     := NFormat(FItens[i].ProdutoId);
    sgItens.Cells[ciNome, vLinha]          := FItens[i].NomeProduto;
    sgItens.Cells[ciMarca, vLinha]         := NFormat(FItens[i].MarcaId) + ' - ' + FItens[i].NomeMarca;
    sgItens.Cells[ciUnidade, vLinha]       := FItens[i].UnidadeCompraId;
    sgItens.Cells[ciQuantidade, vLinha]    := NFormatEstoque(FItens[i].Quantidade);
    sgItens.Cells[ciPrecoUnit, vLinha]     := NFormat(FItens[i].PrecoUnitario);
    sgItens.Cells[ciValorTotal, vLinha]    := NFormat(FItens[i].ValorTotal);
    sgItens.Cells[ciValorDesconto, vLinha] := NFormatN(FItens[i].ValorDesconto);
    sgItens.Cells[ciValorOutDesp, vLinha]  := NFormatN(FItens[i].ValorOutrasDespesas);
    sgItens.Cells[ciValorLiquido, vLinha]  := NFormat(FItens[i].ValorLiquido);
    sgItens.Cells[ciEntregues, vLinha]     := NFormatN(FItens[i].Entregues, 3);
    sgItens.Cells[ciBaixados, vLinha]      := NFormatN(FItens[i].Baixados, 3);
    sgItens.Cells[ciCancelados, vLinha]    := NFormatN(FItens[i].Cancelados, 3);
    sgItens.Cells[ciSaldo, vLinha]         := NFormat(FItens[i].Saldo, 4);
  end;
  sgItens.RowCount := IIfInt(vLinha > 1, vLinha + 1, 2);
end;

procedure TFormRelacaoCompras.sgComprasDblClick(Sender: TObject);
begin
  inherited;
  InformacoesPedidoCompra.Informar( SFormatInt(sgCompras.Cells[coCompraId, sgCompras.Row]) );
end;

procedure TFormRelacaoCompras.sgComprasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coCompraId, coTotalFaturamento] then
    vAlinhamento := taRightJustify
  else if ACol = coStatusAnalitico then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgCompras.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoCompras.sgComprasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coStatusAnalitico then begin
    AFont.Style := [fsBold];
    AFont.Color := IIfInt(sgCompras.Cells[coStatus, ARow] = 'A', $000096DB, clBlue);
  end
  else if sgCompras.Cells[coTipoLinha, ARow] = ctTotalizador then begin
    AFont.Style  := [fsBold];
    AFont.Color  := clWhite;
    ABrush.Color := $006C6C6C;
  end;
end;

procedure TFormRelacaoCompras.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vCompras: TArray<RecCompras>;
  vComprasIds: TArray<Integer>;
  vLinha: Integer;
  totalLiquido: Double;
begin
  inherited;
  sgCompras.ClearGrid();
  sgItens.ClearGrid();

  FItens := nil;
  vComprasIds := nil;

  if not FrCodigoCompra.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrCodigoCompra.TrazerFiltros('COM.COMPRA_ID'))
  else begin
    if not FrEmpresas.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrEmpresas.getSqlFiltros('COM.EMPRESA_ID'));

    if not FrProdutos.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, 'COM.COMPRA_ID in(select distinct COMPRA_ID from COMPRAS_ITENS where ' + FrProdutos.getSqlFiltros('PRODUTO_ID') + ')');

    if not FrFornecedores.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrFornecedores.getSqlFiltros('COM.FORNECEDOR_ID'));

    if not FrFornecedores.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrFornecedores.getSqlFiltros('COM.FORNECEDOR_ID'));

    if not FrDataCadastro.NenhumaDataValida then
      _Biblioteca.WhereOuAnd(vSql, FrDataCadastro.getSqlFiltros('trunc(COM.DATA_HORA_COMPRA)') );

    if not FrPrevisaoEntrega.NenhumaDataValida then
      _Biblioteca.WhereOuAnd(vSql, FrDataCadastro.getSqlFiltros('COM.PREVISAO_ENTREGA') );

    if not FrGruposFornecedores.EstaVazio then
      _Biblioteca.WhereOuAnd( vSql, FrDataCadastro.getSqlFiltros('FRN.GRUPO_FORNECEDOR_ID', vSql) );

    _Biblioteca.WhereOuAnd(vSql, gbStatus.GetSql('COM.STATUS'));
  end;

  vSql := vSql + 'order by COM.COMPRA_ID ';

  vCompras := _Compras.BuscarComprasComando(Sessao.getConexaoBanco, vSql);
  if vCompras = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vLinha := 1;
  totalLiquido := 0.0;
  for i := Low(vCompras) to High(vCompras) do begin
    sgCompras.Cells[coCompraId, i + 1]             := NFormatN( vCompras[i].CompraId );
    sgCompras.Cells[coFornecedor, i + 1]           := NFormatN( vCompras[i].FornecedorId ) + ' - ' + vCompras[i].NomeFornecedor;
    sgCompras.Cells[coStatusAnalitico, i + 1]      := vCompras[i].StatusAnalitico;
    sgCompras.Cells[coEmpresa, i + 1]              := NFormatN( vCompras[i].EmpresaId ) + ' - ' + vCompras[i].NomeEmpresa;
    sgCompras.Cells[coComprador, i + 1]            := NFormatN( vCompras[i].CompradorId ) + ' - ' + vCompras[i].NomeComprador;
    sgCompras.Cells[coTotalFaturamento, i + 1]     := NFormat( vCompras[i].ValorLiquido );
    sgCompras.Cells[coDataFaturamento, i + 1]      := DFormat( vCompras[i].DataFaturamento );
    sgCompras.Cells[coPrevisaoEntrega, i + 1]      := DFormat( vCompras[i].PrevisaoEntrega );
    sgCompras.Cells[coDataHoraCompra, i + 1]       := DHFormatN( vCompras[i].DataHoraCompra );
    sgCompras.Cells[coUsuarioCadastro, i + 1]      := NFormatN( vCompras[i].UsuarioCadastroId ) + ' - ' + vCompras[i].NomeComprador;
    sgCompras.Cells[coDataHoraBaixa, i + 1]        := DHFormatN( vCompras[i].DataHoraBaixa );
    sgCompras.Cells[coUsuarioBaixa, i + 1]         := NFormatN( vCompras[i].UsuarioBaixaId );
    sgCompras.Cells[coDataHoraCancelamento, i + 1] := DHFormatN( vCompras[i].CompraId );
    sgCompras.Cells[coUsuarioCancelamento, i + 1]  := NFormatN( vCompras[i].UsuarioCancelamentoId );
    sgCompras.Cells[coStatus, i + 1]               := vCompras[i].Status;
    sgCompras.Cells[coTipoLinha, i + 1]            := ctLinhaNorm;

    totalLiquido := totalLiquido + vCompras[i].ValorLiquido;

    inc(vLinha);

    _Biblioteca.AddNoVetorSemRepetir(vComprasIds, vCompras[i].CompraId);
  end;

  sgCompras.Cells[coTipoLinha, vLinha] := ctTotalizador;
  sgCompras.Cells[coComprador, vLinha] := 'Total L�quido: ';
  sgCompras.Cells[coTotalFaturamento, vLinha] := NFormat(totalLiquido);

  sgCompras.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vCompras) + 1 );

  FItens := _ComprasItens.BuscarComprasItensComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('ITE.COMPRA_ID', vComprasIds));

  sgComprasClick(nil);
  SetarFoco(sgCompras);
end;

procedure TFormRelacaoCompras.FormShow(Sender: TObject);
begin
  inherited;
  FrEmpresas.InserirDadoPorChave( Sessao.getEmpresaLogada.EmpresaId );
  SetarFoco(FrEmpresas);
end;

procedure TFormRelacaoCompras.Imprimir(Sender: TObject);
begin
  inherited;

end;

procedure TFormRelacaoCompras.miBaixarItensCompraSelecionadaClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar;
begin
  inherited;
  vRetTela := BaixarItensCompras.Baixar( SFormatInt(sgCompras.Cells[coCompraId, sgCompras.Row]), SFormatInt(sgCompras.Cells[coEmpresaId, sgCompras.Row]) );
  if vRetTela.BuscaCancelada then
    Exit;

  Carregar(Sender);
end;

procedure TFormRelacaoCompras.miCancelarItensCompraClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar;
begin
  inherited;

  vRetTela := CancelarItensCompras.Cancelar(SFormatInt( sgCompras.Cells[coCompraid, sgCompras.Row] ));
  if vRetTela.BuscaCancelada then
    Exit;

  Carregar(Sender);
end;

procedure TFormRelacaoCompras.miReimprimirPedidoCompraClick(Sender: TObject);
begin
  inherited;
  ImpressaoPedidoCompraGrafico.Imprimir( SFormatInt( sgCompras.Cells[coCompraid, sgCompras.Row] ) );
end;

procedure TFormRelacaoCompras.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if gbStatus.NenhumMarcado then begin
    _Biblioteca.Exclamar('Pelo menos 1 status tem que ser marcado, verifique!');
    SetarFoco(gbStatus);
    Abort;
  end;
end;

procedure TFormRelacaoCompras.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    ciProdutoId,
    ciQuantidade,
    ciPrecoUnit,
    ciValorTotal,
    ciValorDesconto,
    ciValorOutDesp,
    ciValorLiquido,
    ciEntregues,
    ciBaixados,
    ciCancelados,
    ciSaldo]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
