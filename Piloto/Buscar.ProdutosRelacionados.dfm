inherited FormBuscarProdutosRelacionados: TFormBuscarProdutosRelacionados
  Caption = 'Busca de produtos relacionados'
  ClientHeight = 253
  ClientWidth = 544
  ExplicitWidth = 550
  ExplicitHeight = 282
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 216
    Width = 544
    ExplicitTop = 216
    ExplicitWidth = 544
  end
  object sgProdutosRelacionados: TGridLuka
    Left = 2
    Top = 2
    Width = 538
    Height = 213
    Align = alCustom
    ColCount = 4
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goEditing]
    TabOrder = 1
    OnDrawCell = sgProdutosRelacionadosDrawCell
    OnKeyDown = sgProdutosRelacionadosKeyDown
    OnKeyPress = NumerosPonto
    OnSelectCell = sgProdutosRelacionadosSelectCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Produto'
      'Nome'
      'Qtde. sugerida'
      'Qtde. adicionar')
    OnArrumarGrid = sgProdutosRelacionadosArrumarGrid
    Grid3D = False
    RealColCount = 4
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      54
      274
      93
      90)
  end
end
