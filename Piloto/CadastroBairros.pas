unit CadastroBairros;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Biblioteca, _Bairros,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _FrameHerancaPrincipal, _RecordsEspeciais,
  _FrameHenrancaPesquisas, FrameCidades, _Sessao, _RecordsCadastros, PesquisaBairros,
  Vcl.Mask, EditLukaData, FrameRotas, CheckBoxLuka;

type
  TFormCadastroBairros = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eNome: TEditLuka;
    FrCidade: TFrCidades;
    FrRota: TFrRotas;
  private
    procedure PreencherRegistro(pBairro: RecBairros);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroBairros }

procedure TFormCadastroBairros.BuscarRegistro;
var
  bairros: TArray<RecBairros>;
begin
  bairros := _Bairros.BuscarBairros(Sessao.getConexaoBanco, 0, [eID.AsInt], 0);

  if bairros = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    SetarFoco(eID);
    Exit;
  end;

  inherited;
  PreencherRegistro(bairros[0]);
end;

procedure TFormCadastroBairros.ExcluirRegistro;
var
  retorno: RecRetornoBD;
begin
  retorno := _Bairros.ExcluirBairro(Sessao.getConexaoBanco, eID.AsInt);

  if retorno.TeveErro then begin
    _Biblioteca.Exclamar(retorno.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroBairros.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  vRotaId: Integer;
begin
  inherited;
  vRotaId := 0;
  if not FrRota.EstaVazio then
    vRotaId := FrRota.GetRota().RotaId;

  vRetBanco :=
    _Bairros.AtualizarBairro(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eNome.Text,
      FrCidade.GetCidade.cidade_id,
      vRotaId,
      _Biblioteca.ToChar(ckAtivo)
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormCadastroBairros.Modo(pEditando: Boolean);
begin
  inherited;

  _Biblioteca.Habilitar([
    FrCidade,
    eNome,
    FrRota,
    ckAtivo],
    pEditando
  );

  if pEditando then begin
    SetarFoco(eNome);
    ckAtivo.Checked := True;
  end;
end;

procedure TFormCadastroBairros.PesquisarRegistro;
var
  vBairro: RecBairros;
begin
  vBairro := RecBairros(PesquisaBairros.PesquisarBairro(False));
  if vBairro = nil then
    Exit;

  inherited;
  PreencherRegistro(vBairro);
end;

procedure TFormCadastroBairros.PreencherRegistro(pBairro: RecBairros);
begin
  eID.AsInt       := pBairro.bairro_id;
  eNome.Text      := pBairro.nome;
  ckAtivo.Checked := (pBairro.ativo = 'S');
  FrCidade.InserirDadoPorChave(pBairro.cidade_id, False);
  FrRota.InserirDadoPorChave(pBairro.rota_id, False);

  pBairro.Free;
end;

procedure TFormCadastroBairros.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eNome.Trim = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar o nome da cidade!');
    SetarFoco(eNome);
    Abort;
  end;

  if FrCidade.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio informar a cidade do bairro!');
    SetarFoco(FrCidade);
    Abort;
  end;
end;

end.
