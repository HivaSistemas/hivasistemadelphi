unit CadastroProdutos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, EditLuka, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.Mask, EditLukaData, _FrameHerancaPrincipal, _FrameHenrancaPesquisas,
  FrameFornecedores, FrameMarcas, FrameUnidades, _Biblioteca, _Sessao, _RecordsEspeciais,
  FrameICMS, _Produtos, _RecordsCadastros, PesquisaProdutos, _ProdutosRelacionados,
  FrameProdutos, _Empresas, FrameEmpresas, _PrecosProdutos, ComboBoxLuka, FrameImagem,
  _CustosProdutos, Frame.DepartamentosSecoesLinhas, Frame.HerancaInsercaoExclusao,
  Frame.ConversaoUnidadesCompras, _ProdutosMultiplosCompras, Frame.PisCofins, Logs,
  CheckBoxLuka, GroupBoxLuka, Vcl.Grids, GridLuka, System.Math, _ProdutosKit,
  StaticTextLuka, Vcl.Menus, FrameCodigoOriginalFornecedores, _ProdutosFornCodOriginais,
  FrameOrdenacaoLocais, FrameProdutosGruposTributacoesEmpresas, _ProdutosGruposTribEmpresas,
  FrameCodigoBarrasAuxiliar, _ProdutosCodigosBarrasAux, Frame.EnderecoEstoque;

type
  TFormCadastroProdutos = class(TFormHerancaCadastroCodigo)
    lbl1: TLabel;
    eNomeProduto: TEditLuka;
    pcDados: TPageControl;
    tsPrincipal: TTabSheet;
    eDataCadastro: TEditLukaData;
    lb17: TLabel;
    FrFabricante: TFrFornecedores;
    tsComplementos: TTabSheet;
    stCaracteristicas: TStaticText;
    eCaracteristicas: TMemo;
    FrUnidadeVenda: TFrUnidades;
    FrMarca: TFrMarcas;
    lb1: TLabel;
    eMultiploVenda: TEditLuka;
    ePeso: TEditLuka;
    lb2: TLabel;
    eVolume: TEditLuka;
    lb3: TLabel;
    ckAceitarEstoqueNegativo: TCheckBox;
    ckInativarProdutoZerarEstoque: TCheckBox;
    ckBloquearParaCompras: TCheckBox;
    ckBloquearParaVendas: TCheckBox;
    eCodigoBarras: TEditLuka;
    lb5: TLabel;
    tsParametrosCompra: TTabSheet;
    lb6: TLabel;
    eNomeCompra: TEditLuka;
    sbCopiarProduto: TSpeedButton;
    FrLinhaProduto: TFrameDepartamentosSecoesLinhas;
    FrMultiplosCompra: TFrameConversaoUnidadesCompra;
    ckProdutoDiversoPDV: TCheckBox;
    ckPermitirDevolucao: TCheckBox;
    eCodigoBalanca: TEditLuka;
    Label3: TLabel;
    tsControleEstoque: TTabSheet;
    FrProdutoKit: TFrProdutos;
    eQuantidadeKit: TEditLuka;
    Label4: TLabel;
    sgProdutosKit: TGridLuka;
    lbNomePesquisa: TLabel;
    ckExigirDataFabricacaoLote: TCheckBoxLuka;
    ckExigirDataVencimentoLote: TCheckBoxLuka;
    cbTipoControleEstoque: TComboBoxLuka;
    FrUnidadeEntrega: TFrUnidades;
    cbDefinicaoAutomaticaLote: TComboBoxLuka;
    Label5: TLabel;
    ckSobEncomenda: TCheckBox;
    ckRevenda: TCheckBox;
    ckUsoConsumo: TCheckBox;
    Label6: TLabel;
    eDiasAvisoVencimento: TEditLuka;
    ckSujeitoComissao: TCheckBox;
    GroupBoxLuka1: TGroupBoxLuka;
    ePercentualComissaoVista: TEditLuka;
    lb14: TLabel;
    ePercentualComissaoPrazo: TEditLuka;
    lb13: TLabel;
    ckBloquearEntradaSemPedCompra: TCheckBox;
    ckExigirSeparacao: TCheckBox;
    Label7: TLabel;
    eValorAdicionalFrete: TEditLuka;
    ckExigirModeloNota: TCheckBox;
    FrCodigoOriginalFornecedores: TFrCodigoOriginalFornecedores;
    lbl2: TLabel;
    eCodigoOriginalFabricante: TEditLuka;
    tsFiscal: TTabSheet;
    lb4: TLabel;
    eNCM: TEditLuka;
    eCEST: TEditLuka;
    lb7: TLabel;
    FrProdutosGruposTributacoes: TFrProdutosGruposTributacoesEmpresas;
    ckSepararSomenteCodigoBarras: TCheckBoxLuka;
    FrFoto1: TFrImagem;
    FrFoto2: TFrImagem;
    FrFoto3: TFrImagem;
    stctxtlk1: TStaticTextLuka;
    stctxtlk2: TStaticTextLuka;
    stctxtlk3: TStaticTextLuka;
    ckAtivoImobilizado: TCheckBox;
    lbl3: TLabel;
    eMotivoInatividade: TEditLuka;
    ckServico: TCheckBox;
    FrCodigoBarrasAuxiliar: TFrCodigoBarrasAuxiliar;
    ckControlaPeso: TCheckBoxLuka;
    Label2: TLabel;
    eMultiploVenda2: TEditLuka;
    Label8: TLabel;
    eMultiploVenda3: TEditLuka;
    TabSheet1: TTabSheet;
    Label9: TLabel;
    ePrecoVenda: TEditLuka;
    Label10: TLabel;
    eCustoCompra: TEditLuka;
    TabSheet2: TTabSheet;
    frEnderecoEstoque: TFrEnderecoEstoque;
    StaticTextLuka1: TStaticTextLuka;
    sgCustoCompraFixo: TGridLuka;
    ckEmitirSomenteNFE: TCheckBox;
    eCodigoSistemaAnterior: TEditLuka;
    Label11: TLabel;
    ckExportarWeb: TCheckBox;

    procedure sbCopiarProdutoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ckPermitirDevolucaoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eNomeProdutoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cbTipoControleEstoqueChange(Sender: TObject);
    procedure sgProdutosKitDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure eQuantidadeKitKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgProdutosKitSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure sgProdutosKitGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgProdutosKitArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
    procedure sgProdutosKitKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgCustoCompraFixoDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgCustoCompraFixoSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure sgCustoCompraFixoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sgCustoCompraFixoGetCellColor(Sender: TObject; ARow,
      ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    FNovoCadastro: Boolean;
    FProdutoNovoID: Integer;
    FProcessoEntradaNotaFiscal: Boolean;

    procedure PreencherRegistro(pProduto: RecProdutos);
    procedure AddProdutoGridKit(pProdutoId: Integer; pNomeProduto: string; pQuantidade: Double; pParticipacao: Double; pCarregandoProduto: Boolean);
    procedure ControlarTipoControleEstoque(pNovoCadastro: Boolean; pTipoControleEstoque: string);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
    procedure ExibirLogs; override;
  end;

function CadastrarProdutoEntradaNota(
  pCNPJFornecedor: string;
  pFornecedorId: Integer;
  pNomeFantasiaFornecedor: string;
  const pNomeProduto: string;
  const pNCM: string;
  const pCest: string;
  const pCodigoBarras: string;
  pCodigoOriginal: string;
  pUnidade: string
): TRetornoTelaFinalizar<Integer>;

implementation

{$R *.dfm}

{ TFormCadastroProdutos }

const
  coKitProdutoId    = 0;
  coNomeProdutoKit  = 1;
  coQuantidadeKit   = 2;
  coParticipacaoKit = 3;

const
  cfEmpresaId            = 0;
  cfEmpresaNome          = 1;
  cfCusto                = 2;
  cfZerarCustoAnalitico  = 3;
  cfZerarCustoSintetico  = 4;

function CadastrarProdutoEntradaNota(
  pCNPJFornecedor: string;
  pFornecedorId: Integer;
  pNomeFantasiaFornecedor: string;
  const pNomeProduto: string;
  const pNCM: string;
  const pCest: string;
  const pCodigoBarras: string;
  pCodigoOriginal: string;
  pUnidade: string
): TRetornoTelaFinalizar<Integer>;
var
  vForm: TFormCadastroProdutos;
  vCodigoOriginalFornecedor: TArray<RecProdutosFornCodOriginais>;
begin
  vForm := TFormCadastroProdutos.Create(Application);
  vForm.FProcessoEntradaNotaFiscal := True;

  if _Biblioteca.Perguntar('Deseja utilizar um produto como base para cadastro do produto ' + pNomeProduto + '?') then
    vForm.sbCopiarProduto.Click;

  vForm.ckAtivo.Checked    := True;
  vForm.eNomeProduto.Text  := pNomeProduto;
  vForm.eNomeCompra.Text   := pNomeProduto;
  vForm.eCodigoBarras.Text := pCodigoBarras;

  SetLength(vCodigoOriginalFornecedor, 1);
  vCodigoOriginalFornecedor[0].FornecedorId   := pFornecedorId;
  vCodigoOriginalFornecedor[0].NomeFornecedor := pNomeFantasiaFornecedor;
  vCodigoOriginalFornecedor[0].CodigoOriginal := pCodigoOriginal;
  vForm.FrCodigoOriginalFornecedores.Codigos := vCodigoOriginalFornecedor;

  vForm.eNCM.Text  := pNCM;
  vForm.eCEST.Text := pCEST;

  if not vForm.FrFabricante.EstaVazio then begin
    if _Biblioteca.EMatrizFilial( vForm.FrFabricante.GetFornecedor.cadastro.cpf_cnpj, pCNPJFornecedor ) then
      vForm.eCodigoOriginalFabricante.Text := pCodigoOriginal;
  end;

  vForm.FrUnidadeVenda.InserirDadoPorChave( pUnidade, False );

  vForm.FormStyle := fsNormal;
  vForm.Visible := False;

  if Result.Ok(vForm.ShowModal, True) then
    Result.Dados := vForm.FProdutoNovoID;
end;

procedure TFormCadastroProdutos.BuscarRegistro;
var
  produtos: TArray<RecProdutos>;
begin
  produtos := _Produtos.BuscarProdutos(Sessao.getConexaoBanco, 0, [eId.AsInt], False, '');

  if produtos = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    SetarFoco(eID);
    Exit;
  end;

  inherited;
  FNovoCadastro := False;
  PreencherRegistro(produtos[0]);
end;

procedure TFormCadastroProdutos.cbTipoControleEstoqueChange(Sender: TObject);
begin
  inherited;
  ControlarTipoControleEstoque(FNovoCadastro, cbTipoControleEstoque.GetValor);
end;

procedure TFormCadastroProdutos.ckPermitirDevolucaoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  SetarFoco(eCaracteristicas, Key);
end;

procedure TFormCadastroProdutos.ControlarTipoControleEstoque(pNovoCadastro: Boolean; pTipoControleEstoque: string);
begin
  if Em(cbTipoControleEstoque.GetValor, ['L', 'G']) then begin
    _Biblioteca.Habilitar([FrProdutoKit, eQuantidadeKit, sgProdutosKit], False, False);
    _Biblioteca.Habilitar([ckExigirDataFabricacaoLote, ckExigirDataVencimentoLote], True, False);
  end
  else if cbTipoControleEstoque.GetValor = 'P' then
    _Biblioteca.Habilitar([FrProdutoKit, eQuantidadeKit, sgProdutosKit, ckExigirDataFabricacaoLote, ckExigirDataVencimentoLote], False, False)
  else if Em(cbTipoControleEstoque.GetValor, ['K', 'A']) then begin
    _Biblioteca.Habilitar([ckExigirDataFabricacaoLote, ckExigirDataVencimentoLote], False, True);
    _Biblioteca.Habilitar([FrProdutoKit, eQuantidadeKit, sgProdutosKit], True, False);

    if not pNovoCadastro then
      _Biblioteca.Habilitar([FrProdutoKit, eQuantidadeKit], False, False);
  end
  else if Em(cbTipoControleEstoque.GetValor, ['I']) then begin
    _Biblioteca.Habilitar([FrProdutoKit, eQuantidadeKit, sgProdutosKit], True, False);
  end
  else begin
    _Biblioteca.Habilitar([ckExigirDataFabricacaoLote, ckExigirDataVencimentoLote], False, True);
    _Biblioteca.Habilitar([FrProdutoKit, eQuantidadeKit, sgProdutosKit, ckExigirDataFabricacaoLote, ckExigirDataVencimentoLote], False, False);
  end;

//  _Biblioteca.Habilitar([cbTipoControleEstoque], pNovoCadastro, False);
end;

procedure TFormCadastroProdutos.eNomeProdutoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  SetarFoco(FrFabricante, Key);
end;

procedure TFormCadastroProdutos.eQuantidadeKitKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;

  if Key <> VK_RETURN then
    Exit;

  if FrProdutoKit.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio informar o produto para compor o kit!');
    SetarFoco(FrProdutoKit);
    Exit;
  end;

  if eQuantidadeKit.AsCurr = 0 then begin
    Exclamar('� necess�rio informar a quantidade do produto que vai compor o kit!');
    SetarFoco(eQuantidadeKit);
    Exit;
  end;

  AddProdutoGridKit(
    FrProdutoKit.getProduto().produto_id,
    FrProdutoKit.getProduto().nome,
    eQuantidadeKit.AsDouble,
    0,
    False
  );
end;

procedure TFormCadastroProdutos.AddProdutoGridKit(pProdutoId: Integer; pNomeProduto: string; pQuantidade: Double; pParticipacao: Double; pCarregandoProduto: Boolean);
var
  vLinha: Integer;
begin
  vLinha := sgProdutosKit.Localizar([coKitProdutoId], [NFormat(pProdutoId)]);
  if vLinha > -1 then begin
    _Biblioteca.Exclamar('Produto j� inserido na linha ' + NFormat(vLinha) + '!');
    sgProdutosKit.Row := vLinha;
    SetarFoco(FrProdutoKit);
    eQuantidadeKit.Clear;
    Exit;
  end;

  if sgProdutosKit.Cells[coKitProdutoId, 1] = '' then
    vLinha := 1
  else
    vLinha := sgProdutosKit.RowCount;

  sgProdutosKit.Cells[coKitProdutoId, vLinha]    := NFormat(pProdutoId);
  sgProdutosKit.Cells[coNomeProdutoKit, vLinha]  := pNomeProduto;
  sgProdutosKit.Cells[coQuantidadeKit, vLinha]   := NFormat(pQuantidade, 4);
  sgProdutosKit.Cells[coParticipacaoKit, vLinha] := NFormat(pParticipacao, 4);

  sgProdutosKit.RowCount := IfThen(vLinha > 1, vLinha + 1, 2);

  if not pCarregandoProduto then begin
    _Biblioteca.LimparCampos([FrProdutoKit, eQuantidadeKit]);
    sgProdutosKit.Row := vLinha;
    SetarFoco(FrProdutoKit);
  end;
end;

procedure TFormCadastroProdutos.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _Produtos.ExcluirProduto(Sessao.getConexaoBanco, eID.AsInt);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroProdutos.ExibirLogs;
begin
  inherited;
  Logs.Iniciar('LOGS_PRODUTOS', ['PRODUTO_ID'], 'VW_TIPOS_ALTER_LOGS_PRODUTOS', [eId.AsInt]);
end;

procedure TFormCadastroProdutos.FormCreate(Sender: TObject);
begin
  inherited;
  FProcessoEntradaNotaFiscal := False;
  frEnderecoEstoque.sgPesquisa.ColWidths[0] := 25;
  frEnderecoEstoque.Visible := Sessao.getParametros.UtilEnderecoEstoque = 'S';
end;

procedure TFormCadastroProdutos.FormShow(Sender: TObject);
begin
  inherited;
  if FProcessoEntradaNotaFiscal then begin
    SetarFoco(eID);
    keybd_event(VK_RETURN, 0, 0, 0);
  end;
end;

procedure TFormCadastroProdutos.GravarRegistro(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  vEmpresasEstoque: TArray<Integer>;
  vProdutosKit: TArray<RecProdutosKit>;
  vUnidadeEntregaId: string;
  enderecos: TArray<Integer>;
  custoFixo: TArray<RecProdutoCustosFixo>;
begin
  inherited;

  SetLength(vEmpresasEstoque, 1);
  vEmpresasEstoque[0] := Sessao.getEmpresaLogada.EmpresaId;
  if eID.AsInt = 0 then
    vEmpresasEstoque := _Empresas.BuscarIdsEmpresas(Sessao.getConexaoBanco);

  vProdutosKit := nil;
  if Em(cbTipoControleEstoque.GetValor, ['K', 'A', 'I']) then begin
    for i := 1 to sgProdutosKit.RowCount -1 do begin
      SetLength(vProdutosKit, Length(vProdutosKit) + 1);

      vProdutosKit[High(vProdutosKit)].Ordem            := i;
      vProdutosKit[High(vProdutosKit)].ProdutoId        := SFormatInt(sgProdutosKit.Cells[coKitProdutoId, i]);
      vProdutosKit[High(vProdutosKit)].Quantidade       := SFormatDouble(sgProdutosKit.Cells[coQuantidadeKit, i]);
      vProdutosKit[High(vProdutosKit)].PercParticipacao := SFormatDouble(sgProdutosKit.Cells[coParticipacaoKit, i]);
    end;
  end;

  if eID.AsInt <> 0 then begin
    for i := 1 to sgCustoCompraFixo.RowCount -1 do begin
      SetLength(custoFixo, Length(custoFixo) + 1);

      custoFixo[High(custoFixo)].EmpresaId  := SFormatInt(sgCustoCompraFixo.Cells[cfEmpresaId, i]);
      custoFixo[High(custoFixo)].ProdutoId  := eID.AsInt;
      custoFixo[High(custoFixo)].CustoFixo  := SFormatDouble(sgCustoCompraFixo.Cells[cfCusto, i]);
      custoFixo[High(custoFixo)].ZerarCusto := sgCustoCompraFixo.Cells[cfZerarCustoSintetico, i];
    end;
  end;

  vUnidadeEntregaId := '';
  if not FrUnidadeEntrega.EstaVazio then
    vUnidadeEntregaId := FrUnidadeEntrega.getUnidade().unidade_id;

  if not frEnderecoEstoque.EstaVazio then
    enderecos := frEnderecoEstoque.GetArrayOfInteger;

  vRetBanco :=
    _Produtos.AtualizarProduto(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eNomeProduto.Text,
      vEmpresasEstoque,
      eNCM.Text,
      FrMarca.GetMarca.marca_id,
      FrFabricante.GetFornecedor.cadastro_id,
      eMultiploVenda.AsDouble,
      ePeso.AsDouble,
      eVolume.AsDouble,
      FrUnidadeVenda.getUnidade.unidade_id,
      _Biblioteca.ToChar(ckBloquearParaCompras),
      _Biblioteca.ToChar(ckBloquearParaVendas),
      _Biblioteca.ToChar(ckPermitirDevolucao),
      eCaracteristicas.Lines.Text,
      eNomeCompra.Text,
      _Biblioteca.ToChar(ckAceitarEstoqueNegativo),
      eCodigoBarras.Text,
      eCodigoBalanca.Text,
      FrLinhaProduto.GetProdutoDeptoSecaoLinha().DeptoSecaoLinhaId,
      _Biblioteca.ToChar(ckInativarProdutoZerarEstoque),
      eCEST.Text,
      FrFoto1.getImagem,
      FrFoto2.getImagem,
      FrFoto3.getImagem,
      FrMultiplosCompra.Multiplos,
      _Biblioteca.ToChar(ckAtivo),
      _Biblioteca.ToChar(ckProdutoDiversoPDV),
      cbTipoControleEstoque.GetValor,
      _Biblioteca.ToChar(ckExigirDataFabricacaoLote),
      _Biblioteca.ToChar(ckExigirDataVencimentoLote),
      vProdutosKit,
      _Biblioteca.ToChar(ckSobEncomenda),
      _Biblioteca.ToChar(ckRevenda),
      _Biblioteca.ToChar(ckUsoConsumo),
      _Biblioteca.ToChar(ckAtivoImobilizado),
      _Biblioteca.ToChar(ckServico),
      _Biblioteca.ToChar(ckSujeitoComissao),
      _Biblioteca.ToChar(ckBloquearEntradaSemPedCompra),
      _Biblioteca.ToChar(ckExigirModeloNota),
      _Biblioteca.ToChar(ckExigirSeparacao),
      eDiasAvisoVencimento.AsInt,
      eMotivoInatividade.Text,
      eValorAdicionalFrete.AsDouble,
      ePercentualComissaoVista.AsDouble,
      ePercentualComissaoPrazo.AsDouble,
      eCodigoOriginalFabricante.Trim,
      vUnidadeEntregaId,
      ckSepararSomenteCodigoBarras.CheckedStr,
      FrCodigoOriginalFornecedores.Codigos,
      cbDefinicaoAutomaticaLote.GetValor,
      FrProdutosGruposTributacoes.ProdutosGruposTribEmpresas,
      FrCodigoBarrasAuxiliar.CodigoBarras,
      _Biblioteca.ToChar(ckControlaPeso),
      eMultiploVenda2.AsDouble,
      eMultiploVenda3.AsDouble,
      IIfStr(ckEmitirSomenteNFE.Checked, 'S', 'N'),
      enderecos,
      custoFixo,
      IIfStr(ckExportarWeb.Checked, 'S', 'N')
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);

  if FProcessoEntradaNotaFiscal then begin
    FProdutoNovoID := vRetBanco.AsInt;
    ModalResult := mrOk;
  end;
end;

procedure TFormCadastroProdutos.Modo(pEditando: Boolean);
begin
  inherited;
  FNovoCadastro := True;

  _Biblioteca.Habilitar([
    pcDados,
    eNomeProduto,
    eNomeCompra,
    FrMultiplosCompra,
    FrFabricante,
    FrMarca,
    FrUnidadeVenda,
    eMultiploVenda,
    eMultiploVenda2,
    eMultiploVenda3,
    ePeso,
    eVolume,
    eCodigoBarras,
    eCodigoBalanca,
    ckEmitirSomenteNFE,
    ckExportarWeb,
    FrLinhaProduto,
    eNCM,
    ckAceitarEstoqueNegativo,
    ckInativarProdutoZerarEstoque,
    ckBloquearParaCompras,
    ckBloquearParaVendas,
    ckPermitirDevolucao,
    ckProdutoDiversoPDV,
    eCaracteristicas,
    FrUnidadeEntrega,
    eCEST,
    ckAtivo,
    FrFoto1,
    FrFoto2,
    FrFoto3,
    sbLogs,
    cbTipoControleEstoque,
    ckExigirDataFabricacaoLote,
    ckExigirDataVencimentoLote,
    sgProdutosKit,
    cbDefinicaoAutomaticaLote,
    ckSobEncomenda,
    eDiasAvisoVencimento,
    eMotivoInatividade,
    ckRevenda,
    ckUsoConsumo,
    ckAtivoImobilizado,
    ckServico,
    ckSujeitoComissao,
    ckBloquearEntradaSemPedCompra,
    ckExigirModeloNota,
    ckExigirSeparacao,
    eValorAdicionalFrete,
    ePercentualComissaoVista,
    ePercentualComissaoPrazo,
    FrCodigoOriginalFornecedores,
    eCodigoOriginalFabricante,
    FrProdutosGruposTributacoes,
    sgCustoCompraFixo,
    ckSepararSomenteCodigoBarras,
    FrCodigoBarrasAuxiliar,
    ckControlaPeso,
    ePrecoVenda,
    eCustoCompra,
    frEnderecoEstoque],
    pEditando
  );

  _Biblioteca.Habilitar([sbCopiarProduto], not pEditando, False);

  if pEditando then begin
    SetarFoco(eNomeProduto);
    ckAtivo.Checked := True;
    cbTipoControleEstoque.SetIndicePorValor('N');
  end;
end;

procedure TFormCadastroProdutos.PesquisarRegistro;
var
  vProduto: RecProdutos;
begin
  vProduto := RecProdutos(PesquisaProdutos.PesquisarProduto(False, False));
  if vProduto = nil then
    Exit;

  inherited;
  PreencherRegistro(vProduto);
  ControlarTipoControleEstoque(False, cbTipoControleEstoque.GetValor);
end;

procedure TFormCadastroProdutos.PreencherRegistro(pProduto: RecProdutos);
var
  i: Integer;
  vProdutosKit: TArray<RecProdutosKit>;
  precoVenda, custoCompra: Double;
  enderecos: TArray<Integer>;
  custoFixo: TArray<RecProdutoCustosFixo>;
begin
  eID.AsInt            := pProduto.produto_id;
  eNomeProduto.Text    := pProduto.nome;
  eDataCadastro.AsData := pProduto.data_cadastro;
  ckAtivo.Checked      := (pProduto.ativo = 'S');
  eCodigoSistemaAnterior.Text := pProduto.ChaveImportacao;

  // Principais
  FrFabricante.InserirDadoPorChave(pProduto.fornecedor_id, False);
  FrMarca.InserirDadoPorChave(pProduto.marca_id, False);
  FrUnidadeVenda.InserirDadoPorChave(pProduto.unidade_venda);
  eMultiploVenda.AsDouble := pProduto.multiplo_venda;
  eMultiploVenda2.AsDouble := pProduto.multiplo_venda_2;
  eMultiploVenda3.AsDouble := pProduto.multiplo_venda_3;
  ePeso.AsDouble := pProduto.peso;
  eVolume.AsDouble := pProduto.volume;
  FrLinhaProduto.InserirDadoPorChave(pProduto.LinhaProdutoId);
  eCodigoBarras.Text := pProduto.codigo_barras;
  eCodigoBalanca.Text := pProduto.codigoBalanca;
  eValorAdicionalFrete.AsDouble := pProduto.ValorAdicionalFrete;
  ckAceitarEstoqueNegativo.Checked := (pProduto.aceitar_estoque_negativo = 'S');
  ckInativarProdutoZerarEstoque.Checked := (pProduto.inativar_zerar_estoque = 'S');
  ckBloquearParaCompras.Checked := (pProduto.bloqueado_compras = 'S');
  ckBloquearParaVendas.Checked := (pProduto.bloqueado_vendas = 'S');
  ckSujeitoComissao.Checked := (pProduto.SujeitoComissao = 'S');
  ePercentualComissaoVista.AsDouble := pProduto.PercentualComissaoVista;
  ePercentualComissaoPrazo.AsDouble := pProduto.PercentualComissaoPrazo;
  ckPermitirDevolucao.Checked := (pProduto.permitir_devolucao = 'S');
  ckProdutoDiversoPDV.Checked := (pProduto.produtoDiversosPDV = 'S');
  ckSobEncomenda.Checked := (pProduto.SobEncomenda = 'S');
  ckBloquearEntradaSemPedCompra.Checked := (pProduto.BloquearEntradSemPedCompra = 'S');
  ckExigirSeparacao.Checked := (pProduto.ExigirSeparacao = 'S');
  ckExigirModeloNota.Checked := (pProduto.NaoExigirModeloNotaFiscal = 'S');
  ckSepararSomenteCodigoBarras.CheckedStr := pProduto.ConfSomenteCodigoBarras;
  ckControlaPeso.Checked := (pProduto.ControlaPeso = 'S');
  ckEmitirSomenteNFE.Checked := pProduto.emitir_somente_nfe = 'S';
  ckExportarWeb.Checked := pProduto.exportar_web = 'S';

  FrFoto1.setFoto(pProduto.foto_1);
  FrFoto2.setFoto(pProduto.foto_2);
  FrFoto3.setFoto(pProduto.foto_3);

  // Complementos
  eCaracteristicas.Text := pProduto.caracteristicas;
  ckRevenda.Checked := (pProduto.Revenda = 'S');
  ckUsoConsumo.Checked := (pProduto.UsoConsumo = 'S');
  ckAtivoImobilizado.Checked := (pProduto.AtivoImobilizado = 'S');
  ckServico.Checked := (pProduto.Servico = 'S');

  // Parametros de compra
  eNomeCompra.Text               := pProduto.nome_compra;
  eCodigoOriginalFabricante.Text := pProduto.CodigoOriginalFabricante;
  FrMultiplosCompra.Multiplos := _ProdutosMultiplosCompras.BuscarProdutoMultiploCompras(Sessao.getConexaoBanco, 0, [pProduto.produto_id]);
  FrCodigoOriginalFornecedores.Codigos := _ProdutosFornCodOriginais.BuscarCodigosOriginais(Sessao.getConexaoBanco, 0, [pProduto.produto_id]);
  FrCodigoBarrasAuxiliar.CodigoBarras  := _ProdutosCodigosBarrasAux.BuscarProdutosCodigosBarrasAux(Sessao.getConexaoBanco, 0, [pProduto.produto_id]);

  // Controle de estoque
  cbTipoControleEstoque.SetIndicePorValor(pProduto.TipoControleEstoque);
  cbTipoControleEstoqueChange(nil);
  ckExigirDataFabricacaoLote.Checked := pProduto.ExigirDataFabricacaoLote = 'S';
  ckExigirDataVencimentoLote.Checked := pProduto.ExigirDataVencimentoLote = 'S';
  cbDefinicaoAutomaticaLote.SetIndicePorValor( pProduto.TipoDefAutomaticaLote );
  eDiasAvisoVencimento.AsInt := pProduto.DiasAvisoVencimento;
  eMotivoInatividade.Text    := pProduto.MotivoInatividade;
  FrUnidadeEntrega.InserirDadoPorChave( pProduto.UnidadeEntregaId, False );

  if Em(cbTipoControleEstoque.GetValor, ['K', 'A', 'I']) then begin
    vProdutosKit := _ProdutosKit.BuscarProdutosKit(Sessao.getConexaoBanco, 0, [eID.AsInt]);
    for i := Low(vProdutosKit) to High(vProdutosKit) do begin
      AddProdutoGridKit(
        vProdutosKit[i].ProdutoId,
        vProdutosKit[i].NomeProduto,
        vProdutosKit[i].Quantidade,
        vProdutosKit[i].PercParticipacao,
        True
      );
    end;
  end;

  // Fiscal
  eNCM.Text := pProduto.codigo_ncm;
  eCEST.Text := pProduto.cest;

  FrProdutosGruposTributacoes.ProdutosGruposTribEmpresas := _ProdutosGruposTribEmpresas.BuscarProdutosGruposTribEmpresas(Sessao.getConexaoBanco, 0, [pProduto.produto_id]);

  precoVenda := 0;
  custoCompra := 0;

  _Produtos.BuscarPrecoVendaCustoCompra(Sessao.getConexaoBanco, Sessao.getEmpresaLogada.EmpresaId, pProduto.produto_id, precoVenda, custoCompra);
  ePrecoVenda.AsDouble := precoVenda;
  eCustoCompra.AsDouble := custoCompra;

  enderecos := _Produtos.BuscarEnderecosEstoque(Sessao.getConexaoBanco, pProduto.produto_id);
  for i := 0 to Length(enderecos) - 1 do
    frEnderecoEstoque.InserirDadoPorChave(enderecos[i]);

  sgCustoCompraFixo.ClearGrid();
  custoFixo := _Produtos.buscarCustoFixoProdutosPorEmpresa(Sessao.getConexaoBanco, pProduto.produto_id);
  for i := 0 to Length(custoFixo) - 1 do begin
    sgCustoCompraFixo.Cells[cfEmpresaId, i + 1]   := NFormat(custoFixo[i].EmpresaId);
    sgCustoCompraFixo.Cells[cfEmpresaNome, i + 1] := custoFixo[i].EmpresaNome;
    if custoFixo[i].CustoFixo <> 0 then
      sgCustoCompraFixo.Cells[cfCusto, i + 1] := NFormatN(custoFixo[i].CustoFixo, 2)
    else
      sgCustoCompraFixo.Cells[cfCusto, i + 1] := '0';

    sgCustoCompraFixo.Cells[cfZerarCustoSintetico, i + 1] := custoFixo[i].ZerarCusto;
    if custoFixo[i].ZerarCusto = 'S' then
      sgCustoCompraFixo.Cells[cfZerarCustoAnalitico, i + 1] := 'Sim'
    else
      sgCustoCompraFixo.Cells[cfZerarCustoAnalitico, i + 1] := 'N�o';

    sgCustoCompraFixo.RowCount := IIfInt(i + 1 > 2, i + 1, 2);
  end;

  pProduto.Free;
  pcDados.ActivePage := tsPrincipal;
end;

procedure TFormCadastroProdutos.sbCopiarProdutoClick(Sender: TObject);
var
  vProduto: RecProdutos;
begin
  vProduto := RecProdutos(PesquisaProdutos.PesquisarProduto(False, False));
  if vProduto = nil then
    Exit;

  Modo(True);
  PreencherRegistro(vProduto);
  ControlarTipoControleEstoque(True, cbTipoControleEstoque.GetValor);

  _Biblioteca.LimparCampos([eCodigoBarras, eCodigoOriginalFabricante, FrCodigoOriginalFornecedores, ckSepararSomenteCodigoBarras]);

  eID.Clear;
end;

procedure TFormCadastroProdutos.sgCustoCompraFixoDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[cfCusto] then
    vAlinhamento := taRightJustify
  else if ACol in[cfZerarCustoAnalitico] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgCustoCompraFixo.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormCadastroProdutos.sgCustoCompraFixoGetCellColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if (ACol <> cfZerarCustoAnalitico) or (ARow = 0) then
    Exit;

  AFont.Style := [fsBold];

  if sgCustoCompraFixo.Cells[cfZerarCustoSintetico, ARow] = 'S' then
    AFont.Color := clBlue
  else
    AFont.Color := clRed;
end;

procedure TFormCadastroProdutos.sgCustoCompraFixoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_SPACE then begin
    if sgCustoCompraFixo.Cells[cfEmpresaId, sgCustoCompraFixo.Row] = '' then
      Exit;

    sgCustoCompraFixo.Cells[cfZerarCustoSintetico, sgCustoCompraFixo.Row] := IIfStr(sgCustoCompraFixo.Cells[cfZerarCustoSintetico, sgCustoCompraFixo.Row] = 'N', 'S', 'N');
    if sgCustoCompraFixo.Cells[cfZerarCustoSintetico, sgCustoCompraFixo.Row] = 'S' then
      sgCustoCompraFixo.Cells[cfZerarCustoAnalitico, sgCustoCompraFixo.Row] := 'Sim'
    else
      sgCustoCompraFixo.Cells[cfZerarCustoAnalitico, sgCustoCompraFixo.Row] := 'N�o';
  end;
end;

procedure TFormCadastroProdutos.sgCustoCompraFixoSelectCell(Sender: TObject;
  ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  if ACol = cfCusto then
    sgCustoCompraFixo.Options := sgCustoCompraFixo.Options + [goEditing]
  else
    sgCustoCompraFixo.Options := sgCustoCompraFixo.Options - [goEditing];
end;

procedure TFormCadastroProdutos.sgProdutosKitArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
begin
  inherited;
  TextCell := _Biblioteca.NFormat(_Biblioteca.SFormatDouble(TextCell), 4);
end;

procedure TFormCadastroProdutos.sgProdutosKitDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coKitProdutoId, coQuantidadeKit, coParticipacaoKit] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgProdutosKit.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormCadastroProdutos.sgProdutosKitGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coParticipacaoKit then
    AFont.Color := _Biblioteca.corEdicaoGrid;
end;

procedure TFormCadastroProdutos.sgProdutosKitKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_DELETE then
    sgProdutosKit.DeleteRow(sgProdutosKit.Row);
end;

procedure TFormCadastroProdutos.sgProdutosKitSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  CanSelect := ACol = coParticipacaoKit;
end;

procedure TFormCadastroProdutos.VerificarRegistro(Sender: TObject);
var
  i: Integer;
  vPercAcumulado: Currency;
begin
  inherited;

  if eNomeProduto.Trim = '' then begin
    _Biblioteca.Exclamar('� necess�rio definir um nome para o produto!');
    SetarFoco(eNomeProduto);
    Abort;
  end;

  if FrFabricante.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio definir um fornecedor para o produto!');
    SetarFoco(FrFabricante);
    Abort;
  end;

  if FrMarca.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio definir uma marca para o produto!');
    SetarFoco(FrMarca);
    Abort;
  end;

  if FrUnidadeVenda.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio definir a unidade de venda do produto!');
    SetarFoco(FrUnidadeVenda);
    Abort;
  end;

  if not FrUnidadeEntrega.EstaVazio then begin
    if FrUnidadeVenda.getUnidade.unidade_id = FrUnidadeEntrega.getUnidade.unidade_id then begin
      _Biblioteca.Exclamar('A unidade de entrega n�o pode ser a mesma unidade de venda!');
      SetarFoco(FrUnidadeEntrega);
      Abort;
    end;
  end;

  if FrLinhaProduto.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio definir linha do produto!');
    SetarFoco(FrLinhaProduto);
    Abort;
  end;

  eNomeCompra.Text := Trim(eNomeCompra.Text);
  if eNomeCompra.Text = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar o nome de compra do produto!');
    SetarFoco(eNomeCompra);
    Abort;
  end;

  if ckSepararSomenteCodigoBarras.Checked and (eCodigoBarras.Trim = '') then begin
    _Biblioteca.Exclamar('Quando a op��o "11 - Separar somente com c�digo de barras" o produto tem que ter c�digo de barras informado!');
    SetarFoco(ckSepararSomenteCodigoBarras);
    Abort;
  end;

  if
    FrFoto1.EstaVazio and
    ( (not FrFoto2.EstaVazio) or (not FrFoto3.EstaVazio) )
  then begin
    _Biblioteca.Exclamar('As fotos do produto devem ser colocadas em ordem!');
    SetarFoco(FrFoto1);
    Abort;
  end;

  if FrFoto2.EstaVazio and (not FrFoto3.EstaVazio) then begin
    _Biblioteca.Exclamar('As fotos do produto devem ser colocadas em ordem!');
    SetarFoco(FrFoto2);
    Abort;
  end;

  FrProdutosGruposTributacoes.VerificarRegistro;

  if eCodigoBalanca.Text <> '' then begin
    if (Length(eCodigoBalanca.Text) < 5) then begin
      _Biblioteca.Exclamar('O c�digo da balan�a tem que ter 5 dig�tos!');
      SetarFoco(eCodigoBalanca);
      Abort;
    end;

    if Copy(eCodigoBalanca.Text, 1, 1) <> '2' then begin
      _Biblioteca.Exclamar('O primeiro d�gito do c�digo da balan�a deve ser 2!');
      SetarFoco(eCodigoBalanca);
      Abort;
    end;

    if ckProdutoDiversoPDV.Checked then begin
      _Biblioteca.Exclamar('Quando utilizar c�digo de balan�a n�o � poss�vel !');
      SetarFoco(eCodigoBalanca);
      Abort;
    end;
  end;

  if Em(cbTipoControleEstoque.GetValor, ['A', 'K']) then begin
    if sgProdutosKit.Cells[coKitProdutoId, 1] = '' then begin
      _Biblioteca.Exclamar('O produto teve o controle de estoque definido como kit mas n�o foi adicionado nenhum produto para o compor, por favor adicione um produto!');
      SetarFoco(FrProdutoKit);
      Abort;
    end;

    vPercAcumulado := 0;

    for i := 1 to sgProdutosKit.RowCount -1 do begin
      if SFormatCurr(sgProdutosKit.Cells[coParticipacaoKit, i]) = 0 then begin
        _Biblioteca.Exclamar('O percentual de participa��o do produto no kit n�o foi bem definido, verifique!');
        SetarFoco(sgProdutosKit);
        sgProdutosKit.Row := i;
        Abort;
      end;

      vPercAcumulado := vPercAcumulado + SFormatCurr(sgProdutosKit.Cells[coParticipacaoKit, i]);
    end;

    if vPercAcumulado <> 100 then begin
      _Biblioteca.Exclamar('O percentual de participa��o dos produtos do kit devem totalizar 100%!');
      SetarFoco(sgProdutosKit);
      sgProdutosKit.Row := 1;
      Abort;
    end;
  end;

  if Em(cbTipoControleEstoque.GetValor, ['I']) then begin
    if sgProdutosKit.Cells[coKitProdutoId, 1] = '' then begin
      _Biblioteca.Exclamar('O produto teve o controle de estoque definido como industrializa��o mas n�o foi adicionado nenhum produto para o compor, por favor adicione um produto!');
      SetarFoco(FrProdutoKit);
      Abort;
    end;
  end;

  if (not ckExigirDataVencimentoLote.Checked) and (eDiasAvisoVencimento.AsInt > 0) then begin
    _Biblioteca.Exclamar('N�o � permitido inserir dias de aviso de vencimento se o produto n�o exigir data de vencimento do lote!');
    SetarFoco(eDiasAvisoVencimento);
    Abort;
  end;

//  if _Produtos.ExisteProdutoNomeMarcaCadastrado(Sessao.getConexaoBanco, eID.AsInt, eNomeProduto.Text, FrMarca.GetMarca.marca_id) then begin
//    _Biblioteca.Exclamar('J� existe um produto com este nome e para esta marca!');
//    SetarFoco(eNomeProduto);
//    Abort;
//  end;

  if (eMotivoInatividade.Trim = '') and ((not ckAtivo.Checked) or (ckInativarProdutoZerarEstoque.Checked)) then begin
    _Biblioteca.Exclamar('� necess�rio definir um motivo para inativar o produto!');
    SetarFoco(eMotivoInatividade);
    Abort;
  end;

  if eNomeProduto.Text <> eNomeCompra.Text then
  begin
    if not _Biblioteca.Perguntar('O nome de compra do produto est� diferente do nome de venda. Deseja gravar mesmo assim?','Nome de compra') then
    begin
      SetarFoco(eNomeCompra);
      Abort;
    end;
  end;

  if (ckAtivo.Checked) and (ckInativarProdutoZerarEstoque.Checked) then
    eMotivoInatividade.Text := '';
end;

end.
