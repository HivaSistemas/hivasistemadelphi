unit FrameTiposCobranca;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _RecordsCadastros,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _TiposCobranca, _Sessao, System.Math, PesquisaTiposCobranca,
  Vcl.Buttons, Vcl.Menus;

type
  TFrTiposCobranca = class(TFrameHenrancaPesquisas)
  private
    FCondicaoId: Integer;
    FFormaPagamento: string;
    FNatureza: string;
  public
    constructor Create(AOwner: TComponent); override;

    procedure setCondicaoId(const pValor: Integer);
    procedure setFormaPagamento(const pValor: string);
    procedure setNatureza(const pValor: string);
    function GetTipoCobranca(pLinha: Integer = -1): RecTiposCobranca;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarDiretoTodos: TArray<TObject>; override;

    function AdicionarPesquisando: TObject; override;
    function AdicionarPesquisandoTodos: TArray<TObject>; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrTiposCobranca }

function TFrTiposCobranca.AdicionarDireto: TObject;
var
  vTiposCobranca: TArray<RecTiposCobranca>;
begin
  if FFormaPagamento = '' then
    vTiposCobranca := _TiposCobranca.BuscarTiposCobrancas(Sessao.getConexaoBanco, 0, [FChaveDigitada], FNatureza )
  else
    vTiposCobranca := _TiposCobranca.BuscarTiposCobrancas(Sessao.getConexaoBanco, 2, [FChaveDigitada, FFormaPagamento], FNatureza );

  if vTiposCobranca = nil then
    Result := nil
  else
    Result := vTiposCobranca[0];
end;

function TFrTiposCobranca.AdicionarDiretoTodos: TArray<TObject>;
begin
  Result := TArray<TObject>(_TiposCobranca.BuscarTiposCobrancasComando(Sessao.getConexaoBanco, 'where 1 = 1 ' + FChaveDigitada));
end;

function TFrTiposCobranca.AdicionarPesquisando: TObject;
begin
  Result := PesquisaTiposCobranca.PesquisarTiposCobranca(FCondicaoId, FFormaPagamento, FNatureza);
end;

function TFrTiposCobranca.AdicionarPesquisandoTodos: TArray<TObject>;
begin
  Result := PesquisaTiposCobranca.PesquisarVarios(FNatureza);
end;

constructor TFrTiposCobranca.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FCondicaoId := 0;
  FFormaPagamento := '';
  FColunaPesquisa := 'TPC.COBRANCA_ID';
end;

function TFrTiposCobranca.GetTipoCobranca(pLinha: Integer): RecTiposCobranca;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecTiposCobranca(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrTiposCobranca.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecTiposCobranca(FDados[i]).CobrancaId = RecTiposCobranca(pSender).CobrancaId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrTiposCobranca.MontarGrid;
var
  i: Integer;
  x: RecTiposCobranca;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      x := RecTiposCobranca(FDados[i]);
      AAdd([IntToStr(x.CobrancaId), x.Nome]);
    end;
  end;
end;

procedure TFrTiposCobranca.setCondicaoId(const pValor: Integer);
begin
  FCondicaoId := pValor;
end;

procedure TFrTiposCobranca.setFormaPagamento(const pValor: string);
begin
  FFormaPagamento := pValor;
end;

procedure TFrTiposCobranca.setNatureza(const pValor: string);
begin
  FNatureza := pValor;
end;

end.
