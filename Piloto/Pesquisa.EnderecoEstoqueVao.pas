unit Pesquisa.EnderecoEstoqueVao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _RecordsCadastros,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, System.StrUtils, System.Math,
  _Biblioteca, _Sessao, Vcl.ExtCtrls;

type
  TFormPesquisaEnderecoEstoqueVao = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  protected
    procedure BuscarRegistros; override;
  end;

function Pesquisar(): TObject;

implementation

{$R *.dfm}

uses _EnderecoEstoqueVao;

const
  coCodigo    = 1;
  coDescricao = 2;
  coAtivo     = 3;

function Pesquisar(): TObject;
var
  obj: TObject;
begin
  obj := _HerancaPesquisas.Pesquisar(TFormPesquisaEnderecoEstoqueVao, _EnderecoEstoqueVao.GetFiltros);
  if obj = nil then
    Result := nil
  else
    Result := RecEnderecoEstoqueVao(obj);
end;

{ TFormPesquisaEnderecoEstoqueVao }

procedure TFormPesquisaEnderecoEstoqueVao.BuscarRegistros;
var
  i: Integer;
  vRua: TArray<RecEnderecoEstoqueVao>;
begin
  inherited;

  vRua :=
    _EnderecoEstoqueVao.BuscarEnderecoEstoqueVao(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text]
    );

  if vRua = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vRua);

  for i := Low(vRua) to High(vRua) do begin
    sgPesquisa.Cells[coSelecionado, i + 1] := 'N�o';
    sgPesquisa.Cells[coCodigo, i + 1]   := IntToStr(vRua[i].vao_id);
    sgPesquisa.Cells[coDescricao, i + 1] := vRua[i].descricao;
  end;

  sgPesquisa.RowCount := IfThen(Length(vRua) = 1, 2, High(vRua) + 2);
  sgPesquisa.SetFocus;
end;

procedure TFormPesquisaEnderecoEstoqueVao.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = coCodigo then
    vAlinhamento := taRightJustify
  else if ACol = coDescricao then
    vAlinhamento := taLeftJustify
  else
    vAlinhamento := taCenter;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormPesquisaEnderecoEstoqueVao.sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;

  if ARow = 0 then
    Exit;
end;

end.
