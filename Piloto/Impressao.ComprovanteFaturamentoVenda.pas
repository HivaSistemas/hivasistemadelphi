unit Impressao.ComprovanteFaturamentoVenda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatorioSimples, QRCtrls, _Sessao, _Biblioteca,
  QuickRpt, Vcl.ExtCtrls, _Orcamentos, _OrcamentosItens, _RecordsOrcamentosVendas,
  QRExport, QRPDFFilt, QRWebFilt;

type
  TFormComprovanteFaturamentoVenda = class(TFormHerancaRelatorioSimples)
    qr2: TQRLabel;
    qrOrcamentoId: TQRLabel;
    qr8: TQRLabel;
    qrCliente: TQRLabel;
    qrClienteAssinatura: TQRLabel;
    qrCpfCnpjUsuarioImpressao: TQRLabel;
    qr12: TQRLabel;
    qrEnderecoCliente: TQRLabel;
    qr15: TQRLabel;
    qrBairroCliente: TQRLabel;
    qrCidadeCliente: TQRLabel;
    qr18: TQRLabel;
    qrCepCliente: TQRLabel;
    qr21: TQRLabel;
    qr23: TQRLabel;
    qr24: TQRLabel;
    qr25: TQRLabel;
    qr26: TQRLabel;
    qr27: TQRLabel;
    qr6: TQRLabel;
    qr28: TQRLabel;
    qrProdutoId: TQRLabel;
    qrNomeProduto: TQRLabel;
    qrMarca: TQRLabel;
    qrValorUnitario: TQRLabel;
    qrQuantidade: TQRLabel;
    qrUnidade: TQRLabel;
    qrValorTotal: TQRLabel;
    qr4: TQRLabel;
    qrTelefoneCliente: TQRLabel;
    qr14: TQRLabel;
    qrCelularCliente: TQRLabel;
    qr9: TQRLabel;
    qrVendedor: TQRLabel;
    qr17: TQRLabel;
    qrCondicaoPagamento: TQRLabel;
    qr20: TQRLabel;
    qr22: TQRLabel;
    qr29: TQRLabel;
    qrTotalSerPago: TQRLabel;
    qrValorDesconto: TQRLabel;
    qrValorOutrasDespesas: TQRLabel;
    qrl1: TQRLabel;
    qrlGrValorFrete: TQRLabel;
    qrTotalProdutos: TQRLabel;
    qr16: TQRLabel;
    QRLabel4: TQRLabel;
    qrOrcamentoIdTM: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel8: TQRLabel;
    qrClienteTM: TQRLabel;
    QRLabel12: TQRLabel;
    qrCelularClienteTM: TQRLabel;
    QRLabel5: TQRLabel;
    qrEnderecoClienteTM: TQRLabel;
    QRLabel9: TQRLabel;
    qrCidadeClienteTM: TQRLabel;
    QRLabel13: TQRLabel;
    qrEstadoClienteTM: TQRLabel;
    QRLabel7: TQRLabel;
    qrBairroClienteTM: TQRLabel;
    QRLabel11: TQRLabel;
    qrCepClienteTM: TQRLabel;
    QRBand1: TQRBand;
    qrProdutoIdTM: TQRLabel;
    qrVendedorTM: TQRLabel;
    QRShape4: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    qrNomeProdutoTM: TQRLabel;
    qrValorUnitarioTM: TQRLabel;
    qrQuantidadeTM: TQRLabel;
    qrUnidadeTM: TQRLabel;
    qrValorTotalTM: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    qrTotalProdutosTM: TQRLabel;
    qrValorOutrasDespesastm: TQRLabel;
    qrlGrValorFreteTM: TQRLabel;
    qrValorDescontoTM: TQRLabel;
    qrTotalSerPagoTM: TQRLabel;
    qrMensagensFinaisTM: TQRMemo;
    QRPDFFilter1: TQRPDFFilter;
    QRShape3: TQRShape;
    QRLabel21: TQRLabel;
    qrApelidoTM: TQRLabel;
    QRLabel22: TQRLabel;
    qrApelido: TQRLabel;
    QRLabel23: TQRLabel;
    qrValorAdiantado: TQRLabel;
    QRLabel24: TQRLabel;
    qrValorAdiantadoTM: TQRLabel;
    procedure qrBandaDetalhesBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qrRelatorioBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure qrRelatorioNeedData(Sender: TObject; var MoreData: Boolean);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qrRelatorioNFBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure qrRelatorioNFNeedData(Sender: TObject; var MoreData: Boolean);

  private
    FPosicImp: Integer;
    FProdutos: TArray<RecOrcamentosItensImpressao>;
  end;

procedure Imprimir(
  const pOrcamentoId: Integer;
  pGerarPDF: Boolean = False;
  pEnviarEmail: Boolean = False
);

procedure EnviarPorEmail(const pOrcamentoId: Integer);

implementation

{$R *.dfm}

uses _Email;

procedure Imprimir(
  const pOrcamentoId: Integer;
  pGerarPDF: Boolean = False;
  pEnviarEmail: Boolean = False
);
var
  vForm: TFormComprovanteFaturamentoVenda;
  vOrcamento: RecOrcamentosImpressao;
  vItens: TArray<RecOrcamentosItensImpressao>;
  vImpressora: RecImpressora;
  pCaminhoPDF: String;
begin
  if pOrcamentoId = 0 then
    Exit;

  vOrcamento := _Orcamentos.BuscarInformacoesImpressao(Sessao.getConexaoBanco, pOrcamentoId);
  if vOrcamento.OrcamentoId = 0 then begin
    NenhumRegistro;
    Exit;
  end;

  vItens := _OrcamentosItens.BuscarInformacoesImpressao(Sessao.getConexaoBanco, pOrcamentoId);
  if vItens = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vImpressora := Sessao.getImpressora( toOrcamento );
  if not vImpressora.validaImpresssora then
    Exit;

  vForm := TFormComprovanteFaturamentoVenda.Create(Application, vImpressora);


  vForm.FProdutos := vItens;
  vForm.PreencherCabecalho( vOrcamento.EmpresaId, True );

  vForm.qr13.Caption := 'Comprovante de Faturamento de Venda';

  vForm.qrOrcamentoId.Caption         := NFormat(pOrcamentoId);
  vForm.qrOrcamentoIdTM.Caption       := NFormat(pOrcamentoId);
  vForm.qrVendedor.Caption            := NFormat(vOrcamento.VendedorId) + ' - ' + vOrcamento.NomeVendedor;
  vForm.qrVendedorTM.Caption          := NFormat(vOrcamento.VendedorId) + ' - ' + vOrcamento.NomeVendedor;
  vForm.qrCondicaoPagamento.Caption   := vOrcamento.NomeCondicaoPagamento;
  vForm.qrCliente.Caption             := NFormat(vOrcamento.ClienteId) + ' - ' + vOrcamento.NomeCliente;
  vForm.qrApelido.Caption             := vOrcamento.Apelido;
  vForm.qrClienteTM.Caption           := NFormat(vOrcamento.ClienteId) + ' - ' + vOrcamento.NomeCliente;
  vForm.qrApelidoTM.Caption           := vOrcamento.Apelido;
  vForm.qrEnderecoCliente.Caption     := vOrcamento.Logradouro + ' ' + vOrcamento.Complemento + ' ' + vOrcamento.Numero;
  vForm.qrEnderecoClienteTM.Caption   := vOrcamento.Logradouro + ' ' + vOrcamento.Complemento + ' ' + vOrcamento.Numero;
  vForm.qrBairroCliente.Caption       := vOrcamento.NomeBairro;
  vForm.qrBairroClienteTM.Caption     := vOrcamento.NomeBairro;
  vForm.qrCidadeCliente.Caption       := vOrcamento.NomeCidade + ' - ' + vOrcamento.NomeEstado;
  vForm.qrCidadeClienteTM.Caption     := vOrcamento.NomeCidade;
  //vForm.qrEstadoCliente.Caption       := vOrcamento.NomeEstado;
  vForm.qrEstadoClienteTM.Caption     := vOrcamento.NomeEstado;
  vForm.qrCepCliente.Caption          := vOrcamento.Cep;
  vForm.qrCepClienteTM.Caption        := vOrcamento.Cep;
  vForm.qrTelefoneCliente.Caption     := vOrcamento.TelefonePrincipal;
  vForm.qrCelularCliente.Caption      := vOrcamento.TelefoneCelular;
  vForm.qrCelularClienteTM.Caption    := vOrcamento.TelefoneCelular;

  vForm.qrTotalProdutos.Caption         := NFormat(vOrcamento.ValorTotalProdutos);
  vForm.qrTotalProdutosTM.Caption       := NFormat(vOrcamento.ValorTotalProdutos);
  vForm.qrValorOutrasDespesas.Caption   := NFormat(vOrcamento.ValorOutrasDespesas);
  vForm.qrValorOutrasDespesasTM.Caption := NFormat(vOrcamento.ValorOutrasDespesas);
  vForm.qrlGrValorFrete.Caption         := NFormat(vOrcamento.ValorFrete + vOrcamento.ValorFreteItens);
  vForm.qrlGrValorFreteTM.Caption       := NFormat(vOrcamento.ValorFrete + vOrcamento.ValorFreteItens);
  vForm.qrValorDesconto.Caption         := NFormat(vOrcamento.ValorDesconto);
  vForm.qrValorDescontoTM.Caption       := NFormat(vOrcamento.ValorDesconto);
  vForm.qrValorAdiantado.Caption        := NFormat(vOrcamento.ValorAdiantamento);
  vForm.qrValorAdiantadoTM.Caption      := NFormat(vOrcamento.ValorAdiantamento);
  vForm.qrTotalSerPago.Caption          := NFormat(vOrcamento.ValorTotal - vOrcamento.ValorAdiantamento);
  vForm.qrTotalSerPagoTM.Caption        := NFormat(vOrcamento.ValorTotal - vOrcamento.ValorAdiantamento);

  VForm.qrClienteAssinatura.Caption     := vOrcamento.NomeCliente;
  VForm.qrCpfCnpjUsuarioImpressao.Caption := vOrcamento.CpfCnpj;


  vForm.Imprimir(1, pGerarPDF);

  vForm.Free;
end;

procedure EnviarPorEmail(const pOrcamentoId: Integer);
begin
  Imprimir(pOrcamentoId,False,True);
end;

procedure TFormComprovanteFaturamentoVenda.QRBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrProdutoIdTM.Caption     := NFormat(FProdutos[FPosicImp].ProdutoId);
  qrNomeProdutoTM.Caption   := FProdutos[FPosicImp].NomeProduto;
  qrValorUnitarioTM.Caption := NFormat(FProdutos[FPosicImp].PrecoUnitario);
  qrQuantidadeTM.Caption    := NFormatEstoque(FProdutos[FPosicImp].Quantidade);
  qrUnidadeTM.Caption       := FProdutos[FPosicImp].UnidadeVenda;
  qrValorTotalTM.Caption    := NFormat(FProdutos[FPosicImp].ValorTotal);
end;

procedure TFormComprovanteFaturamentoVenda.qrBandaDetalhesBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrProdutoId.Caption     := NFormat(FProdutos[FPosicImp].ProdutoId);
  qrNomeProduto.Caption   := FProdutos[FPosicImp].NomeProduto;
  qrMarca.Caption         := FProdutos[FPosicImp].NomeMarca;
  qrValorUnitario.Caption := NFormat(FProdutos[FPosicImp].PrecoUnitario);
  qrQuantidade.Caption    := NFormatEstoque(FProdutos[FPosicImp].Quantidade);
  qrUnidade.Caption       := FProdutos[FPosicImp].UnidadeVenda;
  qrValorTotal.Caption    := NFormat(FProdutos[FPosicImp].ValorTotal);
end;


procedure TFormComprovanteFaturamentoVenda.qrRelatorioBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicImp := -1;
end;

procedure TFormComprovanteFaturamentoVenda.qrRelatorioNeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicImp);
  MoreData := (Length(FProdutos) > FPosicImp);
end;

procedure TFormComprovanteFaturamentoVenda.qrRelatorioNFBeforePrint(
  Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicImp := -1;
end;

procedure TFormComprovanteFaturamentoVenda.qrRelatorioNFNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicImp);
  MoreData := (Length(FProdutos) > FPosicImp);
end;

end.
