inherited FormInformacoesOperacoesCaixa: TFormInformacoesOperacoesCaixa
  Caption = 'Informa'#231#245'es de opera'#231#245'es do banco/caixa'
  ClientHeight = 352
  ClientWidth = 596
  ExplicitWidth = 602
  ExplicitHeight = 381
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 5
    Top = 6
    Width = 61
    Height = 14
    Caption = 'Movimento'
  end
  object lb19: TLabel [1]
    Left = 319
    Top = 6
    Width = 155
    Height = 14
    Caption = 'Usu'#225'rio realizou movimento'
  end
  object lb6: TLabel [2]
    Left = 488
    Top = 6
    Width = 90
    Height = 14
    Caption = 'Data movimento'
  end
  inherited pnOpcoes: TPanel
    Top = 315
    Width = 596
    ExplicitTop = 315
    ExplicitWidth = 596
  end
  object eMovimentoId: TEditLuka
    Left = 5
    Top = 22
    Width = 76
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 1
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object st3: TStaticText
    Left = 86
    Top = 13
    Width = 228
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Tipo de movimento'
    Color = 38619
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    Transparent = False
  end
  object stTipoMovimento: TStaticText
    Left = 86
    Top = 28
    Width = 228
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Sangria'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 3
    Transparent = False
  end
  object eUsuarioMovimento: TEditLuka
    Left = 319
    Top = 22
    Width = 164
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 4
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eDataCadastro: TEditLukaData
    Left = 488
    Top = 22
    Width = 104
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1; '
    MaxLength = 10
    ReadOnly = True
    TabOrder = 5
    Text = '  /  /    '
  end
  object pcDados: TPageControl
    Left = 0
    Top = 47
    Width = 596
    Height = 268
    ActivePage = tsGerais
    Align = alBottom
    TabOrder = 6
    object tsGerais: TTabSheet
      Caption = 'Informa'#231#245'es gerais'
      object lb11: TLabel
        Left = 405
        Top = 29
        Width = 60
        Height = 14
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Dinheiro'
      end
      object lb12: TLabel
        Left = 405
        Top = 52
        Width = 60
        Height = 14
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Cheque'
      end
      object lb13: TLabel
        Left = 405
        Top = 75
        Width = 60
        Height = 14
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Cart'#227'o'
      end
      object lb14: TLabel
        Left = 405
        Top = 98
        Width = 60
        Height = 14
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Cobran'#231'a'
      end
      object lb2: TLabel
        Left = 2
        Top = 0
        Width = 142
        Height = 14
        Caption = 'Conta origem/destino din.'
      end
      object lb4: TLabel
        Left = 171
        Top = 0
        Width = 69
        Height = 14
        Caption = 'Observa'#231#245'es'
      end
      object lb17: TLabel
        Left = 2
        Top = 45
        Width = 30
        Height = 14
        Caption = 'Turno'
      end
      object lb5: TLabel
        Left = 405
        Top = 121
        Width = 60
        Height = 14
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Cr'#233'dito'
      end
      object sbAbrirInformacoesTurno: TSpeedButton
        Left = 57
        Top = 64
        Width = 18
        Height = 18
        Hint = 'Abrir informa'#231#245'es do turno'
        Caption = '...'
        OnClick = sbAbrirInformacoesTurnoClick
      end
      object Label1: TLabel
        Left = 80
        Top = 45
        Width = 65
        Height = 14
        Caption = 'Mov. origem'
      end
      object sbInformacoesMovimentoOrigem: TSpeedButton
        Left = 135
        Top = 64
        Width = 18
        Height = 18
        Caption = '...'
        OnClick = sbInformacoesMovimentoOrigemClick
      end
      object eValorDinheiro: TEditLuka
        Left = 467
        Top = 22
        Width = 97
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 0
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorCheque: TEditLuka
        Left = 467
        Top = 45
        Width = 97
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 1
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorCartao: TEditLuka
        Left = 467
        Top = 68
        Width = 97
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 2
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorCobranca: TEditLuka
        Left = 467
        Top = 91
        Width = 97
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 3
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object stSPC: TStaticText
        Left = 402
        Top = 0
        Width = 182
        Height = 15
        Alignment = taCenter
        AutoSize = False
        Caption = 'Valores do movimento'
        Color = 15790320
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 4
        Transparent = False
      end
      object eContaOrigemDestino: TEditLuka
        Left = 2
        Top = 15
        Width = 164
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 5
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eObservacoes: TMemo
        Left = 171
        Top = 15
        Width = 223
        Height = 66
        TabOrder = 6
      end
      object eTurnoId: TEditLuka
        Left = 2
        Top = 60
        Width = 54
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 7
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eValorCredito: TEditLuka
        Left = 467
        Top = 114
        Width = 97
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 8
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eMovimentoOrigemId: TEditLuka
        Left = 80
        Top = 60
        Width = 54
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 9
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
    end
    object tsItensOperacao: TTabSheet
      Caption = 'T'#237'tulos do movimento'
      ImageIndex = 1
      object sgTitulosMovimento: TGridLuka
        Left = 0
        Top = 0
        Width = 588
        Height = 239
        Align = alClient
        DefaultRowHeight = 19
        DrawingStyle = gdsClassic
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        TabOrder = 0
        OnDblClick = sgTitulosMovimentoDblClick
        OnDrawCell = sgTitulosMovimentoDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Codigo'
          'Tipo de cobran'#231'a'
          'Documento'
          'Or'#231'amento'
          'Valor')
        Grid3D = False
        RealColCount = 10
        AtivarPopUpSelecao = False
        ColWidths = (
          54
          224
          93
          73
          101)
      end
    end
  end
end
