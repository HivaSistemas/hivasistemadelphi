inherited FormPesquisaCentroCustos: TFormPesquisaCentroCustos
  Caption = 'Pesquisa de centro de custos'
  ClientHeight = 205
  ClientWidth = 407
  ExplicitWidth = 413
  ExplicitHeight = 234
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 407
    Height = 159
    ColCount = 4
    OnDrawCell = sgPesquisaDrawCell
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Nome'
      'Ativo?')
    RealColCount = 4
    ExplicitLeft = 0
    ExplicitTop = 46
    ExplicitWidth = 407
    ExplicitHeight = 159
    ColWidths = (
      28
      55
      199
      41)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Text = '0'
  end
  inherited pnFiltro1: TPanel
    Width = 407
    ExplicitWidth = 407
    inherited eValorPesquisa: TEditLuka
      Width = 200
      ExplicitWidth = 200
    end
  end
end
