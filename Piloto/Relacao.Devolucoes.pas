unit Relacao.Devolucoes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _Sessao, _Biblioteca,
  Vcl.Grids, GridLuka, Vcl.StdCtrls, Vcl.ExtCtrls, FrameCondicoesPagamento, System.StrUtils,
  GroupBoxLuka, FrameDataInicialFinal, FrameEmpresas, _FrameHerancaPrincipal, System.Math,
  _FrameHenrancaPesquisas, FrameClientes, Vcl.ComCtrls, Vcl.Buttons, _RecordsRelatorios,
  _RelacaoDevolucoes, _DevolucoesItens, _RecordsOrcamentosVendas, Informacoes.Devolucao,
  StaticTextLuka, Vcl.Menus, _Devolucoes, _RecordsEspeciais, ImpressaoComprovanteDevolucaoGrafico,
  Frame.Inteiros, FrameProdutos, CheckBoxLuka, FrameVendedores;

type
  TFormRelacaoDevolucoesVendas = class(TFormHerancaRelatoriosPageControl)
    FrClientes: TFrClientes;
    FrEmpresas: TFrEmpresas;
    FrDataDevolucao: TFrDataInicialFinal;
    spSeparador: TSplitter;
    sgDevolucoes: TGridLuka;
    sgProdutos: TGridLuka;
    StaticTextLuka1: TStaticTextLuka;
    StaticTextLuka2: TStaticTextLuka;
    pmOpcoes: TPopupMenu;
    miCancelarDevolucao: TMenuItem;
    miReimprimirComprovante: TMenuItem;
    FrCodigoDevolucao: TFrameInteiros;
    FrProdutos: TFrProdutos;
    FrCodigoOrcamento: TFrameInteiros;
    FrVendedores: TFrVendedores;
    pn1: TPanel;
    st5: TStaticText;
    st1: TStaticText;
    st4: TStaticText;
    st8: TStaticText;
    stValorBruto: TStaticTextLuka;
    stValorOutrasDespesas: TStaticTextLuka;
    stValorDesconto: TStaticTextLuka;
    stValorFrete: TStaticTextLuka;
    st2: TStaticText;
    stValorLiquido: TStaticTextLuka;
    miN1: TMenuItem;
    miGerarArquivoTXT: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure sgDevolucoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgDevolucoesClick(Sender: TObject);
    procedure sgDevolucoesDblClick(Sender: TObject);
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure miCancelarDevolucaoClick(Sender: TObject);
    procedure miReimprimirComprovanteClick(Sender: TObject);
    procedure sgDevolucoesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure miGerarArquivoTXTClick(Sender: TObject);
  private
    FItensDevolucoes: TArray<RecDevolucoesItens>;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

const
  coDevolucaoId       = 0;
  coOrcamentoId       = 1;
  coCliente           = 2;
  coValorBruto        = 3;
  coValorOutDesp      = 4;
  coValorDesconto     = 5;
  coValorFrete        = 6;
  coValorST           = 7;
  coValorIPI          = 8;
  coValorLiquido      = 9;
  coVendedor          = 10;
  coDataHoraDevolucao = 11;
  coUsuarioDevolucao  = 12;
  coEmpresa           = 13;
  coTipoLinha         = 14;


  (* Grid de produto *)
  cpProdutoId     = 0;
  cpNome          = 1;
  cpMarca         = 2;
  cpPrecoUnitario = 3;
  cpDevolvidosEnt = 4;
  cpDevolvidosPen = 5;
  cpDevolvidosSem = 6;
  cpUnidade       = 7;
  cpValorTotal    = 8;

procedure TFormRelacaoDevolucoesVendas.Carregar(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vComando: string;
  vDevolucoesIds: TArray<Integer>;
  vDevolucoes: TArray<_RecordsRelatorios.RecDevolucoes>;

  vTotaisDev: record
    ValorBruto: Currency;
    ValorOutrasDepesas: Currency;
    ValorDesconto: Currency;
    ValorFrete: Currency;
    ValorLiquido: Currency;
  end;

begin
  inherited;
  //Coment�rio para demonstrar altera��o no arquivo
  sgDevolucoes.ClearGrid();
  sgProdutos.ClearGrid();
  vDevolucoesIds := nil;

  vTotaisDev.ValorBruto         := 0;
  vTotaisDev.ValorOutrasDepesas := 0;
  vTotaisDev.ValorDesconto      := 0;
  vTotaisDev.ValorFrete         := 0;
  vTotaisDev.ValorLiquido       := 0;

  stValorBruto.AsCurr          := 0;
  stValorOutrasDespesas.AsCurr := 0;
  stValorDesconto.AsCurr       := 0;
  stValorFrete.AsCurr          := 0;
  stValorLiquido.AsCurr        := 0;

  if not FrEmpresas.EstaVazio then
    vComando := ' where EMPRESA_ID ' + FrEmpresas.getSqlFiltros;

  if not FrClientes.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrClientes.getSqlFiltros('CLIENTE_ID'));

  if not FrDataDevolucao.NenhumaDataValida then
    _Biblioteca.WhereOuAnd(vComando, FrDataDevolucao.getSqlFiltros('DATA_HORA_DEVOLUCAO'));

  if not FrCodigoDevolucao.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrCodigoDevolucao.getSqlFiltros('DEVOLUCAO_ID'));

  if not FrCodigoOrcamento.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrCodigoOrcamento.getSqlFiltros('ORCAMENTO_ID'));

  if not FrProdutos.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, ' DEVOLUCAO_ID in(select distinct DEVOLUCAO_ID from DEVOLUCOES_ITENS where ' + FrProdutos.getSqlFiltros('PRODUTO_ID') + ')');

  if not FrVendedores.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrVendedores.getSqlFiltros('VENDEDOR_ID'));

  vComando := vComando + ' order by DEVOLUCAO_ID ';
  vDevolucoes := _RelacaoDevolucoes.BuscarDevolucoes(Sessao.getConexaoBanco, vComando);
  if vDevolucoes = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vLinha := 0;
  for i := Low(vDevolucoes) to High(vDevolucoes) do begin
    Inc(vLinha);

    sgDevolucoes.Cells[coDevolucaoId, vLinha]       := NFormat(vDevolucoes[i].DevolucaoId);
    sgDevolucoes.Cells[coOrcamentoId, vLinha]       := NFormat(vDevolucoes[i].OrcamentoId);
    sgDevolucoes.Cells[coCliente, vLinha]           := NFormat(vDevolucoes[i].ClienteId) + ' - ' + vDevolucoes[i].NomeCliente;
    sgDevolucoes.Cells[coValorBruto, vLinha]        := NFormat(vDevolucoes[i].ValorBruto);
    sgDevolucoes.Cells[coValorOutDesp, vLinha]      := NFormatN(vDevolucoes[i].ValorOutrasDepesas);
    sgDevolucoes.Cells[coValorDesconto, vLinha]     := NFormatN(vDevolucoes[i].ValorDesconto);
  //  sgDevolucoes.Cells[coValorFrete, vLinha]        := NFormatN(vDevolucoes[i].ValorFrete);
    sgDevolucoes.Cells[coValorLiquido, vLinha]      := NFormat(vDevolucoes[i].ValorLiquido);
  //  sgDevolucoes.Cells[coVendedor, vLinha]          := getInformacao(vDevolucoes[i].VendedorId, vDevolucoes[i].NomeVendedor);
    sgDevolucoes.Cells[coDataHoraDevolucao, vLinha] := DHFormat(vDevolucoes[i].DataHoraDevolucao);
    sgDevolucoes.Cells[coUsuarioDevolucao, vLinha]  := NFormat(vDevolucoes[i].UsuarioCadastroId) + ' - ' + vDevolucoes[i].NomeUsuarioDevolucao;
    sgDevolucoes.Cells[coEmpresa, vLinha]           := NFormat(vDevolucoes[i].EmpresaId) + ' - ' + vDevolucoes[i].NomeEmpresa;
   // sgDevolucoes.Cells[coTipoLinha, vLinha]         := cgLDetalhe;

    vTotaisDev.ValorBruto         := vTotaisDev.ValorBruto + vDevolucoes[i].ValorBruto;
    vTotaisDev.ValorOutrasDepesas := vTotaisDev.ValorOutrasDepesas + vDevolucoes[i].ValorOutrasDepesas;
    vTotaisDev.ValorDesconto      := vTotaisDev.ValorDesconto + vDevolucoes[i].ValorDesconto;
   // vTotaisDev.ValorFrete         := vTotaisDev.ValorFrete + vDevolucoes[i].ValorFrete;
    vTotaisDev.ValorLiquido       := vTotaisDev.ValorLiquido + vDevolucoes[i].ValorLiquido;

    _Biblioteca.AddNoVetorSemRepetir(vDevolucoesIds, vDevolucoes[i].DevolucaoId);
  end;

  Inc(vLinha);
  sgDevolucoes.Cells[coValorBruto, vLinha]    := NFormat(vTotaisDev.ValorBruto);
  sgDevolucoes.Cells[coValorOutDesp, vLinha]  := NFormatN(vTotaisDev.ValorOutrasDepesas);
  sgDevolucoes.Cells[coValorDesconto, vLinha] := NFormatN(vTotaisDev.ValorDesconto);
  sgDevolucoes.Cells[coValorFrete, vLinha]    := NFormatN(vTotaisDev.ValorFrete);
  sgDevolucoes.Cells[coValorLiquido, vLinha]  := NFormat(vTotaisDev.ValorLiquido);
  //sgDevolucoes.Cells[coTipoLinha, vLinha]     := cgLTotal;

  sgDevolucoes.SetLinhasGridPorTamanhoVetor( vLinha );

  stValorBruto.Somar( vTotaisDev.ValorBruto );
  stValorOutrasDespesas.Somar( vTotaisDev.ValorOutrasDepesas );
  stValorDesconto.Somar( vTotaisDev.ValorDesconto );
  stValorFrete.Somar( vTotaisDev.ValorFrete );
  stValorLiquido.Somar( vTotaisDev.ValorLiquido );

  FItensDevolucoes := _DevolucoesItens.BuscarDevolucoesItensComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('ITE.DEVOLUCAO_ID', vDevolucoesIds) + ' order by ITE.DEVOLUCAO_ID, ITE.ITEM_ID');
  pcDados.ActivePage := tsResultado;
  sgDevolucoesClick(sbCarregar);
end;

procedure TFormRelacaoDevolucoesVendas.FormCreate(Sender: TObject);
begin
  inherited;
  ActiveControl := FrEmpresas.sgPesquisa;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId, False);
end;

procedure TFormRelacaoDevolucoesVendas.Imprimir(Sender: TObject);
begin
  inherited;

end;

procedure TFormRelacaoDevolucoesVendas.miCancelarDevolucaoClick(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  vDevolucaoId: Integer;
begin
  inherited;
  vDevolucaoId := SFormatInt( sgDevolucoes.Cells[coDevolucaoId, sgDevolucoes.Row] );
  if vDevolucaoId = 0 then
    Exit;

  vRetBanco := _Devolucoes.CancelarDevolucao( Sessao.getConexaoBanco, vDevolucaoId );
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar( Sender );
end;

procedure TFormRelacaoDevolucoesVendas.miGerarArquivoTXTClick(Sender: TObject);
begin
  inherited;
 // sgDevolucoes.gerarArquivoTxt;
end;

procedure TFormRelacaoDevolucoesVendas.miReimprimirComprovanteClick(Sender: TObject);
begin
  inherited;
 // ImpressaoComprovanteDevolucaoGrafico.Imprimir( SFormatInt( sgDevolucoes.Cells[coDevolucaoId, sgDevolucoes.Row] ), True );
end;

procedure TFormRelacaoDevolucoesVendas.sgDevolucoesClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
begin
  inherited;
  sgProdutos.ClearGrid;
  if sgDevolucoes.Cells[coDevolucaoId, 1] = '' then
    Exit;

  vLinha := 0;
  for i := Low(FItensDevolucoes) to High(FItensDevolucoes) do begin
    if SFormatInt(sgDevolucoes.Cells[coDevolucaoId, sgDevolucoes.Row]) <> FItensDevolucoes[i].DevolucaoId then
      Continue;

    Inc(vLinha);

    sgProdutos.Cells[cpProdutoId, vLinha]     := NFormat(FItensDevolucoes[i].ProdutoId);
    sgProdutos.Cells[cpNome, vLinha]          := FItensDevolucoes[i].NomeProduto;
    sgProdutos.Cells[cpMarca, vLinha]         := NFormat(FItensDevolucoes[i].MarcaId) + ' - ' + FItensDevolucoes[i].NomeMarca;
    sgProdutos.Cells[cpPrecoUnitario, vLinha] := NFormat(FItensDevolucoes[i].PrecoUnitario);
    sgProdutos.Cells[cpDevolvidosEnt, vLinha] := NFormatNEstoque(FItensDevolucoes[i].QuantidadeDevolvidosEntregues);
    sgProdutos.Cells[cpDevolvidosPen, vLinha] := NFormatNEstoque(FItensDevolucoes[i].QuantidadeDevolvidosPendencias);
    sgProdutos.Cells[cpDevolvidosSem, vLinha] := NFormatNEstoque(FItensDevolucoes[i].QuantidadeDevolvSemPrevisao);
    sgProdutos.Cells[cpUnidade, vLinha]       := FItensDevolucoes[i].UnidadeVenda;
    sgProdutos.Cells[cpValorTotal, vLinha]    := NFormat(FItensDevolucoes[i].ValorBruto)
  end;
  sgProdutos.SetLinhasGridPorTamanhoVetor( vLinha );
end;

procedure TFormRelacaoDevolucoesVendas.sgDevolucoesDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Devolucao.Informar(SFormatInt(sgDevolucoes.Cells[coDevolucaoId, sgDevolucoes.Row]));
end;

procedure TFormRelacaoDevolucoesVendas.sgDevolucoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coDevolucaoId, coOrcamentoId, coValorBruto, coValorOutDesp, coValorDesconto, coValorFrete, coValorLiquido] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgDevolucoes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoDevolucoesVendas.sgDevolucoesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

// if sgDevolucoes.Cells[coTipoLinha, ARow] = cgLTotal then begin
    AFont.Color := clWhite;
    AFont.Style := [fsBold];
    ABrush.Color := clActiveCaption;
  end;
//end;

procedure TFormRelacaoDevolucoesVendas.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[cpProdutoId, cpPrecoUnitario, cpDevolvidosEnt, cpDevolvidosPen, cpDevolvidosSem, cpValorTotal] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoDevolucoesVendas.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
