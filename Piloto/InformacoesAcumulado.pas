unit InformacoesAcumulado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Sessao, _Biblioteca, _Acumulados,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Grids, GridLuka, FrameFechamento, _AcumuladosItens, _NotasFiscais,
  _FrameHerancaPrincipal, FrameEndereco, Vcl.Mask, EditLukaData, Vcl.ComCtrls, MixVendas,
  EditLuka, FrameFechamentoJuros, _AcumuladosPagamentos, _AcumuladosPagamentosCheques, Informacoes.TurnoCaixa,
  _AcumuladosOrcamentos, _RecordsNotasFiscais, SpeedButtonLuka, _RecordsFinanceiros;

type
  TFormInformacoesAcumulado = class(TFormHerancaFinalizar)
    lb1: TLabel;
    lb2: TLabel;
    lb13: TLabel;
    eAcumuladoId: TEditLuka;
    eCliente: TEditLuka;
    st3: TStaticText;
    stStatus: TStaticText;
    eEmpresa: TEditLuka;
    pcDados: TPageControl;
    tsGerais: TTabSheet;
    lb3: TLabel;
    lb4: TLabel;
    lb5: TLabel;
    lb6: TLabel;
    lb7: TLabel;
    lb8: TLabel;
    lb9: TLabel;
    lb10: TLabel;
    lb11: TLabel;
    lb15: TLabel;
    lb16: TLabel;
    lb17: TLabel;
    lb18: TLabel;
    lb22: TLabel;
    lb23: TLabel;
    eDataCadastro: TEditLukaData;
    eDataRecebimento: TEditLukaData;
    eTotalAcumulado: TEditLuka;
    eTotalProdutos: TEditLuka;
    eOutrasDespesas: TEditLuka;
    eValorDesconto: TEditLuka;
    eUsuarioCadastro: TEditLuka;
    eUsuarioRecebimento: TEditLuka;
    eTurnoId: TEditLuka;
    stSPC: TStaticText;
    eCondicaoPagamento: TEditLuka;
    eValorFrete: TEditLuka;
    tsItens: TTabSheet;
    sgProdutos: TGridLuka;
    tsObservacoes: TTabSheet;
    lb21: TLabel;
    sbAdicionarObservacaoNFe: TSpeedButton;
    eObservacoesNotaFiscalEletronica: TMemo;
    tsNotasFiscais: TTabSheet;
    sgNotasFiscais: TGridLuka;
    eValorJuros: TEditLuka;
    lb19: TLabel;
    lb20: TLabel;
    FrFechamento: TFrFechamentoJuros;
    tsPedidos: TTabSheet;
    sgPedidos: TGridLuka;
    FrEndereco: TFrEndereco;
    eIndiceDescontoVenda: TEditLuka;
    lb25: TLabel;
    sbInformacoesTurno: TSpeedButtonLuka;
    SpeedButtonLuka1: TSpeedButtonLuka;
    tsFinanceiros: TTabSheet;
    sgReceber: TGridLuka;
    StaticText1: TStaticText;
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure sbInformacoesTurnoClick(Sender: TObject);
    procedure sgPedidosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgPedidosDblClick(Sender: TObject);
    procedure sgNotasFiscaisDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgNotasFiscaisDblClick(Sender: TObject);
    procedure sgReceberDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgReceberGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgReceberDblClick(Sender: TObject);
    procedure tsFinanceirosShow(Sender: TObject);
  private
    FAcumuladoId: Integer;
  public
    { Public declarations }
  end;

procedure Informar(pAcumuladoId: Integer);

implementation

{$R *.dfm}

uses
  Informacoes.Orcamento, Informacoes.NotaFiscal, Informacoes.TituloReceber,
  _ContasReceber;

type
  cgItens = record
  const
    ProdutoId      = 0;
    Nome           = 1;
    Marca          = 2;
    PrecoUnitario  = 3;
    Quantidade     = 4;
    Unidade        = 5;
    ValorDesconto  = 6;
    OutrasDespesas = 7;
    ValorLiquido   = 8;
    ValorFrete     = 9;
  end;

  cgPedidos = record
  const
    PedidoId            = 0;
    DataHoraRecebimento = 1;
    Vendedor            = 2;
    Valor               = 3;
    ValorDevolvidos     = 4;
  end;

  cgNotas = record
  const
    NotaFiscalId    = 0;
    NumeroNota      = 1;
    TipoNota        = 2;
    DataEmissao     = 3;
    TipoMovimento   = 4;
    Valor           = 5;
    Empresa         = 6;
  end;

const
  (* Grid de contas a receber *)
  crbReceberId      = 0;
  crbTipoCobranca   = 1;
  crbStatus         = 2;
  crbDocumento      = 3;
  crbDataVencimento = 4;
  crbValorDocumento = 5;
  crbParcela        = 6;
  crbNumeroParcelas = 7;

procedure Informar(pAcumuladoId: Integer);
var
  i: Integer;

  vAcumulado: TArray<RecAcumulados>;
  vItens: TArray<RecAcumuladosItens>;
  vPedidos: TArray<RecAcumuladosOrcamentos>;
  vNotas: TArray<RecNotaFiscal>;

  vForm: TFormInformacoesAcumulado;
begin
  if pAcumuladoId = 0 then
   Exit;

  vAcumulado := _Acumulados.BuscarAcumulados(Sessao.getConexaoBanco, 0, [pAcumuladoId]);
  if vAcumulado = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  vItens   := _AcumuladosItens.BuscarAcumuladosItens(Sessao.getConexaoBanco, 0, [pAcumuladoId]);
  vPedidos := _AcumuladosOrcamentos.BuscarAcumuladosOrcamentos(Sessao.getConexaoBanco, 0, [pAcumuladoId]);
  vNotas   := _NotasFiscais.BuscarNotasFiscais(Sessao.getConexaoBanco, 7, [pAcumuladoId]);

  vForm := TFormInformacoesAcumulado.Create(nil);

  vForm.FAcumuladoId := pAcumuladoId;
  vForm.eAcumuladoId.SetInformacao(vAcumulado[0].AcumuladoId);
  vForm.eCliente.SetInformacao(vAcumulado[0].ClienteId, vAcumulado[0].NomeCliente);
  vForm.eEmpresa.SetInformacao(vAcumulado[0].EmpresaId, vAcumulado[0].NomeEmpresa);
  vForm.stStatus.Caption := vAcumulado[0].StatusAnalitico;
  vForm.eCondicaoPagamento.SetInformacao(vAcumulado[0].CondicaoId, vAcumulado[0].NomeCondicaoPagamento);
  vForm.eDataCadastro.AsDataHora := vAcumulado[0].DataHoraFechamento;
  vForm.eUsuarioCadastro.SetInformacao( vAcumulado[0].UsuarioFechamentoId, vAcumulado[0].NomeUsuarioFechamento );
  vForm.eDataRecebimento.AsDataHora := vAcumulado[0].DataHoraRecebimento;
  vForm.eUsuarioRecebimento.SetInformacao( vAcumulado[0].UsuarioRecebimentoId, vAcumulado[0].NomeUsuarioRecebimento );
  vForm.eIndiceDescontoVenda.SetInformacao( vAcumulado[0].IndiceDescontoVendaId, vAcumulado[0].NomeIndDescontoVenda );
  vForm.eTurnoId.SetInformacao( vAcumulado[0].TurnoId );
  vForm.eTotalProdutos.AsDouble  := vAcumulado[0].ValorTotalProdutos;
  vForm.eOutrasDespesas.AsDouble := vAcumulado[0].ValorOutrasDespesas;
  vForm.eValorDesconto.AsDouble  := vAcumulado[0].ValorDesconto;
  vForm.eValorFrete.AsDouble     := vAcumulado[0].ValorFrete;
  vForm.eValorJuros.AsDouble     := vAcumulado[0].ValorJuros;
  vForm.eTotalAcumulado.AsDouble := vAcumulado[0].ValorTotal;
  vForm.eObservacoesNotaFiscalEletronica.Lines.Add(vAcumulado[0].ObservacoesNfe);

  vForm.FrFechamento.SomenteLeitura(True);
  vForm.FrFechamento.setCondicaoPagamento(vAcumulado[0].CondicaoId, vAcumulado[0].NomeCondicaoPagamento, 0);
  vForm.FrFechamento.ValorFrete := vAcumulado[0].ValorFrete;
  vForm.FrFechamento.setTotalProdutos(vAcumulado[0].ValorTotalProdutos);
  vForm.FrFechamento.eValorOutrasDespesas.AsDouble := vAcumulado[0].ValorOutrasDespesas;
  vForm.FrFechamento.eValorOutrasDespesasChange(nil);

  vForm.FrFechamento.eValorDesconto.AsDouble := vAcumulado[0].ValorDesconto;
  vForm.FrFechamento.eValorDescontoChange(nil);

  vForm.FrFechamento.CartoesDebito   := _AcumuladosPagamentos.BuscarAcumuladosPagamentos(Sessao.getConexaoBanco, 0, [pAcumuladoId, 'D']);
  vForm.FrFechamento.CartoesCredito  := _AcumuladosPagamentos.BuscarAcumuladosPagamentos(Sessao.getConexaoBanco, 0, [pAcumuladoId, 'C']);
  vForm.FrFechamento.Cobrancas       := _AcumuladosPagamentos.BuscarAcumuladosPagamentos(Sessao.getConexaoBanco, 1, [pAcumuladoId]);
  vForm.FrFechamento.Cheques         := _AcumuladosPagamentosCheques.BuscarAcumuladosPagamentosCheques(Sessao.getConexaoBanco, 0, [pAcumuladoId]);

  vForm.FrFechamento.eValorDinheiro.AsDouble      := vAcumulado[0].ValorDinheiro;
  vForm.FrFechamento.eValorCheque.AsDouble        := vAcumulado[0].ValorCheque;
  vForm.FrFechamento.eValorCartaoDebito.AsDouble  := vAcumulado[0].ValorCartaoDebito;
  vForm.FrFechamento.eValorCartaoCredito.AsDouble := vAcumulado[0].ValorCartaoCredito;
  vForm.FrFechamento.eValorCobranca.AsDouble      := vAcumulado[0].ValorCobranca;
  vForm.FrFechamento.eValorCredito.AsDouble       := vAcumulado[0].ValorCredito;
  vForm.FrFechamento.eValorPix.AsDouble           := vAcumulado[0].ValorPix;

  vForm.FrEndereco.SomenteLeitura(True);
  vForm.FrEndereco.setLogradouro(vAcumulado[0].Logradouro);
  vForm.FrEndereco.setComplemento(vAcumulado[0].Complemento);
  vForm.FrEndereco.setNumero(vAcumulado[0].Numero);
  vForm.FrEndereco.setBairroId(vAcumulado[0].BairroId);
  vForm.FrEndereco.setPontoReferencia(vAcumulado[0].PontoReferencia);
  vForm.FrEndereco.setCep(vAcumulado[0].Cep);

  for i := Low(vItens) to High(vItens) do begin
    vForm.sgProdutos.Cells[cgItens.ProdutoId, i + 1]      := NFormat(vItens[i].ProdutoId);
    vForm.sgProdutos.Cells[cgItens.Nome, i + 1]           := vItens[i].NomeProduto;
    vForm.sgProdutos.Cells[cgItens.Marca, i + 1]          := vItens[i].NomeMarca;
    vForm.sgProdutos.Cells[cgItens.PrecoUnitario, i + 1]  := NFormat(vItens[i].PrecoUnitario);
    vForm.sgProdutos.Cells[cgItens.Quantidade, i + 1]     := NFormat(vItens[i].Quantidade);
    vForm.sgProdutos.Cells[cgItens.Unidade, i + 1]        := vItens[i].UnidadeVenda;
    vForm.sgProdutos.Cells[cgItens.ValorDesconto, i + 1]  := NFormatN(vItens[i].ValorTotalDesconto);
    vForm.sgProdutos.Cells[cgItens.OutrasDespesas, i + 1] := NFormatN(vItens[i].ValorTotalOutrasDespesas);
    vForm.sgProdutos.Cells[cgItens.ValorLiquido, i + 1]   := NFormat(vItens[i].ValorTotal + vItens[i].ValorTotalOutrasDespesas - vItens[i].ValorTotalDesconto);
    vForm.sgProdutos.Cells[cgItens.ValorFrete, i + 1]     := NFormatN(vItens[i].ValorTotalFrete);
  end;
  vForm.sgProdutos.SetLinhasGridPorTamanhoVetor( Length(vItens) );

  for i := Low(vPedidos) to High(vPedidos) do begin
    vForm.sgPedidos.Cells[cgPedidos.PedidoId, i + 1]            := NFormat(vPedidos[i].OrcamentoId);
    vForm.sgPedidos.Cells[cgPedidos.DataHoraRecebimento, i + 1] := DHFormatN(vPedidos[i].DataHoraRecebimento);
    vForm.sgPedidos.Cells[cgPedidos.Vendedor, i + 1]            := getInformacao(vPedidos[i].VendedorId, vPedidos[i].NomeVendedor);
    vForm.sgPedidos.Cells[cgPedidos.Valor, i + 1]               := NFormat(vPedidos[i].ValorTotal);
    vForm.sgPedidos.Cells[cgPedidos.ValorDevolvidos, i + 1]     := NFormatN(vPedidos[i].ValorDevAcumuladoAberto);
  end;
  vForm.sgPedidos.SetLinhasGridPorTamanhoVetor( Length(vPedidos) );

  for i := Low(vNotas) to High(vNotas) do begin
    vForm.sgNotasFiscais.Cells[cgNotas.NotaFiscalId, i + 1]  := NFormat(vNotas[i].nota_fiscal_id);
    vForm.sgNotasFiscais.Cells[cgNotas.NumeroNota, i + 1]    := NFormat(vNotas[i].numero_nota);
    vForm.sgNotasFiscais.Cells[cgNotas.TipoNota, i + 1]      := vNotas[i].TipoNotaAnalitico;
    vForm.sgNotasFiscais.Cells[cgNotas.DataEmissao, i + 1]   := DHFormatN(vNotas[i].data_hora_emissao);
    vForm.sgNotasFiscais.Cells[cgNotas.TipoMovimento, i + 1] := vNotas[i].TipoMovimentoAnalitico;
    vForm.sgNotasFiscais.Cells[cgNotas.Valor, i + 1]         := NFormat(vNotas[i].valor_total);
    vForm.sgNotasFiscais.Cells[cgNotas.Empresa, i + 1]       := getInformacao(vNotas[i].empresa_id, vNotas[i].nome_fantasia_emitente
    );
  end;
  vForm.sgNotasFiscais.SetLinhasGridPorTamanhoVetor( Length(vNotas) );

  vForm.ShowModal;

  vForm.Free;
end;

procedure TFormInformacoesAcumulado.FormShow(Sender: TObject);
begin
  inherited;
  pcDados.ActivePageIndex := 0;
end;

procedure TFormInformacoesAcumulado.sbInformacoesTurnoClick(Sender: TObject);
begin
  inherited;
  Informacoes.TurnoCaixa.Informar(eTurnoId.AsInt);
end;

procedure TFormInformacoesAcumulado.sgNotasFiscaisDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.NotaFiscal.Informar( SFormatInt(sgNotasFiscais.Cells[cgNotas.NotaFiscalId, sgNotasFiscais.Row]) );
end;

procedure TFormInformacoesAcumulado.sgNotasFiscaisDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[cgNotas.NotaFiscalId, cgNotas.NumeroNota, cgNotas.Valor] then
    vAlinhamento := taRightJustify
  else if ACol in[cgNotas.TipoNota, cgNotas.TipoMovimento] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgNotasFiscais.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesAcumulado.sgPedidosDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar( SFormatInt(sgPedidos.Cells[cgPedidos.PedidoId, sgPedidos.Row]) );
end;

procedure TFormInformacoesAcumulado.sgPedidosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[
    cgPedidos.PedidoId,
    cgPedidos.Valor,
    cgPedidos.ValorDevolvidos]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgPedidos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesAcumulado.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    cgItens.ProdutoId,
    cgItens.PrecoUnitario,
    cgItens.Quantidade,
    cgItens.ValorDesconto,
    cgItens.OutrasDespesas,
    cgItens.ValorLiquido,
    cgItens.ValorFrete
  ] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesAcumulado.sgReceberDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.TituloReceber.Informar( SFormatInt(sgReceber.Cells[crbReceberId, sgReceber.Row]) );
end;

procedure TFormInformacoesAcumulado.sgReceberDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[crbReceberId, crbValorDocumento, crbParcela, crbNumeroParcelas] then
    vAlinhamento := taRightJustify
  else if ACol = crbStatus then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgReceber.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesAcumulado.sgReceberGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol <> crbStatus then
    Exit;

  AFont.Style := [fsBold];
  if sgReceber.Cells[crbStatus, ARow] = 'Aberto' then
    AFont.Color := $000096DB
  else
    AFont.Color := clBlue;
end;

procedure TFormInformacoesAcumulado.tsFinanceirosShow(Sender: TObject);
var
  i: Integer;
  vReceber: TArray<RecContasReceber>;
begin
  inherited;
  sgReceber.ClearGrid();
  vReceber := _ContasReceber.BuscarContasReceber(Sessao.getConexaoBanco, 9, [FAcumuladoId]);
  for i := Low(vReceber) to High(vReceber) do begin
    sgReceber.Cells[crbReceberId, i + 1]      := NFormat(vReceber[i].ReceberId);
    sgReceber.Cells[crbTipoCobranca, i + 1]   := getInformacao(vReceber[i].cobranca_id, vReceber[i].nome_tipo_cobranca);
    sgReceber.Cells[crbStatus, i + 1]         := IIfStr(vReceber[i].status = 'A', 'Aberto', 'Baixado');
    sgReceber.Cells[crbDocumento, i + 1]      := vReceber[i].documento;
    sgReceber.Cells[crbDataVencimento, i + 1] := DFormat(vReceber[i].data_vencimento);
    sgReceber.Cells[crbValorDocumento, i + 1] := NFormat(vReceber[i].ValorDocumento);
    sgReceber.Cells[crbParcela, i + 1]        := NFormat(vReceber[i].Parcela);
    sgReceber.Cells[crbNumeroParcelas, i + 1] := NFormat(vReceber[i].NumeroParcelas);
  end;
  sgReceber.SetLinhasGridPorTamanhoVetor( Length(vReceber) );
end;

end.
