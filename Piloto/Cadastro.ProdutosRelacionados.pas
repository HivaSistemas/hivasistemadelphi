unit Cadastro.ProdutosRelacionados;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, _Sessao, _Biblioteca, _ProdutosRelacionados,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameProdutos, Vcl.StdCtrls, _RecordsEspeciais,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _RecordsCadastros, System.Math,
  _HerancaCadastro, CheckBoxLuka;

type
  TFormCadastroProdutosRelacionados = class(TFormHerancaCadastroCodigo)
    FrProdutoPrincipal: TFrProdutos;
    FrProdutoRelacionado: TFrProdutos;
    eQuantidadeSugestao: TEditLuka;
    lb1: TLabel;
    sgProdutosRelacionados: TGridLuka;
    procedure FormShow(Sender: TObject);
    procedure sgProdutosRelacionadosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure eQuantidadeSugestaoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure sbDesfazerClick(Sender: TObject);
    procedure sgProdutosRelacionadosDblClick(Sender: TObject);
    procedure sbGravarClick(Sender: TObject);
    procedure sgProdutosRelacionadosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    FLinhaEditando: Integer;

    procedure Modo(pEditando: Boolean); reintroduce;

    procedure FrProdutoPrincipalOnAposPesquisar(Sender: TObject);
    procedure FrProdutoPrincipalOnAposDeletar(Sender: TObject);

    procedure FrProdutoRelacionadoOnAposDeletar(Sender: TObject);
  end;

implementation

{$R *.dfm}

{ TFormCadastroProdutosRelacionados }

const
  coProdutoId     = 0;
  coNome          = 1;
  coQuantidadeSug = 2;

procedure TFormCadastroProdutosRelacionados.eQuantidadeSugestaoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  vPosic: Integer;
  vLinha: Integer;
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if FrProdutoRelacionado.EstaVazio then begin
    Exclamar('O produto a ser relacionado n�o foi informado, verifique!');
    SetarFoco(FrProdutoRelacionado);
    Exit;
  end;

  if FrProdutoPrincipal.getProduto.produto_id = FrProdutoRelacionado.getProduto.produto_id then begin
    Exclamar('O produto a ser relacionado n�o pode ser o mesmo principal, verifique!');
    SetarFoco(FrProdutoRelacionado);
    Exit;
  end;

  if FLinhaEditando > 0 then
    vLinha := FLinhaEditando
  else if sgProdutosRelacionados.Cells[coProdutoId, 1] = '' then
    vLinha := 1
  else begin
    vPosic := sgProdutosRelacionados.Localizar([coProdutoId], [NFormat(FrProdutoRelacionado.getProduto().produto_id)]);
    if vPosic > 0 then begin
      Exclamar('Este produto j� foi inserido no grid, verifique!');
      SetarFoco(FrProdutoRelacionado);
      Exit;
    end;

    vLinha := sgProdutosRelacionados.RowCount;
  end;

  sgProdutosRelacionados.Cells[coProdutoId, vLinha]     := NFormat(FrProdutoRelacionado.getProduto().produto_id);
  sgProdutosRelacionados.Cells[coNome, vLinha]          := FrProdutoRelacionado.getProduto().nome;
  sgProdutosRelacionados.Cells[coQuantidadeSug, vLinha] := NFormat(eQuantidadeSugestao.AsDouble, 4);

  if FLinhaEditando = 0 then
    sgProdutosRelacionados.RowCount := vLinha + 1;

  FLinhaEditando := 0;
  FrProdutoRelacionado.Clear;
  eQuantidadeSugestao.AsDouble := 0;

  SetarFoco(FrProdutoRelacionado);
end;

procedure TFormCadastroProdutosRelacionados.FormCreate(Sender: TObject);
begin
  FrProdutoPrincipal.OnAposPesquisar := FrProdutoPrincipalOnAposPesquisar;
  FrProdutoPrincipal.OnAposDeletar   := FrProdutoPrincipalOnAposDeletar;
  FrProdutoRelacionado.OnAposDeletar := FrProdutoRelacionadoOnAposDeletar;
end;

procedure TFormCadastroProdutosRelacionados.FormShow(Sender: TObject);
begin
  Modo(False);
end;

procedure TFormCadastroProdutosRelacionados.FrProdutoPrincipalOnAposDeletar(Sender: TObject);
begin
  Modo(False);
end;

procedure TFormCadastroProdutosRelacionados.FrProdutoPrincipalOnAposPesquisar(Sender: TObject);
var
  i: Integer;
  vProdutosRelacionados: TArray<RecProdutosRelacionados>;
begin
  Modo(True);

  vProdutosRelacionados := _ProdutosRelacionados.BuscarProdutosRelacionados(Sessao.getConexaoBanco, 0, [FrProdutoPrincipal.getProduto.produto_id, Sessao.getParametrosEmpresa.EmpresaId]);
  for i := Low(vProdutosRelacionados) to High(vProdutosRelacionados) do begin
    sgProdutosRelacionados.Cells[coProdutoId, i + 1]     := NFormat(vProdutosRelacionados[i].ProdutoRelacionadoId);
    sgProdutosRelacionados.Cells[coNome, i + 1]          := vProdutosRelacionados[i].NomeProdutoRelacionado;
    sgProdutosRelacionados.Cells[coQuantidadeSug, i + 1] := NFormat(vProdutosRelacionados[i].QuantidadeSugestao, 4);
  end;
  sgProdutosRelacionados.RowCount := IfThen(Length(vProdutosRelacionados) > 1, High(vProdutosRelacionados) + 2, 2);
end;

procedure TFormCadastroProdutosRelacionados.FrProdutoRelacionadoOnAposDeletar(Sender: TObject);
begin
  FLinhaEditando := 0;
  eQuantidadeSugestao.AsDouble := 0;
end;

procedure TFormCadastroProdutosRelacionados.Modo(pEditando: Boolean);
begin
  _Biblioteca.Habilitar([
    FrProdutoRelacionado,
    eQuantidadeSugestao,
    sgProdutosRelacionados,
    sbGravar,
    sbDesfazer],
    pEditando
  );

  FLinhaEditando := 0;
  _Biblioteca.Habilitar([FrProdutoPrincipal], not pEditando, not pEditando);

  if not pEditando then
    SetarFoco(FrProdutoPrincipal)
  else
    SetarFoco(FrProdutoRelacionado);
end;

procedure TFormCadastroProdutosRelacionados.sbDesfazerClick(Sender: TObject);
begin
  if Sessao.FinalizandoPorTempoInativo then
    Exit;

  if not _Biblioteca.Perguntar('Deseja realmente sair?') then
    Exit;

  Modo(False);
end;

procedure TFormCadastroProdutosRelacionados.sbGravarClick(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  vProdutosRelacionados: TArray<RecProdutosRelacionados>;
begin
  if sgProdutosRelacionados.Cells[coProdutoId, 1] <> '' then begin
    SetLength(vProdutosRelacionados, sgProdutosRelacionados.RowCount - 1);
    for i := 1 to sgProdutosRelacionados.RowCount - 1 do begin
      vProdutosRelacionados[i - 1].ProdutoRelacionadoId := SFormatInt(sgProdutosRelacionados.Cells[coProdutoId, i]);
      vProdutosRelacionados[i - 1].QuantidadeSugestao   := SFormatDouble(sgProdutosRelacionados.Cells[coQuantidadeSug, i]);
    end;
  end;

  vRetBanco :=
    _ProdutosRelacionados.AtualizarProdutosRelacionado(
      Sessao.getConexaoBanco,
      FrProdutoPrincipal.getProduto().produto_id,
      vProdutosRelacionados
    );

  if vRetBanco.TeveErro then begin
    Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  _Biblioteca.Informar(coAlteracaoRegistroSucesso);
  Modo(False);
end;

procedure TFormCadastroProdutosRelacionados.sgProdutosRelacionadosDblClick(Sender: TObject);
begin
  inherited;
  if sgProdutosRelacionados.Cells[coProdutoId, sgProdutosRelacionados.Row] = '' then
    Exit;

  FlinhaEditando := sgProdutosRelacionados.Row;
  eQuantidadeSugestao.AsDouble := SFormatDouble(sgProdutosRelacionados.Cells[coQuantidadeSug, sgProdutosRelacionados.Row]);
  FrProdutoRelacionado.InserirDadoPorChave(SFormatInt(sgProdutosRelacionados.Cells[coProdutoId, sgProdutosRelacionados.Row]), False);

  SetarFoco(FrProdutoRelacionado);
end;

procedure TFormCadastroProdutosRelacionados.sgProdutosRelacionadosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coProdutoId, coQuantidadeSug] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgProdutosRelacionados.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormCadastroProdutosRelacionados.sgProdutosRelacionadosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_DELETE then
    sgProdutosRelacionados.DeleteRow(sgProdutosRelacionados.Row);
end;

end.
