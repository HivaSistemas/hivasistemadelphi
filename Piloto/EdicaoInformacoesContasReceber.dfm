inherited FormEdicaoInformacoesContasReceber: TFormEdicaoInformacoesContasReceber
  Caption = 'Edi'#231#227'o de informa'#231#245'es de contas a receber'
  ClientHeight = 91
  ClientWidth = 367
  OnShow = FormShow
  ExplicitWidth = 373
  ExplicitHeight = 120
  PixelsPerInch = 96
  TextHeight = 14
  object lb6: TLabel [0]
    Left = 32
    Top = 6
    Width = 81
    Height = 14
    Caption = 'Data de vencto'
  end
  object lbl18: TLabel [1]
    Left = 127
    Top = 6
    Width = 70
    Height = 14
    Caption = 'Valor do doc.'
  end
  object lb14: TLabel [2]
    Left = 215
    Top = 6
    Width = 40
    Height = 14
    Caption = 'Parcela'
  end
  object lb1: TLabel [3]
    Left = 271
    Top = 6
    Width = 48
    Height = 14
    Caption = 'Qtd.parc.'
  end
  inherited pnOpcoes: TPanel
    Top = 54
    Width = 367
    TabOrder = 4
    ExplicitTop = 54
    ExplicitWidth = 341
  end
  object eDataVencimento: TEditLukaData
    Left = 32
    Top = 20
    Width = 89
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1; '
    MaxLength = 10
    TabOrder = 0
    Text = '  /  /    '
    OnKeyDown = ProximoCampo
  end
  object eValorDocumento: TEditLuka
    Left = 127
    Top = 20
    Width = 82
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 1
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eParcela: TEditLuka
    Left = 215
    Top = 20
    Width = 50
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 3
    TabOrder = 2
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eQtdeParcelas: TEditLuka
    Left = 271
    Top = 20
    Width = 58
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 3
    TabOrder = 3
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
