unit FrameFornecedores;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _RecordsCadastros,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Sessao, System.Math, _Fornecedores, PesquisaFornecedores,
  Vcl.Buttons, Vcl.Menus;

type
  TFrFornecedores = class(TFrameHenrancaPesquisas)
    ckRevenda: TCheckBox;
    ckUsoConsumo: TCheckBox;
    ckServico: TCheckBox;
  public
    function GetFornecedor(pLinha: Integer = -1): RecFornecedores;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrFornecedores }

function TFrFornecedores.AdicionarDireto: TObject;
var
  vDados: TArray<RecFornecedores>;
begin
  vDados := _Fornecedores.BuscarFornecedores(Sessao.getConexaoBanco, 0, [FChaveDigitada], ckRevenda.Checked, ckUsoConsumo.Checked, ckServico.Checked);
  if vDados = nil then
    Result := nil
  else
    Result := vDados[0];
end;

function TFrFornecedores.AdicionarPesquisando: TObject;
begin
  Result := PesquisaFornecedores.PesquisarFornecedor(ckRevenda.Checked, ckUsoConsumo.Checked, ckServico.Checked);
end;

function TFrFornecedores.GetFornecedor(pLinha: Integer): RecFornecedores;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecFornecedores(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrFornecedores.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecFornecedores(FDados[i]).cadastro_id = RecFornecedores(pSender).cadastro_id then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrFornecedores.MontarGrid;
var
  i: Integer;
  pSender: RecFornecedores;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      pSender := RecFornecedores(FDados[i]);
      AAdd([IntToStr(pSender.cadastro_id), pSender.cadastro.nome_fantasia]);
    end;
  end;
end;

end.
