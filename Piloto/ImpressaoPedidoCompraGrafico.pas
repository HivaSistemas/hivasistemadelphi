unit ImpressaoPedidoCompraGrafico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosGraficos, QRExport, _Biblioteca, _Sessao,
  QRPDFFilt, QRWebFilt, QRCtrls, QuickRpt, Vcl.ExtCtrls, _Compras, _ComprasItens;

type
  TFormImpressaoPedidoCompraGrafico = class(TFormHerancaRelatoriosGraficos)
    qrbndColumnHeaderBand1: TQRBand;
    qrbndDetailBand1: TQRBand;
    qr23: TQRLabel;
    qr24: TQRLabel;
    qr25: TQRLabel;
    qr6: TQRLabel;
    qr26: TQRLabel;
    qr28: TQRLabel;
    qr27: TQRLabel;
    qr2: TQRLabel;
    qrCompraId: TQRLabel;
    qr8: TQRLabel;
    qrComprador: TQRLabel;
    qr12: TQRLabel;
    qrDataFaturamento: TQRLabel;
    qr15: TQRLabel;
    qrPrevisaoChegada: TQRLabel;
    qr17: TQRLabel;
    qrQtdeProdutos: TQRLabel;
    qrValorTotal: TQRLabel;
    qr20: TQRLabel;
    qrProdutoId: TQRLabel;
    qrNomeProduto: TQRLabel;
    qrMarca: TQRLabel;
    qrPrecoUnitario: TQRLabel;
    qrQtde: TQRLabel;
    qrUnidade: TQRLabel;
    qrValorTotalProduto: TQRLabel;
    qr4: TQRLabel;
    qrObservacoes: TQRMemo;
    qr9: TQRLabel;
    qrlFornecedor: TQRLabel;
    QRShape7: TQRShape;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLabel1: TQRLabel;
    qrCodigoOriginal: TQRLabel;
    procedure qrRelatorioA4BeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure qrRelatorioA4NeedData(Sender: TObject; var MoreData: Boolean);
    procedure qrbndDetailBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
  private
    FPosicao: Integer;
    FItens: TArray<RecComprasItens>;
  end;

procedure Imprimir(pCompraId: Integer);

implementation

{$R *.dfm}

procedure Imprimir(pCompraId: Integer);
var
  vForm: TFormImpressaoPedidoCompraGrafico;
  vImpressora: RecImpressora;
  vCompras: TArray<RecCompras>;
begin
  if pCompraId = 0 then
    Exit;

  vCompras := _Compras.BuscarCompras(Sessao.getConexaoBanco, 0, [pCompraId]);
  if vCompras = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  vImpressora.TipoImpressora := 'G';
  vImpressora.AbrirPreview   := True;

  vForm := TFormImpressaoPedidoCompraGrafico.Create(Application, vImpressora);
  vForm.PreencherCabecalho(Sessao.getEmpresaLogada.EmpresaId, True);

  vForm.FItens := _ComprasItens.BuscarComprasItens(Sessao.getConexaoBanco, 0, [pCompraId]);

  vForm.qrCompraId.Caption        := NFormat(vCompras[0].CompraId);
  vForm.qrQtdeProdutos.Caption    := NFormat(Length(vForm.FItens));
  vForm.qrValorTotal.Caption      := NFormat(vCompras[0].ValorLiquido);
  vForm.qrComprador.Caption       := getInformacao(vCompras[0].CompradorId, vCompras[0].NomeComprador);
  vForm.qrDataFaturamento.Caption := DFormat(vCompras[0].DataFaturamento);
  vForm.qrPrevisaoChegada.Caption := DFormat(vCompras[0].PrevisaoEntrega);
  vForm.qrlFornecedor.Caption     := getInformacao(vCompras[0].FornecedorId, vCompras[0].NomeFornecedor);
  vForm.qrObservacoes.Lines.Text  := vCompras[0].Observacoes;

  vForm.Imprimir;

  vForm.Free;
end;

procedure TFormImpressaoPedidoCompraGrafico.qrbndDetailBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrProdutoId.Caption         := NFormat(FItens[FPosicao].ProdutoId);
  qrNomeProduto.Caption       := FItens[FPosicao].NomeProduto;
  qrMarca.Caption             := FItens[FPosicao].NomeMarca;
  qrCodigoOriginal.Caption    := FItens[FPosicao].CodigoOriginal;
  qrPrecoUnitario.Caption     := NFormat(FItens[FPosicao].PrecoUnitario);
  qrQtde.Caption              := NFormatNEstoque(FItens[FPosicao].Quantidade);
  qrUnidade.Caption           := FItens[FPosicao].UnidadeCompraId;
  qrValorTotalProduto.Caption := NFormat(FItens[FPosicao].ValorLiquido);
end;

procedure TFormImpressaoPedidoCompraGrafico.qrRelatorioA4BeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicao := -1;
end;

procedure TFormImpressaoPedidoCompraGrafico.qrRelatorioA4NeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicao);
  MoreData := FPosicao < Length(FItens);
end;

end.
