unit GerarEntregas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _Biblioteca, System.Math, Buscar.Funcionario,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, Vcl.StdCtrls, FrameNumeros, FrameProdutos, _Sessao, _EntregasPendentes,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameClientes, Vcl.ComCtrls, Vcl.Buttons, Informacoes.Orcamento,
  Vcl.Menus, _RecordsExpedicao, _EntregasItensPendentes, _RecordsEspeciais, _ComunicacaoNFE, _Entregas,
  Buscar.DocumentoFiscalEmitir, Frame.DocumentoFiscalEmitir, Legendas, _Retiradas, FrameDataInicialFinal, _Orcamentos,
  StaticTextLuka, _Devolucoes, DefinirEntregas, ComboBoxLuka, ImpressaoComprovanteEntregaGrafico,
  MemoAltis, FrameVendedores, CheckBoxLuka, FrameLocais, _RecordsNotasFiscais, _NotasFiscais;

type
  TFormGerarEntregas = class(TFormHerancaRelatoriosPageControl)
    FrClientes: TFrClientes;
    FrProdutos: TFrProdutos;
    FrOrcamentos: TFrNumeros;
    sgOrcamentos: TGridLuka;
    spSeparador: TSplitter;
    sgItens: TGridLuka;
    pmEntregasPendentes: TPopupMenu;
    pmItens: TPopupMenu;
    FrPrevisaoEntrega: TFrDataInicialFinal;
    FrDataRecebimento: TFrDataInicialFinal;
    StaticTextLuka1: TStaticTextLuka;
    StaticTextLuka2: TStaticTextLuka;
    cbAgrupamento: TComboBoxLuka;
    lb1: TLabel;
    pnRodape: TPanel;
    st3: TStaticText;
    st6: TStaticText;
    stPesoTotal: TStaticText;
    stQuantidadeProdutos: TStaticText;
    st7: TStaticText;
    meObservacoes: TMemoAltis;
    lb2: TLabel;
    FrVendedores: TFrVendedores;
    miGerarEntregaProdutosSelecionados: TSpeedButton;
    miGerarRetiradaProdutosSelecionados: TSpeedButton;
    miTransformarEntregasSelecionadasSemPrevisao: TSpeedButton;
    miDefinirQuantidadeTodosItens: TSpeedButton;
    miRemoverQuantidadeTodosItensF8: TSpeedButton;
    miLegendas: TSpeedButton;
    FrLocais: TFrLocais;
    Label1: TLabel;
    cbAguardandoCliente: TComboBoxLuka;
    procedure sgOrcamentosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
    procedure FormShow(Sender: TObject);
    procedure sgOrcamentosClick(Sender: TObject);
    procedure miGerarEntregaProdutosSelecionadosClick(Sender: TObject);
    procedure sgOrcamentosDblClick(Sender: TObject);
    procedure miDefinirQuantidadeTodosItensClick(Sender: TObject);
    procedure miRemoverQuantidadeTodosItensF8Click(Sender: TObject);
    procedure sgItensKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure miTransformarEntregasSelecionadasSemPrevisaoClick(Sender: TObject);
    procedure sgOrcamentosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure miLegendasClick(Sender: TObject);
    procedure sgOrcamentosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    FItens: TArray<RecEntregasItensPendentes>;
    FLegendaGrid: TImage;

    procedure SalvarItemGerarEntrega(
      pOrcamentoId: Integer;
      pItemId: Integer;
      pLote: string;
      pQuantidade: Double;
      pPesoTotal: Double;
      pLocalId: Integer
    );

    function LengendaGrid(pRetornoTotal: Boolean; pRetornoParcial: Boolean): TPicture;
    procedure TotalizarProdutos;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

const
  (* Grid Orcamentos *)
  coLegenda             = 0;
  coOrcamentoId         = 1;
  coCliente             = 2;
  coEmpresa             = 3;
  coLocal               = 4;
  coDataCadastro        = 5;
  coVendedor            = 6;
  coDataHoraEntrega     = 7;
  coEmpresaId           = 8;
  coLocalId             = 9;
  coTipoRetornoEntrega  = 10;
  coTipoLinha           = 11;
  coObservacoes         = 12;
  coMaximo              = 13;

  (* Grid de Itens *)
  ciProdutoId     = 0;
  ciNome          = 1;
  ciMarca         = 2;
  ciQuantidade    = 3;
  ciUnidade       = 4;
  ciLote          = 5;
  ciQtdeEntregar  = 6;
  ciPesoTotalEntr = 7;
  ciItemId        = 8;
  ciMultiploVenda = 9;
  ciPesoUnitario  = 10;

  coCorRetornoTotal   = clRed;
  coCorRetornoParcial = $000080FF;

  clCabecalho = 'CAB';
  clNormal    = 'NOR';

procedure TFormGerarEntregas.Carregar(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vAgrupamentoId: Integer;

  vComando: string;
  vOrcamentoIds: TArray<Integer>;
  vEntregas: TArray<RecEntregasPendentes>;
begin
  inherited;
  vLinha := 0;
  vAgrupamentoId := -1;
  sgItens.ClearGrid;
  sgOrcamentos.ClearGrid;

  FItens := nil;
  vOrcamentoIds := nil;

  vComando :=
    'where (ENT.ORCAMENTO_ID, ENT.LOCAL_ID, ENT.PREVISAO_ENTREGA) in(select distinct ORCAMENTO_ID, LOCAL_ID, PREVISAO_ENTREGA from ENTREGAS_ITENS_PENDENTES where SALDO > 0) ';

  if not FrOrcamentos.EstaVazio then
    vComando := vComando + ' and ' + FrOrcamentos.TrazerFiltros('ORC.ORCAMENTO_ID');

  if not FrClientes.EstaVazio then
    vComando := vComando + ' and ' + FrClientes.getSqlFiltros('ORC.CLIENTE_ID');

  if not FrVendedores.EstaVazio then
    vComando := vComando + ' and ' + FrVendedores.getSqlFiltros('ORC.VENDEDOR_ID');

  if not FrProdutos.EstaVazio then
    vComando := vComando + ' and ORC.ORCAMENTO_ID in(select distinct ORCAMENTO_ID from ORCAMENTOS_ITENS where ' + FrProdutos.getSqlFiltros('PRODUTO_ID') + ') ';

  if not FrPrevisaoEntrega.NenhumaDataValida then
    vComando := vComando + ' and ' + FrPrevisaoEntrega.getSqlFiltros('ENT.PREVISAO_ENTREGA');

  if not FrDataRecebimento.NenhumaDataValida then
    vComando := vComando + ' and ' + FrDataRecebimento.getSqlFiltros('trunc(DATA_HORA_RECEBIMENTO)');

  if not FrLocais.EstaVazio then
    vComando := vComando + ' and ' + FrLocais.getSqlFiltros('ENT.LOCAL_ID');

  vComando := vComando + ' and ENT.EMPRESA_ID = ' + IntToStr(Sessao.getEmpresaLogada.EmpresaId);

  vComando := vComando + ' and ORC.AGUARDANDO_CLIENTE = ''' + cbAguardandoCliente.GetValor + ''' ';

  if cbAgrupamento.GetValor = 'ROT' then
    vComando := vComando + ' order by BAI.ROTA_ID '
  else if cbAgrupamento.GetValor = 'CLI' then
    vComando := vComando + ' order by ORC.CLIENTE_ID '
  else
    vComando := vComando + ' order by ORC.ORCAMENTO_ID ';

  vEntregas := _EntregasPendentes.BuscarEntregasPendentesComando(Sessao.getConexaoBanco, vComando);
  if vEntregas = nil then begin
    NenhumRegistro;
    Exit;
  end;

  SetLength(vOrcamentoIds, Length(vEntregas));
  for i := Low(vEntregas) to High(vEntregas) do begin
    Inc(vLinha);

    if cbAgrupamento.GetValor = 'ROT' then begin
      if vAgrupamentoId <> vEntregas[i].RotaId then begin
        if vEntregas[i].RotaId = 0 then
          sgOrcamentos.Cells[coOrcamentoId, vLinha] := 'ROTA: SEM ROTA DEFINIDA'
        else
          sgOrcamentos.Cells[coOrcamentoId, vLinha] := 'ROTA: ' + NFormat(vEntregas[i].RotaId) + ' - ' + vEntregas[i].NomeRota;

        sgOrcamentos.Cells[coTipoLinha, vLinha] := clCabecalho;

        Inc(vLinha);
        vAgrupamentoId := vEntregas[i].RotaId;
      end;
    end
    else if cbAgrupamento.GetValor = 'CLI' then begin
      if vAgrupamentoId <> vEntregas[i].ClienteId then begin
        sgOrcamentos.Cells[coOrcamentoId, vLinha] := 'CLIENTE: ' + NFormat(vEntregas[i].ClienteId) + ' - ' + vEntregas[i].NomeCliente;

        sgOrcamentos.Cells[coTipoLinha, vLinha] := clCabecalho;

        Inc(vLinha);
        vAgrupamentoId := vEntregas[i].ClienteId;
      end;
    end;

    sgOrcamentos.Cells[coOrcamentoId, vLinha]       := NFormat(vEntregas[i].OrcamentoId);
    sgOrcamentos.Cells[coCliente, vLinha]           := NFormat(vEntregas[i].ClienteId) + ' - ' + vEntregas[i].NomeCliente;
    sgOrcamentos.Cells[coEmpresa, vLinha]           := NFormat(vEntregas[i].EmpresaId) + ' - ' + vEntregas[i].NomeEmpresa;
    sgOrcamentos.Cells[coLocal, vLinha]             := NFormat(vEntregas[i].LocalId) + ' - ' + vEntregas[i].NomeLocal;
    sgOrcamentos.Cells[coDataCadastro, vLinha]      := DFormat(vEntregas[i].DataCadastro);
    sgOrcamentos.Cells[coVendedor, vLinha]          := NFormat(vEntregas[i].VendedorId) + ' - ' + vEntregas[i].NomeVendedor;
    sgOrcamentos.Cells[coVendedor, vLinha]          := NFormat(vEntregas[i].VendedorId) + ' - ' + vEntregas[i].NomeVendedor;
    sgOrcamentos.Cells[coDataHoraEntrega, vLinha]   := DHFormat(vEntregas[i].PrevisaoEntrega);

    sgOrcamentos.Cells[coObservacoes, vLinha]        := vEntregas[i].ObservacoesExpedicao;
    sgOrcamentos.Cells[coEmpresaId, vLinha]          := NFormat(vEntregas[i].EmpresaId);
    sgOrcamentos.Cells[coLocalId, vLinha]            := NFormat(vEntregas[i].LocalId);
    sgOrcamentos.Cells[coTipoRetornoEntrega, vLinha] := vEntregas[i].TipoRetornoEntrega;

    _Biblioteca.AddNoVetorSemRepetir(vOrcamentoIds, vEntregas[i].OrcamentoId);
  end;
  sgOrcamentos.SetLinhasGridPorTamanhoVetor( vLinha );

  vComando :=
    'where EIP.SALDO > 0 ' +
    'and ' + FiltroInInt('EIP.ORCAMENTO_ID', vOrcamentoIds) +
    'order by ' +
    '  PRO.NOME';

  FItens := _EntregasItensPendentes.BuscarEntregasItensPendentesComando(Sessao.getConexaoBanco, vComando);
  sgOrcamentosClick(Sender);

  pcDados.ActivePage := tsResultado;
  sgOrcamentos.SetFocus;
end;

procedure TFormGerarEntregas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  FLegendaGrid.Free;
end;

procedure TFormGerarEntregas.FormCreate(Sender: TObject);
begin
  inherited;
  FLegendaGrid := TImage.Create(Self);
  FLegendaGrid.Height := 15;

  _Biblioteca.LimparCampos([stPesoTotal, stQuantidadeProdutos]);
end;

procedure TFormGerarEntregas.FormShow(Sender: TObject);
begin
  inherited;
  sgItens.Col := ciQtdeEntregar;
end;

procedure TFormGerarEntregas.Imprimir(Sender: TObject);
begin
  inherited;

end;

function TFormGerarEntregas.LengendaGrid(pRetornoTotal: Boolean; pRetornoParcial: Boolean): TPicture;
var
	p: Integer;
  l: Integer;
begin
 	l := 0;

  if pRetornoTotal then
    Inc(l, 10);

  if pRetornoParcial then
    Inc(l, 10);

  p := 1;
  FLegendaGrid.Width := l + p;
  FLegendaGrid.Picture := nil;

  if pRetornoTotal then begin
  	FLegendaGrid.Canvas.Pen.Color := coCorRetornoTotal;
  	FLegendaGrid.Canvas.Brush.Color := coCorRetornoTotal;
  	FLegendaGrid.Canvas.Rectangle(p, 0, p + 5, 15);
    Inc(p, 10);
  end;

  if pRetornoParcial then begin
  	FLegendaGrid.Canvas.Pen.Color := coCorRetornoParcial;
  	FLegendaGrid.Canvas.Brush.Color := coCorRetornoParcial;
  	FLegendaGrid.Canvas.Rectangle(p, 0, p + 5, 15);
//    Inc(p, 10);
  end;

  Result := FLegendaGrid.Picture;
end;

procedure TFormGerarEntregas.miDefinirQuantidadeTodosItensClick(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  for i := 1 to sgItens.RowCount -1 do begin
    sgItens.Cells[ciQtdeEntregar, i] := sgItens.Cells[ciQuantidade, i];
    SalvarItemGerarEntrega(
      SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]),
      SFormatInt(sgItens.Cells[ciItemId, i]),
      sgItens.Cells[ciLote, i],
      SFormatDouble(sgItens.Cells[ciQtdeEntregar, i]),
      SFormatDouble(sgItens.Cells[ciQtdeEntregar, i]) * SFormatDouble(sgItens.Cells[ciPesoUnitario, i]),
      SFormatInt(sgOrcamentos.Cells[coLocalId, sgOrcamentos.Row])
    );
  end;
end;

procedure TFormGerarEntregas.miGerarEntregaProdutosSelecionadosClick(Sender: TObject);
var
  i: Integer;
  j: Integer;

  vLocalId: Integer;
  vOrcamentoId: Integer;
  vAgrupamentoId: Integer;
  vNotasIds: TArray<Integer>;

  vRetBanco: RecRetornoBD;
  vRetiradaEntregaId: Integer;
  vPrevisaoEntrega: TDateTime;

  vRetiradas: TArray<RecRetirada>;
  vEntregas: TArray<RecEntrega>;
  vEntregasIds: TArray<Integer>;

  vOrcamentoIds: TArray<Integer>;
  vItens: TArray<RecEntregasItensPendentes>;
  vDocumentoEmitir: TRetornoTelaFinalizar<RecDocumentoFiscalEmitir>;
  vRecRetornoBD: RecRetornoBD;
  vNotas: TArray<RecNotaFiscal>;
begin
  inherited;

  vNotasIds     := nil;
  vOrcamentoIds := nil;

  (* Descobrindo quais pedidos tem entregas a serem realizadas *)
  for i := Low(FItens) to High(FItens) do begin
    if NFormatN(FItens[i].QtdeEntregar) = '' then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir(vOrcamentoIds, FItens[i].OrcamentoId);
  end;

  if vOrcamentoIds = nil then begin
    Exclamar('N�o foi definido nenhum produto para gera��o de retirada!');
    Exit;
  end;

  for i := Low(vOrcamentoIds) to High(vOrcamentoIds) do begin
    if _Devolucoes.ExistemDevolucoesPendentes(Sessao.getConexaoBanco, vOrcamentoIds[i]) then begin
      Exclamar('N�o � permitida esta opera��o enquanto houver pend�ncias de devolu��es para o pedido ' + NFormat(vOrcamentoIds[i]) + '!');
      Abort;
    end;
  end;

  vDocumentoEmitir :=
    Buscar.DocumentoFiscalEmitir.Buscar(
      (Sender = miGerarEntregaProdutosSelecionados) and (Sessao.getParametrosEmpresa.TrabalharControleSeparacao = 'S')
    );

  if vDocumentoEmitir.RetTela = trCancelado then
    Exit;

  Sessao.getConexaoBanco.IniciarTransacao;

  if Sender = miGerarEntregaProdutosSelecionados then
    vAgrupamentoId := _Entregas.getAgrupamentoId(Sessao.getConexaoBanco)
  else
    vAgrupamentoId := _Retiradas.getAgrupamentoId(Sessao.getConexaoBanco);

  for i := 1 to sgOrcamentos.RowCount -1 do begin
    if sgOrcamentos.Cells[coTipoLinha, i] = clCabecalho then
      Continue;

    vItens := nil;
    vOrcamentoId := SFormatInt(sgOrcamentos.Cells[coOrcamentoId, i]);
    vLocalId := SFormatInt(sgOrcamentos.Cells[coLocalId, i]);
    vPrevisaoEntrega := ToDataHora(sgOrcamentos.Cells[coDataHoraEntrega, i]);

    (* Montando os itens a serem entregues *)
    for j := Low(FItens) to High(FItens) do begin
      if NFormatN(FItens[j].QtdeEntregar) = '' then
        Continue;

      if
        (vLocalId <> FItens[j].LocalId) or
        (vOrcamentoId <> FItens[j].OrcamentoId) or
        (vPrevisaoEntrega <> FItens[j].PrevisaoEntrega)
      then
        Continue;

      SetLength(vItens, Length(vItens) + 1);
      vItens[High(vItens)] := FItens[j];
    end;

    if vItens = nil then
      Continue;

    if Sender = miGerarEntregaProdutosSelecionados then begin
      vRetBanco :=
        _EntregasPendentes.GerarEntregaPendencia(
          Sessao.getConexaoBanco,
          vOrcamentoId,
          Sessao.getEmpresaLogada.EmpresaId,
          vLocalId,
          vPrevisaoEntrega,
          vAgrupamentoId,
          vDocumentoEmitir.Dados.TipoNotaGerar,
          vItens,
          Sessao.getParametros.UtilizaControleManifesto = 'S'
        );
    end
    else begin
      vRetBanco :=
        _EntregasPendentes.GerarRetiradaPendenciaEntrega(
          Sessao.getConexaoBanco,
          vOrcamentoId,
          Sessao.getEmpresaLogada.EmpresaId,
          vLocalId,
          vPrevisaoEntrega,
          vAgrupamentoId,
          vDocumentoEmitir.Dados.TipoNotaGerar,
          vItens
        );
    end;

    Sessao.AbortarSeHouveErro(vRetBanco);
    vRetiradaEntregaId := vRetBanco.AsInt;

    if Sender = miGerarRetiradaProdutosSelecionados then begin
      vRetBanco :=
        _Orcamentos.GerarNotaRetiraAto(
          Sessao.getConexaoBanco,
          [vRetiradaEntregaId],
          False
        );

      Sessao.AbortarSeHouveErro(vRetBanco);
      _Biblioteca.AddNoVetorSemRepetir(vNotasIds, vRetBanco.AsInt);
    end;
  end;

  Sessao.getConexaoBanco.FinalizarTransacao;

  if (Sessao.getParametrosEmpresa.TrabalharControleSeparacao = 'S') and (Sender = miGerarEntregaProdutosSelecionados) then begin
    _Biblioteca.Informar('Entregas geradas com sucesso. Os documentos fiscais ser�o impressos ao gerar o controle de entrega.');

    {$REGION 'REALIZANDO A IMPRESS�O DE ENTREGAS QUE N�O TER�O SEPARA��O'}
      vEntregas :=
        _Entregas.BuscarEntregasComando(
          Sessao.getConexaoBanco,
          'where ENT.AGRUPADOR_ID = ' + IntToStr(vAgrupamentoId) +
          'and ENT.STATUS = ''AGC'' '
        );

      for i := Low(vEntregas) to High(vEntregas) do
        ImpressaoComprovanteEntregaGrafico.Imprimir( vEntregas[i].EntregaId, tpEntrega );
    {$ENDREGION}
  end
  else if Sender = miGerarEntregaProdutosSelecionados then begin
    _Biblioteca.Informar('Entregas geradas com sucesso. Os documentos fiscais ser�o impressos ao gerar o controle de entrega.');

    vEntregas := _Entregas.BuscarEntregasComando(Sessao.getConexaoBanco, ' where ENT.AGRUPADOR_ID = ' + IntToStr(vAgrupamentoId));
    if Sessao.getParametros.UtilizaControleManifesto = 'N' then begin
      for i := Low(vEntregas) to High(vEntregas) do begin
        vRecRetornoBD := _Entregas.GerarNotaEntrega(Sessao.getConexaoBanco, vEntregas[i].EntregaId, true);
        _Biblioteca.AddNoVetorSemRepetir(vEntregasIds, vEntregas[i].EntregaId);
      end;

      vNotas :=
        _NotasFiscais.BuscarNotasFiscaisComando(
          Sessao.getConexaoBanco,
          'where ' + _Biblioteca.FiltroInInt('NFI.ENTREGA_ID', vEntregasIds) +
          ' and NFI.TIPO_MOVIMENTO = ''VEN'' ' +
          ' and NFI.STATUS = ''N'' ' +
          ' and NFI.ENTREGA_ID in(select NFI2.ENTREGA_ID from ENTREGAS NFI2 where NFI2.TIPO_NOTA_GERAR <> ''NI'') '
        );

      for i := Low(vNotas) to High(vNotas) do begin
        if not _ComunicacaoNFE.EnviarNFe(vNotas[i].nota_fiscal_id) then begin
          _Biblioteca.Exclamar(
            'Ocorreu um problema ao tentar emitir a nota fiscal eletr�nica automaticamente!' + Chr(13) + Chr(10) +
            'Fa�a a emiss�o pela pela de "Rela��o de notas fiscais"'
          );
        end;
      end;
    end;

    for i := Low(vEntregas) to High(vEntregas) do
      ImpressaoComprovanteEntregaGrafico.Imprimir( vEntregas[i].EntregaId, tpEntrega );
  end
  else if Sender = miGerarRetiradaProdutosSelecionados then begin
    if vDocumentoEmitir.Dados.EmitirRecibo then begin
      vRetiradas := _Retiradas.BuscarRetiradasComando(Sessao.getConexaoBanco, ' where RET.AGRUPADOR_ID = ' + IntToStr(vAgrupamentoId));
      for i := Low(vRetiradas) to High(vRetiradas) do
        ImpressaoComprovanteEntregaGrafico.Imprimir( vRetiradas[i].RetiradaId, tpRetirada );
    end;

    // Se n�o foi selecionado nada para ser emitido
    if vDocumentoEmitir.Dados.NenhumDocumentoFiscalEmitir then
      MensagemPadraoNaoEmissaoNFe
    else begin
      for i := Low(vNotasIds) to High(vNotasIds) do begin
        if not _ComunicacaoNFE.EnviarNFe(vNotasIds[i]) then begin
          _Biblioteca.MensagemPadraoNaoEmissaoNFe;
          Break;
        end;
      end;
    end;
  end;

  Carregar(Sender);
end;

procedure TFormGerarEntregas.miLegendasClick(Sender: TObject);
var
  vLegendas: TFormLegendas;
begin
  inherited;
  vLegendas := TFormLegendas.Create(Self);

  vLegendas.AddLegenda(coCorRetornoParcial, 'Retorno parcial', clWhite);
  vLegendas.AddLegenda(coCorRetornoTotal, 'Retorno total', clWhite);

  vLegendas.Show;
end;

procedure TFormGerarEntregas.miRemoverQuantidadeTodosItensF8Click(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  for i := 1 to sgItens.RowCount -1 do begin
    sgItens.Cells[ciQtdeEntregar, i] := '';

    SalvarItemGerarEntrega(
      SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]),
      SFormatInt(sgItens.Cells[ciItemId, i]),
      sgItens.Cells[ciLote, i],
      0,
      0,
      SFormatInt(sgOrcamentos.Cells[coLocalId, sgOrcamentos.Row])
    );
  end;
end;

procedure TFormGerarEntregas.miTransformarEntregasSelecionadasSemPrevisaoClick(Sender: TObject);
type
  RecEntregasDesagendar = record
    EmpresaId: Integer;
    OrcamentoId: Integer;
    LocalId: Integer;
    PrevisaoEntrega: TDateTime;
  end;

var
  i: Integer;
  j: Integer;
  vRetBanco: RecRetornoBD;

  vEntregas: TArray<RecEntregasDesagendar>;
  vItens: TArray<RecEntregasItensPendentes>;

  procedure AdicionarVetorSemRepetir( pEmpresaId: Integer; pOrcamentoId: Integer; pPrevisaoEntrega: TDateTime; pLocalId: Integer );
  var
    i: Integer;
    vExiste: Boolean;
  begin
    vExiste := False;
    for i := Low(vEntregas) to High(vEntregas) do begin
      vExiste :=
        (vEntregas[i].EmpresaId = pEmpresaId) and
        (vEntregas[i].OrcamentoId = pOrcamentoId) and
        (vEntregas[i].PrevisaoEntrega = pPrevisaoEntrega) and
        (vEntregas[i].LocalId = pLocalId);

      if vExiste then
        Break;
    end;

    if not vExiste then begin
      SetLength(vEntregas, Length(vEntregas) + 1);

      vEntregas[High(vEntregas)].EmpresaId := pEmpresaId;
      vEntregas[High(vEntregas)].OrcamentoId := pOrcamentoId;
      vEntregas[High(vEntregas)].PrevisaoEntrega := pPrevisaoEntrega;
      vEntregas[High(vEntregas)].LocalId := pLocalId;
    end;
  end;

begin
  inherited;

  if not _Biblioteca.Perguntar('Deseja realmente desagendar as entregas selecionadas?') then
    Exit;

  vItens := nil;
  vEntregas := nil;
  (* Descobrindo quais pedidos tem entregas a serem realizadas *)
  for i := Low(FItens) to High(FItens) do begin
    if NFormatN(FItens[i].QtdeEntregar) = '' then
      Continue;

    AdicionarVetorSemRepetir(FItens[i].EmpresaId, FItens[i].OrcamentoId, FItens[i].PrevisaoEntrega, FItens[i].LocalId);
  end;

  if vEntregas = nil then begin
    Exclamar('N�o foi definido nenhum produto para gera��o de retirada!');
    Exit;
  end;

  for i := Low(vEntregas) to High(vEntregas) do begin
    if _Devolucoes.ExistemDevolucoesPendentes(Sessao.getConexaoBanco, vEntregas[i].OrcamentoId) then begin
      Exclamar('N�o � permitida esta opera��o enquanto houver pend�ncias de devolu��es para o pedido ' + NFormat(vEntregas[i].OrcamentoId) + '!');
      Abort;
    end;
  end;

  Sessao.getConexaoBanco.IniciarTransacao;
  for i := Low(vEntregas) to High(vEntregas) do begin
    vItens := nil;
    (* Montando os itens a serem entregues *)
    for j := Low(FItens) to High(FItens) do begin
      if
        (vEntregas[i].OrcamentoId <> FItens[j].OrcamentoId) or
        (vEntregas[i].EmpresaId <> FItens[j].EmpresaId) or
        (vEntregas[i].PrevisaoEntrega <> FItens[j].PrevisaoEntrega) or
        (vEntregas[i].LocalId <> FItens[j].LocalId)
      then
        Continue;

      if NFormatN(FItens[j].QtdeEntregar) = '' then
        Continue;

      SetLength(vItens, Length(vItens) + 1);

      vItens[High(vItens)].OrcamentoId  := FItens[j].OrcamentoId;
      vItens[High(vItens)].ProdutoId    := FItens[j].ProdutoId;
      vItens[High(vItens)].ItemId       := FItens[j].ItemId;
      vItens[High(vItens)].QtdeEntregar := FItens[j].QtdeEntregar;
      vItens[High(vItens)].Lote         := FItens[j].Lote;
    end;

    vRetBanco :=
      _EntregasPendentes.DesagendarEntregas(
        Sessao.getConexaoBanco,
        vEntregas[i].EmpresaId,
        vEntregas[i].OrcamentoId,
        vEntregas[i].PrevisaoEntrega,
        vEntregas[i].LocalId,
        vItens
      );

    if vRetBanco.TeveErro then begin
      Sessao.getConexaoBanco.VoltarTransacao;
      Exclamar(vRetBanco.MensagemErro);
      Exit;
    end;
  end;

  Sessao.getConexaoBanco.FinalizarTransacao;

  _Biblioteca.Informar('Entregas desagendadas com sucesso.');
  Carregar(Sender);
end;

procedure TFormGerarEntregas.sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
begin
  inherited;
  if ACol <> ciQtdeEntregar then
    Exit;

  if not ValidarMultiplo(SFormatDouble(TextCell), SFormatDouble(sgItens.Cells[ciMultiploVenda, ARow])) then begin
    TextCell := '';
    Exit;
  end;

  TextCell := NFormatNEstoque(SFormatDouble(TextCell));
  if SFormatCurr(TextCell) > SFormatCurr(sgItens.Cells[ciQuantidade, ARow]) then begin
    _Biblioteca.Exclamar('A quantidade a entregar n�o pode ser maior que a quantidade pendente!');
    TextCell := NFormatNEstoque(SFormatCurr(sgItens.Cells[ciQuantidade, ARow]));
    sgItens.SetFocus;
    Exit;
  end;

  sgItens.Cells[ciPesoTotalEntr, ARow] := NFormatNEstoque( SFormatDouble(TextCell) * SFormatDouble(sgItens.Cells[ciPesoUnitario, ARow]) );

  SalvarItemGerarEntrega(
    SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]),
    SFormatInt(sgItens.Cells[ciItemId, ARow]),
    sgItens.Cells[ciLote, ARow],
    SFormatDouble(TextCell),
    SFormatDouble(sgItens.Cells[ciPesoTotalEntr, ARow]),
    SFormatInt(sgOrcamentos.Cells[coLocalId, sgOrcamentos.Row])
  );
end;

procedure TFormGerarEntregas.SalvarItemGerarEntrega(
  pOrcamentoId: Integer;
  pItemId: Integer;
  pLote: string;
  pQuantidade: Double;
  pPesoTotal: Double;
  pLocalId: Integer
);
var
  i: Integer;
begin
  for i := Low(FItens) to High(FItens) do begin
    if (pOrcamentoId <> FItens[i].OrcamentoId) or (pItemId <> FItens[i].ItemId) or (pLote <> FItens[i].Lote) or (pLocalId <> FItens[i].LocalId) then
      Continue;

    FItens[i].QtdeEntregar := pQuantidade;
    FItens[i].PesoTotal    := pPesoTotal;
    Break;
  end;

  TotalizarProdutos;
end;

procedure TFormGerarEntregas.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[ciProdutoId, ciQuantidade, ciQtdeEntregar, ciPesoTotalEntr] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormGerarEntregas.sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;


  if ACol = ciQtdeEntregar then begin
    AFont.Color := coCorFonteEdicao1;
    ABrush.Color := coCorCelulaEdicao1;
  end;
end;

procedure TFormGerarEntregas.sgItensKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_F8 then
    miRemoverQuantidadeTodosItensF8Click(Sender)
  else if Key = VK_F6 then
    miDefinirQuantidadeTodosItensClick(Sender);
end;

procedure TFormGerarEntregas.sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  CanSelect := (ACol = ciQtdeEntregar);
  if ACol = ciQtdeEntregar then
    sgItens.Options := sgItens.Options + [goEditing]
  else
    sgItens.Options := sgItens.Options - [goEditing];
end;

procedure TFormGerarEntregas.sgOrcamentosClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vOrcamentoId: Integer;
  vEmpresaId: Integer;
  vLocalId: Integer;
  vPrevisaoEntrega: string;
begin
  inherited;
  vLinha := 0;
  sgItens.ClearGrid;

  meObservacoes.Lines.Text := (sgOrcamentos.Cells[coObservacoes, sgOrcamentos.Row]);

  vOrcamentoId     := SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]);
  vEmpresaId       := SFormatInt(sgOrcamentos.Cells[coEmpresaId, sgOrcamentos.Row]);
  vLocalId         := SFormatInt(sgOrcamentos.Cells[coLocalId, sgOrcamentos.Row]);
  vPrevisaoEntrega := sgOrcamentos.Cells[coDataHoraEntrega, sgOrcamentos.Row];
  for i := Low(FItens) to High(FItens) do begin
    if
      (vOrcamentoId <> FItens[i].OrcamentoId) or
      (vEmpresaId <> FItens[i].EmpresaId) or
      (vLocalId <> FItens[i].LocalId) or
      (vPrevisaoEntrega <> _Biblioteca.DHFormat(FItens[i].PrevisaoEntrega))
    then
      Continue;

    Inc(vLinha);

    sgItens.Cells[ciProdutoId, vLinha]     := NFormat(FItens[i].ProdutoId);
    sgItens.Cells[ciNome, vLinha]          := FItens[i].Nome;
    sgItens.Cells[ciMarca, vLinha]         := FItens[i].Marca;
    sgItens.Cells[ciQuantidade, vLinha]    := NFormatEstoque(FItens[i].Saldo);
    sgItens.Cells[ciUnidade, vLinha]       := FItens[i].Unidade;
    sgItens.Cells[ciLote, vLinha]          := FItens[i].Lote;
    sgItens.Cells[ciQtdeEntregar, vLinha]  := NFormatNEstoque(FItens[i].QtdeEntregar);
    sgItens.Cells[ciPesoTotalEntr, vLinha] := NFormatNEstoque(FItens[i].PesoTotal);
    sgItens.Cells[ciItemId, vLinha]        := NFormat(FItens[i].ItemId);
    sgItens.Cells[ciMultiploVenda, vLinha] := NFormatEstoque(FItens[i].MultiploVenda);
    sgItens.Cells[ciPesoUnitario, vLinha]  := NFormatEstoque(FItens[i].Peso);
  end;
  sgItens.RowCount := IfThen(vLinha > 1, vLinha + 1, 2);
  sgItens.Col := ciQtdeEntregar;
end;

procedure TFormGerarEntregas.sgOrcamentosDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar(SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]));
end;

procedure TFormGerarEntregas.sgOrcamentosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if sgOrcamentos.Cells[coTipoLinha, ARow] = clCabecalho then
    vAlinhamento := taLeftJustify
  else if ACol = coOrcamentoId then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  if sgOrcamentos.Cells[coTipoLinha, ARow] = clCabecalho then
    sgOrcamentos.MergeCells([ARow, ACol], [ARow, coOrcamentoId], [ARow, coDataHoraEntrega], vAlinhamento, Rect)
  else
    sgOrcamentos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormGerarEntregas.sgOrcamentosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgOrcamentos.Cells[coTipoLinha, ARow] = clCabecalho then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end
  else if ACol <> coDataHoraEntrega then
    Exit;

  if _Biblioteca.ToData(sgOrcamentos.Cells[coDataHoraEntrega, ARow]) < Date then
    AFont.Color := clRed
  else if _Biblioteca.ToData(sgOrcamentos.Cells[coDataHoraEntrega, ARow]) = Date  then
    AFont.Color := $000096DB
  else
    AFont.Color := clBlue;

end;

procedure TFormGerarEntregas.sgOrcamentosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgOrcamentos.Cells[coOrcamentoId, ARow] = '' then
    Exit;

  if ACol = coLegenda then begin
    APicture :=
      LengendaGrid(
        sgOrcamentos.Cells[coTipoRetornoEntrega, ARow] = 'RTO',
        sgOrcamentos.Cells[coTipoRetornoEntrega, ARow] = 'RPA'
      );
  end
end;

procedure TFormGerarEntregas.TotalizarProdutos;
var
  i: Integer;
  vTotalPeso: Double;
  vQtdeProdutos: Integer;
begin
  vTotalPeso := 0;
  vQtdeProdutos := 0;

  for i := Low(FItens) to High(FItens) do begin
    if NFormatN(FItens[i].QtdeEntregar) = '' then
      Continue;

    vTotalPeso := vTotalPeso + FItens[i].PesoTotal;
    vQtdeProdutos := vQtdeProdutos + 1;
  end;

  stPesoTotal.Caption := NFormatNEstoque( vTotalPeso );
  stQuantidadeProdutos.Caption := NFormatNEstoque( vQtdeProdutos );
end;

procedure TFormGerarEntregas.VerificarRegistro(Sender: TObject);
begin
  inherited;
  _Biblioteca.LimparCampos([stPesoTotal, stQuantidadeProdutos, meObservacoes]);
end;

end.
