unit LancamentoManualCaixaBanco;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.Buttons, Vcl.ExtCtrls,
  _HerancaCadastro, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, _Sessao, _Biblioteca,
  FrameContas, Vcl.StdCtrls, EditLuka, ComboBoxLuka, FramePlanosFinanceiros, _RecordsEspeciais,
  FrameCentroCustos, _MovimentosContas, Vcl.Mask, EditLukaData, CheckBoxLuka;

type
  TTipoLancamento = (tpNenhum, tpTransferencia, tpTarifa);

  TFormLancamentoManualCaixaBanco = class(TFormHerancaCadastroCodigo)
    FrConta: TFrContas;
    cbTipoMovimento: TComboBoxLuka;
    lbl7: TLabel;
    eValorMovimento: TEditLuka;
    lb17: TLabel;
    FrContaDestino: TFrContas;
    FrPlanoFinanceiro: TFrPlanosFinanceiros;
    FrCentroCusto: TFrCentroCustos;
    eDataHoraMovimento: TEditLukaData;
    lb1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FLancamentoViaExtratoConta: TTipoLancamento;
  protected
    procedure ExcluirRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure Modo(pEditando: Boolean); override;
    procedure PesquisarRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function LancarTarifaTransferencia(
  pValor: Currency;
  pDataTransferencia: TDateTime;
  pConta: string;
  pEntrada: Boolean;
  pLancamentoTarifa: Boolean
): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

{ TFormLancamentoManualCaixaBanco }

function LancarTarifaTransferencia(
  pValor: Currency;
  pDataTransferencia: TDateTime;
  pConta: string;
  pEntrada: Boolean;
  pLancamentoTarifa: Boolean
): TRetornoTelaFinalizar;
var
  vForm: TFormLancamentoManualCaixaBanco;
begin
  vForm := TFormLancamentoManualCaixaBanco.Create(Application);

  vForm.Modo(True);

  if pLancamentoTarifa then
    vForm.cbTipoMovimento.AsString      := 'SAM'
  else
    vForm.cbTipoMovimento.AsString      := 'TRA';

  vForm.eValorMovimento.AsCurr        := pValor;
  vForm.eDataHoraMovimento.AsDataHora := pDataTransferencia;

  if pEntrada then
    vForm.FrContaDestino.InserirDadoPorChave(pConta, False)
  else
    vForm.FrConta.InserirDadoPorChave(pConta, False);

  _Biblioteca.SomenteLeitura([vForm.cbTipoMovimento, vForm.eValorMovimento, vForm.eDataHoraMovimento], True);

  if not pLancamentoTarifa then begin
    vForm.FLancamentoViaExtratoConta := tpTransferencia;

    if not vForm.FrConta.EstaVazio then
      vForm.FrConta.SomenteLeitura(True);

    if not vForm.FrContaDestino.EstaVazio then
      vForm.FrContaDestino.SomenteLeitura(True);
  end
  else begin
    vForm.FLancamentoViaExtratoConta := tpTarifa;
    vForm.FrContaDestino.SomenteLeitura(True);
  end;

  vForm.AbrirModal;
end;

procedure TFormLancamentoManualCaixaBanco.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _MovimentosContas.ExcluirMovimento(Sessao.getConexaoBanco, eID.AsInt);
  Sessao.AbortarSeHouveErro(vRetBanco);

  inherited;
end;

procedure TFormLancamentoManualCaixaBanco.FormCreate(Sender: TObject);
begin
  inherited;
  FLancamentoViaExtratoConta := tpNenhum;
end;

procedure TFormLancamentoManualCaixaBanco.FormShow(Sender: TObject);
begin
  inherited;
  if FLancamentoViaExtratoConta = tpTransferencia then begin
    if not FrConta.EstaVazio then
      SetarFoco(FrContaDestino);

    if not FrContaDestino.EstaVazio then
      SetarFoco(FrConta);
  end
  else
    SetarFoco(FrCentroCusto);
end;

procedure TFormLancamentoManualCaixaBanco.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  vPlanoFinanceiroId: string;
  vCentroCustoId: Integer;
  vContaDestinoId: string;
begin
  inherited;

  vPlanoFinanceiroId := '';
  if not FrPlanoFinanceiro.EstaVazio then
    vPlanoFinanceiroId := FrPlanoFinanceiro.getDados.PlanoFinanceiroId;

  vCentroCustoId := 0;
  if not FrCentroCusto.EstaVazio then
    vCentroCustoId := FrCentroCusto.GetCentro().CentroCustoId;

  vContaDestinoId := '';
  if not FrContaDestino.EstaVazio then
    vContaDestinoId := FrContaDestino.GetConta().Conta;

  vRetBanco :=
    _MovimentosContas.AtualizarMovimentos(
      Sessao.getConexaoBanco,
      eID.AsInt,
      FrConta.GetConta().Conta,
      vContaDestinoId,
      eValorMovimento.AsDouble,
      eDataHoraMovimento.AsDataHora,
      cbTipoMovimento.GetValor,
      vPlanoFinanceiroId,
      vCentroCustoId,
      False
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormLancamentoManualCaixaBanco.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    cbTipoMovimento,
    eValorMovimento,
    eDataHoraMovimento,
    FrConta,
    FrContaDestino,
    FrPlanoFinanceiro,
    FrCentroCusto],
    pEditando
  );

  if pEditando then begin
    cbTipoMovimento.ItemIndex := 0;
    SetarFoco(cbTipoMovimento);
  end;
end;

procedure TFormLancamentoManualCaixaBanco.PesquisarRegistro;
begin
  inherited;

end;

procedure TFormLancamentoManualCaixaBanco.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if eValorMovimento.AsCurr = 0 then begin
    _Biblioteca.Exclamar('O valor do movimento a ser lan�ado n�o foi informado corretamente, verifique!');
    SetarFoco(eValorMovimento);
    Abort;
  end;

  if not eDataHoraMovimento.DataHoraOk then begin
    _Biblioteca.Exclamar('A data/hora do movimento n�o foi informado corretamente, verifique!');
    SetarFoco(eDataHoraMovimento);
    Abort;
  end;

  if FrConta.EstaVazio then begin
    _Biblioteca.Exclamar('A conta n�o foi informada corretamente, verifique!');
    SetarFoco(FrConta);
    Abort;
  end;

  if cbTipoMovimento.GetValor = 'TRA' then begin
    if FrContaDestino.EstaVazio then begin
      _Biblioteca.Exclamar('A conta de destino n�o foi informada corretamente, verifique!');
      SetarFoco(FrContaDestino);
      Abort;
    end;
  end;

  if FrCentroCusto.EstaVazio then begin
    _Biblioteca.Exclamar('O centro de custo n�o foi informado corretamente, verifique!');
    SetarFoco(FrCentroCusto);
    Abort;
  end;
end;

end.
