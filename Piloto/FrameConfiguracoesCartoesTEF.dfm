inherited FrConfiguracoesCartoesTEF: TFrConfiguracoesCartoesTEF
  Width = 560
  Height = 141
  ExplicitWidth = 560
  ExplicitHeight = 141
  object sgConfig: TGridLuka
    Left = 0
    Top = 0
    Width = 560
    Height = 141
    Align = alClient
    ColCount = 7
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 6
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
    TabOrder = 0
    OnDrawCell = sgConfigDrawCell
    OnSelectCell = sgConfigSelectCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Campo'
      '1'#186' texto a pocurar'
      'Posi'#231#227'o inicial'
      'Qtde.caract.copiar'
      '2'#186' texto a pocurar'
      'Posi'#231#227'o inicial'
      'Qtde.caract.copiar')
    HRow.Strings = (
      'Bandeira'
      'Rede'
      'Nr.cart'#227'o truncado'
      'NSU'
      'C'#243'd.autoriza'#231#227'o')
    OnGetCellColor = sgConfigGetCellColor
    Grid3D = False
    RealColCount = 7
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      100
      99
      71
      98
      97
      75
      98)
  end
end
