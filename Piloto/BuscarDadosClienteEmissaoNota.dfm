inherited FormBuscarDadosClienteEmissaoNota: TFormBuscarDadosClienteEmissaoNota
  Caption = 'Buscar dados cliente emiss'#227'o da nota'
  ClientHeight = 101
  ClientWidth = 388
  ExplicitWidth = 394
  ExplicitHeight = 130
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 64
    Width = 388
    ExplicitTop = 93
    ExplicitWidth = 335
    inherited sbFinalizar: TSpeedButton
      Left = 129
      Top = -1
      ExplicitLeft = 129
      ExplicitTop = -1
    end
  end
  inline FrCliente: TFrClientes
    Left = 3
    Top = 1
    Width = 380
    Height = 50
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 3
    ExplicitTop = 1
    ExplicitWidth = 380
    ExplicitHeight = 50
    inherited CkAspas: TCheckBox [0]
    end
    inherited CkFiltroDuplo: TCheckBox [1]
    end
    inherited CkPesquisaNumerica: TCheckBox [2]
      Checked = False
      State = cbUnchecked
    end
    inherited CkMultiSelecao: TCheckBox [3]
      Top = 28
      ExplicitTop = 28
    end
    inherited PnTitulos: TPanel [4]
      Width = 380
      ExplicitWidth = 326
      inherited lbNomePesquisa: TLabel
        Width = 39
        Caption = 'Cliente'
        ExplicitWidth = 39
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 275
        ExplicitLeft = 221
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel [5]
      Left = 348
      Height = 25
      Align = alNone
      ExplicitLeft = 348
      ExplicitHeight = 25
      inherited sbPesquisa: TSpeedButton
        Top = 1
        ExplicitTop = 1
      end
    end
    inherited ckSomenteAtivos: TCheckBox [6]
      Checked = True
      State = cbChecked
    end
    inherited sgPesquisa: TGridLuka [7]
      Width = 348
      Height = 33
      Align = alNone
      ExplicitWidth = 348
      ExplicitHeight = 33
    end
  end
end
