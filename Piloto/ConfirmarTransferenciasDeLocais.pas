unit ConfirmarTransferenciasDeLocais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatorios, Vcl.StdCtrls,
  StaticTextLuka, Vcl.Grids, GridLuka, EditLuka, _FrameHerancaPrincipal,
  FrameDataInicialFinal, CheckBoxLuka, Vcl.Buttons, Vcl.ExtCtrls,
  _TransferenciasLocaisItensTemp, _TransferenciasLocaisTemp, _Sessao, _Biblioteca;

type
  TFormConfirmarTransferenciasDeLocais = class(TFormHerancaRelatorios)
    Label1: TLabel;
    FrDataTransferencia: TFrDataInicialFinal;
    eTransferencia: TEditLuka;
    sgTransferencias: TGridLuka;
    StaticTextLuka1: TStaticTextLuka;
    sgItens: TGridLuka;
    StaticTextLuka2: TStaticTextLuka;
    miConfirmarDevolucao: TSpeedButton;
    procedure sgTransferenciasClick(Sender: TObject);
    procedure sgTransferenciasDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure miConfirmarDevolucaoClick(Sender: TObject);
  private
    FItens: TArray<RecTransferenciasLocaisItensTemp>;
  public
    procedure Carregar(Sender: TObject); override;
  end;

var
  FormConfirmarTransferenciasDeLocais: TFormConfirmarTransferenciasDeLocais;

implementation

{$R *.dfm}

uses _RecordsEspeciais;

const
  (* Capa *)
  coTransferencia    = 0;
  coEmpresa          = 1;
  coLocal            = 2;
  coDataHoraCadastro = 3;

  (* Itens *)
  ciProdutoId  = 0;
  ciNome       = 1;
  ciQuantidade = 2;
  ciLocal      = 3;

{ TFormConfirmarTransferenciasDeLocais }

procedure TFormConfirmarTransferenciasDeLocais.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vTransferencias: TArray<RecTransferenciasLocaisTemp>;
begin
  inherited;
  FItens := nil;

  sgItens.ClearGrid();
  sgTransferencias.ClearGrid();

  vSql := 'where TRA.STATUS = ''P'' ';

  if not FrDataTransferencia.NenhumaDataValida then
    vSql := vSql + ' and ' + FrDataTransferencia.getSqlFiltros('trunc(TRA.DATA_HORA_CADASTRO)');

  if eTransferencia.AsInt > 0 then
    vSql := vSql + ' and TRA.TRANSFERENCIA_LOCAL_ID = ' + IntToStr(eTransferencia.AsInt);

  vSql := vSql + ' order by TRA.TRANSFERENCIA_LOCAL_ID desc ';
  vTransferencias := _TransferenciasLocaisTemp.BuscarTransferenciasLocaisTempComando(Sessao.getConexaoBanco, vSql);
  if vTransferencias = nil then begin
    NenhumRegistro;
    Exit;
  end;

  for i := Low(vTransferencias) to High(vTransferencias) do begin
    sgTransferencias.Cells[coTransferencia, i + 1]    := NFormat(vTransferencias[i].TransferenciaLocalId);
    sgTransferencias.Cells[coEmpresa, i + 1]          := NFormat(vTransferencias[i].EmpresaId) + ' - ' + vTransferencias[i].NomeEmpresa;
    sgTransferencias.Cells[coLocal, i + 1]            := NFormat(vTransferencias[i].LocalOrigemId) + ' - ' + vTransferencias[i].NomeLocal;
    sgTransferencias.Cells[coDataHoraCadastro, i + 1] := DHFormat(vTransferencias[i].DataHoraCadastro);
  end;
  sgTransferencias.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vTransferencias) );

  SetarFoco(sgTransferencias);
  sgTransferenciasClick(nil);
end;

procedure TFormConfirmarTransferenciasDeLocais.miConfirmarDevolucaoClick(
  Sender: TObject);
var
  vTransferenciaId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vTransferenciaId := SFormatInt(sgTransferencias.Cells[coTransferencia, sgTransferencias.Row]);
  if vTransferenciaId = 0 then begin
    NenhumRegistro;
    Exit;
  end;

  vRetBanco :=
    _TransferenciasLocaisTemp.ConfirmarTransferenciaLocais(
      Sessao.getConexaoBanco,
      vTransferenciaId
    );

  if vRetBanco.TeveErro then begin
    Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  RotinaSucesso;
  Carregar(nil);
end;

procedure TFormConfirmarTransferenciasDeLocais.sgItensDrawCell(Sender: TObject;
  ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    ciProdutoId,
    ciQuantidade]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormConfirmarTransferenciasDeLocais.sgTransferenciasClick(
  Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vTransferenciaId: Integer;
begin
  inherited;
  sgItens.ClearGrid();

  vLinha := 0;
  vTransferenciaId := SFormatInt(sgTransferencias.Cells[coTransferencia, sgTransferencias.Row]);

  if vTransferenciaId = 0 then
    Exit;

  FItens := _TransferenciasLocaisItensTemp.BuscarTransferenciasLocaisItensTemp(Sessao.getConexaoBanco, 0, [vTransferenciaId]);

  for i := Low(FItens) to High(FItens) do begin
    Inc(vLinha);
    sgItens.Cells[ciProdutoId, vLinha]  := NFormat(FItens[i].ProdutoId);
    sgItens.Cells[ciNome, vLinha]       := FItens[i].NomeProduto;
    sgItens.Cells[ciQuantidade, vLinha] := NFormatEstoque(FItens[i].Quantidade);
    sgItens.Cells[ciLocal, vLinha] := NFormat(FItens[i].LocalDestinoId) + ' - ' + FItens[i].NomeLocal;
  end;
  sgItens.RowCount := IIfInt(vLinha > 1, vLinha + 1, 2);
end;

procedure TFormConfirmarTransferenciasDeLocais.sgTransferenciasDrawCell(
  Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    coTransferencia]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgTransferencias.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
