object FormImpressaoDanfe: TFormImpressaoDanfe
  Left = 0
  Top = 0
  Caption = 'FormImpressaoDanfe'
  ClientHeight = 749
  ClientWidth = 1234
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object qrDanfe: TQuickRep
    Left = 80
    Top = -70
    Width = 1111
    Height = 1572
    BeforePrint = qrDanfeBeforePrint
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE')
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    OnNeedData = qrDanfeNeedData
    Options = [FirstPageHeader, LastPageFooter]
    Page.Columns = 1
    Page.Orientation = poPortrait
    Page.PaperSize = A4
    Page.Continuous = False
    Page.Values = (
      50.000000000000000000
      2970.000000000000000000
      50.000000000000000000
      2100.000000000000000000
      50.000000000000000000
      50.000000000000000000
      0.000000000000000000)
    PrinterSettings.Copies = 1
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.UseStandardprinter = False
    PrinterSettings.UseCustomBinCode = True
    PrinterSettings.CustomBinCode = 15
    PrinterSettings.ExtendedDuplex = 1
    PrinterSettings.UseCustomPaperCode = False
    PrinterSettings.CustomPaperCode = 9
    PrinterSettings.PrintMetaFile = False
    PrinterSettings.MemoryLimit = 1000000
    PrinterSettings.PrintQuality = 0
    PrinterSettings.Collate = 0
    PrinterSettings.ColorOption = 2
    PrintIfEmpty = True
    ReportTitle = 'DANFE NFe'
    SnapToGrid = True
    Units = MM
    Zoom = 140
    PrevFormStyle = fsNormal
    PreviewInitialState = wsMaximized
    PrevShowThumbs = False
    PrevShowSearch = False
    PrevInitialZoom = qrZoom100
    PreviewDefaultSaveType = stPDF
    PreviewLeft = 0
    PreviewTop = 0
    object qrbRecibo: TQRBand
      Left = 26
      Top = 26
      Width = 1058
      Height = 82
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        154.970238095238100000
        1999.494047619048000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbPageHeader
      object QRPDFShape1: TQRPDFShape
        Left = 0
        Top = 0
        Width = 1058
        Height = 82
        Size.Values = (
          154.970238095238100000
          0.000000000000000000
          0.000000000000000000
          1999.494047619048000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        VertAdjust = 0
        ShapeType = qrsRectangle
      end
      object QRLabel10: TQRLabel
        Left = 10
        Top = 44
        Width = 134
        Height = 16
        Size.Values = (
          30.238095238095240000
          18.898809523809520000
          83.154761904761900000
          253.244047619047600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'DATA DE RECEBIMENTO'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRShape101: TQRShape
        Left = 156
        Top = 38
        Width = 1
        Height = 43
        Size.Values = (
          81.264880952380950000
          294.821428571428600000
          71.815476190476190000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qrlRecebemosDe1: TQRLabel
        Left = 6
        Top = 6
        Width = 617
        Height = 19
        Size.Values = (
          35.907738095238100000
          11.339285714285710000
          11.339285714285710000
          1166.056547619048000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 
          'RECEBEMOS DE %s OS PRODUTOS/SERVI'#199'OS CONSTANTES DA NOTA FISCAL I' +
          'NDICADO AO LADO'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrNumeroNotaDestacar: TQRLabel
        Left = 931
        Top = 34
        Width = 114
        Height = 22
        Size.Values = (
          41.577380952380950000
          1759.479166666667000000
          64.255952380952380000
          215.446428571428600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '000.000.000'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrSerieNotaDestacar: TQRLabel
        Left = 956
        Top = 54
        Width = 10
        Height = 23
        Size.Values = (
          43.467261904761900000
          1806.726190476190000000
          102.053571428571400000
          18.898809523809520000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = '1'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel14: TQRLabel
        Left = 898
        Top = 54
        Width = 49
        Height = 23
        Size.Values = (
          43.467261904761900000
          1697.113095238095000000
          102.053571428571400000
          92.604166666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'S'#201'RIE'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel13: TQRLabel
        Left = 898
        Top = 33
        Width = 20
        Height = 23
        Size.Values = (
          43.467261904761900000
          1697.113095238095000000
          62.366071428571430000
          37.797619047619050000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'N'#186
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRLabel12: TQRLabel
        Left = 940
        Top = 6
        Width = 53
        Height = 22
        Size.Values = (
          41.577380952380950000
          1776.488095238095000000
          11.339285714285710000
          100.163690476190500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'NF-e'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel11: TQRLabel
        Left = 168
        Top = 44
        Width = 262
        Height = 16
        Size.Values = (
          30.238095238095240000
          317.500000000000000000
          83.154761904761900000
          495.148809523809500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'IDENTIFICA'#199#195'O E ASSINATURA DO RECEBEDOR'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRPDFShape2: TQRPDFShape
        Left = 1
        Top = 35
        Width = 871
        Height = 2
        Size.Values = (
          3.779761904761905000
          1.889880952380952000
          66.145833333333320000
          1646.086309523810000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        VertAdjust = 0
        ShapeType = qrsHorLine
      end
      object QRPDFShape3: TQRPDFShape
        Left = 872
        Top = 2
        Width = 1
        Height = 81
        Size.Values = (
          153.080357142857100000
          1647.976190476190000000
          3.779761904761905000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        VertAdjust = 0
        ShapeType = qrsVertLine
      end
      object QRShape1: TQRShape
        Left = 442
        Top = 38
        Width = 1
        Height = 45
        Size.Values = (
          85.044642857142860000
          835.327380952381100000
          71.815476190476190000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qrNomeRazaoSocialDestcabecalho: TQRLabel
        Left = 454
        Top = 57
        Width = 398
        Height = 21
        Size.Values = (
          39.687500000000000000
          858.005952380952400000
          107.723214285714300000
          752.172619047618900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNomeRazaoSocialDestinatario'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel3: TQRLabel
        Left = 454
        Top = 40
        Width = 77
        Height = 14
        Size.Values = (
          26.458333333333330000
          858.005952380952400000
          75.595238095238110000
          145.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'DESTINAT'#193'RIO'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRShape2: TQRShape
        Left = 629
        Top = 2
        Width = 2
        Height = 36
        Size.Values = (
          68.035714285714290000
          1188.735119047619000000
          3.779761904761905000
          3.779761904761905000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRLabel7: TQRLabel
        Left = 640
        Top = 5
        Width = 90
        Height = 14
        Size.Values = (
          26.458333333333330000
          1209.523809523810000000
          9.449404761904762000
          170.089285714285700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'VALOR NOTA'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object qrlbl1: TQRLabel
        Left = 869
        Top = 218
        Width = 168
        Height = 21
        Size.Values = (
          39.687500000000000000
          1643.062500000000000000
          412.750000000000000000
          317.500000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrTotalNotaFiscal'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrTotalNotaFiscalcapa: TQRLabel
        Left = 641
        Top = 20
        Width = 132
        Height = 23
        Size.Values = (
          43.467261904761900000
          1211.413690476190000000
          37.797619047619050000
          249.464285714285700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        BiDiMode = bdRightToLeft
        ParentBiDiMode = False
        Caption = 'qrTotalNFcapa'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
    end
    object qrbDadosDanfe: TQRChildBand
      Left = 26
      Top = 108
      Width = 1058
      Height = 264
      AlignToBottom = False
      BeforePrint = qrbDadosDanfeBeforePrint
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        498.928571428571400000
        1999.494047619048000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      ParentBand = qrbRecibo
      PrintOrder = cboAfterParent
      object QRShape5: TQRShape
        Left = 1
        Top = 15
        Width = 439
        Height = 157
        Size.Values = (
          296.711309523809500000
          1.889880952380952000
          28.348214285714280000
          829.657738095238100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Style = bsClear
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape6: TQRShape
        Left = 594
        Top = 15
        Width = 464
        Height = 157
        Size.Values = (
          296.711309523809500000
          1122.589285714286000000
          28.348214285714290000
          876.904761904761900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Style = bsClear
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape7: TQRShape
        Left = 0
        Top = 173
        Width = 1058
        Height = 84
        Size.Values = (
          158.750000000000000000
          0.000000000000000000
          326.949404761904800000
          1999.494047619048000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Style = bsClear
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel17: TQRLabel
        Left = 469
        Top = 14
        Width = 78
        Height = 28
        Size.Values = (
          52.916666666666670000
          886.354166666666700000
          26.458333333333330000
          148.166666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = True
        Caption = 'DANFE'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 12
      end
      object QRShape8: TQRShape
        Left = 0
        Top = 218
        Width = 1058
        Height = 1
        Size.Values = (
          1.889880952380952000
          0.000000000000000000
          411.994047619047600000
          1999.494047619048000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape9: TQRShape
        Left = 309
        Top = 218
        Width = 1
        Height = 39
        Size.Values = (
          73.705357142857140000
          583.973214285714300000
          411.994047619047600000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape11: TQRShape
        Left = 592
        Top = 173
        Width = 1
        Height = 84
        Size.Values = (
          158.750000000000000000
          1118.809523809524000000
          326.949404761904800000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRLabel29: TQRLabel
        Left = 9
        Top = 180
        Width = 126
        Height = 14
        Size.Values = (
          26.458333333333330000
          17.008928571428570000
          340.178571428571400000
          238.125000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'NATUREZA DA OPERA'#199#195'O'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel30: TQRLabel
        Left = 9
        Top = 222
        Width = 108
        Height = 12
        Size.Values = (
          22.678571428571430000
          17.008928571428570000
          419.553571428571400000
          204.107142857142900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'INSCRI'#199#195'O ESTADUAL'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel31: TQRLabel
        Left = 315
        Top = 222
        Width = 195
        Height = 12
        Size.Values = (
          22.678571428571430000
          595.312500000000000000
          419.553571428571400000
          368.526785714285700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'INSC. ESTADUAL DO SUBST. TRIBUT'#193'RIO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel32: TQRLabel
        Left = 604
        Top = 222
        Width = 25
        Height = 12
        Size.Values = (
          22.678571428571430000
          1141.488095238095000000
          419.553571428571400000
          47.247023809523810000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'CNPJ'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object qrNaturezaOperacao: TQRLabel
        Left = 8
        Top = 193
        Width = 575
        Height = 21
        Size.Values = (
          39.687500000000000000
          15.119047619047620000
          364.747023809523800000
          1086.681547619048000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNaturezaOperacao'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrInscricaoEstadualEmitente: TQRLabel
        Left = 9
        Top = 235
        Width = 186
        Height = 21
        Size.Values = (
          39.687500000000000000
          17.008928571428570000
          444.122023809523800000
          351.517857142857100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'qrInscricaoEstadualEmitente'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrInscricaoEstadualSubstitutoEmitente: TQRLabel
        Left = 317
        Top = 235
        Width = 249
        Height = 21
        Size.Values = (
          39.687500000000000000
          599.092261904761900000
          444.122023809523800000
          470.580357142857100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'qrInscricaoEstadualSubstitutoEmitente'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrCNPJEmitente: TQRLabel
        Left = 607
        Top = 235
        Width = 111
        Height = 21
        Size.Values = (
          39.687500000000000000
          1147.157738095238000000
          444.122023809523800000
          209.776785714285700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'qrCNPJEmitente'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrLogoEmpresa: TQRImage
        Left = 6
        Top = 23
        Width = 124
        Height = 124
        Size.Values = (
          234.345238095238100000
          11.339285714285710000
          43.467261904761910000
          234.345238095238100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Stretch = True
      end
      object qriBarCode: TQRImage
        Left = 599
        Top = 21
        Width = 444
        Height = 48
        Size.Values = (
          89.958333333333320000
          1132.416666666667000000
          39.687500000000000000
          838.729166666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Center = True
      end
      object QRLabel2: TQRLabel
        Left = 602
        Top = 79
        Width = 91
        Height = 14
        Size.Values = (
          26.458333333333330000
          1137.708333333333000000
          149.300595238095200000
          171.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'CHAVE DE ACESSO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object qrChaveAcesso: TQRLabel
        Left = 602
        Top = 94
        Width = 441
        Height = 21
        Size.Values = (
          39.687500000000000000
          1137.708333333333000000
          177.270833333333300000
          833.437500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrChaveAcesso'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrlMsgAutorizado: TQRLabel
        Left = 602
        Top = 132
        Width = 438
        Height = 28
        Size.Values = (
          52.916666666666670000
          1137.708333333333000000
          248.708333333333300000
          828.145833333333200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 
          'Consulta de autenticidade no portal nacional da NF-e www.nfe.faz' +
          'enda.gov.br/portal ou no site da Sefaz Autorizadora'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrlDescricao: TQRLabel
        Left = 602
        Top = 180
        Width = 186
        Height = 14
        Size.Values = (
          26.458333333333330000
          1137.708333333333000000
          340.178571428571400000
          351.517857142857100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'PROTOCOLO DE AUTORIZA'#199#195'O DE USO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object qrProtocoloNFe: TQRLabel
        Left = 598
        Top = 193
        Width = 445
        Height = 21
        Size.Values = (
          39.687500000000000000
          1129.770833333333000000
          365.125000000000000000
          841.375000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrProtocoloNFe'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrSerieNota: TQRLabel
        Left = 508
        Top = 130
        Width = 27
        Height = 23
        Size.Values = (
          43.467261904761900000
          960.059523809523800000
          245.684523809523800000
          51.026785714285710000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = '000'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel22: TQRLabel
        Left = 446
        Top = 131
        Width = 45
        Height = 23
        Size.Values = (
          43.467261904761900000
          842.886904761904800000
          247.574404761904800000
          85.044642857142860000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'S'#201'RIE'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel18: TQRLabel
        Left = 460
        Top = 37
        Width = 88
        Height = 17
        Size.Values = (
          32.127976190476190000
          869.345238095238100000
          69.925595238095240000
          166.309523809523800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        Caption = 'DOC. AUXILIAR'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel19: TQRLabel
        Left = 462
        Top = 51
        Width = 97
        Height = 17
        Size.Values = (
          32.127976190476190000
          873.125000000000000000
          96.383928571428570000
          183.318452380952400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        Caption = 'DA NOTA FISCAL'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel20: TQRLabel
        Left = 446
        Top = 110
        Width = 18
        Height = 19
        Size.Values = (
          35.907738095238100000
          842.886904761904800000
          207.886904761904800000
          34.017857142857140000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'N'#186
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrNumeroNota: TQRLabel
        Left = 471
        Top = 112
        Width = 130
        Height = 21
        Size.Values = (
          39.687500000000000000
          890.133928571428600000
          211.666666666666700000
          245.684523809523800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '000.000.000'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel24: TQRLabel
        Left = 446
        Top = 150
        Width = 57
        Height = 23
        Size.Values = (
          43.467261904761900000
          842.886904761904800000
          283.482142857142900000
          107.723214285714300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'FOLHA'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel26: TQRLabel
        Left = 446
        Top = 93
        Width = 52
        Height = 19
        Size.Values = (
          35.907738095238100000
          842.886904761904800000
          175.758928571428600000
          98.273809523809520000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = '1 - SA'#205'DA'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel27: TQRLabel
        Left = 446
        Top = 79
        Width = 71
        Height = 17
        Size.Values = (
          32.127976190476190000
          842.886904761904800000
          149.300595238095200000
          134.181547619047600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = '0 - ENTRADA'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRShape102: TQRShape
        Left = 560
        Top = 78
        Width = 31
        Height = 28
        Size.Values = (
          52.916666666666670000
          1058.333333333333000000
          147.410714285714300000
          58.586309523809520000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Style = bsClear
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qrEntradaSaida: TQRLabel
        Left = 567
        Top = 81
        Width = 18
        Height = 21
        Size.Values = (
          39.687500000000000000
          1071.562500000000000000
          153.080357142857100000
          34.017857142857140000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrlTransModFrete'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRShape4: TQRShape
        Left = 595
        Top = 73
        Width = 463
        Height = 1
        Size.Values = (
          1.889880952380952000
          1124.479166666667000000
          137.961309523809500000
          875.014880952381100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape66: TQRShape
        Left = 595
        Top = 119
        Width = 463
        Height = 1
        Size.Values = (
          1.889880952380952000
          1124.479166666667000000
          224.895833333333300000
          875.014880952381100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object qriBarCodeContingencia: TQRImage
        Left = 599
        Top = 125
        Width = 444
        Height = 42
        Size.Values = (
          79.375000000000000000
          1132.416666666667000000
          235.479166666666700000
          838.729166666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Center = True
      end
      object QRShape69: TQRShape
        Left = 1
        Top = 3
        Width = 1057
        Height = 1
        Size.Values = (
          1.889880952380952000
          1.889880952380952000
          5.669642857142857000
          1997.604166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Pen.Style = psDot
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object qrRazaoSocialEmpresa: TQRLabel
        Left = 133
        Top = 20
        Width = 306
        Height = 20
        Size.Values = (
          37.797619047619050000
          251.354166666666700000
          37.797619047619050000
          578.303571428571400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Hiva varejo e atacado ltda'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrEnderecoEmpresa: TQRLabel
        Left = 132
        Top = 43
        Width = 307
        Height = 47
        Size.Values = (
          88.824404761904760000
          249.464285714285700000
          81.264880952380970000
          580.193452380952400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 
          'Rod BR-497 Nr KM 1+480 A; GALPAO: 01; ARMZ: 11,12,13 e 14; Jardi' +
          'm Europa'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrCidadeUF: TQRLabel
        Left = 132
        Top = 98
        Width = 305
        Height = 17
        Size.Values = (
          32.127976190476190000
          249.464285714285700000
          185.208333333333300000
          576.413690476190500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'UBERLANDIA - MG'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrTelefoneEmail: TQRLabel
        Left = 156
        Top = 131
        Width = 255
        Height = 36
        Size.Values = (
          68.035714285714290000
          294.821428571428600000
          247.574404761904800000
          481.919642857142900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Fone: - nfeb2w@b2winc.com Cep: 38414583'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrQtdePaginas: TQRLabel
        Left = 538
        Top = 150
        Width = 26
        Height = 22
        Size.Values = (
          41.577380952380950000
          1016.755952380952000000
          283.482142857142800000
          49.136904761904760000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '/02'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrNumeroFolha: TQRSysData
        Left = 508
        Top = 150
        Width = 28
        Height = 22
        Size.Values = (
          41.577380952380950000
          960.059523809523800000
          283.482142857142800000
          52.916666666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        Data = qrsPageNumber
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        OnPrint = qrNumeroFolhaPrint
        ParentFont = False
        Text = ''
        Transparent = True
        ExportAs = exptText
        FontSize = 8
      end
      object QRLabel6: TQRLabel
        Left = 472
        Top = 65
        Width = 76
        Height = 17
        Size.Values = (
          32.127976190476190000
          892.023809523809500000
          122.842261904761900000
          143.630952380952400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        Caption = 'ELETR'#212'NICA'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrSemValorFiscal: TQRLabel
        Left = 669
        Top = 27
        Width = 325
        Height = 38
        Size.Values = (
          71.815476190476190000
          1264.330357142857000000
          51.026785714285710000
          614.211309523809500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'SEM VALOR FISCAL'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -24
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 18
      end
    end
    object qrbEmitenteDestinatario: TQRChildBand
      Left = 26
      Top = 372
      Width = 1058
      Height = 408
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        771.071428571428600000
        1999.494047619048000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      ParentBand = qrbDadosDanfe
      PrintOrder = cboAfterParent
      object QRShape36: TQRShape
        Left = 0
        Top = 262
        Width = 1058
        Height = 127
        Size.Values = (
          240.014880952381000000
          0.000000000000000000
          495.148809523809500000
          1999.494047619048000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Style = bsClear
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel1: TQRLabel
        Left = 0
        Top = 0
        Width = 165
        Height = 19
        Size.Values = (
          35.907738095238100000
          0.000000000000000000
          0.000000000000000000
          311.830357142857100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'DESTINAT'#193'RIO / REMETENTE'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel5: TQRLabel
        Left = 0
        Top = 245
        Width = 270
        Height = 17
        Size.Values = (
          32.127976190476190000
          0.000000000000000000
          463.020833333333300000
          510.267857142857100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'TRANSPORTADOR / VOLUMES TRANSPORTADOS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel4: TQRLabel
        Left = 0
        Top = 142
        Width = 132
        Height = 19
        Size.Values = (
          35.907738095238100000
          0.000000000000000000
          268.363095238095200000
          249.464285714285700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'C'#193'LCULO DO IMPOSTO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRShape12: TQRShape
        Left = 0
        Top = 18
        Width = 837
        Height = 124
        Size.Values = (
          234.345238095238100000
          0.000000000000000000
          34.017857142857140000
          1581.830357142857000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Style = bsClear
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape15: TQRShape
        Left = 630
        Top = 18
        Width = 1
        Height = 42
        Size.Values = (
          79.375000000000000000
          1190.625000000000000000
          34.395833333333340000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape16: TQRShape
        Left = 518
        Top = 60
        Width = 1
        Height = 42
        Size.Values = (
          79.375000000000000000
          978.958333333333200000
          113.770833333333300000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape17: TQRShape
        Left = 725
        Top = 60
        Width = 1
        Height = 42
        Size.Values = (
          79.375000000000000000
          1370.541666666667000000
          113.770833333333300000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape18: TQRShape
        Left = 253
        Top = 102
        Width = 1
        Height = 41
        Size.Values = (
          76.729166666666680000
          478.895833333333400000
          193.145833333333300000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape19: TQRShape
        Left = 461
        Top = 102
        Width = 1
        Height = 41
        Size.Values = (
          76.729166666666680000
          870.479166666666800000
          193.145833333333300000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape20: TQRShape
        Left = 553
        Top = 102
        Width = 1
        Height = 41
        Size.Values = (
          76.729166666666680000
          1045.104166666667000000
          193.145833333333300000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape13: TQRShape
        Left = 0
        Top = 59
        Width = 837
        Height = 2
        Size.Values = (
          3.779761904761905000
          0.000000000000000000
          111.502976190476200000
          1581.830357142857000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape14: TQRShape
        Left = 0
        Top = 101
        Width = 837
        Height = 2
        Size.Values = (
          3.779761904761905000
          0.000000000000000000
          190.877976190476200000
          1581.830357142857000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape21: TQRShape
        Left = 851
        Top = 18
        Width = 207
        Height = 124
        Size.Values = (
          234.345238095238100000
          1608.288690476190000000
          34.017857142857140000
          391.205357142857100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Style = bsClear
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape22: TQRShape
        Left = 851
        Top = 59
        Width = 207
        Height = 1
        Size.Values = (
          1.889880952380952000
          1608.288690476190000000
          111.502976190476200000
          391.205357142857100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape23: TQRShape
        Left = 851
        Top = 101
        Width = 207
        Height = 1
        Size.Values = (
          1.889880952380952000
          1608.288690476190000000
          190.877976190476200000
          391.205357142857100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape25: TQRShape
        Left = 0
        Top = 155
        Width = 1058
        Height = 84
        Size.Values = (
          158.750000000000000000
          0.000000000000000000
          292.931547619047600000
          1999.494047619048000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Style = bsClear
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape26: TQRShape
        Left = 0
        Top = 202
        Width = 1058
        Height = 1
        Size.Values = (
          1.889880952380952000
          0.000000000000000000
          381.755952380952400000
          1999.494047619048000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape27: TQRShape
        Left = 266
        Top = 155
        Width = 1
        Height = 47
        Size.Values = (
          88.824404761904760000
          502.708333333333300000
          292.931547619047600000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape28: TQRShape
        Left = 148
        Top = 203
        Width = 1
        Height = 36
        Size.Values = (
          68.035714285714290000
          279.702380952381000000
          383.645833333333300000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape29: TQRShape
        Left = 458
        Top = 155
        Width = 1
        Height = 47
        Size.Values = (
          88.824404761904760000
          865.565476190476200000
          292.931547619047600000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape30: TQRShape
        Left = 678
        Top = 155
        Width = 1
        Height = 47
        Size.Values = (
          88.824404761904760000
          1281.339285714286000000
          292.931547619047600000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape31: TQRShape
        Left = 862
        Top = 155
        Width = 1
        Height = 47
        Size.Values = (
          88.824404761904760000
          1629.077380952381000000
          292.931547619047600000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape32: TQRShape
        Left = 301
        Top = 203
        Width = 1
        Height = 36
        Size.Values = (
          68.035714285714290000
          568.854166666666700000
          383.645833333333300000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape33: TQRShape
        Left = 428
        Top = 203
        Width = 1
        Height = 36
        Size.Values = (
          68.035714285714290000
          808.869047619047600000
          383.645833333333300000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape34: TQRShape
        Left = 594
        Top = 202
        Width = 1
        Height = 37
        Size.Values = (
          69.925595238095240000
          1122.589285714286000000
          381.755952380952400000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape35: TQRShape
        Left = 802
        Top = 202
        Width = 1
        Height = 37
        Size.Values = (
          69.925595238095240000
          1515.684523809524000000
          381.755952380952400000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape37: TQRShape
        Left = 1
        Top = 304
        Width = 1057
        Height = 1
        Size.Values = (
          1.889880952380952000
          1.889880952380952000
          574.523809523809500000
          1997.604166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape38: TQRShape
        Left = 1
        Top = 346
        Width = 1057
        Height = 1
        Size.Values = (
          1.889880952380952000
          1.889880952380952000
          653.898809523809500000
          1997.604166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape39: TQRShape
        Left = 431
        Top = 262
        Width = 1
        Height = 127
        Size.Values = (
          240.770833333333300000
          814.916666666666800000
          494.770833333333300000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape42: TQRShape
        Left = 560
        Top = 263
        Width = 1
        Height = 41
        Size.Values = (
          76.729166666666680000
          1058.333333333333000000
          497.416666666666700000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape43: TQRShape
        Left = 661
        Top = 263
        Width = 1
        Height = 41
        Size.Values = (
          76.729166666666680000
          1248.833333333333000000
          497.416666666666700000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape44: TQRShape
        Left = 798
        Top = 263
        Width = 1
        Height = 41
        Size.Values = (
          76.729166666666680000
          1508.125000000000000000
          497.416666666666700000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape45: TQRShape
        Left = 854
        Top = 263
        Width = 1
        Height = 41
        Size.Values = (
          76.729166666666680000
          1613.958333333333000000
          497.416666666666700000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape46: TQRShape
        Left = 749
        Top = 305
        Width = 1
        Height = 42
        Size.Values = (
          79.375000000000000000
          1415.520833333333000000
          576.791666666666800000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape47: TQRShape
        Left = 804
        Top = 304
        Width = 1
        Height = 43
        Size.Values = (
          82.020833333333320000
          1518.708333333333000000
          574.145833333333300000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape48: TQRShape
        Left = 127
        Top = 347
        Width = 1
        Height = 42
        Size.Values = (
          79.375000000000000000
          240.770833333333300000
          656.166666666666800000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape49: TQRShape
        Left = 294
        Top = 347
        Width = 1
        Height = 42
        Size.Values = (
          79.375000000000000000
          555.625000000000000000
          656.166666666666800000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape50: TQRShape
        Left = 588
        Top = 347
        Width = 1
        Height = 42
        Size.Values = (
          79.375000000000000000
          1111.250000000000000000
          656.166666666666800000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape51: TQRShape
        Left = 804
        Top = 346
        Width = 1
        Height = 43
        Size.Values = (
          82.020833333333320000
          1518.708333333333000000
          653.520833333333200000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRLabel34: TQRLabel
        Left = 5
        Top = 21
        Width = 109
        Height = 14
        Size.Values = (
          26.458333333333330000
          9.449404761904762000
          39.687500000000000000
          205.997023809523800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'NOME / RAZ'#195'O SOCIAL'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel35: TQRLabel
        Left = 635
        Top = 21
        Width = 52
        Height = 14
        Size.Values = (
          26.458333333333330000
          1200.074404761905000000
          39.687500000000000000
          98.273809523809520000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'CNPJ / CPF'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel36: TQRLabel
        Left = 858
        Top = 21
        Width = 90
        Height = 14
        Size.Values = (
          26.458333333333330000
          1621.517857142857000000
          39.687500000000000000
          170.089285714285700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'DATA DA EMISS'#195'O'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel37: TQRLabel
        Left = 5
        Top = 63
        Width = 53
        Height = 14
        Size.Values = (
          26.458333333333330000
          9.449404761904762000
          119.062500000000000000
          100.163690476190500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'ENDERE'#199'O'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel38: TQRLabel
        Left = 522
        Top = 63
        Width = 91
        Height = 14
        Size.Values = (
          26.458333333333330000
          986.517857142857100000
          119.062500000000000000
          171.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'BAIRRO / DISTRITO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel39: TQRLabel
        Left = 730
        Top = 63
        Width = 20
        Height = 14
        Size.Values = (
          26.458333333333330000
          1379.613095238095000000
          119.062500000000000000
          37.797619047619050000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'CEP'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel40: TQRLabel
        Left = 858
        Top = 63
        Width = 132
        Height = 14
        Size.Values = (
          26.458333333333330000
          1621.517857142857000000
          119.062500000000000000
          249.464285714285700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'DATA DA SAIDA / ENTRADA'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel41: TQRLabel
        Left = 5
        Top = 105
        Width = 52
        Height = 14
        Size.Values = (
          26.458333333333330000
          9.449404761904762000
          198.437500000000000000
          98.273809523809520000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'MUNIC'#205'PIO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel42: TQRLabel
        Left = 261
        Top = 105
        Width = 55
        Height = 14
        Size.Values = (
          26.458333333333330000
          493.258928571428600000
          198.437500000000000000
          103.943452380952400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'FONE / FAX'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel43: TQRLabel
        Left = 467
        Top = 105
        Width = 14
        Height = 14
        Size.Values = (
          26.458333333333330000
          882.574404761904800000
          198.437500000000000000
          26.458333333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'UF'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel44: TQRLabel
        Left = 560
        Top = 105
        Width = 108
        Height = 14
        Size.Values = (
          26.458333333333330000
          1058.333333333333000000
          198.437500000000000000
          204.107142857142900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'INSCRI'#199#195'O ESTADUAL'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel45: TQRLabel
        Left = 858
        Top = 105
        Width = 77
        Height = 14
        Size.Values = (
          26.458333333333330000
          1621.517857142857000000
          198.437500000000000000
          145.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'HORA DE SA'#205'DA'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel46: TQRLabel
        Left = 9
        Top = 161
        Width = 134
        Height = 14
        Size.Values = (
          26.458333333333330000
          17.008928571428570000
          304.270833333333300000
          253.244047619047600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'BASE DE C'#193'LCULO DO ICMS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel47: TQRLabel
        Left = 278
        Top = 161
        Width = 78
        Height = 14
        Size.Values = (
          26.458333333333330000
          525.386904761904800000
          304.270833333333300000
          147.410714285714300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'VALOR DO ICMS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel48: TQRLabel
        Left = 469
        Top = 161
        Width = 206
        Height = 14
        Size.Values = (
          26.458333333333330000
          886.354166666666700000
          304.270833333333300000
          389.315476190476200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'BASE DE C'#193'LCULO DE ICMS SUBSTITUI'#199#195'O'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel49: TQRLabel
        Left = 688
        Top = 161
        Width = 151
        Height = 14
        Size.Values = (
          26.458333333333330000
          1300.238095238095000000
          304.270833333333300000
          285.372023809523800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'VALOR DO ICMS SUBSTITUI'#199#195'O'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel50: TQRLabel
        Left = 870
        Top = 161
        Width = 146
        Height = 14
        Size.Values = (
          26.458333333333330000
          1644.196428571429000000
          304.270833333333300000
          275.922619047619000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'VALOR TOTAL DOS PRODUTOS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel51: TQRLabel
        Left = 9
        Top = 203
        Width = 84
        Height = 14
        Size.Values = (
          26.458333333333330000
          17.008928571428570000
          383.645833333333300000
          158.750000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'VALOR DO FRETE'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel52: TQRLabel
        Left = 156
        Top = 203
        Width = 94
        Height = 14
        Size.Values = (
          26.458333333333330000
          294.821428571428600000
          383.645833333333300000
          177.648809523809500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'VALOR DO SEGURO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel53: TQRLabel
        Left = 308
        Top = 203
        Width = 53
        Height = 14
        Size.Values = (
          26.458333333333330000
          582.083333333333300000
          383.645833333333300000
          100.163690476190500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'DESCONTO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel54: TQRLabel
        Left = 434
        Top = 203
        Width = 154
        Height = 12
        Size.Values = (
          22.678571428571430000
          820.208333333333300000
          383.645833333333300000
          291.041666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'OUTRAS DESPESAS ACESS'#211'RIAS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel55: TQRLabel
        Left = 604
        Top = 203
        Width = 66
        Height = 14
        Size.Values = (
          26.458333333333330000
          1141.488095238095000000
          383.645833333333300000
          124.732142857142900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'VALOR DO IPI'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel56: TQRLabel
        Left = 810
        Top = 203
        Width = 115
        Height = 14
        Size.Values = (
          26.458333333333330000
          1530.803571428571000000
          383.645833333333300000
          217.336309523809500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'VALOR TOTAL DA NOTA'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel57: TQRLabel
        Left = 5
        Top = 264
        Width = 109
        Height = 14
        Size.Values = (
          26.458333333333330000
          9.449404761904762000
          498.928571428571400000
          205.997023809523800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'NOME / RAZ'#195'O SOCIAL'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel58: TQRLabel
        Left = 435
        Top = 265
        Width = 104
        Height = 13
        Size.Values = (
          23.812500000000000000
          822.854166666666800000
          500.062500000000000000
          195.791666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'FRETE POR CONTA'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel59: TQRLabel
        Left = 569
        Top = 264
        Width = 67
        Height = 14
        Size.Values = (
          26.458333333333330000
          1075.342261904762000000
          498.928571428571400000
          126.622023809523800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'C'#211'DIGO ANTT'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel60: TQRLabel
        Left = 667
        Top = 264
        Width = 95
        Height = 14
        Size.Values = (
          26.458333333333330000
          1260.550595238095000000
          498.928571428571400000
          179.538690476190500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'PLACA DO VE'#205'CULO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel61: TQRLabel
        Left = 807
        Top = 264
        Width = 14
        Height = 14
        Size.Values = (
          26.458333333333330000
          1525.133928571429000000
          498.928571428571400000
          26.458333333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'UF'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel62: TQRLabel
        Left = 859
        Top = 264
        Width = 52
        Height = 14
        Size.Values = (
          26.458333333333330000
          1623.407738095238000000
          498.928571428571400000
          98.273809523809520000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'CNPJ / CPF'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel63: TQRLabel
        Left = 5
        Top = 306
        Width = 53
        Height = 14
        Size.Values = (
          26.458333333333330000
          9.449404761904762000
          578.303571428571400000
          100.163690476190500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'ENDERE'#199'O'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel64: TQRLabel
        Left = 439
        Top = 306
        Width = 52
        Height = 14
        Size.Values = (
          26.458333333333330000
          829.657738095238100000
          578.303571428571400000
          98.273809523809520000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'MUNIC'#205'PIO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel65: TQRLabel
        Left = 756
        Top = 306
        Width = 14
        Height = 14
        Size.Values = (
          26.458333333333330000
          1428.750000000000000000
          578.303571428571400000
          26.458333333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'UF'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel66: TQRLabel
        Left = 812
        Top = 306
        Width = 108
        Height = 14
        Size.Values = (
          26.458333333333330000
          1534.583333333333000000
          578.303571428571400000
          204.107142857142900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'INSCRI'#199#195'O ESTADUAL'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel67: TQRLabel
        Left = 5
        Top = 348
        Width = 64
        Height = 14
        Size.Values = (
          26.458333333333330000
          9.449404761904762000
          657.678571428571400000
          120.952380952381000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'QUANTIDADE'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel68: TQRLabel
        Left = 133
        Top = 348
        Width = 39
        Height = 14
        Size.Values = (
          26.458333333333330000
          251.354166666666700000
          657.678571428571400000
          73.705357142857140000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'ESP'#201'CIE'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel69: TQRLabel
        Left = 301
        Top = 348
        Width = 38
        Height = 14
        Size.Values = (
          26.458333333333330000
          568.854166666666700000
          657.678571428571400000
          71.815476190476190000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'MARCA'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel70: TQRLabel
        Left = 441
        Top = 348
        Width = 64
        Height = 14
        Size.Values = (
          26.458333333333330000
          833.437500000000000000
          657.678571428571400000
          120.952380952381000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'NUMERA'#199#195'O'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel71: TQRLabel
        Left = 595
        Top = 348
        Width = 62
        Height = 14
        Size.Values = (
          26.458333333333330000
          1124.479166666667000000
          657.678571428571400000
          117.172619047619000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'PESO BRUTO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel72: TQRLabel
        Left = 810
        Top = 348
        Width = 67
        Height = 14
        Size.Values = (
          26.458333333333330000
          1530.803571428571000000
          657.678571428571400000
          126.622023809523800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'PESO L'#205'QUIDO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object qrNomeRazaoSocialDestinatario: TQRLabel
        Left = 7
        Top = 36
        Width = 617
        Height = 21
        Size.Values = (
          39.687500000000000000
          13.229166666666670000
          68.035714285714290000
          1166.056547619048000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNomeRazaoSocialDestinatario'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrCPFCNPJDestinatario: TQRLabel
        Left = 644
        Top = 35
        Width = 158
        Height = 21
        Size.Values = (
          39.687500000000000000
          1217.083333333333000000
          66.145833333333330000
          298.601190476190500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'qrCPFCNPJDestinatario'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrEnderecoDestinatario: TQRLabel
        Left = 8
        Top = 76
        Width = 507
        Height = 21
        Size.Values = (
          39.687500000000000000
          15.875000000000000000
          142.875000000000000000
          957.791666666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrEnderecoDestinatario'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrBairroDestinatario: TQRLabel
        Left = 521
        Top = 76
        Width = 201
        Height = 21
        Size.Values = (
          39.687500000000000000
          984.627976190476200000
          143.630952380952400000
          379.866071428571400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrBairroDestinatario'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrCEPDestinatario: TQRLabel
        Left = 731
        Top = 76
        Width = 94
        Height = 21
        Size.Values = (
          39.687500000000000000
          1381.712962962963000000
          144.050925925926000000
          177.858796296296300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrCEPDestinatario'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrCidadeDestinatario: TQRLabel
        Left = 3
        Top = 119
        Width = 247
        Height = 22
        Size.Values = (
          41.577380952380950000
          5.669642857142857000
          224.895833333333300000
          466.800595238095200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrCidadeDestinatario'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrTelefoneDestinatario: TQRLabel
        Left = 261
        Top = 119
        Width = 153
        Height = 21
        Size.Values = (
          39.687500000000000000
          493.258928571428600000
          224.895833333333300000
          289.151785714285700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'qrTelefoneDestinatario'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrUFDestinatario: TQRLabel
        Left = 468
        Top = 119
        Width = 81
        Height = 21
        Size.Values = (
          39.687500000000000000
          884.464285714285700000
          224.895833333333300000
          153.080357142857100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrUFDestinatario'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrInscricaoEstadualDestinatario: TQRLabel
        Left = 560
        Top = 119
        Width = 206
        Height = 21
        Size.Values = (
          39.687500000000000000
          1058.333333333333000000
          224.895833333333300000
          389.315476190476200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'qrInscricaoEstadualDestinatario'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrDataEmissao: TQRLabel
        Left = 868
        Top = 35
        Width = 179
        Height = 21
        Size.Values = (
          39.687500000000000000
          1640.416666666667000000
          66.145833333333320000
          338.288690476190500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrDataEmissao'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrDataSaidaEntrada: TQRLabel
        Left = 868
        Top = 76
        Width = 179
        Height = 21
        Size.Values = (
          39.687500000000000000
          1640.416666666667000000
          143.630952380952400000
          338.288690476190500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrDataSaidaEntrada'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrBaseICMS: TQRLabel
        Left = 118
        Top = 174
        Width = 140
        Height = 21
        Size.Values = (
          39.687500000000000000
          223.005952380952400000
          328.839285714285700000
          264.583333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrBaseICMS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrValorICMS: TQRLabel
        Left = 331
        Top = 174
        Width = 122
        Height = 21
        Size.Values = (
          39.687500000000000000
          625.550595238095200000
          328.839285714285700000
          230.565476190476200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrValorICMS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrBaseICMSST: TQRLabel
        Left = 541
        Top = 174
        Width = 130
        Height = 21
        Size.Values = (
          39.687500000000000000
          1022.425595238095000000
          328.839285714285700000
          245.684523809523800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrBaseICMSST'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrValorICMSST: TQRLabel
        Left = 707
        Top = 174
        Width = 150
        Height = 21
        Size.Values = (
          39.687500000000000000
          1336.145833333333000000
          328.839285714285700000
          283.482142857142900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrValorICMSST'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrTotalProdutos: TQRLabel
        Left = 869
        Top = 174
        Width = 168
        Height = 21
        Size.Values = (
          39.687500000000000000
          1643.062500000000000000
          328.083333333333300000
          317.500000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrTotalProdutos'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrValorFrete: TQRLabel
        Left = 18
        Top = 218
        Width = 122
        Height = 21
        Size.Values = (
          39.687500000000000000
          34.395833333333330000
          412.750000000000000000
          230.187500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrValorFrete'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrValorSeguro: TQRLabel
        Left = 158
        Top = 218
        Width = 130
        Height = 21
        Size.Values = (
          39.687500000000000000
          298.979166666666700000
          412.750000000000000000
          246.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrValorSeguro'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrValorDesconto: TQRLabel
        Left = 305
        Top = 218
        Width = 112
        Height = 21
        Size.Values = (
          39.687500000000000000
          576.791666666666800000
          412.750000000000000000
          211.666666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrValorDesconto'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrOutrasDespesas: TQRLabel
        Left = 449
        Top = 218
        Width = 130
        Height = 21
        Size.Values = (
          39.687500000000000000
          849.312500000000000000
          412.750000000000000000
          246.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrOutrasDespesas'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrValorIPI: TQRLabel
        Left = 644
        Top = 218
        Width = 150
        Height = 21
        Size.Values = (
          39.687500000000000000
          1217.083333333333000000
          412.750000000000000000
          283.104166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrValorIPI'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrTotalNotaFiscal: TQRLabel
        Left = 869
        Top = 218
        Width = 168
        Height = 21
        Size.Values = (
          39.687500000000000000
          1642.306547619048000000
          411.994047619047600000
          317.500000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrTotalNotaFiscal'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrNomeTransportadora: TQRLabel
        Left = 7
        Top = 277
        Width = 419
        Height = 21
        Size.Values = (
          39.687500000000000000
          13.229166666666670000
          523.875000000000000000
          791.104166666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNomeTransportadora'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrCodigoANTT: TQRLabel
        Left = 566
        Top = 280
        Width = 94
        Height = 21
        Size.Values = (
          39.687500000000000000
          1068.916666666667000000
          529.166666666666700000
          177.270833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrCodigoANTT'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrPlacaVeiculo: TQRLabel
        Left = 667
        Top = 280
        Width = 97
        Height = 21
        Size.Values = (
          39.687500000000000000
          1260.550595238095000000
          529.166666666666700000
          183.318452380952400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'qrPlacaVeiculo'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrUFVeiculo: TQRLabel
        Left = 806
        Top = 280
        Width = 38
        Height = 21
        Size.Values = (
          39.687500000000000000
          1524.000000000000000000
          529.166666666666700000
          71.437500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrUFVeiculo'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrCPFCNPJTransportadora: TQRLabel
        Left = 859
        Top = 280
        Width = 179
        Height = 21
        Size.Values = (
          39.687500000000000000
          1623.407738095238000000
          529.166666666666700000
          338.288690476190500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'qrCPFCNPJTransportadora'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrEnderecoTransportadora: TQRLabel
        Left = 8
        Top = 322
        Width = 417
        Height = 21
        Size.Values = (
          39.687500000000000000
          15.875000000000000000
          608.541666666666800000
          788.458333333333200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrEnderecoTransportadora'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrCidadeTransportadora: TQRLabel
        Left = 440
        Top = 322
        Width = 305
        Height = 21
        Size.Values = (
          39.687500000000000000
          830.791666666666800000
          608.541666666666800000
          576.791666666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrCidadeTransportadora'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrUFTransportadora: TQRLabel
        Left = 756
        Top = 322
        Width = 38
        Height = 21
        Size.Values = (
          39.687500000000000000
          1428.750000000000000000
          608.541666666666800000
          71.437500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrUFTransportadora'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrInscricaoEstadualTransp: TQRLabel
        Left = 812
        Top = 322
        Width = 172
        Height = 21
        Size.Values = (
          39.687500000000000000
          1534.583333333333000000
          608.541666666666700000
          325.059523809523800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'qrInscricaoEstadualTransp'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrQuantidadeTransp: TQRLabel
        Left = 10
        Top = 364
        Width = 102
        Height = 21
        Size.Values = (
          39.687500000000000000
          18.520833333333330000
          687.916666666666800000
          193.145833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrQuantidadeTransp'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrEspecieTransportadora: TQRLabel
        Left = 130
        Top = 364
        Width = 158
        Height = 21
        Size.Values = (
          39.687500000000000000
          246.062500000000000000
          687.916666666666800000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrEspecieTransportadora'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrMarcaTransportadora: TQRLabel
        Left = 298
        Top = 364
        Width = 126
        Height = 21
        Size.Values = (
          39.687500000000000000
          563.562500000000000000
          687.916666666666800000
          238.125000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrMarcaTransportadora'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrNumeracaoTransportadora: TQRLabel
        Left = 438
        Top = 364
        Width = 140
        Height = 21
        Size.Values = (
          39.687500000000000000
          827.767857142857100000
          687.916666666666800000
          264.583333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNumeracaoTransportadora'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrPesoBrutoTransp: TQRLabel
        Left = 595
        Top = 364
        Width = 127
        Height = 21
        Size.Values = (
          39.687500000000000000
          1124.479166666667000000
          687.916666666666700000
          240.014880952381000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'qrPesoBrutoTransp'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrPesoLiquidoTransp: TQRLabel
        Left = 810
        Top = 364
        Width = 139
        Height = 21
        Size.Values = (
          39.687500000000000000
          1530.803571428571000000
          687.916666666666700000
          262.693452380952400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'qrPesoLiquidoTransp'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrHoraSaida: TQRLabel
        Left = 868
        Top = 119
        Width = 179
        Height = 21
        Size.Values = (
          39.687500000000000000
          1640.416666666667000000
          224.895833333333300000
          338.288690476190500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrHoraSaida'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr6: TQRLabel
        Left = 0
        Top = 394
        Width = 183
        Height = 17
        Size.Values = (
          32.127976190476190000
          0.000000000000000000
          744.613095238095200000
          345.848214285714300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'DADOS DO PRODUTO / SERVI'#199'OS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRShape68: TQRShape
        Left = 519
        Top = 278
        Width = 19
        Height = 20
        Size.Values = (
          37.797619047619050000
          980.848214285714300000
          525.386904761904800000
          35.907738095238100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qr1: TQRLabel
        Left = 436
        Top = 278
        Width = 57
        Height = 12
        Size.Values = (
          22.678571428571430000
          823.988095238095200000
          525.386904761904800000
          107.723214285714300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = '1-EMITENTE'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object qr2: TQRLabel
        Left = 436
        Top = 289
        Width = 80
        Height = 12
        Size.Values = (
          22.678571428571430000
          823.988095238095200000
          546.175595238095200000
          151.190476190476200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = '2-DESTINAT'#193'RIO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object qr3: TQRLabel
        Left = 524
        Top = 280
        Width = 8
        Height = 18
        Size.Values = (
          34.395833333333340000
          989.541666666666800000
          529.166666666666700000
          15.875000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '1'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
    end
    object qrbHeaderItens: TQRBand
      Left = 26
      Top = 780
      Width = 1058
      Height = 25
      Frame.DrawTop = True
      Frame.DrawBottom = True
      Frame.DrawLeft = True
      Frame.DrawRight = True
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        47.247023809523810000
        1999.494047619048000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbColumnHeader
      object QRLabel142: TQRLabel
        Left = 14
        Top = 9
        Width = 57
        Height = 14
        Size.Values = (
          26.458333333333330000
          26.458333333333330000
          17.008928571428570000
          107.723214285714300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'C'#211'D. PROD.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel143: TQRLabel
        Left = 124
        Top = 9
        Width = 179
        Height = 14
        Size.Values = (
          26.458333333333330000
          234.345238095238100000
          17.008928571428570000
          338.288690476190500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'DESCRI'#199#195'O DO PRODUTO / SERVI'#199'OS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel144: TQRLabel
        Left = 355
        Top = 9
        Width = 45
        Height = 14
        Size.Values = (
          26.458333333333330000
          670.907738095238100000
          17.008928571428570000
          85.044642857142860000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'NCM / SH'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object lblCST: TQRLabel
        Left = 410
        Top = 10
        Width = 36
        Height = 14
        Size.Values = (
          26.458333333333330000
          774.851190476190500000
          18.898809523809520000
          68.035714285714290000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'CSOSN'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel146: TQRLabel
        Left = 453
        Top = 9
        Width = 27
        Height = 14
        Size.Values = (
          26.458333333333330000
          856.116071428571400000
          17.008928571428570000
          51.026785714285710000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'CFOP'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel147: TQRLabel
        Left = 490
        Top = 9
        Width = 28
        Height = 14
        Size.Values = (
          26.458333333333330000
          926.041666666666700000
          17.008928571428570000
          52.916666666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'UNID.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel148: TQRLabel
        Left = 527
        Top = 9
        Width = 64
        Height = 14
        Size.Values = (
          26.458333333333330000
          995.967261904761900000
          17.008928571428570000
          120.952380952381000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'QUANTIDADE'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel149: TQRLabel
        Left = 602
        Top = 9
        Width = 60
        Height = 14
        Size.Values = (
          26.458333333333330000
          1137.708333333333000000
          17.008928571428570000
          113.392857142857100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'V. UNIT'#193'RIO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel150: TQRLabel
        Left = 709
        Top = 9
        Width = 45
        Height = 14
        Size.Values = (
          26.458333333333330000
          1339.925595238095000000
          17.008928571428570000
          85.044642857142860000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'V. TOTAL'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel151: TQRLabel
        Left = 800
        Top = 9
        Width = 42
        Height = 14
        Size.Values = (
          26.458333333333330000
          1511.904761904762000000
          17.008928571428570000
          79.375000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'BC ICMS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel152: TQRLabel
        Left = 863
        Top = 9
        Width = 38
        Height = 14
        Size.Values = (
          26.458333333333330000
          1630.967261904762000000
          17.008928571428570000
          71.815476190476190000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'V. ICMS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel153: TQRLabel
        Left = 919
        Top = 9
        Width = 25
        Height = 14
        Size.Values = (
          26.458333333333330000
          1736.800595238095000000
          17.008928571428570000
          47.247023809523810000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'V. IPI'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel156: TQRLabel
        Left = 966
        Top = 10
        Width = 25
        Height = 12
        Size.Values = (
          22.678571428571430000
          1825.625000000000000000
          18.898809523809520000
          47.247023809523810000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'ICMS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel154: TQRLabel
        Left = 969
        Top = -1
        Width = 70
        Height = 13
        Size.Values = (
          24.568452380952380000
          1831.294642857143000000
          -1.889880952380952000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'AL'#237'QUOTAS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel157: TQRLabel
        Left = 1020
        Top = 10
        Width = 13
        Height = 12
        Size.Values = (
          22.678571428571430000
          1927.678571428571000000
          18.898809523809520000
          24.568452380952380000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'IPI'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRShape10: TQRShape
        Left = 85
        Top = 0
        Width = 1
        Height = 32
        Size.Values = (
          59.531250000000000000
          160.954861111111100000
          0.000000000000000000
          2.204861111111111000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape24: TQRShape
        Left = 346
        Top = 0
        Width = 1
        Height = 32
        Size.Values = (
          59.531250000000000000
          653.741319444444400000
          0.000000000000000000
          2.204861111111111000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape40: TQRShape
        Left = 410
        Top = 0
        Width = 1
        Height = 32
        Size.Values = (
          59.531250000000000000
          775.008680555555600000
          0.000000000000000000
          2.204861111111111000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape41: TQRShape
        Left = 447
        Top = 0
        Width = 1
        Height = 32
        Size.Values = (
          59.531250000000000000
          844.461805555555600000
          0.000000000000000000
          2.204861111111111000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape57: TQRShape
        Left = 484
        Top = 0
        Width = 1
        Height = 32
        Size.Values = (
          59.531250000000000000
          915.017361111111000000
          0.000000000000000000
          2.204861111111111000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape58: TQRShape
        Left = 526
        Top = 0
        Width = 1
        Height = 32
        Size.Values = (
          59.531250000000000000
          994.392361111111000000
          0.000000000000000000
          2.204861111111111000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape59: TQRShape
        Left = 594
        Top = 0
        Width = 1
        Height = 32
        Size.Values = (
          59.531250000000000000
          1122.274305555556000000
          0.000000000000000000
          2.204861111111111000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape60: TQRShape
        Left = 669
        Top = 0
        Width = 1
        Height = 32
        Size.Values = (
          59.531250000000000000
          1264.487847222222000000
          0.000000000000000000
          2.204861111111111000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape61: TQRShape
        Left = 759
        Top = 0
        Width = 1
        Height = 32
        Size.Values = (
          59.531250000000000000
          1434.262152777778000000
          0.000000000000000000
          2.204861111111111000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape62: TQRShape
        Left = 846
        Top = 0
        Width = 1
        Height = 32
        Size.Values = (
          59.531250000000000000
          1598.524305555556000000
          0.000000000000000000
          2.204861111111111000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape63: TQRShape
        Left = 907
        Top = 0
        Width = 1
        Height = 32
        Size.Values = (
          59.531250000000000000
          1714.279513888889000000
          0.000000000000000000
          2.204861111111111000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape64: TQRShape
        Left = 959
        Top = 0
        Width = 1
        Height = 32
        Size.Values = (
          59.531250000000000000
          1812.395833333333000000
          0.000000000000000000
          2.204861111111111000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape65: TQRShape
        Left = 1004
        Top = 8
        Width = 3
        Height = 16
        Size.Values = (
          30.238095238095240000
          1897.440476190476000000
          15.119047619047620000
          5.669642857142857000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape67: TQRShape
        Left = 959
        Top = 10
        Width = 99
        Height = 1
        Size.Values = (
          1.889880952380952000
          1812.395833333333000000
          18.898809523809520000
          187.098214285714300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
    end
    object qrbISSQN: TQRBand
      Left = 26
      Top = 821
      Width = 1058
      Height = 284
      AlignToBottom = True
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        536.726190476190500000
        1999.494047619048000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbPageFooter
      object QRShape56: TQRShape
        Left = 0
        Top = 85
        Width = 1058
        Height = 174
        Size.Values = (
          328.839285714285700000
          0.000000000000000000
          160.639880952381000000
          1999.494047619048000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Style = bsClear
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object qr11: TQRLabel
        Left = 586
        Top = 94
        Width = 112
        Height = 14
        Size.Values = (
          26.458333333333330000
          1107.470238095238000000
          177.648809523809500000
          211.666666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'RESERVADO AO FISCO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object qr12: TQRLabel
        Left = 0
        Top = 68
        Width = 112
        Height = 17
        Size.Values = (
          32.127976190476190000
          0.000000000000000000
          128.511904761904800000
          211.666666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'DADOS ADICIONAIS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrDadosAdicionais: TQRMemo
        Left = 8
        Top = 112
        Width = 561
        Height = 138
        Size.Values = (
          260.803571428571400000
          15.119047619047620000
          211.666666666666700000
          1060.223214285714000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Lines.Strings = (
          'Dados Adicionais....')
        ParentFont = False
        Transparent = True
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 7
      end
      object rbDadosAdicionais: TQRShape
        Left = 578
        Top = 84
        Width = 1
        Height = 174
        Size.Values = (
          328.839285714285700000
          1092.351190476190000000
          158.750000000000000000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qrDataHoraImpressao: TQRLabel
        Left = 2
        Top = 264
        Width = 106
        Height = 16
        Size.Values = (
          30.238095238095240000
          3.779761904761905000
          498.928571428571400000
          200.327380952381000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'qrDataHoraImpressao'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qr4: TQRLabel
        Left = 7
        Top = 94
        Width = 179
        Height = 14
        Size.Values = (
          26.458333333333330000
          13.229166666666670000
          177.648809523809500000
          338.288690476190500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'INFORMA'#199#213'ES COMPLEMENTARES'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
    end
    object qrItensNota: TQRBand
      Left = 26
      Top = 805
      Width = 1058
      Height = 16
      Frame.DrawBottom = True
      Frame.DrawLeft = True
      Frame.DrawRight = True
      Frame.Style = psClear
      AlignToBottom = False
      BeforePrint = qrItensNotaBeforePrint
      Color = clWhite
      TransparentBand = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -8
      Font.Name = 'Arial'
      Font.Style = []
      ForceNewColumn = False
      ForceNewPage = False
      ParentFont = False
      Size.Values = (
        30.238095238095240000
        1999.494047619048000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbDetail
      object qrs2: TQRShape
        Left = 85
        Top = 0
        Width = 1
        Height = 15
        Size.Values = (
          29.104166666666670000
          161.395833333333300000
          0.000000000000000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qrs3: TQRShape
        Left = 346
        Top = 0
        Width = 1
        Height = 15
        Size.Values = (
          29.104166666666670000
          653.520833333333400000
          0.000000000000000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qrs4: TQRShape
        Left = 410
        Top = 0
        Width = 1
        Height = 15
        Size.Values = (
          29.104166666666670000
          775.229166666666800000
          0.000000000000000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qrs5: TQRShape
        Left = 447
        Top = -1
        Width = 1
        Height = 15
        Size.Values = (
          29.104166666666670000
          844.020833333333500000
          -2.645833333333333000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qrs6: TQRShape
        Left = 484
        Top = 0
        Width = 1
        Height = 15
        Size.Values = (
          29.104166666666670000
          915.458333333333200000
          0.000000000000000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qrs7: TQRShape
        Left = 526
        Top = 0
        Width = 1
        Height = 15
        Size.Values = (
          29.104166666666670000
          994.833333333333400000
          0.000000000000000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qrs8: TQRShape
        Left = 594
        Top = 0
        Width = 1
        Height = 15
        Size.Values = (
          29.104166666666670000
          1121.833333333333000000
          0.000000000000000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qrs9: TQRShape
        Left = 669
        Top = 0
        Width = 1
        Height = 17
        Size.Values = (
          31.750000000000000000
          1264.708333333333000000
          0.000000000000000000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qrs10: TQRShape
        Left = 759
        Top = 0
        Width = 1
        Height = 15
        Size.Values = (
          28.348214285714290000
          1434.419642857143000000
          0.000000000000000000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qrs11: TQRShape
        Left = 846
        Top = 0
        Width = 1
        Height = 15
        Size.Values = (
          28.348214285714290000
          1598.839285714286000000
          0.000000000000000000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qrs12: TQRShape
        Left = 907
        Top = 0
        Width = 1
        Height = 15
        Size.Values = (
          28.348214285714290000
          1714.122023809524000000
          0.000000000000000000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qrs13: TQRShape
        Left = 959
        Top = 0
        Width = 1
        Height = 15
        Size.Values = (
          28.348214285714290000
          1812.395833333333000000
          0.000000000000000000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qrs14: TQRShape
        Left = 1005
        Top = 0
        Width = 1
        Height = 15
        Size.Values = (
          28.348214285714290000
          1899.330357142857000000
          0.000000000000000000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object qrProdutoId: TQRLabel
        Left = 6
        Top = 0
        Width = 74
        Height = 15
        Size.Values = (
          28.348214285714290000
          11.339285714285710000
          0.000000000000000000
          139.851190476190500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '4'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrNomeProduto: TQRLabel
        Left = 89
        Top = 0
        Width = 255
        Height = 15
        Size.Values = (
          28.348214285714290000
          168.199404761904800000
          0.000000000000000000
          481.919642857142900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = True
        Caption = 'PRODUTO TESTE'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrNCM: TQRLabel
        Left = 351
        Top = 0
        Width = 55
        Height = 15
        Size.Values = (
          28.348214285714290000
          663.348214285714300000
          0.000000000000000000
          103.943452380952400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '85258029'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrCST: TQRLabel
        Left = 413
        Top = 0
        Width = 32
        Height = 15
        Size.Values = (
          28.348214285714290000
          780.520833333333200000
          0.000000000000000000
          60.476190476190480000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '060'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrCFOP: TQRLabel
        Left = 451
        Top = 0
        Width = 30
        Height = 15
        Size.Values = (
          28.348214285714290000
          852.336309523809500000
          0.000000000000000000
          56.696428571428570000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '5102'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrUnidade: TQRLabel
        Left = 490
        Top = 0
        Width = 32
        Height = 15
        Size.Values = (
          28.348214285714290000
          926.041666666666800000
          0.000000000000000000
          60.476190476190480000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'UND'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrQuantidade: TQRLabel
        Left = 529
        Top = 0
        Width = 64
        Height = 15
        Size.Values = (
          28.348214285714290000
          999.747023809523800000
          0.000000000000000000
          120.952380952381000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '2,00'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrValorUnitario: TQRLabel
        Left = 602
        Top = 0
        Width = 60
        Height = 15
        Size.Values = (
          28.348214285714290000
          1137.708333333333000000
          0.000000000000000000
          113.392857142857100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '15,96'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrValorTotalProduto: TQRLabel
        Left = 675
        Top = 0
        Width = 78
        Height = 15
        Size.Values = (
          28.348214285714290000
          1275.669642857143000000
          0.000000000000000000
          147.410714285714300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '31,92'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrBaseICMSProduto: TQRLabel
        Left = 764
        Top = 0
        Width = 78
        Height = 15
        Size.Values = (
          28.348214285714290000
          1443.869047619048000000
          0.000000000000000000
          147.410714285714300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '31,92'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrValorICMSProduto: TQRLabel
        Left = 851
        Top = 0
        Width = 54
        Height = 15
        Size.Values = (
          28.663194444444440000
          1608.446180555556000000
          0.000000000000000000
          101.423611111111100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '31,92'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrValorIPIProduto: TQRLabel
        Left = 912
        Top = 0
        Width = 44
        Height = 15
        Size.Values = (
          28.663194444444440000
          1723.098958333333000000
          0.000000000000000000
          83.784722222222220000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '31,92'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrAliquotaICMS: TQRLabel
        Left = 966
        Top = 0
        Width = 36
        Height = 15
        Size.Values = (
          28.663194444444440000
          1825.625000000000000000
          0.000000000000000000
          68.350694444444440000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '18,00'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrAliquotaIPI: TQRLabel
        Left = 1011
        Top = 0
        Width = 36
        Height = 15
        Size.Values = (
          28.663194444444440000
          1911.614583333333000000
          0.000000000000000000
          68.350694444444440000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '1,65'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
    end
    object QRChildBand1: TQRChildBand
      Left = 26
      Top = 1105
      Width = 1058
      Height = 408
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        771.071428571428600000
        1999.494047619048000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      ParentBand = qrbDadosDanfe
      PrintOrder = cboAfterParent
      object QRShape3: TQRShape
        Left = 0
        Top = 262
        Width = 1058
        Height = 127
        Size.Values = (
          240.014880952381000000
          0.000000000000000000
          495.148809523809500000
          1999.494047619048000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Style = bsClear
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel9: TQRLabel
        Left = 0
        Top = 0
        Width = 165
        Height = 19
        Size.Values = (
          35.907738095238100000
          0.000000000000000000
          0.000000000000000000
          311.830357142857100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'DESTINAT'#193'RIO / REMETENTE'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel15: TQRLabel
        Left = 0
        Top = 245
        Width = 270
        Height = 16
        Size.Values = (
          30.238095238095240000
          0.000000000000000000
          463.020833333333300000
          510.267857142857100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'TRANSPORTADOR / VOLUMES TRANSPORTADOS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel16: TQRLabel
        Left = 0
        Top = 142
        Width = 132
        Height = 19
        Size.Values = (
          35.907738095238100000
          0.000000000000000000
          268.363095238095200000
          249.464285714285700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'C'#193'LCULO DO IMPOSTO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRShape52: TQRShape
        Left = 0
        Top = 18
        Width = 837
        Height = 124
        Size.Values = (
          234.345238095238100000
          0.000000000000000000
          34.017857142857140000
          1581.830357142857000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Style = bsClear
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape53: TQRShape
        Left = 630
        Top = 18
        Width = 1
        Height = 42
        Size.Values = (
          79.375000000000000000
          1190.625000000000000000
          34.395833333333340000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape54: TQRShape
        Left = 518
        Top = 60
        Width = 1
        Height = 42
        Size.Values = (
          79.375000000000000000
          978.958333333333200000
          113.770833333333300000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape55: TQRShape
        Left = 725
        Top = 60
        Width = 1
        Height = 42
        Size.Values = (
          79.375000000000000000
          1370.541666666667000000
          113.770833333333300000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape70: TQRShape
        Left = 253
        Top = 102
        Width = 1
        Height = 41
        Size.Values = (
          76.729166666666680000
          478.895833333333400000
          193.145833333333300000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape71: TQRShape
        Left = 461
        Top = 102
        Width = 1
        Height = 41
        Size.Values = (
          76.729166666666680000
          870.479166666666800000
          193.145833333333300000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape72: TQRShape
        Left = 553
        Top = 102
        Width = 1
        Height = 41
        Size.Values = (
          76.729166666666680000
          1045.104166666667000000
          193.145833333333300000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape73: TQRShape
        Left = 0
        Top = 59
        Width = 837
        Height = 2
        Size.Values = (
          3.779761904761905000
          0.000000000000000000
          111.502976190476200000
          1581.830357142857000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape74: TQRShape
        Left = 0
        Top = 101
        Width = 837
        Height = 2
        Size.Values = (
          3.779761904761905000
          0.000000000000000000
          190.877976190476200000
          1581.830357142857000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape75: TQRShape
        Left = 851
        Top = 18
        Width = 207
        Height = 124
        Size.Values = (
          234.345238095238100000
          1608.288690476190000000
          34.017857142857140000
          391.205357142857100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Style = bsClear
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape76: TQRShape
        Left = 851
        Top = 59
        Width = 207
        Height = 1
        Size.Values = (
          1.889880952380952000
          1608.288690476190000000
          111.502976190476200000
          391.205357142857100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape77: TQRShape
        Left = 851
        Top = 101
        Width = 207
        Height = 1
        Size.Values = (
          1.889880952380952000
          1608.288690476190000000
          190.877976190476200000
          391.205357142857100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape78: TQRShape
        Left = 0
        Top = 155
        Width = 1058
        Height = 84
        Size.Values = (
          158.750000000000000000
          0.000000000000000000
          292.931547619047600000
          1999.494047619048000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Style = bsClear
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRShape79: TQRShape
        Left = 0
        Top = 202
        Width = 1058
        Height = 1
        Size.Values = (
          1.889880952380952000
          0.000000000000000000
          381.755952380952400000
          1999.494047619048000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape80: TQRShape
        Left = 266
        Top = 155
        Width = 1
        Height = 47
        Size.Values = (
          88.824404761904760000
          502.708333333333300000
          292.931547619047600000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape81: TQRShape
        Left = 148
        Top = 203
        Width = 1
        Height = 36
        Size.Values = (
          68.035714285714290000
          279.702380952381000000
          383.645833333333300000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape82: TQRShape
        Left = 458
        Top = 155
        Width = 1
        Height = 47
        Size.Values = (
          88.824404761904760000
          865.565476190476200000
          292.931547619047600000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape83: TQRShape
        Left = 678
        Top = 155
        Width = 1
        Height = 47
        Size.Values = (
          88.824404761904760000
          1281.339285714286000000
          292.931547619047600000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape84: TQRShape
        Left = 862
        Top = 155
        Width = 1
        Height = 47
        Size.Values = (
          88.824404761904760000
          1629.077380952381000000
          292.931547619047600000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape85: TQRShape
        Left = 301
        Top = 203
        Width = 1
        Height = 36
        Size.Values = (
          68.035714285714290000
          568.854166666666700000
          383.645833333333300000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape86: TQRShape
        Left = 428
        Top = 203
        Width = 1
        Height = 36
        Size.Values = (
          68.035714285714290000
          808.869047619047600000
          383.645833333333300000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape87: TQRShape
        Left = 594
        Top = 202
        Width = 1
        Height = 37
        Size.Values = (
          69.925595238095240000
          1122.589285714286000000
          381.755952380952400000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape88: TQRShape
        Left = 802
        Top = 202
        Width = 1
        Height = 37
        Size.Values = (
          69.925595238095240000
          1515.684523809524000000
          381.755952380952400000
          1.889880952380952000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape89: TQRShape
        Left = 1
        Top = 304
        Width = 1057
        Height = 1
        Size.Values = (
          1.889880952380952000
          1.889880952380952000
          574.523809523809500000
          1997.604166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape90: TQRShape
        Left = 1
        Top = 346
        Width = 1057
        Height = 1
        Size.Values = (
          1.889880952380952000
          1.889880952380952000
          653.898809523809500000
          1997.604166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape91: TQRShape
        Left = 431
        Top = 262
        Width = 1
        Height = 127
        Size.Values = (
          240.770833333333300000
          814.916666666666800000
          494.770833333333300000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape92: TQRShape
        Left = 560
        Top = 263
        Width = 1
        Height = 41
        Size.Values = (
          76.729166666666680000
          1058.333333333333000000
          497.416666666666700000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape93: TQRShape
        Left = 661
        Top = 263
        Width = 1
        Height = 41
        Size.Values = (
          76.729166666666680000
          1248.833333333333000000
          497.416666666666700000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape94: TQRShape
        Left = 798
        Top = 263
        Width = 1
        Height = 41
        Size.Values = (
          76.729166666666680000
          1508.125000000000000000
          497.416666666666700000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape95: TQRShape
        Left = 854
        Top = 263
        Width = 1
        Height = 41
        Size.Values = (
          76.729166666666680000
          1613.958333333333000000
          497.416666666666700000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape96: TQRShape
        Left = 749
        Top = 305
        Width = 1
        Height = 42
        Size.Values = (
          79.375000000000000000
          1415.520833333333000000
          576.791666666666800000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape97: TQRShape
        Left = 804
        Top = 304
        Width = 1
        Height = 43
        Size.Values = (
          82.020833333333320000
          1518.708333333333000000
          574.145833333333300000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape98: TQRShape
        Left = 127
        Top = 347
        Width = 1
        Height = 42
        Size.Values = (
          79.375000000000000000
          240.770833333333300000
          656.166666666666800000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape99: TQRShape
        Left = 294
        Top = 347
        Width = 1
        Height = 42
        Size.Values = (
          79.375000000000000000
          555.625000000000000000
          656.166666666666800000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape100: TQRShape
        Left = 588
        Top = 347
        Width = 1
        Height = 42
        Size.Values = (
          79.375000000000000000
          1111.250000000000000000
          656.166666666666800000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRShape103: TQRShape
        Left = 804
        Top = 346
        Width = 1
        Height = 43
        Size.Values = (
          82.020833333333320000
          1518.708333333333000000
          653.520833333333200000
          2.645833333333333000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsVertLine
        VertAdjust = 0
      end
      object QRLabel21: TQRLabel
        Left = 5
        Top = 21
        Width = 109
        Height = 14
        Size.Values = (
          26.458333333333330000
          9.449404761904762000
          39.687500000000000000
          205.997023809523800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'NOME / RAZ'#195'O SOCIAL'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel23: TQRLabel
        Left = 635
        Top = 21
        Width = 52
        Height = 14
        Size.Values = (
          26.458333333333330000
          1200.074404761905000000
          39.687500000000000000
          98.273809523809520000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'CNPJ / CPF'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel25: TQRLabel
        Left = 858
        Top = 21
        Width = 90
        Height = 14
        Size.Values = (
          26.458333333333330000
          1621.517857142857000000
          39.687500000000000000
          170.089285714285700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'DATA DA EMISS'#195'O'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel28: TQRLabel
        Left = 5
        Top = 63
        Width = 53
        Height = 14
        Size.Values = (
          26.458333333333330000
          9.449404761904762000
          119.062500000000000000
          100.163690476190500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'ENDERE'#199'O'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel33: TQRLabel
        Left = 522
        Top = 63
        Width = 91
        Height = 14
        Size.Values = (
          26.458333333333330000
          986.517857142857100000
          119.062500000000000000
          171.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'BAIRRO / DISTRITO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel73: TQRLabel
        Left = 730
        Top = 63
        Width = 20
        Height = 14
        Size.Values = (
          26.458333333333330000
          1379.613095238095000000
          119.062500000000000000
          37.797619047619050000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'CEP'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel74: TQRLabel
        Left = 858
        Top = 63
        Width = 132
        Height = 14
        Size.Values = (
          26.458333333333330000
          1621.517857142857000000
          119.062500000000000000
          249.464285714285700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'DATA DA SAIDA / ENTRADA'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel75: TQRLabel
        Left = 5
        Top = 105
        Width = 52
        Height = 14
        Size.Values = (
          26.458333333333330000
          9.449404761904762000
          198.437500000000000000
          98.273809523809520000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'MUNIC'#205'PIO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel76: TQRLabel
        Left = 261
        Top = 105
        Width = 55
        Height = 14
        Size.Values = (
          26.458333333333330000
          493.258928571428600000
          198.437500000000000000
          103.943452380952400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'FONE / FAX'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel77: TQRLabel
        Left = 467
        Top = 105
        Width = 14
        Height = 14
        Size.Values = (
          26.458333333333330000
          882.574404761904800000
          198.437500000000000000
          26.458333333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'UF'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel78: TQRLabel
        Left = 560
        Top = 105
        Width = 108
        Height = 14
        Size.Values = (
          26.458333333333330000
          1058.333333333333000000
          198.437500000000000000
          204.107142857142900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'INSCRI'#199#195'O ESTADUAL'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel79: TQRLabel
        Left = 858
        Top = 105
        Width = 77
        Height = 14
        Size.Values = (
          26.458333333333330000
          1621.517857142857000000
          198.437500000000000000
          145.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'HORA DE SA'#205'DA'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel80: TQRLabel
        Left = 9
        Top = 161
        Width = 134
        Height = 14
        Size.Values = (
          26.458333333333330000
          17.008928571428570000
          304.270833333333300000
          253.244047619047600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'BASE DE C'#193'LCULO DO ICMS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel81: TQRLabel
        Left = 278
        Top = 161
        Width = 78
        Height = 14
        Size.Values = (
          26.458333333333330000
          525.386904761904800000
          304.270833333333300000
          147.410714285714300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'VALOR DO ICMS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel82: TQRLabel
        Left = 469
        Top = 161
        Width = 206
        Height = 14
        Size.Values = (
          26.458333333333330000
          886.354166666666700000
          304.270833333333300000
          389.315476190476200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'BASE DE C'#193'LCULO DE ICMS SUBSTITUI'#199#195'O'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel83: TQRLabel
        Left = 688
        Top = 161
        Width = 151
        Height = 14
        Size.Values = (
          26.458333333333330000
          1300.238095238095000000
          304.270833333333300000
          285.372023809523800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'VALOR DO ICMS SUBSTITUI'#199#195'O'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel84: TQRLabel
        Left = 870
        Top = 161
        Width = 146
        Height = 14
        Size.Values = (
          26.458333333333330000
          1644.196428571429000000
          304.270833333333300000
          275.922619047619000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'VALOR TOTAL DOS PRODUTOS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel85: TQRLabel
        Left = 9
        Top = 203
        Width = 84
        Height = 14
        Size.Values = (
          26.458333333333330000
          17.008928571428570000
          383.645833333333300000
          158.750000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'VALOR DO FRETE'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel86: TQRLabel
        Left = 156
        Top = 203
        Width = 94
        Height = 14
        Size.Values = (
          26.458333333333330000
          294.821428571428600000
          383.645833333333300000
          177.648809523809500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'VALOR DO SEGURO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel87: TQRLabel
        Left = 308
        Top = 203
        Width = 53
        Height = 14
        Size.Values = (
          26.458333333333330000
          582.083333333333300000
          383.645833333333300000
          100.163690476190500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'DESCONTO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel88: TQRLabel
        Left = 434
        Top = 203
        Width = 154
        Height = 12
        Size.Values = (
          22.678571428571430000
          820.208333333333300000
          383.645833333333300000
          291.041666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'OUTRAS DESPESAS ACESS'#211'RIAS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel89: TQRLabel
        Left = 604
        Top = 203
        Width = 66
        Height = 14
        Size.Values = (
          26.458333333333330000
          1141.488095238095000000
          383.645833333333300000
          124.732142857142900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'VALOR DO IPI'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel90: TQRLabel
        Left = 810
        Top = 203
        Width = 115
        Height = 14
        Size.Values = (
          26.458333333333330000
          1530.803571428571000000
          383.645833333333300000
          217.336309523809500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'VALOR TOTAL DA NOTA'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel91: TQRLabel
        Left = 5
        Top = 264
        Width = 109
        Height = 14
        Size.Values = (
          26.458333333333330000
          9.449404761904762000
          498.928571428571400000
          205.997023809523800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'NOME / RAZ'#195'O SOCIAL'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel92: TQRLabel
        Left = 435
        Top = 265
        Width = 104
        Height = 13
        Size.Values = (
          23.812500000000000000
          822.854166666666800000
          500.062500000000000000
          195.791666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'FRETE POR CONTA'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel93: TQRLabel
        Left = 569
        Top = 264
        Width = 67
        Height = 14
        Size.Values = (
          26.458333333333330000
          1075.342261904762000000
          498.928571428571400000
          126.622023809523800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'C'#211'DIGO ANTT'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel94: TQRLabel
        Left = 667
        Top = 264
        Width = 95
        Height = 14
        Size.Values = (
          26.458333333333330000
          1260.550595238095000000
          498.928571428571400000
          179.538690476190500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'PLACA DO VE'#205'CULO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel95: TQRLabel
        Left = 807
        Top = 264
        Width = 14
        Height = 14
        Size.Values = (
          26.458333333333330000
          1525.133928571429000000
          498.928571428571400000
          26.458333333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'UF'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel96: TQRLabel
        Left = 859
        Top = 264
        Width = 52
        Height = 14
        Size.Values = (
          26.458333333333330000
          1623.407738095238000000
          498.928571428571400000
          98.273809523809520000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'CNPJ / CPF'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel97: TQRLabel
        Left = 5
        Top = 306
        Width = 53
        Height = 14
        Size.Values = (
          26.458333333333330000
          9.449404761904762000
          578.303571428571400000
          100.163690476190500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'ENDERE'#199'O'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel98: TQRLabel
        Left = 439
        Top = 306
        Width = 52
        Height = 14
        Size.Values = (
          26.458333333333330000
          829.657738095238100000
          578.303571428571400000
          98.273809523809520000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'MUNIC'#205'PIO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel99: TQRLabel
        Left = 756
        Top = 306
        Width = 14
        Height = 14
        Size.Values = (
          26.458333333333330000
          1428.750000000000000000
          578.303571428571400000
          26.458333333333330000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'UF'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel100: TQRLabel
        Left = 812
        Top = 306
        Width = 108
        Height = 14
        Size.Values = (
          26.458333333333330000
          1534.583333333333000000
          578.303571428571400000
          204.107142857142900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'INSCRI'#199#195'O ESTADUAL'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel101: TQRLabel
        Left = 5
        Top = 348
        Width = 64
        Height = 14
        Size.Values = (
          26.458333333333330000
          9.449404761904762000
          657.678571428571400000
          120.952380952381000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'QUANTIDADE'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel102: TQRLabel
        Left = 133
        Top = 348
        Width = 39
        Height = 14
        Size.Values = (
          26.458333333333330000
          251.354166666666700000
          657.678571428571400000
          73.705357142857140000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'ESP'#201'CIE'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel103: TQRLabel
        Left = 301
        Top = 348
        Width = 38
        Height = 14
        Size.Values = (
          26.458333333333330000
          568.854166666666700000
          657.678571428571400000
          71.815476190476190000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'MARCA'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel104: TQRLabel
        Left = 441
        Top = 348
        Width = 64
        Height = 14
        Size.Values = (
          26.458333333333330000
          833.437500000000000000
          657.678571428571400000
          120.952380952381000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'NUMERA'#199#195'O'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel105: TQRLabel
        Left = 595
        Top = 348
        Width = 62
        Height = 14
        Size.Values = (
          26.458333333333330000
          1124.479166666667000000
          657.678571428571400000
          117.172619047619000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'PESO BRUTO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel106: TQRLabel
        Left = 810
        Top = 348
        Width = 67
        Height = 14
        Size.Values = (
          26.458333333333330000
          1530.803571428571000000
          657.678571428571400000
          126.622023809523800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'PESO L'#205'QUIDO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel107: TQRLabel
        Left = 7
        Top = 36
        Width = 617
        Height = 21
        Size.Values = (
          39.687500000000000000
          13.229166666666670000
          68.035714285714290000
          1166.056547619048000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNomeRazaoSocialDestinatario'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel108: TQRLabel
        Left = 644
        Top = 35
        Width = 158
        Height = 21
        Size.Values = (
          39.687500000000000000
          1217.083333333333000000
          66.145833333333330000
          298.601190476190500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'qrCPFCNPJDestinatario'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel109: TQRLabel
        Left = 8
        Top = 76
        Width = 507
        Height = 21
        Size.Values = (
          39.687500000000000000
          15.875000000000000000
          142.875000000000000000
          957.791666666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrEnderecoDestinatario'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel110: TQRLabel
        Left = 521
        Top = 76
        Width = 201
        Height = 21
        Size.Values = (
          39.687500000000000000
          984.627976190476200000
          143.630952380952400000
          379.866071428571400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrBairroDestinatario'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel111: TQRLabel
        Left = 731
        Top = 76
        Width = 94
        Height = 21
        Size.Values = (
          39.687500000000000000
          1381.712962962963000000
          144.050925925926000000
          177.858796296296300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrCEPDestinatario'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel112: TQRLabel
        Left = 3
        Top = 119
        Width = 247
        Height = 22
        Size.Values = (
          41.577380952380950000
          5.669642857142857000
          224.895833333333300000
          466.800595238095200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrCidadeDestinatario'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel113: TQRLabel
        Left = 261
        Top = 119
        Width = 153
        Height = 21
        Size.Values = (
          39.687500000000000000
          493.258928571428600000
          224.895833333333300000
          289.151785714285700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'qrTelefoneDestinatario'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel114: TQRLabel
        Left = 468
        Top = 119
        Width = 81
        Height = 21
        Size.Values = (
          39.687500000000000000
          884.464285714285700000
          224.895833333333300000
          153.080357142857100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrUFDestinatario'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel115: TQRLabel
        Left = 560
        Top = 119
        Width = 206
        Height = 21
        Size.Values = (
          39.687500000000000000
          1058.333333333333000000
          224.895833333333300000
          389.315476190476200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'qrInscricaoEstadualDestinatario'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel116: TQRLabel
        Left = 868
        Top = 35
        Width = 179
        Height = 21
        Size.Values = (
          39.687500000000000000
          1640.416666666667000000
          66.145833333333320000
          338.288690476190500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrDataEmissao'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel117: TQRLabel
        Left = 868
        Top = 76
        Width = 179
        Height = 21
        Size.Values = (
          39.687500000000000000
          1640.416666666667000000
          143.630952380952400000
          338.288690476190500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrDataSaidaEntrada'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel118: TQRLabel
        Left = 118
        Top = 174
        Width = 140
        Height = 21
        Size.Values = (
          39.687500000000000000
          223.005952380952400000
          328.839285714285700000
          264.583333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrBaseICMS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel119: TQRLabel
        Left = 331
        Top = 174
        Width = 122
        Height = 21
        Size.Values = (
          39.687500000000000000
          625.550595238095200000
          328.839285714285700000
          230.565476190476200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrValorICMS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel120: TQRLabel
        Left = 541
        Top = 174
        Width = 130
        Height = 21
        Size.Values = (
          39.687500000000000000
          1022.425595238095000000
          328.839285714285700000
          245.684523809523800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrBaseICMSST'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel121: TQRLabel
        Left = 707
        Top = 174
        Width = 150
        Height = 21
        Size.Values = (
          39.687500000000000000
          1336.145833333333000000
          328.839285714285700000
          283.482142857142900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrValorICMSST'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel122: TQRLabel
        Left = 869
        Top = 174
        Width = 168
        Height = 21
        Size.Values = (
          39.687500000000000000
          1643.062500000000000000
          328.083333333333300000
          317.500000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrTotalProdutos'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel123: TQRLabel
        Left = 18
        Top = 218
        Width = 122
        Height = 21
        Size.Values = (
          39.687500000000000000
          34.395833333333330000
          412.750000000000000000
          230.187500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrValorFrete'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel124: TQRLabel
        Left = 158
        Top = 218
        Width = 130
        Height = 21
        Size.Values = (
          39.687500000000000000
          298.979166666666700000
          412.750000000000000000
          246.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrValorSeguro'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel125: TQRLabel
        Left = 305
        Top = 218
        Width = 112
        Height = 21
        Size.Values = (
          39.687500000000000000
          576.791666666666800000
          412.750000000000000000
          211.666666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrValorDesconto'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel126: TQRLabel
        Left = 449
        Top = 218
        Width = 130
        Height = 21
        Size.Values = (
          39.687500000000000000
          849.312500000000000000
          412.750000000000000000
          246.062500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrOutrasDespesas'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel127: TQRLabel
        Left = 644
        Top = 218
        Width = 150
        Height = 21
        Size.Values = (
          39.687500000000000000
          1217.083333333333000000
          412.750000000000000000
          283.104166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrValorIPI'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel128: TQRLabel
        Left = 869
        Top = 218
        Width = 168
        Height = 21
        Size.Values = (
          39.687500000000000000
          1642.306547619048000000
          411.994047619047600000
          317.500000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrTotalNotaFiscal'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel129: TQRLabel
        Left = 7
        Top = 277
        Width = 419
        Height = 21
        Size.Values = (
          39.687500000000000000
          13.229166666666670000
          523.875000000000000000
          791.104166666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNomeTransportadora'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel130: TQRLabel
        Left = 566
        Top = 280
        Width = 94
        Height = 21
        Size.Values = (
          39.687500000000000000
          1068.916666666667000000
          529.166666666666700000
          177.270833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrCodigoANTT'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel131: TQRLabel
        Left = 667
        Top = 280
        Width = 97
        Height = 21
        Size.Values = (
          39.687500000000000000
          1260.550595238095000000
          529.166666666666700000
          183.318452380952400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'qrPlacaVeiculo'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel132: TQRLabel
        Left = 806
        Top = 280
        Width = 38
        Height = 21
        Size.Values = (
          39.687500000000000000
          1524.000000000000000000
          529.166666666666700000
          71.437500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrUFVeiculo'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel133: TQRLabel
        Left = 859
        Top = 280
        Width = 179
        Height = 21
        Size.Values = (
          39.687500000000000000
          1623.407738095238000000
          529.166666666666700000
          338.288690476190500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'qrCPFCNPJTransportadora'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel134: TQRLabel
        Left = 8
        Top = 322
        Width = 417
        Height = 21
        Size.Values = (
          39.687500000000000000
          15.875000000000000000
          608.541666666666800000
          788.458333333333200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrEnderecoTransportadora'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel135: TQRLabel
        Left = 440
        Top = 322
        Width = 305
        Height = 21
        Size.Values = (
          39.687500000000000000
          830.791666666666800000
          608.541666666666800000
          576.791666666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrCidadeTransportadora'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel136: TQRLabel
        Left = 756
        Top = 322
        Width = 38
        Height = 21
        Size.Values = (
          39.687500000000000000
          1428.750000000000000000
          608.541666666666800000
          71.437500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrUFTransportadora'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel137: TQRLabel
        Left = 812
        Top = 322
        Width = 172
        Height = 21
        Size.Values = (
          39.687500000000000000
          1534.583333333333000000
          608.541666666666700000
          325.059523809523800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'qrInscricaoEstadualTransp'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel138: TQRLabel
        Left = 10
        Top = 364
        Width = 102
        Height = 21
        Size.Values = (
          39.687500000000000000
          18.520833333333330000
          687.916666666666800000
          193.145833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'qrQuantidadeTransp'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel139: TQRLabel
        Left = 130
        Top = 364
        Width = 158
        Height = 21
        Size.Values = (
          39.687500000000000000
          246.062500000000000000
          687.916666666666800000
          298.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrEspecieTransportadora'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel140: TQRLabel
        Left = 298
        Top = 364
        Width = 126
        Height = 21
        Size.Values = (
          39.687500000000000000
          563.562500000000000000
          687.916666666666800000
          238.125000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrMarcaTransportadora'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel141: TQRLabel
        Left = 438
        Top = 364
        Width = 140
        Height = 21
        Size.Values = (
          39.687500000000000000
          827.767857142857100000
          687.916666666666800000
          264.583333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrNumeracaoTransportadora'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel145: TQRLabel
        Left = 595
        Top = 364
        Width = 127
        Height = 21
        Size.Values = (
          39.687500000000000000
          1124.479166666667000000
          687.916666666666700000
          240.014880952381000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'qrPesoBrutoTransp'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel155: TQRLabel
        Left = 810
        Top = 364
        Width = 139
        Height = 21
        Size.Values = (
          39.687500000000000000
          1530.803571428571000000
          687.916666666666700000
          262.693452380952400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'qrPesoLiquidoTransp'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel158: TQRLabel
        Left = 868
        Top = 119
        Width = 179
        Height = 21
        Size.Values = (
          39.687500000000000000
          1640.416666666667000000
          224.895833333333300000
          338.288690476190500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'qrHoraSaida'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel159: TQRLabel
        Left = 0
        Top = 390
        Width = 183
        Height = 16
        Size.Values = (
          30.238095238095240000
          0.000000000000000000
          737.053571428571400000
          345.848214285714300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'DADOS DO PRODUTO / SERVI'#199'OS'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRShape104: TQRShape
        Left = 519
        Top = 278
        Width = 19
        Height = 20
        Size.Values = (
          37.797619047619050000
          980.848214285714300000
          525.386904761904800000
          35.907738095238100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel160: TQRLabel
        Left = 436
        Top = 278
        Width = 57
        Height = 12
        Size.Values = (
          22.678571428571430000
          823.988095238095200000
          525.386904761904800000
          107.723214285714300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = '1-EMITENTE'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel161: TQRLabel
        Left = 436
        Top = 289
        Width = 80
        Height = 12
        Size.Values = (
          22.678571428571430000
          823.988095238095200000
          546.175595238095200000
          151.190476190476200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = '2-DESTINAT'#193'RIO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel162: TQRLabel
        Left = 524
        Top = 280
        Width = 8
        Height = 18
        Size.Values = (
          34.395833333333340000
          989.541666666666800000
          529.166666666666700000
          15.875000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '2'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
    end
  end
  object QRPDFFilter1: TQRPDFFilter
    CompressionOn = False
    TextEncoding = AnsiEncoding
    Codepage = '1252'
    Left = 40
    Top = 16
  end
end
