unit _BoletoSicoob;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPrincipal, Vcl.Imaging.jpeg,
  QRCtrls, QRBarcodeComum, QuickRpt, Vcl.ExtCtrls, _CobrancaBancaria, _Biblioteca, _Sessao;

type
  TFormBoletoSicoob = class(TFormHerancaPrincipal)
    qrBoletoSICOOB: TQuickRep;
    qrbnd1: TQRBand;
    QRShape121: TQRShape;
    QRShape13: TQRShape;
    QRShape125: TQRShape;
    QRShape127: TQRShape;
    QRShape16: TQRShape;
    QRShape126: TQRShape;
    QRShape1: TQRShape;
    QRShape193: TQRShape;
    QRShape199: TQRShape;
    QRShape122: TQRShape;
    QRShape207: TQRShape;
    QRShape212: TQRShape;
    QRShape211: TQRShape;
    QRShape213: TQRShape;
    QRShape214: TQRShape;
    QRShape216: TQRShape;
    QRShape215: TQRShape;
    QRShape217: TQRShape;
    QRShape218: TQRShape;
    QRShape219: TQRShape;
    QRShape220: TQRShape;
    QRShape221: TQRShape;
    QRShape222: TQRShape;
    QRShape223: TQRShape;
    QRShape224: TQRShape;
    QRShape225: TQRShape;
    QRShape226: TQRShape;
    QRShape227: TQRShape;
    QRShape228: TQRShape;
    QRShape229: TQRShape;
    QRShape230: TQRShape;
    QRShape231: TQRShape;
    QRShape95: TQRShape;
    QRShape98: TQRShape;
    QRShape100: TQRShape;
    QRShape99: TQRShape;
    QRShape101: TQRShape;
    QRShape102: TQRShape;
    QRShape104: TQRShape;
    QRShape103: TQRShape;
    QRShape105: TQRShape;
    QRShape106: TQRShape;
    QRShape107: TQRShape;
    QRShape108: TQRShape;
    QRShape109: TQRShape;
    QRShape110: TQRShape;
    QRShape111: TQRShape;
    QRShape112: TQRShape;
    QRShape113: TQRShape;
    QRShape114: TQRShape;
    QRShape115: TQRShape;
    QRShape116: TQRShape;
    QRShape117: TQRShape;
    QRShape118: TQRShape;
    QRShape119: TQRShape;
    qr1: TQRLabel;
    qr2: TQRLabel;
    qrVencimentoBra: TQRLabel;
    qr3: TQRLabel;
    qr4: TQRLabel;
    qr5: TQRLabel;
    qr6: TQRLabel;
    qr7: TQRLabel;
    qr8: TQRLabel;
    qr9: TQRLabel;
    qr10: TQRLabel;
    qr11: TQRLabel;
    qr12: TQRLabel;
    qr13: TQRLabel;
    qr14: TQRLabel;
    qr15: TQRLabel;
    qrInstrucaoBra: TQRLabel;
    qr16: TQRLabel;
    qr17: TQRLabel;
    qr18: TQRLabel;
    qr19: TQRLabel;
    qr20: TQRLabel;
    qr21: TQRLabel;
    qr22: TQRLabel;
    qr23: TQRLabel;
    qrBeneficiario: TQRLabel;
    qrDataEmissaoBra: TQRLabel;
    qrNumeroDocumentoBra: TQRLabel;
    qrEspecieDocumentoBra: TQRLabel;
    qr24: TQRLabel;
    qrCarteiraBra: TQRLabel;
    qrAceiteBra: TQRLabel;
    qrDataProcessamentoBra: TQRLabel;
    qrValorBra: TQRLabel;
    qrPagador: TQRLabel;
    qrValorDocumentoBra: TQRLabel;
    qrAgenciaBeneficiario: TQRLabel;
    qr25: TQRLabel;
    qrNossoNumeroBra: TQRLabel;
    qr26: TQRLabel;
    QRShape123: TQRShape;
    qrCodigoBarraBra: TQRBarcode;
    QRShape124: TQRShape;
    qr27: TQRLabel;
    qrVencimentoBra3: TQRLabel;
    qr28: TQRLabel;
    qr29: TQRLabel;
    qr30: TQRLabel;
    qrBeneficiario3: TQRLabel;
    qrValorDocumentoBra3: TQRLabel;
    qrAgenciaBeneficiario3: TQRLabel;
    qrNossoNumeroBra3: TQRLabel;
    qr31: TQRLabel;
    QRShape132: TQRShape;
    QRShape133: TQRShape;
    qrLinhaDigitavelBRA: TQRLabel;
    qr32: TQRLabel;
    qr33: TQRLabel;
    qrVencimentoBra2: TQRLabel;
    qr34: TQRLabel;
    qr35: TQRLabel;
    qr36: TQRLabel;
    qr37: TQRLabel;
    qr38: TQRLabel;
    qr39: TQRLabel;
    qr40: TQRLabel;
    qr41: TQRLabel;
    qr42: TQRLabel;
    qr43: TQRLabel;
    qr44: TQRLabel;
    qr45: TQRLabel;
    qrInstrucaoBra2: TQRLabel;
    qr46: TQRLabel;
    qr47: TQRLabel;
    qr48: TQRLabel;
    qr49: TQRLabel;
    qr50: TQRLabel;
    qr51: TQRLabel;
    qr52: TQRLabel;
    qr53: TQRLabel;
    qrBeneficiario2: TQRLabel;
    qrDataEmissaoBra2: TQRLabel;
    qrNumeroDocumentoBra2: TQRLabel;
    qrEspecieDocumentoBra2: TQRLabel;
    qr54: TQRLabel;
    qrCarteiraBra2: TQRLabel;
    qrAceite: TQRLabel;
    qrDataProcessamentoBra2: TQRLabel;
    qrValorBra2: TQRLabel;
    qrPagador2: TQRLabel;
    qrValorDocumentoBra2: TQRLabel;
    qrAgenciaBeneficiario2: TQRLabel;
    qr55: TQRLabel;
    qrNossoNumeroBra2: TQRLabel;
    qr56: TQRLabel;
    qr57: TQRLabel;
    QRShape236: TQRShape;
    qrCpfCnpjBra: TQRLabel;
    qrLogradouroBra: TQRLabel;
    qrCepBra: TQRLabel;
    qrCidadeBra: TQRLabel;
    qrCpfCnpjBra2: TQRLabel;
    qrLogradouroBra2: TQRLabel;
    qrCepBra2: TQRLabel;
    qrCidadeBra2: TQRLabel;
    QRShape235: TQRShape;
    qr58: TQRLabel;
    qr59: TQRLabel;
    qrNumero_Nota1: TQRLabel;
    qrNumero_Nota: TQRLabel;
    qr60: TQRLabel;
    qr61: TQRLabel;
    qrPagador3: TQRLabel;
    qrNumeroDocumentoBra3: TQRLabel;
    qr62: TQRLabel;
    qrPedido: TQRLabel;
    qrPedido1: TQRLabel;
    qrDocumento: TQRLabel;
    qrDocumento1: TQRLabel;
    qrlDataEmissao: TQRLabel;
    qrDataEmissaoBra3: TQRLabel;
    qr63: TQRLabel;
    qrBeneficiarioAvalista: TQRLabel;
    qr64: TQRLabel;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    qr65: TQRLabel;
    qr66: TQRLabel;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    qr67: TQRLabel;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    qr68: TQRLabel;
    qr69: TQRLabel;
    QRShape12: TQRShape;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    qr70: TQRLabel;
    qrDocumento2: TQRLabel;
    qrPedido2: TQRLabel;
    qrNumero_Nota2: TQRLabel;
    qrNomeFantasiaEmpresa: TQRLabel;
    qr71: TQRLabel;
    qr72: TQRLabel;
    qrEnderecoBeneficiario: TQRLabel;
    qr73: TQRLabel;
    qrCPFCNPJBeneficiario: TQRLabel;
    qrCPFCNPJBeneficiario2: TQRLabel;
    qr74: TQRLabel;
    QRImage2: TQRImage;
    QRImage1: TQRImage;
    QRImage3: TQRImage;
    qrObservacao1: TQRLabel;
    qrObservacao: TQRLabel;
    qrbnd2: TQRBand;
    qr75: TQRLabel;
    QRImage8: TQRImage;
    QRShape135: TQRShape;
    QRShape136: TQRShape;
    qr76: TQRLabel;
    qr77: TQRLabel;
    qr78: TQRLabel;
    qr79: TQRLabel;
    QRShape137: TQRShape;
    QRShape138: TQRShape;
    qr80: TQRLabel;
    qr81: TQRLabel;
    QRShape139: TQRShape;
    QRShape140: TQRShape;
    QRShape141: TQRShape;
    QRShape142: TQRShape;
    QRShape143: TQRShape;
    QRShape144: TQRShape;
    QRShape145: TQRShape;
    QRShape146: TQRShape;
    QRShape147: TQRShape;
    QRShape148: TQRShape;
    QRShape149: TQRShape;
    QRShape150: TQRShape;
    QRShape151: TQRShape;
    QRShape152: TQRShape;
    QRShape153: TQRShape;
    QRShape154: TQRShape;
    QRShape155: TQRShape;
    QRShape156: TQRShape;
    QRShape157: TQRShape;
    QRShape158: TQRShape;
    QRShape159: TQRShape;
    qr82: TQRLabel;
    qr83: TQRLabel;
    qr84: TQRLabel;
    qr85: TQRLabel;
    qr86: TQRLabel;
    qr87: TQRLabel;
    qr88: TQRLabel;
    qr89: TQRLabel;
    qr90: TQRLabel;
    qr91: TQRLabel;
    qr92: TQRLabel;
    qr93: TQRLabel;
    qr94: TQRLabel;
    qr95: TQRLabel;
    qr96: TQRLabel;
    qr97: TQRLabel;
    qr98: TQRLabel;
    qr99: TQRLabel;
    qr100: TQRLabel;
    qr101: TQRLabel;
    qr102: TQRLabel;
    qr103: TQRLabel;
    QRShape160: TQRShape;
    QRShape161: TQRShape;
    QRImage9: TQRImage;
    QRShape162: TQRShape;
    QRShape163: TQRShape;
    qr104: TQRLabel;
    qrbnd3: TQRBand;
    qr105: TQRLabel;
    QRImage10: TQRImage;
    QRShape164: TQRShape;
    QRShape165: TQRShape;
    qr106: TQRLabel;
    qr107: TQRLabel;
    qr108: TQRLabel;
    qr109: TQRLabel;
    QRShape166: TQRShape;
    QRShape167: TQRShape;
    qr110: TQRLabel;
    qr111: TQRLabel;
    QRShape168: TQRShape;
    QRShape169: TQRShape;
    QRShape170: TQRShape;
    QRShape171: TQRShape;
    QRShape172: TQRShape;
    QRShape173: TQRShape;
    QRShape174: TQRShape;
    QRShape175: TQRShape;
    QRShape176: TQRShape;
    QRShape177: TQRShape;
    QRShape178: TQRShape;
    QRShape179: TQRShape;
    QRShape180: TQRShape;
    QRShape181: TQRShape;
    QRShape182: TQRShape;
    QRShape183: TQRShape;
    QRShape184: TQRShape;
    QRShape185: TQRShape;
    QRShape186: TQRShape;
    QRShape187: TQRShape;
    QRShape188: TQRShape;
    qr112: TQRLabel;
    qr113: TQRLabel;
    qr114: TQRLabel;
    qr115: TQRLabel;
    qr116: TQRLabel;
    qr117: TQRLabel;
    qr118: TQRLabel;
    qr119: TQRLabel;
    qr120: TQRLabel;
    qr121: TQRLabel;
    qr122: TQRLabel;
    qr123: TQRLabel;
    qr124: TQRLabel;
    qr125: TQRLabel;
    qr126: TQRLabel;
    qr127: TQRLabel;
    qr128: TQRLabel;
    qr129: TQRLabel;
    qr130: TQRLabel;
    qr131: TQRLabel;
    qr132: TQRLabel;
    qr133: TQRLabel;
    QRShape189: TQRShape;
    QRShape190: TQRShape;
    QRImage11: TQRImage;
    QRShape191: TQRShape;
    QRShape192: TQRShape;
    qr134: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;
procedure Emitir(pBoletos: TArray<RecDadosBoletoBancario>);

implementation

{$R *.dfm}

procedure Emitir(pBoletos: TArray<RecDadosBoletoBancario>);
var
  i: Integer;
  vImagem: TJPEGImage;
  vForm: TFormBoletoSicoob;
begin
  if pBoletos = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
	end;

  vForm := TFormBoletoSicoob.Create(Application);

  if Sessao.getParametrosEmpresa.LogoEmpresa <> nil then begin
    try
      Sessao.getParametrosEmpresa.LogoEmpresa.Position := 0;
      vImagem := TJPEGImage.Create;
      vImagem.LoadFromStream(Sessao.getParametrosEmpresa.LogoEmpresa);
//      vForm.qrLogo.Picture.Graphic := vImagem;
    except

    end;
  end;

  try
    for i := Low(pBoletos) to High(pBoletos) do begin
      vForm.qrBeneficiario3.Caption := pBoletos[i].Beneficiario;
      vForm.qrAgenciaBeneficiario3.Caption := pBoletos[i].agencia + '-' + pBoletos[i].DigitoAgencia + ' / ' + pBoletos[i].ContaCorrente + '-' + pBoletos[i].DigitoContaCorrente;
      vForm.qrVencimentoBra3.Caption := DFormat(pBoletos[i].DataVencimento);
      vForm.qrPagador3.Caption := pBoletos[i].Pagador;
      vForm.qrDataEmissaoBra3.Caption := DFormat(pBoletos[i].DataEmissao);
      vForm.qrNumeroDocumentoBra3.Caption := NFormat(pBoletos[i].ReceberId);
      vForm.qrDocumento2.Caption := pBoletos[i].documento;
      vForm.qrNumero_Nota2.Caption := pBoletos[i].NumeroNota;
      vForm.qrNomeFantasiaEmpresa.Caption := pBoletos[i].Beneficiario;

      vForm.qrNossoNumeroBra3.Caption :=
        pBoletos[i].NossoNumero + '-' +
        pBoletos[i].DigitoVerificadorNossoNumero;

      vForm.qrValorDocumentoBra3.Caption := NFormat(pBoletos[i].ValorDocumento);


      vForm.qrVencimentoBra2.Caption := DFormat(pBoletos[i].DataVencimento);
      vForm.qrBeneficiario2.Caption := pBoletos[i].Beneficiario;
      vForm.qrCPFCNPJBeneficiario2.Caption := 'CNPJ/CPF: ' + pBoletos[i].CnpjBeneficiario;
      vForm.qrAgenciaBeneficiario2.Caption := pBoletos[i].agencia + '-' + pBoletos[i].DigitoAgencia + ' / ' + pBoletos[i].ContaCorrente + '-' + pBoletos[i].DigitoContaCorrente;
      vForm.qrEnderecoBeneficiario.Caption := pBoletos[i].EnderecoBeneficiario;
      vForm.qrDataEmissaoBra2.Caption := DFormat(pBoletos[i].DataEmissao);
      vForm.qrNumeroDocumentoBra2.Caption := NFormat(pBoletos[i].ReceberId);
      vForm.qrDataProcessamentoBra2.Caption := DFormat(pBoletos[i].DataEmissao);
      vForm.qrNossoNumeroBra2.Caption :=
        pBoletos[i].NossoNumero + '-' +
        pBoletos[i].DigitoVerificadorNossoNumero;

      vForm.qrValorDocumentoBra2.Caption := NFormat(pBoletos[i].ValorDocumento);
      vForm.qrInstrucaoBra2.Caption := pBoletos[i].Instrucao;

      vForm.qrPagador2.Caption := pBoletos[i].cliente;
      vForm.qrCpfCnpjBra2.Caption := pBoletos[i].CpfCnpj;
      vForm.qrLogradouroBra2.Caption := pBoletos[i].Logradouro;
      vForm.qrCepBra2.Caption := pBoletos[i].Cep;
      vForm.qrCidadeBra2.Caption := pBoletos[i].Cidade + '/' + pBoletos[i].Estado;

      vForm.qrLinhaDigitavelBRA.Caption := pBoletos[i].LinhaDigitavel;
      vForm.qrVencimentoBra.Caption := DFormat(pBoletos[i].DataVencimento);
      vForm.qrBeneficiario.Caption := pBoletos[i].Beneficiario;
      vForm.qrAgenciaBeneficiario.Caption := pBoletos[i].agencia + '-' + pBoletos[i].DigitoAgencia + ' / ' + pBoletos[i].ContaCorrente + '-' + pBoletos[i].DigitoContaCorrente;
      vForm.qrDataEmissaoBra.Caption := DFormat(pBoletos[i].DataEmissao);
      vForm.qrNumeroDocumentoBra.Caption := NFormat(pBoletos[i].ReceberId);
      vForm.qrDataProcessamentoBra.Caption := DFormat(pBoletos[i].DataEmissao);
      vForm.qrNossoNumeroBra.Caption :=
        pBoletos[i].NossoNumero + '-' +
        pBoletos[i].DigitoVerificadorNossoNumero;
      vForm.qrValorDocumentoBra.Caption := NFormat(pBoletos[i].ValorDocumento);
      vForm.qrInstrucaoBra.Caption := pBoletos[i].Instrucao;
      vForm.qrPagador.Caption := pBoletos[i].cliente;
      vForm.qrCpfCnpjBra.Caption := pBoletos[i].CpfCnpj;
      vForm.qrLogradouroBra.Caption := pBoletos[i].Logradouro;
      vForm.qrCepBra.Caption := pBoletos[i].Cep;
      vForm.qrCidadeBra.Caption := pBoletos[i].Cidade + '/' + pBoletos[i].Estado;
      vForm.qrBeneficiarioAvalista.Caption := pBoletos[i].Beneficiario;
      vForm.qrCodigoBarraBra.BarText := pBoletos[i].CodigoBarras;

      vForm.qrBoletoSICOOB.Preview;
    end;
  finally
    vForm.Free;
  end;
end;

end.
