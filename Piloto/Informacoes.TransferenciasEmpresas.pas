unit Informacoes.TransferenciasEmpresas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.StdCtrls, EditLuka, Vcl.Mask, EditLukaData, Vcl.ComCtrls,
  StaticTextLuka, Vcl.Grids, GridLuka, _TransfProdutosEmpresasItens, _TransfProdutosEmpresas, _Sessao,
  _Biblioteca;

type
  TFormInformacoesTransferenciaEmpresas = class(TFormHerancaFinalizar)
    eTransferenciaId: TEditLuka;
    lb1: TLabel;
    lb2: TLabel;
    eEmpresaOrigem: TEditLuka;
    Label1: TLabel;
    eEmpresaDestino: TEditLuka;
    Label2: TLabel;
    eMotorista: TEditLuka;
    eUsuarioCadastro: TEditLuka;
    Label4: TLabel;
    Label5: TLabel;
    eDataHoraCadastro: TEditLukaData;
    eUsuarioBaixa: TEditLuka;
    Label6: TLabel;
    eDataHoraBaixa: TEditLukaData;
    Label7: TLabel;
    eUsuarioCancelamento: TEditLuka;
    Label8: TLabel;
    Label9: TLabel;
    eDataHoraCancelamento: TEditLukaData;
    eValorTransferencia: TEditLuka;
    Label3: TLabel;
    StaticTextLuka1: TStaticTextLuka;
    stStatus: TStaticText;
    sgItens: TGridLuka;
    StaticTextLuka2: TStaticTextLuka;
    stQuantidadeItensGrid: TStaticText;
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure Informar(const pTransferenciaId: Integer);

implementation

const
  (* Grid de Itens *)
  ciProduto      = 0;
  ciNome         = 1;
  ciMarca        = 2;
  ciPrecUnitario = 3;
  ciQuantidade   = 4;
  ciValorTotal   = 5;
  ciLote         = 6;
  ciLocalOrigem  = 7;

{$R *.dfm}

procedure Informar(const pTransferenciaId: Integer);
var
  i: Integer;
  vItens: TArray<RecTransfProdutosEmpresasItens>;
  vTransferencia: TArray<RecTransfProdutosEmpresas>;
  vForm: TFormInformacoesTransferenciaEmpresas;
begin
  if pTransferenciaId = 0 then
    Exit;

  vTransferencia := _TransfProdutosEmpresas.BuscarTransfProdutosEmpresas(Sessao.getConexaoBanco, 0, [pTransferenciaId]);
  if vTransferencia = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vItens := _TransfProdutosEmpresasItens.BuscarTransfProdutosEmpresasItens(Sessao.getConexaoBanco, 0, [pTransferenciaId]);
  if vItens = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vForm := TFormInformacoesTransferenciaEmpresas.Create(Application);
  vForm.ReadOnlyTodosObjetos(True);

  vForm.eTransferenciaId.AsInt           := vTransferencia[0].TransferenciaId;
  vForm.stStatus.Caption                 := vTransferencia[0].StatusAnalitico;
  vForm.eDataHoraCadastro.AsDataHora     := vTransferencia[0].DataHoraCadastro;
  vForm.eDataHoraBaixa.AsDataHora        := vTransferencia[0].DataHoraBaixa;
  vForm.eDataHoraCancelamento.AsDataHora := vTransferencia[0].DataHoraBaixa;
  vForm.eValorTransferencia.AsCurr       := vTransferencia[0].ValorTransferencia;

  vForm.eEmpresaOrigem.SetInformacao(vTransferencia[0].EmpresaOrigemId, vTransferencia[0].NomeEmpresaOrigem);
  vForm.eEmpresaDestino.SetInformacao(vTransferencia[0].EmpresaDestinoId, vTransferencia[0].NomeEmpresaDestino);
  vForm.eUsuarioBaixa.SetInformacao(vTransferencia[0].UsuarioBaixaId, vTransferencia[0].NomeUsuarioBaixa);
  vForm.eUsuarioCadastro.SetInformacao(vTransferencia[0].UsuarioCadastroId, vTransferencia[0].NomeUsuarioCadastro);
  vForm.eMotorista.SetInformacao(vTransferencia[0].MotoristaId, vTransferencia[0].NomeMotorista);
  vForm.eUsuarioCancelamento.SetInformacao(vTransferencia[0].UsuarioCancelamentoId, vTransferencia[0].NomeUsuarioCancelamento);

  for i := Low(vItens) to High(vItens) do begin

    vForm.sgItens.Cells[ciProduto, i + 1]       := NFormat(vItens[i].ProdutoId);
    vForm.sgItens.Cells[ciNome, i + 1]          := vItens[i].NomeProduto;
    vForm.sgItens.Cells[ciMarca, i + 1]         := NFormat(vItens[i].MarcaId) + ' - ' + vItens[i].NomeMarca;
    vForm.sgItens.Cells[ciLocalOrigem, i + 1]   := NFormat(vItens[i].LocalOrigemId) + ' - ' + vItens[i].NomeLocalOrigem;
    vForm.sgItens.Cells[ciLote, i + 1]          := vItens[i].Lote;
    vForm.sgItens.Cells[ciPrecUnitario, i + 1]  := NFormat(vItens[i].PrecoUnitario);
    vForm.sgItens.Cells[ciQuantidade, i + 1]    := NFormatEstoque(vItens[i].Quantidade);
    vForm.sgItens.Cells[ciValorTotal, i + 1]    := NFormat(vItens[i].ValorTotal);
  end;
  vForm.sgItens.SetLinhasGridPorTamanhoVetor( Length(vItens) );

  vForm.stQuantidadeItensGrid.Caption := NFormat(vForm.sgItens.RowCount -1);

  vForm.ShowModal;

  FreeAndNil(vForm);
end;

procedure TFormInformacoesTransferenciaEmpresas.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[
    ciProduto,
    ciPrecUnitario,
    ciQuantidade,
    ciValorTotal]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
