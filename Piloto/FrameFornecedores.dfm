inherited FrFornecedores: TFrFornecedores
  inherited CkMultiSelecao: TCheckBox
    Left = 121
    ExplicitLeft = 121
  end
  inherited PnTitulos: TPanel
    inherited lbNomePesquisa: TLabel
      Width = 66
      Caption = 'Fornecedores'
      ExplicitWidth = 66
    end
  end
  inherited ckSomenteAtivos: TCheckBox
    Left = 121
    ExplicitLeft = 121
  end
  object ckRevenda: TCheckBox [8]
    Left = 121
    Top = 56
    Width = 97
    Height = 17
    Caption = 'Revenda'
    Checked = True
    State = cbChecked
    TabOrder = 8
    Visible = False
  end
  object ckUsoConsumo: TCheckBox [9]
    Left = 225
    Top = 24
    Width = 97
    Height = 17
    Caption = 'Uso e consumo'
    Checked = True
    State = cbChecked
    TabOrder = 9
    Visible = False
  end
  object ckServico: TCheckBox [10]
    Left = 225
    Top = 40
    Width = 97
    Height = 17
    Caption = 'Servi'#231'o'
    Checked = True
    State = cbChecked
    TabOrder = 10
    Visible = False
  end
end
