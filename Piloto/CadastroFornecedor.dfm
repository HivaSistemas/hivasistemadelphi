inherited FormCadastroFornecedores: TFormCadastroFornecedores
  Caption = 'Cadastro de fornecedores'
  ClientHeight = 410
  ClientWidth = 984
  ExplicitTop = 0
  ExplicitWidth = 990
  ExplicitHeight = 439
  DesignSize = (
    984
    410)
  PixelsPerInch = 96
  TextHeight = 14
  inherited lbRazaoSocialNome: TLabel
    Font.Color = clRed
    ParentFont = False
  end
  inherited lbNomeFantasiaApelido: TLabel
    Left = 125
    Top = 46
    Font.Color = clRed
    ParentFont = False
    ExplicitLeft = 125
    ExplicitTop = 46
  end
  inherited lbCPF_CNPJ: TLabel
    Font.Color = clRed
    ParentFont = False
  end
  inherited Label2: TLabel
    Left = 805
    Top = 5
    ExplicitLeft = 805
    ExplicitTop = 5
  end
  inherited lb6: TLabel
    Left = 341
    Top = 46
    ExplicitLeft = 341
    ExplicitTop = 46
  end
  inherited lb7: TLabel
    Left = 495
    Top = 47
    ExplicitLeft = 495
    ExplicitTop = 47
  end
  inherited pnOpcoes: TPanel
    Height = 410
    TabOrder = 11
    ExplicitHeight = 410
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 934
    Top = 22
    TabOrder = 5
    ExplicitLeft = 934
    ExplicitTop = 22
  end
  inherited eRazaoSocial: TEditLuka
    Left = 327
    Width = 472
    ExplicitLeft = 327
    ExplicitWidth = 472
  end
  inherited eNomeFantasia: TEditLuka
    Left = 125
    Top = 61
    Width = 210
    TabOrder = 3
    ExplicitLeft = 125
    ExplicitTop = 61
    ExplicitWidth = 210
  end
  inherited rgTipoPessoa: TRadioGroupLuka
    Left = 624
    Top = 48
    TabOrder = 8
    ExplicitLeft = 624
    ExplicitTop = 48
  end
  object ckSuperSimples: TCheckBox [14]
    Left = 798
    Top = 66
    Width = 99
    Height = 17
    Anchors = [akTop, akRight]
    Caption = 'Super simples'
    TabOrder = 9
    Visible = False
    OnKeyDown = ProximoCampo
  end
  inherited pcDados: TPageControl
    Left = 125
    Top = 89
    Width = 859
    Height = 320
    ActivePage = tsCompras
    TabOrder = 10
    ExplicitLeft = 125
    ExplicitTop = 89
    ExplicitWidth = 859
    ExplicitHeight = 320
    inherited tsPrincipais: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 851
      ExplicitHeight = 291
      inherited lb9: TLabel
        Left = 247
        Top = 45
        ExplicitLeft = 247
        ExplicitTop = 45
      end
      inherited lb11: TLabel
        Left = 3
        ExplicitLeft = 3
      end
      inherited lb12: TLabel
        Left = 129
        ExplicitLeft = 129
      end
      inherited lb13: TLabel
        Left = 697
        Top = 131
        Visible = False
        ExplicitLeft = 697
        ExplicitTop = 131
      end
      inherited cbEstadoCivil: TComboBoxLuka
        TabOrder = 3
      end
      inherited rgSexo: TRadioGroupLuka
        TabOrder = 9
      end
      inherited eDataNascimento: TEditLukaData
        TabOrder = 2
      end
      inherited FrEndereco: TFrEndereco
        inherited lb8: TLabel
          Font.Charset = ANSI_CHARSET
        end
        inherited lb9: TLabel
          Font.Charset = ANSI_CHARSET
        end
        inherited lb10: TLabel
          Font.Charset = ANSI_CHARSET
        end
        inherited lb1: TLabel
          Font.Charset = ANSI_CHARSET
        end
        inherited lb2: TLabel
          Font.Charset = ANSI_CHARSET
        end
        inherited FrBairro: TFrBairros
          inherited PnTitulos: TPanel
            inherited lbNomePesquisa: TLabel
              Height = 15
              Font.Charset = ANSI_CHARSET
            end
          end
        end
      end
      inherited eEmail: TEditLuka
        Left = 247
        Top = 59
        Width = 275
        TabOrder = 6
        ExplicitLeft = 247
        ExplicitTop = 59
        ExplicitWidth = 275
      end
      inherited eTelefone: TEditTelefoneLuka
        Left = 3
        TabOrder = 4
        ExplicitLeft = 3
      end
      inherited eCelular: TEditTelefoneLuka
        Left = 129
        TabOrder = 5
        ExplicitLeft = 129
      end
      inherited eFax: TEditTelefoneLuka
        Left = 697
        Top = 145
        TabOrder = 10
        Visible = False
        ExplicitLeft = 697
        ExplicitTop = 145
      end
      inline FrGrupoFornecedor: TFrGruposFornecedores
        Left = 529
        Top = 42
        Width = 320
        Height = 41
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 7
        TabStop = True
        ExplicitLeft = 529
        ExplicitTop = 42
        ExplicitWidth = 320
        ExplicitHeight = 41
        inherited sgPesquisa: TGridLuka
          Width = 295
          Height = 24
          ExplicitWidth = 295
          ExplicitHeight = 24
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 112
            Caption = 'Grupo de fornecedor'
            ExplicitWidth = 112
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          Height = 25
          ExplicitLeft = 295
          ExplicitHeight = 25
        end
      end
    end
    inherited tsContatos: TTabSheet
      ExplicitWidth = 851
      ExplicitHeight = 291
      inherited FrTelefones: TFrCadastrosTelefones
        Width = 851
        Height = 291
        ExplicitWidth = 851
        ExplicitHeight = 291
        inherited sgTelefones: TGridLuka
          Top = 37
          Width = 851
          ExplicitTop = 37
          ExplicitWidth = 851
        end
      end
    end
    inherited tsEnderecos: TTabSheet
      ExplicitWidth = 851
      ExplicitHeight = 291
      inherited FrDiversosEnderecos: TFrDiversosEnderecos
        Width = 851
        Height = 291
        ExplicitWidth = 851
        ExplicitHeight = 291
        inherited sgOutrosEnderecos: TGridLuka
          Top = 84
          Width = 851
          ExplicitTop = 84
          ExplicitWidth = 851
        end
        inherited FrBairro: TFrBairros
          inherited PnTitulos: TPanel
            inherited lbNomePesquisa: TLabel
              Height = 15
            end
            inherited pnSuprimir: TPanel
              inherited ckSuprimir: TCheckBox
                Visible = False
              end
            end
          end
        end
      end
    end
    inherited tsObservacoes: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 851
      ExplicitHeight = 291
      inherited eObservacoes: TMemo
        Width = 851
        Height = 291
        ExplicitWidth = 851
        ExplicitHeight = 291
      end
    end
    object tsCompras: TTabSheet
      Caption = 'Compras'
      ImageIndex = 4
      object rgTipoFornecedor: TRadioGroupLuka
        Left = 3
        Top = 192
        Width = 318
        Height = 59
        Caption = ' Tipo de fornecedor '
        Columns = 2
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        Items.Strings = (
          'Revenda'
          'Distribuidora'
          'Ind'#250'stria'
          'Outro')
        ParentFont = False
        TabOrder = 4
        TabStop = True
        Valores.Strings = (
          'R'
          'D'
          'I'
          'O')
      end
      inline FrPlanoFinanceiroRevenda: TFrPlanosFinanceiros
        Left = 3
        Top = 53
        Width = 297
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 3
        ExplicitTop = 53
        ExplicitWidth = 297
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 272
          Height = 23
          ExplicitWidth = 272
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 297
          ExplicitWidth = 297
          inherited lbNomePesquisa: TLabel
            Width = 215
            Caption = 'Plano financeiro entradas para revenda'
            ExplicitWidth = 215
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 192
            ExplicitLeft = 192
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 272
          Height = 24
          ExplicitLeft = 272
          ExplicitHeight = 24
        end
      end
      inline FrPlanoFinanceiroUsoConsumo: TFrPlanosFinanceiros
        Left = 3
        Top = 98
        Width = 297
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 3
        ExplicitTop = 98
        ExplicitWidth = 297
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 272
          Height = 23
          ExplicitWidth = 272
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 297
          ExplicitWidth = 297
          inherited lbNomePesquisa: TLabel
            Width = 225
            Caption = 'Plano financeiro entradas uso e consumo'
            ExplicitWidth = 225
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 192
            ExplicitLeft = 192
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 272
          Height = 24
          ExplicitLeft = 272
          ExplicitHeight = 24
        end
      end
      object ckFornecedorRevenda: TCheckBoxLuka
        Left = 3
        Top = 0
        Width = 241
        Height = 17
        Caption = '1 - Fornecedor para revenda'
        TabOrder = 0
        OnClick = ckFornecedorRevendaClick
        CheckedStr = 'N'
      end
      object ckFornecedorUsoConsumo: TCheckBoxLuka
        Left = 3
        Top = 17
        Width = 241
        Height = 17
        Caption = '2 - Fornecedor para uso e consumo'
        TabOrder = 1
        OnClick = ckFornecedorUsoConsumoClick
        CheckedStr = 'N'
      end
      object rgTipoRateioFrete: TRadioGroupLuka
        Left = 327
        Top = 194
        Width = 324
        Height = 59
        Caption = ' Tipo de rateio do frete  '
        Columns = 2
        Items.Strings = (
          'Peso'
          'Quantidade'
          'Valor'
          'Manual')
        TabOrder = 5
        TabStop = True
        Valores.Strings = (
          'P'
          'Q'
          'V'
          'M')
      end
      object ckServico: TCheckBoxLuka
        Left = 3
        Top = 34
        Width = 241
        Height = 17
        Caption = '3 - Fornecedor de servi'#231'o'
        TabOrder = 6
        OnClick = ckServicoClick
        CheckedStr = 'N'
      end
      inline FrPlanoFinanceiroServico: TFrPlanosFinanceiros
        Left = 3
        Top = 146
        Width = 297
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 7
        TabStop = True
        ExplicitLeft = 3
        ExplicitTop = 146
        ExplicitWidth = 297
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 272
          Height = 23
          ExplicitWidth = 272
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 297
          ExplicitWidth = 297
          inherited lbNomePesquisa: TLabel
            Width = 181
            Caption = 'Plano financeiro entradas servi'#231'o'
            ExplicitWidth = 181
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 192
            ExplicitLeft = 192
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 272
          Height = 24
          ExplicitLeft = 272
          ExplicitHeight = 24
        end
      end
    end
  end
  inherited eDataCadastro: TEditLukaData
    Left = 805
    Top = 20
    TabOrder = 4
    ExplicitLeft = 805
    ExplicitTop = 20
  end
  inherited eInscricaoEstadual: TEditLuka
    Left = 341
    Top = 61
    TabOrder = 6
    ExplicitLeft = 341
    ExplicitTop = 61
  end
  inherited eCNAE: TMaskEdit
    Left = 495
    Top = 61
    TabOrder = 7
    ExplicitLeft = 495
    ExplicitTop = 61
  end
end
