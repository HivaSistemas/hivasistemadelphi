inherited FormCadastroCidades: TFormCadastroCidades
  Caption = 'Cadastro de cidades'
  ClientHeight = 200
  ClientWidth = 516
  ExplicitWidth = 522
  ExplicitHeight = 229
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 210
    Top = 5
    Width = 32
    Height = 14
    Caption = 'Nome'
  end
  object lb2: TLabel [1]
    Left = 406
    Top = 51
    Width = 65
    Height = 14
    Caption = 'C'#243'digo IBGE'
  end
  object lbl1: TLabel [3]
    Left = 126
    Top = 99
    Width = 140
    Height = 14
    Caption = 'Tipo bloq. venda entregar'
  end
  object Label2: TLabel [4]
    Left = 126
    Top = 143
    Width = 110
    Height = 14
    Caption = 'C'#225'lculo de frete(KM)'
  end
  inherited pnOpcoes: TPanel
    Height = 200
    ExplicitHeight = 182
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 467
    Top = 0
    TabOrder = 6
    Visible = False
    ExplicitLeft = 467
    ExplicitTop = 0
  end
  object eNome: TEditLuka
    Left = 210
    Top = 19
    Width = 303
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 2
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  inline FrEstado: TFrEstados
    Left = 126
    Top = 50
    Width = 272
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 50
    ExplicitWidth = 272
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 247
      Height = 23
      ExplicitWidth = 247
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 272
      ExplicitWidth = 272
      inherited lbNomePesquisa: TLabel
        Width = 37
        Height = 14
        Caption = 'Estado'
        ExplicitWidth = 37
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 167
        ExplicitLeft = 167
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 247
      Height = 24
      ExplicitLeft = 247
      ExplicitHeight = 24
    end
  end
  object eCodigoMunicipioIBGE: TEditLuka
    Left = 406
    Top = 65
    Width = 107
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 8
    TabOrder = 4
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object cbTipoBloqueioVendaEntregar: TComboBoxLuka
    Left = 126
    Top = 113
    Width = 147
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    ItemIndex = 2
    TabOrder = 5
    Text = 'N'#227'o permitir venda'
    Items.Strings = (
      'N'#227'o bloquear'
      'Sempre bloquear'
      'N'#227'o permitir venda')
    Valores.Strings = (
      'N'
      'S'
      'V')
    AsInt = 0
    AsString = 'V'
  end
  object eValorCalculoFreteKM: TEditLuka
    Left = 126
    Top = 157
    Width = 147
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 8
    TabOrder = 7
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
