inherited FormBaixarTitulosReceber: TFormBaixarTitulosReceber
  Caption = 'Baixa de t'#237'tulos a receber'
  ClientHeight = 526
  ClientWidth = 804
  OnShow = FormShow
  ExplicitWidth = 810
  ExplicitHeight = 555
  PixelsPerInch = 96
  TextHeight = 14
  object lb8: TLabel [0]
    Left = 4
    Top = 137
    Width = 80
    Height = 14
    Caption = 'Data de pagto.'
  end
  object lb22: TLabel [1]
    Left = 93
    Top = 137
    Width = 89
    Height = 14
    Caption = 'Valor dos t'#237'tulos'
  end
  object lb23: TLabel [2]
    Left = 301
    Top = 137
    Width = 58
    Height = 14
    Caption = 'Valor juros'
  end
  object lb1: TLabel [3]
    Left = 616
    Top = 137
    Width = 109
    Height = 14
    Caption = 'Valor l'#237'q. dos titulos'
  end
  object lb2: TLabel [4]
    Left = 313
    Top = 176
    Width = 69
    Height = 14
    Caption = 'Observa'#231#245'es'
  end
  object lb3: TLabel [5]
    Left = 405
    Top = 137
    Width = 85
    Height = 14
    Caption = 'Valor reten'#231#245'es'
  end
  object lb4: TLabel [6]
    Left = 197
    Top = 137
    Width = 63
    Height = 14
    Caption = 'Valor multa'
  end
  object lb5: TLabel [7]
    Left = 511
    Top = 137
    Width = 88
    Height = 14
    Caption = 'Valor adiantado'
  end
  inherited pnOpcoes: TPanel
    Top = 489
    Width = 804
    TabOrder = 10
    ExplicitTop = 489
    ExplicitWidth = 804
  end
  object eDataPagamento: TEditLukaData
    Left = 4
    Top = 151
    Width = 85
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1; '
    MaxLength = 10
    TabOrder = 0
    Text = '  /  /    '
    OnExit = eDataPagamentoExit
    OnKeyDown = ProximoCampo
  end
  object eValorTitulos: TEditLuka
    Left = 93
    Top = 151
    Width = 100
    Height = 22
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 1
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorJuros: TEditLuka
    Left = 301
    Top = 151
    Width = 100
    Height = 22
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 3
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorTotal: TEditLuka
    Left = 616
    Top = 151
    Width = 125
    Height = 22
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 6
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eObservacoes: TEditLuka
    Left = 313
    Top = 190
    Width = 488
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 7
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object sgTitulos: TGridLuka
    Left = 3
    Top = 2
    Width = 798
    Height = 135
    Align = alCustom
    ColCount = 11
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
    TabOrder = 9
    OnDblClick = sgTitulosDblClick
    OnDrawCell = sgTitulosDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Documento'
      'Cliente'
      'Parcela'
      'Valor'
      'Multa'
      'Juros'
      'Reten'#231#227'o'
      'Valor l'#237'quido'
      'Data Vencto.'
      'Dias atraso'
      'Tipo de cobran'#231'a')
    OnGetCellColor = sgTitulosGetCellColor
    Grid3D = False
    RealColCount = 15
    OrdenarOnClick = True
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      74
      153
      48
      62
      61
      57
      69
      87
      83
      75
      186)
  end
  inline FrPagamento: TFrPagamentoFinanceiro
    Left = 4
    Top = 176
    Width = 303
    Height = 309
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 8
    TabStop = True
    ExplicitLeft = 4
    ExplicitTop = 176
    ExplicitHeight = 309
    inherited grpFechamento: TGroupBox
      Height = 309
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitHeight = 309
      inherited lbllb11: TLabel
        Top = 125
        ExplicitTop = 125
      end
      inherited lbllb12: TLabel
        Top = 145
        ExplicitTop = 145
      end
      inherited lbllb13: TLabel
        Left = 32
        Top = 165
        Width = 78
        ExplicitLeft = 32
        ExplicitTop = 165
        ExplicitWidth = 78
      end
      inherited lbllb14: TLabel
        Top = 227
        ExplicitTop = 227
      end
      inherited lbllb15: TLabel
        Top = 247
        ExplicitTop = 247
      end
      inherited lbllb10: TLabel
        Top = 268
        ExplicitTop = 268
      end
      inherited lbllb21: TLabel
        Top = 61
        ExplicitTop = 61
      end
      inherited lbllb7: TLabel
        Top = 61
        Width = 9
        Height = 14
        ExplicitTop = 61
        ExplicitWidth = 9
        ExplicitHeight = 14
      end
      inherited lbllb19: TLabel
        Top = 81
        ExplicitTop = 81
      end
      inherited lbllb1: TLabel
        Top = 41
        ExplicitTop = 41
      end
      inherited lb4: TLabel
        Top = 185
        ExplicitTop = 185
      end
      inherited lb1: TLabel
        Top = 21
        ExplicitTop = 21
      end
      inherited sbBuscarDadosDinheiro: TImage
        Top = 121
        OnClick = FrPagamentosbBuscarDadosDinheiroClick
        ExplicitTop = 121
      end
      inherited sbBuscarDadosCheques: TImage
        Top = 141
        OnClick = FrPagamentosbBuscarDadosChequesClick
        ExplicitTop = 141
      end
      inherited sbBuscarDadosCartoesDebito: TImage
        Top = 161
        ExplicitTop = 161
      end
      inherited sbBuscarDadosCartoesCredito: TImage
        Top = 181
        ExplicitTop = 181
      end
      inherited sbBuscarDadosCobranca: TImage
        Top = 224
        ExplicitTop = 224
      end
      inherited sbBuscarDadosCreditos: TImage
        Top = 244
        ExplicitTop = 244
      end
      inherited Label1: TLabel
        Top = 207
        ExplicitTop = 207
      end
      inherited sbBuscarDadosPix: TImage
        Top = 203
        OnClick = FrPagamentosbBuscarDadosPixClick
        ExplicitTop = 203
      end
      inherited eValorDinheiro: TEditLuka
        Top = 118
        Height = 22
        ExplicitTop = 118
        ExplicitHeight = 22
      end
      inherited eValorCheque: TEditLuka
        Top = 138
        Height = 22
        ExplicitTop = 138
        ExplicitHeight = 22
      end
      inherited eValorCartaoDebito: TEditLuka
        Top = 158
        Height = 22
        ExplicitTop = 158
        ExplicitHeight = 22
      end
      inherited eValorCobranca: TEditLuka
        Top = 220
        Height = 22
        ExplicitTop = 220
        ExplicitHeight = 22
      end
      inherited eValorCredito: TEditLuka
        Top = 239
        Height = 22
        ExplicitTop = 239
        ExplicitHeight = 22
      end
      inherited stPagamento: TStaticText
        Top = 100
        ExplicitTop = 100
      end
      inherited eValorDiferencaPagamentos: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorDesconto: TEditLuka
        Top = 54
        Height = 22
        ExplicitTop = 54
        ExplicitHeight = 22
      end
      inherited ePercentualDesconto: TEditLuka
        Left = 193
        Top = 54
        Height = 22
        ExplicitLeft = 193
        ExplicitTop = 54
        ExplicitHeight = 22
      end
      inherited eValorTotalASerPago: TEditLuka
        Top = 75
        ExplicitTop = 75
      end
      inherited eValorJuros: TEditLuka
        Top = 33
        Height = 22
        ExplicitTop = 33
        ExplicitHeight = 22
      end
      inherited eValorCartaoCredito: TEditLuka
        Top = 178
        Height = 22
        OnChange = nil
        ExplicitTop = 178
        ExplicitHeight = 22
      end
      inherited stValorTotal: TStaticText
        Top = 100
        ExplicitTop = 100
      end
      inherited stDinheiroDefinido: TStaticText
        Top = 122
        ExplicitTop = 122
      end
      inherited stChequeDefinido: TStaticText
        Top = 142
        ExplicitTop = 142
      end
      inherited stCartaoDebitoDefinido: TStaticText
        Top = 162
        ExplicitTop = 162
      end
      inherited stCobrancaDefinido: TStaticText
        Top = 224
        ExplicitTop = 224
      end
      inherited stCreditoDefinido: TStaticText
        Top = 244
        ExplicitTop = 244
      end
      inherited stCartaoCreditoDefinido: TStaticText
        Top = 182
        ExplicitTop = 182
      end
      inherited eValorMulta: TEditLuka
        Top = 12
        Height = 22
        ExplicitTop = 12
        ExplicitHeight = 22
      end
      inherited eValorPix: TEditLuka
        Top = 199
        Height = 22
        ExplicitTop = 199
        ExplicitHeight = 22
      end
      inherited stPixDefinido: TStaticText
        Top = 203
        ExplicitTop = 203
      end
    end
  end
  object eValorRetencao: TEditLuka
    Left = 405
    Top = 151
    Width = 100
    Height = 22
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 4
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorMulta: TEditLuka
    Left = 197
    Top = 151
    Width = 100
    Height = 22
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 2
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorAdiantado: TEditLuka
    Left = 511
    Top = 151
    Width = 100
    Height = 22
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 5
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object ckGerarCreditoTroco: TCheckBox
    Left = 116
    Top = 465
    Width = 140
    Height = 17
    Caption = 'Gerar cr'#233'dito do troco'
    Checked = True
    State = cbChecked
    TabOrder = 11
    Visible = False
  end
  object frxBaixas: TfrxDBDataset
    UserName = 'frxdstBaixas'
    CloseDataSource = False
    FieldAliases.Strings = (
      'BAIXA_ID=BAIXA_ID'
      'EMPRESA_ID=EMPRESA_ID'
      'VALOR_TITULOS=VALOR_TITULOS'
      'VALOR_LIQUIDO=VALOR_LIQUIDO'
      'VALOR_JUROS=VALOR_JUROS'
      'VALOR_DESCONTO=VALOR_DESCONTO'
      'VALOR_DINHEIRO=VALOR_DINHEIRO'
      'VALOR_CHEQUE=VALOR_CHEQUE'
      'VALOR_CARTAO_DEBITO=VALOR_CARTAO_DEBITO'
      'VALOR_COBRANCA=VALOR_COBRANCA'
      'VALOR_CREDITO=VALOR_CREDITO'
      'VALOR_TROCO=VALOR_TROCO'
      'DATA_HORA_BAIXA=DATA_HORA_BAIXA'
      'OBSERVACOES=OBSERVACOES'
      'CADASTRO_ID=CADASTRO_ID'
      'VALOR_CARTAO_CREDITO=VALOR_CARTAO_CREDITO'
      'VALOR_MULTA=VALOR_MULTA'
      'VALOR_PIX=VALOR_PIX'
      'CLIENTE=CLIENTE'
      'CPF_CNPJ=CPF_CNPJ'
      'VALOR_ADIANTADO=VALOR_ADIANTADO'
      'VALOR_RETENCAO=VALOR_RETENCAO'
      'TIPO_BAIXA=TIPO_BAIXA')
    DataSet = qBaixas
    BCDToCurrency = False
    Left = 542
    Top = 305
  end
  object frxDBReceber: TfrxDBDataset
    UserName = 'frxdstReceber'
    CloseDataSource = False
    FieldAliases.Strings = (
      'RECEBER_ID=RECEBER_ID'
      'EMPRESA_ID=EMPRESA_ID'
      'ORIGEM=ORIGEM'
      'ORIGEM_ID=ORIGEM_ID'
      'DOCUMENTO=DOCUMENTO'
      'DATA_CADASTRO=DATA_CADASTRO'
      'DATA_EMISSAO=DATA_EMISSAO'
      'DATA_VENCIMENTO=DATA_VENCIMENTO'
      'VALOR_DOCUMENTO=VALOR_DOCUMENTO'
      'PARCELA=PARCELA'
      'NUMERO_PARCELAS=NUMERO_PARCELAS'
      'OBSERVACOES=OBSERVACOES'
      'CLIENTE=CLIENTE'
      'NOME_FANTASIA=NOME_FANTASIA'
      'VALOR_RETENCAO=VALOR_RETENCAO'
      'BAIXA_ID=BAIXA_ID'
      'CPF_CNPJ=CPF_CNPJ'
      'VALOR_ADIANTADO=VALOR_ADIANTADO'
      'VALOR_DESCONTO=VALOR_DESCONTO'
      'VALOR_JUROS=VALOR_JUROS'
      'VALOR_MULTA=VALOR_MULTA'
      'VALOR_LIQUIDO=VALOR_LIQUIDO')
    DataSet = qReceber
    BCDToCurrency = False
    Left = 616
    Top = 306
  end
  object qBaixas: TOraQuery
    SQL.Strings = (
      'select'
      '  BAI.BAIXA_ID,'
      '  BAI.EMPRESA_ID,'
      '  BAI.VALOR_ADIANTADO,'
      '  BAI.VALOR_RETENCAO,'
      '  BAI.VALOR_TITULOS,'
      '  BAI.VALOR_LIQUIDO,'
      '  BAI.VALOR_JUROS,'
      '  BAI.VALOR_DESCONTO,'
      '  BAI.VALOR_DINHEIRO,'
      '  BAI.VALOR_CHEQUE,'
      '  BAI.VALOR_CARTAO_DEBITO,'
      '  BAI.VALOR_COBRANCA,'
      '  BAI.VALOR_CREDITO,'
      '  BAI.VALOR_TROCO,'
      '  BAI.DATA_HORA_BAIXA,'
      '  BAI.OBSERVACOES,'
      '  BAI.CADASTRO_ID,'
      '  CAD.CADASTRO_ID || '#39' -  '#39' || CAD.NOME_FANTASIA as CLIENTE,'
      '  BAI.VALOR_CARTAO_CREDITO,'
      '  BAI.VALOR_MULTA,'
      '  BAI.VALOR_PIX,'
      '  CAD.CPF_CNPJ,'
      
        '  case when (select count(*) from CONTAS_RECEBER_BAIXAS_ITENS CR' +
        'B where BAIXA_ID = BAI.BAIXA_ID) in (0, 1) then '#39'Individual'#39' els' +
        'e '#39'Agrupada'#39' end as TIPO_BAIXA'
      'from '
      '  CONTAS_RECEBER_BAIXAS BAI'
      ''
      'inner join CADASTROS CAD'
      'on CAD.CADASTRO_ID = BAI.CADASTRO_ID'
      ''
      ''
      'where 1 = 1')
    Left = 544
    Top = 352
    object qBaixasBAIXA_ID: TFloatField
      FieldName = 'BAIXA_ID'
      Required = True
    end
    object qBaixasEMPRESA_ID: TIntegerField
      FieldName = 'EMPRESA_ID'
      Required = True
    end
    object qBaixasVALOR_TITULOS: TFloatField
      FieldName = 'VALOR_TITULOS'
      Required = True
    end
    object qBaixasVALOR_LIQUIDO: TFloatField
      FieldName = 'VALOR_LIQUIDO'
      Required = True
    end
    object qBaixasVALOR_JUROS: TFloatField
      FieldName = 'VALOR_JUROS'
      Required = True
    end
    object qBaixasVALOR_DESCONTO: TFloatField
      FieldName = 'VALOR_DESCONTO'
      Required = True
    end
    object qBaixasVALOR_DINHEIRO: TFloatField
      FieldName = 'VALOR_DINHEIRO'
      Required = True
    end
    object qBaixasVALOR_CHEQUE: TFloatField
      FieldName = 'VALOR_CHEQUE'
      Required = True
    end
    object qBaixasVALOR_CARTAO_DEBITO: TFloatField
      FieldName = 'VALOR_CARTAO_DEBITO'
      Required = True
    end
    object qBaixasVALOR_COBRANCA: TFloatField
      FieldName = 'VALOR_COBRANCA'
      Required = True
    end
    object qBaixasVALOR_CREDITO: TFloatField
      FieldName = 'VALOR_CREDITO'
      Required = True
    end
    object qBaixasVALOR_TROCO: TFloatField
      FieldName = 'VALOR_TROCO'
      Required = True
    end
    object qBaixasDATA_HORA_BAIXA: TDateTimeField
      FieldName = 'DATA_HORA_BAIXA'
      Required = True
    end
    object qBaixasOBSERVACOES: TStringField
      FieldName = 'OBSERVACOES'
      Size = 200
    end
    object qBaixasCADASTRO_ID: TFloatField
      FieldName = 'CADASTRO_ID'
    end
    object qBaixasVALOR_CARTAO_CREDITO: TFloatField
      FieldName = 'VALOR_CARTAO_CREDITO'
      Required = True
    end
    object qBaixasVALOR_MULTA: TFloatField
      FieldName = 'VALOR_MULTA'
      Required = True
    end
    object qBaixasVALOR_PIX: TFloatField
      FieldName = 'VALOR_PIX'
      Required = True
    end
    object qBaixasCLIENTE: TStringField
      FieldName = 'CLIENTE'
      Size = 144
    end
    object qBaixasCPF_CNPJ: TStringField
      FieldName = 'CPF_CNPJ'
      Size = 19
    end
    object qBaixasVALOR_ADIANTADO: TFloatField
      FieldName = 'VALOR_ADIANTADO'
      Required = True
    end
    object qBaixasVALOR_RETENCAO: TFloatField
      FieldName = 'VALOR_RETENCAO'
      Required = True
    end
    object qBaixasTIPO_BAIXA: TStringField
      FieldName = 'TIPO_BAIXA'
      Size = 10
    end
  end
  object qReceber: TOraQuery
    SQL.Strings = (
      'select'
      '  RECEBER_ID,'
      '  EMPRESA_ID,'
      '  ORIGEM,'
      '  ORIGEM_ID,'
      '  DOCUMENTO,'
      '  DATA_CADASTRO,'
      '  DATA_EMISSAO,'
      '  DATA_VENCIMENTO,'
      '  VALOR_DOCUMENTO,'
      '  PARCELA,'
      '  NUMERO_PARCELAS,'
      '  OBSERVACOES,'
      '  CLIENTE,'
      '  NOME_FANTASIA,'
      '  VALOR_RETENCAO,'
      '  BAIXA_ID,'
      '  CPF_CNPJ,'
      '  VALOR_ADIANTADO,'
      '  VALOR_DESCONTO,'
      '  VALOR_JUROS,'
      '  VALOR_MULTA,'
      '  VALOR_LIQUIDO'
      'from'
      '  VW_CONTAS_RECEBER_RELATORIO REC'
      ''
      'where 1 = 1')
    MasterSource = dsBaixas
    MasterFields = 'BAIXA_ID'
    DetailFields = 'BAIXA_ID'
    Left = 616
    Top = 353
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'BAIXA_ID'
        Value = nil
      end>
    object qReceberRECEBER_ID: TFloatField
      FieldName = 'RECEBER_ID'
      Required = True
    end
    object qReceberEMPRESA_ID: TIntegerField
      FieldName = 'EMPRESA_ID'
      Required = True
    end
    object qReceberORIGEM: TStringField
      FieldName = 'ORIGEM'
      Size = 9
    end
    object qReceberORIGEM_ID: TFloatField
      FieldName = 'ORIGEM_ID'
    end
    object qReceberDOCUMENTO: TStringField
      FieldName = 'DOCUMENTO'
      Required = True
      Size = 25
    end
    object qReceberDATA_CADASTRO: TDateTimeField
      FieldName = 'DATA_CADASTRO'
      Required = True
    end
    object qReceberDATA_EMISSAO: TDateTimeField
      FieldName = 'DATA_EMISSAO'
    end
    object qReceberDATA_VENCIMENTO: TDateTimeField
      FieldName = 'DATA_VENCIMENTO'
      Required = True
    end
    object qReceberVALOR_DOCUMENTO: TFloatField
      FieldName = 'VALOR_DOCUMENTO'
      Required = True
    end
    object qReceberPARCELA: TIntegerField
      FieldName = 'PARCELA'
      Required = True
    end
    object qReceberNUMERO_PARCELAS: TIntegerField
      FieldName = 'NUMERO_PARCELAS'
      Required = True
    end
    object qReceberOBSERVACOES: TStringField
      FieldName = 'OBSERVACOES'
      Size = 200
    end
    object qReceberCLIENTE: TStringField
      FieldName = 'CLIENTE'
      Size = 144
    end
    object qReceberNOME_FANTASIA: TStringField
      FieldName = 'NOME_FANTASIA'
      Required = True
      Size = 100
    end
    object qReceberVALOR_RETENCAO: TFloatField
      FieldName = 'VALOR_RETENCAO'
      Required = True
    end
    object qReceberBAIXA_ID: TFloatField
      FieldName = 'BAIXA_ID'
      Required = True
    end
    object qReceberCPF_CNPJ: TStringField
      FieldName = 'CPF_CNPJ'
      Required = True
      Size = 19
    end
    object qReceberVALOR_ADIANTADO: TFloatField
      FieldName = 'VALOR_ADIANTADO'
      Required = True
    end
    object qReceberVALOR_DESCONTO: TFloatField
      FieldName = 'VALOR_DESCONTO'
      Required = True
    end
    object qReceberVALOR_JUROS: TFloatField
      FieldName = 'VALOR_JUROS'
      Required = True
    end
    object qReceberVALOR_MULTA: TFloatField
      FieldName = 'VALOR_MULTA'
      Required = True
    end
    object qReceberVALOR_LIQUIDO: TFloatField
      FieldName = 'VALOR_LIQUIDO'
    end
  end
  object dsBaixas: TDataSource
    DataSet = qBaixas
    Left = 542
    Top = 409
  end
  object qCreditoGerado: TOraQuery
    SQL.Strings = (
      
        'SELECT VALOR_DOCUMENTO, PAGAR_ID FROM CONTAS_PAGAR WHERE BAIXA_O' +
        'RIGEM_ID IN('
      '  SELECT'
      '    BAIXA_ID'
      '  FROM CONTAS_PAGAR_BAIXAS'
      '  WHERE BAIXA_RECEBER_ORIGEM_ID = :BAIXA_RECEBER_ORIGEM_ID'
      ')')
    Left = 616
    Top = 408
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'BAIXA_RECEBER_ORIGEM_ID'
        Value = nil
      end>
    object qCreditoGeradoVALOR_DOCUMENTO: TFloatField
      FieldName = 'VALOR_DOCUMENTO'
      Required = True
    end
    object qCreditoGeradoPAGAR_ID: TFloatField
      FieldName = 'PAGAR_ID'
      Required = True
    end
  end
  object frxReport1: TfrxReport
    Version = '5.1.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44043.704685613400000000
    ReportOptions.LastChange = 44655.948154490740000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 584
    Top = 255
    Datasets = <
      item
        DataSet = frxBaixas
        DataSetName = 'frxdstBaixas'
      end
      item
        DataSet = dmRelatorio.dstEmpresa
        DataSetName = 'frxdstEmpresa'
      end
      item
        DataSet = frxDBReceber
        DataSetName = 'frxdstReceber'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 68.031540000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000001000
          Width = 68.031540000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 26.456710000000000000
          Top = 34.015770000000010000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 15.118120000000000000
          Top = 49.133889999999990000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 340.157700000000000000
          Top = 3.779530000000001000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 321.260050000000000000
          Top = 34.015770000000010000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade/UF:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 340.157700000000000000
          Top = 49.133889999999990000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 82.708720000000000000
          Top = 3.779530000000001000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 383.512060000000000000
          Top = 3.779530000000001000
          Width = 200.315090000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."CNPJ"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 84.929190000000000000
          Top = 18.897650000000000000
          Width = 551.811380000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            
              '[frxdstEmpresa."LOGRADOURO"] [frxdstEmpresa."COMPLEMENTO"] n'#176'[fr' +
              'xdstEmpresa."NUMERO"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 82.708720000000000000
          Top = 34.015770000000010000
          Width = 207.874150000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_BAIRRO"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 383.512060000000000000
          Top = 34.015770000000010000
          Width = 185.196970000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_CIDADE"] / [frxdstEmpresa."ESTADO_ID"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 82.708720000000000000
          Top = 49.133889999999990000
          Width = 75.590600000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 383.512060000000000000
          Top = 49.133889999999990000
          Width = 260.787570000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."E_MAIL"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 545.252320000000000000
          Top = 3.779530000000001000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Emitido em [Date] '#224's [Time]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 545.252320000000000000
          Top = 18.897650000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pag. [Page] de [TotalPages#]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 128.504020000000000000
        Top = 147.401670000000000000
        Width = 718.110700000000000000
        DataSet = frxBaixas
        DataSetName = 'frxdstBaixas'
        RowCount = 0
        object Shape3: TfrxShapeView
          Top = 98.267779999999990000
          Width = 718.110700000000000000
          Height = 30.236240000000000000
          Fill.BackColor = cl3DLight
        end
        object Shape1: TfrxShapeView
          Left = 0.559060000000000000
          Width = 718.110700000000000000
          Height = 26.456710000000000000
          Fill.BackColor = cl3DLight
        end
        object Memo5: TfrxMemoView
          Left = 44.133890000000000000
          Top = 37.795300000000000000
          Width = 79.370130000000000000
          Height = 15.118120000000000000
          DataField = 'BAIXA_ID'
          DataSet = frxBaixas
          DataSetName = 'frxdstBaixas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstBaixas."BAIXA_ID"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 3.779530000000000000
          Top = 37.795300000000000000
          Width = 37.795300000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Baixa:')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 221.771800000000000000
          Top = 37.795300000000000000
          Width = 102.047310000000000000
          Height = 15.118120000000000000
          DataField = 'DATA_HORA_BAIXA'
          DataSet = frxBaixas
          DataSetName = 'frxdstBaixas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstBaixas."DATA_HORA_BAIXA"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 132.283550000000000000
          Top = 37.795300000000000000
          Width = 86.929190000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Data da baixa:')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 4.000000000000000000
          Top = 56.692949999999990000
          Width = 86.929190000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total t'#237'tulos')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 3.779530000000000000
          Top = 75.590600000000000000
          Width = 86.929190000000000000
          Height = 15.118120000000000000
          DataField = 'VALOR_TITULOS'
          DataSet = frxBaixas
          DataSetName = 'frxdstBaixas'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstBaixas."VALOR_TITULOS"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 94.488250000000000000
          Top = 56.692949999999990000
          Width = 75.590548740000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Multa')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 94.267780000000000000
          Top = 75.590600000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataField = 'VALOR_MULTA'
          DataSet = frxBaixas
          DataSetName = 'frxdstBaixas'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstBaixas."VALOR_MULTA"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 173.858380000000000000
          Top = 56.692949999999990000
          Width = 79.370078740000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Juros')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 173.637910000000000000
          Top = 75.590600000000000000
          Width = 79.370130000000000000
          Height = 15.118120000000000000
          DataField = 'VALOR_JUROS'
          DataSet = frxBaixas
          DataSetName = 'frxdstBaixas'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstBaixas."VALOR_JUROS"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 257.008040000000000000
          Top = 56.692949999999990000
          Width = 102.047310000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Vlr. adiantado')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 256.787570000000000000
          Top = 75.590600000000000000
          Width = 102.047310000000000000
          Height = 15.118120000000000000
          DataField = 'VALOR_ADIANTADO'
          DataSet = frxBaixas
          DataSetName = 'frxdstBaixas'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstBaixas."VALOR_ADIANTADO"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 363.055350000000000000
          Top = 56.692949999999990000
          Width = 79.370130000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Reten'#231#227'o')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 362.834880000000000000
          Top = 75.590600000000000000
          Width = 79.370130000000000000
          Height = 15.118120000000000000
          DataField = 'VALOR_RETENCAO'
          DataSet = frxBaixas
          DataSetName = 'frxdstBaixas'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstBaixas."VALOR_RETENCAO"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 446.205010000000000000
          Top = 56.692949999999990000
          Width = 90.708720000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Desconto')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 445.984540000000000000
          Top = 75.590600000000000000
          Width = 90.708720000000000000
          Height = 15.118120000000000000
          DataField = 'VALOR_DESCONTO'
          DataSet = frxBaixas
          DataSetName = 'frxdstBaixas'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstBaixas."VALOR_DESCONTO"]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 540.693260000000000000
          Top = 56.692949999999990000
          Width = 124.724490000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total a ser pago')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 540.472790000000000000
          Top = 75.590600000000000000
          Width = 124.724490000000000000
          Height = 15.118120000000000000
          DataField = 'VALOR_LIQUIDO'
          DataSet = frxBaixas
          DataSetName = 'frxdstBaixas'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstBaixas."VALOR_LIQUIDO"]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = -0.779530000000000000
          Top = 102.047310000000000000
          Width = 718.110700000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'T'#205'TULOS BAIXADOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Left = 1.118120000000000000
          Top = 98.267779999999990000
          Width = 718.110700000000000000
          Color = clBlack
          Diagonal = True
        end
        object Memo49: TfrxMemoView
          Width = 718.110700000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'COMPROVANTE DE PAGAMENTO FINANCEIRO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line4: TfrxLineView
          Left = -0.779530000000000000
          Top = 26.456709999999990000
          Width = 718.110700000000000000
          Color = clBlack
          Diagonal = True
        end
        object Memo73: TfrxMemoView
          Left = 418.307360000000000000
          Top = 37.795300000000000000
          Width = 102.047310000000000000
          Height = 15.118120000000000000
          DataField = 'TIPO_BAIXA'
          DataSet = frxBaixas
          DataSetName = 'frxdstBaixas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstBaixas."TIPO_BAIXA"]')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Left = 328.819110000000000000
          Top = 37.795300000000000000
          Width = 86.929190000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Tipo de baixa:')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 212.031632760000000000
        Top = 449.764070000000000000
        Width = 718.110700000000000000
        object Shape4: TfrxShapeView
          Width = 718.110700000000000000
          Height = 26.456710000000000000
          Fill.BackColor = cl3DLight
        end
        object mmValorProduto: TfrxMemoView
          Left = 3.779530000000000000
          Top = 37.795300000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Dinheiro....................: ')
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Width = 718.110700000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'TOTAL FORMAS DE PAGAMENTO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          Left = 3.779530000000000000
          Top = 60.472480000000010000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cart'#227'o de cr'#233'dito.....: ')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Left = 3.779530000000000000
          Top = 83.149659999999990000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cart'#227'o de d'#233'bito......: ')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Left = 3.779530000000000000
          Top = 105.826840000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cheque......................: ')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Left = 3.779530000000000000
          Top = 128.504020000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cobran'#231'a...................: ')
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          Left = 3.779530000000000000
          Top = 151.181200000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cr'#233'dito.......................: ')
          ParentFont = False
        end
        object mmDinheiro: TfrxMemoView
          Left = 162.519790000000000000
          Top = 37.795300000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'R$ 120,20')
          ParentFont = False
        end
        object mmCartaoCredito: TfrxMemoView
          Left = 162.519790000000000000
          Top = 60.472480000000010000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'R$ 120,20')
          ParentFont = False
        end
        object mmCartaoDebito: TfrxMemoView
          Left = 162.519790000000000000
          Top = 83.149659999999990000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'R$ 120,20')
          ParentFont = False
        end
        object mmCheque: TfrxMemoView
          Left = 162.519790000000000000
          Top = 105.826840000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'R$ 120,20')
          ParentFont = False
        end
        object mmCobranca: TfrxMemoView
          Left = 162.519790000000000000
          Top = 128.504020000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'R$ 120,20')
          ParentFont = False
        end
        object mmCredito: TfrxMemoView
          Left = 162.519790000000000000
          Top = 151.181200000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'R$ 120,20')
          ParentFont = False
        end
        object mmLabelCodigoNovoCredito: TfrxMemoView
          Left = 359.055350000000000000
          Top = 36.015769999999970000
          Width = 325.039580000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'C'#243'd. novo cr'#233'dito (125.654): R$ 120,20')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 3.779527560000000000
          Top = 177.196970000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Pix..............................: ')
          ParentFont = False
        end
        object mmPix: TfrxMemoView
          Left = 162.519685040000000000
          Top = 177.196970000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'R$ 120,20')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 684.094930000000000000
        Width = 718.110700000000000000
        object Shape2: TfrxShapeView
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 90.708720000000000000
        Top = 298.582870000000000000
        Width = 718.110700000000000000
        DataSet = frxDBReceber
        DataSetName = 'frxdstReceber'
        RowCount = 0
        object Line3: TfrxLineView
          Left = 0.220470000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Diagonal = True
        end
        object Memo15: TfrxMemoView
          Left = 3.779530000000000000
          Top = 3.779530000000022000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cod. t'#237'tulo:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 135.063080000000000000
          Top = 3.779530000000022000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Documento:')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 4.897650000000000000
          Top = 22.677180000000020000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 65.252010000000000000
          Top = 3.779530000000022000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DataField = 'RECEBER_ID'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstReceber."RECEBER_ID"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 203.874150000000000000
          Top = 3.779530000000022000
          Width = 105.826840000000000000
          Height = 15.118120000000000000
          DataField = 'DOCUMENTO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstReceber."DOCUMENTO"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 315.480520000000000000
          Top = 3.779530000000022000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Parcela:')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 365.393940000000000000
          Top = 3.779530000000022000
          Width = 30.236240000000000000
          Height = 15.118120000000000000
          DataField = 'PARCELA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstReceber."PARCELA"]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 402.409710000000000000
          Top = 3.779530000000022000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Nr. parcelas:')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 471.220780000000000000
          Top = 3.779530000000022000
          Width = 30.236240000000000000
          Height = 15.118120000000000000
          DataField = 'NUMERO_PARCELAS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstReceber."NUMERO_PARCELAS"]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 506.457020000000000000
          Top = 3.779530000000022000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Vencimento:')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 575.268090000000000000
          Top = 3.779530000000022000
          Width = 79.370130000000000000
          Height = 15.118120000000000000
          DataField = 'DATA_VENCIMENTO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstReceber."DATA_VENCIMENTO"]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 50.913420000000000000
          Top = 22.677180000000020000
          Width = 238.110390000000000000
          Height = 15.118120000000000000
          DataField = 'CLIENTE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstReceber."CLIENTE"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 294.803340000000000000
          Top = 22.677180000000020000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'CPF/CNPJ:')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 356.275820000000000000
          Top = 22.677180000000020000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DataField = 'CPF_CNPJ'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstReceber."CPF_CNPJ"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 476.220780000000000000
          Top = 22.677180000000020000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Origem:')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 523.354670000000000000
          Top = 22.677180000000020000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DataField = 'ORIGEM'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstReceber."ORIGEM"]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 597.165740000000000000
          Top = 22.677180000000020000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cod. Origem:')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 669.976810000000000000
          Top = 22.677180000000020000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          DataField = 'ORIGEM_ID'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstReceber."ORIGEM_ID"]')
          ParentFont = False
        end
        object Line5: TfrxLineView
          Top = 86.929190000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Diagonal = True
        end
        object Memo59: TfrxMemoView
          Left = 4.000000000000000000
          Top = 45.354359999999990000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total t'#237'tulo')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 3.779530000000000000
          Top = 64.252009999999990000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DataField = 'VALOR_DOCUMENTO'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstReceber."VALOR_DOCUMENTO"]')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 79.370130000000000000
          Top = 45.354359999999990000
          Width = 56.692898740000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Multa')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 79.149660000000000000
          Top = 64.252009999999990000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          DataField = 'VALOR_MULTA'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstReceber."VALOR_MULTA"]')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 143.622140000000000000
          Top = 45.354359999999990000
          Width = 49.133838740000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Juros')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Left = 143.401670000000000000
          Top = 64.252009999999990000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          DataField = 'VALOR_JUROS'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstReceber."VALOR_JUROS"]')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 200.315090000000000000
          Top = 45.354359999999990000
          Width = 79.370130000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Vlr. adiantado')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 200.094620000000000000
          Top = 64.252009999999990000
          Width = 79.370130000000000000
          Height = 15.118120000000000000
          DataField = 'VALOR_ADIANTADO'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstReceber."VALOR_ADIANTADO"]')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 287.464750000000000000
          Top = 45.354359999999990000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Reten'#231#227'o')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 287.244280000000000000
          Top = 64.252009999999990000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          DataField = 'VALOR_RETENCAO'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstReceber."VALOR_RETENCAO"]')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 355.496290000000000000
          Top = 45.354359999999990000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Desconto')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Left = 355.275820000000000000
          Top = 64.252009999999990000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          DataField = 'VALOR_DESCONTO'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstReceber."VALOR_DESCONTO"]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 419.748300000000000000
          Top = 45.354359999999990000
          Width = 98.267780000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total l'#237'quido')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 419.527830000000000000
          Top = 64.252009999999990000
          Width = 98.267780000000000000
          Height = 15.118120000000000000
          DataField = 'VALOR_LIQUIDO'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstReceber."VALOR_LIQUIDO"]')
          ParentFont = False
        end
      end
    end
  end
end
