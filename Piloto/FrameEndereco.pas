unit FrameEndereco;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.Mask, _Biblioteca,  _RecordsCadastros,
  EditLukaData, _FrameHenrancaPesquisas, FrameBairros, Vcl.StdCtrls, EditLuka, Buscar.Enderecos,
  Vcl.CheckLst, EditCEPLuka, _Bairros, _Sessao, CadastrarNovoEnderecoVenda;

type
  TFrEndereco = class(TFrameHerancaPrincipal)
    lb2: TLabel;
    eNumero: TEditLuka;
    lb8: TLabel;
    lb9: TLabel;
    lb10: TLabel;
    eLogradouro: TEditLuka;
    eComplemento: TEditLuka;
    FrBairro: TFrBairros;
    ckValidarEndereco: TCheckBox;
    ckValidarComplemento: TCheckBox;
    ckValidarBairro: TCheckBox;
    ckValidarCEP: TCheckBox;
    eCEP: TEditCEPLuka;
    lb1: TLabel;
    ePontoReferencia: TEditLuka;
    ckValidarPontoReferencia: TCheckBox;
    ckValidarNumero: TCheckBox;
    procedure FrBairrosbPesquisaClick(Sender: TObject);
    procedure eNumeroExit(Sender: TObject);
  private
    FInscricaoEstadual: string;
  public
    procedure Clear; override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
    procedure setLogradouro(const pValor: string);
    procedure setComplemento(const pValor: string);
    procedure setNumero(const pValor: string);
    procedure setBairroId(const pValor: Integer);
    procedure setPontoReferencia(const pValor: string);
    procedure setCep(const pValor: string);
    procedure setInscricaoEstadual(const pValor: string);
    procedure BuscarBairro(const pCodigoCidadeIBGE: string; const pNomeBairro: string);

    procedure SetFocus; override;
    procedure VerificarDados;
    procedure SomenteLeitura(pValue: Boolean); override;

    function getLogradouro: string;
    function getComplemento: string;
    function getNumero: string;
    function getBairroId: Integer;
    function getPontoReferencia: string;
    function getCep: string;
    function getInscricaoEstadual: string;

    function CarregarEnderecoCliente(pCadastroId: Integer; pSomenteEnderecoEntrega: Boolean): Boolean;
    procedure CadastrarNovoEndereco(pCadastroId: Integer);
  end;

implementation

{$R *.dfm}

{ TFrEndereco }

procedure TFrEndereco.BuscarBairro(const pCodigoCidadeIBGE, pNomeBairro: string);
var
  vBairro: TArray<RecBairros>;
begin
  vBairro := _Bairros.BuscarBairros(Sessao.getConexaoBanco, 3, [pNomeBairro, pCodigoCidadeIBGE], 0);
  if vBairro <> nil then
    FrBairro.InserirDadoPorChave(vBairro[0].bairro_id);
end;

procedure TFrEndereco.CadastrarNovoEndereco(pCadastroId: Integer);
var
  vRetorno: TRetornoTelaFinalizar<RecDiversosEnderecos>;
begin
  vRetorno := CadastrarNovoEnderecoVenda.Cadastrar(pCadastroId);
  if vRetorno.BuscaCancelada then
    Exit;

  setLogradouro(vRetorno.Dados.logradouro);
  setComplemento(vRetorno.Dados.complemento);
  setNumero(vRetorno.Dados.numero);
  setBairroId(vRetorno.Dados.bairro_id);
  setPontoReferencia(vRetorno.Dados.ponto_referencia);
  setCep(vRetorno.Dados.cep);
  setInscricaoEstadual(vRetorno.Dados.InscricaoEstadual);
end;

function TFrEndereco.CarregarEnderecoCliente(pCadastroId: Integer; pSomenteEnderecoEntrega: Boolean): Boolean;
var
  vRetorno: TRetornoTelaFinalizar<RecDiversosEnderecos>;
begin
  Result := False;

  vRetorno := Buscar.Enderecos.Buscar(pCadastroId, pSomenteEnderecoEntrega);
  if vRetorno.BuscaCancelada then
    Exit;

  setLogradouro(vRetorno.Dados.logradouro);
  setComplemento(vRetorno.Dados.complemento);
  setNumero(vRetorno.Dados.numero);
  setBairroId(vRetorno.Dados.bairro_id);
  setPontoReferencia(vRetorno.Dados.ponto_referencia);
  setCep(vRetorno.Dados.cep);
  setInscricaoEstadual(vRetorno.Dados.InscricaoEstadual);

  Result := True;
end;

procedure TFrEndereco.Clear;
begin
 // inherited;
  _Biblioteca.LimparCampos([eComplemento, eLogradouro, eNumero, FrBairro, eCep, ePontoReferencia]);
  setInscricaoEstadual('');
end;

procedure TFrEndereco.eNumeroExit(Sender: TObject);
begin
  inherited;
  if ckValidarNumero.Checked and (eNumero.Text = '') then
    eNumero.Text := 'SN';
end;


procedure TFrEndereco.FrBairrosbPesquisaClick(Sender: TObject);
begin
  inherited;
  FrBairro.sbPesquisaClick(Sender);
end;

function TFrEndereco.getBairroId: Integer;
begin
  if not FrBairro.EstaVazio then
    Result := FrBairro.GetBairro.bairro_id
  else
    Result := 0;
end;

function TFrEndereco.getCep: string;
begin
  if eCep.getCepOk then
    Result := eCep.Text
  else
    Result := '';
end;

function TFrEndereco.getComplemento: string;
begin
  Result := eComplemento.Text;
end;

function TFrEndereco.getInscricaoEstadual: string;
begin
  Result := FInscricaoEstadual;
end;

function TFrEndereco.getLogradouro: string;
begin
  Result := eLogradouro.Text;
end;

function TFrEndereco.getNumero: string;
begin
  Result := eNumero.Text;
end;

function TFrEndereco.getPontoReferencia: string;
begin
  Result := ePontoReferencia.Text;
end;

procedure TFrEndereco.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([eLogradouro, eComplemento, FrBairro, eCep, eNumero, ePontoReferencia], pEditando, pLimpar);

  if not pEditando and pLimpar then
    setInscricaoEstadual('');
end;

procedure TFrEndereco.setBairroId(const pValor: Integer);
begin
  FrBairro.InserirDadoPorChave(pValor, False);
end;

procedure TFrEndereco.setCep(const pValor: string);
begin
  eCep.Text := pValor;
end;

procedure TFrEndereco.setComplemento(const pValor: string);
begin
  eComplemento.Text := pValor;
end;

procedure TFrEndereco.SetFocus;
begin
  inherited;
  SetarFoco(eLogradouro);
end;

procedure TFrEndereco.setInscricaoEstadual(const pValor: string);
begin
  FInscricaoEstadual := pValor;
end;

procedure TFrEndereco.setLogradouro(const pValor: string);
begin
  eLogradouro.Text := pValor;
end;

procedure TFrEndereco.setNumero(const pValor: string);
begin
  eNumero.Text := pValor;
end;

procedure TFrEndereco.setPontoReferencia(const pValor: string);
begin
  ePontoReferencia.Text := pValor;
end;

procedure TFrEndereco.SomenteLeitura(pValue: Boolean);
begin
  inherited;
  _Biblioteca.SomenteLeitura([
    eLogradouro,
    eComplemento,
    eNumero,
    FrBairro,
    ePontoReferencia,
    eCep],
    pValue
  );
end;

procedure TFrEndereco.VerificarDados;
begin
  if ckValidarEndereco.Checked and (eLogradouro.Text = '') then begin
    _Biblioteca.Exclamar('N�o foi informado o logradouro, verifique!');
    SetarFoco(eLogradouro);
    Abort;
  end;

  if ckValidarComplemento.Checked and (eComplemento.Text = '') then begin
    _Biblioteca.Exclamar('N�o foi informado o complemento, verifique!');
    SetarFoco(eComplemento);
    Abort;
  end;

  if ckValidarBairro.Checked then begin
    if FrBairro.EstaVazio then begin
      _Biblioteca.Exclamar('N�o foi informado o bairro, verifique!');
      SetarFoco(FrBairro);
      Abort;
    end;

    if FrBairro.GetBairro.bairro_id = 1 then begin
      _Biblioteca.Exclamar('N�o � permitido utilizar o bairro de importa��o!');
      SetarFoco(FrBairro);
      Abort;
    end;
  end;

  if ckValidarNumero.Checked and (eNumero.Text = '') then begin
    _Biblioteca.Exclamar('N�o foi informado o n�mero, verifique!');
    SetarFoco(eNumero);
    Abort;
  end;

  if ckValidarPontoReferencia.Checked and (ePontoReferencia.Text = '') then begin
    _Biblioteca.Exclamar('N�o foi informado o ponto de refer�ncia, verifique!');
    SetarFoco(ePontoReferencia);
    Abort;
  end;

  if ckValidarCEP.Checked and not eCep.getCepOk then begin
    _Biblioteca.Exclamar('N�o foi informado o cep, verifique!');
    SetarFoco(eCep);
    Abort;
  end;
end;

end.
