inherited FormPesquisaDepartamentosSecoesLinhas: TFormPesquisaDepartamentosSecoesLinhas
  Caption = 'Pesquisa grupo de produtos'
  ClientHeight = 439
  ClientWidth = 762
  ExplicitWidth = 770
  ExplicitHeight = 470
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 762
    Height = 393
    ColCount = 5
    OnDrawCell = sgPesquisaDrawCell
    CorLinhaFoco = 38619
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Nome'
      'N'#237'vel'
      'Ativo')
    OnGetCellColor = sgPesquisaGetCellColor
    RealColCount = 5
    ExplicitWidth = 762
    ExplicitHeight = 393
    ColWidths = (
      28
      138
      199
      301
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Text = '0'
  end
  inherited pnFiltro1: TPanel
    Width = 762
    ExplicitWidth = 762
    inherited lblPesquisa: TLabel
      Left = 205
      Width = 96
      Caption = 'Grupo de produto'
      ExplicitLeft = 205
      ExplicitWidth = 96
    end
    inherited eValorPesquisa: TEditLuka
      Width = 554
      ExplicitWidth = 554
    end
  end
end
