unit untDmRelatorio;

interface

uses
  System.SysUtils, System.Classes, Data.DB, MemDS, DBAccess, Ora, frxClass,
  frxDBSet,_Sessao;

type
  TdmRelatorio = class(TDataModule)
    dstEmpresa: TfrxDBDataset;
    qryEmpresa: TOraQuery;
    qryEmpresaEMPRESA_ID: TIntegerField;
    qryEmpresaCADASTRO_ID: TFloatField;
    qryEmpresaBAIRRO_ID: TFloatField;
    qryEmpresaCIDADE_ID: TFloatField;
    qryEmpresaNOME_BAIRRO: TStringField;
    qryEmpresaLOGRADOURO: TStringField;
    qryEmpresaCOMPLEMENTO: TStringField;
    qryEmpresaNUMERO: TStringField;
    qryEmpresaPONTO_REFERENCIA: TStringField;
    qryEmpresaCEP: TStringField;
    qryEmpresaTELEFONE_PRINCIPAL: TStringField;
    qryEmpresaTELEFONE_FAX: TStringField;
    qryEmpresaE_MAIL: TStringField;
    qryEmpresaDATA_FUNDACAO: TDateTimeField;
    qryEmpresaINSCRICAO_ESTADUAL: TStringField;
    qryEmpresaINSCRICAO_MUNICIPAL: TStringField;
    qryEmpresaCNAE: TStringField;
    qryEmpresaCNPJ: TStringField;
    qryEmpresaNOME_FANTASIA: TStringField;
    qryEmpresaNOME_RESUMIDO_IMPRESSOES_NF: TStringField;
    qryEmpresaATIVO: TStringField;
    qryEmpresaRAZAO_SOCIAL: TStringField;
    qryEmpresaTIPO_EMPRESA: TStringField;
    qryEmpresaNOME_CIDADE: TStringField;
    qryEmpresaESTADO_ID: TStringField;
    qryEmpresaCODIGO_IBGE_CIDADE: TIntegerField;
    qryEmpresaCODIGO_IBGE_ESTADO: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmRelatorio: TdmRelatorio;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdmRelatorio.DataModuleCreate(Sender: TObject);
begin
  qryEmpresa.Session := Sessao.getConexaoBanco;
  qryEmpresa.ParamByName('EMPRESAID').AsInteger := Sessao.getEmpresaLogada.EmpresaId;
  qryEmpresa.Open;

end;

end.
