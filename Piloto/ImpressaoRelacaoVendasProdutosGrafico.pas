unit ImpressaoRelacaoVendasProdutosGrafico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaImpressaoRelatoriosGrafico, _Biblioteca,
  QRCtrls, qrpctrls, QuickRpt, Vcl.ExtCtrls, QRExport, QRPDFFilt, QRWebFilt;

type
  TTipoImpressao = (tiVendedor, tiVendedorProdutos);

  RecProdutoImpresssao = record
    ProdutoId: Integer;
    NomeProduto: string;
    NomeMarca: string;
    Quantidade: Double;
    Unidade: string;
    ValorLiquido: Double;
  end;

  RecDadosImpressao = record
    Vendedor: string;
    QtdeTotal: Double;
    TotalLiquido: Double;
    Produtos: TArray<RecProdutoImpresssao>;
  end;

  RecDadosVendDevol = record
    Pedido: string;
    Devolucao: string;
    Movimento: string;
    DataRecebimento: TDateTime;
    Cliente: string;
    Vendedor: string;
    CondPagto: string;
    ValorProduto: Double;
    ValorTotal: Double;
    OutrasDespesas: Double;
    Desconto: Double;
    ValorBruto: Double;
    ValorLiquido: Double;
    PercLucroBruto: Double;
    PercLucroLiquido: Double;
    Empresa: string;
    Posicao: Integer;
  end;

  RecDadosProdutoVendDevol = record
    Posicao: Integer;
    ProdutoId: Integer;
    Nome: string;
    Marca: string;
    PrecoUnitario: Double;
    Quantidade: Double;
    Unidade: string;
    ValorTotal: Double;
    OutrasDespesas: Double;
    Desconto: Double;
    ValorLucroBruto: Double;
    ValorLucroLiquido: Double;
    PercLucroBruto: Double;
    PercLucroLiquido: Double;
  end;

  TFormImpressaoRelacaoVendasProdutosGrafico = class(TFormHerancaImpressaoRelatoriosGrafico)
    qrbndColumnHeaderBand1: TQRBand;
    qrbndDetailBand1: TQRBand;
    qrCabValorLiquido: TQRLabel;
    qrCabQuantidade: TQRLabel;
    qrCabNomeProduto: TQRLabel;
    qrCabProdutoId: TQRLabel;
    QRSubDetail1: TQRSubDetail;
    qrCabMarca: TQRLabel;
    qrCabUnidade: TQRLabel;
    qrValorLiquidoProduto: TQRLabel;
    qrQuantidadeProduto: TQRLabel;
    qrNomeProduto: TQRLabel;
    qrProdutoId: TQRLabel;
    qrMarcaProduto: TQRLabel;
    qrUnidadeProduto: TQRLabel;
    qrNomeVendedor: TQRLabel;
    qr8: TQRLabel;
    qr18: TQRLabel;
    qr19: TQRLabel;
    qrQuantidadeVendedor: TQRLabel;
    qrTotalLiquidoVendedor: TQRLabel;
    qrSeparadorVendedores: TQRShape;
    procedure qrRelatorioA4BeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure qrbndDetailBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qrRelatorioA4NeedData(Sender: TObject; var MoreData: Boolean);
    procedure QRSubDetail1NeedData(Sender: TObject; var MoreData: Boolean);
    procedure QRSubDetail1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
  private
    FPosicVendedores: Integer;
    FPosicProdutos: Integer;

    FDados: TArray<RecDadosImpressao>;
  public
    { Public declarations }
  end;

procedure Imprimir(
  AOwner: TComponent;
  pOpcaoRelatorio: Integer;
  pDados: TArray<RecDadosImpressao>;
  pFiltrosUtilizados: TArray<string>
);

implementation

{$R *.dfm}

procedure Imprimir(
  AOwner: TComponent;
  pOpcaoRelatorio: Integer;
  pDados: TArray<RecDadosImpressao>;
  pFiltrosUtilizados: TArray<string>
);
var
  vForm: TFormImpressaoRelacaoVendasProdutosGrafico;
begin
  vForm := TFormImpressaoRelacaoVendasProdutosGrafico.Create(AOwner, pFiltrosUtilizados);

  vForm.FDados := pDados;

  if pOpcaoRelatorio = Integer(tiVendedor) then begin
    vForm.QRSubDetail1.Enabled := False;
    _Biblioteca.Visibilidade([
      vForm.qrCabValorLiquido,
      vForm.qrCabNomeProduto,
      vForm.qrCabQuantidade,
      vForm.qrCabProdutoId,
      vForm.qrCabMarca,
      vForm.qrCabUnidade,
      vForm.qrSeparadorVendedores],
      False
    );

    vForm.qrbndDetailBand1.Height := 20;
  end;

  vForm.Preview;

  vForm.Free;
end;

procedure TFormImpressaoRelacaoVendasProdutosGrafico.qrbndDetailBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  FPosicProdutos := -1;

  qrNomeVendedor.Caption         := FDados[FPosicVendedores].Vendedor;
  qrQuantidadeVendedor.Caption   := NFormatNEstoque(FDados[FPosicVendedores].QtdeTotal);
  qrTotalLiquidoVendedor.Caption := NFormatN(FDados[FPosicVendedores].TotalLiquido);
end;

procedure TFormImpressaoRelacaoVendasProdutosGrafico.qrRelatorioA4BeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicVendedores := -1;
end;

procedure TFormImpressaoRelacaoVendasProdutosGrafico.qrRelatorioA4NeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicVendedores);
  MoreData := FPosicVendedores < Length(FDados);
end;

procedure TFormImpressaoRelacaoVendasProdutosGrafico.QRSubDetail1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrProdutoId.Caption           := NFormat(FDados[FPosicVendedores].Produtos[FPosicProdutos].ProdutoId);
  qrNomeProduto.Caption         := FDados[FPosicVendedores].Produtos[FPosicProdutos].NomeProduto;
  qrMarcaProduto.Caption        := FDados[FPosicVendedores].Produtos[FPosicProdutos].NomeMarca;
  qrQuantidadeProduto.Caption   := NFormatEstoque(FDados[FPosicVendedores].Produtos[FPosicProdutos].Quantidade);
  qrUnidadeProduto.Caption      := FDados[FPosicVendedores].Produtos[FPosicProdutos].Unidade;
  qrValorLiquidoProduto.Caption := NFormat(FDados[FPosicVendedores].Produtos[FPosicProdutos].ValorLiquido);
end;

procedure TFormImpressaoRelacaoVendasProdutosGrafico.QRSubDetail1NeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicProdutos);
  MoreData := FPosicProdutos < Length(FDados[FPosicVendedores].Produtos);
end;

end.
