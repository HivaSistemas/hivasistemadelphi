unit GruposTributacoesFederalVenda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Biblioteca,
  ComboBoxLuka, CheckBoxLuka, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _GruposTribFederalVenda,
  _RecordsEspeciais, _Sessao, PesquisaGruposTributacoesFederalVenda;

type
  TFormGruposTributacoesFederalVenda = class(TFormHerancaCadastroCodigo)
    lb25: TLabel;
    cbOrigemProduto: TComboBoxLuka;
    lb3: TLabel;
    cbCstSaidaPIS: TComboBoxLuka;
    lb5: TLabel;
    cbCstSaidaCOFINS: TComboBoxLuka;
    lb1: TLabel;
    eDescricao: TEditLuka;
    procedure FormCreate(Sender: TObject);
  private
    procedure PreencherRegistro(pGrupo: RecGruposTribFederalVenda);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormGruposTributacoesFederalVenda }

procedure TFormGruposTributacoesFederalVenda.BuscarRegistro;
var
  vGrupo: TArray<RecGruposTribFederalVenda>;
begin
  vGrupo := _GruposTribFederalVenda.BuscarGruposTribFederalVenda(Sessao.getConexaoBanco, 0, [eID.AsInt]);
  if vGrupo = nil then begin
    _Biblioteca.NenhumRegistro;
    SetarFoco(eID);
    Abort;
  end;

  inherited;
  PreencherRegistro(vGrupo[0]);
end;

procedure TFormGruposTributacoesFederalVenda.ExcluirRegistro;
begin
  Sessao.AbortarSeHouveErro( _GruposTribFederalVenda.ExcluirGruposTribFederalVenda(Sessao.getConexaoBanco, eID.AsInt) );
  _Biblioteca.RegistroExcluidoSucesso;
  inherited;
end;

procedure TFormGruposTributacoesFederalVenda.FormCreate(Sender: TObject);
begin
  inherited;
  Sessao.SetComboBoxPisCofins(cbCstSaidaPIS);
  Sessao.SetComboBoxPisCofins(cbCstSaidaCOFINS);
end;

procedure TFormGruposTributacoesFederalVenda.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vRetBanco :=
    _GruposTribFederalVenda.AtualizarGruposTribFederalVenda(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eDescricao.Text,
      cbOrigemProduto.AsInt,
      cbCstSaidaPIS.AsString,
      cbCstSaidaCOFINS.AsString,
      ckAtivo.CheckedStr
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormGruposTributacoesFederalVenda.Modo(pEditando: Boolean);
begin
  inherited;

  _Biblioteca.Habilitar([
    eDescricao,
    cbOrigemProduto,
    cbCstSaidaPIS,
    cbCstSaidaCOFINS],
    pEditando
  );

  if pEditando then
    SetarFoco(eDescricao);
end;

procedure TFormGruposTributacoesFederalVenda.PesquisarRegistro;
var
  vGrupo: RecGruposTribFederalVenda;
begin
  vGrupo := RecGruposTribFederalVenda(PesquisaGruposTributacoesFederalVenda.Pesquisar);
  if vGrupo = nil then
    Exit;

  inherited;
  PreencherRegistro(vGrupo);
end;

procedure TFormGruposTributacoesFederalVenda.PreencherRegistro(pGrupo: RecGruposTribFederalVenda);
begin
  eID.AsInt                   := pGrupo.GrupoTribFederalVendaId;
  eDescricao.Text             := pGrupo.Descricao;
  ckAtivo.CheckedStr          := pGrupo.Ativo;
  cbOrigemProduto.AsInt       := pGrupo.OrigemProduto;
  cbCstSaidaPIS.AsString      := pGrupo.CstPis;
  cbCstSaidaCOFINS.AsString   := pGrupo.CstCofins;
end;

procedure TFormGruposTributacoesFederalVenda.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if eDescricao.Trim = '' then begin
    _Biblioteca.Exclamar('A descri��o do grupo de tributa��o n�o foi informada corretamente, verifique!');
    SetarFoco(eDescricao);
    Abort;
  end;

  if cbOrigemProduto.AsString = '' then begin
    _Biblioteca.Exclamar('A origem do produto n�o foi informada corretamente, verifique!');
    SetarFoco(cbOrigemProduto);
    Abort;
  end;

  if cbCstSaidaPIS.AsString = '' then begin
    _Biblioteca.Exclamar('A CST do PIS do produto n�o foi informada corretamente, verifique!');
    SetarFoco(cbCstSaidaPIS);
    Abort;
  end;

  if cbCstSaidaCOFINS.AsString = '' then begin
    _Biblioteca.Exclamar('A CST do COFINS do produto n�o foi informada corretamente, verifique!');
    SetarFoco(cbCstSaidaCOFINS);
    Abort;
  end;
end;

end.
