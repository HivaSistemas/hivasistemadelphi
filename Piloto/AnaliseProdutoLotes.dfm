inherited FormAnaliseProdutoLotes: TFormAnaliseProdutoLotes
  BorderIcons = [biSystemMenu, biMinimize, biMaximize]
  Caption = 'Controle de ponta de estoque'
  ClientHeight = 515
  ClientWidth = 904
  ExplicitWidth = 910
  ExplicitHeight = 544
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 515
    ExplicitHeight = 515
    inherited sbDesfazer: TSpeedButton
      Top = 51
      ExplicitTop = 51
    end
    inherited sbExcluir: TSpeedButton
      Left = -112
      Visible = False
      ExplicitLeft = -112
    end
    inherited sbPesquisar: TSpeedButton
      Left = -112
      Visible = False
      ExplicitLeft = -112
    end
    inherited sbLogs: TSpeedButton
      Left = 0
      Top = 473
      Width = 117
      ExplicitLeft = 0
      ExplicitTop = 473
      ExplicitWidth = 117
    end
  end
  inline FrProduto: TFrProdutos
    Left = 123
    Top = 6
    Width = 331
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 123
    ExplicitTop = 6
    ExplicitWidth = 331
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 306
      Height = 23
      ExplicitWidth = 306
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 331
      ExplicitWidth = 331
      inherited lbNomePesquisa: TLabel
        Width = 48
        ExplicitWidth = 48
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 226
        ExplicitLeft = 226
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 306
      Height = 24
      ExplicitLeft = 306
      ExplicitHeight = 24
    end
  end
  object sgLotes: TGridLuka
    Left = 123
    Top = 48
    Width = 779
    Height = 466
    Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
    Anchors = [akLeft, akTop, akRight, akBottom]
    ColCount = 8
    Ctl3D = True
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goThumbTracking]
    ParentCtl3D = False
    PopupMenu = pmOpcoes
    TabOrder = 2
    OnDrawCell = sgLotesDrawCell
    OnSelectCell = sgLotesSelectCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Lote'
      'Ponta est.'
      'Mult. pont. est.'
      'F'#237'sico'
      'Dispon'#237'vel'
      'Reservado'
      'Data fabrica'#231#227'o'
      'Data vencimento')
    OnGetCellColor = sgLotesGetCellColor
    OnArrumarGrid = sgLotesArrumarGrid
    Grid3D = False
    RealColCount = 8
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      123
      67
      99
      78
      75
      88
      107
      110)
  end
  object pmOpcoes: TPopupMenu
    Left = 664
    Top = 248
    object miAtivarDesativarPontaEstLoteSel: TMenuItem
      Caption = 'Ativar/desat. ponta estoque lote sel.'
      OnClick = miAtivarDesativarPontaEstLoteSelClick
    end
    object miAtivarDesativarPontaEstTodosLotes: TMenuItem
      Caption = 'Ativar/desat. ponta estoque todos lotes'
      OnClick = miAtivarDesativarPontaEstTodosLotesClick
    end
  end
end
