inherited FormAdiantamentoAcumulado: TFormAdiantamentoAcumulado
  Caption = 'Adiantamento de acumulado'
  ClientHeight = 335
  ClientWidth = 550
  ExplicitWidth = 556
  ExplicitHeight = 364
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label1: TLabel
    Top = 4
    Width = 38
    Caption = 'Pedido'
    ExplicitTop = 4
    ExplicitWidth = 38
  end
  object lb1: TLabel [1]
    Left = 217
    Top = 4
    Width = 39
    Height = 14
    Caption = 'Cliente'
  end
  object lb3: TLabel [2]
    Left = 447
    Top = 4
    Width = 98
    Height = 14
    Caption = 'Data recebimento'
  end
  object sbInformacoesPedido: TSpeedButtonLuka [3]
    Left = 194
    Top = 22
    Width = 18
    Height = 18
    Hint = 'Abrir informa'#231#245'es do pedido'
    Flat = True
    NumGlyphs = 2
    OnClick = sbInformacoesPedidoClick
    TagImagem = 13
    PedirCertificacao = False
    PermitirAutOutroUsuario = False
  end
  object lbl1: TLabel [4]
    Left = 418
    Top = 43
    Width = 126
    Height = 14
    Caption = 'Valor do adiantamento'
  end
  object lb2: TLabel [5]
    Left = 126
    Top = 43
    Width = 52
    Height = 14
    Caption = 'Vendedor'
  end
  object lb4: TLabel [6]
    Left = 305
    Top = 43
    Width = 31
    Height = 14
    Caption = 'Saldo'
  end
  inherited pnOpcoes: TPanel
    Height = 335
    TabOrder = 5
    ExplicitHeight = 294
    inherited sbDesfazer: TSpeedButton
      Top = 90
      ExplicitTop = 90
    end
    inherited sbExcluir: TSpeedButton
      Left = -136
      Visible = False
      ExplicitLeft = -136
    end
    inherited sbPesquisar: TSpeedButton
      Top = 44
      ExplicitTop = 44
    end
    inherited sbLogs: TSpeedButton
      Left = -135
      ExplicitLeft = -135
    end
  end
  inherited eID: TEditLuka
    Top = 18
    Width = 67
    TabOrder = 0
    ExplicitTop = 18
    ExplicitWidth = 67
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 651
    TabOrder = 6
    Visible = False
    ExplicitLeft = 651
  end
  object eCliente: TEditLuka
    Left = 217
    Top = 18
    Width = 224
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 1
    OnKeyDown = eIDKeyDown
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eDataRecebimento: TEditLukaData
    Left = 447
    Top = 18
    Width = 98
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1; '
    MaxLength = 10
    ReadOnly = True
    TabOrder = 2
    Text = '  /  /    '
  end
  inline FrPagamentoFinanceiro: TFrPagamentoFinanceiro
    Left = 189
    Top = 85
    Width = 303
    Height = 244
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 7
    TabStop = True
    ExplicitLeft = 189
    ExplicitTop = 85
    ExplicitHeight = 244
    inherited grpFechamento: TGroupBox
      Height = 244
      ExplicitHeight = 205
      inherited lbllb11: TLabel
        Top = 63
        ExplicitTop = 63
      end
      inherited lbllb12: TLabel
        Top = 83
        ExplicitTop = 83
      end
      inherited lbllb13: TLabel
        Left = 32
        Top = 103
        Width = 78
        ExplicitLeft = 32
        ExplicitTop = 103
        ExplicitWidth = 78
      end
      inherited lbllb14: TLabel
        Top = 143
        ExplicitTop = 143
      end
      inherited lbllb15: TLabel
        Top = 163
        ExplicitTop = 163
      end
      inherited lbllb10: TLabel
        Top = 215
        ExplicitTop = 215
      end
      inherited lbllb21: TLabel
        Left = 315
        Visible = False
        ExplicitLeft = 315
      end
      inherited lbllb7: TLabel
        Left = 553
        Width = 9
        Height = 14
        Visible = False
        ExplicitLeft = 553
        ExplicitWidth = 9
        ExplicitHeight = 14
      end
      inherited lbllb19: TLabel
        Top = 20
        Font.Color = clBlack
        ExplicitTop = 20
      end
      inherited lbllb1: TLabel
        Left = 315
        Visible = False
        ExplicitLeft = 315
      end
      inherited lb4: TLabel
        Top = 123
        ExplicitTop = 123
      end
      inherited lb1: TLabel
        Left = 315
        Visible = False
        ExplicitLeft = 315
      end
      inherited sbBuscarDadosDinheiro: TImage
        Top = 59
        ExplicitTop = 59
      end
      inherited sbBuscarDadosCheques: TImage
        Top = 79
        ExplicitTop = 79
      end
      inherited sbBuscarDadosCartoesDebito: TImage
        Top = 99
        ExplicitTop = 99
      end
      inherited sbBuscarDadosCartoesCredito: TImage
        Top = 119
        ExplicitTop = 119
      end
      inherited sbBuscarDadosCobranca: TImage
        Top = 140
        ExplicitTop = 140
      end
      inherited sbBuscarDadosCreditos: TImage
        Top = 160
        ExplicitTop = 160
      end
      inherited Label1: TLabel
        Top = 184
        ExplicitTop = 184
      end
      inherited sbBuscarDadosPix: TImage
        Top = 180
        ExplicitTop = 180
      end
      inherited eValorDinheiro: TEditLuka
        Top = 56
        Height = 22
        ExplicitTop = 56
        ExplicitHeight = 22
      end
      inherited eValorCheque: TEditLuka
        Top = 76
        Height = 22
        ExplicitTop = 76
        ExplicitHeight = 22
      end
      inherited eValorCartaoDebito: TEditLuka
        Top = 96
        Height = 22
        ExplicitTop = 96
        ExplicitHeight = 22
      end
      inherited eValorCobranca: TEditLuka
        Top = 136
        Height = 22
        ExplicitTop = 136
        ExplicitHeight = 22
      end
      inherited eValorCredito: TEditLuka
        Top = 156
        Height = 22
        ExplicitTop = 156
        ExplicitHeight = 22
      end
      inherited stPagamento: TStaticText
        Top = 38
        ExplicitTop = 38
      end
      inherited eValorDiferencaPagamentos: TEditLuka
        Top = 208
        Height = 22
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitTop = 208
        ExplicitHeight = 22
      end
      inherited eValorDesconto: TEditLuka
        Left = 416
        Height = 22
        Visible = False
        ExplicitLeft = 416
        ExplicitHeight = 22
      end
      inherited ePercentualDesconto: TEditLuka
        Left = 512
        Height = 22
        Visible = False
        ExplicitLeft = 512
        ExplicitHeight = 22
      end
      inherited eValorTotalASerPago: TEditLuka
        Top = 12
        ExplicitTop = 12
      end
      inherited eValorJuros: TEditLuka
        Left = 416
        Height = 22
        Visible = False
        ExplicitLeft = 416
        ExplicitHeight = 22
      end
      inherited eValorCartaoCredito: TEditLuka
        Top = 116
        Height = 22
        ExplicitTop = 116
        ExplicitHeight = 22
      end
      inherited stValorTotal: TStaticText
        Top = 38
        ExplicitTop = 38
      end
      inherited stDinheiroDefinido: TStaticText
        Top = 60
        ExplicitTop = 60
      end
      inherited stChequeDefinido: TStaticText
        Top = 80
        ExplicitTop = 80
      end
      inherited stCartaoDebitoDefinido: TStaticText
        Top = 100
        ExplicitTop = 100
      end
      inherited stCobrancaDefinido: TStaticText
        Top = 140
        ExplicitTop = 140
      end
      inherited stCreditoDefinido: TStaticText
        Top = 160
        ExplicitTop = 160
      end
      inherited stCartaoCreditoDefinido: TStaticText
        Top = 120
        ExplicitTop = 120
      end
      inherited eValorMulta: TEditLuka
        Left = 416
        Height = 22
        Visible = False
        ExplicitLeft = 416
        ExplicitHeight = 22
      end
      inherited eValorPix: TEditLuka
        Top = 177
        Height = 22
        ExplicitTop = 177
        ExplicitHeight = 22
      end
      inherited stPixDefinido: TStaticText
        Top = 180
        ExplicitTop = 180
      end
    end
  end
  object eValorAdiantamento: TEditLuka
    Left = 418
    Top = 57
    Width = 127
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    Text = '0,00'
    OnChange = eValorAdiantamentoChange
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eVendedor: TEditLuka
    Left = 126
    Top = 57
    Width = 173
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 3
    OnKeyDown = eIDKeyDown
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eSaldo: TEditLuka
    Left = 305
    Top = 57
    Width = 107
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 8
    Text = '0,00'
    OnChange = eValorAdiantamentoChange
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
end
