unit TransferenciaLocais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _EstoquesDivisao,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, Frame.Lotes, FrameProdutos, _Biblioteca,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameLocais, Vcl.Grids, _Sessao,
  GridLuka, StaticTextLuka, _RecordsEspeciais, _TransferenciasLocais, _TransferenciasLocaisTemp, _TransferenciasLocaisItens,
  CheckBoxLuka, Data.DB, MemDS, DBAccess, Ora, frxClass, frxDBSet, OraCall, _TransferenciasLocaisItensTemp,
  Datasnap.DBClient, MidasLib, SelecionarVariosOpcoes ;

type
  TFormTransferenciaLocais = class(TFormHerancaCadastroCodigo)
    FrLocalOrigem: TFrLocais;
    FrLocalDestino: TFrLocais;
    lb1: TLabel;
    eObservacao: TEditLuka;
    lb7: TLabel;
    FrProduto: TFrProdutos;
    eQuantidade: TEditLuka;
    FrLote: TFrameLotes;
    sgItens: TGridLuka;
    StaticTextLuka4: TStaticTextLuka;
    stEstoqueFisico: TStaticText;
    stEstoqueDisponivel: TStaticText;
    StaticTextLuka2: TStaticTextLuka;
    frxReport: TfrxReport;
    dstTrans: TfrxDBDataset;
    OraSession1: TOraSession;
    cdsProduto: TClientDataSet;
    cdsProdutocodigoProd: TStringField;
    cdsProdutoNomeProduto: TStringField;
    cdsProdutoMarca: TStringField;
    cdsProdutoUnidade: TStringField;
    cdsProdutoQuantidade: TFloatField;
    cdsProdutoLocalDestino: TStringField;
    cdsProdutoLocalOrigem: TStringField;
    cdsProdutoObservacao: TStringField;
    cdsProdutoLote: TStringField;
    procedure sgItensDblClick(Sender: TObject);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure eQuantidadeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbGravarClick(Sender: TObject);
  private
    FLinha: Integer;
    procedure AdicionarNoProdutoGrid(
      pProdutoId: Integer;
      pNomeProduto: string;
      pMarca: string;
      pUnidade: string;
      pLote: string;
      pQuantidade: Double;
      pLocalDestinoId: Integer;
      pNomeLocalDestino: string;
      pDataFabricacao: TDate;
      pDataVencimento: TDate;
      pEstoqueFisico: Double;
      pEstoqueDisponivel: Double
    );

    procedure FrProdutoOnAposPesquisar(Sender: TObject);
    procedure FrProdutoOnAposDeletar(Sender: TObject);
    procedure FrLocalOrigemOnAposPesquisar(Sender: TObject);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

uses untDmRelatorio;

{ TFormTransferenciaLocais }

const
  coProdutoId         = 0;
  coNomeProduto       = 1;
  coMarca             = 2;
  coUnidade           = 3;
  coLote              = 4;
  coQuantidade        = 5;
  coLocalDestino      = 6;
  coNomeLocalDestino  = 7;
  coDataFabricacao    = 8;
  coDataVencimento    = 9;
  coEstoqueFisico     = 10;
  coEstoqueDisponivel = 11;

  (* Ocultas *)
  coLocalDestinoId    = 12;

procedure TFormTransferenciaLocais.AdicionarNoProdutoGrid(
  pProdutoId: Integer;
  pNomeProduto: string;
  pMarca: string;
  pUnidade: string;
  pLote: string;
  pQuantidade: Double;
  pLocalDestinoId: Integer;
  pNomeLocalDestino: string;
  pDataFabricacao: TDate;
  pDataVencimento: TDate;
  pEstoqueFisico: Double;
  pEstoqueDisponivel: Double
);
var
  vLinha: Integer;
begin
  if FLinha = 0 then begin
    vLinha := sgItens.Localizar([coProdutoId, coLocalDestinoId, coLote], [NFormat(pProdutoId), NFormat(pLocalDestinoId), pLote]);
    if vLinha > -1 then begin
      Exclamar('Este produto j� foi adicionado no grid!');
      sgItens.Row := vLinha;
      sgItens.SetFocus;
      Abort;
    end;

    if sgItens.Cells[coProdutoId, 1] = '' then
      FLinha := 1
    else begin
      FLinha := sgItens.RowCount;
      sgItens.RowCount := sgItens.RowCount + 1;
    end;
  end;

  sgItens.Cells[coProdutoId, FLinha]      := NFormat(pProdutoId);
  sgItens.Cells[coNomeProduto, FLinha]    := pNomeProduto;
  sgItens.Cells[coMarca, FLinha]          := pMarca;
  sgItens.Cells[coUnidade, FLinha]        := pUnidade;
  sgItens.Cells[coLocalDestino, FLinha]   := NFormat(pLocalDestinoId) + ' - ' + pNomeLocalDestino;
  sgItens.Cells[coQuantidade, FLinha]     := NFormatNEstoque(pQuantidade);
  sgItens.Cells[coLote, FLinha]           := pLote;
  sgItens.Cells[coDataFabricacao, FLinha] := DFormatN(pDataFabricacao);
  sgItens.Cells[coDataVencimento, FLinha] := DFormatN(pDataVencimento);
  sgItens.Cells[coEstoqueFisico, FLinha]  := NFormatNEstoque(pEstoqueFisico);
  sgItens.Cells[coEstoqueDisponivel, FLinha] := NFormatNEstoque(pEstoqueDisponivel);
  sgItens.Cells[coLocalDestinoId, FLinha]    := NFormat(pLocalDestinoId);

  sgItens.Row := FLinha;
  Flinha := 0
end;

procedure TFormTransferenciaLocais.BuscarRegistro;
begin
  inherited;

end;

procedure TFormTransferenciaLocais.eQuantidadeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if FrProduto.EstaVazio then begin
    Exclamar('O produto n�o foi informado, verifique!');
    FrProduto.SetFocus;
    Exit;
  end;

  if FrLocalDestino.EstaVazio then begin
    Exclamar('O local de destino do produto n�o foi definido!');
    SetarFoco(FrLocalDestino);
    Exit;
  end;

  if not _Biblioteca.ValidarMultiplo(eQuantidade.AsDouble, FrProduto.getProduto().multiplo_venda) then begin
    SetarFoco(eQuantidade);
    Exit;
  end;

  if eQuantidade.AsCurr = 0 then begin
    Exclamar('A quantidade n�o foi inforamada corretamente, verifique!');
    SetarFoco(eQuantidade);
    Exit;
  end;

  if not FrLote.LoteValido then begin
    Exclamar('O lote informado n�o � v�lido, verifique!');
    SetarFoco(FrLote);
    Exit;
  end;

  if (FrProduto.getProduto.ExigirDataFabricacaoLote = 'S') or (FrProduto.getProduto.ExigirDataVencimentoLote = 'S') then begin


  end;

  if FrLocalOrigem.GetLocais.local_id = FrLocalDestino.GetLocais.local_id then begin
    Exclamar('O local de origem e destino n�o podem ser os mesmos, verifique!');
    SetarFoco(FrLocalDestino);
    Exit;
  end;

  AdicionarNoProdutoGrid(
    FrProduto.getProduto().produto_id,
    FrProduto.getProduto().nome,
    FrProduto.getProduto().nome_marca,
    FrProduto.getProduto().unidade_venda,
    FrLote.Lote,
    eQuantidade.AsDouble,
    FrLocalDestino.GetLocais().local_id,
    FrLocalDestino.GetLocais().nome,
    FrLote.DataFabricacao,
    FrLote.DataVencimento,
    0, // Estoque atual
    0 // Estoque disponivel
  );

  _Biblioteca.LimparCampos([FrProduto, FrLocalDestino, eQuantidade]);
  SetarFoco(FrProduto);
end;

procedure TFormTransferenciaLocais.ExcluirRegistro;
begin
  inherited;

end;

procedure TFormTransferenciaLocais.FormCreate(Sender: TObject);
begin
  inherited;
  FrLocalOrigem.OnAposPesquisar := FrLocalOrigemOnAposPesquisar;
  FrProduto.OnAposPesquisar := FrProdutoOnAposPesquisar;
  FrProduto.OnAposDeletar := FrProdutoOnAposDeletar;
end;

procedure TFormTransferenciaLocais.FrLocalOrigemOnAposPesquisar(Sender: TObject);
begin
  _Biblioteca.Habilitar([FrLocalOrigem], False, False);
  SetarFoco(eObservacao);
end;

procedure TFormTransferenciaLocais.FrProdutoOnAposDeletar(Sender: TObject);
begin
//  _Biblioteca.LimparCampos([eEstoqueAtual, eUnidadeProduto, eQuantidade]);
  FrLote.Clear;
end;

procedure TFormTransferenciaLocais.FrProdutoOnAposPesquisar(Sender: TObject);
var
  vEstoques: TArray<RecEstoquesDivisao>;
  vLotesString: TArray<String>;
  vOpcaoSelecionada: Integer;
  i: Integer;
begin
  if Em(FrProduto.getProduto.TipoControleEstoque, ['K', 'A']) then begin
    _Biblioteca.Informar('Produtos que s�o cotrolados como "Kit" n�o pode ter seu estoque transferido, transfira os produtos que comp�e o kit.');
    SetarFoco(FrProduto);
    FrProduto.Clear;
    Exit;
  end;

  vEstoques := _EstoquesDivisao.BuscarEstoque(Sessao.getConexaoBanco, 3, [Sessao.getEmpresaLogada.EmpresaId, FrLocalOrigem.GetLocais.local_id, FrProduto.getProduto.produto_id]);
  if vEstoques = nil then begin
    _Biblioteca.Exclamar('N�o foi encontrado estoque para o produto selecionado no local ' + FrLocalOrigem.GetLocais().nome + '!');
    SetarFoco(FrProduto);
    FrProduto.Clear;
    Exit;
  end;

  if _Biblioteca.Arredondar(vEstoques[0].Fisico, _Biblioteca.getCasasDecimaisEstoque) <= 0 then begin
    _Biblioteca.Exclamar('N�o foi encontrado nenhuma quantidade de estoque f�sico para o produto selecionado no local ' + FrLocalOrigem.GetLocais().nome + '!');
    SetarFoco(FrProduto);
    FrProduto.Clear;
    Exit;
  end;

  stEstoqueFisico.Caption := NFormatEstoque(vEstoques[0].Fisico);
  stEstoqueDisponivel.Caption := NFormatEstoque(vEstoques[0].Disponivel);

  FrLote.SetTipoControleEstoque(
    FrProduto.getProduto.TipoControleEstoque,
    FrProduto.getProduto.ExigirDataFabricacaoLote,
    FrProduto.getProduto.ExigirDataVencimentoLote
  );

  if (FrProduto.getProduto.TipoControleEstoque = 'L') OR (FrProduto.getProduto.TipoControleEstoque = 'P') then begin
    if Length(vEstoques) = 1 then begin
      FrLote.Lote := vEstoques[0].Lote;
      FrLote.DataFabricacao := vEstoques[0].DataFabricacao;
      FrLote.DataVencimento := vEstoques[0].DataVencimento;
    end
    else begin
      for I := Low(vEstoques) to High(vEstoques) do begin
        SetLength(vLotesString, Length(vLotesString) + 1);

        if FrProduto.getProduto.TipoControleEstoque = 'L' then
          vLotesString[High(vLotesString)] := vEstoques[i].Lote + ' Disp.: '  + NFormatEstoque(vEstoques[i].Fisico)
        else
          vLotesString[High(vLotesString)] :=
            'Bitola: ' + Copy(vEstoques[i].Lote, 1, Pos('-', vEstoques[i].Lote) - 1) + ' ' +
            'Tonalidade: ' + Copy(vEstoques[i].Lote, Pos('-', vEstoques[i].Lote) + 1, Length(vEstoques[i].Lote)) +
            ' Disp.: '  + NFormatEstoque(vEstoques[i].Fisico);
      end;

      vOpcaoSelecionada := SelecionarVariosOpcoes.Selecionar('Lotes dispon�veis', vLotesString, 0).Dados;
      if vOpcaoSelecionada = -1 then
        Exit;

      FrLote.Lote := vEstoques[vOpcaoSelecionada].Lote;
      FrLote.DataFabricacao := vEstoques[vOpcaoSelecionada].DataFabricacao;
      FrLote.DataVencimento := vEstoques[vOpcaoSelecionada].DataVencimento;
    end;
  end;
end;

procedure TFormTransferenciaLocais.GravarRegistro(Sender: TObject);
var
  i: Integer;
  vRetornoBanco: RecRetornoBD;
  vItens: TArray<RecTransferenciasLocaisItens>;
  vItensTemp: TArray<RecTransferenciasLocaisItensTemp>;
begin
  inherited;

  if Sessao.getParametros.UtilConfirmacaoTransferenciaLocais = 'N' then begin
    SetLength(vItens, sgItens.RowCount -1);
    for i := 1 to sgItens.RowCount -1 do begin
      vItens[i - 1].TransferenciaLocalId := 0;
      vItens[i - 1].LocalDestinoId       := SFormatInt(sgItens.Cells[coLocalDestinoId, i]);
      vItens[i - 1].ItemId               := i;
      vItens[i - 1].ProdutoId            := SFormatInt(sgItens.Cells[coProdutoId, i]);
      vItens[i - 1].Quantidade           := SFormatDouble(sgItens.Cells[coQuantidade, i]);
      vItens[i - 1].Lote                 := sgItens.Cells[coLote, i];
    end;

    vRetornoBanco :=
      _TransferenciasLocais.AtualizarTransferenciaLocal(
        Sessao.getConexaoBanco,
        eID.AsInt,
        Sessao.getEmpresaLogada.EmpresaId,
        FrLocalOrigem.GetLocais.local_id,
        eObservacao.Text,
        vItens
      );
  end
  else begin
    SetLength(vItensTemp, sgItens.RowCount -1);
    for i := 1 to sgItens.RowCount -1 do begin
      vItensTemp[i - 1].TransferenciaLocalId := 0;
      vItensTemp[i - 1].LocalDestinoId       := SFormatInt(sgItens.Cells[coLocalDestinoId, i]);
      vItensTemp[i - 1].ItemId               := i;
      vItensTemp[i - 1].ProdutoId            := SFormatInt(sgItens.Cells[coProdutoId, i]);
      vItensTemp[i - 1].Quantidade           := SFormatDouble(sgItens.Cells[coQuantidade, i]);
      vItensTemp[i - 1].Lote                 := sgItens.Cells[coLote, i];
    end;

    vRetornoBanco :=
      _TransferenciasLocaisTemp.AtualizarTransferenciaLocal(
        Sessao.getConexaoBanco,
        eID.AsInt,
        Sessao.getEmpresaLogada.EmpresaId,
        FrLocalOrigem.GetLocais.local_id,
        eObservacao.Text,
        'P',
        vItensTemp
      );
  end;

  if vRetornoBanco.TeveErro then begin
    Exclamar(vRetornoBanco.MensagemErro);
    Abort;
  end;

  if vRetornoBanco.AsInt > 0 then
    _Biblioteca.Informar('Nova transfer�ncia de locais efetuada com sucesso, c�digo: ' + NFormat(vRetornoBanco.AsInt));
end;

procedure TFormTransferenciaLocais.Modo(pEditando: Boolean);
begin
  inherited;
   _Biblioteca.Habilitar([
    FrLocalOrigem,
    eObservacao,
    FrProduto,
    eQuantidade,
    sgItens,
    eQuantidade,
    stEstoqueFisico,
    stEstoqueDisponivel],
    pEditando
  );

  if pEditando then
    SetarFoco(FrLocalOrigem);
end;

procedure TFormTransferenciaLocais.PesquisarRegistro;
begin
  inherited;

end;

procedure TFormTransferenciaLocais.sbGravarClick(Sender: TObject);
var
  vLinha: Integer;
begin
  cdsProduto.Close;
  cdsProduto.CreateDataSet;
  cdsProduto.Open;

  for vLinha :=  sgItens.RowCount-1 downto 1 do
  begin
    cdsProduto.Insert;
    cdsProdutocodigoProd.AsString  := sgItens.Cells[coProdutoId, vLinha];
    cdsProdutoNomeProduto.AsString := sgItens.Cells[coNomeProduto, vLinha];
    cdsProdutoLote.AsString        := sgItens.Cells[coLote, vLinha];
    cdsProdutoMarca.AsString       := sgItens.Cells[coMarca, vLinha];
    cdsProdutoUnidade.AsString     := sgItens.Cells[coUnidade, vLinha];
    cdsProdutoQuantidade.AsFloat   := StrToFloatDef(sgItens.Cells[coQuantidade, vLinha],0);
    cdsProdutoLocalDestino.AsString:= sgItens.Cells[coLocalDestino, vLinha];
    cdsProdutoLocalOrigem.AsString := IntToStr(FrLocalOrigem.GetLocais().local_id) +' '+ FrLocalOrigem.GetLocais().nome;
    cdsProdutoObservacao.AsString  := eObservacao.Text;
    cdsProduto.Post;
  end;
  inherited;
  frxReport.ShowReport;
//  cdsProduto.Free;
end;

procedure TFormTransferenciaLocais.sgItensDblClick(Sender: TObject);
begin
  inherited;
  FrProduto.InserirDadoPorChave(SFormatInt(sgItens.Cells[coProdutoId, sgItens.Row]));
  FrLocalDestino.InserirDadoPorChave( SFormatInt(sgItens.Cells[coLocalDestinoId, sgItens.Row]) );
  eQuantidade.AsDouble := SFormatDouble(sgItens.Cells[coQuantidade, sgItens.Row]);
  FrLote.Lote := sgItens.Cells[coLote, sgItens.Row];
  FrLote.DataFabricacao := ToData(sgItens.Cells[coDataFabricacao, sgItens.Row]);
  FrLote.DataVencimento := ToData(sgItens.Cells[coDataVencimento, sgItens.Row]);

  FLinha := sgItens.Row;
  SetarFoco(FrProduto);
end;

procedure TFormTransferenciaLocais.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coProdutoId, coQuantidade] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormTransferenciaLocais.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if sgItens.Cells[coProdutoId, 1] = '' then begin
    _Biblioteca.Exclamar('Nenhum produto foi informado!');
    SetarFoco(FrProduto);
    Abort;
  end;
end;

end.
