unit BaixarItensCompras;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Grids, GridLuka, _RecordsEspeciais,
  Vcl.Buttons, Vcl.ExtCtrls, _Biblioteca, _Sessao, _Compras, _ComprasItens, _ComprasBaixas,
  Vcl.StdCtrls, StaticTextLuka, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, _ComprasItensBaixas,
  FrameLocais, EditLuka, Frame.Lotes, _AjustesEstoque, _RecordsEstoques, _RecordsCadastros,
  _MotivosAjusteEstoque, Vcl.Menus;

type
  TFormBaixarItensCompras = class(TFormHerancaFinalizar)
    sgItens: TGridLuka;
    StaticTextLuka1: TStaticTextLuka;
    FrLocal: TFrLocais;
    lbl7: TLabel;
    eQtdeBaixar: TEditLuka;
    sgItensBaixar: TGridLuka;
    StaticTextLuka2: TStaticTextLuka;
    FrLote: TFrameLotes;
    stItemSelecionado: TStaticText;
    st3: TStaticText;
    Label1: TLabel;
    procedure sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure sgItensDblClick(Sender: TObject);
    procedure sgItensBaixarDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure eQtdeBaixarKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgItensBaixarKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    FCompraId: Integer;
    FEmpresaId: Integer;

    procedure AddNoGrid(
      pItemId: Integer;
      pProdutoId: Integer;
      pNome: string;
      pLocalId: Integer;
      pNomeLocal: string;
      pLote: string;
      pQuantidadeEmbalagem: Double;
      pQuantidadeBaixar: Double;
      pPrecoUnitario: Double
    );
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Baixar(pCompraId: Integer; pEmpresaId: Integer): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

uses _BibliotecaGenerica;

const
  (* Grid de itens *)
  coProdutoId                = 0;
  coNome                     = 1;
  coMarca                    = 2;
  coUnidade                  = 3;
  coQuantidade               = 4;
  coEntregues                = 5;
  coBaixados                 = 6;
  coCancelados               = 7;
  coSaldo                    = 8;
  coQtdeBaixar               = 9;
  (* Ocultas *)
  coItemId                   = 10;
  coMultiploCompra           = 11;
  coTipoControleEstoque      = 12;
  coExigirDataFabricacaoLote = 13;
  coExigirDataVencimentoLote = 14;
  coQuantidadeEmbalagem      = 15;
  coPrecoUnitario            = 16;

  (* Grid de itens para baixa *)
  cbProdutoId           = 0;
  cbNome                = 1;
  cbLocal               = 2;
  cbLote                = 3;
  cbQtdeBaixar          = 4;
  (* Ocultas *)
  cbLocalId             = 5;
  cbItemId              = 6;
  cbQuantidadeEmbalagem = 7;
  cbPrecoUnitario       = 8;

function Baixar(pCompraId: Integer; pEmpresaId: Integer): TRetornoTelaFinalizar;
var
  i: Integer;
  vForm: TFormBaixarItensCompras;
  vItens: TArray<RecComprasItens>;
begin
  if pCompraId = 0 then
    Exit;

  vItens := _ComprasItens.BuscarComprasItens(Sessao.getConexaoBanco, 1, [pCompraId]);
  if vItens = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vForm := TFormBaixarItensCompras.Create(nil);
  vForm.FCompraId := pCompraId;
  vForm.FEmpresaId := pEmpresaId;

  for i := Low(vItens) to High(vItens) do begin
    vForm.sgItens.Cells[coProdutoId, i + 1]      := _Biblioteca.NFormat(vItens[i].ProdutoId);
    vForm.sgItens.Cells[coNome, i + 1]           := vItens[i].NomeProduto;
    vForm.sgItens.Cells[coMarca, i + 1]          := _Biblioteca.NFormat(vItens[i].MarcaId) + ' - ' + vItens[i].NomeMarca;
    vForm.sgItens.Cells[coUnidade, i + 1]        := vItens[i].UnidadeCompraId;
    vForm.sgItens.Cells[coQuantidade, i + 1]     := _Biblioteca.NFormatEstoque(vItens[i].Quantidade);
    vForm.sgItens.Cells[coEntregues, i + 1]      := _Biblioteca.NFormatEstoque(vItens[i].Entregues);
    vForm.sgItens.Cells[coBaixados, i + 1]       := _Biblioteca.NFormatEstoque(vItens[i].Baixados);
    vForm.sgItens.Cells[coCancelados, i + 1]     := _Biblioteca.NFormatEstoque(vItens[i].Cancelados);
    vForm.sgItens.Cells[coSaldo, i + 1]          := _Biblioteca.NFormatEstoque(vItens[i].Saldo);
    vForm.sgItens.Cells[coMultiploCompra, i + 1] := _Biblioteca.NFormat(vItens[i].MultiploCompra, 4);
    vForm.sgItens.Cells[coItemId , i + 1]        := _Biblioteca.NFormat(vItens[i].ItemId);
    vForm.sgItens.Cells[coQuantidadeEmbalagem , i + 1] := _Biblioteca.NFormat(vItens[i].QuantidadeEmbalagem, 3);

    vForm.sgItens.Cells[coTipoControleEstoque, i + 1]      := vItens[i].TipoControleEstoque;
    vForm.sgItens.Cells[coExigirDataFabricacaoLote, i + 1] := vItens[i].ExigirDataFabricacaoLote;
    vForm.sgItens.Cells[coExigirDataVencimentoLote, i + 1] := vItens[i].ExigirDataVencimentoLote;
    vForm.sgItens.Cells[coPrecoUnitario, i + 1]            := _Biblioteca.NFormat(vItens[i].PrecoUnitario);
  end;
  vForm.sgItens.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vItens) );

  Result.Ok(vForm.ShowModal, True);
end;

procedure TFormBaixarItensCompras.AddNoGrid(
  pItemId: Integer;
  pProdutoId: Integer;
  pNome: string;
  pLocalId: Integer;
  pNomeLocal: string;
  pLote: string;
  pQuantidadeEmbalagem: Double;
  pQuantidadeBaixar: Double;
  pPrecoUnitario: Double
);
var
  vLinha: Integer;
begin
  vLinha := sgItensBaixar.Localizar([cbProdutoId, cbLocalId, cbLote], [_Biblioteca.NFormat(pProdutoId), _Biblioteca.NFormat(pLocalId), pLote]);
  if vLinha = -1 then begin
    if sgItensBaixar.Cells[cbProdutoId, 1] = '' then
      vLinha := 1
    else begin
      vLinha := sgItensBaixar.RowCount;
      sgItensBaixar.RowCount := sgItensBaixar.RowCount + 1;
    end;
  end;

  sgItensBaixar.Cells[cbProdutoId, vLinha]           := _Biblioteca.NFormat(pProdutoId);
  sgItensBaixar.Cells[cbNome, vLinha]                := pNome;
  sgItensBaixar.Cells[cbLocal, vLinha]               := _Biblioteca.NFormat(pLocalId) + ' - ' + pNomeLocal;
  sgItensBaixar.Cells[cbLote, vLinha]                := pLote;
  sgItensBaixar.Cells[cbQtdeBaixar, vLinha]          := _Biblioteca.NFormat(pQuantidadeBaixar, 3);
  sgItensBaixar.Cells[cbItemId, vLinha]              := _Biblioteca.NFormat(pItemId);
  sgItensBaixar.Cells[cbLocalId, vLinha]             := _Biblioteca.NFormat(pLocalId);
  sgItensBaixar.Cells[cbQuantidadeEmbalagem, vLinha] := _Biblioteca.NFormat(pQuantidadeEmbalagem, 3);
  sgItensBaixar.Cells[cbPrecoUnitario, vLinha]       := _Biblioteca.NFormat(pPrecoUnitario, 3);
end;

procedure TFormBaixarItensCompras.eQtdeBaixarKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if FrLocal.EstaVazio then begin
    _Biblioteca.Exclamar('O local de baixa do produto n�o foi informado corretamente, verifique!');
    _Biblioteca.SetarFoco(FrLocal);
    Exit;
  end;

  if not FrLote.LoteValido then begin
    _Biblioteca.Exclamar('O lote do produto n�o foi informado corretamente, verifique!');
    _Biblioteca.SetarFoco(FrLote);
    Exit;
  end;

  if eQtdeBaixar.AsCurr = 0 then begin
    _Biblioteca.Exclamar('A quantidade a se baixar n�o foi definida corretamente, verifique!');
    _Biblioteca.SetarFoco(eQtdeBaixar);
    Exit;
  end;

  if SFormatCurr(sgItens.Cells[coQtdeBaixar, sgItens.Row]) + eQtdeBaixar.AsCurr > SFormatCurr(sgItens.Cells[coSaldo, sgItens.Row]) then begin
    _Biblioteca.Exclamar(
      'A quantidade j� defininida a se baixar mais a quantidade que est� sendo definida n�o pode ' +
      'ser maior que a quantidade dispon�vel para o produto ' + sgItens.Cells[coNome, sgItens.Row] + ', verifique!'
    );
    _Biblioteca.SetarFoco(eQtdeBaixar);
    Exit;
  end;

  AddNoGrid(
    SFormatInt(sgItens.Cells[coItemId, sgItens.Row]),
    SFormatInt(sgItens.Cells[coProdutoId, sgItens.Row]),
    sgItens.Cells[coNome, sgItens.Row],
    FrLocal.GetLocais.local_id,
    FrLocal.GetLocais.nome,
    FrLote.Lote,
    SFormatDouble(sgItens.Cells[coQuantidadeEmbalagem, sgItens.Row]),
    eQtdeBaixar.AsDouble,
    SFormatDouble(sgItens.Cells[coPrecoUnitario, sgItens.Row])
  );

  sgItens.Cells[coQtdeBaixar, sgItens.Row] := _Biblioteca.NFormatN( SFormatDouble(sgItens.Cells[coQtdeBaixar, sgItens.Row]) + eQtdeBaixar.AsDouble );

  _Biblioteca.LimparCampos([FrLocal, eQtdeBaixar]);
  _Biblioteca.SetarFoco(FrLocal);
end;

procedure TFormBaixarItensCompras.Finalizar(Sender: TObject);
var
  i: Integer;
  vMotivoAjuste: RecMotivoAjusteEstoque;

  vValorTotal: Double;
  vRetBanco: RecRetornoBD;
  vItens: TArray<RecComprasItensBaixas>;
  vItensAjuste: TArray<RecAjusteEstoqueItens>;
begin
  vItens := nil;
  vValorTotal := 0;

  for i := 1 to sgItensBaixar.RowCount -1 do begin
    if sgItensBaixar.Cells[cbQtdeBaixar, i] = '' then
      Continue;

    SetLength(vItens, Length(vItens) + 1);
    SetLength(vItensAjuste, Length(vItensAjuste) + 1);

    vItens[High(vItens)].ItemId     := SFormatInt( sgItensBaixar.Cells[cbItemId, i] );
    vItens[High(vItens)].Quantidade := SFormatDouble( sgItensBaixar.Cells[cbQtdeBaixar, i] );

    vItensAjuste[High(vItensAjuste)].Quantidade    := SFormatDouble( sgItensBaixar.Cells[cbQtdeBaixar, i] ) * (SFormatDouble( sgItensBaixar.Cells[cbQuantidadeEmbalagem, i] ));
    vItensAjuste[High(vItensAjuste)].LocalId       := SFormatInt( sgItensBaixar.Cells[cbLocalId, i] );
    vItensAjuste[High(vItensAjuste)].Lote          := sgItensBaixar.Cells[cbLote, i];
    vItensAjuste[High(vItensAjuste)].Natureza      := 'E';
    vItensAjuste[High(vItensAjuste)].produto_id    := SFormatInt(sgItensBaixar.Cells[coProdutoId, i]);
    vItensAjuste[High(vItensAjuste)].item_id       := SFormatInt(sgItensBaixar.Cells[cbItemId, i]);

    vItensAjuste[High(vItensAjuste)].PrecoUnitario :=
      _BibliotecaGenerica.zvl(_Biblioteca.Arredondar(SFormatDouble(sgItensBaixar.Cells[cbPrecoUnitario, i]) / SFormatDouble( sgItensBaixar.Cells[cbQuantidadeEmbalagem, i] ), 3), 0.01);

    vItensAjuste[High(vItensAjuste)].ValorTotal :=
      _Biblioteca.Arredondar(vItensAjuste[High(vItensAjuste)].PrecoUnitario * vItensAjuste[High(vItensAjuste)].Quantidade, 2);

    vValorTotal := vValorTotal + vItensAjuste[High(vItensAjuste)].ValorTotal;
  end;

  if vItens = nil then begin
    _Biblioteca.Exclamar('Nenhum item foi definido para baixa!');
    Abort;
  end;

  vMotivoAjuste := _MotivosAjusteEstoque.BuscarMotivoAjusteEstoques(Sessao.getConexaoBanco, 0, [Sessao.getParametros.MotivoAjuEstBxCompraId])[0];

  Sessao.getConexaoBanco.IniciarTransacao;

  vRetBanco := _ComprasBaixas.AtualizarComprasBaixas(Sessao.getConexaoBanco, FCompraId, vItens, True);
  Sessao.AbortarSeHouveErro( vRetBanco );

  vRetBanco :=
    _AjustesEstoque.AtualizarAjustesEstoque(
      Sessao.getConexaoBanco,
      0,
      FEmpresaId,
      Sessao.getParametros.MotivoAjuEstBxCompraId,
      'Requisi��o autom�tica ref. compra: ' + _Biblioteca.NFormat(FCompraId) + '.',
      vMotivoAjuste.PlanoFinanceiroId,
      'RCO',
      vRetBanco.AsInt,
      vValorTotal,
      'FISICO',
      vItensAjuste,
      True
    );
  Sessao.AbortarSeHouveErro( vRetBanco );

  Sessao.getConexaoBanco.FinalizarTransacao;

  _Biblioteca.Informar('Baixa de compra realizada com sucesso. C�digo: ' + _Biblioteca.NFormat(vRetBanco.AsInt));
  inherited;
end;

procedure TFormBaixarItensCompras.FormShow(Sender: TObject);
begin
  inherited;
  _Biblioteca.Habilitar([stItemSelecionado, FrLocal, FrLote, eQtdeBaixar], False);
end;

procedure TFormBaixarItensCompras.sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
begin
  inherited;
  TextCell := _Biblioteca.NFormatN( SFormatDouble(TextCell), 4 );

  if TextCell = '' then
    Exit;

  if not _Biblioteca.ValidarMultiplo( SFormatDouble(TextCell), SFormatDouble( sgItens.Cells[coMultiploCompra, ARow] ) ) then
    TextCell := '';

  if SFormatCurr( TextCell ) > SFormatCurr( sgItens.Cells[coSaldo, ARow] ) then begin
    _Biblioteca.Exclamar('A quantidade a cancelar n�o pode ser maior que o saldo dispon�vel, verifique!');
    TextCell := '';
  end;
end;

procedure TFormBaixarItensCompras.sgItensBaixarDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[cbProdutoId, cbQtdeBaixar] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItensBaixar.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormBaixarItensCompras.sgItensBaixarKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  vLinha: Integer;
begin
  inherited;
  if Key = VK_DELETE then begin
    vLinha := sgItens.Localizar([coProdutoId], [sgItensBaixar.Cells[cbProdutoId, sgItensBaixar.Row]]);
    sgItens.Cells[ coQtdeBaixar, vLinha ] := _Biblioteca.NFormatN( SFormatDouble(sgItens.Cells[coQtdeBaixar, vLinha]) - SFormatDouble(sgItensBaixar.Cells[cbQtdeBaixar, sgItensBaixar.Row]) );

    sgItensBaixar.DeleteRow( sgItensBaixar.Row );
  end;
end;

procedure TFormBaixarItensCompras.sgItensDblClick(Sender: TObject);
begin
  inherited;
  _Biblioteca.Habilitar([stItemSelecionado, FrLocal, eQtdeBaixar], True);
  stItemSelecionado.Caption := sgItens.Cells[coProdutoId, sgItens.Row] + ' - ' + sgItens.Cells[coNome, sgItens.Row];

  FrLote.Modo(True);
  FrLote.SetTipoControleEstoque(sgItens.Cells[coTipoControleEstoque, sgItens.Row], sgItens.Cells[coExigirDataFabricacaoLote, sgItens.Row], sgItens.Cells[coExigirDataVencimentoLote, sgItens.Row]);

  _Biblioteca.SetarFoco(FrLocal);
end;

procedure TFormBaixarItensCompras.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[
    coProdutoId,
    coQuantidade,
    coEntregues,
    coBaixados,
    coCancelados,
    coSaldo,
    coQtdeBaixar,
    coPrecoUnitario]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormBaixarItensCompras.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if Sessao.getParametros.MotivoAjuEstBxCompraId = 0 then begin
    _Biblioteca.Exclamar('O motivo de ajuste de estoque para baixas de compras n�o foi informado nos "Par�metros", por favor fa�a a parametriza��o!');
    Abort;
  end;
end;

end.
