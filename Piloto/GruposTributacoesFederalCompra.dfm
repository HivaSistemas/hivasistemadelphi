inherited FormGruposTributacoesFederalCompra: TFormGruposTributacoesFederalCompra
  Caption = 'Grupos de tribu'#231#245'es federal de compra'
  ClientHeight = 217
  ClientWidth = 628
  ExplicitWidth = 634
  ExplicitHeight = 246
  PixelsPerInch = 96
  TextHeight = 14
  object lb25: TLabel [1]
    Left = 126
    Top = 47
    Width = 102
    Height = 14
    Caption = 'Origem do produto'
  end
  object lb3: TLabel [2]
    Left = 126
    Top = 90
    Width = 83
    Height = 14
    Caption = 'CST entrada PIS'
  end
  object lb5: TLabel [3]
    Left = 126
    Top = 133
    Width = 105
    Height = 14
    Caption = 'CST entrada COFINS'
  end
  object lb1: TLabel [4]
    Left = 212
    Top = 5
    Width = 53
    Height = 14
    Caption = 'Descri'#231#227'o'
  end
  inherited pnOpcoes: TPanel
    Height = 217
    ExplicitHeight = 217
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 579
    OnKeyDown = ProximoCampo
    ExplicitLeft = 579
  end
  object cbOrigemProduto: TComboBoxLuka
    Left = 126
    Top = 61
    Width = 494
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    TabOrder = 3
    OnKeyDown = ProximoCampo
    Items.Strings = (
      '0 - Nacional'
      '1 - Entrangeira(Imp. Direta)'
      '2 - Estrangeira(Adiq. merc. interno)'
      '3 - Nacional(Com cont. imp. superior 40%)'
      '4 - Nacional(Produ'#231#227'o feita em conf. decreto 288/67)'
      '5 - Nacional(Com cont. imp. inferior 40%)'
      '6 - Estrangeira(Imp. direta, sem similiar merc. interno)'
      '7 - Estrangeira(Adiq. merc. interno, sem similiar merc. interno)'
      '8 - Nacional(Com cont. de importa'#231#227'o superior a 70%)')
    Valores.Strings = (
      '0'
      '1'
      '2'
      '3'
      '4'
      '5'
      '6'
      '7'
      '8')
    AsInt = 0
  end
  object cbCstEntradaPIS: TComboBoxLuka
    Left = 126
    Top = 104
    Width = 494
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Anchors = [akLeft, akTop, akRight]
    Color = clWhite
    TabOrder = 4
    OnKeyDown = ProximoCampo
    AsInt = 0
  end
  object cbCstEntradaCOFINS: TComboBoxLuka
    Left = 126
    Top = 147
    Width = 494
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Anchors = [akLeft, akTop, akRight]
    Color = clWhite
    TabOrder = 5
    OnKeyDown = ProximoCampo
    AsInt = 0
  end
  object eDescricao: TEditLuka
    Left = 212
    Top = 19
    Width = 360
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 6
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
