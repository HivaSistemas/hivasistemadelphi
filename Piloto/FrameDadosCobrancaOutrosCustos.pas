unit FrameDadosCobrancaOutrosCustos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, StaticTextLuka, _Biblioteca,
  Vcl.StdCtrls, Vcl.Grids, GridLuka, FrameFornecedores, _FrameHenrancaPesquisas, System.DateUtils,
  FrameTiposCobranca, Vcl.Mask, EditLukaData, EditLuka, Vcl.ExtCtrls, _BibliotecaGenerica, _Sessao,
  _RecordsFinanceiros, FramePlanosFinanceiros, FrameCentroCustos;

type
  TFrDadosCobrancaOutrosCustos = class(TFrameHerancaPrincipal)
    pnInformacoesPrincipais: TPanel;
    lbllb10: TLabel;
    lbllb9: TLabel;
    lbllb6: TLabel;
    lbllb2: TLabel;
    lbl2: TLabel;
    eRepetir: TEditLuka;
    ePrazo: TEditLuka;
    eValorCobranca: TEditLuka;
    eDataVencimento: TEditLukaData;
    FrTiposCobranca: TFrTiposCobranca;
    eDocumento: TEditLuka;
    FrFornecedor: TFrFornecedores;
    pn1: TPanel;
    sgCobrancas: TGridLuka;
    stValorTotalDefinido: TStaticText;
    stTotalDefinido: TStaticText;
    st3: TStaticTextLuka;
    stQtdeTitulos: TStaticText;
    st1: TStaticText;
    stPrazoMedioCalc: TStaticText;
    FrPlanoFinanceiro: TFrPlanosFinanceiros;
    FrCentroCustos: TFrCentroCustos;
    procedure eDocumentoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgCobrancasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgCobrancasKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgCobrancasDblClick(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
  private
    FLinhaEditando: Integer;
    procedure CalcularPagamentos;
    function getTitulos: TArray<RecTitulosFinanceiros>;
    procedure setTitulos(pTitulos: TArray<RecTitulosFinanceiros>);
    function getValorTotal: Double;
  published
    property Titulos: TArray<RecTitulosFinanceiros> read getTitulos write setTitulos;
    property ValorTotal: Double read getValorTotal;
  end;

implementation

{$R *.dfm}

const
  coCobrancaId         = 0;
  coNomeTipoCobranca   = 1;
  coFornecedorId       = 2;
  coNomeFornecedor     = 3;
  coValorCobranca      = 4;
  coDocumento          = 5;
  coParcela            = 6;
  coQtdeParcelas       = 7;
  coDataVencimento     = 8;
  coPlanoFinanceiro    = 9;
  coCentroCusto        = 10;

  (* Ocultas *)
  coPlanoFinanceiroId = 11;
  coCentroCustoId     = 12;

procedure TFrDadosCobrancaOutrosCustos.CalcularPagamentos;
var
  i: Integer;
  vDataAtual: TDateTime;
  vQtdeTitulos: Integer;
  vDiasPrazoMedio: Double;
begin
  vQtdeTitulos := 0;
  vDiasPrazoMedio := 0;
  _Biblioteca.LimparCampos([stValorTotalDefinido, stQtdeTitulos, stPrazoMedioCalc]);

  if sgCobrancas.Cells[coCobrancaId, 1] = '' then
    Exit;

  vDataAtual := Sessao.getData;

  for i := 1 to sgCobrancas.RowCount -1 do begin
    if sgCobrancas.Cells[coCobrancaId, i] <> '' then
      Inc(vQtdeTitulos);

    stValorTotalDefinido.Caption := _Biblioteca.NFormat( SFormatDouble(stValorTotalDefinido.Caption) + SFormatDouble(sgCobrancas.Cells[coValorCobranca, i]) );
    vDiasPrazoMedio := vDiasPrazoMedio + ToDataHora(sgCobrancas.Cells[coDataVencimento, i]) - vDataAtual;
  end;

  stQtdeTitulos.Caption    := _Biblioteca.NFormatN(vQtdeTitulos);
  stPrazoMedioCalc.Caption := _Biblioteca.NFormatN(vDiasPrazoMedio / vQtdeTitulos);
end;

constructor TFrDadosCobrancaOutrosCustos.Create(AOwner: TComponent);
begin
  inherited;
  FLinhaEditando := -1;
  FrTiposCobranca.setNatureza('P');
end;

procedure TFrDadosCobrancaOutrosCustos.eDocumentoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  i: Integer;
  vLinha: Integer;
  vRepeticao: Integer;

  vValorRestante: Currency;
  vDataVencimento: TDateTime;
  vValorProporcional: Currency;
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if FrFornecedor.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio informar o fornecedor!');
    _Biblioteca.SetarFoco(FrFornecedor);
    Exit;
  end;

  if FrPlanoFinanceiro.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio informar o plano financeiro!');
    _Biblioteca.SetarFoco(FrPlanoFinanceiro);
    Exit;
  end;

  if FrCentroCustos.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio informar o centro de custos!');
    _Biblioteca.SetarFoco(FrCentroCustos);
    Exit;
  end;

  if FrTiposCobranca.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio informar o tipo de cobran�a!');
    _Biblioteca.SetarFoco(FrTiposCobranca);
    Exit;
  end;

  if not eDataVencimento.DataOk then begin
    _Biblioteca.Exclamar('A data de vencimento n�o foi definida corretamente!');
    _Biblioteca.SetarFoco(eDataVencimento);
    Exit;
  end;

  if eDocumento.Trim = '' then begin
    _Biblioteca.Exclamar('O documento n�o foi informado corretamente!');
    _Biblioteca.SetarFoco(eDocumento);
    Exit;
  end;

  vDataVencimento := eDataVencimento.AsData;
  vRepeticao := _BibliotecaGenerica.zvl(eRepetir.AsInt, 1);

  vValorProporcional := Arredondar( eValorCobranca.AsCurr / vRepeticao, 2 );
  vValorRestante := ( eValorCobranca.AsCurr - (vValorProporcional * vRepeticao) );

  for i := 1 to vRepeticao do begin
    if sgCobrancas.Cells[coDataVencimento, 1] = '' then
      vLinha := 1
    else begin
      vLinha := sgCobrancas.RowCount - 1;
      Inc(vLinha);
    end;

    sgCobrancas.Cells[coDataVencimento, vLinha]     := DFormat(vDataVencimento);
    sgCobrancas.Cells[coValorCobranca, vLinha]      := _Biblioteca.NFormat(vValorProporcional + IIfDbl(i = 1, vValorRestante) ); // Adiciona o valor que iria sobrar na primeira parcela
    sgCobrancas.Cells[coFornecedorId, vLinha]       := _Biblioteca.NFormat(FrFornecedor.GetFornecedor().cadastro_id);
    sgCobrancas.Cells[coNomeFornecedor, vLinha]     := FrFornecedor.GetFornecedor().cadastro.razao_social;
    sgCobrancas.Cells[coValorCobranca, vLinha]      := _Biblioteca.NFormat(vValorProporcional + IIfDbl(i = 1, vValorRestante) ); // Adiciona o valor que iria sobrar na primeira parcela
    sgCobrancas.Cells[coDocumento, vLinha]          := eDocumento.Text;
    sgCobrancas.Cells[coNomeTipoCobranca, vLinha]   := FrTiposCobranca.GetTipoCobranca.Nome;
    sgCobrancas.Cells[coCobrancaId, vLinha]         := _Biblioteca.NFormat(FrTiposCobranca.GetTipoCobranca.CobrancaId);
    sgCobrancas.Cells[coPlanoFinanceiro, vLinha]    := getInformacao(FrPlanoFinanceiro.getDados().PlanoFinanceiroId, FrPlanoFinanceiro.getDados().Descricao);
    sgCobrancas.Cells[coCentroCusto, vLinha]        := getInformacao(FrCentroCustos.GetCentro.CentroCustoId, FrCentroCustos.GetCentro.Nome);

    sgCobrancas.Cells[coPlanoFinanceiroId, vLinha]  := FrPlanoFinanceiro.getDados().PlanoFinanceiroId;
    sgCobrancas.Cells[coCentroCustoId, vLinha]      := _Biblioteca.NFormatN(FrCentroCustos.GetCentro.CentroCustoId);

    if FLinhaEditando = -1 then begin
      sgCobrancas.Cells[coParcela, vLinha]          := _Biblioteca.NFormat(i);
      sgCobrancas.Cells[coQtdeParcelas, vLinha]     := _Biblioteca.NFormat(vRepeticao);
    end;

    if ePrazo.AsInt > 0 then
      vDataVencimento := IncDay(vDataVencimento, ePrazo.AsInt);

    if FLinhaEditando = -1 then
      sgCobrancas.RowCount := vLinha + 1;
  end;

  FLinhaEditando := -1;

  _Biblioteca.LimparCampos([
    FrFornecedor,
    eDataVencimento,
    eValorCobranca,
    eRepetir,
    ePrazo,
    FrTiposCobranca,
    eDocumento]
  );

  _Biblioteca.Habilitar([FrFornecedor, ePrazo, eRepetir], True);
  _Biblioteca.SetarFoco(FrTiposCobranca);
   FLinhaEditando := -1;
   CalcularPagamentos;
end;

function TFrDadosCobrancaOutrosCustos.getTitulos: TArray<RecTitulosFinanceiros>;
var
  i: Integer;
begin
  Result := nil;

  if sgCobrancas.Cells[coCobrancaId, 1] = '' then
    Exit;

  SetLength(Result, sgCobrancas.RowCount -1);
  for i := 1 to sgCobrancas.RowCount -1 do begin
    Result[i - 1].ItemId         := i;
    Result[i - 1].CobrancaId     := SFormatInt( sgCobrancas.Cells[coCobrancaId, i] );
    Result[i - 1].NomeCobranca   := sgCobrancas.Cells[coNomeTipoCobranca, i];

    Result[i - 1].FornecedorId   := SFormatInt( sgCobrancas.Cells[coFornecedorId, i] );
    Result[i - 1].NomeFornecedor := sgCobrancas.Cells[coNomeFornecedor, i];

    Result[i - 1].DataVencimento := ToData( sgCobrancas.Cells[coDataVencimento, i] );
    Result[i - 1].Valor          := SFormatDouble( sgCobrancas.Cells[coValorCobranca, i] );
    Result[i - 1].Parcela        := SFormatInt( sgCobrancas.Cells[coParcela, i] );
    Result[i - 1].NumeroParcelas := SFormatInt( sgCobrancas.Cells[coQtdeParcelas, i] );
    Result[i - 1].Documento      := sgCobrancas.Cells[coDocumento, i];

    Result[i - 1].PlanoFinanceiroId := sgCobrancas.Cells[coPlanoFinanceiroId, i];
    Result[i - 1].CentroCustoId     := SFormatInt( sgCobrancas.Cells[coCentroCustoId, i] );
  end;
end;

function TFrDadosCobrancaOutrosCustos.getValorTotal: Double;
begin
  Result := _Biblioteca.SFormatDouble(stValorTotalDefinido.Caption);
end;

procedure TFrDadosCobrancaOutrosCustos.setTitulos(pTitulos: TArray<RecTitulosFinanceiros>);
var
  i: Integer;
begin
  if pTitulos = nil then
    Exit;

  for i := Low(pTitulos) to High(pTitulos) do begin
    sgCobrancas.Cells[coCobrancaId, i + 1]       := _Biblioteca.NFormat(pTitulos[i].CobrancaId);
    sgCobrancas.Cells[coNomeTipoCobranca, i + 1] := pTitulos[i].NomeCobranca;
    sgCobrancas.Cells[coFornecedorId, i + 1]     := _Biblioteca.NFormat(pTitulos[i].FornecedorId);
    sgCobrancas.Cells[coNomeFornecedor, i + 1]   := pTitulos[i].NomeFornecedor;
    sgCobrancas.Cells[coDataVencimento, i + 1]   := _Biblioteca.DFormat(pTitulos[i].DataVencimento);
    sgCobrancas.Cells[coValorCobranca, i + 1]    := _Biblioteca.NFormat( pTitulos[i].Valor );
    sgCobrancas.Cells[coParcela, i + 1]          := _Biblioteca.NFormat( pTitulos[i].Parcela );
    sgCobrancas.Cells[coQtdeParcelas, i + 1]     := _Biblioteca.NFormat( pTitulos[i].NumeroParcelas );
    sgCobrancas.Cells[coDocumento, i + 1]        := pTitulos[i].Documento;
    sgCobrancas.Cells[coPlanoFinanceiro, i + 1]  := getInformacao(pTitulos[i].PlanoFinanceiroId, '');
    sgCobrancas.Cells[coCentroCusto, i + 1]      := _Biblioteca.NFormatN(pTitulos[i].CentroCustoId);

    sgCobrancas.Cells[coPlanoFinanceiroId, i + 1]:= pTitulos[i].PlanoFinanceiroId;
    sgCobrancas.Cells[coCentroCustoId, i + 1]    := _Biblioteca.NFormatN(pTitulos[i].CentroCustoId);
  end;
  sgCobrancas.SetLinhasGridPorTamanhoVetor( Length(pTitulos) );

  CalcularPagamentos;
end;

procedure TFrDadosCobrancaOutrosCustos.sgCobrancasDblClick(Sender: TObject);
begin
  inherited;
  if sgCobrancas.Cells[coDataVencimento, sgCobrancas.Row] = '' then
    Exit;

  FLinhaEditando := sgCobrancas.Row;

  FrTiposCobranca.InserirDadoPorChave(SFormatInt(sgCobrancas.Cells[coCobrancaId, FLinhaEditando]), False);
  FrFornecedor.InserirDadoPorChave(SFormatInt(sgCobrancas.Cells[coFornecedorId, FLinhaEditando]), False);

  eDataVencimento.AsData  := ToData(sgCobrancas.Cells[coDataVencimento, FLinhaEditando]);
  eValorCobranca.AsDouble := SFormatDouble(sgCobrancas.Cells[coValorCobranca, FLinhaEditando]);
  eDocumento.Text         := sgCobrancas.Cells[coDocumento, FLinhaEditando];

  _Biblioteca.Habilitar([FrTiposCobranca, ePrazo, eRepetir, FrFornecedor], False, False);
  _Biblioteca.SetarFoco(eDataVencimento);
end;

procedure TFrDadosCobrancaOutrosCustos.sgCobrancasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    coCobrancaId,
    coFornecedorId,
    coValorCobranca,
    coParcela,
    coQtdeParcelas]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgCobrancas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFrDadosCobrancaOutrosCustos.sgCobrancasKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_DELETE then begin
    sgCobrancas.DeleteRow( sgCobrancas.Row );
    CalcularPagamentos;
  end;
end;

end.
