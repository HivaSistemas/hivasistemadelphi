inherited FormPesquisaFuncoesFuncionario: TFormPesquisaFuncoesFuncionario
  Caption = 'Pesquisa cargos de usu'#225'rios'
  ClientHeight = 270
  ClientWidth = 534
  ExplicitWidth = 542
  ExplicitHeight = 301
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 534
    Height = 224
    ColCount = 4
    OnDrawCell = sgPesquisaDrawCell
    CorLinhaFoco = 38619
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Nome'
      'Ativo')
    OnGetCellColor = sgPesquisaGetCellColor
    RealColCount = 4
    ExplicitWidth = 534
    ExplicitHeight = 224
    ColWidths = (
      28
      55
      256
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Text = '0'
  end
  inherited pnFiltro1: TPanel
    Width = 534
    ExplicitWidth = 534
    inherited eValorPesquisa: TEditLuka
      Width = 324
      ExplicitWidth = 324
    end
  end
end
