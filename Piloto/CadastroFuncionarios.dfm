inherited FormCadastroFuncionarios: TFormCadastroFuncionarios
  Caption = 'Cadastro de usu'#225'rios'
  ClientHeight = 398
  ClientWidth = 737
  ExplicitWidth = 743
  ExplicitHeight = 427
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 259
    Top = 47
    Width = 32
    Height = 14
    Caption = 'Nome'
  end
  object lb2: TLabel [1]
    Left = 505
    Top = 136
    Width = 74
    Height = 14
    Caption = 'Data de nasc.'
  end
  object lb3: TLabel [2]
    Left = 259
    Top = 92
    Width = 43
    Height = 14
    Caption = 'Apelido'
  end
  object lb4: TLabel [3]
    Left = 403
    Top = 92
    Width = 18
    Height = 14
    Caption = 'CPF'
  end
  object lb5: TLabel [4]
    Left = 505
    Top = 92
    Width = 15
    Height = 14
    Caption = 'RG'
  end
  object lb6: TLabel [5]
    Left = 610
    Top = 92
    Width = 57
    Height = 14
    Caption = 'Org'#227'o exp.'
  end
  object lb17: TLabel [6]
    Left = 568
    Top = 12
    Width = 76
    Height = 14
    Caption = 'Data cadastro'
  end
  inherited pnOpcoes: TPanel
    Height = 398
    ExplicitHeight = 398
    inherited sbDesfazer: TSpeedButton
      Top = 102
      ExplicitTop = 102
    end
    inherited sbExcluir: TSpeedButton
      Top = 153
      ExplicitTop = 153
    end
    inherited sbPesquisar: TSpeedButton
      Top = 50
      ExplicitTop = 50
    end
    inherited sbLogs: TSpeedButton
      Left = 0
      Top = 345
      ExplicitLeft = 0
      ExplicitTop = 345
    end
  end
  inherited eID: TEditLuka
    TabOrder = 10
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 688
    Top = 4
    TabOrder = 11
    ExplicitLeft = 688
    ExplicitTop = 4
  end
  object eNome: TEditLuka
    Left = 259
    Top = 62
    Width = 462
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 1
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eDataNascimento: TEditLukaData
    Left = 505
    Top = 151
    Width = 100
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '!99/99/0000;1;_'
    MaxLength = 10
    TabOrder = 6
    Text = '  /  /    '
    OnKeyDown = ProximoCampo
  end
  object eApelido: TEditLuka
    Left = 259
    Top = 107
    Width = 139
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 2
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object pcDados: TPageControl
    Left = 126
    Top = 179
    Width = 609
    Height = 217
    ActivePage = f
    TabOrder = 9
    object tsPrincipais: TTabSheet
      Caption = 'Principais'
      object lb9: TLabel
        Left = 3
        Top = 86
        Width = 35
        Height = 14
        Caption = 'E-mail'
      end
      object eEmail: TEditLuka
        Left = 3
        Top = 100
        Width = 291
        Height = 22
        CharCase = ecUpperCase
        TabOrder = 1
        OnKeyDown = ProximoCampo
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eServidorEmail: TEditLuka
        Left = 301
        Top = 100
        Width = 295
        Height = 22
        CharCase = ecUpperCase
        TabOrder = 2
        Visible = False
        OnKeyDown = ProximoCampo
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eNomeUsuarioEmail: TEditLuka
        Left = 3
        Top = 140
        Width = 291
        Height = 22
        CharCase = ecUpperCase
        TabOrder = 3
        Visible = False
        OnKeyDown = ProximoCampo
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eSenhaEmail: TEditLuka
        Left = 301
        Top = 140
        Width = 163
        Height = 22
        CharCase = ecUpperCase
        TabOrder = 4
        Visible = False
        OnKeyDown = ProximoCampo
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object ePortaServidorEmail: TEditLuka
        Left = 471
        Top = 140
        Width = 125
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 5
        Visible = False
        OnKeyDown = ePortaServidorEmailKeyDown
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      inline FrEndereco: TFrEndereco
        Left = 0
        Top = 0
        Width = 601
        Height = 88
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        ExplicitWidth = 601
        ExplicitHeight = 88
        inherited lb9: TLabel
          Left = 311
          ExplicitLeft = 311
        end
        inherited lb10: TLabel
          Left = 535
          ExplicitLeft = 535
        end
        inherited lb1: TLabel
          Left = 311
          ExplicitLeft = 311
        end
        inherited lb2: TLabel
          Left = 535
          ExplicitLeft = 535
        end
        inherited eCEP: TEditCEPLuka
          Left = 535
          Width = 65
          Height = 22
          OnKeyDown = ProximoCampo
          ExplicitLeft = 535
          ExplicitWidth = 65
          ExplicitHeight = 22
        end
        inherited eLogradouro: TEditLuka
          Width = 303
          Height = 22
          OnKeyDown = ProximoCampo
          ExplicitWidth = 303
          ExplicitHeight = 22
        end
        inherited eComplemento: TEditLuka
          Left = 311
          Height = 22
          OnKeyDown = ProximoCampo
          ExplicitLeft = 311
          ExplicitHeight = 22
        end
        inherited FrBairro: TFrBairros
          Width = 303
          ExplicitWidth = 303
          inherited sgPesquisa: TGridLuka
            Width = 278
            ExplicitWidth = 278
          end
          inherited PnTitulos: TPanel
            Width = 303
            ExplicitWidth = 303
            inherited lbNomePesquisa: TLabel
              Height = 15
            end
            inherited pnSuprimir: TPanel
              Left = 198
              ExplicitLeft = 198
            end
          end
          inherited pnPesquisa: TPanel
            Left = 278
            ExplicitLeft = 278
          end
        end
        inherited ckValidarComplemento: TCheckBox
          Width = 145
          Checked = True
          State = cbChecked
          ExplicitWidth = 145
        end
        inherited ckValidarBairro: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited ckValidarEndereco: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited ePontoReferencia: TEditLuka
          Left = 311
          Height = 22
          OnKeyDown = ProximoCampo
          ExplicitLeft = 311
          ExplicitHeight = 22
        end
        inherited eNumero: TEditLuka
          Left = 535
          Width = 65
          Height = 22
          OnKeyDown = ProximoCampo
          ExplicitLeft = 535
          ExplicitWidth = 65
          ExplicitHeight = 22
        end
        inherited ckValidarNumero: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
    end
    object tsDadosTrabalhistas: TTabSheet
      Caption = 'Dados trabalhistas'
      ImageIndex = 2
      object lb18: TLabel
        Left = 1
        Top = 1
        Width = 39
        Height = 14
        Caption = 'Sal'#225'rio'
      end
      object lb19: TLabel
        Left = 97
        Top = 1
        Width = 58
        Height = 14
        Caption = 'N'#186' carteira'
      end
      object lb20: TLabel
        Left = 223
        Top = 1
        Width = 28
        Height = 14
        Caption = 'S'#233'rie'
      end
      object lb21: TLabel
        Left = 319
        Top = 1
        Width = 54
        Height = 14
        Caption = 'PIS/Pasep'
      end
      object lb7: TLabel
        Left = 1
        Top = 42
        Width = 83
        Height = 14
        Caption = 'Data de admis.'
      end
      object lb22: TLabel
        Left = 97
        Top = 42
        Width = 66
        Height = 14
        Caption = 'Data deslig.'
      end
      object eSalario: TEditLuka
        Left = 1
        Top = 15
        Width = 90
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 8
        TabOrder = 0
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eNumeroCarteira: TEditLuka
        Left = 97
        Top = 15
        Width = 120
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 30
        TabOrder = 1
        OnKeyDown = ProximoCampo
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = True
      end
      object eSerieCarteira: TEditLuka
        Left = 223
        Top = 15
        Width = 90
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 10
        TabOrder = 2
        OnKeyDown = ProximoCampo
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = True
      end
      object ePisPasep: TEditLuka
        Left = 319
        Top = 15
        Width = 160
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 30
        TabOrder = 3
        OnKeyDown = ProximoCampo
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = True
      end
      object eDataAdmissao: TEditLukaData
        Left = 1
        Top = 56
        Width = 90
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '!99/99/0000;1;_'
        MaxLength = 10
        TabOrder = 4
        Text = '  /  /    '
        OnKeyDown = ProximoCampo
      end
      object eDataDesligamento: TEditLukaData
        Left = 97
        Top = 56
        Width = 90
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '!99/99/0000;1;_'
        MaxLength = 10
        TabOrder = 5
        Text = '  /  /    '
        OnKeyDown = ProximoCampo
      end
    end
    object f: TTabSheet
      Caption = 'Sistema'
      ImageIndex = 1
      object lb13: TLabel
        Left = 1
        Top = 2
        Width = 77
        Height = 14
        Caption = '% Com. a vista'
      end
      object lb14: TLabel
        Left = 95
        Top = 2
        Width = 81
        Height = 14
        Caption = '% Com. a prazo'
      end
      object lb15: TLabel
        Left = 189
        Top = 2
        Width = 94
        Height = 14
        Caption = '% Desc. de venda'
      end
      object lb16: TLabel
        Left = 302
        Top = 2
        Width = 111
        Height = 14
        Caption = '% Desc. adic. financ. '
      end
      object sbResetarSenha: TSpeedButton
        Left = 478
        Top = 166
        Width = 123
        Height = 22
        Caption = 'Resetar senha'
        OnClick = sbResetarSenhaClick
      end
      object lb23: TLabel
        Left = 440
        Top = 2
        Width = 90
        Height = 14
        Caption = '% Desc. por item'
      end
      object ePercentualComissaoVista: TEditLuka
        Left = 1
        Top = 16
        Width = 90
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 5
        TabOrder = 0
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object ePercentualComissaoPrazo: TEditLuka
        Left = 95
        Top = 16
        Width = 90
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 5
        TabOrder = 1
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object ePercentualDescontoAdicVenda: TEditLuka
        Left = 189
        Top = 16
        Width = 109
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 5
        TabOrder = 2
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object ePercentualDescontoAdicFinanceiro: TEditLuka
        Left = 301
        Top = 16
        Width = 133
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 5
        TabOrder = 3
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      inline FrCadastro: TFrameCadastros
        Left = 0
        Top = 43
        Width = 303
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        TabStop = True
        ExplicitTop = 43
        ExplicitWidth = 303
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 278
          Height = 23
          ExplicitWidth = 278
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 303
          ExplicitWidth = 303
          inherited lbNomePesquisa: TLabel
            Width = 54
            ExplicitWidth = 54
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 198
            ExplicitLeft = 198
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 278
          Height = 24
          ExplicitLeft = 278
          ExplicitHeight = 24
        end
      end
      object gbFuncoesUsuario: TGroupBoxLuka
        Left = 1
        Top = 87
        Width = 240
        Height = 74
        Caption = ' Fun'#231#245'es do usu'#225'rio  '
        TabOrder = 6
        OpcaoMarcarDesmarcar = False
        object ckVendedor: TCheckBoxLuka
          Left = 7
          Top = 19
          Width = 97
          Height = 17
          Caption = '1 - Vendedor'
          TabOrder = 0
          CheckedStr = 'N'
        end
        object ckCaixa: TCheckBoxLuka
          Left = 7
          Top = 35
          Width = 97
          Height = 17
          Caption = '2 - Caixa'
          TabOrder = 1
          CheckedStr = 'N'
        end
        object ckComprador: TCheckBoxLuka
          Left = 7
          Top = 51
          Width = 97
          Height = 17
          Caption = '3 - Comprador'
          TabOrder = 2
          CheckedStr = 'N'
        end
        object ckMotorista: TCheckBoxLuka
          Left = 127
          Top = 19
          Width = 97
          Height = 17
          Caption = '4 - Motorista'
          TabOrder = 3
          CheckedStr = 'N'
        end
        object ckAjudante: TCheckBoxLuka
          Left = 127
          Top = 35
          Width = 97
          Height = 17
          Caption = '5 - Ajudante'
          TabOrder = 4
          CheckedStr = 'N'
        end
      end
      object ePercDescItemPrecoManual: TEditLuka
        Left = 440
        Top = 16
        Width = 127
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 5
        TabOrder = 4
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object ckAcessaAltisW: TCheckBoxLuka
        Left = 365
        Top = 143
        Width = 125
        Height = 17
        Caption = 'Acessa AltisW'
        TabOrder = 10
        Visible = False
        CheckedStr = 'N'
      end
      object ckFinalizarSessao: TCheckBoxLuka
        Left = 341
        Top = 44
        Width = 156
        Height = 17
        Caption = 'finalizar sess'#227'o'
        Checked = True
        State = cbChecked
        TabOrder = 7
        CheckedStr = 'S'
      end
      object ckDevolucaoSomenteFiscal: TCheckBoxLuka
        Left = 341
        Top = 90
        Width = 156
        Height = 17
        Caption = 'Devolu'#231#227'o somente fiscal'
        TabOrder = 9
        CheckedStr = 'N'
      end
      object ckAcessoHivaMobile: TCheckBox
        Left = 341
        Top = 67
        Width = 159
        Height = 17
        Caption = 'Acesso hiva mobile'
        Enabled = False
        TabOrder = 8
        OnKeyDown = ProximoCampo
      end
    end
    object pcEmpresas: TTabSheet
      Caption = 'Empresas'
      ImageIndex = 3
      inline FrEmpresas: TFrEmpresas
        Left = 1
        Top = 0
        Width = 322
        Height = 188
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 1
        ExplicitWidth = 322
        ExplicitHeight = 188
        inherited sgPesquisa: TGridLuka
          Width = 297
          Height = 171
          ExplicitWidth = 297
          ExplicitHeight = 171
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 322
          ExplicitWidth = 322
          inherited lbNomePesquisa: TLabel
            Width = 53
            ExplicitWidth = 53
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 217
            ExplicitLeft = 217
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 297
          Height = 172
          ExplicitLeft = 297
          ExplicitHeight = 172
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Acrescimo comiss'#227'o'
      ImageIndex = 4
      object sbSalvarMeta: TSpeedButton
        Left = 490
        Top = 3
        Width = 108
        Height = 25
        Caption = 'Salvar'
        Enabled = False
        OnClick = sbSalvarMetaClick
      end
      object sbCancelarMeta: TSpeedButton
        Left = 490
        Top = 34
        Width = 108
        Height = 25
        Caption = 'Cancelar'
        Enabled = False
        OnClick = sbCancelarMetaClick
      end
      object sgFaixasAcrescimo: TGridLuka
        Left = 4
        Top = 54
        Width = 374
        Height = 134
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        ColCount = 4
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        Enabled = False
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goEditing]
        TabOrder = 0
        OnDrawCell = sgFaixasAcrescimoDrawCell
        OnKeyDown = sgFaixasAcrescimoKeyDown
        OnKeyPress = NumerosVirgula
        OnSelectCell = sgFaixasAcrescimoSelectCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = clWindow
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Faixa'
          'Valor inicial'
          'Valor final'
          '% Acrescimo')
        Grid3D = False
        RealColCount = 31
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          61
          119
          83
          81)
      end
      inline frEmpresa: TFrEmpresasFaturamento
        Left = 4
        Top = 2
        Width = 374
        Height = 41
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 4
        ExplicitTop = 2
        ExplicitWidth = 374
        ExplicitHeight = 41
        inherited sgPesquisa: TGridLuka
          Width = 349
          Height = 24
          ExplicitWidth = 349
          ExplicitHeight = 24
        end
        inherited PnTitulos: TPanel
          Width = 374
          ExplicitWidth = 374
          inherited lbNomePesquisa: TLabel
            Width = 138
            Caption = 'Empresa de faturamento:'
            ExplicitWidth = 138
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Left = 145
            ExplicitLeft = 145
          end
          inherited pnSuprimir: TPanel
            Left = 269
            ExplicitLeft = 269
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 349
          Height = 25
          ExplicitLeft = 349
          ExplicitHeight = 25
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
      end
    end
  end
  object eCPF: TEditCPF_CNPJ_Luka
    Left = 403
    Top = 107
    Width = 97
    Height = 22
    EditMask = '999.999.999-99'
    MaxLength = 14
    TabOrder = 3
    Text = '   .   .   -  '
    OnKeyDown = ProximoCampo
    Tipo = [tccCPF]
  end
  object eRG: TEditLuka
    Left = 505
    Top = 107
    Width = 100
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 4
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eOrgaoExpedidor: TEditLuka
    Left = 610
    Top = 107
    Width = 111
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 5
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eDataCadastro: TEditLukaData
    Left = 568
    Top = 27
    Width = 100
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '!99/99/0000;1;_'
    MaxLength = 10
    TabOrder = 7
    Text = '  /  /    '
    OnKeyDown = ProximoCampo
  end
  inline FrFoto: TFrImagem
    Left = 126
    Top = 49
    Width = 128
    Height = 128
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 8
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 49
  end
  inline FrCargoFuncionario: TFrCargosFuncionarios
    Left = 259
    Top = 137
    Width = 245
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 12
    TabStop = True
    ExplicitLeft = 259
    ExplicitTop = 137
    ExplicitWidth = 245
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 220
      Height = 24
      ExplicitWidth = 220
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 245
      ExplicitWidth = 245
      inherited lbNomePesquisa: TLabel
        Width = 30
        Caption = 'Cargo'
        ExplicitWidth = 30
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 140
        ExplicitLeft = 140
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 220
      Height = 25
      ExplicitLeft = 220
      ExplicitHeight = 25
    end
  end
end
