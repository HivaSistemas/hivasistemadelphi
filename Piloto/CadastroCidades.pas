unit CadastroCidades;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Biblioteca, _Sessao,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _FrameHerancaPrincipal, _RecordsEspeciais, _Cidades,
  _FrameHenrancaPesquisas, FrameEstados, _RecordsCadastros, PesquisaCidades,
  ComboBoxLuka, CheckBoxLuka;

type
  TFormCadastroCidades = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eNome: TEditLuka;
    FrEstado: TFrEstados;
    lb2: TLabel;
    eCodigoMunicipioIBGE: TEditLuka;
    lbl1: TLabel;
    cbTipoBloqueioVendaEntregar: TComboBoxLuka;
    Label2: TLabel;
    eValorCalculoFreteKM: TEditLuka;
  private
    procedure PreencherRegistro(pCidade: RecCidades);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroCidades }

procedure TFormCadastroCidades.BuscarRegistro;
var
  vCidade: TArray<RecCidades>;
begin
  vCidade := _Cidades.BuscarCidades(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if vCidade = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    SetarFoco(eID);
    Exit;
  end;

  inherited;
  PreencherRegistro(vCidade[0]);
end;

procedure TFormCadastroCidades.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _Cidades.ExcluirCidade(Sessao.getConexaoBanco, eId.AsInt);
  Sessao.AbortarSeHouveErro(vRetBanco);

  inherited;
end;

procedure TFormCadastroCidades.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vRetBanco :=
    _Cidades.AtualizarCidade(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eNome.Text,
      eCodigoMunicipioIBGE.AsInt,
      FrEstado.getEstado.estado_id,
      _Biblioteca.ToChar(ckAtivo),
      cbTipoBloqueioVendaEntregar.GetValor,
      eValorCalculoFreteKM.AsInt
    );

  Sessao.AbortarSeHouveErro(vRetBanco);
  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormCadastroCidades.Modo(pEditando: Boolean);
begin
  inherited;

  _Biblioteca.Habilitar([
    FrEstado,
    eNome,
    eCodigoMunicipioIBGE,
    cbTipoBloqueioVendaEntregar,
    eValorCalculoFreteKM,
    ckAtivo],
    pEditando
  );

  if pEditando then begin
    SetarFoco(eNome);
    ckAtivo.Checked := True;
  end;
end;

procedure TFormCadastroCidades.PesquisarRegistro;
var
  vCidade: RecCidades;
begin
  vCidade := RecCidades(PesquisaCidades.PesquisarCidade(False));
  if vCidade = nil then
    Exit;

  inherited;
  PreencherRegistro(vCidade);
end;

procedure TFormCadastroCidades.PreencherRegistro(pCidade: RecCidades);
begin
  eID.AsInt := pCidade.cidade_id;
  eNome.Text := pCidade.nome;
  eCodigoMunicipioIBGE.AsInt := pCidade.codigo_ibge;
  FrEstado.InserirDadoPorChave(pCidade.estado_id, False);
  cbTipoBloqueioVendaEntregar.SetIndicePorValor( pCidade.TipoBloqueioVendaEntregar );

  eValorCalculoFreteKM.AsInt := pCidade.valor_calculo_frete_km;

  ckAtivo.Checked := (pCidade.ativo = 'S');
end;

procedure TFormCadastroCidades.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if eNome.Trim = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar o nome da cidade!');
    SetarFoco(eNome);
    Abort;
  end;

  if FrEstado.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio informar o estado da cidade!');
    SetarFoco(FrEstado);
    Abort;
  end;

  if eCodigoMunicipioIBGE.AsInt = 0 then begin
    if not _Biblioteca.Perguntar('Para emiss�o de nota fiscal eletr�nica � necess�rio o c�digo IBGE, deseja continuar sem inform�-lo?') then begin
      SetarFoco(eCodigoMunicipioIBGE);
      Abort;
    end;
  end;

  if cbTipoBloqueioVendaEntregar.ItemIndex = -1 then begin
    _Biblioteca.Exclamar('� necess�rio informar se haver� bloqueio na venda a entregar para esta cidade!');
    SetarFoco(cbTipoBloqueioVendaEntregar);
    Abort;
  end;

end;

end.
