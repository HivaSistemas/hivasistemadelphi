unit InformacoesCreditosUtilizados;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Grids, GridLuka, _ContasPagar,
  Vcl.Buttons, Vcl.ExtCtrls, _ContasReceber, _Sessao, _Biblioteca, _RecordsFinanceiros;

type
  TFormInformacoesCreditosUtilizados = class(TFormHerancaFinalizar)
    sgCreditos: TGridLuka;
    procedure sgCreditosDblClick(Sender: TObject);
    procedure sgCreditosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  private
    FNatureza: string;
  public
    { Public declarations }
  end;

procedure Informar(pBaixaId: Integer; pNatureza: string);

implementation

{$R *.dfm}

uses
  Informacoes.TituloReceber, Informacoes.TituloPagar;

const
  coCodigo       = 0;
  coTipo         = 1;
  coDataCadastro = 2;
  coDocumento    = 3;
  coValor        = 4;

  (* Ocultas *)
  coNatureza     = 5;

procedure Informar(pBaixaId: Integer; pNatureza: string);
var
  i: Integer;

  vPagar: TArray<RecContaPagar>;
  vReceber: TArray<RecContasReceber>;

  vForm: TFormInformacoesCreditosUtilizados;
begin
  if pBaixaId = 0 then
    Exit;

  if pNatureza = 'P' then begin
    vReceber := _ContasReceber.BuscarContasReceber(Sessao.getConexaoBanco, 7, [pBaixaId, Sessao.getParametros.TipoCobrancaGeracaoCredId, Sessao.getParametros.TipoCobrancaGerCredImpId]);
    if vReceber = nil then begin
      _Biblioteca.NenhumRegistro;
      Exit;
    end;
  end
  else begin
    vPagar := _ContasPagar.BuscarContasPagar(Sessao.getConexaoBanco, 5, [pBaixaId, Sessao.getParametros.TipoCobrancaGeracaoCredId, Sessao.getParametros.TipoCobrancaGerCredImpId]);
    if vPagar = nil then begin
      _Biblioteca.NenhumRegistro;
      Exit;
    end;
  end;

  vForm := TFormInformacoesCreditosUtilizados.Create(nil);
  vForm.FNatureza := pNatureza;

  for i := Low(vPagar) to High(vPagar) do begin
    vForm.sgCreditos.Cells[coCodigo, i + 1]       := NFormat(vPagar[i].PagarId);
    vForm.sgCreditos.Cells[coTipo, i + 1]         := 'A pagar';
    vForm.sgCreditos.Cells[coDataCadastro, i + 1] := DFormat(vPagar[i].data_cadastro);
    vForm.sgCreditos.Cells[coDocumento, i + 1]    := vPagar[i].documento;
    vForm.sgCreditos.Cells[coValor, i + 1]        := NFormat(vPagar[i].ValorDocumento);
    vForm.sgCreditos.Cells[coNatureza, i + 1]     := pNatureza;
  end;

  for i := Low(vReceber) to High(vReceber) do begin
    vForm.sgCreditos.Cells[coCodigo, i + 1]       := NFormat(vReceber[i].ReceberId);
    vForm.sgCreditos.Cells[coTipo, i + 1]         := 'A receber';
    vForm.sgCreditos.Cells[coDataCadastro, i + 1] := DFormat(vReceber[i].data_cadastro);
    vForm.sgCreditos.Cells[coDocumento, i + 1]    := vReceber[i].documento;
    vForm.sgCreditos.Cells[coValor, i + 1]        := NFormat(vReceber[i].ValorDocumento);
    vForm.sgCreditos.Cells[coNatureza, i + 1]     := pNatureza;
  end;

  vForm.sgCreditos.SetLinhasGridPorTamanhoVetor( IIfInt(pNatureza = 'P', Length(vReceber), Length(vPagar)) );

  vForm.ShowModal;

  vForm.Free;
end;

procedure TFormInformacoesCreditosUtilizados.sgCreditosDblClick(Sender: TObject);
begin
  inherited;
  if FNatureza = 'R' then
    Informacoes.TituloPagar.Informar( SFormatInt( sgCreditos.Cells[coCodigo, sgCreditos.Row] ) )
  else
    Informacoes.TituloReceber.Informar( SFormatInt( sgCreditos.Cells[coCodigo, sgCreditos.Row] ) );
end;

procedure TFormInformacoesCreditosUtilizados.sgCreditosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coTipo] then
    vAlinhamento := taCenter
  else if ACol in[coCodigo, coValor] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgCreditos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
