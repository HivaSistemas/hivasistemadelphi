inherited FormInformacoesGruposTributacoesEstadualCompra: TFormInformacoesGruposTributacoesEstadualCompra
  Caption = 'Informa'#231#245'es do grupo de tributa'#231#245'es estadual de compra'
  ClientHeight = 438
  ClientWidth = 546
  ExplicitWidth = 552
  ExplicitHeight = 467
  PixelsPerInch = 96
  TextHeight = 14
  object Label1: TLabel [0]
    Left = 8
    Top = 8
    Width = 37
    Height = 14
    Caption = 'C'#243'digo'
  end
  object lb1: TLabel [1]
    Left = 94
    Top = 8
    Width = 53
    Height = 14
    Caption = 'Descri'#231#227'o'
  end
  inherited pnOpcoes: TPanel
    Top = 401
    Width = 546
    TabOrder = 4
    ExplicitLeft = -8
    ExplicitTop = 593
    ExplicitWidth = 1242
  end
  inline FrICMS: TFrICMSCompra
    Left = 8
    Top = 47
    Width = 530
    Height = 348
    Align = alCustom
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 8
    ExplicitTop = 47
    ExplicitWidth = 530
    ExplicitHeight = 348
    inherited pnBotoes: TPanel
      Left = 1000
      Align = alNone
      Enabled = False
      Visible = False
      ExplicitLeft = 1000
      inherited eValor: TEditLuka
        Height = 22
        TabStop = False
        Enabled = False
        ExplicitHeight = 22
      end
    end
    inherited pnGrid: TPanel
      Width = 530
      Height = 348
      ExplicitWidth = 537
      ExplicitHeight = 393
      inherited sgICMS: TGridLuka
        Width = 530
        Height = 348
        Align = alCustom
        OnKeyDown = ProximoCampo
        ExplicitWidth = 530
        ExplicitHeight = 348
      end
    end
  end
  object eID: TEditLuka
    Left = 8
    Top = 22
    Width = 80
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 0
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eDescricao: TEditLuka
    Left = 94
    Top = 22
    Width = 392
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 1
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object ckAtivo: TCheckBoxLuka
    Left = 492
    Top = 5
    Width = 46
    Height = 17
    Caption = 'Ativo'
    Enabled = False
    TabOrder = 2
    CheckedStr = 'N'
  end
end
