unit CadastroFuncoesFuncionarios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _RecordsCadastros, _Sessao,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _CargosFuncionario, _RecordsEspeciais, PesquisaFuncoesFuncionario,
  CheckBoxLuka;

type
  TFormCadastroCargosFuncionarios = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eNome: TEditLuka;
  private
    procedure PreencherRegistro(pFuncaoFuncionario: RecCargosFuncionario);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

uses _Biblioteca;

{ TFormCadastroFuncoesFuncionarios }

procedure TFormCadastroCargosFuncionarios.BuscarRegistro;
var
  funcoes_funcionario: TArray<RecCargosFuncionario>;
begin
  funcoes_funcionario := _CargosFuncionario.BuscarCargosFuncionarios(Sessao.getConexaoBanco, 0, [eId.AsInt]);

  if funcoes_funcionario = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    eID.SetFocus;
    Exit;
  end;

  inherited;
  PreencherRegistro(funcoes_funcionario[0]);
end;

procedure TFormCadastroCargosFuncionarios.ExcluirRegistro;
var
  retorno: RecRetornoBD;
begin
  retorno := _CargosFuncionario.ExcluirCargosFuncionario(Sessao.getConexaoBanco, eId.AsInt);

  if retorno.TeveErro then begin
    _Biblioteca.Exclamar(retorno.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroCargosFuncionarios.GravarRegistro(Sender: TObject);
var
  retorno: RecRetornoBD;
begin
  inherited;
  retorno :=
    _CargosFuncionario.AtualizarCargosFuncionario(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eNome.Text,
      _Biblioteca.ToChar(ckAtivo)
    );

  if retorno.TeveErro then begin
    _Biblioteca.Exclamar(retorno.MensagemErro);
    Abort;
  end;

  if retorno.AsInt > 0 then
    _Biblioteca.Informar(coNovoRegistroSucessoCodigo + IntToStr(retorno.AsInt))
  else
    _Biblioteca.Informar(coAlteracaoRegistroSucesso);
end;

procedure TFormCadastroCargosFuncionarios.Modo(pEditando: Boolean);
begin
  inherited;

  _Biblioteca.Habilitar(
    [
      eNome,
      ckAtivo
    ],
    pEditando
  );

  if pEditando then
    eNome.SetFocus;
end;

procedure TFormCadastroCargosFuncionarios.PesquisarRegistro;
var
  funcoes: RecCargosFuncionario;
begin
  funcoes := RecCargosFuncionario(PesquisaFuncoesFuncionario.PesquisarFuncaoFuncionario);

  if funcoes = nil then
    Exit;

  inherited;
  PreencherRegistro(funcoes);
end;

procedure TFormCadastroCargosFuncionarios.PreencherRegistro(pFuncaoFuncionario: RecCargosFuncionario);
begin
  eID.AsInt := pFuncaoFuncionario.CargoId;
  eNome.Text := pFuncaoFuncionario.nome;
  ckAtivo.Checked := (pFuncaoFuncionario.ativo = 'S');
end;

procedure TFormCadastroCargosFuncionarios.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eNome.Text = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar o nome da cidade!');
    eNome.SetFocus;
    Abort;
  end;
end;

end.
