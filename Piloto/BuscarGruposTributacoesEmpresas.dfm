inherited FormBuscarGruposTributacoesEmpresas: TFormBuscarGruposTributacoesEmpresas
  Caption = 'Busca de grupos de tributa'#231#245'es de empresas'
  ClientHeight = 142
  ClientWidth = 658
  ExplicitWidth = 664
  ExplicitHeight = 171
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 105
    Width = 658
    ExplicitTop = 105
    ExplicitWidth = 658
  end
  inline FrGrupoTribEstadualCompra: TFrGruposTributacoesEstadualCompra
    Left = 4
    Top = 5
    Width = 320
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 4
    ExplicitTop = 5
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Height = 24
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      inherited lbNomePesquisa: TLabel
        Width = 187
        Caption = 'Grupo de trib. estadual de compra'
        ExplicitWidth = 187
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Height = 25
      ExplicitHeight = 25
    end
  end
  inline FrGrupoTributacoesEstadualVenda: TFrGruposTributacoesEstadualVenda
    Left = 331
    Top = 5
    Width = 320
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 331
    ExplicitTop = 5
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Height = 24
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      inherited lbNomePesquisa: TLabel
        Width = 186
        ExplicitWidth = 186
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Height = 25
      ExplicitHeight = 25
    end
  end
  inline FrGrupoTributacoesFederalCompra: TFrGruposTributacoesFederalCompra
    Left = 4
    Top = 56
    Width = 320
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 4
    ExplicitTop = 56
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Height = 24
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      inherited lbNomePesquisa: TLabel
        Width = 184
        ExplicitWidth = 184
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Height = 25
      ExplicitHeight = 25
    end
  end
  inline FrGrupoTributacoesFederalVenda: TFrGruposTributacoesFederalVenda
    Left = 331
    Top = 56
    Width = 320
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 4
    TabStop = True
    ExplicitLeft = 331
    ExplicitTop = 56
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Height = 24
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      inherited lbNomePesquisa: TLabel
        Width = 177
        ExplicitWidth = 177
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Height = 25
      ExplicitHeight = 25
    end
  end
end
