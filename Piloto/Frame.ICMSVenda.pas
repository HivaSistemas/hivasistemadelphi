unit Frame.ICMSVenda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.StdCtrls, _Biblioteca,
  ComboBoxLuka, _FrameHenrancaPesquisas, FrameEstados, EditLuka, FrameCFOPs;

type
  TFrameICMSVenda = class(TFrameHerancaPrincipal)
    gbICMS: TGroupBox;
    lb1: TLabel;
    cbCST: TComboBoxLuka;
    procedure cbCSTChange(Sender: TObject);
  private
    { Private declarations }
  public
    procedure VerificarRegistro;
    procedure SetFocus; override;

    procedure Clear; override;
    procedure SomenteLeitura(pValue: Boolean); override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
  end;

implementation

{$R *.dfm}

procedure TFrameICMSVenda.cbCSTChange(Sender: TObject);
begin
  inherited;
  Clear;
end;

procedure TFrameICMSVenda.Clear;
begin
  inherited;

end;

procedure TFrameICMSVenda.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([cbCST], pEditando, pLimpar);
end;

procedure TFrameICMSVenda.SetFocus;
begin
  inherited;
  SetarFoco(cbCST);
end;

procedure TFrameICMSVenda.SomenteLeitura(pValue: Boolean);
begin
  inherited;
  _Biblioteca.SomenteLeitura([cbCST], pValue);
end;

procedure TFrameICMSVenda.VerificarRegistro;
begin
  if cbCST.ItemIndex = -1 then begin
    Exclamar('O CST n�o foi informado corretamente!');
    SetarFoco(cbCST);
    Abort;
  end;
end;

end.
