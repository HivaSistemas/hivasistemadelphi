unit CadastroRotas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Sessao, _Rotas,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _Biblioteca, _RecordsEspeciais, PesquisaRotas,
  CheckBoxLuka;

type
  TFormCadastroRotas = class(TFormHerancaCadastroCodigo)
    eNome: TEditLuka;
    lb1: TLabel;
  private
    procedure PreencherRegistro(pRota: RecRotas);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroRotas }

procedure TFormCadastroRotas.BuscarRegistro;
var
  vRotas: TArray<RecRotas>;
begin
  vRotas := _Rotas.BuscarRotas(Sessao.getConexaoBanco, 0, [eId.AsInt], False);
  if vRotas = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    SetarFoco(eID);
    Exit;
  end;

  inherited;
  PreencherRegistro(vRotas[0]);
end;

procedure TFormCadastroRotas.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _Rotas.ExcluirRota(Sessao.getConexaoBanco, eId.AsInt);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroRotas.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vRetBanco :=
    _Rotas.AtualizarRota(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eNome.Text,
      _Biblioteca.ToChar(ckAtivo)
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormCadastroRotas.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    eNome,
    ckAtivo],
    pEditando
  );

  if pEditando then
    SetarFoco(eNome);
end;

procedure TFormCadastroRotas.PesquisarRegistro;
var
  vRota: RecRotas;
begin
  vRota := RecRotas(PesquisaRotas.PesquisarRota(False));
  if vRota = nil then
    Exit;

  inherited;
  PreencherRegistro(vRota);
end;

procedure TFormCadastroRotas.PreencherRegistro(pRota: RecRotas);
begin
  eID.AsInt       := pRota.RotaId;
  eNome.Text      := pRota.nome;
  ckAtivo.Checked := (pRota.ativo = 'S');

  pRota.Free;
end;

procedure TFormCadastroRotas.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eNome.Trim = '' then begin
    _Biblioteca.Exclamar('O nome da rota n�o foi informada corretamente, verifique!');
    SetarFoco(eNome);
    Abort;
  end;
end;

end.
