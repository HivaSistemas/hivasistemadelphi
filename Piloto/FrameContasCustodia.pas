unit FrameContasCustodia;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Menus, _Biblioteca, _Sessao,
  Vcl.Buttons, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _ContasCustodia, PesquisaContasCustodia;

type
  TFrContasCustodia = class(TFrameHenrancaPesquisas)
  public
    function getDados(pLinha: Integer = -1): RecContasCustodia;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrContasCustodia }

function TFrContasCustodia.AdicionarDireto: TObject;
var
  vDados: TArray<RecContasCustodia>;
begin
  vDados := _ContasCustodia.BuscarContasCustodia(Sessao.getConexaoBanco, 0, [FChaveDigitada], ckSomenteAtivos.Checked);
  if vDados = nil then
    Result := nil
  else
    Result := vDados[0];
end;

function TFrContasCustodia.AdicionarPesquisando: TObject;
begin
  Result := PesquisaContasCustodia.Pesquisar(ckSomenteAtivos.Checked);
end;

function TFrContasCustodia.getDados(pLinha: Integer): RecContasCustodia;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecContasCustodia(FDados[IIfInt(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrContasCustodia.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecContasCustodia(FDados[i]).ContaCustodiaId = RecContasCustodia(pSender).ContaCustodiaId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrContasCustodia.MontarGrid;
var
  i: Integer;
  vSender: RecContasCustodia;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecContasCustodia(FDados[i]);
      AAdd([IntToStr(vSender.ContaCustodiaId), vSender.nome]);
    end;
  end;
end;

end.
