inherited FormPesquisaClientesGrupos: TFormPesquisaClientesGrupos
  Caption = 'Pesquisa grupos de clientes'
  ClientHeight = 205
  ClientWidth = 371
  ExplicitWidth = 379
  ExplicitHeight = 236
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 371
    Height = 159
    ColCount = 4
    OnDrawCell = sgPesquisaDrawCell
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Nome'
      'Ativo?')
    RealColCount = 4
    ColWidths = (
      28
      55
      199
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Left = 8
    Top = 168
    Text = '0'
    ExplicitLeft = 8
    ExplicitTop = 168
  end
  inherited pnFiltro1: TPanel
    Width = 371
    inherited eValorPesquisa: TEditLuka
      Width = 167
    end
  end
end
