unit PesquisaIndicesDescontosVenda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _IndicesDescontosVenda,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _Sessao, _Biblioteca,
  Vcl.ExtCtrls;

type
  TFormPesquisaIndicesDescontosVenda = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  protected
    procedure BuscarRegistros; override;
  end;

function Pesquisar: TObject;

implementation

{$R *.dfm}

const
  coDescricao          = 2;
  coPercDesconto       = 3;
  coPrecoCusto         = 4;
  coTipoDescPrecoCusto = 5;
  coAtivo              = 6;

function Pesquisar: TObject;
var
  vDado: TObject;
begin
  vDado := _HerancaPesquisas.Pesquisar(TFormPesquisaIndicesDescontosVenda, _IndicesDescontosVenda.GetFiltros);
  if vDado = nil then
    Result := nil
  else
    Result := RecIndicesDescontosVenda(vDado);
end;

{ TFormPesquisaIndicesDescontosVenda }

procedure TFormPesquisaIndicesDescontosVenda.BuscarRegistros;
var
  i: Integer;
  vDados: TArray<RecIndicesDescontosVenda>;
begin
  inherited;

  vDados :=
    _IndicesDescontosVenda.BuscarIndicesDescontosVenda(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text]
    );

  if vDados = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vDados);

  for i := Low(vDados) to High(vDados) do begin
    sgPesquisa.Cells[coSelecionado, i + 1]        := charNaoSelecionado;
    sgPesquisa.Cells[coCodigo, i + 1]             := NFormat(vDados[i].IndiceId);
    sgPesquisa.Cells[coDescricao, i + 1]          := vDados[i].Descricao;
    sgPesquisa.Cells[coPercDesconto, i + 1]       := NFormat(vDados[i].PercentualDesconto);
    sgPesquisa.Cells[coPrecoCusto, i + 1]         := _Biblioteca.SimNao( vDados[i].PrecoCusto );
    sgPesquisa.Cells[coTipoDescPrecoCusto, i + 1] := vDados[i].TipoDescontoPrecoCustoAnalitico;
    sgPesquisa.Cells[coAtivo, i + 1]              := _Biblioteca.SimNao( vDados[i].Ativo );
  end;
  sgPesquisa.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vDados) );
  SetarFoco(sgPesquisa);
end;

procedure TFormPesquisaIndicesDescontosVenda.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coSelecionado, coAtivo, coTipoDescPrecoCusto, coPrecoCusto] then
    vAlinhamento := taCenter
  else if ACol in[coCodigo, coPercDesconto]  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormPesquisaIndicesDescontosVenda.sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol in[coAtivo, coPrecoCusto] then begin
    AFont.Style := [fsBold];
    AFont.Color := _Biblioteca.AzulVermelho(sgPesquisa.Cells[ACol, ARow]);
  end;
end;

end.
