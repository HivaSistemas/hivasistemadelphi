inherited FormPesquisaCadastros: TFormPesquisaCadastros
  Caption = 'Pesquisa de cadastros'
  ClientHeight = 471
  ClientWidth = 794
  ExplicitWidth = 802
  ExplicitHeight = 502
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 794
    Height = 425
    ColCount = 16
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goThumbTracking]
    ScrollBars = ssBoth
    OnDrawCell = sgPesquisaDrawCell
    CorLinhaFoco = 38619
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Raz'#227'o Social'
      'Nome Fanstasia'
      'CPF/CNPJ'
      'Logradouro'
      'Complemento'
      'Bairro'
      'Cidade/UF'
      'E-mail'
      'Cliente?'
      'Forne.?'
      'Motor.?'
      'Transp.?'
      'Profis.?'
      'Ativo?')
    RealColCount = 16
    Indicador = True
    ExplicitWidth = 794
    ExplicitHeight = 425
    ColWidths = (
      28
      55
      187
      184
      113
      143
      120
      194
      195
      52
      41
      64
      64
      64
      64
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Left = 8
    Top = 416
    Text = '0'
    ExplicitLeft = 8
    ExplicitTop = 416
  end
  inherited pnFiltro1: TPanel
    Width = 794
    ExplicitWidth = 794
    inherited lblPesquisa: TLabel
      Left = 201
      Width = 48
      Caption = 'Cadastro'
      ExplicitLeft = 201
      ExplicitWidth = 48
    end
    inherited lblOpcoesPesquisa: TLabel
      Left = 1
      ExplicitLeft = 1
    end
    inherited eValorPesquisa: TEditLuka
      Left = 201
      Width = 588
      ExplicitLeft = 201
      ExplicitWidth = 588
    end
    inherited cbOpcoesPesquisa: TComboBoxLuka
      Left = 1
      ExplicitLeft = 1
    end
  end
end
