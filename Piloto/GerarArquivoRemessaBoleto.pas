unit GerarArquivoRemessaBoleto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, _RecordsCadastros,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _Sessao, _Biblioteca, _RemessasCobrancas,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, _FrameHerancaPrincipal, _PortadoresContas, _RecordsEspeciais,
  _FrameHenrancaPesquisas, FramePortadores, FrameDataInicialFinal, _ContasReceber,
  Vcl.Grids, GridLuka, Vcl.Menus, _CobrancaBancaria, _CobrancaBradesco, _Contas,
  SpeedButtonLuka, Vcl.StdCtrls, EditLuka, _Empresas, ComboBoxLuka, CheckBoxLuka;

type
  TFormGerarArquivoRemessaBoleto = class(TFormHerancaRelatoriosPageControl)
    FrPortador: TFrPortadores;
    FrDataEmissao: TFrDataInicialFinal;
    FrDataVencimento: TFrDataInicialFinal;
    sgBoletos: TGridLuka;
    lb2: TLabel;
    cbSituacaoRemessa: TComboBoxLuka;
    SpeedButton1: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure sgBoletosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgBoletosGetCellPicture(Sender: TObject; ARow, ACol: Integer;
      var APicture: TPicture);
    procedure sgBoletosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton1Click(Sender: TObject);
  private
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

uses _EmitirBoletos, _RecordsRelatorios, _RelacaoContasReceber, _Imagens;

const
  tiSelecionado        = 0;
  coReceberId          = 1;
  coDocumento          = 2;
  coDataEmissao        = 3;
  coDataVencimento     = 4;
  coValorDocumento     = 5;
  coNomeCliente        = 6;
  coNossoNumero        = 7;

{ TFormGerarArquivoRemessaBoleto }

procedure TFormGerarArquivoRemessaBoleto.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  FTitulos: TArray<_RecordsRelatorios.RecContasReceber>;
begin
  inherited;
  vSql := '';

  WhereOuAnd(vSql,' CON.EMPRESA_ID = ' + IntToStr( Sessao.getParametrosEmpresa.EmpresaId ));

  if not FrPortador.EstaVazio then
    WhereOuAnd(vSql, FrPortador.getSqlFiltros('CON.PORTADOR_ID'));

  if not FrDataEmissao.EstaVazio then
    WhereOuAnd(vSql, FrDataEmissao.getSqlFiltros('CON.DATA_EMISSAO'));

  if not FrDataVencimento.EstaVazio then
    WhereOuAnd(vSql, FrDataVencimento.getSqlFiltros('CON.DATA_VENCIMENTO'));

  if cbSituacaoRemessa.GetValor <> 'NaoFiltrar' then
  begin
    if cbSituacaoRemessa.GetValor = 'NaoGerado' then
      WhereOuAnd(vSql,' CON.REMESSA_ID is null ')
    else
      WhereOuAnd(vSql,' CON.REMESSA_ID is not null ');
  end;

  WhereOuAnd(vSql,' CON.STATUS = ''A'' ');

  FTitulos := _RelacaoContasReceber.BuscarContasReceberComando(
    Sessao.getConexaoBanco,
    vSql
  );

  if FTitulos = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  for i := Low(FTitulos) to High(FTitulos) do
  begin
    sgBoletos.Cells[tiSelecionado, i + 1]     := charNaoSelecionado;
    sgBoletos.Cells[coReceberId, i + 1]       := NFormat(FTitulos[i].receber_id);
    sgBoletos.Cells[coDocumento, i + 1]       := FTitulos[i].Documento;
    sgBoletos.Cells[coDataEmissao, i + 1]     := DFormat(FTitulos[i].data_emissao);
    sgBoletos.Cells[coDataVencimento, i + 1]  := DFormat(FTitulos[i].data_vencimento);
    sgBoletos.Cells[coValorDocumento, i + 1]  := NFormat(FTitulos[i].valor_documento);
    sgBoletos.Cells[coNomeCliente, i + 1]     := FTitulos[i].nome_cliente;
    sgBoletos.Cells[coNossoNumero, i + 1]     := FTitulos[i].nosso_numero;
  end;
  sgBoletos.SetLinhasGridPorTamanhoVetor( Length(FTitulos) );

  SetarFoco(sgBoletos);
end;

procedure TFormGerarArquivoRemessaBoleto.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrPortador);
end;

procedure TFormGerarArquivoRemessaBoleto.Imprimir(Sender: TObject);
begin
  inherited;

end;

procedure TFormGerarArquivoRemessaBoleto.sgBoletosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coReceberId,  coValorDocumento] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgBoletos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormGerarArquivoRemessaBoleto.sgBoletosGetCellPicture(
  Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgBoletos.Cells[ACol, ARow] = '' then
   Exit;

  if ACol = tiSelecionado then begin
    if sgBoletos.Cells[ACol, ARow] = charNaoSelecionado then
      APicture := _Imagens.FormImagens.im1.Picture
    else if sgBoletos.Cells[ACol, ARow] = charSelecionado then
      APicture := _Imagens.FormImagens.im2.Picture;
  end;
end;

procedure TFormGerarArquivoRemessaBoleto.sgBoletosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  MarcarDesmarcarTodos(sgBoletos, tiSelecionado, Key, Shift);
end;

procedure TFormGerarArquivoRemessaBoleto.SpeedButton1Click(Sender: TObject);
var
  vBoletos: TBoletosACBr;
  vParametrosImpressao: TParametrosImpressao;
  vLinha: Integer;
begin
  inherited;
  if sgBoletos.Localizar([tiSelecionado],[charSelecionado]) = -1 then begin
    Exclamar('� necess�rio selecionar pelo menos um titulo!');
    Abort;
  end;

  vBoletos := TBoletosACBr.Create;
  vParametrosImpressao:= TParametrosImpressao.Create;
  try
    for vLinha := 1 to sgBoletos.RowCount -1 do
    begin
      if sgBoletos.Cells[tiSelecionado,vLinha] = charSelecionado then
        vParametrosImpressao.AdicionarContasReceber(RetornaNumeros(sgBoletos.Cells[coReceberId, vLinha]).ToInteger);
    end;

    vBoletos.GerarArquivoRemessa(vParametrosImpressao);

  finally
    vParametrosImpressao.Free;
    vBoletos.Free;
  end;
  Informar('Remessa de cobran�a gerada com sucesso!');

  pcDados.ActivePage := tsFiltros;
  sgBoletos.ClearGrid();
end;

procedure TFormGerarArquivoRemessaBoleto.VerificarRegistro(Sender: TObject);
begin
  inherited;
  sgBoletos.ClearGrid();

  if FrPortador.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio informar o portador para continuar!');
    SetarFoco(FrPortador);
    Abort;
  end;

end;

end.
