inherited FormAcompanhamentoContagem: TFormAcompanhamentoContagem
  Caption = 'Acompanhamento de Contagem'
  ClientHeight = 479
  ClientWidth = 857
  ExplicitWidth = 863
  ExplicitHeight = 508
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 479
    ExplicitHeight = 479
  end
  inherited pcDados: TPageControl
    Width = 735
    Height = 479
    ExplicitWidth = 709
    ExplicitHeight = 479
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 701
      ExplicitHeight = 450
      inline FrInventario: TFrInventario
        Left = 4
        Top = 7
        Width = 637
        Height = 41
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 4
        ExplicitTop = 7
        ExplicitWidth = 637
        ExplicitHeight = 41
        inherited sgPesquisa: TGridLuka
          Width = 612
          Height = 24
          ExplicitWidth = 612
          ExplicitHeight = 24
        end
        inherited PnTitulos: TPanel
          Width = 637
          ExplicitWidth = 637
          inherited lbNomePesquisa: TLabel
            Width = 101
            ExplicitWidth = 101
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 532
            ExplicitLeft = 532
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 612
          Height = 25
          ExplicitLeft = 612
          ExplicitHeight = 25
        end
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 701
      ExplicitHeight = 450
      object st2: TStaticText
        Left = 0
        Top = 0
        Width = 727
        Height = 16
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Contagem'
        Color = clSilver
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        Transparent = False
        ExplicitWidth = 701
      end
      object st1: TStaticText
        Left = 0
        Top = 153
        Width = 727
        Height = 16
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Itens com diverg'#234'ncia'
        Color = clSilver
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        Transparent = False
        ExplicitWidth = 701
      end
      object sgItens: TGridLuka
        Left = 0
        Top = 169
        Width = 727
        Height = 281
        Align = alClient
        ColCount = 8
        Ctl3D = True
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        GradientEndColor = 15395562
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goThumbTracking]
        ParentCtl3D = False
        TabOrder = 2
        OnDrawCell = sgItensDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Produto'
          'Nome'
          'Und.'
          'Marca'
          'Local'
          'Estoque'
          'Quantidade'
          'Diferen'#231'a')
        Grid3D = False
        RealColCount = 8
        Indicador = True
        AtivarPopUpSelecao = False
        ExplicitWidth = 701
        ColWidths = (
          48
          258
          38
          122
          36
          52
          76
          64)
      end
      object sgCapa: TGridLuka
        Left = 0
        Top = 16
        Width = 727
        Height = 137
        Align = alTop
        Ctl3D = True
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        GradientEndColor = 15395562
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goThumbTracking]
        ParentCtl3D = False
        TabOrder = 3
        OnClick = sgCapaClick
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'C'#243'digo'
          'Nome'
          'Data Inicio'
          'Data Fim'
          'Nro Contagem')
        Grid3D = False
        RealColCount = 7
        Indicador = True
        AtivarPopUpSelecao = False
        ExplicitWidth = 737
        ColWidths = (
          52
          309
          120
          127
          90)
      end
    end
  end
end
