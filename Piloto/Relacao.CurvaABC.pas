unit Relacao.CurvaABC;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _Biblioteca, _Sessao,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, FrameDataInicialFinal, FrameEmpresas, System.StrUtils,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameClientes, Vcl.Grids, System.Math,
  GridLuka, _RecordsRelatorios, _Relacao.CurvaABC, Vcl.StdCtrls, CheckBoxLuka,
  EditLuka, Generics.Collections, Types, FrameMarcas, FrameProdutos, Data.DB,
  Datasnap.DBClient, frxClass, frxDBSet;

type
  TFormRelacaoCurvaABC = class(TFormHerancaRelatoriosPageControl)
    FrClientes: TFrClientes;
    FrEmpresas: TFrEmpresas;
    FrDataCadastro: TFrDataInicialFinal;
    pcResultado: TPageControl;
    tsPorProdutosValorTotal: TTabSheet;
    tsSinteticoTipoCobranca: TTabSheet;
    sgProdutosValor: TGridLuka;
    tsPorProdutosQuantidade: TTabSheet;
    sgProdutosQuantidade: TGridLuka;
    ts1: TTabSheet;
    sgClientesValor: TGridLuka;
    sgClientesQuantidade: TGridLuka;
    FrDataRecebimento: TFrDataInicialFinal;
    percClasseA: TEditLuka;
    Label1: TLabel;
    Label2: TLabel;
    percClasseB: TEditLuka;
    Label3: TLabel;
    percClasseC: TEditLuka;
    Label4: TLabel;
    percClasseD: TEditLuka;
    FrMarca: TFrMarcas;
    FrProdutos: TFrProdutos;
    frxReport: TfrxReport;
    dstValorProdutos: TfrxDBDataset;
    cdsValorProdutos: TClientDataSet;
    cdsValorRanking: TStringField;
    CdsValorProduto: TStringField;
    cdsValorUnidade: TStringField;
    cdsValorPrecoUnitario: TStringField;
    cdsValorQuantidade: TStringField;
    cdsValorCustoTotal: TStringField;
    cdsValorClassificacao: TStringField;
    cdsValorTotal: TStringField;
    cdsValorPercentual: TStringField;
    frxReport1: TfrxReport;
    frxDBDataset1: TfrxDBDataset;
    ClientDataSet1: TClientDataSet;
    cdsQuantidadeRanking: TStringField;
    cdsQuantidadeCliente: TStringField;
    cdsQuantidadeQuantidade: TStringField;
    cdsQuantidadeUnidade: TStringField;
    cdsQuantidadePercentual: TStringField;
    cdsQuantidadeClassificacao: TStringField;
    cdsValorProdutoNegrito: TStringField;
    cdsValorQuantidadeNegrito: TStringField;
    cdsValorCustoTotalNegrito: TStringField;
    cdsValorTotalNegrito: TStringField;
    cdsQuantidadeClienteNegrito: TStringField;
    cdsQuantidadeQuantidadeNegrito: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure sgProdutosValorDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgProdutosQuantidadeDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgClientesValorDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgClientesQuantidadeDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgProdutosValorGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgProdutosQuantidadeGetCellColor(Sender: TObject; ARow,
      ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgClientesValorGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgClientesQuantidadeGetCellColor(Sender: TObject; ARow,
      ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

const
  (* Grid de Produtos(Valor total) *)
  cpvRanking       = 0;
  cpvProdutoId     = 1;
  cpvNome          = 2;
  cpvPrecoUnit     = 3;
  cpvQuantidade    = 4;
  cpvUnidade       = 5;
  cpvCustoTotal    = 6;
  cpvValorTotal    = 7;
  cpvPercentual    = 8;
  cpvClassificacao = 9;
  cpvTipoLinha     = 10;

  (* Grid de Produtos(Quantidade) *)
  cpqRanking       = 0;
  cpqProdutoId     = 1;
  cpqNome          = 2;
  cpqUnidade       = 3;
  cpqQuantidade    = 4;
  cpqPercentual    = 5;
  cpqClassificacao = 6;
  cpqTipoLinha     = 7;

  (* Grid de Clientes(Valor total) *)
  ccvRanking       = 0;
  ccvClienteId     = 1;
  ccvNome          = 2;
  ccvQuantidade    = 3;
  ccvValorTotal    = 4;
  ccvPercentual    = 5;
  ccvClassificacao = 6;
  ccvTipoLinha     = 7;

  (* Grid de Clientes(Quantidade) *)
  ccqRanking       = 0;
  ccqClienteId     = 1;
  ccqNome          = 2;
  ccqQuantidade    = 3;
  ccqPercentual    = 4;
  ccqClassificacao = 5;
  ccqTipoLinha     = 6;

  // agrupados
  coLinhaDetalhe   = 'D';
  coLinhaCabAgrup  = 'CAGRU';
  coLinhaTotVenda  = 'TV';
  coTotalAgrupador = 'TAGRU';  
procedure TFormRelacaoCurvaABC.Carregar(Sender: TObject);
var
  i: Integer;
  ranking: Integer;
  vLinha: Integer;
  vComando: string;
  vComandoPodutoId: string;
  vCurvaProdutos: TArray<RecProdutoCurvaABC>;
  vCurvaClientes: TArray<RecClienteCurvaABC>;
  percentuaisOrdenados: TIntegerDynArray;
  classesOrdenados: TArray<string>;
  agrupando: Boolean;

  vTotaisAgrupador: record
    totalQuantidade: Double;
    totalValor: Double;
    totalCusto: Double;
  end;     

  procedure TotalizarGrupo(classificacao: string);
  var
    vAgrupador: string;
  begin
    Inc(vLinha);

    sgProdutosValor.Cells[cpvNome, vLinha]       := 'TOTAL DA CLASSE ' + classificacao;
    sgProdutosValor.Cells[cpvQuantidade, vLinha] := NFormatN(vTotaisAgrupador.totalQuantidade);
    sgProdutosValor.Cells[cpvCustoTotal, vLinha] := NFormatN(vTotaisAgrupador.totalCusto);
    sgProdutosValor.Cells[cpvValorTotal, vLinha] := NFormatN(vTotaisAgrupador.totalValor);
    sgProdutosValor.Cells[cpvTipoLinha, vLinha]  := coTotalAgrupador;

    vTotaisAgrupador.totalQuantidade := 0;
    vTotaisAgrupador.totalValor      := 0;
    vTotaisAgrupador.totalCusto      := 0;
  end;

  procedure TotalizarPorQuantidadeProduto(classificacao: string);
  var
    vAgrupador: string;
  begin
    Inc(vLinha);

    sgProdutosQuantidade.Cells[cpqNome, vLinha]       := 'TOTAL DA CLASSE ' + classificacao;
    sgProdutosQuantidade.Cells[cpqQuantidade, vLinha] := NFormatN(vTotaisAgrupador.totalQuantidade);
    sgProdutosQuantidade.Cells[cpqTipoLinha, vLinha]  := coTotalAgrupador;

    vTotaisAgrupador.totalQuantidade := 0;
  end;

  procedure TotalizarPorValorCliente(classificacao: string);
  var
    vAgrupador: string;
  begin
    Inc(vLinha);

    sgClientesValor.Cells[ccvNome, vLinha]       := 'TOTAL DA CLASSE ' + classificacao;
    sgClientesValor.Cells[ccvQuantidade, vLinha] := NFormatN(vTotaisAgrupador.totalQuantidade);
    //sgClientesValor.Cells[ccvCustoTotal, vLinha] := NFormatN(vTotaisAgrupador.totalCusto);
    sgClientesValor.Cells[ccvValorTotal, vLinha] := NFormatN(vTotaisAgrupador.totalValor);
    sgClientesValor.Cells[ccvTipoLinha, vLinha]  := coTotalAgrupador;

    vTotaisAgrupador.totalQuantidade := 0;
    vTotaisAgrupador.totalValor      := 0;
    vTotaisAgrupador.totalCusto      := 0;
  end;

  procedure TotalizarPorQuantidadeCliente(classificacao: string);
  var
    vAgrupador: string;
  begin
    Inc(vLinha);

    sgClientesQuantidade.Cells[ccqNome, vLinha]       := 'TOTAL DA CLASSE ' + classificacao;
    sgClientesQuantidade.Cells[ccqQuantidade, vLinha] := NFormatN(vTotaisAgrupador.totalQuantidade);
    sgClientesQuantidade.Cells[ccqTipoLinha, vLinha]  := coTotalAgrupador;

    vTotaisAgrupador.totalQuantidade := 0;
    vTotaisAgrupador.totalValor      := 0;
    vTotaisAgrupador.totalCusto      := 0;
  end;
begin
  inherited;
  sgProdutosValor.ClearGrid();
  sgProdutosQuantidade.ClearGrid();
  sgClientesValor.ClearGrid();
  sgClientesQuantidade.ClearGrid();

  vTotaisAgrupador.totalQuantidade := 0;
  vTotaisAgrupador.totalValor := 0;
  vTotaisAgrupador.totalCusto := 0;

  if not FrClientes.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, ' CLIENTE_ID ' + FrClientes.getSqlFiltros);

  if not FrDataCadastro.NenhumaDataValida then
    _Biblioteca.WhereOuAnd(vComando, FrDataCadastro.getSqlFiltros('DATA_CADASTRO'));

  if not FrDataCadastro.NenhumaDataValida then
    _Biblioteca.WhereOuAnd(vComando, FrDataCadastro.getSqlFiltros('DATA_CADASTRO'));

  vComando := '';

  if not FrEmpresas.EstaVazio then
    vComando := ' where ORC.EMPRESA_ID ' + FrEmpresas.getSqlFiltros;

  if not FrMarca.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, ' MAR.MARCA_ID ' + FrMarca.getSqlFiltros);

  if not FrProdutos.EstaVazio then
    _Biblioteca.WhereOuAnd(vComandoPodutoId, ' RES.PRODUTO_ID ' + FrProdutos.getSqlFiltros);

  if not FrDataRecebimento.NenhumaDataValida then
    _Biblioteca.WhereOuAnd(vComando, FrDataRecebimento.getSqlFiltros('TRUNC(ORC.DATA_HORA_RECEBIMENTO)'));

  percentuaisOrdenados := TIntegerDynArray.Create(
    percClasseA.AsInt,
    percClasseB.AsInt,
    percClasseC.AsInt,
    percClasseD.AsInt
  );

  TArray.Sort<Integer>(percentuaisOrdenados);
  SetLength(classesOrdenados, 4);

  if percentuaisOrdenados[0] = percClasseA.AsInt then
    classesOrdenados[0] := 'A'
  else if percentuaisOrdenados[0] = percClasseB.AsInt then
    classesOrdenados[0] := 'B'
  else if percentuaisOrdenados[0] = percClasseC.AsInt then
    classesOrdenados[0] := 'C'
  else
    classesOrdenados[0] := 'D';

  if percentuaisOrdenados[1] = percClasseA.AsInt then
    classesOrdenados[1] := 'A'
  else if percentuaisOrdenados[1] = percClasseB.AsInt then
    classesOrdenados[1] := 'B'
  else if percentuaisOrdenados[1] = percClasseC.AsInt then
    classesOrdenados[1] := 'C'
  else
    classesOrdenados[1] := 'D';

  if percentuaisOrdenados[2] = percClasseA.AsInt then
    classesOrdenados[2] := 'A'
  else if percentuaisOrdenados[2] = percClasseB.AsInt then
    classesOrdenados[2] := 'B'
  else if percentuaisOrdenados[2] = percClasseC.AsInt then
    classesOrdenados[2] := 'C'
  else
    classesOrdenados[2] := 'D';

  if percentuaisOrdenados[3] = percClasseA.AsInt then
    classesOrdenados[3] := 'A'
  else if percentuaisOrdenados[3] = percClasseB.AsInt then
    classesOrdenados[3] := 'B'
  else if percentuaisOrdenados[3] = percClasseC.AsInt then
    classesOrdenados[3] := 'C'
  else
    classesOrdenados[3] := 'D';            
    
  vCurvaProdutos := _Relacao.CurvaABC.BuscarCurvaProdutos(
    Sessao.getConexaoBanco,
    vComando,
    vComandoPodutoId,
    percentuaisOrdenados,
    classesOrdenados
  );

  vComando := '';

  if vCurvaProdutos = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vLinha := 0;
  agrupando := false;
  ranking := 0;
  for i := Low(vCurvaProdutos) to High(vCurvaProdutos) do begin
    if vCurvaProdutos[i].Tipo <> 'PV' then
      Continue;

    if vCurvaProdutos[i].Classificacao <> 'A' then
      Continue;

    // AGRUPADOR CLASSE A
    if not agrupando then begin
      agrupando := true;
      Inc(vLinha);

      sgProdutosValor.Cells[cpvNome, vLinha]   := 'CLASSE A';
      sgProdutosValor.Cells[cpvTipoLinha, vLinha] := coLinhaCabAgrup;
    end;

    Inc(vLinha);
    Inc(ranking);
    sgProdutosValor.Cells[cpvRanking, vLinha]       := NFormat(ranking) + 'º';
    sgProdutosValor.Cells[cpvProdutoId, vLinha]     := NFormat(vCurvaProdutos[i].ProdutoId);
    sgProdutosValor.Cells[cpvNome, vLinha]          := vCurvaProdutos[i].NomeProduto;
    sgProdutosValor.Cells[cpvPrecoUnit, vLinha]     := NFormat(vCurvaProdutos[i].PrecoMedioUnitario);
    sgProdutosValor.Cells[cpvQuantidade, vLinha]    := NFormatEstoque(vCurvaProdutos[i].Quantidade);
    sgProdutosValor.Cells[cpvUnidade, vLinha]       := vCurvaProdutos[i].UnidadeVenda;
    sgProdutosValor.Cells[cpvCustoTotal, vLinha]    := NFormat(vCurvaProdutos[i].CustoTotal);
    sgProdutosValor.Cells[cpvValorTotal, vLinha]    := NFormat(vCurvaProdutos[i].ValorTotal);
    sgProdutosValor.Cells[cpvPercentual, vLinha]    := NFormat(vCurvaProdutos[i].Percentual);
    sgProdutosValor.Cells[cpvClassificacao, vLinha] := vCurvaProdutos[i].Classificacao;

    vTotaisAgrupador.totalQuantidade := vTotaisAgrupador.totalQuantidade + vCurvaProdutos[i].Quantidade;
    vTotaisAgrupador.totalValor := vTotaisAgrupador.totalValor + vCurvaProdutos[i].ValorTotal;
    vTotaisAgrupador.totalCusto := vTotaisAgrupador.totalCusto + vCurvaProdutos[i].CustoTotal;
  end;

  if agrupando then begin
    TotalizarGrupo('A');
    Inc(vLinha);
  end;

  vTotaisAgrupador.totalQuantidade := 0;
  vTotaisAgrupador.totalValor := 0;
  agrupando := false;
  for i := Low(vCurvaProdutos) to High(vCurvaProdutos) do begin
    if vCurvaProdutos[i].Tipo <> 'PV' then
      Continue;

    if vCurvaProdutos[i].Classificacao <> 'B' then
      Continue;

    // AGRUPADOR CLASSE B
    if not agrupando then begin
      agrupando := true;
      Inc(vLinha);

      sgProdutosValor.Cells[cpvNome, vLinha]   := 'CLASSE B';
      sgProdutosValor.Cells[cpvTipoLinha, vLinha] := coLinhaCabAgrup;
    end;

    Inc(vLinha);
    Inc(ranking);
    sgProdutosValor.Cells[cpvRanking, vLinha]       := NFormat(ranking) + 'º';
    sgProdutosValor.Cells[cpvProdutoId, vLinha]     := NFormat(vCurvaProdutos[i].ProdutoId);
    sgProdutosValor.Cells[cpvNome, vLinha]          := vCurvaProdutos[i].NomeProduto;
    sgProdutosValor.Cells[cpvPrecoUnit, vLinha]     := NFormat(vCurvaProdutos[i].PrecoMedioUnitario);
    sgProdutosValor.Cells[cpvQuantidade, vLinha]    := NFormatEstoque(vCurvaProdutos[i].Quantidade);
    sgProdutosValor.Cells[cpvUnidade, vLinha]       := vCurvaProdutos[i].UnidadeVenda;
    sgProdutosValor.Cells[cpvCustoTotal, vLinha]    := NFormat(vCurvaProdutos[i].CustoTotal);
    sgProdutosValor.Cells[cpvValorTotal, vLinha]    := NFormat(vCurvaProdutos[i].ValorTotal);
    sgProdutosValor.Cells[cpvPercentual, vLinha]    := NFormat(vCurvaProdutos[i].Percentual);
    sgProdutosValor.Cells[cpvClassificacao, vLinha] := vCurvaProdutos[i].Classificacao;

    vTotaisAgrupador.totalQuantidade := vTotaisAgrupador.totalQuantidade + vCurvaProdutos[i].Quantidade;
    vTotaisAgrupador.totalValor := vTotaisAgrupador.totalValor + vCurvaProdutos[i].ValorTotal;
    vTotaisAgrupador.totalCusto := vTotaisAgrupador.totalCusto + vCurvaProdutos[i].CustoTotal;
  end;

  if agrupando then begin
    TotalizarGrupo('B');
    Inc(vLinha);
  end;

  vTotaisAgrupador.totalQuantidade := 0;
  vTotaisAgrupador.totalValor := 0;
  agrupando := false;
  for i := Low(vCurvaProdutos) to High(vCurvaProdutos) do begin
    if vCurvaProdutos[i].Tipo <> 'PV' then
      Continue;

    if vCurvaProdutos[i].Classificacao <> 'C' then
      Continue;
                                                            
    // AGRUPADOR CLASSE C
    if not agrupando then begin
      agrupando := true;
      Inc(vLinha);

      sgProdutosValor.Cells[cpvNome, vLinha]   := 'CLASSE C';
      sgProdutosValor.Cells[cpvTipoLinha, vLinha] := coLinhaCabAgrup;
    end;

    Inc(vLinha);
    Inc(ranking);
    sgProdutosValor.Cells[cpvRanking, vLinha]       := NFormat(ranking) + 'º';
    sgProdutosValor.Cells[cpvProdutoId, vLinha]     := NFormat(vCurvaProdutos[i].ProdutoId);
    sgProdutosValor.Cells[cpvNome, vLinha]          := vCurvaProdutos[i].NomeProduto;
    sgProdutosValor.Cells[cpvPrecoUnit, vLinha]     := NFormat(vCurvaProdutos[i].PrecoMedioUnitario);
    sgProdutosValor.Cells[cpvQuantidade, vLinha]    := NFormatEstoque(vCurvaProdutos[i].Quantidade);
    sgProdutosValor.Cells[cpvUnidade, vLinha]       := vCurvaProdutos[i].UnidadeVenda;
    sgProdutosValor.Cells[cpvCustoTotal, vLinha]    := NFormat(vCurvaProdutos[i].CustoTotal);
    sgProdutosValor.Cells[cpvValorTotal, vLinha]    := NFormat(vCurvaProdutos[i].ValorTotal);
    sgProdutosValor.Cells[cpvPercentual, vLinha]    := NFormat(vCurvaProdutos[i].Percentual);
    sgProdutosValor.Cells[cpvClassificacao, vLinha] := vCurvaProdutos[i].Classificacao;

    vTotaisAgrupador.totalQuantidade := vTotaisAgrupador.totalQuantidade + vCurvaProdutos[i].Quantidade;
    vTotaisAgrupador.totalValor := vTotaisAgrupador.totalValor + vCurvaProdutos[i].ValorTotal;
    vTotaisAgrupador.totalCusto := vTotaisAgrupador.totalCusto + vCurvaProdutos[i].CustoTotal;
  end;

  if agrupando then begin
    TotalizarGrupo('C');
    Inc(vLinha);
  end;

  vTotaisAgrupador.totalQuantidade := 0;
  vTotaisAgrupador.totalValor := 0;
  agrupando := false;
  for i := Low(vCurvaProdutos) to High(vCurvaProdutos) do begin
    if vCurvaProdutos[i].Tipo <> 'PV' then
      Continue;

    if vCurvaProdutos[i].Classificacao <> 'D' then
      Continue;
                                                            
    // AGRUPADOR CLASSE D
    if not agrupando then begin
      agrupando := true;
      Inc(vLinha);

      sgProdutosValor.Cells[cpvNome, vLinha]   := 'CLASSE D';
      sgProdutosValor.Cells[cpvTipoLinha, vLinha] := coLinhaCabAgrup;
    end;

    Inc(vLinha);
    Inc(ranking);
    sgProdutosValor.Cells[cpvRanking, vLinha]       := NFormat(ranking) + 'º';
    sgProdutosValor.Cells[cpvProdutoId, vLinha]     := NFormat(vCurvaProdutos[i].ProdutoId);
    sgProdutosValor.Cells[cpvNome, vLinha]          := vCurvaProdutos[i].NomeProduto;
    sgProdutosValor.Cells[cpvPrecoUnit, vLinha]     := NFormat(vCurvaProdutos[i].PrecoMedioUnitario);
    sgProdutosValor.Cells[cpvQuantidade, vLinha]    := NFormatEstoque(vCurvaProdutos[i].Quantidade);
    sgProdutosValor.Cells[cpvUnidade, vLinha]       := vCurvaProdutos[i].UnidadeVenda;
    sgProdutosValor.Cells[cpvCustoTotal, vLinha]    := NFormat(vCurvaProdutos[i].CustoTotal);
    sgProdutosValor.Cells[cpvValorTotal, vLinha]    := NFormat(vCurvaProdutos[i].ValorTotal);
    sgProdutosValor.Cells[cpvPercentual, vLinha]    := NFormat(vCurvaProdutos[i].Percentual);
    sgProdutosValor.Cells[cpvClassificacao, vLinha] := vCurvaProdutos[i].Classificacao;

    vTotaisAgrupador.totalQuantidade := vTotaisAgrupador.totalQuantidade + vCurvaProdutos[i].Quantidade;
    vTotaisAgrupador.totalValor := vTotaisAgrupador.totalValor + vCurvaProdutos[i].ValorTotal;
    vTotaisAgrupador.totalCusto := vTotaisAgrupador.totalCusto + vCurvaProdutos[i].CustoTotal;
  end;

  if agrupando then begin
    TotalizarGrupo('D');
    Inc(vLinha);
  end;
    
  sgProdutosValor.RowCount := IfThen(vLinha > 1, vLinha + 1, 2);

  vLinha := 0;
  ranking := 0;
  agrupando := false;
  for i := Low(vCurvaProdutos) to High(vCurvaProdutos) do begin
    if vCurvaProdutos[i].Tipo <> 'PQ' then
      Continue;

    if vCurvaProdutos[i].Classificacao <> 'A' then
      Continue;

    // AGRUPADOR CLASSE A
    if not agrupando then begin
      agrupando := true;
      Inc(vLinha);

      sgProdutosQuantidade.Cells[cpqNome, vLinha]   := 'CLASSE A';
      sgProdutosQuantidade.Cells[cpqTipoLinha, vLinha] := coLinhaCabAgrup;
    end;

    Inc(vLinha);
    Inc(ranking);
    sgProdutosQuantidade.Cells[cpqRanking, vLinha]       := NFormat(ranking) + 'º';
    sgProdutosQuantidade.Cells[cpqProdutoId, vLinha]     := NFormat(vCurvaProdutos[i].ProdutoId);
    sgProdutosQuantidade.Cells[cpqNome, vLinha]          := vCurvaProdutos[i].NomeProduto;
    sgProdutosQuantidade.Cells[cpqUnidade, vLinha]       := vCurvaProdutos[i].UnidadeVenda;
    sgProdutosQuantidade.Cells[cpqQuantidade, vLinha]    := NFormatEstoque(vCurvaProdutos[i].Quantidade);
    sgProdutosQuantidade.Cells[cpqPercentual, vLinha]    := NFormat(vCurvaProdutos[i].Percentual);
    sgProdutosQuantidade.Cells[cpqClassificacao, vLinha] := vCurvaProdutos[i].Classificacao;

    vTotaisAgrupador.totalQuantidade := vTotaisAgrupador.totalQuantidade + vCurvaProdutos[i].Quantidade;
  end;

  if agrupando then begin
    TotalizarPorQuantidadeProduto('A');
    Inc(vLinha);
  end;

  vTotaisAgrupador.totalQuantidade := 0;
  agrupando := false;
  for i := Low(vCurvaProdutos) to High(vCurvaProdutos) do begin
    if vCurvaProdutos[i].Tipo <> 'PQ' then
      Continue;

    if vCurvaProdutos[i].Classificacao <> 'B' then
      Continue;

    // AGRUPADOR CLASSE B
    if not agrupando then begin
      agrupando := true;
      Inc(vLinha);

      sgProdutosQuantidade.Cells[cpqNome, vLinha]   := 'CLASSE B';
      sgProdutosQuantidade.Cells[cpqTipoLinha, vLinha] := coLinhaCabAgrup;
    end;

    Inc(vLinha);
    Inc(ranking);
    sgProdutosQuantidade.Cells[cpqRanking, vLinha]       := NFormat(ranking) + 'º';
    sgProdutosQuantidade.Cells[cpqProdutoId, vLinha]     := NFormat(vCurvaProdutos[i].ProdutoId);
    sgProdutosQuantidade.Cells[cpqNome, vLinha]          := vCurvaProdutos[i].NomeProduto;
    sgProdutosQuantidade.Cells[cpqUnidade, vLinha]       := vCurvaProdutos[i].UnidadeVenda;
    sgProdutosQuantidade.Cells[cpqQuantidade, vLinha]    := NFormatEstoque(vCurvaProdutos[i].Quantidade);
    sgProdutosQuantidade.Cells[cpqPercentual, vLinha]    := NFormat(vCurvaProdutos[i].Percentual);
    sgProdutosQuantidade.Cells[cpqClassificacao, vLinha] := vCurvaProdutos[i].Classificacao;

    vTotaisAgrupador.totalQuantidade := vTotaisAgrupador.totalQuantidade + vCurvaProdutos[i].Quantidade;
  end;

  if agrupando then begin
    TotalizarPorQuantidadeProduto('B');
    Inc(vLinha);
  end;

  vTotaisAgrupador.totalQuantidade := 0;
  agrupando := false;
  for i := Low(vCurvaProdutos) to High(vCurvaProdutos) do begin
    if vCurvaProdutos[i].Tipo <> 'PQ' then
      Continue;

    if vCurvaProdutos[i].Classificacao <> 'C' then
      Continue;

    // AGRUPADOR CLASSE C
    if not agrupando then begin
      agrupando := true;
      Inc(vLinha);

      sgProdutosQuantidade.Cells[cpqNome, vLinha]   := 'CLASSE C';
      sgProdutosQuantidade.Cells[cpqTipoLinha, vLinha] := coLinhaCabAgrup;
    end;

    Inc(vLinha);
    Inc(ranking);
    sgProdutosQuantidade.Cells[cpqRanking, vLinha]       := NFormat(ranking) + 'º';
    sgProdutosQuantidade.Cells[cpqProdutoId, vLinha]     := NFormat(vCurvaProdutos[i].ProdutoId);
    sgProdutosQuantidade.Cells[cpqNome, vLinha]          := vCurvaProdutos[i].NomeProduto;
    sgProdutosQuantidade.Cells[cpqUnidade, vLinha]       := vCurvaProdutos[i].UnidadeVenda;
    sgProdutosQuantidade.Cells[cpqQuantidade, vLinha]    := NFormatEstoque(vCurvaProdutos[i].Quantidade);
    sgProdutosQuantidade.Cells[cpqPercentual, vLinha]    := NFormat(vCurvaProdutos[i].Percentual);
    sgProdutosQuantidade.Cells[cpqClassificacao, vLinha] := vCurvaProdutos[i].Classificacao;

    vTotaisAgrupador.totalQuantidade := vTotaisAgrupador.totalQuantidade + vCurvaProdutos[i].Quantidade;
  end;

  if agrupando then begin
    TotalizarPorQuantidadeProduto('C');
    Inc(vLinha);
  end;

  vTotaisAgrupador.totalQuantidade := 0;
  agrupando := false;
  for i := Low(vCurvaProdutos) to High(vCurvaProdutos) do begin
    if vCurvaProdutos[i].Tipo <> 'PQ' then
      Continue;

    if vCurvaProdutos[i].Classificacao <> 'D' then
      Continue;

    // AGRUPADOR CLASSE D
    if not agrupando then begin
      agrupando := true;
      Inc(vLinha);

      sgProdutosQuantidade.Cells[cpqNome, vLinha]   := 'CLASSE D';
      sgProdutosQuantidade.Cells[cpqTipoLinha, vLinha] := coLinhaCabAgrup;
    end;

    Inc(vLinha);
    Inc(ranking);
    sgProdutosQuantidade.Cells[cpqRanking, vLinha]       := NFormat(ranking) + 'º';
    sgProdutosQuantidade.Cells[cpqProdutoId, vLinha]     := NFormat(vCurvaProdutos[i].ProdutoId);
    sgProdutosQuantidade.Cells[cpqNome, vLinha]          := vCurvaProdutos[i].NomeProduto;
    sgProdutosQuantidade.Cells[cpqUnidade, vLinha]       := vCurvaProdutos[i].UnidadeVenda;
    sgProdutosQuantidade.Cells[cpqQuantidade, vLinha]    := NFormatEstoque(vCurvaProdutos[i].Quantidade);
    sgProdutosQuantidade.Cells[cpqPercentual, vLinha]    := NFormat(vCurvaProdutos[i].Percentual);
    sgProdutosQuantidade.Cells[cpqClassificacao, vLinha] := vCurvaProdutos[i].Classificacao;

    vTotaisAgrupador.totalQuantidade := vTotaisAgrupador.totalQuantidade + vCurvaProdutos[i].Quantidade;
  end;

  if agrupando then begin
    TotalizarPorQuantidadeProduto('D');
    Inc(vLinha);
  end;

  vTotaisAgrupador.totalQuantidade := 0;
  sgProdutosQuantidade.RowCount := IfThen(vLinha > 1, vLinha + 1, 2);

  vCurvaClientes := _Relacao.CurvaABC.BuscarCurvaClientes(
    Sessao.getConexaoBanco,
    vComando,
    percentuaisOrdenados,
    classesOrdenados
  );

  vLinha := 0;
  ranking := 0;
  agrupando := false;
  for i := Low(vCurvaClientes) to High(vCurvaClientes) do begin
    if vCurvaClientes[i].Tipo <> 'CV' then
      Continue;

    if vCurvaClientes[i].Classificacao <> 'A' then
      Continue;

    // AGRUPADOR CLASSE A
    if not agrupando then begin
      agrupando := true;
      Inc(vLinha);

      sgClientesValor.Cells[ccvNome, vLinha]      := 'CLASSE A';
      sgClientesValor.Cells[ccvTipoLinha, vLinha] := coLinhaCabAgrup;
    end;

    Inc(vLinha);
    Inc(ranking);
    sgClientesValor.Cells[ccvRanking, vLinha]       := NFormat(ranking) + 'º';
    sgClientesValor.Cells[ccvClienteId, vLinha]     := NFormat(vCurvaClientes[i].ClienteId);
    sgClientesValor.Cells[ccvNome, vLinha]          := vCurvaClientes[i].NomeCliente;
    sgClientesValor.Cells[ccvQuantidade, vLinha]    := NFormat(vCurvaClientes[i].QuantidadeVendas);
    //sgClientesValor.Cells[ccvCustoTotal, vLinha]    := NFormat(vCurvaClientes[i].CustoTotal);
    sgClientesValor.Cells[ccvValorTotal, vLinha]    := NFormat(vCurvaClientes[i].ValorTotal);
    sgClientesValor.Cells[ccvPercentual, vLinha]    := NFormat(vCurvaClientes[i].Percentual);
    sgClientesValor.Cells[ccvClassificacao, vLinha] := vCurvaClientes[i].Classificacao;

    vTotaisAgrupador.totalQuantidade := vTotaisAgrupador.totalQuantidade + vCurvaClientes[i].QuantidadeVendas;
    vTotaisAgrupador.totalValor := vTotaisAgrupador.totalValor + vCurvaClientes[i].ValorTotal;
    //vTotaisAgrupador.totalCusto := vTotaisAgrupador.totalCusto + vCurvaClientes[i].CustoTotal;
  end;

  if agrupando then begin
    TotalizarPorValorCliente('A');
    Inc(vLinha);
  end;

  vTotaisAgrupador.totalQuantidade := 0;
  vTotaisAgrupador.totalValor := 0;
  //vTotaisAgrupador.totalCusto := 0;

  agrupando := false;
  for i := Low(vCurvaClientes) to High(vCurvaClientes) do begin
    if vCurvaClientes[i].Tipo <> 'CV' then
      Continue;

    if vCurvaClientes[i].Classificacao <> 'B' then
      Continue;

    // AGRUPADOR CLASSE B
    if not agrupando then begin
      agrupando := true;
      Inc(vLinha);

      sgClientesValor.Cells[ccvNome, vLinha]   := 'CLASSE B';
      sgClientesValor.Cells[ccvTipoLinha, vLinha] := coLinhaCabAgrup;
    end;

    Inc(vLinha);
    Inc(ranking);
    sgClientesValor.Cells[ccvRanking, vLinha]       := NFormat(ranking) + 'º';
    sgClientesValor.Cells[ccvClienteId, vLinha]     := NFormat(vCurvaClientes[i].ClienteId);
    sgClientesValor.Cells[ccvNome, vLinha]          := vCurvaClientes[i].NomeCliente;
    sgClientesValor.Cells[ccvQuantidade, vLinha]    := NFormat(vCurvaClientes[i].QuantidadeVendas);
    //sgClientesValor.Cells[ccvCustoTotal, vLinha]    := NFormat(vCurvaClientes[i].CustoTotal);
    sgClientesValor.Cells[ccvValorTotal, vLinha]    := NFormat(vCurvaClientes[i].ValorTotal);
    sgClientesValor.Cells[ccvPercentual, vLinha]    := NFormat(vCurvaClientes[i].Percentual);
    sgClientesValor.Cells[ccvClassificacao, vLinha] := vCurvaClientes[i].Classificacao;

    vTotaisAgrupador.totalQuantidade := vTotaisAgrupador.totalQuantidade + vCurvaClientes[i].QuantidadeVendas;
    vTotaisAgrupador.totalValor := vTotaisAgrupador.totalValor + vCurvaClientes[i].ValorTotal;
    //vTotaisAgrupador.totalCusto := vTotaisAgrupador.totalCusto + vCurvaClientes[i].CustoTotal;
  end;

  if agrupando then begin
    TotalizarPorValorCliente('B');
    Inc(vLinha);
  end;

  vTotaisAgrupador.totalQuantidade := 0;
  vTotaisAgrupador.totalValor := 0;
  //vTotaisAgrupador.totalCusto := 0;

  agrupando := false;
  for i := Low(vCurvaClientes) to High(vCurvaClientes) do begin
    if vCurvaClientes[i].Tipo <> 'CV' then
      Continue;

    if vCurvaClientes[i].Classificacao <> 'C' then
      Continue;

    // AGRUPADOR CLASSE C
    if not agrupando then begin
      agrupando := true;
      Inc(vLinha);

      sgClientesValor.Cells[ccvNome, vLinha]   := 'CLASSE C';
      sgClientesValor.Cells[ccvTipoLinha, vLinha] := coLinhaCabAgrup;
    end;

    Inc(vLinha);
    Inc(ranking);
    sgClientesValor.Cells[ccvRanking, vLinha]       := NFormat(ranking) + 'º';
    sgClientesValor.Cells[ccvClienteId, vLinha]     := NFormat(vCurvaClientes[i].ClienteId);
    sgClientesValor.Cells[ccvNome, vLinha]          := vCurvaClientes[i].NomeCliente;
    sgClientesValor.Cells[ccvQuantidade, vLinha]    := NFormat(vCurvaClientes[i].QuantidadeVendas);
    //sgClientesValor.Cells[ccvCustoTotal, vLinha]    := NFormat(vCurvaClientes[i].CustoTotal);
    sgClientesValor.Cells[ccvValorTotal, vLinha]    := NFormat(vCurvaClientes[i].ValorTotal);
    sgClientesValor.Cells[ccvPercentual, vLinha]    := NFormat(vCurvaClientes[i].Percentual);
    sgClientesValor.Cells[ccvClassificacao, vLinha] := vCurvaClientes[i].Classificacao;

    vTotaisAgrupador.totalQuantidade := vTotaisAgrupador.totalQuantidade + vCurvaClientes[i].QuantidadeVendas;
    vTotaisAgrupador.totalValor := vTotaisAgrupador.totalValor + vCurvaClientes[i].ValorTotal;
    //vTotaisAgrupador.totalCusto := vTotaisAgrupador.totalCusto + vCurvaClientes[i].CustoTotal;
  end;

  if agrupando then begin
    TotalizarPorValorCliente('C');
    Inc(vLinha);
  end;

  vTotaisAgrupador.totalQuantidade := 0;
  vTotaisAgrupador.totalValor := 0;
  //vTotaisAgrupador.totalCusto := 0;

  agrupando := false;
  for i := Low(vCurvaClientes) to High(vCurvaClientes) do begin
    if vCurvaClientes[i].Tipo <> 'CV' then
      Continue;

    if vCurvaClientes[i].Classificacao <> 'D' then
      Continue;

    // AGRUPADOR CLASSE D
    if not agrupando then begin
      agrupando := true;
      Inc(vLinha);

      sgClientesValor.Cells[ccvNome, vLinha]   := 'CLASSE D';
      sgClientesValor.Cells[ccvTipoLinha, vLinha] := coLinhaCabAgrup;
    end;

    Inc(vLinha);
    Inc(ranking);
    sgClientesValor.Cells[ccvRanking, vLinha]       := NFormat(ranking) + 'º';
    sgClientesValor.Cells[ccvClienteId, vLinha]     := NFormat(vCurvaClientes[i].ClienteId);
    sgClientesValor.Cells[ccvNome, vLinha]          := vCurvaClientes[i].NomeCliente;
    sgClientesValor.Cells[ccvQuantidade, vLinha]    := NFormat(vCurvaClientes[i].QuantidadeVendas);
    //sgClientesValor.Cells[ccvCustoTotal, vLinha]    := NFormat(vCurvaClientes[i].CustoTotal);
    sgClientesValor.Cells[ccvValorTotal, vLinha]    := NFormat(vCurvaClientes[i].ValorTotal);
    sgClientesValor.Cells[ccvPercentual, vLinha]    := NFormat(vCurvaClientes[i].Percentual);
    sgClientesValor.Cells[ccvClassificacao, vLinha] := vCurvaClientes[i].Classificacao;

    vTotaisAgrupador.totalQuantidade := vTotaisAgrupador.totalQuantidade + vCurvaClientes[i].QuantidadeVendas;
    vTotaisAgrupador.totalValor := vTotaisAgrupador.totalValor + vCurvaClientes[i].ValorTotal;
    //vTotaisAgrupador.totalCusto := vTotaisAgrupador.totalCusto + vCurvaClientes[i].CustoTotal;
  end;

  if agrupando then begin
    TotalizarPorValorCliente('D');
    Inc(vLinha);
  end;

  vTotaisAgrupador.totalQuantidade := 0;
  vTotaisAgrupador.totalValor := 0;
  //vTotaisAgrupador.totalCusto := 0;

  sgClientesValor.RowCount := IfThen(vLinha > 1, vLinha + 1, 2);

  agrupando := false;
  ranking := 0;
  vLinha := 0;
  for i := Low(vCurvaClientes) to High(vCurvaClientes) do begin
    if vCurvaClientes[i].Tipo <> 'CQ' then
      Continue;

    if vCurvaClientes[i].Classificacao <> 'A' then
      Continue;

    // AGRUPADOR CLASSE A
    if not agrupando then begin
      agrupando := true;
      Inc(vLinha);

      sgClientesQuantidade.Cells[ccqNome, vLinha]   := 'CLASSE A';
      sgClientesQuantidade.Cells[ccqTipoLinha, vLinha] := coLinhaCabAgrup;
    end;

    Inc(vLinha);
    Inc(ranking);
    sgClientesQuantidade.Cells[ccqRanking, vLinha]       := NFormat(ranking) + 'º';
    sgClientesQuantidade.Cells[ccqClienteId, vLinha]     := NFormat(vCurvaClientes[i].ClienteId);
    sgClientesQuantidade.Cells[ccqNome, vLinha]          := vCurvaClientes[i].NomeCliente;
    sgClientesQuantidade.Cells[ccqQuantidade, vLinha]    := NFormat(vCurvaClientes[i].QuantidadeVendas);
    sgClientesQuantidade.Cells[ccqPercentual, vLinha]    := NFormat(vCurvaClientes[i].Percentual);
    sgClientesQuantidade.Cells[ccqClassificacao, vLinha] := vCurvaClientes[i].Classificacao;

    vTotaisAgrupador.totalQuantidade := vTotaisAgrupador.totalQuantidade + vCurvaClientes[i].QuantidadeVendas;
  end;

  if agrupando then begin
    TotalizarPorQuantidadeCliente('A');
    Inc(vLinha);
  end;

  vTotaisAgrupador.totalQuantidade := 0;
  vTotaisAgrupador.totalValor := 0;

  agrupando := false;
  for i := Low(vCurvaClientes) to High(vCurvaClientes) do begin
    if vCurvaClientes[i].Tipo <> 'CQ' then
      Continue;

    if vCurvaClientes[i].Classificacao <> 'B' then
      Continue;

    // AGRUPADOR CLASSE B
    if not agrupando then begin
      agrupando := true;
      Inc(vLinha);

      sgClientesQuantidade.Cells[ccqNome, vLinha]   := 'CLASSE B';
      sgClientesQuantidade.Cells[ccqTipoLinha, vLinha] := coLinhaCabAgrup;
    end;

    Inc(vLinha);
    Inc(ranking);
    sgClientesQuantidade.Cells[ccqRanking, vLinha]       := NFormat(ranking) + 'º';
    sgClientesQuantidade.Cells[ccqClienteId, vLinha]     := NFormat(vCurvaClientes[i].ClienteId);
    sgClientesQuantidade.Cells[ccqNome, vLinha]          := vCurvaClientes[i].NomeCliente;
    sgClientesQuantidade.Cells[ccqQuantidade, vLinha]    := NFormat(vCurvaClientes[i].QuantidadeVendas);
    sgClientesQuantidade.Cells[ccqPercentual, vLinha]    := NFormat(vCurvaClientes[i].Percentual);
    sgClientesQuantidade.Cells[ccqClassificacao, vLinha] := vCurvaClientes[i].Classificacao;

    vTotaisAgrupador.totalQuantidade := vTotaisAgrupador.totalQuantidade + vCurvaClientes[i].QuantidadeVendas;
  end;

  if agrupando then begin
    TotalizarPorQuantidadeCliente('B');
    Inc(vLinha);
  end;

  vTotaisAgrupador.totalQuantidade := 0;
  vTotaisAgrupador.totalValor := 0;

  agrupando := false;
  for i := Low(vCurvaClientes) to High(vCurvaClientes) do begin
    if vCurvaClientes[i].Tipo <> 'CQ' then
      Continue;

    if vCurvaClientes[i].Classificacao <> 'C' then
      Continue;

    // AGRUPADOR CLASSE C
    if not agrupando then begin
      agrupando := true;
      Inc(vLinha);

      sgClientesQuantidade.Cells[ccqNome, vLinha]   := 'CLASSE C';
      sgClientesQuantidade.Cells[ccqTipoLinha, vLinha] := coLinhaCabAgrup;
    end;

    Inc(vLinha);
    Inc(ranking);
    sgClientesQuantidade.Cells[ccqRanking, vLinha]       := NFormat(ranking) + 'º';
    sgClientesQuantidade.Cells[ccqClienteId, vLinha]     := NFormat(vCurvaClientes[i].ClienteId);
    sgClientesQuantidade.Cells[ccqNome, vLinha]          := vCurvaClientes[i].NomeCliente;
    sgClientesQuantidade.Cells[ccqQuantidade, vLinha]    := NFormat(vCurvaClientes[i].QuantidadeVendas);
    sgClientesQuantidade.Cells[ccqPercentual, vLinha]    := NFormat(vCurvaClientes[i].Percentual);
    sgClientesQuantidade.Cells[ccqClassificacao, vLinha] := vCurvaClientes[i].Classificacao;

    vTotaisAgrupador.totalQuantidade := vTotaisAgrupador.totalQuantidade + vCurvaClientes[i].QuantidadeVendas;
  end;

  if agrupando then begin
    TotalizarPorQuantidadeCliente('C');
    Inc(vLinha);
  end;

  vTotaisAgrupador.totalQuantidade := 0;
  vTotaisAgrupador.totalValor := 0;

  agrupando := false;
  for i := Low(vCurvaClientes) to High(vCurvaClientes) do begin
    if vCurvaClientes[i].Tipo <> 'CQ' then
      Continue;

    if vCurvaClientes[i].Classificacao <> 'D' then
      Continue;

    // AGRUPADOR CLASSE D
    if not agrupando then begin
      agrupando := true;
      Inc(vLinha);

      sgClientesQuantidade.Cells[ccqNome, vLinha]   := 'CLASSE D';
      sgClientesQuantidade.Cells[ccqTipoLinha, vLinha] := coLinhaCabAgrup;
    end;

    Inc(vLinha);
    Inc(ranking);
    sgClientesQuantidade.Cells[ccqRanking, vLinha]       := NFormat(ranking) + 'º';
    sgClientesQuantidade.Cells[ccqClienteId, vLinha]     := NFormat(vCurvaClientes[i].ClienteId);
    sgClientesQuantidade.Cells[ccqNome, vLinha]          := vCurvaClientes[i].NomeCliente;
    sgClientesQuantidade.Cells[ccqQuantidade, vLinha]    := NFormat(vCurvaClientes[i].QuantidadeVendas);
    sgClientesQuantidade.Cells[ccqPercentual, vLinha]    := NFormat(vCurvaClientes[i].Percentual);
    sgClientesQuantidade.Cells[ccqClassificacao, vLinha] := vCurvaClientes[i].Classificacao;

    vTotaisAgrupador.totalQuantidade := vTotaisAgrupador.totalQuantidade + vCurvaClientes[i].QuantidadeVendas;
  end;

  if agrupando then begin
    TotalizarPorQuantidadeCliente('D');
    Inc(vLinha);
  end;

  vTotaisAgrupador.totalQuantidade := 0;
  vTotaisAgrupador.totalValor := 0;

  sgClientesQuantidade.RowCount := IfThen(vLinha > 1, vLinha + 1, 2);

  pcDados.ActivePage := tsResultado;
  pcResultado.ActivePage := tsPorProdutosValorTotal;
  SetarFoco(sgProdutosValor);
end;

procedure TFormRelacaoCurvaABC.FormCreate(Sender: TObject);
begin
  inherited;
  ActiveControl := FrEmpresas.sgPesquisa;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId, False);
  tsSinteticoTipoCobranca.TabVisible := False;
  ts1.TabVisible := False;
end;

procedure TFormRelacaoCurvaABC.Imprimir(Sender: TObject);
var
  vCnt: Integer;
begin
  inherited;
  if pcResultado.ActivePage = tsPorProdutosValorTotal then begin
    cdsValorProdutos.Close;
    cdsValorProdutos.CreateDataSet;
    cdsValorProdutos.Open;

    for vCnt := sgProdutosValor.RowCount - 1 downto 1 do
    begin

      cdsValorProdutos.Insert;

      if (sgProdutosValor.Cells[cpvProdutoId,vCnt] = '') and (sgProdutosValor.Cells[cpvTipoLinha,vCnt] <> coLinhaTotVenda) then begin
        cdsValorProdutoNegrito.AsString         := sgProdutosValor.Cells[cpvNome,vCnt];

        if Pos('TOTAL', cdsValorProdutoNegrito.AsString) > 0 then begin
          cdsValorQuantidadeNegrito.AsString    := sgProdutosValor.Cells[cpvQuantidade,vCnt];
          cdsValorCustoTotalNegrito.AsString    := sgProdutosValor.Cells[cpvCustoTotal,vCnt];
          cdsValorTotalNegrito.AsString         := sgProdutosValor.Cells[cpvValorTotal,vCnt];
        end;
      end
      else begin
        cdsValorRanking.AsString       := sgProdutosValor.Cells[cpvRanking,vCnt];
        CdsValorProduto.AsString       := sgProdutosValor.Cells[cpvProdutoId,vCnt] + ' - ' + sgProdutosValor.Cells[cpvNome,vCnt];
        cdsValorPrecoUnitario.AsString := sgProdutosValor.Cells[cpvPrecoUnit,vCnt];
        cdsValorQuantidade.AsString    := sgProdutosValor.Cells[cpvQuantidade,vCnt];
        cdsValorUnidade.AsString       := sgProdutosValor.Cells[cpvUnidade,vCnt];
        cdsValorCustoTotal.AsString    := sgProdutosValor.Cells[cpvCustoTotal,vCnt];
        cdsValorTotal.AsString         := sgProdutosValor.Cells[cpvValorTotal,vCnt];
        cdsValorPercentual.AsString    := sgProdutosValor.Cells[cpvPercentual,vCnt];
        cdsValorClassificacao.AsString := sgProdutosValor.Cells[cpvClassificacao,vCnt];
      end;

      cdsValorProdutos.Post;
    end;

    frxReport.ShowReport;
  end
  else begin
    ClientDataSet1.Close;
    ClientDataSet1.CreateDataSet;
    ClientDataSet1.Open;

    for vCnt := sgProdutosQuantidade.RowCount - 1 downto 1 do
    begin

      ClientDataSet1.Insert;

//  cpqRanking       = 0;
//  cpqProdutoId     = 1;
//  cpqNome          = 2;
//  cpqUnidade       = 3;
//  cpqQuantidade    = 4;
//  cpqPercentual    = 5;
//  cpqClassificacao = 6;
//  cpqTipoLinha     = 7;

      if (sgProdutosQuantidade.Cells[cpqProdutoId,vCnt] = '') and (sgProdutosQuantidade.Cells[cpqTipoLinha,vCnt] <> coLinhaTotVenda) then begin
        cdsQuantidadeClienteNegrito.AsString         := sgProdutosQuantidade.Cells[cpqNome,vCnt];
        cdsQuantidadeQuantidadeNegrito.AsString      := sgProdutosQuantidade.Cells[cpvQuantidade,vCnt];

//        if Pos('TOTAL', cdsQuantidadeClienteNegrito.AsString) > 0 then begin
//        end;
      end
      else begin
        cdsQuantidadeRanking.AsString       := sgProdutosQuantidade.Cells[cpqRanking,vCnt];
        cdsQuantidadeCliente.AsString       := sgProdutosQuantidade.Cells[cpqProdutoId,vCnt] + ' - ' + sgProdutosQuantidade.Cells[cpqNome,vCnt];
        cdsQuantidadeQuantidade.AsString    := sgProdutosQuantidade.Cells[cpqQuantidade,vCnt];
        cdsQuantidadeUnidade.AsString       := sgProdutosQuantidade.Cells[cpqUnidade,vCnt];
        cdsQuantidadePercentual.AsString    := sgProdutosQuantidade.Cells[cpqPercentual,vCnt];
        cdsQuantidadeClassificacao.AsString := sgProdutosQuantidade.Cells[cpqClassificacao,vCnt];
      end;

      ClientDataSet1.Post;
    end;

    frxReport1.ShowReport;
  end;
end;

procedure TFormRelacaoCurvaABC.sgClientesQuantidadeDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[ccqClienteId, ccqQuantidade, ccqPercentual] then
    vAlinhamento := taRightJustify
  else if ACol = ccqClassificacao then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgClientesQuantidade.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoCurvaABC.sgClientesQuantidadeGetCellColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if sgClientesQuantidade.Cells[ccqTipoLinha, ARow] = coLinhaTotVenda then begin
    AFont.Color := clWhite;
    AFont.Style := [fsBold];
    ABrush.Color := $000096DB;
  end
  else if sgClientesQuantidade.Cells[ccqTipoLinha, ARow] = coLinhaCabAgrup then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end
  else if sgClientesQuantidade.Cells[ccqTipoLinha, ARow] = coTotalAgrupador then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao3;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao3;
  end;
end;

procedure TFormRelacaoCurvaABC.sgClientesValorDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[ccvClienteId, ccvQuantidade, ccvValorTotal, ccvPercentual] then
    vAlinhamento := taRightJustify
  else if ACol = ccvClassificacao then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgClientesValor.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoCurvaABC.sgClientesValorGetCellColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if sgClientesValor.Cells[ccvTipoLinha, ARow] = coLinhaTotVenda then begin
    AFont.Color := clWhite;
    AFont.Style := [fsBold];
    ABrush.Color := $000096DB;
  end
  else if sgClientesValor.Cells[ccvTipoLinha, ARow] = coLinhaCabAgrup then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end
  else if sgClientesValor.Cells[ccvTipoLinha, ARow] = coTotalAgrupador then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao3;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao3;
  end;
end;

procedure TFormRelacaoCurvaABC.sgProdutosQuantidadeDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[cpqProdutoId, cpqQuantidade, cpqPercentual] then
    vAlinhamento := taRightJustify
  else if ACol = cpqClassificacao then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgProdutosQuantidade.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoCurvaABC.sgProdutosQuantidadeGetCellColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if sgProdutosQuantidade.Cells[cpqTipoLinha, ARow] = coLinhaTotVenda then begin
    AFont.Color := clWhite;
    AFont.Style := [fsBold];
    ABrush.Color := $000096DB;
  end
  else if sgProdutosQuantidade.Cells[cpqTipoLinha, ARow] = coLinhaCabAgrup then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end
  else if sgProdutosQuantidade.Cells[cpqTipoLinha, ARow] = coTotalAgrupador then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao3;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao3;
  end;
end;

procedure TFormRelacaoCurvaABC.sgProdutosValorDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[cpvProdutoId, cpvPrecoUnit, cpvQuantidade, cpvValorTotal, cpvPercentual] then
    vAlinhamento := taRightJustify
  else if ACol = cpvClassificacao then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgProdutosValor.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoCurvaABC.sgProdutosValorGetCellColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if sgProdutosValor.Cells[cpvTipoLinha, ARow] = coLinhaTotVenda then begin
    AFont.Color := clWhite;
    AFont.Style := [fsBold];
    ABrush.Color := $000096DB;
  end
  else if sgProdutosValor.Cells[cpvTipoLinha, ARow] = coLinhaCabAgrup then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end
  else if sgProdutosValor.Cells[cpvTipoLinha, ARow] = coTotalAgrupador then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao3;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao3;
  end;
end;

procedure TFormRelacaoCurvaABC.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if percClasseA.AsInt = 0 then begin
    Exclamar('Necessário informar o % Classe D');
    SetarFoco(percClasseA);
    Abort;
  end;

  if percClasseB.AsInt = 0 then begin
    Exclamar('Necessário informar o % Classe D');
    SetarFoco(percClasseB);
    Abort;
  end;

  if percClasseC.AsInt = 0 then begin
    Exclamar('Necessário informar o % Classe D');
    SetarFoco(percClasseC);
    Abort;
  end;

  if percClasseD.AsInt = 0 then begin
    Exclamar('Necessário informar o % Classe D');
    SetarFoco(percClasseD);
    Abort;
  end;

end;

end.
