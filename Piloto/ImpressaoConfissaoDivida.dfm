inherited FormImpressaoConfissaoDivida: TFormImpressaoConfissaoDivida
  Caption = 'FormImpressaoConfissaoDivida'
  ExplicitWidth = 1030
  ExplicitHeight = 754
  PixelsPerInch = 96
  TextHeight = 13
  inherited qrRelatorioNF: TQuickRep
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Page.Values = (
      0.000000000000000000
      509.300000000000000000
      0.000000000000000000
      750.100000000000000000
      0.000000000000000000
      0.000000000000000000
      0.000000000000000000)
    inherited qrBandTitulo: TQRBand
      Size.Values = (
        289.388020833333300000
        750.755208333333300000)
      inherited qrlNFNomeEmpresa: TQRLabel
        Size.Values = (
          34.726562500000000000
          6.614583333333332000
          16.536458333333330000
          744.140625000000000000)
        FontSize = 6
      end
      inherited qrNFCnpj: TQRLabel
        Top = 31
        Size.Values = (
          33.072916666666670000
          6.614583333333332000
          51.263020833333340000
          744.140625000000000000)
        FontSize = 7
        ExplicitTop = 31
      end
      inherited qrNFEndereco: TQRLabel
        Size.Values = (
          34.726562500000000000
          6.614583333333332000
          85.989583333333340000
          744.140625000000000000)
        FontSize = 7
      end
      inherited qrNFCidadeUf: TQRLabel
        Size.Values = (
          42.994791666666670000
          6.614583333333332000
          122.369791666666700000
          744.140625000000000000)
        FontSize = 7
      end
      inherited qrNFTipoDocumento: TQRLabel
        Size.Values = (
          33.072916666666670000
          1.653645833333333000
          211.666666666666700000
          719.335937500000000000)
        FontSize = 7
      end
      inherited qrSeparador2: TQRShape
        Size.Values = (
          3.307291666666666000
          3.307291666666666000
          281.119791666666700000
          724.296875000000000000)
      end
      inherited qrNFTitulo: TQRLabel
        Size.Values = (
          33.072916666666670000
          1.653645833333333000
          246.393229166666700000
          719.335937500000000000)
        FontSize = 7
      end
      inherited qrlNFEmitidoEm: TQRLabel
        Size.Values = (
          23.151041666666670000
          370.416666666666700000
          3.307291666666666000
          357.187500000000000000)
        FontSize = -6
      end
      inherited qrlNFTelefoneEmpresa: TQRLabel
        Top = 101
        Size.Values = (
          42.994791666666670000
          6.614583333333332000
          167.018229166666700000
          744.140625000000000000)
        FontSize = 7
        ExplicitTop = 101
      end
    end
    inherited PageFooterBand1: TQRBand
      Size.Values = (
        24.804687500000000000
        750.755208333333300000)
      inherited qrlNFSistema: TQRLabel
        Size.Values = (
          23.151041666666670000
          582.083333333333400000
          0.000000000000000000
          138.906250000000000000)
        FontSize = -6
      end
    end
  end
  inherited qrRelatorioA4: TQuickRep
    Height = 561
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Page.PaperSize = Custom
    Page.Values = (
      100.000000000000000000
      1485.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    ExplicitHeight = 561
    inherited qrCabecalho: TQRBand
      Height = 88
      Size.Values = (
        232.833333333333300000
        1899.708333333333000000)
      ExplicitHeight = 88
      inherited qrCaptionEndereco: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          44.979166666666670000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrEmpresa: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          7.937500000000000000
          568.854166666666700000)
        FontSize = 7
      end
      inherited qrEndereco: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          44.979166666666670000
          1513.416666666667000000)
        FontSize = 7
      end
      inherited qrLogoEmpresa: TQRImage
        Size.Values = (
          211.666666666666700000
          13.229166666666670000
          2.645833333333333000
          211.666666666666700000)
      end
      inherited qrCaptionEmpresa: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          7.937500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrEmitidoEm: TQRLabel
        Size.Values = (
          29.104166666666670000
          1529.291666666667000000
          0.000000000000000000
          370.416666666666700000)
        FontSize = 5
      end
      inherited qr1: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          82.020833333333320000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrBairro: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          82.020833333333320000
          568.854166666666700000)
        FontSize = 7
      end
      inherited qr3: TQRLabel
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          82.020833333333320000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrCidadeUf: TQRLabel
        Width = 215
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          82.020833333333320000
          568.854166666666700000)
        FontSize = 7
        ExplicitWidth = 215
      end
      inherited qr5: TQRLabel
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          7.937500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrCNPJ: TQRLabel
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          7.937500000000000000
          248.708333333333300000)
        FontSize = 7
      end
      inherited qr7: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrTelefone: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          119.062500000000000000
          187.854166666666700000)
        FontSize = 7
      end
      inherited qrFax: TQRLabel
        Size.Values = (
          34.395833333333330000
          767.291666666666800000
          119.062500000000000000
          187.854166666666700000)
        FontSize = 7
      end
      inherited qr10: TQRLabel
        Size.Values = (
          34.395833333333330000
          603.250000000000000000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qr11: TQRLabel
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrEmail: TQRLabel
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          119.062500000000000000
          769.937500000000000000)
        FontSize = 7
      end
      inherited qr13: TQRLabel
        Size.Values = (
          52.916666666666670000
          711.729166666666800000
          164.041666666666700000
          529.166666666666700000)
        Caption = 'CONFISS'#195'O DE D'#205'VIDA'
        FontSize = 12
      end
      inherited qrLabelCep: TQRLabel
        Size.Values = (
          39.687500000000000000
          1656.291666666667000000
          82.020833333333330000
          74.083333333333330000)
        FontSize = 9
      end
      inherited qrCEP: TQRLabel
        Size.Values = (
          39.687500000000000000
          1735.666666666667000000
          82.020833333333330000
          171.979166666666700000)
        FontSize = 9
      end
      object QRShape4: TQRShape
        Left = 4
        Top = 82
        Width = 713
        Height = 2
        Size.Values = (
          5.291666666666667000
          10.583333333333330000
          216.958333333333400000
          1886.479166666667000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
    end
    object qrbndDetailBand1: TQRBand [1]
      Left = 38
      Top = 126
      Width = 718
      Height = 306
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        809.625000000000000000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbDetail
      object qrMeConfissao: TQRMemo
        AlignWithMargins = True
        Left = 3
        Top = 0
        Width = 712
        Height = 258
        Size.Values = (
          682.625000000000000000
          7.937500000000000000
          0.000000000000000000
          1883.833333333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = True
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        Lines.Strings = (
          'Confiss'#227'o da d'#237'vida:')
        ParentFont = False
        Transparent = False
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 7
      end
      object qr8: TQRLabel
        Left = 5
        Top = 268
        Width = 42
        Height = 15
        Size.Values = (
          39.687500000000000000
          13.229166666666670000
          709.083333333333300000
          111.125000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Pedido:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrPedido: TQRLabel
        Left = 49
        Top = 268
        Width = 72
        Height = 15
        Size.Values = (
          39.687500000000000000
          129.645833333333300000
          709.083333333333400000
          190.500000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '2.500'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr9: TQRLabel
        Left = 5
        Top = 284
        Width = 67
        Height = 15
        Size.Values = (
          39.687500000000000000
          13.229166666666670000
          751.416666666666700000
          177.270833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Dt/Hora rec.:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrDataHoraRecebimento: TQRLabel
        Left = 74
        Top = 284
        Width = 72
        Height = 15
        Size.Values = (
          39.687500000000000000
          195.791666666666700000
          751.416666666666800000
          190.500000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '01/09/2018 15:25:52'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr14: TQRLabel
        Left = 175
        Top = 268
        Width = 59
        Height = 15
        Size.Values = (
          39.687500000000000000
          463.020833333333300000
          709.083333333333300000
          156.104166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Vendedor:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrVendedor: TQRLabel
        Left = 235
        Top = 268
        Width = 270
        Height = 15
        Size.Values = (
          39.687500000000000000
          621.770833333333400000
          709.083333333333400000
          714.375000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '5 - EZEQUIEL EUGENIO TESTE'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr2: TQRLabel
        Left = 175
        Top = 284
        Width = 33
        Height = 15
        Size.Values = (
          39.687500000000000000
          463.020833333333300000
          751.416666666666700000
          87.312500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Valor:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrValorAcumulado: TQRLabel
        Left = 209
        Top = 284
        Width = 112
        Height = 15
        Size.Values = (
          39.687500000000000000
          552.979166666666800000
          751.416666666666800000
          296.333333333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'R$ 600,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrCodigoCliente: TQRLabel
        Left = 401
        Top = 284
        Width = 112
        Height = 15
        Size.Values = (
          39.687500000000000000
          1060.979166666667000000
          751.416666666666800000
          296.333333333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '2.500'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qr12: TQRLabel
        Left = 334
        Top = 284
        Width = 66
        Height = 15
        Size.Values = (
          39.687500000000000000
          883.708333333333300000
          751.416666666666700000
          174.625000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'C'#243'd.cliente:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
    end
    inherited qrbndRodape: TQRBand
      Top = 432
      Height = 80
      Size.Values = (
        211.666666666666700000
        1899.708333333333000000)
      ExplicitTop = 432
      ExplicitHeight = 80
      inherited qrUsuarioImpressao: TQRLabel
        Left = 5
        Top = 63
        Enabled = False
        Size.Values = (
          31.750000000000000000
          13.229166666666670000
          166.687500000000000000
          354.541666666666700000)
        FontSize = 7
        ExplicitLeft = 5
        ExplicitTop = 63
      end
      inherited qrSistema: TQRLabel
        Left = 640
        Top = 63
        Width = 67
        Enabled = False
        Size.Values = (
          31.750000000000000000
          1693.333333333333000000
          166.687500000000000000
          177.270833333333300000)
        Caption = 'Hiva 3.0'
        FontSize = 7
        ExplicitLeft = 640
        ExplicitTop = 63
        ExplicitWidth = 67
      end
      object qrlCliente: TQRLabel
        Left = 385
        Top = 32
        Width = 40
        Height = 15
        Size.Values = (
          39.687500000000000000
          1018.645833333333000000
          84.666666666666670000
          105.833333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Cliente'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrlCpfCnpjCliente: TQRLabel
        Left = 385
        Top = 47
        Width = 23
        Height = 15
        Size.Values = (
          39.687500000000000000
          1018.645833333333000000
          124.354166666666700000
          60.854166666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Cpf:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRShape2: TQRShape
        Left = 385
        Top = 21
        Width = 300
        Height = 11
        Size.Values = (
          29.104166666666670000
          1018.645833333333000000
          55.562500000000000000
          793.750000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape1: TQRShape
        Left = 31
        Top = 21
        Width = 317
        Height = 11
        Size.Values = (
          29.104166666666670000
          82.020833333333340000
          55.562500000000000000
          838.729166666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object qrEmpresaCredora: TQRLabel
        Left = 31
        Top = 32
        Width = 50
        Height = 15
        Size.Values = (
          39.687500000000000000
          82.020833333333330000
          84.666666666666670000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Empresa'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrCNPJEmpresaCredora: TQRLabel
        Left = 31
        Top = 47
        Width = 98
        Height = 15
        Size.Values = (
          39.687500000000000000
          82.020833333333330000
          124.354166666666700000
          259.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = '00.000.000/0001-40'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
    end
  end
end
