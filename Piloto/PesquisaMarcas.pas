unit PesquisaMarcas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _Biblioteca, System.Math,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _Sessao, _Marcas, _RecordsCadastros,
  Vcl.ExtCtrls;

type
  TFormPesquisaMarcas = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgPesquisaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  protected
    procedure BuscarRegistros; override;
  end;

function PesquisarMarca: TObject;

implementation

{$R *.dfm}

const
  cp_nome   = 2;
  cp_ativo  = 3;

function PesquisarMarca: TObject;
var
  r: TObject;
begin
  r := _HerancaPesquisas.Pesquisar(TFormPesquisaMarcas, _Marcas.GetFiltros);
  if r = nil then
    Result := nil
  else
    Result := RecMarcas(r);
end;

{ TFormPesquisaMarcas }

procedure TFormPesquisaMarcas.BuscarRegistros;
var
  i: Integer;
  marcas: TArray<RecMarcas>;
begin
  inherited;

  marcas :=
    _Marcas.BuscarMarcas(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text]
    );

  if marcas = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(marcas);

  for i := Low(marcas) to High(marcas) do begin
    sgPesquisa.Cells[coSelecionado, i + 1] := charNaoSelecionado;//'N�o';
    sgPesquisa.Cells[coCodigo, i + 1] := IntToStr(marcas[i].marca_id);
    sgPesquisa.Cells[cp_nome, i + 1]  := marcas[i].nome;
    sgPesquisa.Cells[cp_ativo, i + 1] := marcas[i].ativo;
  end;

  sgPesquisa.RowCount := IfThen(Length(marcas) = 1, 2, High(marcas) + 2);
  sgPesquisa.SetFocus;
end;

procedure TFormPesquisaMarcas.sgPesquisaDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  alinhamento: TAlignment;
begin
  inherited;

  if _Biblioteca.Em(ACol, [coSelecionado, cp_ativo]) then
    alinhamento := taCenter
  else if ACol = coCodigo then
    alinhamento := taRightJustify
  else
    alinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], alinhamento, Rect);
end;

procedure TFormPesquisaMarcas.sgPesquisaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  MarcarDesmarcarTodos(sgPesquisa, coSelecionado, Key ,Shift);
end;

end.
