unit RelacaoAjustesEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, _AjustesEstoque, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameEmpresas, FrameFuncionarios, FrameDataInicialFinal, Frame.MotivosAjusteEstoque,
  Vcl.Grids, GridLuka, Vcl.StdCtrls, StaticTextLuka, _Sessao, _Biblioteca, _AjustesEstoqueItens, _RecordsEstoques,
  FrameNumeros, Informacoes.AjustaEstoque, FrameProdutos, CheckBoxLuka,
  ComboBoxLuka;

type
  TFormRelacaoAjusteEstoque = class(TFormHerancaRelatoriosPageControl)
    FrEmpresas: TFrEmpresas;
    FrMotivosAjusteEstoque: TFrMotivosAjusteEstoque;
    FrDataCadastro: TFrDataInicialFinal;
    FrUsuarioInsercao: TFrFuncionarios;
    StaticTextLuka1: TStaticTextLuka;
    sgAjustes: TGridLuka;
    splSeparador: TSplitter;
    StaticTextLuka2: TStaticTextLuka;
    sgItens: TGridLuka;
    FrCodigoAjuste: TFrNumeros;
    FrProdutos: TFrProdutos;
    SpeedButton1: TSpeedButton;
    Panel1: TPanel;
    st4: TStaticText;
    st1: TStaticText;
    stTotalSaidas: TStaticText;
    stTotalEntradas: TStaticText;
    Label2: TLabel;
    cbTipoEstoqueAjustado: TComboBoxLuka;
    Label1: TLabel;
    cbAgrupamento: TComboBoxLuka;
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgAjustesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgAjustesClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sgAjustesDblClick(Sender: TObject);
    procedure sgAjustesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure SpeedButton1Click(Sender: TObject);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure FormCreate(Sender: TObject);
  private
    FItens: TArray<RecAjusteEstoqueItens>;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

uses _RecordsEspeciais, _ComunicacaoNFE;

const
  coAjusteEstoqueId     = 0;
  coEmpresa             = 1;
  coTipoAnalitico       = 2;
  coValorTotal          = 3;
  coDataHoraAjuste      = 4;
  coUsuarioAjuste       = 5;
  coMotivoAjuste        = 6;
  coTipoEstoqueAjustado = 7;
  coObservacao          = 8;
  coTipo                = 9;

  coTipoLinha           = 10;

  (* Itens *)
  ciProdutoId         = 0;
  ciNome              = 1;
  ciMarca             = 2;
  ciNaturezaAnalitico = 3;
  ciQuantidade        = 4;
  ciQuantidadeInformada = 5;
  ciUnidade           = 6;
  ciPrecoUnitario     = 7;
  ciValorTotal        = 8;
  ciNatureza          = 9;

  // Tipos de linhas
  coLinhaDetalhe   = 'D';
  coLinhaCabAgrup  = 'CAGRU';
  coLinhaTotVenda  = 'TV';
  coTotalAgrupador = 'TAGRU';
  coTotalAgrupadorGeral = 'TAGRUG';

procedure TFormRelacaoAjusteEstoque.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vAjustes: TArray<RecAjusteEstoque>;
  vAjustesIds: TArray<Integer>;

  vAgrupadorId: string;
  vLinha: Integer;

  vTotais: record
    total: Double;
  end;

  vTotaisAgrupador: record
    total: Double;
  end;

  procedure TotalizarGrupo;
  begin
    Inc(vLinha);

    sgAjustes.Cells[coEmpresa, vLinha]    := 'TOTAL DO AGRUPADOR: ';
    sgAjustes.Cells[coValorTotal, vLinha] := NFormatN(vTotaisAgrupador.total);
    sgAjustes.Cells[coTipoLinha, vLinha]  := coTotalAgrupador;

    vTotaisAgrupador.total := 0;

    Inc(vLinha);
  end;
begin
  inherited;
  sgAjustes.ClearGrid();
  sgItens.ClearGrid();

  FItens := nil;
  vAjustesIds := nil;

  if not FrCodigoAjuste.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrCodigoAjuste.TrazerFiltros('AJU.AJUSTE_ESTOQUE_ID'))
  else begin
    if not FrEmpresas.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrEmpresas.getSqlFiltros('AJU.EMPRESA_ID'));

    if not FrMotivosAjusteEstoque.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrMotivosAjusteEstoque.getSqlFiltros('AJU.MOTIVO_AJUSTE_ID') );

    if not FrProdutos.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, ' AJU.AJUSTE_ESTOQUE_ID in(select distinct AJUSTE_ESTOQUE_ID from AJUSTES_ESTOQUE_ITENS where ' + FrProdutos.getSqlFiltros('PRODUTO_ID') + ') ');

    if not FrUsuarioInsercao.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrUsuarioInsercao.getSqlFiltros('AJU.USUARIO_AJUSTE_ID') );

    if not FrDataCadastro.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrDataCadastro.getSqlFiltros('trunc(AJU.DATA_HORA_AJUSTE)') );

    if cbTipoEstoqueAjustado.GetValor = 'Fisico' then
      _Biblioteca.WhereOuAnd(vSql, ' AJU.TIPO_ESTOQUE_AJUSTADO = ''FISICO''' )
    else if cbTipoEstoqueAjustado.GetValor = 'Fiscal' then
      _Biblioteca.WhereOuAnd(vSql, ' AJU.TIPO_ESTOQUE_AJUSTADO = ''FISCAL''' );

  end;

  vSql := vSql + ' order by ';
  if cbAgrupamento.GetValor = 'MOT' then
    vSql := vSql + ' AJU.MOTIVO_AJUSTE_ID, AJU.AJUSTE_ESTOQUE_ID '
  else
    vSql := vSql + ' AJU.AJUSTE_ESTOQUE_ID ';

  vAjustes := _AjustesEstoque.BuscarAjustesEstoqueComando(Sessao.getConexaoBanco, vSql);
  if vAjustes = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vAgrupadorId := '';

  vTotais.total          := 0;
  vTotaisAgrupador.total := 0;

  vLinha := 0;
  for i := Low(vAjustes) to High(vAjustes) do begin
    if cbAgrupamento.GetValor = 'MOT' then begin
      if vAgrupadorId <> IntToStr(vAjustes[i].motivo_ajuste_id) then begin
        if i > 0 then begin
          TotalizarGrupo;
        end;
        Inc(vLinha);

        sgAjustes.Cells[coEmpresa, vLinha]    := getInformacao(vAjustes[i].motivo_ajuste_id, vAjustes[i].MotivoAjusteEstoque);
        sgAjustes.Cells[coTipoLinha, vLinha]  := coLinhaCabAgrup;

        vAgrupadorId := IntToStr(vAjustes[i].motivo_ajuste_id);
      end;
    end;

    Inc(vLinha);

    sgAjustes.Cells[coAjusteEstoqueId, vLinha]     := NFormatN( vAjustes[i].ajuste_estoque_id );
    sgAjustes.Cells[coEmpresa, vLinha]             := NFormatN( vAjustes[i].EmpresaId ) + ' - ' + vAjustes[i].NomeFantasia;
    sgAjustes.Cells[coTipoAnalitico, vLinha]       := vAjustes[i].TipoAnalitico;
    sgAjustes.Cells[coTipo, vLinha]                := vAjustes[i].Tipo;
    sgAjustes.Cells[coValorTotal, vLinha]          := NFormatN( vAjustes[i].ValorTotal );
    sgAjustes.Cells[coDataHoraAjuste, vLinha]      := DHFormat( vAjustes[i].data_hora_ajuste );
    sgAjustes.Cells[coUsuarioAjuste, vLinha]       := NFormatN( vAjustes[i].usuario_ajuste_id ) + ' - ' + vAjustes[i].NomeUsuarioAjuste;
    sgAjustes.Cells[coMotivoAjuste, vLinha]        := NFormatN( vAjustes[i].motivo_ajuste_id ) + ' - ' + vAjustes[i].MotivoAjusteEstoque;
    sgAjustes.Cells[coTipoEstoqueAjustado, vLinha] := vAjustes[i].TipoEstoqueAjustado;
    sgAjustes.Cells[coObservacao, vLinha]          := vAjustes[i].observacao;

    sgAjustes.Cells[coTipoLinha, vLinha]           := coLinhaDetalhe;

    vTotais.total    := vTotais.total + _Biblioteca.SFormatDouble(sgAjustes.Cells[coValorTotal, vLinha]);
    vTotaisAgrupador.total      := vTotaisAgrupador.total + _Biblioteca.SFormatDouble(sgAjustes.Cells[coValorTotal, vLinha]);

    _Biblioteca.AddNoVetorSemRepetir(vAjustesIds, vAjustes[i].ajuste_estoque_id);
  end;

  if cbAgrupamento.GetValor <> 'NEN' then begin
    TotalizarGrupo;
    Inc(vLinha);
  end
  else
    Inc(vLinha);

  sgAjustes.Cells[coEmpresa, vLinha]    := 'TOTAL GERAL: ';
  sgAjustes.Cells[coValorTotal, vLinha] := NFormatN(vTotais.total);
  sgAjustes.Cells[coTipoLinha, vLinha]  := coTotalAgrupadorGeral;

  Inc(vLinha);

  sgAjustes.SetLinhasGridPorTamanhoVetor(vLinha);

  FItens := _AjustesEstoqueItens.BuscarAjusteEstoqueItensComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('ITE.AJUSTE_ESTOQUE_ID', vAjustesIds));

  sgAjustesClick(nil);
  SetarFoco(sgAjustes);
end;

procedure TFormRelacaoAjusteEstoque.FormCreate(Sender: TObject);
begin
  inherited;
  cbTipoEstoqueAjustado.Visible := Sessao.getParametros.AjusteEstoqueReal = 'S';
  Label2.Visible :=  Sessao.getParametros.AjusteEstoqueReal = 'S';
  if Sessao.getParametros.AjusteEstoqueReal = 'N' then
    sgAjustes.OcultarColunas([coTipoEstoqueAjustado]);
end;

procedure TFormRelacaoAjusteEstoque.FormShow(Sender: TObject);
begin
  inherited;
  FrEmpresas.InserirDadoPorChave( Sessao.getEmpresaLogada.EmpresaId, False );
  SetarFoco( FrEmpresas );
end;

procedure TFormRelacaoAjusteEstoque.Imprimir(Sender: TObject);
begin
  inherited;

end;

procedure TFormRelacaoAjusteEstoque.sgAjustesClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vAjusteId: Integer;
  vTotalEntradas: Double;
  vTotalSaidas: Double;
begin
  inherited;
  vLinha := 0;
  vTotalEntradas := 0;
  vTotalSaidas := 0;
  sgItens.ClearGrid();
  vAjusteId := SFormatInt(sgAjustes.Cells[coAjusteEstoqueId, sgAjustes.Row]);
  for i := Low(FItens) to High(FItens) do begin
    if vAjusteId <> FItens[i].ajuste_estoque_id then
      Continue;

    Inc(vLinha);

    sgItens.Cells[ciProdutoId, vLinha]     := NFormat(FItens[i].produto_id);
    sgItens.Cells[ciNome, vLinha]          := FItens[i].NomeProduto;
    sgItens.Cells[ciMarca, vLinha]         := NFormat(FItens[i].MarcaId) + ' - ' + FItens[i].NomeMarca;
    sgItens.Cells[ciNaturezaAnalitico, vLinha] := FItens[i].NaturezaAnalitico;
    sgItens.Cells[ciNatureza, vLinha]      := FItens[i].Natureza;
    sgItens.Cells[ciUnidade, vLinha]       := FItens[i].Unidade;
    sgItens.Cells[ciQuantidade, vLinha]    := NFormatEstoque(FItens[i].Quantidade);
    sgItens.Cells[ciQuantidadeInformada, vLinha]    := NFormatEstoque(FItens[i].QuantidadeInformado);
    sgItens.Cells[ciPrecoUnitario, vLinha] := NFormat(FItens[i].PrecoUnitario);
    sgItens.Cells[ciValorTotal, vLinha]    := NFormat(FItens[i].ValorTotal);

    if FItens[i].Natureza = 'E' then
      vTotalEntradas := vTotalEntradas + FItens[i].ValorTotal
    else
      vTotalSaidas := vTotalSaidas + FItens[i].ValorTotal;

  end;
  sgItens.SetLinhasGridPorTamanhoVetor( vLinha );
  stTotalSaidas.Caption := NFormat(vTotalSaidas);
  stTotalEntradas.Caption := NFormat(vTotalEntradas);
end;

procedure TFormRelacaoAjusteEstoque.sgAjustesDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.AjustaEstoque.Informar( SFormatInt(sgAjustes.Cells[coAjusteEstoqueId, sgAjustes.Row]) );
end;

procedure TFormRelacaoAjusteEstoque.sgAjustesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in [coAjusteEstoqueId, coValorTotal] then
    vAlinhamento := taRightJustify
  else if ACol in [coTipoAnalitico, coTipoEstoqueAjustado] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgAjustes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoAjusteEstoque.sgAjustesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coTipoAnalitico then begin
    AFont.Style := [fsBold];
    AFont.Color := _AjustesEstoque.getCorTipoAjuste(sgAjustes.Cells[coTipo, ARow]);
  end
  else if ACol = coTipoEstoqueAjustado then begin
    AFont.Style := [fsBold];
    AFont.Color := _AjustesEstoque.getCorTipoEstoqueAjustado(sgAjustes.Cells[coTipoEstoqueAjustado, ARow]);
  end;

  if sgAjustes.Cells[coTipoLinha, ARow] = coLinhaTotVenda then begin
    AFont.Color := clWhite;
    AFont.Style := [fsBold];
    ABrush.Color := $000096DB;
  end
  else if sgAjustes.Cells[coTipoLinha, ARow] = coLinhaCabAgrup then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end
  else if sgAjustes.Cells[coTipoLinha, ARow] = coTotalAgrupador then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao3;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao3;
  end
  else if sgAjustes.Cells[coTipoLinha, ARow] = coTotalAgrupadorGeral then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao4;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao4;
  end;
end;

procedure TFormRelacaoAjusteEstoque.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[ciNaturezaAnalitico] then
    vAlinhamento := taCenter
  else if ACol in[
    ciProdutoId,
    ciQuantidade,
    ciQuantidadeInformada,
    ciPrecoUnitario,
    ciValorTotal]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoAjusteEstoque.sgItensGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = ciNaturezaAnalitico then begin
    AFont.Style := [fsBold];
    if sgItens.Cells[ciNatureza, ARow] = 'E' then
      AFont.Color := clBlue
    else if sgItens.Cells[ciNatureza, ARow] = 'S' then
      AFont.Color := clGreen
    else
      AFont.Color := $000080FF;
  end;
end;

procedure TFormRelacaoAjusteEstoque.SpeedButton1Click(Sender: TObject);
var
  ajusteEstoqueId: Integer;
  vRetornoBanco: RecRetornoBD;
  i: Integer;
  vAjusteId: Integer;
  vExisteEntrada: Boolean;
  vExisteSaida: Boolean;
begin
  inherited;
  if not Sessao.AutorizadoRotina('emitir_nota_ajuste_estoque') then begin
    _Biblioteca.NaoAutorizadoRotina;
    Exit;
  end;

  if sgAjustes.Cells[coAjusteEstoqueId, sgAjustes.Row] = '' then
    Exit;

  vAjusteId := SFormatInt(sgAjustes.Cells[coAjusteEstoqueId, sgAjustes.Row]);

  vExisteEntrada := false;
  vExisteSaida := false;
  for i := 0 to sgItens.RowCount - 1 do begin
    if sgItens.cells[coTipo, i] = 'S' then
      vExisteSaida := true;

    if sgItens.cells[coTipo, i] = 'E' then
      vExisteEntrada := true;
  end;

   vRetornoBanco :=
      _AjustesEstoque.AtualizarAjusteEstoque(
        Sessao.getConexaoBanco,
        vAjusteId,
        vExisteEntrada,
        vExisteSaida
      );

    if vRetornoBanco.TeveErro then begin
      Exclamar(vRetornoBanco.MensagemErro);
      Abort;
    end;

    if _Biblioteca.Perguntar('Deseja emitir as notas fiscais geradas?') then begin
      for i := Low(vRetornoBanco.AsArrayInt) to High(vRetornoBanco.AsArrayInt) do begin
        if not _ComunicacaoNFE.EnviarNFe(vRetornoBanco.AsArrayInt[i]) then begin
          Exclamar(
            'Ocorreu um problema ao tentar emitir a nota fiscal eletr�nica automaticamente!' + Chr(13) + Chr(10) +
            'Fa�a a emiss�o pela tela "Rela��o de notas fiscais"'
          );

          Break;
        end;
      end;
    end;
end;

procedure TFormRelacaoAjusteEstoque.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
