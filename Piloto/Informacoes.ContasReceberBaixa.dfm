﻿inherited FormInformacoesContasReceberBaixa: TFormInformacoesContasReceberBaixa
  Left = 20
  Top = 20
  Caption = 'Informac'#245'es da baixa de contas a receber'
  ClientHeight = 368
  ClientWidth = 720
  OnShow = FormShow
  ExplicitWidth = 726
  ExplicitHeight = 397
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 331
    Width = 720
    ExplicitTop = 331
    ExplicitWidth = 720
  end
  object pcDados: TPageControl
    Left = 0
    Top = 0
    Width = 720
    Height = 331
    ActivePage = tsGerais
    Align = alClient
    TabOrder = 1
    object tsGerais: TTabSheet
      Caption = 'Gerais'
      object lb7: TLabel
        Left = 1
        Top = 3
        Width = 30
        Height = 14
        Caption = 'Baixa'
      end
      object lb1: TLabel
        Left = 465
        Top = 3
        Width = 76
        Height = 14
        Caption = 'Usu'#225'rio baixa'
      end
      object lb2: TLabel
        Left = 263
        Top = 3
        Width = 91
        Height = 14
        Caption = 'Data pagamento'
      end
      object lb3: TLabel
        Left = 364
        Top = 3
        Width = 59
        Height = 14
        Caption = 'Data baixa'
      end
      object lb11: TLabel
        Left = 1
        Top = 141
        Width = 51
        Height = 14
        AutoSize = False
        Caption = 'Dinheiro'
      end
      object lb12: TLabel
        Left = 95
        Top = 141
        Width = 60
        Height = 14
        AutoSize = False
        Caption = 'Cheque'
      end
      object lb13: TLabel
        Left = 189
        Top = 141
        Width = 60
        Height = 14
        AutoSize = False
        Caption = 'Cart'#227'o'
      end
      object lb14: TLabel
        Left = 283
        Top = 141
        Width = 60
        Height = 14
        AutoSize = False
        Caption = 'Cobran'#231'a'
      end
      object lb5: TLabel
        Left = 377
        Top = 141
        Width = 88
        Height = 14
        AutoSize = False
        Caption = 'Cr'#233'ditos a pagar'
      end
      object lb4: TLabel
        Left = 1
        Top = 182
        Width = 69
        Height = 14
        Caption = 'Observa'#231#245'es'
      end
      object lb8: TLabel
        Left = 59
        Top = 3
        Width = 97
        Height = 14
        Caption = 'Empresa da baixa'
      end
      object lb10: TLabel
        Left = 284
        Top = 84
        Width = 114
        Height = 14
        Caption = 'Usu'#225'rio enviou caixa'
        Visible = False
      end
      object lb15: TLabel
        Left = 1
        Top = 43
        Width = 68
        Height = 14
        AutoSize = False
        Caption = 'Valor t'#237'tulos'
      end
      object lb16: TLabel
        Left = 178
        Top = 43
        Width = 44
        Height = 14
        AutoSize = False
        Caption = 'Juros'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object lb17: TLabel
        Left = 258
        Top = 43
        Width = 60
        Height = 14
        AutoSize = False
        Caption = 'Desconto'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object lb18: TLabel
        Left = 541
        Top = 43
        Width = 79
        Height = 12
        AutoSize = False
        Caption = 'Valor l'#237'quido'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lb6: TLabel
        Left = 649
        Top = 43
        Width = 30
        Height = 14
        Caption = 'Turno'
      end
      object sbInfoOrcamentoId: TSpeedButton
        Left = 695
        Top = 61
        Width = 15
        Height = 18
        Hint = 'Abrir informa'#231#245'es do or'#231'amento'
        Caption = '...'
        OnClick = sbInfoOrcamentoIdClick
      end
      object sbCreditos: TSpeedButton
        Left = 467
        Top = 159
        Width = 14
        Height = 18
        Hint = 'Abrir informa'#231#245'es dos cr'#233'ditos utilizados'
        Caption = '...'
        OnClick = sbCreditosClick
      end
      object lb9: TLabel
        Left = 592
        Top = 141
        Width = 105
        Height = 14
        AutoSize = False
        Caption = 'Baixa pagar origem'
      end
      object sbInformacoesBaixa: TSpeedButton
        Left = 697
        Top = 159
        Width = 14
        Height = 18
        Hint = 'Abrir informa'#231#245'es da baixa a receber que originou esta baixa'
        Caption = '...'
        OnClick = sbInformacoesBaixaClick
      end
      object lb19: TLabel
        Left = 433
        Top = 43
        Width = 86
        Height = 14
        AutoSize = False
        Caption = 'Valor reten'#231#227'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object lb20: TLabel
        Left = 109
        Top = 43
        Width = 36
        Height = 14
        AutoSize = False
        Caption = 'Multa'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object lb21: TLabel
        Left = 335
        Top = 43
        Width = 73
        Height = 14
        AutoSize = False
        Caption = 'Valor adiant.'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 482
        Top = 141
        Width = 51
        Height = 14
        AutoSize = False
        Caption = 'Pix'
      end
      object SpeedButton1: TSpeedButton
        Left = 572
        Top = 159
        Width = 14
        Height = 18
        Hint = 'Abrir informa'#231#245'es dos cr'#233'ditos utilizados'
        Caption = '...'
        Visible = False
        OnClick = sbCreditosClick
      end
      object eBaixaId: TEditLuka
        Left = 1
        Top = 17
        Width = 54
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 0
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eUsuarioBaixa: TEditLuka
        Left = 465
        Top = 17
        Width = 244
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 4
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eDataPagamento: TEditLukaData
        Left = 263
        Top = 17
        Width = 97
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        TabOrder = 2
        Text = '  /  /    '
      end
      object eDataBaixa: TEditLukaData
        Left = 364
        Top = 17
        Width = 97
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        TabOrder = 3
        Text = '  /  /    '
      end
      object eValorDinheiro: TEditLuka
        Left = 1
        Top = 155
        Width = 90
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 13
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorCheque: TEditLuka
        Left = 95
        Top = 155
        Width = 90
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 14
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorCartao: TEditLuka
        Left = 189
        Top = 155
        Width = 90
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 15
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorCobranca: TEditLuka
        Left = 283
        Top = 155
        Width = 90
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 16
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object stValores: TStaticText
        Left = -1
        Top = 126
        Width = 710
        Height = 15
        Alignment = taCenter
        AutoSize = False
        Caption = 'Valores recebidos na baixa'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 19
        Transparent = False
      end
      object eValorCredito: TEditLuka
        Left = 377
        Top = 155
        Width = 90
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 17
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eObservacoes: TMemo
        Left = 1
        Top = 196
        Width = 708
        Height = 106
        TabOrder = 20
      end
      object eEmpresaBaixa: TEditLuka
        Left = 59
        Top = 17
        Width = 200
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 1
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eUsuarioEnviouCaixa: TEditLuka
        Left = 284
        Top = 98
        Width = 235
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 12
        Visible = False
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eValorBruto: TEditLuka
        Left = 1
        Top = 57
        Width = 104
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = -1
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorJuros: TEditLuka
        Left = 178
        Top = 57
        Width = 76
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 7
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorDesconto: TEditLuka
        Left = 258
        Top = 57
        Width = 73
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 8
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorLíquido: TEditLuka
        Left = 543
        Top = 57
        Width = 104
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 10
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eTurnoId: TEditLuka
        Left = 649
        Top = 57
        Width = 45
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 11
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object st3: TStaticText
        Left = 185
        Top = 87
        Width = 93
        Height = 18
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Receber caixa'
        Color = 15395562
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 21
        Transparent = False
        Visible = False
      end
      object stReceberCaixa: TStaticText
        Left = 185
        Top = 104
        Width = 93
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Sim'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 22
        Transparent = False
        Visible = False
      end
      object st1: TStaticTextLuka
        Left = 1
        Top = 87
        Width = 178
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Tipo de movimento'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 23
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object stTipo: TStaticText
        Left = 1
        Top = 103
        Width = 178
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Retira no ato'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = 33023
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 24
        Transparent = False
      end
      object eBaixaPagarOrigemId: TEditLuka
        Left = 592
        Top = 155
        Width = 105
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 18
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eValorRetencao: TEditLuka
        Left = 433
        Top = 57
        Width = 104
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 9
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorMulta: TEditLuka
        Left = 109
        Top = 57
        Width = 65
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 6
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorAdiantado: TEditLuka
        Left = 335
        Top = 57
        Width = 93
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 25
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorPix: TEditLuka
        Left = 482
        Top = 155
        Width = 90
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 26
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
    end
    object tsContasRecebimentoDinheiro: TTabSheet
      Caption = 'Contas recebimento dinheiro'
      ImageIndex = 3
      object sgContasDinheiro: TGridLuka
        Left = 0
        Top = 0
        Width = 712
        Height = 302
        Align = alClient
        ColCount = 3
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        TabOrder = 0
        OnDrawCell = sgContasDinheiroDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Conta'
          'Descricao'
          'Valor')
        Grid3D = False
        RealColCount = 10
        AtivarPopUpSelecao = False
        ColWidths = (
          54
          213
          161)
      end
    end
    object tsTitulosBaixados: TTabSheet
      Caption = 'T'#237'tulos baixados'
      ImageIndex = 1
      object sgTitulosBaixados: TGridLuka
        Left = 0
        Top = 0
        Width = 712
        Height = 302
        Align = alClient
        ColCount = 6
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        TabOrder = 0
        OnDblClick = sgTitulosBaixadosDblClick
        OnDrawCell = sgTitulosBaixadosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'C'#243'digo'
          'Cliente'
          'Documento'
          'Data vencto'
          'Valor'
          'Tipo de cobran'#231'a')
        Grid3D = False
        RealColCount = 10
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          54
          224
          93
          73
          101
          175)
      end
    end
    object tsTitulosGerados: TTabSheet
      Caption = 'T'#237'tulos gerados'
      ImageIndex = 2
      object sgTitulosGerados: TGridLuka
        Left = 0
        Top = 0
        Width = 712
        Height = 302
        Align = alClient
        ColCount = 6
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        TabOrder = 0
        OnDblClick = sgTitulosGeradosDblClick
        OnDrawCell = sgTitulosGeradosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'C'#243'digo'
          'Cliente'
          'Documento'
          'Data vencto'
          'Valor'
          'Tipo cobran'#231'a'
          '')
        Grid3D = False
        RealColCount = 10
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          54
          213
          93
          73
          80
          159)
      end
    end
  end
end
