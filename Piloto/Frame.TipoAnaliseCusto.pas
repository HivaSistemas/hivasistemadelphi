unit Frame.TipoAnaliseCusto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.StdCtrls,
  Vcl.ExtCtrls, RadioGroupLuka;

type
  TFrTipoAnaliseCusto = class(TFrameHerancaPrincipal)
    rgTiposCusto: TRadioGroupLuka;
  private
    { Private declarations }
  public
    procedure RemoverCustoA();
    function getSQL(pColuna: string = 'TIPO_ANALISE_CUSTO'): string;
  end;

implementation

{$R *.dfm}

{ TFrTipoAnaliseCusto }

function TFrTipoAnaliseCusto.getSQL(pColuna: string): string;
begin
  Result := pColuna + ' in(' + rgTiposCusto.GetValor + ') ';
end;

procedure TFrTipoAnaliseCusto.RemoverCustoA;
begin
  rgTiposCusto.Items.Clear;
  rgTiposCusto.Items.Add('B');
  rgTiposCusto.Items.Add('C');
  rgTiposCusto.Items.Add('B + C');

  rgTiposCusto.Valores.Clear;
  rgTiposCusto.Valores.Add('''B''');
  rgTiposCusto.Valores.Add('''C''');
  rgTiposCusto.Valores.Add('''B'', ''C''');
end;

end.
