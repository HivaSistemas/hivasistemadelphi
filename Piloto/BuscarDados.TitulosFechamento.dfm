inherited FormBuscarDadosTitulosFechamento: TFormBuscarDadosTitulosFechamento
  Caption = 'Busca de t'#237'tulos fechamento'
  ClientHeight = 364
  ClientWidth = 600
  ExplicitWidth = 606
  ExplicitHeight = 393
  PixelsPerInch = 96
  TextHeight = 14
  object sgCartoes: TGridLuka [0]
    Left = 8
    Top = 71
    Width = 600
    Height = 250
    Align = alCustom
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goRowSelect]
    TabOrder = 1
    Visible = False
    OnDrawCell = sgCartoesDrawCell
    OnKeyDown = GridKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Sel?'
      'Num. trans.'
      'Cliente'
      'Valor'
      'Cobran'#231'a')
    OnGetCellColor = GridGetCellColor
    Grid3D = False
    RealColCount = 5
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      32
      101
      242
      64
      129)
  end
  object sgCobrancas: TGridLuka [1]
    Left = 160
    Top = 21
    Width = 600
    Height = 250
    Align = alCustom
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect]
    TabOrder = 2
    Visible = False
    OnDrawCell = sgCobrancasDrawCell
    OnKeyDown = GridKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Sel?'
      'Documento'
      'Cliente'
      'Valor'
      'Vencimento'
      'Cobran'#231'a')
    OnGetCellColor = GridGetCellColor
    Grid3D = False
    RealColCount = 5
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      32
      83
      262
      112
      75)
  end
  object sgCheques: TGridLuka [2]
    Left = 80
    Top = 61
    Width = 600
    Height = 250
    Align = alCustom
    ColCount = 10
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect]
    TabOrder = 3
    Visible = False
    OnDrawCell = sgChequesDrawCell
    OnKeyDown = GridKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Sel?'
      'Num. cheque'
      'Cliente'
      'Valor'
      'Banco'
      'Ag'#234'ncia'
      'Conta'
      'Vencimento'
      'Emitente'
      'Telefone emit.')
    OnGetCellColor = GridGetCellColor
    Grid3D = False
    RealColCount = 10
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      32
      80
      175
      64
      54
      64
      64
      77
      152
      107)
  end
  inherited pnOpcoes: TPanel
    Top = 327
    Width = 600
    ExplicitTop = 327
    ExplicitWidth = 600
  end
  object stTipoTitulos: TStaticTextLuka
    Left = 0
    Top = 0
    Width = 600
    Height = 17
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Cart'#245'es'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 4
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
end
