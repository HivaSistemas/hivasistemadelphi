unit FrameListaNegra;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Menus, _Sessao, _Biblioteca,
  Vcl.Buttons, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _ListaNegra, PesquisaListaNegra;

type
  TFrListaNegra = class(TFrameHenrancaPesquisas)
  public
    function getDados(pLinha: Integer = -1): RecListaNegra;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrListaNegra }

function TFrListaNegra.AdicionarDireto: TObject;
var
  vDados: TArray<RecListaNegra>;
begin
  vDados := _ListaNegra.BuscarListaNegra(Sessao.getConexaoBanco, 0, [FChaveDigitada], ckSomenteAtivos.Checked);
  if vDados = nil then
    Result := nil
  else
    Result := vDados[0];
end;

function TFrListaNegra.AdicionarPesquisando: TObject;
begin
  Result := PesquisaListaNegra.Pesquisar(ckSomenteAtivos.Checked);
end;

function TFrListaNegra.getDados(pLinha: Integer): RecListaNegra;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecListaNegra(FDados[IIfInt(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrListaNegra.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecListaNegra(FDados[i]).ListaId = RecListaNegra(pSender).ListaId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrListaNegra.MontarGrid;
var
  i: Integer;
  vSender: RecListaNegra;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecListaNegra(FDados[i]);
      AAdd([IntToStr(vSender.ListaId), vSender.nome]);
    end;
  end;
end;

end.
