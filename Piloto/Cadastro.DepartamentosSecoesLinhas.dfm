inherited FormCadastroDepartamentosSecoesLinhas: TFormCadastroDepartamentosSecoesLinhas
  Caption = 'Cadastro grupo de produtos'
  ClientHeight = 206
  ClientWidth = 451
  ExplicitWidth = 457
  ExplicitHeight = 235
  PixelsPerInch = 96
  TextHeight = 14
  object lbDepartamento: TLabel [0]
    Left = 126
    Top = 53
    Width = 36
    Height = 14
    Caption = 'N'#237'vel 1'
  end
  object lb1: TLabel [1]
    Left = 126
    Top = 91
    Width = 36
    Height = 14
    Caption = 'N'#237'vel 2'
  end
  object lb2: TLabel [2]
    Left = 126
    Top = 135
    Width = 36
    Height = 14
    Caption = 'N'#237'vel 3'
  end
  inherited Label1: TLabel
    Top = 33
    ExplicitTop = 33
  end
  inherited pnOpcoes: TPanel
    Height = 206
    ExplicitHeight = 190
  end
  inherited eID: TEditLuka
    Top = 47
    Alignment = taLeftJustify
    TabOrder = 4
    OnKeyPress = NumerosPonto
    TipoCampo = tcTexto
    ExplicitTop = 47
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 126
    Top = 8
    TabOrder = 5
    ExplicitLeft = 126
    ExplicitTop = 8
  end
  object eDepartamento: TEditLuka
    Left = 126
    Top = 88
    Width = 317
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 3
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eSecao: TEditLuka
    Left = 126
    Top = 130
    Width = 317
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 1
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eLinha: TEditLuka
    Left = 126
    Top = 174
    Width = 317
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 2
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
