unit BuscarGruposTributacoesEmpresas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Biblioteca,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, _FrameHenrancaPesquisas,
  FrameGruposTributacoesEstadualCompras, FrameGruposTributacoesEstadualVenda,
  FrameGruposTributacoesFederalCompra, FrameGruposTributacoesFederalVenda;

type
  RecGruposTributacoesEmpresas = record
    GrupoTribEstadualCompraId: Integer;
    NomeGrupoTribEstadualCompra: string;

    GrupoTribFederalCompraId: Integer;
    NomeGrupoTribFederalCompra: string;

    GrupoTribEstadualVendaId: Integer;
    NomeGrupoTribEstadualVenda: string;

    GrupoTribFederalVendaId: Integer;
    NomeGrupoTribFederalVenda: string;
  end;

  TFormBuscarGruposTributacoesEmpresas = class(TFormHerancaFinalizar)
    FrGrupoTribEstadualCompra: TFrGruposTributacoesEstadualCompra;
    FrGrupoTributacoesEstadualVenda: TFrGruposTributacoesEstadualVenda;
    FrGrupoTributacoesFederalCompra: TFrGruposTributacoesFederalCompra;
    FrGrupoTributacoesFederalVenda: TFrGruposTributacoesFederalVenda;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function BuscarGrupos(pGrupos: RecGruposTributacoesEmpresas): TRetornoTelaFinalizar<RecGruposTributacoesEmpresas>;

implementation

{$R *.dfm}

function BuscarGrupos(pGrupos: RecGruposTributacoesEmpresas): TRetornoTelaFinalizar<RecGruposTributacoesEmpresas>;
var
  vForm: TFormBuscarGruposTributacoesEmpresas;
begin
  vForm := TFormBuscarGruposTributacoesEmpresas.Create(nil);

  vForm.FrGrupoTribEstadualCompra.InserirDadoPorChave( pGrupos.GrupoTribEstadualCompraId, False );
  vForm.FrGrupoTributacoesEstadualVenda.InserirDadoPorChave( pGrupos.GrupoTribEstadualVendaId, False );
  vForm.FrGrupoTributacoesFederalCompra.InserirDadoPorChave( pGrupos.GrupoTribFederalCompraId, False );
  vForm.FrGrupoTributacoesFederalVenda.InserirDadoPorChave( pGrupos.GrupoTribFederalVendaId, False );

  SetarFoco(vForm.FrGrupoTribEstadualCompra);

  if Result.Ok(vForm.ShowModal, True) then begin
    Result.Dados.GrupoTribEstadualCompraId   := vForm.FrGrupoTribEstadualCompra.getGrupo().GrupoTribEstadualCompraId;
    Result.Dados.NomeGrupoTribEstadualCompra := vForm.FrGrupoTribEstadualCompra.getGrupo().Descricao;

    Result.Dados.GrupoTribEstadualVendaId    := vForm.FrGrupoTributacoesEstadualVenda.getGrupo().GrupoTribEstadualVendaId;
    Result.Dados.NomeGrupoTribEstadualVenda  := vForm.FrGrupoTributacoesEstadualVenda.getGrupo().Descricao;

    Result.Dados.GrupoTribFederalCompraId    := vForm.FrGrupoTributacoesFederalCompra.getGrupo().GrupoTribFederalCompraId;
    Result.Dados.NomeGrupoTribFederalCompra  := vForm.FrGrupoTributacoesFederalCompra.getGrupo().Descricao;

    Result.Dados.GrupoTribFederalVendaId     := vForm.FrGrupoTributacoesFederalVenda.getGrupo().GrupoTribFederalVendaId;
    Result.Dados.NomeGrupoTribFederalVenda   := vForm.FrGrupoTributacoesFederalVenda.getGrupo().Descricao;
  end;
end;

{ TFormBuscarGruposTributacoesEmpresas }

procedure TFormBuscarGruposTributacoesEmpresas.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if FrGrupoTribEstadualCompra.EstaVazio then begin
    _Biblioteca.Exclamar('O grupo de tributa��o estadual de compra n�o foi definido, verifique!');
    SetarFoco(FrGrupoTribEstadualCompra);
    Abort;
  end;

  if FrGrupoTributacoesEstadualVenda.EstaVazio then begin
    _Biblioteca.Exclamar('O grupo de tributa��o estadual de venda n�o foi definido, verifique!');
    SetarFoco(FrGrupoTributacoesEstadualVenda);
    Abort;
  end;

  if FrGrupoTributacoesFederalCompra.EstaVazio then begin
    _Biblioteca.Exclamar('O grupo de tributa��o federal de compra n�o foi definido, verifique!');
    SetarFoco(FrGrupoTributacoesFederalCompra);
    Abort;
  end;

  if FrGrupoTributacoesFederalVenda.EstaVazio then begin
    _Biblioteca.Exclamar('O grupo de tributa��o federal de venda n�o foi definido, verifique!');
    SetarFoco(FrGrupoTributacoesFederalVenda);
    Abort;
  end;
end;

end.
