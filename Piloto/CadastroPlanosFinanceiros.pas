unit CadastroPlanosFinanceiros;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls,
  CheckBoxLuka, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, RadioGroupLuka, _PlanosFinanceiros;

type
  TFormCadastroPlanosFinanceiros = class(TFormHerancaCadastroCodigo)
    rgTipo: TRadioGroupLuka;
    ckExibirDRE: TCheckBoxLuka;
    lbDepartamento: TLabel;
    eDescricao: TEditLuka;
    procedure eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); override;
  private
    procedure PreencherRegistro(pRegistro: RecPlanosFinanceiros);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

uses _Sessao, _Biblioteca, _RecordsEspeciais, PesquisaPlanosFinanceiros, System.Types, System.StrUtils;

{ TFormCadastroPlanosFinanceiros }

procedure TFormCadastroPlanosFinanceiros.eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  i: Integer;
  vMascara: TStringDynArray;
begin
  if Key <> VK_RETURN then
    Exit;

  eID.Trim;
  if eID.Text = '' then begin
    Modo(True);
    Exit;
  end;

  vMascara := SplitString(eID.Text, '.');
  if Length(vMascara) > 3 then begin
    Exclamar('A m�scara informada est� incorreta!');
    SetarFoco(eID);
    Abort;
  end;

  eID.Text := '';
  for i := Low(vMascara) to High(vMascara) do begin
    if
      (i <> High(vMascara)) and
      (
        (vMascara[i] = '') or
        (vMascara[i] <> RetornaNumeros(vMascara[i]))
      )
    then begin
      Exclamar('A m�scara informada est� incorreta!');
      SetarFoco(eID);
      Abort;
    end;

    if vMascara[i] = '' then
      eID.Text := eID.Text + '.???'
    else
      eID.Text := eID.Text + IfThen(i in[1,2], '.') + IIfStr(i = 0, vMascara[i], LPad(vMascara[i], 3, '0'));
  end;

  inherited;
end;

procedure TFormCadastroPlanosFinanceiros.BuscarRegistro;
var
  vId: string;
  vNovo: Boolean;
  vPlanosFinanceiros: TArray<RecPlanosFinanceiros>;
begin
  vNovo := False;

  if Pos('???', eID.Text) > 0 then begin
    vPlanosFinanceiros := _PlanosFinanceiros.BuscarPlanosFinanceiros(Sessao.getConexaoBanco, 0, [Copy(eID.Text, 1, Pos('???', eID.Text) - 2)], False);
    vNovo := True;
  end
  else
    vPlanosFinanceiros := _PlanosFinanceiros.BuscarPlanosFinanceiros(Sessao.getConexaoBanco, 0, [eID.Text], False);

  if vPlanosFinanceiros = nil then begin
    NenhumRegistro;
    SetarFoco(eID);
    Abort;
  end;

  if vNovo then begin
    vPlanosFinanceiros[0].Descricao := '';
    vPlanosFinanceiros[0].PermiteAlteracao := 'S';
  end;

  vId := eID.Text;
  inherited;

  PreencherRegistro(vPlanosFinanceiros[0]);
  eID.Text := vId;
end;

procedure TFormCadastroPlanosFinanceiros.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _PlanosFinanceiros.ExcluirPlanoFinanceiro(Sessao.getConexaoBanco, eID.Text);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroPlanosFinanceiros.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vRetBanco :=
    _PlanosFinanceiros.AtualizarPlanosFinanceiros(
      Sessao.getConexaoBanco,
      eID.Text,
      eDescricao.Text,
      Copy(eID.Text, 1, Length(eID.Text) - 4),
      rgTipo.GetValor,
      ToChar(ckExibirDRE)
    );

  if vRetBanco.TeveErro then begin
    Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  if vRetBanco.AsString <> '' then
    Informar(coNovoRegistroSucessoCodigo + vRetBanco.AsString)
  else
    _Biblioteca.Informar(coAlteracaoRegistroSucesso);
end;

procedure TFormCadastroPlanosFinanceiros.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    eDescricao,
    rgTipo,
    ckExibirDRE],
    pEditando
  );

  if pEditando then begin
    ckExibirDRE.Checked := True;
    rgTipo.ItemIndex := 0;
    SetarFoco(eDescricao);
  end;
end;

procedure TFormCadastroPlanosFinanceiros.PesquisarRegistro;
var
  vPlanoFinanceiro: RecPlanosFinanceiros;
begin
  vPlanoFinanceiro := RecPlanosFinanceiros(PesquisaPlanosFinanceiros.Pesquisar(False, False));
  if vPlanoFinanceiro = nil then
    Exit;

  inherited;
  PreencherRegistro(vPlanoFinanceiro);
end;

procedure TFormCadastroPlanosFinanceiros.PreencherRegistro(pRegistro: RecPlanosFinanceiros);
begin
  eID.Text               := pRegistro.PlanoFinanceiroId;
  eDescricao.Text        := pRegistro.Descricao;
  ckExibirDRE.CheckedStr := pRegistro.ExibirDre;
  rgTipo.SetIndicePorValor(pRegistro.Tipo);

  if pRegistro.PermiteAlteracao = 'N' then begin
    eDescricao.Enabled := False;
    rgTipo.Enabled := False;
    ckExibirDRE.Enabled := False;
  end;

  pRegistro.Free;
end;

procedure TFormCadastroPlanosFinanceiros.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eDescricao.Trim = '' then begin
    Exclamar('� necess�rio informar a descri��o!');
    SetarFoco(eDescricao);
    Abort;
  end;
end;

end.
