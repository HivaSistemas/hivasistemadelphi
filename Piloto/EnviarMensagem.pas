unit EnviarMensagem;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaBarra, Vcl.ExtCtrls, _Biblioteca, _Sessao,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameFuncionarios, _Funcionarios,
  FrameEmpresas, Vcl.StdCtrls, EditLuka, Vcl.Buttons, SpeedButtonLuka, _Mensagens, _RecordsEspeciais,
  FrameCagosFuncionarios;

type
  TFormEnviarMensagem = class(TFormHerancaBarra)
    cpOpcoes: TCategoryPanelGroup;
    cpCargos: TCategoryPanel;
    cpEmpresas: TCategoryPanel;
    cpUsuarios: TCategoryPanel;
    FrFuncionarios: TFrFuncionarios;
    FrEmpresas: TFrEmpresas;
    pn1: TPanel;
    eAssunto: TEditLuka;
    eMensagem: TMemo;
    lb1: TLabel;
    lb2: TLabel;
    sbEnviarMensagem: TSpeedButtonLuka;
    ckPermitirResposta: TCheckBox;
    ckImportante: TCheckBox;
    FrCargosFuncionarios: TFrCargosFuncionarios;
    procedure sbEnviarMensagemClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FUsuarioDestinoId: Integer;
    FMensagemResponderId: Integer;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

procedure Enviar;
procedure Responder(pMensagemId: Integer; pAssunto: string; pUsuarioDestinoId: Integer);
procedure Encaminhar(pAssunto: string; pMensagem: string);

implementation

{$R *.dfm}

procedure Enviar;
var
  vForm: TFormEnviarMensagem;
begin
  vForm := TFormEnviarMensagem.Create(Application);
  vForm.ShowModal;
  vForm.Free;
end;

procedure Responder(pMensagemId: Integer; pAssunto: string; pUsuarioDestinoId: Integer);
var
  vForm: TFormEnviarMensagem;
begin
  if pUsuarioDestinoId = 0 then
    Exit;

  vForm := TFormEnviarMensagem.Create(Application);

  vForm.pnOpcoes.Visible := False;
  vForm.Caption := 'Responder mensagem';

  vForm.FUsuarioDestinoId := pUsuarioDestinoId;
  vForm.FMensagemResponderId := pMensagemId;

  vForm.eAssunto.Text := 'RESP: ' + pAssunto;
  vForm.eAssunto.ReadOnly := True;

  vForm.ShowModal;

  vForm.Free;
end;

procedure Encaminhar(pAssunto: string; pMensagem: string);
var
  vForm: TFormEnviarMensagem;
begin
  vForm := TFormEnviarMensagem.Create(Application);

  vForm.Caption := 'Encaminhar mensagem';
  vForm.eAssunto.Text := 'ENC: ' + pAssunto;
  vForm.eAssunto.ReadOnly := True;

  vForm.eMensagem.Lines.Add('Mensagem original...: ');
  vForm.eMensagem.Lines.Add('');
  vForm.eMensagem.Lines.Add(pMensagem);

  vForm.ShowModal;

  vForm.Free;
end;

procedure TFormEnviarMensagem.VerificarRegistro(Sender: TObject);
begin
  if
    (FUsuarioDestinoId = 0) and
    FrFuncionarios.EstaVazio and
    FrEmpresas.EstaVazio and
    FrCargosFuncionarios.EstaVazio
  then begin
    Exclamar('Nenhuma das op��es de destinat�rio da mensagem foi informada, verifique!');
    SetarFoco(FrFuncionarios);
    Abort;
  end;

  eMensagem.Lines.Text := Trim(eMensagem.Lines.Text);
  if eMensagem.Lines.Text = '' then begin
    Exclamar('A mensagem n�o foi informada!');
    SetarFoco(eMensagem);
    Abort;
  end;

  if eAssunto.Trim = '' then begin
    if not _Biblioteca.Perguntar('O assunto n�o foi informado, deseja continuar?') then begin
      SetarFoco(eAssunto);
      Abort;
    end;
  end;

  inherited;
end;

procedure TFormEnviarMensagem.FormCreate(Sender: TObject);
begin
  inherited;
  FUsuarioDestinoId := 0;
  FMensagemResponderId := 0;
end;

procedure TFormEnviarMensagem.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_F9 then
    sbEnviarMensagemClick(Sender);
end;

procedure TFormEnviarMensagem.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrFuncionarios);
end;

procedure TFormEnviarMensagem.sbEnviarMensagemClick(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  vFuncionariosIds: TArray<Integer>;
begin
  vFuncionariosIds := nil;
  VerificarRegistro(Sender);

  if pnOpcoes.Visible then begin
    vFuncionariosIds :=
      _Funcionarios.BuscarFuncionariosEnvioMensagem(
        Sessao.getConexaoBanco,
        FrFuncionarios.GetArrayOfInteger,
        FrEmpresas.GetArrayOfInteger,
        FrCargosFuncionarios.GetArrayOfInteger
      );
  end
  else begin
    SetLength(vFuncionariosIds, 1);
    vFuncionariosIds[0] := FUsuarioDestinoId;
  end;

  if vFuncionariosIds = nil then begin
    Exclamar('N�o foi encontrado nenhum funcion�rio para envio da mensagem!');
    SetarFoco(FrFuncionarios);
    Abort;
  end;

  Sessao.getConexaoBanco.IniciarTransacao;

  vRetBanco :=
    _Mensagens.AtualizarMensagem(
      Sessao.getConexaoBanco,
      0,
      eAssunto.Text,
      eMensagem.Lines.Text,
      vFuncionariosIds,
      'N',
      ToChar(ckPermitirResposta),
      'N',
      'ENV',
      False
    );

  if vRetBanco.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    Exclamar(vRetBanco.MensagemErro);
    SetarFoco(eMensagem);
    Abort;
  end;

  if FMensagemResponderId > 0 then begin
    vRetBanco := _Mensagens.SetarMensagemRespondida(Sessao.getConexaoBanco, FMensagemResponderId, False);
    if vRetBanco.TeveErro then begin
      Sessao.getConexaoBanco.VoltarTransacao;
      Exclamar(vRetBanco.MensagemErro);
      SetarFoco(eMensagem);
      Abort;
    end;
  end;

  Sessao.getConexaoBanco.FinalizarTransacao;

  ModalResult := mrOk;
end;

end.
