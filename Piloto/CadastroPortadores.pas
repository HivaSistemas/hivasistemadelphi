unit CadastroPortadores;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.Buttons, _Sessao, _Biblioteca,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, Vcl.StdCtrls, StaticTextLuka, _Portadores, PesquisaPortadores,
  RadioGroupLuka, EditLuka, CheckBoxLuka, _HerancaCadastro, _RecordsEspeciais, _PortadoresContas,
  Vcl.Menus, Vcl.ComCtrls, PesquisaBancosCaixas, _RecordsCadastros,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameContasBoletos;

type
  TFormCadastroPortadores = class(TFormHerancaCadastroCodigo)
    Label2: TLabel;
    eDescricao: TEditLuka;
    PageControl1: TPageControl;
    tsPrincipais: TTabSheet;
    rgTipo: TRadioGroupLuka;
    rgIncidenciaFinanceiro: TRadioGroupLuka;
    pmContas: TPopupMenu;
    miInserirConta: TMenuItem;
    Label3: TLabel;
    meInstrucoesBoleto: TMemo;
    sgContas: TGridLuka;
    StaticTextLuka1: TStaticTextLuka;
    miDeletarContaEmpresaSelecionada: TMenuItem;
    FrContasBoletos: TFrContasBoletos;
    procedure eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); reintroduce;
    procedure rgTipoClick(Sender: TObject);
    procedure miInserirContaClick(Sender: TObject);
    procedure miDeletarContaEmpresaSelecionadaClick(Sender: TObject);
    procedure sgContasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  private
    procedure PreencherRegistro(pPortador: RecPortadores);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroPortadores }

const
  coEmpresaId    = 0;
  coNomeEmpresa  = 1;
  coConta        = 2;
  coEmpresaConta = 3;
  coContaId      = 4;

procedure TFormCadastroPortadores.BuscarRegistro;
var
  vPortadores: TArray<RecPortadores>;
begin
  inherited;

  vPortadores := _Portadores.BuscarPortadores(Sessao.getConexaoBanco, 0, [eID.Text]);
  if vPortadores <> nil then
    PreencherRegistro(vPortadores[0]);
end;

procedure TFormCadastroPortadores.eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  //inherited;
  if Key <> VK_RETURN then
   Exit;

  if Trim(eID.Text) = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar o portador!');
    Abort;
  end;

  BuscarRegistro;
end;

procedure TFormCadastroPortadores.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _Portadores.ExcluirPortador(Sessao.getConexaoBanco, eID.Text);

  if vRetBanco.TeveErro then begin
    Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  Informar('Portador exclu�do com sucesso.');
  inherited;
end;

procedure TFormCadastroPortadores.GravarRegistro(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  vContas: TArray<RecPortadoresContas>;
  vContaboletoId : Integer;
begin
  vContas := nil;
  if rgTipo.GetValor = 'B' then begin
    for i := sgContas.FixedRows to sgContas.RowCount -1 do begin
      if sgContas.Cells[coConta, i] = '' then
        Continue;

      SetLength(vContas, Length(vContas) + 1);
      vContas[High(vContas)].EmpresaId  := SFormatInt( sgContas.Cells[coEmpresaId, i] );
      vContas[High(vContas)].PortadorId := eID.Text;
      vContas[High(vContas)].ContaId    := sgContas.Cells[coContaId, i];
    end;
  end;

  if FrContasBoletos.getDados <> nil then
    vContaboletoId := FrContasBoletos.getDados.ContaboletoId;

  vRetBanco :=
    _Portadores.AtualizarPortador(
      Sessao.getConexaoBanco,
      eID.Text,
      eDescricao.Text,
      rgTipo.GetValor,
      rgIncidenciaFinanceiro.GetValor,
      meInstrucoesBoleto.Lines.Text,
      ToChar(ckAtivo),
      vContaboletoId,
      vContas
    );

  if vRetBanco.TeveErro then begin
    Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  Informar('Portador atualizado com sucesso.');

  inherited;
end;

procedure TFormCadastroPortadores.miDeletarContaEmpresaSelecionadaClick(Sender: TObject);
begin
  inherited;
  sgContas.Cells[coConta, sgContas.Row]   := '';
  sgContas.Cells[coContaId, sgContas.Row] := '';
end;

procedure TFormCadastroPortadores.miInserirContaClick(Sender: TObject);
var
  vConta: RecContas;
begin
  inherited;
  vConta := RecContas(PesquisaBancosCaixas.PesquisarBancoCaixa( 'B', True, 0 ));
  if vConta <> nil then begin
    sgContas.Cells[coConta, sgContas.Row]        := vConta.Conta + '-' + vConta.DigitoConta;
    sgContas.Cells[coContaId, sgContas.Row]      := vConta.Conta;
    sgContas.Cells[coEmpresaConta, sgContas.Row] := NFormatN(vConta.EmpresaId) + ' - ' + vConta.NomeEmpresa;
  end;
end;

procedure TFormCadastroPortadores.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    eDescricao,
    rgTipo,
    rgIncidenciaFinanceiro,
    meInstrucoesBoleto,
    sgContas,
    ckAtivo,
    FrContasBoletos],
    pEditando
  );

  if pEditando then
    SetarFoco(rgTIpo);
end;

procedure TFormCadastroPortadores.PesquisarRegistro;
var
  vPortadores: RecPortadores;
begin
  vPortadores := RecPortadores(PesquisaPortadores.PesquisarPortador);
  if vPortadores = nil then
    Exit;

  inherited;
  PreencherRegistro(vPortadores);
end;

procedure TFormCadastroPortadores.PreencherRegistro(pPortador: RecPortadores);
var
  i: Integer;
  vPosic: Integer;
  vContas: TArray<RecPortadoresContas>;
begin
  eID.Text := pPortador.PortadorId;
  eDescricao.Text := pPortador.Descricao;
  rgTipo.SetIndicePorValor(pPortador.Tipo);
  rgTipoClick(nil);

  rgIncidenciaFinanceiro.SetIndicePorValor(pPortador.IncidenciaFinanceiro);
  meInstrucoesBoleto.Lines.Text := pPortador.InstrucaoBoleto;
  ckAtivo.Checked := pPortador.ativo = 'S';
  FrContasBoletos.InserirDadoPorChave(pPortador.ContasBoletoId);

  if rgTipo.GetValor = 'B' then begin
    vContas := _PortadoresContas.BuscarPortadores(Sessao.getConexaoBanco, 0, [eID.Text]);
    for i := Low(vContas) to High(vContas) do begin
      vPosic := sgContas.Localizar([coEmpresaId], [NFormatN(vContas[i].EmpresaId)]);
      if vPosic = -1 then
        Continue;

      sgContas.Cells[coConta, vPosic]        := vContas[i].ContaId + '-' + vContas[i].DigitoConta;
      sgContas.Cells[coContaId, vPosic]      := vContas[i].ContaId;
      sgContas.Cells[coEmpresaConta, vPosic] := NFormatN(vContas[i].EmpresaId) + ' - ' + vContas[i].NomeEmpresa;
    end;
  end;
end;

procedure TFormCadastroPortadores.rgTipoClick(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  _Biblioteca.Habilitar([sgContas, meInstrucoesBoleto], rgTipo.GetValor = 'B');
  if rgTipo.GetValor = 'B' then begin
    meInstrucoesBoleto.Lines.Text :=
      'Apos o vencimento, pagavel apenas no :BANCO, com multa de R$ :MULTA mais juros de R$ :JUROS_DIA ao dia.' +
      'Protesto automatico com 5 dia corridos.';

    for i := Low(Sessao.getEmpresas) to High(Sessao.getEmpresas) do begin
      sgContas.Cells[coEmpresaId, i + 1]   := NFormat(Sessao.getEmpresas[i].EmpresaId);
      sgContas.Cells[coNomeEmpresa, i + 1] := Sessao.getEmpresas[i].NomeFantasia;
    end;
    sgContas.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(Sessao.getEmpresas) );
  end;
end;

procedure TFormCadastroPortadores.sgContasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coEmpresaId] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgContas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormCadastroPortadores.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eDescricao.Text = '' then begin
    _Biblioteca.Exclamar('A descri��o do portador n�o foi informada corretamente, verifique!');
    SetarFoco(eDescricao);
    Abort;
  end;

  if rgTipo.GetValor = '' then begin
    _Biblioteca.Exclamar('O tipo de portador n�o foi definido corretamente, verifique!');
    SetarFoco(rgTipo);
    Abort;
  end;

  if rgIncidenciaFinanceiro.GetValor = '' then begin
    _Biblioteca.Exclamar('A incid�ncia financeiran�o foi definida corretamente, verifique!');
    SetarFoco(rgIncidenciaFinanceiro);
    Abort;
  end;

  if (rgTipo.GetValor = 'B') and (sgContas.Cells[coEmpresaId, 1] = '') then begin
    _Biblioteca.Exclamar('As contas para recebimento n�o foram definidas corretamente, verifique!');
    SetarFoco(sgContas);
    Abort;
  end;


end;

end.
