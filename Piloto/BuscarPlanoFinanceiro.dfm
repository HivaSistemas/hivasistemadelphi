inherited FormBuscarPlanoFinanceiro: TFormBuscarPlanoFinanceiro
  Caption = 'Buscar plano financeiro'
  ClientHeight = 99
  ClientWidth = 466
  OnShow = FormShow
  ExplicitWidth = 472
  ExplicitHeight = 128
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 62
    Width = 466
  end
  inline FrPlanosFinanceiro: TFrPlanosFinanceiros
    Left = 8
    Top = 8
    Width = 449
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 8
    ExplicitTop = 8
    ExplicitWidth = 449
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 424
      Height = 24
    end
    inherited PnTitulos: TPanel
      Width = 449
      inherited lbNomePesquisa: TLabel
        Width = 88
        Caption = 'Plano financeiro'
        ExplicitWidth = 88
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 379
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 424
      Height = 25
    end
  end
end
