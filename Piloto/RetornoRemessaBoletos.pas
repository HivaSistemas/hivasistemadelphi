unit RetornoRemessaBoletos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _CobrancaBancaria, _CobrancaBradesco,
  Vcl.StdCtrls, EditLuka, SpeedButtonLuka, _FrameHerancaPrincipal, _Sessao, _ContasReceber,
  _FrameHenrancaPesquisas, FramePortadores, Vcl.ComCtrls, Vcl.Buttons, _Biblioteca, _RecordsCadastros,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Portadores, _PortadoresContas, _Contas, _RecordsFinanceiros,
  CheckBoxLuka, TFlatSplitterUnit, StaticTextLuka, Vcl.Menus, _RecordsEspeciais, _ContasReceberBaixas,
  _ContasReceberBaixasItens, Informacoes.TituloReceber;

type
  TFormRetornoRemessaBoletos = class(TFormHerancaRelatoriosPageControl)
    FrPortador: TFrPortadores;
    sbPesquisaCaminhoArquivo: TSpeedButtonLuka;
    eCaminhoArquivo: TEditLuka;
    lb1: TLabel;
    pn1: TPanel;
    st1: TStaticTextLuka;
    st2: TStaticTextLuka;
    FlatSplitter1: TFlatSplitter;
    sgBoletos: TGridLuka;
    sgOutros: TGridLuka;
    stTotalBoletosPagos: TStaticTextLuka;
    st6: TStaticText;
    st5: TStaticText;
    stTotalBoletosBaixar: TStaticTextLuka;
    st15: TStaticText;
    stQtdeBoletosPagos: TStaticTextLuka;
    st3: TStaticText;
    stQtdeBoletosBaixar: TStaticTextLuka;
    sbGravar: TSpeedButtonLuka;
    st4: TStaticText;
    stContaBaixa: TStaticTextLuka;
    st7: TStaticText;
    stDataCredito: TStaticTextLuka;
    procedure sbPesquisaCaminhoArquivoClick(Sender: TObject);
    procedure sgBoletosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure sgOutrosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgBoletosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sbGravarClick(Sender: TObject);
    procedure sgBoletosDblClick(Sender: TObject);
  private
    FNossoNumeroBoletos: TArray<string>;
    FBoletosBaixar: TArray<RecContasReceber>;
    FPortadorId: string;

    procedure IdentificarBoletos;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

uses _EmitirBoletos;

{ TFormRetornoRemessaBoletos }

const
  coBaixar              = 0;
  coNossoNumero         = 1;
  coDataVencimento      = 2;
  coDataCredito         = 3;
  coValorDocumento      = 4;
  coValorMulta          = 5;
  coValorJuros          = 6;
  coValorDesconto       = 7;
  coValorPago           = 8;
  coValorTarifa         = 9;
  coReceberId           = 10;
  coNomeCliente         = 11;
  coCodigoRetorno       = 12;
  coDescricaoRetorno    = 13;
  coObservacao          = 14;

  // Ocultas
  coDataPagamento       = 15;
  coArqReceberId        = 16;
  coValorAltis          = 17;
  coDataVencimentoAltis = 18;
  coCadastroId          = 19;
  coCpfCnpj             = 20;
  coDocumento           = 21;
  coTituloAdiantado     = 22;
  coEmDuplicidade       = 23;
  coDataTarifa          = 24;
  coContaRetorno        = 25;

  (* Grid de outros documentos *)
  codNossoNumero      = 0;
  codDataVencimento   = 1;
  codValorDocumento   = 2;
  codValorTarifa      = 3;
  codCodigoRetorno    = 4;
  codDescricaoRetorno = 5;


procedure TFormRetornoRemessaBoletos.Carregar(Sender: TObject);
var
  i: Integer;
  j: Integer;
  vPosicBoleto: Integer;
  vLinhaBoleto: Integer;
  vCobranca: ICobrancaBancaria;
  vConta: TArray<RecContas>;
  vRegistros: TArray<RecBoletoRetornoRemessa>;
  vContasPortadores: TArray<RecPortadoresContas>;


  vBoletos: TBoletosACBr;
  vParametrosImpressao: TParametrosImpressao;
begin
  inherited;

  vBoletos := TBoletosACBr.Create;
  vParametrosImpressao:= TParametrosImpressao.Create;
  try
    vParametrosImpressao.ContasBoletosId := FrPortador.getPortador().ContasBoletoId;
    vParametrosImpressao.CaminhoArqRetorno := eCaminhoArquivo.Text;



    vBoletos.LerArquivoRetorno(vParametrosImpressao);


    if not vBoletos.CdsTitulos.IsEmpty then


  finally
    vParametrosImpressao.Free;
    vBoletos.Free;
  end;




  exit;
  vCobranca := nil;
  vConta := nil;

  FNossoNumeroBoletos := nil;

  vPosicBoleto := 0;
  FPortadorId := FrPortador.getPortador().PortadorId;

  vContasPortadores := _PortadoresContas.BuscarPortadores(Sessao.getConexaoBanco, 1, [FPortadorId, Sessao.getEmpresaLogada.EmpresaId]);
  if vContasPortadores = nil then begin
    _Biblioteca.Exclamar('N�o existe conta cadastrada para este portador para esta empresa!');
    Abort;
  end;

  vConta := _Contas.BuscarContas(Sessao.getConexaoBanco, 0, [vContasPortadores[0].ContaId], '', False, 0);
  if vConta = nil then begin
    _Biblioteca.Exclamar('A conta ' + vContasPortadores[0].ContaId + ' n�o foi encontrada!');
    Abort;
  end;

  stContaBaixa.Caption := vContasPortadores[0].ContaId;

  // Bradesco
  vCobranca := TCobrancaBradesco.Create;

  vRegistros :=
    vCobranca.getRetorno(
      vConta[0].CodigoEmpresaCobranca,
      eCaminhoArquivo.Text
    );

  if vRegistros = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  //Verificando os vRegistros em duplicidade
  for i := Low(vRegistros) to High(vRegistros) do begin
    if vRegistros[i].RegistroEmDuplicidade = 'S' then
      Continue;

    for j := i + 1 to high(vRegistros) do begin
      if vRegistros[j].RegistroEmDuplicidade = 'S' then
        Continue;

      if
        (vRegistros[i].NossoNumero = vRegistros[j].NossoNumero) and
        (vRegistros[i].DataVencimento = vRegistros[j].DataVencimento) and
        (vRegistros[i].ValorDocumento = vRegistros[j].ValorDocumento) and
        (vRegistros[i].Retorno.ValorPago = vRegistros[j].Retorno.ValorPago)
      then begin
        vRegistros[i].RegistroEmDuplicidade := 'S';
        vRegistros[j].RegistroEmDuplicidade := 'S';
      end;

      if
        (FrPortador.getPortador.PortadorId = '237') and
        (vRegistros[j].Retorno.TipoId = 17) and
        (vRegistros[i].NossoNumero = vRegistros[j].NossoNumero) and
        (vRegistros[i].ValorDocumento = vRegistros[j].ValorDocumento) and
        (vRegistros[i].Retorno.ValorPago = vRegistros[j].Retorno.ValorPago)
      then
        vRegistros[j].RegistroEmDuplicidade := 'S';
    end;
  end;

  // Montando a grid de documentos a baixar
  vLinhaBoleto := 0;
  for i := Low(vRegistros) to High(vRegistros) do begin
    if not vRegistros[i].Retorno.Baixar then
      Continue;

    Inc(vLinhaBoleto);

    sgBoletos.Cells[coNossoNumero, vLinhaBoleto]      := trim(vRegistros[i].NossoNumero);
    sgBoletos.Cells[coDataVencimento, vLinhaBoleto]   := DFormatN(vRegistros[i].DataVencimento);
    sgBoletos.Cells[coDataCredito, vLinhaBoleto]      := DFormatN(vRegistros[i].DataVencimento);

    sgBoletos.Cells[coValorTarifa, vLinhaBoleto]      := NFormatN(vRegistros[i].Retorno.ValorTarifa);
    sgBoletos.Cells[coDataPagamento, vLinhaBoleto]    := DFormatN(vRegistros[i].DataPagamento);
    sgBoletos.Cells[coValorDocumento, vLinhaBoleto]   := NFormatN(vRegistros[i].ValorDocumento);
    sgBoletos.Cells[coValorDesconto, vLinhaBoleto]    := NFormatN(vRegistros[i].Retorno.Desconto);
    sgBoletos.Cells[coValorJuros, vLinhaBoleto]       := NFormatN(vRegistros[i].Retorno.Juros);
    sgBoletos.Cells[coValorMulta, vLinhaBoleto]       := NFormatN(vRegistros[i].Retorno.Multa);
    sgBoletos.Cells[coCodigoRetorno, vLinhaBoleto]    := NFormatN(vRegistros[i].Retorno.TipoId);
    sgBoletos.Cells[coDescricaoRetorno, vLinhaBoleto] := vRegistros[i].Retorno.DescricaoTipo;
    sgBoletos.Cells[coContaRetorno, vLinhaBoleto]     := vRegistros[i].Retorno.ContaArquivo;

    if vRegistros[i].Retorno.TipoId = 17 then
      sgBoletos.Cells[coValorPago, vLinhaBoleto] := ''
    else
      sgBoletos.Cells[coValorPago, vLinhaBoleto] := NFormat(vRegistros[i].Retorno.ValorPago);

    sgBoletos.Cells[coEmDuplicidade, vLinhaBoleto]  := vRegistros[i].RegistroEmDuplicidade;
    sgBoletos.Cells[coArqReceberId, vLinhaBoleto]   := NFormat(vRegistros[i].ReceberId);
    sgBoletos.Cells[coDataTarifa, vLinhaBoleto]     := DFormat(vRegistros[i].Retorno.DataCredito);

    if not DataOk(sgBoletos.Cells[coDataTarifa, vLinhaBoleto]) then
      sgBoletos.Cells[coDataTarifa, vLinhaBoleto] := DFormat(vRegistros[i].Retorno.DataOcorrencia);

    if NFormatN(SFormatDouble(sgBoletos.Cells[coNossoNumero, vLinhaBoleto])) = NFormatN(0) then
      sgBoletos.Cells[coObservacao, vLinhaBoleto] := 'Nosso n�mero inv�lido';

    stTotalBoletosPagos.Somar( vRegistros[i].ValorDocumento );
    stQtdeBoletosPagos.Somar( 1 );

    SetLength(FNossoNumeroBoletos, Length(FNossoNumeroBoletos) + 1);

    FNossoNumeroBoletos[vPosicBoleto] := sgBoletos.Cells[coNossoNumero, vLinhaBoleto];

    Inc(vPosicBoleto);
  end;
  sgBoletos.SetLinhasGridPorTamanhoVetor( vLinhaBoleto );

  // Montando a grid de documentos a baixar
  vLinhaBoleto := 0;
  for i := Low(vRegistros) to High(vRegistros) do begin
    if vRegistros[i].Retorno.Baixar then
      Continue;

    Inc(vLinhaBoleto);

    sgOutros.Cells[codNossoNumero, vLinhaBoleto]      := vRegistros[i].NossoNumero;
    sgOutros.Cells[codDataVencimento, vLinhaBoleto]   := DFormatN(vRegistros[i].DataVencimento);
    sgOutros.Cells[codValorDocumento, vLinhaBoleto]   := NFormatN(vRegistros[i].ValorDocumento);
    sgOutros.Cells[codValorTarifa, vLinhaBoleto]      := NFormatN(vRegistros[i].Retorno.ValorTarifa);
    sgOutros.Cells[codCodigoRetorno, vLinhaBoleto]    := NFormatN(vRegistros[i].Retorno.TipoId);
    sgOutros.Cells[codDescricaoRetorno, vLinhaBoleto] := vRegistros[i].Retorno.DescricaoTipo;
  end;
  sgOutros.SetLinhasGridPorTamanhoVetor( vLinhaBoleto );

  IdentificarBoletos;

  SetarFoco(sgBoletos);
end;

procedure TFormRetornoRemessaBoletos.FormShow(Sender: TObject);
begin
  inherited;
  FrPortador.InserirDadoPorChave('237', False);
end;

procedure TFormRetornoRemessaBoletos.IdentificarBoletos;
var
  i: Integer;
  j: Integer;
  vSql: string;
  vContasReceber: TArray<RecContasReceber>;
begin
  vSql := '';
  vContasReceber := nil;
  FBoletosBaixar := nil;

  if FNossoNumeroBoletos = nil then
    Exit;

  vSql :=
    'where COR.PORTADOR_ID =  ' + FPortadorId + ' ' +
    'and ' + FiltroInStr('lpad(COR.NOSSO_NUMERO, 11, ''0'')', FNossoNumeroBoletos) + ' ' +
    'and COR.EMPRESA_ID = ' + IntToStr(Sessao.getEmpresaLogada.EmpresaId) + ' ' +
    'order by ' +
    '  COR.DATA_VENCIMENTO desc ';

  vContasReceber := _ContasReceber.BuscarContasReceberComando(Sessao.getConexaoBanco, vSql);
  if vContasReceber = nil then
    Exit;

  for i := Low(vContasReceber) to High(vContasReceber) do begin
    for j := 1 to sgBoletos.RowCount - 1 do begin
      if
        (Trim(sgBoletos.Cells[coNossoNumero, j]) = '') or
        (IntToStr(StrToInt64Def(vContasReceber[i].NossoNumero, 0)) <> IntToStr(StrToInt64Def(sgBoletos.Cells[coNossoNumero, j], 0)))
      then
        Continue;

      if vContasReceber[i].status = 'A' then begin
        sgBoletos.Cells[coBaixar, j] := 'Sim';
        sgBoletos.Cells[coObservacao, j] := 'Identificado!';

        stTotalBoletosBaixar.Somar( vContasReceber[i].ValorDocumento );
        stQtdeBoletosBaixar.Somar( 1 );
      end
      else begin
        sgBoletos.Cells[coBaixar, j] := charNaoPermiteSelecionar;
        sgBoletos.Cells[coObservacao, j] := 'Identificado mas j� foi baixado!';
      end;

      if (SFormatInt(sgBoletos.Cells[coCodigoRetorno, j]) = 17) and (FPortadorId = '237') then begin
        sgBoletos.Cells[coBaixar, j] := '---';
        sgBoletos.Cells[coObservacao, j] := 'Identificado no Hiva, mas n�o ser� baixado por ser do tipo 17.'
      end;

      if sgBoletos.Cells[coEmDuplicidade, j] = 'S' then begin
        sgBoletos.Cells[coBaixar, j] := '---';
        sgBoletos.Cells[coObservacao, j] := 'Identificado no Hiva, mas foi pagado em duplicidade.'
      end;

      sgBoletos.Cells[coReceberId, j]           := NFormat(vContasReceber[i].ReceberId);
      sgBoletos.Cells[coNomeCliente, j]         := getInformacao(vContasReceber[i].CadastroId, vContasReceber[i].NomeCliente);
      sgBoletos.Cells[coValorAltis, j]          := NFormat(vContasReceber[i].ValorDocumento);
      sgBoletos.Cells[coDataVencimentoAltis, j] := DFormat(vContasReceber[i].data_vencimento);
      sgBoletos.Cells[coCadastroId, j]          := NFormat(vContasReceber[i].CadastroId);
//            sgBoletos.Cells[coCpfCnpj, j] := vContasReceber[i].CpfCnpj;
      sgBoletos.Cells[coDocumento, j]           := vContasReceber[i].documento;

      stDataCredito.Caption := sgBoletos.Cells[coDataCredito, j];

      if sgBoletos.Cells[coBaixar, j] = 'Sim' then begin
        SetLength(FBoletosBaixar, Length(FBoletosBaixar) + 1);
        FBoletosBaixar[High(FBoletosBaixar)] := vContasReceber[i];
      end;
    end;
  end;

  for j := 1 to sgBoletos.RowCount - 1 do begin
    if sgBoletos.Cells[coReceberId, j] = '' then begin
      sgBoletos.Cells[coBaixar, j] := '---';
      sgBoletos.Cells[coObservacao, j] := 'Boleto n�o localizado!';
    end;
  end;
end;

procedure TFormRetornoRemessaBoletos.Imprimir(Sender: TObject);
begin
  inherited;
  sgBoletos.ClearGrid();
  if FrPortador.EstaVazio then begin
    _Biblioteca.Exclamar('O portador n�o foi informado corretamente, verifique!');
    SetarFoco(FrPortador);
    Abort;
  end;

  if FrPortador.getPortador.PortadorId <> '237' then begin
    _Biblioteca.Exclamar('Apenas o portador 237 pode ser utilizado para este recurso!');
    SetarFoco(FrPortador);
    Abort;
  end;

  if not FileExists(eCaminhoArquivo.Text) then begin
    _Biblioteca.Exclamar('O arquivo informado � inv�lido!');
    SetarFoco(eCaminhoArquivo);
    Abort;
  end;
end;

procedure TFormRetornoRemessaBoletos.sbGravarClick(Sender: TObject);
var
  i: Integer;
  vBaixaId: Integer;
  vRetBanco: RecRetornoBD;
  vConta: TArray<RecTitulosBaixasPagtoDin>;
begin
  inherited;
  if stQtdeBoletosBaixar.AsInt = 0 then begin
    _Biblioteca.Exclamar('A baixa n�o ser� permitida pois n�o existe nenhum boelto identificado!');
    Abort;
  end;

  if FBoletosBaixar = nil then begin
    _Biblioteca.Exclamar('A baixa n�o ser� permitida pois n�o existe nenhum boelto identificado!');
    Abort;
  end;

  Sessao.getConexaoBanco.IniciarTransacao;

  SetLength(vConta, 1);
  vConta[0].ContaId := stContaBaixa.Caption;
  vConta[0].Valor   := stTotalBoletosBaixar.AsCurr;

  vRetBanco :=
    _ContasReceberBaixas.AtualizarContaReceberBaixa(
      Sessao.getConexaoBanco,
      0,
      Sessao.getEmpresaLogada.EmpresaId,
      0,
      0,
      stTotalBoletosBaixar.AsCurr,
      0,
      0,
      0,
      0,
      0,
      stTotalBoletosBaixar.AsCurr,
      0,
      0,
      0,
      0,
      0,
      StrToDate(stDataCredito.Caption),
      'N',
      'Baixa realizada pelo retorno de remessa de boeltos com o arquivo arquivo ' + ExtractFileName(eCaminhoArquivo.Text),
      'S',
      'BBO',
      0,
      vConta,
      nil,
      nil,
      nil,
      nil,
      nil,
      0,
      0,
      True,
      0,
      nil,
      'N'
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  vBaixaId := vRetBanco.AsInt;
  for i := Low(FBoletosBaixar) to High(FBoletosBaixar) do begin
    // Gravando os t�tulos da baixa
    vRetBanco :=
      _ContasReceberBaixasItens.AtualizarContasReceberBaixasItens(
        Sessao.getConexaoBanco,
        vBaixaId,
        FBoletosBaixar[i].ReceberId,
        FBoletosBaixar[i].ValorDocumento,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        True
      );

    Sessao.AbortarSeHouveErro(vRetBanco);
  end;

  Sessao.getConexaoBanco.FinalizarTransacao;

  _Biblioteca.Informar('Baixa com o arquivo de remessa realizada com sucesso. C�digo: ' + NFormatN(vRetBanco.AsInt) );
  Carregar(Sender);
end;

procedure TFormRetornoRemessaBoletos.sbPesquisaCaminhoArquivoClick(Sender: TObject);
var
  vDialog: TOpenDialog;
begin
  inherited;
  vDialog := TOpenDialog.Create(Self);

  vDialog.DefaultExt := '*.ret';
  vDialog.Filter := '';
  vDialog.Filter := 'Arquivo de retorno|*.ret';

  if vDialog.Execute then
    eCaminhoArquivo.Text := vDialog.FileName;

  vDialog.Free;
end;

procedure TFormRetornoRemessaBoletos.sgBoletosDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.TituloReceber.Informar(SFormatInt(sgBoletos.Cells[coReceberId, sgBoletos.Row]));
end;

procedure TFormRetornoRemessaBoletos.sgBoletosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if Acol in[
    coValorDocumento,
    coValorMulta,
    coValorJuros,
    coValorDesconto,
    coValorPago,
    coValorTarifa,
    coReceberId,
    coCodigoRetorno]
  then
    vAlinhamento := taRightJustify
  else if ACol in[coBaixar] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgBoletos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRetornoRemessaBoletos.sgBoletosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coBaixar then begin
    if sgBoletos.Cells[ACol, ARow] = 'Sim' then begin
      AFont.Style := [fsBold];
      AFont.Color := clBlue;
    end;
  end;
end;

procedure TFormRetornoRemessaBoletos.sgOutrosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[codValorDocumento, codValorTarifa, codCodigoRetorno] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgOutros.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRetornoRemessaBoletos.VerificarRegistro(Sender: TObject);
begin
  inherited;

  _Biblioteca.LimparCampos([
    sgBoletos,
    sgOutros,
    stTotalBoletosPagos,
    stQtdeBoletosPagos,
    stTotalBoletosBaixar,
    stQtdeBoletosBaixar
  ]);

  if eCaminhoArquivo.Text = '' then begin
    _Biblioteca.Exclamar('Nenhum arquivo foi informado, verifique!');
    SetarFoco(eCaminhoArquivo);
    Abort;
  end;
end;

end.
