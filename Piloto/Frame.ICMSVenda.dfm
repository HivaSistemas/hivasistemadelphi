inherited FrameICMSVenda: TFrameICMSVenda
  Width = 513
  Height = 64
  ExplicitWidth = 513
  ExplicitHeight = 64
  object gbICMS: TGroupBox
    Left = 0
    Top = 0
    Width = 513
    Height = 64
    Align = alClient
    Caption = '  N'#227'o contribuinte  '
    TabOrder = 0
    object lb1: TLabel
      Left = 6
      Top = 16
      Width = 18
      Height = 14
      Caption = 'CST'
    end
    object cbCST: TComboBoxLuka
      Left = 6
      Top = 31
      Width = 46
      Height = 22
      Hint = 'CST para n'#227'o contribuintes'
      BevelKind = bkFlat
      Style = csOwnerDrawFixed
      Color = clWhite
      TabOrder = 0
      OnChange = cbCSTChange
      OnKeyDown = ProximoCampo
      Items.Strings = (
        '00'
        '10'
        '20'
        '30'
        '40'
        '41'
        '50'
        '51'
        '60'
        '70'
        '90')
      Valores.Strings = (
        '00'
        '10'
        '20'
        '30'
        '40'
        '41'
        '50'
        '51'
        '60'
        '60'
        '70'
        '90')
    end
  end
end
