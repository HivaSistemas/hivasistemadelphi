unit TransferenciasProdutosEntreEmpresas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Biblioteca,
  CheckBoxLuka, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _FrameHerancaPrincipal, _Sessao, _RecordsEspeciais,
  _FrameHenrancaPesquisas, FrameEmpresas, StaticTextLuka, Vcl.Grids, GridLuka, _RecordsEstoques,
  Frame.Lotes, FrameProdutos, FrameLocais, _TransfProdutosEmpresasItens, _TransfProdutosEmpresas, _EstoquesDivisao,
  FrameMotoristas, _CustosProdutos, _RecordsCadastros, _Cadastros, Vcl.Menus, _ComunicacaoNFE;

type
  TFormTransferenciasProdutosEntreEmpresas = class(TFormHerancaCadastroCodigo)
    FrEmpresaDestino: TFrEmpresas;
    FrEmpresaOrigem: TFrEmpresas;
    lb7: TLabel;
    FrLocalOrigem: TFrLocais;
    FrProduto: TFrProdutos;
    eQuantidade: TEditLuka;
    FrLote: TFrameLotes;
    sgItens: TGridLuka;
    st1: TStaticTextLuka;
    stEstoqueFisico: TStaticText;
    stEstoqueDisponivel: TStaticText;
    st2: TStaticTextLuka;
    st3: TStaticTextLuka;
    st6: TStaticTextLuka;
    stQtdeProdutos: TStaticTextLuka;
    stValorTotal: TStaticTextLuka;
    FrMotorista: TFrMotoristas;
    pmOpcoes: TPopupMenu;
    miBuscarTodosProdutosEstoqueDisponivel: TMenuItem;
    procedure eQuantidadeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure sgItensDblClick(Sender: TObject);
    procedure sgItensKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure miBuscarTodosProdutosEstoqueDisponivelClick(Sender: TObject);
  private
    procedure addGrid(
      pProdutoId: Integer;
      pNome: string;
      pMarca: string;
      pLote: string;
      pQuantidade: Double;
      pUnidade: string;
      pPrecoUnitario: Double;
      pLocalOrigemId: Integer;
      pNomeLocalOrigem: string;
      pTipoControleEstoque: string
    );

    procedure FrEmpresaOrigemOnAposPesquisar(Sender: TObject);

    procedure FrProdutoOnAposPesquisar(Sender: TObject);
    procedure FrProdutoOnAposDeletar(Sender: TObject);

    procedure FrLocalOnAposPesquisar(Sender: TObject);
    procedure FrLocaloOnAposDeletar(Sender: TObject);

    procedure checarEstoqueProduto;
    procedure calcularTotalProdutos;
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormTransferenciasProdutosEntreEmpresas }

const
  coProdutoId      = 0;
  coNome           = 1;
  coMarca          = 2;
  coQuantidade     = 3;
  coUnidade        = 4;
  coLote           = 5;
  coPrecoUnitario  = 6;
  coValorTotal     = 7;
  coLocalOrigemId  = 8;
  coNomeLocal      = 9;

  (* Ocultas *)
  coTipoControleEstoque = 10;

procedure TFormTransferenciasProdutosEntreEmpresas.BuscarRegistro;
begin
  _Biblioteca.NenhumRegistro;
  Exit;

  inherited;
end;

procedure TFormTransferenciasProdutosEntreEmpresas.calcularTotalProdutos;
var
  i: Integer;
begin
  stValorTotal.AsCurr := 0;
  for i := 1 to sgItens.RowCount -1 do
    stValorTotal.Somar( SFormatCurr( sgItens.Cells[coValorTotal, i] ) );
end;

procedure TFormTransferenciasProdutosEntreEmpresas.checarEstoqueProduto;
var
  vEstoques: TArray<RecEstoquesDivisao>;
begin
  _Biblioteca.LimparCampos([stEstoqueFisico, stEstoqueDisponivel]);

  if FrProduto.EstaVazio or FrLocalOrigem.EstaVazio then
    Exit;

  vEstoques := _EstoquesDivisao.BuscarEstoque(Sessao.getConexaoBanco, 1, [FrEmpresaOrigem.getEmpresa.EmpresaId, FrLocalOrigem.GetLocais.local_id, FrProduto.getProduto.produto_id]);
  if vEstoques = nil then begin
    _Biblioteca.Exclamar('N�o foi encontrado estoque para o produto selecionado no local ' + FrLocalOrigem.GetLocais().nome + '!');
    SetarFoco(FrProduto);
    FrProduto.Clear;
    Exit;
  end;

  stEstoqueFisico.Caption     := NFormatEstoque(vEstoques[0].Fisico);
  stEstoqueDisponivel.Caption := NFormatEstoque(vEstoques[0].Disponivel);
end;

procedure TFormTransferenciasProdutosEntreEmpresas.eQuantidadeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  vCustos: TArray<RecCustoProduto>;
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if FrProduto.EstaVazio then begin
    _Biblioteca.Exclamar('O produto n�o foi informado corretamente, verifique!');
    SetarFoco(FrProduto);
    Exit;
  end;

  if FrLocalOrigem.EstaVazio then begin
    _Biblioteca.Exclamar('O local de origem n�o foi informado corretamente, verifique!');
    SetarFoco(FrLocalOrigem);
    Exit;
  end;

  if not FrLote.LoteValido then begin
    Exclamar('O lote informado n�o � v�lido, verifique!');
    SetarFoco(FrLote);
    Exit;
  end;

  if eQuantidade.AsCurr = 0 then begin
    Exclamar('A quantidade n�o foi inforamada corretamente, verifique!');
    SetarFoco(eQuantidade);
    Exit;
  end;

  vCustos := _CustosProdutos.BuscarCustoProdutos(Sessao.getConexaoBanco, 0, [FrProduto.getProduto.produto_id, FrEmpresaOrigem.getEmpresa.EmpresaId]);
  if vCustos = nil then begin
    _Biblioteca.Exclamar('N�o foi encontrado os custos para este produto!');
    SetarFoco(FrProduto);
    Exit;
  end;

  if NFormatN(zvl2([vCustos[0].PrecoFinal, vCustos[0].PrecoFinalMedio, vCustos[0].PrecoLiquido, vCustos[0].PrecoLiquidoMedio])) = '' then begin
    _Biblioteca.Exclamar('N�o h� custos definido para este produto!');
    SetarFoco(FrProduto);
    Exit;
  end;

  addGrid(
    FrProduto.getProduto().produto_id,
    FrProduto.getProduto().nome,
    FrProduto.getProduto().nome_marca,
    FrLote.Lote,
    eQuantidade.AsDouble,
    FrProduto.getProduto().unidade_venda,
    _Biblioteca.Arredondar(zvl2([vCustos[0].PrecoFinal, vCustos[0].PrecoFinalMedio, vCustos[0].PrecoLiquido, vCustos[0].PrecoLiquidoMedio]), 2),
    FrLocalOrigem.GetLocais().local_id,
    FrLocalOrigem.GetLocais().nome,
    FrProduto.getProduto().TipoControleEstoque
  );

  _Biblioteca.LimparCampos([FrProduto, FrLote, eQuantidade, stEstoqueFisico, stEstoqueDisponivel]);
  SetarFoco(FrProduto);

  calcularTotalProdutos;
end;

procedure TFormTransferenciasProdutosEntreEmpresas.ExcluirRegistro;
begin
  inherited;

end;

procedure TFormTransferenciasProdutosEntreEmpresas.GravarRegistro(Sender: TObject);
var
  i: Integer;
  vMotoristaId: Integer;
  vRetBanco: RecRetornoBD;
  vItens: TArray<RecTransfProdutosEmpresasItens>;
begin
  inherited;

  vItens := nil;
  SetLength(vItens, sgItens.RowCount -1);
  for i := 1 to sgItens.RowCount -1 do begin
    vItens[i - 1].TransferenciaId     := 0;
    vItens[i - 1].ProdutoId           := SFormatInt(sgItens.Cells[coProdutoId, i]);
    vItens[i - 1].ItemId              := i;
    vItens[i - 1].LocalOrigemId       := SFormatInt(sgItens.Cells[coLocalOrigemId, i]);
    vItens[i - 1].Lote                := sgItens.Cells[coLote, i];
    vItens[i - 1].Quantidade          := SFormatDouble(sgItens.Cells[coQuantidade, i]);
    vItens[i - 1].TipoControleEstoque := sgItens.Cells[coTipoControleEstoque, i];;
    vItens[i - 1].PrecoUnitario       := SFormatDouble(sgItens.Cells[coPrecoUnitario, i]);
    vItens[i - 1].ValorTotal          := SFormatDouble(sgItens.Cells[coValorTotal, i]);
  end;

  vMotoristaId := 0;
  if not FrMotorista.EstaVazio then
    vMotoristaId := FrMotorista.getMotorista().Cadastro.cadastro_id;

  vRetBanco :=
    _TransfProdutosEmpresas.AtualizarTransfProdutosEmpresas(
      Sessao.getConexaoBanco,
      eID.AsInt,
      FrEmpresaOrigem.getEmpresa().EmpresaId,
      FrEmpresaDestino.getEmpresa().EmpresaId,
      vMotoristaId,
      'A',
      stValorTotal.AsDouble,
      vItens
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.InformarAlteracaoRegistro( vRetBanco.AsInt );

  if _Biblioteca.Perguntar('Deseja emitir a NF-e de transfer�ncia agora?') then
    _ComunicacaoNFE.EnviarNFe(vRetBanco.AsInt2);
end;

procedure TFormTransferenciasProdutosEntreEmpresas.miBuscarTodosProdutosEstoqueDisponivelClick(Sender: TObject);
var
  i: Integer;
  vEstoques: TArray<RecEstoquesDivisao>;
begin
  inherited;

  if FrEmpresaOrigem.EstaVazio then begin
    _Biblioteca.Exclamar('A empresa de origem dos produtos n�o foi definida, verifique!');
    SetarFoco(FrEmpresaOrigem);
    Exit;
  end;

  vEstoques := _EstoquesDivisao.BuscarEstoque(Sessao.getConexaoBanco, 2, [FrEmpresaOrigem.getEmpresa.EmpresaId]);
  if vEstoques = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  for i := Low(vEstoques) to High(vEstoques) do begin
    if vEstoques[i].ProdutoId <> vEstoques[i].ProdutoPaiId then
      Continue;

    if Sessao.EProdutoKit(vEstoques[i].TipoControleEstoque) then
      Continue;

    if vEstoques[i].Disponivel <= 0 then
      Continue;

    addGrid(
      vEstoques[i].ProdutoId,
      vEstoques[i].NomeProduto,
      vEstoques[i].NomeMarca,
      vEstoques[i].Lote,
      vEstoques[i].Disponivel,
      vEstoques[i].UnidadeVenda,
      vEstoques[i].CustoTransferencia,
      vEstoques[i].LocalId,
      vEstoques[i].NomeLocal,
      vEstoques[i].TipoControleEstoque
    );
  end;

  calcularTotalProdutos;
end;

procedure TFormTransferenciasProdutosEntreEmpresas.addGrid(
  pProdutoId: Integer;
  pNome: string;
  pMarca: string;
  pLote: string;
  pQuantidade: Double;
  pUnidade: string;
  pPrecoUnitario: Double;
  pLocalOrigemId: Integer;
  pNomeLocalOrigem: string;
  pTipoControleEstoque: string
);
var
  vLinha: Integer;
begin
  vLinha := sgItens.Localizar([coProdutoId, coLocalOrigemId, coLote], [NFormat(pProdutoId), NFormat(pLocalOrigemId), pLote]);
  if vLinha = -1 then begin
    if sgItens.Cells[coProdutoId, 1] = '' then
      vLinha := 1
    else begin
      vLinha := sgItens.RowCount;
      sgItens.RowCount := sgItens.RowCount + 1;
    end;
  end;

  sgItens.Cells[coProdutoId, vLinha]           := NFormat(pProdutoId);
  sgItens.Cells[coNome, vLinha]                := pNome;
  sgItens.Cells[coMarca, vLinha]               := pMarca;
  sgItens.Cells[coUnidade, vLinha]             := pUnidade;
  sgItens.Cells[coLocalOrigemId, vLinha]       := NFormat(pLocalOrigemId);
  sgItens.Cells[coNomeLocal, vLinha]           := pNomeLocalOrigem;
  sgItens.Cells[coQuantidade, vLinha]          := NFormatNEstoque(pQuantidade);
  sgItens.Cells[coLote, vLinha]                := pLote;
  sgItens.Cells[coPrecoUnitario, vLinha]       := NFormat(pPrecoUnitario);
  sgItens.Cells[coValorTotal, vLinha]          := NFormat(pPrecoUnitario * pQuantidade);
  sgItens.Cells[coTipoControleEstoque, vLinha] := pTipoControleEstoque;

  stQtdeProdutos.Caption := NFormat(sgItens.RowCount -1);
  sgItens.Row := vLinha;
end;

procedure TFormTransferenciasProdutosEntreEmpresas.FormCreate(Sender: TObject);
begin
  inherited;
  FrEmpresaOrigem.OnAposPesquisar := FrEmpresaOrigemOnAposPesquisar;

  FrProduto.OnAposPesquisar := FrProdutoOnAposPesquisar;
  FrProduto.OnAposDeletar   := FrProdutoOnAposDeletar;

  FrLocalOrigem.OnAposPesquisar := FrLocalOnAposPesquisar;
  FrLocalOrigem.OnAposDeletar   := FrLocaloOnAposDeletar;
end;

procedure TFormTransferenciasProdutosEntreEmpresas.FrEmpresaOrigemOnAposPesquisar(Sender: TObject);
begin
  FrEmpresaOrigem.Modo(False, False);
  SetarFoco(FrEmpresaDestino);
end;

procedure TFormTransferenciasProdutosEntreEmpresas.FrLocalOnAposPesquisar(Sender: TObject);
begin
  checarEstoqueProduto;
end;

procedure TFormTransferenciasProdutosEntreEmpresas.FrLocaloOnAposDeletar(
  Sender: TObject);
begin
  checarEstoqueProduto;
end;

procedure TFormTransferenciasProdutosEntreEmpresas.FrProdutoOnAposDeletar(Sender: TObject);
begin
  FrLote.Clear;
  checarEstoqueProduto;
end;

procedure TFormTransferenciasProdutosEntreEmpresas.FrProdutoOnAposPesquisar(Sender: TObject);
begin
  if Em(FrProduto.getProduto.TipoControleEstoque, ['K', 'A']) then begin
    _Biblioteca.Informar('Produtos que s�o cotrolados como "Kit" n�o pode ter seu estoque transferido, transfira os produtos que comp�e o kit.');
    SetarFoco(FrProduto);
    FrProduto.Clear;
    Exit;
  end;

  FrLote.SetTipoControleEstoque(FrProduto.getProduto.TipoControleEstoque, FrProduto.getProduto.ExigirDataFabricacaoLote, FrProduto.getProduto.ExigirDataVencimentoLote);

  checarEstoqueProduto;
end;

procedure TFormTransferenciasProdutosEntreEmpresas.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    FrEmpresaOrigem,
    FrEmpresaDestino,
    FrProduto,
    FrLocalOrigem,
    FrLote,
    eQuantidade,
    sgItens,
    FrMotorista,
    stEstoqueFisico,
    stEstoqueDisponivel,
    stQtdeProdutos,
    stValorTotal],
    pEditando
  );

  if pEditando then
    SetarFoco(FrEmpresaOrigem);
end;

procedure TFormTransferenciasProdutosEntreEmpresas.PesquisarRegistro;
begin
  inherited;

end;

procedure TFormTransferenciasProdutosEntreEmpresas.sgItensDblClick(Sender: TObject);
begin
  inherited;
  FrProduto.InserirDadoPorChave( SFormatInt(sgItens.Cells[coProdutoId, sgItens.Row]), False );
  FrLocalOrigem.InserirDadoPorChave( SFormatInt(sgItens.Cells[coLocalOrigemId, sgItens.Row]), False );

  eQuantidade.AsDouble := SFormatDouble(sgItens.Cells[coQuantidade, sgItens.Row]);
  FrLote.Lote          := sgItens.Cells[coLote, sgItens.Row];

  SetarFoco(FrProduto);
end;

procedure TFormTransferenciasProdutosEntreEmpresas.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if
    ACol in[
      coProdutoId,
      coQuantidade,
      coPrecoUnitario,
      coValorTotal,
      coLocalOrigemId]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormTransferenciasProdutosEntreEmpresas.sgItensKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_DELETE then begin
    sgItens.DeleteRow( sgItens.Row );
    calcularTotalProdutos;
  end;
end;

procedure TFormTransferenciasProdutosEntreEmpresas.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if FrEmpresaOrigem.EstaVazio then begin
    _Biblioteca.Exclamar('A empresa de origem n�o foi informada corretamente, verifique!');
    SetarFoco(FrEmpresaOrigem);
    Abort;
  end;

  if FrEmpresaDestino.EstaVazio then begin
    _Biblioteca.Exclamar('A empresa de destino n�o foi informada corretamente, verifique!');
    SetarFoco(FrEmpresaDestino);
    Abort;
  end;

  if FrEmpresaOrigem.getEmpresa.EmpresaId = FrEmpresaDestino.getEmpresa.EmpresaId then begin
    _Biblioteca.Exclamar('A empresa de origem e destino n�o podem ser as mesmas, verifique!');
    SetarFoco(FrEmpresaDestino);
    Abort;
  end;

  if not _Cadastros.EFornecedor( Sessao.getConexaoBanco, FrEmpresaOrigem.getEmpresa.CadastroId ) then begin
    _Biblioteca.Exclamar(
      'Para realizar a transfer�ncia a empresa de origem deve estar cadastrada como fornecedor, ' +
      'por favor realize o cadastro da empresa de origem como fornecedor!'
    );

    SetarFoco(FrProduto);
    Abort;
  end;

  if sgItens.Cells[coProdutoId, sgItens.Row] = '' then begin
    _Biblioteca.Exclamar('Nenhum produto foi informado, verifique!');
    SetarFoco(FrProduto);
    Abort;
  end;
end;

end.
