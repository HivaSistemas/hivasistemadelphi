unit Informacao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPrincipal, Vcl.StdCtrls, _Biblioteca,
  Vcl.Imaging.pngimage, Vcl.ExtCtrls, Vcl.Buttons;

type
  TFormInformacao = class(TFormHerancaPrincipal)
    sbSim: TPanel;
    eInformacao: TMemo;
    im1: TImage;
    procedure sbOkClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure CriarInformacao(pCaptionForm: string; pInformacao: string);

implementation

{$R *.dfm}

procedure CriarInformacao(pCaptionForm: string; pInformacao: string);
var
  vForm: TFormInformacao;
begin
  vForm := TFormInformacao.Create(nil);

  vForm.sbSim.Color := _Biblioteca.getCorVerdePadraoAltis;
  vForm.Caption := vForm.Caption + ' ' + pCaptionForm;
  vForm.eInformacao.Lines.Text := pInformacao;

  vForm.ShowModal;

  vForm.Free;
end;

procedure TFormInformacao.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    sbOkClick(Sender);

  if Key = Ord('O') then
    sbOkClick(Sender);
end;

procedure TFormInformacao.sbOkClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrOk;
end;

end.
