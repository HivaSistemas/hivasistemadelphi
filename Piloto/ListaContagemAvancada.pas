unit ListaContagemAvancada;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl,
  Vcl.ComCtrls, Vcl.StdCtrls, CheckBoxLuka, Vcl.Buttons, Vcl.ExtCtrls,
  Vcl.Grids, GridLuka, ComboBoxLuka, FrameDataInicialFinal, FrameMarcas,
  Frame.DepartamentosSecoesLinhas, _FrameHerancaPrincipal, _RecordsEspeciais,
  _FrameHenrancaPesquisas, FrameProdutos, _ListaContagem, _Biblioteca, _Sessao,
  ImpressaoListaContagemEstoque, FrameLocais, FrameEmpresas, RadioGroupLuka,
  Frame.EnderecoEstoque, Frame.EnderecoEstoqueVao, Frame.EnderecoEstoqueNivel,
  Frame.EnderecoEstoqueModulo, Frame.EnderecoEstoqueRua, FrameFaixaNumeros,
  Data.DB, Datasnap.DBClient, frxClass, frxDBSet, FrameInventario;

type
  TFormListaContagemAvancada = class(TFormHerancaRelatoriosPageControl)
    sgItens: TGridLuka;
    frxReport: TfrxReport;
    dstPagar: TfrxDBDataset;
    cdsPagar: TClientDataSet;
    cdsProduto: TStringField;
    cdsNome: TStringField;
    cdsCodigoBarras: TStringField;
    cdsMarca: TStringField;
    cdsUnidade: TStringField;
    cdsQuantidadeFisico: TFloatField;
    cdsCodigoOriginal: TStringField;
    cdsLote: TStringField;
    cdsQuantidadeContada: TStringField;
    cdsAgrupador: TStringField;
    FrInventario: TFrInventario;
    sbIniciar: TSpeedButton;
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure sbImprimirClick(Sender: TObject);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sbIniciarClick(Sender: TObject);
  private
    vEstoques: TArray<RecRelacaoEstoque>;
  protected
    procedure Carregar(Sender: TObject); override;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses _AjustesEstoque, _Inventario;

const
  coProdutoId        = 0;
  coNome             = 1;
  coUnidade          = 2;
  coMarca            = 3;
  coLote             = 4;
  coLocal            = 5;

  coCodigoBarras     = 6;
  coQuantidadeFisico = 7;
  coCodigoOriginal   = 8;

  coTipoLinha        = 9;

  coLinhaDetalhe   = 'D';
  coLinhaCabAgrup  = 'CAGRU';

{ TFormHerancaRelatoriosPageControl1 }

procedure TFormListaContagemAvancada.Carregar(Sender: TObject);
var
  i: Integer;
  j: Integer;
  vLinha: Integer;
  vAgrupadorId: Integer;
  vSql: string;
  vSqlEnderecoEstoque: string;
  vEstoquesTotal: TArray<RecRelacaoEstoque>;
  vEstoqueAjuste: TArray<RecProdutosZerarEstoque>;
  vSqlAjustes: string;
  vAdicionarProduto: Boolean;
begin
  inherited;
  vSql := '';
  sgItens.ClearGrid;

  if FrInventario.EstaVazio then begin
    Exclamar('A contagem de estoque inv�lida!');
    SetarFoco(FrInventario);
    Abort;
  end;

  WhereOuAnd(vSql,
            'EST.EMPRESA_ID in '
           +'(select EMPRESA_ID '
           +'   from ESTOQUES_DIVISAO '
           +'  where EMPRESA_ID = '+ FrInventario.GetInventario.EmpresaId.ToString + ')');

  vSql := ' inner join inventario_itens invi '
         +'    on pro.produto_id = invi.produto_id '
         +'   and invi.local_id = div.local_id '
         +' inner join inventario inv '
         +'    on invi.inventario_id = inv.inventario_id '
         +'   and inv.empresa_id = '+ FrInventario.GetInventario.EmpresaId.ToString
         +'   and inv.'+ FrInventario.getSqlFiltros('INVENTARIO_ID')
         + vSql;

  vEstoquesTotal := _ListaContagem.BuscarEstoque(
    Sessao.getConexaoBanco,
    vSql,
    '',
    False,
    False,
    'order by PRO.PRODUTO_ID');

  if vEstoquesTotal = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;
 
  vEstoques := vEstoquesTotal;
  vLinha := 0;
  vAgrupadorId := -1;
  for i := Low(vEstoques) to High(vEstoques) do begin

    Inc(vLinha);
    sgItens.Cells[coProdutoId, vLinha]       := NFormat(vEstoques[i].ProdutoId);
    sgItens.Cells[coNome, vLinha]            := vEstoques[i].NomeProduto;
    sgItens.Cells[coUnidade, vLinha]         := vEstoques[i].UnidadeVenda;
    sgItens.Cells[coMarca, vLinha]           := NFormatN(vEstoques[i].MarcaId) + ' - ' + vEstoques[i].NomeMarca;
    sgItens.Cells[coLote, vLinha]            := vEstoques[i].Lote;
    sgItens.Cells[coLocal, vLinha]           := NFormatN(vEstoques[i].LocalId);

    // ocultas
    sgItens.Cells[coCodigoBarras, vLinha] := vEstoques[i].CodigoBarras;
    sgItens.Cells[coQuantidadeFisico, vLinha] := vEstoques[i].EstoqueFisico;
    sgItens.Cells[coCodigoOriginal, vLinha] := vEstoques[i].CodigoOriginal;

    sgItens.Cells[coTipoLinha, vLinha]       := coLinhaDetalhe;
  end;

  sgItens.SetLinhasGridPorTamanhoVetor( vLinha );
  SetarFoco(sgItens);

  sbIniciar.Enabled := FrInventario.GetInventario.DataInicio = 0;
end;

procedure TFormListaContagemAvancada.sbImprimirClick(Sender: TObject);
var
  vCnt: Integer;
begin
  inherited;
  //
//  ImpressaoListaContagemEstoque.Imprimir(vEstoques);
  if sgItens.IsRowEmpty(1) then
    exit;
  cdsPagar.Close;
  cdsPagar.CreateDataSet;
  cdsPagar.Open;

  for vCnt := sgItens.RowCount - 1 downto 1 do begin
    cdsPagar.Insert;

    if sgItens.Cells[coTipoLinha, vCnt] = coLinhaCabAgrup then begin
      cdsAgrupador.AsString         := sgItens.Cells[coNome, vCnt];

      if vCnt <> sgItens.RowCount - 1 then begin
        cdsPagar.Post;
        cdsPagar.Insert;
      end;
    end
    else begin
      cdsProduto.AsString           := sgItens.Cells[coProdutoId, vCnt];
      cdsNome.AsString              := sgItens.Cells[coNome, vCnt];
      cdsCodigoBarras.AsString      := sgItens.Cells[coCodigoBarras, vCnt];
      cdsMarca.AsString             := sgItens.Cells[coMarca, vCnt];
      cdsUnidade.AsString           := sgItens.Cells[coUnidade, vCnt];
      cdsCodigoOriginal.AsString    := sgItens.Cells[coCodigoOriginal, vCnt];
      cdsLote.AsString              := sgItens.Cells[coLote, vCnt];
      cdsQuantidadeFisico.AsFloat   := SFormatDouble(sgItens.Cells[coQuantidadeFisico, vCnt]);
      cdsQuantidadeContada.AsString := '--------------';
    end;

    cdsPagar.Post;
  end;

  frxReport.ShowReport;

end;

procedure TFormListaContagemAvancada.sbIniciarClick(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vRetBanco :=
    _Inventario.AtualizarInventario(
      Sessao.getConexaoBanco,
      FrInventario.GetInventario.InventarioId,
      FrInventario.GetInventario.EmpresaId,
      FrInventario.GetInventario.Nome,
      Now,
      0,
      FrInventario.GetInventario.UltimaContagem,
      []
    );

  Sessao.AbortarSeHouveErro(vRetBanco);
  sgItens.ClearGrid;
  FrInventario.Clear;
  sbIniciar.Enabled := False;
end;

procedure TFormListaContagemAvancada.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coNome, coUnidade, coMarca, coLote, coLocal] then
    vAlinhamento := taLeftJustify
  else
    vAlinhamento := taRightJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormListaContagemAvancada.sgItensGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgItens.Cells[coTipoLinha, ARow] = coLinhaCabAgrup then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end;
end;

end.
