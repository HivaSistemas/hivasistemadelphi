unit BaixarControleEntrega;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.StdCtrls, _Biblioteca, _Sessao,
  StaticTextLuka, Vcl.ExtCtrls, Vcl.Grids, GridLuka, Vcl.Buttons, _ControlesEntregas, _Entregas,
  _RecordsExpedicao, _EntregasItens, EditLuka, _Imagens, BuscarInformacoesPessoaRecebeuEntrega,
  _RecordsEspeciais, Informacoes.Entrega, BuscaDados;

type
  TFormBaixarControleEntrega = class(TFormHerancaFinalizar)
    sgEntregas: TGridLuka;
    StaticTextLuka2: TStaticTextLuka;
    sgItens: TGridLuka;
    StaticTextLuka1: TStaticTextLuka;
    lb12: TLabel;
    eEmpresa: TEditLuka;
    eVeiculo: TEditLuka;
    lb13: TLabel;
    eMotorista: TEditLuka;
    Label1: TLabel;
    eControleEntregaId: TEditLuka;
    Label2: TLabel;
    procedure sgEntregasClick(Sender: TObject);
    procedure sgEntregasGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure sgEntregasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
    procedure sgEntregasKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgEntregasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgEntregasDblClick(Sender: TObject);
    procedure sgEntregasKeyPress(Sender: TObject; var Key: Char);
  private
    FItens: TArray<RecEntregaItem>;
    procedure SalvarItemRetornar(
      pEntregaId: Integer;
      pItemId: Integer;
      pLote: string;
      pQuantidade: Double
    );
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Finalizar(Sender: TObject); override;
  end;

function Baixar(pControleEntregaId: Integer): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

const
  (* Grid de entregas *)
  ceConferido         = 0;
  ceEntregaId         = 1;
  ceStatusAnalitico   = 2;
  ceCliente           = 3;
  ceEmpresa           = 4;
  ceLocal             = 5;
  cePesoTotal         = 6;
  ceNomePessoaRecebeu = 7;
  ceCPFPessoaRecebeu  = 8;
  ceObservacoes       = 9;
  ceDataHoraEntrega   = 10;
  ceStatus            = 11;

  (* Grid de itens *)
  ciProdutoId     = 0;
  ciNome          = 1;
  ciMarca         = 2;
  ciLote          = 3;
  ciQuantidade    = 4;
  ciQtdeRetornada = 5;
  ciItemId        = 6;
  ciMultiploVenda = 7;

function Baixar(pControleEntregaId: Integer): TRetornoTelaFinalizar;
var
  i: Integer;
  vForm: TFormBaixarControleEntrega;
  vControle: TArray<RecControlesEntregas>;
  vEntregas: TArray<RecEntrega>;
  vEntregasIds: TArray<Integer>;
begin
  if pControleEntregaId = 0 then
    Exit;

  vEntregasIds := nil;

  vForm := TFormBaixarControleEntrega.Create(nil);

  vControle := _ControlesEntregas.BuscarControles(Sessao.getConexaoBanco, 0, [pControleEntregaId]);

  vForm.eControleEntregaId.SetInformacao(vControle[0].ControleEntregaId);
  vForm.eEmpresa.SetInformacao(vControle[0].EmpresaId, vControle[0].NomeEmpresa);
  vForm.eVeiculo.SetInformacao(vControle[0].VeiculoId, vControle[0].NomeVeiculo);
  vForm.eMotorista.SetInformacao(vControle[0].MotoristaId, vControle[0].NomeMotorista);

  vEntregas := _Entregas.BuscarEntregas(Sessao.getConexaoBanco, 2, [pControleEntregaId]);
  for i := Low(vEntregas) to High(vEntregas) do begin
    vForm.sgEntregas.Cells[ceConferido, i + 1]         := charNaoSelecionado;
    vForm.sgEntregas.Cells[ceEntregaId, i + 1]         := NFormat(vEntregas[i].EntregaId);
    vForm.sgEntregas.Cells[ceCliente, i + 1]           := NFormat(vEntregas[i].ClienteId) + ' - ' + vEntregas[i].NomeCliente;
    vForm.sgEntregas.Cells[ceEmpresa, i + 1]           := NFormat(vEntregas[i].EmpresaId) + ' - ' + vEntregas[i].NomeEmpresa;
    vForm.sgEntregas.Cells[ceLocal, i + 1]             := NFormat(vEntregas[i].LocalId) + ' - ' + vEntregas[i].NomeLocal;
    vForm.sgEntregas.Cells[cePesoTotal, i + 1]         := NFormatNEstoque(vEntregas[i].PesoTotal);
    vForm.sgEntregas.Cells[ceNomePessoaRecebeu, i + 1] := '';
    vForm.sgEntregas.Cells[ceCPFPessoaRecebeu, i + 1]  := '';

    _Biblioteca.AddNoVetorSemRepetir(vEntregasIds, vEntregas[i].EntregaId);
  end;
  vForm.sgEntregas.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vEntregas) );

  vForm.FItens := _EntregasItens.BuscarEntregaItensComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('ITE.ENTREGA_ID', vEntregasIds));
  vForm.sgEntregasClick(nil);

  Result.Ok(vForm.ShowModal, True);
end;

{ TFormBaixarControleEntrega }

procedure TFormBaixarControleEntrega.Finalizar(Sender: TObject);
var
  i: Integer;
  j: Integer;
  vEntregaId: Integer;
  vRetBanco: RecRetornoBD;
  vItens: TArray<RecEntregaItem>;
begin

  Sessao.getConexaoBanco.IniciarTransacao;

  for i := 1 to sgEntregas.RowCount -1 do begin
    vItens := nil;
    vEntregaId := SFormatInt(sgEntregas.Cells[ceEntregaId, i]);
    for j := Low(FItens) to High(FItens) do begin
      if vEntregaId <> FItens[j].EntregaId then
        Continue;

      SetLength(vItens, Length(vItens) + 1);
      vItens[High(vItens)] := FItens[j];
    end;

    vRetBanco :=
      _Entregas.BaixarEntrega(
        Sessao.getConexaoBanco,
        vEntregaId,
        sgEntregas.Cells[ceStatus, i],
        sgEntregas.Cells[ceNomePessoaRecebeu, i],
        sgEntregas.Cells[ceCPFPessoaRecebeu, i],
        ToDataHora(sgEntregas.Cells[ceDataHoraEntrega, i]),
        sgEntregas.Cells[ceObservacoes, i],
        vItens
      );

    if vRetBanco.TeveErro then begin
      Sessao.getConexaoBanco.VoltarTransacao;
      _Biblioteca.Exclamar(vRetBanco.MensagemErro);
      Abort;
    end;
  end;

  vRetBanco :=
    _ControlesEntregas.BaixarControleEntrega(
      Sessao.getConexaoBanco,
      eControleEntregaId.AsInt
    );

  if vRetBanco.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  Sessao.getConexaoBanco.FinalizarTransacao;

  RotinaSucesso;
  inherited;
end;

procedure TFormBaixarControleEntrega.sgEntregasClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vEntregaId: Integer;
begin
  inherited;
  sgItens.ClearGrid();
  vEntregaId := SFormatInt(sgEntregas.Cells[ceEntregaId, sgEntregas.Row]);

  vLinha := 0;
  for i := Low(FItens) to High(Fitens) do begin
    if vEntregaId <> FItens[i].EntregaId then
      Continue;

    Inc(vLinha);
    sgItens.Cells[ciProdutoId, vLinha]     := NFormat(FItens[i].ProdutoId);
    sgItens.Cells[ciNome, vLinha]          := FItens[i].NomeProduto;
    sgItens.Cells[ciMarca, vLinha]         := FItens[i].NomeMarca;
    sgItens.Cells[ciLote, vLinha]          := FItens[i].Lote;
    sgItens.Cells[ciQuantidade, vLinha]    := NFormatEstoque(FItens[i].Quantidade);
    sgItens.Cells[ciQtdeRetornada, vLinha] := '';
    sgItens.Cells[ciItemId , vLinha]       := NFormat(FItens[i].ItemId);
    sgItens.Cells[ciMultiploVenda , vLinha]:= NFormatEstoque(FItens[i].MultiploVenda);
  end;
  sgItens.SetLinhasGridPorTamanhoVetor( vLinha );
end;

procedure TFormBaixarControleEntrega.sgEntregasDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Entrega.Informar( SFormatInt(sgEntregas.Cells[ceEntregaId, sgEntregas.Row]) );
end;

procedure TFormBaixarControleEntrega.sgEntregasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[ceConferido, ceStatusAnalitico] then
    vAlinhamento := taCenter
  else if ACol in[ceEntregaId, cePesoTotal] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgEntregas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormBaixarControleEntrega.sgEntregasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = ceStatusAnalitico then begin
     AFont.Style := [fsBold];

    if sgEntregas.Cells[ceStatus, ARow] = 'RPA' then
      AFont.Color := _Entregas.coCorRetornoParcial
    else if sgEntregas.Cells[ceStatus, ARow] = 'RTO' then
      AFont.Color := _Entregas.coCorRetornoTotal
    else if sgEntregas.Cells[ceStatus, ARow] = 'ENT' then
      AFont.Color := _Entregas.coCorEntregaTotal;
  end;
end;

procedure TFormBaixarControleEntrega.sgEntregasGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgEntregas.Cells[ceEntregaId, ARow] = '' then
    Exit;

  if ACol = ceConferido then begin
    if sgEntregas.Cells[ACol, ARow] = charNaoSelecionado then
      APicture := _Imagens.FormImagens.im1.Picture
    else if sgEntregas.Cells[ACol, ARow] = charSelecionado then
      APicture := _Imagens.FormImagens.im2.Picture;
  end;
end;

procedure TFormBaixarControleEntrega.sgEntregasKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  i: Integer;
  vRetTela: TRetornoTelaFinalizar<RecInfoEntrega>;
begin
  inherited;
  if Key <> VK_SPACE then
    Exit;

  vRetTela.Dados.Status           := sgEntregas.Cells[ceStatus, sgEntregas.Row];
  vRetTela.Dados.NomePessoa       := sgEntregas.Cells[ceNomePessoaRecebeu, sgEntregas.Row];
  vRetTela.Dados.CPFPessoa        := sgEntregas.Cells[ceCPFPessoaRecebeu, sgEntregas.Row];
  vRetTela.Dados.DataHoraRetirada := ToData(sgEntregas.Cells[ceDataHoraEntrega, sgEntregas.Row]);
  vRetTela.Dados.Observacoes      := sgEntregas.Cells[ceObservacoes, sgEntregas.Row];

  vRetTela := BuscarInformacoesPessoaRecebeuEntrega.Buscar(vRetTela.Dados);
  if vRetTela.BuscaCancelada then
    Exit;

  sgEntregas.Cells[ceStatusAnalitico, sgEntregas.Row] :=
    _Biblioteca.Decode(
      vRetTela.Dados.Status, [
      'RPA', 'Ret. parcial',
      'RTO', 'Ret. total',
      'ENT', 'Ent. total']
    );

  sgEntregas.Cells[ceConferido, sgEntregas.Row]         := charSelecionado;
  sgEntregas.Cells[ceStatus, sgEntregas.Row]            := vRetTela.Dados.Status;
  sgEntregas.Cells[ceNomePessoaRecebeu, sgEntregas.Row] := vRetTela.Dados.NomePessoa;
  sgEntregas.Cells[ceCPFPessoaRecebeu, sgEntregas.Row]  := vRetTela.Dados.CPFPessoa;
  sgEntregas.Cells[ceDataHoraEntrega, sgEntregas.Row]   := DHFormatN(vRetTela.Dados.DataHoraRetirada);
  sgEntregas.Cells[ceObservacoes, sgEntregas.Row]       := vRetTela.Dados.Observacoes;

  if vRetTela.Dados.Status = 'RTO' then begin
    for i := 1 to sgItens.RowCount -1 do begin
      sgItens.Cells[ciQtdeRetornada, i] := sgItens.Cells[ciQuantidade, i];

      SalvarItemRetornar(
        SFormatInt(sgEntregas.Cells[ceEntregaId, sgEntregas.Row]),
        SFormatInt(sgItens.Cells[ciItemId, i]),
        sgItens.Cells[ciLote, i],
        SFormatDouble(sgItens.Cells[ciQtdeRetornada, i])
      );
    end;
  end;
end;

procedure TFormBaixarControleEntrega.sgEntregasKeyPress(Sender: TObject; var Key: Char);
var
  vLinhaEntrega: Integer;
  vValorDigitado: string;
begin
  inherited;
  if CharInSet(Key, ['0'..'9', 'A'..'Z', 'a'..'z' , '.', '-' , '?']) then begin
    vValorDigitado := BuscaDados.BuscarDados(tiTexto, 'Busca digitada', Key);
    if vValorDigitado <> '' then begin
      vLinhaEntrega := sgEntregas.Localizar([ceEntregaId], [ NFormat(SFormatInt(vValorDigitado)) ]);
      if vLinhaEntrega > -1 then begin
        sgEntregas.Row := vLinhaEntrega;
        keybd_event(VK_SPACE, 0, 0, 0)
      end;
    end
    else
      SetFocus;
  end;
end;

procedure TFormBaixarControleEntrega.sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
begin
  inherited;
  if ACol <> ciQtdeRetornada then
    Exit;

  if not ValidarMultiplo(SFormatDouble(TextCell), SFormatDouble(sgItens.Cells[ciMultiploVenda, ARow])) then begin
    TextCell := '';
    Exit;
  end;

  TextCell := NFormatNEstoque(SFormatDouble(TextCell));
  if SFormatCurr(TextCell) > SFormatCurr(sgItens.Cells[ciQuantidade, ARow]) then begin
    _Biblioteca.Exclamar('A quantidade a ser retornada n�o pode ser maior que a quantidade pendente!');
    sgItens.SetFocus;
    TextCell := '';
    Exit;
  end;

  SalvarItemRetornar(
    SFormatInt(sgEntregas.Cells[ceEntregaId, sgEntregas.Row]),
    SFormatInt(sgItens.Cells[ciItemId, ARow]),
    sgItens.Cells[ciLote, ARow],
    SFormatDouble(TextCell)
  );
end;

procedure TFormBaixarControleEntrega.SalvarItemRetornar(
  pEntregaId: Integer;
  pItemId: Integer;
  pLote: string;
  pQuantidade: Double
);
var
  i: Integer;
begin
  for i := Low(FItens) to High(FItens) do begin
    if (pEntregaId <> FItens[i].EntregaId) or (pItemId <> FItens[i].ItemId) or (pLote <> FItens[i].Lote) then
      Continue;

    FItens[i].QtdeRetornar := pQuantidade;
    Break;
  end;
end;

procedure TFormBaixarControleEntrega.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[ciItemId, ciProdutoId, ciQuantidade, ciQtdeRetornada] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormBaixarControleEntrega.sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if (ACol = ciQtdeRetornada) and (sgEntregas.Cells[ceStatus, sgEntregas.Row] = 'RPA') then begin
    AFont.Color := _Biblioteca.coCorFonteEdicao2;
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
  end;
end;

procedure TFormBaixarControleEntrega.sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if (ACol = ciQtdeRetornada) and (sgEntregas.Cells[ceStatus, sgEntregas.Row] = 'RPA') then
    sgItens.Options := sgItens.Options + [goEditing]
  else
    sgItens.Options := sgItens.Options - [goEditing];
end;

procedure TFormBaixarControleEntrega.VerificarRegistro(Sender: TObject);
var
  i: Integer;
  j: Integer;

  vEntregaId: Integer;
  vExisteRetorno: Boolean;
  vTodosRetornados: Boolean;
begin
  inherited;

  for i := 1 to sgEntregas.RowCount -1 do begin
    vEntregaId := SFormatInt(sgEntregas.Cells[ceEntregaId, i]);
    if sgEntregas.Cells[ceStatus, i] = '' then begin
      _Biblioteca.Exclamar('O status da entrega ' + NFormat(vEntregaId) + ' ainda n�o foi definido, pressione "Espa�o" sobre o grid de entregas para definir o status!');
      sgEntregas.Row := i;
      SetarFoco(sgEntregas);
      Abort;
    end;

    if sgEntregas.Cells[ceStatus, i] = 'RPA' then begin
      vExisteRetorno := False;
      vTodosRetornados := True;
      for j := Low(FItens) to High(FItens) do begin
        if vEntregaId <> FItens[j].EntregaId then
          Continue;

        if (not vExisteRetorno) and (_Biblioteca.NFormatNEstoque(FItens[j].QtdeRetornar) <> '') then
          vExisteRetorno := True;

        if vTodosRetornados and (_Biblioteca.NFormatEstoque(FItens[j].Quantidade) <> _Biblioteca.NFormatEstoque(FItens[j].QtdeRetornar)) then
          vTodosRetornados := False;
      end;

      if not vExisteRetorno then begin
        _Biblioteca.Exclamar('O status da entrega ' + NFormat(vEntregaId) + ' esta definido como "Retorno parcial", por�m n�o foi definido nenhum produto para retorno!');
        sgEntregas.Row := i;
        SetarFoco(sgEntregas);
        Abort;
      end;

      if vTodosRetornados then begin
        _Biblioteca.Exclamar('O status da entrega ' + NFormat(vEntregaId) + ' esta definido como "Retorno parcial", por�m todos os produtos foram definidos para retornar totalmente!');
        sgEntregas.Row := i;
        SetarFoco(sgEntregas);
        Abort;
      end;
    end;
  end;
end;

end.
