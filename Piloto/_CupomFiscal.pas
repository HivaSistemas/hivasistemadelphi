unit _CupomFiscal;

interface

uses
  System.StrUtils, System.SysUtils, _Sessao, _Biblioteca, _NotasFiscais, _NotasFiscaisItens, _RecordsNotasFiscais, _RecordsCadastros, _ImpressorasECF,
  _RecordsEspeciais, _OrcamentosPagamentos, _RecordsOrcamentosVendas, BuscarDadosCartoesTEF, _ContasReceber, _Orcamentos, _ComunicacaoTEF,
  Impressao.ComprovantePagamentoGrafico, _RecordsFinanceiros, _ContasReceberBaixas, _ContasRecBaixasPagamentos, _ECF;

function EmitirCupomFiscal(pNotaFiscalId: Integer): Boolean;
function EmitirComprovanteNaoFiscal(pReceberBaixaId: Integer): Boolean;

procedure CancelarUltimoCupomFiscal;

implementation

function EmitirCupomFiscal(pNotaFiscalId: Integer): Boolean;
var
  i: Integer;
  vNomePessoa: string;
  vTemCartaoTEF: Boolean;
  vTemCartaoPOS: Boolean;
  vRetorno: RecRespostaBanco;
  vDadosCupom: TArray<RecNotaFiscal>;
  vItensCupom: TArray<RecNotaFiscalItem>;

  vRetProcessamentoTEF: RecRespostaTEF;
  vDadosCartoes: TArray<RecOrcamentosPagamentos>;
  vRetornoCartoes: TRetornoTelaFinalizar<RecCartoes>;
begin
  Result := False;

  if Sessao.getImpressora = nil then begin
    _Biblioteca.Exclamar('N�o foram encontrado dados sobre a impressora de cupom fiscal no seu computador, por favor verifique no cadastros de impressoras fiscais!');
    Abort;
  end;

  if Sessao.ECF.SemComunicacao then
    Exit;

  vDadosCupom := _NotasFiscais.BuscarNotasFiscais(Sessao.getConexaoBanco, 0, [pNotaFiscalId]);
  if vDadosCupom = nil then begin
    _Biblioteca.Exclamar('N�o foram encontrado dados da nota fiscal a ser emitida!');
    Abort;
  end;

  if vDadosCupom[0].nome_consumidor_final <> '' then
    vNomePessoa := vDadosCupom[0].nome_consumidor_final
  else
    vNomePessoa := IfThen(vDadosCupom[0].tipo_pessoa_destinatario = 'F', vDadosCupom[0].nome_fantasia_destinatario, vDadosCupom[0].razao_social_destinatario);

  if
    not _ComunicacaoECF2.AbrirCupom(
      vDadosCupom[0].cpf_cnpj_destinatario,
      vNomePessoa,
      ''
    )
  then
    Exit;

  vItensCupom := _NotasFiscaisItens.BuscarNotasFiscaisItens(Sessao.getConexaoBanco, 0, [pNotaFiscalId]);

  if vItensCupom = nil then begin
    _Biblioteca.Exclamar('Os itens da nota fiscal n�o foram encontrados!');
    Abort;
  end;

  for i := Low(vItensCupom) to High(vItensCupom) do begin
    if not
      _ComunicacaoECF2.VenderItem(
        IntToStr(vItensCupom[i].produto_id),
        vItensCupom[i].nome_produto,
        '',
        vItensCupom[i].quantidade,
        vItensCupom[i].unidade,
        vItensCupom[i].preco_unitario,
        vItensCupom[i].cst,
        vItensCupom[i].percentual_icms,
        vItensCupom[i].valor_total_desconto
      )
    then begin
      _ComunicacaoECF2.CancelarCupomAberto;
      Exit;
    end;
  end;

  if not _ComunicacaoECF2.IniciarFechamentoCupom(0, 0) then begin // Implementar o desconto da capa e outras despesas da capa
    _ComunicacaoECF2.CancelarCupomAberto;
    Exit;
  end;

  vTemCartaoTEF := False;
  vTemCartaoPOS := False;
  if vDadosCupom[0].valor_recebido_cartao > 0 then begin
    vDadosCartoes := _OrcamentosPagamentos.BuscarOrcamentosPagamentos(Sessao.getConexaoBanco, 0, [vDadosCupom[0].orcamento_id]);

    for i := Low(vDadosCartoes) to High(vDadosCartoes) do begin
      if vDadosCartoes[i].tipo_recebimento_cartao = 'T' then
        vTemCartaoTEF := True
      else if vDadosCartoes[i].tipo_recebimento_cartao = 'P' then
        vTemCartaoPOS := True;
    end;

    if vTemCartaoTEF then
      vRetornoCartoes := BuscarDadosCartoesTEF.BuscarRespostaTEF(vDadosCartoes)
    else if vTemCartaoPOS then begin

    end;

    if vRetornoCartoes.RetTela = trCancelado then begin
      _ComunicacaoECF2.CancelarCupomAberto;
      Exit;
    end;
  end;

  if
    not _ComunicacaoECF2.TotalizarCupom(
      vDadosCupom[0].valor_recebido_dinheiro,
      vDadosCupom[0].valor_recebido_cartao,
      vDadosCupom[0].valor_recebido_cobranca,
      vDadosCupom[0].valor_recebido_cheque,
      vDadosCupom[0].valor_recebido_credito,
      vDadosCupom[0].valor_recebido_pos
    )
  then begin
    _ComunicacaoECF2.CancelarCupomAberto;
    Exit;
  end;

  if not _ComunicacaoECF2.FinalizarCupom('','') then begin
    _ComunicacaoECF2.CancelarCupomAberto;
    Exit;
  end;

  if not Sessao.ECF.AtualizarInformacoesECF then begin
    Exclamar('Falha ao pegar os dados do cupom fiscal!');
    _ComunicacaoECF2.CancelarUltimoCupom;
    Exit;
  end;

  Sessao.getConexaoBanco.IniciarTransacao; // Iniciando a transa��o aqui por causa da gera��o de cart�es

  for i := Low(vRetornoCartoes.Dados.Cartoes) to High(vRetornoCartoes.Dados.Cartoes) do begin
    vRetorno :=
      _Orcamentos.GerarCartoesReceber(
        Sessao.getConexaoBanco,
        vDadosCupom[0].orcamento_id,
        vRetornoCartoes.Dados.Cartoes[i].ItemId,
        Sessao.getTurnoCaixaAberto.TurnoId,
        Sessao.getUsuarioLogado.caixa_id,
        Sessao.getTurnoCaixaAberto.FuncionarioId,
        vRetornoCartoes.Dados.Cartoes[i].Nsu,
        vRetornoCartoes.Dados.Cartoes[i].CodigoAutorizacaoTef
      );

    if vRetorno.TeveErro then begin
      Sessao.getConexaoBanco.VoltarTransacao;
      _ComunicacaoECF2.CancelarUltimoCupom;
      _Biblioteca.Exclamar(vRetorno.MensagemErro);
      Exit;
    end;

    vRetorno :=
      _OrcamentosPagamentos.AtualizarOrcamentosPagamentos(
        Sessao.getConexaoBanco,
        vDadosCupom[0].orcamento_id,
        vRetornoCartoes.Dados.Cartoes[i].CobrancaId,
        vRetornoCartoes.Dados.Cartoes[i].ItemId,
        vRetornoCartoes.Dados.Cartoes[i].Valor,
        'CR',
        vRetornoCartoes.Dados.Cartoes[i].nsu
      );

    if vRetorno.TeveErro then begin
      Sessao.getConexaoBanco.VoltarTransacao;
      _ComunicacaoECF2.CancelarUltimoCupom;
      _Biblioteca.Exclamar(vRetorno.MensagemErro);
      Exit;
    end;
  end;

  vRetorno := _NotasFiscais.SetarCupomImpresso(Sessao.getConexaoBanco, pNotaFiscalId, TECF(Sessao.ECF).COO);
  if vRetorno.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    _ComunicacaoECF2.CancelarUltimoCupom;

    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Exit;
  end;

  Sessao.getConexaoBanco.FinalizarTransacao;

  // Imprimir as vias dos cart�es
  if vTemCartaoTEF then begin
    if
      not _ComunicacaoECF2.ImprimirComprovanteTEF(
        'CRT',
        TECF(Sessao.ECF).COO,
        vDadosCupom[0].valor_recebido_cartao,
        BuscarDadosCartoesTEF.getTextoComprovante(vRetornoCartoes.Dados.Cartoes),
        [],
        nil
      )
    then begin
      _Biblioteca.Exclamar('Falha ao imprimir comprovante TEF!' + chr(13) + Chr(10) + 'Reemita pela tela de outras opera��es.');
      Exit;
    end;

    if not vRetornoCartoes.Dados.Cartoes[vRetornoCartoes.Dados.IndexUltimoCartao].Confirmado then begin
      vRetProcessamentoTEF :=
        _ComunicacaoTEF.EnviarConfirmacao(
          _ComunicacaoTEF.getId001,
          IntToStr(vRetornoCartoes.Dados.Cartoes[vRetornoCartoes.Dados.IndexUltimoCartao].id_tef),
          0,
          vRetornoCartoes.Dados.Cartoes[vRetornoCartoes.Dados.IndexUltimoCartao].rede,
          vRetornoCartoes.Dados.Cartoes[vRetornoCartoes.Dados.IndexUltimoCartao].nsu,
          vRetornoCartoes.Dados.Cartoes[vRetornoCartoes.Dados.IndexUltimoCartao].finalizacao
        );

      if not vRetProcessamentoTEF.Confirmado then begin
        CancelarCartoes(vRetornoCartoes.Dados.Cartoes);
        Exclamar('Falha ao confirmar o �ltimo cart�o TEF, todas as outras transa��es foram canceladas!');
        Exit;
      end;
    end;
    Impressao.ComprovantePagamentoGrafico.Imprimir(vDadosCupom[0].orcamento_id);
  end;

  Result := True;
end;

function EmitirComprovanteNaoFiscal(pReceberBaixaId: Integer): Boolean;
var
  i: Integer;
  vTemCartaoTEF: Boolean;
  vTemCartaoPOS: Boolean;
  vRetBanco: RecRespostaBanco;

  vDadosCartoes: TArray<RecContasRecBaixasPagamentos>;
  vRetornoCartoes: TRetornoTelaFinalizar<RecCartoes>;

  vDadosBaixa: TArray<RecContaReceberBaixa>;
begin
  Result := False;

  if Sessao.getImpressora = nil then begin
    _Biblioteca.Exclamar('N�o foi encontrado dados sobre a impressora de cupom fiscal no seu computador, por favor verifique no cadastros de impressoras fiscais!');
    Abort;
  end;

  vDadosBaixa := _ContasReceberBaixas.BuscarContaReceberBaixas(Sessao.getConexaoBanco, 0, [pReceberBaixaId], False);
  if vDadosBaixa = nil then begin
    _Biblioteca.Exclamar('N�o foi encontrado os dados da baixa a receber!');
    Abort;
  end;

  vTemCartaoTEF := False;
  vTemCartaoPOS := False;
  if vDadosBaixa[0].valor_cartao > 0 then begin
//    vDadosCartoes := _ContasRecBaixasPagamentos.BuscarContasRecBaixasPagamentoss(Sessao.getConexaoBanco, 0, [pReceberBaixaId]);

    for i := Low(vDadosCartoes) to High(vDadosCartoes) do begin
      if vDadosCartoes[i].TipoRecebimentoCartao = 'T' then
        vTemCartaoTEF := True
      else if vDadosCartoes[i].TipoRecebimentoCartao = 'P' then
        vTemCartaoPOS := True;
    end;

    if vTemCartaoTEF then
      vRetornoCartoes := BuscarDadosCartoesTEF.BuscarRespostaTEF( BuscarDadosCartoesTEF.ConverterArrayFinanceiro(vDadosCartoes) )
    else if vTemCartaoPOS then begin

    end;

    if vRetornoCartoes.RetTela = trCancelado then
      Exit;
  end;

  vRetBanco := _ContasReceberBaixas.SetarComprovanteImpresso(Sessao.getConexaoBanco, pReceberBaixaId);
  if vRetBanco.TeveErro then begin
    Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  Result := True;
end;

procedure CancelarUltimoCupomFiscal;
begin
  _ComunicacaoECF2.CancelarUltimoCupom;
end;

end.
