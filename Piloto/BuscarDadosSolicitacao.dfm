inherited FormBuscarDadosSolicitacao: TFormBuscarDadosSolicitacao
  Caption = 'Buscar dados solicita'#231#227'o'
  ClientHeight = 91
  ClientWidth = 200
  ExplicitWidth = 206
  ExplicitHeight = 120
  PixelsPerInch = 96
  TextHeight = 14
  object Label1: TLabel [0]
    Left = 8
    Top = 8
    Width = 106
    Height = 14
    Caption = 'N'#250'mero solicita'#231#227'o'
  end
  inherited pnOpcoes: TPanel
    Top = 54
    Width = 200
    inherited sbFinalizar: TSpeedButton
      Left = 41
      ExplicitLeft = 41
    end
  end
  object eNumeroSolicitacao: TEditLuka
    Left = 8
    Top = 24
    Width = 185
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 1
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
