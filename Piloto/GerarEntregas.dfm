inherited FormGerarEntregas: TFormGerarEntregas
  Caption = 'Gerar entrega de produtos'
  ClientHeight = 541
  OnShow = FormShow
  ExplicitLeft = -31
  ExplicitHeight = 570
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 541
    ExplicitHeight = 541
    inherited sbImprimir: TSpeedButton
      Left = 1
      Top = 378
      ExplicitLeft = 1
      ExplicitTop = 378
    end
    object miGerarEntregaProdutosSelecionados: TSpeedButton [2]
      Left = 1
      Top = 67
      Width = 117
      Height = 26
      Caption = 'Gerar &entrega'
      Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = miGerarEntregaProdutosSelecionadosClick
    end
    object miGerarRetiradaProdutosSelecionados: TSpeedButton [3]
      Left = 0
      Top = 106
      Width = 117
      Height = 24
      Caption = 'Gerar &retirada no ato'
      Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = miGerarEntregaProdutosSelecionadosClick
    end
    object miTransformarEntregasSelecionadasSemPrevisao: TSpeedButton [4]
      Left = 1
      Top = 140
      Width = 117
      Height = 26
      Caption = 'trans. em s/&previs'#227'o'
      Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = miTransformarEntregasSelecionadasSemPrevisaoClick
    end
    object miDefinirQuantidadeTodosItens: TSpeedButton [5]
      Left = 1
      Top = 196
      Width = 117
      Height = 25
      Caption = '(F6) Sel. todos itens '
      Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = miDefinirQuantidadeTodosItensClick
    end
    object miRemoverQuantidadeTodosItensF8: TSpeedButton [6]
      Left = 1
      Top = 228
      Width = 117
      Height = 24
      Caption = '(F8) Remov. todos itens '
      Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = miRemoverQuantidadeTodosItensF8Click
    end
    object miLegendas: TSpeedButton [7]
      Left = 1
      Top = 506
      Width = 117
      Height = 24
      Caption = '&Legendas'
      Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = miLegendasClick
    end
    inherited ckOmitirFiltros: TCheckBoxLuka
      Left = 1
      Top = 410
      ExplicitLeft = 1
      ExplicitTop = 410
    end
  end
  inherited pcDados: TPageControl
    Height = 541
    ExplicitHeight = 541
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 29
      ExplicitWidth = 884
      ExplicitHeight = 512
      object lb1: TLabel
        Left = 619
        Top = 1
        Width = 73
        Height = 14
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alCustom
        Caption = 'Agrupamento'
      end
      object Label1: TLabel
        Left = 619
        Top = 47
        Width = 151
        Height = 14
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alCustom
        Caption = 'Aguardando contato cliente'
      end
      inline FrClientes: TFrClientes
        Left = 1
        Top = 3
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 3
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 45
            ExplicitWidth = 45
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
        inherited ckSomenteAtivos: TCheckBox
          Width = 112
          ExplicitWidth = 112
        end
      end
      inline FrProdutos: TFrProdutos
        Left = 0
        Top = 88
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitTop = 88
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 48
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
        inherited ckSomenteAtivos: TCheckBox
          Width = 113
          ExplicitWidth = 113
        end
      end
      inline FrOrcamentos: TFrNumeros
        Left = 418
        Top = 90
        Width = 134
        Height = 72
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 5
        TabStop = True
        ExplicitLeft = 418
        ExplicitTop = 90
        inherited pnDescricao: TPanel
          Caption = ' C'#243'digo do pedido'
        end
      end
      inline FrPrevisaoEntrega: TFrDataInicialFinal
        Left = 416
        Top = 0
        Width = 194
        Height = 43
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 416
        ExplicitHeight = 43
        inherited Label1: TLabel
          Width = 108
          Height = 14
          Caption = 'Previs'#227'o de entrega'
          ExplicitWidth = 108
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrDataRecebimento: TFrDataInicialFinal
        Left = 416
        Top = 46
        Width = 199
        Height = 44
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 4
        TabStop = True
        ExplicitLeft = 416
        ExplicitTop = 46
        ExplicitWidth = 199
        ExplicitHeight = 44
        inherited Label1: TLabel
          Width = 90
          Height = 14
          Caption = 'Data rec. pedido'
          ExplicitWidth = 90
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      object cbAgrupamento: TComboBoxLuka
        Left = 619
        Top = 15
        Width = 141
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 6
        Text = 'Nenhum'
        Items.Strings = (
          'Nenhum'
          'Cliente'
          'Rota')
        Valores.Strings = (
          'NEN'
          'CLI'
          'ROT')
        AsInt = 0
        AsString = 'NEN'
      end
      inline FrVendedores: TFrVendedores
        Left = 0
        Top = 173
        Width = 404
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitTop = 173
        ExplicitWidth = 404
        inherited sgPesquisa: TGridLuka
          Width = 379
          ExplicitWidth = 379
        end
        inherited PnTitulos: TPanel
          Width = 404
          ExplicitWidth = 404
          inherited lbNomePesquisa: TLabel
            Width = 65
            ExplicitWidth = 65
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 299
            ExplicitLeft = 299
          end
        end
        inherited pnPesquisa: TPanel
          Left = 379
          ExplicitLeft = 379
        end
        inherited ckSomenteAtivos: TCheckBox
          Width = 113
          ExplicitWidth = 113
        end
      end
      inline FrLocais: TFrLocais
        Left = 0
        Top = 257
        Width = 403
        Height = 79
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 7
        TabStop = True
        ExplicitTop = 257
        ExplicitWidth = 403
        ExplicitHeight = 79
        inherited sgPesquisa: TGridLuka
          Width = 378
          Height = 62
          ExplicitWidth = 378
          ExplicitHeight = 62
        end
        inherited CkAspas: TCheckBox
          Top = 60
          ExplicitTop = 60
        end
        inherited CkFiltroDuplo: TCheckBox
          Top = 23
          ExplicitTop = 23
        end
        inherited CkPesquisaNumerica: TCheckBox
          Top = 40
          ExplicitTop = 40
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 94
            Caption = 'Local do produto:'
            ExplicitWidth = 94
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          Height = 63
          ExplicitLeft = 378
          ExplicitHeight = 63
        end
      end
      object cbAguardandoCliente: TComboBoxLuka
        Left = 619
        Top = 61
        Width = 166
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 8
        Text = 'N'#227'o'
        Items.Strings = (
          'N'#227'o'
          'Sim')
        Valores.Strings = (
          'N'
          'S')
        AsInt = 0
        AsString = 'N'
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 512
      object spSeparador: TSplitter
        Left = 0
        Top = 176
        Width = 884
        Height = 6
        Cursor = crVSplit
        Align = alTop
        ResizeStyle = rsUpdate
        ExplicitTop = 175
      end
      object sgOrcamentos: TGridLuka
        Left = 0
        Top = 17
        Width = 884
        Height = 159
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alTop
        ColCount = 8
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        PopupMenu = pmEntregasPendentes
        TabOrder = 0
        OnClick = sgOrcamentosClick
        OnDblClick = sgOrcamentosDblClick
        OnDrawCell = sgOrcamentosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Leg.'
          'Pedido'
          'Cliente'
          'Empresa'
          'Local'
          'Data cadastro'
          'Vendedor'
          'Data/hora prev. entrega')
        OnGetCellColor = sgOrcamentosGetCellColor
        OnGetCellPicture = sgOrcamentosGetCellPicture
        Grid3D = False
        RealColCount = 10
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          28
          71
          161
          143
          106
          88
          139
          137)
      end
      object sgItens: TGridLuka
        Left = 0
        Top = 199
        Width = 884
        Height = 271
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 8
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
        PopupMenu = pmItens
        TabOrder = 1
        OnDrawCell = sgItensDrawCell
        OnKeyDown = sgItensKeyDown
        OnSelectCell = sgItensSelectCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Produto'
          'Nome'
          'Marca'
          'Qtde.'
          'Und.'
          'Lote'
          'Qtde. entr./Desag.'
          'Peso total entr.')
        OnGetCellColor = sgItensGetCellColor
        OnArrumarGrid = sgItensArrumarGrid
        Grid3D = False
        RealColCount = 15
        AtivarPopUpSelecao = False
        ColWidths = (
          56
          216
          164
          79
          42
          98
          108
          94)
      end
      object StaticTextLuka1: TStaticTextLuka
        Left = 0
        Top = 182
        Width = 884
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Itens da pend'#234'ncia selecionada'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object StaticTextLuka2: TStaticTextLuka
        Left = 0
        Top = 0
        Width = 884
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Pend'#234'ncias de entregas'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object pnRodape: TPanel
        Left = 0
        Top = 470
        Width = 884
        Height = 42
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 4
        object lb2: TLabel
          Left = 283
          Top = -2
          Width = 69
          Height = 14
          Caption = 'Observa'#231#245'es'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = []
          ParentFont = False
        end
        object st3: TStaticText
          Left = 0
          Top = -2
          Width = 280
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Totais para os produtos definidos'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          Transparent = False
        end
        object st6: TStaticText
          Left = 0
          Top = 13
          Width = 150
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Peso total'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 1
          Transparent = False
        end
        object stPesoTotal: TStaticText
          Left = 0
          Top = 26
          Width = 150
          Height = 16
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '150,000'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 2
          Transparent = False
        end
        object st7: TStaticText
          Left = 149
          Top = 13
          Width = 131
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Qtde. de produtos'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 4
          Transparent = False
        end
        object stQuantidadeProdutos: TStaticText
          Left = 149
          Top = 26
          Width = 131
          Height = 16
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '3'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 3
          Transparent = False
        end
        object meObservacoes: TMemoAltis
          Left = 283
          Top = 10
          Width = 601
          Height = 32
          CharCase = ecUpperCase
          ReadOnly = True
          TabOrder = 5
        end
      end
    end
  end
  object pmEntregasPendentes: TPopupMenu
    Left = 328
    Top = 116
  end
  object pmItens: TPopupMenu
    Left = 312
    Top = 316
  end
end
