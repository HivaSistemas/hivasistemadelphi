inherited FormCadastroProdutoPai: TFormCadastroProdutoPai
  Caption = 'Cadastrar produto pai/filho'
  ClientHeight = 94
  ClientWidth = 570
  ExplicitWidth = 576
  ExplicitHeight = 123
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 472
    Top = 50
    Width = 80
    Height = 14
    Caption = 'Qtde vezes pai'
  end
  inherited pnOpcoes: TPanel
    Height = 94
    ExplicitHeight = 94
    inherited sbExcluir: TSpeedButton
      Left = -104
      Visible = False
      ExplicitLeft = -104
    end
    inherited sbPesquisar: TSpeedButton
      Left = -104
      Visible = False
      ExplicitLeft = -104
    end
  end
  object eID: TEditLuka
    Left = 126
    Top = 20
    Width = 80
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 1
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  inline FrProdutoFilho: TFrProdutos
    Left = 124
    Top = 4
    Width = 442
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 124
    ExplicitTop = 4
    ExplicitWidth = 442
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 417
      Height = 23
      ExplicitWidth = 417
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 442
      ExplicitWidth = 442
      inherited lbNomePesquisa: TLabel
        Width = 69
        Caption = 'Produto filho'
        ExplicitWidth = 69
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 372
        ExplicitLeft = 372
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 417
      Height = 24
      ExplicitLeft = 417
      ExplicitHeight = 24
    end
  end
  inline FrProdutoPai: TFrProdutos
    Left = 124
    Top = 49
    Width = 346
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 124
    ExplicitTop = 49
    ExplicitWidth = 346
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 321
      Height = 23
      ExplicitWidth = 321
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 346
      ExplicitWidth = 346
      inherited lbNomePesquisa: TLabel
        Width = 63
        Caption = 'Produto pai'
        ExplicitWidth = 63
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 276
        ExplicitLeft = 276
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 321
      Height = 24
      ExplicitLeft = 321
      ExplicitHeight = 24
    end
  end
  object eQuantidadeVezesPai: TEditLuka
    Left = 472
    Top = 65
    Width = 93
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    OEMConvert = True
    TabOrder = 4
    Text = '0,000000'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 6
    NaoAceitarEspaco = False
  end
end
