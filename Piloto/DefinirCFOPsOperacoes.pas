unit DefinirCFOPsOperacoes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastro, Vcl.Grids, GridLuka, _FrameHerancaPrincipal,
  _FrameHenrancaPesquisas, FrameEmpresas, Vcl.Buttons, Vcl.ExtCtrls, _Sessao, _Biblioteca,
  Vcl.Menus, Buscar.CfopCstOperacoes, System.Math, _CFOPsCstOperacoes, _RecordsEspeciais;

type
  TFormDefinirCFOPOperacoes = class(TFormHerancaCadastro)
    FrEmpresa: TFrEmpresas;
    sgCFOPs: TGridLuka;
    pmOperacoes: TPopupMenu;
    miDeletarOperacao: TMenuItem;
    miInserirOperacao: TMenuItem;
    miN1: TMenuItem;
    procedure miDeletarOperacaoClick(Sender: TObject);
    procedure miInserirOperacaoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sgCFOPsDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgCFOPsKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    procedure FrEmpresaOnAposPesquisar(Sender: TObject);

    procedure InserirNoGrid(
      pCfopId: string;
      pTipoMovimento: string;
      pTipoOperacao: string;
      pCst: string;
      pTipoMovimentoSint: string;
      pTipoOperacaoSint: string
    );
  protected
    procedure Modo(pEditando: Boolean); override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormDefinirCFOPOperacoes }

const
  coTipoOperacao  = 0;
  coCST           = 1;
  coTipoMovimento = 2;
  coCFOPId        = 3;

  (* Ocultas *)
  coTipoMovimentoSint = 4;
  coTipoOperacaoSint  = 5;

procedure TFormDefinirCFOPOperacoes.FormCreate(Sender: TObject);
begin
  inherited;
  FrEmpresa.OnAposPesquisar := FrEmpresaOnAposPesquisar;
end;

procedure TFormDefinirCFOPOperacoes.FrEmpresaOnAposPesquisar(Sender: TObject);
var
  i: Integer;
  vCFOPs: TArray<RecCFOPsCSTOperacoes>;
begin
  vCFOPs := _CFOPsCstOperacoes.BuscarCFOPCstOperacao(Sessao.getConexaoBanco, 0, [FrEmpresa.getEmpresa.EmpresaId]);

  Modo(True);
  FrEmpresa.Modo(False, False);

  for i := Low(vCFOPs) to High(vCFOPs) do begin
    InserirNoGrid(
      vCFOPs[i].CFOPid,
      vCFOPs[i].TipoMovimentoAnalitico,
      vCFOPs[i].TipoOperacaoAnalitico,
      vCFOPs[i].Cst,
      vCFOPs[i].TipoMovimento,
      vCFOPs[i].TipoOperacao
    );
  end;
end;

procedure TFormDefinirCFOPOperacoes.GravarRegistro(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  vCFOPs: TArray<RecCFOPsCSTOperacoes>;
begin
  inherited;
  vCFOPs := nil;

  SetLength(vCFOPs, sgCFOPs.RowCount -1);
  for i := 1 to sgCFOPs.RowCount -1 do begin
    vCFOPs[i - 1].CFOPid        := sgCFOPs.Cells[coCFOPid, i];
    vCFOPs[i - 1].TipoMovimento := sgCFOPs.Cells[coTipoMovimentoSint, i];
    vCFOPs[i - 1].TipoOperacao  := sgCFOPs.Cells[coTipoOperacaoSint, i];
    vCFOPs[i - 1].Cst           := sgCFOPs.Cells[coCst, i];
  end;

  vRetBanco := _CFOPsCstOperacoes.AtualizarCFOPsCst(Sessao.getConexaoBanco, FrEmpresa.getEmpresa.EmpresaId, vCFOPs);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  RotinaSucesso;
  Modo(False);
end;

procedure TFormDefinirCFOPOperacoes.InserirNoGrid(
  pCfopId: string;
  pTipoMovimento: string;
  pTipoOperacao: string;
  pCst: string;
  pTipoMovimentoSint: string;
  pTipoOperacaoSint: string
);
var
  vLinha: Integer;
begin
  vLinha := sgCFOPs.Localizar([coTipoOperacaoSint, coTipoMovimentoSint, coCST, coCFOPId], [pTipoOperacaoSint, pTipoMovimentoSint, pCst, pCfopId]);
  if vLinha > -1 then begin
    _Biblioteca.Exclamar('O registro j� se encontra inserido no grid!');
    sgCFOPs.Row := vLinha;
    Exit;
  end;

  if sgCFOPs.Cells[coTipoOperacao, 1] = '' then
    vLinha := 1
  else
    vLinha := sgCFOPs.RowCount;

  sgCFOPs.Cells[coTipoOperacao, vLinha]      := pTipoOperacao;
  sgCFOPs.Cells[coCST, vLinha]               := pCst;
  sgCFOPs.Cells[coTipoMovimento, vLinha]     := pTipoMovimento;
  sgCFOPs.Cells[coCFOPId, vLinha]            := pCfopId;
  sgCFOPs.Cells[coTipoMovimentoSint, vLinha] := pTipoMovimentoSint;
  sgCFOPs.Cells[coTipoOperacaoSint, vLinha]  := pTipoOperacaoSint;

  sgCFOPs.RowCount := IfThen(vLinha > 1, vLinha + 1, 2);
end;

procedure TFormDefinirCFOPOperacoes.miDeletarOperacaoClick(Sender: TObject);
begin
  inherited;
  sgCFOPs.DeleteRow(sgCFOPs.Row);
end;

procedure TFormDefinirCFOPOperacoes.miInserirOperacaoClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar<RecOperacoesCST>;
begin
  inherited;
  vRetTela := Buscar.CfopCstOperacoes.Buscar(0, '', '', '');
  if vRetTela.BuscaCancelada then
    Exit;

  InserirNoGrid(
    vRetTela.Dados.CfopId,
    vRetTela.Dados.TipoMovimentoAnalitico,
    vRetTela.Dados.TipoOperacaoAnalitico,
    vRetTela.Dados.Cst,
    vRetTela.Dados.TipoMovimento,
    vRetTela.Dados.TipoOperacao
  );
end;

procedure TFormDefinirCFOPOperacoes.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([sgCFOPs], pEditando);

  if not pEditando then begin
    FrEmpresa.Modo(True);
    _Biblioteca.SetarFoco(FrEmpresa);
  end;
end;

procedure TFormDefinirCFOPOperacoes.sgCFOPsDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  inherited;
  sgCFOPs.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taCenter, Rect);
end;

procedure TFormDefinirCFOPOperacoes.sgCFOPsKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_INSERT then
    miInserirOperacaoClick(nil)
  else if Key = VK_DELETE then
    miDeletarOperacaoClick(nil);
end;

procedure TFormDefinirCFOPOperacoes.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
