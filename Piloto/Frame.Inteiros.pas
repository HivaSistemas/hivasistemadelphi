unit Frame.Inteiros;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.Grids, BuscaDados,
  GridLuka, _Biblioteca, Vcl.ExtCtrls;

type
  TFrameInteiros = class(TFrameHerancaPrincipal)
    sgInteiros: TGridLuka;
    Panel1: TPanel;
    procedure sgInteirosKeyPress(Sender: TObject; var Key: Char);
    procedure sgInteirosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgInteirosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  private
    { Private declarations }
  public
    function getSqlFiltros(pColuna: string): string; overload;
    function getSqlFiltros(pColuna: string; var pSqlAtual: string; pWhereOuAnd: Boolean = True): string; overload;

    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
    procedure Clear; override;
    function EstaVazio: Boolean; override;
  end;

implementation

{$R *.dfm}

const
  coCodigo = 0;

procedure TFrameInteiros.Clear;
begin
  inherited;
  sgInteiros.ClearGrid(1);
end;

function TFrameInteiros.EstaVazio: Boolean;
begin
  Result := sgInteiros.Cells[coCodigo, 0] = '';
end;

function TFrameInteiros.getSqlFiltros(pColuna: string; var pSqlAtual: string; pWhereOuAnd: Boolean): string;
begin
  Result := getSqlFiltros(pColuna);

  if (Result <> '') and pWhereOuAnd then
    _Biblioteca.WhereOuAnd(pSqlAtual, Result);
end;

function TFrameInteiros.getSqlFiltros(pColuna: string): string;
var
  i: Integer;
  vInteiros: TArray<Integer>;
begin
  Result := '';
  if sgInteiros.Cells[coCodigo, 0] = '' then begin
    Result := '';
    Exit;
  end;

  SetLength(vInteiros, sgInteiros.RowCount);
  for i := 0 to sgInteiros.RowCount -1 do
    vInteiros[i] := SFormatInt(sgInteiros.Cells[coCodigo, i]);

  Result := FiltroInInt(pColuna, vInteiros);
end;

procedure TFrameInteiros.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;

  sgInteiros.EditorMode := pEditando;
  sgInteiros.Enabled := pEditando;

  if pLimpar then
  	Clear;
end;

procedure TFrameInteiros.sgInteirosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  inherited;
  sgInteiros.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taRightJustify, Rect);
end;

procedure TFrameInteiros.sgInteirosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_DELETE then begin
    sgInteiros.DeleteRow(sgInteiros.Row);
    sgInteiros.Repaint;
  end;
end;

procedure TFrameInteiros.sgInteirosKeyPress(Sender: TObject; var Key: Char);
var
  vLinha: Integer;
  vDigito: string;
begin
  inherited;
  if CharInSet(Key, ['0'..'9']) then begin
    vDigito := BuscaDados.BuscarDados(tiNumerico, 'Busca digitada', Key);
    if vDigito = '' then
      Exit;

    if sgInteiros.Cells[coCodigo, 0] = '' then
      vLinha := 0
    else
      vLinha := sgInteiros.RowCount;

    sgInteiros.Cells[coCodigo, vLinha] := vDigito;
    sgInteiros.RowCount := vLinha + 1;

    sgInteiros.Row := vLinha;
    SetarFoco(sgInteiros);
  end;
end;

end.
